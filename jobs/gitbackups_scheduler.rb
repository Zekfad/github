# rubocop:disable Style/FrozenStringLiteralComment
require "scientist"

module GitHub
  module Jobs
    # Timer that schedules maintenance runs for Git backups
    class GitbackupsScheduler < Job
      def self.active_job_class
        ::GitbackupsSchedulerJob
      end

      areas_of_responsibility :backups

      extend Scientist

      # Adjust as the number of networks and workers changes
      schedule interval: 5.minutes

      MAINTENANCE_JOB_THRESHOLD = 500

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.queue_length
        Resque.size(GitHub::Jobs::GitbackupsMaintenance.queue)
      end

      # Run every interval.
      def self.perform
        Failbot.push app: "gitbackups"

        return if queue_length > MAINTENANCE_JOB_THRESHOLD

        # We give each kind a budget of 250, which need-maintenance splits into
        # most-active and least-maintained.
        budget = 250

        schedule_networks(budget)

        schedule_wikis(budget)

        schedule_gists(budget)
      end

      # Schedule up to +max+ networks for maintenance
      def self.schedule_networks(max)
        schedule_maintenance("networks", max: max, to_spec: lambda { |n| "network/#{n["network_id"]}" })
      end

      # Schedule up to +max+ wikis for maintenance
      def self.schedule_wikis(max)
        schedule_maintenance("wikis", max: max, to_spec: lambda { |w| "#{w["network_id"]}/#{w["repository_id"]}.wiki" })
      end

      # Schedule up to +max+ gists for maintenance
      def self.schedule_gists(max)
        schedule_maintenance("gists", max: max, to_spec: lambda { |g| "gist/#{g["repo_name"]}" })
      end

      # Returns which networks need maintenance.
      #
      # max - maximum number of results to return
      # count - minimum number of pending incrementals in order to return
      def self.schedule_maintenance(repo_type, max: 20, to_spec: nil)
        needing_maintenance = science "gitbackups.vitess.need-maintenance" do |e|
          e.use do
            GitHub::Backups.need_maintenance(repo_type, max: max)
          end
          e.try do
            GitHub::Backups.need_maintenance(repo_type, max: max, use_vitess: true)
          end
        end

        # Only those with over +count+ pending incrementals have enough going
        # on for us to run maintenance
        to_schedule = needing_maintenance.map(&to_spec)

        GitHub::Backups.schedule_maintenance(to_schedule)

        return to_schedule.length
      end

      # The queue the scheduler runs on. The actual maintenance jobs don't run
      # on this queue but on the specific queue for them.
      def self.queue
        :gitbackups_scheduler
      end

    end
  end
end
