# frozen_string_literal: true

module GitHub
  module Jobs
    class DisableRepositoryInteractionLimits < Job
      def self.active_job_class
        ::DisableRepositoryInteractionLimitsLegacyJob
      end

      areas_of_responsibility :community_and_safety

      def self.queue
        :disable_repository_interaction_limits
      end

      def self.perform(owner_id, actor_id)
        new(owner_id, actor_id).perform
      end

      def initialize(owner_id, actor_id)
        @owner_id = owner_id
        @actor_id = actor_id
      end

      def perform
        return unless GitHub.interaction_limits_enabled?

        owner = User.find(@owner_id)
        actor = User.find(@actor_id)

        owner.repositories.public_scope.each do |repo|
          RepositoryInteractionAbility.disable_all_for(:repository, repo, actor)
        end
      end
    end
  end
end
