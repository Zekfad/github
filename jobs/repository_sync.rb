# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Moves objects and refs pushed into an alternated repository to the
    # shared storage area in network.git, synchronizing all objects and refs.
    #
    # See the docs on repository storage for more information on
    # synchronization and shared object storage:
    #
    # https://githubber.com/article/technology/dotcom/repository-storage
    #
    # This job is enqueued from the RepositoryPush job and runs at most one
    # process per repository.
    #
    # See the git-nw-sync manual for more information on what happens on the
    # fs backend:
    #
    # https://github.com/github/git-nw/blob/master/man/git-nw-sync.1.ronn
    class RepositorySync < Job
      def self.active_job_class
        ::RepositorySyncJob
      end

      areas_of_responsibility :git, :repositories

      extend GitHub::HashLock

      # Exception raised when the job fails due to git command failure.
      class Failed < StandardError
      end

      # Perform the sync of objects and refs into network.git.
      #
      # repo_id - The integer repository id.
      #
      # Returns nothing.
      def self.perform(repo_id)
        repository = Repository.find_by_id(repo_id)
        return if repository.nil?

        # give failbot some additional context for exception reports
        Failbot.push spec: repository.dgit_spec,
                     alternates: repository.shared_storage_enabled?

        # if a backup-utils backup is in progress, delay the sync operation by
        # requeuing after a short sleep period.
        if GitHub::Enterprise.backup_in_progress?
          clear_lock(repo_id)
          repository.synchronize_shared_storage
          sleep 1
          return
        end

        GitHub.dogstats.time "repository", tags: ["action:synchronize_shared_storage", "via:job"] do
          repository.synchronize_shared_storage!
        end
      rescue GitRPC::CommandFailed, Repository::CommandFailed => boom
        raise RepositorySync::Failed, "Failed to GC repository: #{boom.message}"
      end

      def self.queue
        :repository_sync
      end
    end
  end
end
