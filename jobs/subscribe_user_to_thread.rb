# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class SubscribeUserToThread < Job
      def self.active_job_class
        ::SubscribeUserToThreadJob
      end

      areas_of_responsibility :notifications

      def self.queue
        :notifications
      end

      # Subscribes a User to a thread, by calling
      # GitHub.newsies.subscribe_user_to_thread
      def self.perform_with_retry(user_id, repository_id, commit_oid, reason = nil, options = {})
        # Support both string and symbol keys from legacy RockQueue.
        options.deep_stringify_keys!

        user, repo = nil
        ActiveRecord::Base.connected_to(role: :reading) do
          user = User.find_by_id(user_id)
          repo = Repository.find_by_id(repository_id)
        end

        return unless user && repo
        return unless commit = repo.commits.find(commit_oid)

        response = Newsies::ThreadSubscription.throttle { GitHub.newsies.subscribe_to_thread(user, repo, commit, reason) }

        if response.failed?
          retries = options["retries"].to_i
          LegacyApplicationJob.retry_later(self.active_job_class, [user_id, repository_id, commit_oid, reason, options], retries: retries)
        end
      end
    end
  end
end
