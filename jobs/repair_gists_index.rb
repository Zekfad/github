# rubocop:disable Style/FrozenStringLiteralComment

require "github/redis/mutex"

module GitHub
  module Jobs

    # The purpose of this job is to reconcile the state of Gist records
    # between the database and the search index. We do this by iterating over
    # Gist records in batches of 250. The `updated_at` timestamp of the
    # each PR is compared between the database and the search index. The
    # search index is then updated for any PRs where the timestamps disagree.
    #
    # Each repair job will process 250 pull requests. When it has reconciled
    # all those pull requests it wil enqueue another job. This process will
    # continue until all pull requests have been processed.
    #
    # To make this whole process faster, multiple repair jobs can be enqueued.
    # The current offest into the gists table is stored in redis.
    # Access to this value is coordinated via a shared mutex. Don't spin up
    # too many repair jobs otherwise you'll kill the database or the search
    # index or both.
    class RepairGistsIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairGistsIndexJob
      end

      areas_of_responsibility :search, :gist

      reconcile "gist",
        fields: %w[updated_at],
        limit: 250,
        accept: :gist_is_searchable?,
        include: [:user]

      def self.queue
        :index_bulk
      end
    end
  end
end
