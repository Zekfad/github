# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Timer that schedules maintenance jobs for individual gists. See
    # Gist::Maintenance for scheduling logic and different types of
    # maintenance tasks.
    class GistMaintenanceScheduler < Job
      def self.active_job_class
        ::GistMaintenanceSchedulerJob
      end

      areas_of_responsibility :gist

      schedule interval: 2.minutes

      # This job is scheduled in GHE and github.com environments.
      def self.enabled?
        true
      end

      # The number of maintenance jobs to enqueue at each interval.
      def self.jobs_per_interval
        1000
      end

      # Minimum length of time between maintenance runs for any single
      # gist in seconds. Gists that have received maintenance
      # within this time period will not be scheduled unless they've received
      # more than 50 pushes.
      def self.min_age
        1.week
      end

      # Run every interval, schedules jobs_per_interval maintenance jobs to run.
      def self.perform
        SlowQueryLogger.disabled do
          gists = Gist.schedule_maintenance(jobs_per_interval, min_age)

          unless gists.empty?
            most_stale  = gists.min_by { |gist| gist.last_maintenance_at || gist.created_at }
            lag_time = Time.now - (most_stale.last_maintenance_at || most_stale.created_at)
            GitHub.dogstats.histogram("gist", lag_time, tags: ["action:maint", "type:lag"])

            most_active = gists.max_by { |gist| gist.pushed_count_since_maintenance }
            GitHub.dogstats.histogram("gist", most_active.pushed_count_since_maintenance, tags: ["action:maint", "type:most_active"])
          end

          Gist.maintenance_counts.each do |status, gist_count|
            GitHub.dogstats.gauge("git_maintenance.count", gist_count, tags: ["type:gist", "status:#{status}"])
          end

          gists
        end
      end

      # The queue the scheduler runs on. The actual maintenance jobs don't run
      # on this queue, they're enqueued on the fs maintenance queues.
      def self.queue
        :gist_maintenance_scheduler
      end
    end
  end
end
