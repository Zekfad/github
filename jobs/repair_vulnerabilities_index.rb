# frozen_string_literal: true

module GitHub
  module Jobs
    class RepairVulnerabilitiesIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairVulnerabilitiesIndexJob
      end

      areas_of_responsibility :security_advisories

      reconcile("vulnerability", {
        fields: ["updated_at"],
        limit: 500,
        accept: :globally_available?,
        include: [:vulnerable_version_ranges],
      })

      def self.queue
        :index_bulk
      end
    end
  end
end
