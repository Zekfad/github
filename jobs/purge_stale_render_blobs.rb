# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class PurgeStaleRenderBlobs < Job
      def self.active_job_class
        ::PurgeStaleRenderBlobsJob
      end

      areas_of_responsibility :rendering

      extend GitHub::HashLock

      schedule interval: 1.hour

      class << self
        attr_accessor :purge_batch_size
        attr_accessor :ttl
      end
      self.purge_batch_size = 100
      self.ttl = 24.hours

      def self.queue
        :purge_stale_render_blobs
      end

      def self.enabled?
        GitHub.storage_cluster_enabled?
      end

      def self.expire_batch
        RenderBlob.where(["updated_at < ?", ttl.ago]).limit(purge_batch_size)
      end

      def self.perform
        loop do
          render_blobs = expire_batch
          render_blobs.destroy_all
          break if expire_batch.count == 0
        end
      end
    end
  end
end
