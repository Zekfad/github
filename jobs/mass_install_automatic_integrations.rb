# frozen_string_literal: true

module GitHub
  module Jobs
    class MassInstallAutomaticIntegrations < InstallAutomaticIntegrations
      areas_of_responsibility :ecosystem_apps

      TIME_TO_INSTALL_KEY = "mass_apps_installations:mean_time_to_install_ms"

      def self.active_job_class
        ::MassInstallAutomaticIntegrationsJob
      end

      def self.queue
        :mass_install_automatic_integrations
      end

      def self.perform_with_retry(target_id, integration_id, trigger_id, repo_ids = INSTALL_ON_ALL_REPOS, options = {})
        IntegrationInstallation.throttle do
          PendingAutomaticInstallation.throttle do
            super
          end
        end
      end

      def self.track_time_to_install(integration, mean_time_to_install_ms)
        ::Redis.new(GitHub.redis_config).set(TIME_TO_INSTALL_KEY, mean_time_to_install_ms)

        super
      end
      private_class_method :track_time_to_install
    end
  end
end
