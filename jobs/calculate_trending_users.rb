# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class CalculateTrendingUsers < Job
      def self.active_job_class
        ::CalculateTrendingUsersJob
      end

      areas_of_responsibility :explore

      def self.queue
        :calculate_trending_users
      end

      def self.perform(period)
        GitHub.dogstats.time("calculate_trending_users", tags: ["action:calculate", "type:#{period}"]) do
          self.calculate(period)
        end

      rescue => error
        Failbot.report(error)
        raise
      end

      def self.clear_cache(period)
        GitHub.cache.delete("trending:users:query:#{period}")
      end

      def self.calculate(period)
        clear_cache(period)

        users = Trending.users({
          period: period,
          from_job: true,
        })
      end
    end
  end
end
