# frozen_string_literal: true

module GitHub
  module Jobs
    class CreatePullRequestResolvedMergeCommit < Job
      def self.active_job_class
        ::CreatePullRequestResolvedMergeCommitJob
      end

      areas_of_responsibility :pull_requests

      def self.queue
        :create_pull_request_merge_commit
      end

      def self.perform(job_id, pull_request_id, user_id, base_sha, head_sha, resolved_files, new_head_ref = nil)
        status = JobStatus.find(job_id)
        return unless status

        status.track do
          return unless pull = PullRequest.find_by_id(pull_request_id)
          return unless user = User.find_by_id(user_id)

          context = {
            user_id: user.id,
            repo_id: pull.repository_id,
            pull_request_id: pull.id,
            base_oid: base_sha,
            head_oid: head_sha,
          }
          context[:new_head_ref] = new_head_ref if new_head_ref
          Failbot.push(context)

          if new_head_ref
            unless pull.head_repository.writable_by?(user)
              Failbot.report(Error.new("Head repository not writable by user"))
              return
            end

            pull.head_repository.heads.create(new_head_ref, head_sha, user)
            pull.head_ref = new_head_ref
            pull.save!
            pull.synchronize!(user: user, repo: pull.repository)
            pull.reload
          end

          pull.merge_base_into_head(
            user: user,
            author_email: user.default_author_email(pull.repository, pull.head_sha),
            base_oid: base_sha,
            expected_head_oid: head_sha,
            resolve_conflicts: resolved_files.each_with_object({}) { |(filename, contents), result| result[CGI.unescape(filename)] = contents },
          )
        end
      end
    end
  end
end
