# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs

    # The purpose of this job is to reconcile the models with the index.
    #
    # To make this whole process faster, multiple repair jobs can be enqueued. The current offset
    # into the topics table is stored in redis. Access to this value is coordinated via a shared
    # mutex. Don't spin up too many repair jobs otherwise you'll kill the database or the search
    # index or both.
    class RepairWorkflowRunsIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairWorkflowRunsIndexJob
      end

      areas_of_responsibility :search

      reconcile "workflow_run",
        fields: %w[updated_at],
        limit: 250,
        model_class: Actions::WorkflowRun

      def self.queue
        :index_bulk
      end
    end
  end
end
