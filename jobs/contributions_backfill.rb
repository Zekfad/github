# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ContributionsBackfill < Job
      def self.active_job_class
        ::ContributionsBackfillJob
      end

      areas_of_responsibility :user_profile
      # How many concurrent jobs with the same key are allowed. Since this is
      # backfilling contributions for an entire repository, only one at a time
      # per repository, please:
      RUNNING_JOBS_PER_KEY = 1

      # How long a running (or crashed) job is allowed to hold onto a lock, in
      # seconds, before it expires and another one takes over.
      JOB_LOCK_TTL = 10.minutes

      def self.guid(repo_id, reset)
        ary = [self.name, repo_id, reset]
        Digest::SHA1.hexdigest(ary.to_json)
      end

      def self.perform_with_retry(repo_id, reset = false, options = {})
        options = options.with_indifferent_access
        repo = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(repo_id) }
        return if repo.nil?

        Failbot.push repo: repo&.readonly_name_with_owner

        key = ["contrib-backfill", repo.id].join(":")
        restraint.lock!(key, RUNNING_JOBS_PER_KEY, JOB_LOCK_TTL) do
          CommitContribution.backfill!(repo, reset)
        end
      rescue GitHub::Restraint::UnableToLock
        options["guid"] ||= guid(repo_id, reset)
        LegacyApplicationJob.retry_later(self.active_job_class, [repo_id, reset, options], retries: options["retries"], guid: options["guid"])
      rescue ::GitRPC::InvalidRepository
        # The repository either doesn't exist yet or has been deleted. Either way
        # we should behave as though we hadn't found the repository in the database.
        nil
      end

      # Internal: a concurrency restraint using resque redis
      def self.restraint
        @restraint ||= GitHub::Restraint.new
      end

      def self.queue
        :contributions_backfill
      end
    end
  end
end
