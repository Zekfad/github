# frozen_string_literal: true

module GitHub
  module Jobs
    # Updates a user's email marketing preference.
    class UpdateNewsletterPreference < Job
      def self.active_job_class
        ::UpdateNewsletterPreferenceJob
      end

      areas_of_responsibility :user_growth

      def self.queue
        :user_signup
      end

      #
      # user_id - the User id
      # preference - "transactional" or "marketing"
      # signup - a Boolean. Whether or not the preference is being set as part of the signup process
      #
      def self.perform(user_id, preference, signup = false)
        return unless user = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }

        if preference == "marketing"
          NewsletterPreference.set_to_marketing(user: user, source: "job", signup: signup)
        else
          NewsletterPreference.set_to_transactional(user: user, source: "job")
        end
      end
    end
  end
end
