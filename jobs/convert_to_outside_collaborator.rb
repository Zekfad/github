# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ConvertToOutsideCollaborator < Job
      def self.active_job_class
        ::ConvertToOutsideCollaboratorJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :convert_to_outside_collaborator
      end

      def self.perform(org_id, user_id, options = {})
        new(org_id, user_id, options).perform
      end

      def initialize(org_id, user_id, options = {})
        @org_id = org_id
        @user_id = user_id
        @save_settings = options["save_settings"] || options[:save_settings]
      end

      def perform
        return unless org = Organization.find(@org_id)
        return unless user = User.find(@user_id)

        org.convert_to_outside_collaborator!(user, save_settings: @save_settings)
      end
    end
  end
end
