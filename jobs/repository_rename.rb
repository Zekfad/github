# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RepositoryRename < Job
      def self.active_job_class
        ::RepositoryRenameJob
      end

      areas_of_responsibility :repositories

      def self.queue
        :critical
      end

      def self.perform(repo_id, old_name, old_lock_reason = nil)
        new(repo_id, old_name, old_lock_reason).perform
      end

      attr_reader :repo, :old_name, :old_lock_reason

      def initialize(repo_id, old_name, old_lock_reason = nil)
        @repo = Repository.find_by_id(repo_id)
        @old_name = old_name
        @old_lock_reason = old_lock_reason
      end

      def perform
        fail "repo doesn't exist" if repo.nil?

        # more context for exceptions
        Failbot.push(repo_id: repo.id)

        repo.public_keys.each(&:save)
        repo.rename_downloads old_name, repo.name
        GitHub::Spokes.client.write_nwo_file(repo, repo.name_with_owner)
      ensure
        if old_lock_reason
          repo.lock_including_descendants!(old_lock_reason)
        elsif repo.locked
          repo.unlock_including_descendants! if repo.locked_on_rename?
        end
      end
    end
  end
end
