# frozen_string_literal: true

module GitHub
  module Jobs
    class NewsletterPreview < Job
      def self.active_job_class
        ::NewsletterPreviewJob
      end

      areas_of_responsibility :email, :devtools, :dependabot_alerts

      def self.queue
        :devtools
      end

      def self.perform(type, user_id, period, email)
        user = User.find(user_id)
        case type
        when "explore"
          ExploreMailer.overview(user: user, period: period, email: email).deliver_now
        when "vulnerability"
          email ||= user.email
          VulnerabilityMailer.digest(user: user, period: period, email: email, accounts: [user]).deliver_now
        else
          Failbot.report(ArgumentError.new("Invalid type for NewsletterPreview: #{type}"), user: user.login)
        end
      end
    end
  end
end
