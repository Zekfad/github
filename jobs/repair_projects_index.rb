# rubocop:disable Style/FrozenStringLiteralComment

require "github/redis/mutex"

module GitHub
  module Jobs

    # The purpose of this job is to reconcile the models with the index.
    #
    # To make this whole process faster, multiple repair jobs can be enqueued.
    # The current offest into the repositories table is stored in redis.
    # Access to this value is coordinated via a shared mutex. Don't spin up
    # too many repair jobs otherwise you'll kill the database or the search
    # index or both.
    class RepairProjectsIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairProjectsIndexJob
      end

      areas_of_responsibility :search

      reconcile "project",
        fields: %w[updated_at],
        include: [:owner],
        limit: 500

      reconcile "project_card",
        fields: %w[updated_at],
        include: [:project, :content],
        limit: 500

      # Returns the queue where the repair job will run.
      def self.queue
        :index_bulk
      end
    end
  end
end
