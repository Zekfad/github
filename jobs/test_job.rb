# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # The Hello World of resque jobs.
    #
    # To queue up one of these:
    #
    #     TestJob.perform_later
    #
    # To make it run for a different length of time (default is 1.0 seconds):
    #
    #     TestJob.perform_later(3.0)
    #
    # To allocate a 30M string to emulate memory use:
    #
    #     TestJob.perform_later(3.0, 30 * 1024 * 1024)
    #
    # To put it on a different queue:
    #
    #     TestJob.set(queue: :a_different_queue).perform_later
    #
    # To queue up a bunch:
    #
    #     10.times { TestJob.perform_later }
    class TestJob
      def self.active_job_class
        ::TestJob
      end

      def self.queue
        :test
      end

      def self.perform(duration = 1.0, alloc = 0, should_raise = false)
        GitHub::Logger.log fn: "TestJob", duration: duration, alloc: alloc
        # Test the database connection
        User.count
        # Test the redis connection
        GitHub.resque_redis.exists("test:key")
        # Allocate some RES mem before sleeping
        string = "x" * alloc
        # Take up space in resque
        sleep(duration)

        # Blow up if we were asked to.
        raise "kaboom" if should_raise
      end
    end
  end
end
