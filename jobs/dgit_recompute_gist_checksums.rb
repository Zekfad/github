# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    class DgitRecomputeGistChecksums < Job
      def self.active_job_class
        ::SpokesRecomputeGistChecksumsJob
      end

      areas_of_responsibility :dgit

      extend GitHub::HashLock

      def self.perform(gist_id, from_host)
        Failbot.push spec: "gist/#{gist_id}",
                     app: "github-dgit"
        GitHub::Logger.log_context(job: name.demodulize, spec: "gist/#{gist_id}") do
          GitHub::Logger.log(method: "perform!", from_host: from_host) do
            perform!(gist_id, from_host)
          end
        end
      rescue Freno::Throttler::Error
      end

      def self.perform!(gist_id, from_host)
        raise ArgumentError, "given nil gist_id (from_host: #{from_host})" if gist_id.nil?
        gist = Gist.find_by_id(gist_id)
        raise GitHub::DGit::ChecksumInitError, "Gist ID #{gist_id} not found" unless gist

        from_host = :vote if from_host == "vote"

        GitHub::DGit::Maintenance.recompute_gist_checksums(gist, from_host)
      end

      def self.queue
        :dgit_repairs
      end
    end
  end
end
