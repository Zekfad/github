# rubocop:disable Style/FrozenStringLiteralComment
require "github/jobs/retryable_job"

module GitHub
  module Jobs
    class PostToHookshot < Job
      def self.active_job_class
        ::PostToHookshotJob
      end

      areas_of_responsibility :webhook

      extend Hookshot::DeliverJobLogger
      include GitHub::Jobs::RetryableJob

      def self.queue
        :post_to_hookshot
      end

      def self.perform_with_retry(payload, options = {})
        # round-trip through JSON to get string keys only
        payload = GitHub::JSON.parse(GitHub::JSON.encode(payload))

        Hook::DeliverySystem.post_payload_to_hookshot(payload)
      rescue Hookshot::PayloadTooLarge => e
        log_params = {
            error: e,
            parent: payload["parent"],
            guid: payload["guid"],
        }

        log_payload_too_large_error(**log_params)
        Failbot.report e
      end

      def self.tags(payload)
        action = payload.dig("payload", "action")
        event_type = payload["event"]
        GitHub::TaggingHelper.create_hook_event_tags(event_type, action)
      end
    end
  end
end
