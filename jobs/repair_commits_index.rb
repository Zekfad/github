# frozen_string_literal: true

require "github/redis/mutex"

module GitHub
  module Jobs

    # The purpose of this job is to reconcile the state of Repository commits
    # between the file servers and the search index. We do this by
    # iterating over each Repository, reading the last index commit from the
    # search index, and then reconciling any differences with the file
    # servers.
    #
    # Each repair worker will process 100 repositories. When it has reconciles
    # all the commits it will enqueue another job. This process will
    # continue until all repositories have been reconciled.
    #
    # To make this whole process faster, multiple repair jobs can be enqueued.
    # The current offest into the repositories table is stored in redis.
    # Access to this value is coordinated via a shared mutex. Don't spin up
    # too many repair jobs otherwise you'll kill the database or the search
    # index or both.
    class RepairCommitsIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairCommitsIndexJob
      end

      areas_of_responsibility :search

      def self.queue
        :index_bulk
      end

      # Create a new repair job.
      #
      # name - The name of the index being repaired.
      # opts - Options Hash
      #        'cluster' - cluster name
      def initialize(name, opts = {})
        super(name, opts)

        @reconcilers = [CommitsReconciler.new(
          index: index,
          group_key: group_key,
          redis: redis,
        )]
      end

      class CommitsReconciler < ::Elastomer::Repair::Reconciler

        LIMIT = GitHub.enterprise? ? 1 : 100

        # Create a new CommitsReconciler that will reconcile commits between
        # the file servers and the search index.
        #
        # opts - Options Hash
        def initialize(opts)
          opts = opts.merge \
            type: "repository",
            limit: LIMIT,
            accept: :commits_are_searchable?

          super opts
        end

        # Internal: Perform the bulk indexing operations to bering the search
        # index in sync with the database records.
        #
        # upsert - Array of model IDs to update / add
        # remove - Array of model IDs to remove
        #
        # Returns the result of the bulk indexing operation.
        def update_search_index(upsert, remove, metadata)
          models_hash = models.index_by(&:id)

          upsert.each do |id|
            begin
              adapter = Elastomer::Adapters::Commit.create(models_hash[id])
              index.store(adapter)
            rescue TimeoutError => boom
              Failbot.report(boom, repo_id: id)
            end
          end

          unless remove.blank?
            begin
              query = {query: {terms: {repo_id: remove}}}

              index.docs.delete_by_query(query, type: %w[commit repository])
            rescue TimeoutError => boom
              Failbot.report boom
            end
          end
        end
      end
    end
  end
end
