# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class WikiReceive < CoreReceive
      def self.active_job_class
        ::WikiReceiveJob
      end

      attr_reader :wiki

      def self.queue
        :wiki_receive
      end

      def initialize(message)
        @message = message.symbolize_keys!
        @message[:repo].chomp!(".wiki")
        prepare_message
        @wiki   = @repository.unsullied_wiki

        @commit = nil
        @diffs  = nil
      end

      # initializes new WikiReceive instance for testing purposes.
      def self.from_repo(repo, sha)
        new(user: repo.user.to_s, repo: repo.to_s, after: sha)
      end

      def stale?
        super || @wiki.nil? || !@wiki.exist?
      end

      def self.perform_for(wiki, sha)
        message = wiki.payload_for_wikireceive sha
        new(message).perform
      end

      def perform
        return if deleted? || stale? || commit.nil? || !branch_or_tag? ||
          branch_name != "master" # Wikis always use "master"
        # TODO: GitRPC per call timeout 5.minutes
        perform_task :create_events
      end

      def create_events
        user = commit.author_email.present? && User.find_by_email(commit.author_email)
        return unless user

        user_id   = user.id
        updates   = []

        added_pages.each do |page|
          updates << wiki_update_for(page, :created)
        end

        modified_pages.each do |page|
          updates << wiki_update_for(page, :edited)
        end

        return unless updates.present?

        # TODO: Look into why we don't use the pusher as the actor???
        GitHub.instrument "wiki.push", actor_id: user_id, repo_id: @wiki.repository.id, updates: updates
      end

      def wiki_update_for(page, action)
        {action: action, page_name: page.name, sha: page.revision_oid.to_s}
      end

      def added_pages
        scan_diffs if !defined?(@added_pages)
        @added_pages
      end

      def modified_pages
        scan_diffs if !defined?(@modified_pages)
        @modified_pages
      end

      def commit
        @commit ||=
          begin
            @wiki.commits.find(@after)
          rescue GitRPC::InvalidObject
            nil
          end
      end

      def diffs
        @diffs ||= commit.diff
      end

      def scan_diffs
        @added_pages    = []
        @modified_pages = []
        diffs.each do |diff|
          next if diff.deleted?

          local = File.basename("#{diff.b_path || diff.a_path}")
          next if local.blank?
          name  = ::File.basename(local, ::File.extname(local))
          next if name.blank?

          page = @wiki.pages.find(name, commit.oid)
          next if page.nil?

          if diff.added?
            @added_pages << page
          else
            @modified_pages << page
          end
        end
      end
    end
  end
end
