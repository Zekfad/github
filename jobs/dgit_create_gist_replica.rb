# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    class DgitCreateGistReplica < Job
      def self.active_job_class
        ::SpokesCreateGistReplicaJob
      end

      areas_of_responsibility :dgit

      def self.perform(gist_id, host)
        Failbot.push app: "github-dgit",
                     spec: "gist/#{gist_id}"

        GitHub::Logger.log_context(job: name.demodulize, spec: "gist/#{gist_id}") do
          GitHub::Logger.log(method: "perform!", host: host) do
            GitHub.dogstats.time("dgit.actions.create-gist", tags: ["server:#{host}"]) do
              perform!(gist_id, host)
            end
          end
        end
      rescue Freno::Throttler::Error
        # Ignore freno errors and rely on retries from our own maintenance scheduler
      rescue GitHub::DGit::ReplicaCreateAlreadyPresentError
        # Ignore this case. This race does still occur in practice, but it's not
        # very interesting.
      end

      def self.perform!(gist_id, host, via: nil)
        Failbot.push(via: via) unless via.nil?

        gist = Gist.find_by_id(gist_id)
        raise GitHub::DGit::ReplicaCreateError, "There is no gist #{gist_id}" unless gist

        all_replicas = GitHub::DGit::Routing.all_gist_replicas(gist_id)
        Failbot.push delegate_replicas: all_replicas.inspect

        raise GitHub::DGit::ReplicaCreateError, "#{host} is not a DGit host" unless GitHub::DGit::get_hosts.include?(host)

        GitHub.stats.increment "dgit.#{host}.actions.create-gist" if GitHub.enterprise?
        GitHub.dogstats.increment "dgit", tags: ["action:create_gist", "host:#{host}"]

        # Insert the new replica in a CREATING state.
        begin
          ActiveRecord::Base.connected_to(role: :writing) do
            gist_db = GitHub::DGit::DB.for_gist_id(gist_id)
            sql = gist_db.SQL.new \
              gist_id: gist_id,
              host: host,
              checksum: "creating",
              state: GitHub::DGit::CREATING,
              read_weight: 0
            sql.add <<-SQL
              INSERT INTO gist_replicas (gist_id, host, checksum, state, read_weight, created_at, updated_at)
              VALUES (:gist_id, :host, :checksum, :state, :read_weight, NOW(), NOW())
            SQL
            gist_db.throttle { sql.run }
          end # connected_to
        rescue ActiveRecord::RecordNotUnique
          raise GitHub::DGit::ReplicaCreateAlreadyPresentError, "There is already a replica of gist #{gist_id} on #{host}"
        end

        # Repair it into existence.  Abracadabra.
        GitHub::Logger.log(method: "GitHub::Jobs::DgitRepairGistReplica.perform!") do
          GitHub::Jobs::DgitRepairGistReplica.perform!(gist_id, host, true, via: via)
        end

      rescue GitHub::DGit::ReplicaRepairError => e
        GitHub::DGit::Maintenance::set_gist_state(gist_id, host, GitHub::DGit::FAILED)
        raise e
      end

      def self.queue
        :dgit_repairs
      end
    end
  end
end
