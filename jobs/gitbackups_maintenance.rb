# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Timer that schedules maintenance runs for Git backups
    class GitbackupsMaintenance < Job
      def self.active_job_class
        ::GitbackupsMaintenanceJob
      end

      areas_of_responsibility :backups

      # Run every interval.
      def self.perform(spec)
        Failbot.push app: "gitbackups"

        GitHub::Backups.maintenance spec
      end

      # A single queue while we're on small scale, might expand to avoid
      # blocking at a later time.
      def self.queue
        :gitbackups_maintenance
      end
    end
  end
end
