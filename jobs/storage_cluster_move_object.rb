# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # StorageMoveObject moves an object from from_host to to_host.
    #
    # This job gets queued from StorageClusterMaintenanceSchedulerJob.perform_now
    class StorageClusterMoveObject < Job
      def self.active_job_class
        ::StorageClusterMoveObjectJob
      end

      areas_of_responsibility :data_infrastructure

      def self.queue
        :storage_cluster
      end

      def self.perform(oid, from_host, to_host)
        ok, body = GitHub::Storage::Client.move_to(from_host, [{oid: oid, fileservers: [to_host]}])
        if ok
          successes = body["objects"].find_all { |o| o["success"].size > 0 }
          successes.each do |obj|
            received = obj["success"].map { |h| URI.parse(h).host }
            GitHub::Storage::Creator.create_replicas_for_oid(oid, received)
            GitHub::Storage::Creator.delete_replica_from_host(from_host, oid)
          end
        end
      end
    end
  end
end
