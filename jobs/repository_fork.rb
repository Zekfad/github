# frozen_string_literal: true

require "fileutils"

module GitHub
  module Jobs
    class RepositoryFork < Job
      def self.active_job_class
        ::RepositoryForkJob
      end

      areas_of_responsibility :repositories, :git

      extend GitHub::HashLock

      # Perform the long running portion of a repository fork. This runs
      # Repository#clone_fork method on the repository created with
      # Repository#fork.
      #
      # owner_id - The user id of the person who owns this fork.
      # repo_id  - The repository id of the new repo.
      #
      # Returns nothing.
      def self.perform(owner_id, repo_id, parent_id = nil, template = nil, serve = nil)

        owner = User.find_by_id(owner_id)
        return if owner.nil?

        repo  = owner.repositories.find(repo_id)
        return if repo.nil?

        Failbot.push repo_id: repo.id,
                     spec: repo.dgit_spec,
                     user: owner.login,
                     user_id: owner.id

        GitHub::Logger.log(
          job: "RepositoryFork",
          at: "start",
          spec: repo.dgit_spec,
        )

        repo.clone_fork

        GitHub::Logger.log(
          job: "RepositoryFork",
          at: "finish",
          spec: repo.dgit_spec,
        )
        nil
      end

      # Fork jobs are coalesced such that only one job runs or is enqueued for a
      # given repository. See GitHub::HashLock#lock for more info on how key
      # scoping works.
      def self.lock(owner_id, repo_id, *args)
        repo_id.to_s
      end

      def self.queue
        :critical
      end
    end
  end
end
