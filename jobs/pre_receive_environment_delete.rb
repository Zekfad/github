# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::Jobs::PreReceiveEnvironmentDelete < GitHub::Jobs::Job
  def self.active_job_class
    ::PreReceiveEnvironmentDeleteJob
  end

  areas_of_responsibility :enterprise_only

  def self.perform(env_id, sha = nil)
    # hook_env = PreReceiveEnvironment.find(env_id)
    update_script = Rails.production? ?
        "/usr/local/share/enterprise/ghe-hook-env-cleanup" :
        "#{Rails.root}/script/ghe-hook-env-cleanup"
    if sha.nil?
      run([update_script, env_id, "-a"])
    else
      run([update_script, env_id, sha])
    end
  end

  def self.run(command)
    process = POSIX::Spawn::Child.new(*(command))

    if !process.status.success?
      Failbot.push(
          command: command.inspect,
          stdout: process.out,
          stderr: process.err,
          status: process.status.exitstatus.inspect,
      )
      raise DownloadException process.out
    else
      return process.out.chomp
    end
  end

  def self.queue
    :pre_receive_environment_delete
  end
end
