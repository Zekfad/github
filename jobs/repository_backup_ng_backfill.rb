# frozen_string_literal: true

module GitHub
  module Jobs
    class RepositoryBackupNgBackfill < RepositoryBackupNg
      def self.active_job_class
        ::RepositoryBackupNgBackfillJob
      end

      areas_of_responsibility :backups

      # We use the different queue in order not to block the backups from new
      # activity, but it's otherwise the same job.
      def self.queue
        :gitbackups_backfill
      end

    end
  end
end
