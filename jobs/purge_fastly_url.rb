# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    class PurgeFastlyUrl < Job
      def self.active_job_class
        ::PurgeFastlyUrlJob
      end

      areas_of_responsibility :data_infrastructure

      extend GitHub::HashLock

      def self.queue
        :cdn
      end

      def self.perform(options = {})
        options = options.with_indifferent_access
        return unless http = faraday
        res = http.run_request(:purge, options["url"], nil, nil)
        if res.status != 200
          retry_later(options)
        end
      rescue Faraday::Error => err
        retry_later(options)
        raise err
      end

      def self.retry_later(options)
        LegacyApplicationJob.retry_later(self.active_job_class, [options], retries: options["retries"].to_i)
      end

      def self.lock_timeout
        300
      end

      def self.faraday
        return @faraday if defined?(@faraday) && @faraday
        @faraday = Faraday.new { |f| f.adapter(Faraday.default_adapter) }
      end
    end
  end
end
