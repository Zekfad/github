# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ClearSoftBounces < Job
      def self.active_job_class
        ::ClearSoftBouncesJob
      end

      areas_of_responsibility :email

      schedule interval: 24.hours

      def self.queue
        :clear_soft_bounces
      end

      # Public: Don't run on Enterprise
      def self.enabled?
        !GitHub.enterprise?
      end

      # Public:
      #
      # Returns nothing.
      def self.perform
        return unless enabled?
        new.perform
      end

      def perform
        EmailRole.clear_soft_bounces
      end
    end
  end
end
