# frozen_string_literal: true

require "github/hash_lock"

module GitHub
  module Jobs
    class DgitMoveNetworkInDc < Job
      def self.active_job_class
        ::SpokesMoveNetworkInDcJob
      end

      areas_of_responsibility :dgit
      extend GitHub::HashLock

      def self.queue
        :dgit_repairs
      end

      def self.perform(network_id, dc, add_hosts, sub_hosts, why)
        Failbot.push app:        "github-dgit",
                     spec:       "network/#{network_id}",
                     datacenter: dc,
                     add_hosts:  add_hosts,
                     sub_hosts:  sub_hosts,
                     why:        why

        GitHub::Logger.log_context(job: name.demodulize, spec: "network/#{network_id}") do
          GitHub::Logger.log(method: "perform", dc: dc, add_hosts: add_hosts, sub_hosts: sub_hosts, why: why) do

            # Create the new replicas first
            add_hosts.each do |host|
              GitHub::Logger.log(method: "GitHub::Jobs::DgitCreateNetworkReplica.perform!", host: host) do
                GitHub::Jobs::DgitCreateNetworkReplica.perform!(network_id, host)
              end
            end

            # Remove redundant replicas
            sub_hosts.each do |host|
              GitHub::Logger.log(method: "GitHub::Jobs::DgitDestroyNetworkReplica.perform!", host: host) do
                GitHub::Jobs::DgitDestroyNetworkReplica.perform!(network_id, host, true)
              end
            end
          end
        end

        GitHub.dogstats.increment("dgit.actions.move-network-in-dc", tags: ["dc:#{dc}", "why:#{why}"])
      end
    end
  end
end
