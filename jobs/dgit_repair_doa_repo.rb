# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"
require "github/dgit/util"

module GitHub
  module Jobs
    # Complete the initialization of a repo that got stuck in `creating`
    # state, by calculating the checksums on all replicas and picking the
    # majority.  Absent a majority, we pick the replica that looks most
    # fully initialized.
    class DgitRepairDoaRepo < Job
      def self.active_job_class
        ::SpokesRepairDoaRepoJob
      end

      areas_of_responsibility :dgit

      def self.perform(repo_id, is_wiki = false)
        Failbot.push app: "github-dgit"

        repo = Repository.find_by_id(repo_id)
        raise GitHub::DGit::ReplicaRepairError, "Replica #{repo_id} not found" unless repo

        Failbot.push spec: repo.dgit_spec(wiki: is_wiki)

        GitHub::Logger.log_context(job: name.demodulize, spec: repo.dgit_spec(wiki: is_wiki)) do
          GitHub::Logger.log(method: "perform!") do
            GitHub.dogstats.time "dgit.actions.repair-doa-repo" do
              perform!(repo_id, is_wiki)
            end
          end
        end
      rescue Freno::Throttler::Error
      end

      def self.perform!(repo_id, is_wiki = false)
        repo = Repository.find_by_id(repo_id)
        raise GitHub::DGit::ReplicaRepairError, "Replica #{repo_id} not found" unless repo

        checksum = is_wiki ? GitHub::DGit::Routing.wiki_checksum(repo.network.id, repo_id)
                           : GitHub::DGit::Routing.repo_checksum(repo.network.id, repo_id)

        # If we have no checksum at all then we need to fully re-initialize the repository.
        unless checksum
          GitHub::Logger.log no_checksum: true

          repo.network.initialize_placeholder_network_replicas unless repo.fork?
          repo.sync_routes_from_network

          checksum = is_wiki ? GitHub::DGit::Routing.wiki_checksum(repo.network.id, repo_id)
                            : GitHub::DGit::Routing.repo_checksum(repo.network.id, repo_id)
        end

        raise GitHub::DGit::ReplicaRepairError, "No checksum found for #{is_wiki ? "wiki" : "repo" } #{repo_id}" unless checksum

        if checksum != "creating"
          GitHub::Logger.log invalid_replica_checksum: true,
              checksum: checksum,
              message: "skipping repair: unexpected non-creating checksum"
          return
        end

        replicas = GitHub::DGit::Routing.all_repo_replicas(repo_id, is_wiki).select(&:active?)

        rpc_map = {}
        replicas.each do |r|
          rpc = is_wiki ? r.to_route(repo.wiki_shard_path).build_maint_rpc
                        : r.to_route(repo.original_shard_path).build_maint_rpc
          rpc_map[r] = rpc
        end

        # log the action
        repo_type = is_wiki ? GitHub::DGit::RepoType::WIKI : GitHub::DGit::RepoType::REPO
        GitHub.stats.increment "dgit.nohost.actions.repair-doa-repo" if GitHub.enterprise?
        GitHub.dogstats.increment "dgit", tags: ["type:nohost", "action:repair_doa_repo"]

        # XXX - how should this behave w.r.t. voting vs non-voting replicas?
        #       in other words, should non-voting replicas be allowed to define the
        #       best checksum?

        no_replicas_exist = !replicas.map { |r| rpc_map[r].exist? }.any?
        GitHub::Logger.log num_replicas: replicas.size, no_replicas_exist: no_replicas_exist

        if no_replicas_exist
          if is_wiki
            repo.unsullied_wiki.create_git_repository_on_disk
          else
            repo.setup_git_repository
          end
        else
          replicas.each do |r|
            GitHub::DGit::Maintenance.backup_before_repair_with_rpc(rpc_map[r], self)
          end

          host_checksums = GitHub::DGit::Maintenance.recompute_checksums(repo, nil, is_wiki: is_wiki)
          best_checksum = GitHub::DGit::Util.get_majority(host_checksums) ||
                          host_checksums[get_most_complete(repo.dgit_spec(wiki: is_wiki), replicas, rpc_map)]
          GitHub::Logger.log host_checksums: host_checksums.inspect, best_checksum: best_checksum
          GitHub::DGit::Delegate.update_checksums(repo.network.id, repo.id, is_wiki, nil, best_checksum)
        end

        return true
      end

      def self.queue
        :dgit_repairs
      end

      # Returns the name of the host with the most complete repo.
      # XXX: Consider batching these calls into a single API?
      #  - replicas = an array of repo or wiki replicas
      #  - rpc_map      = map from replica to its RPC handle
      def self.get_most_complete(spec, replicas, rpc_map)
        scores = replicas.map do |replica|
          score = 0
          r = rpc_map[replica]
          begin
            if r.fs_exist?("HEAD")
              score += 1
              score += 1 if r.fs_read("HEAD").length > 0
            end
            if r.fs_exist?("config")
              score += 1
              score += 1 if r.config_get("core.dgit")
            end
            if r.fs_exist?("info/nwo")
              score += 1
              score += 1 if r.fs_read("info/nwo").length > 0
            end
            score += 1 if r.fs_exist?("audit_log")
          rescue GitRPC::Error
          end
          [replica.host, score]
        end.sort_by { |x| -x[1] }

        if scores[0][1] < 4
          raise GitHub::DGit::ReplicaRepairError, "Repo #{spec} has no good-enough replicas: #{scores}"
        end
        return scores[0][0]
      end
    end
  end
end
