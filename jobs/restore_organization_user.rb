# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RestoreOrganizationUser < Job
      def self.active_job_class
        ::RestoreOrganizationUserJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :restore_organization_user
      end

      # Public: Queue restore job.
      #
      # restorable_organization_user - The Restorable::OrganizationUser object.
      def self.enqueue(restorable_organization_user:, actor:)
        RestoreOrganizationUserJob.perform_later({"restorable_organization_user_id" => restorable_organization_user.id, "actor_id" => actor.id})
      end

      # Public: Perform job to restore organization user.
      #
      # options - Hash with "restorable_organization_user_id" and "status_id" keys.
      def self.perform_with_retry(options)
        restorable_organization_user_id = options.fetch("restorable_organization_user_id")
        restorable_organization_user = Restorable::OrganizationUser.find(restorable_organization_user_id)
        actor = User.find(options.fetch("actor_id"))

        status = JobStatus.find("restorable_#{restorable_organization_user.id}")
        status.track do
          restorable_organization_user.restore(actor: actor)
        end
      end
    end
  end
end
