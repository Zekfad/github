# frozen_string_literal: true

module GitHub
  module Jobs
    class SlottedCounterIncrement < Job
      def self.active_job_class
        ::SlottedCounterIncrementJob
      end

      areas_of_responsibility :downloads, :releases

      def self.queue
        :slotted_counters
      end

      def self.perform(record_type, record_id)
        SlottedCounterService.increment_type_and_id(record_type, record_id)
      end
    end
  end
end
