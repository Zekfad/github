# frozen_string_literal: true

module GitHub
  module Jobs
    class RepositoryDiskUsage < Job
      def self.active_job_class
        ::RepositoryDiskUsageJob
      end

      areas_of_responsibility :repositories

      def self.queue
        :repository_disk_usage
      end

      def self.perform(repository_id)
        repository = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(repository_id) }
        return unless repository
        Failbot.push(repo_id: repository.id)
        repository.update_disk_usage
      end
    end
  end
end
