# rubocop:disable Style/FrozenStringLiteralComment

require "failbot"

module GitHub
  module Jobs
    class SyncAssetUsage < Job
      MARKER_OFFSET = 1.minute

      def self.active_job_class
        ::SyncAssetUsageJob
      end

      areas_of_responsibility :git_protocols

      # Bubbles the erroring log line up to #parse_s3_logs_by_repository
      class LogLineError < StandardError
        attr_reader :orig_err, :line
        def initialize(err, line)
          @orig_err = err
          @line = line.gsub(/(\s+)((?:\d+\.){3}\d+|\[?(?:[a-f0-9]+:(?:[a-f0-9]*:)+[a-f0-9]*)\]?)(\s+)/, '\1IP-ADDRESS\3')
          super(err.to_s)
        end
      end

      extend GitHub::HashLock

      GIGABYTE = (1024 ** 3).to_f

      minutes = ENV["GITHUB_LFS_SYNC_INTERVAL"].to_i.abs
      schedule interval: (minutes.zero? ? 60 : minutes).minutes

      def self.queue
        :sync_asset_usage
      end

      def self.enabled?
        true
      end

      def self.lock_timeout
        600
      end

      def self.lock(options = {})
        name # only option is "retries"
      end

      # options - Hash
      #           "retries" - Integer number of times this job has been retried.
      def self.perform(options = {})
        options = options.with_indifferent_access
        # Retrieve the timestamped key of the most recent log this job has
        # parsed
        return unless marker = Asset::SyncStatus.get(:all)

        scanner = log_scanner
        rollback_time = scanner.marker_to_time(marker) - MARKER_OFFSET
        rollback_marker = scanner.time_to_marker(rollback_time)

        logset = stat_time("timing") {
           job = self.new

           # S3 logs aren't returned in a consistent order and in some cases
           # won't be returned at all, so if the job isn't backed up, parse
           # logs from a minute before the marker to catch any stragglers.
           if Time.now - rollback_time < 2.days.seconds
             logset = job.perform(rollback_marker, scanner)
             if logset.scan_error? || !logset.truncated?
               break logset
             end
           end

           # Then, if there are still logs remaining, parse from the marker
           # itself, ensuring that we always move forwards (i.e. in cases
           # where more than the maximum number of logs we retrieve were
           # generated in a 1 minute span)
           job.perform(marker, scanner)
        }

        # Save the timestamped key of the last log we parsed so we can use it
        # on the next run
        Asset::SyncStatus.set(:all, logset.last_log_key) unless logset.last_log_key.blank?

        if logset.truncated? || logset.scan_error?
          retry_job(options)
        end
      rescue StandardError => err
        retry_job(options)
        Failbot.report(err, retries: options["retries"].to_i)
        raise if Rails.test?
      end

      def self.retry_job(options)
        retries = options["retries"].to_i
        LegacyApplicationJob.retry_later(self.active_job_class, [options], retries: retries)
      end

      def self.log_scanner
        scanner = Asset::LogScanner.new(GitHub.s3_primary_client, "github-cloud-logs", "s3/")
        # Only tracking Git LFS files right now
        scanner.object_key_matcher = %r{\A/alambic/media}
        scanner.minimum_log_parts = 13
        scanner
      end

      def perform(marker, log_scanner)
        logset = log_scanner.find(marker)
        stat_gauge "queried_logs", logset.size
        return logset if logset.size.zero?

        diff_sec = Time.now - log_scanner.marker_to_time(marker)
        stat_gauge "marker_offset", (diff_sec/60).round(1)

        net_activities = parse_s3_logs_by_repository(logset)
        owner_activities = aggregate_by_owner(net_activities)
        save_owner_activities(owner_activities)

        return logset
      end

      # Downloads and parses the given log files.
      # Returns multi-dimensional hash to optimize DB insert queries. Each
      # actor id row is kept so we can reject seen request IDs for each owner
      # in #aggregate_by_owner.
      # {
      #   [network_id, repo_id]: {
      #     started_at: {
      #       actor_id: [
      #         {up: 123, down: 123, file: "s3/abc", id: "abc"},
      #         {up: 123, down: 123, file: "s3/abc", id: "abc"},
      #       ]
      #     }
      #   }
      # }
      def parse_s3_logs_by_repository(logset)
        activity = {}
        logset.each do |log_date, log, line|
          log_path = log.respond_to?(:path) ?
            log.path : "#{log.bucket_name}/#{log.key}"
          aggregate_by_repository(activity, line, log_date, log_path)
        end
        activity
      ensure
        stat_gauge "lines", logset.line_count
        stat_gauge "parsed_logs", logset.log_count
      end

      def aggregate_by_repository(activity, line, log_date, log_path)
        return unless item = begin
          parse_line(line, log_date)
        rescue StandardError => err
          raise LogLineError.new(err, line[:raw].to_s)
        end

        by_repo = activity[[item[:network_id], item[:repo_id]]] ||= {}
        by_time = by_repo[item[:hour_aligned_time]] ||= {}
        (by_time[[item[:actor_id], item[:key_id]]] ||= []) << {
          up: item[:bandwidth_up].to_f,
          down: item[:bandwidth_down].to_f,
          file: log_path,
        }
      end

      # Returns multi-dimensional hash to optimize DB insert queries.
      # {
      #   owner_id: {
      #     repo_id: {
      #       started_at: {
      #         actor_id: {up: 123, down: 123, files: [], ids: []}
      #       }
      #     }
      #   }
      # }
      def aggregate_by_owner(activity)
        owners_activity = {}
        activity.keys.in_groups_of(300) do |repo_pairs|
          net_ids = repo_pairs.map { |(net_id, _repo_id)| net_id }.compact
          nets = ActiveRecord::Base.connected_to(role: :reading) do
            RepositoryNetwork.
              where(id: net_ids).
              includes(root: :owner).
              all
          end.map { |n| [n.id, n] }.to_h

          repo_pairs.each do |pair|
            net_id, repo_id = *pair
            next unless net = nets[net_id]
            next unless owner = net.network_owner
            by_owner = owners_activity[owner.id] ||= {}
            by_owner_repo = by_owner[repo_id] ||= {}
            next unless by_repo = activity[pair]
            by_repo.each do |started_at, actors|
              seen_files = ActiveRecord::Base.connected_to(role: :reading) do
                Asset::Activity.seen_source_files(:lfs, owner.id, repo_id, started_at)
              end
              log_collision = false
              by_time = by_owner_repo[started_at] ||= {}
              actors.each do |actor_id, items|
                by_actor = by_time[actor_id] ||= {up: 0.0, down: 0.0, files: []}
                items.each do |item|
                  if seen_files.include?(item[:file])
                    if !log_collision
                      stat_incr "id_collision"
                      log_collision = true
                    end
                    next
                  end

                  by_actor[:up] += item[:up]
                  by_actor[:down] += item[:down]
                  by_actor[:files] << item[:file]
                end
              end
            end
          end
        end
        return owners_activity
      end

      def save_owner_activities(activities)
        ActiveRecord::Base.transaction do
          activities.each do |owner_id, repos|
            actor_ids = Set.new
            key_ids = Set.new

            repos.each do |repo_id, times|
              times.each do |started_at, actors|
                total = nil
                actors.each do |(actor_id, key_id), actor_items|
                  if total.nil?
                    total = actor_items
                  else
                    total[:up] += actor_items[:up]
                    total[:down] += actor_items[:down]
                    total[:files] += actor_items[:files]
                  end

                  Asset::ActorActivity.track(:lfs, owner_id, actor_id, key_id, repo_id, started_at,
                    up: actor_items[:up],
                    down: actor_items[:down])
                  actor_ids << actor_id
                  key_ids << key_id
                end

                total[:files].uniq!
                Asset::Activity.track(:lfs, owner_id, repo_id, started_at,
                  up: total[:up],
                  down: total[:down],
                  source_files: total[:files]
                )
              end
            end

            RebuildStorageUsageJob.perform_later(owner_id, {"notify" => true, "asset_type" => "lfs", "actor_ids" => actor_ids.to_a, "key_ids" => key_ids.to_a})
          end
        end
      end

      def parse_line(item, log_date = nil)
        return unless item
        query_params = item[:query]
        line = item[:raw]
        method = item[:method]
        parsed_info = item.dup

        parsed_info[:actor_id] = first_int_parameter(query_params["actor_id"])
        parsed_info[:key_id] = first_int_parameter(query_params["key_id"])
        parsed_info[:network_id] = key_to_network_id(line.key)
        parsed_info[:repo_id] = first_int_parameter(query_params["repo_id"])

        bytes_sent = line.bytes_sent.to_f
        object_size = line.object_size.to_f
        case method
        when "GET"
          parsed_info[:bandwidth_down] = bytes_sent / GIGABYTE
        when "PUT"
          parsed_info[:bandwidth_up] = object_size / GIGABYTE
        end
        parsed_info
      end

      def first_int_parameter(val)
        # If there is more than one query param, we want the first one
        case id = val
        when Array # If we get more than one query param, we want the first
          id[0].to_i
        when String # If we only get back 1 query param,
          id.to_i
        when nil
          0
        end
      end

      def key_to_network_id(key)
        key.split("/").reject(&:empty?)[2].to_i
      end

      def self.stat_time(suffix, &block)
        GitHub.dogstats.time("s3_usage."+suffix, options, &block)
      end

      def self.options
        { tags: ["type:legacy_lfs"] }
      end

      def stat_gauge(suffix, value)
        GitHub.dogstats.gauge("s3_usage."+suffix, value, self.class.options)
      end

      def stat_incr(suffix)
        GitHub.dogstats.increment("s3_usage."+suffix, self.class.options)
      end
    end
  end
end
