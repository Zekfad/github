# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ChatterboxNotification < Job
      def self.active_job_class
        ::ChatterboxNotificationJob
      end

      areas_of_responsibility :background_jobs

      # Set the queue this job should run in.
      def self.queue
        :chatterbox_notification
      end

      def self.perform(topic, message)
        # NOTE: This can raise an exception (unlike say!), but because we are
        #       in a background job I'm going to keep it this way, so we can be
        #       notified of failures to deliver.
        GitHub::Chatterbox.client.say topic, message
      end
    end
  end
end
