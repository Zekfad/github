# frozen_string_literal: true

module GitHub
  module Jobs
    class UserSuspend < Job
      def self.active_job_class
        ::UserSuspendJob
      end

      areas_of_responsibility :user_growth

      def self.queue
        :user_suspend
      end

      def self.perform(user_id, reason)
        return unless user = User.find_by_id(user_id)
        user.suspend(reason)
      end
    end
  end
end
