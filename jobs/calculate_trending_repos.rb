# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class CalculateTrendingRepos < Job
      def self.active_job_class
        ::CalculateTrendingReposJob
      end

      areas_of_responsibility :explore

      def self.queue
        :calculate_trending_repos
      end

      def self.perform(period)
        GitHub.dogstats.time("calculate_trending_repos", tags: ["action:calculate", "type:#{period}"]) do
          self.calculate(period)
        end
      end

      def self.clear_cache(period)
        GitHub.cache.delete("trending:repos:query:#{period}")
      end

      def self.calculate(period)
        clear_cache(period)

        begin
          trending_ids = Trending.repo_ids({
            period: period,
            from_job: true,
          })

          repositories = Repository.where(id: trending_ids.map(&:first)).index_by(&:id)

          repos = trending_ids.map do |repo_id, val|
            [repositories[repo_id], val]
          end
        rescue => error
          Failbot.report(error)
          raise
        end
      end
    end
  end
end
