# frozen_string_literal: true

module GitHub
  module Jobs
    # Handle any tasks that can be done out of the scope of a request
    # after a User signs up.
    class UserSignup < Job
      def self.active_job_class
        ::UserSignupJob
      end

      areas_of_responsibility :user_growth

      def self.queue
        :user_signup
      end

      # Perform tasks that are necessary after user creation.
      #
      # user_id          - The id of the user that was created.
      #
      # Returns nothing.
      def self.perform(user_id)
        new(user_id).perform
      end

      def initialize(user_id)
        @user_id = user_id
      end

      def perform
        Failbot.push(user_id: user_id)
        user = User.find_by_id(user_id)
        return unless user
        Failbot.push(user: user.login)

        user.send_email_verification
        email_verification_in_millseconds = (Time.now - user.created_at) * 1_000
        GitHub.dogstats.timing("user.signup.email.verification.time", email_verification_in_millseconds)

        user.report_creation_to_octolytics
        maybe_auto_subscribe_to_explore_email(user)
      end

      private

      attr_reader :user_id

      def maybe_auto_subscribe_to_explore_email(user)
        variant = GitHub::UserResearch.experiment_variant(
          experiment: UserExperimentCohort::AUTO_SUBSCRIBE_NEW_USERS_TO_EXPLORE_EMAIL,
          subject: user,
        )

        if variant == :auto_subscribe
          NewsletterSubscription.subscribe(user, "explore", "weekly", auto_subscribed: true)
        end
      end
    end
  end
end
