# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class MigrationEnqueueDestroyFileJobs < Job
      def self.active_job_class
        ::MigrationEnqueueDestroyFileJobsJob
      end

      areas_of_responsibility :migration

      schedule interval: 1.hour

      def self.queue
        :migration_enqueue_destroy_file_jobs
      end

      # Whether this job should be scheduled for the current environment. Be sure
      # to consider whether the job should be run under GitHub Enterprise.
      def self.enabled?
        !GitHub.enterprise?
      end

      DAYS_TO_KEEP_FILES = 7

      # Queue jobs for migration files that should be removed.
      def self.perform
        MigrationFile.older_than(DAYS_TO_KEEP_FILES.days.ago).find_each do |file|
          MigrationDestroyFileJob.perform_later("migration_id" => file.migration_id)
        end
      end
    end
  end
end
