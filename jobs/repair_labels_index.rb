# frozen_string_literal: true

module GitHub
  module Jobs
    # The purpose of this job is to reconcile the models with the index.
    #
    # To make this whole process faster, multiple repair jobs can be enqueued. The current offset
    # into the labels table is stored in redis. Access to this value is coordinated via a shared
    # mutex. Don't spin up too many repair jobs otherwise you'll kill the database or the search
    # index or both.
    class RepairLabelsIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairLabelsIndexJob
      end

      areas_of_responsibility :search

      reconcile "label",
        fields: %w[updated_at],
        limit: 2_000

      # Returns the queue where the repair job will run.
      def self.queue
        :index_bulk
      end
    end
  end
end
