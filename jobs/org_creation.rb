# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class OrgCreation < Job
      def self.active_job_class
        ::OrgCreationJob
      end

      areas_of_responsibility :analytics

      def self.queue
        :analytics
      end

      def self.perform(creator_id, org_id)
        return unless GitHub.octolytics_enabled?
        return unless creator = User.find_by_id(creator_id)
        return unless org = Organization.find_by_id(org_id)

        new(creator, org).perform
      end

      def initialize(creator, org)
        @creator = creator
        @org     = org
      end

      def perform
        GitHub.analytics.record([{
          event_type: "org_create",
          timestamp: org.created_at.to_i,
          dimensions: {
            id: org.id,
            name: org.name,
            creator_id: creator.id,
            creator_login: creator.login,
          }.merge(location),
          context: {
            plan: plan,
          },
        }])

        org.admins.where.not(id: creator.id).each do |admin|
          OrganizationMailer.admin_added(admin, org, creator).deliver_now
        end
      end

      private

      attr_reader :creator, :org

      def plan
        org.plan.try(:name)
      end

      def location
        GitHub::Location.look_up(creator.last_ip) || {}
      end
    end
  end
end
