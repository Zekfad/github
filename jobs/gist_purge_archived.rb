# frozen_string_literal: true

module GitHub
  module Jobs
    # Scheduled job that purges archived gists after they've been
    # archived for some time. See expiration_period and records_per_interval
    # for concrete
    class GistPurgeArchived < Job
      def self.active_job_class
        ::GistPurgeArchivedJob
      end

      areas_of_responsibility :gist

      schedule interval: 1.hour

      BATCH_SIZE = 10000

      # Restrict ourselves to dotcom. The semantics for this on-prem are more
      # complex.
      def self.enabled?
        !GitHub.enterprise?
      end

      # How long repositories may remain in the archived state before being
      # considered stale and eligible for purge.
      def self.expiration_period
        3.months
      end

      # Maximum number of records to purge on each interval.
      def self.records_per_interval
        BATCH_SIZE
      end

      # Purge stale archived repositories.
      def self.perform
        SlowQueryLogger.disabled do
          expire_time = Time.now - expiration_period
          Archived::Gist.purge_stale_records(records_per_interval, expire_time)
        end
      end

      def self.queue
        :archive_restore
      end

      # Only one of these jobs should run at any given time.
      extend GitHub::HashLock
    end
  end
end
