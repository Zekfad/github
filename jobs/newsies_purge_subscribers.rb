# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class NewsiesPurgeSubscribers < Job
      def self.active_job_class
        ::NewsiesPurgeSubscribersJob
      end

      areas_of_responsibility :notifications

      READ_BATCH_SIZE = 1000

      def self.queue
        :notifications
      end

      def self.perform(repository_id, options = {})
        options = options.with_indifferent_access
        return unless repository = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(repository_id) }

        response = Newsies::Response.new do
          list = Newsies::List.new("Repository", repository_id)
          user_ids = Set.new
          ActiveRecord::Base.connected_to(role: :reading) do
            user_ids.merge(Newsies::ListSubscription.for_list(list).distinct.pluck(:user_id))
            user_ids.merge(Newsies::ThreadTypeSubscription.for_list(list).distinct.pluck(:user_id))
            user_ids.merge(Newsies::ThreadSubscription.for_list(list).distinct.pluck(:user_id))
          end

          user_ids_without_access = []
          user_ids.each_slice(READ_BATCH_SIZE) do |batched_user_ids|
            ActiveRecord::Base.connected_to(role: :reading) do
              User.where(id: batched_user_ids).each do |user|
                user_ids_without_access << user.id unless repository.readable_by?(user)
              end
            end
          end

          GitHub.newsies.async_delete_all_for_list_and_users(list, user_ids_without_access)
        end

        if response.failed?
          retries = options["retries"].to_i
          LegacyApplicationJob.retry_later(self.active_job_class, [repository_id, options], retries: retries)
        end
      end
    end
  end
end
