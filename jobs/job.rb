# rubocop:disable Style/FrozenStringLiteralComment

require "github/areas_of_responsibility"
require "github/jobs/retry_errors"
require "github/throttler"

module GitHub
  module Jobs
    class Job
      include GitHub::AreasOfResponsibility

      # Any job class may use `retry_on_error` to add automatic retry on known
      # errors as long as it defines `self.perform_with_retry` instead of
      # `self.perform`.
      extend GitHub::Jobs::RetryErrors

      # Automatically retry when throttler errors are encountered.
      # This will only apply to jobs defining `self.perform_with_retry`
      # instead of `self.perform`.
      retry_on_error Freno::Throttler::Error

      # Valid TimerDaemon scopes. See .schedule
      VALID_SCOPES = %i(global host process).freeze

      # Public: Returns a hash for jobs running at intervals, nil otherwise.
      # See .schedule for keys.
      def self.schedule_options
        @schedule_options
      end

      def self.wrapped?
        !!active_job_class
      end

      def self.active_job_class
      end

      # Public: Schedules this job to be run at `interval` seconds.
      #
      # Options:
      #   scope: Defines where the job is scheduled. Defaults to :global
      #     :global  - scheduled once globally
      #     :host    - scheduled per host. e.g. unicorn monitoring
      #     :process - scheduled per host, per process
      #
      # See docs/timers.md for details.
      def self.schedule(interval:, scope: :global)
        raise ArgumentError.new "scope must be one of #{VALID_SCOPES}" unless VALID_SCOPES.include?(scope)
        raise ArgumentError.new "interval must be greater than 0" unless interval > 0

        @schedule_options = {
          interval: interval,
          scope: scope,
        }
      end
    end
  end
end
