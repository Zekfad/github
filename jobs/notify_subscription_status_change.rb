# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class NotifySubscriptionStatusChange < Job
      def self.active_job_class
        ::NotifySubscriptionStatusChangeJob
      end

      areas_of_responsibility :notifications

      def self.queue
        :notify_subscription_status_change
      end

      def self.perform(user_id, list_id, thread_id, data, list_type = "Repository")
        user = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }
        return unless user
        list = ActiveRecord::Base.connected_to(role: :reading) { list_type.constantize.find_by_id(list_id) }
        return unless list
        channel = GitHub::WebSocket::Channels.thread_subscription(user, list, thread_id)
        GitHub::WebSocket.notify_user_channel(user, channel, data)
      end

      def self.enqueue(record, user, list, thread, data = {})
        data = {timestamp: Time.now.to_i}.merge(data)
        NotifySubscriptionStatusChangeJob.perform_later(user.id, list.id, thread.id, data, list.class.name)
      end
    end
  end
end
