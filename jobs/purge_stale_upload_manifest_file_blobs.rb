# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class PurgeStaleUploadManifestFileBlobs < Job
      def self.active_job_class
        ::PurgeStaleUploadManifestFileBlobsJob
      end

      areas_of_responsibility :code_collab

      extend GitHub::HashLock

      schedule interval: 1.hour

      class << self
        attr_accessor :purge_batch_size
        attr_accessor :ttl
      end
      self.purge_batch_size = 100
      self.ttl = 24.hours

      def self.queue
        :purge_stale_upload_manifest_file_blobs
      end

      def self.enabled?
        GitHub.storage_cluster_enabled?
      end

      def self.expire_batch
        UploadManifest.joins(:files).
          where("commit_oid is null AND upload_manifests.updated_at < ? AND storage_blob_id is not null", ttl.ago).
          limit(purge_batch_size).distinct
      end

      def self.perform
        while expire_batch.count != 0
          expire_batch.each do |manifest|
            manifest.cleanup
          end
        end
      end
    end
  end
end
