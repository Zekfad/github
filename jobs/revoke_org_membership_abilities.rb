# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RevokeOrgMembershipAbilities < Job
      def self.active_job_class
        ::RevokeOrgMembershipAbilitiesJob
      end

      areas_of_responsibility :orgs

      extend GitHub::HashLock

      DEPENDENT_JOBS = [
        -> (org, user) { RemoveOrgMemberForksJob.enqueue(org, user) },
        -> (org, user) { RemoveOrgMemberWatchedRepositoriesJob.enqueue(org, user) },
        -> (org, user) { RemoveOrgMemberRepositoryStarsJob.enqueue(org, user) },
        -> (org, user) { RemoveOrgMemberIssueAssignmentsJob.enqueue(org, user) },
        -> (org, user) { RemoveOrgMemberPackageAccessJob.perform_later(org, user) },
        -> (org, _) {
          IntegrationInstallation.where(target_id: org.id).each do |installation|
            UpdateIntegrationInstallationRateLimitJob.enqueue(installation.id)
          end
        },
        -> (org, user) {
          if org.business && !GitHub.single_business_environment?
            BusinessMembershipCleanupJob.perform_later(org.business, user_ids: [user.id])
          end
        },
        -> (org, user) {
            TradeControls::OrganizationComplianceCheckJob.perform_later(org.id, reason: :organization_member)
            TradeControls::OrganizationComplianceCheckJob.perform_later(org.id, reason: :organization_admin)
        },
      ].freeze

      def self.queue
        :revoke_org_membership_abilties
      end

      def self.enqueue(organization, user, queue_delete_jobs = true)
        RevokeOrgMembershipAbilitiesJob.perform_later(organization.id, user.id, queue_delete_jobs)
      end

      # Only a single unique instance of the job can be concurrently running
      # per organization and member.
      def self.lock(org_id, member_id, queue_delete_jobs = true, options = {})
        "#{name}::#{org_id}::#{member_id}::#{queue_delete_jobs}"
      end

      def self.perform_with_retry(organization_id, user_id, queue_delete_jobs = true, opts = {})
        org = Organization.find_by_id(organization_id)
        user = User.find_by_id(user_id)
        return unless org && user

        org.teams_for(user).each do |team|
          team.remove_member(user, force: true, send_notification: false, queue_delete_jobs: queue_delete_jobs)
        end

        Ability.throttle do
          org.remove_member_without_callbacks_and_notifications(user, background: false)
        end

        # Dependent jobs: Must be run after the user's abilities have been
        # revoked.
        DEPENDENT_JOBS.each { |job| job.call(org, user) }
      end
    end
  end
end
