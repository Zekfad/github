# frozen_string_literal: true

module GitHub
  module Jobs
    class DgitFixZeroReplicaGist < Job
      def self.active_job_class
        ::SpokesFixZeroReplicaGistJob
      end

      areas_of_responsibility :dgit

      def self.queue
        :dgit_repairs
      end

      def self.perform(gist_id)
        GitHub::DGit::DB.for_gist_id(gist_id).throttle do
          GitHub.dogstats.time("dgit.actions.fix-zero-replica-gist") do
            do_fix(gist_id)
          end
        end
      end

      def self.do_fix(gist_id)
        gist = Gist.find_by_id(gist_id)
        return unless gist
        Failbot.push spec: gist.dgit_spec,
                     app: "github-dgit"

        replicas = GitHub::DGit::Routing.all_gist_replicas(gist_id)
        return unless replicas.empty?

        fileservers = GitHub::DGit.get_fileservers
        routes = fileservers.map { |fs| fs.to_route(gist.original_shard_path) }
        answers, errors = ::GitRPC.send_multiple(routes.map(&:rpc_url), :exist?, [])
        found = routes.select { |route| answers[route.rpc_url] }
        unless found.empty?
          fileservers = found.map(&:fileserver)
          GitHub::DGit::Maintenance.insert_placeholder_gist_replicas_and_checksums(gist_id, fileservers)
          GitHub::DGit::Maintenance.recompute_gist_checksums(gist, :vote)
        else
          Failbot.report GitHub::DGit::ZeroReplicaGists.new("Could not find any replicas for gist"),
            gist_id: gist_id,
            zero_voting_replicas_count: 1,
            zero_nonvoting_replicas_count: 1,
            zero_voting_replica_ids: [gist_id],
            zero_nonvoting_replica_ids: [gist_id]
        end
      end
    end
  end
end
