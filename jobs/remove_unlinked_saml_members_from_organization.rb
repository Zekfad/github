# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RemoveUnlinkedSamlMembersFromOrganization < Job
      def self.active_job_class
        ::RemoveUnlinkedSamlMembersFromOrganizationJob
      end

      def self.queue
        :remove_unlinked_saml_members_from_organization
      end

      # TODO: only enqueue one job per organization at a time
      def self.enqueue(organization, actor)
        RemoveUnlinkedSamlMembersFromOrganizationJob.perform_later(organization.id, actor.id)
      end

      def self.perform(organization_id, actor_id)
        return unless organization = Organization.find_by_id(organization_id)
        return unless actor = User.find_by_id(actor_id)
        return unless organization.adminable_by?(actor)

        organization.unlinked_saml_members.find_each do |member|
          organization.remove_member(member, reason: :saml_external_identity_missing)
          Organization::RemovedMemberNotification.new(organization, member).add_saml_external_identity_missing
        end
      end
    end
  end
end
