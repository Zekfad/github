# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    class DgitRecomputeChecksums < Job
      def self.active_job_class
        ::SpokesRecomputeChecksumsJob
      end

      areas_of_responsibility :dgit

      extend GitHub::HashLock

      def self.perform(repo_id, from_host, is_wiki)
        Failbot.push app: "github-dgit"

        repo = find_repo(repo_id)
        Failbot.push spec: repo.dgit_spec(wiki: is_wiki)

        GitHub::Logger.log_context(job: name.demodulize, spec: repo.dgit_spec(wiki: is_wiki)) do
          GitHub::Logger.log(method: "perform!", from_host: from_host) do
            perform!(repo_id, from_host, is_wiki)
          end
        end
      rescue Freno::Throttler::Error
      end

      def self.perform!(repo_id, from_host, is_wiki)
        repo = find_repo(repo_id)

        from_host = :vote if from_host == "vote"

        GitHub::DGit::Maintenance.recompute_checksums(repo, from_host, is_wiki: is_wiki)
      end

      def self.queue
        :dgit_repairs
      end

      def self.find_repo(repo_id)
        repo = Repository.find_by_id(repo_id)
        repo = Archived::Repository.find_by_id(repo_id) unless repo
        raise GitHub::DGit::ChecksumInitError, "Repo ID #{repo_id} not found" unless repo
        return repo
      end
    end
  end
end
