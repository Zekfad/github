# frozen_string_literal: true

module GitHub
  module Jobs
    class RepairEnterprisesIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairEnterprisesIndexJob
      end

      reconcile "enterprise", {
        model_class: Business,
        fields: ["updated_at"],
        limit: 250,
      }

      def self.queue
        :index_bulk
      end
    end
  end
end
