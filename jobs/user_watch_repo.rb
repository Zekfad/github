# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class UserWatchRepo < Job
      def self.active_job_class
        ::UserWatchRepoJob
      end

      areas_of_responsibility :notifications

      def self.queue
        :notifications
      end

      def self.perform(user_id, repo_id, enqueue = false, retryable = false, options = {})
        options = options.with_indifferent_access

        return unless user = User.find_by_id(user_id)
        return unless repo = Repository.find_by_id(repo_id)

        if enqueue
          # We use Domain::Notifications for throttling because user.watch_repo
          # writes using the ListSubscription and ThreadTypeSubscription
          # models which both belong to the Notifications domain.
          ApplicationRecord::Domain::Notifications.throttle { user.watch_repo(repo, enqueue: false, retryable: true) }
        elsif retryable
          retries = options["retries"].to_i
          LegacyApplicationJob.retry_later(self.active_job_class, [user_id, repo_id, false, false, options], retries: retries)
        end

        # We use Domain::Notifications for throttling because user.watch_repo
        # writes using the ListSubscription and ThreadTypeSubscription
        # models which both belong to the Notifications domain.
        ApplicationRecord::Domain::Notifications.throttle { user.watch_repo(repo, enqueue: false, retryable: false) }
      rescue Freno::Throttler::Error
        LegacyApplicationJob.retry_later(self.active_job_class, [user_id, repo_id, false, false, options], {
          retries: options["retries"].to_i,
        })
      end
    end
  end
end
