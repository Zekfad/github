# frozen_string_literal: true

module GitHub
  module Jobs
    class PagesPropagateHttpsRedirect < Job
      def self.active_job_class
        ::PagesPropagateHttpsRedirectJob
      end

      areas_of_responsibility :pages

      def self.queue
        Page.page_queue
      end

      def self.perform(user_id)
        if user = User.find_by_id(user_id)
          user.propagate_https_redirect
        end
      end

    end
  end
end
