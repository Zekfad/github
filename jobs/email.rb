# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class Email < Job
      def self.active_job_class
        ::EmailJob
      end

      # Any EmailError raised with one of the following reason codes
      # will trigger an automated response to the User informing them
      # that GitHub failed to process their email.
      NOTIFIABLE_ERRORS = [:blank_body, :locked_issue, :must_verify_email, :disabled].freeze

      # Represents an error in processing inbound email.
      class EmailError < StandardError
      end

      attr_reader :mail, :sender, :target, :creation, :error, :from_address
      # The result code of the email process - this defaults to "success", but will
      # be changed to an error_code if an EmailError is raised
      attr_accessor :result

      def self.queue
        :email
      end

      def initialize(mail = {})
        @mail = mail.deep_stringify_keys
        @mail["headers"] ||= {}
        @result = "success"
      end

      # Helper for setting up a new instance of the job with the correct
      # mail hash from Astrotrain.  If you want to reply to a comment
      # as bob:
      #
      #   EmailJob.for(@comment, @bob, :body => 'my reply')
      #
      # This is mainly used in tests or the console.  In production, this job
      # is spawned from the Metroplex worker from a live email.
      def self.for(target, author, options = {})
        new(mail_hash_for(target, author, options))
      end

      def self.mail_hash_for(target, author, options = {})
        headers = {"message-id" => "<123>"}
        if custom_headers = options.delete(:headers)
          headers.update(custom_headers)
        end

        {
          "reply_code" => GitHub::Email::Token.target_token(target, author),
          "from"       => [{"address" => author.email}],
          "headers"    => headers,
          "body"       => options.delete(:body).to_s,
          "html"       => options.delete(:html).to_s,
        }.update(options.stringify_keys)
      end

      # Public: handle the case where we can't process
      # an inbound email for some reason.
      #
      # Notifies the User of the processing error
      # when appropriate.
      #
      # Raises EmailJob::EmailError
      # Returns nil
      def email_error!(error_code)
        self.result = error_code
        GitHub.dogstats.increment "email_reply", tags: ["error:#{error_code}"]

        reply_with_processing_failure(error_code)

        raise EmailError, error_code
      end

      def load_sender_and_target
        code = @mail["reply_code"] || email_error!(:no_reply_code)
        @mail["from"].present? || email_error!(:no_from_address)
        @from_address = @mail["from"].first["address"]

        begin
          # Determine sender from reply code or email address.
          @sender = GitHub::Email::Token.sender_from_token(code) || email_error!(:no_user_for_sender)
          @target = GitHub::Email::Token.target_from_token(code, @sender) || email_error!(:no_target)
        rescue GitHub::Email::Token::LegacyTokenError
          AccountMailer.legacy_reply_token_bounce(self).deliver_now
          email_error!(:legacy_token)
        end

        check_for_ignoring(@sender, @target)
      end

      def check_for_ignoring(sender, target)
        target_user_ids = target_author_ids(target)

        ActiveRecord::Base.connected_to(role: :reading) do
          if sender.blocked_by?(*target_user_ids)
            email_error!(:ignored_by_targets)
          elsif sender.blocking?(*target_user_ids)
            email_error!(:ignoring_targets)
          end
        end
      end

      def target_author_ids(target)
        user_ids = []

        if target.respond_to?(:repository)
          user_ids << target.repository.try(:owner_id)
        end

        if target.respond_to?(:user_id)
          user_ids << target.user_id
        end

        user_ids.compact!
        user_ids.uniq!

        user_ids
      end

      def log_result_in_syslog
        message = {}
        if @error
          message[:error] = @error.to_s
        else
          message.update \
            saved_at: Time.now.utc.iso8601,
            url: url_of(@creation)
        end
        GitHub::Logger.log("message-id" => @mail["headers"]["message-id"],
                           "from" => @from_address,
                           "message" => message,
                           "result" => result.to_s)
      end

      AUTOREPLY_HEADERS = %w(precedence x-auto-response-suppress)
      # Internal: Return the first Auto-reply header for the email if found
      # Otherwise returns nil
      def autoreply_header
        self.class.autoreply_header(@mail["headers"])
      end
      alias_method :autoreply_value, :autoreply_header

      def self.autoreply_header(headers)
        AUTOREPLY_HEADERS.detect do |an_autoreply_header|
          headers[an_autoreply_header].present?
        end
      end

      def url_of(item)
        case item
          when CommitComment
            "%s/%s/commit/%s#commitcomment-%d" % [
              GitHub.url,
              item.repository.name_with_owner,
              item.commit.oid, item.id]
          when IssueComment
            "%s#issuecomment-%d" % [
              item.issue.permalink, item.id]
          when PullRequestReviewComment
            "%s#issuecomment-%d" % [
              item.pull_request.permalink, item.id]
        end
      end

      # Public: Returns the Message-Id header of this email.
      #
      # Returns a String or nil.
      def message_id
        @mail["headers"]["message-id"]
      end

      # Public: Returns the Subject header of this email.
      def subject
        @mail["subject"]
      end

      # Internal: Reply to the user's email thread to let them
      # know why we could not create their email comment on GitHub.
      def reply_with_processing_failure(error_code)
        return unless NOTIFIABLE_ERRORS.include?(error_code)
        return unless sender && target
        return unless GitHub.flipper[:email_reply_notices].enabled?(sender)

        AccountMailer.email_reply_unsuccessful(self, error_code).deliver_now
        GitHub.dogstats.increment "email_reply", tags: ["action:error_replied_to_user"]
      end
    end
  end
end
