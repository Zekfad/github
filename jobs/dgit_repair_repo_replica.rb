# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    # Using a combination of `git fetch` and `rsync`, bring a given
    # repository replica back into compliance with other replicas for that
    # repository.
    class DgitRepairRepoReplica < Job
      def self.active_job_class
        ::SpokesRepairRepoReplicaJob
      end

      areas_of_responsibility :dgit

      # Don't sync locks or git state with rsync.  Git state syncs better with git.
      # Implicitly also excluded here is /objects/info/alternates, which
      # is intentionally allowed to differ until the next network
      # maintenance.
      EXCLUDE_RSYNC = [
        "/objects",
        "/refs",
        "/packed-refs",
        "/packed-refs.new",
        "/dgit-state*",
        "*.lock",
        "/.backup_lock",
        "/ghlog*",
        "/gitmon.model",
        "/info/last-sync",
        "/fsck",
        "/audit_log",
      ]

      # Try this many times -- repairing a repository can fail if it
      # changes while the repair is taking place.  If after several
      # attempts, the replica still doesn't match the repository, mark the
      # whole network's state as FAILED.
      REPAIR_ATTEMPTS = 3

      def self.lock_contents
        "#{Socket.gethostname.split(".").first}##{Process.pid}"
      end

      def self.lock_name
        "dgit-repo-repair.lock"
      end

      def self.key_name(id)
        "dgit:locked-repair:#{id}"
      end

      def self.perform(repo_id, network_id, host, is_wiki = false)
        Failbot.push app: "github-dgit"

        repo = Repository.find_by_id(repo_id)
        raise GitHub::DGit::ReplicaRepairError, "Repo ID #{repo_id} not found" unless repo
        raise GitHub::DGit::ReplicaRepairError, "Network ID #{network_id} is incorrect (expected #{repo.network.id})" unless repo.network.id == network_id

        Failbot.push spec: repo.dgit_spec(wiki: is_wiki)

        GitHub::Logger.log_context(job: name.demodulize, spec: repo.dgit_spec(wiki: is_wiki)) do
          GitHub::Logger.log(method: "perform!", host: host) do
            perform!(repo_id, network_id, host, is_wiki)
          end
        end
      rescue Freno::Throttler::Error
      end

      def self.perform!(repo_id, network_id, host, is_wiki = false)
        repo_type = is_wiki ? GitHub::DGit::RepoType::WIKI : GitHub::DGit::RepoType::REPO
        replica = is_wiki ? GitHub::DGit::Routing.wiki_replica_for_host(repo_id, host)
                          : GitHub::DGit::Routing.repo_replica_for_host(repo_id, host)
        raise GitHub::DGit::ReplicaRepairError, "Replica #{repo_id} on #{host} not found" unless replica

        repo = Repository.find_by_id(repo_id)
        raise GitHub::DGit::ReplicaRepairError, "Repo ID #{repo_id} not found" unless repo
        raise GitHub::DGit::ReplicaRepairError, "Network ID #{network_id} is incorrect (expected #{repo.network.id})" unless repo.network.id == network_id

        if replica.checksum == replica.expected_checksum
          GitHub::Logger.log no_repair_needed: true,
              checksum: replica.expected_checksum,
              message: "skipping repair: replica ok"
          return true
        end

        rpc = is_wiki ? replica.to_route(repo.wiki_shard_path).build_maint_rpc
                      : replica.to_route(repo.original_shard_path).build_maint_rpc

        # Make sure the destination is set up to look like a Spokes
        # repository, or the initial checksum computation will fail.
        force_dgit_init = begin
          rpc.config_get("core.dgit") != "true"
        rescue ::GitRPC::InvalidRepository
          false
        end

        # Try to resolve the problem by voting for the checksum. This
        # fixes cases where there is unanimous agreement on disk and the
        # only mismatch is in the database.
        unless force_dgit_init
          begin
            host_checksums = GitHub::DGit::Maintenance.recompute_checksums(
                               repo, :unanimous, is_wiki: is_wiki)

            if host_checksums[:repo] == host_checksums[host]
              GitHub::Logger.log message: "Unanimous on disk checksums.", no_repair_needed: true
              return true
            end
          rescue GitHub::DGit::ThreepcError, GitHub::DGit::ThreepcFailedToLock, GitHub::DGit::ChecksumInitError, ::GitRPC::Error => e
            GitHub::Logger.log_exception({method: "recompute_checksums", unanimous: true}, e)
            GitHub::Logger.log message: "Unanimous recompute failed", repair_needed: true
          end
        end

        begin
          read_route = if !is_wiki
            repo.dgit_read_routes.first
          else
            repo.dgit_wiki_read_routes.first
          end
          read_route_resolved = read_route.resolved_host
          read_route_path = read_route.path
        rescue GitHub::DGit::UnroutedError => e
          GitHub::Logger.log_exception({method: "dgit_read_routes"}, e)
          GitHub::Logger.log message: "Repair failed: unrouted (no available read replica)"
          return
        end

        # bail if we're somehow repairing from the same host
        if read_route.original_host == host
          raise GitHub::DGit::ReplicaRepairError, "Cannot repair repo #{repo_id} on #{host} from itself"
        end

        # log the action
        our_lock = false
        GitHub.stats.increment "dgit.#{host}.actions.repair-repo" if GitHub.enterprise?
        GitHub.dogstats.time("dgit.actions.repair-repo", tags: ["server:#{host}"]) do
          start = Time.now
          GitHub::Logger.log message: "DGit repairing #{is_wiki ? "wiki" : "repo"} replica #{repo_id} on #{host} from source host #{read_route.original_host}"

          # rpc.fs_lock and git fetch won't work unless we have a repo
          # directory to play in.  It doesn't matter what's there, though,
          # because git fetch and rsync will overwrite everything.  And
          # since the checksum is bad (we just checked!), no one will
          # accidentally read from or write to the empty repo.
          if !rpc.exist?
            rpc.init
            read_replica = GitHub::DGit::Routing.repo_replica_for_host(repo_id, read_route.original_host)
            reader_rpc = is_wiki ? read_replica.to_route(repo.wiki_shard_path).build_maint_rpc
                                 : read_replica.to_route(repo.original_shard_path).build_maint_rpc
            if reader_rpc.nw_linked?
              needs_sync = true
              rpc.nw_link
            end
          end

          # It's possible this repository replica doesn't require any repair. If
          # we recompute the checksums we could find we don't have any work
          # to do. But be careful... our replica could be so broken that we can't
          # recompute a checksum.
          begin
            host_checksums = GitHub::DGit::Maintenance.recompute_checksums(
                               repo, read_route.original_host,
                               is_wiki: is_wiki, force_dgit_init: force_dgit_init)
            if host_checksums[:repo] == host_checksums[host]
              GitHub::Logger.log message: "Recomputed checksums match.  No repair needed."
              return true
            end
          rescue GitHub::DGit::ChecksumInitError, ::GitRPC::Error, GitHub::DGit::ThreepcError, GitHub::DGit::ThreepcFailedToLock => e
            Failbot.report!(e, app: "github-dgit-debug")
            GitHub::Logger.log_exception({method: "recompute_checksums", host: read_route.original_host}, e)
            GitHub::Logger.log message: "Failed to recompute checksums. Replica requires repair"
          end

          # check to see if someone else has a lock
          if !rpc.fs_lock(lock_name, lock_contents, 1.hour.to_i)
            GitHub::Logger.log message: "Unable to get the repair lock"
            return
          end
          our_lock = true

          GitHub::DGit::Maintenance.backup_before_repair_with_rpc(rpc, self)

          (1..REPAIR_ATTEMPTS).each do |repair_attempt|
            if repair_attempt < REPAIR_ATTEMPTS && !Rails.test?
              one_repair_attempt(repair_attempt, read_route_resolved, read_route_path, rpc, host, repo_id, repo_type, is_wiki, start)
              begin
                host_checksums = GitHub::DGit::Maintenance.recompute_checksums(
                                   repo, read_route.original_host, is_wiki: is_wiki)
                if check_agreement(host_checksums, host, repo, repair_attempt)
                  rpc.nw_sync if needs_sync
                  return true
                end
              rescue GitHub::DGit::Error, ::GitRPC::Error, GitHub::DGit::ThreepcError
                # Mask any checksum recomputation failure unless it's the last attempt
                # In particular, very busy repos often can't `fs_read` the
                # `dgit-state` file, because it's missing during locked writes.
                raise if repair_attempt == REPAIR_ATTEMPTS
              end
            else
              # XXX: Untangle this AR dependency: with_dgit_lock only really needs the
              # XXX: NWO and a Delegate for use to instantiate the 3PC Client.  No need
              # XXX: to pass around the full AR object here.
              repo_for_lock = is_wiki ? repo.unsullied_wiki : repo
              GitHub.dogstats.time("dgit.actions.repair-repo.locked", tags: ["server:#{host}"]) do
                begin
                  GitHub.kv.set(key_name(repo_id), "repairing", expires: 5.minutes.from_now)
                  host_checksums = DGit.with_dgit_lock(repo_for_lock, read_route.original_host) do
                    one_repair_attempt(repair_attempt, read_route_resolved, read_route_path, rpc, host, repo_id, repo_type, is_wiki, start)
                  end
                  if check_agreement(host_checksums, host, repo, repair_attempt)
                    rpc.nw_sync if needs_sync
                    return true
                  end
                rescue GitHub::DGit::ThreepcError
                  raise if repair_attempt == REPAIR_ATTEMPTS
                ensure
                  GitHub.kv.del(key_name(repo_id))
                end
              end
            end
          end
          GitHub::Logger.log message: "Repair unsuccessful after #{REPAIR_ATTEMPTS} attempts"

          GitHub::DGit::Maintenance::set_network_state(network_id, host, GitHub::DGit::FAILED, GitHub::DGit::NOT_DORMANT)
          nil
        end
      rescue ::GitRPC::InvalidRepository, GitHub::DGit::ReplicaRepairError => e
        GitHub::DGit::Maintenance::set_network_state(network_id, host, GitHub::DGit::FAILED, GitHub::DGit::NOT_DORMANT)
        raise e
      ensure
        rpc.fs_unlock(lock_name) if our_lock
      end

      def self.one_repair_attempt(repair_attempt, read_route_resolved, read_route_path, replica_rpc, host, repo_id, repo_type, is_wiki, start)
        # repair step 1: use rpc to tell the broken replica to fetch git
        # state from the read-affinity replica.
        if Rails.development? || Rails.test?
          git_url = "file://#{read_route_path}"
          rsync_url = read_route_path + "/"
        else
          git_url = "git@#{read_route_resolved}:#{read_route_path}"
          rsync_url = git_url + "/"
        end

        # Save the size of the audit log prior to `git fetch`.  The fetch
        # will add lines to the audit_log, which will make `rsync
        # --append` produce garbage.  By truncating it back to its
        # pre-`fetch` size, we discard fetch's audit_log entries (which
        # rsync would normally overwrite anyway) and let `rsync --append`
        # work properly.
        if repair_attempt != 2 && replica_rpc.fs_exist?("audit_log")
          audit_log_size = replica_rpc.fs_size("audit_log")
        end

        begin
          res = replica_rpc.fetch(git_url, refspec: "+refs/*:refs/*", force: true, prune: true, dgit_disabled: true, committer_date: Time.current.to_s(:git))
        rescue GitRPC::CommandFailed => e
          raise GitHub::DGit::ReplicaRepairError, "Could not git fetch: #{e}"
        end
        GitHub::Logger.log stdout: res["out"], stderr: res["err"], message: "git fetch succeeded."
        replica_rpc.fs_truncate("audit_log", audit_log_size) if audit_log_size

        # repair step 2: tell the broken replica to rsync everything
        # except git state from the read-affinity replica.
        # Optimize passes 1 and 3, and make 2 more robust.  Specifically,
        # use --append to make audit_log updates much faster in the first
        # and third passes, and omit it in the second.  Use --checksum in
        # the second to pick up any files that may have the same size and
        # mtime but different contents.
        # So: the first pass handles the normal case quickly, and any
        # problems will cause the Spokes checksum to stay wrong, and we'll
        # go to the second pass.  The second pass will be more thorough
        # and will pick up quirks like a non-appended audit_log or a
        # config that was updated twice within the same second.  The third
        # pass is fast again, because it happens under lock, and
        # presumably the second pass will have taken care of the quirks.
        options1 = {repair_attempt: repair_attempt,
                    include: ["audit_log"], exclude: ["*"]}
        if repair_attempt == 2
          options1[:checksum] = true
        else
          options1[:append] = true
        end

        options2 = {repair_attempt: repair_attempt, exclude: EXCLUDE_RSYNC}
        options2[:checksum] = true if repair_attempt == 2

        res1 = do_rsync(replica_rpc, rsync_url, **options1)
        res2 = do_rsync(replica_rpc, rsync_url, **options2)
        res1 && res2
      end

      def self.do_rsync(replica_rpc, rsync_url, exclude: [], repair_attempt:, checksum: nil, append: nil, include: [])
        rsync_opts = {archive: true, verbose: true, hard_links: true,
                      delete: true, include: include, exclude: exclude}
        rsync_opts[:checksum] = true if checksum
        rsync_opts[:append] = true if append

        GitHub::Logger.log message: "rsync beginning for #{rsync_url}."
        res = replica_rpc.rsync(rsync_url, ".", rsync_opts)
        if !res["ok"]
          if GitHub::DGit::RETRYABLE_RSYNC_RESULTS.include?(res["status"])
            GitHub::Logger.log message: "rsync failed with retryable result #{res["status"]}"
            return false
          else
            raise GitHub::DGit::ReplicaRepairError, "Could not rsync: #{res["err"]}"
          end
        end

        if repair_attempt == 1
          GitHub::Logger.log stderr: res["err"], message: "rsync succeeded: #{res["out"].lines.size} lines of output."
        else
          GitHub::Logger.log stdout: res["out"], stderr: res["err"], message: "rsync succeeded."
        end

        true
      end

      def self.check_agreement(host_checksums, host, repo, repair_attempt)
        return unless host_checksums[:repo] == host_checksums[host]

        GitHub::Logger.log repair_attempts: repair_attempt, message: "DGit completed repair for repo #{repo.id} on #{host}"
        GitHub.dogstats.increment("dgit.actions.repair-repo.attempts", tags: ["attempts:#{repair_attempt}"])
        return true
      end

      def self.is_repairing?(repo)
        GitHub.kv.get(key_name(repo.id)).value { nil } == "repairing"
      end

      def self.queue
        :dgit_repairs
      end
    end
  end
end
