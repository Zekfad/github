# frozen_string_literal: true

module GitHub
  module Jobs
    class DestroyIntegration < Job
      def self.active_job_class
        ::DestroyIntegrationJob::Legacy
      end

      areas_of_responsibility :platform

      def self.queue
        :destroy_integration
      end

      def self.perform(integration_id)
        new(integration_id).perform
      end

      def initialize(integration_id)
        @integration_id = integration_id
      end

      def perform
        Integration.find(@integration_id).destroy
      end
    end
  end
end
