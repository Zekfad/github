# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class DeliverIntegrationUpdateEmail < Job
      def self.active_job_class
        ::DeliverIntegrationUpdateEmailJob
      end

      areas_of_responsibility :ecosystem_apps

      def self.queue
        :deliver_integration_update_email
      end

      def self.perform(installation_id)
        installation = IntegrationInstallation.find(installation_id)

        integration = installation.integration
        target      = installation.target

        target.admins.each do |admin|
          IntegrationMailer.updated_permissions(installation, recipient: admin).deliver_now
        end

        # If the installation can be updated by repo admins, find those admins
        # and email them as well.
        return unless target.organization?
        return unless integration.repository_permissions_only?

        # If the installation is installed on all, we don't want
        # to calculate if there are any repo admins that _could_
        # manage all of them.
        #
        # Too expensive.
        return if installation.installed_on_all_repositories?

        repo_admin_ids = Array(installation.repositories.map do |repo|
          repo.user_ids_with_privileged_access(min_action: :admin)
        end.reduce(:&))

        repo_admin_ids = repo_admin_ids - target.admins.pluck(:id)
        return if repo_admin_ids.empty?

        repo_admins = User.where(id: repo_admin_ids)

        repo_admins.each do |admin|
          IntegrationMailer.updated_permissions_repo_adminable(installation, recipient: admin).deliver_now
        end
      end
    end
  end
end
