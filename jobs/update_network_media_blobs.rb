# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class UpdateNetworkMediaBlobsError < RuntimeError; end

    class UpdateNetworkMediaBlobs < Job
      def self.active_job_class
        ::UpdateNetworkMediaBlobsJob
      end

      ALLOWED_ACTIONS = [
        :archive, :unarchive
      ].freeze

      areas_of_responsibility :lfs

      extend GitHub::HashLock

      def self.queue
        :lfs
      end

      def self.lock(options)
        options.with_indifferent_access["network_id"].to_i
      end

      def self.perform(options)
        options    = options.with_indifferent_access
        action     = options["action"].try(:to_sym)
        network_id = options["network_id"].try(:to_i)

        raise UpdateNetworkMediaBlobsError, "Must provide non-nil \"network_id\" argument to UpdateNetworkMediaBlobs job." unless network_id
        raise UpdateNetworkMediaBlobsError, "Must provide non-nil \"action\" argument to UpdateNetworkMediaBlobs job." unless action
        raise UpdateNetworkMediaBlobsError, "Unable to perform '#{action}' across `Media::Blob`s" unless ALLOWED_ACTIONS.include?(action)

        Media::Blob.where("repository_network_id = ?", network_id)
          .find_each(batch_size: 100) { |blob| blob.send(action) }
      rescue StandardError
        retries = options["retries"].to_i
        options["retries"] = retries + 1
        LegacyApplicationJob.retry_later(self.active_job_class, [options], retries: retries)

        raise
      end
    end
  end
end
