# frozen_string_literal: true

module GitHub
  module Jobs
    class FastTimelineExperiment < Job
      def self.active_job_class
        ::FastTimelineExperimentJob
      end

      include Scientist

      def self.queue
        :experiments
      end

      # Public: Perform fast timeline experiment for given timeline subject and viewer
      #
      # subject_type     - Symbol of timeline subject type, `:issue` or `:pull_request`
      # subject_id       - ID of timeline subject, Issue or PullRequest
      # viewer_id        - ID of viewing User
      # since            - Time to restrict to only more-recent items
      # all_valid_events - Flag to remove some events
      #
      # Returns nothing
      def self.perform(subject_type, subject_id, viewer_id, since = nil, all_valid_events = false)
        new(subject_type, subject_id, viewer_id, since, all_valid_events).perform
      end

      attr_reader :subject_type, :subject_id, :viewer, :start_time, :since, :all_valid_events

      def initialize(subject_type, subject_id, viewer, since, all_valid_events)
        @subject_type     = subject_type
        @subject_id       = subject_id
        @viewer           = viewer
        @since            = since
        @all_valid_events = all_valid_events
      end

      def perform
        return if load_subject.nil? || viewer.nil?

        prepare

        Platform::Security::RepositoryAccess.with_viewer(viewer) do
          run_experiment
        end
      end

      def prepare
        # fill memcache
        run_legacy_code
      end

      def run_experiment
        record_start_time

        science "fast-timeline" do |e|
          e.context science_context

          e.run_if do
            GitHub.flipper["fast-timeline-experiment"].enabled?(viewer)
          end

          e.use do
            clear_caches
            run_legacy_code
          end

          e.try do
            clear_caches
            run_new_code
          end

          e.clean do |value|
            value.map { |item| [item.class.name, item.try(:id)] }
          end

          e.compare do |control, candidate|
            control = filter_by_start_time(control).map do |item|
              next item unless item.is_a?(::DeprecatedPullRequestReviewThread)
              if GitHub.flipper[:use_review_thread_ar_model].enabled?(item.repository)
                item.activerecord_pull_request_review_thread
              else
                Platform::Models::PullRequestReviewThread.from_review_comments(item.comments)
              end
            end

            candidate = filter_by_start_time(candidate)

            Set.new(control) == Set.new(candidate)
          end
        end
      end

      def run_legacy_code
        subject = load_subject
        subject.send(:legacy_all_timeline_items_for, viewer,
                     since: since, all_valid_events: all_valid_events)
      end

      def run_new_code
        subject = load_subject
        subject.send(:new_all_timeline_items_for, viewer,
                     since: since, all_valid_events: all_valid_events)
      end

      def record_start_time
        @start_time = Time.now
      end

      def filter_by_start_time(values)
        values.reject do |value|
          value.created_at.to_i >= start_time.to_i
        end
      end

      def science_context
        {
          viewer_id: viewer.id,
          viewer_login: viewer.login,
          subject_type: subject_type,
          subject_id: subject_id,
          subject_repository: load_subject.repository.nwo,
          since: since,
          all_valid_events: all_valid_events,
          start_time: start_time,
        }
      end

      def load_subject
        subject_class.find(subject_id)
      end

      def clear_caches
        subject_class.connection.clear_query_cache
      end

      def subject_class
        case subject_type.to_sym
        when :issue
          Issue
        when :pull_request
          PullRequest
        else
          raise ArgumentError, "#{subject_type} is not supported"
        end
      end
    end
  end
end
