# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::Jobs::OtpSmsTimingCleanup < GitHub::Jobs::Job
  def self.active_job_class
    ::OtpSmsTimingCleanupJob
  end

  schedule interval: 30.seconds

  def self.queue
    :otp_sms_timing_cleanup
  end

  # Don't schedule this job under GHE.
  def self.enabled?
    !GitHub.enterprise?
  end

  def self.perform
    return unless GitHub.two_factor_sms_enabled?
    expired = OtpSmsTiming.expired_outstanding
    return if expired.empty?
    expired_data = expired.map(&:attributes)
    delete_keys(expired)
    uids = expired_data.map { |e| e["user_id"] }
    creds = TwoFactorCredential.where(user_id: uids)
    creds_by_uid = creds.group_by(&:user_id)

    expired_data.each do |timing|
      next unless cred = creds_by_uid[timing["user_id"]].try(:first)
      next unless country = cred.country_code
      provider = timing["provider"]
      GitHub.dogstats.increment "two_factor.sms_timing", tags: ["action:dropped", "provider:#{provider}", "country:#{country}", "store:mysql"]

      # Switch the cred's provider if it hasn't changed since this SMS was sent.
      cred.switch_provider if cred.provider.nil? || cred.provider == provider
    end

    # Save outside of the main loop since creds could show up multiple times.
    creds.select(&:changed?).each(&:save)
  end

  # Delete a set of keys
  #
  # keys - An Array of OtpSmsTiming objects.
  #
  # Returns nothing.
  def self.delete_keys(keys)
    keys.delete_all
  end
  private_class_method :delete_keys
end
