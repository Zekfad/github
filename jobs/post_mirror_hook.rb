# frozen_string_literal: true

module GitHub
  module Jobs
    # Same job as +RepositoryPush+ but with lower priority.
    class PostMirrorHook < RepositoryPush
      def self.active_job_class
        ::PostMirrorHookJob
      end

      areas_of_responsibility :repositories, :code_collab, :pull_requests

      def self.queue
        :post_mirror_hook
      end
    end
  end
end
