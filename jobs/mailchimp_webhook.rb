# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Responds to a WebHook event from MailChimp
    class MailchimpWebhook < Job
      def self.active_job_class
        ::MailchimpWebhookJob
      end

      areas_of_responsibility :unassigned

      extend GitHub::HashLock

      def self.queue
        :mailchimp
      end

      def self.perform(*args)
        job = new(*args)
        job.perform
        job
      end

      def initialize(type, email_address, options = {})
        @email_address = email_address
        @email         = UserEmail.find_by_email(@email_address)
        @user          = @email.try(:user)
        @webhook_type  = type
        @options       = options

        Failbot.push(
          job: self.class.name,
          user: @user.try(:login),
          email_id: @email.try(:id),
        )
      end

      def perform
        GitHub.dogstats.increment "mailchimp", tags: ["job:incoming_webhook"]
        unless @email
          raise GitHub::Mailchimp::WebhookError,
            "Missing UserEmail"
        end

        case @webhook_type.to_s
        when "unsubscribe"
          unsubscribe_webhook
        when "cleaned"
          bounce_webhook
        else
          raise GitHub::Mailchimp::WebhookError,
            "Unsupported hook '#{@webhook_type.inspect}'"
        end
      end

      def unsubscribe_webhook
        NewsletterPreference.set_to_transactional(user: @user)
      end

      def bounce_webhook
        # marking as a hard bounce
        # see http://kb.mailchimp.com/delivery/deliverability-research/soft-vs-hard-bounces
        @email.mark_as_bouncing!(hard_or_soft: :hard, source: :mailchimp)
      end
    end
  end
end
