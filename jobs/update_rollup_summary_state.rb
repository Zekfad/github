# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class UpdateRollupSummaryState < Job
      def self.active_job_class
        ::UpdateRollupSummaryStateJob
      end

      areas_of_responsibility :notifications

      def self.queue
        :notifications
      end

      def self.perform_with_retry(repo_id, issue_id, issue_event_id, options = {})
        new(repo_id, issue_id, issue_event_id, options).perform
      end

      attr_reader :repo_id, :issue_id, :issue_event_id, :options

      def initialize(repo_id, issue_id, issue_event_id, options = {})
        @repo_id = repo_id
        @issue_id = issue_id
        @issue_event_id = issue_event_id
        @options = options.with_indifferent_access
      end

      def perform
        return unless repository = Repository.find_by_id(repo_id)
        return unless issue = Issue.find_by_id(issue_id)

        rollup_response = GitHub.newsies.web.find_rollup_summary_by_thread(repository, issue)

        if rollup_response.success?
          issue_event = IssueEvent.find(issue_event_id).event
          last_summarized_issue_event = IssueEvent.summarized_events.where(issue_id: issue_id).last.event

          if issue_event == last_summarized_issue_event
            update_rollup_summary_for(rollup_response.value, issue_event)
          end
        else
          retry_later
        end
      end

      def update_rollup_summary_for(summary, issue_event)
        response = RollupSummary.throttle { GitHub.newsies.web.update_rollup_summary(summary, issue_event) }

        retry_later if response.failed?
      end

      def retry_later
        LegacyApplicationJob.retry_later(self.class.active_job_class, [repo_id, issue_id, issue_event_id, options], {
          retries: options["retries"].to_i,
        })
      end
    end
  end
end
