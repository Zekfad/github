# rubocop:disable Style/FrozenStringLiteralComment

# Send user survey responses to Octolytics
module GitHub
  module Jobs
    class UserSurveyAnswersToOctolytics < Job
      def self.active_job_class
        ::UserSurveyAnswersToOctolyticsJob
      end

      include FactsFromSurveyAnswers

      areas_of_responsibility :analytics

      def self.queue
        :analytics
      end

      def self.perform(user_id, octolytics: GitHub.analytics)
        return unless user = User.find_by_id(user_id)
        survey = Survey.find_by_slug!("user_identification")

        new(
          user: user,
          survey: survey,
          octolytics: octolytics
        ).perform
      end

      def initialize(user:, survey:, octolytics:)
        @user       = user
        @survey     = survey
        @octolytics = octolytics
      end

      def perform
        octolytics.record(analytics_events) if analytics_events.any?
      end

      private

      attr_reader :user, :survey, :octolytics

      def analytics_events
        @analytics_events ||= facts.map do |fact|
          {
            event_type: "survey_response",
            dimensions: {
              user_id: user.id,
              question: fact.dimension,
              answer: fact.value,
            },
            timestamp: fact.recorded_at.to_i,
          }
        end
      end

      def survey_answers
        survey.answers.for(user)
      end
    end
  end
end
