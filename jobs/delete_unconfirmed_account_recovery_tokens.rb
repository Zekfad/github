# frozen_string_literal: true

module GitHub
  module Jobs
    class DeleteUnconfirmedAccountRecoveryTokens < Job
      def self.active_job_class
        ::DeleteUnconfirmedAccountRecoveryTokensJob
      end

      BATCH_SIZE = 1000
      TIME_LIMIT = 30.minutes

      schedule interval: TIME_LIMIT

      def self.queue
        :delete_unconfirmed_account_recovery_tokens
      end

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.perform
        DelegatedRecoveryToken.where("confirmed_at IS NULL AND created_at < ?", TIME_LIMIT.ago).find_each(batch_size: BATCH_SIZE) do |token|
          DelegatedRecoveryToken.throttle { token.destroy }
        end
      end
    end
  end
end
