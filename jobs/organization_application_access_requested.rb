# frozen_string_literal: true

module GitHub
  module Jobs
    class OrganizationApplicationAccessRequested < Job
      def self.active_job_class
        ::OrganizationApplicationAccessRequestedJob
      end

      areas_of_responsibility :platform, :orgs

      def self.queue
        :organization_application_access_requested
      end

      def self.perform(*args)
        new(*args).perform
      end

      def initialize(requestor_id, approval_id)
        @requestor = User.find_by_id(requestor_id)
        @approval  = OauthApplicationApproval.find_by_id(approval_id)
      end

      def perform
        return unless @requestor && @approval

        oauth_app    = @approval.application
        organization = @approval.organization

        organization.admins.each do |admin|
          mail = OrganizationMailer.application_access_requested(
            @requestor, organization, oauth_app, admin
          )

          mail.deliver_now
        end
      end
    end
  end
end
