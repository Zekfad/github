# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class PurgeRestorables < Job
      def self.active_job_class
        ::PurgeRestorablesJob
      end

      areas_of_responsibility :restorables

      # Only one of these jobs should run at any given time.
      extend GitHub::HashLock

      schedule interval: 24.hours

      def self.queue
        :purge_restorables
      end

      def self.enabled?
        true
      end

      def self.expiration_period
        Time.now - ::RepositoryPurgeArchivedJob.expiration_period + 1.day
      end

      def self.models
        @models ||= begin
          models = [
            Restorable::OrganizationUser,
            Restorable::TypeState,
            Restorable::Membership,
            Restorable::Repository,
            Restorable::RepositoryStar,
            Restorable::WatchedRepository,
            Restorable::IssueAssignment,
          ]
          models << Restorable::LdapTeamSyncUser if GitHub.enterprise?
          models
        end
      end
      # purge records one day earlier than Archived Repository Records


      # Public: Purges Restorable records older than EXPIRATION_PERIOD
      def self.perform(expiration: nil)
        expiration ||= expiration_period
        Restorable.where("created_at < ?", expiration).find_in_batches do |restorables|
          models.each do |model|
            delete_dependent_records(model, restorables)
          end

          # Remove Restorable records at the end to ensure the dependents aren't abandoned
          GitHub.dogstats.histogram "restorable.purged_records", restorables.count
          Restorable.throttle do
            Restorable.where(id: restorables.map(&:id)).delete_all
          end
        end
      end

      def self.delete_dependent_records(model, restorables)
        model.where(restorable_id: restorables.map(&:id)).find_in_batches do |batch|
          GitHub.dogstats.histogram "restorable.purged_records", batch.count
          model.throttle do
            model.where(id: batch.map(&:id)).delete_all
          end
        end
      end
    end
  end
end
