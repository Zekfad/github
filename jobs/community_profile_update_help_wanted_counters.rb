# frozen_string_literal: true

module GitHub
  module Jobs
    class CommunityProfileUpdateHelpWantedCounters < Job
      def self.active_job_class
        ::CommunityProfileUpdateHelpWantedCountersJob
      end

      areas_of_responsibility :community_and_safety

      # reusing the queue created by RepositorySetCommunityHealthFlagsJob
      def self.queue
        :detect_community_health_files
      end

      def self.perform(repository_id)
        repo = Repository.find_by_id(repository_id)
        return unless repo

        repo.build_community_profile if repo.community_profile.nil?
        help_wanted_label = repo.help_wanted_label
        good_first_issue_label = repo.good_first_issue_label
        community_profile = repo.community_profile

        community_profile.help_wanted_issues_count = if help_wanted_label
          help_wanted_label.issues.open_issues.without_pull_requests.count
        else
          0
        end

        community_profile.good_first_issue_issues_count = if good_first_issue_label
          good_first_issue_label.issues.open_issues.without_pull_requests.count
        else
          0
        end

        community_profile.save
        repo.touch # trigger repo search indexer
      end
    end
  end
end
