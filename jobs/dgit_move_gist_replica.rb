# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    class DgitMoveGistReplica < Job
      def self.active_job_class
        ::SpokesMoveGistReplicaJob
      end

      areas_of_responsibility :dgit

      def self.perform(gist_id, from_host, to_host, queued_time = nil)
        Failbot.push spec: "gist/#{gist_id}",
                     app: "github-dgit"

        GitHub::Logger.log_context(job: name.demodulize, spec: "gist/#{gist_id}") do
          GitHub::Logger.log(method: "perform!", from_host: from_host, to_host: to_host) do
            perform!(gist_id, from_host, to_host, queued_time)
          end
        end
      rescue Freno::Throttler::Error
        # Ignore freno errors and rely on retries from our own maintenance scheduler
      rescue GitHub::DGit::ReplicaDestroyNotFoundError
        # Ignore. This is possible due to races.
      end

      def self.perform!(gist_id, from_host, to_host, queued_time = nil)
        raise GitHub::DGit::ReplicaMoveError, "Source and destination are both #{from_host}" if from_host == to_host

        # If a gist was replicated to another host after this time,
        # don't raise; assume another move job was scheduled before us.
        # Use either the time this job was queued (plus a fudge factor of
        # one minute) or an hour ago, if no time was specified.
        fail_time = queued_time ? Time.at(queued_time) - 1.minute : Time.now - 1.hour

        # See where we're currently replicated and check that there is a
        # replica on `from_host`. The destroy job will also do this, but
        # we want to do it early so we can fail before going through
        # with the create job.
        replicas = GitHub::DGit::Routing.all_gist_replicas(gist_id)
        Failbot.push delegate_replicas: replicas.inspect

        unless replicas.any? { |r| r.host == from_host }
          return if replicas.any? { |r| r.created_at > fail_time }
          raise GitHub::DGit::ReplicaDestroyNotFoundError, "Gist #{gist_id} on #{from_host} not found"
        end

        GitHub::Logger.log(method: "GitHub::Jobs::DgitCreateGistReplica.perform!", host: to_host) do
          GitHub::Jobs::DgitCreateGistReplica.perform!(gist_id, to_host, via: :dgit_move_gist_replica)
        end

        GitHub::Logger.log(method: "GitHub::Jobs::DgitDestroyGistReplica.perform!", host: from_host) do
          # Inhibit repairs to avoid having the destroy job try to create any new replicas. We just created one.
          GitHub::Jobs::DgitDestroyGistReplica.perform!(gist_id, from_host, via: :dgit_move_gist_replica, inhibit_repairs: true)
        end
      end

      def self.queue
        :dgit_repairs
      end
    end
  end
end
