# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    # Moves a Repository record and associated records into archive tables and
    # removes all git repository storage after performing a backup.
    #
    # All aspects of this job must be idempotent or at least retryable. If
    # requeueing the job fails repeatedly, the logic must be corrected to allow
    # the job to complete.
    class RepositoryDelete < Job
      def self.active_job_class
        ::RepositoryDeleteJob
      end

      areas_of_responsibility :repositories

      extend GitHub::HashLock

      def self.queue
        :archive_restore
      end

      def self.job_id(repo_id)
        "repository-delete-job_#{repo_id}"
      end

      def self.status(repo_id)
        JobStatus.find(job_id(repo_id))
      end

      def self.perform_with_retry(repo_id, unused, repo_path, opts = {})
        status = JobStatus.create(id: job_id(repo_id))
        status.track do
          # if the repository record doesn't exist we assume all work has been
          # completed.
          repo = Repository.find_by(id: repo_id)

          # if the repository isn't found, that suggests either
          # a) we are queuing job with bad parameters
          # b) we are queuing a job and then archiving the repository elsewhere
          if repo.nil?
            raise RepositoryNotFound.new(repo_id)
          end

          Failbot.push spec: repo.dgit_spec

          GitHub::Logger.log_context(job: name.demodulize, spec: repo.dgit_spec) do
            GitHub::Logger.log(method: "remove_via_job") do
              repo.remove_via_job
            end
          end
        end
      end

      # All RepositoryDelete jobs are locked by their repository id. No attempt
      # will be made to enqueue a job that's already running and the lock is
      # checked before running the job.
      def self.lock(repo_id, unused, repo_path, opts = {})
        repo_id
      end

      class RepositoryNotFound < StandardError
        def initialize(repository_id)
          super("Attempt to archive and purge repository (#{repository_id}) that doesn't exist")
        end
      end
    end
  end
end
