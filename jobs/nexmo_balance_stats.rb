# rubocop:disable Style/FrozenStringLiteralComment

# Records the balance for our Nexmo account, which is one of our SMS providers.
module GitHub
  module Jobs
    class NexmoBalanceStats < Job
      def self.active_job_class
        ::NexmoBalanceStatsJob
      end

      schedule interval: 10.minutes

      def self.queue
        :nexmo_balance_stats
      end

      def self.enabled?
        GitHub.two_factor_sms_enabled?
      end

      def self.perform
        return unless GitHub::SMS.providers_for_env.map(&:provider_name).include?(:nexmo)

        provider = GitHub::SMS::Nexmo.new
        balance = provider.account_balance
        GitHub.dogstats.gauge("two_factor.nexmo_balance", balance)
      end
    end
  end
end
