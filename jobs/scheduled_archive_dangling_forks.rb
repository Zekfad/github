# frozen_string_literal: true

module GitHub
  module Jobs
    class ScheduledArchiveDanglingForks < Job
      def self.active_job_class
        ::ScheduledArchiveDanglingForksJob
      end

      areas_of_responsibility :orgs, :repositories

      schedule interval: 24.hours

      def self.queue
        :archive_dangling_forks
      end

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.perform
        ArchiveDanglingForksJob.perform_later
      end
    end
  end
end
