# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class UserContributionsBackfill < Job
      def self.active_job_class
        ::UserContributionsBackfillJob
      end

      areas_of_responsibility :user_profile

      extend GitHub::HashLock

      def self.queue
        :user_contributions_backfill
      end

      # This job uses repo_ids to maintain its own work queue: it will attempt
      # to process the first repo in the `repo_ids` list, and then requeues the
      # remainder.
      def self.perform_with_retry(repo_ids, user_id, opts = {})
        queue_remainder = true

        # Clone to avoid confusing hash lock key generation:
        repo_ids = repo_ids.dup
        repo_id = repo_ids.shift

        user, repo = nil
        ActiveRecord::Base.connected_to(role: :reading) do
          repo = Repository.find_by_id(repo_id)
          user = User.find_by_id(user_id)
        end

        if repo && user
          Failbot.push repo: repo&.readonly_name_with_owner
          Failbot.push user: user.login
          CommitContribution.backfill_user!(repo, user)
        end
      rescue Freno::Throttler::Error
        # Automatic retry will requeue this job when this error occurs, and we
        # don't want to queue both this job and the subsequent repos at the same
        # time. Just allow this to be retried and don't queue anything else.
        queue_remainder = false
        raise
      ensure
        if queue_remainder && repo_ids.any?
          UserContributionsBackfillJob.perform_later(repo_ids, user_id)
        end
      end

      # Prevent several jobs from calculating contributions for this repo
      # and user at the same time. Competing jobs cause RecordNotUnique
      # exceptions as they simultaneously try to delete and insert into the
      # commit_contributions table.
      #
      # Returns the lock key String.
      def self.lock(repo_ids, user_id, opts = {})
        [repo_ids.first, user_id].join(":")
      end
    end
  end
end
