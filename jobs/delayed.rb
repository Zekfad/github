# frozen_string_literal: true

module GitHub
  module Jobs
    class Delayed < Job
      def self.active_job_class
        ::DelayedJob
      end

      areas_of_responsibility :background_jobs

      def self.queue
        :delayed
      end

      def self.perform(klass, id, method, *args)
        return unless object = klass.constantize.find_by_id(id)
        object.send(method, *args)
      end
    end
  end
end
