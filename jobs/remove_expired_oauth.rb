# frozen_string_literal: true

require "github/hash_lock"

module GitHub
  module Jobs

    # This job will remove all expired OauthAccesses.
    class RemoveExpiredOauth < Job
      def self.active_job_class
        ::RemoveExpiredOauthJob
      end

      areas_of_responsibility :platform

      extend GitHub::HashLock

      BATCH_SIZE = 100

      schedule interval: 1.minute
      def self.enabled?; true; end

      # Public: The queue from which this job processor will request work.
      #
      # Returns the queue name as a Symbol
      #
      def self.queue
        :remove_expired_oauth
      end

      def self.perform
        expired_oauth_accesses = ActiveRecord::Base.connected_to(role: :reading) do
          OauthAccess.where("expires_at_timestamp < ?", Time.current.to_i).to_a
        end

        expired_oauth_accesses.each_slice(BATCH_SIZE).each do |batch|
          OauthAccess.throttle do
            batch.map(&:expire)
          end
        end
      end
    end
  end
end
