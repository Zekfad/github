# frozen_string_literal: true

module GitHub::Jobs
  class PageCertificateWork < Job
    def self.active_job_class
      ::PageCertificateWorkJob
    end

    areas_of_responsibility :pages

    # We can retry and retry and retry. Eventually, we have to give up.
    # By default, we give up after about 20 retries. With exponential
    # backoff, 20 tries is hella long and there's really no reason for it.
    MAXIMUM_RETRIES = 5

    Error = Class.new(RuntimeError)
    TryLaterError = Class.new(Error) # Raise this when you want to retry the job.
    BadStateError = Class.new(Error)
    BadActionError = Class.new(Error)
    TooManyRetriesError = Class.new(Error)

    # Potentially transient errors that, when encountered, mean a portion of
    # work should be retried.
    RECOVERABLE_ERRORS = [
      TryLaterError,
      Earthsmoke::Error,
      Fastly::Error,
      Faraday::Error,
      Page::Certificate::RecoverableError,
    ]

    retry_on_error *RECOVERABLE_ERRORS

    def self.queue
      "pages_https"
    end

    # Queue this job to be run.
    #
    # Returns nothing.
    def self.enqueue(certificate_id)
      PageCertificateWorkJob.perform_later(certificate_id)
    end

    # Do a portion of work for a certificate.
    #
    # certificate_id - The ID of the Page::Certificate we're doing work for.
    #
    # Returns nothing.
    def self.perform_with_retry(certificate_id, options = {})
      Failbot.push(app: "pages-certificates")

      retries = (options[:retries] || options["retries"]).to_i

      Failbot.push(retries: retries)
      GitHub.dogstats.histogram("pages.certificates.retries", retries)

      if retries > MAXIMUM_RETRIES
        Failbot.report(
          TooManyRetriesError.new("exceeded maximum retries: #{retries}"),
        )
        return
      end

      new(certificate_id).perform
    end

    # Instantiate a new PageCertificateWork instance.
    #
    # certificate_id - The ID of the Page::Certificate we're doing work for.
    #
    # Returns nothing.
    def initialize(certificate_id)
      @certificate_id = certificate_id
    end

    # Perform a portion of work.
    #
    # Returns nothing.
    def perform
      return if certificate.nil?

      state = certificate.current_state
      action = certificate.with_lock { do_work(state) }

      case action
      when :proceed
        schedule_work
      when :retry, :error_retry
        schedule_retry
      when :halt, :error_halt
        # do nothing
      else
        raise BadActionError, action
      end

      GitHub.dogstats.increment("pages.certificates.work", tags: [
        "state:#{state}",
        "action:#{action}",
      ])
    end

    def do_work(state)
      case state
      when :new, :authorization_revoked, :bad_authz
        certificate.request_authorization
      when :authorization_created
        certificate.request_authorization_verification
      when :authorization_pending, :approved
        certificate.check_authorization_verification
      when :authorized
        certificate.request_certificate
      when :issued
        certificate.upload_certificate
      when :uploaded
        certificate.check_uploaded_certificate
      else
        raise BadStateError, certificate.current_state
      end
    rescue Acme::Client::Error => err
      certificate.reset_flow
      track_error(err)
      :error_retry
    rescue *RECOVERABLE_ERRORS => err
      track_error(err)
      :error_retry
    rescue => err
      track_error(err)
      :error_halt
    end

    # The certificate we're doing work for.
    #
    # Returns a Page::Certificate instance.
    def certificate
      return @certificate if defined?(@certificate)
      @certificate = Page::Certificate.find(@certificate_id)
    end

    # Schedule the next portion of work to be run.
    #
    # Returns nothing.
    def schedule_work
      self.class.enqueue(@certificate_id)
    end

    # Schedule the current portion of work to be retried.
    #
    # Returns nothing.
    def schedule_retry
      raise TryLaterError, "Retry requested for certificate_id=#{@certificate_id}"
    end

    def track_error(err)
      return if certificate.nil?

      Failbot.push(
        certificate_id: certificate.id,
        certificate_domain: certificate.domain,
        certificate_state: certificate.current_state,
        fastly_certificate_id: certificate.fastly_certificate_id,
        authorization_url: certificate.authorization_url,
      )
      Failbot.report(err, app: "pages-certificates")

      GitHub.dogstats.increment("pages.certificates.error", tags: [
        "state:#{certificate.current_state}",
        "class:#{err.class.name.gsub(/::/, "_").downcase}",
      ])
    end
  end
end
