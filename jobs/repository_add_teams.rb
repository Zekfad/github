# frozen_string_literal: true

module GitHub
  module Jobs
    class RepositoryAddTeams < Job
      def self.active_job_class
        ::RepositoryAddTeamsJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :repository_add_teams
      end

      # Perform the long running portion of a adding teams of a repository
      # to another one. Used when forking a repository.
      #
      # repo_id         - The repository id of the repo the teams get added to.
      # other_repo_id   - The repository id of the repo the teams are read from.
      # opts            - Job options, including retry counts.
      #
      def self.perform_with_retry(repo_id, other_repo_id, opts = {})
        repo = Repository.find_by_id(repo_id)
        return if repo.blank?

        other_repo = Repository.find_by_id(other_repo_id)
        return if other_repo.blank?

        repo.add_teams_of!(other_repo)
      end
    end
  end
end
