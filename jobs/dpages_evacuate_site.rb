# frozen_string_literal: true

require "github/hash_lock"
require "github/timeout_and_measure"
require "github/pages/management/delegate"
require "github/pages/management/evacuate"

module GitHub
  module Jobs
    # DpagesEvacuateSite evacuates a single site from the given host.
    # To enqueue, pass an array of arguments including [page_id, page_deployment_id, host].
    # page_deployment_id may be nil for legacy deployments.
    class DpagesEvacuateSite < Job
      def self.active_job_class
        ::DpagesEvacuateSiteJob
      end

      areas_of_responsibility :pages

      class EvacuationFailed < RuntimeError
        def areas_of_responsibility
          [:pages]
        end
      end

      extend GitHub::HashLock
      include GitHub::TimeoutAndMeasure

      # The queue the evacuations run on. The scheduler jobs don't run on
      # this queue, they're enqueued on the :dpages_evacuations_scheduler queue.
      def self.queue
        :dpages_evacuations
      end

      # Perform the evacuation for the single site on the single host.
      def self.perform(*args)
        new(args, delegate: GitHub::Pages::Management::Delegate.new(logger: GitHub::Logger)).perform
      end

      def initialize(args, delegate: nil)
        @delegate = delegate || GitHub::Pages::Management::Delegate.new
        @args = args
      end

      def validate_args!
        # args must contain [page_id, page_deployment_id, host]
        if !@args.is_a?(Array) || @args.size != 3
          raise ArgumentError, "invalid arguments: #{@args.inspect}"
        end

        page_id, page_deployment_id, host = *@args
        unless page_id.is_a?(Integer)
          raise ArgumentError, "page_id must be an integer, but was #{page_id.class}: #{page_id.inspect}"
        end

        unless page_deployment_id.nil? || page_deployment_id.is_a?(Integer)
          raise ArgumentError, "page_deployment_id must be nil or an integer, but was #{page_deployment_id.class}: #{page_deployment_id.inspect}"
        end

        unless host.is_a?(String) && host.size > 0
          raise ArgumentError, "host must be a string, but was #{host.class}: #{host.inspect}"
        end

        true
      end

      # Limit the execution time of this job so we don't have dangling evacuations.
      # If it times out, then fine: the scheduler will enqueue it again.
      def max_execution_time
        10.minutes
      end

      # Perform the evacuation, limited to max_execution_time, with logs and stats along the way.
      def perform
        validate_args!

        timeout_and_measure(max_execution_time.to_i, "pages.dpages_evacuations.work") do
          page_id, page_deployment_id, host = *@args

          command = GitHub::Pages::Management::Evacuate.new(delegate: @delegate, host: host)
          Failbot.push(app: "pages", page_id: page_id, page_deployment_id: page_deployment_id, host: host)

          unless GitHub::Pages::Management::Evacuate.evacuating_hosts.include?(host)
            @delegate.log "Skipping evacution of page_id=#{page_id} page_deployment_id=#{page_deployment_id.inspect}:" \
              " host=#{host} no longer evacuting"
            break false
          end

          @delegate.log "Evacuating page_id=#{page_id} page_deployment_id=#{page_deployment_id.inspect} from host=#{host}"

          result = begin
            command.evacuate_single_site(page_id: page_id, page_deployment_id: page_deployment_id)
          rescue GitHub::Pages::Management::ExecutionError => e
            Failbot.push(logged: @delegate.logged)
            raise e
          end

          unless result
            @delegate.log "Failed to evacuate page_id=#{page_id} page_deployment_id=#{page_deployment_id.inspect} from host=#{host}"
            Failbot.push(logged: @delegate.logged)
            raise EvacuationFailed, "Evacuation failed for host #{host}"
          end

          true
        end
      end
    end
  end
end
