# rubocop:disable Style/FrozenStringLiteralComment

require "github/redis/mutex"

module GitHub
  module Jobs

    # The purpose of this job is to reconcile the state of Issues and
    # Milestones between the database and the search index. We do this by
    # iterating over each of these models in batches. The `updated_at`
    # timestamp of each model is compared between the database and the search
    # index. The search index is updated for any record where the timestamp
    # disagrees.
    #
    # Each repair job will process 250 records. When it has repaired all the
    # issues and milestones it will enqueue another job. This process will
    # continue until all issues and milestones have been repaired.
    #
    # To make this whole process faster, multiple repair jobs can be enqueued.
    # The current offest into the issues and milestones tables are stored in
    # redis. Access to these values is coordinated via a shared mutex. Don't
    # spin up too many repair jobs otherwise you'll kill the database or the
    # search index or both.
    class RepairIssuesIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairIssuesIndexJob
      end

      areas_of_responsibility :search, :issues

      reconcile "issue",
        fields: %w[updated_at],
        limit: 750,
        reject: :spammy?,
        include: [:repository, {assignments: :assignee}, {comments: :user}, :labels],
        joins: "USE INDEX (PRIMARY) INNER JOIN repositories ON repositories.id = issues.repository_id",
        conditions: "repositories.has_issues is true AND issues.pull_request_id is null"

      reconcile "milestone",
        fields: %w[updated_at],
        limit: 750,
        reject: :spammy?,
        include: [:repository, :created_by],
        joins: :repository,
        conditions: "repositories.has_issues is true"

      def self.queue
        :index_bulk
      end
    end
  end
end
