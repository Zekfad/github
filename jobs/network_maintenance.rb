# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class NetworkMaintenance < Job
      def self.active_job_class
        ::NetworkMaintenanceJob
      end

      Error = Class.new(StandardError)

      areas_of_responsibility :git

      # Network maintenance can take quite a while on large repositories.
      # Make sure the job hashlock and GitRPC give it at least this much time
      # to run.
      JOB_TIMEOUT = 120*60

      def self.queue
        :network_maintenance
      end

      def self.lock(network_id, previous_status: nil)
        network_id
      end

      # Add a minute to the hashlock, so if the GitRPC operation runs hard up
      # against the timeout, another job doesn't start and immediately race
      # with the one that's ending.
      def self.lock_timeout
        JOB_TIMEOUT+60
      end

      def self.log(entity, data, &blk)
        GitHub::Logger.log_context(job: active_job_class.to_s, spec: entity.dgit_spec) do
          GitHub::Logger.log(data, &blk)
        end
      end

      def self.perform(network_id, previous_status: nil)
        Failbot.push spec: "network/#{network_id}"

        return unless network = ActiveRecord::Base.connected_to(role: :reading) { RepositoryNetwork.find_by_id(network_id) }

        log(network, method: "perform") do
          log(network,
              message: "starting git maintenance",
              status: network.maintenance_status,
              maintenance_elapsed: Time.now - (network.last_maintenance_at || network.created_at),
              maintenance_pushes: network.pushed_count_since_maintenance || 0)

          GitHub.dogstats.time("git_maintenance.perform", tags: ["type:network"]) do
            perform!(network, previous_status)
          ensure
            status = network.maintenance_status
            tags = ["type:network", "result:#{status}"]
            if previous_status
              tags << "previous_result:#{previous_status}"
            end
            GitHub.dogstats.increment("git_maintenance", tags: tags)
          end
        ensure
          log(network,
              at: "exit",
              message: "exiting git maintenance",
              status: network.maintenance_status)
        end

        nil
      end

      # Run network maintenance for the given network.
      def self.perform!(network, previous_status)
        # If a network has no root it's likely some sort of failed detach has
        # removed the actual data from this network. There is really nothing for
        # us to do here but mark it broken so we don't keep trying to perform
        # maintenance continuously.
        unless network.root
          network.mark_as_broken
          Failbot.report(Error.new("RepositoryNetwork has no valid root"), app: "github")
          return
        end

        # There is no maintenance to be done for orphaned networks so we should
        # just mark it as broken and move on.
        if network.orphaned?
          network.mark_as_broken
          Failbot.report(Error.new("RepositoryNetwork has become orphaned"), app: "github")
          return
        end

        # if a backup-utils backup is in progress, delay the maintenance operation
        # by requeuing after a short sleep period.
        if GitHub::Enterprise.backup_in_progress?
          clear_lock(network.id)
          network.schedule_maintenance
          sleep 1
          return
        end

        GitRPC.with_timeout(JOB_TIMEOUT) do
          begin
            network.perform_maintenance(previous_status)
          rescue GitRPC::Protocol::DGit::ResponseError => e
            GitHub::Logger.log_exception({answers: format_results(e.answers), errors: format_results(e.errors)}, e)
          end
        end
      end

      def self.format_results(results)
        results.map { |route, obj| "#{format_result(route, obj)}" }.join("/")
      end

      def self.format_result(route, obj)
        "#{::GitRPC::Protocol::DGit.format_route(route)} => {out => #{obj["out"].dump}, err => #{obj["err"].dump}}"
      end

      # All NetworkMaintenance jobs are locked by their network id. No attempt
      # will be made to enqueue a job that's already running and the lock is
      # checked before running the job.
      extend GitHub::HashLock
    end
  end
end
