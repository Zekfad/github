# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    class DgitCreateNetworkReplica < Job
      def self.active_job_class
        ::SpokesCreateNetworkReplicaJob
      end

      areas_of_responsibility :dgit

      def self.perform(network_id, host)
        Failbot.push spec: "network/#{network_id}",
                     app: "github-dgit"

        GitHub::Logger.log_context(job: name.demodulize, spec: "network/#{network_id}") do
          GitHub::Logger.log(method: "perform!", host: host) do
            GitHub.dogstats.time("dgit.actions.create-network", tags: ["server:#{host}"]) do
              perform!(network_id, host)
            end
          end
        end
      rescue Freno::Throttler::Error
        # Ignore freno errors and rely on retries from our own maintenance scheduler
      rescue GitHub::DGit::ReplicaCreateAlreadyPresentError
        # Ignore this case. This race does still occur in practice, but it's not
        # very interesting.
      end

      def self.perform!(network_id, host, after_rsync = nil)
        raise GitHub::DGit::ReplicaCreateError, "#{host} is not a DGit host" unless GitHub::DGit::get_hosts(storage_class: :all).include?(host)

        GitHub.stats.increment "dgit.#{host}.actions.create" if GitHub.enterprise?
        GitHub.dogstats.increment "dgit", tags: ["action:create", "host:#{host}"]

        # Insert a new network replica.
        begin
          network_replica_id = ActiveRecord::Base.connected_to(role: :writing) do
            db = GitHub::DGit::DB.for_network_id(network_id)
            sql = db.SQL.new \
              network_id: network_id,
              host: host,
              state: GitHub::DGit::CREATING
            sql.add <<-SQL
              INSERT INTO network_replicas (network_id, host, state, read_weight, created_at, updated_at)
                VALUES (:network_id, :host, :state, 0, NOW(), NOW())
            SQL
            db.throttle { sql.run }
            sql.last_insert_id
          end # connected_to
        rescue ActiveRecord::RecordNotUnique
          raise GitHub::DGit::ReplicaCreateAlreadyPresentError, "There is already a replica of network #{network_id} on #{host}"
        end

        # There's a bit of a race here.
        # - If someone deletes a fork from the network at this point, the
        #   DELETE associated with that will find no rows in
        #   repository_replicas matching <host,repo_id> to delete.  We may
        #   in fact end up adding <host,repo_id> right after it was
        #   supposed to be deleted.
        # - If someone creates a fork in the network right now, we may end
        #   up getting the same repository_replicas row inserted twice,
        #   and get a key conflict, which would make either the fork or
        #   the network-replica creation fail.
        # The cleanup process is to get network_replicas and
        # repository_replicas back in sync.
        # FIXME: take the fork lock, maybe?

        # Generate the repo set of values to insert for repository_replicas.
        repo_ids = GitHub::DGit::Util.repo_ids_for(network_id)

        unless repo_ids.empty?
          repo_values_to_insert = repo_ids.map do |repo_id|
            [repo_id, GitHub::DGit::RepoType::REPO, network_replica_id, host, "creating", GitHub::SQL::NOW, GitHub::SQL::NOW]
          end

          db = GitHub::DGit::DB.for_network_id(network_id)

          # Insert the repo replicas values.
          repo_values_to_insert.each_slice(100) do |batch|
            sql = db.SQL.new \
              network_id: network_id,
              values: GitHub::SQL::ROWS(batch)
            sql.add <<-SQL
              INSERT INTO repository_replicas
                          (repository_id, repository_type, network_replica_id, host, checksum, created_at, updated_at)
                   VALUES :values
            SQL
            db.throttle { sql.run }
          end

          # Generate the wiki set of values to insert for repository_replicas.
          #
          # Not all repositories have wikis so the set of repo IDs is narrowed
          # down here to just those repo IDs that already have a wiki-typed
          # entry in the repository_checksums table.
          sql = db.SQL.new \
            network_id: network_id,
            repo_ids: repo_ids,
            repository_type: GitHub::DGit::RepoType::WIKI
          sql.add <<-SQL
            SELECT rc.repository_id
              FROM repository_checksums rc
             WHERE rc.repository_id IN :repo_ids
               AND rc.repository_type = :repository_type
          SQL
          wiki_ids = sql.results.flatten

          wiki_values_to_insert = wiki_ids.map do |wiki_id|
            [wiki_id, GitHub::DGit::RepoType::WIKI, network_replica_id, host, "creating", GitHub::SQL::NOW, GitHub::SQL::NOW]
          end

          unless wiki_values_to_insert.empty?
            # Insert the wiki replicas values.
            sql = db.SQL.new \
              network_id: network_id,
              values: GitHub::SQL::ROWS(wiki_values_to_insert)
            sql.add <<-SQL
              INSERT INTO repository_replicas
                          (repository_id, repository_type, network_replica_id, host, checksum, created_at, updated_at)
                   VALUES :values
            SQL
            db.throttle { sql.run }
          end

          # Repair it into existence.  Shazam.
          # Seriously, "network repair" and "network creation" are the same
          # big blob of rsyncs and checksum-checking.
          GitHub::Logger.log(method: "GitHub::Jobs::DgitRepairNetworkReplica.perform!") do
            GitHub::Jobs::DgitRepairNetworkReplica.perform!(network_id, host, GitHub::DGit::CREATING, after_rsync)
          end
        else
          # It's empty.  There's nothing to copy or checksum.
          GitHub::DGit::Maintenance::set_network_state(network_id, host, GitHub::DGit::ACTIVE)
        end

        # check read-weight total
        GitHub::DGit::Maintenance.rebalance_read_weight(network_id, new_host: host)

      # Catch repair errors here, but not creation errors.
      # Set the state to FAILED so one more repair attempt can be made
      # before it just gets destroyed.
      rescue GitHub::DGit::ReplicaRepairError => e
        GitHub::DGit::Maintenance::set_network_state(network_id, host, GitHub::DGit::FAILED)
        raise e
      end

      def self.queue
        :dgit_repairs
      end
    end
  end
end
