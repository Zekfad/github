# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RepositoryUpdateLanguageStats < Job
      def self.active_job_class
        ::RepositoryUpdateLanguageStatsJob
      end

      areas_of_responsibility :languages

      extend GitHub::HashLock

      def self.queue
        :languages
      end

      def self.perform(repo_id)
        if repository = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(repo_id) }
          Failbot.push(repo_id: repo_id)
          repository.analyze_languages
        end
      end
    end
  end
end
