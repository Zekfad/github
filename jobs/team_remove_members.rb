# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::Jobs::TeamRemoveMembers < GitHub::Jobs::Job
  def self.active_job_class
    ::TeamRemoveMembersJob
  end

  areas_of_responsibility :orgs

  def self.queue
    :team_remove_members
  end

  # Perform the long running process of revoking abilities between a team and
  # its members and doing other associated cleanup.
  # This job does not require a Team to exist (it probably has already been
  # destroyed).
  #
  # team_id         - The ID of the team whose members will be removed
  # team_name       - The name of the team
  # org_id          - The ID of organization this team belonged to
  # member_ids      - A list of User IDs representing the membership of this team
  # repo_ids        - A list of Repository IDs associated with this team
  #
  def self.perform_with_retry(team_id, team_name, org_id, member_ids = [], repo_ids = [], legacy_owners = false, opts = {})
    return unless team_id

    org = Organization.find_by_id(org_id)

    team = Team::Membership::PseudoTeam.new(team_id, team_name, org, legacy_owners: legacy_owners)

    memberships = Team::Membership.new(team, member_ids, repo_ids, send_notification: true, team_destroyed: true)
    memberships.remove do |member|
      # Queue a "removed" MembershipEvent for each member.
      # Note: This normally happens via Ability.revoke when a member is removed
      # from a team.
      # We queue the event manually because the team has now been deleted.
      if org.present?
        Hook::Event::MembershipEvent.queue(action: :removed,
                                           member_id: member.id,
                                           member_login: member.login,
                                           team_id: team_id,
                                           team_name: team_name,
                                           organization_id: org_id,
                                           actor_id: GitHub.context[:actor_id])
      end
    end
  end
end
