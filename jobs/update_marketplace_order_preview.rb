# frozen_string_literal: true

module GitHub
  module Jobs
    class UpdateMarketplaceOrderPreview < Job
      def self.active_job_class
        ::UpdateMarketplaceOrderPreviewJob
      end

      areas_of_responsibility :marketplace

      def self.queue
        :marketplace
      end

      # Create or update an order preview/cart for the specified user/listing.
      #
      # It's possible that order preview updates may run out of order as a user changes
      # accounts/plans/etc. - so only update older information.
      def self.perform(user_id, listing_id, account_id, plan_id, quantity, viewed_at)
        ActiveRecord::Base.connected_to(role: :reading) do
          viewer = User.find_by(id: user_id)
          variables = {
            input: {
              "userId" => user_id,
              "marketplaceListingId" => listing_id,
              "accountId" => account_id,
              "marketplaceListingPlanId" => plan_id,
              "quantity" => quantity,
              "viewedAt" => Time.at(viewed_at).iso8601,
            },
          }

          listing = Marketplace::Listing.find_by(id: listing_id)
          listing_plan = listing.listing_plans.find_by(id: plan_id) if listing

          if listing&.publicly_listed? && listing_plan&.published?
            Platform.execute(
              UpdateMarketplaceOrderPreviewMutation,
              target: :internal,
              context: { viewer: viewer },
              variables: variables,
            )
          end
        end
      end

      UpdateMarketplaceOrderPreviewMutation = <<-'GRAPHQL'
        mutation($input: UpdateMarketplaceOrderPreviewInput!) {
          updateMarketplaceOrderPreview(input: $input) {
            __typename
          }
        }
      GRAPHQL
    end
  end
end
