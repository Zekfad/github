# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class ClearTeamMemberships < Job
      def self.active_job_class
        ::ClearTeamMembershipsJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :team_remove_members
      end

      extend GitHub::HashLock

      def self.perform_with_retry(org_id, access_list, options = {})
        new(org_id, access_list, options).perform
      end

      # Handles the task of clearing all Organization membership info.
      #
      # org_id      - Integer ID of the Organization that has been affected.
      # access_list - Array of Integer IDs of dependent objects.  If removing
      #               a User, this is a list of Repository IDs.  If removing a
      #               Repo, this is a list of User IDs.
      # options     - Hash specifying the member.
      #               :user - Integer of the User ID.
      #               :repo - Integer of the Repository ID.
      def initialize(org_id, access_list, options = {})
        @org_id = org_id
        @access_list = Array(access_list)
        @options = options.with_indifferent_access
      end

      def perform
        return unless @options["user"] || @options["repo"]

        @org = Organization.find_by_id @org_id.to_i
        @watched = 0

        if @options["user"] && @user = User.find_by_id(@options["user"])
          # clearing a list of repositories for a specific user
          clear_repositories_for_user(@access_list)
        elsif @options["repo"] && @repo = Repository.find_by_id(@options["repo"])
          # clearing a list of users for a specific repository
          remove_repo_data(@access_list)
        else
          return # nothing to do here.
        end

        GitHub.dogstats.histogram "newsies", @access_list.size, tags: ["action:clear_memberships", "type:count"]
        GitHub.dogstats.histogram "newsies", @watched, tags: ["action:clear_memberships", "type:watched"]
      end

      # Clear a list of repositories for a specific user:
      #
      # * unwatch any repos the user is watching
      # * unassign any issues the user is assigned to
      # * hide the user from the org if the user is still visible and no longer has access
      #
      def clear_repositories_for_user(repository_ids)
        # this is duplicating the logic of Repository#pullable_by, but externally so it can be done in bulk.
        # ignore public repositories, since they remain pullable:
        public_repo_ids = Repository.public_scope.where(id: repository_ids).pluck(:id)

        # get the list of all of the private repos in this list that the user *can* access (via teams or ownership)
        private_repo_ids = repository_ids - public_repo_ids
        accessible_private_repo_ids = @user.associated_repository_ids(including: [:indirect, :owned], repository_ids: private_repo_ids)

        # now we have the full list of the repos the user can pull
        pullable_repo_ids = public_repo_ids + accessible_private_repo_ids

        # and we can see what's left over
        repo_ids_to_clear = repository_ids - pullable_repo_ids
        return if repo_ids_to_clear.empty?

        clear_assigned_issues(repo_ids_to_clear)
        clear_watched_repositories(repo_ids_to_clear)
        clear_starred_repositories(repo_ids_to_clear)
        clear_org_member_details(@org, @user)
      end

      # Clears every team user from the team member's repo.
      #
      # user_ids - Array of Integer User IDs.
      #
      # Returns nothing.
      def remove_repo_data(user_ids)
        # track user ids that cannot pull repo
        user_ids_without_access = []

        User.where(id: user_ids).each do |user|
          next if @repo.pullable_by?(user)

          user_ids_without_access << user.id

          user.unstar @repo
          user.clear_issue_assignments(scope: @repo.issues)
          clear_org_member_details(@org, user)
        end

        @watched += user_ids_without_access.length
        GitHub.newsies.async_delete_all_for_list_and_users(@repo, user_ids_without_access) unless user_ids_without_access.empty?
      end

      def clear_starred_repositories(repo_ids_to_clear)
        @user.starred_repositories.where(id: repo_ids_to_clear).each do |starred_repo|
          @user.unstar starred_repo
        end
      end

      def clear_assigned_issues(repo_ids_to_clear)
        @user.clear_issue_assignments(scope: Issue.where(repository_id: repo_ids_to_clear))
      end

      def clear_watched_repositories(repo_ids_to_clear)
        return if repo_ids_to_clear.empty?

        # don't lookup repositories, as they may no longer exist,
        # and even if they have we still wish to cleanup subscriptions
        repos_to_clear = repo_ids_to_clear.map do |id|
          Repository.new.tap { |r| r.id = id }
        end

        @watched += repos_to_clear.length
        GitHub.newsies.async_delete_all_for_user_and_lists(@user.id, repos_to_clear)
      end

      def clear_org_member_details(org, user)

        begin
          user.reload
        rescue ActiveRecord::RecordNotFound
          return
        end

        return unless org
        return if org.direct_or_team_member?(user)
        org.conceal_member user
        GitHub.newsies.get_and_update_settings(user) do |settings|
          settings.email(org, nil)
        end
      end
    end
  end
end
