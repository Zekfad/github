# frozen_string_literal: true

module GitHub
  module Jobs
    class PullRequestReviewStats < Job
      def self.active_job_class
        ::PullRequestReviewStatsJob
      end

      areas_of_responsibility :pull_requests

      def self.queue
        :pull_request_review_stats
      end

      def self.perform(pull_request_id)
        self.new.perform(pull_request_id)
      end

      def perform(pull_request_id)
        return unless GitHub.octolytics_enabled?
        return unless pull = PullRequest.find_by_id(pull_request_id)
        return unless pull.user
        GitHub.analytics.record([event(pull)])
      end

      private

      def event(pull)
        suggester = PullRequest::SuggestedReviewers.new(pull)
        suggested = suggester.find(excluding: pull.user).map do |suggestion|
          {
            user_id: suggestion.user.id,
            user_login: suggestion.user.login,
            author_score: suggestion.author,
            commenter_score: suggestion.commenter,
            total_score: suggestion.score,
          }
        end

        {
          event_type: "pull_request_reviewer_suggestions",
          measures: {
            suggestions_available: suggested.size,
          },
          dimensions: {
            pull_request_id: pull.id,
            actor_id: pull.user.id,
            actor_login: pull.user.login,
            repository_id: pull.repository.id,
            repository_name: pull.repository.name,
            repository_has_license: !!pull.repository.license,
            repository_public: pull.repository.public?,
            repository_owner_type: pull.repository.owner.class.name.downcase,
            repository_owner_id: pull.repository.owner.id,
            repository_owner_login: pull.repository.owner.login,
          },
          context: {
            suggested: suggested,
          },
        }
      end
    end
  end
end
