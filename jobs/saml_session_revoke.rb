# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class SamlSessionRevoke < Job
      def self.active_job_class
        ::SamlSessionRevokeJob
      end

      schedule interval: 60.seconds

      def self.queue
        :saml_session_revoke
      end

      # Public: Returns true for an Enterprise instances configured with SAML.
      def self.enabled?
        return true if GitHub.private_instance_user_provisioning?
        GitHub.enterprise? && GitHub.auth.saml?
      end

      # Public: Revoke UserSessions that are past the expiry specified by the
      # original authentication response.
      #
      # Returns nothing.
      def self.perform
        return unless enabled?
        new.perform
      end

      def perform
        GitHub.instrument "saml_session_revoke.perform" do
          SAML::Session.expired.each_record do |saml_session|
            saml_session.destroy
          end
        end
      end
    end
  end
end
