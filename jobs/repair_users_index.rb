# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs

    # The purpose of this job is to reconcile the state of Users and
    # Organizations between the database and the search index. We do this by
    # iterating over each User, reading the state of the user from the
    # database and the search index, and then resolving the two based on their
    # `updated_at` timestamps.
    #
    # Each repair job will process 250 users. When it has repaired all the
    # users it will enqueue another job. This process will continue until all
    # users have been repaired.
    #
    # To make this whole process faster, multiple repair jobs can be enqueued.
    # The current offest into the users table is stored in redis. Access to
    # this value is coordinated via a shared mutex. Don't spin up too many
    # repair jobs otherwise you'll kill the database or the search index or
    # both.
    class RepairUsersIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairUsersIndexJob
      end

      areas_of_responsibility :search

      reconcile "user",
        fields: %w[updated_at],
        limit: 250,
        accept: :searchable?

      def self.queue
        :index_bulk
      end
    end
  end
end
