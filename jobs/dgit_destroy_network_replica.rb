# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    class DgitDestroyNetworkReplica < Job
      def self.active_job_class
        ::SpokesDestroyNetworkReplicaJob
      end

      areas_of_responsibility :dgit

      def self.perform(network_id, host, inhibit_repairs = false)
        Failbot.push spec: "network/#{network_id}",
                     app: "github-dgit"

        GitHub::Logger.log_context(job: name.demodulize, spec: "network/#{network_id}") do
          GitHub::Logger.log(method: "perform", host: host) do
            GitHub.dogstats.time("dgit.actions.destroy-network", tags: ["server:#{host}"]) do
              perform!(network_id, host, inhibit_repairs)
            end
          end
        end
      rescue Freno::Throttler::Error
        # Ignore freno errors and rely on retries from our own maintenance scheduler
      rescue GitHub::DGit::ReplicaDestroyNotFoundError
        # Ignore. This is possible due to races.
      end

      def self.perform!(network_id, host, inhibit_repairs = false)
        replica = GitHub::DGit::Routing.network_replica_for_host(network_id, host)
        raise GitHub::DGit::ReplicaDestroyNotFoundError, "Network #{network_id} on #{host} not found" unless replica

        return unless GitHub::DGit::Maintenance.set_network_state(network_id, host, GitHub::DGit::DESTROYING)

        path = GitHub::Routing.nw_storage_path(network_id: network_id)
        rpc = replica.to_route(path).build_maint_rpc
        path = GitHub::DGit.dev_route(path, host) if Rails.development? || Rails.test?
        pattern = %r{^#{GitHub.repository_root}/(?:dgit\d{1,2}/)?[0-9a-f]/nw/[0-9a-f]{2}/[0-9a-f]{2}/[0-9a-f]{2}/\d+$}
        raise GitHub::DGit::ReplicaDestroyError, "Refusing to delete unexpected path #{path.inspect}" unless path =~ pattern

        GitHub.stats.increment "dgit.#{host}.actions.destroy" if GitHub.enterprise?
        GitHub.dogstats.increment "dgit", tags: ["host:#{host}", "action:destroy"]

        begin
          if replica.online?
            GitHub::Logger.log(fn: "destroy_network_replica", path: path) do
              rpc.destroy_network_replica(path)
            end
          end
        rescue ::GitRPC::ConnectionError, SocketError => e
        end

        # Order for these next three steps is important.
        # - Deleting the network_replicas row should be last, because its absence
        #   is what indicates that the destroy succeeded.  If it's still there as
        #   state=DESTROYING, then this operation will get retryed as needed.
        # - Schedule the creation event before deleting the network_replicas
        #   row, so that the creation event can avoid the host with to-be-deleted
        #   replica when picking a host on which to place the to-be-created
        #   replica.

        # Get a list of repository IDs for the network at hand.
        repo_ids = GitHub::DGit::Util.repo_ids_for(network_id)

        db = GitHub::DGit::DB.for_network_id(network_id)

        unless repo_ids.empty?
          repo_ids.each_slice(100) do |batch|
            # Delete the rows from repository_replicas.
            sql = db.SQL.new \
              network_id: network_id,
              repo_ids: batch,
              host: host,
              types: GitHub::DGit::RepoType::REPO_ISH
            sql.add <<-SQL
              DELETE FROM repository_replicas
                    WHERE repository_id IN :repo_ids
                      AND host = :host
                      AND repository_type IN :types
            SQL
            db.throttle { sql.run }
          end
        end

        # Resolve and schedule (but do not complete synchronously) the creation
        # of a replacement replica, if needed. spokes-sweeper handles this for dotcom
        begin
          if !inhibit_repairs && GitHub.enterprise?
            GitHub::DGit::Maintenance.fix_bad_network_replica_counts(only_network_id: network_id)
          end
        ensure
          # Delete the row from network_replicas.  This is the final indicator of success.
          db.throttle do
            GitHub::DGit::Maintenance.delete_network_replica_for_host(network_id, host)
          end
        end

        # check read-weight total
        GitHub::DGit::Maintenance.rebalance_read_weight(network_id)
      end

      def self.queue
        :dgit_repairs
      end
    end
  end
end
