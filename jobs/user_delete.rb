# frozen_string_literal: true

module GitHub
  module Jobs
    class UserDelete < Job
      def self.active_job_class
        ::UserDeleteJob
      end

      areas_of_responsibility :unassigned

      def self.queue
        :user_delete
      end

      def self.perform_with_retry(user_id, login, options = {})
        user = User.find_by(id: user_id)
        return unless user

        Failbot.push(user: login, user_id: user_id)

        ApplicationRecord::Domain::Users.transaction do
          ApplicationRecord::Domain::Repositories.transaction do
            user.destroy!
          end
        end
      end
    end
  end
end
