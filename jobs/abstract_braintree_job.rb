# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::Jobs::AbstractBraintreeJob < GitHub::Jobs::Job
  areas_of_responsibility :gitcoin

  def self.active_job_class
    ::AbstractBraintreeJob
  end

  # Any errors that happen during this job will be reported to Failbot, but
  # only those listed below will automatically schedule the job to be retried.
  RETRYABLE_ERRORS = [
    ::Billing::Zuora::SynchronizationError,
    Braintree::DownForMaintenanceError,
    Braintree::SSLCertificateError,
    Braintree::ServerError,
    Errno::ECONNREFUSED,
    Errno::ECONNRESET,
    Errno::ETIMEDOUT,
    Faraday::ConnectionFailed,
    Faraday::TimeoutError,
    GitHub::Restraint::UnableToLock,
    Net::HTTPRequestTimeOut,
    Net::ReadTimeout,
    Zuorest::HttpError,
  ]

  # How many of these jobs are allowed to run at the same time?
  RUNNING_JOBS = 1

  # How long a running (or crashed) job is allowed to hold onto a lock, in
  # seconds, before it expires and another one takes over.
  JOB_LOCK_TTL = 3.minutes

  def self.queue; :billing; end

  attr_reader :retry_options

  private

  def retry_args
    raise NotImplementedError
  end

  def lock_key
    raise NotImplementedError
  end

  def perform_with_lock_and_retry
    perform_with_retry do
      with_lock { yield }
    end
  end

  def perform_with_retry
    yield
  rescue => error
    if error.class == Braintree::ValidationsFailed
      handle_validation_subscription_error(error)
    elsif RETRYABLE_ERRORS.include?(error.class)
      LegacyApplicationJob.retry_later self.class.active_job_class, retry_args, retry_options
    else
      Failbot.report(error)
    end
  end

  def with_lock
    restraint.lock!(lock_key, RUNNING_JOBS, JOB_LOCK_TTL) { yield }
  end

  def restraint
    GitHub::Restraint.new
  end

  # Private: handle Braintree::ValidationsFailed exceptions.
  #
  def handle_validation_subscription_error(error)
    case subscription_error_code_for(error)
    when Braintree::ErrorCodes::Subscription::CannotEditCanceledSubscription
      # NB: Occasionally we may run into a race condition where the
      # synchronizer is running when the subscription has already been
      # canceled on Braintree's side. We can ignore the sync then.
      nil
    else
      Failbot.report(error)
    end
  end

  # Private: Returns the subscription error code.
  # a 'cannot edit canceled subscription' error.
  #
  # error - a BraintreeSubscription:ValidationsFailed error
  #
  # Returns an Integer of the error code off subscription.
  def subscription_error_code_for(error)
    sub_error = error.error_result.try(:errors).try(:for, :subscription)
    sub_error && (sub_error.first || sub_error.deep_errors.first).code
  end
end
