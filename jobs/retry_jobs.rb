# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    class RetryJobs < Job
      def self.active_job_class
        ::RetryJobsJob
      end

      areas_of_responsibility :background_jobs

      RESTRAINT_KEY = "retry_jobs"

      # This many jobs are allowed to run in parallel.
      RESTRAINT_CONCURRENCY = 25

      # Allow instances of RetryJobs to run for this many seconds before
      # another is allowed to proceed. The total runtime is assumed to be less
      # than 30 seconds, but this will limit concurrency in cases where
      # processing becomes slow enough that there are a lot of these jobs
      # trying to run.
      RESTRAINT_TTL = 60

      # TimedResque allows a granularity of 1 second for defining when a job
      # should retry again. This makes sure a RetryJobs job is always available
      # and running, every second, to meet that demand.
      schedule interval: 1.second

      def self.queue
        :retry_jobs
      end

      def self.enabled?
        true
      end

      # How many seconds can pass before another one of these is allowed to be
      # queued? This isn't a guarantee of concurrency control alone,
      # particularly if the queue is paused, but will prevent many duplicates
      # from being queued and trying to run all at once.
      def self.lock_timeout
        RESTRAINT_TTL
      end

      def self.perform
        scheduled = queued = delay = nil
        restraint.lock!(RESTRAINT_KEY, RESTRAINT_CONCURRENCY, RESTRAINT_TTL) do
          scheduled = GitHub.timed_resque.count
          # The retryable job requeuing gets half the restraint TTL to finish
          # its work as a wide buffer in case something slows everything down
          # such as decoding/encoding large job payloads.
          queued = GitHub.timed_resque.retry!(RESTRAINT_TTL/2)
          delay = GitHub.timed_resque.delay
        end

        GitHub.dogstats.gauge("job.retry.scheduled", scheduled)
        GitHub.dogstats.count("job.retry.queued", queued)
        GitHub.dogstats.gauge("job.retry.delay", delay)
      rescue Restraint::UnableToLock
        # it's fine, another one of these is already running
      end

      # Internal: a concurrency restraint using resque redis
      def self.restraint
        @restraint ||= GitHub::Restraint.new
      end

    end
  end
end
