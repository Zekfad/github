# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/delegate"
require "github/dgit/error"
require "github/dgit/maintenance"

module GitHub
  module Jobs
    # Using `rsync`, bring a given gist replica back into compliance with
    # other replicas for that gist.
    class DgitRepairGistReplica < Job
      def self.active_job_class
        ::SpokesRepairGistReplicaJob
      end

      areas_of_responsibility :dgit

      # Don't sync locks or git state with rsync.  Git state syncs better with git.
      EXCLUDE_RSYNC = [
        "/dgit-state*",
        "/packed-refs.new",
        "*.lock",
        "/.backup_lock",
        "/objects/info/alternates+",
      ]
      REPAIR_ATTEMPTS = 4

      def self.perform(gist_id, host)
        Failbot.push spec: "gist/#{gist_id}",
                     app: "github-dgit"

        GitHub::Logger.log_context(job: name.demodulize, spec: "gist/#{gist_id}", host: host) do
          GitHub::Logger.log(method: "perform!") do
            GitHub.dogstats.time("dgit.actions.repair-gist", tags: ["server:#{host}"]) do
              perform!(gist_id, host, false)
            end
          end
        end
      rescue Freno::Throttler::Error
        # Ignore freno errors and rely on retries from our own maintenance scheduler
      end

      def self.gist_replica_for_host(gist_id, host)
        # Retry getting a network replica a few times, to make sure
        # the database has caught up.
        gr = nil
        4.times do
          gr = GitHub::DGit::Routing.gist_replica_for_host(gist_id, host)
          break if gr
          GitHub.dogstats.increment("repair_gist_replica.wait_for_replication.retry")
          sleep 5 unless Rails.env.test?
        end
        raise GitHub::DGit::ReplicaRepairError, "Replica gist #{gist_id} on #{host} not found" unless gr
        gr
      end

      def self.perform!(gist_id, host, is_creating = false, via: nil)
        gr = gist_replica_for_host(gist_id, host)
        desc = (is_creating ? "Create" : "Repair")
        gist = Gist.find_by_id(gist_id)

        if GitHub::DGit::Routing.gist_checksum(gist_id) == gr.checksum && gr.active?
          GitHub::Logger.log no_repair_needed: true,
              checksum: gr.checksum,
              message: "skipping #{desc}: replica ok"
          return
        end

        if (is_creating && (gr.state != GitHub::DGit::CREATING))
          GitHub::Logger.log invalid_replica_state: true,
              replica_state: GitHub::DGit::STATES[gr.state],
              message: "#{desc} failed: expected state #{GitHub::DGit::STATES[GitHub::DGit::CREATING]}"
          return
        end

        # Obtain a read route for a non-evacuating host to use for a repair.
        # If repairs are attempted from a donor host that is currently being
        # evacuated, the underlying disk contents may be removed out from
        # under us mid-repair.  Try to avoid that.
        begin
          read_route = nil
          gist.dgit_read_routes.each do |r|
            if ((r.host != host) && !GitHub::DGit::Util.is_evacuating?(r.host))
              read_route = r
              break
            end
          end

          # Couldn't find a route on a non-evacuating host:
          # fall back to using the first read route.
          if read_route.nil?
            read_route = gist.dgit_read_routes.first
          end
        rescue GitHub::DGit::UnroutedError => e
          GitHub::Logger.log_exception({method: "dgit_read_routes"}, e)
          return
        end

        read_route_resolved = read_route.resolved_host
        read_route_path = read_route.path

        if read_route.original_host == host
          raise GitHub::DGit::ReplicaRepairError,
                "Cannot #{desc.downcase} gist #{gist_id} on #{host} from itself"
        end

        #
        # First recompute checksums for the case that the replica already exists
        # and has not transitioned to FAILED.  If the refreshed replica checksum
        # matches the record in the database, ensure this replica is toggled
        # ACTIVE and return early.
        #
        unless (is_creating || (gr.state == GitHub::DGit::FAILED))
          host_checksums = GitHub::DGit::Maintenance.recompute_gist_checksums(
                             gist, read_route.original_host, host_to_activate: host)
          if host_checksums[:repo] == host_checksums[host]
            GitHub::Logger.log message: "DGit completed #{desc.downcase} by refreshing checkums", no_repair_needed: true
            return true
          end
        end

        #
        # Refreshing checksums alone didn't work, or, this is a new creation:
        # actually perform a repair.
        #
        tell_statsd = !is_creating
        GitHub.stats.increment "dgit.#{host}.actions.repair-gist" if tell_statsd && GitHub.enterprise?
        GitHub.dogstats.increment "dgit", tags: ["action:repair_gist", "host:#{host}"]
        GitHub::Logger.log(message: "start #{desc.downcase}",
                           is_creating: is_creating,
                           source_host: read_route.original_host,
                           via: via)
        GitHub::DGit::Maintenance.set_gist_state(gist_id, host, GitHub::DGit::REPAIRING) unless is_creating

        rpc = gr.to_route(gist.original_shard_path).build_maint_rpc
        GitHub::DGit::Maintenance.backup_before_repair_with_rpc(rpc, self) unless is_creating

        (1..REPAIR_ATTEMPTS).each do |repair_attempt|
          rsync_url = if Rails.development? || Rails.test?
            read_route_path + "/"
          else
            "git@#{read_route_resolved}:#{read_route_path}/"
          end

          if (is_creating || (gr.state == GitHub::DGit::FAILED))
            begin
              rpc.ensure_dir(rpc.backend.path)
            rescue GitRPC::CommandFailed => e
              GitHub::Logger.log_exception({method: "ensure_dir"}, e)
              raise GitHub::DGit::ReplicaRepairError, "Could not mkdir: #{e}"
            end
          else
            raise ::GitRPC::InvalidRepository unless rpc.exist?
          end

          rsync_opts = {archive: true, verbose: true, hard_links: true,
                        delete: true, stats: true, exclude: EXCLUDE_RSYNC}

          # Use --checksum to find files with matching mtime and size.
          # Only necessary on the first pass -- passes 2+ only exist to
          # pick up files that have changed, which will have new mtimes.
          rsync_opts[:checksum] = true if repair_attempt == 1

          GitHub::Logger.log message: "rsync beginning."
          begin
            res = rpc.rsync(rsync_url, ".", rsync_opts)
          rescue GitRPC::Timeout => e
            # A client-side timeout may leave rsync running. We don't know
            # what state this replica is in and it will be changing. Best we
            # can do leave it "REPAIRING" and try again after the sweeper picks
            # up that this repair has been running too long and destroy it.
            GitHub::Logger.log_exception({method: "rsync"}, e)
            raise GitHub::DGit::ReplicaRepairFatalError, "Timeout waiting for rsync"
          end

          unless res["ok"]
            raise GitHub::DGit::ReplicaRepairError, "Could not rsync"
          end

          if repair_attempt == 1
            GitHub::Logger.log message: "rsync succeeded: #{res["out"].lines.size} lines of output."
          else
            GitHub::Logger.log stdout: res["out"], stderr: res["err"], message: "rsync succeeded."
          end

          next unless (res["out"] =~ /Number of (?:created files|files transferred): 0/) && (res["out"] =~ /Total transferred file size: 0 bytes/)

          GitHub::DGit::DB.for_gist_id(gist.id).throttle do
            host_checksums = GitHub::DGit::Maintenance.recompute_gist_checksums(
                               gist, read_route.original_host, host_to_activate: host)
          end
          ok = (host_checksums[:repo] == host_checksums[host])
          if !ok
            GitHub::Logger.log message: "mismatched checksum on #{host}: want " \
                         "#{host_checksums[:repo]} but got #{host_checksums[host]}"
          else
            GitHub::Logger.log message: "DGit completed #{desc.downcase}. Returning OK"
            return true
          end
        end

        GitHub::Logger.log message: "#{desc} unsuccessful after #{REPAIR_ATTEMPTS} attempts"
        GitHub::DGit::Maintenance::set_gist_state(gist_id, host, GitHub::DGit::FAILED)
        nil
      rescue GitHub::DGit::ReplicaRepairError, ::GitRPC::Error => e
        GitHub::DGit::Maintenance::set_gist_state(gist_id, host, GitHub::DGit::FAILED)
        raise e
      end

      def self.queue
        :dgit_repairs
      end
    end
  end
end
