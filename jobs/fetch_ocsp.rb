# frozen_string_literal: true

module GitHub::Jobs
  class FetchOcsp < Job
    def self.active_job_class
      ::FetchOcspJob
    end

    areas_of_responsibility :web_flow

    extend GitHub::HashLock

    def self.queue
      GitSigning::QUEUE
    end

    def self.perform(b64_subject, b64_issuer, *b64_untrusted)
      subject = decode_cert(b64_subject)
      issuer  = decode_cert(b64_issuer)
      untrusted = b64_untrusted.map { |u| decode_cert(u) }

      GitHub::OCSP.check_sync(subject, issuer, untrusted)
    end

    def self.decode_cert(b64)
      OpenSSL::X509::Certificate.new(Base64.decode64(b64))
    end
  end
end
