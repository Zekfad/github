# frozen_string_literal: true

module GitHub
  module Jobs
    class UpdateSiteStats < Job
      def self.active_job_class
        ::UpdateSiteStatsJob
      end

      areas_of_responsibility :background_jobs

      schedule interval: 24.hours

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.queue
        :site_stats
      end

      def self.perform
        update_users_count
        update_repositories_count
        update_issues_count
        update_paid_teams_count
      rescue UnavailableError => e
        Failbot.report(e)
        raise if Rails.test? # swallow errors in prod, surface in test environment
      end

      def self.update_users_count
        GitHub.kv.set(GitHub::MarketingStats::USER_MILLIONS_KEY, users_count.to_s)
      end

      def self.update_repositories_count
        GitHub.kv.set(GitHub::MarketingStats::REPO_MILLIONS_KEY, repositories_count.to_s)
      end

      def self.update_issues_count
        GitHub.kv.set(GitHub::MarketingStats::ISSUE_MILLIONS_KEY, issues_count.to_s)
      end

      def self.update_paid_teams_count
        GitHub.kv.set(GitHub::MarketingStats::PAID_ORGS_COUNT_KEY, paid_orgs_count.to_s)
      end

      def self.users_count
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          User.not_spammy.count
        end
      end

      def self.repositories_count
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          Repository.not_spammy.count
        end
      end

      def self.issues_count
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          Issue.not_spammy.count
        end
      end

      def self.paid_orgs_count
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          Organization.where(plan: GitHub::Plan.all_non_free_org_plans.map(&:name)).count
        end
      end
    end
  end
end
