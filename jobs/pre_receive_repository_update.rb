# rubocop:disable Style/FrozenStringLiteralComment

require "failbot"
require "posix/spawn"

module GitHub
  module Jobs

    # Enqueued after a new hook is configured with a given repository
    # or the given repository receives a push and we need to update the
    # bits on disk for pre-receive hooks.
    #
    #   repository_id - The id if the repository containing pre-receive scripts
    #   url           - URL of where to retrieve this repository (internal URL)
    #
    class PreReceiveRepositoryUpdate < Job
      def self.active_job_class
        ::PreReceiveRepositoryUpdateJob
      end

      areas_of_responsibility :enterprise_only

      attr_reader :repository_id, :url

      class Error < StandardError; end

      def initialize(repository_id, url)
        @repository_id = repository_id
        @url = url
      end

      # Returns the Repository object associated with the push.
      def repository
        @repository ||= Repository.find_by_id(repository_id)
      end

      def perform
        Failbot.push(
          job: self.class.name,
          repo: repository&.readonly_name_with_owner,
          url: url,
        )

        return unless repository

        result = nil
        if Rails.development? || Rails.test?
          hook_repo_dir = File.join(GitHub.custom_hooks_dir, "repos")
          repo_path = File.join(hook_repo_dir, repository_id.to_s)
          if Dir.exist?(repo_path)
            result = POSIX::Spawn::Child.new("git", "-C", repo_path, "pull")
          else
            FileUtils.mkdir_p(hook_repo_dir)
            result = POSIX::Spawn::Child.new("git", "clone", url, repo_path)
          end
        else
          result = POSIX::Spawn::Child.new("/usr/local/share/enterprise/ghe-hook-repo-update", repository_id.to_s, url)
        end

        unless result.status.exitstatus == 0
          raise Error, "There's been a problem updating the repository:
exit status: #{result.status.exitstatus}
stdout: #{result.out}
stderr: #{result.err}"
        end
      end

      def self.queue
        :pre_receive_repository_update
      end

      def self.perform(*args)
        new(*args).perform
      end
    end
  end
end
