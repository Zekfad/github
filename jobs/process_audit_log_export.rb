# frozen_string_literal: true

module GitHub
  module Jobs
    class ProcessAuditLogExport < Job
      def self.active_job_class
        ::ProcessAuditLogExportJob
      end

      areas_of_responsibility :audit_log

      # Using the critical queue because users are waiting for results
      def self.queue
        :critical
      end

      def self.perform(export_id, audit_event = nil)
        export = AuditLogExport.find(export_id)
        status = JobStatus.find(export.token)
        return unless export && status

        status.track do
          export.process
        end
      end
    end
  end
end
