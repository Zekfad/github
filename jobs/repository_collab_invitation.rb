# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RepositoryCollabInvitation < Job
      def self.active_job_class
        ::RepositoryCollabInvitationJob
      end

      areas_of_responsibility :repositories

      def self.queue
        :invite_collaborator_to_repository
      end

      def self.perform(invitation_id)
        if invitation = RepositoryInvitation.find_by_id(invitation_id)
          RepositoryMailer.collab_invited(invitation).deliver_now
          summary = GitHub.newsies.web.rollup_summary_from_repository_invitation(invitation)
          GitHub.newsies.trigger(
            invitation,
            recipient_ids: [invitation.invitee.id],
            reason: "invitation",
            event_time: invitation.created_at,
          )
        end
      end
    end
  end
end
