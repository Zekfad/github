# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    # Checks the status of asynchronous batch jobs sent to Mailchimp, and re-enqueues itself with
    # an exponential backoff until the job is no longer pending
    class MailchimpBatchStatus < Job
      def self.active_job_class
        ::MailchimpBatchStatusJob
      end

      areas_of_responsibility :unassigned

      def self.queue
        :mailchimp
      end

      def self.perform(*args)
        job = new(*args)
        job.perform
        job
      end

      def initialize(batch_id, options = {})
        @batch_id   = batch_id
        @options    = options.with_indifferent_access
      end

      def perform
        with_mailchimp_retries do
          batch = GitHub::Mailchimp.find_batch(@batch_id)
          if batch["status"] == "finished"
            GitHub.dogstats.increment("mailchimp", tags: ["job:batch_status", "type:finished"])

            return if batch["errored_operations"] == 0
            raise GitHub::Mailchimp::BatchError.new(batch), error_message
          else
            retry_job
          end
        end
      end

      # Public: When Mailchimp API calls fail due to 5xx server errors,
      # retry the call up to 25 times. Otherwise, raise an exception
      # and fail the job.
      def with_mailchimp_retries(&block)
        yield
      rescue GitHub::Mailchimp::Error => boom
        if GitHub::Mailchimp::ServerErrorStatuses.include?(boom.status_code)
          retry_job
        else
          raise
        end
      end

      def retry_job
        GitHub.dogstats.increment "mailchimp", tags: ["job:batch_status", "try:retry"]
        retries = @options["retries"] || 3
        LegacyApplicationJob.retry_later(self.class.active_job_class, [@batch_id, @options], retries: retries)
      end

      private

      def error_message
        "There were errored operations among the Mailchimp batched operations."
      end
    end
  end
end
