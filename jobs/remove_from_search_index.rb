# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs

    # This job will remove a document from the ElasticSearch index. The
    # document to remove is identified by the 'type' and the 'id' of the
    # document. The 'type' is the name of the adapter to use when removing the
    # document. The 'id' is the ID of the database record.
    #
    class RemoveFromSearchIndex < Job
      include GitHub::ServiceMapping

      def self.active_job_class
        ::RemoveFromSearchIndexJob
      end

      extend GitHub::HashLock

      # Public: child class to encapsulate stats calls so
      # we can test the publishing via MemoryDogstatsD
      class PerformStats
        def initialize(job_class, resolved_service, adapter_class)
          @tags = {
            job: job_class.name.demodulize.underscore.downcase,
            content_type: adapter_class.name.demodulize.underscore.downcase,
            catalog_service: resolved_service,
          }
        end

        def timing_since(key, start_time)
          GitHub.dogstats.timing_since(key, start_time, tags: @tags)
        end
      end

      # Public: Remove a document from the ElastSearch index.
      #
      # type - The adapter name as a String
      # id   - The numeric ID of the database record
      # args - An optional index name as a String, and an options hash
      #        including Hydro params only available at job enqueue time
      #        (:request_id, :slicing, :routing, etc...)
      #
      # Returns the Hash response from the ElasticSearch server.
      #
      def self.perform(type, id, *args)
        new(type, id, *args).perform
      end

      attr_reader :id, :type, :opts, :args

      def initialize(type, id, *args)
        @type = type
        @id = id
        @args = args
        @opts = @args.extract_options!
      end

      def perform
        if type == "code" && !Search::ClusterStatus.code_search_indexing_enabled?
          return nil
        end

        hydrated_at = Time.now.utc
        adapter_class = Elastomer.env.lookup_adapter(type)
        adapter = adapter_class.create(id, *args)
        adapter.document_type = type if adapter.respond_to?(:document_type=)
        statter = PerformStats.new(self.class, logical_service, adapter_class)

        Elastomer.remove_from_search_index(adapter, opts)
        if opts["pushed_at"].present?
          statter.dogstats.timing_since("search.indexing.updated_at_to_indexed_at", options["pushed_at"])
        end

      rescue Elastomer::ModelMissing
        nil  # these things happen
      end

      def logical_service
          if type.downcase == "code"
            "#{::GitHub::ServiceMapping::SERVICE_PREFIX}/code_search_indexing"
          else
            "#{::GitHub::ServiceMapping::SERVICE_PREFIX}/search_muddle"
          end
      end

      # Public: The queue from which this job processor will request work.
      #
      # Returns the queue name as a Symbol
      #
      def self.queue
        :index_high
      end

    end
  end
end
