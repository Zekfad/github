# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class RemoveOutsideCollaboratorFromOrganization < Job
      def self.active_job_class
        ::RemoveOutsideCollaboratorFromOrganizationJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :remove_outside_collaborator_from_organization
      end

      def self.perform(user_id, organization_id, options = {})
        new(user_id, organization_id, options).perform
      end

      def initialize(user_id, organization_id, options = {})
        @user_id         = user_id
        @organization_id = organization_id
        @reason          = options["reason"] || options[:reason]
      end

      def perform
        return unless user = User.find(@user_id)
        return unless org = Organization.find(@organization_id)

        org.remove_outside_collaborator!(user, reason: @reason)
      end
    end
  end
end
