# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class UpdateStoragePartitions < Job
      def self.active_job_class
        ::UpdateStoragePartitionsJob
      end

      areas_of_responsibility :data_infrastructure

      schedule interval: 15.minutes

      def self.queue
        :storage_cluster
      end

      def self.enabled?
        GitHub.storage_cluster_enabled?
      end

      def self.perform
        errors = []

        GitHub::Storage::Allocator.all_hosts_with_partitions.each do |host, partitions|
          ok, stats = GitHub::Storage::Client.stats(host, partitions)
          if !ok
            errors << "host-error: #{host}, #{stats["message"]}"
            next
          end

          stats["partitions"].each do |partition, v|
            if v["error"].present?
              errors << "partition-error: #{host}, #{partition}, #{v["error"]}"
              next
            end

            unless GitHub::Storage::Allocator.update_partition_usage(host, partition, v["disk_free"], v["disk_used"])
              errors << "update-error: #{host}, #{partition}"
            end
          end
        end

        if errors.present?
          GitHub::Logger.log(method: __method__,
                             errors: errors)
          raise "Errors present"
        end
      end
    end
  end
end
