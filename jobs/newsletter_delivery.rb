# frozen_string_literal: true

module GitHub
  module Jobs
    class NewsletterDelivery < Job
      def self.active_job_class
        ::NewsletterDeliveryJob
      end

      areas_of_responsibility :explore

      schedule interval: 1.hour

      def self.queue
        :newsletter_delivery
      end

      def self.enabled?
        !GitHub.enterprise?
      end

      def self.perform
        ActiveRecord::Base.connected_to(role: :reading) do
          NewsletterSubscription.start_delivery(type: "explore")
          NewsletterSubscription.start_delivery(type: "vulnerability")
        end
      end
    end
  end
end
