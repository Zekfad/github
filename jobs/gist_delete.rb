# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    # Moves a Gist record and associated records into archive tables.
    #
    # All aspects of this job must be idempotent or at least retryable. If
    # requeueing the job fails repeatedly, the logic must be corrected to allow
    # the job to complete.
    class GistDelete < Job
      def self.active_job_class
        ::GistDeleteJob
      end

      areas_of_responsibility :gist

      extend GitHub::HashLock

      def self.queue
        :archive_restore
      end

      retry_on_error Freno::Throttler::CircuitOpen, Freno::Throttler::WaitedTooLong

      def self.perform_with_retry(gist_id, repo_path, options = {})
        Rails.logger.info("GitHub::Jobs::GistDelete %p (new)" % [[gist_id, repo_path]])

        # if the gist record doesn't exist we assume all work has been
        # completed.
        gist = Gist.find_by_id(gist_id)
        return if gist.nil?

        # if the gist isn't marked as deleted, that's pretty troubling;
        # bail out instead of assuming this really should be deleted.
        if gist.active?
          fail "Attempt to archive and purge gist not marked for deletion"
        end

        gist.archive
      end

      # All GistDelete jobs are locked by their gist id. No attempt
      # will be made to enqueue a job that's already running and the lock is
      # checked before running the job.
      def self.lock(gist_id, repo_path, options = {})
        gist_id
      end
    end
  end
end
