# frozen_string_literal: true

module GitHub
  module Jobs
    class ClearAbilities < Job
      def self.active_job_class
        ::ClearAbilitiesJob
      end

      def self.queue
        :clear_abilities
      end

      # Perform a throttled deletion of abilities referencing any of the
      # specified abilities through the indirect grant ancestry information in
      # parent_id and/or grandparent_id
      #
      # ids - IDs of the abilities whose dependent abilities we want to delete.
      #
      def self.perform_with_retry(ability_id, ability_type, options = {})
        new(ability_id, ability_type).perform
      end

      attr_reader :ability_id, :ability_type

      def initialize(ability_id, ability_type)
        @ability_id = ability_id
        @ability_type = ability_type
      end

      def perform
        Ability.clear!(ability_id, ability_type)
      end
    end
  end
end
