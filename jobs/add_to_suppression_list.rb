# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    class AddToSuppressionList < Job
      def self.active_job_class
        ::AddToSuppressionListJob
      end

      areas_of_responsibility :user_growth

      def self.queue
        :add_to_suppression_list
      end

      # Add the user to the suppression list.
      #
      # user_id - The integer user id.
      #
      # Returns nothing.
      def self.perform(user_id)
        user = User.find_by_id(user_id)
        return unless user

        GitHub.dogstats.time "suppression_list", tags: ["action:add_user", "via:job"] do
          SuppressionList.add_user(user)
        end
      end
    end
  end
end
