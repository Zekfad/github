# rubocop:disable Style/FrozenStringLiteralComment

# Public: Use this job to call GitHub.newsies.ignore_list asynchronously.
#
# Example:
#
#  > user = User.find_by_login("jonmagic")
#  > repo = Repository.with_name_with_owner("github/linguist")
#  > response = GitHub.newsies.ignore_list(user, repo)
#  > if response.failed?
#  >   IgnoreListNotificationsJob.enqueue(user, repo)
#  > end
#
module GitHub
  module Jobs
    class IgnoreListNotifications < Job
      def self.active_job_class
        ::IgnoreListNotificationsJob
      end

      areas_of_responsibility :notifications

      def self.queue
        :notifications
      end

      def self.enqueue(user, repository)
        IgnoreListNotificationsJob.perform_later(user_id: user.id, repository_id: repository.id)
      end

      def self.perform_with_retry(options)
        options = options.with_indifferent_access

        user_id = options.fetch("user_id")
        repository_id = options.fetch("repository_id")
        user, repository = nil
        ActiveRecord::Base.connected_to(role: :reading) do
          user = User.find_by_id(user_id)
          repository = Repository.find_by_id(repository_id)
        end

        return unless user.present? && repository.present?

        response = Newsies::ListSubscription.throttle { GitHub.newsies.ignore_list(user, repository) }
        if response.failed?
          retries = options["retries"].to_i
          LegacyApplicationJob.retry_later(self.active_job_class, [options], retries: retries)
        end
      end
    end
  end
end
