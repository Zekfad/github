# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

module GitHub
  module Jobs
    class RepositoryRestore < Job
      def self.active_job_class
        ::RepositoryRestoreJob
      end

      include Scientist
      extend GitHub::HashLock

      areas_of_responsibility :repositories

      COMPLETED_MESSAGE = "Done!".freeze
      FAILED_MESSAGE = "Unable to complete this job...".freeze
      CACHE_EXPIRES = 1.hour

      class Error < StandardError; end

      def self.queue
        :archive_restore
      end

      def self.perform(repo_id, actor_id = nil)
        new(repo_id, actor_id).perform
      end

      def initialize(repo_id, actor_id = nil)
        @repo_id = repo_id.to_i
        @actor = actor_id && User.find(actor_id)
      end

      def perform
        record_job_state("started")

        unless message.nil?
          record_job_state("failed", "message_not_nil")
          return nil
        end
        self.message = default_message

        # Find the archived record
        begin
          archived = Archived::Repository.find(@repo_id)
        rescue ActiveRecord::RecordNotFound => exception
          # Ensure archived record exists
          record_job_state("failed", "invalid_repo")
          self.message = "Invalid deleted repository."
          return false
        end

        # Validate that archived record is ok to restore
        unless archived.valid?(:restore)
          if GitHub.rails_6_0?
            error_key = archived.errors.keys.first
          else
            error_key = archived.errors.attribute_names.first
          end

          record_job_state("failed", error_key)
          self.message = archived.errors.messages[error_key].first
          return false
        end

        self.message = "Restoring DB data..."
        @repo = Repository.restore(@repo_id, @actor)

        if @repo.present?
          self.message = COMPLETED_MESSAGE

          @repo.enqueue_analyze_language_breakdown

          self.message = default_message
          record_job_state("success")

          return true
        else
          self.message = FAILED_MESSAGE
          record_job_state("failed", "repo_not_returned")

          return false
        end
      rescue Error => exception
        self.message = FAILED_MESSAGE
        record_job_state("failed", exception.class.name)
        false
      rescue => exception
        self.message = FAILED_MESSAGE
        record_job_state("failed", exception.class.name)
        raise
      end

      # Public: Gets the HTTP status for this job.
      #
      # message - Optional String message.  If given, check for an ellipsis
      # ("..."), which indicates that the job is still running.  Final messages
      # or errors end in just a period.
      #
      # Return a Symbol status for Rails, like :ok.
      def status(message = nil)
        return :accepted if message.to_s["..."]
        locked? ? :accepted : :ok
      end

      # Public: Gets the current message describing the state of this job.
      #
      # Returns a String.
      def message
        GitHub.kv.get(message_key).value { "Status unavailable" }
      end

      # Public: Sets the current message describing the state of this job.
      #
      # msg - A String message.
      #
      # Returns nothing.
      def message=(msg)
        GitHub.kv.set(message_key, msg, expires: CACHE_EXPIRES.from_now)
      rescue GitHub::KV::UnavailableError
        # Noop if KV is unavailable. We don't want to hold up performing the
        # rest of the job.
      end

      # Public: Clears the message and lock only if the message is not pending.
      # Pending messages have an ellipsis in them ("...").
      #
      # Returns true if the message was reset, or false.
      def reset_message_if_finished
        return false if message.nil?
        return false if message["..."]
        reset_message
        self.class.clear_lock(@repo_id)
        true
      end

      def reset_message
        ActiveRecord::Base.connected_to(role: :writing) do
          GitHub.kv.del(message_key)
        end
      rescue GitHub::KV::UnavailableError
        # Noop, if KV is unavailable. Rely on expiration to clean up the flag.
      end

      def locked?
        self.class.locked?(@repo_id)
      end

      def default_message
        if status == :ok
          repo = Repository.find_by_id(@repo_id)
          return repo ? COMPLETED_MESSAGE : "Finished?  No Repository was found though..."
        end

        "Queued..."
      end

      def safe_message
        message || default_message
      end

      def message_key
        @message_key ||= "repo-restore:#{@repo_id}:message"
      end

      def record_job_state(state, error = "")
        tags = ["state:#{state}", "initiated_by:#{initiated_by}"]
        tags.push("error:#{error}") if error.present?
        GitHub.dogstats.increment("repo.restore_job", tags: tags)
      end

      def initiated_by
        GitHub.context[:from].presence || GitHub.context[:job].presence || "unknown"
      end
    end
  end
end
