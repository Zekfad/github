# frozen_string_literal: true

module GitHub
  module Jobs
    class StafftoolsReport < Job
      def self.active_job_class
        ::StafftoolsReportJob
      end

      areas_of_responsibility :stafftools

      extend GitHub::HashLock

      def self.queue
        :staff_tools_report
      end

      def self.perform(report, skipmc = false)
        GitHub.cache.skip = true if skipmc

        GitHub::Reports.update(report)
      end
    end
  end
end
