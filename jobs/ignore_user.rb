# frozen_string_literal: true

module GitHub
  module Jobs
    class IgnoreUser < Job
      def self.active_job_class
        ::IgnoreUserJob
      end

      areas_of_responsibility :community_and_safety

      def self.queue
        :ignore_user
      end

      def self.perform(user_id, blockee_id)
        return unless user = User.find_by_id(user_id.to_i)
        return unless blockee = User.find_by_id(blockee_id.to_i)

        user.unfollow(blockee)
        blockee.unfollow user

        user.rebuild_contributions
        blockee.rebuild_contributions

        blockee_repos = blockee.repositories
        user_repos = user.repositories

        blockee_repos.each do |repo|
          repo.remove_member(user, user)
          user.unstar(repo)
        end

        user_repos.each do |repo|
          blockee.unstar(repo)
          repo.remove_member(blockee, user)
        end

        # Unsubscribe blockee from all user's repos, and delete existing notifications
        GitHub.newsies.async_delete_all_for_user_and_lists(blockee, user_repos)

        # Unpin repositories owned by the blocker/blockee
        ProfilePinner.unpin(*blockee_repos, user: user, viewer: user)
        ProfilePinner.unpin(*user_repos, user: blockee, viewer: user)

        # Cancel any pending invitations for the blocked user.
        RepositoryInvitation.cancel_all_invitations_involving(
          repo_ids: user_repos.pluck(:id),
          user: blockee,
        )

        # Remove any GitHub Sponsors relationships between the users
        User.where(id: [user_id, blockee_id])
            .includes(plan_subscription: :subscription_items)
            .flat_map(&:subscription_items)
            .select(&:listable_is_sponsorable?).each do |item|
          listing = item.listing

          # This is already checked by is_sponsorable? but do it here explicitly
          # to avoid causing problems in future when we support non-User
          # sponsorables.
          #
          # We can remove the Marketplace::Listing checks when sponsors decouples from
          # Marketplace.
          # See https://github.com/github/sponsors/issues/353
          next if (listing.is_a?(Marketplace::Listing) && listing.listable_type != User.name) ||
                  (listing.is_a?(SponsorsListing) && listing.sponsorable_type != User.name)

          maintainer_id = listing.is_a?(Marketplace::Listing) ? listing.listable_id : listing.sponsorable_id
          next unless [user_id, blockee_id].include?(maintainer_id)

          item.cancel!(actor: user, force: true)
        end

        if user.organization?
          user.pending_invitations.where(invitee_id: blockee.id).each { |i| i.cancel(actor: user) }
        end

        # Remove account succession relationships, captured in SuccessorInvitation associations
        SuccessorInvitation.terminate_all(user, blockee)
      end
    end
  end
end
