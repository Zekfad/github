# rubocop:disable Style/FrozenStringLiteralComment

require "github/redis/mutex"

module GitHub
  module Jobs
    # This repair job doesn't actually do any repairing directly, but queues up a Newsies::IndexNotificationsJob
    # to do that work, since we cannot yet use the built in reconciler for notifications.
    #
    # This class overrides just enough in the base job to allow it to work from the UI
    #
    # Note:
    #    - The reported progress/times will not be accurate since a separate job does the work which is not being monitored
    #    - Unlike other repair jobs, the work cannot be done in parallel, so only one job should be queued at once
    class RepairNotificationsIndex < ::Elastomer::Repair
      def self.active_job_class
        ::RepairNotificationsIndexJob
      end

      FINISHED_KEY = "notifications/finished".freeze

      areas_of_responsibility :notifications

      def self.queue
        :notifications
      end

      def show_worker_count_input?
        true
      end

      def kv_enabled_key
        "#{group_key}/enabled"
      end

      def start(concurrency = 1)
        started = redis.hsetnx(group_key, STARTED_KEY, Time.now.iso8601)

        # Don't re-queue if we've already started the process once
        if started
          queue_repair_jobs(concurrency)
        end
      end

      def enable
        GitHub.kv.set(kv_enabled_key, "true")
        super
      end

      def reset!
        GitHub.kv.del(kv_enabled_key)
        super
      end

      def disable
        GitHub.kv.del(kv_enabled_key)
        super
      end

      def queue_repair_jobs(concurrency = 1)
        # Queues multiple jobs for the listed user ids
        # The number of jobs queued will match the passed `concurrency` and each
        # job will be passed a slice of the user ids to process
        user_ids = feature_flagged_user_ids
        slice_size = (user_ids.length / concurrency.to_f).ceil

        user_ids.each_slice(slice_size) do |user_ids_slice|
          ::Newsies::IndexNotificationsJob.perform_later(
            index_name: @index_name,
            cluster_name: @cluster_name,
            user_ids: user_ids_slice,
            kv_enabled_key: kv_enabled_key,
          )
        end

        # Mark the job as "finished" immediately
        # The queued job will still be running, but that's okay for our needs right now
        redis.hsetnx(group_key, FINISHED_KEY, Time.now.iso8601)

        # This stat will not be realistic in terms of time taken, but setting it will allow reported stats
        # to show the repair job as "finished"
        redis.hincrbyfloat(group_key, ELAPSED_KEY, 1)
      end

      # This is obviously incorrect
      def progress
        100.0
      end

      # This reads back the value set in repair! so we know that
      # the background job has been triggered at least
      def finished?
        redis.hexists(group_key, FINISHED_KEY)
      end

      # Internal: Load all the users that are feature flagged into redis
      #
      # If the feature is staff shipped we have to lookup the employees manually
      # as the feature flag doesn't store individual employees for a staff ship
      def feature_flagged_user_ids
        ActiveRecord::Base.connected_to(role: :reading) do
          if staff_shipped?
            # All staff members
            GitHub::FeatureFlag.employees_team.member_ids
          else
            # Only explicitly listed actors
            FlipperFeature.find_by_name(:notifications_search)&.actors&.map(&:id) || []
          end
        end
      end

      # Internal: Are either of the feature flags staff shipped?
      #
      # We check two flags here:
      #
      # - notifications_repair_job - enables just this repair job to run without also enabling the UI
      # - notifications_search     - enables the repair job to run _and_ the UI
      def staff_shipped?
        FlipperFeature
          .where(name: [:notifications_repair_job, :notifications_search])
          .any? do |feature|
            feature.flipper_gates.where(name: "groups", value: "preview_features").exists?
          end
      end
    end
  end
end
