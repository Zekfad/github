# frozen_string_literal: true

module GitHub
  module Jobs
    class MigrateLegacyAdminTeams < Job
      def self.active_job_class
        ::MigrateLegacyAdminTeamsJob
      end

      areas_of_responsibility :orgs

      def self.queue
        :migrate_legacy_admin_teams
      end

      def self.perform_with_retry(organization_id, opts = {})
        new(organization_id).perform
      end

      def initialize(organization_id)
        @organization_id = organization_id
      end

      def perform
        organization.migrate_legacy_admin_teams!
      end

      private

      def organization
        @organization ||= Organization.find(@organization_id)
      end
    end
  end
end
