# frozen_string_literal: true

class HooksIntPk < GitHub::Migration
  def up
    # Change from unsigned to signed int to match child columns
    change_column :hooks, :id, "int not null auto_increment"
    #shorten long string columns
    change_column :hooks, :name, :string, limit: 100, null: true
    change_column :hooks, :installation_target_type, :string, limit: 40, null: false
    # drop dead index
    remove_index :hooks, name: "index_hooks_on_creator_id"

    # make same changes in archived_hooks table
    change_column :archived_hooks, :id, "int not null auto_increment"
    change_column :archived_hooks, :name, :string, limit: 100, null: true
    change_column :archived_hooks, :installation_target_type, :string, limit: 40, null: false
    # drop dead indexes
    remove_index :archived_hooks, name: "index_hooks_on_repository_id_and_active"
    remove_index :archived_hooks, name: "index_hooks_on_updated_at"

  end
  def down
    change_column :hooks, :id, "int unsigned not null auto_increment"
    change_column :hooks, :name, :string, limit: 255, null: true
    change_column :hooks, :installation_target_type, :string, limit: 255, null: false
    add_index :hooks, :creator_id
    change_column :archived_hooks, :id, "int unsigned not null auto_increment"
    change_column :archived_hooks, :name, :string, limit: 255, null: true
    change_column :archived_hooks, :installation_target_type, :string, limit: 255, null: false
    add_index :archived_hooks, :active, name: "index_hooks_on_repository_id_and_active"
    add_index :archived_hooks, :updated_at, name: "index_hooks_on_updated_at"
  end
end
