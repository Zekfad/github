# frozen_string_literal: true

class AddZuoraWebhooksTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :zuora_webhooks do |t|
      t.integer :kind, null: false
      t.string :account_id, limit: 64
      t.text :payload, null: false
      t.datetime :processed_at
      t.timestamps null: false

      t.index :processed_at
    end
  end
end
