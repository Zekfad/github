# frozen_string_literal: true

class AddUpdateAndCreationColumnsToAcv < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :acv_contributors, :created_at, :datetime, null: false
    add_column :acv_contributors, :updated_at, :datetime, null: false
  end
end
