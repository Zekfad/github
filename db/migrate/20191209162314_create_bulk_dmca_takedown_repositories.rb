# frozen_string_literal: true
#
class CreateBulkDmcaTakedownRepositories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)
  def change
    create_table :bulk_dmca_takedown_repositories do |t|
      t.belongs_to :repository, index: { name: :bulk_dmca_takedown_repositories_on_repository_id }
      t.belongs_to :bulk_dmca_takedown, index: { name: :bulk_dmca_takedown_repositories_on_bulk_id }

      t.timestamps null: false
    end
  end
end
