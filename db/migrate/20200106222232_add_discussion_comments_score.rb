# frozen_string_literal: true

class AddDiscussionCommentsScore < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussion_comments, :score, :integer, null: false, default: 0
    add_index :discussion_comments, [:score, :repository_id, :discussion_id],
      name: "index_discussion_comments_on_score_repo_discussion"
  end
end
