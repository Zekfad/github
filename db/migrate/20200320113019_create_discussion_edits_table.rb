# frozen_string_literal: true

class CreateDiscussionEditsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussion_edits do |t|
      t.belongs_to :discussion, null: false, index: true
      t.datetime :edited_at, null: false
      t.belongs_to :editor, null: false
      t.timestamps null: false
      t.belongs_to :performed_by_integration
      t.datetime :deleted_at
      t.belongs_to :deleted_by
      t.mediumblob :diff
      t.integer :user_content_edit_id, index: { unique: true }
    end
  end
end
