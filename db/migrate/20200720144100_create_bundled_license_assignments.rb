# frozen_string_literal: true

class CreateBundledLicenseAssignments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  # See https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/migrations-and-transitions/database-migrations-for-dotcom/ for tips

  def change
    create_table :bundled_license_assignments do |t|
      t.string :enterprise_agreement_number, limit: 128, null: false
      t.integer :business_id
      t.string :email, limit: 320, null: false
      t.string :subscription_id, limit: 36, null: false
      t.integer :user_id
      t.boolean :revoked, null: false, default: false
      t.timestamps null: false

      t.index :enterprise_agreement_number
      t.index [:business_id, :email]
      t.index :subscription_id, unique: true
    end
  end
end
