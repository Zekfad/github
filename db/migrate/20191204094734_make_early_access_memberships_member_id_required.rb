# frozen_string_literal: true

class MakeEarlyAccessMembershipsMemberIdRequired < GitHub::Migration
  def change
    change_column_null :early_access_memberships, :member_id, false
    change_column_null :early_access_memberships, :user_id, true
  end
end
