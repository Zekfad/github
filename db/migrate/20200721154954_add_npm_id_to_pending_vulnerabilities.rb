# frozen_string_literal: true

class AddNpmIdToPendingVulnerabilities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :pending_vulnerabilities, :npm_id, :integer, unsigned: true, null: true, default: nil
    add_index :pending_vulnerabilities, :npm_id, unique: true
  end
end
