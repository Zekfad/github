# frozen_string_literal: true

class CreateBusinessUserAccounts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :business_user_accounts do |t|
      t.integer :business_id, null: false
      t.integer :user_id, null: true
      t.boolean :collaborator, default: false, null: false
      t.timestamps null: false
    end

    add_index :business_user_accounts, [:business_id, :user_id]
  end
end
