# frozen_string_literal: true

class CreateRepositoryImports < GitHub::Migration
  self.use_connection_class ApplicationRecord::Collab
  def change
    create_table :repository_imports do |t|
      t.belongs_to :import, null: false
      t.belongs_to :repository, null: false

      t.index(
        [:repository_id],
        unique: true,
        name: "index_repository_imports_repository_id"
      )

      t.index(
        [:import_id],
        name: "index_repository_imports_import_id"
      )

      t.timestamps null: false
    end
  end
end
