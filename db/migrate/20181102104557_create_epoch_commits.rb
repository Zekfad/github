# frozen_string_literal: true

class CreateEpochCommits < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :epoch_commits, id: false do |t|
      t.column     :id, "BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.column     :commit_oid, "VARCHAR(40)", null: false
      t.references :repository, null: false
      t.column     :version_vector, :blob, null: false
      t.column     :epoch_id, "BIGINT UNSIGNED", null: false
      t.datetime   :created_at, null: false
    end
    add_index :epoch_commits, [:epoch_id, :created_at]
    add_index :epoch_commits, [:repository_id, :commit_oid, :created_at], name: :index_epoch_commits_on_repo_commit_oid
  end
end
