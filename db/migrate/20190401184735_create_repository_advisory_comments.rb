# frozen_string_literal: true

class CreateRepositoryAdvisoryComments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :repository_advisory_comments do |t|
      t.references :repository_advisory, index: true, null: false
      t.references :user, index: true, null: false
      t.mediumblob :body, null: false
      t.column :formatter, "varchar(20)", null: true
      t.timestamps null: false
    end
  end
end
