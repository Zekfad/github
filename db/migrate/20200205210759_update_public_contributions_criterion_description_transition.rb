# frozen_string_literal: true

require "github/transitions/20200205210759_update_public_contributions_criterion_description"

class UpdatePublicContributionsCriterionDescriptionTransition < GitHub::Migration
  def self.up
    return unless GitHub.sponsors_enabled?
    return unless Rails.development?

    transition = GitHub::Transitions::UpdatePublicContributionsCriterionDescription.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
