# frozen_string_literal: true

class RemoveLicenseStateFromUserLicense < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_column :user_licenses, :license_state, :integer, null: false, default: 0
  end
end
