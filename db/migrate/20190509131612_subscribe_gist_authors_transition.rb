# frozen_string_literal: true

require "github/transitions/20190509131612_subscribe_gist_authors"

class SubscribeGistAuthorsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::SubscribeGistAuthors.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
