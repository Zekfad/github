# frozen_string_literal: true

class AddFeaturedStateToSponsorsMemberships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_memberships, :featured_state, :integer, default: 0
    add_index :sponsors_memberships, %i(featured_state sponsorable_id)
  end
end
