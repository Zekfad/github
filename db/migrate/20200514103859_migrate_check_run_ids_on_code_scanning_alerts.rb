# frozen_string_literal: true

class MigrateCheckRunIdsOnCodeScanningAlerts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    change_column :code_scanning_alerts, :check_run_id, "bigint(11) unsigned NOT NULL"
  end

  def down
    change_column :code_scanning_alerts, :check_run_id, "int(11) NOT NULL"
  end
end
