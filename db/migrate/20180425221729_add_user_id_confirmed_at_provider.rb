# frozen_string_literal: true

class AddUserIdConfirmedAtProvider < GitHub::Migration
  def change
    add_index :delegated_recovery_tokens, %i(user_id provider confirmed_at), name: :index_on_user_id_and_provider_and_confirmed_at
  end
end
