# frozen_string_literal: true

class AddOrgCredAuthIndexes < GitHub::Migration
  def change
    add_index :organization_credential_authorizations, [:credential_id, :credential_type, :revoked_by_id], name: "index_on_credential_id_and_credential_type_and_revoked_by_id"
  end
end
