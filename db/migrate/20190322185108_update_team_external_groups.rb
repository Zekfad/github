# frozen_string_literal: true

class UpdateTeamExternalGroups < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  def change
    remove_index :team_external_groups, column: :team_id, if_exists: true               # Will be replaced w/ composite index below
    remove_column :team_external_groups, :guid , if_exists: true                        # No longer needed for Team Sync

    add_column :team_external_groups, :group_id, :string, limit: 50, null: false        # Create group_id column, not null
    add_column :team_external_groups, :group_name, :string, limit: 100, null: false     # Create group_name column for display when editing a team
    add_column :team_external_groups, :group_description, :string                       # Create group_description column for display when editing a team
    add_column :team_external_groups, :status, :integer, default: 1, null: false        # Default to 1 assuming we will need to match Protobuf enum
    add_column :team_external_groups, :synced_at, :datetime                             # Datetime which the Team/Group was marked as synced

    add_index :team_external_groups, [:team_id, :group_id], unique: true                # Add unique composite index to prevent dupes & looking up groups
  end
end
