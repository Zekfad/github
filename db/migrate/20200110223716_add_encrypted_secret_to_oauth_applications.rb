# frozen_string_literal: true

class AddEncryptedSecretToOauthApplications < GitHub::Migration
  def change
    add_column :oauth_applications, :encrypted_secret, :binary, limit: 255, null: true
  end
end
