# frozen_string_literal: true

class DropNotificationEntriesSpam < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def up
    drop_table :notification_entries_spam
  end

  def down
    create_table :notification_entries_spam do |t|
      t.string :list_type, null: false, limit: 64, default: "Repository"
      t.string :thread_key, null: false, limit: 80
      t.datetime :created_at, null: false

      t.index [:list_type, :thread_key], name: "index_notification_entries_spam_on_list_type_and_thread_key", unique: true
    end
  end
end
