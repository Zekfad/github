# frozen_string_literal: true

# This is the final step of renaming `created_on` to `contributed_on`
class RemoveCreatedOnFromEnterpriseContributions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    # Drop the remaining index and the column.
    remove_index :enterprise_contributions, name: "index_enterprise_contributions_on_user_id_and_created_on"
    remove_column :enterprise_contributions, :created_on

    # Make the index of the new column unique (we could not do this until
    # tests got fixed by https://github.com/github/github/pull/90516)
    remove_index :enterprise_contributions, name: "index_enterprise_contributions_on_user_installation_contributed"
    add_index :enterprise_contributions, [:user_id, :enterprise_installation_id, :contributed_on], unique: true, name: "index_enterprise_contributions_on_user_installation_contributed"
  end
end
