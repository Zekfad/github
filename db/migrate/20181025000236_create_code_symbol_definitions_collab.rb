# frozen_string_literal: true

class CreateCodeSymbolDefinitionsCollab < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :code_symbol_definitions do |t|
      t.column :symbol, "VARBINARY(767)", null: false
      t.column :path, "VARBINARY(767)", null: false
      t.column :line, "VARBINARY(1024)", null: false
      t.column :docs, :mediumblob
      t.string :kind, null: false, limit: 20
      t.integer :row, null: false
      t.integer :col, null: false
      t.column :language_name, "VARCHAR(40)", null: false
      t.references :repository_code_symbol_index, null: false
      t.timestamps null: false
    end

    add_index :code_symbol_definitions, [:repository_code_symbol_index_id, :symbol, :kind, :path, :row, :col],
      name: "index_code_symbol_defs_on_repo_code_symbol_index_sym_kind_path",
      unique: true
  end
end
