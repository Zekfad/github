# frozen_string_literal: true

class AddPerRepoAssetActivity < GitHub::Migration
  def up
    add_column :asset_activities , :repository_id, :integer, default: 0, null: false

    add_index :asset_activities, [:asset_type, :owner_id, :activity_started_at, :repository_id], name: "index_asset_activities_on_type_owner_started_at_and_repo", unique: true
    add_index :asset_activities, [:asset_type, :owner_id, :repository_id], name: "index_asset_activities_on_type_owner_and_repo"
    remove_index :asset_activities, name: "index_asset_activities_on_type_owner_and_started_at"
  end

  def down
    remove_column :asset_activities , :repository_id

    add_index :asset_activities, [:asset_type, :owner_id, :activity_started_at], name: "index_asset_activities_on_type_owner_and_started_at", unique: true
    remove_index :asset_activities,  name: "index_asset_activities_on_type_owner_started_at_and_repo"
    remove_index :asset_activities,  name: "index_asset_activities_on_type_owner_and_repo"
  end
end
