# frozen_string_literal: true

class CreateScopedIntegrationInstallations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :scoped_integration_installations, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"

      t.belongs_to :integration_installation, null: false

      t.timestamps null: false
    end

    add_index :scoped_integration_installations, :integration_installation_id,
      name: "index_scoped_installations_on_integration_installation_id"
  end
end
