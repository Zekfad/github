# frozen_string_literal: true

require "github/transitions/20200401171316_delete_org_stargazers"

class DeleteOrgStargazersTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DeleteOrgStargazers.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
