# frozen_string_literal: true

class RemoveManagedColumnFromIntegrations < GitHub::Migration
  def change
    remove_column :integrations, :managed, :boolean, default: false, null: false
  end
end
