# frozen_string_literal: true

class AddReportingAttributesToMigrations < GitHub::Migration
  def change
    add_index  :migrations, :guid, unique: true
    add_index  :migrations, [:owner_id, :guid]
    add_column :migrations, :migratable_resources_count, :integer
    add_index  :migrations, :migratable_resources_count
    add_column :migrations, :archive_size, "bigint(20)"
    add_index  :migrations, :archive_size
  end
end
