# frozen_string_literal: true

class DropProfilePinnedRepositories < GitHub::Migration
  def up
    drop_table :profile_pinned_repositories
  end

  def down
    create_table :profile_pinned_repositories do |t|
      t.integer :profile_id, null: false
      t.integer :repository_id, null: false
      t.integer :position, null: false, default: 1
    end
    add_index :profile_pinned_repositories, [:profile_id, :repository_id, :position],
              unique: true, name: "index_profile_pinned_repositories_unique"
    add_index :profile_pinned_repositories, :repository_id
  end
end
