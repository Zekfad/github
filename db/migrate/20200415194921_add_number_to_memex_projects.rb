# frozen_string_literal: true

class AddNumberToMemexProjects < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Memex)

  def change
    add_column :memex_projects, :number, :integer
    add_index :memex_projects, [:number, :owner_type, :owner_id], unique: true
  end
end
