# frozen_string_literal: true

class DropSavedNotificationEntriesInMysql2 < GitHub::Migration
  # This migration is part of https://github.com/github/notifications/issues/267
  # and is meant to drop a table in the `mysql2` cluster that we've already moved
  # into the new `notifications_entries` cluster.
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    # We don't have multiple database clusters in Enterprise, so there's no need to drop the tables
    return if GitHub.enterprise?

    drop_table :saved_notification_entries
  end

  def self.down
    # We don't have multiple database clusters in Enterprise
    return if GitHub.enterprise?

    create_table :saved_notification_entries, if_not_exists: true do |t|
      t.integer :user_id, null: false
      t.integer :summary_id, null: false
      t.string  :list_type, null: false, default: "Repository", limit: 64
      t.integer :list_id, null: false
      t.string  :thread_key, null: false, limit: 80
      t.datetime :created_at, null: false
    end
    change_column :saved_notification_entries, :id, "bigint(11) NOT NULL AUTO_INCREMENT"
    add_index :saved_notification_entries, [:user_id, :list_type, :list_id, :thread_key], unique: true,
      name: "index_saved_notification_entries_on_user_list_and_thread", if_not_exists: true
    add_index :saved_notification_entries, [:user_id, :created_at],
      name: "index_saved_notification_entries_on_user_id_and_created_at", if_not_exists: true
    add_index :saved_notification_entries, [:user_id, :list_type, :list_id, :created_at],
      name: "index_saved_notification_entries_on_user_list_and_created_at", if_not_exists: true
    add_index :saved_notification_entries, [:user_id, :summary_id], if_not_exists: true
    add_index :saved_notification_entries, [:list_id, :list_type, :thread_key],
      name: "index_saved_notification_entries_on_list_and_thread", if_not_exists: true
  end
end
