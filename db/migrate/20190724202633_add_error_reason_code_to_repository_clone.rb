# frozen_string_literal: true

class AddErrorReasonCodeToRepositoryClone < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :repository_clones, :error_reason_code, :integer, null: true
  end
end
