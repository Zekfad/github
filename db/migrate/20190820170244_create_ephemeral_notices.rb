# frozen_string_literal: true

class CreateEphemeralNotices < GitHub::Migration

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :ephemeral_notices do |t|
      t.references :user, null: false
      t.integer :parent_id, null: false
      t.string :parent_type, null: false, limit: 100
      t.integer :notice_type, default: 0, null: false # Enum in the model
      t.column :notice_text, "VARBINARY(1024)", null: false
      t.text :link_to
      t.column :link_title, "VARBINARY(1024)"

      t.timestamps null: false
    end

    add_index :ephemeral_notices, [:parent_id, :parent_type, :user_id], name: "parent_ephemeral_notice_user_idx"
  end
end
