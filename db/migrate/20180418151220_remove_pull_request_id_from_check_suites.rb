# frozen_string_literal: true

class RemovePullRequestIdFromCheckSuites < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    remove_index  :check_suites, [:pull_request_id]
    remove_column :check_suites, :pull_request_id
  end
end
