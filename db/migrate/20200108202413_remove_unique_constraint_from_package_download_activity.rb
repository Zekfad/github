# frozen_string_literal: true

class RemoveUniqueConstraintFromPackageDownloadActivity < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    # Remove the unique index that asserts only one version of a package can be downloaded at a given datetime
    remove_index :package_download_activities, name: "index_package_download_activities_on_package_id_and_started_at", if_exists: true

    # Add the non-unique index
    add_index :package_download_activities, [:package_id, :started_at],
      name: "index_package_download_activities_on_pkg_id_and_started_at", unique: false, if_not_exists: true
  end

  def down
    remove_index :package_download_activities, name: "index_package_download_activities_on_pkg_id_and_started_at", if_exists: true

    add_index :package_download_activities, [:package_id, :started_at],
      name: "index_package_download_activities_on_package_id_and_started_at", unique: true, if_not_exists: true
  end
end
