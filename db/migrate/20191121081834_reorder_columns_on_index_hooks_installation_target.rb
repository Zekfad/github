# frozen_string_literal: true

class ReorderColumnsOnIndexHooksInstallationTarget < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_index :hooks, :installation_target_type
  end
end
