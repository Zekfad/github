# frozen_string_literal: true

class DataHoundsIntPk < GitHub::Migration
  def up
    # Change from unsigned to signed int to match child columns
    change_column :data_hounds, :id, "int not null auto_increment"
    #shorten long string columns
    change_column :data_hounds, :type, :string, limit: 40, null: false
    change_column :data_hounds, :tasks, :string, limit: 100, null: true
    change_column :data_hounds, :status, :string, limit: 20, null: true
    change_column :data_hounds, :server, :string, limit: 100, null: true
  end
  def down
    change_column :data_hounds, :id, "int unsigned not null auto_increment"
    change_column :data_hounds, :type, :string, limit: 255, null: false
    change_column :data_hounds, :tasks, :string, limit: 255, null: true
    change_column :data_hounds, :status, :string, limit: 255, null: true
    change_column :data_hounds, :server, :string, limit: 255, null: true
  end
end
