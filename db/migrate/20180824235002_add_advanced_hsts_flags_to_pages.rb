# frozen_string_literal: true

class AddAdvancedHstsFlagsToPages < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_column :pages, :hsts_include_sub_domains, :boolean, default: false, null: false
    add_column :pages, :hsts_preload, :boolean, default: false, null: false
  end

  def down
    remove_column :pages, :hsts_include_sub_domains
    remove_column :pages, :hsts_preload
  end
end
