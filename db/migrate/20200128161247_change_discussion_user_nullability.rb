# frozen_string_literal: true

class ChangeDiscussionUserNullability < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column_null :discussions, :user_id, true
    change_column_null :discussion_comments, :user_id, true
  end
end
