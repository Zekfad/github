# frozen_string_literal: true

class AddTokenScanResultLocations < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Notify)

  def self.up
    create_table :token_scan_result_locations do |t|
      t.belongs_to :token_scan_result, null: false
      t.timestamps null: false
      t.column :commit_oid, :string, limit: 40, null: false
      t.column :blob_oid, :string, limit: 40, null: false
      t.column :path, "VARBINARY(1024)", null: false
      t.column :start_line, :integer, null: false
      t.column :end_line, :integer, null: false
      t.column :start_column, :integer, null: false
      t.column :end_column, :integer, null: false

      t.index :token_scan_result_id
    end
  end

  def self.down
    drop_table :token_scan_result_locations, if_exists: true
  end
end
