# frozen_string_literal: true

class CreateRepositoryAdvisories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :repository_advisories do |t|
      t.belongs_to :repository, null: false
      t.belongs_to :author, null: false
      t.belongs_to :publisher, null: true
      t.belongs_to :assignee, null: true
      t.integer :number, null: false
      t.binary :title, limit: 1024, null: false
      t.mediumblob :description, null: true
      t.integer :state, default: 0, null: false
      t.integer :severity, null: false
      t.blob :affected_versions, null: false
      t.blob :fixed_versions, null: true
      t.mediumblob :impact, null: true
      t.mediumblob :workarounds, null: true
      t.mediumblob :patches, null: true
      t.string :cve_id, limit: 40, null: true
      t.timestamps null: false
      t.datetime :published_at, null: true
      t.datetime :closed_at, null: true
      t.datetime :withdrawn_at, null: true
    end

    add_index :repository_advisories, [:repository_id, :number], unique: true
    add_index :repository_advisories, [:repository_id, :state]
  end
end
