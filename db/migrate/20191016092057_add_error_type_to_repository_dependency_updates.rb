# frozen_string_literal: true

class AddErrorTypeToRepositoryDependencyUpdates < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :repository_dependency_updates, :error_type, :string, limit: 60, null: true
  end
end
