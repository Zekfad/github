# frozen_string_literal: true

class AddPayoutProbationToSponsorsListings < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  include GitHub::SafeDatabaseMigrationHelper

  def up
    add_column :sponsors_listings, :payout_probation_started_at, :datetime, if_not_exists: true
    add_column :sponsors_listings, :payout_probation_ended_at, :datetime, if_not_exists: true
    add_index :sponsors_listings, [:payout_probation_started_at, :payout_probation_ended_at],
      name: "index_sponsors_listings_on_payout_probation", if_not_exists: true
    add_index  :sponsors_listings, [:state, :payout_probation_started_at],
      name: "index_sponsors_listings_on_state_and_payout_probation_started", if_not_exists: true
    add_index  :sponsors_listings, [:state, :payout_probation_ended_at, :payout_probation_started_at],
      name: "index_sponsors_listings_on_state_and_payout_probation", if_not_exists: true
  end

  def down
    remove_index :sponsors_listings, name: "index_sponsors_listings_on_payout_probation", if_exists: true
    remove_index :sponsors_listings, name: "index_sponsors_listings_on_state_and_payout_probation_started", if_exists: true
    remove_index :sponsors_listings, name: "index_sponsors_listings_on_state_and_payout_probation", if_exists: true
    remove_column :sponsors_listings, :payout_probation_started_at, if_exists: true
    remove_column :sponsors_listings, :payout_probation_ended_at, if_exists: true
  end
end
