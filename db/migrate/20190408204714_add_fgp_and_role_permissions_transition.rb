# frozen_string_literal: true

require "github/transitions/20190408204714_add_fgp_and_role_permissions"

class AddFgpAndRolePermissionsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddFgpAndRolePermissions.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
