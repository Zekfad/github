# frozen_string_literal: true

class AddStateToWorkspaces < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :workspaces, :state, :int, null: false, default: 0
  end
end
