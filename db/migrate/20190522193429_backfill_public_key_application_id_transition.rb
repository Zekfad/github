# frozen_string_literal: true

require "github/transitions/20190522193429_backfill_public_key_application_id"

class BackfillPublicKeyApplicationIdTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillPublicKeyApplicationId.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
