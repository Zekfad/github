# frozen_string_literal: true

require "github/transitions/20200128183602_populate_xref_close_issue_references"

class PopulateXrefCloseIssueReferencesTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::PopulateXrefCloseIssueReferences.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
