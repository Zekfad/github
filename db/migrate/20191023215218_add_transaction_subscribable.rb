# frozen_string_literal: true

class AddTransactionSubscribable < GitHub::Migration
  def change
    add_column :transactions, :current_subscribable_id, :integer
    add_column :transactions, :current_subscribable_type, :integer
    add_column :transactions, :current_subscribable_quantity, :integer
    add_column :transactions, :old_subscribable_id, :integer
    add_column :transactions, :old_subscribable_type, :integer
    add_column :transactions, :old_subscribable_quantity, :integer

    add_index :transactions,
      [:current_subscribable_id, :old_subscribable_id, :current_subscribable_type, :old_subscribable_type],
      name: "index_transactions_on_subscribables"
  end
end
