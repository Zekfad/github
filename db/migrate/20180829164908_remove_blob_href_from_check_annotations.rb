# frozen_string_literal: true

class RemoveBlobHrefFromCheckAnnotations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    remove_column :check_annotations, :blob_href
  end
end
