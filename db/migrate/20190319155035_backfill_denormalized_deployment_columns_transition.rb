# frozen_string_literal: true

require "github/transitions/20190319155035_backfill_denormalized_deployment_columns"

class BackfillDenormalizedDeploymentColumnsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillDenormalizedDeploymentColumns.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
