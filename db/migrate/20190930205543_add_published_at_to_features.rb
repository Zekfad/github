# frozen_string_literal: true

class AddPublishedAtToFeatures < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_column :features, :published_at, :datetime, null: :true
    add_index :features, [:published_at, :flipper_feature_id, :legal_agreement_required], name: "idx_on_published_at_flipper_feature_id_legal_agreement_required"
  end

  def down
    remove_index :features, name: "idx_on_published_at_flipper_feature_id_legal_agreement_required"
    remove_column :features, :published_at
  end
end
