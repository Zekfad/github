# frozen_string_literal: true

class RemoveDiscussionsClosedAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :discussions, name: "index_discussions_on_closed_at_and_repository_id"
    remove_column :discussions, :closed_at
  end

  def down
    add_column :discussions, :closed_at, :datetime
    add_index :discussions, [:closed_at, :repository_id]
  end
end
