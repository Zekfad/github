# frozen_string_literal: true

class FixSponsorsMetricsIndex < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :sponsors_activity_metrics, name: "index_metrics_on_sponsorable_and_recordedon_and_metric_and_value", if_exists: true
    add_index :sponsors_activity_metrics, [:sponsorable_id, :sponsorable_type, :metric, :recorded_on], unique: true, name: "index_sponsors_metrics_on_sponsorable_and_metric_and_recorded_on", if_not_exists: true
  end

  def down
    remove_index :sponsors_activity_metrics, name: "index_sponsors_metrics_on_sponsorable_and_metric_and_recorded_on", if_exists: true
    add_index :sponsors_activity_metrics, [:sponsorable_id, :sponsorable_type, :metric, :recorded_on, :value], unique: true, name: "index_metrics_on_sponsorable_and_recordedon_and_metric_and_value", if_not_exists: true
  end
end
