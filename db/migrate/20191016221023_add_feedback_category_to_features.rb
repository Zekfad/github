# frozen_string_literal: true

class AddFeedbackCategoryToFeatures < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :features, :feedback_category, :string
  end
end
