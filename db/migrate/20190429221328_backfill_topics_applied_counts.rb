# frozen_string_literal: true

require "github/transitions/20190429221306_backfill_topics_applied_counts"

class BackfillTopicsAppliedCounts < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillTopicsAppliedCounts.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
