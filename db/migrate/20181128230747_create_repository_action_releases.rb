# frozen_string_literal: true

class CreateRepositoryActionReleases < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    create_table :repository_action_releases do |t|
      t.integer :repository_action_id, null: false
      t.integer :release_id, null: false
      t.boolean :published_on_marketplace, default: false, null: false
      t.timestamps null: false
    end

    add_index :repository_action_releases, :repository_action_id
    add_index :repository_action_releases, [:release_id, :repository_action_id], unique: true, name: "index_repository_action_releases_on_release_and_action_id"
  end
end
