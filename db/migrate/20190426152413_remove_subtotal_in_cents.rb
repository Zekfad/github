# frozen_string_literal: true

class RemoveSubtotalInCents < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    remove_column :billing_transactions, :subtotal_in_cents, :integer, null: false, default: 0
  end
end
