# frozen_string_literal: true

class AddLocationToWorkspaces < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :workspaces, :location, :string, limit: 40
  end
end
