# frozen_string_literal: true

class CreateMobilePushNotificationSchedules < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips
  self.use_connection_class(ApplicationRecord::Domain::Notifications)

  def change
    create_table :mobile_push_notification_schedules do |t|
      t.references :user, null: false
      t.column :day, :tinyint, null: false
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false

      t.timestamps null: false
    end

    add_index(
      :mobile_push_notification_schedules,
      [:user_id, :day, :start_time, :end_time],
      name: "index_schedules_on_user_and_day_and_start_time_and_end_time",
    )
  end
end
