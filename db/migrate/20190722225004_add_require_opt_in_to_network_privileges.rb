# frozen_string_literal: true

class AddRequireOptInToNetworkPrivileges < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :network_privileges, :require_opt_in, :boolean, default: false, null: false
  end
end
