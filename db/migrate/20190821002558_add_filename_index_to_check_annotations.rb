# frozen_string_literal: true

class AddFilenameIndexToCheckAnnotations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :check_annotations, :filename
  end
end
