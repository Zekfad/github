# frozen_string_literal: true

class AddPreviousAggregationIndexForSharedStorageAggregations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :shared_storage_artifact_aggregations,
      [:billable_owner_type, :billable_owner_id, :directly_billed, :repository_id, :owner_id, :aggregate_effective_at],
      name: "idx_shared_storage_artifact_aggregations_previous_aggregation"
  end
end
