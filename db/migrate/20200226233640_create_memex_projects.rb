# frozen_string_literal: true

class CreateMemexProjects < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Memex)

  def change
    create_table :memex_projects do |t|
      t.references :owner, polymorphic: { limit: 30 }, null: false
      t.references :creator, null: false
      t.column :title, "VARBINARY(1024)"
      t.mediumblob :description
      t.boolean :public, null: false, default: false
      t.datetime :closed_at
      t.timestamps null: false
    end

    add_index(
      :memex_projects,
      [:owner_id, :owner_type, :closed_at, :created_at],
      name: "index_memex_projects_on_owner_closed_at_and_created_at",
    )
  end
end
