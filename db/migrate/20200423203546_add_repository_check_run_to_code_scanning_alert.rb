# frozen_string_literal: true

class AddRepositoryCheckRunToCodeScanningAlert < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :code_scanning_alerts, :repository_id, :integer, null: true, if_not_exists: true
    add_column :code_scanning_alerts, :check_run_id, :integer, null: true, if_not_exists: true
    add_index :code_scanning_alerts, [:repository_id, :alert_number, :check_run_id], name: "index_code_scanning_alerts_on_repo_and_number_and_check_run", if_not_exists: true
  end
end
