# frozen_string_literal: true

class AddUniqueIdToAppleTransactionOnPlanSubscription < GitHub::Migration
  # See https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/migrations-and-transitions/database-migrations-for-dotcom/ for tips

  def change
    add_index :plan_subscriptions, [:user_id, :apple_transaction_id], unique: true
  end
end
