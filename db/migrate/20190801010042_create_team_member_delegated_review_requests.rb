# frozen_string_literal: true

class CreateTeamMemberDelegatedReviewRequests < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :team_member_delegated_review_requests do |t|
      t.integer    :team_id, null: false
      t.integer    :member_id, null: false
      t.datetime   :delegated_at, null: false

      t.index [:team_id, :member_id], name: "index_team_member_delegated_review_requests_on_team_member", unique: true
    end
  end
end
