# frozen_string_literal: true

class AddStartAndEndColumnToAnnotations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_annotations, :start_column, :integer, null: true
    add_column :check_annotations, :end_column, :integer, null: true
  end
end
