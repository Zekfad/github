# frozen_string_literal: true

class AddDeploymentStatusIdToIssueEventDetails < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :issue_event_details, :deployment_status_id, :integer
    add_column :archived_issue_event_details, :deployment_status_id, :integer
  end
end
