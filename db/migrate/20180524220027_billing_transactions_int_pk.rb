# frozen_string_literal: true

class BillingTransactionsIntPk < GitHub::Migration
  def up
    # Change from unsigned to signed int to match child columns
    change_column :billing_transactions, :id, "int not null auto_increment"
  end
  def down
    change_column :billing_transactions, :id, "int unsigned not null auto_increment"
  end
end
