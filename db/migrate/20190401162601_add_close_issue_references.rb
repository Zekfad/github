# frozen_string_literal: true

class AddCloseIssueReferences < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :close_issue_references do |t|
      t.integer :pull_request_id, null: false # pull request doing the closing
      t.integer :issue_id, null: false # issue that will be closed
      t.integer :pull_request_author_id, null: false # author of the pull request
      t.integer :issue_repository_id, null: false # repo containing the issue that will be closed
      t.timestamps null: false
    end

    add_index :close_issue_references, [:pull_request_id, :issue_id], unique: true
    add_index :close_issue_references, [:pull_request_author_id, :issue_id],
      name: "idx_close_issue_refs_pr_author_issue_id"
    add_index :close_issue_references, [:issue_id, :issue_repository_id]
    add_index :close_issue_references, :issue_repository_id
  end
end
