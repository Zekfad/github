# frozen_string_literal: true

class UpdateMeteredRefillRatePlanChargeId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column_null :billing_prepaid_metered_usage_refills, :zuora_rate_plan_charge_id, true
  end

  def down
    change_column_null :billing_prepaid_metered_usage_refills, :zuora_rate_plan_charge_id, false
  end
end
