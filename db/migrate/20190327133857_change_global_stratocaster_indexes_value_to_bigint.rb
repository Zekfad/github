# frozen_string_literal: true

class ChangeGlobalStratocasterIndexesValueToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql5)

  def change
    change_column :global_stratocaster_indexes, :value, :bigint
  end
end
