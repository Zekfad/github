# frozen_string_literal: true

class ChangeActionsAggregateDurationInMillisecondsToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    reversible do |dir|
      dir.up do
        change_column :actions_usage_aggregations, :aggregate_duration_in_milliseconds, :bigint, null: false
      end

      dir.down do
        change_column :actions_usage_aggregations, :aggregate_duration_in_milliseconds, :int, null: false
      end
    end
  end
end
