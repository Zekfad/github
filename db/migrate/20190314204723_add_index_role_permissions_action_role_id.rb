# frozen_string_literal: true

class AddIndexRolePermissionsActionRoleId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_index :role_permissions, [:role_id, :action]
  end
end
