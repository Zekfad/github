# frozen_string_literal: true

class CreateUserSeenFeatures < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :user_seen_features do |t|
      t.references :user
      t.references :feature
      t.timestamps null: false

      t.index [:user_id, :feature_id], unique: true
    end
  end
end
