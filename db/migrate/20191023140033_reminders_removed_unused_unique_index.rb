# frozen_string_literal: true

class RemindersRemovedUnusedUniqueIndex < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_table :reminder_delivery_times do |t|
      t.remove_index name: :index_reminder_delivery_times_on_reminder_id_and_time_and_day
      t.remove :reminder_id
      t.change :schedulable_id, :int, null: false, default: nil
      t.change :schedulable_type, "varchar(20)", null: false, deafult: nil
    end
  end

  def down
    change_table :reminder_delivery_times do |t|
      t.integer :reminder_id, null: false
      t.index [:reminder_id, :time, :day], unique: true, name: "index_reminder_delivery_times_on_reminder_id_and_time_and_day"
      t.change :schedulable_id, :int, null: true
      t.change :schedulable_type, "varchar(20)", null: true, default: nil
    end
  end
end
