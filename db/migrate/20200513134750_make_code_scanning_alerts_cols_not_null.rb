# frozen_string_literal: true

class MakeCodeScanningAlertsColsNotNull < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    change_column_null :code_scanning_alerts, :repository_id, false
    change_column_null :code_scanning_alerts, :check_run_id, false
  end
end
