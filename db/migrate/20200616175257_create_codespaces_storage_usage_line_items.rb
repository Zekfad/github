# frozen_string_literal: true

class CreateCodespacesStorageUsageLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :codespaces_storage_usage_line_items, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.belongs_to :owner, class_name: "User", null: false
      t.belongs_to :actor, class_name: "User", null: false
      t.belongs_to :repository, class_name: "Repository", null: false
      t.string :billable_owner_type, null: false, limit: 12
      t.integer :billable_owner_id, null: false
      t.boolean :directly_billed, default: true, null: false
      t.string :unique_billing_identifier, limit: 36, null: false
      t.integer :synchronization_batch_id, null: true
      t.column :submission_state, "enum('unsubmitted', 'submitted', 'skipped')", null: false, default: "unsubmitted"
      t.string :submission_state_reason, limit: 24, null: true
      t.column :size_in_bytes, "bigint NOT NULL"
      t.integer :duration_in_seconds, null: false
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false

      t.timestamps null: false

      t.index :owner_id
      t.index :actor_id
      t.index :synchronization_batch_id, name: "index_on_synchronization_batch_id"
      t.index :unique_billing_identifier, unique: true, name: "index_on_unique_billing_identifier"
      t.index [:billable_owner_type, :billable_owner_id], name: "index_on_billable_owner"
      t.index [:submission_state, :submission_state_reason], name: "index_on_submission_state_and_reason"
      t.index [:submission_state, :created_at], name: "index_on_submission_state_and_created_at"
    end
  end
end
