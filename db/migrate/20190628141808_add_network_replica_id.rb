# frozen_string_literal: true

class AddNetworkReplicaId < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def self.up
    add_column :repository_replicas, :network_replica_id, :bigint, null: true, if_not_exists: true
  end

  def self.down
    remove_column :repository_replicas, :network_replica_id, if_exists: true
  end
end
