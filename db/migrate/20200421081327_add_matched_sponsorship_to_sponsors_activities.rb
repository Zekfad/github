# frozen_string_literal: true

class AddMatchedSponsorshipToSponsorsActivities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_activities, :matched_sponsorship, :boolean, default: false, null: false
  end
end
