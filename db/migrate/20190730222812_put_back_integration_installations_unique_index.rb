# frozen_string_literal: true

class PutBackIntegrationInstallationsUniqueIndex < GitHub::Migration

  def up
    # This index originally had a UNIQUE constraint which was removed as
    # part of https://github.com/github/github/pull/98004 for Actions.
    # Now that we support scoped integration installations, we can put back
    # the UNIQUE constraint to prevent duplicated per integration/target installations
    remove_index :integration_installations, name: :index_installations_on_integration_and_target

    # Add the UNIQUE index with a new name to prevent the issues described here
    # https://github.com/github/github/pull/98004#issuecomment-421456054
    add_index :integration_installations,
              [:integration_id, :target_id, :target_type],
              name: :index_installations_on_integration_id_and_target_id,
              unique: true
  end

  def down
    remove_index :integration_installations, name: :index_installations_on_integration_id_and_target_id

    add_index :integration_installations,
              [:integration_id, :target_id, :target_type],
              name: :index_installations_on_integration_and_target
  end
end
