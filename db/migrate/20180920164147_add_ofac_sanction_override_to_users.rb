# frozen_string_literal: true

class AddOFACSanctionOverrideToUsers < GitHub::Migration
  def change
    add_column :users, :ofac_sanction_override, :boolean, default: false, null: false
  end
end
