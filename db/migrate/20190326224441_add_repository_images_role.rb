# frozen_string_literal: true

class AddRepositoryImagesRole < GitHub::Migration
  def up
    add_column :repository_images, :role, :integer, null: false, default: 0

    remove_index :repository_images, :repository_id
    add_index :repository_images, [:repository_id, :guid], unique: true
    add_index :repository_images, [:role, :repository_id, :guid]
  end

  def down
    remove_index :repository_images, [:repository_id, :guid]
    add_index :repository_images, :repository_id

    remove_column :repository_images, :role
  end
end
