# frozen_string_literal: true

class DropSomeWorkflowRunsIndexes < GitHub::Migration
  # See https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/migrations-and-transitions/database-migrations-for-dotcom/ for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    remove_index :workflow_runs, name: "index_workflow_runs_on_repository_id_and_event"
    remove_index :workflow_runs, name: "index_workflow_runs_on_repository_id_and_action"
    remove_index :workflow_runs, name: "index_workflow_runs_on_repository_id_and_name"
    remove_index :workflow_runs, name: "index_workflow_runs_on_repository_id_and_head_branch"
  end
end
