# frozen_string_literal: true

class DropVsoPlans < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    drop_table :vso_plans
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
