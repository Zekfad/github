# frozen_string_literal: true

class GitbackupsRepositoryIncrementalsUseVarchar < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Gitbackups)

  def change
    change_column :repository_incrementals, :checksum, "VARCHAR(48)"
  end
end
