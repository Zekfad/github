# frozen_string_literal: true

class ActivitySponsorCanBeNull < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :sponsors_activities, :sponsor_id, :integer, null: true
  end

  def down
    change_column :sponsors_activities, :sponsor_id, :integer, null: false
  end
end
