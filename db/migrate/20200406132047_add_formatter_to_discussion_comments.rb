# frozen_string_literal: true

class AddFormatterToDiscussionComments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussion_comments, :formatter, :string, limit: 20
  end
end
