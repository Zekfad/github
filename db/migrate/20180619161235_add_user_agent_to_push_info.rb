# frozen_string_literal: true
#
class AddUserAgentToPushInfo < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    add_column :web_push_subscriptions, :user_agent, :text, null: false
  end

  def down
    remove_column :web_push_subscriptions, :user_agent
  end
end
