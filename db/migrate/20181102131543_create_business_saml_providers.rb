# frozen_string_literal: true

class CreateBusinessSamlProviders < GitHub::Migration
  def change
    create_table :business_saml_providers do |t|
      t.integer :business_id, null: false
      t.string :sso_url, null: false
      t.string :issuer, default: nil
      t.text :idp_certificate, null: false
      t.boolean :disable_admin_demote, null: false, default: false
      t.timestamps null: false
      t.integer :digest_method, null: false, default: 0
      t.integer :signature_method, null: false, default: 0
      t.string :secret, null: false
      t.string :recovery_secret, null: false
      t.integer :recovery_used_bitfield, null: false, default: 0
      t.boolean :recovery_codes_viewed, null: false, default: false
    end

    add_index :business_saml_providers, :business_id
  end
end
