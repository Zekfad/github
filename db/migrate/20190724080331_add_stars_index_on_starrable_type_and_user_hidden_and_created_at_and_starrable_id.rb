# frozen_string_literal: true

class AddStarsIndexOnStarrableTypeAndUserHiddenAndCreatedAtAndStarrableId < GitHub::Migration
  def change
    add_index :stars, [:starrable_type, :user_hidden, :created_at, :starrable_id], name: "starrable_type_and_user_hidden_and_created_at_and_starrable_id"
  end
end
