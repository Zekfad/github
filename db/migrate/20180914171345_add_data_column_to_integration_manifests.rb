# frozen_string_literal: true

class AddDataColumnToIntegrationManifests < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :integration_manifests, :data, :json
  end
end
