# frozen_string_literal: true

class AddEnvironmentIndexToDeploymentStatuses < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :deployment_statuses, [:deployment_id, :environment]
  end
end
