# frozen_string_literal: true

class WorkspacesAddUniqueIndexOnName < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :workspaces, [:name], unique: true
  end
end
