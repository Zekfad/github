# frozen_string_literal: true

class AddTosNotesFieldToBusinesses < GitHub::Migration
  def change
    add_column :businesses, :terms_of_service_notes, :string, after: :terms_of_service_company_name
  end
end
