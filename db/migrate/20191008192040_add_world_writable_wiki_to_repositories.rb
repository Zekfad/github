# frozen_string_literal: true

class AddWorldWritableWikiToRepositories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repositories, :world_writable_wiki, :boolean, default: false, null: false
    add_column :archived_repositories, :world_writable_wiki, :boolean, default: false, null: false
  end
end
