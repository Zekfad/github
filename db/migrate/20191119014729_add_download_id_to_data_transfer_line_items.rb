# frozen_string_literal: true

class AddDownloadIdToDataTransferLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :package_registry_data_transfer_line_items, :download_id, :string, limit: 50
  end
end
