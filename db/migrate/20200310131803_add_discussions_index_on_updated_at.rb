# frozen_string_literal: true

class AddDiscussionsIndexOnUpdatedAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :discussions, [:repository_id, :updated_at]
  end
end
