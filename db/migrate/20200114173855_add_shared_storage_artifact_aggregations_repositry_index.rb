# frozen_string_literal: true

class AddSharedStorageArtifactAggregationsRepositryIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :shared_storage_artifact_aggregations,
      [:billable_owner_type, :repository_visibility, :billable_owner_id, :aggregate_effective_at, :repository_id],
      name: "index_shared_storage_artifact_aggregations_on_repository_id"
  end
end
