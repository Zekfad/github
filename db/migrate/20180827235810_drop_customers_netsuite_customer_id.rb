# frozen_string_literal: true

class DropCustomersNetsuiteCustomerId < GitHub::Migration
  def change
    remove_column :customers, :netsuite_customer_id, :string, limit: 30, null: true
  end
end
