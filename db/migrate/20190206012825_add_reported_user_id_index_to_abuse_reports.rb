# frozen_string_literal: true

class AddReportedUserIdIndexToAbuseReports < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :abuse_reports, :reported_user_id
  end
end
