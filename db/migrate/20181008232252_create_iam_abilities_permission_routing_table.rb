# frozen_string_literal: true

class CreateIamAbilitiesPermissionRoutingTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def up
    create_table :abilities_permissions_routing, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.column :cluster, "tinyint(1) unsigned", null: false, default: 0
      t.column :subject_type, "varchar(60)", null: false
      t.timestamps null: false
    end

    add_index :abilities_permissions_routing, [:subject_type], name: :index_abilities_permissions_routing_by_subject_type
    add_index :abilities_permissions_routing, [:cluster, :subject_type], name: :index_abilities_permissions_routing_by_cluster, unique: true
  end

  def down
    drop_table :abilities_permissions_routing
  end
end
