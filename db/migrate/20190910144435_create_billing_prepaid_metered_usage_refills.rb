# frozen_string_literal: true

class CreateBillingPrepaidMeteredUsageRefills < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :billing_prepaid_metered_usage_refills do |t|
      t.references :owner, polymorphic: { limit: 12 }, null: false
      t.date :expires_on, null: false
      t.integer :amount_in_subunits, null: false
      t.string :currency_code, limit: 3, null: false, default: "USD"
      t.string :zuora_rate_plan_charge_id, limit: 32, null: false
      t.timestamps null: false

      t.index [:owner_id, :owner_type, :expires_on], name: :idx_billing_prepaid_metered_usage_refills_on_owner_and_expiry
      t.index :zuora_rate_plan_charge_id, unique: true, name: :index_billing_prepaid_metered_usage_refills_on_zuora_rpc_id
    end
  end
end
