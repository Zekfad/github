# frozen_string_literal: true

class AddDirectMentionMobilePushToUserNotificationSettings < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def change
    add_column :notification_user_settings, :direct_mention_mobile_push, :boolean, default: false, null: false
  end
end
