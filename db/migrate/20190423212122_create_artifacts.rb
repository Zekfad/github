# frozen_string_literal: true

class CreateArtifacts < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :artifacts do |t|
      t.integer :check_run_id, null: false
      t.text :source_url, null: false
      t.column :name, "varbinary(1024)", null: false

      t.timestamps null: false
    end
    add_index :artifacts, :check_run_id
  end
end
