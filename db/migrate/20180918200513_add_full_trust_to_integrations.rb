# frozen_string_literal: true

class AddFullTrustToIntegrations < GitHub::Migration
  def change
    add_column :integrations, :full_trust, :boolean, default: false, null: false
  end
end
