# frozen_string_literal: true

class AddProfilePins < GitHub::Migration
  def change
    create_table :profile_pins, id: false do |t|
      t.primary_key :id, limit: 8
      t.integer :profile_id, null: false
      t.integer :pinned_item_id, null: false
      t.integer :pinned_item_type, null: false
      t.integer :position, null: false, default: 1
    end

    add_index :profile_pins, [:profile_id, :pinned_item_type, :pinned_item_id],
      unique: true, name: "index_profile_pins_unique"
    add_index :profile_pins, [:pinned_item_type, :pinned_item_id]
    add_index :profile_pins, [:position, :profile_id]
  end
end
