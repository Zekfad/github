# frozen_string_literal: true

class AddRolePermissions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    create_table :role_permissions do |t|
      t.references :roles, null: false
      t.references :fine_grained_permissions, null: false
      t.timestamps null: false
    end
  end
end
