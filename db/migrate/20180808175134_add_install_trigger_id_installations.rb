# frozen_string_literal: true

class AddInstallTriggerIdInstallations < GitHub::Migration
  def change
    add_column :integration_installations, :integration_install_trigger_id, :integer

    remove_index :integration_installations, [:integration_version_id]
    remove_index :integration_installations, name: "index_on_subscription_item_id_and_created_at"
  end
end
