# frozen_string_literal: true

class CreateEnterpriseInstallationUserAccountsUploads < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :enterprise_installation_user_accounts_uploads do |t|
      t.belongs_to :business,                null: false
      t.belongs_to :enterprise_installation, null: true
      t.integer    :uploader_id
      t.string     :content_type,            null: false, limit: 40
      t.integer    :size,                    null: false
      t.string     :name,                    null: false
      t.string     :guid,                    limit: 36
      t.integer    :state,                   null: false, default: 0
      t.integer    :storage_blob_id
      t.string     :oid,                     limit: 64
      t.string     :storage_provider,        limit: 30
      t.integer    :sync_state,              null: false, default: 0

      t.timestamps null: false
    end

    add_index :enterprise_installation_user_accounts_uploads,
      [:business_id, :guid],
      name: "index_enterprise_inst_user_accounts_uploads_on_business_id"

    add_index :enterprise_installation_user_accounts_uploads,
      [:enterprise_installation_id],
      name: "index_enterprise_inst_user_accounts_uploads_on_installation_id"
  end
end
