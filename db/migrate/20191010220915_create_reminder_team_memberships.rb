# frozen_string_literal: true

class CreateReminderTeamMemberships < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :reminder_team_memberships do |t|
      t.integer :reminder_id, null: false
      t.integer :team_id, null: false

      t.timestamps null: false

      t.index [:reminder_id, :team_id], unique: true
      t.index [:team_id]
    end
  end
end
