# frozen_string_literal: true

class CreateOrganizationDomains < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :organization_domains do |t|
      t.integer :organization_id, null: false
      t.boolean :verified, null: false, default: false
      t.string :domain, null: false
    end

    add_index :organization_domains, :organization_id
    add_index :organization_domains, [:domain, :organization_id], unique: true
  end
end
