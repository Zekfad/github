# frozen_string_literal: true

class AddRepositoryNetworkCacheVersionNumberColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repository_networks,
               :cache_version_number,
               :integer,
               null: false,
               default: 0
  end
end
