# frozen_string_literal: true

class AddUserStatuses < GitHub::Migration
  def change
    create_table :user_statuses do |t|
      t.belongs_to :user, null: false
      t.column :emoji, "varbinary(44)", null: true
      t.column :message, "varbinary(800)", null: true
      t.timestamps null: false
    end
    add_index :user_statuses, :user_id, unique: true
  end
end
