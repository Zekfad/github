# frozen_string_literal: true

class AddIndexToOrganizationInvitation < GitHub::Migration

  def change
    add_index :organization_invitations, :created_at
  end
end
