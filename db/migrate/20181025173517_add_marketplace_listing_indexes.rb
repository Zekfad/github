# frozen_string_literal: true

class AddMarketplaceListingIndexes < GitHub::Migration
  def change
    add_index :marketplace_listing_screenshots, [:marketplace_listing_id, :sequence], name: "index_marketplace_listing_screenshots_on_listing_sequence"
    add_index :marketplace_listing_supported_languages, [:marketplace_listing_id, :sequence], name: "index_marketplace_listing_supported_language_on_listing_sequence"
  end
end
