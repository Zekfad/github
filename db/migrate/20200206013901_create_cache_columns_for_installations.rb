# frozen_string_literal: true

class CreateCacheColumnsForInstallations < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :integration_installations, :permissions_cache, :json
    add_column :integration_installations, :repository_selection_cache, :string, limit: 8
  end
end
