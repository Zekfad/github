# frozen_string_literal: true

class DreamlifterCheckRunsColumns < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_runs, :number, :integer, null: true
    add_column :check_runs, :completed_log_url, "varbinary(1024)", null: true
    add_column :check_runs, :completed_log_lines, :integer, null: true
  end
end
