# frozen_string_literal: true

class AddQuantityIndexToSubscriptionItems < GitHub::Migration
  include  GitHub::SafeDatabaseMigrationHelper

  def up
    add_index :subscription_items, [:plan_subscription_id, :quantity, :marketplace_listing_plan_id], name: "index_subscription_items_on_plan_quantity_listing_plan", if_not_exists: true
  end

  def down
    remove_index :subscription_items, name: "index_subscription_items_on_plan_quantity_listing_plan", if_exists: true
  end
end
