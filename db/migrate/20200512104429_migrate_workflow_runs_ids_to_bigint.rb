# frozen_string_literal: true

class MigrateWorkflowRunsIdsToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    change_column :workflow_runs, :check_suite_id, "bigint(11) unsigned NOT NULL"
  end

  def down
    change_column :workflow_runs, :check_suite_id, "int(11) NOT NULL"
  end
end
