# frozen_string_literal: true

class CreateSponsorshipMatchBans < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsorship_match_bans do |t|
      t.integer :sponsorable_id, null: false
      t.integer :sponsorable_type, null: false
      t.integer :sponsor_id, null: false
      t.integer :sponsor_type, null: false

      t.timestamps null: false
    end

    add_index :sponsorship_match_bans, [:sponsorable_id, :sponsorable_type], name: "index_sponsorship_match_bans_on_sponsorable"
    add_index :sponsorship_match_bans, [:sponsor_id, :sponsor_type], name: "index_sponsorship_match_bans_on_sponsor"
  end
end
