# frozen_string_literal: true

class AddRemarketingNoticeColumnsToMarketplaceOrderPreviews < GitHub::Migration
  def change
    add_column :marketplace_order_previews, :retargeting_notice_triggered_at, :datetime, null: true
    add_column :marketplace_order_previews, :retargeting_notice_dismissed_at, :datetime, null: true
  end
end
