# frozen_string_literal: true

class PolymorphicUserRoles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def up
    add_column :user_roles, :actor_type, :string, limit: 60, null: false
    add_column :user_roles, :actor_id, :integer, null: false
    add_index :user_roles, [:actor_id, :actor_type, :role_id, :target_id, :target_type], name: "idx_user_roles_actor_role_and_target"
  end

  def down
    remove_index :user_roles, name: "idx_user_roles_actor_role_and_target"
    remove_column :user_roles, :actor_id
    remove_column :user_roles, :actor_type
  end
end
