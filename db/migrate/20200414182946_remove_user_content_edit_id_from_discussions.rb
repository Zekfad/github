# frozen_string_literal: true

class RemoveUserContentEditIdFromDiscussions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_column :discussion_edits, :user_content_edit_id, :integer, index: { unique: true }
  end
end
