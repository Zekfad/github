# frozen_string_literal: true

class AddStreamingLogUrlToCheckRuns < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_runs, :streaming_log_url, :text, null: true
  end
end
