# frozen_string_literal: true

class AddCheckSuiteIdToCheckRuns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_runs, :check_suite_id, :integer
  end
end
