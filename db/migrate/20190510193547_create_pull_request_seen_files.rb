# frozen_string_literal: true

class CreatePullRequestSeenFiles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :user_reviewed_files do |t|
      t.belongs_to :pull_request, null: false
      t.belongs_to :user, null: false
      t.column :filepath, "varbinary(1024)", null: false
      t.column :head_sha, "varbinary(40)", null: false
      t.timestamps null: false
    end

    create_table :archived_user_reviewed_files do |t|
      t.belongs_to :pull_request, null: false
      t.belongs_to :user, null: false
      t.column :filepath, "varbinary(1024)", null: false
      t.column :head_sha, "varbinary(40)", null: false
      t.timestamps null: false
    end

    add_index :archived_user_reviewed_files, [:pull_request_id]
    add_index :user_reviewed_files, [:pull_request_id, :user_id, :filepath], unique: true, name: "index_reviewed_files_on_pull_user_and_filepath"
    add_index :user_reviewed_files, [:pull_request_id, :user_id], name: "index_reviewed_files_on_pull_id_and_user_id"
  end
end
