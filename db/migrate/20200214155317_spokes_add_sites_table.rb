# frozen_string_literal: true

class SpokesAddSitesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Spokes)
  def change
    create_table :sites do |t|
      t.column :site, "varchar(12)", null: false
      t.column :region, "varchar(8)", null: false
    end

    add_index :sites, :site, unique: true
  end
end
