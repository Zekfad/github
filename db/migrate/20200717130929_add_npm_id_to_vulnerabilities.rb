# frozen_string_literal: true

class AddNpmIdToVulnerabilities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :vulnerabilities, :npm_id, :integer, unsigned: true, null: true, default: nil
    add_index :vulnerabilities, :npm_id, unique: true
  end
end
