# frozen_string_literal: true

class IssueEventDetailsIdBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    change_column :issue_event_details, :id, "bigint(11) NOT NULL AUTO_INCREMENT"
    add_column :issue_event_details, :created_at, :datetime, null: true
    add_column :issue_event_details, :updated_at, :datetime, null: true
    change_column :archived_issue_event_details, :id, "bigint(11) NOT NULL AUTO_INCREMENT"
    add_column :archived_issue_event_details, :created_at, :datetime, null: true
    add_column :archived_issue_event_details, :updated_at, :datetime, null: true
  end
  def self.down
    change_column :issue_event_details, :id, "int(11) NOT NULL AUTO_INCREMENT"
    remove_column :issue_event_details, :created_at
    remove_column :issue_event_details, :updated_at
    change_column :archived_issue_event_details, :id, "int(11) NOT NULL AUTO_INCREMENT"
    remove_column :archived_issue_event_details, :created_at
    remove_column :archived_issue_event_details, :updated_at
  end
end
