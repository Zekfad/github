# frozen_string_literal: true

class AddKeyIdToAssetActorStatuses < GitHub::Migration
  def up
    add_column :asset_actor_statuses , :key_id, :integer, default: 0, null: false

    add_index :asset_actor_statuses, [:asset_type, :owner_id, :actor_id, :key_id], name: "index_asset_actor_statuses_on_type_owner_actor_and_key", unique: true
    remove_index :asset_actor_statuses, name: "index_asset_actor_statuses_on_type_owner_and_actor"
  end

  def down
    remove_column :asset_actor_statuses , :key_id

    add_index :asset_actor_statuses, [:asset_type, :owner_id, :actor_id], name: "index_asset_actor_statuses_on_type_owner_and_actor", unique: true
    remove_index :asset_actor_statuses, name: "index_asset_actor_statuses_on_type_owner_actor_and_key"
  end
end
