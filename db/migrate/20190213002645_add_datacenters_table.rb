# frozen_string_literal: true

class AddDatacentersTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Spokes)
  def change
    create_table :datacenters do |t|
      t.column :datacenter, "varchar(8)", null: false
      t.column :region, "varchar(8)", null: false
    end

    add_index :datacenters, :datacenter, unique: true
  end
end
