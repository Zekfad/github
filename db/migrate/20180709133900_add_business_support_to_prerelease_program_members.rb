# frozen_string_literal: true

class AddBusinessSupportToPrereleaseProgramMembers < GitHub::Migration
  def change
    add_column :prerelease_program_members, :member_id, :integer, null: true, after: :user_id
    add_column :prerelease_program_members, :member_type, :string, limit: 30, null: false, default: "User", after: :member_id
    add_index :prerelease_program_members, [:user_id, :member_type]
    add_index :prerelease_program_members, [:member_id, :member_type]
    remove_index :prerelease_program_members, [:user_id]
  end
end
