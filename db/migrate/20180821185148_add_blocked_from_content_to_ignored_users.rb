# frozen_string_literal: true

class AddBlockedFromContentToIgnoredUsers < GitHub::Migration
  def change
    add_column :ignored_users, :blocked_from_content_id, :integer
    add_column :ignored_users, :blocked_from_content_type, :string, limit: 32
  end
end
