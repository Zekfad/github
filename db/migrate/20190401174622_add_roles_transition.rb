# frozen_string_literal: true

require "github/transitions/20190401174622_add_roles"

class AddRolesTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddRoles.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
