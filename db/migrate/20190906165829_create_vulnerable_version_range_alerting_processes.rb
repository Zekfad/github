# frozen_string_literal: true

class CreateVulnerableVersionRangeAlertingProcesses < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    create_table :vulnerable_version_range_alerting_processes do |t|
      t.integer :vulnerability_alerting_event_id, null: false
      t.integer :vulnerable_version_range_id, null: false
      t.datetime :processed_at
      t.timestamps null: false

      t.index [:vulnerability_alerting_event_id, :vulnerable_version_range_id],
        name: "index_vae_id_and_vvr_id",
        unique: true
      t.index [:vulnerability_alerting_event_id, :processed_at],
        name: "index_vae_id_and_processed_at"
    end
  end
end
