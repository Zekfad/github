# frozen_string_literal: true

class CreateIssueBoosts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :issue_boosts do |t|
      t.belongs_to :user, null: false
      t.belongs_to :issue, null: false
      t.column :value, "tinyint(1) unsigned", null: false, default: 1
      t.timestamps null: false
    end

    add_index :issue_boosts, :issue_id, unique: true
    add_index :issue_boosts, :user_id
  end
end
