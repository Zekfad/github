# frozen_string_literal: true

class AddEncryptedColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Spokes)
  extend GitHub::SafeDatabaseMigrationHelper

  def self.up
    add_column :fileservers, :encrypted, "tinyint(1)", null: false, default: 0, if_not_exists: true
  end

  def self.down
    remove_column :fileservers, :encrypted, if_exists: true
  end
end
