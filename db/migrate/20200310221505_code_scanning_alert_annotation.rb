# frozen_string_literal: true

class CodeScanningAlertAnnotation < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    create_table :code_scanning_alerts do |t|
      t.belongs_to :check_annotation, null: false, index: { unique: true }
      t.timestamps null: false
      t.column :alert_number, :integer, null: false
    end
  end

  def self.down
    drop_table :code_scanning_alerts, if_exists: true
  end
end
