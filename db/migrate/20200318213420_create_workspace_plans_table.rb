# frozen_string_literal: true

class CreateWorkspacePlansTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :workspace_plans do |t|
      t.integer :owner_id, null: false
      t.string :owner_type, limit: 12, null: false

      t.integer :resource_group_id
      t.string :name, limit: 90, null: false

      t.timestamps null: false

      t.index [:owner_id, :owner_type, :resource_group_id],
        unique: true,
        name: "index_on_owner_and_resource_group"
      t.index [:resource_group_id, :name],
        unique: true,
        name: "index_on_resource_group_and_name"
    end
  end
end
