# frozen_string_literal: true

class CreateSourceLoginIndexOnUsers < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    add_index :users, :source_login
  end
end
