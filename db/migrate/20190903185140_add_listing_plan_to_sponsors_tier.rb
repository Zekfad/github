# frozen_string_literal: true

class AddListingPlanToSponsorsTier < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_tiers, :marketplace_listing_plan_id, :integer, null: true
    add_index :sponsors_tiers, :marketplace_listing_plan_id
  end
end
