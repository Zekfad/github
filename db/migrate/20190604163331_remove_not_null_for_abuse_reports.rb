# frozen_string_literal: true

class RemoveNotNullForAbuseReports < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :abuse_reports, :reported_content_id, :integer, null: true
    change_column :abuse_reports, :reported_content_type, :string, limit: 40, null: true
  end

  def down
    change_column :abuse_reports, :reported_content_id, :integer, null: false
    change_column :abuse_reports, :reported_content_type, :string, limit: 40, null: false
  end
end
