# frozen_string_literal: true

class NetworksUnpackedAndBackupHost < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    remove_index :repository_networks, column: [:backup_host, :disk_usage], name: "index_repository_networks_on_backup_host_and_disk_usage"
    add_index    :repository_networks, [:maintenance_status, :unpacked_size_in_mb], name: "index_repository_networks_on_maint_status_and_unpacked_size"
  end
end
