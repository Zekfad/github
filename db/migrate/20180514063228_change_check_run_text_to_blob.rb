# frozen_string_literal: true

class ChangeCheckRunTextToBlob < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    change_column :check_runs, :text, :blob
    change_column :check_runs, :summary, :blob
  end

  def down
    change_column :check_runs, :text, :text
    change_column :check_runs, :summary, :text
  end
end
