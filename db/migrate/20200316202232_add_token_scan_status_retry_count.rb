# frozen_string_literal: true

class AddTokenScanStatusRetryCount < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Notify)

  def up
    add_column :token_scan_statuses, :retry_count, :integer, null: false, default: 0, if_not_exists: true
  end

  def down
    remove_column :token_scan_statuses, :retry_count, if_exists: true
  end
end
