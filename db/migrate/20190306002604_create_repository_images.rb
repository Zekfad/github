# frozen_string_literal: true

class CreateRepositoryImages < GitHub::Migration
  def change
    create_table "repository_images" do |t|
      t.references :repository, null: false

      t.integer    :uploader_id,        null: false
      t.string     :content_type,       null: false, limit: 40
      t.integer    :size,               null: false
      t.string     :name,               null: false
      t.string     :guid,               limit: 36
      t.integer    :state,              default: 0
      t.integer    :storage_blob_id
      t.string     :oid,                limit: 64

      t.timestamps null: false
    end

    add_index :repository_images, [:repository_id]
    add_index :repository_images, [:uploader_id]
  end
end
