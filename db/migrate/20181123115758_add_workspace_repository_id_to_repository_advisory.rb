# frozen_string_literal: true

class AddWorkspaceRepositoryIdToRepositoryAdvisory < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_table :repository_advisories do |t|
      t.belongs_to :workspace_repository, index: { unique: true }
    end
  end
end
