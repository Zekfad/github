# frozen_string_literal: true

class AddBusinessUserAccountIdToEnterpriseInstallationUserAccount < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :enterprise_installation_user_accounts, :business_user_account_id, :integer
    add_index :enterprise_installation_user_accounts, [:business_user_account_id], name: "idx_ent_installation_user_accounts_on_business_user_account_id"
  end
end
