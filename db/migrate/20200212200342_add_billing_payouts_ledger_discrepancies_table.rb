# frozen_string_literal: true

class AddBillingPayoutsLedgerDiscrepanciesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :billing_payouts_ledger_discrepancies do |t|
      t.references :stripe_connect_account, null: false
      t.column :status, "enum('unresolved', 'resolved')", null: false, default: "unresolved"
      t.integer :discrepancy_in_subunits, null: false, default: 0
      t.timestamps null: false

      t.index [:status, :created_at],
        name: "index_payouts_ledger_discrepancies_on_status_and_created_at"
    end
  end
end
