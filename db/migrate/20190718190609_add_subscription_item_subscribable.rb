# frozen_string_literal: true

class AddSubscriptionItemSubscribable < GitHub::Migration
  def change
    add_column(:subscription_items, :subscribable_id, :integer)
    add_column(:subscription_items, :subscribable_type, :integer)
    add_index(:subscription_items,
      [:plan_subscription_id, :subscribable_type, :subscribable_id],
      unique: true,
      name: "index_subscription_items_on_plan_sub_and_subscribable",
    )
    add_index(:subscription_items,
      [:subscribable_type, :subscribable_id, :created_at],
      name: "index_subscription_items_on_subscribable_and_created_at",
    )
    add_index(:subscription_items,
      [:subscribable_type, :subscribable_id, :updated_at],
      name: "index_subscription_items_on_subscribable_and_updated_at",
    )
    add_index(:subscription_items,
      [:plan_subscription_id, :quantity, :subscribable_type, :subscribable_id],
      name: "index_subscription_items_on_plan_quantity_subscribable",
    )
  end
end
