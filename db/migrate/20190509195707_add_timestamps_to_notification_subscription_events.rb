# frozen_string_literal: true

class AddTimestampsToNotificationSubscriptionEvents < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def change
    add_column :notification_subscription_events, :created_at, :datetime, null: true
    add_column :notification_subscription_events, :updated_at, :datetime, null: true
  end
end
