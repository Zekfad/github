# frozen_string_literal: true

class CreateSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsorships do |t|
      t.integer :donor_type, null: false
      t.integer :donor_id,   null: false
      t.integer :maintainer_type, null: false
      t.integer :maintainer_id,   null: false

      t.integer :privacy_level,              index: true, null: false, default: 0
      t.boolean :is_donor_opted_in_to_email, index: true, null: false, default: false

      t.timestamps null: false
    end

    add_index :sponsorships, [:maintainer_id, :maintainer_type],
      name: "index_maintainer"
    add_index :sponsorships, [:donor_id, :donor_type, :maintainer_id, :maintainer_type],
      unique: true, name: "index_sponsorships_unique"
  end
end
