# frozen_string_literal: true

class AddOptionsToReminderEventSubscriptions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :reminder_event_subscriptions, :options, "varbinary(1024)", null: false, default: ""
  end
end
