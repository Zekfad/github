# frozen_string_literal: true

class AddTopContributorIdsToGraphStatus < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :repository_contribution_graph_statuses, :top_contributor_ids, :blob, if_not_exists: true
  end

  def self.down
    remove_column :repository_contribution_graph_statuses, :top_contributor_ids, if_exists: true
  end
end
