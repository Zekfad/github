# frozen_string_literal: true

require "github/transitions/20181016223550_backfill_enterprise_global_customer"

class BackfillEnterpriseGlobalCustomerTransition < GitHub::Migration
  def self.up
    return unless GitHub.single_business_environment?
    transition = GitHub::Transitions::BackfillEnterpriseGlobalCustomer.new
    transition.perform
  end

  def self.down
  end
end
