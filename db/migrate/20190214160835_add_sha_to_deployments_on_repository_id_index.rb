# frozen_string_literal: true

class AddShaToDeploymentsOnRepositoryIdIndex < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    remove_index :deployments, name: "index_deployments_on_repository_id", if_exists: true
    add_index :deployments, [:repository_id, :sha], name: "index_deployments_on_repository_and_sha", if_not_exists: true
  end

  def self.down
    remove_index :deployments, name: "index_deployments_on_repository_and_sha", if_exists: true
    add_index :deployments, :repository_id, name: "index_deployments_on_repository_id", if_not_exists: true
  end
end
