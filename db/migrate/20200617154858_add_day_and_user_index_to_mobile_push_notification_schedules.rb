# frozen_string_literal: true

class AddDayAndUserIndexToMobilePushNotificationSchedules < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Domain::Notifications)

  def change
    add_index(
      :mobile_push_notification_schedules,
      [:user_id, :day],
      name: "index_schedules_on_day_and_user",
      unique: true
    )
  end
end
