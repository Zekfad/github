# frozen_string_literal: true

class AddIntegrationInstallTriggers < GitHub::Migration
  def self.up
    create_table :integration_install_triggers do |t|
      t.integer :integration_id, null: false
      t.integer :install_type, null: false
      t.string  :path, null: true
      t.string  :reason, null: true
    end

    add_index :integration_install_triggers, [:integration_id, :install_type], name: "install_triggers_by_integration_id_and_type"
    add_index :integration_install_triggers, [:install_type]
  end

  def self.down
    drop_table :integration_install_triggers
  end
end
