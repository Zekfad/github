# frozen_string_literal: true

class WebauthnRegistration < GitHub::Migration
  def change
    change_column_null :u2f_registrations, :certificate, true
    add_column :u2f_registrations, :is_webauthn_registration, :boolean, default: false, null: false
    # Specifying `binary` without a limit creates a default blob (With a size
    # limit of 65535).
    add_column :u2f_registrations, :webauthn_attestation, :binary
  end
end
