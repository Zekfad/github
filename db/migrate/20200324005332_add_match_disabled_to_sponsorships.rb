# frozen_string_literal: true

class AddMatchDisabledToSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsorships, :match_disabled_at, :datetime
  end
end
