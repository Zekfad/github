# frozen_string_literal: true

class AddCheckSuiteIndicesForFiltering < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :check_suites, [:repository_id, :github_app_id, :creator_id], name: "by_repo_app_and_creator"
    add_index :check_suites, [:repository_id, :github_app_id, :head_branch], name: "by_repo_app_and_head_branch"
    add_index :check_suites, [:repository_id, :github_app_id, :event], name: "by_repo_app_and_event"
    add_index :check_suites, [:repository_id, :github_app_id, :status], name: "by_repo_app_and_status"
    add_index :check_suites, [:repository_id, :github_app_id, :conclusion], name: "by_repo_app_and_conclusion"
  end
end
