# frozen_string_literal: true

class CreateAutosaveCheckpoints < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :autosave_checkpoints do |t|
      t.references  :repository, null: false
      t.column      :branch, "VARBINARY(1024)", null:  false
      t.column      :sha, "VARCHAR(40)", null: false
      t.column      :commit_sha, "VARCHAR(40)", null: false
      t.timestamps  null: false
    end
    add_index :autosave_checkpoints, [:repository_id, :branch, :sha, :created_at], name: :index_autosave_checkpoints_on_repo_branch_sha_created
  end
end
