# frozen_string_literal: true

class CreateSponsorsFraudReviews < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_fraud_reviews do |t|
      t.integer  :sponsors_listing_id, null: false, index: true
      t.integer  :state, default: 0, null: false, index: true
      t.integer  :reviewer_id, null: true
      t.datetime :reviewed_at, null: true

      t.timestamps null: false
    end
  end
end
