# frozen_string_literal: true

class AddMatchLimitReachedAtToSponsorsListings < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_listings, :match_limit_reached_at, :datetime
  end
end
