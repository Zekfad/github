# frozen_string_literal: true

class AddSswsTokenAndUrlToTeamSyncBusinessTenants < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :team_sync_business_tenants, :ssws_token, :string, null: true
    add_column :team_sync_business_tenants, :url, :text, null: true
  end
end
