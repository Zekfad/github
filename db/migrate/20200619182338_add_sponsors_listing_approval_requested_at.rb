# frozen_string_literal: true

class AddSponsorsListingApprovalRequestedAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_listings, :approval_requested_at, :datetime, null: true
    add_index :sponsors_listings, [:state, :approval_requested_at]
  end
end
