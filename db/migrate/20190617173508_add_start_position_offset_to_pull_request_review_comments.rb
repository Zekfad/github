# frozen_string_literal: true

class AddStartPositionOffsetToPullRequestReviewComments < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :pull_request_review_comments, :start_position_offset, "INT(11) unsigned"

    add_column :archived_pull_request_review_comments, :start_position_offset, "INT(11) unsigned"
  end
end
