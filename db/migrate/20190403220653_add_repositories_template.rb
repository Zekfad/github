# frozen_string_literal: true

class AddRepositoriesTemplate < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repositories, :template, :boolean, default: false, null: false
    add_index :repositories, [:template, :active, :owner_id]

    add_column :archived_repositories, :template, :boolean, default: false, null: false
  end
end
