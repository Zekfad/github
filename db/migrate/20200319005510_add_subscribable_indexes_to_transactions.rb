# frozen_string_literal: true

class AddSubscribableIndexesToTransactions < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper

  def self.up
    add_index :transactions,
      [:current_subscribable_id, :current_subscribable_type],
      name: "index_transactions_on_current_subscribables", if_not_exists: true
    add_index :transactions,
      [:old_subscribable_id, :old_subscribable_type],
      name: "index_transactions_on_old_subscribables", if_not_exists: true
  end

  def self.down
    remove_index :transactions, name: "index_transactions_on_current_subscribables", if_exists: true
    remove_index :transactions, name: "index_transactions_on_old_subscribables", if_exists: true
  end
end
