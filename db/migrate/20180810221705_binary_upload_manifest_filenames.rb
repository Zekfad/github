# frozen_string_literal: true

class BinaryUploadManifestFilenames < GitHub::Migration
  def up
    change_column :upload_manifest_files, :name, "varbinary(1024)"
  end

  def down
    change_column :upload_manifest_files, :name, "varchar(255)"
  end
end
