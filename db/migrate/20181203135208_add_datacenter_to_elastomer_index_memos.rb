# frozen_string_literal: true

class AddDatacenterToElastomerIndexMemos < GitHub::Migration
  def change
    add_column :elastomer_index_memos, :datacenter, :string, limit: 255, null: false, default: "default"
    remove_index :elastomer_index_memos, :name
    add_index :elastomer_index_memos, :datacenter
    add_index :elastomer_index_memos, [:name, :datacenter], unique: true
  end
end
