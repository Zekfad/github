# frozen_string_literal: true

class AddContinuousIntegrationFailuresOnlyToNotificationUserSettings < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Mysql2)

  def change
    add_column :notification_user_settings, :continuous_integration_failures_only, :boolean, default: true, null: false
  end
end
