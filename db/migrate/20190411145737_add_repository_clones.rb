# frozen_string_literal: true

class AddRepositoryClones < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :repository_clones do |t|
      t.integer :template_repository_id, null: false, index: true
      t.integer :clone_repository_id, null: false
      t.integer :state, null: false, default: 0
      t.integer :cloning_user_id, null: false, index: true
      t.timestamps null: false
    end

    add_index :repository_clones, :clone_repository_id, unique: true
    add_index :repository_clones, [:clone_repository_id, :state]
  end
end
