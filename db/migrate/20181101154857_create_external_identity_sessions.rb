# frozen_string_literal: true

class CreateExternalIdentitySessions < GitHub::Migration
  def change
    create_table :external_identity_sessions do |t|
      t.belongs_to :user_session, null: false
      t.belongs_to :external_identity, null: false
      t.datetime   :expires_at, null: false

      t.timestamps null: false
    end

    add_index :external_identity_sessions, :expires_at, name: :index_external_sessions_on_expires_at
    add_index :external_identity_sessions, [:user_session_id, :expires_at], name: :index_external_sessions_on_user_session_id_and_expires_at
  end
end
