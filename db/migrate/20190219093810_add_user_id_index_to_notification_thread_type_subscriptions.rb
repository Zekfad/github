# frozen_string_literal: true

class AddUserIdIndexToNotificationThreadTypeSubscriptions < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    add_index :notification_thread_type_subscriptions, :user_id, if_not_exists: true
  end

  def self.down
    remove_index :notification_thread_type_subscriptions, :user_id, if_exists: true
  end
end
