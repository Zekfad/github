# frozen_string_literal: true

class AddProductUUIDFields < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    add_column :product_uuids, :product_type, :string, limit: 32
    add_column :product_uuids, :product_key, :string, limit: 64
    add_column :product_uuids, :billing_cycle, :integer

    add_index :product_uuids, [:product_type, :product_key]
  end
end
