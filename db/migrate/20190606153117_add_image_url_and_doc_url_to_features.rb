# frozen_string_literal: true

class AddImageUrlAndDocUrlToFeatures < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :features, :image_link, :text
    add_column :features, :documentation_link, :text
  end
end
