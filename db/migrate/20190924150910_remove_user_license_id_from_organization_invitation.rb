# frozen_string_literal: true

class RemoveUserLicenseIdFromOrganizationInvitation < GitHub::Migration
  def up
    remove_index :organization_invitations, :user_license_id
    remove_column :organization_invitations, :user_license_id
  end

  def down
    add_column :organization_invitations, :user_license_id, :integer
    add_index :organization_invitations, :user_license_id
  end
end
