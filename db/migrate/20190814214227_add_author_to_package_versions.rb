# frozen_string_literal: true

class AddAuthorToPackageVersions < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_column :package_versions, :author_id, :integer, null: :true, if_not_exists: true
    add_index :package_versions, :author_id, if_not_exists: true
  end

  def down
    remove_index :package_versions, :author_id, if_exists: true
    remove_column :package_versions, :author_id, if_exists: true
  end
end
