# frozen_string_literal: true

class AuthenticationTokensAuthenticatableIdBigInt < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips

  def up
    change_column :authentication_tokens, :authenticatable_id, :bigint
  end

  def down
    change_column :authentication_tokens, :authenticatable_id, :int
  end
end
