# frozen_string_literal: true

require "github/transitions/20191001141702_tgummerer_spokes_fileserver_site_column"

class TgummererSpokesFileserverSiteColumnTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::TgummererSpokesFileserverSiteColumn.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
