# frozen_string_literal: true

class AddStargazerCountToTopics < GitHub::Migration
  def change
    add_column :topics, :stargazer_count, :integer, null: false, default: 0
  end
end
