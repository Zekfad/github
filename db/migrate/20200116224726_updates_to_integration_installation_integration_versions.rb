# frozen_string_literal: true

class UpdatesToIntegrationInstallationIntegrationVersions < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    # This should _never_ be NULL, lets fix that.
    change_column :integration_installations, :integration_version_id, :integer, null: false

    # This needs to be backfilled, so it can be NULL for now.
    add_column :integration_installations, :integration_version_number, :integer
  end

  def down
    remove_column :integration_installations, :integration_version_number
    change_column :integration_installations, :integration_version_id, :integer, null: true
  end
end
