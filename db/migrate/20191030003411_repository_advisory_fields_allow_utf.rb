# frozen_string_literal: true

class RepositoryAdvisoryFieldsAllowUtf < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    # each of these columns takes user input
    # thus it needs to handle full UTF
    # converting these all to VARBINARY with same length
    # There is no reason for these fields to have extended UTF in their values (we just can not really prevent it)
    # there is no reason to extend the length of these fields
    # also, there are no more columns in this table which directly accept user input, and are not a blob or varbinary
    change_column :repository_advisories, :cve_id,    "varbinary(40)",  null: true
    change_column :repository_advisories, :ecosystem, "varbinary(50)",  null: true
    change_column :repository_advisories, :package,   "varbinary(100)", null: true
  end

  def down
    change_column :repository_advisories, :package,   "VARCHAR(100)", null: true
    change_column :repository_advisories, :ecosystem, "VARCHAR(50)",  null: true
    change_column :repository_advisories, :cve_id,    "VARCHAR(40)",  null: true
  end
end
