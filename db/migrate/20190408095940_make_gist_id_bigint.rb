# frozen_string_literal: true

class MakeGistIdBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Spokes)

  def change
    change_column :gist_replicas, :id, :bigint, auto_increment: true
  end
end
