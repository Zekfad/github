# frozen_string_literal: true

class AddProcessingStatusToSharedStorageAggregations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :shared_storage_artifact_aggregations, :submission_state, "enum('unsubmitted', 'submitted', 'skipped')", default: nil
    add_column :shared_storage_artifact_aggregations, :submission_state_reason, :string, limit: 24, default: nil
    add_index :shared_storage_artifact_aggregations, [:submission_state, :submission_state_reason],
      name: "index_shared_storage_aggregations_on_submission_state_and_reason"
  end
end
