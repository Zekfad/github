# frozen_string_literal: true

class RemoveEncryptedColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Spokes)
  extend GitHub::SafeDatabaseMigrationHelper

  def self.up
    remove_column :fileservers, :encrypted, if_exists: true
  end

  def self.down
    add_column :fileservers, :encrypted, "tinyint(1)", null: false, default: 0, if_not_exists: true
  end
end
