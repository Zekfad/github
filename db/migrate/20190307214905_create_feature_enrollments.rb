# frozen_string_literal: true

class CreateFeatureEnrollments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    create_table :feature_enrollments, if_not_exists: true do |t|
      t.references :feature, index: true, null: false
      t.references :user, index: false, null: false
      t.boolean :enrolled, default: true, null: false

      t.timestamps null: false
    end

    add_index :feature_enrollments, [:user_id, :feature_id], unique: true, if_not_exists: true
  end

  def self.down
    drop_table :feature_enrollments, if_exists: true
  end
end
