# frozen_string_literal: true

class AddHeadBranchToCheckSuites < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    add_column :check_suites, :head_branch, "VARBINARY(1024)", null: true
    add_column :check_suites, :head_sha, :string, limit: 64, null: true
    change_column :check_suites, :branch, "VARBINARY(1024)", null: true

    add_index :check_suites, [:head_sha, :repository_id]
  end

  def down
    remove_column :check_suites, :head_branch
    remove_column :check_suites, :head_sha
    change_column :check_suites, :branch, "VARBINARY(1024)", null: false

    remove_index :check_suites, [:head_sha, :repository_id]
  end
end
