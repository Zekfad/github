# frozen_string_literal: true

class AddSecurityPolicyToCommunityProfile < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :community_profiles, :has_security_policy, :boolean, null: false, default: false
  end
end
