# frozen_string_literal: true

class PlanTrial < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :plan_trials do |t|
      t.belongs_to :user, null: false
      t.belongs_to :pending_plan_change, null: false, index: true
      t.string :plan, null: false, limit: 30

      t.timestamps null: false
    end

    add_index :plan_trials, [:user_id, :plan], unique: true
  end
end
