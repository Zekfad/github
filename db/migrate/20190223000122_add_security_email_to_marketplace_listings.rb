# frozen_string_literal: true

class AddSecurityEmailToMarketplaceListings < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    add_column :marketplace_listings, :security_email, :string
  end
end
