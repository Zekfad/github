# frozen_string_literal: true

class AddAzureSubscriptionIdToEnterpriseAgreements < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :enterprise_agreements, :azure_subscription_id, :string, limit: 36
    add_index :enterprise_agreements, :azure_subscription_id, unique: true
  end
end
