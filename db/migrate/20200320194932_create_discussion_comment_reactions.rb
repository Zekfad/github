# frozen_string_literal: true

class CreateDiscussionCommentReactions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussion_comment_reactions do |t|
      t.belongs_to :discussion_comment, null: false, index: true
      t.belongs_to :user, null: false
      t.boolean :user_hidden, default: false, null: false
      t.string :content, limit: 30, null: false

      t.timestamps null: false

      t.index(
        [:discussion_comment_id, :content, :created_at],
        name: "reactions_on_discussion_comment_content_created",
      )
      t.index(
        [:discussion_comment_id, :user_hidden, :created_at],
        name: "reactions_on_discussion_comment_hidden_created",
      )
      t.index(
        [:user_id, :discussion_comment_id, :content],
        unique: true,
        name: "index_reactions_comment_identity",
      )
      t.index(
        [:user_id, :user_hidden],
        name: "comment_reactions_on_user_hidden_and_user_id",
      )
    end
  end
end
