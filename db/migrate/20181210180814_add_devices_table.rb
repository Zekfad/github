# frozen_string_literal: true

class AddDevicesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :authenticated_devices do |table|
      table.column :user_id, "int(11) unsigned NOT NULL"
      table.string :device_id, null: false, limit: 32
      table.column :display_name, "varbinary(1024)", null: false
      table.datetime :accessed_at, null: false
      table.datetime :approved_at, null: true
      table.timestamps null: false
      table.index [:user_id, :device_id, :approved_at], unique: true, name: :index_authenticated_devices_on_user_id_device_id_and_approved_at
    end
  end
end
