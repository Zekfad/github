# frozen_string_literal: true

class IndexCustomRolesForOrg < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def self.up
    add_index :roles, [:owner_id, :owner_type]
  end

  def self.down
    remove_index :roles, [:owner_id, :owner_type]
  end
end
