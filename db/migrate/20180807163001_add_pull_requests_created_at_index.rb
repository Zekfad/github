# frozen_string_literal: true

class AddPullRequestsCreatedAtIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    remove_index :pull_requests, :user_id
    add_index :pull_requests, [:user_id, :created_at]
  end

  def down
    remove_index :pull_requests, [:user_id, :created_at]
    add_index :pull_requests, :user_id
  end
end
