# frozen_string_literal: true

class AddDiscussionVotes < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussion_votes do |t|
      t.belongs_to :user, null: false
      t.belongs_to :discussion, null: false
      t.belongs_to :comment, null: true
      t.datetime :created_at, null: false
    end

    add_index :discussion_votes, [:user_id, :discussion_id, :comment_id],
      name: "index_discussion_votes_on_user_discussion_comment"
  end
end
