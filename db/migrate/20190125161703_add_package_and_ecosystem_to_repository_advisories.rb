# frozen_string_literal: true

class AddPackageAndEcosystemToRepositoryAdvisories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :repository_advisories, :package, "VARCHAR(100)"
    add_column :repository_advisories, :ecosystem, "VARCHAR(50)"
  end
end
