# frozen_string_literal: true

class CreateFeatures < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    create_table :features, if_not_exists: true do |t|
      t.string :public_name, limit: 40, null: false, index: { unique: true }
      t.string :slug, limit: 60, null: false, index: { unique: true }
      t.mediumblob :description
      t.text :feedback_link
      t.boolean :prerelease, default: true, null: false
      t.boolean :enrolled_by_default, default: false, null: false

      t.timestamps null: false
    end
  end

  def self.down
    drop_table :features, if_exists: true
  end
end
