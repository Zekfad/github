# frozen_string_literal: true

require "github/transitions/20200604135246_remove_create_issue_fgp"

class RemoveCreateIssueFgpTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::RemoveCreateIssueFgp.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
