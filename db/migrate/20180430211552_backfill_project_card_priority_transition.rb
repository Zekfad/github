# frozen_string_literal: true

require "github/transitions/20180430211552_backfill_project_card_priority"

class BackfillProjectCardPriorityTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillProjectCardPriority.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
