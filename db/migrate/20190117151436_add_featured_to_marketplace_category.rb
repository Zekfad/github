# frozen_string_literal: true

class AddFeaturedToMarketplaceCategory < GitHub::Migration
  def change
    add_column :marketplace_categories, :featured, :boolean, null: false, default: false
    add_column :marketplace_categories, :featured_position, :integer, null: true

    add_index :marketplace_categories, %i[featured featured_position], unique: true
  end
end
