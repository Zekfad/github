# frozen_string_literal: true

class CreateMemexProjectItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Memex)

  def change
    create_table :memex_project_items do |t|
      t.references :memex_project, null: false
      t.references :content, polymorphic: { limit: 20 }
      t.references :creator, null: false
      t.string :priority, limit: 20
      t.timestamps null: false
    end

    add_index(
      :memex_project_items,
      [:memex_project_id, :content_type, :content_id],
      unique: true,
      name: "index_memex_project_items_on_memex_project_id_and_content",
    )
    add_index :memex_project_items, [:memex_project_id, :priority], unique: true

    create_table :draft_issues do |t|
      t.column :title, "VARBINARY(1024)"
      t.timestamps null: false
    end
  end
end
