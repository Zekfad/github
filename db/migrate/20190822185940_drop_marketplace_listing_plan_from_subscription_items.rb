# frozen_string_literal: true

class DropMarketplaceListingPlanFromSubscriptionItems < GitHub::Migration
  def up
    remove_index :subscription_items, name: :index_on_marketplace_listing_plan_id_and_created_at
    remove_index :subscription_items, name: :index_on_marketplace_listing_plan_id_and_updated_at
    remove_index :subscription_items, name: :index_subscription_items_on_plan_quantity_listing_plan
    remove_index :subscription_items, name: :index_subscription_items_on_plan_sub_and_list_plan
    remove_column :subscription_items, :marketplace_listing_plan_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
