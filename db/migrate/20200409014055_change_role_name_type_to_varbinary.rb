# frozen_string_literal: true

class ChangeRoleNameTypeToVarbinary < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def self.up
    change_column :roles, :name, "VARBINARY(255)"
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
