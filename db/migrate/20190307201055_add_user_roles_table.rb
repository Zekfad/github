# frozen_string_literal: true

class AddUserRolesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    create_table :user_roles do |t|
      t.references :roles, null: false
      t.integer :user_id, null: false
      t.integer :target_id, null: false
      t.string :target_type, null: false, limit: 60
      t.timestamps null: false
    end
  end
end
