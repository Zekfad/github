# frozen_string_literal: true

class AddIgnoredAtToSponsorsMemberships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_memberships, :ignored_at, :datetime
    add_index :sponsors_memberships, :ignored_at
  end
end
