# frozen_string_literal: true


class AddIndexOnUserIdAndSummaryIdForSavedNotificationEntries < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    add_index :saved_notification_entries, [:user_id, :summary_id], if_not_exists: true
  end

  def self.down
    remove_index :saved_notification_entries, [:user_id, :summary_id], if_exists: true
  end
end
