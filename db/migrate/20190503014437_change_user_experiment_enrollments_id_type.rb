# frozen_string_literal: true

class ChangeUserExperimentEnrollmentsIdType < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
      change_column :user_experiment_enrollments, :id, "bigint(20) NOT NULL AUTO_INCREMENT"
  end

  def down
      change_column :user_experiment_enrollments, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
