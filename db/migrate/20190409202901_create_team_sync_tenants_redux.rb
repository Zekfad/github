# frozen_string_literal: true

class CreateTeamSyncTenantsRedux < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def self.up
    create_table :team_sync_tenants do |t|
      t.integer :organization_id, null: false
      t.integer :provider_type, null: false
      t.string :provider_id, null: false, limit: 100
      t.integer :status, null: false, default: 0
      t.timestamps null: false

      t.index :organization_id, unique: true
    end
  end

  def self.down
    drop_table :team_sync_tenants, if_exists: true
  end
end
