# frozen_string_literal: true

class AddMobileTimeZoneToProfiles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql1)

  def change
    add_column :profiles,
      :mobile_time_zone_name,
      :string,
      limit: 40, # Longest known value is 28, giving some buffer
      null: true
  end
end
