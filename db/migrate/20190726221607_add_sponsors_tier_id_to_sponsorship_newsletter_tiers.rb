# frozen_string_literal: true

class AddSponsorsTierIdToSponsorshipNewsletterTiers < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :sponsorship_newsletter_tiers, :sponsors_tier_id, :integer, if_not_exists: true
    add_index :sponsorship_newsletter_tiers, [:sponsorship_newsletter_id, :sponsors_tier_id], unique: true, name: "index_newsletter_and_tier", if_not_exists: true
    add_index :sponsorship_newsletter_tiers, :sponsors_tier_id, name: "index_newsletter_tiers_on_tier", if_not_exists: true
  end

  def self.down
    remove_index :sponsorship_newsletter_tiers, name: "index_newsletter_tiers_on_tier", if_exists: true
    remove_index :sponsorship_newsletter_tiers, name: "index_newsletter_and_tier", if_exists: true
    remove_column :sponsorship_newsletter_tiers, :sponsors_tier_id, if_exists: true
  end
end
