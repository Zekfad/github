# frozen_string_literal: true

class ChangeActionsUsageWorkflowIndex < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_index :actions_usage_line_items, :workflow_id

    add_index :actions_usage_line_items, [:workflow_id, :created_at]
  end
end
