# frozen_string_literal: true

class AddNormalizedDomainToUserEmails < GitHub::Migration
  def change
    add_column :user_emails, :normalized_domain, :string
    add_index :user_emails, [:normalized_domain, :user_id]
  end
end
