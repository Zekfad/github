# frozen_string_literal: true

class CreateRepositoryContributionGraphStatuses < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :repository_contribution_graph_statuses do |t|
      t.integer   :repository_id,     null: false
      t.column    :job_status_id,     "char(36)"  # SecureRandom.uuid
      t.string    :last_indexed_oid,  limit: 64   # Support a sha256 future
      t.datetime  :last_viewed_at,    null: false
      t.timestamps                    null: false

      t.index :repository_id, unique: true
      t.index :last_viewed_at
    end
  end
end
