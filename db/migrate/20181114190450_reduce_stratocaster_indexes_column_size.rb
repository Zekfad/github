# frozen_string_literal: true

class ReduceStratocasterIndexesColumnSize < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql5)

  def up
    change_column :stratocaster_indexes, :index_key, :string, limit: 32
  end

  def down
    change_column :stratocaster_indexes, :index_key, :string, limit: 255
  end
end
