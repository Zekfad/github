# frozen_string_literal: true
#
class CreateWebPushSubscription < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)
  def up
    create_table :web_push_subscriptions do |t|
      t.text :endpoint, null: false
      t.belongs_to :user, null: false
      t.string :auth, null: false
      t.string :p256dh, null: false

      t.timestamps null: false
    end
    add_index :web_push_subscriptions, :user_id
  end

  def down
    drop_table :web_push_subscriptions
  end
end
