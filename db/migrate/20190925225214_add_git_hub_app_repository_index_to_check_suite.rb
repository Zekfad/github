# frozen_string_literal: true

class AddGitHubAppRepositoryIndexToCheckSuite < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :check_suites, [:github_app_id, :repository_id], name: "index_check_suites_on_github_app_id_repository_id"
  end
end
