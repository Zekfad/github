# frozen_string_literal: true

require "github/transitions/20180821172658_migrate_duplicate_admin_center_business_settings"

class MigrateDuplicateAdminCenterBusinessSettingsTransition < GitHub::Migration
  def self.up
    return unless GitHub.single_business_environment?
    transition = GitHub::Transitions::MigrateDuplicateAdminCenterBusinessSettings.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
