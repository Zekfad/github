# frozen_string_literal: true

class AddAppliedCountToTopics < GitHub::Migration
  def change
    add_column :topics, :applied_count, :integer, null: false, default: 0
    add_index :topics, [:flagged, :name, :applied_count]
    remove_index :topics, [:flagged, :name]
  end
end
