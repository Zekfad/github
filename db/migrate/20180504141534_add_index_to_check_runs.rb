# frozen_string_literal: true

class AddIndexToCheckRuns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :check_runs, [:check_suite_id, :name]
  end
end
