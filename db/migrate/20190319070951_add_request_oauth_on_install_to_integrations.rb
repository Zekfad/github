# frozen_string_literal: true

class AddRequestOauthOnInstallToIntegrations < GitHub::Migration
  def change
    add_column :integrations, :request_oauth_on_install, :boolean, default: false, null: false
  end
end
