# frozen_string_literal: true

class AddEnterpriseUserProvisioningSettings < GitHub::Migration

  def change
    add_column(:business_saml_providers, :provisioning_enabled, :boolean, null: false, default: false)
    add_column(:business_saml_providers, :scim_deprovisioning_enabled, :boolean, null: false, default: false)
    add_column(:business_saml_providers, :saml_deprovisioning_enabled, :boolean, null: false, default: false)
  end
end
