# frozen_string_literal: true

class RemoveDefaultColumnFromIntegrations < GitHub::Migration
  def change
    remove_column :integrations, :default, :boolean, default: false, null: false
  end
end
