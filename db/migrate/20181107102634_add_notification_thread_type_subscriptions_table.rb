# frozen_string_literal: true

class AddNotificationThreadTypeSubscriptionsTable < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    create_table :notification_thread_type_subscriptions do |t|
      t.integer :user_id, null: false
      t.integer :list_id, null: false
      t.string  :list_type, null: false, default: "Repository", limit: 64
      t.string  :thread_type, null: false, limit: 64
      t.datetime :created_at, null: false
    end

    add_index :notification_thread_type_subscriptions,
      [:list_id, :list_type, :user_id, :thread_type],
      unique: true,
      name: "index_notification_tts_by_list_and_user", if_not_exists: true
  end

  def self.down
    drop_table :notification_thread_type_subscriptions
  end
end
