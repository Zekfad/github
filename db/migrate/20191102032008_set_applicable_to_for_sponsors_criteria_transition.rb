# frozen_string_literal: true

require "github/transitions/20191102032008_set_applicable_to_for_sponsors_criteria"

class SetApplicableToForSponsorsCriteriaTransition < GitHub::Migration
  def self.up
    return unless GitHub.sponsors_enabled?
    return unless Rails.development?

    transition = GitHub::Transitions::SetApplicableToForSponsorsCriteria.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
