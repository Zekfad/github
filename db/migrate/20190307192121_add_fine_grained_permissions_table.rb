# frozen_string_literal: true

class AddFineGrainedPermissionsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    create_table :fine_grained_permissions do |t|
      t.string :action, null: false
      t.timestamps  null: false
    end

    add_index :fine_grained_permissions, :action, unique: true
  end
end
