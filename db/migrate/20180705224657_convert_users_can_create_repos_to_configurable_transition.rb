# frozen_string_literal: true

require "github/transitions/20180705224657_convert_users_can_create_repos_to_configurable.rb"

class ConvertUsersCanCreateReposToConfigurableTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::ConvertUsersCanCreateReposToConfigurable.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
