# frozen_string_literal: true

class AddDiscussionTransfersEventId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussion_transfers, :new_discussion_event_id, :integer,
      after: :new_discussion_id
    add_index :discussion_transfers, :new_discussion_event_id
  end
end
