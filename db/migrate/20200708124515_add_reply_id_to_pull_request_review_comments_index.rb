# frozen_string_literal: true

class AddReplyIdToPullRequestReviewCommentsIndex < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips

  include GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    remove_index :pull_request_review_comments, name: :index_pull_request_review_comments_on_pull_request_review_id, if_exists: true
    add_index :pull_request_review_comments, [:pull_request_review_id, :reply_to_id], name: "pull_request_review_id_and_reply_to_id", if_not_exists: true
  end

  def down
    remove_index :pull_request_review_comments, name: :pull_request_review_id_and_reply_to_id, if_exists: true
    add_index :pull_request_review_comments, [:pull_request_review_id], name: "index_pull_request_review_comments_on_pull_request_review_id", if_not_exists: true
  end
end
