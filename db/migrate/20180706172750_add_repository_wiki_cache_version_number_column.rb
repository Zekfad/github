# frozen_string_literal: true

class AddRepositoryWikiCacheVersionNumberColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repository_wikis,
               :cache_version_number,
               :integer,
               null: false,
               default: 1
  end
end
