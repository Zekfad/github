# frozen_string_literal: true

class IndexIssuesOnRepositoryIdAndStateAndPullRequestIdAndUserHiddenAndUserId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :issues, [:repository_id, :state, :pull_request_id, :user_hidden, :user_id], name: "repository_id_and_state_and_pull_request_id_and_user"
    remove_index :issues, [:repository_id, :state, :pull_request_id]
  end
end
