# frozen_string_literal: true

class ChangeMigratableResourcesIdType < GitHub::Migration
  def up
    change_column :migratable_resources, :id, "bigint(20) NOT NULL AUTO_INCREMENT"
  end

  def down
    change_column :migratable_resources, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
