# frozen_string_literal: true

class AddTosColumnsToBusinesses < GitHub::Migration
  def change
    add_column :businesses, :terms_of_service_type, :string, default: "Corporate", limit: 30, null: false
    add_column :businesses, :terms_of_service_company_name, :binary, default: nil, limit: 240, null: true
    add_column :businesses, :billing_term_ends_at, :datetime, null: true
  end
end
