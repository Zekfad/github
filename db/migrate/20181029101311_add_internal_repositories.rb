# frozen_string_literal: true

class AddInternalRepositories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    create_table :internal_repositories do |t|
      t.references  :repository
      t.references  :business
      t.timestamps  null: false
      t.index [:repository_id, :business_id], unique: true
    end
  end
end
