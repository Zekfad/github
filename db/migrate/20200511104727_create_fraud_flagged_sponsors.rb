# frozen_string_literal: true

class CreateFraudFlaggedSponsors < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :fraud_flagged_sponsors do |t|
      t.integer :sponsors_fraud_review_id, null: false
      t.integer :sponsor_id, null: false

      t.string :matched_current_client_id
      t.string :matched_current_ip, limit: 40
      t.string :matched_historical_ip, limit: 40
      t.string :matched_historical_client_id
      t.string :matched_ip_region
      t.text :matched_user_agent

      t.timestamps null: false
    end

    add_index :fraud_flagged_sponsors, [:sponsors_fraud_review_id, :sponsor_id],
      unique: true, name: "index_review_and_sponsor"
  end
end
