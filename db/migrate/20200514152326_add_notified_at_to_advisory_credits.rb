# frozen_string_literal: true

class AddNotifiedAtToAdvisoryCredits < GitHub::Migration
  use_connection_class ApplicationRecord::Collab

  def change
    add_column :advisory_credits, :notified_at, :datetime
  end
end
