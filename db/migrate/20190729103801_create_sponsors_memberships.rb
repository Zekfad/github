# frozen_string_literal: true

class CreateSponsorsMemberships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_memberships do |t|
      t.integer    :state, null: false, default: 0
      t.integer    :sponsorable_type, null: false
      t.integer    :sponsorable_id, null: false
      t.references :survey, null: false
      t.references :reviewer, null: true
      t.boolean    :appeal_allowed, null: false, default: false
      t.timestamps null: false
    end

    add_index :sponsors_memberships, [:sponsorable_id, :sponsorable_type], unique: true,
      name: :index_sponsors_membership_on_sponsorable_type_and_sponsorable_id

    add_index :sponsors_memberships, [:state, :sponsorable_type]
  end
end
