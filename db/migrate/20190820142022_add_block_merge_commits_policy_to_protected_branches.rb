# frozen_string_literal: true

class AddBlockMergeCommitsPolicyToProtectedBranches < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :protected_branches, :block_merge_commits_enforcement_level, :integer,
      null: false, default: "0", if_not_exists: true
    add_column :archived_protected_branches, :block_merge_commits_enforcement_level, :integer,
      null: false, default: "0", if_not_exists: true
  end
end
