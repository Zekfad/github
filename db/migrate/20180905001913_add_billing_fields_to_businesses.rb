# frozen_string_literal: true

class AddBillingFieldsToBusinesses < GitHub::Migration
  def change
    add_column :businesses, :customer_id, :integer, null: true, default: nil
    add_column :businesses, :can_self_serve, :boolean, null: false, default: true
  end
end
