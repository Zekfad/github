# frozen_string_literal: true

class AddSharedStorageArtifactsAggregationsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :shared_storage_artifact_aggregations do |t|
      t.belongs_to :owner, null: false
      t.belongs_to :repository, null: false
      t.timestamp :aggregate_effective_at, null: false
      t.column :repository_visibility, "enum('unknown', 'public', 'private')", null: false, default: "unknown"
      t.integer :aggregate_size_in_bytes, limit: 8, null: false
      t.belongs_to :previous_aggregation, default: nil
      t.belongs_to :synchronization_batch, default: nil
      t.timestamps null: false

      t.index [:owner_id, :repository_id, :aggregate_effective_at],
        name: "index_shared_storage_artifact_aggregations_on_owner_repo_time"
      t.index :synchronization_batch_id, name: "index_on_shared_storage_artifacts_synchronization_batch"
    end
  end
end
