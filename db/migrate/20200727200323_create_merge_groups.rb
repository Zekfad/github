# frozen_string_literal: true

class CreateMergeGroups < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :merge_groups do |t|
      t.references :merge_queue, null: false
      t.string :base_oid, limit: 40
      t.column :locked, "TINYINT(1)", null: true
      t.integer :state, null: false, default: 0
      t.integer :merge_group_entries_count, null: false, default: 0

      t.timestamps null: true

      t.index [:merge_queue_id, :locked], unique: true
      t.index [:merge_queue_id, :state, :merge_group_entries_count], name: "queue_id_state_entry_count"
    end
  end
end
