# frozen_string_literal: true

class AddBusinessSupportToEarlyAccessMemberships < GitHub::Migration
  def up
    add_column :early_access_memberships, :member_id, :integer, null: true, after: :user_id
    add_column :early_access_memberships, :member_type, :string, limit: 30, null: false, default: "User", after: :member_id
    add_index :early_access_memberships, [:user_id, :member_type, :feature_slug], unique: true, name: "idx_early_access_memberships_user_id_member_type_feature_slug"
    add_index :early_access_memberships, [:member_id, :member_type, :feature_slug], unique: true, name: "idx_early_access_memberships_member_id_member_type_feature_slug"
    remove_index :early_access_memberships, [:user_id, :feature_slug]
  end

  def down
    remove_column :early_access_memberships, :member_id
    remove_column :early_access_memberships, :member_type
    remove_index :early_access_memberships, [:user_id, :member_type, :feature_slug], unique: true, name: "idx_early_access_memberships_user_id_member_type_feature_slug"
    remove_index :early_access_memberships, [:member_id, :member_type, :feature_slug], unique: true, name: "idx_early_access_memberships_member_id_member_type_feature_slug"
    add_index :early_access_memberships, [:user_id, :feature_slug], unique: true, name: "index_early_access_memberships_on_user_id_and_feature_slug"
  end
end
