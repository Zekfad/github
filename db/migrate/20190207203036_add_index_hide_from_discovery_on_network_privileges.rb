# frozen_string_literal: true

class AddIndexHideFromDiscoveryOnNetworkPrivileges < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :network_privileges, [:repository_id, :hide_from_discovery], name: "index_repository_and_hide_from_discovery", unique: true
  end
end
