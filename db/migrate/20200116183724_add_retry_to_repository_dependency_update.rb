# frozen_string_literal: true

class AddRetryToRepositoryDependencyUpdate < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :repository_dependency_updates, :retry, :boolean, default: false, null: false
  end
end
