# frozen_string_literal: true

class ChangeColumnNullOnUserLabelsDescription < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    # set any existing nulls to empty string if migration is rolled back
    change_column_null :user_labels, :description, true, ""
  end
end
