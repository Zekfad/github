# frozen_string_literal: true

class AddSubscribableToSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsorships, :subscribable_type, :integer
    add_column :sponsorships, :subscribable_id, :integer
  end
end
