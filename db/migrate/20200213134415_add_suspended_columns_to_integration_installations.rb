# frozen_string_literal: true

class AddSuspendedColumnsToIntegrationInstallations < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    # Allow a user to suspend an installation and allow us to know who performed
    # the action.
    add_belongs_to :integration_installations, :user_suspended_by, index: true
    add_column     :integration_installations, :user_suspended_at, :bigint

    # Allow an integrator to suspend an installation.
    add_column :integration_installations, :integrator_suspended,    :boolean, null: false, default: false
    add_column :integration_installations, :integrator_suspended_at, :bigint

    add_index  :integration_installations, :integrator_suspended
  end
end
