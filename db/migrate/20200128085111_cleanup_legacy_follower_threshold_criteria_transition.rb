# frozen_string_literal: true

require "github/transitions/20200128085111_cleanup_legacy_follower_threshold_criteria"

class CleanupLegacyFollowerThresholdCriteriaTransition < GitHub::Migration
  def self.up
    return unless GitHub.sponsors_enabled?
    return unless Rails.development?

    transition = GitHub::Transitions::CleanupLegacyFollowerThresholdCriteria.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
