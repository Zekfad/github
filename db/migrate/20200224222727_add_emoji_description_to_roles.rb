# frozen_string_literal: true

class AddEmojiDescriptionToRoles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def self.up
    add_column :roles, :description, "VARBINARY(200)"
  end

  def self.down
    remove_column :roles, :description
  end
end
