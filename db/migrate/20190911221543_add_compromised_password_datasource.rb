# frozen_string_literal: true

class AddCompromisedPasswordDatasource < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :compromised_password_datasources do |t|
      t.string :name, limit: 50, null: false
      t.string :version, limit: 50, null: false
      t.timestamps null: false
    end

    add_index :compromised_password_datasources,
      [:name, :version],
      unique: true
  end
end
