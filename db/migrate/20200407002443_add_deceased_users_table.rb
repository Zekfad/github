# frozen_string_literal: true

class AddDeceasedUsersTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :deceased_users do |t|
      t.belongs_to :user, null: false, index: { unique: true }

      t.timestamps null: false
    end
  end
end
