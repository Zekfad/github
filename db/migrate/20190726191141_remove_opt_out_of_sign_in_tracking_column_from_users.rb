# frozen_string_literal: true

class RemoveOptOutOfSignInTrackingColumnFromUsers < GitHub::Migration
  def change
    remove_column :users, :opt_out_of_sign_in_tracking, "tinyint(1)", null: false, default: 0
  end
end
