# frozen_string_literal: true

class AddStaffAccessGrantIdToRepositoryUnlocks < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repository_unlocks, :staff_access_grant_id, :integer
    add_index :repository_unlocks, :staff_access_grant_id
  end
end
