# frozen_string_literal: true

class AddTokenScanResultLocationUniqueIndex < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Notify)

  def up
    remove_index :token_scan_result_locations, column: [:token_scan_result_id], if_exists: true
    add_index :token_scan_result_locations, [:token_scan_result_id, :commit_oid, :path, :start_line, :end_line, :start_column, :end_column], unique: true, name: "index_token_scan_result_locations_on_result_and_location", if_not_exists: true
  end

  def down
    remove_index :token_scan_result_locations, name: "index_token_scan_result_locations_on_result_and_location", if_exists: true
    add_index :token_scan_result_locations, [:token_scan_result_id], if_not_exists: true
  end
end
