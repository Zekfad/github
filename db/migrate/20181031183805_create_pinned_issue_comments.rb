# frozen_string_literal: true

class CreatePinnedIssueComments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :pinned_issue_comments do |t|
      t.belongs_to :issue_comment, null: false
      t.belongs_to :issue, null: false
      t.belongs_to :pinned_by, null: false
      t.timestamps null: false
    end

    add_index :pinned_issue_comments, [:issue_id, :issue_comment_id], unique: true
    add_index :pinned_issue_comments, [:issue_comment_id]
  end
end
