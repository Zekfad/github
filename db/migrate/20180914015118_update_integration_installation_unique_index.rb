# frozen_string_literal: true

class UpdateIntegrationInstallationUniqueIndex < GitHub::Migration
  def change
    # This index had a unique constraint on it.
    remove_index :integration_installations, name: :index_installations_on_integration_and_target
    # We still want the index, but we don't want the uniqueness constraint.
    add_index :integration_installations, [:integration_id, :target_id, :target_type], name: :index_installations_on_integration_and_target
  end
end
