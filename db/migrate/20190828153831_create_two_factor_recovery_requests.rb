# frozen_string_literal: true

class CreateTwoFactorRecoveryRequests < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    create_table :two_factor_recovery_requests do |t|
      t.references :user, null: false

      # first stage of flow - was the user able to provide an OTP?
      t.boolean :otp_verified, default: false, null: false

      # second stage of flow - another piece of information to verify the user
      t.references :oauth_access
      t.references :authenticated_device
      t.references :public_key

      # flag when the user completed the flow
      t.datetime :request_completed_at

      # has someone from staff approved this lockout request?
      t.references :reviewer, null: true
      t.datetime :review_completed_at

      t.timestamps null: false
    end

    add_index :two_factor_recovery_requests, [:user_id, :created_at],
      name: "index_two_factor_recovery_requests_on_user_id_and_created_at"
  end

  def down
    drop_table :two_factor_recovery_requests
  end
end
