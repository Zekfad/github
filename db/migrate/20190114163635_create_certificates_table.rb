# frozen_string_literal: true

class CreateCertificatesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :certificates do |t|
      t.binary :serial,         null: false, limit: 20 # RFC5280 4.1.2.2
      t.binary :issuer_digest,  null: false, limit: 32
      t.binary :subject_digest, null: false, limit: 32
      t.binary :ski_digest,     null: true,  limit: 32
      t.binary :aki_digest,     null: true,  limit: 32
      t.column :der, :blob,     null: false            # Max 65KB
      t.binary :fingerprint,    null: false, limit: 32
      t.timestamps              null: false

      t.index [:aki_digest]
      t.index [:issuer_digest, :serial]
      t.index [:subject_digest, :ski_digest]
      t.index [:fingerprint], unique: true
    end
  end
end
