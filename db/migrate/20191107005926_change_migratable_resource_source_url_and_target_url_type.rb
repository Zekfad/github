# frozen_string_literal: true

class ChangeMigratableResourceSourceUrlAndTargetUrlType < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  def up
    remove_index :migratable_resources, column: [:guid, :source_url], if_exists: true
    change_column :migratable_resources, :source_url, :text, null: false
    change_column :migratable_resources, :target_url, :text, default: nil
    add_index :migratable_resources, [:guid, :source_url], unique: true, length: { guid: 36, source_url: 511 }, if_not_exists: true
  end

  def down
    remove_index :migratable_resources, column: [:guid, :source_url], if_exists: true
    change_column :migratable_resources, :source_url, "varchar(255)", null: false
    change_column :migratable_resources, :target_url, "varchar(255)", default: nil
    add_index :migratable_resources, [:guid, :source_url], unique: true, if_not_exists: true
  end
end
