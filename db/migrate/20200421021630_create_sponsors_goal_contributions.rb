# frozen_string_literal: true

class CreateSponsorsGoalContributions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_goal_contributions do |t|
      t.references :sponsors_goal, null: false
      t.references :sponsor, null: false
      t.references :sponsors_tier, null: false

      t.timestamps null: false
    end

    add_index :sponsors_goal_contributions, :sponsors_goal_id
    add_index :sponsors_goal_contributions, [:sponsor_id, :created_at]
  end
end
