# frozen_string_literal: true

class MigrateColumnsFromCheckSuitesToWorkflowRuns < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :workflow_runs, :event, "VARCHAR(50)", null: true
    add_column :workflow_runs, :action, "VARBINARY(400)", null: true
    add_column :workflow_runs, :name, "VARBINARY(1024)", null: true
    add_column :workflow_runs, :head_branch, "VARBINARY(1024)", null: true
    add_column :workflow_runs, :head_sha, "VARCHAR(64)", null: true
    add_column :workflow_runs, :workflow_file_path, "VARBINARY(1024)", null: true
    add_column :workflow_runs, :completed_log_url, "VARBINARY(1024)", null: true
    add_column :workflow_runs, :external_id, "VARCHAR(64)", null: true
    add_column :workflow_runs, :repository_id, "INT(11)", null: true

    add_index :workflow_runs, [:repository_id, :event],
      name: "index_workflow_runs_on_repository_id_and_event"
    add_index :workflow_runs, [:repository_id, :action],
      name: "index_workflow_runs_on_repository_id_and_action"
    add_index :workflow_runs, [:repository_id, :name],
      name: "index_workflow_runs_on_repository_id_and_name"
    add_index :workflow_runs, [:repository_id, :head_branch],
      name: "index_workflow_runs_on_repository_id_and_head_branch"
  end
end
