# frozen_string_literal: true

require "github/transitions/20190411011109_backfill_integration_aliases"

class BackfillIntegrationAliasesTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillIntegrationAliases.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
