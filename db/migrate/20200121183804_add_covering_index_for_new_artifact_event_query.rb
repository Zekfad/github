# frozen_string_literal: true

class AddCoveringIndexForNewArtifactEventQuery < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :shared_storage_artifact_events, [:owner_id, :source, :repository_visibility, :effective_at, :repository_id, :event_type, :size_in_bytes], name: "index_on_fields_for_stafftools_breakdown"
  end
end
