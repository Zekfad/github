# frozen_string_literal: true

class AddSourceColumnToVulnerabilities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def up
    # Add source and source_identifier columns to the vulnerabilities table to
    # mirror the columns in pending_vulnerabilities.
    add_column :vulnerabilities, :source, :string, limit: 64
    add_column :vulnerabilities, :source_identifier, :string, limit: 128

    add_index :vulnerabilities, [:source, :source_identifier], unique: true

    # Change limits on the corresponding pending_vulnerabilities columns to
    # match the limits on the new vulnerabilities columns.
    change_column :pending_vulnerabilities, :source, :string, limit: 64
    change_column :pending_vulnerabilities, :source_identifier, :string, limit: 128

    # Make way for the new compound, unique index to match the index on the new
    # vulnerabilities columns.
    remove_index :pending_vulnerabilities, :source_identifier

    add_index :pending_vulnerabilities, [:source, :source_identifier], unique: true
  end

  def down
    remove_index :pending_vulnerabilities, [:source, :source_identifier]

    add_index :pending_vulnerabilities, :source_identifier

    change_column :pending_vulnerabilities, :source_identifier, :string, limit: 255
    change_column :pending_vulnerabilities, :source, :string, limit: 255

    remove_index :vulnerabilities, [:source, :source_identifier]

    remove_column :vulnerabilities, :source_identifier
    remove_column :vulnerabilities, :source
  end
end
