# frozen_string_literal: true

class DropNotificationEntriesInMysql2 < GitHub::Migration
  # This migration is part of https://github.com/github/notifications/issues/267
  # and is meant to drop a table in the `mysql2` cluster that we've already moved
  # into the new `notifications_entries` cluster.
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    # We don't have multiple database clusters in Enterprise, so there's no need to drop the tables
    return if GitHub.enterprise?

    drop_table :notification_entries
  end

  def self.down
    # We don't have multiple database clusters in Enterprise
    return if GitHub.enterprise?

    create_table :notification_entries, if_not_exists: true do |t|
      t.references :user, null: false
      t.references :summary, null: false
      t.string :list_type, null: false, default: "Repository", limit: 64
      t.references :list, null: false
      t.string :thread_key, null: false, limit: 80
      t.boolean :unread, null: false, default: 1
      t.string :reason, limit: 40
      t.datetime :updated_at, null: true
      t.datetime :last_read_at, null: true
    end
    change_column :notification_entries, :id, "bigint(11) NOT NULL AUTO_INCREMENT"
    add_index(
      :notification_entries,
      [:user_id, :summary_id],
      unique: true,
      name: "by_summary",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :list_type, :list_id, :thread_key],
      unique: true,
      name: "index_notification_entries_for_user_by_thread",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :reason],
      name: "by_user",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :unread, :reason],
      name: "by_unread_user",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:unread, :updated_at],
      name: "index_notification_entries_on_unread_and_updated_at",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :updated_at],
      name: "index_notification_entries_on_user_id_and_updated_at",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:list_type, :list_id, :thread_key],
      name: "index_notification_entries_for_list_by_thread",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :list_type, :list_id, :reason],
      name: "index_notification_entries_for_user_by_list",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :list_type, :list_id, :unread, :reason],
      name: "index_notification_entries_for_user_by_unread_list",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :unread, :thread_key, :list_type, :list_id],
      name: "user_id_and_unread_and_thread_key_and_list_type_and_list_id",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :unread, :updated_at],
      name: "user_id_and_unread_and_updated_at",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :unread, :list_type, :updated_at],
      name: "user_id_and_unread_and_list_type_and_updated_at",
      if_not_exists: true
    )
    add_index(
      :notification_entries,
      [:user_id, :list_type, :updated_at],
      name: "user_id_and_list_type_and_updated_at",
      if_not_exists: true
    )
  end
end
