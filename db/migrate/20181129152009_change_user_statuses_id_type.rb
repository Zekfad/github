# frozen_string_literal: true

class ChangeUserStatusesIdType < GitHub::Migration
  def up
    change_column :user_statuses, :id, "bigint(20) NOT NULL AUTO_INCREMENT"
  end

  def down
    change_column :user_statuses, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
