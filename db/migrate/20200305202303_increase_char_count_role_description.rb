# frozen_string_literal: true

class IncreaseCharCountRoleDescription < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def self.up
    change_column :roles, :description, "VARBINARY(608)"
  end

  def self.down
    change_column :roles, :description, "VARBINARY(200)"
  end
end
