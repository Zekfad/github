# frozen_string_literal: true

class RemoveShaFromCheckSuites < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    change_column :check_suites, :head_sha, :string, limit: 64, null: false

    remove_column :check_suites, :sha
    remove_column :check_suites, :branch
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
