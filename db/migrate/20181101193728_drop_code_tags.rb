# frozen_string_literal: true

class DropCodeTags < GitHub::Migration
  def up
    drop_table :code_tags
  end
end
