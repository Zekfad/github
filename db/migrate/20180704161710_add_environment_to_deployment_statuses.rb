# frozen_string_literal: true

class AddEnvironmentToDeploymentStatuses < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :deployment_statuses, :environment, :string, null: true, default: nil
    add_column :archived_deployment_statuses, :environment, :string, null: true, default: nil

    # Unused index
    remove_index :deployment_statuses, name: "index_deployment_statuses_on_state"
  end
end
