# frozen_string_literal: true

class AddDiscussionSpotlightsFields < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussion_spotlights, :emoji, "varbinary(44)", null: true, after: :position
    add_column :discussion_spotlights, :preconfigured_color, :integer, limit: 1, null: true,
      after: :emoji
    add_column :discussion_spotlights, :custom_color, "varchar(10)", null: true, after: :preconfigured_color
    add_column :discussion_spotlights, :pattern, :integer, limit: 1, default: 0,
      null: false, after: :custom_color
  end
end
