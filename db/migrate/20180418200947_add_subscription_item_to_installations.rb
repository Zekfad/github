# frozen_string_literal: true

class AddSubscriptionItemToInstallations < GitHub::Migration
  def change
    add_column :integration_installations, :subscription_item_id, :integer
    add_index :integration_installations, [:subscription_item_id, :created_at], name: "index_on_subscription_item_id_and_created_at"
  end
end
