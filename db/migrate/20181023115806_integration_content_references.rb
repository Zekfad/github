# frozen_string_literal: true

class IntegrationContentReferences < GitHub::Migration
  def change
    create_table :integration_content_references do |t|
      t.string      :value, limit: 100, null: false
      t.integer     :reference_type, default: 0, null: false
      t.belongs_to  :integration_version, null: false
      t.timestamps  null: false
      t.index :integration_version_id
    end
  end
end
