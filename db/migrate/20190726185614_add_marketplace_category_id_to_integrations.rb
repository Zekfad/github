# frozen_string_literal: true

class AddMarketplaceCategoryIdToIntegrations < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :integrations, :marketplace_category_id, :integer, null: true
    add_index :integrations, :marketplace_category_id
  end
end
