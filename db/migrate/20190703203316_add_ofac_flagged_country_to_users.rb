# frozen_string_literal: true

class AddOFACFlaggedCountryToUsers < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :users, :ofac_flagged_country, :integer
  end
end
