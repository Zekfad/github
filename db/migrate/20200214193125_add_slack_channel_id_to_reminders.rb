# frozen_string_literal: true

class AddSlackChannelIdToReminders < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :reminders, :slack_channel_id, :string, limit: 9
  end
end
