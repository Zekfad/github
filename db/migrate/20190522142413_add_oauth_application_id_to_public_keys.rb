# frozen_string_literal: true

class AddOauthApplicationIdToPublicKeys < GitHub::Migration
  def self.up
    add_column :public_keys, :oauth_application_id, :integer, null: true
    add_index :public_keys, [:repository_id, :oauth_application_id], name: "index_public_keys_on_repository_id_oauth_application_id", if_not_exists: true
  end

  def self.down
    remove_index :public_keys, name: "index_public_keys_on_repository_id_oauth_application_id", if_exists: true
    remove_column :public_keys, :oauth_application_id
  end
end
