# frozen_string_literal: true

class AddGuidIndexToWorkspaces < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :workspaces, [:guid, :owner_id], unique: true
  end
end
