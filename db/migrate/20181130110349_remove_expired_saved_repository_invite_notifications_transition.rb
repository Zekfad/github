# frozen_string_literal: true

require "github/transitions/20181130110349_remove_expired_saved_repository_invite_notifications"

class RemoveExpiredSavedRepositoryInviteNotificationsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::RemoveExpiredSavedRepositoryInviteNotifications.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
