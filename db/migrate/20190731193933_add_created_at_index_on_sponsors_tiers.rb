# frozen_string_literal: true

class AddCreatedAtIndexOnSponsorsTiers < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :sponsors_tiers, [:name, :sponsors_listing_id, :created_at],
      name: "index_sponsors_tiers_on_name_listing_and_created_at"
  end
end
