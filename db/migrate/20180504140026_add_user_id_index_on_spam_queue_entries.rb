# frozen_string_literal: true

class AddUserIdIndexOnSpamQueueEntries < GitHub::Migration
  def change
    add_index :spam_queue_entries, :user_id
  end
end
