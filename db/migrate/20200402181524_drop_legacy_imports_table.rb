# frozen_string_literal: true

class DropLegacyImportsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql1)

  def up
    drop_table :imports
  end

  def down
    create_table :imports do |t|
      t.belongs_to :repository, index: true
      t.datetime :started_at
      t.datetime :finished_at
      t.text :authors
      t.string :state
      t.string :scm
      t.string :url
      t.string :username
      t.string :password
      t.boolean :success
      t.text :stderr
    end
  end
end
