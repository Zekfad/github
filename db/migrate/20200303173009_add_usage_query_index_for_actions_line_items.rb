# frozen_string_literal: true

class AddUsageQueryIndexForActionsLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_index :actions_usage_line_items, [:synchronization_batch_id, :end_time], name: "index_on_synchronization_batch_id_and_end_time"
  end

  def self.down
    remove_index :actions_usage_line_items, column: [:synchronization_batch_id, :end_time], name: "index_on_synchronization_batch_id_and_end_time"
  end
end
