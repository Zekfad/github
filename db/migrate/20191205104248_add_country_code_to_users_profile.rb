# frozen_string_literal: true

class AddCountryCodeToUsersProfile < GitHub::Migration
  def change
    add_column :profiles, :country_code, "char(2)"
  end
end
