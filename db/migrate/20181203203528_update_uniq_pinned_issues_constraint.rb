# frozen_string_literal: true

class UpdateUniqPinnedIssuesConstraint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    # remove unique index
    remove_index :pinned_issues, [:repository_id, :sort]
    # add index back without unique
    add_index :pinned_issues, [:repository_id, :sort]
  end
end
