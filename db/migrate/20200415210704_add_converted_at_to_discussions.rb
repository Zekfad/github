# frozen_string_literal: true

class AddConvertedAtToDiscussions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussions, :converted_at, :datetime, after: :issue_id
  end
end
