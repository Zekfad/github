# frozen_string_literal: true

class DropReviewFilesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    drop_table :pull_request_review_files
    drop_table :archived_pull_request_review_files
  end

  def down
    create_table :pull_request_review_files do |t|
      t.belongs_to :pull_request_review, null: false
      t.belongs_to :pull_request, null: false
      t.column :filepath, "varbinary(1024)", null: false
      t.column :head_sha, "varbinary(40)", null: false
      t.integer :state, null: false
      t.integer :status, null: false
      t.timestamps null: false
    end

    create_table :archived_pull_request_review_files do |t|
      t.belongs_to :pull_request_review, null: false
      t.belongs_to :pull_request, null: false
      t.column :filepath, "varbinary(1024)", null: false
      t.column :head_sha, "varbinary(40)", null: false
      t.integer :state, null: false
      t.integer :status, null: false
      t.timestamps null: false
    end
  end
end
