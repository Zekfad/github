# frozen_string_literal: true

class CreateStripeConnectAccountsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :stripe_connect_accounts do |t|
      t.references :payable, polymorphic: { limit: 32 }, null: false
      t.string :stripe_account_id, null: false, collation: "utf8_bin"
      t.timestamps null: false
    end

    add_index :stripe_connect_accounts, [:payable_id, :payable_type], unique: true
  end
end
