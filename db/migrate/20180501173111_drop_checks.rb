# frozen_string_literal: true

class DropChecks < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    drop_table :checks
  end
end
