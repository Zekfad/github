# frozen_string_literal: true

class MigrateCheckRunsIdsToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    change_column :check_runs, :id, "bigint(11) unsigned NOT NULL AUTO_INCREMENT"
    change_column :check_runs, :check_suite_id, "bigint(11) unsigned NOT NULL"
  end

  def down
    change_column :check_runs, :id, "int(11) NOT NULL AUTO_INCREMENT"
    change_column :check_runs, :check_suite_id, "int(11) NOT NULL"
  end
end
