# frozen_string_literal: true

class AddIssueEditsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    create_table :issue_edits do |t|
      t.belongs_to :issue, null: false
      t.datetime :edited_at, null: false
      t.belongs_to :editor, null: false
      t.timestamps null: false
      t.belongs_to :performed_by_integration
      t.datetime :deleted_at
      t.belongs_to :deleted_by
      t.mediumblob :diff
      t.integer :user_content_edit_id
      t.index :issue_id
      t.index :user_content_edit_id
    end
  end
end
