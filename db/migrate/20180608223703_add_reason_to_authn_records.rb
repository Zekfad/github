# frozen_string_literal: true

class AddReasonToAuthnRecords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :authentication_records, :flagged_reason, :string, null: true, limit: 32
  end
end
