# frozen_string_literal: true

class AddPreReleaseToPackageVersions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_column :package_versions, :pre_release, :boolean, default: false, null: false
    add_index :package_versions, [:registry_package_id, :pre_release, :version], name: :index_package_versions_on_package_pre_release_and_version
  end

  def down
    remove_column :package_versions, :pre_release
    remove_index :package_versions, name: :index_package_versions_on_package_pre_release_and_version
  end
end
