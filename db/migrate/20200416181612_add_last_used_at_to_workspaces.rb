# frozen_string_literal: true

class AddLastUsedAtToWorkspaces < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :workspaces, :last_used_at, :datetime, after: :updated_at
  end
end
