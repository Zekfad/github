# frozen_string_literal: true

# This is the first step of renaming `created_on` to `contributed_on`
class AddContributedOnToEnterpriseContributionsWithIndexPrecautions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :enterprise_contributions, :contributed_on, :date, null: false

    # To avoid breaking tests, we're:
    #   - creating the new 3-column index without the uniqueness (will be added on step 3)
    #   - removing the old unique index (table is empty and uniqueness is model-checked;
    #                                    plus we'd do it on step 3 anyway)
    add_index :enterprise_contributions, [:user_id, :enterprise_installation_id, :contributed_on], name: "index_enterprise_contributions_on_user_installation_contributed"
    add_index :enterprise_contributions, [:user_id, :contributed_on]

    remove_index :enterprise_contributions, name: "index_enterprise_contributions_on_user_installation_and_created"
  end
end
