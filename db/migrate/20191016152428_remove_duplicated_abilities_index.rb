# frozen_string_literal: true

class RemoveDuplicatedAbilitiesIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::IamAbilities)

  # As reported in https://github.com/github/database-infrastructure/issues/2212
  # the index is a duplicate of index_abilities_on_si_and_st_and_priority_and_at_and_ai
  def change
    remove_index :abilities, column: [:subject_id, :subject_type, :priority], name: "index_abilities_on_subject_id_subject_type_priority"
  end
end
