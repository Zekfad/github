# frozen_string_literal: true

class RemoveUnneededNullConstraintMarketplaceListings < GitHub::Migration
  def up
    change_column :marketplace_listings, :privacy_policy_url, :text, null: true
    change_column :marketplace_listings, :support_url, :text, null: true
    change_column :marketplace_listings, :short_description, :string, null: true
    change_column :marketplace_listings, :full_description, :blob, null: true
  end

  def down
    change_column :marketplace_listings, :privacy_policy_url, :text, null: false
    change_column :marketplace_listings, :support_url, :text, null: false
    change_column :marketplace_listings, :short_description, :string, null: false
    change_column :marketplace_listings, :full_description, :blob, null: false
  end
end
