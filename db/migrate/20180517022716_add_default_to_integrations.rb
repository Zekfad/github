# frozen_string_literal: true

class AddDefaultToIntegrations < GitHub::Migration
  def change
    add_column :integrations, :default, :boolean, default: false, null: false
    add_index :integrations, [:default]
  end
end
