# frozen_string_literal: true

class CreateWorkflowRunsTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :workflow_runs do |t|
      t.references :workflow, null: false
      t.references :check_suite, null: false
      t.integer :run_number, null: false, default: 0 # this will be shown in the UI: an incremental number scoped to the repo

      t.index [:workflow_id, :check_suite_id, :run_number], name: "index_workflow_runs_on_workflow_and_check_suite_and_run_number"
      t.index [:check_suite_id], unique: true
    end
  end
end
