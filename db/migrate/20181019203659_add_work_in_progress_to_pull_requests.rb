# frozen_string_literal: true

class AddWorkInProgressToPullRequests < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    add_column :pull_requests, :work_in_progress, :boolean, null: false, default: false
    add_column :archived_pull_requests, :work_in_progress, :boolean, null: false, default: false
  end

  def self.down
    remove_column :pull_requests, :work_in_progress
    remove_column :archived_pull_requests, :work_in_progress
  end
end
