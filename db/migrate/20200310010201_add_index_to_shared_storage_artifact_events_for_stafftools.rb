# frozen_string_literal: true

class AddIndexToSharedStorageArtifactEventsForStafftools < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_index :shared_storage_artifact_events,
      [:owner_id, :repository_visibility, :effective_at, :repository_id, :source, :aggregation_id, :event_type, :size_in_bytes],
      name: "index_on_fields_for_stafftools_shared_storage_breakdown"
  end

  def down
    remove_index :shared_storage_artifact_events,
      name: "index_on_fields_for_stafftools_shared_storage_breakdown"
  end
end
