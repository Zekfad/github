# frozen_string_literal: true

class ChangeFeatureEnrollmentUserToPolymorphic < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    # This field will no longer be required or used, so no longer needs to be indexed
    remove_index :feature_enrollments, [:user_id, :feature_id]

    change_table(:feature_enrollments) do |t|
      t.string :enrollee_type, limit: 40, null: false
      t.integer :enrollee_id, null: false
      t.change :user_id, :integer, null: true
    end

    add_index :feature_enrollments, [:enrollee_id, :enrollee_type, :feature_id], name: "index_on_enrollee_id_and_enrollee_type_and_feature_id", unique: true
  end

  def down
    remove_index :feature_enrollments, name: "index_on_enrollee_id_and_enrollee_type_and_feature_id"

    change_table(:feature_enrollments) do |t|
      t.remove :enrollee_type
      t.remove :enrollee_id
      t.change :user_id, :integer, null: false
    end

    add_index :feature_enrollments, [:user_id, :feature_id], unique: true
  end
end
