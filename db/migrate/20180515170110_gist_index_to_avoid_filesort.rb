# frozen_string_literal: true

class GistIndexToAvoidFilesort < GitHub::Migration
  def change
    # Intentionally add redundant index to optimize specific query that filters
    # on these 3 columns and sorts by id
    add_index :gists, [:user_id, :public, :delete_flag]

    # Drop dead index
    remove_index :gists, :pushed_at

    # Shorten long string columns
    change_column :gists, :repo_name, :string, limit: 40
    change_column :gists, :host, :string, limit: 20
    change_column :gists, :disabling_reason, :string, limit: 20
    change_column :gists, :maintenance_status, :string, limit: 20

    # Also make the column changes to the archived_gists table
    change_column :archived_gists, :repo_name, :string, limit: 40
    change_column :archived_gists, :host, :string, limit: 20
    change_column :archived_gists, :disabling_reason, :string, limit: 20
    change_column :archived_gists, :maintenance_status, :string, limit: 20
  end
end
