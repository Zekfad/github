# frozen_string_literal: true

class AddDisputesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :billing_disputes do |t|
      t.integer :platform, null: false
      t.string :platform_dispute_id, null: false, collation: "utf8_bin"
      t.integer :amount_in_subunits, null: false, default: 0
      t.string :currency_code, limit: 3, null: false, default: "USD"
      t.string :reason, null: false, default: "general"
      t.string :status, null: false, default: "unknown"
      t.boolean :refundable, null: false, default: false
      t.timestamp :response_due_by, null: true

      t.references :user
      t.references :billing_transaction

      t.timestamps null: false

      t.index [:platform, :platform_dispute_id], unique: true
      t.index :user_id
    end
  end
end
