# frozen_string_literal: true

class CreateRepositoryTasksTable < GitHub::Migration
  def change
    create_table :repository_tasks do |t|
      t.string      :path, null: false
      t.column      :name, "VARBINARY(1024)", null: false
      t.column      :description, :mediumblob
      t.column      :icon_name, "varchar(20)"
      t.column      :color, "varchar(6)"
      t.boolean     :featured, null: false, default: false
      t.belongs_to  :repository, class_name: "Repository",  null: false
      t.timestamps  null: false
    end
    add_index :repository_tasks, [:repository_id, :path], unique: true
    add_index :repository_tasks, [:repository_id, :featured]
  end
end
