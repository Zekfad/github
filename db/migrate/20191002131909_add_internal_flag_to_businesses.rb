# frozen_string_literal: true

class AddInternalFlagToBusinesses < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :businesses, :staff_owned, :boolean, null: false, default: false
  end
end
