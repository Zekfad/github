# frozen_string_literal: true

class AddAppleReceiptToPlanSubscription < GitHub::Migration

  def change
    add_column :plan_subscriptions, :apple_receipt_id, :text, null: true
    add_column :plan_subscriptions, :apple_transaction_id, "varchar(255)", null: true

    add_index :plan_subscriptions, :apple_transaction_id
  end
end
