# frozen_string_literal: true

class AddOrganizationDiscussionPosts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :organization_discussion_posts do |t|
      t.integer :number, null: false
      t.column :title, "varbinary(1024)", null: false
      t.mediumblob :body, null: false
      t.string :formatter, limit: 20
      t.belongs_to :user, null: false, index: true
      t.belongs_to :organization, null: false
      t.datetime :pinned_at
      t.belongs_to :pinned_by_user
      t.boolean :private, null: false, default: 1
      t.timestamps null: false
    end

    add_index :organization_discussion_posts, [:organization_id, :number], unique: true,
      name: "idx_org_discussion_posts_on_organization_id_and_number"
    add_index :organization_discussion_posts, [:organization_id, :pinned_at],
      name: "idx_org_discussion_posts_on_organization_id_and_pinned_at"
    add_index :organization_discussion_posts, [:organization_id, :private],
      name: "idx_org_discussion_posts_on_organization_id_and_private"
  end
end
