# frozen_string_literal: true

class OauthAuthorizationIntegrationVersion < GitHub::Migration
  def self.up
    add_column :oauth_authorizations, :integration_version_number, :int

    add_index :oauth_authorizations, [:application_id, :application_type, :integration_version_number],
      name: "index_authorizations_on_application_id_type_version_number"

    remove_index :oauth_authorizations,
      name: "index_oauth_authorizations_on_application_id_application_type"
  end

  def self.down
    add_index :oauth_authorizations, [:application_id, :application_type],
      name: "index_oauth_authorizations_on_application_id_application_type"

    remove_index :oauth_authorizations,
      name: "index_authorizations_on_application_id_type_version_number"

    remove_column :oauth_authorizations, :integration_version_number
  end
end
