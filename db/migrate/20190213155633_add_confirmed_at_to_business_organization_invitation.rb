# frozen_string_literal: true

class AddConfirmedAtToBusinessOrganizationInvitation < GitHub::Migration
  def change
    add_column :business_organization_invitations, :confirmed_at, :datetime
  end
end
