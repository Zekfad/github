# frozen_string_literal: true

class CreatePendingAutomaticInstallations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    create_table :pending_automatic_installations do |t|
      t.string  :trigger_type, null: false, limit: 50
      t.string  :target_type, null: false, limit: 50
      t.integer :target_id, null: false

      t.integer :status, limit: 1, null: false, default: 0
      t.integer :reason, limit: 1, null: false, default: 0

      t.datetime :installed_at
      t.timestamps null: false
    end

    add_index :pending_automatic_installations,
              [:trigger_type, :target_type, :target_id],
              name: "index_pending_automatic_installations_on_trigger_and_target",
              unique: true

    add_index :pending_automatic_installations,
              [:target_type, :target_id],
              name: "index_pending_automatic_installations_on_target"

    add_index :pending_automatic_installations, [:trigger_type, :status]
  end

  def down
    drop_table :pending_automatic_installations
  end
end
