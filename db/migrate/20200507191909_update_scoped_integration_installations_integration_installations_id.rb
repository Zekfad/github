# frozen_string_literal: true

class UpdateScopedIntegrationInstallationsIntegrationInstallationsId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :scoped_integration_installations, :integration_installation_id, :bigint
  end

  def down
    change_column :scoped_integration_installations, :integration_installation_id, :int
  end
end
