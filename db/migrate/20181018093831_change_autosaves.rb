# frozen_string_literal: true

class ChangeAutosaves < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    remove_index :autosaves, name: :index_autosaves_on_repo_branch_sha_created
    remove_column :autosaves, :path
    remove_column :autosaves, :sha

    add_column :autosaves, :commit_oid, "VARCHAR(40)", null: false
    add_index :autosaves, [:repository_id, :branch, :commit_oid, :created_at], name: :index_autosaves_on_repo_branch_commit_created
  end

  def self.down
    remove_index :autosaves, name: :index_autosaves_on_repo_branch_commit_created
    remove_column :autosaves, :commit_oid

    add_column :autosaves, :sha, "VARCHAR(40)", null: false
    add_column :autosaves, :path, "VARBINARY(1024)", null: false
    add_index :autosaves, [:repository_id, :branch, :sha, :created_at], name: :index_autosaves_on_repo_branch_sha_created
  end

end
