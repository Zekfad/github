# frozen_string_literal: true

class AddDiscussionCommentsDeletedAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_column :discussion_comments, :deleted_at, :datetime
    add_index :discussion_comments, [:user_id, :discussion_id, :user_hidden, :deleted_at],
      name: "idx_discussion_comments_on_user_discussion_user_hidden_deleted"
    remove_index :discussion_comments, name: "idx_discussion_comments_on_user_and_discussion_and_user_hidden"
  end

  def down
    add_index :discussion_comments, [:user_id, :discussion_id, :user_hidden],
      name: "idx_discussion_comments_on_user_and_discussion_and_user_hidden"
    remove_index :discussion_comments,
      name: "idx_discussion_comments_on_user_discussion_user_hidden_deleted"
    remove_column :discussion_comments, :deleted_at
  end
end
