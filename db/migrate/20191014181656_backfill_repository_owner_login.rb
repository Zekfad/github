# frozen_string_literal: true

require "github/transitions/20191014173401_backfill_owner_login"

class BackfillRepositoryOwnerLogin < GitHub::Migration
  def up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillOwnerLogin.new(dry_run: false)
    transition.perform
  end
end
