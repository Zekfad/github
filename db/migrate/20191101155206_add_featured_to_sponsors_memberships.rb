# frozen_string_literal: true

class AddFeaturedToSponsorsMemberships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_memberships, :featured, :bool, default: false
  end
end
