# frozen_string_literal: true

class AddIndexOnAuthenticatedDeviceForAuthenticationRecords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :authentication_records, [:user_id, :authenticated_device_id], name: "index_authentication_records_on_user_id_and_device_id"
  end
end
