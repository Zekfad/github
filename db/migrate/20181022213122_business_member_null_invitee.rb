# frozen_string_literal: true

class BusinessMemberNullInvitee < GitHub::Migration
  def change
    change_column_null :business_member_invitations, :invitee_id, true
  end
end
