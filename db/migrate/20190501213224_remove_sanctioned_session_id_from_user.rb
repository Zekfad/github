# frozen_string_literal: true

class RemoveSanctionedSessionIdFromUser < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    remove_reference :users, :ofac_sanctioned_session, references: :user_sessions
  end
end
