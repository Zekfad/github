# frozen_string_literal: true

class AddPerRepoAssetActorActivity < GitHub::Migration
  def up
    add_column :asset_actor_activities , :repository_id, :integer, default: 0, null: false

    add_index :asset_actor_activities, [:asset_type, :owner_id, :actor_id, :activity_started_at, :repository_id], name: "index_asset_actor_activities_on_type_owner_actor_time_and_repo", unique: true
    add_index :asset_actor_activities, [:asset_type, :owner_id, :repository_id, :activity_started_at], name: "index_asset_actor_activities_on_type_owner_repo_and_time"
    remove_index :asset_actor_activities, name: "index_asset_actor_activities_on_type_owner_actor_and_started_at"
  end

  def down
    add_index :asset_actor_activities, [:asset_type, :owner_id, :actor_id, :activity_started_at], name: "index_asset_actor_activities_on_type_owner_actor_and_started_at", unique: true
    remove_index :asset_actor_activities,  name: "index_asset_actor_activities_on_type_owner_actor_time_and_repo"
    remove_index :asset_actor_activities,  name: "index_asset_actor_activities_on_type_owner_repo_and_time"

    remove_column :asset_actor_activities , :repository_id
  end
end
