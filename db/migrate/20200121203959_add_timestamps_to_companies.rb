# frozen_string_literal: true

class AddTimestampsToCompanies < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_timestamps :companies, null: true
  end
end
