# frozen_string_literal: true

class UpdateSponsorsActivitiesColumns < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Collab)

  def up
    # Tier changes should track the old tier as well as the current one
    add_column :sponsors_activities, :old_sponsors_tier_id, :integer, null: true

    # Clarify that the user in question is the maintainer, which also is polymorphic
    rename_column :sponsors_activities, :user_id, :sponsorable_id
    add_column :sponsors_activities, :sponsorable_type, :integer, null: false

    # Clarify that the actor in question is the sponsor, which also is polymorphic
    rename_column :sponsors_activities, :actor_id, :sponsor_id
    add_column :sponsors_activities, :sponsor_type, :integer, null: false

    # Track the timestamp of the event/activity separately from the AR timestamps
    add_column :sponsors_activities, :timestamp, :datetime, null: false

    # Index needs to handle polymorphic sponsorable
    remove_index :sponsors_activities, column: [:sponsorable_id, :created_at], if_exists: true
    add_index :sponsors_activities, [:sponsorable_id, :sponsorable_type, :timestamp], name: "index_sponsors_activities_on_sponsorable_and_timestamp", if_not_exists: true
  end

  def down
    remove_column :sponsors_activities, :old_sponsors_tier_id

    rename_column :sponsors_activities, :sponsorable_id, :user_id
    remove_column :sponsors_activities, :sponsorable_type

    rename_column :sponsors_activities, :sponsor_id, :actor_id
    remove_column :sponsors_activities, :sponsor_type

    remove_column :sponsors_activities, :timestamp, :datetime, null: false

    add_index :sponsors_activities, [:user_id, :created_at], if_not_exists: true
    remove_index :sponsors_activities, name: "index_sponsors_activities_on_timestamp_and_sponsorable", if_exists: true
  end
end
