# frozen_string_literal: true

class CreateUserLicenses < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :user_licenses do |t|
      t.string :email, null: true
      t.belongs_to :business, index: false, null: false, foreign_key: false
      t.integer :license_type, null: false
      t.belongs_to :user, index: false, null: true, foreign_key: false
      t.integer :license_state, null: false, default: 0

      t.timestamps null: false

      t.index([:business_id, :email], unique: true)
      t.index([:user_id, :business_id], unique: true)
    end
  end
end
