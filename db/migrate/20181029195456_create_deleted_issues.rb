# frozen_string_literal: true

class CreateDeletedIssues < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :deleted_issues do |t|
      t.belongs_to :repository, null: false
      t.belongs_to :deleted_by, null: false
      t.integer :number, null: false
      t.timestamps null: false
    end

    add_index :deleted_issues, [:repository_id, :number], unique: true

    create_table :archived_deleted_issues do |t|
      t.belongs_to :repository, null: false
      t.belongs_to :deleted_by, null: false
      t.integer :number, null: false
      t.timestamps null: false
    end

    add_index :archived_deleted_issues, :repository_id
  end
end
