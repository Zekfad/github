# frozen_string_literal: true

class AddIndexToPlatformTransactionIdOnBillingTransaction < GitHub::Migration
  def change
    add_index :billing_transactions, :platform_transaction_id
  end
end
