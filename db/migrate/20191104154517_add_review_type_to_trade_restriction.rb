# frozen_string_literal: true

class AddReviewTypeToTradeRestriction < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    change_column :trade_controls_restrictions, :type,
                  "enum('unrestricted', 'partial', 'full', 'review')",
                  null: false, default: "unrestricted", index: true
  end

  def down
    change_column :trade_controls_restrictions, :type,
                  "enum('unrestricted', 'partial', 'full')",
                  null: false, default: "unrestricted", index: true
  end
end
