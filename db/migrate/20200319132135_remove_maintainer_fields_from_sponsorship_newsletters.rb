# frozen_string_literal: true

class RemoveMaintainerFieldsFromSponsorshipNewsletters < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    change_column_null :sponsorship_newsletters, :sponsorable_id, false
    change_column_null :sponsorship_newsletters, :sponsorable_type, false

    remove_column :sponsorship_newsletters, :maintainer_id, if_exists: true
    remove_column :sponsorship_newsletters, :maintainer_type, if_exists: true
    remove_index :sponsorship_newsletters,
      name: "index_sponsorship_newsletters_on_maintainer_and_state", if_exists: true
  end

  def self.down
    add_column :sponsorship_newsletters, :maintainer_id, :int, if_not_exists: true
    add_column :sponsorship_newsletters, :maintainer_type, :int, if_not_exists: true

    change_column_null :sponsorship_newsletters, :maintainer_id, true
    change_column_null :sponsorship_newsletters, :maintainer_type, true

    add_index :sponsorship_newsletters, [:maintainer_type, :maintainer_id, :state],
      name: "index_sponsorship_newsletters_on_maintainer_and_state", if_not_exists: true
  end
end
