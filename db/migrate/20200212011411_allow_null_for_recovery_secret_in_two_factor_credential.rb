# frozen_string_literal: true

class AllowNullForRecoverySecretInTwoFactorCredential < GitHub::Migration
  def change
    change_column_null :two_factor_credentials, :recovery_secret, true
  end
end
