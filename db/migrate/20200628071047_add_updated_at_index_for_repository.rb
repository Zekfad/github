# frozen_string_literal: true

class AddUpdatedAtIndexForRepository < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :repositories, [:owner_id, :updated_at], name: "index_repositories_on_owner_id_and_updated_at"
  end
end
