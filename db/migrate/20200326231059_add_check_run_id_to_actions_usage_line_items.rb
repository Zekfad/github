# frozen_string_literal: true

class AddCheckRunIdToActionsUsageLineItems < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :actions_usage_line_items, :check_run_id, :integer

    add_index :actions_usage_line_items, :check_run_id
  end
end
