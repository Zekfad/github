# frozen_string_literal: true

class RemoveNullConstraintFromMarketplaceListingPrimaryCategoryId < GitHub::Migration
  def change
    change_column_null :marketplace_listings, :primary_category_id, true
  end
end
