# frozen_string_literal: true

class AddExpiresAtToUserStatuses < GitHub::Migration
  def change
    add_column :user_statuses, :expires_at, :datetime, null: true
  end
end
