# frozen_string_literal: true

class CreateSiteScopedIntegrationInstallations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :site_scoped_integration_installations, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"

      t.belongs_to :integration, null: false

      t.integer :target_id, null: false
      t.string  :target_type, null: false, limit: 25

      t.timestamps null: false
    end

    add_index :site_scoped_integration_installations, :integration_id

    add_index :site_scoped_integration_installations,
              [:target_id, :target_type],
              name: "index_site_scoped_integration_installations_on_target"

    add_index :site_scoped_integration_installations, :created_at
  end
end
