# frozen_string_literal: true

class AddIndexOnConfigEntries < GitHub::Migration
  def change
    add_index :configuration_entries, [:name, :target_type, :value]
  end
end
