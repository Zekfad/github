# frozen_string_literal: true

class RemoveRepositoryPledgie < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    remove_column :repositories, :pledgie, :integer
    remove_column :archived_repositories, :pledgie, :integer
  end
end
