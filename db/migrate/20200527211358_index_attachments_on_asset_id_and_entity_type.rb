# frozen_string_literal: true

class IndexAttachmentsOnAssetIdAndEntityType < GitHub::Migration
  def change
    add_index :attachments, [:asset_id, :entity_type]
  end
end
