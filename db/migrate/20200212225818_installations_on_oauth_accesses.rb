# frozen_string_literal: true

class InstallationsOnOauthAccesses < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :oauth_accesses, :installation_id,   :bigint
    add_column :oauth_accesses, :installation_type, :string, limit: 33 # Our longest type is 'SiteScopedIntegrationInstallation'

    add_index :oauth_accesses, [:installation_id, :installation_type], unique: true
  end
end
