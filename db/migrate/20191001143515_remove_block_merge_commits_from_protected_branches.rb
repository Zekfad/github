# frozen_string_literal: true

class RemoveBlockMergeCommitsFromProtectedBranches < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    remove_column :protected_branches, :prefix, if_exists: true
    remove_column :protected_branches, :block_merge_commits_enforcement_level, if_exists: true
    remove_index :protected_branches, name: "index_protected_branches_on_repository_id_and_prefix_and_name", if_exists: true

    remove_column :archived_protected_branches, :prefix, if_exists: true
    remove_column :archived_protected_branches, :block_merge_commits_enforcement_level, if_exists: true
    remove_index :archived_protected_branches, name: "archived_protected_branches_repository_id_prefix_name", if_exists: true
  end

  def down
    add_column :protected_branches, :block_merge_commits_enforcement_level, :integer, null: false, default: "0", if_not_exists: true
    add_column :protected_branches, :prefix, :boolean, null: false, default: "0", if_not_exists: true
    add_index :protected_branches, [:repository_id, :prefix, :name],
      name: "index_protected_branches_on_repository_id_and_prefix_and_name", if_not_exists: true

    add_column :archived_protected_branches, :block_merge_commits_enforcement_level, :integer, null: false, default: "0", if_not_exists: true
    add_column :archived_protected_branches, :prefix, :boolean, null: false, default: "0", if_not_exists: true
    add_index :archived_protected_branches, [:repository_id, :prefix, :name],
      name: "archived_protected_branches_repository_id_prefix_name", if_not_exists: true
  end
end
