# frozen_string_literal: true

class AddEncryptedRecoverySecretToTwoFactorCredential < GitHub::Migration
  def change
    add_column :two_factor_credentials, :encrypted_recovery_secret, :binary, limit: 255, null: true
  end
end
