# frozen_string_literal: true

class AddSignOutToCustomMessages < GitHub::Migration

  def change
    add_column :custom_messages, :sign_out_message, :mediumblob
  end
end
