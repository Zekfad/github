# frozen_string_literal: true

class DiscussionCategoryNonNullableEmoji < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :discussion_categories, :emoji, "VARBINARY(44)", null: false, default: ":hash:"
  end

  def down
    change_column :discussion_categories, :emoji, "VARBINARY(44)", null: true
  end
end
