# frozen_string_literal: true

class AddSharedStorageUsageIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_index(
      :shared_storage_artifact_aggregations,
      [:billable_owner_type, :repository_visibility, :billable_owner_id, :aggregate_effective_at, :aggregate_size_in_bytes],
      name: :idx_shared_storage_artifact_aggregations_sum_covering,
    )
    add_index(
      :shared_storage_artifact_aggregations,
      [:billable_owner_type, :repository_visibility, :billable_owner_id, :owner_id, :aggregate_effective_at, :aggregate_size_in_bytes],
      name: :idx_shared_storage_artifact_aggregations_sum_with_owner_covering,
    )
  end
end
