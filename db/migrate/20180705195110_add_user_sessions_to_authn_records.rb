# frozen_string_literal: true

class AddUserSessionsToAuthnRecords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :authentication_records, :user_session_id, :integer
    add_column :authentication_records, :ip_address, :string, limit: 40
    add_index :authentication_records, [:user_session_id, :ip_address], unique: true
  end
end
