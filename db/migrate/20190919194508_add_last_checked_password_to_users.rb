# frozen_string_literal: true

class AddLastCheckedPasswordToUsers < GitHub::Migration

  def change
    add_column :users, :weak_password_check_result, :binary, limit: 128, null: true
  end
end
