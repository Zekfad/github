# frozen_string_literal: true

class AddFiscalHostToSponsorsMembership < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :sponsors_memberships, :fiscal_host, :integer, default: 0, null: false, if_not_exists: true
    add_index :sponsors_memberships, %i(fiscal_host state), if_not_exists: true
  end

  def self.down
    remove_index :sponsors_memberships, %i(fiscal_host state), if_exists: true
    remove_column :sponsors_memberships, :fiscal_host, if_exists: true
  end
end
