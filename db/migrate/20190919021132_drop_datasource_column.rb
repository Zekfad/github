# frozen_string_literal: true

class DropDatasourceColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    remove_column :compromised_passwords, :datasource, "BIGINT UNSIGNED", null: false, default: 0
  end

  def self.down
    add_column :compromised_passwords, :datasource, "BIGINT UNSIGNED", null: false, default: 0
  end
end
