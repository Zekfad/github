# frozen_string_literal: true

class AddSponsorableToSponsorshipNewsletters < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :sponsorship_newsletters, :sponsorable_type, :integer, if_not_exists: true
    add_column :sponsorship_newsletters, :sponsorable_id, :integer, if_not_exists: true

    change_column_null :sponsorship_newsletters, :maintainer_type, true
    change_column_null :sponsorship_newsletters, :maintainer_id, true

    add_index :sponsorship_newsletters,
      [:sponsorable_type, :sponsorable_id, :state],
      name: "index_sponsorship_newsletters_on_sponsorable_and_state", if_not_exists: true
  end

  def self.down
    remove_index :sponsorship_newsletters,
      name: "index_sponsorship_newsletters_on_sponsorable_and_state", if_exists: true

    change_column_null :sponsorship_newsletters, :maintainer_type, false
    change_column_null :sponsorship_newsletters, :maintainer_id, false

    remove_column :sponsorship_newsletters, :sponsorable_type, :integer, if_exists: true
    remove_column :sponsorship_newsletters, :sponsorable_id, :integer, if_exists: true
  end
end
