# frozen_string_literal: true

class RemoveUnusedActionsUsageLineItemsColumns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_column :actions_usage_line_items, :repository_visibility
    remove_column :actions_usage_line_items, :self_hosted

    remove_index :actions_usage_line_items, name: "index_on_billable_owner_and_usage"
    rename_index :actions_usage_line_items, "index_on_billable_owner_and_usage_v2", "index_on_billable_owner_and_usage"
  end

  def down
    add_column :actions_usage_line_items, :repository_visibility,
      :string,
      null: false,
      default: "PRIVATE",
      limit: 30

    add_column :actions_usage_line_items,
      :self_hosted,
      :bool,
      null: false,
      default: false

    rename_index :actions_usage_line_items, "index_on_billable_owner_and_usage", "index_on_billable_owner_and_usage_v2"
    add_index :actions_usage_line_items,
      [:billable_owner_type, :billable_owner_id, :end_time, :job_runtime_environment, :duration_in_milliseconds, :duration_multiplier],
      name: "index_on_billable_owner_and_usage"
  end
end
