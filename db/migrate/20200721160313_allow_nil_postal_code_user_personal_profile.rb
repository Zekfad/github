# frozen_string_literal: true

class AllowNilPostalCodeUserPersonalProfile < GitHub::Migration
  # See https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/migrations-and-transitions/database-migrations-for-dotcom/ for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column_null :user_personal_profiles, :postal_code, true
  end

  def down
    change_column_null :user_personal_profiles, :postal_code, false
  end
end
