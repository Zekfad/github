# frozen_string_literal: true

class AddGhsaIdToVulns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :vulnerabilities, :ghsa_id, :string, limit: 19
    add_index :vulnerabilities, :ghsa_id, unique: true
    add_column :pending_vulnerabilities, :ghsa_id, :string, limit: 19
    add_index :pending_vulnerabilities, :ghsa_id, unique: true
    add_index :pending_vulnerabilities, :created_at
  end
end
