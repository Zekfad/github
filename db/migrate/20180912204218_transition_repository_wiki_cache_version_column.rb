# frozen_string_literal: true

require "github/transitions/20180817153000_backfill_repository_wikis_cache_version_number.rb"

class TransitionRepositoryWikiCacheVersionColumn < GitHub::Migration
  def self.up
    return unless GitHub.enterprise? || Rails.development?
    GitHub::Transitions::BackfillRepositoryWikisCacheVersionNumber.new(dry_run: false).perform
  end

  def self.down
  end
end
