# frozen_string_literal: true

class AddIndexToIssueTransfersNewIssueId < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :issue_transfers, :new_issue_id, if_not_exists: true
  end
end
