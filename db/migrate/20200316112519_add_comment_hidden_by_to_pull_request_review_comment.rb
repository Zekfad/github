# frozen_string_literal: true

class AddCommentHiddenByToPullRequestReviewComment < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :pull_request_review_comments, :comment_hidden_by, :integer, after: :comment_hidden_classifier
    add_column :archived_pull_request_review_comments, :comment_hidden_by, :integer, after: :comment_hidden_classifier
  end
end
