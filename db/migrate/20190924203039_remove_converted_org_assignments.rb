# frozen_string_literal: true

require "github/transitions/20190918214048_remove_converted_org_assignments"

class RemoveConvertedOrgAssignments < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::RemoveConvertedOrgAssignments.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
