# frozen_string_literal: true

class AddExpiresAtToIgnoredUsers < GitHub::Migration
  def change
    add_column :ignored_users, :expires_at, :datetime
    add_index :ignored_users, [:user_id, :expires_at], name: :index_ignored_users_on_user_id_and_expires_at
  end
end
