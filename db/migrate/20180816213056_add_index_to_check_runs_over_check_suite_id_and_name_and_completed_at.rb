# frozen_string_literal: true

class AddIndexToCheckRunsOverCheckSuiteIdAndNameAndCompletedAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    # Note that we can remove the previous index below since it's covered by the
    # newly created index.
    remove_index :check_runs, name: "index_check_runs_on_check_suite_id_and_name"
    add_index :check_runs, [:check_suite_id, :name, :completed_at]
  end
end
