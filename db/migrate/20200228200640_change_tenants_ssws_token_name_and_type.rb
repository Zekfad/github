# frozen_string_literal: true

class ChangeTenantsSswsTokenNameAndType < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :team_sync_tenants, :encrypted_ssws_token, :binary, limit: 1024, null: true
    remove_column :team_sync_tenants, :ssws_token, :string, null: true
  end
end
