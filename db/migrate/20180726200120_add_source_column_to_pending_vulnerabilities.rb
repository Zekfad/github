# frozen_string_literal: true

class AddSourceColumnToPendingVulnerabilities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)
  def up
    add_column :pending_vulnerabilities, :source, :string
    add_column :pending_vulnerabilities, :source_identifier, :string
    add_index :pending_vulnerabilities, :source_identifier

    # the small identifier field is preventing ingested vulns from being inserted idempotently
    change_column :pending_vulnerabilities, :external_identifier, :string, limit: 255
    change_column :vulnerabilities, :identifier, :string, limit: 255
  end

  def down
    remove_column :pending_vulnerabilities, :source
    remove_column :pending_vulnerabilities, :source_identifier

    change_column :pending_vulnerabilities, :external_identifier, :string, limit: 50
    change_column :vulnerabilities, :identifier, :string, limit: 50
  end
end
