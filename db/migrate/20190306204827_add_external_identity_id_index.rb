# frozen_string_literal: true

class AddExternalIdentityIdIndex < GitHub::Migration
  def change
    add_index :external_identity_sessions, [:external_identity_id], unique: false
  end
end
