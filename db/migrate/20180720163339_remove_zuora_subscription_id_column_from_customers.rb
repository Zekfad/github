# frozen_string_literal: true

class RemoveZuoraSubscriptionIdColumnFromCustomers < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper

  def self.up
    remove_column :customers, :zuora_subscription_id
    add_index :customers, :braintree_customer_id, if_not_exists: true
  end

  def self.down
    add_column :customers, :zuora_subscription_id, :string, limit: 32, default: nil, null: true, if_not_exists: true
    add_index :customers, :zuora_subscription_id, unique: true, name: :index_customers_on_zuora_subscription_id, if_not_exists: true
    remove_index :customers, :braintree_customer_id, if_exists: true
  end
end
