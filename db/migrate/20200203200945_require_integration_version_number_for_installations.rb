# frozen_string_literal: true

class RequireIntegrationVersionNumberForInstallations < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def self.up
    change_column :integration_installations, :integration_version_number, :integer, null: false

    add_index :integration_installations, :integration_version_id
    add_index :integration_installations, :integration_version_number
  end

  def self.down
    remove_index :integration_installations, :integration_version_id
    remove_index :integration_installations, :integration_version_number

    change_column :integration_installations, :integration_version_number, :integer, null: true
  end
end
