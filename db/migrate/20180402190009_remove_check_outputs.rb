# frozen_string_literal: true

class RemoveCheckOutputs < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    drop_table :check_outputs
  end

  def down
    create_table :check_outputs do |t|
      t.integer :check_run_id
      t.string :title
      t.text :summary
      t.text :images
      t.text :text
    end
  end
end
