# frozen_string_literal: true

require "github/transitions/20190311112110_backfill_deployment_status_environment"

class BackfillDeploymentStatusEnvironmentTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillDeploymentStatusEnvironment.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
