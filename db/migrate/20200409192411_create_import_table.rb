# frozen_string_literal: true

class CreateImportTable < GitHub::Migration
  self.use_connection_class ApplicationRecord::Collab

  def change
    create_table :imports do |t|
      t.belongs_to :user, null: false
      t.timestamps null: false
      t.boolean :completed, default: false, null: false
    end
  end
end
