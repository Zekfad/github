# frozen_string_literal: true

class AddGhsaIdToRepoAdvis < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :repository_advisories, :ghsa_id, :string, limit: 19
    add_index :repository_advisories, :ghsa_id, unique: true
  end
end
