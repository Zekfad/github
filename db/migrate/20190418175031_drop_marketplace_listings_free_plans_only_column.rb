# frozen_string_literal: true

class DropMarketplaceListingsFreePlansOnlyColumn < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    remove_column :marketplace_listings, :free_plans_only
  end
end
