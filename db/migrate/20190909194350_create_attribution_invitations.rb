# frozen_string_literal: true

class CreateAttributionInvitations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :attribution_invitations do |t|
      t.references :source, index: true, null: false
      t.references :target, null: false
      t.references :creator, index: true, null: false
      t.references :owner, index: true, null: false
      t.integer :state, null: false, default: 0, limit: 1

      t.timestamps null: false
    end

    add_index :attribution_invitations, [:target_id, :owner_id]
  end
end
