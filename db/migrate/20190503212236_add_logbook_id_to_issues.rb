# frozen_string_literal: true

class AddLogbookIdToIssues < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    add_column :issues, :created_by_logbook_id, :string, limit: 36, null: true, if_not_exists: true
    add_index :issues, :created_by_logbook_id, unique: true, name: "index_issues_on_created_by_logbook_id"
    add_column :archived_issues, :created_by_logbook_id, :string, limit: 36, null: true, if_not_exists: true
  end

  def self.down
    remove_column :archived_issues, :created_by_logbook_id, if_exists: true
    remove_index :issues, name: "index_issues_on_created_by_logbook_id", if_exists: true
    remove_column :issues, :created_by_logbook_id, if_exists: true
  end
end
