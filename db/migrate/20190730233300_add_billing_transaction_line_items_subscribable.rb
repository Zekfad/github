# frozen_string_literal: true

class AddBillingTransactionLineItemsSubscribable < GitHub::Migration
  def change
    add_column(:billing_transaction_line_items, :subscribable_id, :integer)
    add_column(:billing_transaction_line_items, :subscribable_type, :integer)
    add_index(:billing_transaction_line_items,
      [:subscribable_type, :subscribable_id, :created_at],
      name: "index_line_items_on_subscribable_and_created_at",
    )
  end
end
