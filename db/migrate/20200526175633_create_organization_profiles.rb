# frozen_string_literal: true

class CreateOrganizationProfiles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :organization_profiles do |t|
      t.string :sponsors_update_email
      t.integer :organization_id, null: false

      t.timestamps null: false
    end
    add_index :organization_profiles, :organization_id, unique: true
  end
end
