# frozen_string_literal: true

class AddCategoryToDiscussions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussions, :discussion_category_id, :integer, null: true
  end
end
