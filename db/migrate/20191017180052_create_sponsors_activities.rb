# frozen_string_literal: true

class CreateSponsorsActivities < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  # Expected use:
  # - line items display the actor login, action description (eg. "started sponsoring you"), tier amount, timestamp
  # - always scoped to a User (user or org)
  # - optionally scoped to a time period
  # - paginated via GraphQL
  #
  # user.sponsors_activity.where("created_at BETWEEN ? AND ?").order(created_at: :desc)
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_activities do |t|
      t.references :user, null: false
      t.integer :actor_id, null: false
      t.integer :action, null: false
      t.references :sponsors_tier, null: true

      t.timestamps null: false
    end

    add_index :sponsors_activities, [:user_id, :created_at]
  end
end
