# frozen_string_literal: true

class AddUserDashboardPins < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :user_dashboard_pins, id: false do |t|
      t.primary_key :id, limit: 8
      t.integer :user_id, null: false
      t.integer :pinned_item_id, null: false
      t.integer :pinned_item_type, null: false
      t.integer :position, null: false, default: 1
      t.timestamps null: false
    end

    add_index :user_dashboard_pins, [:user_id, :pinned_item_type, :pinned_item_id],
      unique: true, name: "index_user_dashboard_pins_unique"
    add_index :user_dashboard_pins, [:user_id, :position]
  end
end
