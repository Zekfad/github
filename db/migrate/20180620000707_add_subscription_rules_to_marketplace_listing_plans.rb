# frozen_string_literal: true

class AddSubscriptionRulesToMarketplaceListingPlans < GitHub::Migration
  def change
    add_column :marketplace_listing_plans, :subscription_rules, :integer
  end
end
