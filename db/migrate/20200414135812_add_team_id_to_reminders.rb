# frozen_string_literal: true

class AddTeamIdToReminders < GitHub::Migration
  self.use_connection_class ApplicationRecord::Collab

  def change
    add_column :reminders, :team_id, :integer
    add_index :reminders, :team_id
  end
end
