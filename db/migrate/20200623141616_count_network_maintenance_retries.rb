# frozen_string_literal: true

class CountNetworkMaintenanceRetries < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repository_networks, :maintenance_retries, :integer, default: 0
  end
end
