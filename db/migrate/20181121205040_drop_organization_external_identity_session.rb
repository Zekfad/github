# frozen_string_literal: true

class DropOrganizationExternalIdentitySession < GitHub::Migration
  def self.up
    drop_table :organization_external_identity_sessions, if_exists: true
  end

  def self.down
    create_table :organization_external_identity_sessions, if_not_exists: true do |t|
      t.belongs_to :user_session, null: false
      t.belongs_to :external_identity, null: false
      t.datetime   :expires_at, null: false

      t.timestamps null: false
    end

    add_index :organization_external_identity_sessions,
                            :expires_at,
                            name: :index_org_external_sessions_on_expires_at, if_not_exists: true

    add_index :organization_external_identity_sessions,
                            [:user_session_id, :expires_at],
                            name: :index_org_external_sessions_on_user_session_id_and_expires_at, if_not_exists: true
  end
end
