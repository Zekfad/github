# frozen_string_literal: true

class CreateCodespacesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    unless table_exists?(:codespaces)
      # Schema copied from workspaces table in collab-structure.sql
      execute <<~SQL
        CREATE TABLE `codespaces` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `repository_id` int(11) NOT NULL,
          `owner_id` int(11) NOT NULL,
          `pull_request_id` int(11) DEFAULT NULL,
          `guid` char(36) DEFAULT NULL,
          `name` varchar(90) NOT NULL,
          `slug` varchar(100) DEFAULT NULL,
          `oid` char(40) NOT NULL,
          `ref` varchar(255) NOT NULL,
          `created_at` datetime NOT NULL,
          `updated_at` datetime NOT NULL,
          `last_used_at` datetime DEFAULT NULL,
          `plan_id` int(11) DEFAULT NULL,
          `location` varchar(40) NOT NULL,
          `state` int(11) NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`),
          UNIQUE KEY `index_codespaces_on_owner_id_and_name` (`owner_id`,`name`),
          UNIQUE KEY `index_codespaces_on_repository_id_and_owner_id_and_name` (`repository_id`,`owner_id`,`name`),
          UNIQUE KEY `index_codespaces_on_guid_and_owner_id` (`guid`,`owner_id`),
          UNIQUE KEY `index_codespaces_on_slug_and_owner_id` (`slug`,`owner_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      SQL
    end
  end

  def down
    execute "DROP TABLE IF EXISTS `codespaces`"
  end
end
