# frozen_string_literal: true

class DropRunIdAndRunProvider < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_column :actions_usage_line_items, :run_id
    remove_column :actions_usage_line_items, :run_provider

    change_column_null :actions_usage_line_items, :job_runtime_environment, false
    change_column_null :actions_usage_line_items, :repository_visibility, false
    change_column_null :actions_usage_line_items, :duration_multiplier, false

    add_index :actions_usage_line_items, :job_id, unique: true
  end

  def down
    add_column :actions_usage_line_items, :run_id, :string, limit: 36, null: false
    add_column :actions_usage_line_items, :run_provider, :string, limit: 20, null: false

    change_column_null :actions_usage_line_items, :job_runtime_environment, true
    change_column_null :actions_usage_line_items, :repository_visibility, true
    change_column_null :actions_usage_line_items, :duration_multiplier, true

    remove_index :actions_usage_line_items, :job_id
  end
end
