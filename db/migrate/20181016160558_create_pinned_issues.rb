# frozen_string_literal: true

class CreatePinnedIssues < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :pinned_issues do |t|
      t.belongs_to :repository, null: false
      t.belongs_to :issue, null: false
      t.belongs_to :pinned_by, null: false
      t.column :sort, :string, limit: 126, null: false
      t.timestamps null: false
    end

    add_index :pinned_issues, [:repository_id, :issue_id], unique: true
    add_index :pinned_issues, [:repository_id, :sort], unique: true
  end
end
