# frozen_string_literal: true

class AddRepositoryToCodespacesBillingEntries < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Collab)
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :codespace_billing_entries, :repository_id, :integer, null: true
  end
end
