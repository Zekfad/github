# frozen_string_literal: true

# SELECT * FROM hive.snapshots_presto.authentication_records where ip_address is null or client is null or octolytics_id is null or ip_address is null;
# 0 records
class MakeAuthRecordColumnsNonNullable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    change_column :authentication_records, :ip_address, :string, limit: 40, null: false
    change_column :authentication_records, :client, :string, limit: 40, null: false
    change_column :authentication_records, :octolytics_id, :string, limit: 32, null: false
  end
end
