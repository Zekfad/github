# frozen_string_literal: true

class AddDiscussionTransfers < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussion_transfers do |t|
      t.belongs_to :old_repository, null: false
      t.belongs_to :old_discussion, null: false, index: true
      t.integer :old_discussion_number, null: false

      t.belongs_to :new_repository, null: false
      t.belongs_to :new_discussion, null: false, index: true

      t.belongs_to :actor, null: false
      t.integer :state, null: false, default: 0
      t.column :reason, "varbinary(1024)"

      t.timestamps null: false
    end

    add_index :discussion_transfers, [:old_repository_id, :old_discussion_number],
      name: "index_discussion_transfers_on_old_repo_and_old_discussion_number"
  end
end
