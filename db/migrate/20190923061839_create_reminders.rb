# frozen_string_literal: true
class CreateReminders < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :reminders do |t|
      t.string :remindable_type, limit: 13, null: false
      t.integer :remindable_id, null: false
      t.integer :reminder_slack_workspace_id, null: false
      t.string :slack_channel, limit: 80, null: false # Slack channels can be 80 characters long (no Emoji)
      t.string :time_zone_name, limit: 40, null: false # Longest known value is 28, giving some buffer

      t.integer :min_age, default: 0, null: false
      t.integer :min_staleness, default: 0, null: false
      t.boolean :include_unassigned_prs, default: false, null: false
      t.boolean :include_reviewed_prs, default: false, null: false
      t.boolean :require_review_request, default: true, null: false
      t.boolean :ignore_draft_prs, default: true, null: false
      t.integer :ignore_after_approval_count, default: 0, null: false

      t.timestamps null: false

      t.index [:reminder_slack_workspace_id]
      t.index [:remindable_type, :remindable_id]
    end
  end
end
