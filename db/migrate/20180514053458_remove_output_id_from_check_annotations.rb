# frozen_string_literal: true

class RemoveOutputIdFromCheckAnnotations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    remove_column :check_annotations, :check_output_id
  end
end
