# frozen_string_literal: true

class AddReasonToAbuseReports < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :abuse_reports, :reason, :integer, null: false, default: 0, limit: 1
    add_index :abuse_reports, [:reported_content_id, :reported_content_type, :reason], name: "index_abuse_reports_on_content_and_reason"
  end
end
