# frozen_string_literal: true

class AddLegalAgreementRequiredToFeatures < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :features, :legal_agreement_required, :boolean, null: false, default: false
    add_index :features, :legal_agreement_required
  end
end
