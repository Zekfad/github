# frozen_string_literal: true

class AddFeaturedDescriptionToSponsorsMemberships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_memberships, :featured_description, "varbinary(256)", null: true
  end
end
