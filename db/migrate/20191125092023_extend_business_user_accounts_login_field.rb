# frozen_string_literal: true

class ExtendBusinessUserAccountsLoginField < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :business_user_accounts, :login, :string, limit: 255, null: false, default: ""
  end

  def down
    change_column :business_user_accounts, :login, :string, limit: 40, null: false, default: ""
  end
end
