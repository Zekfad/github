# frozen_string_literal: true

class RemoveUserIdFromEarlyAccessMemberships < GitHub::Migration
  def up
    remove_index :early_access_memberships, column: [:user_id, :member_type, :feature_slug], unique: true, name: "idx_early_access_memberships_user_id_member_type_feature_slug"
    remove_column :early_access_memberships, :user_id
  end

  def down
    add_column :early_access_memberships, :user_id, :integer, null: true
    add_index :early_access_memberships, [:user_id, :member_type, :feature_slug], unique: true, name: "idx_early_access_memberships_user_id_member_type_feature_slug"
  end
end
