# frozen_string_literal: true

class RemoveRequire2faFromUsers < GitHub::Migration
  def change
    remove_column :users, :require_2fa, :boolean, null: true
  end
end
