# frozen_string_literal: true

class DreamlifterAddArtifactsColumns < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :artifacts, :size, :bigint, default: 0, null: false
    # in a different migration we should drop check_run_id and make check_suite_id non nullable
    add_column :artifacts, :check_suite_id, :integer, null: true
    add_index :artifacts, :check_suite_id

    change_column :artifacts, :check_run_id, :integer, null: true
  end
end
