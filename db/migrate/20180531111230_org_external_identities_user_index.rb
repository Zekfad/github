# frozen_string_literal: true

class OrgExternalIdentitiesUserIndex < GitHub::Migration
  def change
    # add missing index to avoid table scans
    add_index :organization_external_identities, :user_id

    # shorten long string columns
    change_column :organization_external_identities, :provider_type, :string, limit: 40, null: false
    change_column :organization_external_identities, :guid, :string, limit: 36, null: false
  end
end
