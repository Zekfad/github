# frozen_string_literal: true

class GistHostNullable < ActiveRecord::Migration[5.2]
  def up
    change_column :gists, :host, "varchar(20)", default: "dgit."
    change_column :archived_gists, :host, "varchar(20)", default: "dgit."
  end

  def down
    change_column :gists, :host, "varchar(20)", default: nil
    change_column :archived_gists, :host, "varchar(20)", default: nil
  end
end
