# frozen_string_literal: true

class ActivitySponsorTypeCanBeNull < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :sponsors_activities, :sponsor_type, :integer, null: true
  end

  def down
    change_column :sponsors_activities, :sponsor_type, :integer, null: false
  end
end
