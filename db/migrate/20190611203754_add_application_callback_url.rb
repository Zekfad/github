# frozen_string_literal: true

class AddApplicationCallbackUrl < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :application_callback_urls do |t|
      t.blob :url, null: false
      t.integer :application_id, null: false
      t.column :application_type, "VARCHAR(32)", null: false
      t.timestamps null: false
    end

    add_index(
      :application_callback_urls, [:application_id, :application_type, :url],
      unique: true,
      name: :index_callback_urls_on_application_and_url,
      length: { url: 2_000 }
    )
  end
end
