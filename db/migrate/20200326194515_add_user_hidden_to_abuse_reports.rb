# frozen_string_literal: true

class AddUserHiddenToAbuseReports < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :abuse_reports, :user_hidden, :boolean, default: false, null: false
    add_index :abuse_reports, [:reporting_user_id, :user_hidden], name: "index_abuse_reports_on_reporting_user_id_and_user_hidden"
  end
end
