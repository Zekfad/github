# frozen_string_literal: true

class AddPrimaryToMannequinEmail < GitHub::Migration
  def change
    add_column :mannequin_emails, :primary, :boolean, null: false, default: false
  end
end
