# frozen_string_literal: true

class AddRolesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    create_table :roles do |t|
      t.string :name, null: false
      t.integer :owner_id
      t.string :owner_type
      t.timestamps  null: false
    end
  end
end
