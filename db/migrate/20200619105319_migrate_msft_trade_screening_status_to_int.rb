# frozen_string_literal: true

class MigrateMsftTradeScreeningStatusToInt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :user_personal_profiles, :user_id
    add_index :user_personal_profiles, :user_id, unique: true

    change_column :user_personal_profiles, :msft_trade_screening_status, :tinyint, null: false, default: 0
    add_index :user_personal_profiles, :msft_trade_screening_status, name: "index_on_msft_trade_screening_status"
  end

  def down
    remove_index :user_personal_profiles, :user_id
    add_index :user_personal_profiles, :user_id

    change_column :user_personal_profiles, :msft_trade_screening_status, "enum('not_screened', 're_screening_in_progress', 'no_hit', 'hit_in_review', 'true_match', 'ingestion_error', 'conditions_apply_ssi_d', 'conditions_apply_ssi_d30', 'conditions_apply_ssi_e', 'conditions_apply_ssi_e60', 'conditions_apply_ssi_f', 'conditions_apply_ssi_f14', 'conditions_apply_lic_r', 'conditions_apply_lic_a')", null: false, default: "not_screened"
  end
end
