# frozen_string_literal: true

require "github/transitions/20200204190628_add_base_roles"

class AddBaseRolesTransition < GitHub::Migration
  def self.up
    # Ensures transition is only run in:
    # * enterprise dev/test/prod, and
    # * dotcom dev.
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddBaseRoles.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
