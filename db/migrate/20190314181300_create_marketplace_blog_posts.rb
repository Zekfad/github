# frozen_string_literal: true

class CreateMarketplaceBlogPosts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :marketplace_blog_posts do |t|
      t.column :title, "varbinary(200)", null: false
      t.integer :external_post_id, null: false
      t.string :url, limit: 175, null: false
      t.column :description, "varbinary(700)", null: true
      t.string :author, limit: 75, null: false
      t.boolean :featured, default: false, null: false
      t.datetime :published_at, null: false
      t.timestamps null: false
    end

    add_index :marketplace_blog_posts, [:featured, :published_at]
    add_index :marketplace_blog_posts, :external_post_id, unique: true
  end
end
