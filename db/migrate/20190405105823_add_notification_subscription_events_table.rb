# frozen_string_literal: true

class AddNotificationSubscriptionEventsTable < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    create_table :notification_subscription_events, if_not_exists: true do |t|
      t.references :subscription, polymorphic: true, null: false
      t.string :event_name, limit: 32, null: false
    end
    add_index :notification_subscription_events,
                            [:subscription_type, :subscription_id, :event_name],
                            name: :index_events_on_subscription_type_id_and_name,
                            unique: true, if_not_exists: true
  end

  def self.down
    drop_table :notification_subscription_events, if_exists: true
  end
end
