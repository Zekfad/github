# frozen_string_literal: true

class NetworkReplicasBigint < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def self.up
    change_column :network_replicas, :id, "bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT"
  end

  def self.down
    change_column :network_replicas, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
