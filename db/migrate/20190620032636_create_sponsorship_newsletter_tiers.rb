# frozen_string_literal: true

class CreateSponsorshipNewsletterTiers < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsorship_newsletter_tiers do |t|
      t.integer :sponsorship_newsletter_id, null: false
      t.integer :marketplace_listing_plan_id, null: false

      t.timestamps null: false

      t.index([:sponsorship_newsletter_id, :marketplace_listing_plan_id], unique: true, name: "index_newsletter_and_listing_plan")
      t.index(:marketplace_listing_plan_id, name: "index_newsletter_tiers_on_listing_plan")
    end
  end
end
