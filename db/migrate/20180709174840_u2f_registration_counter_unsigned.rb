# frozen_string_literal: true

class U2fRegistrationCounterUnsigned < GitHub::Migration
  def self.up
    change_column :u2f_registrations, :counter, "int(11) UNSIGNED NOT NULL"
  end

  def self.down
    change_column :u2f_registrations, :counter, "int(11) NOT NULL"
  end
end
