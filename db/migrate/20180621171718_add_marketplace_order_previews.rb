# frozen_string_literal: true

class AddMarketplaceOrderPreviews < GitHub::Migration
  def change
    create_table :marketplace_order_previews do |t|
      t.references :user, null: false
      t.references :account, null: false
      t.references :marketplace_listing, null: false
      t.references :marketplace_listing_plan, null: false
      t.integer :quantity, null: false

      t.datetime :viewed_at, null: false
      t.datetime :email_notification_sent_at, null: true

      t.timestamps null: false
    end

    add_index :marketplace_order_previews, [:user_id, :marketplace_listing_id],
      unique: true,
      name: "index_marketplace_order_previews_on_user_id_and_listing_id"
    add_index :marketplace_order_previews, [:viewed_at, :email_notification_sent_at],
      name: "index_marketplace_order_previews_on_viewed_at_and_sent_at"
  end
end
