# frozen_string_literal: true

class AddApplicableToToSponsorsCriteria < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_criteria, :applicable_to, :integer, null: false, default: 0, limit: 1
    add_index :sponsors_criteria, :applicable_to
  end
end
