# frozen_string_literal: true

class AddIsUtcToNotificationThreadTypeSubscriptions < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Mysql2)

  def change
    add_column :notification_thread_type_subscriptions, :is_utc, :boolean, default: false, null: false, if_not_exists: true

    add_index :notification_thread_type_subscriptions, :is_utc, if_not_exists: true
  end
end
