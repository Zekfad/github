# frozen_string_literal: true

class CreateStaffAccessGrants < GitHub::Migration
  def change
    create_table :staff_access_grants do |t|
        t.integer :granted_by_id, null: false
        t.integer :revoked_by_id
        t.belongs_to :staff_access_request, index: true
        t.string :reason, null: false
        t.datetime :revoked_at
        t.datetime :expires_at, null: false
        t.timestamps null: false
    end

    add_index :staff_access_grants, :granted_by_id
    add_reference :staff_access_grants, :accessible, polymorphic: true, index: true
  end
end
