# frozen_string_literal: true

class AddIndexForDocusignEnvelopeId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :sponsors_listings, :docusign_envelope_id, unique: true
  end
end
