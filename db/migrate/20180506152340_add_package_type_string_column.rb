# frozen_string_literal: true

class AddPackageTypeStringColumn < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    return nil if GitHub.enterprise?

    add_column :registry_packages, :registry_package_type, "varchar(40)"

    add_index :registry_packages, [:owner_id, :name, :registry_package_type], name: "index_packages_on_owner_id_and_name_and_registry_package_type"
    add_index :registry_packages, [:repository_id, :name, :registry_package_type], unique: true, name: "index_packages_on_repo_id_and_name_and_registry_package_type"
  end

  def self.down
    return nil if GitHub.enterprise?

    remove_index :registry_packages, name: "index_packages_on_owner_id_and_name_and_registry_package_type", if_exists: true
    remove_index :registry_packages, name: "index_packages_on_repo_id_and_name_and_registry_package_type", if_exists: true

    remove_column :registry_packages, :registry_package_type, if_exists: true
  end
end
