# frozen_string_literal: true

class AddSubmittedAtIndexToPullRequestReviews < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :pull_request_reviews, [:pull_request_id, :submitted_at]
  end
end
