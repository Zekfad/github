# frozen_string_literal: true

class AddDataSourceToCompromisedPasswords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :compromised_passwords, :datasource, "BIGINT UNSIGNED", null: false, default: 0
  end
end
