# frozen_string_literal: true

class AddFeaturedAtIndexToMarketplaceListings < GitHub::Migration
  def change
    add_index :marketplace_listings, [:state, :featured_at]
    remove_index :marketplace_listings, name: "index_marketplace_listings_on_state"
  end
end
