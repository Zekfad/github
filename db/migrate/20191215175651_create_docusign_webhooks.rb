# frozen_string_literal: true

class CreateDocusignWebhooks < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :docusign_webhooks do |t|
      t.string :fingerprint, null: false, limit: 64
      t.text :payload, null: false
      t.datetime :processed_at
      t.timestamps null: false

      t.index :fingerprint, unique: true
      t.index :processed_at
    end
  end
end
