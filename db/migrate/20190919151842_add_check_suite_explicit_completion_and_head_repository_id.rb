# frozen_string_literal: true

class AddCheckSuiteExplicitCompletionAndHeadRepositoryId < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites, :explicit_completion, :boolean, null: false, default: false
    add_column :check_suites, :head_repository_id, :integer, null: true
  end
end
