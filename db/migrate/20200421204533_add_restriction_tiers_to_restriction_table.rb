# frozen_string_literal: true

class AddRestrictionTiersToRestrictionTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips

  def up
      change_column :trade_controls_restrictions, :type,
                  "enum('unrestricted', 'partial', 'full', 'review', 'tier_0', 'tier_1')",
                  null: false, default: "unrestricted"
  end

  def down
      change_column :trade_controls_restrictions, :type,
                  "enum('unrestricted', 'partial', 'full', 'review')",
                  null: false, default: "unrestricted"
  end
end
