# frozen_string_literal: true

require "github/transitions/20190909221516_delete_orphaned_review_requests_for_deleted_teams"

class DeleteOrphanedReviewRequestsForDeletedTeamsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DeleteOrphanedReviewRequestsForDeletedTeams.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
