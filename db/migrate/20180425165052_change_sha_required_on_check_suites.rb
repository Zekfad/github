# frozen_string_literal: true

class ChangeShaRequiredOnCheckSuites < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    change_column :check_suites, :sha, :string, limit: 64, null: true
  end

  def self.down
    change_column :check_suites, :sha, :string, limit: 64, null: false
  end
end
