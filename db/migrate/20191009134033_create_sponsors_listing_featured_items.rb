# frozen_string_literal: true

class CreateSponsorsListingFeaturedItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_listing_featured_items do |t|
      t.references :sponsors_listing, null: false
      t.integer    :featureable_type, null: false
      t.integer    :featureable_id, null: false
      t.integer    :position, null: false, default: 1
      t.tinyblob   :description, null: true

      t.timestamps null: false
    end

    add_index :sponsors_listing_featured_items, [:sponsors_listing_id, :featureable_type, :featureable_id],
      unique: true, name: :index_sponsors_listing_featured_items_unique
    add_index :sponsors_listing_featured_items, [:featureable_type, :featureable_id],
      name: :index_sponsors_listing_featured_items_on_featureable
  end
end
