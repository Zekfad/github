# frozen_string_literal: true

class AddCreatorToImport < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class ApplicationRecord::Collab

  def change
    change_table :imports do |t|
      t.belongs_to :creator
    end
  end
end
