# frozen_string_literal: true

class AddCommentAndUserColumnsToPullRequestReviewFiles < GitHub::Migration

  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :pull_request_review_files, :pull_request_review_comment_id, "int(11) unsigned", if_not_exists: true

    add_column :pull_request_review_files, :user_id, :integer, if_not_exists: true

    remove_index :pull_request_review_files, name: "index_review_files_on_pull_id_and_state", if_exists: true

    add_index :pull_request_review_files, [:pull_request_id, :state, :filepath, :user_id], name: "index_review_files_on_pull_request_id_state_filepath_and_user", if_not_exists: true
  end

  def self.down
    remove_index :pull_request_review_files, name: "index_review_files_on_pull_request_id_state_filepath_and_user", if_exists: true

    add_index :pull_request_review_files, [:pull_request_id, :state], name: "index_review_files_on_pull_id_and_state", if_not_exists: true

    remove_column :pull_request_review_files, :user_id, if_exists: true

    remove_column :pull_request_review_files, :pull_request_review_comment_id, if_exists: true
  end
end
