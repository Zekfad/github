# frozen_string_literal: true

class EnablePolymorphicIntegrationOwnerMigration < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def up
    add_column :integrations, :owner_type, :string, null: false,
      default: "User",
      limit: 32,
      after: :owner_id

    add_index :integrations, [:owner_id, :owner_type, :deleted_at]
    remove_index :integrations, [:owner_id, :deleted_at]
  end

  def down
    remove_index :integrations, [:owner_id, :owner_type, :deleted_at]
    add_index :integrations, [:owner_id, :deleted_at]
    remove_column :integrations, :owner_type
  end
end
