# frozen_string_literal: true

require "github/transitions/20200630141645_add_discussion_category_fine_grained_permissions.rb"

class AddDiscussionCategoryFineGrainedPermissionsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddDiscussionCategoryFineGrainedPermissions.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
