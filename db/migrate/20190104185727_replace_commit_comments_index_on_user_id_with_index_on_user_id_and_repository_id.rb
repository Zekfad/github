# frozen_string_literal: true

class ReplaceCommitCommentsIndexOnUserIdWithIndexOnUserIdAndRepositoryId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_index :commit_comments, [:user_id, :repository_id]
    remove_index :commit_comments, [:user_id]
  end

  def down
    add_index :commit_comments, [:user_id]
    remove_index :commit_comments, [:user_id, :repository_id]
  end
end
