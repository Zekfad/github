# frozen_string_literal: true

class AddIndexForPayoutLedgerEntriesByTransactionTimestamp < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_index :billing_payouts_ledger_entries,
      [:stripe_connect_account_id, :transaction_type, :transaction_timestamp],
      name: "index_ledger_entries_on_acct_id_and_txn_type_and_timestamp"

    remove_index :billing_payouts_ledger_entries,
      column: [:stripe_connect_account_id, :transaction_type],
      name: "index_billing_payouts_ledger_entries_on_stripe_acct_and_txn_type"
  end

  def down
    add_index :billing_payouts_ledger_entries,
      [:stripe_connect_account_id, :transaction_type],
      name: "index_billing_payouts_ledger_entries_on_stripe_acct_and_txn_type"

    remove_index :billing_payouts_ledger_entries,
      column: [:stripe_connect_account_id, :transaction_type, :transaction_timestamp],
      name: "index_ledger_entries_on_acct_id_and_txn_type_and_timestamp"
  end
end
