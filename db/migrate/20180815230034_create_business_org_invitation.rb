# frozen_string_literal: true

class CreateBusinessOrgInvitation < GitHub::Migration
  def change
    create_table :business_organization_invitations do |t|
      t.integer :business_id, null: false
      t.integer :inviter_id, null: false
      t.integer :invitee_id, null: false
      t.datetime :accepted_at, null: true
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: true
      t.datetime :canceled_at, null: true
    end

    add_index :business_organization_invitations, :invitee_id
    add_index :business_organization_invitations, :business_id
  end
end
