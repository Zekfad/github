# frozen_string_literal: true

class DenormalizeBusinessUserAccountLogin < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :business_user_accounts, :login, :string, limit: 40, null: false, default: "", if_not_exists: true
    add_index :business_user_accounts, [:business_id, :user_id, :login], name: "index_business_user_accounts_on_business_id_user_id_login", if_not_exists: true
    remove_index :business_user_accounts, column: [:business_id, :user_id], if_exists: true
  end

  def self.down
    add_index :business_user_accounts, [:business_id, :user_id], if_not_exists: true
    remove_index :business_user_accounts, name: "index_business_user_accounts_on_business_id_user_id_login", if_exists: true
    remove_column :business_user_accounts, :login, if_exists: true
  end
end
