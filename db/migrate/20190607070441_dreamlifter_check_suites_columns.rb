# frozen_string_literal: true

class DreamlifterCheckSuitesColumns < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites, :completed_log_url, "varbinary(1024)", null: true
  end
end
