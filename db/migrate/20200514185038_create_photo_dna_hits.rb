# frozen_string_literal: true

class CreatePhotoDnaHits < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :photo_dna_hits do |t|
      t.integer :uploader_id, null: false
      t.integer :content_id, null: false
      # "RepositoryImage" should be longest content type string
      t.string :content_type, null: false, limit: 15

      t.timestamps null: false
    end

    add_index :photo_dna_hits, :uploader_id
    add_index :photo_dna_hits, [:content_id, :content_type]
  end
end
