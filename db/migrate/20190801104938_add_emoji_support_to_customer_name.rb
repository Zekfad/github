# frozen_string_literal: true

class AddEmojiSupportToCustomerName < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  def up
    change_column :customers, :name, "varbinary(1024)"
    remove_index :customers, name: "index_customers_on_billing_email_id", if_exists: true
    remove_index :customers, name: "index_customers_on_billing_type", if_exists: true
  end

  def down
    change_column :customers, :name, "varchar(255)"
    add_index :customers, [:billing_email_id], unqiue: false, name: "index_customers_on_billing_email_id", if_not_exists: true
    add_index :customers, [:billing_type], unqiue: false, name: "index_customers_on_billing_type", if_not_exists: true
  end
end
