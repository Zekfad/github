# frozen_string_literal: true

class CreateSponsorsGoals < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_goals do |t|
      t.references :sponsors_listing, null: false
      t.integer    :state, null: false, default: 0
      t.integer    :kind, null: false, default: 0
      t.integer    :target_value, null: false, default: 0
      t.blob       :description, null: true
      t.datetime   :completed_at, null: true
      t.datetime   :retired_at, null: true

      t.timestamps null: false
    end

    add_index :sponsors_goals, [:sponsors_listing_id, :state]
  end
end
