# frozen_string_literal: true

class AddDiscussions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussions do |t|
      t.column :title, "VARBINARY(1024)", null: false
      t.column :body, :mediumblob
      t.belongs_to :repository, null: false
      t.belongs_to :user, null: false
      t.belongs_to :chosen_comment
      t.boolean :user_hidden, default: false, null: false
      t.integer :milestone_id
      t.integer :state, null: false, default: 0
      t.integer :comment_count, null: false, default: 0
      t.datetime :closed_at
      t.timestamps null: false
    end

    add_index :discussions, [:created_at, :repository_id]
    add_index :discussions, [:repository_id, :user_id, :user_hidden]
    add_index :discussions, [:closed_at, :repository_id]
    add_index :discussions, [:chosen_comment_id, :user_id]
    add_index :discussions, [:user_id, :user_hidden]
    add_index :discussions, :milestone_id
  end
end
