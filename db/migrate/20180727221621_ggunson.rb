# frozen_string_literal: true

class Ggunson < GitHub::Migration
  def self.up
    add_index :archived_project_cards, :content_id
  end

  def self.down
    remove_index :archived_project_cards, [:content_id]
  end
end
