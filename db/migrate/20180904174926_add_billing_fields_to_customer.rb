# frozen_string_literal: true

class AddBillingFieldsToCustomer < GitHub::Migration
  def change
    # Making all of these nullable since for users they'll be set on the users
    # table until we move them over
    add_column :customers, :billing_end_date, :datetime, null: true, default: nil
    add_column :customers, :billing_start_date, :datetime, null: true, default: nil
    add_column :customers, :billing_attempts, :integer, null: true, default: nil
    add_column :customers, :billing_type, :string, limit: 20, null: true, default: nil
    add_column :customers, :billing_email_id, :integer, null: true, default: nil
    add_column :customers, :term_length, :integer, null: true, default: nil

    add_index :customers, :billing_email_id
    add_index :customers, :billing_type
  end
end
