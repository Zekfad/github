# frozen_string_literal: true

class AddHideFromDiscoveryToNetworkPrivileges < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :network_privileges, :hide_from_discovery, :boolean, default: false, null: false
  end
end
