# frozen_string_literal: true

class AddMatchedCurrentIpRegionAndUserAgentToFraudFlaggedSponsors < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_column :fraud_flagged_sponsors, :matched_ip_region, :string
    remove_column :fraud_flagged_sponsors, :matched_user_agent, :text
    add_column :fraud_flagged_sponsors, :matched_current_ip_region_and_user_agent, :text, null: true
  end
end
