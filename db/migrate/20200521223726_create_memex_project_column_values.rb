# frozen_string_literal: true

class CreateMemexProjectColumnValues < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Memex)

  def change
    create_table :memex_project_column_values do |t|
      t.references :memex_project_column, null: false
      t.references :memex_project_item, null: false
      t.mediumblob :value, null: true
      t.references :creator
      t.timestamps null: false
    end

    add_index(
      :memex_project_column_values,
      [:memex_project_column_id, :memex_project_item_id],
      unique: true,
      name: "index_memex_project_column_values_on_column_and_item"
    )
  end
end
