# frozen_string_literal: true

class AddSponsorableToSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :sponsorships, :sponsorable_type, :integer, if_not_exists: true
    add_column :sponsorships, :sponsorable_id, :integer, if_not_exists: true

    change_column_null(:sponsorships, :maintainer_type, true)
    change_column_null(:sponsorships, :maintainer_id, true)

    add_index :sponsorships,
      [:sponsor_id, :sponsor_type, :sponsorable_id, :sponsorable_type],
      name: "index_sponsorships_sponsor_sponsorable_unique",
      unique: true, if_not_exists: true

    remove_index :sponsorships, name: "index_sponsorships_sponsor_maintainer_unique", if_exists: true
  end

  def self.down
    add_index :sponsorships,
      [:donor_id, :donor_type, :maintainer_id, :maintainer_type],
      name: "index_sponsorships_unique",
      unique: true, if_not_exists: true

    remove_index :sponsorships, name: "index_sponsorships_sponsor_maintainer_unique", if_exists: true

    change_column_null(:sponsorships, :maintainer_type, false)
    change_column_null(:sponsorships, :maintainer_id, false)

    remove_column :sponsorships, :sponsorable_type, :integer, if_exists: true
    remove_column :sponsorships, :sponsorable_id, :integer, if_exists: true
  end
end
