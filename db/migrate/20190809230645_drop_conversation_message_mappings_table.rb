# frozen_string_literal: true

class DropConversationMessageMappingsTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    drop_table :conversation_message_mappings
  end

  def down
    create_table :conversation_message_mappings do |t|
      t.integer :conversation_id, null: false, index: true
      t.integer :conversation_message_id, null: false, index: true
      t.string :original_type, null: false
      t.integer :original_id
    end
  end
end
