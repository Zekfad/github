# frozen_string_literal: true

class RemoveDiscussionVoting < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    drop_table :discussion_votes
  end

  def down
    create_table :discussion_votes do |t|
      t.belongs_to :user, null: false
      t.belongs_to :discussion, null: false
      t.datetime :created_at, null: false
    end

    add_index :discussion_votes, [:discussion_id, :user_id]
  end
end
