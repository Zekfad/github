# frozen_string_literal: true

class MakePrereleaseProgramMembersMemberIdRequired < GitHub::Migration
  def change
    change_column_null :prerelease_program_members, :member_id, false
    change_column_null :prerelease_program_members, :user_id, true
  end
end
