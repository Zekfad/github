# frozen_string_literal: true

class ChangeAcvIdToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :acv_contributors, :id, :bigint, auto_increment: true
  end
  def down
    change_column :acv_contributors, :id, :int, auto_increment: true
  end
end
