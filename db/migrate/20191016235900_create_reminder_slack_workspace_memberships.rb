# frozen_string_literal: true

class CreateReminderSlackWorkspaceMemberships < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :reminder_slack_workspace_memberships do |t|
      t.integer :user_id, null: false
      t.integer :reminder_slack_workspace_id, null: false

      t.timestamps null: false

      t.index [:user_id, :reminder_slack_workspace_id], unique: true, name: "index_reminder_slack_user_on_user_id_and_slack_workspace_id"
    end
  end
end
