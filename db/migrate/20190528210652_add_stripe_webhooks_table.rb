# frozen_string_literal: true

class AddStripeWebhooksTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :stripe_webhooks do |t|
      t.integer :kind, null: false
      t.string :fingerprint, null: false, limit: 64
      t.text :payload, null: false
      t.datetime :processed_at
      t.timestamps null: false

      t.index :fingerprint, unique: true
      t.index :processed_at
    end
  end
end
