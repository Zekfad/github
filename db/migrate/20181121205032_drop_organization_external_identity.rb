# frozen_string_literal: true

class DropOrganizationExternalIdentity < GitHub::Migration
  def self.up
    drop_table :organization_external_identities, if_exists: true
  end

  def self.down
    create_table :organization_external_identities, if_not_exists: true do |t|
      t.belongs_to :user, null: true, default: nil
      t.integer    :provider_id,   null: false
      t.string     :provider_type, null: false, limit: 40
      t.timestamps null: false
      t.string     :guid, limit: 36, null: false
      t.belongs_to :organization_invitation, null: true, default: nil
    end

    add_index :organization_external_identities,
                            :guid, unique: true, if_not_exists: true

    add_index :organization_external_identities,
                            [:provider_id, :provider_type, :user_id],
                            name: :index_on_provider_and_user,
                            unique: true, if_not_exists: true

    add_index :organization_external_identities,
                            :organization_invitation_id,
                            name: :index_on_invitation, if_not_exists: true

    add_index :organization_external_identities,
                            :user_id, if_not_exists: true
  end
end
