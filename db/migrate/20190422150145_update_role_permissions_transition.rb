# frozen_string_literal: true

require "github/transitions/20190422150145_update_role_permissions"

class UpdateRolePermissionsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::UpdateRolePermissions.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
