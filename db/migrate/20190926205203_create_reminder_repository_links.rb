# frozen_string_literal: true
class CreateReminderRepositoryLinks < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :reminder_repository_links do |t|
      t.integer :reminder_id, null: false
      t.integer :repository_id, null: false
      t.timestamps null: false

      t.index [:reminder_id] # repository_id points to another database so can't be used in joins
    end
  end
end
