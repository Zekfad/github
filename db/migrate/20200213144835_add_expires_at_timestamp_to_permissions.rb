# frozen_string_literal: true

class AddExpiresAtTimestampToPermissions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_column :permissions, :expires_at, :bigint
    add_index  :permissions, :expires_at
  end
end
