# frozen_string_literal: true

class AddMatchEnabledToSponsorsListing < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)
  include GitHub::SafeDatabaseMigrationHelper

  def up
    add_column :sponsors_listings, :match_disabled, :boolean, null: false, default: false, if_not_exists: true
    add_index :sponsors_listings, [:match_disabled, :created_at], if_not_exists: true
    add_index :sponsors_listings, [:match_disabled, :published_at], if_not_exists: true
  end

  def down
    remove_index :sponsors_listings, [:match_disabled, :created_at], if_exists: true
    remove_index :sponsors_listings, [:match_disabled, :published_at], if_exists: true
    remove_column :sponsors_listings, :match_disabled, if_exists: true
  end
end
