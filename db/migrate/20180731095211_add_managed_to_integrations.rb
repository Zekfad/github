# frozen_string_literal: true

class AddManagedToIntegrations < GitHub::Migration
  def change
    add_column :integrations, :managed, :boolean, null: false, default: false
  end
end
