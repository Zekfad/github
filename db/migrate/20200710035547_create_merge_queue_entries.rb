# frozen_string_literal: true

class CreateMergeQueueEntries < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :merge_queue_entries do |t|
      t.references :repository, null: false
      t.references :pull_request, null: false
      t.integer    :state, null: false, default: 0

      t.references :enqueuer, null: false
      t.datetime   :enqueued_at, null: false

      t.references :dequeuer, null: true
      t.datetime   :dequeued_at, null: true
      t.integer    :dequeue_reason, null: true

      t.datetime   :deploy_started_at, null: true
      t.boolean    :solo, null: false, default: false

      t.timestamps null: false
    end

    add_index :merge_queue_entries, [:repository_id, :state, :enqueued_at],
      name: :index_merge_queue_entry_on_repository_and_state_and_enqueued_at
  end
end
