# frozen_string_literal: true

class AddExpiresAtTimestampToInstallations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :scoped_integration_installations, :expires_at, :bigint
    add_index  :scoped_integration_installations, :expires_at
  end
end
