# frozen_string_literal: true

class AddMoreIndexesToNotificationThreadSubscriptions < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    add_index :notification_thread_subscriptions,
      [:list_id, :user_id, :reason, :list_type, :ignored],
      name: "index_on_list_id_user_id_reason_list_type_ignored", if_not_exists: true
    add_index :notification_thread_subscriptions,
      [:user_id, :list_type, :ignored],
      name: "index_on_user_id_list_type_ignored", if_not_exists: true
    add_index :notification_thread_subscriptions,
      [:list_id, :user_id, :list_type, :ignored],
      name: "index_on_list_id_user_id_list_type_ignored", if_not_exists: true
    add_index :notification_thread_subscriptions,
      [:user_id, :reason, :list_type, :ignored],
      name: "index_on_user_id_reason_list_type_ignored", if_not_exists: true
  end

  def self.down
    remove_index :notification_thread_subscriptions, name: "index_on_list_id_user_id_reason_list_type_ignored", if_exists: true
    remove_index :notification_thread_subscriptions, name: "index_on_user_id_list_type_ignored", if_exists: true
    remove_index :notification_thread_subscriptions, name: "index_on_list_id_user_id_list_type_ignored", if_exists: true
    remove_index :notification_thread_subscriptions, name: "index_on_user_id_reason_list_type_ignored", if_exists: true
  end
end
