# frozen_string_literal: true

require "github/transitions/20200724213152_add_codespace_org_creator_role"

class AddCodespaceOrgCreatorRoleTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddCodespaceOrgCreatorRole.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
