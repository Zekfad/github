# frozen_string_literal: true

class AddBillingEmailToBusiness < GitHub::Migration
  def change
    add_column :businesses, :billing_email, :string
  end
end
