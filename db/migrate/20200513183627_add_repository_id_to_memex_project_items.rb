# frozen_string_literal: true

class AddRepositoryIdToMemexProjectItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Memex)

  def up
    add_column :memex_project_items, :repository_id, :integer
    add_index(
      :memex_project_items,
      [:memex_project_id, :repository_id, :content_type, :content_id],
      unique: true,
      name: "index_memex_project_items_on_memex_project_repository_id_content",
    )
    remove_index :memex_project_items, name: "index_memex_project_items_on_memex_project_id_and_content"
  end

  def down
    remove_column :memex_project_items, :repository_id
    remove_index :memex_project_items, name: "index_memex_project_items_on_memex_project_repository_id_content"
    add_index(
      :memex_project_items,
      [:memex_project_id, :content_type, :content_id],
      unique: true,
      name: "index_memex_project_items_on_memex_project_id_and_content",
    )
  end
end
