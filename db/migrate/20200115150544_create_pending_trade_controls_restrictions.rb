# frozen_string_literal: true

class CreatePendingTradeControlsRestrictions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :pending_trade_controls_restrictions do |t|
      t.integer :target_id, null: false, index: true
      t.datetime :enforcement_on, null: false, index: true
      t.column :reason, "enum('organization_admin', 'organization_billing_manager', 'organization_member', 'organization_outside_collaborator')", null: false, index: true
      t.column :enforcement_status, "enum('completed', 'cancelled', 'pending')", null: false, default: "pending"
      t.decimal :last_restriction_threshold, precision: 5, scale: 2, null: false
      t.timestamps null: false
    end

    add_index :pending_trade_controls_restrictions, [:enforcement_status, :enforcement_on], name: "index_enforcement_status_on_date"
  end
end
