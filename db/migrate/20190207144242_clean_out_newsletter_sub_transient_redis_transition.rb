# frozen_string_literal: true

class CleanOutNewsletterSubTransientRedisTransition < GitHub::Migration
  def self.up
    # Now cleaned up
  end

  def self.down
  end
end
