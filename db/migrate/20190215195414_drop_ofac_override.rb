# frozen_string_literal: true

class DropOFACOverride < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    remove_column :users, :ofac_sanction_override, :boolean, default: false, null: false
  end
end
