# frozen_string_literal: true

class AddSourceArtifactIdIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :shared_storage_artifact_events, [:source_artifact_id]
  end
end
