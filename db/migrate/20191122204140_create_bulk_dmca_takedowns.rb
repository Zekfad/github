# frozen_string_literal: true

class CreateBulkDmcaTakedowns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :bulk_dmca_takedowns do |t|
      t.integer :disabling_user_id, null: false
      t.text :notice_public_url, null: false
      t.integer :status, null: false, default: 1
      t.timestamps null: false
    end
  end
end
