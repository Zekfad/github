# frozen_string_literal: true

class AddIndexForNameAndUserIdToOauthApplications < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    add_index :oauth_applications, [:user_id, :name]
    remove_index :oauth_applications, :user_id
  end

  def down
    add_index :oauth_applications, :user_id
    remove_index :oauth_applications, [:user_id, :name]
  end
end
