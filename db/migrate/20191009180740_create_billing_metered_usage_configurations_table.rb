# frozen_string_literal: true

class CreateBillingMeteredUsageConfigurationsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :billing_metered_usage_configurations do |t|
      t.references :owner, polymorphic: { limit: 12 }, null: false, index: { unique: true, name: "index_billing_metered_usage_configurations_on_owner" }
      t.boolean :enforce_spending_limit, null: false, default: true
      t.integer :spending_limit_in_subunits, null: false, default: 0
      t.string :spending_limit_currency_code, null: false, limit: 3, default: "USD"

      t.timestamps null: false
    end
  end
end
