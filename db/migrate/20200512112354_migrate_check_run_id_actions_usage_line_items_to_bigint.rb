# frozen_string_literal: true

class MigrateCheckRunIdActionsUsageLineItemsToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column :actions_usage_line_items, :check_run_id, "bigint(11) unsigned DEFAULT NULL"
  end

end
