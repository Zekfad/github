# frozen_string_literal: true

require "github/transitions/20191203132159_backfill_early_access_memberships"

class BackfillEarlyAccessMembershipsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillEarlyAccessMemberships.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
