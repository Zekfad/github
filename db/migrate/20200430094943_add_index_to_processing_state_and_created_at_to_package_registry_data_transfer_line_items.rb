# frozen_string_literal: true

class AddIndexToProcessingStateAndCreatedAtToPackageRegistryDataTransferLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :package_registry_data_transfer_line_items, [:submission_state, :created_at], name: "index_on_submission_state_and_created_at"
  end
end
