# frozen_string_literal: true

class CreateUserLabelsTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :user_labels do |t|
      t.integer :user_id, null: false
      t.column :name, "varbinary(1024)", null: false, index: true
      t.column :lowercase_name, "varbinary(1024)", null: false
      t.column :description, "varbinary(400)", null: false
      t.string :color, limit: 10
      t.timestamps null: false
    end

    add_index :user_labels, [:user_id, :name]
    add_index :user_labels, [:user_id, :lowercase_name]
  end
end
