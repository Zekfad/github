# frozen_string_literal: true

class ChangeMigratableResourcesModelIdType < GitHub::Migration
  def up
    change_column :migratable_resources, :model_id, "bigint(20) DEFAULT NULL"
  end

  def down
    change_column :migratable_resources, :model_id, "int(11) DEFAULT NULL"
  end
end
