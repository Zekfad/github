# frozen_string_literal: true

require "github/transitions/20190114161528_populate_profile_pins.rb"

class RunPopulateProfilePins < GitHub::Migration
  def up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::PopulateProfilePins.new(dry_run: false)
    transition.perform
  end

  def down
  end
end
