# frozen_string_literal: true

class DropRepositoryStaffRequests < GitHub::Migration
   # Code using this table was removed in https://github.com/github/github/pull/109354

  def self.up
    drop_table :repository_staff_grant_requests, if_exists: true
  end

  def self.down
    create_table :repository_staff_grant_requests, if_not_exists: true do |t|
      t.belongs_to :requested_by, null: false
      t.belongs_to :repository,   null: false
      t.string     :reason,       null: false
      t.datetime   :expires_at,   null: false
      t.boolean    :cancelled,    null: false, default: false
      t.belongs_to :cancelled_by, null: true
      t.datetime   :cancelled_at, null: true
      t.timestamps                null: false
    end

    add_index :repository_staff_grant_requests, :repository_id, if_not_exists: true
    add_index :repository_staff_grant_requests, :requested_by_id, if_not_exists: true
  end
end
