# frozen_string_literal: true

require "github/transitions/20200123093224_backfill_repository_sequences"

class BackfillRepositorySequencesTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?

    GitHub::Transitions::BackfillRepositorySequences.new(
      dry_run: false,
      archived: false,
    ).perform

    GitHub::Transitions::BackfillRepositorySequences.new(
      dry_run: false,
      archived: true,
    ).perform
  end

  def self.down
  end
end
