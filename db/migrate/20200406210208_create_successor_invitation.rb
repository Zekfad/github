# frozen_string_literal: true

class CreateSuccessorInvitation < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :successor_invitations do |t|
      t.integer :target_id, null: false
      t.string :target_type, null: false, limit: 40
      t.integer :inviter_id, null: false
      t.integer :invitee_id, null: false

      t.datetime :created_at, null: false
      t.datetime :accepted_at, null: true
      t.datetime :declined_at, null: true
      t.datetime :canceled_at, null: true
    end

    add_index :successor_invitations, [:target_id, :target_type], name: "index_successor_invitations_on_target"
    add_index :successor_invitations, :inviter_id
    add_index :successor_invitations, :invitee_id
  end
end
