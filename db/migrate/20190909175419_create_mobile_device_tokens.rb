# frozen_string_literal: true

class CreateMobileDeviceTokens < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def change
    create_table :mobile_device_tokens do |t|
      t.references :user, null: false
      t.integer :service, null: false, default: 0, limit: 1
      t.string :device_token, null: false
      t.timestamps null: false
    end

    add_index :mobile_device_tokens, [:user_id, :service, :device_token], unique: true, name: "index_mobile_device_tokens_on_user_id_and_service_and_token"
  end
end
