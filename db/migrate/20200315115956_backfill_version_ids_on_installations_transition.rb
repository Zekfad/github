# frozen_string_literal: true

require "github/transitions/20200315115956_backfill_version_ids_on_installations"

class BackfillVersionIdsOnInstallationsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillVersionIdsOnInstallations.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
