# frozen_string_literal: true

class AddParentCommentIdToDiscussionComments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussion_comments, :parent_comment_id, :integer, after: :discussion_id
    add_index :discussion_comments, [:parent_comment_id, :discussion_id]
  end
end
