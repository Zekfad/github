# frozen_string_literal: true

class DropNotificationThreadSubscriptionReasons < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    drop_table :notification_thread_subscription_reasons
  end

  def self.down
    create_table :notification_thread_subscription_reasons do |t|
      t.integer :list_id, null: false
      t.string  :list_type, null: false, default: "Repository", limit: 64
      t.string  :thread_key, null: false, limit: 80
      t.integer :user_id, null: false
      t.string  :reason, limit: 50
      t.integer :team_id, null: true
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: true
    end

    add_index :notification_thread_subscription_reasons, [:user_id, :reason],
      name: "index_notification_reasons_on_user_id_and_reason", if_not_exists: true

    add_index :notification_thread_subscription_reasons, [:user_id, :list_id, :list_type, :thread_key, :reason, :team_id], unique: true,
      name: "index_notification_reasons_by_user", if_not_exists: true

    add_index :notification_thread_subscription_reasons, [:user_id, :list_type, :list_id, :created_at],
      name: "index_notification_reasons_on_user_and_list_and_created_at", if_not_exists: true

    add_index :notification_thread_subscription_reasons, [:list_type, :list_id, :thread_key, :created_at],
      name: "index_notification_reasons_on_list_and_thread_and_created_at", if_not_exists: true

    add_index :notification_thread_subscription_reasons, [:user_id, :list_type, :list_id, :updated_at],
      name: "index_notification_reasons_on_user_and_list_and_updated_at", if_not_exists: true
  end
end
