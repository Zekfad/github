# frozen_string_literal: true

class AddVerifiedToMannequinEmail < GitHub::Migration
  def change
    add_column :mannequin_emails, :verified, :boolean, null: false, default: false
  end
end
