# frozen_string_literal: true

class AddInstalledAtToSubItems < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    add_column :subscription_items, :installed_at, :datetime
    add_index :subscription_items, [:plan_subscription_id, :installed_at, :created_at], name: "idx_subscription_items_plan_sub_id_and_installed_at"
    remove_index :subscription_items, :marketplace_listing_plan_id
  end

  def down
    add_index :subscription_items, :marketplace_listing_plan_id
    remove_column :subscription_items, :installed_at
    remove_index :subscription_items, name: "idx_subscription_items_plan_sub_id_and_installed_at"
  end
end
