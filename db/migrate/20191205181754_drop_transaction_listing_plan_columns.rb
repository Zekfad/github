# frozen_string_literal: true

class DropTransactionListingPlanColumns < GitHub::Migration
  def up
    remove_column :transactions, :current_listing_plan_id
    remove_column :transactions, :current_listing_plan_quantity
    remove_column :transactions, :old_listing_plan_id
    remove_column :transactions, :old_listing_plan_quantity
  end

  def down
    add_column :transactions, :current_listing_plan_id, :integer
    add_column :transactions, :current_listing_plan_quantity, :integer
    add_column :transactions, :old_listing_plan_id, :integer
    add_column :transactions, :old_listing_plan_quantity, :integer
  end
end
