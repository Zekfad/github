# frozen_string_literal: true

class AddMemexProjectIdToDraftIssues < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Memex)

  def change
    add_column :draft_issues, :memex_project_item_id, :integer, null: false
    add_index :draft_issues, :memex_project_item_id
  end
end
