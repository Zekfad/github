# frozen_string_literal: true

require "github/transitions/20200225091357_delete_legacy_user_content_edits"

class DeleteLegacyUserContentEditsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DeleteLegacyUserContentEdits.new(dry_run: false, "batch-size": 10000)
    transition.perform

    execute <<~SQL
      ALTER TABLE `user_content_edits` ENGINE=InnoDB
    SQL
  end

  def self.down
  end
end
