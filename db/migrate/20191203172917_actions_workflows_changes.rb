# frozen_string_literal: true

class ActionsWorkflowsChanges < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    change_column :workflows, :slug, :string, limit: 60, null: true

    add_index :workflows, [:repository_id, :path], unique: true
  end
end
