# frozen_string_literal: true

require "github/transitions/20200130150612_backfill_version_numbers_on_installations"

class BackfillVersionNumbersOnInstallationsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillVersionNumbersOnInstallations.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
