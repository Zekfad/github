# frozen_string_literal: true

class CreateMergeGroupEntries < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :merge_group_entries do |t|
      t.references :merge_queue, null: false
      t.references :merge_group, null: false
      t.references :merge_queue_entry, null: false
      t.integer :position, null: false
      t.integer :retries, null: false, default: 0
      t.string :head_oid, limit: 40
      t.column :head_ref, "VARBINARY(1024)"
      t.integer :state, null: false, default: 0

      t.index [:merge_group_id, :merge_queue_entry_id], unique: true, name: "merge_group_queue_entry_id"
      t.index [:merge_queue_id, :merge_queue_entry_id], name: "queue_id_queue_entry_id"
    end
  end
end
