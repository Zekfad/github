# frozen_string_literal: true

class AddVsoPlans < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :vso_plans do |t|
      t.belongs_to :user, null: false
      t.column :subscription, "char(36)", null: false
      t.string :resource_group, null: false, limit: 90
      t.string :name, null: false, limit: 90

      t.timestamps null: false

      t.index [:user_id, :resource_group, :subscription], unique: true
      t.index [:subscription, :resource_group]
    end
  end
end
