# frozen_string_literal: true

class AddUniqueIdToTransactionOnPlanSubscription < GitHub::Migration
  # See https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/migrations-and-transitions/database-migrations-for-dotcom/ for tips

  def change
    remove_index :plan_subscriptions, :apple_transaction_id
    remove_index :plan_subscriptions, [:user_id, :apple_transaction_id]
    add_index :plan_subscriptions, :apple_transaction_id, unique: true
  end
end
