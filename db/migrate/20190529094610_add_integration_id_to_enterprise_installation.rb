# frozen_string_literal: true

class AddIntegrationIdToEnterpriseInstallation < GitHub::Migration
  def change
    add_column :enterprise_installations, :integration_id, :integer
    add_index :enterprise_installations, [:integration_id], name: "enterprise_installations_on_integration_id"
  end
end
