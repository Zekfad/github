# frozen_string_literal: true

class AddLongLivedToFlipperFeatures < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql1)

  def change
    add_column :flipper_features, :long_lived, :boolean, default: false, null: false
  end
end
