# frozen_string_literal: true

class CreateProjectRepositoryLink < GitHub::Migration
  def up
    create_table :project_repository_links do |t|
      t.integer :project_id, null: false
      t.integer :repository_id, null: false
      t.integer :creator_id
      t.timestamps null: false
    end

    add_index :project_repository_links, [:project_id, :repository_id], unique: true
    add_index :project_repository_links, [:repository_id]

    create_table :archived_project_repository_links do |t|
      t.references :project, null: false, index: true
      t.references :repository, null: false, index: true
      t.integer :creator_id
      t.timestamps null: false
    end
  end

  def down
    drop_table :project_repository_links
    drop_table :archived_project_repository_links
  end
end
