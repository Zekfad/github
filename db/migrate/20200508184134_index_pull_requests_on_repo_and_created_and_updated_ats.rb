# frozen_string_literal: true

class IndexPullRequestsOnRepoAndCreatedAndUpdatedAts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :pull_requests, [:repository_id, :updated_at]
    add_index :pull_requests, [:repository_id, :created_at]
  end
end
