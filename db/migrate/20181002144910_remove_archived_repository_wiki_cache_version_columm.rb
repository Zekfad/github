# frozen_string_literal: true

class RemoveArchivedRepositoryWikiCacheVersionColumm < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    remove_column :archived_repository_wikis,
                  :cache_version,
                  :integer,
                  null: false,
                  default: 1
  end
end
