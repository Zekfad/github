# frozen_string_literal: true

class AddStripeWebhooksStatusColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :stripe_webhooks, :status, "enum('pending', 'processed', 'ignored')", null: true
    add_index :stripe_webhooks, [:status, :created_at]
  end
end
