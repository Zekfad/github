# frozen_string_literal: true

class AddBillCycleDayToCustomer < GitHub::Migration
  def change
    add_column :customers, :bill_cycle_day, :integer, limit: 1, null: true, default: 0
  end
end
