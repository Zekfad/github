# frozen_string_literal: true

class AddIndexOnNetworkReplicasToRepositoryReplicas < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def self.up
    add_index :repository_replicas, :network_replica_id, if_not_exists: true
  end

  def self.down
    remove_index :repository_replicas, :network_replica_id, if_exists: true
  end
end
