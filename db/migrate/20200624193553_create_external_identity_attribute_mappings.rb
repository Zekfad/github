# frozen_string_literal: true

class CreateExternalIdentityAttributeMappings < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :external_identity_attribute_mappings do |t|
      t.references :provider, polymorphic: true
      t.integer :scheme, null: false
      t.integer :target, null: false
      t.text :source, null: false
      t.integer :state, default: 0, null: false
      t.timestamps null: false

      t.index [:provider_id, :provider_type], name: "index_on_id_attr_mapping_by_provider"
      t.index [:provider_id, :provider_type, :scheme, :target], unique: true,
        name: "unique_index_on_id_attr_mapping_by_provider"
    end
  end
end
