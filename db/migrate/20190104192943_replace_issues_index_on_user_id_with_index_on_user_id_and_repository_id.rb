# frozen_string_literal: true

class ReplaceIssuesIndexOnUserIdWithIndexOnUserIdAndRepositoryId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_index :issues, [:user_id, :repository_id]
    remove_index :issues, [:user_id]
  end

  def down
    add_index :issues, [:user_id]
    remove_index :issues, [:user_id, :repository_id]
  end
end
