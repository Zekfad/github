# frozen_string_literal: true

class RemoveOnUpdateFromLineItemTimestamps < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column_default :package_registry_data_transfer_line_items,
      :downloaded_at,
      from: -> { "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" },
      to: -> { "CURRENT_TIMESTAMP" }

    change_column_default :shared_storage_artifact_aggregations,
      :aggregate_effective_at,
      from: -> { "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" },
      to: -> { "CURRENT_TIMESTAMP" }

    change_column_default :shared_storage_artifact_events,
      :effective_at,
      from: -> { "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" },
      to: -> { "CURRENT_TIMESTAMP" }
  end
end
