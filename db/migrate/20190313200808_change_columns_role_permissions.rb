# frozen_string_literal: true

class ChangeColumnsRolePermissions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    rename_column :role_permissions, :fine_grained_permissions_id, :fine_grained_permission_id
    rename_column :role_permissions, :roles_id, :role_id
    add_column :role_permissions, :action, :string, null: false, limit: 60
  end
end
