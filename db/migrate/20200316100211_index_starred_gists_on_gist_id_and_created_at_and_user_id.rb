# frozen_string_literal: true

class IndexStarredGistsOnGistIdAndCreatedAtAndUserId < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_index :starred_gists, [:gist_id, :created_at, :user_id]
  end
end
