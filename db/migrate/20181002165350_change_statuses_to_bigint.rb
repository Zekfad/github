# frozen_string_literal: true

class ChangeStatusesToBigint < GitHub::Migration
  def up
    return unless GitHub.enterprise?

    change_column :statuses, :id, "bigint NOT NULL AUTO_INCREMENT"
  end

  def down
    return unless GitHub.enterprise?

    change_column :statuses, :id, "int(11) unsigned NOT NULL AUTO_INCREMENT"
  end
end
