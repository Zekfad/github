# frozen_string_literal: true

class AddIndexToRepositoryInvitation < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :repository_invitations, :created_at
  end
end
