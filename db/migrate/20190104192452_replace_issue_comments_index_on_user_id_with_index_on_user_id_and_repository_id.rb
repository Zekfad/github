# frozen_string_literal: true

class ReplaceIssueCommentsIndexOnUserIdWithIndexOnUserIdAndRepositoryId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_index :issue_comments, [:user_id, :repository_id]
    remove_index :issue_comments, [:user_id]
  end

  def down
    add_index :issue_comments, [:user_id]
    remove_index :issue_comments, [:user_id, :repository_id]
  end
end
