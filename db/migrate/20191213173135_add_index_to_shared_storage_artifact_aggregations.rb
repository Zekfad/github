# frozen_string_literal: true

class AddIndexToSharedStorageArtifactAggregations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :shared_storage_artifact_aggregations, [
      :synchronization_batch_id,
      :directly_billed,
      :repository_visibility,
      :aggregate_effective_at,
      :owner_id,
    ], name: "index_shared_storage_aggregations_on_billable_usage"
  end
end
