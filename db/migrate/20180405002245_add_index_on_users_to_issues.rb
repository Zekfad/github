# frozen_string_literal: true

class AddIndexOnUsersToIssues < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :issues, [:user_id], name: "index_issues_on_user_id"
  end
end
