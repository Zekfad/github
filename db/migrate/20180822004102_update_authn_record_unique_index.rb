# frozen_string_literal: true

class UpdateAuthnRecordUniqueIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    # this index had a unique constraint on it
    remove_index :authentication_records, [:user_session_id, :ip_address]
    # we still want the index, but we don't want the uniqueness constrant
    add_index :authentication_records, [:user_session_id, :ip_address]
  end
end
