# frozen_string_literal: true

class DropLegacyGistColumns < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  def change
    remove_index :gists, column: [:host], if_exists: true
    remove_column :gists, :host, "varchar(20)", if_exists: true
    remove_column :archived_gists, :host, "varchar(20)", if_exists: true
  end
end
