# frozen_string_literal: true

class AddArchivedAtToProjectCards < GitHub::Migration
  def change
    add_column :archived_project_cards, :archived_at, :datetime
    add_column :project_cards, :archived_at, :datetime
    add_index :project_cards, [:column_id, :archived_at, :priority]
  end
end
