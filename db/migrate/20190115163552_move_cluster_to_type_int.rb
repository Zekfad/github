# frozen_string_literal: true

class MoveClusterToTypeInt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def up
    change_column :abilities_permissions_routing, :cluster, :int
  end

  def down
    change_column :abilities_permissions_routing, :cluster, :int, limit: 1
  end
end
