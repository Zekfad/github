# frozen_string_literal: true

class AddConcreteRepositoryContentEditsTables < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    create_table :commit_comment_edits do |t|
      t.belongs_to :commit_comment, null: false
      t.datetime :edited_at, null: false
      t.belongs_to :editor, null: false
      t.timestamps null: false
      t.belongs_to :performed_by_integration
      t.datetime :deleted_at
      t.belongs_to :deleted_by
      t.mediumblob :diff
      t.integer :user_content_edit_id
      t.index :commit_comment_id
      t.index :user_content_edit_id, unique: true
    end

    create_table :issue_comment_edits do |t|
      t.belongs_to :issue_comment, null: false
      t.datetime :edited_at, null: false
      t.belongs_to :editor, null: false
      t.timestamps null: false
      t.belongs_to :performed_by_integration
      t.datetime :deleted_at
      t.belongs_to :deleted_by
      t.mediumblob :diff
      t.integer :user_content_edit_id
      t.index :issue_comment_id
      t.index :user_content_edit_id, unique: true
    end

    create_table :pull_request_review_edits do |t|
      t.belongs_to :pull_request_review, null: false
      t.datetime :edited_at, null: false
      t.belongs_to :editor, null: false
      t.timestamps null: false
      t.belongs_to :performed_by_integration
      t.datetime :deleted_at
      t.belongs_to :deleted_by
      t.mediumblob :diff
      t.integer :user_content_edit_id
      t.index :pull_request_review_id
      t.index :user_content_edit_id, unique: true
    end

    create_table :pull_request_review_comment_edits do |t|
      t.belongs_to :pull_request_review_comment, null: false
      t.datetime :edited_at, null: false
      t.belongs_to :editor, null: false
      t.timestamps null: false
      t.belongs_to :performed_by_integration
      t.datetime :deleted_at
      t.belongs_to :deleted_by
      t.mediumblob :diff
      t.integer :user_content_edit_id
      t.index :pull_request_review_comment_id, name: "index_on_pull_request_review_comment_id"
      t.index :user_content_edit_id, unique: true
    end

    create_table :repository_advisory_edits do |t|
      t.belongs_to :repository_advisory, null: false
      t.datetime :edited_at, null: false
      t.belongs_to :editor, null: false
      t.timestamps null: false
      t.belongs_to :performed_by_integration
      t.datetime :deleted_at
      t.belongs_to :deleted_by
      t.mediumblob :diff
      t.integer :user_content_edit_id
      t.index :repository_advisory_id
      t.index :user_content_edit_id, unique: true
    end

    create_table :repository_advisory_comment_edits do |t|
      t.belongs_to :repository_advisory_comment, null: false
      t.datetime :edited_at, null: false
      t.belongs_to :editor, null: false
      t.timestamps null: false
      t.belongs_to :performed_by_integration
      t.datetime :deleted_at
      t.belongs_to :deleted_by
      t.mediumblob :diff
      t.integer :user_content_edit_id
      t.index :repository_advisory_comment_id, name: "index_on_pull_request_review_comment_id"
      t.index :user_content_edit_id, unique: true
    end
  end
end
