# frozen_string_literal: true

class AddIssueIndexToPinnedIssues < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :pinned_issues, :issue_id
  end
end
