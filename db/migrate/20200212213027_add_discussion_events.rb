# frozen_string_literal: true

class AddDiscussionEvents < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussion_events do |t|
      t.belongs_to :repository, null: false
      t.belongs_to :discussion, null: false
      t.belongs_to :actor
      t.belongs_to :comment
      t.integer :event_type, null: false
      t.datetime :created_at, null: false
    end

    add_index :discussion_events, [:repository_id, :discussion_id, :event_type, :created_at],
      name: "idx_discussion_events_on_repo_discussion_type_created_at"
    add_index :discussion_events, :actor_id
    add_index :discussion_events, :comment_id
  end
end
