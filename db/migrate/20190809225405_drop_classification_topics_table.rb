# frozen_string_literal: true

class DropClassificationTopicsTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    drop_table :classification_topics
  end

  def down
    create_table :classification_topics do |t|
      t.string :name, unique: true, null: false
      t.boolean :active, index: true, default: false, null: false

      t.timestamps null: false
    end
  end
end
