# frozen_string_literal: true

class IntegrationInstallationsIdBigInt < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips

  def up
    change_column :integration_installations, :id, "bigint(20) NOT NULL AUTO_INCREMENT"
  end

  def down
    change_column :integration_installations, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
