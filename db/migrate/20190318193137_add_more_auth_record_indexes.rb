# frozen_string_literal: true

class AddMoreAuthRecordIndexes < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :authentication_records, :authenticated_device_id, "int(11) unsigned NULL"
    add_index :authentication_records, [:user_id, :country_code, :created_at], name: :authentication_records_on_user_id_country_code_and_created_at
    add_index :authentication_records, [:user_id, :octolytics_id, :created_at], name: :authentication_records_on_user_id_octolytics_id_and_created_at
  end
end
