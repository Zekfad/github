# frozen_string_literal: true

class AddBillingCountryToSponsorsMemberships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :sponsors_memberships, :survey_id, :integer, null: true
    add_column :sponsors_memberships, :billing_country, :string, limit: 60, null: true
    add_column :sponsors_memberships, :contact_email_id, :integer, null: true
    add_column :sponsors_memberships, :reviewed_at, :datetime, null: true
    add_index :sponsors_memberships, :contact_email_id
    add_index :sponsors_memberships, :reviewed_at
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
