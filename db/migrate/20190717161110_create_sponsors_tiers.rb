# frozen_string_literal: true

class CreateSponsorsTiers < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_tiers do |t|
      t.references :sponsors_listing, null: false

      t.binary :name, limit: 1024, null: false
      t.mediumblob :description, null: false
      t.integer :state, limit: 2, null: false, default: 0
      t.integer :monthly_price_in_cents, null: false, default: 0
      t.integer :yearly_price_in_cents, null: false, default: 0

      t.timestamps null: false
    end

    add_index :sponsors_tiers, [:name, :sponsors_listing_id, :state]
    add_index :sponsors_tiers, [:sponsors_listing_id, :monthly_price_in_cents],
      name: "index_sponsors_tiers_on_listing_and_monthly_price_in_cents"
  end
end
