# frozen_string_literal: true

class CheckSuiteHidden < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites, :hidden, :boolean, default: false, null: false
    add_index :check_suites, [:repository_id, :hidden]
  end
end
