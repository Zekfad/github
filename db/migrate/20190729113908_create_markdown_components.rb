# frozen_string_literal: true

class CreateMarkdownComponents < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :markdown_components do |t|
      t.integer :container_id, null: false
      t.string :container_type, null: false, limit: 100
      t.blob :body, null: false
      t.integer :container_order, null: false
      t.timestamps null: false
    end

    add_index :markdown_components, [:container_id, :container_type]
  end
end
