# frozen_string_literal: true

class GgunsonRepositoryReplicasUpdateId < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def self.up
    # Change PK from signed int to unsigned bigint
    change_column :repository_replicas, :id, "bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT"
  end

  def self.down
    change_column :repository_replicas, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
