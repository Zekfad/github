# frozen_string_literal: true

class AddSponsorsToSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :sponsorships, :sponsor_type, :integer, if_not_exists: true
    add_column :sponsorships, :sponsor_id, :integer, if_not_exists: true
    add_column :sponsorships, :is_sponsor_opted_in_to_email, :boolean, null: false, default: false, if_not_exists: true

    change_column_null(:sponsorships, :donor_type, true)
    change_column_null(:sponsorships, :donor_id, true)

    add_index :sponsorships,
      [:sponsor_id, :sponsor_type, :maintainer_id, :maintainer_type],
      name: "index_sponsorships_sponsor_maintainer_unique",
      unique: true, if_not_exists: true
    add_index :sponsorships, :is_sponsor_opted_in_to_email, if_not_exists: true

    remove_index :sponsorships, name: "index_sponsorships_unique", if_exists: true
    remove_index :sponsorships, column: :is_donor_opted_in_to_email, if_exists: true
  end

  def self.down
    add_index :sponsorships, :is_donor_opted_in_to_email, if_not_exists: true
    add_index :sponsorships,
      [:donor_id, :donor_type, :maintainer_id, :maintainer_type],
      name: "index_sponsorships_unique",
      unique: true, if_not_exists: true

    remove_index :sponsorships, :is_sponsor_opted_in_to_email, if_exists: true
    remove_index :sponsorships, name: "index_sponsorships_sponsor_maintainer_unique", if_exists: true

    change_column_null(:sponsorships, :donor_type, false)
    change_column_null(:sponsorships, :donor_id, false)

    remove_column :sponsorships, :sponsor_type, if_exists: true
    remove_column :sponsorships, :sponsor_id, if_exists: true
    remove_column :sponsorships, :is_sponsor_opted_in_to_email, if_exists: true
  end
end
