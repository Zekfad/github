# frozen_string_literal: true

require "github/transitions/20180227092833_rehash_ldap_mapping_dn_with_canonical_representation.rb"

class RehashLdapMappingDnWithCanonicalRepresentationRedux < GitHub::Migration

  def up
    return if !GitHub.enterprise?
    GitHub::Transitions::RehashLdapMappingDnWithCanonicalRepresentation.new(dry_run: false).perform
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end

end
