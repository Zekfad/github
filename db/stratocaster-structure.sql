DROP TABLE IF EXISTS `stratocaster_events`;
CREATE TABLE `stratocaster_events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `raw_data` mediumblob,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_stratocaster_events_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
