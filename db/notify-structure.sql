DROP TABLE IF EXISTS `archived_repository_vulnerability_alerts`;
CREATE TABLE `archived_repository_vulnerability_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `vulnerable_version_range_id` int(11) NOT NULL,
  `show_alert` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerable_manifest_path` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dismisser_id` int(11) DEFAULT NULL,
  `dismiss_reason` varchar(255) DEFAULT NULL,
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pending_vulnerabilities`;
CREATE TABLE `pending_vulnerabilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_identifier` varchar(255) DEFAULT NULL,
  `description` blob,
  `status` varchar(12) NOT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `severity` varchar(12) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `external_reference` varchar(255) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `reviewer_a_id` int(11) DEFAULT NULL,
  `review_a_at` timestamp NULL DEFAULT NULL,
  `reviewer_b_id` int(11) DEFAULT NULL,
  `review_b_at` timestamp NULL DEFAULT NULL,
  `submitted_by_id` int(11) DEFAULT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `review_notes` mediumblob,
  `vulnerability_id` int(11) DEFAULT NULL,
  `reject_reason` varchar(32) DEFAULT NULL,
  `source` varchar(64) DEFAULT NULL,
  `source_identifier` varchar(128) DEFAULT NULL,
  `ghsa_id` varchar(19) NOT NULL,
  `simulation` tinyint(1) NOT NULL DEFAULT '0',
  `cve_id` varchar(20) DEFAULT NULL,
  `white_source_id` varchar(20) DEFAULT NULL,
  `summary` varbinary(1024) DEFAULT NULL,
  `npm_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pending_vulnerabilities_on_ghsa_id` (`ghsa_id`),
  UNIQUE KEY `index_pending_vulnerabilities_on_source_and_source_identifier` (`source`,`source_identifier`),
  UNIQUE KEY `index_pending_vulnerabilities_on_npm_id` (`npm_id`),
  KEY `index_pending_vulnerabilities_on_external_identifier` (`external_identifier`),
  KEY `index_pending_vulnerabilities_on_vulnerability_id` (`vulnerability_id`),
  KEY `index_pending_vulnerabilities_on_created_at` (`created_at`),
  KEY `index_pending_vulnerabilities_on_simulation` (`simulation`),
  KEY `index_pending_vulnerabilities_on_cve_id` (`cve_id`),
  KEY `index_pending_vulnerabilities_on_white_source_id` (`white_source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pending_vulnerability_references`;
CREATE TABLE `pending_vulnerability_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pending_vulnerability_id` int(11) NOT NULL,
  `url` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_pending_vulnerability_references_pending_vulnerability_id` (`pending_vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pending_vulnerable_version_ranges`;
CREATE TABLE `pending_vulnerable_version_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pending_vulnerability_id` int(11) DEFAULT NULL,
  `affects` varchar(100) NOT NULL,
  `requirements` varchar(100) NOT NULL,
  `fixed_in` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ecosystem` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pending_vulnerable_version_ranges_on_pending_vuln_id` (`pending_vulnerability_id`),
  KEY `index_pending_vulnerable_version_ranges_on_ecosystem` (`ecosystem`),
  KEY `index_pending_vvrs_on_pending_vulnerability_id` (`pending_vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_dependency_updates`;
CREATE TABLE `repository_dependency_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `repository_vulnerability_alert_id` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `reason` int(11) NOT NULL DEFAULT '0',
  `trigger_type` int(11) NOT NULL DEFAULT '0',
  `manifest_path` varchar(255) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `body` mediumblob,
  `error_body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `error_title` varbinary(1024) DEFAULT NULL,
  `dry_run` tinyint(1) NOT NULL DEFAULT '0',
  `error_type` varchar(60) DEFAULT NULL,
  `retry` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_pull_request_dependency_updates_on_repo_dep_and_state` (`repository_id`,`manifest_path`,`package_name`,`state`),
  KEY `index_repository_dependency_updates_on_pull_request_id` (`pull_request_id`),
  KEY `index_repository_dependency_updates_on_rva_id` (`repository_vulnerability_alert_id`),
  KEY `index_repository_dependency_updates_on_state_and_created_at` (`state`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_vulnerability_alerts`;
CREATE TABLE `repository_vulnerability_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `vulnerable_version_range_id` int(11) NOT NULL,
  `show_alert` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerable_manifest_path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `vulnerable_requirements` varchar(64) DEFAULT NULL,
  `last_detected_at` datetime DEFAULT NULL,
  `dismisser_id` int(11) DEFAULT NULL,
  `dismiss_reason` varchar(255) DEFAULT NULL,
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_vulnerability_alerts_unique` (`vulnerability_id`,`vulnerable_version_range_id`,`vulnerable_manifest_path`,`repository_id`),
  KEY `index_on_vulnerable_version_range_id` (`vulnerable_version_range_id`),
  KEY `index_on_repository_id_and_show_alert` (`repository_id`,`show_alert`),
  KEY `index_on_repository_id_and_last_detected_at` (`repository_id`,`last_detected_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `team_group_mappings`;
CREATE TABLE `team_group_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `group_id` varchar(40) NOT NULL,
  `group_name` varbinary(400) NOT NULL,
  `group_description` varbinary(2048) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `synced_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_group_mappings_on_team_id_and_group_id` (`team_id`,`group_id`),
  KEY `index_team_group_mappings_on_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `team_sync_business_tenants`;
CREATE TABLE `team_sync_business_tenants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `provider_type` int(11) NOT NULL,
  `provider_id` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `setup_url_template` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `url` text,
  `encrypted_ssws_token` varbinary(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_sync_business_tenants_on_business_id` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `team_sync_tenants`;
CREATE TABLE `team_sync_tenants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `provider_type` int(11) NOT NULL,
  `provider_id` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `url` text,
  `encrypted_ssws_token` varbinary(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_sync_tenants_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `token_scan_result_locations`;
CREATE TABLE `token_scan_result_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_scan_result_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `commit_oid` varchar(40) NOT NULL,
  `blob_oid` varchar(40) NOT NULL,
  `path` varbinary(1024) NOT NULL,
  `start_line` int(11) NOT NULL,
  `end_line` int(11) NOT NULL,
  `start_column` int(11) NOT NULL,
  `end_column` int(11) NOT NULL,
  `ignore_token` int(11) NOT NULL DEFAULT '0',
  `blob_paths_processed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_token_scan_result_locations_on_result_and_location` (`token_scan_result_id`,`commit_oid`,`path`,`start_line`,`end_line`,`start_column`,`end_column`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `token_scan_results`;
CREATE TABLE `token_scan_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `token_type` varchar(64) NOT NULL,
  `token_signature` varchar(64) NOT NULL,
  `resolution` int(11) DEFAULT NULL,
  `resolver_id` int(11) DEFAULT NULL,
  `resolved_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_token_scan_results_on_repository_and_type_and_signature` (`repository_id`,`token_type`,`token_signature`),
  KEY `index_token_scan_results_on_repo_and_resolution_and_created_at` (`repository_id`,`resolution`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `token_scan_statuses`;
CREATE TABLE `token_scan_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) NOT NULL,
  `scheduled_at` datetime DEFAULT NULL,
  `scanned_at` datetime DEFAULT NULL,
  `scan_state` int(11) NOT NULL DEFAULT '0',
  `retry_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_token_scan_statuses_on_repository_id` (`repository_id`),
  KEY `index_token_scan_statuses_on_scan_state` (`scan_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vulnerabilities`;
CREATE TABLE `vulnerabilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(12) NOT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `severity` varchar(12) NOT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `external_reference` varchar(255) DEFAULT NULL,
  `description` blob,
  `created_by_id` int(11) NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `published_by_id` int(11) DEFAULT NULL,
  `withdrawn_at` datetime DEFAULT NULL,
  `withdrawn_by_id` int(11) DEFAULT NULL,
  `simulation` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ghsa_id` varchar(19) NOT NULL,
  `source` varchar(64) DEFAULT NULL,
  `source_identifier` varchar(128) DEFAULT NULL,
  `cve_id` varchar(20) DEFAULT NULL,
  `white_source_id` varchar(20) DEFAULT NULL,
  `summary` varbinary(1024) DEFAULT NULL,
  `npm_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_vulnerabilities_on_ghsa_id` (`ghsa_id`),
  UNIQUE KEY `index_vulnerabilities_on_source_and_source_identifier` (`source`,`source_identifier`),
  UNIQUE KEY `index_vulnerabilities_on_npm_id` (`npm_id`),
  KEY `index_vulnerabilities_on_identifier` (`identifier`),
  KEY `index_vulnerabilities_on_platform` (`platform`),
  KEY `index_vulnerabilities_on_severity` (`severity`),
  KEY `index_vulnerabilities_on_published_at` (`published_at`),
  KEY `index_vulnerabilities_on_updated_at` (`updated_at`),
  KEY `index_on_status_and_platform_and_simulation_and_updated_at` (`status`,`platform`,`simulation`,`updated_at`),
  KEY `index_vulnerabilities_on_simulation` (`simulation`),
  KEY `index_vulnerabilities_on_cve_id` (`cve_id`),
  KEY `index_vulnerabilities_on_white_source_id` (`white_source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vulnerability_alerting_event_subscriptions`;
CREATE TABLE `vulnerability_alerting_event_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_alerting_event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `repository_vulnerability_alert_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_and_repo_vuln_alert` (`user_id`,`repository_vulnerability_alert_id`),
  KEY `index_vae_user_and_repo_vuln_alert` (`vulnerability_alerting_event_id`,`user_id`,`repository_vulnerability_alert_id`),
  KEY `index_vulnerability_alerting_event_subscriptions_on_rva_id` (`repository_vulnerability_alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vulnerability_alerting_events`;
CREATE TABLE `vulnerability_alerting_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) DEFAULT NULL,
  `reason` int(11) NOT NULL DEFAULT '0',
  `actor_id` int(11) DEFAULT NULL,
  `processed_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `alert_count` int(11) NOT NULL DEFAULT '0',
  `notification_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vulnerability_references`;
CREATE TABLE `vulnerability_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) NOT NULL,
  `url` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_vulnerability_references_on_vulnerability_id` (`vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vulnerable_version_range_alerting_processes`;
CREATE TABLE `vulnerable_version_range_alerting_processes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_alerting_event_id` int(11) NOT NULL,
  `vulnerable_version_range_id` int(11) NOT NULL,
  `processed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_vae_id_and_vvr_id` (`vulnerability_alerting_event_id`,`vulnerable_version_range_id`),
  KEY `index_vae_id_and_processed_at` (`vulnerability_alerting_event_id`,`processed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vulnerable_version_ranges`;
CREATE TABLE `vulnerable_version_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) DEFAULT NULL,
  `affects` varchar(100) NOT NULL,
  `fixed_in` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `requirements` varchar(75) NOT NULL,
  `ecosystem` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_vulnerable_version_ranges_on_affects` (`affects`),
  KEY `index_vulnerable_version_ranges_on_ecosystem` (`ecosystem`),
  KEY `index_vulnerable_version_ranges_on_vulnerability_id` (`vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
