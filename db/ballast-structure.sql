DROP TABLE IF EXISTS `artifacts`;
CREATE TABLE `artifacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_run_id` bigint(11) unsigned DEFAULT NULL,
  `source_url` text NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `size` bigint(20) NOT NULL DEFAULT '0',
  `check_suite_id` bigint(11) unsigned DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_artifacts_on_check_run_id` (`check_run_id`),
  KEY `index_artifacts_on_check_suite_id` (`check_suite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `authenticated_devices`;
CREATE TABLE `authenticated_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `device_id` varchar(32) NOT NULL,
  `display_name` varbinary(1024) NOT NULL,
  `accessed_at` datetime NOT NULL,
  `approved_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_authenticated_devices_on_device_id_and_user_id` (`device_id`,`user_id`),
  KEY `index_authenticated_devices_on_user_id_and_approved_at` (`user_id`,`approved_at`),
  KEY `index_authenticated_devices_on_approved_at_and_accessed_at` (`approved_at`,`accessed_at`),
  KEY `index_authenticated_devices_on_accessed_at` (`accessed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `authentication_records`;
CREATE TABLE `authentication_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) DEFAULT NULL,
  `octolytics_id` varchar(32) NOT NULL,
  `client` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_agent` text,
  `flagged_reason` varchar(32) DEFAULT NULL,
  `user_session_id` int(11) DEFAULT NULL,
  `ip_address` varchar(40) NOT NULL,
  `authenticated_device_id` int(11) unsigned DEFAULT NULL,
  `region_name` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_authentication_records_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_authentication_records_on_user_session_id_and_ip_address` (`user_session_id`,`ip_address`),
  KEY `authentication_records_on_user_id_country_code_and_created_at` (`user_id`,`country_code`,`created_at`),
  KEY `authentication_records_on_user_id_octolytics_id_and_created_at` (`user_id`,`octolytics_id`,`created_at`),
  KEY `index_authentication_records_on_created_at` (`created_at`),
  KEY `index_authentication_records_on_user_id_and_device_id` (`user_id`,`authenticated_device_id`),
  KEY `index_authentication_records_ondevice_id_ip_address_and_user_id` (`authenticated_device_id`,`ip_address`,`user_id`),
  KEY `index_authentication_records_on_ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `check_annotations`;
CREATE TABLE `check_annotations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varbinary(1024) NOT NULL,
  `warning_level` varchar(255) DEFAULT NULL,
  `message` blob NOT NULL,
  `start_line` int(11) NOT NULL,
  `end_line` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `raw_details` text,
  `title` varbinary(1024) DEFAULT NULL,
  `check_run_id` bigint(11) unsigned DEFAULT NULL,
  `start_column` int(11) DEFAULT NULL,
  `end_column` int(11) DEFAULT NULL,
  `suggested_change` blob,
  `check_suite_id` bigint(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_check_annotations_on_check_run_id` (`check_run_id`),
  KEY `index_check_annotations_on_filename` (`filename`),
  KEY `index_check_annotations_on_check_suite_id` (`check_suite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `check_runs`;
CREATE TABLE `check_runs` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `conclusion` int(11) DEFAULT NULL,
  `details_url` text,
  `name` varbinary(1024) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `title` varbinary(1024) DEFAULT NULL,
  `summary` blob,
  `images` text,
  `text` blob,
  `check_suite_id` bigint(11) unsigned NOT NULL,
  `actions` mediumblob,
  `number` int(11) DEFAULT NULL,
  `completed_log_url` varbinary(1024) DEFAULT NULL,
  `completed_log_lines` int(11) DEFAULT NULL,
  `streaming_log_url` text,
  `display_name` varbinary(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_check_runs_on_check_suite_id_and_created_at` (`check_suite_id`,`created_at`),
  KEY `index_check_runs_on_check_suite_id_and_name_and_completed_at` (`check_suite_id`,`name`,`completed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `check_steps`;
CREATE TABLE `check_steps` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `check_run_id` bigint(11) unsigned NOT NULL,
  `number` int(11) NOT NULL,
  `conclusion` int(11) DEFAULT NULL,
  `name` varbinary(1024) NOT NULL,
  `completed_log_url` text,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `external_id` varchar(255) DEFAULT NULL,
  `completed_log_lines` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_check_steps_on_check_run_id_and_number` (`check_run_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `check_suites`;
CREATE TABLE `check_suites` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `push_id` bigint(20) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `github_app_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `conclusion` int(11) DEFAULT NULL,
  `head_branch` varbinary(1024) DEFAULT NULL,
  `head_sha` varchar(64) NOT NULL,
  `rerequestable` tinyint(1) NOT NULL DEFAULT '1',
  `check_runs_rerunnable` tinyint(1) NOT NULL DEFAULT '1',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `completed_log_url` varbinary(1024) DEFAULT NULL,
  `name` varbinary(1024) DEFAULT NULL,
  `event` varchar(50) DEFAULT NULL,
  `explicit_completion` tinyint(1) NOT NULL DEFAULT '0',
  `head_repository_id` int(11) DEFAULT NULL,
  `workflow_file_path` varbinary(1024) DEFAULT NULL,
  `external_id` varchar(64) DEFAULT NULL COMMENT 'Supplied by creator to allow external systems to work with check suites idempotently',
  `action` varbinary(400) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_external_id_per_app` (`repository_id`,`github_app_id`,`external_id`),
  KEY `index_check_suites_on_push_id` (`push_id`),
  KEY `index_check_suites_on_head_sha_and_repository_id` (`head_sha`,`repository_id`),
  KEY `index_check_suites_on_repository_id_and_hidden` (`repository_id`,`hidden`),
  KEY `index_check_suites_on_name` (`name`),
  KEY `index_check_suites_on_github_app_id_repository_id` (`github_app_id`,`repository_id`),
  KEY `by_repo_app_and_creator` (`repository_id`,`github_app_id`,`creator_id`),
  KEY `by_repo_app_and_head_branch` (`repository_id`,`github_app_id`,`head_branch`),
  KEY `by_repo_app_and_event` (`repository_id`,`github_app_id`,`event`),
  KEY `by_repo_app_and_status` (`repository_id`,`github_app_id`,`status`),
  KEY `by_repo_app_and_conclusion` (`repository_id`,`github_app_id`,`conclusion`),
  KEY `by_repo_app_and_name` (`repository_id`,`github_app_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `code_scanning_alerts`;
CREATE TABLE `code_scanning_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_annotation_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `alert_number` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `check_run_id` bigint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_code_scanning_alerts_on_check_annotation_id` (`check_annotation_id`),
  KEY `index_code_scanning_alerts_on_repo_and_number_and_check_run` (`repository_id`,`alert_number`,`check_run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `collection_items`;
CREATE TABLE `collection_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `content_type` varchar(30) NOT NULL,
  `slug` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_collection_items_on_collection_id` (`collection_id`),
  KEY `index_collection_items_on_content_id_and_content_type` (`content_id`,`content_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `collection_urls`;
CREATE TABLE `collection_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `title` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `collection_videos`;
CREATE TABLE `collection_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `title` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `thumbnail_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `collections`;
CREATE TABLE `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `attribution_url` text,
  `display_name` varchar(100) NOT NULL,
  `image_url` text,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_collections_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `content_reference_attachments`;
CREATE TABLE `content_reference_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_reference_id` int(11) DEFAULT NULL,
  `integration_id` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `title` varbinary(1024) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_content_reference_id_and_integration_id` (`content_reference_id`,`integration_id`),
  KEY `index_on_content_reference_id_and_state` (`content_reference_id`,`state`),
  KEY `index_content_reference_attachments_on_integration_id` (`integration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `content_references`;
CREATE TABLE `content_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content_id` int(11) NOT NULL,
  `content_type` varchar(30) NOT NULL,
  `reference_hash` varchar(32) NOT NULL,
  `reference` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_content_id_and_content_type_and_reference_hash` (`content_id`,`content_type`,`reference_hash`),
  KEY `index_content_references_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_post_replies`;
CREATE TABLE `discussion_post_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `discussion_post_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_post_replies_on_discussion_post_id_and_number` (`discussion_post_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_posts`;
CREATE TABLE `discussion_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `pinned_at` datetime DEFAULT NULL,
  `pinned_by_user_id` int(11) DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `title` varbinary(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_posts_on_team_id_and_number` (`team_id`,`number`),
  KEY `index_discussion_posts_on_team_id_and_pinned_at` (`team_id`,`pinned_at`),
  KEY `index_discussion_posts_on_team_id_and_private` (`team_id`,`private`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `docusign_webhooks`;
CREATE TABLE `docusign_webhooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fingerprint` varchar(64) NOT NULL,
  `payload` text NOT NULL,
  `processed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_docusign_webhooks_on_fingerprint` (`fingerprint`),
  KEY `index_docusign_webhooks_on_processed_at` (`processed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `organization_domains`;
CREATE TABLE `organization_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `domain` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_organization_domains_on_domain_and_organization_id` (`domain`,`organization_id`),
  KEY `index_organization_domains_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pages_embargoed_cnames`;
CREATE TABLE `pages_embargoed_cnames` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `previous_owner_id` int(11) NOT NULL,
  `previous_repository_id` int(11) NOT NULL,
  `cname` varbinary(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pages_embargoed_cnames_cname` (`cname`),
  KEY `index_pages_embargoed_cnames_previous_owner_id` (`previous_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pushes`;
CREATE TABLE `pushes` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `pusher_id` int(11) DEFAULT NULL,
  `before` varchar(40) DEFAULT NULL,
  `after` varchar(40) DEFAULT NULL,
  `ref` varbinary(1024) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pushes_on_pusher_id` (`pusher_id`),
  KEY `index_pushes_on_repository_id_and_pusher_id_and_created_at` (`repository_id`,`pusher_id`,`created_at`),
  KEY `index_pushes_on_repository_id_and_after` (`repository_id`,`after`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reserved_logins`;
CREATE TABLE `reserved_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reserved_logins_on_login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `retired_namespaces`;
CREATE TABLE `retired_namespaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_login` varchar(40) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_retired_namespaces_on_owner_login_and_name` (`owner_login`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `slotted_counters`;
CREATE TABLE `slotted_counters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `record_type` varchar(30) NOT NULL,
  `record_id` int(11) NOT NULL,
  `slot` int(11) NOT NULL DEFAULT '0',
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `records_and_slots` (`record_type`,`record_id`,`slot`),
  KEY `index_slotted_counters_on_record_type_and_record_id_and_count` (`record_type`,`record_id`,`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `stafftools_roles`;
CREATE TABLE `stafftools_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `state` varchar(20) NOT NULL DEFAULT 'unknown',
  `description` varchar(255) DEFAULT NULL,
  `target_url` blob,
  `sha` char(40) NOT NULL,
  `repository_id` int(11) unsigned NOT NULL,
  `creator_id` int(11) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pull_request_id` int(11) unsigned DEFAULT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'default',
  `oauth_application_id` int(11) DEFAULT NULL,
  `tree_oid` binary(20) DEFAULT NULL,
  `commit_oid` binary(20) DEFAULT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_statuses_on_sha_and_context_and_repository_id` (`sha`,`context`,`repository_id`),
  KEY `index_statuses_on_commit_oid_and_repository_id_and_context` (`commit_oid`,`repository_id`,`context`),
  KEY `index_statuses_on_repository_id_and_created_at_and_context` (`repository_id`,`created_at`,`context`),
  KEY `index_statuses_on_repository_id_and_sha_and_context` (`repository_id`,`sha`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `stripe_webhooks`;
CREATE TABLE `stripe_webhooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` int(11) NOT NULL,
  `fingerprint` varchar(64) NOT NULL,
  `payload` text NOT NULL,
  `processed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `account_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `status` enum('pending','processed','ignored') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_stripe_webhooks_on_fingerprint` (`fingerprint`),
  KEY `index_stripe_webhooks_on_processed_at` (`processed_at`),
  KEY `index_stripe_webhooks_account_kind_created` (`account_id`,`kind`,`created_at`),
  KEY `index_stripe_webhooks_on_status_and_created_at` (`status`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_stafftools_roles`;
CREATE TABLE `user_stafftools_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stafftools_role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_stafftools_roles_on_user_id` (`user_id`),
  KEY `index_user_stafftools_roles_on_stafftools_role_id` (`stafftools_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `web_push_subscriptions`;
CREATE TABLE `web_push_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endpoint` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `auth` varchar(255) NOT NULL,
  `p256dh` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_agent` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_web_push_subscriptions_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `workflow_runs`;
CREATE TABLE `workflow_runs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `check_suite_id` bigint(11) unsigned NOT NULL,
  `run_number` int(11) NOT NULL DEFAULT '0',
  `trigger_id` bigint(20) DEFAULT NULL,
  `trigger_type` varchar(30) DEFAULT NULL,
  `event` varchar(50) DEFAULT NULL,
  `action` varbinary(400) DEFAULT NULL,
  `name` varbinary(1024) DEFAULT NULL,
  `head_branch` varbinary(1024) DEFAULT NULL,
  `head_sha` varchar(64) DEFAULT NULL,
  `workflow_file_path` varbinary(1024) DEFAULT NULL,
  `completed_log_url` varbinary(1024) DEFAULT NULL,
  `external_id` varchar(64) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_workflow_runs_on_check_suite_id` (`check_suite_id`),
  KEY `index_workflow_runs_on_workflow_and_check_suite_and_run_number` (`workflow_id`,`check_suite_id`,`run_number`),
  KEY `index_workflow_runs_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `workflows`;
CREATE TABLE `workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `name` varbinary(1024) NOT NULL,
  `path` varbinary(1024) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `enabled_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_workflows_on_repository_id_and_path` (`repository_id`,`path`),
  KEY `index_workflows_on_repository_id_and_state` (`repository_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `zuora_webhooks`;
CREATE TABLE `zuora_webhooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` int(11) NOT NULL,
  `account_id` varchar(64) DEFAULT NULL,
  `payload` text NOT NULL,
  `processed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('pending','processed','ignored','investigating') DEFAULT NULL,
  `investigation_notes` text,
  PRIMARY KEY (`id`),
  KEY `index_zuora_webhooks_on_processed_at` (`processed_at`),
  KEY `index_zuora_webhooks_on_account_id_and_kind` (`account_id`,`kind`),
  KEY `index_zuora_webhooks_on_status_and_created_at` (`status`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
