DROP TABLE IF EXISTS `cold_networks`;
CREATE TABLE `cold_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_cold_networks_on_network_id` (`network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `fileservers`;
CREATE TABLE `fileservers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `fqdn` varchar(255) NOT NULL,
  `embargoed` tinyint(1) NOT NULL DEFAULT '0',
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `evacuating` tinyint(1) NOT NULL DEFAULT '0',
  `quiescing` tinyint(1) NOT NULL DEFAULT '0',
  `datacenter` varchar(20) DEFAULT NULL,
  `rack` varchar(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `evacuating_reason` varchar(255) DEFAULT NULL,
  `quiescing_reason` varchar(255) DEFAULT NULL,
  `embargoed_reason` varchar(255) DEFAULT NULL,
  `non_voting` tinyint(1) NOT NULL DEFAULT '0',
  `hdd_storage` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_fileservers_on_host` (`host`),
  UNIQUE KEY `index_fileservers_on_fqdn` (`fqdn`),
  KEY `index_fileservers_by_location` (`datacenter`,`rack`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `gist_replicas`;
CREATE TABLE `gist_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gist_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `checksum` varchar(255) NOT NULL,
  `state` int(11) NOT NULL,
  `read_weight` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_gist_replicas_on_gist_id_and_host` (`gist_id`,`host`),
  KEY `index_gist_replicas_on_updated_at` (`updated_at`),
  KEY `index_gist_replicas_on_host` (`host`),
  KEY `index_gist_replicas_on_state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `network_replicas`;
CREATE TABLE `network_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `state` int(11) NOT NULL,
  `read_weight` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_host` (`network_id`,`host`),
  KEY `host_only` (`host`),
  KEY `index_network_replicas_on_state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_checksums`;
CREATE TABLE `repository_checksums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_type` int(11) NOT NULL DEFAULT '0',
  `checksum` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_checksums_on_repository_type` (`repository_id`,`repository_type`),
  KEY `index_repository_checksums_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_replicas`;
CREATE TABLE `repository_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_type` int(11) NOT NULL DEFAULT '0',
  `host` varchar(255) NOT NULL,
  `checksum` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_replicas_on_repository_type_and_host` (`repository_id`,`repository_type`,`host`),
  KEY `update_time` (`updated_at`),
  KEY `host_only` (`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
