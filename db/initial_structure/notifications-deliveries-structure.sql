DROP TABLE IF EXISTS `mobile_push_notification_deliveries`;
CREATE TABLE `mobile_push_notification_deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_device_token_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `list_type` varchar(64) NOT NULL,
  `list_id` int(11) NOT NULL,
  `thread_type` varchar(64) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `comment_type` varchar(64) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `reason` varchar(40) DEFAULT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '0',
  `state_explanation` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deliveries_on_comment_and_token` (`comment_type`,`comment_id`,`mobile_device_token_id`),
  KEY `index_deliveries_on_state_and_list_and_thread` (`state`,`list_type`,`list_id`,`thread_type`,`thread_id`),
  KEY `index_deliveries_on_user_and_state` (`user_id`,`state`),
  KEY `index_deliveries_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `notification_deliveries`;
CREATE TABLE `notification_deliveries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `delivered_at` datetime NOT NULL,
  `list_id` int(11) NOT NULL,
  `thread_key` varchar(255) NOT NULL,
  `comment_key` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `handler` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_list` (`list_id`,`thread_key`,`comment_key`,`user_id`,`handler`),
  KEY `by_time` (`delivered_at`),
  KEY `by_user` (`user_id`,`list_id`,`thread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
