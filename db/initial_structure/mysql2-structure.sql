DROP TABLE IF EXISTS `hidden_users`;
CREATE TABLE `hidden_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_hidden_users_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `notification_deliveries`;
CREATE TABLE `notification_deliveries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `delivered_at` datetime NOT NULL,
  `list_id` int(11) NOT NULL,
  `thread_key` varchar(255) NOT NULL,
  `comment_key` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `handler` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_list` (`list_id`,`thread_key`,`comment_key`,`user_id`,`handler`),
  KEY `by_time` (`delivered_at`),
  KEY `by_user` (`user_id`,`list_id`,`thread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `notification_entries`;
CREATE TABLE `notification_entries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `summary_id` int(11) NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) NOT NULL,
  `thread_key` varchar(80) NOT NULL,
  `unread` tinyint(1) DEFAULT '1',
  `reason` varchar(40) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_read_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_summary` (`user_id`,`summary_id`),
  UNIQUE KEY `index_notification_entries_for_user_by_thread` (`user_id`,`list_type`,`list_id`,`thread_key`),
  KEY `by_user` (`user_id`,`reason`),
  KEY `by_unread_user` (`user_id`,`unread`,`reason`),
  KEY `index_notification_entries_on_unread_and_updated_at` (`unread`,`updated_at`),
  KEY `index_notification_entries_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_notification_entries_for_list_by_thread` (`list_type`,`list_id`,`thread_key`),
  KEY `index_notification_entries_for_user_by_list` (`user_id`,`list_type`,`list_id`,`reason`),
  KEY `index_notification_entries_for_user_by_unread_list` (`user_id`,`list_type`,`list_id`,`unread`,`reason`),
  KEY `user_id_and_unread_and_thread_key_and_list_type_and_list_id` (`user_id`,`unread`,`thread_key`,`list_type`,`list_id`),
  KEY `user_id_and_unread_and_updated_at` (`user_id`,`unread`,`updated_at`),
  KEY `user_id_and_unread_and_list_type_and_updated_at` (`user_id`,`unread`,`list_type`,`updated_at`),
  KEY `user_id_and_list_type_and_updated_at` (`user_id`,`list_type`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `notification_entries_spam`;
CREATE TABLE `notification_entries_spam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `thread_key` varchar(80) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_notification_entries_spam_on_list_type_and_thread_key` (`list_type`,`thread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `notification_subscriptions`;
CREATE TABLE `notification_subscriptions` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) unsigned NOT NULL,
  `ignored` tinyint(1) unsigned NOT NULL,
  `notified` tinyint(1) unsigned DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_notification_subscriptions_on_user_list_type_and_list_id` (`user_id`,`list_type`,`list_id`),
  KEY `index_notification_subscriptions_on_list_ignored_and_created_at` (`list_type`,`list_id`,`ignored`,`created_at`),
  KEY `index_notification_subscriptions_on_notified_list_type_and_user` (`notified`,`list_type`,`user_id`),
  KEY `index_notification_subscriptions_on_user_list_ignored_created_at` (`user_id`,`list_type`,`ignored`,`created_at`),
  KEY `index_notification_subscriptions_on_notified_and_user_id` (`notified`,`user_id`),
  KEY `index_notification_subscriptions_on_list_ignored_and_user` (`list_type`,`list_id`,`ignored`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `notification_thread_subscription_reasons`;
CREATE TABLE `notification_thread_subscription_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `thread_key` varchar(80) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reason` varchar(50) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_notification_reasons_by_user` (`user_id`,`list_id`,`list_type`,`thread_key`,`reason`,`team_id`),
  KEY `index_notification_reasons_on_user_id_and_reason` (`user_id`,`reason`),
  KEY `index_notification_reasons_on_user_and_list_and_created_at` (`user_id`,`list_type`,`list_id`,`created_at`),
  KEY `index_notification_reasons_on_list_and_thread_and_created_at` (`list_type`,`list_id`,`thread_key`,`created_at`),
  KEY `index_notification_reasons_on_user_and_list_and_updated_at` (`user_id`,`list_type`,`list_id`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `notification_thread_subscriptions`;
CREATE TABLE `notification_thread_subscriptions` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) unsigned NOT NULL,
  `ignored` tinyint(1) unsigned NOT NULL,
  `reason` varchar(40) DEFAULT NULL,
  `thread_key` varchar(80) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_list_type_and_list_id_and_thread_key_and_user_id` (`list_type`,`list_id`,`thread_key`,`user_id`),
  KEY `index_notification_thread_subscriptions_on_lt_li_thrd_and_ignore` (`list_type`,`list_id`,`thread_key`,`ignored`),
  KEY `user_id_and_ignored_and_list_type_and_list_id_and_thread_key` (`user_id`,`ignored`,`list_type`,`list_id`,`thread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `notification_user_settings`;
CREATE TABLE `notification_user_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `raw_data` blob,
  `auto_subscribe` tinyint(1) NOT NULL DEFAULT '1',
  `auto_subscribe_teams` tinyint(1) NOT NULL DEFAULT '1',
  `notify_own_via_email` tinyint(1) NOT NULL DEFAULT '0',
  `participating_web` tinyint(1) NOT NULL DEFAULT '0',
  `participating_email` tinyint(1) NOT NULL DEFAULT '1',
  `subscribed_web` tinyint(1) NOT NULL DEFAULT '0',
  `subscribed_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify_comment_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify_pull_request_review_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify_pull_request_push_email` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerability_ui_alert` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerability_web` tinyint(1) NOT NULL DEFAULT '0',
  `vulnerability_email` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `rollup_summaries`;
CREATE TABLE `rollup_summaries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) unsigned DEFAULT NULL,
  `raw_data` blob,
  `thread_key` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_rollup_summaries_on_list_type_and_list_id_and_thread_key` (`list_type`,`list_id`,`thread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `stratocaster_events`;
CREATE TABLE `stratocaster_events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `raw_data` mediumblob,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_stratocaster_events_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
