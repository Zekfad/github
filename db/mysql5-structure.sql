DROP TABLE IF EXISTS `global_stratocaster_indexes`;
CREATE TABLE `global_stratocaster_indexes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index_key` varchar(32) NOT NULL,
  `value` bigint(20) NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_global_stratocaster_indexes_on_index_key_and_modified_at` (`index_key`,`modified_at`),
  KEY `index_global_stratocaster_indexes_on_index_key_and_value` (`index_key`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `key_values`;
CREATE TABLE `key_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` blob NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_key_values_on_key` (`key`),
  KEY `index_key_values_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `search_index_configurations`;
CREATE TABLE `search_index_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `index_type` varchar(100) NOT NULL,
  `index_version` int(11) NOT NULL,
  `slice_version` int(11) DEFAULT NULL,
  `version_sha` varchar(40) DEFAULT NULL,
  `is_readable` tinyint(1) NOT NULL DEFAULT '0',
  `is_writable` tinyint(1) NOT NULL DEFAULT '0',
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `cluster` varchar(100) NOT NULL,
  `slice_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_search_index_configurations_on_fullname` (`fullname`),
  KEY `index_search_index_configurations_on_index_type_and_version` (`index_type`,`index_version`),
  KEY `index_search_index_configurations_on_index_type_and_slice_name` (`index_type`,`slice_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `search_index_template_configurations`;
CREATE TABLE `search_index_template_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(127) NOT NULL,
  `template_type` varchar(100) NOT NULL,
  `template_version` int(11) NOT NULL DEFAULT '0',
  `cluster` varchar(100) NOT NULL,
  `version_sha` varchar(40) DEFAULT NULL,
  `is_writable` tinyint(1) NOT NULL DEFAULT '0',
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_search_index_template_configurations_on_fullname` (`fullname`),
  KEY `index_search_index_template_configurations_on_template_type` (`template_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `stratocaster_indexes`;
CREATE TABLE `stratocaster_indexes` (
  `index_key` varchar(32) NOT NULL DEFAULT '',
  `value` blob NOT NULL,
  PRIMARY KEY (`index_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
/*!50100 PARTITION BY KEY ()
PARTITIONS 2 */;
-- partitions are always set to 2 for consistency checks but may differ in production.
