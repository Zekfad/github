# rubocop:disable Style/FrozenStringLiteralComment

# IMPORTANT: This file is run after every db:migrate and needs to be idempotent.
require "github/dgit/sql"

# Rails 5.2 runs seeds using the inline adapter, which raises NoMethod
# errors when trying to call `clear_lock`. We need to set the adapter
# here for now until there is a better config option for the adapter used
# for seeds.
ActiveJob::Base.queue_adapter = Rails.application.config.active_job.queue_adapter

if Rails.env.development?

  unless User.ghost.present?
    User.create_ghost
  end

  mona = User.find_by(login: "monalisa")
  unless mona.present?
    mona = User.create(login: "monalisa",
                       password: GitHub.default_password,
                       email: "octocat@github.com",
                       require_email_verification: false,
                       billing_attempts: 0,
                       plan: GitHub::Plan.pro)
    mona.emails.map(&:verify!)
  end

  github = Organization.find_by(login: "github")
  unless github.present?
    github = Organization.create(login: "github",
                                 billing_email: "test-github-support@github.test.com",
                                 plan: GitHub::Plan.business_plus,
                                 seats: 1000,
                                 admin: mona)

    github.teams.create(name: "Employees").add_member(mona)
    GitHub::FeatureFlag.team_cache.clear
    mona.clear_employee_memo
    mona.update(gh_role: "staff")
    mona.stafftools_roles << StafftoolsRole.new(name: "super-admin")
  end

  github_business = Business.find_by(name: "GitHub, Inc")
  unless github_business.present?
    github_business = Business.create(
      name: "GitHub, Inc",
      seats: 1000,
      owners: [mona],
      organizations: [github],
      customer: Customer.create(
        billing_type: "invoice",
        billing_end_date: GitHub::Billing.today + 1.year,
        name: "GitHub, Inc",
        billing_attempts: 0,
        term_length: 12,
      ),
    )
  end


  category = Marketplace::Category.find_by_name("Default Category")
  unless category.present?
    Marketplace::Category.create(
      name: "Default Category",
      description: "The default marketplace app category.",
      navigation_visible: true,
    )
  end

  GitHub::DGit::SpokesSQL.run <<-SQL
    REPLACE INTO datacenters (datacenter, region)
    VALUES ("dc1", "region1"),
      ("dc2", "region2")
  SQL

  GitHub::DGit::SpokesSQL.run <<-SQL
    REPLACE INTO fileservers (host, fqdn, online, rack, datacenter, site, non_voting)
    VALUES ("dgit1", "dgit1.", 1, "ab1", "dc1", "dc1-iad", 0),
      ("dgit2", "dgit2.", 1, "ab2", "dc1", "dc1-iad", 0),
      ("dgit3", "dgit3.", 1, "ab3", "dc1", "dc1-iad", 0),
      ("dgit4", "dgit4.", 1, "ab4", "dc1", "dc1-iad", 0),
      ("dgit5", "dgit5.", 1, "n1", "dc2", "dc2-sea", 0)
  SQL

  Page::FileServer.github_sql.run <<-SQL
    TRUNCATE TABLE pages_fileservers
  SQL

  Page::FileServer.github_sql.run <<-SQL
    REPLACE INTO pages_fileservers (host, online, embargoed, disk_free, disk_used, created_at, updated_at)
    VALUES ("localhost", 1, 0, 100000, 0, NOW(), NOW())
  SQL

  (0..7).each do |partition|
    Page::Partition.github_sql.run <<-SQL, partition: partition.to_s(16)
      REPLACE INTO pages_partitions (`host`, `partition`, `disk_free`, `disk_used`, `created_at`, `updated_at`)
      VALUES ("localhost", :partition, 1000000, 0, NOW(), NOW())
    SQL
  end

  unless Survey.github_sql.run("SELECT id FROM surveys WHERE slug = :slug", slug: "org_downgrade").value?
    require "github/transitions/20150819185447_org_downgrade_survey.rb"
    transition = GitHub::Transitions::OrgExitSurvey.new
    transition.perform
  end

  unless Survey.where(slug: "per_seat_org_downgrade").exists?
    require "github/transitions/20160607211429_per_seat_downgrade_survey.rb"
    GitHub::Transitions::PerSeatDowngradeSurvey.new.perform
  end

  unless Survey.where(slug: "user_identification").exists?
    require "github/transitions/20190405113134_add_user_onboarding_survey"
    GitHub::Transitions::AddUserOnboardingSurvey.new(dry_run: false).perform
  end

  unless SurveyQuestion.where(short_text: "user_role").exists?
    require "github/transitions/20200210160651_add_role_question_to_user_identification_survey"
    GitHub::Transitions::AddRoleQuestionToUserIdentificationSurvey.new(dry_run: false).perform
  end

  unless Survey.where(slug: "per_seat_org_downgrade_text_only").exists?
    require "github/transitions/20200124160135_add_per_seat_org_downgrade_text_only_survey.rb"
    GitHub::Transitions::AddPerSeatOrgDowngradeTextOnlySurvey.new(dry_run: false).perform
  end

  unless SurveyQuestion.where(short_text: "organization_id", survey_id: Survey.where(slug: "per_seat_org_downgrade_text_only").last&.id).exists?
    require "github/transitions/20200316181412_add_organization_id_question_to_downgrade_survey"
    GitHub::Transitions::AddOrganizationIdQuestionToDowngradeSurvey.new(dry_run: false).perform
  end

  unless Survey.where(slug: "code_scanning").exists?
    require "github/transitions/20200324185251_create_code_scanning_waitlist_survey.rb"
    GitHub::Transitions::CreateCodeScanningWaitlistSurvey.new(dry_run: false).perform
  end

  unless Survey.find_by(slug: "code_scanning")&.questions&.find_by(short_text: "private_repositories").present?
    require "github/transitions/20200416134656_add_private_repository_advanced_security_survey_question.rb"
    GitHub::Transitions::AddPrivateRepositoryAdvancedSecuritySurveyQuestion.new(dry_run: false).perform
  end

  unless Survey.where(slug: "workspaces").exists?
    require "github/transitions/20200403135139_create_workspaces_signup_survey.rb"
    GitHub::Transitions::CreateWorkspacesSignupSurvey.new(dry_run: false).perform
  end

  unless Survey.where(slug: "okta_team_sync").exists?
    require "github/transitions/20200504234046_create_okta_team_sync_beta_signup_survey.rb"
    GitHub::Transitions::CreateOktaTeamSyncBetaSignupSurvey.new(dry_run: false).perform
  end

  unless Survey.where(slug: "org_creation").exists?
    require "github/transitions/20160628220736_add_org_creation_survey.rb"
    GitHub::Transitions::AddOrgCreationSurvey.new.perform
    require "github/transitions/20200519233106_update_org_creation_survey.rb"
    GitHub::Transitions::UpdateOrgCreationSurvey.new(dry_run: false).perform
  end

  Storage::FileServer.github_sql.run <<-SQL
    REPLACE INTO storage_file_servers (host, online, embargoed, created_at, updated_at)
    VALUES ("localhost", 1, 0, NOW(), NOW())
  SQL

  PreReceiveEnvironment.github_sql.run <<-SQL
    REPLACE INTO pre_receive_environments (id, name, image_url, checksum, created_at, updated_at)
    VALUES (1, 'Default', 'githubenterprise://internal', '0', NOW(), NOW())
  SQL

  # GitHub Actions Zuora Product UUIDs
  Billing::ProductUUID.create(product_type: "github.actions", product_key: "Public Repos Usage", billing_cycle: "month", zuora_product_id: "2c92c0f86cccaa55016ccf2b442b2ca6", zuora_product_rate_plan_id: "2c92c0f96ccccd81016ccf2b44e90326", zuora_product_rate_plan_charge_ids: { usage: "2c92c0f96ccccd84016ccf2b46746ed5"})
  Billing::ProductUUID.create(product_type: "github.actions", product_key: "Private Repos Usage", billing_cycle: "month", zuora_product_id: "2c92c0f86cccaa55016ccf2b442b2ca6", zuora_product_rate_plan_id: "2c92c0f86cccaa55016ccf2b473c6e5c", zuora_product_rate_plan_charge_ids: { usage: "2c92c0f96ccccd84016ccf2b47fd6eda"})


  # GitHub PackageRegistry Zuora Product UUIDs
  Billing::ProductUUID.create(product_type: "github.package_registry", product_key: Billing::PackageRegistry::ZuoraProduct.product_key, billing_cycle: "month", zuora_product_id: "2c92c0f96cfb2609016cfd7705ea5cb5", zuora_product_rate_plan_id: "2c92c0f86cfb1829016cfd7707373028", zuora_product_rate_plan_charge_ids: { bandwidth: "2c92c0f86cfb182c016cfd7708c90755" })

  # GitHub Shared Storage Product UUIDs
  Billing::ProductUUID.create(
    product_type: ::Billing::SharedStorage::ZuoraProduct.product_type,
    product_key: ::Billing::SharedStorage::ZuoraProduct.product_key,
    billing_cycle: "month",
    zuora_product_id: "2c92c0f86dcf20a8016dd618f863738b",
    zuora_product_rate_plan_id: "2c92c0f96dcf2f3d016dd619033654cc",
    zuora_product_rate_plan_charge_ids: { usage: "2c92c0f96dcf2f3d016dd619105b41fe" },
  )

  # GitHub Codespaces Product UUIDs
  Billing::ProductUUID.create(
    product_type: ::Billing::Codespaces::ZuoraProduct.product_type,
    product_key: ::Billing::Codespaces::ZuoraProduct.product_key,
    billing_cycle: "month",
    zuora_product_id: "2c92c0f97375285e017376f2b88a06d0",
    zuora_product_rate_plan_id: "2c92c0f8737518f8017376f2b9da2132",
    zuora_product_rate_plan_charge_ids: {
      compute: "2c92c0f973752850017376f2bad31799",
      storage: "2c92c0f973752850017376f2bb0817a3"
    }
  )


  unless OauthApplication.find_by_key(GitHub.gist3_oauth_client_id).present?
    github = Organization.find_by(login: "github")
    OauthApplication.create(name: "gist",
                            user: github,
                            url: "http://#{GitHub.gist3_host_name}",
                            callback_url: "http://#{GitHub.gist3_host_name}/auth/github/callback",
                            key: GitHub.gist3_oauth_client_id,
                            encrypted_secret: Rails.root.join("test/fixtures/encrypted_gist3_oauth_secret").binread,
                            domain: GitHub.gist3_host_name,
                            full_trust: true)
  end

  # Create FGP preset roles and permissions data
  GitHub.system_roles.reconcile(purge: true)

  # Create a feature preview and associated flags
  test_feature = FlipperFeature.create(name: "test_toggleable_feature")
  test_feature.enable

  Feature.create(
    public_name: "Test Toggleable Feature",
    slug: "test_toggleable_feature",
    flipper_feature_id: test_feature.id,
    description: "A test toggleable feature, to test Feature Preview.",
    feedback_category: "other",
  )

  # Enable octocaptcha in development
  FlipperFeature.create(name: "octocaptcha").enable
end
