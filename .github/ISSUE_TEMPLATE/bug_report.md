---
name: Bug report
about: Create a bug report to help us improve
title: '[Bug] '
labels: 'bug :beetle:'
assignees: ''

---

Thank you for taking the time to file an issue. Please make sure you have completed the relevant items from the checklist below. After doing so, you may delete the checklist.

- [ ] Briefly describe the bug's *user-visible* impact in the title
- [ ] Describe the bug and the situations in which it occurs in the "summary" section of the template below.
- [ ] If possible, include reproduction steps using the template below.
- [ ] If this is an API bug, please include curl commands and output if possible.
- [ ] If this is a visual bug, please include a screenshot or a GIF.
- [ ] Record any ideas you have as to the cause of the problem or how to fix it.
- [ ] Add any labels that you might think are relevant to the problem.
- [ ] Check the [Service Catalog](https://catalog.githubapp.com/) to find the correct team to @-mention. If it's still unclear which team to assign the issue to, drop by in [#eng-maintainership](https://github.slack.com/archives/CGYKZBE07), and we'll be happy to help you there.
    - Note: Very few folks <kbd>Watch</kbd> this repo as it is so high-traffic. _If you do not @-mention a team or individual the bug may not be seen_.
- [ ] Add a /cc for any other relevant teams.


**Summary**



**Steps to reproduce:**

1.
2.
3.

**Actual behaviour:**

[Describe the behavior that actually happens under the conditions listed above.]

**Expected behaviour:**

[Describe what you expect the behaviour under the conditions listed above to be.]

