# frozen_string_literal: true
require "json"
require "openssl"

module ERBLint
  module Linters
    module CustomHelpers
      CWD_PREFIX = "#{Dir.pwd}/"

      def is_counter_correct? processed_source
        comment_node = nil
        expected_count = 0
        rule_name = self.class.name.match(/:?:?(\w+)\Z/)[1]
        offenses_count = @offenses.length

        processed_source.parser.ast.descendants(:erb).each do |node|
          indicator_node, _, code_node, _ = *node
          indicator = indicator_node&.loc&.source
          comment = code_node&.loc&.source&.strip

          if indicator == "#" && comment.start_with?("erblint:count") && comment.match(rule_name)
            comment_node = code_node
            expected_count = comment.match(/\s(\d+)\s?$/)[1].to_i
          end
        end

        if offenses_count == 0
          add_offense(processed_source.to_source_range(comment_node.loc), "Unused erblint:count comment for #{rule_name}") if comment_node
          return
        end

        first_offense = @offenses[0]

        if comment_node.nil?
          add_offense(processed_source.to_source_range(first_offense.source_range), "#{rule_name}: If you must, add <%# erblint:counter #{rule_name} #{offenses_count} %> to bypass this check.")
        else
          clear_offenses
          if expected_count != offenses_count
            add_offense(processed_source.to_source_range(comment_node.loc), "Incorrect erblint:counter number for #{rule_name}. Expected: #{expected_count}, actual: #{offenses_count}.")
          end
        end
      end

      def is_rule_disabled? processed_source
        processed_source.parser.ast.descendants(:erb).each do |node|
          indicator_node, _, code_node, _ = *node
          indicator = indicator_node&.loc&.source
          comment = code_node&.loc&.source&.strip
          rule_name = self.class.name.match(/:?:?(\w+)\Z/)[1]

          if indicator == "#" && comment.start_with?("erblint:disable") && comment.match(rule_name)
            if @offenses.any?
              clear_offenses
            else
              add_offense(processed_source.to_source_range(code_node.loc), "Unused erblint:disable comment for #{rule_name}")
            end
          end
        end
      end

      def generate_offense klass, processed_source, tag, message = nil
        message ||= klass::MESSAGE
        if ENV["GITHUB_CI"]
          offense = generate_janky_failure(klass, processed_source, tag, message)
        else
          klass_name = klass.name.split("::")[-1]
          offense = ["#{klass_name}:#{message}", tag.node.loc.source].join("\n")
        end
        add_offense(processed_source.to_source_range(tag.loc), offense)
      end

      def possible_attribute_values tag, attr_name
        value = tag.attributes[attr_name]&.value || nil
        basic_conditional_code_check(value || "") || [value].compact
      end

      # Map possible values from condition
      def basic_conditional_code_check code
        conditional_match = code.match(/["'](.+)["']\sif|unless\s.+/) || code.match(/.+\s?\s["'](.+)["']\s:\s["'](.+)["']/)
        [conditional_match[1], conditional_match[2]].compact if conditional_match
      end

      def tags(processed_source)
        processed_source.parser.nodes_with_type(:tag).map { |tag_node| BetterHtml::Tree::Tag.from_node(tag_node) }
      end

      # basically element.classList.has(token)
      def has_class(tag, token)
        tokens = tag.attributes["class"]&.value&.split(" ")
        tokens && tokens.include?(token)
      end

      # see: https://github.com/github/ci/blob/master/docs/json-test-output.md
      def generate_janky_failure(klass, processed_source, tag, message)
        filename = processed_source.filename.sub(CWD_PREFIX, "")
        data = {
          suite: 'erblint',
          areas_of_responsibility: ['web_systems', 'design_systems'],
          name: klass.name.split("::")[-1],
          message: message,
          location: "#{filename}:#{tag.node.loc.line}"
        }
        data[:fingerprint] = generate_fingerprint(data)

        "===FAILURE===\n#{JSON.pretty_generate(data)}\n===END FAILURE==="
      end

      def generate_fingerprint(data, key = "THE CAKE IS A LIE")
        OpenSSL::HMAC.hexdigest("SHA256", key, data.to_json)[0..31]
      end
    end
  end
end
