require_relative "custom_helpers"

module ERBLint
  module Linters
    class NestedInteractiveElements < Linter
      include LinterRegistry
      include ERBLint::Linters::CustomHelpers

      INTERACTIVE_ELEMENTS = %w(button summary input select textarea a)
      MESSAGE = "Nesting interactive elements produces invalid HTML. Unexpected bad things can happen.".freeze

      def run(processed_source)
        last_interactive_element = nil
        tags(processed_source).each do |tag|
          next unless INTERACTIVE_ELEMENTS.include?(tag.name)

          if last_interactive_element && tag.name == last_interactive_element.name && tag.closing?
            last_interactive_element = nil
          end
          next if tag.closing?

          if last_interactive_element
            next if last_interactive_element.name == "summary" && tag.name == "a"
            next if tag.name == "input" && tag.attributes["type"]&.value == "hidden"

            message = "Found <#{tag.name}> nested inside of <#{last_interactive_element.name}>.\n" + MESSAGE
            generate_offense(self.class, processed_source, tag, message)
          end

          last_interactive_element = tag unless tag&.name == "input"
        end

        is_rule_disabled?(processed_source)
      end
    end
  end
end
