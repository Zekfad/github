# frozen_string_literal: true
require_relative 'custom_helpers'

module ERBLint
  module Linters
    class TooltipCounter < Linter
      include LinterRegistry
      include ERBLint::Linters::CustomHelpers

      TOOLTIP_CLASS = "tooltipped"
      MESSAGE = """A tooltip hides information by default so should be used sparingly.
See https://styleguide.github.com/primer/components/tooltips/#implementation-and-accessibility"""

      def run(processed_source)
        tags(processed_source).each do |tag|
          next if tag.closing?
          classes = tag.attributes["class"]&.value&.split(" ")
          next unless classes && classes.include?(TOOLTIP_CLASS)

          generate_offense(self.class, processed_source, tag)
        end

        is_counter_correct?(processed_source)
      end
    end
  end
end
