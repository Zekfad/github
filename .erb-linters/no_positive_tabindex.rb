# frozen_string_literal: true
require_relative 'custom_helpers'

module ERBLint
  module Linters
    class NoPositiveTabindex < Linter
      include LinterRegistry
      include ERBLint::Linters::CustomHelpers

      MESSAGE = """Positive tabindex is error-prone and often inaccessible.
http://styleguide.github.com/primer/principles/accessibility/#keyboard-accessibility"""

      def run(processed_source)
        tags(processed_source).each do |tag|
          next if tag.closing?
          next unless tag.attributes["tabindex"]&.value.to_i > 0

          generate_offense(self.class, processed_source, tag)
        end

        is_rule_disabled?(processed_source)
      end
    end
  end
end
