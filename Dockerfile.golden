FROM 895557238572.dkr.ecr.us-east-1.amazonaws.com/build-pipelines-github-full:07a485732f37a27238b5b5641f60ef067d7b822d

ARG GIT_TOKEN
RUN chmod 755 /usr/local/bundle
RUN chown -R build:docker /usr/local/bundle
RUN mkdir -p /workspace/github && chown build:build /workspace/github

USER build
COPY --chown=build:docker . /workspace/github/
RUN mkdir /workspace/github/log && mkdir -p /workspace/github/public && touch /workspace/github/public/robots.txt
RUN git config --global credential.helper '!f() { sleep 1; echo "username=git"; echo "password=${GIT_TOKEN}"; }; f'
WORKDIR /workspace/github

# Build ruby
RUN script/build-ruby

# Build ruby-next
RUN RUBY_NEXT=1 script/build-ruby

# Build git and then remove the source directory since it will contain a large git checkout and we don't need it
RUN script/build-git

# Pull babeld out of the main bootstrap. We can't remove the source directory because it builds in place though
RUN script/build-babeld && rm -f vendor/babeld/*.o

# Pull the node build out of the main bootstrap.
RUN script/build-node

# Build dgit-update and remove the vendored directories to save space.
RUN script/build-subproject dgit-update && rm -rf vendor/dgit-update/vendor/*

# Build gpgverify and remove the vendored directories to save space.
RUN script/build-subproject gpgverify && rm -rf vendor/gpgverify/.setup && rm -rf vendor/gpgverify/vendor/*

# Rather than invoke script/bootstrap in development mode, pull out these extra dependencies for certain builds
# we don't build ghvfs because it builds a docker container, which we won't cache in the golden image
# Build alambic
RUN script/build-subproject alambic

# Build lfs-server
RUN script/build-subproject lfs-server

# Build slumlord
RUN script/build-subproject slumlord

RUN git config --global credential.helper store

USER root
