ahead_behind
============

Get ahead-behind information from a given branch.

### Synopsis

    rpc.ahead_behind('master', 'other-branch')
    rpc.ahead_behind('master', ['other-branch', 'yet-another-branch', ...])

### Description

Returns a hash with the ahead-behind information of a given branch against
either another branch, or a set of branches.

Ahead-behind counts for a pair of branches A, B mean the number of commits
available in A which don't exist in B, and the number of commits available
in B that don't exist in A.

This is useful information to see if a branch is is a fast-forward of another
one, whether the two branches are divering, and how active a given branch is.

In the dotcom interface, we usually display this information in the Branches
page.

The batch mode (i.e. when the second argument is an array) is encouraged for
all use cases where more than a single ahead-behind comparison is needed,
not only because it saves roundtrips, but also because it makes computation
significantly less expensive.

### Arguments

Any of the given branch names in the call can be provided as a 40-character SHA1
instead (i.e. a full OID).

**It is encouraged to provide full OIDs instead of reference or branch names to
the call, because we do not want to write reference names in the cache key, as
the OIDs pointed by a reference eventually change**.

### Return value

The call returns a Hash like the following:

    {
      "chuyeow/master" => [3, 9],
      "subwindow/master" => [0, 3],
      "semis" => [1, 12],
      "webweaver/master" => [0, 0],
    }

Where the keys are the given "right side" branches to the call (the ones passed
as the second argument) and the values is a two-integer array containing the ahead
and behind count, respectively.

Note that even if a single reference is passed on the right side, a full hash will
always be returned (in this case, a hash with a single key).

    rpc.ahead_behind('master', 'other')
    #=> { 'other' => [3, 6] }

### Exceptions

This function raises no exceptions. If an ahead-behind call fails, an empty hash
will be returned. Users are expected to handle this gracefully.

A partial hash can also be returned (e.g. some of the requested may be missing
because we failed to perform the RPC operation on the FS, and yet some of the
keys may be available because they were already cached on the client).

### Examples

Read the ahead-behind information for several branches

    rpc.ahead_behind('master', ['cuyeow/master', 'subwindow/master', 'semis', 'webweaver/master'])
    # =>
    # {
    #   "chuyeow/master" => [3, 9],
    #   "subwindow/master" => [0, 3],
    #   "semis" => [1, 12],
    #   "webweaver/master" => [0, 0],
    # }

### Files

Defined in `client/ahead_behind.rb` and `backend/ahead_behind.rb`.
