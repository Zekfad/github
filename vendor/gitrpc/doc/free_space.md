free_space
==========

Calculate the free space on the partition holding a repository, in KB.
Implemented as a spawn of `df -k`.  Repositories hosted by DGit will
calculate the free space on all replicas and return the minimum.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.free_space

### Return value

When successful, the free space on the partition in KB.

### Exceptions

 - Raises `GitRPC::Client::CalculateFreeSpaceFailed` when `df -k` fails.

### Files

 - [gitrpc/client/repo_network_storage.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/repo_network_storage.rb)
