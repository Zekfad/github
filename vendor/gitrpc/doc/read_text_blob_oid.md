read_text_blob_oid
==================

Find the first non-binary blob and return it's oid. 

### Synopsis

    >> rpc = GitRPC.new('file:/great/repo.git')
    >> rpc.read_text_blob_oid(commit_oid)

### Description

Look through each blob in the tree (from the passed in commit) and
return the oid of the first one that isn't binary (based on git's
definition of binary-ness). 

### Arguments

commit_oid - The commit to look for the blob in. 

### Return Value

The 40 character oid that references the first text blob. 

### Files

Defined in
[gitrpc/client/read_text_blob_oid.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_text_blob_oid.rb)

