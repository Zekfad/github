merge_single_file
===================

Given head, base and merge-base blob oids, attempt to run a merge for a single file.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.merge_single_file(
      { :oid => 'a8be0a1c96eeaaaf4599aa5c96d75226b3a3c506', :path => 'README.md', :filemode => 0100644 },
      { :oid => 'c8f8d3ce4a9128d0ce1584a1f8a4e9ed5ac162f2', :path => 'README.md', :filemode => 0100644 },
      { :oid => '20a2fe87d4a0d6492c5b98c5ae549da9f63b8369', :path => 'README.md', :filemode => 0100644 },
    )

### Description

Runs a merge for a single blob, allowing you to see the file with the conflicts marked.

### Arguments

- `ancestor`: Hash with :oid, :path, and :filemode, of the blob at the merge-base

- `ours`: Hash with :oid, :path, and :filemode, of the blob at the head revision

- `theirs`: Hash with :oid, :path, and :filemode, of the blob at the base revision

- `options`: options to be passed to the Rugged merge method. See Rugged docs for more information

### Return Value

Returns a Hash with the following keys:

  - :automergeable => false for conflicts, true otherwise

  - :path => path of the file after merge

  - :filemode => file permission mode after merge

  - :data => file contents after the merge. If there were conflicts, it may contain conflict merge markers

### Files

Defined in [gitrpc/client/merge_single_file.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/merge_single_file.rb)
