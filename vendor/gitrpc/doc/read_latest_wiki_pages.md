`read_latest_wiki_pages`
=================

Find all blobs in the repository that are wiki pages and return their
paths and oids sorted by recency of the change.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_latest_wiki_pages("DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234")

### Description

Recursively find all blobs in the repository under the given directory,
and for each one determine if it is a valid wiki page (see below). Sort
the list of pages by the most recent commit that touched that file and
return the path to the file and the commit_oid that last touched it. 


### Arguments

* `oid` - The commit in which to look for wiki pages. 

### Return value

An array of pages with the following structure:

```ruby
[
  [path, commit_oid]
]
```

### Valid Wiki Pages

A blob is considered a valid wiki page if it has a file extension that
we can render in wikis. The list of extensions is currently:

* md
* mkd
* mkdn
* mdown
* markdown
* textile
* rdoc
* org
* creole
* rst
* rst.txt
* rest
* rest.txt
* asciidoc
* pod
* wiki
* mediawiki

### Files

Defined in [`lib/gitrpc/client/wiki.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/wiki.rb#L15)
