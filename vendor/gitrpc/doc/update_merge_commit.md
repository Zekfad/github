update_merge_commit
===================

Given a merge commit for a pull request, and a new message and author,
create a new merge commit with the same tree.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.update_merge_commit(commit_oid, :message => "Merge #192",
                                        :author => {
                                          :email => "andy@github.com",
                                          :name => "Andy Delcambre",
                                          :time => Time.now})

### Description

Create a new commit object with the same parents and tree as the given
merge commit. The new commit will have the new options passed in.

### Arguments

- `commit_oid`: The existing commit to use the tree and parents from.

- `info`: The commit information hash. See the following section for
  details on this data structure.

- `squash_commits`: Squash history into a single merge commit.

- `&block`: An optional block that signs the commit. This block should accept a
            String argument containing the staged raw commit. The block should
            return the signature over the commit.

            Eg.
              rpc = GitRPC.new('file:/great/repo.git')
              opts = {
                :message => "Merge #192",
                :author => {
                  :email => "andy@github.com",
                  :name => "Andy Delcambre",
                  :time => Time.now
                }
              }
              rpc.update_merge_commit(commit_oid, opts) do |raw|
                SigningService.sign(raw)
              end

### Commit Info

The `info` argument is for specifying commit metadata. It has the
following structure:

    { 'message'   => required string commit message,
      'committer' => required committer information hash,
      'author'    => optional author information hash }

The `committer` and `author` hashes have the following members:

    { 'name'      => required full name string,
      'email'     => required email address string,
      'time'      => required time value }

The `committer` information hash is required; the `author` is optional. If
no `author` is given the `committer` information is used in both places.

The `time` value may be either an ISO8601 string or a simple unixtime array in
the format `[unixtime, utc_offset]` where both elements are integer seconds.

### Return Value

The call returns the string oid of the newly created commit.

### Files

Defined in [gitrpc/client/update_merge_commit.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/update_merge_commit.rb)
