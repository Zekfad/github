read_head_oid
=============

Retrieve the OID of the `HEAD` ref. 

### Synopsis

    >> rpc = GitRPC.new('file:/great/repo.git')
    >> rpc.read_head_oid

### Description

Retrieve the OID of the HEAD ref in the git repo. This is used in gist
to show the main gist page (which always shows the tip of the head ref). 

### Arguments

None

### Return Value

The 40 character oid that the HEAD ref points at. 

### Files

Defined in
[gitrpc/client/read_refs.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_refs.rb#L27)
