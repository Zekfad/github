parse_tag_signatures
======================

Read GPG signatures and signing payloads from tags.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.parse_tag_signatures([oid1, oid2, ...])

### Description

Read GPG signatures and signing payloads from tags.

### Arguments

The only argument is an Array of 40 char oid strings identifying the tag objects
to read signing data from.

### Return Value

Returns an Array of Arrays, each containing signing data for one of the tags
passed as arguments. The first element in each Array is the GPG signature,
parsed from the tag message if present, or nil otherwise.The second element is
the signing payload String, or nil if the tag is not signed. The ordering of the
signature data Arrays matches the ordering of the oids passed as arguments.

### Exceptions

Raises `GitRPC::InvalidObject` if object isn't a tag.
Raises `GitRPC::ObjectMissing` if an object is missing.
Raises `GitRPC::InvalidFullOid` if an OID is invalid.

### Files

Defined in
[gitrpc/client/parse_signatures.rb](https://github.com/github/github/blob/master/vendor/gitrpc/lib/gitrpc/client/parse_signatures.rb#L29-L31)
