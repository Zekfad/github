create_tree
===================

Create a new tree with a set of contents, optionaly updating an existing
tree.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.create_tree(files, base_tree_oid = nil)

### Description

Create a new tree object with the contents specified in the `files`
hash. This can either be a brand new tree, or update an existing tree
specified by oid in `base_tree_oid`.

### Arguments

- `files`: Hash of `filename => data` pairs. The filename must be a string
   and may include slashes into subtrees. The data should be either a Hash, a
   blob oid or nil (to indicate that the file with the specified filename
   should be deleted). If data is a Hash, the following keys are acceptable:

   - `data`   - String blob content of the new tree entry
   - `mode`   - Numeric (octal) file mode, e.g. 0100644 or 0100755
   - `source` - String path of an old tree entry to remove (for file moves).
                Will also provide mode and data if not overridden.

- `base_tree_oid`: Optional tree oid pointing to a tree to use as the
  base for the new tree. If specified any file paths not specified in
  the `files` param will be taken from this tree.

### Return Value

The call returns the string oid of the newly created tree.

### Files

Defined in [gitrpc/client/create_tree_changes.rb](https://github.com/github/gitrpc/blob/8c8bce64/lib/gitrpc/client/create_tree_changes.rb)
