read_tree_entry_oids
====================

Fetch the oids for multiple paths in a specified tree.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tree_entry_oids(commit_oid, ["README.md", "lib/great"])

### Description

Retrieve multiple entries in a tree and return their oids. This only
loads the object's tree entries and not any objects (i.e. no blob load
ever takes place).

### Arguments

This call takes two arguments.

oid   - The oid of the commit or tree to find the entries in.
paths - The array of strings identifying paths to look up.

### Return value

A hash of `{ path => oid }` for all the specified paths.

### Exceptions

If the oid does not exist, a `GitRPC::ObjectMissing` error is raised.

If one of the specified paths does not exist, a
`GitRPC::NoSuchPath` error is raised.

### Files

Defined in
[gitrpc/client/read_tree_entries.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_tree_entries.rb#L107)
