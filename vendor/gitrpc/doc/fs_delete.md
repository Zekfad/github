fs_delete
=========

Remove a file or directory recursively from within the git repository on disk.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.fs_delete("info/some-file.txt")

### Description

Remove the literal file from within the git repository. If the file is a
directory, remove it and all its contents recursively. The file need not exist
prior to the call.

This does not operate on versioned git data but the repository structure itself.

### Arguments

 - `file` - The string filename to remove, relative to the git repository directory.

### Return value

The call always returns nil.

### Exceptions

 - Raises `GitRPC::IllegalPath` when attempt is made to access a file outside the repository.
 - Raises `GitRPC::SystemError` when a system level occurred while accessing the file.

### Files

 - [gitrpc/client/disk.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/disk.rb)
 - [gitrpc/backend/disk.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/backend/disk.rb)
