describe
=========

A wrapper around `git describe`. Returns the most recent tag that is
reachable from a commit or blob.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.describe(object_name, sha_length=40)

### Description

Takes a commit sha and a length specifier and returns a string
description of the most recent tag reachable from the commit. Alternatively
takes a blob and returns the same result such that the tag contains that blob.

If length is zero, just the tag name will be returned.

If length is non-zero, the format will be:

```
<tag-name>-<revisions behind commit>-g<commit sha truncated to length>
```

If a blob sha is given, the format will be the same as above with an additional
suffix:

```
:<name-of-blob>
```


### Arguments

commitish - The commit to start looking for a tag from. Any format
defined in gitrevisions is acceptable.
length - The length of the portion of the commit sha to append to the
tag name.


### Return Value

A string representation of the most recent tag reachable from the commit.


### Files

Defined in [gitrpc/client/describe.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/describe.rb)
