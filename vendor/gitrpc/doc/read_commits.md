read_commits
============

Retrieve commit information for a list of commit oids.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_commits(oids)

### Description

Retrieve commit information for a list of commit oids.

This method is optimized for loading multiple commits using a
single cache lookup followed possibly by a single RPC call for commits
that aren't in cache.

### Arguments

The call takes a single array of 40 char oid strings identifying commits
to load. Only full sha1 object ids are allowed. References (branch / tag
names) and abbreviated sha1s are not permitted. All oids must identify
commit objects.

### Return Value

The call returns an array guaranteed to have the same number of elements
as the list of oids given as input where the elements of the resulting
array match up with the `oids` provided.

Each element of the array is a hash with the following members:

    { 'type'      => 'commit',
      'oid'       => string sha1,
      'tree'      => string sha1,
      'parents'   => [oid, ...],
      'author'    => [name, email, time],
      'committer' => [name, email, time],
      'message'   => string commit message,
      'encoding'  => string commit message encoding name }

All keys are guaranteed to be present and non-nil. When a commit has
no parent commits, the `parents` array is present but empty.

The `author` and `committer` values are simple three element tuple arrays.
All three elements are guaranteed to be present. The `time` element is an
array of the form `[unixtime, utc_offset]` array where the both values are
integers in seconds. The `unixtime` is the number of seconds since epoch. The
`utc_offset` is the number of seconds for the timezone offset where the commit
was made.

### Exceptions

Raises `GitRPC::ObjectMissing` when an object specified in `oids`
does not exist or can not be loaded from the object store.

Raises `GitRPC::InvalidObject` when the object is found but is not a
commit object.

### Examples

Read commits for all branch heads:

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    branches = rpc.read_refs.select { |ref, oid| ref =~ %r(^refs/heads/) }
    commits = rpc.read_commits(branches.values)

Read commits for the first 30 revisions in the commit history of the
master branch:

    head = rpc.read_refs['refs/heads/master']
    oids = rpc.list_revision_history(head)
    commits = rpc.read_commits(oids[0,30])

### Files

Defined in [gitrpc/client/read_objects.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/read_objects.rb#L67)
