fs_write
========

Read a file from the git repository on disk.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.fs_write("info/new-file.txt", "some great data\n")

### Description

Write the entire contents of a file in the repository, creating intermediate
directories if necessary.

This does not operate on versioned git data but the repository structure itself.

### Arguments

 - `file` - The string filename to write, relative to the git repository directory.
 - `data` - The string data to write, or `nil` to truncate the file.

### Return value

The call always returns nil.

### Exceptions

 - Raises `GitRPC::IllegalPath` when attempt is made to access a file outside the repository.
 - Raises `GitRPC::InvalidRepository` when the repository directory does not exist.
 - Raises `GitRPC::SystemError` when a system level occurred while accessing the file.

### Files

 - [gitrpc/client/disk.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/disk.rb)
 - [gitrpc/backend/disk.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/backend/disk.rb)
