update_ref
==========

Update a ref's oid.

### Synopsis

    >> rpc = GitRPC.new('file:/great/repo.git')
    >> rpc.update_ref("refs/heads/master", "1eb5554da4c0b8d39020b6d92c3cb4d2c94f7b60")

### Description

### Arguments

- `ref` - A String of the full ref name such as `"refs/heads/foo"`. No
          validation of the ref will be done so you may update refs outside the
          typical refs/heads namespace.
- `new_value` - A String OID of the new commit.
- `old_value` - An optional String OID of the old commit to validate against.
                If the old value is different than the current, a
                `GitRPC::RefUpdateError` exception will be raised.

### Return Value

`nil` will always be returned, however if there was an error updating the ref, a
`GitRPC::RefUpdateError` exception will be raised.

### Caching

Calling `update_ref` will force a recalculation of `repository_reference_key` to
invalidate the client's current ref cache.

### See Also

 - [read\_refs()](read_refs.md) - For reading ref values.
