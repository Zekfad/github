read_tree_entry
===============

Fetch a single entry in a tree by the path. 

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tree_entry(commit_oid, path = 'lib/great')

### Description

Retrieve a single entry in a tree. The value returned will be all
information about the single entry in the tree, including it's type
(blob, tree, or commit for submodules). 

If the entry is blob the contents of the blob are returned. The blob
contents may be truncated (see truncation, below). 

Additionally, if the entry in the tree is a symlink, the symlink will be
followed, and the oid of the symlink target will also be returned,
unless the symlink points outside of the repository 

### Arguments

The call takes one or two arguments, and an options hash. The first argument
is the 40 char oid string identifying the commit or tree object to read. If a
commit is given, it is peeled to find the tree. The second argument is the path
to the tree entry to return. This argument is optional and defaults to the root
of the given tree in the first parameter.

The options hash allows specifying the size limits for the blob
returned, as well as the type of object expected. The options hash looks
like:

    {
      "truncate" => Bytes to truncate the blobs to.
      "limit"    => Number of bytes above which to never return a blob.
      "type"     => The string type expected, "blob", "tree", or "commit"
    }


### Return Value

The call returns a hash with the following structure:

Note that this structure is the same as a single entry from the
read_tree_entries call. 

    {
      "type" => Either tree, blob or commit (for submodules),
      "oid"  => The oid of the entry,
      "mode" => The mode of the entry,
      "name" => The name of the entry,
      "truncated" => True if the blob was truncated.
      "size" => The size of the blob. nil if this entry is not a blob.
      "symlink_target" => The OID of the target of the symlink if it exists.
        This key is not present if the entry is not a symlink
        or the symlink points outside of the repo. 
      "symlink_target_object" => A hash just like this one for the symlink
        target, if it exists. This key is not present if the entry is not a
        symlink or the symlink points outside of the repo. 
      "data" => The contents of the blob. This key does not exist if the 
                object is not a blob. 
      "encoding" => The encoding name of the blob data. This key does not exist
                    if the object is not a blob.
      "binary" => A boolean specifying whether the blob data is binary (true)
                  or text (false). This key does not exist if the object is not
                  a blob.
    }

### Exceptions

Raises `GitRPC::InvalidObject` when the provided oid does not point to
an object of the specified type. If nil is passed as the type, all types
are valid and this exception shall not be raised.

Raises `GitRPC::NoSuchPath` when the path does not exist in the given
tree.

Raises `GitRPC::ObjectMissing` when the oid provided does not point to
an object.

### Content Truncation

This call truncates blobs according to the global truncation limits
inside GitRPC.  You may pass in limits in the options hash, but are
defaulted to the global GitRPC limits.

There are two limits for blobs:

1. The truncation limit defaults to 1mb, and any data above this limit will be
truncated in this call. 

2. The maximum data limit defaults to 10mb. Any blobs larger than this size
will not be returned *at all*. This is to limit the resource requirements on
the fileservers so as to not need to read the entire blob off disk to truncate
it. 

There is a separate api call called [`read_full_blob`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_blobs.rb#L83) that does not
truncate blobs at all. If you need the full blob, you can use the oid
returned in this call to fetch the contents, but please note, this could
be very expensive, use with care. 

### Examples

Read the tree entry for the repository root

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    rpc.read_tree_entry('deadbee...')

Read the tree entry for a repository subtree

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    rpc.read_tree_entry('deadbee...', 'lib/foo')

### Files

Defined in
[gitrpc/client/read_tree_entries.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_tree_entries.rb#L33)
