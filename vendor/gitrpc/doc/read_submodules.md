read_submodules
==========

Read the submodules from .gitmodules at a specific commit.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_submodules(sha)

### Description

Get the list of submodules for the repository at the current sha. 

### Arguments

- `sha`: The commit sha to fetch the submodules from.

### Return Value

Returns an array of submodules:

    [
      {
        "url"  => "Remote url of the submodule",
        "name" => "The name of the submodule",
        "path" => "The path of the submodule in the repo"
      }
    ]

### Files

Defined in
[gitrpc/client/read_submodules.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_submodules.rb)
