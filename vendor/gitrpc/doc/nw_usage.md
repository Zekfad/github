nw_usage
========

Calculate the space used by a repository network, in KB.  Implemented as a
spawn of `git nw-usage`.  Repositories hosted by DGit will calculate the
space used on each replica and return the maximum.  As a side effect, the
file `usage` will be stored in the network directory, containing the space
used (in KB) in text format.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.nw_usage

### Return value

When successful, the disk space used by the repo network, in KB.

### Exceptions

 - Raises `GitRPC::Client::CalculateDiskUsageFailed` when `git nw-usage` fails.

### Files

 - [gitrpc/client/repo_network_storage.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/repo_network_storage.rb)
