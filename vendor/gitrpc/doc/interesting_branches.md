interesting_branches
====================

Find a list of recent branches, primarily ones without a pull request
already associated with them. 

### Summary

    => rpc = GitRPC.new('file:/great/repo.git')
    => rpc.interesting_branches(default_branch="master", limit=50)

### Description

Find a list of the most recent branches in the repo up to the given
limit. The given default branch is not included in the results.

"interesting" is defined as recent branches, which do not already have
pull-request associated with them. This is used for the sample
comparisons on the compare view in dotcom.

If there are not enough recent branches without pull-requests, the
remainder up to the limit is filled with branches which do have pull
requests.

### Arguments

* `default_branch`: The default branch in the repo to exclude from the
  results.
* `limit`: The number of interesting branches to return.

Both arguments are optional.

### Return Value

Returns an array of interesting branches. Each branch is represented as
a three element array as follows.

    [
      timestamp, # Last updated timestamp for this branch
      sha,       # The target that the branch points at
      ref        # The name of the ref
    ]

### Files

Defined in [`lib/gitrpc/client/interesting_branches.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/interesting_branches.rb)
