language_stats
==============

Find the breakdown of software languages in the repository, as reported
by linguist.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.language_stats("DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234", incremental = true)

### Description

Use linguist to calculate language statistics for the entire repository.
This looks at all blobs that are source code (as determined by linguist)
and then uses linguist to determine which language each of those blobs
are in. The sum up the total size of all blobs for each language and
return the total size in bytes for each language.

### Caching

The language stats calls are unique in that they have a on disk cache on
the fileservers, in the repository themselves. This is a language stats
file which is in Messagepack, and then compressed using zlib. The cache
is used automatically by default, and if you pass the incremental flag,
when calculating stats for a new commit, it will use the stats from a
previous commit to speed up detection.

### Arguments

* `commit_id` - The commit in the repository to calculate language stats
  from.
* `incremental` - Use the previous cached values to speed up language
  reconigtion time on new commits

### Return value

A hash of language stats with the following structure:

```ruby
{
  'Ruby' => 46319,     # Language key, and total size of all blobs with
  'JavaScript' => 258  # that lanaguage as the value, in bytes
}
```

### Files

Defined in [`lib/gitrpc/client/language_stats.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/language_stats.rb#L3)
