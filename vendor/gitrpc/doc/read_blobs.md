read_blobs
==========

Retrieve blob information and content for a list of blob oids.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_blobs(oids)

### Description

Retrieve blob information and content for a list of blob oids.

This method is optimized for loading multiple blobs using a
single cache lookup followed possibly by a single RPC call for blobs
that aren't in cache.

### Arguments

The call takes a single array of 40 char oid strings identifying blobs
to load. Only full sha1 object ids are allowed. All oids must identify
blob objects.

### Return Value

The call returns an array guaranteed to have the same number of elements
as the list of oids given as input where the elements of the resulting
array match up with the `oids` provided.

Each element of the array is a hash with the following members:

    { 'type'      => 'blob',
      'oid'       => string sha1,
      'size'      => integer byte size of blob object,
      'data'      => string blob data,
      'encoding'  => string blob data character encoding,
      'binary'    => boolean indicating whether blob is binary or text,
      'truncated' => boolean indicating whether blob exceeded a max size }

All keys are guaranteed to be present and non-nil. If a blob exceeds 500kb, the
data member will be truncated to 500kb. If a blob exceeds 5MB, the blob will be
returned with an empty data member. The full contents of truncated blobs can be
fetched with `read_full_blob`.

### Exceptions

Raises `GitRPC::ObjectMissing` when an object specified in `oids`
does not exist or can not be loaded from the object store.

Raises `GitRPC::InvalidObject` when the object is found but is not a
blob object.

### Examples

Read a single blob

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    rpc.read_blobs(['deadbee...'])

Read blob data for all top-level tree entries on the master branch:

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    commit_sha = rpc.read_refs['refs/heads/master']
    commit = rpc.read_commits([commit_sha]).first
    tree = rpc.read_trees([commit.tree]).first
    blob_entries = tree['entries'].select { |entry| entry['type'] == 'blob' }
    blob_oids = blob_entries.map { |entry| entry['oid'] }
    blobs = rpc.read_blobs(blob_oids)

### Files

Defined in [gitrpc/client/read_objects.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/read_objects.rb#L129)
