list_revision_history
=====================

Retrieve a revision list starting at the specified commit.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.list_revision_history(oid)

### Description

Retrieve a revision list starting at the specified commit.
This walks commit history but does not load any tree or blob objects
and has good performance characteristics as a result.

### Arguments

The call takes a single 40 char oid string identifying the commit where
history traversal starts.

### Return Value

Returns an array of oid strings in topological order, starting with the
oid provided as input. The array has only a single element if the commit
has no parents.

### Examples

    >> rpc = GitRPC.new('/great/repo.git')
    >> rpc.read_refs['refs/heads/master']
    => "cd55e2dbaf6c0e34b593f91312e1932839531273"
    >> rpc.list_revision_history("cd55e2dbaf6c0e34b593f91312e1932839531273").size
    => 226
    >> pp rpc.list_revision_history("cd55e2dbaf6c0e34b593f91312e1932839531273")
    ["cd55e2dbaf6c0e34b593f91312e1932839531273",
     "a921599b3263ba8e8cc0d8f8c498645d71ae8891",
     "ad764905477a97c685c0a958bc7530f334100085",
     ...]

### Notes

There is currently no limit on the number of oids returned in the
resultset. Assuming the default memcached maximum cache entry size of
1MB, it should be possible to fit around 25,500 revisions. Larger
histories will not be cached and require an RPC call and disk access
to retrieve.

### Files

Defined in [gitrpc/client/history.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/history.rb#L42)
