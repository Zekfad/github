read_blob_contributors
======================

For a particular file starting at a given commit in this Repository, fetch a
list of unique contributor email addresses plus their most recent commit OID
and the date of that commit, plus the count of commits they are the author of
(plus the commits they are a co-author of, if requested), in sorted order by
commit date.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_blob_contributors('2eafc6a3a24d1311fe47d2cadac58dc693df0c48', 'README.md')
    >> { 'data' =>
         [ { 'author' => 'someone@email.com',
             'commit' => '3596496baae66500ebdca4146104bb17d96d0fe0',
             'time'   => 1389900558,
             'offset' => -28800,
             'count'  => 1 },
           { 'author' => 'someoneelse@example.com',
             ... } ],
         'truncated' => true / false }

### Description

Given a file name and starting commit, this will fetch a list of the authors
who have touched that file in reverse chronological order, and for each
unique author it will return the author's email address, the OID of the last
commit they made, the time of that commit (as epoch seconds plus the offset
from UTC in seconds), and the count of commits they are the author of.

This list of authors is returned as a structure containing a list of authors
and a flag `truncated` indicating if the list is comprehensive or if there
may be additional authors that were left off because of a timeout or data
size limit.

If the operation times out and `truncated` is true, the count of commits
associated with an author may be inaccurate.

### Arguments

The call requires two arguments: the 40 character OID string of the commit at
which to begin traversing the project history and the path from the root of
the repository of the specific file being examined.

An optional `co_authors` argument, when `true`, will consider emails tagged as
`Co-authored-by` in the commit message trailers as authors, just as if they
were `author`s. Defaults to `false`.

By default, each author is uniquely identified by their email, regardless of the
name used. If the optional `group_by_signature` argument is `true`, the entire
signature (name and email) is used as an identifier, meaning the same e-mail can
appear multiple times on the list (if it was used with different names).

### Return Value

The call returns a structure that looks like:

    { 'data' =>
      [ { 'author' => 'someone@email.com',
          'commit' => '3596496baae66500ebdca4146104bb17d96d0fe0',
          'time'   => 1389900558,
          'offset' => -28800,
          'count'  => 1 },
        { 'author' => 'someoneelse@example.com',
          ... } ],
      'truncated' => true / false }

### Exceptions

Raises `GitRPC::InvalidObject` if the given commit OID does not resolve to a
commit or when the given path cannot be found in the repository.

Raises `GitRPC::Timeout` if we run out of time processing the request,
although it works hard to provide truncated results in this case instead of
raising an exception.  There are some cases, however, where external factors
may result in a timeout.

### Files

* Client API in [lib/gitrpc/client/read_blob_and_repository_contributors.rb](/lib/gitrpc/client/read_blob_and_repository_contributors.rb)
* Backend implementation in [lib/gitrpc/backend/read_contributors.rb](/lib/gitrpc/backend/read_contributors.rb)
