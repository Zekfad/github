read_tree_entry
===============

Fetch a single entry in a tree by the path. And return the oid.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tree_entry_oid(commit_oid, path = 'lib/great', type = "blob")

### Description

Retrieve a single entry in a tree and return the oid. This only loads
the object's tree entry and not the object itself (i.e. no blob load
ever takes place). 

If no path is passed, the oid of the root tree is returned. 

If a type is passed, if the object at the path does not match the type,
a `GitRPC::InvalidObject` error is raised. 

### Arguments

This calls takes one, two, or three arguments. 

commit_oid - The oid of the commit to find the tree entry in. 
path       - OPTIONAL: The path to the tree entry to return. If path is not
               passed the root tree is returned. 
type       - OPTIONAL: The type of object we are expecting. If the type
               does not match, an exception is raised. Defaults to
               returning any type. 

### Return value

The oid of the tree entry. 

### Exceptions

If the type of the entry does not match the passed type, a
`GitRPC::InvalidObject` error is raised.

If the path cannot be found in the tree, a
`GitRPC::NoSuchPath` error is raised.

### Files

Defined in
[gitrpc/client/read_tree_entries.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_tree_entries.rb#L83)
