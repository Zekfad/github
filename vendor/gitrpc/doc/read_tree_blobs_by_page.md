read_tree_blobs_by_page
=======================

Fetch a summary of all trees in this Repository and return the results by page.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tree_blobs_by_page(tree_id, 0)

### Description

Fetch a summary of all trees in this Repository and return paginated results.
Each page is returned as a hash enumerating the current page number, the next
page number (if a next page is available), and an array of blob hashes. The
actual blob data is **not** returned by this method.

### Arguments

The call takes the 40 char oid string identifying the tree object to read and
the page number of the results to return.

### Return Value

The call returns a hash containing the following information:

    { 'current_page' => current page number (zero based),
      'next_page'    => next page number or nil if this is the last page,
      'blobs'        => array of blob hashes }

Each blob hash in the array contains the following data:

    { 'oid'   => 40 char sha1 oid of the blob,
      'name'  => filename (not including the path prefix),
      'size'  => the untruncated size of the blob,
      'mode'  => the integer filemode of the blob }

### Exceptions

Raises `GitRPC::InvalidObject` when the provided oid does not resolve to a
tree object.

### Examples

Read all the blob summaries for the repository

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    page = 0
    tree_id = 'deadbee...'
    blobs = []

    while(page) do
      hash = rpc.read_tree_blobs_by_page(tree_id, page)
      blobs.concat(hash['blobs'])
      page = hash['next_page']
    end

Read the full blob content for all blobs in the repository less than 100K in
size

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    page = 0
    tree_id = 'deadbee...'
    oids = []

    while(page) do
      hash = rpc.read_tree_blobs_by_page(tree_id, page)
      ary = hash['blobs'].map { |blob| blob['size'] > 100*1024 ? nil : blob['oid'] }
      oids.concat(ary)
      page = hash['next_page']
    end

    oids.compact!
    rpc.read_blobs(oids)

### Files

Defined in
[gitrpc/client/read_tree_blobs.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_tree_blobs.rb#L65)
