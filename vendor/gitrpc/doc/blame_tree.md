blame_tree
==========

A wrapper around `git blame-tree`, a custom command that finds the most
recent commit that touched each entry in a tree. This is used to show
last modified times in a directory listing. 

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.blame_tree("DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234", "dir/")

### Description

Look up a tree at a specific revision, and find the most recent commit
that touched each entry in the tree. This simply shells out to our
custom `git blame-tree` command in our fork of git.
https://github.com/github/git/blob/github/builtin/blame-tree.c

### Arguments

* `commit_id` - The commit you want to look for the tree in. This will
  be the starting point for the history search to blame the tree.
* `path` - The path to a tree at the commit to blame the contents of.
  This is optional and will do the root tree if ommitted.

### Return value

A hash with the following structure, each key is an entry in the tree,
the corresponding value is the oid of the last commit to touch that
entry.

```
{
  "README" => "DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234",
}
```

### Files

Defined in [`lib/gitrpc/client/blame_tree.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/blame_tree.rb)
