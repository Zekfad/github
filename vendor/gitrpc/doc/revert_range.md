revert_range
============

Given a range of commits, create a new commit that reverts the range.

### Summary

    rpc.revert_range(commit_oid1, commit_oid2, info_hash)

### Description

Calculate the reverse diff that a series of commits added and apply it
to the repository in a new commit. The effect at the end will be code in
the state as if the series of commits had not happened.

This is primarily used in wikis.

### Arguments

- `commit_oid1`: The start of the commit range to revert.
- `commit_oid2`: The end of the commit range. Optional: if `commit_oid2`
  is nil then we just revert the single commit specified by `commit_oid1`.
- `info`: The commit information hash. See the following section for
  details on this data structure.

### Commit Info

The `info` argument is for specifying commit metadata. It has the
following structure:

    { 'message'   => required string commit message,
      'committer' => required committer information hash,
      'author'    => optional author information hash,
      'tree'      => optional oid of the tree you'd like to commit}

The `committer` and `author` hashes have the following members:

    { 'name'      => required full name string,
      'email'     => required email address string,
      'time'      => required  time value }

The `committer` information hash is required; the `author` is optional. If
no `author` is given the `committer` information is used in both places.

The `time` value may be either an ISO8601 string or a simple unixtime array in
the format `[unixtime, utc_offset]` where both elements are integer seconds.

### Return Value

Returns the oid of the newly created revert commit.

### Files

Defined in [`lib/gitrpc/client/revert_range.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/revert_range.rb).
