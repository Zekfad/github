slow
====

Emulate a slow response by sleeping on the server side.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.slow(time)

### Description

Emulate a slow response by sleeping on the server side. This is
primarily useful for network analysis and testing RPC protocol support
for call timeouts.

### Arguments

The single required argument is a Float amount of time to sleep in
seconds. If this value is greater than the configured RPC timeout, the
call should not return and instead raise an exception.

### Return Value

The call always returns the string "OK".

### Exceptions

Raises a `GitRPC::Timeout` when the sleep time exceeds the configured
timeout value.

### Files

Defined in [gitrpc/client/auxiliary.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/auxiliary.rb#L42)
