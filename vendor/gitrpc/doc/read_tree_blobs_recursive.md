read_tree_blobs_recursive
=========================

Fetch a summary of all trees in this Repository.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tree_blobs_recursive(tree_id)

### Description

Fetch a summary of all trees in this Repository. The result is an array of
blob hashes containing the blob name, oid, size, etc. The actual blob data is
**not** returned by this method.

### Arguments

The call takes the 40 char oid string identifying the tree object to read.

### Return Value

The call returns an array of hashes with the following structure:

    { 'oid'   => 40 char sha1 oid of the blob,
      'name'  => filename (not including the path prefix),
      'size'  => the untruncated size of the blob,
      'mode'  => the integer filemode of the blob }

### Exceptions

Raises `GitRPC::InvalidObject` when the provided oid does not resolve to a
tree object.

### Examples

Read the blob summaries for the repository

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    rpc.read_tree_blobs_recursive('deadbee...')

Read the full blob content for all blobs in the repository less than 100K in
size

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    blobs = rpc.read_tree_blobs_recursive('deadbee...')
    oids = blobs.map { |blob| blob['size'] > 100*1024 ? nil : blob['oid'] }
    oids.compact!
    rpc.read_blobs(oids)

### Files

Defined in
[gitrpc/client/read_tree_blobs.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_tree_blobs.rb#L42)
