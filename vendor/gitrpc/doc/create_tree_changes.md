create_tree_changes
===================

Create a new commit with a set of changes to an existing commit.

This signature applies to `stage_signed_tree_changes` and
`persist_signed_tree_changes`.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.create_tree_changes(parent, info, files = nil, reset = false)

### Description

Create a new commit with a set of changes to an existing commit.
The changes specified may be applied as offsets to an existing tree (e.g.,
only modify the `README`) or as a whole new tree that replaces the prior
tree entirely.

You must specify either a tree oid in the info hash to use for the new
commit, or an array of files to create a new tree with, described below.

### Arguments

- `parent`: The 40 char sha1 oid of the parent commit or nil to create
  a commit with no parents and an entirely new tree.  

- `info`: The commit information hash. See the following section for
  details on this data structure.

- `files`: Hash of `filename => data` pairs. The filename must be a string
   and may include slashes into subtrees. The data should be either a Hash or nil
   (to indicate that the file with the specified filename should be deleted).
   If data is a Hash, the following keys are acceptable:

   - `data`   - String blob content of the new tree entry
   - `mode`   - Numeric (octal) file mode, e.g. 0100644 or 0100755
   - `source` - String path of an old tree entry to remove (for file moves).
                Will also provide mode and data if not overridden.

   For simplicity, the data may also be a string with the entire blob content of
   the new tree entry.

- `reset`: Optional boolean specifying whether the newly created tree
  should be reset to empty (true) before writing entries. By
  default, tree entries specified in `files` overlay existing tree
  entries.

- `signature`: Optional String signature to include in commit.

- `&block`: An optional block that signs the commit. This block should accept a
            String argument containing the staged raw commit. The block should
            return the signature over the commit.

            Eg.
              rpc = GitRPC.new('file:/great/repo.git')
              rpc.create_tree_changes(parent, info) do |raw|
                SigningService.sign(raw)
              end

### Commit Info

The `info` argument is for specifying commit metadata. It has the
following structure:

    { 'message'   => required string commit message,
      'committer' => required committer information hash,
      'author'    => optional author information hash,
      'tree'      => optional oid of the tree you'd like to commit}

The `committer` and `author` hashes have the following members:

    { 'name'      => required full name string,
      'email'     => required email address string,
      'time'      => optional time value }

The `committer` information hash is required; the `author` is optional. If
no `author` is given the `committer` information is used in both places.

The `time` value may be either an ISO8601 string or a simple unixtime array in
the format `[unixtime, utc_offset]` where both elements are integer seconds.

### Operation

The file mode will be inherited from the previous entry when changing an
existing file. New files will be created with a file mode of 0100644.

### Return Value

The call returns the string oid of the newly created commit.

### Files

Defined in [gitrpc/client/create_tree_changes.rb](https://github.com/github/gitrpc/blob/8c8bce64/lib/gitrpc/client/create_tree_changes.rb#L41)
