config_store
============

Add or update a git config option.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.config_store('pull.rebase', true)

### Description

Add or update a git config option.

### Arguments

- `name`: The option name. The name is actually the section and the key separated by a dot.
- `value`: The option value. This must be a String, Integer, or true/value value.

### Return Value

The call returns nil, or raises a GitRPC::Failure exception if the value
is an invalid type.

### Files

Defined in
[gitrpc/client/config.rb](https://github.com/github/gitrpc/blob/1dc93a54/lib/gitrpc/client/config.rb#L19)
