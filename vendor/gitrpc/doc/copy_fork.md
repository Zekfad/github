copy_fork
=========

Copy the given fork from the given network into this network.
Must be called from a network.git repository.

There is also a `prepare_copy_fork` call that does the copy fork in two
steps, with the first step able to be preformed while the old repository
is still online and usable.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.prepare_copy_fork("github-fs102-cp1-prd.iad.github.net:/data/repositories/7/nw/74/5d/31/765432/network.git", 1234)
    rpc.copy_fork("github-fs102-cp1-prd.iad.github.net:/data/repositories/7/nw/74/5d/31/765432/network.git", 1234)
    

### See also

These calls are direct calls into `git copy-fork`. You can see more
documentation by running `git copy-fork --help` or looking at the
[code](https://github.com/github/github/blob/master/git-bin/git-copy-fork).
