remove
======

Remove repository from disk.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.remove()

### Description

Remove repository from disk.

### Return Value

The init call always returns nil.

### Files

Defined in [gitrpc/client/disk.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/disk.rb#L37)
