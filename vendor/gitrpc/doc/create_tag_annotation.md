create_tag_annotation
=====================

Create an annotated tag object in the repository. 

### Synopsis

    => rpc = GitRPC.new('file:/great/repo.git')
    => rpc.create_tag_annotation(tag_name='v1.0.0',
                                 target_oid,
                                 {
                                   :message => "Release v1.0.0",
                                   :tagger  => {
                                     :name  => "Andy Delcambre",
                                     :email => "andy@example.com",
                                     :time  => "2015-01-13T14:18:16-07:00"
                                   }
                                 })

### Description

Create an annotated tag in the git repository using the given `name`,
pointing at the `target_oid`. You also specify the message for the
annotated tag object, and the tagger object. 

The object is created in the git repository, and the oid of the object
is returned. 

If the `target_oid` doesn't exist in the database, an error is raised. 

### Arguments

`tag_name` => The name of the tag to create.
`target_oid` => The oid of the object the tag should point to. 
`annotation` => A hash of options for the tag:

    {
      :message => The message for the tag object
      :tagger => The user who is creating the tag:
        {
          :name => Full name of the user
          :email => Email of the user
          :time => ISO-8601 timestamp
        }
    }

### Return value

A 40 char oid representing the tag object.

### Files

Defined in [`lib/gitrpc/client/create_tag_annotation.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/create_tag_annotation.rb)
