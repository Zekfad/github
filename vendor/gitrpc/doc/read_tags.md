read_tags
=========

Retrieve tag information for a list of tag oids.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tags(oids)

### Description

Retrieve tag information for a list of tag oids.

This method is optimized for loading multiple tag objects using a single cache
lookup followed possibly by a single RPC call for tags that aren't in cache.

### Arguments

The call takes a single array of 40 char oid strings identifying tags
to load. Only full sha1 object ids are allowed. All oids must identify
tag objects.

Note that normal tags don't have objects - they simply point to a commit.
The oids specified in the list must only identify real tag objects.

### Return Value

The call returns an array guaranteed to have the same number of elements
as the list of oids given as input where the elements of the resulting
array match up with the `oids` provided.

Each element of the array is a hash with the following members:

    { 'type'        => 'tag',
      'oid'         => string sha1 of the tag,
      'name'        => string tag name,
      'message'     => string tag message,
      'signature'   => string PGP signature data,
      'tagger'      => [name, email, time],
      'target'      => string destination object id,
      'target_type' => string object type of target }

The `target` almost always points to a commit oid although tagging other
types of objects is possible.

The `message` and `tagger` attributes may be nil if that information was not
specified in the tag.

### Exceptions

Raises `GitRPC::ObjectMissing` when an object specified in `oids`
does not exist or can not be loaded from the object store.

Raises `GitRPC::InvalidObject` when the object is found but is not a
tag object.

### Files

Defined in [gitrpc/client/read_objects.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/read_objects.rb#L238)
