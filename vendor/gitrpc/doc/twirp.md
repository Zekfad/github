# GitRPC over Twirp

We are moving calls from BERT-RPC to Twirp in order to use a combination of HTTP, protobuf and JSON and thus use standard technologies. This lets us specify the services, calls and messages using a widely understood format.

We communicate with the server over the same port and the ernicorns recognise whether the input is HTTP or BERT-RPC. Aside from the standard Twirp request, we also recognise two extra header fields which specify how the backend is to behave.

The first is the `X-Git-Path` header field, which specifies the repository or network path we're to operate on. The second is the `X-GitRPC-Options` header field, which is a JSON-encoded dictionary specifying the backend options.

 - timeout    - Number of seconds that this request can take
 - max        - Total number of bytes that native commands can generate.
 - env        - Hash of environment variables to set for native commands.
 - alternates - Array of alternate object directory paths.

These are all the same options which are available over BERT-RPC but in a way
that we can push them over HTTP.

We set up Rack middleware to extract this information and set up the `Backend` instance that the service handlers can access. The middleware also takes care of reporting to gitmon and making exception data useful. The service handlers receive an `env` as input, into which we add a `:backend` key whose value is the `Backend` instance appropriate for this call.

If a call raises an exception, we convert it such that we can send it over to the client similarly to how we do it under BERT-RPC. The returned error has a code of `"internal"` and we add into its `meta` field two keys.

 - message -   The exception message
 - backtrace - The backtrace as a single string (the values have to be scalar)

Everything else is handled by the Twirp gem (or package in whatever language).
