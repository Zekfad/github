# rubocop:disable Style/FrozenStringLiteralComment
# GitRPC is a network-oriented library for accessing remote git repositories
# efficiently.
#
# Options
# -------
#
# GitRPC.new takes an options hash that can be used to control the default
# environment. No options are required.
#
# :cache => connection (Dalli::Client)
# The memcache connection used to store and retrieve git objects. A global
# default may also be set using GitRPC.cache=.
#
# :env => A hash of environment variables (string)
# The environ hash is enabled for native command invocations only.
#
# :max => maximum amount of output to generate in bytes (int)
# Native commands that generate more than this amount of output are halted and
# a GitRPC::MaximumOutputExceeded is raised. By default, there is no limit on
# output size.
#
# :timeout => seconds (int)
# Native command operations are halted after this time limit and a
# GitRPC::Timeout exception is raised. By default, there is no timeout.
#
# :connect_timeout => seconds (int)
# Connection attempts from client to backend are halted after this time
# limit and a GitRPC::ConnectionError exception is raised. By default, there
# is no timeout.
#
# :alternates => [path, ...] (array)
# List of alternative object directories. This is sets the
# GIT_ALTERNATE_OBJECT_DIRECTORIES environment variable for native command
# invocations and also configures Rugged with alternate object directories.
# The value must be an array of path strings. These can be absolute paths or
# paths relative to the git repository directory. For instance, setting this
# value to ["../repo-2.git/objects"] will expand the path relative to
# repo-1.git.
#
# Usage
# -----
#
# Get this party started:
#
#     >> require 'gitrpc'
#     >> git = GitRPC.new('/path/to/repository.git')
#     #<GitRPC::Client:0x10c... @backend=...>
#
# Retrieve all refs for the repository in a single hash (cached: 1 netop)
#
#     >> git.refs
#     { 'refs/heads/master' => 'deadbeee...', 'refs/head/some-branch' => ... }
#
# Resolve a complex revision down to a sha1 object id (not cached)
#
#     >> git.rev_parse('master@{2.days.ago}')
#     'deadbeee...'
#
# Read commit meta-data in batch and nothing else (cached: 1 netop)
#
#     >> git.read_commits(['deadbeee...', 'badfffff...'])
#     [{"type"=>"commit",
#       "oid"=>"d3224d5...",
#       "parents"=>["fd1545cc..."],
#       "tree"=>"6d1470d...",
#       "author"=>["Ryan Tomayko", "ryan@github.com", "2012-06-13T03:29:30Z"],
#       "committer"=>["Ryan Tomayko", "ryan@github.com", "2012-06-13T03:29:30Z"],
#       "message"=>":encoding docs",
#       "encoding"=>"UTF-8"},
#      {"type"=>"commit",
#       "oid"=>"abcdef0...",
#       ...}
#
# Read commits for all refs (cached: 2 netop)
#
#     >> git.read_commits(git.refs.values)
#     [{"type"=>"commit",...]
#
require "time"
require "gitrpc/util"
require "gitrpc/util/bert_ext"
require "gitrpc/error"
require "gitrpc/failure"

require "gitrpc/backend"
require "gitrpc/client"
require "gitrpc/diff"
require "gitrpc/encoding"
require "gitrpc/gitmon_client"
require "gitrpc/middleware"
require "gitrpc/protocol"
require "gitrpc/send_multiple"
require "gitrpc/timer"

require "gitrpc/twirp"
require_relative "../proto/spokes.backend_twirp.rb"

module GitRPC
  NULL_OID  = "0" * 40
  NULL_MODE = "0" * 6
  EMPTY_TREE_OID = "4b825dc642cb6eb9a060e54bf8d69288fbee4904".freeze
  NULL_CHAR = "\x00"

  # Create a new GitRPC::Client object for the given git repository URL.
  #
  # url     - String or URI identifying the git repository's location.
  # options - General options. These are passed all the way down to the backend.
  #           See the OPTIONS section above for available options. Options
  #           should have symbol keys.
  #
  # Returns a GitRPC::Client object in all its glory.
  def self.new(url, options = {})
    cache = options.delete(:cache) || self.cache
    backend = GitRPC::Protocol.resolve(url, options)
    GitRPC::Client.new(backend, cache)
  end

  # Make a one-off RPC call to multiple endpoints at the same time.
  #
  # urls          - any set of URLs. BERTRPC URLs will be accessed
  #                 in parallel, all others will be accessed serially.
  # message, args - The call to send to the remote.
  # options       - General options. These are passed to all the
  #                 backends. See the OPTIONS section above.
  #
  # Returns two hashes, one with answers and one with errors,
  # keyed by URL.
  def self.send_multiple(urls, message, args, options = {})
    GitRPC::SendMultiple.send_multiple(urls, message, args, options)
  end

  # Set the memcache connection used by new client connections for storing and
  # retrieving cached repository data. This is typically configured by the calling
  # environment during boot. When no cache is configured, a simple hash-backed
  # cache is used.
  #
  # conn - A Dalli::Client or compatible object. The object must respond to the
  #        #get, #set, #get_multi, and #delete methods and must support raw
  #        value storage.
  #
  # It's highly recommended that a configured memcache connection be available
  # in production environments.
  def self.cache=(conn)
    @cache = conn
  end

  # The Cache object used to store and retrieve values in memcache.
  def self.cache
    @cache ||= GitRPC::Util::HashCache.new
  end

  class << self
    # Global default timeout for all GitRPC connections. This is the same as
    # passing the :timeout option to GitRPC.new
    #
    # This will be overridden if you pass a timeout option to GitRPC.new as well.
    #
    # timeout - seconds (int)
    attr_accessor :timeout

    # Object returned by GitRPC::Backend#host_metadata
    attr_accessor :host_metadata

    # Extra env vars to pass to all native spawns
    attr_accessor :extra_native_env
  end

  # Run a block with the GitRPC timeout temporarily set to the
  # specified value rather than the default (which at the time of this
  # writing is 9 seconds for production and 30 minutes for resque
  # jobs). timeout should be either an integer or float number of
  # seconds, or a Duration like `5.minutes`.
  def self.with_timeout(timeout)
    original_timeout = @timeout
    @timeout = timeout.to_f
    yield
  ensure
    @timeout = original_timeout
  end

  # Global default connect timeout for all GitRPC connections. This is the
  # same as passing the :connect_timeout option to GitRPC.new
  #
  # This will be overridden if you pass a connect_timeout option to
  # GitRPC.new as well.
  #
  # connect_timeout - seconds (int)
  class << self
    attr_accessor :connect_timeout
  end

  # Enable gitmon tracking of backend calls.
  class << self
    attr_writer :gitmon_enabled
  end

  # Repository template directory, used to symlink the hooks directory
  class << self
    attr_accessor :hooks_template
  end

  class << self
    attr_accessor :repository_root
  end

  def self.gitmon_enabled?
    @gitmon_enabled
  end

  class << self
    attr_writer :local_git_host_name
  end

  def self.local_git_host_name
    @local_git_host_name || "localhost"
  end

  class << self
    attr_writer :optimize_local_access
  end

  def self.optimize_local_access
    @optimize_local_access || false
  end

  # Instrumentation
  # ---------------
  # Various GitRPC components publish instrumentation stats via the instrument
  # method. The instrumenter object may be configured explicitly if needed. By
  # default, ActiveSupport::Notifications is used.
  #
  # Notify for an operation. This is may be called by any GitRPC component to
  # generate an event.
  # Args match ActiveSupport::Notifications.publish API of (name, started, ended, id, payload)
  def self.publish(name, *args)
    @instrumenter.publish("#{name}.gitrpc", *args) unless @instrumenter.nil?
  end

  # Instrument an operation. This is may be called by any GitRPC component to
  # record an instrumentation event.
  #
  # event   - Name of the event as a symbol. The event is published under the
  #           gitrpc namespace automatically.
  # payload - Hash payload for the event. Available keys are based on the event
  #           being published.
  #
  # Returns the result of executing the block.
  def self.instrument(event, payload = {}, &block)
    if @instrumenter
      block ||= proc {}
      @instrumenter.instrument("#{event}.gitrpc", payload, &block)
    elsif block
      block.call(payload)
    end
  end

  # The object that receives instrument messages. This is AS::Notifications by
  # default but may be set to any object that responds to #instrument.
  #
  # Returns an object that responds to #instrument or nil if no instrumentation
  # backend is configured.
  def self.instrumenter
    @instrumenter
  end

  # Configure the instrumentation backend that will receive events. The object
  # must respond to #instrument.
  def self.instrumenter=(object)
    @instrumenter = object
  end

  # If AS::Notifications is available, use it as the instrumentation backend by
  # default. Set GitRPC.instrumenter = nil explicitly to disable this.
  if defined?(ActiveSupport::Notifications)
    @instrumenter = ActiveSupport::Notifications
  end

  # Returns a rack app which implements the twirp protocol
  def self.rack_app
    server_svc = ::Spokes::Backend::HealthService.new(HealthHandler.new)
    GitRPC::Twirp::Middleware.setup_for(server_svc, require_repo: false)


    git_svc = ::Spokes::Backend::GitService.new(GitHandler.new)
    GitRPC::Twirp::Middleware.setup_for(git_svc)

    Rack::Builder.new do
      use GitRPC::Twirp::Middleware::BackendMiddleware
      use GitRPC::Twirp::Middleware::GitmonMiddleware
      use GitRPC::Twirp::Middleware::TimeoutMiddleware
      run Rack::Cascade.new([server_svc, git_svc])
    end
  end
end
