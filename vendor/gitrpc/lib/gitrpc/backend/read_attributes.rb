# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    ATTRIBUTE_LOAD_FLAGS = Rugged::Repository::Attributes.parse_opts(
                             :skip_system => true,
                             :priority => [:index]
                           )
    GITATTRIBUTES        = ".gitattributes".freeze

    rpc_reader :read_attributes

    def read_attributes(tree_oid, paths, keys = nil)
      ensure_valid_full_sha1(tree_oid)

      paths = paths.compact

      tree = Rugged::Tree.lookup(rugged, tree_oid)

      gitattributes_index = build_gitattributes_index(tree, paths)
      read_attributes_from_index(gitattributes_index, paths, keys)
    rescue Rugged::OdbError
      raise GitRPC::InvalidObject, "Invalid tree oid #{tree_oid}"
    end

    private
    def read_attributes_from_index(index, paths, keys)
      original_index, rugged.index = rugged.index, index

      paths.map do |path|
        # We do not validate the existance of paths in the tree here
        # because it would be too expensive. #fetch_attributes will
        # just return an empty hash for unknown paths.
        rugged.fetch_attributes(path, keys, ATTRIBUTE_LOAD_FLAGS)
      end
    ensure
      rugged.index = original_index
    end

    def build_gitattributes_index(tree, paths)
      gitattributes_index = Rugged::Index.new
      read_path_from_tree_into_index(GITATTRIBUTES, tree, gitattributes_index)

      visited_paths = Set.new
      paths.each do |path|
        while index = path.rindex("/")
          path = path[0, index]

          unless visited_paths.include?(path)
            visited_paths << path
            read_path_from_tree_into_index("#{path}/#{GITATTRIBUTES}", tree, gitattributes_index)
          end
        end
      end

      gitattributes_index
    end

    def read_path_from_tree_into_index(path, tree, index)
      entry = begin
        tree.path(path)
      rescue Rugged::TreeError
        # Do nothing if the path does not exist in the tree
      end

      return unless entry && entry[:type] == :blob

      index << {
        path: path,
        oid: entry[:oid],
        mode: entry[:filemode],
      }
    end
  end
end
