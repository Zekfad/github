# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Public: Find as good common ancestors as possible for a merge
    #
    # base_ref - A ref name or commit oid used as the base of comparison
    # head_ref - A ref name or commit oid used as the head for comparison
    #
    # Returns the merge base commit oid as a String
    rpc_reader :merge_base
    def merge_base(base_ref, head_ref)
      deref_base = dereference_ref(base_ref)
      deref_head = dereference_ref(head_ref)

      rugged.merge_base(deref_base, deref_head)
    end

    # Public: Find as good common ancestors as possible for a merge
    #
    # base_commit_oid - A commit oid used as the base for comparison
    # head_commit_oid - A commit oid used as the head for comparison
    #
    # Returns the merge base commit oid as a String
    rpc_reader :best_merge_base
    def best_merge_base(base_commit_oid, head_commit_oid)
      ensure_valid_full_sha1(base_commit_oid)
      ensure_valid_full_sha1(head_commit_oid)

      merge_bases = rugged.merge_bases(base_commit_oid, head_commit_oid)

      case merge_bases.length
      when 0
        nil
      when 1
        merge_bases.first
      else
        # The best merge base is the merge base that is behind
        # the head_commit_oid by the fewest amount of commits.
        #
        # Ties are broken by sorting on the number of commits and
        # the SHA-1 value of the merge base. This is quite arbitrary,
        # but gives us a stable approach to resolve ties.
        merge_bases.min_by do |merge_base_oid|
          _, behind = rugged.ahead_behind(merge_base_oid, head_commit_oid)

          [behind, merge_base_oid]
        end
      end
    rescue Rugged::OdbError => e
      message = case e.message
      when /object not found - no match for id \((\w{40})\)/
        "fatal: Not a valid commit name #{$1}\n"
      else
        e.message
      end

      raise GitRPC::InvalidObject.new(message)
    end

    # Public: Find as good common ancestors as possible for a merge
    #
    # The git-merge-base program 1.) writes a single SHA1 to stdout when a
    # common ancestor is located, 2.) exits 1 when no common ancestor exists,
    # 3.) exits 128 because either the base or head SHA1 do not exist, 4.) exits 128 or
    # something else when some kind of repository or system failure occurs.
    #
    # base_ref - A ref name or commit oid used as the base of comparison
    # head_ref - A ref name or commit oid used as the head for comparison
    # git_command - "merge-base" or "best-merge-base"
    # timeout - Maximum number of seconds that the operation can take to complete
    #
    # Returns the merge base commit oid as a String
    rpc_reader :native_merge_base
    def native_merge_base(base_ref, head_ref, git_command = "merge-base", timeout = nil)
      raise ArgumentError if !["merge-base", "best-merge-base"].include?(git_command)

      ensure_valid_commitish(base_ref)
      ensure_valid_commitish(head_ref)

      res = spawn_git(git_command, [base_ref, head_ref], nil, {}, nil, timeout)

      if res["ok"]
        res["out"].rstrip
      elsif res["status"] == 1
        nil
      elsif res["err"] =~ /not a valid (object|commit) name/i
        raise GitRPC::InvalidObject, res["err"]
      else
        raise GitRPC::Error, res["err"]
      end
    end

    def dereference_ref(ref)
      obj = begin
        rugged.rev_parse(ref)
      rescue Rugged::ReferenceError => e
        raise GitRPC::InvalidObject, "fatal: Not a valid commit name #{ref}\n"
      end

      case obj
      when Rugged::Commit
        obj.oid
      when Rugged::Tag::Annotation
        obj.target_id
      else
        raise GitRPC::InvalidObject, "\"#{ref}\" points to a #{obj.class} not a commit, tag or reference"
      end
    end

    rpc_reader :branch_base
    def branch_base(head_oid, base_oid, root_oid)
      ensure_valid_full_sha1(head_oid)
      ensure_valid_full_sha1(base_oid)
      ensure_valid_full_sha1(root_oid)

      res = spawn_git("branch-base", [head_oid, base_oid, root_oid])
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      res["out"].strip
    end
  end
end
