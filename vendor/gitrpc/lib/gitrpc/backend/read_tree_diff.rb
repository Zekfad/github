# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_reader :read_tree_diff
    def read_tree_diff(commit1_oid, commit2_oid = nil, paths: [], diff_trees: false)
      if (commit1_oid.nil? || commit1_oid == NULL_OID) &&
          (commit2_oid.nil? || commit2_oid == NULL_OID)
        return []
      end

      unless commit1_oid == NULL_OID
        commit1 = rugged.lookup(commit1_oid)
        unless commit1.is_a?(Rugged::Commit) || (diff_trees && commit1.is_a?(Rugged::Tree))
          raise GitRPC::InvalidObject, "Invalid commit oid #{commit1_oid}"
        end
      end

      if commit2_oid.nil?
        # this is how the old native_read_diff implementation worked
        commit2 = commit1
        commit1 = commit1.parents[0]
      elsif commit2_oid != NULL_OID
        commit2 = rugged.lookup(commit2_oid)
        unless commit2.is_a?(Rugged::Commit) || (diff_trees && commit2.is_a?(Rugged::Tree))
          raise GitRPC::InvalidObject, "Invalid commit oid #{commit2_oid}"
        end
      end

      diff = rugged.diff(commit1, commit2, paths: paths)
      deltas = []
      diff.each_delta do |delta|
        old_file = delta.old_file || {}
        new_file = delta.new_file || {}

        deltas << {
          "status" => delta.status.to_s,
          "old_file" => {
            "oid" => old_file[:oid],
            "path" => old_file[:path],
            "mode" => old_file[:mode],
          },
          "new_file" => {
            "oid" => new_file[:oid],
            "path" => new_file[:path],
            "mode" => new_file[:mode],
          }
        }
      end

      deltas
    end
  end
end
