# frozen_string_literal: true

require_relative "read_adjusted_positions/positioning_data"

module GitRPC
  class Backend
    # Returns a map of blob lookup data to maps of position mappings. See
    # client documentation and reducer output for details.
    #
    # NOTE: This endpoint is used for both read_adjusted_positions and
    # read_commit_adjusted_positions
    rpc_reader :read_adjusted_positions
    def read_adjusted_positions(positioning_data, options = {})
      skip_bad = !!options["skip_bad"]

      positioning_data.each_with_object({}) do |row, oid_mappings|
        data = ReadAdjustedPositions::PositioningData.new(*row, skip_bad: skip_bad, backend: self)
        oid_mappings[data.blob_lookup_data] = data.adjusted_positions_map
      end
    end
  end
end
