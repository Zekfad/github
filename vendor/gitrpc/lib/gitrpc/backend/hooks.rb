# frozen_string_literal: true

module GitRPC
  class Backend
    rpc_writer :post_mirror_hook
    def post_mirror_hook(updated_refs, committer_date: nil, committer_email: nil, mirror_pusher: nil)
      env = {"GIT_COMMITTER_NAME" => "mirrors"}
      env["GIT_COMMITTER_EMAIL"] = committer_email if committer_email
      env["GIT_COMMITTER_DATE"] = committer_date if committer_date
      env["GIT_PUSHER"] = mirror_pusher if mirror_pusher

      res = spawn(["hooks/post-mirror"], updated_refs.join, env)
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      nil
    end

    rpc_reader :pre_receive_hook
    def pre_receive_hook(refline, sockstat_env = {})
      env = { "CUSTOM_HOOKS_ONLY" => "1"}
      env.merge!(sockstat_env.select { |k, v| k.to_s.start_with?("GIT_SOCKSTAT_VAR_") })

      res = spawn(["hooks/pre-receive"], refline, env)
      {
        "ok" => res["ok"],
        "status" => res["status"],
        "out" => res["out"].force_encoding("UTF-8").scrub,
        "err" => res["err"].force_encoding("UTF-8").scrub
      }
    end
  end
end
