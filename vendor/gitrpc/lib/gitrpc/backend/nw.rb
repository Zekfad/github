# frozen_string_literal: true

module GitRPC
  class Backend
    GIT_NW_REPACK_TRACE2 = {
      "GIT_TRACE2_GRAPHITE" => "af_unix:dgram:/var/run/datadog/dsd.socket",
      "GIT_TRACE2_GRAPHITE_CATEGORIES" => [
        "commit-graph",
        "delta-islands",
        "pack-bitmap",
        "pack-bitmap-write",
        "pack-objects",
      ].join(","),
    }

    rpc_writer :nw_rm
    def nw_rm
      res = spawn_git("nw-rm", "--force")
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      res["ok"]
    end

    rpc_reader :nw_linked?
    def nw_linked?
      res = spawn_git("nw-linked")
      !!res["ok"]
    end

    rpc_writer :nw_sync
    def nw_sync
      res = spawn_git("nw-sync")
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      nil
    end

    rpc_writer :nw_gc, output_varies: true, no_git_repo: true
    def nw_gc(log: true, root_network: nil, window_byte_limit: nil,
              changed_path_bloom_filters: false)
      argv = []
      argv << "--log" if log
      # TODO: reintroduce the `--root-network` argument.  This
      # currently tickles an edge case (at least) in the
      # `chromium/chromium` repository, where passing this makes
      # building bitmaps take 3+ hours, and thus the network is
      # failing maintenance.  See also
      # https://github.com/github/git/issues/1026
      argv << "--window-byte-limit=#{window_byte_limit}" if window_byte_limit
      argv << "--changed-paths" if changed_path_bloom_filters

      spawn_git("nw-gc", argv)
    end

    rpc_writer :nw_link
    def nw_link
      res = spawn_git("nw-link")
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      nil
    end

    rpc_writer :nw_unlink
    def nw_unlink
      res = spawn_git("nw-unlink")
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      nil
    end

    rpc_writer :nw_repack
    def nw_repack(window_byte_limit: nil, changed_path_bloom_filters: false)
      argv, env = [], {}
      argv << "--log"
      argv << "--window-byte-limit=#{window_byte_limit}" if window_byte_limit
      if trace2_enabled?(:nw_repack)
        env = GIT_NW_REPACK_TRACE2
      end
      argv << "--changed-paths" if changed_path_bloom_filters
      res = spawn_git("nw-repack", argv, nil, env)
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      nil
    end

    rpc_writer :nw_fsck
    def nw_fsck(trust_synced: false)
      argv = []
      argv << "--connectivity-only"
      argv << "--trust-synced" if trust_synced
      spawn_git("nw-fsck", argv)
    end

    rpc_writer :last_fsck, output_varies: true
    def last_fsck(never: false, force: false)
      argv = []
      argv << "--never" if never
      argv << "--force" if force
      res = spawn_git("last-fsck", argv)

      # The caller only cares about the text, not the exit statuses,
      # and we don't want to raise an exception if the backend exit
      # statuses disagree (e.g. because some backends have never run
      # fsck on this repo before.)
      res["status"] = 0

      res
    end

    rpc_writer :janitor, output_varies: true
    def janitor(fix: false, type: nil, ignore_hooks: false)
      argv = []
      argv << "--fix" if fix
      if type
        raise GitRPC::Error, "invalid type #{type}" unless ["network", "fork", "wiki", "gist"].include?(type)
        argv << "--type=#{type}"
      end
      argv << "--ignore-hooks" if ignore_hooks

      spawn_git("janitor", argv)
    end
  end
end
