# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_reader :ahead_behind
    def ahead_behind(base_ref, others, timeout = nil)
      ensure_valid_commitish(base_ref)

      ret = spawn_git("ahead-behind", ["--stdin", "--base", base_ref], others.join("\n"), {}, nil, timeout)
      ahead_behinds = {}

      if ret["ok"]
        ret["out"].each_line do |line|
          ref, ahead, behind = line.split(" ")
          ahead_behinds[ref] = [ahead.to_i, behind.to_i]
        end
      end

      ahead_behinds
    end
  end
end
