# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_reader :tree_file_list
    def tree_file_list(oid, list_directories, list_submodules, skip_directories)
      tree = case object = rugged.lookup(oid)
      when Rugged::Commit
        object.tree
      when Rugged::Tree
        object
      else
        raise(GitRPC::InvalidObject, "Invalid object type, expected tree but was #{object.type}")
      end

      file_list = []

      tree.walk(:preorder) do |root, entry|
        name = entry[:name]
        name = File.join(root, name) unless root.empty?

        case entry[:type]
        when :tree
          next false if skip_directories.include?(entry[:name])
          file_list << name if list_directories

        when :commit
          file_list << name if list_directories && list_submodules

        else
          file_list << name
        end

        true
      end

      file_list
    rescue Rugged::TreeError, Rugged::OdbError => boom
      raise GitRPC::ObjectMissing.new(boom, oid)
    end
  end
end
