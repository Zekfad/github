# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_reader :merge_single_file
    def merge_single_file(ancestor, ours, theirs, options = {})
      options[:our_label] ||= "ours"
      options[:their_label] ||= "theirs"
      Rugged::Blob.merge_files(rugged, ancestor, ours, theirs, options)
    end
  end
end
