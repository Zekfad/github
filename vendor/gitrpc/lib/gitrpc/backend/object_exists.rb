# frozen_string_literal: true

module GitRPC
  class Backend
    rpc_reader :object_exists?
    def object_exists?(oid, type = nil)
      if type
        rugged.read_header(oid)[:type].to_s == type
      else
        rugged.exists?(oid)
      end
    rescue Rugged::OdbError
      false
    end

    rpc_reader :batch_check_objects_exist
    def batch_check_objects_exist(oids)
      oids.inject({}) { |h, oid| h[oid] = object_exists?(oid); h }
    end
  end
end
