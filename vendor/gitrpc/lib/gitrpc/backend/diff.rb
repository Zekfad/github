# frozen_string_literal: true

module GitRPC
  class Backend
    rpc_reader :file_changed?
    def file_changed?(file, before, after)
      ensure_valid_commitish(before)
      ensure_valid_commitish(after)

      res = if before == NULL_OID
        spawn_git("show", ["--name-only", "--pretty=format:", after])
      else
        spawn_git("diff", ["--name-only", before, after])
      end

      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      res["out"].split("\n").include?(file)
    end

    rpc_reader :raw_diff
    def raw_diff(before, after)
      ensure_valid_commitish(before) if before
      ensure_valid_commitish(after) if after

      res = spawn_git("diff", ["--no-ext-diff", "--raw", "--no-abbrev", "#{before}..#{after}"])
      return [] if !res["ok"]
      res["out"].split("\n")
    end

    rpc_reader :diff_shortstat
    def diff_shortstat(range)
      if range.start_with?("-")
        raise GitRPC::InvalidOid, "unacceptable range: '#{range}'"
      end

      res = spawn_git("diff", ["--shortstat", "--no-ext-diff", range, "--"])
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      res["out"].strip
    end

    rpc_reader :diff_tree
    def diff_tree(before, after, include_tree_entry: false, recurse: false)
      ensure_valid_commitish(before)
      ensure_valid_commitish(after)

      argv = ["-z"]
      argv << "-t" if include_tree_entry
      argv << "-r" if recurse
      argv += [before, after]

      res = spawn_git("diff-tree", argv)
      if !res["ok"]
        raise GitRPC::ObjectMissing.new(res["err"], $1) if res["err"] =~ /fatal: bad object ([0-9a-f]{40})/
        raise GitRPC::CommandFailed.new(res)
      end
      res["out"].split("\0").each_slice(2).to_a
    end
  end
end
