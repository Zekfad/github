# frozen_string_literal: true

module GitRPC
  class Backend
    # Public: Scan a repository for credentials using the find-creds script.
    #
    # refs - An optional Array of refs that changed: ([[before, after,
    # ref1_name], ...])
    #
    # If no ref changes are passed, the full repository history will be scanned.
    # Otherwise, we process each ref change to build out a set of OIDs to
    # include/exclude from the scan. We include all new tips. But, to avoid
    # scanning objects that have already been scanned, we need exclude the tips
    # from any refs that have not changed. For example, imagine the following
    # tree:
    #
    # A---B---C---D---E---F---G       ← master == origin/master
    #              \
    #               H---I             ← newbranch
    #
    #
    # A fresh push of newbranch to GitHub would contain a ref change list of
    # [[NULL_OID, OID_OF_I, "refs/heads/newbranch"]]. If we naively passed just
    # the "before" and "after" OIDs to `find-creds` it would search the entire
    # commit history from `I` back to `A`. But, `A`-`G` have already been
    # scanned. So, we exclude scanning `A`-`D` by excluding scanning of any tips
    # of unchanged refs.
    #
    # Returns an Array of credentials found (["CRED_TYPE TOKEN OID REPORT_URL", ...])
    rpc_reader :find_creds
    def find_creds(refs = [], options = {})
      raise ArgumentError, "max_result must be set" unless options.key?(:max_result)

      # If both before and after are nil, we do a full repository scan
      if refs.empty?
        args = ["--all"]
        args << "--max-result"
        args << "#{options[:max_result]}"
        args << "--buffer-results" if options[:buffer_results]
        args << "--dedup-results" if options[:dedup_results]
        args << "--skip-fp" if options[:skip_fp]
        res = spawn_git("find-creds", args)
      else
        # Assume all refs are unchanged to start
        unchanged_refs = read_refs
        include_oids = []
        exclude_oids = []
        refs.each do |before, after, ref|
          # Remove any refs that we know have changed
          unchanged_refs.delete(ref)
          include_oids << after unless after == GitRPC::NULL_OID
          exclude_oids << before unless before == GitRPC::NULL_OID
        end
        exclude_oids.concat(unchanged_refs.values)
        args = ["--stdin"]
        args << "--max-result"
        args << "#{options[:max_result]}"
        args << "--buffer-results" if options[:buffer_results]
        args << "--dedup-results" if options[:dedup_results]
        args << "--skip-fp" if options[:skip_fp]
        res = spawn_git(
          "find-creds",
          args,
          include_oids.join("\n") + "\n" + exclude_oids.map { |e| "^#{e}" }.join("\n")
        )
      end
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      res["out"].split("\n")
    end
  end
end
