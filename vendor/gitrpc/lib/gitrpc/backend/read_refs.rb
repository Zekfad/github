# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    include GitRPC::Util

    # Pattern used to filter refs to only local branches, tags, and notes.
    REFS_FILTER = %r{^refs/(?:heads|guest|tags|notes)/}
    HIDDEN_REFS_FILTER = %r{^refs/__gh__($|/)}

    # Public: Retrieve refs mappings for repository.
    #
    # filter - String specifying which set of refs should be returned. "default"
    #          means that only heads and tags will be returned. "extended" returns
    #          refs that would show up in `git ls-remote`. "all" returns all of the
    #          refs on the fileserver, including some that are only intended to be
    #          used inside of this app.
    #
    # Returns a Hash of { ref => oid } mappings.
    rpc_reader :read_refs
    def read_refs(filter = "default")
      predicate = {
        "default"  => lambda { |name| name =~ REFS_FILTER },
        "extended" => lambda { |name| name !~ HIDDEN_REFS_FILTER },
        "all"      => lambda { |name| true },
      }.fetch(update_boolean_read_refs_filter(filter))
      refs = {}
      rugged.refs.each do |ref|
        # PERF if refs are guaranteed to be sorted we can break out after the
        # last tag ref instead of comparing all keys.
        name = ref.name.b
        if predicate.call(name)
          refs[name] = ref.target_id
        end
      end
      refs
    end

    # Public: Retrieve target OIDs for a given set of ref names
    #
    # names - Array of String
    #
    # Returns Array of Strings as target OIDs
    rpc_reader :read_qualified_refs
    def read_qualified_refs(qualified_names)
      qualified_names.map do |qualified_name|
        begin
          reference = rugged.references[qualified_name]
          next nil unless reference

          reference.resolve.target_id
        rescue Rugged::ReferenceError
          nil
        end
      end
    end

    # Public: How many branches and tags does this repository have?
    #
    # Returns Hash
    rpc_reader :ref_counts
    def ref_counts
      result = { branches: 0, references: 0, tags: 0 }

      rugged.references.each_name do |ref_name|
        result[:references] += 1

        if ref_name.start_with?("refs/heads/")
          result[:branches] += 1
        elsif ref_name.start_with?("refs/tags/")
          result[:tags] += 1
        end
      end

      result
    end

    # Public: Fetch the oid of the HEAD ref
    #
    # Returns a sha1
    rpc_reader :read_head_oid
    def read_head_oid
      rugged.head.target_id
    end

    # Public: list references
    #
    # This is basically a subset of read_refs (it returns only ref
    # names, not oids), but it doesn't filter out remote and hidden
    # branches like read_refs does.
    #
    # Returns an array of strings.
    rpc_reader :list_refs
    def list_refs
      res = spawn_git("for-each-ref", "--format=%(refname)")
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      res["out"].split("\n")
    end

    # Public: list branch names and dates the way BranchFinder likes them
    #
    # Returns an array of strings.
    rpc_reader :raw_branch_names_and_dates
    def raw_branch_names_and_dates
      res = spawn_git("for-each-ref",
                      ["--format=%(committerdate:rfc2822)|%(refname)",
                      "--sort=refname:short",
                      "--sort=committerdate",
                      "refs/heads/"])
      raise GitRPC::CommandFailed.new(res) if !res["ok"]
      res["out"].split("\n").map { |x| x.split("|", 2) }
    end
  end
end
