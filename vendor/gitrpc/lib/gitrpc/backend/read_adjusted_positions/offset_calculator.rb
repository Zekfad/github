# frozen_string_literal: true

module GitRPC
  class Backend
    module ReadAdjustedPositions
      class OffsetCalculator

        class << self
          # Returns the adjusted linenos based on the changes contained in the given patch.
          #
          # patch   - an instance of Rugged::Patch describing the changes to apply to the linenos
          # linenos - an ordered list of linenos in the a blob
          #
          # Returns an ordered list of line nos in the b blob corresponding to those provided
          #   for the a blob.
          def adjusted_linenos_for_patch(patch, linenos)
            hunks_with_offset = to_enum(:each_hunk_with_offset, patch)
            current_hunk, offset = hunks_with_offset.next

            linenos.map do |current_lineno|
              # If there are no more hunks in this patch, we can simply
              # shift all remaining line numbers by the current offset
              if current_hunk.nil?
                current_lineno + offset

              # If the current line number is before the hunk, we can
              # simply shift it with the current offset
              elsif before_hunk?(current_hunk, current_lineno)
                current_lineno + offset

              # If a line number falls inside a hunk, it got deleted
              elsif in_hunk?(current_hunk, current_lineno)
                nil

              # If a line falls at the end of a hunk, we take the next hunk
              # and offset and redo the current line
              else
                current_hunk, offset = hunks_with_offset.next

                redo
              end
            end
          end

          private

          # Takes a Rugged::Patch and yields each hunk plus
          # the offset that line numbers between the current hunk and the
          # previous hunk got shifted by the patch.
          #
          # If the yielded hunk is nil, all hunks in the patch have been
          # processed.
          def each_hunk_with_offset(patch)
            offset = 0

            patch.each do |hunk|
              yield hunk, offset
              offset += hunk.new_lines - hunk.old_lines
            end

            yield nil, offset
          end

          def before_hunk?(hunk, lineno)
            lineno < hunk.old_start || (lineno == hunk.old_start && hunk.old_lines == 0)
          end

          def in_hunk?(hunk, lineno)
            lineno >= hunk.old_start && lineno < hunk.old_start + hunk.old_lines
          end
        end
      end
    end
  end
end
