# frozen_string_literal: true

module GitRPC
  class Backend
    module ReadAdjustedPositions

      # Public: An abstract base class encapsulating data required to resolve a single blob object.
      class BlobData
        attr_reader :repository

        # blob_oid   - A String oid pointing to the blob
        # repository - An instance of Rugged::Repository
        def initialize(repository:)
          @repository = repository
        end

        # Public: Override in subclasses as needed. Should be cached as it can be called
        #   multiple times.
        #
        # Returns the string blob oid
        def blob_oid
          fail NotImplementedError
        end

        # Public: loads the blob for the given blob oid.
        #
        # Returns a Rugged::Blob instance.
        def load_blob
          blob = Rugged::Blob.lookup(repository, blob_oid)
          raise GitRPC::InvalidObject, blob_oid unless blob.type == :blob
          blob
        rescue Rugged::InvalidError
          fail GitRPC::InvalidObject, blob_oid
        end
      end
    end
  end
end
