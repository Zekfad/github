# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    module ReadAdjustedPositions

      # Responsible for loading a patch for the purposes of calculating adjusted positions.
      class PatchResolver
        DIFF_OPTIONS = {
          context_lines: 0,
          skip_binary_check: false,
          max_size: 1 * 1024 * 1024
        }

        class << self

          # Public: loads the diff patch from source to destination.
          #
          # source      - a BlobData instance
          # destination - a BlobData instance
          #
          # Returns nil if the source and destination are the same, otherwise returns
          #   a Rugged::Patch instance.
          def load_patch(source, destination)

            # There are no adjusted positions to calculate if the blobs are the same.
            # Same for if the commits match as we don't adjust across just paths.
            return nil if source == destination

            begin
              blob1 = source.load_blob
              blob2 = destination.load_blob
            rescue Rugged::OdbError => e
              oid = read_oid(e.message)
              raise GitRPC::ObjectMissing.new("Cannot load blob for source #{source}, destination #{destination}", oid)
            end

            return nil if blob1.oid == blob2.oid

            blob1.diff(blob2, **DIFF_OPTIONS)
          end

          private

          def read_oid(str)
            if match = str.match(/object not found - no match for id \((\w*)\)/)
              match[1]
            end
          end
        end
      end
    end
  end
end
