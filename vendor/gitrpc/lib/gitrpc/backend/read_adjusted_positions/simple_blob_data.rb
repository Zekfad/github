# frozen_string_literal: true

require_relative "blob_data"

module GitRPC
  class Backend
    module ReadAdjustedPositions

      # Public: A simple class for loading a blob given its blob_oid and access to the backend
      class SimpleBlobData < BlobData
        # blob_oid   - A String oid pointing to the blob
        # repository - An instance of Rugged::Repository
        def initialize(repository:, blob_oid:)
          super(repository: repository)
          @blob_oid   = blob_oid
        end

        # overrides abstract method in super class with a simple getter.
        attr_reader :blob_oid
      end
    end
  end
end
