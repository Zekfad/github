# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_writer :create_revert_commit
    def create_revert_commit(*args)
      options, err = create_revert_commit_options(*args)
      return [nil, err] unless err.nil?

      [Rugged::Commit.create(rugged, options), nil]
    rescue Rugged::IndexError
      [nil, "merge_conflict"]
    rescue Rugged::TreeError, Rugged::RevertError => e
      [e.to_s, "error"]
    end

    rpc_writer :create_revert_commit_rugged
    alias create_revert_commit_rugged create_revert_commit

    rpc_reader :stage_signed_revert_commit
    def stage_signed_revert_commit(*args)
      options, err = create_revert_commit_options(*args)
      return [nil, err] unless err.nil?

      [Rugged::Commit.create_to_s(rugged, options), nil]
    rescue Rugged::IndexError
      [nil, "merge_conflict"]
    rescue Rugged::TreeError, Rugged::RevertError => e
      [e.to_s, "error"]
    end

    rpc_writer :persist_signed_revert_commit
    def persist_signed_revert_commit(base_data, signature, *args)
      # ensure that any blobs/trees are created on all replicas.
      _, err = create_revert_commit_options(*args)
      return [nil, err] unless err.nil?

      oid = Rugged::Commit.create_with_signature(rugged, base_data, signature, "gpgsig")
      [oid, nil]
    rescue Rugged::IndexError
      [nil, "merge_conflict"]
    rescue Rugged::TreeError, Rugged::RevertError => e
      [e.to_s, "error"]
    end

    def create_revert_commit_options(revert, ours, author, commit_message, mainline, committer = nil)
      revert = resolve_commit(revert)
      ours = resolve_commit(ours)

      author = symbolize_keys(author)
      author[:time] = iso8601(author[:time])

      committer ||= author
      committer = symbolize_keys(committer)
      committer[:time] = iso8601(committer[:time])

      commit_message = Rugged.prettify_message(commit_message, false)

      merge_options = {
        :fail_on_conflict => true,
        :skip_reuc => true,
        :mainline => mainline,
        :no_recursive => true,
      }
      index = rugged.revert_commit(revert, ours, **merge_options)
      return [nil, "merge_conflict"] if (index.nil? || index.conflicts?)

      options = {
        :message    => commit_message,
        :committer  => committer,
        :author     => author,
        :parents    => [ours],
        :tree       => index.write_tree(rugged)
      }

      [options, nil]
    end

    def build_revert_commit_message(commit)
      <<-EOS
Revert "#{commit.summary}"

This reverts commit #{commit.oid}.
      EOS
    end

    RevertTimeout = Class.new(Timeout)

    # Public: Revert all commits starting at (but not including) `range_start_commit_oid`
    # and ending at `range_end_commit_oid`, on top of `target_commit_oid`.
    rpc_writer :create_revert_commits_for_range
    def create_revert_commits_for_range(range_start_commit_oid, range_end_commit_oid, target_commit_oid, author, committer, timeout)
      ours = resolve_commit(target_commit_oid)

      walk_options = {
        show: range_end_commit_oid,
        hide: range_start_commit_oid,
        simplify: true
      }
      commits = Rugged::Walker.to_enum(:walk, rugged, **walk_options)

      deadline = timeout ? Process.clock_gettime(Process::CLOCK_MONOTONIC) + timeout : nil

      revert_commit_id = commits.inject(ours.oid) do |result_id, commit|
        if deadline && deadline < Process.clock_gettime(Process::CLOCK_MONOTONIC)
          raise RevertTimeout
        end

        if commit.parent_ids.size > 1
          return ["merge commit encountered in specified commit range", "error"]
        end

        message = build_revert_commit_message(commit)

        options, err = create_revert_commit_options(commit.oid, result_id, author, message, nil, committer)
        return [nil, err] unless err.nil?

        Rugged::Commit.create(rugged, options)
      end

      [revert_commit_id, nil]
    end
  end
end
