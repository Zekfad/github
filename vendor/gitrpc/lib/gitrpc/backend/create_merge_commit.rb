# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    SUBMODULE_MODE = 0160000

    def resolve_commit(rev)
      if valid_full_sha1?(rev)
        Rugged::Commit.lookup(rugged, rev)
      else
        obj = Rugged::Object.rev_parse(rugged, rev + "^{}")
        unless obj.type == :commit
          raise GitRPC::InvalidObject.new("'#{obj.oid}' is a #{obj.type}, not a commit")
        end
        obj
      end
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, rev)
    rescue Rugged::InvalidError, Rugged::ReferenceError => e
      raise GitRPC::InvalidObject.new(e.message)
    end

    rpc_writer :create_merge_commit
    def create_merge_commit(*args)
      options, err, details = create_merge_commit_options(*args)
      return [nil, err, details] unless err.nil?

      [Rugged::Commit.create(rugged, options), nil]
    rescue Rugged::IndexError
      # Errors when building the index count as merge conflicts
      [nil, "merge_conflict"]
    rescue Rugged::TreeError => e
      [e.to_s, "error"]
    end

    rpc_reader :stage_signed_merge_commit
    def stage_signed_merge_commit(*args)
      options, err, details = create_merge_commit_options(*args)
      return [nil, err, details] unless err.nil?

      [Rugged::Commit.create_to_s(rugged, options), nil, nil, options[:tree]]
    rescue Rugged::IndexError
      # Errors when building the index count as merge conflicts
      [nil, "merge_conflict"]
    rescue Rugged::TreeError => e
      [e.to_s, "error"]
    end

    rpc_writer :persist_signed_merge_commit
    def persist_signed_merge_commit(base_data, signature, *args)
      # ensure that any blobs/trees are created on all replicas.
      options, err, details = create_merge_commit_options(*args)
      return [nil, err, details] unless err.nil?

      oid = Rugged::Commit.create_with_signature(rugged, base_data, signature, "gpgsig")
      [oid, nil]
    rescue Rugged::IndexError
      # Errors when building the index count as merge conflicts
      [nil, "merge_conflict"]
    rescue Rugged::TreeError => e
      [e.to_s, "error"]
    end

    def create_merge_commit_options(base, head, author, commit_message, committer = nil, opts = {}, return_tree = false)
      record_conflicts = opts[:record_conflicts] || false
      resolve_conflicts = opts[:resolve_conflicts] || {}
      max_conflicts = opts[:max_conflicts] || 25
      more_conflict_info = opts[:more_conflict_info]

      base = resolve_commit(base)
      head = resolve_commit(head)

      author = symbolize_keys(author)
      author[:time] = iso8601(author[:time])

      committer ||= author
      committer = symbolize_keys(committer)
      committer[:time] = iso8601(committer[:time])

      commit_message = Rugged.prettify_message(commit_message, false)

      merge_base = rugged.merge_base(base, head)
      return [nil, "already_merged"] if merge_base == head.oid

      # If the caller already knows which tree they want and we have it, we can
      # return immediately.
      tree = opts[:tree]
      if valid_full_sha1?(tree) && (rugged.exists?(tree) || tree == EMPTY_TREE_OID)
        options = {
          :message    => commit_message,
          :committer  => committer,
          :author     => author,
          :parents    => [base, head],
          :tree       => tree
        }

        return [options, nil]
      end

      ancestor_tree = merge_base && Rugged::Commit.lookup(rugged, merge_base).tree
      merge_options = {
        :fail_on_conflict => !record_conflicts && resolve_conflicts.empty?,
        :skip_reuc => true,
        :no_recursive => true,
        :renames => false,
      }
      index = base.tree.merge(head.tree, ancestor_tree, merge_options)
      return [nil, "merge_conflict"] if index.nil?

      if index.conflicts?
        if !resolve_conflicts.empty?
          # verify all the conflicted files have resolutions
          entries = []
          index.conflicts.each do |ci|
            filename = (ci[:ours] || ci[:theirs])[:path]
            return [nil, "merge_conflict"] if resolve_conflicts[filename].nil?
            entries << {
              :path => filename,
              :mode => (ci[:ours] || ci[:theirs])[:mode], #XXX
              :mtime => committer[:time],
            }
          end

          index.conflict_cleanup
          entries.each do |entry|
            entry[:oid] = rugged.write(resolve_conflicts[entry[:path]], :blob)
            index.add(entry)
          end

          # fall through to the bottom where we commit the changes
        elsif record_conflicts
          conflicts = {}
          maxed_out = false
          count = 0
          index.conflicts.each do |ci|
            filename = (ci[:ours] || ci[:theirs])[:path]
            # for now, we only really handle simple conflicts, so we're not
            # storing filename or mode changes. we can just add them here
            # later if we need it.
            conflicts[filename] = {
              :base => nil,
              :head => nil,
              :ancestor => nil,
            }

            if ci[:theirs]
              oid = ci[:theirs][:oid]
              if more_conflict_info && ci[:theirs][:mode] != SUBMODULE_MODE
                blob, = read_blobs([oid])
              else
                blob = nil
              end
              info = conflicts[filename][:base] = {
                :oid => oid,
              }

              if blob
                info[:file_size] = blob["size"]
                info[:binary] = blob["binary"]
              end
            end

            if ci[:ours]
              oid = ci[:ours][:oid]
              if more_conflict_info && ci[:ours][:mode] != SUBMODULE_MODE
                blob, = read_blobs([oid])
              else
                blob = nil
              end
              info = conflicts[filename][:head] = {
                :oid => oid,
              }

              if blob
                info[:file_size] = blob["size"]
                info[:binary] = blob["binary"]
              end
            end

            if ci[:ancestor]
              oid = ci[:ancestor][:oid]
              if more_conflict_info && ci[:ancestor][:mode] != SUBMODULE_MODE
                blob, = read_blobs([oid])
              else
                blob = nil
              end
              info = conflicts[filename][:ancestor] = {
                :oid => oid,
              }

              if blob
                info[:file_size] = blob["size"]
                info[:binary] = blob["binary"]
              end
            end

            conflicts[filename][:type] =  if ci[:ours] && ci[:theirs]
            # file exists in both revisions
            if ci[:ours][:mode] == SUBMODULE_MODE || ci[:theirs][:mode] == SUBMODULE_MODE
              :submodule_conflict
            elsif ci[:ours][:mode] != ci[:theirs][:mode]
              :mode_conflict
            elsif ci[:ours][:filename] != ci[:theirs][:filename]
              :filename_conflict
            else
              :regular_conflict
            end
            else
              # file only exists in one revision
              ci[:ours] ? :not_in_theirs : :not_in_ours
            end
            count = count + 1
            if count >= max_conflicts
              maxed_out = true
              break
            end
          end
          index.clear

          return [nil, "merge_conflict", {
              :base => base.oid,
              :head => head.oid,
              :conflicted_files => conflicts,
              :more_conflicted_files => maxed_out
            }]
        else
          return [nil, "merge_conflict"]
        end
      end

      options = {
        :message    => commit_message,
        :committer  => committer,
        :author     => author,
        :parents    => [base, head],
        :tree       => index.write_tree(rugged)
      }

      [options, nil]
    end
  end
end
