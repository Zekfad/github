# rubocop:disable Style/FrozenStringLiteralComment
require "licensee"

module GitRPC
  class Backend
    rpc_reader :detect_license
    def detect_license(commit_oid)
      project = Licensee::Projects::GitProject.new(rugged, revision: commit_oid)
      project.license ? project.license.key : "no-license"
    end
  end
end
