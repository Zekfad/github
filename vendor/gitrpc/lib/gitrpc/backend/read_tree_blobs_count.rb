# frozen_string_literal: true

module GitRPC
  class Backend
    rpc_reader :read_tree_blobs_count
    def read_tree_blobs_count(oid, limit = nil)
      tree = fetch_tree(oid)

      tree.count_recursive(limit)
    end
  end
end
