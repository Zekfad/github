# rubocop:disable Style/FrozenStringLiteralComment
require "editor_config"

module GitRPC
  class Backend
    class EditorConfigTimeout < Timeout
    end

    # Internal: Get combined EditorConfig for blob paths.
    #
    #
    # tree_oid - Root tree OID String
    # paths    - Array of String paths
    # deadline - Float unix timestamp to run until
    #
    # TODO: default deadline = nil can be removed one day after this is fully
    # rolled out to all file servers.
    #
    # Returns a Hash of String paths to EditorConfig Hash results.
    def fetch_editor_config(tree_oid, paths, deadline = nil)
      result = {}
      timeout = deadline - Time.now.to_f if deadline

      begin
        self.timeout(timeout, EditorConfigTimeout) do
          tree = rugged.lookup(tree_oid)
          if tree.type != :tree
            raise GitRPC::InvalidObject, "Invalid object type, expected tree but was #{tree.type}"
          end

          paths.each do |path|
            config = EditorConfig.load(path) do |config_path|
              begin
                entry = tree.path(config_path)
              rescue Rugged::TreeError
              else
                rugged.lookup(entry[:oid]).content
              end
            end
            result[path] = EditorConfig.preprocess(config)
          end
        end
      rescue EditorConfigTimeout
      end

      result
    end
    rpc_reader :fetch_editor_config
  end
end
