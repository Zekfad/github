# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_writer :rewrite_merge_commit
    def rewrite_merge_commit(*args)
      options = rewrite_merge_commit_options(*args)
      Rugged::Commit.create(rugged, options)
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, args.first)
    end

    rpc_reader :stage_signed_rewrite_merge_commit
    def stage_signed_rewrite_merge_commit(*args)
      options = rewrite_merge_commit_options(*args)
      Rugged::Commit.create_to_s(rugged, options)
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, args.first)
    end

    rpc_writer :persist_signed_rewrite_merge_commit
    def persist_signed_rewrite_merge_commit(base_data, signature, *args)
      # ensure that any blobs/trees are created on all replicas.
      _ = rewrite_merge_commit_options(*args)

      Rugged::Commit.create_with_signature(rugged, base_data, signature, "gpgsig")
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message)
    end

    def rewrite_merge_commit_options(commit_oid, info, squash_commits = false)
      raise ArgumentError.new("Committer is required") unless info["committer"]
      raise ArgumentError.new("Commit message is required") unless info["message"]

      message   = info["message"]
      committer = symbolize_keys(info["committer"])
      author    = symbolize_keys(info["author"] || committer.dup)

      [author, committer].each do |person|
        person[:time] =
          case person[:time]
          when String
            iso8601(person[:time])
          when Array
            unixtime_to_time(person[:time])
          else
            raise ArgumentError, "Invalid time value: #{person[:time]}"
          end
      end

      old_merge_commit = rugged.lookup(commit_oid)

      {
        :message    => message,
        :committer  => committer,
        :author     => author,
        :parents    => squash_commits ? [old_merge_commit.parents.first] : old_merge_commit.parents,
        :tree       => old_merge_commit.tree,
      }
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, commit_oid)
    end
  end
end
