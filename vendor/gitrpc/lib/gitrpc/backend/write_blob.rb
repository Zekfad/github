# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Public: Write a blob into the repository
    # content - a string containing the blob contents
    #
    # Returns the object name for this new blob
    rpc_writer :write_blob
    def write_blob(content)
      raise ArgumentError.new("Content must be a String") unless content.is_a?(String)

      rugged.write(content, :blob)
    end
  end
end
