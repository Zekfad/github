# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    TREE_ENTRY_SIZE_LIMIT = 10 * 1024 * 1024
    # Public: Fetch the metadata about the entries in a tree
    # at a path.
    #
    # oid - The oid of the commit to find the tree entry for
    # path - The path to find tree entries for.
    # limit - the maximum number of entries to return
    # flatten_paths - recurse into trees with a single entry
    # skip_size - do not return the size of the referenced blobs
    #
    # See Client#read_tree_entries for usage documentation.
    rpc_reader :read_tree_entries
    def read_tree_entries(oid, path, limit, flatten_paths, skip_size = false)
      path = "" if path.nil?
      tree = root = fetch_tree(oid)

      if path && !path.empty?
        tree = root.path(path)
        if tree[:type] != :tree
          raise GitRPC::InvalidObject, "Invalid type at path: #{path}, expected tree, got #{tree[:type]}"
        end
        tree = rugged.lookup(tree[:oid])
      end

      entries = []
      opts = { :flatten_paths => flatten_paths, :skip_size => skip_size }
      tree.each.with_index do |entry, index|
        break if limit && index >= limit
        entries << entry_to_hash(entry, path, root, opts)
      end

      result = {
        "entries" => entries,
        "oid" => tree.oid
      }

      if limit && tree.length > limit
        result["truncated_entries"] = tree.length - limit
      end

      result
    rescue Rugged::TreeError => e
      raise NoSuchPath,  e
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, oid)
    end

    # Public: Fetch metadata about all of the entries in a tree that
    # match the given conditions, recursively.  Similar in behavior
    # to `read_tree_entries_recursive`
    #
    # oid        - The oid of a commit or tree to walk from
    # path       - The path to the tree to fetch
    # conditions - Conditions Hash as documented in the client/* method
    # limit      - The maximum Integer number of entries to return, or false for no limit
    rpc_reader :read_tree_entries_recursive_filtered
    def read_tree_entries_recursive_filtered(oid, path, conditions, limit)
      # Symbolize the keys of conditions, for simplicity
      conditions.keys.each do |key|
        conditions[key.to_sym] = conditions.delete(key)
      end

      # Prepare params and flags
      truncated = false
      path = "" if path.nil?
      tree = root = fetch_tree(oid)

      if path && !path.empty?
        tree = root.path(path)
        if tree[:type] != :tree
          raise GitRPC::InvalidObject, "Invalid type at path: #{path}, expected tree, got #{tree[:type]}"
        end
        tree = rugged.lookup(tree[:oid])
      end

      # Walk & accumulate
      result = {"entries" => [], "oid" => tree.oid}

      # If we use either of the size filters, we can't skip fetching the size.
      use_min = !!conditions[:min_size]
      use_max = !!conditions[:max_size]
      opts = { :skip_size => (!use_min && !use_max) }

      tree.walk(:preorder).with_index do |(root_path, entry), index|
        if limit && index >= limit
          result["truncated_entries"] = true
          break
        end

        entry[:name].scrub!
        info = entry_to_hash(entry, File.join(path, root_path), root, opts)
        if object_info_matches_conditions?(info, conditions)
          result["entries"] << info
        end
      end

      result
    rescue Rugged::TreeError => e
      raise NoSuchPath,  e
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, oid)
    end

    # Public: Fetch the metadata about all of the entries in a tree recursively
    #
    # oid - The oid of the committish to find the tree entries for.
    # path - The path to the tree to fetch
    # limit - the maximum number of entries to return
    # skip_size - do not return the size of the referenced blobs
    rpc_reader :read_tree_entries_recursive
    def read_tree_entries_recursive(oid, path = nil, limit = 1000, skip_size = false)
      truncated = false
      path = "" if path.nil?
      tree = root = fetch_tree(oid)

      if path && !path.empty?
        tree = root.path(path)
        if tree[:type] != :tree
          raise GitRPC::InvalidObject, "Invalid type at path: #{path}, expected tree, got #{tree[:type]}"
        end
        tree = rugged.lookup(tree[:oid])
      end

      opts = { :skip_size => skip_size }
      entries = []
      tree.walk(:preorder).with_index do |(root_path, entry), index|
        if limit && index >= limit
          truncated = true
          break
        end
        entries << entry_to_hash(entry, File.join(path, root_path.b), root, opts)
      end
      result = {
        "entries" => entries,
        "oid" => tree.oid
      }

      result["truncated_entries"] = true if limit && truncated

      result
    rescue Rugged::TreeError => e
      raise NoSuchPath,  e
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, oid)
    end

    # Public: read a single tree entry given a path and a sha.
    #
    # oid - the oid of the commit or tree to find the entry in.
    # path - The path of the blob or tree to look for.
    #
    # See Client#read_tree_entry for usage.
    rpc_reader :read_tree_entry
    def read_tree_entry(oid, path = nil, options = {})
      truncate = if options.key?("truncate")
        options["truncate"]
      else
        1024 * 1024
      end
      limit = if options.key?("limit")
        options["limit"]
      else
        TREE_ENTRY_SIZE_LIMIT
      end

      type = if options.key?("type")
        options["type"]
      end

      path = "" if path.nil?
      entry = root = fetch_tree(oid)

      if path && !path.empty?
        entry = root.path(path)
        actual_type = entry[:type].to_s
      else
        actual_type = "tree"
      end

      if type && type != actual_type
        raise GitRPC::InvalidObject, "Invalid type at path: #{path}, expected #{type}, got #{entry[:type]}"
      end

      res = if path && !path.empty?
      entry_to_hash(entry, File.dirname(path), root,
                    :full_blob => true,
                    :truncate => truncate,
                    :limit => limit)
      else
        {
          "type" => "tree",
          "oid"  => root.oid,
          "mode" => 040000,
          "name" => "",
          "path" => "",
          "size" => nil,
        }
      end

      res["name"] = path
      res
    rescue Rugged::TreeError => e
      raise NoSuchPath,  e
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, oid)
    end

    rpc_reader :read_tree_entry_oid
    def read_tree_entry_oid(oid, path = nil, options = {})
      type = options.fetch("type", nil)

      path = "" if path.nil?
      root = fetch_tree(oid)

      # If there's no path, return the root tree's id
      return root.oid if path.empty?

      entry = root.path(path)

      if type && type != entry[:type].to_s
        raise GitRPC::InvalidObject, "Invalid type at path: #{path}, expected #{type}, got #{entry[:type]}"
      end

      entry[:oid]
    rescue Rugged::TreeError => e
      raise NoSuchPath,  e
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, oid)
    end

    rpc_reader :read_tree_entry_oids
    def read_tree_entry_oids(oid, paths = [])
      root = fetch_tree(oid)

      oids = {}

      paths.each do |path|
        entry = root.path(path)
        oids[path] = entry[:oid]
      end

      oids
    rescue Rugged::TreeError => e
      raise GitRPC::NoSuchPath, e
    rescue Rugged::OdbError => e
      raise GitRPC::ObjectMissing.new(e.message, oid)
    end

    private

    # Private: Used by `read_tree_entries_recursive_filtered` to test
    # if the objects that are returned by the tree walk qualify
    # for the result set.
    #
    #  object   - Hash of a tree entry, as produced by `entry_to_hash`
    #  c        - Hash of conditions, with symbol keys.
    #
    # Returns true or false
    def object_info_matches_conditions?(object, c)
      result = true

      # Numerical comparisons should fail if specified and used
      # on incompatible objects.
      inf = Float::INFINITY

      result &&= ((object["size"] || inf) <= c[:max_size])  if c.has_key?(:max_size)
      result &&= ((object["size"] || -inf) >= c[:min_size]) if c.has_key?(:min_size)
      result &&= (object["type"] == c[:type])               if c.has_key?(:type)
      result &&= (!!object["binary"] == !!c[:binary])       if c.has_key?(:binary)

      result
    end

    def fetch_tree(oid)
      obj = rugged.lookup(oid)
      case obj.type.to_sym
      when :commit
        obj.tree
      when :tree
        obj
      else
        raise GitRPC::InvalidObject, "Invalid object type #{obj.type}, expected commit or tree"
      end
    end

    def resolve_symlink(entry, path, root)
      resolved = rugged.read(entry[:oid]).data.strip
      base = File.join("/", path)
      dest = File.expand_path(resolved, base).sub(/\A\/+/, "")

      target = root.path(dest)
      [dest, target] if [:blob, :tree].include?(target[:type])
    rescue
      nil
    end

    def symlink?(mode)
      mode & 0120000 == 0120000
    end

    def flatten_entry_path(entry)
      path = nil

      loop do
        tree = rugged.lookup(entry[:oid])
        break if tree.count != 1

        entry = tree[0]
        break if entry[:type] != :tree

        path = File.join(path || "", entry[:name].b)
      end

      path
    end

    def entry_to_hash(entry, path, root_tree, options = {})
      opts = { :resolve_symlink => true,
               :full_blob       => false,
               :flatten_paths   => false,
               :skip_size       => false,
      }.merge(options)

      name = entry[:name].b

      full_path = File.join(path, name)
      full_path.gsub!(%r{\A(\.?/)*}, "")

      res = {
        "type" => entry[:type].to_s,
        "oid"  => entry[:oid],
        "mode" => entry[:filemode],
        "name" => name,
        "path" => full_path,
        "size" => nil
      }

      if entry[:type] == :blob
        if symlink?(entry[:filemode]) && opts[:resolve_symlink]
          sym_path, target = resolve_symlink(entry, path, root_tree)
          if target
            res["symlink_target"] = target[:oid]
            res["symlink_target_object"] = entry_to_hash(target, File.dirname(sym_path), root_tree, :resolve_symlink => false, :full_blob => false, :skip_size => opts[:skip_size])
          end
        end

        if !opts[:skip_size] || opts[:full_blob]
          header = rugged.read_header(entry[:oid])
          if opts[:full_blob]
            res.merge!(get_blob_object(entry[:oid], header, opts[:truncate], opts[:limit]))
          else
            res.merge!("size" => header[:len]) # rubocop:disable Performance/RedundantMerge
          end
        end
      end

      if opts[:flatten_paths] && entry[:type] == :tree
        if flat = flatten_entry_path(entry)
          res["simplified_path"] = File.join(full_path, flat)
        end
      end

      res
    end

  end
end
