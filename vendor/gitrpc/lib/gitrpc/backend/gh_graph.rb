# frozen_string_literal: true

require_relative "gh_graph/graph_data"
require_relative "gh_graph/bucketed_graph_data"
require_relative "contribution_caches"

module GitRPC
  class Backend
    rpc_reader :gh_graph_data
    def gh_graph_data(oid, version: 1)
      ensure_valid_full_sha1(oid)
      GraphData.new(self, oid, version: version).process
    end

    rpc_reader :gh_graph_data_by_day_and_author
    def gh_graph_data_by_day_and_author(from_oid: nil, to_oid:)
      return BucketedGraphData.new(backend: self, from_oid: from_oid, to_oid: to_oid).data
    end

    rpc_writer :clear_graph_cache
    def clear_graph_cache
      GitRPC::Backend::ContributionCaches.new(self).clear
    end
  end
end
