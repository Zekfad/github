# frozen_string_literal: true

module GitRPC
  class Backend
    class LogParser
      include Enumerable

      # git log output for co-author trailer parser
      # Eg:
      #   946702800 -0500----someone@example.org
      #   co-authored-by: Homer Simpson <hsimpson@github.com>
      #
      #   1 file changed, 2 insertions(+), 1 deletion(-)
      #
      #   946702800 -0500----someone@example.org
      #   1 file changed, 1 insertion(+)
      CO_AUTHOR_FORMAT = "%m%ad----%ae%n%-(trailers:only,unfold)"

      # matches the signature of an authorship trailer
      # NOTE: This is copied from app/models/git_actor.rb These must be kept in sync.
      #       The plan is to get this one into a proper gitrpc backend endpoint and then
      #       Reference that in GitActor instead of defining it there at all.
      # eg:
      #   Homer Simpson <hsimpson@github.com>
      SIGNATURE_MATCHER = /\A(?<name>.+)\s\<(?<email>.+@.+)\>\z/

      # matches the first log line for each commit.
      # eg:
      # >1510684028 -0500----hsimpson@github.com
      HEADER_MATCHER = /\A(?<side>[<>])(?<timestamp>\d+) (?<zone_offset>[\-\+]?\d+)----(?<email>.+)\z/

      # Used for quickly triaging a line to see if it contains statistic information
      STATS_PREFIX = /files? changed/

      # Matches the change statistics line
      STATS_MATCHER = /\A \d+ files? changed, ((?<insertions>\d+) insertions?\(\+\))?(, )?((?<deletions>\d+) deletions?\(\-\))?/

      def initialize(backend:, revision_or_range:, max_commits: nil)
        @backend = backend
        @max_commits = max_commits
        @revision_or_range = revision_or_range
      end

      attr_reader :backend, :max_commits, :revision_or_range

      def each
        lines = fetch_log
        enumerator = lines.each

        while enumerator.peek
          commit_data = extract_commit_lines(enumerator)

          # empty commits have no contribution relevance
          next unless commit_data&.key?(:stats)

          yield commit_data
        end
      rescue StopIteration
      end

      private

      def fetch_log
        args = [
          "--pretty=format:#{CO_AUTHOR_FORMAT}",
          "--date=raw",
          "--shortstat",
          "--no-renames",
          "--no-merges"
        ]
        args << "--max-count=#{max_commits}" if max_commits
        args << revision_or_range

        res = backend.spawn_git_ro("log", args)
        fail GitRPC::CommandFailed.new(res) unless res["ok"]

        res["out"].split("\n")
      end

      def extract_commit_lines(enumerator)
        line = enumerator.next
        line = enumerator.next until header_match = match_header(line)

        commit_data = extract_header_data(header_match)
        return nil if commit_data.nil?

        while line = enumerator.peek
          if line.empty?
            next enumerator.next
          elsif stats = extract_change_counts(line)
            commit_data[:stats] = stats
            enumerator.next
          elsif match = line.match(GitRPC::Backend::GIT_TRAILER_MATCHER)
            if email = extract_coauthor_email(match)
              commit_data[:emails] << email
            end
            enumerator.next
          elsif match_header(line)
            # We've hit the next commit. Return what we've extracted.
            return commit_data
          else
            # Ignore invalid lines.
            enumerator.next
          end
        end
      rescue StopIteration
        commit_data
      end

      # The first line contains the unix epoch and the email address
      def extract_header_data(header_match)
        return if header_match.nil?

        timestamp = Integer(header_match[:timestamp])
        zone = Time.zone_offset(header_match[:zone_offset]) || 0
        {
          removal: header_match[:side] == "<",
          emails: [header_match[:email]],
          timestamp: timestamp + zone
        }
      end

      def extract_change_counts(stats_line)
        return nil unless stats_line[STATS_PREFIX]

        match_data = stats_line.match(STATS_MATCHER)
        return nil unless match_data

        [match_data[:insertions].to_i, match_data[:deletions].to_i]
      end

      def extract_coauthor_email(trailer_match)
        return unless trailer_match[:key].downcase == "co-authored-by"

        signature = trailer_match[:value]
        match = signature.match(SIGNATURE_MATCHER)
        return unless match

        match[:email]
      end

      def match_header(line)
        return nil if line.nil?
        return nil unless line.start_with?("<", ">")

        line&.match(HEADER_MATCHER)
      end
    end
  end
end
