# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    RECURSION_LIMT=3

    rpc_reader :peel_to_commit_or_tree
    def peel_to_commit_or_tree(oid)
      return nil if oid.nil? || oid == NULL_OID
      obj = rugged.lookup(oid)
      peel_object(obj)
    end

    def peel_object(obj, depth = 1)
      case obj
      when Rugged::Tree
        obj.oid
      when Rugged::Commit
        obj.oid
      when Rugged::Tag::Annotation
        raise TypeError, "Tag recursion too deep" if depth >= RECURSION_LIMT
        peel_object(obj.target, depth+1)
      end
    end
  end
end
