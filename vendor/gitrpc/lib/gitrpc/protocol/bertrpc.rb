# rubocop:disable Style/FrozenStringLiteralComment
require "gitrpc"
require "bertrpc"
require "securerandom"

module GitRPC
  class Protocol
    # The BERTRPC protocol implementation handles encoding and decoding of
    # remote backend method invocations using BERTRPC. See the BERTRPC website
    # for more information on the wire protocol:
    #
    # <http://bert-rpc.org/>
    #
    # BERTRPC URLs look like this:
    #
    #     bertrpc://example.org/path/to/repo.git
    #
    # They must include the host and path parts and may also include a port as
    # in the following example:
    #
    #     bertrpc://example.org:9119/path/to/repo.git
    #
    # The BERTRPCServer handler module is the server side. It must be running on
    # the remote host under a "gitrpc" module name.
    class BERTRPC < Protocol
      # The URI object used to construct this object.
      attr_reader :url

      # The host to connect to. This must be specified in the URL.
      attr_reader :host

      # The port to connect to. Defaults to 8149 when not given in the URL.
      attr_reader :port

      # The full path to the repository on disk on the remote machine. This must
      # be specified in the URL.
      attr_reader :path

      # Default BERTRPC port. Used when no port is given in the URL.
      class << self
        attr_accessor :port
      end
      self.port = 8149

      # The remote backend implementation - used by dgit
      attr_reader :remote

      # Create a new BERTRPC protocol object for the given URL and options. No
      # actual connection is established until the #send_message method is
      # called so no exception.
      #
      # url     - A URI object whose scheme is "bertrpc". The host and path
      #           parts must be present. The port is optional but supported.
      # options - The options hash to pass to the backend.
      #
      def initialize(url, options = {})
        @url = url
        @options = options.dup

        if @options[:timeout].nil? && GitRPC.timeout
          @options[:timeout] = GitRPC.timeout
        end

        @host = url.host
        @port = url.port || self.class.port
        @path = url.path

        if GitRPC.optimize_local_access && @host == GitRPC.local_git_host_name
          @remote = BERTLocalFileWrapper.new(url, options)
        else
          service = ::BERTRPC::Service.new(@host, @port, client_timeout,
                                            connect_timeout)
          @remote = service.call.gitrpc
        end
      end

      # Internal: Actual timeout value to use as the client connect / read
      # timeout. This is fudged for network access to give enough time
      # for the remote side to timeout and return giving a deeper backtrace.
      #
      # Returns a float number of seconds for the timeout or nil when no timeout
      # is given.
      def client_timeout
        GitRPC::Timer.fudge(timeout, :client)
      end

      def timeout
        options[:timeout]
      end

      # Grab connect_timeout from options or global value if not specified.
      def connect_timeout
        options[:connect_timeout] || GitRPC.connect_timeout
      end

      # Protocol implementations must expose the provided options hash.
      attr_reader :options

      # Make the remote call and translate exceptions if necessary.
      def send_message(message, *args)
        GitRPC.instrument(:bertrpc_send_message, name: message, args: args, url: @url) do |instrument_payload|
          status, res = @remote.send_message(@path, options, message, args)
          if status == :ok
            res
          elsif status == :boom
            GitRPC::Failure.raise(res)
          else
            raise GitRPC::NetworkError, "invalid bertrpc status: #{status.inspect}"
          end
        end
      rescue Mochilo::PackError => boom
        raise GitRPC::EncodingError, boom
      rescue RuntimeError => boom
        if boom.to_s.include?("Cannot encode to erlang external format")
          raise GitRPC::EncodingError, boom
        else
          raise
        end
      rescue ::BERTRPC::BERTRPCError
        raise self.class.map_error($!)
      end

      # Default repository scope cache key
      def repository_key
        Digest::MD5.hexdigest(@path)
      end

      # Send [message, args] to the specified route via BERTRPC
      # This interface is different than protocol method `send_message` in that
      # exceptions are not squashed into GitRPC::Errors for callers that need to
      # full detail such as the DGIT protocol.
      def self.send_single(route, message, args, options = {})
        proto = from_route(route, options)

        GitRPC.instrument(:bertrpc_send_message, name: message, args: args, route: route, url: proto.url) do |instrument_payload|
          status, res = proto.remote.send_message(route.path, options, message, args)

          # [:boom, exception] is for errors that happen on the GitRPC server
          if status == :boom
            GitRPC::Failure.raise res
          else
            return res
          end
        end
      end

      # Send [message, args] to all of the routes via BERTRPC. Return
      # the resulting answers and errors. This method does no error or
      # consistency checking except to verify that we got *some* kind
      # of result (which might be a timeout or connection error or
      # whatever) for every route.
      #
      # Each item in routes may be a Route-like object (that responds
      # to resolved_host, port, and path) or a GitRPC::Protcol::BERTRPC
      # object (that responds to remote and path).
      def self.send_multiple(routes, message, args, options = {})
        answers = {}
        errors = {}

        # Fill in timeout values, if they're missing.
        options = {
          :timeout => GitRPC.timeout,
          :connect_timeout => GitRPC.connect_timeout,
        }.merge(options)

        rpc_start = Time.now
        notify_base = {
          name: message,
          args: args
        }

        handler = ::BERTRPC::MuxHandler.new
        routes.each do |route|
          notify_payload = notify_base.dup
          if route.respond_to? :remote
            rpc = route.remote
            notify_payload[:url] = route.url
          else
            proto = from_route(route, options)
            notify_payload[:route] = route
            notify_payload[:url] = proto.url
            rpc = proto.remote
          end

          cs = handler.queue(rpc)
          cs.on_complete do |result|
            answers[route] = result

            GitRPC.publish(:bertrpc_send_message, rpc_start, Time.now, SecureRandom.hex(10), notify_payload)
          end
          cs.on_error do |error|
            errors[route] = error.is_a?(Array) ? GitRPC::Failure.decode(error) : error
            notify_payload[:exception] = [errors[route].class.name, errors[route].message]

            GitRPC.publish(:bertrpc_send_message, rpc_start, Time.now, SecureRandom.hex(10), notify_payload)
          end
          rpc.send_message(route.path, options, message, args)
        end
        handler.run(options[:timeout], options[:connect_timeout])
        errors.each do |route, error|
          errors[route] = unify_bert_timeouts(error)
        end

        # Make sure we got the right number of answers + errors.
        unless answers.length + errors.length == routes.length
          raise DGit::ResponseError.new(
              "Expected #{routes.length} responses, got #{answers.length} + #{errors.length}",
                  answers: answers, errors: errors, rpc_operation: message)
        end

        [answers, errors]
      end

      def self.from_route(route, options = {})
        url = URI.parse("bertrpc://#{route.resolved_host}:#{route.port}#{route.path}")
        self.new(url, options)
      end

      def self.map_error(err)
        boom = case err
        when ::BERTRPC::ConnectionError
          GitRPC::ConnectionError.new(err)
        when ::BERTRPC::ReadTimeoutError
          GitRPC::Timeout.new(err)
        when ::BERTRPC::ReadError, ::BERTRPC::ProtocolError
          GitRPC::NetworkError.new(err)
        else
          GitRPC::Failure.wrap(err)
        end
        if !boom.backtrace || boom.backtrace.empty?
          boom.set_backtrace(caller)
        end
        boom
      end

      def self.unify_bert_timeouts(error)
        if error.is_a?(::BERTRPC::ReadTimeoutError)
          boom = GitRPC::Timeout.new(error)
          boom.set_backtrace(error.backtrace)
          boom
        else
          error
        end
      end
    end

    class BERTLocalFileWrapper
      attr_accessor :rpc, :path, :mux_handler, :call_state
      def initialize(route, options)
        @rpc = GitRPC::Backend.new(route.path, options)
        @path = route.path
        @call_state = BERTLocalCallState.new
      end

      def send_message(path, opts, message, args)
        # In case we need to run in parallel, we queue up
        # the message so it's read later and other remote
        # rpc calls can be set up in parallel.
        if mux_handler
          call_state.queue_message(@rpc, message, args)
        else
          res = @rpc.send_message(message, *args)
          [:ok, res]
        end
      end
    end

    class BERTLocalCallState < ::BERTRPC::MuxHandler::CallState
      def queue_message(rpc, message, args)
        @rpc = rpc
        @message = message
        @args = args
      end

      def read_result
        res = @rpc.send_message(@message, *@args)
        [:ok, res]
      rescue => boom
        failure = GitRPC::Failure.new(boom)
        [:boom, failure.encode]
      end

      def to_s
        "<local=true error=#{@error.inspect} result=#{@result.inspect}>"
      end

      def connected?
        true
      end

      def finished?
        true
      end

      def run
      end

      def select_fds
        [nil, nil]
      end
    end

    # Ernie server-side handler for the BERTRPC protocol. This runs on the
    # remote side and receives the #send_message call made by the protocol
    # implementation.
    module BERTRPCServer
      def send_message(path, options, message, args)
        return [:ok, true] if message == :_ping

        backend = GitRPC::Backend.new(path, options)
        res = backend.send_message(message, *args)
        [:ok, res]
      rescue => boom
        Ernicorn.filter_backtrace(boom)
        failure = GitRPC::Failure.new(boom)
        [:boom, failure.encode]
      ensure
        backend.cleanup if backend
      end

      # Register the server module with Ernie.
      def self.expose
        Ernie.expose(:gitrpc, self)
      end
    end
  end
end
