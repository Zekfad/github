# frozen_string_literal: true

require "gitrpc/twirp/middleware"
require "gitrpc/twirp/services"
