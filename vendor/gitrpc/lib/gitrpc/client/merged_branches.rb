# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: given the tip of a branch and a list of branch names, select
    # those branches that are reachable from the tip of the branch
    # (i.e. have been merged into it)
    #
    # Returns: an Array
    def select_merged_branches(base_branch_oid, branches)
      ensure_valid_full_sha1(base_branch_oid)

      branches & merged_branches(base_branch_oid)
    end

    # Private: Returns a list of all the branches that have been merged
    # to a given branch, where `base_branch_oid` is the tip of the branch
    # itself.
    #
    # *********
    # Internal API.
    # Public callers are encouraged to use `select_merged_branches` instead.
    # *********
    #
    # This is a potentially expensive operation and it's aggressively cached.
    # Note that the result depends on the tip of the branch we're checking
    # and *on the total amount of branches in the repository*. However, by caching
    # all the branches that have been merged into `base_branch_oid`, we assume
    # that any new branches pushed will *not* be merged into the main branch
    # (otherwise `base_branch_oid` would have changed, so the caching is still
    # accurate.
    #
    # This assumption doesn't hold, however,  if somebody pushes a branch which is
    # not new (i.e. pushes a reference pointing to an old commit in the repository),
    # but this behavior is not common so we won't attempt to invalidate the cache
    # in this case to reduce complexity.
    #
    # Note that because of the same caching reasons, this API may return branch
    # names that were merged into `base_branch_oid` but whose reference no longer
    # exists.
    #
    # Returns: an Array of branch names. All these branches are reachable from
    # the `base_branch_oid` commit. **Use this list as a membership test, not as a
    # authoritative enumeration of branches on a repository.**
    def merged_branches(base_branch_oid)
      key = repository_cache_key("merged_branches", base_branch_oid)

      @cache.fetch(key) do
        send_message(:merged_branches, base_branch_oid)
      end
    end
  end
end
