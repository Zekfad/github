# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def merge_single_file(ancestor, ours, theirs, options = {})
      # if the file was created in both branches, we might not have been
      # passed an ancestor, so we'll just stub that out here...
      a = ancestor || {
        :filename => nil,
        :oid => nil,
        :mode => nil
      }

      digest = ::Digest::MD5.hexdigest([
        a[:filename],        a[:oid],        a[:mode],
        ours[:filename],     ours[:oid],     ours[:mode],
        theirs[:filename],   theirs[:oid],   theirs[:mode],
        options[:our_label], options[:their_label],
      ].join(":"))

      @cache.fetch("merge_single_file:v1:#{digest}") do
        send_message(:merge_single_file, ancestor, ours, theirs, **options)
      end
    end
  end
end
