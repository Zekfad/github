# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def read_submodules(oid)
      ensure_valid_full_sha1(oid)

      @cache.fetch(content_cache_key("read_submodules", "v1", oid)) do
        send_message(:read_submodules, oid)
      end
    end
  end
end
