# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Create a merge commit
    #
    # base           - the commit to merge into
    # head           - the commit to merge
    # author         - author information hash
    # commit_message - the message to use for the commit
    def create_merge_commit(base, head, author, commit_message, options = {})
      author = stringify_keys(author)
      author["time"] = author["time"].iso8601

      args = [base, head, author, commit_message]
      if !options[:committer].nil?
        committer = stringify_keys(options.delete(:committer))
        committer["time"] = committer["time"].iso8601
        args << committer
      else
        args << nil
      end

      if block_given?
        base_data, err, details, tree_id = send_message(:stage_signed_merge_commit, *args, **options)
        return [base_data, err, details] unless err.nil?
        signature = yield base_data
        options[:tree] = tree_id

        if signature.nil? || signature.empty?
          send_message(:create_merge_commit, *args, **options)
        else
          send_message(:persist_signed_merge_commit, base_data, signature, *args, **options)
        end
      else
        send_message(:create_merge_commit, *args, **options)
      end
    end
  end
end
