# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def sorted_tag_refs
      @cache.fetch(tag_refs_key) do
        send_message(:sorted_tag_refs)
      end
    end

    def tag_refs_key
      repository_reference_cache_key("sorted_tag_refs:v3")
    end
  end
end
