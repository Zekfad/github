# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Return detailed commit log data covering <sha> history.
    #
    # oid - a String commit oid
    #
    # Returns array of data formatted as described by GitRPC::Backend::GraphData#process
    def gh_graph_data(oid, version: 1)
      return [] if oid.nil?

      ensure_valid_full_sha1(oid)
      send_message(:gh_graph_data, oid, version: version)
    end

    # Public: Return the commit, addition, and deletion counts for each author email for each
    #   day of the provided commit range.
    #
    # from_oid: the start of the commit range for which we are calculating the statistics.
    #   Defaults to the root if nil.
    # to_oid: the end of the commit range for which we are calculating the statistics.
    #
    # Returns an array of metrics hashes, ordered by date and then author email
    #
    # Example:
    # [{ date: "2018-10-08", author: "person@org.com", additions: 50, deletions: 3, commits: 2 }]
    def gh_graph_data_by_day_and_author(from_oid: nil, to_oid:)
      return {} if to_oid.nil?

      ensure_valid_full_sha1(to_oid)
      ensure_valid_full_sha1(from_oid) unless from_oid.nil?

      send_message(:gh_graph_data_by_day_and_author, from_oid: from_oid, to_oid: to_oid)
    end

    # Public: clears the graph cache
    #
    # Returns nothing
    def clear_graph_cache
      send_message(:clear_graph_cache)
    end
  end
end
