# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Perform a RPC roundtrip, returning whatever's provided as input.
    # This is primarily useful in tests and network analysis.
    #
    # obj - Any serializeable type. Hashes, arrays, and scalars.
    #
    # Returns the argument provided without modification.
    def echo(obj = nil)
      send_message :echo, obj
    end

    # Public: Emulate a slow response by sleeping on the server side. This is
    # primarily useful for network analysis and testing RPC protocol support
    # for call timeouts.
    #
    # time - Float amount of time to sleep in seconds. If this value is greater
    #        than the configured RPC timeout, the call should not return and
    #        instead raise an exception.
    #
    # Returns the literal string "OK".
    #
    # Raises a `GitRPC::Timeout` when the sleep time exceeds the configured
    # timeout value.
    def slow(time)
      send_message :slow, time
    end

    # Public: generate a random number on the server side.  This is a
    # simple way to make multiple, identical calls produce different
    # results.
    def rand
      send_message :rand
    end

    # Public: generate a random number on the server side.  This is
    # just like rand() except it's tagged read-only, so it will only
    # run on one backend.
    def one_rand
      send_message :one_rand
    end

    # Public: Check whether the repository is available.
    def online?
      send_message :online?
    rescue GitRPC::NetworkError
      false
    end
  end
end
