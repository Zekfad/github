# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Scan a repository for credentials using the find-creds script.
    #
    # refs - An optional Array of refs that changed: ([[before, after,
    # ref1_name], ...])
    #
    # If no ref changes are passed, the full repository history will be scanned.
    # Otherwise, we process each ref change to build out a set of OIDs to
    # include/exclude from the scan. We include all new tips. But, to avoid
    # scanning objects that have already been scanned, we need exclude the tips
    # from any refs that have not changed. For example, imagine the following
    # tree:
    #
    # A---B---C---D---E---F---G       ← master == origin/master
    #              \
    #               H---I             ← newbranch
    #
    #
    # A fresh push of newbranch to GitHub would contain a ref change list of
    # [[NULL_OID, OID_OF_I, "refs/heads/newbranch"]]. If we naively passed just
    # the "before" and "after" OIDs to `find-creds` it would search the entire
    # commit history from `I` back to `A`. But, `A`-`G` have alredy been
    # scanned. So, we exclude scanning `A`-`D` by excluding scanning of any tips
    # of unchanged refs.
    #
    # Returns an Array of credentials found (["CRED_TYPE TOKEN OID REPORT_URL", ...])
    def find_creds(refs: [], **options)
      raise ArgumentError, "max_result must be set" unless options.key?(:max_result)

      refs.each do |before, after, refname|
        ensure_valid_full_sha1(before)
        ensure_valid_full_sha1(after)
        ensure_valid_refname(refname)
      end
      send_message(:find_creds, refs, **options)
    end
  end
end
