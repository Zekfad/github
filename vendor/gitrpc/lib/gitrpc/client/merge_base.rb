# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Find as good common ancestors as possible for a merge.
    #
    # commit_oid1 - A full 40-character commit oid used as the base of comparison
    # commit_oid2 - A full 40-character commit oid used as the head for comparison
    # timeout     - Maximum number of seconds that the operation can take to complete
    #
    # Returns the merge base commit oid as a String. May raise
    # GitRPC::InvalidObject if target oid is missing or not a commit.
    # Raises GitRPC::Timeout if the operation takes to long.
    def merge_base(commit_oid1, commit_oid2, timeout: nil)
      native_merge_base(commit_oid1, commit_oid2, "merge-base", timeout)
    end

    # Public: Find the best merge base for merging head to base. This is very
    # much like `merge_base` except that if base and head have multiple merge
    # bases, it chooses the best merge base among the alternatives more
    # intelligently than "git merge-base".
    #
    # commit_oid1 - A full 40-character commit oid used as the base of comparison
    # commit_oid2 - A full 40-character commit oid used as the head for comparison
    # timeout     - Maximum number of seconds that the operation can take to complete
    #
    # Returns the merge base commit oid as a String. May raise
    # GitRPC::InvalidObject if target oid is missing or not a commit.
    # Raises GitRPC::Timeout if the operation takes to long.
    def best_merge_base(commit_oid1, commit_oid2, timeout: nil)
      native_merge_base(commit_oid1, commit_oid2, "best-merge-base", timeout)
    end

    def rugged_best_merge_base(commit_oid1, commit_oid2)
      ensure_valid_full_sha1(commit_oid1)
      ensure_valid_full_sha1(commit_oid2)

      cache_key = content_cache_key("rugged-best-merge-base-v2", commit_oid1, commit_oid2)
      @cache.fetch(cache_key) do
        send_message(:best_merge_base, commit_oid1, commit_oid2)
      end
    end

    # Public: Find the best comparison base commit for a branch
    #
    # head_oid - The branch head ref
    # base_oid - The ref of the branch that head_oid was originally based on
    # root_oid - The SHA1 of the commit where head_oid initially started
    #
    # Returns a string containing the 40-char oid of the best
    # comparison base commit, or raises GitRPC::CommandFailed.
    def branch_base(head_oid, base_oid, root_oid)
      ensure_valid_full_sha1(head_oid)
      ensure_valid_full_sha1(base_oid)
      ensure_valid_full_sha1(root_oid)

      send_message(:branch_base, head_oid, base_oid, root_oid)
    end

    # Private: Find common ancestor between two commits with native Git.
    #
    # commit_oid1 - A full 40-character commit oid used as the base of comparison
    # commit_oid2 - A full 40-character commit oid used as the head for comparison
    # git_command - Merge base strategy to use (either "best-merge-base" or "merge-base")
    # timeout     - Maximum number of seconds that the operation can take to complete
    #
    # Returns the merge base commit oid as a String. May raise
    # GitRPC::InvalidObject if target oid is missing or not a commit.
    # Raises GitRPC::Timeout if the operation takes to long.
    def native_merge_base(commit_oid1, commit_oid2, git_command, timeout)
      ensure_valid_full_sha1(commit_oid1)
      ensure_valid_full_sha1(commit_oid2)

      cache_key = content_cache_key(git_command, commit_oid1, commit_oid2)
      @cache.fetch(cache_key) do
        raise GitRPC::Timeout if timeout && timeout <= 0
        send_message(:native_merge_base, commit_oid1, commit_oid2, git_command, timeout)
      end
    end
  end
end
