# rubocop:disable Style/FrozenStringLiteralComment
require "json"

module GitRPC
  class Client
    DEFAULT_AUDIT_LOG_LIMIT = 100

    # Public: Read repositories audit log lines.
    #
    # offset - Integer byte offset to start reading from
    # limit  - Integer maximum number of lines
    #
    # Entry Hash
    #   "byteoffset": Integer byte offset to line
    #   "bytesize": Integer byte size of line
    #   "ref": String ref name
    #   "old_oid": String old OID of ref
    #   "new_oid": String new OID of ref
    #   "committer_name": String name of GIT_committer_NAME
    #   "committer_email": String email of GIT_committer_EMAIL
    #   "committer_date_timestamp": Integer unix timestamp of ref change
    #   "committer_date_offset": Integer localtime offset
    #   "socket": Hash of optional data from sockstat
    #
    # Returns an Array of entry Hashs and EOF boolean.
    def read_audit_log(offset: 0, limit: DEFAULT_AUDIT_LOG_LIMIT)
      send_message(:read_audit_log, offset, limit)
    end

    # Public: Read repositories audit log lines.
    #
    # offsets - Array of Integer byte offsets to start reading from.
    #
    # Returns an Array of entry Hashs in the same format as read_audit_log.
    def read_audit_log_by_offsets(offsets: [])
      if offsets.any?
        send_message(:read_audit_log_by_offsets, offsets)
      else
        []
      end
    end

    # Public: Get sha1sum of the audit log
    #
    # Returns the sha1sum as a string, or an error string in a form
    # convenient to dgit-diagnose.
    def sha1sum_audit_log
      send_message(:sha1sum_audit_log)
    end

    # Public: Get the time of the last line of the audit log
    #
    # Returns a Time object, or raises GitRPC::CommandFailed.
    def last_audit_log_time
      send_message(:last_audit_log_time)
    end

    # Public: Parse and page through all entries in the audit log.
    #
    # offset     - Integer byte offset to start reading from
    # batch_size - Integer maximum number of lines to read over the wire at time
    #
    # Returns Enumerator yielding Hash of parsed audit log entries.
    def enum_for_audit_log(**kargs)
      enum_for(:each_audit_log_entry, **kargs)
    end

    # Internal: Parse and page through all entries in the audit log.
    #
    # Prefer using public enum_for_audit_log API.
    #
    # offset     - Integer byte offset to start reading from
    # batch_size - Integer maximum number of lines to read over the wire at time
    #
    # Returns nothing.
    def each_audit_log_entry(offset: 0, batch_size: DEFAULT_AUDIT_LOG_LIMIT, &block)
      eof = false

      until eof
        lines, eof = send_message(:read_audit_log, offset, batch_size)
        lines.each(&block)

        if last = lines.last
          offset = last["byteoffset"] + last["bytesize"]
        end
      end

      nil
    end
  end
end
