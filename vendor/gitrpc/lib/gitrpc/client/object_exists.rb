# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Check if object exists in the repository.
    #
    # oid - String OID
    # type - "commit", "tree", "blob", "tag" or nil
    #
    # Returns true if object exists with specific type or false.
    def object_exists?(oid, type = nil)
      ensure_valid_full_sha1(oid)

      if backend.disagreement_possible?
        send_demux(:object_exists?, oid, type)
          .select { |route, answer| route.voting? }
          .map(&:last).all?
      else
        send_message(:object_exists?, oid, type)
      end
    end

    # Public: Check if a set of object exists in the repository.
    #
    # oid - Array of String OIDs
    #
    #     rpc.batch_check_objects_exist(["4bf7...", "bbf7..."])
    #     #=> {"4bf7..." => true, "bbf7..." => false}
    #
    # Returns a Hash of input String OIDs mapping to Boolean values.
    def batch_check_objects_exist(oids)
      oids.each { |oid| ensure_valid_full_sha1(oid) }
      send_message(:batch_check_objects_exist, oids)
    end
  end
end
