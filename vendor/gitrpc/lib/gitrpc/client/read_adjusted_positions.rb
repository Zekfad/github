# frozen_string_literal: true

require_relative "read_adjusted_positions/reducer"

module GitRPC
  class Client
    # Public: a convenience method acting the same as read_adjusted_positions only
    # for a single hash of blob oids and a position.
    #
    # position    - The 0 based positive integer position in the starting blob.
    # source      - The String oid of the blob that we are starting from.
    # destination - The String oid of the blob to which we are comparing.
    #
    # Returns an Integer or nil adjusted position in the second blob.
    def read_adjusted_position_with_base(position:, source:, destination:, **options)
      data = { position: position, source: source, destination: destination }
      read_adjusted_positions_with_base([data], **options).first
    end

    def read_adjusted_position(position, blob1_oid, blob2_oid, options = {})
      read_adjusted_position_with_base(position: position, source: blob1_oid, destination: blob2_oid, **options)
    end

    # Public: Given a set of positioning information, returns a corresponding set of
    #   adjusted_positions. Matching blob pairs are condensed to one calculation to
    #   improve performance.
    #
    # Example:
    #
    # Given the following diff:
    #
    # diff --git a/sample b/sample
    # index 8422d40..194bca4 100644
    # --- a/sample
    # +++ b/sample
    # @@ -1,4 +1,5 @@
    #  A
    #  B
    #  C
    # +C.2
    #  D
    #
    # adjusted_positions([[3, "8422d40", "194bca4"], [1, "8422d40", "194bca4"]])
    # => [4, 1]
    #
    # Parameters:
    # positioning_data - array of 3-tuples each tuple matching the signature of `read_adjusted_position`.
    #
    # Returns an array of Integer or nils equal in length to the `position_data` array.
    def read_adjusted_positions_with_base(positioning_data, skip_bad: false)
      reducer = ReadAdjustedPositions::Reducer.new(positioning_data, skip_bad: skip_bad)
      options = { "skip_bad" => skip_bad }
      reduced = reducer.reduced_data
      if reduced.empty?
        reducer.ordered_positions({})
      else
        adjusted_info = send_message(:read_adjusted_positions, reduced, options)
        reducer.ordered_positions(adjusted_info)
      end
    end

    def read_adjusted_positions(positioning_data, skip_bad: false)
      data = positioning_data.map do |position, start_oid, end_oid|
        { position: position, source: start_oid, destination: end_oid }
      end

      read_adjusted_positions_with_base(data, skip_bad: skip_bad)
    end

    # Public: a convenience method acting the same as read_commit_adjusted_positions but only
    #   for a single position.
    #
    # read_commit_adjusted_position(
    #     position: 3,
    #     source: {
    #       start_commit_oid: fff1,
    #       end_commit_oid:   eee1,
    #       base_commit_oid:  bbb1,
    #       path:             path1
    #     },
    #     destination: {
    #       start_commit_oid: fff2,
    #       end_commit_oid:   eee2,
    #       base_commit_oid:  bbb2,
    #       path:             path2
    #     })
    #
    # => 4
    #
    # position    - The Integer position of the line in source.
    # source      - A Hash containing the path and one of the following:
    #              1. The start_commit_oid, end_commit_oid, and base_commit_oid that can be resolved
    #                 into a single commit or proxy tree.
    #              2. The commit_oid on which the path resides
    # destination - Same as source only pointing to the destination tree or object.
    #
    # Note: the resolvable commit oids are usually used to determine the exact blob and tree
    #   used for the base of a diff. As comments on deletions are tied to the start of a diff
    #   which can be a "proxy tree", we need to be able to resolve them here as well.
    #
    # Returns an Integer or nil adjusted position in the second blob.
    def read_commit_adjusted_position_with_base(position:, source:, destination:, **options)
      positioning_data = [
        {
          position:    position,
          source:      source,
          destination: destination
        }
      ]
      read_commit_adjusted_positions_with_base(positioning_data, **options).first
    end

    def read_commit_adjusted_position(position, commit1_oid, commit2_oid, path1, path2, options = {})
      read_commit_adjusted_position_with_base(
        position: position,
        source: {
          commit_oid: commit1_oid,
          path:       path1
        },
        destination: {
          commit_oid: commit2_oid,
          path:       path2
        }, **options)
    end

    # Public: Given a set of positioning information, returns a corresponding set of
    #   adjusted_positions.
    #
    # Example:
    #
    # Given the diff described for read_adjusted_positions
    #
    # diff --git a/sample b/sample
    # index 8422d40..194bca4 100644
    # --- a/sample
    # +++ b/sample
    # @@ -1,4 +1,5 @@
    #  A
    #  B
    #  C
    # +C.2
    #  D
    #
    # path1 = path2 = "sample"
    #
    # read_commit_adjusted_positions(
    #   [
    #     {
    #       position: 3,
    #       source: {
    #         start_commit_oid: commit1_oid,
    #         end_commit_oid:   commit2_oid,
    #         base_commit_oid:  commit1_oid,
    #         path:             path1
    #       },
    #       destination: {
    #         start_commit_oid: commit1_oid,
    #         end_commit_oid:   commit2_oid,
    #         base_commit_oid:  commit1_oid,
    #         path:             path2
    #       }
    #     },
    #     {
    #       position: 1,
    #       source: {
    #         commit_oid: abc1,
    #         path:       path1
    #       },
    #       destination: {
    #         commit_oid: abc2,
    #         path: path2
    #       }
    #     }
    #     ...
    #   ])
    #
    # => [4, 1]
    #
    # Parameters:
    # positioning_data - array of hashes, each matching the signature of `read_commit_adjusted_position`.
    #
    # Returns an array of Integer, nils, or :bad equal in length to the `position_data` array.
    def read_commit_adjusted_positions_with_base(positioning_data, skip_bad: false)
      reducer = ReadAdjustedPositions::Reducer.new(positioning_data, skip_bad: skip_bad)
      options = { "skip_bad" => skip_bad }
      reduced = reducer.reduced_data
      if reduced.empty?
        reducer.ordered_positions({})
      else
        adjusted_info = send_message(:read_adjusted_positions, reduced, options)
        reducer.ordered_positions(adjusted_info)
      end
    end

    def read_commit_adjusted_positions(positioning_data, options = {})
      data = positioning_data.map do |position, commit1_oid, commit2_oid, path1, path2|
        {
          position: position,
          source: {
            commit_oid: commit1_oid,
            path:       path1
          },
          destination: {
            commit_oid: commit2_oid,
            path:       path2
          }
        }
      end
      read_commit_adjusted_positions_with_base(data, **options)
    end
  end
end
