# rubocop:disable Style/FrozenStringLiteralComment

# Public: get resque worker count
#
# Returns an integer.
module GitRPC
  class Client
    def resque_worker_count
      send_message(:resque_worker_count)
    end
  end
end
