# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client

    # Public: Fetch a summary of a single tree in this Repository. The result
    # is an Array of blob Hashes containing the blob name, oid, size, etc. The
    # actual blob data is **not** returned by this method.
    #
    # tree_id - The 40 char sha1 oid of the tree (not a commit oid).
    # path    - String path to a subtree from the root of the tree. This
    #           must not start with a slash (/) and is always relative
    #           to the root of the repository.
    #
    # Returns an Array of Hashes:
    #
    #     { 'oid'   => 40 char sha1 oid of the blob,
    #       'name'  => filename (not including the path prefix),
    #       'size'  => the untruncated size of the blob,
    #       'mode'  => the integer filemode of the blob }
    #
    def read_tree_blobs(tree_id, path = nil)
      ensure_valid_full_sha1(tree_id)

      path = read_tree_blobs_path_normalize(path)
      cache_key = content_cache_key("read_tree_blobs", tree_id, md5(path.to_s))

      @cache.fetch(cache_key) do
        send_message(:read_tree_blobs, tree_id, path)
      end
    end

    # Public: Fetch a summary of all trees in this Repository. The result
    # is an Array of blob Hashes containing the blob name, oid, size, etc. The
    # actual blob data is **not** returned by this method.
    #
    # tree_id - The 40 char sha1 oid of the tree (not a commit oid).
    #
    # Returns an Array of Hashes:
    #
    #     { 'oid'   => 40 char sha1 oid of the blob,
    #       'name'  => filename (including the path prefix),
    #       'size'  => the untruncated size of the blob,
    #       'mode'  => the integer filemode of the blob }
    #
    def read_tree_blobs_recursive(tree_id)
      ensure_valid_full_sha1(tree_id)

      cache_key = content_cache_key("read_tree_blobs_recursive", tree_id)

      @cache.fetch(cache_key) do
        send_message(:read_tree_blobs_recursive, tree_id)
      end
    end

    # Public: Fetch a summary of all trees in this Repository and return the
    # results in "pages" of fixed size. All the blobs in the tree can be
    # returned by requestign subsequent pages of data. The returned blobs
    # contain the name, oid, size, and file mode of the blob. The actual blob
    # data is **not** returned by this method.
    #
    # tree_id - The 40 char sha1 oid of the tree (not a commit oid).
    # page    - The page number (zero-based).
    #
    # Returns a Hash
    #
    #   { 'current_page' => current page number (zero based),
    #     'next_page'    => next page number or nil if this is the last page,
    #     'blobs'        => Array of blob Hashes }
    #
    def read_tree_blobs_by_page(tree_id, page)
      ensure_valid_full_sha1(tree_id)

      cache_key = content_cache_key("read_tree_blobs_by_page", tree_id, page)

      @cache.fetch(cache_key) do
        send_message(:read_tree_blobs_by_page, tree_id, page)
      end
    end

    # Internal: Normalizes a path into a format acceptable to git. This will
    # remove leading slashes and convert empty or '.' paths to nil.
    #
    # path - The path String or nil.
    #
    # Return the normalized path String or nil.
    def read_tree_blobs_path_normalize(path)
      return if path.nil?

      path = path.sub(/^\//, "")
      path = nil if path.empty? || path == "."
      path
    end

  end  # Client
end  # GitRPC
