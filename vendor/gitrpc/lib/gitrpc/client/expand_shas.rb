# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Expand an array of short SHAs (of any length, but at least 7 characters)
    # to their full value.
    #
    # Returns a hash of `{ short_sha => full_sha }` for all the SHAs that were
    # found in the repository. SHAs that were not found or that had a different
    # type than `expected_type` will not be included in the hash.
    #
    # sha_list - an Array of SHA strings
    # expected_type - String, expected object type for the SHAs; if nil, all shas
    # will be expanded regardless of their type
    #
    # Returns the Array with the SHAs that don't exist removed
    def expand_shas(sha_list, expected_type = nil)
      send_message(:expand_shas, sha_list, expected_type)
    end

    # Find out whether a given SHA exists in the repository
    #
    # sha - a String representing a SHA1
    #
    # Returns true or false
    def sha_valid?(sha)
      ensure_valid_full_sha1(sha)

      cache_key = repository_cache_key("sha_valid", sha)
      @cache.fetch(cache_key) do
        send_message(:sha_valid?, sha)
      end
    end
  end
end
