# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def read_wiki_pages(oid, directory_prefix)
      if !valid_full_sha1?(oid)
        raise ::GitRPC::InvalidFullOid, "wrong argument type #{oid.inspect} (expected 40c String OID)"
      end

      hashed_prefix = ::Digest::MD5.hexdigest(directory_prefix)
      wiki_key = content_cache_key("wiki-pages", oid, hashed_prefix, "v3")
      @cache.fetch(wiki_key) do
        send_message(:read_wiki_pages, oid, directory_prefix.b)
      end
    end

    def read_latest_wiki_pages(oid)
      if !valid_full_sha1?(oid)
        raise ::GitRPC::InvalidFullOid, "wrong argument type #{oid.inspect} (expected 40c String OID)"
      end

      wiki_key = content_cache_key("latest-wiki-pages", oid, "v3")
      @cache.fetch(wiki_key) do
        entries = blame_tree(oid)

        commit_oids = []
        entries.select! do |path, commit_oid|
          filename = ::File.basename(path)
          if GitRPC::Backend.valid_wiki_page?(filename)
            commit_oids << commit_oid
          end
        end

        commits = {}
        read_commits(commit_oids.uniq).each do |commit|
          commits[commit["oid"]] = commit
        end

        pages = []
        entries.each do |path, commit_oid|
          commit = commits[commit_oid]
          pages << [path, commit]
        end

        pages.sort do |entry_a, entry_b|
          time1 = Time.at(*entry_a[1]["author"][2])
          time2 = Time.at(*entry_b[1]["author"][2])

          time2 <=> time1
        end

        pages.map do |entry, commit|
          [entry, commit["oid"]]
        end
      end
    end
  end
end
