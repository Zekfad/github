# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    include GitRPC::Util

    # Public: Retrieve ref name to oid mapping from the repository. This includes
    # branches, tags, and any other special refs (when not filtered). Signed tags
    # are dereferenced so that the resulting value is the oid of the commit, not
    # the oid of the tag object.
    #
    # filter - String specifying which set of refs should be returned. "default"
    #          means that only heads and tags will be returned. "extended" returns
    #          refs that would show up in `git ls-remote`. "all" returns all of the
    #          refs on the fileserver, including some that are only intended to be
    #          used inside of this app.
    #
    # Returns a hash of fully qualified ref name strings to commit oid mappings.
    def read_refs(filter = "default")
      filter = update_boolean_read_refs_filter(filter)
      cache_key = refs_key(filter)
      @cache.fetch(cache_key) do
        refs = send_message(:read_refs, filter)
        GitRPC.instrument(:set_refs_cache, refs: refs, key: cache_key, cache: @cache)
        refs
      end
    end

    # Internal: Cache key used to store the main refs hash.
    def refs_key(filter = "default")
      repository_reference_cache_key("refs", "v4", filter.to_s)
    end

    # Internal: All of the possible refs_keys
    def refs_keys
      [true, false, "default", "extended", "all"].map { |filter| refs_key(filter) }
    end

    # Public: Read a list of fully qualified refs
    #
    # names - Array of fully qualified ref names
    #
    # Returns an Array with pairs of fully qualified refnames and target commit OID
    def read_qualified_refs(ref_names)
      ref_name_to_cache_key = ref_names.map { |ref_name|
        [ref_name, qualified_ref_cache_key(ref_name)]
      }.to_h

      cached_refs = {}
      missing_ref_names = []

      target_oids_from_cache = @cache.get_multi(ref_name_to_cache_key.values)
      ref_names.each do |ref_name|
        cache_key = ref_name_to_cache_key[ref_name]
        if target_oids_from_cache.has_key?(cache_key)
          cached_refs[ref_name] = target_oids_from_cache[cache_key]
        else
          missing_ref_names << ref_name
        end
      end

      if missing_ref_names.any?
        target_oids = send_message(:read_qualified_refs, missing_ref_names)
        missing_ref_names.zip(target_oids) do |ref_name, target_oid|
          cached_refs[ref_name] = target_oid
          @cache.set(ref_name_to_cache_key[ref_name], target_oid)
        end
      end

      ref_names.zip(cached_refs.values_at(*ref_names))
    end

    private def qualified_ref_cache_key(name)
      repository_reference_cache_key("read_qualified_ref", "v1", Digest::SHA1.hexdigest(name))
    end

    # Public: Return the ref name that most closely matches the path
    #
    # path - the path, typically of the form ref_name/path_string
    # limit - Integer indicating the maximum length of ref names
    #
    # Returns a two-element Array consisting of fully qualified refname and
    # target commit OID, or nil if no ref name was found.
    def read_ref_from_path(path, limit: nil)
      path = limit_path(path.b, limit) if limit
      ref_names = candidate_ref_names_from_path(path.b)

      ref_name_to_cache_key = ref_names.map { |ref_name|
        [ref_name, qualified_ref_cache_key(ref_name)]
      }.to_h

      target_oids_from_cache = @cache.get_multi(ref_name_to_cache_key.values)
      ref_names.each do |ref_name|
        cache_key = ref_name_to_cache_key[ref_name]

        # check if cache is missing refs
        break unless target_oids_from_cache.key?(cache_key)

        # only return oids for refs that actually exist
        target_oid = target_oids_from_cache[cache_key]
        next unless target_oid

        return [ref_name, target_oid]
      end

      # Rebuild cache with missing refs
      target_oids = send_message(:read_qualified_refs, ref_names)
      return if target_oids.empty?

      ref_names.zip(target_oids) do |ref_name, target_oid|
        next unless target_oid

        @cache.set(ref_name_to_cache_key[ref_name], target_oid)
        return [ref_name, target_oid]
      end

      nil
    end

    # Internal: Limit path by given limit
    #
    # Returns String
    private def limit_path(path, limit)
      return path if path.bytesize <= limit

      sliced_path = path.byteslice(0, limit)
      return sliced_path if path[limit] == "/"

      sliced_path.gsub(%r{\/[^\/]*\z}, "")
    end

    # Internal: Splits up path into potential ref names
    #
    # Returns Array<String>
    private def candidate_ref_names_from_path(path)
      candidate_ref_names = []

      path.each_char.with_index do |char, index|
        next if char != "/"
        candidate_ref_names << path[0...index]
      end
      candidate_ref_names << path

      candidate_ref_names.reverse.flat_map do |candidate_ref_name|
        ["refs/heads/#{candidate_ref_name}", "refs/tags/#{candidate_ref_name}"]
      end
    end

    # Public: Number of refs, branches and tags
    #
    # Returns Hash
    def ref_counts
      cache_key = repository_reference_cache_key("ref_counts", "v1")
      @cache.fetch(cache_key) do
        send_message(:ref_counts)
      end
    end

    # Public: Read the oid of the HEAD ref
    #
    # Returns a sha1
    def read_head_oid
      cache_key = repository_reference_cache_key("read_head_oid", "v1")
      @cache.fetch(cache_key) do
        send_message(:read_head_oid)
      end
    end

    # Public: list references
    #
    # This is basically a subset of read_refs (it returns only ref
    # names, not oids), but it doesn't filter out remote and hidden
    # branches like read_refs does.
    #
    # Returns an array of strings.
    def list_refs
      send_message(:list_refs)
    end

    # Public: list branch names and dates the way BranchFinder likes them
    #
    # Returns an array of [date, refname] arrays.
    def raw_branch_names_and_dates
      send_message(:raw_branch_names_and_dates)
    end
  end
end
