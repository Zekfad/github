# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Write a blob into the repository
    def write_blob(content)
      send_message(:write_blob, content)
    end
  end
end
