# rubocop:disable Style/FrozenStringLiteralComment
require "bert"

module BERT
  def self.encode(ruby)
    response = GitRPC.instrument(:bert_encode) do
      ::BERT::Encoder.encode(ruby)
    end
    GitRPC.instrument(:bert_encode_size, :size => response.bytesize)
    response
  end

  def self.decode(bert)
    GitRPC.instrument(:bert_decode_size, :size => bert.bytesize)
    GitRPC.instrument(:bert_decode) do
      ::BERT::Decoder.decode(bert)
    end
  end
end
