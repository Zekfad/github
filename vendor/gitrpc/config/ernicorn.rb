# Example Ernicorn configuration file for GitRPC
#
# Most Unicorn config options are supported:
#
#    http://unicorn.bogomips.org/Unicorn/Configurator.html
#
# Example unicorn config files:
#
#   http://unicorn.bogomips.org/examples/unicorn.conf.rb
#   http://unicorn.bogomips.org/examples/unicorn.conf.minimal.rb

# load server code and expose modules
require "gitrpc"
require "gitrpc/protocol/bertrpc"
require "rugged"
Ernicorn.expose(:gitrpc, GitRPC::Protocol::BERTRPCServer)

# server options
port = GitRPC::Protocol::BERTRPC::PORT
listen port
worker_processes 2
working_directory File.expand_path("../..", __FILE__)

# limit mmap memory
Rugged::Settings["mwindow_size"] = 32 * 1024 * 1024
Rugged::Settings["mwindow_mapped_limit"] = 64 * 1024 * 1024

# tell 'em
puts "Starting server on port #{port} ..."
puts "Here's a GitRPC URL you can use for testing:"
puts "bertrpc://localhost:#{port}#{File.expand_path('../../.git', __FILE__)}"

# turn on BERTRPC call logging
Ernicorn.loglevel 1

# enable COW where possible
GC.respond_to?(:copy_on_write_friendly=) &&
  GC.copy_on_write_friendly = true

# hook into new child immediately after forking
after_fork  do |server, worker|
end

# hook into master immediately before forking a worker
before_fork do |server, worker|
end
