GitRPC
======

A network oriented library for accessing remote Git repositories reliably and
efficiently. GitRPC is a recasting of the many independent components that make
up GitHub's application git stack into a single cohesive library and set of
abstractions.

  - Optimized for efficient access over a network.
  - Small core RPC interface designed for batch operation.
  - Simple objects only: hashes, arrays, and scalar types.
  - Layered design (Client → Cache → Protocol → Backend).
  - Aggressive client side caching (memcached).
  - [Rugged](https://github.com/libgit2/rugged) (libgit2) git access.

See [RATIONALE][R] for more on the history behind this project and its goals.

[R]: https://github.com/github/github/blob/master/vendor/gitrpc/RATIONALE.md

Status
------

GitRPC is currently under heavy pull request driven development. If you're
interested in coming up to speed, consider reviewing our progress so far:

 - [#2](https://github.com/github/gitrpc/pull/2) Basic development
   environment, `GitRPC::Backend`, and project roadmap discussion.
 - [#3](https://github.com/github/gitrpc/pull/3) Ports `Backend#refs` from
   native commands to Rugged.
 - [#4](https://github.com/github/gitrpc/pull/4) `GitRPC::Client` and basic
   caching strategy.
 - [#5](https://github.com/github/gitrpc/pull/5) Ports `Backend#read_commits`
   from native commands to Rugged.
 - [#9](https://github.com/github/gitrpc/pull/9) `GitRPC.new`, general library
   options, and usage documentation.
 - [#8](https://github.com/github/gitrpc/pull/8) Kicks off the basic framework
   for URL protocol resolvers and implements `file:` URLs.
 - [#10](https://github.com/github/gitrpc/pull/10) The `bertrpc:` protocol
   implementation and dealing with remote failures / exceptions.
 - [#11](https://github.com/github/gitrpc/pull/11) The `chimney:` protocol
   implementation / looking up repo hosts in redis.
 - [#19](https://github.com/github/gitrpc/pull/19) Adds the `list_revision_history`
   RPC call.
 - [#20](https://github.com/github/gitrpc/pull/20) Adds the `init` and `exists?`
   checks for creating repos remotely.
 - [#21](https://github.com/github/gitrpc/pull/21) Adds support for publishing
   `ActiveSupport::Notifications` style instrumentation events.

At this point most of the core framework is stablizing and the focus is on
implementing RPC calls needed by Gist 2.0 and github.com.

Basic Usage
-----------

Get this party started:

    >> require 'gitrpc'
    >> git = GitRPC.new('file:/path/to/repository.git')
    #<GitRPC::Client:0x10c... @backend=...>

Optionally enable git object caching:

    >> require 'dalli'
    >> GitRPC.cache = Dalli::Client.new('localhost:11211')

Retrieve all refs for the repository in a single hash (cached: 1 netop):

    >> git.read_refs
    { 'refs/heads/master' => 'deadbeee...', 'refs/head/some-branch' => ... }

Resolve a complex revision down to a sha1 object id (not cached):

    >> git.rev_parse('master@{2.days.ago}')
    'deadbeee...'

Read commit metadata in batch and nothing else (cached: 1 netop):

    >> git.read_commits(['deadbeee...', 'badfffff...'])
    [{"type"      => "commit",
      "oid"       => "d3224d5...",
      "parents"   => ["fd1545cc..."],
      "tree"      => "6d1470d...",
      "author"    => ["Ryan Tomayko", "ryan@github.com", "2012-06-13T03:29:30Z"],
      "committer" => ["Ryan Tomayko", "ryan@github.com", "2012-06-13T03:29:30Z"],
      "message"   => ":encoding docs",
      "encoding"  => "UTF-8"},
     {"type"      => "commit",
      "oid"       => "abcdef0...",
      ...}

Connect to a repository on a remote machine via BERTRPC:

    >> git = GitRPC.new('bertrpc://example.com:8175/path/to/repository.git')
    >> git.rev_parse('HEAD')
    'deadbeee...'
    >> git.refs
    { ... }

Call Reference Documentation
----------------------------

All public calls are documented individually here:

https://github.com/github/github/tree/master/vendor/gitrpc/doc

Architecture
------------

GitRPC is designed to work in both large distributed environments with
multi-machine storage / cache clusters as well as entirely locally
using the same basic interfaces and with the same error modes.

The architecture is enabled by separating operations into three distinct layers:
from bottom up they are the **Backend**, **Protocol**, and **Client** layers.

### Backend

**GitRPC::Backend** is where local git repository access is performed. Operations
are modeled as a simple flat namespace of RPC method invocations on this object.

Most methods are implemented with Rugged (libgit2) for its rich API and to
minimize process creation overhead. Fast native git command spawning is also
available.

See [GitRPC::Backend][b] for more information.

[b]: https://github.com/github/gitrpc/tree/master/lib/gitrpc/backend.rb

### Protocol

Access to the **Backend** is mediated by one or more **GitRPC::Protocol**
implementations. Local and remote access to repositories is enabled through a
system of pluggable URL resolvers:

    GitRPC.new("file:///data/repositories/example.git")
    GitRPC.new("bertrpc://fs1.rs.github.com:8195/rtomayko/example.git")
    GitRPC.new("chimney:rtomayko/example.git")
    GitRPC.new("http://github.com/rtomayko/example.git")

See [GitRPC::Protocol][p] for more information on protocol responsibilities. In
addition to resolving URLs, protocol implementations also handle RPC
encoded/decoding, network access, and translating error modes.

Some calls are also available via the Twirp RPC protocol[twirp].

[p]: https://github.com/github/gitrpc/tree/master/lib/gitrpc/protocol.rb
[twirp]: doc/twirp.md

### Client

The **GitRPC::Client** implements network frontend logic for all public
**Backend** methods. This part of the library is always available on the client
regardless of whether the **Backend** is being accessed remotely.

`GitRPC.new` returns a **Client** object so this is also the main interface for
calling code.

All public **Backend** methods must be explicitly implemented on the **Client**
interface. There is no `method_missing` style catchall that exposes all backend
calls by default. This is to encourage thinking through the caching and network
access patterns for all git access.

Where possible, **Client** method implementations are designed to avoid
communication with the **Backend** fully or partially through aggressive caching
and short circuiting logic.

See [GitRPC::Client][c] for more information on this layer.

[c]: https://github.com/github/gitrpc/tree/master/lib/gitrpc/client.rb

Contributing
------------

### Setup

To set up your environment bundle for development and running tests:

    $ script/bootstrap --local

To run tests:

    $ script/cibuild

To run individual tests:

    $ script/cibuild test/client/disk_test.rb [...]

Note that some tests fail if IPv6 is enabled. A workaround is to remove `::1`
from the `localhost` line in your `/etc/hosts` file (i.e., leaving only
`127.0.0.1`).

### Pull Requests

For simple bug fixes and small tweaks, submit a pull request.

### New calls

See the [CONTRIBUTING][T] file for details on adding new calls. Each call
requires a client and backend implementation, tests, and documentation. The
CONTRIBUTING file includes templates and examples.

[T]: https://github.com/github/gitrpc/tree/master/CONTRIBUTING.md
