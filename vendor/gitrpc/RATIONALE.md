# History, Context, and Contours

GitHub has used [Grit](https://github.com/mojombo/grit) for git repository
access since its inception. Grit was created in a time when all repository
access was local and its design reflects this. Grit is an OO abstraction over
Git's microformats and access patterns designed foremost to be easy to
understand conceptually for the programmer in an OO environment.

GitHub moved to a distributed network approach by strapping a light RPC layer
named [Smoke][3] on top of Grit. An important trait of Smoke was that it was a
mostly transparent layer over Grit as far as calling code was concerned. This
let the GitHub codebase move quickly from a single machine architecture on EY to
a network architecture on Rackspace. Smoke shimmed into Grit's existing
interfaces so existing calling code "Just Worked".

But this is a lie. Network applications must consider the network:

  - Care must be taken to minimize network round trips. Latency is not zero.
    Loading information in batch is preferable to loading small pieces
    individually.
  - Caching CPU or disk intense operations is often needed.
  - You have to worry about how much data is going over the wire. Partial
    responses (say after a timeout period) can be useful.
  - You need to handle various types of network failures.
  - For many operations, it's beneficial to position specific parts of the
    implementation on the client and other parts on the server. At the very
    least, it should be obvious on which side of the network a piece of code
    will be run.

Not only are Grit and Smoke not designed to help with these issues, they in many
ways complicate and obscure them. Two new components were created to deal with
this:

  - [Walker: Texas Ranger][2], which is the real interface used to access repositories
    on github.com. It's main thing is caching but it also provides a custom set
    of data objects. These are designed foremost to be lightweight and
    serializeable and sacrifice many of the OOP features in Grit's objects. Most
    GitHub features are built on these Walker interfaces and objects: tree, blob,
    commit, compare, pull requests, etc. all use Walker objects instead of Grit
    objects but rely on Smoke/Grit for network access.

    Also, it's called "Walker".

  - Smoke Grit [extensions][4] and [commands][5]. These are server-side methods
    added to `Grit::Git` or custom git commands. They perform multiple git
    operations in a single network round trip or work on large amounts of data
    locally, nearer to the repository, so that it doesn't need to be transfered
    over the network. These things are hacked into the Smoke library in a way
    that doesn't make sense at all. It's mostly github.com application logic
    sitting in the middle of a general purpose RPC library. I hate it.

The Walker, Smoke, Grit system is not designed for reuse. Walker is tied to
GitHub application models. Attempts to use the full Git stack in other projects
have failed. Gist reimplements portions of Walker and it's part of the same
project.

It's time to burn all of this down and consider how best to build a network
oriented Git interface. This library will encompass the functionality of the
current Walker, Smoke/Chimney, and Grit components.

[1]: https://github.com/mojombo/grit
[2]: https://github.com/github/github/blob/c81c6d283553b02e9a2ff31535a2f9a12c6ef0e5/lib/walker.rb
[3]: https://github.com/github/smoke
[4]: https://github.com/github/github/blob/master/vendor/internal-gems/smoke/lib/smoke/git.rb
[5]: https://github.com/github/github/tree/master/vendor/internal-gems/smoke/lib/smoke/git-bin
