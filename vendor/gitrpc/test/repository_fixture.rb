# frozen_string_literal: true
require "fileutils"
require "posix/spawn"
require "rugged"

# Utilities for setting up a git repo directory for tests.
class RepositoryFixture
  include FileUtils

  EMPTY_TREE_OID = "4b825dc642cb6eb9a060e54bf8d69288fbee4904"

  def self.sample(fixture_name)
    cache = GitRPC::Util::HashCache.new
    path = File.expand_path("../examples/#{fixture_name}.git", __FILE__)
    GitRPC.new("fakerpc:#{path}", :cache => cache)
  end

  attr_accessor :last_rugged_commit_oid

  # Full path to the test repository on disk.
  attr_reader :path

  # Create a new fixture repository object. The new object instance can be used
  # to setup and teardown the repository fixture and perform common operations
  # like adding test commits or executing git commands.
  #
  # path - Where to create the test repo on disk. If this is a relative path,
  #        the repository is created beside this file under the test/ directory.
  #
  def initialize(path = "test.git")
    fail "refusing to modify #{path}" if (path.start_with?("/") && path.split("/").size <= 3)
    if path.include?("/")
      @path = path
    else
      @path = File.expand_path("../#{path}", __FILE__)
    end

    @last_rugged_commit_oid = nil

    teardown
  end

  # Creates a test repository at the given path. The repository is initialized
  # as a bare repo but left empty.
  def setup
    teardown
    create_directory
    init
  end

  # Removes a test repository from disk.
  def teardown
    rm_rf(@path) if File.directory?(@path)
  end

  def create_directory
    mkdir_p(@path)
  end

  # Initialize the bare repository using a minimal template to speed things up a
  # bit.
  def init
    template = File.expand_path("../repository_fixture_template", __FILE__)
    command "git init -q --bare --template='#{template}'"
  end

  # Get the Rugged::Repository instance.
  def rugged
    @rugged ||= Rugged::Repository.new(@path)
  end

  # Add an empty commit to the current HEAD branch.
  #
  # message   - Commit message to use.
  # options   - Hash with :name and :email to be used
  #             as the author info for the commit
  #
  # Returns the String commit OID
  def commit_changes(message, options = {})
    name  = options[:name]  || "Some Author"
    email = options[:email] || "someone@example.org"
    commit_time = options[:commit_time] || "1342581713 -0700"
    gpg_config = options[:gpg] ? "-c commit.gpgsign=true" : "-c commit.gpgsign=false"

    command "
      set -e
      export GIT_COMMITTER_NAME='#{name}'
      export GIT_COMMITTER_EMAIL='#{email}'
      export GIT_COMMITTER_DATE='#{commit_time}'
      export GIT_AUTHOR_NAME='#{name}'
      export GIT_AUTHOR_EMAIL='#{email}'
      export GIT_AUTHOR_DATE='#{commit_time}'
      git #{gpg_config} --work-tree='.' commit -q --allow-empty -m '#{message}'
    "

    rev_parse("HEAD")
  end
  alias add_empty_commit commit_changes

  # Public: The same as add_empty_commit only it acts on the repository using rugged
  #         so there is no command line interpretation of strings.
  def add_simple_empty_commit(message, email: "someone@example.org",
                                       name:  "Some Author",
                                       time:  Time.now)
    rugged_commit_files(nil, message, email: email, name: name, time: time)
  end

  # Add a commit which adds a submodule to the project
  #
  # target  - the git repo to add
  # path    - the path to link to
  # message - the commit message
  #
  # Returns the oid of the commit
  def add_submodule_commit(target, sub_path, message)
    work_tree = "#{@path}/work"
    mkdir_p work_tree
    Dir.chdir(work_tree) do
      `git --git-dir='#{@path}' --work-tree='#{work_tree}' submodule add #{target} #{sub_path} > /dev/null 2>&1`
    end
    commit_changes message
  end

  # Create an empty commit. The commit will be built from scratch (using
  # `git hash-object`) instead of using one of Git's commit-building commands
  # (like `git commit` or `git commit-tree`). Among other things, this allows
  # for the creation of garbage commits for testing errors.
  #
  # message        - Commit message to use.
  # author_line    - Signature for the author. See signature details below.
  # committer_line - Signature for the committer. See signature details below.
  #
  # The committer's signature should follow this format:
  #
  #   committer Firstname Lastname <email@address.com> unixtimestamp +HHMM
  #
  # Real life example:
  #
  #   committer Jake Boxer <jake@github.com> 1362610527 -0800
  #
  # The author's signature should be the exact same format, except start with
  # "author" instead of "committer". Example:
  #
  #   author Jake Boxer <jake@github.com> 1362610527 -0800
  #
  # Returns the new commit's oid.
  def add_empty_raw_commit(message = nil, author_line = nil, committer_line = nil)
    message        ||= "message for raw commit"
    author_line    ||= "author Some Author <someone@example.org> 1342581713 -0700"
    committer_line ||= "committer Some Author <someone@example.org> 1342581713 -0700"

    work_tree = "#{@path}/work"

    mkdir_p work_tree

    # Create an empty tree
    tree_oid = command("git --work-tree='#{work_tree}' write-tree").strip

    # Build the raw commit
    input = [
      "tree #{tree_oid}",
      author_line,
      committer_line,
      "",
      message
    ].join("\n")

    # Write the raw commit to a file
    path = File.join(work_tree, "garbage_commit.txt")
    File.open(path, "w") { |f| f.write(input) }

    # Add the raw commit to the database
    command("git --work-tree='#{work_tree}' hash-object -w -t commit #{path}").strip
  end

  def create_tag_object(name, message, oid, object_type, tagger = "Some Author <someone@example.org> 1342581713 -0700")
    input = [
      "object #{oid}",
      "type #{object_type}",
      "tag #{name}"
    ]

    input << "tagger #{tagger}" if tagger

    if message
      input << ""
      input << message
    end

    input << ""

    child =
      POSIX::Spawn::Child.new(
        "git", "--git-dir", @path, "hash-object", "-t", "tag", "-w", "--stdin",
        :input => input.join("\n")
      )
    if !child.success?
      raise child.err
    else
      child.out.strip
    end
  end

  FAKE_SIGNATURE_DATA = [
    "iQEcBAABAgAGBQJPBOcQAAoJEHm+PkMAQRiG55UH/ibkoyesorxV998bDzyb8eX3",
    "Y5+v/q8gdC27fGBmTOdROeGelxNafn/Ryr74aHY62VkuIWXfoL9vWD3ehCOKBcgV",
    "7krvEAvtUu6W1oxdGNLemTCAy4G9pjlnipW1I6yVz+MQe/3oGdNIjPgeUhcq87EM",
    "oEHnhWxuvLMere98m8qG4uUnzpPWIN2fIR00eRW9wx/DuquN/351AvcdAqV8UZFl",
    "UtlO+geZgB4xl5nqo3AmEsZK2kVZ7/Hlqb8feXqOWg81cJiZEgeW1ZfyIObPc4z+",
    "diUBSGY8LSnkM9ZH4I2aGj+ycamlzTNJd6GVQ4gI+TVsjf9j6p/0UsyqiE076K8=",
    "=T+90"
  ].join("\n")

  FAKE_SIGNATURE = [
    "-----BEGIN PGP SIGNATURE-----",
    FAKE_SIGNATURE_DATA,
    "-----END PGP SIGNATURE-----"
  ].join("\n")

  def create_signed_tag_object(name, message, oid, object_type)
    create_tag_object(name, "#{message}\n#{FAKE_SIGNATURE}", oid, object_type)
  end

  # Grab the oid for the object at name.
  def rev_parse(name)
    `git --git-dir='#{@path}' rev-parse '#{name}'`.strip
  end

  # Grab the list of commit oids for a history.
  def rev_list(name, count)
    `git --git-dir='#{@path}' rev-list -n #{count} '#{name}'`.split("\n")
  end

  # Stages a set of files in the index.
  #
  # files_hash - Hash of path name => content strings.
  def stage_files(files_hash)
    files_hash.each do |filename, contents|
      root, basename = File.split(filename)

      if root !~ /\.\//
        dir = File.join(work_tree, root)
        mkdir_p dir
        path = File.join(dir, basename)
      else
        path = File.join(work_tree, filename)
      end

      if contents
        File.open(path, "wb") { |f| f.write(contents) }
      else
        FileUtils.rm(path)
      end
    end

    command "git --work-tree='#{work_tree}' add -A"
  end

  # Writes a set of files in a new commit.
  #
  # files_hash - Hash of path name => content strings.
  # message    - The commit message
  # options    - Optional parameters to the commit, including
  #              :branch which defaults to 'master', :name and
  #              :email to specific author info for the commit
  #
  # Returns the commit oid of the new commit.
  def commit_files(files_hash, message, options = {})
    branch = options[:branch] || "master"
    old_branch = nil

    with_work_tree(branch) do
      stage_files files_hash
      commit_changes message, options
      rev_parse("HEAD")
    end
  end

  def rugged_commit_files(files_hash, message, email:   "someone@example.org",
                                               name:    "Some Author",
                                               time:    Time.now,
                                               branch:  nil,
                                               qualified_ref: nil,
                                               parents: current_default_parents)

    author = { email: email, name: name, time: time }

    if parents.count == 1 && last_rugged_commit_oid && parents.first != last_rugged_commit_oid
      parent = rugged.lookup(parents.first)
      rugged.index.read_tree(parent.tree)
    elsif parents.empty?
      rugged.index.clear
    end

    tree_oid = write_tree(files_hash)

    opts = {
      author:    author,
      message:   message,
      parents:   parents,
      committer: author,
      tree:      tree_oid
    }

    commit_oid = Rugged::Commit.create(rugged, opts)

    qualified_ref = "refs/heads/#{branch}" if branch && qualified_ref.nil?

    if qualified_ref
      if rugged.references.exist?(qualified_ref)
        rugged.references.update(qualified_ref, commit_oid)
      else
        rugged.references.create(qualified_ref, commit_oid)
      end
    end

    self.last_rugged_commit_oid = commit_oid
  end

  def current_default_parents
    [rugged.head.target&.oid]
  rescue Rugged::ReferenceError
    []
  end

  def write_tree(files_hash)
    return EMPTY_TREE_OID if files_hash.nil? || files_hash.empty?

    index = rugged.index

    mkdir_p(work_tree)
    rugged.workdir = work_tree

    files_hash.each_pair do |path, contents|
      File.write(File.join(rugged.workdir, path), contents)
      index.add path
    end

    index.write_tree
  end

  def with_work_tree(branch)
    unless command("git branch").empty?
      old_branch = command "git name-rev --name-only HEAD"
      command "git symbolic-ref HEAD refs/heads/#{branch}"
    end

    mkdir_p(work_tree)
    yield
  ensure
    if old_branch
      command "git --work-tree='#{work_tree}' checkout #{old_branch}" rescue RuntimeError
      command "git --work-tree='#{work_tree}' clean -f"
    end
  end

  def work_tree
    "#{path}/work"
  end

  # Merge the given branch into another
  #
  # branch - name of the branch to merge
  #
  # Returns the oid of the merge commit
  def merge(branch, into: "master")
    with_work_tree(into) do
      command "git --work-tree='#{work_tree}' merge #{branch}"
      rev_parse "HEAD"
    end
  end

  # Create a new branch to commit to
  #
  # name - The name of the branch to create.
  #
  # Returns nothing
  def create_branch(name)
    command "git branch #{name}"
  end

  # Delete a branch
  #
  # name - The name of the branch to delete.
  #
  # Returns nothing
  def delete_branch(name)
    command "git branch -D #{name}"
  end

  # Delete all branches
  #
  # Returns nothing
  def delete_all_branches
    delete_all_refs("refs/heads")
  end

  # Delete all refs
  #
  # Returns nothing
  def delete_all_refs(prefix = "")
    command "git for-each-ref --format='delete %(refname)' #{prefix} | git update-ref --stdin"
  end

  def commit_symlink(src, dest, message)
    work_tree = "#{@path}/work"
    mkdir_p work_tree

    ln_s src, File.expand_path(dest, work_tree)

    command "git --work-tree='#{work_tree}' add -A"
    commit_changes message

    rev_parse("HEAD")
  end

  # Get the repository's Rugged::Config instance. This is instantiated each
  # time so that current config settings and values are available; otherwise
  # config.get may return stale data.
  def config
    Rugged::Config.new(File.join(rugged.path, "config"))
  end

  # Execute a shell command within the context of a repository. The command may
  # span multiple lines.
  #
  # sh - String shell text to execute with the system /bin/sh
  #
  # Returns nothing.
  def command(sh)
    child = POSIX::Spawn::Child.new("/bin/sh", "-c", sh, :chdir => @path)
    if child.success?
      child.out
    else
      fail "command failed: #{child.err}"
    end
  end
end
