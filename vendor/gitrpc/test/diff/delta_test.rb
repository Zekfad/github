# frozen_string_literal: true
require_relative "../setup"
require "gitrpc"

class DiffDeltaTest < Minitest::Test
  def test_path
    delta = GitRPC::Diff::Delta.new(
      status: "A",
      similarity: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    )

    assert_equal "foo", delta.path

    delta = GitRPC::Diff::Delta.new(
      status: "A",
      similarity: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo2"
      }
    )

    assert_equal "foo2", delta.path
  end
end
