# frozen_string_literal: true
require_relative "../../setup"

require "gitrpc"

class DiffSummaryDeltaTest < Minitest::Test
  def setup
    @summary_delta = GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    )
  end

  def test_equality_to_summary_delta
    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    ), @summary_delta

    refute_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 3,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    ), @summary_delta

    refute_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "160000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    ), @summary_delta
  end

  def test_equality_to_delta
    assert_equal GitRPC::Diff::Delta.new(
      status: "A",
      similarity: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    ), @summary_delta

    assert_equal @summary_delta, GitRPC::Diff::Delta.new(
      status: "A",
      similarity: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    )

    refute_equal GitRPC::Diff::Delta.new(
      status: "A",
      similarity: 0,
      old_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    ), @summary_delta

    refute_equal @summary_delta, GitRPC::Diff::Delta.new(
      status: "A",
      similarity: 0,
      old_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "foo"
      }
    )
  end
end
