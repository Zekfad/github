# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"

class DiffTreeParserTest < Minitest::Test
  def test_parse
    input = [
      ":000000 100644 0000000000000000000000000000000000000000 7843ebec99cfc25bcb63b86123cdb83993ff76de A\00addition.txt",
      ":100644 000000 7843ebec99cfc25bcb63b86123cdb83993ff76de 0000000000000000000000000000000000000000 D\00deletion.txt",
      ":100644 100644 7843ebec99cfc25bcb63b86123cdb83993ff76de a3dcb783401e5234df33fee69e92c249a22fb244 M\00modification.txt",
      ":100644 100644 7843ebec99cfc25bcb63b86123cdb83993ff76de 4492405b16db6e5f8be7216a42ca280f7e53de74 M\00modification_to_binary.txt",
      ":100644 100755 7843ebec99cfc25bcb63b86123cdb83993ff76de a3dcb783401e5234df33fee69e92c249a22fb244 M\00modification_with_mode_change.txt",
      ":100644 100755 7843ebec99cfc25bcb63b86123cdb83993ff76de 7837f9c220680c1cc1a7c2cce5bf4f8fa531a715 M\00mode_change.txt",

      ":000000 160000 0000000000000000000000000000000000000000 a0a4cc9229e0c3d1c8bc837fc28aeddc8f7ed1db A\00submodule_addition",
      ":160000 000000 a0a4cc9229e0c3d1c8bc837fc28aeddc8f7ed1db 0000000000000000000000000000000000000000 D\00submodule_deletion",
      ":160000 160000 a0a4cc9229e0c3d1c8bc837fc28aeddc8f7ed1db 04fe32fde04bdd7849d6af8b711820fdcf6af190 M\00submodule_modification",

      ":000000 120000 0000000000000000000000000000000000000000 a4599dba6e67c4ec96bf9c3959acb83cfdbbaf7c A\00symlink_addition",
      ":120000 000000 a4599dba6e67c4ec96bf9c3959acb83cfdbbaf7c 0000000000000000000000000000000000000000 D\00symlink_deletion",
      ":120000 120000 a4599dba6e67c4ec96bf9c3959acb83cfdbbaf7c 7837f9c220680c1cc1a7c2cce5bf4f8fa531a715 M\00symlink_modification",

      ":100644 100644 e73ad092fb3730260f0738b6df1cfd8b432d840d e73ad092fb3730260f0738b6df1cfd8b432d840d R100\00rename.txt\00rename1.txt",
      ":100644 100644 e73ad092fb3730260f0738b6df1cfd8b432d840d f32716b32436c56d1aa42cac152060674d3b9702 R092\00rename_with_modifications.txt\00rename_with_modifications1.txt",

      ":160000 120000 e73ad092fb3730260f0738b6df1cfd8b432d840d e73ad092fb3730260f0738b6df1cfd8b432d840d T\00type_change",

      "1\t0\taddition.txt",
      "0\t1\tdeletion.txt",
      "1\t1\tmodification.txt",
      "-\t-\tmodification_to_binary.txt",
      "1\t1\tmodification_with_mode_change.txt",
      "1\t1\tmode_change.txt",

      "1\t0\tsubmodule_addition",
      "0\t1\tsubmodule_deletion",
      "1\t1\tsubmodule_modification",

      "1\t0\tsymlink_addition",
      "0\t1\tsymlink_deletion",
      "1\t1\tsymlink_modification",

      "0\t0\t\00rename.txt\00rename1.txt",
      "1\t1\t\00rename_with_modifications.txt\00rename_with_modifications1.txt",

      "1\t1\ttype_change",

      " 15 files changed, 9 insertions(+), 11 deletions(-)"
    ].join("\00")

    summary = GitRPC::Diff::DiffTreeParser.new(input).parse_summary

    assert_equal 15, summary.changed_files
    assert_equal 9, summary.additions
    assert_equal 11, summary.deletions

    assert_equal 16, summary.deltas.length

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "addition.txt"
      },
      new_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "addition.txt"
      }
    ), summary.deltas[0]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "D",
      similarity: 0,
      additions: 0,
      deletions: 1,
      old_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "deletion.txt"
      },
      new_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "deletion.txt"
      }
    ), summary.deltas[1]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: 1,
      deletions: 1,
      old_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "modification.txt"
      },
      new_file: {
        oid: "a3dcb783401e5234df33fee69e92c249a22fb244",
        mode: "100644",
        path: "modification.txt"
      }
    ), summary.deltas[2]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: nil,
      deletions: nil,
      old_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "modification_to_binary.txt"
      },
      new_file: {
        oid: "4492405b16db6e5f8be7216a42ca280f7e53de74",
        mode: "100644",
        path: "modification_to_binary.txt"
      }
    ), summary.deltas[3]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: 1,
      deletions: 1,
      old_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "modification_with_mode_change.txt"
      },
      new_file: {
        oid: "a3dcb783401e5234df33fee69e92c249a22fb244",
        mode: "100755",
        path: "modification_with_mode_change.txt"
      }
    ), summary.deltas[4]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: 1,
      deletions: 1,
      old_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "mode_change.txt"
      },
      new_file: {
        oid: "7837f9c220680c1cc1a7c2cce5bf4f8fa531a715",
        mode: "100755",
        path: "mode_change.txt"
      }
    ), summary.deltas[5]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "submodule_addition"
      },
      new_file: {
        oid: "a0a4cc9229e0c3d1c8bc837fc28aeddc8f7ed1db",
        mode: "160000",
        path: "submodule_addition"
      }
    ), summary.deltas[6]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "D",
      similarity: 0,
      additions: 0,
      deletions: 1,
      old_file: {
        oid: "a0a4cc9229e0c3d1c8bc837fc28aeddc8f7ed1db",
        mode: "160000",
        path: "submodule_deletion"
      },
      new_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "submodule_deletion"
      }
    ), summary.deltas[7]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: 1,
      deletions: 1,
      old_file: {
        oid: "a0a4cc9229e0c3d1c8bc837fc28aeddc8f7ed1db",
        mode: "160000",
        path: "submodule_modification"
      },
      new_file: {
        oid: "04fe32fde04bdd7849d6af8b711820fdcf6af190",
        mode: "160000",
        path: "submodule_modification"
      }
    ), summary.deltas[8]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "symlink_addition"
      },
      new_file: {
        oid: "a4599dba6e67c4ec96bf9c3959acb83cfdbbaf7c",
        mode: "120000",
        path: "symlink_addition"
      }
    ), summary.deltas[9]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "D",
      similarity: 0,
      additions: 0,
      deletions: 1,
      old_file: {
        oid: "a4599dba6e67c4ec96bf9c3959acb83cfdbbaf7c",
        mode: "120000",
        path: "symlink_deletion"
      },
      new_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "symlink_deletion"
      }
    ), summary.deltas[10]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: 1,
      deletions: 1,
      old_file: {
        oid: "a4599dba6e67c4ec96bf9c3959acb83cfdbbaf7c",
        mode: "120000",
        path: "symlink_modification"
      },
      new_file: {
        oid: "7837f9c220680c1cc1a7c2cce5bf4f8fa531a715",
        mode: "120000",
        path: "symlink_modification"
      }
    ), summary.deltas[11]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "R",
      similarity: 100,
      additions: 0,
      deletions: 0,
      old_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "100644",
        path: "rename.txt"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "100644",
        path: "rename1.txt"
      }
    ), summary.deltas[12]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "R",
      similarity: 92,
      additions: 1,
      deletions: 1,
      old_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "100644",
        path: "rename_with_modifications.txt"
      },
      new_file: {
        oid: "f32716b32436c56d1aa42cac152060674d3b9702",
        mode: "100644",
        path: "rename_with_modifications1.txt"
      }
    ), summary.deltas[13]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "D",
      similarity: 0,
      additions: 0,
      deletions: 1,
      old_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "160000",
        path: "type_change"
      },
      new_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "type_change"
      }
    ), summary.deltas[14]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "type_change"
      },
      new_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "120000",
        path: "type_change"
      }
    ), summary.deltas[15]
  end

  def test_parse_filename_with_newlines
    input = [
      ":000000 100644 0000000000000000000000000000000000000000 7843ebec99cfc25bcb63b86123cdb83993ff76de A\00file\nwith\nnewlines.txt",

      "1\t1\tfile\nwith\nnewlines.txt",

      " 1 file changed, 1 insertion(+), 1 deletion(-)"
    ].join("\00")

    summary = GitRPC::Diff::DiffTreeParser.new(input).parse_summary

    assert_equal 1, summary.changed_files
    assert_equal 1, summary.additions
    assert_equal 1, summary.deletions

    assert_equal 1, summary.deltas.length

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 1,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "file\nwith\nnewlines.txt"
      },
      new_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "file\nwith\nnewlines.txt"
      }
    ), summary.deltas[0]
  end

  def test_parse_with_too_many_files
    input = [
      ":000000 100644 0000000000000000000000000000000000000000 7843ebec99cfc25bcb63b86123cdb83993ff76de A\00file1.txt",
      ":000000 100644 0000000000000000000000000000000000000000 7843ebec99cfc25bcb63b86123cdb83993ff76de A\00file2.txt",
      ":000000 100644 0000000000000000000000000000000000000000 7843ebec99cfc25bcb63b86123cdb83993ff76de A\00file3.txt",
      ":000000 100644 0000000000000000000000000000000000000000 7843ebec99cfc25bcb63b86123cdb83993ff76de A\00file4.txt",
      ":160000 120000 e73ad092fb3730260f0738b6df1cfd8b432d840d e73ad092fb3730260f0738b6df1cfd8b432d840d T\00file5.txt",

      "1\t1\tfile1.txt",
      "1\t1\tfile2.txt",
      "1\t1\tfile3.txt",
      "1\t1\tfile4.txt",
      "1\t1\tfile5.txt",

      " 5 file changed, 5 insertion(+)"
    ].join("\00")

    with_max_files(4) do
      summary = GitRPC::Diff::DiffTreeParser.new(input).parse_summary

      assert_predicate summary, :too_many_files?

      assert_equal 5, summary.changed_files
      assert_equal 5, summary.additions
      assert_equal 0, summary.deletions

      assert_equal 4, summary.deltas.length
    end

    with_max_files(5) do
      summary = GitRPC::Diff::DiffTreeParser.new(input).parse_summary

      refute_predicate summary, :too_many_files?

      assert_equal 5, summary.changed_files
      assert_equal 5, summary.additions
      assert_equal 0, summary.deletions

      # the 5th file (`file5.txt`) is a typechange, so it's broken
      # up into 2 deltas.
      assert_equal 6, summary.deltas.length
    end
  end

  def test_does_not_get_confused_by_weird_file_names
    input = [
      ":000000 100644 0000000000000000000000000000000000000000 7843ebec99cfc25bcb63b86123cdb83993ff76de A\00sortOrderTestFiles/)\t41\t0x29\tRIGHT PARENTHESIS",
      ":100644 100644 e73ad092fb3730260f0738b6df1cfd8b432d840d f32716b32436c56d1aa42cac152060674d3b9702 R092\00old-file-name\x001\t2\tnew-file-name",
      ":000000 100644 0000000000000000000000000000000000000000 7843ebec99cfc25bcb63b86123cdb83993ff76de A\00sortOrderTestFiles/U\t85\t0x55\tLATIN CAPITAL LETTER U",

      "1\t0\tsortOrderTestFiles/)\t41\t0x29\tRIGHT PARENTHESIS",
      "1\t0\t\x00old-file-name\x001\t2\tnew-file-name",
      "1\t0\tsortOrderTestFiles/U\t85\t0x55\tLATIN CAPITAL LETTER U",

      " 3 files changed, 3 insertions(+)"
    ].join("\00")

    summary = GitRPC::Diff::DiffTreeParser.new(input).parse_summary

    assert_equal 3, summary.deltas.length

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "sortOrderTestFiles/)\t41\t0x29\tRIGHT PARENTHESIS"
      },
      new_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "sortOrderTestFiles/)\t41\t0x29\tRIGHT PARENTHESIS"
      }
    ), summary.deltas[0]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "R",
      similarity: 92,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "e73ad092fb3730260f0738b6df1cfd8b432d840d",
        mode: "100644",
        path: "old-file-name"
      },
      new_file: {
        oid: "f32716b32436c56d1aa42cac152060674d3b9702",
        mode: "100644",
        path: "1\t2\tnew-file-name"
      }
    ), summary.deltas[1]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "sortOrderTestFiles/U\t85\t0x55\tLATIN CAPITAL LETTER U"
      },
      new_file: {
        oid: "7843ebec99cfc25bcb63b86123cdb83993ff76de",
        mode: "100644",
        path: "sortOrderTestFiles/U\t85\t0x55\tLATIN CAPITAL LETTER U"
      }
    ), summary.deltas[2]
  end

  private

  def with_max_files(max)
    old_value = GitRPC::Diff::DiffTreeParser::MAX_FILES

    GitRPC::Diff::DiffTreeParser.send :remove_const, :MAX_FILES
    GitRPC::Diff::DiffTreeParser.const_set(:MAX_FILES, max)

    yield
  ensure
    GitRPC::Diff::DiffTreeParser.send :remove_const, :MAX_FILES
    GitRPC::Diff::DiffTreeParser.const_set(:MAX_FILES, old_value)
  end
end
