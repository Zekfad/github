# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendParticipationStatsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @backend = nil
  end

  def test_participation_stats_raises_invalid_base_oid
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.participation_stats("--output=/usr/bin/git", "", 0, 0)
    end
  end
end
