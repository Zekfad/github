# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendPushLogTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_push_log
    entries = @backend.push_log
    assert_equal 2, entries.size
    entries.each do |entry|
      ref, before, after, rest = entry.split(" ", 4)
      assert_equal "refs/heads/master", ref
    end

    entries = @backend.push_log(ref: "refs/heads/master")
    assert_equal 2, entries.size
    entries = @backend.push_log(ref: "refs/heads/foo")
    assert_equal 0, entries.size

    entries = @backend.push_log(limit: 1)
    assert_equal 1, entries.size
    entries = @backend.push_log(before_oid: @oid1)
    assert_equal 1, entries.size
    entries = @backend.push_log(before_oid: @oid2)
    assert_equal 0, entries.size
    entries = @backend.push_log(after_oid: @oid1)
    assert_equal 1, entries.size
    entries = @backend.push_log(after_oid: @oid2)
    assert_equal 1, entries.size
    entries = @backend.push_log(newer_than: "900000000")
    assert_equal 2, entries.size
    entries = @backend.push_log(newer_than: "17514144000")
    assert_equal 0, entries.size
    entries = @backend.push_log(older_than: "900000000")
    assert_equal 0, entries.size
    entries = @backend.push_log(older_than: "17514144000")
    assert_equal 2, entries.size
  end
end
