# -*- encoding: utf-8 -*-
# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

# In this test suite, we compare against ASCII-8BIT strings whenever non-ASCII
# characters are involved because read_contributors string results come that way
class BackendReadBlobContributorsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @expected = setup_test_commits
    @head = @fixture.rev_parse("HEAD")

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @expected = nil
    @backend = nil
  end

  def setup_test_commits(xtra = "")
    [{ :files => { "README" => "some readme content#{xtra}" },
        :message => "first commit#{xtra}",
        :name => "First Commit Åü†hør",
        :email => "firstcommitauthor@blah.com" },
      { :files => { "README" => "modified readme content#{xtra}" },
        :message => "second commit häß ÜTƒ8 ∂å†ã #{xtra}",
        :name  => "Second Commit Author",
        :email => "secondcommitauthor@blah.com" },
      { :files => { "other_file" => "some other stuff#{xtra}" },
        :message => "third commit#{xtra}",
        :name  => "Third Commit Author",
        :email => "thirdcommitauthor@blah.com" },
      { :files => { "other_file" => "modified the stuff#{xtra}" },
        :message => "fourth commit häß ÜTƒ8 ∂å†ã #{xtra}",
        :name  => "Fourth Commit Åü†hør",
        :email => "fourthcommitauthor@blah.com" }
    ].map do |commit|
      commit[:sha] = @fixture.commit_files(
        commit[:files],
        commit[:message],
        { :name => commit[:name], :email => commit[:email] })
      commit
    end
  end

  def setup_test_commits_with_coauthors
    # Make a commit with a single author
    commit1_time = 1111111111
    @fixture.commit_files(
      { "some_file" => "some words" },
      "First commit",
      { name: "Author One", email: "author1@blah.com", commit_time: commit1_time })

    # Make a commit with the same author, but add a co-author
    message = <<~MESSAGE
      Two heads think better than one

      Co-authored-by: Author 2 <author2@blah.com>
    MESSAGE
    @commit2_time = 1111111112
    @commit2_sha = @fixture.commit_files({ "some_file" => "moar words" }, message,
      name: "Author 1", email: "author1@blah.com", commit_time: @commit2_time)

    # Make a commit with a new author, add former two as co-authors
    # (mixed with some garbage that should be ignored)
    message = <<~MESSAGE
      I get by with a little help from my friends

      Co-authored-by: Someone with no e-mail
      # FIRST VALID CO-AUTHOR (key case and name should be indifferent):
      CO-AUTHORED-BY: Authør ⓵ 🌈🦄 <author1@blah.com>
      Some-other-key: Not Coauthor <unrelated@blah.com>
      # SECOND VALID CO-AUTHOR (name omitted):
      Co-authored-by: <author2@blah.com>
      Co-authored-by: valid.email.but.no.markers@blah.com
    MESSAGE
    @commit3_time = 1111111113
    @commit3_sha = @fixture.commit_files({ "some_file" => "even moar words" }, message,
      name: "Author 3", email: "author3@blah.com", commit_time: @commit3_time)

    # Bonus: a commit in an unrelated file , so we can test the path/repository
    #        scope for authors and coauthors
    message = <<~MESSAGE
      Unrelated

      Co-authored-by: Unrelated Coauthor <unrelated_co_author@blah.com>
    MESSAGE
    @unrelated_commit_sha = @fixture.commit_files({ "unrelated_file" => "words" }, message,
      name: "Unrelated Author", email: "unrelated_author@blah.com", commit_time: commit1_time)

    # Finally, make an extra commit not involving *every* previous author,
    # so we can check proper timestamp and latest commit attribution
    message = <<~MESSAGE
      Joining the party

      CO-AUTHORED-BY: Author 3 <author3@blah.com>
    MESSAGE
    @commit4_time = 1111111114
    @commit4_sha = @fixture.commit_files({ "some_file" => "infinite words" }, message,
      name: "Author 4", email: "author4@blah.com", commit_time: @commit4_time)
  end

  def test_read_contributors
    res = @backend.read_contributors(@head, "README")

    assert !res["truncated"]
    assert_equal 2, res["data"].size
    assert_matching_author @expected[1], res["data"][0]
    assert_equal 1, res["data"][0]["count"]
    assert_matching_author @expected[0], res["data"][1]
    assert_equal 1, res["data"][1]["count"]

    res = @backend.read_contributors(@head, "other_file")

    assert !res["truncated"]
    assert_equal 2, res["data"].size
    assert_matching_author @expected[3], res["data"][0]
    assert_matching_author @expected[2], res["data"][1]
  end

  def test_read_contributors_limit
    original_blob_contributors_limit = GitRPC::Backend.blob_contributors_limit
    GitRPC::Backend.blob_contributors_limit = 1

    res = @backend.read_contributors(@head, "README")

    assert res["truncated"]
    assert_equal 1, res["data"].size
    assert_matching_author @expected[1], res["data"][0]
    assert_equal 1, res["data"][0]["count"]

    res = @backend.read_contributors(
      @fixture.rev_parse("HEAD~3"), "README")

    assert res["truncated"]
    assert_equal 1, res["data"].size
    assert_matching_author @expected[0], res["data"][0]
  ensure
    GitRPC::Backend.blob_contributors_limit = original_blob_contributors_limit
  end

  def test_read_contributors_longer_history
    # add some repeated commits
    @expected.push *setup_test_commits(" 2")
    @expected.push *setup_test_commits(" 3")
    @expected.push *setup_test_commits(" 4")
    @head = @fixture.rev_parse("HEAD")

    res = @backend.read_contributors(@head, "README")

    assert !res["truncated"]
    assert_equal 2, res["data"].size
    assert_matching_author @expected[13], res["data"][0]
    assert_equal 4, res["data"][0]["count"]
    assert_matching_author @expected[12], res["data"][1]
    assert_equal 4, res["data"][1]["count"]

    res = @backend.read_contributors(@head, "other_file")

    assert !res["truncated"]
    assert_equal 2, res["data"].size
    assert_matching_author @expected[15], res["data"][0]
    assert_matching_author @expected[14], res["data"][1]

    res = @backend.read_contributors(
      @fixture.rev_parse("HEAD~3"), "README")

    assert !res["truncated"]
    assert_equal 2, res["data"].size
    assert_matching_author @expected[12], res["data"][0]
    assert_matching_author @expected[9], res["data"][1]
  end

  def test_read_contributors_exceptions
    assert_raises(GitRPC::ObjectMissing, "") {
      @backend.read_contributors("0" * 40, "README")
    }

    assert_raises(GitRPC::ObjectMissing, "") {
      @backend.read_contributors(@head, "missing_file")
    }
  end

  def test_read_contributors_email_with_space
    raw_email  = "   spaceinemail.com   "
    message    = "My author put spaces in their email"
    raw_author = "author Who|Are|You <#{raw_email}> 12345678 -0700"

    # make a commit with a space in the email
    @fixture.stage_files({ "bogus" => "some words" })
    sha = @fixture.add_empty_raw_commit(message, raw_author)

    # confirm that the raw commit really has a space in author email
    commit_output = @fixture.command("git cat-file commit #{sha}")
    author_line = commit_output.split("\n").find { |x| x =~ /^author/ }
    assert_match /<#{raw_email}>/, author_line

    # confirm that read_contributors strips the spaces
    res = @backend.read_contributors(sha, "bogus")
    assert !res["truncated"]
    assert_equal 1, res["data"].size
    assert_equal raw_email.strip, res["data"][0]["author"]
    assert_equal sha, res["data"][0]["commit"]
  end

  def test_read_contributors_missing_timestamp
    message    = "My author left out a timestamp"
    raw_author = "author Who|Are|You <example@person.com>"

    # make a commit with no timestamp
    @fixture.stage_files({ "bogus" => "some words" })
    sha = @fixture.add_empty_raw_commit(message, raw_author)

    # confirm that the raw commit really got our author line
    commit_output = @fixture.command("git cat-file commit #{sha}")
    author_line = commit_output.split("\n").find { |x| x =~ /^author/ }
    assert_equal raw_author, author_line

    # confirm that read_contributors strips the spaces
    res = @backend.read_contributors(sha, "bogus")
    assert !res["truncated"]
    assert_equal 1, res["data"].size
    assert_equal 0, res["data"][0]["time"]
    assert_equal 0, res["data"][0]["offset"]
  end

  def test_read_contributors_skips_commit_with_empty_author
    # Yes, it is invalid, but was found in the wild, see https://git.io/v5HEl
    empty_author = "author  <> 1475704250 +0000"
    normal_author = "author Someone <someone@example.com> 12345678 -0700"
    message = "Why do people mess with git plumbing?"

    sha1 = @fixture.add_empty_raw_commit(message, empty_author)
    sha2 = @fixture.add_empty_raw_commit(message, "parent #{sha1}", normal_author)

    res = @backend.read_contributors(sha2)
    assert_equal 1, res["data"].size
    assert_equal 1, res["data"][0]["count"]
    assert_equal "someone@example.com", res["data"][0]["author"]
  end

  def test_read_contributors_with_single_empty_author
    empty_author = "author  <> 1475704250 +0000"
    message = "If you skip me, result will be empty"

    sha = @fixture.add_empty_raw_commit(message, empty_author)

    res = @backend.read_contributors(sha)
    refute res["truncated"]
    assert_empty res["data"]
  end

  # Simulate output from spawning 'git log' with a timeout
  class FakeGitLog
    attr_reader :out
    attr_reader :err

    def initialize(out, err = "")
      @out = out
      @err = err
    end

    def exec!
      raise POSIX::Spawn::TimeoutExceeded
    end
  end

  def test_read_contributors_timeout
    commit = @expected.first
    output = "#{commit[:sha]}|1342581713 -0700|#{commit[:name]} <#{commit[:email]}>\n".dup.force_encoding("ASCII-8BIT")

    @backend.stub :build_git, FakeGitLog.new(output) do
      res = @backend.read_contributors(@head, "README")

      assert res["truncated"]
      assert_equal 1, res["data"].size
      assert_matching_author commit, res["data"].first
    end
  end

  def test_read_contributors_co_authors_false_by_default
    setup_test_commits_with_coauthors

    explicit = @backend.read_contributors(@commit4_sha, "some_file", co_authors: false)
    implicit = @backend.read_contributors(@commit4_sha, "some_file")

    assert_equal explicit, implicit
  end

  def test_read_contributors_ignoring_co_authors
    setup_test_commits_with_coauthors

    res = @backend.read_contributors(@commit4_sha, "some_file")
    data = res["data"].sort_by { |k| k["author"] }
    assert_equal 3, data.size
    data.unshift("dummy element to align array index with author number")

    assert_equal "author1@blah.com", data[1]["author"]
    assert_equal "Author 1", data[1]["name"]
    assert_equal 2, data[1]["count"]
    assert_equal @commit2_sha, data[1]["commit"]
    assert_equal @commit2_time, data[1]["time"]

    assert_equal "author3@blah.com", data[2]["author"]
    assert_equal "Author 3", data[2]["name"]
    assert_equal 1, data[2]["count"]
    assert_equal @commit3_sha, data[2]["commit"]
    assert_equal @commit3_time, data[2]["time"]

    assert_equal "author4@blah.com", data[3]["author"]
    assert_equal "Author 4", data[3]["name"]
    assert_equal 1, data[3]["count"]
    assert_equal @commit4_sha, data[3]["commit"]
    assert_equal @commit4_time, data[3]["time"]
  end

  def test_read_contributors_returns_most_recently_used_name_for_each_email
    setup_test_commits_with_coauthors

    res = @backend.read_contributors(@commit4_sha, "some_file")
    names = res["data"].sort_by { |k| k["author"] }.map { |c| c["name"].force_encoding("UTF-8") }
    assert_equal ["Author 1", "Author 3", "Author 4"], names

    res = @backend.read_contributors(@commit4_sha, "some_file", co_authors: true)
    names = res["data"].sort_by { |k| k["author"] }.map { |c| c["name"].force_encoding("UTF-8") }
    assert_equal ["Authør ⓵ 🌈🦄", "", "Author 3", "Author 4"], names
  end

  def test_read_contributors_considering_co_authors
    setup_test_commits_with_coauthors

    res = @backend.read_contributors(@commit4_sha, "some_file", co_authors: true)
    data = res["data"].sort_by { |k| k["author"] }
    assert_equal 4, data.size
    data.unshift("dummy element to align array index with author number")

    assert_equal "author1@blah.com", data[1]["author"]
    assert_equal "Authør ⓵ 🌈🦄".dup.force_encoding("ASCII-8BIT"), data[1]["name"]
    assert_equal 3, data[1]["count"]
    assert_equal @commit3_sha, data[1]["commit"]
    assert_equal @commit3_time, data[1]["time"]

    assert_equal "author2@blah.com", data[2]["author"]
    assert_equal "", data[2]["name"]
    assert_equal 2, data[2]["count"]
    assert_equal @commit3_sha, data[2]["commit"]
    assert_equal @commit3_time, data[2]["time"]

    assert_equal "author3@blah.com", data[3]["author"]
    assert_equal "Author 3", data[3]["name"]
    assert_equal 2, data[3]["count"]
    assert_equal @commit4_sha, data[3]["commit"]
    assert_equal @commit4_time, data[3]["time"]

    assert_equal "author4@blah.com", data[4]["author"]
    assert_equal "Author 4", data[4]["name"]
    assert_equal 1, data[4]["count"]
    assert_equal @commit4_sha, data[4]["commit"]
    assert_equal @commit4_time, data[4]["time"]
  end

  def test_read_contributors_with_valid_and_invalid_utf_in_ascii_encoded_commit
    message = <<~MESSAGE
      Mind you, e-mail addresses *can* have encoding (as long as it is
      valid UTF-8, per https://tools.ietf.org/html/rfc6531), but we may
      get anything encoding-wise on the wild, so we should test valid and
      invalid UTF-8 (both inside and outside emails)

      CO-AUTHORED-BY: Touché Turtle <touché@blah.com>
      CO-AUTHORED-BY: Mr. Odd Byte\xAF <odd\xAF@blah.com>
    MESSAGE
    @fixture.stage_files({ "some_file" => "words" })
    raw_author = "author Who|Are|You <author@blah.com>"
    sha = @fixture.add_empty_raw_commit(message, raw_author, "encoding ASCII")

    res = @backend.read_contributors(sha, "some_file", co_authors: true)
    data = res["data"]

    assert_equal "author@blah.com", res["data"][0]["author"]
    assert_equal "touché@blah.com".dup.force_encoding("ASCII-8BIT"), res["data"][1]["author"]
    assert_equal "odd\xAF@blah.com".dup.force_encoding("ASCII-8BIT"), res["data"][2]["author"]
  end

  def test_read_contributors_ignores_authors_and_co_authors_not_contributing_to_path
    setup_test_commits_with_coauthors
    res = @backend.read_contributors(@commit4_sha, "some_file")
    authors = res["data"].map { |h| h["author"] }

    assert_includes authors, "author1@blah.com"
    refute_includes authors, "unrelated_author@blah.com"
    refute_includes authors, "unrelated_co_author@blah.com"
  end

  def test_read_contributors_for_entire_repo
    setup_test_commits_with_coauthors
    res = @backend.read_contributors(@commit4_sha)
    authors = res["data"].map { |h| h["author"] }

    assert_includes authors, "author1@blah.com"
    assert_includes authors, "unrelated_author@blah.com"
    refute_includes authors, "unrelated_co_author@blah.com"
  end

  def test_read_contributors_for_entire_repo_with_co_authors
    setup_test_commits_with_coauthors
    res = @backend.read_contributors(@commit4_sha, co_authors: true)
    authors = res["data"].map { |h| h["author"] }

    assert_includes authors, "author1@blah.com"
    assert_includes authors, "unrelated_author@blah.com"
    assert_includes authors, "unrelated_co_author@blah.com"
  end

  def test_read_contributors_default_grouping
    setup_test_commits_with_coauthors

    assert_equal @backend.read_contributors(@commit4_sha),
                 @backend.read_contributors(@commit4_sha, group_by_signature: false),
                 "Contributors should default to group by email only"
  end

  def test_read_contributors_group_by_email
    setup_test_commits_with_coauthors
    res = @backend.read_contributors(@commit4_sha)

    authors = res["data"].map { |h| h["author"] }
    assert_equal authors, authors.uniq, "Contributors grouped by email should have unique authors field"

    author1_data = res["data"].find { |h| h["author"] == "author1@blah.com" }
    assert_equal 2, author1_data["count"]
  end

  def test_read_contributors_group_by_signature
    setup_test_commits_with_coauthors

    res = @backend.read_contributors(@commit4_sha, group_by_signature: true)
    contributors = res["data"].select { |h| h["author"] == "author1@blah.com" }
    names = contributors.map { |h| h["name"] }.sort
    assert_equal ["Author 1", "Author One"], names
  end

  def test_read_contributors_group_by_signature_with_coauthors
    setup_test_commits_with_coauthors

    res = @backend.read_contributors(@commit4_sha, group_by_signature: true, co_authors: true)
    contributors = res["data"].select { |h| h["author"] == "author1@blah.com" }
    names = contributors.map { |h| h["name"] }.sort
    assert_equal ["Author 1", "Author One", "Authør ⓵ 🌈🦄".dup.force_encoding("ASCII-8BIT")], names
  end

  # Assertion helpers

  def assert_matching_author(expected_data, author_data)
    assert_equal expected_data[:email], author_data["author"]
    assert_equal expected_data[:sha], author_data["commit"]
    assert_equal expected_data[:name].dup.force_encoding("ASCII-8BIT"), author_data["name"]
    assert_kind_of Numeric, author_data["time"]
    assert_kind_of Numeric, author_data["offset"]
  end
end
