# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendPeelComitOrTreeTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)

    @commit_oid = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "commit1", time: Time.now)
    @tree_oid = @backend.rugged.lookup(@commit_oid).tree.oid
    @tag1_oid = @fixture.create_tag_object("test-tag-1", "test tag message", @commit_oid, "commit")
    @tag2_oid = @fixture.create_tag_object("test-tag-2", "test tag message", @tag1_oid, "tag")

    @blob_oid = @fixture.rugged.write("new stuff", "blob")
  end

  def teardown
    @fixture.teardown
  end

  def test_peel_from_blob
    assert_nil @backend.peel_to_commit_or_tree(@blob_oid)
  end

  def test_peel_from_tree
    assert_equal @tree_oid, @backend.peel_to_commit_or_tree(@tree_oid)
  end

  def test_peel_from_commit
    assert_equal @commit_oid, @backend.peel_to_commit_or_tree(@commit_oid)
  end

  def test_peel_from_tag_on_commit
    assert_equal @commit_oid, @backend.peel_to_commit_or_tree(@tag1_oid)
  end

  def test_peel_from_tag_on_tag
    assert_equal @commit_oid, @backend.peel_to_commit_or_tree(@tag2_oid)
  end

  def test_peel_from_nested_tag
    nested_tag_oid = 10.times.reduce(@tag1_oid) do |oid|
      @fixture.create_tag_object("wraps-#{oid}", "test tag message", oid, "tag")
    end
    assert_raises TypeError do
      @backend.peel_to_commit_or_tree(nested_tag_oid)
    end
  end
end
