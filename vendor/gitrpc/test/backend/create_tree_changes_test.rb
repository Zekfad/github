# rubocop:disable Style/FrozenStringLiteralComment
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

# Tests the GitRPC::Backend::Files module methods
class BackendCommitTreeChangesTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @backend = GitRPC::Backend.new(@fixture.path)
    @rugged = @backend.rugged

    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_files
    @fixture.commit_files({
      "README"          => "stuff",
      "user.rb"         => "def stuff; end",
      "subtree/file.js" => "function stuff() {}"
    }, "add first files")

    @HEAD = @fixture.rev_parse("HEAD")
  end

  def test_build_in_subtree
    files = { "doc/read.md" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }

    oid = @backend.create_tree_changes(parent=nil, info, files, false)
    assert_equal "c9a8c6f89f28b5d90834acf38ecfca86b3c89bac", oid

    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 1, commit.tree.count
    assert_equal "doc", commit.tree[0][:name]

    subtree = @rugged.lookup(commit.tree[0][:oid])
    assert_equal "read.md", subtree[0][:name]
    assert_equal "hiya", @rugged.lookup(subtree[0][:oid]).content
  end

  def test_rebuild_same_commits
    files = { "doc/read.md" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }

    oid1 = @backend.create_tree_changes(parent=nil, info, files, false)
    assert_equal "c9a8c6f89f28b5d90834acf38ecfca86b3c89bac", oid1

    oid2 = @backend.create_tree_changes(parent=nil, info, files, false)
    assert_equal "c9a8c6f89f28b5d90834acf38ecfca86b3c89bac", oid2

    oid3 = @backend.create_tree_changes(parent=nil, info, files, false)
    assert_equal "c9a8c6f89f28b5d90834acf38ecfca86b3c89bac", oid3
  end

  def test_stage_signed_tree_changes
    files = { "doc/read.md" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }

    raw, tree_oid = @backend.stage_signed_tree_changes(parent=nil, info, files, false)
    oid = @backend.create_tree_changes(parent=nil, info, files, false)

    assert_equal @rugged.lookup(oid).tree_oid, tree_oid
    assert_equal @rugged.read(oid).data, raw
  end

  def test_commit_in_subtree
    files = { "doc/read.md" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }

    oid = @backend.create_tree_changes(parent=nil, info, files, false)
    assert_equal "c9a8c6f89f28b5d90834acf38ecfca86b3c89bac", oid

    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 1, commit.tree.count
    assert_equal "doc", commit.tree[0][:name]

    subtree = @rugged.lookup(commit.tree[0][:oid])
    assert_equal "read.md", subtree[0][:name]
    assert_equal "hiya", @rugged.lookup(subtree[0][:oid]).content
  end

  def test_create_tree_changes_empty_history
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }

    oid = @backend.create_tree_changes(parent=nil, info, files, false)
    assert_equal "c953f091f2e49200cd212c8fb5f40474e9ec62d4", oid

    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 1, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content
  end

  def test_create_tree_changes_empty_history_new_ref
    files = { "README" => "hiya" }
    info  = { "message" => "another test commit\n", "committer" => @committer }

    oid = @backend.create_tree_changes(parent=nil, info, files, false)
    @backend.update_ref("refs/heads/brandnew", oid, nil)
    assert_equal "d79d1b1000943917a887d317c7f39b42747e88aa", oid

    commit = @rugged.lookup(oid)
    assert_equal "another test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal [], commit.parents
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 1, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content
  end

  def test_create_tree_changes_parent_commit_no_ref
    setup_test_files

    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @backend.create_tree_changes(parent=@HEAD, info, files, false)
    assert_equal "d7434263ca3d085d7661e048fe8598ae4d108f7d", oid

    # verify the commit
    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    # verify the tree
    assert_equal 3, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content

    # ref should not have moved
    assert_equal @HEAD, @rugged.rev_parse_oid("master")
  end

  def test_create_tree_changes_parent_commit_and_move_ref
    setup_test_files

    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @backend.create_tree_changes(parent=@HEAD, info, files, false)
    @backend.update_ref("refs/heads/master", oid, parent)
    assert_equal "d7434263ca3d085d7661e048fe8598ae4d108f7d", oid

    # verify the commit
    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    # verify the tree
    assert_equal 3, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content

    # ref should move
    assert_equal oid, @rugged.rev_parse_oid("master")
  end

  def test_create_tree_changes_parent_commit_and_move_ref_head
    setup_test_files

    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @backend.create_tree_changes(parent=@HEAD, info, files, false)
    @backend.update_ref("HEAD", oid, parent)
    assert_equal "d7434263ca3d085d7661e048fe8598ae4d108f7d", oid

    # verify the commit
    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    # verify the tree
    assert_equal 3, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content

    # ref should move
    assert_equal oid, @rugged.rev_parse_oid("master")
  end

  def test_create_tree_changes_non_utc_timezone
    @committer["time"] = "2011-12-21T03:04:05-05:00"

    setup_test_files
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @backend.create_tree_changes(parent=@HEAD, info, files, false)

    # Grab the raw object, since Rugged may be throwing out the UTC offset when
    # parsing the ISO8601 string into a Time.
    raw_object = @rugged.lookup(oid).read_raw

    if RUBY_VERSION >= "1.9.0"
      # If we're on 1.9+, verify the UTC offset
      assert_match /^committer Some Guy <guy@example.com> 1324454645 -0500$/, raw_object.data
      assert_equal "3643558ccc7120d9c1e3ef013545af21a92d700b", oid
    else
      # If we're on 1.8, there should be no UTC offset
      assert_match /^committer Some Guy <guy@example.com> 1324454645 \+0000$/, raw_object.data
      assert_equal "39a9d8711fd4217dbdc98cb9519e375809b1326a", oid
    end
  end

  def test_create_tree_changes_simple_unixtime
    skip "Zone offsets not supported under MRI < 1.9.2" if RUBY_VERSION < "1.9"

    @committer["time"] = [1324454645, -18000]

    setup_test_files
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @backend.create_tree_changes(parent=@HEAD, info, files, false)

    # Grab the raw object, since Rugged may be throwing out the UTC offset when
    # parsing the ISO8601 string into a Time.
    raw_object = @rugged.lookup(oid).read_raw

    if RUBY_VERSION >= "1.9.0"
      # If we're on 1.9+, verify the UTC offset
      assert_match /^committer Some Guy <guy@example.com> 1324454645 -0500$/, raw_object.data
      assert_equal "3643558ccc7120d9c1e3ef013545af21a92d700b", oid
    else
      # If we're on 1.8, there should be no UTC offset
      assert_match /^committer Some Guy <guy@example.com> 1324454645 \+0000$/, raw_object.data
      assert_equal "39a9d8711fd4217dbdc98cb9519e375809b1326a", oid
    end
  end

  def test_create_tree_changes_delete_file
    setup_test_files
    files = { "README" => "changed", "user.rb" => nil }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @backend.create_tree_changes(parent=@HEAD, info, files, false)
    assert_equal "5c83d25ef29c4ab1e8e5b6ffb06dc3bbb548720e", oid

    # verify the commit
    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    # verify that the tree now only has 2 objects in it, since user.rb was
    # deleted
    assert_equal 2, commit.tree.count

    # verify that the README was changed properly
    assert_equal "README", commit.tree[0][:name]
    assert_equal "changed", @rugged.lookup(commit.tree[0][:oid]).content

    # ref should not have moved
    assert_equal @HEAD, @rugged.rev_parse_oid("master")
  end

  def test_create_tree_changes_delete_file_in_subtree
    setup_test_files
    files = { "subtree/file.js" => nil }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @backend.create_tree_changes(parent=@HEAD, info, files, false)
    assert_equal "793d2ad7b47a27ab61cf2e32c26d0e1b8640e2d0", oid

    # verify the commit
    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    # verify that the tree now only has 2 objects in it, since subtree/file.js
    # was deleted
    assert_equal 2, commit.tree.count
  end

  def test_create_tree_changes_reset_existing_tree
    setup_test_files
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @backend.create_tree_changes(parent=@HEAD, info, files, reset=true)
    assert_equal "5794f2a3b0058328baee23074a35d1f40e252811", oid

    # verify the tree
    commit = @rugged.lookup(oid)
    assert_equal 1, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content
  end

  def test_create_tree_changes_null_filenames
    setup_test_files
    files = { "test\000test" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }

    assert_raises ArgumentError do
      oid = @backend.create_tree_changes(parent=@HEAD, info, files, reset=true)
    end
  end

  def test_create_tree_changes_bad_ref
    setup_test_files

    # Update the repo again so @HEAD is not the parent
    new_head_oid = @backend.create_tree_changes(
      @HEAD,
      {"message" => "@HEAD is no longer HEAD", "committer" => @committer},
      {"README" => "changed"}
    )
    @backend.update_ref("refs/heads/master", new_head_oid, @HEAD)

    assert_raises GitRPC::RefUpdateError do
      new_head_oid = @backend.create_tree_changes(
        @HEAD,
        {"message" => "test commit", "committer" => @committer},
        {"README" => "hiya"},
        false
      )
      @backend.update_ref("refs/heads/master", new_head_oid, @HEAD)
    end
  end

  def test_create_tree_changes_file_mode
    setup_test_files
    mode = 0100644

    # ensure the mode is what we expect
    oid   = @HEAD
    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]

    # make a file change and ensure the mode isn't changed
    files = { "user.rb" => "def other; end" }
    info  = { "message" => "test commit 1", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]

    # make a new file and ensure the mode is correct
    files = { "newfile" => "cool contents" }
    info  = { "message" => "test commit 4", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = tree["newfile"]
    assert_equal mode, entry[:filemode]

    mode = 0100755

    # explicitly change mode and check the new one
    @fixture.command("chmod 0755 work/user.rb")
    @fixture.command("git --work-tree='work' add -A")
    @fixture.commit_changes("test commit 2")
    oid   = @fixture.rev_parse("HEAD")

    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]

    # make a file change and ensure the mode isn't changed
    files = { "user.rb" => "def again; end" }
    info  = { "message" => "test commit 3", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]
  end

  def test_create_tree_changes_file_mode_in_subtree
    setup_test_files
    mode = 0100644

    # ensure the mode is what we expect
    oid   = @HEAD
    tree  = @rugged.lookup(oid).tree
    tree  = @rugged.lookup(tree["subtree"][:oid])
    entry = tree["file.js"]
    assert_equal mode, entry[:filemode]

    # make a file change and ensure the mode isn't changed
    files = { "subtree/file.js" => "function other() {}" }
    info  = { "message" => "test commit 1", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    tree  = @rugged.lookup(tree["subtree"][:oid])
    entry = tree["file.js"]
    assert_equal mode, entry[:filemode]

    # make a new file and ensure the mode is correct
    files = { "subtree/newfile" => "cool contents" }
    info  = { "message" => "test commit 4", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    tree  = @rugged.lookup(tree["subtree"][:oid])
    entry = tree["newfile"]
    assert_equal mode, entry[:filemode]

    mode = 0100755

    # explicitly change mode and check the new one
    @fixture.command("chmod 0755 work/subtree/file.js")
    @fixture.command("git --work-tree='work' add -A")
    @fixture.commit_changes("test commit 2")
    oid   = @fixture.rev_parse("HEAD")

    tree  = @rugged.lookup(oid).tree
    tree  = @rugged.lookup(tree["subtree"][:oid])
    entry = tree["file.js"]
    assert_equal mode, entry[:filemode]

    # make a file change and ensure the mode isn't changed
    files = { "subtree/file.js" => "function again() {}" }
    info  = { "message" => "test commit 3", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    tree  = @rugged.lookup(tree["subtree"][:oid])
    entry = tree["file.js"]
    assert_equal mode, entry[:filemode]
  end

  def get_tree_entry(tree, path)
    tree.path(path)
  rescue Rugged::TreeError
    nil
  end

  def test_create_tree_changes_new_file_in_new_subtree
    setup_test_files

    mode = 0100644
    oid  = @HEAD

    # make a new file in a new subtree and ensure the mode is correct
    files = { "subtree/even/further/newfile" => "cool contents" }
    info  = { "message" => "test commit", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = get_tree_entry(tree, "subtree/even/further/newfile")
    assert_equal mode, entry[:filemode]
    assert_equal "cool contents", @rugged.lookup(entry[:oid]).content
  end

  def test_create_tree_changes_file_mode_with_multiple_files_subtrees_and_symlinks
    setup_test_files

    @fixture.commit_files({
                            "subtree/even/further/user.rb" => "def thing; end",
                            "some/other/tree/text"         => "hiya",
                          }, "add more files")

    # explicitly change modes and check to be sure
    @fixture.command("chmod 0755 work/subtree/even/further/user.rb")
    @fixture.command("chmod 0755 work/subtree/file.js")
    @fixture.command("ln -fs work/user.rb work/some/other/link")
    @fixture.command("git --work-tree='work' add -A")
    @fixture.commit_changes("changing modes and making symlinks")
    oid   = @fixture.rev_parse("HEAD")

    tree    = @rugged.lookup(oid).tree
    modes   = [0100755, 0100644, 0100755, 0120000]
    entries = []
    entries.push get_tree_entry(tree, "subtree/even/further/user.rb")
    entries.push get_tree_entry(tree, "user.rb")
    entries.push get_tree_entry(tree, "subtree/file.js")
    entries.push get_tree_entry(tree, "some/other/link")
    assert_equal modes, entries.collect { |entry|  entry[:filemode] }

    # make changes and ensure the modes haven't changed
    files = {
      "subtree/file.js"              => "function again() {}",
      "subtree/even/further/user.rb" => "def other; end",
      "subtree/even/more.txt"        => "nothing special",
      "some/other/tree/text"         => "later",
      "some/other/link"              => "../../user.rb",
    }
    info  = { "message" => "test commit 3", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    modes   = [0100755, 0100644, 0100755, 0120000, 0100644]
    entries = []
    entries.push get_tree_entry(tree, "subtree/even/further/user.rb")
    entries.push get_tree_entry(tree, "user.rb")
    entries.push get_tree_entry(tree, "subtree/file.js")
    entries.push get_tree_entry(tree, "some/other/link")
    entries.push get_tree_entry(tree, "subtree/even/more.txt")
    assert_equal modes, entries.collect { |entry|  entry[:filemode] }
  end

  def test_set_mode_for_new_file
    mode = 0100755

    files = { "somefile" => { "data" => "hiya", "mode" => mode } }
    info  = { "message" => "test commit", "committer" => @committer }
    oid = @backend.create_tree_changes(nil, info, files)

    tree  = @rugged.lookup(oid).tree
    entry = get_tree_entry(tree, "somefile")
    assert_equal mode, entry[:filemode]
    assert_equal "hiya", @rugged.lookup(entry[:oid]).content
  end

  def test_keep_mode_on_edit_with_hash_input
    setup_test_files
    mode = 0100644

    # ensure the mode is what we expect
    oid   = @HEAD
    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]

    # make a file change and ensure the mode isn't changed
    files = { "user.rb" => { "data" => "def other; end" } }
    info  = { "message" => "test commit 1", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]

    # make a new file and ensure the mode is correct
    files = { "newfile" => { "data" => "cool contents" } }
    info  = { "message" => "test commit 4", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = tree["newfile"]
    assert_equal mode, entry[:filemode]

    mode = 0100755

    # explicitly change mode and check the new one
    @fixture.command("chmod 0755 work/user.rb")
    @fixture.command("git --work-tree='work' add -A")
    @fixture.commit_changes("test commit 2")
    oid   = @fixture.rev_parse("HEAD")

    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]

    # make a file change and ensure the mode isn't changed
    files = { "user.rb" => { "data" => "def again; end" } }
    info  = { "message" => "test commit 3", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]
  end

  def test_rename_file
    setup_test_files

    files = { "somefile" => { "source" => "user.rb" } }
    info  = { "message" => "test commit", "committer" => @committer }
    oid = @backend.create_tree_changes(@HEAD, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = get_tree_entry(tree, "somefile")
    assert_equal "def stuff; end", @rugged.lookup(entry[:oid]).content

    assert_nil get_tree_entry(tree, "user.rb")
  end

  def test_get_mode_from_renamed_file
    setup_test_files

    mode = 0100755

    # explicitly change mode and check the new one
    @fixture.command("chmod 0755 work/user.rb")
    @fixture.command("git --work-tree='work' add -A")
    @fixture.commit_changes("test commit 2")
    oid   = @fixture.rev_parse("HEAD")

    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]

    files = { "somefile" => { "source" => "user.rb" } }
    info  = { "message" => "test commit 3", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = tree["somefile"]
    assert_equal mode, entry[:filemode]
  end

  def test_remove_old_file_and_make_new_contents_and_also_take_mode
    setup_test_files
    mode = 0100755

    # explicitly change mode and check the new one
    @fixture.command("chmod 0755 work/user.rb")
    @fixture.command("git --work-tree='work' add -A")
    @fixture.commit_changes("test commit 2")
    oid   = @fixture.rev_parse("HEAD")

    tree  = @rugged.lookup(oid).tree
    entry = tree["user.rb"]
    assert_equal mode, entry[:filemode]

    files = { "somefile" => { "source" => "user.rb", "data" => "def other_stuff; end" } }
    info  = { "message" => "test commit 3", "committer" => @committer }
    oid   = @backend.create_tree_changes(oid, info, files, false)

    tree  = @rugged.lookup(oid).tree
    entry = tree["somefile"]
    assert_equal "def other_stuff; end", @rugged.lookup(entry[:oid]).content
    assert_equal mode, entry[:filemode]

    assert_nil get_tree_entry(tree, "user.rb")
  end

  def test_unsorted_input
    setup_test_files
    parent = @fixture.rev_parse("HEAD")
    info  = { "message" => "test commit 3", "committer" => @committer }
    files = {
      "a/deep/deep/file" => { "data" => "def stuff; end" },
      "a/deep/file" => { "data" => "def stuff; end" },
      "a/deep/deep/otherfile" => { "data" => "def stuff; end" },
    }

    oid = @backend.create_tree_changes(parent, info, files, false)
    tree = @rugged.lookup(oid).tree

    assert tree.path("a/deep/deep/file")
    assert tree.path("a/deep/deep/otherfile")
    assert tree.path("a/deep/file")
  end

  def test_source_with_data
    setup_test_files
    parent = @fixture.rev_parse("HEAD")
    info  = { "message" => "test commit 3", "committer" => @committer }
    files = {
      "a/file" => { "source" => "user.rb", "data" => "def stuff; end" },
      "user.rb" => "def stuff; end",
    }

    oid = @backend.create_tree_changes(parent, info, files, false)
    tree = @rugged.lookup(oid).tree

    assert tree.path("a/file")
    assert tree.path("user.rb")
  end
end
