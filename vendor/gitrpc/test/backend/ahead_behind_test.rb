# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendAheadBehindTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @backend = nil
  end

  def test_ahead_behind_rejects_invalid_base_ref
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.ahead_behind("--output=/usr/bin/git", [])
    end
  end
end
