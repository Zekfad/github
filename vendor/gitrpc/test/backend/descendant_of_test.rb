# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendDescendantOfTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.add_empty_commit("commit 1")
    @commit2 = @fixture.add_empty_commit("commit 2")
    @commit3 = @fixture.add_empty_commit("commit 3")

    @fixture.command "git symbolic-ref HEAD refs/heads/noparents"
    @disjoint_oid = @fixture.add_empty_commit("commit 4")

    @fixture.command "git symbolic-ref HEAD refs/heads/master"
    @master_oid = @fixture.rev_parse("master")

    @commit5 = @fixture.commit_files({"README" => "Hello."}, "commit 5")
  end

  def test_descendant_of_with_oids
    expected = {
      [@master_oid, @commit2] => true,
      [@commit2, @master_oid] => false
    }

    assert_equal expected, @backend.descendant_of([
      [@master_oid, @commit2],
      [@commit2, @master_oid]
    ])
  end

  def test_descendant_of_with_invalid_commit
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.descendant_of([
        ["--output=/usr/bin/git", @commit2]
      ])
    end
  end

  def test_descendant_of_with_invalid_ancestor
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.descendant_of([
        [@master_oid, "--output=/usr/bin/git"]
      ])
    end
  end

  def test_descendant_of_with_invalid_commit_and_ancestor
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.descendant_of([
        ["--output=/usr/bin/git", "--output=/usr/bin/git"]
      ])
    end
  end

  def test_descendant_of_with_missing_object
    assert_raises(GitRPC::ObjectMissing) do
      @backend.descendant_of([["deadbeef" * 5, @master_oid]])
    end
  end

  def test_descendant_of_with_blob_oid
    commit = @backend.read_commits([@commit5]).first
    tree   = @backend.read_trees([commit["tree"]]).first

    assert_raises(GitRPC::InvalidObject) do
      @backend.descendant_of([[tree["entries"]["README"]["oid"], @master_oid]])
    end
  end

  def test_descendant_of_without_common_ancestors
    expected = {
      [@disjoint_oid, @master_oid] => false,
      [@master_oid, @disjoint_oid] => false
    }

    assert_equal expected, @backend.descendant_of([
      [@disjoint_oid, @master_oid],
      [@master_oid, @disjoint_oid]
    ])
  end
end
