# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendCopyForkTest < Minitest::Test
  def setup
    @subfixture = RepositoryFixture.new
    @subfixture.setup
    @subfixture.command("mkdir nw1 nw2")
    @fixture = RepositoryFixture.new("#{@subfixture.path}/nw1/1.git")
    @fixture.setup
    @fixture2 = RepositoryFixture.new("#{@subfixture.path}/nw2/1.git")
    @fixture2.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
    @backend.nw_link
    @nw1_shared_storage_path = "#{@subfixture.path}/nw1/network.git"
    @nwbackend = GitRPC::Backend.new(@nw1_shared_storage_path)

    @backend2 = GitRPC::Backend.new(@fixture2.path)
    @backend2.nw_link
    @nw2_shared_storage_path = "#{@subfixture.path}/nw2/network.git"
    @nwbackend2 = GitRPC::Backend.new(@nw2_shared_storage_path)
  end

  def teardown
    @fixture.teardown
    @fixture2.teardown
    @subfixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_estimate_copy_fork
    size = @nwbackend.estimate_copy_fork(["1"])
    assert_kind_of Integer, size
    assert size > 0
  end

  def test_prepare_copy_fork
    assert_raises GitRPC::CommandFailed do
      @backend2.file_changed?("README", @oid1, @oid2)
    end

    e = assert_raises GitRPC::CommandFailed do
      @nwbackend2.prepare_copy_fork(@nw1_shared_storage_path, ["1"])
    end
    assert_match /The following files in.*are not recognized/, e.to_s

    @fixture.command("mkdir ../tmp && mv index work ../tmp")
    assert_nil @nwbackend2.prepare_copy_fork(@nw1_shared_storage_path, ["1"])

    assert_equal true, @backend2.file_changed?("README", @oid1, @oid2)
  end

  def test_copy_fork
    assert_raises GitRPC::CommandFailed do
      @backend2.file_changed?("README", @oid1, @oid2)
    end

    e = assert_raises GitRPC::CommandFailed do
      @nwbackend2.copy_fork(@nw1_shared_storage_path, "1")
    end
    assert_match /The following files in.*are not recognized/, e.to_s

    @fixture.command("mkdir ../tmp && mv index work ../tmp")
    assert_nil @nwbackend2.copy_fork(@nw1_shared_storage_path, "1")

    assert_equal true, @backend2.file_changed?("README", @oid1, @oid2)
  end

  def test_unknown_files
    out = @backend.unknown_files
    assert_equal "Command failed [1]: index\nwork/README\n\n\n", out
  end
end
