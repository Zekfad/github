# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendRsyncTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @fixture2 = RepositoryFixture.new("test2.git")
    @fixture2.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @fixture2.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture2.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture2.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_rsync
    assert_raises GitRPC::CommandFailed do
      @backend.file_changed?("README", @oid1, @oid2)
    end

    res = @backend.rsync("#{@fixture2.path}/", @fixture.path, archive: true, delete: true)
    assert res["ok"]

    assert_equal true, @backend.file_changed?("README", @oid1, @oid2)
  end

  def test_supported_cipher
      # We explicitly set the cipher for rsync to use to give us the best performance.
      # This test ensures it's still supported by current ssh.
      # An empty string means ssh no longer supports the faster cipher.
      assert @backend.faster_cipher != ""
  end
end
