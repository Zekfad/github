# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendTreeFileListTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @submodule_fixture = RepositoryFixture.new("submodule.git")
    @submodule_fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @submodule_fixture.teardown
  end

  def setup_test_commits
    @commit = @fixture.commit_files({
      "README"      => "hello",
      "subdir/test" => "somedata",
      "subdir/test2" => "shooop",
      "dir2/README" => "dawoop",
      "dir2/file.c" => "this is source code"
    }, "commit msg")

    @submodule_fixture.commit_files({
        "README" => "an submodule"
      }, "in sub")

    @submodule_commit = @fixture.add_submodule_commit(@submodule_fixture.path,
                                                      "submod",
                                                      "Add a submodule")
  end

  def test_basic_file_list
    res = @backend.tree_file_list(@submodule_commit, true, true, [])

    assert_includes res, "README"
    assert_includes res, "subdir/test2"
    assert_includes res, "dir2/README"
    assert_includes res, "submod"
    assert_includes res, "subdir"
    assert_includes res, "dir2"
  end

  def test_ignore_submodules
    res = @backend.tree_file_list(@submodule_commit, true, false, [])

    assert_includes res, "README"
    assert_includes res, "subdir/test2"
    assert_includes res, "dir2/README"
    assert_includes res, "dir2"
    assert !res.include?("submod")
  end

  def test_skip_directories
    res = @backend.tree_file_list(@submodule_commit, true, true, ["dir2"])

    assert_includes res, "README"
    assert_includes res, "subdir/test2"
    assert !res.include?("dir2")
    assert !res.include?("dir2/README")
  end

  def test_dont_list_directories
    res = @backend.tree_file_list(@submodule_commit, false, true, [])

    assert_includes res, "README"
    assert_includes res, "subdir/test2"
    assert_includes res, "dir2/README"
    assert !res.include?("submod")
    assert !res.include?("dir2")
    assert !res.include?("subdir")
  end
end
