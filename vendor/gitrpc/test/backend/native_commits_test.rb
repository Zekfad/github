# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

# Tests the GitRPC::Backend::NativeCommits module methods
class BackendNativeCommitsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("commit 1")
    @commit1 = @fixture.rev_parse("HEAD")

    @fixture.add_empty_commit("commit 2")
    @commit2 = @fixture.rev_parse("HEAD")

    @fixture.add_empty_commit("commit 3")
    @commit3 = @fixture.rev_parse("HEAD")
  end

  def test_read_commits_single_commit_data
    res = @backend.native_read_commits([@commit1])
    assert_equal 1, res.size
    assert_equal "commit", res[0]["type"]
    assert_equal @commit1, res[0]["oid"]
    assert_equal "commit 1\n\n", res[0]["message"]
    assert_equal "UTF-8", res[0]["encoding"]
    assert_equal "Some Author", res[0]["author"][0]
    assert_equal "someone@example.org", res[0]["author"][1]
    assert_equal Time.parse("2012-07-17T20:21:53-07:00"), Time.parse(res[0]["author"][2])
    assert !res[0]["committer"].nil?
  end

  def test_read_commits_multiple
    res = @backend.native_read_commits([@commit1, @commit3, @commit2])
    assert_equal 3, res.size
    assert_equal @commit1, res[0]["oid"]
    assert_equal @commit3, res[1]["oid"]
    assert_equal @commit2, res[2]["oid"]
  end

  def test_read_commits_missing
    assert_raises GitRPC::ObjectMissing do
      @backend.native_read_commits([@commit1, "0" * 40, @commit2])
    end
  end
end
