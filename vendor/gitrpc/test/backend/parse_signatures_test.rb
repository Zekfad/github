# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"

class BackendParseSignaturesTest < Minitest::Test
  def setup
    path = File.expand_path("../../examples/signed_objects.git", __FILE__)
    @backend = GitRPC::Backend.new(path)

    @signed_commit_oid = "58266c4ab0e73d2a94319fa6695ba75bdc73edc8"

    @signed_commit_payload = <<-HERE
tree 68aba62e560c0ebc3396e8ae9335232cd93a3f60
author Ben Toews <mastahyeti@users.noreply.github.com> 1453744339 -0700
committer Ben Toews <mastahyeti@users.noreply.github.com> 1453744339 -0700

signed commit
    HERE

    @signed_commit_signature = <<-HERE.rstrip
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAABAgAGBQJWpmDTAAoJEKAG779xTNk6ikMIANpbvFs/ywzm2rEH9KXm1JE2
wnziL7ghZj1pxSuockBC9tA+sACaC0mYVpQwEhFrE6x5Br1jkBsAbCiKexBXVBtj
3ay47QrJN2KXTwwEWioHLOz00nBzZd8tVglf1K6uUXgrMJdHSnwPnpuvSpVnIf0B
byXsLyyaaCrEObYQJpCT19+TObY3b8MTtl6NkN8IWhXbEmrrgANj44wumqogI5wQ
LrVnOUfV60+fKPtMrNpYiW2t0OcolfLCzY0SyGYyV/YpESRvDogZ1j9wPq4I0wzl
LtqgxoimCEIMoI79uKMilsAun5DpGwAL7ewq2L6s+j2IazGgC/c/35XiApVsjRM=
=yycO
-----END PGP SIGNATURE-----
    HERE

    @unsigned_commit_oid = "644bfda241e0ae7c487a6102d9db39798921464f"

    @signed_tag_oid = "2a9cb251b98d10386d29c8bd43f19f1922b87cc5"

    @signed_tag_payload = <<-HERE
object 644bfda241e0ae7c487a6102d9db39798921464f
type commit
tag signed
tagger Ben Toews <mastahyeti@users.noreply.github.com> 1455220502 -0700

my signed commit
    HERE

    @signed_tag_signature = <<-HERE
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAABAgAGBQJWvOcWAAoJEKAG779xTNk6g7kIAN7FtE8891iAO8verPxpgDdB
KmUbI5QKWJphEtTFiI6aBu3K2Nox6c6jn/tyC34sRkYapAjGK2nH9MTUEpUjJQML
i/y//1Z+mewxQnXm5wT4Q5O7RQyFXHh6YxMyXrJk0mNo0kOXGnL7Ni4uoXEDv8mq
+K9Fg7J/1bNzLUoavPtmwfe/ZSNfrXOLTwk/CueO0lQjwYieWdYOZtPuCfhMFfGK
xHW8sjGegty5vfBM6Z1TX8o2f79wYp6U6TA8QZUpE1m+wUm9gLKc+kIGWypafYJq
HLFkF3Gw4XDR6F9GT6IBbxaAhGYoxuH1qvoW/DC0ONYPVtF2tFoXaPlC7x7o2N4=
=qtws
-----END PGP SIGNATURE-----
    HERE

    @unsigned_tag_oid = "94a7f208672b6387c937a564e19ac96d778d3b98"
  end

  def test_parse_commit_signature_for_signed_commit
    result = @backend.parse_commit_signatures([@signed_commit_oid])
    assert_equal [[@signed_commit_signature, @signed_commit_payload]], result
  end

  def test_parse_tag_signature_for_signed_tag
    result = @backend.parse_tag_signatures([@signed_tag_oid])
    assert_equal [[@signed_tag_signature, @signed_tag_payload]], result
  end

  def test_parse_commit_signature_for_unsigned
    result = @backend.parse_commit_signatures([@unsigned_commit_oid])
    assert_equal [false], result
  end

  def test_parse_tag_signature_for_unsigned
    result = @backend.parse_tag_signatures([@unsigned_tag_oid])
    assert_equal [false], result
  end

  def test_parse_commit_signature_multiple_oids
    expected = [[@signed_commit_signature, @signed_commit_payload], false]
    actual = @backend.parse_commit_signatures([@signed_commit_oid, @unsigned_commit_oid])
    assert_equal expected, actual
  end

  def test_parse_tag_signature_multiple_oids
    expected = [[@signed_tag_signature, @signed_tag_payload], false]
    actual = @backend.parse_tag_signatures([@signed_tag_oid, @unsigned_tag_oid])
    assert_equal expected, actual
  end

  def test_parse_commit_signature_preserves_oid_order
    result  = @backend.parse_commit_signatures([@signed_commit_oid, @unsigned_commit_oid])
    reverse = @backend.parse_commit_signatures([@unsigned_commit_oid, @signed_commit_oid])
    assert_equal reverse, result.reverse
  end

  def test_parse_tag_signature_preserves_oid_order
    result  = @backend.parse_tag_signatures([@signed_tag_oid, @unsigned_tag_oid])
    reverse = @backend.parse_tag_signatures([@unsigned_tag_oid, @signed_tag_oid])
    assert_equal reverse, result.reverse
  end

  def test_parse_commit_signature_for_non_commit
    assert_raises GitRPC::InvalidObject do
      @backend.parse_commit_signatures([@signed_tag_oid])
    end
  end

  def test_parse_tag_signature_for_non_tag
    assert_raises GitRPC::InvalidObject do
      @backend.parse_tag_signatures([@signed_commit_oid])
    end
  end

  def test_parse_commit_signature_missing_object
    assert_raises GitRPC::ObjectMissing do
      @backend.parse_commit_signatures(["a"*40])
    end
  end

  def test_parse_tag_signature_missing_object
    assert_raises GitRPC::ObjectMissing do
      @backend.parse_tag_signatures(["a"*40])
    end
  end
end
