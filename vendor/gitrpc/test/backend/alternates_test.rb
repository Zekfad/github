# frozen_string_literal: true
require_relative "../setup"
require "gitrpc/backend"
require "repository_fixture"

# Tests GitRPC::Backend's support for multiple object stores. Rugged and spawn
# interfaces are supported.
class BackendAlternatesTest < Minitest::Test
  include GitRPC::Util

  def setup
    @repo = RepositoryFixture.new
    @repo.setup
    @repo_objects = [
      @repo.add_empty_commit("commit 0 in repo"),
      @repo.commit_files({"README" => "stuff" }, "commit 1 in repo"),
      @repo.add_empty_commit("commit 2 in repo")
    ]

    @alternate = RepositoryFixture.new("alternate.git")
    @alternate.setup
    @alternate_objects = [
      @alternate.add_empty_commit("commit 0 in alternate"),
      @alternate.commit_files({"README" => "stuff" }, "commit 1 in alternate"),
      @alternate.add_empty_commit("commit 2 in alternate")
    ]

    @backend = GitRPC::Backend.new(@repo.path, :alternates => ["#{@alternate.path}/objects"])
  end

  def teardown
    @repo.teardown
    @alternate.teardown
  end

  def test_read_objects_across_object_stores
    objects = @backend.read_objects([@repo_objects[0], @alternate_objects[1]])
    assert_equal 2, objects.size
    assert_equal @repo_objects[0], objects[0]["oid"]
    assert_equal @alternate_objects[1], objects[1]["oid"]
  end

  def test_spawn_git_command
    res = @backend.spawn_git("cat-file", ["-p", @repo_objects[0]])
    assert res["ok"]
    assert res["out"].start_with?("tree "), res["out"]

    res = @backend.spawn_git("cat-file", ["-p", @alternate_objects[1]])
    assert res["ok"]
    assert res["out"].start_with?("tree ")
  end

  def test_read_objects_missing_commit
    assert_raises GitRPC::ObjectMissing do
      @backend.read_objects([@repo_objects[0], "0" * 40, @alternate_objects[1]])
    end
  end

  def test_alternates_after_cleanup
    @backend.read_objects([@alternate_objects[0]])
    @backend.cleanup
    @backend.read_objects([@alternate_objects[1]])
  end
end
