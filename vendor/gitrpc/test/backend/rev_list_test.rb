# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

require "minitest/mock"

class BackendRevListTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }

    @backend = GitRPC::Backend.new(@fixture.path)
    @rugged = Rugged::Repository.new(@fixture.path)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @oid2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @oid3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")
    @oid4 = @fixture.commit_files({"AUTHORS" => "Brian"}, "commit 4")

    @tag_oid = @fixture.create_tag_object("v1.0", "We made it", @oid4, "commit")

    @head_oid3 = @backend.create_tree_changes(@oid2, {
      "message"   => "head commit 3",
      "committer" => @committer
    }, {"AUTHORS" => "JP"})
    @head_oid4 = @backend.create_tree_changes(@head_oid3, {
      "message"   => "head commit 4",
      "committer" => @committer
    }, {"AUTHORS" => "Josh"})
    @head_oid5 = @backend.create_tree_changes([@oid4, @head_oid4], {
      "message"   => "head commit 5",
      "committer" => @committer
    }, {"AUTHORS" => "Brian + Josh"})
  end

  def test_list_commit_parent_revisions
    res = @backend.rev_list([@oid1], [], {})
    assert_equal [@oid1], res

    res = @backend.rev_list([@oid2], [], {})
    assert_equal [@oid2, @oid1], res

    res = @backend.rev_list([@oid3], [], {})
    assert_equal [@oid3, @oid2, @oid1], res
  end

  def test_deferences_tag_object
    res = @backend.rev_list([@tag_oid], [], {})
    assert_equal [@oid4, @oid3, @oid2, @oid1], res
  end

  def test_list_commits_excluding_branch
    res = @backend.rev_list([@head_oid5], [], {})
    assert_equal [@head_oid5, @oid4, @oid3, @oid2, @oid1, @head_oid4, @head_oid3], res

    res = @backend.rev_list([@head_oid5], [@oid4], {})
    assert_equal [@head_oid5, @head_oid4, @head_oid3], res

    res = @backend.rev_list([@head_oid5], [@head_oid4], {})
    assert_equal [@head_oid5, @oid4, @oid3], res
  end

  def test_list_commits_including_multiple_commits
    res = @backend.rev_list([@oid4], [], {})
    assert_equal [@oid4, @oid3, @oid2, @oid1], res

    res = @backend.rev_list([@head_oid4], [], {})
    assert_equal [@head_oid4, @head_oid3, @oid2, @oid1], res

    res = @backend.rev_list([@oid4, @head_oid4], [], {})
    assert_equal [@oid4, @oid3, @oid2, @oid1, @head_oid4, @head_oid3], res
  end

  def test_list_commit_parent_revisions_in_reverse
    res = @backend.rev_list([@oid1], [], { "reverse" => true })
    assert_equal [@oid1], res

    res = @backend.rev_list([@oid2], [], { "reverse" => true })
    assert_equal [@oid1, @oid2], res

    res = @backend.rev_list([@oid3], [], { "reverse" => true })
    assert_equal [@oid1, @oid2, @oid3], res
  end

  def test_list_commit_merges
    res = @backend.rev_list([@oid4], [], { "merges" => true })
    assert_equal [], res

    res = @backend.rev_list([@head_oid5], [], { "merges" => true })
    assert_equal [@head_oid5], res
  end

  def test_list_commits_for_paths
    res = @backend.rev_list([@head_oid5], [], { "paths" => ["README"] })
    assert_equal [@oid3, @oid2, @oid1], res

    res = @backend.rev_list([@head_oid5], [], { "paths" => ["AUTHORS"] })
    assert_equal [@head_oid5, @oid4, @head_oid4, @head_oid3], res

    res = @backend.rev_list([@oid4], [], { "paths" => ["AUTHORS"] })
    assert_equal [@oid4], res
  end

  def test_list_commits_with_skip
    res = @backend.rev_list([@oid4], [], { "skip" => 0 })
    assert_equal [@oid4, @oid3, @oid2, @oid1], res

    res = @backend.rev_list([@oid4], [], { "skip" => 1 })
    assert_equal [@oid3, @oid2, @oid1], res

    res = @backend.rev_list([@oid4], [], { "skip" => 10 })
    assert_equal [], res
  end

  def test_raises_obj_missing_on_invalid_obj
    assert_raises GitRPC::ObjectMissing do
      @backend.rev_list([SecureRandom.hex(20)], [], {})
    end

    assert_raises GitRPC::ObjectMissing do
      @backend.rev_list([SecureRandom.hex(20)], [], {})
    end

    assert_raises GitRPC::ObjectMissing do
      @backend.rev_list([GitRPC::NULL_OID], [], {})
    end
  end

  def test_list_commit_parent_revisions_with_limit
    res = @backend.rev_list([@oid3], [], { "limit" => 1 })
    assert_equal [@oid3], res

    res = @backend.rev_list([@oid3], [], { "limit" => 2 })
    assert_equal [@oid3, @oid2], res
  end

  def test_requires_included_oid
    assert_raises TypeError do
      @backend.rev_list([nil], [], {})
    end
  end

  def test_raise_invalid_full_oid
    assert_raises Rugged::InvalidError do
      @backend.rev_list(["lol"], [], {})
    end
  end

  def test_raise_non_commit_object
    assert tree_oid = Rugged::Commit.lookup(@rugged, @oid1).tree.oid
    assert_raises GitRPC::InvalidObject do
      @backend.rev_list([tree_oid], [], {})
    end
  end
end
