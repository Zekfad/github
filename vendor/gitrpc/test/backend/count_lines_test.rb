# -*- encoding: utf-8 -*-
# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendCountLinesTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit = @fixture.commit_files({
      "test1"      => "hello\r\nworld\r\n",
      "test2"       => "somedata",
      "test3"       => "a\nb\nc\nd\ne",
    }, "commit 2")
    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @backend = nil
  end

  def test_line_counts
    t1 = @fixture.rev_parse("#{@commit}:test1")
    t2 = @fixture.rev_parse("#{@commit}:test2")
    t3 = @fixture.rev_parse("#{@commit}:test3")
    t4 = "0bf6ee3c1f3ea1badc747918547249e3d4bbebd4"

    counts = @backend.count_lines([t1, t2, t3, t4])
    assert_equal 2, counts[t1]
    assert_equal 1, counts[t2]
    assert_equal 5, counts[t3]
    assert_nil counts[t4]
  end
end
