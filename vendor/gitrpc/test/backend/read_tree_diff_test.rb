# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class BackendReadTreeDiff < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @commit3 = @fixture.commit_files({"README" => nil}, "commit 3")
    @commit4 = @fixture.commit_files({"README" => "Hi again."}, "commit 4")
    @commit5 = @fixture.commit_files({"foo/bar" => "test"}, "commit 5")
    @commit6 = @fixture.commit_files({"foo/nested/bar" => "test"}, "commit 6")

    work_tree = "#{@fixture.path}/work"
    @fixture.command "git --work-tree='#{work_tree}' mv README README.md"
    @fixture.commit_changes "rename"
    @rename = @fixture.rev_parse("HEAD")

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_read_tree_diffs
    diffs = @backend.read_tree_diff(@commit1, @commit2)
    assert diff = diffs.first

    assert_equal "modified", diff["status"]

    assert_equal "README", diff["old_file"]["path"]
    assert_equal 0100644, diff["old_file"]["mode"]
    assert_equal "63af512c383ca207450e4340019cf55737bb40a0", diff["old_file"]["oid"]

    assert_equal "README", diff["new_file"]["path"]
    assert_equal 0100644, diff["new_file"]["mode"]
    assert_equal "9407a031d6de7be6466c72c1d6d58c9c3d9d2866", diff["new_file"]["oid"]
  end

  def test_read_tree_added_diffs
    diffs = @backend.read_tree_diff(@commit1)
    assert diff = diffs.first

    assert_equal "added", diff["status"]

    assert_equal "README", diff["old_file"]["path"]
    assert_equal 0, diff["old_file"]["mode"]
    assert_equal "0000000000000000000000000000000000000000", diff["old_file"]["oid"]

    assert_equal "README", diff["new_file"]["path"]
    assert_equal 0100644, diff["new_file"]["mode"]
    assert_equal "63af512c383ca207450e4340019cf55737bb40a0", diff["new_file"]["oid"]
  end

  def test_read_tree_branch_added
    diffs = @backend.read_tree_diff(GitRPC::NULL_OID, @commit1)
    assert diff = diffs.first

    assert_equal "added", diff["status"]

    assert_equal "README", diff["old_file"]["path"]
    assert_equal 0, diff["old_file"]["mode"]
    assert_equal "0000000000000000000000000000000000000000", diff["old_file"]["oid"]

    assert_equal "README", diff["new_file"]["path"]
    assert_equal 0100644, diff["new_file"]["mode"]
    assert_equal "63af512c383ca207450e4340019cf55737bb40a0", diff["new_file"]["oid"]
  end

  def test_read_tree_branch_deleted
    diffs = @backend.read_tree_diff(@commit1, GitRPC::NULL_OID)
    assert diff = diffs.first

    assert_equal "deleted", diff["status"]

    assert_equal "README", diff["old_file"]["path"]
    assert_equal 0100644, diff["old_file"]["mode"]
    assert_equal "63af512c383ca207450e4340019cf55737bb40a0", diff["old_file"]["oid"]

    assert_equal "README", diff["new_file"]["path"]
    assert_equal 0, diff["new_file"]["mode"]
    assert_equal "0000000000000000000000000000000000000000", diff["new_file"]["oid"]
  end


  def test_read_tree_nothing
    diffs = @backend.read_tree_diff(GitRPC::NULL_OID, nil)
    assert_equal 0, diffs.size

    diffs = @backend.read_tree_diff(GitRPC::NULL_OID, GitRPC::NULL_OID)
    assert_equal 0, diffs.size
  end

  def test_read_tree_removed_diffs
    diffs = @backend.read_tree_diff(@commit2, @commit3)
    assert diff = diffs.first

    assert_equal "deleted", diff["status"]

    assert_equal "README", diff["old_file"]["path"]
    assert_equal 0100644, diff["old_file"]["mode"]
    assert_equal "9407a031d6de7be6466c72c1d6d58c9c3d9d2866", diff["old_file"]["oid"]

    assert_equal "README", diff["new_file"]["path"]
    assert_equal 0, diff["new_file"]["mode"]
    assert_equal "0000000000000000000000000000000000000000", diff["new_file"]["oid"]
  end

  def test_assume_parent
    parent_less = @backend.read_tree_diff(@commit2)
    parent_with = @backend.read_tree_diff(@commit1, @commit2)
    assert_equal parent_less, parent_with
  end

  def test_read_tree_diffs_paths
    # test commits without changes in the paths
    diffs = @backend.read_tree_diff(@commit1, @commit2, paths: ["foo/"])
    assert_equal 0, diffs.size

    # test commits with a change in the paths
    diffs = @backend.read_tree_diff(@commit1, @commit5, paths: ["foo/"])
    assert_equal 1, diffs.size

    # test commits with changes within nested folders within the paths
    diffs = @backend.read_tree_diff(@commit1, @commit6, paths: ["foo/"])
    assert_equal 2, diffs.size
  end


  def test_read_tree_diffs_diff_trees
    commit1_tree_oid = @backend.rugged.lookup(@commit1).tree.oid

    # test does not allow trees
    assert_raises GitRPC::InvalidObject do
      @backend.read_tree_diff(commit1_tree_oid, @commit4)
    end

    # test allows trees
    diffs = @backend.read_tree_diff(commit1_tree_oid, @commit4, diff_trees: true)
    assert_equal 1, diffs.size
  end
end
