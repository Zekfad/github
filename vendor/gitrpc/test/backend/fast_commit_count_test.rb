# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendFastCommitCountTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @backend = nil
  end

  def test_fast_commit_count_rejects_invalid_oids
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.fast_commit_count("--output=/usr/bin/git")
    end
  end
end
