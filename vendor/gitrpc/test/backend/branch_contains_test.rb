# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendBranchContainsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @backend = nil
  end

  def test_branch_contains_rejects_invalid_oid
    assert_raises(GitRPC::InvalidOid) do
      @backend.branch_contains("--output=/usr/bin/git")
    end
  end
end
