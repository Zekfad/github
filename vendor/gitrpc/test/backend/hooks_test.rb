# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendHooksTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_post_mirror_hook
    wrong_updated_refs = ["nope"]
    updated_refs = ["foobar"]

    @fixture.command("printf '#!/bin/sh\ngrep -q foobar\n' > hooks/post-mirror && chmod 755 hooks/post-mirror")
    assert_raises GitRPC::CommandFailed do
      @backend.post_mirror_hook(wrong_updated_refs)
    end
    assert_nil @backend.post_mirror_hook(updated_refs)
  end

  def test_pre_receive_hook
    @fixture.command("printf '#!/bin/sh\ngrep foobar\n' > hooks/pre-receive && chmod 755 hooks/pre-receive")

    res = @backend.pre_receive_hook("nope", {})
    refute res["ok"]
    assert_equal "", res["out"]
    assert_equal "", res["err"]
    res = @backend.pre_receive_hook("foobar", {})
    assert res["ok"]
    assert_equal "foobar\n", res["out"]
    assert_equal "", res["err"]
  end

  def test_pre_receive_hook_utf8
    @fixture.command("printf '#!/bin/sh\necho \"stdout\xe2\x98\x98\"\necho \"stderr\xE2\x98\xA0\" 1>&2\n' > hooks/pre-receive && chmod 755 hooks/pre-receive")

    res = @backend.pre_receive_hook("utf8-test", {})
    assert res["ok"]
    assert_equal "UTF-8", res["out"].encoding.name
    assert_equal "UTF-8", res["err"].encoding.name
    assert_equal "stdout\xe2\x98\x98\n", res["out"] # SHAMROCK U+2618
    assert_equal "stderr\xe2\x98\xa0\n", res["err"] # SKULL AND CROSSBONES U+2620
  end

end
