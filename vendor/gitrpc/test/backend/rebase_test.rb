# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendRebaseTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
    @rugged = @backend.rugged

    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("Initial import")

    @fixture.create_branch("topic")
    @fixture.create_branch("conflict")

    @master_commit = @fixture.commit_files({"one" => "foo"}, "Master")
    @topic_commit = @fixture.commit_files({"two" => "bar"}, "Topic", { :branch => "topic" })
    @conflicting_commit = @fixture.commit_files({"one" => "baz", "two" => "bar"}, "Conflict", { :branch => "conflict" })
  end

  def test_rebase
    expected_commit_oid = "6cce1314f2053264287fac0f96d969fd0c1cb859"

    oid = @backend.rebase(@topic_commit, @master_commit, @committer)
    assert_equal expected_commit_oid, oid
  end

  def test_rebase_and_merge_produce_same_tree
    commit_oid, error = @backend.create_merge_commit("master", "topic", @committer, "Example merge")
    assert_nil error

    assert rebase_oid = @backend.rebase(@topic_commit, @master_commit, @committer)

    assert rebase_commit = @rugged.lookup(rebase_oid)
    assert merge_commit = @rugged.lookup(commit_oid)

    assert_equal rebase_commit.tree, merge_commit.tree
  end

  def test_rebase_conflict
    assert_nil @backend.rebase(@conflicting_commit, @master_commit, @committer)
  end

  def test_rebase_with_timeout
    stub_values = [0, 10]
    Process.stub(:clock_gettime, lambda { |_| stub_values.shift }) do
      @backend.rebase(@topic_commit, @master_commit, @committer, timeout: 10)
    end

    stub_values = [0, 11]
    Process.stub(:clock_gettime, lambda { |_| stub_values.shift }) do
      assert_raises GitRPC::Backend::RebaseTimeout do
        @backend.rebase(@topic_commit, @master_commit, @committer, timeout: 10)
      end
    end
  end
end
