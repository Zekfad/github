# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendDistinctCommitsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_distinct_commits
    commits = @backend.distinct_commits("refs/heads/master")
    assert_equal [@oid2, @oid1], commits

    commits = @backend.distinct_commits("refs/heads/master", count: true)
    assert_equal 2, commits
  end

  def test_distinct_commits_rejects_unsanitary_refs
    assert_raises(GitRPC::UnsafeArgument) do
      @backend.distinct_commits("--output=/usr/bin/git")
    end
  end

  def test_distinct_commits_rejects_unsanitary_refs_with_exclude
    assert_raises(GitRPC::UnsafeArgument) do
      @backend.distinct_commits("refs/heads/master",
        exclude: ["--output=/usr/bin/git"])
    end
  end
end
