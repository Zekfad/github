# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

class BackendFilterableCommitCount < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @fixture.add_empty_commit("commit from the past", :commit_time => @past_time)
    @head = @fixture.rev_parse("HEAD")

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_timeout
    err = assert_raises(GitRPC::Timeout) { @backend.filterable_commit_count(@head, nil, nil, nil, ["otherperson@example.com"], 0.0000000001) }
  end

  def test_filterable_commit_count_rejects_invalid_starting_oids
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.filterable_commit_count("--output=/usr/bin/git",
        nil, nil, nil, ["otherperson@example.com"])
    end
  end
end
