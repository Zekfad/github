# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendFindCredsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits
    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    has_tokens = "test = 'a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd'"
    has_tokens2 = "test = '4a7b78a8276ad872facaab32029ad4cee4ab2efd'"
    has_tokens3 = <<-END
var r = request.get('AKIAI6KIQRRVMGK3WK5Q',
  { aws:
    { key: 'j4kaxM7TUiN7Ou0//v1ZqOVn3Aq7y1ccPh/tHTna'
    , secret: 'http://f9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd@github.com'
    , bucket: 'log.curlybracecast.com'
    }
  }, function (e, resp, body) {
    console.log(r.headers)
    console.log(body)
  }
)
END
    has_tokens4 = "test = datadog"
    has_tokens5 = "test = hapikey"
    has_tokens6 = "test = messagebird"

    # We want to ensure each commit has an incrementing timestamp, as git-find-
    # creds ends up invoking rev-list, which orders things by commit timestamp.
    current_time = Time.now.to_i
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1", commit_time: current_time)
    @blob1 = @fixture.rugged.blob_at(@oid1, "README")
    @oid2 = @fixture.commit_files({"README" => "Hello2."}, "commit 2", commit_time: current_time + 1)
    @blob2 = @fixture.rugged.blob_at(@oid2, "README")
    @oid3 = @fixture.commit_files({"README" => has_tokens}, "commit 3", commit_time: current_time + 2)
    @blob3 = @fixture.rugged.blob_at(@oid3, "README")
    @fixture.create_branch("branch")
    @oid4 = @fixture.commit_files(
      {"README" => has_tokens2},
      "commit 4",
      branch: "branch",
      commit_time: current_time + 3
    )
    @blob4 = @fixture.rugged.blob_at(@oid4, "README")
    @oid5 = @fixture.commit_files({"README" => has_tokens3}, "commit 5", commit_time: current_time + 4)
    @blob5 = @fixture.rugged.blob_at(@oid5, "README")
    # Modify a file to include tokens we have already seen
    @oid6 = @fixture.commit_files(
      {
        "README" => [has_tokens, has_tokens2, has_tokens3].join("\n")
      },
      "commit 6",
      commit_time: current_time + 5
    )
    @blob6 = @fixture.rugged.blob_at(@oid6, "README")
    @oid7 = @fixture.commit_files(
      {
        "README" => has_tokens4
      },
      "commit 7",
      commit_time: current_time + 6
    )
    @blob7 = @fixture.rugged.blob_at(@oid7, "README")
    @oid8 = @fixture.rugged_commit_files(
      { "README" => has_tokens5 },
      "commit 8",
      time: Time.local(current_time + 7),
      qualified_ref: "refs/pull/test",
      parents: [@oid7])
    @blob8 = @fixture.rugged.blob_at(@oid8, "README")
    @oid9 = @fixture.rugged_commit_files(
      { "README" => has_tokens6 },
      "commit 9",
      time: Time.local(current_time + 8),
      qualified_ref: "refs/__gh__/test",
      parents: [@oid8])
    @blob9 = @fixture.rugged.blob_at(@oid9, "README")
  end

  def test_find_creds
    # Throws argument error without max_result passed in
    assert_raises(ArgumentError, /max_result must be set/) do
      creds = @backend.find_creds([[GitRPC::EMPTY_TREE_OID, @oid4, "refs/heads/master"]])
    end

    # Works with specifying a EMPTY_TREE_OID include_oids
    creds = @backend.find_creds([[GitRPC::EMPTY_TREE_OID, @oid4, "refs/heads/master"]], { max_result: 100 })
    assert_equal 0, creds.size

    # Finds tokens that exist in branch but not those that exist in master
    creds = @backend.find_creds([[GitRPC::NULL_OID, @oid4, "refs/heads/branch"]], { max_result: 100 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 3, creds.size
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48  }

    # Finds tokens between valid OIDs. This test purposefully avoid using OIDs
    # that overlap with the branch we created, since this test isn't meant to
    # exercise the logic for how we exclude objects that already exist (and are
    # untouched) in another ref.
    creds = @backend.find_creds([[@oid3, @oid5, "refs/heads/master"]], { max_result: 100 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 4, creds.size
    assert_includes creds, { type: "AWS_KEYID", token: "AKIAI6KIQRRVMGK3WK5Q", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 21, end_column: 41 }
    assert_includes creds, { type: "AWS_SECRET", token: "j4kaxM7TUiN7Ou0//v1ZqOVn3Aq7y1ccPh/tHTna", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 3, end_line: 3, start_column: 12, end_column: 52 }
    assert_includes creds, { type: "GITHUB", token: "f9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob5.oid, report_url: "", start_line: 4, end_line: 4, start_column: 22, end_column: 62 }

    # Finds all the tokens when we simulate push of all new branches
    creds = @backend.find_creds([
      [GitRPC::NULL_OID, @oid5, "refs/heads/master"],
      [GitRPC::NULL_OID, @oid4, "refs/heads/branch"],
    ],  { max_result: 100 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 10, creds.size
    assert_includes creds, { type: "AWS_KEYID", token: "AKIAI6KIQRRVMGK3WK5Q", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 21, end_column: 41 }
    assert_includes creds, { type: "AWS_SECRET", token: "j4kaxM7TUiN7Ou0//v1ZqOVn3Aq7y1ccPh/tHTna", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 3, end_line: 3, start_column: 12, end_column: 52 }
    assert_includes creds, { type: "GITHUB", token: "f9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob5.oid, report_url: "", start_line: 4, end_line: 4, start_column: 22, end_column: 62 }
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "GITHUB", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }

    # Finds all tokens we commited in "commit 6". We want to validate these are
    # found in @blob6. We want to validate this since the next test (scanning
    # all commits in all branches) shouldn't report the matches in "commit 6",
    # as `git-find-creds` calls `hypercredscan` with the `-dedup` option. This
    # causes git-find-creds to only report the first blob (commit
    # chronologically) where the token was found. This test gives us confidence
    # that `-dedup` is working as intended.
    creds = @backend.find_creds([[@oid5, @oid6, "refs/heads/master"]], { max_result: 100 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 10, creds.size
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob6.oid, report_url: "", start_line: 2, end_line: 2, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob6.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 2, end_line: 2, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_KEYID", token: "AKIAI6KIQRRVMGK3WK5Q", blob: @blob6.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 3, end_line: 3, start_column: 21, end_column: 41 }
    assert_includes creds, { type: "AWS_SECRET", token: "j4kaxM7TUiN7Ou0//v1ZqOVn3Aq7y1ccPh/tHTna", blob: @blob6.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 5, end_line: 5, start_column: 12, end_column: 52 }
    assert_includes creds, { type: "GITHUB", token: "f9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob6.oid, report_url: "", start_line: 6, end_line: 6, start_column: 22, end_column: 62 }
    assert_includes creds, { type: "GITHUB", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob6.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob6.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }

    # Finds all tokens we commited across all commits in all branches. We want
    # to validate that, even though all prior tokens were re-added in "commit
    # 6", that we report on the original commits that introduced these tokens.
    # We shouldn't report the matches in "commit 6", as `git-find-creds` calls
    # `hypercredscan` with the `-dedup` option. This causes git-find-creds to
    # only report the first blob (commit chronologically) where the token was
    # found. This test gives us confidence that `-dedup` is working as
    # intended.
    creds = @backend.find_creds([
      [@oid1, @oid6, "refs/heads/master"],
      [@oid1, @oid4, "refs/heads/branch"]
    ], { dedup_results: true, max_result: 100 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 10, creds.size
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_KEYID", token: "AKIAI6KIQRRVMGK3WK5Q", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 21, end_column: 41 }
    assert_includes creds, { type: "AWS_SECRET", token: "j4kaxM7TUiN7Ou0//v1ZqOVn3Aq7y1ccPh/tHTna", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 3, end_line: 3, start_column: 12, end_column: 52 }
    assert_includes creds, { type: "GITHUB", token: "f9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob5.oid, report_url: "", start_line: 4, end_line: 4, start_column: 22, end_column: 62 }
    assert_includes creds, { type: "GITHUB", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }

    # Triggers full repository history scan when you specify an empty ref array
    creds = @backend.find_creds([], {dedup_results: true, max_result: 100})
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 11, creds.size
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_KEYID", token: "AKIAI6KIQRRVMGK3WK5Q", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 21, end_column: 41 }
    assert_includes creds, { type: "AWS_SECRET", token: "j4kaxM7TUiN7Ou0//v1ZqOVn3Aq7y1ccPh/tHTna", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 3, end_line: 3, start_column: 12, end_column: 52 }
    assert_includes creds, { type: "GITHUB", token: "f9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob5.oid, report_url: "", start_line: 4, end_line: 4, start_column: 22, end_column: 62 }
    assert_includes creds, { type: "GITHUB", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "DATADOG_NAME_PRESENCE", token: "datadog", blob: @blob7.oid, report_url: "", start_line: 1, end_line: 1, start_column: 7, end_column: 14 }

    # We refute here to ensure that these tokens from pull/ghinternal refs are not included when performing a full history scan.
    refute_includes creds, { type: "HUBSPOT_HAPIKEY_NAME_PRESENCE", token: "hapikey", blob: @blob8.oid, report_url: "", start_line: 1, end_line: 1, start_column: 7, end_column: 14 }
    refute_includes creds, { type: "MESSAGEBIRD_NAME_PRESENCE", token: "messagebird", blob: @blob9.oid, report_url: "", start_line: 1, end_line: 1, start_column: 7, end_column: 18 }

    # Fails to find any tokens when ref OIDs are reversed
    creds = @backend.find_creds([[@oid5, @oid3, "refs/heads/master"]], {max_result: 100})
    assert_equal 0, creds.size

    # Limit creds returned on max_result value passed in with empty references
    creds = @backend.find_creds([], { max_result: 7 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 7, creds.size
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_KEYID", token: "AKIAI6KIQRRVMGK3WK5Q", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 21, end_column: 41 }
    assert_includes creds, { type: "DATADOG_TOKEN", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "https://app.datadoghq.com/api/v1/check_github", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "DATADOG_TOKEN", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://app.datadoghq.com/api/v1/check_github", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "GITHUB", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }

    # Limit creds returned on skip_fp being passed in with empty reference
    creds = @backend.find_creds([], { max_result: 5, skip_fp: true })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 5, creds.size
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_KEYID", token: "AKIAI6KIQRRVMGK3WK5Q", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 21, end_column: 41 }
    assert_includes creds, { type: "GITHUB", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_SECRET", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }

    # list any datadog creds found in commit.
    creds = @backend.find_creds([[@oid6, @oid7, "refs/heads/master"]], { max_result: 1, skip_fp: false })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 1, creds.size
    assert_includes creds, { type: "DATADOG_NAME_PRESENCE", token: "datadog", blob: @blob7.oid, report_url: "", start_line: 1, end_line: 1, start_column: 7, end_column: 14 }

    # skip any datadog creds found in commit when skip_fp is set
    creds = @backend.find_creds([[@oid6, @oid7, "refs/heads/master"]], { max_result: 1, skip_fp: true })
    assert_equal 0, creds.size

    # Limit creds returned on max_result value passed in with non-empty references
    creds = @backend.find_creds([[GitRPC::NULL_OID, @oid4, "refs/heads/branch"]], { max_result: 2 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 2, creds.size
    assert_includes creds, { type: "AWS_SECRET", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "DATADOG_TOKEN", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "https://app.datadoghq.com/api/v1/check_github", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }

    # Results buffered and returned when references specified
    creds = @backend.find_creds([[GitRPC::NULL_OID, @oid4, "refs/heads/branch"]], { buffer_results: true, max_result: 100 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 1, creds.size
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }

    # Results buffered and returned  with no references specified
    creds = @backend.find_creds([], { buffer_results: true, max_result: 100 })
    creds.map! { |cred| JSON.parse(cred, symbolize_names: true) }
    assert_equal 12, creds.size
    assert_includes creds, { type: "GITHUB", token: "4a7b78a8276ad872facaab32029ad4cee4ab2efd", blob: @blob4.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
    assert_includes creds, { type: "AWS_KEYID", token: "AKIAI6KIQRRVMGK3WK5Q", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 1, end_line: 1, start_column: 21, end_column: 41 }
    assert_includes creds, { type: "AWS_SECRET", token: "j4kaxM7TUiN7Ou0//v1ZqOVn3Aq7y1ccPh/tHTna", blob: @blob5.oid, report_url: "https://awsrisks.com/submitkeys", start_line: 3, end_line: 3, start_column: 12, end_column: 52 }
    assert_includes creds, { type: "GITHUB", token: "f9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob5.oid, report_url: "", start_line: 4, end_line: 4, start_column: 22, end_column: 62 }
    assert_includes creds, { type: "GITHUB", token: "a9fdc6e9e33e3b0afe618b0ef4fe4f3dc42014fd", blob: @blob3.oid, report_url: "", start_line: 1, end_line: 1, start_column: 8, end_column: 48 }
  end
end
