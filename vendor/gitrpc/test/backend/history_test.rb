# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

class BackendHistoryTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1", {:email => "guy@example.com"})
    @oid2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2", {:email => "Guy@example.com"})
    @oid3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3", {:email => "someguy@example.com"})
    @oid4 = @fixture.commit_files({"AUTHORS" => "Brian"}, "commit 4", {:email => "SomeGuy@example.com"})
  end

  def test_list_commit_parent_revisions
    res = @backend.list_revision_history(@oid1)
    assert_equal [@oid1], res
    res = @backend.count_revision_history(@oid1)
    assert_equal 1, res

    res = @backend.list_revision_history(@oid2)
    assert_equal [@oid2, @oid1], res
    res = @backend.count_revision_history(@oid2)
    assert_equal 2, res

    res = @backend.list_revision_history(@oid3)
    assert_equal [@oid3, @oid2, @oid1], res
    res = @backend.count_revision_history(@oid3)
    assert_equal 3, res
    res = @backend.count_revision_history("HEAD")
    assert_equal 4, res

    assert_raises(GitRPC::InvalidCommitish) do
      @backend.list_revision_history("--output=/usr/bin/git")
    end

    res = @backend.list_revision_history_multiple([@oid1])
    assert_equal [@oid1], res
    res = @backend.list_revision_history_multiple([@oid2])
    assert_equal [@oid2, @oid1], res
    res = @backend.list_revision_history_multiple([@oid1, @oid2])
    assert_equal [@oid1, @oid2], res
    res = @backend.list_revision_history_multiple([@oid2], exclude: [@oid1])
    assert_equal [@oid2], res
    res = @backend.list_revision_history_multiple([@oid4])
    assert_equal [@oid4, @oid3, @oid2, @oid1], res
    res = @backend.list_revision_history_multiple([@oid4], findobjs: [@oid2])
    assert_equal [@oid2], res
  end

def test_list_revisions_by_author
    res = @backend.list_revision_history_multiple([@oid4], authors: ["guy@example.com"])
    assert_equal [@oid1], res
    res = @backend.list_revision_history_multiple([@oid4], authors: ["guy@example.com", "Guy@example.com"])
    assert_equal [@oid2, @oid1], res
    res = @backend.list_revision_history_multiple([@oid4], authors: ["SomeGuy@example.com"])
    assert_equal [@oid4], res
    res = @backend.list_revision_history_multiple([@oid4], authors: ["SomeGuy@example.com"], regexp_ignore_case: true)
    assert_equal [@oid4, @oid3], res
    res = @backend.list_revision_history_multiple([@oid4], authors: [".*guy@example.com"])
    assert_equal [@oid3, @oid1], res
    res = @backend.list_revision_history_multiple([@oid4], authors: [".*guy@example.com"], regexp_ignore_case: true)
    assert_equal [@oid4, @oid3, @oid2, @oid1], res
    res = @backend.list_revision_history_multiple([@oid4], authors: [".*guy@example.com"], regexp_ignore_case: true, fixed_strings: true)
    assert_equal [], res

    count = @backend.count_revision_history(@oid4, authors: ["guy@example.com"])
    assert_equal 1, count
    count = @backend.count_revision_history(@oid4, authors: ["guy@example.com", "Guy@example.com"])
    assert_equal 2, count
    count = @backend.count_revision_history(@oid4, authors: ["SomeGuy@example.com"], regexp_ignore_case: true)
    assert_equal 2, count
    count = @backend.count_revision_history(@oid4, authors: [".*guy@example.com"], regexp_ignore_case: true)
    assert_equal 4, count
    count = @backend.count_revision_history(@oid4, authors: [".*guy@example.com"], regexp_ignore_case: true, fixed_strings: true)
    assert_equal 0, count
end

  def test_raises_obj_missing_on_invalid_obj
    assert_raises GitRPC::ObjectMissing do
      @backend.list_revision_history(SecureRandom.hex(20))
    end

    assert_raises GitRPC::ObjectMissing do
      @backend.list_revision_history(SecureRandom.hex(20), limit: 10)
    end

    assert_raises GitRPC::ObjectMissing do
      @backend.list_revision_history_multiple([SecureRandom.hex(20)])
    end

    assert_raises GitRPC::InvalidFullOid do
      @backend.list_revision_history_multiple(["HEAD"])
    end
  end

  def test_list_commit_parent_revisions_with_limit
    res = @backend.list_revision_history(@oid3, limit: 1)
    assert_equal [@oid3], res

    res = @backend.list_revision_history(@oid3, limit: 2)
    assert_equal [@oid3, @oid2], res

    res = @backend.list_revision_history_multiple([@oid4], max: 3)
    assert_equal [@oid4, @oid3, @oid2], res
  end

  def test_list_commit_parent_revisions_with_offset
    res = @backend.list_revision_history(@oid3, offset: 1)
    assert_equal [@oid2, @oid1], res

    res = @backend.list_revision_history(@oid3, offset: 2)
    assert_equal [@oid1], res

    res = @backend.list_revision_history_multiple([@oid4], skip: 2)
    assert_equal [@oid2, @oid1], res
  end

  def test_list_commit_parent_revisions_with_pathspec
    res = @backend.list_revision_history(@oid4, path: "AUTHORS")
    assert_equal [@oid4], res

    res = @backend.list_revision_history(@oid3, path: "AUTHORS")
    assert_equal [], res

    res = @backend.list_revision_history_multiple([@oid4], path: "AUTHORS")
    assert_equal [@oid4], res
    res = @backend.list_revision_history_multiple([@oid4], path: "README")
    assert_equal [@oid3, @oid2, @oid1], res
    res = @backend.list_revision_history_multiple([@oid4], path: "nonexistent")
    assert_equal [], res
  end

  def test_search_revision_history
    res = @backend.search_revision_history("grep", "foo", "HEAD")
    assert_equal [], res
    res = @backend.search_revision_history("grep", "commit 1", "HEAD")
    assert_equal [@oid1], res
    res = @backend.search_revision_history("grep", "Commit 1", "HEAD")
    assert_equal [], res
    res = @backend.search_revision_history("grep", "commit", "HEAD")
    assert_equal [@oid4, @oid3, @oid2, @oid1], res
    res = @backend.search_revision_history("grep", "commit", "HEAD", max: 2)
    assert_equal [@oid4, @oid3], res
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.search_revision_history("grep", "foo", "--output=/usr/bin/git")
    end
  end

  def test_list_revisions_enforces_valid_commitish
    assert_raises GitRPC::InvalidCommitish do
      @backend.count_revision_history("--output=/tmp/owned")
    end
  end

  def test_timeout_in_list_revision_history_multiple
    assert_raises(GitRPC::Timeout) {
      @backend.list_revision_history_multiple([@oid1], path: "AUTHORS", timeout: 0.0000000001)
    }
  end

  def test_large_number_of_commits_in_list_revision_history_multiple
    # Generate a large number of commit hashes. For context: https://github.com/github/dsp-secret-scanning/issues/323
    large_commit_set = 10000.times.map { @oid1 }

    res = @backend.list_revision_history_multiple([], findobjs: large_commit_set)

    assert_equal [@oid1], res
  end
end
