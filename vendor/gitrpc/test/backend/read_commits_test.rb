# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

# Tests the GitRPC::Backend::Commits module methods
class BackendReadCommitsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("commit 1")
    @commit1 = @fixture.rev_parse("HEAD")

    @fixture.add_empty_commit("commit 2")
    @commit2 = @fixture.rev_parse("HEAD")

    @fixture.add_empty_commit("commit 3")
    @commit3 = @fixture.rev_parse("HEAD")
  end

  def test_read_commits_single_commit_data
    res = @backend.read_commits([@commit1])
    assert_equal 1, res.size
    assert_equal "commit", res[0]["type"]
    assert_equal @commit1, res[0]["oid"]
    assert_equal "commit 1\n", res[0]["message"]
    assert_equal "UTF-8", res[0]["encoding"]

    assert_equal "Some Author", res[0]["author"][0]
    assert_equal "someone@example.org", res[0]["author"][1]
    assert_equal [1342581713, -25200], res[0]["author"][2]

    assert !res[0]["committer"].nil?
  end

  def test_read_commits_multiple
    res = @backend.read_commits([@commit1, @commit3, @commit2])
    assert_equal 3, res.size
    assert_equal @commit1, res[0]["oid"]
    assert_equal @commit3, res[1]["oid"]
    assert_equal @commit2, res[2]["oid"]
  end

  def test_read_commits_missing
    assert_raises GitRPC::ObjectMissing do
      @backend.read_commits([@commit1, "0" * 40, @commit2])
    end
  end

  def test_read_commits_gpgsig
    skip "This uses the gitrpc repo as the test fixture, we don't have a repo now that we're inside dotcom"

    path = File.expand_path("../../../.git", __FILE__)
    backend = GitRPC::Backend.new(path)
    res = backend.read_commits(["0344dd6c051f47b9b27ca3429d561405b4a0796f"])
    commit = res[0]
    assert_equal "This commit is GPG signed\n", commit["message"]
  end

  def test_read_commits_with_trailers
    expected_message = <<~MSG
      My fancy commit message

      Co-authored-by: Brian Lopez <brian@github.com>
      Co-authored-by: Chester N. <chesterbr@github.com>
      Signed-off-by: Parker M\xDFoore <parkr@github.com>
    MSG

    commit4 = @fixture.add_simple_empty_commit(expected_message)

    res = @backend.read_objects([commit4])
    refute_operator res.first, :has_key?, "trailers"

    [:git, :regexp].each do |method|
      res = @backend.read_objects([commit4], nil, false, read_trailers: method)
      commit = res[0]
      assert_operator commit, :has_key?, "trailers"
      assert_equal 1, res.size
      assert_equal expected_message, commit["message"]
      assert commit["trailers"].key?("co-authored-by")
      assert commit["trailers"].key?("signed-off-by")
      assert_equal commit["trailers"]["co-authored-by"], [
        "Brian Lopez <brian@github.com>",
        "Chester N. <chesterbr@github.com>",
      ]
      expected_trailer = "Parker M\xDFoore <parkr@github.com>".dup.force_encoding("ISO-8859-1")
      assert_equal [expected_trailer], commit["trailers"]["signed-off-by"]
    end
  end

  def test_trailers_mixed_line_endings
    message = "test\n\nmultiple authors!\r\n\r\nCo-authored-by: Matt Clark <mclark@github.com>"
    commit_oid = @fixture.add_simple_empty_commit(message)
    [:git, :regexp].each do |method|

      res = @backend.read_objects([commit_oid], nil, false, read_trailers: method)
      commit = res[0]

      assert_operator commit, :has_key?, "trailers"
      assert commit["trailers"].key?("co-authored-by")
      assert_equal ["Matt Clark <mclark@github.com>"], commit["trailers"]["co-authored-by"]
    end
  end

  def test_read_commits_with_mixed_valid_and_invalid_trailers
    expected_message = <<~MSG
      I wrote a small library which does a neat thing when you pass it an option.

      For example, you need to send this option:

      Co-authored-by: George Clooney <george@github.com>
      Signed-off-by: Homer Simpson <homer@github.com>

      Config-Value: yes
      Some-Other: hello

      Signed-off-by: Matt Clark <mclark@github.com>
      Co-authored-by Brian Lopez <brian@github.com>
      Co-authored-by: Chester N. <chesterbr@github.com>
      #Co-authored-by: someone that wont show up
      Signed-off-by:Parker Moore <parkr@github.com>
      signed-off-by: Parker Moore <parkr@github.com>
      Co-authored-by: Arthur Schreiber <arthurschreiber@github.com>
      Co-authored-by: 🔥Céline Dîøn <utf8isgreat@gmail.com>
      Signed-Off-BY: Parker Moore <parkr@github.com>
        this is also part
      Made-possible-by: Kevin Bacon [bacon@kevin.net]
      Made-possible-by: yes
      A1234-yup: should match
      1234-nope: shouldnt match



    MSG

    @fixture.add_empty_commit(expected_message)

    commit4 = @fixture.rev_parse("HEAD")

    commit = @backend.read_objects([commit4]).first
    refute_operator commit, :has_key?, "trailers"

    [:git, :regexp].each do |method|
      commit = @backend.read_objects([commit4], nil, false, read_trailers: method).first
      assert_operator commit, :has_key?, "trailers"
      trailers = commit["trailers"]
      refute trailers.key?("Config-Value") # TODO should not match non-trailer headers
      refute trailers.key?("Some-Other")
      assert_equal 5, trailers.keys.count
      assert trailers.key?("a1234-yup")
      assert trailers.key?("co-authored-by")
      assert trailers.key?("signed-off-by")
      assert trailers.key?("made-possible-by")
      assert_equal [
        "Chester N. <chesterbr@github.com>",
        "Arthur Schreiber <arthurschreiber@github.com>",
        "🔥Céline Dîøn <utf8isgreat@gmail.com>"
      ], trailers["co-authored-by"]

      # TODO we should also pass this (continuation)
      assert_equal [
        "Matt Clark <mclark@github.com>",
        "Parker Moore <parkr@github.com>",
        "Parker Moore <parkr@github.com> this is also part",
      ], trailers["signed-off-by"]
      assert_equal [
        "Kevin Bacon [bacon@kevin.net]",
        "yes"
      ], trailers["made-possible-by"]
    end
  end

  def test_trailers_made_invalid_subsequent_regular_text
    message = <<~MSG
      My fancy commit message

      Co-authored-by: Brian Lopez <brian@github.com>
      Co-authored-by: Chester N. <chesterbr@github.com>
      Signed-off-by: Parker Moore <parkr@github.com>

      This was a really good pairing session, I really enjoyed it!
    MSG

    @fixture.add_empty_commit(message)

    oid = @fixture.rev_parse("HEAD")
    [:git, :regexp].each do |method|
      res = @backend.read_objects([oid], nil, false, read_trailers: method)
      commit = res[0]
      assert_empty commit["trailers"]
    end
  end
end
