# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class BackendGhGraphTest < Minitest::Test

  DEFAULT_EMAIL = "someone@example.org"

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @time = Time.local(2000)
    @backend = GitRPC::Backend.new(@fixture.path)
    @caches = GitRPC::Backend::ContributionCaches.new(@backend)
  end

  def teardown
    @fixture.teardown
  end

  def zone_offset
    Time.zone_offset(@time.zone)
  end

  def setup_test_commits
    @oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "commit 1", time: @time)

    message = "commit 2\n\nco-authored-by: Homer Simpson <hsimpson@github.com>"
    @oid2 = @fixture.rugged_commit_files({ "README" => "Hello.\nWorld.\n" }, message, time: @time, parents: [@oid1])
  end

  def self.all_versions
    GitRPC::Backend::ContributionCaches::LATEST_VERSION.downto(GitRPC::Backend::ContributionCaches::FIRST_VERSION).to_a
  end

  def test_clear_graph_cache
    v1_path = File.join(@fixture.path, GitRPC::Backend::ContributionCaches::V1::PATH)
    v2_path = File.join(@fixture.path, GitRPC::Backend::ContributionCaches::V2::PATH)
    refute File.exist?(v1_path)
    refute File.exist?(v2_path)

    # first clear without any cache to ensure this is safe
    @backend.clear_graph_cache
    refute File.exist?(v1_path)
    refute File.exist?(v2_path)

    v2 = @caches.new_instance(version: 2)
    v2.write("a" * 40, [[1, 2, 3, 4, "email", 5]])

    v1 = @caches.new_instance(version: 1)
    v1.write("a" * 40, [[1, 2, 3, 4, "email", 5]])

    assert File.exist?(v1_path)
    assert File.exist?(v2_path)

    @backend.clear_graph_cache
    refute File.exist?(v1_path)
    refute File.exist?(v2_path)
  end

  all_versions.each do |version|
    define_method("test_V#{version}_happy_path") do
      setup_test_commits

      expected_entries = if version == 1
        [
          [@time.to_i, 946166400, 2, 1, DEFAULT_EMAIL, zone_offset],
          [@time.to_i, 946166400, 1, 0, DEFAULT_EMAIL, zone_offset]
        ]
      else
        [
          [@time.to_i, 946166400, 2, 1, [DEFAULT_EMAIL, "hsimpson@github.com"], zone_offset],
          [@time.to_i, 946166400, 1, 0, [DEFAULT_EMAIL], zone_offset]
        ]
      end

      data = @backend.gh_graph_data(@oid2, version: version)
      assert_equal expected_entries, data

      assert File.exist?(File.join(@fixture.path, @caches.file_path(version: version)))
    end

    define_method("test_V#{version}_empty_commit_message") do
      @oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "", time: @time)
      @oid2 = @fixture.rugged_commit_files({ "README" => "Hello." }, "", time: @time, parents: [@oid1])

      email = version < 2 ? DEFAULT_EMAIL : [DEFAULT_EMAIL]

      expected_entries = [
        [@time.to_i, 946166400, 1, 1, email, zone_offset],
        [@time.to_i, 946166400, 1, 0, email, zone_offset]
      ]

      data = @backend.gh_graph_data(@oid2, version: version)
      assert_equal expected_entries, data
    end

    define_method("test_V#{version}_empty_commit") do
      message = "commit 1\n\nco-authored-by: Ned Flanders <nflanders@github.com>"
      @oid1 = @fixture.add_simple_empty_commit(message, time: @time)
      @oid2 = @fixture.rugged_commit_files({ "README" => "Hello." }, "commit 2", time: @time, parents: [@oid1])

      email = version < 2 ? DEFAULT_EMAIL : [DEFAULT_EMAIL]

      expected = [[@time.to_i, 946166400, 1, 0, email, zone_offset]]

      data = @backend.gh_graph_data(@oid2, version: version)
      assert_equal expected, data
    end

    define_method("test_V#{version}_cache_update") do
      message = "commit 1\n\nco-authored-by: Ned Flanders <nflanders@github.com>"
      oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, message, time: @time)

      if version < 2
        row1 = [@time.to_i, 946166400, 1, 0, DEFAULT_EMAIL, zone_offset]
        row2 = [@time.to_i, 946166400, 1, 1, DEFAULT_EMAIL, zone_offset]
      else
        row1 = [@time.to_i, 946166400, 1, 0, [DEFAULT_EMAIL, "nflanders@github.com"], zone_offset]
        row2 = [@time.to_i, 946166400, 1, 1, [DEFAULT_EMAIL, "lsimpson@github.com"], zone_offset]
      end
      data = @backend.gh_graph_data(oid1, version: version)
      assert_equal [row1], data

      message = "commit 2\n\nco-authored-by: Lisa Simpson <lsimpson@github.com>"
      oid2 = @fixture.rugged_commit_files({ "README" => "Hello reader!" }, message, time: @time, parents: [oid1])

      data = @backend.gh_graph_data(oid2, version: version)
      assert_equal [row2, row1], data
    end

    define_method("test_V#{version}_destructive_push") do
      message = "commit 1\n\nco-authored-by: Ned Flanders <nflanders@github.com>"
      oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, message, time: @time, parents: [])

      if version < 2
        row1 = [@time.to_i, 946166400, 1, 0, DEFAULT_EMAIL, zone_offset]
        row2 = [@time.to_i, 946166400, 2, 0, "hsimpson@github.com", zone_offset]
      else
        row1 = [@time.to_i, 946166400, 1, 0, [DEFAULT_EMAIL, "nflanders@github.com"], zone_offset]
        row2 = [@time.to_i, 946166400, 2, 0, %w(hsimpson@github.com lsimpson@github.com), zone_offset]
      end

      data = @backend.gh_graph_data(oid1, version: version)
      assert_equal [row1], data

      message = "commit 2\n\nco-authored-by: Lisa Simpson <lsimpson@github.com>"
      oid2 = @fixture.rugged_commit_files({ "README" => "Hello\nreader!" },
        message, time: @time, email: "hsimpson@github.com", parents: [])

      # simulate deletion of unreachable commit
      File.delete(File.join(@fixture.path, "objects", oid1[0, 2], oid1[2, 38]))

      data = @backend.gh_graph_data(oid2, version: version)
      assert_equal [row2], data
    end

    define_method("test_V#{version}_corrupted_data") do
      File.open(File.join(@fixture.path, @caches.file_path(version: version)), "w") do |f|
        f.write "# Not Zlib::Deflated graph data"
      end

      oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "commit 1", time: @time)
      data = @backend.gh_graph_data(oid1, version: version)

      email = version < 2 ? DEFAULT_EMAIL : [DEFAULT_EMAIL]

      assert_equal [[@time.to_i, 946166400, 1, 0, email, zone_offset]], data
    end
  end

  def test_builds_on_previous_version
    oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "commit 1", time: @time)
    fake_data = [12, 13, 0, 1, "homer@github.com", 3]

    # ugly fudging of data
    v1 = @caches.new_instance(version: 1)
    v1.write(oid1, [fake_data])

    data = @backend.gh_graph_data(oid1, version: 1)
    assert_equal [fake_data], data

    fake_data[4] = [fake_data[4]]

    assert_equal [fake_data], @backend.gh_graph_data(oid1, version: 2)

    oid2 = @fixture.rugged_commit_files({ "README" => "Hello reader!" }, "commit 2", time: @time, parents: [oid1])
    data = @backend.gh_graph_data(oid2, version: 2)

    assert_equal [[@time.to_i, 946166400, 1, 1, [DEFAULT_EMAIL], zone_offset], fake_data], data
  end

  def test_v2_mixed_trailers
    message = <<~'MSG'
      Commit 1

      co-authored-by: Buck Rogers <brogers@github.com>
      favourite-color: blue
      co-authored-by: Barack Obama <bobama@github.com>
    MSG
    oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, message, time: @time)

    data = @backend.gh_graph_data(oid1, version: 2)
    emails = [DEFAULT_EMAIL, "brogers@github.com", "bobama@github.com"]
    assert_equal [[@time.to_i, 946166400, 1, 0, emails, zone_offset]], data
  end

  def test_v2_bad_coauthor_signature
    message = <<~'MSG'
      Commit 1

      co-authored-by: Buck Rogers <brogers@github.com
    MSG
    oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, message, time: @time)

    data = @backend.gh_graph_data(oid1, version: 2)
    assert_equal [[@time.to_i, 946166400, 1, 0, [DEFAULT_EMAIL], zone_offset]], data
  end

  def test_gh_graph_data_by_day_and_author_with_missing_tz_info
    output = <<~LOG
    >946702800 +51800----someone@example.org
     1 file changed, 2 insertions(+), 1 deletion(-)
    LOG
    result = { "ok" => true, "out" => output }
    data = @backend.stub(:spawn_git_ro, result) do
      @backend.gh_graph_data_by_day_and_author(from_oid: nil, to_oid: @oid1)
    end

    expected_data = [
      {
        date: "2000-01-01",
        author: "someone@example.org",
        additions: 2,
        deletions: 1,
        commits: 1
      }
    ]

    assert_equal expected_data, data
  end

  def test_gh_graph_data_by_day_and_author_with_invalid_email
    output = <<~LOG
    >946702800 +51800----nobody
     1 file changed, 2 insertions(+), 1 deletion(-)
    LOG
    result = { "ok" => true, "out" => output }
    data = @backend.stub(:spawn_git_ro, result) do
      @backend.gh_graph_data_by_day_and_author(from_oid: nil, to_oid: @oid1)
    end

    expected_data = [
      {
        date: "2000-01-01",
        author: "nobody",
        additions: 2,
        deletions: 1,
        commits: 1
      }
    ]

    assert_equal expected_data, data
  end

  def test_gh_graph_data_by_day_and_author_bad_trailers
    output = <<~LOG
    >946702800 +51800----someone@example.org
    Ref:
    https: //github.com/some/repo/issues/1

     1 file changed, 3 insertions(+)
    LOG
    result = { "ok" => true, "out" => output }
    data = @backend.stub(:spawn_git_ro, result) do
      @backend.gh_graph_data_by_day_and_author(from_oid: nil, to_oid: @oid1)
    end

    expected_data = [
      {
        date: "2000-01-01",
        author: "someone@example.org",
        additions: 3,
        deletions: 0,
        commits: 1
      }
    ]

    assert_equal expected_data, data
  end
end
