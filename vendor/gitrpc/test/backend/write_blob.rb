# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

# Tests the GitRPC::Backend::Files module methods
class BackendWriteBlobTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_write_blob
    assert_equal "e5aa740ad4898eb5901ba1c6872c7f85169dd4a9", @backend.write_blob("yuhoo")
  end

  def test_write_badarg
    assert_raises(ArgumentError) do
      @backend.write_blob(10)
    end
  end
end
