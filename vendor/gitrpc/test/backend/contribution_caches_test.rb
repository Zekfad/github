# frozen_string_literal: true

require "setup"

require "gitrpc"
require "gitrpc/backend/contribution_caches"
require "repository_fixture"

# Tests the GitRPC::Backend::Commits module methods
class ContributionCachesTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)
    @rugged = Rugged::Repository.new(@fixture.path)

    @caches = GitRPC::Backend::ContributionCaches.new(@backend)
  end

  def teardown
    @fixture.teardown
  end

  def head_oid
    @head_oid ||= "a" * 40
  end

  def simple_row
    @simple_row ||= [1234, 9999, 2, 5, "me@github.com", -12000]
  end

  def author_list_row
    @row ||= [1234, 9999, 2, 5, ["me@github.com", "me2@github.com"], -12000]
  end

  def all_versions
    GitRPC::Backend::ContributionCaches::LATEST_VERSION.downto(GitRPC::Backend::ContributionCaches::FIRST_VERSION).to_a
  end

  def test_new_instance
    assert_raises(ArgumentError) do
      @caches.new_instance(version: GitRPC::Backend::ContributionCaches::LATEST_VERSION + 1)
    end

    assert_raises(ArgumentError) do
      @caches.new_instance(version: GitRPC::Backend::ContributionCaches::FIRST_VERSION - 1)
    end

    instance = @caches.new_instance(version: GitRPC::Backend::ContributionCaches::FIRST_VERSION)
    assert_equal GitRPC::Backend::ContributionCaches::V1, instance.class
  end

  def test_file_path
    assert_equal GitRPC::Backend::ContributionCaches::V1::PATH, @caches.file_path(version: 1)
    assert_equal GitRPC::Backend::ContributionCaches::V2::PATH, @caches.file_path(version: 2)
  end

  def test_find_existing_cache
    all_versions.each { |v| assert_nil @caches.find_existing_cache(version: v) }

    @backend.fs_write(GitRPC::Backend::ContributionCaches::V1::PATH, "data")

    assert_equal GitRPC::Backend::ContributionCaches::V1, @caches.find_existing_cache(version: 1).class
    assert_equal GitRPC::Backend::ContributionCaches::V1, @caches.find_existing_cache(version: 2).class

    @backend.fs_write(GitRPC::Backend::ContributionCaches::V2::PATH, "data")
    assert_equal GitRPC::Backend::ContributionCaches::V1, @caches.find_existing_cache(version: 1).class
    assert_equal GitRPC::Backend::ContributionCaches::V2, @caches.find_existing_cache(version: 2).class
  end

  def test_clear
    refute @backend.fs_exist?(GitRPC::Backend::ContributionCaches::V1::PATH)

    # make sure it works with no cache files
    @caches.clear

    @backend.fs_write(GitRPC::Backend::ContributionCaches::V1::PATH, "data")
    @backend.fs_write(GitRPC::Backend::ContributionCaches::V2::PATH, "data")
    assert @backend.fs_exist?(GitRPC::Backend::ContributionCaches::V1::PATH)
    assert @backend.fs_exist?(GitRPC::Backend::ContributionCaches::V2::PATH)

    @caches.clear
    refute @backend.fs_exist?(GitRPC::Backend::ContributionCaches::V1::PATH)
    refute @backend.fs_exist?(GitRPC::Backend::ContributionCaches::V2::PATH)
  end

  [1, 2].each do |version|
    define_method("test_V#{version}_caches_data") do
      cache = @caches.new_instance(version: version)
      expected_path = File.join(@fixture.path, @caches.file_path(version: version))
      row = version > 1 ? author_list_row : simple_row

      refute File.exist?(expected_path)

      assert_raises(GitRPC::Backend::ContributionCaches::Error) do
        cache.write("foo", [row])
      end

      assert_raises(GitRPC::Backend::ContributionCaches::Error) do
        cache.write(head_oid, [])
      end

      cache.write(head_oid, [row])
      assert File.exist?(expected_path)

      cache2 = @caches.find_existing_cache(version: version)
      assert_equal cache.class, cache2.class
      assert_equal head_oid, cache2.head_oid
      assert_equal [row], cache2.commits
    end
  end
end
