# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendMergeBaseTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.add_empty_commit("commit 1")
    @fixture.command "git tag v0.1 HEAD"
    @tag_oid = @fixture.rev_parse("v0.1")

    @commit2 = @fixture.add_empty_commit("commit 2")
    @fixture.command "git branch some-branch HEAD"
    @branch_oid = @fixture.rev_parse("some-branch")

    @fixture.command "git tag -a -m 'v0.2 release' v0.2 HEAD"
    @annotated_oid = @fixture.rev_parse("v0.2")

    @commit3 = @fixture.add_empty_commit("commit 3")

    @fixture.command "git symbolic-ref HEAD refs/heads/noparents"
    @commit4 = @fixture.add_empty_commit("commit 4")

    @fixture.command "git symbolic-ref HEAD refs/heads/master"
    @master_oid = @fixture.rev_parse("master")

    @commit5 = @fixture.commit_files({"README" => "Hello."}, "commit 5")
  end

  def test_merge_base_from_tag
    assert_equal @commit1, @backend.merge_base("v0.1", "master")
  end

  def test_merge_base_from_branch
    assert_equal @commit2, @backend.merge_base("some-branch", "master")
  end

  def test_merge_base_from_oid
    assert_equal @commit2, @backend.merge_base(@branch_oid, @master_oid)
  end

  def test_merge_base_from_annotated_tag
    assert_equal @commit2, @backend.merge_base("v0.2", @master_oid)
  end

  def test_merge_base_with_bad_object
    e = nil

    begin
      @backend.merge_base("deadbeefcafe", "master")
    rescue GitRPC::InvalidObject => e
    end

    assert !e.nil?
  end

  def test_merge_base_with_invalid_base_ref
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.native_merge_base("--output=/usr/bin/git", "master")
    end
  end

  def test_merge_base_with_invalid_head_ref
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.native_merge_base("master", "--output=/usr/bin/git")
    end
  end

  def test_merge_base_with_invalid_base_and_head_ref
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.native_merge_base("--output=/usr/bin/git", "--output=/usr/bin/git")
    end
  end

  def test_merge_base_with_blob_oid
    commit = @backend.read_commits([@commit5]).first
    tree = @backend.read_trees([commit["tree"]]).first

    e = nil

    begin
      @backend.merge_base(tree["entries"]["README"]["oid"], "master")
    rescue GitRPC::InvalidObject => e
    end

    assert !e.nil?
  end

  def test_merge_base_without_common_ancestors
    assert_nil @backend.merge_base("noparents", @master_oid)
  end

  def test_branch_base
    branch_base = @backend.branch_base(@commit5, @commit4, @commit4)
    assert_equal @commit4, branch_base
  end
end

class BackendBestMergeBaseTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def assert_best_base(head_oid, base_oid, best_base_oid)
    # The answer should be symmetric in its arguments
    assert_equal best_base_oid, @backend.best_merge_base(head_oid, base_oid)
    assert_equal best_base_oid, @backend.best_merge_base(base_oid, head_oid)
  end

  def create_commit(parents: [], message:)
    @backend.create_tree_changes(parents, {
      "message"   => message,
      "tree"      => "4b825dc642cb6eb9a060e54bf8d69288fbee4904",
      "committer" => @committer || {
        "name"  => "Some Guy",
        "email" => "guy@example.com",
        "time"  => "2000-01-01T00:00:00Z"
      }
    })
  end

  def test_best_merge_base_with_bad_object
    commit0 = create_commit(message: "commit 0", parents: [])
    commit1 = create_commit(message: "commit 1", parents: [commit0])

    assert_raises GitRPC::InvalidObject do
      @backend.merge_base("deadbeefcafe0000000000000000000000000000", commit1)
    end

    assert_raises GitRPC::InvalidObject do
      @backend.merge_base(commit1, "deadbeefcafe0000000000000000000000000000")
    end
  end

  def test_best_merge_base_with_non_commit_object
    commit0 = create_commit(message: "commit 0", parents: [])
    commit1 = create_commit(message: "commit 1", parents: [commit0])

    assert_raises(GitRPC::InvalidObject) do
      @backend.merge_base("4b825dc642cb6eb9a060e54bf8d69288fbee4904", commit1)
    end

    assert_raises(GitRPC::InvalidObject) do
      @backend.merge_base(commit1, "4b825dc642cb6eb9a060e54bf8d69288fbee4904")
    end
  end

  #     0--1--2--3--4--5--6---7----8--9    <- master
  #         \        \   /        /
  #          \        \ /        /
  #           \        ·        /
  #            \      / \      /
  #             \    /   \    /
  #              A--B--C--D--E--F          <- branch
  def test_simple_criss_cross
    10.times do |n|
      @committer = {
        "name"  => "Some Guy #{n}",
        "email" => "guy#{n}@example.com",
        "time"  => "2000-01-01T00:00:00Z"
      }

      commit0 = create_commit(message: "commit 0", parents: [])
      commit1 = create_commit(message: "commit 1", parents: [commit0])
      commit2 = create_commit(message: "commit 2", parents: [commit1])
      commit3 = create_commit(message: "commit 3", parents: [commit2])
      commit4 = create_commit(message: "commit 4", parents: [commit3])
      commit5 = create_commit(message: "commit 5", parents: [commit4])

      commitA = create_commit(message: "commit A", parents: [commit1])
      commitB = create_commit(message: "commit B", parents: [commitA])
      commitC = create_commit(message: "commit C", parents: [commitB])

      commit6 = create_commit(message: "commit 6", parents: [commit5, commitB])
      commit7 = create_commit(message: "commit 7", parents: [commit6])

      commitD = create_commit(message: "commit D", parents: [commitC, commit4])
      commitE = create_commit(message: "commit E", parents: [commitD])
      commitF = create_commit(message: "commit F", parents: [commitE])

      commit8 = create_commit(message: "commit 8", parents: [commit7, commitE])
      commit9 = create_commit(message: "commit 9", parents: [commit8])

      # Cases with a single merge base:
      assert_best_base(commitA, commit1, commit1)
      assert_best_base(commitA, commit3, commit1)
      assert_best_base(commitC, commit2, commit1)
      assert_best_base(commitA, commit5, commit1)
      assert_best_base(commitE, commit3, commit3)
      assert_best_base(commitE, commit5, commit4)
      assert_best_base(commitD, commit9, commitD)
      assert_best_base(commitF, commit9, commitE)

      ## Cases with multiple merge bases:
      assert_best_base(commitD, commit6, commit4)
      assert_best_base(commitD, commit7, commit4)
      assert_best_base(commitF, commit6, commit4)
      assert_best_base(commitF, commit7, commit4)
    end
  end
end
