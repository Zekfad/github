# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendReadSubmodulesTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @submodule_fixture = RepositoryFixture.new("submodule.git")
    @submodule_fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def setup_test_commits
    @commit = @fixture.commit_files({
      "README"       => "hello",
    }, "normal commit")

    @submodule_fixture.commit_files({
        "README" => "an submodule"
      }, "in sub")

    @submodule_commit = @fixture.add_submodule_commit(@submodule_fixture.path,
                                                      "submod",
                                                      "Add a submodule")

    @dot_submodule = @fixture.add_submodule_commit(@submodule_fixture.path,
                                                      "submod.git",
                                                      "Add a submodule")

    @slash_submodule = @fixture.add_submodule_commit(@submodule_fixture.path,
                                                      '.\\\external\\\Cello',
                                                      "Add a submodule")

    @bad_submodule_file = @fixture.commit_files({
        ".gitmodules" => "This is not a valid .gitmodules file"
      }, "bad submodule")
  end

  def test_read_submodules
    res = @backend.read_submodules(@submodule_commit)
    submod = res.first
    assert_equal "submod", submod["path"]
    assert_equal @submodule_fixture.path, submod["url"]
  end

  def test_read_invalid_submodules_type
    @wrong_type_submodule_file = @fixture.commit_files({
      ".gitmodules" => nil,
      ".gitmodules/foo" => "fake file"
    }, "invalid submodule type")

    res = @backend.read_submodules(@wrong_type_submodule_file)
  end

  def test_read_submodules_with_none
    res = @backend.read_submodules(@commit)
    assert_equal([], res)
  end

  def test_submodules_with_a_dot
    res = @backend.read_submodules(@dot_submodule)
    assert_equal 2, res.size
    submod = res.detect { |x| x["name"] == "submod.git" }

    assert submod
    assert_equal "submod.git", submod["name"]
    assert_equal "submod.git", submod["path"]
    assert_match %r{test/submodule\.git$}, submod["url"]
  end

  def test_submodules_with_slashes
    res = @backend.read_submodules(@slash_submodule)
    assert_equal 3, res.size
    submod = res.detect { |x| x["name"] == ".\\external\\Cello" }

    assert submod
    assert_equal ".\\external\\Cello", submod["name"]
    assert_equal ".\\external\\Cello", submod["path"]
    assert_match %r{test/submodule\.git$}, submod["url"]
  end

  def test_invalid_submodules_file
    res = @backend.read_submodules(@bad_submodule_file)
    assert_equal 0, res.size
  end
end
