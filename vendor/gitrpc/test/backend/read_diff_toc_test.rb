# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class BackendReadDiffTocTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({"README" => "Hello. This is the majority of the content of the file.\n"}, "commit 1")
    @commit2 = @fixture.commit_files({"README2" => "Hello. This is the majority of the content of the file.\nNew!\n", "README" => nil}, "commit 2")

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_bad_commit_oids
    assert_raises(GitRPC::ObjectMissing) { @backend.native_read_diff_toc("DEADBEEF", "DEADBEEF2") }
  end

  def test_timeout
    assert_raises(GitRPC::Timeout) { @backend.native_read_diff_toc(@commit1, @commit2, "timeout" => 0.0000000001) }
    @backend.native_read_diff_toc(@commit1, @commit2, "timeout" => 50)
  end
end
