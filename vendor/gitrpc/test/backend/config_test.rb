# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendConfigTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @fixture.config.store("github.test", "true")

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_config_get_with_existing_value
    res = @backend.config_get("github.test")
    assert_equal "true", res
  end

  def test_config_get_with_missing_value
    res = @backend.config_get("github.missing")
    assert_nil res
  end

  def test_config_store_with_existing_value
    res = @backend.config_store("github.test", "false")
    assert_nil res
    assert_equal "false", @fixture.config.get("github.test")
  end

  def test_config_store_with_missing_value
    res = @backend.config_store("github.missing", "true")
    assert_nil res
    assert_equal "true", @fixture.config.get("github.missing")
  end

  def test_config_store_with_invalid_value
    assert_raises TypeError do
      @backend.config_store("github.test", :symbol)
    end
  end

  def test_config_delete_with_existing_value
    res = @backend.config_delete("github.test")
    assert_equal true, res
    assert_nil @fixture.config.get("github.test")
  end

  def test_config_delete_with_missing_value
    res = @backend.config_delete("github.missing")
    assert_equal false, res
    assert_nil @fixture.config.get("github.missing")
  end
end
