# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ProtocolFakeRPCTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @url = "fakerpc:#{@fixture.path}"
    @proto = GitRPC::Protocol.resolve(@url)
  end

  def teardown
    @fixture.teardown
  end

  def test_resolve
    proto = GitRPC::Protocol.resolve(@url)
    assert proto.is_a?(GitRPC::Protocol::FakeRPC)
  end

  def test_url
    assert @proto.url.is_a?(URI)
    assert_equal "fakerpc", @proto.url.scheme
    assert_equal @fixture.path, @proto.url.path
  end

  def test_path
    assert_equal @fixture.path, @proto.path
  end

  def test_options
    opts = { :max => 1000 }
    proto = GitRPC::Protocol.resolve(@url, opts)
    assert_equal({ :max => 1000, :trace2 => [] }, proto.options)
    assert_same opts, proto.options
  end

  def test_send_message
    message = [123, "string", {"key" => "value"}]
    res = @proto.send_message(:echo, [123, "string", {"key" => "value"}])
    assert_equal message, res
  end

  def test_send_message_bad_object
    assert_raises(GitRPC::EncodingError) do
      @proto.send_message(:echo, Object.new)
    end
  end

  def test_send_message_symbols
    msg = { :foo => :bar }
    res = @proto.send_message(:echo, **msg)
    assert_equal msg, res
  end

  def test_send_message_resets_backend
    backend = @proto.instance_variable_get(:@backend)
    @proto.send_message(:echo, "hi")
    refute_same backend, @proto.instance_variable_get(:@backend)
  end

  def test_error
    assert_raises(GitRPC::InvalidRepository) do
      @proto.send_message(:read_refs)
    end
  end

  def test_failure
    @fixture.setup
    assert_raises(GitRPC::Failure) { @proto.send_message(:not_implemented_method) }
    failure = exception { @proto.send_message(:not_implemented_method) }

    original = failure.original
    assert original.kind_of?(NoMethodError)
  end

  def exception
    yield
  rescue => boom
    boom
  end
end
