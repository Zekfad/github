# frozen_string_literal: true
# Test server used in BERTRPC protocol tests. The bertrem library is used to
# start a child process with the server running on a port.
class BERTRPCTestServer
  attr_reader :pid

  def initialize(port, source_root: nil)
    @port = port
    @source_root = source_root
  end

  def start
    require "ernicorn"
    if @pid = fork
      wait!
    else
      start!
    end
  end

  def stop
    if @pid
      Process.kill("KILL", @pid) rescue nil
      Process.waitpid(@pid)
      @pid = nil
    end
  end

  def wait!
    start = Time.now
    loop do
      break if connect
      sleep 0.01
      fail "BERTRPC server in child didn't start" if Time.now - start > 2.0
    end
  end

  def connect
    sock = TCPSocket::new("127.0.0.1", @port)
    sock.close rescue nil
    true
  rescue Errno::ECONNREFUSED
    false
  end

  def start!
    # STDOUT.reopen "/dev/null"
    # STDERR.reopen "/dev/null"
    server = TCPServer.new "127.0.0.1", @port
    require "ernicorn"
    Ernicorn.expose(:gitrpc, GitRPC::Protocol::BERTRPCServer)
    Ernicorn.set_source_root(@source_root)
    loop do
      client = server.accept
      Ernicorn.process(client, client)
      client.close
    end
  ensure
    server.close if server
  end
end
