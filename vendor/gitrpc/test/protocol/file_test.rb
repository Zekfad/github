# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ProtocolFileTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @url = "file:#{@fixture.path}"
    @proto = GitRPC::Protocol.resolve(@url)
  end

  def teardown
    @fixture.teardown
  end

  def test_resolve
    proto = GitRPC::Protocol.resolve(@url)
    assert proto.is_a?(GitRPC::Protocol::File)
  end

  def test_url
    assert @proto.url.is_a?(URI)
    assert_equal "file", @proto.url.scheme
    assert_equal @fixture.path, @proto.url.path
  end

  def test_path
    assert_equal @fixture.path, @proto.path
  end

  def test_options
    proto = GitRPC::Protocol.resolve(@url, :max => 1000)
    assert_equal({:max => 1000, :trace2 => []}, proto.options)
  end

  def test_send_message
    message = [123, "string", {"key" => "value"}]
    res = @proto.send_message(:echo, [123, "string", {"key" => "value"}])
    assert_equal message, res
  end

  def test_error
    assert_raises(GitRPC::InvalidRepository) do
      @proto.send_message(:read_refs)
    end
  end

  def test_failure
    @fixture.setup
    assert_raises(GitRPC::Failure) { @proto.send_message(:not_implemented_method) }
    failure = exception { @proto.send_message(:not_implemented_method) }

    original = failure.original
    assert original.kind_of?(NoMethodError)
  end

  def test_cleanup
    mock_backend = Minitest::Mock.new
    if ruby_2_6?
      mock_backend.expect(:send_message, true, [:exist?, {}])
    else
      mock_backend.expect(:send_message, true, [:exist?])
    end
    mock_backend.expect(:cleanup, nil)
    @proto.stub(:backend, mock_backend) do
      @proto.send_message(:exist?)
    end
    mock_backend.verify
  end

  def exception
    yield
  rescue => boom
    boom
  end
end
