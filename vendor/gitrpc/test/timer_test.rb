# frozen_string_literal: true
require_relative "setup"

require "gitrpc"

class TimerTest < Minitest::Test
  include GitRPC::Timer

  def test_timeout_on_module
    assert_raises GitRPC::Timer::Error do
      GitRPC::Timer.timeout 0.010 do
        sleep 1.0
      end
    end
  end

  def test_timeout_on_mixin
    assert_raises GitRPC::Timer::Error do
      timeout 0.010 do
        sleep 1.0
      end
    end
  end

  def test_timeout_non_timeout
    called = false
    timeout(1.0) { called = true }
    assert called
  end
end
