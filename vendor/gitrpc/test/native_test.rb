# frozen_string_literal: true
require_relative "setup"
require "gitrpc/native"

class NativeTest < Minitest::Test
  def setup
    @path = File.expand_path("..", __FILE__)
    @env = {"SOME_VAR" => "SOME_VAL"}
    @options = {:timeout => 30}
    @native = GitRPC::Native.new(@path, @env, @options)
  end

  def test_basic_command
    res = @native.spawn(["echo", "hello", "there"])
    assert res[:ok]
    assert_equal "hello there\n", res[:out]
  end

  def test_command_result
    res = @native.spawn(["echo", "hello", "there"])
    assert res[:ok]
    assert_equal 0,  res[:status]
    assert_equal "hello there\n", res[:out]
    assert_equal "", res[:err]
    assert_equal %w[echo hello there], res[:argv]
    assert_equal @options.merge(:chdir => @path, :input => nil, :pgroup_kill => true, :noexec => true),
      res[:options]
  end

  def test_environment_variables
    res = @native.spawn(["/bin/sh", "-c", "echo $SOME_VAR"])
    assert res[:ok]
    assert_equal "SOME_VAL\n", res[:out]
  end

  def test_piping_input
    res = @native.spawn(["cat"], "some input\n")
    assert res[:ok]
    assert_equal "some input\n", res[:out]
  end

  def test_timeout
    @native = GitRPC::Native.new(@path, @env, :timeout => 0.100)
    assert_raises POSIX::Spawn::TimeoutExceeded do
      @native.spawn(["sleep", "5"])
    end
  end
end
