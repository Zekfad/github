# frozen_string_literal: true
require_relative "setup"

require "gitrpc"
require "repository_fixture"
require "fake_instrumenter"
require "gitrpc/util/hashcache"

class ClientTest < Minitest::Test
  def setup
    @instrumenter = GitRPC.instrumenter = FakeInstrumenter.new

    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @options = {}
    @protocol = GitRPC::Protocol.resolve("fakerpc:#{@fixture.path}", @options)
    @backend = @protocol.backend
    @client = GitRPC::Client.new(@protocol, @cache)
  end

  def teardown
    GitRPC.instrumenter = nil
    @fixture.teardown
  end

  def test_options
    assert_same @backend.options, @client.options
    assert_same @options, @client.options
  end

  def test_features
    refute @client.feature_enabled?(:foo)
    refute @client.feature_enabled?("foo")

    @client.enable_feature(:foo)
    assert @client.feature_enabled?(:foo)
    assert @client.feature_enabled?("foo")

    @client.enable_feature("bar")
    assert @client.feature_enabled?(:bar)
    assert @client.feature_enabled?("bar")
  end

  def test_default_cache_keys
    assert_equal "", @client.content_key
    assert_equal @backend.repository_key, @client.repository_key
  end

  def test_content_cache_key
    oid = "deadbeef00deadbeef00deadbeef00deadbeef00"
    assert_equal "v2::#{oid}", @client.content_cache_key(oid)
    assert_equal "v2::#{oid}:42", @client.content_cache_key(oid, 42)
  end

  def test_repository_cache_key
    @client.repository_key = "r300"
    assert_equal "v2:r300:something", @client.repository_cache_key("something")
    assert_equal "v2:r300:something:42", @client.repository_cache_key("something", 42)
  end

  def test_network_cache_key
    @client.network_key = "r300"
    assert_equal "v2:r300:something", @client.network_cache_key("something")
    assert_equal "v2:r300:something:42", @client.network_cache_key("something", 42)
  end

  def test_repository_reference_cache_key
    @client.repository_key = "r500"
    @client.repository_reference_key_delegate = TestRefKeyDelegate.new { "5:b621eeccd5c7edac9b7dcba35a8d5afd075e24f2" }

    assert_equal "v2:r500:5:b621eeccd5c7edac9b7dcba35a8d5afd075e24f2:something",
                 @client.repository_reference_cache_key("something")

    assert_equal "v2:r500:5:b621eeccd5c7edac9b7dcba35a8d5afd075e24f2:something:42",
                 @client.repository_reference_cache_key("something", 42)
  end

  def test_repository_reference_cache_key_updates
    @client.repository_key = "r500"
    @client.repository_reference_key_delegate = TestRefKeyDelegate.new { "5:#{SecureRandom.hex(20)}" }

    first_one = @client.repository_reference_key

    5.times do
      assert_equal first_one, @client.repository_reference_key
    end

    @client.clear_repository_reference_key!
    second_one = @client.repository_reference_key
    refute_equal first_one, second_one

    5.times do
      assert_equal second_one, @client.repository_reference_key
    end
  end

  def test_instruments_remote_calls
    @client.echo("howdy")
    assert @instrumenter.published_event?(:remote_call)

    name, payload = @instrumenter.find_event(:remote_call)
    assert_equal :echo, payload[:name]
    assert_equal ["howdy"], payload[:args]
  end

  class TestRefKeyDelegate
    def initialize(&block)
      @compute = block
    end

    def repository_reference_key
      @compute.call
    end
  end
end
