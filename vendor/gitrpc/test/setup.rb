# frozen_string_literal: true
# Basic test environment.
#
# This should set up the load path for testing only. Don't require any support libs
# or gitrpc stuff in here.

# some things use this to load default config, like chimney
ENV["RAILS_ENV"] = "test"

# Hide any .gitconfig that the user might have:
ENV["HOME"] = File.expand_path("../fixtures", __FILE__)
ENV["XDG_CONFIG_HOME"] = nil


# this is unfortunate
require "rubygems" if !defined?(Gem)
require "bundler/setup"

if ENV["COVERAGE"]
  require "simplecov"
  SimpleCov.start do
    add_filter "/test/"

    add_group "Client", "lib/gitrpc/client"
    add_group "Backend", "lib/gitrpc/backend"
  end
end

require "bert"
BERT::Encode.version = (ENV["BERT_VERSION"] || :v3).to_sym

if ENV["RUBY_NEXT"] && ENV["UPGRADE_WARNINGS_LOGDIR"]
  require_relative "../../../test/test_helpers/parallel_collector"
  require_relative "../../../test/test_helpers/warnings"

  at_exit do
    WarningsCollector.instance.process
  end
end

# bring in minitest
require "minitest/autorun"

# put lib and test dirs directly on load path
$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
$LOAD_PATH.unshift File.expand_path("..", __FILE__)
