# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class AheadBehindTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
  end

  def teardown
  end

  def test_ahead_behind_is_available
    expect = {
      "chuyeow/master" => [3, 9],
      "subwindow/master" => [0, 3],
      "semis" => [1, 12],
      "webweaver/master" => [0, 0],
    }

    # ahead-behind counts should always be available,
    # even if bitmaps have not been generated
    ab = @client.ahead_behind("master", expect.keys)
    assert_equal expect, ab
  end
end
