# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class NetworkStorageTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
  end

  def test_nw_usage
    res = @client.nw_usage
    assert res.is_a?(Numeric)
    assert res > 0
  ensure
    # nw-usage remembers its output in a file called `usage` in the
    # network directory.  Delete it if it got created.
    path = @client.backend.path + "/../usage"
    File.unlink(path) if File.exist?(path)
  end

  def test_free_space
    res = @client.free_space
    assert res.is_a?(Numeric)
    assert res > 0
  end

  def test_unpacked_size
    res = @client.get_unpacked_size
    assert res.is_a?(Numeric)
    assert res >= 0
    assert res < 10
  end

  def test_maint_size
    res = @client.get_maint_size
    assert res.is_a?(Numeric)
    assert res >= 0
    assert res < 10
  end
end
