# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class GistTest < Minitest::Test
  # Mostly legacy tests for the Gist namespaced calls
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
  end

  def test_gist_corrupt
    fixture = RepositoryFixture.new
    fixture.setup
    client = GitRPC.new("fakerpc:#{fixture.path}")

    assert client.gist_corrupt?, "Repo is corrupt with no HEAD"
    refute @client.gist_corrupt?, "Repo is not corrupt with commits"
  end

  def test_gist_head_oid
    assert_equal "4bf7a39e8c4ec54f8b4cd594a3616d69004aba69", @client.gist_head_oid
  end

  def test_gist_text_blob_oid
    assert_equal "e43b0f988953ae3a84b00331d0ccf5f7d51cb3cf",
      @client.gist_text_blob_oid("4bf7a39e8c4ec54f8b4cd594a3616d69004aba69")
  end

  def test_gist_blob_oid_by_path
    assert_equal "e43b0f988953ae3a84b00331d0ccf5f7d51cb3cf",
      @client.gist_blob_oid_by_path(".gitignore", "4bf7a39e8c4ec54f8b4cd594a3616d69004aba69")
  end

  def test_gist_blob_by_path
    result = @client.gist_blob_by_path(".gitignore", "4bf7a39e8c4ec54f8b4cd594a3616d69004aba69")
    {
      "oid"=>"e43b0f988953ae3a84b00331d0ccf5f7d51cb3cf",
      "name"=>".gitignore",
      "path"=>".gitignore",
      "content"=>".DS_Store\n",
      "size"=>10,
      "truncated"=>false,
      "binary"=>false,
      "encoding"=>"UTF-8",
    }.each do |key, value|
      assert_equal value, result[key]
    end
    assert_nil result["name_encoding"]
  end

  def test_gist_title
    assert_equal ".gitignore", @client.gist_title("4bf7a39e8c4ec54f8b4cd594a3616d69004aba69")
  end
end
