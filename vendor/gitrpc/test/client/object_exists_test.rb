# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ObjectExistsTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
  end

  def test_object_exists
    assert @client.object_exists?("4bf7a39e8c4ec54f8b4cd594a3616d69004aba69"), "Object should be found"
    assert @client.object_exists?("4bf7a39e8c4ec54f8b4cd594a3616d69004aba69", "commit"), "Commit should be found"
    assert @client.object_exists?("bbf747873075ac28667d246491ffdefbd314fe4f", "tree"), "Tree should be found"
    assert @client.object_exists?("d4fc2d5e810d9b4bc1ce67702603080e3086a4ed", "blob"), "Blob should be found"
    assert @client.object_exists?("9e0e6e66756dcd9070ccec61b5865dd5a06c5695", "tag"), "Tag should be found"

    refute @client.object_exists?("4bf7a39e8c4ec54f8b4cd594a3616d69004aba69", "blob"), "Commit is not a blob"
    refute @client.object_exists?("0"*40), "Should not find non-existant object"
    assert_raises GitRPC::InvalidFullOid do
      @client.object_exists?("NOTASHA")
    end
  end

  def test_batch_check_objects_exist
    assert_equal({
      "4bf7a39e8c4ec54f8b4cd594a3616d69004aba69" => true,
      "bbf747873075ac28667d246491ffdefbd314fe4f" => true,
      "d4fc2d5e810d9b4bc1ce67702603080e3086a4ed" => true,
      "9e0e6e66756dcd9070ccec61b5865dd5a06c5695" => true,
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" => false
    }, @client.batch_check_objects_exist([
      "4bf7a39e8c4ec54f8b4cd594a3616d69004aba69",
      "bbf747873075ac28667d246491ffdefbd314fe4f",
      "d4fc2d5e810d9b4bc1ce67702603080e3086a4ed",
      "9e0e6e66756dcd9070ccec61b5865dd5a06c5695",
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    ]))

    assert_raises GitRPC::InvalidFullOid do
      @client.batch_check_objects_exist(["NOTASHA"])
    end
  end
end
