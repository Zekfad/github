# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientReadDiffSummaryTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({
      "README" => "Hello.\nWelcome to the repo.\n",
      "ABOUT"  => "This is a repo about changes.\n"
    }, "commit 1")

    @commit2 = @fixture.commit_files({
      "README"  => "Hello.\Welcome to the repository.\nWelcome to all!\n",
      "ABOUT"   => "This is a repo about changes.\nAdditions, and deletions.\n",
      "ANOTHER" => "Just some other file.\n"
    }, "commit 2")

    @empty = @fixture.commit_changes("empty")

    @commit3 = @fixture.commit_files({
      "ADDITIONS" => "This is a new file!\nAnd it's great.\nThe\n\n\nEND.\n"
    }, "commit 3")

    @whitespace = @fixture.commit_files({
      "ANOTHER" => "Just some other file.   \nLine after whitespace lines\n"
    }, "whitespace")

    @rename = @fixture.commit_files({
      "additions2" => "This is a new file!\nAnd it's great.\nThe\n\n\nEND.\n",
      "ADDITIONS"  => nil
    }, "renamed")

    @binary = @fixture.commit_files({
      "test.exe" => (1..3000).to_a.pack("C*")
    }, "binary stuff")

    @nonascii = @fixture.commit_files({
      "Здравствуй" => "Hello"
    }, "nonascii filename")

    cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => cache)
  end

  def teardown
    @fixture.teardown
  end

  def test_read_summary
    summary = @client.read_diff_summary(@commit1, @commit2, nil)

    assert_equal 4, summary.additions
    assert_equal 2, summary.deletions
    assert_equal 3, summary.changed_files

    assert_equal 3, summary.deltas.size

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "9ed5163d6efc675622fc9799628e3ba56db17ab0",
        mode: "100644",
        path: "ABOUT"
      },
      new_file: {
        oid: "2d9ae229eb00cb204525cbada48d7c2ed0730cc0",
        mode: "100644",
        path: "ABOUT"
      }
    ), summary.deltas[0]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "ANOTHER"
      },
      new_file: {
        oid: "af9eccdc1af6488df00ff0c2d59ea72aa40fe409",
        mode: "100644",
        path: "ANOTHER"
      }
    ), summary.deltas[1]

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: 2,
      deletions: 2,
      old_file: {
        oid: "4dd092bc3d2677772177e58bb9a6440e785f92ee",
        mode: "100644",
        path: "README"
      },
      new_file: {
        oid: "a8b351bde01634a61b60fa84514a10806ea37e5d",
        mode: "100644",
        path: "README"
      }
    ), summary.deltas[2]
  end

  def test_read_summary_writes_to_cache
    refute @client.diff_summary_cached?(@commit1, @commit2, nil)

    @client.read_diff_summary(@commit1, @commit2, nil)

    assert @client.diff_summary_cached?(@commit1, @commit2, nil)
  end

  def test_cached_summary_deltas_are_frozen
    result = @client.read_diff_summary(@commit1, @commit2, nil)
    assert result.deltas.all?(&:frozen?)
    result = @client.read_diff_summary(@commit1, @commit2, nil)
    assert result.deltas.all?(&:frozen?)
  end

  def test_read_summary_empty_diff
    summary = @client.read_diff_summary(@commit2, @empty, nil)

    assert_equal 0, summary.additions
    assert_equal 0, summary.deletions
    assert_equal 0, summary.changed_files

    assert_equal 0, summary.deltas.size
  end

  def test_read_summary_only_additions
    summary = @client.read_diff_summary(@empty, @commit3, nil)

    assert_equal 6, summary.additions
    assert_equal 0, summary.deletions
    assert_equal 1, summary.changed_files

    assert_equal 1, summary.deltas.size

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 6,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "ADDITIONS"
      },
      new_file: {
        oid: "6a57714eb98f95462e30d9fddb4a9c4b317320f0",
        mode: "100644",
        path: "ADDITIONS"
      }
    ), summary.deltas[0]
  end

  def test_read_summary_with_whitespace_ignored
    summary = @client.read_diff_summary(@commit3, @whitespace, nil, algorithm: "ignore-whitespace")

    assert_equal 1, summary.additions
    assert_equal 0, summary.deletions
    assert_equal 1, summary.changed_files

    assert_equal 1, summary.deltas.size

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "M",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "af9eccdc1af6488df00ff0c2d59ea72aa40fe409",
        mode: "100644",
        path: "ANOTHER"
      },
      new_file: {
        oid: "d26d37cdd9253eda4419e80df230b243c8063211",
        mode: "100644",
        path: "ANOTHER"
      }
    ), summary.deltas[0]
  end

  def test_read_summary_with_renames
    summary = @client.read_diff_summary(@whitespace, @rename, nil)

    assert_equal 0, summary.additions
    assert_equal 0, summary.deletions
    assert_equal 1, summary.changed_files

    assert_equal 1, summary.deltas.size

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "R",
      similarity: 100,
      additions: 0,
      deletions: 0,
      old_file: {
        oid: "6a57714eb98f95462e30d9fddb4a9c4b317320f0",
        mode: "100644",
        path: "ADDITIONS"
      },
      new_file: {
        oid: "6a57714eb98f95462e30d9fddb4a9c4b317320f0",
        mode: "100644",
        path: "additions2"
      }
    ), summary.deltas[0]
  end

  def test_fake_algorithm
    assert_raises(GitRPC::InvalidDiffAlgorithm) do
      @client.read_diff_summary(@commit3, @whitespace, nil, algorithm: "foobar")
    end
  end

  def test_not_available_on_timeout
    summary = @client.read_diff_summary(@commit1, @commit3, nil, timeout: 0.0000000001)
    refute_predicate summary, :available?
  end

  def test_returns_immediately_on_0_timeout
    @client.backend.stub(:send_message, lambda { |*args| fail "should not be called" }) do
      summary = @client.read_diff_summary(@commit1, @commit3, nil, timeout: 0)
      refute_predicate summary, :available?
      assert_equal "timeout", summary.unavailable_reason
    end
  end

  def test_does_not_cache_summary_on_timeout
    refute @client.diff_summary_cached?(@commit1, @commit3, nil)

    @client.read_diff_summary(@commit1, @commit3, nil, timeout: 0.0000000001)

    refute @client.diff_summary_cached?(@commit1, @commit3, nil)
  end

  def test_does_not_send_message_if_timeout_was_cached_with_larger_timeout_value
    summary = @client.read_diff_summary(@commit1, @commit3, nil, timeout: 0.0000001)
    refute_predicate summary, :available?

    @client.backend.stub(:send_message, lambda { |*args| fail "unexpected call" }) do
      summary = @client.read_diff_summary(@commit1, @commit3, nil, timeout: 0.000000001)
      refute_predicate summary, :available?
    end
  end

  def test_does_send_message_if_timeout_was_cached_with_smaller_timeout_value
    summary = @client.read_diff_summary(@commit1, @commit3, nil, timeout: 0.0000001)
    refute_predicate summary, :available?

    summary = @client.read_diff_summary(@commit1, @commit3, nil, timeout: 5)
    assert_predicate summary, :available?
  end

  def test_does_not_send_message_if_summary_was_cached_with_larger_timeout
    summary = @client.read_diff_summary(@commit1, @commit3, nil, timeout: 5)
    assert_predicate summary, :available?

    summary = @client.read_diff_summary(@commit1, @commit3, nil, timeout: 0.0000001)
    assert_predicate summary, :available?
  end

  def test_not_available_on_command_busy
    @client.backend.stub(:send_message, lambda { |*args| raise GitRPC::CommandBusy }) do
      summary = @client.read_diff_summary(@commit1, @commit3, nil)
      refute_predicate summary, :available?
    end
  end

  def test_does_not_cache_summary_on_command_busy
    @client.backend.stub(:send_message, lambda { |*args| raise GitRPC::CommandBusy }) do
      refute @client.diff_summary_cached?(@commit1, @commit3, nil)

      @client.read_diff_summary(@commit1, @commit3, nil)

      refute @client.diff_summary_cached?(@commit1, @commit3, nil)
    end
  end

  def test_if_bad_oid
    summary = @client.read_diff_summary(@commit1, "f" * 40, nil)
    refute summary.available?
    assert_equal "missing commits", summary.unavailable_reason
  end

  def test_ref_argument
    assert_raises(GitRPC::InvalidFullOid) { @client.read_diff_summary("master~1", "master", nil) }
  end

  def test_nil_oid1
    summary = @client.read_diff_summary(nil, @commit2, nil)

    assert_equal 4, summary.additions
    assert_equal 2, summary.deletions
    assert_equal 3, summary.changed_files

    assert_equal 3, summary.deltas.size
  end

  def test_binary_addition
    summary = @client.read_diff_summary(@rename, @binary, nil)

    assert_equal 0, summary.additions
    assert_equal 0, summary.deletions
    assert_equal 1, summary.changed_files

    assert_equal 1, summary.deltas.size

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: nil,
      deletions: nil,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "test.exe"
      },
      new_file: {
        oid: "ba38b69e9a3f859f0a3114c1fa919026c1e91575",
        mode: "100644",
        path: "test.exe"
      }
    ), summary.deltas[0]
  end

  def test_nonascii_filename
    summary = @client.read_diff_summary(@binary, @nonascii, nil)

    assert_equal 1, summary.additions
    assert_equal 0, summary.deletions
    assert_equal 1, summary.changed_files

    assert_equal 1, summary.deltas.size

    assert_equal GitRPC::Diff::Summary::Delta.new(
      status: "A",
      similarity: 0,
      additions: 1,
      deletions: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "\xD0\x97\xD0\xB4\xD1\x80\xD0\xB0\xD0\xB2\xD1\x81\xD1\x82\xD0\xB2\xD1\x83\xD0\xB9".b
      },
      new_file: {
        oid: "5ab2f8a4323abafb10abb68657d9d39f1a775057",
        mode: "100644",
        path: "\xD0\x97\xD0\xB4\xD1\x80\xD0\xB0\xD0\xB2\xD1\x81\xD1\x82\xD0\xB2\xD1\x83\xD0\xB9".b
      }
    ), summary.deltas[0]
  end
end
