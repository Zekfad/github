# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ReadTreeBlobsCountTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
    @head = @client.read_head_oid
  end

  def test_read_tree_blobs_count
    assert_equal 30, @client.read_tree_blobs_count(@head)
    assert_equal 20, @client.read_tree_blobs_count(@head, 20)
  end

  def test_read_tree_blobs_count_with_many_files
    client = RepositoryFixture.sample("ten_million_files")
    head = client.read_head_oid

    assert_equal 10000744, client.read_tree_blobs_count(head)
    assert_equal 1_000_000, client.read_tree_blobs_count(head, 1_000_000)
  end

  def test_read_tree_blobs_with_bad_oids
    assert_raises(GitRPC::Failure) do
      @client.read_tree_blobs_count("0"*40)
    end

    assert_raises(GitRPC::InvalidObject) do
      # Blob oid
      @client.read_tree_blobs_count("d4fc2d5e810d9b4bc1ce67702603080e3086a4ed")
    end
  end
end
