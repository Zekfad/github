# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

require "minitest/mock"

class ClientRevParseTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("commit 1")
    @oid = @fixture.rev_parse("HEAD")
  end

  def test_rev_parse_with_nil_name
    assert_raises TypeError do
      @client.rev_parse(nil)
    end
  end

  def test_rev_parse_with_special_dots
    assert_nil @client.rev_parse("foo..bar")
  end

  def test_rev_parse_does_not_invoke_backend_when_refs_cached
    @client.read_qualified_refs(["master", "refs/heads/master", "refs/tags/master"]) # fill cache

    @client.backend.stub :send_message, lambda { fail } do
      assert_equal @oid, @client.rev_parse("master")
    end
  end
end
