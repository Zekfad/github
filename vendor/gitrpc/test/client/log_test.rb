# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class ClientLogTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
    @oid3 = @fixture.commit_files({"README" => "Hallo."}, "commit 3\n\nCo-AuThoReD-By: Arthur Schreiber <arthurschreiber@github.com>", :commit_time => @past_time)
    @oid4 = @fixture.commit_files({"README" => "Hi."}, "commit 4\n\nCo-authored-by: invalid", :commit_time => @past_time)
  end

  def test_activity_summary
    summary = @client.activity_summary("1999")
    assert_equal ["     4\tSome Author <someone@example.org>"], summary

    summary = @client.activity_summary("2525")
    assert_equal [], summary
  end

  def test_activity_summary_coauthors
    summary = @client.activity_summary_coauthors("1999")
    assert_equal [
      [["Some Author", "someone@example.org"], { authored_count: 4, committed_count: 4 }],
      [["Arthur Schreiber", "arthurschreiber@github.com"], { authored_count: 1, committed_count: 0 }]
    ], summary

    summary = @client.activity_summary_coauthors("2525")
    assert_equal [], summary
  end

  def test_contributor_shortlog
    contributors = @client.contributor_shortlog("HEAD")
    assert_equal "     4\tSome Author <someone@example.org>\n", contributors
  end

  def test_repo_graph_log_times
    expected = 4.times.map { Time.new(2000, 1, 1).strftime("%a,%e %b %Y %H:%M:%S %z") }
    actual = @client.repo_graph_log_times("HEAD")
    assert_equal expected, actual
  end

  def test_commit_stats_log
    commit_stats_log = @client.commit_stats_log("HEAD")
    expected = [
      "#{@oid4}\n 1 file changed, 1 insertion(+), 1 deletion(-)\n",
      "#{@oid3}\n 1 file changed, 1 insertion(+), 1 deletion(-)\n",
      "#{@oid2}\n 1 file changed, 1 insertion(+), 1 deletion(-)\n",
      "#{@oid1}\n 1 file changed, 1 insertion(+)\n"
    ]
    assert_equal expected, commit_stats_log
  end

  def test_paged_commits_log
    commits = @client.paged_commits_log("HEAD")
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @client.paged_commits_log(@oid1)
    assert_equal [@oid1], commits

    commits = @client.paged_commits_log("HEAD", path: "nonexistent")
    assert_equal [], commits
    commits = @client.paged_commits_log("HEAD", path: "README")
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @client.paged_commits_log("HEAD", author_emails: ["nobody@example.org"])
    assert_equal [], commits
    commits = @client.paged_commits_log("HEAD", author_emails: ["someone@example.org"])
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @client.paged_commits_log("HEAD", since_time: "2525")
    assert_equal [], commits
    commits = @client.paged_commits_log("HEAD", since_time: "1999")
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @client.paged_commits_log("HEAD", until_time: "1999")
    assert_equal [], commits
    commits = @client.paged_commits_log("HEAD", until_time: "2525")
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @client.paged_commits_log("HEAD", skip: 1)
    assert_equal [@oid3, @oid2, @oid1], commits

    commits = @client.paged_commits_log("HEAD", max_count: 1)
    assert_equal [@oid4], commits
  end

  def test_paged_commits_log_timeouts
    @client.stub(:send_message, -> (*args) { raise GitRPC::Timeout }) do
      err = assert_raises(GitRPC::Timeout) { @client.paged_commits_log(@oid1) }
      refute_predicate err, :cached?
    end

    @client.stub(:send_message, -> (*args) { flunk("should not be called") }) do
      @cache.stub(:set, -> (*args) { flunk("should not be called") }) do
        err = assert_raises(GitRPC::Timeout) { @client.paged_commits_log(@oid1) }
        assert_predicate err, :cached?
      end
    end

    assert_equal [@oid2, @oid1], @client.paged_commits_log(@oid2)
    assert_raises(GitRPC::Timeout) { @client.paged_commits_log(@oid1) }
  end

  def test_contributor_log_caching
    keys = @cache.track_sets do
      @client.contributor_log(@oid1, ignore_merge_commits: true, mailmap: true)
    end

    assert_equal ["v2::contributor_log:#{@oid1}:ignore_merge_commits:mailmap"], keys
  end

  def test_log_last_commit_oneline
    oneline = @client.log_last_commit_oneline
    assert_equal "#{@oid4[0..6]} commit 4", oneline
  end
end
