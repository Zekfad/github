# frozen_string_literal: true

require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "repository_fixtures/adjusted_positions"

class ClientReadAdjustedPositionsTest < Minitest::Test
  include RepositoryFixtures::AdjustedPositions

  def setup
    @fixture = setup_positioning_fixture
    @commit1 = @fixture.rev_parse("HEAD~")
    @commit2 = @fixture.rev_parse("HEAD")

    @client = GitRPC.new("fakerpc:#{@fixture.path}", cache: GitRPC::Util::HashCache.new)
    @backend = @client.backend.backend

    @rugged = @fixture.rugged

    @start_oid = @rugged.blob_at(@commit1, "README").oid
    @end_oid   = @rugged.blob_at(@commit2, "README2").oid

    @start_oid2 = @rugged.blob_at(@commit1, "notes").oid
    @end_oid2   = @rugged.blob_at(@commit2, "notes").oid
  end

  def teardown
    @fixture.teardown
  end

  def with_max_blob_size(max_size)
    old_value = GitRPC::Backend::ReadAdjustedPositions::PatchResolver::DIFF_OPTIONS

    GitRPC::Backend::ReadAdjustedPositions::PatchResolver.send :remove_const, :DIFF_OPTIONS
    GitRPC::Backend::ReadAdjustedPositions::PatchResolver.const_set(:DIFF_OPTIONS, old_value.merge(max_size: max_size))

    yield
  ensure
    GitRPC::Backend::ReadAdjustedPositions::PatchResolver.send :remove_const, :DIFF_OPTIONS
    GitRPC::Backend::ReadAdjustedPositions::PatchResolver.const_set(:DIFF_OPTIONS, old_value)
  end

  def test_single_successful_repositioning
    EXPECTED_README_MAPPINGS.each_pair do |from, to|
      if to.nil?
        assert_nil @client.read_adjusted_position(from, @start_oid, @end_oid)
      else
        assert_equal to, @client.read_adjusted_position(from, @start_oid, @end_oid)
      end
    end

    EXPECTED_NOTES_MAPPINGS.each_pair do |from, to|
      if to.nil?
        assert_nil @client.read_adjusted_position(from, @start_oid2, @end_oid2)
      else
        assert_equal to, @client.read_adjusted_position(from, @start_oid2, @end_oid2)
      end
    end
  end

  def test_matching_blob_oids
    assert_equal 5, @client.read_adjusted_position(5, @start_oid, @start_oid)
    assert_equal 220, @client.read_adjusted_position(220, @start_oid, @start_oid)
  end

  def test_multiple_successful_repositioning
    start_positions = EXPECTED_README_MAPPINGS.keys.map { |k| [k, @start_oid, @end_oid] }
    assert_equal EXPECTED_README_MAPPINGS.values, @client.read_adjusted_positions(start_positions)

    start_positions = EXPECTED_NOTES_MAPPINGS.keys.map { |k| [k, @start_oid2, @end_oid2] }
    assert_equal EXPECTED_NOTES_MAPPINGS.values, @client.read_adjusted_positions(start_positions)
  end

  def test_multiple_over_different_blobs
    start_positions = EXPECTED_README_MAPPINGS.keys.map { |k| [k, @start_oid, @end_oid] }
    start_positions += EXPECTED_NOTES_MAPPINGS.keys.map { |k| [k, @start_oid2, @end_oid2] }

    expected = EXPECTED_README_MAPPINGS.values + EXPECTED_NOTES_MAPPINGS.values
    assert_equal expected, @client.read_adjusted_positions(start_positions)
  end

  def test_invalid_oid
    assert_raises(GitRPC::InvalidFullOid) { @client.read_adjusted_position(5, "foo", @end_oid) }
    assert_raises(GitRPC::InvalidFullOid) { @client.read_adjusted_position(5, @start_oid, "bar") }

    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      assert_equal :bad, @client.read_adjusted_position(5, "foo", @end_oid, skip_bad: true)
      assert_equal :bad, @client.read_adjusted_position(5, @start_oid, "bar", skip_bad: true)
    end
  end

  def test_matching_oids
    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      @client.read_adjusted_position(5, @start_oid, @start_oid)
    end
  end

  def test_commit_object_missing
    err = assert_raises(GitRPC::ObjectMissing) { @client.read_adjusted_position(5, @start_oid, "1" * 40) }
    assert_equal "1" * 40, err.oid
    assert_equal :bad, @client.read_adjusted_position(5, @start_oid, "1" * 40, skip_bad: true)

    err = assert_raises(GitRPC::ObjectMissing) { @client.read_adjusted_position(5, "2" * 40, @end_oid) }
    assert_equal "2" * 40, err.oid
    assert_equal :bad, @client.read_adjusted_position(5, "2" * 40, @end_oid, skip_bad: true)
  end

  def test_null_blob
    assert_raises(GitRPC::ObjectMissing) { @client.read_adjusted_position(5, "0" * 40, @end_oid) }
    assert_equal :bad, @client.read_adjusted_position(5, "0" * 40, @end_oid, skip_bad: true)
  end

  def test_invalid_object
    assert_raises(GitRPC::InvalidObject) { @client.read_adjusted_position(5, @start_oid, @commit2) }
    assert_equal :bad, @client.read_adjusted_position(5, @start_oid, @commit2, skip_bad: true)

    assert_raises(GitRPC::InvalidObject) { @client.read_adjusted_position(5, @commit1, @end_oid) }
    assert_equal :bad, @client.read_adjusted_position(5, @commit1, @end_oid, skip_bad: true)
  end

  def test_invalid_position
    err = assert_raises(GitRPC::Client::ReadAdjustedPositions::InvalidPosition) do
      @client.read_adjusted_position(-4, @start_oid, @end_oid)
    end
    assert_equal "invalid position '-4'", err.message

    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      assert_equal :bad, @client.read_adjusted_position(-4, @start_oid, @end_oid, skip_bad: true)
    end

    err = assert_raises(GitRPC::Client::ReadAdjustedPositions::InvalidPosition) do
      @client.read_adjusted_position(3.3, @start_oid, @end_oid)
    end
    assert_equal "invalid position '3.3'", err.message

    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      assert_equal :bad, @client.read_adjusted_position(3.3, @start_oid, @end_oid, skip_bad: true)
    end

    err = assert_raises(GitRPC::Client::ReadAdjustedPositions::InvalidPosition) do
      @client.read_adjusted_position("hello", @start_oid, @end_oid)
    end
    assert_equal "invalid position 'hello'", err.message

    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      assert_equal :bad, @client.read_adjusted_position("hello", @start_oid, @end_oid, skip_bad: true)
    end
  end

  def test_max_blob_size
    # FIXME: this value is passed in to Rugged but appears to have no effect. What am I missing?
    with_max_blob_size(10) do
      @client.read_adjusted_position(0, @start_oid, @end_oid)
    end
  end

  def test_condensed_data
    expected_data = [[EXPECTED_README_MAPPINGS.keys, @start_oid, @end_oid]]
    called = false

    expected_params = -> (*args) do
      called = true
      expected_args = [:read_adjusted_positions, expected_data, { "skip_bad" => false }]
      assert_equal expected_args, args
      {}
    end

    begin
      @client.stub(:send_message, expected_params) do
        start_positions = EXPECTED_README_MAPPINGS.keys.map { |k| [k, @start_oid, @end_oid] }
        assert_equal EXPECTED_README_MAPPINGS.values, @client.read_adjusted_positions(start_positions)
      end
    rescue GitRPC::Client::ReadAdjustedPositions::MissingPositionError
      # just ignore; this condition gets tested elsewhere
    end

    assert called, "send_message never called!"
  end

  def test_missing_position_error
    backend_data = { [@start_oid, @end_oid] => { 6 => 8 } }

    from_backend = ->(*args) { backend_data }

    test_subject = -> do
      @client.stub(:send_message, from_backend) do
        @client.read_adjusted_positions([[6, @start_oid, @end_oid]])
      end
    end

    # sanity check/sucess scenario
    test_subject.call

    # missing blob pair
    backend_data = { ["1" * 40, "2" * 40] => { 6 => 8 } }
    err = assert_raises(GitRPC::Client::ReadAdjustedPositions::MissingPositionError, &test_subject)

    expected = {
      position: 6,
      source: "c78fe751684c87aeb397e121359a2a7bd1b32c2c",
      destination: "9dd061c3b6d8faee03c1543ad22afa05ae52735d"
    }
    assert_equal expected, err.data

    # missing position mapping
    backend_data = { [@start_oid, @end_oid] => { 2 => 300 } }
    err = assert_raises(GitRPC::Client::ReadAdjustedPositions::MissingPositionError, &test_subject)
    assert_equal expected, err.data
  end

  ######################################

  def test_successful_read_adjusted_positions_with_commits
    assert_equal 4, @client.read_commit_adjusted_position(3, @commit1, @commit2, "README", "README2")
    assert_equal 4, @client.read_commit_adjusted_position(3, @commit1, @commit2, "README", "README2")

    start_positions = EXPECTED_README_MAPPINGS.keys.map { |k| [k, @commit1, @commit2, "README", "README2"] }
    start_positions += EXPECTED_NOTES_MAPPINGS.keys.map { |k| [k, @commit1, @commit2, "notes", "notes"] }

    expected = EXPECTED_README_MAPPINGS.values + EXPECTED_NOTES_MAPPINGS.values
    assert_equal expected, @client.read_commit_adjusted_positions(start_positions)
  end

  def test_duplicate_positioning_data
    data = [[3, @commit1, @commit2, "README", "README2"]] * 3
    assert_equal [4, 4, 4], @client.read_commit_adjusted_positions(data, skip_bad: true)
  end

  def test_commit_invalid_object
    assert_raises(GitRPC::InvalidObject) { @client.read_commit_adjusted_position(3, @start_oid, @end_oid, "README", "README2") }
    assert_equal :bad, @client.read_commit_adjusted_position(3, @start_oid, @end_oid, "README", "README2", skip_bad: true)
  end

  def test_commit_invalid_oid
    assert_raises(GitRPC::InvalidFullOid) { @client.read_commit_adjusted_position(3, nil, @end_oid, "README", "README2") }

    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      assert_equal :bad, @client.read_commit_adjusted_position(3, nil, @end_oid, "README", "README2", skip_bad: true)
    end
  end

  def test_commit_invalid_position
    assert_raises(GitRPC::Client::ReadAdjustedPositions::InvalidPosition) do
      @client.read_commit_adjusted_position("bad pos", @commit2, @commit2, "README", "README2")
    end

    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      assert_equal :bad, @client.read_commit_adjusted_position(3, nil, @end_oid, "README", "README2", skip_bad: true)
    end
  end

  def test_commit_invalid_path
    assert_raises(GitRPC::NoSuchPath) { @client.read_commit_adjusted_position(3, @commit1, @commit2, "missing-file", "still-gone") }
    assert_equal :bad, @client.read_commit_adjusted_position(3, @commit1, @commit2, "missing-file", "still-gone", skip_bad: true)

    assert_raises(GitRPC::NoSuchPath) { @client.read_commit_adjusted_position(3, @commit1, @commit2, "README", "missing-file") }
    assert_equal :bad, @client.read_commit_adjusted_position(3, @commit1, @commit2, "README", "missing-file", skip_bad: true)

    assert_raises(GitRPC::NoSuchPath) do
      @client.read_commit_adjusted_position(3, @commit2, @commit2, "README", nil)
    end

    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      assert_equal :bad, @client.read_commit_adjusted_position(3, @commit2, @commit2, "README", nil, skip_bad: true)
    end
  end

  def test_matching_commit_oids
    @client.stub(:send_message, -> (*args) { flunk "should not call send_message" }) do
      @client.read_commit_adjusted_position(5, @commit1, @commit1, "README", "README")
    end
  end
end

  ######################################

class ClientReadBasePositionTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    content = (1..200).to_a.join("\n")
    @c1 = @fixture.commit_files({ "subdir/notes.txt" => content }, "C1")

    @fixture.create_branch("feature")

    content = ((1..120).to_a + (125..200).to_a).join("\n")
    @c2 = @fixture.commit_files({ "subdir/notes.txt" => content }, "C2", branch: "feature")

    content = ((1..100).to_a + ["Foo"] + (101..200).to_a).join("\n")
    @c3 = @fixture.commit_files({ "subdir/notes.txt" => content }, "C3")

    @c4 = @fixture.merge("master", into: "feature")

    content = ((1..100).to_a + ["Foo"] + (101..120).to_a + (125..175).to_a + (178..200).to_a).join("\n")
    @c5 = @fixture.commit_files({ "subdir/notes.txt" => content }, "C5", branch: "feature")

    @client = GitRPC.new("fakerpc:#{@fixture.path}", cache: GitRPC::Util::HashCache.new)
  end

  def test_read_base_position_with_proxy_tree_change
    data = {
      position: 175,
      source: {
        start_commit_oid: @c2,
        end_commit_oid:   @c5,
        base_commit_oid:  @c3,
        path:             "subdir/notes.txt"
      },
      destination: {
        commit_oid: @c2,
        path:       "subdir/notes.txt"
      }
    }
    pos = @client.read_commit_adjusted_position_with_base(**data)
    assert_equal 174, pos
  end

  def test_read_base_position_without_proxy_tree
    data = {
      position: 10,
      source: {
        start_commit_oid: @c1,
        end_commit_oid:   @c2,
        base_commit_oid:  @c1,
        path:             "subdir/notes.txt"
      },
      destination: {
        commit_oid: @c1,
        path:       "subdir/notes.txt"
      }
    }
    pos = @client.read_commit_adjusted_position_with_base(**data)
    assert_equal 10, pos
  end

  def test_read_base_position_with_nil_base
    data = {
      position: 10,
      source: {
        start_commit_oid: @c1,
        end_commit_oid:   @c2,
        base_commit_oid:  nil,
        path:             "subdir/notes.txt"
      },
      destination: {
        commit_oid: @c1,
        path:       "subdir/notes.txt"
      }
    }
    pos = @client.read_commit_adjusted_position_with_base(**data)
    assert_equal 10, pos
  end
end
