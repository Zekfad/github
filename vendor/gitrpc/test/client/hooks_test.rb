# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class ClientHooksTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_post_mirror_hook
    wrong_updated_refs = ["nope"]
    updated_refs = ["foobar"]

    @fixture.command("printf '#!/bin/sh\ngrep -q foobar\n' > hooks/post-mirror && chmod 755 hooks/post-mirror")
    assert_raises GitRPC::CommandFailed do
      @client.post_mirror_hook(wrong_updated_refs)
    end
    assert_nil @client.post_mirror_hook(updated_refs)
  end

  def test_pre_receive_hook
    @fixture.command("printf '#!/bin/sh\ngrep foobar\n' > hooks/pre-receive && chmod 755 hooks/pre-receive")

    res = @client.pre_receive_hook("nope", {})
    refute res["ok"]
    assert_equal "", res["out"]
    assert_equal "", res["err"]
    res = @client.pre_receive_hook("foobar", {})
    assert res["ok"]
    assert_equal "foobar\n", res["out"]
    assert_equal "", res["err"]

    env = { "GIT_SOCKSTAT_VAR_repo_name"=> "test_repo" }
    res = @client.pre_receive_hook("foobar", env)

    assert res["ok"]
  end
end
