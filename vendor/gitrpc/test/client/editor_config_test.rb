# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientEditorConfigTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("editorconfig-plugin-tests")
  end

  def test_fetch_editor_config
    expected = {
      "README.rst" => {
        "indent_style" => "space",
        "indent_size" => "4",
        "end_of_line" => "lf",
        "insert_final_newline" => "true",
        "trim_trailing_whitespace" => "true",
        "charset" => "utf-8",
        "tab_width" => "4"
      }
    }
    actual = @client.fetch_editor_config("8f7d1478a461c6aa25836d739d5f66559f1aff15", ["README.rst"])
    assert_equal expected, actual

    expected = {
      "test_files/3_space.txt" => {
        "indent_style" => "space",
        "indent_size" => "3",
        "tab_width" => "3"
      },
      "test_files/4_space.py" => {
        "indent_style" => "space",
        "indent_size" => "4",
        "tab_width" => "8"
      }
    }
    actual = @client.fetch_editor_config("8f7d1478a461c6aa25836d739d5f66559f1aff15", ["test_files/3_space.txt", "test_files/4_space.py"])
    assert_equal expected, actual
  end

  def test_no_config
    expected = {
      "README.rst" => {}
    }
    actual = @client.fetch_editor_config("a64513e5241a079f6ae0ee7b297cafbb8c1c4708", ["README.rst"])
    assert_equal expected, actual
  end

  def test_requires_valid_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.fetch_editor_config("1234567", ["README.rst"])
    end
  end

  def test_require_tree_oid
    assert_raises GitRPC::InvalidObject do
      @client.fetch_editor_config("de98819d33edd9dd9d07537a021feed6e298ce83", ["README.rst"])
    end
  end
end
