# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class DescribeTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
  end

  def test_describe
    assert_equal "v1.2-8-g4bf7a39e8c4ec54f8b4cd594a3616d69004aba69", @client.describe("HEAD")
    assert_equal "v1.2-8-g4bf7a39", @client.describe("HEAD", 7)
    assert_equal "v1.2-0-gfce84b4", @client.describe("v1.2", 7)
  end

  def test_describe_handles_tags_with_leading_dashes
    assert_equal "-v2.0-19-gfc18e48", @client.describe("v1.2^", 7)
  end

  def test_describe_handles_blobs
    # d4fc2d5e810d9b4bc1ce67702603080e3086a4ed is a blob in `defunkt_facebox`
    sha="d4fc2d5e810d9b4bc1ce67702603080e3086a4ed"
    assert_equal "-v2.0-4-g2525b57:README.txt", @client.describe(sha, 7)
    assert_equal "-v2.0-4-g2525b57620f58af55465ef049a189d021924c232:README.txt",
      @client.describe(sha, 40)
  end
end
