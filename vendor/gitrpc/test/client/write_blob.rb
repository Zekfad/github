# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class WriteBlobTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def test_write_blob
    assert_equal "fe53efd8bfe3ffd395ef65f04898c48ce506f757", @client.write_blob("spawn is bad")
  end

  def test_write_blob_badarg
    assert_raises(GitRPC::Failure) do
      @client.write_blob(42)
    end
  end
end
