# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientReadBlobOidsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({
      "file1.txt"              => "file1 content",
      "file2.txt"              => "file2 content",
      "subdirectory/other.txt" => "other content"
      }, "commit 1")
    @commit2 = @fixture.commit_files({
      "file2.txt" => "new file2 content",
      "file3.txt" => "file3 content"
      }, "commit 2")

    cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => cache)
  end

  def test_resolves_expected_blob_oids
    expected = [
      "2e80f50e920610d8e6048341a06bb8cdb2f01df7",
      "79cd8cc836eece298bbcac9fe8a22174a5ffcdfc",
      "2e80f50e920610d8e6048341a06bb8cdb2f01df7",
      "6aa787d1d6ff6f4672e28a35cb6bff00fae6ef7a",
      "e7d29a106b49c0d850a711d3841f9105402390aa"
    ]
    blob_oids = @client.read_blob_oids([
      [@commit1, "file1.txt"],
      [@commit1, "file2.txt"],
      [@commit2, "file1.txt"],
      [@commit2, "file2.txt"],
      [@commit2, "file3.txt"]
    ])

    assert_equal expected, blob_oids
  end

  def test_object_missing
    missing_oid = "5" * 40
    err = assert_raises(GitRPC::ObjectMissing) { @client.read_blob_oid(missing_oid, "file1.txt") }
    assert_equal missing_oid, err.oid

    assert_nil @client.read_blob_oid(missing_oid, "file1.txt", skip_bad: true)
  end

  def test_invalid_oid
    assert_raises(GitRPC::InvalidFullOid) { @client.read_blob_oid("foo", "file1.txt") }
    assert_raises(GitRPC::InvalidFullOid) { @client.read_blob_oid(nil, nil) }
  end

  def test_invalid_object_type
    blob_oid = "2e80f50e920610d8e6048341a06bb8cdb2f01df7"
    assert_raises(GitRPC::InvalidObject) do
      @client.read_blob_oid(blob_oid, "file1.txt")
    end
    assert_nil @client.read_blob_oid(blob_oid, "file1.txt", skip_bad: true)
  end

  def test_not_a_blob
    err = assert_raises(GitRPC::InvalidObject) do
      @client.read_blob_oid(@commit1, "subdirectory")
    end
    assert_equal "path 'subdirectory' of #{@commit1} is 'tree' instead of expected blob", err.message

    assert_nil @client.read_blob_oid(@commit1, "subdirectory", skip_bad: true)
  end

  def test_missing_path
    assert_raises(GitRPC::NoSuchPath) { @client.read_blob_oid(@commit1, "fileX.txt") }
    assert_nil @client.read_blob_oid(@commit1, "fileX.txt", skip_bad: true)

    assert_raises(GitRPC::NoSuchPath) { @client.read_blob_oid(@commit1, nil) }
    assert_nil @client.read_blob_oid(@commit1, nil, skip_bad: true)
  end

end
