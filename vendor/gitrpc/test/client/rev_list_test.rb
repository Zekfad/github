# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

require "minitest/mock"

class ClientRevListTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
    @rugged = Rugged::Repository.new(@fixture.path)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @oid2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @oid3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")
    @oid4 = @fixture.commit_files({"AUTHORS" => "Brian"}, "commit 4")

    @tag_oid = @fixture.create_tag_object("v1.0", "We made it", @oid4, "commit")

    @head_oid3 = @client.create_tree_changes(@oid2, {
      "message"   => "head commit 3",
      "committer" => @committer
    }, {"AUTHORS" => "JP"})
    @head_oid4 = @client.create_tree_changes(@head_oid3, {
      "message"   => "head commit 4",
      "committer" => @committer
    }, {"AUTHORS" => "Josh"})
    @head_oid5 = @client.create_tree_changes([@oid4, @head_oid4], {
      "message"   => "head commit 5",
      "committer" => @committer
    }, {"AUTHORS" => "Brian + Josh"})
  end

  def test_list_commit_parent_revisions
    res = @client.rev_list(@oid1)
    assert_equal [@oid1], res

    res = @client.rev_list(@oid2)
    assert_equal [@oid2, @oid1], res

    res = @client.rev_list(@oid3)
    assert_equal [@oid3, @oid2, @oid1], res
  end

  def test_deferences_tag_object
    res = @client.rev_list(@tag_oid)
    assert_equal [@oid4, @oid3, @oid2, @oid1], res
  end

  def test_list_commits_excluding_branch
    res = @client.rev_list(@head_oid5)
    assert_equal [@head_oid5, @oid4, @oid3, @oid2, @oid1, @head_oid4, @head_oid3], res

    res = @client.rev_list(@head_oid5, exclude_oids: @oid4)
    assert_equal [@head_oid5, @head_oid4, @head_oid3], res

    res = @client.rev_list(@head_oid5, exclude_oids: @head_oid4)
    assert_equal [@head_oid5, @oid4, @oid3], res
  end

  def test_list_symmetric_difference
    res = @client.rev_list([@oid4, @head_oid4], symmetric: true)
    assert_equal [@oid4, @oid3, @head_oid4, @head_oid3], res

    res = @client.rev_list([@oid4, @head_oid4], symmetric: true, exclude_oids: [@oid3])
    assert_equal [@oid4, @head_oid4, @head_oid3], res

    res = @client.rev_list([@oid4, @head_oid4], symmetric: true, exclude_oids: [@oid3, @head_oid3])
    assert_equal [@oid4, @head_oid4], res
  end

  def test_list_symmetric_difference_wrong_args
    error = assert_raises ArgumentError do
      @client.rev_list(@oid4, symmetric: true)
    end
    assert_equal "need exactly 2 commits to create a symmetric difference", error.message

    error = assert_raises ArgumentError do
      @client.rev_list([@oid4, @head_oid4, @oid3], symmetric: true)
    end
    assert_equal "need exactly 2 commits to create a symmetric difference", error.message
  end

  def test_list_commits_including_multiple_commits
    res = @client.rev_list(@oid4)
    assert_equal [@oid4, @oid3, @oid2, @oid1], res

    res = @client.rev_list(@head_oid4)
    assert_equal [@head_oid4, @head_oid3, @oid2, @oid1], res

    res = @client.rev_list([@oid4, @head_oid4])
    assert_equal [@oid4, @oid3, @oid2, @oid1, @head_oid4, @head_oid3], res
  end

  def test_list_commit_parent_revisions_in_reverse
    res = @client.rev_list(@oid1, reverse: true)
    assert_equal [@oid1], res

    res = @client.rev_list(@oid2, reverse: true)
    assert_equal [@oid1, @oid2], res

    res = @client.rev_list(@oid3, reverse: true)
    assert_equal [@oid1, @oid2, @oid3], res
  end

  def test_list_commit_merges
    res = @client.rev_list(@oid4, merges: true)
    assert_equal [], res

    res = @client.rev_list(@head_oid5, merges: true)
    assert_equal [@head_oid5], res
  end

  def test_list_commits_for_paths
    res = @client.rev_list(@head_oid5, paths: ["README"])
    assert_equal [@oid3, @oid2, @oid1], res

    res = @client.rev_list(@head_oid5, paths: ["AUTHORS"])
    assert_equal [@head_oid5, @oid4, @head_oid4, @head_oid3], res

    res = @client.rev_list(@oid4, paths: ["AUTHORS"])
    assert_equal [@oid4], res
  end

  def test_list_commits_with_skip
    res = @client.rev_list(@oid4, skip: 0)
    assert_equal [@oid4, @oid3, @oid2, @oid1], res

    res = @client.rev_list(@oid4, skip: 1)
    assert_equal [@oid3, @oid2, @oid1], res

    res = @client.rev_list(@oid4, skip: 10)
    assert_equal [], res
  end

  def test_raises_obj_missing_on_invalid_obj
    assert_raises GitRPC::ObjectMissing do
      @client.rev_list(SecureRandom.hex(20))
    end

    assert_raises GitRPC::ObjectMissing do
      @client.rev_list(SecureRandom.hex(20))
    end

    assert_raises GitRPC::ObjectMissing do
      @client.rev_list(GitRPC::NULL_OID)
    end
  end

  def test_list_commit_parent_revisions_with_limit
    res = @client.rev_list(@oid3, limit: 1)
    assert_equal [@oid3], res

    res = @client.rev_list(@oid3, limit: 2)
    assert_equal [@oid3, @oid2], res
  end

  def test_requires_included_oid
    assert_raises ArgumentError do
      @client.rev_list(nil)
    end
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.rev_list([nil])
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.rev_list(["lol"])
    end
  end

  def test_raise_non_commit_object
    assert tree_oid = Rugged::Commit.lookup(@rugged, @oid1).tree.oid
    assert_raises GitRPC::InvalidObject do
      @client.rev_list(tree_oid)
    end
  end
end
