# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class BranchContainsTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
    @head_oid = @client.rev_parse("HEAD")

    @writeable_repo = RepositoryFixture.new
    @writeable_repo.setup
    @writeable_repo.add_empty_commit "initial commit"
    @writeable_repo_client = GitRPC.new("fakerpc:#{@writeable_repo.path}", :cache => GitRPC::Util::HashCache.new)
  end

  def test_branch_contains_first_commit
    assert_equal ["chuyeow/master", "licensed", "master", "semis", "subwindow/master", "webweaver/master"],
      @client.branch_contains("75822fc6473f2375d73a7ea5df7bcef6b7de03ca")
  end

  def test_branch_contains_head_oid
    assert_equal ["licensed", "master", "webweaver/master"],
      @client.branch_contains(@head_oid)
  end

  def test_branch_contains_not_oid
    ["HEAD", @head_oid[0, 30]].each do |bad_value|
      assert_raises(GitRPC::InvalidFullOid) do
        @client.branch_contains(bad_value)
      end
    end
  end

  def test_branch_contains_non_existent
    assert_raises(GitRPC::ObjectMissing) do
      @client.branch_contains("deadbeef10"*4)
    end
  end

  def test_branch_contains_wrong_object_type
    assert_raises(GitRPC::InvalidObject) do
      @client.branch_contains("d4fc2d5e810d9b4bc1ce67702603080e3086a4ed") # blob OID
    end
  end

  def test_tag_contains_first_commit
    assert_equal ["-v2.0", "v1.2"],
      @client.tag_contains("75822fc6473f2375d73a7ea5df7bcef6b7de03ca")
  end

  def test_tag_contains_head_oid
    assert_equal [], @client.tag_contains(@head_oid)
  end

  def test_tag_contains_not_oid
    ["HEAD", @head_oid[0, 30]].each do |bad_value|
      assert_raises(GitRPC::InvalidFullOid) do
        @client.tag_contains(bad_value)
      end
    end
  end

  def test_tag_contains_non_existent
    assert_raises(GitRPC::ObjectMissing) do
      @client.tag_contains("deadbeef10"*4)
    end
  end

  def test_tag_contains_wrong_object_type
    assert_raises(GitRPC::InvalidObject) do
      @client.tag_contains("d4fc2d5e810d9b4bc1ce67702603080e3086a4ed") # blob OID
    end
  end

  def test_commit_is_visible_when_on_at_least_one_branch
    branch = "temp_test_branch"
    @writeable_repo.create_branch(branch)
    files = {"temp_test_file" => "content"}
    oid = @writeable_repo.commit_files(files, "test commit", :branch => branch)

    assert @writeable_repo_client.commit_visible?(oid)

    @writeable_repo.command "git branch -D #{branch}"
    @writeable_repo_client.clear_repository_reference_key!  # clear the cache

    refute @writeable_repo_client.commit_visible?(oid)
  end

  def test_commit_is_visible_when_on_at_least_one_tag
    branch = "temp_test_branch"
    @writeable_repo.create_branch(branch)
    files = {"temp_test_file" => "content"}
    oid = @writeable_repo.commit_files(files, "test commit", :branch => branch)
    @writeable_repo.command "git branch -D #{branch}"
    # all of this branch stuff is because I don't see how to (clearly) create a
    # commit separately and then put it on a tag

    tag = "temp_test_tag"
    @writeable_repo.command "git tag #{tag} #{oid}"
    assert @writeable_repo_client.commit_visible?(oid)

    @writeable_repo_client.clear_repository_reference_key!  # clear the cache

    @writeable_repo.command "git tag -d #{tag}"
    refute @writeable_repo_client.commit_visible?(oid)
  end

  def test_commit_not_visible_when_on_other_ref
    branch = "temp_test_branch"
    @writeable_repo.create_branch(branch)
    files = {"temp_test_file" => "content"}
    oid = @writeable_repo.commit_files(files, "test commit", :branch => branch)
    @writeable_repo.command "git branch -D #{branch}"
    # all of this branch stuff is because I don't see how to (clearly) create a
    # commit separately and then put it on a non-standard ref

    @writeable_repo.command "git update-ref refs/whatever #{oid}"
    refute @writeable_repo_client.commit_visible?(oid)
  end

  def test_bulk_commits_visible
    branch = "temp_test_branch"
    @writeable_repo.create_branch(branch)
    files = {"temp_test_file1" => "content"}
    oid1  = @writeable_repo.commit_files(files, "test commit", :branch => branch)
    files = {"temp_test_file2" => "content"}
    oid2  = @writeable_repo.commit_files(files, "test commit", :branch => branch)

    expected = { oid1 => true, oid2 => true }
    assert_equal expected, @writeable_repo_client.commits_visible([oid1, oid2])

    @writeable_repo.command "git update-ref refs/heads/#{branch} #{oid1}"
    @writeable_repo_client.clear_repository_reference_key!  # clear the cache

    expected = { oid1 => true, oid2 => false }
    assert_equal expected, @writeable_repo_client.commits_visible([oid1, oid2])

    @writeable_repo.command "git branch -D #{branch}"
    @writeable_repo_client.clear_repository_reference_key!  # clear the cache

    expected = { oid1 => false, oid2 => false }
    assert_equal expected, @writeable_repo_client.commits_visible([oid1, oid2])
  end
end
