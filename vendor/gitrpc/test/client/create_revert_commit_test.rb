# rubocop:disable Style/FrozenStringLiteralComment
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientCreateRevertCommitTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @client = GitRPC.new("fakerpc:#{@fixture.path}")
    @rugged = Rugged::Repository.new(@fixture.path)

    @author = {
      "name"  => "Some Human",
      "email" => "human@example.com",
      "time"  => Time.iso8601("2000-01-01T00:00:00Z")
    }

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid  = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
  end

  def test_creates_revert_commit
    message = "revert it\n"

    rev_oid, err = @client.create_revert_commit(@oid, @oid, @author, @author, message)
    assert_nil err

    rev_commit = @client.read_commits([rev_oid]).first

    assert_equal [@oid], rev_commit["parents"]
    assert_equal message, rev_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [@author["time"].to_i, 0]
    ]

    assert_equal expected_author, rev_commit["author"]
    assert_equal expected_author, rev_commit["committer"]
  end

  def test_creates_revert_commit_with_signature
    message = "revert it\n"
    sig = "fake-signature"

    rev_oid, err = @client.create_revert_commit(@oid, @oid, @author, @author, message) do |raw|
      assert raw.is_a?(String)
      "fake-signature"
    end
    assert_nil err

    rev_commit = @client.read_commits([rev_oid]).first

    assert rev_commit["has_signature"]
    assert_equal sig, @rugged.lookup(rev_oid).header_field("gpgsig")
    assert_equal [@oid], rev_commit["parents"]
    assert_equal message, rev_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [@author["time"].to_i, 0]
    ]

    assert_equal expected_author, rev_commit["author"]
    assert_equal expected_author, rev_commit["committer"]
  end

  def test_create_revert_commit_with_nil_signature
    message = "revert it\n"

    rev_oid, err = @client.create_revert_commit(@oid, @oid, @author, @author, message) { |r| nil }
    assert_nil err

    rev_commit = @client.read_commits([rev_oid]).first

    refute rev_commit["has_signature"]
    refute @rugged.lookup(rev_oid).header_field?("gpgsig")
    assert_equal [@oid], rev_commit["parents"]
    assert_equal message, rev_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [@author["time"].to_i, 0]
    ]

    assert_equal expected_author, rev_commit["author"]
    assert_equal expected_author, rev_commit["committer"]
  end

  def test_creates_revert_commits_for_range
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1")
    commit2_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nSecond"
    }, "2")
    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3")

    revert_commit_id, err = @client.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, timeout: 10)

    revert_commit1 = Rugged::Commit.lookup(@rugged, revert_commit_id)
    commit1 = Rugged::Commit.lookup(@rugged, commit1_id)

    assert_equal commit1.tree_id, revert_commit1.tree_id
    assert_equal <<-EOS, revert_commit1.message
Revert "2"

This reverts commit #{commit2_id}.
EOS

    revert_commit2 = revert_commit1.parents[0]
    assert_equal <<-EOS, revert_commit2.message
Revert "3"

This reverts commit #{commit3_id}.
EOS

    assert_equal [commit3_id], revert_commit2.parent_ids
  end

  def test_creates_revert_commits_for_range_timeout_failure
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1")
    commit2_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nSecond"
    }, "2")
    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3")

    revert_commit_id, err = @client.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, timeout: 0.0001)
    assert_nil revert_commit_id
    assert_equal "timeout", err

    revert_commit_id, err = @client.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, timeout: -10.0)
    assert_nil revert_commit_id
    assert_equal "timeout", err

    revert_commit_id, err = @client.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, timeout: 0.0)
    assert_nil revert_commit_id
    assert_equal "timeout", err
  end

  def test_creates_revert_commits_for_range_fails_on_merge_commit
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1", branch: "master")

    @fixture.create_branch("topic")

    commit2_id = @fixture.commit_files({
      "other.md" => "Goodbye.\n\nSecond"
    }, "2", branch: "topic")

    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3", branch: "master")

    merge_commit_id = @fixture.merge("topic")

    message, err = @client.create_revert_commits_for_range(commit3_id, merge_commit_id, merge_commit_id, @author, @author, timeout: 10)

    assert_equal "error", err
    assert_equal "merge commit encountered in specified commit range", message
  end

  def test_handles_single_voting_replica_rebase_timeouts_properly
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1")
    commit2_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nSecond"
    }, "2")
    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3")

    voting_route = GitRPC::Protocol::DGit::Route.new("localhost", "repo.git", voting: true)
    nonvoting_route = GitRPC::Protocol::DGit::Route.new("localhost", "repo.git", voting: false)

    send_message_stub = lambda do |*args|
      raise GitRPC::Protocol::DGit::ResponseError.new("something failed", errors: [
        [voting_route, GitRPC::Backend::RevertTimeout.new],
        [nonvoting_route, StandardError.new]
      ])
    end

    @client.stub(:send_message, send_message_stub) do
      revert_commit_id, err = @client.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, timeout: 0.0001)

      assert_nil revert_commit_id
      assert_equal "timeout", err
    end
  end

  def test_handles_multi_voting_replica_rebase_timeouts_properly
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1")
    commit2_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nSecond"
    }, "2")
    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3")

    voting_route = GitRPC::Protocol::DGit::Route.new("localhost", "repo.git", voting: true)
    nonvoting_route = GitRPC::Protocol::DGit::Route.new("localhost", "repo.git", voting: false)

    send_message_stub = lambda do |*args|
      raise GitRPC::Protocol::DGit::ResponseError.new("something failed", errors: [
        [voting_route, GitRPC::Backend::RevertTimeout.new],
        [nonvoting_route, StandardError.new]
      ])
    end

    @client.stub(:send_message, send_message_stub) do
      revert_commit_id, err = @client.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, timeout: 0.0001)

      assert_nil revert_commit_id
      assert_equal "timeout", err
    end
  end

  def test_handles_multi_voting_replica_timeouts_and_non_timeouts_properly
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1")
    commit2_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nSecond"
    }, "2")
    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3")

    voting_route = GitRPC::Protocol::DGit::Route.new("localhost", "repo.git", voting: true)
    other_voting_route = GitRPC::Protocol::DGit::Route.new("localhost", "repo.git", voting: true)

    send_message_stub = lambda do |*args|
      raise GitRPC::Protocol::DGit::ResponseError.new("something failed", errors: [
        [voting_route, GitRPC::Backend::RevertTimeout.new],
        [other_voting_route, StandardError.new]
      ])
    end

    @client.stub(:send_message, send_message_stub) do
      assert_raises GitRPC::Protocol::DGit::ResponseError do
        @client.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, timeout: 0.0001)
      end
    end
  end
end
