# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

class FastCommitCountTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_files

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
  end

  def setup_test_files
    10.times do |i|
      @fixture.add_empty_commit("commit #{i}")
    end

    @HEAD = @fixture.rev_parse("HEAD")
  end

  def teardown
    @fixture.teardown
  end

  def test_fast_commit_count
    assert_equal 10, @client.fast_commit_count(@HEAD)
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.fast_commit_count(nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.fast_commit_count("lol")
    end
  end

  def test_limit_not_required
    assert_equal 10, @client.fast_commit_count(@HEAD, nil)
  end
end
