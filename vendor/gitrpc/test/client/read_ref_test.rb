# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientReadRefTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @client = GitRPC.new("fakerpc:#{@fixture.path}")

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @commit3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")
  end

  def test_read_symbolic_ref
    ref = @client.read_symbolic_ref("HEAD")
    assert_equal ref, "refs/heads/master"

    # non-well-formed reference name
    assert_raises(GitRPC::InvalidReferenceName) do
      @client.read_symbolic_ref("blarg")
    end

    # well-formed but nonexistent reference names
    assert_raises(GitRPC::InvalidReferenceName) do
      @client.read_symbolic_ref("HEBD")
    end
    assert_raises(GitRPC::InvalidReferenceName) do
      @client.read_symbolic_ref("refs/remotes/foo/HEAD")
    end
  end
end
