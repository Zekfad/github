# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class RevertRangeTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
    @commits = 10.times.map do |i|
      @fixture.commit_files({i.to_s => "# Content"}, "Commit #{i}")
    end
    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }
  end

  def teardown
    @fixture.teardown
  end

  def test_revert_range_single_commit
    oid = @client.revert_range(@commits.last, nil, "committer" => @committer, "message" => "Revert a commit")
    assert @client.sha_valid?(oid)
    assert_equal 9.times.map(&:to_s), @client.read_tree_entries(oid)["entries"].map { |x| x["name"] }
  end

  def test_revert_range
    oid = @client.revert_range(@commits[5], @commits.last, "committer" => @committer, "message" => "Revert a commit")
    assert @client.sha_valid?(oid)
    assert_equal 6.times.map(&:to_s), @client.read_tree_entries(oid)["entries"].map { |x| x["name"] }
  end

  def test_revert_range_invalid_time
    assert_raises(GitRPC::Failure) do
      @client.revert_range(@commits[5], @commits.last, "committer" => @committer.merge("time" => false), "message" => "Revert a commit")
    end
  end

  def test_revert_range_no_committer
    assert_raises(GitRPC::Failure) do
      @client.revert_range(@commits[5], @commits.last, "committer" => nil, "message" => "Revert a commit")
    end
  end

  def test_revert_range_wrong_order
    assert_raises(GitRPC::Error) do
      @client.revert_range(@commits.last, @commits[5], "committer" => @committer, "message" => "Revert a commit")
    end
  end

  def test_revert_range_with_parent_oid
    parent_oid = @commits[-3]
    oid = @client.revert_range(@commits.first, nil, "parent_oid" => parent_oid, "committer" => @committer, "message" => "Revert a commit")
    assert @client.sha_valid?(oid)
    assert_equal parent_oid, @client.read_commits([oid])[0]["parents"][0]
  end

  def test_revert_range_with_invalid_parent_oid
    assert_raises(GitRPC::Error) do
      parent_oid = @commits.first
      oid = @client.revert_range(@commits.last, nil, "parent_oid" => parent_oid, "committer" => @committer, "message" => "Revert a commit")
    end
  end

  def test_revert_range_with_invalid_oid
    assert_raises(GitRPC::InvalidObject) do
      oid = @client.revert_range("-BAD", @commits.last, "committer" => @committer, "message" => "Revert a commit")
    end
  end

  # Test for security issue: https://github.com/github/github/issues/150551
  def test_revert_range_with_invalid_but_extant_second_ref
    @client.update_ref("refs/heads/--output=/tmp/foo", @commits.last)
    refs = @client.read_refs
    assert_equal @commits.last, refs["refs/heads/--output=/tmp/foo"]

    assert_raises(GitRPC::InvalidObject) do
      oid = @client.revert_range(@commits[8], "--output=/tmp/foo", "committer" => @committer, "message" => "Revert a commit")
    end
  end
end
