# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"

class ClientFetchesRunningTest < Minitest::Test
  def setup
    @client = GitRPC.new("fakerpc:/tmp")
  end

  def test_fetches_running
    count = @client.fetches_running
    assert_kind_of Integer, count
  end
end
