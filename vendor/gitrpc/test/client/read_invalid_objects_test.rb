# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendReadInvalidObjectsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @client = GitRPC.new("fakerpc:#{@fixture.path}")

    rugged = Rugged::Repository.new(@fixture.path)
    @bad_tag = rugged.write("this is not a valid tag", :tag)
  end

  def test_bad_tag_raises_good_error
    assert_raises GitRPC::InvalidObject do
      @client.read_objects([@bad_tag])
    end
  end
end
