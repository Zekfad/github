# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class ClientPeelComitOrTreeTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def test_peel_from_nil
    assert_raises TypeError do
      @client.peel_to_commit_or_tree(nil)
    end
  end

  def test_peel_from_commit
    commit_oid = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "commit1", time: Time.now)
    assert_equal commit_oid, @client.peel_to_commit_or_tree(commit_oid)
  end
end
