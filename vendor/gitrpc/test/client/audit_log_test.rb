# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientAuditLogTest < Minitest::Test
  SOCKSTAT_AUDIT_LOG = File.expand_path("../../examples/sockstat_audit_log", __FILE__)

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @audit_log = File.join(@fixture.path, "audit_log")
    FileUtils.cp(SOCKSTAT_AUDIT_LOG, @audit_log)

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
  end

  def teardown
    @fixture.teardown
  end

  def test_read_audit_log_by_offsets
    lines = @client.read_audit_log_by_offsets(offsets: [0])
    expected = {
      "byteoffset" => 0,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "0000000000000000000000000000000000000000",
      "new_oid" => "d05a91386fa2ce466f9045089dab53896c7dd341",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login"=>"someone"
      }
    }
    assert_equal expected, lines[0]
  end

  def test_sha1sum_audit_log
    sha1sum = @client.sha1sum_audit_log
    assert_match /\A[0-9a-f]{7}\z/, sha1sum
  end

  def test_last_audit_log_time
    time = @client.last_audit_log_time
    assert_kind_of Time, time
  end

  def test_return_empty_for_nonexistent_audit_log
    FileUtils.rm(@audit_log)
    refute File.exist?(@audit_log)

    lines, eof = @client.read_audit_log
    assert_equal [], lines
    assert eof
  end

  def test_read_entire_audit_log_from_top
    lines, eof = @client.read_audit_log(limit: 100)
    assert_equal 10, lines.size
    assert eof

    assert_equal({
      "byteoffset" => 0,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "0000000000000000000000000000000000000000",
      "new_oid" => "d05a91386fa2ce466f9045089dab53896c7dd341",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login" => "someone"
      }
    }, lines[0])

    assert_equal({
      "byteoffset" => 1602,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "977c954a5d52a25bf658693817a8282daabe21f3",
      "new_oid" => "b43e94f468b4fe9a0b817d25eeb1b20c62b7a44e",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login" => "someone"
      }
    }, lines[-1])
  end

  def test_read_truncated_audit_log_from_top
    lines, eof = @client.read_audit_log(limit: 3)
    assert_equal 3, lines.size
    refute eof

    assert_equal [
      {
        "byteoffset" => 0,
        "bytesize" => 178,
        "ref" => "refs/heads/master",
        "old_oid" => "0000000000000000000000000000000000000000",
        "new_oid" => "d05a91386fa2ce466f9045089dab53896c7dd341",
        "committer_name" => "Some Author",
        "committer_email" => "someone@example.org",
        "committer_date_timestamp" => 1342581713,
        "committer_date_offset" => -25200,
        "sockstat" => {
          "user_login" => "someone"
        }
      },
      {
        "byteoffset" => 178,
        "bytesize" => 178,
        "ref" => "refs/heads/master",
        "old_oid" => "d05a91386fa2ce466f9045089dab53896c7dd341",
        "new_oid" => "ef64b4f1cb09900d4f79c017897273cbdcb7643e",
        "committer_name" => "Some Author",
        "committer_email" => "someone@example.org",
        "committer_date_timestamp" => 1342581713,
        "committer_date_offset" => -25200,
        "sockstat" => {
          "user_login" => "someone"
        }
      },
      {
        "byteoffset" => 356,
        "bytesize" => 178,
        "ref" => "refs/heads/master",
        "old_oid" => "ef64b4f1cb09900d4f79c017897273cbdcb7643e",
        "new_oid" => "a7d6711daf5a43b682d0a21cb7212109c74bcbdc",
        "committer_name" => "Some Author",
        "committer_email" => "someone@example.org",
        "committer_date_timestamp" => 1342581713,
        "committer_date_offset" => -25200,
        "sockstat" => {
          "user_login" => "someone"
        }
      }
    ], lines
  end

  def test_read_audit_log_from_byte_offset
    lines, eof = @client.read_audit_log(offset: 178, limit: 3)
    assert_equal 3, lines.size
    refute eof

    assert_equal [
      {
        "byteoffset" => 178,
        "bytesize" => 178,
        "ref" => "refs/heads/master",
        "old_oid" => "d05a91386fa2ce466f9045089dab53896c7dd341",
        "new_oid" => "ef64b4f1cb09900d4f79c017897273cbdcb7643e",
        "committer_name" => "Some Author",
        "committer_email" => "someone@example.org",
        "committer_date_timestamp" => 1342581713,
        "committer_date_offset" => -25200,
        "sockstat" => {
          "user_login" => "someone"
        }
      },
      {
        "byteoffset" => 356,
        "bytesize" => 178,
        "ref" => "refs/heads/master",
        "old_oid" => "ef64b4f1cb09900d4f79c017897273cbdcb7643e",
        "new_oid" => "a7d6711daf5a43b682d0a21cb7212109c74bcbdc",
        "committer_name" => "Some Author",
        "committer_email" => "someone@example.org",
        "committer_date_timestamp" => 1342581713,
        "committer_date_offset" => -25200,
        "sockstat" => {
          "user_login" => "someone"
        }
      },
      {
        "byteoffset" => 534,
        "bytesize" => 178,
        "ref" => "refs/heads/master",
        "old_oid" => "a7d6711daf5a43b682d0a21cb7212109c74bcbdc",
        "new_oid" => "92533ebc0c16e412a355004934d22354d733118e",
        "committer_name" => "Some Author",
        "committer_email" => "someone@example.org",
        "committer_date_timestamp" => 1342581713,
        "committer_date_offset" => -25200,
        "sockstat" => {
          "user_login" => "someone"
        }
      }
    ], lines
  end

  def test_read_audit_log_from_byte_offset_with_corrupt_line
    lines, eof = @client.read_audit_log(offset: 200, limit: 2)
    assert_equal 2, lines.size
    refute eof

    assert_equal({
      "byteoffset" => 200,
      "bytesize" => 156,
      "corrupt" => true,
      "line" => "91386fa2ce466f9045089dab53896c7dd341 ef64b4f1cb09900d4f79c017897273cbdcb7643e Some Author <someone@example.org> 1342581713 -0700\t{ \"user_login\":\"someone\" }\n"
    }, lines[0])
    assert_equal({
      "byteoffset" => 356,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "ef64b4f1cb09900d4f79c017897273cbdcb7643e",
      "new_oid" => "a7d6711daf5a43b682d0a21cb7212109c74bcbdc",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login" => "someone"
      }
    }, lines[1])
  end

  def test_enumerate_empty_audit_log
    FileUtils.rm(@audit_log)
    refute File.exist?(@audit_log)

    enum = @client.enum_for_audit_log(batch_size: 2)

    assert_raises(StopIteration) do
      enum.next
    end
  end

  def test_enumerate_entire_audit_log
    enum = @client.enum_for_audit_log(batch_size: 2)

    assert line = enum.next
    assert_equal({
      "byteoffset" => 0,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "0000000000000000000000000000000000000000",
      "new_oid" => "d05a91386fa2ce466f9045089dab53896c7dd341",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login" => "someone"
      }
    }, line)

    assert line = enum.next
    assert_equal({
      "byteoffset" => 178,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "d05a91386fa2ce466f9045089dab53896c7dd341",
      "new_oid" => "ef64b4f1cb09900d4f79c017897273cbdcb7643e",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login" => "someone"
      }
    }, line)

    assert line = enum.next
    assert_equal({
      "byteoffset" => 356,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "ef64b4f1cb09900d4f79c017897273cbdcb7643e",
      "new_oid" => "a7d6711daf5a43b682d0a21cb7212109c74bcbdc",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login" => "someone"
      }
    }, line)

    enum.next
    enum.next

    assert line = enum.next
    assert_equal({
      "byteoffset" => 890,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "01e625cf0a7787b48a69be0e61a7c6d603c884c6",
      "new_oid" => "38784b61f155e43a3a8b1397c93670067f63aa21",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login" => "someone"
      }
    }, line)
  end

  def test_enumerate_audit_log_from_byte_offset
    enum = @client.enum_for_audit_log(batch_size: 2, offset: 1602)

    assert line = enum.next
    assert_equal({
      "byteoffset" => 1602,
      "bytesize" => 178,
      "ref" => "refs/heads/master",
      "old_oid" => "977c954a5d52a25bf658693817a8282daabe21f3",
      "new_oid" => "b43e94f468b4fe9a0b817d25eeb1b20c62b7a44e",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {
        "user_login" => "someone"
      }
    }, line)

    assert_raises(StopIteration) do
      enum.next
    end
  end
end
