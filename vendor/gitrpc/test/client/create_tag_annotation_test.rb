# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class CreateTagAnnotationTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
    @commit = @fixture.commit_files({"README.md" => "# Example Readme\n"}, "commit 1")
    @tagger = {
      :name => "Andy Delcambre",
      :email => "andy@example.com",
      :time => Time.now
    }
  end

  def teardown
    @fixture.teardown
  end

  def test_create_tag_annotation
    oid = @client.create_tag_annotation("v1.0", @commit, :message => "This is a tag", :tagger => @tagger)
    assert oid

    tag = @fixture.rugged.lookup(oid)
    assert_equal "v1.0", tag.name
    assert_equal "This is a tag", tag.message
    assert_equal @commit, tag.target.oid
    assert_equal "Andy Delcambre", tag.tagger[:name]
    assert_equal "andy@example.com", tag.tagger[:email]
  end

  def test_create_tag_annotation_with_bad_target_oid
    assert_raises(GitRPC::Failure) {
      @client.create_tag_annotation("v1.0", "0"*40, :message => "This is a tag", :tagger => @tagger)
    }
  end
end
