# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class SortedTagRefsTest < Minitest::Test
  def test_sorted_tag_refs_returns_tags_in_order
    client = RepositoryFixture.sample("defunkt_facebox")
    assert_equal [["refs/tags/-v2.0", "75822fc6473f2375d73a7ea5df7bcef6b7de03ca", "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                  ["refs/tags/v1.2", "9e0e6e66756dcd9070ccec61b5865dd5a06c5695", "fce84b4fccc622fa682b8550785cd596a0539c33"]],
                 client.sorted_tag_refs
  end

  def test_sorted_tag_refs_returns_nothing_with_no_tags
    fixture = RepositoryFixture.new
    fixture.setup
    client = GitRPC.new("fakerpc:#{fixture.path}")

    assert_equal [], client.sorted_tag_refs
  end

  def test_sorted_tag_refs_with_bad_tags
    client = RepositoryFixture.sample("invalid_objects_test")
    assert_equal [["refs/tags/v1_0_0", "7c6454770093d4cadd139af7c5c0223ed541b00b", "7c6454770093d4cadd139af7c5c0223ed541b00b"],
                  ["refs/tags/v1_0_1", "b6942e9663c0cec63022ac6fd2d4f97a5b7978f3", "c054ccaefbf2da31c3b19178f9e3ef20a3867924"],
                  ["refs/tags/v0_0_9", "422e4ca6172379373ef6d31c11fdda27fbe97894", "9d2d0a47bceca611f9e959601223365c7c2c9860"]],
                 client.sorted_tag_refs
  end

  def test_sorted_tag_refs_semver_tags_of_same_date
    client = RepositoryFixture.sample("semver_tags")
    assert_equal [["refs/tags/v1.0.0", "071557d42d6f4acb432a4b920050888275492703", "a8e4ab4495c8a939a4b2cb1561d1f8145f4e9c31"],
                  ["refs/tags/v1.0.1", "a0f205d70353a6e9e355bb49c2e8b337b282e93e", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.2", "8eb3f2871b94ddef562a7be1bf495ca020dcba12", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.3", "11e2ea8b3256db1cfecb025ae8f6ab0fa2e972a4", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.4", "405847bb195962575fb870470509722c32b9cfd4", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.5", "254facfffb82c762d365b68bc2b6d1a8edbe9665", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.6", "c2b542ebe1486d1dfc75b3738c2149a06035f68e", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.7", "4198afb2586348dac50a79c21b6ff35e14487239", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.8", "1198ecb02b3bfe3573e13ef636b73afd2ff8c635", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.9", "77cbf32606f47cb1fe6330e5d8649a1499e9e3ff", "4710f022e3a43611afe3ac7f345c717f61cb8aba"],
                  ["refs/tags/v1.0.10", "4d8e2a6938a4f6f4d2f473c1aa6f10ef566490b2", "4710f022e3a43611afe3ac7f345c717f61cb8aba"]],
                 client.sorted_tag_refs
  end
end
