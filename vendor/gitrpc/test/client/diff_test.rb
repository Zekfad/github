# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class ClientDiffTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_file_changed?
    assert_equal @client.file_changed?("README", @oid1, @oid2), true
    assert_equal @client.file_changed?("nonexistent", @oid1, @oid2), false
  end

  def test_raw_diff
    raw_diff = @client.raw_diff(@oid1, @oid2)
    assert_equal [":100644 100644 51cd7e8ece63f4cfbee8911acbc674e235d07aae 63af512c383ca207450e4340019cf55737bb40a0 M\tREADME"], raw_diff

    raw_diff = @client.raw_diff("dead", "beef")
    assert_equal [], raw_diff
  end

  def test_diff_shortstat
    shortstat = @client.diff_shortstat("#{@oid1}..#{@oid2}")
    assert_equal "1 file changed, 1 insertion(+), 1 deletion(-)", shortstat
  end

  def test_diff_tree
    diff = @client.diff_tree(@oid1, @oid2)
    assert_equal [[":100644 100644 51cd7e8ece63f4cfbee8911acbc674e235d07aae 63af512c383ca207450e4340019cf55737bb40a0 M", "README"]], diff
  end
end
