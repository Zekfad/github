# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class ClientPushLogTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_push_log
    entries = @client.push_log
    assert_equal 2, entries.size
    entries.each do |entry|
      ref, before, after, rest = entry.split(" ", 4)
      assert_equal "refs/heads/master", ref
    end

    entries = @client.push_log(ref: "refs/heads/master")
    assert_equal 2, entries.size
    entries = @client.push_log(ref: "refs/heads/foo")
    assert_equal 0, entries.size

    entries = @client.push_log(limit: 1)
    assert_equal 1, entries.size
    entries = @client.push_log(before_oid: @oid1)
    assert_equal 1, entries.size
    entries = @client.push_log(before_oid: @oid2)
    assert_equal 0, entries.size
    entries = @client.push_log(after_oid: @oid1)
    assert_equal 1, entries.size
    entries = @client.push_log(after_oid: @oid2)
    assert_equal 1, entries.size
    entries = @client.push_log(newer_than: "900000000")
    assert_equal 2, entries.size
    entries = @client.push_log(newer_than: "17514144000")
    assert_equal 0, entries.size
    entries = @client.push_log(older_than: "900000000")
    assert_equal 0, entries.size
    entries = @client.push_log(older_than: "17514144000")
    assert_equal 2, entries.size
  end
end
