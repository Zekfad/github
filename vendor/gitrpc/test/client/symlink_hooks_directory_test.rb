# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class SymlinkHooksDirectoryTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    @template = RepositoryFixture.sample("defunkt_facebox")
  end

  def teardown
    @fixture.teardown
  end

  def test_fixture_doesnt_have_symlinked_hooks
    assert File.directory?(File.join(@fixture.path, "hooks"))
    refute File.symlink?(File.join(@fixture.path, "hooks"))
  end

  def test_symlink_hooks_dir_blows_up_with_no_hooks_configured
    assert_raises GitRPC::Failure do
      @client.symlink_hooks_directory
    end
  end

  def test_symlink_hooks_dir_blows_up_with_bad_hooks_directory
    GitRPC.hooks_template.tap do |orig_value|
      begin
        GitRPC.hooks_template = "/foo/bar/bang"
        assert_raises GitRPC::Failure do
          @client.symlink_hooks_directory
        end
      ensure
        GitRPC.hooks_template = orig_value
      end
    end
  end

  def test_symlink_hooks_dir
    GitRPC.hooks_template.tap do |orig_value|
      begin
        GitRPC.hooks_template = File.expand_path("../../repository_fixture_template/hooks", __FILE__)
        @client.symlink_hooks_directory
        assert File.symlink?(File.join(@fixture.path, "hooks"))
        assert_equal GitRPC.hooks_template, File.readlink(File.join(@fixture.path, "hooks"))
      ensure
        GitRPC.hooks_template = orig_value
      end
    end
  end

end
