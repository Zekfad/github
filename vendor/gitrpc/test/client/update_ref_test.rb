# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientUpdateRefTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @client = GitRPC.new("fakerpc:#{@fixture.path}")

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @commit3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")
  end

  def test_create_new_ref
    refute @client.read_refs["refs/heads/foo"]

    @client.update_ref("refs/heads/foo", @commit1)

    assert_equal @commit1, @client.read_refs["refs/heads/foo"]
  end

  def test_update_ref_batch
    stuff  = "create refs/heads/foo #{@commit1}\n"
    stuff += "create refs/heads/bar #{@commit2}\n"
    stuff += "update refs/heads/bas #{@commit3}\n"
    ret = @client.update_ref_batch(stuff, reason: "batch ftw")
    assert_nil ret
    refs = @client.read_refs
    assert_equal @commit1, refs["refs/heads/foo"]
    assert_equal @commit2, refs["refs/heads/bar"]
    assert_equal @commit3, refs["refs/heads/bas"]
    out = @fixture.command("git reflog refs/heads/foo")
    assert out =~ /batch ftw/

    stuff  = "update refs/heads/foo #{@commit2} #{@commit1}\n"
    stuff += "update refs/heads/bar #{@commit1} #{@commit3}\n" # oldvalue doesn't match
    stuff += "create refs/heads/bas #{@commit1}\n"
    assert_raises GitRPC::RefUpdateError do
      ret = @client.update_ref_batch(stuff)
    end
    refs = @client.read_refs
    assert_equal @commit1, refs["refs/heads/foo"]
    assert_equal @commit2, refs["refs/heads/bar"]
    assert_equal @commit3, refs["refs/heads/bas"]

    stuff  = "update refs/heads/foo #{@commit2}\n"
    ret = @client.update_ref_batch(stuff, reason: "different name", committer_name: "Somebody", committer_email: "somebody@example.com", tz: "UTC")
    assert_nil ret
    refs = @client.read_refs
    assert_equal @commit2, refs["refs/heads/foo"]
    out = @fixture.command("grep 'different name' logs/refs/heads/foo")
    assert out =~ /Somebody <somebody@example.com>/
    assert out =~ /\+0000/

    stuff  = "update refs/heads/foo #{@commit3}\n"
    ret = @client.update_ref_batch(stuff, reason: "different timezone", committer_name: "Ice Station Zebra", committer_email: "icestationzebra@example.com", tz: "Antarctica/McMurdo")
    assert_nil ret
    refs = @client.read_refs
    assert_equal @commit3, refs["refs/heads/foo"]
    out = @fixture.command("grep 'different timezone' logs/refs/heads/foo")
    assert out =~ /Ice Station Zebra <icestationzebra@example.com>/
    assert out !~ /\+0000/
  end

  def test_create_new_ref_with_old_value
    refute @client.read_refs["refs/heads/foo"]

    @client.update_ref("refs/heads/foo", @commit1, old_value: "0"*40)

    assert_equal @commit1, @client.read_refs["refs/heads/foo"]
  end

  def test_create_new_ref_with_bad_old_value
    refute @client.read_refs["refs/heads/foo"]

    assert_raises GitRPC::RefUpdateError do
      @client.update_ref("refs/heads/foo", @commit1, old_value: @commit2)
    end

    refute @client.read_refs["refs/heads/foo"]
  end

  def test_create_new_ref_with_date
    refute @client.read_refs["refs/heads/foo"]

    @client.update_ref("refs/heads/foo", @commit1, committer_date: "@1000000000 +0000")

    assert_equal @commit1, @client.read_refs["refs/heads/foo"]
    out = @fixture.command("cat logs/refs/heads/foo")
    assert out =~ /1000000000 \+0000/
  end

  def test_update_existing_ref
    @client.update_ref("refs/heads/foo", @commit1)
    assert_equal @commit1, @client.read_refs["refs/heads/foo"]

    @client.update_ref("refs/heads/foo", @commit2)

    assert_equal @commit2, @client.read_refs["refs/heads/foo"]
  end

  def test_update_existing_ref_with_old_value
    @client.update_ref("refs/heads/foo", @commit1)
    assert_equal @commit1, @client.read_refs["refs/heads/foo"]

    @client.update_ref("refs/heads/foo", @commit2, old_value: @commit1)

    assert_equal @commit2, @client.read_refs["refs/heads/foo"]
  end

  def test_update_existing_ref_with_bad_old_value
    @client.update_ref("refs/heads/foo", @commit1)
    assert_equal @commit1, @client.read_refs["refs/heads/foo"]

    assert_raises GitRPC::RefUpdateError do
      @client.update_ref("refs/heads/foo", @commit2, old_value: @commit3)
    end

    assert_equal @commit1, @client.read_refs["refs/heads/foo"]
  end

  def test_delete_existing_ref
    @client.update_ref("refs/heads/foo", @commit1)
    assert_equal @commit1, @client.read_refs["refs/heads/foo"]

    @client.delete_ref("refs/heads/foo")

    refute @client.read_refs["refs/heads/foo"]
  end

  def test_delete_existing_ref_with_old_value
    @client.update_ref("refs/heads/foo", @commit1)
    assert_equal @commit1, @client.read_refs["refs/heads/foo"]

    @client.delete_ref("refs/heads/foo", old_value: @commit1)

    refute @client.read_refs["refs/heads/foo"]
  end

  def test_delete_existing_ref_with_bad_old_value
    @client.update_ref("refs/heads/foo", @commit1)
    assert_equal @commit1, @client.read_refs["refs/heads/foo"]

    assert_raises GitRPC::RefUpdateError do
      @client.delete_ref("refs/heads/foo", old_value: @commit2)
    end

    assert_equal @commit1, @client.read_refs["refs/heads/foo"]
  end

  def test_update_ref_creates_audit_log_entry
    @client.update_ref("refs/heads/foo", @commit1)
    assert_equal @commit1, @client.read_refs["refs/heads/foo"]
    assert old_logs = @client.enum_for_audit_log.to_a.size

    @client.update_ref("refs/heads/foo", @commit2)

    assert_equal @commit2, @client.read_refs["refs/heads/foo"]
    assert_equal old_logs+1, @client.enum_for_audit_log.to_a.size
  end

  def test_create_new_ref_with_invalid_new_value
    assert_raises(GitRPC::InvalidFullOid) do
      @client.update_ref("refs/heads/foo", "refs/heads/master")
    end
  end

  def test_create_new_ref_with_invalid_old_value
    assert_raises(GitRPC::InvalidFullOid) do
      @client.update_ref("refs/heads/foo", @commit1, old_value: "refs/heads/master")
    end
  end

  def test_delete_ref_with_invalid_old_value
    @client.update_ref("refs/heads/foo", @commit1)
    assert_equal @commit1, @client.read_refs["refs/heads/foo"]

    assert_raises(GitRPC::InvalidFullOid) do
      @client.delete_ref("refs/heads/foo", old_value: "refs/heads/master")
    end
  end

  def test_update_symbolic_ref_invalid_values
    ["HEAD2", "head", "config"].each do |input|
      assert_raises(GitRPC::InvalidReferenceName) do
        @client.update_symbolic_ref("HEAD", input)
      end
    end

    ["HEAD2", "head", "config"].each do |input|
      assert_raises(GitRPC::InvalidReferenceName) do
        @client.update_symbolic_ref(input, "refs/heads/master")
      end
    end
  end

  def test_update_symbolic_ref_with_max_ref_length
    maxlen = 255 # try to sanity-check GitHub.maximum_ref_length
    refname = "refs/heads/" + ("abcd" * 61)
    assert_equal maxlen, refname.length

    refute @client.read_refs[refname]
    @client.update_ref(refname, @commit1)
    assert_equal @commit1, @client.read_refs[refname]

    @client.update_symbolic_ref("HEAD", refname)
  end

  def test_update_ref_fails_on_dgit_repo
    @fixture.setup
    res = @client.update_ref("refs/heads/master", "0"*40)
    assert_nil res, "update-ref should be ok on non-DGit repos"

    @client.config_store("core.dgit", true)
    e = assert_raises GitRPC::RefUpdateError do
      res = @client.update_ref("refs/heads/master", "0"*40)
    end
    assert_match /not allowed in read-only mode/, e.to_s

    res = @client.update_ref("refs/heads/master", "0"*40, dgit_disabled: true)
    assert_nil res, "update-ref should work on a DGit repo with DGit disabled"
  end
end
