# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class WikiTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("wiki")
    @head = "3f834fdfff4ac86f1caacc2f8abb3b7d3fad001d"
  end

  def test_read_wiki_pages
    results = @client.read_wiki_pages(@head, "")
    assert_equal 2, results["pages"].size

    assert_equal "foo/_footer.md", results["footers"]["foo"]["path"]
    assert_equal "_footer.md", results["footers"][""]["path"]

    assert_equal "foo/_sidebar.md", results["sidebars"]["foo"]["path"]
    assert_equal "_sidebar.md", results["sidebars"][""]["path"]
  end

  def test_read_wiki_pages_doesnt_return_non_wiki_pages
    assert @client.read_tree_entry(@head, "not_a_wiki_page")
    wiki_paths = @client.read_wiki_pages(@head, "")["pages"].map { |x| x["path"] }
    refute wiki_paths.include?("not_a_wiki_page")
  end

  def test_read_latest_wiki_pages
    results = @client.read_latest_wiki_pages(@head)
    assert_equal [
      ["_footer.md", "3f834fdfff4ac86f1caacc2f8abb3b7d3fad001d"],
      ["foo/_sidebar.md", "fbb027d93739ffd93c709394ae760499bc7fb6fe"],
      ["foo/_footer.md", "d11accfac1674a7a461c910e25e7223f1b784bd0"],
      ["_sidebar.md", "fbc7e7abe764e4bd24c4f1afb866bd865fd727bf"],
      ["foo/a_page.md", "8e4fe564b631ebd093783639859785928ac594fe"],
      ["index.md", "317c970c68c93f470c19d7ecf200c27178de09f5"]
    ], results
  end
end
