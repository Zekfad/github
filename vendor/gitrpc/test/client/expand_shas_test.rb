# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class ClientExpandShasTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
  end

  def teardown
    @fixture.teardown
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.sha_valid?(nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.sha_valid?("lol")
    end
  end
end
