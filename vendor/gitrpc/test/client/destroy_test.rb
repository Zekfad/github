# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class ClientDestroyTest < Minitest::Test
  def setup
    @subfixture = RepositoryFixture.new
    @subfixture.setup
    @subfixture.command("mkdir -p dgit9/e/nw/01/23/45/678")
    @fixture = RepositoryFixture.new("#{@subfixture.path}/dgit9/e/nw/01/23/45/678/9abc.git")
    @fixture.setup

    setup_test_commits

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
  end

  def teardown
    @fixture.teardown
    @subfixture.teardown
  end

  def setup_test_commits
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
  end

  def test_destroy_network_replica
    refute @client.nw_linked?

    assert_nil @client.nw_link
    assert @client.nw_linked?

    assert_raises(GitRPC::IllegalPath) do  # not a network path
      @client.destroy_network_replica(@fixture.path)
    end

    networkpath = File.dirname(@fixture.path)
    assert_raises(GitRPC::IllegalPath) do  # network path, but not matching the rpc object
      @client.destroy_network_replica(networkpath)
    end

    @client2 = GitRPC::Backend.new(networkpath)
    assert File.exist?(networkpath)
    assert_nil @client2.destroy_network_replica(networkpath)
    refute File.exist?(networkpath)
  end
end
