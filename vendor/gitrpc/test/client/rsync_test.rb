# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class ClientRsyncTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @fixture2 = RepositoryFixture.new("test2.git")
    @fixture2.setup

    setup_test_commits

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
  end

  def teardown
    @fixture.teardown
    @fixture2.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture2.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture2.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_rsync
    assert_raises GitRPC::CommandFailed do
      @client.file_changed?("README", @oid1, @oid2)
    end

    res = @client.rsync("#{@fixture2.path}/", @fixture.path, archive: true, delete: true)
    assert res["ok"]

    assert_equal true, @client.file_changed?("README", @oid1, @oid2)
  end
end
