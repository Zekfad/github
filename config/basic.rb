# rubocop:disable Style/FrozenStringLiteralComment

# Lightweight environment configuration.
#
# This file is useful when you want to load a small portion of the whole
# github environment for daemons that want to keep their resident memory
# usage low or commands that need to boot fast.
#
# After requiring this file, the following is available:
#
# - RAILS_ENV constant is set.
# - Basic load path is configured so libraries may be required.
# - Rails.environment? predicate methods are available.
# - Rails.logger is set to a valid Logger object.
#
# This file should be required using its absolute canonical path without any
# relative path parts. The easiest way to accomplish this is to use expand_path
# as follows:
#
#     require File.expand_path('../../config/basic', __FILE__)

# Bail out early unless we're using safe-ruby
unless ENV["HAS_USED_SCRIPT_ENVIRONMENT"]
  abort "Error: The GitHub app requires using safe-ruby\n" \
        "Error: Try `#{$0.sub(/script\//, "bin/")}`"
end

config_basic_start = Process.clock_gettime(Process::CLOCK_MONOTONIC)

# Enable GC stats tracking first thing if the interpreter supports it
GC.enable_stats if GC.respond_to?(:enable_stats)

if ENV["TRACE_ALLOCATIONS"]
  require "objspace"
  ObjectSpace.trace_object_allocations_start
end

if ENV["GC_PROFILE"]
  GC::Profiler.enable
end

# Disable all Gem deprecation warnings.
if defined?(Gem::Deprecate) && Gem::Deprecate.respond_to?(:skip=)
  Gem::Deprecate.skip = true
end

# Calculate gem dir
vendor_gems = "vendor/gems/#{RUBY_VERSION}"

# We'll need this to get realpaths
require "pathname"

ROOT = Pathname.new(File.expand_path("../..", __FILE__)).realpath.to_s unless defined?(ROOT)

# Determine ENV["RAILS_ROOT"] from this file's location. We take the realpath
# to remove all symlinks and other non-canonical path components.
ENV["RAILS_ROOT"] ||= ROOT

running_tests = $PROGRAM_NAME == Pathname(ROOT).join("script/rails").to_s && (ARGV.first == "test" || ARGV.first.match(/^test[:_]/))

# Determine RAILS_ENV from the environment or current hostname
ENV["RAILS_ENV"] ||=
  if ENV["RAILS_ROOT"].start_with? "/data/github/"
    "production"
  elsif running_tests
    "test"
  else
    "development"
  end

# Mimic enough of Rails so you can check the current Rails environment and
# have a default logger.
if !defined?(Rails)
  module Rails
    # Replicate ActiveSupport::StringInquirer
    @env = Class.new(String) do
      def method_missing(method_name, *)
        method_name.to_s =~ /(.+)\?/ ? $1 == self : super
      end
    end.new(ENV["RAILS_ENV"])

    @root = Pathname.new(ENV["RAILS_ROOT"])

    class << self
      attr_reader :env, :root

      # environment predicates
      %w(development test production staging fi).each do |environment|
        method_name = :"#{environment}?"
        define_method(method_name) { self.env.send(method_name) }
      end
    end

    def self.logger
      return ::LOGGER if defined?(::LOGGER)
      @null_logger ||=
        begin
          require "logger"
          Logger.new(nil)
        end
    end
  end
end

# load gpanel config if it exists
gpanel_config = "#{Rails.root}/.app-config/#{Rails.env}.rb"
load gpanel_config if File.file? gpanel_config

# Set ENTERPRISE environment variable when runtime mode file exists in
# development or test. This means all commands abide by the currently
# selected, persistent runtime mode set by script/setup.
current_runtime_file = "#{Rails.root}/tmp/runtime/current"
if %w(development test).include?(ENV["RAILS_ENV"]) && File.exist?(current_runtime_file)
  current_runtime = File.read(current_runtime_file).chomp
  if current_runtime == "enterprise"
    ENV["ENTERPRISE"] = "1"
  else
    ENV["ENTERPRISE"] = ENV["E"] = ENV["FI"] = nil
  end
elsif ENV["RAILS_ENV"] == "production" && !ENV["ENTERPRISE"].to_s.empty?
  current_runtime = "enterprise"
else
  current_runtime = "dotcom"
  ENV["ENTERPRISE"] = ENV["E"] = ENV["FI"] = nil
end

unless ENV["BOOTSTRAPPING"]
  if %w(staging production).include?(ENV["RAILS_ENV"])
    # Verify the environment has been bootstrapped by checking that the
    # config/load_path.rb file has been generated.
    if !File.exist?("#{Rails.root}/config/load_path.rb")
      warn "WARN The #{current_runtime} gem environment is out-of-date or has yet to be bootstrapped."
      warn "     File not found: #{Rails.root}/config/load_path.rb"
      warn "     Run script/bootstrap to remedy this situation."
      fail "gem environment not configured in #{ENV["RAILS_ENV"]}, missing load_path.rb"
    end
  else
    if (ENV["CIBUILD_BOOTSTRAP_COMPLETE"] && !File.exist?("#{Rails.root}/config/load_path.rb"))
      warn "WARN The #{current_runtime} has no cached load_path file."
      warn "     But CIBUILD_BOOTSTRAP_COMPLETE is already set, refusing to autobootstrap."
      warn "Debugging output follows:"
      warn `pwd`
      warn `cat .git/HEAD`
      warn `git rev-parse HEAD`
      warn `git status`
      fail "gem environment not configured in #{ENV["RAILS_ENV"]} despite running in CI"
    end

    # Run a more exhaustive bootstrap check in non-production environments by making
    # sure the Gemfile matches the Ruby+Gemfile.lock checksum.
    #
    # Verify the environment has been bootstrapped by checking that the
    # config/load_path.rb file has been generated.
    if !File.exist?("#{Rails.root}/config/load_path.rb")
      warn "WARN The #{current_runtime} gem environment is out-of-date or has yet to be bootstrapped."
      warn "     File not found: #{Rails.root}/config/load_path.rb"
      warn "     Running script/bootstrap --local to remedy this situation..."
      system "#{Rails.root}/script/bootstrap --local"

      if !File.exist?("#{Rails.root}/config/load_path.rb")
        warn "WARN The #{current_runtime} gem environment is STILL out-of-date."
        warn "     File still not found: #{Rails.root}/config/load_path.rb"
        warn "     Please contact your network administrator."
        fail "gem environment not configured in #{ENV["RAILS_ENV"]}, still missing load_path.rb"
      end
    end

    require_relative "../lib/bootstrap_cache"
    cache = BootstrapCache.new(Rails.root)
    if cache.changed?
      if ENV["CIBUILD_BOOTSTRAP_COMPLETE"]
        warn "WARN The #{current_runtime} gem environment cache is out of date."
        warn "     But CIBUILD_BOOTSTRAP_COMPLETE is already set, refusing to autobootstrap."
        warn "Debugging output follows:"
        warn `pwd`
        warn `cat .git/HEAD`
        warn `git rev-parse HEAD`
        warn `git status`
        fail "gem environment not configured in #{ENV["RAILS_ENV"]} despite running in CI"
      end

      warn "WARN The #{current_runtime} gem environment is out-of-date or has yet to be bootstrapped."
      warn "     Running script/bootstrap --local to remedy this situation..."
      system "#{Rails.root}/script/bootstrap --local"

      if cache.changed?
        if cache.ruby_version.changed?
          warn "WARN It looks like ruby versions changed."
          fail "Re-run the command to pick up the new version. <3"
        end
        warn "WARN The #{current_runtime} gem environment is STILL out-of-date."
        warn "     Please contact your network administrator."
        fail "gem environment not configured in #{ENV["RAILS_ENV"]}, checksums on files '#{cache.changes.map(&:filenames).flatten}' dont match."
      end
    end

    # Verify that the git build is up to date, bootstrap if not
    if ENV["RAILS_ENV"] == "development" && !ENV["SKIP_GIT_BUILD"]
      pinned_git_version  = File.read("#{Rails.root}/config/git-version").
        split("\n").find { |line| line[0] != "#" }
      current_git_version = File.read("#{Rails.root}/vendor/git-core/version").
        strip rescue nil
      if current_git_version != pinned_git_version
        warn "WARN The git build is out-of-date.  Current: #{current_git_version}, pinned: #{pinned_git_version}. Building ..."
        system("#{Rails.root}/script/build-git") || fail("git build failed")
      end
    end

    # If script/server is run with LOCAL_PRIMER=1 flag,
    # npm link the primer packages on the developer's computer
    if ENV["RAILS_ENV"] == "development"
      primerized = File.exist?("#{Rails.root}/node_modules/primer") && File.symlink?("#{Rails.root}/node_modules/primer")
      want_local_primer = ENV["LOCAL_PRIMER"] == "1"
      if primerized
        warn "-" * 80
        warn " Development primer is linked to the local primer repository found here"
        warn " #{File.expand_path("../primer", Rails.root)}"
        warn "-" * 80
        unless want_local_primer
          warn "Turning off linked primer"
          system("#{Rails.root}/script/primerize off")
        end
      elsif want_local_primer
        warn "Linking primer on your machine to the GitHub development environment."
        warn " #{File.expand_path("../primer", Rails.root)}"
        system("#{Rails.root}/script/primerize on")
      end
    end
  end
end

# Disallow use of system gems by default in all environments. This ensures the
# gem environment is totally isolated to only stuff specified in the Gemfile.
require "rbconfig"
ENV["GEM_PATH"] = "#{Rails.root}/#{vendor_gems}/ruby/#{RbConfig::CONFIG['ruby_version']}"
ENV["GEM_HOME"] = "#{Rails.root}/#{vendor_gems}/ruby/#{RbConfig::CONFIG['ruby_version']}"

Gem.paths = ENV

# put RAILS_ROOT/bin on PATH
binpath = "#{Rails.root}/bin"
ENV["PATH"] = "#{binpath}:#{ENV['PATH']}" if !ENV["PATH"].include?(binpath)

# For dev and test, we need the `git-nw` executables in PATH:
if ENV["RAILS_ENV"] != "production"
  git_nw_path = "#{Rails.root}/vendor/git-nw-ng/bin"
  if !ENV["PATH"].split(":").include?(git_nw_path)
    ENV["PATH"] = "#{git_nw_path}:#{ENV['PATH']}"
  end
end

# Bring in the pregenerated load path file. See script/bootstrap.rb invocation
# of script/build-load-path-cache.rb for more on this.
if File.exist?("#{Rails.root}/config/load_path.rb")
  require "#{Rails.root}/config/load_path"
end

module Kernel
  alias_method :require, :gem_original_require
  private :require

  # Don't allow gem activations, we do this manually
  def gem(*)
  end
  private :gem
end

# Disable when bootstrapping as directories might not be in their final state yet.
# This is particularly important for our legacy git-based deploys.
# See https://github.com/github/developer-flow/issues/19
unless ENV["BOOTSTRAPPING"]
  require "bootsnap"

  Bootsnap.setup(
    cache_dir:            "#{Rails.root}/tmp/cache",
    development_mode:     ENV["RAILS_ENV"] == "development" && !ENV["PRELOAD"] && !ENV["FASTDEV"],
    load_path_cache:      true,
    autoload_paths_cache: false,
    disable_trace:        false,
    compile_cache_iseq:   %w(development test).include?(ENV["RAILS_ENV"]),
    compile_cache_yaml:   %w(development test).include?(ENV["RAILS_ENV"]),
  )
end

# Enable failbot reporting of unhandled exceptions.
require "github"
require "github/config/failbot"

require "github/boot_metrics"
GitHub::BootMetrics.initial_boot_time = config_basic_start

# Enable tracing.
require "rbtrace"

# Allow reflection on the version of Rails on the path
unless Rails.respond_to?(:version)
  require "rails/version"

  module Rails
    def self.version
      Rails::VERSION::STRING
    end
  end
end

# Zeus bails out if it detects RAILS_ENV is set, so we need to remove
# RAILS_ENV for Zeus to work
if ENV["GH_ZEUS"] == "1"
  ENV.delete "RAILS_ENV"
end

GitHub::BootMetrics.record_state("basic")
