# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :pull_reminders_migration do |access|
    access.allow(:first_party_slack_integrations)
  end
end
