# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :show_discussion do |access|
    discussion_must_be_readable(access)

    access.ensure_context :repo
    access.allow(:everyone) { |context| context[:repo].public? }
    access.allow(:discussion_reader)
  end

  define_access :show_discussion_comment do |access|
    resource_must_belong_to_repo(access)
    discussion_must_be_readable(access)

    access.allow(:everyone) { |context| context[:repo].public? }
    access.allow(:discussion_reader)
    access.allow(:discussion_comment_reader)
  end

  define_access :get_discussion_category do |access|
    resource_must_belong_to_repo(access)
    discussion_must_be_readable(access)

    access.allow(:everyone) { |context| context[:repo].public? }
    access.allow(:discussion_reader)
  end
end
