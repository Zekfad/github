# rubocop:disable Style/FrozenStringLiteralComment

class Api::AccessControl < Egress::AccessControl

  # Literally
  role :everyone

  role :nobody do |context|
    next false
  end

  # Authenticated with no OAuth scopes
  role :authenticated_user do |context|
    context.user
  end

  role :authenticated_user_using_personal_access_token do |context|
    context.user && context.user.using_personal_access_token?
  end

  # Repo roles

  role :repo_creator, scope: "public_repo" do |context|
    user = extract(context, :user)
    next false unless user

    actor = user.try(:installation) || user
    next true if !actor.can_have_granular_permissions?

    actor.permissions["administration"] == :write
  end

  role :private_repo_creator, scope: "repo" do |context|
    user = extract(context, :user)
    next false unless user

    actor = user.try(:installation) || user
    next true if !actor.can_have_granular_permissions?

    actor.permissions["administration"] == :write
  end

  role :associated_public_org_owned_repo_lister, scope: %w(public_repo read:org)
  role :private_repo_lister, scope: "repo"

  # A role exclusively mutating public resources (Watching, etc)
  role :public_repo_resource_mutator, scope: "public_repo"

  # A user who can read the given repository and all its subresources (e.g.,
  # issues, stargazers, releases, wiki, graphs, etc.).
  #
  # WARNING: Think twice before using this role. In most cases, you're better
  # off using a more *specific* role. For example, imagine we're adding a forum
  # to every repository: define a repo_forum_reader role and use it instead.
  role :repo_resources_reader do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) && repo.readable_by?(user)
  end

  role :v4_private_resource_lister do |context|
    user, resource = extract(context, :user, :resource)

    if resource.is_a?(Platform::GraphqlUserResource)
      Ability.can_at_least?(:read, resource.permissions)
    elsif resource == "metadata"
      SCOPES_WITH_PRIVATE_REPOSITORY_ACCESS.any? { |s| scope?(user, s) }
    else
      scope?(user, "repo")
    end
  end

  # A user that can read a repository's git contents.
  role :repo_contents_reader do |context|
    user, repo, key = extract(context, :user, :repo, :public_key)
    repo ||= extract(context, :resource)

    # We allow access if the actor is a user, they're allowed access, and they
    # can read from the repository, or if the actor is a deploy key belonging to
    # this repository.
    next true if user && oauth_allows_access?(user, repo) && repo.resources.contents.readable_by?(user)
    key && repo && key.deploy_key? && key.repository == repo
  end

  # A user that can write a repository's git contents.
  role :repo_contents_writer do |context|
    user, repo, key = extract(context, :user, :repo, :public_key)
    repo ||= extract(context, :resource)

    # We allow access if the actor is a user, they're allowed access, and they
    # can write to the repository, or if the actor is a read-write deploy key
    # belonging to this repository.
    next true if user && oauth_allows_access?(user, repo) && repo.resources.contents.writable_by?(user)
    key && repo && key.deploy_key? && key.repository == repo && !key.read_only?
  end

  # A user that can write a repository's packages.
  role :repo_packages_writer, scope: "write:packages" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)
    actor = user.try(:installation) || user

    if actor&.can_have_granular_permissions?
      repo.resources.packages.writable_by?(actor)
    else
      user && oauth_allows_package_read_access?(user, repo) && repo.resources.contents.writable_by?(user)
    end
  end

  # A user that can read a repository's packages.
  role :repo_packages_reader, scope: "read:packages" do |context|
    user, repo = extract(context, :user, :repo)
    next false unless user
    repo ||= extract(context, :resource)
    actor = user.try(:installation) || user

    if actor&.can_have_granular_permissions?
      repo.resources.packages.readable_by?(actor)
    else
      user && oauth_allows_package_read_access?(user, repo) && repo.resources.contents.readable_by?(user)
    end
  end

  # A user that can delete a private repository's packages.
  role :private_repository_package_deleter, scope: "delete:packages" do |context|
    user, repo = extract(context, :user, :repo)
    next false unless user && repo
    next false unless scope?(user, "read:packages")

    if user.can_have_granular_permissions?
      repo.resources.packages.writable_by?(user)
    else
      user && repo.adminable_by?(user)
    end
  end

  # A user that can write a specific file
  role :repo_file_writer do |context|
    user, repo, path = extract(context, :user, :repo, :path)
    repo ||= extract(context, :resource)

    can_write_file = user && oauth_allows_access?(user, repo) && repo.resources.file(path).writable_by?(user)

    next false unless can_write_file

    # If it's not a blacklisted path, it's fine.
    next true unless RefUpdatesPolicy.blacklisted_path_for_apps?(path)

    # These three conditionals implement a prohibition on apps pushing
    # code to blacklisted paths using the API.  There's a similar set of logic
    # in RefUpdatesPolicy, in app/models/ref_updates_policy.rb, that
    # governs writing workflows/a.yml using "git push."  If you make changes
    # here, consider updating that one, too.

    # Installations (server to server)
    if user.can_have_granular_permissions?
      installation = IntegrationInstallation.with_repository(repo).where(integration: user.installation.integration).first
      unless repo.resources.workflows.writable_by?(installation)
        next false
      end
    end


    # Oauth Apps without `workflow` scope
    next false if user.using_auth_via_oauth_application? && !user.oauth_access?("workflow")


    # GitHub Apps (user to server)
    if user.using_auth_via_integration?
      installation = IntegrationInstallation.with_repository(repo).where(integration: user.oauth_access.application).first
      unless repo.resources.workflows.writable_by?(installation)
        next false
      end
    end

    true
  end

  # A user that can read a specific file
  role :repo_file_reader do |context|
    user, repo, path = extract(context, :user, :repo, :path)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) && repo.resources.file(path).readable_by?(user)
  end

  role :repo_vulnerability_alert_reader do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) &&
      repo.vulnerability_alerts_visible_to?(user) && oauth_application_policy_satisfied?(user, repo)
  end

  role :repo_administration_reader do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) &&
      repo.resources.administration.readable_by?(user) && oauth_application_policy_satisfied?(user, repo)
  end

  role :repo_invitations_reader do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)
    next unless user && repo
    next unless oauth_allows_access?(user, repo) || scope?(user, "repo:invite")

    repo.resources.administration.readable_by?(user) && oauth_application_policy_satisfied?(user, repo)
  end

  role :repo_invitations_writer do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)
    next unless user && repo
    next unless oauth_allows_access?(user, repo) || scope?(user, "repo:invite")

    repo.resources.administration.writable_by?(user) && oauth_application_policy_satisfied?(user, repo)
  end

  role :repo_administration_writer do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) &&
      repo.resources.administration.writable_by?(user) && oauth_application_policy_satisfied?(user, repo)
  end

  role :repo_interaction_limiter do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    next unless user && oauth_allows_access?(user, repo) && oauth_application_policy_satisfied?(user, repo)
    repo.async_can_set_interaction_limits?(user).sync
  end

  role :topics_manager do |context|
    user, repo = extract(context, :user, :resource)

    next false unless user && repo
    next false unless oauth_allows_access?(user, repo) && oauth_application_policy_satisfied?(user, repo)

    repo.can_manage_topics?(user)
  end

  role :repo_pages_reader do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) && repo.resources.pages.readable_by?(user)
  end

  role :repo_pages_writer do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) && repo.resources.pages.writable_by?(user)
  end

  role :repo_comment_minimizer do |context|
    user, comment = extract(context, :user, :resource)
    user && comment && scope?(user, "repo") && comment.async_minimizable_by?(user).sync
  end

  # A user who can read the given private repository and all its subresources
  # (e.g., issues, stargazers, releases, wiki, graphs, etc.).
  #
  # WARNING: Think twice before using this role. In most cases, you're better
  # off using a more *specific* role. For example, imagine we're adding a forum
  # to every repository: define a private_repo_forum_reader role and use it
  # instead.
  role :private_repo_resources_reader, scope: "repo" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) && repo.readable_by?(user)
  end

  # A user that has full write access to a repository and all its
  # subresources (i.e., write access to milestones, labels, releases, git
  # contents, etc.)
  #
  # WARNING: Think twice before using this role. In most cases, you're better
  # off using a more *specific* role. For example, imagine we're adding a forum
  # to every repository: define a repo_forum_writer role and use it instead.
  role :repo_resources_writer do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)
    user && oauth_allows_access?(user, repo) && repo.writable_by?(user)
  end

  role :repo_transferer do |context|
    user, repo, new_owner = extract(context, :user, :repo, :new_owner)
    repo ||= extract(context, :resource)

    actor = user.try(:installation) || user

    user_results = user && oauth_allows_access?(user, repo) &&
      repo.resources.administration.writable_by?(user) && oauth_application_policy_satisfied?(user, repo)
    next false unless user_results

    if actor && actor.can_have_granular_permissions?
      installation = actor.integration.installations.find_by(target: new_owner)
      next false unless installation

      user_results && installation.permissions["administration"] == :write
    else
      user_results
    end
  end

  # A user who has admin access to the given repo
  role :repo_admin do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)
    user && oauth_allows_access?(user, repo) &&
      repo.adminable_by?(user) && oauth_application_policy_satisfied?(user, repo)
  end

  # A user who has read access to *at least one* subresource on the given repo
  role :repo_metadata_reader do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) && repo.resources.metadata.readable_by?(user)
  end

  # `repo_metadata_reader` above mandates that a user have the `repo` token scope, but there are other
  # tokens, like `repo:status` or `repo_deployment`, which ought also be granted private repo access
  role :v4_private_repo_metadata_reader, scope: SCOPES_WITH_PRIVATE_REPOSITORY_ACCESS + ["repo:invite"] do |context|
    user, repo, from_invitation = extract(context, :user, :repo, :from_invitation)

    # viewing a repository from an invitation is a special case. we grant access to the metadata,
    # but as far as the abilities are concerned, `repo.resources.metadata.readable_by?` is false
    next true if from_invitation
    user && repo.resources.metadata.readable_by?(user)
  end

  role :v4_private_repo_lister, scope: SCOPES_WITH_PRIVATE_REPOSITORY_ACCESS

  role :installed_integration do |context|
    user, repo, current_integration = extract(context, :user, :resource, :current_integration)
    associated_repository_ids = context[:associated_repository_ids]

    if associated_repository_ids.nil?
      user.respond_to?(:installation) &&
        user.installation.repositories.exists?(repo.id)
    elsif current_integration
      IntegrationInstallation.with_repository(repo).find_by(integration: current_integration.id).present?
    else
      user.respond_to?(:installation) &&
        associated_repository_ids.include?(repo.id)
    end
  end

  # A user with permission to receive a redirect when accessing a relocated
  # repository via its old name-with-owner. (Because a redirect can reveal the
  # existence of a private repository, we only redirect for requests that have
  # permission to know that a repository exists).
  role :repo_redirect_follower, scope: SCOPES_WITH_PRIVATE_REPOSITORY_ACCESS do |context|
    user, repo = extract(context, :user, :resource)
    user && repo.pullable_by?(user)
  end

  # A user that is an owner of a repo
  role :repo_owner do |context|
    user, repo = extract(context, :user, :repo)
    user && oauth_allows_access?(user, repo) &&
      repo.owner == user
  end

  # A user that has at least push access to the given repo
  role :repo_member do |context|
    user, repo = extract(context, :user, :repo)
    user && oauth_allows_access?(user, repo) &&
      repo.pushable_by?(user)
  end

  # A user who can admin a repo or is removing their own membership from a repo
  role :repo_member_remover do |context|
    user, repo, collab = extract(context, :user, :repo, :collab)
    repo ||= extract(context, :resource)
    user && oauth_allows_access?(user, repo) &&
      (repo.resources.administration.writable_by?(user) || (user == collab && repo.member?(user)))
  end

  # A user that authored a comment
  role :commit_comment_author, depends_on: [:repo_resources_reader] do |context|
    user, repo, comment = extract(context, :user, :repo, :comment)
    actor = user.try(:bot) || user
    next false unless actor && repo

    is_author = oauth_allows_access?(actor, repo) && comment && comment.user_id == actor.id

    if actor.bot?
      is_author && repo.resources.contents.writable_by?(actor)
    else
      is_author
    end
  end

  role :org_repo_creator, scope: %w(public_repo) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.can_create_repository?(user)
  end

  role :repo_forker do |context|
    user, repo, org = extract(context, :user, :resource, :organization)
    next unless user && repo

    readable = oauth_allows_access?(user, repo) && repo.resources.contents.readable_by?(user)
    next unless readable

    actor = user.try(:installation) || user

    if actor.can_have_granular_permissions?
      actor.installed_on_all_repositories? && actor.permissions["administration"] == :write
    elsif org.present?
      org.can_create_repository?(actor)
    else
      # We are defaulting to true, because if you're not forking to an organization
      # and you're not a GitHub App, then you're a user forking to your own account.
      true
    end
  end

  role :org_private_repo_creator, scope: %w(repo) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.can_create_repository?(user, visibility: "private")
  end

  # A user who has access to delete a repository.
  #
  # Deleting a repository requires oauth and the oauth scope 'delete_repo'.
  role :repo_deleter, scope: "delete_repo" do |context|
    user, repo = extract(context, :user, :resource)
    user && repo.resources.administration.writable_by?(user)
  end

  # A user with pull access to commit status
  role :status_reader, scope: "repo:status" do |context|
    user, repo = extract(context, :user, :resource)

    user && repo.resources.statuses.readable_by?(user)
  end

  # A user who has access to write commit statuses
  role :status_writer do |context|
    user, repo = extract(context, :user, :resource)
    user && oauth_allows_status_access?(user, repo) && repo.resources.statuses.writable_by?(user)
  end

  # A user with access to list deployments
  role :deployment_lister, scope: "repo_deployment" do |context|
    user, repo = extract(context, :user, :resource)
    user && repo.resources.deployments.readable_by?(user)
  end

  # A user with access to read deployments
  role :deployment_reader, scope: "repo_deployment" do |context|
    user, deployment = extract(context, :user, :resource)
    repo = deployment.repository
    user && repo.resources.deployments.readable_by?(user)
  end

  # A user with access to write deployments
  role :deployment_writer do |context|
    user, deployment = extract(context, :user, :resource)
    repo = deployment.repository
    user && oauth_allows_deploy_access?(user, repo) && repo.resources.deployments.writable_by?(user)
  end

  # A user with privilege to disable/enable access to this repository.
  role :repo_access_admin, scope: "site_admin" do |context|
    context.user && context.user.site_admin?
  end

  role :repo_hook_reader, scope: "read:repo_hook" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    next false unless user && repo
    repo.resources.repository_hooks.readable_by?(user)
  end

  role :repo_hook_writer, scope: "write:repo_hook" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    next false unless user && repo
    repo.resources.repository_hooks.writable_by?(user)
  end

  role :repo_hook_admin, scope: "admin:repo_hook" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    next false unless user && repo
    repo.resources.repository_hooks.writable_by?(user)
  end

  role :org_hook_reader, scope: "admin:org_hook" do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_hooks.readable_by?(user)
  end

  role :org_hook_writer, scope: "admin:org_hook" do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_hooks.writable_by?(user)
  end

  role :repo_pre_receive_hook_reader, scope: "admin:pre_receive_hook" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)
    user && repo.resources.repository_pre_receive_hooks.readable_by?(user)
  end

  role :repo_pre_receive_hook_writer, scope: "admin:pre_receive_hook" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)
    user && repo.resources.repository_pre_receive_hooks.writable_by?(user)
  end

  role :org_pre_receive_hook_reader, scope: "admin:pre_receive_hook" do |context|
    user, org = extract(context, :user, :resource)
    next false unless user && org

    org.resources.organization_pre_receive_hooks.readable_by?(user)
  end

  role :org_pre_receive_hook_writer, scope: "admin:pre_receive_hook" do |context|
    user, org = extract(context, :user, :resource)
    next false unless user && org

    org.resources.organization_pre_receive_hooks.writable_by?(user)
  end

  # Org roles

  role :org_two_factor_auditor, scope: %w(read:org repo user) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.adminable_by?(user)
  end

  role :user_self_accessible_orgs_lister, scope: %w(read:org repo user) do |context|
    context.user
  end

  role :user_self_org_membership_reader, scope: %w(read:org repo user) do |context|
    next false unless context.user

    if (installation = context.user.try(:installation))
      context.resource.resources.members.readable_by?(installation)
    else
      context.member && context.user == context.member
    end
  end

  role :user_self_org_membership_writer, scope: %w(write:org repo user) do |context|
    member, organization, user = extract(context, :resource, :organization, :user)

    next false unless user && organization

    if user.can_have_granular_permissions?
      organization.resources.members.writable_by?(user)
    else
      member && user == member
    end
  end

  role :v4_user_org_member_reader do |context|
    user, resource = extract(context, :user, :resource)

    if resource.is_a?(Platform::GraphqlUserResource)
      Ability.can_at_least?(:read, resource.permissions)
    else
      scope?(user, "read:org")
    end
  end

  role :org_member_reader, scope: %w(read:org repo user) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.members.readable_by?(user)
  end

  role :v4_org_member_reader, scope: %w(read:org) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.members.readable_by?(user)
  end

  role :installation_org_member_reader do |context|
    user, org = extract(context, :user, :resource)
    actor = user.try(:installation) || user

    actor && actor.can_have_granular_permissions? &&
      org && org.resources.members.readable_by?(actor)
  end

  role :org_member_writer, scope: %w(admin:org repo) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.members.writable_by?(user)
  end

  role :v4_org_member_writer, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.members.writable_by?(user)
  end

  role :team_member_reader, scope: %w(read:org repo user) do |context|
    user, team = extract(context, :user, :team)
    user && team && team.visible_to?(user)
  end

  role :v4_team_member_reader, scope: %w(read:org read:discussion) do |context|
    user, team = extract(context, :user, :team)
    user && team && team.visible_to?(user)
  end

  role :team_repo_lister, scope: %w(read:org repo user) do |context|
    user, team = extract(context, :user, :team)
    user && team && team.visible_to?(user)
  end

  role :team_repo_reader, scope: %w(read:org repo user) do |context|
    user, team, repo = extract(context, :user, :team, :repo)

    next false unless team.visible_to?(user)
    next true if repo.public?

    if user.can_have_granular_permissions?
      repo.resources.metadata.readable_by?(user)
    else
      # If the repo is private, the user must have read access to it *and*
      # have the repo scope.
      repo.pullable_by?(user) && scope?(user, "repo")
    end
  end

  role :team_repo_adder, scope: %w(admin:org repo) do |context|
    user, team, repo = extract(context, :user, :team, :repo)

    # The team must be visible to the
    # user, and the user must have admin access to the repo.
    next false unless team.visible_to?(user)
    repo.resources.administration.writable_by?(user)
  end

  role :team_repo_remover, scope: %w(admin:org repo) do |context|
    user, team, repo = extract(context, :user, :team, :repo)

    next false unless team.visible_to?(user)

    if user.can_have_granular_permissions?
      repo.resources.administration.writable_by?(user) || \
        team.organization.resources.members.writable_by?(user)
    else
      next false unless repo.pullable_by?(user)

      # The user must have admin access to either the repo or the team.
      repo.adminable_by?(user) || team.adminable_by?(user)
    end
  end

  role :team_project_lister, scope: %w(read:org) do |context|
    user, team = extract(context, :user, :resource)
    user && team && team.visible_to?(user)
  end

  role :team_project_adder, scope: %w(admin:org) do |context|
    user, team, project = extract(context, :user, :resource, :project)

    if [user, team, project].all?
      # We need a user, a team, and a project. The team must be visible to the
      # user, and the user must have admin access to the project.
      team.visible_to?(user) && project.adminable_by?(user)
    else
      false
    end
  end

  role :team_project_remover, scope: %w(admin:org) do |context|
    user, team, project = extract(context, :user, :resource, :project)

    if [user, team, project].all? && team.visible_to?(user) && project.readable_by?(user)
      # We need a user, a team, and a project. The team and project must be
      # visible to the user, and the user must have admin access to either the
      # project or the team.
      project.adminable_by?(user) || team.adminable_by?(user)
    else
      false
    end
  end

  # A user who can admin the team
  role :team_maintainer, scope: %w(admin:org repo) do |context|
    user, team = extract(context, :user, :team)
    user && team && team.adminable_by?(user)
  end

  role :v4_team_maintainer, scope: %w(write:org) do |context|
    actor, team = extract(context, :user, :team)
    actor && team && team.updatable_by?(actor)
  end

  role :team_membership_reader, scope: %w(read:org repo) do |context|
    user, team = extract(context, :user, :team)
    user && team && team.visible_to?(user)
  end

  role :user_self_team_membership_reader, scope: %w(read:org repo) do |context|
    context.user && context.member && context.user == context.member
  end

  role :team_membership_deleter, scope: %w(admin:org repo) do |context|
    user, team = extract(context, :user, :team)
    user && team && team.adminable_by?(user)
  end

  role :user_self_team_membership_deleter, scope: %w(admin:org repo) do |context|
    context.user && context.member && context.user == context.member
  end

  # A user with proper OAuth scope. Needed because /user/orgs
  # requires authentication but returns public info without
  # proper scope.
  role :team_lister, scope: %w(read:org repo user) do |context|
    context.user
  end

  role :user_orgs_lister, scope: %w(read:org repo user) do |context|
    context.user && context.user == context.target
  end

  # A user who can create teams for this org
  role :team_creator, scope: %w(admin:org repo) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.can_create_team?(user)
  end

  role :v4_team_creator, scope: %w(write:org) do |context|
    actor, org = extract(context, :user, :org)
    actor && org && org.can_create_team?(actor)
  end

  role :team_discussions_lister, scope: %w(read:discussion) do |context|
    user, team = extract(context, :user, :resource)
    team&.visible_to?(user)
  end

  role :team_discussion_reader, scope: %w(read:discussion) do |context|
    user, discussion = extract(context, :user, :resource)
    discussion&.readable_by?(user)
  end

  role :team_discussion_creator, scope: %w(write:discussion) do |context|
    user, team, = extract(context, :user, :resource)
    team&.can_create_team_discussion?(user)
  end

  role :team_discussion_updater, scope: %w(write:discussion) do |context|
    user, discussion = extract(context, :user, :resource)
    discussion&.viewer_can_update?(user)
  end

  role :team_discussion_deleter, scope: %w(write:discussion) do |context|
    user, discussion = extract(context, :user, :resource)
    discussion&.viewer_can_delete?(user)
  end

  role :team_discussion_comments_lister, scope: %w(read:discussion) do |context|
    user, discussion = extract(context, :user, :resource)
    discussion&.readable_by?(user)
  end

  role :team_discussion_comment_reader, scope: %w(read:discussion) do |context|
    user, comment = extract(context, :user, :resource)

    # Make an exception to the platform enforcement rule to allow loading of the
    # parent discussion object.
    comment&.readable_by?(user)
  end

  role :team_discussion_comment_creator, scope: %w(write:discussion) do |context|
    user, discussion = extract(context, :user, :resource)
    discussion&.viewer_can_update?(user)
  end

  role :team_discussion_comment_updater, scope: %w(write:discussion) do |context|
    user, comment = extract(context, :user, :resource)

    # Make an exception to the platform enforcement rule to allow loading of the
    # parent discussion object.
    comment&.viewer_can_update?(user)
  end

  role :team_discussion_comment_deleter, scope: %w(write:discussion) do |context|
    user, comment = extract(context, :user, :resource)

    # Make an exception to the platform enforcement rule to allow loading of the
    # parent discussion object.
    comment&.viewer_can_delete?(user)
  end

  # A user who has rights to view the organization's outside collaborators
  role :org_outside_collaborator_reader, scope: %w(read:org repo) do |context|
    user, org = extract(context, :user, :resource)
    next false unless user && org

    actor = user.try(:installation) || user
    if actor.can_have_granular_permissions?
      org.resources.members.readable_by?(actor)
    else
      org.adminable_by?(actor)
    end
  end

  # A user who has admin rights for an organization
  role :v3_org_admin, scope: %w(admin:org repo) do |context|
    user, org = extract(context, :user, :resource)

    next false unless user && org

    if org.user?
      if (organization_via_context = extract(context, :organization))
        org = organization_via_context
      end
    end

    org.adminable_by?(user)
  end

  role :organization_administration_writer, scope: %w(admin:org repo) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_administration.writable_by?(user)
  end

  role :organization_administration_reader, scope: %w(admin:org repo) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_administration.readable_by?(user)
  end

  role :v4_organization_administration_writer, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_administration.writable_by?(user)
  end

  role :v4_organization_administration_reader, scope: %w(read:org) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_administration.readable_by?(user)
  end

  role :organization_plan_reader, scope: %w(read:org) do |context|
    user, org = extract(context, :user, :resource)
    org.resources.organization_plan.readable_by?(user)
  end

  # A user who has admin rights for an organization
  role :org_admin, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.adminable_by?(user)
  end

  role :organization_blocks_reader, scope: %w(admin:org repo) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_user_blocking.readable_by?(user)
  end

  role :organization_blocks_writer, scope: %w(admin:org repo) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_user_blocking.writable_by?(user)
  end

  # A user who has admin rights for an org's business
  role :org_business_admin, scope: %w(admin:enterprise) do |context|
    user, org = extract(context, :user, :current_org)
    business = org&.async_business&.sync

    user && business && business.adminable_by?(user)
  end

  # A user who can set interaction limits at the organization level
  role :organization_interaction_limits_writer, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_administration.writable_by?(user)
  end

  # A user who can read interaction limits at the organization level
  role :organization_interaction_limits_reader, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)
    user && org && org.resources.organization_administration.readable_by?(user)
  end

  # Business roles

  # A user that has admin rights on a business
  role :business_admin, scope: %w(admin:enterprise) do |context|
    user, business = extract(context, :user, :resource)
    next false if !user || !business

    business.resources.enterprise_administration.writable_by?(user)
  end

  # A user that has billing manager rights on a business
  role :business_billing_manager, scope: %w(manage_billing:enterprise) do |context|
    user, business = extract(context, :user, :resource)
    user && business && (business.billing_manager?(user) || business.owner?(user))
  end

  # A user that can cancel a business org invitation (either an admin of the
  # business or an admin of the org).
  role :business_organization_invitation_canceler, scope: %w(admin:enterprise admin:org) do |context|
    user, invitation = extract(context, :user, :resource)
    user && invitation && (
      invitation.business.adminable_by?(user) || invitation.invitee.adminable_by?(user)
    )
  end

  # A user that can view the business profile
  role :business_profile_viewer, scope: %w(read:enterprise) do |context|
    user, business = extract(context, :user, :resource)
    user && business && business.readable_by?(user)
  end

  # A user that can view a business member invitation
  role :business_member_invitation_viewer do |context|
    user, invitation = extract(context, :user, :resource)
    user && invitation && invitation.readable_by?(user)
  end

  # A user or integration that has `user_provisioning` permission on the Enterprise.
  role :business_user_provisioner, scope: %w(admin:enterprise) do |context|
    next false unless GitHub.private_instance_user_provisioning?
    user, business = extract(context, :user, :resource)
    next false if !user || !business

    business.resources.user_provisioning.writable_by?(user)
  end

  # User roles

  # A user with privilege to administer other users
  role :site_admin do |context|
    current_user, route_pattern, request_method = extract(context, :user, :route_pattern, :request_method)
    # Check user scope, and role based permissions
    (!GitHub.require_site_admin_scope? || scope?(current_user, "site_admin")) && Stafftools::AccessControl.api_authorized?(current_user, { route_pattern: route_pattern, request_method: request_method })
  end

  # Logged in user reading their own user data
  role :user_self_reader, scope: "read:user" do |context|
    current_user, user = extract(context, :user, :resource)
    current_user && current_user == user
  end

  role :user_reader do |context|
    context.user
  end

  role :user_hovercard_reader, scope: "repo"

  role :user_editor, scope: "user"
  role :user_emailer, scope: "user:email"

  role :user_exporter do |context|
    actor, user = extract(context, :user, :resource)
    next false unless scope?(actor, "repo") && scope?(actor, "user")
    actor == user
  end

  role :public_key_reader, scope: "read:public_key"
  role :public_key_writer, scope: "write:public_key"
  role :public_key_admin,  scope: "admin:public_key"

  role :gpg_key_lister, scope: "read:gpg_key" do |context|
    current_user = extract(context, :user)
    next true unless current_user.using_auth_via_integration?

    current_user.resources.gpg_keys.readable_by?(current_user)
  end

  role :gpg_key_reader, scope: "read:gpg_key" do |context|
    current_user = extract(context, :user)
    current_user.resources.gpg_keys.readable_by?(current_user)
  end

  role :gpg_key_writer, scope: "write:gpg_key" do |context|
    current_user = extract(context, :user)
    current_user.resources.gpg_keys.writable_by?(current_user)
  end

  role :gpg_key_admin, scope: "admin:gpg_key" do |context|
    current_user = extract(context, :user)
    current_user.resources.gpg_keys.writable_by?(current_user)
  end

  role :user_email_reader do |context|
    current_user = extract(context, :user)
    current_user.resources.emails.readable_by?(current_user.oauth_access)
  end

  role :user_email_writer do |context|
    current_user = extract(context, :user)
    current_user.resources.emails.writable_by?(current_user.oauth_access)
  end

  role :user_keys_reader do |context|
    current_user = extract(context, :user)
    current_user.resources.keys.readable_by?(current_user)
  end

  role :user_keys_writer do |context|
    current_user = extract(context, :user)
    current_user.resources.keys.writable_by?(current_user)
  end

  role :user_followers_reader do |context|
    current_user = extract(context, :user)
    current_user.resources.followers.readable_by?(current_user)
  end

  role :user_followers_writer, scope: "user:follow" do |context|
    current_user = extract(context, :user)
    current_user.resources.followers.writable_by?(current_user)
  end

  role :user_blocking_reader, scope: "user" do |context|
    current_user = extract(context, :user)
    current_user.resources.blocking.readable_by?(current_user)
  end

  role :user_blocking_writer, scope: "user" do |context|
    current_user = extract(context, :user)
    current_user.resources.blocking.writable_by?(current_user)
  end

  role :user_repository_watching_reader do |context|
    current_user, repo = extract(context, :user, :resource)

    next false unless oauth_allows_access?(current_user, repo)
    next false unless repo.resources.metadata.readable_by?(current_user)

    current_user.resources.watching.readable_by?(current_user)
  end

  role :user_repository_watching_writer do |context|
    current_user, repo = extract(context, :user, :resource)

    next false unless oauth_allows_access?(current_user, repo)
    next false unless repo.resources.metadata.readable_by?(current_user)

    current_user.resources.watching.writable_by?(current_user)
  end

  role :user_repository_starring_reader do |context|
    current_user, repo = extract(context, :user, :resource)

    # This is to match parity with the old access control
    # where you didn't need a scope of any kind.
    next true if repo.public? && !current_user.using_auth_via_integration?

    next false unless oauth_allows_access?(current_user, repo)
    next false unless repo.resources.metadata.readable_by?(current_user)

    current_user.resources.starring.readable_by?(current_user)
  end

  role :user_repository_starring_writer do |context|
    current_user, repo = extract(context, :user, :resource)

    next false unless oauth_allows_access?(current_user, repo)
    next false unless repo.resources.metadata.readable_by?(current_user)

    current_user.resources.starring.writable_by?(current_user)
  end

  # GitHub app installations can read this from users
  role :user_plan_reader, scope: "user" do |context|
    current_user, user = extract(context, :user, :resource)

    # Organizations have `organization_plan` instead, see `organization_plan_reader`
    next false unless user.resources.respond_to?(:plan)

    user.resources.plan.readable_by?(current_user)
  end

  role :external_contributions_writer do |context|
    user = extract(context, :user)

    user.using_auth_via_integration? &&
      user.resources.external_contributions.writable_by?(user.oauth_access)
  end

  role :enterprise_vulnerabilities_reader do |context|
    user = extract(context, :user)

    next false unless user.can_have_granular_permissions?
    Ability.can_at_least?(:read, user.installation.permissions["enterprise_vulnerabilities"])
  end

  role :v4_user_emailer, scope: %w(read:user user:email) do |context|
    current_user, target = extract(context, :user, :target)

    # in the event of an email being private, we'll require that
    # only the actual user can read their own email
    next true if target.nil?
    next false unless current_user == target

    # Unless the request is user-to-server we're good to go.
    next true unless current_user.using_auth_via_integration?

    target.resources.emails.readable_by?(current_user)
  end

  role :user_audit_log_reader, scope: "user" do |context|
    viewer, audit_log_actor = extract(context, :user, :resource)

    next false unless viewer == audit_log_actor
    (viewer.using_personal_access_token? || viewer.using_basic_auth?)
  end

  # Gist roles

  # Anyone can create a gist (anon included). Oauth must have 'gist' scope.
  role :gist_creator do |context|
    user = extract(context, :user)
    if GitHub.anonymous_gist_creation_enabled?
      scope?(user, "gist")
    else
      user && scope?(user, "gist")
    end
  end

  role :gist_private_lister do |context|
    user, target = extract(context, :user, :target)

    valid = user && target && scope?(user, "gist")

    if valid
      if target.is_a?(Organization)
        target.adminable_by?(user)
      else
        user.id == target.id
      end
    end
  end

  # A user that can edit a gist. Oauth must have 'gist' scope.
  role :gist_editor do |context|
    user, gist = extract(context, :user, :resource)
    user && gist.try(:available?) && scope?(user, "gist") && gist.user_id == user.id
  end

  # A user that can fork a gist. Oauth must have 'gist' scope.
  role :gist_forker do |context|
    user, gist = extract(context, :user, :resource)
    user && gist.try(:available?) && scope?(user, "gist")
  end

  role :gist_fork_reader do |context|
    user, gist = extract(context, :user, :resource)
    user && gist.try(:available?)
  end

  role :gist_star_reader do |context|
    user, gist = extract(context, :user, :resource)
    user && gist.try(:available?)
  end

  # A user that can star a gist. Oauth must have 'gist' scope.
  role :gist_starrer do |context|
    user, gist = extract(context, :user, :resource)
    user && gist.try(:available?) && scope?(user, "gist")
  end

  role :gist_comment_creator do |context|
    user, gist = extract(context, :user, :resource)
    user && gist.try(:available?) && scope?(user, "gist")
  end

  role :gist_comment_editor do |context|
    user, comment = extract(context, :user, :resource)
    user && comment && scope?(user, "gist") && user.id == comment.user_id
  end

  role :gist_comment_minimizer do |context|
    user, comment = extract(context, :user, :resource)
    user && comment && scope?(user, "gist") && comment.async_minimizable_by?(user).sync
  end

  # A user that can read issues in a repo
  role :issue_reader, scope: "repo" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) && repo.resources.issues.readable_by?(user)
  end

  # A user that can create or update all issues in a repo
  role :issue_writer do |context|
    user, repo = extract(context, :user, :repo)

    user && oauth_allows_access?(user, repo) && repo.resources.issues.writable_by?(user)
  end

  # # A user that can create or update all issues in both the new repo
  # # and the original repo the issue is transferred from
  role :issue_transferer do |context|
    user, new_repo, issue = extract(context, :user, :repo, :issue)

    actor = user.try(:bot) || user

    return false unless actor

    old_repo = issue.repository

    oauth_allows_access?(actor, new_repo) && oauth_allows_access?(actor, old_repo) &&
      new_repo.resources.contents.writable_by?(actor) && old_repo.resources.contents.writable_by?(actor)
  end

  # A user who is the author of an issue
  # and has write access to it
  role :issue_author, depends_on: [:repo_resources_reader] do |context|
    user, issue, repo = extract(context, :user, :resource, :repo)
    actor = user.try(:bot) || user

    is_author = (issue.user_id == actor.id || issue.assignee_id == actor.id)

    if actor.bot?
      # installation might not still have write access to issue even if
      # it were the initial author
      is_author && repo.resources.issues.writable_by?(actor)
    else
      is_author
    end
  end

  # A user that can read comments on an issue
  role :issue_comment_reader do |context|
    user, resource, repo = extract(context, :user, :resource, :repo)

    user && oauth_allows_access?(user, repo) && repo.resources.issues.readable_by?(user) &&

    # Resource must exist and be readable by the user.
    resource && resource.readable_by?(user)
  end

  # A user that is the author of an issue comment
  # and has write access to issue
  role :issue_comment_author, depends_on: [:repo_resources_reader] do |context|
    user, comment, repo = extract(context, :user, :resource, :repo)
    actor = user.try(:bot) || user

    is_author = comment.user_id == actor.id

    if actor.bot?
      is_author && repo.resources.issues.writable_by?(actor)
    else
      is_author
    end
  end

  # A user that can lock an issue
  role :issue_locker do |context|
    user, repo, issue = extract(context, :user, :repo, :resource)

    next false unless user && repo && issue

    next false unless oauth_allows_access?(user, repo) && repo.resources.metadata.readable_by?(user)

    issue && issue.lockable_by?(user)
  end

  # A user that can label an issue or pr
  role :issue_or_pr_labeller do |context|
    user, repo, issue_or_pr = extract(context, :user, :repo, :resource)

    oauth_allows_access?(user, repo) && issue_or_pr.labelable_by?(actor: user)
  end

  # A user that can add assignees to an issue
  role :issue_assigner do |context|
    user, repo, issue = extract(context, :user, :repo, :resource)

    next false unless user && repo && issue

    next false unless oauth_allows_access?(user, repo)

    issue.assignable_by?(actor: user)
  end

  role :dashboard_user do |context|
    user = extract(context, :user)
    user && scope?(user, "public_repo")
  end

  role :private_dashboard_user, scope: "repo" do |context|
    extract(context, :user)
  end

  # Pull Request roles

  # A user who is the author of a pull request
  role :pull_request_author, depends_on: [:repo_resources_reader] do |context|
    user, pull, repo = extract(context, :user, :resource, :repo)
    actor = user.try(:bot) || user

    is_author = pull.user_id == actor.id

    if actor.bot?
      is_author && repo.resources.pull_requests.writable_by?(actor)
    else
      is_author
    end
  end

  # A user that is the author of a pull request comment
  role :pull_request_comment_author, depends_on: [:repo_resources_reader] do |context|
    user, comment, repo = extract(context, :user, :resource, :repo)
    actor = user.try(:bot) || user

    is_author = comment.user_id == actor.id

    if actor.bot?
      is_author && repo.resources.pull_requests.writable_by?(actor)
    else
      is_author
    end
  end

  # A user that can read pull requests in a repo
  role :pull_request_reader do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) &&
      repo.resources.pull_requests.readable_by?(user)
  end

  # A user that can write pull requests in a repo
  role :pull_request_writer do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) &&
      repo.resources.pull_requests.writable_by?(user)
  end

  # A user that can request pull request reviews
  role :pull_request_review_requester do |context|
    user, repo, pr = extract(context, :user, :repo, :resource)

    next false unless user && repo && pr
    next false unless oauth_allows_access?(user, repo)

    pr.can_request_review?(user)
  end

  role :pull_request_review_author, scope: ["repo", "public_repo"] do |context|
    user, review = extract(context, :user, :resource)

    next false unless user && review
    next false unless oauth_allows_access?(user, review.repository)

    review_accessible = if user.can_have_granular_permissions?
      review.repository.resources.pull_requests.writable_by?(user)
    elsif user.using_auth_via_integration?
      integration  = user.oauth_access.application
      installation = user.oauth_access.installation || integration.installations.find_by(target: review.repository.owner)

      installation && review.repository.resources.pull_requests.writable_by?(installation)
    else
      true
    end

    review_accessible && review.user_id == user.id
  end

  role :v4_repository_invitation_reader do |context|
    user, invitation = extract(context, :user, :resource)
    next false unless user && invitation

    repository = invitation.repository
    next false unless repository && oauth_allows_invitation_access?(user, repository)

    invitation.readable_by?(user)
  end

  role :user_public_repository_invitation_reader, scope: ["public_repo", "repo:invite"] do |context|
    context.user
  end

  role :user_private_repository_invitation_reader, scope: "repo:invite"

  role :repository_invitation_accepter, scope: ["public_repo", "repo:invite"] do |context|
    user, invitation = extract(context, :user, :resource)
    repository = invitation.try(:repository)

    next false unless user && invitation && repository && oauth_allows_invitation_access?(user, repository)

    if user.can_have_granular_permissions?
      repository.resources.administration.writable_by?(user)
    else
      invitation.invitee_id == user.id
    end
  end

  role :asset_reader, scope: "user:assets:read" do |context|
    context.asset
  end

  role :asset_writer, scope: "user:assets" do |context|
    if context.asset
      context.user && context.asset.user_id == context.user.id
    else
      context.user
    end
  end

  role :user_file_writer, scope: "user:assets" do |context|
    uploader, file_owner = extract(context, :user, :owner)
    uploader && file_owner && file_owner == uploader
  end

  role :notification_dashboard_reader, scope: %w(notifications repo) do |context|
    extract(context, :user)
  end

  role :notification_reader, scope: %w(notifications repo) do |context|
    user, resource = extract(context, :user, :resource)
    next false unless user && resource

    case resource
    when ::Repository
      resource.pullable_by?(user)
    else
      resource.try(:readable_by?, user)
    end
  end

  role :v4_repo_notification_writer, scope: %w(notifications) do |context|
    user, repo = extract(context, :user, :resource)
    actor = user.try(:installation) || user

    next false if actor.can_have_granular_permissions? # GitHub Apps are not permitted at this time

    user && repo && repo.pullable_by?(user)
  end

  role :v4_user_notification_reader, scope: %w(notifications) do |context|
    viewer, user = extract(context, :user, :resource)
    actor = user.try(:installation) || user

    next false if actor.can_have_granular_permissions? # GitHub Apps are not permitted at this time

    actor.id == viewer.id
  end

  role :notification_thread_reader, scope: %w(notifications) do |context|
    viewer, resource = extract(context, :user, :resource)
    actor = viewer.try(:installation) || viewer

    next false if actor.can_have_granular_permissions? # GitHub Apps are not permitted at this time

    resource.readable_by?(actor)
  end

  role :notification_filter_reader, scope: %w(notifications) do |context|
    user, notification_filter = extract(context, :user, :resource)
    actor = user.try(:installation) || user

    next false if actor.can_have_granular_permissions? # GitHub Apps are not permitted at this time

    user && notification_filter && notification_filter.readable_by?(user)
  end

  role :notification_list_with_thread_count_reader, scope: %w(notifications) do |context|
    viewer, notification_list_with_thread_count = extract(context, :user, :resource)
    actor = viewer.try(:installation) || viewer

    next false if actor.can_have_granular_permissions? # GitHub Apps are not permitted at this time

    notification_list_with_thread_count.readable_by?(actor)
  end

  role :v4_user_notification_writer, scope: %w(notifications) do |context|
    viewer, resource = extract(context, :user, :resource)
    actor = viewer.try(:installation) || viewer

    next false if actor.can_have_granular_permissions? # GitHub Apps are not permitted at this time

    resource.readable_by?(actor)
  end

  role :v4_thread_notification_writer, scope: %w(notifications) do |context|
    user, resource = extract(context, :user, :resource)

    next false if user.can_have_granular_permissions? # GitHub Apps are not permitted at this time

    user && resource && resource.readable_by?(user)
  end

  role :user_notification_settings_reader, scope: %w(user) do |context|
    viewer, user = extract(context, :user, :resource)

    actor = user.try(:installation) || user

    actor.id == viewer.id
  end

  role :user_notification_settings_writer, scope: %w(user) do |context|
    viewer, user = extract(context, :user, :resource)

    actor = user.try(:installation) || user

    actor.id == viewer.id
  end

  role :event_reader do |context|
    current_user, user = extract(context, :user, :resource)
    # Duplicate current behavior. Doesn't make any sense to have repo scope to
    # get user event data
    current_user && user == current_user && scope?(current_user, "repo")

    # This is what it should be I think:
    # current_user && user == current_user && scope?(current_user, 'user')
  end

  # This is for when avatars are viewed on upload and requires
  # the user to have admin rights over the avatar.
  # OAuth is disabled for users since there is no public Avatar API.
  role :personal_avatar_reader do |context|
    avatar, current_user = extract(context, :avatar, :user)
    next false if avatar.nil?
    next false if current_user.nil?
    next true if current_user.site_admin?
    avatar.owner.try(:avatar_editable_by?, current_user)
  end

  role :avatar_reader do |context|
    avatar, owner_type, owner_id = extract(context, :avatar, :owner_type, :owner_id)
    avatar && avatar.owner_type == owner_type && avatar.owner_id == owner_id.to_i
  end

  role :avatar_writer do |context|
    uploader, avatar_owner = extract(context, :user, :owner)
    uploader && avatar_owner && avatar_owner.avatar_editable_by?(uploader)
  end

  role :reaction_writer do |context|
    user, repo, resource = extract(context, :user, :repo, :resource)
    actor = user.try(:installation) || user
    next false unless actor && resource

    if !actor.can_have_granular_permissions? && repo &&
        !oauth_allows_access?(actor, repo)
      next false
    end

    Reaction.actor_can_react_to?(actor, resource, repository: repo)
  end

  role :team_discussion_reaction_writer, scope: %w(write:discussion) do |context|
    user, resource = extract(context, :user, :resource)
    actor = user.try(:installation) || user
    next false unless actor && resource

    Reaction.actor_can_react_to?(actor, resource)
  end

  role :team_discussion_reaction_deleter, scope: %w(write:discussion) do |context|
    user, reaction, resource = extract(context, :user, :reaction, :resource)
    actor = user.try(:installation) || user
    next false unless actor && reaction && resource

    if actor.can_have_granular_permissions?
      # Bots are not allowed to delete reactions as themselves. Hence this
      # branch is only triggered when the other branch will also be triggered
      # later on to check for the perms of the backing user. As a result, all
      # we check for in this branch is write permissions for the Bot.
      resource.viewer_can_update?(actor)
    else
      reaction.user == actor
    end
  end

  role :project_creator do |context|
    user, owner = extract(context, :user, :owner)

    next false if user.nil? || owner.nil?

    if owner.is_a?(Repository)
      next false unless oauth_allows_access?(user, owner)
    elsif owner.instance_of?(Organization)
      # TODO: remove `repo` scope in favor or `write:org`
      # https://github.com/github/github/issues/108838
      next false unless scope?(user, "repo") || scope?(user, "write:org")
    elsif owner.instance_of?(User)
      next false unless scope?(user, "repo")
    end

    owner.projects_writable_by?(user)
  end

  role :project_lister do |context|
    user, owner = extract(context, :user, :owner)

    next false if user.nil? || owner.nil?

    if owner.is_a?(Repository)
      next false unless oauth_allows_access?(user, owner)
    elsif owner.instance_of?(Organization)
      # TODO: remove `repo` scope in favor or `read:org`
      # https://github.com/github/github/issues/108838
      next false unless scope?(user, "repo") || scope?(user, "read:org")
    elsif owner.instance_of?(User)
      next false unless scope?(user, "repo")
    end

    owner.projects_readable_by?(user)
  end

  role :project_reader do |context|
    user, project, owner = extract(context, :user, :resource, :owner)
    owner ||= project.owner

    next false if user.nil? || owner.nil?

    if owner.is_a?(Repository)
      next false unless oauth_allows_access?(user, owner)
    elsif owner.instance_of?(Organization)
      # TODO: remove `repo` scope in favor or `read:org`
      # https://github.com/github/github/issues/108838
      next false unless scope?(user, "repo") || scope?(user, "read:org")
    elsif owner.instance_of?(User)
      next false unless scope?(user, "repo")
    end

    project.readable_by?(user)
  end

  role :project_writer do |context|
    user, project = extract(context, :user, :resource)
    owner = project.owner

    next false if user.nil? || owner.nil?

    if owner.is_a?(Repository)
      next false unless oauth_allows_access?(user, owner)
    elsif owner.instance_of?(Organization)
      # TODO: remove `repo` scope in favor or `write:org`
      # https://github.com/github/github/issues/108838
      next false unless scope?(user, "repo") || scope?(user, "write:org")
    elsif owner.instance_of?(User)
      next false unless scope?(user, "repo")
    end

    project.writable_by?(user)
  end

  role :project_admin do |context|
    user, project = extract(context, :user, :resource)
    owner = project.owner

    next false if user.nil? || owner.nil?

    if owner.is_a?(Repository)
      next false unless oauth_allows_access?(user, owner)
    elsif owner.instance_of?(Organization)
      # TODO: remove `repo` scope in favor or `write:org`
      # https://github.com/github/github/issues/108838
      next false unless scope?(user, "repo") || scope?(user, "write:org")
    elsif owner.instance_of?(User)
      next false unless scope?(user, "repo")
    end

    project.adminable_by?(user)
  end

  role :project_repository_links_write do |context|
    user, project, repo = extract(context, :user, :resource, :current_repo)
    next false if user.nil? || repo.nil?

    # Always check Repository access before Project access!
    next false unless oauth_allows_access?(user, repo)
    next false unless scope?(user, "repo")

    project.writable_by?(user)
  end

  # A user who has access to read check suites
  role :check_suite_reader do |context|
    user, repo = extract(context, :user, :resource)
    next false unless user
    actor = user.try(:installation) || user

    if actor.can_have_granular_permissions?
      repo.resources.checks.readable_by?(actor)
    else
      oauth_allows_access?(user, repo) && repo.resources.contents.readable_by?(actor)
    end
  end

  # A user who has access to write check suites for a repo
  role :check_suite_writer do |context|
    user, repo = extract(context, :user, :resource)
    next false unless user
    actor = user.try(:installation) || user

    if (actor.user? && actor.using_auth_via_integration?) || actor.can_have_granular_permissions?
      repo.resources.checks.writable_by?(actor)
    end
  end

  # A user who has access to request check suites for a repo
  role :check_suite_requester do |context|
    user, repo = extract(context, :user, :resource)
    next false unless user
    next false if user.using_auth_via_oauth_application?

    actor = user.try(:installation) || user

    if actor.can_have_granular_permissions?
      repo.resources.checks.writable_by?(actor)
    else
      oauth_allows_access?(user, repo) && repo.resources.contents.writable_by?(actor)
    end
  end

  # A user who has access to read check runs
  role :check_run_reader do |context|
    user, repo = extract(context, :user, :resource)
    next false unless user
    actor = user.try(:installation) || user

    if actor.can_have_granular_permissions?
      repo.resources.checks.readable_by?(actor)
    else
      oauth_allows_access?(user, repo) && repo.resources.contents.readable_by?(actor)
    end
  end

  # A user who has access to write check runs
  role :check_run_writer do |context|
    user, repo = extract(context, :user, :resource)
    next false unless user
    actor = user.try(:installation) || user

    if (actor.user? && actor.using_auth_via_integration?) || actor.can_have_granular_permissions?
      repo.resources.checks.writable_by?(actor)
    end
  end

  # A user who has access to write check suite permissions for a repo
  role :check_suite_preferences_writer do |context|
    user, repo = extract(context, :user, :resource)
    next false unless user
    next false if user.using_auth_via_oauth_application?

    actor = user.try(:installation) || user

    if actor.can_have_granular_permissions?
      repo.resources.checks.writable_by?(actor)
    else
      oauth_allows_access?(user, repo) && repo.adminable_by?(actor)
    end
  end

  # A user who has read access to the Actions API
  role :actions_reader do |context|
    user, repo = extract(context, :user, :resource)

    next false unless user
    next false unless oauth_allows_access?(user, repo) # This will pass for GitHub App server-to-server and user-to-server no matter what

    if user.can_have_granular_permissions?
      repo.resources.actions.readable_by?(user)
    else
      repo.resources.contents.readable_by?(user)
    end
  end

  # A user who has write access to the Actions API
  role :actions_writer do |context|
    user, repo = extract(context, :user, :resource)

    next false unless user
    next false unless oauth_allows_access?(user, repo) # This will pass for GitHub App server-to-server and user-to-server no matter what

    if user.can_have_granular_permissions?
      repo.resources.actions.writable_by?(user)
    else
      repo.resources.contents.writable_by?(user)
    end
  end

  # A user who has access to write to the Enterprise self-hosted runners API
  role :business_self_hosted_runners_writer, scope: %w(admin:enterprise) do |context|
    user, business = extract(context, :user, :resource)

    next false unless user
    next false if user.can_have_granular_permissions?

    business.adminable_by?(user)
  end

  # A user who has access to write to the Organization self-hosted runners API
  role :org_self_hosted_runners_writer, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)

    next false unless user

    if user.can_have_granular_permissions?
      org.resources.organization_self_hosted_runners.writable_by?(user)
    else
      org.adminable_by?(user)
    end
  end

  # A user who has access to read the Organization self-hosted runners API
  role :org_self_hosted_runners_reader, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)

    next false unless user

    if user.can_have_granular_permissions?
      org.resources.organization_self_hosted_runners.readable_by?(user)
    else
      org.adminable_by?(user)
    end
  end

  # A user who has access to write to the Secrets API
  role :actions_secrets_writer do |context|
    user, repo = extract(context, :user, :resource)

    next false unless user
    next false unless oauth_allows_access?(user, repo) && oauth_application_policy_satisfied?(user, repo)

    if user.can_have_granular_permissions?
      repo.resources.secrets.writable_by?(user)
    else
      repo.resources.contents.writable_by?(user)
    end
  end

  # A user who has access to read the Secrets API
  role :actions_secrets_reader do |context|
    user, repo = extract(context, :user, :resource)

    next false unless user
    next false unless oauth_allows_access?(user, repo) && oauth_application_policy_satisfied?(user, repo)

    if user.can_have_granular_permissions?
      repo.resources.secrets.readable_by?(user)
    else
      # Users with write access may read secrets. This does not give access to values. Metadata only.
      repo.resources.contents.writable_by?(user)
    end
  end

  # A user who has access to write to the Organization secrets API
  role :org_actions_secrets_writer, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)

    next false unless user

    if user.can_have_granular_permissions?
      org.resources.organization_secrets.writable_by?(user)
    else
      org.adminable_by?(user)
    end
  end

  # A user who has access to read the Organization secrets API
  role :org_actions_secrets_reader, scope: %w(admin:org) do |context|
    user, org = extract(context, :user, :resource)

    next false unless user

    if user.can_have_granular_permissions?
      org.resources.organization_secrets.readable_by?(user)
    else
      org.adminable_by?(user)
    end
  end

  # An integrator with access to write content references and attachments
  role :content_reference_integration_writer do |context|
    actor, content_reference = extract(context, :user, :resource)

    # Currently we only support content references on repository level resources
    # this check will need to be updated when this changes
    actor.can_have_granular_permissions? && content_reference.writable_by?(actor)
  end

  # An integrator with access to read content references and attachments
  role :content_reference_integration_reader do |context|
    actor, content_reference = extract(context, :user, :resource)

    # Currently we only support content references on repository level resources
    # this check will need to be updated when this changes
    actor.can_have_granular_permissions? && content_reference.readable_by?(actor)
  end

  # Integration-centric roles

  # An actor with privilege to read metadata about the given integration
  # installation
  role :integration_installation_self do |context|
    user, installation = extract(context, :user, :installation)
    user.respond_to?(:installation) && user.installation == installation
  end

  # A user that can modify a Marketplace::Listing and its images.
  role :marketplace_listing_writer do |context|
    user, listing = extract(context, :user, :marketplace_listing)
    user && (listing.adminable_by?(user) || user.can_admin_marketplace_listings?)
  end

  # A user that can modify a SponsorsListing.
  role :sponsors_listing_writer do |context|
    user, listing = extract(context, :user, :sponsors_listing)

    if listing.sponsorable.organization?
      next false unless scope?(user, "admin:org")
    end

    user && (listing.adminable_by?(user) || user.can_admin_sponsors_listings?)
  end

  # A user that can read their installations via an Integration
  role :user_installations_reader do |context|
    user = extract(context, :user)

    next false unless user
    user.using_auth_via_integration?
  end

  # A user that can list repositories on an installation that the user can access
  role :user_installation_repos_lister do |context|
    user, installation = extract(context, :user, :installation)

    next false unless user
    allowed_auth = user.using_basic_auth? ||
      user.using_personal_access_token? && (
        installation.target.organization? && scope?(user, "read:org") ||
        installation.target.user? && scope?(user, "read:user")
      ) ||
      user.using_auth_via_integration?

    allowed_auth && user.can_access_installation?(installation)
  end

  # A user that can add/remove the given repository to/from the installation
  role :user_repo_installation_writer do |context|
    user, installation, repository = extract(context, :user, :installation, :resource)

    next false unless user
    allowed_auth = user.using_basic_auth? ||
      user.using_personal_access_token? && scope?(user, "repo")

    next false unless allowed_auth
    target = repository.owner
    next false unless target == installation.target

    installation.integration.installable_on_by?(target: target, actor: user, repositories: [repository])
  end

  role :github_app_reader do |context|
    user, app = extract(context, :user, :resource)

    next true if app.public?
    next false unless user

    if (installation = user.try(:installation))
      # Installations are allowed to read themselves:
      next true if installation.integration_id == app.id
    else
      owner = app.owner
      next scope?(user, "read:user") if owner.id == user.id
      next scope?(user, "read:org")  if owner.is_a?(Organization) && owner.resources.members.readable_by?(user)
    end
  end

  # Apps are allowed to write their own credentials for Launch.
  role :launch_app do |context|
    app, user = extract(context, :app, :user)
    app && user.try(:integration) == app
  end

  role :presence_participant do |context|
    user, repo = extract(context, :user, :resource)
    actor = user.try(:installation) || user

    next false unless repo.resources.contents.writable_by?(actor)
    oauth_allows_access?(user, repo)
  end

  role :composable_comment_creator do |context|
    user, repo = extract(context, :user, :repo)

    repo.resources.composable_comments.writable_by?(user)
  end

  role :composable_comment_updater do |context|
    user, composable_comment = extract(context, :user, :resource)

    composable_comment.integration == user.integration
  end

  role :ephemeral_notice_accessor do |context|
    user, resource = extract(context, :user, :resource)

    # Check the integration that made the resource is the actor
    actor = user.installation
    resource.integration == actor.integration
  end

  # This check allows access to asset uploading endpoints only to clients using
  # the GitHub Mobile OAuth ID. Since the Mobile OAuth client secret is distributed
  # within the client applications, any consumer can extract this secret and make
  # requests identifying as this OAuth app to bypass this restriction. Therefore,
  # this check is to prevent casual public access to this API and should not be used
  # to enforce any sort of trustworthy identification of the GitHub Mobile apps.
  role :github_mobile_apps_upload_asset, scope: %w(user repo user:assets) do |context|
    user = extract(context, :user)

    # GitHub mobile apps are all oauth apps at this point
    next false unless user.using_auth_via_oauth_application?

    # Ensure they are specific oauth apps
    app = user.oauth_access.application
    # TODO: Don't assume that these Apps will exist, be defensive here.
    android_app_id = Apps::Internal.oauth_application(:android_mobile).id
    ios_app_id = Apps::Internal.oauth_application(:ios_mobile).id
    (app.id == ios_app_id || app.id == android_app_id)
  end

  # A user who has write access to the codespace.
  role :codespace_writer do |context|
    user, codespace = extract(context, :user, :resource)

    next false unless is_using_codespaces_app?(user)
    next false if user.using_auth_via_oauth_application? && !scope?(user, "repo")

    if user.using_auth_via_oauth_application? || user.using_auth_via_integration?
      codespace.writable_by?(user)
    else
      user.can_have_granular_permissions?
    end
  end

  # A user who has access to read a user's codespaces.
  role :codespace_reader do |context|
    user, codespace = extract(context, :user, :resource)

    next false unless is_using_codespaces_app?(user)
    next false if user.using_auth_via_oauth_application? && !scope?(user, "repo")

    if user.using_auth_via_oauth_application? || user.using_auth_via_integration?
      codespace.readable_by?(user)
    else
      user.can_have_granular_permissions?
    end
  end

  # A user who has access to list the user's codespaces.
  role :codespace_lister do |context|
    user, owner = extract(context, :user, :resource)

    next false unless is_using_codespaces_app?(user)
    next false if user.using_auth_via_oauth_application? && !scope?(user, "repo")

    if user.using_auth_via_oauth_application? || user.using_auth_via_integration?
      user == owner
    else
      user.can_have_granular_permissions?
    end
  end

  def self.is_using_codespaces_app?(user)
    app =
      if user.can_have_granular_permissions?
        user.integration
      elsif user.using_auth_via_integration?
        user.oauth_access.application
      elsif user.using_auth_via_oauth_application?
        user.oauth_application
      end

    ::Apps::Internal.capable?(:access_codespaces, app: app)
  end

  # Request made by the Slack integration or Pull Panda bot user for organization with both Slack and Pull Panda installed
  role :first_party_slack_integrations do |context|
    user, organization = extract(context, :user, :resource)
    integration = user.installation&.integration
    slack_installed = IntegrationInstallation.with_target(organization).where(integration: ::Apps::Internal.integration(:slack)).exists?
    pull_panda_installed = IntegrationInstallation.with_target(organization).where(integration: ::Apps::Internal.integration(:pull_panda)).exists?

    next false unless user&.bot?
    next false unless slack_installed && pull_panda_installed
    next false unless integration == ::Apps::Internal.integration(:slack) || integration == ::Apps::Internal.integration(:pull_panda)

    true
  end

  role :security_events_reader, scope: "security_events" do |context|
    user, repository = extract(context, :user, :resource)
    next false unless user
    next false unless repository&.code_scanning_enabled?

    actor = user.try(:installation) || user
    oauth_allows_access?(user, repository) && repository.resources.security_events.readable_by?(actor)
  end

  role :security_events_writer, scope: "security_events" do |context|
    user, repository = extract(context, :user, :resource)
    next false unless user
    next false unless repository&.code_scanning_enabled?
    actor = user.try(:installation) || user

    repository.code_scanning_upload_endpoint_available? &&
      oauth_allows_access?(user, repository) &&
      repository.resources.security_events.writable_by?(actor)
  end

  # A user that can read discussions in a repo
  role :discussion_reader, scope: "repo" do |context|
    user, repo = extract(context, :user, :repo)
    repo ||= extract(context, :resource)

    user && oauth_allows_access?(user, repo) && repo.resources.discussions.readable_by?(user)
  end

  # A user that can create or update all discussions in a repo
  role :discussion_writer do |context|
    user, repo = extract(context, :user, :repo)

    user && oauth_allows_access?(user, repo) && repo.resources.writable_by?(user)
  end

  # A user that can read comments on discussions
  role :discussion_comment_reader do |context|
    user, resource, repo = extract(context, :user, :resource, :repo)

    user &&
      oauth_allows_access?(user, repo) &&
      repo.resources.discussions.readable_by?(user) &&
      resource &&
      resource.readable_by?(user)
  end
end
