# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :enterprise do |access|
    access.allow :site_admin
  end
end
