# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :administer_business do |access|
    access.ensure_context :resource
    access.allow :business_admin
  end

  define_access :manage_business_billing do |access|
    access.ensure_context :resource
    access.allow :business_admin
    access.allow :business_billing_manager
  end

  define_access :cancel_business_organization_invitation do |access|
    access.ensure_context :resource
    access.allow :business_organization_invitation_canceler
  end

  define_access :view_business_profile do |access|
    access.ensure_context :resource
    access.allow :business_profile_viewer
  end

  define_access :view_business_member_invitation do |access|
    access.ensure_context :resource
    access.allow :business_member_invitation_viewer
  end

  define_access :read_business_audit_log do |access|
    access.allow :business_admin
  end

  define_access :read_business_and_org_audit_log do |access|
    access.allow :business_admin
    access.allow :org_business_admin
  end

  define_access :write_business_enterprise_installation_user_accounts do |access|
    access.ensure_context :resource
    access.allow :business_admin
  end

  define_access :enterprise_scim_writer do |access|
    access.ensure_context :resource
    access.allow :business_admin
    access.allow :business_user_provisioner if GitHub.enterprise?
  end

  define_access :view_enterprise_external_identities do |access|
    access.ensure_context :resource
    access.allow :business_admin
  end
end
