# frozen_string_literal: true

class Api::AccessControl < Egress::AccessControl
  define_access :enterprise_installation_create do |access|
    access.allow :everyone
  end

  define_access :enterprise_installation do |access|
    access.allow :everyone
  end

  define_access :external_contributions do |access|
    access.ensure_context :user
    access.allow :external_contributions_writer
  end

  define_access :enterprise_vulnerabilities do |access|
    access.ensure_context :user
    access.allow :enterprise_vulnerabilities_reader
  end
end
