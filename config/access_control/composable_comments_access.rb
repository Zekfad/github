# frozen_string_literal: true

# Wherever possible, we delegate to existing permissions here like
# :issue_writer and :pull_request_writer
# If an app already has permissions to create issue comments
# on issues or pull requests, then it also has permission
# to create composable comments.

class Api::AccessControl < Egress::AccessControl
  define_access :create_composable_comment do |access|
    access.ensure_context :user, :resource, :repo

    resource_must_belong_to_repo(access)

    access.allow(:issue_writer) do |context|
      user = extract(context, :user)
      actor = user.try(:installation) || user

      next false unless actor.can_have_granular_permissions?
      issue_must_match_permission(context, :write)
    end

    access.allow(:pull_request_writer) do |context|
      user = extract(context, :user)
      actor = user.try(:installation) || user

      next false unless actor.can_have_granular_permissions?
      issue_must_match_permission(context, :write)
    end

    access.allow(:composable_comment_creator) do |context|
      user = extract(context, :user)

      user.can_have_granular_permissions?
    end
  end

  define_access :update_composable_comment, :delete_composable_comment do |access|
    access.ensure_context :user, :resource

    access.allow(:composable_comment_updater) do |context|
      user = extract(context, :user)
      actor = user.try(:installation) || user

      actor.can_have_granular_permissions?
    end
  end
end
