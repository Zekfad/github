# rubocop:disable Style/FrozenStringLiteralComment

require "socket"

$stderr.puts "Loading config/console.rb"
$console = true # so we know we're in a console (for guards)

GitHub.component = :console
# Load license so it won't be lazy loaded later and cause issues
GitHub::Enterprise.license if GitHub.enterprise? && !Rails.development?

def enable_query_logging
  ActiveRecord::Base.logger = Logger.new(STDOUT)
end

# Ability debugging
require File.expand_path(File.join(File.dirname(__FILE__), "console/ability_debug"))

# Bump timeout up from 20 second default.
GitRPC.timeout = 30.minutes.to_i

# shortcut to Repository.nwo.
def with_name_with_owner(name_with_owner)
  GitHub::Resources.with_name_with_owner(name_with_owner)
end

alias :nwo :with_name_with_owner

# user  = the 'defunkt'
# user  = the 'chris@github.com'
# team  = the 'github/brunch'
# team  = the '@github/brunch'
# repo  = the 'defunkt/dotjs'
# pr    = the 'github/gist/pull/123'
# issue = the 'github/gist/issues/123'
# issue = the 'github/gist#123'
# disc  = the 'github/gist#345'
# entry = the 'github/github/blob/master/Blakefile'
def find_by_uri(str)  # rubocop:disable GitHub/FindByDef
  GitHub::Resources.find_by_uri(str)
end

alias :find_by_url :find_by_uri
alias :the  :find_by_uri
alias :that :find_by_uri
alias :yon  :find_by_uri
alias :such :find_by_uri
alias :dat  :find_by_uri

# Allows you to look up and fetch an object based on its global relay id.
# Default user is Hubot.
def find_by_global_relay_id(id, user = User.find_by_login("hubot"))  # rubocop:disable GitHub/FindByDef
  Platform::Security::RepositoryAccess.with_viewer(user) do
    permission = Platform::Authorization::Permission.new(viewer: user)

    Platform::Helpers::NodeIdentification.untyped_object_from_id(id, permission: permission).sync
  end
rescue Platform::Errors
  puts "Something went wrong. Sorry!"
end

def browse(model)
  GitHub::Resources.url_for_model(model).tap do |url|
    POSIX::Spawn::Child.new("open", url) if Rails.env.development?
  end
end

# >> time { some_slow_method }
#     user     system      total        real
# 0.960000   0.020000   0.980000 (  0.990485)
def time(times = 1)
  require "benchmark"
  ret = nil
  Benchmark.bm { |x| x.report { times.times { ret = yield } } }
  ret
end

# Because tekkub can't type long words
Repo = Repository

# Is this username or IP blacklisted as spam?
def spam?(v)
  GitHub.kv.get("spam.blacklisted_ip.#{v}").value! || GitHub.cache.get("blacklisted_login:#{v.downcase}", true)
end

# Un-spam-flag someone
def despam(v)
  GitHub.kv.del("spam.blacklisted_ip.#{v}")
  GitHub.cache.delete("blacklisted_login:#{v.downcase}")
end

# Removes fraud accounts based on an IP.  Creates a gist full of external
# transaction IDs that need to be refunded
def purge_fraud(ip)
  us = User.where last_ip: ip
  us.each do |u|
    u.billing_transactions.sales.select(&:refundable?).map do |t|
      GitHub::Billing.refund_transaction t.transaction_id
    end
    u.plan = "free"
    u.save
    u.suspend("Fraudster")
  end
  "Suspended #{us.size} accounts"
end

def generate_prepaid_codes(plan, num)
  codes = []

  num.times do
    c = Coupon.new
    c.plan = plan
    c.discount = 1
    c.duration = 6 * 30 # 6 months
    c.group = "prepaid"
    c.note = "Prepaid plan"
    c.save!

    codes << c.code
  end

  puts codes.join(" ")
end

def verbose_log!
  Object.const_set(:RAILS_DEFAULT_LOGGER, Logger.new($stdout))
  ActiveRecord::Base.logger = RAILS_DEFAULT_LOGGER
end

# Be extremely verbose when running callbacks. Every callback ever run will be
# printed to $stderr.
def verbose_callbacks!
  ActiveSupport::Callbacks::CallbackChain.class_eval do
    def run(object, options = {}, &terminator)
      enumerator = options[:enumerator] || :each

      unless block_given?
        send(enumerator) do |callback|
          $stderr.puts callback.method.to_s
          callback.call(object)
        end
      else
        send(enumerator) do |callback|
          puts callback.method.to_s
          result = callback.call(object)
          break result if terminator.call(result, object)
        end
      end
    end
  end
end

# Find the poorly cased OauthAccess by token
def token(toke)
  OauthAccess.find_by_token(toke)
end

# bump an OauthApplication's rate_limit
def bump_rate(key, limit = 12500, temporary = false)
  if oauth = OauthApplication.find_by_key(key)
    if temporary
      oauth.set_temporary_rate_limit(limit)
    else
      oauth.rate_limit = limit
    end
    oauth.save
  end
end

# Shortcut to run queries and blocks of code against the readonly database.
def rodb(&block)
  ActiveRecord::Base.connected_to(role: :reading) { yield }
end

# Formerly, this was a shortcut to use the throttler.  Now it's a
# reminder of the correct way to throttle (introduced in
# https://github.com/github/github/pull/113502)
def throttle(&block)
  fail "Please use model-specific throttles, i.e. `User.throttle { users.update_all(...) }`"
end

# custom marginalia config for the console
# see config/initializers/marginalia

Marginalia::Comment.update!(OpenStruct.new(request_category: "console"))

def trace_chargeback(txn_id)
  if (txn = Billing::BillingTransaction.find_by_transaction_id txn_id)
    puts txn[:user_login]

    if (user = User.find txn[:user_id])
      puts "User disabled: #{user.disabled?}"
    else
      puts "User no longer exists"
    end
  else
    puts "No transaction found"
  end
end

# Set the current GitHub context actor as the currently logged-in user. This
# works with gh-console, which uses sudo from a personal account.
if File.exist?("/etc/github_users") # we're running in production!
  github_users = Hash[File.readlines("/etc/github_users").map { |u| u.strip.split(":") }]
  login = ENV["SUDO_USER"]

  if login && (actor = the(github_users[login]))
    GitHub.context.push actor: actor
    Audit.context.push actor: actor
  else
    warn "could not determine current user for audit logging!"
  end
end

# And if we're in a console, let's say so and also say where from:
GitHub.context.push console_host: Socket.gethostname
Audit.context.push console_host: Socket.gethostname
