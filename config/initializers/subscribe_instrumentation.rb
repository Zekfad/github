# frozen_string_literal: true

require "github/config/notifications"
require "audit"

# A whitelist for events to audit via instrumentation
sub = Audit.subscribe GitHub.instrumentation_service, Audit::ACTIONS

if Rails.test?
  def sub.logger=(value)
    @logger = value
  end
  sub.logger = Logger.new(STDOUT)
end

unless Rails.test?
  require "instrumentation_subscribers/user_asset"
  InstrumentationSubscribers::UserAsset.attach!

  require "instrumentation_subscribers/hook"
  InstrumentationSubscribers::Hook.attach!

  require "instrumentation_subscribers/release_asset"
  InstrumentationSubscribers::ReleaseAsset.attach!
end
