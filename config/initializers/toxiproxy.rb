# rubocop:disable Style/FrozenStringLiteralComment

if Rails.env.development?
  require "toxiproxy"

  # Automatically setup toxiproxy proxies for all database.yml entries with
  # resilient_properties config.
  #
  # resilient_properties defines the circuit breaker configuration, which tells
  # the active record adapter to use a Resilient::Trilogy instance, and provides a
  # name which is used to create the toxiproxy proxy.
  #
  # For resilient, add the following config to the corresponding database.yml entry:
  #   port: 14000 (must be unique)
  #   resilient_properties:
  #     name: github_development_mysql_notifications_primary
  #     error_threshold_percentage: 50
  #     request_volume_threshold: 0
  #     window_size_in_seconds: 60
  #     bucket_size_in_seconds: 10
  #     sleep_window_seconds: 10
  #
  # port needs to exist in database.yml for both resilient as toxiproxy needs to
  # know where to point the proxy to. It simply needs to be set to a port that
  # is available for toxiproxy to bind to.
  #
  # name should be github_#{rails env}_mysql_#{feature}_#{purpose}
  #   rails env: (ie: development, test...)
  #   feature: (ie: notifications, stratocaster...)
  #   purpose: (ie: primary, secondary, shard1...)
  #
  # name must be unique across all toxiproxy proxies.
  #
  # You can see the currently configured proxies by running:
  #   curl -i localhost:8474/proxies
  #
  # You can disable a proxy like so:
  #   curl -i -d '{"enabled":false}' localhost:8474/proxies/github_development_mysql_notifications_primary
  #
  # You can enable a proxy like so:
  #   curl -i -d '{"enabled":true}' localhost:8474/proxies/github_development_mysql_notifications_primary
  if Toxiproxy.running?
    resilient_configurations = ActiveRecord::Base.configurations.configs_for.select { |db_config|
      db_config.config.key?("resilient_properties")
    }

    resilient_configurations.each do |db_config|
      config = db_config.config

      Toxiproxy.populate({
        name: config.fetch("resilient_properties").fetch("name"),
        listen: "localhost:#{config.fetch("port")}",
        upstream: "localhost:#{GitHub.toxiproxy_upstream_mysql_port}",
      })
    end
  end
end
