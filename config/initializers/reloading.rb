# frozen_string_literal: true

Rails.configuration.to_prepare do
  # Reload Notification service classes in dev mode
  GitHub.newsies = nil
  GitHub::FeatureFlag.team_cache.clear

  if !Rails.application.config.cache_classes && Object.const_defined?(:Views)
    Object.send(:remove_const, :Views)
  end
end
