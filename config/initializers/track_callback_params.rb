# frozen_string_literal: true

if Rails.test?
  # monkeypatch in tracking for :if and :unless options on *_action
  module AbstractController
    module Callbacks
      module ClassMethods
        def _normalize_callback_option(options, from, to) # :nodoc:
          if from = options[from]
            from_option = Array(from).map(&:to_s).to_set
            from = proc { |c| from_option.include? c.action_name }
            from.instance_variable_set(:@_from, from_option)
            options[to] = Array(options[to]).unshift(from)
          end
        end
      end
    end
  end
end
