# frozen_string_literal: true

# Annotate HTML with template file names in development.
# Rails 6.1 will include this functionality, so this is
# gated to not run in 6.1.
#
# Rails PR: https://github.com/rails/rails/pull/38848
if Rails.env.development? && GitHub.rails_6_0?
  class CustomHandler < ActionView::Template::Handlers::ERB
    def call(template, source)
      if template.format == :html
        out = super.gsub("@output_buffer.to_s", "")

        "@output_buffer.safe_append='<!-- BEGIN: #{ template.short_identifier } -->\n'.freeze;" +
          out +
          "@output_buffer.safe_append='<!-- END: #{ template.short_identifier } -->\n'.freeze;@output_buffer.to_s;"
      else
        super
      end
    end
  end

  ActionView::Template.register_template_handler :erb, CustomHandler.new
end
