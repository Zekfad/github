# frozen_string_literal: true
require "darrrr"

Darrrr.cache = GitHub.cache # for caching remote configs
Darrrr.authority = "#{GitHub.scheme}://#{GitHub.host_name}"
Darrrr.privacy_policy = "#{GitHub.help_url}/articles/github-privacy-statement/"
Darrrr.icon_152px = "#{Darrrr.authority}/images/modules/logos_page/GitHub-Mark.png"

Darrrr::AccountProvider.configure do |config|
  config.signing_private_key = GitHub.account_provider_private_key
  config.symmetric_key = GitHub.account_provider_symmetric_key

  config.tokensign_pubkeys_secp256r1 = lambda do |context|
    key_versions = GitHub.cache.fetch("delegated-account-recovery-public-keys", ttl: DelegatedAccountRecoveryController::CONFIG_CACHE_TIME) do
      GitHub::DarrrrEarthsmokeEncryptor.low_level_key.export.key_versions.map(&:to_h)
    end

    if context&.any?
      if context[:key_version_id] == DelegatedRecoveryToken::LEGACY_KEY_VERSION_ID
        [GitHub.legacy_account_provider_public_key]
      else
        if specific_key = key_versions.find { |key| key[:id] == context[:key_version_id] }
          [GitHub::DarrrrEarthsmokeEncryptor.key_version_to_pubkey(specific_key)].compact
        else
          []
        end
      end
    else
      publishable_keys = [GitHub.legacy_account_provider_public_key]

      if newest_key = key_versions.first
        publishable_keys.unshift(GitHub::DarrrrEarthsmokeEncryptor.key_version_to_pubkey(newest_key))

        if newest_key[:staged]
          activated_key = key_versions.find { |key| !key[:staged] }
          raise StandardError, "No activated key found" unless activated_key
          publishable_keys << GitHub::DarrrrEarthsmokeEncryptor.key_version_to_pubkey(activated_key)
        end
      end

      publishable_keys
    end
  end

  config.save_token_return = "#{Darrrr.authority}/settings/security/delegated-account-recovery/save-token-return"
  config.recover_account_return = "#{Darrrr.authority}/settings/security/delegated-account-recovery/recover-account-return"
end

if GitHub.github_as_recovery_provider_enabled?
  Darrrr::RecoveryProvider.configure do |config|
    config.signing_private_key = GitHub.recovery_provider_private_key
    config.countersign_pubkeys_secp256r1 = [GitHub.recovery_provider_public_key]
    config.token_max_size = 8192
    config.save_token = "#{Darrrr.authority}/settings/security/recovery-provider/save-token"
    config.recover_account = "#{Darrrr.authority}/settings/security/recovery-provider/recover-account"
  end
else
  Darrrr.register_recovery_provider("https://www.facebook.com")
end

if Rails.env.development?
  # All URLs are https in test
  Darrrr.allow_unsafe_urls = true

  # Allow yourself to act as an account provider
  Darrrr.register_recovery_provider("#{GitHub.scheme}://#{GitHub.host_name}")
  Darrrr.register_account_provider("#{GitHub.scheme}://#{GitHub.host_name}")

  # If testing with oreoshake/delegated-account-recovery-locker, uncomment this line to enable the provider
  # Darrrr.register_recovery_provider("http://localhost:9292")
  # Darrrr.register_account_provider("http://localhost:9292")
end

Darrrr.this_account_provider.custom_encryptor = GitHub::DarrrrEarthsmokeEncryptor
Darrrr.faraday_config_callback = lambda do |connection|
  connection.headers["Connection"] = ""
  connection.adapter(Faraday.default_adapter)
end
