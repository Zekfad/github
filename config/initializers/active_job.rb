# frozen_string_literal: true

require "serializers/stratocaster_event"
Rails.application.config.active_job.custom_serializers += [
  Serializers::StratocasterEvent,
]

module ActiveJob
  module QueueAdapters
    class TestAdapter
      def locked?(job)
        now = Time.now.to_i
        timeout = now + job.lock_timeout + 1
        lock_set = lock_set(job)
        lock_key = job.lock_key

        if job_locks[lock_set][lock_key].nil?
          job_locks[lock_set][lock_key] = timeout
          return false
        end

        old = job_locks[lock_set][lock_key]
        job_locks[lock_set][lock_key] = timeout
        now <= old
      end

      def clear_lock(job)
        job_locks[lock_set(job)].delete(job.lock_key)
      end

      def clear_all_locks(job_class = nil)
        if job_class
          job_locks[job_class.to_s].clear
        else
          job_locks.clear
        end
      end

      def job_locks
        @job_locks ||= Hash.new { |h, k| h[k] = {} }
      end

      def lock_set(job)
        job.class.name
      end
    end
  end
end
