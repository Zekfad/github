# frozen_string_literal: true

require "bert"

BERT::Encode.version = :v3
