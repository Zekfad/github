# rubocop:disable Style/FrozenStringLiteralComment

GitHub::Application.configure do
  config.session_store :cookie_store,
    httponly: true,
    secure:   Rails.env == "test" ? false : GitHub.ssl?,
    key:      GitHub.session_key
end
