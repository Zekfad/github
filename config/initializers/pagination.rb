# frozen_string_literal: true

WillPaginate::ViewHelpers.pagination_options.update inner_window: 2
