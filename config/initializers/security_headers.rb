# frozen_string_literal: true
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#                       Seriously, CC @github/prodsec and @github/dotcom-security
#                       if you need to touch this file
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
SecureHeaders::Configuration.default do |config|
  config.cookies = {
    httponly: { except: %w(_octo) },
    secure: GitHub.ssl? || SecureHeaders::OPT_OUT,
    samesite: {
      lax: { except: %w(saml_csrf_token saml_return_to saml_csrf_token_legacy saml_return_to_legacy) },
      none: { only: %w(saml_csrf_token saml_return_to) }
    }
  }

  # Attempts to tell abobe products to ignore any crossdomain.xml files.
  # https://www.adobe.com/devnet-docs/acrobatetk/tools/AppSec/xdomain.html
  # Previously unapplied, we can enable later
  config.x_permitted_cross_domain_policies = SecureHeaders::OPT_OUT

  # Prevent file downloads from opening
  # http://msdn.microsoft.com/en-us/library/ie/jj542450(v=vs.85).aspx
  # Previously unapplied, we can enable later
  config.x_download_options = SecureHeaders::OPT_OUT

  # Sets the x-xss-protection header to enable client-side XSS protection.
  # X-Xss-Protection: 1; mode=block
  config.x_xss_protection = SecureHeaders::XXssProtection::DEFAULT_VALUE

  # Sets the x-content-type-options header to prevent content sniffing.
  # X-Content-Type-Options: nosniff
  config.x_content_type_options = SecureHeaders::XContentTypeOptions::DEFAULT_VALUE

  # Sets the strict transport security header to force use of SSL for all
  # connections.
  max_age = Rails.development? ? 1.minute : 365.days
  hsts_config = "max-age=#{max_age}; includeSubdomains".dup
  # The 'preload' value requests that the domain be added to Chrome's header
  # file of preloaded HSTS sites. This could be unexpected or unwanted on
  # Enterprise installs.
  hsts_config << "; preload" unless GitHub.enterprise?
  config.hsts = hsts_config.freeze

  # Sets the x-frames-options header to prevent clickjacking.
  config.x_frame_options = SecureHeaders::XFrameOptions::DENY

  # Sets the Content-Security-Policy header. Determines what a browser is
  # allowed to do.
  config.csp = {
    # Because safari doesn't follow the spec WRT schemeless sources,
    # we can't strip the schemes for non-SSL instances.
    preserve_schemes: !GitHub.ssl?,

    default_src: GitHub::CSP::Policy::DEFAULT_SOURCES,
    script_src: GitHub::CSP::Policy::SCRIPT_SOURCES,
    worker_src: GitHub::CSP::Policy::WORKER_SOURCES,
    object_src: GitHub::CSP::Policy::OBJECT_SOURCES,
    style_src: GitHub::CSP::Policy::STYLE_SOURCES,
    img_src: GitHub::CSP::Policy::IMG_SOURCES,
    media_src: GitHub::CSP::Policy::MEDIA_SOURCES,
    frame_src: GitHub::CSP::Policy::FRAME_SOURCES,
    # Should enforce the same policy as `X-FRAME-OPTIONS`
    frame_ancestors: GitHub::CSP::Policy::FRAME_ANCESTORS,
    font_src: GitHub::CSP::Policy::FONT_SOURCES,
    connect_src: GitHub::CSP::Policy::CONNECT_SOURCES,
    base_uri: GitHub::CSP::Policy::BASE_URI_SOURCES,
    form_action: GitHub::CSP::Policy::FORM_ACTIONS,
    plugin_types: GitHub::CSP::Policy::PLUGIN_TYPES,
    manifest_src: GitHub::CSP::Policy::MANIFEST_SOURCES,
    block_all_mixed_content: true,
  }.freeze

  if GitHub.certificate_transparency_enabled?
    config.expect_certificate_transparency = {
      enforce: false,
      max_age: 30.days,
      report_uri: GitHub.browser_errors_url,
    }.freeze
  else
    config.expect_certificate_transparency = SecureHeaders::OPT_OUT
  end

  config.referrer_policy = %w(
    origin-when-cross-origin
    strict-origin-when-cross-origin
  ).freeze
end
