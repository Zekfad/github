# rubocop:disable Style/FrozenStringLiteralComment

require "date"

class Date
  ## accept 1.8 marshaled data on 1.9
  ## based on ruby/1.8/date.rb Date._load
  class ::FakeRational
    def to_r
      Rational(@numerator, @denominator)
    end
  end
  def self._load(str)
    ajd, of, sg = Marshal.load(str.gsub("\rRational", "\x11FakeRational"))
    ajd = ajd.to_r if ajd.is_a?(FakeRational)
    of  = of.to_r  if  of.is_a?(FakeRational)
    new.marshal_load([ajd, of, sg])
  end
end
