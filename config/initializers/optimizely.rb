# frozen_string_literal: true

# must run after instrumentation initializer
# implicitly in the correct order because "instrumentation" is alphabetically
# before "optimizely", so this after_initialize block is registered after
# the instrumentation after_initialize block.
GitHub::Application.config.after_initialize do
  GitHub.optimizely
end
