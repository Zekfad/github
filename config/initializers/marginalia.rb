# rubocop:disable Style/FrozenStringLiteralComment

require "marginalia"
Marginalia.application_name = "github"
module Marginalia::Comment

  def self.category
    self.marginalia_controller.request_category if self.marginalia_controller.respond_to?(:request_category)
  end

  def self.route
    self.marginalia_controller.marginalia_route if self.marginalia_controller.respond_to?(:marginalia_route)
  end

  def self.request_id
    self.marginalia_controller.env["HTTP_X_GITHUB_REQUEST_ID"] if self.marginalia_controller.respond_to?(:env)
  end

  def self.deployed_to
    GitHub.deployed_to
  end

  def self.server
    ENV.fetch("KUBE_NODE_HOSTNAME", Failbot.hostname)
  end
end

# we don't use #insert because it calls #insert_into_active_record, which
# attempts to access the DB earlier than expected.  Busts an environment
# test: https://github.com/github/github/commit/a2ef079a8c787cb8563b553148eee8d5ea4c4ecc
#
Marginalia::Railtie.insert_into_action_controller
Marginalia::Railtie.insert_into_active_job

# Hard codes the Marginalia setup because we know what adapter we're using.
# would love to find another way to inspect this without lazily accessing the DB.
ActiveRecord::ConnectionAdapters::TrilogyAdapter.module_eval do
  include Marginalia::ActiveRecordInstrumentation
end

components = [:application, :category, :route, :request_id, :deployed_to, :server, :job]

# Enable line information in comments in development if asked.
if (Rails.development? || Rails.test?) && ENV["GH_MARGINALIA_LINES"]
  Marginalia::Comment.lines_to_ignore = /\.rvm|gem|vendor|marginalia|rbenv|slow_query_logger|active_record_enumerable_protection/
  components << :line
end

Marginalia::Comment.components = components
