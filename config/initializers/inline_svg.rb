# frozen_string_literal: true

module InlineSvg
  class FilesystemInlineSvgLoader
    attr_reader :assets, :filters, :paths

    def initialize(paths: [], filters: [])
      @paths = Array(paths).compact.map { |p| Pathname.new(p) }
      @filters = Array(filters).map { |f| Regexp.new(f) }
      @files = find_files unless GitHub.lazy_find_inline_svg_files?
    end

    def reload!
      @files = nil
    end

    def named(asset_name)
      @files ||= find_files

      file = @files.detect { |file| file.include?(asset_name) }
      if file
        File.read(file)
      else
        raise InlineSvg::AssetFile::FileNotFound.new("Asset not found: #{asset_name}")
      end
    end

    private

    def find_files
      @paths.map { |path| find_assets(path) }
        .inject([], :+)
        .sort_by(&:length)
        .reverse
    end

    def find_assets(path)
      acc = []
      path.each_child do |child|
        if child.directory?
          acc.concat(find_assets(child))
        elsif child.readable_real?
          acc << child.to_s if matches_all_filters?(child.to_s)
        end
      end
      acc
    end

    def matches_all_filters?(path)
      filters.all? { |f| f.match?(path) }
    end
  end
end

InlineSvg.configure do |config|
  dirs = [
    "#{Rails.root}/public/static/images/modules",
    "#{Rails.root}/public/static/images/icons",
  ]

  # Cache SVG paths at boot time for performance
  svg_loader = InlineSvg::FilesystemInlineSvgLoader.new(
    paths: dirs,
    filters: /\.svg/,
  )

  if Rails.development? && !ENV["FASTDEV"] && !ENV["PRELOAD"]
    # Allow SVG assets to be dynamically reloaded in development mode:
    reloader = ActiveSupport::EventedFileUpdateChecker.new([], dirs.index_with("svg")) do
      svg_loader.reload!
    end
    ActionDispatch::Callbacks.before do
      reloader.execute_if_updated
    end
  end

  config.asset_file = svg_loader
end
