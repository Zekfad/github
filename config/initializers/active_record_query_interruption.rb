# frozen_string_literal: true

require "trilogy"
require "active_record/connection_adapters/abstract_mysql_adapter"

# Patches for database adapters and instrumentation
module ActiveRecord
  module QueryInterruption
  end

  class ::Trilogy::MysqlError
    include ::ActiveRecord::QueryInterruption

    def mysql_timeout?
      error_code == 1317 || error_code == 1028 || error_code == 3024
    end
  end

  class QueryCanceled
    include QueryInterruption

    def mysql_timeout?
      true
    end
  end

  class StatementTimeout
    include QueryInterruption

    def mysql_timeout?
      true
    end
  end
end
