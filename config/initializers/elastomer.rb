# rubocop:disable Style/FrozenStringLiteralComment

require "elastomer"

postfix = "test" if Rails.test?
Elastomer.setup(postfix: postfix)

class Elastomer::Client::TimeoutError
  def failbot_context
    { app: "github-timeout" }
  end
end

module ElastomerWithEnabledCheck
  # This error is raised if search is disabled (access mode is "ignore") as
  # configured by the environment. It subclasses Client::Error so the existing
  # error handling code will handle it like any other client error.
  SearchDisabled = Class.new Elastomer::Client::Error

  def request(*args)
    if GitHub.elasticsearch_access_raises?
      raise GitHub::Config::UnexpectedDatastoreAccess
    elsif GitHub.elasticsearch_access_ignored?
      raise SearchDisabled
    else
      super(*args)
    end
  end
end

Elastomer::Client.send(:prepend, ElastomerWithEnabledCheck)
