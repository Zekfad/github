# rubocop:disable Style/FrozenStringLiteralComment

Time::DATE_FORMATS[:bold] = "%d %B %Y"
Time::DATE_FORMATS[:git] = -> (time) { time.strftime("@#{time.to_i} %z") }
Time::DATE_FORMATS[:date] = "%F"
Time::DATE_FORMATS[:zuora_usage_file] = "%m/%d/%Y"
Time::DATE_FORMATS[:eventer] = "%Y-%m-%dT%H:%M:%S"
Time::DATE_FORMATS[:deprecation_mailer] = -> (time) { day_format = ActiveSupport::Inflector.ordinalize(time.day); time.strftime("%B #{day_format}, %Y at %H:%M (%Z)") }
