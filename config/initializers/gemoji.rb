# rubocop:disable Style/FrozenStringLiteralComment
#
require "gemoji"

Emoji.create("atom")
Emoji.create("basecamp")
Emoji.create("basecampy")
Emoji.create("bowtie")
Emoji.create("electron")
Emoji.create("feelsgood")
Emoji.create("finnadie")
Emoji.create("goberserk")
Emoji.create("godmode")
Emoji.create("hurtrealbad")
Emoji.create("neckbeard")
Emoji.create("octocat")
Emoji.create("rage1")
Emoji.create("rage2")
Emoji.create("rage3")
Emoji.create("rage4")
Emoji.create("shipit")
Emoji.create("suspect")
Emoji.create("trollface")
