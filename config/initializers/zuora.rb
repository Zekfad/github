# frozen_string_literal: true

require "zuorest"

if GitHub.billing_enabled?
  background_job = GitHub.role.to_s.include?("worker")
  GitHub.zuorest_client = Zuorest::RestClient.new(
    server_url: GitHub.zuora_rest_server,
    access_key_id: GitHub.zuora_access_key_id,
    secret_access_key: GitHub.zuora_secret_access_key,
    client_id: GitHub.zuora_client_id,
    client_secret: GitHub.zuora_client_secret,
    timeout: (background_job ? 60 : 5),
    open_timeout: (background_job ? 60 : 5),
  ) do |conn|
    conn.request :instrumentation, name: "billing.zuora_client.request", instrumenter: GlobalInstrumenter
  end

  Zuorest::Model::Base.zuora_rest_client = GitHub.zuorest_client
end
