# frozen_string_literal: true

# These are the gems that should not be cleaned out between bundles.
# List is divided in the following parts:
# 1. Rails gems that change between Rails versions
# 2. Other libraries that change versions between Rails versions
BUNDLER_PERSISTENT_GEMS_AFTER_CLEAN = %w(
  rails
  railties
  actionpack
  actiontext
  activemodel
  activesupport
  actionmailbox
  actionmailer
  activerecord
  actioncable
  activejob
  actionview
  activestorage

  tzinfo
  zeitwerk
)
