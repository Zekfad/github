# rubocop:disable Style/FrozenStringLiteralComment

require "timer_daemon"
require "active_support/core_ext/numeric/time"
require "github/config/redis"
require File.expand_path("../basic", __FILE__)
require "github"
require "github/config/stats"
require "github/config/resque"
require "github/config/active_job"
require "resque/buffered_resque"
require "timed_resque"
require "timed_resque/buffered_timed_resque"
require "github/dgit"
require "job_scheduler"

# configure daemon
daemon = TimerDaemon.instance
daemon.err = GitHub::Logger.method(:log)
daemon.redis = GitHub.resque_redis

ScheduledFsTimerError = Class.new(StandardError)

# report exceptions to Haystack
daemon.error do |boom, timer|
  Failbot.report(ScheduledFsTimerError.new("timer #{timer.name} failed"), timer: timer.name)
end

# test timer
daemon.schedule "test", 30.seconds do
  daemon.log({ test: "hello from #$$" })
end

daemon.schedule "dgit-loadavg-check", 10.seconds, scope: :host do
  GitHub::DGit.loadavg_check
end
