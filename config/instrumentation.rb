# rubocop:disable Style/FrozenStringLiteralComment

require "cache_key_logging_blacklist"

# Loaded when instrumentation is enabled for the current process to install any
# custom hooks. The file is loaded during Rails's after_initialize hook so
# most classes should be available.
#
# See also: config/initializers/instrumentation.rb

# Instrument Memcached time
class Memcached::Rails
  class << self
    attr_accessor :query_time, :query_count, :query_tracing
  end
  self.query_count = 0
  self.query_time = 0
  self.query_tracing = false
end

module Memcached::Rails::WithTiming
  STATS_SAMPLE_RATES = {
    decr: 0.5,
    incr: 0.25,
    get: 0.5,
    # As of 2019-04-03 get_multi is called around 135k times per second on the
    # fe pods and around 500-600k/sec across all environments.  1% sampling is
    # probably more than we need.
    get_multi: 0.01,
    set: 0.5,
    add: 0.5,
    replace: 0.5,
    delete: 0.5,
    prepend: 0.5,
    append: 0.5,
  }.freeze

  [:decr, :incr, :get, :get_multi, :set, :add, :replace, :delete, :prepend, :append].each do |op|
    class_eval <<-RUBY, __FILE__, __LINE__+1
      def #{op}(*args, &block)
        if (prev = Memcached::Rails.query_tracing) == false
          Memcached::Rails.query_tracing = true
          start = Time.now
        end
        GitHub.tracer.with_span("rpc.memcached.#{op}".freeze, enable: GitHub.verbose_tracing?) do |span|
          span.set_tag("component".freeze, "memcached".freeze)
          if :#{op} == :get_multi
            cache_keys = args[0]
            span.set_tag("cache_key".freeze, CacheKeyLoggingBlacklist.scrub_multi(cache_keys).join(", ".freeze))
            span.set_tag("cache_key_count".freeze, cache_keys.length)
          else
            span.set_tag("cache_key".freeze, CacheKeyLoggingBlacklist.scrub(args[0]))
          end

          tags = ["rpc_operation:#{op}"]
          sample_rate = #{STATS_SAMPLE_RATES[op] || 0.5}

          start_time = GitHub::Dogstats.monotonic_time
          begin
            super(*args, &block)
          ensure
            elapsed = GitHub::Dogstats.duration(start_time)

            GitHub.dogstats.timing("rpc.memcached.time".freeze, elapsed, tags: tags, sample_rate: sample_rate)
            GitHub.dogstats.distribution("rpc.memcached.dist.time", elapsed, tags: tags)
          end
        end
      ensure
        if prev == false
          Memcached::Rails.query_time += (Time.now-start)
          Memcached::Rails.query_count += 1
          Memcached::Rails.query_tracing = prev
        end
      end
    RUBY
  end
end

Memcached::Rails.send(:prepend, Memcached::Rails::WithTiming)

# Instrument Memcached time
class GitHub::Cache::Client
  class << self
    attr_accessor :query_time, :query_count, :query_tracing, :query_keys
  end
  self.query_count = 0
  self.query_time = 0
  self.query_tracing = false
  self.query_keys = Hash.new(0)
end

module GitHub::Cache::Client::WithInstrumentation
  module QueryKey
    [:get, :set, :add].each do |op|
      class_eval <<-RUBY, __FILE__, __LINE__+1
        def #{op}(key, *args, &block)
          super(key, *args, &block)
        ensure
          GitHub::Cache::Client.query_keys[key] += 1
        end
      RUBY
    end

    [:get_multi].each do |op|
      class_eval <<-RUBY, __FILE__, __LINE__+1
        def #{op}(keys, *args, &block)
          super(keys, *args, &block)
        ensure
          keys.each do |key|
            GitHub::Cache::Client.query_keys[key] += 1
          end
        end
      RUBY
    end
  end

  module Aggregate
    [:decr, :incr, :get, :get_multi, :set, :add, :replace, :delete, :prepend, :append].each do |op|
      class_eval <<-RUBY, __FILE__, __LINE__+1
        def #{op}(*args, &block)
          if (prev = GitHub::Cache::Client.query_tracing) == false
            GitHub::Cache::Client.query_tracing = true
            start = Time.now
          end
          super(*args, &block)
        ensure
          if prev == false
            GitHub::Cache::Client.query_time += (Time.now-start)
            GitHub::Cache::Client.query_count += 1
            GitHub::Cache::Client.query_tracing = prev
          end
        end
      RUBY
    end
  end

  include QueryKey
  include Aggregate
end

GitHub::Cache::Client.send(:prepend, GitHub::Cache::Client::WithInstrumentation)

# Instrument activerecord objects
class ActiveRecord::Base
  class << self
    attr_accessor :obj_count, :obj_types
  end
  self.obj_count = 0
  self.obj_types = Hash.new(0)

  def instrument_object_creation
    ActiveRecord::Base.obj_count += 1
    ActiveRecord::Base.obj_types[self.class.name] += 1
  end

  after_initialize :instrument_object_creation
end

# Instrument template rendering time
require "rack/bug/panels/templates_panel/trace"
require "rack/bug/panels/templates_panel/rendering"
class Rack::Bug::TemplatesPanel::Rendering
  def html
    "<li><div class='template'><span class='name'>#{name}</span><span class='timing'>#{time_summary}</span></div>#{children_html}</li>"
  end
  def time_summary
    if children.any?
      "%.2fms</span><span class='exclusive'>(%.2fms)" % [time * 1_000, exclusive_time * 1_000]
    else
      "%.2fms" % (time * 1_000)
    end
  end
  def time
    (@end_time || Time.now) - @start_time
  end
end

class ActionView::Template
  class << self
    attr_accessor :template_trace, :template_trace_enabled

    def template_trace_reset
      self.template_trace = Rack::Bug::TemplatesPanel::Trace.new
    end
  end
  self.template_trace_enabled = false
end

module ActionView::Template::WithTiming
  def render(view, locals, buffer = ActionView::OutputBuffer.new, add_to_stack: true, &block)
    if !ActionView::Template.template_trace_enabled
      if GitHub.rails_6_0?
        return super(view, locals, buffer, &block)
      else
        return super(view, locals, buffer, add_to_stack: add_to_stack, &block)
      end
    end

    begin
      ActionView::Template.template_trace.start(virtual_path)
      if GitHub.rails_6_0?
        super(view, locals, buffer, &block)
      else
        super(view, locals, buffer, add_to_stack: add_to_stack, &block)
      end
    ensure
      ActionView::Template.template_trace.finished(virtual_path)
    end
  end
end

ActionView::Template.send(:prepend, ActionView::Template::WithTiming)

# Load all subscribers from config/instrumentation/
Dir[Rails.root + "config/instrumentation/*.rb"].each { |l| require l }

HydroLoader.load_github unless GitHub.lazy_load_hydro?
