# rubocop:disable Style/FrozenStringLiteralComment

# Public: debug class for displaying grant information, including its derivation
# and audit logs.
class Ability::Debug

  # Initialize a new debug instance.
  #
  # ability - the ability to explain
  #
  def initialize(ability)
    @ability = ability
  end

  # Public: print an explanation of the ability to STDOUT.
  #
  # This includes the ability's derivations.
  #
  # Returns self.
  def explain
    puts @ability.inspect
    @ability.ancestor_derivations.each do |derivation|
      parent = derivation.parent? ? " (parent)" : ""
      puts "  " << derivation.ancestor.inspect << parent
    end
    self
  end
end

Ability.module_eval do
  # Public: print debug information for abilities between an actor and subject.
  #
  # For console usage.
  def self.debug(actor, subject)
    abilities = between(actor, subject).map(&:debug)
    if abilities.empty?
      puts "no abilities found"
    else
      abilities.each(&:explain)
    end
    nil
  end

  # Public: an Ability::Debug instance for this ability. For use in console.
  def debug
    Ability::Debug.new(self)
  end
end
