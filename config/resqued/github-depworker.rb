# rubocop:disable Style/FrozenStringLiteralComment

# Configuration for github-depworker hosts. Shared between resque and
# aqueduct. Loads after github-environment.rb.

GitHub.role = :depworker

default_pool = 24
repo_dep_pool = GitHub.environment.fetch("DEPWORKER_REPOSITORY_DEPENDENCIES_POOL", default_pool).to_i
vuln_id_pool = GitHub.environment.fetch("DEPWORKER_VULNERABILITY_IDENTIFICATION_POOL", default_pool).to_i
dependabot_pool = GitHub.environment.fetch("DEPWORKER_DEPENDABOT_POOL", default_pool).to_i
manifest_backfill_pool = GitHub.environment.fetch("DEPWORKER_MANIFEST_BACKFILL_POOL", default_pool).to_i

# The WorkerPoolConfig divides workers between resque and aqueduct depending on
# feature flag rollout. If 25% of jobs are sent to aqueduct via feature flag,
# 25% of the worker pool will be allocated to aqueduct.
worker_pool_config = Resqued::WorkerPoolConfig.new(
  # Which type of worker is this, aqueduct or resque?
  worker_type: if GitHub.environment["ENABLE_AQUEDUCT"]
                 Resqued::WorkerPoolConfig::AQUEDUCT_WORKER
               else
                 Resqued::WorkerPoolConfig::RESQUE_WORKER
               end,
  # The app-role determines the rollout feature flag name.
  app_role: :github_depworker,
  # The total number of aqueduct and resque workers to run on the host.
  total_pool_size: [repo_dep_pool, vuln_id_pool, dependabot_pool].max + 1,
  # The minimum number of resque workers to run, regardless of pool allocation.
  min_resque_workers: 1,
  # The minimum number of aqueduct workers to run, regardless of pool allocation.
  min_aqueduct_workers: 1
)

# Apply the worker pool config to this resqued config, which toggles the aqueduct
# resqued adapter when necessary and adds a hook to restart workers if the
# feature flag controlling pool allocation changes.
worker_pool_config.apply(self)

# Configure worker pool sizes for aqueduct and resque.
if worker_pool_config.aqueduct_worker?
  worker_pool worker_pool_config.aqueduct_workers, shuffle_queues: true
else
  worker_pool worker_pool_config.resque_workers, shuffle_queues: true
end

queue "repository_dependencies", count: repo_dep_pool
queue "vulnerability_identification", count: vuln_id_pool
queue "dependabot", count: dependabot_pool
queue "manifest_backfill", count: manifest_backfill_pool
