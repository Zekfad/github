# rubocop:disable Style/FrozenStringLiteralComment

# Shared configuration for all resque workers in Enterprise. Loads after
# github-environment.rb.

GitHub.role = :enterpriseworker

high_num  = ENV["ENTERPRISE_RESQUED_HIGH_WORKERS"].to_i
low_num   = ENV["ENTERPRISE_RESQUED_LOW_WORKERS"].to_i
maint_num = ENV["ENTERPRISE_RESQUED_MAINT_WORKERS"].to_i

casts = { "" => nil, "1" => true, "0" => false, "true" => true, "false" => false }

jobs_enabled = casts[ENV["ENTERPRISE_RESQUED_JOBS_ENABLED"]]
is_replica = casts[ENV["ENTERPRISE_IS_REPLICA"]]
maintenance_enabled = casts[ENV["ENTERPRISE_RESQUED_MAINTENANCE_ENABLED"]]

high_queues = %w[
  archive
  archive_project
  archive_project_cards
  audit_logs
  cancel_repo_invitation
  clear_abilities
  commit_upload_manifest
  convert_to_outside_collaborator
  create_pull_request_merge_commit
  critical
  delete_dependent_abilities
  deliver_integration_update_email
  deliver_notifications
  deliver_mobile_push_notifications
  destroy_integration
  destroy_team
  destroy_team_dependants
  enforce_two_factor_requirement
  event
  org_insights_backfill
  fix_duplicate_labels
  gist_push
  high
  import_project
  index_high
  issue_triage
  kubernetes_deliver_notifications
  ldap_new_member_sync
  ldap_team_sync
  ldap_user_sync
  low
  mail
  notifications
  notifications_maintenance
  populate_cloned_project
  handle_team_parent_changes
  remove_noncollab_assignees_from_issue
  remove_oauth_user_tokens
  remove_org_member_data
  remove_outside_collaborator_from_organization
  remove_unlinked_saml_members_from_organization
  remove_user_from_org
  remove_repo_member
  remove_team_from_repo
  repair_abilities_from_users_to_ancestor_teams
  repository_add_teams
  repository_pre_receive
  request_pr_reviewers
  restore_organization_user
  retry_jobs
  retry_transfer_issue
  revoke_org_apps_management_grants
  revoke_org_membership_abilties
  signup_emails
  team_add_forks
  team_member_added_emails
  team_remove_members
  transfer_discussion
  transfer_issue
  transfer_repository
  transform_user_into_org
  unlink_project_repo_links
  update_close_issue_references
  update_organization_team_privacy
  update_member_repo_permissions
  update_invitation_repo_permissions
  update_team_repo_permissions
  user_delete
  user_signup
  user_suspend
  user_suspend_dormant
  update_table_user_hidden
  wiki_receive
  migrate_legacy_admin_teams
  repository_push
  pull_request_synchronization
  saml_session_revoke
  staff_tools_report
  team_update_forked_repository_permissions
  subscribe_and_notify
  sync_organization_default_repository_permission
  maintain_tracking_ref
  notify_subscription_status_change
  clear_graph_data_on_block
  uninstall_integration_installation
  upgrade_integration_installation_version
  update_integration_installation_rate_limit
  install_automatic_integrations
  integration_installation_repository_removal
  workspace_repository_abilities
  disable_two_factor_requirement
  clone_template_repo_files
  sync_scoped_integration_installations
  remove_biz_user_forks
  deliver_hook_event
  post_to_hookshot
  post_hooks_to_aqueduct
  redeliver_hooks
  hook_delivery_race_condition_check
  remove_invalid_user_forks
  synchronize_pull_request
]

low_queues = %w[
  actions
  analytics
  kubernetes_analytics
  archive_dangling_forks
  archive_restore
  repository_purge
  background_destroy
  boomtown
  calculate_stars_count
  github_connect
  dependabot
  devtools
  dotcom_importer
  dgit_repairs
  dgit_schedulers
  remove_expired_oauth
  cdn
  graphs
  import_issue
  index_low
  languages
  lfs
  map_import_records
  netgraph
  oauth_access_bumps
  oauth_authorization_bumps
  prepare_import_archive
  public_key_accesses
  pull_request_review_stats
  purge_restorables
  purge_stale_upload_manifest_file_blobs
  rebalance_milestone
  rebalance_project_column
  rebalance_memex_project
  reminders
  repository_dependencies
  repository_disk_usage
  stars
  task_list_instrumentation
  contributions_backfill
  contributions_track_push
  contributions_instrument_push
  update_event_feeds
  user_contributions_backfill
  vulnerability_identification
  toggle_hidden_user_in_notifications
  cancel_team_membership_requests
  change_network_root
  slotted_counters
  project_workflows
  project_workflow_resync
  oauth_application_delete
  remove_oauth_app_tokens
  remove_repo_keys_for_policymaker_and_app
  create_check_suites
  git_signing
  mailers
  repository_set_license
  mirrors
  remove_integration_tokens
  collect_metrics
  create_auto_inactive_deployment_statuses
  integration_installation_instrumentation
  integration_manifests
  process_unfurls
  deliver_repository_push_notifications
  enforce_active_user_session_limit
  invalidate_content_attachments_cache
  newsletter_delivery_run
  add_to_suppression_list
  calculate_followerings_count
  calculate_trending_repos
  calculate_trending_users
  chatterbox_notification
  clear_soft_bounces
  core_receive
  delayed
  email_reply
  calculate_topic_applied_counts
  email
  email_unsubscribe
  feature_showcases
  gist_flagged_by_user
  ignore_user
  migration_destroy_file
  migration_enqueue_destroy_file_jobs
  move_repo_downloads
  repository_access
  network_maintenance
  network_maintenance_scheduler
  gist_maintenance
  gist_maintenance_scheduler
  newsies_auto_subscribe
  newsletter_delivery
  nexmo_balance_stats
  oauth_app_policy_usage_stats
  organization_application_access_approved
  organization_application_access_requested
  otp_sms_timing_cleanup
  post_mirror_hook
  post_receive
  pre_receive_environment_delete
  pre_receive_environment_download
  pre_receive_repository_update
  purge_archived_assets
  purge_stale_render_blobs
  rebuild_storage_usage
  remove_from_suppression_list
  repository_fsck
  repository_mirror
  repository_network_operations
  repository_purge_archived
  repository_sync
  sync_asset_usage
  synchronize_plan_subscription
  vulnerability_notification
  storage_cluster
  dpages_evacuations
  dpages_evacuations_scheduler
  dpages_maintenance
  dpages_maintenance_scheduler
  wiki_maintenance
  wiki_maintenance_scheduler
  interactive_components
  stale_check_runs
  manifest_backfill
  gitbackups_perform
  gitbackups_backfill
  index_bulk
  invalidate_expired_invites
  process_mentioned_references
  revoke_oauth_accesses
  token_scanning
  token_scanning_scheduler
  token_path_discovery
  token_scanning_config_change
  code_scanning
]

low_queues_primary = %w[
  page
]

low_queues += low_queues_primary unless is_replica

# Trigger loading license so each worker doesn't have to
GitHub::Enterprise.license

# all non-maint workers process the high queues.
# low queues are processed only by low workers.
if jobs_enabled
  high_num.times { worker *high_queues }
  low_num.times  { worker *(high_queues + low_queues) }
end

if maintenance_enabled
  maint_num.times { worker "maint_#{GitHub.local_git_host_name}" }
end

before_fork do |x|
  # enterprise uses environment variables to inject customer configuration. When
  # resqued reloads due to a customer update, it needs to slurp up the latest
  # environment variables from the shared/env.d/*.sh files.
  lines = POSIX::Spawn::Child.new(". #{ENV["ENTERPRISE_APP_INSTANCE"]}/.app-config/production.sh && env").out.split("\n")
  enterprise_variables = lines.grep(/\A(ENTERPRISE|GH_|GITHUB)/).map { |l| l.split("=", 2) }
  enterprise_variables.each do |(key, value)|
    ENV[key] = value
  end
end

after_fork do |worker|
  # renice maint worker to very low cpu/io priority
  if worker.queues == ["maint_#{GitHub.local_git_host_name}"]
    Process.setpriority(Process::PRIO_PROCESS, 0, 15) # nice 15
    system("ionice", "-c", "3", "-p", $$.to_s) # wish list: ioprio_set(2)
  end
end
