# rubocop:disable Style/FrozenStringLiteralComment

# Common configuration for production, enterprise, review-lab, and staging-lab.
# Not used in development.

# github-*worker.rb should override this.
#
# GitHub.role is used for a bunch of things, including default tags on
# metrics, as a flipper actor, to look up role-specific config, etc.
ENV["GITHUB_CONFIG_ROLE"] ||= "resque"

before_fork do |x|
  # Load the app in resqued-listener. This keeps startup CPU seconds at
  # N (the time to boot the app once) rather than N*M (where M is the
  # number of workers).
  require "#{Rails.root}/config/environment"

  # We don't have to worry about a 10s request timer in background jobs.
  # TODO: we may need to change this for jobs that run in Kubernetes.
  GitRPC.timeout = 30.minutes.to_i
  Elastomer.config.read_timeout = 30.seconds.to_i

  # Flush logs immediately. Jobs infrequently enough that the default of
  # 1000 might result in us missing some log lines.
  Rails.logger.auto_flushing = 1 if Rails.logger.respond_to?(:auto_flushing=)

  # We don't need a database connection before forking the workers.
  ActiveRecord::Base.clear_all_connections!

  # Annotate resqued-listener's procline with the current app version.
  x.app_version = GitHub.current_sha[0, 7]

  # Prune the worker set only on startup to reduce contention on the
  # workers list in redis when worker processes wake up en masse.
  # Allows failures to ensure resqued can boot up properly with Redis offline
  begin
    Resque::Worker.new("*").prune_dead_workers
  rescue *GitHub::Config::Redis::REDIS_DOWN_EXCEPTIONS
  rescue ::Redis::BaseError => e
    Failbot.report!(e)
  end

  # This is a workaround for https://github.com/grpc/grpc/issues/8798. GRPC
  # doesn't provide a mechanism for resetting shared resources after forking,
  # so we need to make sure no shared resources were created before forking.
  # The shared resources are created the first time a channel is created, so
  # raise if any channels exist. Once the above issue is fixed, this can be
  # replaced with a reset in after_fork.
  # This workaround also exists in config/unicorn.rb.
  if defined?(GRPC) && ENV["RAILS_ENV"] != "test"
    ObjectSpace.each_object(GRPC::Core::Channel) do |chan|
      raise RuntimeError, "A GRPC channel was created before forking! Please "+
                          "create GRPC resources after forking."
    end
  end

  begin
    require_relative "../../vendor/rover/report"
    Rover.report!
  rescue Rover::Error => e
    $stderr.puts e
  end

  GitHub::BootMetrics.record_state("resqued.before_fork")
  GitHub::BootMetrics.send_to_datadog
end

after_fork do |worker|
  worker.extend(Resque::VersionInProcline)
  ActiveRecord::Base.establish_connection # rubocop:disable GitHub/DoNotCallMethodsOnActiveRecordBase
  GitHub.cache.reset
  GitHub::Logger.setup

  # Disable fork-per-job. It adds unnecessary time to each job. Also, we prefer the graceful shutdown behavior that comes with cant_fork=true.
  worker.cant_fork = true
end
