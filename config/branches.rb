# rubocop:disable Style/FrozenStringLiteralComment

rails_root = ENV["RAILS_ROOT"] || "/data/github/current"

worker_processes 2
preload_app true
timeout 30

pid "#{rails_root}/tmp/pids/unicorn.pid"
listen "#{rails_root}/tmp/sockets/unicorn.sock", backlog: 256

logfile = "#{rails_root}/log/unicorn.log"
stderr_path logfile
stdout_path logfile

before_fork do |server, worker|
  t1 = Time.now
  server.logger.info "master before_fork=start worker=#{worker.nr}"

  # wait until last worker boots to send QUIT signal
  if worker.nr == (server.worker_processes - 1)
    server.logger.info "master before_fork=last_worker worker=#{worker.nr}"
    old_pid = "#{rails_root}/tmp/pids/unicorn.pid.oldbin"
    if File.exist?(old_pid) && server.pid != old_pid
      begin
        server.logger.info "master before_fork=quit worker=#{worker.nr}"
        Process.kill("QUIT", File.read(old_pid).to_i)
      rescue Errno::ENOENT, Errno::ESRCH
        server.logger.info "master before_fork=quit_rescue worker=#{worker.nr}"
      end
    end
  end
  duration = (Time.now-t1) * 1000
  server.logger.info "master before_fork=end worker=#{worker.nr} duration=#{'%.2f' % duration}"
end

after_fork do |server, worker|
  t1 = Time.now
  server.logger.info "worker=#{worker.nr} after_fork=start"
  ActiveRecord::Base.clear_all_connections!
  GitHub.cache.reset
  GitHub.resque_redis._client.reconnect
  duration = (Time.now-t1) * 1000
  server.logger.info "worker=#{worker.nr} after_fork=end duration=#{'%.2f' % duration}"
end
require "unicorn/kill_introspect_worker"
require "unicorn/log_preload"

before_exec do |server|
  t1 = Time.now
  server.logger.info "master before_exec=start"
  ENV.delete("RUBYLIB")
  duration = (Time.now-t1) * 1000
  server.logger.info "master before_exec=end duration=#{'%.2f' % duration}"
end
