# frozen_string_literal: true

if GitHub.billing_enabled?
  class ZuoraClientSubscriber
    # Env gives us access to
    # Request
    # :method - :get, :post, ...
    # :url    - URI for the current request; also contains GET parameters
    # :body   - POST parameters for :post/:put requests
    # :request_headers
    #
    # Response
    # :status - HTTP response status code, such as 200
    # :body   - the response body
    # :response_headers
    def call(name, start, ends, transaction_id, env)
      http_method = env.method.to_s.upcase
      duration = ends - start
      status = env.status
      url = env.url

      tags = [
        "http_method:#{http_method}",
        "status:#{status}",
        "path:#{normalize_path(http_method, url.path)}"
      ]

      GitHub.dogstats.timing("billing.zuora_client.request", duration.in_milliseconds, tags: tags)
    end

    private

    def normalize_path(method, path)
      case method.to_s.upcase
      when "GET", "DELETE"
        # Both GET and DELETE require an ending ID so we match everything up to the ID and replace the ID with
        # :id
        path.gsub(/(\/v1(:?[\/[\w-]+]+))\/[\w-]+/, "\\1/:id")
      when "POST"
        # POST requests don't have an interpolated portion so we leave as is
        path
      when "PUT"
        # We only use PUT requests in a handful of places and they mostly follow the convention of
        # - /v1/object/foo/:id
        # - /v1/foo/:id
        # - /v1/subscription/:id/suspend or cancel or resume
        path.gsub(/(\/v1(?:\/object)?\/[\w-]+)(?:\/[\w-]+)?(\/(?:cancel|suspend|resume))?/, "\\1/:id\\2")
      else
        # If we see these, we should investigate other HTTP methods and which are supported by zuora
        "unknown"
      end
    end
  end

  GlobalInstrumenter.subscribe("billing.zuora_client.request", ZuoraClientSubscriber.new)
end
