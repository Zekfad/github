# rubocop:disable Style/FrozenStringLiteralComment

GitHub.subscribe /\Amarketplace\_purchase\.(purchased|cancelled|changed)\Z/ do |name, start, ending, transaction_id, payload|
  item = Billing::SubscriptionItem.find payload[:subscription_item_id]
  listing = item.subscribable.listing
  action = name.split(".").last

  item.account.transactions.create \
    action: "mp_#{action}",
    current_subscribable: item.subscribable,
    current_subscribable_quantity: item.quantity,
    old_subscribable_id: payload[:previous_subscribable_id],
    old_subscribable_type: payload[:previous_subscribable_type],
    old_subscribable_quantity: payload[:previous_quantity],
    active_listing: listing.billable?
end

GitHub.subscribe(/\Asponsorship\.(added|cancelled|changed)\Z/) do |name, start, ending, transaction_id, payload|
  item = Billing::SubscriptionItem.find payload[:subscription_item_id]
  listing = item.subscribable.listing
  action = name.split(".").last

  item.account.transactions.create \
    action: "sp_#{action}",
    current_subscribable: item.subscribable,
    current_subscribable_quantity: item.quantity,
    old_subscribable_id: payload[:previous_subscribable_id],
    old_subscribable_type: payload[:previous_subscribable_type],
    old_subscribable_quantity: payload[:previous_quantity],
    active_listing: listing.billable?
end
