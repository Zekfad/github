# rubocop:disable Style/FrozenStringLiteralComment

if GitHub.enterprise? && GitHub.auth.ldap?
  def debug_logging
    return unless GitHub::LDAP.debug_logging_enabled?
    yield if block_given?
  end

  GitHub.subscribe "bind.net_ldap" do |*args|
    debug_logging do
      event = ActiveSupport::Notifications::Event.new(*args)
      GitHub.auth.log_ldap \
        transaction_id: event.transaction_id,
        method: "Net::LDAP#bind",
        at: "Bind result",
        result: event.payload[:result],
        duration: event.duration
    end
  end

  GitHub.subscribe "bind.net_ldap_connection" do |*args|
    debug_logging do
      event = ActiveSupport::Notifications::Event.new(*args)
      GitHub.auth.log_ldap \
        transaction_id: event.transaction_id,
        method: "Net::LDAP::Connection#bind",
        at: "Bind result",
        result: event.payload[:result].result,
        duration: event.duration
    end
  end

  GitHub.subscribe "search.net_ldap" do |*args|
    debug_logging do
      event = ActiveSupport::Notifications::Event.new(*args)

      if event.payload[:result].nil?
        # log search error
        GitHub.auth.log_ldap \
          transaction_id: event.transaction_id,
          method: "Net::LDAP#search",
          at: "Search failed",
          duration: event.duration
      else
        # log server capabilities
        if (event.payload[:attributes] || []).include?(:namingContexts)
          result = event.payload[:result].first
          capabilities = [
            :namingContexts,          result[:namingcontexts],
            :supportedControl,        result[:supportedcontrol],
            :supportedExtension,      result[:supportedextension],
            :supportedFeatures,       result[:supportedfeatures],
            :supportedLdapVersion,    result[:supportedldapversion],
            :supportedsaslmechanisms, result[:supportedsaslmechanisms]
          ]
          GitHub.auth.log_ldap \
            transaction_id: event.transaction_id,
            method: "Net::LDAP#search",
            at: "Search Root DSE",
            capabilities: capabilities,
            duration: event.duration
        else
          # log search
          GitHub.auth.log_ldap \
            transaction_id: event.transaction_id,
            method: "Net::LDAP#search",
            at: "Search result",
            filter: event.payload[:filter].to_s,
            base: event.payload[:base],
            attributes: event.payload[:attributes],
            duration: event.duration
        end
      end
    end
  end

  GitHub.subscribe "search.net_ldap_connection" do |*args|
    debug_logging do
      event = ActiveSupport::Notifications::Event.new(*args)

      if event.payload[:result].present? && event.payload[:result].failure?
        result = event.payload[:result]
        GitHub.auth.log_ldap \
          transaction_id: event.transaction_id,
          method: "Net::LDAP::Connection#search",
          at: "Search failed",
          result_code: result.result_code,
          error_message: result.error_message,
          matched_dn: result.result[:matchedDN]
      end

      # ignore rootDSE query
      next if (event.payload[:attributes] || []).include?(:namingContexts.to_s.to_ber)
      GitHub.auth.log_ldap \
        transaction_id: event.transaction_id,
        method: "Net::LDAP::Connection#search",
        at: "Search",
        filter: event.payload[:filter].to_s,
        base: event.payload[:base],
        scope: event.payload[:scope],
        limit: event.payload[:limit],
        result_count: event.payload[:result_count],
        page_count: event.payload[:page_count],
        duration: event.duration
    end
  end

  ##### LDAP Sync
  # New user sync on first login
  GitHub.subscribe "ldap_new_member_sync.perform" do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    GitHub.stats.increment("ldap.sync.new_members.total")
    GitHub.stats.timing("ldap.sync.new_members.runtime", event.duration)
    GitHub.auth.log_sync \
      event.payload.merge \
        at: "New user sync",
        runtime: event.duration
  end

  # Single team sync
  GitHub.subscribe "team.ldap_sync" do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    GitHub.stats.increment("ldap.sync.team.total")
    GitHub.stats.timing("ldap.sync.team.runtime", event.duration)
    GitHub.auth.log_sync \
      event.payload.merge \
        at: "Team sync",
        runtime: event.duration
  end

  GitHub.subscribe "team_sync.member_search" do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    GitHub.stats.increment("ldap.sync.team_member_search.total")
    GitHub.stats.timing("ldap.sync.team_member_search.runtime", event.duration)
    GitHub.auth.log_sync \
      event.payload.merge \
        at: "Team member search",
        result_count: event.payload[:result_count],
        runtime: event.duration
  end

  # Single team sync error
  GitHub.subscribe :ldap_sync_error do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    GitHub.auth.log_sync \
      event.payload.merge \
      at: "Team sync error",
      runtime: event.duration
  end

  # Team sync
  GitHub.subscribe "ldap_team_sync.perform" do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    GitHub.stats.increment("ldap.sync.teams.total")
    GitHub.stats.timing("ldap.sync.teams.runtime", event.duration)
    GitHub.auth.log_sync \
      event.payload.merge \
        at: "Bulk team sync",
        runtime: event.duration
  end

  # User sync
  GitHub.subscribe "user.ldap_sync" do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    GitHub.stats.increment("ldap.sync.user.total")
    GitHub.stats.timing("ldap.sync.user.runtime", event.duration)
    GitHub.auth.log_sync \
      event.payload.merge \
        at: "User sync",
        runtime: event.duration
  end

  GitHub.subscribe "ldap_user_sync.perform" do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    GitHub.stats.increment("ldap.sync.users.total")
    GitHub.stats.timing("ldap.sync.users.runtime", event.duration)
    GitHub.auth.log_sync \
      event.payload.merge \
        at: "Bulk user sync",
        runtime: event.duration
  end

  GitHub.subscribe "ldap.authenticate" do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    if event.payload[:user]
      GitHub.stats.timing("ldap.authenticate.success.runtime", event.duration)
    else
      GitHub.stats.timing("ldap.authenticate.failure.runtime", event.duration)
    end
  end

  GitHub.subscribe "ldap.authenticate.timeout" do |*args|
    event = ActiveSupport::Notifications::Event.new(*args)
    GitHub.stats.increment("ldap.authenticate.timeout.total")
  end
end
