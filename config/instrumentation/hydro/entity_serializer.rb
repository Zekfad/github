# frozen_string_literal: true

# Common entity serializer for Hydro payload fields
module Hydro
  module EntitySerializer

    # This list is a mirror of the values declared in the following schemas:
    #   https://github.com/github/hydro-schemas/blob/master/github/v1/entities/news_feed_event.proto
    #   https://github.com/github/hydro-schemas/blob/master/github/v1/entities/news_feed_event_group.proto
    #
    # If you change NEWS_FEED_EVENT_TYPES, you must also update those schemas and vice versa
    NEWS_FEED_EVENT_TYPES = [:ATOM, :COMMIT_COMMENT, :CREATE, :DELETE, :DEPLOYMENT,
      :DOWNLOAD, :FOLLOW, :FORK, :GOLLUM, :ISSUE_COMMENT, :ISSUES, :MEMBER, :PUBLIC,
      :PULL_REQUEST_REVIEW_COMMENT, :PULL_REQUEST, :PUSH, :RELEASE, :WATCH, :SPONSOR].freeze

    AUTH_TYPE_MAPPING = {
      anon: :ANON,
      oauth: :OAUTH,
      oauth_key_secret: :OAUTH_SERVER_TO_SERVER,
      basic: :BASIC,
      jwt: :JWT,
      personal_access_token: :PERSONAL_ACCESS_TOKEN,
      integration: :INTEGRATION_USER_TO_SERVER,
      integration_installation: :INTEGRATION_SERVER_TO_SERVER,
    }

    class << self
      def app(app, overrides: {})
        return if app.nil?

        payload = {
          app_type: app.class.name.underscore.upcase,
          id: app.id,
          name: app.name,
          url: app.url,
          callback_url: app.callback_url,
          created_at: app.created_at,
        }

        case app
        when Integration
          domain = begin
            URI(app.url).hostname
          rescue ArgumentError, URI::InvalidURIError
          end

          payload.merge!(
            domain: domain,
            owner_id: app.owner_id,
            integration_bot_id: app.bot_id,
            integration_is_public: app.public?,
          )
        when OauthApplication
          payload.merge!(
            domain: app.domain,
            owner_id: app.user_id,
          )
        end

        payload.merge(overrides)
      end

      def repository_action(global_action_id)
        _, action_id = Platform::Helpers::NodeIdentification.from_global_id(global_action_id)
        action = RepositoryAction.find_by(id: action_id)
        return if action.nil?

        {
          id: action.id,
          name: action.name,
          description: action.description,
          color: action.color,
          icon_name: action.icon_name,
          path: action.path,
          repository_id: action.repository_id,
          created_at: action.created_at,
          action_is_featured: action.featured?,
          action_is_public: action.repository.public?,
          readme: action.readme&.data,
        }
      end

      def repository_image(repo_image)
        return unless repo_image

        {
          id: repo_image.id,
          name: repo_image.name,
          content_type: repo_image.content_type,
          size: repo_image.size,
          uploader_id: repo_image.uploader_id,
          repository_id: repo_image.repository_id,
          created_at: repo_image.created_at,
          updated_at: repo_image.updated_at,
          role: repo_image.role.upcase.to_sym, # e.g., :OPEN_GRAPH
        }
      end

      def repository_owner(repository)
        return unless repository

        user(repository.owner)
      end

      def user_repository_member?(repository, user)
        return false unless repository && user

        repository.member?(user)
      end

      def string_bytesize(string)
        string.try(:bytesize) || 0
      end

      def marketplace_listing_from_database_id(listing_id)
        listing = Marketplace::Listing.find_by(id: listing_id)
        return if listing.nil?

        marketplace_listing(listing)
      end

      def marketplace_listing(listing)
        return if listing.nil?

        # This type needs to match one of the defined MarketplaceListing::Type
        # Enums in Hydro.
        listing_type = if listing.listable_is_integration?
          "github_app"
        elsif listing.listable_is_oauth_application?
          "oauth_app"
        elsif listing.listable_is_sponsorable?
          "user"
        else
          "unknown"
        end

        {
          id: listing.id,
          name: listing.name,
          slug: listing.slug,
          type: listing_type,
          app_id: listing.listable_id,
          current_state: listing.current_state.name.to_s,
          primary_category_id: listing.primary_category_id,
          secondary_category_id: listing.secondary_category_id,
        }
      end

      def marketplace_listing_plan_from_database_id(listing_plan_id)
        listing_plan = Marketplace::ListingPlan.find_by(id: listing_plan_id)
        return if listing_plan.nil?

        marketplace_listing_plan(listing_plan)
      end

      def marketplace_listing_plan(listing_plan)
        return if listing_plan.nil?

        MarketplaceListingPlanEntitySerializer.serialize(listing_plan)
      end

      def sponsors_listing(listing)
        return if listing.nil?

        {
          id: listing.id,
          slug: listing.slug,
          sponsorable_type: listing.sponsorable_type,
          sponsorable_id: listing.sponsorable_id,
          current_state: listing.current_state.name.to_s,
        }
      end

      def sponsors_tier(tier)
        {
          id: tier.id,
          monthly_price_in_cents: tier.monthly_price_in_cents,
          current_state: tier.current_state.name,
        }
      end

      def sponsorship(sponsorship)
        return if sponsorship.nil?

        {
          privacy_level: sponsorship.privacy_level,
          is_sponsor_opted_in_to_email: sponsorship.is_sponsor_opted_in_to_email,
          sponsor: Hydro::EntitySerializer.user(sponsorship.sponsor),
          maintainer: Hydro::EntitySerializer.user(sponsorship.sponsorable),
          id: sponsorship.id,
        }
      end

      def sponsors_goal(goal)
        return if goal.blank?

        {
          id: goal.id,
          kind: goal.kind,
          state: goal.current_state.name,
          target_value: goal.target_value,
          description: goal.description,
        }
      end

      def billing_type(billing_type_string)
        case billing_type_string
        when "card"
          :SELF_SERVE
        when "invoice"
          :INVOICED
        else
          :UNKNOWN
        end
      end

      def destination_account_type(account_type_string)
        case account_type_string
        when "stripe_connect"
          :STRIPE_CONNECT
        else
          :UNKNOWN_DESTINATION_ACCOUNT_TYPE
        end
      end

      def payment_gateway(payment_gateway_string)
        case payment_gateway_string
        when "stripe"
          :STRIPE
        when "paypal"
          :PAYPAL
        else
          :UNKNOWN_PAYMENT_GATEWAY
        end
      end

      def payout_status(status_string)
        case status_string
        when "created"
          :CREATED
        when "failed"
          :FAILED
        else
          :UNKNOWN_STATUS
        end
      end

      def funding_instrument_type(instrument_type_string)
        case instrument_type_string
        when "bank_account"
          :BANK_ACCOUNT
        when "card"
          :CARD
        else
          :UNKNOWN_FUNDING_INSTRUMENT_TYPE
        end
      end

      def auth_type(auth_type)
        AUTH_TYPE_MAPPING[auth_type&.to_sym]
      end

      # ex
      # {
      #   "solved":false,
      #   "score":0,
      #   "user_ip":"4.53.133.38",
      #   "other":
      #   {
      #     "session":"1735ab185935956b9.7894433602",
      #     "time_verified":1521583567,
      #     "attempted":true,
      #     "session_created":"2018-03-20T22:05:07+00:00",
      #     "check_answer":"2018-03-20T22:05:31+00:00",
      #     "verified":"2018-03-20T22:06:07+00:00",
      #     "previously_verified":true,
      #     "session_timed_out":true,
      #     "suppressed":true,
      #     "suppress_limited":true,
      #   }
      # }
      #
      def funcaptcha_response(response, overrides: {})
        return unless response.present?

        new_response = response.slice("solved", "score", "user_ip", "error")

        if response["other"].present?
          new_response.merge!(response["other"].slice(
            "session",
            "time_verified",
            "attempted",
            "session_created",
            "check_answer",
            "verified",
            "previously_verified",
            "session_timed_out",
            "suppressed",
            "suppress_limited",
          ))
        end
        new_response.merge(overrides)
      end

      def search_type(unfiltered_search_types)
        unfiltered_search_types.map do |val|
          SEARCH_TYPES.include?(val) ? val : "unknown"
        end
      end
      SEARCH_TYPES = %w(audit_log blog code commit gist gist_quicksearch issue marketplace project pull_request registry_package repo topic user user_login wiki).freeze

      def interactive_component(component, overrides: {})
        container_type = case c_type = component.container_type.to_s
        when "ComposableComment"
          c_type
        else
          "UNKNOWN"
        end

        {
          id: component.id,
          global_relay_id: component.global_relay_id.to_s,
          container_id: component.container_id,
          container_type: enum_from_string(container_type),
          external_id: component.external_id,
          elements: component.elements,
          container_order: component.container_order,
          created_at: component.created_at,
          updated_at: component.updated_at,
          interacted_at: component.interacted_at,
        }.merge(overrides)
      end

      def composable_comment(comment, overrides: {})
        pos = case comment.position
        when :inline
          "INLINE"
        else
          "UNKNOWN"
        end

        {
          id: comment.id,
          global_relay_id: comment.global_relay_id.to_s,
          integration: integration(comment.integration),
          issue: issue(comment.issue),
          components: comment.components.to_json,
          position: pos,
          created_at: comment.created_at,
          updated_at: comment.updated_at,
        }.merge(overrides)
      end

      def integration(entity, overrides: {})
        return if entity.nil?

        domain = begin
          URI(entity.url).hostname
        rescue ArgumentError, URI::InvalidURIError
        end

        {
          id: entity.id,
          name: entity.name,
          domain: domain,
          url: entity.url,
          callback_url: entity.callback_url,
          owner_id: entity.owner_id,
          bot_id: entity.bot_id,
          public: entity.public?,
          created_at: entity.created_at,
        }.merge(overrides)
      end

      def integration_installation(entity, overrides: {})
        return if entity.nil?

        if entity.has_key? :business
          target_id = entity[:business_id]
          target_name = entity[:business]
          target_type = "Business"
        elsif entity.has_key? :org
          target_id = entity[:org_id]
          target_name = entity[:org]
          target_type = "Organization"
        else
          target_id = entity[:user_id]
          target_name = entity[:user]
          target_type = "User"
        end

        {
          id: entity[:installation_id],
          target_type: target_type,
          target_id: target_id,
          target_name: target_name,
          repository_selection: entity[:repository_selection],
        }.merge(overrides)
      end

      def gist(gist, overrides: {})
        {
          id: gist.id,
          global_relay_id: gist.global_relay_id.to_s,
          repo_name: gist.repo_name.to_s,
          description: gist.description.to_s,
          visibility: gist.visibility.upcase,
          parent_id: gist.parent_id,
          created_at: gist.created_at,
          owner_id: gist.user_id,
        }.merge(overrides)
      end

      def gist_files(gist, gist_files)
        return unless gist
        return unless gist_files

        gist_files.map do |file|
          {
            filename: file.name.to_s,
            mime_type: file.mime_type.to_s,
            language: file.language.try(:name).to_s,
            raw_url: gist.raw_url_for(file).to_s,
            size: file.size,
          }
        end
      end

      def specimen_data(data)
        return if data.nil?
        trimmed_data = data[0..GitHub::SpamChecker::GIST_MAX_FILE_DATA]
        specimen = Spam::Specimen.new(trimmed_data)
        {
          raw_data: trimmed_data,
          clean_data: specimen.data[0..GitHub::SpamChecker::GIST_MAX_FILE_DATA],
          clean_text: specimen.text,
          has_repeated_links: specimen.has_repeated_links?,
          nothing_but_image_links: specimen.nothing_but_image_links?,
          uses_spacing_tricks: specimen.uses_spacing_tricks?,
        }
      end

      def gist_specimen_files(gist_files)
        return unless gist_files

        gist_files.map do |file|
          specimen_data file.data
        end.compact
      end

      def gist_specimen_files_path(gist_files)
        return unless gist_files

        gist_files.map do |file|
          specimen_data file.name
        end.compact
      end

      def gist_comment(gist_comment)
        return if gist_comment.nil?

        {
          id: gist_comment.id,
          global_relay_id: gist_comment.global_relay_id,
          body: gist_comment.body,
          formatter: gist_comment.formatter,
          created_at: gist_comment.created_at,
          updated_at: gist_comment.updated_at,
        }
      end

      def gist_comment_gist_fields(gist_comment)
        return {} unless gist_comment&.gist.present?

        gist = gist_comment.gist
        first_file = gist.files.first

        {
          all_comments_by_gist_author: gist.all_comments_by_author?,
          specimen_gist_description: specimen_data(gist.description),
          specimen_gist_first_path: specimen_data(first_file&.name),
          specimen_gist_content: specimen_data(first_file&.data),
        }
      end

      def hovercard_context_string(subject_str)
        type_s, id = subject_str&.split(":")

        # The lookup against SUBJECT_PREFIX_MAP is intentional to ensure that the type_s is supported
        subject_type = Hovercard::SUBJECT_PREFIX_MAP[type_s]&.model_name&.param_key if type_s

        {
          type: subject_type,
          id: id&.to_i,
        }
      end

      def ip_address(ip, overrides: {})
        ip = Hydro::IpAddr.new(ip)
        return unless ip.valid?

        {
          address: ip.to_s,
          version: ip.version || :UNKNOWN,
          v4_int: ip.ipv4_int,
          v6_int: ip.ipv6_int,
        }.merge(overrides)
      end

      def commit(commit, overrides: {})
        return unless commit&.repository

        {
          global_relay_id: commit.global_relay_id,
          oid: commit.oid,
          repository_id: commit.repository.id,
        }.merge(overrides)
      end

      def commit_comment(comment)
        return unless comment

        {
          id: comment.id,
          global_relay_id: comment.global_relay_id,
          oid: comment.commit&.oid,
          repository_id: comment.repository_id,
          author_id: comment.user_id,
          created_at: comment.created_at,
          updated_at: comment.updated_at,
        }
      end

      def issue(issue, overrides: {})
        return if issue.nil?

        {
          id: issue.id,
          global_relay_id: issue.global_relay_id,
          number: issue.number,
          issue_state: issue.state.upcase,
          body_size: issue.body&.size,
          updated_at: issue.updated_at,
          created_at: issue.created_at,
        }.merge(overrides)
      end

      def issue_domain(issue, overrides: {})
        issue(issue, **overrides)
          .slice(:id, :global_relay_id, :number, :created_at)
          .merge(title: issue.title,
                 body: issue.body,
                 labels: issue.labels.map { |label| label(label) },
                 owner_id: issue.owner&.id)
      end

      def issue_template(template, overrides: {})
        return if template.nil?

        {
          name_hash: Digest::MD5.hexdigest(template.name.to_s),
          title: template.uses?(:title),
          label: template.uses?(:labels),
          assignee: template.uses?(:assignees),
        }.merge(overrides)
      end

      def discussion(discussion, overrides: {})
        return if discussion.nil?

        {
          id: discussion.id,
          global_relay_id: discussion.global_relay_id,
          number: discussion.number,
          body_size: discussion.body&.size || 0,
          updated_at: discussion.updated_at,
          created_at: discussion.created_at,
          comment_count: discussion.comment_count,
          score: discussion.score,
        }.merge(overrides)
      end

      def discussion_comment(discussion_comment, overrides: {})
        return if discussion_comment.nil?

        {
          id: discussion_comment.id,
          global_relay_id: discussion_comment.global_relay_id,
          body_size: discussion_comment.body&.size || 0,
          updated_at: discussion_comment.updated_at,
          created_at: discussion_comment.created_at,
          comment_count: discussion_comment.comment_count,
          score: discussion_comment.score,
        }.merge(overrides)
      end

      def file_extension(path)
        pieces = path.split(".")
        pieces.size > 1 ? pieces.last : ""
      end

      def pull_request(pull_request, overrides: {})
        return if pull_request.nil?

        {
          id: pull_request.id,
          global_relay_id: pull_request.global_relay_id,
          author_id: pull_request.user_id,
          pull_request_state: pull_request.state.upcase,
          created_at: pull_request.created_at,
          updated_at: pull_request.updated_at,
          draft: pull_request.draft,
          total_commits: pull_request.total_commits,
        }.merge(overrides)
      end

      def pull_request_review(review, overrides: {})
        return unless review

        {
          id: review.id,
          pull_request_id: review.pull_request_id,
          user_id: review.user_id,
          state: PullRequestReview.state_name(review.state)&.upcase,
          head_sha: review.head_sha,
          body: review.body,
          created_at: review.created_at,
          updated_at: review.updated_at,
          submitted_at: review.submitted_at,
        }.merge(overrides)
      end

      def issue_comment(issue_comment, overrides: {})
        return if issue_comment.nil?

        {
          id: issue_comment.id,
          global_relay_id: issue_comment.global_relay_id,
          formatter: issue_comment.formatter,
          updated_at: issue_comment.updated_at,
          created_at: issue_comment.created_at,
        }.merge(overrides)
      end

      def pull_request_review_comment(review_comment, overrides: {})
        return if review_comment.nil?

        {
          id: review_comment.id,
          global_relay_id: review_comment.global_relay_id,
          body: review_comment.body,
          contains_suggestion: review_comment.body_contains_suggestion?,
          author_id: review_comment.user_id,
          updated_at: review_comment.updated_at,
          created_at: review_comment.created_at,
          start_position_offset: review_comment.start_position_offset,
        }.merge(overrides)
      end

      def oauth_application(entity, overrides: {})
        return if entity.nil?

        {
          id: entity.id,
          name: entity.name,
          domain: entity.domain,
          url: entity.url,
          callback_url: entity.callback_url,
          owner_id: entity.user_id,
          created_at: entity.created_at,
        }.merge(overrides)
      end

      def profile(profile, overrides: {})
        return if profile.nil?

        status = if profile.association(:user_status).loaded?
          user_status(profile.user_status)
        end

        {
          id: profile.id,
          name: profile.name,
          email: profile.email,
          website_url: profile.blog,
          company: profile.company,
          location: profile.location,
          bio: profile.bio,
          hireable: profile.hireable?,
          display_staff_badge: profile.display_staff_badge?,
          created_at: profile.created_at,
          updated_at: profile.updated_at,
          user_status: status,
        }.merge(overrides)
      end

      def profile_specimen_data_fields(profile)
        return if profile.nil?

        {
          specimen_company: specimen_data(profile.company),
          specimen_location: specimen_data(profile.location),
          specimen_name: specimen_data(profile.name),
          specimen_website_url: specimen_data(profile.blog),
        }
      end

      def request_context(github_context, overrides: {})
        return if github_context.nil?

        ip = Hydro::IpAddr.new(github_context[:actor_ip])

        return unless ip.valid?

        {
          request_id: github_context[:request_id],
          request_method: github_context[:method].try(:upcase),
          request_url: github_context[:url],
          request_category: github_context[:request_category],
          ip_address: ip.to_s,
          ip_version: ip.version || :VERSION_UNKNOWN,
          v4_int: ip.ipv4_int,
          v6_int: ip.ipv6_int,
          user_agent: github_context[:user_agent]&.dup&.force_encoding(Encoding::UTF_8),
          session_id: github_context[:session_id],
          controller: github_context[:controller],
          controller_action: github_context[:controller_action],
          api_route: github_context[:api_route].try(:call),
          from: github_context[:from],
          auth: auth_type(github_context[:auth]) || :AUTH_UNKNOWN,
          client_id: github_context[:client_id],
          referrer: github_context[:referrer]&.dup&.force_encoding(Encoding::UTF_8),
          device_cookie: github_context[:device_cookie]&.dup&.force_encoding(Encoding::UTF_8),
        }.merge(GitHub::Location.look_up(ip.to_s).slice(
          :country_code,
          :country_name,
          :region,
          :region_name,
          :city,
        )).merge(overrides)
      end

      def repository(repository, overrides: {})
        return if repository.nil?

        {
          id: repository.id,
          global_relay_id: repository.global_relay_id,
          name: repository.name,
          description: repository.description,
          visibility: repository.visibility,
          parent_id: repository.parent_id,
          network_id: repository.network_id,
          stargazer_count: repository.stargazer_count,
          public_fork_count: [repository.public_fork_count, 0].max,
          pushed_at: repository.pushed_at,
          updated_at: repository.updated_at,
          created_at: repository.created_at,
          template: repository.template?,
          disk_usage: repository.disk_usage.to_i,
          default_branch: repository.default_branch,
          primary_language_name: repository.primary_language&.name,
          organization_id: repository.organization_id,
          owner_id: repository.owner_id,
          wiki_world_writable: repository.wiki_world_writable?,
        }.merge(overrides)
      end

      def repository_domain(repository, override: {})
        repository(repository, **override).slice(:id, :global_relay_id, :name, :description, :visibility, :parent_id, :default_branch, :created_at, :updated_at).merge(full_name: repository.full_name)
      end

      def changed_file(changed_file, overrides: nil)
        overrides ||= {}
        return if changed_file.nil?

        {
          previous_blob_oid: changed_file.previous_oid,
          current_blob_oid: changed_file.oid,
          change_type: changed_file.change_type,
          current_path: changed_file.path,
          previous_path: changed_file.previous_path,
        }.merge(overrides)
      end

      def repository_advisory(repository_advisory, overrides: {})
        {
          id: repository_advisory.id,
          ghsa_id: repository_advisory.ghsa_id,
          permalink: repository_advisory.permalink,
          severity: enum_from_string(repository_advisory.severity),
          state: repository_advisory.state,
          created_at: repository_advisory.created_at,
          updated_at: repository_advisory.updated_at,
          published_at: repository_advisory.published_at,
          withdrawn_at: repository_advisory.withdrawn_at,
        }.merge(overrides)
      end

      def repository_advisory_content(repository_advisory)
        {
          ghsa_id: repository_advisory.ghsa_id,
          permalink: repository_advisory.permalink,
          severity: enum_from_string(repository_advisory.severity),
          state: repository_advisory.state,
          title: repository_advisory.title,
          description: repository_advisory.description,
          ecosystem: repository_advisory.ecosystem,
          package: repository_advisory.package,
          affected_versions: repository_advisory.affected_versions,
          patches: repository_advisory.patches,
          cve_id: repository_advisory.cve_id,
        }
      end

      # An advisory credit doesn't have a corresponding Hydro _entity_, but the
      # Hydro messages that describe its lifecycle have enough in common that
      # it's helpful to extract the message building logic to one location.
      def advisory_credit(payload, except: [])
        actor = payload[:actor]
        advisory_credit = payload[:advisory_credit]
        repository_advisory = payload[:repository_advisory]
        repository = payload[:repo]
        repository_owner = repository&.owner
        security_advisory = payload[:vulnerability]&.becomes(SecurityAdvisory)
        creator = payload[:creator]
        recipient = payload[:recipient]
        current_state =
          case
          when advisory_credit.accepted? then :ACCEPTED
          when advisory_credit.declined? then :DECLINED
          when advisory_credit.notified? then :PENDING
          else :NOT_NOTIFIED
          end

        message = {
          request_context: request_context(GitHub.context.to_hash),
          advisory_credit_id: advisory_credit.id,
          ghsa_id: advisory_credit.ghsa_id,
          advisory_credit_created_at: advisory_credit.created_at,
          advisory_credit_current_state: current_state,
        }

        message[:actor] = user(actor) if actor
        message[:repository_advisory] = repository_advisory(repository_advisory) if repository_advisory
        message[:repository] = repository(repository) if repository
        message[:repository_owner] = user(repository_owner) if repository_owner
        message[:security_advisory] = security_advisory.hydro_entity_payload if security_advisory
        message[:advisory_credit_creator] = user(creator) if creator
        message[:advisory_credit_recipient] = user(recipient) if recipient

        message.except!(*except)

        message
      end

      def spamurai_form_signals(signals, overrides: {})
        # For case when spamurai form signals is a hash
        # This can happen when it is serialized into kv then pulled
        # back out. When pulled out it turns into a hash.
        if signals.is_a?(Hash) && signals.key?(:request_params) && signals.key?(:current_timestamp)
          signals = SpamuraiFormSignals.new(**signals)
        end

        return if signals.nil?
        return unless signals.is_a?(SpamuraiFormSignals)

        load_to_submit_in_milliseconds = signals.load_to_submit_in_milliseconds && [
          signals.load_to_submit_in_milliseconds,
          2147483647,
        ].min # max size is 32 bit integer

        {
          load_to_submit_in_milliseconds: load_to_submit_in_milliseconds,
          load_to_submit_timestamp_hacked: signals.load_to_submit_timestamp_hacked?,
          load_to_submit_timestamp_missing: signals.load_to_submit_timestamp_missing?,
          load_to_submit_timestamp_secret_missing: signals.load_to_submit_timestamp_secret_missing?,
          honeypot_failure: signals.honeypot_failure?,
        }.merge(overrides)
      end

      def spam_queue(spam_queue, overrides: {})
        return if spam_queue.nil?

        {
          id: spam_queue.id,
          name: spam_queue.name,
          created_at: spam_queue.created_at,
          global_relay_id: spam_queue.global_relay_id,
        }.merge(overrides)
      end

      def spam_queue_entry(spam_queue_entry, overrides: {})
        return if spam_queue_entry.nil?

        {
          id: spam_queue_entry.id,
          created_at: spam_queue_entry.created_at,
          global_relay_id: spam_queue_entry.global_relay_id,
          additional_context: spam_queue_entry.additional_context,
        }.merge(overrides)
      end

      STARRABLE_TYPES = {
        "Gist" => :GIST,
        "Repository" => :REPOSITORY,
      }

      def starrable(entity, overrides: {})
        return if entity.nil?

        visiblity = entity.public? ? :PUBLIC : :PRIVATE
        {
          type: STARRABLE_TYPES.fetch(entity.class.name, :UNKNOWN),
          id: entity.id,
          global_relay_id: entity.global_relay_id,
          visibility: visiblity,
        }.merge(overrides)
      end

      def user(entity, overrides: {})
        return unless entity

        UserEntitySerializer
          .serialize(entity)
          .merge(overrides)
      end

      def user_domain(entity, overrides: {})
        user(entity, **overrides).slice(:id, :login, :type, :global_relay_id, :spamurai_classification, :created_at).tap  do |hash|
          hash[:avatar_url] = entity.try(:avatar_url) || ""
          hash[:site_admin] = Api::Serializer.instance_admin?(entity)
        end
      end

      def user_email(entity, overrides: {})
        return if entity.nil?

        domain_metadata = EmailDomainReputationRecord.metadata(entity.email)
        domain_reputation = EmailDomainReputationRecord.reputation(entity.email)

        {
          id: entity.id,
          address: entity.email,
          verified_at: entity.verified_at,
          domain_reputation: domain_reputation.reputation,
          domain_reputation_lower_bound: domain_reputation.reputation_lower_bound,
          domain_sample_size: domain_reputation.sample_size,
          domain_not_spammy_sample_size: domain_reputation.not_spammy_sample_size,
          associated_domains: domain_metadata[:email_domains],
          associated_mx_exchanges: domain_metadata[:mx_exchanges],
          associated_a_records: domain_metadata[:a_records],
          has_valid_public_suffix: domain_metadata[:has_valid_public_suffix],
        }.merge(overrides)
      end

      def user_status(entity, overrides: {})
        return if entity.nil?

        {
          id: entity.id,
          emoji: entity.emoji,
          message: entity.message,
          user_id: entity.user_id,
          created_at: entity.created_at,
          updated_at: entity.updated_at,
          organization_id: entity.organization_id,
          limited_availability: entity.limited_availability,
          expires_at: entity.expires_at,
        }.merge(overrides)
      end

      def user_spammy_classification(user)
        return nil unless user
        return nil unless user.persisted?
        return :SPAMMY if user.spammy?
        return :HAMMY if user.hammy?
        :NONE
      end

      def user_spammy_reason(user)
        return nil unless user
        return nil unless user.persisted?
        user.spammy_reason || ""
      end

      def user_suspended(user)
        return nil unless user
        return nil unless user.persisted?
        user.suspended?
      end

      def user_currently_deleted(user)
        return true unless user
        return true unless user.persisted?
        false
      end

      def team(team, overrides: {})
        return unless team

        {
          id: team.id,
          global_relay_id: team.global_relay_id,
          organization_id: team.organization_id,
          name: team.name,
          slug: team.slug,
          description: team.description,
          created_at: team.created_at,
          updated_at: team.updated_at,
        }.merge(overrides)
      end

      def organization(entity, overrides: {})
        return unless entity

        spamurai_classification = if entity.spammy?
          :SPAMMY
        elsif entity.hammy?
          :HAMMY
        else
          :SPAMURAI_CLASSIFICATION_UNKNOWN
        end

        {
          id: entity.id,
          login: entity.login.to_s,
          billing_plan: entity.plan.try(:display_name),
          spammy: entity.spammy?,
          suspended: entity.suspended?,
          spamurai_classification: spamurai_classification,
          global_relay_id: entity.global_relay_id,
          created_at: entity.created_at,
        }.merge(overrides)
      end

      def invitation(entity, overrides: {})
        return unless entity

        {
          created_at: entity.created_at,
          email: entity.email,
          id: entity.id,
          invitee_id: entity.invitee_id,
          inviter_id: entity.inviter_id,
          role: entity.role,
          updated_at: entity.updated_at,
        }.merge(overrides)
      end

      def business(entity, overrides: {})
        return unless entity

        {
          id: entity.id,
          slug: entity.slug.to_s,
          global_relay_id: entity.global_relay_id,
          created_at: entity.created_at,
        }.merge(overrides)
      end

      TEAM_SYNC_TENANT_PROVIDER_TYPES = {
        unknown: :UNKNOWNPROVIDER,
        azuread: :AZUREAD,
        okta: :OKTA,
      }

      TEAM_SYNC_TENANT_STATES = {
        unknown: :UNKNOWN,
        pending: :PENDING,
        failed: :FAILED,
        ready: :READY,
        enabled: :ENABLED,
        disabled: :DISABLED,
      }

      def team_sync_tenant(entity)
        return unless entity

        {
          id: entity.id,
          global_relay_id: entity.global_relay_id,
          provider_type: TEAM_SYNC_TENANT_PROVIDER_TYPES[entity.provider_type.to_sym],
          provider_id: entity.provider_id,
          status: team_sync_tenant_status_mapping(entity.status),
          created_at: entity.created_at,
          updated_at: entity.updated_at,
          organization_id: entity.organization_id,
        }
      end

      def team_sync_tenant_status_mapping(status)
        TEAM_SYNC_TENANT_STATES[status.to_sym]
      end

      TEAM_GROUP_MAPPING_STATES = {
        unknown: :UNKNOWN,
        unsynced: :UNSYNCED,
        pending: :PENDING,
        synced: :SYNCED,
        disabled: :DISABLED,
        failed: :FAILED,
      }

      def team_group_mapping(entity)
        return unless entity

        {
          id: entity.id,
          global_relay_id: entity.global_relay_id,
          group_id: entity.group_id,
          group_name: entity.group_name,
          group_description: entity.group_description,
          status: TEAM_GROUP_MAPPING_STATES[entity.status.to_sym],
          synced_at: entity.synced_at,
          created_at: entity.created_at,
          updated_at: entity.updated_at,
          team_id: entity.team_id,
          organization_id: entity.tenant.organization_id,
          team_sync_tenant_id: entity.tenant_id,
        }
      end

      USER_LICENSE_TYPES = {
        volume: :VOLUME,
        enterprise: :ENTERPRISE,
      }

      def user_license(user_license)
        {
          id: user_license.id,
          email: user_license.email,
          user_id: user_license.user_id,
          business_id: user_license.business_id,
          license_type: USER_LICENSE_TYPES[user_license.license_type.to_sym],
        }
      end

      def enterprise_installation(installation)
        {
          id: installation.id,
          host_name: installation.host_name,
          customer_name: installation.customer_name,
          owner_type: installation.owner_type == "Business" ? :BUSINESS : :ORG,
          owner_id: installation.owner_id,
        }
      end

      def news_feed_event_type_to_hydro(event_type)
        event_type = event_type.chomp("Event").underscore.upcase.to_sym
        NEWS_FEED_EVENT_TYPES.include?(event_type) ? event_type : :UNKNOWN
      end

      def news_feed_event(entity, overrides: {})
        return unless entity

        visibility =  if entity[:public]
          :VISIBILITY_PUBLIC
        else
          :VISIBILITY_PRIVATE
        end

        type = news_feed_event_type_to_hydro(entity[:type])
        {
          id: entity[:id],
          repo_id: entity[:repo_id],
          type: type,
          actor_id: entity[:actor_id],
          target_id: entity[:target_id],
          visibility: visibility,
          additional_details_shown: entity[:additional_details_shown] == true,
          grouped: entity[:grouped],
        }.merge(overrides)
      end

      def news_feed_event_group(entity, overrides: {})
        return unless entity

        type = news_feed_event_type_to_hydro(entity[:type])
        {
          quantity: entity[:quantity],
          type: type,
          includes_viewer: entity[:includes_viewer],
        }
      end

      def project(entity, overrides: {})
        return unless entity

        types = { organization: "ORG", repository: "REPO", user: "USER" }
        type = types.fetch(entity.owner_type.downcase.to_sym, "TYPE_UNKNOWN")

        serialized = {
          id: entity.id,
          global_relay_id: entity.global_relay_id,
          type: type,
          created_at: entity.created_at,
          updated_at: entity.updated_at,
          visibility: entity.public? ? "PUBLIC" : "PRIVATE",
        }

        if entity.public?
          serialized[:name] = entity.name
          serialized[:description] = entity.body
        end

        serialized
      end

      def project_card(entity, overrides: {})
        content_type = if entity.is_note?
          "NOTE"
        elsif content = entity.content
          content.pull_request? ? "PULL_REQUEST" : "ISSUE"
        else
          nil
        end

        {
          content_type: content_type,
        }
      end

      def project_column(entity, overrides: {})
        return unless entity

        {
          id: entity.id,
          name: entity.name,
        }
      end

      def enum_from_string(entity)
        return unless entity

        entity&.underscore&.upcase&.to_sym
      end

      def graphql_accessed_object(graphql_object)
        application_object = graphql_object.object
        graphql_defn = graphql_object.class
        {
          database_id: application_object.respond_to?(:id) ? application_object.id : nil,
          ruby_class_name: application_object.class.name,
          global_relay_id: application_object.respond_to?(:global_relay_id) ? application_object.global_relay_id : nil,
          graphql_type_name: graphql_defn.graphql_name,
        }
      end

      def content_reference_content_context(entity)
        if entity.is_a?(PullRequestReviewComment)
          return :PR_COMMENT
        elsif entity.is_a?(IssueComment)
          return :ISSUE_COMMENT
        elsif entity.is_a?(Issue) && !entity.pull_request?
          return :ISSUE_BODY
        elsif entity.is_a?(Issue) && entity.pull_request?
          return :PR_BODY
        else
          return :CONTENT_CONTEXT_UNKNOWN
        end
      end

      def branch_protection_rule(entity, overrides: {})
        return unless entity

        {
          id: entity.id,
          required_linear_history: entity.required_linear_history_enabled?,
          allowed_force_pushes: !entity.block_force_pushes_enabled?,
          allowed_deletions: !entity.block_deletions_enabled?,
          global_relay_id: entity.global_relay_id,
          pattern: entity.name_for_display,
        }.merge(overrides)
      end

      AVATAR_OWNER_TYPES = {
        "Integration" => :INTEGRATION,
        "NonMarketplaceListing" => :NON_MARKETPLACE_LISTING,
        "Marketplace::Listing" => :MARKETPLACE_LISTING,
        "OauthApplication" => :OAUTH_APPLICATION,
        "Team" => :TEAM,
        "Business" => :BUSINESS,
        "User" => :USER,
      }

      def avatar(avatar, overrides: {})
        return if avatar.nil?

        {
          id: avatar.id,
          asset_id: avatar.asset_id,
          content_type: avatar.content_type,
          created_at: avatar.created_at,
          owner_id: avatar.owner_id,
          owner_type: AVATAR_OWNER_TYPES.fetch(avatar.owner_type, :UNKNOWN),
          cropped_x: avatar.cropped_x,
          cropped_y: avatar.cropped_y,
          cropped_width: avatar.cropped_width,
          cropped_height: avatar.cropped_height,
          uploader_id: avatar.uploader_id,
          updated_at: avatar.updated_at,
          storage_blob_id: avatar.storage_blob_id,
          oid: avatar.oid,
          size: avatar.size,
          width: avatar.width,
          height: avatar.height,
          storage_provider: avatar.storage_provider,
        }.merge(overrides)
      end

      def page_status(status)
        case status
        when "built"
          :BUILT
        when "building"
          :BUILDING
        when "errored"
          :ERRORED
        else
          :BUILD_STATUS_UNKNOWN
        end
      end

      def page(page)
        return if page.nil?

        {
          id: page.id,
          repository: repository(page.repository),
          cname: page.cname,
          has_custom_404: page.four_oh_four,
          status: page_status(page.status),
          built_revision: page.built_revision,
          source_ref_name: page.source_ref_name,
          source_subdir: page.source_subdir,
          https_redirect: page.https_redirect,
          hsts_max_age: page.hsts_max_age.to_i,
          hsts_include_sub_domains: page.hsts_include_sub_domains,
          hsts_preload: page.hsts_preload,
        }
      end

      def payment_method(entity)
        return unless entity

        payment_instrument_type = \
          case
          when entity.credit_card?
            :CREDIT_CARD
          when entity.paypal?
            :PAYPAL
          else
            :PAYMENT_INSTRUMENT_TYPE_UNKNOWN
          end

        {
          id: entity.id,
          payment_instrument_type: payment_instrument_type,
          credit_card_bin: entity.bank_identification_number,
          credit_card_unique_id: entity.unique_number_identifier,
          paypal_email: entity.paypal_email,
        }
      end

      def pull_request_refresh_tab_context(tab_context)
        return :UNKNOWN unless %w(conversation checks files_changed).include?(tab_context)

        return tab_context.upcase.to_sym
      end

      def label(entity)
        return unless entity
        {
          id: entity.id,
          name: entity.name,
          color: entity.color,
          repository_id: entity.repository_id,
          description: entity.description,
          created_at: entity.created_at,
          updated_at: entity.updated_at,
        }
      end

      def contact_link(entity)
        return unless entity

        {
          name: entity["name"],
          about: entity["about"],
          url: entity["url"],
        }
      end

      def package(pkg, total_size: nil, version_count: nil)
        return unless pkg
        {
          id: pkg.id,
          global_id: pkg.global_relay_id,
          name: pkg.name,
          registry_type: pkg.package_type.to_s.upcase,
          total_size: total_size.to_i,
          version_count: version_count.to_i,
          owner_id: pkg.repository&.owner_id&.to_i,
        }.tap do |msg|
          next unless pkg.association(:repository).loaded? && pkg.repository

          msg[:repository] = repository(pkg.repository)
          msg[:visibility] = msg[:repository][:visibility]

          case pkg.repository.owner
          when ::Organization
            msg[:owner_org] = organization(pkg.repository.owner)
          else
            msg[:owner_user] = user(pkg.repository.owner)
          end
        end
      end

      def package_version(version, files_count: nil, size: nil)
        return unless version
        {
          id: version.id,
          global_id: version.global_relay_id,
          version: version.version,
          platform: version.platform,
          package_size: size || version.size,
          files_count: files_count.to_i,
          pre_release: version.pre_release,
        }.tap do |msg|
          next unless version.association(:package).loaded? && version.package

          msg[:package_id] = version.package.id
          msg[:package_global_id] = version.package.global_relay_id
        end
      end

      def package_file(file)
        return unless file
        {
          id: file.id,
          global_id: file.global_relay_id,
          name: file.filename,
          guid: file.guid,
          size: file.size,
        }.tap do |msg|
          next unless file.association(:package_version).loaded? && file.package_version

          msg[:version_id] = file.package_version.id
          msg[:version_global_id] = file.package_version.global_relay_id
        end
      end

      def package_file_storage_service(service)
        return unless service.present?

        case service.to_s.upcase
        when "AWS_S3", "S3"
          { name: "AWS_S3" }
        when "FASTLY"
          { name: "FASTLY" }
        else
          { name: "N_A" }
        end
      end

      def user_asset(user_asset, overrides: {})
        {
          id: user_asset.id,
          user_id: user_asset.user_id,
          name: user_asset.name,
          content_type: user_asset.content_type,
          size: user_asset.size,
          guid: user_asset.guid,
          state: user_asset.state.upcase.to_sym,
          storage_blob_id: user_asset.storage_blob_id,
          oid: user_asset.oid,
          storage_provider: user_asset.storage_provider,
          storage_external_url: user_asset.storage_external_url,
          created_at: user_asset.created_at,
          updated_at: user_asset.updated_at,
        }.merge(overrides)
      end

      def feature(feature)
        return if feature.nil?

        {
          id: feature.id,
          public_name: feature.public_name,
          slug: feature.slug,
          flipper_feature_id: feature.flipper_feature_id,
          enrolled_by_default: feature.enrolled_by_default?,
          created_at: feature.created_at,
        }
      end

      def ip_allow_list_entry(entry)
        return unless entry.present?

        {
          id: entry.id,
          owner_type: entry.owner.is_a?(Business) ? :BUSINESS : :ORG,
          owner_id: entry.owner.id,
          allow_list_value: entry.whitelisted_value,
          name: entry.name,
          active: entry.active?,
        }
      end

      def ip_allow_list_entries(entries)
        return [] unless entries.present?

        entries.each.map { |entry| ip_allow_list_entry(entry) }
      end

      def spam_reputation(reputation)
        {
          sample_size: reputation.sample_size,
          not_spammy_sample_size: reputation.not_spammy_sample_size,
          spammy_sample_size: reputation.spammy_sample_size,
          reputation: reputation.reputation,
          reputation_lower_bound: reputation.reputation_lower_bound,
          plus_minus: reputation.plus_minus,
          maximum_reputation: reputation.maximum_reputation,
          minimum_reputation: reputation.minimum_reputation,
        }
      end

      def codespace(codespace, overrides: {})
        return if codespace.nil?

        {
          id: codespace.id,
          guid: codespace.guid,
          repository_id: codespace.repository_id,
          owner_id: codespace.owner_id,
          pull_request_id: codespace.pull_request_id,
          oid: codespace.oid,
          ref: codespace.ref,
          location: codespace.location,
          vso_environment: Codespaces.vscs_target(codespace.owner).to_s,
          name: codespace.name,
        }.merge(overrides)
      end

      def codespace_compute_usage(compute_usage, overrides: {})
        {
          owner_id: compute_usage.owner_id,
          actor_id: compute_usage.actor_id,
          billable_duration_in_seconds: compute_usage.billable_duration_in_seconds,
          start_time: compute_usage.start_time,
          end_time: compute_usage.end_time,
          unique_billing_identifier: compute_usage.unique_billing_identifier,
          repository: repository(compute_usage.repository),
          sku: compute_usage.billing_sku,
          computed_usage: compute_usage.computed_usage.to_f,
        }.merge(overrides)
      end

      def codespace_storage_usage(storage_usage, overrides: {})
        {
            owner_id: storage_usage.owner_id,
            actor_id: storage_usage.actor_id,
            billable_duration_in_seconds: storage_usage.billable_duration_in_seconds,
            start_time: storage_usage.start_time,
            end_time: storage_usage.end_time,
            unique_billing_identifier: storage_usage.unique_billing_identifier,
            repository: repository(storage_usage.repository),
            sku: storage_usage.billing_sku,
            size_in_bytes: storage_usage.size_in_bytes,
            computed_usage: storage_usage.computed_usage.to_f,
        }.merge(overrides)
      end

      def sponsors_fraud_review(fraud_review)
        return if fraud_review.blank?

        {
          id: fraud_review.id,
          state: fraud_review.state.to_s,
        }
      end
    end
  end
end
