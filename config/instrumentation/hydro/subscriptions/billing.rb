# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("billing.change_billing_type") do |payload|
    message = {
      account: serializer.user(payload[:user]),
      current_billing_type: serializer.billing_type(payload[:billing_type]),
      previous_billing_type: serializer.billing_type(payload[:old_billing_type]),
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.v1.BillingTypeChange")
  end

  subscribe("billing.free_trial_conversion") do |payload|
    actor = User.find_by(id: payload[:actor_id])
    user = User.find_by(id: payload[:user_id])
    listing_plan = Marketplace::ListingPlan.find_by(id: payload[:listing_plan_id])
    product_uuid = listing_plan&.product_uuid(user&.plan_duration)

    message = {
      actor: serializer.user(actor),
      resource: "BillingProduct",
      resource_action: "free_trial_conversion",
      resource_quantity_delta: 0,
      resource_quantity_total: payload[:quantity].to_i,
      target_entity: {
        entity_type: :BILLING_PRODUCT,
        entity_id: product_uuid&.id,
      },
      target_entity_owner: serializer.user(user),
      request_context: serializer.request_context(GitHub.context.to_hash),
    }
    publish(message, schema: "github.v1.UserBehavior")
  end

  subscribe("billing.lfs_change") do |payload|
    actor = User.find_by(id: payload[:actor_id])
    user = User.find_by(id: payload[:user_id])
    lfs_product_uuid = Asset::Status.product_uuid(user&.plan_duration)

    delta = payload[:new_lfs_count].to_i - payload[:old_lfs_count].to_i

    resource_action = \
      if payload[:new_lfs_count] == 0
        "delete"
      elsif payload[:old_lfs_count] == 0
        "create"
      else
        "change_quantity"
      end

    message = {
      actor: serializer.user(actor),
      resource: "BillingProduct",
      resource_action: resource_action,
      resource_quantity_delta: delta,
      resource_quantity_total: payload[:new_lfs_count]&.to_i,
      target_entity: {
        entity_type: :BILLING_PRODUCT,
        entity_id: lfs_product_uuid&.id,
      },
      target_entity_owner: serializer.user(user),
      request_context: serializer.request_context(GitHub.context.to_hash),
    }
    publish(message, schema: "github.v1.UserBehavior")
  end

  subscribe("billing.metered_line_item_submitted") do |payload|
    message = {
      metered_product: payload[:metered_product],
      line_item_id: payload[:line_item_id],
      synchronization_batch_id: payload[:synchronization_batch_id],
    }
    publish(message, schema: "github.billing.v0.MeteredLineItemSubmitted")
  end

  subscribe("billing.payment_method.addition") do |payload|
    actor = User.find_by(id: payload[:actor_id])
    account = User.find_by(id: payload[:account_id])
    payment_method = PaymentMethod.find_by(id: payload[:payment_method_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(actor),
      account: serializer.user(account),
      payment_method: serializer.payment_method(payment_method),
    }

    publish(message, schema: "github.billing.v0.PaymentMethodAddition")
  end

  subscribe("billing.payment_transaction") do |payload|
    account = User.find(payload[:account_id])
    payment_method = PaymentMethod.find(payload[:payment_method_id])

    message = {
      account: serializer.user(account),
      payment_method: serializer.payment_method(payment_method),
      success: payload[:success],
      payment_amount_in_cents: payload[:payment_amount],
      attempt_number: payload[:attempt_number],
      processor_response_code: payload[:processor_response_code],
      processor_response: payload[:processor_response],
    }

    publish(message, schema: "github.billing.v0.PaymentTransaction")
  end

  subscribe("billing.plan_change") do |payload|
    actor = User.find_by(id: payload[:actor_id])
    user = User.find_by(id: payload[:user_id])
    old_plan = GitHub::Plan.find(payload[:old_plan_name])
    old_plan_product_uuid = old_plan&.product_uuid(user&.plan_duration)
    new_plan = GitHub::Plan.find(payload[:new_plan_name])
    new_plan_product_uuid = new_plan&.product_uuid(user&.plan_duration)

    if old_plan_product_uuid
      old_plan_seat_delta = 0 - payload[:old_seat_count].to_i

      product_delete_message = {
        actor: serializer.user(actor),
        resource: "BillingProduct",
        resource_action: "delete",
        resource_quantity_delta: old_plan_seat_delta,
        resource_quantity_total: 0,
        target_entity: {
          entity_type: :BILLING_PRODUCT,
          entity_id: old_plan_product_uuid.id,
        },
        target_entity_owner: serializer.user(user),
        request_context: serializer.request_context(GitHub.context.to_hash),
        user_first_name: payload[:user_first_name],
        user_last_name: payload[:user_last_name],
      }
      publish(product_delete_message, schema: "github.v1.UserBehavior")
    end

    if new_plan_product_uuid
      product_create_message = {
        actor: serializer.user(actor),
        resource: "BillingProduct",
        resource_action: "create",
        resource_quantity_delta: payload[:new_seat_count].to_i,
        resource_quantity_total: payload[:new_seat_count].to_i,
        target_entity: {
          entity_type: :BILLING_PRODUCT,
          entity_id: new_plan_product_uuid.id,
        },
        target_entity_owner: serializer.user(user),
        request_context: serializer.request_context(GitHub.context.to_hash),
        user_first_name: payload[:user_first_name],
        user_last_name: payload[:user_last_name],
      }
      publish(product_create_message, schema: "github.v1.UserBehavior")
    end
  end

  subscribe("billing.redeem_coupon") do |payload|
    actor = User.find_by(id: payload[:actor_id])
    user = User.find_by(id: payload[:user_id])

    message = {
      actor: serializer.user(actor),
      resource: "CouponRedemption",
      resource_action: "create",
      target_entity: {
        entity_type: :COUPON,
        entity_id: payload[:coupon_id],
      },
      target_entity_owner: serializer.user(user),
      request_context: serializer.request_context(GitHub.context.to_hash),
    }
    publish(message, schema: "github.v1.UserBehavior")
  end

  subscribe("billing.seat_count_change") do |payload|
    actor = User.find_by(id: payload[:actor_id])
    user = User.find_by(id: payload[:user_id])
    plan_product_uuid = user&.plan&.product_uuid(user&.plan_duration)

    delta = payload[:new_seat_count].to_i - payload[:old_seat_count].to_i

    message = {
      actor: serializer.user(actor),
      resource: "BillingProduct",
      resource_action: "change_quantity",
      resource_quantity_delta: delta,
      resource_quantity_total: payload[:new_seat_count]&.to_i,
      target_entity: {
        entity_type: :BILLING_PRODUCT,
        entity_id: plan_product_uuid&.id,
      },
      target_entity_owner: serializer.user(user),
      request_context: serializer.request_context(GitHub.context.to_hash),
    }
    publish(message, schema: "github.v1.UserBehavior")
  end

  subscribe("billing.serialized_metered_line_item") do |payload|
    message = {
      metered_product: payload[:metered_product],
      line_item_id: payload[:line_item_id],
      csv_data: payload[:csv_data],
    }
    publish(message, schema: "github.billing.v0.SerializedMeteredLineItem")
  end

  subscribe("billing.subscription_item_change") do |payload|
    actor = User.find_by(id: payload[:actor_id])
    user = User.find_by(id: payload[:user_id])

    old_subscribable = payload[:old_subscribable]
    next if old_subscribable.is_a?(SponsorsTier)

    new_subscribable = payload[:new_subscribable]
    next if new_subscribable.is_a?(SponsorsTier)

    old_product_uuid = old_subscribable&.product_uuid(user&.plan_duration)
    new_product_uuid = new_subscribable&.product_uuid(user&.plan_duration)

    if old_product_uuid
      old_quantity_delta = 0 - payload[:old_quantity].to_i

      delete_message = {
        actor: serializer.user(actor),
        resource: "BillingProduct",
        resource_action: "delete",
        resource_quantity_delta: old_quantity_delta,
        resource_quantity_total: 0,
        target_entity: {
          entity_type: :BILLING_PRODUCT,
          entity_id: old_product_uuid&.id,
        },
        target_entity_owner: serializer.user(user),
        request_context: serializer.request_context(GitHub.context.to_hash),
      }
      publish(delete_message, schema: "github.v1.UserBehavior")
    end

    if new_product_uuid && payload[:new_quantity].to_i.positive?
      create_message = {
        actor: serializer.user(actor),
        resource: "BillingProduct",
        resource_action: "create",
        resource_quantity_delta: payload[:new_quantity].to_i,
        resource_quantity_total: payload[:new_quantity].to_i,
        target_entity: {
          entity_type: :BILLING_PRODUCT,
          entity_id: new_product_uuid&.id,
        },
        target_entity_owner: serializer.user(user),
        request_context: serializer.request_context(GitHub.context.to_hash),
      }
      publish(create_message, schema: "github.v1.UserBehavior")
    end
  end

  subscribe("billing.subscription_item_quantity_change") do |payload|
    actor = User.find_by(id: payload[:actor_id])
    user = User.find_by(id: payload[:user_id])

    subscribable = payload[:subscribable]
    next if subscribable.is_a?(SponsorsTier)

    product_uuid = subscribable&.product_uuid(user&.plan_duration)

    delta = payload[:new_quantity].to_i - payload[:old_quantity].to_i

    message = {
      actor: serializer.user(actor),
      resource: "BillingProduct",
      resource_action: "change_quantity",
      resource_quantity_delta: delta,
      resource_quantity_total: payload[:new_quantity].to_i,
      target_entity: {
        entity_type: :BILLING_PRODUCT,
        entity_id: product_uuid&.id,
      },
      target_entity_owner: serializer.user(user),
      request_context: serializer.request_context(GitHub.context.to_hash),
    }
    publish(message, schema: "github.v1.UserBehavior")
  end

  subscribe("browser.billing.upgrade.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      target: payload[:target],
    }

    publish(message, schema: "github.v1.BillingUpgradeClick")
  end

  subscribe("browser.payment_method.verification_failure") do |payload|
    target = User.find_by(id: payload[:account_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      account: serializer.user(target),
    }

    publish(message, schema: "github.billing.v0.PaymentMethodVerificationFailure")
  end

  subscribe("trial.extended") do |payload|
    message = {
      account: serializer.user(payload[:account]),
      previous_duration_in_days: payload[:previous_duration_in_days],
      new_duration_in_days: payload[:new_duration_in_days],
    }

    publish(message, schema: "github.billing.v0.TrialExtended")
  end

  subscribe("trial.plan_change") do |payload|
    message = {
      account: serializer.user(payload[:account]),
      trial_plan: payload[:trial_plan],
      new_plan: payload[:new_plan],
      trial_expired: payload[:trial_expired],
      user_initiated: payload[:user_initiated],
    }

    publish(message, schema: "github.billing.v0.TrialPlanChange")
  end

  subscribe("trial.signup") do |payload|
    message = {
      account: serializer.user(payload[:account]),
      previous_plan: payload[:previous_plan],
      current_plan: payload[:current_plan],
    }

    publish(message, schema: "github.billing.v0.TrialSignup")
  end

  subscribe("business_licenses.snapshot") do |payload|
    business = payload[:business]

    message = ActiveRecord::Base.connected_to(role: :reading) do
      {
        request_context: serializer.request_context(GitHub.context.to_hash),
        business: serializer.business(business),
        max_enterprise_seats: business.seats,
        filled_enterprise_seats: business.unique_enterprise_users_count,
        max_volume_seats: business.volume_license_cap,
        filled_volume_seats: business.unique_volume_users_count
      }
    end

    publish(message, schema: "github.billing.v0.LicenseSnapshot")
  end
end
