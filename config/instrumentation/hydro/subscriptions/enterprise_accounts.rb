# frozen_string_literal: true

# Hydro event subscriptions related to enterprise accounts.
Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("enterprise_account.profile_view") do |payload|
    enterprise = payload[:enterprise]
    actor = payload[:actor]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      enterprise: serializer.business(enterprise),
      actor: serializer.user(actor),
      actor_is_owner: enterprise&.owner?(actor),
      actor_is_billing_manager: enterprise&.billing_manager?(actor),
      actor_is_organization_member: enterprise&.user_is_member_of_owned_org?(actor),
    }

    publish(message, schema: "github.enterprise_account.v0.ProfileView")
  end

  subscribe("browser.enterprise_account.profile_organization_click") do |payload|
    enterprise = Business.find_by id: payload[:enterprise_id]
    organization = Organization.find_by id: payload[:organization_id]
    actor = User.find_by id: payload[:actor_id]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      enterprise: serializer.business(enterprise),
      organization: serializer.organization(organization),
      actor: serializer.user(actor),
      actor_is_owner: enterprise&.owner?(actor),
      actor_is_billing_manager: enterprise&.billing_manager?(actor),
      actor_is_organization_member: enterprise&.user_is_member_of_owned_org?(actor),
    }

    publish(message, schema: "github.enterprise_account.v0.ProfileOrganizationClick")
  end
end
