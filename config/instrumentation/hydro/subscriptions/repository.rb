# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("browser.repository_empty_state.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:client][:user]),
      target: payload[:event_target],
      repository_context: payload[:repository_context],
    }

    publish(message, schema: "github.v1.RepositoryEmptyStateClick")
  end

  subscribe("repository.missing_branch_redirect") do |payload|
    repo = payload[:repository]

    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      missing_branch_was_master: payload[:missing_branch_was_master],
    }

    if repo.public?
      message[:spamurai_form_signals] = serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals])
    end

    publish(message.freeze, schema: "github.repositories.v1.MissingBranchRedirect",
      publisher: GitHub.low_latency_hydro_publisher)
  end
end
