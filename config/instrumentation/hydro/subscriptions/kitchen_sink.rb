# frozen_string_literal: true

# These are uncategorized Hydro event subscriptions.
# To make this more manageable and provide better ownership tracking:
# 1. Create a new file in `config/instrumentation/hydro/subscriptions` with a
#    `Hydro::EventForwarder.configure(source: GlobalInstrumenter)` block like below.
# 2. Add new or move existing subscriptions to that file.
# 3. Include your subscriptions with a `require_relative` statement for your new file
#    at the bottom of `config/instrumentation/hydro.rb`
# 4. Add an ownership line for your new file in CODEOWNERS under "Hydro Subscriptions"

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("platform.query") do |payload, event_name|
    origin = case payload[:origin]
    when Platform::ORIGIN_INTERNAL
      :INTERNAL
    when Platform::ORIGIN_API
      :GRAPHQL_API
    when Platform::ORIGIN_REST_API
      :REST_API
    when Platform::ORIGIN_MANUAL_EXECUTION
      :MANUAL_EXECUTION
    else
      :ORIGIN_UNKNOWN
    end

    target = case payload[:target]
    when :public
      :PUBLIC_SCHEMA
    when :internal
      :INTERNAL_SCHEMA
    else
      :TARGET_UNKOWN
    end

    operation_type = case payload[:operation_type]
    when "query"
      :QUERY
    when "mutation"
      :MUTATION
    when "subscription"
      :SUBSCRIPTION
    else
      :UNKNOWN_OPERATION_TYPE
    end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      app: serializer.app(payload[:app]),
      viewer: serializer.user(payload[:viewer]),
      query_string: payload[:query_string],
      origin: origin,
      target: target,
      schema_version: payload[:schema_version],
      dotcom_sha: payload[:dotcom_sha],
      selected_operation: payload[:selected_operation],
      operation_type: operation_type,
      valid: payload[:valid],
      errors: payload[:errors],
      accessed_objects: payload[:accessed_objects].map { |o| serializer.graphql_accessed_object(o) },
      query_hash: payload[:query_hash],
      variables_hash: payload[:variables_hash],
      cpu_time_ms: payload[:cpu_time_ms],
      idle_time_ms: [payload[:idle_time_ms].to_i, 0].max,
      mysql_count: payload[:mysql_count],
      mysql_time_ms: payload[:mysql_time_ms],
      gitrpc_count: payload[:gitrpc_count],
      gitrpc_time_ms: payload[:gitrpc_time_ms],
      timed_out: payload[:timed_out],
      node_count: payload[:node_count],
      complexity_cost: payload[:complexity_cost],
    }

    publish(message, schema: "github.v1.GraphQLQuery")
  end

  subscribe(/staff\.(mark_as_spammy|mark_not_spammy)/) do |payload, event_name|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      account: serializer.user(payload[:account]),
      previously_spammy: payload[:previously_spammy],
      currently_spammy: payload[:currently_spammy],
      previous_spammy_reason: payload[:previous_spammy_reason],
      current_spammy_reason: payload[:reason],
      spammy_classifier_type: payload[:spammy_classifier_type],
      spammy_classifier_id: payload[:spammy_classifier_id],
      matched_regexp: payload[:matched_regexp],
      subject: payload[:subject],
      classification: payload[:classification],
      origin: payload[:origin],
    }

    publish(message, schema: "github.v1.AccountClassification")
  end

  subscribe(/spam_queue_entry\.(create|move|drop|resolved)/) do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      spam_queue_entry: serializer.spam_queue_entry(payload[:spam_queue_entry]),
      current_spam_queue: serializer.spam_queue(payload[:current_spam_queue]),
      previous_spam_queue: serializer.spam_queue(payload[:previous_spam_queue]),
      event_type: payload[:event_type],
      account: serializer.user(payload[:account]),
      resolution: payload[:resolution],
      spammy_reason: payload[:spammy_reason],
      queued_time_in_seconds: payload[:queued_time_in_seconds],
      origin: payload[:origin],
    }

    publish(message, schema: "github.v1.SpamQueueEntry")
  end

  subscribe(/abuse_classification\.publish/) do |payload|
    # If this is nil, implicitly this mean previous_* and current_* are the same
    # In cases where previous_* is different, we grab that value before the
    # user is updated and send that value here to be instrumented.
    previous_classification = payload[:serialized_previous_classification]
    previous_classification = serializer.user_spammy_classification(payload[:account]) if previous_classification.nil?
    previous_spammy_reason = payload[:serialized_previous_spammy_reason]
    previous_spammy_reason = serializer.user_spammy_reason(payload[:account]) if previous_spammy_reason.nil?
    previously_suspended = payload[:serialized_previously_suspended]
    previously_suspended = serializer.user_suspended(payload[:account]) if previously_suspended.nil?

    origin = payload[:origin]
    origin ||= Spam::Origin.call(origin)
    origin = origin.upcase

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      account: serializer.user(payload[:account]),
      previous_classification: previous_classification,
      current_classification: serializer.user_spammy_classification(payload[:account]),
      previous_spammy_reason: previous_spammy_reason,
      current_spammy_reason: serializer.user_spammy_reason(payload[:account]),
      previously_suspended: previously_suspended,
      currently_suspended: serializer.user_suspended(payload[:account]),
      currently_deleted: serializer.user_currently_deleted(payload[:account]),
      origin: origin,
      queue_action: payload[:queue_action],
      queue_entry: serializer.spam_queue_entry(payload[:queue_entry]),
      previous_queue: serializer.spam_queue(payload[:previous_queue]),
      current_queue: serializer.spam_queue(payload[:current_queue]),
      queued_time_in_seconds: payload[:queued_time_in_seconds] && payload[:queued_time_in_seconds].to_i,
      rule_name: payload[:rule_name],
      rule_version: payload[:rule_version],
    }

    publish(message, schema: "github.v1.AbuseClassification")
  end

  subscribe(/add_account_to_spamurai_queue/) do |payload|
    message = payload.slice(:account_global_relay_id, :additional_context, :queue_global_relay_id, :origin)
    publish(message, schema: "hamzo.v1.AddToQueue")
  end

  subscribe(/repository_secret\.(create|remove)/) do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
      actor: serializer.user(payload[:user]),
      name: payload[:name],
      state: payload[:state],
    }

    publish(message, schema: "github.v1.RepositorySecretChanged")
  end

  subscribe(/issue\.(pinned|unpinned)/) do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
      actor: serializer.user(payload[:actor]),
      state: payload[:state],
      issue: serializer.issue(payload[:issue]),
    }
     publish(message, schema: "github.v1.IssuePinUpdated")
  end

  subscribe("issue.deleted") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
      actor: serializer.user(payload[:actor]),
      issue: serializer.issue(payload[:issue]),
    }
     publish(message, schema: "github.v1.IssueDeleted")
  end

  subscribe("repository_image.created") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      uploader: serializer.user(payload[:uploader]),
      repository_image: serializer.repository_image(payload[:repository_image]),
      repository: serializer.repository(payload[:repository]),
    }

    publish(message, schema: "github.v1.RepositoryImageCreate")
  end

  subscribe("repository_image.deleted") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      destroyed_repository_image: serializer.repository_image(payload[:repository_image]),
    }

    publish(message, schema: "github.v1.RepositoryImageDestroy")
  end

  subscribe(/package\.(published|downloaded)/) do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      action: payload[:action].to_s.upcase.to_sym,
      registry_package_id: payload[:registry_package_id],
      package_version: payload[:version],
      package_size: payload[:package_size],
      user_agent: payload[:user_agent],
    }

    publish(message, schema: "github.package_registry.v0.PackageEvent")
  end

  subscribe("package_registry.package_version_deleted") do |payload|
    request_context = GitHub.context.to_hash
    message = {
      request_context: serializer.request_context(request_context),
      actor: serializer.user(payload[:actor]),
      package: serializer.package(payload[:package]),
      version: serializer.package_version(payload[:version], files_count: payload[:files_count], size: payload[:size]),
      deleted_at: payload[:deleted_at],
      storage_service: serializer.package_file_storage_service(payload[:storage_service]),
      user_agent: payload[:user_agent],
      via_actions: payload[:via_actions],
      event_id: request_context[:request_id] || SecureRandom.uuid,
    }

    publish(message, schema: "package_registry.v0.PackageVersionDeleted")
  end

  subscribe("package_registry.package_file_destroyed") do |payload|
    request_context = GitHub.context.to_hash
    message = {
        artifact_id: payload[:artifact_id],
        storage_service: serializer.package_file_storage_service(payload[:storage_service]),
        event_id: request_context[:request_id] || SecureRandom.uuid,
        repository: { id: payload[:repository_id] },
    }

    publish(message, schema: "package_registry.v0.PackageFileDestroyed")
  end

  subscribe("package_registry.package_version_published") do |payload|
    request_context = GitHub.context.to_hash
    message = {
      request_context: serializer.request_context(request_context),
      actor: serializer.user(payload[:actor]),
      package: serializer.package(payload[:package]),
      version: serializer.package_version(payload[:version], files_count: payload[:files_count], size: payload[:size]),
      published_at: payload[:published_at],
      storage_service: serializer.package_file_storage_service(payload[:storage_service]),
      user_agent: payload[:user_agent],
      repository: serializer.repository(payload[:repository]),
      via_actions: payload[:via_actions],
      file: serializer.package_file(payload[:file]),
      event_id: request_context[:request_id] || SecureRandom.uuid,
    }

    publish(message, schema: "package_registry.v0.PackageVersionPublished")
  end

  subscribe("packages.package_transferred") do |payload|
    request_context = GitHub.context.to_hash
    message = {
      request_context: serializer.request_context(request_context),
      actor: serializer.user(payload[:actor]),
      package: serializer.package(payload[:package], total_size: payload[:size]),
      transferred_at: payload[:transferred_at],
      storage_service: serializer.package_file_storage_service(payload[:storage_service]),
      user_agent: payload[:user_agent],
      repository: serializer.repository(payload[:repository]),
      previous_repository: serializer.repository(payload[:previous_repository]),
      previous_owner_user: serializer.user(payload[:previous_owner_user]),
      previous_owner_org: serializer.organization(payload[:previous_owner_org]),
      previous_owner_id: payload[:previous_owner_id],
      previous_owner_global_id: payload[:previous_owner_global_id],
      event_id: request_context[:request_id] || SecureRandom.uuid,
    }

    publish(message, schema: "package_registry.v0.PackageTransferred")
  end

  subscribe(/(browser\.)?code_navigation\.(view_blob_symbols_available|view_blob_symbols_unavailable|view_blob_language_not_supported|click_on_symbol|navigate_to_definition|click_on_blob_definitions|navigate_to_blob_definition|click_on_references|navigate_to_reference|click_on_definitions)/) do |payload|
    repository = Repository.find_by(id: payload[:repository_id])
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(repository),
      action: payload[:action].to_s.upcase.to_sym,
      ref: payload[:ref]&.dup&.force_encoding(Encoding::UTF_8),
      language: payload[:language],
    }
    message[:actor] = serializer.user(User.find_by(id: payload[:user_id])) if payload[:user_id]

    publish(message, schema: "github.code_navigation.v0.JumpToEvent")
  end

  subscribe("repository_clone.created") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      clone_repository: serializer.repository(payload[:clone_repository]),
      template_repository: serializer.repository(payload[:template_repository]),
    }

    publish(message, schema: "github.v1.RepositoryCloneCreate")
  end

  subscribe("profile_pin.created") do |payload|
    message = { position: payload[:position] }
    message[:repository_id] = payload[:repository_id] if payload[:repository_id]
    message[:gist_id] = payload[:gist_id] if payload[:gist_id]
    message[:user] = serializer.user(payload[:user]) if payload[:user]
    if payload[:organization]
      message[:organization] = serializer.organization(payload[:organization])
    end

    publish(message, schema: "github.v1.ProfilePinCreate")
  end

  subscribe("profile_pin.deleted") do |payload|
    message = { position: payload[:position] }
    message[:repository_id] = payload[:repository_id] if payload[:repository_id]
    message[:gist_id] = payload[:gist_id] if payload[:gist_id]
    message[:user] = serializer.user(payload[:user]) if payload[:user]
    if payload[:organization]
      message[:organization] = serializer.organization(payload[:organization])
    end

    publish(message, schema: "github.v1.ProfilePinDestroy")
  end

  subscribe("user_dashboard_pin.created") do |payload|
    payload[:pinned_item_type] = "UNKNOWN" unless %w(repository gist user organization team issue pull_request project).include?(payload[:pinned_item_type].downcase)

    message = { position: payload[:position] }
    message[:pinned_item_id] = payload[:pinned_item_id] if payload[:pinned_item_id]
    message[:pinned_item_type] = payload[:pinned_item_type].upcase if payload[:pinned_item_type]
    message[:user] = serializer.user(payload[:user]) if payload[:user]

    publish(message, schema: "github.v1.UserDashboardPinCreate")
  end

  subscribe("user_dashboard_pin.deleted") do |payload|
    payload[:pinned_item_type] = "UNKNOWN" unless %w(repository gist user organization team issue pull_request project).include?(payload[:pinned_item_type].downcase)

    message = { position: payload[:position] }
    message[:pinned_item_id] = payload[:pinned_item_id] if payload[:pinned_item_id]
    message[:pinned_item_type] = payload[:pinned_item_type].upcase if payload[:pinned_item_type]
    message[:user] = serializer.user(payload[:user]) if payload[:user]

    publish(message, schema: "github.v1.UserDashboardPinDestroy")
  end

  subscribe("organization.create") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      organization: serializer.organization(payload[:organization]),
      actor: serializer.user(payload[:actor]),
      number_of_organizations_adminable_by_actor: payload[:number_of_organizations_adminable_by_actor],
      first_organization_adminable_by_actor: serializer.organization(payload[:first_organization_adminable_by_actor]),
      actor_email: serializer.user_email(payload[:actor_email]),
      actor_profile: serializer.profile(payload[:actor_profile]),
      visitor_id: payload[:visitor_id],
    }

    publish(message, schema: "github.v1.OrganizationCreate")
  end

  subscribe("organization.cancel_invitation") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      organization: serializer.organization(payload[:organization]),
      actor: serializer.user(payload[:actor]),
      email: payload[:email],
      user: serializer.user(payload[:user]),
      invitation: serializer.invitation(payload[:invitation]),
      organization_profile: serializer.profile(payload[:organization].profile)
    }

    publish(message, schema: "github.v1.OrganizationCancelInvitation")
  end

  subscribe("organization.invite_member") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      organization: serializer.organization(payload[:organization]),
      actor: serializer.user(payload[:actor]),
      email: payload[:email],
      user: serializer.user(payload[:user]),
      invitation: serializer.invitation(payload[:invitation]),
      organization_profile: serializer.profile(payload[:organization].profile)
    }

    publish(message, schema: "github.v1.OrganizationInviteMember")
  end

  subscribe("organization.retry_invitation") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      organization: serializer.organization(payload[:organization]),
      actor: serializer.user(payload[:actor]),
      email: payload[:email],
      user: serializer.user(payload[:user]),
      invitation: serializer.invitation(payload[:invitation]),
      organization_profile: serializer.profile(payload[:organization].profile)
    }

    publish(message, schema: "github.v1.OrganizationRetryInvitation")
  end

  subscribe("update_manifest.repository") do |payload|
    encrypted_content = GitHub.dependency_graph_api_hydro_key.encrypt(
      payload[:manifest_file][:content].to_s,
      local: true,
    )

    manifest_file = {
      filename: payload[:manifest_file][:filename],
      path: payload[:manifest_file][:path],
      content: encrypted_content,
      git_ref: payload[:manifest_file][:git_ref].to_s,
      pushed_at: payload[:manifest_file][:pushed_at],
    }

    message = {
      repository_id: payload[:repository_id],
      owner_id: payload[:owner_id],
      repository_private: payload[:repository_private],
      repository_fork: payload[:repository_fork],
      repository_nwo:  payload[:repository_nwo],
      repository_stargazer_count:  payload[:repository_stargazer_count],
      manifest_file: manifest_file,
      is_backfill: payload.fetch(:is_backfill, false),
    }

    publish(message,
            topic: "github.dependencygraph.v1.RepositoryManifestFileChange",
            schema: "github.dependencygraph.v0.RepositoryManifestFileChange",
            partition_key: message[:repository_id])
  end

  subscribe("user_email.verify") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      actor: serializer.user(payload[:actor]),
      previous_primary_email: serializer.user_email(payload[:previous_primary_email]),
      current_primary_email: serializer.user_email(payload[:current_primary_email]),
      verified_email: serializer.user_email(payload[:verified_email]),
    }

    publish(message, schema: "github.v1.UserEmailVerify")
  end

  subscribe("oauth_access.create") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      database_id: payload[:oauth_access].id,
      scopes: Array.wrap(payload[:scopes]),
      accessible_organization_ids: Array.wrap(payload[:accessible_organization_ids]),
    }

    case payload[:oauth_access].application_type
    when "OauthApplication"
      message[:app_type] = :OAUTH_APPLICATION
      message[:oauth_application] = serializer.oauth_application(payload[:app])
    when "Integration"
      message[:app_type] = :INTEGRATION
      message[:integration] = serializer.integration(payload[:app])
    end

    publish(message, schema: "github.v1.OauthAccess")
  end

  subscribe(/user\.(star|unstar)/) do |payload, event|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      star_id: payload[:star].id,
      action_type: event.split(".").last.upcase,
      context_type: (Star::STARRABLE_CONTEXTS.include?(payload[:context]) ? payload[:context] : "other").upcase,
    }

    case payload[:entity]
    when Gist
      publish(message.merge({
        gist: serializer.gist(payload[:entity]),
        gist_owner: serializer.user(payload[:owner]),
        gist_stars_count: payload[:stars_count],
      }), schema: "github.v1.GistStar")
    when Repository
      publish(message.merge({
        repository: serializer.repository(payload[:entity]),
        repository_owner: serializer.user(payload[:owner]),
        repository_stars_count: payload[:stars_count],
      }), schema: "github.v1.RepositoryStar")
    end
  end

  subscribe("repository.visibility_changed") do |payload|
    message = {
      name_with_owner: payload[:name_with_owner],
      repository_id: payload[:repository_id],
      is_private: payload[:is_private],
      is_fork: payload[:is_fork],
      visibility: payload[:visibility],
    }
    publish(message, schema: "github.v1.RepositoryVisibilityChanged")
  end

  subscribe("repository.archived_status_changed") do |payload|
    message = {
      repository_id: payload[:repository_id],
      is_archived: payload[:is_archived],
    }
    publish(message, schema: "github.v1.RepositoryArchivedStatusChanged")
  end

  subscribe("repository.create") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
      gitignore_template: payload[:gitignore_template],
      license_template: payload[:license_template],
      init_with_readme: payload[:init_with_readme],
    }

    publish(message, schema: "github.v1.RepositoryCreate")
  end

  subscribe("repository.rename") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      previous_name: payload[:previous_name],
      current_name: payload[:current_name],
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
    }

    publish(message, schema: "github.v1.RepositoryRename")
  end

  subscribe("repository.push") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      owner: serializer.user(payload[:owner]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
      before: payload[:before],
      after: payload[:after],
      ref: payload[:ref]&.dup&.force_encoding(Encoding::UTF_8),
      changed_files: payload[:changed_files].map { |f| serializer.changed_file(f) },
      forced: payload[:forced],
      large: payload[:large],
      commit_count: payload[:commit_count],
      commit_oids: payload[:commit_oids],
      branch_protection_rule: serializer.branch_protection_rule(payload[:branch_protection_rule]),
      feature_flags: payload[:feature_flags],
    }

    publish(message, schema: "github.v1.RepositoryPush", partition_key: message[:repository][:id])
  end

  subscribe("repository.aleph_reindex") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      owner: serializer.user(payload[:owner]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
      after: payload[:after],
      ref: payload[:ref]&.dup&.force_encoding(Encoding::UTF_8),
      feature_flags: payload[:feature_flags],
    }

    publish(message,
      topic: "github.v0.AlephdIndexRepository",
      schema: "github.v1.RepositoryPush",
      partition_key: message[:repository][:id],
    )
  end

  subscribe("repository.post_receive") do |payload|
    ref_updates = payload[:ref_updates].map do |(before, after, ref)|
      {
        ref_name: ref&.dup&.force_encoding(Encoding::UTF_8),
        previous_ref_oid: before,
        current_ref_oid: after,
      }
    end
    message = {
      actor: serializer.user(payload[:actor]),
      owner: serializer.user(payload[:owner]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
      ref_updates: Array.wrap(ref_updates),
      feature_flags: payload[:feature_flags],
      pushed_at: payload[:pushed_at],
    }

    publish(message, schema: "github.v1.PostReceive", partition_key: message[:repository][:id])
  end

  subscribe("repository.deleted") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      deleted_repository: serializer.repository(payload[:deleted_repository]),
    }

    publish(message, schema: "github.v1.RepositoryDeleted")
  end

  subscribe("repository.restored") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      restored_repository: serializer.repository(payload[:restored_repository]),
    }

    publish(message, schema: "github.v1.RepositoryRestored")
  end

  subscribe("repository.invite") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      invitee: serializer.user(payload[:invitee]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
    }

    publish(message, schema: "github.v1.RepositoryInvite")
  end

  subscribe("gist.create") do |payload|
    gist = payload[:gist]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      gist: serializer.gist(gist),
      head_sha: gist.sha.to_s,
      files: serializer.gist_files(gist, payload[:files]),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_files: serializer.gist_specimen_files(payload[:files].first(3)),
      specimen_gist_description: serializer.specimen_data(gist.description),
      specimen_files_path: serializer.gist_specimen_files_path(payload[:files].first(3)),
    }

    publish(message, schema: "github.v1.GistCreate")
  end

  subscribe("gist.update") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      current_head_sha: payload[:current_head_sha],
      current_gist: payload[:current_serialized_gist],
      current_files: payload[:current_serialized_files],
      previous_head_sha: payload[:previous_head_sha],
      previous_gist: payload[:previous_serialized_gist],
      previous_files: payload[:previous_serialized_files],
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_files: payload[:specimen_files],
      specimen_gist_description: serializer.specimen_data(payload[:current_description]),
      specimen_files_path: payload[:specimen_files_path],
    }

    publish(message, schema: "github.v1.GistUpdate")
  end

  subscribe("gist_comment.create") do |payload|
    gist_comment = payload[:gist_comment]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(gist_comment.user),
      gist: serializer.gist(gist_comment.gist),
      gist_comment: serializer.gist_comment(gist_comment),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_body: serializer.specimen_data(gist_comment.body),
    }.merge(serializer.gist_comment_gist_fields(gist_comment))

    publish(message, schema: "github.v1.GistCommentCreate", publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("gist_comment.update") do |payload|
    gist_comment = payload[:gist_comment]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(gist_comment.user),
      gist: serializer.gist(gist_comment.gist),
      gist_comment: serializer.gist_comment(gist_comment),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      current_specimen_body: serializer.specimen_data(gist_comment.body),
      previous_specimen_body: serializer.specimen_data(payload[:previous_body]),
    }.merge(serializer.gist_comment_gist_fields(gist_comment))

    publish(message, schema: "github.v1.GistCommentUpdate", publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("contact_form.submit") do |payload|
    contact_request = payload[:contact_request]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      name: contact_request.name.to_s,
      email: contact_request.email.to_s,
      subject: contact_request.subject.to_s,
      body: contact_request.comments.to_s,
      form_type: contact_request.flavor.to_s.gsub("-", "_").upcase,
      referring_url: contact_request.referring_url.to_s,
      reported_content_url: contact_request.content_url.to_s,
    }

    if contact_request.classifier.present?
      classifier = contact_request.classifier
      possible_classifiers = Platform::Enums::AbuseReportReason.values.keys
      sanitized_classifier = possible_classifiers.include?(classifier) ? classifier : "UNSPECIFIED"
      message[:reported_content_classifier] = sanitized_classifier
    end

    if (content_object = contact_request.content_object)
      message[:reported_content_type] = contact_request.content_type.underscore.upcase
      message[:reported_content_id] = content_object.id
      message[:reported_content] = [
        content_object.try(:title),
        content_object.try(:body),
      ].compact.join("\n")
    end

    message[:reported_user] = serializer.user(payload[:reported_user]) if payload[:reported_user].present?
    message[:reported_integration] = serializer.integration(payload[:reported_integration]) if payload[:reported_integration].present?

    publish(message, schema: "github.v1.ContactFormSubmission")
  end

  subscribe("issue_comment.create") do |payload|
    if payload[:issue].pull_request_id.nil? && payload[:repository].public?
      message = {
        request_context: serializer.request_context(GitHub.context.to_hash),
        actor: serializer.user(payload[:actor]),
        repository: serializer.repository(payload[:repository]),
        repository_owner: serializer.user(payload[:repository_owner]),
        issue: serializer.issue(payload[:issue]),
        issue_creator: serializer.user(payload[:issue_creator]),
        issue_comment: serializer.issue_comment(payload[:issue_comment]),
        body: payload[:issue_comment].body,
        spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
        specimen_body: serializer.specimen_data(payload[:issue_comment].body),
      }

      publish(message, schema: "github.v1.IssueCommentCreate", publisher: GitHub.low_latency_hydro_publisher)
    end
  end

  subscribe("issue_comment.update") do |payload|
    if payload[:issue].pull_request_id.nil? && payload[:repository].public?
      message = {
        request_context: serializer.request_context(GitHub.context.to_hash),
        actor: serializer.user(payload[:actor]),
        repository: serializer.repository(payload[:repository]),
        repository_owner: serializer.user(payload[:repository_owner]),
        issue: serializer.issue(payload[:issue]),
        issue_creator: serializer.user(payload[:issue_creator]),
        issue_comment: serializer.issue_comment(payload[:issue_comment]),
        spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
        current_specimen_body: serializer.specimen_data(payload[:issue_comment].body),
        previous_specimen_body: serializer.specimen_data(payload[:previous_body]),
      }

      publish(message, schema: "github.v1.IssueCommentUpdate", publisher: GitHub.low_latency_hydro_publisher)
    end
  end

  subscribe(/issue_comment\.(pinned|unpinned)/) do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      state: payload[:state],
      issue_comment: serializer.issue_comment(payload[:issue_comment]),
      issue: serializer.issue(payload[:issue]),
    }

    publish(message, schema: "github.v1.IssueCommentPinUpdated")
  end

  subscribe("issue.create") do |payload|
    common_msg = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      repository_owner: serializer.user(payload[:repository_owner]),
      issue: serializer.issue(payload[:issue]),
      issue_creator: serializer.user(payload[:issue_creator]),
    }.freeze

    if payload[:issue].pull_request_id.nil?
      issue_created_msg = {
        repository: serializer.repository_domain(payload[:repository]),
        actor: serializer.user_domain(payload[:actor]),
        issue: serializer.issue_domain(payload[:issue]),
      }

      publish(issue_created_msg, schema: "github.domain_events.v0.IssueCreated", publisher: GitHub.low_latency_hydro_publisher)
      if payload[:repository].public?
        issue_create_msg = common_msg.merge(
          title: payload[:issue].title,
          body: payload[:issue].body,
          spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
          specimen_body: serializer.specimen_data(payload[:issue].body),
          specimen_title: serializer.specimen_data(payload[:issue].title),
        )

        publish(issue_create_msg, schema: "github.v1.IssueCreate", publisher: GitHub.low_latency_hydro_publisher)
      end

      from_template_msg = common_msg.merge(
        issue_template: serializer.issue_template(payload[:template]),
      )

      publish(from_template_msg, schema: "github.v1.IssueCreateTemplate")
    end
  end

  subscribe(/browser.rerequest_review\.(sidebar|mergebox)/) do |payload|
    actor = User.find_by(id: payload[:actor_id])
    subject = User.find_by(id: payload[:subject_id])
    pull = PullRequest.find_by(id: payload[:pull_request_id])
    if pull.present?
      message = {
        pull_request: serializer.pull_request(pull),
        actor: serializer.user(actor),
        subject_user: serializer.user(subject),
        repository: serializer.repository(pull.repository),
        request_context: serializer.request_context(GitHub.context.to_hash,
          overrides: browser_request_context_overrides(payload)),
        action: serializer.enum_from_string(payload[:action]),
      }

      publish(message, schema: "github.v1.PullRequestReviewRequest")
    end
  end

  subscribe(/pull_request_file\.(viewed|unviewed|dismissed)/) do |payload|
    message = {
      pull_request: serializer.pull_request(payload[:pull_request]),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      file_path: serializer.enum_from_string(payload[:file_path]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      action: serializer.enum_from_string(payload[:action]),
    }

    publish(message, schema: "github.v1.MarkFileAsViewed")
  end

  subscribe("pull_request.create") do |payload|
    pull = payload[:pull_request]
    if pull.present?
      hydro_payload = {
        pull_request: serializer.pull_request(pull),
        issue: serializer.issue(pull.issue),
        actor: serializer.user(pull.user),
        repository: serializer.repository(pull.repository),
        repository_owner: serializer.user(pull.repository.owner),
        request_context: serializer.request_context(GitHub.context.to_hash),
        spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamuri_form_signals]),
      }
      publish(hydro_payload, schema: "github.v1.PullRequestCreate", publisher: GitHub.low_latency_hydro_publisher)
    end
  end

  subscribe("pull_request.close") do |payload|
    pull = payload[:pull_request]
    actor = payload[:actor]
    if pull.present?
      hydro_payload = {
        pull_request: serializer.pull_request(pull),
        issue: serializer.issue(pull.issue),
        repository: serializer.repository(pull.repository),
        repository_owner: serializer.user(pull.repository.owner),
      }

      if actor.present?
        hydro_payload[:actor] = serializer.user(actor)
      end

      publish(hydro_payload, schema: "github.v1.PullRequestClose")
    end
  end

  subscribe("pull_request.merge") do |payload|
    pull = payload[:pull_request]
    actor = payload[:actor]
    if pull.present?
      hydro_payload = {
        pull_request: serializer.pull_request(pull),
        issue: serializer.issue(pull.issue),
        repository: serializer.repository(pull.repository),
        repository_owner: serializer.user(pull.repository.owner),
      }

      if actor.present?
        hydro_payload[:actor] = serializer.user(actor)
      end

      publish(hydro_payload, schema: "github.v1.PullRequestMerge")
    end
  end

  subscribe("pull_request.ready_for_review") do |payload|
    pull = payload[:pull_request]
    actor = payload[:actor]
    if pull.present?
      hydro_payload = {
        pull_request: serializer.pull_request(pull),
        issue: serializer.issue(pull.issue),
        repository: serializer.repository(pull.repository),
        repository_owner: serializer.user(pull.repository.owner),
      }

      if actor.present?
        hydro_payload[:actor] = serializer.user(actor)
      end

      publish(hydro_payload, schema: "github.v1.PullRequestReadyForReview")
    end
  end

  subscribe("pull_request.converted_to_draft") do |payload|
    pull = payload[:pull_request]
    actor = payload[:actor]
    if pull.present?
      hydro_payload = {
        pull_request: serializer.pull_request(pull),
        issue: serializer.issue(pull.issue),
        repository: serializer.repository(pull.repository),
        repository_owner: serializer.user(pull.repository.owner),
      }

      if actor.present?
        hydro_payload[:actor] = serializer.user(actor)
      end

      publish(hydro_payload, schema: "github.v1.PullRequestConvertToDraft")
    end
  end

  subscribe("pull_request.reopen") do |payload|
    pull = payload[:pull_request]
    actor = payload[:actor]
    if pull.present?
      hydro_payload = {
        pull_request: serializer.pull_request(pull),
        issue: serializer.issue(pull.issue),
        repository: serializer.repository(pull.repository),
        repository_owner: serializer.user(pull.repository.owner),
      }

      if actor.present?
        hydro_payload[:actor] = serializer.user(actor)
      end

      publish(hydro_payload, schema: "github.v1.PullRequestReopen")
    end
  end

  subscribe("browser.pull_request.select_diff_range") do |payload|
    client_context = payload[:client][:context]
    message = {
      actor: serializer.user(User.find_by(id: payload[:actor_id])),
      repository: serializer.repository(Repository.find_by(id: payload[:repository_id])),
      pull_request: serializer.pull_request(PullRequest.find_by(id: payload[:pull_request_id])),
      starting_diff_position: client_context[:starting_diff_position],
      ending_diff_position: client_context[:ending_diff_position],
      line_count: client_context[:line_count],
      diff_type: payload[:diff_type],
      whitespace_ignored: payload[:whitespace_ignored],
    }

    publish(message, schema: "github.v1.PullRequestMultilineSelect")
  end

  subscribe("browser.pull_request.diffbar_filter_by_revision") do |payload|
    pull = PullRequest.find_by(id: payload[:pull_request_id])
    if pull.present?
      message = {
        actor: serializer.user(User.find_by(id: payload[:user_id])),
        pull_request: serializer.pull_request(pull),
        repository: serializer.repository(pull.repository),
        current_revision_number: payload[:current_revision_number],
        viewed_revision_number: payload[:viewed_revision_number],
        filter_selection: payload[:filter_selection]
      }

      publish(message, schema: "github.v1.PullRequestDiffbarFilterByRevision")
    end
  end

  subscribe("browser.pull_request.local_checkout") do |payload|
    message = {
      actor: serializer.user(User.find_by(id: payload[:user_id])),
      action: payload[:action],
      client_type: payload[:client_type],
    }

    publish(message, schema: "github.v1.PullRequestLocalCheckout")
  end

  subscribe("browser.pull_request.merge_external") do |payload|
    message = {
      actor: serializer.user(User.find_by(id: payload[:user_id])),
      client_type: payload[:client_type],
    }

    publish(message, schema: "github.v1.PullRequestMergeExternal")
  end

  subscribe("issue.update") do |payload|
    if payload[:issue].pull_request_id.nil? && payload[:repository].public?
      message = {
        request_context: serializer.request_context(GitHub.context.to_hash),
        actor: serializer.user(payload[:actor]),
        repository: serializer.repository(payload[:repository]),
        repository_owner: serializer.user(payload[:repository_owner]),
        issue: serializer.issue(payload[:issue]),
        issue_updater: serializer.user(payload[:issue_updater]),
        previous_title: payload[:previous_title],
        current_title: payload[:current_title],
        previous_body: payload[:previous_body],
        current_body: payload[:current_body],
        spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
        specimen_body: serializer.specimen_data(payload[:current_body]),
        specimen_title: serializer.specimen_data(payload[:current_title]),
      }

      publish(message, schema: "github.v1.IssueUpdate", publisher: GitHub.low_latency_hydro_publisher)
    end
  end

  subscribe("browser.check_suite_image.expand") do |payload|

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      check_suite_id: serializer.user(payload[:client][:check_suite_id]),
      check_run_id: serializer.user(payload[:client][:check_run_id]),
      github_app_id: serializer.user(payload[:client][:github_app_id]),
      filename: serializer.user(payload[:client][:filename]),
    }

    publish(message, schema: "github.v1.CheckSuiteImageExpand")
  end

  subscribe("browser.check_suite.external_click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      check_suite_id: serializer.user(payload[:client][:check_suite_id]),
      check_run_id: serializer.user(payload[:client][:check_run_id]),
      github_app_id: serializer.user(payload[:client][:github_app_id]),
      external_link_url: serializer.user(payload[:client][:link_url]),
      external_link_text: serializer.user(payload[:client][:link_text]),
    }

    publish(message, schema: "github.v1.CheckSuiteExternalClick")
  end

  subscribe("browser.marketplace.recommended_project_management.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user:            serializer.user(payload[:client][:user]),
      issue_count:     payload[:issue_count],
    }

    publish(message, schema: "github.marketplace.v0.RecommendedProjectManagementClick")
  end

  subscribe("browser.marketplace.recommended_project_management.dismissed") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user:            serializer.user(payload[:client][:user]),
      issue_count:     payload[:issue_count],
    }

    publish(message, schema: "github.marketplace.v0.RecommendedProjectManagementDismissed")
  end

  subscribe("browser.marketplace.trending_apps.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      marketplace_listing: serializer.marketplace_listing_from_database_id(payload[:marketplace_listing_id]),
    }

    publish(message, schema: "github.marketplace.v0.TrendingAppsClick")
  end

  subscribe("marketplace.trending_apps.displayed") do |payload|
    listing_ids = payload[:marketplace_listing_ids]
    listings = Marketplace::Listing.where(id: listing_ids)

    serialized_listings = listings.map do |listing|
      serializer.marketplace_listing(listing)
    end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      marketplace_listings: serialized_listings,
    }

    publish(message, schema: "github.marketplace.v0.TrendingAppsDisplayed")
  end

  subscribe("browser.marketplace.recommended_page.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
    }

    publish(message, schema: "github.marketplace.v0.RecommendedPageClick")
  end

  subscribe("browser.marketplace.recommended_listing.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      marketplace_listing: serializer.marketplace_listing_from_database_id(payload[:marketplace_listing_id]),
    }

    publish(message, schema: "github.marketplace.v0.RecommendedListingClick")
  end

  subscribe("marketplace.recommended_listings.displayed") do |payload|
    listing_ids = payload[:marketplace_listing_ids]
    listings = Marketplace::Listing.where(id: listing_ids)

    serialized_listings = listings.map do |listing|
      serializer.marketplace_listing(listing)
    end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      marketplace_listings: serialized_listings,
    }

    publish(message, schema: "github.marketplace.v0.RecommendedListingsDisplayed")
  end

  subscribe("browser.marketplace.action.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      user: serializer.user(payload[:client][:user]),
      repository_action: serializer.repository_action(payload[:repository_action_id]),
      location: payload[:location],
      source_url: payload[:source_url],
    }

    publish(message, schema: "github.marketplace.v0.ActionClick")
  end

  subscribe("marketplace.action.list") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository_owner: serializer.user(payload[:repository_owner]),
      repository_action: serializer.repository_action(payload[:repository_action_id]),
      repository: serializer.repository(payload[:repository]),
      release_tag_name: payload[:release_tag_name],
    }

    publish(message, schema: "github.marketplace.v0.ActionList")
  end

  subscribe("browser.marketplace.action_use_button.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      user: serializer.user(payload[:client][:user]),
      repository_action: serializer.repository_action(payload[:repository_action_id]),
      location: payload[:location],
      source_url: payload[:source_url],
    }

    publish(message, schema: "github.marketplace.v0.ActionUseButtonClick")
  end

  subscribe("browser.marketplace.action_use_button_repository.click") do |payload|
    _, repo_id = Platform::Helpers::NodeIdentification.from_global_id(payload[:repository_id])
    repo = Repository.find_by(id: repo_id)

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      user: serializer.user(payload[:client][:user]),
      repository_action: serializer.repository_action(payload[:repository_action_id]),
      repository: serializer.repository(repo),
      location: payload[:location],
      source_url: payload[:source_url],
    }

    publish(message, schema: "github.marketplace.v0.ActionUseButtonRepositoryClick")
  end

  subscribe("marketplace.action_use_button_repository.search") do |payload|
    results = payload[:repository_ids].map do |global_id|
      _, repo_id = Platform::Helpers::NodeIdentification.from_global_id(global_id)
      repo = Repository.find_by(id: repo_id)
      serializer.repository(repo)
    end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      repository_action: serializer.repository_action(payload[:repository_action_id]),
      query: payload[:query],
      results: results,
    }

    publish(message, schema: "github.marketplace.v0.ActionUseButtonRepositorySearch")
  end

  subscribe("browser.marketplace.action.delist") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      user: serializer.user(payload[:client][:user]),
      repository_action: serializer.repository_action(payload[:repository_action_id]),
      location: payload[:location],
      source_url: payload[:source_url],
    }

    publish(message, schema: "github.marketplace.v0.ActionDelist")
  end

  subscribe("browser.marketplace_listing_click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      user: serializer.user(payload[:client][:user]),
      marketplace_listing: serializer.marketplace_listing_from_database_id(payload[:marketplace_listing_id]),
      location: payload[:location],
      source_url: payload[:source_url],
      destination_url: payload[:destination_url],
    }

    publish(message, schema: "github.marketplace.v0.ListingClick")
  end

  subscribe("marketplace.listing_install") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      marketplace_listing: serializer.marketplace_listing_from_database_id(payload[:listing_id]),
      billing_plan: serializer.marketplace_listing_plan_from_database_id(payload[:listing_plan_id]),
    }

    publish(message, schema: "github.marketplace.v0.ListingInstall")
  end

  subscribe("marketplace.listing_install") do |payload|
    listing = Marketplace::Listing.find_by(id: payload[:listing_id])
    next if listing&.listable_is_sponsorable?

    product_uuid = Billing::ProductUUID.find_by(
      product_type: "marketplace.listing_plan",
      product_key: payload[:listing_plan_id],
      billing_cycle: payload[:user]&.plan_duration,
    )

    message = {
      actor: serializer.user(payload[:user]),
      resource: "BillingProduct",
      resource_action: "create",
      resource_quantity_delta: payload[:quantity].to_i,
      resource_quantity_total: payload[:quantity].to_i,
      target_entity: {
        entity_type: :BILLING_PRODUCT,
        entity_id: product_uuid&.id,
      },
      target_entity_owner: serializer.user(payload[:user]),
      request_context: serializer.request_context(GitHub.context.to_hash),
    }
    publish(message, schema: "github.v1.UserBehavior")
  end

  subscribe("marketplace.listing_state_change") do |payload|
    message = {
      marketplace_listing: serializer.marketplace_listing_from_database_id(payload[:listing_id]),
      previous_state: payload[:previous_state].to_s,
    }

    publish(message, schema: "github.marketplace.v0.ListingStateChange")
  end

  subscribe("browser.marketplace_retargeting_notice_click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      user: serializer.user(payload[:client][:user]),
      marketplace_listings: payload[:marketplace_listing_ids].map { |id| serializer.marketplace_listing_from_database_id(id) },
      marketplace_listing_plans: payload[:marketplace_listing_plan_ids].map { |id| serializer.marketplace_listing_plan_from_database_id(id) },
      notice_name: payload[:notice_name],
    }

    publish(message, schema: "github.marketplace.v0.RetargetingNoticeClick")
  end

  subscribe("browser.marketplace_retargeting_notice_dismissed") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      user: serializer.user(payload[:client][:user]),
      marketplace_listings: payload[:marketplace_listing_ids].map { |id| serializer.marketplace_listing_from_database_id(id) },
      marketplace_listing_plans: payload[:marketplace_listing_plan_ids].map { |id| serializer.marketplace_listing_plan_from_database_id(id) },
      notice_name: payload[:notice_name],
    }

    publish(message, schema: "github.marketplace.v0.RetargetingNoticeDismissed")
  end

  subscribe("marketplace_retargeting_notice_displayed") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      marketplace_listings: payload[:marketplace_listing_ids].map { |id| serializer.marketplace_listing_from_database_id(id) },
      marketplace_listing_plans: payload[:marketplace_listing_plan_ids].map { |id| serializer.marketplace_listing_plan_from_database_id(id) },
      notice_name: payload[:notice_name],
    }

    publish(message, schema: "github.marketplace.v0.RetargetingNoticeDisplayed")
  end

  subscribe("browser.user-hovercard-hover") do |payload|
    user = User.find_by(id: payload[:userId])

    message = {
      actor: serializer.user(payload[:client][:user]),
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      user: serializer.user(user),
      context: serializer.hovercard_context_string(payload[:subject]),
    }

    publish(message, schema: "github.v1.UserHovercardHover")
  end

  subscribe("browser.repository-hovercard-hover") do |payload|
    repo = Repository.find_by(id: payload[:repositoryId])

    message = {
      actor: serializer.user(payload[:client][:user]),
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      repository: serializer.repository(repo),
      context: serializer.hovercard_context_string(payload[:subject]),
    }

    publish(message, schema: "github.v1.RepositoryHovercardHover")
  end

  subscribe("browser.team-hovercard-hover") do |payload|
    team = Team.find_by(id: payload[:teamId])

    message = {
      actor: serializer.user(payload[:client][:user]),
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      team: serializer.team(team),
      context: serializer.hovercard_context_string(payload[:subject]),
    }

    publish(message, schema: "github.v1.TeamHovercardHover")
  end

  subscribe("browser.issue-hovercard-hover") do |payload|
    issue = Issue.find_by(id: payload[:pullRequestOrIssueId])

    message = {
      actor: serializer.user(payload[:client][:user]),
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      issue: serializer.issue(issue),
      context: serializer.hovercard_context_string(payload[:subject]),
    }

    publish(message, schema: "github.v1.IssueHovercardHover")
  end

  subscribe("browser.pull-request-hovercard-hover") do |payload|
    pr = PullRequest.find_by(id: payload[:pullRequestOrIssueId])

    message = {
      actor: serializer.user(payload[:client][:user]),
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      pull_request: serializer.pull_request(pr),
      context: serializer.hovercard_context_string(payload[:subject]),
    }

    publish(message, schema: "github.v1.PullRequestHovercardHover")
  end

  subscribe("browser.organization-hovercard-hover") do |payload|
    organization = Organization.find_by(id: payload[:organizationId])

    message = {
      actor: serializer.user(payload[:client][:user]),
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      organization: serializer.organization(organization),
      context: serializer.hovercard_context_string(payload[:subject]),
    }

    publish(message, schema: "github.v1.OrganizationHovercardHover")
  end

  subscribe("browser.commit-hovercard-hover") do |payload|
    commit_id = payload[:commitGlobalRelayId]
    commit = if commit_id
      decoded_commit_id = Platform::Helpers::NodeIdentification.from_global_id(commit_id).last
      Platform::Security::RepositoryAccess.with_viewer(payload[:actor]) do
        Platform::Objects::Commit.load_from_global_id(decoded_commit_id).sync
      end
    end

    message = {
      actor: serializer.user(payload[:client][:user]),
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      commit: serializer.commit(commit),
      context: serializer.hovercard_context_string(payload[:subject]),
    }

    publish(message, schema: "github.v1.CommitHovercardHover")
  end

  subscribe("browser.news_feed.event.click") do |payload|
    target_type = if payload[:target_type]
      payload[:target_type].upcase.to_sym
    else
      :TARGET_TYPE_UNKNOWN
    end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      client_timestamp: payload[:client][:timestamp].try(:to_i),
      server_timestamp: Time.zone.now,
      actor: serializer.user(payload[:client][:user]),
      originating_request_id: payload[:originating_request_id],
      action_target: payload[:action_target],
      context: payload[:org_id] ? :ORG : :USER,
      event: serializer.news_feed_event(payload[:event]),
      target_type: target_type,
      event_group: serializer.news_feed_event_group(payload[:event_group]),
    }

    if payload[:org_id]
      message[:org] = {id: payload[:org_id]}
    end

    publish(message, schema: "github.v1.NewsFeedClick")
  end

  subscribe(/(browser\.)?news_feed.event.view/) do |payload|
    org = if payload[:org]
      serializer.organization(payload[:org])
    elsif payload[:org_id]
      {id: payload[:org_id]}
    end

    user = payload[:actor]
    user ||= payload[:client] && payload[:client][:user]

    target_type = if payload[:target_type]
      payload[:target_type].upcase.to_sym
    else
      :TARGET_TYPE_UNKNOWN
    end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(user),
      org: org,
      context: org.nil? ? :USER : :ORG,
      event: serializer.news_feed_event(payload[:event]),
      target_type: target_type,
      event_group: serializer.news_feed_event_group(payload[:event_group]),
    }

    publish(message, schema: "github.v1.NewsFeedView")
  end

  subscribe("browser.pull-request-refresh") do |payload|
    pr = PullRequest.find_by(id: payload[:pull_request_id])

    message = {
      actor: serializer.user(payload[:client][:user]),
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      pull_request: serializer.pull_request(pr),
      repository: serializer.repository(pr.repository),
      tab_context: serializer.pull_request_refresh_tab_context(payload[:tab_context]),
    }

    publish(message, schema: "github.v1.PullRequestRefresh")
  end

  subscribe("spam.notification") do |payload|
    message = {
      actor_login: payload[:actor].try(:login),
      channel: payload[:channel],
      message: payload[:message],
    }

    publish(message, schema: "hamzo.v0.ChatterboxNotification")
  end

  subscribe("account.rename") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(GitHub.context.to_hash.dig(:hydro, :actor)),
      account: serializer.user(payload[:account]),
      previous_login: payload[:previous_login],
      current_login: payload[:current_login],
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
    }

    publish(message, schema: "github.v1.AccountRename")
  end

  subscribe("payout.transfer") do |payload|
    message = {
      account: serializer.user(payload[:account]),
      total_transfer_amount_in_cents: payload[:total_transfer_amount_in_cents],
      match_amount_in_cents: payload[:match_amount_in_cents],
      sponsorship_amount_in_cents: payload[:sponsorship_amount_in_cents],
      destination_account_type: serializer.destination_account_type(payload[:destination_account_type]),
      destination_account_id: payload[:destination_account_id],
      payment_gateway: serializer.payment_gateway(payload[:payment_gateway]),
      matched: payload[:matched],
    }

    publish(message, schema: "github.payouts.v0.Transfer")
  end

  subscribe("payout.bank_payout") do |payload|
    message = {
      sponsored_maintainer: serializer.user(payload[:sponsored_maintainer]),
      total_payout_amount_in_cents: payload[:total_payout_amount_in_cents],
      destination_account_type: serializer.destination_account_type(payload[:destination_account_type]),
      destination_account_id: payload[:destination_account_id],
      status: serializer.payout_status(payload[:status]),
      funding_instrument_type: serializer.funding_instrument_type(payload[:funding_instrument_type]),
      payout_id: payload[:payout_id],
      failure_code: payload[:failure_code],
    }

    publish(message, schema: "github.payouts.v0.BankPayout")
  end

  subscribe("profile.update") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      previous_profile: serializer.profile(payload[:previous_profile]),
      current_profile: serializer.profile(payload[:current_profile]),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      changed_attribute_names: payload[:changed_attribute_names],
    }.merge(serializer.profile_specimen_data_fields(payload[:current_profile]))

    publish(message, schema: "github.v1.ProfileUpdate")
  end

  subscribe("profile_picture.update") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      avatar: serializer.avatar(payload[:avatar]),
    }

    publish(message, schema: "github.v1.PrimaryAvatarUpdate")
  end

  subscribe("octocaptcha.signup") do |payload|
    event_id = Base64.encode64("#{payload[:event_type]}-#{payload[:session_id]}-#{payload[:occurred_at].to_i}")
    message = {
      event_id: event_id,
      occurred_at: payload[:occurred_at],
      event_type: payload[:event_type],
      session_id: payload[:session_id],
      show_captcha: payload[:show_captcha],
      show_one_page_flow: true,
      validation_value: payload[:validation_value],
      validation_error: payload[:validation_error],
      signup_time: payload[:signup_time],
      funcaptcha_response: serializer.funcaptcha_response(payload[:funcaptcha_response]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      source_page: payload[:source_page],
      email_address: payload[:email_address],
      raw_funcaptcha_response_data: payload[:funcaptcha_response] && payload[:funcaptcha_response].to_json,
    }

    publish(message, schema: "github.v1.OctocaptchaSignup")
  end

  subscribe(/(org|team|repo).(add|remove)_member/) do |payload|
    object = payload[:team] || payload[:repo] || payload[:org]

    if object
      action = if payload[:action]
        payload[:action].upcase.to_sym
      end

      context = object.class.name.upcase.to_sym
      group_id = object.id

      reason = if payload[:reason]
        payload[:reason].upcase.to_sym
      end

      message = {
        request_context: serializer.request_context(GitHub.context.to_hash),
        context: context || :CONTEXT_UNKNOWN,
        actor: serializer.user(payload[:actor]),
        user: serializer.user(payload[:user]),
        action: action || :ACTION_UNKNOWN,
        group_id: group_id,
        reason: reason || :REASON_UNKNOWN,
      }

      publish(message, schema: "github.v1.MembershipUpdate")
    end
  end

  subscribe("user_profile.page_view") do |payload|
    message = {
      profile_user: serializer.user(payload.fetch(:profile_user)),
      profile_viewer: serializer.user(payload.fetch(:profile_viewer)),
      request_context: serializer.request_context(GitHub.context.to_hash),
      has_organization_memberships: payload.fetch(:has_organization_memberships),
      scoped_org_id: payload.fetch(:scoped_org_id),
      selected_tab: payload.fetch(:selected_tab),
      profile_readme_rendered: payload.fetch(:profile_readme_rendered, false),
    }

    publish(message, schema: "github.v1.UserProfileView")
  end

  subscribe("browser.user_profile.highlights_click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      scoped_org_id: payload.fetch(:scoped_org_id),
      client_timestamp: payload[:client][:timestamp].try(:to_i),
      server_timestamp: Time.zone.now,
      target_url: payload.fetch(:target_url),
      target_type: payload.fetch(:target_type),
      originating_request_id: payload.fetch(:originating_request_id),
    }

    publish(message, schema: "github.v1.UserProfileHighlightsClick")
  end

  subscribe("browser.user_profile.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      profile_user: serializer.user(User.find(payload.fetch(:profile_user_id))),
      client_timestamp: payload[:client][:timestamp].try(:to_i),
      server_timestamp: Time.zone.now,
      originating_request_id: payload.fetch(:originating_request_id),
      target: payload.fetch(:target),
    }

    publish(message, schema: "github.v1.UserProfileClick")
  end

  subscribe("dashboard.page_view") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      currently_starred_repos_count: payload[:currently_starred_repos_count],
      currently_following_users_count: payload[:currently_following_users_count],
      events_shown: payload[:events_shown],
      banner_shown: payload[:banner_shown],
      banner_dismissable: payload[:banner_dismissable],
      page_type: payload[:page_type].to_s.upcase.to_sym.presence,
      dashboard_version: payload[:dashboard_version],
    }

    publish(message, schema: "github.v1.DashboardPageView")
  end

  subscribe("browser.clone_or_download.click") do |payload|
    repo = if payload[:repository_id]
      Repository.find_by(id: payload[:repository_id])
    end
    gist = if payload[:gist_id]
      Gist.find_by(id: payload[:gist_id])
    end
    owner = if repo && repo.owner
      serializer.user(repo.owner)
    elsif gist && gist.owner
      serializer.user(gist.owner)
    end
    message = {
      actor: serializer.user(payload[:client][:user]),
      feature_clicked: payload[:feature_clicked],
      owner: owner,
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      git_repository_type: payload[:git_repository_type],
    }
    message[:repository] = serializer.repository(repo) if repo
    message[:gist] = serializer.gist(gist) if gist
    publish(message, schema: "github.v1.CloneDownloadClick")
  end

  subscribe("browser.dashboard.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      event_context: payload[:event_context],
      target: payload[:target],
      record_id: payload[:record_id],
      dashboard_context: payload[:dashboard_context] == "user" ? :USER : :ORG,
      dashboard_version: payload[:dashboard_version],
    }

    publish(message, schema: "github.v1.DashboardClick")
  end

  subscribe("browser.force_push_timeline_diff.click") do |payload|
    message = {
      pull_request_id: payload[:pull_request_id],
      repository_id: payload[:repository_id],
      event_id: payload[:event_id],
      user_id: payload[:client]&.[](:user)&.id,
    }

    publish(message, schema: "github.v1.ForcePushTimelineClick")
  end

  subscribe("check_suite.status_changed") do |payload|
    message = {
      check_suite_id: payload[:check_suite_id],
      previous_status: payload[:previous_status],
      current_status: payload[:current_status],
      repository_id: payload[:repository_id],
      head_sha: payload[:head_sha],
      conclusion: payload[:conclusion],
      app: serializer.integration(payload[:app]),
    }

    publish(message, schema: "github.v1.CheckSuiteStatusChange")
  end

  subscribe("check_run.status_changed") do |payload|
    message = {
      check_run_id: payload[:check_run_id],
      previous_status: payload[:previous_status],
      current_status: payload[:current_status],
      previous_conclusion: payload[:previous_conclusion],
      current_conclusion: payload[:current_conclusion],
    }

    publish(message, schema: "github.v1.CheckRunStatusChange")
  end

  subscribe("comment.toggle_minimize") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      actor_was_staff: payload[:actor_was_staff],
      event_type: serializer.enum_from_string(payload[:event_type]),
      reason: payload[:reason],
      comment_id: payload[:comment_id],
      comment_type: serializer.enum_from_string(payload[:comment_type]),
      classifier: serializer.enum_from_string(payload[:classifier]),
      user: serializer.user(payload[:user]),
      request_context: serializer.request_context(GitHub.context.to_hash),
    }

    publish(message, schema: "github.v1.CommentToggleMinimize")
  end

  subscribe(/user\.beta_feature\.(enroll|unenroll)/) do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      feature: payload[:feature].upcase,
      action: payload[:action].upcase,
      organization: serializer.organization(payload[:organization]),
    }

    publish(message, schema: "github.v1.BetaFeatureEnrollmentEvent")
  end

  subscribe("browser.per_seat_plan_callout.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(User.find(payload[:actor_id])),
      organization: serializer.user(Organization.find(payload[:organization_id])),
      callout_page: payload[:callout_page],
      callout_location: payload[:callout_location],
      callout_text: payload[:callout_text],
      callout_destination: payload[:callout_destination],
      current_plan: payload[:current_plan],
    }

    publish(message, schema: "github.v1.PerSeatPlanCalloutClick")
  end

  subscribe("sso_page.view") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      org: serializer.organization(payload[:org]),
      business: serializer.business(payload[:business]),
    }

    publish(message, schema: "github.v1.SsoPageView")
  end

  subscribe("browser.suggested_changes.target.click") do |payload|
    target_type = if payload[:target_type]
      payload[:target_type].upcase.to_sym
    else
      :TARGET_TYPE_UNKNOWN
    end

    relationship_to_suggestion = if payload[:relationship_to_suggestion]
      payload[:relationship_to_suggestion].upcase.to_sym
    else
      :RELATIONSHIP_TYPE_UNKNOWN
    end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      target_type: target_type,
      pull_request_id: payload[:pull_request_id],
      relationship_to_suggestion: relationship_to_suggestion,
    }

    publish(message, schema: "github.v1.SuggestedChangesClick")
  end

  subscribe("pull_request_review_comment.create") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      repository_owner: serializer.user(payload[:repository_owner]),
      pull_request: serializer.pull_request(payload[:pull_request]),
      pull_request_review_comment: serializer.pull_request_review_comment(payload[:review_comment]),
      issue: serializer.issue(payload[:issue]),
      specimen_body: serializer.specimen_data(payload[:review_comment].body),
      pull_request_review: serializer.pull_request_review(payload[:pull_request_review]),
    }

    publish(message, schema: "github.v1.PullRequestReviewCommentCreate", publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("pull_request_review_comment.update") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      pull_request: serializer.pull_request(payload[:pull_request]),
      pull_request_review: serializer.pull_request_review(payload[:pull_request_review]),
      pull_request_review_comment: serializer.pull_request_review_comment(payload[:review_comment]),
      specimen_body: serializer.specimen_data(payload[:review_comment]&.body),
    }

    publish(message, schema: "github.v1.PullRequestReviewCommentUpdate", publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("pull_request_review_comment.delete") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      pull_request: serializer.pull_request(payload[:pull_request]),
      pull_request_review: serializer.pull_request_review(payload[:pull_request_review]),
      pull_request_review_comment: serializer.pull_request_review_comment(payload[:review_comment]),
    }

    publish(message, schema: "github.v1.PullRequestReviewCommentDelete")
  end

  subscribe("pull_request_review.submit") do |payload|
    review = payload[:review]

    message = {
      actor: serializer.user(review.user),
      repository: serializer.repository(review.repository),
      pull_request: serializer.pull_request(review.pull_request),
      pull_request_review: serializer.pull_request_review(review),
    }

    publish(message, schema: "github.v1.PullRequestReviewSubmit")
  end

  subscribe("pull_request_review.update") do |payload|
    review, old_body = payload.values_at(:review, :old_body)

    message = {
      actor: serializer.user(review.user),
      repository: serializer.repository(review.repository),
      pull_request: serializer.pull_request(review.pull_request),
      pull_request_review: serializer.pull_request_review(review),
      old_body: old_body,
    }

    publish(message, schema: "github.v1.PullRequestReviewEdit")
  end

  subscribe("pull_request_review.dismiss") do |payload|
    review, actor = payload.values_at(:review, :actor)

    message = {
      actor: serializer.user(actor),
      repository: serializer.repository(review.repository),
      pull_request: serializer.pull_request(review.pull_request),
      pull_request_review: serializer.pull_request_review(review),
    }

    publish(message, schema: "github.v1.PullRequestReviewDismiss")
  end

  subscribe("pull_request.review_request_delegated") do |payload|
    message = {
      organization: serializer.organization(payload[:organization]),
      team: serializer.team(payload[:team]),
      repository: serializer.repository(payload[:repository]),
      pull_request: serializer.pull_request(payload[:pull_request]),
      algorithm: payload[:algorithm].to_s.upcase,
    }

    publish(message, schema: "github.v1.PullRequestReviewRequestDelegation")
  end

  subscribe("token_scan.detection") do |payload|
    message = {
      repository: serializer.repository(payload[:repository]),
      actor: serializer.user(payload[:actor]),
      processed: payload[:processed],
      error: payload[:error],
      token: payload[:token],
      file_extension: payload[:file_extension],
    }

    Hydro::PublishRetrier.publish(message, schema: "github.v1.TokenScan")
  end

  subscribe("secret_scanning.error") do |payload|
    message = {
      request_context: payload[:request_context],
      actor: serializer.user(payload[:actor]),
      error_type: payload[:error_type],
      repository: serializer.repository(payload[:repository]),
    }

    Hydro::PublishRetrier.publish(message, schema: "github.v1.SecretScanningError")
  end

  subscribe("security_analysis.update") do |payload|
    message = {
      event_name: payload[:event_name],
      owner: serializer.user(payload[:owner]),
      actor: serializer.user(payload[:actor]),
    }

    Hydro::PublishRetrier.publish(message, schema: "github.security_settings.v0.SecuritySettingsUpdate")
  end

  subscribe("repository_advisory.create") do |payload|
    repository_advisory = payload[:repository_advisory]
    repo = repository_advisory.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      author: serializer.user(repository_advisory.author),
      repository: serializer.repository(repo),
      repository_owner: serializer.user(repo.owner),
      repository_advisory: serializer.repository_advisory(repository_advisory),
    }

    publish(message, schema: "github.v1.RepositoryAdvisoryCreate")
  end

  subscribe("repository_advisory.update") do |payload|
    repository_advisory = payload[:repository_advisory]
    repo = repository_advisory.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(repository_advisory.author),
      repository: serializer.repository(repo),
      repository_owner: serializer.user(repo.owner),
      repository_advisory: serializer.repository_advisory(repository_advisory),
    }

    publish(message, schema: "github.v1.RepositoryAdvisoryUpdate")

    if payload[:request_curation]
      publish(
        {
          actor: serializer.user(payload[:actor]),
          repository_advisory_content: serializer.repository_advisory_content(repository_advisory),
        },
        schema: "advisory_db.v0.RepositoryAdvisoryCurationRequest",
      )
    end
  end

  subscribe("repository_advisory.publish") do |payload|
    repository_advisory = payload[:repository_advisory]
    repo = repository_advisory.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(repo),
      repository_owner: serializer.user(repo.owner),
      repository_advisory: serializer.repository_advisory(repository_advisory),
    }

    publish(message, schema: "github.v1.RepositoryAdvisoryPublish")

    publish(
      {
        actor: serializer.user(payload[:actor]),
        repository_advisory_content: serializer.repository_advisory_content(repository_advisory),
      },
      schema: "advisory_db.v0.RepositoryAdvisoryCurationRequest",
    )
  end

  subscribe("repository_advisory.request_cve") do |payload|
    repository_advisory = payload[:repository_advisory]

    message = {
      actor: serializer.user(payload[:actor]),
      repository_advisory: serializer.repository_advisory(repository_advisory),
      title: repository_advisory.title,
      description: repository_advisory.description,
      ecosystem: repository_advisory.ecosystem,
      package: repository_advisory.package,
      affected_versions: repository_advisory.affected_versions,
      patches: repository_advisory.patches,
      cve_id: repository_advisory.cve_id,
    }

    publish(message, schema: "advisory_db.v0.CVERequest")
  end

  subscribe("repository_advisory.workspace_open") do |payload|
    repository_advisory = payload[:repository_advisory]
    repo = repository_advisory.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(repo),
      repository_owner: serializer.user(repo.owner),
      repository_advisory: serializer.repository_advisory(repository_advisory),
      workspace: serializer.repository(repository_advisory.workspace_repository),
    }

    publish(message, schema: "github.v1.RepositoryAdvisoryWorkspaceOpen")
  end

  subscribe("repository_dependency_update.created.vulnerability") do |payload|
    repository_dependency_update = payload[:repository_dependency_update]
    repository_vulnerability_alert = payload[:repository_vulnerability_alert]
    security_advisory = payload[:security_advisory]
    security_vulnerability = payload[:security_vulnerability]

    message = {
      repository_vulnerability_alert: repository_vulnerability_alert.hydro_entity_payload,
      repository: serializer.repository(repository_vulnerability_alert.repository),
      owner: serializer.user(repository_vulnerability_alert.repository.owner),
      security_advisory: security_advisory.hydro_entity_payload,
      security_vulnerability: security_vulnerability.hydro_entity_payload,
      trigger: repository_dependency_update.trigger_type,
      description: security_advisory.description,
      github_bot_install_id: payload[:github_bot_install_id].to_s,
      repository_dependency_update_id: repository_dependency_update.id.to_s,
      dry_run: repository_dependency_update.dry_run?,
    }

    schema = if repository_dependency_update.manual?
      "github.v1.RepositoryVulnerabilityAlertResolveRequest"
    else
      "github.v1.RepositoryVulnerabilityAlertCreate"
    end

    publish(message, schema: schema)
  end

  subscribe("repository_dependency_update.created") do |payload|
    message = {
      repository_dependency_update: payload[:repository_dependency_update].hydro_entity_payload,
      repository: serializer.repository(payload[:repository]),
    }

    publish(message, schema: "github.v1.RepositoryDependencyUpdateCreated")
  end

  subscribe("repository_dependency_update.complete") do |payload|
    message = {
      repository_dependency_update: payload[:repository_dependency_update].hydro_entity_payload,
      repository: serializer.repository(payload[:repository]),
      pull_request: serializer.pull_request(payload[:pull_request]),
    }

    publish(message, schema: "github.v1.RepositoryDependencyUpdateComplete")
  end

  subscribe("repository_dependency_update.errored") do |payload|
    message = {
      repository_dependency_update: payload[:repository_dependency_update].hydro_entity_payload,
      repository: serializer.repository(payload[:repository]),
    }

    publish(message, schema: "github.v1.RepositoryDependencyUpdateErrored")
  end

  subscribe("repository_dependency_update.cleaned_up") do |payload|
    message = {
      repository_dependency_update: payload[:repository_dependency_update].hydro_entity_payload,
      repository: serializer.repository(payload[:repository]),
    }

    publish(message, schema: "github.v1.RepositoryDependencyUpdateCleanedUp")
  end

  subscribe("repository_dependency_updates.vulnerabilities.enabled") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      repository: serializer.repository(payload[:repository]),
    }

    if payload[:org]
      message[:org] = serializer.organization(payload[:org])
    end

    publish(message, schema: "github.v1.RepositoryDependencyUpdatesVulnerabilitiesEnabled")
  end

  subscribe("repository_dependency_updates.vulnerabilities.disabled") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      repository: serializer.repository(payload[:repository]),
    }

    if payload[:org]
      message[:org] = serializer.organization(payload[:org])
    end

    publish(message, schema: "github.v1.RepositoryDependencyUpdatesVulnerabilitiesDisabled")
  end

  subscribe("repository.push_vulnerability_notification") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
    }

    publish(message, schema: "github.v1.RepositoryPushVulnerabilityNotification")
  end

  subscribe("delete_manifest.repository") do |payload|
    manifest_file = {
      filename: payload[:manifest_file][:filename],
      path: payload[:manifest_file][:path],
      git_ref: payload[:manifest_file][:git_ref].to_s,
      pushed_at: payload[:manifest_file][:pushed_at],
    }

    message = {
      repository_id: payload[:repository_id],
      repository_private: payload[:repository_private],
      repository_fork: payload[:repository_fork],
      manifest_file: manifest_file,
    }

    Hydro::PublishRetrier.publish(message, schema: "github.dependencygraph.v0.RepositoryManifestFileDeleted")
  end

  subscribe("integration.create") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      integration: serializer.integration(payload[:integration]),
      actor: serializer.user(payload[:actor]),
      owner: serializer.user(payload[:owner]),
      from_manifest: payload[:from_manifest],
    }

    if payload[:from_manifest]
      manifest_json = JSON.generate(payload[:manifest])
      message[:manifest_json] = manifest_json
      message[:manifest_hash] = Digest::SHA256.base64digest(manifest_json)
    end

    publish(message, schema: "github.v1.IntegrationCreate")
  end

  subscribe("suggested_change.applied") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      repository_owner: serializer.user(payload[:repository_owner]),
      comment: serializer.pull_request_review_comment(payload[:comment]),
      pull_request: serializer.pull_request(payload[:pull_request]),
      issue: serializer.issue(payload[:issue]),
      file_extension: payload[:file_extension],
      error: payload[:error],
      commit_sha: payload[:commit_sha],
    }

    publish(message, schema: "github.v1.SuggestedChangeApplied")
  end

  subscribe("branch_protection_rule.create") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      repository_owner: serializer.user(payload[:repository_owner]),
      branch_protection_rule: serializer.branch_protection_rule(payload[:branch_protection_rule]),
      action: :CREATE,
    }

    publish(message, schema: "github.v1.BranchProtectionRuleChange")
  end

  subscribe("branch_protection_rule.update") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      repository_owner: serializer.user(payload[:repository_owner]),
      branch_protection_rule: serializer.branch_protection_rule(payload[:branch_protection_rule]),
      action: :UPDATE,
    }

    publish(message, schema: "github.v1.BranchProtectionRuleChange")
  end

  subscribe("branch_protection_rule.destroy") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      repository_owner: serializer.user(payload[:repository_owner]),
      branch_protection_rule: serializer.branch_protection_rule(payload[:branch_protection_rule]),
      action: :DESTROY,
    }

    publish(message, schema: "github.v1.BranchProtectionRuleChange")
  end

  subscribe("stratocaster.timeline_update") do |payload|
    message = {
      index_key: payload[:index_key],
      event_id: payload[:event_id],
      current_ref: GitHub.current_ref,
    }

    options = {
      schema: Stratocaster::TIMELINE_UPDATE_SCHEMA,
      partition_key: payload[:index_key],
    }

    if GitHub.dynamic_lab?
      options[:topic] = Stratocaster::REVIEW_LAB_TIMELINE_TOPIC
    end

    GitHub.stratocaster.publish_to_hydro do
      publish_without_error_reporting(message, **options)
    end
  end

  subscribe("browser.feature_gate_upsell.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      feature_name: payload[:feature_name]&.upcase,
    }

    publish(message, schema: "github.v1.FeatureGateUpsellClick")
  end

  subscribe("newsletter.preference.change") do |payload|
    publish(payload, schema: "github.v1.NewsletterPreferenceChange")
  end

  subscribe("content_reference.create") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      content_reference_id: payload[:content_reference_id],
      repository: serializer.repository(payload[:repository]),
      content_reference_url: payload[:content_reference_url],
      content_context: serializer.content_reference_content_context(payload[:content]),
      content_id: payload[:content].id,
    }

    publish(message, schema: "github.v1.ContentReferenceCreate")
  end

  subscribe("content_attachment.create") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      integration: serializer.integration(payload[:integration]),
      content_reference_url: payload[:content_reference_url],
      repository: serializer.repository(payload[:repository]),
      content_context: serializer.content_reference_content_context(payload[:content]),
      content_id: payload[:content].id,
      content_title: payload[:title],
      content_body: payload[:body],
    }

    publish(message, schema: "github.v1.ContentAttachmentCreate")
  end

  subscribe("blob.excerpt") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      path: payload[:path],
      blob_oid: payload[:blob_oid],
      first_line: payload[:first_line],
      last_line: payload[:last_line],
      direction: payload[:direction],
    }

    publish(message, schema: "github.v1.BlobExcerptLoaded")
  end

  subscribe("blob.edit.page_view") do |payload|
    path = payload[:path]&.dup&.force_encoding(Encoding::UTF_8)
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      target_repository: serializer.repository(payload[:target_repository]),
      branch: payload[:branch]&.dup&.force_encoding(Encoding::UTF_8),
      can_commit_to_branch: payload[:can_commit_to_branch],
      path: path,
      file_extension: serializer.file_extension(path),
    }

    publish(message, schema: "github.v1.BlobEditPageView")
  end

  subscribe("blob.new.page_view") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      target_repository: serializer.repository(payload[:target_repository]),
      branch: payload[:branch]&.dup&.force_encoding(Encoding::UTF_8),
      can_commit_to_branch: payload[:can_commit_to_branch],
    }

    publish(message, schema: "github.v1.BlobNewPageView")
  end

  subscribe(/blob\.(new|edit|delete)\.succeeded/) do |payload|
    path = payload[:path]&.dup&.force_encoding(Encoding::UTF_8)
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      target_repository: serializer.repository(payload[:target_repository]),
      branch: payload[:branch]&.dup&.force_encoding(Encoding::UTF_8),
      can_commit_to_branch: payload[:can_commit_to_branch],
      target_branch: payload[:target_branch]&.dup&.force_encoding(Encoding::UTF_8),
      path: path,
      file_extension: serializer.file_extension(path),
      action: serializer.enum_from_string(payload[:action]),
    }

    publish(message, schema: "github.v1.BlobActionSucceeded")
  end

  subscribe(/\Aproject\.(create|delete|open|close)\z/) do |payload|
    actions = { create: "CREATE", delete: "DELETE", open: "OPEN", close: "CLOSE" }

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      project: serializer.project(payload[:project]),
      project_owner_user: serializer.user(payload[:owner_user]),
      project_owner_repo: serializer.repository(payload[:owner_repo]),
      action: actions.fetch(payload[:action], "ACTION_UNKNOWN"),
    }

    publish(message, schema: "github.v1.ProjectEvent")
  end

  subscribe(/\Aproject_card\.(create|delete)\z/) do |payload|
    actions = { create: "CREATE", delete: "DELETE" }
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      project: serializer.project(payload[:project]),
      card: serializer.project_card(payload[:card]),
      action: actions.fetch(payload[:action], "ACTION_UNKNOWN"),
    }

    publish(message, schema: "github.v1.ProjectCardEvent")
  end

  subscribe("project_card.move") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      automated: payload[:automated],
      project: serializer.project(payload[:project]),
      card: serializer.project_card(payload[:card]),
      source_column: serializer.project_column(payload[:source_column]),
      target_column: serializer.project_column(payload[:target_column]),
    }

    publish(message, schema: "github.projects.v0.ProjectCardMove")
  end

  subscribe(/\Aaqueduct\.double-publish\z/) do |payload|
    message = payload.slice(:application, :endpoint, :duration)
    publish(message, schema: "aqueduct.v0.ClientTiming")
  end

  subscribe("pages.build") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:actor]),
      page: serializer.page(payload[:page]),
    }
    publish(message, schema: "github.v1.RepositoryPagesBuild")
  end

  subscribe("pages.cname_change") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:actor]),
      page: serializer.page(payload[:page]),
      cname: payload[:cname]&.to_s,
      old_cname: payload[:old_cname]&.to_s,
    }
    publish(message, schema: "github.v1.RepositoryPagesCnameChange")
  end

  subscribe("pages.visibility_change") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:actor]),
      page: serializer.page(payload[:page]),
      public: payload[:public]
    }
    publish(message, schema: "github.v1.RepositoryPagesVisibilityChange")
  end

  subscribe("browser.authentication.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      auth_type: payload[:auth_type].presence || :AUTH_TYPE_UNKNOWN,
      location_in_page: payload[:location_in_page],
    }
    if payload[:repository_id]
      repo = Repository.find_by(id: payload[:repository_id])
      message[:repository] = serializer.repository(repo) if repo
      message[:owner] = serializer.user(repo.owner) if repo&.owner
    end
    publish(message, schema: "github.v1.AuthenticationClick")
  end

  subscribe("browser.repository.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      target: payload[:target],
    }
    if payload[:repository_id]
      repo = Repository.find_by(id: payload[:repository_id])
      message[:repository] = serializer.repository(repo) if repo
      message[:owner] = serializer.user(repo.owner) if repo&.owner
    end
    publish(message, schema: "github.v1.RepositoryClick")
  end

  subscribe("browser.repository_create.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      target: payload[:event_target],
    }

    publish(message, schema: "github.v1.RepositoryCreateClick")
  end

  subscribe("notifications.delivery") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: payload[:handler].to_s.upcase,
      reason: payload[:reason],
    }

    publish(message, schema: "github.notifications.v0.NotificationDelivery")
  end

  subscribe("notifications.read") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: payload[:handler].to_s.upcase,
      action: "READ",
      version: payload.has_key?(:version) ? payload[:version].to_s.upcase : nil, # If key doesn't exist use default value
    }

    publish(message, schema: "github.notifications.v0.NotificationUserAction")
  end

  subscribe("notifications.unread") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: payload[:handler].to_s.upcase,
      action: "UNREAD",
      version: payload.has_key?(:version) ? payload[:version].to_s.upcase : nil, # If key doesn't exist use default value
    }

    publish(message, schema: "github.notifications.v0.NotificationUserAction")
  end

  subscribe("notifications.archive") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: payload[:handler].to_s.upcase,
      action: "ARCHIVE",
      version: payload.has_key?(:version) ? payload[:version].to_s.upcase : nil, # If key doesn't exist use default value
    }

    publish(message, schema: "github.notifications.v0.NotificationUserAction")
  end

  subscribe("notifications.unarchive") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: payload[:handler].to_s.upcase,
      action: "UNARCHIVE",
      version: payload.has_key?(:version) ? payload[:version].to_s.upcase : nil, # If key doesn't exist use default value
    }

    publish(message, schema: "github.notifications.v0.NotificationUserAction")
  end

  subscribe("notifications.unsubscribe") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: payload[:handler].to_s.upcase,
      action: "UNSUBSCRIBE",
      version: payload.has_key?(:version) ? payload[:version].to_s.upcase : nil, # If key doesn't exist use default value
    }

    publish(message, schema: "github.notifications.v0.NotificationUserAction")
  end

  subscribe("notifications.star") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: payload[:handler].to_s.upcase,
      action: "STAR",
      version: payload.has_key?(:version) ? payload[:version].to_s.upcase : nil, # If key doesn't exist use default value
    }

    publish(message, schema: "github.notifications.v0.NotificationUserAction")
  end

  subscribe("notifications.unstar") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: payload[:handler].to_s.upcase,
      action: "UNSTAR",
      version: payload.has_key?(:version) ? payload[:version].to_s.upcase : nil, # If key doesn't exist use default value
    }

    publish(message, schema: "github.notifications.v0.NotificationUserAction")
  end

  subscribe("browser.notifications.read") do |payload|
    user = payload[:client][:user]

    message = {
      user: serializer.user(user),
      list_type: payload[:list_type],
      list_id: payload[:list_id].to_s,
      thread_type: payload[:thread_type],
      thread_id: payload[:thread_id].to_s,
      comment_type: payload[:comment_type],
      comment_id: payload[:comment_id].to_s,
      handler: "WEB",
      action: "READ",
      version: payload.has_key?(:version) ? payload[:version].to_s.upcase : nil, # If key doesn't exist use default value
    }

    publish(message, schema: "github.notifications.v0.NotificationUserAction")
  end

  subscribe("github_connect.connect") do |payload|
    message = {
      enterprise_installation: serializer.enterprise_installation(payload[:enterprise_installation]),
    }
    publish(message, schema: "github.github_connect.v0.Connect")
  end

  subscribe("github_connect.update_features") do |payload|
    message = {
      enterprise_installation: serializer.enterprise_installation(payload[:enterprise_installation]),
      features: Array.wrap(payload[:features]),
      actor: serializer.user(payload[:actor]),
    }
    publish(message, schema: "github.github_connect.v0.UpdateFeatures")
  end

  subscribe "team_sync_tenant.status_change" do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      organization: serializer.organization(payload[:organization]),
      team_sync_tenant: serializer.team_sync_tenant(payload[:team_sync_tenant]),
      previous_status: serializer.team_sync_tenant_status_mapping(payload[:previous_status]),
      current_status: serializer.team_sync_tenant_status_mapping(payload[:current_status]),
    }
    publish(message, schema: "github.v1.TeamSyncTenantStatusChange")
  end

  subscribe "team_group_mapping.update" do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      team: serializer.team(payload[:team]),
      organization: serializer.organization(payload[:organization]),
      team_sync_tenant: serializer.team_sync_tenant(payload[:team_sync_tenant]),
      current_mappings: Array(payload[:current_mappings]).map { |mapping| serializer.team_group_mapping(mapping) },
      previous_group_ids: Array(payload[:previous_group_ids]),
    }
    publish(message, schema: "github.v1.TeamGroupMappingUpdate")
  end

  subscribe("suggested_username.publish") do |payload|
    message = {
      suggestion_count: payload[:suggestion_count],
      action: payload[:action],
      actor: {
        user_id: payload[:actor][:user_id],
        login: payload[:actor][:login],
      },
    }

    publish(message, schema: "github.suggested_username.v0.SuggestedUsername")
  end

  subscribe("browser.organization.new.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
      overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      target: payload[:target],
    }

    publish(message, schema: "github.v1.OrganizationNewClick")
  end

  subscribe("browser.issue_cross_references.click") do |payload|
    message = {
      actor: serializer.user(User.find_by(id: payload[:user_id])),
      issue: serializer.issue(Issue.find_by(id: payload[:issue_id])),
      pull_request: serializer.pull_request(PullRequest.find_by(id: payload[:pull_request_id])),
      reference_location: payload[:reference_location],
      request_context: serializer.request_context(GitHub.context.to_hash),
    }
    publish(message, schema: "github.v1.IssueClosedByXrefsClick")
  end

  subscribe("browser.issue_timeline_filter_menu.click") do |payload|
    message = {
      actor: serializer.user(User.find_by(id: payload[:user_id])),
      issue: serializer.issue(Issue.find_by(id: payload[:issue_id])),
      filter_selection: payload[:filter_selection],
      request_context: serializer.request_context(GitHub.context.to_hash),
    }
    publish(message, schema: "github.v1.IssueTimelineFilterClick")
  end

  subscribe("browser.show_annotation.click") do |payload|
    pr = PullRequest.find_by(id: payload[:pull_request_id])
    repo = Repository.find_by(id: payload[:repository_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      pull_request: serializer.pull_request(pr),
      repository: serializer.repository(repo),
      actor: serializer.user(payload[:client][:user]),
      non_diff_entry: payload[:non_diff_entry],
    }

    publish(message, schema: "github.v1.ShowAnnotationClick")
  end

  subscribe("actions.dreamlifter_joined_waitlist") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
    }

    publish(message, schema: "github.actions.v0.DreamlifterJoinedWaitlist")
  end

  subscribe("mobile.joined_waitlist") do |payload|
    message = {
      is_android: payload[:is_android],
      is_ios: payload[:is_ios],
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
    }

    publish(message, schema: "github.mobile.v0.WaitlistRegistration")
  end

  subscribe("actions.artifact_storage_event") do |payload|
    message = {
      artifact_id: payload[:artifact_id],
      artifact_name: payload[:artifact_name],
      artifact_repository_owner_id: payload[:artifact_repository_owner_id],
      artifact_repository_id: payload[:artifact_repository_id],
      artifact_repository_visibility: serializer.enum_from_string(payload[:artifact_repository_visibility]),
      artifact_size_in_bytes: payload[:artifact_size_in_bytes],
      check_suite_id: payload[:check_suite_id],
      check_run_id: payload[:check_run_id],
      artifact_event_type: serializer.enum_from_string(payload[:event_type]),
      expires_at: payload[:expires_at],
      previously_expired_at: payload[:previously_expired_at],
    }

    publish(message, schema: "github.actions.v0.ArtifactStorageEvent")
  end

  subscribe("browser.actions.onboarding_setup_workflow_click") do |payload|
    repository = Repository.find_by(id: payload[:repository_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      repository: serializer.repository(repository),
      workflow_template: payload[:workflow_template],
    }

    publish(message, schema: "github.actions.v0.OnboardingSetupWorkflowClick")
  end

  subscribe("browser.actions.setup_actions_popover_click") do |payload|
    repository = Repository.find_by(id: payload[:repository_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      repository: serializer.repository(repository),
      click_type: serializer.enum_from_string(payload[:click_type]),
    }

    publish(message, schema: "github.actions.v0.SetupActionsPopoverClick")
  end

  subscribe("browser.actions.workflow_upgrade_popover_click") do |payload|
    repository = Repository.find_by(id: payload[:repository_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      repository: serializer.repository(repository),
      location: payload[:location],
    }

    publish(message, schema: "github.actions.v0.WorkflowUpgradePopoverClick")
  end

  subscribe("composable_comments.create") do |payload|
     message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      app: serializer.integration(payload[:app]),
      repository: serializer.repository(payload[:repository]),
      issue: serializer.issue(payload[:issue]),
      composable_comment: serializer.composable_comment(payload[:composable_comment]),
    }

    publish(message, schema: "github.v1.ComposableCommentCreate")
  end

  subscribe("composable_comments.update") do |payload|
     message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      app: serializer.integration(payload[:app]),
      repository: serializer.repository(payload[:repository]),
      issue: serializer.issue(payload[:issue]),
      previous_components: payload[:previous_components],
      composable_comment: serializer.composable_comment(payload[:composable_comment]),
    }

    publish(message, schema: "github.v1.ComposableCommentUpdate")
  end

  subscribe("composable_comments.delete") do |payload|
     message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      app: serializer.integration(payload[:app]),
      repository: serializer.repository(payload[:repository]),
      issue: serializer.issue(payload[:issue]),
      composable_comment: serializer.composable_comment(payload[:composable_comment]),
    }

    publish(message, schema: "github.v1.ComposableCommentDelete")
  end

  subscribe("interactive_components.button_click") do |payload|
     message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      app: serializer.integration(payload[:app]),
      interactive_component: serializer.interactive_component(payload[:interactive_component]),
    }

    publish(message, schema: "github.v1.InteractiveComponentButtonClick")
  end

  subscribe("webhook.delivery_metadata") do |payload|
    hook, hook_event, hook_payload = payload.values_at(:hook, :hook_event, :hook_payload)

    message = ActiveRecord::Base.connected_to(role: :reading) do
      {
        delivery_guid: hook_payload[:guid],
        hook_id: hook.id,
        hook_url: payload[:safe_hook_url],
        hook_event: hook_event.event_type,
        hook_action: hook_event.try(:action),
        hook_actor: serializer.user(hook_event.actor),
        hook_installation_target_type: hook.installation_target_type.underscore.upcase.gsub("/", "_").to_sym,
        hook_installation_target_id: hook.installation_target_id,
        hook_creator: serializer.user(hook.creator),
        hook_oauth_application: serializer.app(hook.oauth_application),
        hook_payload_bytes: hook_payload.to_json.bytesize,
        hook_target_repository: serializer.repository(hook_event.target_repository),
        hook_target_organization: serializer.organization(hook_event.target_organization),
        hook_integration_installation_id: hook_payload.dig(:payload, :installation, :id),
      }
    end

    publish(message, schema: "github.webhooks.v0.DeliveryMetadata")
  end

  subscribe("organization.bulk.invite") do |payload|
     message = {
      actor: serializer.user(payload[:actor]),
      organization: serializer.organization(payload[:organization]),
      successful_user_invites: payload[:successful_user_invites],
      successful_email_invites: payload[:successful_email_invites],
      failed_user_invites: payload[:failed_user_invites],
      failed_email_invites: payload[:failed_email_invites],
    }

    publish(message, schema: "github.v1.OrganizationBulkInvite")
  end

  subscribe("browser.organization.invite.click") do |payload|
    organization = Organization.find_by(id: payload[:organization_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      organization: serializer.organization(organization),
      event_context: payload[:event_type],
      invitee: payload[:invitee],
    }

    publish(message, schema: "github.v1.OrganizationInviteClick")
  end

  subscribe("repository.commit_status") do |payload|
    status = payload[:status]

    message = {
      database_id: status.id,
      context: status.context,
      commit_sha: status.sha,
      state: status.state.upcase,
      repository_id: status.repository_id,
      branch_names: status.branches.map { |name| name.dup.force_encoding("UTF-8") },
    }

    publish(message, schema: "github.v1.RepositoryCommitStatusCreated")
  end

  subscribe("label.create") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      label: serializer.label(payload[:label]),
      issue_id: payload[:issue_id],
    }

    publish(message, schema: "github.v1.LabelCreate")
  end

  subscribe("label.update") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      label: serializer.label(payload[:label]),
    }

    publish(message, schema: "github.v1.LabelUpdate")
  end

  subscribe("label.delete") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      label: serializer.label(payload[:label]),
    }

    publish(message, schema: "github.v1.LabelDelete")
  end

  subscribe("wiki.create") do |payload|
    wiki_content = [payload[:page_name], payload[:page_body]].compact.join(" ")

    message = {
      actor: serializer.user(payload[:actor]),
      page_body: payload[:page_body],
      page_name: payload[:page_name],
      page_url: payload[:page_url],
      repository: serializer.repository(payload[:repository]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      repository_owner: serializer.repository_owner(payload[:repository]),
      actor_is_member: serializer.user_repository_member?(payload[:repository], payload[:actor]),
      page_body_size: serializer.string_bytesize(payload[:page_body]),
      specimen_wiki_content: serializer.specimen_data(wiki_content),
    }

    publish(message, schema: "github.v1.WikiCreate")
  end

  subscribe("wiki.update") do |payload|
    wiki_content = [payload[:page_name], payload[:page_body]].compact.join(" ")

    message = {
      actor: serializer.user(payload[:actor]),
      page_body: payload[:page_body],
      page_name: payload[:page_name],
      page_url: payload[:page_url],
      repository: serializer.repository(payload[:repository]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      repository_owner: serializer.repository_owner(payload[:repository]),
      actor_is_member: serializer.user_repository_member?(payload[:repository], payload[:actor]),
      page_body_size: serializer.string_bytesize(payload[:page_body]),
      specimen_wiki_content: serializer.specimen_data(wiki_content),
    }

    publish(message, schema: "github.v1.WikiUpdate")
  end

  subscribe("browser.contact_link.click") do |payload|
    message = {
      actor: serializer.user(User.find_by(id: payload[:actor_id])),
      repository: serializer.repository(Repository.find_by(id: payload[:repository_id])),
      request_context: serializer.request_context(GitHub.context.to_hash),
      contact_link: serializer.contact_link(payload[:contact_link]),
    }

    publish(message, schema: "github.v1.ContactLinkClick")
  end

  subscribe("browser.blank_issue.click") do |payload|
    message = {
      actor: serializer.user(User.find_by(id: payload[:actor_id])),
      repository: serializer.repository(Repository.find_by(id: payload[:repository_id])),
      request_context: serializer.request_context(GitHub.context.to_hash),
      blank_issue_clicked: payload[:blank_issue_clicked],
    }

    publish(message, schema: "github.v1.BlankIssueClick")
  end

  subscribe("browser.feature_preview.clicks.open_modal") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      link_location: serializer.enum_from_string(payload[:link_location]),
    }

    publish(message, schema: "github.v1.FeaturePreviewOpenModalClick")
  end

  subscribe("browser.feature_preview.clicks.close_modal") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
    }

    publish(message, schema: "github.v1.FeaturePreviewCloseModalClick")
  end

  subscribe("browser.feature_preview.clicks.feedback") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      feature: serializer.feature(Feature.find_by(slug: payload[:feature_slug])),
      display_location: serializer.enum_from_string(payload[:display_location]),
    }

    publish(message, schema: "github.v1.FeaturePreviewFeedbackClick")
  end

  subscribe("browser.feature_preview.clicks.documentation") do |payload|
    feature = Feature.find_by(slug: payload[:feature_slug])
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:client][:user]),
      feature: serializer.feature(feature),
      documentation_link: feature&.documentation_link,
    }

    publish(message, schema: "github.v1.FeaturePreviewDocumentationClick")
  end

  subscribe("commit_comment.create") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      actor_is_member: serializer.user_repository_member?(payload[:repository], payload[:actor]),
      body: payload[:commit_comment].body,
      commit_comment: serializer.commit_comment(payload[:commit_comment]),
      repository: serializer.repository(payload[:repository]),
      repository_owner: serializer.repository_owner(payload[:repository]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_body: serializer.specimen_data(payload[:commit_comment]&.body),
    }

    publish(message, schema: "github.v1.CommitCommentCreate", publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("commit_comment.update") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      actor_is_member: serializer.user_repository_member?(payload[:repository], payload[:actor]),
      current_body: payload[:commit_comment].body,
      commit_comment: serializer.commit_comment(payload[:commit_comment]),
      previous_body: payload[:previous_body],
      repository: serializer.repository(payload[:repository]),
      repository_owner: serializer.repository_owner(payload[:repository]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_body: serializer.specimen_data(payload[:commit_comment]&.body),
    }

    publish(message, schema: "github.v1.CommitCommentUpdate", publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("key_link.create") do |payload|
    key_link = payload[:key_link]

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(key_link.owner),
      repository_owner: serializer.user(key_link.owner.owner),
      prefix: key_link.key_prefix,
      template: key_link.url_template,
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamuri_form_signals]),
    }

    publish(message, schema: "github.v1.KeyLinkCreate")
  end

  subscribe("user_asset.uploaded") do |payload|
    user_asset = payload[:user_asset]

    message = {
      actor: serializer.user(user_asset.uploader),
      user_asset: serializer.user_asset(user_asset),
      upload_ip: serializer.ip_address(payload[:upload_ip]),
    }

    publish(message, schema: "github.v1.UserAssetScan")
  end

  subscribe("user_asset.scan_requested") do |payload|
    user_asset = payload[:user_asset]

    message = {
      actor: serializer.user(user_asset.uploader),
      user_asset: serializer.user_asset(user_asset),
      upload_ip: serializer.ip_address(payload[:upload_ip]),
    }

    publish(message, schema: "github.v1.UserAssetScan")
  end

  subscribe("two_factor_account_recovery.updated") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      action_type: payload[:action_type] || :ACTION_TYPE_UNKNOWN,
      evidence_type: payload[:evidence_type] || :EVIDENCE_TYPE_UNKNOWN,
    }

    publish(message, schema: "github.v1.TwoFactorRecoveryRequestChanged")
  end

  subscribe("automated_two_factor_account_recovery_request_review") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      review_required: payload[:review_required],
      reason_type: payload[:reason_type] || :REASON_TYPE_UNKNOWN,
      audit_log_event_type: payload[:audit_log_event_type],
      email_status_type: payload[:email_status_type] || :EMAIL_STATUS_TYPE_UNKNOWN,
      org_membership_type: payload[:org_membership_type] || :ORGANIZATION_MEMBERSHIP_TYPE_UNKNOWN
    }

    publish(message, schema: "github.v1.AutomatedTwoFactorRecoveryRequestReview")
  end

  subscribe("browser.release_compare.click") do |payload|
    actor = User.find_by(id: payload.fetch(:actor_id))
    repository = Repository.find_by(id: payload.fetch(:repository_id))

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(actor),
      repository: serializer.repository(repository),
      current_tag_name: payload[:current_tag],
      previous_tag_name: payload[:previous_tag],
      location: payload[:location],
    }

    publish(message, schema: "github.v1.ReleaseCompareLinkClick")
  end

  subscribe("security_incident_response.remediation_complete") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      account: serializer.user(payload[:account]),
      account_global_relay_id: payload[:account_id],
      successful_remediations: payload[:successful_remediations],
      failed_remediations: payload[:failed_remediations],
      errors: payload[:errors],
      staffnote: payload[:staffnote],
      email_body_template: payload[:email_body_template],
      incident_response_id: payload[:incident_response_id],
      batch_count: payload[:batch_count],
      batch_index: payload[:batch_index],
      inputs: payload[:inputs],
    }

    publish(message, schema: "github.v1.SecurityIncidentRemediation")
  end

  subscribe("browser.settings_context_dropdown.click") do |payload|
    user = User.find_by(id: payload[:user_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(user),
      event_context: payload[:event_context],
      target: payload[:target],
      target_name: payload[:target_name],
      target_id: payload[:target_id],
      target_link: payload[:target_link]
    }

    publish(message, schema: "github.v1.SettingsContextDropdown")
  end

  subscribe("performed.job") do |payload|
    backend = case payload[:backend]
    when :resque then :RESQUE
    when :aqueduct then :AQUEDUCT
    else nil
    end

    time_since_boot = Process.clock_gettime(Process::CLOCK_MONOTONIC).to_i - GitHub::BootMetrics.initial_boot_time.to_i

    message = {
      queue: payload[:queue],
      backend: backend,
      job_class: payload[:job_class],
      active_job_id: payload[:active_job_id],
      success: payload[:success],
      execution_time_ms: payload[:timer]&.elapsed_ms,
      cpu_time_ms: payload[:timer]&.elapsed_cpu_ms,
      idle_time_ms: payload[:timer]&.elapsed_idle_ms,
      hostname: GitHub.local_host_name,
      pid: Process.pid,
      process_runtime_seconds: time_since_boot,
      current_ref: GitHub.current_ref,
      revision: GitHub.current_sha,
      mysql_queries: GitHub::MysqlInstrumenter.query_count,
      mysql_time_ms: (GitHub::MysqlInstrumenter.query_time * 1000).round,
      gitrpc_count: GitRPCLogSubscriber.rpc_count,
      gitrpc_time_ms: (GitRPCLogSubscriber.rpc_time * 1000).round,
    }

    initial_mem_usage, current_mem_usage = GitHub::JobStats.memory_usage_report
    if initial_mem_usage.present? && current_mem_usage.present?
      delta = current_mem_usage.delta(initial_mem_usage)
      message.merge!({
        memory_usage_recorded: true,
        memory_rss: current_mem_usage.rss,
        memory_shared: current_mem_usage.shared,
        memory_private: current_mem_usage.priv,
        memory_rss_delta: delta.rss,
        memory_shared_delta: delta.shared,
        memory_private_delta: delta.priv,
      })
    end

    if GitHub::JobStats.enqueued.any?
      message[:jobs_enqueued] = GitHub::JobStats.enqueued.transform_keys(&:to_s)
    end

    publish(message, schema: "github.v1.JobComplete")
  end

  subscribe("dependency_graph.request_snapshot") do |payload|
    message = {
      push_id: payload[:push_id],
      before_sha: payload[:before_sha],
      sha: payload[:sha],
      ref: payload[:ref],
      pushed_at: payload[:pushed_at],
      repository: serializer.repository(payload[:repository]),
      owner_name: payload[:owner_name],
      manifest_files: payload[:manifest_files],
    }

    publish(message, schema: "github.dependencygraph.v0.RequestPushSnapshot")
  end

  subscribe("browser.global_header.user_menu_dropdown.click") do |payload|
    user = User.find_by(id: payload[:user_id])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(user),
      target: payload[:target],
      request_url: payload[:request_url],
    }

    publish(message, schema: "github.v1.GlobalHeaderUserMenuClick")
  end
end
