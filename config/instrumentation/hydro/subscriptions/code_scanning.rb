# frozen_string_literal: true

# These are Hydro event subscriptions related to Code Scanning.

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("code_scanning.new_analysis") do |payload|
    payload.freeze
    repo_id = payload[:repository_id]
    publish(payload, schema: "turboscan.v0.Analysis", topic: "turboscan.v0.NewAnalysis", partition_key: repo_id)
  end

  subscribe("codescanning.status_report") do |params|
    # Most params come in from JSON as string keys,
    # but are expected as symbol keys by instrumenter.
    message = {
      action_name: params["action_name"],
      action_oid: params["action_oid"],
      action_started_at: params["action_started_at"],
      autobuild_failure: params["autobuild_failure"],
      autobuild_languages: params["autobuild_languages"],
      analyze_builtin_queries_cpp_duration_ms: params["analyze_builtin_queries_cpp_duration_ms"],
      analyze_builtin_queries_csharp_duration_ms: params["analyze_builtin_queries_csharp_duration_ms"],
      analyze_builtin_queries_go_duration_ms: params["analyze_builtin_queries_go_duration_ms"],
      analyze_builtin_queries_java_duration_ms: params["analyze_builtin_queries_java_duration_ms"],
      analyze_builtin_queries_javascript_duration_ms: params["analyze_builtin_queries_javascript_duration_ms"],
      analyze_builtin_queries_python_duration_ms: params["analyze_builtin_queries_python_duration_ms"],
      analyze_custom_queries_cpp_duration_ms: params["analyze_custom_queries_cpp_duration_ms"],
      analyze_custom_queries_csharp_duration_ms: params["analyze_custom_queries_csharp_duration_ms"],
      analyze_custom_queries_go_duration_ms: params["analyze_custom_queries_go_duration_ms"],
      analyze_custom_queries_java_duration_ms: params["analyze_custom_queries_java_duration_ms"],
      analyze_custom_queries_javascript_duration_ms: params["analyze_custom_queries_javascript_duration_ms"],
      analyze_custom_queries_python_duration_ms: params["analyze_custom_queries_python_duration_ms"],
      analyze_failure_language: params["analyze_failure_language"],
      analysis_key: params["analysis_key"],
      analysis_name: params["workflow_name"], # Field had to be renamed in the protobuf schema
      cause: params["cause"],
      commit_oid: params["commit_oid"],
      completed_at: params["completed_at"],
      disable_default_queries: params["disable_default_queries"],
      exception: params["exception"],
      job_name: params["job_name"],
      languages: params["languages"],
      matrix_vars: params["matrix_vars"],
      num_results_in_sarif: params["num_results_in_sarif"],
      paths: params["paths"],
      paths_ignore: params["paths_ignore"],
      queries: params["queries"],
      raw_upload_size_bytes: params["raw_upload_size_bytes"],
      ref: params["ref"],
      repository_id: params[:repository_id],
      started_at: params["started_at"],
      status: params["status"],
      workflow_languages: params["workflow_languages"],
      workflow_run_id: params["workflow_run_id"],
      zipped_upload_size_bytes: params["zipped_upload_size_bytes"],
    }
    repo_id = params[:repository_id]

    publish(message, schema: "turboscan.v0.StatusReport", partition_key: repo_id)
  end
end
