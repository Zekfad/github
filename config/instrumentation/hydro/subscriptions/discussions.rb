# frozen_string_literal: true

# These are Hydro event subscriptions related to Discussions.

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("repository.enable_discussions") do |payload|
    repo = payload[:repository]

    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
    }.freeze

    publish(message, schema: "github.discussions.v1.EnableDiscussions",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("repository.disable_discussions") do |payload|
    repo = payload[:repository]

    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
    }.freeze

    publish(message, schema: "github.discussions.v1.DisableDiscussions",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion.create") do |payload|
    discussion = payload[:discussion]
    repo = discussion.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      discussion: serializer.discussion(discussion),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_body: discussion.public? ? serializer.specimen_data(discussion.body) : nil,
      specimen_title: discussion.public? ? serializer.specimen_data(discussion.title) : nil,
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      author: serializer.user(discussion.user),
    }.freeze

    publish(message, schema: "github.discussions.v1.DiscussionCreate",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion.update") do |payload|
    discussion = payload[:discussion]
    repo = discussion.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      discussion: serializer.discussion(discussion),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_body: discussion.public? ? serializer.specimen_data(discussion.body) : nil,
      specimen_title: discussion.public? ? serializer.specimen_data(discussion.title) : nil,
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      author: serializer.user(discussion.user),
    }.freeze

    publish(message, schema: "github.discussions.v1.DiscussionUpdate",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion.delete") do |payload|
    discussion = payload[:discussion]
    repo = discussion.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      discussion: serializer.discussion(discussion),
      actor: serializer.user(payload[:actor]),
      author: serializer.user(discussion.user),
    }

    if repo
      message[:repository] = serializer.repository(repo)
      message[:repository_owner] = serializer.repository_owner(repo.owner)
    end

    message = message.freeze

    publish(message, schema: "github.discussions.v1.DiscussionDelete",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion_comment.create") do |payload|
    comment = payload[:discussion_comment]
    discussion = comment.discussion
    repo = comment.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      discussion_comment: serializer.discussion_comment(comment),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_body: discussion.public? ? serializer.specimen_data(comment.body) : nil,
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      author: serializer.user(comment.user),
      discussion: serializer.discussion(discussion),
      discussion_author: serializer.user(discussion.user),
    }.freeze

    publish(message, schema: "github.discussions.v1.DiscussionCommentCreate",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion_comment.update") do |payload|
    comment = payload[:discussion_comment]
    discussion = comment.discussion
    repo = comment.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      discussion_comment: serializer.discussion_comment(comment),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      specimen_body: comment.public? ? serializer.specimen_data(comment.body) : nil,
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      author: serializer.user(comment.user),
      discussion: serializer.discussion(discussion),
      discussion_author: serializer.user(discussion.user),
    }.freeze

    publish(message, schema: "github.discussions.v1.DiscussionCommentUpdate",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion_comment.delete") do |payload|
    comment = payload[:discussion_comment]
    discussion = comment.discussion
    repo = comment.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      discussion_comment: serializer.discussion_comment(comment),
      actor: serializer.user(payload[:actor]),
      author: serializer.user(comment.user),
    }

    if repo
      message[:repository] = serializer.repository(repo)
      message[:repository_owner] = serializer.repository_owner(repo.owner)
    end

    if discussion
      message[:discussion] = serializer.discussion(discussion)
      message[:discussion_author] = serializer.user(discussion.user)
    end

    message = message.freeze

    publish(message, schema: "github.discussions.v1.DiscussionCommentDelete",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion_comment.mark_as_answer") do |payload|
    comment = payload[:discussion_comment]
    discussion = comment.discussion
    repo = comment.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      discussion_comment: serializer.discussion_comment(comment),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      author: serializer.user(comment.user),
      discussion: serializer.discussion(discussion),
      discussion_author: serializer.user(discussion.user),
    }.freeze

    publish(message, schema: "github.discussions.v1.DiscussionCommentMarkAsAnswer",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion_comment.unmark_as_answer") do |payload|
    comment = payload[:discussion_comment]
    discussion = comment.discussion
    repo = comment.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      discussion_comment: serializer.discussion_comment(comment),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      author: serializer.user(comment.user),
      discussion: serializer.discussion(discussion),
      discussion_author: serializer.user(discussion.user),
    }.freeze

    publish(message, schema: "github.discussions.v1.DiscussionCommentUnmarkAsAnswer",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion_comment.suggest_as_answer") do |payload|
    comment = payload[:discussion_comment]
    discussion = payload[:discussion]
    repo = comment.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      discussion: serializer.discussion(discussion),
      discussion_comment: serializer.discussion_comment(comment),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      author: serializer.user(comment.user),
      discussion_author: serializer.user(discussion.user),
    }.freeze

    publish(message, schema: "github.discussions.v1.DiscussionCommentSuggestAsAnswer",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("discussion_comment.unsuggest_as_answer") do |payload|
    comment = payload[:discussion_comment]
    discussion = payload[:discussion]
    repo = comment.repository

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      discussion: serializer.discussion(discussion),
      discussion_comment: serializer.discussion_comment(comment),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      repository: serializer.repository(repo),
      repository_owner: serializer.repository_owner(repo.owner),
      author: serializer.user(comment.user),
      discussion_author: serializer.user(discussion.user),
    }.freeze

    publish(message, schema: "github.discussions.v1.DiscussionCommentUnsuggestAsAnswer",
      publisher: GitHub.low_latency_hydro_publisher)
  end

  subscribe("browser.discussions.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      event_context: payload[:event_context],
      target: payload[:target],
      discussion_id: payload[:discussion_id],
      discussion_comment_id: payload[:discussion_comment_id],
      discussion_repository_id: payload[:discussion_repository_id],
      current_repository_id: payload[:current_repository_id],
    }

    publish(message, schema: "github.v1.DiscussionClick")
  end

  subscribe("browser.discussions.click_profile") do |payload|
    if payload[:is_discussion_comment]
      comment = DiscussionComment.find(payload[:id].to_i)
      discussion = comment.discussion
    else
      comment = nil
      discussion = Discussion.find(payload[:id].to_i)
    end

    message = {
      actor: serializer.user(payload[:client][:user]),
      discussion: serializer.discussion(discussion),
      discussion_comment: serializer.discussion_comment(comment),
    }

    publish(message, schema: "github.v1.DiscussionClickProfile")
  end

  subscribe("browser.discussions.apply_filter") do |payload|
    repository = Repository.find(payload[:repository_id])

    message = {
      actor: serializer.user(payload[:client][:user]),
      repository: serializer.repository(repository),
      filter: payload[:filter]&.upcase&.to_sym,
      sort: payload[:sort]&.upcase&.to_sym,
    }

    publish(message, schema: "github.v1.DiscussionApplyFilter")
  end
end
