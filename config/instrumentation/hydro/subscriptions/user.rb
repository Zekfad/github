# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("user.change_new_repository_default_branch_setting") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      old_default_branch: payload[:old_default_branch],
      new_default_branch: payload[:new_default_branch],
    }

    business = payload[:business]
    if business
      message[:business] = serializer.business(business)
    else
      message[:repository_owner] = serializer.repository_owner(payload[:repository_owner])
    end

    publish(message.freeze, schema: "github.users.v1.ChangeNewRepositoryDefaultBranchSetting")
  end

  subscribe("user.change_status") do |payload|
    message = {
      user: serializer.user(payload[:user]),
      emoji: payload[:emoji],
      message: payload[:message],
      new_status: serializer.user_status(payload[:user_status]),
    }

    publish(message, schema: "github.v1.UserStatusChange")
  end

  subscribe("user.signup") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      email: serializer.user_email(payload[:signup_email]),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      funcaptcha_session_id: GitHub.context[:funcaptcha_session_id],
      funcaptcha_solved: GitHub.context[:funcaptcha_solved],
      funcaptcha_response: serializer.funcaptcha_response(GitHub.context[:funcaptcha_response]),
      elected_to_receive_marketing_email: payload[:elected_to_receive_marketing_email],
      visitor_id: GitHub.context[:visitor_id].to_s,
      ga_id: GitHub.context[:ga_id].to_s,
    }

    publish(message, schema: "github.v1.UserSignup")
  end

  subscribe("user.successful_login") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      return_to: payload[:return_to],
      known_device: payload[:authentication_record]&.known_device?,
      known_location: payload[:authentication_record]&.known_location?,
      email: serializer.user_email(payload[:primary_email]),
      elected_to_receive_marketing_email: payload[:elected_to_receive_marketing_email],
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
      visitor_id: GitHub.context[:visitor_id].to_s,
      ga_id: GitHub.context[:ga_id].to_s,
    }

    publish(message, schema: "github.v1.SuccessfulLogin")
  end

  subscribe("user.failed_login") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
    }

    publish(message, schema: "github.v1.FailedLogin")
  end

  subscribe("user.logout") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      known_device: payload[:known_device],
      known_location: payload[:known_location],
      email: serializer.user_email(payload[:actor].primary_user_email),
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
    }

    publish(message, schema: "github.v1.UserLogout")
  end

  subscribe("user.user_country_change") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      previous_country_code: payload[:previous_location][:country_code],
      previous_country_name: payload[:previous_location][:country_name],
      previous_region: payload[:previous_location][:region],
      previous_region_name: payload[:previous_location][:region_name],
      previous_city: payload[:previous_location][:city],
      anomalous_session: payload[:anomalous_session],
    }

    publish(message, schema: "github.v1.UserCountryChange")
  end

  subscribe("user.destroy") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      actor: serializer.user(payload[:actor]),
      delete_type: payload[:delete_type] || :UNKNOWN,
      actor_was_staff: payload[:actor_was_staff],
      email: serializer.user_email(payload[:email]),
    }

    publish(message, schema: "github.v1.UserDestroy")
  end

  subscribe("user.add_email") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
      actor: serializer.user(payload[:actor]),
      primary_email: serializer.user_email(payload[:primary_email]),
      added_email: serializer.user_email(payload[:added_email]),
    }

    publish(message, schema: "github.v1.UserAddEmail")
  end

  subscribe("user.follow") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      followee: serializer.user(payload[:followee]),
      actor: serializer.user(payload[:actor]),
    }

    publish(message, schema: "github.v1.UserFollow")
  end

  subscribe("user.unfollow") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      followee: serializer.user(payload[:followee]),
      actor: serializer.user(payload[:actor]),
    }

    publish(message, schema: "github.v1.UserUnfollow")
  end

  subscribe("user.last_ip_update") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      previous_ip: serializer.ip_address(payload[:previous_last_ip]),
      current_ip: serializer.ip_address(payload[:current_last_ip]),
    }

    publish(message, schema: "github.v1.UserIPAddressUpdate")
  end

  subscribe("user.profile_readme_action") do |payload|
    change_type = case payload[:change_type]
    when Push::ChangedFile::ADDITION
      "CREATE"
    when Push::ChangedFile::DELETION
      "DELETE"
    when Push::ChangedFile::MODIFYING, Push::ChangedFile::RENAMING
      "UPDATE"
    else
      payload[:change_type]
    end

    readme_body = if payload[:repository]&.public?
      serializer.specimen_data(payload[:readme_body])
    end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      repository: serializer.repository(payload[:repository]),
      owner: serializer.user(payload[:owner]),
      actor: serializer.user(payload[:actor]),
      change_type: change_type,
      readme_body: readme_body,
      occurred_at: Time.zone.now,
    }

    publish(message, schema: "github.v1.ProfileReadmeAction")
  end

  subscribe("user.email_link_click") do |payload|
    message = {
      email_source: payload[:email_source],
      user: serializer.user(payload[:user]),
      auto_subscribed: payload[:auto_subscribed],
      request_context: serializer.request_context(GitHub.context.to_hash),
    }

    publish(message, schema: "github.v1.EmailLinkClick")
  end

  subscribe("user.password_update") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      account: serializer.user(payload[:account]),
      update_type: payload[:update_type],
      spamurai_form_signals: serializer.spamurai_form_signals(GitHub.context[:spamurai_form_signals]),
    }

    publish(message, schema: "github.v1.PasswordUpdate")
  end

  subscribe("user.reminders.integration_error") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      error: payload[:error],
      organization: serializer.organization(payload[:organization]),
    }

    publish(message, schema: "github.v1.ReminderIntegrationError")
  end
end
