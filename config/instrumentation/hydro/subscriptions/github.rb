# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GitHub) do
  subscribe("security_advisory.publish") do |payload|
    security_advisory = SecurityAdvisory.find(payload.fetch(:security_advisory_id))
    return unless security_advisory.disclosed?

    message = { security_advisory: security_advisory.hydro_entity_payload }
    publish(message, schema: "github.v1.SecurityAdvisoryPublish")
  end

  subscribe("security_advisory.update") do |payload|
    security_advisory = SecurityAdvisory.find(payload.fetch(:security_advisory_id))
    return unless security_advisory.disclosed?

    message = { security_advisory: security_advisory.hydro_entity_payload }
    publish(message, schema: "github.v1.SecurityAdvisoryUpdate")
  end

  subscribe("security_advisory.withdraw") do |payload|
    security_advisory = SecurityAdvisory.find(payload.fetch(:security_advisory_id))
    return unless security_advisory.disclosed?

    message = { security_advisory: security_advisory.hydro_entity_payload }
    publish(message, schema: "github.v1.SecurityAdvisoryWithdraw")
  end

  subscribe("integration_installation.create") do |payload|
    installer = User.find_by(id: payload[:installer_id])
    integration = Integration.find_by(id: payload[:integration_id])
    installation = IntegrationInstallation.find_by(id: payload[:installation_id])

    serialized_repositories = if installation&.installed_on_all_repositories?
      []
    else
      serialized_repositories = installation.repositories.map do |repo|
        serializer.repository(repo)
      end
    end

    message = {
      installation: serializer.integration_installation(payload),
      integration: serializer.integration(integration),
      repositories: serialized_repositories,
      sender: serializer.user(installer),
    }

   publish(message, schema: "github.v1.IntegrationInstallationCreate")
  end

  subscribe("integration_installation.destroy") do |payload|
    integration = Integration.find_by(id: payload[:integration_id])
    uninstaller = User.find_by(id: payload[:actor_id])

    message = {
      installation: serializer.integration_installation(payload),
      integration: serializer.integration(integration),
      sender: serializer.user(uninstaller),
    }

    publish(message, schema: "github.v1.IntegrationInstallationDelete")
  end

  subscribe("integration_installation.repositories_added") do |payload|
    actor = User.find_by(id: payload[:actor_id])

    repositories = Repository.where(id: payload[:repositories_added])
    serialized_repositories = repositories.map do |repo|
      serializer.repository(repo)
    end

    message = {
      installation: serializer.integration_installation(payload),
      repositories_added: serialized_repositories,
      sender: serializer.user(actor),
    }

    publish(message, schema: "github.v1.IntegrationInstallationRepositoriesAdded")
  end

  subscribe("integration_installation.repositories_removed") do |payload|
    actor = User.find_by(id: payload[:actor_id])

    repositories = Repository.where(id: payload[:repositories_removed])
    serialized_repositories = repositories.map do |repo|
      serializer.repository(repo)
    end

    message = {
      installation: serializer.integration_installation(payload),
      repositories_removed: serialized_repositories,
      sender: serializer.user(actor),
    }

    publish(message, schema: "github.v1.IntegrationInstallationRepositoriesRemoved")
  end

  subscribe(/feature_enrollment\.(enrolled|unenrolled)/) do |payload|
    actor = User.find_by(id: payload[:actor_id])

    message = {
      toggleable_feature: payload[:toggleable_feature],
      actor: serializer.user(actor),
      enrollee_id: payload[:enrollee_id],
      enrollee_type: serializer.enum_from_string(payload[:enrollee_type]),
      action: payload[:enrolled] ? FeatureEnrollment::ENROLLED_ACTION : FeatureEnrollment::UNENROLLED_ACTION,
    }

    publish(message, schema: "github.v1.FeatureEnrollmentEvent")
  end
end
