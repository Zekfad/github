# frozen_string_literal: true

# These are Hydro event subscriptions related to GitHub Codespaces.

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("browser.codespace_create.click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
                                                  overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      repository_id: payload[:repository_id],
      target: payload[:target],
      originating_request_id: payload[:originating_request_id],
      server_timestamp: Time.zone.now,
      client_timestamp: payload[:client][:timestamp].try(:to_i),
    }

    publish(message, schema: "github.codespaces.v0.CodespaceCreateClick")
  end

  subscribe("browser.codespace_open.click") do |payload|
    user = payload.dig(:client, :user)
    codespace = user.codespaces.find_by(id: payload[:codespace_id]) if user

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
                                                  overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(user),
      codespace: serializer.codespace(codespace),
      target: payload[:target],
      originating_request_id: payload[:originating_request_id],
      server_timestamp: Time.zone.now,
      client_timestamp: payload[:client][:timestamp].try(:to_i),
    }

    publish(message, schema: "github.codespaces.v0.CodespaceOpenClick")
  end

  subscribe("browser.codespace_destroy.click") do |payload|
    user = payload.dig(:client, :user)
    codespace = user.codespaces.find_by(id: payload[:codespace_id]) if user

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
                                                  overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(user),
      codespace: serializer.codespace(codespace),
      target: payload[:target],
      originating_request_id: payload[:originating_request_id],
      server_timestamp: Time.zone.now,
      client_timestamp: payload[:client][:timestamp].try(:to_i),
    }

    publish(message, schema: "github.codespaces.v0.CodespaceDestroyClick")
  end

  subscribe("codespaces.created") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      codespace: serializer.codespace(payload[:codespace]),
    }

    publish(message, schema: "github.codespaces.v0.CodespaceCreated")
  end

  subscribe("codespaces.destroyed") do |payload|
    message = {
      codespace: serializer.codespace(payload[:codespace]),
    }

    publish(message, schema: "github.codespaces.v0.CodespaceDestroyed")
  end

  subscribe("codespaces.provisioned") do |payload|
    message = {
      codespace: serializer.codespace(payload[:codespace]),
    }

    publish(message, schema: "github.codespaces.v0.CodespaceProvisioned")
  end

  subscribe("codespaces.compute_usage") do |payload|
    message = serializer.codespace_compute_usage(payload[:usage])
    publish(message, schema: "github.codespaces.v0.ComputeUsage")
  end

  subscribe("codespaces.storage_usage") do |payload|
    message = serializer.codespace_storage_usage(payload[:usage])
    publish(message, schema: "github.codespaces.v0.StorageUsage")
  end
end
