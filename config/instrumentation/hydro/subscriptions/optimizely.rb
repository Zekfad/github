# frozen_string_literal: true

Hydro::EventForwarder.configure(source: GitHub) do
  subscribe("optimizely.decision") do |payload|
    next unless GitHub.flipper[:optimizely_instrumentation].enabled?

    message ={
      request_context: serializer.request_context(GitHub.context.to_hash),
      user_analytics_id: payload[1],
      experiment: payload[3][:experiment_key],
      variant: payload[3][:variation_key],
      attributes: JSON.dump(payload[2] || {}),
    }

    publish(message, schema: "github.v1.OptimizelyDecision")
  end

  subscribe("optimizely.track") do |payload|
    next unless GitHub.flipper[:optimizely_instrumentation].enabled?

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      event: payload[0],
      user_analytics_id: payload[1],
      attributes: JSON.dump(payload[2] || {}),
      event_tags: JSON.dump(payload[3] || {}),
    }

    publish(message, schema: "github.v1.OptimizelyTrack")
  end
end
