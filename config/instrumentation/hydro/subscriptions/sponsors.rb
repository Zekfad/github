# frozen_string_literal: true

# These are Hydro event subscriptions related to the GitHub Sponsors program.

Hydro::EventForwarder.configure(source: GlobalInstrumenter) do
  subscribe("sponsors.repo_funding_links_button_toggle") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      owner: serializer.user(payload[:owner]),
      toggle_state: payload[:toggle_state],
      toggled_at: Time.zone.now,
    }
    publish(message, schema: "github.v1.FundingToggle")
  end

  subscribe("sponsors.repo_funding_links_file_action") do |payload|
    action =
      case payload[:change_type]
      when Push::ChangedFile::ADDITION
        "CREATE"
      when Push::ChangedFile::DELETION
        "DELETE"
      when Push::ChangedFile::MODIFYING, Push::ChangedFile::RENAMING
        "UPDATE"
      end

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      repository: serializer.repository(payload[:repository]),
      owner: serializer.user(payload[:owner]),
      repo_platforms: payload[:platforms],
      action: action,
      occurred_at: Time.zone.now,
    }
    publish(message, schema: "github.v1.FundingFileAction")
  end

  subscribe(/sponsors\.sponsor_sponsorship_(cancel|create)/) do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      sponsorship: serializer.sponsorship(payload[:sponsorship]),
      listing: serializer.sponsors_listing(payload[:listing]),
      tier: serializer.sponsors_tier(payload[:tier]),
      matchable: payload[:matchable],
      action: payload[:action],
      goal: serializer.sponsors_goal(payload[:goal]),
    }

    publish(message, schema: "github.sponsors.v1.SponsorshipCreateCancel")
  end

  subscribe("sponsors.sponsor_sponsorship_preference_change") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      previous_sponsorship: serializer.sponsorship(payload[:previous_sponsorship]),
      current_sponsorship: serializer.sponsorship(payload[:current_sponsorship]),
      listing: serializer.sponsors_listing(payload[:listing]),
      tier: serializer.sponsors_tier(payload[:tier]),
    }

    publish(message, schema: "github.sponsors.v1.SponsorshipPreferenceChange")
  end

  subscribe("sponsors.sponsor_sponsorship_tier_change") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      sponsorship: serializer.sponsorship(payload[:sponsorship]),
      listing: serializer.sponsors_listing(payload[:listing]),
      previous_tier: serializer.sponsors_tier(payload[:previous_tier]),
      current_tier: serializer.sponsors_tier(payload[:current_tier]),
      goal: serializer.sponsors_goal(payload[:goal]),
    }

    publish(message, schema: "github.sponsors.v1.SponsorshipTierChange")
  end

  subscribe("sponsors.sponsor_sponsorship_pending_change") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      sponsorship: serializer.sponsorship(payload[:sponsorship]),
      listing: serializer.sponsors_listing(payload[:listing]),
      old_tier: serializer.sponsors_tier(payload[:old_tier]),
      action: payload[:action].to_s,
      goal: serializer.sponsors_goal(payload[:goal]),
    }
    message[:new_tier] = serializer.sponsors_tier(payload[:new_tier]) if payload[:new_tier].present?

    publish(message, schema: "github.sponsors.v1.SponsorshipPendingChange")
  end

  subscribe("sponsors.sponsor_transfer_reversal") do |payload|
    message = {
      sponsorship: serializer.sponsorship(payload[:sponsorship]),
      listing: serializer.sponsors_listing(payload[:listing]),
      tier: serializer.sponsors_tier(payload[:tier]),
      currency_code: payload[:currency_code],
      total_amount_in_subunits: payload[:total_amount_in_subunits],
      payment_amount_in_subunits: payload[:payment_amount_in_subunits],
      match_amount_in_subunits: payload[:match_amount_in_subunits],
    }

    publish(message, schema: "github.sponsors.v1.SponsorshipTransferReversal")
  end

  subscribe(/sponsors\.sponsored_developer_(approve|create|request_approval|redraft)/) do |payload|
    message = {
      user: serializer.user(payload[:user]),
      action: payload[:action].to_s,
      request_context: serializer.request_context(GitHub.context.to_hash),
    }

    publish(message, schema: "github.sponsors.v0.AccountStatusChange")
  end

  subscribe("sponsors.sponsored_developer_profile_update") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      user: serializer.user(payload[:user]),
      bio: payload[:bio],
      request_context: serializer.request_context(GitHub.context.to_hash),
    }

    publish(message, schema: "github.sponsors.v0.ProfileUpdate")
  end

  subscribe("sponsors.sponsored_developer_tier_description_update") do |payload|
    message = {
      actor: serializer.user(payload[:actor]),
      request_context: serializer.request_context(GitHub.context.to_hash),
      listing: serializer.sponsors_listing(payload[:listing]),
      tier: serializer.sponsors_tier(payload[:tier]),
      current_tier_description: payload[:current_tier_description],
      previous_tier_description: payload[:previous_tier_description],
      sponsors_count_on_tier: payload[:sponsors_count_on_tier],
    }

    publish(message, schema: "github.sponsors.v1.TierDescriptionUpdate")
  end

  subscribe("sponsors.sponsored_developer_update_newsletter_send") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      sponsored_developer: serializer.user(payload[:sponsored_developer]),
      subject: payload[:subject],
      body: payload[:body],
    }

    publish(message, schema: "github.sponsors.v0.NewsletterSent")
  end

  # TODO: use in GitHub Sponsors stafftools
  subscribe("sponsors.waitlist_invite_sponsored_developer") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
    }

    publish(message, schema: "github.sponsors.v0.InvitedFromWaitlist")
  end

  subscribe("sponsors.waitlist_join") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      user: serializer.user(payload[:user]),
    }

    publish(message, schema: "github.sponsors.v0.JoinedWaitlist")
  end

  subscribe("browser.sponsors.button_click") do |payload|
    sponsored_developer = User.find_by_login(payload[:sponsored_developer_login])

    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      button: serializer.enum_from_string(payload[:button]),
      actor: serializer.user(payload[:client][:user]),
      user: serializer.user(sponsored_developer),
    }
    publish(message, schema: "github.sponsors.v0.ButtonClick")
  end

  subscribe("browser.sponsors.repo_funding_links_link_click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      clicked_at: payload[:client][:timestamp].try(:to_i),
      selected_platform: payload[:platform],
      repo_platforms: payload[:platforms],
      repo_id: payload["repo_id"]&.to_i,
      owner_id: payload["owner_id"]&.to_i,
      is_mobile: payload["is_mobile"] == "true",
    }
    publish(message, schema: "github.v1.FundingLinkClick")
  end

  subscribe("browser.sponsors.repo_funding_links_button_click") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash,
        overrides: browser_request_context_overrides(payload)),
      actor: serializer.user(payload[:client][:user]),
      clicked_at: payload[:client][:timestamp].try(:to_i),
      repo_platforms: payload[:platforms],
      repo_id: payload["repo_id"]&.to_i,
      owner_id: payload["owner_id"]&.to_i,
      is_mobile: payload["is_mobile"] == "true",
    }
    publish(message, schema: "github.v1.FundingButtonClick")
  end

  subscribe("sponsors.element_displayed") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      element: payload[:element],
      actor: serializer.user(payload[:actor]),
      sponsor: serializer.user(payload[:sponsor]),
      sponsorable: serializer.user(payload[:sponsorable]),
    }

    publish(message, schema: "github.sponsors.v0.ElementDisplayed")
  end

  subscribe("sponsors.post_sponsorship_survey_submitted") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      survey_id: payload[:survey_id],
      survey_group_id: payload[:survey_group_id],
      total_accounts_sponsoring: payload[:total_accounts_sponsoring],
      tier: serializer.sponsors_tier(payload[:tier]),
    }

    message[:sponsorable] = if payload[:sponsorable].user?
      serializer.user(payload[:sponsorable])
    else
      serializer.organization(payload[:sponsorable])
    end

    publish(message, schema: "github.sponsors.v1.PostSponsorshipSurveySubmitted")
  end

  subscribe("sponsors.sponsorship_canceled_survey_submitted") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      survey_id: payload[:survey_id],
      survey_group_id: payload[:survey_group_id],
      total_accounts_sponsoring: payload[:total_accounts_sponsoring],
    }

    message[:sponsorable] = if payload[:sponsorable].user?
      serializer.user(payload[:sponsorable])
    else
      serializer.organization(payload[:sponsorable])
    end

    publish(message, schema: "github.sponsors.v1.SponsorshipCanceledSurveySubmitted")
  end

  subscribe("sponsors.profile_viewed") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      referring_account: serializer.user(payload[:referring_account]),
      origin: payload[:origin],
    }

    [:sponsorable, :sponsor].each do |attribute|
      next unless payload[attribute]

      message[attribute] = if payload[attribute].user?
        serializer.user(payload[attribute])
      else
        serializer.organization(payload[attribute])
      end
    end

    message[:source] = if payload[:source].present?
      payload[:source]
    else
      begin
        case URI.parse(GitHub.context[:referrer]).host
        when /twitter\.com/ then Sponsors::TrackingParameters::TWITTER_SOURCE
        when /facebook\.com/ then Sponsors::TrackingParameters::FACEBOOK_SOURCE
        end
      rescue URI::InvalidURIError
        nil
      end
    end

    publish(message, schema: "github.sponsors.v0.ProfileViewed")
  end

  subscribe("sponsors.sponsors_fraud_review_create") do |payload|
    message = {
      fraud_review: serializer.sponsors_fraud_review(payload[:fraud_review]),
      listing: serializer.sponsors_listing(payload[:listing]),
      sponsorable: serializer.user(payload[:sponsorable]),
    }

    publish(message, schema: "github.sponsors.v1.SponsorsFraudReviewCreate")
  end

  subscribe("sponsors.sponsors_fraud_review_state_change") do |payload|
    message = {
      fraud_review: serializer.sponsors_fraud_review(payload[:fraud_review]),
      previous_state: payload[:previous_state].to_s,
      reviewer: serializer.user(payload[:reviewer]),
      sponsorable: serializer.user(payload[:sponsorable]),
      reviewed_at: payload[:reviewed_at],
    }

    publish(message, schema: "github.sponsors.v1.SponsorsFraudReviewStateChange")
  end

  subscribe("sponsors.goal_event") do |payload|
    message = {
      request_context: serializer.request_context(GitHub.context.to_hash),
      actor: serializer.user(payload[:actor]),
      listing: serializer.sponsors_listing(payload[:listing]),
      goal: serializer.sponsors_goal(payload[:goal]),
      action: payload[:action],
    }

    publish(message, schema: "github.sponsors.v1.GoalEvent")
  end

  subscribe("sponsors.sponsors_listing_match_limit_reached") do |payload|
    message = {
      listing: serializer.sponsors_listing(payload[:listing]),
    }

    publish(message, schema: "github.sponsors.v0.SponsorsListingMatchLimitReached")
  end

  subscribe("sponsors.sponsor_fraud_flagged") do |payload|
    message = {
      fraud_review: serializer.sponsors_fraud_review(payload[:fraud_review]),
      listing: serializer.sponsors_listing(payload[:listing]),
      sponsorable: serializer.user(payload[:sponsorable]),
      sponsor: serializer.user(payload[:sponsor]),
      matched_current_client_id: payload[:matched_current_client_id],
      matched_current_ip: payload[:matched_current_ip],
      matched_historical_ip: payload[:matched_historical_ip],
      matched_historical_client_id: payload[:matched_historical_client_id],
      matched_current_ip_region_and_user_agent: payload[:matched_current_ip_region_and_user_agent],
    }

    publish(message, schema: "github.sponsors.v0.SponsorFraudFlagged")
  end
end
