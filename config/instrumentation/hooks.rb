# frozen_string_literal: true
# Hook related stats
GitHub.subscribe "hook.create" do |name, start, ending, transaction_id, payload|
  payload[:events].each do |event_type|
    tags = GitHub::TaggingHelper.create_hook_event_tags(event_type)
    GitHub.dogstats.increment("hooks.create", tags: tags + ["hook_type:#{payload[:hook_type]}_hook"])
  end
end

class HookEventSubscriber
  def self.subscribe(*args, &original_block)

    with_stats = proc do |name, start, ending, transaction_id, payload|
      GitHub.dogstats.increment("hooks.triggered.count", tags: ["event_type:#{name}"])
      original_block.call(name, start, ending, transaction_id, payload)
    end

    GitHub.subscribe(*args, &with_stats)
  end
end

# Integration related events
HookEventSubscriber.subscribe "integration_installation.create" do |name, start, ending, transaction_id, payload|
  # Do not enqueue any delivery jobs for Dependabot installs
  next if payload[:integration_id] == GitHub.dependabot_github_app&.id

  integration = Integration.new(id: payload[:integration_id])
  if integration.send_deprecated_event?
    Hook::Event::IntegrationInstallationEvent.queue(
      action: :created,
      installation_id: payload[:installation_id],
      integration_id: payload[:integration_id],
      actor_id: payload[:installer_id],
      triggered_at: start,
    )
  end

  Hook::Event::InstallationEvent.queue(
    action: :created,
    installation_id: payload[:installation_id],
    integration_id: payload[:integration_id],
    actor_id: payload[:installer_id],
    requester_id: payload[:requester_id],
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "integration_installation.repositories_added" do |name, start, ending, transaction_id, payload|
  # Do not enqueue any delivery jobs for Dependabot installs
  next if payload[:integration_id].to_i == GitHub.dependabot_github_app&.id

  integration = Integration.new(id: payload[:integration_id])
  if integration.send_deprecated_event?
    Hook::Event::IntegrationInstallationRepositoriesEvent.queue(
      action: :added,
      installation_id: payload[:installation_id],
      actor_id: payload[:actor_id],
      repositories_added: payload[:repositories_added],
      repository_selection: payload[:repository_selection],
      triggered_at: start,
    )
  end

  Hook::Event::InstallationRepositoriesEvent.queue(
    action: :added,
    installation_id: payload[:installation_id],
    actor_id: payload[:actor_id],
    repositories_added: payload[:repositories_added],
    repository_selection: payload[:repository_selection],
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "integration_installation.repositories_removed" do |name, start, ending, transaction_id, payload|
  integration = Integration.new(id: payload[:integration_id])
  if integration.send_deprecated_event?
    legacy_event = Hook::Event::IntegrationInstallationRepositoriesEvent.new(
      action: :removed,
      installation_id: payload[:installation_id],
      actor_id: payload[:actor_id],
      repositories_removed: payload[:repositories_removed],
      repository_selection: payload[:repository_selection],
      triggered_at: start,
    )
    delivery_system = Hook::DeliverySystem.new(legacy_event)
    delivery_system.generate_hookshot_payloads
    delivery_system.deliver_later
  end

  event = Hook::Event::InstallationRepositoriesEvent.new(
    action: :removed,
    installation_id: payload[:installation_id],
    actor_id: payload[:actor_id],
    repositories_removed: payload[:repositories_removed],
    repository_selection: payload[:repository_selection],
    triggered_at: start,
  )
  delivery_system = Hook::DeliverySystem.new(event)
  delivery_system.generate_hookshot_payloads
  delivery_system.deliver_later
end

HookEventSubscriber.subscribe "integration_installation.contact_email_changed" do |name, start, ending, transaction_id, payload|
  Hook::Event::InstallationEvent.queue(
    action: :updated,
    installation_id: payload[:installation_id],
    integration_id: payload[:integration_id],
    actor_id: payload[:actor_id],
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "integration_installation.version_updated" do |name, start, ending, transaction_id, payload|
  Hook::Event::InstallationEvent.queue(
    action: :new_permissions_accepted,
    installation_id: payload[:installation_id],
    integration_id: payload[:integration_id],
    actor_id: payload[:actor_id],
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "integration_installation.suspend" do |name, start, ending, transaction_id, payload|
  Hook::Event::InstallationEvent.queue(
    action: :suspend,
    installation_id: payload[:installation_id],
    integration_id: payload[:integration_id],
    actor_id: payload[:actor_id],
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "integration_installation.unsuspend" do |name, start, ending, transaction_id, payload|
  Hook::Event::InstallationEvent.queue(
    action: :unsuspend,
    installation_id: payload[:installation_id],
    integration_id: payload[:integration_id],
    actor_id: payload[:actor_id],
    triggered_at: start,
  )
end

# Hook Delivery Listeners
HookEventSubscriber.subscribe /\Ahook.(create|ping)\Z/ do |name, start, ending, transaction_id, payload|
  Hook::Event::PingEvent.queue(hook_id: payload[:hook_id], triggered_at: start) if payload[:webhook]
end

HookEventSubscriber.subscribe "stars.update_count" do |name, start, ending, transaction_id, payload|
  next if spammy_user_acting_outside_own_repos?(payload)
  attrs = payload.slice(:user_id, :starred_id, :starred_type)
  Hook::Event::WatchEvent.queue(attrs.merge(triggered_at: start))
end

HookEventSubscriber.subscribe(/\Astar\.(create|destroy)\Z/) do |name, start, ending, transaction_id, payload|
  next unless payload[:starred_type] == "Repository"
  next if spammy_user_acting_outside_own_repos?(payload)

  attrs = payload.slice(:starred_id, :user_id, :star_id, :action)
  attrs[:triggered_at] = start
  Hook::Event::StarEvent.queue(attrs)
end

HookEventSubscriber.subscribe "deployment.create" do |name, start, ending, transaction_id, payload|
  Hook::Event::DeploymentEvent.queue(
    deployment_id: payload[:deployment_id],
    triggered_at: start,
    action: "created",
  )
end

HookEventSubscriber.subscribe "deployment_status.create" do |name, start, ending, transaction_id, payload|
  unless payload[:deployment_status_state] == DeploymentStatus::INACTIVE
    Hook::Event::DeploymentStatusEvent.queue(
      deployment_status_id: payload[:deployment_status_id],
      triggered_at: start,
      action: "created",
    )
  end
end

HookEventSubscriber.subscribe "pull_request_review.submit" do |name, start, ending, transaction_id, payload|
  review_id = payload.values_at(:id)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::PullRequestReviewEvent.queue(
    action: :submitted,
    pull_request_review_id: review_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "pull_request_review.update" do |name, start, ending, transaction_id, payload|
  review_id, changes = payload.values_at(:review_id, :changes)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::PullRequestReviewEvent.queue(
    action: :edited,
    pull_request_review_id: review_id,
    changes: changes,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "pull_request_review.dismiss" do |name, start, ending, transaction_id, payload|
  actor_id, review_id = payload.values_at(:actor_id, :review_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::PullRequestReviewEvent.queue(
    action: :dismissed,
    actor_id: actor_id,
    pull_request_review_id: review_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "pull_request_review_comment.submission" do |name, start, ending, transaction_id, payload|
  comment_id, submitted = payload.values_at(:comment_id, :submitted)

  if submitted && !spammy_user_acting_outside_own_repos?(payload)
    Hook::Event::PullRequestReviewCommentEvent.queue(
      action: :created,
      pull_request_review_comment_id: comment_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe "pull_request_review_comment.update" do |name, start, ending, transaction_id, payload|
  comment_id, changes, actor_id, submitted = payload.values_at(:comment_id, :changes, :actor_id, :submitted)
  if submitted && !spammy_user_acting_outside_own_repos?(payload)
    Hook::Event::PullRequestReviewCommentEvent.queue(
      action: :edited,
      pull_request_review_comment_id: comment_id,
      changes: changes,
      actor_id: actor_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe /\Aregistry_package.(create|update)\Z/ do |name, start, ending, transaction_id, payload|
  Hook::Event::RegistryPackageEvent.queue(
      registry_package_id: payload[:registry_package_id],
      package_version_id: payload[:package_version_id],
      action: payload[:action],
      actor_id: payload[:actor_id],
      triggered_at: start)
end

HookEventSubscriber.subscribe /\Apackage.(create|update)\Z/ do |name, start, ending, transaction_id, payload|
  Hook::Event::PackageEvent.queue(
      registry_package_id: payload[:registry_package_id],
      package_version_id: payload[:package_version_id],
      action: payload[:action],
      actor_id: payload[:actor_id],
      triggered_at: start)
end

HookEventSubscriber.subscribe /\Apackagev2.(create|update)\Z/ do |name, start, ending, transaction_id, payload|
  Hook::Event::PackageV2Event.queue(
      actor_id: payload[:actor_id],
      registry_package: payload[:package],
      version: payload[:version],
      action: payload[:action],
      triggered_at: Time.now)
end

HookEventSubscriber.subscribe /\Arepository_dispatch.create\Z/ do |name, start, ending, transaction_id, payload|
  Hook::Event::RepositoryDispatchEvent.queue(
      repository_id: payload[:repository_id],
      action: payload[:action],
      actor_id: payload[:actor_id],
      branch: payload[:branch],
      client_payload: payload[:client_payload],
      triggered_at: start)
end

HookEventSubscriber.subscribe /\Aworkflow_dispatch\Z/ do |name, start, ending, transaction_id, payload|
  Hook::Event::WorkflowDispatchEvent.queue(
      repository_id: payload[:repository_id],
      actor_id: payload[:actor_id],
      ref: payload[:ref],
      workflow: payload[:workflow],
      inputs: payload[:inputs],
      triggered_at: start)
end

HookEventSubscriber.subscribe /\Aworkflow_run.status_changed\Z/ do |name, start, ending, transaction_id, payload|
  Hook::Event::WorkflowRunEvent.queue(
    action: payload[:action],
    run_id: payload[:run_id],
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "release.create" do |name, start, ending, transaction_id, payload|
  next if payload[:spammy]
  Hook::Event::ReleaseEvent.queue(
    release_id: payload[:release_id],
    triggered_at: start,
    action: :created,
    actor_id: payload[:actor_id],
  )
end

HookEventSubscriber.subscribe "release.published" do |name, start, ending, transaction_id, payload|
  Hook::Event::ReleaseEvent.queue(
    release_id: payload[:release_id],
    triggered_at: start,
    action: :published,
    actor_id: payload[:actor_id],
  )
end

HookEventSubscriber.subscribe "release.unpublish" do |name, start, ending, transaction_id, payload|
  next if payload[:spammy]

  Hook::Event::ReleaseEvent.queue(
    release_id: payload[:release_id],
    triggered_at: start,
    action: :unpublished,
    actor_id: payload[:actor_id],
  )
end

HookEventSubscriber.subscribe "release.prerelease" do |name, start, ending, transaction_id, payload|
  next if payload[:spammy]

  Hook::Event::ReleaseEvent.queue(
    release_id: payload[:release_id],
    triggered_at: start,
    action: :prereleased,
    actor_id: payload[:actor_id],
  )
end

HookEventSubscriber.subscribe "release.release" do |name, start, ending, transaction_id, payload|
  next if payload[:spammy]

  Hook::Event::ReleaseEvent.queue(
    release_id: payload[:release_id],
    triggered_at: start,
    action: :released,
    actor_id: payload[:actor_id],
  )
end

HookEventSubscriber.subscribe "release.update" do |name, start, ending, transaction_id, payload|
  next if payload[:spammy]

  Hook::Event::ReleaseEvent.queue(
    release_id: payload[:release_id],
    triggered_at: start,
    action: :edited,
    actor_id: payload[:actor_id],
    changes: payload[:changes],
  )
end

HookEventSubscriber.subscribe /\Arepository_vulnerability_alert.(create|dismiss)\Z/ do |name, start, ending, transaction_id, payload|
  Hook::Event::RepositoryVulnerabilityAlertEvent.queue(
      action: payload[:action],
      repository_id: payload[:repo_id],
      alert_id: payload[:alert_id],
      triggered_at: start)
end

HookEventSubscriber.subscribe "page/build.finished" do |name, start, ending, transaction_id, payload|
  page_build_id, status, previous_status = payload.values_at(:page_build_id, :status, :previous_status)
  Hook::Event::PageBuildEvent.queue(page_build_id: page_build_id, triggered_at: start) if status != previous_status
end

HookEventSubscriber.subscribe "commit_comment.create" do |name, start, ending, transaction_id, payload|
  comment_id = payload.values_at(:commit_comment_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::CommitCommentEvent.queue(
    action: :created,
    commit_comment_id: comment_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "issue_comment.create" do |name, start, ending, transaction_id, payload|
  comment_id, actor_id = payload.values_at(:issue_comment_id, :actor_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::IssueCommentEvent.queue(
    action: :created,
    issue_comment_id: comment_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "issue_comment.update" do |name, start, ending, transaction_id, payload|
  actor_id, old_body, comment_id = payload.values_at(:actor_id, :old_body, :issue_comment_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::IssueCommentEvent.queue(
    action: :edited,
    issue_comment_id: comment_id,
    actor_id: actor_id,
    old_body: old_body,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "milestone.create" do |name, start, ending, transaction_id, payload|
  milestone_id, actor_id = payload.values_at(:milestone_id, :actor_id)

  Hook::Event::MilestoneEvent.queue(
    action: :created,
    milestone_id: milestone_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "milestone.close" do |name, start, ending, transaction_id, payload|
  milestone_id, actor_id = payload.values_at(:milestone_id, :actor_id)
  Hook::Event::MilestoneEvent.queue(
    action: :closed,
    milestone_id: milestone_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "milestone.open" do |name, start, ending, transaction_id, payload|
  milestone_id, actor_id = payload.values_at(:milestone_id, :actor_id)
  Hook::Event::MilestoneEvent.queue(
    action: :opened,
    milestone_id: milestone_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "milestone.update" do |name, start, ending, transaction_id, payload|
  milestone_id, changes, actor_id = payload.values_at(:milestone_id, :changes, :actor_id)
  Hook::Event::MilestoneEvent.queue(
    action: :edited,
    milestone_id: milestone_id,
    actor_id: actor_id,
    changes: changes,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "repo.access" do |name, start, ending, transaction_id, payload|
  repo_id, actor_id = payload.values_at(:repo_id, :actor_id)

  if payload[:access] == :public
    # TODO: The `PublicEvent` will be deprecated in the future. For now, trigger both events to avoid disrupting integrators.
    Hook::Event::PublicEvent.queue(
      repo_id: repo_id,
      actor_id: actor_id,
      triggered_at: start,
    )
    Hook::Event::RepositoryEvent.queue(
      action: :publicized,
      repository_id: repo_id,
      actor_id: actor_id,
      triggered_at: start,
    )
  else
    Hook::Event::RepositoryEvent.queue(
      action: :privatized,
      repository_id: repo_id,
      actor_id: actor_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe "repo.create" do |name, start, ending, transaction_id, payload|
  repo_id, actor_id, fork_parent = payload.values_at(:repo_id, :actor_id, :fork_parent)

  Hook::Event::ForkEvent.queue(fork_repository_id: repo_id, triggered_at: start) if fork_parent
  Hook::Event::RepositoryEvent.queue(
      action: :created,
      repository_id: repo_id,
      actor_id: actor_id,
      triggered_at: start,
  )
end

HookEventSubscriber.subscribe "repo.update" do |name, start, ending, transaction_id, payload|
  if !payload[:changes].empty?
    repo_id, actor_id = payload.values_at(:repo_id, :actor_id)
    Hook::Event::RepositoryEvent.queue(
        action: :edited,
        repository_id: repo_id,
        actor_id: actor_id,
        triggered_at: start,
        changes: payload[:changes],
    )
  end
end

HookEventSubscriber.subscribe "repo.archived" do |name, start, ending, transaction_id, payload|
  repo_id, actor_id = payload.values_at(:repo_id, :actor_id)

  Hook::Event::RepositoryEvent.queue(
      action: :archived,
      repository_id: repo_id,
      actor_id: actor_id,
      triggered_at: start,
  )
end

HookEventSubscriber.subscribe "repo.unarchived" do |name, start, ending, transaction_id, payload|
  repo_id, actor_id = payload.values_at(:repo_id, :actor_id)

  Hook::Event::RepositoryEvent.queue(
      action: :unarchived,
      repository_id: repo_id,
      actor_id: actor_id,
      triggered_at: start,
  )
end

HookEventSubscriber.subscribe "repo.transfer" do |name, start, ending, transaction_id, payload|
  repo_id, actor_id = payload.values_at(:repo_id, :actor_id)
  changes = payload.slice(:old_user_id, :owner_was_org)

  Hook::Event::RepositoryEvent.queue(
    action: :transferred,
    repository_id: repo_id,
    actor_id: actor_id,
    triggered_at: start,
    changes: changes,
  )
end

HookEventSubscriber.subscribe "repo.rename" do |name, start, ending, transaction_id, payload|
  repo_id, actor_id = payload.values_at(:repo_id, :actor_id)
  changes = payload.slice(:old_name)

  Hook::Event::RepositoryEvent.queue(
    action: :renamed,
    repository_id: repo_id,
    actor_id: actor_id,
    triggered_at: start,
    changes: changes,
  )
end

HookEventSubscriber.subscribe "ability.grant" do |name, start, ending, transaction_id, payload|
  priority                 = payload[:priority]
  actor_id, actor_type     = payload.values_at(:ability_actor_id, :ability_actor_type)
  subject_id, subject_type = payload.values_at(:ability_subject_id, :ability_subject_type)
  grantor_id               = payload[:grantor_id] || GitHub.context[:actor_id] || actor_id

  if (priority == :direct) && (actor_type == "Team") && (subject_type == "Repository")
    # TODO: The `TeamAddEvent` will be deprecated in the future. For now, trigger both events to avoid disrupting integrators.
    # Team granted access to a repo
    Hook::Event::TeamAddEvent.queue(
      team_id: actor_id,
      repository_id: subject_id,
      triggered_at: start,
    )
    Hook::Event::TeamEvent.queue(
      action: :added_to_repository,
      actor_id: GitHub.context[:actor_id] || actor_id,
      team_id: actor_id,
      repo_id: subject_id,
      triggered_at: start,
    )
  elsif (priority == :direct) && (actor_type == "User") && (subject_type == "Repository")
    # User granted access to a repo
    Hook::Event::MemberEvent.queue(
      action: :added,
      user_id: actor_id,
      repo_id: subject_id,
      actor_id: grantor_id,
      triggered_at: start,
      changes: {
        new_permission: payload[:action],
      },
    )
  elsif (priority == :direct) && (actor_type == "User") && (subject_type == "Team")
    # User added to a Team
    team = Team.find(subject_id)
    user = User.find(actor_id)
    Hook::Event::MembershipEvent.queue(
      action: :added,
      member_id: user.id,
      member_login: user.login,
      team_id: team.id,
      team_name: team.name,
      organization_id: team.organization_id,
      actor_id: grantor_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe "ability.revoke" do |name, start, ending, transaction_id, payload|
  priority                 = payload[:priority]
  actor_id, actor_type     = payload.values_at(:ability_actor_id, :ability_actor_type)
  subject_id, subject_type = payload.values_at(:ability_subject_id, :ability_subject_type)

  if (priority == :direct) && (actor_type == "User") && (subject_type == "Team")
    # User removed from a Team
    if team = Team.find_by_id(subject_id)
      user = User.find(actor_id)
      Hook::Event::MembershipEvent.queue(
        action: :removed,
        member_id: actor_id,
        member_login: user.login,
        team_id: team.id,
        team_name: team.name,
        organization_id: team.organization_id,
        actor_id: GitHub.context[:actor_id] || actor_id,
        triggered_at: start,
      )
    end
  elsif (priority == :direct) && (actor_type == "User") && (subject_type == "Repository")
    # User removed from a repo
    Hook::Event::MemberEvent.queue(
      action: :removed,
      user_id: actor_id,
      repo_id: subject_id,
      actor_id: GitHub.context[:actor_id] || actor_id,
      triggered_at: start,
    )
  elsif (priority == :direct) && (actor_type == "Team") && (subject_type == "Repository")
    # Team removed access to a repo
    Hook::Event::TeamEvent.queue(
      action: :removed_from_repository,
      actor_id: GitHub.context[:actor_id] || actor_id,
      team_id: actor_id,
      repo_id: subject_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe /\Aissue\.(create|transform_to_pull)\Z/ do |name, start, ending, transaction_id, payload|
  issue_id, pull_request_id, user_id = payload.values_at(:issue_id, :pull_request_id, :user_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  if pull_request_id
    Hook::Event::PullRequestEvent.queue(
      action: :opened,
      pull_request_id: pull_request_id,
      actor_id: user_id,
      triggered_at: start,
    )
  else
    Hook::Event::IssuesEvent.queue(
      action: :opened,
      issue_id: issue_id,
      actor_id: user_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe /\Aissue\.event\.(closed|reopened)\Z/ do |name, start, ending, transaction_id, payload|
  issue_id, pull_request_id, event, actor_id = payload.values_at(:issue_id, :pull_request_id, :event, :actor_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  if pull_request_id
    Hook::Event::PullRequestEvent.queue(
      action: event.to_sym,
      pull_request_id: pull_request_id,
      actor_id: actor_id,
      triggered_at: start,
    )
  else
    Hook::Event::IssuesEvent.queue(
      action: event.to_sym,
      issue_id: issue_id,
      actor_id: actor_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe /\Aissue\.event\.(locked|unlocked)\Z/ do |name, start, ending, transaction_id, payload|
  issue_id, pull_request_id, event, actor_id = payload.values_at(:issue_id, :pull_request_id, :event, :actor_id)
  next if spammy_user_acting_outside_own_repos?(payload)
  next if issue_id.nil? && pull_request_id.nil?

  if pull_request_id
    Hook::Event::PullRequestEvent.queue(
      action: event.to_sym,
      pull_request_id: pull_request_id,
      actor_id: actor_id,
      triggered_at: start,
    )
  else
    Hook::Event::IssuesEvent.queue(
      action: event.to_sym,
      issue_id: issue_id,
      actor_id: actor_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe /\Aissue\.event\.(assigned|unassigned)\Z/ do |name, start, ending, transaction_id, payload|
  issue_id, pull_request_id, event, subject_id, assignee_id = payload.values_at(:issue_id, :pull_request_id, :event, :subject_id, :assignee_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  if pull_request_id
    Hook::Event::PullRequestEvent.queue(
      action: event.to_sym,
      pull_request_id: pull_request_id,
      actor_id: subject_id,
      assignee_id: assignee_id,
      triggered_at: start,
    )
  else
    Hook::Event::IssuesEvent.queue(
      action: event.to_sym,
      issue_id: issue_id,
      actor_id: subject_id,
      assignee_id: assignee_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe /\Aissue\.event\.(review_requested|review_request_removed)\Z/ do |name, start, ending, transaction_id, payload|
  pull_request_id, event, subject_id, subject_type, actor_id = payload.values_at(:pull_request_id, :event, :subject_id, :subject_type, :actor_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::PullRequestEvent.queue(
    action: event.to_sym,
    pull_request_id: pull_request_id,
    actor_id: actor_id,
    subject_id: subject_id,
    subject_type: subject_type,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe /\Aissue\.event\.(labeled|unlabeled)\Z/ do |name, start, ending, transaction_id, payload|
  issue_id, pull_request_id, event, actor_id, label_id = payload.values_at(:issue_id, :pull_request_id, :event, :actor_id, :label_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  if pull_request_id
    Hook::Event::PullRequestEvent.queue(
      action: event.to_sym,
      pull_request_id: pull_request_id,
      actor_id: actor_id,
      label_id: label_id,
      triggered_at: start,
    )
  else
    Hook::Event::IssuesEvent.queue(
      action: event.to_sym,
      issue_id: issue_id,
      actor_id: actor_id,
      label_id: label_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe /\Aissue\.event\.(milestoned|demilestoned)\Z/ do |name, start, ending, transaction_id, payload|
  event, issue_id, actor_id, milestone_id = payload.values_at(:event, :issue_id, :actor_id, :milestone_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::IssuesEvent.queue(
    action: event.to_sym,
    issue_id: issue_id,
    actor_id: actor_id,
    milestone_id: milestone_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe /\Aissue\.(delete|transferred|pinned|unpinned)\Z/ do |name, start, ending, transaction_id, payload|
  event, issue_id, actor_id = payload.values_at(:event, :issue_id, :actor_id)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::IssuesEvent.queue(
    action: event.to_sym,
    actor_id: actor_id,
    triggered_at: start,
    issue_id: issue_id,
  )
end

HookEventSubscriber.subscribe "issue.update" do |name, start, ending, transaction_id, payload|
  issue_id, pull_request_id, actor_id, old_title, title, old_body, body = payload.values_at(:issue_id, :pull_request_id, :actor_id, :old_title, :title, :old_body, :body)
  next if spammy_user_acting_outside_own_repos?(payload)

  changes = {
    old_title: old_title,
    title: title,
    old_body: old_body,
    body: body,
  }

  if pull_request_id
    Hook::Event::PullRequestEvent.queue(
      action: :edited,
      pull_request_id: pull_request_id,
      actor_id: actor_id,
      changes: changes,
      triggered_at: start,
    )
  else
    Hook::Event::IssuesEvent.queue(
      action: :edited,
      issue_id: issue_id,
      actor_id: actor_id,
      changes: changes,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe "label.create" do |name, start, ending, transaction_id, payload|
  label_id, actor_id = payload.values_at(:label_id, :actor_id)

  Hook::Event::LabelEvent.queue(
    action: :created,
    label_id: label_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "label.update" do |name, start, ending, transaction_id, payload|
  label_id, actor_id, changes = payload.values_at(:label_id, :actor_id, :changes)
  Hook::Event::LabelEvent.queue(
    action: :edited,
    label_id: label_id,
    actor_id: actor_id,
    changes:  changes,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "pull_request.synchronize" do |name, start, ending, transaction_id, payload|
  pull_request_id, actor_id, before, after = payload.values_at(:pull_request_id, :actor_id, :before, :after)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::PullRequestEvent.queue(
    action: :synchronize,
    pull_request_id: pull_request_id,
    actor_id: actor_id,
    before: before,
    after: after,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "pull_request.change_base" do |name, start, ending, transaction_id, payload|
  pull_request_id, actor_id, before_base_ref, before_base_sha, after_base_ref, after_base_sha = payload.values_at(:pull_request_id, :actor_id, :before_base_ref, :before_base_sha, :after_base_ref, :after_base_sha)
  next if spammy_user_acting_outside_own_repos?(payload)

  Hook::Event::PullRequestEvent.queue(
    action: :edited,
    pull_request_id: pull_request_id,
    actor_id: actor_id,
    changes: {
      old_base_ref: before_base_ref,
      old_base_sha: before_base_sha,
      base_ref: after_base_ref,
      base_sha: after_base_sha,
      triggered_at: start,
    },
  )
end

HookEventSubscriber.subscribe "pull_request.ready_for_review" do |name, start, ending, transaction_id, payload|
  pull_request_id, actor_id = payload.values_at(:pull_request_id, :actor_id)
  Hook::Event::PullRequestEvent.queue(
    action: :ready_for_review,
    pull_request_id: pull_request_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "pull_request.converted_to_draft" do |name, start, ending, transaction_id, payload|
  pull_request_id, actor_id = payload.values_at(:pull_request_id, :actor_id)
  Hook::Event::PullRequestEvent.queue(
    action: :converted_to_draft,
    pull_request_id: pull_request_id,
    actor_id: actor_id,
    triggered_at: start
  )
end

HookEventSubscriber.subscribe "post_receive.refs.destroy" do |name, start, ending, transaction_id, payload|
  repo_id, ref, pusher_id = payload.values_at(:repo_id, :ref, :pusher_id)
  Hook::Event::DeleteEvent.queue(
    repository_id: repo_id,
    ref: ref,
    pusher_id: pusher_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "post_receive.refs.create" do |name, start, ending, transaction_id, payload|
  repo_id, ref, pusher_id = payload.values_at(:repo_id, :ref, :pusher_id)
  Hook::Event::CreateEvent.queue(
    repository_id: repo_id,
    ref: ref,
    pusher_id: pusher_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "wiki.push" do |name, start, ending, transaction_id, payload|
  actor_id, repo_id, updates = payload.values_at(:actor_id, :repo_id, :updates)
  Hook::Event::GollumEvent.queue(
    actor_id: actor_id,
    repository_id: repo_id,
    updates: updates,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "user.create" do |name, start, ending, transaction_id, payload|
  if GitHub.user_hooks_enabled?
    user_id, actor_id = payload.values_at(:user_id, :actor_id)
    Hook::Event::UserEvent.queue(
      action: :created,
      user_id: user_id,
      triggered_at: start,
      actor_id: actor_id,
    )
  end
end

HookEventSubscriber.subscribe "org.create" do |name, start, ending, transaction_id, payload|
  if GitHub.extended_org_hooks_enabled?
    actor_id, org_id = payload.values_at(:actor_id, :org_id)
    Hook::Event::OrganizationEvent.queue(
      action: :created,
      actor_id: actor_id,
      organization_id: org_id,
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe "org.add_member" do |name, start, ending, transaction_id, payload|
  actor_id, user_id, org_id = payload.values_at(:actor_id, :user_id, :org_id)
  Hook::Event::OrganizationEvent.queue(
    action: :member_added,
    actor_id: actor_id,
    user_id: user_id,
    organization_id: org_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "org.remove_member" do |name, start, ending, transaction_id, payload|
  actor_id, user_id, org_id = payload.values_at(:actor_id, :user_id, :org_id)
  Hook::Event::OrganizationEvent.queue(
    action: :member_removed,
    actor_id: actor_id,
    user_id: user_id,
    organization_id: org_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "org.invite_member" do |name, start, ending, transaction_id, payload|
  actor_id, org_id, invitation_id, user_id = payload.values_at(:actor_id, :org_id, :invitation_id, :user_id)
  Hook::Event::OrganizationEvent.queue(
    action: :member_invited,
    actor_id: actor_id,
    invitation_id: invitation_id,
    organization_id: org_id,
    user_id: user_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "org.rename" do |name, start, ending, transaction_id, payload|
  org_id, actor_id = payload.values_at(:org_id, :actor_id)
  changes = payload.slice(:old_login)

  Hook::Event::OrganizationEvent.queue(
    action: :renamed,
    organization_id: org_id,
    actor_id: actor_id,
    triggered_at: start,
    changes: changes,
  )
end

HookEventSubscriber.subscribe "repo.update_member" do |name, start, ending, transaction_id, payload|
  actor_id, user_id, repo_id, old_permission, new_permission = payload.values_at(:actor_id, :user_id, :repo_id, :old_permission, :new_permission)

  changes = {
    old_permission: old_permission,
    new_permission: new_permission,
  }

  Hook::Event::MemberEvent.queue(
    action: :edited,
    actor_id: actor_id,
    user_id: user_id,
    repo_id: repo_id,
    changes: changes,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project.create" do |name, start, ending, transaction_id, payload|
  project_id, actor_id = payload.values_at(:project_id, :actor_id)
  Hook::Event::ProjectEvent.queue(
    action: :created,
    project_id: project_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project.update" do |name, start, ending, transaction_id, payload|
  project_id, actor_id, changes = payload.values_at(:project_id, :actor_id, :changes)

  next unless changes.has_key?(:name) || changes.has_key?(:body)

  Hook::Event::ProjectEvent.queue(
    action: :edited,
    project_id: project_id,
    actor_id: actor_id,
    changes: changes,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project.close" do |name, start, ending, transaction_id, payload|
  project_id, actor_id = payload.values_at(:project_id, :actor_id)

  Hook::Event::ProjectEvent.queue(
    action: :closed,
    project_id: project_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project.open" do |name, start, ending, transaction_id, payload|
  project_id, actor_id = payload.values_at(:project_id, :actor_id)

  Hook::Event::ProjectEvent.queue(
    action: :reopened,
    project_id: project_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project_column.create" do |name, start, ending, transaction_id, payload|
  project_column_id, actor_id = payload.values_at(:project_column_id, :actor_id)

  Hook::Event::ProjectColumnEvent.queue(
    action: :created,
    project_column_id: project_column_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project_column.update" do |name, start, ending, transaction_id, payload|
  project_column_id, actor_id, changes = payload.values_at(:project_column_id, :actor_id, :changes)

  next unless changes[:name].present?

  Hook::Event::ProjectColumnEvent.queue(
    action: :edited,
    project_column_id: project_column_id,
    actor_id: actor_id,
    changes: changes,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project_column.move" do |name, start, ending, transaction_id, payload|
  project_column_id, actor_id, after_id = payload.values_at(:project_column_id, :actor_id, :after_id)

  Hook::Event::ProjectColumnEvent.queue(
    action: :moved,
    project_column_id: project_column_id,
    actor_id: actor_id,
    after_id: after_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project_card.create" do |name, start, ending, transaction_id, payload|
  project_card_id, actor_id = payload.values_at(:project_card_id, :actor_id)

  Hook::Event::ProjectCardEvent.queue(
    action: :created,
    project_card_id: project_card_id,
    actor_id: actor_id,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project_card.update" do |name, start, ending, transaction_id, payload|
  project_card_id, actor_id, changes = payload.values_at(:project_card_id, :actor_id, :changes)

  Hook::Event::ProjectCardEvent.queue(
    action: :edited,
    project_card_id: project_card_id,
    actor_id: actor_id,
    changes: changes,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project_card.convert" do |name, start, ending, transaction_id, payload|
  project_card_id, actor_id, changes = payload.values_at(:project_card_id, :actor_id, :changes)

  Hook::Event::ProjectCardEvent.queue(
    action: :converted,
    project_card_id: project_card_id,
    actor_id: actor_id,
    changes: changes,
    triggered_at: start,
  )
end

HookEventSubscriber.subscribe "project_card.move" do |name, start, ending, transaction_id, payload|
  project_card_id, actor_id, after_id, changes = payload.values_at(:project_card_id, :actor_id, :after_id, :changes)

    Hook::Event::ProjectCardEvent.queue(
      action: :moved,
      project_card_id: project_card_id,
      actor_id: actor_id,
      after_id: after_id,
      changes: changes,
      triggered_at: start,
    )
end

# We disallow spammy users from triggering webhooks if they are operating
# outside of repos they have been granted write access to
def spammy_user_acting_outside_own_repos?(payload)
  payload[:spammy] && !payload[:allowed]
end

HookEventSubscriber.subscribe "org.block_user" do |name, start, ending, transaction_id, payload|
  Hook::Event::OrgBlockEvent.queue({
    org_id: payload[:org_id],
    blocked_user_id: payload[:blocked_user_id],
    actor_id: payload[:actor_id],
    action: :blocked,
    triggered_at: start,
  })
end

HookEventSubscriber.subscribe "org.unblock_user" do |name, start, ending, transaction_id, payload|
  Hook::Event::OrgBlockEvent.queue({
    org_id: payload[:org_id],
    blocked_user_id: payload[:blocked_user_id],
    actor_id: payload[:actor_id],
    action: :unblocked,
    triggered_at: start,
  })
end

# Marketplace hook events
HookEventSubscriber.subscribe /marketplace\_purchase\..*/ do |name, start, ending, transaction_id, payload|
  Hook::Event::MarketplacePurchaseEvent.queue \
    payload.merge(action: name.split(".").last, triggered_at: start)
end

# Sponsors hook events
HookEventSubscriber.subscribe "sponsors.sponsor_sponsorship_create" do |name, start, ending, transaction_id, payload|
  Hook::Event::SponsorshipEvent.queue(
    action: :created,
    sponsorship_id: payload[:sponsorship_id],
    actor_id: payload[:actor_id],
    current_tier_id: payload[:current_tier_id],
  )
end

HookEventSubscriber.subscribe "sponsors.sponsor_sponsorship_cancel" do |name, start, ending, transaction_id, payload|
  Hook::Event::SponsorshipEvent.queue(
    action: :cancelled,
    sponsorship_id: payload[:sponsorship_id],
    actor_id: payload[:actor_id],
    current_tier_id: payload[:current_tier_id],
  )
end

HookEventSubscriber.subscribe "sponsors.sponsor_sponsorship_pending_cancellation" do |name, start, ending, transaction_id, payload|
  Hook::Event::SponsorshipEvent.queue(
    action: :pending_cancellation,
    sponsorship_id: payload[:sponsorship_id],
    actor_id: payload[:actor_id],
    current_tier_id: payload[:current_tier_id],
    pending_change_on: payload[:pending_change_on],
  )
end

HookEventSubscriber.subscribe "sponsors.sponsor_sponsorship_edited" do |name, start, ending, transaction_id, payload|
  Hook::Event::SponsorshipEvent.queue(
    action: :edited,
    sponsorship_id: payload[:sponsorship_id],
    actor_id: payload[:actor_id],
    current_tier_id: payload[:current_tier_id],
    changes: payload[:changes],
  )
end

HookEventSubscriber.subscribe "sponsors.sponsor_sponsorship_tier_change" do |name, start, ending, transaction_id, payload|
  Hook::Event::SponsorshipEvent.queue(
    action: :tier_changed,
    sponsorship_id: payload[:sponsorship_id],
    actor_id: payload[:actor_id],
    current_tier_id: payload[:current_tier_id],
    previous_tier_id: payload[:previous_tier_id],
  )
end

HookEventSubscriber.subscribe "sponsors.sponsor_sponsorship_pending_tier_change" do |name, start, ending, transaction_id, payload|
  Hook::Event::SponsorshipEvent.queue(
    action: :pending_tier_change,
    sponsorship_id: payload[:sponsorship_id],
    actor_id: payload[:actor_id],
    current_tier_id: payload[:current_tier_id],
    pending_change_tier_id: payload[:pending_change_tier_id],
    pending_change_on: payload[:pending_change_on],
  )
end

# Check related events
HookEventSubscriber.subscribe "check_suite.request" do |name, start, ending, transaction_id, payload|
  Hook::Event::CheckSuiteEvent.queue(
    action: :requested,
    check_suite_id: payload[:check_suite_id],
    specific_app_only: true,
    actor_id: payload[:actor_id],
  )
end

HookEventSubscriber.subscribe "check_suite.rerequest" do |name, start, ending, transaction_id, payload|
  Hook::Event::CheckSuiteEvent.queue(
    action: :rerequested,
    check_suite_id: payload[:check_suite_id],
    specific_app_only: true,
    actor_id: payload[:actor_id],
  )
end

HookEventSubscriber.subscribe "check_suite.complete" do |name, start, ending, transaction_id, payload|
  Hook::Event::CheckSuiteEvent.queue(
    action: :completed,
    check_suite_id: payload[:check_suite_id],
  )
end

HookEventSubscriber.subscribe "check_run.create" do |name, start, ending, transaction_id, payload|
  Hook::Event::CheckRunEvent.queue(
    action: :created,
    check_run_id: payload[:check_run_id],
  )
end

HookEventSubscriber.subscribe "check_run.rerequest" do |name, start, ending, transaction_id, payload|
  Hook::Event::CheckRunEvent.queue(
    action: :rerequested,
    check_run_id: payload[:check_run_id],
    specific_app_only: true,
    actor_id: payload[:actor_id],
  )
end

HookEventSubscriber.subscribe "check_run.request_action" do |name, start, ending, transaction_id, payload|
  Hook::Event::CheckRunEvent.queue(
    action: :requested_action,
    check_run_id: payload[:check_run_id],
    specific_app_only: true,
    actor_id: payload[:actor_id],
    requested_action: payload[:requested_action],
  )
end

HookEventSubscriber.subscribe "check_run.complete" do |name, start, ending, transaction_id, payload|
  Hook::Event::CheckRunEvent.queue(
    action: :completed,
    check_run_id: payload[:check_run_id],
  )
end

HookEventSubscriber.subscribe "content_reference.create" do |name, start, ending, transaction_id, payload|
  Hook::Event::ContentReferenceEvent.queue(
    action: :created,
    content_reference_id: payload[:content_reference_id],
  )
end

HookEventSubscriber.subscribe "repo.config.enable_anonymous_git_access" do |name, start, ending, transaction_id, payload|
  if GitHub.anonymous_git_access_enabled?
    Hook::Event::RepositoryEvent.queue(
      action: :anonymous_access_enabled,
      repository_id: payload[:repo_id],
      actor_id: payload[:actor_id],
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe "repo.config.disable_anonymous_git_access" do |name, start, ending, transaction_id, payload|
  if GitHub.anonymous_git_access_enabled?
    Hook::Event::RepositoryEvent.queue(
      action: :anonymous_access_disabled,
      repository_id: payload[:repo_id],
      actor_id: payload[:actor_id],
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe "repository_import.import" do |name, start, ending, transaction_id, payload|
  Hook::Event::RepositoryImportEvent.queue(
    status: payload[:status],
    repository_id: payload[:repo_id],
    actor_id: payload[:actor_id],
    triggered_at: start,
  )
end

if GitHub.enterprise?
  HookEventSubscriber.subscribe "enterprise.config.enable_anonymous_git_access" do |name, start, ending, transaction_id, payload|
    Hook::Event::EnterpriseEvent.queue(
      action: :anonymous_access_enabled,
      actor_id: payload[:actor_id],
      triggered_at: start,
    )
  end

  HookEventSubscriber.subscribe "enterprise.config.disable_anonymous_git_access" do |name, start, ending, transaction_id, payload|
    Hook::Event::EnterpriseEvent.queue(
      action: :anonymous_access_disabled,
      actor_id: payload[:actor_id],
      triggered_at: start,
    )
  end
end

HookEventSubscriber.subscribe("public_key.create") do |name, start, _, _, payload|
  next unless payload[:repo_id]

  attrs = {
    action: :created,
    triggered_at: start,
    key_id: payload[:public_key_id],
    repository_id: payload[:repo_id],
    actor_id: payload[:actor_id],
  }

  Hook::Event::DeployKeyEvent.queue(attrs)
end
