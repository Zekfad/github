# rubocop:disable Style/FrozenStringLiteralComment

GitHub.subscribe "profile_picture.update" do |event, start, ending, transaction_id, payload|
  if payload[:type] == "Integration"
    ContentReferenceAttachmentRefreshBodyHtmlJob.perform_later(payload[:integration_id])
  end
end
