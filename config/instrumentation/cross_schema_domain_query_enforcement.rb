# frozen_string_literal: true

# See https://githubber.com/article/technology/dotcom/schema-domains for more context.

# Instrument unless we're currently running in enterprise mode.
if !GitHub.enterprise?
  query_subscriber = GitHub::SchemaDomain::QuerySubscriber.new(
    raise_errors: !Rails.production?,
    report_errors: Rails.production?,
  )
  ActiveSupport::Notifications.subscribe "sql.active_record", query_subscriber

  if Rails.production?
    transaction_subscriber = GitHub::SchemaDomain::TransactionSubscriber.new(
      raise_errors: false,
      report_errors: false,
    )
    ActiveSupport::Notifications.subscribe "transaction.active_record", transaction_subscriber
  end
end
