# frozen_string_literal: true

::Audit::Hydro::EventForwarder.configure(source: GitHub.instrumentation_service) do
  Audit::ACTIONS.each do |event|
    subscribe(event) do |payload|
      message = build_publish_message(event, payload)
      publish(message, schema: "audit_log.v2.AuditEntry")
    end
  end
end
