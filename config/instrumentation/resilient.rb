# rubocop:disable Style/FrozenStringLiteralComment

GitHub.subscribe "resilient.circuit_breaker.allow_request" do |name, start, ending, transaction_id, payload|
  if key = payload[:key]
    GitHub.dogstats.timing("resilient.circuit_breaker.allow_request.timing", (ending - start) * 1_000, tags: ["key:#{key.name}"])

    if payload[:force_closed]
      GitHub.dogstats.increment("resilient.circuit_breaker.force_closed", tags: ["key:#{key.name}"])
    end

    if payload[:force_open]
      GitHub.dogstats.increment("resilient.circuit_breaker.force_open", tags: ["key:#{key.name}"])
    end

    if payload[:result]
      GitHub.dogstats.increment("resilient.circuit_breaker.allowed", tags: ["key:#{key.name}"])
    else
      GitHub.dogstats.increment("resilient.circuit_breaker.rejected", tags: ["key:#{key.name}"])
    end
  end
end

GitHub.subscribe "resilient.circuit_breaker.open" do |name, start, ending, transaction_id, payload|
  if key = payload[:key]
    if payload[:result]
      GitHub.dogstats.increment("resilient.circuit_breaker.open", tags: ["key:#{key.name}"])
    end
  end
end

GitHub.subscribe "resilient.circuit_breaker.allow_single_request" do |name, start, ending, transaction_id, payload|
  if key = payload[:key]
    if payload[:result]
      GitHub.dogstats.increment("resilient.circuit_breaker.allow_single_request", tags: ["key:#{key.name}"])
    else
      GitHub.dogstats.increment("resilient.circuit_breaker.deny_single_request", tags: ["key:#{key.name}"])
    end
  end
end

GitHub.subscribe "resilient.circuit_breaker.success" do |name, start, ending, transaction_id, payload|
  if key = payload[:key]
    GitHub.dogstats.increment("resilient.circuit_breaker.success", tags: ["key:#{key.name}"])

    # this only happens if the circuit was open and mark success closed it;
    # shows us that circuits are correctly being closed
    if payload[:closed_the_circuit]
      GitHub.dogstats.increment("resilient.circuit_breaker.success.closed_the_circuit", tags: ["key:#{key.name}"])
    end
  end
end

GitHub.subscribe "resilient.circuit_breaker.failure" do |name, start, ending, transaction_id, payload|
  if key = payload[:key]
    GitHub.dogstats.increment("resilient.circuit_breaker.failure", tags: ["key:#{key.name}"])
  end
end
