# rubocop:disable Style/FrozenStringLiteralComment

valid_keys = %w(
  issue
  issue_comment
  pull_request
  pull_request_review_comment
  commit_comment
  discussion_message
  milestone
).freeze

GitHub.subscribe "task_list.update" do |*args|
  event = ActiveSupport::Notifications::Event.new(*args)
  type, checked, key, actor_id, repository_id = event.payload.values_at :type, :value, :key, :actor_id, :repository_id
  if valid_keys.include?(key)
    case type
    when :checked
      stat = "task_list.#{key}.#{'un' if !checked}check"
      p [:instrument, stat] if Rails.env.development?
    when :reordered
      TaskListInstrumentationJob.perform_later(actor_id, repository_id, key)
    end
  end
end
