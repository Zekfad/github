# rubocop:disable Style/FrozenStringLiteralComment

# Reporting timing info for aqueduct-related events
require "github/config/notifications"
require "github/config/stats"

# Most metrics are emitted from GitHub::Aqueduct::Job. The resulting hierarchy:
#
# For enqueue:
#
#   rpc.aqueduct.enqueue.time      - timing for the entire enqueue call
#     rpc.aqueduct.callbacks       - timing for the before_enqueue callback
#     rpc.aqueduct.job_encode.time - encoding the job args to JSON
#     rpc.aqueduct.time            - timing from the aqueduct client rpc call
#       rpc.aqueduct.http-request.time  - internal timing from excon adapter
#       rpc.aqueduct.http-response.time - internal timing from excon adapter
#
# TODO: the aqueduct client should emit relevant metrics to a configurable
# instrumenter. This would include a new metric for protobuf encoding and
# would also replace our rpc.aqueduct.time measurement of the send_job call.
# The above JSON encoding timer is so named to make room for a future
# `rpc.aqueduct.encode.time` for measuring the internal protobuf encoding.
#
# The excon instrumentation is configured in `GitHub.aqueduct` and
# `GitHub.aqueduct_double_publish`.
GitHub.subscribe(/^aqueduct(-double-publish)?\.excon\.request/) do |event, start, finish, id, payload|
  client_name = event.include?("double") ? "aqueduct-double-publish" : "aqueduct"

  payload ||= {}
  # :path => "/twirp/aqueduct.api.v1.JobQueueService/Send"
  operation = payload.fetch(:path, "").sub("/twirp/", "")

  GitHub.dogstats.timing(
    "rpc.aqueduct.http-request.time",
    (finish - start) * 1000,
    tags: [
      "rpc_operation:#{operation}",
      "rpc_host:#{payload[:host]}",
      "rpc_client:#{client_name}",
    ],
  )
end

GitHub.subscribe(/^aqueduct(-double-publish)?\.excon\.response/) do |event, start, finish, id, payload|
  client_name = event.include?("double") ? "aqueduct-double-publish" : "aqueduct"

  payload ||= {}
  response = payload.fetch(:response, {})
  # :path => "/twirp/aqueduct.api.v1.JobQueueService/Send"
  operation = response.fetch(:path, "").sub("/twirp/", "")

  GitHub.dogstats.timing(
    "rpc.aqueduct.http-response.time",
    (finish - start) * 1000,
    tags: [
      "rpc_operation:#{operation}",
      "rpc_host:#{response[:host]}",
      "rpc_client:#{client_name}",
      "rpc_status:#{response[:status]}", # http response code
    ],
  )
end
