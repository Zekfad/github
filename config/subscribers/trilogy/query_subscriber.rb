# frozen_string_literal: true

module Trilogy::QuerySubscriber
  class << self
    def call(name, start, finish, id, payload = {})
      return if payload[:cached]

      time_span = TimeSpan.new(start, finish)
      connection_info = ConnectionInfo.new(
        url: payload[:connection_url],
        host: payload[:connected_host],
        config_host: config_host(payload[:connection_url]),
      )

      GitHub::MysqlInstrumenter.track_query(
        payload[:sql],
        payload[:result],
        connection_info,
        time_span: time_span,
      )

      GitHub::MysqlQueryCounter.
        count(payload[:type], connection_info.config_host)

      SlowQueryReporter.
        new(payload[:sql], time_span.duration_seconds, connection_info).
        report
    end

    def subscribe
      ActiveSupport::Notifications.subscribe(
        TrilogyAdapter::Events::QUERY,
        self,
      )
    end

    private

    def config_host(url)
      return nil unless url
      @config_host ||= {}
      @config_host[url] ||= begin
        Addressable::URI.parse("mysql://#{url}").host
      end
    end
  end
end

Trilogy::QuerySubscriber.subscribe
