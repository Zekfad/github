# frozen_string_literal: true

class Trilogy
  class ChangeDatabaseSubscriber
    def initialize(notifier: ActiveSupport::Notifications, stats: GitHub.dogstats)
      @notifier = notifier
      @stats = stats
    end

    def call(name, started_at, finished_at, id, payload = {})
      stats.timing "rpc.mysql.change_db.time",
                   TimeSpan.new(started_at, finished_at).duration,
                   tags: ["rpc_host:#{payload[:host]}"]
    end

    def subscribe
      notifier.subscribe TrilogyAdapter::Events::CHANGE_DATABASE, self
    end

    private

    attr_reader :notifier, :stats
  end
end

Trilogy::ChangeDatabaseSubscriber.new.subscribe
