# frozen_string_literal: true

class Trilogy
  class ConnectSubscriber
    def initialize(notifier: ActiveSupport::Notifications, stats: GitHub.dogstats)
      @notifier = notifier
      @stats = stats
    end

    def call(name, started_at, finished_at, id, payload = {})
      stats.timing "rpc.mysql.connect.time",
                   TimeSpan.new(started_at, finished_at).duration,
                   tags: ["rpc_host:#{payload[:host]}"]
    end

    def subscribe
      notifier.subscribe TrilogyAdapter::Events::CONNECT, self
    end

    private

    attr_reader :notifier, :stats
  end
end

Trilogy::ConnectSubscriber.new.subscribe
