# frozen_string_literal: true

module QueryWarningLogger
  class QueryWarning < StandardError
    attr_reader :sql, :level, :code, :message

    def initialize(sql, level, code, message)
      @sql, @level, @code, @message = sql, level, code, message

      super "[#{level}] #{code}: #{message}\n\n#{sql}"
    end
  end
end

module Trilogy::QueryWarningSubscriber
  IGNORED_WARNING_CODES = [
    # ER_DUP_ENTRY and ER_DUP_ENTRY_WITH_KEY_NAME should not raise warning
    # to Sentry as these warnings can happen anywhere INSERT IGNORE is used
    # and a duplicate exists. INSERT IGNORE is currently used heavily by
    # GitHub::Migrator.
    1062,
    1586,
    # Unsafe statement written to the binary log using statement format since
    # BINLOG_FORMAT = STATEMENT. INSERT... ON DUPLICATE KEY UPDATE on a table
    # with more than one UNIQUE KEY is unsafe:
    1592,
  ]
  IGNORED_WARNING_MESSAGES = [
    "Column 'subgroup' cannot be null",
  ]

  class << self
    def call(name, start, finish, id, payload)
      payload[:warnings].
        reject { |warning| ignore?(*warning) }.
        each do |warning|
          digested_sql = GitHub::SQL::Digester.digest_sql(payload[:sql])
          error = QueryWarningLogger::QueryWarning.new(digested_sql, *warning)
          error.set_backtrace(caller)
          report(error)
        end
    end

    def subscribe
      ActiveSupport::Notifications.subscribe(TrilogyAdapter::Events::WARNING, self)
    end

    private

    def ignore?(level, code, message)
      IGNORED_WARNING_CODES.include?(code) || IGNORED_WARNING_MESSAGES.include?(message)
    end

    def report(error)
      # Failbot may execute SQL when generating context, so we must avoid reporting recursively.
      # See https://github.com/github/availability/issues/80
      return if @reporting_error
      @reporting_error = true

      Failbot.report(
        error,
        rollup: Digest::MD5.hexdigest(error.sql),
        sql_sanitized: error.sql,
        app: "github-query-warning",
      )
    ensure
      @reporting_error = false
    end
  end
end

Trilogy::QueryWarningSubscriber.subscribe if Rails.env.production?
