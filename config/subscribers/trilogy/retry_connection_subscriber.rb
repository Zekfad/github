# frozen_string_literal: true

module Trilogy::RetryConnectionSubscriber
  class << self
    def call(name, start, finish, id, payload = {})
      GitHub.dogstats.increment("rpc.mysql.retry.query.count", tags: [
        "exception:#{payload[:error].class.name}",
      ])
    end

    def subscribe
      ActiveSupport::Notifications.subscribe(TrilogyAdapter::Events::RETRY_CONNECTION, self)
    end
  end
end

Trilogy::RetryConnectionSubscriber.subscribe
