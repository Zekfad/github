# frozen_string_literal: true

module Trilogy::TransactionSubscriber
  class << self
    def call(name, start, finish, id, payload = {})
      transaction_time_span = TimeSpan.new(payload[:transaction_start], finish)

      GitHub::MysqlInstrumenter.add_transaction_time(transaction_time_span.duration_seconds)

      SlowTransactionReporter.
        new(transaction_time_span.duration, payload[:queries]).
        report
    end

    def subscribe
      ActiveSupport::Notifications.subscribe(TrilogyAdapter::Events::TRANSACTION, self)
    end
  end
end

Trilogy::TransactionSubscriber.subscribe
