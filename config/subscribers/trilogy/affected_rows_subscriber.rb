# frozen_string_literal: true

module QueryAffectedRowsLogger
  class QueryAffectedRows < StandardError; end
end

module Trilogy::AffectedRowsSubscriber
  AFFECTED_ROWS_THRESHOLD = 2_000

  class << self
    def call(name, start, finish, id, payload = {})
      sql           = payload[:query]
      affected_rows = payload[:affected_rows]

      if affected_rows >= AFFECTED_ROWS_THRESHOLD
        digested_sql = GitHub::SQL::Digester.digest_sql(sql)
        e = QueryAffectedRowsLogger::QueryAffectedRows.new("Changed #{affected_rows} rows\n#{digested_sql}")
        e.set_backtrace(caller)

        Failbot.report!(e,
          rollup: Digest::MD5.hexdigest(digested_sql),
          sql_sanitized: digested_sql,
          app: "github-query-affected-rows",
        )
      end
    end

    def subscribe
      ActiveSupport::Notifications.subscribe(TrilogyAdapter::Events::AFFECTED_ROWS, self)
    end
  end
end

Trilogy::AffectedRowsSubscriber.subscribe
