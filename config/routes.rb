# frozen_string_literal: true

GitHub::Application.routes.draw do
  if Rails.env.test?
    ::TestRoutes = ActionDispatch::Routing::RouteSet.new unless defined?(::TestRoutes)
    mount TestRoutes, at: "/"
  end

  REPO_REGEX = /(?:\w|\.|\-)+/i unless defined?(REPO_REGEX)
  # Copied from User::LOGIN_REGEX, but modified slightly because:
  # "Regexp anchor characters are not allowed in routing requirements"
  #
  # This supports "_" for legacy superfans with logins that still contain "_".
  # It also supports old usernames with leading dashes like -andrew-
  USERID_REGEX = /-?[a-z0-9][a-z0-9\-\_]*/i unless defined?(USERID_REGEX)

  # This supports int or shaish Gist IDs
  GISTID_REGEX   = /\d+|[a-f0-9]{32}|[a-f0-9]{20}?/ unless defined?(GISTID_REGEX)

  GIT_OID_REGEX = /[a-f0-9]{40}/ unless defined?(GIT_OID_REGEX)

  BRANCH_REGEX = /[^\/]+(\/[^\/]+)?/ unless defined?(BRANCH_REGEX)

  APP_FILTER_REGEX = %r{(app/)?[^/]+}i unless defined?(APP_FILTER_REGEX)

  # Allow periods and dashes in package names, but also allow % for URL-encoded characters
  PACKAGE_DEPENDENCY_REGEX = /(?:([-:.%\w]+)|(@[-:.\/%\w]+))/i unless defined?(PACKAGE_DEPENDENCY_REGEX)

  WEBHOOK_GUID_REGEX = /\h{8}-\h{4}-\h{4}-\h{4}-\h{12}/ unless defined?(WEBHOOK_GUID_REGEX)
  WEBHOOK_REGEX = /\d+/ unless defined?(WEBHOOK_REGEX)

  # GitHub fully supports IPv4 and IPv6. Routes don't allow anchors, so we must
  # convert the existing regex to a string, remove the anchors, and then convert
  # it back to a regex.
  IP_REGEX = Regexp.union(
    Regexp.new(Resolv::IPv4::Regex.to_s.gsub(/(\\A)|(\\z)/, "")),
    Regexp.new(Resolv::IPv6::Regex.to_s.gsub(/(\\A)|(\\z)/, "")),
  ) unless defined?(IP_REGEX)

  AUTHORIZATION_KEY_REGEX = /[a-f0-9]{20}|Iv1\.[a-f0-9]{16}/ unless defined?(AUTHORIZATION_KEY_REGEX)

  # Workspace names are built off the owner's login and the repository name. The
  # repo regex is inclusive of the user regex, so we can just use that.
  WORKSPACE_REGEX = REPO_REGEX unless defined?(WORKSPACE_REGEX)
  CODESPACE_REGEX = WORKSPACE_REGEX unless defined?(CODESPACE_REGEX)

  # This constraint will redirect all notifications routes to the relevant notifications v2 routes
  # unless the GitHub.notifications_v2_disabled? configuration is true.
  NOTIFICATIONS_V2_REDIRECT_CONSTRAINT = lambda do |request|
    !GitHub.notifications_v2_disabled?
  end unless defined?(NOTIFICATIONS_V2_REDIRECT_CONSTRAINT)

  # Internal: handle optional segments in Rails 2 and 3 compatible way
  def optional(segment)
    "(#{segment})"
  end

  ##
  # Status
  get "/status", to: "site#status_lolrails"

  # User Hovercards
  # Deprecated route slated to be removed
  get "/hovercards", to: "hovercards/users#show_legacy"

  ##
  # Comment Edit History
  resources :user_content_edits, only: [:show, :destroy]
  get "/user_content_edits/show_edit_history/:comment_id", to: "user_content_edits#show_edit_history", as: :show_comment_edit_history
  post "/user_content_edits/dismiss_edit_history_onboarding/:content_id", to: "user_content_edits#dismiss_edit_history_onboarding", as: :dismiss_edit_history_onboarding

  # Live updates
  get "/socket-worker.js", to: "socket_worker#show"
  get "/_ws", to: "web_sockets#show", as: :web_socket

  ##############################################################################
  # NOTE: Only routes that should work on BOTH GitHub and Gist should appear
  # before this line
  ##############################################################################

  # Gists
  gist_routes = -> do
    get     "/",                  to: "gists/gists#new", as: :new_gist
    get     "/new",               to: "gists/gists#new"
    post    "/",                  to: "gists/gists#create", as: :gists
    get     "/detect_language",   to: "gists/gists#detect_language", as: :detect_gist_language

    # Public listing urls
    get     "/discover",          to: "gists/listings#discover", as: :discover_gists
    get     "/gists",             to: redirect("/discover")

    get     "/forked",            to: "gists/listings#forked", as: :forked_gists
    get     "/starred",           to: "gists/listings#starred", as: :starred_gists

    get     "/search",            to: "gists/searches#show", as: :gist_search
    get     "/search/quick",      to: "gists/searches#quick", as: :gist_quicksearch

    get     "/mine",              to: "gists/listings#mine", as: :my_gist_listings
    get     "/mine/forked",       to: "gists/listings#my_forked"
    get     "/mine/starred",      to: "gists/listings#my_starred"

    get "/join",    to: "gists/users#new", as: :gist_signup

    # UserDashboardPinsController (for gists only)
    post "/users/:user_id/dashboard-pin/:item_id", to: "user_dashboard_pins#create",
      as: :user_dashboard_gist_pin
    delete "/users/:user_id/dashboard-pin/:item_id", to: "user_dashboard_pins#destroy"

    # Notification routes for gists
    post "/notifications/beta/mark",        to: "notifications_v2#mark_as_read"
    post "/notifications/beta/unmark",      to: "notifications_v2#mark_as_unread"
    post "/notifications/beta/archive",     to: "notifications_v2#mark_as_archived"
    post "/notifications/beta/unarchive",   to: "notifications_v2#mark_as_unarchived"
    post "/notifications/beta/subscribe",   to: "notifications_v2#mark_as_subscribed"
    post "/notifications/beta/unsubscribe", to: "notifications_v2#mark_as_unsubscribed"
    post "/notifications/beta/star",        to: "notifications_v2#mark_as_starred"
    post "/notifications/beta/unstar",      to: "notifications_v2#mark_as_unstarred"

    constraints user_id: USERID_REGEX, gist_id: GISTID_REGEX, sha: GIT_OID_REGEX do
      # Return the default file in a gist via raw
      get "/:user_id/:gist_id/raw", to: "gists/gists#raw",
        format: false, as: :raw_user_gist

      get "/:user_id/:gist_id/raw/:sha", to: "gists/gists#raw", format: false

      # Default raw route for a gist file at a specific revision
      get "/:user_id/:gist_id/raw/:sha/:file", to: "gists/gists#raw",
        format: false, constraints: { file: /.+/ },
        as: :raw_user_gist_sha_file

      get "/:user_id/:gist_id/raw/:file", to: "gists/gists#raw",
        format: false, constraints: { file: /.+/ }

      if GitHub.gist3_domain?
        get "/auth/github", to: "gists/sessions#new", as: :gist_oauth_login
        get "/auth/github/callback", to: "gists/sessions#create", as: :gist_oauth_callback
        post "/auth/github/logout", to: "gists/sessions#destroy", as: :gist_oauth_logout

        # Redirect support/contact links per #44072
        get "/support", to: redirect("/support", host: GitHub.host_name)
        get "/contact", to: redirect("/contact", host: GitHub.host_name), as: false
        get "/contact/:flavor", to: redirect("/contact/%{flavor}", host: GitHub.host_name), as: false
        get "/login",   to: redirect("/login",   host: GitHub.host_name), as: false

        post "/preview", to: "comment_preview#show", as: false

        # For comment image uploads
        post "/upload/policies/:model", to: "upload_policies#create"
        put  "/upload/:model(/:id)",    to: "uploads#update"
        post "/upload/:model(/:id)",    to: "uploads#create"

        # Allow switching between desktop and mobile experience
        post   "/site/mobile_preference", to: "site#mobile_preference"
        post   "/site/toggle_site_admin_and_employee_status", to: "site#toggle_site_admin_and_employee_status"
        post   "/site/toggle_site_admin_performance_stats",   to: "site#toggle_site_admin_performance_stats"

        # Allow setting user protocol for cloning
        post   "/users/set_protocol",  to: "users#set_protocol"

        # Shortcuts cheatsheet
        get    "/site/keyboard_shortcuts", to: "site#keyboard_shortcuts"

        # Redirect any stafftools request to GitHub host
        get "/stafftools/graphs/flamegraph", to: "stafftools/graphs#flamegraph"
        get "/stafftools(/:path)", to: redirect("/stafftools/%{path}", host: GitHub.host_name),
          constraints: { path: /.*/ }

        # Add routes for user statuses so they can be viewed and changed
        get "/users/status",           to: "user_statuses#show"
        put "/users/status",           to: "user_statuses#update"
        get "/users/status/members",   to: "user_statuses#member_statuses"
        get "/users/status/emoji",     to: "user_statuses#emoji_picker"
        get "/users/status/organizations", to: "user_statuses#org_picker"

        # Duplicate the notice dismissal path so notices can be dismissed on gist pages
        post "/settings/dismiss-notice/:notice", to: "settings/dismissals#create"

        get "/_render_node/:id/*path", to: "nodes#show"
      end

      # This route also handles the legacy routes of:
      # - /:gist_id
      # - /:gist_id.{txt,js,pibb}
      get     "/:user_id",          to: "gists/users#show", as: :user_gists
      get     "/:user_id/public",   to: "gists/users#show",
        as: :user_public_gists, visibility: "public"
      get     "/:user_id/secret",   to: "gists/users#show",
        as: :user_secret_gists, visibility: "secret"

      # - Handle: /:gist_id/:sha legacy route
      get     "/:user_id/:sha",     to: "gists/users#show"
      # - Handle: /:gist_id/:sha legacy route
      get     "/:user_id/:sha.txt", to: "gists/users#show"

      # - Handles /:gist_id/{forks,stars,revisions}
      get     "/:user_id/forks",   to: "gists/users#show", legacy_redirect: "forks"
      get     "/:user_id/stars",   to: "gists/users#show", legacy_redirect: "stars"
      get     "/:user_id/revisions",   to: "gists/users#show", legacy_redirect: "revisions"
      get     "/:user_id/download",   to: "gists/users#show", legacy_redirect: "download"

      get     "/:user_id/forked",   to: "gists/users#forked", as: :user_forked_gists
      get     "/:user_id/forked/public",   to: "gists/users#forked", visibility: "public"
      get     "/:user_id/forked/secret",   to: "gists/users#forked", visibility: "secret"

      get     "/:user_id/starred",  to: "gists/users#starred", as: :user_starred_gists
      get     "/:user_id/starred/public",  to: "gists/users#starred", visibility: "public"
      get     "/:user_id/starred/secret",  to: "gists/users#starred", visibility: "secret"

      get     "/:user_id/:gist_id.json",               to: "gists/stateless#show", format: "json"
      get     "/:user_id/:gist_id/:revision.:format",  to: "gists/stateless#show", format: "json"

      get     "/:user_id/:gist_id", to: "gists/gists#show", as: :user_gist

      get     "/:user_id/:gist_id/:revision",      to: "gists/gists#show",
        as: :user_gist_at_revision, constraints: { revision: GIT_OID_REGEX }

      get     "/:user_id/:gist_id/forks",             to: "gists/gists#forks", as: :forks_user_gist
      get     "/:user_id/:gist_id/edit",              to: "gists/gists#edit", as: :edit_user_gist
      get     "/:user_id/:gist_id/revisions",         to: "gists/gists#revisions", as: :revisions_user_gist
      get     "/:user_id/:gist_id/download",          to: "gists/gists#archive", as: :download_user_gist, format: "zip"
      get     "/:user_id/:gist_id/suggestions",       to: "gists/gists#suggestions", as: :suggestions_user_gist
      put     "/:user_id/:gist_id",                   to: "gists/gists#update", as: :update_user_gist
      put     "/:user_id/:gist_id/make_public",       to: "gists/gists#make_public", as: :make_public_user_gist
      put     "/:user_id/:gist_id/file/:oid",         to: "gists/gists#update_file", as: :update_user_gist_file
      delete  "/:user_id/:gist_id",                   to: "gists/gists#destroy", as: :delete_user_gist
      post    "/:user_id/:gist_id/star",              to: "gists/gists#star", as: :star_gist
      post    "/:user_id/:gist_id/fork",              to: "gists/gists#fork", as: :fork_user_gist
      get     "/:user_id/:gist_id/stargazers",        to: "gists/gists#stargazers", as: :stargazers_gist
      post    "/:user_id/:gist_id/unstar",            to: "gists/gists#unstar", as: :unstar_gist

      get     "/:user_id/:gist_id/subscription",      to: "gists/gists#subscription", as: :subscription_gist
      post    "/:user_id/:gist_id/subscribe",         to: "gists/gists#subscribe", as: :subscribe_gist

      post    "/:user_id/:gist_id/comments",             to: "gists/comments#create",
        as: :user_gist_comments
      delete  "/:user_id/:gist_id/comments/:comment_id", to: "gists/comments#destroy",
        as: :delete_user_gist_comment
      put     "/:user_id/:gist_id/comments/:comment_id", to: "gists/comments#update",
        as: :update_user_gist_comment

      get "/:user_id/:gist_id/archive/:commitish.zip",   to: "gists/gists#archive",
        action: "archive", format: "zip", as: :zipball_user_gist
      get "/:user_id/:gist_id/archive/:commitish.tar.gz", to: "gists/gists#archive",
        action: "archive", format: "tar.gz", as: :tarball_user_gist

      # Allow user hovercards under the same path as github.com to prevent cross domain issues
      get "/users/:user_id/hovercard", to: "hovercards/users#show"
    end

    # NOTE: Due to the way Rails routing works this has to the be the last route in the
    #       Gist routing block.  This route makes sure all routes that are on GitHub.com
    #       that are not Gist routes do not work on gist.github.com.
    match "/:path", to: "gists/application#not_found", constraints: { path: /.*/ }, via: [:get, :post, :put, :delete, :patch]
  end

  if GitHub.gist3_domain?
    constraints({host: /#{GitHub.gist3_host_name}\.?/}, &gist_routes)
  else
    scope "/gist", &gist_routes
  end

  ##
  # OctoCaptcha
  if GitHub.funcaptcha_enabled?
    constraints(host: /#{GitHub.urls.octocaptcha_host_name}/) do
      get "/", to: "octocaptcha#index"
      get "/test", to: "octocaptcha#test"
      match "/:path", to: "octocaptcha#not_found", constraints: { path: /.*/ }, via: [:get, :post, :put, :delete, :patch]
    end
  end


  # Chatops endpoints
  if GitHub.chatops_endpoint_enabled?
    constraints(host: /\Achatops\.github\.(?:localhost|com)\Z/) do
      scope controller: "chatops/search" do
        get  "/_chatops/search",         action: "list"
        post "/_chatops/search/:chatop", action: "execute_chatop"
      end

      scope controller: "chatops/geyser" do
        get  "/_chatops/geyser",         action: "list"
        post "/_chatops/geyser/:chatop", action: "execute_chatop"
      end

      scope controller: "chatops/spokes" do
        get  "/_chatops/spokes",         action: "list"
        post "/_chatops/spokes/:chatop", action: "execute_chatop"
      end

      scope controller: "chatops/backup" do
        get  "/_chatops/backup",         action: "list"
        post "/_chatops/backup/:chatop", action: "execute_chatop"
      end

      scope controller: "chatops/resque" do
        get  "/_chatops/resque",         action: "list"
        post "/_chatops/resque/:chatop", action: "execute_chatop"
      end

      scope controller: "chatops/vulnerability" do
        get  "/_chatops/vulnerability",         action: "list"
        post "/_chatops/vulnerability/:chatop", action: "execute_chatop"
      end

      scope controller: "chatops/features" do
        get  "/_chatops/features",         action: "list"
        post "/_chatops/features/:chatop", action: "execute_chatop"
      end

      scope controller: "chatops/dependency_graph" do
        get  "/_chatops/dependency_graph",         action: "list"
        post "/_chatops/dependency_graph/:chatop", action: "execute_chatop"
      end

      scope controller: "chatops/secret_scanning" do
        get  "/_chatops/secret_scanning",         action: "list"
        post "/_chatops/secret_scanning/:chatop", action: "execute_chatop"
      end
    end
  end

  ##
  # Legacy (rofl)
  get "/~:path", to: "site#tilde", constraints: { path: /.*/ }

  ##
  # Bounces
  get "/:contributing",       to: redirect("/about/jobs"), contributing: /contributing(.md)?/i
  get "/launch",              to: redirect("/search")

  ##
  # Head
  get "/manifest.json", to: "head#manifest"

  ##
  # Gist aliases
  get "/gist", to: redirect(GitHub.gist_url)
  get "/gists", to: redirect(GitHub.gist_url)

  # Non-Enterprise Bounces
  #
  # Before adding/removing things from this (and other GitHub.enterprise?)
  # block(s) check ensure that they are also moved to the global_blacklist
  # in config/initializers/blacklist.rb
  unless GitHub.enterprise?
    get "/c",                    to: redirect("/contact")
    get "/press",                to: redirect("/about/press")
    get "/edu",                  to: redirect("https://education.github.com")
    get "/mac",                  to: redirect("https://mac.github.com")
    get "/windows",              to: redirect("https://windows.github.com")
    get "/shop",                 to: redirect("https://shop.github.com")
    get "/pages",                to: redirect("https://pages.github.com")
    get "/training/online",      to: redirect("https://training.github.com/classes/")
    get "/training/events",      to: redirect("https://training.github.com/schedule/")
    get "/training/free",        to: redirect("https://training.github.com/kit/")
    get "/training/:other",      to: redirect("https://training.github.com/")
    get "/training",             to: redirect("https://training.github.com/")
    get "/site/terms",           to: redirect("#{GitHub.help_url}/github/site-policy/github-terms-of-service"), as: :site_terms
    get "/terms",                to: redirect("#{GitHub.help_url}/github/site-policy/github-terms-of-service")
    get "/tos",                  to: redirect("#{GitHub.help_url}/github/site-policy/github-terms-of-service")
    get "/site/corporate-terms", to: redirect("#{GitHub.help_url}/articles/github-corporate-terms-of-service/"), as: :site_corp_terms
    get "/site/education-terms", to: redirect("https://education.github.com/schools/terms/"), as: :site_education_terms
    get "/site/esa",             to: redirect("#{GitHub.help_url}/en/articles/github-enterprise-subscription-agreement"), as: :site_esa
    get "/mirrors",              to: redirect("#{GitHub.help_url}/articles/about-github-mirrors")
    get "/codeql",               to: redirect("https://securitylab.github.com/tools/codeql")
    get "/forrester",            to: redirect("https://resources.github.com/forrester")
  end

  scope controller: "site", action: "bounce" do
    get "/repositories/new", as: nil, format: false, defaults: { to: "/new" }
  end

  # Retired Features
  get "/inbox", to: redirect("/410")
  get "/inbox/sent", to: redirect("/410")

  # Changed password reset URL
  get "/sessions/forgot_password", to: redirect("/password_reset")

  ##
  # GitHub Private Instance bootstrap process
  get   "/bootstrap-instance",                       to: "bootstrap_instance#index",                     as: :bootstrap_instance
  patch "/bootstrap-instance/enterprise-profile",    to: "bootstrap_instance#update_enterprise_profile", as: :bootstrap_instance_update_enterprise_profile
  get   "/bootstrap-instance/configuration(/:step)", to: "bootstrap_instance#configuration",             as: :bootstrap_instance_configuration
  patch "/bootstrap-instance/configuration/:step",   to: "bootstrap_instance#update_configuration",      as: :bootstrap_instance_update_configuration
  patch "/bootstrap-instance/commit",                to: "bootstrap_instance#commit",                    as: :bootstrap_instance_commit

  ##
  # Site
  unless GitHub.enterprise?
    get  "/buildingthefuture",                 to: "site#building_the_future"
    get  "/buildingthefuture/tiffani",         to: "site#building_the_future_tiffani"
    get  "/buildingthefuture/conrad",          to: "site#building_the_future_conrad"
    get  "/buildingthefuture/julius",          to: "site#building_the_future_julius"
    get  "/buildingthefuture/jamica",          to: "site#building_the_future_jamica"
    get  "/business",                          to: "site#business", as: :business_marketing
    get  "/business/customers",                to: redirect("/customer-stories?type=enterprise")
    get  "/business/customer",                 to: redirect("/customer-stories?type=enterprise")
    get  "/business/customers/:customer_story", to: redirect("/customer-stories/%{customer_story}")
    get  "/business/features",                 to: "site#business_features"
    get  "/business/partners",                 to: redirect("https://partner.github.com")
    get  "/business/security",                 to: "site#business_security"
    get  "/business/try",                      to: "site#business_try"
    get  "/business/why-github-for-work",      to: "site#business_why_github"
    get  "/case-studies",                      to: redirect("/customer-stories")
    get  "/case-studies/:customer_story",      to: redirect("/customer-stories/%{customer_story}")
    get  "/choose-team",                       to: "site#choose_team"
    get  "/cloud-trial",                       to: "site#enterprise_cloud_trial"
    get  "/customers",                         to: redirect("/customer-stories")
    get  "/customer",                          to: redirect("/customer-stories", status: 302)
    get  "/enterprise",                        to: "site#enterprise", as: :enterprise_marketing_page
    get  "/enterprise/security",               to: "site#enterprise_security"
    get  "/features/package-registry",         to: "site#feature_package_registry"
    get  "/features/codespaces",               to: "site#codespaces"
    get  "/git-guides/",                       to: "site#git_guides_index"
    get  "/git-guides/git-add",                to: "site#git_guides_add"
    get  "/git-guides/git-clone",              to: "site#git_guides_clone"
    get  "/git-guides/git-commit",             to: "site#git_guides_commit"
    get  "/git-guides/git-init",               to: "site#git_guides_init"
    get  "/git-guides/git-pull",               to: "site#git_guides_pull"
    get  "/git-guides/git-push",               to: "site#git_guides_push"
    get  "/git-guides/git-remote",             to: "site#git_guides_remote"
    get  "/git-guides/git-status",             to: "site#git_guides_status"
    get  "/git-guides/install-git",            to: "site#git_guides_install"
    get  "/learn",                             to: "site#coming_soon"
    get  "/learn/devops",                      to: "site#learn_devops"
    get  "/learn/security",                    to: "site#learn_security"
    get  "/learn/forrester",                   to: "site#forrester"
    get  "/learn/forrester",                   to: "site#forrester"
    get  "/logos",                             to: "site#logos"
    get  "/mobile",                            to: "site#mobile"
    get  "/open-source",                       to: "site#open_source"
    get  "/open-source/stories",               to: redirect("/customer-stories?type=open+source")
    get  "/open-source/stories/:username",     to: redirect("/customer-stories/%{username}")
    get  "/personal",                          to: "site#personal"
    get  "/pricing",                           to: "site#pricing", as: :pricing
    get  "/pricing/plans",                     to: redirect("/pricing")
    get  "/pricing/developer",                 to: redirect("/pricing", status: 302)
    get  "/pricing/team",                      to: redirect("/pricing", status: 302)
    get  "/pricing/business-hosted",           to: redirect("/pricing", status: 302)
    get  "/pricing/business-cloud",            to: redirect("/pricing", status: 302)
    get  "/pricing/business-enterprise",       to: redirect("/pricing", status: 302)
    get  "/pricing/enterprise",                to: redirect("/pricing", status: 302)
    get  "/relaunch-styles",                   to: "site#relaunch_styles"
    get  "/resources",                         to: redirect("https://resources.github.com")
    get  "/save-net-neutrality",               to: "site#netneutrality"
    get  "/security",                          to: "site#security"
    get  "/security/incident-response",        to: "site#security_incident"
    get  "/security/team",                     to: "site#security_team"
    get  "/security/trust",                    to: "site#security_trust"
    get  "/services",                          to: redirect("https://services.github.com")
    get  "/site/assets/:name.:format",         to: "site#asset_redirect"
    get  "/site/report-bug",                   to: "site#report_bug", as: :report_bug
    post "/site/toggle_labs",                  to: "site#toggle_labs", as: :toggle_labs
    post "/site/toggle_canary",                to: "site#toggle_canary", as: :toggle_canary
    get  "/team",                              to: "site#team", as: :team_marketing_page
    get  "/site/privacy",                      to: redirect("#{GitHub.help_url}/articles/github-privacy-policy/"), as: :site_privacy
    get  "/save-net-neutrality",               to: "site#netneutrality"
    get  "/template",                          to: "site#template_playground"
    get  "/ten",                               to: "site#tenyears"
    get  "/universe-2016",                     to: "site#universe_2016"
    get  "/site-map",                          to: "site#site_map"

    scope module: :site do
      resources :customer_stories, only: [:index, :show], path: "customer-stories"
      resources :readme, only: [:show, :index], path: "readme"
    end
  end

  scope module: :site do
    resources :events, only: [:index, :show]
  end

  if GitHub.security_dot_txt_enabled?
    get    "/.well-known/security.txt",                   to: "site#security_txt"
  end

  if !GitHub.enterprise?
    get "/.well-known/microsoft-identity-association.json", to: "site#microsoft_identity_association_json"
  end

  get    "/developer-stories",                          to: "site#developer_stories"
  get    "/developer-stories/alicia",                   to: "site#developer_stories_alicia"
  get    "/developer-stories/amy",                      to: "site#developer_stories_amy"
  get    "/developer-stories/lisa",                     to: "site#developer_stories_lisa"
  get    "/developer-stories/mario",                    to: "site#developer_stories_mario"
  get    "/premium-support",                            to: "site#premium_support"
  get    "/features",                                   to: "site#features"
  get    "/features/code-review",                       to: "site#feature_code_review"
  get    "/features/project-management",                to: "site#feature_project_management"
  get    "/features/integrations",                      to: "site#feature_integrations"
  get    "/features/insights",                          to: "site#feature_insights"
  get    "/features/actions",                           to: "site#feature_actions"
  unless GitHub.enterprise?
    get    "/features/packages",                          to: "site#feature_packages"
  end
  get    "/features/security",                          to: "site#feature_security"
  get    "/410",                                        to: "site#feature_gone"
  post   "/site/mobile_preference",                     to: "site#mobile_preference", as: :set_mobile_preference
  get    "/site/force502",                              to: "site#force502"
  get    "/site/keyboard_shortcuts",                    to: "site#keyboard_shortcuts"
  get    "/site/sha",                                   to: "site#sha"
  get    "/site/boomtown",                              to: "site#boomtown"
  get    "/site/contexttown",                           to: "site#contexttown"
  get    "/site/roletown",                              to: "site#roletown"
  get    "/site/componenttown",                         to: "site#componenttown"
  get    "/site/bounce",                                to: "site#bounce"
  post   "/site/custom_sleeptown",                      to: "site#custom_sleeptown"
  get    "/site/sleeptown",                             to: "site#sleeptown"
  post   "/site/preview",                               to: "comment_preview#show"
  get    "/site/sciencetown",                           to: "site#sciencetown"
  get    "/site/toggle_site_admin_and_employee_status", to: "site#toggle_site_admin_and_employee_status"
  post   "/site/toggle_site_admin_and_employee_status", to: "site#toggle_site_admin_and_employee_status", as: :toggle_site_admin
  post   "/site/toggle_site_admin_performance_stats",   to: "site#toggle_site_admin_performance_stats",   as: :toggle_site_admin_performance_stats

  get    "/site/konami",                                to: "site#konami"

  get    "/_render_node/:id/*path", to: "nodes#show", as: :show_node_partial, legacy: true
  get    "/_graphql/:operation_name", to: "queries#query", as: :graphql
  post   "/_graphql/:operation_name", to: "queries#query"

  # A place to send people who we need a verified email from.
  get  "/account/unverified-email", to: "site#unverified_email", as: :unverified_email

  # For more information on this, see:
  #   https://github.com/github/customer-feedback/issues/1224
  #
  # We need to route GitHub apps differently on Enterprise
  # so setup the prefix ahead of time.
  app_prefix = GitHub.enterprise? ? "github-apps" : "apps"

  get "/#{app_prefix}/github-actions",        to: redirect("/features/actions") # The GitHub Actions app page will never be revealed to the end-user.
  get "/#{app_prefix}/github/github-actions", to: redirect("/features/actions") # The GitHub Actions app page will never be revealed to the end-user.

  if GitHub.dependabot_enabled?
    get "/#{app_prefix}/dependabot",        to: redirect(GitHub.dependabot_help_url)
    get "/#{app_prefix}/github/dependabot", to: redirect(GitHub.dependabot_help_url)
  end

  if GitHub.pages_github_app_available?
    get "/#{app_prefix}/github-pages",         to: redirect(GitHub.pages_help_url)
    get "/#{app_prefix}/github/github-pages",  to: redirect(GitHub.pages_help_url)
  end

  unless GitHub.enterprise?
    get "/#{app_prefix}/github-code-scanning",         to: redirect("/features/security")
    get "/#{app_prefix}/github/github-code-scanning",  to: redirect("/features/security")
  end

  # IntegrationsListingsController
  scope "/#{app_prefix}" do
    get "/feature/:feature", to: "integration_listings#feature",    as: :"apps_feature"
    get "/:name/install",    to: "integration_listings#install",    as: :"app_install"
    get "/:name/learn_more", to: "integration_listings#learn_more", as: :"app_learn_more"
  end

  # IntegrationsController
  get "/#{app_prefix}/:id",                  to: "integrations#show", as: :alias_app
  get "/#{app_prefix}/:owner/:id",           to: "integrations#show", as: :user_app
  get "/#{app_prefix}/businesses/:slug/:id", to: "integrations#show", as: :business_app

  unless GitHub.enterprise?
    get "/#{app_prefix}", to: "integration_listings#index", as: :apps
  end

  app_route_prefixes = {
    "/#{app_prefix}"                  => "alias_app",
    "/#{app_prefix}/:owner"           => "user_app",
    "/#{app_prefix}/businesses/:slug" => "business_app",
  }

  # IntegrationInstallationsController
  app_route_prefixes.each do |prefix, helper|
    scope "/#{prefix}" do
      get   "/:integration_id/installations/new",             to: "integration_installations#new",                as: :"new_#{helper}_installation"
      match "/:integration_id/installations/new/permissions", to: "integration_installations#permissions",        as: :"#{helper}_installation_permissions", via: [:get, :post]
      post  "/:integration_id/installations",                 to: "integration_installations#create",             as: :"#{helper}_installations"
      get   "/:integration_id/installations/suggestions",     to: "integration_installations#suggestions",        as: :"#{helper}_installations_suggestions"
      get   "/:integration_id/installations/:id",             to: "integration_installations#edit",               as: :"edit_#{helper}_installation"
      put   "/:integration_id/installations/:id",             to: "integration_installations#update",             as: :"update_#{helper}_installation"
      get   "/:integration_id/installations/:id/permissions", to: "integration_installations#edit_permissions",   as: :"edit_#{helper}_installation_permissions"
      put   "/:integration_id/installations/:id/permissions", to: "integration_installations#update_permissions", as: :"update_#{helper}_installation_permissions"
    end
  end

  # IntegrationInstallationsController
  app_route_prefixes.each do |prefix, helper|
    scope "/#{prefix}" do
      delete "/:integration_id/requests/:request_id", to: "integration_installation_requests#destroy", as: :"cancel_#{helper}_integration_installation_request"
    end
  end

  # MarketplaceListingsController
  unless GitHub.enterprise?
    marketplace_search_constraint = -> (request) do
      request.query_parameters["query"].present? ||
        request.query_parameters["type"].present? ||
        request.query_parameters["category"].present? ||
        request.query_parameters["verification"].present?
    end

    constraints marketplace_search_constraint do
      get "/marketplace", to: "marketplace/searches#show", as: :marketplace_search
    end

    get    "/marketplace",                                      to: "marketplaces#show",                          as: :marketplace
    post   "/marketplace",                                      to: "marketplace_listings#create"
    post   "/marketplace/preview",                              to: "marketplace_listings#preview",               as: :marketplace_preview
    get    "/marketplace/free-trials",                          to: "marketplace_listings#free_trials",           as: :marketplace_free_trials
    get    "/marketplace/orders/pending",                       to: "marketplace/pending_orders#index",           as: :marketplace_pending_orders
    get    "/marketplace/installations/pending",                to: "marketplace/pending_installations#index",    as: :marketplace_pending_installations
    post   "/marketplace/pending_installations/dismiss",        to: "marketplace/pending_installations/dismissals#create", as: :dismiss_marketplace_pending_installations_notice
    delete "/marketplace/previews/:id",                         to: "marketplace_purchases#delete_pending",       as: :delete_pending_marketplace_order
    get    "/marketplace/category/:slug",                       to: "marketplace/categories#show",                as: :marketplace_category
    get    "/marketplace/new",                                  to: "marketplace_listings#new",                   as: :new_marketplace_listing
    get    "/marketplace/new/:type/:id",                        to: "marketplace_listings#new_with_integratable", as: :new_marketplace_listing_with_integratable
    get    "/marketplace/manage",                               to: "marketplace_listings#manage",                as: :manage_marketplace_listings
    get    "/marketplace/actions",                              to: redirect("/marketplace?type=actions", status: 302), as: :marketplace_actions
    get    "/marketplace/actions/usable_icons",                 to: "marketplace/actions/usable_icons#index",     as: :marketplace_actions_usable_icons
    post   "/marketplace/actions/agreement_signatures",         to: "marketplace/actions/agreement_signatures#create", as: :marketplace_actions_agreement_signatures
    get    "/marketplace/actions/:slug",                        to: "marketplace/actions#show",                   as: :marketplace_action
    delete "/marketplace/actions/:slug",                        to: "marketplace/actions#destroy",                as: :delete_marketplace_action
    get    "/marketplace/app_selector",                         to: "marketplace/app_selector#index",             as: :marketplace_app_selector
    get    "/marketplace/:listing_slug",                        to: "marketplace_listings#show",                  as: :marketplace_listing
    post   "/marketplace/:listing_slug/agreement_signatures",   to: "marketplace_agreement_signatures#create",    as: :marketplace_agreement_signatures
    get    "/marketplace/:listing_slug/plan/:plan_id",          to: "marketplace_listings#show",                  as: :marketplace_listing_preview_plan
    put    "/marketplace/:listing_slug",                        to: "marketplace_listings#update"
    get    "/marketplace/:listing_slug/edit",                   to: "marketplace_listings#edit",                  as: :edit_marketplace_listing
    get    "/marketplace/:listing_slug/edit/overview",          to: redirect("/marketplace/%{listing_slug}/edit") # /overview was moved to /edit
    get    "/marketplace/:listing_slug/edit/description",       to: "marketplace_listings#edit_description",      as: :edit_description_marketplace_listing
    get    "/marketplace/:listing_slug/edit/contact",           to: "marketplace_listings#edit_contact_info",     as: :edit_contact_info_marketplace_listing
    get    "/marketplace/:listing_slug/edit/featured_customers", to: "marketplace/listings/featured_customers#index", as: :edit_featured_customers_marketplace_listing
    put    "/marketplace/:listing_slug/edit/featured_customers", to: "marketplace/listings/featured_customers#update", as: :update_featured_customers_marketplace_listing
    delete "/marketplace/:listing_slug/edit/featured_customers/:id", to: "marketplace/listings/featured_customers#destroy", as: :destroy_featured_customers_marketplace_listing

    get    "/marketplace/:listing_slug/install/:subscription_item_id", to: "marketplace_listing_installations#new",      as: :install_marketplace_listing
    get    "/marketplace/:listing_slug/upgrade/:plan_number/:account_id", to: "marketplace_purchases#integrator_upgrade", as: :marketplace_plan_upgrade
    get    "/marketplace/:listing_slug/order/:plan_id",          to: "marketplace_purchases#new",                  as: :marketplace_order
    get    "/marketplace/:listing_slug/order_preview/:plan_id",  to: "marketplace_purchases#preview",              as: :marketplace_order_preview
    post   "/marketplace/:listing_slug/order/:plan_id",          to: "marketplace_purchases#create",               as: :marketplace_order_purchase
    post   "/marketplace/:listing_slug/order/:plan_id/upgrade",  to: "marketplace_purchases#update",               as: :marketplace_order_upgrade
    get    "/marketplace/:listing_slug/screenshots",             to: "marketplace_listings#screenshots",           as: :marketplace_listing_screenshots
    post   "/marketplace/:listing_slug/redraft",                 to: "marketplace_listings#redraft",               as: :redraft_marketplace_listing
    put    "/marketplace/:listing_slug/screenshot/:id",          to: "marketplace_listing_screenshots#update"
    delete "/marketplace/:listing_slug/screenshot/:id",          to: "marketplace_listing_screenshots#destroy",    as: :marketplace_listing_screenshot
    post   "/marketplace/:listing_slug/request_approval",        to: "marketplace_listings#request_approval",      as: :request_marketplace_listing_approval
    post   "/marketplace/:listing_slug/request_unverified_approval", to: "marketplace/listings/unverified_approval_requests#create", as: :unverified_listing_approval_request
    post   "/marketplace/:listing_slug/request_verified_approval", to: "marketplace/listings/verified_approval_requests#create", as: :verified_listing_approval_request
    get    "/marketplace/:listing_slug/edit/plans",              to: "marketplace_listing_plans#index",            as: :marketplace_listing_plans
    get    "/marketplace/:listing_slug/edit/plans/new",          to: "marketplace_listing_plans#new",              as: :new_marketplace_listing_plan
    get    "/marketplace/:listing_slug/edit/plans/:id",          to: "marketplace_listing_plans#show",             as: :marketplace_listing_plan
    post   "/marketplace/:listing_slug/edit/plans",              to: "marketplace_listing_plans#create"
    put    "/marketplace/:listing_slug/edit/plans/:id",          to: "marketplace_listing_plans#update"
    delete "/marketplace/:listing_slug/edit/plans/:id",          to: "marketplace_listing_plans#destroy"
    put "/marketplace/:listing_slug/edit/plans/:id/retire",            to: "marketplace_listing_plans#retire",            as: :retire_marketplace_listing_plan
    put "/marketplace/:listing_slug/edit/plans/:id/publish",           to: "marketplace_listing_plans#publish",           as: :publish_marketplace_listing_plan
    get    "/marketplace/:listing_slug/hook",                    to: "marketplace_listing_hooks#show",              as: :marketplace_listing_hook
    post   "/marketplace/:listing_slug/hook",                    to: "marketplace_listing_hooks#create"
    put    "/marketplace/:listing_slug/hook",                    to: "marketplace_listing_hooks#update"
    patch  "/marketplace/:listing_slug/hook",                    to: "marketplace_listing_hooks#update"

    constraints(guid: WEBHOOK_GUID_REGEX, id: WEBHOOK_REGEX, hook_id: /\d+/) do
      get  "/marketplace/:listing_slug/hook/:hook_id/deliveries",                     to: "hook_deliveries#index",     as: :marketplace_listing_hook_deliveries, context: "marketplace_listing"
      get  "/marketplace/:listing_slug/hook/:hook_id/deliveries/:id",                 to: "hook_deliveries#show",      as: :marketplace_listing_hook_delivery, context: "marketplace_listing"
      get  "/marketplace/:listing_slug/hook/:hook_id/deliveries/:id/payload.:format", to: "hook_deliveries#payload",   as: :marketplace_listing_delivery_payload, format: "json", context: "marketplace_listing"
      post "/marketplace/:listing_slug/hook/:hook_id/deliveries/:guid/redeliver",       to: "hook_deliveries#redeliver", as: :marketplace_listing_redeliver_hook_delivery, context: "marketplace_listing"
    end

    get    "/marketplace/:listing_slug/insights",                to: "marketplace_listing_insights#index",
      as: :marketplace_listing_insights
    get    "/marketplace/:listing_slug/insights/visitor_graph_data",    to: "marketplace_listing_insights#visitor_graph_data",
      as: :marketplace_listing_insights_visitor_graph_data
    get    "/marketplace/:listing_slug/insights/transactions",          to: "marketplace_listing_transactions#index",
      as: :marketplace_listing_insights_transactions
    get    "/marketplace/:listing_slug/insights/inactive_customers",    to: "marketplace/listings/insights/inactive_customers#index",
      as: :marketplace_listing_insights_inactive_customers

    get "/works-with",                                          to: "works_with_github_listings#index",
        as: :works_with_github
    get "/works-with/new",                                      to: "works_with_github_listings#new",
        as: :new_works_with_github_listing
    get "/works-with/category/:slug",                           to: "works_with_github_listings#index",
        as: :works_with_github_category
    get "/works-with/:listing_id/edit",                         to: "works_with_github_listings#edit",
        as: :edit_works_with_github_listing
  end

  if GitHub.billing_enabled?
    namespace :oauth do
      get "/stripe_connect", to: "stripe_connect#authenticate"
    end
  end

  ## GitHub Sponsors
  if GitHub.sponsors_enabled?
    resource :sponsors, only: [:show], format: false do
      resources :accounts,  only: :index, module: :sponsors
      resources :community, only: :index, module: :sponsors
      resources :featured,  only: :index, module: :sponsors
    end

    resources :sponsorables, module: :sponsors, path: :sponsors,
                             constraints: { id: USERID_REGEX },
                             only: [:show], format: false do
      resource :dashboard, only: [:show] do
        resource :fiscal_host, only: :show do
          resource :payouts, only: :show, module: :fiscal_host
          resource :transactions, only: :show, module: :fiscal_host
        end

        resource :profile, only: [:show, :update] do
          resource :meet_the_team, only: [:show, :edit], module: :profile
          resource :featured_work, only: [:show, :edit], module: :profile
        end

        resources :goals, only: [:index]
        resource :goal, only: [:new, :create, :edit, :update] do
          resource :retirement, only: [:create], module: :goals
        end

        # not named the more sensible `:tier_pricing_check` because some people
        # who write adblockers think that all URLs with `pricing` should be
        # blocked (which is very silly) and also a `check` means `money` in
        # en_US so that way lies sadness too.
        resource :tier_verify, only: :create
        resources :tiers, only: [:index, :new, :edit, :update, :create, :destroy],
                           constraints: { id: /.+/ } do
          resource :retirement,  only: :create, module: :tiers
        end

        resource :your_sponsors, only: :show do
          resource :export, only: :create, module: :your_sponsors
        end

        resources :updates, only: [:index, :new, :edit, :update, :create],
                             constraints: { id: /.+/ }
        resource :activity, only: :show
        resources :webhooks, only: [:index, :new, :create, :edit, :update, :destroy],
                             constraints: { id: /\d+/ } do
          # Routes to non-sponsors controller so cannot be made resourceful.
          resources :deliveries, only: [:index], constraints: { id: WEBHOOK_REGEX }, to: "/hook_deliveries#index", context: "sponsors_listing"
          resources :deliveries, only: [:show], constraints: { id: WEBHOOK_REGEX }, to: "/hook_deliveries#show", context: "sponsors_listing"
        end

        resource :payouts, only: :show do
          resource :balance, only: :show, module: :payouts
          resource :latest,  only: :show, module: :payouts
        end

        resource :settings,  only: :show
      end

      resource :sponsorships, only: [:create, :update, :show]
      resource :signup, only: [:show, :create]
      resource :request_approval, only: [:create, :destroy]
      resource :redraft, only: :create
      resource :stripe_account, only: [:new, :edit]
      resource :legal_name, only: :update
      resource :contact_email, only: :update
      resource :waitlist, only: [:show, :create, :update]
      resource :country_of_residence, only: :update
      resource :docusign_envelope, only: [:create]
      resource :sponsors_partial, only: :show
      resource :sponsorship_created_survey_response, only: :create
      resource :sponsorship_canceled_survey_response, only: :create
      resource :button, only: :show
      resource :card, only: :show
      resource :stripe_account_transition, only: [:show]
    end
  end

  # RepositoryInvitationsController
  get "/:user_id/:repository/invitations", to: "repository_invitations#show", as: :repository_invitation, repository: REPO_REGEX
  post "repository_invitations/:invitation_id/accept", to: "repository_invitations#accept", as: :repository_invitation_accept
  post "repository_invitations/:invitation_id/reject", to: "repository_invitations#reject", as: :repository_invitation_reject
  post "repository_invitations/:invitation_id/block_inviter", to: "repository_invitations#block_inviter", as: :block_repository_inviter
  put "repository_invitations/:invitation_id/set_permissions", to: "repository_invitations#set_permissions", as: :repository_invitation_permissions

  # RepositoryAccessRequestControler - Used for staff access requests when temporarily unlocking private repos
  get "/:user_id/:repository/access_request/:id", to: "repos/access_request#show", as: :repository_access_request, repository: REPO_REGEX
  post "/:user_id/:repository/access_request/:id/consent", to: "repos/access_request#consent", as: :repository_access_request_consent, repository: REPO_REGEX

  # SubscriptionItemsController
  delete "/subscription_items/:id", to: "subscription_items#destroy", as: :subscription_item

  # Gitignore templates
  get "/site/gitignore/:template", to: "gitignore#show", as: :gitignore_template, template: /[\w\.\+-]+/

  get "/_jobs/:id", to: "jobs#show", as: :job_status

  ##
  # Styleguide
  unless GitHub.enterprise?
    get "/styleguide/*path", to: "styleguide#index"
    get "/styleguide",       to: "styleguide#index"
  end

  ##
  # Developer program
  unless GitHub.enterprise?
    get "/developer", to: redirect(GitHub.developer_help_url)

    get    "/developer/register", to: "developer_program_membership#new", as: "register_developer_program"
    post   "/developer/register", to: "developer_program_membership#create"
    get    "/developer/thanks",   to: "developer_program_membership#show", as: "thanks_developer_program"

    put    "/developer/membership", to: "developer_program_membership#update", as: "developer_program_membership"
    delete "/developer/membership", to: "developer_program_membership#destroy"
  end

  ##
  # Identicons for GitHub and OAuth apps
  get "/identicons/app/:type/:id", to: "app_identicons#show", as: :app_identicon, defaults: { format: :svg }

  ##
  # Identicons for users
  get "/identicons/:id.png",          to: "identicons#show", as: :identicon,       constraints: { id: /[a-z0-9]{32}/ }
  get "/identicons/via_email",        to: "identicons#show", as: :email_identicon
  get "/identicons/:user_login.png",  to: "identicons#show", as: :user_identicon

  get "/orgs/improved-permissions", to: "orgs/marketing#index", as: :orgs_marketing

  # team sync url for Orgs
  get "orgs/team-sync/azure-callback", to: "orgs/team_sync#azure_callback",             as: :team_sync_azure_callback

  # Organization routes
  scope "/orgs/:org", constraints: { org: USERID_REGEX } do
    org = self

    org.get   "/",                      to: "orgs/repositories#index", as: :org_root
    org.get   "/hovercard",             to: "hovercards/organizations#show"

    org.get   "/search", to: "orgs/organizations#search", as: :org_search

    org.get   "/top_languages", to: "orgs/organizations#top_languages", as: :org_top_languages

    org.get   "/dashboard/pulls",       to: "issues#redirect_to_scoped_org_dashboard", as: :org_pulls_dashboard, pulls_only: true
    org.get   "/dashboard/issues",      to: "issues#redirect_to_scoped_org_dashboard", as: :org_issues_dashboard

    org.put   "/application_access",                         to: "orgs/organizations#application_access",  as: :org_application_access
    org.post  "/policies/applications/:application_id/request",   to: "orgs/oauth_application_approvals#request_approval", as: :org_request_oauth_app_approval
    org.get   "/policies/applications/:application_id",           to: "orgs/oauth_application_approvals#show", as: :org_application_approval
    org.put   "/policies/applications/:application_id/set_state", to: "orgs/oauth_application_approvals#set_state", as: :org_set_application_approval_state

    org.delete  "/removed_member_notifications",                           to: "orgs/removed_member_notifications#destroy", as: :org_destroy_removed_member_notifications

    org.get     "/people",                                                 to: "orgs/people#index", as: :org_people
    org.put     "/people/set_role",                                        to: "orgs/people#set_role", as: :org_set_role
    org.delete  "/people/destroy_members",                                 to: "orgs/people#destroy_members", as: :org_destroy_people
    org.delete  "/people/destroy_invitations",                             to: "orgs/people#destroy_invitations", as: :org_destroy_invitations
    org.post    "/people/retry_invitations",                               to: "orgs/people#retry_invitations", as: :org_retry_invitations
    org.post    "/people/add_member_for_new_org",                          to: "orgs/people#add_member_for_new_org", as: :add_member_for_new_org
    org.post    "/people/add_member_to_org",                               to: "orgs/people#add_member_to_org", as: :add_member_to_org
    org.delete  "/people/destroy_member_for_new_org",                      to: "orgs/people#destroy_member_for_new_org", as: :destroy_member_for_new_org
    org.get     "/people/destroy_members_dialog",                          to: "orgs/people#destroy_members_dialog", as: :org_destroy_people_dialog
    org.put     "/people/set_visibility",                                  to: "orgs/people#set_visibility", as: :org_set_people_visibility
    org.get     "/people/failed_invitations",                              to: "orgs/people#failed_invitations", as: :org_failed_invitations
    org.get     "/people/failed_invitation_toolbar_actions",               to: "orgs/people#failed_invitation_toolbar_actions", as: :org_failed_invitation_toolbar_actions
    org.get     "/people/pending_invitations",                             to: "orgs/people#pending_invitations", as: :org_pending_invitations
    org.get     "/people/pending_invitation_toolbar_actions",              to: "orgs/people#pending_invitation_toolbar_actions", as: :org_pending_invitation_toolbar_actions
    org.get     "/people/toolbar_actions",                                 to: "orgs/people#members_toolbar_actions", as: :org_members_toolbar_actions
    org.get     "/people/invitations_action_dialog",                       to: "orgs/people#invitations_action_dialog", as: :org_invitations_action_dialog
    org.post    "/people/dismiss_members_help",                            to: "orgs/people#dismiss_make_direct_members_help", as: :org_people_dismiss_members_help
    org.post    "/people/dismiss_membership_banner",                       to: "orgs/people#dismiss_org_membership_banner", as: :org_people_dismiss_membership_banner
    org.put     "/people/convert_to_outside_collaborators",                to: "orgs/people#convert_to_outside_collaborators", as: :org_people_convert_to_outside_collaborators

    org.put "/people/remove-outside-collaborators",
            to: "orgs/people#remove_outside_collaborators",
            as: :org_people_remove_outside_collaborators

    org.get     "/people/:person_login",                                   to: "orgs/people#show", as: :org_person
    org.get     "/people/:person_login/repositories/:user_id/:repository", to: "orgs/people#repository_permissions", as: :repository_permissions, repository: REPO_REGEX
    org.delete  "/people/:person_login/repositories/:user_id/:repository", to: "orgs/people#destroy_repository_permissions", repository: REPO_REGEX
    org.get     "/people/:person_login/sso",                               to: "orgs/people#sso", as: :org_person_sso
    org.delete  "/people/:person_login/sso_session/:session_id",           to: "orgs/people#revoke_sso_session", as: :org_person_revoke_sso_session
    org.delete  "/people/:person_login/sso_token/:token_id",               to: "orgs/people#revoke_sso_token", as: :org_person_revoke_sso_token
    org.delete  "/people/:person_login/external_identity",                 to: "orgs/people#unlink_identity", as: :org_person_unlink_identity
    org.put     "/people/:person_login/license",                           to: "orgs/user_licenses#update", as: :org_person_license
    org.get     "/outside-collaborators",   to: "orgs/people#outside_collaborators", as: :org_outside_collaborators
    org.get     "/outside_collaborators/toolbar_actions",                  to: "orgs/people#outside_collaborators_toolbar_actions", as: :org_outside_collaborators_toolbar_actions
    org.get     "/pending_members", to: "orgs/people#pending_members", as: :org_pending_members
    org.get     "/pending_collaborators", to: "orgs/people#pending_collaborators", as: :org_pending_collaborators
    org.get     "/pending_collaborator_invitations/toolbar_actions", to: "orgs/people#pending_collaborator_invitations_toolbar_actions", as: :org_pending_collaborator_invitations_toolbar_actions
    org.put     "/people/cancel_pending_collaborator_invitations", to: "orgs/people#cancel_pending_collaborator_invitations", as: :org_people_cancel_pending_collaborator_invitations
    org.get     "/tab_counts", to: "orgs/people#tab_counts", as: :orgs_people_tab_counts


    org.get     "/migration", to: "orgs/migration#index", as: :org_migrate_overview
    org.get     "/migration/customize_member_privileges", to: "orgs/migration#customize_member_privileges", as: :org_customize_member_privileges
    org.put     "/migration/update_member_privileges", to: "orgs/migration#update_member_privileges", as: :org_update_member_privileges
    org.get     "/migration/owners_team", to: "orgs/migration#owners_team", as: :org_migrate_owners_team

    org.get    "packages", to: "orgs/packages#index", as: :org_packages

    org.get    "memexes",                            to: "orgs/memexes#index", as: :org_memexes
    org.get    "memexes/new",                        to: "orgs/memexes#new", as: :new_org_memex
    org.get    "memexes/suggestions/repositories",   to: "orgs/memexes#suggested_repositories", as: :org_memex_suggested_repositories
    org.get    "memexes/suggestions/issues_and_pulls", to: "orgs/memexes#suggested_issues_and_pulls", as: :org_memex_suggested_issues_and_pulls
    org.get    "memexes/:memex_number",              to: "orgs/memexes#show", as: :show_org_memex
    org.put    "memexes/:memex_number",              to: "orgs/memexes#update", as: :update_org_memex
    org.post   "memexes",                            to: "orgs/memexes#create", as: :create_org_memex
    org.get    "memexes/:memex_number/items",        to: "orgs/memex_items#index", as: :org_memex_items
    org.post   "memexes/:memex_number/items",        to: "orgs/memex_items#create", as: :create_org_memex_item
    org.put    "memexes/:memex_number/items",        to: "orgs/memex_items#update", as: :update_org_memex_item
    org.delete "memexes/:memex_number/items",        to: "orgs/memex_items#destroy", as: :destroy_org_memex_items
    org.get    "memexes/:memex_number/items/suggestions/assignees", to: "orgs/memex_items#suggested_assignees", as: :org_memex_item_suggested_assignees
    org.get    "memexes/:memex_number/items/suggestions/labels", to: "orgs/memex_items#suggested_labels", as: :org_memex_item_suggested_labels
    org.get    "memexes/:memex_number/items/suggestions/milestones", to: "orgs/memex_items#suggested_milestones", as: :org_memex_item_suggested_milestones
    org.post   "memexes/:memex_number/columns",      to: "orgs/memex_project_columns#create", as: :create_org_memex_project_column
    org.put    "memexes/:memex_number/columns",      to: "orgs/memex_project_columns#update", as: :update_org_memex_project_column
    org.get    "memex_item_contents",                to: "orgs/memex_item_contents#suggested_memexes", as: :content_suggested_memexes
    org.put    "memex_item_contents",                to: "orgs/memex_item_contents#set_memexes", as: :set_memexes_on_content

    org.get    "projects",                           to: "orgs/projects#index", as: :org_projects
    org.get    "projects/new",                       to: "orgs/projects#new", as: :new_org_project
    org.get    "projects/new/linkable_repositories", to: "orgs/projects#linkable_repositories", as: :new_org_project_linkable_repositories
    org.get    "projects/:number/edit",              to: "orgs/projects#edit", as: :edit_org_project
    org.get    "projects/:number",                   to: "orgs/projects#show", as: :org_project
    org.delete "projects",                           to: "orgs/projects#destroy"
    org.post   "projects",                           to: "orgs/projects#create"
    org.put    "projects/:number",                   to: "orgs/projects#update"
    org.put    "projects/:number/state",             to: "orgs/projects#update_state", as: :update_org_project_state
    org.post   "projects/:number/clone",             to: "orgs/projects#clone", as: :org_project_clone

    org.get    "projects/:number/hovercard", to: "hovercards/organization_projects#show", as: :org_project_hovercard

    org.get    "projects/:number/search_results", to: "orgs/projects#search_results", as: :org_project_search_results
    org.get    "projects/:number/repository_results", to: "orgs/projects#repository_results", as: :org_project_repository_results
    org.get    "projects/:number/target_owner_results", to: "orgs/projects#target_owner_results", as: :org_project_target_owner_results
    org.get    "projects/:number/activity", to: "orgs/projects#activity", as: :org_project_activity
    org.get    "projects/:number/add_cards_link", to: "orgs/projects#add_cards_link", as: :org_project_add_cards_link
    org.get    "projects/:number/linkable_repositories", to: "orgs/projects#linkable_repositories"

    org.get    "projects/:project_number/settings", to: "project_settings#index", as: :org_project_settings
    org.put    "projects/:project_number/settings", to: "project_settings#update"
    org.get    "projects/:project_number/settings/admins", to: "project_settings#admins", as: :project_settings_admins
    org.put    "projects/:project_number/settings/org", to: "project_settings#update_org", as: :project_settings_update_org
    org.get    "projects/:project_number/settings/teams", to: "project_settings#teams", as: :project_settings_teams
    org.post   "projects/:project_number/settings/teams", to: "project_settings#add_team"
    org.get    "projects/:project_number/settings/teams/team_results", to: "project_settings#team_results"
    org.put    "projects/:project_number/settings/teams/:team_id", to: "project_settings#update_team", as: :project_settings_update_team
    org.delete "projects/:project_number/settings/teams/:team_id", to: "project_settings#remove_team", as: :project_settings_remove_team
    org.get    "projects/:project_number/settings/users", to: "project_settings#users", as: :org_project_settings_users
    org.post   "projects/:project_number/settings/users", to: "project_settings#add_user"
    org.put    "projects/:project_number/settings/users/:collaborator_id", to: "project_settings#update_user", as: :project_settings_update_user
    org.delete "projects/:project_number/settings/users/:collaborator_id", to: "project_settings#remove_user", as: :project_settings_remove_user
    org.get    "projects/:project_number/settings/linked_repositories", to: "project_settings#linked_repositories", as: :org_project_settings_linked_repositories
    org.post   "projects/:project_number/settings/linked_repositories", to: "project_settings#link_repository"
    org.delete "projects/:project_number/settings/linked_repositories", to: "project_settings#unlink_repository"

    org.get    "projects/:project_number/cards",                        to: "orgs/project_cards#index", as: :org_project_cards
    org.get    "projects/:project_number/cards/archived",               to: "orgs/project_cards#archived", as: :org_project_archived_cards
    org.get    "projects/:project_number/cards/archived/search",        to: "orgs/project_cards#search_archived", as: :org_project_search_archived_cards
    org.get    "projects/:project_number/cards/archived/:id",           to: "orgs/project_cards#check_archived", as: :org_project_check_archived_card
    org.delete "projects/:project_number/cards/:id",                    to: "orgs/project_cards#destroy", as: :org_project_card
    org.get    "projects/:project_number/cards/:id",                    to: "orgs/project_cards#show"
    org.put    "projects/:project_number/cards/:id/archive",            to: "orgs/project_cards#archive", as: :archive_org_project_card
    org.put    "projects/:project_number/cards/:id/unarchive",          to: "orgs/project_cards#unarchive", as: :unarchive_org_project_card
    org.get    "projects/:project_number/cards/:id/columns",            to: "orgs/project_cards#card_columns", as: :org_project_card_columns
    org.get    "projects/:project_number/cards/:id/closing_references", to: "orgs/project_cards#closing_references", as: :org_project_card_closing_references
    org.get    "projects/:project_number/cards/:id/closing_references/:reference_id", to: "orgs/project_cards#closing_reference", as: :org_project_card_closing_reference
    org.post   "projects/:project_number/columns",                      to: "orgs/project_columns#create", as: :org_project_columns
    org.put    "projects/:project_number/columns/:id",                  to: "orgs/project_columns#update"
    org.get    "projects/:project_number/columns/:id",                  to: "orgs/project_columns#show", as: :org_project_column
    org.delete "projects/:project_number/columns/:id",                  to: "orgs/project_columns#destroy"
    org.put    "projects/:project_number/columns/:id/archive",          to: "orgs/project_columns#archive", as: :org_project_column_archive
    org.put    "projects/:project_number/columns/:id/workflow",         to: "orgs/project_columns#update_workflow", as: :org_project_column_update_workflow
    org.get    "projects/:project_number/automation_options",           to: "orgs/project_columns#automation_options", as: :org_project_column_automation_options
    org.put    "projects/:project_number/reorder_columns",              to: "orgs/project_columns#reorder", as: :org_reorder_project_columns
    org.put    "projects/:project_number/columns/:column_id/cards",     to: "orgs/project_cards#update", as: :org_project_column_cards
    org.get    "projects/:project_number/columns/:column_id/cards",     to: "orgs/project_cards#index"
    org.post   "projects/:project_number/notes/:id/convert_to_issue",   to: "orgs/project_cards#convert_to_issue", as: :org_project_convert_note_to_issue
    org.get    "projects/:project_number/preview_note",                 to: "orgs/project_cards#preview_note", as: :org_project_preview_note
    org.get    "projects/:project_number/pull_request_status/:id",      to: "orgs/project_cards#pull_request_status", as: :org_card_pull_request_status
    org.put    "projects/:project_number/notes/:id",                    to: "orgs/project_cards#update_note", as: :org_project_update_note
    org.put    "projects/:project_number/notes/:id/task_list",          to: "orgs/project_cards#update_note_task_list", as: :org_project_update_note_task_list

    org.get    "policies",                    to: "orgs/policies#index", as: :org_policies
    org.get    "policies/:policy_natural_id", to: "orgs/policies#show", as: :org_policy
    org.post   "policies/:policy_natural_id/evaluations", to: "orgs/policies#evaluate", as: :org_policy_evaluations
    org.get    "policies/:policy_natural_id/outcomes/:resource_name", to: "orgs/policy_outcomes#show", as: :org_outcome, resource_name: REPO_REGEX

    org.get    "topics", to: "orgs/topics#index", as: :org_topics
    org.get    "topics/most_used", to: "orgs/topics#most_used", as: :org_most_used_topics

    org.get     "/teams",                 to: "orgs/teams#index", as: :teams
    org.get     "/teams/autocomplete",    to: "orgs/teams#autocomplete", as: :autocomplete_teams
    org.get     "/child_teams",           to: "orgs/teams#child_teams", as: :child_teams
    org.get     "/new-team",              to: "orgs/teams#new", as: :new_team
    org.get     "/teams/_minisearch",     to: "orgs/teams#minisearch", as: :team_minisearch
    org.post    "/teams/_check_name",     to: "orgs/teams#check_name", as: :check_team_name
    if GitHub.enterprise?
      org.get     "/teams/group_suggestions", to: "orgs/teams#ldap_group_suggestions", as: :ldap_group_suggestions
    else
      org.get     "/teams/group_suggestions", to: "orgs/team_sync/external_groups#external_group_suggestions", as: :external_group_suggestions
      org.get     "/teams/:team_slug/mappings", to: "orgs/team_sync/group_mappings#list_group_mappings", as: :list_group_mappings
    end
    org.put     "/teams/set_visibility",  to: "orgs/teams#set_visibility", as: :teams_set_visibility
    org.get     "/teams/important_changes_summary", to: "orgs/teams#important_changes_summary", as: :org_teams_important_changes_summary
    org.get     "/teams/toolbar_actions", to: "orgs/teams#teams_toolbar_actions", as: :org_teams_toolbar_actions

    org.get     "/owners_team", to: "orgs/teams#owners_team", as: :org_owners_team
    org.put     "/owners_team", to: "orgs/teams#rename_owners_team", as: :org_rename_owners_team
    org.delete  "/owners_team", to: "orgs/teams#destroy_owners_team", as: :org_destroy_owners_team

    org.get     "/teams/:team_slug",                           to: "orgs/team_discussions#index", as: :team
    org.get     "/teams/:team_slug/hovercard",                 to: "hovercards/teams#show"
    org.get     "/teams/:team_slug/teams",                     to: "orgs/teams#teams", as: :team_teams
    org.delete  "/teams/:team_slug/teams",                     to: "orgs/teams#destroy_team_teams", as: :destroy_team_teams
    org.get     "/teams/:team_slug/edit",                      to: "orgs/teams#edit", as: :edit_team
    org.get     "/teams/:team_slug/edit/review_assignment",    to: "orgs/teams#review_assignment", as: :edit_team_review_assignment
    org.put     "/teams/:team_slug/edit/review_assignment",    to: "orgs/teams#update_review_assignment", as: :update_team_review_assignment
    org.get     "/teams/:team_slug/members_toolbar_actions",   to: "orgs/teams#members_toolbar_actions", as: :org_team_members_toolbar_actions
    org.get     "/teams/:team_slug/toolbar_actions",           to: "orgs/teams#team_teams_toolbar_actions", as: :team_teams_toolbar_actions
    org.put     "/teams/:team_slug",                           to: "orgs/teams#update"
    org.delete  "/teams/:team_slug",                           to: "orgs/teams#destroy"
    org.delete  "/teams",                                      to: "orgs/teams#destroy_teams", as: :org_destroy_teams
    org.post    "/teams",                                      to: "orgs/teams#create", as: :create_team
    org.get     "/teams_goto",                                 to: "orgs/teams#goto",   as: :goto_team
    org.get     "/team_parent_search",                         to: "orgs/teams#parent_search", as: :team_parent_search
    org.post    "/teams/:team_slug/leave",                     to: "orgs/teams#leave",  as: :leave_team
    org.put     "/teams/:team_slug/migrate_legacy_admin_team", to: "orgs/teams#migrate_legacy_admin_team", as: :migrate_legacy_admin_team
    org.get     "/teams/:team_slug/breadcrumbs",               to: "orgs/teams#team_breadcrumbs", as: :org_team_breadcrumbs
    org.put     "/teams/:team_slug/move_child",                to: "orgs/teams#move_child_team", as: :org_team_move_child_team
    org.get     "/teams/:team_slug/child_search",              to: "orgs/teams#child_search", as: :org_team_child_search

    # Reactions for subjects owned by teams
    org.put     "/teams/:team_slug/reactions",                 to: "reactions#update", as: :update_team_reaction, context: "team"
    org.get     "/teams/:team_slug/reactions/list",            to: "reactions#list", context: "team"
    org.get     "/teams/:team_slug/reactions/group",           to: "reactions#group", context: "team"

    resources :reminders, except: [:edit], path: "/teams/:team_slug/settings/reminders", module: [:orgs, :teams], as: :team_reminders do
      collection do
        get :repository_suggestions
      end
      member do
        post :reminder_test
      end
    end

    org.get     "/announcements",                             to: "orgs/announcements#index", as: :org_announcements
    org.get     "/announcements/:number",                     to: "orgs/announcements#show", as: :org_announcement
    org.post    "/announcements",                             to: "orgs/announcements#create"
    org.delete  "/announcements/:number",                     to: "orgs/announcements#destroy"
    org.put     "/announcements/:number",                     to: "orgs/announcements#update"

    org.get     "/discussions",                       to: "orgs/discussions#index", as: :org_discussions
    org.get     "/discussions/author_filter_content", to: "orgs/discussions#author_filter_content",
      as: :org_discussions_author_filter_content

    org.get     "/teams/:team_slug/discussions",            to: "orgs/team_discussions#index", as: :team_discussions
    org.get     "/teams/:team_slug/discussions/:number",    to: "orgs/team_discussions#show", as: :team_discussion
    org.post    "/teams/:team_slug/discussions",            to: "orgs/team_discussions#create"
    org.put     "/teams/:team_slug/discussions/:number",    to: "orgs/team_discussions#update"
    org.delete  "/teams/:team_slug/discussions/:number",    to: "orgs/team_discussions#destroy"

    org.get     "/teams/:team_slug/discussions/:discussion_number/comments",         to: "orgs/team_discussion_comments#index"
    org.get     "/teams/:team_slug/discussions/:discussion_number/comments/:number", to: "orgs/team_discussion_comments#show", as: :team_discussion_comment
    org.post    "/teams/:team_slug/discussions/:discussion_number/comments",         to: "orgs/team_discussion_comments#create"
    org.put     "/teams/:team_slug/discussions/:discussion_number/comments/:number", to: "orgs/team_discussion_comments#update"
    org.delete  "/teams/:team_slug/discussions/:discussion_number/comments/:number", to: "orgs/team_discussion_comments#destroy"

    # Add backwards-compatible aliases for discussion comment routes.
    org.get     "/teams/:team_slug/discussions/:discussion_number/replies",         to: "orgs/team_discussion_comments#index"
    org.get     "/teams/:team_slug/discussions/:discussion_number/replies/:number", to: "orgs/team_discussion_comments#show"
    org.post    "/teams/:team_slug/discussions/:discussion_number/replies",         to: "orgs/team_discussion_comments#create"
    org.put     "/teams/:team_slug/discussions/:discussion_number/replies/:number", to: "orgs/team_discussion_comments#update"
    org.delete  "/teams/:team_slug/discussions/:discussion_number/replies/:number", to: "orgs/team_discussion_comments#destroy"

    org.post  "/teams/dismiss_teams_banner", to: "orgs/teams#dismiss_org_teams_banner", as: :org_teams_dismiss_teams_banner

    org.get   "/teams/:team_slug/repositories",                       to: "orgs/team_repositories#index", as: :team_repositories
    org.get   "/teams/:team_slug/repositories/suggestions",           to: "orgs/team_repositories#suggestions", as: :team_repository_suggestions
    org.post  "/teams/:team_slug/repositories",                       to: "orgs/team_repositories#create"
    org.put   "/teams/:team_slug/repositories/:repository_id",        to: "orgs/team_repositories#update", as: :team_repository
    org.get   "/teams/:team_slug/repositories_toolbar_actions",       to: "orgs/teams#repositories_toolbar_actions", as: :org_team_repositories_toolbar_actions
    org.post  "/teams/:team_slug/repositories/remove",                to: "orgs/team_repositories#bulk_remove", as: :team_repository_bulk_remove
    org.get   "/teams/:team_slug/repositories/accessible_to_members", to: "orgs/team_repositories#accessible_to_members", as: :org_team_repositories_accessible_to_members

    org.get "/teams/:team_slug/projects", to: "orgs/team_projects#index", as: :team_projects
    org.post "/teams/:team_slug/projects", to: "orgs/team_projects#create"
    org.get "/teams/:team_slug/projects/suggestions", to: "orgs/team_projects#suggestions", as: :team_project_suggestions
    org.put "/teams/:team_slug/projects/:project_id", to: "orgs/team_projects#update", as: :team_project
    org.delete "/teams/:team_slug/projects/:project_id", to: "orgs/team_projects#destroy"

    org.get     "/teams/:team_slug/members",                         to: "orgs/team_members#index", as: :team_members
    org.post    "/teams/:team_slug/members",                         to: "orgs/team_members#create"
    org.get     "/teams/:team_slug/members/suggestions",             to: "orgs/team_members#suggestions", as: :team_member_suggestions
    org.delete  "/teams/:team_slug/members/destroy",                 to: "orgs/team_members#destroy", as: :destroy_team_member
    org.put     "/teams/:team_slug/members/set_role",                to: "orgs/team_members#set_role", as: :team_set_role
    org.put     "/teams/:team_slug/members/set_maintainer",          to: "orgs/team_members#set_maintainer", as: :team_set_maintainer
    org.put     "/teams/:team_slug/members/migrate_to_collaborator", to: "orgs/team_members#migrate_to_collaborator", as: :team_members_migrate_to_collaborator

    org.post    "/teams/:team_slug/membership_requests",         to: "orgs/team_membership_requests#create", as: :team_membership_requests
    org.delete  "/teams/:team_slug/membership_requests/cancel",  to: "orgs/team_membership_requests#destroy", as: :destroy_team_membership_request
    org.post    "/teams/:team_slug/membership_requests/approve", to: "orgs/team_membership_requests#approve", as: :approve_team_membership_request
    org.post    "/teams/:team_slug/membership_requests/deny",    to: "orgs/team_membership_requests#deny", as: :deny_team_membership_request

    org.post    "/teams/change_parent_requests/:id/approve",     to: "orgs/team_change_parent_requests#approve", as: :approve_team_change_parent_request
    org.delete  "/teams/change_parent_requests/:id/cancel",      to: "orgs/team_change_parent_requests#cancel",  as: :cancel_team_change_parent_request

    org.get  "/audit-log/export", to: "orgs/audit_log_export#show", as: :org_audit_log_export
    org.post "/audit-log/export(.:format)", to: "orgs/audit_log_export#create"

    org.get  "/audit-log/export-git", to: "orgs/audit_log_git_event_export#show", as: :org_audit_log_git_event_export
    org.post "/audit-log/export-git(.:format)", to: "orgs/audit_log_git_event_export#create"

    org.get    "/invitations/via_email", to: "orgs/invitations#edit", as: :org_edit_email_invitation

    org.get "/invitations/reinstate_status",
            to: "orgs/invitations#reinstate_status",
            as: :org_reinstate_status

    org.get "/invitations/show_reinstated",
            to: "orgs/invitations#show_reinstated",
            as: :org_show_reinstated

    org.get "/invitations/reinstate_complete",
            to: "orgs/invitations#reinstate_complete",
            as: :org_reinstate_complete

    org.get "/invitations/:invitee_login/edit",
            to: "orgs/invitations#edit",
            as: :org_edit_invitation

    org.put    "/invitations/via_email", to: "orgs/invitations#update", as: :org_email_invitation
    org.put    "/invitations/:invitee_login", to: "orgs/invitations#update", as: :org_invitation
    org.get    "/invitations/invitee_suggestions", to: "orgs/invitations#invitee_suggestions", as: :org_invitations_invitee_suggestions
    org.post   "/invitations/member_adder_add", to: "orgs/invitations#member_adder_add", as: :org_invitations_member_adder_add
    org.post   "/invitations/create_for_new_org", to: "orgs/invitations#create_for_new_org", as: :org_invitations_create_for_new_org
    org.post   "/invitations/bulk_create_for_new_org", to: "orgs/invitations#bulk_create_for_new_org", as: :bulk_org_invitations_create_for_new_org
    org.get    "/invitations/licensing_details", to: "orgs/invitations_licensing_details#show", as: :org_invitations_licensing_details
    org.post   "/invitations", to: "orgs/invitations#create", as: :org_invitations
    org.get    "/invitation",  to: "orgs/invitations#show_pending", as: :org_show_invitation
    org.post   "/invitation",  to: "orgs/invitations#accept_pending", as: :org_accept_invitation
    org.delete "/invitations/:organization_invitation_id", to: "orgs/invitations#destroy", as: :destroy_org_invitation
    org.put    "/invitations/:organization_invitation_id/cancel", to: "orgs/invitations#destroy", as: :cancel_org_invitation
    org.get    "/opt-out", to: "orgs/invitations#opt_out_confirmation", as: :show_org_invitation_opt_out_confirmation
    org.post   "/opt-out", to: "orgs/invitations#opt_out", as: :org_invitation_opt_out

    org.resource :creation_survey, only: [:new, :create], module: :orgs, as: :org_creation_survey, path: "welcome_survey"

    org.get "/attribution-invitations", to: "orgs/attribution_invitations#index", as: :org_attribution_invitations
    org.post "/attribution-invitations", to: "orgs/attribution_invitations#create", as: :create_org_attribution_invitation
    org.get    "/attribution-invitations/target-suggestions", to: "orgs/attribution_invitations#target_suggestions", as: :org_attribution_invitations_target_suggestions

    # Organization user blocks
    org.get    "/user_blocks/suggestions", to: "orgs/user_blocks#suggestions", as: :org_user_blocks_suggestions
    org.post   "/user_blocks", to: "orgs/user_blocks#create", as: :org_user_block
    org.post   "/user_block_and_notify", to: "orgs/user_blocks#create_and_notify", as: :org_user_block_notify
    org.delete "/user_blocks", to: "orgs/user_blocks#destroy", as: :destroy_org_user_block

    # SAML SSO endpoints
    org.get  "/sso",           to: "orgs/identity_management#sso",                     as: :org_idm_sso
    org.get  "/sso_status",    to: "orgs/identity_management#sso_status",              as: :org_idm_sso_status
    org.get  "/sso_modal",     to: "orgs/identity_management#sso_modal",               as: :org_idm_sso_modal
    org.get  "/sso_complete",  to: "orgs/identity_management#sso_complete",            as: :org_idm_sso_complete
    org.get  "/sso/sign_up",   to: "orgs/identity_management#sso_sign_up",             as: :org_idm_sso_sign_up
    org.get  "/saml/metadata", to: "orgs/identity_management/saml#metadata",           as: :org_idm_saml_metadata
    org.post "/saml/initiate", to: "orgs/identity_management/saml#initiate",           as: :org_idm_saml_initiate
    org.post "/saml/consume",  to: "orgs/identity_management/saml#consume",            as: :org_idm_saml_consume
    org.get  "/saml/recover",  to: "orgs/identity_management/saml#recover_prompt",     as: :org_idm_saml_recover
    org.post "/saml/recover",  to: "orgs/identity_management/saml#recover"
    org.delete "/saml/revoke", to: "orgs/identity_management/saml#revoke",             as: :org_idm_saml_revoke

    # team synchronization endpoints
    org.post "/team-sync/install", to: "orgs/team_sync#install", as: :team_sync_install
    org.post "/team-sync/okta_install", to: "orgs/team_sync#okta_install", as: :team_sync_okta_install
    org.get  "/team-sync/setup",          to: "orgs/team_sync#setup",                      as: :team_sync_setup
    org.post "/team-sync/initiate", to: "orgs/team_sync#initiate",             as: :team_sync_initiate
    org.get  "/team-sync/review",         to: "orgs/team_sync#review",  as: :team_sync_review
    org.post "/team-sync/approve", to: "orgs/team_sync#approve", as: :team_sync_approve
    org.delete "/team-sync/cancel", to: "orgs/team_sync#cancel", as: :team_sync_cancel
    org.delete "/team-sync/disable", to: "orgs/team_sync#disable", as: :team_sync_disable

    org.namespace :orgs do
      namespace :team_sync do
        resource :okta_credentials, only: [:new, :create, :edit, :update]
      end
    end

    org.delete "/dismiss_notice", to: "organizations#dismiss_notice", as: :dismiss_org_notice

    org.resources :insights, only: [:index, :show], param: :period, module: :orgs, as: :org_insights do
      collection do
        if GitHub.dependency_graph_enabled?
          get :dependencies, action: :package_dependencies_index, as: :packages_dashboard
          get "dependencies/graphs/security", action: :package_dependencies_security_graph, as: :packages_dashboard_security_graph
          get "dependencies/graphs/licenses", action: :package_dependencies_licenses_graph, as: :packages_dashboard_licenses_graph
          get "dependencies/license_menu_content", action: :package_dependencies_license_menu_content, as: :packages_dashboard_license_menu_content
        end
        get :repositories, action: :get_org_repos, as: :get_repos
        put :update_repositories, action: :update_org_repos, as: :update_repos
      end
    end

    # Organization domain verification
    org.resources :domain, only: [:new, :create, :destroy], controller: "orgs/domains", as: :org_domains do
      member do
        get :verification_steps
        put :regenerate_token
        put :verify
      end
    end

    org.resource :licensing_headroom, only: :show, controller: "orgs/licensing_headroom", as: :org_licensing_headroom
  end

  post "/users/password", to: "users#password_check", as: :password_check

  # User routes
  scope "/users/:user_id", constraints: { user_id: USERID_REGEX } do

    ##
    # User Hovercards
    get "hovercard", to: "hovercards/users#show"
    # Arctic code vault badge
    get "/acv/hovercard", to: "hovercards/acv_badges#show"

    get "projects", to: "users/projects#index", as: :user_projects
    post "projects", to: "users/projects#create"
    get "projects/new/linkable_repositories", to: "users/projects#linkable_repositories", as: :new_user_project_linkable_repositories
    get "projects/:number", to: "users/projects#show", as: :user_project
    put "projects/:number", to: "users/projects#update"
    delete "projects", to: "users/projects#destroy"
    put "projects/:number/state", to: "users/projects#update_state", as: :update_user_project_state
    post "projects/:number/clone", to: "users/projects#clone", as: :user_project_clone
    get "projects/:number/edit", to: "users/projects#edit", as: :edit_user_project
    get "projects/:number/search_results", to: "users/projects#search_results", as: :user_project_search_results
    get "projects/:number/linkable_repositories", to: "users/projects#linkable_repositories"
    get "projects/:number/repository_results", to: "users/projects#repository_results", as: :user_project_repository_results
    get "projects/:number/target_owner_results", to: "users/projects#target_owner_results", as: :user_project_target_owner_results
    get "projects/:number/add_cards_link", to: "users/projects#add_cards_link", as: :user_project_add_cards_link
    get "projects/:number/activity", to: "users/projects#activity", as: :user_project_activity

    get "projects/:number/hovercard", to: "hovercards/user_projects#show", as: :user_project_hovercard

    post "projects/:project_number/columns", to: "users/project_columns#create", as: :user_project_columns
    put "projects/:project_number/reorder_columns", to: "users/project_columns#reorder", as: :user_reorder_project_columns
    get "projects/:project_number/columns/:id", to: "users/project_columns#show", as: :user_project_column
    put "projects/:project_number/columns/:id", to: "users/project_columns#update"
    delete "projects/:project_number/columns/:id", to: "users/project_columns#destroy"
    put "projects/:project_number/columns/:id/workflow", to: "users/project_columns#update_workflow", as: :user_project_column_update_workflow
    put "projects/:project_number/columns/:id/archive", to: "users/project_columns#archive", as: :user_project_column_archive

    get "projects/:project_number/cards", to: "users/project_cards#index", as: :user_project_cards
    get "projects/:project_number/cards/archived", to: "users/project_cards#archived", as: :user_project_archived_cards
    get "projects/:project_number/cards/archived/search", to: "users/project_cards#search_archived", as: :user_project_search_archived_cards
    get "projects/:project_number/cards/archived/:id", to: "users/project_cards#check_archived", as: :user_project_check_archived_card
    get "projects/:project_number/cards/:id", to: "users/project_cards#show", as: :user_project_card
    delete "projects/:project_number/cards/:id", to: "users/project_cards#destroy"
    put "projects/:project_number/cards/:id/archive", to: "users/project_cards#archive", as: :archive_user_project_card
    put "projects/:project_number/cards/:id/unarchive", to: "users/project_cards#unarchive", as: :unarchive_user_project_card
    get "projects/:project_number/cards/:id/columns", to: "users/project_cards#card_columns", as: :user_project_card_columns
    get "projects/:project_number/cards/:id/closing_references", to: "users/project_cards#closing_references", as: :user_project_card_closing_references
    get "projects/:project_number/cards/:id/closing_references/:reference_id", to: "users/project_cards#closing_reference", as: :user_project_card_closing_reference
    post "projects/:project_number/notes/:id/convert_to_issue", to: "users/project_cards#convert_to_issue", as: :user_project_convert_note_to_issue
    get "projects/:project_number/preview_note", to: "users/project_cards#preview_note", as: :user_project_preview_note
    get "projects/:project_number/pull_request_status/:id", to: "users/project_cards#pull_request_status", as: :user_card_pull_request_status
    put "projects/:project_number/notes/:id", to: "users/project_cards#update_note", as: :user_project_update_note
    put "projects/:project_number/notes/:id/task_list", to: "users/project_cards#update_note_task_list", as: :user_project_update_note_task_list
    get "projects/:project_number/columns/:column_id/cards", to: "users/project_cards#index", as: :user_project_column_cards
    put "projects/:project_number/columns/:column_id/cards", to: "users/project_cards#update"

    get "projects/:project_number/automation_options", to: "users/project_columns#automation_options", as: :user_project_column_automation_options

    get    "projects/:project_number/settings", to: "project_settings#index", as: :user_project_settings
    put    "projects/:project_number/settings", to: "project_settings#update"
    get    "projects/:project_number/settings/admins", to: "project_settings#admins"
    get    "projects/:project_number/settings/users", to: "project_settings#users", as: :user_project_settings_users
    post   "projects/:project_number/settings/users", to: "project_settings#add_user"
    put    "projects/:project_number/settings/users/:collaborator_id", to: "project_settings#update_user"
    delete "projects/:project_number/settings/users/:collaborator_id", to: "project_settings#remove_user"
    get    "projects/:project_number/settings/linked_repositories", to: "project_settings#linked_repositories", as: :user_project_settings_linked_repositories
    post   "projects/:project_number/settings/linked_repositories", to: "project_settings#link_repository"
    delete "projects/:project_number/settings/linked_repositories", to: "project_settings#unlink_repository"

    # Feature preview
    get    "feature_previews",                     to: "feature_preview#index",    as: :feature_previews
    get    "feature_preview/indicator_check",      to: "feature_preview#indicator_check"
    patch  "feature_preview/:feature_id/enroll",   to: "feature_preview#enroll",   as: :feature_previews_enroll
    patch  "feature_preview/:feature_id/unenroll", to: "feature_preview#unenroll", as: :feature_previews_unenroll

    ## GitHub Sponsors legacy redirects for users
    if GitHub.sponsors_enabled?
      get "sponsors/signup", to: redirect("/sponsors/%{user_id}/signup")
      get "sponsors",        to: redirect("/sponsors/%{user_id}/dashboard")
      get "sponsorship",     to: redirect("/sponsors/%{user_id}")
    end

    post "config-repo", to: "users/configuration_repositories#create", as: :create_user_config_repo
    post "config-repo/profile-readme-opt-in", to: "users/configuration_repositories#post_profile_readme_opt_in", as: :post_profile_readme_opt_in
  end

  # Redirect the legacy enterprise account profile path
  get "/businesses/:slug", to: redirect("/enterprises/%{slug}")

  resources :enterprises, controller: "businesses", param: :slug, only: [:show] do
    member do
      get :people
      get :org_filter_menu_content
      get :pending_members
      get :outside_collaborators
      get "outside_collaborator/:login", to: "businesses#outside_collaborator", as: :outside_collaborator
      get :pending_collaborators
      get :pending_organizations
      get :unowned_organizations
      post "unowned_organizations/:login/claim", to: "businesses#claim_unowned_organization", as: :claim_unowned_organization

      get :admins, to: "businesses/admins#index"
      get :pending_admins, to: "businesses/admins#pending"
      get :admin_suggestions, to: "businesses/admins#suggestions"
      put :set_role, to: "businesses/admins#set_role"

      get "invitations/:login", to: "businesses/invitations#member", as: :member_invitations
      get "invitations/:login/admin", to: "businesses/invitations#admin", as: :admin_invitations
      get "invitations/:login/outside_collaborator", to: "businesses/invitations#outside_collaborator", as: :collaborator_invitations

      # When admins are directly added to the business
      post :admins, to: "businesses/admins#create"
      # When admins are invited to the business
      post :invite_admin, to: "businesses/member_invitations#invite_admin"
      get :admin_invitation, to: "businesses/member_invitations#show_pending_admin_invitation"
      post :admin_invitation, to: "businesses/member_invitations#accept_pending_admin_invitation"
      put "admin_invitations/:invitation_id/cancel", to: "businesses/member_invitations#cancel_admin_invitation", as: :cancel_admin_invitation

      get "billing_manager_invitation", to: "businesses/member_invitations#show_pending_billing_manager_invitation"
      post "billing_manager_invitation", to: "businesses/member_invitations#accept_pending_billing_manager_invitation"

      delete :admins, to: "businesses/admins#destroy"

      get "data", to: "businesses/data_packs#index", as: :data_packs
      put "data", to: "businesses/data_packs#update"

      resources :enterprise_organization_invitations, path: "organization_invitations",
        controller: "businesses/organization_invitations", param: :organization_login, only: [:new, :create] do
        get "suggestions", on: :collection
        member do
          get "pending", action: "show_pending"
          post "pending/accept", action: "accept_pending", as: "accept_pending"
          post "pending/cancel", action: "cancel_pending", as: "cancel_pending"
          post "pending/confirm", action: "confirm_pending", as: "confirm_pending"
          post "pending/resend", action: "resend_pending", as: "resend_pending"
        end
      end

      delete :organizations, to: "businesses#remove_organization"

      delete "removed_member_notifications", to: "businesses/removed_member_notifications#destroy", as: :destroy_removed_member_notifications

      resources :insights, as: :enterprise_insights, controller: "businesses/insights", only: [:index] do
        collection do
          get :settings
          post :base_url, action: :update_base_url
        end
      end

      scope :settings do
        get "profile", to: "businesses/profiles#show", as: :settings_profile
        put "profile", to: "businesses/profiles#update"
        get "member_privileges", to: "businesses/member_privileges#index", as: :settings_member_privileges
        put "update_members_can_change_repo_visibility", to: "businesses/member_privileges#update_members_can_change_repo_visibility", as: :update_members_can_change_repo_visibility
        put "update_default_branch_setting", to: "businesses/member_privileges#update_default_branch_setting", as: :update_default_branch_setting
        put "update_allow_private_repository_forking", to: "businesses/member_privileges#update_allow_private_repository_forking", as: :update_allow_private_repository_forking
        put "members_can_create_repositories", to: "businesses/member_privileges#update_members_can_create_repositories", as: :settings_members_can_create_repositories
        put "members_can_delete_repositories", to: "businesses/member_privileges#update_members_can_delete_repositories", as: :settings_members_can_delete_repositories
        put "members_can_delete_issues", to: "businesses/member_privileges#update_members_can_delete_issues", as: :settings_members_can_delete_issues
        put "members_can_update_protected_branches", to: "businesses/member_privileges#update_members_can_update_protected_branches", as: :settings_members_can_update_protected_branches
        put "members_can_invite_outside_collaborators", to: "businesses/member_privileges#update_members_can_invite_outside_collaborators", as: :settings_members_can_invite_outside_collaborators
        put "default_repository_permission", to: "businesses/member_privileges#update_default_repository_permission", as: :update_default_repository_permission
        put "action_execution_capability", to: "businesses/member_privileges#update_action_execution_capability", as: :update_action_execution_capability

        get "teams", to: "businesses/teams#index", as: :settings_teams
        put "update_team_discussions_allowed", to: "businesses/teams#update_team_discussions_allowed", as: :update_team_discussions_allowed

        get "organizations", to: "businesses/organizations#settings", as: :settings_organizations
        put "members_can_view_dependency_insights", to: "businesses/organizations#update_members_can_view_dependency_insights", as: :update_members_can_view_dependency_insights

        post "metered_exports",     to: "businesses/metered_exports#create",      as: "metered_exports"
        get  "metered_exports/:id", to: "businesses/metered_exports#show",        as: "metered_export"
        get "billing", to: "businesses/billing_settings#show", as: :settings_billing
        get "actions_usage", to: "businesses/billing_settings/actions_usage#show", as: :settings_billing_actions_usage
        get "packages_usage", to: "businesses/billing_settings/packages_usage#show", as: :settings_billing_packages_usage
        get "shared_storage", to: "businesses/billing_settings/shared_storage#show", as: :settings_billing_shared_storage
        get "lfs_usage", to: "businesses/billing_settings/lfs_usage#show", as: :settings_billing_lfs_usage
        get "billing/:tab", to: "businesses/billing_settings#show", as: :settings_billing_tab, tab: /cost_management|past_invoices/
        post "billing/cost_management", to: "businesses/metered_billing_costs#update", as: :metered_billing_costs
        put "billing/members_can_make_purchases", to: "businesses/billing_settings#update_members_can_make_purchases", as: :billing_settings_members_can_make_purchases

        get "billing/invoices/:invoice_number", to: "invoices#show", as: :show_invoice
        get "billing/invoices/:invoice_number/pay", to: "invoices#pay", as: :pay_invoice
        get "billing/invoices/:invoice_number/payment_method", to: "invoices#payment_method", as: :invoice_payment_method
        get "billing/invoices/:invoice_number/signature", to: "invoices#payment_page_signature", as: :invoice_signature

        if GitHub.licensed_mode?
          get "license", to: "businesses/licenses#index", as: :settings_license
          get "license/download", to: "businesses/licenses#download", as: :settings_license_download
        end

        get "audit-log", to: "businesses/audit_log#index", as: :settings_audit_log
        get "audit-log/suggestions", to: "businesses/audit_log#suggestions", as: :settings_audit_log_suggestions
        get "audit-log/export", to: "businesses/audit_log#export", as: :settings_audit_log_export
        post "audit-log/export(.:format)", to: "businesses/audit_log#create_export"
        get  "audit-log/export-git", to: "businesses/audit_log#git_export", as: :settings_audit_log_git_event_export
        post "audit-log/export-git(.:format)", to: "businesses/audit_log#create_git_export"

        get "hooks", to: "businesses/hooks#index", as: :hooks
        post "hooks", to: "businesses/hooks#create"
        get "hooks/new", to: "businesses/hooks#new", as: :new_hook
        get "hooks/:id", to: "businesses/hooks#show", as: :hook
        put "hooks/:id", to: "businesses/hooks#update"
        delete "hooks/:id", to: "businesses/hooks#destroy"

        constraints(guid: WEBHOOK_GUID_REGEX, id: WEBHOOK_REGEX, hook_id: /\d+/) do
          get  "hooks/:hook_id/deliveries",                     to: "hook_deliveries#index",     as: :hook_deliveries, context: "enterprise"
          get  "hooks/:hook_id/deliveries/:id",                 to: "hook_deliveries#show",      as: :hook_delivery, context: "enterprise"
          get  "hooks/:hook_id/deliveries/:id/payload.:format", to: "hook_deliveries#payload",   as: :delivery_payload, format: "json", context: "enterprise"
          post "hooks/:hook_id/deliveries/:guid/redeliver",       to: "hook_deliveries#redeliver", as: :redeliver_hook_delivery, context: "enterprise"
        end

        get "pages", to: "businesses/pages#index", as: :settings_pages
        patch "pages", to: "businesses/pages#update_pages_settings"

        get "projects", to: "businesses/projects#index", as: :settings_projects
        put "update_organization_projects_allowed", to: "businesses/projects#update_organization_projects_allowed", as: :update_settings_organization_projects
        put "update_repository_projects_allowed", to: "businesses/projects#update_repository_projects_allowed", as: :update_settings_repository_projects

        get "security", to: "businesses/security#index", as: :settings_security
        put "update_two_factor_required", to: "businesses/security#update_two_factor_required", as: :update_settings_two_factor_required
        get "two_factor_required_status", to: "businesses/security#two_factor_required_status", as: :check_setting_two_factor_required
        post "ssh_cert_requirement", to: "businesses/security#ssh_cert_requirement", as: :update_settings_ssh_cert_requirement
        put "update_ip_whitelisting_enabled", to: "businesses/security#update_ip_whitelisting_enabled", as: :update_ip_whitelisting_enabled
        put "update_ip_whitelisting_app_access_enabled", to: "businesses/security#update_ip_whitelisting_app_access_enabled", as: :update_ip_whitelisting_app_access_enabled
        put "saml_provider", to: "businesses/saml_provider#update", as: :settings_saml_provider
        delete "saml_provider", to: "businesses/saml_provider#delete"
        get "saml_provider/recovery_codes", to: "businesses/saml_provider#recovery_codes", as: :settings_saml_provider_recovery_codes
        post "saml_provider/recovery_codes/download", to: "businesses/saml_provider#download_recovery_codes", as: :settings_saml_provider_download_recovery_codes
        get "saml_provider/recovery_codes/print", to: "businesses/saml_provider#print_recovery_codes", as: :settings_saml_provider_print_recovery_codes
        put "saml_provider/regenerate_recovery_codes", to: "businesses/saml_provider#regenerate_recovery_codes", as: :settings_saml_provider_regenerate_recovery_codes
        put "saml_provider/update_user_provisioning", to: "businesses/saml_provider#update_user_provisioning", as: :settings_saml_provider_update_user_provisioning

        get    "ssh_certificate_authorities/new", to: "ssh_certificate_authorities#new",     as: :ssh_certificate_authorities_new
        post   "ssh_certificate_authorities",     to: "ssh_certificate_authorities#create",  as: :ssh_certificate_authorities
        delete "ssh_certificate_authorities/:id", to: "ssh_certificate_authorities#destroy", as: :ssh_certificate_authority

        get "support", to: "businesses/support#index", as: :settings_support
        get "support/entitlee_suggestions", to: "businesses/support#entitlee_suggestions", as: :settings_support_entitlee_suggestions
        post "support/entitlee", to: "businesses/support#create_entitlee", as: :settings_support_entitlee_create
        delete "support/entitlee/:id", to: "businesses/support#delete_entitlee", as: :settings_support_entitlee_delete
        patch "support/support_link", to: "businesses/support#update_support_link", as: :settings_update_support_link

        get "actions",                         to: "businesses/actions_settings#show",                       as: :settings_actions
        put "actions/enable_org",              to: "businesses/actions/policies#enable_org",                 as: :settings_actions_org_enablement
        get "actions/policies/bulk_orgs",      to: "businesses/actions/policies#bulk",                       as: :settings_actions_bulk_orgs
        put "actions/policies/bulk_orgs",      to: "businesses/actions/policies#bulk_update",                as: :update_settings_actions_bulk_orgs
        put "actions/policies",                to: "businesses/actions/policies#update",                     as: :settings_actions_policies
        put "actions/policies/fork_pr_workflows_policy", to: "businesses/actions/policies#update_fork_pr_workflows_policy", as: :settings_actions_fork_pr_workflows_policy

        get    "actions/self-hosted-runners",                         to: "businesses/actions/self_hosted_runners#index",               as: :settings_actions_self_hosted_runners
        get    "actions/self-hosted-runners/new",                     to: "businesses/actions/self_hosted_runners#new",                 as: :settings_actions_add_runner
        get    "actions/self-hosted-runners/delete-runner-modal/:id", to: "businesses/actions/self_hosted_runners#delete_runner_modal", as: :settings_actions_delete_runner_modal
        delete "actions/self-hosted-runners/:id",                     to: "businesses/actions/self_hosted_runners#destroy",             as: :settings_actions_delete_runner

        get   "actions/labels",                 to: "businesses/actions/runner_labels#index",                 as: :settings_actions_runner_labels
        post  "actions/label",                  to: "businesses/actions/runner_labels#create",                as: :settings_actions_create_runner_label
        put    "actions/labels",                to: "businesses/actions/runner_labels#update",                as: :settings_actions_update_runner_labels
        post   "actions/runner-groups",         to: "businesses/actions/runner_groups#create",                as: :settings_actions_create_runner_group
        get    "actions/runner-groups/targets", to: "businesses/actions/runner_groups#show_selected_targets", as: :settings_actions_runner_group_targets
        get    "actions/runner-groups/bulk",    to: "businesses/actions/runner_groups#bulk_actions",          as: :settings_actions_runner_group_bulk_actions
        put    "actions/runner-groups/runners", to: "businesses/actions/runner_groups#update_runners",        as: :settings_actions_update_runner_group_runners
        get    "actions/runner-groups/menu",    to: "businesses/actions/runner_groups#show_menu",             as: :settings_actions_runner_groups_menu
        put    "actions/runner-groups/:id",     to: "businesses/actions/runner_groups#update",                as: :settings_actions_update_runner_group
        delete "actions/runner-groups/:id",     to: "businesses/actions/runner_groups#destroy",               as: :settings_actions_delete_runner_group

        resources :ip_whitelist_entries,
          as: :enterprise_ip_whitelist_entries,
          only: %w(new create edit update destroy),
          controller: "ip_whitelist_entries"

        get     "installations",                           to: "businesses/installations#index",                      as: :settings_business_installations
        get     "installations/:id",                       to: "businesses/installations#show",                       as: :settings_business_installation
        put     "installations/:id/update",                to: "businesses/installations#update",                     as: :update_settings_business_installation
        delete  "installations/:id",                       to: "businesses/installations#destroy"
        get     "installations/:id/permissions/update",    to: "businesses/installations#permissions_update_request", as: :permissions_update_request_settings_business_installation
        put     "installations/:id/permissions/update",    to: "businesses/installations#update_permissions",         as: :update_permissions_settings_business_installation

        if GitHub.enterprise?
          get "packages", to: "businesses/packages#show", as: :settings_packages
          put "packages", to: "businesses/packages#update_packages_settings", as: :update_settings_packages
        end

        if GitHub.single_business_environment?
          scope :admin_center_options do
            get "", to: "businesses/admin_center_options#index", as: :admin_center_options
            post "change_allow_force_push", to: "businesses/admin_center_options#change_allow_force_push", as: :settings_change_allow_force_push
            post "change_ssh_access", to: "businesses/admin_center_options#change_ssh_access", as: :settings_change_ssh_access
            post "change_suggested_protocol", to: "businesses/admin_center_options#change_suggested_protocol", as: :settings_change_suggested_protocol
            post "change_git_lfs_access", to: "businesses/admin_center_options#change_git_lfs_access", as: :settings_change_git_lfs_access
            post "change_showcase_access", to: "businesses/admin_center_options#change_showcase_access", as: :settings_change_showcase_access
            post "change_org_creation", to: "businesses/admin_center_options#change_org_creation", as: :settings_change_org_creation
            post "change_org_membership_visibility", to: "businesses/admin_center_options#change_org_membership_visibility", as: :settings_change_org_membership_visibility
            post "change_reactivate_suspended", to: "businesses/admin_center_options#change_reactivate_suspended", as: :settings_change_reactivate_suspended
            post "change_reactivate_suspended_on_sync", to: "businesses/admin_center_options#change_reactivate_suspended_on_sync", as: :settings_change_reactivate_suspended_on_sync
            post "toggle_ldap_debugging", to: "businesses/admin_center_options#toggle_ldap_debugging", as: :settings_toggle_ldap_debugging
            post "change_ldap_search_depth", to: "businesses/admin_center_options#change_ldap_search_depth", as: :settings_change_ldap_search_depth
            post "change_max_object_size", to: "businesses/admin_center_options#change_max_object_size", as: :settings_change_max_object_size
            post "change_repo_visibility", to: "businesses/admin_center_options#change_repo_visibility", as: :settings_change_repo_visibility
            post "toggle_saml_debugging", to: "businesses/admin_center_options#toggle_saml_debugging", as: :settings_toggle_saml_debugging
            post "change_cross_repo_conflict_editor", to: "businesses/admin_center_options#change_cross_repo_conflict_editor", as: :settings_change_cross_repo_conflict_editor
            post "change_dormancy_threshold", to: "businesses/admin_center_options#change_dormancy_threshold", as: :settings_change_dormancy_threshold
            post "change_anonymous_git_access", to: "businesses/admin_center_options#change_anonymous_git_access", as: :settings_change_anonymous_git_access
            post "change_anonymous_git_access_locked", to: "businesses/admin_center_options#change_anonymous_git_access_locked", as: :settings_change_anonymous_git_access_locked
          end

          scope :messages do
            get "", to: "businesses/custom_messages#show", as: :custom_messages
            get "suspended_user_message", to: "businesses/custom_messages#suspended_user_message", as: :custom_messages_suspended_user_message
            put "preview_suspended_message", to: "businesses/custom_messages#preview_suspended_message", as: :custom_messages_suspended_message_preview
            patch "", to: "businesses/custom_messages#update"
            post "", to: "businesses/custom_messages#create"
            get "authorization_provider_name", to: "businesses/custom_messages#authorization_provider_name", as: :custom_messages_authorization_provider_name
            put "preview_authorization_provider_name", to: "businesses/custom_messages#preview_authorization_provider_name", as: :custom_messages_authorization_provider_name_preview
            get "sign_out_message", to: "businesses/custom_messages#sign_out_message", as: :custom_messages_sign_out_message
            put "preview_sign_out_message", to: "businesses/custom_messages#preview_sign_out_message", as: :custom_messages_sign_out_message_preview
            unless GitHub.auth.saml?
              get "sign_in_message", to: "businesses/custom_messages#sign_in_message", as: :custom_messages_sign_in_message
              put "preview_sign_in_message", to: "businesses/custom_messages#preview_sign_in_message", as: :custom_messages_sign_in_message_preview
            end
            get "announcement", to: "businesses/custom_messages#announcement", as: :custom_messages_announcement
            put "preview_announcement", to: "businesses/custom_messages#preview_announcement", as: :custom_messages_announcement_preview
            patch "announcement", to: "businesses/custom_messages#set_announcement", as: :custom_messages_set_announcement
          end

          scope :dotcom_connection do
            get "", to: "businesses/dotcom_connection#index", as: :admin_settings_dotcom_connection
            post "", to: "businesses/dotcom_connection#create", as: :admin_settings_create_dotcom_connection
            post "resume_dotcom_connection", to: "businesses/dotcom_connection#resume", as: :admin_settings_resume_dotcom_connection
            delete "", to: "businesses/dotcom_connection#destroy", as: :admin_settings_delete_dotcom_connection
            get "complete/:version.:token", to: "businesses/dotcom_connection#complete", as: :admin_settings_complete_dotcom_connection

            post "change_search", to: "businesses/dotcom_connection#change_search", as: :admin_settings_change_dotcom_search
            post "change_private_search", to: "businesses/dotcom_connection#change_private_search", as: :admin_settings_change_dotcom_private_search
            post "change_contributions", to: "businesses/dotcom_connection#change_contributions", as: :admin_settings_change_dotcom_contributions
            post "change_license_usage_sync", to: "businesses/dotcom_connection#change_license_usage_sync", as: :admin_settings_change_license_usage_sync
            post "change_content_analysis", to: "businesses/dotcom_connection#change_content_analysis", as: :admin_settings_change_ghe_content_analysis
            post "change_actions_download_archive", to: "businesses/dotcom_connection#change_actions_download_archive", as: :admin_settings_change_actions_download_archive

            get "change_complete", to: "businesses/dotcom_connection#change_complete", as: :admin_settings_change_complete
          end

          resources :pre_receive_hooks, as: :enterprise_pre_receive_hooks,
            controller: "businesses/pre_receive_hooks", only: [:destroy]
          resources :pre_receive_hook_targets, as: :enterprise_pre_receive_hook_targets,
            controller: "businesses/pre_receive_hook_targets", only: [:new, :create, :update, :show] do
            get "repository_search", on: :collection
            get "file_list", on: :collection
          end

          resources :pre_receive_environments, as: :enterprise_pre_receive_environments,
            controller: "businesses/pre_receive_environments", only: [:create, :new, :index, :show, :destroy, :update] do
            post "download", on: :member
            get "environment_actions", on: :member
          end
        end
      end

      if !GitHub.single_business_environment?
        resources :enterprise_installations, as: :enterprise_enterprise_installations,
          only: [:index, :create], controller: "businesses/enterprise_installations" do
          collection do
            post "sync_user_accounts"
          end
        end

        resources :server_licenses, only: :show, module: :businesses, constraints: { id: /[\da-f]{6}(?:[\da-f]{14})?/ }

        resource :enterprise_licensing, only: :show, controller: "businesses/enterprise_licensing" do
          member do
            get "download_consumed_licenses"
          end
        end
      end

      resources :organizations, as: :enterprise_organizations, only: %w(new create), controller: "businesses/organizations" do
        member do
          get :invite
          post :add_owner
          delete :cancel_invitation
          get :finish
        end
      end

      scope :organizations, as: :organizations do
        scope :settings, as: :settings, controller: "businesses/organizations/settings" do
          get "action_execution_capability"
          get "default_repository_permission"
          get "repository_creation"
          get "allow_private_repository_forking"
          get "members_can_invite_collaborators"
          get "members_can_change_repository_visibility"
          get "members_can_delete_repositories"
          get "members_can_delete_issues"
          get "members_can_update_protected_branches"
          get "members_can_view_dependency_insights"
          get "team_discussions"
          get "organization_projects"
          get "repository_projects"
          get "two_factor_required"
          get "saml_identity_provider"
        end
      end

      # Business SAML SSO routes
      get "/saml/metadata", to: "businesses/identity_management/saml#metadata", as: :idm_saml_metadata
      post "/saml/initiate", to: "businesses/identity_management/saml#initiate", as: :idm_saml_initiate
      post "/saml/consume", to: "businesses/identity_management/saml#consume", as: :idm_saml_consume
      get "/saml/recover", to: "businesses/identity_management/saml#recover_prompt", as: :idm_saml_recover
      post "/saml/recover", to: "businesses/identity_management/saml#recover"
      delete "/saml/revoke", to: "businesses/identity_management/saml#revoke", as: :idm_saml_revoke
      get  "/sso",           to: "businesses/identity_management#sso",             as: :business_idm_sso
      get  "/sso_status",    to: "businesses/identity_management#sso_status",      as: :business_idm_sso_status
      get  "/sso_modal",     to: "businesses/identity_management#sso_modal",       as: :business_idm_sso_modal
      get  "/sso_complete",  to: "businesses/identity_management#sso_complete",    as: :business_idm_sso_complete
      get  "/sso/sign_up",   to: "businesses/identity_management#sso_sign_up",     as: :business_idm_sso_sign_up

      # team synchronization endpoints
      post "/team-sync/install", to: "businesses/team_sync#install"
      get  "/team-sync/setup",          to: "businesses/team_sync#setup"
      post "/team-sync/initiate", to: "businesses/team_sync#initiate"
      get  "/team-sync/review",         to: "businesses/team_sync#review"
      post "/team-sync/approve", to: "businesses/team_sync#approve"
      delete "/team-sync/cancel", to: "businesses/team_sync#cancel"
      delete "/team-sync/disable", to: "businesses/team_sync#disable"

      get "/team-sync/okta-credentials/new", to: "businesses/team_sync/okta_credentials#new"
      post "/team-sync/okta-credentials", to: "businesses/team_sync/okta_credentials#create"
      get "/team-sync/okta-credentials/edit", to: "businesses/team_sync/okta_credentials#edit"
      patch "/team-sync/okta-credentials", to: "businesses/team_sync/okta_credentials#update"
      put "/team-sync/okta-credentials", to: "businesses/team_sync/okta_credentials#update"

      if !GitHub.single_business_environment?
        resources :user_licenses, only: :update, param: :login, controller: "businesses/user_licenses"

        resource :bulk_member_actions, only: :show, controller: "businesses/bulk_member_actions", as: :enterprise_bulk_member_actions do
          member do
            put :user_license_types
          end
        end
      end

      get :support, to: "businesses/private_instance_support#index", as: :private_instance_support
    end
  end

  resources :enterprise_user_accounts, controller: "business_user_accounts", only: [] do
    member do
      get :organizations
      get :enterprise_installations
      get :sso
      delete "external_identity", to: "business_user_accounts#unlink_identity", as: :unlink_identity
      delete "/sso_session/:session_id", to: "business_user_accounts#revoke_sso_session", as: :revoke_sso_session
      delete "/sso_token/:token_id", to: "business_user_accounts#revoke_sso_token", as: :revoke_sso_token
    end
  end

  resources :enterprise_installations, only: [:new, :destroy] do
    member do
      get "complete"
      get "upgrade"
    end
  end

  post "/dotcom_connection/webhooks", to: "dotcom_connection#webhooks", as: :dotcom_connection_webhooks
  get "enterprises/team-sync/azure-callback", to: "businesses/team_sync#azure_callback"

  if GitHub.single_business_environment?
    get "/enterprise(/*route)", to: "businesses#global_business_redirect", as: :global_enterprise

    get "/admin/settings", to: "businesses#redirect_to_business_settings", defaults: { group: "admin_center_options" }
    get "/admin/pre_receive_hook_targets", to: "businesses#redirect_to_business_settings", defaults: { group: "hooks" }
    get "/admin/global_hooks(/*path)", to: "businesses#redirect_to_business_settings", defaults: { group: "hooks" }
    get "/admin/:group(/*path)", to: "businesses#redirect_to_business_settings"
  end

  ##
  # PublicKeys controller
  post   "/account/public_keys",                         to: "public_keys#create",  as: :public_keys
  put    "/account/public_keys/:id",                     to: "public_keys#update",  as: :public_key,                                                             id: /[^\/.?]+/
  delete "/account/public_keys/:id",                     to: "public_keys#destroy",                                                                                 id: /[^\/.?]+/
  post   "/account/public_keys/:id/verify",              to: "public_keys#verify",  as: :verify_public_key,                                                      id: /[^\/.?]+/
  delete "/account/public_keys/:id/authorizations/:org", to: "public_keys#remove_authorization", as: :public_key_authorization,                                  id: /[^\/.?]+/

  ##
  # PublicKeys controller
  post   "/account/gpg_keys",                         to: "gpg_keys#create",  as: :gpg_keys
  delete "/account/gpg_keys/:id",                     to: "gpg_keys#destroy", as: :gpg_key

  if GitHub.billing_enabled?
    # CouponsController
    get  "/redeem/",       to: "coupons#find",  as: "find_coupon"
    get  "/redeem/:code",  to: "coupons#show",  as: "redeem_coupon"
    post "/redeem/:code",  to: "coupons#redeem"
    get  "/coupon/:code",  to: redirect("/redeem/%{code}")
    get  "/coupons/:code", to: redirect("/redeem/%{code}")
    get  "/$/:code",       to: redirect("/redeem/%{code}")

    get  "/golden_ticket/:code",   to: "coupons#show_golden_ticket",    as: "show_golden_ticket_coupon"
    post "/golden_ticket",         to: "coupons#redeem_golden_ticket",  as: "redeem_golden_ticket_coupon"
    get  "/golden_ticket_confirm", to: "coupons#confirm_golden_ticket", as: "confirm_golden_ticket_coupon"

    # AssetStatusController
    get  "/account/billing/data/upgrade",                          to: "asset_status#upgrade",   as: "billing_upgrade_data_plan"
    get  "/account/billing/data/downgrade",                        to: "asset_status#downgrade", as: "billing_downgrade_data_plan"
    put  "/account/billing/data",                                  to: "asset_status#update",    as: "billing_data_plan"
    get  "/organizations/:organization_id/billing/data/upgrade",   to: "asset_status#upgrade",   as: "org_billing_upgrade_data_plan",   target: "organization"
    get  "/organizations/:organization_id/billing/data/downgrade", to: "asset_status#downgrade", as: "org_billing_downgrade_data_plan", target: "organization"
    put  "/organizations/:organization_id/billing/data",           to: "asset_status#update",    as: "org_billing_data_plan",           target: "organization"

    # BillingSettingsController
    post "/billing/extra",                                             to: "billing_settings#extra_update",          as: "billing_extra_update"
    post "/account/billing/update_credit_card",                        to: "billing_settings#update_credit_card",    as: "update_credit_card"
    get  "/account/billing/client_token",                              to: "billing_settings#client_token",          as: "client_token"
    get  "/account/billing/history",                                   to: "billing_settings#payment_history",       as: "payment_history"
    get  "/account/receipt/:id",                                       to: "billing_settings#receipt",               as: "receipt"
    post "/account/cc_update",                                         to: "billing_settings#cc_update",             as: "cc_update"
    post "/account/user_profile_update",                               to: "billing_settings#user_profile_update",   as: "user_profile_update"
    post "/account/cycle_update",                                      to: "billing_settings#cycle_update",          as: "cycle_update"
    get  "/account/plans",                                             to: "billing_settings#plans",                 as: "plans"
    get  "/account/upgrade",                                           to: "billing_settings#upgrade",               as: "upgrade"
    post "/account/downgrade_with_exit_survey",                        to: "billing_settings#downgrade_with_exit_survey", as: "downgrade_with_exit_survey"
    get  "/organizations/:organization_id/billing/receipt/:id",        to: "billing_settings#receipt",            as: "org_receipt",            target: "organization"
    get  "/organizations/:organization_id/billing/plans",              to: "billing_settings#plans",              as: "org_plans",              target: "organization"
    post "/organizations/:organization_id/billing/cc_update",          to: "billing_settings#cc_update",          as: "org_cc_update",          target: "organization"
    post "/organizations/:organization_id/billing/cycle_update",       to: "billing_settings#cycle_update",       as: "org_cycle_update",       target: "organization"
    post "/organizations/:organization_id/billing/extra",              to: "billing_settings#extra_update",       as: "org_extra_update",       target: "organization"
    get  "/organizations/:organization_id/billing/history",            to: "billing_settings#payment_history",    as: "org_payment_history",    target: "organization"
    post "/organizations/:organization_id/billing/update_credit_card", to: "billing_settings#update_credit_card", as: "org_update_credit_card", target: "organization"
    put  "/organizations/:organization_id/billing/update_email",       to: "billing_settings#update_email",       as: "update_billing_email",   target: "organization"
    post "/organizations/:organization_id/billing/downgrade_with_exit_survey", to: "billing_settings#downgrade_with_exit_survey", as: "org_downgrade_with_exit_survey", target: "organization"
    get  "/account/downgrade/features_count",                           to: "billing_settings#downgrade_features_count",     as: "billing_downgrade_features_count"

    get  "/account/billing/metered_exports/:id", to: "settings/metered_exports#show",    as: "metered_export"
    post "/account/billing/metered_exports",     to: "settings/metered_exports#create",  as: "metered_exports"
    post "/organizations/:organization_id/billing/metered_exports",     to: "settings/metered_exports#create",      as: "org_metered_exports",        target: "organization"
    get  "/organizations/:organization_id/billing/metered_exports/:id", to: "settings/metered_exports#show",        as: "org_metered_export",         target: "organization"

    # BillingManagersController
    get  "/organizations/:organization_id/billing_managers/new",                   to: "billing_managers#new",                 as: "org_new_billing_manager"
    post "/organizations/:organization_id/billing_managers",                       to: "billing_managers#create",              as: "org_billing_managers"
    get  "/organizations/:organization_id/billing_managers/invitation",            to: "billing_managers#show_pending",        as: "org_show_pending_billing_manager_invitation"
    get  "/organizations/:organization_id/billing_managers/invitation/sign_up",    to: "billing_managers#sign_up",             as: "org_sign_up_billing_manager_invitation"
    delete  "/organizations/:organization_id/billing_managers/invitation/:id",     to: "billing_managers#cancel",              as: "org_billing_manager_invitation", constraints: { id: /.*/ }
    post "/organizations/:organization_id/billing_managers/accept",                to: "billing_managers#accept",              as: "org_accept_billing_manager_invitation"
    delete "/organizations/:organization_id/billing_managers/:id",                 to: "billing_managers#destroy",             as: "org_billing_manager"
    get    "/organizations/:organization_id/billing_managers/invitee_suggestions", to: "billing_managers#invitee_suggestions", as: "org_billing_manager_suggestions"
    post "/organizations/:organization_id/billing_managers/invitation/:id/resend", to: "billing_managers#resend_invitation",   as: "org_resend_billing_manager_invitation", constraints: { id: /.*/ }

    # PendingPlanChangesController
    put "/pending_plan_changes/:id", to: "pending_plan_changes#update", as: :update_pending_plan_change

    # PendingSubscriptionItemChangesController
    resources :pending_subscription_item_changes, only: [:update, :destroy]

    # Orgs::ConsumedLicensesController
    get "/organizations/:organization_id/consumed_licenses", to: "orgs/consumed_licenses#show", as: :org_consumed_licenses
  end

  ##
  # DevTools
  #
  # DevTools are used by the product and engineering teams to build GitHub, and
  # DevTools routes are not available to Enterprise site admins. If Enterprise
  # site admins will need a feature, consider adding it to Stafftools.

  if GitHub.devtools_enabled?
    get "devtools", to: "devtools#index"
    namespace :devtools do
      get "restricted"

      get "feature_flags/autocomplete", to: "feature_flags#autocomplete", as: :feature_flags_autocomplete
      get "feature_flags/team_autocomplete", to: "feature_flags#team_autocomplete", as: :feature_flags_team_autocomplete
      get "feature_flags/service_autocomplete", to: "feature_flags#service_autocomplete", as: :feature_flags_service_autocomplete
      resources :feature_flags do
        get :history
        post :enable
        post :disable
        post :groups, to: "feature_flags#activate_group", as: :group
        delete "/groups/:group", to: "feature_flags#deactivate_group", as: :delete_group
        get "/actors/new", to: "feature_flags#add_actor", as: :add_actor
        get "/actors/bulk/github_team", to: "feature_flags#add_github_team", as: :add_github_team
        post "/actors/bulk/github_team", to: "feature_flags#activate_github_team", as: :activate_github_team
        post :actors, to: "feature_flags#activate_actor", as: :actor
        delete :actors, to: "feature_flags#deactivate_actor", as: :delete_actor
        put "/actor_percentage", to: "feature_flags#activate_actor_percentage", as: :actor_percentage
        put "/random_percentage", to: "feature_flags#activate_random_percentage", as: :random_percentage
      end

      resources :toggleable_features do
        post :publish
        post :unpublish
      end

      resources :exceptions, only: :index

      get "spamurai", to: "spamurai#dashboard"

      get "/surveys", to: "surveys#index"
      get "/surveys/:slug", to: "surveys#show", slug: /[^\/.?]+/, as: :survey
    end

    scope path: "/devtools" do
      resource :spamurai, controller: "devtools/spamurai", format: false, only: [] do

        resource :settings, controller: "devtools/spamurai/settings", format: false, only: [:show] do
          post :create, as: nil

          member do
            post :toggle_spam_pattern_checking
            post :toggle_spamurai
          end
        end

        resources :patterns, controller: "devtools/spamurai/patterns", only: [:index, :new, :create, :show, :update, :destroy] do
          member do
            post :disable
            post :enable
          end
          collection do
            get :queues
            post :add_queue_pattern
            delete :remove_queue_pattern
          end
        end

        resources :blacklists, controller: "devtools/spamurai/blacklists",
                           only: [:index, :create, :destroy]

        resources :datasources, controller: "devtools/spamurai/datasources",
                           only: [:index, :show]
      end

      # These aren't in the root-level namespace, so we can use "apps" instead of "github-apps" even in Enterprise
    end

    # legacy URL redirects
    legacy_admin_sections = /backscatter|growth|hounds|interaction|surveys/
    scope controller: "devtools", action: "bounce", format: false do
      get "admin/:section/*args", section: legacy_admin_sections
      get "stafftools/:section/*args", section: /experiments|spamurai/
      get "stafftools/science/*args", section: "experiments"
    end
  end

  # legacy admin URL redirects
  scope controller: "stafftools/admin_redirects", format: false do
    get "/admin/repos",        action: "repositories", as: nil
    get "/admin/repositories", action: "repositories", as: nil
    get "/admin/users",        action: "users",        as: nil
    get "/admin/coupons",      action: "coupons",      as: nil

    if Rails.development?
      get "/admin/fake_login(/:id)", action: "fake_login", as: nil
    end
  end

  # Alpha signup for GitHub team synchronization
  get   "/features/team-sync/signup",  to: "team_sync_beta_memberships#signup", as: :team_sync_beta_signup

  # Beta signup for Scheduled reminders
  get   "/features/reminders/signup",  to: "reminders_beta_memberships#signup", as: :reminders_beta_signup
  post  "/features/reminders/agree",   to: "reminders_beta_memberships#agree",  as: :reminders_beta_agree, format: false
  get   "/features/reminders/thanks",  to: "reminders_beta_memberships#thanks", as: :reminders_beta_thanks, format: false

  # Beta signup for Code Scanning
  unless GitHub.enterprise?
    get   "/features/security/advanced-security/signup", to: "code_scanning_beta_memberships#signup", as: :code_scanning_beta_signup
    post  "/features/security/advanced-security/agree",  to: "code_scanning_beta_memberships#agree",  as: :code_scanning_beta_agree, format: false
    get   "/features/security/advanced-security/thanks", to: "code_scanning_beta_memberships#thanks", as: :code_scanning_beta_thanks, format: false
  end

  # Beta signup for Codespaces
  get   "/features/codespaces/signup",  to: "codespaces_beta_memberships#signup", as: :codespaces_beta_signup
  post  "/features/codespaces/agree",   to: "codespaces_beta_memberships#agree",  as: :codespaces_beta_agree, format: false
  get   "/features/codespaces/thanks",  to: "codespaces_beta_memberships#thanks", as: :codespaces_beta_thanks, format: false

  # Beta signup for Okta Team Sync
  get   "/features/okta-team-sync/signup",  to: "okta_team_sync_beta_memberships#signup", as: :okta_team_sync_beta_signup
  post  "/features/okta-team-sync/agree",   to: "okta_team_sync_beta_memberships#agree",  as: :okta_team_sync_beta_agree, format: false
  patch "/features/okta-team-sync/agree",   to: "okta_team_sync_beta_memberships#agree", format: false
  get   "/features/okta-team-sync/thanks",  to: "okta_team_sync_beta_memberships#thanks", as: :okta_team_sync_beta_thanks, format: false

  ##
  # Biztools
  get :biztools, to: redirect("/biztools/coupons")
  namespace :biztools do
    resources :showcase_collections, path: "showcases" do
      post :perform_healthcheck, to: "showcase_items#perform_healthcheck"
      member do
        put :featured, to: "showcase_collections#set_featured"
        delete :featured, to: "showcase_collections#remove_featured"
      end

      resources :showcase_items, path: "items",
                                 only: [:create, :edit, :update, :destroy]
    end

    # RepositoryActionsController
    resources :repository_actions, only: [:index, :update, :show, :edit] do
      scope module: "repository_actions" do
        resource :listing, only: [:destroy]
      end
      collection do
        get :creators, to: "repository_actions#creators"
        post "/creators/:creator_id",
          to: "repository_actions#verify_creator",
          as: "toggle_creator"
        post "/creators/:creator_id/reindex",
          to: "repository_actions#reindex_creator",
          as: "reindex_creator"
      end
    end

    resources :topics, only: [:index] do
      collection do
        post :import
      end
    end
    resources :collections, only: :index do
      collection do
        post :import
      end
    end
    put "/topics/:topic", to: "topics#update", as: :update_topic
    put "/collections/:collection", to: "collections#update", as: :update_collection

    if GitHub.billing_enabled?
      resource :accounting, only: :show, controller: "accounting" do
        post :yearly_transactions
        post :yearly_refunds
      end

      get "/:user_id/billing/redemptions", to: "users#redemptions", as: :user_redemptions

      get "/:user_id/opensource", to: "coupons#opensource", as: :coupons_opensource
      get "/maintainers", to: "users#maintainers", as: :maintainers

      coupon_name_regex = /[a-z0-9\._\-+:*!&<>()%^]+/i
      get "/coupons/groups/:group", to: "coupons#groups", as: :coupon_groups
      resources :coupons, id: coupon_name_regex, format: false do
        post :search, on: :collection
        post :apply, on: :collection
        get :report, on: :member
      end

      resources :users, only: [:index] do
        collection do
          get "/:user_id", to: "users#show"
          put "/:user_id/enable_tos_prompt", to: "users#enable_tos_prompt", as: :enable_tos_prompt
        end
        resource :coupon, only: [] do
          delete :destroy, to: "coupons#revoke"
        end

        resources :education_terms, only: :create, controller: "users/education_terms"
      end
    end

    get "/works-with",                            to: "works_with_github_listings#index",          as: :works_with_github
    get "/works-with/categories",                 to: "integration_features#index",                as: :works_with_github_categories
    get "/works-with/:id",                        to: "works_with_github_listings#show",           as: :works_with_github_listing
    get "/works-with/:id/edit",                   to: "works_with_github_listings#edit",           as: :edit_works_with_github_listing
    put "/works-with/:id",                        to: "works_with_github_listings#update"
    post "/works-with/:id/approve",               to: "works_with_github_listings#approve",        as: :approve_works_with_github_listing
    post "/works-with/:id/reject",                to: "works_with_github_listings#reject",        as: :reject_works_with_github_listing
    post "/works-with/:id/delist",                to: "works_with_github_listings#delist",        as: :delist_works_with_github_listing

    get "/marketplace",                           to: "marketplace_listings#index",       as: :marketplace
    get "/marketplace/categories",                to: "marketplace_categories#index",     as: :marketplace_categories
    get "/marketplace/categories/new",            to: "marketplace_categories#new",       as: :new_marketplace_category
    get "/marketplace/categories/:category_slug/edit", to: "marketplace_categories#edit", as: :edit_marketplace_category
    put "/marketplace/categories/:category_slug", to: "marketplace_categories#update",    as: :marketplace_category
    post "/marketplace/categories",               to: "marketplace_categories#create"
    get "/marketplace/agreements",                to: "marketplace_agreements#index",     as: :marketplace_agreements
    get "/marketplace/agreements/new",            to: "marketplace_agreements#new",       as: :new_marketplace_agreement
    post "/marketplace/agreements",               to: "marketplace_agreements#create"
    get "/marketplace/agreements/:id",            to: "marketplace_agreements#show",      as: :marketplace_agreement
    get "/marketplace/recommendations",           to: "marketplace_recommendations#index", as: :marketplace_recommendations
    put "/marketplace/recommendations",           to: "marketplace_recommendations#update", as: :update_marketplace_recommendations
    delete "/marketplace/recommendations/:listing_slug", to: "marketplace_recommendations#destroy", as:  :destroy_marketplace_recommendations
    put "/marketplace/featured_customers/:id",    to: "marketplace_featured_customers#update", as: :marketplace_featured_customers
    get "/marketplace/stories",                   to: "marketplace/stories#index",        as: :marketplace_stories
    post "/marketplace/featured_stories/:id",     to: "marketplace/featured_stories#create", as: :feature_marketplace_story
    delete "/marketplace/featured_stories/:id",   to: "marketplace/featured_stories#destroy", as: :unfeature_marketplace_story
    get "/marketplace/:listing_slug",             to: "marketplace_listings#show",        as: :marketplace_listing
    get "/marketplace/:listing_slug/edit",        to: "marketplace_listings#edit",        as: :edit_marketplace_listing
    put "/marketplace/:listing_slug",             to: "marketplace_listings#update"
    put "/marketplace/:listing_slug/feature",     to: "marketplace_listings#feature",     as: :feature_marketplace_listing
    put "/marketplace/:listing_slug/unfeature",   to: "marketplace_listings#unfeature",   as: :unfeature_marketplace_listing
    post "/marketplace/:listing_id/approve",      to: "marketplace_listings#approve",     as: :approve_marketplace_listing
    post "/marketplace/:listing_id/reject",       to: "marketplace_listings#reject",      as: :reject_marketplace_listing
    post "/marketplace/:listing_id/delist",       to: "marketplace_listings#delist",      as: :delist_marketplace_listing
    post "/marketplace/:listing_id/reindex",      to: "marketplace_listings#reindex", as: :reindex_marketplace_listing
    get "/marketplace/:listing_id/hook",          to: "marketplace_listings#hook",        as: :marketplace_listing_hook
  end

  ##
  # Stafftools: The Next Generation
  get "stafftools/okta_team_sync_beta_signup", to: "stafftools/okta_team_sync_beta_signup#index", as: :stafftools_okta_team_sync_beta_signup
  post "stafftools/okta_team_sync_beta_signup/:membership_id", to: "stafftools/okta_team_sync_beta_signup#toggle_access", as: :stafftools_toggle_okta_team_sync_beta

  get "stafftools/reminders_beta_signup", to: "stafftools/reminders_beta_signup#index", as: :stafftools_reminders_beta_signup
  post "stafftools/reminders_beta_signup/:user_id", to: "stafftools/reminders_beta_signup#toggle_access", as: :stafftools_toggle_reminders_beta

  get "stafftools/advanced_security_beta_signup", to: "stafftools/code_scanning_beta_signup#index", as: :stafftools_code_scanning_beta_signup
  post "stafftools/advanced_security_beta_signup/toggle", to: "stafftools/code_scanning_beta_signup#toggle_access", as: :stafftools_toggle_code_scanning_beta

  get "stafftools/codespaces_beta_signup", to: "stafftools/codespaces_beta_signup#index", as: :stafftools_codespaces_beta_signup
  post "stafftools/codespaces_beta_signup/:user_id", to: "stafftools/codespaces_beta_signup#toggle_access", as: :stafftools_toggle_codespaces_beta

  get :stafftools, to: "stafftools#index"
  get "stafftools/_modal", to: "stafftools#modal", as: :stafftools_modal
  put "stafftools/comments/:id/minimize", to: "stafftools/comments#minimize", as: :stafftools_minimize_comment
  put "stafftools/comments/:id/unminimize", to: "stafftools/comments#unminimize", as: :stafftools_unminimize_comment
  delete "stafftools/comments/:id", to: "stafftools/comments#destroy", as: :stafftools_delete_comment

  get "/trade-controls/violations/:user_id", to: "stafftools/users/trade_controls_restrictions#check_violations"

  namespace :stafftools do

    resources :stream_processors, only: [:index], param: :group_id do
      put :pause, to: "stream_processors#pause"
      put :resume, to: "stream_processors#resume"
    end

    if GitHub.sponsors_enabled?
      # GitHub Sponsors Stafftools
      resource :sponsors, module: :sponsors, only: [], format: false do
        get "/", to: redirect("/stafftools/sponsors/members")

        resources :members, only: [:index, :show, :update] do
          resource :acceptance, only: [:create, :destroy], module: :members
          resource :feature, only: :update, module: :members
          resource :reputable_orgs_partial, only: :show, module: :members
          resource :transfers_preview_partial, only: :show, module: :members
          resource :payouts_preview_partial, only: :show, module: :members
          resources :criterion, only: :update, module: :members
          resource :ban, only: [:create, :destroy], module: :members
          resource :ignore, only: [:create, :destroy], module: :members
          resources :staff_notes, only: :create, module: :members
          resources :transactions, only: [:index], module: :members
          resources :transfers, only: [:index], module: :members
          resources :transfer_match_reversals, only: [:create], module: :members
          resources :receipts, only: :create, module: :members

          resource :listing, module: :members, only: [] do
            resource :approval, only: [:create, :destroy], module: :listing
            resource :payout_toggle, only: [:create, :destroy], module: :listing
            resource :match_toggle, only: [:create, :destroy], module: :listing
            resource :stripe_sync, only: :create, module: :listing
            resource :stripe_balance_partial, only: :show, module: :listing
            resources :webhooks, only: :show, module: :listing do
              patch :toggle_active_status
            end
            resource :docusign_envelope, only: [:update, :destroy], module: :listing
            resources :match_bans, only: [:create, :destroy], module: :listing
          end
        end

        resources :transactions_exports, only: :create, module: :members

        resource :waitlist, only: [:show] do
          resources :queue, only: :index, module: :waitlist
          resource :export, only: :create, module: :waitlist
          resources :auto_acceptance_review, only: :index, module: :waitlist
        end

        resources :fraud_reviews, only: [:index, :show, :update] do
          resources :transactions, only: [:index], module: :fraud_reviews
        end

        resources :find_membership, only: [:show]
      end
    end

    resources :stripe_webhooks, only: [:index] do
      put "/ignore", to: "stripe_webhooks#ignore"
      put "/perform", to: "stripe_webhooks#perform"
    end

    resources :zuora_webhooks, only: [:index] do
      put "/ignore", to: "zuora_webhooks#ignore"
      put "/investigate", to: "zuora_webhooks#investigate"
      put "/perform", to: "zuora_webhooks#perform"
    end

    resources :org_insights, only: [:index] do
      post :enqueue_org_insights_backfill, on: :collection
    end

    get :accounting, to: redirect("/biztools/accounting")

    get "/bundled_license_assignments/orphaned", to: "bundled_license_assignments#orphaned", as: :orphaned_bundled_license_assignments

    unless GitHub.enterprise?
      resources :azure_metered_billing_extracts, only: :index
    end

    resources :showcase_collections, path: "showcases" do
      post :perform_healthcheck, to: "showcase_items#perform_healthcheck"
      member do
        put :featured, to: "showcase_collections#set_featured"
        delete :featured, to: "showcase_collections#remove_featured"
      end

      resources :showcase_items, path: "items",
                                 only: [:create, :edit, :update, :destroy]
    end

    unless GitHub.enterprise?
      resources :enterprise_installations, only: [:index, :show, :destroy] do
        get :contributions, on: :member
        get :accounts, on: :member
        put :block, on: :member
        put :unblock, on: :member
      end
    end

    resources :chargebacks, only: [:index, :create] do
      get :unrecorded_disputes, on: :collection
    end

    resources :networks, only: :index do
      collection do
        post :schedule_maintenance_for_failed
      end
    end

    resources :fileservers, only: :index
    resources :reflogs, only: :index

    resources :receipts, only: [:create, :show]

    resources :reports, only: :index do
      collection do
        get :dormant_users
        get :active_users
        get :suspended_users
        get :all_users
        get :all_organizations
        get :all_repositories
        # NOTE: see: https://github.com/github/enterprise/issues/1213
        get :developers
      end
    end

    resources :projects, only: :show do
      member do
        get :permissions
        put :change_owner
        put :restore
        put :unlock
        put :unlink
        delete :destroy
      end
    end

    get "/topics/:topic", as: :topic, to: "topics#show"
    put "/topics/:topic", to: "topics#update"

    get "/dependency_graph/", as: :dependency_graph, to: "dependency_graph#index"
    match "/dependency_graph/package_results", as: :dependency_graph_package_results, to: "dependency_graph#package_results", via: [:get, :post]
    post "/dependency_graph/assign_packages", as: :dependency_graph_assign_packages, to: "dependency_graph#assign_package"

    get "/bulk_dmca_takedown/parse_notice", to: "bulk_dmca_takedown#parse_notice"
    post "/bulk_dmca_takedown/new", to: "bulk_dmca_takedown#new", as: :new_stafftools_bulk_dmca_takedown
    get "/bulk_dmca_takedown/:id/validate_repos", to: "bulk_dmca_takedown#validate_repos", as: "bulk_dmca_takedown_validate_repos"
    post "/bulk_dmca_takedown/:id/start_job", to: "bulk_dmca_takedown#start_job", as: "bulk_dmca_takedown_start_job"
    resources :bulk_dmca_takedown, only: [:show, :index]

    get "repositories", to: "repositories#index"
    resources :repositories, path: "/repositories/:user_id",
                             id: REPO_REGEX, format: false,
                             defaults: { format: :html },
                             only: [:show]

    resources :repositories, path: "/repositories/:user_id",
                             id: REPO_REGEX, format: false,
                             only: [:destroy] do


      member do
        get :abuse_reports
        get :admin
        get :collaboration
        get :database
        get :dependency_graph
        get :deploy_keys
        get :disk
        get :events
        get :funding_links
        get :topics
        get :languages
        get :notifications
        get :overview
        get :permissions
        get :releases
        get :search
        get :security
        get :uploads

        post :admin_disable
        post :archive
        post :analyze_language
        post :change_allow_force_push
        post :change_max_object_size
        post :change_warn_disk_quota
        post :change_lock_disk_quota
        post :change_ssh_access
        post :clear_dependencies
        post :detect_manifests
        post :enable_code_search
        post :fix_duplicate_labels
        post :fix_issue_transfers
        post :fsck
        post :funding_links_disable
        post :lock
        post :lock_for_migration
        post :hide_from_google
        post :hide_from_discovery
        post :require_login
        post :require_opt_in
        post :collaborators_only
        post :pause_repo_invite_limit
        post :purge_code
        post :purge_commits
        post :purge_discussions
        post :purge_issues
        post :purge_projects
        post :purge_repository
        post :purge_wiki
        post :reassign_package
        post :rebuild_commit_contributions
        post :reindex_code
        post :reindex_commits
        post :reindex_discussions
        post :reindex_issues
        post :reindex_projects
        post :reindex_repository
        post :reindex_wiki
        post :toggle_allow_git_graph
        post :rebuild_contribution_insight_graphs
        post :toggle_permission
        post :toggle_public_push
        post :toggle_token_scanning
        post :rescan_for_tokens
        post :dry_run_secret_scan
        post :unlock
        post :wiki_mark_as_broken
        post :wiki_restore
        post :wiki_schedule_maintenance
        post :redetect_license
        post :redetect_community_health_files
        post :request_access, to: "repositories/staff_access#request_access"
        post :schedule_backup
        post :schedule_wiki_backup
        post :unarchive
        post :change_anonymous_git_access
        post :change_anonymous_git_access_locked
        post :set_used_by

        put  :set_interaction_limit
        put  :unlock_repository
        put  :staff_unlock, to: "repositories/staff_access#staff_unlock"
        put  :staff_override_unlock, to: "repositories/staff_access#override_unlock"

        delete :cancel_access_request, to: "repositories/staff_access#cancel_access_request"
        delete :cancel_unlock, to: "repositories/staff_access#cancel_unlock"
        delete :disable, to: "repositories#remove_disable"
        delete :purge_events
      end

      collection do
        post ":id/permissions/search", to: "repositories#permissions", as: :search_permissions
      end

      resources :images, controller: :repository_images, only: [:index, :destroy]

      get :abilities, to: redirect("/stafftools/repositories/%{user_id}/%{repository_id}/collaboration")

      resources :recommendations, controller: :repository_recommendations, format: false,
                                  only: [:index] do
        collection do
          post :opt_out
          post :opt_in
        end
      end

      resources :collaborators, only: [:index]
      resources :notification_settings, only: [:index], controller: :repository_notifications

      resource :country_block, only: [:create, :destroy]
      resource :dmca_takedown, only: [:create, :destroy]

      resources :issues, only: [:index, :show, :destroy] do
        get :database, on: :member

        resources :issue_comments, only: [:index, :show], as: "comments",
                  path: "comments" do
          get :first, on: :collection
          get :database, on: :member
        end
        resources :subscriptions, only: :index, controller: :issue_subscriptions
      end

      resources :discussions, only: [:index, :show, :destroy] do
        member do
          get :database
          put :lock
          put :unlock
        end

        resources :discussion_comments, only: [:index, :show], as: "comments",
                  path: "comments" do
          member do
            get :database
            get :nested_comments
          end
        end
        resources :subscriptions, only: :index, controller: :discussion_subscriptions
      end

      resources :large_files, only: [:index] do
        collection do
          post   :preview, to: "large_files#enable_repo_preview"
          delete :preview, to: "large_files#disable_repo_preview"
          post   :purge_objects, to: "large_files#purge_objects"
          post   :restore_objects, to: "large_files#restore_objects"
        end

        member do
          post :archive
          post :unarchive
        end
      end

      resource :mirror, only: [:create, :update, :destroy] do
        put :sync
      end

      get :network_tree, to: redirect("/stafftools/repositories/%{user_id}/%{repository_id}/network/tree")
      get :children, to: redirect("/stafftools/repositories/%{user_id}/%{repository_id}/network/children")
      get :siblings, to: redirect("/stafftools/repositories/%{user_id}/%{repository_id}/network/siblings")
      resource :network, only: :show do
        get :children
        get :siblings
        get :tree

        post :change_root
        post :detach
        post :extract
        post :new_extract
        post :increment_git_cache
        post :mark_as_broken
        post :reattach
        post :attach_to
        post :rebuild_network_graph
        post :reparent
        post :schedule_maintenance
      end

      resource :pages, only: [:show, :create, :destroy] do
        get :status
        get :certificate_status
        get :https_status
        put :clear_domain
        put :clear_generated_pages
        put :request_https_certificate
        delete :delete_https_certificate
      end

      resources :protected_branches, only: :index

      get :pulls, to: redirect("/stafftools/repositories/%{user_id}/%{repository_id}/issues")
      get :pull_requests, to: redirect("/stafftools/repositories/%{user_id}/%{repository_id}/issues")
      resources :pull_requests, only: [:show, :destroy] do
        collection do
          post :purge
          post :reindex
        end

        member do
          get :database
          post :sync
        end

        resources :issue_comments, only: [:index, :show, :minimize], as: "comments",
                  path: "comments" do
          get :first, on: :collection
          get :database, on: :member
        end
        resources :review_comments, only: [:index, :show] do
          get :database, on: :member
        end
        resources :subscriptions, only: :index, controller: :issue_subscriptions
      end

      resources :redirects, only: [:index, :create, :destroy]
      resources :repository_files, only: [:index, :show, :destroy] do
        get :database, on: :member
      end

      get :pushlog, to: redirect("/stafftools/repositories/%{user_id}/%{repository_id}/reflog")
      resource :reflog, only: [:show] do
        post :restore
      end

      resources :projects, only: [:index]

      resource :svn, only: [:show], controller: "slumlord" do
        post :toggle_blocked
        post :toggle_debug
      end

      resources :watchers, only: :index, controller: :repository_watchers

      resources :vulnerability_alerts, only: [:index, :show]

      resource :dependabot, only: [:show], controller: :repository_dependabot
    end

    # External asset hosting (via git lfs)
    get    "/large_file_storage",                 to: "large_file_storage#index",        as: :large_file_storage
    get    "/:user_id/large_file_storage",        to: "large_file_storage#show",     as: :user_large_file_storage
    put    "/:user_id/large_file_storage",        to: "large_file_storage#update",   as: :user_update_large_file_storage
    get    "/:user_id/large_file_storage/edit",   to: "large_file_storage#edit",     as: :user_edit_large_file_storage

    # Codespaces
    scope "/users/:user_id", as: :user do
      resources :codespaces, controller: :codespaces, only: [:index, :show]
    end

    unless GitHub.enterprise?
      # Package hosting via GPR
      get    "/:user_id/packages",          to: "packages#index",     as: :user_packages
      get    "/:user_id/actions_packages",        to: "actions_packages#show",     as: :user_actions_packages_usage
    end

    get "/indexing", to: redirect("/stafftools/search_indexes")
    get "/indexing/new", to: redirect("/stafftools/search_indexes/new")
    get "/indexing/:name/manage", to: redirect("/stafftools/search_indexes/%{name}")
    get "/indexing/:name/repair", to: redirect("/stafftools/search_indexes/%{name}/repair")

    resources :search_indexes, except: [:edit] do
      collection do
        post :toggle_code_search
        post :toggle_code_search_indexing
      end

      member do
        put :make_primary

        get :repair
        post :repair, to: "search_indexes#start_repair"
        put :pause_repair
        delete :repair, to: "search_indexes#reset_repair"
      end
    end

    resources :reserved_logins, only: [:index, :create, :destroy], param: :login
    get "/reserved_logins/search", to: "reserved_logins#search"

    resources :banned_ip_addresses, only: [:index, :destroy, :create] do
      member do
        post :ban
        post :whitelist
      end
      collection do
        post :search
      end
    end

    resource :bulk_ticketer, only: [:show, :create]

    if GitHub.single_business_environment?
      get "/organizations", to: redirect("/business")
      get "/users/organizations", to: redirect("/business")
    end

    get "/rename_status/:user_id", to: "users#rename_status", as: :user_rename_status

    resources :users, only: [:index, :show, :destroy] do
      collection do
        get :by_ip
        get :dormant
        get :invoiced
        get :site_admins
        get :suspended

        get :invite
        post :invite, to: "users#send_invite"

        post :restore
        post :suspend_dormant
      end

      get "/org_owners", to: redirect("/stafftools/users/%{user_id}/owners")
      get "/history", to: "users/billing#history", as: "billing_history"

      nested do
        scope :metered_billing, as: :metered_billing do
          get :actions, to: "users/metered_billing#actions"
          get :packages_bandwidth, to: "users/metered_billing#packages_bandwidth"
          get :shared_storage_usage, to: "users/metered_billing#shared_storage_usage"
          get :actions_artifact_expirations, to: "users/metered_billing#actions_artifact_expirations"
          patch :advance_quota_reset_date, to: "users/metered_billing#advance_quota_reset_date"
        end

        resources :metered_exports, only: [:index, :show], controller: "users/metered_exports"
      end

      member do
        get :abilities
        get :activity
        get :acv_contributions
        get :admin
        get :anon_access_repos
        get :billing
        get :billing_managers
        get :collab_repos
        get :contributing_repos
        get :database
        get :disabled_repos
        get :dmca_repos
        get :enterprise_installations
        get :ssh_certificate_authorities
        get :ip_whitelisting
        patch :disable_ip_whitelisting
        get :internal_repos
        get :invitations, to: "teams#invitations"
        get :invitation_opt_outs, to: "teams#invitation_opt_outs"
        get :keys
        get :log
        get :members, to: "teams#direct_members"
        get :outside_collaborators, to: "teams#outside_collaborators"
        get :owners, to: "teams#owners"
        post :add_owner, to: "teams#add_owner"
        get :overview
        get :pending_collaborators, to: "teams#pending_collaborators"
        get :public_repos
        get :purgatory
        get :rate_limits
        get :repositories
        get :search
        get :security
        get :security_history
        post :set_hubber_access_reason
        get :starred_repos
        get :successors
        get :terms_of_service
        get :user_interactions
        get :watching
        get :retired_namespaces
        get :domains
        get :abuse_reports

        post :block_user
        post :backfill_org_insights
        post :change_allow_force_push
        post :change_ssh_access
        post :charge
        post :toggle_large_scale_contributor
        post :flag_contribution_classes
        post :keys
        post :lock_billing
        post :ofac_sanction_override
        post :pay_by_credit_card
        post :pay_by_invoice
        post :rebuild_commit_contributions
        post :reindex
        post :remove_credit_card
        post :remove_transform_lockout
        post :run_team_sync_configuration
        post :rename
        post :reset_rate_limits
        post :revoke_employee_access
        post :run_pending_changes
        post :set_github_biztools_user
        post :set_github_developer
        post :set_site_admin
        post :mark_deceased
        post :sync_external_subscription
        post :reset_billing_attempts
        post :temporary_rate_limit_whitelist
        post :toggle_acv_contribution_ignore
        post :toggle_interaction_ban
        post :toggle_operator_mode
        post :toggle_rate_limit_whitelisted
        post :toggle_standard_contractual_clauses
        post :trade_controls_restrictions_enforce, to: "users/trade_controls_restrictions#enforce"
        post :trade_controls_restrictions_update, to: "users/trade_controls_restrictions#update"
        post :trade_controls_restrictions_override, to: "users/trade_controls_restrictions#override"
        post :pending_trade_controls_restrictions_extend, to: "users/pending_trade_controls_restrictions#extend"
        post :pending_trade_controls_restrictions_cancel, to: "users/pending_trade_controls_restrictions#cancel"
        post :unblock_user
        post :unfollow_user
        post :unlock_billing
        post :toggle_legal_hold
        post :retire_namespace
        post :unretire_namespace
        post :clear_org_token_scan_backfill
        post :start_org_token_scan_backfill

        put :add_note
        put :change_plan
        put :set_interaction_limit
        put :set_terms_of_service
        put :toggle_suspension

        delete :clear_all_activity
        delete :clear_public_activity
        delete :revoke_privileged_access
        delete :lockout, to: "users#remove_lockout"

        post :action_invocation, to: "action_invocation#unblock"
        delete :action_invocation, to: "action_invocation#block"

        post   :preview_lfs, to: "large_files#enable_user_preview"
        delete :preview_lfs, to: "large_files#disable_user_preview"
        post   :rebuild_status, to: "large_files#rebuild_status"

        unless GitHub.enterprise?
          post   :preview_gpr, to: "packages#enable_user_preview"
          delete :preview_gpr, to: "packages#disable_user_preview"
        end

        get :subpoena_discovery, to: "legal#subpoena_discovery"

        resource :dependabot, only: [:show], controller: :dependabot do
          post :enable_rollout
          post :disable_rollout
        end

        resource :dependency_graph, controller: :dependency_graph do
          get  :org, to: "dependency_graph#show"
          post :clear_org_dependencies
          post :redetect_org_dependencies
        end
      end

      resource :data_packs, only: :destroy, to: "users/data_packs#delete"
      unless GitHub.enterprise?
        resources :prepaid_metered_usage_refills, only: [:index, :new, :create, :destroy], controller: "users/prepaid_metered_usage_refills"
      end

      resources :domains, only: [:show] do
        member do
          put :verify
          put :unverify
        end
      end

      resources :organization_external_identities, only: [:index, :show, :destroy],
        path: :external_identities,
        as: :external_identities

      get "/legacy_show", to: redirect("/stafftools/users/%{user_id}")

      resources :applications, only: [:index, :show, :update] do
        get :index, to: "applications#user", on: :collection
        get "/developers/", to: "applications#developers", on: :collection
        get "/github/", to: "applications#github", on: :collection
        put :update_creation_limit, to: "applications#update_creation_limit", on: :collection

        member do
          post :suspend
          post :enable
          post :disable_full_trust
        end

        resource :integration_listing, only: [:create, :update]
        resource :app_listing, only: [:create, :update], controller: "integration_listings"

        resource :authorization, only: [:show, :destroy]
      end

      put "/authenticated_devices/verify", to: "authenticated_devices#verify"
      put "/authenticated_devices/unverify", to: "authenticated_devices#unverify"

      resources :avatars, only: [:index, :destroy] do
        collection do
          post :revert_to_gravatar
          post :undo
          post :purge
        end

        member do
          put :promote
        end
      end

      if GitHub.billing_enabled?
        resource :billing_date_change, only: [:show, :create]
        delete "/subscription_items/:id", to: "subscription_items#destroy", as: :subscription_item

        get "/billing/per_seat_pricing_model", to: redirect("/stafftools/users/%{user_id}/per_seat_pricing_model")
        resource :per_seat_pricing_model, only: [:show, :update],
          controller: "per_seat_pricing_model"

        resource :seat_change, only: :show,
          controller: "seat_change"

        resource :plan_subscription, only: [] do
          collection do
            get :synchronization
            put :update_billing_date
          end
        end
      end

      get "/issue_comments", to: redirect("/stafftools/users/%{user_id}/comments")
      resources :comments, only: :index

      resources :emails, only: [:index, :create, :destroy] do
        collection do
          get :duplicates
          put :disable_mandatory_email_verification
          put :disable_marketing_email
          put :repair_primary
          put :request_verification
          put :restore_mandatory_email_verification
          put :set_email
          put :change_email_notifications
        end

        member do
          put :repair
        end
      end

      resources :gists, only: [:index, :show, :destroy] do
        collection do
          get :archived
        end

        member do
          post :restore

          post :dmca_takedown
          delete :dmca_takedown, to: "gists#dmca_restore"

          post :country_block
          delete :country_block, to: "gists#remove_country_block"

          post :schedule_backup
          post :mark_as_broken
          post :schedule_maintenance
        end
      end

      resources :gpg_keys, only: [:index, :show] do
        get :database, on: :member
      end

      resources :installations, only: [:index, :show, :update],
        controller: "integration_installations"

      resources :apps, only: [:index, :show], controller: "integrations", as: :apps do
        collection do
          get :authorizations
          put :update_creation_limit
        end

        member do
          post :rename
          patch :toggle_hook_active_status
        end

        resource :listing, only: [:create, :update], controller: "integration_listings"
      end

      get  :ldap, to: "ldap#index", on: :collection
      post :ldap, to: "ldap#create", on: :collection
      resource :ldap, only: [:update], controller: "ldap" do
        put :sync
      end

      resources :oauth_tokens, only: [:index, :show, :destroy] do
        put :compare, on: :member
      end

      get "/orgs", to: redirect("/stafftools/users/%{user_id}/organization_memberships")
      resources :organization_memberships, only: [:index, :show]
      post "/organization_memberships/remove_user/:id",
          to: "organization_memberships#remove_user",
          as: :remove_from_org
      put "/organization_memberships/enable_two_factor_requirement/:id",
           to: "organization_memberships#enable_two_factor_requirement",
           as: :enable_two_factor_requirement
      put "/organization_memberships/disable_two_factor_requirement/:id",
           to: "organization_memberships#disable_two_factor_requirement",
           as: :disable_two_factor_requirement

      resource :password, only: [] do
        put :randomize
        put :send_reset_email
      end

      resource :feature_enrollments, only: [:show] do
        post :toggle
      end

      resources :projects, only: [:index]

      resource :saml_provider, only: :destroy, to: "orgs/saml_providers#destroy"

      resource :spam, only: [:create, :destroy], controller: "spam" do
        post :flag_all
        post :whitelist
        post :toggle_renaming_or_deleting
        post :toggle_override_for_owned_orgs
      end

      resources :teams, only: [:index, :show] do
        collection do
          post :set_invitation_rate_limit
          post :reset_invitation_rate_limit
          post :remove_invitation_opt_out
        end

        member do
          get :database
          get :members
          get :repositories
          get :child_teams
          get :external_groups
          get :requests
          put :sync
        end

        resource :ldap, only: [] do
          put :update, to: "ldap#update_team"
          put :sync, to: "ldap#sync_team"
        end

        get "/external_groups/:group_id/members", to: "teams#external_members", as: :external_members
      end

      resource :two_factor_credential, only: :destroy do
        get :sms_log
        get :sms_logs
        put :check_otp
        put :send_test_sms
        post :change_sms_provider
        post :approve_2fa_removal_request
        post :decline_2fa_removal_request
      end

      resources :trial_extensions, only: [:create], controller: "users/trial_extensions"

      unless GitHub.enterprise?
        resources :staff_access_requests, only: :create, controller: "users/staff_access_requests" do
          member do
            put :cancel
          end
        end
      end

      if GitHub.enterprise?
        resource :saml_mapping, only: :update, controller: "users/saml_mapping"
      end
    end

    get "/search", to: "search#search", as: :search
    get "/audit_log", to: "search#audit_log", as: :audit_log
    get "/audit_log/advanced_search", to: "search#audit_log_advanced_search", as: :audit_log_advanced_search
    get "/cname", to: "search#cname", as: :search_cname
    get "/user_assets", to: "search#user_assets", as: :search_user_assets

    get "/user_assets/:id", to: "user_assets#show", as: :user_asset
    delete "/user_assets/:id", to: "user_assets#destroy"
    post "/user_assets/:id/send_for_scanning", to: "user_assets#send_for_scanning", as: :user_asset_scan
    post "/users/:user_id/batch_scan_user_assets", to: "user_assets#send_for_batch_scan_for_user", as: :user_asset_batch_scan
    post "/repositories/:repository_id/batch_scan_repository_assets", to: "user_assets#send_for_batch_scan_for_repository", as: :repository_asset_batch_scan

    get  "/restore/:id/partial", to: "purgatory#restore_partial", as: :restore_repo_partial
    get  "/restore/:id", to: "purgatory#restore_status", as: :restore_repo
    post "/restore/:id", to: "purgatory#restore"
    post "/restore_bulk", to: "purgatory#restore_bulk", as: :restore_repo_bulk

    get  "/purge/:id", to: "purgatory#purge_status", as: :purge_repo
    post "/purge/:id", to: "purgatory#purge"

    post "/impersonate/:id", to: "sessions#impersonate", as: "impersonate"

    unless GitHub.enterprise?
      post "/override_impersonate/:id", to: "sessions#override_impersonate", as: "override_impersonate"
    end

    delete "/impersonate",   to: "sessions#back_to_the_login", as: "deimpersonate"

    if Rails.development?
      get "/fake_login/:id", to: "sessions#fake_login", as: :fake_login
    end

    post "/ssh_audit", to: "site#ssh_audit", as: :ssh_audit

    get "/graphs/flamegraph", to: "graphs#flamegraph"

    get  "/explore", to: "explore#index", as: :explore
    post "/explore/queue_trending", to: "explore#queue_trending", as: :trending_job_queue

    get "/storage", to: "storage#index", as: :storage
    get "/storage/blobs", to: "storage#blobs", as: :storage_blobs
    get "/storage/blobs/:oid", to: "storage#blob", as: :storage_blob
    get "/assets", to: "assets#index", as: :assets
    get "/assets/avatars/:id", to: "assets#avatar", as: :asset_avatar
    get "/assets/:oid", to: "assets#show", as: :asset

    if GitHub.lockouts_enabled?
      get    "/locked_ip", to: "locked_ip#index", as: :locked_ip
      delete "/locked_ip/:ip_address", to: "locked_ip#destroy", as: :remove_locked_ip, ip_address: IP_REGEX
      post   "/locked_ip/:ip_address/whitelist", to: "locked_ip#whitelist", as: :whitelist_ip, ip_address: IP_REGEX
      delete "/locked_ip/:ip_address/whitelist", to: "locked_ip#unwhitelist", as: :unwhitelist_ip, ip_address: IP_REGEX
    end

    if GitHub.billing_enabled?
      get  "/billing_transactions/search", to: "billing_transactions#search", as: :billing_transaction

      post "/billing_transactions/refunds/:transaction_id", to: "refunds#create", as: :billing_transactions_refund
      post "/:user/refund_transaction", to: "refunds#create", as: :user_refund_transaction
    end

    unless GitHub.single_business_environment?
      resources :enterprises, controller: "businesses", param: :slug do
        member do
          get    :organizations
          patch  :organizations,    to: "businesses#add_organization"
          delete :organizations,    to: "businesses#remove_organization"
          patch  "organizations/transfer", to: "businesses#transfer_organization", as: :transfer_organization
          get    :people
          get    :members
          get    :pending_members
          get    :outside_collaborators
          get    :pending_collaborators
          get    :support_entitlees
          get    :billing_managers
          get    :pending_billing_managers
          delete :billing_managers, to: "businesses#remove_billing_manager"
          get    :admins
          get    :pending_admins
          put    "pending_admins/:invitation_id/cancel", to: "businesses#cancel_admin_invitation", as: :cancel_admin_invitation
          patch  :admins,           to: "businesses#add_owner"
          delete :admins,           to: "businesses#remove_owner"
          get    :hooks,            to: "businesses#hooks"
          get    "hooks/:id",       to: "businesses#hook", as: :hook
          patch  "hooks/:id/toggle_active_status", to: "businesses#toggle_hook_active_status", as: :toggle_hook_active_status
          get    :find_organizations
          get    :security
          delete :saml_provider,    to: "businesses#remove_saml_provider"
          patch  :disable_ip_whitelisting, to: "businesses#disable_ip_whitelisting"
          patch  :rename_slug,      to: "businesses#rename_slug"
          get    :enterprise_installations
          get    :complete
          get    :admin_suggestions, to: "businesses#admin_suggestions"

          resources :enterprise_agreements, only: [:new, :edit, :create, :update], controller: "businesses/enterprise_agreements"
          resources :licenses, only: [:index], controller: "businesses/licenses", as: :enterprise_licenses
          resources :watermark_reports, only: [:index], controller: "businesses/watermark_reports", as: :enterprise_watermark_reports
          get :bundled_license_assignments, to: "bundled_license_assignments#enterprise", as: :bundled_license_assignments

          scope as: :enterprise do
            resource :debug_tools, only: [:show], controller: "businesses/debug_tools" do
              post :sync_billing
            end
          end

          resources :external_identities, only: [:show, :destroy],
            controller: "business_external_identities",
            as: :enterprise_external_identities do

            collection do
              get :linked_saml_members
              get :unlinked_saml_members
              get :linked_saml_orgs
            end
          end

          resources :prepaid_metered_usage_refills, only: [:index, :new, :create, :destroy], controller: "businesses/prepaid_metered_usage_refills"
          resource :actions_packages, only: [:show], controller: "businesses/actions_packages"
          resource :actions_packages, only: [:show], controller: "businesses/actions_packages"
          resources :metered_exports, only: [:index, :show], controller: "businesses/metered_exports"
        end
        collection do
          post :check_slug
        end
      end
    end

    get "/organizations/:user/hooks",     to: "hooks#index", as: :org_hooks
    get "/organizations/:user/hooks/:id", to: "hooks#show",  as: :org_hook
    patch "/organizations/:user/hooks/:id/toggle_active_status", to: "hooks#toggle_active_status", as: :org_hook_toggle_active_status
    get "/:user_id/:repository/hooks",       to: "hooks#index", repository: REPO_REGEX, as: :repo_hooks
    get "/:user_id/:repository/hooks/:id",   to: "hooks#show",  repository: REPO_REGEX, as: :repo_hook
    patch "/:user_id/:repository/hooks/:id/toggle_active_status", to: "hooks#toggle_active_status", repository: REPO_REGEX, as: :repo_hook_toggle_active_status

    delete "/commit_comments/:user/:comment_id", to: "commit_comments#destroy", as: :user_commit_comment
    resources :commit_comments, only: [:show]
    delete "/gist_comments/:user/:comment_id", to: "gist_comments#destroy", as: :user_gist_comment

    post :dismiss_license_notice, to: "users#dismiss_license_notice", as: :dismiss_license_notice
    post :dismiss_certificate_notice, to: "users#dismiss_certificate_notice", as: :dismiss_certificate_notice

    put "/:user/remove_watch/:repo_id", to: "users#remove_watch", as: :user_remove_watch

    resources :vulnerabilities, except: [:new, :create] do
      post :enqueue_dotcom_sync, on: :collection
      put :publish, on: :member
      put :create_range_alerts, on: :member
      delete :withdraw, on: :member
      resources :vulnerable_versions, only: [:new, :create, :destroy] do
        put :process_alerts, on: :member
        get :dependent_count, on: :member
        get :preview_alert, on: :member
        get :alert_progress, on: :member
      end
    end

    resources :pending_vulnerabilities do
      put :review, on: :member
      put :submit, on: :member
      put :reject, on: :member
      resources :pending_vulnerable_versions, only: [:new, :create, :destroy, :edit, :update] do
        get :dependent_count, on: :member
        get :preview_alert, on: :member
      end
    end

    get "/internal-apps",                         to: "internal_apps#index", as: :internal_apps
    get "/internal-apps/:app_alias",              to: "internal_apps#show", as: :internal_apps_show

    resources :integration_installation_triggers, path: "automatic-apps"
  end

  # Redirect from old stafftools routes to the new ones
  get "/stafftools(TNG)/:user(/:repo(/*path))", repo: REPO_REGEX,
                                                to: "stafftools/site#redirect"

  # WikiController
  get  "/wiki",        to: "wiki#help_redirect", as: nil
  delete "/wiki/help", to: "wiki#dismiss_help",  as: :wiki_dismiss_help

  # Discussions Dashboard
  get "/discussions",           to: "discussions_dashboard#index", created_by: true,
    as: :all_discussions
  get "/discussions/commented", to: "discussions_dashboard#index", commented: true,
    as: :all_discussions_commented

  ##
  # Support archive
  # https://support.github.com/discussions/sales/658-interest-in-githubfi
  get "/discussions/:category/:thread", to: "site#support_bounce"
  get "/discussions/:category",         to: "site#support_bounce"

  # Package dependencies dashboard
  if GitHub.dependency_graph_enabled?
    get "/dependency-insights", to: "package_dependencies/global#index", as: :packages_dashboard
    get "/dependency-insights/graphs/security", to: "package_dependencies/global#security_graph", as: :packages_dashboard_security_graph
    get "/dependency-insights/graphs/licenses", to: "package_dependencies/global#licenses_graph", as: :packages_dashboard_licenses_graph
    get "/dependency-insights/:ecosystem/:name/:version(/:tab)", to: "package_dependencies/global#show", as: :package_details, constraints: { name: PACKAGE_DEPENDENCY_REGEX, version: PACKAGE_DEPENDENCY_REGEX }
    get "/dependency-insights/license_menu_content", to: "package_dependencies/global#license_menu_content", as: :packages_dashboard_license_menu_content
  end

  # Issues Dashboard
  get "/issues",            to: "issues#dashboard", created_by: true, as: :all_issues
  get "/issues/assigned",   to: "issues#dashboard", assigned:   true, as: :all_issues_assigned
  get "/issues/mentioned",  to: "issues#dashboard", mentioned:  true, as: :all_issues_mentioned

  post "/comments/issues", to: "comments/issues#create", as: :new_issue_from_comment

  # Pull Requests Dashboard
  get "/pulls",             to: "issues#dashboard", created_by: true, pulls_only: true, as: :all_pulls
  get "/pulls/assigned",    to: "issues#dashboard", assigned:   true, pulls_only: true, as: :all_pulls_assigned
  get "/pulls/mentioned",   to: "issues#dashboard", mentioned:  true, pulls_only: true, as: :all_pulls_mentioned
  get "/pulls/review-requested", to: "issues#dashboard", review_requested:  true, pulls_only: true, as: :all_pulls_review_requested
  get "/pull/:user_id/:repository/:id/review-status", to: "issues#pr_review_status", pulls_only: true, as: :pull_review_status

  get "/issues/show_menu_content", to: "issues#show_menu_content", as: :show_menu_content_issues_dashboard

  post "/upload/manifests",       to: "upload_manifests#create", as: :upload_manifests
  post "/upload/policies/:model", to: "upload_policies#create", as: :upload_policy
  put  "/upload/:model(/:id)",    to: "uploads#update"
  post "/upload/:model(/:id)",    to: "uploads#create", as: :upload

  ##
  # New repository
  get "/new",    to: "repositories#new", as: :new_repository

  ##
  # New project
  get "/new/project", to: "users/projects#new", as: :new_project

  ##
  # Repository import
  get "/new/import",  to: "repository_imports#new",    as: :new_repository_import
  post "/new/import", to: "repository_imports#create", as: :repository_imports
  # For /owner/repo/import, to: "repository_imports#show" search this file for "Repository import"

  # DownloadsController
  get    "/downloads/:user_id/:repository/:name", to: "downloads#show",                       repository: REPO_REGEX, name: /.+/

  ##
  # Ajax
  post "/users/follow",          to: "users#follow", as: :follow_user, format: false
  post "/users/unfollow",        to: "users#unfollow", as: :unfollow_user, format: false
  post "/users/set_protocol",    to: "users#set_protocol", as: :user_set_protocol, format: false
  post "/users/checkout-preference", to: "users#update_checkout_preference", as: :user_update_checkout_preference
  get "/users/status",           to: "user_statuses#show", as: :user_status
  put "/users/status",           to: "user_statuses#update"
  get "/users/status/members",   to: "user_statuses#member_statuses", as: :member_statuses
  get "/users/status/emoji",     to: "user_statuses#emoji_picker", as: :user_status_emoji_picker
  get "/users/status/organizations", to: "user_statuses#org_picker", as: :user_status_org_picker

  # Autocomplete
  get "/autocomplete/users", to: "autocomplete#users",  as: :autocomplete_users
  get "/autocomplete/all-organizations", to: "autocomplete#organizations", as: :autocomplete_organizations
  get "/autocomplete/:org/users", to: "autocomplete#org_users", as: :autocomplete_org_users

  get "/autocomplete/user-suggestions", to: "autocomplete#user_suggestions", as: :user_suggestions
  get "/autocomplete/organizations",    to: "autocomplete#organization_suggestions", as: :organization_suggestions
  get "/autocomplete/emoji",            to: "autocomplete#emoji_suggestions", as: :emoji_suggestions

  namespace :email, format: false do
    resource :subscribe,     only: [:create, :update]
    resource :unsubscribe,   only: [:show]

    if GitHub.email_preference_center_enabled?
      resource :preferences, only: [:show, :update]
      resource :optin,       only: [:create, :show]
      get "/", to: redirect("/email/preferences")
    else
      get "/", to: redirect("/settings/email#preferences")
    end
  end
  if GitHub.email_preference_center_enabled?
    get "/email-optin", to: redirect(path: "/email/optin"),          format: false
  else
    get "/email-optin", to: redirect("/settings/email#preferences"), format: false
  end

  resource :newsletter_preferences, only: [:update]

  get "/mailers/unsubscribe",             to: redirect("/email/unsubscribe"), format: false
  get "/settings/unsubscribe/newsletter", to: redirect("/email/unsubscribe"), format: false

  get "/search",                to: "codesearch#index",           as: :search
  get "/search/advanced",       to: "codesearch#advanced_search", as: :advanced_search
  get "/search/count",          to: "codesearch#count",           as: :search_count
  get "/search/explore-topics", to: "codesearch#explore_topics",  as: :search_explore_topics
  get "/search/unscoped-gds",   to: "codesearch#dark_ship",       as: :unscoped_search_geyser_dark_ship

  if GitHub.enterprise?
    get "/dotcom-search", to: "dotcom_codesearch#index",    as: :dotcom_search
    get "/dotcom-search/advanced",       to: "dotcom_codesearch#advanced_search", as: :dotcom_advanced_search
    get "/dotcom-search/count",    to: "dotcom_codesearch#count",           as: :dotcom_search_count
  end

  # NotificationsController

  # get redirects from v2 -> beta
  get  "/notifications/v2",       to: redirect(path: "/notifications/beta")
  get  "/notifications/v2/*rest", to: redirect(path: "/notifications/beta/%{rest}")

  get "/notifications/beta", to: redirect(path: "/notifications"), constraints: NOTIFICATIONS_V2_REDIRECT_CONSTRAINT, as: :notification_beta_redirect
  get "/notifications", to: "notifications_v2#index", constraints: NOTIFICATIONS_V2_REDIRECT_CONSTRAINT, as: :notifications_v2_index
  get "/notifications/all", to: redirect("/notifications"), constraints: NOTIFICATIONS_V2_REDIRECT_CONSTRAINT, as: :notifications_beta_redirect_all
  get "/notifications/read", to: redirect("/notifications?query=is%3Aread"), constraints: NOTIFICATIONS_V2_REDIRECT_CONSTRAINT, as: :notifications_beta_redirect_read
  get "/notifications/participating", to: redirect("/notifications?query=reason%3Aparticipating"), constraints: NOTIFICATIONS_V2_REDIRECT_CONSTRAINT, as: :notifications_beta_redirect_participating
  get "/notifications/saved", to: redirect("/notifications?query=is%3Asaved"), constraints: NOTIFICATIONS_V2_REDIRECT_CONSTRAINT, as: :notifications_beta_redirect_saved

  get  "/notifications/beta",                                  to: "notifications_v2#index"
  get  "/notifications/beta/shelf",                            to: "notifications_v2#shelf", as: :notification_shelf
  post "/notifications/beta/set_preferred_inbox_query",        to: "notifications_v2#set_preferred_inbox_query", as: :notifications_beta_set_preferred_inbox_query
  post "/notifications/beta/update_view_preference",           to: "notifications_v2#update_view_preference", as: :notifications_beta_update_view_preference
  post "/notifications/beta/mark",                             to: "notifications_v2#mark_as_read", as: :notifications_beta_mark_as_read
  post "/notifications/beta/unmark",                           to: "notifications_v2#mark_as_unread", as: :notifications_beta_mark_as_unread
  post "/notifications/beta/archive",                          to: "notifications_v2#mark_as_archived", as: :notifications_beta_mark_as_archived
  post "/notifications/beta/unarchive",                        to: "notifications_v2#mark_as_unarchived", as: :notifications_beta_mark_as_unarchived
  post "/notifications/beta/subscribe",                        to: "notifications_v2#mark_as_subscribed", as: :notifications_beta_mark_as_subscribed
  post "/notifications/beta/unsubscribe",                      to: "notifications_v2#mark_as_unsubscribed", as: :notifications_beta_mark_as_unsubscribed
  post "/notifications/beta/star",                             to: "notifications_v2#mark_as_starred", as: :notifications_beta_mark_as_starred
  post "/notifications/beta/unstar",                           to: "notifications_v2#mark_as_unstarred", as: :notifications_beta_mark_as_unstarred
  post "/notifications/beta/create_custom_inbox",              to: "notifications_v2#create_custom_inbox", as: :notifications_beta_create_custom_inbox
  post "/notifications/beta/dismiss_unwatch_suggestion_alert", to: "notifications_v2#dismiss_unwatch_suggestion_alert", as: :notifications_beta_dismiss_unwatch_suggestion_alert
  put  "/notifications/beta/update_custom_inbox",              to: "notifications_v2#update_custom_inbox", as: :notifications_beta_update_custom_inbox
  delete "/notifications/beta/delete_custom_inbox",            to: "notifications_v2#delete_custom_inbox", as: :notifications_beta_delete_custom_inbox

  # Legacy non-get routes for backwards compatibility while rolling v2 -> beta
  post "/notifications/v2/mark",                             to: "notifications_v2#mark_as_read"
  post "/notifications/v2/unmark",                           to: "notifications_v2#mark_as_unread"
  post "/notifications/v2/archive",                          to: "notifications_v2#mark_as_archived"
  post "/notifications/v2/unarchive",                        to: "notifications_v2#mark_as_unarchived"
  post "/notifications/v2/subscribe",                        to: "notifications_v2#mark_as_subscribed"
  post "/notifications/v2/unsubscribe",                      to: "notifications_v2#mark_as_unsubscribed"
  post "/notifications/v2/star",                             to: "notifications_v2#mark_as_starred"
  post "/notifications/v2/unstar",                           to: "notifications_v2#mark_as_unstarred"
  post "/notifications/v2/create_custom_inbox",              to: "notifications_v2#create_custom_inbox"
  put  "/notifications/v2/update_custom_inbox",              to: "notifications_v2#update_custom_inbox"
  delete "/notifications/v2/delete_custom_inbox",            to: "notifications_v2#delete_custom_inbox"

  get  "/notifications/beta/recent_notifications_alert",     to: "notifications_v2#recent_notifications_alert"
  get  "/notifications/beta/notifications_exist",            to: "notifications_v2#notifications_exist"
  get  "/notifications/beta/custom_inboxes_dialog",          to: "notifications_v2#custom_inboxes_dialog"
  get  "/notifications/beta/suggestions/repositories",       to: "notifications_v2#suggested_repositories", as: :notifications_beta_suggested_repositories

  get  "/notifications",                                     to: "notifications#index",                      as: :notifications
  get  "/notifications/all",                                 to: "notifications#index",                      as: :all_notifications,                 all:    true
  get  "/notifications/read",                                to: "notifications#index",                      as: :read_notifications,                filter: "read"
  get  "/notifications/participating",                       to: "notifications#index",                      as: :participating_notifications,       filter: "participating"
  get  "/notifications/saved",                               to: "notifications#index",                      as: :saved_notifications,               filter: "saved"
  post "/notifications/mark",                                to: "notifications#mark_as_read",               as: :mark_notifications
  post "/notifications/unmark",                              to: "notifications#mark_as_unread",             as: :mark_notification_as_unread
  post "/notifications/mute",                                to: "notifications#mute",                       as: :mute_notifications
  post "/notifications/unmute",                              to: "notifications#unmute",                     as: :unmute_notifications
  post "notifications/unwatch_repositories",                 to: "notifications#unwatch_repositories",       as: :unwatch_repositories
  post "notifications/unwatch_all",                          to: "notifications#unwatch_all",                as: :unwatch_all
  get  "/notifications/subscription",                        to: "notifications#subscription",               as: :notification_subscription
  get  "/notifications/beacon/:data.gif",                    to: "notifications#beacon",                     as: :notification_beacon
  get  "/notifications/unsubscribe/:data",                   to: "notifications#email_mute_via_list",        as: :email_mute_via_list
  get  "/notifications/unsubscribe-auth/:data",              to: "notifications#email_mute_via_footer",      as: :email_mute_via_footer
  post "/notifications/subscribe",                           to: "notifications#subscribe",                  as: :notifications_subscribe
  post "/notifications/thread",                              to: "notifications#thread_subscribe",           as: :notifications_thread_subscribe
  get  "/notifications/thread_subscription",                 to: "notifications#thread_subscription",        as: :notifications_thread_subscription
  post "/notifications/settings",                            to: "notifications#save_handlers",              as: :save_notification_settings
  post "/notifications/emails",                              to: "notifications#save_email_settings",        as: :save_notification_emails
  post "/notifications/organization_routing",                to: "notifications#update_organization_routing", as: :update_organization_notification_routing
  post "/notifications/own",                                 to: "notifications#toggle_own",                 as: :own_contributions
  post "/notifications/comment_email",                       to: "notifications#toggle_comment",             as: :notifications_comment_email
  post "/notifications/pull_request_review_email",           to: "notifications#toggle_pull_request_review", as: :notifications_pull_request_review_email
  post "/notifications/pull_request_push_email",             to: "notifications#toggle_pull_request_push",   as: :notifications_pull_request_push_email
  get  "/notifications/unsubscribe-vulnerability/:data",     to: "notifications#email_mute_vulnerabilities", as: :email_mute_vulnerabilities
  post "/notifications/vulnerability",                       to: "notifications#toggle_vulnerability",       as: :notifications_vulnerability
  post "/notifications/digest",                              to: "notifications#toggle_digest_subscription", as: :notifications_digest
  post "/notifications/continuous-integration",              to: "notifications#toggle_ci",                  as: :notifications_ci
  get  "/watching",                                          to: "notifications#watching",                   as: :watching
  post "/watching/auto",                                     to: "notifications#toggle_auto",                as: :auto_subscriptions
  post "/watching/repositories",                             to: "notifications#toggle_auto_subscribe_repositories", as: :toggle_auto_subscribe_repositories
  post "/watching/teams",                                    to: "notifications#toggle_auto_subscribe_teams",        as: :toggle_auto_subscribe_teams

  # NotificationSubscriptionsController
  get  "/notifications/subscriptions",                       to: "notification_subscriptions#index",            as: :notification_subscriptions
  post "/notification_subscriptions",                        to: "notification_subscriptions#create",           as: :create_notification_subscription
  post "/notifications/subscriptions/dismiss_notice",        to: "notification_subscriptions#dismiss_notice",   as: :dismiss_notification_subscriptions_notice
  delete "/notifications/subscriptions",                     to: "notification_subscriptions#destroy",          as: :destroy_notification_subscriptions

  # Organization Repositories
  get    "/organizations/:organization_id/repository_imports/new", to: "repository_imports#new", as: :new_organization_repository_import

  org_repos_options = {
    controller: "repositories",
    path:       "/organizations/:organization_id/repositories",
    format:     false,
  }
  scope org_repos_options do
    # Exploded out from map.resources :organizations
    get    "/", action: "index", as: :organization_repositories
    post   "/", action: "create"
    get    "/new", action: "new", as: :new_organization_repository
  end

  # OrganizationsController
  get    "/orgs/:org/dashboard",                            to: "organizations#show",              as: :org_dashboard
  get    "/orgs/:org/news-feed",                            to: "dashboard_feeds#show",            as: :organizations_news_feed
  post   "/organizations/transform",                        to: "organizations#transform",         as: :transform_organizations
  get    "/organizations/transforming",                     to: "organizations#transforming",      as: :transforming_organizations
  post   "/organizations",                                  to: "organizations#create"
  get    "/organizations/new",                              to: "organizations#new"
  get    "/organizations/plan",                             to: "organizations#plan",              as: :org_plan
  post   "/organizations/plan",                             to: "organizations#choose_plan"
  get    "/organizations/enterprise_plan",                  to: "organizations#enterprise_plan",   as: :org_enterprise_plan
  match  "/organizations/signup_billing",                   to: "organizations#signup_billing",    as: :org_signup_billing, via: [:get, :post]
  post   "/organizations/payment",                          to: "organizations#collect_payment",   as: :org_collect_payment
  get    "/organizations/seats",                            to: "organizations#seats",             as: :model_org_seats
  post   "/organizations/check_name",                       to: "organizations#check_name",        as: :organization_check_name
  post   "/organizations/check_billing_email",              to: "organizations#check_billing_email", as: :organization_check_billing_email
  post   "/organizations/check_company_name",               to: "organizations#check_company_name", as: :organization_check_company_name
  get    "/organizations/:organization_id/ajax_your_repos", to: "organizations#ajax_your_repos",   as: :ajax_your_repos_organization
  get    "/organizations/:organization_id/invite",          to: "organizations#invite",            as: :invite_organization
  post   "/organizations/:organization_id/ignore_upgrade",  to: "organizations#ignore_upgrade",    as: :ignore_upgrade_organization
  put    "/organizations/:organization_id",                 to: "organizations#update",            as: :org_update
  patch  "/organizations/:organization_id/tos",             to: "organizations#tos",               as: :org_tos
  put    "/organizations/:organization_id/projects_enabled", to: "organizations#update_projects_enabled", as: :organization_projects_enabled
  put    "/organizations/:organization_id/update_team_discussions_enabled", to: "organizations#update_team_discussions_enabled", as: :update_team_discussions_enabled
  put    "/organizations/:organization_id/default_repository_permission", to: "organizations#update_default_repository_permission", as: :organization_default_repository_permission
  put    "/organizations/:organization_id/members_can_delete_repositories", to: "organizations#update_members_can_delete_repositories", as: :organization_members_can_delete_repositories
  put    "/organizations/:organization_id/members_can_delete_issues", to: "organizations#update_members_can_delete_issues", as: :organization_members_can_delete_issues
  put    "/organizations/:organization_id/display_commenter_full_name", to: "organizations#update_display_commenter_full_name", as: :organization_display_commenter_full_name
  put    "/organizations/:organization_id/readers_can_create_discussions", to: "organizations#update_readers_can_create_discussions", as: :organization_readers_can_create_discussions
  put    "/organizations/:organization_id/members_can_update_protected_branches", to: "organizations#update_members_can_update_protected_branches", as: :organization_members_can_update_protected_branches
  put    "/organizations/:organization_id/members_can_change_repo_visibility", to: "organizations#update_members_can_change_repo_visibility", as: :organization_members_can_change_repo_visibility
  put    "/organizations/:organization_id/members_can_create_teams", to: "organizations#update_members_can_create_teams", as: :organization_members_can_create_teams
  put    "/organizations/:organization_id/members_can_invite_outside_collaborators", to: "organizations#update_members_can_invite_outside_collaborators", as: :organization_members_can_invite_outside_collaborators
  put    "/organizations/:organization_id/members_can_view_dependency_insights", to: "organizations#update_members_can_view_dependency_insights", as: :organization_members_can_view_dependency_insights
  put    "/organizations/:organization_id/update_package_creation_permission", to: "organizations#update_package_creation_permission", as: :organization_update_package_creation_permission
  put    "/organizations/:organization_id/action_execution_capability", to: "organizations#update_action_execution_capability", as: :organization_update_action_execution_capability
  put    "/organizations/:organization_id/fork_pr_workflows_policy", to: "organizations#update_fork_pr_workflows_policy", as: :update_organization_fork_pr_workflows_policy
  put    "/organizations/:organization_id/settings",        to: "organizations#update_settings", as: :organization_settings
  delete "/organizations/:organization_id",                 to: "organizations#destroy"
  post   "/organizations/:organization_id/leave",           to: "organizations#leave",             as: :leave_org
  put    "/organizations/:organization_id/rename",          to: "organizations#rename",            as: :org_rename
  get    "/account/organizations/new",                      to: "organizations#new",               as: :new_organization
  get    "/account/organizations/plan",                     to: "organizations#plan"
  post   "/account/organizations/plan",                     to: "organizations#choose_plan"
  get    "/account/organizations/new/:coupon",              to: "organizations#new"
  get    "/account/organizations/access_list_members",      to: "organizations#access_list_members"

  get    "/organizations/:organization_id/:login.private",  to: "atom_feeds#org_show",    as: :private_org_feed


  # OrganizationsController legacy URL redirects
  get "/organizations/:organization_id/settings",                  to: redirect("/organizations/%{organization_id}/settings/profile")
  get "/organizations/:organization_id/edit",                      to: redirect("/organizations/%{organization_id}/settings/profile")
  get "/organizations/:organization_id",                           to: redirect("/orgs/%{organization_id}/dashboard")
  get "/organizations/:organization_id/settings/billing/per_seat", to: redirect("/account/upgrade?org=%{organization_id}&target=organization")

  # OrganizationsController environment specific routes
  if GitHub.ldap_sync_enabled?
    get "/organizations/:organization_id/import", to: "organizations#import", as: :import_organization
  end

  # Payment methods (org and user)
  get     "/settings/billing/payment_method", to: "payment_method#zuora_payment_show", as: :zuora_payment
  get     "/settings/billing/payment/signature", to: "payment_method#zuora_payment_page_signature", as: :zuora_payment_page_signature
  get     "/settings/billing/payment", to: "payment_method#show",    as: :payment
  get     "/settings/billing/payment_modal", to: "payment_method#show_modal",    as: :payment_modal
  put     "/settings/billing/payment", to: "payment_method#update"
  delete  "/settings/billing/payment", to: "payment_method#destroy"

  get     "/organizations/:organization_id/settings/billing/payment", to: "payment_method#show",   as: :org_payment
  get     "/organizations/:organization_id/settings/billing/payment_modal", to: "payment_method#show_modal",   as: :org_payment_modal
  put     "/organizations/:organization_id/settings/billing/payment", to: "payment_method#update"
  delete  "/organizations/:organization_id/settings/billing/payment", to: "payment_method#destroy"

  get     "/organizations/:organization_id/settings/billing/seats",        to: "seats#show",         as: :org_seats
  put     "/organizations/:organization_id/settings/billing/seats",        to: "seats#update"
  get     "/organizations/:organization_id/settings/billing/remove_seats", to: "seats#remove_seats", as: :remove_org_seats
  put     "/organizations/:organization_id/settings/billing/switch",       to: "seats#switch",       as: :org_switch_to_seats
  delete  "/organizations/:organization_id/settings/billing/seats",        to: "seats#cancel",       as: :cancel_org_seats

  get     "/organizations/:organization_id/settings/billing/invoices/:invoice_number", to: "invoices#show", as: :show_invoice
  get     "/organizations/:organization_id/settings/billing/invoices/:invoice_number/pay", to: "invoices#pay", as: :pay_invoice
  get     "/organizations/:organization_id/settings/billing/invoices/:invoice_number/payment_method", to: "invoices#payment_method", as: :invoice_payment_method
  get     "/organizations/:organization_id/settings/billing/invoices/:invoice_number/signature", to: "invoices#payment_page_signature", as: :invoice_signature

  get    "/organizations/:organization_id/settings/ssh_certificate_authorities/new", to: "ssh_certificate_authorities#new",     as: :ssh_certificate_authorities_new_organization
  post   "/organizations/:organization_id/settings/ssh_certificate_authorities",     to: "ssh_certificate_authorities#create",  as: :ssh_certificate_authorities_organization
  delete "/organizations/:organization_id/settings/ssh_certificate_authorities/:id", to: "ssh_certificate_authorities#destroy", as: :ssh_certificate_authority_organization

  # Packages (org and user)

  unless GitHub.enterprise?
    scope "/:owner_type/:owner_id", constraints: { owner_type: /users|orgs/, owner_id: USERID_REGEX } do
      get    "/packages", to: "registry_two/packages#index", as: :packages_two
      get    "/packages/:ecosystem", to: "registry_two/packages#ecosystem_index"

      scope "/packages/:ecosystem/:name/settings" do
        get    "/", to: "registry_two/package_settings#show", as: :package_settings
        get    "/collaborator_suggestions", to: "registry_two/package_settings#collaborator_suggestions", as: :package_collaborator_suggestions
        get    "/toolbar_actions", to: "registry_two/package_settings#toolbar_actions",  as: :package_members_toolbar_actions

        post   "/add_collaborator", to: "registry_two/package_settings#add_collaborator", as: :add_package_collaborator
        post   "/update_collaborator", to: "registry_two/package_settings#update_collaborator", as: :update_package_collaborator
        post   "/bulk_update_collaborators", to: "registry_two/package_settings#bulk_update_collaborators", as: :bulk_update_package_collaborators
        post   "/change_visibility", to: "registry_two/package_settings#change_visibility", as: :change_package_visibility

        delete "/remove_collaborator", to: "registry_two/package_settings#remove_collaborator", as: :remove_package_collaborator
        delete "/delete_package", to: "registry_two/package_settings#delete_package", as: :delete_package
      end

      get    "/packages/:ecosystem/:name", to: "registry_two/packages#show"
      get    "/packages/:ecosystem/:name/versions", to: "registry_two/package_versions#show", as: :package_versions_two
      get    "/packages/:ecosystem/:name/:version", to: "registry_two/packages#show", as: :package_two

      delete "/packages/:ecosystem/:name/versions/:version/delete_package_version", to: "registry_two/package_versions#delete_package_version", as: :delete_package_version, constraints: { version: /.+/ }
    end
  end

  resources :organization_ip_whitelist_entries,
    path: "/organizations/:organization_id/settings/ip_whitelist_entries",
    only: %w(new create edit update destroy),
    controller: "ip_whitelist_entries"

  ##
  # Teams
  teams_options = {
    controller: "teams",
    path:       "/organizations/:organization_id/teams",
    format:     false,
  }

  # the collection of teams
  scope teams_options do
    get  "/",    action: "index"
    get  "/new", action: "new"

    # a specific team
    scope path: "/:team_id", team_id: /\d+|[\w\.\-]+/i do
      get    "/", action: "show"
    end
  end

  scope path: "/organizations/:org", format: false do
    get   "/dashboard/pulls",       to: "issues#redirect_to_scoped_org_dashboard", pulls_only: true, as: nil
    get   "/dashboard/issues",      to: "issues#redirect_to_scoped_org_dashboard", as: nil
  end

  ##
  # Dashboard
  get           "/dashboard/pulls",         to: "issues#redirect_to_scoped_org_dashboard", pulls_only: true
  get           "/dashboard/issues",        to: "issues#redirect_to_scoped_org_dashboard"

  # DashboardController
  get    "/dashboard",                  to: "dashboard#index",            as: :dashboard
  get    "/",                           to: "dashboard#index",            as: :home
  get    "/dashboard/top_repositories", to: "dashboard#top_repositories", as: :dashboard_top_repositories
  get    "/dashboard/ajax_context_list", to: "dashboard#ajax_context_list", as: :dashboard_ajax_context_list
  get    "/dashboard/discover",         to: redirect("/explore"), as: :dashboard_discover
  get    "/dashboard/recent-activity",  to: "dashboard#recent_activity", as: :dashboard_recent_activity
  get    "/dashboard/index/:page",      to: "dashboard#index"
  post   "/dashboard/dismiss_bootcamp", to: "dashboard#dismiss_bootcamp", as: :dismiss_bootcamp
  delete "/dashboard/dismiss_bootcamp", to: "dashboard#dismiss_bootcamp"
  get    "/dashboard/ajax_your_teams",  to: "dashboard#ajax_your_teams"
  get    "/dashboard/ajax_repositories", to: "dashboard#ajax_repositories"
  post   "/ignore_upgrade",             to: "dashboard#ignore_upgrade",    as: :ignore_upgrade
  get    "/dashboard/logged_out",       to: "dashboard#logged_out"

  get    "/dashboard-feed", to: "dashboard_feeds#show", as: :dashboard_news_feed

  get "/:login.private.actor", to: "atom_feeds#actor_show"
  get "/:login.private",       to: "atom_feeds#user_show", as: :private_feed

  get "/login/oauth/authorize",      to: "oauth#request_access", as: :oauth_request, format: false
  post "/login/oauth/authorize",     to: "oauth#authorize", as: :oauth_authorize, format: false
  match "/login/oauth/access_token", to: "oauth#access_token", as: :oauth_access_token, format: false, via: [:get, :post]
  get "/login/oauth/success",        to: "oauth#success", as: :oauth_success, format: false
  post "/login/device/code",         to: "oauth#request_device_authorization", as: :device_authorization_request, format: false

  get  "/login/device",              to: "device_authorization#user_code_prompt", as: :user_code_prompt
  post "/login/device/confirmation", to: "device_authorization#request_access",   as: :device_request
  post "/login/device/authorize",    to: "device_authorization#authorize",        as: :device_authorize
  get  "/login/device/success",      to: "device_authorization#success",          as: :device_success
  get  "/login/device/failure",      to: "device_authorization#failure",          as: :device_failure

  get "/stars", to: "stars#index", format: false
  get "/stars/:user", to: "stars#user_stars", as: :users_stars, format: false
  get "/stars/:user/repositories", to: "stars#user_repositories", as: :users_stars_repositories, format: false
  get "/stars/:user/topics", to: "stars#user_topics", as: :users_stars_topics, format: false

  ##
  # SessionsController
  get  "/session",                            to: redirect("/login")
  post "/session",                            to: "sessions#create"
  get  "/logout",                             to: "sessions#confirm_logout",               as: :confirm_logout
  post "/logout",                             to: "sessions#destroy",                      as: :kill_session
  get  "/login",                              to: "sessions#new",                          as: :login
  get  "/suspended",                          to: "sessions#suspended",                    as: :suspended
  get  "/sessions/two-factor",                to: "sessions#two_factor_prompt",            as: :two_factor_prompt
  post "/sessions/two-factor",                to: "sessions#two_factor_authenticate",      as: :two_factor
  get  "/sessions/two-factor/security-key",   to: "sessions#u2f_prompt",                   as: :u2f_prompt
  post "/sessions/two-factor/security-key",   to: "sessions#u2f_authenticate",             as: :u2f_authenticate
  get  "/sessions/two-factor/recovery",       to: "sessions#two_factor_recover_prompt",    as: :two_factor_recover_prompt
  post "/sessions/two-factor/recovery",       to: "sessions#two_factor_recover",           as: :two_factor_recover
  post "/sessions/two-factor/resend",         to: "sessions#resend_two_factor_sms",        as: :two_factor_resend
  post "/sessions/two-factor/send-fallback",  to: "sessions#send_two_factor_fallback_sms", as: :two_factor_send_fallback
  get  "/sessions/sudo_modal",                to: "sessions#sudo_modal",                   as: :sudo_modal
  post "/sessions/sudo",                      to: "sessions#sudo",                         as: :sudo
  get  "/sessions/in_sudo",                   to: "sessions#in_sudo",                      as: :in_sudo
  get  "/sessions/kill_sudo",                 to: "sessions#kill_sudo",                    as: :kill_sudo

  # TwoFactorRecoveryRequestController
  unless GitHub.enterprise?
    post  "/sessions/recovery/start",     to: "two_factor_recovery_request#start",             as: :two_factor_recovery_request_start
    get  "/sessions/recovery",            to: "two_factor_recovery_request#prompt",            as: :two_factor_recovery_request
    post  "/sessions/recovery/send",      to: "two_factor_recovery_request#send_otp",          as: :two_factor_recovery_request_send_otp
    post  "/sessions/recovery/otp",       to: "two_factor_recovery_request#verify_otp",        as: :two_factor_recovery_request_verify_otp
    post  "/sessions/recovery/device",    to: "two_factor_recovery_request#verify_device",     as: :two_factor_recovery_request_verify_device
    get  "/sessions/recovery/ssh",        to: "two_factor_recovery_request#enter_ssh_key",     as: :two_factor_recovery_request_enter_ssh_key
    post  "/sessions/recovery/ssh",       to: "two_factor_recovery_request#verify_ssh_key",    as: :two_factor_recovery_request_verify_ssh_key
    get  "/sessions/recovery/token",      to: "two_factor_recovery_request#enter_token",       as: :two_factor_recovery_request_enter_token
    post  "/sessions/recovery/token",     to: "two_factor_recovery_request#verify_token",      as: :two_factor_recovery_request_verify_token

    get "/sessions/recovery/:id/abort/:token",   to: "two_factor_recovery_request#abort",         as: :two_factor_recovery_abort_request
    post "/sessions/recovery/:id/abort/:token",  to: "two_factor_recovery_request#confirm_abort", as: :two_factor_recovery_confirm_abort_request

    get  "/sessions/recovery/:id/continue/:token",  to: "two_factor_recovery_request#continue",          as: :two_factor_recovery_continue
    post  "/sessions/recovery/:id/continue/:token", to: "two_factor_recovery_request#confirm_continue",  as: :two_factor_recovery_completed
  end

  if GitHub.sign_in_analysis_enabled? || Rails.env.test?
    get  "/sessions/verified-device", to: "sessions#verified_device_prompt", as: :verified_device_prompt
    post "/sessions/verified-device", to: "sessions#verified_device_authenticate", as: :verified_device_authenticate
    post "/sessions/verified-device/resend", to: "sessions#resend_verification_email", as: :resend_verification_email
  end

  # nginx auth_request endpoint for Enterprise2 private mode
  if GitHub.private_mode_enabled?
    get "/sessions/_auth_request_bounce", to: "sessions#auth_request_bounce", as: :auth_request_bounce
  end

  if GitHub.enterprise?
    # CAS, GitHub OAuth
    #
    # OmniAuth needs the prefix path `/auth/:provider` to 404 to initiate the auth request
    # for external auth providers (CAS)
    get  "/auth/:provider",          to: "sessions#external_provider"
    get  "/auth/:provider/callback", to: "sessions#create"  # CAS
    post "/auth/:provider/callback", to: "sessions#create"
    get  "/auth/failure",            to: redirect("/login")
  end

  # SAML
  #
  #  /saml/consume  - callback endpoint for idP to POST authentication requests
  post "/saml/consume",  to: "sessions#create"
  get  "/saml/metadata", to: "saml#metadata"

  ##
  # UserSessionsController
  delete "/sessions/:id/revoke", to: "settings/user_sessions#revoke", as: :user_sessions_revoke

  ##
  # PasswordResetsController
  get  "/password_reset",                     to: "password_resets#new",       as: :password_reset
  post "/password_reset",                     to: "password_resets#create",    as: :create_password_reset
  get  "/password_reset/:token",              to: "password_resets#edit",      as: :edit_password_reset
  post "/password_reset/:token/2fa",          to: "password_resets#check_otp", as: :check_password_reset_otp
  post "/password_reset/:token/2fa/fallback", to: "password_resets#fallback",  as: :password_reset_fallback
  put  "/password_reset/:token",              to: "password_resets#update",    as: :update_password_reset

  ##
  # Explore
  get "/explore",           to: "explore#index",            as: :explore
  get "/explore/email",     to: "explore#email",            as: :explore_email
  get "/explore/subscribe", to: redirect("/explore/email"), as: :explore_subscribe
  get "/discover",          to: redirect("/explore"),       as: :discover_repositories
  put "/discover/dismiss_recommendation", to: "repository_recommendations#dismiss_recommendation", as: :dismiss_repository_recommendation
  put "/discover/restore_recommendation", to: "repository_recommendations#restore_recommendation", as: :restore_repository_recommendation

  if GitHub.enterprise?
    get "/topics", to: redirect("/explore"), as: :topics
    get "/topic", to: redirect("/explore")
    get "/topics/:topic_name", to: redirect("/explore"), as: :topic_show
    get "/topic/:topic_name", to: redirect("/explore")
    get "/topics/:topic/related", to: redirect("/explore"), as: :related_topic
  else
    get "/topics", to: "topics#index", as: :topics
    get "/topics/:topic_name", to: "topics#show", as: :topic_show
    get "/topics/:topic/related", to: "topics#related", as: :related_topic

    get "/topic", to: redirect("/topics")
    get "/topic/:topic_name", to: redirect("/topics/%{topic_name}")
  end


  get "/showcases",         to: "showcases#index",   as: :showcases
  get "/showcases/search",  to: "showcases#search",  as: :showcases_search
  get "/showcases/:id",     to: "showcases#show",    as: :showcase_collection
  constraints format: "html" do
    get "/collections",       to: "collections#index", as: :collections
    get "/collections/:slug", to: "collections#show",  as: :collection
  end

  ##
  # Trending
  get "/trending/developers(/:language)", to: "trending#developers", as: :trending_developers, constraints: { language: /[^\/]+/ }
  get "/trending(/:language)",            to: "trending#index",      as: :trending_index, constraints: { language: /[^\/]+/ }

  # Dashboard::OverviewController
  if GitHub.enterprise?
    get "/dashboards/overview",                  to: "dashboards/overview#index",    as: "dashboards_overview"
    get "/dashboards/overview/count/:period",    to: "dashboards/overview#count"
  end

  ##
  # Enterprise Activity Dashboards (restricted to Enterprise at the controller level)
  get "/dashboards/audit-log", to: redirect(path: "/admin/audit-log")

  if GitHub.enterprise?
    get "/humans.txt",     to: redirect("https://github.com/humans.txt")

    get "/about",          to: redirect("https://github.com/about")
    get "/about/:page",    to: redirect("https://github.com/about/%{page}")
  else
    constraints format: "html" do
      get "/humans.txt",               to: redirect("/about")
      get "/about",                    to: "about#index", as: :about
      get "/about/team",               to: redirect("/about")
      get "/about/leadership",         to: "about#leadership"
      get "/about/diversity",          to: "about#diversity"
      get "/about/diversity/report",   to: "about#diversity_report"
      get "/about/milestones",         to: "about#milestones"
      get "/about/careers",            to: "about#careers"
      get "/about/careers/remote",     to: "about#remote"
      get "/careers",                  to: redirect("/about/careers")
      get "/about/jobs",               to: redirect("/about/careers")
      get "/about/internships",        to: redirect("/about/jobs#internships")
      get "/about/mentions",           to: redirect("/about/press")
      get "/about/press/satellite",    to: "about#press_satellite"
      get "/about/press/universe",     to: "about#press_universe"
      get "/about/facts",              to: redirect("/about")
    end

    get "/nonprofit",         to: "site#nonprofit", format: false

    namespace :about do
      resources :press, only: [:index]
    end
  end

  # Dat contact us form
  scope controller: "site", format: false do
    unless GitHub.enterprise?
      get  "/c",       action: "contact", as: nil
      get  "/C",       action: "contact", as: nil
    end
    get  "/contact", action: "contact", as: "contact"
    get  "/contact/default", to: redirect("/contact"), format: false
    get  "/contact/:flavor", action: "contact", flavor: Regexp.union(GitHub.contact_form_flavors.keys), as: "flavored_contact"
    put  "/contact", action: "send_contact"
    post "/contact", action: "send_contact"
    get  "/CONTACT", action: "contact", as: nil
    get  "/support", action: "contact"
  end

  get           "/site",                   to: "site#index", format: false if Rails.env == "test"
  get           "/home",                   to: "site#index"

  # signup controller
  get   "/join",                  to: "signup#join",           as: :signup
  post  "/join",                  to: "signup#create_account"
  get   "/join/plan",             to: "signup#plan",           as: :signup_plan
  post  "/join/plan",             to: "signup#process_plan_selection"
  get   "/join/trial",            to: "signup#enterprise_trial_redirect", as: :enterprise_trial_redirect
  post  "/join/select_plan",      to: "signup#select_plan",    as: :select_plan
  post  "/signup_check/username", to: "signup#username_check", as: :username_check
  post  "/signup_check/email",    to: "signup#email_check",    as: :email_check
  get   "/join/get-started",      to: "signup#get_started",    as: :get_started

  # redesigned billing settings
  if GitHub.billing_enabled?
    get "/join/billing",          to: "signup#billing",        as: :signup_billing
    get "/join/signature",        to: "signup#zuora_payment_page_signature",        as: :signup_signature
  end

  # user identification survey
  get   "/join/customize",              to: "user_identification_survey_response#new",          as: :signup_customize
  post  "/join/customize",              to: "user_identification_survey_response#create",       as: :user_identification_survey_response
  get   "/join/customize/autocomplete", to: "user_identification_survey_response#autocomplete", as: :user_identification_survey_response_autocomplete

  # Getting started interstitial
  get "/getting-started", to: redirect(GitHub.help_url)

  # Old signup urls
  get "/signup",             to: redirect("/join")
  get "/signup/:plan",       to: redirect("/join")
  get "/signup/:plan/:code", to: redirect("/join")

  get "/account/organizations", to: "users#organizations", as: :user_organizations, format: false

  post "/account/ignore_user/:login",   to: "users#ignore_user",   as: :ignore_user, format: false
  post "/account/unignore_user/:login", to: "users#unignore_user", as: :unignore_user, format: false

  get "/timeline(.:format)", to: "events#index", as: :timeline_feed, format: ["atom", "json"]

  if GitHub.security_advisories_enabled?
    get "/security-advisories(.:format)", to: "security_advisories#index", as: :security_advisory_feed, format: ["atom"]
  end

  if GitHub.global_advisories_enabled?
    constraints(id: AdvisoryDB.valid_ghsa_id_pattern) do
      get "/advisories",     to: "global_advisories#index", as: :global_advisories
      get "/advisories/:id", to: "global_advisories#show", as: :global_advisory
      get "/advisories/:id/hovercard", to: "hovercards/advisories#show", as: :advisory_hovercard
    end
  end

  post  "/preview", to: "comment_preview#show", as: :preview, format: false

  get "/account",          controller: "users", action: "edit", as: "account", format: false, tab: "profile"
  get "/account/:tab",     controller: "users", action: "edit", as: "account_tab", format: false, tab: /admin|email|ssh|job|connections/
  put "/account",          controller: "users", action: "update", format: false
  put "/account/password", controller: "users", action: "change_password", as: "change_password", format: false

  get  "/account/billing",            to: "users#billing",          as: :billing
  get  "/account/billing/:coupon",    to: "users#billing"
  get  "/account/notifications",      to: redirect("/settings/notifications")
  get  "/account/repositories",       to: redirect("/")
  post "/account/leave_repo/:repo",   to: "account#leave_repo",     as: :leave_repo
  put  "/account/read_broadcast/:id", to: "account#read_broadcast", as: :read_broadcast

  get "/account/upgrade",       to: "users#billing",       as: :plan_upgrade,       format: false

  # settings controller
  get    "/settings",                                                                           to: redirect("/settings/profile")
  get    "/settings/profile",                                                                   to: "settings#user_profile",                                  as: :settings_user_profile
  put    "/settings/unset_profile_email",                                                       to: "settings#unset_profile_email",                           as: :settings_unset_profile_email
  put    "/settings/reset_avatar",                                                              to: "settings#reset_avatar",                                  as: :settings_reset_avatar
  get    "/settings/user_has_gravatar",                                                         to: "settings#user_has_gravatar",                             as: :settings_user_has_gravatar
  get    "/settings/admin",                                                                     to: "settings#user_admin",                                    as: :settings_user_admin
  get    "/settings/security-log",                                                              to: "settings/audit_log#index",                               as: :settings_user_audit_log
  get    "/settings/security-log/suggestions",                                                  to: "settings/audit_log#suggestions",                         as: :settings_user_audit_log_suggestions
  get    "/settings/security-log/export",                                                       to: "settings/audit_log_export#show",                         as: :settings_user_audit_log_export
  post   "/settings/security-log/export(.:format)",                                             to: "settings/audit_log_export#create"
  get    "/settings/emails",                                                                    to: "settings#user_emails",                                   as: :settings_user_emails
  get    "/settings/notifications",                                                             to: "settings#user_notifications",                            as: :settings_user_notifications
  get    "/settings/notifications/organization_routing",                                        to: "settings#user_notification_org_routing",                 as: :settings_user_notifications_org_routing
  get    "/settings/billing",                                                                   to: "settings#user_billing",                                  as: :settings_user_billing
  get    "/settings/billing/:tab",                                                              to: "settings#user_billing",                                  as: :settings_user_billing_tab, tab: /payment_information|cost_management/
  post   "/settings/billing/spending_limit",                                                    to: "settings#spending_limit",                                as: :settings_user_spending_limit
  get    "/settings/ssh/new",                                                                   to: "settings#new_user_ssh",                                  as: :settings_user_new_ssh
  get    "/settings/gpg/new",                                                                   to: "settings#new_user_gpg",                                  as: :settings_user_new_gpg
  get    "/settings/ssh(/:id)",                                                                 to: "settings#user_ssh",                                      as: :settings_user_ssh,                                      id: /\d+/
  get    "/settings/keys(/:id)",                                                                to: "settings#user_keys",                                     as: :settings_user_keys,                                     id: /\d+/
  get    "/settings/renaming",                                                                  to: "settings#renaming",                                      as: :settings_user_renaming
  get    "/settings/two_factor_authentication/intro",                                           to: "settings#two_factor_authentication_intro",               as: :settings_user_two_factor_authentication_intro
  post   "/settings/two_factor_authentication/initiate",                                        to: "settings#two_factor_authentication_initiate",            as: :settings_user_two_factor_authentication_initiate
  get    "/settings/two_factor_authentication/recovery_codes",                                  to: "settings#two_factor_authentication_recovery_codes",      as: :settings_user_two_factor_authentication_recovery_codes
  get    "/settings/two_factor_authentication/verify",                                          to: "settings#two_factor_authentication_verify",              as: :settings_user_two_factor_authentication_verify
  post   "/settings/two_factor_authentication/send_sms",                                        to: "settings#send_two_factor_sms",                           as: :settings_user_two_factor_sms
  get    "/settings/recovery/print",                                                            to: "settings#recovery_print",                                as: :settings_user_recovery_print
  post   "/settings/recovery/download",                                                         to: "settings#recovery_download",                             as: :settings_user_recovery_download
  post   "/settings/two_factor_authentication/enable",                                          to: "settings#two_factor_authentication_enable",              as: :settings_user_two_factor_authentication_enable
  get    "/settings/two_factor_authentication/configure",                                       to: "settings#two_factor_authentication",                     as: :settings_user_two_factor_authentication_configuration
  post   "/settings/two_factor_authentication/disable",                                         to: "settings#two_factor_authentication_disable",             as: :settings_user_two_factor_authentication_disable
  post   "/settings/two_factor_authentication/backup_number",                                   to: "settings#add_two_factor_sms_backup",                     as: :settings_user_two_factor_authentication_add_backup
  delete "/settings/two_factor_authentication/backup_number",                                   to: "settings#destroy_two_factor_sms_backup",                 as: :settings_user_two_factor_authentication_destroy_backup
  put    "/settings/security_checkup",                                                          to: "settings#security_checkup",                              as: :settings_security_checkup
  get    "/settings/repositories",                                                              to: "settings#user_repositories",                             as: :settings_user_repositories
  get    "/settings/deleted_repositories",                                                      to: "settings#user_deleted_repositories",                     as: :settings_user_deleted_repositories
  get    "/settings/restore_repo/:id",                                                          to: "repos/restore#restore_status",                           as: :settings_restore_repo_status
  post   "/settings/restore_repo/:id",                                                          to: "repos/restore#restore",                                  as: :settings_restore_repo
  get    "/settings/restore_repo/:id/partial",                                                  to: "repos/restore#restore_partial",                          as: :settings_restore_repo_partial
  put    "/settings/default_branch",                                                            to: "settings/default_branch_names#update",                   as: :settings_user_default_branch
  post   "/settings/default_branch/check_name",                                                 to: "settings/default_branch_names#check_name",               as: :settings_user_default_branch_check_name
  get    "/settings/organizations",                                                             to: "settings#user_organizations",                            as: :settings_user_organizations
  get    "/settings/enterprises",                                                               to: "settings#user_enterprises",                              as: :settings_user_enterprises
  get    "/settings/ssh/audit/:key/policy",                                                     to: "settings#user_ssh_unknown_origin_audit",                 as: :settings_key_audit_key_of_unknown_origin,              key: /\d+/
  get    "/settings/security",                                                                  to: "settings#user_security",                                 as: :settings_user_security
  get    "/.well-known/change-password",                                                        to: redirect(Rails.application.routes.url_helpers.settings_user_security_path)
  get    "/settings/security_analysis",                                                         to: "settings#user_security_analysis",                        as: :settings_user_security_analysis
  put    "/settings/security_analysis/update",                                                  to: "users/security_analysis#update",                         as: :settings_user_security_analysis_update
  get    "/settings/sessions/:id",                                                              to: "settings/user_sessions#show",                                     as: :settings_show_session
  get    "/settings/sessions/incomplete/:authentication_record_id",                             to: "settings/user_sessions#show",                                     as: :settings_show_incomplete_auth_record
  get    "/settings/sessions/authentications/:authentication_record_id",                        to: "settings/user_sessions#show",                                     as: :settings_show_auth_record
  put    "/settings/privacy/toggle_third_party_analytics"
  put    "/settings/secure_development/update_automated_security_fixes_opt_out",                to: "settings/secure_development#update_automated_security_fixes_opt_out", as: :settings_secure_development_update_automated_security_fixes_opt_out
  get    "/settings/blocked_users",                                                             to: "settings#user_blocks",                                   as: :settings_user_blocks
  put    "/settings/toggle_show_blocked_repo_contributors",                                     to: "settings#toggle_show_blocked_repo_contributors",         as: :settings_user_toggle_show_blocked_repo_contributors
  get    "/settings/user_blocks/suggestions",                                                   to: "users#suggestions",                                      as: :user_blocks_suggestions
  post   "/settings/user_blocks",                                                               to: "users#ignore_user",                                      as: :create_user_block
  delete "/settings/user_blocks",                                                               to: "users#unignore_user",                                    as: :destroy_user_block
  get    "/organizations/:organization_id/settings/profile",                                    to: "orgs/settings#profile",                                  as: :settings_org_profile

  get    "/organizations/:organization_id/settings/actions",                                    to: "orgs/actions_settings#index",                            as: :settings_org_actions
  get    "/organizations/:organization_id/settings/actions/list_runners",                       to: "orgs/actions_settings#list_runners",                     as: :settings_org_actions_list_runners
  get    "/organizations/:organization_id/settings/actions/add-new-runner",                     to: "orgs/actions_settings#add_runner",                       as: :settings_org_actions_add_runner
  get    "/organizations/:organization_id/settings/actions/add-new-runner-instructions",        to: "orgs/actions_settings#add_runner_instructions",          as: :settings_org_actions_add_runner_instructions
  get    "/organizations/:organization_id/settings/actions/delete_runner_modal/:id",            to: "orgs/actions_settings#delete_runner_modal",              as: :settings_org_actions_delete_runner_modal
  get    "/organizations/:organization_id/settings/actions/repository_items",                   to: "orgs/actions_settings/repository_items#index",           as: :settings_org_actions_repository_items
  delete "/organizations/:organization_id/settings/actions/runners/:id",                        to: "orgs/actions_settings#delete_runner",                    as: :settings_org_actions_delete_runner
  get    "/organizations/:organization_id/settings/actions/bulk_actions",                       to: "orgs/actions_settings#bulk_actions",                     as: :org_runner_bulk_actions
  get    "/organizations/:organization_id/settings/actions/labels",                             to: "orgs/actions_settings/runner_labels#index",              as: :settings_org_actions_labels
  post   "/organizations/:organization_id/settings/actions/label",                              to: "orgs/actions_settings/runner_labels#create",             as: :create_org_runner_label
  put    "/organizations/:organization_id/settings/actions/labels",                             to: "orgs/actions_settings/runner_labels#update",             as: :update_org_runner_labels
  get    "/organizations/:organization_id/settings/actions/bulk_labels",                        to: "orgs/actions_settings/runner_labels#bulk",               as: :org_runner_bulk_labels
  put    "/organizations/:organization_id/settings/actions/bulk_labels",                        to: "orgs/actions_settings/runner_labels#bulk_update",        as: :org_runner_bulk_update_labels
  put    "/organizations/:organization_id/settings/actions/policies",                           to: "orgs/actions_settings/policies#update",                  as: :org_actions_policies
  get    "/organizations/:organization_id/settings/actions/repo_policies",                      to: "orgs/actions_settings/policies#repo_dialog",             as: :org_actions_repo_dialog
  put    "/organizations/:organization_id/settings/actions/repo_policies",                      to: "orgs/actions_settings/policies#update_repos",            as: :org_actions_repo_policies
  get    "/organizations/:organization_id/settings/actions/runner_policies",                    to: "orgs/actions_settings/runner_policies#index",            as: :org_actions_runner_policies
  put    "/organizations/:organization_id/settings/actions/runner_policies",                    to: "orgs/actions_settings/runner_policies#update",           as: :org_actions_update_runner_policies

  post   "/organizations/:organization_id/settings/actions/runner-groups",                      to: "orgs/actions_settings/runner_groups#create",                as: :settings_org_actions_create_runner_group
  get    "/organizations/:organization_id/settings/actions/runner-groups/targets",              to: "orgs/actions_settings/runner_groups#show_selected_targets", as: :settings_org_actions_runner_group_targets
  get    "/organizations/:organization_id/settings/actions/runner-groups/bulk",                 to: "orgs/actions_settings/runner_groups#bulk_actions",          as: :settings_org_actions_runner_group_bulk_actions
  put    "/organizations/:organization_id/settings/actions/runner-groups/runners",              to: "orgs/actions_settings/runner_groups#update_runners",        as: :settings_org_actions_update_runner_group_runners
  get    "/organizations/:organization_id/settings/actions/runner-groups/menu",                 to: "orgs/actions_settings/runner_groups#show_menu",             as: :settings_org_actions_runner_groups_menu
  put    "/organizations/:organization_id/settings/actions/runner-groups/:id",                  to: "orgs/actions_settings/runner_groups#update",                as: :settings_org_actions_update_runner_group
  delete "/organizations/:organization_id/settings/actions/runner-groups/:id",                  to: "orgs/actions_settings/runner_groups#destroy",               as: :settings_org_actions_delete_runner_group

  get    "/organizations/:organization_id/settings/codespaces",                                 to: "orgs/codespaces_settings#index",                         as: :settings_org_codespaces
  put    "/organizations/:organization_id/settings/codespaces/toggle_feature",                  to: "orgs/codespaces_settings#toggle_feature",                as: :settings_org_codespaces_toggle_feature
  post   "/organizations/:organization_id/settings/codespaces/user",                            to: "orgs/codespaces_settings#grant_access",                  as: :settings_org_codespaces_grant_access
  delete "/organizations/:organization_id/settings/codespaces/user",                            to: "orgs/codespaces_settings#revoke_access",                 as: :settings_org_codespaces_revoke_access
  get    "/organizations/:organization_id/settings/codespaces/suggestions",                     to: "orgs/codespaces_settings#suggestions",                   as: :settings_org_codespaces_suggestions

  get    "/organizations/:organization_id/settings/user_blocks",                                to: "orgs/settings#user_blocks",                              as: :settings_org_block_users
  get    "/organizations/:organization_id/settings/owners",                                     to: "orgs/settings#owners",                                   as: :settings_org_owners
  get    "/organizations/:organization_id/settings/billing",                                    to: "orgs/settings#billing",                                  as: :settings_org_billing
  get    "/organizations/:organization_id/settings/billing/:tab",                               to: "orgs/settings#billing",                                  as: :settings_org_billing_tab, tab: /payment_information|cost_management/
  post   "/organizations/:organization_id/settings/spending_limit",                             to: "orgs/settings#spending_limit",                           as: :settings_org_spending_limit

  get    "/organizations/:organization_id/settings/secrets",                                    to: "orgs/secrets_settings#index",                            as: :settings_org_secrets
  get    "/organizations/:organization_id/settings/secrets/new",                                to: "orgs/secrets_settings#new_secret",                       as: :settings_org_secrets_new_secret
  post   "/organizations/:organization_id/settings/secrets/new",                                to: "orgs/secrets_settings#create_secret",                    as: :settings_org_secrets_create_secret
  delete "/organizations/:organization_id/settings/secrets/:name",                              to: "orgs/secrets_settings#remove_secret",                    as: :settings_org_secrets_remove_secret
  get    "/organizations/:organization_id/settings/secrets/:name/delete",                       to: "orgs/secrets_settings#remove_secret_partial",            as: :settings_org_secrets_remove_secret_partial
  get    "/organizations/:organization_id/settings/secrets/:name",                              to: "orgs/secrets_settings#update_secret_page",               as: :settings_org_secrets_update_secret_page
  put    "/organizations/:organization_id/settings/secrets/:name",                              to: "orgs/secrets_settings#update_secret",                    as: :settings_org_secrets_update_secret

  get    "/organizations/:organization_id/settings/security",                                   to: "orgs/security_settings#index",                           as: :settings_org_security
  post   "/organizations/:organization_id/settings/security/ssh_cert_requirement",              to: "orgs/security_settings#ssh_cert_requirement",            as: :settings_org_security_ssh_cert_requirement
  put    "/organizations/:organization_id/settings/two_factor_enforcement",                     to: "orgs/two_factor_enforcements#update",                    as: :settings_org_two_factor_enforcement_update
  get    "/organizations/:organization_id/settings/two_factor_enforcement_status",              to: "orgs/two_factor_enforcements#show",                      as: :settings_org_two_factor_enforcement_status
  put    "/organizations/:organization_id/settings/saml_provider",                              to: "orgs/saml_provider#update",                              as: :settings_org_saml_provider
  delete "/organizations/:organization_id/settings/saml_provider",                              to: "orgs/saml_provider#delete"
  get    "/organizations/:organization_id/settings/saml_provider/recovery_codes",               to: "orgs/saml_provider#recovery_codes",                      as: :settings_org_saml_provider_recovery_codes
  put    "/organizations/:organization_id/settings/saml_provider/regenerate_recovery_codes",    to: "orgs/saml_provider#regenerate_recovery_codes",           as: :settings_org_saml_provider_regenerate_recovery_codes
  post   "/organizations/:organization_id/settings/saml_provider/recovery_codes/download",      to: "orgs/saml_provider#download_recovery_codes",             as: :settings_org_saml_provider_download_recovery_codes
  get    "/organizations/:organization_id/settings/saml_provider/recovery_codes/print",         to: "orgs/saml_provider#print_recovery_codes",                as: :settings_org_saml_provider_print_recovery_codes
  put    "/organizations/:organization_id/settings/security/update_ip_whitelisting_enabled",    to: "orgs/security_settings#update_ip_whitelisting_enabled",  as: :settings_org_security_update_ip_whitelisting_enabled
  put    "/organizations/:organization_id/settings/security/update_ip_whitelisting_app_access_enabled",    to: "orgs/security_settings#update_ip_whitelisting_app_access_enabled",  as: :settings_org_security_update_ip_whitelisting_app_access_enabled
  put    "/organizations/:organization_id/settings/security/update_automated_security_fixes_opt_out", to: "orgs/security_settings#update_automated_security_fixes_opt_out",  as: :settings_org_security_update_automated_security_fixes_opt_out
  put    "/organizations/:organization_id/settings/security_analysis/update", to: "orgs/security_analysis#update", as: :settings_org_security_analysis_update

  get    "/organizations/:organization_id/settings/members(.:format)",                          to: "orgs/settings#members_redirect",                         as: :settings_organization_members_redirect

  post   "/beta_enrollments", to: "beta_enrollments#create", as: :beta_enrollments
  delete "/beta_enrollments", to: "beta_enrollments#destroy"

  if GitHub.delegated_recovery_enabled?
    # Delegated account recovery endpoints.
    get    "/.well-known/delegated-account-recovery/configuration",                to: "delegated_account_recovery#well_known_config",           as: :delegated_account_recovery_well_known_config
    get    "/settings/security/delegated-account-recovery",                        to: "delegated_account_recovery#index",                       as: :delegated_account_recovery_index
    post   "/settings/security/delegated-account-recovery",                        to: "delegated_account_recovery#create",                      as: :delegated_account_recovery_create
    delete "/settings/security/delegated-account-recovery/:id",                    to: "delegated_account_recovery#destroy",                     as: :delegated_account_recovery_destroy
    match  "/settings/security/delegated-account-recovery/save-token-return",      to: "delegated_account_recovery#save_token_return",           as: :delegated_account_recovery_save_token_return, via: [:get, :post]
    post   "/settings/security/delegated-account-recovery/recover-account-return", to: "delegated_account_recovery#recover_account_return",      as: :delegated_account_recovery_recover_account_return

    if Rails.env.test? || Rails.env.development?
      post "/settings/security/recovery-provider/save-token",      to: "recovery_provider#save_token",      as: :recovery_provider_save_token
      post "/settings/security/recovery-provider/recover-account", to: "recovery_provider#recover_account", as: :recovery_provider_recover_account
    end
  end

  get    "/settings/replies",            to: "settings/saved_replies#index",   as: :saved_replies
  post   "/settings/replies",            to: "settings/saved_replies#create"
  put    "/settings/replies/:id",        to: "settings/saved_replies#update"
  get    "/settings/replies/:id/edit",   to: "settings/saved_replies#edit",    as: :edit_saved_reply
  delete "/settings/replies/:id",        to: "settings/saved_replies#destroy", as: :saved_reply

  resources :personal_reminders, except: [:edit], path: "/settings/reminders", module: :settings, param: :organization_id do
    member do
      post :reminder_test
    end
  end

  get    "/reminder_slack_workspaces/:organization_id/authorize", to: "reminder_slack_workspaces#authorize", as: :authorize_reminder_slack_workspace
  get    "/reminder_slack_workspaces/:organization_id/callback",  to: "reminder_slack_workspaces#callback",  as: :callback_reminder_slack_workspace

  get    "/settings/auth/recovery-codes",          to: "settings/auth_recovery_codes#index",                   as: :settings_auth_recovery_codes
  put    "/settings/auth/recovery-codes",          to: "settings/auth_recovery_codes#update",                  as: :settings_auth_regenerate_recovery_codes
  post   "/settings/auth/recovery-codes/download", to: "settings/auth_recovery_codes#download_recovery_codes", as: :settings_auth_download_recovery_codes
  get    "/settings/auth/recovery-codes/print",    to: "settings/auth_recovery_codes#print",                   as: :settings_auth_print_recovery_codes

  post   "/settings/migration",                                                                 to: "settings/migrations#start",                          as: :settings_user_migration_start
  post   "/settings/migration/email",                                                           to: "settings/migrations#email",                          as: :settings_user_migration_email
  get    "/settings/migration/download",                                                        to: "settings/migrations#download",                       as: :settings_user_migration_download
  delete "/settings/migration",                                                                 to: "settings/migrations#delete",                         as: :settings_user_migration_delete

  get    "/settings/dotcom-user/callback",             to: "settings/dotcom_users#callback",            as: :settings_dotcom_user_callback
  if GitHub.enterprise?
    get    "/settings/dotcom-user",                      to: "settings#dotcom_user",                      as: :settings_dotcom_user
    post   "/settings/dotcom-user",                      to: "settings/dotcom_users#create",              as: :settings_dotcom_user_create
    delete "/settings/dotcom-user",                      to: "settings/dotcom_users#destroy",             as: :settings_dotcom_user_destroy
    post   "/settings/dotcom-user/change_contributions", to: "settings/dotcom_users#change_contributions", as: :settings_dotcom_user_change_contributions
  else
    get    "/settings/enterprise-installation/:token", to: "settings/enterprise_installations#new",   as: :settings_enterprise_installation_new
    post   "/settings/enterprise-installation/:login/:token", to: "settings/enterprise_installations#create", as: :settings_enterprise_installation_create
    get    "/organizations/:organization_id/settings/enterprise_installations",   to: "settings/enterprise_installations#index", as: :organization_enterprise_installations_list
    delete "/organizations/:organization_id/settings/enterprise_installations/:id",  to: "settings/enterprise_installations#destroy", as: :organization_enterprise_installation_delete
  end

  ##
  # DismissalsController
  post    "/settings/dismiss-notice/:notice",    to: "settings/dismissals#create" , as: :dismiss_notice

  ##
  # U2fRegistrationController
  post   "/u2f/registrations",     to: "u2f_registrations#create",         as: :u2f_create
  delete "/u2f/registrations/:id", to: "u2f_registrations#destroy",        as: :u2f_destroy
  get    "/u2f/trusted_facets",    to: "u2f_registrations#trusted_facets", as: :u2f_trusted_facets

  ##
  # User Settings
  get     "/settings/avatars",      to: "avatars#show"
  get     "/settings/avatars/:id",  to: "avatars#show",  as: :settings_user_avatar
  post    "/settings/avatars/:id",  to: "avatars#update"

  get     "/settings/connections/:id", to: "oauth_accesses#show", id: /\d+/

  get     "/settings/connections/applications",             to: redirect("/settings/applications")
  get     "/settings/connections/applications/:client_id",  to: "oauth_authorizations#show",       as: :settings_oauth_authorization, client_id: AUTHORIZATION_KEY_REGEX
  delete  "/settings/connections/applications/:client_id",  to: "oauth_authorizations#destroy", client_id: AUTHORIZATION_KEY_REGEX

  post    "/settings/connections/applications/:client_id/report", to: "oauth_authorizations#report", as: :report_oauth_authorization, client_id: AUTHORIZATION_KEY_REGEX
  post    "/settings/connections/applications/revoke_all",  to: "oauth_authorizations#revoke_all", as: :revoke_all_settings_oauth_authorizations

  get     "/settings/developers", to: "oauth_applications#developer", as: :settings_user_developer_applications

  get     "/settings/tokens",                 to: "oauth_tokens#index",      as: :settings_user_tokens
  get     "/settings/tokens/new",             to: "oauth_tokens#new",        as: :new_settings_user_token
  post    "/settings/tokens",                 to: "oauth_tokens#create"
  get     "/settings/tokens/:id",             to: "oauth_tokens#show",       as: :settings_user_token
  put     "/settings/tokens/:id",             to: "oauth_tokens#update"
  post    "/settings/tokens/:id/regenerate",  to: "oauth_tokens#regenerate", as: :regenerate_settings_user_token
  delete  "/settings/tokens/:id",             to: "oauth_tokens#destroy"
  post    "/settings/tokens/revoke_all",      to: "oauth_tokens#revoke_all", as: :revoke_all_settings_user_tokens
  delete  "/settings/tokens/:id/authorizations/:org", to: "oauth_tokens#remove_authorization", as: :settings_user_token_authorization

  get     "/settings/applications",                       to: "oauth_applications#index",              as: :settings_user_applications
  get     "/settings/applications/new",                   to: "oauth_applications#new",                as: :new_settings_user_application
  post    "/settings/applications",                       to: "oauth_applications#create"
  get     "/settings/applications/:id",                   to: "oauth_applications#show",               as: :settings_user_application
  put     "/settings/applications/:id",                   to: "oauth_applications#update"
  delete  "/settings/applications/:id",                   to: "oauth_applications#destroy"
  get     "/settings/applications/:id/advanced",          to: "oauth_applications#advanced",           as: :advanced_settings_user_application
  put     "/settings/applications/:id/transfer",          to: "oauth_applications#transfer",           as: :transfer_settings_user_application
  get     "/settings/applications/:id/beta",              to: "oauth_applications#beta_features",      as: :settings_user_applications_beta_features
  post    "/settings/applications/:id/beta",              to: "apps/beta_features#enable"
  delete  "/settings/applications/:id/beta",              to: "apps/beta_features#disable"
  post    "/settings/applications/:id/reset_secret",      to: "oauth_applications#reset_secret",       as: :reset_secret_settings_user_application
  post    "/settings/applications/:id/revoke_all_tokens", to: "oauth_applications#revoke_all_tokens",  as: :revoke_all_tokens_settings_user_application

  get     "/settings/applications/:user_id/transfers/:id",          to: "settings/oauth_application_transfers#show",          as: :settings_user_application_transfer
  delete  "/settings/applications/:user_id/transfers/:id",          to: "settings/oauth_application_transfers#destroy"
  put     "/settings/applications/:user_id/transfers/:id/accept",   to: "settings/oauth_application_transfers#accept",        as: :accept_settings_user_application_transfer

  get     "/settings/apps/",                      to: "settings/integrations#index",           as: :settings_user_apps
  get     "/settings/apps/new",                   to: "settings/integrations#new",             as: :new_settings_user_app
  post    "/settings/apps/new",                   to: "settings/integrations#receive_manifest", as: :receive_user_app_from_manifest
  get     "/settings/apps/manifest",              to: "settings/integrations#new_from_manifest", as: :new_from_manifest
  get     "/settings/apps/authorizations",        to: "settings/integrations#authorizations",  as: :settings_user_app_authorizations
  post    "/settings/apps/",                      to: "settings/integrations#create"
  get     "/settings/apps/:id",                   to: "settings/integrations#show",            as: :settings_user_app
  get     "/settings/apps/:id/permissions",       to: "settings/integrations#permissions",     as: :settings_user_app_permissions
  get     "/settings/apps/:id/installations",     to: "settings/integrations#installations",   as: :settings_user_app_installations
  get     "/settings/apps/:id/advanced",          to: "settings/integrations#advanced",        as: :settings_user_app_advanced
  get     "/settings/apps/:id/beta",              to: "settings/integrations#beta_features",   as: :settings_user_app_beta_features
  put     "/settings/apps/:id/beta-toggle",       to: "settings/integrations#beta_toggle",     as: :settings_user_app_beta_feature_toggle

  resources :ip_whitelist_entries,
    as: "settings_user_apps_ip_whitelist_entries",
    path: "/settings/apps/:integration_id/ip_whitelist_entries",
    only: %w(new create edit update destroy),
    controller: "ip_whitelist_entries"

  constraints(guid: WEBHOOK_GUID_REGEX, id: WEBHOOK_REGEX, hook_id: /\d+/) do
    get  "/settings/apps/:app_id/hooks/:hook_id/deliveries",                     to: "hook_deliveries#index",     as: :settings_user_app_hook_deliveries, context: "integration"
    get  "/settings/apps/:app_id/hooks/:hook_id/deliveries/:id",                 to: "hook_deliveries#show",      as: :settings_user_app_hook_delivery, context: "integration"
    get  "/settings/apps/:app_id/hooks/:hook_id/deliveries/:id/payload.:format", to: "hook_deliveries#payload",   as: :settings_user_app_hook_delivery_payload, format: "json", context: "integration"
    post "/settings/apps/:app_id/hooks/:hook_id/deliveries/:guid/redeliver",       to: "hook_deliveries#redeliver", as: :settings_user_app_redeliver_hook_delivery, context: "integration"
  end
  put     "/settings/apps/:id",                   to: "settings/integrations#update"
  put     "/settings/apps/:id/permissions",       to: "settings/integrations#update_permissions", as: :update_permissions_settings_user_app
  post    "/settings/apps/:id/key",               to: "settings/integrations#generate_key",    as: :generate_key_settings_user_app
  delete  "/settings/apps/:id/key/:key_id",       to: "settings/integrations#remove_key",      as: :remove_key_settings_user_app
  get     "/settings/apps/:id/keys",              to: "settings/integrations#keys",            as: :list_keys_settings_user_app
  put     "/settings/apps/:id/public",            to: "settings/integrations#make_public",     as: :make_public_settings_user_app
  put     "/settings/apps/:id/internal",          to: "settings/integrations#make_internal",   as: :make_internal_settings_user_app
  post    "/settings/apps/:id/reset_secret",      to: "settings/integrations#reset_secret",    as: :reset_secret_settings_user_app
  post    "/settings/apps/:id/revoke_all_tokens", to: "settings/integrations#revoke_all_tokens", as: :revoke_all_tokens_settings_user_app
  delete  "/settings/apps/:id",                   to: "settings/integrations#destroy"
  put     "/settings/apps/:id/transfer",          to: "settings/integrations#transfer",        as: :transfer_settings_user_app

  post    "/settings/apps/preview_note", to: "settings/integrations#preview_note", as: :preview_permissions_note_user_app

  get     "/settings/apps/transfers/:id",        to: "settings/integration_transfers#show",   as: :settings_user_app_transfer
  delete  "/settings/apps/transfers/:id",        to: "settings/integration_transfers#destroy"
  put     "/settings/apps/transfers/:id/accept", to: "settings/integration_transfers#accept", as: :accept_settings_user_app_transfer

  get     "/settings/installations",                           to: "settings/installations#index",                      as: :settings_user_installations
  get     "/settings/installations/:id",                       to: "settings/installations#show",                       as: :settings_user_installation
  put     "/settings/installations/:id/update",                to: "settings/installations#update",                     as: :update_settings_user_installation
  delete  "/settings/installations/:id",                       to: "settings/installations#destroy"
  get     "/settings/installations/:id/permissions/update",    to: "settings/installations#permissions_update_request", as: :permissions_update_request_settings_user_installation
  put     "/settings/installations/:id/permissions/update",    to: "settings/installations#update_permissions",         as: :update_permissions_settings_user_installation
  post    "/settings/installations/:id/suspended",             to: "settings/installations#suspend",                    as: :suspend_settings_user_installation
  delete  "/settings/installations/:id/suspended",             to: "settings/installations#unsuspend",                  as: :unsuspend_settings_user_installation
  ##
  # Org Settings
  get     "/organizations/:organization_id/settings/applications/transfers/:id",          to: "orgs/oauth_application_transfers#show",          as: :settings_org_application_transfer
  delete  "/organizations/:organization_id/settings/applications/transfers/:id",          to: "orgs/oauth_application_transfers#destroy"
  put     "/organizations/:organization_id/settings/applications/transfers/:id/accept",   to: "orgs/oauth_application_transfers#accept",        as: :accept_settings_org_application_transfer

  get     "/organizations/:organization_id/settings/apps/transfers/:id",          to: "orgs/integration_transfers#show",           as: :settings_org_app_transfer
  delete  "/organizations/:organization_id/settings/apps/transfers/:id",          to: "orgs/integration_transfers#destroy"
  put     "/organizations/:organization_id/settings/apps/transfers/:id/accept",   to: "orgs/integration_transfers#accept",         as: :accept_settings_org_app_transfer

  get     "/organizations/:organization_id/settings/member_privileges",                   to: "orgs/settings#member_privileges",   as: :settings_org_member_privileges
  get     "/organizations/:organization_id/settings/security_analysis",                   to: "orgs/settings#security_analysis",   as: :settings_org_security_analysis
  get     "/organizations/:organization_id/settings/roles",                               to: "orgs/roles#repository_roles",       as: :settings_org_repository_roles
  post    "/organizations/:organization_id/settings/roles",                               to: "orgs/roles#create"
  get     "/organizations/:organization_id/settings/roles/new",                           to: "orgs/roles#new",                    as: :new_settings_org_repository_roles
  get     "/organizations/:organization_id/settings/roles/fgps",                          to: "orgs/roles#fgps",                   as: :settings_org_fgps
  get     "/organizations/:organization_id/settings/roles/:id/edit",                      to: "orgs/roles#edit",                   as: :edit_settings_org_repository_roles
  put     "/organizations/:organization_id/settings/roles/:id/update",                    to: "orgs/roles#update",                 as: :update_settings_org_repository_roles
  delete  "/organizations/:organization_id/settings/roles/:id",                           to: "orgs/roles#destroy",                as: :delete_settings_org_repository_roles
  get     "/organizations/:organization_id/settings/roles/new/fgp_metadata",              to: "orgs/roles#fgp_metadata",           as: :settings_org_fgp_metadata
  get     "/organizations/:organization_id/settings/teams",                               to: "orgs/settings#teams",               as: :settings_org_teams
  put     "/organizations/:organization_id/settings/migrate_legacy_admin_teams",          to: "orgs/settings#migrate_legacy_admin_teams", as: :settings_org_migrate_legacy_admin_teams

  get    "/organizations/:organization_id/settings/projects",                             to: "orgs/settings#projects",            as: :settings_org_projects

  get    "/organizations/:organization_id/settings/deleted_repositories",                 to: "orgs/settings#deleted_repositories",     as: :settings_org_deleted_repositories

  get     "/organizations/:organization_id/settings/domains",                             to: "orgs/settings#domains",                     as: :settings_org_domains

  get     "/organizations/:organization_id/settings/import-export",                       to: "orgs/settings#import_export_mannequins",               as: :settings_org_import_export
  get     "/organizations/:organization_id/settings/import-export/attribution-invitations",                  to: "orgs/settings#import_export_attribution_invitations",               as: :settings_org_import_export_invitations

  get     "/organizations/:organization_id/settings/oauth_application_policy/confirm",    to: "orgs/oauth_application_policy#splash",      as: :oauth_application_policy_confirm

  get     "/organizations/:organization_id/settings/audit-log",                           to: "orgs/audit_log#index",                      as: :settings_org_audit_log
  get     "/organizations/:organization_id/settings/audit-log/summary",                   to: "orgs/audit_log#audit_log_summary",          as: :settings_org_audit_log_summary
  get     "/organizations/:organization_id/settings/audit-log/suggestions",               to: "orgs/audit_log#suggestions",                as: :settings_org_audit_log_suggestions

  get     "/organizations/:organization_id/settings/hooks",                               to: "organization_hooks#index",                  as: :organization_hooks
  post    "/organizations/:organization_id/settings/hooks",                               to: "organization_hooks#create"
  get     "/organizations/:organization_id/settings/hooks/new",                           to: "organization_hooks#new",                    as: :new_organization_hook
  get     "/organizations/:organization_id/settings/hooks/:id",                           to: "organization_hooks#show",                   as: :organization_hook
  put     "/organizations/:organization_id/settings/hooks/:id",                           to: "organization_hooks#update"
  delete  "/organizations/:organization_id/settings/hooks/:id",                           to: "organization_hooks#destroy"
  post    "/organizations/:organization_id/settings/hooks/:id/tests",                     to: "organization_hooks#test",                   as: :test_organization_hook

  constraints(guid: WEBHOOK_GUID_REGEX, id: WEBHOOK_REGEX, hook_id: /\d+/) do
    get  "/organizations/:organization_id/settings/hooks/:hook_id/deliveries",                     to: "hook_deliveries#index",     as: :organization_hook_deliveries, context: "organization"
    get  "/organizations/:organization_id/settings/hooks/:hook_id/deliveries/:id",                 to: "hook_deliveries#show",      as: :organization_hook_delivery, context: "organization"
    get  "/organizations/:organization_id/settings/hooks/:hook_id/deliveries/:id/payload.:format", to: "hook_deliveries#payload",   as: :organization_hook_delivery_payload, format: "json", context: "organization"
    post "/organizations/:organization_id/settings/hooks/:hook_id/deliveries/:guid/redeliver",       to: "hook_deliveries#redeliver", as: :organization_redeliver_hook_delivery, context: "organization"
  end

  resources :reminders, except: [:edit], path: "/organizations/:organization_id/settings/reminders", module: :orgs, as: :org_reminders do
    collection do
      get :team_autocomplete
      get :repository_suggestions
    end
    member do
      post :reminder_test
    end
  end

  if GitHub.enterprise?
    post   "/organizations/:organization_id/settings/hooks/:id/update_pre_receive",        to: "organization_hooks#update_pre_receive",     as: :update_organization_pre_receive
  end

  get     "/organizations/:organization_id/settings/apps/",                       to: "orgs/integrations#index",                   as: :settings_org_apps
  get     "/organizations/:organization_id/settings/apps/new",                    to: "orgs/integrations#new",                     as: :new_settings_org_app
  post    "/organizations/:organization_id/settings/apps/new",                    to: "orgs/integrations#receive_manifest",       as: :new_settings_org_app_from_manifest
  post    "/organizations/:organization_id/settings/apps/",                       to: "orgs/integrations#create"
  get     "/organizations/:organization_id/settings/apps/:id",                    to: "orgs/integrations#show",                    as: :settings_org_app
  get     "/organizations/:organization_id/settings/apps/:id/permissions",        to: "orgs/integrations#permissions",             as: :settings_org_app_permissions
  get     "/organizations/:organization_id/settings/apps/:id/installations",      to: "orgs/integrations#installations",           as: :settings_org_app_installations
  get     "/organizations/:organization_id/settings/apps/:id/advanced",           to: "orgs/integrations#advanced",                as: :settings_org_app_advanced
  get     "/organizations/:organization_id/settings/apps/:id/beta",               to: "orgs/integrations#beta_features",           as: :settings_org_app_beta_features
  put     "/organizations/:organization_id/settings/apps/:id/beta-toggle",        to: "orgs/integrations#beta_toggle",             as: :settings_org_app_beta_feature_toggle

  resources :ip_whitelist_entries,
    as: "settings_org_apps_ip_whitelist_entries",
    path: "organizations/:organization_id/settings/apps/:integration_id/ip_whitelist_entries",
    only: %w(new create edit update destroy),
    controller: "ip_whitelist_entries"

  constraints(guid: WEBHOOK_GUID_REGEX, id: WEBHOOK_REGEX, hook_id: /\d+/) do
    get  "/organizations/:organization_id/settings/apps/:app_id/hooks/:hook_id/deliveries",                     to: "hook_deliveries#index",     as: :settings_org_app_hook_deliveries, context: "integration"
    get  "/organizations/:organization_id/settings/apps/:app_id/hooks/:hook_id/deliveries/:id",                 to: "hook_deliveries#show",      as: :settings_org_app_hook_delivery, context: "integration"
    get  "/organizations/:organization_id/settings/apps/:app_id/hooks/:hook_id/deliveries/:id/payload.:format", to: "hook_deliveries#payload",   as: :settings_org_app_hook_delivery_payload, format: "json", context: "integration"
    post "/organizations/:organization_id/settings/apps/:app_id/hooks/:hook_id/deliveries/:guid/redeliver",       to: "hook_deliveries#redeliver", as: :settings_org_app_redeliver_hook_delivery, context: "integration"
  end

  put     "/organizations/:organization_id/settings/apps/:id",                    to: "orgs/integrations#update"
  put     "/organizations/:organization_id/settings/apps/:id/permissions",        to: "orgs/integrations#update_permissions",      as: :update_permissions_settings_org_app
  post    "/organizations/:organization_id/settings/apps/:id/key",                to: "orgs/integrations#generate_key",            as: :generate_key_settings_org_app
  delete  "/organizations/:organization_id/settings/apps/:id/key/:key_id",        to: "orgs/integrations#remove_key",              as: :remove_key_settings_org_app
  get     "/organizations/:organization_id/settings/apps/:id/keys",               to: "orgs/integrations#keys",                    as: :list_keys_settings_org_app
  put     "/organizations/:organization_id/settings/apps/:id/public",             to: "orgs/integrations#make_public",             as: :make_public_settings_org_app
  put     "/organizations/:organization_id/settings/apps/:id/internal",           to: "orgs/integrations#make_internal",           as: :make_internal_settings_org_app
  post    "/organizations/:organization_id/settings/apps/:id/reset_secret",       to: "orgs/integrations#reset_secret",            as: :reset_secret_settings_org_app
  post    "/organizations/:organization_id/settings/apps/:id/revoke_all_tokens",  to: "orgs/integrations#revoke_all_tokens",       as: :revoke_all_tokens_settings_org_app
  delete  "/organizations/:organization_id/settings/apps/:id",                    to: "orgs/integrations#destroy"
  put     "/organizations/:organization_id/settings/apps/:id/transfer",           to: "orgs/integrations#transfer",                as: :transfer_settings_org_app

  post    "/organizations/:organization_id/settings/apps/preview_note", to: "settings/integrations#preview_note", as: :preview_permissions_note_org_app

  get     "/organizations/:organization_id/settings/installations",                           to: "orgs/installations#index",                      as: :settings_org_installations
  get     "/organizations/:organization_id/settings/installations/:id",                       to: "orgs/installations#show",                       as: :settings_org_installation
  put     "/organizations/:organization_id/settings/installations/:id/update",                to: "orgs/installations#update",                     as: :update_settings_org_installation
  delete  "/organizations/:organization_id/settings/installations/:id",                       to: "orgs/installations#destroy"
  get     "/organizations/:organization_id/settings/installations/:id/permissions/update",    to: "orgs/installations#permissions_update_request", as: :permissions_update_request_settings_org_installation
  put     "/organizations/:organization_id/settings/installations/:id/permissions/update",    to: "orgs/installations#update_permissions",         as: :update_permissions_settings_org_installation
  post    "/organizations/:organization_id/settings/installations/:id/suspended",             to: "orgs/installations#suspend",                    as: :suspend_settings_org_installation
  delete  "/organizations/:organization_id/settings/installations/:id/suspended",             to: "orgs/installations#unsuspend",                  as: :unsuspend_settings_org_installation

  # FGP Manage Organization owned GitHub App
  get     "/organizations/:organization_id/settings/permissions/integrations/:id/managers",       to: "orgs/permissions/integrations#managers",           as: :settings_org_permissions_integrations_managers
  get     "/organizations/:organization_id/settings/permissions/integrations/:id/suggestions",    to: "orgs/permissions/integrations#suggestions",        as: :settings_org_permissions_integrations_managers_suggestions
  post    "/organizations/:organization_id/settings/permissions/integrations/:id/grant",          to: "orgs/permissions/integrations#grant",              as: :settings_org_permissions_integrations_managers_grant
  delete  "/organizations/:organization_id/settings/permissions/integrations/:id/revoke",         to: "orgs/permissions/integrations#revoke",             as: :settings_org_permissions_integrations_managers_revoke

  # FGP Manage *all* Organization owned GitHub Apps
  get     "/organizations/:organization_id/settings/permissions/manage_integrations/suggestions", to: "orgs/permissions/manage_integrations#suggestions", as: :settings_org_permissions_manage_integrations_suggestions
  post    "/organizations/:organization_id/settings/permissions/manage_integrations/grant",       to: "orgs/permissions/manage_integrations#grant",       as: :settings_org_permissions_manage_integrations_grant
  delete  "/organizations/:organization_id/settings/permissions/manage_integrations/revoke",      to: "orgs/permissions/manage_integrations#revoke",      as: :settings_org_permissions_manage_integrations_revoke

  delete  "/organizations/:organization_id/settings/connections/:id",                     to: "oauth_accesses#destroy"

  get     "/organizations/:organization_id/settings/applications",                        to: "oauth_applications#index",                  as: :settings_org_applications
  get     "/organizations/:organization_id/settings/applications/new",                    to: "oauth_applications#new",                    as: :new_settings_org_application
  post    "/organizations/:organization_id/settings/applications",                        to: "oauth_applications#create"
  get     "/organizations/:organization_id/settings/applications/:id",                    to: "oauth_applications#show",                   as: :settings_org_application
  put     "/organizations/:organization_id/settings/applications/:id",                    to: "oauth_applications#update"
  delete  "/organizations/:organization_id/settings/applications/:id",                    to: "oauth_applications#destroy"
  get     "/organizations/:organization_id/settings/applications/:id/advanced",           to: "oauth_applications#advanced",               as: :advanced_settings_org_application
  put     "/organizations/:organization_id/settings/applications/:id/transfer",           to: "oauth_applications#transfer",               as: :transfer_settings_org_application
  get     "/organizations/:organization_id/settings/applications/:id/beta",               to: "oauth_applications#beta_features",          as: :settings_org_applications_beta_features
  post    "/organizations/:organization_id/settings/applications/:id/beta",               to: "apps/beta_features#enable"
  delete  "/organizations/:organization_id/settings/applications/:id/beta",               to: "apps/beta_features#disable"
  post    "/organizations/:organization_id/settings/applications/:id/reset_secret",       to: "oauth_applications#reset_secret",           as: :reset_secret_settings_org_application
  post    "/organizations/:organization_id/settings/applications/:id/revoke_all_tokens",  to: "oauth_applications#revoke_all_tokens",      as: :revoke_all_tokens_settings_org_application

  get     "/organizations/:organization_id/settings/interaction_limits",                  to: "orgs/settings#interaction_limits",          as: :org_interaction_limits
  put     "/organizations/:organization_id/settings/interaction_limits",                  to: "orgs/settings#update_interaction_limits",   as: :update_org_interaction_limits

  put     "/organizations/:organization_id/settings/notification_restrictions",           to: "orgs/settings#set_notification_restriction", as: :organization_set_notification_restriction

  # "Repository defaults" settings page
  get    "/organizations/:organization_id/settings/repository-defaults",                  to: "orgs/repository_default_settings#index",            as: :settings_org_repo_defaults
  get    "/organizations/:organization_id/settings/labels", to: redirect("/organizations/%{organization_id}/settings/repository-defaults"), format: false

  # Label defaults for an org
  get    "/organizations/:organization_id/settings/labels/preview/:name",                 to: "orgs/label_settings#preview",                       as: :preview_org_label
  post    "/organizations/:organization_id/settings/labels",                              to: "orgs/label_settings#create",                        as: :org_user_labels
  put    "/organizations/:organization_id/settings/labels/:id",                           to: "orgs/label_settings#update",                        as: :delete_org_user_label
  delete    "/organizations/:organization_id/settings/labels/:id",                        to: "orgs/label_settings#destroy",                       as: :update_org_user_label

  # Repository default branch setting for an org
  put    "/organizations/:organization_id/settings/default-branch",                       to: "orgs/default_branch_settings#update",               as: :update_org_default_branch

  # Org Email Verification
  scope "/organizations/:organization_id" do
    resources :emails, controller: "user_emails", only: [:create], as: :organization_emails
    match "/emails/:id/confirm_verification/:t", controller: "user_emails", action: :confirm_verification, as: :organization_confirm_verification_email, format: false, via: [:get, :post]
    match "/emails/:id/confirm_verification",    controller: "user_emails", action: :confirm_verification, format: false, via: [:get, :post]
  end

  if GitHub.oauth_application_policies_enabled?
    get     "/organizations/:organization_id/settings/oauth_application_policy",            to: "orgs/oauth_application_policy#show",        as: :settings_org_oauth_application_policy
  end

  get "/:user_id/repositories", to: redirect("/%{user_id}?tab=repositories"), format: false

  get "/users/:user_id/contributions", to: "user_contributions#show", as: :user_contributions
  get "/users/:user_id/contributions/sample", to: "user_contributions#sample", as: :user_contributions_sample
  get "/users/:user_id/organizations_info", to: "users#organizations_info", as: :user_organizations_info
  get "/users/:user_id/tab_counts", to: "profiles#tab_counts", as: :user_tab_counts

  post "/users/survey_developer_tools", to: "users#record_developer_tools_survey_popup_shown", as: :record_developer_tools_survey_popup_shown

  put "/users/:user_id/emails/primary", to: "user_emails#set_primary", as: :set_primary_user_email
  put "/users/:user_id/emails/backup", to: "user_emails#set_backup", as: :set_backup_user_email

  # User account succession
  get "/users/:user_id/succession/invitation", to: "successor_invitations#show_pending", as: :pending_successor_invitation
  post "/users/:user_id/succession/invitation/accept", to: "successor_invitations#accept", as: :successor_invitation_accept
  post "/users/:user_id/succession/invitation/decline", to: "successor_invitations#decline", as: :successor_invitation_decline
  post "/succession/set_successor", to: "successor_invitations#create", as: :set_successor
  put  "/succession/unset_successor", to: "successor_invitations#revoke", as: :unset_successor
  put  "/succession/cancel", to: "successor_invitations#cancel", as: :cancel_successor_invitation
  get  "/succession/suggestions", to: "successor_invitations#suggestions", as: :user_successor_suggestions

  ##
  # Reactions
  put "/users/:user_id/reactions", to: "reactions#update", as: :update_reaction
  get "/users/:user_id/reactions/list", to: "reactions#list"
  get "/users/:user_id/reactions/group", to: "reactions#group"

  ##
  # Resourcey
  resources :users, only: [] do
    member do
      post :rename
      delete :dismiss_notice
      delete :dismiss_repository_notice
      post :hide_jobs
      put :set_private_contributions_preference
      put :set_activity_overview_preference, controller: "settings/profiles", action: :update_activity_overview_enabled
      put :set_profile_badges_preference, controller: "settings/profiles", action: :update_profile_badges_preference
      put :set_mobile_opt_out, controller: "settings/profiles", action: :update_mobile_opt_out_enabled
      put :set_emoji_skin_tone, controller: "settings/profiles", action: :update_emoji_skin_tone_preference
    end

    resources :emails, controller: "user_emails" do
      member do
        post :request_verification
      end

      collection do
        put :toggle_visibility
        put :toggle_email_visibility_warning
      end
    end

    unless GitHub.enterprise?
      resources :staff_access_requests, only: [:show], controller: "users/staff_access_requests" do
        member do
          put :accept
          put :deny
        end
      end
    end

    match "/emails/:id/confirm_verification/:t", controller: "user_emails", action: :confirm_verification, as: :confirm_verification_email, format: false, via: [:get, :post]
    match "/emails/:id/confirm_verification",    controller: "user_emails", action: :confirm_verification, format: false, via: [:get, :post]

    collection do
      get  :index, as: false
      post :create, as: false
    end

    new do
      get  :new
    end

    member do
      get :edit
      put :update, as: :user
      delete :destroy, as: false
    end
  end

  post "/topics/:topic_name/star", to: "topics/stars#create", as: :star_topic
  delete "/topics/:topic_name/unstar", to: "topics/stars#destroy", as: :unstar_topic

  resource :my_bio, only: :update, controller: :bios

  put "/users/:user_id/set_pinned_items",     to: "profile_pins#set_pinned_items", as: :user_set_pinned_items
  put "/users/:user_id/reorder_pinned_items", to: "profile_pins#reorder_pinned_items", as: :user_reorder_pinned_items
  get "/users/:user_id/pinned_items_modal",   to: "profile_pins#pinned_items_modal", as: :user_pinned_items_modal
  get "/users/:user_id/pinnable_items",       to: "profile_pins#pinnable_items", as: :user_pinnable_items

  # UserDashboardPinsController (for everything but gists)
  get "/users/:user_id/dashboard-pins", to: "user_dashboard_pins#index", as: :user_dashboard_pinned_items
  post "/users/:user_id/dashboard-pin/:item_id", to: "user_dashboard_pins#create",
    as: :user_dashboard_item_pin
  delete "/users/:user_id/dashboard-pin/:item_id", to: "user_dashboard_pins#destroy"
  put "/users/:user_id/dashboard-pins", to: "user_dashboard_pins#update"

  if GitHub.blog_enabled?
    get "/blog", to: redirect("#{GitHub.blog_url}/")
    get "/blog/broadcasts", to: redirect("#{GitHub.blog_url}/broadcasts/")
    get "/blog/subscribe", to: redirect("#{GitHub.blog_url}/subscribe/")
    get "/blog/category/:category", to: redirect("#{GitHub.blog_url}/category/%{category}/")
    get "/blog/:category.atom", to: redirect("#{GitHub.blog_url}/%{category}.atom"), as: :category_feed, format: "atom"
    get "/updates", to: redirect(GitHub.blog_url)
    get "/updates/satellite-2016", to: redirect(GitHub.blog_url)

    get "/blog/:id", to: "blog_redirects#show"
  end

  concern :codespaces_routing do
    member do
      get "provisioned", as: :provisioned
    end
    collection do
      get  "repository_select", as: :repository_select
      get  "ref_select", as: :ref_select
      post "toggle_dev_flags", as: :toggle_dev_flags
    end
  end

  concern :codespaces_auth_routing do
    collection do
      get  "auth/:identifier", to: "codespaces/auth#port_forwarding", as: :auth_port_forwarding
      get  "auth", to: "codespaces/auth#passthru", as: :auth_redirect
      get  "launch/:identifier", to: "codespaces/auth#passthru", as: :auth_launch
      post ":identifier/cascade_token", to: "codespaces/auth#mint_cascade_token", as: :mint_cascade_token
    end
  end
  ##
  # Workspaces (legacy)
  ##
  resources :workspaces, controller: "codespaces", param: :identifier, constraints: { identifier: CODESPACE_REGEX }, except: [:edit, :update], concerns: [:codespaces_routing, :codespaces_auth_routing]
  ##
  # Codespaces (Workspaces renamed)
  ##
  resources :codespaces, param: :identifier, constraints: { identifier: CODESPACE_REGEX }, except: [:edit, :update], concerns: [:codespaces_routing, :codespaces_auth_routing]
  ##
  # Redirects to support previous codespaces routes
  ##
  constraints(owner: USERID_REGEX, name: CODESPACE_REGEX) do
    ["codespaces", "workspaces"].each do |space|
      get    "/#{space}/:owner/:name/provisioned", to: redirect("/#{space}/%{name}/provisioned")
      post   "/#{space}/:owner/:name/cascade_token", to: redirect("/#{space}/%{name}/cascade_token")
      get    "/#{space}/:owner/:name", to: redirect("/#{space}/%{name}")
      delete "/#{space}/:owner/:name", to: redirect("/#{space}/%{name}")
    end
  end


  ##
  # Repositories
  resources :repositories, only: [:index, :create] do
    collection do
      get "search", to: redirect("/search")
    end
  end

  post "/repositories/check-name", to: "repositories#check_name", as: :repository_check_name
  get "/repositories/transfers/:token", to: "account#accept_repository_transfer_request", as: :repository_transfer

  # Sub Dependencies
  get "/repositories/network/sub_dependencies", to: "network#sub_dependencies", as: :network_sub_dependencies

  ##
  # Guides
  get "/guides/pages",                                                   to: redirect("https://pages.github.com")
  get "/guides/textile-formatting",                                      to: redirect("http://textile.thresholdstate.com/")
  get "/guides/addressing-authentication-problems-with-ssh",             to: redirect("#{GitHub.help_url}/troubleshooting-ssh")
  get "/guides/how-to-not-have-to-type-your-password-for-every-push",    to: redirect("#{GitHub.help_url}/working-with-key-passphrases")
  get "/guides/local-github-config",                                     to: redirect("#{GitHub.help_url}/git-email-settings/")
  get "/guides/the-github-api",                                          to: redirect("#{GitHub.help_url}/api/")
  get "/guides/understanding-deploy-keys",                               to: redirect("#{GitHub.help_url}/deploy-keys/")
  get "/guides/deploying-with-capistrano",                               to: redirect("#{GitHub.help_url}/capistrano")
  get "/guides/issues-with-textmate-set-as-git-editor",                  to: redirect("#{GitHub.help_url}/textmate/")
  get "/guides/managing-multiple-clients-and-their-repositories",        to: redirect("#{GitHub.help_url}/managing-clients/")
  get "/guides/multiple-github-accounts",                                to: redirect("#{GitHub.help_url}/managing-clients/")
  get "/guides/fork-a-project-and-submit-your-modifications",            to: redirect("#{GitHub.help_url}/forking/")
  get "/guides/keeping-a-git-fork-in-sync-with-the-forked-repo",         to: redirect("#{GitHub.help_url}/forking/")
  get "/guides/how-do-i-delete-a-repository",                            to: redirect("#{GitHub.help_url}/deleting-a-repo/")
  get "/guides/change-author-details-in-commit-history",                 to: redirect("#{GitHub.help_url}/changing-author-info/")
  get "/guides/completely-remove-a-file-from-all-revisions",             to: redirect("#{GitHub.help_url}/removing-sensitive-data")
  get "/guides/changing-a-series-of-commits-or-patches",                 to: redirect("#{GitHub.help_url}/rebase")
  get "/guides/remove-a-remote-branch",                                  to: redirect("#{GitHub.help_url}/remotes")
  get "/guides/push-a-branch-to-github",                                 to: redirect("#{GitHub.help_url}/remotes")
  get "/guides/copy-a-remote-branch",                                    to: redirect("#{GitHub.help_url}/remotes")
  get "/guides/rename-a-remote-branch",                                  to: redirect("#{GitHub.help_url}/remotes")
  get "/guides/ignore-for-git",                                          to: redirect("#{GitHub.help_url}/git-ignore")
  get "/guides/push-tags-to-github",                                     to: redirect("#{GitHub.help_url}/remotes/")
  get "/guides/dealing-with-errors-when-pushing",                        to: redirect("#{GitHub.help_url}/remotes/")
  get "/guides/changing-your-origin",                                    to: redirect("#{GitHub.help_url}/remotes/")
  get "/guides/import-an-existing-git-repo",                             to: redirect("#{GitHub.help_url}/remotes/")
  get "/guides/disaster-faq-what-to-do-when-github-goes-bad",            to: redirect("http://ozmm.org/posts/when_github_goes_down.html")
  get "/guides/providing-your-ssh-key",                                  to: redirect("#{GitHub.help_url}/key-setup-redirect")
  get "/guides/git-screencasts",                                         to: redirect("https://gist.github.com/423320")
  get "/guides/git-podcasts",                                            to: redirect("https://gist.github.com/423320")
  get "/guides/feature-requests",                                        to: redirect("https://github.com/contact")
  get "/guides/github-bugs",                                             to: redirect("https://github.com/contact")
  get "/guides/setting-up-a-remote-repository-using-github-and-osx",     to: redirect("#{GitHub.help_url}/")
  get "/guides/using-git-and-github-for-the-windows-for-newbies",        to: redirect("#{GitHub.help_url}/")
  get "/guides/i-m-missing-my-gravatar-icon-on-my-commits",              to: redirect("#{GitHub.help_url}/troubleshooting-common-issues/")
  get "/guides/get-git-on-mac",                                          to: redirect("#{GitHub.help_url}/mac-git-installation")
  get "/guides/compiling-git-on-os-x-leopard",                           to: redirect("#{GitHub.help_url}/mac-git-installation")
  get "/guides/compiling-and-installing-git-on-mac-os-x",                to: redirect("#{GitHub.help_url}/mac-git-installation")
  get "/guides/rebase-howto",                                            to: redirect("#{GitHub.help_url}/rebase/")
  get "/guides/syntax-highlighting-isn-t-working-for-my-language",       to: redirect("#{GitHub.help_url}/troubleshooting-common-issues/")
  get "/guides/how-to-move-a-repo-to-another-account",                   to: redirect("#{GitHub.help_url}/moving-a-repo")
  get "/guides/tell-git-your-user-name-and-email-address",               to: redirect("#{GitHub.help_url}/git-email-settings")
  get "/guides/dealing-with-newlines-in-git",                            to: redirect("#{GitHub.help_url}/dealing-with-lineendings")
  get "/guides/using-the-egit-eclipse-plugin-with-github",               to: redirect("https://wiki.eclipse.org/EGit/User_Guide")
  get "/guides/how-to-clone-from-github-with-ssh-tunnels",               to: redirect("#{GitHub.help_url}/firewalls-and-proxies/")
  get "/guides/dealing-with-firewalls-and-proxies",                      to: redirect("#{GitHub.help_url}/firewalls-and-proxies/")
  get "/guides/how-to-transparently-clone-from-github-with-ssh-tunnels", to: redirect("#{GitHub.help_url}/firewalls-and-proxies/")
  get "/guides/import-from-subversion",                                  to: redirect("#{GitHub.help_url}/svn-importing")
  get "/guides/put-your-git-branch-name-in-your-shell-prompt",           to: redirect("http://www.gitready.com/advanced/2009/01/23/bash-git-status.html")
  get "/guides/pull-requests",                                           to: redirect("#{GitHub.help_url}/pull-requests")
  get "/guides/git-cheat-sheet",                                         to: redirect("#{GitHub.help_url}/git-cheat-sheets")
  get "/guides/readme-formatting",                                       to: redirect("https://github.com/github/markup/blob/master/README.md")
  get "/guides/developing-with-submodules",                              to: redirect("#{GitHub.help_url}/submodules/")
  get "/guides",                                                         to: redirect(GitHub.help_url)

  get "/plans", to: redirect("/pricing")

  # Help
  get "/help", to: redirect(GitHub.help_url)

  ##
  # Beta Agreement https://github.com/github/legal/issues/298
  get  "/prerelease/agreement", to: "prerelease#agreement", as: :prerelease_agreement, format: false
  post "/prerelease/agree",     to: "prerelease#agree",     as: :prerelease_agree, format: false
  get  "/prerelease/thanks",    to: "prerelease#thanks",    as: :prerelease_agreement_thanks, format: false

  # suggestions
  get  "suggestions(/:subject_type(/:subject_id))", to: "suggestions#show", as: :suggestions

  post "/content_reference_attachments/hide", to: "content_reference_attachments#hide", as: :unfurl_hide

  # Attribution Invitiations
  put "/attribution-invitations/:id/accept", to: "attribution_invitations#accept", as: :accept_attribution_invitation
  put "/attribution-invitations/:id/reject", to: "attribution_invitations#reject", as: :reject_attribution_invitation

  #
  # GitHub Component Kit
  #
  # Everything in this block is scoped under /component_kit. All Component Kit
  # routes should be placed in here.
  #
  scope "/component_kit" do
    ##
    # Interactive components
    post "/interactive_components/:interactive_component_id/button_click", to: "interactive_components#button_click", as: :interactive_component_button_click
    post "/interactive_components/:interactive_component_id/ephemeral_notices/:ephemeral_notice_id/dismiss",
      to: "interactive_components#dismiss", as: :interactive_component_ephemeral_notices_dismiss
  end

  ##
  # PagesAuthController
  # Pages auth call should not be within the scope of the repo for security reasons.
  get  "pages/auth",                    to: "pages_auth#authenticate",   as: :authorize_page_id

  #
  # Repository routes
  #
  # Everything in this block is scoped under /:user_id/:repository. All repo
  # routes should be placed in here.
  #
  scope "/:user_id/:repository", constraints: { repository: REPO_REGEX, user_id: USERID_REGEX } do
    ##
    # Retired Features
    get "forkqueue",     to: redirect("/410")
    get "graphs/impact", to: redirect("/410")

    ##
    # Repository import
    scope "import" do
      scope "auth" do
        put "", to: "repository_imports/auth#update", as: :repository_import_auth
      end

      scope "authors" do
        get "", to: "repository_imports/authors#index", as: :repository_import_authors
        get "suggestions", to: "repository_imports/authors#author_suggestions", as: :repository_import_author_suggestions
        put ":id", to: "repository_imports/authors#update", as: :repository_import_author
      end

      scope "large_files" do
        get "", to: "repository_imports/large_files#index", as: :repository_import_large_files
        put "", to: "repository_imports/large_files#update"
      end

      scope "project" do
        put "", to: "repository_imports/project#update", as: :repository_import_project
      end

      get "", to: "repository_imports#show", as: :repository_import
      patch "", to: "repository_imports#update"
      delete "", to: "repository_imports#destroy"
    end

    ##
    # Milestones
    get    "milestones",                     to: "milestones#index", as: :milestones
    get    "milestones/new",                 to: "milestones#new", as: :new_milestone
    post   "milestones",                     to: "milestones#create"
    get    "milestones/:id/edit",            to: "milestones#edit", as: :edit_milestone
    put    "milestones/:id/toggle",          to: "milestones#toggle"
    put    "milestone/:id",                  to: "milestones#update"
    put    "milestone/:id/prioritize",       to: "milestones#prioritize", as: :prioritize_milestone
    delete "milestone/:id",                  to: "milestones#destroy"
    get    "milestone/:number",              to: "milestones#show", as: :milestone
    get    "milestone/:id/issues",           to: "milestones#issues", as: :milestone_issues
    get    "milestone/:id/paginated_issues", to: "milestones#paginated_issues", as: :milestone_paginated_issues
    # Legacy route
    get    "milestones/:milestone_name",  to: "issues#index", milestone_name: /.+/, as: :milestone_query

    get    "projects",                              to: "repos/projects#index", as: :repo_projects
    get    "projects/new",                          to: "repos/projects#new", as: :new_repo_project
    get    "projects/:number/edit",                 to: "repos/projects#edit", as: :edit_repo_project
    get    "projects/:number",                      to: "repos/projects#show", as: :repo_project
    delete "projects",                              to: "repos/projects#destroy"
    post   "projects",                              to: "repos/projects#create"
    put    "projects/:number",                      to: "repos/projects#update"
    put    "projects/:number/state",                to: "repos/projects#update_state", as: :update_repo_project_state
    get    "projects/:number/search_results",       to: "repos/projects#search_results", as: :repo_project_search_results
    get    "projects/:number/target_owner_results", to: "repos/projects#target_owner_results", as: :repo_project_target_owner_results
    get    "projects/:number/activity",             to: "repos/projects#activity", as: :repo_project_activity
    get    "projects/:number/add_cards_link",       to: "repos/projects#add_cards_link", as: :repo_project_add_cards_link
    post   "projects/:number/clone",                to: "repos/projects#clone", as: :repo_project_clone

    get    "projects/:number/hovercard", to: "hovercards/repository_projects#show", as: :repo_project_hovercard

    get    "projects/:project_number/cards",                        to: "repos/project_cards#index", as: :repo_project_cards
    get    "projects/:project_number/cards/archived",               to: "repos/project_cards#archived", as: :repo_project_archived_cards
    get    "projects/:project_number/cards/archived/search",        to: "repos/project_cards#search_archived", as: :repo_project_search_archived_cards
    get    "projects/:project_number/cards/archived/:id",           to: "repos/project_cards#check_archived", as: :repo_project_check_archived_card
    delete "projects/:project_number/cards/:id",                    to: "repos/project_cards#destroy", as: :repo_project_card
    get    "projects/:project_number/cards/:id",                    to: "repos/project_cards#show"
    put    "projects/:project_number/cards/:id/archive",            to: "repos/project_cards#archive", as: :archive_repo_project_card
    put    "projects/:project_number/cards/:id/unarchive",          to: "repos/project_cards#unarchive", as: :unarchive_repo_project_card
    get    "projects/:project_number/cards/:id/columns",            to: "repos/project_cards#card_columns", as: :repo_project_card_columns
    get    "projects/:project_number/cards/:id/closing_references", to: "repos/project_cards#closing_references", as: :repo_project_card_closing_references
    get    "projects/:project_number/cards/:id/closing_references/:reference_id", to: "repos/project_cards#closing_reference", as: :repo_project_card_closing_reference
    post   "projects/:project_number/columns",                      to: "repos/project_columns#create", as: :repo_project_columns
    put    "projects/:project_number/columns/:id",                  to: "repos/project_columns#update"
    get    "projects/:project_number/columns/:id",                  to: "repos/project_columns#show", as: :repo_project_column
    delete "projects/:project_number/columns/:id",                  to: "repos/project_columns#destroy"
    put    "projects/:project_number/columns/:id/archive",          to: "repos/project_columns#archive", as: :repo_project_column_archive
    put    "projects/:project_number/columns/:id/workflow",         to: "repos/project_columns#update_workflow", as: :repo_project_column_update_workflow
    get    "projects/:project_number/automation_options",           to: "repos/project_columns#automation_options", as: :repo_project_column_automation_options
    put    "projects/:project_number/reorder_columns",              to: "repos/project_columns#reorder", as: :repo_reorder_project_columns
    put    "projects/:project_number/columns/:column_id/cards",     to: "repos/project_cards#update", as: :repo_project_column_cards
    get    "projects/:project_number/columns/:column_id/cards",     to: "repos/project_cards#index"
    post   "projects/:project_number/notes/:id/convert_to_issue",   to: "repos/project_cards#convert_to_issue", as: :repo_project_convert_note_to_issue
    get    "projects/:project_number/preview_note",                 to: "repos/project_cards#preview_note", as: :repo_project_preview_note
    get    "projects/:project_number/pull_request_status/:id",      to: "repos/project_cards#pull_request_status", as: :repo_card_pull_request_status
    put    "projects/:project_number/notes/:id",                    to: "repos/project_cards#update_note", as: :repo_project_update_note
    put    "projects/:project_number/notes/:id/task_list",          to: "repos/project_cards#update_note_task_list", as: :repo_project_update_note_task_list

    put    "projects/issues/:issue_number",                       to: "project_issues#update", as: :repo_project_issues
    post   "projects/issues",                                     to: "project_issues#new", as: :new_repo_project_issues

    # Reactions for subjects owned by repositories
    put    "reactions", to: "reactions#update", as: :update_repository_reaction, context: "repository"
    get    "reactions/list", to: "reactions#list", context: "repository"
    get    "reactions/group", to: "reactions#group", context: "repository"

    # Actions
    if GitHub.actions_enabled? || Rails.env.test?
      get "actions", to: "actions#index", as: :actions
      get "actions/workflow-run/:workflow_run_id", to: "actions#workflow_run_item", as: :workflow_run_item_partial
      get "actions/workflow-runs", to: "actions#workflow_runs", as: :workflow_runs_partial

      post "actions/enable", to: "actions#enable", as: :actions_enable
      get "actions/actors", to: "actions/actors#index", as: :actions_actors_menu
      get "actions/branches", to: "actions/branches#index", as: :actions_branches_menu
      get "actions/branches/select", to: "actions/branches#select", as: :actions_branches_select
      get "actions/events", to: "actions/events#index", as: :actions_events_menu
      get "actions/statuses", to: "actions/statuses#index", as: :actions_statuses_menu

      unless GitHub.enterprise?
        get "actions/survey", to: "actions/survey#index", as: :actions_survey
        post "actions/survey/dismiss", to: "actions/survey#dismiss", as: :actions_survey_dismiss
        post "actions/survey/answer", to: "actions/survey#answer", as: :actions_survey_answer
      end

      get "actions/new(/:category)", to: "actions#new", as: :actions_onboarding

      get "actions/runs/:workflow_run_id", to: "actions/workflow_runs#show", as: :workflow_run
      get "actions/runs/:workflow_run_id/timing", to: "actions/workflow_runs#workflow_run_timing_details_partial", as: :workflow_run_timing_details_partial
      get "actions/runs/:workflow_run_id/workflow_run", to: "actions/workflow_runs#workflow_run_partial", as: :workflow_run_partial
      get "actions/runs/:workflow_run_id/:tab", to: "actions/workflow_runs#show", as: :workflow_file_tab, tab: /workflow/
      get "actions/runs/:workflow_run_id/delete", to: "actions/workflow_runs#delete_workflow_run_modal", as: :delete_workflow_run_modal
      delete "actions/runs/:workflow_run_id", to: "actions/workflow_runs#delete_workflow_run", as: :delete_workflow_run
      delete "actions/runs/:workflow_run_id/logs", to: "actions/workflow_runs#delete_logs", as: :delete_workflow_run_logs

      post "actions/workflows/:workflow_id/disable", to: "actions/workflow_runs#disable_workflow", as: :disable_workflow
      post "actions/workflows/:workflow_id/enable", to: "actions/workflow_runs#enable_workflow", as: :enable_workflow

      get "workflows/*workflow_name/badge.svg", to: "workflows/badges#show", as: :workflow_badge, defaults: { format: :svg }
      get "workflows/*workflow_name/settings/badge", to: "workflows/badges#configure", as: :workflow_badge_configure

      get "actions/manual", to: "actions/manual#manual_run_partial", as: :actions_manual_run_partial
      post "actions/manual", to: "actions/manual#trigger", as: :actions_manual_run_trigger
    end

    # Packages
    if PackageRegistryHelper.show_packages?
      get "packages", to: "registry/packages#index", as: :packages
      get "packages/:id", to: "registry/packages#show", as: :package
      get "packages/:id/:version_id/edit", to: "registry/packages#edit", as: :edit_package_version
      post "packages/:id/:version_id/edit", to: "registry/packages#commit", as: :commit_package_version
      get "packages/:id/versions", to: "registry/packages#versions", as: :package_versions
      delete "packages/:package_id/versions/:id/destroy", to: "registry/package_versions#destroy", as: :destroy_package_version
    end

    # For populating an initial discussion with template
    namespace "discussions" do
      resources :welcome_templates, only: [:create]
    end

    ##
    # Discussion Categories
    scope "discussions", module: :discussions do
      resources :categories, only: [:index]
    end

    ##
    # Discussions
    resources :discussions, param: :number do
      collection do
        get :author_filter_content

        namespace :discussions, path: "/" do
          resource :leaderboard, only: :show
          resource :category_choices, only: :show
          resource :activity_indicator, only: :show
        end
      end

      resources :comments, only: [] do
        scope module: :discussions do
          resource :comment_actions_menu, only: :show, as: :actions_menu
        end
      end

      scope module: :discussions do
        resources :spotlights, only: [:new, :edit, :create, :update, :destroy] do
          collection do
            resource :spotlight_emoji_picker, only: [:show], path: "emoji-picker"
            resource :spotlight_preview, only: [:show], path: "preview"
          end
        end
        resource :category_menu, only: :show
        resource :repository_transfer, only: [:show, :update] do
          get :repositories
        end
      end

      member do
        get :reactions, to: "discussions/reactions#show", target: "discussion"
        get :timeline_page, to: "discussions/paginations#show"
        get :timeline_anchor, to: "discussions/permalinks#show"
        get :title
        get :comment_header_reaction_button
        get :body
        get :sidebar
        get :hovercard, to: "hovercards/discussions#show"
        get :block_from_comment_modal
        get :timeline
        get :edits_menu, to: "discussions/edits#edit_history"
        get :edits_log, to: "discussions/edits#edit_history_log"
        get :open_new_issue_modal
        put :lock
        put :unlock
      end

      resources :votes, only: [:index, :create], controller: "discussions/votes" do
        collection do
          delete :destroy
        end
      end

      resources :comments, only: [:create, :update, :destroy, :show], controller: "discussions/comments" do
        member do
          get :reactions, to: "discussions/reactions#show", target: "discussion_comment"
          post :mark_as_answer, to: "discussions/chosen_comments#create"
          delete :unmark_as_answer, to: "discussions/chosen_comments#destroy"
          get :edits_menu, to: "discussions/edits#edit_history"
          get :edits_log, to: "discussions/edits#edit_history_log"
          get :minimize_form
          post :minimize
          post :unminimize
          get :open_new_issue_modal
          get :comment_header_reaction_button
        end
      end
    end

    ##
    # Issues
    get   "issues",                   to: "issues#index",                        as: :issues
    put   "issues/triage",            to: "issues#triage",                       as: :triage_issues

    get   "issues/new",               to: "issues#new",                          as: :new_issue
    get   "issues/new/choose",        to: "issues#choose",                       as: :choose_issue
    match "issues/new/show_partial",  to: "issues#show_partial",                 as: :show_partial_new_issue, via: [:get, :post]
    post  "issues",                   to: "issues#create"

    get   "issues/show_menu_content", to: "issues#show_menu_content",            as: :show_menu_content_issues

    # CloseIssueReferences - used by issues and pull requests
    put   "issues/closing_references", to: "closing_references#update",  as: "closing_issue_reference"
    get   "issues/closing_references/:source_id", to: "closing_references#index",  as: "closing_issue_references"

    get   "issues/:id",               to: "issues#show",          id: /\d+/,  as: :issue
    put   "issues/:id",               to: "issues#update",        id: /\d+/
    delete "issues/:id",              to: "issues#destroy",       id: /\d+/

    get   "issues/:id/linked_closing_reference", to: "issues#linked_closing_reference", as: "linked_closing_reference"

    scope module: :issues do
      put     "issues/:id/assignees",     to: "assignees#update",         id: /\d+/, as: :assignees
      delete  "issues/:id/unassign_self", to: "assignees#unassign_self",  id: /\d+/, as: :unassign_self_from_issue
    end

    put   "issues/prioritize",        to: "pinned_issues#prioritize", as: :prioritize_pinned_issue
    post  "issues/:id/pin",           to: "pinned_issues#create",  as: :pin_issue
    delete "issues/:id/unpin",        to: "pinned_issues#destroy", as: :unpin_issue
    match "issues/:id/show_partial",  to: "issues#show_partial",  id: /\d+/,  as: :show_partial_issue, via: [:get, :post]
    put   "issues/:id/lock",          to: "issues#lock",          id: /\d+/,  as: :lock_issue
    put   "issues/:id/unlock",        to: "issues#unlock",        id: /\d+/,  as: :unlock_issue
    post  "issues/:id/transfer",      to: "issue_transfers#create", id:  /\d+/, as: :transfer_issue
    put   "issues/:id/set_milestone", to: "issues#set_milestone", id: /\d+/,  as: :set_milestone_issue
    post "issues/:id/duplicate",      to: "issues#unmark_as_duplicate", id: /\d+/, as: :unmark_issue_as_duplicate
    post "issues/:id/dismiss_first_contribution_prompt", to: "issues#dismiss_first_contribution_prompt", id: /\d+/, as: :dismiss_issue_first_contribution_prompt
    post "issues/:id/dismiss_first_contribution_prompt_and_redirect", to: "issues#dismiss_first_contribution_prompt_and_redirect", id: /\d+/, as: :dismiss_issue_first_contribution_prompt_and_redirect
    get  "issues/:id/show_from_project", to: "issues#show_from_project", id: /\d+/, as: :show_issue_from_project
    put  "issues/:id/labels", to: "issues/labels#update", id: /\d+/, as: :set_labels_issue
    get  "issues/:id/hovercard", to: "hovercards/issues_and_pull_requests#show", id: /\d+/
    put  "issues/:id/convert",  to: "issues#convert_to_discussion", id: /\d+/, as: :convert_issue

    ##
    # Issue template editor
    get "issues/templates/edit", to: "issue_templates#edit", as: :edit_issue_templates
    post "issues/templates/save", to: "issue_templates#create_many", as: :save_issue_templates
    post "issues/templates/preview", to: "issue_templates#preview", as: :preview_issue_template
    post "issues/templates/markdown_preview", to: "issue_templates#markdown_preview", as: :markdown_preview_issue_template

    ##
    # Issues clean URLs
    match "issues/created_by/*creator",   to: "issues#index",                      via: [:get, :post], as: :issues_created_by,  creator: APP_FILTER_REGEX
    match "issues/assigned/:assignee",    to: "issues#index",                      via: [:get, :post], as: :issues_assigned
    match "issues/mentioned/:mentioned",  to: "issues#index",                      via: [:get, :post], as: :issues_mentioned
    match "pulls/:login",                 to: "issues#index", pulls_only: true, via: [:get, :post], as: :user_pull_requests, login:   APP_FILTER_REGEX
    match "pulls/created_by/:creator",    to: "issues#index", pulls_only: true, via: [:get, :post], as: :pulls_created_by,   creator: APP_FILTER_REGEX
    match "pulls/assigned/:assignee",     to: "issues#index", pulls_only: true, via: [:get, :post], as: :pulls_assigned
    match "pulls/mentioned/:mentioned",   to: "issues#index", pulls_only: true, via: [:get, :post], as: :pulls_mentioned
    get   "pulls/review-requested/:review_requested",   to: "issues#index", pulls_only: true, as: :pulls_review_requested

    ##
    # Pull requests
    get   "pulls", to:  "issues#index", pulls_only: true, as: :pull_requests
    post  "pull/:id/ready_for_review", to: "pull_requests#ready_for_review", pulls_only: true, as: :pull_request_ready_for_review
    post  "pull/:id/convert_to_draft", to: "pull_requests#convert_to_draft", pulls_only: true, as: :pull_request_convert_to_draft
    post  "pull/:id/apply_suggestions", to: "pull_requests#apply_suggestions", as: :pull_request_apply_suggestions

    ##
    # Labels
    get     "labels",             to: "labels#index",                          as: :labels
    post    "labels",             to: "labels#create"
    get     "labels/:id/convert_issues_to_discussions",
      to: "convert_labelled_issues_to_discussions#show", id: /\d+/,
      as: :convert_labelled_issues_to_discussions
    post    "labels/:id/convert_issues_to_discussions",
      to: "convert_labelled_issues_to_discussions#create", id: /\d+/
    put     "labels/:id",         to: "labels#update",   id: /.+/
    delete  "labels/:id",         to: "labels#destroy",  id: /.+/
    get     "labels/preview/:name", to: "labels#preview", name: /.+/, as: :label_preview
    get     "labels/:label_name", to: "issues#index",    label_name: /.+/,  as: :label

    ##
    # Old school labels routes
    get    "issues/labels",     to: "labels#index"
    post   "issues/labels",     to: "labels#create"
    get    "issues/labels/:id", to: "labels#show",     id: /.+/
    put    "issues/labels/:id", to: "labels#update",   id: /.+/
    delete "issues/labels/:id", to: "labels#destroy",  id: /.+/

    ##
    # Catch-all for issues, same as "issues/created_by/:creator"
    get "issues/:creator",  to: "issues#index", login: APP_FILTER_REGEX

    ##
    # Issue comments
    post   "issue_comments",                        to: "issue_comments#create", as: :issue_comments
    put    "issue_comments/:id",                    to: "issue_comments#update", as: :issue_comment
    delete "issue_comments/:id",                    to: "issue_comments#destroy"
    get    "issue_comments/template_suggestions",   to: "issue_comments#template_suggestions"

    ##
    # Composable comments
    get "/composable_comments/:composable_comment_id", to: "composable_comments#show_partial", as: :show_composable_comments_partial

    # Component Kit
    get "/component_kit/templates/*template_path", to: "component_kit/template#show", template_path: /.+/, as: :component_kit_template

    ##
    # Issue permalinks
    # XXX: Legacy - this will 301 to /:user_id/:repository/issues/:id in the controller
    get    "issues/issue/:id", to: "issues#show", legacy: true

    get "generate", to: "clone_template_repositories#new", as: :clone_template_repository
    post "generate", to: "clone_template_repositories#create"

    ##
    # EditRepositoriesController
    get    "settings",                             to: "edit_repositories#options",               as: :edit_repository
    put    "settings/update",                      to: "edit_repositories#update",                as: :update_repository
    put    "settings/update_archive_settings",     to: "edit_repositories#update_archive_settings", as: :update_repository_archive_settings
    put    "settings/update_merge_settings",       to: "edit_repositories#update_merge_settings", as: :update_repository_merge_settings
    put    "settings/update_discussions_settings", to: "edit_repositories#update_discussions_settings", as: :update_repository_discussions_settings
    put    "settings/update_default_branch",       to: "edit_repositories#update_default_branch", as: :update_repository_default_branch
    put    "settings/update_wiki_settings",        to: "edit_repositories#update_wiki_settings",  as: :update_repository_wiki_settings
    put    "settings/update_wiki_access",          to: "edit_repositories#update_wiki_access",    as: :update_repository_wiki_access
    post   "settings/rename",                      to: "edit_repositories#rename",                as: :repo_rename
    post   "settings/transfer",                    to: "edit_repositories#transfer",              as: :repo_transfer
    get    "settings/transfer",                    to: "edit_repositories#transfer_team_suggestions",        as: :repo_transfer_team_suggestions
    post   "settings/abort_transfer",              to: "edit_repositories#abort_transfer",        as: :repo_abort_transfer
    put    "settings/update_meta",                 to: "edit_repositories#update_meta",           as: :update_repo_meta
    get    "settings/topics",                      to: "edit_repositories#topics",                as: :repo_topics
    put    "settings/update_topics",               to: "edit_repositories#update_topics",         as: :update_repo_topics
    put    "settings/update_member/:member_login", to: "edit_repositories#update_member",         as: :repository_update_member
    get    "settings/member_suggestions",          to: "edit_repositories#member_suggestions",    as: :repository_member_suggestions
    post   "settings/set_visibility",              to: "edit_repositories#set_visibility"
    put    "settings/change_anonymous_git_access", to: "edit_repositories#change_anonymous_git_access"
    delete "settings/remove_team",                 to: "edit_repositories#remove_team"
    delete "settings/remove_member",               to: "edit_repositories#remove_member"
    get    "settings/keys(/:id)",                  to: "edit_repositories#keys",                  as: :repository_keys, id: /\d+/
    get    "settings/keys/new",                    to: "edit_repositories#new_key",               as: :repository_new_key
    delete "settings/delete",                      to: "edit_repositories#delete"
    post   "settings/archive",                     to: "edit_repositories#archive"
    post   "settings/unarchive",                   to: "edit_repositories#unarchive"
    get    "settings/access",                      to: "edit_repositories#access",                as: :repository_access_management
    get    "settings/alerts",                      to: "edit_repositories#alerts",                as: :repository_alert
    put    "settings/alerts",                      to: "edit_repositories#update_alerts",         as: :edit_repository_alert
    get    "settings/security_analysis",           to: "edit_repositories#security_analysis",     as: :repository_security_and_analysis
    post   "settings/add_team",                    to: "edit_repositories#add_team"
    post   "settings/add_member",                  to: "edit_repositories#add_member"
    get    "settings/branches",                    to: "edit_repositories#branches",              as: :edit_repository_branches
    get    "settings/interaction_limits",          to: "edit_repositories#interaction_limits",    as: :repository_interaction_limits
    put    "settings/interaction_limits",          to: "edit_repositories#set_interaction_limit", as: :set_repository_interaction_limit
    put    "settings/projects",                    to: "edit_repositories#toggle_projects",       as: :toggle_repository_projects

    get    "settings/pages/unpublish",               to: "edit_repositories#repository_page",     as: :repository_page
    delete "settings/pages/unpublish",               to: "edit_repositories#unpublish_page",      as: :unpublish_page

    post    "members",                            to: "edit_repositories/manage_access#add_member",               as: :add_repository_member
    put     "settings/update_members",            to: "edit_repositories/manage_access#update_members",           as: :update_repository_members
    delete  "settings/remove_members",            to: "edit_repositories/manage_access#remove_members"
    get     "settings/toolbar_actions",           to: "edit_repositories/manage_access#members_toolbar_actions",  as: :members_toolbar_actions
    get     "settings/role_details",              to: "edit_repositories/manage_access#role_details",             as: :settings_role_details

    get "settings/og-template",                      to: "repository_open_graph_images#template",
      as: :repository_open_graph_template
    delete "settings/open-graph-image",              to: "repository_open_graph_images#destroy",
      as: :repository_open_graph_image

    scope path: "settings", module: :settings do
      resources :key_links do
        post "check", on: :collection
      end
    end

    scope path: "settings" do
      resources :branch_protection_rules, except: [:index]
    end

    ##
    # RepositoryNotificationSettingsController
    get    "settings/notifications",               to: "repository_notification_settings#index",   as: :repository_notification_settings
    get    "settings/notifications/edit",          to: "repository_notification_settings#edit",    as: :edit_repository_notification_settings
    put    "settings/notifications",               to: "repository_notification_settings#update",  as: :update_repository_notification_settings
    delete "settings/notifications",               to: "repository_notification_settings#destroy", as: :destroy_repository_notification_settings

    ##
    # RepositoryActionsSettingsController
    get    "settings/actions",                          to: "repository_actions_settings#index",                           as: :repository_actions_settings
    get    "settings/actions/list_runners",             to: "repository_actions_settings#list_runners",                    as: :repository_actions_settings_list_runners
    get    "settings/actions/add-new-runner",           to: "repository_actions_settings#add_runner",                      as: :repository_actions_settings_add_runner
    get    "settings/actions/delete_runner_modal/:id",  to: "repository_actions_settings#delete_runner_modal",             as: :repository_actions_settings_delete_runner_modal
    delete "settings/actions/runners/:id",              to: "repository_actions_settings#delete_runner",                   as: :repository_actions_settings_delete_runner
    put    "settings/actions/execution_capabilities",   to: "repository_actions_settings#update_execution_capabilities",   as: :update_repository_actions_execution_capabilities
    put    "settings/actions/fork_pr_workflows_policy", to: "repository_actions_settings#update_fork_pr_workflows_policy", as: :update_repository_fork_pr_workflows_policy
    get    "settings/actions/bulk_actions",             to: "repository_actions_settings#bulk_actions",                    as: :repo_runner_bulk_actions

    ##
    # Repos::ActionsSettings::RunnerLabelsController
    get    "settings/actions/labels",      to: "repos/actions_settings/runner_labels#index",       as: :repo_runner_labels
    post   "settings/actions/labels",      to: "repos/actions_settings/runner_labels#create",      as: :create_repo_runner_label
    get    "settings/actions/bulk_labels", to: "repos/actions_settings/runner_labels#bulk",        as: :repo_runner_bulk_labels
    put    "settings/actions/bulk_labels", to: "repos/actions_settings/runner_labels#bulk_update", as: :repo_runner_bulk_update_labels
    put    "settings/actions/labels",      to: "repos/actions_settings/runner_labels#update",      as: :update_repo_runner_label

    ##
    # SecretsController
    get    "settings/secrets",                     to: "secrets#secrets",               as: :repository_secrets
    delete "settings/secrets/:key",                to: "secrets#remove_secret",         as: :repository_remove_secret
    get    "settings/secrets/new",                 to: "secrets#new_secret",            as: :repository_new_secret
    post   "settings/secrets/new",                 to: "secrets#add_secret",            as: :repository_add_secret
    get    "settings/secrets/:secret_name",        to: "secrets#edit",                  as: :repository_edit_secret
    put    "settings/secrets/:secret_name",        to: "secrets#update",                as: :repository_update_secret

    ##
    # RepositoryInvitationsController
    post   "invite_member",               to: "repository_invitations#create"
    post   "uninvite_member",             to: "repository_invitations#destroy"

    ##
    # EditRepositoriesController redirects
    get "settings/delete", to: redirect("/%{user_id}/%{repository}/settings")
    get "admin",           to: redirect("/%{user_id}/%{repository}/settings")
    get "admin/:path",     to: redirect("/%{user_id}/%{repository}/settings/%{path}")
    get "edit",            to: redirect("/%{user_id}/%{repository}/settings")
    get "edit/:path",      to: redirect("/%{user_id}/%{repository}/settings/%{path}")
    get "settings/fine_grained", to: redirect("/%{user_id}/%{repository}/settings")
    # nb: this endpoint is for backwards compability while migrating to edit_repositories#access instead
    get "settings/collaboration", to: redirect("/%{user_id}/%{repository}/settings/access")

    ##
    # EditRepositoriesController environment specific routes
    if GitHub.custom_tabs_enabled?
      get    "settings/tabs",       to: "edit_repositories#tabs",       as: :repository_tabs
      post   "settings/add_tab",    to: "edit_repositories#add_tab"
      delete "settings/remove_tab", to: "edit_repositories#remove_tab"
    end

    ##
    # GeneratedPagesController
    post "generated_pages/create",  to: "generated_pages#create",  as: :generated_pages_create
    get  "generated_pages/new",     to: "generated_pages#new",     as: :generated_pages_new
    post "generated_pages/new",     to: "generated_pages#new"
    post "generated_pages/preview", to: "generated_pages#preview", as: :generated_pages_preview
    get  "generated_pages/themes",  to: "generated_pages#themes",  as: :generated_pages_themes
    post "generated_pages/themes",  to: "generated_pages#themes"

    ##
    # PagesController
    get  "settings/pages/status",         to: "pages#status",         as: :pages_status
    get  "settings/pages/certificate_status", to: "pages#certificate_status", as: :pages_certificate_status
    put  "settings/pages/https_status",   to: "pages#https_status",   as: :pages_https_status
    put  "settings/pages/visibility",     to: "pages#visibility",     as: :pages_visibility
    put  "settings/pages/cname",          to: "pages#cname",          as: :pages_cname
    put  "settings/pages/source",         to: "pages#source",         as: :pages_source
    put  "settings/pages/https_redirect", to: "pages#https_redirect", as: :pages_https_redirect
    get  "settings/pages/themes",         to: "pages#themes",         as: :pages_themes
    put  "settings/pages/theme",          to: "pages#theme",          as: :pages_theme
    put  "settings/pages/toggle_preview_deployment", to: "pages#toggle_preview_deployment", as: :pages_toggle_preview_deployment

    ##
    # BranchesController
    get    "branches",                       to: "branches#index",         as: :branches
    post   "branches",                       to: "branches#create",        as: :create_branch
    delete "branches/:name",                 to: "branches#destroy",       as: :destroy_branch,             name: /.+/
    get    "branches/yours",                 to: "branches#yours"
    get    "branches/active",                to: "branches#active"
    get    "branches/stale",                 to: "branches#stale"
    get    "branches/all",                   to: "branches#all"
    get    "branches/history(/:name)",       to: "branches#history",       as: :repository_branch_history,  name: BRANCH_REGEX
    post   "branches/recover",               to: "branches#recover",       as: :recover_branch
    get    "branches/pre_mergeable/:range",  to: "branches#pre_mergeable", as: :pre_mergeable,              range: /.+/
    post   "ref_check",                      to: "branches#ref_check",     as: :ref_check
    get    "branches/statuses",              to: "branches#statuses",      as: :branch_statuses, name: BRANCH_REGEX
    get    "branches/authorization_suggestions", to: "branch_authorization#suggestions", as: :branch_authorization_suggestions
    get    "branches/authorized_actor",      to: "branch_authorization#authorized_actor", as: :branch_authorized_actor
    # Show a nice feature retired message rather than a 404 to users who visit
    # the old base-switching branches page route:
    get    "branches/*name",                 to: "site#feature_gone"

    ##
    # Checks
    get  "commit/:ref/checks",                  to: "checks#index",                as: :checks
    get  "commit/:ref/checks/:id",              to: "checks#show",                 as: :checks_summary
    get  "commit/:ref/checks/:id/logs",         to: "checks#check_logs",           as: :check_run_logs
    get  "commit/:ref/checks/:id/live_logs",    to: "checks#live_logs",            as: :check_run_live_logs
    get  "commit/:ref/checks/:id/logs/:step",   to: "checks#step_logs",            as: :check_step_logs, step: /\d+/
    get  "commit/:ref/checks/:id/annotations",  to: "checks#annotations",          as: :checks_annotations
    get  "commit/:ref/rollup",                  to: "checks_statuses#rollup",      as: :checks_statuses_rollup
    get  "commit/:ref/checks_state_summary",    to: "checks#checks_state_summary", as: :checks_state_summary
    get  "runs/:id",                            to: "check_runs#show",             as: :check_run
    get  "runs/:id/header",                     to: "check_runs#show_header_partial",  as: :check_run_show_header_partial
    get  "runs/:id/toolbar",                    to: "check_runs#show_toolbar_partial",   as: :check_run_show_toolbar_partial
    get  "runs/:id/unseen_check_steps",         to: "check_runs#unseen_check_steps_partial",   as: :check_run_unseen_steps_partial
    get  "runs/:id/step_summary",               to: "check_runs#step_summary_partial",   as: :check_run_step_summary_partial
    put  "runs/:id/rerequest",                  to: "check_runs#rerequest",        as: :rerequest_check_run
    put  "runs/:id/request_action",             to: "check_runs#request_action",   as: :request_action_check_run
    put  "suites/:id/rerequest",                to: "check_suites#rerequest",      as: :rerequest_check_suite
    get  "suites/:id/show_partial",             to: "check_suites#show_partial",   as: :check_suite_show_partial
    put  "suites/:id/cancel",                   to: "check_suites#cancel",         as: :cancel_check_suite
    get  "suites/:id/artifacts/:artifact_id",   to: "check_suites#download_artifact",    as: :download_artifact
    delete  "suites/:id/artifacts/:artifact_id",   to: "check_suites#delete_artifact",    as: :delete_artifact
    get  "suites/:id/logs",                     to: "check_suites#download_logs",    as: :check_suite_logs

    ##
    # Commit
    get    "commit/:name/hovercard",            to: "hovercards/commits#show"
    get    "commit/:name/show_partial",         to: "commit#show_partial",   as: :show_partial_commit
    get    "commit/:name/_render_node/*path",   to: "nodes#show",            as: :show_commit_node_partial
    get    "commit/:name#{optional("/*path")}", to: "commit#show",           as: :commit, format: false
    get    "commit/:name.:format",              to: "commit#show"
    get    "branch_commits/:name",              to: "commit#branch_commits", as: :branch_commits
    get    "commit",                            to: "commit#show"
    put    "commit/:name/lock",                 to: "commit#lock",           as: :lock_commit
    put    "commit/:name/unlock",               to: "commit#unlock",         as: :unlock_commit

    ##
    # CommitsController
    get   "commits(/*name).:format", to: "commits#show", as: :commits_feed, constraints: { name: /.+/, format: "atom" }, defaults: { format: "atom" }
    get   "commits(/*name)",         to: "commits#show",                       constraints: { name: /.+/ }, format: false, defaults: { format: "html" }

    ##
    # Commit Comments
    delete "commit_comment/:id",    to: "commit_comments#destroy", id: /\d+/, as: :destroy_commit_comment
    put    "commit_comment/:id",    to: "commit_comments#update",  id: /\d+/, as: :update_commit_comment
    post   "commit_comment/create", to: "commit_comments#create",                as: :create_commit_comment

    ## Community
    #
    get  "community",                             to: "community#index",                as: :community
    get  "community/code-of-conduct/new",         to: "community#code_of_conduct_tool", as: :code_of_conduct_tool
    post "community/code-of-conduct",             to: "community#code_of_conduct",      as: :add_code_of_conduct
    get  "community/license/new",                 to: "community#license_tool",         as: :license_tool
    post "community/licenset",                    to: "community#license",              as: :add_license
    put  "community/minimize-comment",            to: "community#minimize_comment",        as: :minimize_comment
    put  "community/unminimize-comment",          to: "community#unminimize_comment",      as: :unminimize_comment

    ##
    # Compare
    get    "compare/branch-list", to: "compare#branch_list", as: :compare_branch_list
    get    "compare/tag-list",    to: "compare#tag_list",    as: :compare_tag_list
    match  "compare/:range", to: "compare#show",  as: :compare,        via: [:get, :post], constraints: { range: /.+/ }, format: false, defaults: { format: :html }
    match  "compare",        to: "compare#new",   as: :compare_start,  via: [:get, :post]

    ##
    # Diffs
    get "diffs/:entry", to: "diffs#show",  as: :diff, constraints: { entry: /\d+/ }
    get "diffs",        to: "diffs#index", as: :diffs

    get  "unchanged_files_with_annotations",      to: "diffs#unchanged_files_with_annotations",   as: :unchanged_files_with_annotations

    ##
    # Forking
    get    "fork",  to: "tree#fork_select",  as: :fork_select
    post   "fork",  to: "tree#fork",         as: :fork_repository

    ##
    # Graphs
    get    "graphs",                      to: "graphs#index",                  as: :repo_graphs
    get    "graphs/commit-activity",      to: "graphs#commit_activity",        as: :commit_activity
    get    "graphs/commit-activity-data", to: "graphs#commit_activity_data",   as: :commit_activity_data
    get    "graphs/clone-activity-data",  to: "graphs#clone_activity_data",    as: :clone_activity_data
    get    "graphs/code-frequency",       to: "graphs#code_frequency",         as: :code_frequency
    get    "graphs/code-frequency-data",  to: "graphs#code_frequency_data",    as: :code_frequency_data
    get    "graphs/languages",            to: "graphs#languages",              as: :repository_languages
    get    "graphs/punch-card",           to: "graphs#punch_card",             as: :punch_card
    get    "graphs/punch-card-data",      to: "graphs#punch_card_data",        as: :punch_card_data

    get    "graphs/traffic",              to: "graphs#traffic",                as: :traffic
    get    "graphs/traffic-data",         to: "graphs#traffic_data",           as: :traffic_data
    get    "graphs/contributors",         to: "graphs#contributors",           as: :contributors_graph
    get    "graphs/contributors-data",    to: "graphs#contributors_data",      as: :contributors_graph_data
    get    "graphs/clones",               to: "graphs#clones",                 as: :clones
    get    "graphs/clones-data",          to: "graphs#clones_data",            as: :clones_next_data
    get    "graphs/participation",        to: "repository_participation#show", as: :participation

    ##
    # Hooks
    get    "settings/hooks",                to: "repository_hooks#index",          as: :repository_hooks
    post   "settings/hooks",                to: "repository_hooks#create"
    get    "settings/hooks/new",            to: "repository_hooks#new",            as: :new_repository_hook
    get    "settings/hooks/:id",            to: "repository_hooks#show",           as: :repository_hook
    put    "settings/hooks/:id",            to: "repository_hooks#update"
    delete "settings/hooks/:id",            to: "repository_hooks#destroy"
    post   "settings/hooks/:id/tests",      to: "repository_hooks#test",           as: :test_repository_hook

    constraints(guid: WEBHOOK_GUID_REGEX, id: WEBHOOK_REGEX, hook_id: /\d+/) do
      get  "settings/hooks/:hook_id/deliveries",                     to: "hook_deliveries#index",     as: :repository_hook_deliveries, context: "repository"
      get  "settings/hooks/:hook_id/deliveries/:id",                 to: "hook_deliveries#show",      as: :repository_hook_delivery, context: "repository"
      get  "settings/hooks/:hook_id/deliveries/:id/payload.:format", to: "hook_deliveries#payload",   as: :repository_hook_delivery_payload, format: "json", context: "repository"
      post "settings/hooks/:hook_id/deliveries/:guid/redeliver",       to: "hook_deliveries#redeliver", as: :repository_redeliver_hook_delivery, context: "repository"
    end

    ##
    # Integrations (and legacy github-services)
    get    "settings/installations",             to: "repository_installations#index",        as: :repository_installations

    if GitHub.enterprise?
      post   "settings/hooks/:id/update_pre_receive",        to: "repository_hooks#update_pre_receive",      as: :update_repository_pre_receive
    end

    ##
    # Network
    get    "network",            to: "network#show",       as: :network
    get    "network/meta",       to: "network#meta",       as: :network_meta
    get    "network/chunk",      to: "network#chunk",      as: :network_chunk
    get    "network/members",    to: "network#members",    as: :network_members

    if GitHub.dependency_graph_enabled?
      get    "network/dependents", to: "network#dependents", as: :network_dependents
      get    "network/dependencies", to: "network#dependencies", as: :network_dependencies
      get    "network/updates", to: "network/dependabot#index", as: :network_dependabot
      post   "network/updates", to: "network/dependabot#create", as: :create_network_dependabot_run
      get    "network/updates/:update_job_id", to: "network/dependabot#show", as: :network_dependabot_show
      put    "network/updates/enable", to: "network/dependabot#enable", as: :network_enable_dependabot
      put    "network/dependencies/alerts/dismiss", to: "repository_vulnerability_alerts#dismiss", as: :vulnerability_alert_dismiss
      put    "network/dependencies/alerts/dismiss_many", to: "repository_vulnerability_alerts#dismiss_many", as: :dismiss_many_vulnerability_alerts
      post   "network/dependencies/vulnerabilities/enable_updates", to: "repository_vulnerability_updates#enable", as: :enable_vulnerability_updates
      post   "network/dependencies/vulnerabilities/disable_updates", to: "repository_vulnerability_updates#disable", as: :disable_vulnerability_updates
      put    "network/dependencies/enable", to: "edit_repositories#enable", as: :enable_content_analysis
    end

    if GitHub.dedicated_alerts_ui_enabled?
      get  "network/alerts", to: "repos/alerts#index", as: :network_alerts
      get  "network/alert/*manifest_path/:package_name/:state", to: "repos/alerts#show", as: :network_alert, manifest_path: /.+/, package_name: /.+?/, state: /(open|closed)/
      post "network/alert/*manifest_path/:package_name/:state/bot/resolve", to: "repos/alerts#bot_resolve", as: :bot_resolve_network_alert, manifest_path: /.+/, package_name: /.+?/, state: /(open)/
      get  "network/alert/*manifest_path/:package_name/:state/update-errors/:dependency_update_id", to: "repos/alerts#update_error", as: :network_alert_update_error, manifest_path: /.+/, package_name: /.+?/
    end

    if GitHub.repository_advisories_enabled?
      constraints(id: AdvisoryDB.valid_ghsa_id_pattern) do
        get  "security/advisories",             to: "repos/advisories#index",       as: :repository_advisories
        get  "security/advisories/new",         to: "repos/advisories#new",         as: :new_repository_advisory
        post "security/advisories",             to: "repos/advisories#create",      as: :create_repository_advisory
        get  "security/advisories/:id",         to: "repos/advisories#show",        as: :repository_advisory
        put  "security/advisories/:id",         to: "repos/advisories#update"
        put  "security/advisories/:id/body",    to: "repos/advisories#update_body", as: :update_repository_advisory_body
        put  "security/advisories/:id/publish", to: "repos/advisories#publish",     as: :publish_repository_advisory
        put  "security/advisories/:id/request_cve", to: "repos/advisories#request_cve", as: :request_cve_repository_advisory
        post "security/advisories/:id/add_credit",  to: "repos/advisories#add_credit",  as: :add_repository_advisory_credit
        put "security/advisories/:id/accept_credit", to: "repos/advisories#accept_credit", as: :accept_repository_advisory_credit
        put "security/advisories/:id/decline_credit", to: "repos/advisories#decline_credit", as: :decline_repository_advisory_credit
        put "security/advisories/:id/decline_credit_and_block_user", to: "repos/advisories#decline_credit_and_block_user", as: :decline_repository_advisory_credit_and_block_user

        put  "security/advisories/:id/open_workspace", to: "repos/advisories#open_workspace",
                                                       as: :open_repository_advisory_workspace

        get  "security/advisories/:id/workspace", to: "repos/advisories#workspace",  as: :advisory_workspace

        get  "security/advisories/:id/merge_box", to: "repos/advisories#merge_box",  as: :advisory_workspace_merge_box
        post "security/advisories/:id/merge",     to: "repos/advisories#merge",      as: :merge_advisory_workspace

        match "security/advisories/:id/show_partial", to: "repos/advisories#show_partial",
                                                      as: :show_partial_repository_advisory,
                                                      via: [:get, :post]

        get "security/advisories/:id/autocomplete_collaborator", to: "repos/advisories#autocomplete_collaborator", as: :autocomplete_repository_advisory_collaborator
        post "security/advisories/:id/collaborators", to: "repos/advisories#add_collaborator", as: :add_repository_advisory_collaborator
        delete "security/advisories/:id/collaborators", to: "repos/advisories#remove_collaborator", as: :remove_repository_advisory_collaborator

        get "security/advisories/:id/edit_history_log", to: "repos/advisories#edit_history_log", as: :repository_advisory_edit_history_log

        post   "security/advisories/:id/comments",             to: "repos/advisory_comments#create", as: :create_repository_advisory_comment
        put    "security/advisories/:id/comments/:comment_id", to: "repos/advisory_comments#update", as: :update_repository_advisory_comment
        delete "security/advisories/:id/comments/:comment_id", to: "repos/advisory_comments#destroy"

        get "security/advisories/:id/issue-suggestions", to: "repos/advisory_comment_suggestions#issues", as: :repository_advisory_issue_suggestions
        get "security/advisories/:id/mention-suggestions",  to: "repos/advisory_comment_suggestions#mentions",  as: :repository_advisory_mention_suggestions
      end

      get  "security/token-scanning", to: redirect("/%{user_id}/%{repository}/security/secret-scanning")
      get  "security/token-scanning/:id", to: redirect("/%{user_id}/%{repository}/security/secret-scanning/%{id}")
      get  "security/token-scanning/:id/files", to: redirect("/%{user_id}/%{repository}/security/secret-scanning/%{id}/files")
    end

    constraints(number: /\d+/) do
      get "security/code-scanning", to: "repos/code_scanning#index", as: :repository_code_scanning_results
      get "security/code-scanning/setup", to: "repos/code_scanning#setup", as: :repository_code_scanning_setup
      get "security/code-scanning/refs", to: "repos/code_scanning#ref_list", as: :repository_code_scanning_results_ref_list
      get "security/code-scanning/rules", to: "repos/code_scanning#rule_list", as: :repository_code_scanning_results_rule_list
      get "security/code-scanning/tags", to: "repos/code_scanning#tag_list", as: :repository_code_scanning_results_tag_list
      put "security/code-scanning/close", to: "repos/code_scanning#close", as: :repository_code_scanning_close
      put "security/code-scanning/reopen", to: "repos/code_scanning#reopen", as: :repository_code_scanning_reopen
      get "security/code-scanning/related-location", to: "repos/code_scanning#related_location_popover", as: :repository_code_scanning_related_location_popover
      get "security/code-scanning/:number", to: "repos/code_scanning#show", as: :repository_code_scanning_result
      get "security/code-scanning/:number/code-paths", to: "repos/code_scanning#code_paths", as: :repository_code_scanning_code_paths
      get "security/code-scanning/cwes/:id/hovercard", to: "hovercards/code_scanning#show_cwe"
    end

    unless GitHub.enterprise?
      get "security/policy",        to: "repos/security#policy",        as: :repository_security_policy
    end

    if GitHub.enterprise? && GitHub.configuration_secret_scanning_enabled? || (!GitHub.enterprise? && GitHub.repository_advisories_enabled?)
      get "security/secret-scanning", to: "repos/token_scanning#index", as: :repository_token_scanning_results
      put "security/secret-scanning/resolve", to: "repos/token_scanning#resolve", as: :repository_token_scanning_resolve

      constraints(id: /\d+/) do
        get "security/secret-scanning/:id", to: "repos/token_scanning#show", as: :repository_token_scanning_result
        get "security/secret-scanning/:id/files", to: "repos/token_scanning#show_files", as: :repository_token_scanning_result_files
      end
    end

    if GitHub.configuration_advanced_security_enabled?
      get "security",               to: "repos/security#overview",      as: :repository_security_overview
      get "security/overall-count", to: "repos/security#overall_count", as: :repository_security_overall_count
    end

    ##
    # PublicKeysController
    get    "deploy_keys",            to: "public_keys#index",   as: :deploy_keys
    post   "deploy_keys",            to: "public_keys#create"
    put    "deploy_keys/:id",        to: "public_keys#update",  as: :deploy_key,        id: /[^\/.?]+/
    delete "deploy_keys/:id",        to: "public_keys#destroy",                            id: /[^\/.?]+/
    post   "deploy_keys/:id/verify", to: "public_keys#verify",  as: :verify_deploy_key

    ##
    # Pull Request Review Comments
    delete "pull/:pull_id/review_comment/:id",          to: "pull_request_review_comments#destroy", id: /\d+/,  as: :destroy_review_comment
    put    "pull/:pull_id/review_comment/:id",          to: "pull_request_review_comments#update",  id: /\d+/,  as: :update_review_comment
    post   "pull/:pull_id/review_comment/create",       to: "pull_request_review_comments#create",                 as: :create_review_comment
    get    "pull/:pull_id/review_comment/:id/excerpt",  to: "pull_request_review_comments#excerpt", id: /\d+/,  as: :review_comment_excerpt
    get    "pull/:pull_id/review_comment/suggestion_button", to: "pull_request_review_comments#suggestion_button", as: :review_comment_suggestion_button
    get    "pull/:pull_id/review_comment/:id/actions",  to: "pull_request_review_comments#actions", as: :review_comment_actions

    put    "pull/:pull_id/threads/:thread_id/resolve", to: "pull_request_review_thread_resolutions#create", as: :resolve_review_thread
    delete    "pull/:pull_id/threads/:thread_id/unresolve", to: "pull_request_review_thread_resolutions#destroy", as: :unresolve_review_thread

    ##
    # Pull Request Review
    put    "pull/:pull_id/reviews", to: "pull_request_review_events#create", as: :update_review
    post   "pull/:pull_id/reviews/quick-approval", to: "pull_request_review_events#quick_approve", as: :quick_approve_review
    put    "pull/:pull_id/reviews/:review_id/update", to: "pull_request_reviews#update", as: :pull_request_review_update
    put    "pull/:pull_id/reviews/dismiss/:id", to: "pull_request_review_events#dismiss", as: :dismiss_review

    post "pull/:pull_id/file_review", to: "user_reviewed_files#create", as: :user_review_file
    delete "pull/:pull_id/file_review", to: "user_reviewed_files#destroy"

    get    "pull/:id/suggested-reviewers",        to: "suggested_reviewers#show", as: :suggested_reviewers
    get    "pull/new/suggested-reviewers/:range", to: "suggested_reviewers#show", as: :suggested_reviewers_range, range: /.+/

    post "pull/new/review-requests/:range", to: "review_requests#create_new", as: :create_review_request_new, range: /.+/
    get  "pull/new/review-requests/:range", to: "review_requests#menu", as: :reviewers_menu_content_new, range: /.+/
    post "pull/:id/review-requests", to: "review_requests#create", as: :create_review_request
    post "pull/:id/re-request-review", to: "review_requests#re_request_review", as: :re_request_review
    get  "pull/:id/review-requests", to: "review_requests#menu", as: :reviewers_menu_content

    ##
    # Pull Requests
    match  "pull/new/:range",       to: "pull_requests#new",                 as: :new_pull_request,           via: [:get, :post],  range: /.*/
    match  "pull/new",              to: "pull_requests#new",                                                     via: [:get, :post]
    post   "pull/create",           to: "pull_requests#create",              as: :create_pull_request

    if Rails.env.development?
      get    "merge_button_matrix",   to: "pull_requests#merge_button_matrix", as: :merge_button_matrix
    end

    # ConflictedFiles
    get    "pull/:id/conflict",              to: "conflicted_files#show"
    get    "pull/:id/conflicts",             to: "pull_requests#resolve_conflicts", as: :resolve_conflicts
    post   "pull/:id/conflicts/resolve",     to: "conflicted_files#resolve"

    get    "pull/:id/show_from_project", to: "issues#show_from_project", pulls_only: true, id: /\d+/, as: :show_pull_request_from_project
    match  "pull/:id/show_partial", to: "pull_requests#show_partial",        as: :show_partial_pull_request,  via: [:get, :post]
    get    "pull/:id/show_partial_comparison", to: "pull_requests#show_partial_comparison", as: :show_partial_pull_request_comparison
    get    "pull/:id/show_toc",     to: "pull_requests#show_toc",            as: :show_pull_request_toc
    post   "pull/:id/cleanup",      to: "pull_requests#cleanup",             as: :cleanup_pull_request
    post   "pull/:id/undo_cleanup", to: "pull_requests#undo_cleanup",        as: :undo_cleanup_pull_request
    post   "pull/:id/merge",        to: "pull_requests#merge",               as: :merge_pull_request
    post   "pull/:id/enqueue",      to: "pull_requests#enqueue",             as: :enqueue_pull_request
    post   "pull/:id/update_branch",  to: "pull_requests#update_branch",     as: :update_branch_pull_request
    post   "pull/:id/revert",       to: "pull_requests#revert",              as: :revert_pull_request
    match  "pull/:id/merge-button", to: "pull_requests#merge_button",        as: :merge_button,               via: [:get, :post]
    get    "pull/:id/commits/:sha/_render_node/*path", to: "nodes#show",     as: :show_pull_request_commits_node_partial
    get    "pull/:id/hovercard",    to: "hovercards/issues_and_pull_requests#show", id: /\d+/
    get    "pull/:id/changes_since_last_review", to: "pull_requests#changes_since_last_review", as: :pull_changes_since_last_review
    get    "pull/:id/:tab/:range",  to: "pull_requests#show",                as: :pull_request_files_with_range, id: /\d+/, tab: /commits|files/, range: /(?:[^.]|\.{2,})+/
    get    "pull/:id/:tab",         to: "pull_requests#show",                as: :pull_request_tab,     id: /\d+/ # WARNING This will take precedence over any GET `/pull/:id/foo` route defined after it
    get    "pull/*id.diff",         to: "pull_requests#diff",                as: :pull_request_diff,    id: /.+/
    get    "pull/*id.patch",        to: "pull_requests#patch",               as: :pull_request_patch,   id: /.+/
    get    "pull/*id",              to: "pull_requests#show",                as: :show_pull_request,    id: /.+/
    post   "pull/:id/comment",      to: "pull_requests#comment",             as: :comment_pull_request
    post   "pull/:id/dismiss_protip", to: "pull_requests#dismiss_protip",    as: :dismiss_protip_pull_request, id: /.+/
    post   "pull/:id/change_base",  to: "pull_requests#change_base",         as: :change_pull_base
    post   "pull/:id/set_collab", to: "pull_requests#set_collab"


    ##
    # Issue and PR timelines
    get    "timeline", to: "timeline#show", as: :show_timeline
    get    "related_repositories", to: "related_repositories#index", as: :related_repositories

    ##
    # Releases / Tags
    get    "releases/download/*name(/*path)", to: "releases#download", as: :download_release, name: /.+/, format: false
    get    "releases/new",                    to: "releases#new",      as: :new_release
    post   "releases",                        to: "releases#create",   as: :create_release
    post   "releases/preview",                to: "releases#preview",  as: :preview_release

    # new routes supporting tag names with /
    get    "releases/edit/*name", to: "releases#edit",     as: :edit_release,     name: /.+/
    put    "releases/tag/*name",  to: "releases#update",   as: :update_release,   name: /.+/
    delete "releases/tag/*name",  to: "releases#destroy",  as: :destroy_release,  name: /.+/
    get    "releases/tag/*name",  to: "releases#show",     as: :show_release,     name: /.+/
    get    "releases/latest",     to: "releases#latest",   as: :latest_release
    get    "releases/latest/download(/*path)", to: "releases#download_latest", format: false

    # legacy routes supporting tag slugs
    slug_regex = /[^\/]+/
    get    "releases/:name/edit",                   to: "releases#edit",     name: slug_regex
    put    "releases/:name",                        to: "releases#update",   name: slug_regex
    delete "releases/:name",                        to: "releases#destroy",  name: slug_regex
    get    "releases/:name",                        to: "releases#show",     name: slug_regex
    get    "releases/:name/:asset_id/:unused_name", to: "releases#download", name: slug_regex, unused_name: /.+/, asset_id: /\d+/

    get    "releases",      to: "releases#index",        as: :releases
    get    "releases.atom", to: "releases#index",        as: :releases_feed,  format: "atom"
    get    "tags/check",    to: "releases#check_tag",    as: :check_tag
    post   "tags",          to: "releases#create_tag",   as: :create_tag
    get    "tags",          to: "releases#tag_history",  as: :tags
    get    "tags.atom",     to: "releases#tag_history",  as: :tags_feed,      format: "atom"

    ##
    # Search
    get    "search",            to: "repository_search#index", as: :repo_search
    get    "search/count",      to: "repository_search#count", as: :repo_search_count
    get    "search/gds",        to: "repository_search#dark_ship", as: :repo_search_geyser_dark_ship
    put    "search/toggle_ems", to: "repository_search#toggle_ems", as: :repo_search_geyser_toggle_ems

    get    "upload/(:name)(/*path)", to: "repository_uploads#index", as: :repo_uploads, name: /.+/
    post   "upload",                 to: "repository_uploads#create"
    delete "upload",                 to: "repository_uploads#destroy"

    ##
    # DownloadsController
    get    "downloads",       to: "downloads#index",   as: :downloads
    get    "downloads/:id",   to: "downloads#show",    as: :download,  id: /[^\/.?]+/
    delete "downloads/:id",   to: "downloads#destroy",                    id: /[^\/.?]+/

    ##
    # NotificationsController
    get   "notifications",
            to: redirect { |params, request|
              if request.query_parameters[:list_type] == "team"
                "/notifications?query=is%3Ateam-discussion"
              else
                "/notifications?query=repo%3A#{params[:user_id]}%2F#{params[:repository]}"
              end
            },
            constraints: NOTIFICATIONS_V2_REDIRECT_CONSTRAINT,
            as: :repository_notifications_redirect

    get   "subscription",                 to: "notifications#subscription",          as: :repository_subscription
    get   "unsubscribe_via_email/:data",  to: "notifications#unsubscribe_via_email", as: :repository_unsubscribe_via_email
    get   "notifications",                to: "notifications#index",                 as: :repository_notifications
    post  "notifications/mark",           to: "notifications#mark_as_read",          as: :mark_list_notifications

    ##
    # WikiController
    post   "wiki/_preview",                     to: "wiki#preview",                          id: /[^\/]+/, version_list: /.*/
    get    "wiki",                              to: "wiki#index",        as: :wikis
    post   "wiki",                              to: "wiki#create",       as: nil
    put    "wiki/:id",                          to: "wiki#update",                           id: /[^\/]+/
    delete "wiki/:id",                          to: "wiki#destroy",                          id: /[^\/]+/
    post   "wiki/:id/_compare(/:version_list)", to: "wiki#compare",                          id: /[^\/]+/, version_list: /.*/
    get    "wiki/:id/_compare(/:version_list)", to: "wiki#compare",                          id: /[^\/]+/, version_list: /.*/
    get    "wiki/_history",                     to: "wiki#history"
    post   "wiki/_compare(/:version_list)",     to: "wiki#compare",                                                version_list: /.*/
    get    "wiki/_compare(/:version_list)",     to: "wiki#compare",                                                version_list: /.*/
    get    "wiki/_pages",                       to: "wiki#pages",        as: :wiki_pages
    get    "wiki/_new",                         to: "wiki#new",          as: :new_wiki
    post   "wiki/_revert/:older/:newer",        to: "wiki#revert"
    get    "wiki/:id/_edit",                    to: "wiki#edit",         as: :edit_wiki,  id: /[^\/]+/
    get    "wiki/:id/_current",                 to: "wiki#current",      as: :wiki_current, id: /[^\/]+/
    get    "wiki/:id/_history",                 to: "wiki#history",                          id: /[^\/]+/
    post   "wiki/:id/_revert/:older/:newer",    to: "wiki#revert",                           id: /[^\/]+/
    get    "wiki/:id",                          to: "wiki#show",         as: :wiki,       id: /[^\/]+/, defaults: { format: "html" }
    get    "wiki/:id/:version(/*path)",         to: "wiki#show",                             id: /[^\/]+/, format: false
    get    "wiki/*path",                        to: "wiki#show",                                              format: false
    get    "wikis(/*args)",                     to: "wiki#redirect"

    ##
    # Repositories
    match  "archives/*name",        to: "site#archive_redirect", as: :archives,       via: [:get, :post]
    match  "zipball/:name",         to: "tree#zipball",          as: :zipball_legacy, via: [:get, :post], name: /.+/
    match  "tarball/:name",         to: "tree#tarball",          as: :tarball_legacy, via: [:get, :post], name: /.+/
    match  "archive/*name.tar.gz",  to: "tree#archive",          as: :tarball,        via: [:get, :post], name: /.+/, format: false, _format: "tar.gz"
    match  "archive/*name.zip",     to: "tree#archive",          as: :zipball,        via: [:get, :post], name: /.+/, format: false, _format: "zip"

    get    "check",                     to: "tree#check",                    as: :check_repo
    post   "dismiss-tree-finder-help",  to: "tree#dismiss_tree_finder_help", as: :dismiss_list_help
    match  "find/:name",                to: "tree#find",                     as: :tree_find,    name: /.+/,  via: [:get, :post]
    match  "tree-list/:name",           to: "tree#list",                     as: :tree_list,    name: /.+/,  via: [:get, :post]

    # Aleph code navigation routes
    get    "find-symbols",              to: "code_nav#definition",        as: :code_nav_symbols
    get    "find-definition",           to: "code_nav#definition",        as: :code_nav_definition
    get    "find-references",           to: "code_nav#references",        as: :code_nav_references

    get "tree/delete/*name(/*path)", to: "tree#delete", as: :delete_directory, name: /(.|\n)+/, format: false, defaults: { format: :html }
    delete "tree/*name(/*path)", to: "tree#destroy", as: :destroy_directory, name: /(.|\n)+/, format: false, defaults: { format: :html }

    # nb: this endpoint is for backwards compability while migrating to refs#index instead
    get    "ref-list/*name(/*path)",      to: "refs#index",          as: :ref_list,           name: /.+/,                          format: false, defaults: { format: :html }
    get    "refs/*name(/*path)",          to: "refs#index",          as: :refs,               name: /.+/,                          format: false, defaults: { format: :html }
    get    "refs-tags/*name(/*path)",     to: "refs#tags",           as: :refs_tags,          name: /.+/,                          format: false, defaults: { format: :html }

    get    "tree/*name(/*path)",          to: "files#disambiguate",  as: :tree,               name: /.+/,                          format: false, defaults: { format: :html }
    get    "tree-commit/:name(/*path)",   to: "tree#tree_commit",    as: :tree_commit,        name: /.+/,                          format: false, defaults: { format: :html }
    get    "file-list/:name(/*path)",     to: "files#file_list",     as: :file_list,          name: /.+/,                          format: false, defaults: { format: :html }
    match  "blame/*name(/*path)",         to: "blob#blame",          as: :blame,              name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    match  "tree-save/*name(/*path)",     to: "blob#save",           as: :file_save,          name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    match  "new/*name(/*path)",           to: "blob#new",            as: :new_file,           name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    match  "create/*name(/*path)",        to: "blob#create",         as: :create_file,        name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    match  "edit/*name(/*path)",          to: "blob#edit",           as: :file_edit,          name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    match  "delete/*name(/*path)",        to: "blob#delete",         as: :delete_file,        name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    delete "blob/*name(/*path)",          to: "blob#destroy",        as: :destroy_file,       name: /(.|\n)+/,                          format: false, defaults: { format: :html }
    match  "file-edit/:name(/*path)",     to: "blob#edit",                                    name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    match  "preview/*name(/*path)",       to: "blob#preview",        as: :preview_edit,       name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    match  "raw/*name(/*path)",           to: "blob#raw",            as: :raw_blob,           name: /(.|\n)+/,  via: [:get, :post],  format: false, defaults: { format: :html }
    get    "blob/*name(/*path)",          to: "blob#show",           as: :blob,               name: /(.|\n)+/,                     format: false, defaults: { format: :html }
    get    "codeowners/*name(/*path)",    to: "blob#codeowners",     as: :codeowners,         name: /(.|\n)+/,                     format: false, defaults: { format: :html }

    get "contributors/*name(/*path)", to: "blob#contributors", as: :blob_contributors, name: /(.|\n)+/, format: false, defaults: { format: :html }
    get "contributors-list/*name(/*path)", to: "blob#contributors_list", as: :blob_contributors_list, name: /(.|\n)+/, format: false, defaults: { format: :html }

    match  "blob_excerpt/:oid",           to: "blob#excerpt",        as: :blob_excerpt,       oid: /[0-9a-f]{40}/, via: [:get, :post]

    # Noodle
    get  "workspace(/*name(/*path))",       to: "noodle#workspace",    as: :noodle_workspace,          name: /.+/,  format: false

    get    "files/:id(/*path)",   to: "repository_files#show", as: :repository_file, format: false

    get    "hovercard",           to: "hovercards/repositories#show",      as: :repository_hovercard
    post   "star",                to: "tree#star",                         as: :star_repository,        via: [:get, :post]
    post   "unstar",              to: "tree#unstar",                       as: :unstar_repository,      via: [:get, :post]
    match  "watchers",            to: "repositories#watchers",             as: :watchers,               via: [:get, :post]
    match  "stargazers",          to: "repositories#stargazers",           as: :stargazers_repository,  via: [:get, :post]
    match  "stargazers/you_know", to: "repositories#stargazers_you_know",  as: :stargazers_you_know,    via: [:get, :post]
    match  "contributors",        to: "repositories#contributors",         as: :contributors,           via: [:get, :post]
    match  "contributors_list",   to: "repositories#contributors_list",    as: :contributors_list,      via: [:get, :post]
    match  "used_by_list",        to: "repositories#used_by_list",         as: :used_by_list,           via: [:get, :post]
    match  "sponsors_list",       to: "repositories#sponsors_list",        as: :sponsors_list,          via: [:get, :post]
    match  "environment_status",  to: "repositories#environment_status",   as: :environment_status,     via: [:get, :post]
    match  "packages_list",       to: "repositories#packages_list",        as: :packages_list,          via: [:get, :post]
    match  "show_partial",        to: "tree#show_partial",                 as: :show_partial_tree,      via: [:get, :post]
    match  "noooooope",           to: "repositories#no_content",           as: :no_content,             via: [:get, :post]
    if GitHub.enterprise?
      get    "(/*gopkgpath)", to: "repositories#go_metatag", as: :go_tag, constraints: { query_string: "go-get=1" }
    end

    ##
    # Community contributors
    get    "community_contributors",           to: "repositories#community_contributors",          as: :community_contributors
    get    "community_contributors/hovercard", to: "hovercards/community_contributors#show",       as: :community_contributors_hovercard
    get    "community_contributors/hovercard_contents", to: "hovercards/community_contributors#list", as: :list_community_contributors_hovercard

    match  "pulse(/:period)",                 to: "repositories#pulse",                as: :pulse,                period: "weekly",  via: [:get, :post]
    match  "pulse_committer_data(/:period)",  to: "repositories#pulse_committer_data", as: :pulse_committer_data, period: "weekly",  via: [:get, :post]
    get  "pulse_diffstat_summary",  to: "repositories#pulse_diffstat_summary", as: :pulse_diffstat_summary

    get    "people",                           to: "repos/access#index",                   as: :repo_people_index
    get    "people/export",                    to: "repos/access#export",                  as: :repo_people_export

    match  "deployments", to: "repositories#deployments", as: :deployments, via: [:get]
    match  "full_associated_pulls/:deployment_id", to: "repositories#full_associated_pulls", as: :full_associated_pulls, via: [:get]
    match  "compact_associated_pulls/:deployment_id", to: "repositories#compact_associated_pulls", as: :compact_associated_pulls, via: [:get]
    match  "deployments/activity_log", to: "repositories#deployments_activity_log", as: :deployments_activity_log, via: [:get]

    match "deployments_environment", to: "repositories#deployments_environment_state", as: :deployments_environment_state, via: [:get]

    # legacy suggestion routes for backwards compatiblity of already loaded pages.
    # TODO: Remove after a while.
    get    "suggestions(/:subject_type(/:subject_id))", to: "suggestions#show"

    get    "", to: "files#disambiguate", as: :repository, format: false, defaults: { format: :html }

    # Topics
    get "topic_suggestions",          to: "topic_suggestions#index",   as: :repository_topic_suggestions
    get "topic_autocomplete",         to: "topics#autocomplete",       as: :topic_autocomplete
    post "topic_suggestions/decline", to: "topic_suggestions#decline", as: :decline_topic_suggestion
    post "topic_suggestions/accept",  to: "topic_suggestions#accept",  as: :accept_topic_suggestion

    # Sponsor Funding Links
    resource :funding_links, only: [:show]

    # Contribute to owner/repo-name
    resource :contribute_page, only: [:show], path: "contribute"

    # Opt in to view objectionable content
    post "opt_in", to: "repositories#opt_in_to_view", as: :opt_in_to_view

    # Tiered Reporting routes
    post "report_content", to: "abuse_reports#create"
    get "reported_content", to: "edit_repositories#reported_content"
    get "abuse_reporters", to: "edit_repositories#abuse_reporters"
    put "toggle_tiered_reporting", to: "edit_repositories#toggle_tiered_reporting"
    put  "resolve_abuse_reports",   to: "abuse_reports#resolve_abuse_reports"
    put  "unresolve_abuse_reports", to: "abuse_reports#unresolve_abuse_reports"

    # Merge queue
    resource :merge_queue, only: [:show], path: "queue" do
      resources :entries, only: [:index, :create, :destroy], module: :merge_queues do
        resource :solo_setting, only: [:create]
      end
    end
  end

  ##
  # Standalone Patch and Diff URLs
  scope repository: REPO_REGEX, path: "/raw/:user_id/:repository", format: false do
    get "/pull/:id.diff",  controller:  "patch_diff", action: "raw_diff",  id: /.+/, as: :pull_request_raw_diff
    get "/pull/:id.patch", controller:  "patch_diff", action: "raw_patch", id: /.+/, as: :pull_request_raw_patch
  end

  unless GitHub.enterprise?
    get "editor/actions/marketplace-search", to: "editor/actions#index", as: "editor_actions_search"
    get "editor/actions/marketplace/:action_id", to: "editor/actions#show", as: "editor_action"
  end

  ##
  # Hook Deliveries, legacy path
  constraints(guid: WEBHOOK_GUID_REGEX, id: WEBHOOK_REGEX, hook_id: /\d+/) do
    get  "/hooks/:hook_id/deliveries",                     to: "hook_deliveries#index",     as: :hook_deliveries
    get  "/hooks/:hook_id/deliveries/:id",                 to: "hook_deliveries#show",      as: :hook_delivery
    get  "/hooks/:hook_id/deliveries/:id/payload.:format", to: "hook_deliveries#payload",   as: :hook_delivery_payload, format: "json"
    post "/hooks/:hook_id/deliveries/:guid/redeliver",       to: "hook_deliveries#redeliver", as: :redeliver_hook_delivery
  end

  ##
  # Surveys
  post "/survey-responses", controller: "survey_responses", action: "update", as: :survey_responses

  if Rails.env.test?
    match "/low_risk_sudo",    to: "sudo_test#low_risk_sudo",    via: [:get, :post, :put, :delete]
    match "/2fa_sudo",         to: "sudo_test#two_factor_sudo",  via: [:get, :post, :put, :delete]
    match "/redirect_to_back", to: "sudo_test#redirect_to_back", via: [:get, :post, :put, :delete]

    get "/render_safeguard/test/with_respond_to", to: "render_safeguard#show_with_respond_to"
    get "/render_safeguard/test/without_respond_to", to: "render_safeguard#show_without_respond_to"
  end

  get "/:user_id", to: "profiles#show", user_id: USERID_REGEX, as: :user

  # The only way to get consistent 404 rendering in Rails 3+ is to define a
  # catch all route. This ensures that all bad routes are rendered using the
  # same `render_404` logic as other 404s in the application.
  #
  # WARNING: This route must be the last route in the routes file.
  match "*unmatched_route", to: "site#routing_error", via: [:get, :post, :put, :patch, :delete]
end
