# frozen_string_literal: true
# rubocop:disable Bundler/OrderedGems, Bundler/DuplicatedGem
# bootstrap version: v10 (rev to force rebundling on deploy / checkout)

# grpc and google-protobuf are very picky about their Ruby version
# and won't install on edge versions. We need this so we can compile
# Ruby from the master branch and bundle successfully.
if ENV["RUBY_NEXT"] || ENV["JANKY_ENV_RUBY_SHA"]
  module BundlerHack
    def __materialize__
      if name == "grpc" || name == "google-protobuf"
        Bundler.settings.temporary(force_ruby_platform: true) do
          super
        end
      else
        super
      end
    end
  end
  Bundler::LazySpecification.prepend(BundlerHack)
end

source "https://rubygems.org"

# Configure the gems that should not be cleaned out between bundles.
# This list is defined in `config/bundler_persistent_gems.rb` and includes:
# 1. Rails gems that change between Rails versions
# 2. Other libraries that change versions between Rails versions
require_relative "config/bundler_persistent_gems"
Bundler.settings.temporary(persistent_gems_after_clean: BUNDLER_PERSISTENT_GEMS_AFTER_CLEAN)

if ENV["RAILS_NEXT"] == "1"
  gem "rails", "6.1.0.alpha.7c32f09"
else
  gem "rails", "6.0.3.2.af01810"
end

gem "adaptive_cards",      "1.2.0.rea0368e"
gem "aleph-client",        "2.1.0.r5c076d69"
gem "allocation_sampler",  "1.0.0"
gem "acme-client",         "2.0.6"
gem "addressable",         "~> 2.3"
gem "asciidoctor",         "2.0.10.131.g4284a176"
gem "authzd-client",       "0.9.2.raf966021"
gem "aws-sdk-s3",          "~> 1"
gem "bcrypt"
gem "bertrpc",             "1.3.1-github9"
gem "bert",                "1.1.10.45.gf6727fc"
gem "bindata",             "2.4.6"
gem "bootsnap",            "1.4.6"
gem "braintree",           "~> 2.80"
gem "builder",             "3.2.3"
gem "chatops-controller",  "4.0.1"
gem "chatterbox-client",   "2.1.3.gd04a86a"
gem "charlock_holmes",     "0.7.7"
gem "chroma",              "~> 0.2.0"
gem "coconductor",         "~> 0.9"
gem "codeowners",          "0.0.1.g6d81e53"
gem "color",               "1.5.1"
gem "committee",           "3.2.0"
gem "commonmarker",        "0.21.0.2.g91d4548"
gem "concurrent-ruby-ext"
gem "creole",              "0.3.6"
gem "darrrr",              "0.1.6"
gem "diff-lcs",            "1.1.2"
gem "divvy",               "1.1.4.g677870a"
gem "docusign_esign",      "~> 3.0.0"
gem "dogstatsd-ruby",      "~> 4.1.0"
gem "dogapi",              "~> 1.28.0"
gem "driftwood-client",    "0.0.1.r7a2f4095"
gem "duo_api",             "1.0.1"
gem "earthsmoke",          "1.2.5"
gem "ecma-re-validator",   "~> 0.1"
gem "ed25519",             "~> 1.2"
gem "egress",              "0.0.8"
gem "email_reply_parser",  "0.5.10"
gem "elastomer-client",    "~> 3.2"
gem "escape_utils",        "1.2.1"
gem "ernicorn",            "1.0.3", :path => "vendor/internal-gems/ernicorn"
gem "eventer-client",      "0.4.2.g50ee773"
gem "excon",               "~>0.71"
gem "failbot",             "2.5.3"
gem "faker",               "~> 2.11.0"
gem "faraday",             "~> 0.17.0"
gem "fast_blank",          "1.0.0"
gem "flipper",             "0.17.2"
gem "freno-client",        "~> 0.8.1"
gem "gemoji",              "4.0.0.rc1"
gem "geoip2_compat",       "0.0.3"
gem "geo_pattern",         "~> 1.2.1"
gem "get_process_mem",     "~> 0.2.5"
gem "geyser-client",       "0.14.11.r65c73334"
gem "gibbon",              "2.2.3"
gem "github-ds",           "0.4.0.2.g8cfaed9"
gem "github-launch",       "0.14.0.6920.g7c5bb34d2"
gem "github-linguist",     "7.10.0"
gem "github-markup",       "3.0.4"
gem "github-proto-actions", "1.0.0.r0536fcf"
gem "github-pages-health-check", "1.16.1.8.g5aea803"
gem "github-proto-repositories", "1.2.10.rcfddc4e"
gem "github-proto-users",        "1.1.9.r1d24950"
gem "github-proto-features",     "0.0.1.gf480608"
gem "github-proto-auditlogs",    "0.0.1.gf480608"
gem "github-proto-hooks",        "0.0.1.g0b43f15"
gem "github-proto-support",      "1.1.1.r1d24950"
gem "github-proto-policy",       "1.0.0.r8e0315f"
gem "github-proto-packageregistry", "1.2.3.r1736834"
gem "github-proto-insights",  "1.0.1.rff71795"
gem "github-proto-secretscanning",  "1.1.0.r7c62210"
gem "github_chatops_extensions", "0.0.2"
gem "gitrpc",              :path => "vendor/gitrpc"
gem "google-api-client",   "~> 0.39"
gem "goomba",              "0.0.1.399.g354dff8"
gem "gpgme",               "~> 2.0"
gem "graphql",             "1.10.10"
gem "graphql-batch",       "0.4.3"
gem "graphql-client",      "0.16.0"
gem "graphql-relay-walker", "0.3.0"
gem "graphql-schema_comparator", "1.0.0"
gem "group_syncer",        "1.6.3.r8527e3cb"
gem "grpc",                "1.31.0.pre2"
gem "google-protobuf",     "~> 3.12"
gem "googleapis-common-protos-types", "~> 1.0.1"
gem "hana",                "~> 1.3.6"
gem "hiredis",             "0.6.3"
gem "homograph-detector",  "~> 0.1.1"
gem "horcrux",             "0.1.2"
gem "http_parser.rb",      "~> 0.5.2"
gem "inline_svg",          "1.3.1"
gem "ipaddress"
gem "jwt",                 "~> 1.5"
gem "json_schema",         "~> 0.20.0"
gem "levenshtein-ffi",     "~>1.1"
gem "lz4-ruby",            "~> 0.3.3"
gem "parser",              "~> 2.7.0"
gem "licensee",            "~> 9.10"
gem "lightstep",           "0.12.0.pre.github"
gem "lmdb",                "~> 0.4.8"
gem "lru_redux",           "0.8.4.14.g99d8d60"
gem "mail",                "~> 2.6"
gem "marginalia",          "1.8.0.8.gacc4f1a", :require => false
gem "memcached",           "1.8.0"
gem "mochilo",             "1.3.5.g70afb07"
gem "monetize",            "1.9.4"
gem "money-open-exchange-rates", "~> 1.3"
gem "money",               "~> 6.13.0"
gem "msgpack",             "~> 1.0"
gem "mustache",            "0.99.4"
gem "net-http-persistent", "~> 3.0.0"
gem "nokogiri",            ">= 1.10.4"
gem "oauth",               "0.4.6.github"
gem "oauth2",              "~> 1.4.1"
gem "octolytics",          "0.18.0.1.gaf19991"
gem "octotracer",          "0.0.5.10.g034c2ab"
gem "octicons_helper",     "10.0.0"
gem "oily_png",            "1.1.0"
gem "optimizely-sdk",      "3.3.1"
gem "org-ruby",            "0.9.12"
gem "pathspec",            "~> 0.1.2"
gem "phonelib",            "~> 0.6.22"
gem "pinglish",            "0.2.1"
gem "posix-spawn",         "0.3.14.ra37695d"
gem "prawn",               "2.2.2"
gem "prawn-svg",           "0.30.0"
gem "prawn-table",         "0.2.2"
gem "premailer",           "1.13.1"
gem "presto-client",       "0.5.2"
gem "prometheus_exporter", "0.5.3", :require => false
gem "prose_diff",          "0.3.1"
gem "proto-dependabot-api", "0.0.9.r18ebd746"
gem "proto-policies",      "0.1.4.230.g7b35be3"
gem "proto-registry-metadata-api", "0.4.4.r20ec0e0a"
gem "public_suffix",       "~> 4.0"
gem "rack",                "2.1.4"
gem "rack-bug",            "0.3.0"
gem "rails-controller-testing", "1.0.5"
gem "rake",                "12.3.3"
gem "rblineprof",          "0.3.7", :platforms => :mri
gem "rbnacl",              "~> 7.0"
gem "rbtrace",             "0.4.10", :platforms => :mri
gem "rdoc",                "6.2.1"
gem "RedCloth",            "4.3.2"
gem "redis",               "~> 4.0"
gem "resilient",           "~> 0.4.0"
gem "resque",              "1.20.0.74.g5be47e2"
gem "resqued",             "0.10.0"
gem "rinku",               "2.0.6"
gem "rollup",              "0.4.0.7.gf8ee1df"
gem "rotp",                "~> 6.0.0"
gem "rqrcode",             "0.10.1"
gem "ruby-progressbar",    "~>1.7", :require => false
gem "rubypants",           "0.2.0"
gem "safe_yaml",           "1.0.4"
gem "sourcemap",           "0.1.1"
gem "sanitize",            "5.1.0"
gem "scientist",           "1.3.0"
gem "scrolls",             "0.3.8"
gem "secure_headers",      "6.3.0"
gem "ssh_data",            "1.1.0"
gem "simple_lockfile",     "~> 1.1.1"
gem "simple_uuid",         "~> 0.3.0"
gem "sinatra",             "2.0.8.1"
gem "slop",                "~> 3.4"
gem "spamurai-dev-kit",    "1.0.0"
gem "stackprof",           "0.2.15", :platforms => :mri # :mri_21
gem "statsd-ruby",         "0.3.0.github.3.38.gd478cc7"
gem "stripe",              "5.22.0"
gem "terminal-notifier",   "1.4.2"
gem "terminal-table",      "~>1.6.0"
gem "thread_safe",         "0.3.5.c8158c9"
gem "token-scanning-client", "1.0.0.r142cbe3"
gem "treelights-client",   "0.1.1.gf3f412d"
gem "trilogy",             "1.0.1"
gem "trilogy_adapter",     "1.0.0.r8da7c58"
gem "turboscan-client",    "1.0.0.r46acac25"
gem "twilio-ruby",         "3.11.5"
gem "twirp",               "~> 1.1"
gem "u2f",                 "1.0.0"
gem "unicorn",             "4.8.3"
gem "browser",             "3.0.2"
gem "venice",              "0.6.0"
gem "verdict",             "0.3.1"
gem "version_sorter",      "2.2.4"
gem "view_component",      "2.17.1"
gem "vintage-client",      "0.0.1.gff2b1a4"
gem "wcag_color_contrast", "~> 0.1.0"
gem "webauthn",            "2.1.0"
gem "webpush",             "0.3.2"
gem "wikicloth",           "0.8.1.github5.2.g1bdbe36"
gem "will_paginate",       "~> 3.1"
gem "yajl-ruby",           "~> 1.3"
gem "hydro-client",        "0.3.1.73.gae9d181"
gem "zuorest",             "2.2.0"
gem "aqueduct-client",     "0.1.0.r4adff2a"
gem "ipaddr",              "1.2.2"
gem "active_record_has_many_split_through", "0.1.0.r8efb4c3"

gem "badge-ruler", "0.0.1"
gem "service-catalog-client", "0.4.0"

# enterprise specific gems go here for now
#
# NOTE: the `oa-enterprise` gem is built from the "0-3-stable" branch
#       of the https://github.com/github/omniauth repository
gem "github-ldap",   "1.10.1.7.g5a50c9c"
gem "net-ldap",      "0.16.2"
gem "oa-enterprise", "0.3.2.g3930d46"
gem "octokit",       "~>4.3"
gem "xmldsig",       "0.4.1"

# Used for licenses on both GHES & GitHub.com (the latter for GitHub Connect)
gem "enterprise-crypto", "0.4.22"

group :development, :test do
  gem "arca",   "2.1.3", :require => false
  gem "factory_bot_rails", "6.1.0"
  gem "guard", "~> 2.1", require: false
  gem "erb_lint", "0.0.34", require: false
  gem "guard-livereload", "~> 2.5", require: false
  gem "launchy"
  gem "pry-byebug", "~> 3.9"
  gem "toxiproxy", "1.0.3"
end

group :development do
  gem "benchmark-ips", "2.8.2", :require => false
  gem "sshkey", "~> 1.3.2"
  gem "rubocop", "=0.82.0", :require => false
  gem "rubocop-github", "0.16.0", :require => false
  gem "rubocop-performance", "~> 1.4", :require => false
  gem "rubocop-rails", "~> 2.3", :require => false
  gem "rubocop-rspec", "~> 1.30", ">= 1.30.1", require: false
  gem "letter_opener",     "1.7.0"
end

group :test do
  gem "minitest", "~> 5.13.0"
  gem "minitest-stub-const", "0.6"
  gem "minitest-focus", "1.2.1"
  gem "html-proofer", "3.12.2"
  gem "mocha",             "1.1.0"
  gem "rack-test",         "0.6.3", :require => "rack/test"
  gem "timecop",           "0.9.1"
  gem "fake_braintree",    "0.6.0.24.g0b0af46"
  gem "ladle",             "~> 1.0"
  gem "vcr",               "~> 5.0"
  gem "test-queue",        "0.4.2"
  # mock net:http requests for vcr (eg, braintree)
  gem "webmock",           "3.8.3.9.g382d84c"
  gem "capybara",          "~> 3.33"
  gem "licensed",          "~> 2.0"
  gem "zeus",              "~> 0.15.14", :require => false
  gem "simplecov",         "~> 0.18.5"
  gem "tcr",               "0.1.0"
  gem "pdf-reader", "2.2.0"
end
