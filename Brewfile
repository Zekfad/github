# frozen_string_literal: true
# ruby dependencies
brew "openssl@1.1"
brew "autoconf"
brew "readline"
brew "jemalloc"
brew "sqlite"

# rugged dependencies
brew "cmake"
brew "pkg-config"

# charlock holmes dependencies
brew "icu4c"

# markup dependencies
brew "rakudo-star"

# local HTTPS development dependencies
brew "mkcert"
brew "nss"

# enterprise-crypto dependencies
# Ensure it doesn't conflict with MacGPG2, which should also work
brew "gnupg" unless File.executable? "/usr/local/bin/gpg"
brew "libsodium"

# hypercredscan dependencies
brew "go"
brew "hyperscan"

# java (elasticsearch dependency)
tap "github/bootstrap"
cask "github/bootstrap/zulu8" unless system "/usr/libexec/java_home --failfast --version 1.8 &>/dev/null"

# staging-lab dependency
tap "github/packages"
brew "aws-iam-authenticator"
brew "awscli"
brew "awssume"
# Docker for Mac installs its own kubectl, but only if Kubernetes
# is enabled in the options menu. Otherwise, we need to provide
# our own.
brew "kubectl@1.14"
brew "kustomize@2.0"
brew "container-diff"

# services
brew "memcached", restart_service: true
brew "redis", restart_service: true
brew "nginx", restart_service: true
brew "elasticsearch@2.4", restart_service: true, conflicts_with: ["elasticsearch"]
brew "mysql@5.7", conflicts_with: ["mysql@5.6"]
brew "launchdns", restart_service: true
brew "launch_socket_server", restart_service: true

# Required by authzd, which is mandatory for tests
cask "docker" unless File.executable? "/usr/local/bin/docker"

# Procfile runner
brew "overmind"

# Used to produce JSON in metric publication
brew "jq"
