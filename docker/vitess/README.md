# Running Vitess in Development

This doc describes how to run Vitess locally and integrate with the GitHub rails app.

This is a walkthrough on starting a Vitess cluster with no data.

## Available clusters
To spin up a specific database cluster on vitess, you'll want to set environment variables.

Examples:
```
export VITESS=1
export VITESS_GITHUB=1
export VITESS_NOTIFICATIONS=1
```
with the variables set above and the command run below, you will startup `mysql1` (github) and `mysql2` (notifications) databases.

Current clusters available:
VITESS_BALLAST, VITESS_GITBACKUPS, VITESS_GITHUB, VITESS_IAM, VITESS_KV, VITESS_NOTIFICATIONS, VITESS_NOTIFY, and VITESS_SPOKES

note: auditlogs & vt will always run in vitess to match production (They use VITESS=1.)

### Testing
This is a very manual process at the moment. The next step in this
project is to automate this better. This is so people can start playing.

**Note**: `script/vitess` expects `mysql` in your `bin` path. If that's not the case you can add it by running

```
echo 'export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"' >> ~/.bash_profile
source ~/.bash_profile
```

```
# Shutdown any running vitess instances
script/vitess rm
# Reset env vars for setup
for E in $(env | grep ^VITESS | cut -d= -f1); do unset $E; done
# Normal bootstrap and setup process (non-vitess)
script/bootstrap
script/setup --force # This doesn't work with VITESS yet.
# Start setting up vitess.
export VITESS=1
export VITESS_GITHUB=1
# ...
script/vitess up
# vitess up will add files in db/vschema. If you have errors in this
# script, try removing the untracked files and running again. They will
# eventually be part of the schema migration process, just not yet.

# Rake jobs from script/setup, excluding dropping databases. Vitess is
# not a fan of dropping and creating databases outside of vitess.
bin/rake repos:setup 
bin/rake db:add_initial_admins 
bin/rake db:ghost 
bin/rake db:staff_user 
bin/rake db:create_org 
bin/rake db:setup_global_business 
bin/rake db:migrate_only 
bin/rake db:team_subscriptions_backfill 
bin/rake db:test:prepare 
bin/rake db:ghost 
bin/rake cache:flush

# Start up rails
bin/server
```

## Vitess Web-UI
You can get information from vtctld and vtgate.

#### http://localhost:15000 
In the `vtctld` web interface. There you should see the keyspaces created. They may have slightly different names, but should be easy to figure out.

#### http://localhost:15099
The `vtgate` web interface. Here you can see things like the [queries sent to the vtgate](http://localhost:15099/debug/querylogz).

#### MySQL protocol
`vtgate` exposes a TCP port, so we can use the local MySQL client to talk to it.
```
> mysql --protocol=TCP -P 15306 -uroot
```
That should connect to `vtgate` using the MySQL protocol.
We will use that connection in the Rails app too.

## Help I'm stuck
Come to #databases. @tomkrouper will be happy to help you.

## Vitess Documentation
[Vitess' Documentation](https://vitess.io/docs/) is super helpful and has good tutorials and examples to show what it can do.

## TODO
- Automating rake tasks
- Vertical sharding example.
- Horizontal sharding example
- Setup vtcombo to use vitess in CI.
