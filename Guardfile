# frozen_string_literal: true
require "guard"
require "shellwords"

module ::Guard
  class Github < Plugin

    def initialize(options = {})
      super
      @runner = options[:runner] || "bin/rails test"
    end

    # Called when just `enter` is pressed
    # This method should be principally used for long action like running all specs/tests/...
    # @raise [:task_has_failed] when run_all has failed
    def run_all
      ::Guard::UI.info("Run all is disabled", :reset => true)
    end

    # Called on file(s) modifications that the Guard watches.
    # @param [Array<String>] paths the changes files or paths
    # @raise [:task_has_failed] when run_on_change has failed
    def run_on_changes(paths)
      escaped_paths = paths.map { |path| Shellwords.shellescape(path) }
      existing_paths = escaped_paths.select { |p| File.file?(p) }.uniq
      if existing_paths.any?
        command = "#{@runner} #{existing_paths.join(' ')}"
        ::Guard::UI.info("Running: #{command} ...")
        ENV["RAILS_ENV"] = "test" # need to set this by hand here, lol I dunno
        system(command)
      else
        ::Guard::UI.info("No test files found - tried the following paths: #{paths}")
      end

    end
  end
end

directories %w(app config lib jobs test)

guard :github, :cli => "--encoding-compatibility", :all_on_start => false do
  watch(%r{^lib/(.+)\.rb$})     { |m| ["test/lib/#{m[1]}_test.rb", "test/fast/lib/#{m[1]}_test.rb"] }
  watch(%r{^lib/platform/(unions|objects|interfaces|fields|inputs|enums|mutations|scalars)/(.+)\.rb$}) { |m| ["test/lib/platform/schema_test.rb"] }
  watch(%r{^lib/github/migrator/(.+)\.rb$}) { |m| ["test/lib/github/migrator_test.rb"] }
  watch(%r{^lib/platform/([^/]+)/([^/]+)\.rb$}) { |m| ["test/lib/platform/integration/#{m[1]}/#{m[2]}_test.rb"] }
  watch(%r{^lib/github/prioritizable.rb$}) { |m| ["test/models/project_column_test.rb", "test/models/issue_priority_test.rb"] }
  watch(%r{^lib/github/prioritizable/context.rb$}) { |m| ["test/models/project_column_test.rb", "test/models/milestone_test.rb", "test/models/milestone/prioritization_dependency_test.rb"] }
  watch(%r{^jobs/(.+)\.rb$})    { |m| "test/jobs/#{m[1]}_test.rb" }
  watch(%r{^test/test_helpers/(.+)\.rb$}) { |m| ["test/test_helpers_tests/#{m[1]}_test.rb", "test/fast/test_helpers_tests/#{m[1]}_test.rb"] }
  watch(%r{^test/rubocop/cop/.+/([^/]+)\.rb$}) { |m| "test/rubocop/tests/#{m[1]}_test.rb" }
  watch(%r{^test/.+_test\.rb$})
  watch("lib/github/prefill_associations.rb") { "test/models/prefill_test.rb" }
  watch(%r[config/kubernetes/.+nginx-conf(ig)?\.yaml]) { "test/lib/github/config_test.rb" }

  # Rails example
  watch(%r{^app/mailers/(.+)\.rb$})              { |m| "test/mailers/#{m[1]}_test.rb" }
  watch(%r{^app/views/mailers/(.+)/.+\.erb$})    { |m| "test/mailers/#{m[1]}_mailer_test.rb" }
  watch(%r{^app/models/(.+)\.rb$})               { |m| "test/models/#{m[1]}_test.rb" }
  watch(%r{^app/models/(.+)/.+_dependency\.rb$}) { |m| "test/models/#{m[1]}_test.rb" }
  watch(%r{^app/controllers/(.+)\.rb$})          { |m| "test/integration/#{m[1]}_test.rb" }
  watch(%r{^app/helpers/(.+)\.rb$})              { |m| "test/helpers/#{m[1]}_test.rb" }
  watch(%r{^app/api/(.+)\.rb$})                  { |m| "test/integration/api/#{m[1]}_test.rb" }
  watch(%r{^app/api/serializer/(.+)\.rb$})       { |m| ["test/models/api/serializer/#{m[1]}_test.rb"] }
  watch(%r{^app/api/internal/(.+)\.rb$})         { |m| "test/integration/api/internal_#{m[1]}_test.rb" }
  watch(%r{^app/views/.+\.rb$})                  { "test/integration" }
  watch(%r{^app/view_models/(.+)\.rb$})          { |m| "test/view_models/#{m[1]}_test.rb" }

  watch(%r{^app/controllers/project_(card|column)_controller_actions.rb$}) { |m| ["test/controllers/repos/project_#{m[1]}s_controller_test.rb", "test/controllers/orgs/project_#{m[1]}s_controller_test.rb"] }
  watch(%r{^app/controllers/project_controller_actions.rb$}) { |m| ["test/controllers/repos/projects_controller_test.rb", "test/controllers/orgs/projects_controller_test.rb"] }
  watch(%r{^(lib/github/darrrr_earthsmoke_encryptor.rb|test/lib/delegated_recovery_helpers|lib/platform/helpers/delegated_account_recovery.rb)$}) { |_| %w(
    test/lib/platform/integration/mutations/add_recovery_token_test.rb
    test/lib/platform/integration/mutations/confirm_recovery_token_test.rb
    test/lib/platform/integration/mutations/verify_recovery_token_test.rb
    test/controllers/recovery_provider_controller_test.rb
    test/controllers/delegated_account_recovery_controller_test.rb
  )
  }
end

guard "livereload" do
  extensions = {
    css: :css,
    scss: :css,
    sass: :css,
    js: :js,
    coffee: :js,
    html: :html,
    png: :png,
    gif: :gif,
    jpg: :jpg,
    jpeg: :jpeg,
  }

  rails_view_exts = %w(erb haml slim)

  # file types LiveReload may optimize refresh for
  compiled_exts = extensions.values.uniq
  watch(%r{public/.+\.(#{compiled_exts * "|"})})

  extensions.each do |ext, type|
    watch(%r{
          (?:app|vendor)
          (?:/assets/\w+/(?<path>[^.]+) # path+base without extension
           (?<ext>\.#{ext})) # matching extension (must be first encountered)
          (?:\.\w+|$) # other extensions
          }x) do |m|
      path = m[1]
      "/assets/#{path}.#{type}"
    end
  end

  # file needing a full reload of the page anyway
  watch(%r{app/views/.+\.(#{rails_view_exts * "|"})$})
  watch(%r{app/helpers/.+\.rb})
  watch(%r{config/locales/.+\.yml})
end
