# frozen_string_literal: true

module Dependabot
  module Twirp
    class UpdateJobStatusDecorator
      attr_reader :update_job_status

      delegate :id,
               :state,
               :finished_at,
               :last_error,
               :update_config_id,
               to: :update_job_status

      def initialize(update_job_status)
        @update_job_status = update_job_status
      end

      def pretty_state
        case state
        when :PENDING then "Update check pending"
        when :CANCELLED then "Update check cancelled"
        when :ENQUEUED then "Update check enqueued"
        when :PROCESSING then "Update check processing"
        when :PROCESSED then "Update check processed"
        when :PROCESSED_WITH_ERRORS then "Update check processed with errors"
        when :TIMED_OUT then "Update check timed out"
        when :PENDING_RETRY then "Update check pending retry"
        else
          "Unknown"
        end
      end
    end
  end
end
