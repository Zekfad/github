# frozen_string_literal: true

module Dependabot
  module Twirp
    class UpdateConfigsClient < Dependabot::Twirp::BaseClient
      def list_update_configs(repository_id:)
        rpc(:ListUpdateConfigs, repository_github_id: repository_id)
      end

      def trigger_update_job(repository_id:, update_config_id:)
        rpc(
          :TriggerUpdateJob,
          repository_github_id: repository_id,
          update_config_id: update_config_id
        )
      end

      private

      def twirp_class
        DependabotApi::V1::UpdateConfigsClient
      end
    end
  end
end
