# frozen_string_literal: true
require_relative "./database_selector/last_operations"

class DatabaseSelector

  # If the last write was more than FORCE_REPLICA_READ_TIMEOUT seconds ago
  # we don't do any calculation and assume we can read from the replica.
  FORCE_REPLICA_READ_TIMEOUT = 5.seconds

  attr_reader :force_replica_read_timeout

  def self.instance
    @database_selector ||= new
  end

  def initialize(force_replica_read_timeout: FORCE_REPLICA_READ_TIMEOUT)
    @force_replica_read_timeout = force_replica_read_timeout
  end

  # Chooses the database to read from based on the return value of read_from_replicas?
  def read_from_database(last_operations:, called_from:, &block)
    gtids = last_operations.last_gtids
    GitHub.dogstats.time("database_selector.db_connection_for_ro_query.time") do
      all_replicas_ready = ApplicationRecord.gtid_tracking_clusters.all? do |cluster|
        next true unless gtids.has_key?(cluster.cluster_name)

        read_from_replicas?(cluster, gtids[cluster.cluster_name], force_replica_read_timeout: force_replica_read_timeout)
      end
      if all_replicas_ready
        read_from_replica(last_operations, called_from, &block)
      else
        read_from_primary(called_from, &block)
      end
    end
  end

  # Increments the dogstat for reading from the replica, updates
  # the last read timestamp and then reads from the replica.
  def read_from_replica(last_operations, called_from, &block)
    GitHub.dogstats.increment("database_selector.#{called_from}", tags: ["action:readonly"])

    ActiveRecord::Base.connected_to(role: :reading, &block)
  end

  # Increments the dogstat for reading from a primary, and then
  # reads from the primary.
  def read_from_primary(called_from, &block)
    GitHub.dogstats.increment("database_selector.#{called_from}", tags: ["action:readonly_primary"])

    ActiveRecord::Base.connected_to(role: :writing, prevent_writes: true, &block)
  end

  # Returns whether is OK to read from the replicas based on
  # * the last time we issued a write operation
  # * the replication delay that existed at the time this was invoked
  #
  # Returns true if the last write request was previous to `force_replica_read_timeout`
  # seconds ago.
  # Returns true if the last write request was not previous to `force_replica_read_timeout`
  # seconds ago but the GTID is seen on the replica connection.
  # Returns false otherwise
  #
  def read_from_replicas?(cluster, gtid, force_replica_read_timeout: self.force_replica_read_timeout)
    if gtid.blank? || gtid[:gtid].blank?
      measure_called(time_diff: force_replica_read_timeout, cluster: cluster, reason: :no_gtid, success: true)
      return true
    end

    time_diff = Time.now - Timestamp.to_time(gtid[:time])

    if time_diff >= force_replica_read_timeout
      measure_called(time_diff: time_diff, cluster: cluster, reason: :write_expired, success: true)
      return true
    end

    if replica_contains_gtid?(cluster, gtid[:gtid])
      measure_called(time_diff: time_diff, cluster: cluster, reason: :gtid_success, success: true)
      return true
    end

    measure_called(time_diff: time_diff, cluster: cluster, reason: :master_read, success: false)
    return false
  end

  def replica_contains_gtid?(cluster, gtid)
    ActiveRecord::Base.connected_to(role: :reading) do
      cluster.github_sql.value(<<-SQL, gtid: gtid) == 1
        SELECT GTID_SUBSET(:gtid, @@GLOBAL.GTID_EXECUTED)
      SQL
    end
  end

  private

  def measure_called(time_diff:, cluster:, reason:, success:)
    if time_diff >= force_replica_read_timeout
      GitHub.dogstats.increment("database_selector.time_since_last_write.timed_out", tags: ["cluster:#{cluster.cluster_name}", "reason:#{reason}", "success:#{success}"])
    else
      bucket = ((time_diff * 1000).to_i / 200) * 200
      GitHub.dogstats.increment("database_selector.time_since_last_write", tags: ["cluster:#{cluster.cluster_name}", "bucket:#{bucket}", "reason:#{reason}", "success:#{success}"])
    end
    GitHub.dogstats.histogram("database_selector.called", time_diff, tags: ["cluster:#{cluster.cluster_name}", "reason:#{reason}", "success:#{success}"])
  end
end
