# frozen_string_literal: true

require "graphql"
require "graphql/batch"
require "graphql_monkey_patches"

GraphQL::DeprecatedDSL.activate

# Turn on proper pagination checks for GraphQL::Relay::ArrayConnection
# remove this when it becomes default behavior
# (Our custom connections already do this)
GraphQL::Relay::ConnectionType.bidirectional_pagination = true

module Platform
  ORIGIN_API = "api".freeze
  ORIGIN_REST_API = "rest_api".freeze
  ORIGIN_INTERNAL = "internal".freeze
  ORIGIN_MANUAL_EXECUTION = "manual_execution".freeze

  QUERY_EVENT_KEY = "platform.query"

  # this must be registered after graphql/relay is required, as graphql/relay
  # registers its own connection implementation for ActiveRecord::Relation
  GraphQL::Relay::BaseConnection.register_connection_implementation(ActiveRecord::Relation, Platform::ConnectionWrappers::Relation)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::Helpers::WatchersQuery, Platform::ConnectionWrappers::Newsies)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::Helpers::WatchingQuery, Platform::ConnectionWrappers::Newsies)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::Helpers::NotificationThreadsQuery, Platform::ConnectionWrappers::Notifications)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::Helpers::NotificationThreadSubscriptionsQuery, Platform::ConnectionWrappers::NotificationThreadSubscriptions)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::AuditLogQuery, Platform::ConnectionWrappers::AuditLogQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Audit::Driftwood::Query, Platform::ConnectionWrappers::DriftwoodQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::StafftoolsQuery, Platform::ConnectionWrappers::AuditLogQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::IssueQuery, Platform::ConnectionWrappers::SearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::RepoQuery, Platform::ConnectionWrappers::SearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::UserQuery, Platform::ConnectionWrappers::SearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::UserLoginQuery, Platform::ConnectionWrappers::SearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::MarketplaceQuery, Platform::ConnectionWrappers::SearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::LabelQuery, Platform::ConnectionWrappers::SearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Search::Queries::NotificationSearchQuery, Platform::ConnectionWrappers::SearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::Helpers::FeatureHistoryQuery, Platform::ConnectionWrappers::ElasticSearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::Helpers::ProjectsQuery, Platform::ConnectionWrappers::ElasticSearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::Helpers::IssuesQuery, Platform::ConnectionWrappers::ElasticSearchQuery)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Promise, Platform::ConnectionWrappers::Promise)
  GraphQL::Relay::BaseConnection.register_connection_implementation(Wrappers::RemoteProxyRelation, Platform::ConnectionWrappers::RemoteRelation)
  GraphQL::Relay::BaseConnection.register_connection_implementation(
    Timeline::BaseTimeline,
    Platform::ConnectionWrappers::Timeline,
  )

  # Remove the connection implementation for Array - it's too easy to
  # accidentally return an Array or something that GraphQL::Relay thinks is an
  # Array (such as ActiveRecord::Associations::CollectionProxy) from a resolve
  # proc. If you need to provide an Array instead of an ActiveRecord::Relation,
  # you'll need to convert it to an Array subclass that we can explicitly create.
  # We have one available below as Platform::ArrayWrapper.
  GraphQL::Relay::BaseConnection::CONNECTION_IMPLEMENTATIONS.delete("Array")

  # Some relationships that we need to expose aren't real ActiveRecord
  # relationships but, instead, potentially use several relationships to build
  # an Array. To be explicit about the fact that we have an Array and not
  # something like a ActiveRecord::Associations::CollectionProxy, as mentioned
  # above, we need a subclass of Array that we can initialize manually in order
  # to be able to use the ArrayConnection provided by GraphQL. Otherwise, it's
  # too easy to forget to call `.scoped` and end up with an Array that we didn't
  # know was an Array.
  class ArrayWrapper < Array; end
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::ArrayWrapper, Platform::ConnectionWrappers::ArrayWrapper)

  class ExperimentRelation
    def initialize(name, control:, candidate:)
      @name = name
      @control = control
      @candidate = candidate
    end

    attr_reader :name, :control, :candidate

    def count(*args)
      Scientist.run "#{name}-count" do |e|
        e.use { control.call.count(*args) }
        e.try { candidate.call.count(*args) }
      end
    end

    def include?(*args)
      Scientist.run "#{name}-include" do |e|
        e.use { control.call.include?(*args) }
        e.try { candidate.call.include?(*args) }
      end
    end

    def eql?(*args)
      Scientist.run "#{name}-eql" do |e|
        e.use { control.call.eql?(*args) }
        e.try { candidate.call.eql?(*args) }
      end
    end

    def ==(*args)
      Scientist.run "#{name}-eql" do |e|
        e.use { control.call.send(:==, *args) }
        e.try { candidate.call.public_send(:==, *args) }
      end
    end

    def to_ary
      Scientist.run "#{name}-to-ary" do |e|
        e.use { control.call.send(:to_ary, *args) }
        e.try { candidate.call.public_send(:to_ary, *args) }
      end
    end
  end

  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::ExperimentRelation, Platform::ConnectionWrappers::ExperimentRelation)

  class StableArrayWrapper < Array
    DEFAULT_SORT_BY_PROC = -> (item) { [item.created_at, item.id] }

    attr_writer :sort_by_proc
    def sort_by_proc
      return @sort_by_proc if defined?(@sort_by_proc)
      @sort_by_proc = DEFAULT_SORT_BY_PROC
    end
  end
  GraphQL::Relay::BaseConnection.register_connection_implementation(Platform::StableArrayWrapper, Platform::ConnectionWrappers::StableArray)

  # Enable Connection.nodes by default
  GraphQL::Relay::ConnectionType.default_nodes_field = true

  class << self

    # Public: Primary entry-point for GraphQL query execution.
    #
    # query - The GraphQL query to be executed.
    #
    # context - An arbitrary hash of values which can
    #           be accessed from a resolver.
    #
    # variables - Values for `$variables` in the query.
    #
    # validate - Determines whether or not `query` will be validated
    #            with `GraphQL::StaticValidation::Validator`.
    #
    # raise_exceptions - Determines whether to raise exceptions while
    #                    executing or not.
    #
    # operation_name - The optional operation name. It's only requried if multiple
    #                  operations are present in the query.
    #
    # target - An argument that filters a given schema target from
    #          every query context. Valid values currently include `:public`
    #          and `:internal`.
    #
    # Examples:
    #
    #   Platform.execute(query, target: :internal, context: { viewer: user })
    #
    def execute(query, target:, context: {}, variables: {}, validate: true, operation_name: nil, raise_exceptions: false, request_env: nil)
      GitHub.tracer.with_span("platform.execute".freeze, tags: { "component".freeze => "platform".freeze }) do |span|
        context[:origin] = normalize_origin(context[:origin])
        context[:internal_error] = nil
        context[:session] ||= {}
        context[:target] = target

        # Remove any flags which the viewer doesn't actually have
        requested_feature_flags = context[:feature_flags] || []

        filtered_flags = Platform::Objects::Base::FeatureFlag.filter_requested_flags(
          requested_feature_flags,
          user: context[:viewer],
          app: context[:integration] || context[:oauth_app],
        )

        context[:feature_flags] = filtered_flags

        # Add the tracer if it was explicitly requested
        if context[:performance_trace]
          tracer = Platform::PerformancePaneTracer.new(
            field_profile_path: context[:performance_trace_field_profile_path],
            field_profile_type: context[:performance_trace_field_profile_type],
          )
          context[:tracers] = [tracer]
        else
          tracer = GraphQL::Tracing::NullTracer
        end

        # Initialize this outside of the `trace do ... end` block
        # so that it can be accessed later, outside the block.
        response = nil

        tracer.trace("platform_execute", {}) do
          begin
            query = nil if query.blank?

            context[:query_string] = if query.is_a?(String)
              query.strip
            end

            document = parse_query(query, tracer: tracer)

            if context[:query_string].nil?
              context[:query_string] = document.to_query_string.strip
            end

            variables = parse_variables(variables)
          rescue Platform::Errors::Parse, GraphQL::ParseError => e
            GitHub.dogstats.increment("platform.query.parse_error")
            response = Platform::ParseErrorResponse.new(e, provided_context: context, provided_variables: variables)
            response.to_hydro!
            return response
          end

          query = GraphQL::Query.new(
            Platform::Schema,
            document: document,
            context: context,
            variables: variables,
            validate: validate,
            operation_name: operation_name,
            except: Platform::SchemaRuntimeMask.new(target),
          )

           context[:query_tracker] = Platform::QueryTracker.new(query)

          if request_env && GitHub.flipper[:instrument_gql_timeouts].enabled?
            GitHub::TimeoutMiddleware.notify(request_env, Platform::Response::TimeoutInstrumenter.new(query))
          end

          scrubbed_query = if context[:origin] == ORIGIN_INTERNAL
            # Internal queries use variables and never
            # have user data hard coded in them, they are safe
            # to log and allow us to not validate the queries to use
            # with `QueryPrinter`
            context[:query_string]
          else
            scrubbed_query_for_context(query)
          end
          context[:scrubbed_query] = scrubbed_query

          query_details = {
            string: scrubbed_query,
            stats: query.context[:query_tracker].clock_times,
            variables: variables,
          }

          Platform::GlobalScope.queries << query_details
          Platform::GlobalScope.mutation = query.mutation?

          # Build the `permission` here so that we can inspect the Query object
          # and determine whether it's a mutation or a query.
          context[:permission] = Platform::Authorization::Permission.new(query.context)

          context[:operation_name] = operation_name
          # In a UI scenario, `actor` will always be set to `viewer`
          # But, in the API, `actor` is set by `current_actor`, which means it *could*
          # be an Integration, or a User. In any event, we enforce the existence of an
          # `actor` here so that it is configured in multiple places (UI, GQL API, and tests)
          context[:actor] ||= context[:viewer]

          # Add some context to Failbot in case an error happens or `Failbot.report`
          failbot_context_keys = {
            graphql: true,
            graphql_schema_target: target,
            graphql_query_hash: context[:query_tracker].query_hash,
            graphql_variables_hash: context[:query_tracker].variables_hash,
          }.merge(extract_relevant_context_keys(context))
          Failbot.push(failbot_context_keys)

          context[:query_tracker].track do
            begin
              Platform::Security::RepositoryAccess.with_viewer(context[:viewer]) do
                Platform::Session.run(query, context)
              end
              instrument_errors!(query)
            rescue => exception
              GitHub.dogstats.increment("platform.query.internal_error")
              GitHub.tracer.record_error(span, exception)
              interpreter_ns = query.context.namespace(:interpreter)
              runtime_failbot_ctx = {
                # `current_field` can be nil if an error comes from static validation (because the query isn't running yet)
                graphql_current_field: interpreter_ns[:current_field]&.path,
                graphql_current_path: interpreter_ns[:current_path],
              }

              if raise_exceptions || !Rails.env.production?
                Failbot.push(runtime_failbot_ctx)
                raise exception
              else
                Failbot.report(exception, runtime_failbot_ctx)
              end

              context[:internal_error] = exception
            ensure
              response = Platform::GraphqlResponse.new(query)
            end
          end
        end

        response.to_hydro!
        response
      end
    end

    def normalize_origin(origin)
      origin = case origin
      when String
        origin
      when nil
        ORIGIN_MANUAL_EXECUTION
      else
        origin.to_s
      end

      Platform::GlobalScope.origin = origin
      origin
    end

    def parse_query(query, tracer: nil)
      tracer ||= GraphQL::Tracing::NullTracer
      case query
      when String
        GraphQL.parse(query, tracer: tracer)
      when GraphQL::Language::Nodes::OperationDefinition
        GraphQL::Language::Nodes::Document.new(definitions: [query])
      when GraphQL::Language::Nodes::Document # From our own ApplicationController
        query
      else
        raise Platform::Errors::Parse.new("A query attribute must be specified and must be a string.")
      end
    end

    def parse_variables(variables)
      variables = GitHub::JSON.parse(variables) if variables.is_a?(String)

      return variables if variables.is_a?(Hash)

      # GitHub::JSON will parse into things other than hashes (e.g. empty strings
      # into `nil` and "1" into a lone Integer. If we end up with anything but a
      # Hash, just fall back into an empty one.
      return Hash.new
    rescue Yajl::ParseError => e
      raise Platform::Errors::Parse.new("Failed to parse variables")
    end

    # Scrub a user supplied GraphQL query given the query string and
    # optionally a Hash of variables for the query.
    #
    # query     - A String representing a GraphQL query.
    # variables - A String or a Hash representing the GraphQL query variables.
    #             If a String, must be JSON that can be parsed into a Hash.
    #
    # Returns a String representing the GraphQL query with any variables inlined
    # and scrubbed. Otherwise nil if there is any issue parsing or scrubbing the
    # query.
    def scrubbed_query_for_context(query)
      Platform::Instrumentation::QueryPrinter.new(query).scrubbed
    rescue Platform::Instrumentation::QueryPrinter::InvalidQueryError
      nil # If it's an invalid query, just return nil
    rescue => e
      # Fail guard to make sure the sanitizer
      # handles all edge cases
      Failbot.report(e)
      nil
    end

    def extract_shorthand_arguments(hash)
      if hash.key?(:context) || hash.key?(:variables)
        raise KeyError, "It looks like you passed a :context or :variables key. This probably won't do what you want."
      end

      context = {}
      variables = {}

      hash.each_pair do |key, value|
        if key[0] == "$"
          variables[key[1..-1]] = value
        else
          context[key] = value
        end
      end

      [context, variables]
    end

    def extract_relevant_context_keys(context)
      {
        origin: context[:origin],
        viewer: context[:viewer]&.login,
        oauth_app: context[:oauth_app]&.id,
        integration: context[:integration]&.id,
        granted_oauth_scopes: context[:granted_oauth_scopes],
      }
    end

    # Public: Is the origin coming from a query GitHub wrote?
    #
    # Returns a Boolean.
    def safe_origin?(origin)
      origin == ORIGIN_MANUAL_EXECUTION || origin == ORIGIN_INTERNAL
    end

    # Public: Is the origin coming from the outside world?
    #
    # Returns a Boolean.
    def unsafe_origin?(origin)
      !safe_origin?(origin)
    end

    # Public: Should stringent AuthZ checks be performed on queries coming from this origin?
    #
    # Returns a Boolean.
    def requires_scope?(origin)
      origin == ORIGIN_API
    end

    def instrument_errors!(query)
      query.validation_errors.each do |e|
        tags = ["type:validation"]

        # Some special cases are from the GraphQL ruby gem (see validation_pipeline.rb
        # in the gem). These get added to the validation_errors array but do not come
        # from a root of StaticValidation::Error hence missing code information.
        code = if e.is_a?(GraphQL::StaticValidation::Error) && e.respond_to?(:code)
          e.code.underscore
        elsif e.is_a? GraphQL::Query::OperationNameMissingError
          "operation_name_missing"
        elsif e.is_a? GraphQL::Query::VariableValidationError
          "variable_validation"
        end

        code = e.class.name.demodulize.downcase.underscore unless code

        tags << "code:#{code}"

        GitHub.dogstats.increment("platform.query.runtime_error", tags: tags)
      end

      query.analysis_errors.each do |e|
        code = if e.respond_to?(:type)
          e.type.downcase
        else
          e.class.name.demodulize.downcase.underscore
        end

        GitHub.dogstats.increment(
          "platform.query.runtime_error",
          tags: ["type:analysis", "code:#{code}"],
        )
      end

      query.context.errors.each do |e|
        code = if e.respond_to?(:type)
          e.type.downcase
        else
          e.class.name.demodulize.downcase.underscore
        end

        GitHub.dogstats.increment("platform.query.runtime_error", tags: ["type:execution", "code:#{code}"])
      end
    end
  end
end
