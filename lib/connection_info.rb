# frozen_string_literal: true

class ConnectionInfo
  attr_reader :host, :url, :config_host

  def initialize(info = {})
    @host = info[:host]
    @url = info[:url]
    @config_host = info[:config_host]
  end
end
