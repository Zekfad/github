# frozen_string_literal: true

module GraphQLExtensions
  module MaxQueryDepth
    def analyze?
      if query.context[:origin] == Platform::ORIGIN_INTERNAL
        Rails.development? || Rails.test?
      else
        true
      end
    end
  end
end
