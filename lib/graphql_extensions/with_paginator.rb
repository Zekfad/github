# frozen_string_literal: true

module GraphQLExtensions
  module WithPaginator
    attr_reader :paginator

    def initialize(nodes, arguments, field: nil, max_page_size: nil, parent: nil, context: nil)
      @paginator = Platform::ConnectionWrappers::Paginator.new(arguments, field, max_page_size)
      super
    end
  end
end
