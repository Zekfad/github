# rubocop:disable Style/FrozenStringLiteralComment

module Collab
  class Response < Resiliency::Response

    def stats_tag
      "collab.unavailable_exception.count"
    end

    def failure_app_tag
      "github-collab"
    end

  end
end
