# frozen_string_literal: true
#
module Collab
  module Responses
    STATS_TAG = "collab.unavailable_exception.count"
    FAILURE_APP_TAG = "github-collab"

    class Boolean < Resiliency::Responses::Boolean
      def stats_tag
        ::Collab::Responses::STATS_TAG
      end

      def failure_app_tag
        ::Collab::Responses::FAILURE_APP_TAG
      end
    end

    class Array < Resiliency::Responses::Array
      def stats_tag
        ::Collab::Responses::STATS_TAG
      end

      def failure_app_tag
        ::Collab::Responses::FAILURE_APP_TAG
      end
    end
  end
end
