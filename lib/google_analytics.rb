# frozen_string_literal: true

class GoogleAnalytics
  class Dimensions
    DEVICE_TYPE          = "dimension3"
    EXPLORE              = "dimension7"
    EXPLORE_VERSION      = "dimension8"
    HAS_ACCOUNT          = "dimension5"
    LOGGED_IN            = "dimension1"
    MARKETPLACE_LISTINGS = "dimension6"
    RESPONSIVE           = "dimension10"
  end
end
