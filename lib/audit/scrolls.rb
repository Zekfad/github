# frozen_string_literal: true

module Audit
  module Scrolls
    SYSLOG_EVENT_ITEMS = [
      :actor_id,
      :actor,
      :oauth_app_id,
      :action,
      :user_id,
      :user,
      :repo_id,
      :repo,
      :actor_ip,
      :created_at,
      :from,
      :note,
      :org,
      :org_id,
      :data,
    ].freeze

    def self.service
      ::Audit::Service.new logger: ::Audit::Scrolls::Logger.new
    end

    class Logger
      # Public: Splits non-indexed Audit payload data into a separate 'data'
      # key.
      #
      #   split_payload(:actor_id => 1, :foo => 'bar')
      #   # {:actor_id => 1, :data => {:foo => 'bar'}}
      #
      # Returns a Hash.
      def split_payload(payload)
        data = {}
        payload.each_key do |key|
          next if SYSLOG_EVENT_ITEMS.include?(key.to_sym)
          data[key] = payload.delete(key)
        end
        payload[:data] = data
        payload
      end

      def initialize
      end

      def format_event(payload)
        split_payload(payload).inspect
      end

      # Internal: Logs audit entry to Syslog using GitHub::Logger
      #
      # payload - The Hash of data that is sent over as a string.
      #
      # Returns the Syslog instance.
      def perform(payload)
        GitHub::Logger.log(payload)
      end
    end

  end
end
