# rubocop:disable Style/FrozenStringLiteralComment
#

module Audit
  module Driftwood
    # Export Git events using the Driftwood backend,
    # returns the result as a zipped JSON file
    #
    # org|business id - target's ID to export Git audit log entries from
    # start_date - A datetime variable to indicate the export's start date.
    # end_date - A datetime variable to indicate the export's end date.
    # region - The region the user/org/business belongs to

    DEFAULT_REGION = "US"

    class GitEventExport

      def initialize
          @client = GitHub.driftwood_client
      end

      # Tell Driftwood to start a git event export for a given organization
      def start_org_export(org_id:, token:, start_time:, end_time:, region: DEFAULT_REGION)
        @client.export_org_git(
          organization_id: org_id,
          token: token,
          start_time: start_time,
          end_time: end_time,
          region: DEFAULT_REGION,
        ).execute
      end

      # Tell Driftwood to start a git event export for a given business
      def start_business_export(business_id:, token:, start_time:, end_time:, region: DEFAULT_REGION)
        @client.export_business_git(
          business_id: business_id,
          token: token,
          start_time: start_time,
          end_time: end_time,
          region: DEFAULT_REGION,
        ).execute
      end

      # Query Driftwood to check whether or not a export job is complete, i.e: don't fetch results
      def check_results(id:, token:, region: DEFAULT_REGION)
        resp = @client.export_fetch_results(
          id: id,
          token: token,
          region: DEFAULT_REGION,
          only_check_state: true,
        ).execute

        {finished: resp.finished, successful: resp.successful, json_zip: ""}
      end

      # Query Driftwood to fetch the results of an export job
      def fetch_results(id:, token:, region: DEFAULT_REGION)
        resp = @client.export_fetch_results(
          id: id,
          token: token,
          region: DEFAULT_REGION,
          only_check_state: false,
        ).execute

        {finished: resp.finished, successful: resp.successful, json_zip: resp.json_zip}
      end
    end
  end
end
