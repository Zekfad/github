# rubocop:disable Style/FrozenStringLiteralComment

module Audit
  module Driftwood
    # Runs a query against the Driftwood backend,
    # collects, sorts, format the resulting entry set as JSON/CSV.
    #
    # format - CSV or JSON
    # phrase - A stafftools-style search string (optional).
    # subject - A User, Organization, or Business to scope query (optional).
    #
    # Usage:
    #
    # export = Audit::Drifwood::BulkExport.new({
    #   :phrase      => "org:github (action:repo.create OR action:repo.destroy)"
    # })
    # export.run
    #
    class BulkExport < Audit::BulkExport::Base

      DEFAULT_REGION = "US"

      def initialize(format: "json", phrase: nil, subject: nil)
        super(format: format, phrase: phrase, subject: subject, backend: "driftwood")
      end

      def client
        @client ||= GitHub.driftwood_client
      end

      # Queries the Driftwood backend  and returns the number of entries and the actual entries.
      #
      # Returns an Array matching [Integer, Array].
      def fetch_results
        case subject
        when Organization
          results = org_query(subject.id)
        when Business
          results = business_query(subject.id)
        else
          raise ArgumentError, "Invalid subject, expected Organization, or Business, got #{subject}"
        end

        GitHub.dogstats.histogram("audit.export.results.indices.total", results.length)
        [results.length, results]
      end

      private

      def org_query(org_id)
        query = client.org_query(
          org_id: org_id,
          per_page: GitHub.driftwood_bulk_export_page_size,
          region: DEFAULT_REGION,
          phrase: phrase)

        query_all(query)
      end

      def business_query(business_id)
        query = client.business_query(
          business_id: business_id,
          per_page: GitHub.driftwood_bulk_export_page_size,
          region: DEFAULT_REGION,
          phrase: phrase)

        query_all(query)
      end

      def query_all(query)
        response = query.execute
        return [] if response.empty?

        data = [*response]
        while query.has_next_page?
          response = query.next_page
          break if response.empty?
          data.push(*response)
        end

        data
      end
    end
  end
end
