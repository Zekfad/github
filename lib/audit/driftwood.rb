# frozen_string_literal: true

module Audit
  module Driftwood
     def self.parse_query(query)
       if should_parse_query?(query)
         GitHub.driftwood_client.parse_query(phrase: query).execute
       end
     rescue ::Driftwood::Client::Error
       # do nothing with this, safe to ignore when just parsing the queries
     end

     def self.parse_query_async(query)
       if should_parse_query?(query)
         AuditLogParseQueryJob.perform_later(query)
       end
     end

     def self.should_parse_query?(query)
       GitHub.driftwood_enabled? && !query.blank? && GitHub.flipper[:audit_log_driftwood_parse].enabled?
     end
  end
end
