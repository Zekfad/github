# frozen_string_literal: true

require "#{Rails.root}/config/environment" unless defined?(Audit)
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/audit/transitions/20190826153929_add_operation_type.rb -v | tee -a /tmp/add_operation_type.log
#
module Audit
  module Transitions
    class AddOperationType < Audit::Transition::Base
      def perform
        log "Starting transition #{self.class.to_s.underscore}"

        count = with_each_document do |entry|
          log(entry) if verbose?

          unless dry_run?
            repair(entry)
          end
        end

        log "Finished #{self.class.to_s.underscore}, #{dry_run? ? "found" : "processed"} #{count} documents."

        count
      end

      def query
        {
          query: {
            match_all: {},
          },
        }
      end

      def repair(entry)
        action = entry["action"]

        if type = Audit::ACTIONS_CRUD[action]
          entry["operation_type"] = type
        end

        entry
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  opts = Slop.parse(help: true, strict: true) do
    on "i", "indices=", "Comma seperated list of indices to perform the transition on", as: Array, delimiter: ","
    on "c", "cluster=", "Which cluster to perform the transition (default: GitHub.es_audit_log_cluster)"
    on "v", "verbose", "Log verbose output (default: false)", default: false
    on "r", "run", "Run the transition (default: false)", default: false
    on "s", :ignore_failures, "Run the transition across all documents, not just failed documents if any exist (default: false)", default: false
  end

  options = opts.to_hash
  options = options.merge(dry_run: !options.delete(:run))

  transition = Audit::Transitions::AddOperationType.new(options)
  transition.perform
end
