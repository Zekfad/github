# rubocop:disable Style/FrozenStringLiteralComment

require "#{Rails.root}/config/environment" unless defined?(Audit)
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/audit/transitions/20160927201515_protected_branches_failures_hash_to_json.rb -v | tee -a /tmp/protected_branches_failures_hash_to_json.log
#
module Audit
  module Transitions
    class ProtectedBranchesFailuresHashToJson < Audit::Transition::Base
      def perform
        log "Starting transition #{self.class.to_s.underscore}"

        count = with_each_document do |entry|
          log(entry) if verbose?

          unless dry_run?
            repair(entry)
          end
        end

        log "Finished #{self.class.to_s.underscore}, #{dry_run? ? "found" : "processed"} #{count} documents."

        count
      end

      def query
        {
          query: {
            constant_score: {
              filter: {
                bool: {
                  must: [
                    {
                      term: {
                        action: "protected_branch",
                      },
                    },
                    {
                      exists: {
                        field: "data.failures",
                      },
                    },
                  ],
                },
              },
            },
          },
        }
      end

      def repair(entry)
        data = entry["data"]
        return false if data.blank?
        return false if data["failures"].blank?

        # We're expecting failures to be an Array
        raise DocumentNotRepairable if data["failures"].is_a?(Hash)

        # We don't want to operate on a document that has already been repaired
        raise DocumentNotRepairable unless data["failures_json"].blank?

        data["failures_json"] = GitHub::JSON.encode(data.delete("failures"))
        entry
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  opts = Slop.parse(help: true, strict: true) do
    on "i", "indices=", "Comma seperated list of indices to perform the transition on", as: Array, delimiter: ","
    on "c", "cluster=", "Which cluster to perform the transition (default: GitHub.es_audit_log_cluster)"
    on "v", "verbose", "Log verbose output (default: false)", default: false
    on "r", "run", "Run the transition (default: false)", default: false
    on "s", :ignore_failures, "The transition will only repair failed documents in an index if any are present (default: false)", default: false
  end

  options = opts.to_hash
  options = options.merge(dry_run: !options.delete(:run))

  transition = Audit::Transitions::ProtectedBranchesFailuresHashToJson.new(options)
  transition.perform
end
