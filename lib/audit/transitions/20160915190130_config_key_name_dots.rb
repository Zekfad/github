# rubocop:disable Style/FrozenStringLiteralComment

require "#{Rails.root}/config/environment" unless defined? Audit
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/audit/transitions/20160915190130_config_key_name_dots.rb -v | tee -a /tmp/config_key_name_dots.log
#
module Audit
  module Transitions
    class ConfigKeyNameDots < Audit::Transition::Base
      def perform
        log "Starting transition #{self.class.to_s.underscore}"
        count = with_each_document do |entry|
          log(entry) if verbose?
          repair(entry) unless dry_run?
        end
        log "Finished #{self.class.to_s.underscore}, #{dry_run? ? "found" : "processed"} #{count} documents."
        count
      end

      def query
        {
          query: {constant_score: {
            filter: {bool: {
              should: [
                {exists: {field: "config.key" }},
                {exists: {field: "config.value"}},
              ],
              must: {term: {action: "repo.config"}},
            }},
          }},
        }
      end

      def repair(entry)
        data = entry["data"]
        return false unless data && (data.key?("config.key") || data.key?("config.value"))

        data["config_key"]   = data.delete("config.key")   if data.key? "config.key"
        data["config_value"] = data.delete("config.value") if data.key? "config.value"

        entry
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  opts = Slop.parse(help: true, strict: true) do
    on "i", "indices=", "Comma seperated list of indices to perform the transition on", as: Array, delimiter: ","
    on "c", "cluster=", "Which cluster to perform the transition (default: GitHub.es_audit_log_cluster)", default: GitHub.es_audit_log_cluster
    on "v", "verbose", "Log verbose output (default: false)", default: false
    on "r", "run", "Run the transition (default: false)", default: false
    on "s", :ignore_failures, "The transition will only repair failed documents in an index if any are present (default: false)", default: false
  end

  options = opts.to_hash
  options = options.merge(dry_run: !options.delete(:run))

  transition = Audit::Transitions::ConfigKeyNameDots.new(options)
  transition.perform
end
