# rubocop:disable Style/FrozenStringLiteralComment

module Audit
  module Transition
    class Runner
      # Public: The String Elasticsearch cluster that will be used to find
      # all Audit Log indices to operate on.
      attr_reader :cluster

      # Public: The String directory where Audit Log Transition classes are on disk.
      attr_reader :directory

      # Public: GitHub Audit Log Transition Runner class responsible for
      # loading all transitions that have been marked to be performed. A
      # transition can be marked to run if any of these are true:
      #
      #   - There are no corresponding Audit::Transition::Documents stored in
      #     any of the Audit Log indices for a given transition.
      #   - There are Audit Log Entries that the repair failed to process.
      #
      # This class should not be used directly.
      # Use bin/perform-audit-log-transitions to run your transitions.
      #
      # cluster - The cluster to query for documents (optional)
      # verbose - log verbose output (optional, default: false)
      # dry_run - Don't save/change any data, just log changes
      #           (optional, default: true)
      # ignore_failures - The Boolean determining if the transition will
      #                   only repair failed documents in an index if any are
      #                   present (default: false).
      #
      # Example
      #
      #   Audit::Transition::Runner.new(cluster: "githubsearch3", verbose: true)
      #
      # Returns Audit::Transition::Runner instance.
      def initialize(cluster: GitHub.es_audit_log_cluster, verbose: false, dry_run: true, ignore_failures: false, **other_args)
        # output will be immediately flushed in any process intensive operation
        # while a transition is performing
        STDOUT.sync = true
        Failbot.push app: "github-audit-log-transitions"

        @cluster = cluster
        @verbose = verbose
        @dry_run = dry_run
        @ignore_failures = ignore_failures
        @directory = other_args.fetch(:directory, "lib/audit/transitions")
        @client = Elastomer::Client.new(
          url: Elastomer.config.clusters.fetch(@cluster).fetch(:url),
          read_timeout: 600, # 10m * 60s
          open_timeout: Elastomer.config.open_timeout,
          adapter: :persistent_excon,
          opaque_id: true,
          max_request_size: 25.megabytes,
          strict_params: true,
        )
      end

      # Public: An Array of all transition classes that exist.
      #
      # Returns Array of Audit::Transition::Base classes.
      def transitions
        @transitions ||= begin
          path = Rails.root.join(@directory, "*.rb")
          files = Dir[path].sort
          loaded = []

          files.each do |filename|
            require(filename)
            class_name = File.basename(filename, ".rb").split("_", 2).last.camelize
            begin
              klass = Audit::Transitions.const_get(class_name)
              loaded << klass
            rescue NameError
              log "Audit::Transitions::#{class_name} not found."
            end
          end

          loaded
        end
      end

      # Returns Array of Audit::Transition::Base instances.
      def pending_transitions
        pending = transitions.collect do |klass|
          transition = klass.new(
            cluster: @cluster,
            verbose: verbose?,
            dry_run: dry_run?,
            ignore_failures: ignore_failures?,
            client: @client,
          )

          if transition.pending? || transition.failed?
            transition
          end
        end

        pending.compact
      end

      # Returns nothing.
      def perform
        count = 0
        open_indices do
          pending_transitions.each do |transition|
            log "Found #{transition.class}, running..."
            count += transition.perform
          end
        end
        count
      end

      private

      # Private: Responsible for unblocking writes on indices which originally
      # had writes blocked, then reblocking them after done operating on them.
      #
      # Returns nothing.
      def open_indices
        # We don't want to perform operations on indices if we're not going to
        # be writing to them during a dry run.
        if dry_run?
          yield
          return
        end

        begin
          indices = indices_with_writes_blocked
          unblock_writes(indices)
          yield
        ensure
          block_writes(indices)
          optimize_indices(indices)
        end
      end

      # Private: Find all Audit Log indices that currently have writes blocked.
      #
      # Returns Array of Elastomer::Index instances.
      def indices_with_writes_blocked
        indices = Audit::Elastic.indices(@client.cluster)
        blocked_indices = indices.collect do |index_name|
          index = @client.index(index_name)
          settings = index.settings[index_name]["settings"]["index"]
          blocks = settings["blocks"] && settings["blocks"]["write"]
          if blocks == "true"
            index
          end
        end
        blocked_indices.compact
      end

      # Private: Optimizes each index that previously had writes unblocked then
      # blocked again after performing transitions on them.
      #
      # When a document is repaired it will delete the original document then
      # adds a new document with the same properties. Optimizing the index will
      # remove all the deleted documents from the segment files on disk.
      #
      # indices - The Array of Elastomer::Index instances to optimize.
      #
      # Returns nothing.
      def optimize_indices(indices)
        indices.each do |index|
          begin
            index.optimize(max_num_segments: 1)
          rescue Elastomer::Client::TimeoutError => e
            puts "[#{index.name}] Timed out trying to optimize index" if verbose?
            Failbot.report(e)
          end
        end
      end

      # Private: Before we can perform any transitions on an index
      # we need to ensure we can write to the index by unblocking writes.
      #
      # Every month when the primary index is promoted the old index has its
      # writes blocked.
      #
      # indices - The Array of Elastomer::Index instances to unblock writes for.
      #
      # Returns nothing.
      def unblock_writes(indices)
        indices.each do |index|
          begin
            index.update_settings({
              index: {
                blocks: {
                  write: false,
                },
              },
            })
          rescue Elastomer::Client::TimeoutError => e
            puts "[#{index.name}] Timed out unblocking writes" if verbose?
            Failbot.report(e)
          end
        end
      end

      # Private: Blocks writes to all indices previously operated on. This will
      # put the index back to its original state.
      #
      # indices - The Array of Elastomer::Index instances to block writes for.
      #
      # Returns nothing.
      def block_writes(indices)
        indices.each do |index|
          begin
            index.update_settings({
              index: {
                blocks: {
                  write: true,
                },
              },
            })
          rescue Elastomer::Client::TimeoutError => e
            puts "[#{index.name}] Timed out blocking writes" if verbose?
            Failbot.report(e)
          end
        end
      end

      def ignore_failures?
        !!@ignore_failures
      end

      def verbose?
        !!@verbose
      end

      def dry_run?
        !!@dry_run
      end

      # Internal: Write a log message - send to STDOUT if not in test
      def log(message)
        Rails.logger.debug message
        return if Rails.env.test? || !verbose?
        puts "[#{Time.now.strftime("%Y-%m-%dT%H:%M:%S")}] #{message}"
      end
    end
  end
end
