# rubocop:disable Style/FrozenStringLiteralComment

module Audit
  module Transition
    class Base
      include GitHub::AreasOfResponsibility
      class DocumentNotRepairable < StandardError; end

      BATCH_COUNT = 500

      # Public: Base class for GitHub Audit Log data transitions
      #
      # Use script/generate-audit-log-transition to create a scaffolded version of
      # this along with a test in the proper location.
      #
      # client  - The Elastomer::Client used to issue requests on Elasticsearch.
      # indices - The Array of indices to query over (optional)
      # verbose - log verbose output (optional, default: false)
      # dry_run - Don't save/change any data, just log changes
      #           (optional, default: true)
      # ignore_failures - The Boolean determining if the transition will
      #                   only repair failed documents in an index if any are
      #                   present (default: false).
      #
      # Example
      #
      #   Audit::Transition::Base.new(cluster: "githubsearch3", indices: ["audit_log-2016-01"], verbose: true)
      #
      # Returns Audit::Transition::Base instance.
      def initialize(client: nil, cluster: nil, indices: nil, verbose: false, dry_run: true, ignore_failures: false, **other_args)
        # output will be immediately flushed in any process intensive operation
        # while a transition is performing
        STDOUT.sync = true
        Failbot.push app: "github-audit-log-transitions"

        @cluster = cluster || GitHub.es_audit_log_cluster
        @client  = client
        @indices = indices
        @verbose = verbose
        @dry_run = dry_run
        @ignore_failures = ignore_failures
        @pp = Audit::PrettyProgress.new(timestamp_prefix: true)
      end

      def perform!
        original_dry_run = @dry_run
        @dry_run = false
        perform
      ensure
        @dry_run = original_dry_run
      end

      # Public: Checks to see if this transition should be performed. If there
      # are any transition documents in the cluster for this transition it has
      # already been performed and should not be performed again automatically.
      #
      # Returns true if hasn't been performed, false if it has been performed.
      def performed?
        indices.any? do |index_name|
          state = transition_state_document(index_name)
          state.present? && state.persisted?
        end
      end

      # Public: Checks to see if this transition should be performed.
      #
      # Returns true if it should be performed, false if not.
      def pending?
        !performed?
      end

      # Public: Checks to see if this transition has been completed successfuly
      # across all audit log indices.
      #
      # Returns true if hasn't been performed, false if it has been performed.
      def success?
        indices.all? do |index_name|
          state = transition_state_document(index_name)
          state.present? && state.success?
        end
      end

      # Public: Checks to see if this transition has been performed and any
      # failed entries.
      #
      # Returns true if has failed entries, false otherwise.
      def failed?
        indices.any? do |index_name|
          state = transition_state_document(index_name)
          state.present? && state.failed?
        end
      end

      # Public: Checks to see if this transition is in progress.
      #
      # Returns true if in progress entries, false if not.
      def in_progress?
        indices.any? do |index_name|
          state = transition_state_document(index_name)
          state.present? && state.in_progress?
        end
      end

      # The method responsible for efficiently scanning all audit log indices
      # for matching documents to repair. Each document that matches will go
      # through the repair process. After a document is repaired it replaces
      # the existing document.
      #
      # Returns Integer count of documents successfuly repaired.
      def with_each_document(&block)
        # The counter for the whole transition
        total_count = 0

        indices.each do |index_name|
          index = client.index(index_name)
          unless index.exists?
            log "[#{index_name}] Index doesn't exist, skipping..." if verbose?
            next
          end

          state = transition_state_document(index)
          state.create unless dry_run?

          if !ignore_failures? && state.failed_documents?
            total_count += repair_failed_documents(
              index: index,
              state: state,
              block: block,
            )
          else
            total_count += repair_documents(
              index: index,
              state: state,
              block: block,
            )
          end
        end

        total_count
      end

      # The list of audit log indices to search for documents
      def indices
        @indices ||= Audit::Elastic.indices(client.cluster)
      end

      def scan_options
        {
          size: 2500,
          scroll: "5m",
          type: Elastomer::Adapters::AuditEntry.document_type,
        }
      end

      def bulk_options
        {
          request_size: 5.megabytes,
          timeout: "5m",
        }
      end

      def client
        @client ||= Elastomer.client(@cluster)
      end

      def transition_state_document(index)
        document = Audit::Transition::Document.new(
          client: client,
          transition: self.class.name,
          index: index,
        )
        document.find
        document
      end

      private

      # Private: Repairs all documents matching the query from the given index.
      #
      # index    - The Elastomer::Index to perform the scan query on
      # state    - The Audit::Transition::Document to keep track of transition state.
      # count    - The Integer total count of documents repaired.
      #
      # Returns Integer count of documents that were successfully repaired.
      def repair_documents(index:, state:, block:)
        response = index.docs.count(query, type: Elastomer::Adapters::AuditEntry.document_type)
        found_count = response["count"]

        if dry_run?
          log "[#{index.name}] Found #{found_count} #{"document".pluralize(found_count)} to be repaired"
          return found_count
        elsif found_count.zero?
          log "[#{index.name}] No documents matched the query, skipping..."
          state.success(doc_count: 0)
          return found_count
        else
          log "[#{index.name}] Repairing #{found_count} #{"document".pluralize(found_count)}"
        end

        processed_count = 0
        response = index.bulk(bulk_options) do |bulk|
          processed_count += scan_and_repair_documents(
            index: index,
            query: query,
            bulk: bulk,
            found_count: found_count,
            block: block,
          )
        end

        process_bulk_response(
          index: index,
          response: response,
          state: state,
          count: processed_count,
          found_count: found_count,
        )
      end

      # Private: Repairs only the failed documents for a given index.
      #
      # index    - The Elastomer::Index to perform the scan query on
      # state    - The Audit::Transition::Document to keep track of transition state.
      # count    - The Integer total count of documents repaired.
      #
      # Returns Integer count of documents that were successfully repaired.
      def repair_failed_documents(index:, state:, block:)
        doc_ids = state.failed_documents.documents
        found_count = doc_ids.count

        if dry_run?
          log "[#{index.name}] Found #{found_count} #{"document".pluralize(found_count)} that failed to be repaired, not repairing in dry run"
          return found_count
        else
          log "[#{index.name}] Found #{found_count} #{"document".pluralize(found_count)} that failed to be repaired, repairing only those documents in batches of #{BATCH_COUNT}"
        end

        processed_count = 0
        response = index.bulk(bulk_options) do |bulk|
          doc_ids.in_groups_of(BATCH_COUNT) do |group|
            failed_query = {
              query: {
                ids: {
                  values: group.compact,
                },
              },
            }

            processed_count += scan_and_repair_documents(
              index: index,
              query: failed_query,
              bulk: bulk,
              found_count: found_count,
              block: block,
            )
          end
        end

        process_bulk_response(
          index: index,
          response: response,
          state: state,
          count: processed_count,
          found_count: found_count,
        )
      end

      # Private: Process the response from the bulk index and mark the
      # transition as successful, or log the failures so they can be repaired
      # again.
      #
      # index    - The Elastomer::Index to perform the scan query on
      # response - The Hash response from the bulk indexing request.
      # state    - The Audit::Transition::Document to keep track of transition state.
      # count    - The Integer total count of documents repaired.
      # found_count - The Integer total count of documents matching the query.
      #
      # Returns Integer count of documents that were successfully repaired.
      def process_bulk_response(index:, response:, state:, count:, found_count:)
        # Check to see if any documents failed to be repaired.
        if response && response["errors"].present?
          failed_doc_ids = response["items"].collect do |hit|
            if hit["index"]["status"] > 299
              hit["index"]["_id"]
            end
          end.compact

          # We need to account for the number of items that haven't been repaired.
          count = found_count - failed_doc_ids.length

          state.failed(
            doc_count: count,
            documents: failed_doc_ids,
          )

          log "[#{index.name}] Repaired #{count} out of #{found_count} #{"document".pluralize(found_count)}, #{failed_doc_ids.length} failed to be repaired"

          log(response.inspect) if verbose?
        else
          if state.failed?
            log "[#{index.name}] Successfully repaired #{count} #{"document".pluralize(count)} that previously failed to be repaired"
          else
            log "[#{index.name}] Successfully repaired #{count} #{"document".pluralize(count)}"
          end

          state.success(doc_count: count)
        end

        count
      end

      # Private: With a given query, scan the index and repair each document.
      #
      # index - The Elastomer::Index to perform the scan query on
      # query - The Hash query matching the documents to repair
      # bulk  - The Elastomer::Bulk session to submit the repaired documents
      # found_count - The Integer total count of documents matching the query
      # block - The Proc block used to repair the entry
      #
      # Returns Integer count of documents that were repaired.
      def scan_and_repair_documents(index:, query:, bulk:, found_count:, block:)
        count = 0
        step = determine_step(found_count)

        scan = index.scan(query, scan_options)
        scan.each_document do |doc|
          entry = begin
            block.call(doc["_source"].dup)
          rescue DocumentNotRepairable
            log "#{client.url}/#{doc["_index"]}/#{doc["_type"]}/#{doc["_id"]} is not repairable" if verbose?
            next
          end

          if entry != false
            unless entry.nil?
              entry["_id"] = doc["_id"]
              entry["_type"] = doc["_type"]
              bulk.index(entry)
            end

            count += 1
            if (step != false && count % step == 0) || count == found_count
              @pp.done "[#{index.name}] #{count} #{"document".pluralize(count)} repaired"
            else
              @pp.step "[#{index.name}] #{step} documents are being repaired"
            end
          end
        end

        count
      end

      def ignore_failures?
        !!@ignore_failures
      end

      def verbose?
        !!@verbose
      end

      def dry_run?
        !!@dry_run
      end

      # Internal: Write a log message - send to STDOUT if not in test
      def log(message)
        Rails.logger.debug message
        return if Rails.env.test?
        puts "[#{Time.now.strftime("%Y-%m-%dT%H:%M:%S")}] #{message}"
      end

      # Private: Determine the interval of repaired documents before for logging
      # progress.
      #
      # count - The total number of documents in the index.
      #
      # Returns Integer.
      def determine_step(count)
        case count
        when 0..10_000
          false
        when 10_001..1_000_000
          10_000
        else
          100_000
        end
      end
    end
  end
end
