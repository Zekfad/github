# rubocop:disable Style/FrozenStringLiteralComment

require "audit/bulk_export/csv_formatter"

module Audit
  class BulkExport
    # Abstract class to run a query against an audit log backend
    # that collects, sorts and formats the resulting entry set as JSON/CSV.
    #
    # format -  json or csv (default: json)
    # phrase - A stafftools-style search string (optional).
    # subject - A User, Organization, or Business to scope query (optional).
    # tag - A string  to identify the audit log backend in emitted metrics
    #
    class Base
      FIELDS = %w[
        action
        actor
        user
        org
        repo
        created_at
        data.hook_id
        data.events
        data.events_were
        data.target_login
        data.old_user
        data.team
      ].freeze

      attr_reader :phrase
      attr_reader :subject

      def initialize(format: "json", phrase: nil, subject: nil, backend: nil)
        @format = format
        @phrase = phrase
        @subject = subject
        backend = backend
      end

      # This abstract method needs to be implemented by the specific backend class.
      #
      # Returns an Array matching [Integer, Array] where the first element
      # is the number of entries, and the second element contains the actual entries.
      def fetch_results
        raise NotImplementedError
      end

      # This method fetches the query results using the given phrase and the
      # concrete "fetch_results" implementation.
      #
      # Returns an Array matching [Integer, Array] where the first element
      # is the number of entries, and the second element contains the
      # formatted entries.
      def run
        count, entries = GitHub.dogstats.time("audit.export", tags: %W(action:fetch exporter:#{@backend})) do
          fetch_results
        end

        GitHub.dogstats.time("audit.export", tags: ["action:sort"]) do
          entries.sort_by! do |hit|
            hit["@timestamp"]
          end
        end

        GitHub.dogstats.time("audit.export", tags: %W(action:format type#{@format})) do
          if phrase.present?
            GitHub.dogstats.histogram("audit.export.results", count, tags: ["type:query"])
          else
            GitHub.dogstats.histogram("audit.export.results", count, tags: ["type:default"])
          end
          [count, format_data(entries)]
        end
      end

      private

      # Private: Take the exported data and format to the user's desired output.
      #
      # Returns formatted String.
      def format_data(data)
        case @format
        when "json"
          GitHub::JSON.encode(data)
        when "csv"
          CsvFormatter.process(data, FIELDS)
        else
          raise ArgumentError, "invalid format '#{@format}' expected: json, csv"
        end
      end
    end
  end
end
