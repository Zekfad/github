# frozen_string_literal: true

module Audit
  module Hydro
    class EventForwarder
      RETRY_DELAY = 2.seconds
      MAX_ATTEMPTS = 25 # We want to retry a lot here because audit-log data is quite important

      class SubscribeError < StandardError; end

      def self.configure(source:, &block)
        instance = new(source)
        instance.instance_exec(&block)
      end

      def initialize(instrumenter = nil)
        @instrumenter = instrumenter
        @role_tag = "role:#{GitHub.role}"
      end

      def publish(payload, **options)
        return unless GitHub.hydro_enabled?
        return unless GitHub.flipper[:audit_log_hydro].enabled?

        attempts = 0

        begin
          result = GitHub.sync_hydro_publisher.publish(payload, **options)
          raise(result.error) unless result.success?
        rescue Hydro::Sink::Error, Kafka::DeliveryFailed => e
          attempts += 1
          if retry_publish_on?(e, attempts)
            report_retry(e, options.slice(:schema), payload.dig(:action))
            sleep RETRY_DELAY
            retry
          end
        end

        if !result.success?
          report_error(result.error, options.slice(:schema), payload.dig(:action))
        end
        result
      end

      def build_publish_message(action, payload)
        payload = GitHub.audit.build_payload(action, payload)
        event_key = payload.fetch(:_document_id, SecureRandom.urlsafe_base64(16))
        audit_timestamp = Audit.milliseconds_to_time(payload.fetch(:@timestamp))

        {
          action: action,
          document: payload.to_json,
          document_id: event_key,
          event_time: audit_timestamp,
          audit_log: :GITHUB,
        }
      end

      private

      def retry_publish_on?(error, tries)
        (
          error.kind_of?(Hydro::Sink::BufferOverflow) || error.kind_of?(Kafka::DeliveryFailed)
        ) && tries < MAX_ATTEMPTS
      end

      def report_retry(error, schema:, action:)
        GitHub.dogstats.increment(
          "hydro.audit_event_forwarder.publish_retry",
          tags: [
            "schema:#{schema}",
            "error:#{error.class.name.underscore}",
            "action:#{action}"
          ]
        )
      end

      def report_error(error, schema:, action:)
        GitHub.dogstats.increment(
          "hydro_client.publish_error",
          tags: [
            "schema:#{schema}",
            "error:#{error.class.name.underscore}",
            "action:#{action}"
          ],
        )

        Failbot.report(error, {
          areas_of_responsibility: [:analytics, :audit_log],
          schema: schema,
        })
      end

      def subscribe(pattern, &block)
        if GitHub.hydro_enabled?
          @instrumenter.subscribe(pattern) do |event, _, _, _, payload|
            GitHub.dogstats.time("hydro_client.subscriber_time", {
              tags: ["event:#{event}", @role_tag],
              sample_rate: 0.1,
            }) do
              begin
                yield payload, event
              rescue => e
                if Rails.env.production?
                  Failbot.report(SubscribeError.new("Failed to encode audit log message"))
                  GitHub::Logger.log(e)
                else
                  puts "Failed to encode audit log message: #{e.message}"
                  raise(e)
                end
              end
            end
          end
        end
      end
    end
  end
end
