# frozen_string_literal: true

class Fastly
  FASTLY_API_URL = "https://api.fastly.com".freeze
  FASTLY_CUSTOMER_ID = "5bPJi8MvwBUzRTFXlk5WhX".freeze

  # This is a magic number corresponding to sni.github.map.fastly.net.
  # It is used to upload the certificate to the correct set of servers.
  FASTLY_PAGES_HTTPS_OFFSET = 153

  class Error < RuntimeError
    def areas_of_responsibility
      [:pages]
    end
  end

  class ValidationError < Error
  end

  class CertificateUploadError < Error
    attr_accessor :response
  end

  class CertificateFetchError < Error
    attr_accessor :response
  end

  class CertificateDeletionError < Error
    attr_accessor :response
  end

  class Certificate
    ATTRIBUTES = [:customer_id, :certificate_id, :certificate, :certificate_intermediate, :key_file, :offset, :approved].freeze
    attr_accessor *ATTRIBUTES

    def initialize(params)
      self.offset = FASTLY_PAGES_HTTPS_OFFSET

      ATTRIBUTES.each do |attribute|
        public_send("#{attribute}=", params[attribute]) if params.key?(attribute)
        public_send("#{attribute}=", params[attribute.to_s]) if params.key?(attribute.to_s)
      end
    end

    def self.from_json(body)
      body = JSON.parse(body) if body.is_a?(String)
      body["certificate_id"] ||= body["cert_id"]
      body["certificate"] ||= body["cert"]
      body["certificate_intermediate"] ||= body["cert_intermediate"]
      new(body)
    end

    def to_json(state = nil)
      JSON.generate(to_h, state)
    end

    def to_h
      {
        "cert"              => certificate,
        "cert_intermediate" => certificate_intermediate,
        "key_file"          => key_file,
        "offset"            => offset,
      }
    end
  end

  # Create a new Fastly client.
  #
  # options - Hash of options passed to Faraday.new.
  #
  # Returns nothing.
  def initialize(options = {})
    default_headers = {
      "Fastly-Key"   => GitHub.fastly_api_token,
      "Content-Type" => "application/json",
      "Accept"       => "application/json",
    }
    default_options = {
      url: FASTLY_API_URL,
      headers: default_headers,
      request: { timeout: 10, open_timeout: 5 },
    }
    @connection = Faraday.new(default_options[:url], default_options.merge(options)) do |f|
      f.adapter Faraday.default_adapter
    end
  end

  # Update a certificate. This modifies the certificate & certificate intermediate.
  # Useful for renewing a certificate.
  #
  # certificate - a Fastly::Certificate with the certificate_id, key_file, certificate, and certificate_intermediate fields.
  #
  # Returns a Fastly::Certificate from the response body JSON,
  # or raises a Fastly::CertificateUploadError if an unacceptable response is returned.
  def update_certificate(certificate)
    raise ValidationError, "Certificate ID is required" if certificate.certificate_id.to_s.empty?
    raise ValidationError, "Certificate is required" if certificate.certificate.to_s.empty?
    raise ValidationError, "Certificate intermediate is required" if certificate.certificate_intermediate.to_s.empty?
    raise ValidationError, "Key file is required" if certificate.key_file.to_s.empty?

    response = @connection.put do |req|
      req.url "/tls/#{FASTLY_CUSTOMER_ID}/certificate/#{certificate.certificate_id}"
      req.body = certificate.to_json
    end

    if response.status > 201
      e = CertificateUploadError.new("invalid response code: #{response.status}").tap do |err|
        err.response = response
      end
      raise e
    end

    Fastly::Certificate.from_json(response.body)
  end

  # Upload a certificate to Fastly. This should only be called once per certificate.
  # For all renewals, use #update_certificate instead.
  #
  # certificate - a Fastly::Certificate with the certificate, certificate_intermediate, and key_file fields.
  #
  # Returns a Fastly::Certificate from the response body JSON,
  # or raises a Fastly::CertificateUploadError if an unacceptable response is returned.
  def upload_certificate(certificate)
    raise ValidationError, "Certificate is required" if certificate.certificate.to_s.empty?
    raise ValidationError, "Certificate intermediate is required" if certificate.certificate_intermediate.to_s.empty?
    raise ValidationError, "Key file is required" if certificate.key_file.to_s.empty?

    response = @connection.post do |req|
      req.url "/tls/#{FASTLY_CUSTOMER_ID}/certificate"
      req.body = certificate.to_json
    end

    if response.status > 201
      e = CertificateUploadError.new("invalid response code: #{response.status}").tap do |err|
        err.response = response
      end
      raise e
    end

    Fastly::Certificate.from_json(response.body)
  end

  # Fetches a certificate's information from Fastly.
  #
  # certificate_id - a Fastly certificate ID
  #
  # Returns a Fastly::Certificate from the response body JSON,
  # or raises a Fastly::CertificateFetchError if an unacceptable response is returned.
  def get_certificate(certificate_id:)
    raise ValidationError, "Certificate ID is required" if certificate_id.to_s.empty?

    response = @connection.get do |req|
      req.url "/tls/#{FASTLY_CUSTOMER_ID}/certificate/#{certificate_id}"
    end

    if response.status != 200
      e = CertificateFetchError.new("invalid response code: #{response.status}").tap do |err|
        err.response = response
      end
      raise e
    end

    Fastly::Certificate.from_json(response.body)
  end

  # Deletes a certificate from Fastly.
  #
  # certificate_id - a Fastly Certificate ID
  #
  # Returns true if successful, or errors if not.
  def delete_certificate(certificate_id:)
    raise ValidationError, "Certificate ID is required" if certificate_id.to_s.empty?

    response = @connection.delete do |req|
      req.url "/tls/#{FASTLY_CUSTOMER_ID}/certificate/#{certificate_id}"
    end

    if response.status != 200
      e = CertificateDeletionError.new("invalid response code: #{response.status}").tap do |err|
        err.response = response
      end
      raise e
    end

    true
  end

end
