# rubocop:disable Style/FrozenStringLiteralComment

require "google/apis/fcm_v1"
require "googleauth"

module Firebase
  class CloudMessagingClient
    class ExternalServiceFailure < StandardError; end

    MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging"
    PROJECT_ID = "projects/github-mobile-cc45e"

    # https://firebase.google.com/docs/reference/fcm/rest/v1/ErrorCode
    # Error Codes
    # UNSPECIFIED_ERROR: No more information is available about this error.
    # INVALID_ARGUMENT: (HTTP error code = 400) Request parameters were invalid. An extension of
    #   type google.rpc.BadRequest is returned to specify which field was invalid.
    # UNREGISTERED: (HTTP error code = 404) App instance was unregistered from FCM. This usually
    #   means that the token used is no longer valid and a new one must be used.
    # SENDER_ID_MISMATCH (HTTP error code = 403) The authenticated sender ID is different from the
    #   sender ID for the registration token.
    # QUOTA_EXCEEDED: (HTTP error code = 429) Sending limit exceeded for the message target. An
    #   extension of type google.rpc.QuotaFailure is returned to specify which quota got exceeded.
    # UNAVAILABLE: (HTTP error code = 503) The server is overloaded.
    # INTERNAL: (HTTP error code = 500) An unknown internal error occurred.
    # THIRD_PARTY_AUTH_ERROR: (HTTP error code = 401) APNs certificate or web push auth key was invalid or missing.
    HTTP_STATUS_TO_ERROR_CODE = {
      400 => "INVALID_ARGUMENT",
      401 => "THIRD_PARTY_AUTH_ERROR",
      403 => "SENDER_ID_MISMATCH",
      404 => "UNREGISTERED",
      429 => "QUOTA_EXCEEDED",
      500 => "INTERNAL",
      502 => "BAD_GATEWAY",
      503 => "UNAVAILABLE",
    }

    UNREGISTERED_ERROR = "UNREGISTERED"
    RETRYABLE_ERRORS = %w( UNAVAILABLE INTERNAL QUOTA_EXCEEDED )

    # message - fcm payload
    # device_tokens - Array<String> of device token values.
    #
    # Return Array<Hash> representing the response of the http request
    def send_all(message:, device_tokens:)
      responses = []

      messaging_service.batch do |service|
        device_tokens.each do |token|
          fcm_message = Google::Apis::FcmV1::SendMessageRequest.new(message: message.merge(token: token))
          service.send_message(PROJECT_ID, fcm_message) do |res, err|
            if res
              GitHub.dogstats.increment(stat_key("success"))

              # Using :message_id to maintain feature parity between the legacy fcm API and v1
              responses << { status_code: 200, message_id: res.name }
              next
            end

            # this error is returns for individual subrequests within the batch request that error out
            if err
              GitHub.dogstats.increment(stat_key("failure"), tags: ["status:#{err.status_code}"])

              responses << {
                error: HTTP_STATUS_TO_ERROR_CODE[err.status_code],
                retry_after_header: err.header["Retry-After"]&.first,
                status_code: err.status_code,
              }
            end
          end
        end
      end

      # As per the comments on https://firebase.google.com/docs/cloud-messaging/send-message#send-a-batch-of-messages,
      # the elements in the batch responses array appear in the same order as the tokens in the original
      # request, hence this iteration to inject tokens back into the response
      responses.each_with_index.map { |r, i| r.merge!(token: device_tokens[i]) }

      # This error is raised if the fcm batch endpoint returns an error message
    rescue Google::Apis::Error => ex
      GitHub.dogstats.increment(stat_key("failure"), tags: ["status:#{ex.status_code}"])

      error_message = "Received HTTP #{ex.status_code}"
      Failbot.report(ExternalServiceFailure.new(error_message))

      device_tokens.map do |token|
        {
          error: HTTP_STATUS_TO_ERROR_CODE[ex.status_code],
          retry_after_header: ex.header["Retry-After"]&.first,
          status_code: ex.status_code,
          token: token,
        }
      end
    end

    private

    def messaging_service
      @messaging_service ||= Google::Apis::FcmV1::FirebaseCloudMessagingService.new.tap do |service|
        service.authorization = authorization
        service.authorization.fetch_access_token!
      end
    end

    def authorization
      @authorization ||= Google::Auth::DefaultCredentials.make_creds(
        scope: Firebase::CloudMessagingClient::MESSAGING_SCOPE,
        json_key_io: StringIO.new(GitHub.google_fcm_private_key)
      )
    end

    def stat_key(suffix)
      "firebase.deliver_mobile_push_notifications.#{suffix}"
    end
  end
end
