# frozen_string_literal: true

module ApplicationRecord
  autoload :Ballast, "application_record/ballast"
  autoload :Memex, "application_record/memex"
  autoload :Collab, "application_record/collab"
  autoload :Domain, "application_record/domain"
  autoload :Iam, "application_record/iam"
  autoload :IamAbilities, "application_record/iam_abilities"
  autoload :Mysql1, "application_record/mysql1"
  autoload :Mysql2, "application_record/mysql2"
  autoload :Mysql5, "application_record/mysql5"
  autoload :Notify, "application_record/notify"
  autoload :NotificationsDeliveries, "application_record/notifications_deliveries"
  autoload :NotificationsEntries, "application_record/notifications_entries"
  autoload :Repositories, "application_record/repositories"
  autoload :Spokes, "application_record/spokes"
  autoload :Stratocaster, "application_record/stratocaster"

  def self.connection_info
    return @connection_info if defined?(@connection_info)
    connection_handler = ApplicationRecord::Base.connection_handler # rubocop:disable GitHub/DontCallApplicationRecordBaseMethods

    @connection_info = connection_handler.connection_pool_list.each_with_object({}) do |connection_pool, hash|
      if GitHub.rails_6_0?
        # The name of the connected pool
        # => ActiveRecord::Base is primary, otherwise it's the class name like ApplicationRecord::Ballast
        pool_name = connection_pool.spec.name

        # configuration hash
        config = connection_pool.spec.config

        # Turns "primary" into "mysql1", "ApplicationRecord::Ballast" into "ballast"
        name = pool_name == "primary" ? "mysql1" : pool_name.demodulize.downcase
      else
        pool_config = connection_pool.pool_config

        # The name of the connected pool
        # => in 6.1 ActiveRecord::Base is now always ActiveRecord::Base.
        pool_name = pool_config.connection_specification_name

        # configuration hash
        config = pool_config.db_config.configuration_hash

        # Turns "ActiveRecord::Base" into "mysql1", "ApplicationRecord::Ballast" into "ballast"
        name = pool_name == "ActiveRecord::Base" ? "mysql1" : pool_name.demodulize.downcase
      end

      hash[name] = "#{config[:username]}@#{config[:host]}#{config[:socket]}/#{config[:database]}"
    end
  end

  def self.clusters
    return @clusters if defined?(@clusters)
    @clusters = ApplicationRecord::Base.subclasses # rubocop:disable GitHub/DontCallApplicationRecordBaseMethods
  end

  def self.gtid_tracking_clusters
    return @gtid_tracking_clusters if defined?(@gtid_tracking_clusters)

    @gtid_tracking_clusters = if environment_variable = GitHub.environment["MYSQL_GTID_TRACKING_CLUSTERS"]
      enabled_clusters = environment_variable.split(",").map do |cluster_name|
        cluster_name.strip.to_sym
      end

      clusters.select do |cluster|
        enabled_clusters.include?(cluster.cluster_name)
      end
    else
      clusters.select(&:gtid_tracking_enabled?)
    end
  end
end
