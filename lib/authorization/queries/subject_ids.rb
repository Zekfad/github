# frozen_string_literal: true

require_relative "base_query.rb"

module Authorization
  module Queries
    class SubjectIds < BaseQuery

      attr_reader :actor, :subject_type, :through, :actions

      def initialize(actor:, subject_type:, through: [], actions: :any)
        @actor = actor
        @subject_type = subject_type
        @through = through
        @actions = actions
      end

      def execute_implementation
        return default_result unless actor.participates?

        Ability::Graph.query do |g|
          g.get   :subject_id
          g.from  actor.ability_type, actor.ability_id
          g.to    subject_type
          g.actions actions
          through.each do |connector|
            g.through connector
          end
          g.uniq
        end.values.flatten
      end

      def default_result
        []
      end

      def validation_errors
        errors = []

        unless valid_actor?(actor)
          errors << validation_error(:invalid_actor, actor: actor)
        end

        errors
      end
    end
  end
end
