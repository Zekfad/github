# frozen_string_literal: true

module Authorization
  module Errors
    InvalidQuery = Class.new(ArgumentError)
  end
end
