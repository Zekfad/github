# frozen_string_literal: true

class TaskList
  # Provides a summary of provided TaskList `items`.
  #
  # `items` is an Array of TaskList::Item objects.
  class Summary < Struct.new(:items)
    # Public: returns true if there are any TaskList::Item objects.
    def items?
      item_count > 0
    end

    # Public: returns the number of TaskList::Item objects.
    def item_count
      items.size
    end

    # Public: returns the number of complete TaskList::Item objects.
    def complete_count
      items.count { |i| i.complete? }
    end

    # Public: returns the number of incomplete TaskList::Item objects.
    def incomplete_count
      items.count { |i| !i.complete? }
    end

    def platform_type_name
      "TaskListSummary"
    end
  end
end
