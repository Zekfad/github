# frozen_string_literal: true

module Sponsors
  class OrganizationWaitlistSurvey
    SLUG = "github_sponsors_organizations"
    FISCAL_HOST_QUESTION_SLUG = "fiscal_host_name"

    def self.find_or_create_survey
      survey = Survey.find_by_slug(SLUG)
      return survey if survey

      display_order = 0
      other_text = { text: "Other", short_text: "other" }

      survey = Survey.create!(
        title: "GitHub Sponsored Organizations waitlist",
        slug: SLUG,
      )

      non_profit_question = survey.questions.create!(
        text: "Nonprofit status (don't worry, this is not a requirement to be in the program; we just need to know for tax purposes).",
        short_text: "registered_non_profit",
        display_order: (display_order += 1),
      )
      non_profit_question.choices.create!({
        text: "No, this organization does not have nonprofit status",
        short_text: "no_non_profit",
      })
      non_profit_question.choices.create!({
        text: "Yes, this organization has nonprofit status and can receive tax-deductible donations from individuals (e.g., 501c3)",
        short_text: "yes_non_profit_and_tax_deductible",
      })
      non_profit_question.choices.create!({
        text: "Yes, this organization has nonprofit status but cannot receive tax-deductible donations from individuals (e.g., 501c6)",
        short_text: "yes_non_profit_not_tax_deductible",
      })

      survey.questions.create!(
        text: "Fiscal host name",
        short_text: FISCAL_HOST_QUESTION_SLUG,
        display_order: (display_order += 1),
      ).choices.create!(other_text)

      survey
    end
  end
end
