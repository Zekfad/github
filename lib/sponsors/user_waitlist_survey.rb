# frozen_string_literal: true

module Sponsors
  class UserWaitlistSurvey
    SLUG = "github_sponsors"

    def self.find_or_create_survey
      survey = Survey.find_by_slug(SLUG)
      return survey if survey

      display_order = 0
      other_text = { text: "Other", short_text: "other" }

      survey = Survey.create!(
        title: "GitHub Sponsored Developers waitlist",
        slug: SLUG,
      )

      survey.questions.create!(
        text: "What are your pronouns? (optional)",
        short_text: "pronouns",
        display_order: (display_order += 1),
      ).choices.create!(other_text)

      survey.questions.create!(
        text: "Which open source projects are you most involved with? Please include links.",
        short_text: "oss_projects",
        display_order: (display_order += 1),
      ).choices.create!(other_text)

      survey.questions.create!(
        text: "What is the primary type of open source contributions you make (e.g. code, mentorship, design, business development, issue triage, community management, documentation, content, translation, talks, penetration testing, bug reports, feature requests)?",
        short_text: "oss_primary_contribution_type",
        display_order: (display_order += 1),
      ).choices.create!(other_text)

      survey.questions.create!(
        text: "Why are you interested in being sponsored through the GitHub Sponsors program?",
        short_text: "why_sponsors",
        display_order: (display_order += 1),
      ).choices.create!(other_text)

      survey.questions.create!(
        text: "How did you find out about GitHub Sponsors?",
        short_text: "how_found_sponsors",
        display_order: (display_order += 1),
      ).choices.create!(other_text)

      survey.questions.create!(
        text: "Which developers would you like to sponsor through GitHub Sponsors who aren't already part of the program? Please share their GitHub username(s) if they have one. (optional)",
        short_text: "new_sponsors_suggestions",
        display_order: (display_order += 1),
      ).choices.create!(other_text)

      survey.questions.create!(
        text: "Anything else we should know? (optional)",
        short_text: "anything_else",
        display_order: (display_order += 1),
      ).choices.create!(other_text)

      survey
    end
  end
end
