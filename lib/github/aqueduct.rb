# frozen_string_literal: true

module GitHub
  module Aqueduct
    autoload :Job, "github/aqueduct/job"

    LARGE_PAYLOAD_SIZE_MIN = 5242880  # 5MiB
    LARGE_PAYLOAD_SIZE_MAX = 26214400 # 25MiB

    # Public: Checks if the payload size is considered "large" to aqueduct.
    #
    # size_bytes - The payload size in bytes.
    #
    # Returns a boolean for whether the size is large.
    def self.is_payload_size_large?(size_bytes)
      size_bytes.between?(LARGE_PAYLOAD_SIZE_MIN, LARGE_PAYLOAD_SIZE_MAX)
    end
  end
end
