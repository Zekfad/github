# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Middleware
    autoload :Constants, "github/middleware/constants"
    autoload :AnonymousRequest, "github/middleware/anonymous_request"
  end
end
