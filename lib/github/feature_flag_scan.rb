#!/usr/bin/env ruby
# frozen_string_literal: true

require "open3"

module GitHub
  class FeatureFlagScan
    class FeatureFlagScanError < StandardError; end

    attr_accessor :dirs, :sha

    SEARCH_DIRS = [
      "app",
      "jobs",
      "lib",
      "test",
      "config",
      "script",
    ]

    USAGE_PATTERNS = GitHub::FeatureFlag::FLIPPER_USAGE_PATTERNS

    def initialize(dirs = SEARCH_DIRS)
      @dirs = dirs
      @sha = IO.popen("git rev-parse --short HEAD").read.chomp
    end

    def perform
      pattern_args = USAGE_PATTERNS.flat_map { |pattern| ["-e", pattern.source] }
      cmd = ["git", "grep", "-IEw", *pattern_args, "--", *dirs]

      out, err, status = Open3.capture3(*cmd)

      if status.success?
        {
          sha: sha,
          flags: flags_from(out)
        }.to_json
      else
        Failbot.report(FeatureFlagScanError.new(err), sha: sha)
        false
      end
    end

    private

    def flags_from(grep_results)
      flags = {}

      combined_pattern = Regexp.union(*USAGE_PATTERNS)

      grep_results.lines.each do |line|
        _, filename, _, snippet = line.split(/^([^:]*):()/)
        flag_name = combined_pattern.match(snippet).captures.compact[0]
        flags[flag_name] ||= { files: Set.new }
        flags[flag_name][:files] << filename
      end

      flags.each_pair do |name, values|
        values[:files] = values[:files].to_a

        if ffeature = FlipperFeature.find_by(name: name)
          flags[name].merge!({
            github_id: ffeature.id,
            created_at: ffeature.created_at,
            updated_at: ffeature.updated_at,
            state: ffeature.state,
            description: ffeature.description,
            slack_channel: ffeature.slack_channel,
            github_team: ffeature.github_org_team_id,
            service_name: ffeature.service_name
          })
        end
      end
    end
  end
end
