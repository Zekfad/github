# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Notifications

    class InstrumentationSubscriberError < RuntimeError
      def initialize(exceptions)
        exception_messages = exceptions.map { |e| "[#{e.class.name}] #{e.message}" }
        errors_text = exceptions.count == 1 ? "error" : "errors"
        super "#{exceptions.count} #{errors_text} occurred while running subscribers: #{exception_messages.to_sentence}"
      end
    end

    class FanoutWithExceptionHandling < ActiveSupport::Notifications::Fanout
      def initialize(existing: nil)
        super()

        if existing
          @other_subscribers = existing.instance_variable_get(:@other_subscribers).dup
          @string_subscribers = existing.instance_variable_get(:@string_subscribers).dup
          @listeners_for = existing.instance_variable_get(:@listeners_for).dup
        end
      end

      def start(name, id, payload)
        each_with_exception_handling(listeners_for(name)) do |s|
          s.start(name, id, payload)
        end
      end

      def finish(name, id, payload, listeners = listeners_for(name))
        each_with_exception_handling(listeners) do |s|
          s.finish(name, id, payload)
        end
      end

      def publish(name, *args)
        each_with_exception_handling(listeners_for(name)) do |s|
          s.publish(name, *args)
        end
      end

      private

      def each_with_exception_handling(listeners)
        exceptions = nil

        listeners.each do |listener|
          begin
            yield listener
          rescue => e
            exceptions ||= []
            exceptions << e
            Failbot.report!(e)
          end
        end

      ensure
        if exceptions
          exp = InstrumentationSubscriberError.new(exceptions)
          exp.set_backtrace exceptions.first.backtrace
          raise exp
        end
      end
    end
  end
end
