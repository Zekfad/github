# frozen_string_literal: true
require "github-ds"
require "github/sql"

module GitHub
  class KV
    class MissingConnectionError < StandardError
      def areas_of_responsibility
        [:platform]
      end
    end

    def name
      "GitHub::KV".freeze
    end

    def ttl(key)
      validate_key(key)

      Result.new {
        ApplicationRecord::Domain::KeyValues.github_sql.value(<<-SQL, key: key)
          SELECT expires_at FROM key_values
          WHERE `key` = :key AND (expires_at IS NULL OR expires_at > NOW())
        SQL
      }
    end
  end
end
