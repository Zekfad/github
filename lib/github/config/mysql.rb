# rubocop:disable Style/FrozenStringLiteralComment

require "trilogy"
require "trilogy_adapter"
require "resilient/trilogy"
require "github/trilogy_factory"
require "erb"
require "active_support/lazy_load_hooks"

require "application_record"

module GitHub
  module Config
    module Mysql
      def load_activerecord
        require "active_record"
        # Anytime ActiveRecord is loaded, we also want the ds extensions loaded.
        require "github/ds_extensions"

        if ActiveRecord::Base.configurations.empty?
          ActiveRecord::Base.configurations = database_yml
          write_db = database_config(name: :mysql1_primary)
          read_db = database_config(name: :mysql1_readonly)

          ActiveRecord::Base.connection_handlers[:writing].establish_connection write_db
          ActiveRecord::Base.connection_handlers[:reading].establish_connection read_db
        end
      end

      # An Array of models whose connections should be used to cache schemas in script/cache_schema.
      def schema_cached_models
        @schema_cached_models ||= [
          ApplicationRecord::Ballast,
          ApplicationRecord::Memex,
          ApplicationRecord::Collab,
          ApplicationRecord::Iam,
          ApplicationRecord::IamAbilities,
          ApplicationRecord::Mysql1,
          ApplicationRecord::Mysql2,
          ApplicationRecord::Mysql5,
          ApplicationRecord::Notify,
          ApplicationRecord::NotificationsDeliveries,
          ApplicationRecord::NotificationsEntries,
          ApplicationRecord::Spokes,
          ApplicationRecord::Stratocaster,
          ApplicationRecord::Repositories
        ]
      end

      # Returns the database configuration for the given `name:` and
      # current `Rails.env`.
      def database_config(name: nil)
        config = (name && database_yml[Rails.env][name.to_s]) || database_yml[Rails.env]["mysql1_primary"]
        config.symbolize_keys
      end

      def database_yml
        YAML.load(ERB.new(File.read("#{Rails.root}/config/database.yml")).result)
      end

      module AlternateConnections
        def primary_class?
          super || self == ApplicationRecord::Mysql1
        end

        # Enterprise doesn't support multi-db except for a read replica.
        # We need to treat it slightly differently than the other databases
        # by allowing the other roles to get set for mysql1, but not for the
        # other databases.
        def connects_to_mysql1
          if Rails.env.production?
            if GitHub.enterprise?
              # Enterprise doesn't have an entry for mysql1_readonly_slow
              connects_to database: { writing: :mysql1_primary, reading: :mysql1_readonly, reading_slow: :mysql1_readonly }
            else
              connects_to database: { writing: :mysql1_primary, reading: :mysql1_readonly, reading_slow: :mysql1_readonly_slow }
            end
          else
            # Dev and test only have a mysql1_primary entry
            #
            # We don't call `connects_to_same` because we need the `mysql1`
            # connections to create the handlers and setup inital connections.
            connects_to database: { writing: :mysql1_primary, reading: :mysql1_primary, reading_slow: :mysql1_primary }
          end
        end

        def connects_to(database: {})
          return if !primary_class? && GitHub.enterprise?

          if Rails.env.production? || primary_class?
            super(database: database)
          else
            connects_to_same(database: database[:writing])
          end
        end

        def connects_to_same(database:)
          raise NotImplementedError, "must not be called in production" if Rails.env.production?
          return if GitHub.enterprise?

          if GitHub.rails_6_0?
            pool = nil
            writing_conn_proc = proc do
              pool = establish_connection(database)
            end
            reading_conn_proc = proc do
              connection_handler.send(:owner_to_pool)[self.name] = pool
            end
          else
            owner_to_pool_manager = nil
            writing_conn_proc = proc do
              establish_connection(database)
              owner_to_pool_manager = connection_handler.instance_variable_get(:@owner_to_pool_manager)
            end
            reading_conn_proc = proc do
              connection_handler.instance_variable_get(:@owner_to_pool_manager)[connection_specification_name] = owner_to_pool_manager[connection_specification_name]
            end
          end

          ActiveRecord::Base.connected_to(role: :writing, &writing_conn_proc)
          ActiveRecord::Base.connected_to(role: :reading, &reading_conn_proc)
          ActiveRecord::Base.connected_to(role: :reading_slow, &reading_conn_proc)
        end

        def connected_to(role: nil, prevent_writes: false, &blk)
          if role == :reading_slow
            # Only when called from a console:
            if $console && Rails.env.production?
              warn ""
              warn "Sorry, slow mysql queries aren't allowed from the console!"
              warn "Use the console-specific connection for slow queries:"
              warn ""
              warn "$ gh-dbconsole slow"
              warn ""
              return
            end
          end

          super
        end

        def connect_all_to_same
          raise "must only be called in test" unless Rails.env.test?

          ActiveRecord::Base.establish_connection :mysql1_primary # rubocop:disable GitHub/DoNotCallMethodsOnActiveRecordBase

          ApplicationRecord::Ballast.connects_to_same database: :ballast_primary
          ApplicationRecord::Collab.connects_to_same database: :collab_primary
          ApplicationRecord::Iam.connects_to_same database: :iam_primary
          ApplicationRecord::IamAbilities.connects_to_same database: :abilities_primary
          ApplicationRecord::Memex.connects_to_same database: :memex_primary
          ApplicationRecord::Mysql2.connects_to_same database: :notifications_primary
          ApplicationRecord::Mysql5.connects_to_same database: :kv_primary
          ApplicationRecord::Notify.connects_to_same database: :notify_primary
          ApplicationRecord::NotificationsDeliveries.connects_to_same database: :notifications_deliveries_primary
          ApplicationRecord::NotificationsEntries.connects_to_same database: :notifications_entries_primary
          ApplicationRecord::Repositories.connects_to_same database: :repositories_primary
          ApplicationRecord::Spokes.connects_to_same database: :spokes_write
          ApplicationRecord::Stratocaster.connects_to_same database: :stratocaster_primary
        end
      end
    end
  end

  extend Config::Mysql
end

ActiveSupport.on_load(:active_record) do
  extend GitHub::Config::Mysql::AlternateConnections

  ActiveRecord::Base.connection_handlers = {
    writing: ActiveRecord::Base.connection_handler,
    reading: ActiveRecord::ConnectionAdapters::ConnectionHandler.new,
    reading_slow: ActiveRecord::ConnectionAdapters::ConnectionHandler.new,
  }

  ActiveRecord::ConnectionAdapters::TrilogyAdapter.database_driver = GitHub::TrilogyFactory
end

# See http://dev.mysql.com/doc/refman/5.0/en/storage-requirements.html
# 2^16 - 1 bytes is the limit for BLOB and TEXT fields.
# If you need longer, you should use MEDIUMBLOB or MEDIUMTEXT.
MYSQL_TEXT_FIELD_LIMIT = 65535

# This size will be backed by a MEDIUMBLOB in the database, but is
# just enough space to fit 65k 4-byte unicode characters. Esentially
# this is the binary version of a TEXT field meant to store Unicode.
MYSQL_UNICODE_BLOB_LIMIT = 262144
