# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Config
    module SupportLink
      SUPPORT_LINK_KEY = "enterprise:support_link".freeze
      SUPPORT_LINK_TYPE_KEY = "enterprise:support_link_type".freeze

      # Get the support link to be used in the app. Loaded via the
      # ENTERPRISE_SUPPORT_LINK environment variable in GHES.
      #
      # @support_link is always accompained with @support_link_type
      # For enterprise instances that have not upgraded to a version
      # with this feature, there will only be @support_email set
      #
      # Returns String.
      def support_link
        tmp_support_link || @support_link || @support_email || default_support_email
      end
      attr_writer :support_link
      attr_writer :support_email # Supporting @support_email for legacy purposes

      # Get the support link type for the matching #support_link.
      # Support link type can be "email" or "url" depending on configuration.
      # Loaded via the ENTERPRISE_SUPPORT_LINK_TYPE environment variable in GHES.
      # If not set (on dotcom) then returns "email".
      #
      # Returns String.
      def support_link_type
        tmp_support_link_type || @support_link_type || "email"
      end
      attr_writer :support_link_type

      # Public: Get the temporary GHES support link to be used within the app
      # while a configuration run occurs and persists the support link and type.
      #
      # Returns String.
      def tmp_support_link
        return if !private_instance? || !GitHub.respond_to?(:kv)
        GitHub.kv.get(SUPPORT_LINK_KEY).value { nil }
      end

      # Public: Get the temporary GHES support link type to be used within the app
      # while a configuration run occurs and persists the support link and type.
      #
      # Returns String.
      def tmp_support_link_type
        return if !private_instance? || !GitHub.respond_to?(:kv)
        GitHub.kv.get(SUPPORT_LINK_TYPE_KEY).value { nil }
      end

      # The noreply address is the "unconfigured" default on Enterprise if the
      # admins have not set a support email address. We should not link to or
      # show this default value on Enterprise if it can avoided, but will need
      # to use it in some cases such as outgoing emails if nothing more
      # appropriate is available. For example if nothing is set, or if a
      # support link is being used instead of a support email.
      def default_support_email
        enterprise? ? GitHub.urls.noreply_address : "support@github.com"
      end

      def support_link_text
        support_link_type == "url" ? "Please visit #{support_link}" : "Please contact #{who_to_contact}"
      end

      # Makes just the first word in support_link_text lowercase
      def support_link_text_lowercase
        support_link_type == "url" ? "please visit #{support_link}" : "please contact #{who_to_contact}"
      end

      def support_link_text_no_link
        support_link_type == "url" ? "Please visit" : "Please contact"
      end

      def support_link_text_no_link_lowercase
        support_link_text_no_link.downcase
      end

      def support_link_text_no_please
        support_link_type == "url" ? "visit #{support_link}" : "contact #{who_to_contact}"
      end

      def support_link_text_no_please_no_link
        support_link_type == "url" ? "visit" : "contact"
      end

      def support_link_text_mailto_or_link
        support_link_type == "url" ?  "#{support_link}" : "mailto:#{support_link}"
      end

      # Prefer using support_link and text methods
      # However code such as ApplicationMailer needs an email address,
      # so this code uses support_link if its an email otherwise it uses defaults
      def support_email
        if support_link_type == "email"
          support_link
        else
          default_support_email
        end
      end

      # Is the current support email configured correctly
      # (as by default it is not on Enterprise)?
      def support_link_not_enterprise_default?
        return true unless enterprise?
        support_link != GitHub.urls.noreply_address
      end

      private

      def who_to_contact
        support_link_not_enterprise_default? ?
          support_link :
          "your local GitHub Enterprise site administrator"
      end
    end
  end

  extend Config::SupportLink
end
