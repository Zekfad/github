# rubocop:disable Style/FrozenStringLiteralComment

require "flipper"
require "flipper/adapters/instrumented_with_lazy"

Flipper.configure do |config|
  config.default do
    big_features = GitHub::Config::Flipper.big_features
    mysql = ::Flipper::Adapters::Mysql.new(exclude_actors_for: big_features)
    mysql = ::Flipper::Adapters::InstrumentedWithLazy.new(mysql, instrumenter: GitHub.instrumentation_service)
    if ENV["TEST_ALL_FEATURES"]
      mysql = ::Flipper::Adapters::EnabledByDefault.new(mysql)
      mysql.blacklist(:audit_log_hydro)
      mysql.blacklist(:audit_log_driftwood_query)
      mysql.blacklist(:deliver_team_add_email)
      mysql.blacklist(:registry_disabled)
      mysql.blacklist(:registry_alpha_disabled)
      mysql.blacklist(:api_quota)
      # We don't run the hydro-to-resque relay in the test env, so we shouldn't
      # enable the feature flag to publish all jobs via hydro.
      mysql.blacklist(:enqueue_to_hydro)

      # We don't run aqueduct in the test env, so we shouldn't enable the feature flags
      %w(
        backupworker
        depworker
        dfs
        gitbackups_maintenance
        graphworker
        highworker
        hookworker
        lowworker
        pagesworker
        slowworker
        staff
      ).each do |worker_class|
        mysql.blacklist("aqueduct_enqueue_github_#{worker_class}")
        mysql.blacklist("aqueduct_workers_github_#{worker_class}")
      end

      # Don't skip in test unless explicitly asked:
      mysql.blacklist(:active_job_skip_enqueue)

      # To artificially sunset deprecated GraphQL fields, we generate a feature flag
      # that acts like a switch to turn off a batch of fields every quarter
      # Since these flags are time sensitive, it often leads to a broken master.
      # We don't want to couple tests to time anyways, and we also don't want to have
      # to remove tests for deprecated fields right away. So here we blacklist these flags:
      Platform::Schema.upcoming_changes.switch_flags.each do |sunset_flag|
        mysql.blacklist(sunset_flag)
      end
    end
    memcached = ::Flipper::Adapters::Memcacheable.new(mysql, GitHub.cache)
    lazy = ::Flipper::Adapters::LazyActors.new(memcached, big_features)
    override = ::Flipper::Adapters::Override.new(lazy)
    ::Flipper.new(override, instrumenter: GitHub.instrumentation_service)
  end
end

module GitHub
  module Config
    module Flipper
      BIG_FEATURES = %w(
        advanced_security_private_beta
        advanced_security_public_beta
        actions_v2_upgrade_notice
        alephd_push_events
        container_registry_ui
        discussions
        discussions_for_repo_users
        exact_match_search
        geyser_index
        launch_dreamlifter
        launch_lab
        launch_project
        notifications_v2
        registry
        scheduled_reminders_frontend
        verified_device_enforcement_opt_out
        workspaces
      )

      def self.big_features
        GitHub.enterprise? ? [] : BIG_FEATURES
      end

      def flipper
        ::Flipper.instance
      end
    end
  end

  extend Config::Flipper
end
