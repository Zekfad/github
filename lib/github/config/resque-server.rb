# rubocop:disable Style/FrozenStringLiteralComment

# Resque server config. Loaded into all full environments. Any server specific
# resque modifications and plugins should be loaded here instead of
# github/config/resque.rb because that library is designed for cpu/memory
# constrained environments.
require "github/config/resque"
require "github/config/failbot"
require "github/config/notifications"

require "resque"
require "resque_timer"
require "securerandom"

require "github/resque_pauser"

# configure the failbot backend for exception reporting
require "failbot"
require "failbot/resque_failure_backend"

Resque::Failure.backend = Resque::Failure::Failbot

module Resque
  class Job
    def perform_with_github
      original_component = GitHub.component
      GitHub.component = :resque
      is_setup = setup_marginalia
      setup_contexts
      setup_failbot
      job_class = class_from_payload.to_s.underscore
      send_queued_metrics(job_class)

      original_flipper_memoizing = GitHub.flipper.adapter.memoizing?
      GitHub.flipper.adapter.memoize = true

      success = false
      timer = Timer.start

      GitHub::MysqlInstrumenter.reset_stats
      GitHub::MysqlQueryCounter.start
      GitHub::JobStats.reset
      GitRPCLogSubscriber.reset_stats
      pre_perform_allocation_count = GC.stat(:total_allocated_objects)
      GitHub::JobStats.memory_usage_reset(job_class: job_class)
      # Run with connected_to block to enable the query cache
      ActiveRecord::Base.connected_to(role: :writing) do
        with_tracing { perform_without_github }
      end
      success = true
    ensure
      send_perform_metrics(job_class, timer, success, pre_perform_allocation_count) if timer
      GitHub::MysqlQueryCounter.stop

      # only clear marginalia if it was properly setup by #setup_marginalia
      Marginalia::Comment.clear! if is_setup
      GitHub.component = original_component
      GitHub.flipper.adapter.memoize = original_flipper_memoizing
    end

    # Measures the execution time for a job.
    def send_perform_metrics(job_class, timer, success, pre_perform_allocation_count)
      timer.stop
      GitHub::JobStats.memory_usage_snapshot

      GitHub.publish("performed.resque",
        timer.started_at,
        Time.now,
        SecureRandom.hex(10),
        queue: queue,
        class: job_class,
        success: success,
        db_counts: GitHub::MysqlQueryCounter.counts,
        pre_perform_allocation_count: pre_perform_allocation_count,
        backend: :resque)

      GlobalInstrumenter.instrument("performed.job", {
        queue: queue,
        job_class: class_from_payload.to_s,
        active_job_id: active_job_id_from_payload,
        timer: timer,
        success: success,
        backend: :resque,
      })
    rescue StandardError => err
      handle_job_setup_error(err)
    end

    # Measures the time between when the job was pushed onto the queue by the
    # client and now and reports it as a queued.resque event (AS::Notifications). The
    # duration of the event is the lag between enqueueing and being processed.
    def send_queued_metrics(job_class)
      if ts = queued_at
        GitHub.publish("queued.resque",
          ts,
          Time.now,
          SecureRandom.hex(10),
          queue: queue,
          class: job_class,
          backend: :resque)
      end
    rescue StandardError => err
      handle_job_setup_error(err)
    end

    # see config/initializers/marginalia.rb
    #
    # Returns truthy object if Marginalia is setup, or false.
    def setup_marginalia
      return unless Object.const_defined?(:Marginalia)
      ctrl = marginalia_controller
      ctrl.marginalia_route = payload["class"].to_s.underscore
      Marginalia::Comment.update!(ctrl)
    rescue StandardError => err
      handle_job_setup_error(err)
    end

    def setup_contexts
      job = class_from_payload
      meta = payload["meta"] || {}

      job_context = {
        job: job,
        areas_of_responsibility: payload_class.try(:areas_of_responsibility),
      }

      audit_context = (meta["audit_context"] || {}).merge(job_context)
      github_context = (meta["context"] || {}).merge(job_context)

      Audit.context.reset
      Audit.context.push(audit_context)

      GitHub.context.reset
      GitHub.context.push(github_context)

      SensitiveData.context.reset

      $0 = "#{$0}: #{job}"
    end

    # Setup initial Failbot context for jobs
    def setup_failbot
      Failbot.reset!

      if (meta = payload["meta"]) && meta["queued_at"]
        queue_time = Time.now - Time.at(meta["queued_at"])
      end

      context = meta && meta["context"]

      Failbot.push(
        areas_of_responsibility: areas_of_responsibility,
        connections: proc { ApplicationRecord.connection_info },
        queue: queue,
        queue_time: queue_time,
        queued_from: (context && context["from"]),
        rails: Rails.version,
        request_id: (context && context["request_id"]),
        job: class_from_payload,
      )
    end

    def handle_job_setup_error(err)
      Failbot.report(err, fatal: "NO")
    end

    alias perform_without_github perform
    alias perform perform_with_github

    # Initializes a single fake marginalia controller stub for every job.  Resque
    # performs one job at a time, so we can re-use this object and just set
    # #marginalia_route in #setup_marginalia.
    def marginalia_controller
      @marginalia_controller ||= OpenStruct.new(request_category: "worker")
    end

    private

    def with_tracing
      GitHub.tracer.with_enabled(GitHub.tracing_enabled?) do
        job_name = payload["class"].to_s.underscore
        job_name.gsub!("github/jobs/", "")

        meta = payload.fetch("meta", {})
        tracing_carrier = meta.fetch("opentracing_carrier", {})
        parent_context = GitHub.tracer.extract(OpenTracing::FORMAT_TEXT_MAP, tracing_carrier)

        # NOTE: FollowsFrom would be more appropriate for a background job, but lightstep-ruby doesn't support it yet.
        GitHub.tracer.with_span("job.#{job_name}", child_of: parent_context) do |span|
          span.set_tag("component", "jobs")
          span.set_tag("job.class", payload["class"])
          span.set_tag("job.queue", queue)

          span.set_tag("job.queued_at", meta["queued_at"])

          context = meta.fetch("context", {})
          span.set_tag("guid:github_request_id", context["request_id"])
          span.set_tag("job.queued_from", context["from"])

          yield

          log_data = {event: "job read/write counts"}.merge(GitHub::MysqlQueryCounter.counts).symbolize_keys
          span.log(**log_data)
        end
      end
    end

    def areas_of_responsibility
      class_from_payload.safe_constantize.try(:areas_of_responsibility)
    end

    def class_from_payload
      job_class = payload["class"]

      if job_class == ActiveJob::QueueAdapters::ResqueAdapter::JobWrapper.name
        job_args = payload["args"]&.first
        job_class = job_args["job_class"] if !job_args.blank? && !job_args["job_class"].blank?
      end

      job_class
    end

    def active_job_id_from_payload
      job_class = payload["class"]

      return unless job_class == ActiveJob::QueueAdapters::ResqueAdapter::JobWrapper.name

      job_args = payload["args"]&.first
      return unless job_args.present? && job_args.is_a?(Hash)

      job_args["job_id"]
    end
  end

  module VersionInProcline
    ProclineVersion = "[#{GitHub.current_sha[0, 7]}] "
    def procline(string)
      super(ProclineVersion + string)
    end
  end

  module GlobalWorkerReference
    def initialize(*)
      $resque_worker = self
      super
    end
  end
end

# Resque v1.20.0 only supports a single after_fork hook. This is it.
Resque.after_fork do |job|
  # Need to chdir under enterprise due to upgrade switching out dir inodes
  Dir.chdir Rails.root if GitHub.enterprise?

  # Reset the Failbot context before each job run.
  Failbot.reset!

  # Start a publishing batch for more efficient publishing of events from
  # within a resque job.
  if GitHub.hydro_enabled?
    GitHub.hydro_publisher.start_batch
  end
end

# Stops the queue from reserving new jobs effectively keeping the workers
# from doing any work from a specific queue. Helpful when GitHub's queues
# get a bit overloaded.
Resque.before_reserve do |queue|
  if GitHub.resque_pauser.paused?(queue, GitHub.server_site)
    raise Resque::Job::DontReserve
  end
end

# Runs after a job finishes but before the next job is reserved.
#
# XXX This will need some special attention when resque is upgraded to a newer
# upstream version. The current hook implementation passes the Worker object,
# upstream passes the job.
Resque.after_perform do |worker|
  if GitHub.hydro_enabled?
    result = GitHub.hydro_publisher.flush_batch
    if result && !result.success?
      Failbot.report(result.error, {
        app: "github-hydro",
        areas_of_responsibility: [:analytics],
      })
    end
  end

  next if GitHub.resque_graceful_memory_limit.nil?

  memrss = GitHub::Memory.memrss
  GitHub.dogstats.distribution("worker.memrss", memrss)

  if memrss > GitHub.resque_graceful_memory_limit
    GitHub.dogstats.increment("resque.graceful_exit")
    worker.shutdown
  end
end

# Store a reference to the current worker in a global variable, so that jobs
# can use it to check the shutdown status of the worker. Since this is a
# `prepend` we need to apply it here (at load time).
Resque::Worker.prepend(Resque::GlobalWorkerReference)
