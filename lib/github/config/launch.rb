# frozen_string_literal: true

module GitHub
  module Config
    module Launch
      # The address of the credz service.
      #
      # e.g. "launch-credz.service.iad.github.net:1918"
      attr_reader :launch_credz_address
      def launch_credz_address=(val)
        @launch_credz = nil
        @launch_credz_address = val
      end

      # The HMAC signature secret for request verification.
      # This HMAC signature secret is specific to requests to Credz.
      attr_reader :launch_credz_hmac_secret
      def launch_credz_hmac_secret=(val)
        @launch_credz = nil
        @launch_credz_hmac_secret = val
      end

      # A gRPC client to communicate with the credz client.
      #
      # Returns a gRPC client for the credz service, or returns nil if the address is not configured.
      def launch_credz
        require "github-launch"
        @launch_credz ||= build_launch_grpc_client(
          GitHub::Launch::Services::Credz::CredentialsService,
          addr: launch_credz_address,
          hmac_secret: GitHub.launch_credz_hmac_secret,
        )
      end

      # The address of the deployer service.
      #
      # e.g. "launch-deployer.service.iad.github.net:1918"
      attr_reader :launch_deployer_address
      def launch_deployer_address=(val)
        @launch_deployer = nil
        @launch_deployer_address = val
      end

      # The HMAC signature secret for request verification.
      # This HMAC signature secret is specific to requests to the Deployer.
      attr_reader :launch_deployer_hmac_secret
      def launch_deployer_hmac_secret=(val)
        @launch_deployer = nil
        @launch_deployer_hmac_secret = val
      end

      # The address of the deployer service in lab.
      #
      # e.g. "launch-deployer-lab.service.iad.github.net:1918"
      attr_reader :launch_lab_deployer_address
      def launch_lab_deployer_address=(val)
        @launch_lab_deployer = nil
        @launch_lab_deployer_address = val
      end

      # The HMAC signature secret for request verification.
      # This HMAC signature secret is specific to requests to the Deployer in lab.
      attr_reader :launch_lab_deployer_hmac_secret
      def launch_lab_deployer_hmac_secret=(val)
        @launch_lab_deployer = nil
        @launch_lab_deployer_hmac_secret = val
      end

      # A gRPC client to communicate with the launch_deployer client.
      #
      # Returns a gRPC client for the launch_deployer service, or returns nil if the address is not configured.
      def launch_deployer
        require "github-launch"
        @launch_deployer ||= build_launch_grpc_client(
          GitHub::Launch::Services::Deploy::LaunchDeploymentService,
          addr: launch_deployer_address,
          hmac_secret: GitHub.launch_deployer_hmac_secret,
        )
      end

      # A gRPC client to communicate with the launch_deployer client for the lab environment.
      #
      # Returns a gRPC client for the launch_deployer service (lab environment), or returns nil if the address is not configured.
      def launch_lab_deployer
        require "github-launch"
        @launch_lab_deployer ||= build_launch_grpc_client(
          GitHub::Launch::Services::Deploy::LaunchDeploymentService,
          addr: launch_lab_deployer_address,
          hmac_secret: GitHub.launch_lab_deployer_hmac_secret,
        )
      end

      # A gRPC client to communicate with the self hosted runners client.
      # It uses the same address and secret as the deployer service.
      #
      # Returns a gRPC client for the self hosted runners service, or returns nil if the address is not configured.
      def launch_selfhostedrunners
        require "github-launch"
        @launch_selfhostedrunners ||= build_launch_grpc_client(
          GitHub::Launch::Services::Selfhostedrunners::SelfHostedRunners,
          addr: launch_deployer_address,
          hmac_secret: GitHub.launch_deployer_hmac_secret,
        )
      end

      # A gRPC client to communicate with the runner groups client.
      # It uses the same address and secret as the deployer service.
      #
      # Returns a gRPC client for the runner groups service, or returns nil if the address is not configured.
      def launch_runnergroups
        require "github-launch"
        @launch_runnergroups ||= build_launch_grpc_client(
          GitHub::Launch::Services::Runnergroups::RunnerGroups,
          addr: launch_deployer_address,
          hmac_secret: GitHub.launch_deployer_hmac_secret,
        )
      end

      # A gRPC client to communicate with the artifacts exchange client.
      # It uses the same address and secret than the deployer service.
      #
      # In production GitHub we can see workflow executions from both production and lab Actions app.
      # Artifacts must be requested from appropriate Actions app which created the related check suite, otherwise they will not be found.
      #
      # Returns a gRPC client for the artifacts exchange service, or returns nil if the address is not configured.
      def launch_artifacts_exchange_for_check_suite(check_suite)
        require "github-launch"
        raise ArgumentError.new("check_suite must have been created by a GitHub App") if check_suite.present? && check_suite.github_app.ghost?
        if check_suite.present? && check_suite.github_app.launch_lab_github_app?
          @launch_lab_artifacts_exchange ||= build_launch_grpc_client(
            GitHub::Launch::Services::Artifactsexchange::ActionsArtifactExchange,
            addr: launch_lab_deployer_address,
            hmac_secret: GitHub.launch_lab_deployer_hmac_secret,
          )
        else
          @launch_artifacts_exchange ||= build_launch_grpc_client(
            GitHub::Launch::Services::Artifactsexchange::ActionsArtifactExchange,
            addr: launch_deployer_address,
            hmac_secret: GitHub.launch_deployer_hmac_secret,
          )
        end
      end

      # Public: The ID of the app where launch tokens should be associated.
      def launch_app_id
        @launch_app_id ||= OauthApplication.where(
          user_id: trusted_oauth_apps_owner,
          name: ["GitHub Launch Deploy Button", "GitHub Launch"],
        ).pluck(:id).first
      end

      # Public: The GitHub App (Integration) that the Actions execution environment uses.
      #
      # This is used as part of the identifier for secrets in both prod and lab.
      def launch_github_app
        return nil unless GitHub.actions_enabled?
        @launch_github_app ||= Integration.where(
          owner_id: trusted_oauth_apps_owner,
          name: launch_github_app_name,
        ).first
      end

      # Public: The name of the Actions GitHub App.
      def launch_github_app_name
        @launch_github_app_name ||= "GitHub Actions"
      end
      attr_writer :launch_github_app_name

      # Public: The ID of the GitHub App (Integration) that launch authenticates as in lab.
      def launch_lab_github_app
        return nil unless GitHub.actions_enabled?
        @launch_lab_github_app ||= Integration.where(
          owner_id: trusted_oauth_apps_owner,
          name: launch_lab_github_app_name,
        ).first
      end

      # Public: The name of launch's GitHub App.
      def launch_lab_github_app_name
        @launch_lab_github_app_name ||= "GitHub Actions (lab)"
      end
      attr_writer :launch_lab_github_app_name

      # Where the PEM-encoded Launch CA certificates are stored.
      attr_accessor :launch_grpc_ca_certificates_path

      # The PEM-encoded Launch CA certificates, both the intermediate CA cert and the root CA cert.
      def launch_grpc_channel_credentials
        return @launch_grpc_channel_credentials if defined?(@launch_grpc_channel_credentials)
        # This magic symbol is required to communicate over GRPC in the clear (see https://grpc.io/docs/guides/auth/)
        return @launch_grpc_channel_credentials = :this_channel_is_insecure unless GitHub.launch_grpc_ssl_enabled?
        return @launch_grpc_channel_credentials = nil unless File.exist?(launch_grpc_ca_certificates_path)

        @launch_grpc_channel_credentials = GRPC::Core::ChannelCredentials.new(File.read(launch_grpc_ca_certificates_path))
      end

      # Internal. Builds a GRPC client at the given namespace with the given address.
      # If you are trying to test a GRPC client, use a `mock` instead.
      def build_launch_grpc_client(ns, addr:, hmac_secret:, timeout: 10)
        return nil if addr.blank?

        require "grpc"
        require "github-launch"
        require "github-launch/hmac_client_interceptor"
        if launch_grpc_channel_credentials.nil?
          raise GRPC::Unavailable.new("Launch gRPC CA certificates are missing!!!!!")
        end

        interceptors = []
        if hmac_secret
          interceptors << GitHub::Launch::HMACClientInterceptor.new(hmac_secret: hmac_secret)
        end

        ns::Stub.new(
          addr,
          launch_grpc_channel_credentials,
          timeout: timeout,
          interceptors: interceptors,
        )
      end
    end
  end

  extend Config::Launch
end
