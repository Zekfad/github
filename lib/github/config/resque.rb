# rubocop:disable Style/FrozenStringLiteralComment

# Resque client configuration.
#
# This lib is designed for memory/cpu constrained programs (like hook scripts
# and whatnot) to be able to enqueue resque jobs with minimum overhead. For
# resque server config, see config/initializers/resque-server.rb.
#
# NOTE Don't add more libs here unless absolutely necessary!

# setup the basic Resque object
require "github"
require "github/config/context"
require "github/config/redis"
require "audit/context"

require "resque"
Resque.redis = GitHub.resque_redis

FS_QUEUE_REGEX = /_(fs\d+|github-d?fs\d+-cp\d-prd|github-dfs-[0-9a-f]{7})$/

module Resque
  def expand_payload(payload)
    payload[:created_at] ||= Time.now
    payload[:created_at] = (payload[:created_at].utc.to_f * 1000).round unless payload[:created_at].is_a?(Integer)

    if actor_ip = payload[:actor_ip]
      GitHub.dogstats.time "audit", tags: ["action:ip_lookup"] do
        location = GitHub::Location.look_up(actor_ip)
        location[:location] = {
          lat: location.delete(:latitude).to_f,
          lon: location.delete(:longitude).to_f,
        }

        payload[:actor_location] = location
      end
    end

    payload
  end

  def push_with_meta(queue, item)
    if item.respond_to?(:[]=)
      item[:meta] = {queued_at: Time.now.to_f}
      item[:meta][:context] = expand_payload(GitHub.context.to_hash)
      item[:meta][:audit_context] = expand_payload(Audit.context.to_hash)

      span = GitHub.tracer.last_span
      carrier = {}
      GitHub.tracer.inject(span.context, OpenTracing::FORMAT_TEXT_MAP, carrier) unless span.nil?
      item[:meta][:opentracing_carrier] = carrier unless carrier.empty?
    end
    push_without_meta(queue, item)
  end

  alias push_without_meta push
  alias push push_with_meta

  # In lab we want to prefix the queue name so only lab workers will pick up the jobs.
  #
  # A small monkey patch to add the prefix if set.
  def enqueue_to_with_prefix(queue, klass, *args)
    unless queue.match(FS_QUEUE_REGEX)
      queue = "#{GitHub.resque_queue_prefix}#{queue}".to_sym
    end
    enqueue_to_without_prefix(queue, klass, *args)
  end
  alias_method :enqueue_to_without_prefix, :enqueue_to
  alias_method :enqueue_to, :enqueue_to_with_prefix

  def encode(object)
    # Warn to Haystack if we're trying to encode a payload
    # with invalid UTF8
    encoded = GitHub::JSON.encode(object, warn_utf8: true)

    # Instruments encoded payload sizes by job class. Assumes that the job
    # payload is in one of two formats.
    #
    # 1. ActiveJob format e.g.
    #
    #    {
    #      :class => "ActiveJob::QueueAdapters::ResqueAdapter::JobWrapper",
    #      :args => [{
    #         "job_class" => "BillingUpdateExchangeRatesJob",
    #         "queue_name" => "billing"
    #         "job_id" => "abc"
    #         ...
    #      }]
    #    }
    # 2. Legacy job format e.g.
    #
    #    {
    #      :class => "GitHub::Jobs::CalculateTrendingRepos",
    #      :args => [..]
    #    }
    #
    # There are no guarantees about what `object` actually contains, so we have
    # to defensively type check. This code is brittle and sensitive to changes
    # in other parts of the codebase, and would be better as native resque or
    # ActiveJob instrumentation. However, we only need these metrics to plan
    # for the resque to aqueduct migration, so this code should be short-lived
    # and should only cease to report metrics if the structure of `object`
    # changes.
    begin
      return encoded unless (object.is_a?(Hash) && object.key?(:class) && object[:args].is_a?(Array))

      job_class = case object[:class]
      when ActiveJob::QueueAdapters::ResqueAdapter::JobWrapper.name
        object[:args].first["job_class"] if object[:args].first.is_a?(Hash)
      else
        object[:class]
      end

      if job_class
        GitHub.dogstats.histogram("job.encoded_size", encoded.bytesize, {
          tags: ["job:#{job_class.to_s.underscore}"],
        })
      end
    rescue => e
      # Never interfere with job encoding
      Failbot.report(e)
    end

    encoded
  end

  class Job
    # Returns a Hash of the meta data related to this Job.
    def metadata
      @metadata ||= payload["meta"] || {}
    end

    # Returns the Time that this Job was queued, or nil.
    def queued_at
      @queued_at ||= metadata.key?("queued_at") ? Time.at(metadata["queued_at"]) : nil
    end
  end

  class Worker
    # Override hostname for worker identification.
    #
    # Resque uses a colon-separated string with hostname:pid:queue,list to
    # identify workers, and this overrides the information-only hostname to
    # include a worker's role. This role is stored here to let other tools
    # (gh-resque and amen) retrieve information about resque workers by role
    # rather than just by queue or job.
    def hostname
      @hostname ||= "#{Socket.gethostname}/#{GitHub.role}"
    end

    # Override the worker pruning code to ignore the role in hostname.
    #
    # In case the role changes (e.g. enterprise went from "enterprise" to
    # "enterpriseworker" in 89378744afb3), we'll end up with permanently
    # orphaned worker registrations.
    def prune_dead_workers
      all_workers = Worker.all
      known_workers = worker_pids unless all_workers.empty?
      this_hostname = hostname.split("/").first # <-- ignore this worker's role
      all_workers.each do |worker|
        host, pid = worker.id.split(":")
        host = host.split("/").first # <-- ignore other workers' roles
        next unless host == this_hostname
        next if known_workers.include?(pid)
        log! "Pruning dead worker: #{worker}"
        worker.unregister_worker
      end
    end

  end
end
