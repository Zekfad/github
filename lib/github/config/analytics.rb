# frozen_string_literal: true

require "octolytics"
require "octolytics/adapters/noop"

module GitHub
  module Config
    module Analytics
      # Configuration methods bound to the global `GitHub` module
      module GlobalConfigs
        def analytics
          @analytics ||= ReportingWrapper.new(GitHub::Config::Analytics.unwrapped_analytics)
        end

        # Public: A queue for publishing analytics events to Octolytics.
        # The queues toggle into synchronous mode or asynchronous mode. Async
        # mode is the default when the application is serving requests and sync
        # mode is the default in all other environments (e.g. background jobs).
        #
        # The queue expects enqueued events to conform to the
        # `ActiveSupport::Notifcations` block parameter structure. That is:
        #
        #   [event_name (String), start (Time), end (unused), txn_in (unused), payload (Hash)]
        #
        # config/instrumentation/octolytics.rb maps `AS:N` events to Octolytics
        # events and is the primary `analytics_queue` user.
        #
        # Example:
        #
        #   GitHub.analytics_queue << ['my-event', Time.now, nil, nil, {attribute: "value"}]
        #
        #
        # See docs/octolytics.md for more information about the analytics queue.
        #
        # Array of buffered events to be sent in batches
        def analytics_queue
          @analytics_queue ||= SyncAnalyticsQueue.new
        end

        # Public: Enable a synchronous analytics queue. The sync queue publishes
        # events to Octolytics immediately after they're enqueued.
        #
        # See docs/octolytics.md for more information about the analytics queue.
        #
        # Returns a SyncAnalyticsQueue
        def enable_sync_analytics_queue
          return @analytics_queue if @analytics_queue.is_a?(SyncAnalyticsQueue)

          @analytics_queue = SyncAnalyticsQueue.new
        end

        # Public: Enable an asynchronous analytics queue. The async queue
        # buffers events in memory and must be flushed explicitly. Upon flush,
        # event payloads are pushed to a background job and delivered by Resque.
        #
        # Example:
        #
        #   GitHub.enable_async_analytics_queue
        #   GitHub.analytics_queue << ['my-event', Time.now, nil, nil, {attribute: "value"}]
        #
        #   # Later on ...
        #   GitHub.analytics_queue.flush
        #
        # See docs/octolytics.md for more information about the analytics queue.
        #
        # Returns an AsyncAnalyticsQueue
        def enable_async_analytics_queue
          return @analytics_queue if @analytics_queue.is_a?(AsyncAnalyticsQueue)

          @analytics_queue = AsyncAnalyticsQueue.new
        end

        # Public: Enable an asynchronous analytics queue during execution of the
        # given block. After the block is executed, the analytics queue is
        # flushed and returned to its original (sync/async) state. When sending
        # a potentially large number of events at once, this method may improve
        # performance.
        #
        # The logic was taken from GitHub::FlushOctolyticsEventsMiddleware.
        #
        # Examples
        #
        #   GitHub.with_async_analytics do
        #     comments.each do |comment|
        #       GitHub.analytics_queue << ["my-comment-event", Time.now, nil, nil, { comment_id: comment.id }]
        #     end
        #   end
        #
        # Returns the result of the given block.
        def with_async_analytics
          raise ArgumentError unless block_given?

          original_analytics_queue = @analytics_queue
          enable_async_analytics_queue

          begin
            yield
          ensure
            begin
              analytics_queue.flush if GitHub.octolytics_enabled?
            rescue => error
              Failbot.report(error)
            ensure
              # Always clear the array so a publishing bug doesn't cause the queue
              # to grow without bound.
              analytics_queue.clear
            end

            @analytics_queue = original_analytics_queue
          end
        end
      end

      # Interal: A raw Octolytics client. It's preferable to use
      # `GitHub.analytics` for publishing events, which wraps the client and
      # swallows exceptions.
      #
      # Returns an Octolytics::Client
      def self.unwrapped_analytics
        app_id = GitHub.octolytics_app_id
        options = {
          secret:  GitHub.octolytics_secret,
          reporter_options: {
            environment: GitHub.octolytics_reporter_env,
            timeout: 5,
          },
          collector_options: {
            host: GitHub.octolytics_collector_internal_host,
            timeout: 15,
          },
        }

        if noop_octolytics_collector?
          app_id = "github"

          options.deep_merge!({
            secret: "NONE",
            collector_options: {
              host: "localhost",
              adapter: Octolytics::Adapters::Noop.new,
            },
          })
        end

        @unwrapped_analytics ||= Octolytics::Client.new(app_id, options)
      end

      # Public: In some environments, we should stop short of making POST
      # requests to the collector service.
      #
      # Returns a Boolean
      def self.noop_octolytics_collector?
        GitHub.octolytics_collector_internal_host.blank?
      end

      # An analytics queue implementation that publishes events to the
      # Octolytics collector synchronously as events are enqueued.
      class SyncAnalyticsQueue
        # Publish: Push an event into the queue. The queue expects an
        # `ActiveSupport::Notifcations` block parameters array as an argument.
        #
        # Returns the SyncAnalyticsQueue so that `<<` calls can be chained.
        def <<(event)
          Jobs::PublishOctolyticsEvents.perform({"events" => [event].as_json})
          self
        end

        def flush
          # no op
        end

        def clear
          # no op
        end

        def size
          0
        end
      end

      # An analytics queue implementation that buffers events in memory. After
      # buffering, events must be explicitly flushed (typically at the end of
      # a request).
      class AsyncAnalyticsQueue
        include Enumerable

        delegate :each, to: :buffer

        # Publish: Push an event into the queue. The queue expects an
        # `ActiveSupport::Notifcation` block parameters array as an argument.
        #
        # Returns the SyncAnalyticsQueue so that `<<` calls can be chained.
        def <<(event)
          buffer << event
          self
        end

        # Publish: Flush events to resque for eventual delivery to the
        # Octolytics collector.
        def flush
          return if buffer.empty?

          GitHub.instrument("octolytics.flush_queue") do
            PublishOctolyticsEventsJob.perform_later({events: buffer})
          end
        end

        def clear
          buffer.clear
        end

        def size
          buffer.size
        end

        def buffer
          @buffer ||= []
        end
      end

      class ReportingWrapper
        extend Forwardable

        def_delegators :@client,
          :record!,
          :counts,
          :content,
          :referrers,
          :referrer_paths,
          :generate_actor_hash

        def initialize(client)
          @client = client
        end

        def record(*args)
          @client.record!(*args)
        rescue Octolytics::Error => err
          Failbot.report(err)
        end
      end

    end
  end

  extend Config::Analytics::GlobalConfigs
end
