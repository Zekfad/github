# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Config
    module Render
      # Hide render diffs after this amount
      def max_render_diffs_per_page
        @max_render_diffs_per_page ||= 25
      end
      attr_writer :max_render_diffs_per_page

      # The raw domain that render should use
      def render_raw_host_name
        GitHub.urls.raw_host_name
      end

      # The hostname of the render server
      #
      # Returns the URL String "render.githubusercontent."[com|localhost]
      #         or the set hostname of the render server. `host_name`
      #         in `enterprise? == true`
      def render_host
        @render_host ||= if subdomain_isolation?
          "render.#{GitHub.user_content_host_name}"
        else
          host_name
        end
      end
      attr_writer :render_host

      # The render site URL
      #
      # Returns the URL string "https://render.github.com" and friends
      def render_host_url
        @render_host_url ||= if subdomain_isolation?
          "http#{ssl? ? 's' : ''}://#{render_host}".freeze
        else
          "http#{ssl? ? 's' : ''}://#{render_host}/render".freeze
        end
      end
      attr_writer :render_host_url

      def render_lab_host
        @render_lab_host ||= "render-lab.#{GitHub.user_content_host_name}"
      end
      attr_writer :render_lab_host

      def render_lab_host_url
        @render_lab_host_url ||= "https://#{render_lab_host}"
      end
      attr_writer :render_lab_host_url

      # Figure out the render origin URL given an Origin header.
      #
      # origin - String Origin header value. request.headers['HTTP_ORIGIN']
      #
      # Returns the URL that should be used in the Access-Control-Allow-Origin
      # response header.
      def render_url_for_origin(origin)
        if origin && URI.parse(origin).host == render_lab_host
          render_lab_host_url
        else
          render_host_url
        end
      rescue URI::InvalidURIError
        render_host_url
      end

      # A switch to disable or enable using the render service
      # across the site.
      #
      # Returns Boolean
      def render_enabled?
        true
      end
      attr_reader :render_enabled

      # A list of types that render should limit itself to supporting.
      # An empty list indicates no limit.
      #
      # Returns an Array of Strings
      def render_type_filter
        @render_type_filter ||= []
      end

      # Takes a list of filters, or a string separated by ;
      # and fills in the render_type_filter
      #
      # Returns an Array of strings
      def render_type_filter=(filter)
        if filter.is_a? Array
          @render_type_filter = filter
        else
          @render_type_filter = filter.to_s.split(";")
        end
      end
    end
  end

  extend Config::Render
end
