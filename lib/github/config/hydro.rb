# frozen_string_literal: true

require "hydro"
require "hydro/datadog_reporter"
require "resolv"

module GitHub
  module Config
    module HydroConfig
      MAX_SHUTDOWN_TIME = 10

      DEVELOPMENT_BROKER = "127.0.0.1:9092"

      SRV_DOMAIN = "_kafka-potomac._tcp.service.iad.github.net"

      # For use as a guard with the hydro initializer.
      #
      # During a constrained boot (timerd, git hooks) the initializer is
      # required directly from `rock_queue.rb` since RockQueue needs hydro
      # publishing to be available and configured. However, during a normal full
      # boot, the initializer is loaded twice: once when required directly by
      # `rock_queue.rb`, and again via the regular rails initializer process.
      # This guard prevents the second load from trying to reload hydro schemas
      # or reinitialize the datadog reporting.
      attr_writer :hydro_initialized

      def hydro_initialized?
        @hydro_initialized
      end

      # Is publishing to Hydro enabled?
      #
      # It's not necessary or desirable to check this method before publishing to
      # Hydro. GitHub.hydro_publisher will always return a valid publisher that
      # will route messages to the correct place based on the environment.
      def hydro_enabled?
        return false if GitHub.environment.fetch("GH_HYDRO_UNAVAILABLE", 0) != 0
        true
      end

      # HMAC key for authenticating hydro browser payloads.
      attr_accessor :hydro_browser_payload_secret

      def hydro_client
        HydroLoader.load_github if GitHub.lazy_load_hydro?
        @hydro_client ||= Hydro::Client.new(environment: Rails.env)
      end

      def hydro_sink_initialized?
        defined?(@hydro_publisher) || defined?(@sync_hydro_publisher)
      end

      def hydro_site
        return "unknown" if GitHub.enterprise?
        return "localhost" unless Rails.env.production?
        GitHub.server_site || local_host_name
      end

      # A async hydro publisher that publishes messages in a background thread.
      # Messages are buffered and flushed in batches at 2 second intervals.
      def hydro_publisher
        return @hydro_publisher if defined?(@hydro_publisher)

        GitHub.dogstats.increment("hydro_client.create_publisher", tags: ["publisher:async"])

        @hydro_publisher = hydro_client.publisher(
          sink: hydro_sink,
          site: hydro_site,
          encoder: hydro_encoder,
        )
      end

      # The underlying sink to which encoded Hydro messages will be written
      def hydro_sink(env = Rails.env)
        @manual_hydro_flushing_enabled = true

        if enable_kafka_sink?(env)
          # The Hydro::KafkaSink automatically flushes every 2 seconds. We want
          # to avoid manually flushing because each flush will force a produce
          # operation and diminish throughput. If we service 5 requests in
          # 2 seconds, we'd like to batch all events into a single produce operation
          # rather than 5 individual produce operations.
          # This config is disabled in development mode so that we flush
          # buffered messages between web requests.
          @manual_hydro_flushing_enabled = false unless env == "development"

          max_buffer_size = GitHub.environment.fetch("HYDRO_MAX_BUFFER_SIZE", 1000).to_i
          max_queue_size = GitHub.environment.fetch("HYDRO_MAX_QUEUE_SIZE", 5000).to_i

          kafka_sink = hydro_kafka_sink(env, {
            producer_options: {
              retry_backoff: 1,
              delivery_interval: 2,
              delivery_threshold: 1000,
              max_buffer_size: max_buffer_size,
              max_queue_size: max_queue_size,
              idempotent: hydro_idempotent_publishing?,
            },
          })

          if env == "development"
            # for easy debugging of what's getting published:
            Hydro::Sink.tee(kafka_sink, hydro_log_sink)
          else
            kafka_sink
          end
        elsif env == "test"
          memory_sink = Hydro::MemorySink.new
          Hydro::Sink.tee(memory_sink, hydro_log_sink)
        else
          Hydro::NoopSink.new
        end
      end

      # A wrapper around the normal async hydro publisher that immediately flushes
      # messages on publish. This publisher should be used sparingly because it
      # doesn't batch as effectively as `GitHub.hydro_publisher` and generates
      # more kafka produce requests.
      def low_latency_hydro_publisher
        return @low_latency_hydro_publisher if defined?(@low_latency_hydro_publisher)

        GitHub.dogstats.increment("hydro_client.create_publisher", tags: ["publisher:low_latency"])

        @low_latency_hydro_publisher = LowLatencyHydroPublisher.new(hydro_publisher)
      end

      class LowLatencyHydroPublisher
        extend Forwardable

        def_delegators :publisher,
          :sink,
          :batch,
          :start_batch,
          :flush_batch,
          :close,
          :encode

        attr_reader :publisher

        def initialize(publisher)
          @publisher = publisher
        end

        def publish(message, args)
          batching = @publisher.sink.batching?
          result = @publisher.publish(message, **args)
          @publisher.flush_batch
          @publisher.start_batch if batching
          result
        end
      end

      def sync_hydro_publisher
        return @sync_hydro_publisher if defined?(@sync_hydro_publisher)

        GitHub.dogstats.increment("hydro_client.create_publisher", tags: ["publisher:sync"])

        @sync_hydro_publisher = hydro_client.publisher(
          sink: sync_hydro_sink,
          site: hydro_site,
          encoder: hydro_encoder,
        )
      end

      def sync_hydro_sink(env = Rails.env)
        if enable_kafka_sink?(env)
          idempotent = hydro_idempotent_publishing?
          kafka_sink = hydro_kafka_sink(env, {
            async: false,
            client_options: {
              connect_timeout: 5,
              socket_timeout: 5,
            },
            producer_options: {
              required_acks: idempotent ? :all : 1,
              max_retries: 0,
              ack_timeout: 1,
              idempotent: idempotent,
            },
          })

          if env == "development"
            Hydro::Sink.tee(kafka_sink, hydro_log_sink)
          else
            kafka_sink
          end
        else
          GitHub.hydro_sink(env)
        end
      end

      def enable_kafka_sink?(env = Rails.env)
        (env == "production" && hydro_enabled?) || env == "development"
      end

      def hydro_encoder
        # Report schemas that populate `nil` for scalar fields.
        nil_scalar_handler = ->(error) do
          schema = error.schema.name.gsub(/:+/, ".").downcase.sub("hydro.schemas.", "")
          tags = [
            "schema:#{schema}",
            "field:#{error.field.name}",
          ]
          GitHub.dogstats.increment("hydro_client.nil_scalar", tags: tags)
        end

        Hydro::ProtobufEncoder.new(
          Hydro::Site.new(hydro_site),
          nil_scalar_handler: nil_scalar_handler,
        )
      end

      def hydro_metrics_namespace
        "github-#{Rails.env}"
      end

      def hydro_idempotent_publishing?
        ["true", "1"].include? GitHub.environment["HYDRO_IDEMPOTENT_PUBLISHING"]
      end

      def hydro_producer_ssl_enabled?
        return true if GitHub.dynamic_lab?
        return false if GitHub.enterprise?

        GitHub.flipper[:hydro_ssl_publish].enabled?
      end

      def hydro_kafka_sink(env = Rails.env, options = {})
        use_ssl = hydro_producer_ssl_enabled? && hydro_cert_valid?
        if env == "production"
          seed_brokers = self.hydro_seed_brokers(ssl: use_ssl)
          if seed_brokers.none?
            Failbot.report(ArgumentError.new("GitHub.environment['HYDRO_KAFKA_BROKERS'] not set!"))
            return Hydro::NoopSink.new
          end

          if use_ssl
            options.deep_merge!({ client_options: hydro_kafka_ssl_options })
          end
        else
          seed_brokers = GitHub.environment.fetch("HYDRO_KAFKA_BROKERS", DEVELOPMENT_BROKER).split(",")
        end

        kafka = Hydro::KafkaSink.new(
          **{
            seed_brokers: seed_brokers,
            client_id: "#{hydro_metrics_namespace}:#{local_host_name}",
            logger: Rails.logger,
          }.merge(options),
        )

        Hydro::CircuitBreakingSink.new(kafka)
      end

      def hydro_log_sink
        logger = ::Logger.new(Rails.root.join("log/hydro-#{Rails.env}.log"))
        formatter = ->(message) {
          if message.data
            decoded = Hydro::Decoding::ProtobufDecoder.decode(message.data)
          end

          [
            "\e[36m[#{message.timestamp || Time.now}]\e[0m",
            "\e[35m#{message.schema}\e[0m",
            "\e[33m#{message.data&.bytesize} bytes\e[0m",
            "\e[37m#{decoded&.to_h.to_json}\e[0m",
          ].join(" ")
        }

        Hydro::LogSink.new(logger, formatter: formatter)
      end

      def hydro_consumer(options = {}, publisher = hydro_publisher, env = Rails.env)
        logger = options.with_indifferent_access[:logger]
        client_id = "#{hydro_metrics_namespace}:#{options[:group_id]}:#{local_host_name}"
        if logger.nil?
          logger = Logger.new(STDOUT)
          logger.level = Logger::INFO
          logger.formatter = proc do |severity, datetime, progname, msg|
            "severity=#{severity} time=#{datetime.iso8601} host=#{local_host_name} client_id=#{client_id} consumer_group=#{options[:group_id]} msg=#{msg.inspect}\n"
          end
          options[:logger] = logger
        end

        source = case env
        when "test"
          Hydro::MemorySource.new(**{ sink: publisher.sink }.merge(options))
        when "development", "production"
          hydro_kafka_source(options, env)
        end

        hydro_client.consumer(source: source, logger: logger)
      end

      def hydro_consumer_ssl_enabled?
        return false if GitHub.enterprise?

        GitHub.flipper[:hydro_ssl_consume].enabled?
      end

      def hydro_kafka_source(options, env)
        use_ssl = hydro_consumer_ssl_enabled? && hydro_cert_valid?
        if env == "production"
          seed_brokers = self.hydro_seed_brokers(ssl: use_ssl)
          if seed_brokers.none?
            raise ArgumentError.new("GitHub.environment['HYDRO_KAFKA_BROKERS'] not set!")
          end

          if use_ssl
            options.deep_merge!(hydro_kafka_ssl_options)
          end
        else
          seed_brokers = [DEVELOPMENT_BROKER]
        end

        Hydro::KafkaSource.new(**{
          seed_brokers: seed_brokers,
          client_id: "#{hydro_metrics_namespace}:#{options[:group_id]}:#{local_host_name}",
          logger: Rails.logger,
          socket_timeout: 35, # 5 more than the default session timeout
        }.merge(options))
      end

      def flush_hydro_batches?
        !!@manual_hydro_flushing_enabled
      end

      def hydro_buffered_message_topic_counts(sink)
        sink = sink.sink if sink.is_a?(Hydro::CircuitBreakingSink)

        return {} unless sink.is_a?(Hydro::KafkaSink)

        # Truly hideous, but there is no public API for this info :/
        # Collects pending message counts from the sync kafka client.
        sync_producer = sink.send(:producer)
          .instance_variable_get("@worker")
          .instance_variable_get("@producer")

        pending = sync_producer.instance_variable_get("@pending_message_queue")
          .instance_variable_get("@messages")
          .group_by(&:topic)
          .transform_values(&:size)

        queue = sink.send(:producer).instance_variable_get("@queue")
        operations = []
        queued = queue.size.times.reduce(Hash.new { |h, k| h[k] = 0 }) do |acc, _|
          begin
            operation, args = queue.pop(true)

            if operation == :produce
              acc[args.last[:topic]] += 1
            end

            operations << operation

            acc
          rescue ThreadError
            break acc
          end
        end

        worker_status = sink.send(:producer).instance_variable_get("@worker_thread")&.status

        {
          queued: queued,
          pending: pending,
          buffer_size: sync_producer.buffer_size,
          buffer_bytesize: sync_producer.buffer_bytesize,
          worker_status: worker_status,
          operation_count: operations.group_by(&:to_sym).map { |k, v| [k, v.count] }.to_h,
          operations: operations.take(100),
        }
      rescue NoMethodError => e
        { "error" => e.message }
      end

      # Flushes buffered hydro messages before exiting. Blocks for up to
      # `MAX_SHUTDOWN_TIME` seconds before timing out. Under the hood, hydro-client
      # flushes messages buffered in the kafka client to kafka.
      def close_hydro(timeout:)
        return unless hydro_sink_initialized?

        Timeout.timeout(timeout) do
          GitHub.dogstats.time("hydro_client.kafka.shutdown") do
            GitHub.hydro_publisher.sink.close
          end
        end
      rescue Timeout::Error
        dropped = {
          hydro_publisher: hydro_buffered_message_topic_counts(GitHub.hydro_publisher.sink),
        }

        Failbot.report(Hydro::DirtyExit.new, {
          app: "github-hydro",
          dropped: dropped,
        })
      end

      def hydro_seed_brokers(ssl: true)
        brokers = begin
            HydroConfig.lookup_hydro_seed_brokers
          rescue => e
            Failbot.report(e)
            []
          end

        # Fall back to potomac boot brokers if we can't load brokers from DNS.
        unless brokers.many?
          brokers = GitHub.environment["HYDRO_KAFKA_BROKERS"].to_s.split(",")
        end

        port = (ssl && Rails.env.production?) ? 9093 : 9092
        brokers.map { |broker| broker.split(":").first + ":#{port}" }.shuffle
      end

      def self.lookup_hydro_seed_brokers
        return [] unless Rails.env.production? && !GitHub.enterprise?

        Resolv::DNS
          .open { |dns| dns.getresources(SRV_DOMAIN, Resolv::DNS::Resource::IN::SRV) }
          .map(&:target).map(&:to_s)
      end

      def hydro_kafka_ssl_options
        return {} unless Rails.env.production?

        { ssl_ca_cert: File.read(self.hydro_ssl_cert_filename) }
      end

      def hydro_cert_valid?
        if File.exist?(self.hydro_ssl_cert_filename)
          cert = OpenSSL::X509::Certificate.new(File.read(self.hydro_ssl_cert_filename))
          Date.today < cert.not_after
        end
      rescue OpenSSL::X509::CertificateError => e
        Failbot.report(e)
        false
      end

      def hydro_ssl_cert_filename
        GitHub.environment.fetch("GH_HYDRO_SSL_CA_CERTS", "/etc/ssl/certs/cp1-iad-production-1487801205-root.pem")
      end
    end
  end

  extend Config::HydroConfig

  at_exit do
    GitHub.close_hydro(timeout: GitHub::Config::HydroConfig::MAX_SHUTDOWN_TIME)
  end
end
