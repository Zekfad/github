# rubocop:disable Style/FrozenStringLiteralComment

require "horcrux/entity"
require "horcrux/multiple"
require "horcrux/serializers/gzip_serializer"
require "horcrux/serializers/message_pack_serializer"
require "stratocaster"

module GitHub
  module Config
    module Services
      # Public: Temporarily disables notification/event services for a block.
      #
      # service - Optional Stratocaster::Service instance.
      #
      # Returns nothing.
      def importing(service = GitHub.stratocaster)
        original_value = !!@importing
        service ||= GitHub.stratocaster
        @importing = true
        service.disable

        yield
      ensure
        unless original_value
          @importing = false
          service.enable
        end
      end

      # Public: Checks to see if Notifications should be sent.
      #
      # Returns a Boolean.
      def send_notifications?
        !@importing && !!@send_notifications
      end

      # Public: Checks to see if an import is in progress.
      #
      # Returns a Boolean.
      def importing?
        !!@importing
      end

      # Public: Checks to see if Events should be triggered.
      #
      # service - Optional Stratocaster::Service instance.
      #
      # Returns a Boolean.
      def trigger_events?(service = GitHub.stratocaster)
        service.enabled?
      end

      def timed_resque
        @timed_resque ||= TimedResque.new
      end

      # Public: Setup the application wide audit service
      #
      # Use the ES/Syslog logger in both DotCom and Enterprise
      if Rails.production?
        def audit
          @audit ||= begin
            syslog = ::Audit::Syslog::Logger.new
            multi_logger =
              if GitHub.enterprise?
                loggers = [syslog]

                if GitHub.audit_log_es_logger_enabled?
                  if !GitHub.es_clusters.empty?
                    GitHub.es_clusters.keys.each do |cluster_name|
                      loggers.unshift(::Audit::Elastic::Logger.new(cluster: cluster_name))
                    end
                  else
                    loggers.unshift(::Audit::Elastic::Logger.new)
                  end
                end

                Rails.logger.info("Configured audit loggers: #{loggers.map { |l| l.class.name }}")
                ::Audit::Multi::Logger.new(*loggers)
              else
                splunklogger = ::Audit::SplunkHEC::Logger.new(
                  GitHub.splunk_hec_host,
                  GitHub.splunk_hec_token,
                  GitHub.splunk_hec_index_name
                )
                elastic = ::Audit::Elastic::Logger.new(cluster: GitHub.es_audit_log_cluster)
                ::Audit::Multi::Logger.new(elastic, syslog, splunklogger)
              end
            searcher = ::Audit::Elastic::Searcher.new

            service = ::Audit::Service.new(logger: multi_logger, searcher: searcher)
            service.statsd = GitHub.stats
            service.on_error do |exc|
              Failbot.report(exc, fatal: "NO")
            end

            ::Audit::Resque.new(service)
          end
        end
      elsif Rails.development? || Rails.staging?
        def audit
          @audit ||= begin
            splunklogger = ::Audit::SplunkHEC::Logger.new(
              GitHub.splunk_hec_host,
              GitHub.splunk_hec_token,
              GitHub.splunk_hec_index_name
            )
            elastic = ::Audit::Elastic::Logger.new
            multi_logger = ::Audit::Multi::Logger.new(elastic, splunklogger)
            searcher = ::Audit::Elastic::Searcher.new

            service = ::Audit::Service.new logger: multi_logger, searcher: searcher
            service.statsd = GitHub.stats
            service.on_error do |exc|
              Failbot.report(exc, fatal: "NO")
            end

            ::Audit::Resque.new(service)
          end
        end
      elsif Rails.env.test?
        def audit
          @audit ||= begin
            elastic = ::Audit::Test::Logger.new
            searcher = ::Audit::Test::Searcher.new

            service = ::Audit::Service.new logger: elastic, searcher: searcher
            service.on_error do |exc|
              p exc
            end

            service
          end
        end
      else
        def audit
          @audit ||= begin
            elastic = ::Audit::Elastic::Logger.new
            searcher = ::Audit::Elastic::Searcher.new

            service = ::Audit::Service.new logger: elastic, searcher: searcher
            service.on_error do |exc|
              p exc
            end

            service
          end
        end
      end

      def newsies
        @newsies ||= ::Newsies::Service.new
      end

      def stratocaster
        @stratocaster ||= build_stratocaster
      end

      def build_stratocaster(store = stratocaster_store, indexer = stratocaster_indexer)
        ::Stratocaster::Service.new(store, indexer)
      end

      def stratocaster_store
        @stratocaster_store ||= ::Stratocaster::Mysql2Store.new
      end

      def stratocaster_indexer
        @stratocaster_indexer ||= ::Stratocaster::Indexers::Proxy.default
      end

      attr_writer :stratocaster, :audit, :send_notifications, :newsies_mysql_web, :newsies

      if Rails.env.test?
        module StratocasterTestOverrides
          def stratocaster_store
            @stratocaster_store ||= ::Stratocaster::MemoryStore.new
          end

          def stratocaster_indexer
            @stratocaster_indexer ||= begin
              indexers = { org_all: ::Stratocaster::Indexers::MemoryIndexer.new(max: ::Stratocaster::OrgAllTimeline::INDEX_LENGTH),
                          other: ::Stratocaster::Indexers::MemoryIndexer.new }

              ::Stratocaster::Indexers::Proxy.new(indexers) do |partitions, timelines|
                partitions[:other] = timelines.except(Stratocaster::TimelineTypes::ORG_ALL)          # all except the org_all timeline (this includes activity_all as well)
                partitions[:org_all] = timelines.slice(Stratocaster::TimelineTypes::ORG_ALL)
              end
            end
          end

          def reset_stratocaster
            @stratocaster_store = @stratocaster_indexer = @stratocaster = nil
          end
        end

        prepend StratocasterTestOverrides
      end
    end
  end

  extend Config::Services
  self.send_notifications = true
end
