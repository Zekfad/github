# frozen_string_literal: true

module GitHub
  module Config
    module Logging

      module Destination
        STDOUT = "stdout"
        SYSLOG = "syslog"
      end

      module_function

      def destination
        if destination = ENV["GH_LOG_DESTINATION"]
          return destination
        end

        if Rails.production? && !GitHub.enterprise?
          return Destination::SYSLOG
        end

        # This implies a per-subsystem default, no overriding destination
        nil
      end

    end
  end
end
