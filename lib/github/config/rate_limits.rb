# frozen_string_literal: true

module GitHub
  module Config
    module RateLimits
      API_UNAUTHENTICATED_RATE_LIMIT          = 60      # per hour
      API_DEFAULT_RATE_LIMIT                  = 5000    # per hour
      API_ENTERPRISE_CLOUD_SOFT_RATE_LIMIT    = 15_000  # per hour
      API_ENTERPRISE_CLOUD_HARD_RATE_LIMIT    = 15_000  # per hour
      API_TIER_ONE_RATE_LIMIT                 = 12500   # per hour
      API_TIER_TWO_RATE_LIMIT                 = 62500   # per hour
      API_SEARCH_UNAUTHENTICATED_RATE_LIMIT   = 10      # per minute
      API_SEARCH_DEFAULT_RATE_LIMIT           = 30      # per minute
      API_SEARCH_ENTERPRISE_CLOUD_HARD_RATE_LIMIT = 100 # per minute
      API_LFS_UNAUTHENTICATED_RATE_LIMIT      = 100     # per minute
      API_LFS_DEFAULT_RATE_LIMIT              = 3000    # per minute
      API_RAW_UNAUTHENTICATED_RATE_LIMIT      = 3_000   # per minute
      API_RAW_FINGERPRINT_RATE_LIMIT          = 300_000 # per minute
      API_RAW_DEFAULT_RATE_LIMIT              = 150_000 # per minute
      API_ARCHIVE_UNAUTHENTICATED_RATE_LIMIT  = 1_000   # per minute
      API_ARCHIVE_FINGERPRINT_RATE_LIMIT      = 100_000 # per minute
      API_ARCHIVE_DEFAULT_RATE_LIMIT          = 20_000  # per minute
      API_INTEGRATION_MANIFEST_UNAUTHENTICATED_RATE_LIMIT = 5_000  # per hour
      API_GRAPHQL_UNAUTHENTICATED_RATE_LIMIT  = 0       # per hour
      API_GRAPHQL_DEFAULT_RATE_LIMIT          = 5_000   # per hour
      API_GRAPHQL_HIGHER_RATE_LIMIT           = 10_000  # per hour
      API_GRAPHQL_MUCH_HIGHER_RATE_LIMIT      = 50_000  # per hour
      API_GRAPHQL_ENTERPRISE_CLOUD_HARD_RATE_LIMIT = 10_000 # per hour
      # The default API rate limit for non authenticated requests.
      #
      # Returns the rate limit integer value. Defaults to 60/hr
      def api_unauthenticated_rate_limit
        @api_unauthenticated_rate_limit || API_UNAUTHENTICATED_RATE_LIMIT
      end
      attr_writer :api_unauthenticated_rate_limit

      # The default API rate limit for non white-listed apps.
      #
      # Returns the rate limit integer value. Defaults to 5000
      def api_default_rate_limit
        @api_default_rate_limit || API_DEFAULT_RATE_LIMIT
      end
      attr_writer :api_default_rate_limit

      def api_enterprise_cloud_soft_rate_limit
        @api_enterprise_cloud_soft_rate_limit || API_ENTERPRISE_CLOUD_SOFT_RATE_LIMIT
      end
      attr_writer :api_enterprise_cloud_soft_rate_limit

      def api_enterprise_cloud_hard_rate_limit
        @api_enterprise_cloud_hard_rate_limit || API_ENTERPRISE_CLOUD_HARD_RATE_LIMIT
      end
      attr_writer :api_enterprise_cloud_hard_rate_limit

      # Some Oauth apps get more API calls per hour. This is
      # the default value for apps at this first tier.
      #
      # Returns the rate limit integer value. Defaults to 300000/day
      def api_tier_one_rate_limit
        @api_tier_one_rate_limit || API_TIER_ONE_RATE_LIMIT
      end
      attr_writer :api_tier_one_rate_limit

      # Some Oauth apps get an obscene amount of API calls per hour. This is
      # the default value for apps at this top tier.
      #
      # Returns the rate limit integer value. Defaults to 1.5m/day
      def api_tier_two_rate_limit
        @api_tier_two_rate_limit || API_TIER_TWO_RATE_LIMIT
      end
      attr_writer :api_tier_two_rate_limit

      # The default Search API rate limit for authenticated requests.
      #
      # Returns the rate limit integer value.  Defaults to 30/min
      def api_search_default_rate_limit
        @api_search_default_rate_limit || API_SEARCH_DEFAULT_RATE_LIMIT
      end
      attr_writer :api_search_default_rate_limit

      def api_search_enterprise_cloud_hard_rate_limit
        @api_search_enterprise_cloud_hard_rate_limit || API_SEARCH_ENTERPRISE_CLOUD_HARD_RATE_LIMIT
      end
      attr_writer :api_search_enterprise_cloud_hard_rate_limit

      # The default Search API rate limit for non authenticated requests.
      #
      # Returns the rate limit integer value.  Defaults to 10/minute
      def api_search_unauthenticated_rate_limit
        @api_search_unauthenticated_rate_limit || API_SEARCH_UNAUTHENTICATED_RATE_LIMIT
      end
      attr_writer :api_search_unauthenticated_rate_limit

      # The default LFS API rate limit for non authenticated requests.
      #
      # Returns the rate limit integer value.  Defaults to 100/minute
      def api_lfs_unauthenticated_rate_limit
        @api_lfs_unauthenticated_rate_limit || API_LFS_UNAUTHENTICATED_RATE_LIMIT
      end
      attr_writer :api_lfs_unauthenticated_rate_limit

      # The default LFS API rate limit for authenticated requests.
      #
      # Returns the rate limit integer value.  Defaults to 300/minute
      def api_lfs_default_rate_limit
        @api_lfs_default_rate_limit || API_LFS_DEFAULT_RATE_LIMIT
      end
      attr_writer :api_lfs_default_rate_limit

      # The default raw API rate limit for non authenticated requests.
      #
      # Returns the rate limit integer value.
      def api_raw_unauthenticated_rate_limit
        @api_raw_unauthenticated_rate_limit || API_RAW_UNAUTHENTICATED_RATE_LIMIT
      end
      attr_writer :api_raw_unauthenticated_rate_limit

      # The default raw API rate limit for authenticated requests.
      #
      # Returns the rate limit integer value.
      def api_raw_default_rate_limit
        @api_raw_default_rate_limit || API_RAW_DEFAULT_RATE_LIMIT
      end
      attr_writer :api_raw_default_rate_limit

      # The default raw API rate limit for fingerprinted requests.
      #
      # Returns the rate limit integer value.
      def api_raw_fingerprint_rate_limit
        @api_raw_fingerprint_rate_limit || API_RAW_FINGERPRINT_RATE_LIMIT
      end
      attr_writer :api_raw_fingerprint_rate_limit

      # Returns the rate limit integer value.
      def api_archive_unauthenticated_rate_limit
        @api_archive_unauthenticated_rate_limit || API_ARCHIVE_UNAUTHENTICATED_RATE_LIMIT
      end
      attr_writer :api_archive_unauthenticated_rate_limit

      # The default archive API rate limit for authenticated requests.
      #
      # Returns the rate limit integer value.
      def api_archive_default_rate_limit
        @api_archive_default_rate_limit || API_ARCHIVE_DEFAULT_RATE_LIMIT
      end
      attr_writer :api_archive_default_rate_limit

      # The default app manifest API rate limit for authenticated requests.
      #
      # Returns the rate limit integer value.
      def api_integration_manifest_unauthenticated_rate_limit
        @api_integration_manifest_unauthenticated_rate_limit || API_INTEGRATION_MANIFEST_UNAUTHENTICATED_RATE_LIMIT
      end
      attr_writer :api_integration_manifest_unauthenticated_rate_limit

      # The default archive API rate limit for fingerprinted requests.
      #
      # Returns the rate limit integer value.
      def api_archive_fingerprint_rate_limit
        @api_archive_fingerprint_rate_limit || API_ARCHIVE_FINGERPRINT_RATE_LIMIT
      end
      attr_writer :api_archive_fingerprint_rate_limit

      # The default GraphQL API rate limit for non authenticated requests.
      #
      # Returns the rate limit integer value.  Defaults to 0/hour
      def api_graphql_unauthenticated_rate_limit
        @api_graphql_unauthenticated_rate_limit || API_GRAPHQL_UNAUTHENTICATED_RATE_LIMIT
      end
      attr_writer :api_graphql_unauthenticated_rate_limit

      # The default GraphQL API rate limit for authenticated requests.
      #
      # Returns the rate limit integer value.  Defaults to 5000/hour
      def api_graphql_default_rate_limit
        @api_graphql_default_rate_limit || API_GRAPHQL_DEFAULT_RATE_LIMIT
      end
      attr_writer :api_graphql_default_rate_limit

      # The higher GraphQL API rate limit for whitelisted clients.
      #
      # Returns the rate limit integer value.  Defaults to 10,000/hour
      def api_graphql_higher_rate_limit
        @api_graphql_higher_rate_limit || API_GRAPHQL_HIGHER_RATE_LIMIT
      end
      attr_writer :api_graphql_higher_rate_limit

      # The much higher GraphQL API rate limit for whitelisted clients that are GitHub staff.
      #
      # Returns the rate limit integer value.  Defaults to 50,000/hour
      def api_graphql_much_higher_rate_limit
        @api_graphql_much_higher_rate_limit || API_GRAPHQL_MUCH_HIGHER_RATE_LIMIT
      end
      attr_writer :api_graphql_much_higher_rate_limit

      def api_graphql_enterprise_cloud_hard_rate_limit
        @api_graphql_enterprise_cloud_hard_rate_limit || API_GRAPHQL_ENTERPRISE_CLOUD_HARD_RATE_LIMIT
      end
      attr_writer :api_graphql_enterprise_cloud_hard_rate_limit
    end
  end

  extend Config::RateLimits
end
