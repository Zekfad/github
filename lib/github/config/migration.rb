# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Config
    module Migration
      # Three hours, or 60 seconds * 60 minutes * 3.
      MAX_GH_MIGRATOR_TIME = 60 * 60 * 3

      # Public: True if gh-migrator support is available.
      def migration_api_available?
        !enterprise?
      end

      # Public: A migration coordinator.
      def migrator
        @migrator ||= GitHub::MigrationCoordinator.new(migrator: gh_migrator_proxy)
      end
      attr_writer :migrator

      # Internal.
      def gh_migrator_proxy
        GitHub::MigratorProxy.new
      end

      # Public: The maximum allowable time for gh-migrator exports to run.
      def max_gh_migrator_export_time
        @max_gh_migrator_export_time ||= MAX_GH_MIGRATOR_TIME
      end
      attr_writer :max_gh_migrator_export_time

      # Public: The maximum allowable time for gh-migrator imports to run.
      def max_gh_migrator_import_time
        @max_gh_migrator_import_time ||= MAX_GH_MIGRATOR_TIME
      end
      attr_writer :max_gh_migrator_import_time

      def migration_file_base_path
        "migration"
      end
    end
  end

  extend Config::Migration
end
