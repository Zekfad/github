# rubocop:disable Style/FrozenStringLiteralComment

# Default environment config. This file is required by all of the other
# environment configs and is useful for setting up global defaults. This
# is sourced in the enterprise.rb environment file too, so please make
# sure you take that into account when you update this file.

GitHub.elasticsearch_host = "#{ENV.fetch('GH_ELASTICSEARCH_HOST', 'localhost')}:#{ENV['GH_ELASTICSEARCH_PORT'] || 9200}"
GitHub.signup_enabled = true
GitHub.realtime_backups_enabled = false
GitHub.profiling_enabled = true
GitHub.stats_ui_enabled = true
GitHub.request_limiting_enabled = false

# Statsd
GitHub.stats_hosts = ["127.0.0.1:8127"]

GitHub.enterprise_web_url = ENV["ENTERPRISE_WEB_URL"]
GitHub.enterprise_web_url ||= if Rails.production?
  "https://enterprise.github.com"
else
  "https://enterprise.github.localhost"
end

GitHub.enterprise_web_admin_url = ENV["ENTERPRISE_WEB_ADMIN_URL"]
GitHub.enterprise_web_admin_url ||= if Rails.production?
  "https://enterprise-admin.githubapp.com"
else
  "https://enterprise.github.localhost"
end

GitHub.enterprise_web_hmac = ENV["ENTERPRISE_WEB_HMAC"]
GitHub.enterprise_web_hmac ||= "d9de584042bab368ba1ccd654fad95c5" if Rails.env.development? || Rails.env.test?

GitHub.treelights_url = GitHub.environment.fetch("TREELIGHTS_URL", "http://treelights.localhost:8002")

GitHub.aleph_url = GitHub.environment.fetch("ALEPH_URL", "http://aleph.localhost:8003")
GitHub.aleph_api_hmac_key = ENV["ALEPH_API_HMAC_KEY"]

GitHub.aqueduct_url = GitHub.environment.fetch("AQUEDUCT_URL", "http://localhost:8085/twirp")

GitHub.turboscan_url = GitHub.environment.fetch("TURBOSCAN_URL", "http://localhost:8888/twirp")
GitHub.turboscan_s3_bucket = GitHub.environment.fetch("TURBOSCAN_S3_BUCKET", "github-turbo-scan-dev")

GitHub.zendesk_api_url = ENV["ZENDESK_API_URL"]
GitHub.zendesk_api_token = ENV["ZENDESK_API_TOKEN"]

# These IDs must match what's configured in Zendesk.
# They're used to create support tickets by way of
# SiteController#send_contact.
GitHub.zendesk_brand_id = "400594"
GitHub.zendesk_fields = {
  browser:            "360016114072",
  business_plus:      "360016114092",
  ip_address:         "360016163471",
  javascript_enabled: "360016114132",
  os:                 "360016163651",
  location:           "360016163691",
  login:              "360016114192",
  metadata:           "360016163711",
  plan:               "360016114212",
  referring_article:  "360016114232",
  referring_url:      "360016114252",
  user_spammy:        "360016114272",
  user_suspended:     "360016114292",
}

GitHub.apple_iap_shared_secret = GitHub.environment.fetch("APPLE_IAP_SHARED_SECRET", "")

GitHub.google_fcm_url = GitHub.environment.fetch("GOOGLE_FCM_URL", "")
GitHub.google_fcm_server_key = GitHub.environment.fetch("GOOGLE_FCM_SERVER_KEY", "")
GitHub.google_fcm_private_key = GitHub.environment.fetch("GOOGLE_FCM_PRIVATE_KEY", "")

GitHub.policy_service_url = GitHub.environment.fetch("POLICY_SERVICE_URL", "http://localhost:5000")
GitHub.policy_service_connection_timeout = [1, GitHub.environment.fetch("POLICY_SERVICE_CONNECTION_TIMEOUT", "5").to_i].max
GitHub.policy_service_response_timeout = [1, GitHub.environment.fetch("POLICY_SERVICE_RESPONSE_TIMEOUT", "15").to_i].max

# Codespaces
GitHub.codespaces_vscs_environment = GitHub::Config::VSCS_ENVIRONMENTS[:production]

GitHub.codespaces_app_key = "Iv1.ad5f73e593b03f40"
GitHub.codespaces_app_ppe_key = "Iv1.b8a6cd24769ffe8c"
GitHub.codespaces_app_dev_key = "Iv1.faf5804e51bc0d8f"

GitHub.codespaces_per_user_limit = 2

GitHub.dependabot_url = GitHub.environment.fetch("DEPENDABOT_INTERNAL_URL", "")
GitHub.dependabot_hmac_key = GitHub.environment.fetch("DEPENDABOT_HMAC_KEY", "")

# Package Registry Metadata
GitHub.package_registry_metadata_url = GitHub.environment.fetch("PACKAGE_REGISTRY_METADATA_URL", "http://registry.github.localhost:8000")
GitHub.package_registry_metadata_hmac_key = GitHub.environment.fetch("PACKAGE_REGISTRY_METADATA_HMAC_KEY", "packageregistrymetadatahmac")

# Scanitizer
GitHub.scanitizer_url = GitHub.environment.fetch("SCANITIZER_URL", "")
GitHub.scanitizer_hmac_key = GitHub.environment.fetch("SCANITIZER_HMAC_KEY", "")

# Token scanning service
GitHub.token_scanning_url = GitHub.environment.fetch("TOKEN_SCANNING_URL", "http://localhost:5000/twirp")
