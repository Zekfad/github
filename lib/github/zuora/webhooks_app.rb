# frozen_string_literal: true

module GitHub::Zuora
  class WebhooksApp
    HEADERS = { "Content-Type" => "text/plain" }
    EMPTY_RESPONSE = []

    def self.app
      Rack::Builder.new do
        use Rack::Auth::Basic do |username, password|
          correct_username = Rack::Utils.secure_compare(username, GitHub.zuora_webhook_username)

          old_password_correct = GitHub.zuora_webhook_password.present? &&
            Rack::Utils.secure_compare(password, GitHub.zuora_webhook_password)

          new_password_correct = GitHub.zuora_webhook_new_password.present? &&
            Rack::Utils.secure_compare(password, GitHub.zuora_webhook_new_password)

          if old_password_correct && (GitHub.zuora_webhook_new_password.present? && !new_password_correct)
            GitHub.dogstats.increment("zuora.webhook.old_password_used")
          end

          correct_password = old_password_correct || new_password_correct

          correct_username && correct_password
        end

        run GitHub::Zuora::WebhooksApp
      end
    end

    def self.call(env)
      new.call(env)
    end

    def call(env)
      request = Rack::Request.new(env)
      if request.post?
        WebhookHandler.new(request).process
        [200, HEADERS, EMPTY_RESPONSE]
      else
        [404, HEADERS, EMPTY_RESPONSE]
      end
    end
  end
end
