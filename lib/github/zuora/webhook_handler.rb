# frozen_string_literal: true

# This class has two purposeses
# 1. Log all Zuora webhook requests
# 2. Queue jobs to process requests
# Rack::Request get routed here from Github::Zuora::WebhooksApp
module GitHub::Zuora
  class WebhookHandler
    attr_reader :category, :responsibility, :params, :request

    def initialize(request)
      @request = request
      @params = JSON.parse(request.body.read)
      @category = @params.delete("event_category")&.underscore&.parameterize(separator: "_")
      @responsibility = Billing::ZuoraWebhook.responsibility(category)
    end

    def process
      log_request
      return if category.nil?
      return if ignore_webhook?

      zuora_webhook = ::Billing::ZuoraWebhook.create!(
        kind: category,
        status: :pending,
        account_id: params["AccountId"],
        payload: params,
      )

      ZuoraWebhookJob.perform_later(zuora_webhook)
    end

    private

    def log_request
      GitHub.dogstats.increment("zuora.webhook", tags: ["category:#{category}", "responsibility:#{responsibility}"])
    end

    def ignore_webhook?
      category == "invoice_posted" && params["Amount"] != "0.00"
    end
  end
end
