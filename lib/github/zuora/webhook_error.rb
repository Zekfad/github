# frozen_string_literal: true

module GitHub::Zuora
  class WebhookError < Error
  end
end
