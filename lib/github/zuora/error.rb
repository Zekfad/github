# frozen_string_literal: true

module GitHub::Zuora
  class Error < StandardError
  end
end
