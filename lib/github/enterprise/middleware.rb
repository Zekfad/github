# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Enterprise
    module Middleware
      def self.mount(builder)
        # Allow session sharing with render while in Private mode
        builder.use DualSession, "render.session",
          key: "_gh_render",
          path: "/",
          expire_after: (3 * 365 * 24 * 60 * 60), # seconds
          secret: GitHub.session_secret,
          secure: GitHub.ssl?

        # TamperGuard periodically reloads the enterprise license. This is to prevent any
        # injected code that might monkey patch GitHub.license to bypass seat limits or
        # expiration dates.
        # builder.use TamperGuard,
        #   :backoff  => 8.hours

        # The customer may use an alias for easier access (e.g. http://github/ vs.
        # http://github.foocompany.com/). We need to redirect them to the hostname
        # they configured enterprise to use.
        builder.use EnsureHostname, %w[ 127.0.0.1 localhost ]
      end

      # Allows multiple session cookies to be consumed and coexist in the Rack
      # environment. The DualSession middleware takes the name of a Rack environment
      # key and session options. For instance, we're using it to enable a special
      # render cookie:
      #
      #   use Rack::DualSession, 'render.session',
      #     :key          => '_gh_render',
      #     :path         => '/',
      #     :expire_after => (3 * 365 * 24 * 60 * 60), # seconds
      #     :secret       => GitHub.session_secret
      #
      class DualSession
        def initialize(app, key, options = {})
          @app = app
          @key = key
          @options = options
          @session = Rack::Session::Cookie.new(method(:proxy_call), options)
        end

        def proxy_call(env)
          before_app(env)
          res = @app.call(env)
          after_app(env)
          res
        end

        def before_app(env)
          env[@key] = env.delete("rack.session")
          env["#{@key}.options"] = env.delete("rack.session.options")
        end

        def after_app(env)
          env["rack.session"] = env.delete(@key)
          env["rack.session.options"] = env.delete("#{@key}.options")
        end

        def call(env)
          @session.call(env)
        end
      end

      class TamperGuard
        def initialize(app, options = {})
          @app = app

          @backoff    = options[:backoff]
          @reload_at  = options[:reload_at] || DateTime.now
        end

        def call(env)
          reload_license if DateTime.now > @reload_at

          @app.call(env)
        end

        def reload_license
          Enterprise.license.reload!

          @reload_at = DateTime.now + @backoff
        end
      end

      class EnsureHostname
        def initialize(app, whitelist = [])
          whitelist += [GitHub.host_name, "api.#{GitHub.host_name}", "gist.#{GitHub.host_name}"]
          if GitHub.subdomain_isolation?
            whitelist += ["raw.#{GitHub.host_name}", "pages.#{GitHub.host_name}"]
          end
          @app, @whitelist = app, whitelist
        end

        def call(env)
          request = Rack::Request.new(env)
          # Allow /status checks here for external load balancers
          if @whitelist.include?(env["SERVER_NAME"]) || request.path == "/status"
            @app.call(env)
          else
            url = request.url.sub(env["SERVER_NAME"], GitHub.host_name)
            response = Rack::Response.new
            # Avoid browser redirect caching in local development so we can
            # easily switch between dotcom and enterprise.
            # https://github.com/github/github/pull/49510
            status = Rails.env.development? ? "302" : "301"
            response.redirect(url, status)
            [response.status, response.headers, response.body]
          end
        end
      end
    end
  end
end
