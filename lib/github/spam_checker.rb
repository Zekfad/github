# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  # This module does very basic spam checking.
  module SpamChecker
    include Scientist

    extend self

    # We wait 10 seconds to give async spam checks time to finish https://github.com/github/notifications/issues/296
    DELAY_FOR_EXTERNAL_CHECKS = 10.seconds
    HARD_SPAM_FLAG_PHRASE = "[octocat approved]"
    HARD_SPAM_FLAG_REGEXP = /#{Regexp.escape(HARD_SPAM_FLAG_PHRASE)}/

    # Are external spamminess checks enabled, i.e. should we add the DELAY_FOR_EXTERNAL_CHECKS
    # These are only enabled in production on dotcom
    def external_spamminess_check_enabled?
      Rails.production? &&
        GitHub.spamminess_check_enabled? &&
        !GitHub.dynamic_lab? &&
        !GitHub.staff_host?
    end

    def add_to_message_with_limit(message, addition, total_length = 255)
      addition_length = addition.length + 2
      [message[0..(total_length - addition_length)], addition].join(" ")
    end

    def make_hard_reason(reason)
      add_to_message_with_limit(reason,
                                GitHub::SpamChecker::HARD_SPAM_FLAG_PHRASE, 255)
    end

    WORD_BLACKLIST = [
      /BangBus/i, /Megavideo/i, /getsexon/i, /locksmith/i, /lolita/i, /preteen/i,
      /solahart/i, /(order|buy)\s+(\w+)?\s*online/i, /Download eBook/i,
      /click link above/i, /Streaming Online Video/i, /watch streaming online/i,
      /payday loan/i, /milf(?!o)/i
    ]

    # Constructs a blacklist regexp that will match any of the given words or
    # regexps, ignoring case. Regexp#union doesn't accept flags, so we have to
    # pass its source into Regexp#new with the flags.
    BLACKLIST_REGEXP = Regexp.new(Regexp.union(*WORD_BLACKLIST).source, Regexp::IGNORECASE)

    SPAM_NOTIFICATIONS_ROOM_ID = 567520
    THE_SPAM_ROOM_ID           = 545284

    # Test if a given string contains any blacklisted words.
    #
    # text - String to test
    #
    # Returns Boolean indicating if the String matches the blacklist regexp.
    def blacklisted?(text)
      text =~ BLACKLIST_REGEXP
    end

    # Send this msg to The Spam Notifications Room
    def notify(message)
      return unless GitHub.spamminess_check_enabled?

      if Rails.development? || Rails.test?
        Rails.logger.info("Chat - Spam Notifications: #{message}")
      else
        Spam.notify(message: message)
      end
    end

    def activity_count(user)
      {
        followers: user.followers.count,
        following: user.following.count,
        gists: user.gists.count,
        gist_comments: user.gist_comments.count,
        issues: user.issues.without_pull_requests.count,
        issue_comments: user.issue_comments.count,
        pull_requests: user.pull_requests.count,
        repos: user.repositories.not_forks.count,
        commit_comments: user.commit_comments.count,
        wiki_edits: wiki_edits(user).count,
        stars: user.starred_public_repositories_count,
      }
    end

    def wiki_edits(user)
      return [] unless user.is_a?(User) || (user = User.find_by_login(user))
      event_selector = "actor:#{user.id}:public"
      events = Stratocaster::Timeline.new(event_selector).events
      events.select { |t| t.event_type == Stratocaster::Event::GOLLUM_EVENT }.select { |t| wiki_edit?(t) }
    end

    def wiki_edit?(event)
      return false unless event.respond_to? :payload
      payload = event.payload

      payload["pages"].present? &&
        ["edited", "created"].include?(payload["pages"][0]["action"])
    end

    # Returns true for some moderately arbitrary combinations of activity.
    # For instance, we don't count the number of Gists or GistComments at
    # all, because spammers do zillions of them. But at the moment spammers
    # aren't often meeting these particular criteria.
    #
    # This is quite gamable, but for the moment it's a useful signal to
    # avoid flagging active users as fake or automated spam accounts.
    def fairly_active?(user)
      # Recently created hyperactive accounts don't qualify
      return false unless user.created_at < 24.hours.ago

      num_followers = user.followers.limit(9).count
      num_issues = user.issues.without_pull_requests.limit(7).count
      num_issue_comments = user.issue_comments.limit(7).count
      num_repos = user.repositories.where(parent_id: nil).limit(7).count
      num_commit_comments = user.commit_comments.limit(5).count

      result   = user.has_app_enabled?
      result ||= num_repos > 2 && (num_followers + num_issues + num_commit_comments) > 4
      result ||= (num_repos > 3 || num_followers > 8)
      result ||= (num_issue_comments + num_commit_comments) > 4
      result ||= (num_issues + num_issue_comments + num_repos) > 6
      result
    end

    def old_or_active?(user)
      user.created_at < 30.days.ago || user.hammy? || has_paid_plan?(user) || fairly_active?(user)
    end

    # Public: Does this account give us a positive number of money?
    # See `paid_subscription?` for the logic of how we calculate this.
    #
    # account - An account.
    #
    # Returns a boolean.
    def has_paid_plan?(account)
      org_subscriptions = account.organizations.first(10).map(&:subscription)
      subscriptions = org_subscriptions + [account.subscription]
      subscriptions.any? { |subscription| paid_subscription?(account, subscription) }
    end

    # Public: Does this subscription result in GitHub getting money?
    # This checks to see if the plan is not free or a trial and the amount they give us
    # is greater than any discounts we give them.
    #
    # account - An account.
    # subscription - An account subscription.
    #
    # Returns a boolean.
    def paid_subscription?(account, subscription)
      return false unless subscription
      return false if subscription.plan&.name&.include?("free")
      return false if ::Billing::PlanTrial.active_for?(account, subscription.plan.name)
      return true if subscription.undiscounted_price.to_i > subscription.discount.to_i
      return false
    end

    # Show what entries in word_list match in the given text.  We do this
    # word-by-word (instead of using BLACKLIST_REGEXP, above) so that we
    # can get a list of the individual elements that matched. Only used
    # in spam_checker_test.rb.
    #
    # word_list = Array of words or regexen
    # text      = String to test
    #
    # Returns an Array of all the word_list entries that cause a match.
    def get_matching_words(word_list, text)
      matching_words = []
      word_list.each do |word|
        rexp = Regexp.new(Regexp.union(word).source, Regexp::IGNORECASE)
        matching_words << word if text =~ rexp
      end
      matching_words
    end

    def get_blacklisted_words(text)
      get_matching_words(WORD_BLACKLIST, text)
    end

    # Check the given text in two ways to see if it smells like spam.
    # This allows easy movement from troublesome keywords in the
    # blacklist to spammy phrases (in spammy_text) so we get fewer
    # false positives.
    #
    # text - the String to test
    #
    # returns true if the text is spammy, false if not.
    def check_text(text)
      blacklisted?(text) || spammy_text(text).present?
    end

    # Only looking for low-hanging fruit here the first time out, namely
    # comment bodies that primarily consist of many duplications of the same
    # URL, each on a line by itself.
    def has_duplicate_links?(text, minimum = 2)
      return false unless text && (text.is_a?(String) || text.is_a?(Enumerable))

      text = text.lines if text.is_a?(String)

      link_lines = text.grep(/\A.*https?:\/\//).map(&:chomp)

      link_counts = link_lines.inject(Hash.new(0)) { |h, k| h[k] += 1; h }
      link_counts.any? { |_, v| v > minimum }
    end

    # Check a comment (Gist, Commit, Issue, PullRequestReview, etc) to see if
    # it's spammy.
    #
    # comment - the Comment to test
    #
    # Returns the String reason if the Comment is spammy, or nil otherwise.
    def test_comment(comment)
      return nil unless user = comment.user   # how is this User nil?

      # This method is used by some active filtering, so let's bail out early
      # if the user is already spammy or whitelisted.
      return user.spammy_reason if user.spammy?
      return nil unless user.can_be_flagged?

      # We have both GistComments and IssueComments with nil bodies in the DB.
      return nil if comment.body.nil?

      # Don't flag a user for comments on a repo to which he has rights.
      if repo = comment.try(:repository)
        return nil if repo.private? && repo.member?(user)
      end

      reasons = []
      comment_id_str = comment.id.to_s

      specimen = Spam::Specimen.new(comment.body)
      just_text = cleanup_text(specimen.text)

      reasons.concat gist_comment_tests(comment, specimen) if comment.respond_to? :gist

      if specimen.has_repeated_links?(DUPLICATE_LINK_THRESHOLD)
        reasons << "comment has too many links to the same URL #{DUPLICATE_LINK_THRESHOLD}"
      end

      if reasons.present?
        "#{comment.class} spam (id #{comment.id}): #{reasons.join(', ')}"
      else
        nil    # Yeah, I know the expression is nil anyway, but clarity ftw.
      end
    end

    # This method is called only by test_comment, as it performs checks
    # specific to GistComments, but not *all* the checks performed on
    # all Comments (including GistComments).
    def gist_comment_tests(comment, specimen = nil)
      reasons = []
      return reasons unless comment.respond_to? :gist  # Shouldn't happen

      gist = comment.gist
      seconds_since_parent_created = (comment.created_at || Time.now) - gist.created_at

      # If a user is posting a comment to a Gist within 5 seconds of
      # creating it, the whole thing was likely automated spam.
      if seconds_since_parent_created < 5.1
        reasons << "posting interval"
      end

      # Spammers like to post a single 'file' with the filename the same
      # as the contents of the spam file, then comment on their own Gist.
      if (gist.files_with_path.length == 1 &&
            gist.comments.all? { |gc| gc.user_id == gist.user_id }
          )

        blob, path = gist.files_with_path.first
        if reason = check_content_path_and_description(blob.data, path, gist.description)
          reasons << reason
        end

        if gist.comments.count == 1
          if specimen&.nothing_but_image_links?
            reasons << "user's first comment on his own gist was just an image link"
          end
        end
      end
      reasons
    end

    def check_content_path_and_description(content, path, description)
      content = cleanup_text(content)
      path = cleanup_text(path)
      description = cleanup_text(description)

      # Avoid false positives on things like "hello, world" and "test"
      if content.length > 20
        if path.length > 20 && content[0..path.length-1] == path
          return "single path name (#{summarize_gist_path(path)}) was same as data"
        end

        if (description.present? &&
             description.length > 20 &&
             content[0..description.length-1] == description
           )
          return "description (#{description}) was same as data"
        end
      end
    end

    GIST_MAX_PATH_LENGTH = 300
    def summarize_gist_path(path)
      filename = path[0..GIST_MAX_PATH_LENGTH]
      if path.length > GIST_MAX_PATH_LENGTH
        filename += "." * 3
      end
      filename
    end

    # Check an existing Gist to see if he smells like spam.
    #
    # gist - the Gist to test
    #
    # Returns a String containing info if the Gist is spammy, or nil otherwise.
    def test_gist(gist)
      url_info = gist.secret? ? "[secret]" : gist.url

      if gist.description
        if check_text(gist.description)
          return "Gist spam (description, id: #{gist.id}, url: #{url_info}, description: #{gist.description[0..300]})"
        end

        Spam.each_current_pattern("filename") do |regex|
          if regex =~ gist.description
            return "Gist spam (description, id: #{gist.id}, url: #{url_info}, pattern: #{regex}, description: #{gist.description[0..300]})"
          end
        end
      end

      if Spam::user_ip_is_blacklisted? gist.user
        return "Gist spam (user's IP of #{gist.user.last_ip} is blacklisted)"
      end

      test_gist_files(gist)
    end

    # This is slightly different than the PATH_SKIP_REGEXP used in Pages
    # repo examination, because we are going to skip a little different set
    # of file extensions.  The initial diff is that we're going to skip XML
    # files here, but not in Pages repo examination.
    #
    # Even for files that match these extensions, we'll still do filename
    # match checks, just not content checks.
    GIST_FILE_EXTS_TO_SKIP = %w[ apk bin css db dex docx gif gz ico iml ipynb
                                 jar java jpg jrxml mov mp3 ogg pdf png psd
                                 sql svg swf tgz xml yml zip
                               ]
    GIST_SKIPFILE_REGEXP = /\.(#{GIST_FILE_EXTS_TO_SKIP.join('|')})\z/ix

    GIST_MAX_FILE_DATA = 100 * 1024
    MAX_BLOB_SIZE_TO_CHECK = 50 * 1024

    def test_gist_files(gist)
      if gist.user
        interval = gist.created_at - gist.user.created_at
      else
        interval = -1
      end

      url_info = gist.secret? ? "[secret]" : gist.url

      begin
        reasons = []

        current_filename_regexps = Spam.get_current_regexps("filename")
        current_data_regexps = Spam.get_current_regexps("data")
        gist.files_with_path.each do |blob, path|
          if check_text(path)
            return "Gist spam (filename, id: #{gist.id}, url: #{url_info}, check_text(), path: #{summarize_gist_path(path)})"
          end

          current_filename_regexps.each do |regex|
            if regex =~ path
              return "Gist spam (filename, id: #{gist.id}, url: #{url_info}, current pattern: #{regex}, path: #{summarize_gist_path(path)})"
            end
          end

          next if path =~ GIST_SKIPFILE_REGEXP

          if blob.viewable?
            reasons.concat test_gist_blob_data(blob.data[0..GIST_MAX_FILE_DATA],
                                          interval: interval, gist: gist)

            if reasons.include? "only image link soon after acct creation"
              notify "Found nothing_but_image_links for Gist #{gist.id} #{url_info}, with interval #{interval}"
            elsif reasons.any? { |r| r =~ /obfuscated with tags/ }
              notify "Found spammy text obfuscated with tags in Gist #{gist.id}, #{url_info}"
            end

            if blob.data
              test_data = blob.data[0..GIST_MAX_FILE_DATA]
              current_data_regexps.each do |regex|
                if regex =~ test_data
                  notify "Found matching blob data for #{regex} in Gist #{gist.id}, #{url_info} / #{summarize_gist_path(path)}"
                  reasons << "blob data matched current pattern #{regex}"
                end
              end
            end

            if reasons.count > 0
              return "Gist spam (#{reasons.join(', ')}, id: #{gist.id}, url: #{url_info}, path: #{summarize_gist_path(path)})"
            end
          end

        end
      end

      nil
    end

    # Spammers like to post a mostly innocuous Gist, and then followup
    # with tons of spammy comments on their own Gist.
    def test_gist_owner_comments(gist)
      gist.comments.select { |gc| gc.user_id == gist.user_id }.detect { |gc| GitHub::SpamChecker.check_text(gc.body) }
    end

    # How long after a new User account we think it's normal for a gist
    # to be created on it.
    GIST_CREATE_SUSPICIOUS_INTERVAL = 7.minutes

    # Maximum number of links to the same URL in a single unit of content
    # before we get suspicious.
    DUPLICATE_LINK_THRESHOLD = 6

    def test_gist_blob_data(blob_data, params)
      gist        = params[:gist]
      gist_id_str = gist.id.to_s
      reasons     = []

      specimen = Spam::Specimen.new(blob_data)

      # Unidecode funky characters in the text
      clean_data = cleanup_text(blob_data)
      just_text  = cleanup_text(specimen.text)

      # Regular content pattern check
      if check_text(clean_data)
        reasons << "blob content"
      elsif check_text(just_text)
        reasons << "blob content (obfuscated with tags)"
      end

      if specimen.uses_spacing_tricks?
        reasons << "spacing tricks"
      end

      if specimen.has_repeated_links?(DUPLICATE_LINK_THRESHOLD)
        reasons << "excessive duplicate links to same URL (#{DUPLICATE_LINK_THRESHOLD})"
      end

      interval = params[:interval].to_i
      # If this gist was created too soon after its owner account's creation,
      # it's suspicious and we're going to check a little deeper.
      if interval > 0 && interval < GIST_CREATE_SUSPICIOUS_INTERVAL
        if specimen.nothing_but_image_links?
          reasons << "only image link soon after acct creation"
        end
      end

      reasons
    end

    PATH_EXTS_TO_SKIP = %w[ .apk .bin .css .db .dex .gif .gz .ico .iml .ipynb
                            .jar .java .jpg .mov .mp3 .ogg .pdf .png .psd
                            .sql .svg .swf .tgz .zip
                          ]

    # Verbose mode is for console use.
    def get_repo_filenames(repo, verbose = false)
      oid = repo.default_oid

      # Can't do much if the default oid is nil
      if oid.nil?
        puts "Master sha is nil for #{repo.name}. Skipping it." if verbose
        return []
      end

      # Skip files we don't care about or can't check anyway
      repo.tree_file_list(oid).reject { |f| f.end_with?(*PATH_EXTS_TO_SKIP) }
    end

    def quick_check_pages_repo(repo, verbose = false)
      if repo.owner.spammy?
        puts "Already spammy owner #{repo.name} #{repo.owner.login}: #{repo.owner.spammy_reason}" if verbose
        return 100
      end

      spam_score = 0
      report = ""

      gnarly_filenames = get_repo_filenames(repo, verbose)
      filenames = gnarly_filenames.map { |f| cleanup_text(f) }
      clean_to_gnarly = Hash[filenames.zip(gnarly_filenames)]

      # Again with the tasty console-use juice
      if verbose.is_a?(Integer) && verbose.to_i > 1
        puts "Checking #{filenames.count} filenames"
      end

      # ton of filenames? Almost all on one dir level? Spammer.
      file_count = filenames.count
      deep_count = filenames.count { |f| f =~ /\// }
      shallow_count = file_count - deep_count
      if (file_count > 100 && deep_count < 3) ||
         (file_count > 500 && deep_count < (0.01 * file_count))
         report << "SPAM! #{repo.name} by #{repo.owner.login} has #{file_count - deep_count} top-level files (out of #{file_count} total)"
         spam_score += 30
      end

      if ((file_count > 2000) && deep_count < (0.01 * file_count))
        spam_score += 10
        return spam_score
      end

      js_files = filenames.select { |f| f =~ /\.js\z/i }

      js_regexp = GitHub::SpamChecker::NAUGHTY_JS

      pages_pattern = nil
      # Is he using the naughty JS to track his Google power? Spammer.
      naughty_file = js_files.find do |f|
        begin
          blob = repo.tree_entry(repo.default_oid, clean_to_gnarly[f], limit: MAX_BLOB_SIZE_TO_CHECK)
          next unless blob

          naughty = js_regexp.any? do |expr|
            expr.all? { |e| blob.data =~ e }
          end

          naughty ||= Spam.get_current_patterns("pages_js").any? do |expr|
            if blob.data =~ Regexp.new(expr)
              pages_pattern = expr
              true
            end
          end
        rescue GitRPC::Failure
          next
        end
      end

      if naughty_file
        report << " and a spammy .js file: #{naughty_file}"
        report << " . Matched pattern: #{pages_pattern}"
        spam_score += 40
      end

      # Don't do the HTML checks if there are not at least 3 files in the repo,
      # or if the number of top-level files are less than 65% of the total file
      # count. Spammers put most of their files at the top level, and this
      # avoids walking through some *large* legitimate repos.
      if (file_count > 2) && shallow_count > (0.65 * file_count)
        html_files = filenames.select { |f| f =~ /\.html?\z/i }
        html_file_count = html_files.count

        text_naughty_count = 0

        naughty_html_files = html_files.select do |f|
          next unless blob = repo.blob(repo.default_oid, clean_to_gnarly[f])
          next if blob.data.nil?  # Usually means file was too large to retrieve

          text_says_spam  = check_text blob.data

          text_naughty_count  += 1 if text_says_spam

          text_says_spam
        end

        if html_file_count > 0
          text_naughty_ratio = (100.0 * text_naughty_count) / html_file_count
          if text_naughty_ratio >= 75.0
            report << " and %.01f%% (%d) of its %d HTML files look spammy" % [text_naughty_ratio, text_naughty_count, html_file_count]
            spam_score += 20
          end
        end
      end

      if spam_score < 25  # Arbitrary numbers FTW! TODO: Probably just == 0
        report << "NO NO NO! #{repo.name} by #{repo.owner.login} is INNOCENT, I tell you."
      end

      puts report if verbose
      spam_score
    end

    # Check a repo to see if it smells like spam
    #
    # repo - the Repository to test
    #
    # Returns a String containing a reason if the repo is spammy, nil otherwise
    def test_repo(repo)
      # Forkers aren't spammy, at least not yet
      return if repo.fork? || repo.private?

      reasons = []

      if repo.is_user_pages_repo?
        if quick_check_pages_repo(repo) > 35
          reasons << "Pages branch"
        end
      end

      # Test the text fields against the blacklist
      reasons << "Description" if spammy_text(repo.description).present?
      reasons << "Homepage" if check_text(repo.homepage)
      reasons << "Name" if spammy_text(repo.name).present?
      if reasons.present?
        "Repo spam (id: #{repo.id} -> #{repo.permalink}) : #{reasons.join(', ')}"
      end
    end

    # Returns a spamminess score from 0.0 (pure as the new-fallen snow) to
    # 1.0 (wickedest filth ever conceived).
    def score_repo_content(repo, verbose = false)
      return 1.0 if repo.owner.spammy?
      return 0.0 if repo.empty?

      if verbose
        puts "Checking repo #{repo.name} (#{repo.id}), belonging to #{repo.owner.login} (#{repo.owner.id})"
      end

      all_filenames = get_repo_filenames(repo, verbose)
      possible_offenders = []

      all_filenames.each do |filename|
        blob = repo.tree_entry(repo.default_oid, filename, limit: MAX_BLOB_SIZE_TO_CHECK)
        next unless blob

        if GitHub::SpamChecker.check_text(blob.data)
          possible_offenders << filename
          if verbose
            puts "\t" + "X"*50
            naughty = GitHub::SpamChecker.spammy_text(blob.data)
            puts "FOUND ONE by #{repo.owner.login}: #{filename}\n\t#{naughty}"
            puts "\t" + "X"*50
          end
        end
      end

      percent = 100.0 * possible_offenders.size / all_filenames.size
      if all_filenames.size > 4 && percent > 50.0
        if verbose
          puts "#{repo.name} is HIGHLY LIKELY SPAM (%.02f %%)" % percent
        end
        # We're pretty sure at this point, so give a high score
        return [percent, 0.9].max
      elsif percent > 0.1
        if all_filenames.size > 4
          # if we found something, and there were enough files to check
          # return the percent we found.
          return percent
        else
          # otherwise, return a fairly low, but non-zero, score
          return 0.1
        end
      else
        # Looks OK from what we know how to check right now.
        return 0.0
      end
    end

    # Check an existing Issue to see if it partakes of the spammy nature.
    #
    # issue - the Issue to test
    #
    # Returns a String containing info if the Issue is spammy, or nil otherwise.
    def test_issue(issue)
      return if issue.nil? || (issue.title.blank? && issue.body.blank?)
      reasons = []

      reasons << "title" if spammy_text(issue.title).present?
      reasons << "body" if spammy_text(issue.body).present?
      if reasons.present?
        "Issue spam (id: #{issue.id} -> #{issue.permalink}) : #{reasons.join(', ')}"
      end
    end

    # Check a User Profile to see if he smells like spam
    #
    # profile - the Profile to test
    #
    # Returns a String containing a reason if the profile is spammy, nil otherwise
    def test_profile(profile)
      # Nothing to test if the profile is missing
      return if profile.nil?

      reasons = []

      # Test the text fields against the blacklist
      reasons << "Profile blog spam" if spammy_text(profile.blog).present?
      reasons << "Profile name spam" if check_text(profile.name)
      reasons << "Profile company spam" if check_text(profile.company)
      reasons << "Profile location spam" if check_text(profile.location)

      if reasons.present?
        "Profile spam (#{reasons.join(', ')})"
      end
    end

    # Check a User to see if he smells like a spammer
    #
    # user - the user to test
    #
    # Returns a String containing a reason if the user is spammy, nil otherwise
    def test_user(user)
      reasons = []

      reasons << test_user_login(user)

      reasons.compact!

      if reasons.present?
        "User account spam (#{reasons.join(', ')})"
      end
    end

    # Examine other users at this user's IP address, and determine if
    # he's been hanging around the Wrong Sort, and if we disapprove.
    def test_user_associates(user)
      GitHub.dogstats.time("spam.test_user_associates.time") do
        cluster = Spam::find_cluster_for(user)

        if Spam::cluster_is_suspicious?(cluster)
          spammy_count = cluster.count { |u| u.spammy }
          total = cluster.size

          if Spam::cluster_notifications_enabled?
            notify("User '#{user.login}' (#{user.id}) has #{spammy_count} of #{total} spammy friends at #{user.last_ip}")
          end

          return "#{spammy_count} of #{total} recent users at IP #{user.last_ip} are spammy"
        end
      end
    end

    def get_email_parts(email)
      email = email.email if email.is_a? User

      return [] if email.nil?

      mailbox, domain = email.split("@")

      mailbox.strip! if mailbox
      domain.strip! if domain
      [mailbox, domain]
    end

    MIN_DOT_COUNT = 3
    MIN_DUPE_COUNT = 3
    LONG_MAILBOX_SIZE = 12

    # Test a User's email attribute (in practice, only one of the UserEmails
    # which might be associated with an account) for spamminess.
    #
    # Returns a String with the reasons we think it's spammy (if we do) or nil
    # (if we don't).
    def test_user_email(user)
      # A nil user can't be spammy
      return nil unless user.is_a? User
      return nil unless user.email_spamminess_checks_enabled?

      reasons = []

      # User's email matches >= MIN_DUPE_COUNT existing UserEmail records (with
      # or without the same last_ip).  Only check recently-created accounts so
      # that we don't keep causing trouble for old and acceptable bot or dupe
      # accounts that aren't spammy.
      if user.created_at && user.created_at > 1.hour.ago
        dupe_count = count_obfuscated_duplicate_emails(user)
        if dupe_count >= MIN_DUPE_COUNT
          reasons << "address #{user.email} had #{dupe_count} obfuscated duplicates"
        end
      end

      if reasons.present?
        "User email spam (#{reasons.join(', ')})"
      end
    end

    def count_obfuscated_duplicate_emails(user, options = nil)
      options ||= {}
      pattern = UserEmail.deobfuscate user.email
      base = options[:skip_spammy] ? UserEmail.not_spammy : UserEmail
      scope = base.where(deobfuscated_email: pattern).where("user_id <> ?", user.id)
      ActiveRecord::Base.connected_to(role: :reading) do
        scope.count
      end
    end

    def find_obfuscated_duplicate_emails(user, options = nil)
      return [] unless user.email.present?
      options ||= {}
      limit = (options[:limit] || 100).to_i  # Let's don't get crazy
      pattern = UserEmail.deobfuscate user.email
      base = options[:skip_spammy] ? UserEmail.not_spammy : UserEmail
      scope = base.where(deobfuscated_email: pattern).where("user_id <> ?", user.id).limit(limit)
      ActiveRecord::Base.connected_to(role: :reading) do
        scope.to_a
      end
    end

    def test_user_login(user)
      return if user.nil?

      if last_id = Spam.login_is_tainted?(user.login)
        audit_link = "https://github.com/stafftools/audit_log?query=%28user_id%3A#{last_id}+OR+actor_id%3A#{last_id}%29"
        notify ":postal_horn: Flagging #{user.login} as tainted; previous User id: #{last_id} (#{audit_link})"
        return "login was on tainted list; previous User id: #{last_id}"
      end

      return "login (general)" if spammy_text(user.login).present?

      email = user.email

      mailbox, domain = get_email_parts(email)

      # user.login == 'abcdefgh94qxy' and email == 'abcdefgh@hotmail.com'
      #                        ^^^^^ <- 5 characters tacked on
      # Seeing thousands of these
      if mailbox.present? && user.login =~ /\A#{Regexp.escape(mailbox)}/ &&
         (user.login.length == (mailbox.length + 5)) &&
         domain == "hotmail.com"  # only seeing this with hotmail right now
        return "Fake login/email (extended hotmail pattern)"
      end

      if (drug_match = drug_list_matches(user.login)).present?
        # We get so many false positives off these that we're going to try
        # queuing matches up for review instead of flagging them directly
        # here.
        notify "Pushing #{user.login} for review after match against [#{drug_match.join(', ')}]"
        GlobalInstrumenter.instrument(
          "add_account_to_spamurai_queue",
          {
            account_global_relay_id: user.global_relay_id,
            additional_context: "SpamChecker#test_user_login",
            queue_global_relay_id: SpamQueue::POSSIBLE_SPAMMER_QUEUE_GLOBAL_RELAY_ID,
            origin: :RESQUE_CHECK_FOR_SPAM_USER,
          },
        )
      end
      # If we got to here, all clear
      nil
    end

    # Check the content for a wiki and return a reason for its spamminess,
    # if it is.
    def test_wiki_content(content)
      if content =~ /(support (?:phone )?number[\s,].+){3,}/im ||
         content =~ /(technical support[\s,].+){3,}/im
        "Tech Support Wiki spam"
      elsif content =~ /solahart/im
        "Solahart Wiki spam"
      else
        Spam.get_current_patterns("wiki").each do |expr|
          if content =~ Regexp.new(expr)
            return "Content matched Wiki pattern #{expr}"
          end
        end
        # If nothing else matched, do regular content check
        GitHub::SpamChecker.check_text(content)
      end
    end

    # Flag a User who is attempting to put spam into a wiki.
    def flag_wiki_updater(page, user, repo, reason)
      if reason == "Tech Support Wiki spam"
        GlobalInstrumenter.instrument(
          "add_account_to_spamurai_queue",
          {
            account_global_relay_id: user.global_relay_id,
            additional_context: "SpamChecker#flag_wiki_updater - #{reason}",
            origin: :RESQUE_WIKI_PAGE_SPAM_CHECK,
            queue_global_relay_id: SpamQueue::POSSIBLE_SPAMMER_QUEUE_GLOBAL_RELAY_ID,
          },
        )
        user.safer_mark_as_spammy(reason: "#{reason} on #{repo.permalink}/wiki/#{page.to_param}")
        notify(":hammer:ed User '#{user.login}' for #{reason} on #{repo.permalink}/wiki/#{page.to_param}")
      elsif reason == "Solahart Wiki spam"
        user.safer_mark_as_spammy(reason: "#{reason} on #{repo.permalink}/wiki/#{page.to_param}")
        notify(":hammer:ed User '#{user.login}' for #{reason} on #{repo.permalink}/wiki/#{page.to_param}")
      elsif reason =~ /Content matched Wiki pattern/i
        user.safer_mark_as_spammy(reason: "#{reason} on #{repo.permalink}/wiki/#{page.to_param}")
        notify(":hammer:ed User '#{user.login}' because #{reason} on #{repo.permalink}/wiki/#{page.to_param}")
      elsif reason
        GlobalInstrumenter.instrument(
          "add_account_to_spamurai_queue",
          {
            account_global_relay_id: user.global_relay_id,
            additional_context: "SpamChecker#flag_wiki_updater - default",
            origin: :RESQUE_WIKI_PAGE_SPAM_CHECK,
            queue_global_relay_id: SpamQueue::POSSIBLE_SPAMMER_QUEUE_GLOBAL_RELAY_ID,
          },
        )
        notify("User '#{user.login}' is guilty of WIKI SPAM on #{repo.permalink}/wiki/#{page.to_param}: #{reason}")
      end
    end

    def test_wiki_page(page, user, repo)
      if reason = test_wiki_content(page.data_html.to_s)
        flag_wiki_updater page, user, repo, reason
        true
      end
    end

    # From here to the end has been ported to Hamzo in lib/pattern_helpers.rb
    # as of 2019-09-30. CAUTION: DO NOT ADD ANYTHING BELOW THIS LINE WITHOUT
    # ALSO ADDING TO HAMZO.
    def drug_list_matches(text)
      drug_match = DRUG_LIST.select do |drug_name|
        # eliminates 'alli' and any other short names likely to false-positive
        next if drug_name.length < 5

        text =~ /#{drug_name}/i
      end
    end

    # Better living through chemistry. But let's don't catch 'specialist',
    # 'ambient', etc., in our net.
    DRUG_LIST = %w[
                    abilify accutane ac[iy]clovir a[cd]iph?ex actos adderall
                    adipex alli alphagan alprazolam ambien(?!t) amlodipine
                    amoxicillin apcalis aricept ashwagandha ativan atrovent
                    azithromycin
                    benadryl benicar benemid blopress
                    capoten cardizem cardura carisoprodol caverta cefadroxil
                    celadrin celebrex celexa (?:[^e]|\b)cialis cipro clomid clonazepam
                    colospa codeine concerta cordarone coreg cymbalta
                    cyto(tec|xan)
                    darvocet deltasone desyrel diazepam diflucan digoxin
                    diovan ditropan doxycycline
                    effexor elavil etodolac evista
                    feldene fioricet flomax(tra)? forzest
                    glucophage
                    haldol hydrocodone kamagra klonopin
                    lamisil lasix levaquin levitra lexapro lipitor lipothin
                    lopressor lorazepam lunesta
                    medrol motrin myambutol
                    naprosyn nexium nolvad(ex|ine) noroxin norvasc novolog
                    oxcarbazepina oxycontin paxil percocet phenergan
                    phentermine phentramin plavix plendil ponstel prazosin
                    predniso(lo)?ne prevacid propecia provigil prozac
                    ritalin rivotril rosuvastatin roxithromycin rumalaya
                    serevent seroquel silagra sildenafil sinequan singulair
                    soma(tropin)? strattera suhagra suprax synthroid
                    tadalafil tada(cip|lis) tamoxifen tofranil topamax
                    triamterene tricor trileptal ultram
                    v-gel valium valtrex vardenafil viagra vicodin
                    wellbutrin xanax xenical
                    zanaflex zetia zithromax zocor zoloft zolpidem zyban
                    zyprexa
                  ]

    # PORN_WORDS_1 and PORN_WORDS_2 are split up because we're trying to
    # limit matches to phrasing that contains one from each group. Not
    # ideal, but it limits false positives and tends to still catch most
    # of the pervs at this time (Jan 2013).
    PORN_WORDS_1 = %w[ hentai illegal innocent kinder l[oi]litas? pedo
                       pedo[fph]+ilia pre[te]{2}en nymph(et|o)?s?
                       teens? underage
                     ]

    PORN_WORDS_2 = %w[ bbs cock cum exotic facials fetish incest japanese
                       models movie nude penis porno? pussy russian? sexy?
                       shocking ukraine
                     ]

    EVENT_WORDS = ["Indy", "MLB", "MMA", "NBA", "NFL", "NHL", "NHRA",
                    "National Basketball", "UFC", "WWE", "WWF", "baseball",
                    "college", "cricket", "episode", "football", "golf",
                    "hd [\s\b-]* tv", "hockey", "kick [\s\b-]* off",
                    "lacrosse", "movie", "olympics?",
                    "premier [\s\b-]* league", "racing", "rugby", "scandal",
                    "sex [\s\b-]* tape", "soccer", "sports", "supercross",
                    "tennis", 'tip [\s\b-]* off', "versus", "vs(?![\s]*code)",
                    "wrestling", "world [\s\b-]* cup"
                  ]

    NAUGHTY_JS = [
      # Catch the JS doing Google Analytics tracking for a very large number
      # of Pages spammer. More details here:
      #
      #   https://github.com/github/github/issues/6433#issuecomment-11480518
      #
      # Original regexp from which this is taken is here:
      #
      #   http://rubular.com/r/sIz8PXHsZ0
      [
        /
          var\s+addAsyncScript .*
          var\s+trackUserAction .*
          addAsyncScript.* (domain|dnsdynamic\.com) .* (count|ad)?\/github\/ .*
          var\s+_gaq\s+=\s+_gaq\s+\|\|\s+\[\]; .*
          _gaq\.push
        /imx,
      ],
      [
        /var .+ t.async .* gud .* aas .*
         \/github\/ .*
         var\s+_gaq\s+=\s+_gaq\s+ \| \| \s+\[\]; .*
         _gaq\.push .*
         (_setCustomVar.*github)
        /imx,
      ],
    ]

    # Turn this into a dummy function for now
    def turkify(input)
      input
    end

    # Inserts all the common variations on characters used by Turkish Gist
    # spammers to avoid matching.
    def old_turkify(input)
      output = input.downcase
      output.gsub!("a", "[ÀàÁáÂâÃãÄäÅåĀāĂăĄąǞǟǺǻạa]")
      output.gsub!("ae", "[ÆæǼǽ(ae)]")
      output.gsub!("b", "[ḂḆḇḃb]")
      output.gsub!("c", "[ĆćÇçČčĈĉĊċc]")
      output.gsub!("d", "[ḐḑĎďḊḋĐđÐðǱǳǄǆd]")
      output.gsub!("e", "[ÈèÉéĚěÊêËëĒēĔĕĘęĖėƷʒǮǯəẹểḕȅếe]")
      output.gsub!("f", "[Ḟḟƒﬀﬁﬂﬃﬄﬅf]")
      output.gsub!("g", "[ǴǵĢģǦǧĜĝĞğĠġǤǥg]")
      output.gsub!("h", "[ĤĥĦħh]")
      output.gsub!("i", "[ɨÌìÍíÎîĨĩÏïĪīĬĭĮįİıĲĳḮỉi]") # 2nd Ḯ isn't same as 1st
      output.gsub!("j", "[Ĵĵj]")
      output.gsub!("k", "[ḰḱĶķǨǩĸk]")
      output.gsub!("l", "[ĹĺĻļĽľĿŀŁłǇǉḻḽl]")
      output.gsub!("m", "[Ṁṁm]")
      output.gsub!("n", "[ŃńŅņŇňÑñŉŊŋǊǌṅṇn]")
      output.gsub!("o", "[ÒòÓóÔôÕõÖöŌōŎŏØøŐőǾǿŒœỖȍỡṑo]")
      output.gsub!("p", "[Ṗṗp]")
      output.gsub!("r", "[ŔŕŖŗŘřɼṙr]")
      output.gsub!("s", "[ŚśŞşŠšŜŝṠṡſṧs]")
      output.gsub!("t", "[ŢţŤťṪṫŦŧÞþt]")
      output.gsub!("u", "[ÙùÚúÛûŨũÜüŮůŪūŬŭŲųŰűu]")
      output.gsub!("w", "[ẀẁẂẃŴŵẄẅw]")
      output.gsub!("y", "[ỲỳÝýŶŷŸÿy]")
      output.gsub!("z", "[ŹźŽžŻżƶz]")
      output
    end

    # Build the regexp base for all known variations of these common
    # Turkish-language spammer words, for use in SPAMMY_PHRASES.
    TURKISH_WORDS = {
      "bayiligi"  => turkify("bayiligi"),             # reseller
      "belgesi"   => turkify("belges?(i|n){0,3}"),    # certificate/document
      "bolum"     => turkify("bolumu?"),              # section
      "canli"     => turkify("canlik?"),              # live/streaming
      "fatura"    => turkify("fatura"),               # bill
      "fragmani"  => turkify("fragman(i|n){0,3}"),    # trailer
      "full"      => turkify('tek\s*parca'),          # one piece
      "giris"     => turkify("giris(i|n){0,3}"),      # entry/with/using
      "golleri"   => turkify("gol(leri?)?"),          # goals
      "izle"      => turkify("iz(l|i)e(mek|sene)?"),  # watch
      "kanalda"   => turkify("kanalda"),              # channel
      "merkezi"   => turkify("merkezi?"),             # center
      "mac"       => turkify("mac(i|n){0,3}"),        # match
      "naklen"    => turkify("naklen"),               # live
      "noktasi"   => turkify("noktasi"),              # point
      "odeme"     => turkify("odeme"),                # payment
      "ozeti"     => turkify("ozeti?"),               # highlights/summary
      "seyret"    => turkify("seyret(i|n){0,2}"),     # watch
      "servis"    => turkify("servis(ler)?i?"),       # service
      "sinav"     => turkify("sinav(i|n){0,3}"),      # exam/test
      "sonuclari" => turkify("sonuc(u|lari?)"),       # results
      "tamiri"    => turkify("tamiri?"),              # repair
      "tikla"     => turkify("tikla+(yin(iz))?"),     # click here

    }

    def turkify_and_join(words)
      words.map { |w| TURKISH_WORDS[w] }.join("|")
    end

    # Combo patterns to compress regexp usage using tasty English words
    TURKISH_COMBO = {
      "certificate" => "(#{turkify_and_join(%w[belgesi])})",
      "exam"        => "(#{turkify_and_join(%w[sinav])})",
      "fragment"    => "(#{turkify_and_join(%w[fragmani bolum ozeti])})",
      "live"        => "(#{turkify_and_join(%w[canli naklen])})",
      "watch"       => "(#{turkify_and_join(%w[izle seyret])})",
    }

    SPAMMY_PHRASES = [

      [
        /\b(live|streaming|season|free|download)\b/imx,
        /\b( #{EVENT_WORDS.join('|')} ) \b /imx,
        /\b(watch|online|free|stream(ing)?|PPV|pay-per-view|click \s* here|internet \s* tv)\b/imx,
      ],

      # Turkish spam

      # "CLICK HERE" is tiklayiniz, but with multiple spelling variations,
      # including 'tiklaayin', 'tikla', 'tiklayiniz', etc.  Crazy language.
      [/\b #{TURKISH_WORDS['tikla']} \b/imx],

      # Spam links to multiple shorteners in case they get shut down
      [/ #{turkify('alternatif')} [\b\s-]* #{turkify('adres')} /imx],

      # Lotto results
      [/#{turkify('loto')}/imx,

        /#{TURKISH_WORDS['sonuclari']}/imx,
      ],

      # (Y/y)erinizi = book/location,
      # (G/g)iris == with/entry/using
      # belgisini == certification/license
      [/ #{TURKISH_COMBO['exam']} /imx,

        / #{TURKISH_WORDS['giris']} |
          #{turkify('yerinizi')}    |
          #{TURKISH_COMBO['certificate']}
        /imx,
      ],

      # Going to the trouble to use the İ here is pretty clear
      [/ WATCH [\b\s]* LİVE /imx],

      # "lots of song lyrics"
      [/#{turkify('giderli')} [\b\s]* #{turkify('sarkilar')} [\b\s]* #{turkify('sozleri')}/imx],

      # "exam results announced"
      [/ #{TURKISH_COMBO['exam']} /imx,
        / #{TURKISH_WORDS['sonuclari']} /imx,
        / #{turkify('aciklandi')} /imx,
      ],

      # "Watch episode|trailer|video|full film"
      [
        / ( #{TURKISH_COMBO['watch']} ) /imx,

        / ( full ([\w-]+[\b\s]+){0,3} film    |
            #{turkify('tek')} [\b\s]* #{turkify('parca')}  |
            HD                                |
            #{TURKISH_COMBO['fragment']}      |
            #{TURKISH_COMBO['live']}
          )
        /imx,
      ],

      # "Pine oil"
      [/ #{turkify('cam(lik)?')} [\b\s]* #{turkify('yagi')} /imx],

      # If it has "bill" and "payment" in Turkish, it's spam.
      [/ #{TURKISH_WORDS['fatura']} /imx,     # bill
        / #{TURKISH_WORDS['odeme']} /imx,      # payment
      ],

      # If it has "watch", "golleri" and "mac", it's soccer spam.
      [/ #{TURKISH_COMBO['watch']} /imx,      # watch
        / #{TURKISH_WORDS['golleri']} /imx,    # goals
        / #{TURKISH_WORDS['mac']} /imx,        # match
      ],

      # If it has "mac" and "live"|"channel", it's soccer spam.
      [/ #{TURKISH_COMBO['live']}          |
          #{TURKISH_WORDS['kanalda']}          # channel
        /imx,

        / #{TURKISH_WORDS['mac']} /imx,        # match
      ],

      [/ ( #{TURKISH_COMBO['watch']}       |  # watch
            #{TURKISH_WORDS['golleri']}     |  # goals
            #{turkify('video')}             |  # video
            #{turkify('karsilasmayi')}         # encounter
          )
        /imx,

        / #{TURKISH_WORDS['mac']}              # match
        /imx,

        / ( #{TURKISH_COMBO['live']}        |
            #{TURKISH_COMBO['fragment']}    |  # highlights
            #{TURKISH_WORDS['sonuclari']}      # results
          )
        /imx,

      ],

      # Seems like every other one is for Gatalasay or Fenerbahce
      [
        / ( #{turkify('gatalasay(in)?')} | #{turkify('galatasaray')} |
            #{turkify('fenerbahce')} | #{turkify('gaziantepspor')} |
            #{turkify('trabzonspor')} | #{turkify('besiktas')}
          )
        /imx,
        /
          (
            #{TURKISH_WORDS['mac']} | #{TURKISH_COMBO['live']} |
            #{TURKISH_COMBO['fragment']} | #{TURKISH_WORDS['sonuclari']} |
            #{TURKISH_WORDS['golleri']}
          )
        /imx,
      ],

      # Are there any soccer matches that aren't between these two teams?
      [
        / ( Manchester [\b\s]* United ) | ( Real [\b\s]* Madrid ) /imx,

        / #{TURKISH_COMBO['live']} | #{TURKISH_WORDS['mac']} /imx,
      ],

      # If it's got all three of these, it's some kind of service spam,
      # even without specifics like below.
      [
        / #{TURKISH_WORDS['servis']} /imx,   # service
        / #{TURKISH_WORDS['tamiri']} /imx,   # repair
        / #{turkify('teknik')} /imx,          # technical
      ],

      # Shuttle service / boiler repair / I have no idea spam
      [/ Baykan | #{turkify('hizmeti?')} | #{turkify('not:ozel')} /imx,
        / #{turkify('kombi')} /imx,
        / ( #{TURKISH_WORDS['servis']} | #{TURKISH_WORDS['tamiri']} |
            #{turkify('teknik')}
          )
        /imx,
      ],

      # Various kinds of "service" spam
      [/ #{turkify('acarkent')} | Altus | #{turkify('arcelik')} | Baymak |
          #{turkify('findikli')} | #{turkify('kavacik')} | Merkez | Samsung |
          #{turkify('vaillant')}
        /imx,
        / ( #{TURKISH_WORDS['servis']} | #{TURKISH_WORDS['tamiri']} |
            #{turkify('teknik')}
          )
        /imx,
      ],

      # "Online calculation Appreciation thank you" - some
      # kind of mortgage or interest calc spam.
      [/ #{turkify('online')} [\b\s]* #{turkify('takdir')} /imx,
        / #{turkify('tesekur')} /imx,
        / #{turkify('hesaplama')} /imx,
      ],

      # A rash of Turkish spam for "disinfection" systems
      [/\b #{turkify('Dezenfeksiyonu?')} \b/imx],

      # Welcome to our new Swedish spammers!
      [
        /\b Bingo [\b\s]* lottos? \b/imx,
        /\b uppesittarkv[aä]ll \b/imx,
      ],

      # Back to English spam

      [/click \s+ on/imx,
       /Download \s+ Now/imx,
       /Download \s+ ebook \s+ now!?/imx,
      ],

      [/live [\s-]* stream(ing)? \s* (((for \s* )? free) | online)/imx],

      [/live [\s-]* online \s* free \s* stream(ing)?/imx],

      [/free \s+ internet/imx],

      [/adult|bdsm|bondage/imx,
        /dating|sex|parties|personals/imx,
        /free|bdsm|dating/imx,
      ],

      # Better living through chemistry. But let's don't catch 'specialist',
      # 'ambient', etc., in our net.
      [
        /\b(#{ %w[ adipex ambien carisoprodol celebrex cialis evista fioricet
                   glucophage oxycontin viagra wellbutrin xanax zolpidem
               ].join('|') }
           )\b
        /imx,
      ],

      [/\bprescriptions? \s* online\b/imx],

      [/\b (order|buy) [\b\s]* (cheap|online) \b/imx,
        /\b ( #{ DRUG_LIST.join('|') } ) \b /imx,
      ],

      [/\bpokemon\b/imx,
        /\bdownload\b/imx,
        /\b(early|trial|free|rom)\b/imx,
      ],

      [/\b( #{PORN_WORDS_1.join('|')} )\b /imx,
        /\b( #{PORN_WORDS_2.join('|')} )\b /imx,
      ],

      # Here we're looking for combinations of the porn words as one word,
      # so "kindermodels" or "moviepedo"
      [/\b
         ( #{[PORN_WORDS_1, PORN_WORDS_2].flatten.join('|')} )
         ( #{[PORN_WORDS_1, PORN_WORDS_2].flatten.join('|')} )
         \b
        /imx,
      ],

      # These things are getting complicated. These regexps should match if
      # all the objects between word boundaries match the elements in order.
      # Perhaps an example is better.  For the regexp:
      #
      #  /(buy)[\b\s]+(toads|frogs)[\b\s]+(outside)/
      #
      # "buy toads outside" would match, but not "buy outside toads". This is
      # the critical difference from the spammy phrases above, which match if
      # something in content matches each element of a phrase array, without
      # regard for how they're ordered, or if there is stuff in between.
      #
      # Obviously they're part of the same array, but marking the separation
      # here helps make it clearer which are "ordered" and which are not.

      [
        /
          (buy|order)
          [\b\s]+
          (#{ DRUG_LIST.join('|') })
          [\b\s-]+
          (with[\b\s]+)?        # allow 'with' something-or-other
          ([\w-]{0,8}[\b\s]+)?  # allow an intervening word, like 'gel' or 'pills'
          (online)
        /imx,
      ],

      [
        /
          (download|watch)
          [\b\s]+
          # allow up to 4 intervening words, like 'call of duty'
          ([\w-]+[\b\s]+){1,4}
          (online|free)[\b\s-]?(online|free)
        /imx,
      ],

      [
        /
          (download|watch)
          [\b\s]+
          # allow up to 9 intervening words, like 'call of duty'
          ([\w-]+[\b\s]+){1,9}
          # followed by an optional 'versus [5 more words]'
          ( (v\s|vs.?|versus) [\b\s]* ([\w-]+[\b\s]+){1,5} )?
          # followed by at least 2 of 'live', 'streaming', 'online', or 'free'
          ((live|stream(ing)?|online|free)[\b\s-]*){2,}
        /imx,
      ],

    ] # SPAMMY_PHRASES

    SPAMMY_PHRASES_KEY = "spammy_regexes"

    def spammy_phrases_datasource
      @spammy_phrases_datasource = begin
        SpamDatasource.find_by_name(SPAMMY_PHRASES_KEY) ||
          SpamDatasource.create(name: SPAMMY_PHRASES_KEY,
                                description: "Spammy Regex phrases for SpamChecker (formerly in redis)")
      rescue ActiveRecord::RecordNotUnique
        retry # rubocop:disable GitHub/UnboundedRetries https://github.com/github/github/issues/134041
      end
    end

    def get_spammy_phrases
      return @phrases if defined? @phrases
      @phrases = GitHub::SpamChecker::SPAMMY_PHRASES.dup
      spammy_phrases_datasource.entries.each do |entry|
        regexps = build_regexps(entry.value)
        # If we didn't get all valid RegExps back, toss the whole phrase.
        next if regexps.empty? || regexps.any? { |r| r.nil? }
        @phrases << regexps
      end
      @phrases
    end

    def build_regexps(phrase_string)
      rcond = Regexp::IGNORECASE | Regexp::MULTILINE | Regexp::EXTENDED
      JSON.parse(phrase_string).map { |r| regex_or_nil(r, rcond) }
    rescue ::Yajl::ParseError, ::JSON::ParserError => e
      notify "Got JSON parse error trying to build Regexps from #{phrase_string}. Exception: #{e.inspect}"
      []
    end

    def regex_or_nil(regexp_str, conditions = 0)
      Regexp.new(regexp_str, conditions)
    rescue RegexpError => e
      notify "Couldn't build valid Regexp from #{regexp_str}. Exception: #{e.inspect}"
      nil
    end

    # A 'phrase' here is an Array of RegExps that travel together.
    def push_spammy_phrase(phrase, key = nil)
      value = phrase.map { |r| r.source }.to_json
      entry = spammy_phrases_datasource.entries.find_by_value(value)
      if entry.nil?
        entry = spammy_phrases_datasource.entries.create value: value,
              additional_context: "Added via SpamChecker#push_spammy_phrase"
      end
      entry
    end

    def push_all_phrases(phrases)
      phrases.each { |phrase| push_spammy_phrase(phrase) }
    end

    def clear_spammy_phrases
      spammy_phrases_datasource.entries.destroy_all
      # With phrases gone make sure we clear up any existing memoization as well
      remove_instance_variable(:@phrases) if defined? @phrases
    end

    def spammy_phrase_count
      spammy_phrases_datasource.entries.count
    end

    def cleanup_text(text)
      return text unless text.present?
      text = GitHub::Encoding.try_guess_and_transcode(text)
      text = GitHub::Unidecode.decode(text)

      text.force_encoding("UTF-8").scrub! unless text.encoding == ::Encoding::UTF_8

      text
    end

    # Check whether the given String is spammy. Operates using word
    # combinations, not single-word blacklisting. This is more useful
    # for things like "streaming NFL online" stuff, since none of those
    # words by themselves should be blacklisted, but that phrase, or even
    # those 3 words together, are a much better indicator that something
    # naughty is afoot.
    #
    # text - String to be checked
    #
    # Returns an Array of Strings that got flagged, or an empty Array if
    # none did.
    def spammy_text(text)
      text = cleanup_text(text)
      matching_phrases = spammy_phrases_that_match(text, get_spammy_phrases)
      matching_phrases.uniq.map { |phrase| phrase.join(" ") }
    end

    def spammy_phrases_that_match(text, spammy_phrases)
      return [] unless text.present?
      text = cleanup_text(text)
      matching_phrases = []
      # This text is spam if there is a match for any of the spammy phrases
      spam_match = spammy_phrases.any? do |phrase|
        # This phrase is a match if there is at least one match for each
        # element of the phrase.
        phrase_matches = phrase.all? do |word|
          unless word.is_a? Regexp
            word = Regexp.new(word, Regexp::IGNORECASE | Regexp::MULTILINE)
          end
          text =~ word
        end
        matching_phrases << phrase if phrase_matches
      end
      if spam_match
        # If we had a match for each element, then the matching phrases matter
        matching_phrases
      else
        # If we didn't have a total match, then the elements that did match
        # are of no interest to us.
        []
      end
    end
  end
end
