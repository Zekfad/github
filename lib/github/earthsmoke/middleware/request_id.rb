# frozen_string_literal: true

class GitHub::Earthsmoke::Middleware::RequestID < ::Earthsmoke::Middleware::Base
  def call(env)
    log_request_id_twirp(env)
    @app.call(env)
  end

  def log_request_id_twirp(env)
    return unless request_id = normalized_request_id
    return unless headers = env[Earthsmoke::Middleware::ENV_HEADERS]
    headers["X-GitHub-Request-Id"] = request_id
  end

  # If we have multiple request-ids, join them together in a way that wont
  # confuse intermediary proxies. Commas get treated the same as multiple header
  # values by haproxy. Rack takes multiple incoming values and joins them with
  # a comma. It's easiest to just combine the two values with a `-` so we know
  # both will make it all the way through to the server.
  def normalized_request_id
    return nil unless request_id = GitHub.context[:request_id]
    request_id.gsub(",", "-")
  end
end
