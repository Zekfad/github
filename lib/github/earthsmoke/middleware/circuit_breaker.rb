# frozen_string_literal: true

class GitHub::Earthsmoke::Middleware::CircuitBreaker < ::Earthsmoke::Middleware::Base
  CircuitClosedError = Class.new(::Earthsmoke::Error)

  CIRCUIT_NAME = "earthsmoke"

  def self.circuit
    Resilient::CircuitBreaker.get(CIRCUIT_NAME, instrumenter: GitHub)
  end

  def call(env)
    raise CircuitClosedError unless allow_request?
    @app.call(env).tap { success }
  rescue
    failure
    raise
  end

  def allow_request?
    self.class.circuit.allow_request?
  end

  def success
    self.class.circuit.success
  end

  def failure
    self.class.circuit.failure
  end
end
