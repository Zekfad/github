# frozen_string_literal: true

module GitHub::Earthsmoke::EncryptedAttribute
  extend self

  DECRYPT_CACHE_SIZE = 100

  def self.extended(base)
    base.include(InstanceMethods)
    base.include(Scientist)
  end

  module InstanceMethods
    def get_encrypted_attribute(ciphertext_name, local: true)
      ciphertext = self[ciphertext_name]
      return ciphertext if ciphertext.nil?

      cache = self.class.decrypt_cache_for_attribute[ciphertext_name]
      cache.getset(ciphertext) do
        # postpone this call to avoid calling into gRPC pre-fork
        key = self.class.encrypted_attribute_key(ciphertext_name)

        key.decrypt(ciphertext, local: local)
      end
    end

    def set_encrypted_attribute(ciphertext_name, plaintext, local: true)
      self[ciphertext_name] = if plaintext.nil?
        plaintext
      else
        # postpone this call to avoid calling into gRPC pre-fork
        key = self.class.encrypted_attribute_key(ciphertext_name)

        key.encrypt(plaintext, local: local).tap do |ciphertext|
          cache = self.class.decrypt_cache_for_attribute[ciphertext_name]
          cache[ciphertext] = plaintext
        end
      end
    end

    def record_encrypted_attribute_stats(plaintext_name, action)
      yield
    rescue ::Earthsmoke::Error => e
      raise
    ensure
      GitHub.dogstats.increment("encrypted_attribute.access",
        tags: [
          "attribute:#{plaintext_name}",
          "table:#{self.class.table_name}",
          "action:#{action}",
          "success:#{e.nil?}",
          "error:#{e&.class&.name}",
        ],
      )
    end

    def handle_encrypted_attribute_errors(behavior)
      yield
    rescue ::Earthsmoke::Error => e
      case behavior
      when :return_nil
        nil
      when :raise
        raise e
      else
        raise ArgumentError, "bad encrypted attribute error behavior: #{behavior}"
      end
    end
  end

  # Create an attribute for this model that will be encrypted in the database
  # using a key stored in earthsmoke.
  #
  # ciphertext_name - The name of the attribute where the ciphertext is stored.
  # plaintext_name  - The name of the attribute that will be defined for getting
  #                   and setting plaintext values.
  # local:          - Whether to export the secret key once and do encryption
  #                   and decryption locally instead of making an RPC to the
  #                   earthsmoke server for each operation (default true).
  #
  # Returns nothing.
  def encrypted_attribute(ciphertext_name, plaintext_name, local: true, read_error: :raise, write_error: :raise, mirror: nil)
    RollEarthsmokeKeys.key_names << encrypted_attribute_key_name(ciphertext_name)

    if GitHub.earthsmoke_enabled?
      define_method(plaintext_name) do
        if mirror
          science "mirroring_attributes_#{ciphertext_name}" do |experiment|
            # old way (control) where we read mirrored column
            experiment.use {
              self[mirror]
            }
            # new way (candidate) where we read encrypted column
            experiment.try {
              record_encrypted_attribute_stats(plaintext_name, :read) do
                get_encrypted_attribute(ciphertext_name, local: local)
              end
            }
          end
        else
          handle_encrypted_attribute_errors(read_error) do
            record_encrypted_attribute_stats(plaintext_name, :read) do
              get_encrypted_attribute(ciphertext_name, local: local)
            end
          end
        end
      end

      define_method("#{plaintext_name}=") do |plaintext|
        handle_encrypted_attribute_errors(write_error) do
          record_encrypted_attribute_stats(plaintext_name, :write) do
            set_encrypted_attribute(ciphertext_name, plaintext, local: local)
            self[mirror] = plaintext if mirror
          end
        end

        return plaintext
      end
    else
      define_method(plaintext_name) do
        self[mirror ? mirror : ciphertext_name]
      end

      define_method("#{plaintext_name}=") do |plaintext|
        self[mirror] = plaintext if mirror
        self[ciphertext_name] = plaintext
      end
    end
  end

  def self.key_name(table_name, ciphertext_name)
    "encrypted_attribute:#{table_name}:#{ciphertext_name}"
  end

  # The name of the earthsmoke key to use for encrypting the given attribute.
  #
  # ciphertext_name - The String name of the attribute.
  #
  # Returns a String earthsmoke key name.
  def encrypted_attribute_key_name(ciphertext_name)
    GitHub::Earthsmoke::EncryptedAttribute.key_name(table_name, ciphertext_name)
  end

  # A per-attribute encryption key.
  #
  # Returns a Hash mapping ciphertext attribute names to Earthsmoke::Key
  # instances.
  def encrypted_attribute_key(ciphertext_name)
    GitHub.earthsmoke.key(encrypted_attribute_key_name(ciphertext_name))
  end

  # A per-attribute LRU cache of decryption results.
  #
  # Returns a Hash mapping ciphertext attribute names to LruRedux::Cache
  # instances.
  def decrypt_cache_for_attribute
    @decrypt_cache_for_attribute ||= Hash.new do |h, k|
      h[k] = LruRedux::Cache.new(DECRYPT_CACHE_SIZE)
    end
  end
end
