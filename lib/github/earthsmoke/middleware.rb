# frozen_string_literal: true

module GitHub::Earthsmoke::Middleware
  autoload :RequestID,      "github/earthsmoke/middleware/request_id"
  autoload :CircuitBreaker, "github/earthsmoke/middleware/circuit_breaker"
  autoload :EnableTracing,  "github/earthsmoke/middleware/enable_tracing"
end
