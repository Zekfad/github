# frozen_string_literal: true
require "acme-client"

class GitHub::Earthsmoke::AcmeJwk < Acme::Client::JWK::ECDSA
  # Name of keypair algorithm to use. This is defined in the
  # github.com/github/earthsmoke/algos Go package.
  KEY_PAIR_ALGO = "ECDSA-NIST-P256V1-SHA256"

  # Specify that we don't need a secret key.
  SECRET_KEY_ALGO = "NULL"

  # Curve parameters for the ECDSA implementation from the ACME client.
  CURVE_PARAMS = Acme::Client::JWK::ECDSA::KNOWN_CURVES["prime256v1"]

  # Instantiate a new EarthsmokeJWK.
  #
  # name - The name of the Earthsmoke key to use.
  #
  # Returns nothing.
  def initialize(name)
    @key_name = name
    @curve_params = CURVE_PARAMS
  end

  def key
    GitHub.earthsmoke.low_level_key(@key_name)
  end

  # Override Acme::Client::JWK::ECDSA#sign to use earthsmoke key.
  #
  # message - String message to sign.
  #
  # Returns String signature.
  def sign(message)
    raw = key.sign(message: message, raw: true)
    @key_version_id = raw.key_version_id

    # ASN.1 SEQUENCE
    seq = OpenSSL::ASN1.decode(raw.signature)

    # ASN.1 INTs
    ints = seq.value

    # Big numbers
    bns = ints.map(&:value)

    # Binary R/S values
    r, s = bns.map { |bn| [bn.to_s(16)].pack("H*") }

    # JWS wants raw R/S concatenated.
    [r, s].join
  end

  # The latest key version ID from the earthsmoke server.
  #
  # Returns an Integer.
  def key_version_id
    sign("foo") if @key_version_id.nil?
    @key_version_id
  end

  # Generate this key on the earthsmokee server.
  #
  # Returns nothing.
  def generate
    key.generate(
      key_pair_algo: KEY_PAIR_ALGO,
      secret_key_algo: SECRET_KEY_ALGO,
    )
  end

  # Public key from the key pair.
  #
  # Returns an OpenSSL::PKey::EC
  def public_key
    # Super hacky. The earthsmoke client can't export public keys yet so we get
    # it from a CSR...
    @public_key ||= key.csr.parsed.public_key.public_key
  end
end
