# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::MarketingStats

  USER_MILLIONS_FALLBACK = 15
  REPO_MILLIONS_FALLBACK = 38
  ISSUE_MILLIONS_FALLBACK = 125
  PAID_ORGS_FALLBACK = 119_000

  USER_MILLIONS_KEY = "site_stats.user.user_millions"
  REPO_MILLIONS_KEY = "site_stats.repository.repository_millions"
  ISSUE_MILLIONS_KEY = "site_stats.issue.issue_millions"
  PAID_ORGS_COUNT_KEY = "site_stats.orgs.paid_count"

  def self.user_millions
    count = GitHub.kv.get(USER_MILLIONS_KEY).value { false }
    count ? format_millions(count) : USER_MILLIONS_FALLBACK
  end

  def self.repository_millions
    count = GitHub.kv.get(REPO_MILLIONS_KEY).value { false }
    count ? format_millions(count) : REPO_MILLIONS_FALLBACK
  end

  def self.issue_millions
    count = GitHub.kv.get(ISSUE_MILLIONS_KEY).value { false }
    count ? format_millions(count) : ISSUE_MILLIONS_FALLBACK
  end

  def self.paid_orgs_count
    count = GitHub.kv.get(PAID_ORGS_COUNT_KEY).value { false }
    count ? format_millions(count) : PAID_ORGS_FALLBACK
  end

  def self.format_millions(count)
    if count.length < 7
    # for test
      count.to_i
    else
      count.to_i / 1_000_000
    end
  end
  private_class_method :format_millions
end
