# frozen_string_literal: true

require "github-launch"
require "github/launch_client"

module GitHub
  module LaunchClient
    module SelfHostedRunners
      include Instrumentation::Model

      # The owner of the Runner can be a Repository or an Organization
      # That the GRPC call asks for "repository_id" is tech debt and should be updated.
      # As long as a Global ID is passed, Launch app will be able to look up the Tenant the runner should be associated with.
      def self.get_runner_registration_token(owner, actor:, instrument: false)
        enforce_service_available!

        response = GitHub.launch_selfhostedrunners.register_runner GitHub::Launch::Services::Selfhostedrunners::RegisterRunnerRequest.new(
          repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
        )

        instrument_event(owner: owner, actor: actor, event: "register_self_hosted_runner") if instrument

        response
      end

      def self.get_runner_removal_token(owner, actor:, instrument: false)
        enforce_service_available!

        response = GitHub.launch_selfhostedrunners.register_runner GitHub::Launch::Services::Selfhostedrunners::RegisterRunnerRequest.new(
          repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
        )

        instrument_event(owner: owner, actor: actor, event: "remove_self_hosted_runner") if instrument

        response
      end

      def self.list_runners(owner)
        enforce_service_available!

        request = GitHub::Launch::Services::Selfhostedrunners::ListRunnersRequest.new(
          repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
        )

        GitHub.launch_selfhostedrunners.list_runners_v2(request)
      end

      def self.update_labels(owner, runner_id:, labels:)
        updates = [
          GitHub::Launch::Services::Selfhostedrunners::BulkReplaceRunnerLabelsRequest::RunnerLabelsUpdate.new(
            runner_id: runner_id.to_i,
            label_ids: labels.map(&:to_i),
          )
        ]

          bulk_replace_labels(owner, updates)
      end

      def self.bulk_update_labels(owner, runner_ids:, additions:, removals:)
        enforce_service_available!

        additions.map!(&:to_i)
        removals.map!(&:to_i)

        updates = runner_ids.map do |r|
          GitHub::Launch::Services::Selfhostedrunners::BulkUpdateRunnerLabelsRequest::LabelOps.new(
            runner_id: r.to_i,
            additions: additions,
            removals: removals,
          )
        end
        GitHub.launch_selfhostedrunners.bulk_update_runner_labels GitHub::Launch::Services::Selfhostedrunners::BulkUpdateRunnerLabelsRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          updates: updates,
        )
      end

      def self.bulk_replace_labels(owner, updates)
        enforce_service_available!

        GitHub.launch_selfhostedrunners.bulk_replace_runner_labels GitHub::Launch::Services::Selfhostedrunners::BulkReplaceRunnerLabelsRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          updates: updates,
        )
      end

      def self.get_runner(owner, runner_id)
        enforce_service_available!

        GitHub.launch_selfhostedrunners.get_runner GitHub::Launch::Services::Selfhostedrunners::GetRunnerRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          runner_id: runner_id,
        )
      end

      def self.delete_runner(owner, runner_id, actor:)
        enforce_service_available!

        instrument_event(owner: owner, actor: actor, event: "remove_self_hosted_runner")

        GitHub.launch_selfhostedrunners.delete_runner GitHub::Launch::Services::Selfhostedrunners::DeleteRunnerRequest.new(
          repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          runner_id: runner_id,
        )
      end

      def self.list_downloads(owner)
        enforce_service_available!

        GitHub.launch_selfhostedrunners.list_downloads GitHub::Launch::Services::Selfhostedrunners::ListDownloadsRequest.new(
          repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
        )
      end

      def self.list_labels(owner)
        enforce_service_available!

        GitHub.launch_selfhostedrunners.list_labels GitHub::Launch::Services::Selfhostedrunners::ListLabelsRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
        )
      end

      def self.create_label(owner, name)
        enforce_service_available!

        GitHub.launch_selfhostedrunners.create_label GitHub::Launch::Services::Selfhostedrunners::CreateLabelRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          name: name,
        )
      end

      # Fake successful label deletion
      def self.delete_label(_owner, _label_id)
        GitHub::Launch::Services::Selfhostedrunners::DeleteLabelResponse.new(status: "deleted")
      end

      def self.get_access_policy(owner)
        enforce_service_available!

        GitHub.launch_selfhostedrunners.get_access_policy GitHub::Launch::Services::Selfhostedrunners::GetAccessPolicyRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
        )
      end

      def self.update_access_policy(owner, permission_type:, selected_repositories:)
        enforce_service_available!

        GitHub.launch_selfhostedrunners.update_access_policy GitHub::Launch::Services::Selfhostedrunners::UpdateAccessPolicyRequest.new(
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: owner.global_relay_id),
          permission_type: permission_type,
          selected_repositories: selected_repositories.map { |repository_id| GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: repository_id) },
        )
      end

      def self.enforce_service_available!
        return if GitHub.launch_selfhostedrunners.present?
        raise LaunchClient::ServiceUnavailable, "self hosted runners service is not configured"
      end
      private_class_method :enforce_service_available!

      def self.instrument_event(owner:, event:, actor:)
        payload = {
          actor: actor,
        }

        case owner
        when Repository
          repo = owner
          payload[:repo] = repo
          payload[:org] = repo.owner if repo.owner.is_a? Organization
          GitHub.instrument "#{owner.event_prefix}.#{event}", payload
        when Organization
          payload[:org] = owner
          GitHub.instrument "#{owner.event_prefix}.#{event}", payload
        when Business
          payload[:business] = owner
          GitHub.instrument "enterprise.#{event}", payload
        end
      end
      private_class_method :instrument_event
    end
  end
end
