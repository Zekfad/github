# frozen_string_literal: true

require "github-launch"
require "github/launch_client"

module GitHub
  module LaunchClient
    module Deployer
      def self.workflow_cancel_all(repo)
        enforce_service_available!

        req = GitHub::Launch::Services::Deploy::WorkflowCancelAllRequest.new(
          repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: repo.global_relay_id),
        )
        GitHub.launch_deployer.workflow_cancel_all(req)
      end

      def self.setup_tenant(target)
        enforce_service_available!

        # Enterprise falls back to 30 seconds from config/unicorn.rb
        # whereas prod has a 10 second limit from GLB
        timeout_ms = GitHub.enterprise? ? 25_000 : 7_000

        req = GitHub::Launch::Services::Deploy::SetupTenantRequest.new(
          global_relay_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: target.global_relay_id),
          timeout_ms: timeout_ms
        )
        GitHub.launch_deployer.setup_tenant(req)
      end

      def self.setup_repository(repo)
        enforce_service_available!

        # For enterprises and forks, the repository plan owner is not the same as the Actions plan owner
        actions_plan_owner_id = repo.async_actions_plan_owner.sync.id

        req = GitHub::Launch::Services::Deploy::SetupRepositoryRequest.new(
          repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: repo.global_relay_id),
          owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: repo.owner.global_relay_id),
          plan_owner_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: actions_plan_owner_id),
          name: repo.name,
          owner: repo.owner.login,
        )
        GitHub.launch_deployer.setup_repository(req)
      end

      def self.enforce_service_available!
        return if GitHub.launch_deployer.present?
        raise LaunchClient::ServiceUnavailable, "deployer service is not configured"
      end
      private_class_method :enforce_service_available!
    end
  end
end
