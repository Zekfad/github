# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Aqueduct
    class ExconInstrumentor < ::Excon::Middleware::Instrumentor
      # The error and request instrumentation work fine, but the response
      # instrumentation needs to pass the entire `datum` hash to the
      # instrumentation block, not just `datum[:response]` as implemented in the
      # excon codebase. The problem is that datum[:response] doesn't yet exist
      # at the time the instrumentation block is first called, so the
      # instrumentation code never sees the response data.
      # See https://github.com/excon/excon/issues/621 for more information.
      def response_call(datum)
        if datum.has_key?(:instrumentor)
          datum[:instrumentor].instrument("#{datum[:instrumentor_name]}.response", datum) do
            @stack.response_call(datum)
          end
        else
          @stack.response_call(datum)
        end
      end
    end
  end
end
