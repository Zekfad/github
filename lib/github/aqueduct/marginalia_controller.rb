# frozen_string_literal: true
#
# GitHub::Aqueduct::MarginaliaController is used by Aqueduct lifecycle hooks to
# set the marginalia controller for the duration of a job running on a worker.
#
# This is only required until we're entirely on ActiveJob, which lets us take
# advantage of marginalia 1.6.0+'s ActiveJob integration by populating the :job
# component.
module GitHub
  module Aqueduct
    class MarginaliaController
      def initialize(job_class)
        @job_class = job_class
      end

      def request_category
        :worker
      end

      def marginalia_route
        @job_class
      end
    end
  end
end
