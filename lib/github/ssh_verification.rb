# rubocop:disable Style/FrozenStringLiteralComment

require "openssl"
require "base64"

module GitHub
  module SshVerification
    extend ActionView::Helpers::DateHelper

    SSH_VERIFICATION_TOKEN_SCOPE = "SSHVerificationToken"

    # Generates a verification token.
    #
    # member - The user login to make the token for. Uses repo_permissions format of
    #          "user:<id>" or "repo:<id>" (for deploy keys).
    #
    # fingerprint - The fingerprint of the SSH used to obtain the token.
    #
    # Returns a verification token (base64 encoded string).
    def self.generate_token(member, fingerprint)
      user = if member.starts_with?("user:")
        User.find_by_id(member.split(":")[1].to_i)
      elsif member.starts_with?("repo:")
        # XXX - we don't verify deploy keys right now
        nil
      else
        raise "bogus member param for ssh verification: #{member.inspect}"
      end
      return unless user
      return unless public_key = user.public_keys.find_by_fingerprint(fingerprint)

      user.signed_auth_token(
        scope: SSH_VERIFICATION_TOKEN_SCOPE,
        expires: 1.week.from_now,
        data: {
          created_at: Time.now.to_i,
          public_key: public_key.id,
        },
      )
    end

    # Validates the provided token.
    #
    # unverified_token - The whitespace insensitive token to be validated.
    # user - The User we are validating the token for.
    #
    # Returns the validation result (bool), and the validation message (string).
    def self.validate(unverified_token, user)
      token = GitHub::Authentication::SignedAuthToken.verify(
        token: unverified_token.gsub(/[[:space:]]+/, ""),
        scope: SSH_VERIFICATION_TOKEN_SCOPE,
      )

      unless token.valid?
        if token.expired?
          return false, "That token was created over 1 week ago and has expired. Ask the user to try again."
        elsif token.bad_login?
          return false, "That token was created for an invalid user. Ask the user to try again."
        elsif token.user_suspended?
          return false, "That token was created for a suspended user."
        else
          return false, "That token is malformed or has been tampered with. Ask the user to try again."
        end
      end

      if token.data["public_key"].blank? || token.data["created_at"].blank?
        return false, "That token is missing data. Don't use that token for verification!"
      end

      unless token.user == user
        return false, "That token was created by '#{token.user}', not '#{user}'. Don't use that token for verification!"
      end

      unless public_key = user.public_keys.find_by_id(token.data["public_key"])
        return false, "That token was created by '#{token.user}' using a SSH key that is no longer authorized for their account. Don't use that token for verification!"
      end

      # Keys that are unverified can only be used for verification if the reason
      # they were unverified is due to lack of use ("stale").
      unless public_key.verified? || public_key.unverification_reason == "stale"
        return false, "That token was created by '#{token.user}' using an unverified SSH key. Don't use that token for verification!"
      end

      # Only keys created by GitHub Desktop can be used for verification.
      authorization = public_key.oauth_authorization
      unless authorization.nil? || (authorization.application && authorization.application.github_desktop?)
        return false, "That token was created for an SSH key created by an OAuth application. Don't use that token for verification!"
      end

      # Mention if it was created by Desktop
      created_at = Time.at token.data["created_at"]
      message = if authorization && authorization.application && authorization.application.github_desktop?
        "That token is valid for user '#{token.user}'. It was created #{time_ago_in_words created_at} ago by GitHub Desktop using a SSH key with the fingerprint '#{public_key.fingerprint}'."
      else
        "That token is valid for user '#{token.user}'. It was created #{time_ago_in_words created_at} ago using a SSH key with the fingerprint '#{public_key.fingerprint}'."
      end

      return true, message
    end
  end
end
