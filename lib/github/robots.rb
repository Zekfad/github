# rubocop:disable Style/FrozenStringLiteralComment

module ::GitHub
  ROBOTS = %w[
    CCBot
    coccoc
    Daumoa
    dotbot
    duckduckbot
    EtaoSpider
    Googlebot
    HTTrack
    ia_archiver
    IntuitGSACrawler
    Mail.RU_Bot
    msnbot
    Bingbot
    naverbot
    red-app-gsa-p-one
    rogerbot
    SandDollar
    seznambot
    Slurp
    Swiftbot
    Telefonica
    teoma
    Twitterbot
    Yandex
  ]
  # NOTE: dotbot and rogerbot belong to moz.com, https://halp.githubapp.com/discussions/7434e9ac05f411e38eee37b944fa3739

  ROBOT_REGEX = Regexp.union(ROBOTS)

  def self.robot?(useragent)
    !!robot_type(useragent)
  end

  def self.robot_type(useragent)
    return unless md = useragent.match(ROBOT_REGEX)
    md[0].downcase.gsub(".", "")
  end
end
