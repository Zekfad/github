# rubocop:disable Style/FrozenStringLiteralComment

require "omniauth/enterprise"

module OmniAuth
  module Strategies
    class CAS
      class ServiceTicketValidator
        def initialize(configuration, return_to_url, ticket)

          GitHub.auth.log(
            method: "#{self.class.name}.#{__method__}",
            configuration: configuration,
            return_to_url: return_to_url,
            ticket: ticket,
          )

          @configuration = configuration
          @uri = URI.parse(@configuration.service_validate_url(return_to_url, ticket))
        end

        def user_info
          auth_success = find_authentication_success(get_service_response_body)
          GitHub.auth.log(
            method: "#{self.class.name}.#{__method__}",
            authentication_success: auth_success,
          )
          parse_user_info(auth_success)
        end
      end

      # Public: Reconciles access by suspending or unsuspending the User record
      # when appropriate.
      #
      # user: User object to suspend/unsuspend
      def reconcile_access(user)
        if user.suspended? && GitHub.reactivate_suspended_user?
          user.unsuspend("is granted access in external authentication system")
        end
      end

      def find_user(uid)
        GitHub.auth.find_user(uid, nil)
      end

      protected
      def callback_phase_prelude
        ticket = request.params["ticket"]

        log_data = {method: "#{self.class.name}.#{__method__}"}

        unless ticket
          GitHub.auth.log log_data.merge(no_ticket: "No CAS Ticket")
          return fail!(:no_ticket, "No CAS Ticket")
        end

        validator = ::OmniAuth::Strategies::CAS::ServiceTicketValidator.new(@configuration, callback_url, ticket)

        @user_info = validator.user_info
        GitHub.auth.log log_data.merge(user_info: @user_info)

        if @user_info.nil? || @user_info.empty?
          GitHub.auth.log log_data.merge(log_message: "Invalid CAS Ticket")
          return fail!(:invalid_ticket, "Invalid CAS Ticket")
        end

        user, _ = find_user(@user_info["user"])
        reconcile_access(user) if user
      end

      def callback_phase
        callback_phase_prelude
        super
      end
    end
  end
end

module GitHub
  module Authentication
    class CAS
      class Strategy < ::OmniAuth::Strategies::CAS
        def initialize(app, options = {}, &block)
          super(app, options.dup, &block)
          GitHub.auth.log(
            method: "#{self.class.name}.#{__method__}",
            options: options,
            configuration: @configuration,
          )
        end

        protected
        def request_phase
          GitHub.auth.log(
            method: "#{self.class.name}.#{__method__}",
            callback_url: callback_url,
            login_url: @configuration.login_url(callback_url),
          )

          # delete REFERER from session, since we've added ?return_to the callback_url already
          @env["rack.session"].delete("omniauth.origin")

          super
        end
      end
    end
  end
end
