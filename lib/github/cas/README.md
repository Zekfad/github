# OmniAuth Enterprise 0.2.6 Customizations

We need better logging for CAS authentication. OmniAuth does a poor job of
logging anything at all.

# Files

    strategy.rb => lib/omniauth/strategies/cas.rb
	service_ticket_validator.rb =>
	    lib/omniauth/strategies/cas/service_ticket_validator.rb

The Strategy file is an OmniAuth middleware that gets added to the Rack
Middleware stack in `config.ru`.

The Service Ticket Validator file is the ticket validation methods used in
validating responses
