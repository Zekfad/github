# frozen_string_literal: true

module GitHub
  module JobStats
    # Limit memory usage reporting to once every 15 seconds per job class.
    MEMORY_USAGE_RATE_LIMIT = 30.seconds

    def self.enqueued
      Thread.current[:job_queue_stats] ||= Hash.new { |h, k| h[k] = 0 }
    end

    def self.reset
      enqueued.clear
    end

    def self.record_enqueue(job_class:)
      enqueued[job_class] += 1
    end

    # Public: Initialize memory usage tracking before a job starts.
    #
    # Returns nothing.
    def self.memory_usage_reset(job_class:, clock: Time)
      # Disable memory usage tracking in Enterprise.
      return if GitHub.enterprise?

      @initial_memory_usage = @final_memory_usage = nil

      # Reporting memory usage requires parsing data out of procfs and can be
      # CPU intensive for hosts with high job volume, so we rate limit by
      # job class.
      memory_usage_rate_limit(key: job_class.to_s, clock: clock) do
        @initial_memory_usage = GitHub::Memory.usage
      end
    end

    # Public: Record memory usage after a job completes.
    #
    # Returns nothing.
    def self.memory_usage_snapshot
      @final_memory_usage = GitHub::Memory.usage
    end

    # Public: Report memory usage before and after job execution. If memory
    # usage recording was disabled or didn't succeeded, returns nil. Otherwise,
    # returns an array of [initial_usage, current_usage].
    #
    # Returns [GitHub::Memory::Usage, GitHub::Memory::Usage] or nil.
    def self.memory_usage_report
      return unless @initial_memory_usage&.success? && @final_memory_usage.success?

      [@initial_memory_usage, @final_memory_usage]
    end

    def self.memory_usage_rate_limit(key:, clock: Time)
      @rate_limiters ||= Hash.new { |h, k| h[k] = 0 }
      now = clock.now.to_i
      previous = @rate_limiters[key]
      if now - MEMORY_USAGE_RATE_LIMIT > previous
        yield
        @rate_limiters[key] = now
      end
    end
  end
end
