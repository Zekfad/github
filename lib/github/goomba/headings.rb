# frozen_string_literal: true

module GitHub::Goomba
  # Collection of document headings (`Heading` instances)
  # Each collection knows its heading level (1-6) and additional
  # Heading instances can be inserted (via `push`).
  class Headings
    include Enumerable
    delegate :each, :empty?, to: :@headings

    attr_reader :level

    def initialize(level = 1)
      @headings = []
      @level = level
    end

    # If the level of the new heading matches this collection's level,
    # it is inserted into this collection.
    # Otherwise, it is assumed to be a nested heading and is pushed to the
    # collection's last heading.
    def push(heading)
      if same_level? heading
        @headings.push(heading)
      elsif @headings.empty? # the heading is a nested heading, but skipped a level (or more)
        @headings.push(heading)
      else
        @headings.last.push(heading)
      end
    end

    def inspect
      map { |h| "#{level}. #{h.inspect}" }.join("")
    end

    def same_level?(heading)
      level == heading.level
    end
  end
end
