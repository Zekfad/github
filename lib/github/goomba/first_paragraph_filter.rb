# frozen_string_literal: true

module GitHub::Goomba
  # Output filter that extracts the first HTML paragraph into
  # result[:first_paragraph]
  class FirstParagraphFilter < OutputFilter
    def call(html)
      first_para = Goomba::DocumentFragment.new(html).select("p").first&.to_html
      first_para = first_para.html_safe if result[:html_safe] # rubocop:disable Rails/OutputSafety
      result[:first_paragraph] = first_para
      html
    end
  end
end
