# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  class HttpsFilter < NodeFilter
    attr_reader :selector

    def initialize(*args)
      super

      @filter = GitHub::HTML::HttpsFilter.new("", context, result)
      @selector = Goomba::Selector.new("a[href^='#{@filter.http_url}']")
    end

    def call(node)
      node["href"] = node["href"].sub(/\Ahttp:/, "https:")
      node
    end
  end
end
