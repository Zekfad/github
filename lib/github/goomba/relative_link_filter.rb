# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  # Relative link filter modifies relative links in markdown based on viewing
  # location, making it so these links lead to the correct destination whether
  # viewed locally, as a blob, or (in the case of a README), as part of a tree
  # view.
  #
  # Requires values passed in the context:
  #
  # entity     - must be a repository; skips otherwise
  # path       - path where this markdown content is being shown
  #              (e.g. "README.md", "nested/dir", or "")
  # committish - committish for this markdown content
  #              (branch name, ref name, commit oid)
  # view       - view where this markdown content is being shown
  #              (viz. :tree, :blob, :preview)
  class RelativeLinkFilter < NodeFilter
    SELECTOR = Goomba::Selector.new("a, img")

    def initialize(*args)
      super
      @filter = GitHub::HTML::RelativeLinkFilter.new("", context, result)
    end

    def selector
      SELECTOR
    end

    URL_ATTRIBUTES = {
      a: "href",
      img: "src",
    }.freeze

    def call(element)
      return nil unless @filter.should_process?

      attribute = URL_ATTRIBUTES.fetch(element.tag)
      new_url = @filter.make_relative(element[attribute])
      return unless new_url

      element[attribute] = new_url
      element
    end
  end
end
