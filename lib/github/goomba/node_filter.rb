# frozen_string_literal: true

module GitHub::Goomba
  # Base class for node filters. Node filters are passed each node in a
  # document fragment that matches the string returned by #selector.
  class NodeFilter < Filter
    # Called before the document is serialized to HTML. Override this method to
    # scan the DOM for any content you need for processing (e.g., to find all
    # usernames ahead of time so that a single DB query can be used to fetch
    # all the mentioned users).
    #
    # document - The Goomba::DocumentFragment that will be processed.
    def scan(document)
    end

    # The main filter entry point.
    #
    # node - The current Goomba::Node.
    #
    # Possible return values:
    #
    # nil              - leaves the node unmodified and continues calling other
    #                    filters
    # node             - same as nil
    # false            - removes the node
    # String           - parses the String as a DocumentFragment and replaces
    #                    the node
    # new Node         - replaces the node
    # DocumentFragment - replaces the node
    def call(text)
      raise NotImplementedError
    end

    # Called once the document has been fully processed. Override this method
    # to perform cleanup.
    def finished
    end

    # Perform a filter on html with the given context.
    #
    # html - An HTML String.
    #
    # Returns a filtered HTML String and may modify the result Hash.
    def self.to_html(html, context = nil, result = nil)
      doc = Goomba::DocumentFragment.new(html)
      filter = self.new(context, result)
      filter.scan(doc)
      output = doc.to_html(filters: [filter])
      filter.finished
      output
    end
  end
end
