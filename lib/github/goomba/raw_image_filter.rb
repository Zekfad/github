# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  # Replaces the source of images linking to blobs with the raw URL, allowing
  # users to easily link to images in a repository. This is especially helpful
  # for private repositories, where the raw image URL gets redirected to
  # raw.githubusercontent.com with a token that expires.
  #
  #   Example: <img src="/user/repo/blob/treeish/path.jpg">
  #   Becomes: <img src="/user/repo/raw/treeish/path.jpg">
  #
  # This filter should be run after RelativeLinkFilter
  class RawImageFilter < NodeFilter
    SELECTOR = Goomba::Selector.new("img")

    # Public: Retrieves a Hash mapping elements processed by this filter to
    # their original src attributes.
    #
    # Returns a Hash of Goomba::ElementNode to String.
    def self.non_raw_image_urls(scratch)
      scratch[:non_raw_image_urls] || {}
    end

    def initialize(*args)
      super
      @filter = GitHub::HTML::RawImageFilter.new("", context, result)
      scratch[:non_raw_image_urls] = {}
    end

    def selector
      SELECTOR
    end

    def call(element)
      original_src = element["src"]
      new_src = @filter.make_raw_url(original_src)
      return unless new_src

      element["src"] = new_src
      non_raw_image_urls[element] = original_src

      element
    end

    private

    def non_raw_image_urls
      self.class.non_raw_image_urls(scratch)
    end
  end
end
