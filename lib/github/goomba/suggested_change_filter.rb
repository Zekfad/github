# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  class SuggestedChangeFilter < NodeFilter
    SELECTOR = Goomba::Selector.new('pre[lang="suggestion" i]')

    def selector
      SELECTOR
    end

    def call(node)
      return node unless original_text
      return node unless new_text = new_text(node)

      if context[:for_email] || context[:render_suggested_changes_as_text]
        render(additions: new_text.split("\n"), deletions: original_text, for_email: true)
      else
        deletions, additions = DiffGenerator.new(original_text.join("\n"), new_text, path).generate_diff
        render(additions: additions, deletions: deletions, for_email: false, line_number: line_number)
      end
    end

    private

    def render(locals)
      ApplicationController.render(partial: "filter_partials/suggested_change", locals: locals, formats: [:html])
    end

    def new_text(node)
      node.children&.first&.text_content&.sub(/\n\Z/, "")
    end

    def original_text
      @original_text_content ||= if comment_in_context?
        comment.original_selection.map { |line| line.sub(/\A[+ -]/, "") }
      elsif context[:original_lines]
        Array.wrap(context[:original_lines]).map { |line| line.sub(/\A[+ -]/, "") }
      end
    end

    def path
      @path ||= if comment_in_context?
        comment.path
      elsif context[:path]
        context[:path]
      end
    end

    def comment_in_context?
      context[:subject].is_a?(PullRequestReviewComment)
    end

    def comment
      context[:subject]
    end

    def current_user
      context[:current_user]
    end

    def line_number
      context[:start_line_number] || context[:line_number]
    end
  end
end
