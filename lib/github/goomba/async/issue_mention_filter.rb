# frozen_string_literal: true

module GitHub::Goomba::Async
  # Processes issue references generated in GitHub::Goomba::IssueMentionFilter,
  # replacing the tags with the appropriate result when necessary.
  #
  # Read the documentation of GitHub::Goomba::IssueMentionFilter first to note
  # how plain text and link target references are generated.
  #
  # We look up all collected nwo/number-pairings simultaneously in #async_scan,
  # saving the located references with #add_issue_reference.
  #
  #
  # Plain text references
  # =====================
  #
  # Given the following tag:
  #
  #   <gh:issue-mention nwo="github/github"
  #                     marker="#"
  #                     number="99872"></gh:issue-mention>
  #
  # Note that the nwo attribute of a <gh:issue-mention> tag always contains the
  # nwo exactly as written. We can infer that "github/github#99872" was the text
  # entered by the user. #call_gh will look up the nwo/number pair, and if
  # found, insert a link to the issue with a normalized nwo, the given marker,
  # and number; if not found, it'll restore the text as it was.
  #
  # "Normalized nwo" here means that a reference to an issue in the same
  # repository will omit the nwo entirely in the output text; i.e.
  # "github/github#99872" in the github/github repository will be linked with
  # the text "#99872". Similarly, a reference to an issue in a different
  # repository in the same network will include the owner name before the
  # marker.  If the repositories are unrelated, the full nwo will be used.
  #
  #
  # Link target references
  # ======================
  #
  # These are regular <a> tags with the gh:issue-mention attribute. There are
  # two cases with link target references:
  #
  # 1. The HTML content of the link exactly matches the link target, such as <a
  #    href="https://github.com/github/github/issues/99872"
  #    gh:issue-mention="{…}">https://github.com/github/github/issues/99872</a>.
  #    This will occur any time a link to an Issue, PR or comment is pasted into
  #    a Markdown document bare, as our Markdown processor will autolink the
  #    URL. A user could also manually write such a link.
  #
  # 2. The HTML content of the link does not match the link target, such as <a
  #    href="https://github.com/github/github/issues/99872"
  #    gh:issue-mention="{…}">See here</a>. This will happen any other time a
  #    link to an issue is created.
  #
  # It's worth noting that there's nothing special about the parsed data in the
  # gh:issue-mention attribute — we parsed the data out of the URL at the time
  # we added the attribute, so we save ourselves work by not reparsing the URL
  # in this filter, as we need access to the data in both #async_scan and #call.
  #
  #
  # HTML content matches link target
  # --------------------------------
  #
  # In this common case, we note that the href tag matches the inner HTML of the
  # anchor. As long as the referenced issue actually exists, we replace the
  # anchor with a fully-blown "issue link", which contains additional metadata
  # tags for our JavaScript to show the issue title in a popup. The text of
  # created link is the normalized nwo (see above), followed by a "#" and the
  # issue number.
  #
  # The URL might include an anchor, such as "#issuecomment-428066274",
  # "#pullrequestreview-162721744", or "#commits-pushed-7110086". The kind of
  # anchor will be used to determine additional text clarifying what is being
  # linked to, rendering link texts such as "#99872 (comments)", "#99872
  # (review)" or "#99872 (commits)".
  #
  #
  # HTML content does not match link target
  # ---------------------------------------
  #
  # In this less common case, we only add hovercard data attributes, and return anchor tag as
  # is. The result is that the user's link works as they intended, and that any content inside the
  # link was processed by the rest of the pipeline as expected, but that we still create issue
  # references for the link.
  #
  class IssueMentionFilter < NodeFilter
    SELECTOR = Goomba::Selector.new(match: "gh|issue-mention, a[gh|issue-mention], gh|discussion-mention, a[gh|discussion-mention]")
    ISSUE_ATTR_SELECTOR = Goomba::Selector.new(match: "a[gh|issue-mention]")
    DISCUSSION_ATTR_SELECTOR = Goomba::Selector.new(match: "a[gh|discussion-mention]")

    def initialize(*args)
      super
      scratch[:issue_references] = {}
      scratch[:discussion_references] = {}
      @filter = GitHub::HTML::IssueMentionFilter.new("", context, result)
    end

    def selector
      SELECTOR
    end

    def async_scan
      Promise.all(@nodes.map do |node|
        if node =~ ISSUE_ATTR_SELECTOR
          data = JSON.parse(node["gh:issue-mention"])
          nwo, number = data["nwo"], data["number"]
        elsif node =~ DISCUSSION_ATTR_SELECTOR
          data = JSON.parse(node["gh:discussion-mention"])
          nwo, number = data["nwo"], data["number"]
        else
          nwo, number = node["nwo"], node["number"]
        end

        async_issue_or_discussion_reference(number.to_i, nwo).then do |reference|
          next unless reference

          promises = [reference.async_repo_and_owner]

          if reference.respond_to?(:async_pull_request)
            promises << reference.async_pull_request
          end

          Promise.all(promises)
        end
      end)
    end

    # Translates a `<gh:issue-mention>` tag or `<a gh:issue-mention="...">` tag
    # into regular HTML.
    def call(node)
      if (node =~ ISSUE_ATTR_SELECTOR) || (node =~ DISCUSSION_ATTR_SELECTOR)
        call_a(node)
      else
        call_gh(node)
      end
    end

    def call_gh(node)
      nwo, marker, number =
        node["nwo"], node["marker"], node["number"]

      if reference = issue_reference(nwo, number)
        nwo = normalize_nwo(nwo, reference.issue)

        @filter.issue_link(reference.issue.url,
                           "#{nwo}#{marker}#{number.to_i}",
                           issue: reference.issue)
      elsif reference = discussion_reference(nwo, number)
        nwo = normalize_nwo(nwo, reference.discussion)

        @filter.discussion_link(reference.discussion.url,
          "#{nwo}#{marker}#{number.to_i}",
          discussion: reference.discussion)
      else
        "#{nwo}#{marker}#{number}"
      end
    end

    def call_a(node)
      href, inner_html = node["href"], node.inner_html
      data = JSON.parse(node["gh:issue-mention"] || node["gh:discussion-mention"])
      nwo, number, anchor =
        data["nwo"], data["number"], data["anchor"]
      issue_ref = issue_reference(nwo, number)
      discussion_ref = unless issue_ref
        discussion_reference(nwo, number)
      end

      if !issue_ref &&  !discussion_ref
        node.remove_attribute("gh:issue-mention")
        node.remove_attribute("gh:discussion-mention")
        return nil
      end

      record_belonging_to_repo = issue_ref ? issue_ref.issue : discussion_ref.discussion

      if href == inner_html
        nwo = normalize_nwo(nwo, record_belonging_to_repo)

        threaded = if discussion_ref
          discussion_comment = GitHub::HTML::IssueMentionFilter.references_threaded_discussion_comment?(discussion_ref.discussion, anchor)
        end

        text = "#{nwo}##{number.to_i}#{anchor_text(anchor, threaded: threaded)}"

        if issue_ref
          @filter.issue_link(record_belonging_to_repo.url + anchor.to_s,
            text, issue: record_belonging_to_repo, anchor: anchor)
        else
          @filter.discussion_link(record_belonging_to_repo.url + anchor.to_s,
            text, discussion: record_belonging_to_repo, anchor: anchor)
        end
      else
        # Don't change the original node except for adding hovercard data attributes
        if issue_ref
          add_issue_hovercard_attrs_to_node(node, issue: record_belonging_to_repo)
        else
          add_discussion_hovercard_attrs_to_node(node, discussion: record_belonging_to_repo,
            anchor: anchor)
        end
        node.remove_attribute("gh:issue-mention")
        node.remove_attribute("gh:discussion-mention")
        nil
      end
    end

    def issue_mentions
      result[:issues] ||= []
    end

    def discussion_mentions
      result[:discussions] ||= []
    end

    def async_bot_can_access_issue_or_discussion?(repo, issue_or_discussion)
      return Promise.resolve(false) if repo.nil? || issue_or_discussion.nil?
      return Promise.resolve(false) unless current_user&.can_have_granular_permissions?

      if issue_or_discussion.is_a?(Issue)
        if issue_or_discussion.pull_request?
          repo.resources.pull_requests.async_readable_by?(current_user)
        else
          repo.resources.issues.async_readable_by?(current_user)
        end
      else
        # TODO: Discussions will end up here but we're not sure exactly what to
        # do about that yet.
        # https://github.com/github/github/pull/140918#issuecomment-621492555
        Promise.resolve(false)
      end
    end

    def async_issue_or_discussion(number, owner_or_nwo)
      async_find_repository(owner_or_nwo).then do |repository|
        next Promise.resolve(nil) unless repository

        if current_user&.can_have_granular_permissions?
          async_find_issue_or_discussion(repository, number, search_parents: owner_or_nwo.blank?).then do |issue_or_discussion|
            async_bot_can_access_issue_or_discussion?(repository, issue_or_discussion).then do |bot_can_access|
              next unless bot_can_access
              issue_or_discussion
            end
          end
        else
          async_can_access_repo?(repository).then do |user_can_access|
            next unless user_can_access
            async_find_issue_or_discussion(repository, number, search_parents: owner_or_nwo.blank?)
          end
        end
      end
    end

    def async_issue_or_discussion_reference(number, owner_or_nwo)
      reference = issue_reference(owner_or_nwo, number)
      return Promise.resolve(reference) if reference

      async_issue_or_discussion(number, owner_or_nwo).then do |issue_or_discussion|
        next unless issue_or_discussion

        issue_or_discussion.async_repository.then do
          if issue_or_discussion.is_a?(Discussion)
            add_discussion_reference(owner_or_nwo, number, issue_or_discussion)
          else
            add_issue_reference(owner_or_nwo, number, issue_or_discussion)
          end
        end
      end
    end

    def async_find_issue_or_discussion(repository, number, search_parents: false)
      async_load_issue_or_discussion(repository, number).then do |issue_or_discussion|
        next issue_or_discussion if issue_or_discussion
        next unless search_parents

        # if the issue wasn't found, try searching up the parent chain but only when
        # no explicit repository reference was given (i.e., "foo/bar#33" won't
        # search in other repositories).
        repository.async_parent.then do |parent_repo|
          next unless parent_repo
          async_find_issue_or_discussion(parent_repo, number, search_parents: search_parents)
        end
      end
    end

    def async_load_issue_or_discussion(repository, number)
      Platform::Loaders::IssueOrDiscussionByNumber.load(repository.id, number.to_i).then do |issue_or_discussion|
        next unless issue_or_discussion

        if issue_or_discussion.is_a?(Issue)
          # PRs can't be disabled, so return the Issue
          # for a PR even if Issues are disabled
          next issue_or_discussion if repository.has_issues || issue_or_discussion.pull_request?
        elsif issue_or_discussion.is_a?(Discussion)
          if repository.has_discussions? && repository.discussions_enabled?
            next issue_or_discussion
          end
        end
      end
    end

    def anchor_text(anchor, threaded: nil)
      return if anchor.blank?

      text = case anchor
      when /discussion-diff-/
        "diff"
      when /commits-pushed-/
        "commits"
      when /ref-/
        "reference"
      when /pullrequestreview/
        "review"
      when /discussioncomment-/
        threaded ? "reply in thread" : "comment"
      else
        "comment"
      end

      " (#{text})"
    end

    private

    # Internal: Adds hovercard attributes to a Goomba::ElementNode.
    #   The original intent of this method was to modify an existing anchor node in the case where
    #   the content doesn't match the href link.
    #
    # node - Goomba::ElementNode
    # hovercard_attrs - a hash of String => String
    #
    # Returns a Goomba::ElementNode
    def add_hovercard_attrs_to_node(node, hovercard_attrs:)
      hovercard_attrs.each_pair do |attribute, val|
        node["data-#{attribute}"] = val
      end

      node
    end

    # Internal: Adds hovercard attributes for an issue to a Goomba::ElementNode.
    # Returns a Goomba::ElementNode
    def add_issue_hovercard_attrs_to_node(node, issue:)
      hovercard_attrs = HovercardHelper.hovercard_data_attributes_for_issue_or_pr(issue)
      add_hovercard_attrs_to_node(node, hovercard_attrs: hovercard_attrs)
    end

    # Internal: Adds hovercard attributes for a discussion to a Goomba::ElementNode.
    # Returns a Goomba::ElementNode
    def add_discussion_hovercard_attrs_to_node(node, discussion:, anchor: nil)
      repo = discussion.repository
      return node unless repo

      comment_id, _ = GitHub::HTML::IssueMentionFilter.comment_id_and_type_from(anchor)
      hovercard_attrs = HovercardHelper.hovercard_data_attributes_for_discussion(
        repo.owner&.login,
        repo.name,
        discussion.number,
        comment_id: comment_id,
      )
      add_hovercard_attrs_to_node(node, hovercard_attrs: hovercard_attrs)
    end

    # Internal: Retrieves and updates the existing issue reference or creates a new one for the given
    #   repo identifier and issue number combination. References are cached in the "scratch" hash
    #   shared between all filters in the pipeline instance.
    #
    # owner_or_nwo - a string name with owner representing the repo.
    # number - an issue/PR number
    # issue - the Issue corresponding to the number, to be cached
    #
    # Returns an IssueReference
    def add_issue_reference(owner_or_nwo, number, issue)
      if existing_reference = issue_reference(owner_or_nwo, number)
        existing_reference
      elsif issue.repository
        reference = GitHub::HTML::IssueReference.new(issue, "")
        key = [owner_or_nwo, number]
        scratch[:issue_references][key] = reference
        issue_mentions << reference
        reference
      end
    end

    # Internal: Retrieves and updates the existing discussion reference or creates a new one for the given
    #   repo identifier and discussion number combination. References are cached in the "scratch" hash
    #   shared between all filters in the pipeline instance.
    #
    # owner_or_nwo - a string name with owner representing the repo.
    # number - an discussion number
    # discussion - the Discussion corresponding to the number, to be cached
    #
    # Returns a DiscussionReference
    def add_discussion_reference(owner_or_nwo, number, discussion)
      if existing_reference = discussion_reference(owner_or_nwo, number)
        existing_reference
      elsif discussion.repository
        reference = GitHub::HTML::DiscussionReference.new(discussion)
        key = [owner_or_nwo, number]
        scratch[:discussion_references][key] = reference
        discussion_mentions << reference
        reference
      end
    end

    def issue_reference(owner_or_nwo, number)
      key = [owner_or_nwo, number.to_i]
      scratch[:issue_references][key]
    end

    def discussion_reference(owner_or_nwo, number)
      key = [owner_or_nwo, number.to_i]
      scratch[:discussion_references][key]
    end

    # Internal: Rewrites the nwo according to the repository context. Uses the current nwo of the
    # repo of the matched issue/discussion.
    #
    # nwo - repository owner/name string
    # record_belonging_to_repo - an Issue or Discussion, something that has a #repository
    #
    # Returns the nwo String for the issue/discussion
    def normalize_nwo(nwo, record_belonging_to_repo)
      record_repo = record_belonging_to_repo.repository

      # If we have a repository context, normalize the reference nwo down to the
      # minimum number of components; none if completely equal, owner only if
      # same network.
      #
      # (We may not have a repository context, e.g. a card in an
      # organisation-level Project.)
      if repository
        return "" if record_repo == repository

        if record_repo.source_id == repository.source_id
          return record_repo.owner.name
        end
      end

      # Write out an nwo with the same components as supplied, but fetching the
      # data from the referenced issue/discussion, to account for user and/or
      # repo renames.
      return "" if nwo.blank?
      return record_repo.owner.name if !nwo.include?("/")
      record_repo.name_with_owner
    end
  end
end
