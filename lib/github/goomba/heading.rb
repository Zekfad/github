# frozen_string_literal: true

module GitHub::Goomba
  # Represents a document heading (h1-h6)
  # Knows the heading text, its heading level, deep-linking anchor fragment
  # and most importantly, contains a collection of nested sub headings.
  class Heading
    include Enumerable
    delegate :each, :push, to: :@sub_headings

    attr_reader :text, :level, :anchor

    def initialize(element, anchor)
      @text = element.text_content
      @anchor = anchor
      @level = /\d/.match(element.tag).to_s.to_i

      @sub_headings = Headings.new(level + 1)
    end

    def inspect
      ["#{text}\n"].concat(
        @sub_headings.inspect.lines).join("  ")
    end
  end
end
