# frozen_string_literal: true

module GitHub::Goomba
  # Construct a WarpPipe for running multiple filters.  A pipeline is created once
  # with one to many filters, and it then can be `call`ed many times over the course
  # of its lifetime with input.
  #
  # input_filters   - Array of InputFilter classes. Each must respond to
  #                   new(context, result) and return an instance that responds
  #                   to call(input) and returns a modified String. Filters
  #                   are performed in the order provided.
  # sanitizer       - An optional Goomba::Sanitizer.
  # node_filters    - Array of NodeFilter classes. Each must respond to
  #                   new(context, result) and return an instance that responds
  #                   to #call(node) and, optionally, #selector. Filters that
  #                   operate on the same node are performed in the order
  #                   provided.
  # output_filters  - Array of OutputFilter classes. Each must respond to
  #                   new(context, result) and return an instance. Filters are
  #                   performed in the order provided.
  # default_context - The default context hash. Values specified here will be merged
  #                   into values from the each individual pipeline run.  Can NOT be
  #                   nil.  Default: empty Hash.
  # result_class    - The default Class of the result object for individual
  #                   calls.  Default: Hash.  Protip:  Pass in a Struct to get
  #                   some semblance of type safety.
  # stats_key       - An optional String Graphite key for metrics collection.
  #                   If specified, stats are recorded both under
  #                   github.goomba.warp_pipe and
  #                   github.goomba.warp_pipes.<stats_key>.
  class WarpPipe
    def initialize(input_filters: [], sanitizer: nil, node_filters: [], output_filters: [], default_context: {}, result_class: Hash, stats_key: nil)
      @input_filters = input_filters
      @sanitizer = sanitizer
      @node_filters = node_filters
      @output_filters = output_filters
      @default_context = default_context
      @result_class = result_class
      @stats_key = stats_key
    end

    # Apply all filters in the pipe to the given input.
    #
    # Performs the following operations:
    #
    # 1. Passes the input through each InputFilter in order.
    # 2. Parses (and optionally sanitizes) the output of the last InputFilter
    #    into a Goomba::DocumentFragment.
    # 3. Serializes the DocumentFragment using the NodeFilters to filter text
    #    and element nodes.
    # 4. Passes the serialized HTML through each OutputFilter in order.
    #
    # input   - A String containing text or HTML.
    # context - The context hash passed to each filter. See the Filter docs
    #           for more info on possible values. This object MUST NOT be modified
    #           in place by filters.  Use the Result for passing state back.
    # result  - The result Hash passed to each filter for modification.  This
    #           is where Filters store extracted information from the content.
    #
    # Returns the result Hash after being filtered by this WarpPipe.  Contains an
    # :output key with the String HTML markup. If a sanitizer was used, also
    # contains a :html_safe key with the value true.
    def call(input, context = {}, result = nil)
      reset_timings
      instrument("call") do
        async_call!(input, context, result).sync
      end
    ensure
      report_timings
    end

    def async_call(input, context = {}, result = nil)
      reset_timings
      instrument("async_call") do
        async_call!(input, context, result)
      end
    ensure
      report_timings
    end

    # Like call but guarantee the value returned is a string of HTML markup.
    def to_html(input, context = {}, result = nil)
      async_to_html(input, context, result).sync
    end

    # Like async_call but guarantee the promise returned contains a string of HTML markup.
    def async_to_html(input, context = {}, result = nil)
      async_call(input, context, result).then do |result|
        output = result[:output]

        html = if output.is_a?(String)
          output
        else
          output.to_html
        end

        result[:html_safe] ? html.html_safe : html # rubocop:disable Rails/OutputSafety
      end
    end

    private

    def async_call!(input, context, result)
      context = @default_context.merge(context)
      context = context.freeze
      result ||= @result_class.new
      scratch = {}
      sanitizer = context.fetch(:sanitizer, @sanitizer)

      input_filters = instantiate_filters(@input_filters, InputFilter, context, result, scratch)
      node_filters = instantiate_filters(@node_filters, NodeFilter, context, result, scratch)

      html = input_filters.inject(input) do |text, filter|
        time("#{filter.class.name}#call") do
          filter.call(text)
        end
      end

      doc = Goomba::DocumentFragment.new(html, sanitizer, gather_stats: !!@stats_key)
      result[:html_safe] = true if sanitizer

      node_filters.each do |filter|
        time("#{filter.class.name}#scan") do
          filter.scan(doc)
        end
      end
      html = doc.to_html(filters: node_filters)
      @timings.concat(doc.__stats__) if @stats_key && doc.__stats__
      node_filters.each do |filter|
        time("#{filter.class.name}#finished") do
          filter.finished
        end
      end

      output_filters = instantiate_filters(@output_filters, OutputFilter, context, result, scratch)

      async_call_output_filters = output_filters.reduce(Promise.resolve(html)) do |output_promise, filter|
        output_promise.then do |output|
          if filter.is_a?(AsyncOutputFilter)
            filter.async_call(output)
          else
            time("#{filter.class.name}#call") do
              filter.call(output)
            end
          end
        end
      end

      async_call_output_filters.then do |output|
        result[:output] = output
        result
      end
    end

    def instantiate_filters(filters, klass, context, result, scratch)
      filters
        .map { |filter| filter.new(context, result, scratch) if filter.enabled?(context) }
        .compact
        .each { |filter| raise TypeError, "#{filter} does not inherit from #{klass}" unless filter.is_a?(klass) }
    end

    def reset_timings
      @start_time = GitHub::Dogstats.monotonic_time
      @timings = []
    end

    def time(name)
      return yield unless @stats_key
      start = GitHub::Dogstats.monotonic_time
      begin
        yield
      ensure
        @timings << [name, 1, GitHub::Dogstats.monotonic_time - start]
      end
    end

    def report_timings
      return unless @stats_key
      report_datadog_timings
    end

    # Optimized to reduce number of allocations as suggested by @brasic (https://github.com/github/github/pull/143337#discussion_r422548105)
    WARP_PIPE = "goomba.warp_pipe"
    WARP_PIPE_BY_FILTER = "goomba.warp_pipe.by_filter"
    SPLIT_REGEX = /\.|::|#/
    def report_datadog_timings
      @operation_stats_tags ||= Hash.new { |h, name|
        filter_name, operation = name.split(SPLIT_REGEX)[-2..-1]
        h[name] = ["operation:#{operation}", "filter:#{filter_name}"]
      }
      @pipeline_tags ||= ["pipeline:#{@stats_key}"]
      GitHub.dogstats.batch do |stats|
        @timings.each do |name, call_count, total_duration|
          stats.timing(WARP_PIPE_BY_FILTER, total_duration, tags: @operation_stats_tags[name], sample_rate: 0.01)
        end
        stats.timing(WARP_PIPE, GitHub::Dogstats.duration(@start_time), tags: @pipeline_tags,  sample_rate: 0.01)
      end
    end

    def instrument(action, &blk)
      if @stats_key
        GitHub.instrument("#{action}.#{@stats_key}.warp_pipe.goomba", &blk)
      else
        blk.call
      end
    end
  end
end
