# frozen_string_literal: true
#
module GitHub::Goomba
  class UrlUnfurlFilter < NodeFilter
    include GitHub::UTF8

    SELECTOR = Goomba::Selector.new(match: "a",
      reject: "blockquote a, li a, a[href^='#{GitHub.url}']")

    def selector
      SELECTOR
    end

    def call(node)
      return unless is_unfurlable?(node)
      href = utf8(node["href"])
      ActionController::Base.helpers.content_tag("gh:content-reference", nil, href: href)
    end

    def self.enabled?(context)
      !context[:for_email] &&
          context.has_key?(:subject) &&
          context.has_key?(:entity) &&
          context[:entity].is_a?(Repository)
    end

    def is_unfurlable?(node)
      href = utf8(node["href"])

      # Avoid unfurling hand-crafted markdown links
      return false if utf8(node.text_content) != href

      uri = Addressable::URI.parse(href)
      return false if !uri || uri.host.blank?
      return false unless %w(http https).include?(uri.scheme)

      true
    rescue Addressable::URI::InvalidURIError
      false
    end
  end
end
