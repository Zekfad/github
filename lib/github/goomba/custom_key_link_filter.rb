# frozen_string_literal: true
#
module GitHub::Goomba
  class CustomKeyLinkFilter < TextFilter
    SELECTOR = Goomba::Selector.new(match: ":text", reject: "a :text")

    def self.enabled?(context)
      repository = context[:entity].is_a?(Repository) && context[:entity]

      repository &&
        repository.async_plan_supports?(:custom_key_links).sync &&
        repository.key_links.any?
    end

    def selector
      SELECTOR
    end

    # Lookbehind to ensure that keys are either at the beginning of a
    # line or preceded by non-alpha-numeric character (space, parenthesis,
    # period, comma, etc.).
    KEY_LEAD_RE = %r{(?<=\b)}

    # Lookahead to ensure that keys are either at the end of a line or
    # followed by non-alpha-numeric character (space, parenthesis, period,
    # comma, etc.).
    KEY_TAIL_RE = %r{(?=\b)}

    def call(text)
      content = text.html

      repository.key_links.each do |key_link|
        # Construct regex with two capture groups to match key and number
        re_str = "#{KEY_LEAD_RE}(#{key_link.key_prefix}(\\d+))#{KEY_TAIL_RE}"
        regex = Regexp.new(re_str, Regexp::IGNORECASE)

        # Replace all keys that match the pattern
        content.gsub!(regex) do |match|
          matched_fullid = $1
          matched_number = $2
          url = key_link.url(matched_number)
          # Abort all processing if we detect a single invalid URL
          return unless URI::ABS_URI.match?(url)

          attrs = {
            "class"      => "issue-link js-issue-link",
            "rel"        => "noopener noreferrer nofollow",
          }
          GitHub.dogstats.increment("key_links", tags: ["action:linked"])
          ActionController::Base.helpers.link_to(matched_fullid, url, attrs)
        end
      end

      content
    end

  end
end
