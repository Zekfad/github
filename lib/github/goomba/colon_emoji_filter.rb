# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  # Goomba text filter that replaces :emoji: with images.
  #
  # Context options:
  #   :asset_root (required) - base url to link to emoji sprite
  class ColonEmojiFilter < TextFilter
    IGNORE_PARENTS = GitHub::HTML::EmojiFilter::IGNORE_PARENTS.map { |tag| "#{tag} :text" }.join(", ")
    SELECTOR = Goomba::Selector.new(match: ":text", reject: IGNORE_PARENTS)

    def initialize(*args)
      super
      @filter = GitHub::HTML::EmojiFilter.new("", context, result)
    end

    def selector
      SELECTOR
    end

    def call(text)
      return nil unless text.html.include?(":")
      result = @filter.emoji_image_filter(text.html)
      if result == text.html
        # There were no modifications, so just return nil to keep the text node as-is.
        # FIXME: We should use gsub! instead of gsub to implement this behavior
        # more efficiently.
        nil
      else
        result
      end
    end
  end
end
