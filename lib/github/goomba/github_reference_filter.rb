# frozen_string_literal: true

module GitHub::Goomba
  # TODO: interleave all gh| matches rather than scanning the document
  # separately 'n' times (for n different async_scan filters).

  module Async; end
  require "github/goomba/async/node_filter"
  require "github/goomba/async/mention_filter"
  require "github/goomba/async/team_mention_filter"
  require "github/goomba/async/commit_mention_filter"
  require "github/goomba/async/compare_mention_filter"
  require "github/goomba/async/close_keyword_filter"
  require "github/goomba/async/issue_mention_filter"
  require "github/goomba/async/issue_blob_filter"
  require "github/goomba/async/url_unfurl_filter"
  require "github/goomba/async/advisory_mention_filter"
  require "github/goomba/async/cve_mention_filter"

  class GithubReferenceFilter < AsyncOutputFilter
    SELECTOR = Goomba::Selector.new(match: "gh|*, [gh|*]")
    def async_call(html)
      gh_nodes = Hash[[
        Async::MentionFilter,
        Async::TeamMentionFilter,
        Async::CommitMentionFilter,
        Async::CompareMentionFilter,
        Async::IssueMentionFilter,
        Async::CloseKeywordFilter,
        Async::IssueBlobFilter,
        Async::UrlUnfurlFilter,
        Async::AdvisoryMentionFilter,
        Async::CVEMentionFilter,
      ].map { |fc| [fc::SELECTOR, fc] }]

      doc = Goomba::DocumentFragment.new(html, nil)

      doc.select(SELECTOR).each do |node|
        r = gh_nodes.detect { |selector, val| node.matches(selector) }
        next if r.nil?
        selector, val = r

        if val.is_a?(Class)
          val = gh_nodes[selector] = val.new(context, result, scratch)
        end
        val.add_node(node)
      end

      fs = gh_nodes.values.select { |n| n.is_a?(Async::NodeFilter) }

      Promise.all(fs.map { |f| f.async_scan }).then do
        doc.to_html(filters: fs)
      end
    end
  end
end
