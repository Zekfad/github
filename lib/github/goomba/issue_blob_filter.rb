# frozen_string_literal: true

module GitHub::Goomba
  class IssueBlobFilter < NodeFilter
    SELECTOR = Goomba::Selector.new(match: "a[href^='#{GitHub.url}']")

    PERMALINK_REGEX = %r{
      \A
      https?://
      #{Regexp.escape(GitHub.host_name)}
      /
      (?<nwo>#{GitHub::HTML::IssueMentionFilter::NWO})
      /blob/
      (?<commit_oid>[a-f0-9]{40})
      /
      (?<filepath>[^#]+)
      \#
      (L(?<range_start>\d+)-)?L(?<range_end>\d+)
      \z
    }x

    def selector
      SELECTOR
    end

    def call(node)
      return unless repository

      inner_html = node.inner_html
      return unless node["href"] == inner_html

      href = node["href"].b

      if md = href.match(PERMALINK_REGEX)
        # TODO: Properly support renamed repos here
        return unless repository.name_with_owner == md[:nwo]

        blob_mention_tag(
          filepath: md[:filepath],
          commit_oid: md[:commit_oid],
          range_start: md[:range_start] || md[:range_end],
          range_end: md[:range_end],
          permalink: href,
          text: inner_html,
        )
      end
    end

    def self.enabled?(context)
      return false if context[:hide_code_blobs]
      !context[:for_email]
    end

    # returns an html-safe gh:blob-mention tag
    def blob_mention_tag(filepath:, commit_oid:, range_start:, range_end:, permalink:, text:)
      attrs = {
        commit_oid: commit_oid,
        filepath: filepath,
        permalink: permalink,
        range_start: range_start,
        range_end: range_end,
        text: text,
      }.reject { |k, v| v.nil? }

      # passing nil for body ensures the closing tag remains
      ActionController::Base.helpers.content_tag("gh:blob-mention", nil, attrs)
    end
  end
end
