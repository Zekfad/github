# rubocop:disable Style/FrozenStringLiteralComment
require "addressable/uri"
require "asset_bundles"
require "codeowners"
require "net/http"
require "uri"

module GitHub
  module BrowserStatsHelper
    extend self

    def self.included(base)
      base.helper_method(*instance_methods) if base.respond_to?(:helper_method)
    end

    # Returns a Browser instance
    def parsed_useragent
      @parsed_useragent ||= Browser.new(request.env["HTTP_USER_AGENT"])
    end

    def user_agent_platform(parsed_useragent)
      [parsed_useragent.platform.name, parsed_useragent.platform.version].compact.join(" ")
    end

    # Extracts the major version number from a UA string (if any)
    #
    # Returns an integer version number or zero
    def user_agent_major_version_number(full_version)
      full_version.split(".").first.to_i
    end

    # Public determines if the request's referrer matches the current host.
    # This is used to filter out stats from non-prod hosts.
    def referred_by_prod?
      referrer = Addressable::URI.parse(request.referrer)
      referrer.try(:hostname) == GitHub.host_name
    rescue Addressable::URI::InvalidURIError
      false
    end

    def guess_url_params(url)
      parsed_url = Addressable::URI.parse(url)
      if parsed_url
        GitHub::Application.routes.recognize_path(parsed_url.path, method: :get)
      end
    rescue ActionController::RoutingError, Addressable::URI::InvalidURIError
      nil
    end

    def guess_url_controller_action(url)
      if params = guess_url_params(url)
        return params[:controller], params[:action]
      end
    end

    def report_metrics(stats, user_agent: nil)
      if user_agent
        parsed_useragent = Browser.new(user_agent)
        if parsed_useragent.name
          parameterized_browser = EncodingSafeParameterizeHelper.encoding_safe_parameterize(parsed_useragent.name)
          user_agent_tag = "useragent:#{parameterized_browser}"
          mobile_tag = "mobile:#{!!parsed_useragent.device.mobile?}"
        end
      end

      stats.each do |stat|
        tags = [user_agent_tag, mobile_tag, "logged_in:#{!!stat[:logged_in]}"]
        if request_url = stat[:request_url]
          if match = guess_url_controller_action(request_url.to_s)
            tags << "controller:#{match[0]}_controller"
            tags << "action:#{match[1]}"
          end
        end
        tags.freeze

        report_downloaded_bundles(stat[:downloaded_bundles], tags: tags)
        report_pjax_stats(stat, tags: tags)
        report_timings(Array.wrap(stat[:resource_timings]) + Array.wrap(stat[:navigation_timings]), tags: tags)
        report_web_vitals(Array.wrap(stat[:web_vital_timings]), tags: tags)
        report_increment_key(stat, tags: tags)
        report_hydro_event(stat, tags: tags)
      end
    end

    def report_downloaded_bundles(bundles, tags:)
      return unless bundles
      assets = AssetBundles.get
      bundles = bundles.select { |bundle| assets.bundle_exists?(bundle) }
      bundles.each do |bundle|
        GitHub.dogstats.increment("browser.bundle_downloads", tags: tags + ["bundle:#{bundle}"])
      end
    end

    def report_pjax_stats(stats, tags:)
      return unless stats[:pjax_duration]
      GitHub.dogstats.timing("browser.pjax_performance", stats[:pjax_duration], tags: tags)
    end

    def report_web_vitals(timings, tags:)
      timings.each do |timing|
        timing_tags = tags.dup

        if match = guess_url_controller_action(timing[:name].to_s)
          timing_tags << "controller:#{match[0]}_controller"
          timing_tags << "action:#{match[1]}"
        end

        GitHub.dogstats.batch do |dogstats|
          dogstats.distribution("browser.vitals.dist.cls", timing[:cls], tags: timing_tags) if timing[:cls].to_i > 0
          dogstats.distribution("browser.vitals.dist.fcp", timing[:fcp], tags: timing_tags) if timing[:fcp].to_i > 0
          dogstats.distribution("browser.vitals.dist.fid", timing[:fid], tags: timing_tags) if timing[:fid].to_i > 0
          dogstats.distribution("browser.vitals.dist.lcp", timing[:lcp], tags: timing_tags) if timing[:lcp].to_i > 0
          dogstats.distribution("browser.vitals.dist.ttfb", timing[:ttfb], tags: timing_tags) if timing[:ttfb].to_i > 0
        end
      end
    end

    def report_timings(timings, tags:)
      timings.each do |timing|
        timing_tags = tags.dup

        # TODO: Restrict entryType via GraphQL Enum
        case timing[:entry_type]
        when "navigation", "resource"
          entry_type = timing[:entry_type]
        else
          next
        end

        # TODO: Restrict initiatorType via GraphQL Enum
        case timing[:initiator_type]
        when "navigation", "xmlhttprequest", "fetch", "beacon", "link", "script", "img"
          timing_tags << "initiator:#{timing[:initiator_type]}"
        else
          timing_tags << "initiator:other"
        end

        case timing[:initiator_type]
        when "navigation", "xmlhttprequest", "fetch", "beacon"
          if match = guess_url_controller_action(timing[:name].to_s)
            timing_tags << "controller:#{match[0]}_controller"
            timing_tags << "action:#{match[1]}"
          end
        when "link", "script", "img"
          if timing[:name].path.start_with?("/assets")
            name = File.basename(timing[:name].path).sub(/\-[a-f0-9]+/, "")
            assets = AssetBundles.get
            if assets.bundle_exists?(name)
              timing_tags << "bundle:#{name}"
            end
          end
        end

        GitHub.dogstats.batch do |dogstats|
          dogstats.distribution("browser.#{entry_type}.dist.duration", timing[:duration], tags: timing_tags) if timing[:duration] > 0
          dogstats.distribution("browser.#{entry_type}.dist.transfersize", timing[:transfer_size], tags: timing_tags) if timing[:transfer_size].to_i > 0
          dogstats.distribution("browser.#{entry_type}.dist.encodedbodysize", timing[:encoded_body_size], tags: timing_tags) if timing[:encoded_body_size].to_i > 0
          dogstats.distribution("browser.#{entry_type}.dist.decodedbodysize", timing[:decoded_body_size], tags: timing_tags) if timing[:decoded_body_size].to_i > 0
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.unload", timing, :unload_event_start, :unload_event_end, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.redirect", timing, :redirect_start, :redirect_end, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.appcache", timing, :fetch_start, :domain_lookup_start, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.dns", timing, :domain_lookup_start, :domain_lookup_end, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.tcp", timing, :connect_start, :connect_end, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.request", timing, :request_start, :response_start, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.response", timing, :response_start, :response_end, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.processing", timing, :response_end, :load_event_start, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.onload", timing, :load_event_start, :load_event_end, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.dominteractive", timing, :response_end, :dom_interactive, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.domcontentloaded", timing, :dom_content_loaded_event_start, :dom_content_loaded_event_end, tags: timing_tags)
          report_timing_range_dist(dogstats, "browser.#{entry_type}.dist.domcomplete", timing, :dom_content_loaded_event_end, :dom_complete, tags: timing_tags)

          # TODO Remove when dashboards are migrated to the distribution stats above.
          dogstats.timing("browser.#{entry_type}.duration", timing[:duration], tags: timing_tags) if timing[:duration] > 0
          dogstats.timing("browser.#{entry_type}.transfersize", timing[:transfer_size], tags: timing_tags) if timing[:transfer_size].to_i > 0
          dogstats.timing("browser.#{entry_type}.encodedbodysize", timing[:encoded_body_size], tags: timing_tags) if timing[:encoded_body_size].to_i > 0
          dogstats.timing("browser.#{entry_type}.decodedbodysize", timing[:decoded_body_size], tags: timing_tags) if timing[:decoded_body_size].to_i > 0
          report_timing_range(dogstats, "browser.#{entry_type}.unload", timing, :unload_event_start, :unload_event_end, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.redirect", timing, :redirect_start, :redirect_end, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.appcache", timing, :fetch_start, :domain_lookup_start, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.dns", timing, :domain_lookup_start, :domain_lookup_end, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.tcp", timing, :connect_start, :connect_end, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.request", timing, :request_start, :response_start, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.response", timing, :response_start, :response_end, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.processing", timing, :response_end, :load_event_start, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.onload", timing, :load_event_start, :load_event_end, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.dominteractive", timing, :response_end, :dom_interactive, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.domcontentloaded", timing, :dom_content_loaded_event_start, :dom_content_loaded_event_end, tags: timing_tags)
          report_timing_range(dogstats, "browser.#{entry_type}.domcomplete", timing, :dom_content_loaded_event_end, :dom_complete, tags: timing_tags)
        end
      end
    end

    def report_timing_range_dist(dogstats, name, timing, startKey, endKey, tags:)
      return unless timing[startKey] && timing[endKey]
      ms = timing[endKey] - timing[startKey]
      return unless ms > 0
      return if ms > 1.year * 1000
      dogstats.distribution(name, ms, tags: tags)
    end

    def report_timing_range(dogstats, name, timing, startKey, endKey, tags:)
      return unless timing[startKey] && timing[endKey]
      ms = timing[endKey] - timing[startKey]
      return unless ms > 0
      return if ms > 1.year * 1000
      dogstats.timing(name, ms, tags: tags)
    end

    def report_increment_key(stat, tags:)
      return unless stat[:increment_key]
      GitHub.dogstats.increment(stat[:increment_key], tags: tags)
    end

    def report_hydro_event(stat, tags:)
      return unless stat[:hydro_event_payload]

      decoded = HydroHelper.decode_hydro_payload(
        hmac: stat[:hydro_event_hmac],
        encoded: stat[:hydro_event_payload],
      )
      event_type = decoded[:event_type]
      payload = Hash(decoded[:payload])

      if stat[:visitor_payload] && stat[:visitor_hmac]
        visitor_payload = VarnishHelper.decode_visitor_payload(
          stat[:visitor_payload], stat[:visitor_hmac]
        )

        visitor_id = visitor_payload[:visitor_id].to_i
        if visitor_id > 0
          payload[:visitor_id] = visitor_id
          payload[:client_id] = Analytics::OctolyticsId.coerce(visitor_id).unversioned
        end

        payload.merge!(
          referrer: visitor_payload[:referrer],
          originating_request_id: visitor_payload[:request_id],
        )
      end

      hydro_dogstats_tags = ["hydro_event_type:#{event_type}"]
      hydro_dogstats_tags.push("feature:#{payload[:feature_slug]}") if payload.include?(:feature_slug)

      GitHub.dogstats.increment("hydro.browser_event", tags: hydro_dogstats_tags)

      GlobalInstrumenter.instrument("browser.#{event_type}", payload.merge({
        client: {
          user: User.find_by_id(payload[:user_id]),
          timestamp: stat[:timestamp] ? stat[:timestamp] / 1000 : nil,
          context: HydroHelper.decode_hydro_client_context(stat[:hydro_client_context]),
        },
      }))
    rescue HydroHelper::InvalidPayloadError => e
      Failbot.report(e, app: "github-hydro")
    end
  end
end
