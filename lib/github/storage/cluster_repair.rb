# rubocop:disable Style/FrozenStringLiteralComment

# Queries to see which storage objects need maintenance

require "github/config/mysql"
require "github/config/redis"
require "github/slice_query"

module GitHub::Storage
  class ClusterRepair
    include SliceQuery

    def self.areas_of_responsibility
      [:data_infrastructure]
    end

    # results limit, this correlates to the number of jobs started
    def self.query_limit
      5000
    end

    def self.query_namespace
      "storage"
    end

    def self.query_name(non_voting, datacenter)
      non_voting ? "bad-non-voting-counts-#{datacenter}" : "bad-replica-counts"
    end

    def self.replica_count(non_voting, datacenter)
      non_voting ? GitHub::Storage::Allocator.non_voting_replica_count(datacenter) : GitHub.storage_replica_count.to_i
    end

    # Returns the number of objects to repair
    def self.perform(backing_up: nil, non_voting: false, datacenter: "default")
      copies = replica_count(non_voting, datacenter)
      return 0 if copies < 1
      blob_counts = get_bad_replica_counts(copies: copies, non_voting: non_voting, limit: query_limit, datacenter: datacenter)
      blob_counts.each do |(id, oid, count)|
        if count.to_i < copies
          StorageReplicateObjectJob.perform_later({"oid" => oid, "non_voting" => non_voting, "datacenter" => datacenter})
        elsif !backing_up
          StoragePurgeObjectJob.perform_later({"oid" => oid, "non_voting" => non_voting, "datacenter" => datacenter})
        end
      end
      blob_counts.size
    end

    # Performs a `ClusterRepair` operation on the voting cluster
    def self.perform_voting(backing_up: nil)
      perform(backing_up: backing_up)
    end

    # Performs a `ClusterRepair` operation on the non-voting cluster within
    # the specified datacenter
    def self.perform_non_voting(datacenter, backing_up: nil)
      perform(backing_up: backing_up, non_voting: true, datacenter: datacenter)
    end

    # Gets the list of over/under-replicated blobs within the cluster currently
    # being repaired, as well as the number of replicas on which each of those
    # blobs exist
    def self.get_bad_replica_counts(copies: nil, non_voting: false, limit: nil, slice: true, datacenter: "default")
      copies ||= replica_count(non_voting, datacenter)
      limit ||= query_limit
      key = query_name(non_voting, datacenter)

      if slice
        start_time, offset, slices = get_query_slice(key)
        offset, next_offset = get_contiguous_id_range("storage_blobs", ApplicationRecord::Domain::Storage, offset, slices)
      else
        offset = next_offset = nil
      end

      fileserver_hosts = get_fileserver_hosts(non_voting, datacenter)
      results = GitHub::Storage::Replica.get_bad_replica_counts(
        offset, next_offset, copies, fileserver_hosts, limit)

      if slice
        if results.size >= limit
          next_offset = results.map { |row| row[0] }.max
          put_adjusted_query_slice(key, start_time, offset, next_offset, slices, no_adjustment: true)
        else
          put_adjusted_query_slice(key, start_time, offset, next_offset, slices)
        end
      end

      results
    end

    # Gets the list of online hosts within the cluster currently being repaired
    def self.get_fileserver_hosts(non_voting, datacenter)
      sql = ApplicationRecord::Domain::Storage.github_sql.new(<<-SQL, non_voting: non_voting)
        SELECT fs.host
        FROM storage_file_servers fs
        WHERE fs.non_voting = :non_voting AND fs.online = 1
      SQL

      if non_voting
        sql.add "AND fs.datacenter = :datacenter", datacenter: datacenter
      end

      sql.results.flatten
    end
  end
end
