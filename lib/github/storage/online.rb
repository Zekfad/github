# rubocop:disable Style/FrozenStringLiteralComment

require "application_record/domain/storage"

module GitHub::Storage
  class Online
    def initialize(host:)
      @host = host
    end

    def perform
      sql = ApplicationRecord::Domain::Storage.github_sql.run(<<-SQL, host: @host)
        UPDATE storage_file_servers
        SET online=1
        WHERE host=:host
        LIMIT 1
      SQL

      sql.affected_rows == 1
    end
  end
end
