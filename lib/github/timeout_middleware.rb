# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class TimeoutMiddleware
    class RequestTimeout < StandardError
      attr_accessor :timeout
      attr_reader :env

      def self.is_graphql_request?(env)
        !!(env["REQUEST_PATH"] =~ ::Api::GraphQL::PATH_REGEX)
      end

      def self.for_thread(thread, timeout, env)
        if is_graphql_request?(env)
          klass = PlatformTimeout
        else
          klass = RequestTimeout
        end

        klass.new(env).tap do |ex|
          ex.set_backtrace(thread.backtrace)
          ex.timeout = timeout
        end
      end

      def initialize(env)
        @env = env
        if params = GitHub::TaggingHelper.path_parameters(env)
          super("#{GitHub::TaggingHelper.category(env)}:#{params[:controller]}##{params[:action]}")
        elsif params = GitHub::TaggingHelper.api_parameters(env)
          super("#{params[:app]} #{params[:route]}")
        end
      end

      def notify_watchers
        GitHub::TimeoutMiddleware.watchers(env).each { |watcher| watcher.timeout(env) }
      end

      def report
        report_to_failbot
        report_pull_request_timeout
        log_request
        record_stats
        notify_watchers
        report_to_hydro
      end

      # RequestTimeout can sometimes trigger inside code that isn't designed to
      # gracefully handle being interrupted. This can cause corruption of state
      # that carries through to subsequent requests.
      #
      # Because it may not be safe to process more requests in this worker
      # process, we just report the timeout to Haystack and die.
      def report_and_die!
        report
      ensure
        exit!
      end

      # TimeoutMiddleware exits before HydroMiddleware has a chance to finalize
      # and publish the request payload, so we force HydroMiddleware to publish
      # the incomplete payload here.
      def report_to_hydro
        return unless GitHub.hydro_enabled? && !env[GitHub::HydroMiddleware::PAYLOAD].nil?

        env[GitHub::HydroMiddleware::PAYLOAD].merge!({
          timed_out: true,
          request_category: category,
          current_user: GitHub.context[:actor],
          current_user_id: GitHub.context[:actor_id],
        })

        GitHub::HydroMiddleware.record_request(env)
        GitHub.close_hydro(timeout: 1)
      end

      private

      def filtered_request_body
        body = GitHub.context[:filtered_request_body]
        return nil unless body.present?
        body.squish
      end

      def failbot_context
        context = {
          app: "github-timeout",
          request_body: filtered_request_body,
          request_timeout: timeout,
          request_timer_events: GitHub.request_timer.formatted_stats,
          rollup: Digest::MD5.hexdigest(message),
        }
        if singleton = env[Rack::ProcessUtilization::ENV_KEY]
          singleton.stats_for_failbot_context.merge(context)
        else
          context
        end
      end

      def report_to_failbot
        Failbot.report(self, failbot_context)
      end

      def log_request
        Rack::RequestLogger.log(env, 500, {}, timeout, timeout: true)
      end

      def category
        @category ||= GitHub::TaggingHelper.category(env)
      end

      def record_stats
        GitHub.stats.increment("unicorn.#{category}.request_timeout") if GitHub.enterprise?
        GitHub.stats.increment("unicorn.#{GitHub.rails_version_key}.#{category}.request_timeout") if GitHub.enterprise?

        base_tags = ["rails_version:#{GitHub.rails_version_key}", "request_category:#{category}"]
        GitHub.dogstats.increment("request.timeout.count", tags: base_tags)

        if (params = GitHub::TaggingHelper.path_parameters(env)) && params[:controller]
          # Controller names can contain characters like slashes that don't
          # work well in metric names, so we #parameterize them first. See
          # https://github.com/github/github/issues/22444.
          controller = params[:controller].parameterize
          action = params[:action].parameterize
          ["rails", GitHub.rails_version_key].each do |rails|
            GitHub.dogstats.increment("request_timeout", tags: ["rails:#{rails}", "category:#{category}", "controller:#{controller}", "action:#{action}"])
          end
        end

        if controller = GitHub::TaggingHelper.controller(env)
          controller_tags = base_tags + ["controller:#{controller}"]
          GitHub.dogstats.increment("request.timeout.by_controller.count", tags: controller_tags)

          if action = GitHub::TaggingHelper.action(env)
            action_tags = base_tags + ["action:#{action}"]
            GitHub.dogstats.increment("request.timeout.controller.#{controller}.by_action.count", tags: action_tags)
          end
        end

        # Flush stats before quitting
        GitHub.stats.flush_all if GitHub.enterprise?
      end

      def report_pull_request_timeout
        params = GitHub::TaggingHelper.path_parameters(env)
        return unless params
        return unless params[:controller] == "pull_requests" && params[:action] == "show"
        reason = env["pull_request.timeout_reason"] || "other"
        exception_class = "PullRequestTimeout::#{reason.classify}"
        Failbot.report_trace(self, failbot_context.merge({
          class: exception_class,
          rollup: Digest::MD5.hexdigest(exception_class),
        }))
      end
    end

    class PlatformTimeout < RequestTimeout; end

    attr_reader :app

    WATCHERS = "github.timeout_watchers".freeze

    # Internal:
    def self.watchers(env)
      env[WATCHERS] ||= []
    end

    # Public:
    def self.notify(env, watcher)
      watchers(env) << watcher
    end

    def initialize(app)
      @app = app
    end

    def call(env)
      request_thread = Thread.current

      timer_thread = Thread.new do
        timeout = GitHub.request_timeout(env)
        sleep(timeout)
        exception = RequestTimeout.for_thread(request_thread, timeout, env)
        exception.report_and_die!
      end

      app.call(env)
    ensure
      timer_thread.kill
    end
  end
end
