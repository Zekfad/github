# frozen_string_literal: true

require "github/sql"

module GitHub
  class SQL
    class Subscriber
      # Private: The regex to match select queries.
      SELECT_REGEXP = /\Aselect.*?from\s*`?([\w$]+)`?\.?`?([\w$]+)?`?/mi.freeze

      # Private: These all need to be liberal about allowing keywords like
      # LOW_PRIORITY, IGNORE, QUICK, etc. in between the verb and the
      # other keywords.
      INSERT_REGEXP = /\Ainsert(?:\W+\w+)*?\W+into\W+`?([\w$]+)`?\.?`?([\w$]+)?/mi.freeze
      UPDATE_REGEXP = /\Aupdate(?:\W+\w+)*?\W+([\w$]+)\W+set/mi.freeze
      DELETE_REGEXP = /\Adelete(?:\W+\w+)*?\W+from\W+`?([\w$]+)`?\.?`?([\w$]+)?/mi.freeze
      REPLACE_REGEXP = /\Areplace(?:\W+\w+)*?\W+into\W+`?([\w$]+)`?\.?`?([\w$]+)?/mi.freeze

      def self.call(sql, connection_info, time_span:)
        return false if sql.nil?

        table = nil
        operation_key = case sql.b
        when SELECT_REGEXP
          table = $2 ? $2 : $1
          :select
        when UPDATE_REGEXP
          table = $1
          :update
        when INSERT_REGEXP
          table = $2 ? $2 : $1
          :insert
        when DELETE_REGEXP
          table = $2 ? $2 : $1
          :delete
        when REPLACE_REGEXP
          table = $2 ? $2 : $1
          :replace
        else
          return false
        end

        if table
          table = table.to_s.downcase
        end

        params = {operation: operation_key, table: table, connection_info: connection_info}

        record_stats **params.merge(duration: time_span.duration)
        record_trace **params.merge(time_span: time_span)

        true
      end

      def self.record_stats(operation:, duration:, connection_info:, table:)
        sampler = DynSampler.get_sampler("rpc.mysql.time")
        sample_rate = sampler.sample_rate([operation, connection_info.config_host || :unknown])
        site = GitHub.site_from_host(connection_info.host)

        datadog_tags = [
          "rpc_operation:#{operation}",
          "rpc_site:#{site}",
          "rpc_host:#{connection_info.config_host || :unknown}",
        ]

        GitHub.dogstats.timing("rpc.mysql.time", duration,
                               sample_rate: sample_rate,
                               tags: datadog_tags)

        datadog_tags << "mysql_table:#{table}" if table

        GitHub.dogstats.distribution("rpc.mysql.dist.time", duration, tags: datadog_tags)
      end

      def self.record_trace(operation:, table:, connection_info:, time_span:)
        span = GitHub.tracer.start_span "rpc.mysql.#{operation}.#{table}",
                                        start_time: time_span.starting,
                                        enable: GitHub.verbose_tracing?
        span.set_tag("component", "mysql")
        span.set_tag("rpc.mysql.table", table) if table
        span.set_tag("rpc.operation", operation) if operation
        span.set_tag("rpc.host", connection_info.config_host) if connection_info.config_host
        span.finish(end_time: time_span.ending)
      end
    end
  end
end
