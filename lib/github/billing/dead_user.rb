# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Billing
    # Quacks like a User, if the user isn't around anymore
    class DeadUser
      attr_accessor :braintree_customer_id, :id, :login, :billing_email,
        :billing_extra, :created_at, :user_type, :vat_code

      alias :email :billing_email
      alias :to_s :login

      def initialize
        yield self if block_given?
      end

      def organization?
        user_type == "organization"
      end

      def user?
        user_type == "user"
      end

      def type
        user_type
      end

      def billing_users
        [self]
      end

      def plan
        nil
      end
    end
  end
end
