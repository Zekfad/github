# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Billing
    class Reporting
      # Given an S3 key and bucket name, prepare a temporary CSV handle for writing,
      # yield to a block with that handle (assuming the block will write the file's contents).
      # Then, clean up temporary state, and upload the file to S3 with the given bucket and key.
      #
      # key - what the file's name will be on S3 (can include subdirectories)
      # s3_bucket - the bucket to upload to
      def self.create_and_upload_csv(key, s3_bucket = nil)
        # Write CSV to a temporary file.
        file = Tempfile.new("transactions-csv")
        csv = GitHub::CSV.new(file, **csv_options)

        yield csv

        csv.close
        file.close

        s3_bucket ||= default_s3_bucket
        object = Aws::S3::Resource.new(client: GitHub.s3_primary_client).bucket(s3_bucket).object(key)
        object.put(body: File.read(file.path), content_type: "text/csv")

        # Remove temp file.
        file.unlink

        key
      end

      def self.csv_options
        {}
      end

      def self.default_s3_bucket
        ENV["TRANSACTIONS_CSV_S3_BUCKET"] || GitHub.s3_configs[Rails.env]["transactions_csv_bucket"]
      end
    end
  end
end
