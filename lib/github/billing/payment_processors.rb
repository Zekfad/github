# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Billing
  module PaymentProcessors

    # Public: Factory to create a specific PaymentProcessor based on known
    # types.
    #
    # type        - String type to create. Registered types are:
    #               - "braintree"
    #               - "zuora"
    # customer_id - String customer id that maps to the external payment
    #               processors system.
    #
    # gateway - Billing::Zuora::PaymentGateway only required when type is "zuora".
    #           Used to determine which gateway to assign when updating payment details to Zuora
    #
    # Returns a subclass of PaymentProcessor.
    def self.create(type, customer_id, gateway: nil)
      case type
      when "braintree"
        BraintreeProcessor.new(customer_id)
      when "zuora"
        ZuoraProcessor.new(customer_id, gateway: gateway)
      end
    end
  end
end
