# frozen_string_literal: true

class GitHub::Billing::ZuoraProduct
  include ZuoraSettings

  attr_reader :charges, :custom_product_params, :product_key, :product_name, :product_type

  def self.sync_github_products
    GitHub::Plan.all.select(&:paid?).each do |plan|
      uuids_exist = plan.product_uuid(User::MONTHLY_PLAN) && plan.product_uuid(User::YEARLY_PLAN)
      unless uuids_exist
        plan.sync_to_zuora
      end
    end

    Asset::Status.sync_to_zuora
    Billing::Actions::ZuoraProduct.sync_to_zuora
    Billing::PackageRegistry::ZuoraProduct.sync_to_zuora
    Billing::SharedStorage::ZuoraProduct.sync_to_zuora
    Billing::Codespaces::ZuoraProduct.sync_to_zuora
  end

  def self.create(**options); new(**options).create; end

  def initialize(charges:, product_name:, product_type:, product_key:, custom_product_params: {})
    @charges = charges
    @custom_product_params = custom_product_params
    @product_key = product_key
    @product_name = product_name
    @product_type = product_type
  end

  # Public: Creates ProductUUID records for each type of billing cycle for a given product offering
  #
  # Reuses an existing Zuora Product if one already exists for one of the billing cycles, and doesn't create
  # duplicate records for a given product/cycle. Creates the Product, ProductRatePlan, and
  # ProductRatePlanCharges needed to represent a product offering in Zuora.
  #
  # Returns Array
  def create
    return unless GitHub.billing_enabled?
    product_id = existing_product_id || create_product

    User::PLAN_DURATIONS.each do |billing_cycle|
      next if ::Billing::ProductUUID.find_by(product_type: product_type, product_key: product_key, billing_cycle: billing_cycle)
      product_rate = GitHub.zuorest_client.create_product_rate_plan\
        zuora_product_rate_plan_params(billing_cycle, product_id)
      product_charges = GitHub.zuorest_client.create_action(
        zuora_product_rate_plan_charge_params(billing_cycle, product_rate["Id"]), custom_charge_headers)

      ::Billing::ProductUUID.create \
        product_type: product_type,
        product_key: product_key,
        billing_cycle: billing_cycle,
        zuora_product_id: product_id,
        zuora_product_rate_plan_id: product_rate["Id"],
        zuora_product_rate_plan_charge_ids: convert_to_charge_ids(product_charges)
    end
  end

  private

  def create_product
    GitHub.zuorest_client.create_product(zuora_product_params)["Id"]
  end

  # Private: Add custom headers to a request
  #
  # Returns hash
  def custom_charge_headers
    if charges.any? { |charge| charge[:type].to_s.include?("discount") }
      { "X-Zuora-WSDL-Version" => DISCOUNT_WSDL_VERSION.to_s }
    else
      {}
    end
  end

  # Private: Check if a product already exists from a manual import or previously failed sync
  #
  # Returns String
  def existing_product_id
    ::Billing::ProductUUID.find_by(product_type: product_type, product_key: product_key)&.zuora_product_id
  end

  def zuora_product_params
    {
      Name: product_name,
      EffectiveStartDate: GitHub::Billing.today.to_s,
      EffectiveEndDate: EFFECTIVE_END_DATE,
    }.merge(custom_product_params)
  end

  def zuora_product_rate_plan_params(billing_cycle, product_id)
    {
      EffectiveStartDate: GitHub::Billing.today.to_s,
      EffectiveEndDate: EFFECTIVE_END_DATE,
      Name: "#{product_name} - #{billing_cycle}",
      ProductId: product_id,
    }
  end

  def zuora_product_rate_plan_charge_params(billing_cycle, rate_plan_id)
    defaults = default_charge_params(billing_cycle, rate_plan_id)
    {
      type: "ProductRatePlanCharge",
      objects: charge_objects(billing_cycle).map { |obj| obj.merge(defaults) },
    }
  end

  # Private: Provides the default parameters required to create each ProductRatePlanCharge in Zuora
  #
  # billing_cycle: ["year", "month"]
  # rate_plan_id: "productRatePlanId"
  #
  # Returns Hash
  def default_charge_params(billing_cycle, rate_plan_id)
    billing_period = billing_cycle == User::YEARLY_PLAN ? "Annual" : "Month"
    {
      BillingPeriod: billing_period,
      ChargeType: "Recurring",
      DeferredRevenueAccount: DEFERRED_REVENUE_ACCOUNT,
      Name: "#{product_name} - #{billing_period}",
      ProductRatePlanId: rate_plan_id,
      RecognizedRevenueAccount: RECOGNIZED_REVENUE_ACCOUNT,
      TaxCode: TAX_CODE,
      TaxMode: "TaxExclusive",
      Taxable: false,
      TriggerEvent: "ContractEffective",
    }
  end

  # Private: Converts the simplified charge hash into the version Zuora expects for a flat fee or per unit
  # type of charge
  #
  # Returns Array
  def charge_objects(billing_cycle)
    charges.map do |charge|
      if charge[:type].to_s.include?("discount")
        discount_charge(charge, billing_cycle.to_sym)
      elsif charge[:type].to_s.include?("unit")
        per_unit_charge(charge, billing_cycle.to_sym)
      else
        flat_fee_charge(charge, billing_cycle.to_sym)
      end
    end
  end

  # Private: Provides the parameters required to create a Discount in Zuora
  #
  # Returns Hash
  def discount_charge(charge, billing_cycle)
    {
      ChargeModel: discount_charge_model(charge[:type]),
      ListPrice: charge[:prices][billing_cycle],
      ApplyDiscountTo: "RECURRING",
      DiscountLevel: "subscription",
      ProductDiscountApplyDetailData: {
        ProductDiscountApplyDetail: applied_product_rate_plan_ids(billing_cycle),
      },
      ProductRatePlanChargeTierData: {
        ProductRatePlanChargeTier: [
          {
            discount_rate_plan_charge_tier(charge[:type]) => charge[:prices][billing_cycle],
          },
        ],
      },
    }
  end

  # Private: Zuora products that correspond to GitHub products (i.e. excluding Marketplace) and for the listed billing cycle. Excludes other discounts.
  #
  # Returns ActiveRecord::Relation
  def github_products(billing_cycle)
    ::Billing::ProductUUID.github_products.where(billing_cycle: billing_cycle)
  end

  # Private: Zuora rate plan ids of products we should be applying discounts to
  # (i.e. GitHub Products)
  #
  # Returns Array
  def applied_product_rate_plan_ids(billing_cycle)
    github_products(billing_cycle).map do |product|
      {
        AppliedProductRatePlanId: product.zuora_product_rate_plan_id,
      }
    end
  end

  def discount_charge_model(type)
    type == :percentage_discount ? "Discount-Percentage" : "Discount-Fixed Amount"
  end

  def discount_rate_plan_charge_tier(type)
    type == :percentage_discount ? :DiscountPercentage : :DiscountAmount
  end

  def flat_fee_charge(charge, billing_cycle)
    {
      ChargeModel: "Flat Fee Pricing",
      ListPrice: charge[:prices][billing_cycle],
      ProductRatePlanChargeTierData: {
        ProductRatePlanChargeTier: [
          {
            Currency: "USD",
            Price: charge[:prices][billing_cycle],
            PriceFormat: "Flat Fee",
          },
        ],
      },
    }
  end

  def per_unit_charge(charge, billing_cycle)
    {
      ChargeModel: "Per Unit Pricing",
      DefaultQuantity: charge[:default_quantity] || 0,
      ListPrice: charge[:prices][billing_cycle],
      UOM: charge[:unit],
      ProductRatePlanChargeTierData: {
        ProductRatePlanChargeTier: [
          {
            Currency: "USD",
            Price: charge[:prices][billing_cycle],
            PriceFormat: "Per Unit",
          },
        ],
      },
    }
  end

  # Private: Converts the ProductRatePlanChargeIds from Zuora into a hash for storing on the ProductUUID
  # record, e.g.
  #
  # { base_unit: "ChargeId", unit: "ChargeId" }
  #
  # product_charges: [{"Id" => "ChargeId"}, {"Id" => "ChargeId"}] - provided by Zuora API
  #
  # Returns Hash
  def convert_to_charge_ids(product_charges)
    charges.each_with_index.each_with_object({}) do |(charge, index), charge_hash|
      charge_hash[charge[:type]] = product_charges[index]["Id"]
    end
  end
end
