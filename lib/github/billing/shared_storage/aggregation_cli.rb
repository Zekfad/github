# frozen_string_literal: true

require "thor"

module GitHub
  module Billing
    module SharedStorage
      class AggregationCLI < Thor
        include ActionView::Helpers::DateHelper

        desc "status", "Get the status of Shared Storage Aggregation"
        def status
          say ""
          say "Shared Storage Aggregation"
          say "--------------------------"
          say ""

          say "Scheduled aggregation status: "

          if scheduled_aggregation_enabled?
            say "enabled", :green
          else
            say "disabled", :red
          end

          say ""
          if next_run_time
            say "Next run: #{next_run_time} "
            say "(#{time_ago_in_words(next_run_time)} ago)"
          else
            say "Next run: "
            say "unknown", :yellow
          end
        end

        desc "advance", "Perform Aggregation for the next hour"
        def advance
          say ""
          say "Shared Storage Aggregation"
          say "--------------------------"
          say ""

          if scheduled_aggregation_enabled?
            say "ERROR: ", :red
            say "Scheduled aggregation is currently enabled; cannot continue"
            return
          end

          cutoff = next_run_time || current_hour
          if cutoff.future?
            say "ERROR: ", :red
            say "Next run is in the future; cannot continue"
            return
          end

          say "You are about to perform aggregation for events up to: "
          say cutoff, :yellow

          unless yes?("Continue?")
            return say "Aborting"
          end

          ::Billing::SharedStorage::FanoutAggregationJob.perform_later(cutoff)

          next_run = cutoff + 1.hour
          GitHub.kv.set(kv_key, next_run.to_i.to_s)

          say "FanoutAggregationJob enqueued!", :green
          say ""
          say "Next run: #{next_run}"
        end

        private

        def scheduled_aggregation_enabled?
          GitHub.flipper[:shared_storage_aggregation_job].enabled?
        end

        def kv_key
          ::Billing::SharedStorage::ArtifactEventAggregationStart::NEXT_RUN_KV_KEY
        end

        def next_run_time
          timestamp = GitHub.kv.get(kv_key).value!
          Time.at(timestamp.to_i) if timestamp
        end

        def current_hour
          Time.now.beginning_of_hour
        end
      end
    end
  end
end
