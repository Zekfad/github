# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    class EnterpriseSubscriptionSynchronizer
      def initialize(subscription)
        @subscription = subscription
      end

      def sync
        ActiveRecord::Base.transaction do
          Billing::PrepaidMeteredUsageRefill.transaction do
            if business.customer.nil?
              Billing::CreateCustomer.perform(business)
              business.reload
            end

            sync_business_attributes
            remove_duplicate_zuora_information_from_organizations
            sync_customer_attributes
            sync_sales_serve_plan_subscription
            sync_usage_refills

            ::Billing::UpdateSkippedMeteredLineItemsJob.perform_later(billable_owner: business)
          end
        end
      end

      private

      delegate :customer, to: :business, private: true

      attr_reader :subscription

      def business
        subscription.owner
      end

      def sync_business_attributes
        old_seats = business.seats
        business.update(seats: subscription.seats || 0)
        business.track_seat_change(old_seats)
        sync_data_packs(business.organizations.first) if business.organizations.any?
      end

      def sync_customer_attributes
        customer.update(
          zuora_account_id: subscription.account_id,
          zuora_account_number: subscription.account_number,
          billing_end_date: subscription_end_date,
        )
      end

      def remove_duplicate_zuora_information_from_organizations
        existing_customer = Customer.find_by(zuora_account_number: subscription.account_number)

        if existing_customer.present?
          if business.organizations.exists?(id: existing_customer.organizations.ids)
            existing_customer.update(zuora_account_number: nil, zuora_account_id: nil)
          end
        end
      end

      def sync_sales_serve_plan_subscription
        sales_serve_plan_subscription = customer.sales_serve_plan_subscription || customer.build_sales_serve_plan_subscription

        sales_serve_plan_subscription.update!(
          zuora_subscription_id: subscription.id,
          zuora_subscription_number: subscription.subscription_number,
          billing_start_date: subscription.term_start_date,
          zuora_rate_plan_charges: subscription.active_rate_plan_charges,
        )
      end

      def sync_data_packs(organization)
        packs = subscription.data_packs || 0
        set_data_packs(organization, packs)
      end

      def set_data_packs(organization, packs)
        organization.build_asset_status!
        organization.asset_status.update_data_packs(
          quantity: packs,
          actor: organization,
          force: true,
        )
      end

      # Internal: Given a subscription, determine the prepaid usage refills on the subscription
      # Also enqueues a job to refresh the metered billing configuration, even if there are no
      # refills in Zuora, to handle expiration of refills on old subscriptions.
      #
      # subscription - Zuorest::Model::Subscription
      # default - Integer
      #
      # Returns nothing
      def sync_usage_refills
        subscription.usage_refill_rate_plan_charges.each do |rate_plan_charge|
          if Billing::PrepaidMeteredUsageRefill.exists?(zuora_rate_plan_charge_number: rate_plan_charge[:number])
            unless rate_plan_charge.active?
              refill = Billing::PrepaidMeteredUsageRefill.find_by(zuora_rate_plan_charge_number: rate_plan_charge.number)
              refill&.destroy
            end
          else
            if rate_plan_charge.active?
              Billing::PrepaidMeteredUsageRefillCreator.new(
                owner: business,
                zuora_rate_plan_charge_id: rate_plan_charge[:id],
                zuora_rate_plan_charge_number: rate_plan_charge[:number],
                expires_on: subscription_end_date,
                amount_in_subunits: rate_plan_charge[:price] * rate_plan_charge[:quantity] * 100,
                currency_code: rate_plan_charge[:currency],
              ).create!
            end
          end
        end

        if Billing::PrepaidMeteredUsageRefill.enabled_for?(business)
          Billing::InvoicedMeteredBillingConfigurationUpdateJob.perform_later(business)
        end
      end

      # Internal: Get the end date of the subscription. Unfortunately the
      # `termEndDate` field from Zuora is actually the start of the next
      # period so we need to subtract a day.
      #
      # Returns Date the end date of the subscription
      def subscription_end_date
        @subscription_end_date ||= subscription.term_end_date - 1.day
      end
    end
  end
end
