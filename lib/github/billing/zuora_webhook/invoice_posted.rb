# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Handler for InvoicePosted webhooks from Zuora
    class InvoicePosted
      include GitHubDataAccess
      include ZuoraDataAccess

      attr_reader :account_id, :billing_transaction, :invoice_id

      # Public: Handle a InvoicePosted webhook from Zuora
      #
      # webhook - A ZuoraWebhook object
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initialize a new InvoicePosted webhook handler
      #
      # webhook - A ZuoraWebhook object
      def initialize(webhook)
        @account_id = webhook.account_id
        @invoice_id = webhook.invoice_id
      end

      # Public: Handle a InvoicePosted webhook from Zuora
      #
      # Returns nothing
      def perform
        # Do nothing for non-zero invoices
        return unless zuora_invoice.amount.zero?

        # Do nothing if the user isn't found (i.e. a deleted user)
        return unless plan_subscription

        # cover scenario for apple in app purchases
        if plan_subscription.apple_iap_subscription?
          reset_billing_status # reset to move billing cycle forward
          return
        end

        # Do nothing if we already have a zero-dollar BillingTransaction for this invoice date
        return if account.billing_transactions.where(amount_in_cents: 0).where("DATE(created_at) = ?", invoice_date&.to_date).any?

        if has_plan_attached?
          if account.dunning? && zuora_balance.positive?
            plan_subscription.update(balance_in_cents: zuora_balance.cents)
          else
            reset_billing_status
          end

          create_billing_transaction
        end
      end

      private

      # Internal: Checks if the `plan_subscription` has an associated plan
      # on Zuora.
      #
      # Returns boolean
      def has_plan_attached?
        # The account already has a Zuora subscription
        return true if plan_subscription.zuora_subscription_number?

        # Try to synchronize and check if that attached an orphan or created a new Zuora subscription
        plan_subscription.synchronize_with_lock
        plan_subscription.reload.zuora_subscription_number
      end

      # Internal: Reset the user's billing for a new cycle
      #
      # Returns nothing
      def reset_billing_status
        ::Billing::PlanSubscription::ResetBillingStatus.perform(
          plan_subscription,
          balance: zuora_balance.cents,
          next_billing_date: zuora_subscription.charged_through_date.to_s,
        )
      end

      # Internal: Create or update a BillingTransaction for this $0 charge
      #
      # Returns nothing
      def create_billing_transaction
        @billing_transaction = account.billing_transactions.new(
          amount_in_cents: 0,
          created_at: invoice_date,
          platform: :zuora,
          payment_type: :no_charge,
          service_ends_at: zuora_subscription.charged_through_date.to_s,
        )
        @billing_transaction.log_zero_charge(user: account)
      end

      # Internal: The Zuora invoice date in the GitHub Billing timezone
      #
      # Returns ActiveSupport::TimeWithZone
      def invoice_date
        GitHub::Billing.timezone.parse(zuora_invoice.invoice_date)
      end

      def zuora_balance
        @zuora_balance ||= ::Billing::Money.new(zuora_account["metrics"]["balance"] * 100)
      end
    end
  end
end
