# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Handler for PaymentDeclined webhooks from Zuora
    class PaymentDeclined
      include GitHubDataAccess
      include HydroInstrumentation
      include ZuoraDataAccess

      attr_reader :account_id, :payment_id

      # Public: Handle a PaymentDeclined webhook from Zuora
      #
      # webhook - A ZuoraWebhook object
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initialize a new PaymentDeclined webhook handler
      #
      # webhook - A ZuoraWebhook object
      def initialize(webhook)
        @account_id = webhook.account_id
        @payment_id = webhook.payment_id
      end

      # Public: Handle a PaymentDeclined webhook from Zuora
      #
      # Returns nothing
      def perform
        return unless plan_subscription

        transaction_id = zuora_payment.reference_id || zuora_payment.payment_number
        return if account.billing_transactions.where(transaction_id: transaction_id).any?

        GitHub.dogstats.increment("zuora.payment.count", tags: datadog_tags)
        GitHub.dogstats.count("zuora.payment.amount_in_cents", zuora_payment.amount_in_cents, tags: datadog_tags)

        create_billing_transaction
        update_billing_attempts
        dun_subscription
        instrument_payment_transaction(success: false)
      end

      private

      # Internal: Create or update a BillingTransaction for this payment
      #
      # Returns nothing
      def create_billing_transaction
        ::Billing::PlanSubscription::CreateBillingTransaction.perform \
          plan_subscription,
          service_ends_at: zuora_subscription&.charged_through_date || GitHub::Billing.today,
          zuora_transaction: zuora_payment
      end

      # Internal: Increment the user's billing attempts
      #
      # Returns nothing
      def update_billing_attempts
        User.increment_counter(:billing_attempts, account.id)
        account.reload
      end

      # Internal: Moves the account into dunning
      #
      # Returns nothing
      def dun_subscription
        ::Billing::DunSubscription.perform(account)
      end

      # Internal: The tags to use for Datadog metrics
      #
      # Returns Array
      def datadog_tags
        [
          "gateway:#{zuora_payment.gateway.parameterize}",
          "is_retry:#{zuora_payment.is_retry?}",
          "authorization:declined",
        ]
      end
    end
  end
end
