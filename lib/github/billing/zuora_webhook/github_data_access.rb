# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Common methods for accessing GitHub data in Zuora webhooks
    module GitHubDataAccess
      # Internal: The GitHub user associated with the Zuora customer
      #
      # Returns User|Organization
      def account
        return @account if defined?(@account)
        @account = plan_subscription&.user
      end

      # Internal: The customer referenced in this webhook
      #
      # Returns Customer
      def customer
        return @customer if defined?(@customer)
        @customer = plan_subscription&.customer
      end

      # Internal: The user's payment method
      #
      # Returns PaymentMethod
      def payment_method
        @payment_method ||= account.payment_method
      end

      # Internal: The user's PlanSubscription
      #
      # Returns Billing::PlanSubscription
      def plan_subscription
        @plan_subscription ||= ::Billing::PlanSubscription
          .joins(:customer)
          .find_by(customers: { zuora_account_id: account_id })
      end
    end
  end
end
