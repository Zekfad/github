# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Common methods for Hydro instrumentation in Zuora webhooks
    module HydroInstrumentation
      # Internal: Instrument the payment transaction so that it can be logged
      # in Hydro
      #
      # success - Whether or not this transaction was successful
      #
      # Returns nothing
      def instrument_payment_transaction(success:)
        payload = {
          account_id: account.id,
          payment_method_id: payment_method.id,
          success: success,
          payment_amount: zuora_payment.amount_in_cents,
          attempt_number: zuora_payment.num_consecutive_failures + 1,
          processor_response_code: zuora_payment.processor_response_code,
          processor_response: zuora_payment.processor_response,
        }
        GlobalInstrumenter.instrument("billing.payment_transaction", payload)
      end
    end
  end
end
