# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    class InvoicedOrganizationSubscriptionSynchronizer
      def initialize(subscription)
        @subscription = subscription
      end

      def sync
        ActiveRecord::Base.transaction do
          Billing::PrepaidMeteredUsageRefill.transaction do
            sync_organization_attributes
            sync_data_packs
            sync_zuora_information
            organization.expire_active_coupon
            sync_usage_refills

            ::Billing::UpdateSkippedMeteredLineItemsJob.perform_later(billable_owner: organization)
          end
        end
      end

      private

      attr_reader :subscription

      def organization
        subscription.owner
      end

      def sync_organization_attributes
        old_seats = organization.seats
        old_plan = organization.plan
        old_plan_duration = organization.plan_duration
        old_billing_type = organization.billing_type

        organization.seats = subscription.seats || 0
        organization.plan = subscription.plan || old_plan
        organization.billed_on = subscription.term_end_date.to_date
        organization.disabled = false
        organization.switch_billing_type_to_invoice(organization)

        organization.save!

        GitHub.instrument(
          "billing.change_billing_type",
          old_billing_type: old_billing_type,
          billing_type: organization.billing_type,
          user: organization,
          actor: organization,
          actor_id: organization.id,
        )

        organization.track_seat_change(organization, old_seats: old_seats)
        organization.track_plan_change(organization, old_plan)
        organization.track_plan_duration_change(organization, old_plan_duration)
      end

      def sync_data_packs
        packs = subscription.data_packs || 0
        set_data_packs(packs)
      end

      def set_data_packs(packs)
        organization.build_asset_status!
        organization.asset_status.update_data_packs(
          quantity: packs,
          actor: organization,
          force: true,
        )
      end

      def sync_zuora_information
        customer = organization.customer || Billing::CreateCustomer.perform(organization).customer
        customer.zuora_account_id = subscription.account_id
        customer.zuora_account_number = subscription.account_number
        customer.billing_end_date = subscription.term_end_date
        customer.save!

        plan_subscription = organization.plan_subscription || Billing::PlanSubscription.new(user: organization, customer: customer)
        term_start_date = subscription.term_start_date

        unless term_start_date.blank?
          plan_subscription.billing_start_date = term_start_date
        end

        plan_subscription.zuora_subscription_id = subscription.id
        plan_subscription.zuora_subscription_number = subscription.subscription_number
        plan_subscription.zuora_rate_plan_charges = subscription.active_rate_plan_charges
        plan_subscription.save!
      end

      # Internal: Given a subscription, determine the prepaid usage refills on the subscription
      # Also enqueues a job to refresh the metered billing configuration, even if there are no
      # refills in Zuora, to handle expiration of refills on old subscriptions.
      #
      # subscription - Zuorest::Model::Subscription
      # default - Integer
      #
      # Returns nothing
      def sync_usage_refills
        subscription.usage_refill_rate_plan_charges.each do |rate_plan_charge|
          if Billing::PrepaidMeteredUsageRefill.exists?(zuora_rate_plan_charge_number: rate_plan_charge[:number])
            unless rate_plan_charge.active?
              refill = Billing::PrepaidMeteredUsageRefill.find_by(zuora_rate_plan_charge_number: rate_plan_charge.number)
              refill&.destroy
            end
          else
            if rate_plan_charge.active?
              Billing::PrepaidMeteredUsageRefillCreator.new(
                owner: organization,
                zuora_rate_plan_charge_id: rate_plan_charge[:id],
                zuora_rate_plan_charge_number: rate_plan_charge[:number],
                expires_on: organization.next_billing_date,
                amount_in_subunits: rate_plan_charge[:price] * rate_plan_charge[:quantity] * 100,
                currency_code: rate_plan_charge[:currency],
              ).create!
            end
          end
        end

        Billing::InvoicedMeteredBillingConfigurationUpdateJob.perform_later(organization)
      end
    end
  end
end
