# frozen_string_literal: true

module GitHub::Billing
  module StripeWebhook
    class ChargeDispute
      attr_reader :dispute, :event, :webhook, :stripe_dispute

      # Public: Handle the charge dispute webhook payload
      #
      # webhook - The Billing::StripeWebhook to process
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initializes a new ChargeDispute webhook handler
      #
      # webhook - The Billing::StripeWebhook to process
      def initialize(webhook)
        @webhook = webhook
        @event = ::Stripe::Event.construct_from(webhook.payload)
        @stripe_dispute = event.data.object
      end

      # Public: Handle the charge dispute webhook payload
      #
      # Returns nothing
      def perform
        Billing::BillingTransaction.retry_on_find_or_create_error do
          @dispute = disputed_transaction.disputes.find_or_initialize_by(
            platform: :stripe,
            platform_dispute_id: stripe_dispute.id,
          )

          return if webhook_older_than_latest_update?
          dispute.created_at = Time.at(stripe_dispute.created) if dispute.new_record?

          dispute.update!(
            amount_in_subunits: stripe_dispute.amount,
            currency_code: stripe_dispute.currency.upcase,
            reason: stripe_dispute.reason,
            status: stripe_dispute.status,
            refundable: stripe_dispute.is_charge_refundable,
            response_due_by: Time.at(stripe_dispute.evidence_details.due_by),
            user_id: disputed_transaction.user_id,
            updated_at: Time.at(event.created),
          )
        end
      end

      private

      # Returns Billing::BillingTransaction
      # Raises ActiveRecord::RecordNotFound
      def disputed_transaction
        @disputed_transaction ||= Billing::BillingTransaction.find_by!(
          transaction_id: stripe_dispute.charge,
        )
      end

      # Checks if the webhook event is older than the most recent update in order
      # to prevent saving outdated information if webhooks are processed out of
      # order. Each webhook will have the most recent version of the Stripe Dispute
      # object serialized, so we don't want to update our copy with data that is
      # out-of-data.
      #
      # Returns Boolean
      def webhook_older_than_latest_update?
        return false if dispute.new_record?

        Time.at(event.created) < dispute.updated_at
      end
    end
  end
end
