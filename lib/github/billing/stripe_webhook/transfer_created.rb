# frozen_string_literal: true

module GitHub::Billing
  module StripeWebhook
    class TransferCreated
      attr_reader :transfer

      # Public: Handle the transfer created webhook payload
      #
      # webhook - The Billing::StripeWebhook to process
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initializes a new TransferCreated webhook handler
      #
      # webhook - The Billing::StripeWebhook to process
      def initialize(webhook)
        @webhook = webhook
        @event = ::Stripe::Event.construct_from(webhook.payload)
        @transfer = @event.data.object
      end

      # Public: Handle the transfer created webhook payload
      #
      # Returns nothing
      def perform
        instrument_transfer
        transaction = Billing::PayoutsLedgerTransaction.new(stripe_connect_account)

        listing = nil
        exceeded_match_limit_before = false

        transaction.add_entry(
          transaction_type: :payment,
          amount_in_subunits: (-1 * payment_amount),
          currency_code: transfer.currency.upcase,
          primary_reference_id: zuora_transaction_id,
          additional_reference_ids: {
            stripe_charge_id: stripe_charge_id,
          },
        )

        if matched?
          listing = stripe_connect_account.payable
          exceeded_match_limit_before = listing&.reached_match_limit?

          transaction.add_entry(
            transaction_type: :github_match,
            amount_in_subunits: (-1 * match_amount),
            currency_code: transfer.currency.upcase,
            primary_reference_id: zuora_transaction_id,
          )
          GitHub.dogstats.increment("billing.sponsors.match")
        end

        transaction.add_entry(
          transaction_type: :transfer,
          amount_in_subunits: transfer.amount,
          currency_code: transfer.currency.upcase,
          primary_reference_id: transfer.id,
          additional_reference_ids: {
            zuora_transaction_id: zuora_transaction_id,
          },
        )

        transaction.save!

        if listing && !exceeded_match_limit_before
          listing.reset_total_match_in_cents!

          if listing.reached_match_limit?
            # Store when the match limit was reached at.
            # See SponsorsListing#record_match_limit_reached for other
            # instrumentation that occurs when a listing hits the match limit.
            listing.touch(:match_limit_reached_at)
          end
        end
      end

      private

      def instrument_transfer
        GlobalInstrumenter.instrument("payout.transfer", {
          account: billing_transaction&.user,
          total_transfer_amount_in_cents: transfer.amount,
          match_amount_in_cents: match_amount,
          sponsorship_amount_in_cents: payment_amount,
          destination_account_type: "stripe_connect",
          destination_account_id: transfer.destination,
          payment_gateway: payment_gateway,
          matched: (match_amount > 0),
        })
      end

      def payment_amount
        transfer.metadata["payment_amount"].to_i
      end

      def payment_gateway
        if billing_transaction&.payment_type == :paypal
          "paypal"
        else
          "stripe"
        end
      end

      def billing_transaction
        Billing::BillingTransaction.find_by(platform_transaction_id: zuora_transaction_id)
      end

      def match_amount
        transfer.metadata["match_amount"].to_i
      end

      def matched?
        match_amount.positive?
      end

      def stripe_charge_id
        transfer.metadata["stripe_charge_id"]
      end

      def zuora_transaction_id
        transfer.transfer_group
      end

      def stripe_connect_account
        Billing::StripeConnect::Account.find_by!(stripe_account_id: transfer.destination)
      end
    end
  end
end
