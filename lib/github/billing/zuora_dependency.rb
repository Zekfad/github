# frozen_string_literal: true

module GitHub::Billing::ZuoraDependency
  # Public: Returns the ProductRatePlanId for this plan and the given cycle in Zuora
  #
  # Returns String
  def zuora_id(cycle:)
    product_uuid(cycle).zuora_product_rate_plan_id
  end

  def zuora_charge_ids(cycle:)
    product_uuid(cycle).zuora_product_rate_plan_charge_ids
  end

  # Public: Syncs the plan to Zuora if its ProductUUID records don't exist, and will generate a ProductUUID
  # for each type of billing cycle
  #
  # Returns Array
  def sync_to_zuora
    GitHub::Billing::ZuoraProduct.create \
      product_type: "github.plan",
      product_key: name,
      product_name: zuora_product_name,
      charges: zuora_charges
  end

  # Public: the Billing::ProductUUID object for this plan
  #
  # This returns a Billing::ProductUUID record representing this plan's
  # Zuora Rate Plan ID and the Zuora plan's Product Rate Plan IDs
  #
  def product_uuid(billing_cycle)
    ::Billing::ProductUUID.find_by(product_type: "github.plan", product_key: name, billing_cycle: billing_cycle)
  end

  # Public: Does this plan have separate base and unit costs?
  #
  # This is true for GitHub::Plan.business
  #
  # Returns a boolean
  def base_cost?
    # This is not a great way to check this, so working around this for now
    return true if GitHub.flipper[:munich_pricing].enabled? && business?
    per_seat? && cost != unit_cost
  end

  def zuora_product_name
    case display_name
    when "business cloud", "enterprise"
      "GitHub Business Cloud"
    when "pro"
      "GitHub Developer Plan"
    else
      "GitHub #{display_name.titleize} Plan"
    end
  end

  private

  def zuora_charges
    per_seat? ? per_unit_charges : [flat_charge]
  end

  def per_unit_charges
    [per_unit_charge].tap do |charges|
      charges << per_unit_base_charge if base_cost?
    end
  end

  def flat_charge
    {
      type: :flat,
      prices: { year: yearly_cost, month: cost },
    }
  end

  def per_unit_base_charge
    {
      default_quantity: base_units,
      type: :base_unit,
      prices: { year: yearly_cost / base_units, month: cost / base_units },
      unit: "Seats",
    }
  end

  def per_unit_charge
    {
      type: :unit,
      prices: { year: yearly_unit_cost, month: unit_cost },
      unit: "Seats",
    }
  end
end
