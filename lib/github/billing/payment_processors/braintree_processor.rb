# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Billing
  # Our primary payment processor is Braintree which we use to process all
  # credit card and PayPal payments on GitHub.com.
  class PaymentProcessors::BraintreeProcessor < PaymentProcessors::PaymentProcessor

    SUCCESS_STATUSES = %w(settled settling submitted_for_settlement settlement_pending)
    SLUG = "braintree"

    PROCESSOR_UNAVAILABLE_EXCEPTIONS = [
      Timeout::Error,
      Errno::EINVAL,
      Errno::ECONNRESET,
      Errno::ECONNREFUSED,
      EOFError,
      Net::HTTPBadResponse,
      Net::HTTPHeaderSyntaxError,
      Net::ProtocolError,
      Braintree::DownForMaintenanceError,
      Braintree::ServerError,
      Errno::ETIMEDOUT,
      GitHub::Restraint::UnableToLock,
      Net::ReadTimeout,
    ]

    # Public: Create a customer record in braintree.
    #
    # payment_details - A PaymentProcessorPaymentDetails of payment details.
    #
    # Yields a PaymentProcessorRecord if request to braintree was successful.
    #
    # Returns a Billing::PaymentProcessorResponse.
    def create_with_payment_details(payment_details)
      response =
      if payment_details.credit_card?
        create_customer_with_credit_card(payment_details)
      else
        create_customer_with_paypal(payment_details)
      end

      result = GitHub::Billing::Result.from_braintree(response)

      if response.success?
        record = payment_processor_record_for_customer(response.customer)
        yield record if block_given?
        result.record = record
      end
      result
    end

    # Public: Update a customer record with new payment instrument/billing details.
    #
    # payment_details - A PaymentProcessorPaymentDetails of payment details.
    #
    # Yields a PaymentProcessorRecord if request to braintree was successful.
    #
    # Returns a Billing::PaymentProcessorResponse.
    def update_payment_details(payment_details)
      if customer_id.blank?
        return GitHub::Billing::Result.failure("No customer record")
      end

      begin
        response = persist_payment_details(payment_details)
      rescue *PROCESSOR_UNAVAILABLE_EXCEPTIONS => e
        Failbot.report e, areas_of_responsibility: [:gitcoin], customer_id: customer_id
        return GitHub::Billing::Result.failure \
          "We're having trouble connecting to our payment processor. Please try again later"
      end

      result = GitHub::Billing::Result.from_braintree(response)

      if response.success?
        record = payment_processor_record_for_payment_method(response.credit_card || response.payment_method)
        yield record if block_given?
        result.record = record
      end
      result
    end

    # create or update or even use paypal when persisting the new payment details
    #
    # payment_details - A PaymentProcessorPaymentDetails of payment details.
    #
    # Returns a Braintree::SuccessfulResult or Braintree::ErrorResult
    def persist_payment_details(payment_details)
      if payment_details.update_card_on_file?
        update_credit_card(payment_details)
      elsif payment_details.credit_card?
        create_credit_card(payment_details)
      else
        create_paypal_account(payment_details)
      end
    end

    # Public: Get payment details for a customer.
    #
    # Returns a Billing::PaymentProcessorResponse.
    def get_payment_details
      if customer_id.blank?
        return GitHub::Billing::Result.failure("No customer record")
      end

      customer = GitHub.dogstats.time("braintree.timing.customer_find") do
        Braintree::Customer.find(customer_id)
      end

      record        = payment_processor_record_for_customer(customer)
      result        = GitHub::Billing::Result.success
      result.record = record
      result
    end

    # Public: Clear all payment details. It will no longer be possible to
    # process payment for this customer.
    #
    # Returns truthy if all payment details are successfully deleted.
    def clear_payment_details
      return false if customer_id.blank?

      begin
        customer = GitHub.dogstats.time("braintree.timing.customer_find") do
          Braintree::Customer.find(customer_id)
        end

        customer.payment_methods.map do |instrument|
          GitHub.dogstats.time("braintree.timing.payment_method_delete") do
            begin
              Braintree::PaymentMethod.delete(instrument.token)
            rescue Braintree::NotFoundError
              # This payment method has already been deleted from Braintree.
              true
            end
          end
        end.all?

      rescue Braintree::BraintreeError => boom
        Failbot.report(boom)
        false
      end
    end

    # Public: Charge a payment token.
    #
    # charge_details - PaymentProcessorChargeDetails with information about the charge
    #
    # Returns a Billing::PaymentProcessorResponse.
    def charge(charge_details)
      sale_transaction = sale_params_from_charge_details(charge_details)

      response = GitHub.dogstats.time("braintree.timing.transaction_sale") do
        Braintree::Transaction.sale(sale_transaction)
      end
      transaction = payment_processor_transaction(response.transaction)

      result             = GitHub::Billing::Result.from_braintree(response)
      result.transaction = transaction

      if response.success?
        yield transaction if block_given?
      end
      result
    end

    # Public: Find a Braintree transaction
    #
    # transaction_id - Braintree transaction id
    #
    # Returns a Billing::PaymentProcessorTransaction
    def find_transaction(transaction_id)
      braintree_transaction = GitHub.dogstats.time("braintree.timing.transaction_find") do
        Braintree::Transaction.find(transaction_id)
      end
      payment_processor_transaction braintree_transaction
    end

    private

    ##
    # GitHub -> Braintree
    ##

    def create_customer_with_credit_card(payment_details)
      customer = credit_card_customer_params_from_payment_details(payment_details)
      GitHub.dogstats.time("braintree.timing.customer_create") do
        Braintree::Customer.create(customer)
      end
    end

    def create_customer_with_paypal(payment_details)
      customer = paypal_customer_params_from_payment_details(payment_details)

      GitHub.dogstats.time("braintree.timing.customer_create") do
        Braintree::Customer.create(customer)
      end
    end

    def create_credit_card(payment_details)
      credit_card = credit_card_params_from_payment_details(payment_details)

      GitHub.dogstats.time("braintree.timing.customer_create") do
        Braintree::CreditCard.create(credit_card)
      end
    end

    def update_credit_card(payment_details)
      credit_card = credit_card_params_from_payment_details(payment_details)
        .except(:number, :customer_id)

      GitHub.dogstats.time("braintree.timing.customer_update") do
        Braintree::CreditCard.update(payment_details.token, credit_card)
      end
    end

    # Private: Turn a PaymentProcessorPaymentDetails into a parameter Hash.
    #
    # payment_details - A PaymentProcessorPaymentDetails object.
    #
    # Returns a Hash suitable to use in calls to the Braintree Customer APIs.
    def credit_card_customer_params_from_payment_details(payment_details)
      {
        email: payment_details.email,
        custom_fields: { user_id: payment_details.user_id },
        credit_card: {
          number: payment_details.card_number,
          expiration_month: payment_details.expiration_month,
          expiration_year: payment_details.expiration_year,
          cvv: payment_details.cvv,
          billing_address: {
            country_code_alpha3: payment_details.country_code_alpha3,
            region: payment_details.region,
            postal_code: payment_details.postal_code },
          options: { make_default: true, verify_card: true },
        },
      }
    end

    # Private: Turn a PaymentProcessorPaymentDetails into a parameter Hash.
    #
    # payment_details - A PaymentProcessorPaymentDetails object.
    #
    # Returns a Hash suitable to use in calls to the Braintree Customer APIs.
    def paypal_customer_params_from_payment_details(payment_details)
      {
        payment_method_nonce: payment_details.paypal_nonce,
        email: payment_details.email,
        custom_fields: { user_id: payment_details.user_id },
      }
    end

    # Private: Turn a PaymentProcessorPaymentDetails into a parameter Hash.
    #
    # payment_details - A PaymentProcessorPaymentDetails object.
    #
    # Returns a Hash suitable to use in calls to the Braintree CreditCard APIs.
    def credit_card_params_from_payment_details(payment_details)
      {
        customer_id: customer_id,
        number: payment_details.card_number,
        expiration_month: payment_details.expiration_month,
        expiration_year: payment_details.expiration_year,
        cvv: payment_details.cvv,

        billing_address: {
          country_code_alpha3: payment_details.country_code_alpha3,
          region: payment_details.region,
          postal_code: payment_details.postal_code },

        options: { make_default: true, verify_card: true },
      }
    end

    def create_paypal_account(payment_details)
      payment_method = payment_method_params_from_payment_details(payment_details)

      Braintree::PaymentMethod.create(payment_method)
    end

    # Private: Turn a PaymentProcessorPaymentDetails into a parameter Hash.
    #
    # payment_details - A PaymentProcessorPaymentDetails object.
    #
    # Returns a Hash suitable to use in calls to the Braintree PaymentMethod APIs.
    def payment_method_params_from_payment_details(payment_details)
      {
        customer_id: customer_id,
        payment_method_nonce: payment_details.paypal_nonce,
        options: { make_default: true },
      }
    end

    # Private: Turn a PaymentProcessorChargeDetails object into parameters for a sale.
    #
    # charge_details - A PaymentProcessorChargeDetails object.
    #
    # Returns a Hast suitable to pass into the Braintree::Transaction.sale
    def sale_params_from_charge_details(charge_details)
      {
        payment_method_token: charge_details.token,
        amount: charge_details.amount,
        descriptor: {
          name: charge_details.description,
          url: charge_details.contact_url },
        custom_fields: {
          domain: charge_details.domain,
          user_id: charge_details.user_id },
        options: { submit_for_settlement: true },
      }
    end

    ##
    # Braintree -> GitHub
    ##

    # Private: Create a PaymentProcessorRecord from a braintree customer record.
    #
    # customer - A Braintree::Customer record.
    #
    # Returns a PaymentProcessorRecord.
    def payment_processor_record_for_customer(customer)
      record = PaymentProcessorRecord.new(customer_id: customer.id)

      if instrument = customer.default_payment_method
        payment_processor_record_for_payment_method(instrument, record)
      end

      record
    end

    # Private: Create a PaymentProcessorRecord from a braintree credit_card record.
    #
    # customer - A Braintree::CreditCard record.
    # record   - A PaymentProcessorRecord to fill (optional, default: a new
    #            PaymentProcessorRecord is created).
    #
    # Returns a PaymentProcessorRecord.
    def payment_processor_record_for_payment_method(payment_method, record = nil)
      record ||= PaymentProcessorRecord.new(customer_id: customer_id)

      record.token = payment_method.token

      if payment_method.is_a?(Braintree::CreditCard)
        record.masked_number            = payment_method.masked_number
        record.expiration_month         = payment_method.expiration_month
        record.expiration_year          = payment_method.expiration_year
        record.card_type                = payment_method.card_type
        record.unique_number_identifier = payment_method.unique_number_identifier
      elsif payment_method.is_a?(Braintree::PayPalAccount)
        record.paypal_email     = payment_method.email
      end

      if billing_address = payment_method.try(:billing_address)
        record.region              = billing_address.region
        record.postal_code         = billing_address.postal_code
        record.country_code_alpha3 = billing_address.country_code_alpha3
      end

      record
    end

    # Private: Create a PaymentProcessorTransaction from a Braintree::Transaction
    #
    # Returns a PaymentProcessorTransaction.
    def payment_processor_transaction(raw_transaction)
      transaction                         = PaymentProcessorTransaction.new(raw_transaction)
      if raw_transaction.present?
        transaction.type                    = raw_transaction.type == "credit" ? "refund" : "sale"
        transaction.date                    = raw_transaction.created_at.to_time
        transaction.amount_in_cents         = raw_transaction.amount * 100
        transaction.amount_in_cents         = -transaction.amount_in_cents if transaction.refund?
        transaction.transaction_id          = raw_transaction.id
        transaction.original_transaction_id = raw_transaction.refunded_transaction_id
        transaction.status                  = raw_transaction.status
        transaction.success                 = SUCCESS_STATUSES.include?(transaction.status)
        transaction.status_history          = raw_transaction.status_history
        transaction.settlement_batch_id     = raw_transaction.settlement_batch_id
        transaction.url                     = "#{Braintree::Configuration.instantiate.base_merchant_url}/transactions/#{transaction.id}"
        transaction.customer_details        = raw_transaction.customer_details
        transaction.custom_fields           = raw_transaction.custom_fields

        billing_details                     = raw_transaction.billing_details
        transaction.postal_code             = billing_details.postal_code
        transaction.region                  = billing_details.region
        transaction.country                 = billing_details.country_name
        transaction.country_code_alpha3     = billing_details.country_code_alpha3

        if raw_transaction.paypal_details.token.present?
          transaction.payment_type               = :paypal
          transaction.paypal_email               = raw_transaction.paypal_details.payer_email
        else
          transaction.payment_type               = :credit_card
          credit_card_details                    = raw_transaction.credit_card_details
          transaction.bank_identification_number = credit_card_details.bin
          transaction.last_four                  = credit_card_details.last_4
          transaction.country_of_issuance        = credit_card_details.country_of_issuance
        end
      end
      transaction
    end
  end
end
