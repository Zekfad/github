# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Jobs
    module RetryableJob
      def self.included(base_klass)
        class << base_klass
          include ClassMethods
        end
      end

      class InvalidRetryableJob < StandardError
        def initialize
          super "Last argument for a retryable job must be a Hash"
        end
      end

      module ClassMethods
        def perform(*args)
          options = args.last if args.last.is_a?(Hash)
          raise InvalidRetryableJob if options.nil?

          retries = options.delete("retries") || options.delete(:retries)
          perform_with_retry(*args)
        rescue => error
          raise error if error.is_a?(InvalidRetryableJob)

          LegacyApplicationJob.retry_later(self.active_job_class, @retryable_args || args, retries: retries)
          Failbot.report(error)
          if Rails.env.test?
            msg = "Got an error: #{error} in #{self} - RetryableJob swallows exceptions in dev/test! Check test log for full backtrace"
            puts msg
            Rails.logger.warn msg
            Rails.logger.warn error
          end
        end

        # Jobs including this module should define this
        # method instead of perform
        def perform_with_retry(*args)
          raise NotImplementedError
        end
      end
    end
  end
end
