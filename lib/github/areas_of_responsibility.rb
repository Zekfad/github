# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module AreasOfResponsibility
    UnconfiguredAoRError = Class.new(StandardError)

    def areas_of_responsibility
      self.class.areas_of_responsibility
    end

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      def codeowners
        @codeowners ||= begin
          owners = Set.new

          path = ActiveSupport::Dependencies.search_for_file(self.name.underscore)

          if GitHub.codeowners && path
            relative_path = path.sub("#{Rails.root}/", "")
            GitHub.codeowners.for(relative_path).each_key do |owner|
              owners << owner.identifier
            end
          end

          if GitHub.areas_of_responsibility
            self.areas_of_responsibility.each do |aor|
              aor_definition = GitHub.areas_of_responsibility[aor.to_s]
              raise UnconfiguredAoRError.new("AOR '#{aor}' defined in #{self.name} is not configured in docs/areas-of-responsibility.yaml") unless aor_definition

              aor_definition["teams"].each do |team|
                owners << team
              end
            end
          end

          owners
        end
      end

      def areas_of_responsibility(*areas)
        @areas_of_responsibility ||= []

        if areas.any?
          if @areas_of_responsibility.frozen?
            raise "areas_of_responsibility have already been defined"
          end

          @areas_of_responsibility.replace(areas)
          @areas_of_responsibility.freeze
        end

        @areas_of_responsibility
      end
    end
  end
end
