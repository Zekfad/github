# frozen_string_literal: true

module GitHub
  module Relay
    module TypeName
      def platform_type_name
        class_name = self.class.name
        if i = class_name.rindex("::")
          class_name[(i + 2)..-1]
        else
          class_name
        end
      end
    end

    # Meant to be mixed into classes that represent our GraphQL types. Usually,
    # ActiveRecord::Base classes.
    module GlobalIdentification
      include TypeName

      def global_relay_id
        type_name = ::Platform::Helpers::NodeIdentification.type_name_from_object(self)
        ::Platform::Helpers::NodeIdentification.to_global_id(type_name, global_id)
      end

      def global_id
        id
      end
    end
  end
end
