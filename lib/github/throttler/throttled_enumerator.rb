# frozen_string_literal: true

module GitHub::Throttler

  # An enumerator with built-in throttling.
  class ThrottledEnumerator < Enumerator
    MAX_RETRIES = 5

    # Initialize a new throttled enumerator.
    #
    # enum       - an Enumerable or Enumerator
    # throttler: - The throttler to be used
    #
    # When iterating over this enumerator, each item will be throttled using the
    # default throttler from GitHub::Throttler.
    def initialize(enum, throttler:)
      enum = enum.each
      super() do |results|
        loop do
          retry_count = 0
          begin
            throttler.throttle { results << enum.next }
          rescue Freno::Throttler::Error => e
            GitHub.dogstats.increment("github.throttler.throttler_enumerator.error", tags: ["retry:#{retry_count}"])
            retry_count += 1
            sleep(retry_count)
            if retry_count > MAX_RETRIES
              raise e
            else
              retry
            end
          end
        end
      end
    end
  end
end
