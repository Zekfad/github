# frozen_string_literal: true

module GitHub
  # Handles provisioning initial GitHub Private Instance administrator.
  class PrivateInstanceAdminProvisioner

    # Raised if anything goes wrong while provisioning the user.
    # Designed to be rescued by callers.
    class Error < StandardError; end

    attr_reader :username, :email

    def initialize(username:, email:)
      @username = username
      @email = email
    end

    def run
      error "Not a GitHub Private Instance" unless GitHub.private_instance?

      if User.where(gh_role: "staff").any?
        error "This GitHub Private Instance already contains at least one site administrator"
      end

      unless GitHub.smtp_enabled?
        error "SMTP has not been configured on this GitHub Private Instance"
      end

      # Create the user as a site administrator
      user = User.create_with_random_password(username&.strip, true, email: email&.strip)
      if user.valid?
        log "Created user with username #{user.login}."

        # Send them the welcome email
        reset = PasswordReset.new(
          user: user,
          email: user.email,
          force: true,
          expires: 24.hours.from_now,
        )
        if reset.valid?
          EnterpriseMailer.invite_ghpi_admin(user, reset.link, reset.expires.to_s).deliver_later
          log "Sent welcome email to #{user.email}."
        else
          error "Could not create password reset: #{reset.error_message}"
        end
      else
        error "Could not create user: #{user.errors.full_messages.to_sentence}"
      end
    end

    private

    def error(msg)
      raise Error, msg
    end

    def log(msg)
      return if Rails.env.test?
      STDOUT.puts " --> #{msg}"
    end
  end
end
