# rubocop:disable Style/FrozenStringLiteralComment

require "html/pipeline"

module GitHub
  module Unsullied
    module HTML
      autoload :ExtractCodeBlocksFilter,    "github/unsullied/html/extract_code_blocks_filter"
      autoload :ExtractWikiLinksFilter,     "github/unsullied/html/extract_wiki_links_filter"
      autoload :ExtractWikiLinksNodeFilter, "github/unsullied/html/extract_wiki_links_node_filter"
      autoload :MarkupFilter,               "github/unsullied/html/markup_filter"
      autoload :ProcessWikiLinksFilter,     "github/unsullied/html/process_wiki_links_filter"
      autoload :ProcessCodeBlocksFilter,    "github/unsullied/html/process_code_blocks_filter"
      autoload :SanitizationFilter,         "github/unsullied/html/sanitization_filter"
      autoload :RelativeLinkFilter,         "github/unsullied/html/relative_link_filter"
      autoload :RelativeImageFilter,        "github/unsullied/html/relative_image_filter"

      PipelineFilters = [
        ExtractCodeBlocksFilter,
        ::GitHub::HTML::FilterWhenNotLanguage.new("Markdown", ExtractWikiLinksFilter),
        MarkupFilter,
        ::GitHub::HTML::FilterWhenLanguage.new("Markdown", ExtractWikiLinksNodeFilter),
        ::GitHub::HTML::DowncaseNameFilter,
        ProcessWikiLinksFilter,
        ProcessCodeBlocksFilter,
        SanitizationFilter,
        ::HTML::Pipeline::TableOfContentsFilter,
        ::HTML::Pipeline::EmojiFilter,
        ::GitHub::HTML::SyntaxHighlightFilter,
        ::GitHub::HTML::FilterWhenLanguage.new("Markdown", TaskList::Filter),
        RelativeLinkFilter,
        ::GitHub::HTML::RawImageFilter,
        RelativeImageFilter,
        ::GitHub::HTML::CamoFilter,
        ::GitHub::HTML::NamePrefixFilter,
        ::GitHub::HTML::RelNofollowFilter,
        ::GitHub::HTML::SetTableRoleFilter,
      ]

      Context = { asset_root: "#{GitHub.asset_host_url}/images/icons" }

      def self.pipeline
        ::HTML::Pipeline.new PipelineFilters, Context
      end
    end
  end
end
