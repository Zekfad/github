# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Unsullied
    module HTML
      class RelativeLinkFilter < ::HTML::Pipeline::Filter
        def call
          apply_filter if context[:prefix_relative_links]

          doc
        end

        def apply_filter
          doc.search("a").each do |node|
            href = node.attributes["href"].value rescue nil
            next unless href

            next if href.start_with?("wiki/")
            next if href.match(%r{^[a-z][a-z0-9\+\.\-]+:}i)  # RFC 3986 <3 U
            next if href.match(%r{^//?})
            next if href.match(/^#/)

            new_href = rewrite_relative_links(href.dup)
            node.attributes["href"].value = new_href unless new_href == href
          end
        end

        def rewrite_relative_links(href)
          "wiki/#{href}"
        end
      end
    end
  end
end
