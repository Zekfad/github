# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Unsullied
    module HTML
      class SanitizationFilter < ::HTML::Pipeline::SanitizationFilter
        # Default whitelisted elements.
        EXTRA_ELEMENTS = ["abbr", "acronym", "address", "caption",
          "cite", "col", "colgroup", "dfn", "dir", "menu", "s", "span",
          "strike", "u"]

        CLASS_WHITELIST = %w(anchor internal absent present octicon octicon-link)

        TRANSFORMERS = [
          lambda do |env|
            node = env[:node]
            return unless node["class"]
            node["class"] = node["class"].split(/\s+/).select { |c|
              CLASS_WHITELIST.include?(c) || c.start_with?("language-")
            }.join(" ")
            node.delete("class") if node["class"].empty?
          end,
        ]

        # This is seriously the cleanest way to do a deep dup. Don't judge.
        whitelist = WHITELIST.dup
        whitelist[:attributes] = whitelist[:attributes].each_with_object({}) { |(el, attrs), hash|
          hash[el] = attrs - %w(rel target)
        }
        whitelist.delete :transformers
        CUSTOM_WHITELIST = Marshal.load(Marshal.dump(whitelist))

        CUSTOM_WHITELIST[:elements]               += EXTRA_ELEMENTS
        CUSTOM_WHITELIST[:attributes][:all]       += %w(class)
        %w(a h1 h2 h3 h4 h5 h6).each do |tag|
          (CUSTOM_WHITELIST[:attributes][tag] ||= []) << "id"
        end
        CUSTOM_WHITELIST[:protocols]["a"]["href"] += %w(ftp irc apt)
        CUSTOM_WHITELIST[:remove_contents]        += %w(style)
        CUSTOM_WHITELIST[:transformers]            = TRANSFORMERS

        def add_attributes
          context[:no_follow] ? {"a" => {"rel" => "nofollow"}} : {}
        end

        def whitelist
          CUSTOM_WHITELIST.merge add_attributes: add_attributes
        end
      end
    end
  end
end
