# rubocop:disable Style/FrozenStringLiteralComment

require "set"

module GitHub
  module Importer
    class Issues
      # Gets an Array of Hashes for each Issue.
      attr_reader :issue_entries

      # Gets a Hash of Arrays for Issue comments.  The Hash is keyed off the
      # Integer ID of the issue, containing an Array of the Comment hashes.
      attr_reader :comment_entries

      # Array of Hashes for each Milestone.
      attr_reader :milestone_entries

      # Array of String Label names.
      attr_reader :label_names

      # Hash of Milestones keyed by the Integer import ID from the filename.
      attr_reader :milestones

      # Hash of Issues keyed by the Integer import ID from the filename.
      attr_reader :issues

      # Hash of Issue Comments keyed by the Integer import ID from the issue
      # filename.
      attr_reader :comments

      # Gets a Hash of String logins or emails mapping to a User.
      attr_reader :users

      # Hash of String name to Label.
      attr_reader :labels

      # Array of Users who have opted-in to the import to have their user account
      # associated with any Issue, Milestone, or Comment from the import
      attr_reader :allowed_users

      # The IssueImport that stores the history of this import.
      attr_reader :import

      # Quick and dirty Issues importer.  Loads data in JSON files.  One file
      # per issue or milestone, and one per set of comments of an issue.  Expect
      # a zip or tarball that expands to a directory structure like this:
      #
      #     issues/{num}.json
      #     issues/{num}.comments.json
      #     milestones/{num].json
      #
      # The given numbers only need to be unique as far as the import goes.
      # The import process can either attempt to use them on the site too, or
      # reassign them in the case of a conflict.
      #
      # importer - The User initiating the import. Should be someone with
      #            push access to the repository, or a GitHub staffer.
      # repo     - A Repository to import the issues into.
      # dir      - String directory name where the folder of JSON files
      #            lives.
      # options  - Hash of import options
      #
      #           :fallback       - Optional User to use as the Fallback for
      #                             issues/comments without a valid user.
      #                             Default: importer.
      #
      #           :user_whitelist - Optional array of Users which have opted into the
      #                             import process. Issues/Milestones/Comments
      #                             being imported that are associated with users
      #                             not listed in this list will be re-associated
      #                             with the fallback user before saving.
      #                             If nil, all users are treated as opting in to the
      #                             import. If empty, the fallback user will be used
      #                             as the associated user for each record.
      def initialize(importer, repo, dir, options = {})
        @importer = importer
        @fallback = options.fetch(:fallback, @importer)
        @repo = repo
        unless (@allowed_users = options[:user_whitelist]).nil?
          @allowed_users.map! { |user| user.try(:id) }.compact.uniq
        end

        @root = dir.to_s
        @import = IssueImport.new repository: repo,
          importer: importer

        if !File.directory?(@root)
          raise ArgumentError, "#{@root} is not a directory"
        end
      end

      def run!
        GitHub::RateLimitedCreation.disable_content_creation_rate_limits do
          scan!
          build!
        end
      end

      # Scans and loads the JSON data from the directory.
      #
      # Returns nothing.
      def scan!
        @issue_entries = []
        @comment_entries = {}
        @label_names = []
        @users = {email: Set.new, login: Set.new}
        @issues = {}
        @comments = {}
        @milestone_entries = {}
        @milestones = {}

        scan_milestones(File.join(@root, "milestones"))
        scan_issues(File.join(@root, "issues"))

        @label_names.compact!

        nil
      end

      # Assembles ActiveRecord objects from the scanned JSON data.
      #
      # Returns nothing.
      def build!
        load_users
        load_labels
        load_milestones
        load_issues

        nil
      end

      def save!
        GitHub::RateLimitedCreation.disable_content_creation_rate_limits do
          GitHub.importing do
            save_issues
          end
          @import.save!
          @import
        end
      end

      # Scans the issues/ directory for *.json files.
      #
      # dir - A String directory name.
      #
      # Returns nothing.
      def scan_issues(dir)
        scanned = scan_json_dir(dir) do |entry|
          scan_issue(dir, entry)
        end

        sort_issues if scanned
      end

      # Scans the milestones/ directory for *.json files.
      #
      # dir - A String directory name.
      #
      # Returns nothing.
      def scan_milestones(dir)
        scanned = scan_json_dir(dir) do |entry|
          scan_milestone(dir, entry)
        end
      end

      # Scans a directory for *.json files.
      #
      # dir - A String directory name.
      #
      # Yields valid filenames.
      # Returns true if the directory exists, or false.
      def scan_json_dir(dir)
        return unless File.directory?(dir)
        files = Dir["#{dir}/*.json"].sort_by { |file| file.split(".").first.to_i }

        Dir.foreach(dir) do |entry|
          next if entry =~ /^\./
          yield entry
        end

        true
      end

      # Reads a single issue file and adds it to @issue_entries.
      #
      # dir   - A String directory name that the Issue JSON file is in.
      # entry - A String file basename of the Issue JSON file.
      #
      # Returns nothing.
      def scan_issue(dir, entry)
        num = entry.scan(/^(\d+)\.json$/i).flatten.first.to_s
        return if num.empty?

        full = File.join dir, entry
        hash = massage_issue full, num
        @issue_entries << hash

        scan_issue_comment(dir, num)
      end

      # Reads a single issue comments JSON file and adds it to
      # @comment_entries.
      #
      # dir - A String directory name that the comments JSON file is in.
      # num - An Integer import ID of the Issue that these comments belong to.
      #
      # Returns nothing.
      def scan_issue_comment(dir, num)
        file = File.join dir, "#{num}.comments.json"
        return unless File.readable?(file)

        @comment_entries[num] = massage_issue_comments(file)
      end

      # Reads a single milestone JSON file and adds it to @milestones.
      #
      # dir   - A String directory name that the milestones JSON file is in.
      # entry - A String file basename of the milestone JSON file.
      #
      # Returns nothing.
      def scan_milestone(dir, entry)
        num = entry.scan(/^(\d+)\.json$/i).flatten.first.to_s
        return if num.empty?

        full = File.join dir, entry
        @milestone_entries[num.to_i] = massage_milestone(full, num.to_i)
      end

      # Cleans up the Issue Hash object before attempting to create db records.
      #
      # data      - A Hash of a single Issue or a String filename containing
      #             the Issue JSON data.
      # import_id - The Integer matching the filename.
      #
      # Returns a massaged Hash.
      def massage_issue(data, import_id)
        if !data.is_a?(Hash)
          return massage_issue(parse(data.to_s), import_id)
        end

        data[:user] ||= data.delete(:creator)
        data[:import_id] = import_id
        data[:number] ||= import_id.to_i
        massage_user data, :user
        massage_user data, :assignee
        massage_user data, :closed_by
        massage_dates data, :created_at, :updated_at, :closed_at
        if label_array = data[:labels]
          label_array.each do |label|
            @label_names << label_name(label)
          end
        end

        data
      end

      # Cleans up the Milestone Hash object before attempting to create db
      # records.
      #
      # data      - A Hash of a single Milestone or a String filename
      #             containing the Milestone JSON data.
      # import_id - The Integer matching the filename.
      #
      # Returns a massaged Hash.
      def massage_milestone(data, import_id)
        if !data.is_a?(Hash)
          return massage_milestone(parse(data.to_s), import_id)
        end

        data[:number] ||= data[:import_id] = import_id
        massage_user data, :creator
        massage_dates data, :created_at, :due_on
        data
      end

      # Cleans up the Issue Comments before attempting to create db records.
      #
      # data - An Array of Issue Comments or a String filename containing the
      #        Issue Comment JSON data.
      #
      # Returns an Array of massaged Hashes.
      def massage_issue_comments(data)
        if !data.is_a?(Array)
          return massage_issue_comments(parse(data.to_s))
        end

        data.each { |comment| massage_issue_comment(comment) }
      end

      # Cleans up a single Issue Comment before attempting to create a db record.
      #
      # data - A Hash of a single Issue Comment.
      #
      # Returns a massaged Hash.
      def massage_issue_comment(data)
        massage_user data, :user
        massage_dates data, :created_at, :updated_at
      end

      def massage_dates(hash, *keys)
        keys.each do |key|
          if value = hash[key]
            hash[key] = Time.parse value
          end
        end
      end

      # Cleans up a GitHub user property into an expected format.
      #
      #     hash = {:user => 'bob'}
      #     massage_user hash, :user
      #     hash
      #     # => {:user => {:login => 'bob'}}
      #
      # hash - A Hash of properties for a resource.
      # key  - The Symbol key that contains the User property.
      #
      # Returns nothing, but mutates the given `hash` argument.
      def massage_user(hash, key)
        case value = hash[key]
        when String
          subkey = value["@"] ? :email : :login
          @users[subkey] << value
          hash[key] = {subkey => value}
        when Hash
          if email = value[:email]
            @users[:email] << email
          elsif login = value[:login]
            @users[:login] << login
          end
        end
      end

      # Loads users from the :email and :login sets on @users.
      #
      # Raises if not all the Users are found.
      # Returns nothing.
      def load_users
        emails = @users.delete(:email).to_a
        email_users = emails.empty? ? {} : User.find_by_emails(emails)
        email_users.each do |em, user|
          @users[em.downcase] = user.id
          @users[user.login.downcase] = user.id
        end

        logins = @users.delete(:login).to_a - @users.keys
        User.with_logins(*logins).each do |user|
          @users[user.login.downcase] = user.id
        end
      end

      def load_labels
        @label_names.compact!
        @label_names.uniq!
        return if @label_names.empty?
        found_labels = @repo.labels.where(name: @label_names).
          index_by { |label| label.name }
        new_labels = @label_names.reject { |n| found_labels.key?(n) }
        created_labels = new_labels.inject({}) do |memo, name|
          new_label = @repo.labels.create!(name: name)
          new_label.instrument_creation(context: "issues import")
          memo.update name => new_label
        end
        @labels = created_labels.merge found_labels
      end

      def load_milestones
        found = @repo.milestones.all.index_by { |m| m[:number] }
        created = {}

        if !@milestone_entries.empty?
          new_milestones = @milestone_entries.values.reject { |hash| found.key?(hash[:number].to_i) }
          created = new_milestones.inject({}) do |memo, hash|
            memo.update hash[:number].to_i => create_milestone(hash)
          end
        end
        @milestones = created.merge found
      end

      def create_milestone(hash)
        milestone = @repo.milestones.build number: hash[:number]
        load_user_fields milestone, hash, creator: :created_by
        load_fields milestone, hash, :title, :state, :description,
          :created_at, :due_on
        milestone.created_by ||= @fallback
        milestone.save!
        milestone
      end

      def load_issues
        @issue_entries.each do |issue_hash|
          load_issue issue_hash
          load_issue_comments issue_hash[:import_id]
        end
      end

      def load_issue(issue_hash)
        issue = Issue.new
        issue.repository_id = @repo.id

        load_user_fields issue, issue_hash, :user, :assignee
        load_fields issue, issue_hash, :title, :state, :body, :created_at,
          :updated_at, :closed_at, :number
        issue.user ||= @fallback

        if issue.assignee && !issue.assignable_to?(issue.assignee)
          issue.assignee = nil
        end

        if (milestone = issue_hash[:milestone]).present?
          if milestone.is_a?(Hash)
            milestone_number = milestone[:number]
          else
            milestone_number = milestone.to_i
          end
          issue.milestone = @milestones[milestone_number]
        end

        if (labels = issue_hash[:labels]).present?
          label_names = labels.map { |name| label_name(name) }
          label_names.compact!
          label_records = label_names.map { |name| @labels[name] }
          label_records.compact!
          issue.label_ids = label_records.map { |r| r.id }
        end

        @issues[issue_hash[:import_id]] = {issue: issue, issue_hash: issue_hash}
      end

      def load_issue_comments(issue_import_id)
        arr = @comments[issue_import_id] = []
        (@comment_entries[issue_import_id] || []).each do |comment_hash|
          arr << comment = IssueComment.new
          load_user_fields comment, comment_hash, :user
          load_fields comment, comment_hash, :body, :created_at, :updated_at
          comment.user ||= @fallback
        end
      end

      def load_user_fields(record, hash, *args)
        keys = {}
        if args.size == 1 && args.first.is_a?(Hash)
          keys = args.first
        else
          args.each do |key|
            keys[key] = key
          end
        end

        keys.each do |hash_key, model_key|
          if user_hash = hash[hash_key]
            user_id = @users[(user_hash[:login] || user_hash[:email]).to_s.downcase]

            if allowed_users.nil? || allowed_users.include?(user_id)
              record.send "#{model_key}_id=", user_id
            end
          end
        end
      end

      def load_fields(record, hash, *keys)
        keys.each do |field|
          if value = hash[field]
            record.send "#{field}=", value
          end
        end
      end

      def save_issues
        @issues.each do |import_id, issue_info|
          issue = issue_info[:issue]
          issue_hash = issue_info[:issue_hash]

          next if issue.id # already saved
          if !issue.valid? && issue.errors.size == 1 && issue.errors[:number].any?
            issue.number = nil
          end

          GitHub.context.push(actor_id: issue.user.id) do
            Audit.context.push(actor_id: issue.user.id) do
              issue.save!
            end
          end
          @import.importing issue

          save_comments import_id, issue

          update_event_timestamps issue

          create_closed_event issue, issue_hash
        end
      end

      def save_comments(import_id, issue)
        comments = Array(@comments[import_id])
        comments.each do |comment|
          comment.issue = issue
          comment.save!
        end
      end

      # Updates the timestamps on "labeled", "assigned" and "milestoned" issue events
      # to match the timestamp of the last comment in the issue.
      def update_event_timestamps(issue)
        last_comment = issue.comments.last
        timestamp = last_comment && last_comment.created_at
        timestamp ||= issue.created_at

        events = issue.events.where(event: ["labeled", "assigned", "milestoned"])
        events.update_all(created_at: timestamp)
      end

      # Creates a closed issue event if the issue was closed and both closed_by
      # and closed_at information was provided
      def create_closed_event(issue, issue_hash)
        closer = issue_hash[:closed_by]
        timestamp = issue_hash[:closed_at]

        if issue.state == "closed" && issue.events.closes.empty? && closer && timestamp
          event_attrs = {
            event: "closed",
            actor: closer,
            created_at: timestamp,
          }

          close_event = IssueEvent.new
          load_user_fields close_event, event_attrs, :actor
          close_event.actor ||= @fallback
          load_fields close_event, event_attrs, :created_at, :event

          close_event.issue = issue
          close_event.save!
        end
      end

      # Sorts the issues by the import ID.
      #
      # Returns nothing, but mutates @issue_entries.
      def sort_issues
        @issue_entries.sort! do |x, y|
          x[:number] <=> y[:number]
        end
      end

      def label_name(label)
        name = case label
        when Hash then label[:name].to_s
        when Label then label.name.to_s
        else label.to_s
        end
        name.strip!
        name.blank? ? nil : name
      end

      # Parses a JSON file.
      #
      # file - String JSON filename.
      #
      # Returns a Hash or Array.
      def parse(file)
        Yajl.load(IO.read(file), symbolize_keys: true)
      end
    end
  end
end
