# Importers

This is where the low level import code lives.  Importers should deal with bulk
data, with direct access to the GitHub database.  The bulk data is a public
spec that anyone can use.

* Simple to create.
* Possible to have OSS scripts to convert between formats.  Jira => Issues JSON
format, for example.
* Needs full validators so users can check for valid data before submitting.
* Having the bulk data locally means we can take advantage of more efficient
internal APIs for bulk inserts.

When imports are run, they should be tracked somewhere.  Issue imports are
saved to the IssueImport model, for example.

## Issue Importer

The Issue Importer uses a simplified form of the Issue API V3 JSON format as
[described in this Gist](https://gist.github.com/izuzak/654612901803d0d0bc3f). 
It matches the API v3 where possible, and allows issues and comments to set 
their own numbers and timestamps, even though none of the UI or API support this.

The importer uses a default user (usually a bot account or the user requesting the
import) as the author when creating issues and comments. For users who have explicitly
approved having new issues mapped to their GitHub account, the importer supports 
setting the author to a GitHub user supplied in the import data. To collect this 
approval we currently:
- ask the maintainers of the project  to create an OAuth application
- have them ask their contributors to sign into that app with their GitHub account. 
- fetch the list of users who signed into the application and whitelist the users for the import.

See [this GitHubber article](https://githubber.com/article/crafts/engineering/api-and-addons/api-support-cheat-sheet) 
and [this Halp canned reply](https://halp.githubapp.com/inboxes/technical/canned_replies/bulk-import-issues/edit)
for more information on helping projects move to GitHub with this importer.

### Quickstart

1. Get the zipball/tarball archive with the import data. This is usually emailed
through Halp, or provided as a link.
2. Sign into ops-aux2-cp1-prd: `ssh ops-aux2-cp1-prd.iad.github.net`
3. Extract the archive into some folder. The directory structure should look 
like this:

    ```
git@ops-aux2-cp1-prd:~/imports/my-import$ tree
.
`-- issues
        |-- 1.json
        |-- 1.comments.json
`-- milestones
        |-- 1.json
```

4. Start up gh-console:

  ```ruby
$ gh-console master

>> u = dat 'username1' # the user requesting the import
>> r = dat 'foo/bar' # the repository into which the issues will be imported
>> fallback = dat 'username2' # the default user for the import, i.e. the fallback account

>> appl = OauthApplication.find_by_key('client_id') # the OAuth application used to collect approvals
>> optins = User.where(:id => appl.accesses.pluck(:user_id).uniq) # users which will be whitelisted for the import and will appear as authors of issues and comments

>> i = GitHub::Importer::Issues.new(u, r, "/home/git/imports/my-import", :fallback => fallback, :user_whitelist => optins)
```

5. Scan the JSON files and build the in-memory ActiveRecord objects.

  ```ruby
>> i.run! # same as i.scan! ; i.build!
```

6. You can double check that no imported issue numbers clash:

  ```ruby
>> puts r.issues.map(&:number).sort.to_sentence
>> puts i.issues.map { |array| array.first.to_i }.sort.to_sentence
```

7. Save the Issue records.

  ```ruby
>> i.save!
```

You can see what Issue IDs were imported by scanning the IssueImport record:

  ```ruby
>> i.import.issue_ids
```

If you have a problem with the data, you can modify it and re-run the build
stage.

  ```ruby
>> i.label_names.delete('!') # remove an invalid label name
>> i.build!
>> i.save!
```

### Details

The Issues importer has three main phases: Scanning, Building, and Saving.  To
be honest, the code probably should be written into three distinct classes with
better testing and validation.

1. The **Scanning Phase** reads the JSON data into in-memory hashes.  Here the raw
Issue/Comment/User/Label/Milestone data is read from the JSON.
2. The **Building Phase** builds the unsaved Issue and Comment records.  Labels
and Milestones are also *created* as needed.  Users are not created based on
given emails/login names.  If users are not found, use the fallback user
(usually the owner of the repository).
3. The **Saving Phase** inserts the Issues into the DB.  Patience!

