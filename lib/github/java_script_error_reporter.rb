# frozen_string_literal: true

require "browser"

module GitHub
  class JavaScriptErrorReporter
    # Create an error stack trace reporter.
    #
    # owners   - The Codeowners::File matching teams to source files.
    # resolver - The GitHub::SourceMapResolver to map stack traces to source files.
    #
    # Examples
    #
    #   resolver = GitHub::SourceMapResolver.new("public/assets")
    #   reporter = GitHub::JavaScriptErrorReporter.new(owners: Codeowners::File.new("CODEOWNERS"), resolver: resolver)
    #   reporter.report(error)
    def initialize(owners:, resolver:)
      @owners = owners
      @resolver = resolver
    end

    # Sends the JavaScript exception to failbot.
    #
    # input - A Hash of exception data from Platform::Mutations::ReportBrowserError.
    #
    # Returns nothing.
    def report(input)
      return if bot?(input[:user_agent])

      error = input[:error]
      return unless error && reportable?(error)
      owners = blame(error[:stacktrace])

      detail = {
        type: error[:type],
        value: error[:value],
        stacktrace: error[:stacktrace].reverse,
      }

      context = {
        app: "github-js",
        platform: "javascript",
        sanitized_url: input[:sanitized_url],
        referrer: input[:referrer],
        user_agent: input[:user_agent],
        time_since_load: input[:time_since_load],
        user: input[:user],
        ready_state: input[:ready_state],
        exception_detail: [detail],
        areas_of_responsibility: owners.join(", "),
        rollup: "auto",
      }

      Failbot.report!(Api::JavaScriptError.new(error), context)
    end

    private

    def bot?(user_agent)
      user_agent.blank? || Browser.new(user_agent).bot?
    end

    def reportable?(error)
      required = error.values_at(:type, :value, :stacktrace).none? { |x| x.nil? || x.empty? }
      required && stacktrace?(error)
    end

    def stacktrace?(error)
      error[:stacktrace].any? do |frame|
        frame[:filename].include?(GitHub.asset_host_url.presence || GitHub.url)
      end
    end

    # Finds the owners for the file in the top frame of the exception's
    # stack trace. Owners of this frame are most likely to know how to fix
    # the error, although it could be caused deeper in the stack.
    #
    # stacktrace - Array<{filename: String, lineno: Integer, colno: Integer}>
    #
    # Returns an Array<String> of team names.
    def blame(stacktrace)
      if file = resolve(stacktrace)
        @owners.for(file).keys.map(&:identifier).map do |name|
          name.split("/").last
        end
      else
        []
      end
    end

    # Finds the source file name for the top frame of the stack trace.
    #
    # stacktrace - Array<{filename: String, lineno: Integer, colno: Integer}>
    #
    # Returns a String file name.
    def resolve(stacktrace)
      frame = stacktrace.first
      url, line, col = frame.values_at(:filename, :lineno, :colno)
      @resolver.resolve(url: url, line: line.to_i, column: col.to_i)
    end
  end
end
