# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::HTML
  class DowncaseNameFilter < Filter
    def call
      doc.css("[name], [id]").each do |node|
        if node["name"]
          node["name"] = node["name"].mb_chars.downcase.to_s
        end

        if node["id"]
          node["id"] = node["id"].mb_chars.downcase.to_s
        end
      end
      doc
    end
  end
end
