# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::HTML
  # This filter rewrites image tags with a max-width inline style and also wraps
  # the image in an <a> tag that causes the full size image to be opened in a
  # new tab.
  #
  # The max-width inline styles are especially useful in HTML email which
  # don't use a global stylesheets.
  class ImageMaxWidthFilter < Filter
    MAX_HEIGHT_WIDTH = 2000
    HEIGHT_WIDTH_ATTRS = %w(height width)

    def call
      doc.search("img").each do |element|
        # Cap height/width attributes.

        HEIGHT_WIDTH_ATTRS.each do |attr|
          if (value = element[attr].try(:to_i)) && value > MAX_HEIGHT_WIDTH
            element[attr] = MAX_HEIGHT_WIDTH.to_s
          end
        end

        # Skip if there's already a style attribute. Not sure how this
        # would happen but we can reconsider it in the future.
        next if element["style"]

        # Bail out if src doesn't look like a valid http url. trying to avoid weird
        # js injection via javascript: urls.
        next if element["src"].to_s.strip =~ /\Ajavascript/i

        element["style"] = "max-width:100%;"

        if !has_ancestor?(element, %w(a))
          element = link_image(element)
        end

        if element.parent.is_a?(Nokogiri::HTML::DocumentFragment)
          element = para_image(element)
        end
      end

      doc
    end

    def link_image(element)
      nest_element(element, "a", href: element["src"], target: "_blank", rel: "noopener noreferrer")
    end

    def para_image(element)
      nest_element(element, "p")
    end

    def nest_element(element, tag_name, attrs = {})
      el = doc.document.create_element(tag_name, attrs)
      el.add_child(element.dup)
      element.replace(el)
      el
    end
  end
end
