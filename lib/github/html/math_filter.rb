# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::HTML
  class MathFilter < ::HTML::Pipeline::Filter
    IGNORE_PARENTS = %w(pre code).freeze

    REGEX = /
      (
        (?<!\$|\\) # look-behind to ensure it is not \ or $
        \${2}      # $$
        (?!\$)     # look-ahead to ensure it is not another $
      )
      (.*?)        # non-greedy maths
      (?(1)
        (?<!\$|\\) # look-behind to ensure it is not \ or $
        \1         # $$
        (?!\$)     # look-ahead to ensure it is not another $
      )
    /xm

    def call
      return doc unless enabled?

      doc.xpath(".//text()").each do |node|
        content = node.to_html
        next if !content.include?("$")
        next if has_ancestor?(node, IGNORE_PARENTS)
        html = math_image_filter!(content, force_inline: node.parent.children.size > 1)
        next if html.nil?
        node.replace(html)
      end

      doc
    end

    def self.enabled?(context)
      GitHub.render_enabled? && context[:entity].try(:math_filter_enabled?)
    end

    def enabled?
      self.class.enabled?(context)
    end

    def math_image_filter!(text, force_inline:)
      non_whitespace_length = text.rindex(/\S/) - text.index(/\S/) + 1
      text.gsub!(REGEX) do |maths|
        mode = if force_inline || maths.size != non_whitespace_length
          "inline"
        else
          "displayed"
        end
        just_maths = $2
        Nokogiri::XML::Node.new("img", doc).tap do |img|
          img["class"] = "math math-#{mode}"
          img["alt"] = maths
          img["src"] = math_url(just_maths, mode)
        end
      end
    end

    # Public: The URL used as a prefix to all render calls.
    #
    # Returns String.
    def render_url
      GitHub.render_host_url
    end

    def math_url(math, mode)
      File.join(render_url, "/render/math?#{{math: math, mode: mode}.to_query}")
    end
  end
end
