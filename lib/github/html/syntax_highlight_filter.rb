# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::HTML
  class SyntaxHighlightFilter < Filter
    def call
      doc.search("pre").each do |node|
        html = syntax_highlight_filter(node)
        next if html.nil?
        node.replace(html)
      end
      doc
    end

    def syntax_highlight_filter(node, lang = nil)
      default = context[:highlight] && context[:highlight].to_s
      return unless lang = node["lang"] || default
      return unless scope = tm_scope_for(lang)

      # FIXME: Remove the respond_to? and always use node.text_content once we
      # fully transition to Goomba.
      text = node.respond_to?(:text_content) ? node.text_content : node.inner_text
      lines = GitHub::Colorize.highlight_one(scope, text, timeout: 1, code_snippet: true, fallback_to_plain: false)
      return unless lines

      %Q{<div class="highlight highlight-#{scope.gsub('.', '-')}"><pre>} + lines.join("\n") + %q{</pre></div>}

    rescue GitHub::Colorize::RPCError
      nil
    end

    def language_for(lang_name)
      # FIXME: We can probably come up with a better way to choose between
      # multiple languages that handle a single file extension. Perhaps we
      # could pass the text to be highlighted to Linguist and have it classify
      # it for us. For now we'll just choose the first language that supports
      # the file extension.
      with_period = lang_name.gsub(%r{\A\.?}, ".")
      Linguist::Language[lang_name] || Linguist::Language.find_by_extension(with_period).first
    end

    def tm_scope_for(lang_name)
      if lang = language_for(lang_name)
        lang.tm_scope
      end
    end
  end
end
