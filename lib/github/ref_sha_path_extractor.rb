# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class RefShaPathExtractor
    class InvalidPath < StandardError; end
    def initialize(repository)
      @repository = repository
    end

    # Public: Extract a branch/ref name or a commit SHA and a tree/blob
    # path from a path that we get from the params of a Rails request for a
    # blob or tree. This method verifies the presence of the branch/ref/commit,
    # but not the path. The whole reason this exists is simply to be able to
    # differentiate the two, so verifying the presence of the base is just a
    # necessary step.
    #
    # path - The String to extract from
    #
    # Examples
    #
    #   # where somebranch exists
    #   call("somebranch/lib/foo/bar.rb")
    #   # => ["somebranch", "lib/foo/bar.rb"]
    #
    #   # where master exists
    #   call("master")
    #   # => ["master", nil]
    #
    #   # where blahblah does not exist
    #   call("blahblah")
    #   # => [nil, "blahblah"]
    #
    #   # where 951eb57e171dd651192468aeac6465d3f3fef062 exists
    #   call("951eb57e171dd651192468aeac6465d3f3fef062/lib/foo/bar.rb")
    #   # => ["951eb57e171dd651192468aeac6465d3f3fef062", "lib/foo/bar.rb"]
    #
    # Returns an Array of the ref/SHA and the path.
    #   If no ref/SHA is found, the first array entry will be nil
    #   and the second entry will be the entire string that was provided.
    #   If the ref/SHA is found but no path follows, the second entry
    #   will be nil.
    def call(path)
      raise InvalidPath if path.match(%r{\x00})
      first_chunk = path.split("/").first
      ref_or_sha  = find_ref(path, first_chunk) || find_commit_sha(first_chunk)

      result_path =
        if ref_or_sha.nil?
          path
        elsif ref_or_sha == path
          nil
        else
          # we want to get the path without the ref or sha that it started with.
          # this is a little tricky if it was a short SHA that got expanded.
          to_remove =
            if path.b.starts_with?(ref_or_sha.b)
              ref_or_sha
            else
              first_chunk
            end
          path_param = path.b.sub(%r{#{Regexp.escape(to_remove.b)}(/|$)+}, "")
          File.expand_path("/#{path_param}")[1..-1]
        end

      [ref_or_sha, result_path]
    end

    private

    def find_ref(path, maybe_ref)
      qualified_ref_name, _ = @repository.rpc.read_ref_from_path(path.b, limit: GitHub.maximum_ref_length)
      return unless qualified_ref_name

      qualified_ref_name.gsub(%r{\Arefs/heads/|refs/tags/}, "")
    end

    def find_commit_sha(maybe_sha)
      (sha = @repository.ref_to_sha(maybe_sha)) &&
        (commit = @repository.commits.find(sha)) &&
          commit.oid
    rescue ::GitRPC::InvalidObject,     # object found but not a commit
           ::GitRPC::InvalidRepository, # repository isn't routed, doesn't exist
           ::GitRPC::ObjectMissing      # bogus commit SHA
    end
  end
end
