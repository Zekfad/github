# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  # Extend this module to mark associations to be deleted or destroyed in the
  # background, rather than in-line as part of an ongoing transaction. Used to
  # prevent potential performance issues when deleting records with large
  # numbers of dependents.
  module BackgroundDependentDeletes
    class BackgroundDependentDeleteError < RuntimeError; end

    DEPENDENT_NULLIFY_OPTION = "nullify"

    # Declare this association to be deleted in the background.
    #
    # assoc - Symbol name of an association
    #
    # Sets an after_commit :on => destroy hook to queue a job which deletes the
    # dependent records from the database.
    def delete_dependents_in_background(assoc)
      class_name = self.name

      after_commit on: :destroy do
        # Ask the class that requested a background dependent delete
        # for information about the given association, and grab the
        # ActiveRecord model class from the result so that we can count
        # any callbacks that that model has defined that may run at destruction.
        assoc_class = self.class.reflect_on_association(assoc).klass
        if assoc_class._commit_callbacks.empty?
          commit_cbs = []
          commit_destroy_cbs = []
        else
          # if will always include destroy so we don't need to set both commit_cbs and commit_destroy_cbs
          commit_cbs = []
          commit_destroy_cbs = assoc_class._commit_callbacks.select { |c| c.instance_variable_get(:@if).include?(:destroy) }
        end

        if assoc_class._destroy_callbacks.empty?
          after_destroy_cbs = []
        else
          after_destroy_cbs = assoc_class._destroy_callbacks
        end

        # A model that has any destructors should be destroyed, rather than deleted
        # to allow the callbacks to run.
        destroy_cbs = commit_cbs + commit_destroy_cbs + after_destroy_cbs
        if destroy_cbs.length > 0
          error_msg = "#{assoc_class} has #{destroy_cbs.length} destructor callbacks, and should be destroyed not deleted."
          raise BackgroundDependentDeleteError, error_msg
        end

        args = [class_name, id, assoc]
        key = GitHub::Jobs::DeleteDependentRecords.audit_key(*args)
        if send(assoc).exists?
          DeleteDependentRecordsJob.perform_later(*args)
        end
      end
    end

    # Declare this association to be destroyed in the background.
    #
    # assoc - Symbol name of an association
    #
    # Sets an after_commit :on => destroy hook to queue a job which destroys the
    # dependent records.
    def destroy_dependents_in_background(assoc)
      class_name = self.name

      after_commit on: :destroy do
        if send(assoc).exists?
          DestroyDependentRecordsJob.perform_later(class_name, id, assoc)
        end
      end
    end

    # Declares that the foreign key of the given association should be nullified in the background
    # when the receiver of this method is destroyed.
    #
    # assoc - Symbol name of an association
    def nullify_dependents_in_background(assoc)
      class_name = self.name

      after_commit on: :destroy do
        if send(assoc).exists?
          DestroyDependentRecordsJob.perform_later(
            class_name,
            id,
            assoc,
            dependent: DEPENDENT_NULLIFY_OPTION
          )
        end
      end
    end
  end
end
