# frozen_string_literal: true

require "spokes/spokes_pb"
require "spokes/spokes_twirp"
require "faraday/resilient"

module GitHub
  module Spokes
    class Client
      # Spokes client implementation that makes RPC calls to spokesd.
      # We use Twirp over protobufs to communicate with spokesd.
      class Spokesd
        SERVICE_NAME = "spokesd"

        def client_name
          "spokesd"
        end

        def available?(repository)
          req = ::Spokes::StatusRequest.new(repository: build_repository(repository))

          resp = client.status req
          if resp.error
            # Twirp Errors are not exceptions, so wrap it on our own interpretation
            raise GitHub::Spokes::ClientError.new(resp.error.to_s)
          else
            return resp.data.available
          end
        end

        def pick_fileservers
          req = ::Spokes::PickFileServersRequest.new

          resp = client.pick_file_servers req
            # Twirp Errors are not exceptions, so wrap it on our own interpretation
          raise GitHub::Spokes::ClientError.new(resp.error.to_s) if resp.error

          raise GitHub::DGit::UnroutedError, "no hosts found for placement" if resp.data.file_server.empty?

          resp.data.file_server.map do |fs|
            GitHub::DGit::Fileserver.new(
              name: fs.name,
              ip: field_or_nil(fs.ip),
              datacenter: field_or_nil(fs.datacenter),
              rack: field_or_nil(fs.rack),
              online: fs.online,
              embargoed: fs.embargoed,
              evacuating: fs.evacuating,
              voting: fs.voting,
              hdd_storage: fs.hddStorage)
          end
        end

        def field_or_nil(field)
          # In spokesd values that are NULL are passed around as empty
          # strings, and also put on the wire as such.  However in
          # ruby we actually need NULLable fields in the database to
          # be nil.  This function makes it so.
          field != "" ? field : nil
        end

        def number_field_or_nil(field)
          # In spokesd values that are NULL are passed around as zero
          # values, and also put on the wire as such.  However in
          # ruby we actually need NULLable fields in the database to
          # be nil.  This function makes it so.
          field != 0 ? field : nil
        end

        def repo_type_to_twirp(repo_type)
          case repo_type
          when GitHub::DGit::RepoType::REPO
            ::Spokes::Repository::RepositoryType::REPO
          when GitHub::DGit::RepoType::WIKI
            ::Spokes::Repository::RepositoryType::WIKI
          when GitHub::DGit::RepoType::GIST
            ::Spokes::Repository::RepositoryType::GIST
          when GitHub::DGit::RepoType::NETWORK
            ::Spokes::Repository::RepositoryType::NETWORK
          end
        end

        def create_replica_from_route(route, fileserver, repo_type)
          case repo_type
          when GitHub::DGit::RepoType::REPO, GitHub::DGit::RepoType::WIKI
            GitHub::DGit::Replica::Repo.new(
              db_name: route.db_name,
              fileserver: fileserver,
              read_weight: route.read_weight,
              quiescing: route.quiescing,
              checksum: route.checksum,
              expected_checksum: field_or_nil(route.expected_checksum),
              state: route.replica_state,
              created_at: Time.at(route.created_at),
              updated_at: Time.at(route.updated_at),
              db_network_replica_id: number_field_or_nil(route.network_replica_id),
            )
          when GitHub::DGit::RepoType::GIST
            GitHub::DGit::Replica::Gist.new(
              db_name: route.db_name,
              fileserver: fileserver,
              read_weight: route.read_weight,
              quiescing: route.quiescing,
              state: route.replica_state,
              checksum: route.checksum,
              expected_checksum: field_or_nil(route.expected_checksum),
              created_at: Time.at(route.created_at),
              updated_at: Time.at(route.updated_at),
            )
          when GitHub::DGit::RepoType::NETWORK
            GitHub::DGit::Replica::Network.new(
              db_name: route.db_name,
              fileserver: fileserver,
              read_weight: route.read_weight,
              quiescing: route.quiescing,
              state: route.replica_state,
              created_at: Time.at(route.created_at),
              updated_at: Time.at(route.updated_at),
              db_network_replica_id: number_field_or_nil(route.network_replica_id),
            )
          end
        end

        def all_replicas(network_id, repo_id, repo_type, preferred_dc)
          req = ::Spokes::AllRepoReplicasRequest.new(
            repository: ::Spokes::Repository.new(
              type: repo_type_to_twirp(repo_type),
              networkId: network_id,
              repositoryId: repo_id),
            preferred_dc: preferred_dc)
          resp = client.all_repo_replicas req
            # Twirp Errors are not exceptions, so wrap it on our own interpretation
          raise GitHub::Spokes::ClientError.new(resp.error.to_s) if resp.error

          resp.data.replicas.map do |route|
            fileserver = GitHub::DGit::Fileserver.new(
              name: route.file_server.name,
              ip: field_or_nil(route.file_server.ip),
              datacenter: field_or_nil(route.file_server.datacenter) || GitHub.default_datacenter,
              rack: field_or_nil(route.file_server.rack) || GitHub.default_rack,
              online: route.file_server.online,
              embargoed: route.file_server.embargoed,
              evacuating: route.file_server.evacuating,
              voting: route.file_server.voting,
              hdd_storage: route.file_server.hddStorage)
            create_replica_from_route(route, fileserver, repo_type)
          end
        end

        private

        # build_repository constructs a Repository protobuf object based on the
        # repository-like ActiveRecord object we provide.
        #
        # Raises ArgumentError for an unsupported object.
        def build_repository(repository)
          case repository
          when Repository, Archived::Repository
            ::Spokes::Repository.new(
              nwo: repository.name_with_owner,
              type: ::Spokes::Repository::RepositoryType::REPO,
              networkId: repository.network.id,
              repositoryId: repository.id)
          when ::GitHub::Unsullied::Wiki
            ::Spokes::Repository.new(
              nwo: repository.nwo,
              type: ::Spokes::Repository::RepositoryType::WIKI,
              networkId: repository.network.id,
              repositoryId: repository.id)
          when Gist, Archived::Gist
            ::Spokes::Repository.new(
              nwo: repository.name_with_owner,
              type: ::Spokes::Repository::RepositoryType::GIST,
              repositoryId: repository.id)
          when RepositoryNetwork
            ::Spokes::Repository.new(
              nwo: repository.nwo,
              type: ::Spokes::Repository::RepositoryType::NETWORK,
              networkId: repository.id)
          else
            raise ArgumentError, "Unsupported repository type #{repository}"
          end
        end

        def client
          @client ||= build_client
        end

        def build_client
          ssl = nil

          certs = GitHub.spokesd_certs
          unless certs.nil?
            ssl = {
              ca_file: certs[0],
              client_key: certs[1],
              client_cert: certs[2],
            }
          end

          conn = ::Faraday.new(GitHub.spokesd_url, ssl: ssl) do |conn|
            conn.options[:open_timeout] = 0.250
            # babeld and codeload have a dependency on this and have a
            # timeout of 5 seconds.  Keep this lower than that, so we
            # can respond to them even if we time out on spokesd.
            conn.options[:timeout]      = 4.0
            conn.headers[:user_agent]   = "GitHub::Spokes::Client github-#{GitHub.role} (#{GitHub.current_sha})"

            conn.request :retry,
              max:                 3,
              interval:            0.050,
              interval_randomness: 0.5,
              backoff_factor:      1.2,

              # What methods should faraday attempt a retry?
              # In Twirp, everything is a POST...
              methods:     [:post],
              exceptions:  [Faraday::ConnectionFailed, Faraday::RetriableResponse, Faraday::TimeoutError],
              retry_block: proc { GitHub.dogstats.increment("rpc.#{SERVICE_NAME}.retries") }

            conn.use ::GitHub::FaradayMiddleware::RequestID
            conn.use ::GitHub::FaradayMiddleware::Tracer,
              service_name: SERVICE_NAME,
              parent_span: proc { GitHub.tracer.last_span },
              tracer: GitHub.tracer,
              # If we don't specify the operation, it defaults to "POST", in
              # which case LightStep produces an operation that filters the last
              # component of the URL.  Since in Twirp, that's the RPC method
              # name, which we want to keep, we set the operation explicitly
              # ourselves.
              operation: proc { |env| "#{env[:method].to_s.upcase} #{URI(env[:url]).path}" }
            conn.use ::GitHub::FaradayMiddleware::Datadog, stats: GitHub.dogstats, service_name: SERVICE_NAME
            conn.use Faraday::Resilient, name: SERVICE_NAME
            conn.use ::GitHub::FaradayMiddleware::IncreasingTimeout,
              factor: 2

            conn.adapter :typhoeus
          end

          ::Spokes::SpokesClient.new(conn)
        end
      end
    end
  end
end
