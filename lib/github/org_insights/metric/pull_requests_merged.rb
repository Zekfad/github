# frozen_string_literal: true

module GitHub
  module OrgInsights
    class Metric::PullRequestsMerged < GitHub::OrgInsights::Metric
      self.model = PullRequest
      self.column = :merged_at
      self.metric_name = GitHub::OrgInsights::Metric::PULL_REQUEST_MERGED

      def all
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          relation
            .for_owner(@org_id)
            .group("repository_id")
            .bucketed_by(fully_qualified_column, timespan)
            .count
        end
      end
    end
  end
end
