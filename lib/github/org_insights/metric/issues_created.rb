# frozen_string_literal: true

module GitHub
  module OrgInsights
    class Metric::IssuesCreated < GitHub::OrgInsights::Metric
      self.model = Issue
      self.metric_name = GitHub::OrgInsights::Metric::ISSUE_CREATED

      def all
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          relation.where(pull_request_id: nil)
            .for_owner(@org_id)
            .group("repository_id")
            .bucketed_by(fully_qualified_column, timespan)
            .count
        end
      end
    end
  end
end
