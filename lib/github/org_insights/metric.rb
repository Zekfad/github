# frozen_string_literal: true

module GitHub
  module OrgInsights
    module BucketedBy
      def bucketed_by(column, timespan)
        where(column => timespan.to_range)
          .group(timespan.group_by(column))
          .order(column)
      end

    end

    # Mixin module for extending AR scopes/relations
    module ForRepoOwner
      # Restricts the query to include the given owner_id.
      # May optionally provide a custom association on which to join
      def for_owner(owner_id, join = :repository)
        joins(join).where(repositories: { owner_id: owner_id })
      end
    end

    class Metric
      ISSUE_CREATED               = "IssueCreated"
      ISSUE_CLOSED                = "IssueClosed"
      PULL_REQUEST_CREATED        = "PullRequestCreated"
      PULL_REQUEST_MERGED         = "PullRequestMerged"
      PULL_REQUEST_CLOSED         = "PullRequestClosed"
      PULL_REQUEST_REVIEWED       = "PullRequestReviewed"
      COMMIT_CREATED              = "CommitCreated"
      INTEGRATION_INSTALLED       = "IntegrationInstalled"
      OAUTH_AUTHORIZATION_CREATED = "OauthAuthorizationCreated"

      class_attribute :metric_name
      class_attribute :model
      class_attribute :column, default: :created_at

      attr_reader :org_id, :timespan

      # Lookup method to find the query subclass by name
      # Underscores first so that we can accept underscored or camelized variants.
      # FooBar => GitHub::OrgInsights::Metric::FooBar
      # foo_bar => GitHub::OrgInsights::Metric::FooBar
      def self.[](subclass_name)
        const_get subclass_name.to_s.classify
      end

      def self.subclasses
        %w[
          CommitsContributed
          IssuesClosed
          IssuesCreated
          PullRequestReviewsCreated
          PullRequestsCreated
          PullRequestsMerged
          PullRequestsClosed
        ]
      end

      def self.tracking_name
        eventer_name.underscore
      end

      def self.eventer_name
        name.demodulize
      end

      def initialize(org_id)
        @org_id = org_id
        @org = Organization.find(org_id)
        @timespan = Eventer::Timespan::FullPeriod.new
      end

      def fully_qualified_column
        "#{model.table_name}.#{column}"
      end

      def relation
        model.all.extending(BucketedBy, ForRepoOwner)
      end

      def as_json
        all_dates = Hash.new(0)
        all_metrics = []
        repo_metrics = all.each do |k, total|
          repo_id = k[0]
          date = k[1]

          all_dates[date] += total

          next unless repo_id && date

          repo = Repository.new(id: repo_id)

          all_metrics << Eventer::Messages::SetCounter.build(
            self.class.metric_name,
            owner: repo,
            timestamp: date,
            delta: total,
          ).first
          # can take first here since there's only one owner, ie one message
        end

        org_metrics = all_dates.each do |date, total|
          all_metrics << Eventer::Messages::SetCounter.build(
            self.class.metric_name,
            owner: @org,
            timestamp: date,
            delta: total,
          ).first
          # can take first here since there's only one owner, ie one message
        end

        all_metrics.compact
      end
    end
  end
end
