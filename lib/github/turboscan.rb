# frozen_string_literal: true
require "turboscan"
require "date"

module GitHub
  class Turboscan
    SERVICE_NAME = "turboscan"
    FAILBOT_APP_NAME = "github-turboscan-client"

    READ_TIMEOUT = 3.0 # seconds
    UPLOAD_TIMEOUT = 28.0 # seconds
    UPLOAD_MAX_SIZE = 10.megabytes

    ERRORS_TO_IGNORE = [
      Timeout::Error,
      Faraday::TimeoutError,
      Faraday::SSLError,
      Faraday::ClientError,
      Faraday::ConnectionFailed,
    ].freeze

    CLOSED_REASONS = {
      FALSE_POSITIVE: "false positive",
      WONT_FIX: "won't fix",
      USED_IN_TESTS: "used in tests"
    }.freeze

    class ResultCount
      attr_reader :count

      def initialize(count)
        @count = count
      end
    end

    def self.connection
      @connection ||= Faraday.new(url: GitHub.turboscan_url) do |conn|
        conn.use GitHub::FaradayMiddleware::RequestID
        conn.use GitHub::FaradayMiddleware::HMACAuth, hmac_key: GitHub.turboscan_hmac_key
        conn.use GitHub::FaradayMiddleware::Datadog, stats: GitHub.dogstats, service_name: SERVICE_NAME
        conn.use GitHub::FaradayMiddleware::Tracer,
          service_name: SERVICE_NAME,
          parent_span: proc { GitHub.tracer.last_span },
          tracer: GitHub.tracer,
          # If we don't specify the operation, it defaults to "POST", in
          # which case LightStep produces an operation that filters the last
          # component of the URL.  Since in Twirp, that's the RPC method
          # name, which we want to keep, we set the operation explicitly
          # ourselves.
          operation: proc { |env| "#{env[:method].to_s.upcase} #{URI(env[:url]).path}" }
        conn.use GitHub::FaradayMiddleware::Staffbar, url: GitHub.turboscan_url
        conn.use Faraday::Resilient, name: SERVICE_NAME, options: {
          instrumenter: GitHub,
          sleep_window_seconds: 10,
          error_threshold_percentage: 5,
          window_size_in_seconds: 30,
          bucket_size_in_seconds: 5,
        }
        conn.options[:open_timeout] = 0.1 # connection open timeout in seconds.
        conn.options[:timeout] = READ_TIMEOUT
        conn.adapter :persistent_excon
      end
    end

    def self.with_error_reporting
      response = yield
      if response.nil?
        send_message_to_failbot("Nil response from turboscan")
      elsif response.error && response.error.code != :not_found
        send_message_to_failbot("Error response from turboscan: #{response.error.msg}")
      end
      response
    rescue *ERRORS_TO_IGNORE
      nil
    rescue RuntimeError => e
      Failbot.report(e, { app: FAILBOT_APP_NAME })
      nil
    end

    def self.client
      @client ||= ::Turboscan::Client.new(connection)
    end

    def self.s3_client
      @s3_client ||= GitHub.s3_turboscan_client
    end

    def self.results(options)
      with_error_reporting { client.results(options) }
    end

    def self.counts(options)
      with_error_reporting { client.counts(options) }
    end

    def self.result(options)
      with_error_reporting { client.result(options) }
    end

    def self.code_paths(options)
      with_error_reporting { client.code_paths(options) }
    end

    def self.set_alert_status(options)
      with_error_reporting { client.set_alert_status(options) }
    end

    def self.resolve_result(options)
      with_error_reporting { client.resolve_result(options) }
    end

    def self.reopen_result(options)
      with_error_reporting { client.reopen_result(options) }
    end

    def self.rule_ids(options)
      with_error_reporting { client.rule_ids(options) }
    end

    def self.rules(options)
      with_error_reporting { client.rules(options) }
    end

    def self.rule_tags(options)
      with_error_reporting { client.rule_tags(options) }
    end

    def self.diff(options)
      with_error_reporting { client.diff(options) }
    end

    def self.annotations(options)
      with_error_reporting { client.annotations(options) }
    end

    def self.analyses(options)
      with_error_reporting { client.analyses(options) }
    end

    def self.upload_analysis(repo, params)
      # Send any errors to the github-turboscan-client bucket, rather
      # than the main dotcom one.
      Failbot.push(app: FAILBOT_APP_NAME)

      sarif = params.delete("sarif")

      # sarif content must be gzipped and base64 encoded. Comparing first bytes with gzip magic byte.
      decoded = Base64.decode64(sarif)
      if decoded.nil? || decoded.bytes.take(2) != [0x1f, 0x8b]
        return Faraday::Response.new(status: 400, body: {"msg": "Could not decode SARIF content. Expected Base64 encoded gzip'd file."})
      end
      if decoded.size > UPLOAD_MAX_SIZE
        return Faraday::Response.new(status: 413, body: {"msg": "SARIF content is too large"})
      end

      uri = self.upload_to_s3(self.s3_client, decoded, self.new_upload_path(repo))
      GitHub::Logger.log({fn: "Turboscan::upload_analysis",
                          repo: repo.id,
                          uri: uri})
      self.send_hydro_msg(repo, params, uri)
    end

    # Delegates the status report to all destinations
    def self.report_status(params)
      self.report_status_to_splunk(params)
      self.report_status_to_hydro(params)
      self.report_status_to_datadog(params)
    end

    def self.report_status_to_splunk(params)
      # Try to sanitize exceptions. We might want to move this to the logger
      params["exception"] = URI.encode_www_form_component(params["exception"]) if params["exception"].present?
      params["cause"] = URI.encode_www_form_component(params["cause"]) if params["cause"].present?

      GitHub::Logger.log({fn: "Turboscan::report_status"}.merge(params))
    end

    def self.report_status_to_hydro(params)
      GlobalInstrumenter.instrument("codescanning.status_report", params)
    end

    def self.report_status_to_datadog(params)
      tags = [
          "action-oid:#{params["action_oid"]}",
          "languages:#{params["languages"]}",
          "action-name:#{params["action_name"]}",
          "status:#{params["status"]}",
        ]

      if params["status"] == "success" || params["status"] == "failure" || params["status"] == "aborted"
        # Log the success/failure/aborted event with datadog
        GitHub.dogstats.increment("codeql_action.count", tags: tags)
      end
      # Log the duration with datadog if we have both started_at and completed_at timestamps
      if "completed_at".in?(params)
        start_date = params["started_at"]
        complete_date = params["completed_at"]
        duration_ms = ((complete_date - start_date) * 24 * 60 * 60 * 1000).to_i
        GitHub.dogstats.timing("codeql_action.duration", duration_ms, tags: tags)
      end
      # TODO forward the report to turboscan
      # NB: when doing that, think about whether the response from turboscan should determine the response from dotcom.
    end


    def self.upload_to_s3(s3_client, sarif, target)
      start_time = GitHub::Dogstats.monotonic_time
      bucket = GitHub.turboscan_s3_bucket
      s3_client.put_object(
        body: sarif,
        bucket: bucket,
        key: target,
        server_side_encryption: GitHub.enterprise? ? nil : "aws:kms"
      )
      GitHub.dogstats.timing_since("turboscan_client.s3_upload_duration", start_time)
      target
    end

    def self.new_upload_path(repo)
      "upload/#{repo.id}/#{SimpleUUID::UUID.new.to_guid}.sarif.gz"
    end

    def self.send_hydro_msg(repo, params, uri)
      started_at = Time.parse(params["started_at"]) if params["started_at"]

      args = {
        repository_id: repo.id,
        sarif_uri: uri,
        commit_oid: params["commit_oid"],
        ref: params["ref"],
        analysis_name: params["analysis_name"],
        environment: params["environment"],
        checkout_uri: params["checkout_uri"],
        build_start_at: started_at,
        workflow_run_id: params["workflow_run_id"],
        analysis_key: params["analysis_key"],
        upload_started_at: params[:upload_started_at],
        upload_finished_at: params[:upload_finished_at],
        hydro_enqueued_at: Time.now,
        request_id: params[:request_id],
        repo_nwo: repo.nwo,
        source_repository_id: params[:source_repository_id],
      }

      GitHub.dogstats.increment("turboscan_client.upload_analysis")
      GlobalInstrumenter.instrument("code_scanning.new_analysis", args)
      Faraday::Response.new(status: 202, body: {})
    end

    def self.to_rule_severity(rule_severity)
      rule_severity = rule_severity.to_s.to_sym.upcase
      resolved_rule_severity = ::Turboscan::Proto::RuleSeverity.resolve(rule_severity)
      if resolved_rule_severity != ::Turboscan::Proto::RuleSeverity::NONE
        resolved_rule_severity
      end
    end

    def self.to_resolution_filter(resolution_filter_value)
      resolution = if resolution_filter_value.downcase == "fixed"
          # Alerts fixed (as opposed to closed by user)
          # don't have a manual resolution type set
          :FILTER_NO_RESOLUTION
        else
          ("FILTER_" + resolution_filter_value.upcase.gsub("-", "_")).to_sym
      end
      resolved_resolution = ::Turboscan::Proto::ResultResolutionFilter.resolve(resolution)
      if resolved_resolution != ::Turboscan::Proto::ResultResolutionFilter::FILTER_NONE
        resolved_resolution
      end
    end

    def self.tool_names(options)
      with_error_reporting { client.tool_names(options) }
    end

    def self.to_resolution(resolution)
      resolution = resolution.to_s.to_sym.upcase
      resolved = ::Turboscan::Proto::ResultResolution.resolve(resolution)
      if resolved != ::Turboscan::Proto::ResultResolution::NO_RESOLUTION
        resolved
      end
    end

    def self.connection_info
      @connection_info ||= ConnectionInfo.new(
        url: GitHub.turboscan_url,
        host: Addressable::URI.parse(GitHub.turboscan_url).host,
      )
    end

    def self.send_message_to_failbot(msg)
      error = StandardError.new msg
      error.set_backtrace(caller)
      Failbot.report(error, { app: FAILBOT_APP_NAME })
    end

    def self.api_resolution_reason(reason)
      CLOSED_REASONS[reason]
    end

    def self.resolution_reason_sym(reason)
      CLOSED_REASONS.invert[reason]
    end

    def self.to_alert_state_filter(state)
      # mangle the state string into a namespaced form
      case state
      when "dismissed"
        # 'resolved' is turboscan terminology for what we call 'dismissed' in the API
        state = "closed_resolved"
      when "fixed"
        state = "closed_fixed"
      end
      state = "alert_state_filter_#{state}"

      # default to all alerts
      ::Turboscan::Proto::AlertStateFilter.resolve(state.to_sym.upcase) ||
        ::Turboscan::Proto::AlertStateFilter::ALERT_STATE_FILTER_ALL
    end
  end
end
