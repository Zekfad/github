# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190509131612_subscribe_gist_authors.rb -v | tee -a /tmp/subscribe_gist_authors.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190509131612_subscribe_gist_authors.rb -v -w | tee -a /tmp/subscribe_gist_authors.log
#
module GitHub
  module Transitions
    class SubscribeGistAuthors < Transition
      READ_BATCH_SIZE = 100
      WRITE_BATCH_SIZE = 10
      # We enabled the feature for everyone on May 8th at 3:16pm CET
      CUT_OFF_TIME = Time.utc(2019, 5, 8, 14, 16, 0)

      # The query was executed manually on prod because it timed out
      #  SELECT COALESCE(MIN(id), 0) FROM gists WHERE user_id IS NOT NULL
      #  +----------------------+
      #  | COALESCE(MIN(id), 0) |
      #  +----------------------+
      #  |                    1 |
      #  +----------------------+
      #  1 row in set (53.58 sec)
      MIN_GIST_ID_PROD = 1

      # The query was executed manually on prod because it timed out
      # SELECT COALESCE(MAX(id), 0) FROM gists WHERE user_id IS NOT NULL AND created_at < '2019-05-08 14:16:00'
      #  +----------------------+
      #  | COALESCE(MAX(id), 0) |
      #  +----------------------+
      #  |             96109853 |
      #  +----------------------+
      #  1 row in set (1 min 22.41 sec)
      MAX_GIST_ID_PROD = 96109853

      attr_reader :iterator

      def after_initialize
        @iterator = ApplicationRecord::Domain::Gists.github_sql_batched_between(start: min_id, finish: max_id, batch_size: READ_BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT g.id, g.user_id FROM gists g
          WHERE g.user_id IS NOT NULL
            AND g.id BETWEEN :start AND :last
        SQL

        unless GitHub.enterprise?
          @iterator.add <<-SQL, cut_off_time: CUT_OFF_TIME
             AND g.created_at < :cut_off_time
          SQL
        end
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each_slice(WRITE_BATCH_SIZE) do |batched_gist_data|
          Newsies::ThreadSubscription.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            user_ids = batched_gist_data.map(&:second)

            # Filter batched_gist_data with users of type 'User'
            user_ids = readonly do
              User.with_ids(user_ids).where(type: "User").pluck(:id)
            end
            batched_gist_data = batched_gist_data.select { |row| row if user_ids.include?(row[1]) }

            batched_gist_data.each do |gist_data|
              log "subscribing User #{gist_data[1]} to Gist #{gist_data[0]}" if verbose?
              add_subscription gist_data unless dry_run?
            end
          end
        end
      end

      def add_subscription(gist_data)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = <<-SQL
            INSERT IGNORE INTO notification_thread_subscriptions
              (user_id, list_type, list_id, thread_key, reason, ignored, created_at)
            VALUES
              (:user_id, 'User', :user_id, :thread_key, 'author', 0, :created_at)
          SQL
          sql = Newsies::ThreadSubscription.github_sql.run(sql,
            user_id: gist_data[1],
            thread_key:  "Gist;#{gist_data[0]}",
            created_at: Time.now.utc)

          sql.affected_rows
        end
      end

      def min_id
        if !GitHub.enterprise? && !Rails.development?
          MIN_GIST_ID_PROD
        else
          readonly { ApplicationRecord::Domain::Gists.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM gists WHERE user_id IS NOT NULL") }
        end
      end

      def max_id
        base_sql = "SELECT COALESCE(MAX(id), 0) FROM gists WHERE user_id IS NOT NULL"
        if GitHub.enterprise?
          readonly { ApplicationRecord::Domain::Gists.github_sql.value(base_sql) }
        elsif Rails.development?
          readonly { ApplicationRecord::Domain::Gists.github_sql.value("#{base_sql} AND created_at < :cut_off_time", {cut_off_time: CUT_OFF_TIME}) }
        else
          MAX_GIST_ID_PROD
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::SubscribeGistAuthors.new(options)
  transition.run
end
