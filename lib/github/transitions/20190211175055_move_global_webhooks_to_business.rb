# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190211175055_move_global_webhooks_to_business.rb -v | tee -a /tmp/move_global_webhooks_to_business.log
#
module GitHub
  module Transitions
    class MoveGlobalWebhooksToBusiness < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        @iterator = Hook.github_sql_batched(limit: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id
          FROM hooks
          WHERE id > :last
            AND installation_target_type = "Global"
            AND installation_target_id = 0
          ORDER BY id
          LIMIT :limit
        SQL
      end

      # Returns nothing.
      def perform
        return unless GitHub.single_business_environment?

        business = readonly { GitHub.global_business }
        count = GitHub::SQL::Readonly.new(iterator.batches).sum do |rows|
          unless dry_run?
            Hook.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              Hook.where(id: rows.flatten)
                  .update_all(installation_target_id: business.id,
                              installation_target_type: "Business")
            end
          end

          rows.size
        end

        log "Updated #{count} hooks" if verbose?
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::MoveGlobalWebhooksToBusiness.new(options)
  transition.run
end
