# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200128183602_populate_xref_close_issue_references.rb -v | tee -a /tmp/populate_xref_close_issue_references.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200128183602_populate_xref_close_issue_references.rb -v -w | tee -a /tmp/populate_xref_close_issue_references.log
#
module GitHub
  module Transitions
    class PopulateXrefCloseIssueReferences < Transition

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      INSERT_XREF_QUERY = <<-SQL
        INSERT INTO close_issue_references
        (
          pull_request_id,
          issue_id,
          issue_repository_id,
          pull_request_author_id,
          actor_id,
          created_at,
          updated_at
        )
        VALUES :rows
        ON DUPLICATE KEY
        UPDATE id = id,
               issue_id = VALUES(issue_id),
               issue_repository_id = VALUES(issue_repository_id),
               updated_at = VALUES(updated_at)
      SQL

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM issues") }
        max_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM issues") }

        # Convert body to string as LOCATE does not perform case-insensitive match on blob column type.
        @iterator = ApplicationRecord::Domain::Repositories.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, pull_request_id, CONVERT(body USING utf8) as body_text FROM issues
          WHERE id BETWEEN :start AND :last
          AND pull_request_id IS NOT NULL
          AND body IS NOT NULL
          HAVING (
            LOCATE('fix', body_text) <> 0 OR
            LOCATE('close', body_text) <> 0 OR
            LOCATE('resolve', body_text) <> 0
          )
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        return if rows.empty?

        log "Populate xref for #{rows.size} pull requests." if verbose?
        return if dry_run?

        pull_requests = load_pull_requests_by_id(rows)
        # Build row data for xrefs
        xref_rows = []

        pull_requests.each do |pull_request|
          issue_ids = calculate_close_issues(pull_request)

          issue_ids.each do |issue_id|
            xref_rows << [
              pull_request.id,
              issue_id,
              pull_request.repository_id, # issue_repository_id
              pull_request.user_id, # pull_request_author_id
              pull_request.user_id, # actor_id
              pull_request.created_at,
              pull_request.updated_at,
            ]
          end
        end

        CloseIssueReference.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          run_batch_insert_xref xref_rows
        end unless xref_rows.empty?
      end

      private

      def load_pull_requests_by_id(rows)
        pull_request_ids = rows.map { |row| row[1] }
        readonly do
          PullRequest.where(id: pull_request_ids).
            includes(:repository, issue: [:repository, { events: :actor }])
        end
      end

      # We will be parsing issues from the same repo of the pull request
      # Supported markdown:
      #   gh-number
      #   #number
      #   user/projectgh-number
      #   user/project#number
      #   user/project/issues/number
      #   https://..../user/project/issues/number
      ISSUE_MENTION_MARKER = %r<(?:#|gh-|/(?:issues)/)(?=\d)>i
      CLOSE_KEYWORD_LEADER = /\b(?:(?:close[sd]?|fix(?:e[sd])?|resolve[sd]?)(?:\s+|\s*:\s*))/i
      NUMBER = /(\d+)\b/
      def calculate_close_issues(pull)
        return [] unless pull&.body =~ ISSUE_MENTION_MARKER

        repo_name_with_owner = /#{Regexp.escape(pull.repository.nwo)}/i

        # Try to match all:
        # - Fixes gh-number
        # - Fixes #number
        mention_with_number = /#{CLOSE_KEYWORD_LEADER}(?:gh-|#)#{NUMBER}/i
        issue_numbers = pull.body.scan(mention_with_number).flatten

        # Try to match all:
        # - Fixes user/project#number
        # - Fixes user/projectgh-number
        # - Fixes user/project/issues/number
        mention_with_partial = /#{CLOSE_KEYWORD_LEADER}#{repo_name_with_owner}#{ISSUE_MENTION_MARKER}#{NUMBER}/i
        issue_numbers.concat pull.body.scan(mention_with_partial).flatten

        # Try to match all:
        # - Fixes https://.../user/project/issues/number
        mention_with_url = /#{CLOSE_KEYWORD_LEADER}#{Regexp.escape(GitHub.url)}\/#{repo_name_with_owner}\/issues\/#{NUMBER}/i
        issue_numbers.concat pull.body.scan(mention_with_url).flatten

        return [] if issue_numbers&.empty?

        readonly do
          ApplicationRecord::Domain::Repositories.github_sql.values <<-SQL, repository_id: pull.repository_id, issue_numbers: issue_numbers
            SELECT id FROM issues
            WHERE pull_request_id IS NULL
            AND repository_id = :repository_id
            AND number IN :issue_numbers
          SQL
        end
      end

      def run_batch_insert_xref(xref_rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          ApplicationRecord::Collab.github_sql.run INSERT_XREF_QUERY, rows: GitHub::SQL::ROWS(xref_rows)
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::PopulateXrefCloseIssueReferences.new(options)
  transition.run
end
