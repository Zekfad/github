# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190116040555_disable_all_service_hooks.rb -v | tee -a /tmp/disable_all_service_hooks.log
#
module GitHub
  module Transitions
    class DisableAllServiceHooks < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { Hook.github_sql.value("SELECT id FROM hooks WHERE active = 1 AND name NOT IN ('web', 'email') ORDER BY id ASC LIMIT 1") }
        max_id = readonly { Hook.github_sql.value("SELECT id FROM hooks WHERE active = 1 AND name NOT IN ('web', 'email') ORDER BY id DESC LIMIT 1") }

        @iterator = Hook.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id,name FROM hooks
          WHERE id BETWEEN :start AND :last
          AND active = 1
          AND name NOT IN ('web', 'email')
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        log_affected_rows(rows) if verbose
        return if dry_run

        ids = rows.map(&:first)
        Hook.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          Hook.github_sql.run <<-SQL, ids: ids
            UPDATE hooks SET active = 0 WHERE id IN :ids
          SQL
        end
      end

      private

      def log_affected_rows(rows)
        log("Updating the following batch:")
        rows.each { |id, name| log("ID: #{id}  SERVICE NAME: #{name}") }
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DisableAllServiceHooks.new(options)
  transition.run
end
