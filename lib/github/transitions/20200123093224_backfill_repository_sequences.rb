# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200123093224_backfill_repository_sequences.rb -v | tee -a /tmp/backfill_repository_sequences.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200123093224_backfill_repository_sequences.rb -v -w | tee -a /tmp/backfill_repository_sequences.log
#
module GitHub
  module Transitions
    class BackfillRepositorySequences < Transition
      class Sequence < ApplicationRecord::Domain::Sequences; end
      class RepositorySequence < ApplicationRecord::Domain::Repositories; end

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 10

      attr_reader :iterator

      def after_initialize
        @start = readonly { Sequence.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM `sequences`") }
        @finish = readonly { Sequence.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM `sequences`") }
      end

      def batch_size
        @other_args[:"batch-size"] || BATCH_SIZE
      end

      def archived?
        @other_args[:archived]
      end

      def verify?
        @other_args[:verify]
      end

      def run_query(start:, last:)
        if verify?
          query_string = if archived?
            <<~SQL
              SELECT
                `sequences`.`id` as t1_id,
                `sequences`.`context_id` as t1_repository_id,
                `sequences`.`number` as t1_number,
                `sequences`.`created_at` as t1_created_at,
                `sequences`.`updated_at` as t1_updated_at,
                `archived_repository_sequences`.`id` as t2_id,
                `archived_repository_sequences`.`repository_id` as t2_repository_id,
                `archived_repository_sequences`.`number` as t2_number,
                `archived_repository_sequences`.`created_at` as t2_created_at,
                `archived_repository_sequences`.`updated_at` as t2_updated_at
              FROM `sequences`
              LEFT OUTER JOIN `repositories` ON
                `sequences`.`context_id` = `repositories`.`id` AND
                `sequences`.`context_type` = 'Repository'
              LEFT OUTER JOIN `archived_repositories` ON
                `sequences`.`context_id` = `archived_repositories`.`id` AND
                `sequences`.`context_type` = 'Repository'
              LEFT OUTER JOIN `archived_repository_sequences` ON
                `sequences`.`context_id` = `archived_repository_sequences`.`repository_id` AND
                `sequences`.`context_type` = 'Repository'
              WHERE
                `sequences`.`id` BETWEEN :start AND :last AND
                `sequences`.`context_type` = 'Repository' AND (
                  (
                    /* if repo and archived repo does not exist, we don't care */
                    `repositories`.`id` IS NOT NULL AND
                    `archived_repositories`.`id` IS NOT NULL
                  ) OR (
                    /* if repo does not exist but archived repo exists, archived repo sequence does have to exist */
                    `repositories`.`id` IS NULL AND
                    `archived_repositories`.`id` IS NOT NULL AND
                    `archived_repository_sequences`.`id` IS NOT NULL
                  )
                )
              ORDER BY `sequences`.`id` ASC;
              /* cross-schema-domain-query-exempted */
            SQL
          else
            <<~SQL
              SELECT
                `sequences`.`id` as t1_id,
                `sequences`.`context_id` as t1_repository_id,
                `sequences`.`number` as t1_number,
                `sequences`.`created_at` as t1_created_at,
                `sequences`.`updated_at` as t1_updated_at,
                `repository_sequences`.`id` as t2_id,
                `repository_sequences`.`repository_id` as t2_repository_id,
                `repository_sequences`.`number` as t2_number,
                `repository_sequences`.`created_at` as t2_created_at,
                `repository_sequences`.`updated_at` as t2_updated_at
              FROM `sequences`
              LEFT OUTER JOIN `repositories` ON
                `sequences`.`context_id` = `repositories`.`id` AND
                `sequences`.`context_type` = 'Repository'
              LEFT OUTER JOIN `repository_sequences` ON
                `sequences`.`context_id` = `repository_sequences`.`repository_id` AND
                `sequences`.`context_type` = 'Repository'
              WHERE
                `sequences`.`id` BETWEEN :start AND :last AND
                `sequences`.`context_type` = 'Repository' AND (
                  /* if repo exists, repo sequence does have to exist */
                  `repositories`.`id` IS NOT NULL AND
                  `repository_sequences`.`id` IS NOT NULL
                )
              ORDER BY `sequences`.`id` ASC
              /* cross-schema-domain-query-exempted */
            SQL
          end

          query = readonly { Sequence.github_sql.new(query_string, start: start, last: [last, @finish].min) }
          class_name = archived? ? "ArchivedRepositorySequence" : "RepositorySequence"

          query.hash_results.each do |row|
            if row["t2_id"].nil?
              log "Sequence##{row["t1_id"]} has no corresponding #{class_name}!"
            else
              equal = true
              equal &&= row["t1_repository_id"] == row["t2_repository_id"]
              equal &&= row["t1_number"] == row["t2_number"]
              equal &&= row["t1_created_at"] == row["t2_created_at"]
              equal &&= row["t1_updated_at"] == row["t2_updated_at"]

              log "Sequence##{row["t1_id"]} does not match #{class_name}##{row["t2_id"]}!" unless equal
            end
          end
        elsif dry_run?
          query_string = if archived?
            <<~SQL
              SELECT
                `sequences`.`context_id` as `repository_id`,
                `sequences`.`number`,
                `sequences`.`created_at`,
                `sequences`.`updated_at`
              FROM `sequences`
              LEFT OUTER JOIN `repositories` ON
                `sequences`.`context_id` = `repositories`.`id` AND
                `sequences`.`context_type` = 'Repository'
              LEFT OUTER JOIN `archived_repositories` ON
                `sequences`.`context_id` = `archived_repositories`.`id` AND
                `sequences`.`context_type` = 'Repository'
              WHERE
                `sequences`.`context_type` = 'Repository' AND `sequences`.`id` BETWEEN :start AND :last AND
                `repositories`.`id` IS NULL AND
                `archived_repositories`.`id` IS NOT NULL
              ORDER BY `sequences`.`id` ASC
            SQL
          else
            <<~SQL
              SELECT
                `context_id` as `repository_id`,
                `number`,
                `created_at`,
                `updated_at`
              FROM `sequences`
              WHERE
                `sequences`.`context_type` = 'Repository' AND `sequences`.`id` BETWEEN :start AND :last
              ORDER BY `sequences`.`id` ASC
            SQL
          end

          readonly { Sequence.github_sql.run(query_string, start: start, last: [last, @finish].min) }
        else
          query_string = if archived?
            <<~SQL
              INSERT INTO `archived_repository_sequences` (
                `repository_id`,
                `number`,
                `created_at`,
                `updated_at`
              ) SELECT
                `sequences`.`context_id` as `repository_id`,
                `sequences`.`number`,
                `sequences`.`created_at`,
                `sequences`.`updated_at`
              FROM `sequences`
              LEFT OUTER JOIN `repositories` ON
                `sequences`.`context_id` = `repositories`.`id` AND
                `sequences`.`context_type` = 'Repository'
              LEFT OUTER JOIN `archived_repositories` ON
                `sequences`.`context_id` = `archived_repositories`.`id` AND
                `sequences`.`context_type` = 'Repository'
              WHERE
                `sequences`.`context_type` = 'Repository' AND `sequences`.`id` BETWEEN :start AND :last AND
                `repositories`.`id` IS NULL AND
                `archived_repositories`.`id` IS NOT NULL
              ON DUPLICATE KEY UPDATE
                `number` = `sequences`.`number`,
                `created_at` = `sequences`.`created_at`,
                `updated_at` = `sequences`.`updated_at`
              /* cross-schema-domain-query-exempted */
            SQL
          else
            <<~SQL
              INSERT INTO `repository_sequences` (
                `repository_id`,
                `number`,
                `created_at`,
                `updated_at`
              ) SELECT
                `sequences`.`context_id` as `repository_id`,
                `sequences`.`number`,
                `sequences`.`created_at`,
                `sequences`.`updated_at`
              FROM `sequences`
              WHERE
                `sequences`.`context_type` = 'Repository' AND `sequences`.`id` BETWEEN :start AND :last
              ON DUPLICATE KEY UPDATE
                `number` = `sequences`.`number`,
                `created_at` = `sequences`.`created_at`,
                `updated_at` = `sequences`.`updated_at`
              /* cross-schema-domain-query-exempted */
            SQL
          end

          Sequence.github_sql.run(query_string, start: start, last: [last, @finish].min)
        end
      end

      # Returns nothing.
      def perform
        next_id = @start
        batch_count = 0

        loop do
          break if next_id > @finish

          if verbose && batch_count % 50 == 0
            log "Reached Sequence##{next_id}, Batch##{batch_count}"
          end

          query_block = -> { run_query start: next_id, last: (next_id + batch_size - 1) }

          if verify? || dry_run?
            query_block.call
          else
            RepositorySequence.throttle_with_retry(max_retry_count: 8, &query_block)
          end

          batch_count += 1
          next_id += batch_size
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output", default: false
    on "batch-size=", "Batch size", default: GitHub::Transitions::BackfillRepositorySequences::BATCH_SIZE, as: Integer
    on "archived", "Backfill archived repository sequences", default: false
    on "verify", "Verify that backfilled data is correct", default: false
  end
  options = slop.to_hash

  raise "Can't specify both `--verify` and `--write`" if options[:write] && options[:verify]

  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillRepositorySequences.new(options)
  transition.run
end
