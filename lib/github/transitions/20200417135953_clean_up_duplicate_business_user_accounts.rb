# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200417135953_clean_up_duplicate_business_user_accounts.rb --verbose | tee -a /tmp/clean_up_duplicate_business_user_accounts.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200417135953_clean_up_duplicate_business_user_accounts.rb --verbose -w | tee -a /tmp/clean_up_duplicate_business_user_accounts.log
#
module GitHub
  module Transitions
    class CleanUpDuplicateBusinessUserAccounts < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM businesses") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM businesses") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, slug FROM businesses
          WHERE id BETWEEN :start AND :last
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          duplicates_in_business = []
          accounts_linked_to_users = BusinessUserAccount.where(
            "business_id = ? AND user_id IS NOT NULL", item[0]
          ).find_each do |account|
            emails = account.user.emails.verified.pluck :email

            # Find BusinessUserAccounts in the same Business that are
            # duplicates.
            #
            # Duplicate BusinessUserAccounts are defined as those that:
            #
            # - Belong to the same Business as the specified account
            # - Are not the same BusinessUserAccount as the specified account
            # - Do not have an associated User
            # - Have a login value that case-insensitively matches the specified
            #   account's user's email
            #
            # See https://github.com/github/github/issues/140856.
            duplicates = BusinessUserAccount.where(
              "business_id = ? AND id != ? AND user_id IS NULL AND login IN (?)",
              account.business_id, account.id, emails
            ).find_each do |dupe|
              duplicates_in_business << dupe
              log "Found duplicate BusinessUserAccount `#{dupe.login}` for user `@#{account.user.login}` with emails `#{emails.join(",")}` in the `#{account.business.slug}` enterprise account" if verbose?

              unless dry_run?
                dupe.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
                  log "Deleting duplicate BusinessUserAccount with login `#{dupe.login}` and ID `#{dupe.id}`" if verbose?
                  dupe.destroy
                end
              end
            end
          end

          if duplicates_in_business.any?
            log "#{duplicates_in_business.size} total #{"duplicate".pluralize(duplicates_in_business.size)} in the `#{item[1]}` enterprise account" if verbose?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CleanUpDuplicateBusinessUserAccounts.new(options)
  transition.run
end
