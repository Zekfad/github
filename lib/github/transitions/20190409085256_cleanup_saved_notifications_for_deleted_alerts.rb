# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190409085256_cleanup_saved_notifications_for_deleted_alerts.rb -v | tee -a /tmp/cleanup_saved_notifications_for_deleted_alerts.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190409085256_cleanup_saved_notifications_for_deleted_alerts.rb -v -w | tee -a /tmp/cleanup_saved_notifications_for_deleted_alerts.log
#
module GitHub
  module Transitions
    # Prior to #103050 (this PR talks about repository invitations, but it fixed this too),
    # if a `RepositoryVulnerabilityAlert` notification had been saved for later, but then
    # accepted, we wouldn't remove the saved notification. This caused 500s on trying to
    # render the saved notification UI.
    #
    # This transition finds all saved notifications for `RepositoryVulnerabilityAlert`s,
    # and removes them if the actual `RepositoryVulnerabilityAlert` no longer exists.
    class CleanupSavedNotificationsForDeletedAlerts < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { Newsies::SavedNotificationEntry.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM saved_notification_entries") }
        max_id = readonly { Newsies::SavedNotificationEntry.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM saved_notification_entries") }

        @iterator = Newsies::SavedNotificationEntry.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, thread_key FROM saved_notification_entries
          WHERE id BETWEEN :start AND :last
          AND thread_key LIKE "RepositoryVulnerabilityAlert;%"
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |(id, thread_key)|
          thread_type, alert_id = thread_key.split(";")

          alert_exists = readonly do
            RepositoryVulnerabilityAlert.where(id: alert_id).exists?
          end
          next if alert_exists

          if dry_run?
            puts "Would delete saved_notification_entry: #{id}"
            next
          end

          puts "Deleting saved_notification_entry: #{id}" if verbose?
          Newsies::SavedNotificationEntry.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            Newsies::SavedNotificationEntry.github_sql.run("DELETE FROM saved_notification_entries WHERE id = :id", id: id)
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CleanupSavedNotificationsForDeletedAlerts.new(options)
  transition.run
end
