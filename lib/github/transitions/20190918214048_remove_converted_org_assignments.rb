# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190918214048_remove_converted_org_assignments.rb -v | tee -a /tmp/remove_converted_org_assignments.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190918214048_remove_converted_org_assignments.rb -v -w | tee -a /tmp/remove_converted_org_assignments.log
#
module GitHub
  module Transitions
    # Related issue: https://github.com/github/github/issues/103742
    #
    # The idea here is to remove all Assignment records where the User
    # associated with the Issue is now an Organization. This should not be
    # possible in the current state of the codebase, but we have some bad older
    # data.
    class RemoveConvertedOrgAssignments < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        @min_id = @other_args[:start_id] || readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM assignments") }
        @max_id = @other_args[:end_id] || readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM assignments") }
        @batch_size = @other_args[:batch_size] || BATCH_SIZE
        log "Starting with ID #{@min_id} and finishing with ID #{@max_id}. Batch size is #{@batch_size}." if verbose?

        @iterator = ApplicationRecord::Domain::Repositories.github_sql_batched_between(start: @min_id, finish: @max_id, batch_size: @batch_size)
        @iterator.add <<-SQL
          SELECT id, assignee_id FROM assignments
          WHERE id BETWEEN :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        log "DRY_RUN" if verbose? && dry_run?
        total_records = 0
        @org_id_cache = {}

        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          rows.each do |row|
            assignment_id, user_id = row
            next unless user_is_org?(user_id)

            @org_id_cache[user_id] ||= []
            @org_id_cache[user_id] << assignment_id
          end

          assignment_ids = @org_id_cache.values.flatten
          total_records += assignment_ids.size
          reset_cache_values
          process(assignment_ids)
        end

        log "All done: #{total_records} processed" if verbose?
      end

      def process(ids)
        Assignment.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "deleting #{ids.length} orphans with ids: #{ids}" if verbose? && ids.length > 0
          return if dry_run?

          ActiveRecord::Base.connected_to(role: :writing) do
            Assignment.where(id: ids).destroy_all
          end
        end
      end

      def user_is_org?(user_id)
        return true if @org_id_cache.key?(user_id)

        readonly do
          Organization.find_by(id: user_id)
        end
      end

      # Let's not keep the assignment ids around and blow up memory. Keep the
      # cached user_ids but blow away the values
      def reset_cache_values
        @org_id_cache = @org_id_cache.each_with_object({}) do |(key, _val), hsh|
          hsh[key] = []
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "start_id=", "Assignment ID to start processing at", as: Integer
    on "end_id=", "Assignment ID to end processing at", as: Integer
    on "batch_size=", "Number of rows to process at a time", as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RemoveConvertedOrgAssignments.new(options)
  transition.run
end
