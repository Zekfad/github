# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191022124901_backfill_network_replica_ids.rb -v | tee -a /tmp/backfill_network_replica_ids.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191022124901_backfill_network_replica_ids.rb -v -w | tee -a /tmp/backfill_network_replica_ids.log
#
module GitHub
  module Transitions
    class BackfillNetworkReplicaIds < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      # attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Spokes.github_sql.value("SELECT COALESCE(MIN(repository_id), 0) FROM repository_replicas") }
        max_id = readonly { ApplicationRecord::Spokes.github_sql.value("SELECT COALESCE(MAX(repository_id), 0) FROM repository_replicas") }

        # We scan over the `repository_replicas` table by `repository_id` (not `id`).  We expect 4 (or maybe 8, or 10? Less
        # than a dozen, anyway) rows per `repository_id`, and we prefer to process them all together, so we avoid `LIMIT n`
        # which might slice these groups.
        @iterator = ApplicationRecord::Spokes.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
           SELECT repository_id, id, host FROM repository_replicas
           WHERE repository_id BETWEEN :start AND :last
           AND network_replica_id IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(@iterator.batches).each do |rows|
          log "Processing batch of #{rows.size}" if verbose?
          process(rows)
        end
      end

      def process(rows)
        # Group the replicas together by repo id
        # By dealing with all the replicas for a particular repository
        # at the same time, we reduce some overheads
        rs = rows.group_by { |repo_id, _, _| repo_id }

        # We need to look up the network that each repository is from
        # The most stable abstraction is probably just loading all the repository
        # models and then using `network_id` method!
        log "retrieving networks for #{rs.size} repositories" if verbose?
        network_map = readonly do
          Repository.where(id: rs.keys).each_with_object({}) do |r, hash|
            hash[r.id] = r.network_id
          end
        end
        log "network_map has #{network_map.size} elements" if verbose?

        # Now find all the network replicas that we will need to match against
        # Luckily, we already have a batched function in `DGit::Routing` to do
        # this for us
        networks = network_map.values.uniq
        log "retrieving network replicas for #{networks.size} networks" if verbose?
        network_replicas = readonly do
          GitHub::DGit::Routing.all_network_replicas_for_many(networks)
        end
        log "found network_replicas for #{network_replicas.size} networks" if verbose?

        # We are going to record all the updates we need to perform ready
        # to run them at the end of this chunk
        updates = []

        # For each repository, try to match the repository replicas we
        # have against the network replicas.  Then we can record the ids
        # that we've determined
        rs.each do |repo_id, repo_replicas|
          # Find network for repo
          nw = network_map[repo_id]
          if nw.nil?
            log "No network for #{repo_id} - skipping" if verbose?
            next
          end

          # Match the replicas and record db updates
          repo_replicas.each do |_, repl_id, host|
            nr = network_replicas[nw].find { |nr| nr.fileserver.name == host }
            if nr
              # Record that this repository replica matches that network replica
              updates << [repl_id, nr.db_network_replica_id]
            else
              log "No network replica for #{nw}/#{repo_id} on #{host}" if verbose?
            end
          end
        end

        # Update spokesdb with all the matched replica information
        if !updates.empty?
          ApplicationRecord::Domain::Spokes.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "updating #{updates.size} replicas" if verbose?
            run_batch_update updates unless dry_run?
          end
        end
      end

      def run_batch_update(items)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Spokes.github_sql.new "UPDATE repository_replicas SET network_replica_id = CASE"

          items.each do |item|
            rr_id = item[0]
            nr_id = item[1]
            sql.add <<-SQL, id: rr_id, network_replica_id: nr_id
              WHEN id = :id THEN :network_replica_id
            SQL
          end

          sql.add "END WHERE id IN :ids AND network_replica_id IS NULL", ids: items.map { |item| item[0] }
          sql.run
          sql.affected_rows
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillNetworkReplicaIds.new(options)
  transition.run
end
