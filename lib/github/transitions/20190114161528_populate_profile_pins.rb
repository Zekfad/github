# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190114161528_populate_profile_pins.rb -v | tee -a /tmp/populate_profile_pins.log
#
module GitHub
  module Transitions
    class PopulateProfilePins < Transition
      BATCH_SIZE = 1_000

      INSERT_QUERY = <<-SQL
        INSERT INTO profile_pins
        (profile_id, pinned_item_id, pinned_item_type, position)
        VALUES
        (:profile_id, :repo_id, 0, :position)
        ON DUPLICATE KEY
        UPDATE id = id, position = VALUES(position)
      SQL

      attr_reader :iterator, :count, :min_id, :max_id

      def after_initialize
        @count = 0

        @min_id = readonly do
          ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM profile_pinned_repositories")
        end
        @max_id = readonly do
          ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM profile_pinned_repositories")
        end

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: @min_id, finish: @max_id,
                                                    batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT profile_pinned_repositories.id,
                 profile_pinned_repositories.profile_id,
                 profile_pinned_repositories.repository_id,
                 profile_pinned_repositories.position
          FROM profile_pinned_repositories
          LEFT OUTER JOIN profile_pins
          ON profile_pins.pinned_item_type = 0
          AND profile_pins.pinned_item_id = profile_pinned_repositories.repository_id
          AND profile_pins.profile_id = profile_pinned_repositories.profile_id
          AND profile_pins.position = profile_pinned_repositories.position
          WHERE profile_pinned_repositories.id BETWEEN :start AND :last
          AND profile_pins.id IS NULL
          ORDER BY profile_pinned_repositories.id
        SQL
      end

      # Returns nothing.
      def perform
        if verbose
          log "Updating profile_pins based on profile_pinned_repositories..."
          log "Starting with ID #{min_id} and going to ID #{max_id} with a batch size " \
              "of #{BATCH_SIZE}"
        end
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
        if verbose
          log "Processed #{count} pinned repositories"
          log "There are #{ProfilePinnedRepository.count} profile_pinned_repositories and " \
              "#{ProfilePin.count} profile_pins"
        end
      end

      private

      def process(rows)
        rows.each do |row|
          ProfilePin.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            process_row(row) unless dry_run?
          end
        end
        @count += rows.size
        if verbose && @count % 5_000 == 0
          log "Processed #{@count} pinned repositories..."
        end
      end

      def process_row(row)
        _id, profile_id, repo_id, position = row
        ApplicationRecord::Domain::Users.github_sql.run(INSERT_QUERY, profile_id: profile_id, repo_id: repo_id,
                        position: position)
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::PopulateProfilePins.new(options)
  transition.run
end
