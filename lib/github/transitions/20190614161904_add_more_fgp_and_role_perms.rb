# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190614161904_add_more_fgp_and_role_perms.rb -v | tee -a /tmp/add_more_fgp_and_role_perms.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190614161904_add_more_fgp_and_role_perms.rb -v -w | tee -a /tmp/add_more_fgp_and_role_perms.log
#
module GitHub
  module Transitions
    class AddMoreFgpAndRolePerms < Transition

      ADD_TRIAGE_FGPS = %w(
        set_milestone
      )

      REMOVE_FGPS = %w(
        add_milestone
        remove_milestone
      )

      ADD_MAINTAIN_FGPS = %w(
        set_interaction_limits
        set_social_preview
        force_push_protected_branch
      )

      # Returns nothing.
      def perform
        unless dry_run?
          write_fine_grained_permissions
          add_role_permissions_for_triage
          add_role_permissions_for_maintain
          remove_old_milestone_permissions
          remove_duplicate_role_permissions
        end
      end

      def remove_duplicate_role_permissions
        role_permissions = RolePermission.where(action: "edit_repo_metadata")
        return unless role_permissions.count > 1

        role_permissions.first.destroy
      end

      def write_fine_grained_permissions
        fgps = ADD_TRIAGE_FGPS + ADD_MAINTAIN_FGPS
        fgps.each do |fgp|
          FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            FineGrainedPermission.create(action: fgp)
          end
        end
      end

      def add_role_permissions_for_triage
        triage_role = Role.where(name: "triage").first
        return unless triage_role
        ADD_TRIAGE_FGPS.each do |fgp|
          permission = FineGrainedPermission.where(action: fgp).first

          RolePermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            RolePermission.create(role_id: triage_role.id,
                                  fine_grained_permission_id: permission.id,
                                  action: permission.action)
          end
        end
      end

      def add_role_permissions_for_maintain
        maintain_role = Role.where(name: "maintain").first
        return unless maintain_role
        ADD_MAINTAIN_FGPS.each do |fgp|
          permission = FineGrainedPermission.where(action: fgp).first
          RolePermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            RolePermission.create(role_id: maintain_role.id,
                                  fine_grained_permission_id: permission.id,
                                  action: permission.action)
          end
        end
      end

      def remove_old_milestone_permissions
        FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          REMOVE_FGPS.each do |fgp|
            log "deleting the #{fgp} fgp and associated role permissions"
            FineGrainedPermission.where(action: fgp).destroy_all
          end
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddMoreFgpAndRolePerms.new(options)
  transition.run
end
