# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190507193011_backfill_enterprise_installation_user_licenses.rb -v | tee -a /tmp/backfill_enterprise_installation_user_licenses.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190507193011_backfill_enterprise_installation_user_licenses.rb -v -w | tee -a /tmp/backfill_enterprise_installation_user_licenses.log
#
module GitHub
  module Transitions
    class BackfillEnterpriseInstallationUserLicenses < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100
      EMAIL_EMAIL_INDEX = 1
      EMAIL_ENTERPRISE_INSTALLATION_ID_INDEX = 2

      attr_reader :iterator

      def after_initialize
        min_id = readonly { EnterpriseInstallationUserAccountEmail.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM enterprise_installation_user_account_emails") }
        max_id = readonly { EnterpriseInstallationUserAccountEmail.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM enterprise_installation_user_account_emails") }

        @iterator = EnterpriseInstallationUserAccountEmail.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE).tap do |i|
          i.add <<~SQL
            SELECT  e.id,
                    e.email,
                    a.enterprise_installation_id
            FROM    enterprise_installation_user_account_emails e
              INNER JOIN enterprise_installation_user_accounts a
                ON  e.enterprise_installation_user_account_id = a.id
            WHERE   e.id BETWEEN :start AND :last
              AND   e.primary = true

          SQL
        end
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |email_rows|
          log "ensuring licenses exist for enterprise installation user account email IDs: #{email_rows.map(&:first).join(", ")}" if verbose?

          email_to_enterprise_installation_id_mappings = email_to_enterprise_installation_id_mappings(email_rows)
          enterprise_installation_id_to_business_id_mappings = enterprise_installation_id_to_business_id_mappings(
            email_to_enterprise_installation_id_mappings.values,
          )

          email_to_business_id_mappings = email_to_business_id_mappings(
            email_to_enterprise_installation_id_mappings,
            enterprise_installation_id_to_business_id_mappings,
          )

          email_to_user_id_mappings = email_to_user_id_mappings(email_to_business_id_mappings.keys)

          upsert_user_based_user_licenses(
            user_based_user_license_rows(email_to_business_id_mappings, email_to_user_id_mappings),
          )

          upsert_email_based_user_licenses(
            email_based_user_license_rows(email_to_business_id_mappings, email_to_user_id_mappings),
          )
        end
      end

      private

      def email_to_enterprise_installation_id_mappings(email_rows)
        email_rows.each_with_object(Hash.new) do |row, mapping|
          mapping[row[EMAIL_EMAIL_INDEX]] = row[EMAIL_ENTERPRISE_INSTALLATION_ID_INDEX]
        end
      end


      def enterprise_installation_id_to_business_id_mappings(enterprise_installation_ids)
        rows = readonly do
          EnterpriseInstallation.github_sql.hash_results(<<~SQL, enterprise_installation_ids: enterprise_installation_ids)
            SELECT  id AS enterprise_installation_id,
                    owner_id AS business_id
            FROM    enterprise_installations
            WHERE   id IN :enterprise_installation_ids
              AND   owner_type = 'Business'
          SQL
        end

        rows.each_with_object(Hash.new) do |row, mapping|
          mapping[row["enterprise_installation_id"]] = row["business_id"]
        end
      end

      def email_to_business_id_mappings(email_to_enterprise_installation_id_mappings, relevant_enterprise_installation_rows)
        email_to_enterprise_installation_id_mappings.each_with_object(Hash.new) do |(email, enterprise_installation_id), mapping|
          mapping[email] = relevant_enterprise_installation_rows[enterprise_installation_id]
        end
      end

      def email_to_user_id_mappings(emails)
        rows = readonly do
          UserEmail.github_sql.hash_results(<<~SQL, emails: emails)
            SELECT  email,
                    user_id
            FROM    user_emails
            WHERE   email IN :emails
              AND   state = 'verified'
          SQL
        end

        rows.each_with_object(Hash.new) do |row, mapping|
          mapping[row["email"]] = row["user_id"]
        end
      end

      def user_based_user_license_rows(email_to_business_id_mappings, email_to_user_id_mappings)
        email_to_business_id_mappings.each_with_object(Array.new) do |(email, business_id), result|
          if email_to_user_id_mappings.key?(email)
            result.push(business_id: business_id, user_id: email_to_user_id_mappings[email])
          end
        end
      end

      def email_based_user_license_rows(email_to_business_id_mappings, email_to_user_id_mappings)
        email_to_business_id_mappings.each_with_object(Array.new) do |(email, business_id), result|
          if !email_to_user_id_mappings.key?(email)
            result.push(business_id: business_id, email: email)
          end
        end
      end

      def upsert_user_based_user_licenses(user_license_rows)
        return if user_license_rows.empty?

        UserLicense.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          insert_rows = user_license_rows.map do |row|
            [
              row[:business_id],
              row[:user_id],
              1, # enterprise
              Time.current,
              Time.current,
          ]
          end

          sql = UserLicense.github_sql.new(<<~SQL, rows: GitHub::SQL::ROWS(insert_rows))
            INSERT INTO user_licenses
              (business_id, user_id, license_type, created_at, updated_at)
            VALUES :rows
            ON DUPLICATE KEY UPDATE updated_at = CURRENT_TIMESTAMP
          SQL

          if dry_run?
            log "Would have upserted #{insert_rows.size} user based user licenses rows with:\n#{sql.query}"
          else
            sql.run
          end
        end
      end

      def upsert_email_based_user_licenses(user_license_rows)
        return if user_license_rows.empty?

        UserLicense.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          insert_rows = user_license_rows.map do |row|
            [
              row[:business_id],
              row[:email],
              1, # enterprise
              Time.current,
              Time.current,
          ]
          end

          sql = UserLicense.github_sql.new(<<~SQL, rows: GitHub::SQL::ROWS(insert_rows))
            INSERT INTO user_licenses
              (business_id, email, license_type, created_at, updated_at)
            VALUES :rows
            ON DUPLICATE KEY UPDATE updated_at = CURRENT_TIMESTAMP
          SQL

          if dry_run?
            log "Would have upserted #{insert_rows.size} emailed based user licenses rows with:\n#{sql.query}"
          else
            sql.run
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillEnterpriseInstallationUserLicenses.new(options)
  transition.run
end
