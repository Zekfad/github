# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200427160308_set_ghe_content_analysis_notifications_config.rb --verbose | tee -a /tmp/set_ghe_content_analysis_notifications_config.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200427160308_set_ghe_content_analysis_notifications_config.rb --verbose -w | tee -a /tmp/set_ghe_content_analysis_notifications_config.log
#
module GitHub
  module Transitions
    class SetGheContentAnalysisNotificationsConfig < Transition
      ANALYSIS_KEY = "ghe_content_analysis"
      NOTIFICATIONS_KEY = "ghe_content_analysis_notifications"

      # Returns nothing.
      def perform
        # Skip if ghe_content_analysis_notifications is explicitly set.
        notifications_configuration = GitHub.config.get(NOTIFICATIONS_KEY)
        unless notifications_configuration.nil?
          log <<~MSG
            SKIP: Configuration already set.
            - ghe_content_analysis_notifications: #{notifications_configuration.inspect}"
            MSG
          return
        end

        # Manage the ghe_content_analysis_notifications configuration.
        analysis_enabled = GitHub.config.enabled?(ANALYSIS_KEY)
        if analysis_enabled
          if dry_run?
            log "Would have enabled content analysis notifications."
          else
            log "Enabling content analysis notifications..."
            GitHub.config.enable(NOTIFICATIONS_KEY, GitHub.trusted_oauth_apps_owner)
            log "Done."
          end
        end

        # Double check that the two configurations match.
        notifications_enabled = GitHub.config.enabled?(NOTIFICATIONS_KEY)
        unless notifications_enabled == analysis_enabled
          log <<~MSG
            ERROR: Configuration mismatch!"
            - ghe_content_analysis: #{analysis_enabled ? "enabled" : "disabled"}
            - ghe_content_analysis_notifications: #{notifications_enabled ? "enabled" : "disabled"}

            To confirm your repository scanning configuration, visit: #{GitHub.scheme}://#{GitHub.host_name}/enterprises/YOUR-ORG/settings/dotcom_connection
            For more information, see: #{GitHub.help_url}/enterprise/admin/installation/enabling-security-alerts-for-vulnerable-dependencies-on-github-enterprise-server
            MSG
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::SetGheContentAnalysisNotificationsConfig.new(options)
  transition.run
end
