# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200428111954_clean_up_business_invitations.rb --verbose | tee -a /tmp/clean_up_business_invitations.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200428111954_clean_up_business_invitations.rb --verbose -w | tee -a /tmp/clean_up_business_invitations.log
#
module GitHub
  module Transitions
    class CleanUpBusinessInvitations < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM businesses") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM businesses") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id FROM businesses
          WHERE id BETWEEN :start AND :last
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          business = Business.find_by id: item[0]

          # Pending admin invitations for the enterprise account
          business.invitations.pending.find_each do |invitation|
            if invitation.inviter&.staff?
              log "Ignoring invitation with ID #{invitation.id} from @#{invitation.inviter.login} because they are GitHub staff"
              next
            end

            unless business.owner?(invitation.inviter)
              inviter = invitation.inviter.present? ? "@#{invitation.inviter.login}" : "deleted inviter"
              log "Found #{invitation.role} invitation from #{inviter} who is not an owner of the #{business.slug} enterprise" if verbose?
              unless dry_run?
                ActiveRecord::Base.connected_to(role: :writing) do
                  log "Cancelling #{invitation.role} invitation with ID #{invitation.id} from #{inviter} in the #{business.slug} enterprise" if verbose?
                  invitation.cancel actor: invitation.inviter, notify: false
                end
              end
            end
          end

          # Deliberately ignore pending organization invitations
          # (BusinessOrganizationInvitation) for the enterprise account, because
          # a previous version of this transition code confirmed that there are
          # no remaining pending organization invitations that need to be
          # cancelled, and enterprise account organization invitations are
          # currently not available to customers.
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CleanUpBusinessInvitations.new(options)
  transition.run
end
