# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200115181003_unlock_free_plans.rb -v | tee -a /tmp/unlock_free_plans.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200115181003_unlock_free_plans.rb -v -w | tee -a /tmp/unlock_free_plans.log
#
module GitHub
  module Transitions
    class UnlockFreePlans < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM users") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM users") }
        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)

        @iterator.add <<-SQL
          SELECT users.id
          FROM users
          WHERE users.disabled = true
          AND (users.plan = '#{GitHub::Plan.free}' OR users.plan = '#{GitHub::Plan.free_with_addons}')
          AND users.id BETWEEN :start AND :last
        SQL
      end

      def perform
        @total_unlocked_users = 0

        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end

        log_phrase = dry_run? ? "Would have unlocked" : "Unlocked"
        log "#{log_phrase} #{@total_unlocked_users} users" if verbose?
      end

      def process(user_ids)
        return if user_ids.empty?

        user_ids.each do |id|
          user = readonly { User.find_by(id: id) }

          next if !user || user.enabled?

          user.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log_phrase = dry_run? ? "Would have unlocked" : "Unlocking"
            log "#{log_phrase} user #{user.id}" if verbose?

            user.unlock_billing! unless dry_run?

            @total_unlocked_users += 1
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UnlockFreePlans.new(options)
  transition.run
end
