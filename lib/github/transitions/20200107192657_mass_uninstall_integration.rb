# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200107192657_mass_uninstall_integration.rb -v | tee -a /tmp/mass_uninstall_integration.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200107192657_mass_uninstall_integration.rb -v -w | tee -a /tmp/mass_uninstall_integration.log
#
module GitHub
  module Transitions
    class MassUninstallIntegration < Transition

      BATCH_SIZE = 100
      DEFAULT_LIMIT = 1_000

      attr_reader :iterator

      def after_initialize
        integration_id = other_args.fetch(:integration_id) do
          raise ArgumentError, "must provide an Integration ID"
        end

        limit = @other_args.fetch(:limit) { DEFAULT_LIMIT }

        min_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM integration_installations") }
        max_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM integration_installations") }

        @iterator = ApplicationRecord::Domain::Integrations.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL, integration_id: integration_id, limit: limit
          SELECT id FROM integration_installations
          WHERE id BETWEEN :start AND :last
          AND integration_installations.integration_id = :integration_id
          LIMIT :limit
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          installation_ids = rows.flatten
          installations = IntegrationInstallation.where(id: installation_ids)

          if verbose?
            message = dry_run? ? "Destroying installations #{installation_ids}" : "Found installations #{installation_ids}"
            log message
          end

          unless dry_run?
            IntegrationInstallation.throttle do
              ActiveRecord::Base.connected_to(role: :writing) do
                # We could run into issues here if we are also trying to destroy a bunch of
                # ScoepdIntegrationInstallation child records.
                #
                # We might want to look into skipping the `destroy`
                # on those records all together.
                installations.destroy_all
              end
            end
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "l=", "limit=", "Limits the number of records", as: Integer, default: 100
    on "id=", "integration_id=", "The primary database ID of the Integration", as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::MassUninstallIntegration.new(options)
  transition.run
end
