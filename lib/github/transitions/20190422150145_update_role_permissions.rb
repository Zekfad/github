# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190422150145_update_role_permissions.rb -v | tee -a /tmp/update_role_permissions.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190422150145_update_role_permissions.rb -v -w | tee -a /tmp/update_role_permissions.log
#
module GitHub
  module Transitions
    class UpdateRolePermissions < Transition

      MAINTAIN_FGPS = %w(
        manage_settings_wiki
        manage_settings_projects
        manage_settings_merge_types
        manage_settings_pages
        edit_repo_metadata
      )

      TRIAGE_FGPS = %w(
        request_pr_review
        mark_as_duplicate
        add_milestone
        remove_milestone
      )

      # Returns nothing.
      def perform
        remove_setup_issue_template_fgp

        write_fgps

        associate_maintain_fgps_with_role
        associate_triage_fgps_with_role
      end

      def remove_setup_issue_template_fgp
        log "deleting the setup_issue_template fgp and associated role permissions"
        unless dry_run?
          FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            FineGrainedPermission.where(action: "setup_issue_template").destroy_all
          end
        end
      end

      def write_fgps
        fgps = MAINTAIN_FGPS + TRIAGE_FGPS
        fgps.each do |fgp|
          log "writing #{fgp} to fine_grained_permissions table"

          unless dry_run?
            FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              FineGrainedPermission.create(action: fgp)
            end
          end
        end
      end

      def associate_maintain_fgps_with_role
        maintain_role = Role.where(name: "maintain").first
        return unless maintain_role

        MAINTAIN_FGPS.each do |fgp|
          permission = FineGrainedPermission.where(action: fgp).first
          return unless permission

          log "associating #{fgp} with the maintain role"
          unless dry_run?
            RolePermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              RolePermission.create(role_id: maintain_role.id,
                                    fine_grained_permission_id: permission.id,
                                    action: permission.action)
            end
          end
        end
      end

      def associate_triage_fgps_with_role
        triage_role = Role.where(name: "triage").first
        return unless triage_role

        TRIAGE_FGPS.each do |fgp|
          permission = FineGrainedPermission.where(action: fgp).first
          return unless permission

          log "associating #{fgp} with the triage role"
          unless dry_run?
            RolePermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              RolePermission.create(role_id: triage_role.id,
                                    fine_grained_permission_id: permission.id,
                                    action: permission.action)
            end
          end
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UpdateRolePermissions.new(options)
  transition.run
end
