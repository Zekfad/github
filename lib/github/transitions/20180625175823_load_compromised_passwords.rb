# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "digest/sha1"
require "slop"

# If you are wanting to load in data from a new version of the HIBP database,
# you'll first want to filter out data that was already present in the old
# version. To do this, first download the two versions of the database you wish
# to diff, making sure to get the one that is sorted by hash instead of by
# prevalence. Here are some torrent URLs in case those are hard to find in the
# future:
#
#   - V2: https://downloads.pwnedpasswords.com/passwords/pwned-passwords-ordered-2.0.txt.7z.torrent
#   - V3: https://downloads.pwnedpasswords.com/passwords/pwned-passwords-ordered-by-hash.7z.torrent
#   - V5: https://downloads.pwnedpasswords.com/passwords/pwned-passwords-sha1-ordered-by-hash-v5.7z
#
# Currently loaded version: V5
#
# Once downloaded, you can extract the two archives, diff them, and recompress
# the data using gzip with the following command, replacing old.7z and new.7z
# with the old and new versions of the database you downloaded:
#
#   comm -13 <(7z e -so ./old.7z | cut -d: -f1) <(7z e -so new.7z | cut -d: -f1) | gzip > diff.gz
#
# You'll then be able to provide the resulting diff.gz to this transition.
#
# To run this transition directly for HIBP data (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180625175823_load_compromised_passwords.rb -wf diff.gz --datasource HAVE_I_BEEN_PWNED | tee output.log
#
# This transition also supports loading plaintext wordlists, for example the crackstation wordlist:
#
#   - https://crackstation.net/buy-crackstation-wordlist-password-cracking-dictionary.htm
#
# To import a plaintext wordlist when running the transition add the -p flag. The transition will then
# compute the SHA1 hash before storing the value in the database
#
# To run this transition directly for plaintext data (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180625175823_load_compromised_passwords.rb -wfp <wordlist.gz> --datasource CRACKSTATION | tee output.log
#
module GitHub
  module Transitions
    class LoadCompromisedPasswords < Transition
      def after_initialize
        @plaintext = @other_args[:plaintext] || false
        @datasource = @other_args[:datasource]
        @version = @other_args[:version]

        # Stats
        @count = 0
        @start_count = (CompromisedPassword.last&.id).to_i
      end

      # An Enumerator over the input file we're processing.
      def input_line_enumerator
        @other_args[:reader].each_line
      end

      # An Enumerator over the parsed input file we're processing.
      # It ensures the input for the query is SHA-1 uppercase
      def parsed_input_line_enumerator
        input_line_enumerator.lazy.map do |line|
          stripped = line.to_s.strip
          if @plaintext
            Digest::SHA1.hexdigest(stripped).upcase
          else
            stripped.split(":")[0].upcase
          end
        end
      end

      def logging_sha1_password_enumerator
        parsed_input_line_enumerator.lazy.map do |sha1_password|
          @count += 1
          if @count % (1000 * CompromisedPasswordDatasource::LOAD_BATCH_SIZE) == 0
            batches = @count / CompromisedPasswordDatasource::LOAD_BATCH_SIZE
            log("#{@count} records loaded (#{batches} batches)")
          end

          sha1_password
        end
      end

      # Returns nothing.
      def perform
        if dry_run
          log("Dry run complete - no records inserted")
        else
          CompromisedPasswordDatasource.store_passwords(
            name: @datasource,
            version: @version,
            sha1_passwords: logging_sha1_password_enumerator,
          ).mark_import_as_finished!

          total = (CompromisedPassword.last&.id).to_i
          inserted = total - @start_count
          updated = @count - inserted
          log("#{@start_count} records before loading more data")
          log("#{inserted} records inserted")
          log("#{updated} records updated")
          log("#{total} total records")
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "f", "file=", "gzipfile"
    on "p", "plaintext", "Enable importing plaintext password lists", default: false
    on "d", "datasource=", "The source of the data", required: true
    on "z", "version=", "The source of the data", required: true
  end

  options = slop.to_hash
  file_reader = Zlib::GzipReader.open(options[:file], internal_encoding: "binary")
  options = options.merge(dry_run: !options[:write], reader: file_reader)

  transition = GitHub::Transitions::LoadCompromisedPasswords.new(options)
  transition.run
end
