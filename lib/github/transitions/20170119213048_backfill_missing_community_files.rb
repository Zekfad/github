# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20170119213048_backfill_missing_community_files.rb -v | tee -a /tmp/backfill_missing_community_files.log
#
module GitHub
  module Transitions
    class BackfillMissingCommunityFiles < Transition

      BATCH_SIZE = 100
      REDIS_KEY =  "coc-sequence:ModelIterator:CommunityProfile:current_for_health_files"

      def perform(dry_run = false)
        scope = CommunityProfile.where("has_license = ? AND created_at >= ?", false, Date.parse("1/11/2016")).includes(:repository)
        total_count = readonly { scope.count }
        processed = 0
        if dry_run || total_count == 0
          puts "Found #{total_count} profiles to scan."
        else
          max_id = CommunityProfile.github_sql.new("SELECT MAX(id) FROM community_profiles").value
          current_id.step(max_id, BATCH_SIZE) do |start|
            ending_id = start + BATCH_SIZE
            profiles = []
            readonly { profiles = scope.where("id >= ? AND id < ?", current_id, ending_id).to_a }
            detect_files(profiles)
            set_current_id(ending_id)
            processed += profiles.size
            report_progress(processed, total_count)
          end
        end
        clean_up_current_id
      end

      def report_progress(processed, total_count)
        percent = processed == 0 ? 0.0 : (processed.to_f / total_count) * 100
        progress = [processed, total_count, percent]
        message = "Updated %d of %d (%0.2f%%)"
        status message, *progress
      end

      def detect_files(profiles)
        profiles.each_slice(100).each do |group|
          CommunityProfile.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            now = GitHub::SQL::NOW
            rows = GitHub::SQL::ROWS(
              group.compact.map { |profile|
                begin
                  [
                    profile.id,
                    profile.repository_id,
                    !!(profile.repository && profile.repository.preferred_license),
                    now,
                    now,
                  ]
                rescue GitRPC::ObjectMissing
                  puts "Skipping repository #{profile.repository_id}"
                  [
                    profile.id,
                    profile.repository_id,
                    false,
                    now,
                    now,
                  ]
                end
              },
            )

            CommunityProfile.github_sql.run <<-SQL, rows: rows, now: now
              INSERT INTO
                community_profiles (id, repository_id, has_license, created_at, updated_at)
              VALUES
                :rows
              ON DUPLICATE KEY UPDATE
                has_license = VALUES(has_license),
                updated_at = VALUES(updated_at)
            SQL
          end
        end
      end

      def current_id
        (GitHub.kv.get(REDIS_KEY).value { 0 }).to_i
      end

      def set_current_id(repository_id)
        GitHub.kv.set(REDIS_KEY, repository_id.to_s)
      end

      def clean_up_current_id
        GitHub.kv.del(REDIS_KEY)
      end

    end

  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillMissingCommunityFiles.new(options)
  transition.run
end
