# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190501144552_backfill_outside_collaborator_invite_user_licenses.rb -v | tee -a /tmp/backfill_outside_collaborator_invite_user_licenses.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190501144552_backfill_outside_collaborator_invite_user_licenses.rb -v -w | tee -a /tmp/backfill_outside_collaborator_invite_user_licenses.log
#
module GitHub
  module Transitions
    class BackfillOutsideCollaboratorInviteUserLicenses < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      BATCH_SIZE = 100
      BUSINESS_ORGANIZATION_MEMBERSHIP_ORGANIZATION_ID_INDEX = 1
      BUSINESS_ORGANIZATION_MEMBERSHIP_BUSINESS_ID_INDEX = 2

      attr_reader :iterator

      def after_initialize
        min_id = readonly { Business::OrganizationMembership.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM business_organization_memberships") }
        max_id = readonly { Business::OrganizationMembership.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM business_organization_memberships") }

        @iterator = Business::OrganizationMembership.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE).tap do |i|
          i.add <<~SQL
            SELECT  id,
                    organization_id,
                    business_id
            FROM    business_organization_memberships
            WHERE   id BETWEEN :start AND :last
          SQL
        end
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |business_organization_membership_rows|
          log "ensuring licenses exist for business organization membership IDs: #{business_organization_membership_rows.map(&:first).join(", ")}" if verbose?

          organization_business_mappings = organization_business_mappings(business_organization_membership_rows)
          relevant_repository_invitation_rows = relevant_repository_invitation_rows(organization_business_mappings.keys)

          upsert_user_licenses(
            user_license_rows(organization_business_mappings, relevant_repository_invitation_rows),
          )
        end
      end

      private

      def organization_business_mappings(rows)
        rows.each_with_object(Hash.new) do |row, mapping|
          mapping[row[BUSINESS_ORGANIZATION_MEMBERSHIP_ORGANIZATION_ID_INDEX]] = row[BUSINESS_ORGANIZATION_MEMBERSHIP_BUSINESS_ID_INDEX]
        end
      end

      def relevant_repository_invitation_rows(organization_ids)
        return Array.new if organization_ids.empty?

        readonly do
          RepositoryInvitation.github_sql.hash_results(<<~SQL, organization_ids: organization_ids)
            SELECT  repository_invitations.invitee_id AS user_id,
                    repositories.owner_id AS organization_id
            FROM    repository_invitations
              INNER JOIN repositories ON
                repository_invitations.repository_id = repositories.id AND
                repositories.parent_id IS NULL AND
                repositories.public = false AND
                repositories.active = true
            WHERE owner_id IN :organization_ids
          SQL
        end
      end

      def user_license_rows(organization_business_mappings, repository_invitation_rows)
        repository_invitation_rows.each_with_object(Array.new) do |invitation_row, result|
          user_id = invitation_row["user_id"]
          organization_id = invitation_row["organization_id"]
          business_id = organization_business_mappings[organization_id]

          result.push("user_id" => user_id, "business_id" => business_id) unless business_id.nil?
        end
      end

      def upsert_user_licenses(rows)
        return if rows.empty?

        UserLicense.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          insert_rows = rows.map do |row|
            [
              row["business_id"],
              row["user_id"],
              1, # enterprise
              Time.current,
              Time.current,
          ]
          end

          UserLicense.github_sql.run(<<~SQL, rows: GitHub::SQL::ROWS(insert_rows)) unless dry_run?
            INSERT IGNORE INTO user_licenses
              (business_id, user_id, license_type, created_at, updated_at)
            VALUES :rows
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillOutsideCollaboratorInviteUserLicenses.new(options)
  transition.run
end
