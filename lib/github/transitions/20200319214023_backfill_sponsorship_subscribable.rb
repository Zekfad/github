# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200319214023_backfill_sponsorship_subscribable.rb -v | tee -a /tmp/backfill_sponsorship_subscribable.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200319214023_backfill_sponsorship_subscribable.rb -v -w | tee -a /tmp/backfill_sponsorship_subscribable.log
#
module GitHub
  module Transitions
    class BackfillSponsorshipSubscribable < Transition
      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        @count = 0

        min_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsorships") }
        max_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsorships") }

        @iterator = ApplicationRecord::Domain::Sponsors.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          -- id must go first for batched between to work properly
          SELECT id, subscription_item_id, subscribable_type, subscribable_id FROM sponsorships
          WHERE id BETWEEN :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end

        log "Found #{@count} sponsorships to update" if verbose?
      end

      def process(rows)
        # Sponsorship rows are tuples of: id, subscription_item_id, subscribable_type, subscribable_id
        subscription_item_ids = rows.map { |row| row[1] }
        subscription_items = ApplicationRecord::Domain::Users.github_sql.results(
          "SELECT id, subscribable_type, subscribable_id FROM subscription_items WHERE id in :ids",
          ids: subscription_item_ids,
        ).index_by(&:shift)

        rows.each do |sponsorship_id, subscription_item_id, sponsorship_subscribable_type, sponsorship_subscribable_id|
          if subscription_items[subscription_item_id].blank?
            log "Sponsorship #{sponsorship_id} has an invalid subscription_item_id" if verbose?
            next
          end

          (subscription_subscribable_type, subscription_subscribable_id) = subscription_items[subscription_item_id]
          if sponsorship_subscribable_type != subscription_subscribable_type ||
              sponsorship_subscribable_id != subscription_subscribable_id
            log "Sponsorship #{sponsorship_id} is out of sync: need to change subscribable_type from #{sponsorship_subscribable_type.inspect} to #{subscription_subscribable_type.inspect} and subscribable_id from #{sponsorship_subscribable_id.inspect} to #{subscription_subscribable_id.inspect}" if verbose?
            unless dry_run?
              sync_sponsorship_subscribable(
                sponsorship_id: sponsorship_id,
                subscribable_type: subscription_subscribable_type,
                subscribable_id: subscription_subscribable_id,
              )
            end
          else
            log "Sponsorship #{sponsorship_id} is already in sync" if verbose?
          end
        end
      end

      def sync_sponsorship_subscribable(sponsorship_id:, subscribable_type:, subscribable_id:)
        @count += 1

        ActiveRecord::Base.connected_to(role: :writing) do
          Sponsorship.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            sql = Sponsorship.github_sql.run(
              "UPDATE sponsorships SET subscribable_type=:subscribable_type, subscribable_id=:subscribable_id WHERE id=:sponsorship_id",
              sponsorship_id: sponsorship_id,
              subscribable_type: subscribable_type,
              subscribable_id: subscribable_id,
            )
            log "Updated #{sql.affected_rows} sponsorship(s) with id=#{sponsorship_id}" if verbose?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillSponsorshipSubscribable.new(options)
  transition.run
end
