# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180919031502_backfill_user_emails_normalized_domain.rb -v | tee -a /tmp/backfill_user_emails_normalized_domain.log
#
module GitHub
  module Transitions
    class BackfillUserEmailsNormalizedDomain < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        start_id = @other_args[:start_id] || readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM user_emails") }
        end_id = @other_args[:end_id] || readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM user_emails") }

        log "Starting with ID #{start_id} and finishing with ID #{end_id}"

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: start_id, finish: end_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT user_emails.id
          FROM user_emails
          WHERE user_emails.normalized_domain IS NULL
            AND user_emails.id BETWEEN :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        @total_processed = 0

        GitHub::SQL::Readonly.new(iterator.batches).each do |batch|
          batch.each do |email_ids|
            process(email_ids)
          end
        end

        log_phrase = dry_run? ? "Would have updated" : "Updated"
        log "#{log_phrase} #{@total_processed} email records" if verbose?
      end

      def process(email_ids)
        return if email_ids.empty?

        UserEmail.where(id: email_ids).each do |email|
          email.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do

            unless email.valid?
              log "Didn't update #{email.id} because record is not valid" if verbose?
              next
            end

            log_phrase = dry_run? ? "Would have updated" : "Updating"
            log "#{log_phrase} email #{email.id}" if verbose?

            unless dry_run?
              # set the normalized domain for the user email record.
              email.set_normalized_domain
              email.save!
            end

            @total_processed += 1
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "start_id=", "UserEmail ID to start processing at", as: Integer
    on "end_id=", "UserEmail ID to end processing at", as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillUserEmailsNormalizedDomain.new(options)
  transition.run
end
