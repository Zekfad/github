# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180702131808_convert_users_two_factor_required_to_configurable.rb -v | tee -a /tmp/convert_users_two_factor_required_to_configurable.log
#
module GitHub
  module Transitions
    class ConvertUsersTwoFactorRequiredToConfigurable < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_user_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM users") }
        max_user_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM users") }
        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_user_id, finish: max_user_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT users.id
          FROM users
          WHERE users.type = "Organization"
            AND users.id between :start AND :last
            AND users.require_2fa = 1
        SQL
      end

      # Returns nothing.
      def perform
        total_rows = 0

        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
          total_rows += rows.size
        end

        log "Processed #{total_rows} total rows" if verbose?
      end

      def process(rows)
        return if rows.empty?

        log "require_2fa true for Organization with ids: #{rows}" if verbose?

        return if dry_run?

        Configuration::Entry.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          # Each row is an Array with one element, the org ID
          org_ids = rows.flatten!
          # Don't process orgs that have existing configuration entries
          exclude_org_ids = readonly do
            Configuration::Entry.github_sql.results(<<-SQL, { org_ids: org_ids })
              SELECT entries.target_id
              FROM configuration_entries entries
              WHERE entries.target_id IN :org_ids
                AND entries.target_type = "User"
                AND entries.name = "#{Configurable::TwoFactorRequired::KEY}"
            SQL
          end
          exclude_org_ids.flatten!
          org_ids = org_ids - exclude_org_ids

          now = GitHub::SQL::NOW
          config_rows = GitHub::SQL::ROWS(
            org_ids.map do |organization_id|
              [
                "User", # target_type
                organization_id, # target_id
                organization_id, # updater_id
                Configurable::TwoFactorRequired::KEY, # name
                "true", # value
                now, # created_at
                now, # updated_at
              ]
            end,
          )

          Configuration::Entry.github_sql.run(<<-SQL, { rows: config_rows })
            INSERT INTO configuration_entries
              (target_type, target_id, updater_id, name, value, created_at, updated_at)
            VALUES
              :rows
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ConvertUsersTwoFactorRequiredToConfigurable.new(options)
  transition.run
end
