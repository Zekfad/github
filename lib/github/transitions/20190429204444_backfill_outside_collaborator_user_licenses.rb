# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190429204444_backfill_outside_collaborator_user_licenses.rb -v | tee -a /tmp/backfill_outside_collaborator_user_licenses.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190429204444_backfill_outside_collaborator_user_licenses.rb -v -w | tee -a /tmp/backfill_outside_collaborator_user_licenses.log
#
module GitHub
  module Transitions
    class BackfillOutsideCollaboratorUserLicenses < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      BATCH_SIZE = 100
      BUSINESS_ORGANIZATION_MEMBERSHIP_ORGANIZATION_ID_INDEX = 1
      BUSINESS_ORGANIZATION_MEMBERSHIP_BUSINESS_ID_INDEX = 2

      attr_reader :iterator

      def after_initialize
        min_id = readonly { Business::OrganizationMembership.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM business_organization_memberships") }
        max_id = readonly { Business::OrganizationMembership.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM business_organization_memberships") }

        @iterator = Business::OrganizationMembership.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE).tap do |i|
          i.add <<~SQL
            SELECT  id,
                    organization_id,
                    business_id
            FROM    business_organization_memberships
            WHERE   id BETWEEN :start AND :last
          SQL
        end
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |business_organization_membership_rows|
          log "ensuring licenses exist for business organization membership IDs: #{business_organization_membership_rows.map(&:first).join(", ")}" if verbose?

          organization_business_mappings = organization_business_mappings(business_organization_membership_rows)
          repository_business_mappings = repository_business_mappings(organization_business_mappings)
          user_business_mappings = user_business_mappings(repository_business_mappings)

          upsert_user_licenses(user_business_mappings)
        end
      end

      private

      def organization_business_mappings(rows)
        rows.each_with_object(Hash.new) do |row, mapping|
          mapping[row[BUSINESS_ORGANIZATION_MEMBERSHIP_ORGANIZATION_ID_INDEX]] = row[BUSINESS_ORGANIZATION_MEMBERSHIP_BUSINESS_ID_INDEX]
        end
      end

      def repository_business_mappings(organization_business_mappings)
        return Array.new if organization_business_mappings.empty?

        repository_rows = readonly do
          Repository.github_sql.hash_results(<<~SQL, organization_ids: organization_business_mappings.keys)
            SELECT  id AS repository_id,
                    owner_id AS organization_id
            FROM    repositories
            WHERE   owner_id IN :organization_ids
              AND   parent_id IS NULL
              AND   public = false
              AND   active = true
          SQL
        end

        repository_rows.each_with_object(Hash.new) do |repository_row, mapping|
          mapping[repository_row["repository_id"]] = organization_business_mappings[repository_row["organization_id"]]
        end
      end

      def user_business_mappings(repository_business_mappings)
        return Array.new if repository_business_mappings.empty?

        ability_rows = readonly do
          Ability.github_sql.hash_results(<<~SQL, repository_ids: repository_business_mappings.keys)
            SELECT  actor_id AS user_id,
                    subject_id AS repository_id
            FROM    abilities
            WHERE actor_type = 'User'
              AND actor_id IS NOT NULL
              AND subject_type = 'Repository'
              AND priority = 1
              AND action >= 0
              AND subject_id IN :repository_ids
          SQL
        end

        ability_rows.each_with_object(Hash.new) do |ability_row, mapping|
          mapping[ability_row["user_id"]] = repository_business_mappings[ability_row["repository_id"]]
        end
      end

      def upsert_user_licenses(user_business_mappings)
        return if user_business_mappings.empty?

        UserLicense.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          insert_rows = user_business_mappings.to_a.map do |(user_id, business_id)|
            [
              business_id,
              user_id,
              1, # enterprise
              Time.current,
              Time.current,
          ]
          end

          UserLicense.github_sql.run(<<~SQL, rows: GitHub::SQL::ROWS(insert_rows)) unless dry_run?
            INSERT INTO user_licenses
              (business_id, user_id, license_type, created_at, updated_at)
            VALUES :rows
            ON DUPLICATE KEY UPDATE updated_at = CURRENT_TIMESTAMP
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillOutsideCollaboratorUserLicenses.new(options)
  transition.run
end
