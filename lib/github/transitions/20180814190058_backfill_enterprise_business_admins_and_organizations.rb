# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "github/enterprise"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180814190058_backfill_enterprise_business_admins_and_organizations.rb -v | tee -a /tmp/backfill_enterprise_business_admins_and_organizations.log
#
module GitHub
  module Transitions
    class BackfillEnterpriseBusinessAdminsAndOrganizations < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_organization_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM users WHERE type = 'Organization'") }
        max_organization_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM users WHERE type = 'Organization'") }
        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(
          start: min_organization_id,
          finish: max_organization_id,
          batch_size: BATCH_SIZE,
        )
        @iterator.add <<-SQL
          SELECT users.id
          FROM users
          WHERE users.type = "Organization"
            AND users.id NOT IN (SELECT organization_id FROM business_organization_memberships)
            AND users.id between :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        return unless GitHub.single_business_environment?

        # Add the global business
        GitHub::Enterprise.ensure_business!

        # Make sure it worked!
        business = GitHub.global_business
        return if business.nil?

        # Add existing site admins as business admins
        User.where(type: "User", gh_role: "staff").find_each do |admin|
          business.add_owner(admin, actor: nil)
        end

        # Add orgs
        total_organizations = 0

        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
          total_organizations += rows.size
        end

        log "Processed #{total_organizations} total rows" if verbose?
      end

      def process(rows)
        return if rows.empty?

        log "adding Business::OrganizationMemberships for organization ids: #{rows}" if verbose?

        business_id = GitHub.global_business.id
        Business::OrganizationMembership.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          now = GitHub::SQL::NOW
          config_rows = GitHub::SQL::ROWS(
            rows.map do |iterator_results|
              # iterator_results is an Array with one element, the organization_id
              organization_id = iterator_results.first
              [
                business_id, # GitHub.global_business.id
                organization_id, # organization_id
                now, # created_at
                now, # updated_at
            ]
            end,
          )

          Business::OrganizationMembership.github_sql.run(<<-SQL, { rows: config_rows })
            INSERT INTO business_organization_memberships
              (business_id, organization_id, created_at, updated_at)
            VALUES
              :rows
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillEnterpriseBusinessAdminsAndOrganizations.new(options)
  transition.run
end
