# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191115001043_backfill_repository_advisory_owners.rb -v | tee -a /tmp/backfill_repository_advisory_owners.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191115001043_backfill_repository_advisory_owners.rb -v -w | tee -a /tmp/backfill_repository_advisory_owners.log
#
module GitHub
  module Transitions
    class BackfillRepositoryAdvisoryOwners < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM repository_advisories") }
        max_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM repository_advisories") }

        # This is a relatively small batch. There are about 3000 total repository advisories as of November 2019.
        @iterator = ApplicationRecord::Collab.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, repository_id FROM repository_advisories
          WHERE id BETWEEN :start AND :last
          AND owner_id IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          owner_id = nil
          Repository.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            owner_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value(<<-SQL, repo_id: item[1]) }
              SELECT owner_id
              FROM repositories
              WHERE id = :repo_id AND active = 1
            SQL
          end
          RepositoryAdvisory.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            if owner_id.nil?
              log "No owner_id retrieved for deleted repository id #{item[1]}" if verbose?
            else
              log "Setting repository advisory #{item[0]} in repository #{item[1]} owner_id to #{owner_id}" if verbose?
              write_owner(item[0], owner_id) unless dry_run?
            end
          end
        end
      end

      def write_owner(repository_advisory_id, owner_id)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Collab.github_sql.run <<-SQL, repository_advisory_id: repository_advisory_id, owner_id: owner_id
            UPDATE repository_advisories SET owner_id = :owner_id WHERE id = :repository_advisory_id
          SQL
          sql.affected_rows
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillRepositoryAdvisoryOwners.new(options)
  transition.run
end
