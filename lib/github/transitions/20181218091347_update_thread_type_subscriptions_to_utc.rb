# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20181218091347_update_thread_type_subscriptions_to_utc.rb -v | tee -a /tmp/update_thread_type_subscriptions_to_utc.log
#
module GitHub
  module Transitions
    class UpdateThreadTypeSubscriptionsToUtc < Transition
      # This transition updates the notification_thread_type_subscriptions.created_at column to change the timezone of
      # the stored data from local time (PST) to UTC. The data that was stored there has the correct date, if
      # interpreted as local but we wish to switch it to UTC.
      #
      # The `is_utc` column exists to tell us whether a row needs transitioned or not - new data is already being written
      # in UTC, so it's only older data we need to update.
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly {
          Newsies::ThreadTypeSubscription.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM notification_thread_type_subscriptions WHERE is_utc IS FALSE")
        }
        max_id = readonly {
          Newsies::ThreadTypeSubscription.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM notification_thread_type_subscriptions WHERE is_utc IS FALSE")
        }

        @iterator = Newsies::ThreadTypeSubscription.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, DATE_FORMAT(created_at, "%Y-%m-%d %T") as created_at, is_utc
          FROM notification_thread_type_subscriptions
          WHERE id BETWEEN :start AND :last
          AND is_utc IS FALSE
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      # Returns nothing.
      def process(rows)
        rows.each do |(id, created_at, is_utc)|
          # we should already have filtered this out in the batched query, but just in case
          if is_utc == 1
            log "Skipping #{id} as already utc" if verbose?
            next
          end

          Newsies::ThreadTypeSubscription.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            statement = <<-SQL
              UPDATE notification_thread_type_subscriptions
              SET created_at = :created_at_utc, is_utc = :is_utc
              WHERE id = :id
            SQL

            bindings = {
              id: id,
              created_at_utc: self.class.convert_local_date_string_to_utc_string(created_at),
              is_utc: true,
            }

            log "Updating #{id} from '#{created_at}' to '#{bindings[:created_at_utc]}'" if verbose?
            Newsies::ThreadTypeSubscription.github_sql.run(statement, bindings) unless dry_run?
          end
        end
      end

      # We retrieve, and write, all dates as strings to be very sure that activerecord's timezone
      # handling doesn't change the value for us
      def self.convert_local_date_string_to_utc_string(date_string)
        parts = date_string.match(/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}).?(\d{6})?/).captures
        local_time = Time.local(*parts)
        local_time.utc.strftime("%Y-%m-%d %H:%M:%S")
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UpdateThreadTypeSubscriptionsToUtc.new(options)
  transition.run
end
