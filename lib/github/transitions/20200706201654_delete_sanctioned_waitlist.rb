# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200706201654_delete_sanctioned_waitlist.rb --verbose | tee -a /tmp/delete_sanctioned_waitlist.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200706201654_delete_sanctioned_waitlist.rb --verbose -w | tee -a /tmp/delete_sanctioned_waitlist.log
#
module GitHub
  module Transitions
    class DeleteSanctionedWaitlist < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle_with_retry` block, which should be called on the most specific
      #      `ApplicationRecord::*` class/subclass or object. This will make sure we don't
      #      overwhelm master and cause replication lag.
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      # attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_memberships") }
        max_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_memberships") }
        @records_updated = 0

        @iterator = ApplicationRecord::Domain::Sponsors.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
        SELECT sponsors_memberships.id, sponsors_memberships.billing_country
        FROM sponsors_memberships
        WHERE
          (sponsors_memberships.id BETWEEN :start AND :last)
          AND
          (billing_country IN ('CU', 'IR', 'SY', 'KP'))
        SQL
      end

      # Returns nothing.
      def perform
        readonly_batches = GitHub::SQL::Readonly.new(@iterator.batches)
        readonly_batches.each do |batch|
          process(batch)
        end

        log "Deleted #{@records_updated} total records" if verbose?
      end

      def process(rows)
        if dry_run?
          rows.each do |element|
            log "id: #{element.first}, billing_country: #{element.second}"
          end
        else
          rows.each do |element|
            SponsorsMembership.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              SponsorsMembership.find(element.first).destroy!
            end
          end
          log "Deleted batch of #{rows.size} elements" if verbose?
        end
        @records_updated += 1
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DeleteSanctionedWaitlist.new(options)
  transition.run
end
