# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191102032008_set_applicable_to_for_sponsors_criteria.rb -v | tee -a /tmp/set_applicable_to_for_sponsors_criteria.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191102032008_set_applicable_to_for_sponsors_criteria.rb -v -w | tee -a /tmp/set_applicable_to_for_sponsors_criteria.log
#
module GitHub
  module Transitions
    class SetApplicableToForSponsorsCriteria < Transition

      APPLICABLE_TO_VALUES = {
        "suspension_status"      => :user,
        "blocked_threshold"      => :user,
        "member_reputable_org"   => :user,
        "public_contributions"   => :user,
        "follower_threshold"     => :user,
      }

      # Returns nothing.
      def perform
        criteria = ::SponsorsCriterion.where(slug: APPLICABLE_TO_VALUES.keys)

        criteria.each do |criterion|
          value = APPLICABLE_TO_VALUES[criterion.slug]

          if dry_run?
            log "Would have set #{criterion.slug} to #{value}"
          else
            criterion.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              criterion.applicable_to = value
              criterion.save!
            end

            log "Set #{criterion.slug} to #{value}"
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::SetApplicableToForSponsorsCriteria.new(options)
  transition.run
end
