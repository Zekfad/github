# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191203132159_backfill_early_access_memberships.rb -v | tee -a /tmp/backfill_early_access_memberships.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191203132159_backfill_early_access_memberships.rb -v -w | tee -a /tmp/backfill_early_access_memberships.log
#
module GitHub
  module Transitions
    class BackfillEarlyAccessMemberships < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM early_access_memberships") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM early_access_memberships") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, user_id FROM early_access_memberships
          WHERE id BETWEEN :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        EarlyAccessMembership.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Copying user_id to member_id with #{rows.map { |item| item[0] }}" if verbose?
          run_batch_update rows unless dry_run?
        end
      end

      def run_batch_update(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::Users.github_sql.new "UPDATE early_access_memberships SET member_id = CASE"

          rows.each do |item|
            id = item[0]
            user_id = item[1]
            sql.add <<-SQL, id: id, user_id: user_id
              WHEN id = :id THEN :user_id
            SQL
          end

          sql.add "END WHERE id IN :ids", ids: rows.map { |item| item[0] }
          sql.run
          sql.affected_rows
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillEarlyAccessMemberships.new(options)
  transition.run
end
