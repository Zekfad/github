# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20170612214922_cancel_testing_subscription_items.rb -v | tee -a /tmp/cancel_testing_subscription_items.log
#
module GitHub
  module Transitions
    class CancelTestingSubscriptionItems < Transition
      # Returns nothing.
      def perform
        active_subscription_items = []

        readonly do
          logins =  FEATURE_FLAG_LOGINS | EMPLOYEE_LOGINS

          active_subscription_items = logins.map do |login|
            user = User.find_by_login(login)
            user&.plan_subscription&.subscription_items&.active
          end.flatten.compact

          logins.each do |login|
            user = User.find_by_login(login)
            recurring_marketplace_transactions(user).each do |txn|
              puts "[CHARGED] #{login} #{txn.amount} on #{txn.created_at.to_date}"
            end
          end
        end

        active_subscription_items.select(&:paid?).each do |item|
          item.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            puts "Cancelling #{item.account.login}'s subscription to #{item.subscribable.name} #{item.listing.name}"
            item.update_attribute(:quantity, 0) unless dry_run
          end
        end
      end

      # Returns a list of non-refunded, recurring, marketplace transactions
      def recurring_marketplace_transactions(user)
        return [] unless user

        recurring_txns = user.billing_transactions.where("transaction_type = 'recurring-charge'")
        recurring_txns.select do |txn|
          txn.line_items.where("marketplace_listing_plan_id IS NOT NULL and amount_in_cents > 0").any?
        end.reject(&:was_refunded?)
      end

      # FlipperFeature.find_by_name("marketplace").actors.map(&:login)
      FEATURE_FLAG_LOGINS = %w(reretestorganization technoweenie probablycorey roncohen mcolyer benvinegar svenfuchs zeke djones kdaigle roidrage nickh shorrockin joshk brianr jes5199 danishkhan timhaines dcramer korzonek achiu kareem sturnquest rkh izuzak arifb jammur brntbeer southgate rstocker99 gjtorikian chrishunt fotinakis z00b cheshire137 homeyer pnavarrc corywilkerson jimdotrose fabianperez marydavis ehfeng othercasey broccolini shiftkey caxaria mattrobenolt anarosas lyrixx mikrobi burtonjc jaimefjorge sophshep groundwater tarebyte mrfyda mockra coryvirok zhangchiqing sarahs ctrlaltjustine makwarth antn tucksaun connors pmarsceill rtfpessoa jessewgibbs electronspin joewadcan cmwinters DavidTolley jglovier aakritigupta billbowles jmilas JustinC474 lukehefson aden seemakamath malandrina ezarowny stevepeak 204NoContent alysonla emmabot Kay-Zee jendewalt zacharysarah IlyaFinkelshteyn patrickmckenna sbarnekow chobberoni jleaver emilyistoofunky grodowski leoaxiomzen macqueen FeodorFitsner bernars Mathieuu haleygirl pifafu josephwadcan pamboat mbhartley rishimkumar mynameisbill azenMatt ChristopheDujarric arifb2 bluemazzoo aellispierce vinmat nickh68 ohitsmekatie timothywellsjr kendramoroz alrightjones chrisjahn jjhonson akinaito sherrinetan francisfuzz vinmat007 BAMason alecmorgana notjmilas renee-travisci sarahvessels evilluke Alisuehobbs user-bbbb perribaker samltim testtimtestmongral lacrossegirl1989 tjrwells vinmat585 vinmat587 alter-aellispierce supermockra rstocker99mp mellamopepe1989 FeodorFitsner-OSS FeodorFitsner-PRO retestaccount travis-test-user1 travis-test-user2 travis-test-user3 travis-test-user4 travis-test-user5 zenhubmarketplacet nickh69 vinmat600 vinmat601 vinmat602 vinmat603 vinmat604 reretestaccount otherothercasey originalcasey otheroriginalcasey yetanothercasey)

      # Organization.find_by_name("github").teams.find_by_name("Employees").members.map(&:login)
      EMPLOYEE_LOGINS = %w(petros bkeepers brianmario defunkt aitchabee Haacked hubot jakeboxer jnewland jnunemaker jonmagic jonrohan josh joshaber kevinsawicki leereilly maddox mtodd nathansobo nickh peff probablycorey vmg tclem technoweenie dewski jbarnette leehuffman sbryant TwP matthewmccullough skalnik dreww shawndavenport jasonlong erebor tonyjaramillo dgraham spicycode juliamae keavy Caged lukehefson jch pakwit jgreet shayfrendt johndbritton steveward joshvera mattyoho spraints jeejkang aden alysonla mdo rsese sroberts mutle craigreyes fooforge ElizabethN hoolio gjtorikian brntbeer nakajima balevine niik jasonrudolph dsorkin benbalter grantr ciaramcguire lynnwallenstein jordanmccullough kdaigle JohnCreek piki zerowidth michaeltwofish jdennes jesseplusplus jglovier technicalpickles samlambert chrishunt southgate simonsj charliesome gregose fabianperez rubiojr muan shiftkey izuzak keithduncan jssjr azizshamim kelseyschimm tjl2 jdmaturen emilyistoofunky rathjenc talniv joewilliams bigred ptoomey3 Coecoe haleygirl eanderson rubyist robrix mhagger lisalou bernars mcolyer MikeMcQuaid pamboat johnyerhot antonio jhosman ammeep dbussink jameswhite lildude sophiec dice joewadcan mislav meatcoder bhuga jammur amateurhuman jessephus BrittRinella bfire mastahyeti pastjohn neechbomb zoeweidz mbhartley achiu buckelij megches anna bdemott fernandez dproano gpadak rmulcahy b4mboo jeffmjones jhunnewell leefaus sanicki donal mgriffin oreoshake bvopinek vjayn zcbenz hollyturay merrittqa maxbrunsfeld jatoben shana mfilosa celsmore willstef ross mattcantstop mhp tfgrimes ShikhaThakkar crichID JesperHyrm loranallensmith epemmerl patrickmckenna TessaGit brennaheaps kjpomerleau evelDK asseh franniez markostar nathos stephbwills emcgrew theojulienne dennie Neeners mynameisbill cjs githogan toomanyaaronbrowns shannonhines annieherlitz nmsanchez cmrberry helaili jemoll jbjonesjr anaisFTW jgkite ikeike443 kaihj rocio kylemacey agelender carolynshin bitoiu katelynbryant bescalante jonahberquist coryvj MaryWheeler kluczenk ksekulio dq-turtle mendyslaton AlaineN keeran missinformed woodmanzee Jentam BonnieC theseawitch justinmccormick stevecat atiff brotherben suniljoseph Fermentedbeats sleepysam daniloc Richardstraat Kingerr NataliaHarakka pholleran toyae bas mellybess jleaver jmarlena arthurnn JennLachman pmarsceill patriciadavico ronnie k8d brainstarr marisa Shreekar1 Amy-Worth tiabobia atm1 yourfrienderin msbise abannick jayswan tmaher sjarolimek webdog kpaulisse jmilas tierninho mwiesen mlinksva rafer frconil sandhub danayel jasonmassey WillAbides harsh AnthonyS evanrodriguez gnawhleinad BenEddy mfrosen ihavenotea rewinfrey brittpwalker zawaideh ohitsmekatie ticachica NadaAldahleh simurai bwestover sophshep xhermann imgonnarelph tomkrouper djdefi francisfuzz DarcyCK barbkoz chrisjahn jamesmartin pnsk shortsparty finkDC vinmat diwakargupta11 craigbarrere jordemort shanbar ClayNelson kuychaco shlomi-noach jes5199 iolsen broccolini Mlohoury nickcannariato parkr ggunson rkarthikeyan5 staceb samuelhunt omgitsads nixpad donokuda lee-dohm electroniko LizzyKemp veesa georgicodes snh hax66 CarmenPalMar davidcelis nplasterer tarebyte bswinnerton paladique junfuji janester stoe allthedoll nsqe taconeko vmramirez LizzHale akinaito megbird papimenon remywestin bryanaknight wingr alexandrakelly swinton aaronbbrown jwiebalk bbasata jamesjlee11 MikeNwin tma carlosmn cheshire137 xducati620 BinaryMuse Brits02 kzadeh DaveEide alrightjones nelson-steven sherrinetan bluemazzoo sambong tstone92 dmleong AlexAmsterdam shreyasjoshis leithiaRVA sarabartell sofiehoeg dolcetta rhettg miguelff aymannadeem KT2016 AlexPurkiss zeke literarytea affrae sbarnekow brianamarie mistydemeo katmeister kamilahgriffin katfoz ZeroCool kneemer osowskit mclark reversaw kdraps hktouw tenderlove harrisonleavitt hectorsector tcbyrd pifafu doublemarket KristinaMarie Teaford lecoursen kavgan craigmorrall wordsoup nadiajoyce dimmykumont pravipati mosborne24 acmccall jamfish reem-a arthurschreiber fabien965 jonico MarcoHat chobberoni othercasey EprisBlank evilshawn kivikakk rubyjazzy AllisonZhornist Lieke22 jayleisure cvfernandez mfrisky grokys Tracol pbdunn timothywellsjr bevns stephaniemaria kemjo mslilylove as-cii hugginz chadjlucas rushtonmd NikiLustig mikrobi panand71 beardofedu kytrinyx terrorobe lowply Nickismyname hollenberry ACyphus mcadoo JodiDelman melscoop freakingid NedMorris lukeart kathodos shawnajean bryancross canuckjacq hubberjay heyhayhay jules-p doctorwholock ktfeucht nayafia damieng emrichard ryanpamela skottler corywilkerson swannysec ttaylorr joenash migarjo wi11ialvl talktopri ann-mary JennieOhyoung kimestoesta EddPatterson gracepark jimmyokami newmerator sukoonmusic c-sefkin willf johnjoubert chihiroto padma007 miket waldnzwrld that-pat sarahs dara-leigh zacharysarah chronicleofnerdia sophaskins el33 neverett liliawu AndrewCamp ungb piqrgb nigelabbott groundwater neev7 ThatStoney dekigai antn jessbreckenridge LindseyB smashwilson Kzweng ebaldoz tommyqh courtneyscharff thejandroman hohsiangwu detectiveddubs Raia eileencodes iAmWillShepherd kxactly ikewalker alebourne StanleyGoldman jcansdale jungjeon issc29 spinecone sgorajia sheetz phantomdata CJabola taz mijuhan michelemt jk0 Pseudopam colossus9 gwincr11 PriscillaRaymundo ashleymsmith aellispierce AmySutton mozzadrella ravigadhia angela-crist hbforsyth gamefiend meentastic parthd24 jroll brookienoodle shraw m0rgane solvaholic mikesea kmchef cmwinters perribaker malandrina mockra dpmex4527 mandyzc shanecarr1 rufo jeffjewett nautalice lerebear synthead CJMillah jasonuhl jonabc ebchill chesterbr Raulv81 Helen-Larcos reedjohnston jeffrafter sridharavinash jdpace micahcroff MeccaB tpigott camicano anna-talley KelliKelliKelli nickvanw alimarcozzi brandonrosage ladyray7310 rmosolgo jessicacano SirCN Katsuyumyum darrowby385 connyvanessa philipturnbull Phanatic ThienTr ThomasShaped fatcomma lilliSeBe tmaltiel austinbuck1 scenoka lilliegrace gladwearefriends GracieCole tboneATL jkleinsc Cameronclarkfilm baruckus seejohnrun vanessayuenn skullface keithamus jasoncwarner agnespa)
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CancelTestingSubscriptionItems.new(options)
  transition.run
end
