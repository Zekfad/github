# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190919193454_update_open_issues_to_closed.rb -v | tee -a /tmp/update_open_issues_to_closed.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190919193454_update_open_issues_to_closed.rb -v -w | tee -a /tmp/update_open_issues_to_closed.log
#
module GitHub
  module Transitions
    class UpdateOpenIssuesToClosed < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM issues") }
        max_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM issues") }

        @iterator = ApplicationRecord::Domain::Repositories.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id
          FROM issues
          WHERE id BETWEEN :start AND :last
          AND id IN (
            SELECT issue_id
            FROM issue_events AS events
            WHERE issue_id BETWEEN :start AND :last
            GROUP BY issue_id
            HAVING sum(case event when 'closed' then 1 else 0 end) > sum(case event when 'reopened' then 1 else 0 end)
          )
          AND state = 'open'
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        Issue.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Closing open issues with closed event #{rows.map { |item| item[0] }}" if verbose?
          run_batch_to_close_open_issues rows unless dry_run?
        end
      end

      def run_batch_to_close_open_issues(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::Repositories.github_sql.run <<-SQL, ids: rows.flatten
            UPDATE issues
            SET state = 'closed'
            WHERE id in :ids
          SQL
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UpdateOpenIssuesToClosed.new(options)
  transition.run
end
