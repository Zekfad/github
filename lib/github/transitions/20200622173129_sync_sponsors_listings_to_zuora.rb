# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200622173129_sync_sponsors_listings_to_zuora.rb --verbose | tee -a /tmp/sync_sponsors_listings_to_zuora.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200622173129_sync_sponsors_listings_to_zuora.rb --verbose -w | tee -a /tmp/sync_sponsors_listings_to_zuora.log
#
module GitHub
  module Transitions
    class SyncSponsorsListingsToZuora < Transition

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_listings") }
        max_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_listings") }

        @iterator = ApplicationRecord::Collab.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          -- id must go first for batched between to work properly
          SELECT id FROM sponsors_listings
          WHERE id BETWEEN :start AND :last
          AND state = #{SponsorsListing.state_value(:approved)}
        SQL

        GitHub.zuorest_client.timeout = 60
        GitHub.zuorest_client.open_timeout = 60
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |id, _|
          SponsorsListing.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            if listing = SponsorsListing.find_by_id(id)
              log "(Re)syncing sponsors listing ID #{id} (#{listing.slug}) to zuora" if verbose?
              begin
                listing.sync_to_zuora unless dry_run?
              rescue Faraday::ConnectionFailed, Faraday::TimeoutError, Zuorest::HttpError => e
                message = "Syncing listing ID #{id} (#{listing.slug}) raised error: #{e}"
                message = message + " - #{e.data}" if e.respond_to?(:data)
                log message
              end
            else
              log "Error loading sponsors listing with id=#{id}"
            end
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::SyncSponsorsListingsToZuora.new(options)
  transition.run
end
