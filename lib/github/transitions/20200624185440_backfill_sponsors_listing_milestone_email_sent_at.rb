# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200624185440_backfill_sponsors_listing_milestone_email_sent_at.rb --verbose | tee -a /tmp/backfill_sponsors_listing_milestone_email_sent_at.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200624185440_backfill_sponsors_listing_milestone_email_sent_at.rb --verbose -w | tee -a /tmp/backfill_sponsors_listing_milestone_email_sent_at.log
#
module GitHub
  module Transitions
    class BackfillSponsorsListingMilestoneEmailSentAt < Transition
      TOTAL_SPONSORS_COUNT_THRESHOLD = 10
      MONTHLY_SPONSORSHIP_AMOUNT_THRESHOLD = 25

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_listings") }
        max_id = readonly { ApplicationRecord::Domain::Sponsors.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_listings") }

        @iterator = ApplicationRecord::Domain::Sponsors.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, sponsorable_id FROM sponsors_listings
          WHERE id BETWEEN :start AND :last
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        backfillable_sponsors_listing_ids = []

        rows.each do |item|
          SponsorsListing.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            sponsors_listing_id, sponsorable_id = item

            sponsors_count = ApplicationRecord::Domain::Sponsors.github_sql.value(
              "SELECT COUNT(*)
              FROM sponsorships
              WHERE sponsorable_id=:sponsorable_id
              AND sponsorships.active = 1",
              sponsorable_id: sponsorable_id,
            )

            if sponsors_count >= TOTAL_SPONSORS_COUNT_THRESHOLD
              log "SponsorsListing #{sponsors_listing_id} met total sponsors count threshold. Will backfill milestone_email_sent_at." if verbose?
              backfillable_sponsors_listing_ids << sponsors_listing_id
              next
            end

            monthly_sponsorship_amount_in_dollars = ApplicationRecord::Domain::Sponsors.github_sql.value(
              "SELECT COALESCE(SUM(sponsors_tiers.monthly_price_in_cents)/100, 0)
              FROM sponsorships
              JOIN sponsors_tiers ON sponsorships.subscribable_id = sponsors_tiers.id
              WHERE sponsorships.sponsorable_id = :sponsorable_id
              AND sponsorships.active = 1",
              sponsorable_id: sponsorable_id,
            )

            if monthly_sponsorship_amount_in_dollars >= MONTHLY_SPONSORSHIP_AMOUNT_THRESHOLD
              log "SponsorsListing #{sponsors_listing_id} met monthly sponsorship amount threshold. Will backfill milestone_email_sent_at." if verbose?
              backfillable_sponsors_listing_ids << sponsors_listing_id
              next
            end

            log "SponsorsListing #{sponsors_listing_id} did not meet threshold. Will do nothing." if verbose?
          end
        end

        log "Backfilling milestone_email_sent_at for SponsorsListings with these IDs: #{backfillable_sponsors_listing_ids}" if verbose
        backfill_milestone_email_sent_at(backfillable_sponsors_listing_ids) unless dry_run?
      end

      def backfill_milestone_email_sent_at(sponsors_listing_ids)
        return if sponsors_listing_ids.empty?

        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::Sponsors.github_sql.run(
            "UPDATE sponsors_listings SET milestone_email_sent_at = :milestone_email_sent_at WHERE id in :sponsors_listing_ids",
            milestone_email_sent_at: Time.current,
            sponsors_listing_ids: sponsors_listing_ids,
          )
          log "Set milestone_email_sent_at for #{sql.affected_rows} SponsorsListings" if verbose?
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillSponsorsListingMilestoneEmailSentAt.new(options)
  transition.run
end
