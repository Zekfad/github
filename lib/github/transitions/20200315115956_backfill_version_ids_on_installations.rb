# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200315115956_backfill_version_ids_on_installations.rb -v | tee -a /tmp/backfill_version_ids_on_installations.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200315115956_backfill_version_ids_on_installations.rb -v -w | tee -a /tmp/backfill_version_ids_on_installations.log
#
module GitHub
  module Transitions
    class BackfillVersionIdsOnInstallations < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        # The most common, and preferred, approach is to define your iterator
        # here using `BatchedBetween`. Prefer using a min_id with BatchedBetween
        # queries. Not doing so can lead to _very_ slow transition tests (https://github.com/github/github/pull/92565)
        min_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM integration_installations") }
        max_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM integration_installations") }

        @iterator = ApplicationRecord::Domain::Integrations.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          -- id must go first for batched between to work properly
          SELECT integration_installations.id,
                 integration_versions.id
          FROM integration_installations
          JOIN integration_versions ON
            integration_versions.number = integration_installations.integration_version_number
            AND integration_versions.integration_id = integration_installations.integration_id
          WHERE integration_installations.id BETWEEN :start AND :last
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        IntegrationInstallation.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "doing something with #{rows.map { |item| item[0] }}" if verbose?
          run_batch_update rows unless dry_run?
        end
      end

      def run_batch_update(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::Integrations.github_sql.new "UPDATE integration_installations SET integration_version_id = CASE"

          rows.each do |item|
            id = item[0]
            integration_version_id = item[1]
            sql.add <<-SQL, id: id, integration_version_id: integration_version_id
              WHEN id = :id THEN :integration_version_id
            SQL
          end

          sql.add "END WHERE id IN :ids", ids: rows.map { |item| item[0] }
          sql.run
          sql.affected_rows
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillVersionIdsOnInstallations.new(options)
  transition.run
end
