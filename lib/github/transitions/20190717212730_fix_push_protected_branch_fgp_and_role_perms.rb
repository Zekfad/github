# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190717212730_fix_push_protected_branch_fgp_and_role_perms.rb -v | tee -a /tmp/fix_push_protected_branch_fgp_and_role_perms.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190717212730_fix_push_protected_branch_fgp_and_role_perms.rb -v -w | tee -a /tmp/fix_push_protected_branch_fgp_and_role_perms.log
#
module GitHub
  module Transitions
    class FixPushProtectedBranchFgpAndRolePerms < Transition

      ADD_MAINTAIN_FGPS = %w(
        push_protected_branch
      )

      REMOVE_MAINTAIN_FGPS = %w(
        force_push_protected_branch
      )

      # Returns nothing.
      def perform
        unless dry_run?
          write_fine_grained_permissions(ADD_MAINTAIN_FGPS)
          add_role_permissions(ADD_MAINTAIN_FGPS, role: "maintain")
          clear_permissions(REMOVE_MAINTAIN_FGPS)
        end
      end

      def write_fine_grained_permissions(actions)
        actions.each do |action|
          FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            FineGrainedPermission.create(action: action)
          end
        end
      end

      def add_role_permissions(actions, role:)
        maintain_role = Role.where(name: role).first
        return unless maintain_role
        actions.each do |action|
          permission = FineGrainedPermission.where(action: action).first
          RolePermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            RolePermission.create(
              role_id: maintain_role.id,
              fine_grained_permission_id: permission.id,
              action: permission.action,
            )
          end
        end
      end

      def clear_permissions(actions)
        FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          actions.each do |action|
            log "deleting the #{action} fgp and associated role permissions"
            FineGrainedPermission.where(action: action).destroy_all
          end
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::FixPushProtectedBranchFgpAndRolePerms.new(options)
  transition.run
end
