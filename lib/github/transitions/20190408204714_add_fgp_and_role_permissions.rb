# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190408204714_add_fgp_and_role_permissions.rb -v | tee -a /tmp/add_fgp_and_role_permissions.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190408204714_add_fgp_and_role_permissions.rb -v -w | tee -a /tmp/add_fgp_and_role_permissions.log
#
module GitHub
  module Transitions
    class AddFgpAndRolePermissions < Transition

      TRIAGE_FGPS = %w(
        add_label
        remove_label
        create_issue
        close_issue
        reopen_issue
        close_pull_request
        add_assignee
        remove_assignee
      )

      MAINTAIN_FGPS = %w(
        edit_repo_metadata
        manage_topics
        setup_issue_template
      )

      # Returns nothing.
      def perform
        unless dry_run?
          write_fine_grained_permissions
          associate_fgps_with_roles
        end
      end

      def write_fine_grained_permissions
        add_triage_fgps
        add_maintain_fgps
      end

      def associate_fgps_with_roles
        add_role_permissions_for_triage
        add_role_permissions_for_maintain
      end

      def add_triage_fgps
        TRIAGE_FGPS.each do |fgp|
          FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            FineGrainedPermission.create(action: fgp)
          end
        end
      end

      def add_maintain_fgps
        MAINTAIN_FGPS.each do |fgp|
          FineGrainedPermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            FineGrainedPermission.create(action: fgp)
          end
        end
      end

      def add_role_permissions_for_triage
        triage_role = Role.where(name: "triage").first
        return unless triage_role
        TRIAGE_FGPS.each do |fgp|
          permission = FineGrainedPermission.where(action: fgp).first
          RolePermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            RolePermission.create(role_id: triage_role.id,
                                  fine_grained_permission_id: permission.id,
                                  action: permission.action)
          end
        end
      end

      def add_role_permissions_for_maintain
        maintain_role = Role.where(name: "maintain").first
        return unless maintain_role
        MAINTAIN_FGPS.each do |fgp|
          permission = FineGrainedPermission.where(action: fgp).first
          RolePermission.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            RolePermission.create(role_id: maintain_role.id,
                                  fine_grained_permission_id: permission.id,
                                  action: permission.action)
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AddFgpAndRolePermissions.new(options)
  transition.run
end
