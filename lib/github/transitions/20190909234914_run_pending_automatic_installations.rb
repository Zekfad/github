# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190909234914_run_pending_automatic_installations.rb -v | tee -a /tmp/run_pending_automatic_installations.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190909234914_run_pending_automatic_installations.rb -v -w | tee -a /tmp/run_pending_automatic_installations.log
#
module GitHub
  module Transitions
    class RunPendingAutomaticInstallations < Transition

      APPS_DASHBOARD = "https://app.datadoghq.com/dashboard/r5r-7jr-ghm"
      BATCH_SIZE = 500
      MAX_ALLOWED_TIME_TO_INSTALL = 10 # in seconds

      # This hash represents the number of seconds we have to wait,
      # on each backoff retry, before being able continue enqueuing
      # once the current time to install is over the max
      QUEUE_THROTTLING_BACKOFF = {
        1 => 1,
        2 => 5,
        3 => 10,
        4 => 30,
        5 => 60,
      }

      attr_reader :trigger_type, :mass_installations_queue, :installations_limit, :redis, :stale_installation_ids

      def after_initialize
        @trigger_type = (other_args[:trigger_type] || "").to_sym
        valid_triggers = AutomaticAppInstallation::TRIGGER_HANDLERS.keys

        unless valid_triggers.include?(trigger_type)
          log "Please pass a valid :trigger_type to run the transition. It should be one of: #{valid_triggers}"
          raise ArgumentError.new("No valid :trigger_type was provided")
        end

        @installations_limit = (other_args[:installations_limit] || 0).to_i
        if installations_limit <= 0
          log "Please pass a valid :installation_limit to run the transition"
          raise ArgumentError.new("No valid :installation_limit was provided")
        end

        @mass_installations_queue = GitHub::Jobs::MassInstallAutomaticIntegrations.queue
        @redis = ::Redis.new(GitHub.redis_config)
        @stale_installation_ids = []
      end

      def queue_throttled?
        !!@queue_throttled
      end

      def perform
        mode = dry_run? ? "read" : "write"
        GitHub.dogstats.increment("transitions.run_pending_automatic_installations", tags: ["mode:#{mode}"])

        readonly do
          scope.limit(installations_limit).in_batches(of: BATCH_SIZE) do |pending_batch|
              process_installations(pending_batch)
          end
        end

        destroy_stale_installations
        summarize
      end

      def process_installations(pending_batch)
        if dry_run?
          log "Would have attempted to install on #{pending_batch.size} targets"
          return
        end

        if verbose?
          log "Running pending mass installations on #{pending_batch.size} targets. Keep an eye on the auto installs section of #{APPS_DASHBOARD}}"
        end

        pending_batch.each do |pending_installation|
          if pending_installation.stale?
            stale_installation_ids << pending_installation.id
            next
          end

          with_mass_installations_queue_throttling do
            # We directly call .attempt_installation as opposed to .trigger to avoid
            # adding unnecessary events to the global instrumenter, e.g we don't want to
            # delay processing of organic installations due to mass installations events.
            AutomaticAppInstallation.attempt_installation(
              trigger: trigger_type,
              originator: pending_installation,
              actor: pending_installation,
            )
          end
        end
      end

      def destroy_stale_installations
        if dry_run
          log "would have destroyed stale installations"
          return
        end

        log "destroying #{stale_installation_ids.count} stale installations"
        stale_installation_ids.each_slice(100) do |stale_ids|
          PendingAutomaticInstallation.throttle do
            PendingAutomaticInstallation.where(id: stale_ids).destroy_all
          end
        end
      end

      def summarize
        log ""
        log "Current status for '#{trigger_type}':"
        log "  Pending: '#{pending_count}'"
        log "  Failed : '#{failed_count}'"
      end

      private

      def with_mass_installations_queue_throttling(attempt: 1, &block)
        return block.call unless should_throttle?

        if current_time_to_install < MAX_ALLOWED_TIME_TO_INSTALL
          block.call
        elsif (backoff = QUEUE_THROTTLING_BACKOFF[attempt])
          log "Backing off, current time to install: #{current_time_to_install} seconds"
          sleep backoff
          with_mass_installations_queue_throttling(attempt: attempt + 1, &block)
        else
          @queue_throttled = true
          log "Exiting transition with PENDING installations. Please validate the auto installs section on #{APPS_DASHBOARD}} and make sure all graphs look ok :)"
        end
      end

      def current_time_to_install
        key = GitHub::Jobs::MassInstallAutomaticIntegrations::TIME_TO_INSTALL_KEY
        redis.get(key).to_i / 1000
      end

      def pending_count
        readonly { scope.pending.count }
      end

      def failed_count
        readonly { scope.failed.count }
      end

      def scope
        PendingAutomaticInstallation.for(trigger_type)
      end

      # should_throttle? is disabled by default in test env because Redis is not being
      # flushed after each test case and this can lead to flaky or timed out tests
      def should_throttle?
        !Rails.env.test?
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "t=", "trigger_type", "The trigger type we want to run this transition for"
    on "l=", "installations_limit", "The max number of pending installations to be processed as part of this transition"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RunPendingAutomaticInstallations.new(options)
  transition.run
end
