# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190611215636_continue_backfilling_subscription_item_installed_at_for_integrations.rb -v | tee -a /tmp/continue_backfilling_subscription_item_installed_at_for_integrations.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190611215636_continue_backfilling_subscription_item_installed_at_for_integrations.rb -v -w | tee -a /tmp/continue_backfilling_subscription_item_installed_at_for_integrations.log
#
module GitHub
  module Transitions
    class ContinueBackfillingSubscriptionItemInstalledAtForIntegrations < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ::Billing::SubscriptionItem.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM subscription_items") }
        max_id = readonly { ::Billing::SubscriptionItem.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM subscription_items") }

        @iterator = ::Billing::SubscriptionItem.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT subscription_items.id, integration_installations.created_at
          FROM subscription_items
          INNER JOIN marketplace_listing_plans /* cross-schema-domain-query-exempted */
            ON (marketplace_listing_plans.id = subscription_items.subscribable_id AND
                subscription_items.subscribable_type = #{::Billing::SubscriptionItem::MARKETPLACE_SUBSCRIBABLE_TYPE})
          INNER JOIN marketplace_listings
            ON marketplace_listings.id = marketplace_listing_plans.marketplace_listing_id
            AND marketplace_listings.listable_type = "Integration"
          INNER JOIN plan_subscriptions
            ON plan_subscriptions.id = subscription_items.plan_subscription_id
          INNER JOIN users
            ON users.id = plan_subscriptions.user_id
          INNER JOIN integration_installations
            ON integration_installations.target_id = users.id
            AND integration_installations.target_type = "User"
            AND integration_installations.integration_id = marketplace_listings.listable_id
          WHERE subscription_items.id BETWEEN :start AND :last
          AND subscription_items.installed_at IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          log "processing subscription_item #{item[0]}" if verbose?
          update_installed_at(item) unless dry_run?
        end
      end

      def update_installed_at(item)
        ::Billing::SubscriptionItem.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          ActiveRecord::Base.connected_to(role: :writing) do
            subscription_item_id = item[0]
            integration_installation_created_at = item[1]

            sql = ::Billing::SubscriptionItem.github_sql.run(
              "UPDATE subscription_items SET installed_at = :integration_installation_created_at WHERE id = :subscription_item_id",
              integration_installation_created_at: integration_installation_created_at,
              subscription_item_id: subscription_item_id,
            )

            log "updated installed_at for subscription_item #{subscription_item_id} to #{integration_installation_created_at}" if verbose?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::ContinueBackfillingSubscriptionItemInstalledAtForIntegrations.new(options)
  transition.run
end
