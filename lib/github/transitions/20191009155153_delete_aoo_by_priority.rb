# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191009155153_delete_aoo_by_priority.rb -v | tee -a /tmp/delete_aoo_by_priority.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191009155153_delete_aoo_by_priority.rb -v -w | tee -a /tmp/delete_aoo_by_priority.log
#
module GitHub
  module Transitions
    class DeleteAooByPriority < Transition

      READ_BATCH_SIZE = 1000
      DELETE_BATCH_SIZE = 100
      LEGACY_ADMIN_ON_OWNER_PRIORITY = 2

      attr_reader :iterator

      def after_initialize
        min_id = @other_args.delete(:min_id) || readonly { ApplicationRecord::Domain::IamAbilities.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM abilities /* abilities-select-for-admin-on-owner-write */") }
        max_id = @other_args.delete(:max_id) || readonly { ApplicationRecord::Domain::IamAbilities.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM abilities /* abilities-select-for-admin-on-owner-write */") }

        @iterator = ApplicationRecord::Domain::IamAbilities.github_sql_batched_between(start: min_id, finish: max_id, batch_size: READ_BATCH_SIZE)
        @iterator.bind(priority: LEGACY_ADMIN_ON_OWNER_PRIORITY)
        @iterator.add <<-SQL
          SELECT id FROM abilities
          WHERE id BETWEEN :start AND :last
          AND priority = :priority
          /* abilities-select-for-admin-on-owner-write */
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        ids = rows.flatten
        verb = dry_run? ? "would delete" : "deleting"
        log "#{verb} #{ids.count} admin-on-owner abilities in batches of #{DELETE_BATCH_SIZE}, starting with id #{ids.first} and ending with #{ids.last} "
        ids.each_slice(DELETE_BATCH_SIZE) do |slice|
          Ability.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            delete_abilities(slice) unless dry_run?
          end
        end
      end

      def delete_abilities(ids)
        ApplicationRecord::Domain::IamAbilities.github_sql.run(<<-SQL, ids: ids)
          DELETE FROM abilities
          WHERE id IN :ids
          /* abilities-select-for-admin-on-owner-write */
        SQL
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "s", "min_id=", "The start ID to scan the abilities table for AOO abilities", as: Integer
    on "m", "max_id=", "The max ID to scan the abilities table for AOO abilities", as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DeleteAooByPriority.new(options)
  transition.run
end
