# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200227223833_backfill_short_description_on_sponsors_listings.rb -v | tee -a /tmp/backfill_short_description_on_sponsors_listings.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200227223833_backfill_short_description_on_sponsors_listings.rb -v -w | tee -a /tmp/backfill_short_description_on_sponsors_listings.log
#
module GitHub
  module Transitions
    class BackfillShortDescriptionOnSponsorsListings < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator
      attr_accessor :total_updated_listings, :total_updated_memberships, :total_long_descriptions

      def after_initialize
        min_id = readonly { ::SponsorsListing.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_listings") }
        max_id = readonly { ::SponsorsListing.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_listings") }

        @iterator = ::SponsorsListing.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, sponsorable_id, sponsorable_type, short_description FROM sponsors_listings
          WHERE id BETWEEN :start AND :last
        SQL

        @total_updated_listings = 0
        @total_updated_memberships = 0
        @total_long_descriptions = 0
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end

        log "total listings updated: #{total_updated_listings}" if verbose?
        log "total memberships updated: #{total_updated_memberships}" if verbose?
        log "total memberships with long descriptions: #{total_long_descriptions}" if verbose?
      end

      def process(rows)
        rows.each do |listing_id, sponsorable_id, sponsorable_type, short_description|
          SponsorsListing.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            if dry_run?
              log "would have processed listing #{listing_id}" if verbose?
            else
              log "processing listing #{listing_id}" if verbose?
              write_short_description_on_listing(listing_id, sponsorable_id, sponsorable_type, short_description)
            end
          end
        end
      end

      def write_short_description_on_listing(listing_id, sponsorable_id, sponsorable_type, short_description)
        listing_sponsorable_type = ::SponsorsListing.sponsorable_types.key(sponsorable_type)
        membership_sponsorable_type = ::SponsorsMembership.sponsorable_types[listing_sponsorable_type]

        membership = ::SponsorsMembership
          .where(sponsorable_id: sponsorable_id, sponsorable_type: membership_sponsorable_type)
          .first

        unless membership.present?
          log "no membership for listing #{listing_id}"
          return
        end

        if membership.featured_description.present?
          if membership.featured_description.size > ::SponsorsListing::MAX_SHORT_DESCRIPTION_LENGTH
            log "membership #{membership.id} has a long description"
            self.total_long_descriptions += 1
            return
          end

          ActiveRecord::Base.connected_to(role: :writing) do
            query = <<-SQL
              UPDATE sponsors_listings
              SET short_description = :membership_description
              WHERE id = :listing_id
            SQL

            sql = ::SponsorsListing.github_sql.run(query, membership_description: membership.featured_description, listing_id: listing_id)
            self.total_updated_listings += sql.affected_rows.to_i
          end
        else
          ActiveRecord::Base.connected_to(role: :writing) do
            query = <<-SQL
              UPDATE sponsors_memberships
              SET featured_description = :listing_description
              WHERE id = :membership_id
            SQL

            sql = ::SponsorsMembership.github_sql.run(query, listing_description: short_description, membership_id: membership.id)
            self.total_updated_memberships += sql.affected_rows.to_i
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillShortDescriptionOnSponsorsListings.new(options)
  transition.run
end
