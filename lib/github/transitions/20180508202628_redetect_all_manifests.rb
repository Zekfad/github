# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180508202628_redetect_all_manifests.rb -v | tee -a /tmp/redetect_all_manifests.log
#
module GitHub
  module Transitions
    class RedetectAllManifests < Transition

      # @github/database-transitions-code-review is your friend, and happy to help
      # code review transitions before they're run to make sure they're being nice
      # to our database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :max_id, :iterator, :records_updated

      def after_initialize
        start = @other_args[:start] || readonly { Configuration::Entry.github_sql.new("SELECT COALESCE(MIN(id), 0) FROM configuration_entries").value }
        @max_id = readonly { Configuration::Entry.github_sql.new("SELECT MAX(id) FROM configuration_entries").value }
        @iterator = Configuration::Entry.github_sql_batched_between start: start, finish: max_id, batch_size: BATCH_SIZE
        @iterator.add <<~SQL
          SELECT id, target_id
          FROM configuration_entries
          WHERE name = 'repo_content_analysis' AND target_type = 'Repository'
          AND id between :start and :last
        SQL
        @records_updated = 0
      end

      # Returns nothing.
      def perform
        readonly_batches = GitHub::SQL::Readonly.new(@iterator.batches)
        readonly_batches.each do |batch|
          # each entry in the batch is [configuration_entry_id, repo_id]
          repo_ids = batch.map(&:second)
          process(repo_ids)
        end

        log "Updated #{@records_updated} total records" if verbose?
      end
      def process(repo_ids)
        repos = readonly { Repository.find(repo_ids) }
        repos.each do |repo|
          log "Processing repo: #{repo.full_name}"
          unless dry_run
            log "Re-detecting manifests repo: #{repo.full_name}"
            repo.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              begin
                repo.detect_dependency_manifests
              rescue GitRPC::Timeout => e
                log "Exception in processing #{repo.full_name} (id: #{repo.id}): Operation timed out!"
                next
              end
            end
          end
          @records_updated += 1
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "s", "start=", "(configuration entry) id to start from", as: Integer, default: 0
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RedetectAllManifests.new(options)
  transition.run
end
