# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190529154320_backfill_sponsorships_subscription_items.rb -v | tee -a /tmp/backfill_sponsorships_subscription_items.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190529154320_backfill_sponsorships_subscription_items.rb -v -w | tee -a /tmp/backfill_sponsorships_subscription_items.log
#
module GitHub
  module Transitions
    class BackfillSponsorshipsSubscriptionItems < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsorships") }
        max_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsorships") }

        @iterator = ApplicationRecord::Collab.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<~SQL
          SELECT DISTINCT id, sponsorable_id, sponsor_id FROM sponsorships
          WHERE id BETWEEN :start AND :last
          AND subscription_item_id = 0
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each do |item|
          Sponsorship.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "adding subscription item to sponsorship #{item.first}" if verbose?
            add_marketplace_subscription_item_to_sponsorship item unless dry_run?
            add_sponsors_subscription_item_to_sponsorship item unless dry_run?
          end
        end
      end

      def add_marketplace_subscription_item_to_sponsorship(item)
        sponsorship_id, maintainer_id, sponsor_id = item

        marketplace_listing_id = ::Marketplace::Listing.where(listable_id: maintainer_id)
                                                       .pluck(:id)
        plan_ids = ::Marketplace::ListingPlan.where(marketplace_listing_id: marketplace_listing_id)
                                             .pluck(:id)
        plan_subscription_ids = ::Billing::PlanSubscription.where(user_id: sponsor_id)
                                                           .pluck(:id)
        subscription_item_id = ::Billing::SubscriptionItem
          .with_marketplace_listing_plans_type
          .where(subscribable_id: plan_ids, plan_subscription_id: plan_subscription_ids)
          .pluck(:id)
          .first
        return unless subscription_item_id

        log "setting marketplace subscription item #{subscription_item_id} on sponsorship #{sponsorship_id}" if verbose?

        ActiveRecord::Base.connected_to(role: :writing) do
          query = <<~SQL
            UPDATE sponsorships
            SET subscription_item_id = :subscription_item_id
            WHERE id = :sponsorship_id
          SQL
          ApplicationRecord::Collab.github_sql.run(
            query,
            subscription_item_id: subscription_item_id,
            sponsorship_id: sponsorship_id,
          )
        end
      end

      def add_sponsors_subscription_item_to_sponsorship(item)
        sponsorship_id, maintainer_id, sponsor_id = item

        sponsors_listing_id = ::SponsorsListing.where(sponsorable_id: maintainer_id)
                                               .pluck(:id)
        tier_ids = ::SponsorsTier.where(sponsors_listing_id: sponsors_listing_id)
                                             .pluck(:id)
        plan_subscription_ids = ::Billing::PlanSubscription.where(user_id: sponsor_id)
                                                           .pluck(:id)
        subscription_item_id = ::Billing::SubscriptionItem
          .for_sponsors_tiers
          .where(subscribable_id: tier_ids, plan_subscription_id: plan_subscription_ids)
          .pluck(:id)
          .first
        return unless subscription_item_id

        log "setting sponsors subscription item #{subscription_item_id} on sponsorship #{sponsorship_id}" if verbose?

        ActiveRecord::Base.connected_to(role: :writing) do
          query = <<~SQL
            UPDATE sponsorships
            SET subscription_item_id = :subscription_item_id
            WHERE id = :sponsorship_id
          SQL
          ApplicationRecord::Collab.github_sql.run(
            query,
            subscription_item_id: subscription_item_id,
            sponsorship_id: sponsorship_id,
          )
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillSponsorshipsSubscriptionItems.new(options)
  transition.run
end
