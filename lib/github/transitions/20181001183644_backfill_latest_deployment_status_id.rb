# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20181001183644_backfill_latest_deployment_status_id.rb -v | tee -a /tmp/backfill_latest_deployment_status_id.log
#
module GitHub
  module Transitions
    class BackfillLatestDeploymentStatusId < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM deployments") }
        max_id = readonly { ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM deployments") }

        @iterator = ApplicationRecord::Domain::Repositories.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id FROM deployments
          WHERE id BETWEEN :start AND :last
          AND latest_deployment_status_id IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        total = GitHub::SQL::Readonly.new(iterator.batches).reduce(0) do |sum, batch|
          ids = batch.flatten
          process ids
          sum += ids.size
        end

        log "Processed #{total} total rows." if verbose?
      end

      def process(ids)
        return if ids.empty?
        return if dry_run?

        Deployment.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          Deployment.where(id: ids).each do |deploy|
            log "Backfilling latest_deployment_status_id for deployment #{deploy.id}" if verbose?

            next unless latest_status = deploy.latest_status
            deploy.update_columns(latest_deployment_status_id: latest_status.id)
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillLatestDeploymentStatusId.new(options)
  transition.run
end
