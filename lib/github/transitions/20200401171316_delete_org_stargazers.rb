# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200401171316_delete_org_stargazers.rb -v | tee -a /tmp/delete_org_stargazers.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200401171316_delete_org_stargazers.rb -v -w | tee -a /tmp/delete_org_stargazers.log
#
module GitHub
  module Transitions
    class DeleteOrgStargazers < Transition

      BATCH_SIZE = 100

      attr_reader :iterator, :max_id, :min_id

      def after_initialize
        min_id = @other_args[:start_id] || readonly { Star.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM users") }
        max_id = @other_args[:end_id] || readonly { Star.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM users") }
        @batch_size = @other_args[:batch_size] || BATCH_SIZE
        log "Starting with ID #{min_id} and finishing with ID #{max_id}. Batch size is #{@batch_size}." if verbose?

        @iterator = User.github_sql_batched_between(start: min_id, finish: max_id, batch_size: @batch_size)

        # find all relevant users that are Organizations
        @iterator.add <<-SQL
          SELECT u.id
          FROM users as u
          WHERE u.id BETWEEN :start AND :last
          AND u.type = 'Organization'
          AND NOT u.disabled
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each_slice(@batch_size) do |ids|
          ids = ids.flatten
          log "searching for stars for #{ids.count} org users:\n#{ids}" if verbose?
          found_stars = readonly { Star.github_sql.values("SELECT id FROM stars WHERE user_id IN :ids", ids: ids) }
          delete_stars(found_stars) unless dry_run?
        end
      end

      def delete_stars(ids)
        ids.each_slice(@batch_size) do |ids|
          Star.throttle do
            log "deleting #{ids.count} org stars:\n#{ids}" if verbose?
            Star.github_sql.run(<<-SQL, ids: Array(ids).compact)
              DELETE FROM stars
              WHERE id IN :ids
            SQL
          end
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "start_id=", "Star ID to start processing at", as: Integer
    on "end_id=", "Star ID to end processing at", as: Integer
    on "batch_size=", "Number of rows to process at a time", as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DeleteOrgStargazers.new(options)
  transition.run
end
