# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200124231918_update_follower_threshold_sponsors_criterion.rb -v | tee -a /tmp/update_follower_threshold_sponsors_criterion.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200124231918_update_follower_threshold_sponsors_criterion.rb -v -w | tee -a /tmp/update_follower_threshold_sponsors_criterion.log
#
module GitHub
  module Transitions
    class UpdateFollowerThresholdSponsorsCriterion < Transition
      DEPRECATED_SLUG = "follower_threshold"

      CRITERION_ATTRS = {
        slug: "follower_threshold_v2",
        description: "Account has at least 5 followers?",
        automated: true,
        applicable_to: :user,
      }

      def perform
        create_sponsors_criterion(CRITERION_ATTRS)

        depecated_criterion = readonly { ::SponsorsCriterion.find_by(slug: DEPRECATED_SLUG) }

        if depecated_criterion.present?
          log "Found deprecated criterion (#{DEPRECATED_SLUG})."
          set_criterion_to_inactive(depecated_criterion)
        else
          log "Couldn't find deprecated criterion (#{DEPRECATED_SLUG}), skipping marking inactive"
        end
      end

      private

      def create_sponsors_criterion(attrs)
        if dry_run?
          log "Would have created #{attrs[:slug]}"
        else
          SponsorsCriterion.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "Creating #{attrs[:slug]}..."

            criterion = SponsorsCriterion.new(attrs)

            if criterion.save
              log "Created #{criterion.slug}."
            else
              log "Failed to create #{criterion.slug}: #{criterion.errors.full_messages.to_sentence}"
            end
          end
        end
      end

      def set_criterion_to_inactive(depecated_criterion)
        if dry_run?
          log "Would have set #{depecated_criterion.slug} to inactive"
        else
          log "Setting #{depecated_criterion.slug} to inactive"
          depecated_criterion.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            depecated_criterion.update!(active: false)
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::UpdateFollowerThresholdSponsorsCriterion.new(options)
  transition.run
end
