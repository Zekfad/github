# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190919080921_backfill_sponsors_membership_contact_emails.rb -v | tee -a /tmp/backfill_sponsors_membership_contact_emails.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190919080921_backfill_sponsors_membership_contact_emails.rb -v -w | tee -a /tmp/backfill_sponsors_membership_contact_emails.log
#
module GitHub
  module Transitions
    class BackfillSponsorsMembershipContactEmails < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ::SponsorsMembership.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM sponsors_memberships") }
        max_id = readonly { ::SponsorsMembership.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM sponsors_memberships") }

        @iterator = ::SponsorsMembership.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT sponsors_memberships.id FROM sponsors_memberships
          WHERE sponsors_memberships.contact_email_id IS NULL
            AND sponsors_memberships.id BETWEEN :start AND :last
        SQL

        @total_memberships = 0
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |membership_ids|
          process(membership_ids.flatten)
        end

        log "total memberships updated: #{@total_memberships}" if verbose?
      end

      def process(membership_ids)
        memberships = readonly { ::SponsorsMembership.where(id: membership_ids) }
        sponsorable_ids = memberships.map(&:sponsorable_id)
        primary_email_ids = readonly do
          ::UserEmail.verified.primary.where(
            user_id: sponsorable_ids,
          ).pluck(:user_id, :id).to_h
        end

        # fallback verified emails if primary email is not verified
        verified_email_ids = readonly do
          ::UserEmail.verified.where(
            user_id: sponsorable_ids,
          ).pluck(:user_id, :id).to_h
        end

        memberships.each do |membership|
          if dry_run?
            log "Would have set contact email for #{membership.id}" if verbose?
          else
            ::SponsorsMembership.throttle_writes do
              sponsorable_id = membership.sponsorable_id
              email_id = primary_email_ids[sponsorable_id] || verified_email_ids[sponsorable_id]
              membership.update!(contact_email_id: email_id)
              @total_memberships += 1
            end

            log "Set contact email for #{membership.id}" if verbose?
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillSponsorsMembershipContactEmails.new(options)
  transition.run
end
