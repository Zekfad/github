# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190626165357_delete_orphaned_ability_records_on_deleted_teams.rb -v | tee -a /tmp/delete_orphaned_ability_records_on_deleted_teams.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190626165357_delete_orphaned_ability_records_on_deleted_teams.rb -v -w | tee -a /tmp/delete_orphaned_ability_records_on_deleted_teams.log
#
module GitHub
  module Transitions
    class DeleteOrphanedAbilityRecordsOnDeletedTeams < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator, :max_id, :min_id

      def after_initialize
        @min_id = @other_args[:start_id] || readonly { Ability.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM abilities /* abilities-select-no-priority-needed */") }
        @max_id = @other_args[:end_id] || readonly { Ability.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM abilities /* abilities-select-no-priority-needed */") }
        @batch_size = @other_args[:batch_size] || BATCH_SIZE

        log "Starting with ID #{@min_id} and finishing with ID #{@max_id}. Batch size is #{@batch_size}."

        @iterator = Ability.github_sql_batched_between(start: min_id, finish: max_id, batch_size: @batch_size)
        @iterator.add <<-SQL
          SELECT id as ability_id, actor_id as team_id FROM abilities ab
          WHERE id BETWEEN :start AND :last
          AND ab.actor_type = 'Team'
          UNION
          SELECT id as ability_id, subject_id as team_id FROM abilities ab
          WHERE id BETWEEN :start AND :last
          AND ab.subject_type = 'Team'
          /* abilities-select-no-priority-needed */
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each_slice(@batch_size) do |ability_team_id_pairs|
          Ability.throttle do
            team_ids_from_ability_query = ability_team_id_pairs.map(&:last).uniq
            found_ids = readonly { Team.github_sql.values("SELECT id FROM teams WHERE id IN :team_ids", team_ids: team_ids_from_ability_query) }
            team_ids_to_delete = team_ids_from_ability_query - found_ids
            ability_ids_to_delete = ability_team_id_pairs.select { |ability_id, team_id| team_ids_to_delete.include? team_id }.map(&:first)
            if ability_ids_to_delete.any?
              log "deleting #{ability_ids_to_delete.size} abilities for team ids:\n#{team_ids_to_delete}" if verbose?
              destroy_abilities(ability_ids_to_delete) unless dry_run?
            end
          end
        end
      end

      #delete any orphaned ability records where either the subject or the actor is a team that has been deleted
      def destroy_abilities(ids)
        Ability.github_sql.run(<<-SQL, ids: Array(ids).compact)
          DELETE FROM abilities
          WHERE id IN :ids
        SQL
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "start_id=", "Ability ID to start processing at", as: Integer
    on "end_id=", "Ability ID to end processing at", as: Integer
    on "batch_size=", "Number of rows to process at a time", as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DeleteOrphanedAbilityRecordsOnDeletedTeams.new(options)
  transition.run
end
