# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180821172658_migrate_duplicate_admin_center_business_settings.rb -v | tee -a /tmp/migrate_duplicate_admin_center_business_settings.log
#
module GitHub
  module Transitions
    class MigrateDuplicateAdminCenterBusinessSettings < Transition

      attr_reader :global_business

      NON_SITE_ADMINS_CAN_DELETE_REPOSITORIES_KEY = "disable_non_site_admins_can_delete_repositories".freeze
      NON_SITE_ADMINS_CAN_CHANGE_REPO_VISIBILITY_KEY = "block_non_site_admins_from_changing_repo_visibility".freeze

      def after_initialize
        @global_business = GitHub.global_business
      end

      # Returns nothing.
      def perform
        return unless GitHub.single_business_environment?

        if GitHub.config.enabled?(NON_SITE_ADMINS_CAN_DELETE_REPOSITORIES_KEY)
          actor = Configuration::Entry.find_by(target_type: "global", name: NON_SITE_ADMINS_CAN_DELETE_REPOSITORIES_KEY)&.updater
          @global_business.disallow_members_can_delete_repositories(force: true, actor: actor)
          GitHub.config.delete(NON_SITE_ADMINS_CAN_DELETE_REPOSITORIES_KEY, actor)
        end

        if GitHub.config.enabled?(NON_SITE_ADMINS_CAN_CHANGE_REPO_VISIBILITY_KEY)
          actor = Configuration::Entry.find_by(target_type: "global", name: NON_SITE_ADMINS_CAN_CHANGE_REPO_VISIBILITY_KEY)&.updater
          @global_business.block_members_from_changing_repo_visibility(force: true, actor: actor)
          GitHub.config.delete(NON_SITE_ADMINS_CAN_CHANGE_REPO_VISIBILITY_KEY, actor)
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::MigrateDuplicateAdminCenterBusinessSettings.new(options)
  transition.run
end
