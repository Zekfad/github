# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200130150612_backfill_version_numbers_on_installations.rb -v | tee -a /tmp/backfill_version_numbers_on_installations.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200130150612_backfill_version_numbers_on_installations.rb -v -w | tee -a /tmp/backfill_version_numbers_on_installations.log
#
module GitHub
  module Transitions
    class BackfillVersionNumbersOnInstallations < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM integration_installations") }
        max_id = readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM integration_installations") }

        @iterator = ApplicationRecord::Domain::Integrations.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, integration_version_id FROM integration_installations
          WHERE id BETWEEN :start AND :last
          AND integration_installations.integration_version_number IS NULL
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        IntegrationInstallation.throttle do
          log "doing something with #{rows.map { |item| item[0] }}" if verbose?
          run_batch_update(rows) unless dry_run?
        end
      end

      def run_batch_update(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::Integrations.github_sql.new "UPDATE integration_installations SET integration_version_number = CASE"

          rows.each do |item|
            id = item[0]
            integration_version_id = item[1]
            sql.add <<-SQL, id: id, integration_version_id: integration_version_id
              WHEN id = :id THEN :integration_version_id
            SQL
          end

          sql.add "END WHERE id IN :ids", ids: rows.map { |item| item[0] }
          sql.run
          sql.affected_rows
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillVersionNumbersOnInstallations.new(options)
  transition.run
end
