# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20170404152122_submit_failed_visa_transactions_for_settlement.rb -v | tee -a /tmp/submit_failed_visa_transactions_for_settlement.log
#
module GitHub
  module Transitions
    class SubmitFailedVisaTransactionsForSettlement < Transition
      # Returns nothing.
      def perform
        subscription_ids.each do |id|
          submit_latest_transaction_for_settlement(id)
        end
      end

      private

      def submit_latest_transaction_for_settlement(subscription_id)
        subscription = Braintree::Subscription.find(subscription_id)
        transaction = subscription.transactions.first

        if transaction.status == "authorized"
          submit_transaction_for_settlement(transaction.id)
        end
      rescue Braintree::NotFoundError
        # skip this subscription; it no longer exists
        if verbose
          "Subscription #{subscription_id} no longer exists"
        end
      end

      def submit_transaction_for_settlement(transaction_id)
        if verbose
          puts "Submitting transaction #{transaction_id} for settlement"
        end

        unless dry_run
          Braintree::Transaction.submit_for_settlement(transaction_id)
        end
      end

      # Braintree subscriptions that were retried as part of:
      #   https://github.com/github/github/pull/70995
      #
      # List of IDs generated during dry run and saved in this gist:
      #   https://gist.github.com/chrishunt/bf648dddb4c3edff45b3f3ac954af5ae
      def subscription_ids
        %w(
          j9hkm2 23sy2m 6hbk6m cy8ds6 hvrfm2 9qh4jb 47z7n6 hzwh7r 5njt76 hkvhsr
          cp84xr fn8mbm fbvvx6 f5mgkg 755hgm bnvzhr fv4ffg 8pn6h6 j6h84w dv3zpb
          fmf42m btzpcr 52nzc6 7gyjcr 6hp8vg d4vwkg 8frmtb 79rz8w hb96bm gwpw62
          5d6tcr f4v6yw 7k3hb2 kjfpcr f6gcb2 c5wktw dy7q6m 83cjmm gns75g jt3fzg
          4ssfc6 gss5tw 52v7b2 2hd876 gdzn36 6hnwdw 8ys6h6 4rgvc6 b79qpb 3gcjxr
          f2f98w fjn3w2 fxst4w 9g4r8b gjc9rm j5p49g 4vmk6m 5ksxdb g4sd2m c9j3yb
          8bcgbm jgn8g2 4gk6yw gc3rwm fwmm62 jgscdb 2f8dvg fpnf22 byxnb2 b9v8cr
          8fxgnr 9w4mbm 9h5gh6 b5gymm d5p96m 9r3ds6 3rqwwm cjcs7r fc3gbm 2b9pcr
          8g9wwm jc53gm 35wjxr 7p4xsr 3j4sw2 f9sj2m fnj2yw by3w3r dxwr3r 46nds6
          bwh3w2 7xycsr 2s6jvg 85qkrm 8rsn7r bqkrnr 38mbnr cbv2dw 96fsn6 52b9c6
          gbnxqg 9d9z6m 9md5x6 4x54cr jqz49g 2rpm8b hbfhqg cw23gm dn2ktw 2k3n7r
          293y9g f5r3jw fgpts6 3gh5fg b23fpb k7svfg 86fqzg fyjbtb k2m6tb fsqz6m
          2mtd2m 6hvh5g 3nrbbm d4jkc6 bs4662 gnmqrm 7mb336 dkwr62 kbh44w b7s5hr
          gm9dvg h3n77r j5chyb gpzjg2 6byjcr 3mwf8w ckvkhr d9cwbm 9rywh6 98qyvg
          8x923r k2cm8b 8rj5c6 895522 jvhrh6 22pmbm 2jgqhr dzg2h6 3ks3jw 2vcqm2
          7wcg8b 4tr9pb d8fwwm 9g4cgm 33kwnr bkmxn6 39bvpb 5h7d9g b5zj4w k3jt76
          7gbvrm hqw7db 2hhtpw 8p8c5g bbwqc6 3z78s6 7nmrr2 fbj68b 8zc77r hfqtmm
          7b3y2m bxm3gm dmbsqg cj7ssr 577gwm gct2bm fks6h6 277v4b brnmwm 3z5ryw
          8b2hjw dgh6r2 c772bm fvdw8b d6kp2m 44zz6m 8p4mnr kbqy76 4nfryw 6gnj9g
          bx6hjw 7zv7w2 8nmtcr gvt8jb 5yr9zg h7gzhr 3pfqpb 7g3d9g fwfcjw jky5c6
          66r82m 5djw62 dfz5pb 32mmdw 92g7db 9tqsn6 76gt2m ddhcdb dw9yjb 9xz7b2
          37gt9g ggnwr2 75hxqg 3bzhsr 8czqpb 6h8jxr 72wfm2 96h9fg 6jhdpw jk7zzg
          7hkxyb 6qcmkg k7kw8b 4km4cr 59pwdw c6p3w2 jrf2nr 39zs36 d52zc6 gzxf8w
          5f62tb 5dgnsr g9w5zg fzm6r2 9vyhdb 6vf336 2dzps6 khjwr2 dz53db hjycqg
          8wbhw2 9nttvg d7ygr2 fnbtpw jjyd2m 5csz22 5rm922 3fw4s6 kmmqzg 6ybdmm
          cs384w 58r23r cwyxsr 4zykzg 39k7b2 2xjkpb 2qcfc6 dzvyvg 4vh3n6 3jbndb
          4qrqfg 3xkrbm 43f7n6 j36bbm 4m27qg d3jg62 3ngqc6 5s737r 7zzqrm 9p2p2m
          4pnq8w 7khpmm fstys6 fpbg8b cdw54b dxg2tb 7z282m dqkr3r db98vg dpt7jw
          7fzwyw fpycsr dshg8b df8vx6 k4tw8b jrb6r2 4vvndb gtqgr2 9c6gh6 2w3ds6
          2s3sqg 4nfnsr 3hfdvg bvshw2 957ts6 bdfmyw 3qpdjb 4ts8cr b4vpg2 3rpzzg
          9xcxgm g74w3r dpk44w 5gsgdw dhtj4w f5ctkg dqrwwm b77byw 6jqqzg k4hxgm
          2w3d9g f459pb 7htnjw dkpryw 85pftw hzrmr2 jn7mwm 8fcdg2 995f8w dy82r2
          8hx9zg 2f65pb bgwzhr gjh8pw c2bxyb 5wpbdw 7bfwnr hc2pvg f3nkpb 8ssyg2
          54hcb2 htwwyw h2dhtw cs335g cp48g2 c95wpw 8z582m 2q8q36 3wtp76 gsgtg2
          9879tw 76td2m hv9z8w 9wns7r 3qr662 jp4876 7x2vm2 7wycjw bnrc4b 5hxk22
          23mpvg 6k9sn6 6scsdb dm4wxr b288mm 5f7dxr gxwx4b 66f64w fn97qg bb7t4w
          5p5xyb h8c37r f5j7jw fmshqg 2nn4g2 4z375g 6wp4xr f4r2dw fz5cyb 5dzkfg
          j8p2yw dcd8yw hdpyjb 4rszx6 f4xw76 fqfsn6 hgzw62 8n4j62 grsf4b 8rxm62
          gmq77r 7jhfpb 4gc7gm fqv6wm 76zmbm 94bssr db6z6m jnrjvg 5cz7b2 6d3w3r
          2ntdkg fq8kc6 7wbcrm gx6ps6 9tv262 2bnnpb 85ch36 39w2kg 8wtpxr 34k5sr
          4zdhsr 7538g2 2cnzsr 346fm2 2tspvg fkd7gm 9r3sm2 2rnmwm 9zrr76 hwc7gm
          393m62 4xy536 ctc2wm 58gydw hqtbtb g32gwm b6sf4b 4nghw2 5s8j4w 2rf49g
          9ymnm2 3zr6r2 9ykgdw 7rnv4b c9twr2 2nrngm cwv3n6 g5wbyw bgvjpw fr6bnr
          4dfc5g 382j4w hzx44w d9q6kg k387jw ftdnb2 hqg42m 5pj2nr 9r9vx6 7pcj76
          9rf3yb bgc56m 9g38g2 hs4s5g csqw8b jbxq4b 28y7sr h74ycr 59g6bm jqjjpw
          5cnhn6 3kxf8w 95mmwm 9wjs5g 9h24s6 4nm4pw 7snbnr 2bhnjw bhcqrm 8gvyjb
          fqjsjw j55g9g jrqyg2 63wc36 8x2jg2 7ftt76 8yctcr 5xbrdw 7ytvrm 2pjnyb
          b3mqrm 5vr6yw brgzhr c3typw 4p26r2 2v8dg2 f8p5tw 9yb2dw 57d3yb 8mc7qg
          2hd4pw 49x6r2 7r8722 jr9ftw dv7ymm 23kwcr c4kvtw chwbkg 9qjgtb 7c36tb
          3qn63r 768p76 7fqpmm jk6wnr 734ds6 23fhgm 62qrwm 663t2m 9bnwyw bp63db
          ftwnqg 8zfq8w 49gn5g 9pnycr kcdpr2 cg59db 3j2vgm 2mrw8b j9kftw cqt7zg
          cwr82m 23t6h6 6cf3n6 3j36dw c9m6nr 7gt9fg d2spjb fqphqg 3s4ycr 9wcwbm
          9my37r hv7r3r 9zz2yw 94g2r2 6j3ts6 hyhpjb 2n5btb b8ym8b cfzr4w gh7fm2
          5vtfpb 4gv8vg bcyxqg gjrqpb 6q756m g8r7db kb35rm bhtk22 65j5pb 24sg4w
          4mgryw 5t2khr h6dt4w 8zt9tw 7g7vpb f2zkw2 j2vcjw 525xzg 35wt9g 7bygjb
          6wmhgm 86c56m 5wskqg 864njw hkbt9g j6dgyw 2gywr2 2n2zdb db698w 77fm76
          gmyppw j7wxfg jddb9g gmnrxr 44wwdw 8qwtmm 9kxqm2 gt67b2 4zrw4w 287bnr
          c36qjw c498jb d65qpb 6rkdg2 5wx2yw b2nv4b 3vgyvg cw46yw 8mjsgm ht4dcr
          9zp4pw 59khdb kgytg2 6brth6 bzjj4w c6t7pb 235vhr 6jvgh6 f7t9m2 hsxb3r
          6bfgdw 4mvk22 dzbxdb bpk7w2 d6kh36 2qkzx6 gfb2dw 7862dw gbpkfg gjrvc6
          grkrwm jpbtmm g26fhr 43mhzg cc4pvg 6n67yb 78j5zg 7n9zrm gk3y62 7qrpcr
          336q6m 2w6qc6 cndvm2 kffjcr 7x8ssr 42pxw2 f3g7b2 3ywhsr hx8ssr hwkdg2
          g7f82m jdjgyw fx5j4w 2mym3r ghndg2 8vjbr2 75sf22 5mtwdw 4785hr 9tp4vg
          6p4522 5w6w8b kb7ymm gszvm2 gswhw2 fgvkw2 7bwzw2 2nr29g 9df68b j7z2dw
          3525pb 75jtnr 7gphyb jt59pb brdhgm 8n9cn6 g3c922 hrpr2m 3h53gm jyr5jw
          c883w2 5pcdmm gs2vm2 56s83r dw642m fntsc6 g7mppw 3zvk4b kmngwm 6jpvx6
          j6v58w 54cktw 9cs8pw h4z7jw djmx36 f7sfgm 7vyypw jrb23r 59cbdw cft4jb
          7h889g kfx97r 295krm 2b9z5g c6rqw2 4685tw 872qzg kb6w8b 9x8p2m 4srpg2
          4b7r3r f7cjvg 7fkmjb 526th6 bdqr4w b62xfg dfqhqg hwwfqg 4r6f36 9kvffg
          9c2cjw 2wzx22 f4nchr 6v8tyw 87h6wm 25fqrm 7c93hr 8bkc22 3mr7hr d556kg
          g66bg2 khnmyw 9gzbyw 2ctkfg g7983r 7whwnr c7j6pw 6wwrvg k9hgwm jnfj3r
          6ytxjw 56ns36 f5rsyb 54xzm2 bqgmdw 3bv9qg 8wyffg 7dfx22 8x3z8w 22hb2m
          cjvbkg gwm722 bytrwm 65r3db dq237r 7vz3pb 5td8vg 8m7h7r 6rp8nr bngcm2
          7mwftw jd4y2m 8kpbtb cxcdcr 38q6kg 59bftw 6vfzfg 7xpgpw dxmmmm 4s3ybm
          fw3j76 jxypr2 7wvvdb 4343w2 3nrkn6 g39s7r 954p76 2mjkgm h49bcr j6z4tb
          65m29g 3288tb dpfb2m 36hph6 hvw63r 75xhyb 8y2wkg 4zgyvg 6dkkjw 2k824w
          5bvpxr 5zbmmm 56syyw 4jg2r2 3kpq6m 7998r2 k39kpb fg3r4w fj8vrm 4gwyr2
          7zvys6 38tx36 7drt9g 7wrb4w 86hv22 dsyjtb h9w862 gcjfpb 9r9cgm 4s4nqg
          fnws6m 4vszx6 97wgkg 3w3hm2 j2xg8b bj354b k47s4b fndxqg fnrndb 9vndmm
          3pmgcr fdsx22 3d3n22 8brj9g gfbv5g 3fnnzg kmgzhr 5q7ctw f9qp62 7xwstw
          b2fr8b 62hg4w 4sj7zg cq5fgm c95276 9p7wbm 45jb9g f9z55g 5kdbkg fxsfpb
          jfyfzg g988g2 8mwn7r 84g29g 6y8sn6 by74yw ghncyb c7pvyb d9sv6m ggbvgm
          7gzkqg jwhrh6 gnq8dw c6kv6m 642r76 b8k3tw 68tpjb cc977r bj49b2 4wqgg2
          24szsr 5y49b2 37frs6 d2kktw fvyk4b gfsxhr 3vg9sr bmy5db g4h322 dzzsfg
          8rqj8b 7nvrg2 ckmyxr 884pwm cfjnjw 7kdpjb k3zvx6 6trnx6 5z4z22 cnd7w2
          fkptnr bsw7hr c9mmmm 426wbm dkf9hr 2q6gcr j32ftw cb7gyw d4kjpw 2t3hx6
          6jd4r2 kc223r j48fjw bkssgm 5yzdjb 9ywvgm hddw62 d92p76 bt6cjw dd564w
          cysk6m bmg9x6 7dhhb2 d35876 28rm3r gqhb62 hctsyb 2vt8s6 9ngqtw 8wvb62
          5tjmkg 7dfg3r jk7876 j6gvpb khwzqg 9t88yw gdhsfg cx59zg 24fs4b 4qttmm
          crq2vg f8s5x6 7q7dxr 9mn4g2 3wg8mm dhyz22 fcmm3r 7hy2g2 fbxzgm 558kx6
          3nh4nr jgrkm2 46k74b fv8qtw hy55gm 2rs6dw cknxb2 646vsr jn4jtb 4yk7w2
          7dds22 2cv68b 958s5g 9r8y2m h6q4yw 89484w 6qxzyb 3xv4tb hqdypw 2rgjbm
          7ggjs6 2t62xr cn256m 5v6vm2 hwkxhr 84d2g2 3gx7fg 2yjdcr 43ch4b 2h5t3r
          9r7vpb 98wnqg 6m5t3r 2x5z4b d6zgdw gf9ynr kcknyb 2g6shr 6vs2pw 89x322
          bfp7zg g9cnzg 98st3r dfvdvg 7rtms6 8qybdw hgwvm2 by57w2 bfgf36 34swjb
          jrxxn6 683cyb k4xyyw f4gxw2 kd8sc6 5gmc8w fzwm3r 2vw38w 2t6g2m 9g4vyb
          bpp7gm 9xc54b 6cnh8w 7x2x4b c6shfg k8kn6m 66nfx6 7c2ztw dbpfc6 gy2djb
          ckhwvg 5wq6xr fjvchr 65j8tb 64s82m 5rrhpb 6bxx6m 98j75g 8m9vjw c68vfg
          25qm76 bhbnx6 dhppdw bgb5jw 5nm522 7sycfg hjrp8b fghnzg gpxy62 b8qsfg
          24hjvg h3gqc6 54r736 5svn6m dfwryw 6dqnn6 8cb3x6 6ssd9g 9m9ds6 fkt34b
          9f5zn6 kk4z7r jmf7n6 kh8rcr dp8tnr 6rhk6m k7xfx6 jjzvtw 94v5gm jfdtnr
          gj7sqg 7b298w 6fpgyw fdckqg 3nn5rm b5y7sr 6bgx7r h3v5x6 7zxfm2 6bm3fg
          8jgg2m 33bd76 2r5nb2 jgkhx6 fr3q5g 8kmbkg 86vjpw dybpdw 7bkvyb 53k55g
          jrnrr2 852g2m bjm6r2 9zg522 7cfn22 94ndpw 3f62mm 7tdcgm g6qb62 jc4nfg
          6hf8dw 774nfg 29xb76 jvs2tb 63d4wm 7jftpw 2bvyg2 hqcgwm 5gbt62 grwd76
          8h7wnr c9hbr2 4bdj62 ffjr62 8ydvyb kfyts6 3n6xx6 46cxb2 jj7sm2 8g99n6
          b58d8b 8sv7zg kk6k6m 92ctpw c5gws6 6v3vjw jf5qgm cgx5sr hhwshr bp8qdb
          4btgjb 3gcdxr 9bm5qg 6vqxc6 j33bcr cqjzdb 6bcvw2 g4rhzg g6rcc6 ftd62m
          b3q462 62vhdb 6v6g9g fvg34b kfksx6 49jnfg 5p8vb2 cfhf36 k8wydw 3cghhr
          6dzhx6 7tjdr2 49b5rm 93jp76 dqqcc6 jjq22m jvj24w 43zdnr 697zpb 4x22s6
          2tc97r frt676 7y4ms6 3tcr4w c4kj8b 46jdpw jqpwwm 5snph6 ck5t3r gk6p2m
          fpjpr2 65hd9g crn7rm jvx5rm 5h6262 d72h22 2dy6h6 8vcvc6 fjp3qg 6ggmg2
          8xbrbm 3bcchr drdd8b c4mpr2 45v522 bgs9zg 2f9y62 b36j8b 4fvjwm 8q7qjw
          cmbnm2 263mtb 2qbywm 9c3gwm 6c98dw 73824w 6pry3r c475x6 gbrd76 74gfpb
          jgsbcr 7qb56m cdqdyw cwc9w2 6qtszg 8xvzrm d2b6cr 9888bm k4gf6m 84j3rm
          5q5bxr 7ncvzg 8bs5yb c2nnm2 g9jfc6 bztycr 5jcr76 drpfdb h4v7m2 79nqb2
          cnn3zg 8pzyh6 9385db 8tr3rm 8mt6pw g7nt2m bxsynr cg8b76 d2g2xr kmbzjw
          j3qjr2 69qybm 2rsntw kcxr3r 5mvbcr 59pvfg 5m2s6m 8y2m4w hmfbcr 8zbmxr
          9685w2 2rhn8w 432m3r 7s7sm2 2rb2xr kcmw9g k66ttb 7c2rcr 9872jb ck3mjb
          5y6rvg f4j5qg cq9hx6 59n462 4djd8b b7d8tb b6m8bm 2tnqyb 8ycwvg 3j8v5g
          gmfgs6 6ht7sr 6c93pb 6sr7yb hww28b gshy62 gd84bm dpgdcr gpf2pw 6h7hm2
          db73pb js5nc6 8rt9n6 8dm38w 5b97fg ffsqjw drch8w 3jpth6 8sm94b 99d6cr
          c76cx6 268t4w 78w8h6 94kph6 c8p57r 9b6t3r 287ytb 2fc97r 59tw76 3cmydw
          4sfzyb 9vjh22 6bkzsr g4kbvg fvb7n6 d6k7m2 ds87c6 3gny9g cxsjjb 3zyv5g
          h279jw 66sntw 88q69g 6jg6vg 8jcktw g89t3r 67mdr2 9k3crm 7gwn4b cvmtbm
          gpg5jw ftxtwm b239pb 8rdstw dvdr9g f9fb9g fvmkm2 3d77hr g7jxfg c6y29g
          fzt3fg kdgs22 j8y8tb dhnrwm 8df7x6 c9dpwm 94b98w b9yc4b hfsv5g jqm64w
          8mg6bm 7pynw2 jg754b 2s5qyb jdbg2m 9mqb62 6k2mmm jrcmcr bd3ytb bbwtbm
          gtmvm2 2jgq22 fvf5gm fyqj62 csfz5g 6xwszg jnqxpb gv24jb 6mfhfg 9kvvqg
          k838dw 3hfdh6 gb7m76 dtcyr2 5pzg4w 92zdnr by5chr k4jshr 83q55g 76mw9g
          jjvq5g 7vjy62 4pq4tb fy5nhr gr5yh6 3n29sr 76mhx6 g3m2kg 3bphzg 37s35g
          dq5wvg j54b9g 9tj6jb jyq8kg bxzqgm hk2pdw 6m5kw2 cdrjbm 872vyb 57v2vg
          7jsx6m 87rgmm 99sjdw dzvjdw 88shfg 9d35yb 6qqfyb 86vrs6 9ybzdb 5pgh6m
          85pywm f2xdbm cx5qyb 845tdw 7wfynr 2sbzc6 2srxqg gn52vg gfz7x6 2bpzgm
          5d9hm2 h3vt3r grcpbm 8z955g ffvrg2 3zfx6m kf6jbm 7tydr2 3ghsm2 65zpyw
          8shx6m hnymg2 24276m 4knq5g 6hhbjb 9dr38w 4fq3hr gdkgxr hb3xpb 56wy3r
          7kp7hr gc55gm bw5wpw 4sn7m2 cfd662 fmc5sr 3th5sr 7sgrjb 3ndrxr 8xvbpw
          9t77rm 8m6nm2 g29xn6 3tkvn6 dkw3rm 8dd4kg 99ssrm df4szg fdn8wm fkjpvg
          64m936 fczcc6 cw869g gjdx8w 4smgpw 8pk9w2 chzrmm gdqph6 8xs4nr 2fthpb
          g2ps6m 2pjmxr 7kk9jw dtctdw gbgkrm b8vgjb 435b76 68224w cc7zqg crqsrm
          53ngkg 4dxynr 8333m2 2q5fsr 6jmrcr 354jyw 6kcs6m fq8pyw 5qrz6m fqcpkg
          h452vg 7wtkgm ft5bcr 9r9sx6 5srvsr drd4jb cwjzdb hd47tw 6zw76m 7mk2g2
          bw76s6 9mfkqg gc3w2m 7hh862 4dtykg 3shvdb 8r3qfg c9s5gm 8m2bxr fzprvg
          hrgxm2 cmxdxr hgst8b 9ynzdb dhdvjw 73tsgm 8t4bxr 8rdkgm jbf83r fgy9rm
          43fbxr g5gwpw 6zc97r fw3shr 5889gm 62wpr2 6fgfn6 bk5n6m 9qgvsr cz8bg2
          4t8ttb 2ztj62 92frxr bwdzsr 22kjr2 gv93fg gc38tb 4cwj62 8mk8wm gqvjh6
          fcwcrm k54s6m 6g7hx6 3mqhrm 6383m2 bgr862 hyd8h6 k4z722 4c9pyw j94gg2
          cttmpw 9twn4b 7d4vrm 3ndqyb 8rqpyw 2fjvyb c7pqb2 53g5jw khzvb2 gtck7r
          8wgqgm 5m8rcr 24578w 4h85w2 bxc48b 35s6s6 k7rtdw fnshx6 8ymzw2 39y4nr
          8q9ksr jc96xr hd3pnr 5vw6s6 73cz36 29wdr2 8xmzqg 87prjb 5wwjwm 3t96mm
          fpxr76 9zb43r dhqj2m 2td5b2 39xkw2 786c4b jyggh6 59c6r2 grvjkg js2zqg
          7dbdbm 7cvc22 52ky4w 7mwjbm dfm9n6 gb63fg g27jvg 9dv7m2 jv9mnr bv5qb2
          68q7c6 9stmkg fk3bpw cr83c6 kkbvx6 hdhbbm 528rxr cp6czg hf97pb 84f5db
          49fqm2 7yphfg kczmbm 854k5g kj44dw 2574tb 8p6yyw cp6g62 4qn4nr 5brqc6
          4qgh6m fr4m4w jqt5sr gpmtr2 329kw2 8m57jw jtb62m 93whhr k9978w jshx6m
          7qyshr gthfsr jdyssr fbx536 d9c2pw 78q8nr c35zsr jxjg2m d6k2g2 hv57x6
          gp7m9g chr4wm fbp862 k2jy8b j2hsm2 44vywm k8b5sr gxnw9g 35xj76 k7wzb2
          gf62g2 4hrwpw bff5gm c6m5gm 539p62 jyt95g 7wnxtw cq88bm ftz74b 27jkgm
          6tx7fg kdjgjb ff8cpb d936vg k9rtr2 2kmj62 cmrb9g 935czg 3xj6jb 6hnnpb
          hp248b 9yc2mm 3247hr 3fyvn6 5ss3hr by38h6 62px8w jnvhfg bj6ykg cc7m9g
          cqc7x6 9v7kw2 9y457r 3m8h22 dzc676 457qyb 9mbstw hj84r2 dj6bxr jpfqgm
          8c8m4w 963gs6 8h58jb 4ck5yb 6vqzgm bd6d3r gfhs6m hsmgmm 3yqx22 k8vpyw
          cm262m 53d9db 9bh8bm jjgth6 dgv22m 766pkg bn9v36 jdy6xr 82pgpw 3xvx8w
          fz5sx6 7796xr f6z8wm 2hx5sr 4kghx6 dy9hfg 67bzw2 8x6k7r 6v5vdb 5rz22m
          fbpjh6 dpf5db jgsqgm bjg7tw 86g2s6 4qv8kg 973wnr 48hr76 7xw536 66f34b
          866nzg 6shgpw 3xxfn6 9j6q5g d5rvn6 gm5d8b czyjbm d76xzg 5qxm76 7375qg
          5y6czg 2qns22 hqs5sr 7rystw f9g36m cbp9w2 k4f322 hrqvgm h94m4w 78rz7r
          kbnfdb 4rttnr 823gvg 4w43pb jzx29g 84v3rm 3srnpb c38dtb 8xpynr 4p5h4b
          75bv36 4ynz36 hm8g4w 3kbhx6 39bspb 22q43r 68q34b 8csbcr 94rbmm jhrrg2
          f5kfjw bq5xx6 47bn22 jwqgjb 2h9cx6 h9qy8b dwkjtb fhck7r 9rvv7r 643qjw
          g2h3pb dbhcpb fn72jb 3jnp62 4stvgm 3mpwpw cw3npb 3gsmmm 2m49sr cp5zjw
          5vh29g cc77tw 57vnrm 9sjnrm bz3mmm cbkjdw jtmrxr 38jfqg dzg6s6 9w5cfg
          4rjv7r bf63hr fxkx8w jjvxhr fy955g jhxxrm 9f7qsr 5dd4yw dj7tr2 4sdkn6
          4xyy3r bsjdbm kkhpkg 4kv4r2 3h6dnr csh64w bvvbxr jrswjb 434j62 h37cfg
          9tqrmm 2t8x22 5ngntw 88n9db 7hsf36 h5z9qg jw7mxr dqbjdw 27mgs6 6yschr
          9tk97r 5rxd3r k4tqn6 3pf8r2 cx8jdw jwxg76 72wc4b 5mv76m h5z536 d7n4h6
          k42ws6 5zx7tw gh4vyb dwx4tb dbnjtb 737cc6 62748b 9bfh6m b9hshr 7gdmcr
          93y8yw 539fb2 8g8kqg 56cg2m 9rszyb 8qgt3r 7z3ptb 5wbqqg d39ywm 44vh4b
          349dh6 64yqn6 jm6n6m fnf2pw jtjvgm 44nhx6 fbg8yw gzc9b2 gczqqg g2wwjb
          6kmjr2 kjsvsr 2npwjb 75hx22 g3k62m 79sbpw 5m2bvg 7q78tb 4w3jdw 2y7qw2
          5s5wxr gm6d8b 48dhfg 8vthx6 7tvqgm j9j8pb 2v2dwm 3zfytb 5f222m 69n74b
          638jr2 34prs6 jb3yyw 4qjvjw 2jtyyw 2wzs22 66wq36 8wr2b2 599phr hqcyhr
          chbx22 8h4gg2 8hkqyb 5b6kw2 4v7qsr 6vyfb2 hpc9db 296gs6 jjnc4b dn2722
          6m3cvg hfmkw2 8kg9tb f5qrsr 83d2vg bxvwjb gg8pbm 6w3pkg 9xt55g 5h9ws6
          bc67tw gypgpw 2bqmpw 58pnx6 9kb86m 78q936 9c3zyb 5m22vg jb57zg 9tp36m
          5kfmmm 3krmxr 3vsvyb 4sr462 28d4tb hhgv7r 47df36 7kt5b2 dvn9qg dzdzgm
          67y5yb 72c48b cvjrxr gj4xrm 4pjtwm kmjrmm gnzw9g 7538dw 9zhqw2 c26t3r
          fq4zgm g7mmjb hv9bxr 9gc862 2jb322 9gngjb gsmfb2 hhhtyw bzhxtw 34qt62
          9j2mmm jj7s6m 5vcsx6 dwkksr cz42cr k983fg gb86g2 4npxpb b2dk7r frqsc6
          8d6cpb jvd74b jh5x8w h5rfyb fjgctw kjvyc6 6558h6 6t73cr khf3hr 6h84bm
          dhrv7r f7zdnr cpq6vg bzr38w 73skyb 8689b2 fk8r9g 72zjwm g9r67r cdvwcr
          by6mxr dwc7m2 fg536m 7h3322 5fdxzg f86936 8sdk7r jy8t3r h474wm d9b69g
          25fz36 c7kqqg jj9nfg fxhd3r fgq4kg hvjy62 8t6y62 3d6nxr k3phvg 747qnr
          k7k8nr 5d59r2 5nd462 bhxzdb ddznrm 4bm9n6 6dh6vg jxb4m2 56dspw 5kzg4w
          bpfzyw 9sfqw2 jhpq3r gqbspb 7hf6sr b3bn6m fzwhx6 gyky8b 678xc6 7wfy8b
          fm8jtw cqcmxr crp5yb cfxcfg jwcstw gv2jh6 86rtdw 9yk6xr c2tvw2 cxr7x6
          4kg88b 8hm6mm 6sgw5g 928dyw 99bcs6 3pr2vg gvt2cr 33kdkg 3dwnc6 j8xtx6
          gqckr2 5hjtpb 56m3zg 6vqmjw c3bhcr 85v2g2 6528wm 58dmpw d5688b 9p562m
          9sjqdb 3rdc22 7xht62 h4g4bm gjscfg gqjt22 934kqg ddcrpw hdr9qg jkqdc6
          8h2kn6 dg348b 5vzqyb hcwtwm kgn43r 5qjvw2 5ggrxr bxzyzg g6wfb2 3nj4dw
          4nfh6m gts4pb b664tb d8zwdb 8sy936 gvjqr2 3289yw 6q6462 36nkb2 7cjsrm
          d2wtwm frh8m2 jwsc6m btshhr h89q5g cqc3cr 49bntw 9n6pkg cbkbs6 8c379g
          d28xc6 5ztvyw 2dyg9g 8zr7x6 gxrq8b gccspb 66f3m2 6nqqjw dtmnc6 72twyb
          2h4xzg fqq276 hpg7c6 dbn9b2 44nzqg c5p7cr 7tmhrm b8mgg2 8gmgqg 23sx76
          kfjp4b btspc6 cd3bpw 5rzm9g hxj74b 45xtbm jxn3cr 9bnppb 8h2g4w fkr276
          b28zkg 3v7g76 k9zxrm cfhncr 8r9ywm 6pftrm 24t3hr c4hdwm cgk4r2 4kybn6
          c6t462 h8bntw b6r6mm 56f722 f9jj6m 9chkdw 5mbg36 jpb39g 4qzs4w 4wym5g
          j3xcrm ckwtkg 7b5dtb 5nrczg c529tb gr8y3r h7fnvg cbckdw 458vdb 7nfq36
          kg8n22 4p42gm 7v9c2m 65874b ccjdx6 7tjfqg hv8dbm 42ps8w 7qdxhr gtnxrm
          hssjnr j6jbg2 2vcppb 2mkdpb 7xkj8w jzmvw2 h7sqw2 8fyxfg 5dk84b cvvy22
          c7srcr 8v83pb 683j4b 2fg48w dmtdyw bfms8w jyqsc6 3nzd22 66j822 4mnptb
          5thfjw kb8ptb 5c9s8w dwmj22 gvyc4b kh2gdb 8fbywm 76m8r2 5y54hr 9m5g2m
          5gwx4w kdj4nr 7y2rmm 37rtwm bpgdwm ffrsc6 c4678w 2jng76 htyrqg f4kq62
          d73zw2 cwpp8b bw5r5g 9sq6vg 97ws76 4tzcpw fp5crm fpstdw jfr8kg fdj4rm
          cvq9sr gd8bn6 3yn7fg hc7fjw 2qryfg 4sc422 8fjzqg 2nn8m2 gcmf3r bzk93r
          3n42jb 5rvbqg hmy3s6 883kr2 b39rmm 9b7rb2 2ww8tw 2xj6db g5zphr dykrgm
          2w2d4b 8y8m9g 62gsjb 2fw2n6 9vv4bm hm5ptw j7zrgm 8rtc4b c5qq62 4m49yw
          dmc5nr 73bbxr 8v8d62 cw6wsr jhb8bm gytk62 6tmczg cnhybm kgth76 995w76
          76gf3r jcdnpb k4bfdw c4634b c39wmm hgj6qg crscvg 7qvt4b gb5gvg 9xdynr
          c8jtc6 8rh2jb g4t78w 463g5g gmb9jw 24f722 9njym2 h22wgm 8625kg f8mxtw
          b53bw2 jgmv62 2cq78w 952bcr h44bn6 j77cjb 8xqphr 7zgn6m 2hmxx6 hz2776
          dhfwg2 3dnd6m bq69db 66crn6 fcx38w 5w6myb 3xnpdw 3vp6g2 jqhdtw 5qb2n6
          jzyrdb h46mb2 bxd3g2 c4crjb 9ffb36 bz332m cw3x76 dsm9qg k5pg7r 8yfnx6
          53ny6m 89fphr 47qqtb brfyc6 d3r5kg 5jmpm2 b9h43r cfjybm 5bgdbm c64zsr
          4xb4yw 5qcjfg 3h4rqg 6nqnjb 6wwg36 jpszh6 g7bcpb gttmgm bmtg7r 3g3gcr
          4h872m 32k7s6 84mq5g 4zvx2m 247vkg 7zfs4w b652xr bygsfg f4hkn6 73r9h6
          2n4wpw 668k7r cs7b7r gjhrb2 kc6z3r j5c5qg f7h5qg 2j4j8b f3kc2m cjd8c6
          c9vhhr g229w2 ghb8kg 8276vg ftpg7r k4y4nr 7swjc6 b85qqg 7m6t6m 6yw74b
          86wf7r 5cnvkg 3nqr76 9zhwmm h43mqg 6z55h6 jm9v3r 8w5r76 7yy55g cdz7zg
          j3cw7r h33f7r f5t2gm 24nsxr 5rjcxr 5ddrqg k9h7mm 8s24nr f5fkwm 5rrc4w
          8cqvr2 8tr4yw 6cq27r dyfb4w kgr8hr 98fd62 28m2jb 7jqvgm 5nnryb 5srhfg
          8dmm2m 66pqdw fkbgdb h83dkg cjtxmm frrqkg bf425g 4xtgw2 k3z8tb k2p5r2
          3frqwm 3qx4pb jf332m g4p5sr 8x9r5g jdmttw khcf62 74pxhr jw9rgm dzvkkg
          h8b4nr 2b434w hxkqw2 cqf3rm 2vp3pb cp4r36 65zphr 7msrjb 7j4m5g 8rjwb2
          5rr5b2 4vfb36 6syc9g 4ksv8b hsvdzg jznsmm hspxmm fb9fbm cy658b c9k6gm
          ddrf3r 7fbyrm 7dt2sr j2jqdw 7rg7vg f9z6gm jpnj4b dgtn4w 228n4w 3j4dx6
          5p6px6 6w7rn6 cvnrjb c2n4zg 6ptvdb 5wzrsr jpygw2 9zzjm2 fmvj62 bfhyhr
          cth5bm 6fyfkg 3xd3s6 cxjxmm hg5jbm 9kn7xr 28nrn6 3pr962 jjvx2m 3cck3r
          dr5562 73v93r 46hykg jrp8c6 bpqgyb d2g462 dnhz62 494wyb 2g7kgm 85g48w
          8wb3xr cyk5w2 dyzptw 2xf6sr bk3ytw 278kdb ccvjbm 5q3vn6 6w2xs6 c7fgn6
          6366w2 k3dtkg d6qr5g 2nhbs6 hk2c4w 2xn5yw 8y453r bfg8wm cnn43r 4mxkdw
          4p2jpb gmcz62 3j7ryb 2bghpw gb946m 62j9bm k4v6jb 5gyyh6 9dv8rm 5fyf8b
          fnhp22 5zkdzg 9ycth6 6ymkyw 8m2d4b f6qg2m 82tktb 499kgm cxx8bm 7m93x6
          5jz562 b458fg kj7ztb 9yb5kg fyk8h6 c6pkh6 d2s98b 7knw7r hrjw2m hwzgyb
          34sy6m 29tm5g jjmg36 c6t55g 9pqs4w gtxcrm 28sg5g cn2w5g cbshs6 fqkw36
          4zvz36 3n9ypb 28rr7r czs9kg c3syhr j5fxfg jj22b2 b5sj4b 64pk7r 5fn422
          h7t4fg 4ttwb2 d6tqr2 fbvp8b 4sxhs6 2wcbw2 g4n88w f5q2w2 39q9dw 3czgn6
          fqcww2 hkzcm2 8hpq3r 93qn76 8n5ccr ghxcm2 gv5k8b 6zmknr 6rwhcr ftwfdw
          66wpm2 45kb4w 399c2m hcbmyb 3r2w76 6m8n4w 8ysjfg j2pgw2 gxp44b h86j22
          k5jrqg gqpwyb 3nwdfg b7t8c6 dhqznr fmq93r 4qbr7r 2zmdfg ch2yh6 7nh7c6
          2hqcrm fptyx6 88fcfg 6ky2sr k5rqwm bjp5n6 3pv8rm dgtr9g 2h7qyw 73m6jw
          hy9f7r hkhs9g 823ppb 5s39wm 59f65g 4ft8zg 848wb2 4zjtdw 9bqjx6 jp7kr2
          7r29h6 f55c2m drw2b2 92wb2m k7sg5g k7h88w 8pgp8w 2rh7tw dk99r2 3nsdtw
          jfznjb f5pdnr fkzvr2 25g6w2 5pw48w 2tmg36 dhf2w2 kcdy8w 54qt4b 4htmsr
          fq8x9g 677ncr h2ssmm 9rzzbm dsk34b 8dkrgm j8tqgm 5fkm7r gpkwqg 73wvnr
          7cx2qg jkmfr2 cyzzyw jsf5h6 5d2b5g h6zqh6 759y22 dmb7x6 j9mwsr ctdvnr
          6t7t8w 5bh236 3f4tc6 fpsh4w 6ytk62 4c6rjw f22fdw 22y2gm 9z4636 gx85r2
          hxbfr2 5rncjb 8q6n6m 48r3xr fdkyzg jgwb5g 7yfdzg 7t82pw 89yq8b k78j6m
          fcw776 34dkw2 88y8c6 ccbfr2 3k3npw 3cn236 jzdbn6 6c7m36 5h7p3r k9m776
          jqk93r jgvddw 37sjrm 99n5qg 5t7236 dt8tzg hbfth6 f4bbyb 52yjtw j9g2yb
          2x8qdw jdjn4w b79f62 9nrxs6 2sgqwm 85yns6 4xhj4b 5wn5db 8f667r d46svg
          67d4rm 4hmnmm 2tq376 d7cnpw 7hvq7r 2yq4pb 8h9w36 3yjsfg kd9f36 h9qv3r
          gy42gm 5hxd22 j8h6n6 jny4rm gdb6w2 9ckfnr jn7m36 7z93x6 8d965g g323xr
          4xpcxr 6zqk8b dtj462 h93fh6 85jqdw h29kkg 96g58b 68z93r crpybm 5rxjfg
          5kswjw kdc2qg 4nmq3r 2kjvqg jbxv8b 5s5hmm fv2wsr k968tw 5pjynr fqx79g
          2p965g fr3b7r c7wqyb 7ght8w 9b4rqg 28vqtb cq3mdb 42khpb k678bm 98s8yw
          64rrxr jqtcmm j24fkg 4brt6m 564msr 7snftb 7cj7pw bwzmw2 9xvtrm b39r5g
          gsnryb crkhg2 976h4b fns4zg 32xqbm 8tppx6 2gsksr 5z5gn6 99vp6m 7xqznr
          68kbqg f9fjm2 7zyq3r 67nkr2 62v84b fvd822 cx7ncr 9k8scr j5kss6 8srkw2
          bv9vdw g5y6qg hy4tpb 7ddgw2 gsb2b2 25584b h3vnjb hjq7hr dpdqyb 9fyxxr
          d9s9r2 kkqy8b 5mcxvg 7sjxtw hj7y6m 695rgm bfdyx6 bk6yfg jx2nmm kd7c76
          j4cggm 6bn6b2 3mtjhr 22cmqg cyk9r2 7n8p4b crqw36 h4dkh6 3tpr36 dys3x6
          7t57g2 9svw36 g2nncr hs8z8b 6sndhr 7g2d4b 74vm7r hgqhpb dsjns6 hdtf8b
          hr5gpw bmyxmm dfb2yb 3jcvwm 7h2r7r bj24x6 fgxbmm f45rsr bsgbgm 6yh7xr
          k9q6sr cyfm7r bfsfh6 kd8tzg 6z3npw 4mysjb 3qpcmm 2pt44b cm5jnr 3hghcr
          927tx6 f67ytw cbn562 4x53jb hdywn6 3c8kh6 5r4fkg bdp3s6 k6tktb dknrb2
          4f4h76 d434c6 gjqqh6 2cvbdb 4z8scr 2wdtpb 4tx422 56bprm 3dsnrm 5hdc9g
          dz3k3r 7qwj22 cq4c9g 3dvccr 3r53vg 7ngn9g bbpy4b kjfcvg 3h9zyw 2ztcmm
          jrgcxr km22yb 84fcpw 9r37s6 fng8zg jrxqbm cr8fwm kcqx2m 2vqfbm gzwf3r
          9xjvyw ckg72m jcrmn6 bdk46m 59v4hr bg2wgm 5y9ppb d84jtw dq5gyb 3x2f3r
          9xhd6m 6v29yw grndtw 5bd58b jmgbgm ckp3g2 c4bwgm cg5jx6 64xg5g hb5w7r
          2px3pw 359dc6 655ss6 gqxzh6 933qnr cjh7xr bs7ztb gtxm36 jbzc2m f5d44b
          9c9m7r dn42sr 9yg8hr 5ktzwm 86g3xr 6rshs6 g6q3vg dpkv3r 74vznr dkk562
          9tms76 k6xkdw 6zpv62 5qkhmm jrv46m kctwn6 4vv8fg 649crm 8mdg7r jm3vwm
          gwjcxr bddmsr gt8k62 hk8bb2 dh444b bv5f36 hxt8c6 3gtktb 5vxww2 b74p62
          k8cvwm ch4f8b j5kgb2 9h5vgm 8z6qwm 585qh6 dbmq8b f38d8w 9dpdx6 dmdzwm
          6k8ng2 4fgxs6 fmft4b 6rq93r 7m5bdb 67ccvg cxpq3r c7bh2m ht7vkg hqgz3r
          k9x5yw d3n5yw bs9562 76qw36 gw2xs6 jc5tfg 28wgqg 9fjp4b f253m2 6nnt8w
          5xnpc6 j2ghpw 7ccsc6 jqqd22 f6qdzg cfmmyb 86k3jb 3bxmqg h696sr f8r58b
          65yj6m 3rvyzg 8n2cs6 7wxmqg j44scr dh8kh6 gnfv3r 3mmkbm cnh6yb 5s993r
          6tyqdw 5k4tpb 87hxvg hsfgn6 kjj8pb 4m4t22 4qjbyb 25qnvg 6k88zg 899kkg
          drsms6 bxjjtw bbjcxr ct965g kkfznr 8bmj22 kcmxg2 f2sbjw 3xb8hr dw248w
          k53pzg 4j8k62 d7bjc6 d4rzkg bbtbsr dcr2yb jq7xvg k8nbw2 jcgs76 j2ty8w
          bdkgn6 4zpqr2 fpcjrm b8crcr 7s7hg2 drtjhr dm88tw 5wswdb 6xbrw2 7csppb
          2mcfh6 by6k62 bjxhg2 b7x7xr 5mrdfg 79zb36 gmrjhr bnh5nr bf8hjb gvkmdb
          45rhcr jt3n4b brrz8b fjvdpb hyrr7r 2mzc2m j738m2 fbytpb 3yzzyw 6zz27r
          c828tw 6szv3r jw2nxr 45vgyb 7xwvh6 cdrr7r 5ss3pw gq4h76 7k8bb2 39fx76
          5h88c6 hx92qg f2mv62 kbyzwm 645376 fgt9yw 58cdzg 72zdc6 5w8vnr 54k5w2
          d75phr fxvzyw 5fbv62 32zkkg 6mwjpb 2w5cpw ft2jpb 344nmm fxxw7r 9fct8w
          6fmrw2 cvr8c6 8q7xvg jjt2sr 4ng4x6 k66s2m 3c2mb2 7nr48w dqvp6m 3yshhr
          8cscjb jz3cvg jnsncr fxv84b 4ppjzg 3swqkg 8mjxmm f2cd6m g6d53r jgwdx6
          bphmjw gsvtx6 5zpdm2 6ywzgm hbbn9g 2s8rqg 9y75yw 4wjns6 7wp5r2 cpfptw
          4vwr7r 838j6m k96cmm 67yjhr 3p4cmm 6rkjm2 3n8gw2 6jtbn6 4stzdw 7p62n6
          67tfkg 3tgrw2 6qhppb 3d4g7r dfyv62 8whc76 gw7hxr 7pnv8b 8cv7s6 7n2px6
          gn9cpw 2z4y8w 28myrm 8yv27r fz5376 btywn6 27pwb2 g4cdtw 945pc6 4jtyx6
          ddkxpw dr3ncr 26r4rm b7b2b2 9qt2gm 9s7yhr 3gg58b ctq4hr 3sqyhr dw3yhr
          7mc9kg 7pwxs6 hgfkkg dzhhg2 fdb5kg k8rbgm gpv27r 69kbdb 9mjmgm 7ww4x6
          hnyfwm h2ws76 23qb36 4sg6db 5wws76 h6x4c6 bv3w7r 5hqb36 dv3bqg 4d8yx6
          89bpx6 53gcg2 569gn6 2y34c6 ghjvyw 38d39g j56r36 fsg4x6 5g7hs6 f6hftb
          d7n4pb fcs7mm 9wd2gm 2nbdm2 5fsp6m j4brjw 36qptw h64fr2 9zn422 h7t776
          3z35bm gk839g 8qwyrm 65dwb2 bdxx76 3bhpc6 jg8dm2 2nrbyb 3vfqyw hssx76
          687zh6 45ykbm 9r34zg 7phcvg djrgjw hbzp6m hhvw36 3j8z3r ghz6sr ff8zwm
          3w2fdw cgpgw2 674ns6 hr45r2 h2nppb f97kdw 4kwttw 5sq7g2 7xxytw 5f2p8w
          g5f8rm k2z4hr 3w8hcr kb5562 4w7pzg 8j2s76 2zqrgm hn9dzg jdmhcr j9k2jw
          kg3m36 38gy4b b6ndtw 54znjb 9cxhs6 c53smm 429v3r 6s3c76 37v2jw b7h9kg
          h33n2m 3xr6w2 4mjrdb gmjkbm 88ctm2 893qtb 7rn3pw 8fyxmm 3z64x6 5st72m
          bp9jm2 bb5xcr g76wgm kjm4x6 48v3jb 63379g 2fhyhr d277xr bwnjrm g7ny6m
          fb6fr2 hhwzdw ghqqtb 4c8msr f5byrm gc9k3r gc9gb2 4ss2db fpnd6m 9yr3xr
          jc96gm f8rhpw 9y5p6m bs97xr 4vrn9g 956b5g 39p636 g54drm 8fjz8b 8mxrn6
          fysryb hhq27r 75yyx6 9zy2sr 52h4pb 88wrjw 8ncbjw j7r9h6 bzgnxr 332px6
          3zx236 9kt3s6 crcj8w h5wgjw fjpx9g fg4zdw 7vdfdw fg9rgm 3c8mn6 3sstrm
          fbtqr2 8hgvbm b5zm36 cb6hvg jnprjw 7b2j6m ffz79g
        )
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::SubmitFailedVisaTransactionsForSettlement.new(options)
  transition.run
end
