# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200514193423_acv_contributors_normalize_stealth.rb --verbose | tee -a /tmp/acv_contributors_normalize_stealth.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200514193423_acv_contributors_normalize_stealth.rb --verbose -w | tee -a /tmp/acv_contributors_normalize_stealth.log
#
module GitHub
  module Transitions
    class AcvContributorsNormalizeStealth < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle_with_retry` block, which should be called on the most specific
      #      `ApplicationRecord::*` class/subclass or object. This will make sure we don't
      #      overwhelm master and cause replication lag.
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator, :updated_records

      def after_initialize
        # The most common, and preferred, approach is to define your iterator
        # here using `BatchedBetween`. Prefer using a min_id with BatchedBetween
        # queries. Not doing so can lead to _very_ slow transition tests (https://github.com/github/github/pull/92565)
        min_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM acv_contributors") }
        max_id = readonly { ApplicationRecord::Collab.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM acv_contributors") }

        @iterator = ApplicationRecord::Collab.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          -- id must go first for BatchedBetween to work properly
          SELECT id, contributor_email FROM acv_contributors
          WHERE id BETWEEN :start AND :last
            AND contributor_email LIKE '_%+_%@users.noreply.github.com'
        SQL

        @potentially_updated_records = 0
        @updated_records = 0

        # The main RegExp for matching and fixing modern stealth user emails
        @modern_stealth_email_matcher = %r{
          # Start of string anchor
          \A
          # `myUserId+myUserName` style local part of an email address
          (?<user_id>\d+)\+[^@]+
          # Negative lookbehind assertion: ...but not bots!
          (?<!\[bot\])
          # @ the GitHub stealth user email domain part of an email address
          @users\.noreply\.github\.com
          # End of string anchor
          \z
        }xi
      end

      # Returns nothing.
      def perform
        # A common approach is to only use the perform method to iterate through
        # your iterator, passing off the actual work to another method. Notice
        # that the actual iteration happens inside the `Readonly` model so
        # that we make sure we're hitting the read-only replicas and we can avoid
        # spiking replication lag.
        GitHub::SQL::Readonly.new(@iterator.batches).each do |rows|
          process(rows)
        end

        log "Would have updated #{@potentially_updated_records} total records if not dry-running" if verbose? && dry_run?
        log "Updated #{@updated_records} total records" if verbose?
      end

      def process(rows)
        # Use the throttler to ensure that this transition doesn't cause
        # unnecessary replication delay
        AcvContributor.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Updating #{rows.count} rows with IDs: #{rows.map { |row| row[0] }.join(", ")}" if verbose?
          run_batch_update rows
        end
      end

      def run_batch_update(rows)
        return if rows.empty?

        ActiveRecord::Base.connected_to(role: :writing) do
          # Must use `UPDATE IGNORE` to avoid duplication issues when a
          # contributor has contributions both with and without the
          # normalization of their email for the same repository
          sql = ApplicationRecord::Collab.github_sql.new "UPDATE IGNORE acv_contributors SET contributor_email = CASE"

          changeable_email_count = 0

          rows.each do |row|
            id = row[0]
            original_contributor_email = row[1]

            # Normalize the modern stealth email format to ignore user login
            # myUserId+myUserName@users.noreply.github.com (123+monalisa@...)
            # => myUserId+username@users.noreply.github.com (123+username@...)
            contributor_email = original_contributor_email.sub(@modern_stealth_email_matcher, '\k<user_id>+username@users.noreply.github.com')

            # Log if the email address did not change
            if contributor_email == original_contributor_email
              log "Email address unexpectedly not updated for ID #{id}: \"#{original_contributor_email}\"" if verbose?
            else
              sql.add <<-SQL, id: id, contributor_email: contributor_email
                WHEN id = :id THEN :contributor_email
              SQL

              changeable_email_count += 1
            end
          end

          # HACK: This impossible `WHEN` option is added just in case no others
          # were added by the row iteration block above. Without it, unit tests
          # with limited data sets are failing due to syntax errors of having an
          # `ELSE` but no `WHEN`s.
          if changeable_email_count == 0
            sql.add "WHEN id = 0 THEN `contributor_email`"
          end

          # Default back to the original value if no special CASE is identified
          # WARNING: If this `ELSE` is not present and a none of the `WHEN`
          # options match, the value will be set to `NULL`!
          sql.add "ELSE `contributor_email`"
          sql.add "END"
          sql.add "WHERE id IN :ids", ids: rows.map { |row| row[0] }

          # Execute the query
          if dry_run?
            log "Would have updated #{changeable_email_count} rows if not dry-running" if verbose?
            @potentially_updated_records += changeable_email_count
          else
            sql.run
            log "Updated #{sql.affected_rows} rows" if verbose?
            @updated_records += sql.affected_rows
          end
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AcvContributorsNormalizeStealth.new(options)
  transition.run
end
