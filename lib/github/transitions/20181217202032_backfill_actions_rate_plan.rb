# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20181217202032_backfill_actions_rate_plan.rb -v | tee -a /tmp/backfill_actions_rate_plan.log
#
module GitHub
  module Transitions
    class BackfillActionsRatePlan < Transition
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM plan_subscriptions") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM plan_subscriptions") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id FROM plan_subscriptions
          WHERE id BETWEEN :start AND :last
            AND zuora_subscription_id IS NOT NULL
        SQL

        @total_subscriptions_updated = 0
        @counter = 0
        GitHub.zuorest_client.timeout = 30
        GitHub.zuorest_client.open_timeout = 30
      end

      # Returns nothing.
      def perform
        if single_user_login.present?
          user = User.find_by!(login: single_user_login)
          process([user.plan_subscription.id])
        else
          GitHub::SQL::Readonly.new(iterator.batches).each do |batch|
            batch.each do |subscription_ids|
              process(subscription_ids.flatten)
            end
          end
        end
      end

      def process(subscription_ids)
        plan_subscriptions = readonly { ::Billing::PlanSubscription.includes(:user).where(id: subscription_ids).to_a }
        plan_subscriptions.each do |subscription|
          @counter += 1
          $stderr.printf("\r Current plan subscription #{@counter}")
          user = subscription.user
          next if user.invoiced?

          zuora_subscription = subscription.zuora_subscription
          next unless zuora_subscription.present?

          zuora_params = subscription.zuora_params
          rate_plans = []
          contract_effective_date = GitHub::Billing.today.to_s

          active_rate_plans = zuora_subscription.active_rate_plans

          if !existing_actions_rate_plan(active_rate_plans)
            actions_rate_plan = zuora_params.github_actions_rate_plan
            if actions_rate_plan.present?
              actions_rate_plan[:contractEffectiveDate] = contract_effective_date

              rate_plans << actions_rate_plan
            end
          end

          if !existing_packages_rate_plan(active_rate_plans)
            packages_rate_plan = zuora_params.github_package_registry_rate_plan
            if packages_rate_plan
              packages_rate_plan[:contractEffectiveDate] = contract_effective_date

              rate_plans << packages_rate_plan
            end
          end

          if !existing_shared_storage_rate_plan(active_rate_plans)
            shared_storage_rate_plan = zuora_params.github_shared_storage_rate_plan
            if shared_storage_rate_plan
              shared_storage_rate_plan[:contractEffectiveDate] = contract_effective_date

              rate_plans << shared_storage_rate_plan
            end
          end

          next if rate_plans.empty?

          params = {
            runBilling: false,
            add: rate_plans,
          }

          if dry_run?
            log "[dry_run] would update user #{subscription.user.email}"
            update_count
          else
            response = GitHub.zuorest_client.update_subscription \
              subscription.zuora_subscription_number,
              params, ::Billing::PlanSubscription::ZuoraSynchronizer::ZUORA_VERSION_HEADER

              result = GitHub::Billing::Result.from_zuora(response)

              if result.success?
                readonly { subscription.reload }
                subscription.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) { subscription.update_from_zuora_subscription }
                update_count
              else
                log "Failed to update user `#{subscription.user.email}` with error: `#{result.error_message}`"
              end
          end
        end
      end

      def update_count
        @total_subscriptions_updated += 1

        if verbose?
          log "Updated #{@total_subscriptions_updated} subscriptions"
        end
      end

      def existing_actions_rate_plan(active_rate_plans)
        active_rate_plans.any? do |rate_plan|
          ::Billing::Actions::ZuoraProduct.rate_plan_charges.any? do |rate_plan_charge|
            rate_plan[:productRatePlanId] == rate_plan_charge.zuora_id
          end
        end
      end

      def existing_packages_rate_plan(active_rate_plans)
        product_rate_plan_id = ::Billing::PackageRegistry::ZuoraProduct.uuid.zuora_product_rate_plan_id
        active_rate_plans.any? do |rate_plan|
          rate_plan[:productRatePlanId] == product_rate_plan_id
        end
      end

      def existing_shared_storage_rate_plan(active_rate_plans)
        product_rate_plan_id = ::Billing::SharedStorage::ZuoraProduct.uuid.zuora_product_rate_plan_id
        active_rate_plans.any? do |rate_plan|
          rate_plan[:productRatePlanId] == product_rate_plan_id
        end
      end

      def single_user_login
        other_args[:login]
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "login=", "login", "Updates a specific user instead of all users", as: String
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillActionsRatePlan.new(options)
  transition.run
end
