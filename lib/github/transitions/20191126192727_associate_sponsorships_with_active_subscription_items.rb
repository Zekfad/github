# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191126192727_associate_sponsorships_with_active_subscription_items.rb -v | tee -a /tmp/associate_sponsorships_with_active_subscription_items.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191126192727_associate_sponsorships_with_active_subscription_items.rb -v -w | tee -a /tmp/associate_sponsorships_with_active_subscription_items.log
#
module GitHub
  module Transitions
    class AssociateSponsorshipsWithActiveSubscriptionItems < Transition
      BATCH_SIZE = 100

      attr_reader :iterator, :updated_sponsorships

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM subscription_items") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM subscription_items") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL, subscribable_type: ::Billing::SubscriptionItem.subscribable_types[SponsorsTier.name]
          SELECT id, plan_subscription_id, subscribable_id FROM subscription_items
          WHERE id BETWEEN :start AND :last
          AND quantity > 0
          AND subscribable_type = :subscribable_type
        SQL

        @updated_sponsorships = {}
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          rows.each do |subscription_item_id, plan_subscription_id, subscribable_id|
            process_subscription_item(subscription_item_id: subscription_item_id,
              plan_subscription_id: plan_subscription_id,
              subscribable_id: subscribable_id,
            )
          end

          log @updated_sponsorships if verbose?
        end
      end

      def process_subscription_item(subscription_item_id:, plan_subscription_id:, subscribable_id:)
        sponsor_id = ApplicationRecord::Domain::Users.github_sql.value <<-SQL, plan_subscription_id: plan_subscription_id
          SELECT user_id
          FROM plan_subscriptions
          WHERE id = :plan_subscription_id
        SQL

        maintainer_id = ApplicationRecord::Collab.github_sql.value <<-SQL, subscribable_id: subscribable_id
          SELECT sponsorable_id
          FROM sponsors_listings
          INNER JOIN sponsors_tiers
          ON sponsors_tiers.sponsors_listing_id = sponsors_listings.id
          AND sponsors_tiers.id = :subscribable_id
        SQL

        sponsorships_for_sponsor_and_maintainer = ApplicationRecord::Collab.github_sql.results <<-SQL, sponsor_id: sponsor_id, maintainer_id: maintainer_id
          SELECT id, subscription_item_id FROM sponsorships
          WHERE sponsor_id = :sponsor_id
          AND sponsorable_id = :maintainer_id
        SQL

        if sponsorships_for_sponsor_and_maintainer.count == 0
          log "[Billing::SubscriptionItem #{subscription_item_id}] No Sponsorship found for sponsor_id #{sponsor_id} and maintainer_id #{maintainer_id}! Skipping." if verbose?
          return
        elsif sponsorships_for_sponsor_and_maintainer.count > 1
          log "[Billing::SubscriptionItem #{subscription_item_id}] More than one Sponsorship found for sponsor_id #{sponsor_id} and maintainer_id #{maintainer_id}! Skipping." if verbose?
          return
        end

        sponsorship_id, sponsorship_subscription_item_id = sponsorships_for_sponsor_and_maintainer.first

        if subscription_item_id == sponsorship_subscription_item_id
          log "[Billing::SubscriptionItem #{subscription_item_id}] Sponsorship #{sponsorship_id} already associated with subscription item #{subscription_item_id}. Skipping." if verbose?
          return
        end

        sponsorship_subscription_item_quantity = ApplicationRecord::Domain::Users.github_sql.value <<-SQL, subscription_item_id: sponsorship_subscription_item_id
          SELECT quantity FROM subscription_items
          WHERE id = :subscription_item_id
        SQL

        if sponsorship_subscription_item_quantity > 0
          log "[Billing::SubscriptionItem #{subscription_item_id}] Sponsorship #{sponsorship_id} is associated with active subscription item #{sponsorship_subscription_item_id}. Skipping." if verbose?
          return
        end

        log "[Billing::SubscriptionItem #{subscription_item_id}] Sponsorship #{sponsorship_id} is associated with cancelled subscription item #{sponsorship_subscription_item_id}. Updating." if verbose?
        update_sponsorship(sponsorship_id: sponsorship_id, new_subscription_item_id: subscription_item_id) unless dry_run?
        @updated_sponsorships[sponsorship_id] = { old_subscription_item_id: sponsorship_subscription_item_id, new_subscription_item_id: subscription_item_id }
      end

      def update_sponsorship(sponsorship_id:, new_subscription_item_id:)
        ApplicationRecord::Collab.github_sql.run <<-SQL, sponsorship_id: sponsorship_id, new_subscription_item_id: new_subscription_item_id
          UPDATE sponsorships
          SET subscription_item_id = :new_subscription_item_id
          WHERE id = :sponsorship_id
        SQL
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AssociateSponsorshipsWithActiveSubscriptionItems.new(options)
  transition.run
end
