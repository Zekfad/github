# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "github/enterprise"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20181016223550_backfill_enterprise_global_customer.rb | tee -a /tmp/backfill_enterprise_global_customer.log
#
module GitHub
  module Transitions
    class BackfillEnterpriseGlobalCustomer < Transition

      # Returns nothing.
      def perform
        return unless GitHub.single_business_environment?

        GitHub::Enterprise.ensure_customer!
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  # This transition is intended to be run from a database migration during an
  # Enterprise upgrade or install
  transition = GitHub::Transitions::BackfillEnterpriseGlobalCustomer.new(options)
  transition.run
end
