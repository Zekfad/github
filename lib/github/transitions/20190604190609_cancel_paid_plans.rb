# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190604190609_cancel_paid_plans.rb -v -s=dependabot-preview | tee -a /tmp/cancel_paid_plans.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190604190609_cancel_paid_plans.rb -v -s=dependabot-preview -w | tee -a /tmp/cancel_paid_plans.log
#
module GitHub
  module Transitions
    class CancelPaidPlans < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        @paid_plans = integration.marketplace_listing.listing_plans.paid
        @iterator = ::Billing::SubscriptionItem.where(
          subscribable_type: ::Billing::SubscriptionItem::MARKETPLACE_SUBSCRIBABLE_TYPE,
          subscribable_id: @paid_plans.ids,
        ).active
      end

      # Returns nothing.
      def perform
        log "Cancelling #{@paid_plans.count} paid plans for #{integration.name}" if verbose?

        ActiveRecord::Base.connected_to(role: :reading) do
          log "Cancelling #{iterator.count} subscriptions" if verbose?

          iterator.in_batches(of: BATCH_SIZE) do |scope|
            process(scope.includes(plan_subscription: :user).to_a)
          end
        end
      end

      def process(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          rows.each do |item|
            item.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
              log "Cancelling paid plan #{item.subscribable.name} for #{item.user.login}" if verbose?
              item.cancel!(actor: item.user, force: true) unless dry_run?
            end
          end
        end
      end

      private
      def integration
        @integration ||= Integration.find_by!(slug: other_args[:integration_slug])
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "s=", "integration_slug", "Integration slug to cancel the paid plans"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CancelPaidPlans.new(options)
  transition.run
end
