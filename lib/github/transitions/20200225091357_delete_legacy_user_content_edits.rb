# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200225091357_delete_legacy_user_content_edits.rb -v | tee -a /tmp/delete_legacy_user_content_edits.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200225091357_delete_legacy_user_content_edits.rb -v -w | tee -a /tmp/delete_legacy_user_content_edits.log
#
module GitHub
  module Transitions
    class DeleteLegacyUserContentEdits < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      def after_initialize
        @min_id = @other_args[:start] || readonly { ::UserContentEdit.minimum(:id) } || 0
        @max_id = readonly { ::UserContentEdit.maximum(:id) } || 0

        @count = 0

        @relation = ::UserContentEdit.where(user_content_type: [
          "CommitComment",
          "Issue",
          "IssueComment",
          "PullRequestReview",
          "PullRequestReviewComment",
          "RepositoryAdvisory",
          "RepositoryAdvisoryComment"
        ])
      end

      def batch_size
        @other_args[:"batch-size"] || BATCH_SIZE
      end

      # Returns nothing.
      def perform
        start_id = @min_id
        batch_count = 0

        loop do
          break if start_id > @max_id

          if verbose && batch_count % 50 == 0
            log "Reached UserContentEdit##{start_id}, Rows processed: #{@count}"
          end

          process(start: start_id, finish: (start_id + batch_size - 1))

          batch_count += 1
          start_id += batch_size
        end

        if verbose
          log "Done, Rows processed: #{@count}"
        end
      end

      def process(start:, finish:)
        batch = @relation.where("id BETWEEN :start AND :finish", start: start, finish: finish)

        @count += if dry_run?
          readonly { batch.count }
        else
          UserContentEdit.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            batch.delete_all
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "start=", "ID to start from", as: Integer
    on "batch-size=", "Batch size", default: GitHub::Transitions::DeleteLegacyUserContentEdits::BATCH_SIZE, as: Integer
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DeleteLegacyUserContentEdits.new(options)
  transition.run
end
