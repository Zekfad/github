# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200213225825_backfill_webhook_status.rb --table={zuora_webhooks,stripe_webhooks} -v | tee -a /tmp/backfill_webhook_status.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200213225825_backfill_webhook_status.rb --table={zuora_webhooks,stripe_webhooks} -v -w | tee -a /tmp/backfill_webhook_status.log
#
module GitHub
  module Transitions
    class BackfillWebhookStatus < Transition
      BATCH_SIZE = 100

      attr_reader :iterator, :scope, :table_name

      def after_initialize
        @table_name = other_args[:table]
        @scope = \
          case table_name
          when "zuora_webhooks" then ::Billing::ZuoraWebhook
          when "stripe_webhooks" then ::Billing::StripeWebhook
          else raise "Invalid table name: #{table_name}"
          end

        min_id = readonly { scope.minimum(:id) } || 0
        max_id = readonly { scope.maximum(:id) } || 0

        @iterator = scope.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add(<<~SQL)
          SELECT id, processed_at FROM #{table_name}
          WHERE id BETWEEN :start AND :last
            AND status IS NULL
        SQL

        @total_records = scope.where(id: min_id..max_id).count
        @processed_records = 0
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
          puts "Updated #{@processed_records} / #{@total_records} (dry run: #{dry_run?})"
        end
      end

      def process(rows)
        ids = rows.each_with_object({ processed: [], ignored: [] }) do |(id, processed_at), memo|
          if processed_at.nil?
            memo[:ignored] << id
          else
            memo[:processed] << id
          end
        end

        if dry_run?
          @processed_records += ids[:processed].length
          @processed_records += ids[:ignored].length
        else
          scope.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            @processed_records += scope.where(id: ids[:processed]).update_all(status: :processed)
            @processed_records += scope.where(id: ids[:ignored]).update_all(status: :ignored)
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "t", "table=", "Table to update (stripe_webhooks,zuora_webhooks)"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillWebhookStatus.new(options)
  transition.run
end
