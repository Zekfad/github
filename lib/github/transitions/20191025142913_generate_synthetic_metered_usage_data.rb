# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191025142913_generate_synthetic_metered_usage_data.rb -v | tee -a /tmp/generate_synthetic_metered_usage_data.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191025142913_generate_synthetic_metered_usage_data.rb -v -w | tee -a /tmp/generate_synthetic_metered_usage_data.log
#
module GitHub
  module Transitions
    class GenerateSyntheticMeteredUsageData < Transition
      REPOSITORY_NAME = "synthentic-user-repository"
      PACKAGE_NAME="synthetic-usage-package"
      PACKAGE_VERSION="1.0.0"
      INTEGER_MAX_VALUE = 2 ** 32 / 2 - 1 # Assumes 4-byte signed integer

      def after_initialize
        @owner = User.find_by!(login: other_args[:owner])
      end

      def perform
        return if dry_run?

        ApplicationRecord::Collab.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          generate_actions_usage
          generate_registry_usage
          generate_storage_usage
        end
      end

      private

      attr_reader :owner

      def generate_actions_usage
        generate_actions_usage_line_item(
          runtime: "UBUNTU",
          duration_in_milliseconds: owner.plan.actions_included_private_minutes.minutes.in_milliseconds,
        )
        generate_actions_usage_line_item(
          runtime: "WINDOWS",
          duration_in_milliseconds: 5.minutes.in_milliseconds,
        )
      end

      def generate_actions_usage_line_item(runtime:, duration_in_milliseconds:)
        # To avoid integer overflows for large durations we have to split them up
        durations = [INTEGER_MAX_VALUE] * (duration_in_milliseconds / INTEGER_MAX_VALUE) + [duration_in_milliseconds % INTEGER_MAX_VALUE]

        if verbose?
          log "Creating #{duration_in_milliseconds / 1.minute} of private #{runtime} actions usage..."
        end

        durations.each do |duration|
          ::Billing::ActionsUsageLineItem.create!(
            job_id: SecureRandom.hex(18),
            owner: owner,
            actor: owner,
            repository: repository,
            job_runtime_environment: runtime,
            duration_multiplier: ::Billing::ActionsUsageLineItem.billable_rate_for(runtime: runtime),
            duration_in_milliseconds: duration,
            start_time: (duration / 1000).seconds.ago,
            end_time: Time.current,
            submission_state: :unsubmitted,
            **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(owner),
          )
        end
      end

      def generate_registry_usage
        generate_registry_usage_line_item(size_in_bytes: owner.plan.package_registry_included_bandwidth.gigabytes)
        generate_registry_usage_line_item(size_in_bytes: 1100.gigabytes)
      end

      def generate_registry_usage_line_item(size_in_bytes:)
        if verbose?
          log "Creating #{size_in_bytes / 1.megabyte}MB of private data transfers..."
        end

        ::Billing::PackageRegistry::DataTransferLineItem.create!(
          owner: owner,
          actor: owner,
          registry_package: package,
          registry_package_version: package_version,
          size_in_bytes: size_in_bytes,
          download_id: SecureRandom.hex,
          downloaded_at: Time.current,
          submission_state: :unsubmitted,
          **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(owner),
        )
      end

      def generate_storage_usage
        generate_storage_usage_aggregations(aggregate_size_in_bytes: owner.plan.shared_storage_included_megabytes.megabytes)
        generate_storage_usage_aggregations(aggregate_size_in_bytes: 1800.gigabytes)
      end

      def generate_storage_usage_aggregations(aggregate_size_in_bytes:)
        if verbose?
          log "Creating #{aggregate_size_in_bytes / 1.megabyte}MB of private storage usage..."
        end

        start_on = owner.first_day_in_metered_cycle
        snapshot_timestamp = GitHub::Billing.date_in_timezone(start_on)

        previous_aggregation = ::Billing::SharedStorage::ArtifactAggregation.create!(
          submission_state: :unsubmitted,
          owner: owner,
          repository: repository,
          aggregate_effective_at: snapshot_timestamp,
          aggregate_size_in_bytes: aggregate_size_in_bytes,
          **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(owner),
        )

        base_attributes = previous_aggregation.attributes.except("id", "aggregate_effective_at", "created_at", "updated_at", "previous_aggregation_id").symbolize_keys

        while previous_aggregation.aggregate_effective_at < 1.hour.ago
          previous_aggregation = ::Billing::SharedStorage::ArtifactAggregation.create!(
            submission_state: :unsubmitted,
            aggregate_effective_at: previous_aggregation.aggregate_effective_at + 1.hour,
            previous_aggregation: previous_aggregation,
            **base_attributes,
          )
        end
      end

      def repository
        @repository ||= owner.repositories.find_by(name: REPOSITORY_NAME) || create_repository
      end

      def create_repository
        log "Creating repository..." if verbose?

        Repository.create!(
          name: REPOSITORY_NAME,
          owner: owner,
          created_by_user_id: owner,
        )
      end

      def package
        @package ||= Registry::Package.find_by(owner: owner, repository: repository, name: PACKAGE_NAME) || create_package
      end

      def create_package
        log "Creating package..." if verbose?

        Registry::Package.create!(
          name: PACKAGE_NAME,
          repository: repository,
          owner: owner,
          package_type: :npm,
        )
      end

      def package_version
        @package_version ||= package.package_versions.first || create_package_version
      end

      def create_package_version
        log "Creating package version..." if verbose?

        Registry::PackageVersion.create!(
          version: PACKAGE_VERSION,
          package: package,
          release: release,
          author: owner,
        )
      end

      def release
        @release ||= repository.releases.find_by(name: PACKAGE_VERSION) || create_release
      end

      def create_release
        log "Creating release..." if verbose?

        Release.new(repository: repository, author: owner, name: PACKAGE_VERSION, tag_name: PACKAGE_VERSION).tap do |r|
          r.save(validate: false) # Skip validations about repo being empty
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "owner=", "Login of owner to create usage for", required: true
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::GenerateSyntheticMeteredUsageData.new(options)
  transition.run
end
