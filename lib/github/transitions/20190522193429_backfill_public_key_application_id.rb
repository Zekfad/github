# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190522193429_backfill_public_key_application_id.rb -v | tee -a /tmp/backfill_public_key_application_id.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190522193429_backfill_public_key_application_id.rb -v -w | tee -a /tmp/backfill_public_key_application_id.log
#
module GitHub
  module Transitions
    class BackfillPublicKeyApplicationId < Transition

      BATCH_SIZE = 1000
      BATCH_UPDATE_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM public_keys") }
        max_id = readonly { ApplicationRecord::Domain::Users.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM public_keys") }

        @iterator = ApplicationRecord::Domain::Users.github_sql_batched_between(
          start: min_id,
          finish: max_id,
          batch_size: BATCH_SIZE,
        )

        @iterator.add <<-SQL
          SELECT public_keys.id,
            public_keys.oauth_authorization_id
          FROM public_keys
          WHERE public_keys.id BETWEEN :start AND :last
            AND public_keys.oauth_application_id IS NULL
            AND public_keys.oauth_authorization_id IS NOT NULL
        SQL
      end

      # Returns nothing.
      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        rows.each_slice(BATCH_UPDATE_SIZE).each do |items|
          items = map_public_keys_to_oauth_application_ids(items)

          log "updating next #{items.size} public keys after id=#{items.first.first}" if verbose?

          ApplicationRecord::Domain::Users.throttle do
            next if dry_run?

            ActiveRecord::Base.connected_to(role: :writing) do
              sql = ApplicationRecord::Domain::Users.github_sql.new(<<~SQL)
                UPDATE public_keys
                SET oauth_application_id = CASE
              SQL

              items.each do |(id, application_id)|
                sql.add "WHEN id = :id THEN :application_id", id: id, application_id: application_id
              end

              sql.add "END WHERE id IN :ids", ids: items.map(&:first)

              sql.run
            end
          end
        end
      end

      private

      # Private: Map the PublicKeys that have a valid OauthAuthorization
      # to their respective OauthApplications.
      #
      # Returns an Array
      def map_public_keys_to_oauth_application_ids(items)
        options = {
          oauth_authorization_ids:    items.map(&:last),
          oauth_application_type:     OauthAuthorization::OAUTH_APPLICATION,
          personal_application_token: OauthApplication::PERSONAL_TOKENS_APPLICATION_ID,
        }

        results = OauthAuthorization.github_sql.results <<-SQL, options
          SELECT oauth_authorizations.id,
            oauth_authorizations.application_id
          FROM oauth_authorizations
          WHERE oauth_authorizations.id IN :oauth_authorization_ids
          AND oauth_authorizations.application_type = :oauth_application_type
          AND oauth_authorizations.application_id <> :personal_application_token
        SQL

        # Create a Hash table for easier lookup.
        # Where the OauthAuthorization ID is the key,
        # and the OauthApplication ID is the value.
        oauth_authorizations_map = results.to_h

        # Remove all of the PublicKeys IDs that don't have a
        # corresponding OauthAuthorization.
        items.keep_if do |_, oauth_authorization_id|
          oauth_authorizations_map.key?(oauth_authorization_id)
        end

        # Replace the OauthAuthorization ID with the OauthApplication ID
        # for each PublicKey. This brings the data `items` back into its
        # original state before we broke up the JOIN.
        items.map do |id, oauth_authorization_id|
          [id, oauth_authorizations_map[oauth_authorization_id]]
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillPublicKeyApplicationId.new(options)
  transition.run
end
