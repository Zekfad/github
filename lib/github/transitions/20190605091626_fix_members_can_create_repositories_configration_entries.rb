# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190605091626_fix_members_can_create_repositories_configration_entries.rb -v | tee -a /tmp/fix_members_can_create_repositories_configration_entries.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20190605091626_fix_members_can_create_repositories_configration_entries.rb -v -w | tee -a /tmp/fix_members_can_create_repositories_configration_entries.log
#
module GitHub
  module Transitions
    class FixMembersCanCreateRepositoriesConfigrationEntries < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Domain::ConfigurationEntries.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM configuration_entries") }
        max_id = readonly { ApplicationRecord::Domain::ConfigurationEntries.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM configuration_entries") }

        @iterator = ApplicationRecord::Domain::ConfigurationEntries.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id FROM configuration_entries
          WHERE id BETWEEN :start AND :last
          AND name = 'disable_members_can_create_repositories'
          AND target_type = 'User'
          AND final = 1
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        Configuration::Entry.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          log "Setting final to 0 for configuration_entries.id #{rows.map { |row| row[0] }}" if verbose?
          run_batch_update rows unless dry_run?
        end
      end

      def run_batch_update(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Domain::ConfigurationEntries.github_sql.new <<-SQL, ids: rows.map { |row| row[0] }
            UPDATE configuration_entries
            SET final = 0
            WHERE id IN :ids
          SQL
          sql.run
          sql.affected_rows
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::FixMembersCanCreateRepositoriesConfigrationEntries.new(options)
  transition.run
end
