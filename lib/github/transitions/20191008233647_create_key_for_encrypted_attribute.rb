# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191008233647_create_key_for_encrypted_attribute.rb -v -model=User -column=weak_password_check_result | tee -a /tmp/create_key_for_encrypted_attribute.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191008233647_create_key_for_encrypted_attribute.rb -v -w -model=User -column=weak_password_check_result | tee -a /tmp/create_key_for_encrypted_attribute.log
#
module GitHub
  module Transitions
    class CreateKeyForEncryptedAttribute < Transition
      attr_reader :key_name, :key_pair_algo, :secret_key_algo, :permission

      def after_initialize
        model = Object.const_get(other_args[:model])
        @key_name = GitHub::Earthsmoke::EncryptedAttribute.key_name(
          model.table_name,
          other_args[:column],
        )
        @key_pair_algo = ::EarthsmokeProtocol::DEFAULT_ALGO
        @secret_key_algo = ::EarthsmokeProtocol::DEFAULT_ALGO
        @permission = ::Earthsmoke.permission(
          encrypt: true,
          decrypt: true,
          export: true,
          export_secrets: true, # needed for local encrypt/decrypt
          destroy: true,
          roll: true,
        )
      end

      # Returns nothing.
      def perform
        permission_str = permission.to_h.to_a.select(&:last).map(&:first).join(", ")
        log "Generating earthsmoke key #{key_name} key_pair_algo=#{key_pair_algo} secret_key_algo=#{secret_key_algo} permission=#{permission_str}"

        return if dry_run?

        if key.exists?
          log "Key already exists"
          return
        end

        resp = llk.generate(
          key_pair_algo: key_pair_algo,
          secret_key_algo: secret_key_algo,
          permission: permission,
        )

        log "Success!"
      end

      def key
        GitHub.earthsmoke.key(key_name)
      end

      def llk
        GitHub.earthsmoke.low_level_key(key_name)
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "model=", "the model that the encrypted column is on"
    on "column=", "the column that is encrypted"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CreateKeyForEncryptedAttribute.new(options)
  transition.run
end
