# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20180614210117_remove_orphan_checks.rb -v | tee -a /tmp/remove_orphan_checks.log
#
#
# Stolen mostly from https://github.com/github/github/blob/2b63c3a7ef17aa0ceb6518288a028e8410add786/app/models/data_quality/basic/orphan.rb
module GitHub
  module Transitions
    class RemoveOrphanChecks < Transition

      # @github/database-transitions-code-review is your friend, and happy to help
      # code review transitions before they're run to make sure they're being nice
      # to our database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/transitions

      # Recommended batch size number, but you can modify for your use case
      BALLAST_BATCH_SIZE = 1_000
      # attr_reader :iterator

      ORPHANED_TABLES = [
        ["CheckSuite", "CheckRun", "check_suite_id"],
        ["CheckRun", "CheckAnnotation", "check_run_id"],
      ]

      # Generated via presto, using
      # SELECT check_suites.id FROM hive.snapshots_presto.check_suites LEFT JOIN analytics_mysql.github_production.repositories
      #     ON check_suites.repository_id = repositories.id WHERE repositories.id IS NULL;
      ORPHAN_CHECK_SUITES_IDS = %w(
        101767 102315 102391 144686 258488 258625 258648 261880 272559 293273 320294 320528 323706
        323727 323821 323865 323919 325976 325989 329781 329814 432230 444959 445045 495770 512759
        512790 512980 512995 513073 513826 513853 563277 563517 569696 638899 639864 640168 640488
        640664 641625 641984 658691 659954 734124 735165 735519 762946 790295 793518 793568 793572
        793592 793593 795749 795979 863690 883614 883738 1079881 1112512 1151329 1151952 1172886 1173135
        1184050 1240143 1240145 1246200 1246979 1248539 1249159 1251736 1257376 1258720 1265313 1269646
        1270521 1271562 1294870 1297616 1323482 1325431 1325468 1340994 1341083 1487327 1487351 1487425
        1487509 1615878 1615915 1616053 1616141 1616222 1616275 1617099 1706256 1706508 1706562 1707230
        1707303 1713108 1734315 1739007 1748639 1884848 1885768 1886255 1886343 1886381 1886546 1892019
        1892340 1892421 1892435 1893001 1893016 1898245 1898928 2000227 2076384 2083203 2083556 2083557
        2261220 2306911 2339862 2340243 2352522 2353174 2364745 2365712 2369325 2369988 2370121 2370411
        2370633 2371015 2377515 2379917 2380846 2382608 2382839 2383254 2383455 2383623 2385266 2386529
        2387168 2387371 2388370 2389473 2390013 2396865 2454019 2455117 2456546 2462938 2465080 2519107
        2519128 2519182 2535754 2542194 2542510 2542867 2543451 2550731 2551378 2553186 2742099 2778832
        2828354 2829708 2866939 2867059 2867137 2871357 2934601 2935381 2936037 2937951 2938994 2939772
        2940402 2958100 3034095 3034399 3041026 3058303 3068101 3068174 3068248 3068283 3069066 3069428
        3069798 3073445 3130557 3131016 3131041 3154648 3158137 3181605 3182634 3182643 3182644 3182649
        3182651 3182672 3182678 3182681 3182682 3182686 3182691 3182700 3182708 3214784 3214963 3216603
        3219920 3222103 3225899 3239691 3244267 3245009 3257109 3257399 3257767 3259169 3260948 3261328
        3261727 3311603 3313988 3314856 3330781 3331500 3331543 3331620 3336507 3336508 3336509 3336510
        3336513 3336514 3336515 3336516 3336520 3336523 3336524 3336525 3336527 3336528 3336531 3336539
        3346690 3348381 3352431 3352802 3354685 3356112 3366248 3366313 3371124 3371125 3371127 3371128
        3371131 3371136 3371137 3371139 3371141 3371143 3371144 3371145 3371148 3371150 3371151 3371153
        3379765 3380517 3380935 3381667 3382014 3384146 3384147 3384148 3384151 3384229 3384322 3384721
        3385140 3385557 3385993 3386267 3386381 3386386 3386693 3386786 3387114 3387285 3387576 3387872
        3388145 3388853 3390322 3390557 3390792 3391163 3397465 3401148 3401366 3401389 3401420 3401442
        3401574 3401606 3401654 3401691 3401702 3401707 3401721 3401761 3401791 3401902 3401906 3401908
        3401918 3401920 3401967 3402015 3425773 3425865 3426201 3455303 3455318 3455343 3455358 3455410
        3455421 3455429 3455460 3455477 3455524 3455596 3455618 3455641 3464151 3473116 3476368 3479847
        3483054 3487206 3487322 3487424 3487987 3488246 3490590 3494732 3495038 3496888 3497300 3497575
        3497770 3497771 3504618 3513878 3514809 3514902 3515105 3515292 3515596 3516018 3516464 3517643
        3517995 3518637 3518905 3518991 3519090 3519109 3519299 3519349 3519432 3519591 3519774 3519874
        3522775 3524648 3532732 3541062 3541194 3542865 3543542 3544751 3544929 3545111 3545956 3546079
        3546170 3546743 3546753 3547441 3547604 3548944 3548945 3550164 3550877 3552339 3554276 3554593
        3554960 3555541 3555905 3556018 3556428 3556667 3557472 3557704 3557977 3558286 3558945 3559026
        3559499 3559684 3559843 3560062 3560453 3560755 3561321 3561595 3562122 3562216 3564617 3564789
        3565208 3565300 3567963
      ).map(&:to_i).freeze

      def after_initialize
        ORPHANED_TABLES.each do |parent, child, foreign_key|
          parent_table = parent.constantize.table_name
          child_table  = child.constantize.table_name
          foreign_key  = foreign_key

          max_id = readonly { ApplicationRecord::Ballast.github_sql.value("SELECT MAX(id) FROM #{child_table}") }
          iterator = ApplicationRecord::Ballast.github_sql_batched_between(start: 0, finish: max_id, batch_size: BALLAST_BATCH_SIZE)
          iterator.add <<-SQL
            SELECT #{child_table}.id, #{child_table}.#{foreign_key}
            FROM #{child_table} LEFT JOIN #{parent_table}
            ON #{child_table}.#{foreign_key} = #{parent_table}.id
            WHERE #{child_table}.id BETWEEN :start AND :last
            AND #{parent_table}.id IS NULL
          SQL

          instance_variable_set("@#{parent.downcase}_iterator", iterator)
        end
      end

      # Returns nothing.
      def perform
        log "  * Removing orphaned check_suites records..."

        log "  * Found #{ORPHAN_CHECK_SUITES_IDS.size} orphaned check_suites ..."

        process("check_suites", ORPHAN_CHECK_SUITES_IDS)

        log "  * Removed #{ORPHAN_CHECK_SUITES_IDS.size} orphaned CheckSuite records"

        [@checksuite_iterator, @checkrun_iterator].each_with_index do |iterator, idx|
          log "  * Scanning for orphaned #{ORPHANED_TABLES[idx][0]} -> #{ORPHANED_TABLES[idx][1]} records via #{ORPHANED_TABLES[idx][2]}"
          deleted = 0
          child_table = ORPHANED_TABLES[idx][1].constantize.table_name
          GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
            child_ids = rows.map(&:first).flatten
            deleted += child_ids.size
            process(child_table, child_ids)
          end

          log "  * Removed #{deleted} orphaned #{ORPHANED_TABLES[idx][1]} records"
        end
      end

      def process(child_table, orphan_ids)
        unless dry_run?
          ApplicationRecord::Ballast.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            delete = ApplicationRecord::Ballast.github_sql.new \
              orphan_ids: orphan_ids
            delete.run <<-SQL
              DELETE FROM #{child_table} WHERE id IN :orphan_ids
            SQL
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RemoveOrphanChecks.new(options)
  transition.run
end
