# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200205200838_backfill_stripe_webhook_account.rb -v | tee -a /tmp/backfill_stripe_webhook_account.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200205200838_backfill_stripe_webhook_account.rb -v -w | tee -a /tmp/backfill_stripe_webhook_account.log
#
module GitHub
  module Transitions
    class BackfillStripeWebhookAccount < Transition

      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ApplicationRecord::Ballast.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM stripe_webhooks") }
        max_id = readonly { ApplicationRecord::Ballast.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM stripe_webhooks") }

        @iterator = ApplicationRecord::Ballast.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, payload, kind FROM stripe_webhooks
          WHERE id BETWEEN :start AND :last
          AND kind in (30, 31, 32, 40, 41, 100)
        SQL
      end

      def perform
        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        ::Billing::StripeWebhook.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          rows.each do |item|
            account_id = extract_account_id_from_payload_for_kind(item[1], item[2])
            log "Setting account_id to #{account_id} for webhook #{item[0]}"
          end
          run_batch_update rows unless dry_run?
        end
      end

      def run_batch_update(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Ballast.github_sql.new "UPDATE stripe_webhooks SET account_id = CASE"

          rows.each do |id, payload, kind|
            id = id
            account_id = extract_account_id_from_payload_for_kind(payload, kind)
            sql.add <<-SQL, id: id, account_id: account_id
              WHEN id = :id THEN :account_id
            SQL
          end

          sql.add "END WHERE id IN :ids", ids: rows.map { |item| item[0] }
          sql.run
          sql.affected_rows
        end
      end

      def extract_account_id_from_payload_for_kind(payload, kind)
        parsed_payload = JSON.parse(payload)
        case kind
        when 30, 31, 32
          parsed_payload["data"]["object"]["destination"]
        when 40, 41, 100
          parsed_payload["account"]
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::BackfillStripeWebhookAccount.new(options)
  transition.run
end
