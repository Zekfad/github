# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191120201449_repair_locked_installations.rb -v | tee -a /tmp/repair_locked_installations.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20191120201449_repair_locked_installations.rb -v -w | tee -a /tmp/repair_locked_installations.log
#
module GitHub
  module Transitions
    class RepairLockedInstallations < Transition

      attr_reader :min_id

      def after_initialize
        @min_id = other_args[:min_id] || readonly do
          IntegrationInstallation.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM integration_installations")
        end
      end

      def perform
        locked_installations_and_repos = {}

        readonly do
          IntegrationInstallation.preload(:target).find_each(batch_size: 100, start: min_id) do |installation|
            installation.repositories.preload(:owner).find_each do |repo|
              if repo.owner != installation.target
                locked_installations_and_repos[installation] ||= []
                locked_installations_and_repos[installation] << repo
                log "Detected corrupted installation: #{installation.id}"
              end
            end
          end
        end

        process(locked_installations_and_repos)
      end

      def process(installations_and_repos)
        installations_and_repos.each do |installation, repos|
          integration = installation.integration
          target = installation.target

          repos.each do |repo|
            if dry_run?
              log "Would have attempted to uninstall #{integration.name} from #{target.name}"
              log "Previous repo location: #{target.name}/#{repo.name}, new location: #{repo.name_with_owner}"

              next
            end

            if installation.repository_ids.count == 1
              IntegrationInstallation.throttle do
                installation.uninstall(actor: repo.owner)
                log "Uninstalled #{integration.name} from #{target.name}"
              end
            else
              Permission.throttle do
                ::Permissions::Service.revoke_permissions_granted_on_subjects(
                  actor_id:      installation.ability_id,
                  actor_type:    installation.ability_type,
                  subject_ids:   [repo.id],
                  subject_types: Repository::Resources.individual_type_prefixed_subject_types,
                )
              end
            end

            log "Uninstalled #{repo.name_with_owner} from #{target.name}"
          end

          log "Repaired installation: #{installation.id}"
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "min_id=", "min_id", "The installations iterator minimum id"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RepairLockedInstallations.new(options)
  transition.run
end
