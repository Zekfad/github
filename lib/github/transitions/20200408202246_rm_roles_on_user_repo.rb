# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200408202246_rm_roles_on_user_repo.rb --verbose | tee -a /tmp/rm_roles_on_user_repo.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200408202246_rm_roles_on_user_repo.rb --verbose -w | tee -a /tmp/rm_roles_on_user_repo.log
#
module GitHub
  module Transitions
    class RmRolesOnUserRepo < Transition
      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      attr_reader :iterator

      def after_initialize
        min_id = readonly { ::UserRole.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM user_roles") }
        max_id = readonly { ::UserRole.github_sql.value("SELECT COALESCE(MAX(id), 0) FROM user_roles") }

        @iterator = ::UserRole.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, actor_id, target_id
          FROM user_roles
          WHERE id BETWEEN :start AND :last
          AND target_type = 'Repository'
        SQL

        @total_delete_count = 0
      end

      # Returns nothing.
      def perform
        log "beginning to process data" if verbose?

        GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
          process(rows)
        end
      end

      def process(rows)
        ids_to_check = rows.map { |row| row[2] }
        repo_ids = get_user_owned_repo_ids(ids_to_check)

        if repo_ids
          delete_user_roles(repo_ids: repo_ids)
        end

        log "#{@total_delete_count} rows deleted" if verbose?
      end

      # we have to run seperate queries here since repo is being pulled into
      # a different cluster and we can no longer join across
      def get_user_owned_repo_ids(ids)
        owner_and_repo_ids = Repository.where(id: ids).pluck(:owner_id, :id)
        owner_ids = owner_and_repo_ids.map { |entry| entry[0] }

        # do some manipulation to avoid an extra query against repos
        repo_owner_ids = User.where(id: owner_ids, type: "User").pluck(:id)
        repo_ids = []
        owner_and_repo_ids.each do |entry|
          repo_ids << entry[1] if repo_owner_ids.include?(entry[0])
        end

        repo_ids
      end

      def delete_user_roles(repo_ids:)
        UserRole.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          user_roles = UserRole.where(target_id: repo_ids, target_type: "Repository")
          delete_count = user_roles.size

          log "#{delete_count} to destroy." if verbose?
          if delete_count > 0
            log "Start index: #{user_roles.first.id}, End index: #{user_roles.last.id}" if verbose?
          end
          @total_delete_count += delete_count

          return if dry_run?
          user_roles.destroy_all
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::RmRolesOnUserRepo.new(options)
  transition.run
end
