# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200409193746_acv_contributors_write.rb --verbose | tee -a /tmp/acv_contributors_write.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200409193746_acv_contributors_write.rb --verbose -w | tee -a /tmp/acv_contributors_write.log
#
module GitHub
  module Transitions
    class AcvContributorsWrite < Transition

      # @github/db-schema-reviewers is your friend, and can help code review
      # transitions before they're run to make sure they're being nice to our
      # database clusters. We're usually looking for a few things in transitions:
      #   1. Iterators: We want to query the database for records to change in batches
      #   2. Read-Only Replicas: If we're reading data to be changed, we want to do it on
      #      the read-only replicas to keep load off the master
      #   3. Throttle writes: We want to make sure we wrap any actual writes to the master
      #      in a `throttle` block, this will make sure we don't overwhelm master and cause
      #      replication lag
      #   4. Efficient queries: We want to avoid massive table scans, so make sure your
      #      query has an index or is performant and safe without one.
      #
      #   For more information on all this, checkout the transition docs at
      #   https://githubber.com/article/technology/dotcom/migrations-and-transitions/transitions

      # Recommended batch size number, but you can modify for your use case
      BATCH_SIZE = 100

      #####
      # CAUTIONARY WARNING FOR FUTURE COPY-PASTE-REUSERS:
      #
      # The following `INSERT` query uses a `ON DUPLICATE KEY UPDATE ...` clause
      # to avoid failures when attempting to insert duplicate records without
      # having to restart the transition. However, with this particular table
      # and the likelihood of its data always being loaded in large buckets of
      # millions or billions of records at a time that may include duplicates
      # of records loaded in the past, you should be aware that MySQL's
      # "upsert" behavior here will always permanently allocate an ID for the
      # pending record's insertion, even if it ends up being a duplicate.
      #
      # Advice for future data loading of ACV data:
      #   https://github.com/github/github/pull/141572#issuecomment-620231909
      #####
      INSERT_QUERY = <<-SQL
        INSERT INTO acv_contributors
        (
          repository_id,
          contributor_email,
          created_at,
          updated_at
        )
        VALUES :rows
        ON DUPLICATE KEY UPDATE updated_at = NOW()
      SQL

      def after_initialize
        @file = @other_args[:file]
        @new_repos_only = @other_args[:new_repos_only] || false
        @start_line = @other_args[:start_at].nil? ? 1 : @other_args[:start_at].to_i
        @this_repo = { nwo: nil, id: nil, contributor_count: 0, skip: false }

        if @start_line > 1
          log "starting processing at line #{@start_line}, skipping all lines prior" if verbose?
        end
      end

      # Returns nothing.
      def perform
        return unless @file && File.file?(@file)

        File.open(@file, "r") do |file|
          file.lazy.each_slice(BATCH_SIZE).with_index.each do |lines, index|
            batch_length = [lines.length, BATCH_SIZE].min
            batch_first_line = index * BATCH_SIZE + 1
            batch_last_line = index * BATCH_SIZE + batch_length

            if batch_last_line < @start_line
              log "skipping batch #{index}, lines #{batch_first_line} to #{batch_last_line}" if verbose?
              next
            end

            log "processing batch #{index}, lines #{batch_first_line} to #{batch_last_line}" if verbose?
            process(lines)
          end

          # We need this one-off log to avoid missing it for the last repo processed
          log "Contributors for #{@this_repo[:nwo]}: #{@this_repo[:contributor_count]}" if verbose?
        end
      end

      def process(lines)
        insert_rows = []

        lines.each do |line|
          repo_nwo, email = line.chomp.split(" ", 2)

          if @this_repo[:nwo] != repo_nwo
            log "Contributors for #{@this_repo[:nwo]}: #{@this_repo[:contributor_count]}" if verbose?

            repo = readonly { Repository.nwo(repo_nwo, search_redirects: true) }

            existing_repo_id = if @new_repos_only && repo
              readonly {
                ApplicationRecord::Collab.github_sql.value("SELECT repository_id FROM acv_contributors WHERE repository_id = :repo_id LIMIT 1", repo_id: repo.id)
              }
            end

            # Cache the repository result as the next line may also be an entry
            # for the same one
            @this_repo = {
              nwo: repo_nwo,
              id: repo&.id,
              contributor_count: 0,
              skip: @new_repos_only && existing_repo_id.present?
            }

            if verbose?
              if repo.nil?
                log "Could not find repo: #{repo_nwo}"
              elsif @this_repo[:skip]
                log "Skipping #{repo.id}: #{repo_nwo} as it already exists"
              else
                # Output both the `repo_nwo` and the `repo.nwo` if the repo has
                # been renamed or transferred since the ACV data was archived
                log "Processing: #{repo_nwo} => #{repo.id}" + (repo.nwo != repo_nwo ? " (#{repo.nwo})" : "")
              end
            end
          end

          @this_repo[:contributor_count] += 1

          # If the repo was not found, we can assume it was deleted after
          # the Arctic Code Vault archive data was queried.
          # Also skip if the skip flag is enabled.
          next if @this_repo[:id].nil? || @this_repo[:skip]

          insert_rows << [@this_repo[:id], email, GitHub::SQL::NOW, GitHub::SQL::NOW]
        end

        return if insert_rows.empty?

        AcvContributor.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          run_batch_insert insert_rows unless dry_run?
        end
      end

      def run_batch_insert(rows)
        ActiveRecord::Base.connected_to(role: :writing) do
          sql = ApplicationRecord::Collab.github_sql.run INSERT_QUERY, rows: GitHub::SQL::ROWS(rows)
          log "Inserted or updated #{sql.affected_rows} rows" if verbose?
        end
      end

    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "file=", "Path to contributor file"
    on "start_at=", "Line number to start processing at"
    on "new_repos_only", "Only run this transition on repos that don't already exist in acv_contributors table"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::AcvContributorsWrite.new(options)
  transition.run
end
