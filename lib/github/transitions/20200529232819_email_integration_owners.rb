# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   # First, run the transition in dry-run mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200529232819_email_integration_owners.rb --verbose -e ./email_integration_owners_emailed_dry_run.log | tee -a /tmp/email_integration_owners.log
#   # Then, run the transition in regular mode
#   $ gudo bin/safe-ruby lib/github/transitions/20200529232819_email_integration_owners.rb --verbose -e ./email_integration_owners_emailed.log -w | tee -a /tmp/email_integration_owners.log
#
module GitHub
  module Transitions
    class EmailIntegrationOwners < Transition

      class EmailLog
        attr_reader :log_file, :notified_owners
        def initialize(file:)
          @log_file = file
          @log_file.sync = true

          @notified_owners = Set.new
          load_notified_owners
        end

        def notified?(owner:)
          @notified_owners.include?(owner.id)
        end

        def notified(owner:)
          @notified_owners << owner.id
          @log_file.puts(owner.id)
        end

        private

        def load_notified_owners
          @log_file.each_line do |line|
            @notified_owners << line.to_i
          end
        end
      end

      class TickRateLimiter
        attr_reader :limit, :slack

        def initialize(limit: nil, slack: nil)
          @limit = (limit || 50).to_i
          @slack = (slack || 2.5).to_f
          @tick = 0
        end

        def tick
          @tick += 1
          if @tick > limit
            give_some_slack
            @tick = 0
          end
        end

        def give_some_slack
          sleep slack
        end
      end

      BATCH_SIZE = 1000

      attr_reader :iterator, :email_log, :rate_limiter, :enqueue

      def after_initialize
        min_id = other_args[:min]
        min_id ||= readonly { ApplicationRecord::Domain::Integrations.github_sql.value("SELECT COALESCE(MIN(id), 0) FROM integrations") }
        min_id = min_id.to_i
        max_id = Integration::LEGACY_EVENT_ID_CUTTOFF

        @iterator = ApplicationRecord::Domain::Integrations.github_sql_batched_between(start: min_id, finish: max_id, batch_size: BATCH_SIZE)
        @iterator.add <<-SQL
          SELECT id, owner_id, owner_type FROM integrations
          WHERE id BETWEEN :start AND :last
        SQL

        unless other_args[:email_log]
          $stderr.puts "You must specify and email log file with -e"
          exit 1
        end

        if other_args[:integrations]
          @integration_ids = Integration.find(other_args[:integrations].split(",").map(&:to_i)).map do |integration|
            [integration.id, integration.owner_id, integration.owner_type]
          end
        else
          @integration_ids = false
        end

        if other_args.has_key?(:enqueue)
          @enqueue = other_args[:enqueue]
        else
          @enqueue = false
        end

        log_file = other_args[:email_log]
        log_io = log_file.is_a?(String) ? File.open(log_file, "a+") : log_file
        @email_log = EmailLog.new(file: log_io)
        @rate_limiter = TickRateLimiter.new(limit:  other_args[:limit], slack: other_args[:slack])
      end

      # Returns nothing.
      def perform
        # Here we iterate over batches of integration ids
        if @integration_ids
          process(@integration_ids)
        else
          GitHub::SQL::Readonly.new(iterator.batches).each do |rows|
            process(rows)
          end
        end
      end

      # For each integration row we:
      #
      # 1- find the owner
      # 2- if we've already emailed this owner, skip them
      # 3- send a single email containing all integrations owned by the owner
      #    - collect the integrations owned by this user or org in the mailer
      # 4- log the integration owner as notified
      #
      # Because we notify all admins of an org when notifying, it's possible a
      # user will receive multiple emails. They will receive one email for their
      # user account if they own an integration, one email for each Org they admin
      # which owns an app
      #
      # We do not consider App Managers.
      def process(rows)
        rows.each do |integration_id, owner_id, owner_type|
          if owner_type == "Business"
            log "integration_id=#{integration_id}, owner_id=#{owner_id}, delivered_notification=skipping_business"
            next
          end
          owner = User.where(id: owner_id).first

          unless owner
            log "integration_id=#{integration_id}, owner_id=#{owner_id}, delivered_notification=orphaned_integration"
            next
          end

          if @email_log.notified?(owner: owner)
            log "integration_id=#{integration_id}, owner_id=#{owner.id}, delivered_notification=already_notified_owner"
            next
          end

          mailer = owner.organization? ? OrganizationMailer : AccountMailer

          notify_integration_owners(owner, mailer) unless dry_run?
          @email_log.notified(owner: owner)
          log "integration_id=#{integration_id}, owner_id=#{owner.id}, delivered_notification=true" if verbose?
        end
      end

      def notify_integration_owners(owner, mailer)
        app_names = owner.integrations.select do |app|
          below_cutoff = app.id < Integration::LEGACY_EVENT_ID_CUTTOFF
          below_cutoff && !app.connect_app?
        end.map(&:name)

        return false unless app_names.any?

        @rate_limiter.tick

        mail = mailer.legacy_integration_event_deprecation(owner, app_names)
        if @enqueue
          mail.deliver_later
        else
          mail.deliver_now
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
    on "m", "min=", "min_id to start with"
    on "e", "email_log=", "email log file to use"
    on "l", "limit=", "number of emails to send before sleeping"
    on "s", "slack=", "the amount of seconds to sleep after <limit> number of emails"
    on "i", "integrations=", "A comma separated list of Integration IDs to notify owners of 1,2,3"
    on "q", "enqueue", "Enqueue mail to be delivered async"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::EmailIntegrationOwners.new(options)
  transition.run
end
