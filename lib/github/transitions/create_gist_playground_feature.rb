# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

module GitHub
  module Transitions
    class CreateGistPlaygroundFeature < Transition
      def perform
        flipper_feature = nil

        feature = Feature.new(
          public_name: "Gist Playground",
          slug: "gist-playground",
          feedback_category: "other",
          description: "Collaborative Editing for Gists",
          flipper_feature_id: flipper_feature&.id,
          # feedback_link: "https://example.com",
          enrolled_by_default: false,
        )

        unless dry_run
          feature.save
          log("Feature could not be created: #{feature.errors.messages}") if verbose && feature.errors.any?
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::CreateGistPlaygroundFeature.new(options)
  transition.run
end
