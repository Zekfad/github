# frozen_string_literal: true

require "#{Rails.root}/config/environment"
require "slop"

# To run this transition directly (using gh-screen):
#
#   $ cd /data/github/current
#   $ gudo bin/safe-ruby lib/github/transitions/20190130094452_disable_invalid_email_hooks.rb -v | tee -a /tmp/disable_invalid_email_hooks.log
#
module GitHub
  module Transitions
    class DisableInvalidEmailHooks < Transition
      # We are going to query sparse data, so we explicitly pick a high batch size
      # See https://githubber.com/article/technology/dotcom/transitions#querying-sparse-data
      BATCH_SIZE_READS = 10_000
      BATCH_SIZE_WRITES = 100

      # Returns nothing.
      def perform
        max_id = Hook.github_sql.new("SELECT COALESCE(max(id), 0) FROM hooks").value

        0.step(max_id, BATCH_SIZE_READS) do |start|
          ids = readonly do
            sql = Hook.github_sql.new <<-SQL, start: start, end: start + BATCH_SIZE_READS
              SELECT id FROM hooks h
              WHERE name = 'email'
              AND   id >= :start
              AND   id < :end
              AND NOT EXISTS (
                SELECT * FROM hook_config_attributes c
                WHERE c.hook_id = h.id AND c.key = 'address'
              )
            SQL

            sql.values
          end
          next if ids.empty?

          process(ids)
        end
      end

      def process(hook_ids)
        hook_ids.each_slice(BATCH_SIZE_WRITES) do |ids|
          Hook.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
            log "Disabling hooks #{ids}" if verbose?
            unless dry_run?
              Hook.where(id: ids).update_all(active: false)
            end
          end
        end
      end
    end
  end
end

# Run as a single process if this script is run directly
if $0 == __FILE__
  slop = Slop.parse(help: true, strict: true) do
    on "w", "write", "Enable writes for the transition - this defaults to false (i.e. a dry_run mode) for safety.", default: false
    on "v", "verbose", "Log verbose output"
  end
  options = slop.to_hash
  options = options.merge(dry_run: !options[:write])

  transition = GitHub::Transitions::DisableInvalidEmailHooks.new(options)
  transition.run
end
