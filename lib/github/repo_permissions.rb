# frozen_string_literal: true

require "securerandom"
require "rack"
require "github/dgit"
require "github/dns"
require "rack/request_logger"

module GitHub
  # Basic repository read/write permissions checking used for all native
  # git access to user, wiki, and gist repositories. The following
  # components rely on this implementation for authorization:
  #
  #  - babeld unified git+svn proxy (https://github.com/github/babeld)
  #  - LFS: app/models/media/git_auth.rb
  class RepoPermissions
    # Any values logged that may contain sensitive information are substituted
    # with this string.
    FILTERED = "[FILTERED]"

    SERVICE_NAME = "github/gitauth"
    PERMISSION_DENIED = "permission denied"
    HOOK_DECLINED = "pre-receive hook declined"

    # Determine how the user authenticated.
    def self.member_auth_type(member)
      case
        when member.nil?
          nil
        when member.bot?
          "bot"
        when member.using_auth_via_oauth_application?
          "user_via_oauth"
        when member.using_auth_via_integration?
          "user_via_integration"
        else
          "user"
      end
    end

    # Our caller is hopefully using Rack::RequestLogger middleware. We can use
    # our request env to add additional data to our logging context.
    #
    # Note: You'll see something similiar in Api::App and any other controller
    # base-class
    def self.log_data(request)
      request.env[Rack::RequestLogger::APPLICATION_LOG_DATA] ||= GitHub::Logger.empty
    end

    def self.log_exception(request, e, data = {})
      log_context = data.merge({
        now: Time.now.utc.iso8601,
        request_id: Rack::RequestId.get(request.env),
        ns: "gitauth",
        controller: "GitHub::RepoPermissions",
      })

      GitHub::Logger.log_exception(log_context, e)

      span = self.span(request.env)
      span.set_tag("error", true)
      span.set_tag("error.object", e)
    end

    # Private: The controller name to use for failbot and wherever else needed.
    CONTROLLER = "GitHub::RepoPermissions".freeze

    # Private: The default action for the action key in the application log data
    # key in env. Overridden where it makes sense to something more specific
    # (e.g. gitauth or commit_refs).
    DEFAULT_ACTION = "unknown".freeze

    # Rack handler that wraps request handling in read only database connection
    def self.call(env)
      # Whitelist all gitauth actions for tagging
      env[GitHub::TaggingHelper::STATSD_TAG_ACTIONS] = true
      env[GitHub::TaggingHelper::PROCESS_SERVICE_KEY] = SERVICE_NAME

      action = DEFAULT_ACTION
      request = Rack::Request.new(env)

      ctx = initial_context(request)
      GitHub.context.push(ctx.merge(connections: ApplicationRecord.connection_info))
      Audit.context.push(ctx)

      Failbot.push({
        catalog_service: SERVICE_NAME,
        controller: CONTROLLER,
        request_id: Rack::RequestId.get(env),
      })

      log_data(request).merge!({
        ns: "gitauth",
        controller: CONTROLLER,
        original_user_agent: request.env["HTTP_X_ORIGINAL_USER_AGENT"],
        catalog_service: SERVICE_NAME
      })

      span = self.span(request.env)
      span.operation_name = "#{request.request_method} #{request.path}"
      span.set_tag("ns", "gitauth")
      span.set_tag("controller", CONTROLLER)

      GitHub::Logger.log_context(ns: "gitauth", request_id: Rack::RequestId.get(request.env)) do

        case request.path
        when "/_gitauth"
          action = "gitauth"
          ActiveRecord::Base.connected_to(role: :reading) do
            handle_request(request)
          end
        when "/_commit_refs"
          action = "commit_refs"
          span.set_tag("action", "commit-refs")
          log_data(request)[:action] = "commit-refs"

          body = commit_refs(request)

          span.log(message: body["err"]) if body["err"].present?
          log_data(request)[:ok] = body["ok"]

          headers = {
            "Content-Type"=>"application/json",
          }
          request_id = Rack::RequestId.get(request.env)
          headers["X-GitHub-Request-Id"] = request_id unless request_id.nil?
          [200, headers, [GitHub::JSON.encode(body)]]
        when "/_hello_world"
          action = "hello_world"
          hello_world(request)
        else
          action = "404"
          [
            404, { "Content-Type" => "text/plain" },
            ["unknown gitauth call"]
          ]
        end
      end
    ensure
      # These get plucked out by GitHub::Middleware::Stats to help categorize
      # gitauth requests in DataDog.
      env["process.api.controller"] ||= CONTROLLER
      env["github.api.route"] ||= action
    end

    def self.hello_world(request)
      headers = {
        "Content-Type"=>"application/json",
        "X-GitHub-Request-Id" => Rack::RequestId.get(request.env),
      }

      [
        200,
        headers,
        [
          GitHub::JSON.encode({"hello" => "world"}),
        ],
      ]
    end

    def self.commit_refs(request)
      logging_context = log_data(request)

      body = GitAuth::CommitRefsRequestBody.new(request.body.read)

      logging_context.merge!({
        path: body.path,
        repo: body.repo,
      })

      commit_refs = GitAuth::CommitRefs.new(body)

      result = commit_refs.process

      filtered_actor = filtered_actor(commit_refs.actor)
      Failbot.push(actor: filtered_actor)
      logging_context[:actor] = filtered_actor

      result
    rescue => e
      ref_results = {}
      if body && body.refs
        body.refs.each do |ref, before, after|
          ref_results[ref] = "failure"
        end
      end

      err =
        if GitHub.global_operator_mode_enabled? || (commit_refs&.git_sockstat && commit_refs.git_sockstat.value("user_operator_mode"))
          "fatal error in commit_refs: %s\n" % e.inspect + "\n" + e.backtrace.inspect
        else
          "fatal error in commit_refs\n"
        end

      response = {
        "ok"   => false,
        "err"  => err,
        "refs" => ref_results,
      }

      log_exception(request, e, {
        action: "commit-refs",
        path: body&.path,
        repo: body&.repo,
        user: commit_refs&.actor,
        params: body&.json.inspect,
        response: GitHub::JSON.encode(response),
      })

      Failbot.report(e) unless e.is_a? GitHub::DGit::ThreepcBusyError

      response
    end

    # Internal: Filter member if it appears to be an OAuth token or deploy key.
    #
    # Users may pass an OauthAccess token using basic auth as their username or
    # their password (we accept both). We check to seee if `member` looks like
    # an OauthAccess token (40 hex chars) so that it can be safely filtered in
    # the logs.
    #
    # Deploy keys are represented by the repo ID and name with owner. The repo
    # name also needs to be sanitized before it can be passed on to external
    # logging tools.
    #
    # member - The String member that may contain a user login, OAuth
    #          token, or other representation of the actor performing
    #          the operation.
    #
    # Returns the filtered member as a String.
    def self.filtered_member(member)
      if member =~ OauthAccess::TOKEN_PATTERN
        FILTERED
      elsif member.to_s.start_with?("repo")
        member.split(":")[0..1].join(":")
      else
        member
      end
    end
    private_class_method :filtered_member

    # Internal: Hash token if an access token is used for authentication.
    #
    # Users may pass an OAuth access token using basic auth as their username or
    # their password. Integrations may pass an installation access token as
    # their password. We check to see if `member` or `password` looks like an
    # access token. Whichever one looks like a token is hashed so that it can be
    # safely logged.
    #
    # member   - The String member that may contain a username or an OAuth
    #            access token.
    # password - The String password that may contain a password, an OAuth
    #            access token, or an installation access token.
    #
    # Returns the hashed token as a String or nil if no token was identified.
    def self.hashed_token(member, password)
      if member =~ OauthAccess::TOKEN_PATTERN
        OauthAccess.hash_token(member)
      elsif password =~ OauthAccess::TOKEN_PATTERN
        OauthAccess.hash_token(password)
      elsif password =~ AuthenticationToken::TOKEN_PATTERN_V1
        AuthenticationToken.hash_for(password)
      else
        nil
      end
    end
    private_class_method :hashed_token

    # Rack application for serving permissions requests. This is mounted into
    # config.ru at /_gitauth and expects the following query parameters:
    #
    # path - The logical user, wiki, or gist repository path. The
    #   ".git" extension must be included.
    # member - The string username (or repository name in case of deploy key) of
    #   the account requesting access.
    # action - The requested access level. Must be either 'read' or 'write'.
    # proto - The protocol of the request
    # dc - The datacenter from which the original request was sent.  Used to
    #   sort the results of route lookups to prefer that datacenter.
    #
    # Returns a 200 response with the "<host>:<path>" of the physical repository
    #   when access is granted. If access is not available for any reason, a 403
    #   response is returned with an error message for the user in the body.
    def self.handle_request(request)
      request_id = Rack::RequestId.get(request.env)

      params  = request.POST

      action        = params["action"]
      fingerprint   = params["fingerprint"]
      hostname      = params["hostname"]
      key           = params["key"]
      password      = params["password"]&.b # passwords can contain random chars
      path          = params["path"]
      preferred_dc  = params["dc"]
      protocol      = params["proto"] || (password.nil? ? "ssh" : "http")
      slumlord_mode = params["slumlord"] == "true"

      repo = path.to_s.chomp(".git") if path

      member = params["member"]
      if member && !member.valid_encoding?
        member = member.b
      end
      member ||= slumlord_mode ? :slumlord : :anonymous

      filtered_member = filtered_member(member)
      hashed_token = hashed_token(member, password)

      failbot_context = {
        action: action,
        preferred_dc: preferred_dc,
        fingerprint: fingerprint,
        key: key,
        hostname: hostname,
        member: filtered_member,
        hashed_token: hashed_token,
        proto: protocol,
        request_id: request_id,
        slumlord_mode: slumlord_mode,
        user: filtered_member,
      }
      Failbot.push(failbot_context)

      log_data(request).merge!({
        action: action,
        preferred_dc: preferred_dc,
        path: path,
        repo: repo,
        proto: protocol,
        fingerprint: fingerprint,
        key: key,
        hostname: hostname,
        member: filtered_member,
        hashed_token: hashed_token,
      }.reject { |k, v| v.nil? })

      span = self.span(request.env)
      span.set_tag("action", action)
      span.set_tag("proto", protocol)

      response = ""
      status = :not_ok
      case action
      when "verify-key"
        # This action identifies an SSH key and maps it to an actor.
        #
        # Only after this step has succeeded might a given SSH key be
        # subsequently used together with `verification-token`,
        # `git-lfs-authenticate`, or the default action below.
        # It could also be used alone, in the case of calling
        # `ssh -T`.
        status, response = self.verify_key(request)
      when "verification-token"
        # This action is used with `ssh git@github.com verify`.
        #
        # This generates a token that the user can send to support
        # in the case where they've lost access to their 2FA device
        # or verification mechanisms.
        # Support then disables 2FA on their account, and the user
        # can set up 2FA fresh with a new device.
        # See https://githubber.com/article/crafts/support/web/general/security/identity-verification#ssh-verification
        if member == :anonymous
          # TODO: investigate.
          # We don't return an error message for anonymous requests.
          # Is this intentional?
          response = "\n"
        else
          response = "#{GitHub::SshVerification.generate_token(member, fingerprint) || "Error generating token."}\n"
        end
        # TODO: investigate.
        # We return :ok, whether the response succeeded or not.
        # Is this intentional?
        status = :ok
      when "git-lfs-authenticate"
        # This action generates a token for GitLFS access.
        #
        # It is only used with the SSH protocol, and
        # the resulting token is passed with the Authentication
        # header to an HTTPS request.
        status, response = git_lfs_authenticate(
          lfs_args: params["lfs-args"],
          path: path,
          member: member,
          protocol: protocol,
          password: password,
          key: key,
          ip: request.ip,
          country: request.env["HTTP_X_COUNTRY"],
          original_user_agent: request.env["HTTP_X_ORIGINAL_USER_AGENT"],
          req_id: request_id,
        )

        GitHub.dogstats.increment("lfs.ssh_resp", tags: ["status:#{status}"])
      else
        # This default action is used to determine whether the actor may
        # perform a particular git operation.
        args = {
          path: path,
          ip: request.ip,
          country: request.env["HTTP_X_COUNTRY"],
          original_user_agent: request.env["HTTP_X_ORIGINAL_USER_AGENT"],
          request_id: request_id,
          member: member,
          action: action,
          protocol: protocol,
          key: key,
          password: password,
        }
        permissions = GitAuth::Access.new(**args)
        status, response = permissions.verify

        if status == :not_ip_whitelisted
          log_data(request).merge!({
            ip_allow_list_policy_unsatisfied: true,
            ip_allow_list_policy_owner_id: permissions.target.repository.owner.id,
            ip_allow_list_policy_owner_type: :ORG,
          })
        end

        if status == :ok
          _, shard = response.split(/:/)
          preferred_dc ||= GitHub.datacenter
          stats = permissions.stats
          target = permissions.target

          response = {
            "routes"          => target.routes(shard, protocol, action, preferred_dc),
            "intercept"       => "repl", # Setting intercept=repl tells babeld to hit /_commit_refs.
            "sockstat"        => GitHub::GitSockstat.format(stats),
            "audit_log_kv"    => audit_log_pack_kvs(target, stats),
            "commit_ref_ctx"  => commit_refs_pack_ctx(stats, slumlord_mode, member_auth_type(permissions.user)),
            "postrx_hook_ctx" => {
              "url"    => target.postrx_hook_url,
              # Use the user login for the pusher if available. Otherwise use the SSH key verifier.
              "pusher" => stats.values_at(:user_login, :pubkey_verifier_login).compact.first.to_s,
            },
          }.to_json
        end
      end

      headers = {
        "Content-Length" => response.bytesize.to_s,
        "Content-Type" => "text/plain",
      }
      headers["X-GitHub-Request-Id"] = request_id unless request_id.nil?

      case status
      when :ok
        [200, headers, [response]]
      when :ldap_timeout
        # Terminate the worker in the event of an LDAP timeout to prevent any
        # inconsistencies that may occur due to the timeout.
        Process.kill("QUIT", Process.pid)
        [504, headers, [response]]
      else
        [403, headers, [response]]
      end
    rescue => boom
      log_exception(request, boom, {
        params: params.delete_if { |k, _| k == "password" }.inspect,
        path: path,
        repo: path.to_s.chomp(".git"),
        user: filtered_member,
        hashed_token: hashed_token,
        action: action,
        status: status,
        response: response,
      })

      Failbot.report(boom, { status: status })
      [500, {}, []]
    end

    def self.git_lfs_authenticate(lfs_args:, path:, member:, protocol:, password:, key:, ip:, country:, original_user_agent:, req_id:)
      command, nwo, operation, _ = lfs_args.split(" ", 4)
      if command != "git-lfs-authenticate"
        return [:invalid_command, "Invalid command: #{command.inspect}"]
      end

      nwo = nwo[1..-1] if nwo[0] == "/"
      if path != nwo && path != "#{nwo}.git"
        return [:path_mismatch, "Repository path mismatch: #{nwo.inspect} vs #{path.inspect}"]
      end

      if !operation.in? %w(upload download)
        return [:invalid_operation, "Invalid LFS operation: #{operation.inspect}"]
      end

      action = operation == "upload" ? :write : :read
      args = {
        path: path,
        ip: ip,
        country: country,
        original_user_agent: original_user_agent,
        request_id: req_id,
        member: member,
        action: action,
        protocol: protocol,
        key: key,
        password: password,
      }
      permissions = GitAuth::Access.new(**args)
      if permissions.target.gist?
        return [:bad_permissions, "LFS cannot be used on Gist repositories"]
      end
      status, msg = permissions.verify
      return [:bad_permissions, msg] if status != :ok

      lfs_auth = GitAuth::GitLFS.new(
        operation: operation,
        repository: permissions.target.repository,
        actor: permissions.user,
        deploy_key: permissions.user.nil? ? permissions.public_key : nil,
        protocol: protocol,
      )

      json = GitHub::JSON.encode(lfs_auth.response, pretty: true).to_s

      # Protects us from overflowing babeld buffer
      # https://github.com/github/babeld/pull/310#discussion-diff-26442959
      if json.size > 512
        return [:invalid_json, "JSON output too large"]
      end

      [:ok, json]
    end

    # SAFELY DEPLOYING CHANGES:
    #
    # If changing the set of key-values returned here, ensure first that multiple
    # versions of babeld are able to consume both variations: the set of key-values
    # before your change, and after. Babeld clients will span multiple hosts and
    # may also span multiple versions.  Some changes may require to be staged over
    # multiple deploys.
    #
    # Populate the set of "audit_log_kv" key-values as returned to Gitauth clients.
    # These key-values are later propagated into the Audit Log system as part of
    # fulfilling git operations (fetches, pushes).
    #
    #   target - GitAuth::Target representing the repo, wiki, or gist at hand
    #
    #   stats - Ruby dictionary filled with various pre-queried information
    #           like repository name and relevant user login and ID, as populated
    #           with `GitAuth::Access#stats`.
    #
    def self.audit_log_pack_kvs(target, stats)
      result = {}

      result["repository"] = stats[:repo_name] || ""
      result["repository_id"] = stats[:repo_id] || 0

      # TODO: What should actor and actor_id be here, for all cases?
      result["actor"] = ""
      result["actor_id"] = 0

      result["user"] = stats[:user_login] || ""
      result["user_id"] = stats[:user_id] || 0

      repo = target.repository
      if !target.gist? && repo.in_organization?
        org = repo.organization
        result["org"] = org.login
        result["org_id"] = org.id

        if org.business.present?
          business = org.business
          result["business"] = business.slug
          result["business_id"] = business.id
        end
      end

      result
    end

    # SAFELY DEPLOYING CHANGES:
    #
    # If making changes here and in #commit_refs_actor_from_ctx please consider
    # deploying separately. Requests to _git_auth and _commit_refs may happen
    # on different hosts, with different deploys. So changes may need to be
    # staged over two deploys.
    def self.commit_refs_pack_ctx(stats, slumlord_mode, auth_type)
      h = {}
      if stats.key?(:user_id)
        h["user_id"] = stats[:user_id]
      elsif stats.key?(:pubkey_id)
        h["pubkey_id"] = stats[:pubkey_id]
      end
      if stats.key?(:oauth_access_id)
        h["oauth_access_id"] = stats[:oauth_access_id]
      end
      if stats.key?(:installation_id) && stats.key?(:installation_type)
        h["installation_id"] = stats[:installation_id]
        h["installation_type"] = stats[:installation_type]
      end
      h["slumlord"] = "true" if slumlord_mode
      h["auth_type"] = auth_type if auth_type && !h.empty?
      h.to_json
    end

    class NoActorError < StandardError
    end

    # SAFELY DEPLOYING CHANGES:
    #
    # If making changes here and in #commit_refs_pack_ctx please consider
    # deploying separately. Requests to _git_auth and _commit_refs may happen
    # on different hosts, with different deploys. So changes may need to be
    # staged over two deploys.
    #
    # Returns an (actor, auth_type) pair.
    def self.commit_refs_actor_from_ctx(ctx_json)
      ctx = GitHub::JSON.parse(ctx_json)
      auth_type = ctx.key?("auth_type") && ctx["auth_type"].to_sym

      if auth_type == :bot && ctx.key?("installation_id") && ctx.key?("installation_type")
        # Fetching the Bot through the IntegrationInstallation or
        # ScopedIntegrationInstallation hydrates it with the repository
        # installation which is the abilities delegate.
        installation =
          case ctx["installation_type"]
          when "IntegrationInstallation"
            IntegrationInstallation.find(ctx["installation_id"].to_i)
          when "ScopedIntegrationInstallation"
            ScopedIntegrationInstallation.find(ctx["installation_id"].to_i)
          end
        return installation.bot
      elsif ctx.key?("user_id")
        user = User.find(ctx["user_id"].to_i)
        if ctx.key?("oauth_access_id")
          # Re-attach the oauth access that was used to authenticate so that we
          # can check the scopes used later in RefUpdatesPolicy.
          user.oauth_access = OauthAccess.find(ctx["oauth_access_id"].to_i)
        end
        return user
      elsif ctx.key?("pubkey_id")
        return PublicKey.find(ctx["pubkey_id"].to_i)
      elsif ctx.key?("slumlord") && ctx["slumlord"] == "true"
        return :slumlord
      else
        Failbot.push ctx_json: ctx_json.inspect
        raise NoActorError, "context missing installation_id, installation_type, user_id and pubkey_id"
      end
    rescue ActiveRecord::RecordNotFound
      Failbot.push ctx_json: ctx_json.inspect
      raise NoActorError, "record was deleted before lookup could complete"
    end

    # Describe the actor appropriately for logging
    def self.filtered_actor(actor)
      if actor.is_a? User
        "user:#{actor.id}"
      elsif actor.is_a? PublicKey
        "pubkey:#{actor.id}"
      else
        return actor.to_s
      end
    end

    def self.verify_key(request)
      key, fingerprint, ssh_login = request.POST.fetch_values(
        "key",
        "fingerprint",
        "ssh_login",
      )

      org_id = if match = ssh_login.match(/\Aorg-(\d+)\z/)
        match[1].to_i
      end

      if ssh_certificate?(key)
        verify_certificate(key, org_id: org_id, ip: request.ip)
      else
        verify_public_key(fingerprint, key, org_id: org_id)
      end
    end

    # The 'result' return value is a symbol that is either
    # :ok or something else. The something else doesn't
    # actually matter, it's just for debugging purposes,
    # and logging to datadog.
    def self.verify_public_key(fingerprint, key, org_id:)
      pubkey = GitAuth::SSHKey.with_fingerprint(fingerprint)

      if pubkey
        result, message = pubkey.verify(key, ssh_required: ssh_certificate_requirement_enabled_for_org?(org_id))
      else
        result  = :unknown_key
        message = "Unknown SSH Key"
      end
      [result, message]
    ensure
      tags = ["type:pubkey"]
      tags << "member_type:#{pubkey.type}" if pubkey
      tags << "result:#{result}"
      GitHub.dogstats.increment("gitauth.verify_key", tags: tags)
    end

    def self.ssh_certificate_requirement_enabled_for_org?(org_id)
      return false if org_id.nil?

      business_id = ApplicationRecord::Domain::Users.github_sql.value(<<-SQL, org_id: org_id)
        SELECT business_id
        FROM business_organization_memberships
        WHERE organization_id=:org_id
      SQL

      if business_id
        has_ca = ApplicationRecord::Collab.github_sql.value(<<-SQL, org_id: org_id, business_id: business_id)
          SELECT 1
          FROM ssh_certificate_authorities
          WHERE (owner_type='Business' AND owner_id=:business_id)
            OR (owner_type='User' AND owner_id=:org_id)
          LIMIT 1
        SQL
      else
        has_ca = ApplicationRecord::Collab.github_sql.value(<<-SQL, org_id: org_id)
          SELECT 1
          FROM ssh_certificate_authorities
          WHERE owner_type='User' AND owner_id=:org_id
          LIMIT 1
        SQL
      end
      return false unless has_ca

      rows = ApplicationRecord::Domain::ConfigurationEntries.github_sql.hash_results(<<-SQL, org_id: org_id)
          SELECT name, value, final,
          CASE target_type
          WHEN 'global'     THEN 0
          WHEN 'User'       THEN 1
          END AS priority
          FROM configuration_entries
          WHERE (
            (target_type='global' AND target_id=0)
              OR
            (target_type='User' AND target_id=:org_id)
          ) AND (
            name IN ('ssh_enabled', 'ssh_certificate_requirement')
          )
          ORDER BY priority
      SQL
      entries = {}
      rows.each do |row|
        current = entries[row["name"]]
        next if current && [1, "1", true].include?(current["final"])
        entries[row["name"]] = row
      end
      config = entries.each_with_object({}) do |(name, row), config|
        config[name] = row["value"]
      end

      ssh_enabled = config["ssh_enabled"] != "false" # defaults to true
      cert_required = config["ssh_certificate_requirement"] == "true" # defaults to false

      ssh_enabled && cert_required
    end

    def self.verify_certificate(key, org_id:, ip:)
      result, user_login, ca = GitAuth::SSHCertificateAuthority.validate_certificate(key,
        ip: ip,
      )

      if result == :ok && org_id
        exists = ApplicationRecord::Domain::Users.github_sql.value(<<-SQL, org_id: org_id)
          SELECT 1
          FROM users
          WHERE id=:org_id
        SQL

        # We got a hint that the user is trying to access a repo belonging to
        # the org, so we verify that the CA is usable by that org.
        if exists && !ca.owned_by_organization?(org_id)
          result = :wrong_ca
        end
      end

      GitHub.dogstats.increment("gitauth.verify_key", tags: [
        "type:cert",
        "result:#{result}",
      ])

      if result == :ok
        user_id = ApplicationRecord::Domain::Users.github_sql.value(<<-SQL, user_login: user_login)
          SELECT id
          FROM users
          WHERE login = :user_login
        SQL

        [:ok, "user:#{user_id}:#{user_login}"]
      else
        [:not_ok, "ssh_cert_#{result}"]
      end
    end

    def self.ssh_certificate?(key)
      return false if key.nil?
      algo, _, _ = SSHData.key_parts(key)
      SSHData::Certificate::ALGOS.include?(algo)
    rescue SSHData::Error => e
      Failbot.report(e)
      false
    end

    def self.initial_context(request)
      {
        actor_ip: request.ip,
        catalog_service: SERVICE_NAME,
        from: "GitHub::RepoPermissions",
        request_id: Rack::RequestId.get(request.env),
        server_id: Rack::ServerId.get(request.env),
        url: request_url(request),
        method: request.try(:request_method),
      }
    end

    # The absolute URL for a request.
    #
    # Returns a String or nil.
    def self.request_url(request)
      begin
        "#{request.scheme}://#{request.host_with_port}#{request.fullpath}"
      rescue NoMethodError
      end
    end

    def self.span(env)
      # In almost all conditions our rack app should be called with tracer
      # middleware that sets up a span for the request. In cases where we
      # don't (like in tests), we provide this interface so we can have a
      # default.
      env["rack.span"] ||= GitHub.tracer.start_span("request")
    end
  end
end
