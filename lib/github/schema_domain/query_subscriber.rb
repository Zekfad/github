# frozen_string_literal: true

# Instrumentation meant to check for cross-schema domain queries. Checks each
# executed SQL statement.
#
# See https://githubber.com/article/technology/dotcom/schema-domains for context.
class GitHub::SchemaDomain::QuerySubscriber
  attr_reader :raise_errors, :report_errors

  def initialize(raise_errors: false, report_errors: false)
    @raise_errors = raise_errors
    @report_errors = report_errors
  end

  def call(event, start, ending, notifier_id, payload)
    statement_checker.check(queries: [payload[:sql].to_s], report_errors: report_errors)
  end

  def statement_checker
    @statement_checker ||= GitHub::SchemaDomain::StatementChecker.new(
      raise_errors: raise_errors,
    )
  end
end
