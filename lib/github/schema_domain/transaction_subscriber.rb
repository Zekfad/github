# frozen_string_literal: true

# Instrumentation meant to check for cross-schema domain transactions. Checks
# each SQL statement with a transaction, including nested transactions if they
# are present.
#
# In the test environment, the subscriber presumes each test runs in a top-level
# transaction and only checks the nested transactions it contains.
#
# See https://githubber.com/article/technology/dotcom/schema-domains for context.
class GitHub::SchemaDomain::TransactionSubscriber
  attr_reader :ignore_top_level_transaction, :raise_errors, :report_errors

  def initialize(ignore_top_level_transaction: Rails.test?, raise_errors: false, report_errors: false)
    @ignore_top_level_transaction = ignore_top_level_transaction
    @raise_errors = raise_errors
    @report_errors = report_errors
  end

  def call(event, start, ending, notifier_id, payload)
    # Disabling the cop because this is temporary
    report_error_for_statement = if Rails.production? # rubocop:disable GitHub/DoNotBranchOnRailsEnv
      # Do our own 0.1 sampling to be able to enable the feature for 0.1% of calls
      GitHub.flipper[:report_cross_domain_transactions].enabled? && rand(10) < 1
    else
      report_errors
    end

    select_transaction_statements(payload[:queries]).each do |queries|
      statement_checker.check(queries: queries, report_errors: report_error_for_statement)
    end
  end

  def statement_checker
    @statement_checker ||= GitHub::SchemaDomain::StatementChecker.new(
      checking_transactions: true,
      raise_errors: raise_errors,
    )
  end

  private

    def select_transaction_statements(queries)
      statements = queries.map { |query| query[:sql] }

      return [statements] unless ignore_top_level_transaction

      nested_transactions = []
      this_transaction = []
      savepoints_seen = 0
      releases_seen = 0
      statements.each do |stmnt|
        if stmnt =~ /(?<!RELEASE )\bSAVEPOINT\b/i
          savepoints_seen += 1
        end
        if stmnt =~ /\bRELEASE SAVEPOINT\b/i
          releases_seen += 1
        end
        if savepoints_seen > 0
          this_transaction << stmnt
        end
        if releases_seen > savepoints_seen
          raise RuntimeError.new("Mismatch in SAVEPOINT statements and RELEASE statements:\n\n#{statements.join("\n")}")
        end
        if savepoints_seen > 0 && savepoints_seen == releases_seen
          nested_transactions << this_transaction
          this_transaction = []
          savepoints_seen = 0
          releases_seen = 0
        end
      end

      nested_transactions
    end
end
