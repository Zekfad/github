# rubocop:disable Style/FrozenStringLiteralComment

# Queries to see which repo and network replicas need maintenance

require "github/config/mysql"
require "github/config/redis"
require "github/dgit/error"
require "github/dgit/repo_types"
require "github/dgit/states"
require "github/dgit/constants"
require "github/dgit/cold_storage_sql"
require "github/dgit/replication_strategy"
require "github/dgit/util"
require "github/slice_query"

require "set"

module GitHub
  class DGit
    class Maintenance
      include SliceQuery

      # schedule no more than this many jobs per host, per interval
      # this is the default; it can scale based on the value of
      # /etc/github/resque-worker-count on a particular fileserver
      MAX_JOBS_PER_HOST = 100

      # keep no more than this many old pre-repair backups of any network
      MAX_REPAIR_BACKUPS = 4

      # only look for bad checksums in repository_checksums and
      # repository_replicas rows that have been updated this recently.
      BAD_CHECKSUMS_INTERVAL = 60  # minutes

      # The number of randomly selected networks to consider moving off
      # each too-full host.  Use more than you actually want to move,
      # because some may be disqualified by already being on all valid
      # recipient hosts, or by being on the same rack as a recipient
      # host.
      REBALANCING_SAMPLE = 100

      # The minimum age of a newly created replica (which includes entire
      # newly created or newly imported networks) before we're willing to
      # rebalance it to another host.
      # This is specifically to prevent races between creation-ish
      # processes (creating a repo, importing a repo, creating a replica)
      # and deleting a replica.
      MIN_REBALANCING_AGE = 60  # minutes

      # Number of times to attempt to rsync a backup
      # in preparation for a repo or gist repair.
      BACKUP_RSYNC_ATTEMPTS = 3

      # Cap replica counts at this number (generally 9) for the purposes
      # of graphing them.  That is, there will be `dgit.replica-counts.X`
      # metrics for all X in (0..REPLICA_GRAPH_MAX_COPIES).
      REPLICA_GRAPH_MAX_COPIES = GitHub.dgit_copies * 3
      REPLICA_GRAPH_KEYS = ((0..REPLICA_GRAPH_MAX_COPIES).to_a - [GitHub.dgit_copies]).freeze

      def self.network_query_limit
        (GitHub.kv.get(sweeper_size_key("networks")).value { 1000 } || 1000).to_i
      end

      def self.set_network_sweeper_query_limit(limit)
        GitHub.kv.set(sweeper_size_key("networks"), limit.to_s)
      end

      def self.gist_query_limit
        (GitHub.kv.get(sweeper_size_key("gists")).value { 1000 } || 1000).to_i
      end

      def self.set_gist_sweeper_query_limit(limit)
        GitHub.kv.set(sweeper_size_key("gists"), limit.to_s)
      end

      def self.sweeper_size_key(kind)
        "#{query_namespace}:sweeper:#{kind}"
      end

      # Limit used when dumping diagnostics about a network/gist
      def self.dump_query_limit
        1000
      end

      def self.evac_network_query_limit
        (GitHub.kv.get(evac_size_key("networks")).value { 3000 } || 3000).to_i
      end

      def self.set_evac_network_query_limit(limit)

        GitHub.kv.set(evac_size_key("networks"), limit.to_s)
      end

      def self.evac_network_query_nonvoting_limit
        (GitHub.kv.get(evac_size_key("networks:nonvoting")).value { 3000 } || 3000).to_i
      end

      def self.set_evac_network_query_nonvoting_limit(limit)

        GitHub.kv.set(evac_size_key("networks:nonvoting"), limit.to_s)
      end

      def self.evac_gist_query_limit
        (GitHub.kv.get(evac_size_key("gists")).value { 3000 } || 3000).to_i
      end

      def self.set_evac_gist_query_limit(limit)
        GitHub.kv.set(evac_size_key("gists"), limit.to_s)
      end

      def self.evac_gist_query_nonvoting_limit
        (GitHub.kv.get(evac_size_key("gists:nonvoting")).value { 3000 } || 3000).to_i
      end

      def self.set_evac_gist_query_nonvoting_limit(limit)
        GitHub.kv.set(evac_size_key("gists:nonvoting"), limit.to_s)
      end

      def self.evac_size_key(kind)
        "#{query_namespace}:evacuation:#{kind}"
      end

      # General namespace for adjustable queries
      def self.query_namespace
        "dgit"
      end

      # Describes the evacuation strategy which determines the order in which we
      # retrieve networks and gists to be evacuated.
      #
      # Using per-host we retrieve entities for a single host first, which
      # accelerates getting a single server empty quicker (so it e.g. can be
      # reprovisioned and participate in intake). If we have multiple copies, we
      # can spread the read load around by quiescing the evacuating hosts.
      #
      # Useing per-id means that the entities are sorted by their id, covering
      # (ideally) all the evacuating hosts. This is useful in sdc42-sea where we
      # have a single copy so we can spread the load around by evacuating
      # multiple hosts at the same time.
      module EvacuationStrategy
        HOST = :host
        ID   = :id
      end


      # Annotation for automatically tracking timing stats for
      # the MySQL query methods.  For example, to track stats
      # in the "dgit.queries.get_the_foos.time" stat namespace:
      #
      #     def self.get_the_foos(a, b, c, ...)
      #       # wild MySQL queries go here like normal
      #     end
      #     track_query_time :get_the_foos
      #
      def self.track_query_time(query_method_name)
        singleton_class.class_eval do
          orig_method = "#{query_method_name}_untracked"
          alias_method orig_method, query_method_name
          stat_method_name = query_method_name.to_s.sub("?", "")
          define_method query_method_name do |*args|
            start = Time.now
            result = GitHub.dogstats.time("dgit.queries", tags: ["query:#{stat_method_name}"]) do
              if args.last.class == Hash
                kwargs = args.pop
                self.send(orig_method, *args, **kwargs)
              else
                self.send(orig_method, *args)
              end
            end
            GitHub.stats.timing("dgit.queries.#{stat_method_name}.time", Time.now-start) if GitHub.enterprise?
            result
          end
        end
      end

      # Send DGit stats to graphite and initiate resque jobs for any
      # necessary repairs.
      #
      # This is the function we run in timerd.
      #
      # Note that the order here matters, at least partially.
      # - Cleaning up repo replicas with bad checksums can change networks
      #   to the REPAIRING state.
      # - Counting ACTIVE repos should come after these two
      def self.run_stats_and_maintenance(only_network_id: nil)
        ctx = NetworkMaintenanceContext.new

        if !GitHub.enterprise? && only_network_id.nil?
          replicas_for_cleanup = []
        else
          # Count and clean-up stale replicas
          replicas_for_cleanup = get_network_replicas_for_cleanup(only_network_id: only_network_id)
          replicas_for_cleanup.each do |network_id, host, state|
            next unless ctx.ok_to_queue_job?(host, increment: true)
            ctx.enqueue_destroy_replica(network_id, host)
          end
        end

        if !GitHub.enterprise? && only_network_id.nil?
          failed_replicas = []
        else
          # Count and repair FAILED replicas
          failed_replicas = get_failed_network_replicas(only_network_id: only_network_id)
          failed_replicas.each do |network_id, host|
            next unless ctx.ok_to_queue_job?(host, increment: true)
            repair_network_replica(network_id, host)
          end
        end

        # Count and repair replicas with bad checksums
        if !GitHub.enterprise? && only_network_id.nil?
          bad_checksum_replicas = []
        else
          bad_checksum_replicas = get_repo_replicas_with_bad_checksums(only_network_id: only_network_id)
          bad_checksum_replicas.each do |repo_id, network_id, host, repo_type|
            next unless ctx.ok_to_queue_job?(host, increment: true)
            repair_repo_replica(repo_id, network_id, host, repo_type)
          end
        end

        # Count and repair replicas with 'creating' checksums, i.e., repos
        # that never successfully computed a checksum in the first place
        no_checksum_replicas = get_repo_replicas_with_no_checksums(only_network_id: only_network_id)
        no_checksum_replicas.each do |repo_id, repo_type|
          host = ctx.online_voting_hosts.sample
          next unless ctx.ok_to_queue_job?(host, increment: true)
          repair_doa_repo(repo_id, repo_type, host)
        end

        # TODO: should we check read-weights separately?  They'll get
        # fixed by replica creation/deletion jobs, but we can tidy them up
        # much faster with just database operations.

        # Count and repair networks with bad replication counts:
        # - github.dgit.replica-counts.#{count}
        # Must come after get_repo_replicas_with_bad_checksums, because
        # that call can move things from ACTIVE to REPAIRING.
        #
        if GitHub.enterprise? && !only_network_id.nil?
          GitHub::Logger.log_context(maint: "network", phase: "sliced") do
            fix_bad_network_replica_counts(only_network_id: only_network_id, ctx: ctx)
          end
        end

        if !only_network_id
          write_stats_for_count("networks.cleanup",      replicas_for_cleanup.size)
          write_stats_for_count("networks.failed",       failed_replicas.size)
          write_stats_for_count("repos.bad-checksum",    bad_checksum_replicas.size)
          write_stats_for_count("repos.no-checksum",     no_checksum_replicas.size)
        end

        if !only_network_id
          GitHub::Logger.log_context(maint: "network", phase: "rebalance") do
            rebalanced = queue_rebalancing_jobs(ctx)
            write_stats_for_count("networks.rebalance", rebalanced.size)
          end
        end

        ctx.jobs_per_host
      end

      def self.run_network_evacuation
        voting = GitHub.flipper[:dgit_split_evacuation].enabled? ? true : nil
        ctx = NetworkEvacuationMaintenanceContext.new(voting: voting)

        # Find and repair networks with replicas on bad hosts
        bad_host_networks = GitHub::Logger.log_context(maint: "network", phase: "evac") do
          fix_bad_host_replica_counts(ctx: ctx)
        end

        write_stats_for_count("networks.on-bad-hosts", bad_host_networks.size, tags: ["voting:true"])
      end

      def self.run_network_evacuation_nonvoting
        return unless GitHub.flipper[:dgit_split_evacuation].enabled?

        ctx = NetworkEvacuationMaintenanceContext.new(voting: false, evacuate_by: EvacuationStrategy::ID)

        # Find and repair networks with replicas on bad hosts
        bad_host_networks = GitHub::Logger.log_context(maint: "network", phase: "evac") do
          fix_bad_host_replica_counts(ctx: ctx)
        end

        write_stats_for_count("networks.on-bad-hosts", bad_host_networks.size, tags: ["voting:false"])
      end

      def self.run_gist_stats_and_maintenance(only_gist_id: nil)
        ctx = GistMaintenanceContext.new

        # Count and clean-up stale gist replicas
        if !GitHub.enterprise? && only_gist_id.nil?
          gist_replicas_for_cleanup = []
        else
          gist_replicas_for_cleanup = get_gist_replicas_for_cleanup(only_gist_id: only_gist_id)
          gist_replicas_for_cleanup.each do |gist_id, host, state|
            next unless ctx.ok_to_queue_job?(host, increment: true)
            ctx.enqueue_destroy_replica(gist_id, host)
          end
        end

        if !GitHub.enterprise? && only_gist_id.nil?
          failed_gist_replicas = []
        else
          # Count and repair FAILED gist replicas
          failed_gist_replicas = get_failed_gist_replicas(only_gist_id: only_gist_id)
          failed_gist_replicas.each do |gist_id, host|
            next unless ctx.online_hosts.include? host
            next unless ctx.ok_to_queue_job?(host, increment: true)
            repair_gist_replica(gist_id, host)
          end
        end

        # Count and repair gist replicas with bad checksums
        if !GitHub.enterprise? && only_gist_id.nil?
          bad_checksum_gist_replicas = []
        else
          bad_checksum_gist_replicas = get_gist_replicas_with_bad_checksums(only_gist_id: only_gist_id)
          bad_checksum_gist_replicas.each do |gist_id, host|
            next unless ctx.ok_to_queue_job?(host, increment: true)
            repair_gist_replica(gist_id, host)
          end
        end

        # Count and repair replicas with 'creating' checksums, i.e., gists
        # that never successfully computed a checksum in the first place
        no_checksum_replicas = get_gist_replicas_with_no_checksums(only_gist_id: only_gist_id)
        no_checksum_replicas.each do |gist_id|
          host = ctx.online_voting_hosts.sample
          next unless ctx.ok_to_queue_job?(host, increment: true)
          repair_doa_gist(gist_id, host)
        end

        # TODO: should we check read-weights separately?  They'll get
        # fixed by replica creation/deletion jobs, but we can tidy them up
        # much faster with just database operations.

        # Count and repair gists with bad replication counts:
        # - github.dgit.gist-replica-counts.#{count}
        if GitHub.enterprise? && !only_gist_id.nil?
          GitHub::Logger.log_context(maint: "gist", phase: "sliced") do
            fix_bad_gist_replica_counts(only_gist_id: only_gist_id, ctx: ctx)
          end
        end

        if !only_gist_id
          write_stats_for_count("gists.cleanup",      gist_replicas_for_cleanup.size)
          write_stats_for_count("gists.failed",       failed_gist_replicas.size)
          write_stats_for_count("gists.bad-checksum", bad_checksum_gist_replicas.size)
          write_stats_for_count("gists.no-checksum",  no_checksum_replicas.size)
        end

        ctx.jobs_per_host
      end

      def self.run_gist_evacuation
        voting = GitHub.flipper[:dgit_split_evacuation].enabled? ? true : nil
        ctx =  GistEvacuationMaintenanceContext.new(voting: voting)

        # Find and repair gists with replicas on bad hosts
        bad_host_gists = GitHub::Logger.log_context(maint: "gist", phase: "evac", voting: ctx.voting_s) do
          fix_bad_host_gist_replica_counts(ctx: ctx)
        end

        write_stats_for_count("gists.on-bad-hosts", bad_host_gists.size, tags: ["voting:#{ctx.voting_s}"])
      end

      def self.run_gist_evacuation_nonvoting
        return unless GitHub.flipper[:dgit_split_evacuation].enabled?

        ctx =  GistEvacuationMaintenanceContext.new(voting: false, evacuate_by: EvacuationStrategy::ID)

        # Find and repair gists with replicas on bad hosts
        bad_host_gists = GitHub::Logger.log_context(maint: "gist", phase: "evac", voting: ctx.voting_s) do
          fix_bad_host_gist_replica_counts(ctx: ctx)
        end

        write_stats_for_count("gists.on-bad-hosts", bad_host_gists.size, tags: ["voting:#{ctx.voting_s}"])
      end

      # Make sure all the objects identified as having zero replicas
      # actually still exist.
      def self.ensure_still_exists(counts, table, query)
        zeroes = counts.select { |id, count, _, _| count == 0 }.map { |id, _, _, _| id }
        existing_ids = zeroes.empty? ? [] : ActiveRecord::Base.connected_to(role: :reading) do
          sql = query.new(<<-SQL, table: GitHub::SQL.LITERAL(table), ids: zeroes)
            SELECT id FROM :table WHERE id IN :ids
          SQL
          sql.results.flatten
        end
        counts.select { |id, count, _, _| count > 0 || existing_ids.include?(id) }
      end

      def self.report_zeroes(klass, description, table, counts, fixed: [])
        zeroes = counts.select { |_, count, _, _| count == 0 }.reject { |id, _, _| fixed.include?(id) }
        return unless zeroes.any?

        voting, nonvoting = zeroes.partition { |_, _, _, info| info[:voting] }
        error_info = {
          zero_voting_replicas_count: voting.size,
          zero_nonvoting_replicas_count: nonvoting.size,
          zero_voting_replicas_ids: voting.take(2000).map { |id, _, _| id },
          zero_nonvoting_replicas_ids: nonvoting.take(2000).map { |id, _, _| id },
        }
        error_info[:app] = "github-dgit-debug" if voting.empty?
        Failbot.report(klass.new("#{description} with zero replicas"), error_info)
      end

      # Create `count` replicas of a network or gist (depending on provided context)
      def self.create_replicas(id, count, old_replicas = nil, helper = FixVotingReplicaCounts.new(nil), ctx:)
        old_replicas ||= ctx.all_replicas(id)
        ok_replicas = old_replicas.select(&:healthy?)
        raise ReplicaCreateError, "No healthy source host: #{old_replicas.inspect}" if ok_replicas.empty?
        # We reject anything which is not healthy to avoid races. There is some
        # potential to optimize this so we only reject the close race of
        # DESTROYING.
        same_votingness_replicas = helper.filter_replicas(ok_replicas)
        evac_hosts = same_votingness_replicas.select(&:evacuating?).map(&:host)
        new_hosts = GitHub.dogstats.time("dgit.maintenance.pick_hosts",
                                         tags: ["maint:#{ctx.entity}"]) do
          helper.pick_hosts(count, old_replicas)
        end

        new_hosts.each do |host|
          if (evac = evac_hosts.shift)
            ctx.enqueue_move_replica(id, evac, host)
          else
            ctx.enqueue_create_replica(id, host)
          end
        end
      end

      def self.create_gist_replica(gist_id)
        create_replicas(gist_id, 1, ctx: GistMaintenanceContext.new)
      end

      # Queue a job to destroy one replica of a network.
      #   - network_id = the network whose replica to destroy
      #   - host       = which replica to destroy
      def self.destroy_network_replica(network_id, host, ctx: NetworkMaintenanceContext.new)
        ctx.enqueue_destroy_replica(network_id, host)
      end

      # Queue a job to destroy one replica of a gist.
      #   - gist_id = the gist whose replica to destroy
      #   - host    = which replica to destroy
      def self.destroy_gist_replica(gist_id, host, ctx: GistMaintenanceContext.new)
        ctx.enqueue_destroy_replica(gist_id, host)
      end

      # Queue a job to repair one replica of a repository.  This will fix
      # all objects, refs, and hard state within a single repo, but not
      # touch the rest of the repo's network.  Do this to correct a single
      # bad checksum.
      #   - repo_id         = the repo whose replica to repair
      #   - network_id      = the network that repo belongs to
      #   - host            = which replica to repair
      #   - repo_type       = type of repo
      def self.repair_repo_replica(repo_id, network_id, host, repo_type)
        is_wiki = (repo_type == GitHub::DGit::RepoType::WIKI)
        SpokesRepairRepoReplicaJob.set(queue: "maint_#{host}").perform_later(repo_id, network_id, host, is_wiki)
      end

      # Queue a job to repair one repository with checksum stuck at 'creating'.
      #   - repo_id         = the repo whose replica to repair
      #   - repo_type       = type of repo
      #   - host            = an arbitrarily chosen host to drive the repair
      def self.repair_doa_repo(repo_id, repo_type, host)
        is_wiki = (repo_type == GitHub::DGit::RepoType::WIKI)
        SpokesRepairDoaRepoJob.set(queue: "maint_#{host}").perform_later(repo_id, is_wiki)
      end

      # Queue a job to repair one gist with checksum stuck at 'creating'.
      #   - gist_id         = the gist whose replica to repair
      #   - host            = an arbitrarily chosen host to drive the repair
      # Unlike DOA repos, for gists we only enqueue a job to recompute the
      # checksums, in the hopes that a majority will have been created enough
      # to compute checksums and will have checksums that agree.  In practice,
      # that seems to be the case.
      def self.repair_doa_gist(gist_id, host)
        SpokesRecomputeGistChecksumsJob.set(queue: "maint_#{host}").perform_later(gist_id, :vote)
      end

      # Queue a job to repair one replica of a network.  This will fix
      # all repos within the network.  This is somewhat more expensive,
      # but more comprehensive, than repairing just a single repo.  Do
      # this when a network is in the FAILED state, e.g. after an
      # unexpected 3pc error.
      #   - network_id = the network whose replica to repair
      #   - host       = which replica to repair
      def self.repair_network_replica(network_id, host)
        SpokesRepairNetworkReplicaJob.set(queue: "maint_#{host}").perform_later(network_id, host)
      end

      # Queue a job to repair one replica of a gist.  Do this when
      # a gist is in the FAILED state, e.g. after an unexpected 3pc
      # error.
      #   - gist_id = the gist whose replica to repair
      #   - host    = which replica to repair
      def self.repair_gist_replica(gist_id, host)
        SpokesRepairGistReplicaJob.set(queue: "maint_#{host}").perform_later(gist_id, host)
      end

      # Run all the maintenance jobs (like `run_stats_and_maintenance`),
      # but only for a single network.  This is a useful thing to do if
      # you have reason to believe a network -- or a repo within a network
      # -- is broken and you don't want to wait two minutes for the main
      # job to figure it out.
      def self.check_one_network(network_id)
        SpokesMaintenanceSchedulerJob.set(queue: :dgit_repairs).perform_later(network_id)
      end

      # Provide the checksum (and update time) on all replicas for all
      # repos within a network.
      #
      # Returns a hash mapping: {[repo_id, repo_type] => {host => {checksum: checksum, updated_at: time}}}
      def self.checksums_for_network(network_id)
        forks = GitHub::DGit::Util.repo_ids_for(network_id, include_deleted: false)
        checksums = { }
        return checksums if forks.empty?
        ActiveRecord::Base.connected_to(role: :reading) do
          sql = GitHub::DGit::DB.for_network_id(network_id).SQL.new(<<-SQL, network_id: network_id, forks: forks)
            SELECT repository_id, repository_type, host, updated_at, checksum FROM repository_replicas WHERE repository_id IN :forks
          SQL
          sql.results.each do |repo_id, repo_type, host, updated_at, checksum|
            checksums[[repo_id, repo_type]] ||= { }
            checksums[[repo_id, repo_type]][host] = { updated_at: updated_at, checksum: checksum }
          end
        end
        checksums
      end

      # Recompute the checksum on all replicas, and make sure there's
      # still a repair needed.  The repository's expected checksum is
      # updated to whatever checksum gets computed on the primary-reader
      # node.  We don't want to invalidate all routes.
      #
      # Returns a hash mapping { host => checksum }, plus a bonus
      # { :repo => repo_expected_checksum }
      #
      # recompute_checksums will run to completion with hosts unavailable,
      # as long as the designated read_host can compute the checksum.  The
      # returned hash will be missing keys for any host that was
      # unavailable.
      #
      # Note that this operation grabs the dgit lock on each node, but not
      # all at once like a 3PC writer.  It's possible that some other
      # update will sneak in between the updates, leaving replicas out of
      # sync.  The primary-reader will have the "right" checksum, and one
      # or both of the others may be out of sync.  Maintenance jobs will
      # clean that up, though in the worst case, we could roll back a push
      # we just told the user we accepted.
      def self.recompute_checksums(repo, read_host, is_wiki: false, force_dgit_init: false)
        # If the repo already has some sort of checksums, use 3PC to
        # recompute them.  If it doesn't -- e.g., it has "creating" or
        # "bad" -- then 3PC won't work, and we have to use `dgit-state
        # init`.  Fortunately, if the checksum is "creating," there's no
        # possibility we'll race against a successful 3PC, so `dgit-state
        # init` is safe.
        if !force_dgit_init
          if is_wiki
            wc = GitHub::DGit::Routing.wiki_checksum(repo.network.id, repo.id)
            if wc =~ /\A\d+:[0-9a-f]{10,}\z/
              res = GitHub.dogstats.time("dgit.recompute_checksums", tags: ["type:wiki", "method:3pc"]) do
                repo.unsullied_wiki.recompute_checksums(read_host, ignore_dissent: true) # XXX: See FIXME in Repository#recompute_checksums.
              end
              return log_unanimity(:wiki, res)
            end
          else
            rc = GitHub::DGit::Routing.repo_checksum(repo.network.id, repo.id)
            if rc =~ /\A\d+:[0-9a-f]{10,}\z/
              res = GitHub.dogstats.time("dgit.recompute_checksums", tags: ["type:repo", "method:3pc"]) do
                repo.recompute_checksums(read_host, ignore_dissent: true) # XXX: See FIXME in Repository#recompute_checksums.
              end
              return log_unanimity(:repo, res)
            end
          end
        end

        ret = {}
        GitHub.dogstats.time("dgit.recompute_checksums", tags: ["type:#{is_wiki ? "wiki" : "repo"}", "method:rpc"]) do
          replicas = GitHub::DGit::Routing.all_repo_replicas(repo.id, is_wiki).select(&:online?).reject(&:dormant?).reject(&:destroying?)
          return {} if replicas.empty?

          # You might wonder, "Why look up the repository in the database?" Good question!
          # In RepositoryNetwork#new_extract, the dup'd repository's network_id gets changed
          # before the routes get changed in the database. This means that all_repo_replicas
          # loads the old routes, even though repo.original_shard_path includes the new
          # network ID. In other words, we'd end up looking for the new replicas on the
          # old fileservers. Previously, this code only operated on the union of the
          # (new) network's hosts and the (old) repository's hosts, and it used the old
          # network_id because of the way it created the GitRPC clients. So that all worked
          # out OK. Now that we're using repo.original_shard_path to find the repository on
          # disk, we need to have the same network ID that was used for the routes. This
          # new code assumes that the value of repo.network_id (in the database) matches
          # the network that the repository shares routes with. I think that's a valid
          # assumption.
          repo_for_shard_path = Repository.find(repo.id)
          repo_for_shard_path = repo_for_shard_path.unsullied_wiki if is_wiki
          shard_path = repo_for_shard_path.original_shard_path

          replicas_by_url = Hash[replicas.map { |replica| [replica.to_route(shard_path).rpc_url, replica] }]
          answers, errors = ::GitRPC.send_multiple(replicas_by_url.keys, :dgit_state_init, [DGIT_CURRENT_CHECKSUM_VERSION])

          replicas_by_url.each do |url, replica|
            if checksum = answers[url]
              ret[replica.host] = checksum
              ret[:repo] = checksum if (replica.host == read_host)
            else
              e = errors[url]
              if e.is_a?(::GitRPC::DGitStateInitError) || e.is_a?(::GitRPC::InvalidRepository)
                orig_err = e
                e = ChecksumInitError.new("Could not dgit-state on #{replica.host}: #{orig_err}")
                # This replicates what we'd normally do inside the GitRPC client code
                backtrace = orig_err.backtrace || []
                backtrace += ["---- THE WIRE ----"] + caller
                e.set_backtrace(backtrace)
              end
              raise e if replica.host == read_host
              Failbot.report!(e, app: "github-dgit-debug", spec: repo.dgit_spec, checksums: ret, read_host: read_host)
            end
          end

          if read_host == :vote
            winner = GitHub::DGit::Util.get_majority(voter_checksums(ret))
            raise ChecksumInitError, "No clear majority; votes=#{ret.inspect}" unless winner
            ret[:repo] = winner
          elsif read_host == :unanimous
            winner = GitHub::DGit::Util.get_unanimous(voter_checksums(ret))
            raise ChecksumInitError, "No unanimous checksum; votes=#{ret.inspect}" unless winner
            ret[:repo] = winner
          end

          log_unanimity(is_wiki ? :wiki : :repo, ret)
        end
      ensure
        if ret
          GitHub::DGit::Delegate.update_checksums(repo.network.id, repo.id, is_wiki, ret.reject { |k, v| k==:repo }, ret[:repo])
        end
      end

      # Make sure that all of the forks in a network have replicas that match the network,
      # and that all forks in a network have a checksum record.
      def self.sync_network_replicas(network_id)
        db = GitHub::DGit::DB.for_network_id(network_id)
        forks = GitHub::DGit::Util.repo_ids_for(network_id, include_deleted: false)
        return if forks.empty?

        network_hosts = GitHub::DGit::Routing.all_hosts_for_network(network_id)
        return if network_hosts.empty? # not in dgit?

        fork_hosts = db.SQL.results(<<-FORK_HOSTS, network_id: network_id, repo_ids: forks).group_by(&:first)
          SELECT repository_id, repository_type, host FROM repository_replicas WHERE repository_id IN :repo_ids
        FORK_HOSTS
        fork_checksums = db.SQL.results(<<-FORK_CHECKSUMS, network_id: network_id, repo_ids: forks, repoish: GitHub::DGit::RepoType::REPO_ISH).group_by(&:first)
          SELECT repository_id, repository_type FROM repository_checksums WHERE repository_id IN :repo_ids AND repository_type IN :repoish
        FORK_CHECKSUMS

        forks.each do |fork_id|
          repo_checksum_rows, wiki_checksum_rows = (fork_checksums[fork_id] || []).partition { |row| row[1] == GitHub::DGit::RepoType::REPO }
          repo_rows, wiki_rows = (fork_hosts[fork_id] || []).partition { |row| row[1] == GitHub::DGit::RepoType::REPO }
          repo_hosts = repo_rows.map { |row| row[2] }.sort
          wiki_hosts = wiki_rows.map { |row| row[2] }.sort
          ok = true
          ok = ok && repo_checksum_rows.size == 1
          ok = ok && repo_hosts == network_hosts
          unless wiki_checksum_rows.empty? && wiki_rows.empty?
            ok = ok && wiki_checksum_rows == 1
            ok = ok && wiki_hosts == network_hosts
          end
          if !ok && repo = Repository.find_by_id(fork_id)
            repo.sync_routes_from_network
          end
        end
      end

      # Filter a checksum-results hash to include voting hosts only
      #   hash: a Hash of { host => checksum, host => checksum }
      #   returns: a Hash with the same layout, but all keys correspond to
      #            voting hosts
      def self.voter_checksums(hash)
        voting_hosts = Set.new(GitHub::DGit.get_hosts(voting_only: true, storage_class: :all))
        hash.select { |k, v| voting_hosts.include?(k) }
      end

      # Recompute checksums, and suppress any DGit exceptions
      def self.safely_recompute_checksums(repo, read_host, is_wiki: false)
        recompute_checksums(repo, read_host, is_wiki: is_wiki)
      rescue GitHub::DGit::Error => e
        raise if Rails.test?
        Failbot.report!(e, app: "github-dgit-debug", spec: repo.dgit_spec)
      end

      # Recompute the checksum on all replicas for the given gist.
      # See also `recompute_checksums`.
      def self.recompute_gist_checksums(gist, read_host, store_to_sql: true, fileservers: nil, host_to_activate: nil)
        ret = {}

        # Get all online hosts for the given gist regardless of checksum
        # on each host.
        #
        # We have to deal with a nil `gist.id` at creation time, when the
        # gist object hasn't been saved yet and has neither an ID nor a
        # row in the database.  `all_gist_replicas` depends on state in
        # the database, so when `gist.id` is nil, we use the unsaved state
        # to fetch the host list instead.
        GitHub.dogstats.time("dgit.recompute_checksums", tags: ["type:gist", "method:rpc"]) do
          replica_by_host = {}
          routes =
            if fileservers
              fileservers.map { |fs| fs.to_route(gist.original_shard_path) }
            elsif gist.id
              replicas = GitHub::DGit::Routing.all_gist_replicas(gist.id).select(&:online?).reject(&:dormant?).reject(&:destroying?)
              replicas.each { |rep| replica_by_host[rep.host] = rep }

              replicas.map { |rep| rep.to_route(gist.original_shard_path) }
            else
              gist.rpc.backend.delegate.get_write_routes
            end

          hosts_by_url = Hash[routes.map { |route| [route.rpc_url, route.original_host] }]
          answers, errors = ::GitRPC.send_multiple(hosts_by_url.keys, :dgit_state_init, [DGIT_CURRENT_CHECKSUM_VERSION])

          hosts_by_url.each do |url, host|
            if checksum = answers[url]
              ret[host] = checksum
              ret[:repo] = checksum if (host == read_host)
            else
              e = errors[url]
              if e.is_a?(::GitRPC::DGitStateInitError) || e.is_a?(::GitRPC::InvalidRepository)
                orig_err = e
                e = ChecksumInitError.new("Could not dgit-state on #{host}: #{e}")
                # This replicas what we'd normally do inside the GitRPC client code
                backtrace = orig_err.backtrace || []
                backtrace += ["---- THE WIRE ----"] + caller
                e.set_backtrace(backtrace)
              end
              if host == read_host
                if e.is_a?(ChecksumInitError) && DGit::Util.is_evacuating?(read_host)
                  read_host = :vote
                else
                  raise e
                end
              end
              # Some replica may be in a CREATING state due to a concurrent job
              # (e.g. there are evacuations happening). In that case it's not
              # interesting that such a replica failed to generate a checksum
              # and it can generate quite a bit of noise.
              if replica_by_host[host]&.state != GitHub::DGit::CREATING
                Failbot.report!(e, app: "github-dgit-debug", spec: gist.dgit_spec, checksums: ret, read_host: read_host)
              end
            end
          end

          if read_host == :vote
            winner = GitHub::DGit::Util.get_majority(voter_checksums(ret))
            raise ChecksumInitError, "No clear majority; votes=#{ret.inspect}" unless winner
            ret[:repo] = winner
          elsif read_host == :unanimous
            winner = GitHub::DGit::Util.get_unanimous(voter_checksums(ret))
            raise ChecksumInitError, "No unanimous checksum; votes=#{ret.inspect}" unless winner
            ret[:repo] = winner
          end

          log_unanimity(:gist, ret)
        end
      ensure
        if store_to_sql
          GitHub::DGit::Delegate.update_gist_checksums(gist.id,
                                                       ret.reject { |k, _| k == :repo },
                                                       ret[:repo],
                                                       host_to_activate: host_to_activate)
        end
      end

      def self.log_unanimity(type, checksums)
        raise "unknown checksum type #{type}" unless [:wiki, :repo, :gist].include? type
        uniq = checksums.values.uniq.size
        GitHub.dogstats.increment("dgit.recompute_checksums.uniq", tags: ["type:#{type}", "uniq:#{uniq}"])
        checksums
      end

      def self.init_gist_checksums(gist)
        recompute_gist_checksums(gist, :vote, store_to_sql: false)
      end

      # Change the state of a network_replica
      # network_id - the network ID of the replica to change
      # host - the hostname (e.g. github-dfs-1122334) of the replica to change
      # new_state - the new state of the replica (one of the scalar values in lib/github/dgit/states.rb)
      # prior_state - if provided, only change the state of the replica if the state in the database
      #               matches this prior state. This value may be an array, if prior_state is allowed
      #               to be one of several values.
      def self.set_network_state(network_id, host, new_state, prior_state = nil, update_time = true, ctx: NetworkMaintenanceContext.new)
        ctx.set_replica_state(network_id, host, new_state, prior_state, update_time)
      end

      def self.set_gist_state(gist_id, host, new_state, prior_state = nil, update_time = true, ctx: GistMaintenanceContext.new)
        ctx.set_replica_state(gist_id, host, new_state, prior_state, update_time)
      end

      def self.rebalance_read_weight(network_id, new_host: nil, replicas: nil, ctx: NetworkMaintenanceContext.new)
        ctx.rebalance_read_weight(network_id, new_host: new_host, replicas: replicas)
      end

      def self.rebalance_gist_read_weight(gist_id, replicas: nil, ctx: GistMaintenanceContext.new)
        ctx.rebalance_read_weight(gist_id, replicas: replicas)
      end

      # Delete the given gist's bits from disk for the given replica.
      #   - gist_id   - gist ID, used for logging
      #   - replica   - replica object for the given gist
      #   - path      - gist's original_shard_path
      def self.delete_gist_from_disk_on_replica(gist_id, replica, path)
        host = replica.host

        original_shard_path = path
        path = GitHub::DGit.dev_route(path, host) if Rails.development? || Rails.test?

        # the final 36 in this regex stems from Gist::random_sha -> SecureRandom.hex(16).
        v3_pattern = %r{\A#{GitHub.repository_root}/(?:dgit\d/)?[0-9a-f]/[0-9a-f]{2}/[0-9a-f]{2}/[0-9a-f]{2}/gist/[0-9a-f]{32}.git\z}
        # the final 20 in this regex stems from Gist::random_sha -> SecureRandom.hex(10).
        v2_pattern = %r{\A#{GitHub.repository_root}/(?:dgit\d/)?[0-9a-f]/[0-9a-f]{2}/[0-9a-f]{2}/[0-9a-f]{2}/gist/[0-9a-f]{20}.git\z}
        # before the SecureRandom scheme (v2), a decimal database ID number would be the final gist path component
        v1_pattern = %r{\A#{GitHub.repository_root}/(?:dgit\d/)?[0-9a-f]/[0-9a-f]{2}/[0-9a-f]{2}/[0-9a-f]{2}/gist/[0-9]+.git\z}

        unless (path =~ v1_pattern) || (path =~ v2_pattern) || (path =~ v3_pattern)
          raise GitHub::DGit::ReplicaDestroyError, "Refusing to delete unexpected gist path #{path.inspect}"
        end

        cmd = ["rm", "-rf", path]  # woof

        begin
          GitHub::Logger.log(fn: "fs_delete", path: path) do
            if replica.online?
              rpc = replica.fileserver.build_maint_rpc(original_shard_path)
              rpc.fs_delete(path)
            end
          end
        rescue ::GitRPC::InvalidRepository, ::GitRPC::ConnectionError, SocketError
          # Just a best effort
        end
      end

      # Back up a repository network or gist directory before beginning a repair.
      #   - rpc - an RPC handle for a given repository, network, or gist replica..
      #   - jobclass - the class of the caller
      def self.backup_before_repair_with_rpc(rpc, jobclass)
        src = rpc.backend.path
        parent = src.sub(%r{/\d+(?:\.wiki)?\.git/?$}, "").chomp("/")
        dest = "#{parent}.backup.#{Time.now.to_i}.#{jobclass.to_s.split('::').last}"
        excludes = %w[objects network.git/objects */audit_log audit_log]

        (1..BACKUP_RSYNC_ATTEMPTS).each do |backup_attempt|
          res = rpc.rsync("#{src}/", dest, archive: true, exclude: excludes)

          # Leave the loop if we succeeded
          break if res["ok"]

          # Fail silently if the directory is missing
          return if res["err"] =~ /no such file/i

          if backup_attempt == BACKUP_RSYNC_ATTEMPTS ||
              !GitHub::DGit::RETRYABLE_RSYNC_RESULTS.include?(res["status"])
            raise CommandError, "could not make a backup: #{res['err']}"
          end

          $stderr.puts "Backup attempt #{backup_attempt} of source #{src} failed with retryable result #{res['status']}"
        end

        rpc.prune_backups(parent, MAX_REPAIR_BACKUPS).each do |path, errstr|
          $stderr.puts "Could not delete backup #{path}: #{errstr}"
        end
      end

      # Insert placeholder network_replica rows into the database
      # for the given network ID and set of fileservers.
      def self.insert_placeholder_network_replicas(network_id, dgit_fileservers)
        allocator = GitHub::DGit::ReadWeightAllocator.new
        host_weights = allocator.choose_read_weights(allocator.new_replicas(dgit_fileservers))

        sql_now = GitHub::SQL::NOW
        nr_rows = []
        dgit_fileservers.each do |fs|
          nr_rows << [network_id, fs.name, GitHub::DGit::ACTIVE, host_weights.fetch(fs.name), sql_now, sql_now]
        end

        db = GitHub::DGit::DB.for_network_id(network_id)
        db.transaction do
          db.SQL.run(<<-SQL, network_id: network_id, nr_rows: GitHub::SQL::ROWS(nr_rows))
            INSERT INTO network_replicas
              (network_id, host, state, read_weight, created_at, updated_at)
            VALUES :nr_rows
          SQL
        end # transaction
      end

      # Insert placeholder repository_replica and repository_checksum rows into
      # the database for a given repository or wiki.
      #   - repo_type  - one of GitHub::DGit::RepoType::{REPO,WIKI}
      #   - repo_id    - repository ID for the repo or wiki
      #   - network_id - network ID for the repo or wiki, used to lookup hosts
      #                  by way of existing network_replica entries
      def self.insert_placeholder_replicas_and_checksums(repo_type, repo_id, network_id)
        sql_now = GitHub::SQL::NOW
        network_replica_hosts_and_ids = GitHub::DGit::Routing.all_network_replicas(network_id)
                                .map { |nr| [nr.fileserver.name, nr.db_network_replica_id] }

        rr_rows = []
        network_replica_hosts_and_ids.each do |nr_host, nr_id|
          rr_rows << [repo_id, repo_type, nr_id, nr_host, "creating", sql_now, sql_now]
        end
        checksum_row = [repo_id, repo_type, "creating", sql_now, sql_now]

        db = GitHub::DGit::DB.for_network_id(network_id)
        ActiveRecord::Base.connected_to(role: :writing) do
          db.transaction do
            sql = db.SQL.new \
              network_id: network_id,
              rr_rows: GitHub::SQL::ROWS(rr_rows)
            sql.add <<-SQL
              INSERT INTO repository_replicas
                (repository_id, repository_type, network_replica_id, host, checksum, created_at, updated_at)
              VALUES :rr_rows
            SQL
            sql.run

            sql = db.SQL.new \
              network_id: network_id,
              checksum_row: checksum_row
            sql.add <<-SQL
              INSERT INTO repository_checksums
                (repository_id, repository_type, checksum, created_at, updated_at)
              VALUES :checksum_row
            SQL
            sql.run
          end # transaction
        end # connected_to
      end

      # Allocate weights for the given placeholder gist fileservers.
      def self.allocate_placeholder_gist_weights(gist_fileservers)
        allocator = GitHub::DGit::ReadWeightAllocator.new
        gist_host_weights = allocator.choose_read_weights(allocator.new_replicas(gist_fileservers))
      end

      # Insert placeholder gist_replica and repository_checksum rows into
      # the database for a given gist.  Used for fulfilling gist creation.
      # Read weights for the given fileservers are computed inline.
      #   - gist_id            - gist ID for the gist
      #   - gist_fileservers   - list of DGit::Fileserver objects for which to
      #                          insert placeholder gist_replica records
      #   - checksum           - optional initial checksum contents, defaults
      #                          to the value of "creating"
      def self.insert_placeholder_gist_replicas_and_checksums(gist_id, gist_fileservers, checksum = "creating")
        sql_now = GitHub::SQL::NOW
        state = GitHub::DGit::ACTIVE
        repo_type = GitHub::DGit::RepoType::GIST

        gist_hosts = gist_fileservers.map(&:name)
        gist_host_weights = allocate_placeholder_gist_weights(gist_fileservers)

        gr_rows = []
        gist_hosts.each do |host|
          gr_rows << [gist_id, host, checksum, state, gist_host_weights[host], sql_now, sql_now]
        end
        checksum_row = [gist_id, repo_type, checksum, sql_now, sql_now]

        ActiveRecord::Base.connected_to(role: :writing) do
          gist_db = GitHub::DGit::DB.for_gist_id(gist_id)
          gist_db.transaction do
            gist_db.SQL.run(<<-SQL, gist_id: gist_id, gr_rows: GitHub::SQL::ROWS(gr_rows))
              INSERT INTO gist_replicas
                (gist_id, host, checksum, state, read_weight, created_at, updated_at)
              VALUES :gr_rows
            SQL

            gist_db.SQL.run(<<-SQL, gist_id: gist_id, checksum_row: checksum_row)
              INSERT INTO repository_checksums
                (repository_id, repository_type, checksum, created_at, updated_at)
              VALUES :checksum_row
            SQL
          end # transaction
        end # connected_to
      end

      # Insert a single placeholder gist_replica and repository_checksum row
      # into the database for a given gist, for the purpose of a restore.
      #   - gist_id            - gist ID for the gist
      #   - gist_fileserver    - destination fileserver to where a gist backup
      #                          is being restored
      def self.insert_restore_placeholder_gist_replica_and_checksum(gist_id, gist_fileserver)
        insert_placeholder_gist_replicas_and_checksums(gist_id, [gist_fileserver], "restoring")
      end

      # Insert a single placeholder repository_replica and repository_checksum
      # row into the database for a given repo or wiki, for the purpose of an
      # Enterprise restore.  A single placeholder network_replica row will also
      # be inserted for the case that there is no currently matching
      # network_replica entry for dest_fs.
      #   - repo_id            - repo ID for the repo or wiki
      #   - nw_id              - network ID for the repo or wiki
      #   - is_wiki            - whether this is a wiki
      #   - dest_fs            - destination fileserver to where the repo or
      #                          wiki is being restored
      def self.insert_restore_placeholder_replica_and_checksum(repo_id, nw_id, is_wiki, dest_fs)
        repo_type = is_wiki ? GitHub::DGit::RepoType::WIKI : GitHub::DGit::RepoType::REPO
        sql_now = GitHub::SQL::NOW

        existing_nr = GitHub::DGit::Routing.network_replica_for_host(nw_id, dest_fs.name)
        network_replica_id = existing_nr.nil? ? nil : existing_nr.db_network_replica_id

        checksum_row = [repo_id, repo_type, "restoring", sql_now, sql_now]

        db = GitHub::DGit::DB.for_network_id(nw_id)
        ActiveRecord::Base.connected_to(role: :writing) do
          db.transaction do
            if existing_nr.nil?
              sql = db.SQL.new \
                network_id: nw_id,
                nr_row: [nw_id, dest_fs.name, GitHub::DGit::ACTIVE, 100, sql_now, sql_now]
              sql.add <<-SQL
                INSERT INTO network_replicas
                  (network_id, host, state, read_weight, created_at, updated_at)
                VALUES :nr_row
              SQL
              sql.run
              network_replica_id = sql.last_insert_id
            end

            rr_row = [repo_id, repo_type, network_replica_id, dest_fs.name, "restoring", sql_now, sql_now]

            sql = db.SQL.new \
              network_id: nw_id,
              rr_row: rr_row
            sql.add <<-SQL
              INSERT INTO repository_replicas
                (repository_id, repository_type, network_replica_id, host, checksum, created_at, updated_at)
              VALUES :rr_row
            SQL
            sql.run

            sql = db.SQL.new \
              network_id: nw_id,
              checksum_row: checksum_row
            sql.add <<-SQL
              INSERT INTO repository_checksums
                (repository_id, repository_type, checksum, created_at, updated_at)
              VALUES :checksum_row
            SQL
            sql.run
          end # transaction
        end # connected_to
      end

      ####################################################################
      # Below here are internal routines.  They are public so tests and
      # scripts can get to them, but you're unlikely to need them.

      # The maximum time, in seconds, that a network replica can be in a
      # transient (creating, destroying, or repairing) state.
      MAX_TRANSIENT_TIME = 10800

      # Get all network replicas that have been in a transient state for
      # too long.  Caller decides what to do with them: e.g., destroy a
      # timed-out creation or repair, and restart a timed-out destroy.
      #
      # Return value is rows like [network_id, host, state].
      def self.get_network_replicas_for_cleanup(ctx: nil, only_network_id: nil, limit: nil)
        ctx ||= NetworkMaintenanceContext.new
        limit ||= network_query_limit
        bad_hosts = ctx.bad_hosts
        results = []
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new \
              limit: limit,
              states: [CREATING, DESTROYING, REPAIRING],
              max_transient_time: MAX_TRANSIENT_TIME
            sql.add <<-SQL
              SELECT network_id, host, state
                FROM network_replicas
                WHERE state IN :states
                  AND updated_at < NOW() - INTERVAL :max_transient_time SECOND
            SQL
            if only_network_id
              sql.add "AND network_id=:network_id", network_id: only_network_id
            elsif !bad_hosts.empty?
              sql.add "AND host NOT IN :bad_hosts", bad_hosts: bad_hosts
            end
            sql.add "LIMIT :limit", limit: limit
            results += sql.results
          end # each_network_db
        end # connected_to

        results
      end
      track_query_time :get_network_replicas_for_cleanup

      def self.get_gist_replicas_for_cleanup(only_gist_id: nil, limit: nil)
        ctx ||= GistMaintenanceContext.new
        limit ||= gist_query_limit
        bad_hosts = ctx.bad_hosts
        results = []

        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_gist_db do |db|
            sql = db.SQL.new \
              limit: limit,
              states: [CREATING, DESTROYING, REPAIRING, FAILED],
              max_transient_time: MAX_TRANSIENT_TIME
            sql.add <<-SQL
              SELECT gist_id, host, state
                FROM gist_replicas
                WHERE state IN :states
                  AND updated_at < NOW() - INTERVAL :max_transient_time SECOND
            SQL
            if only_gist_id
              sql.add "AND gist_id = :gist_id", gist_id: only_gist_id
            elsif !bad_hosts.empty?
              sql.add "AND host NOT IN :bad_hosts", bad_hosts: bad_hosts
            end
            sql.add "LIMIT :limit", limit: limit
            results += sql.results
          end # each_gist_db
        end # connected_to

        results
      end
      track_query_time :get_gist_replicas_for_cleanup

      # Get all network replicas that are in the FAILED state.
      #
      # Return value is rows like [network_id, host].
      #
      # TODO: prioritize by last-pushed time.
      def self.get_failed_network_replicas(only_network_id: nil, limit: nil)
        limit ||= network_query_limit
        results = []

        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new failed: FAILED
            sql.add <<-SQL
              SELECT network_id, host
                FROM network_replicas
                WHERE state=:failed
            SQL
            sql.add "AND network_id=:network_id", network_id: only_network_id if only_network_id
            sql.add "LIMIT :limit", limit: limit
            results += sql.results
          end # each_network_db
        end # connected_to

        results
      end
      track_query_time :get_failed_network_replicas

      def self.get_failed_gist_replicas(only_gist_id: nil, limit: nil)
        limit ||= gist_query_limit
        results = []

        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_gist_db do |db|
            sql = db.SQL.new failed: FAILED
            sql.add <<-SQL
              SELECT gist_id, host
                FROM gist_replicas
                WHERE state=:failed
            SQL
            sql.add "AND gist_id = :gist_id", gist_id: only_gist_id if only_gist_id
            sql.add "LIMIT :limit", limit: limit
            results += sql.results
          end # each_gist_db
        end # connected_to

        results
      end
      track_query_time :get_failed_gist_replicas

      # Get all repo replicas that are active but have bad checksums.
      # Caller should initiate repairs.
      #
      # Note that while we check to make sure a replica is ACTIVE before
      # we try to repair it (because a network-level repair subsumes any
      # need for a repo-level repair), we don't actually care if the
      # relevant fileserver is marked `offline` in the `fileservers`
      # table.  If it's marked offline but actually online, repairs will
      # happen.  If it's actually offline, regardless of how it's marked,
      # repairs will get queued.
      #
      # Return value is rows like [repo_id, network_id, host, repository_type].
      #
      # TODO: prioritize by last-pushed time.
      def self.get_repo_replicas_with_bad_checksums(only_network_id: nil, limit: nil)
        limit ||= network_query_limit

        #
        # Recently-updated and now-mismatched tuples of the form:
        #
        #   [ repo_id, host, repository_type ]
        #
        # where the repository_replicas and repository_checksums
        # tables are each checked according to `updated_at`.
        #
        mismatches_found = ActiveRecord::Base.connected_to(role: :reading) do
          results = []
          updated_at_tables = GitHub.enterprise? || only_network_id ? [:ignored] : ["rc", "rr"]
          GitHub::DGit::DB.each_network_db do |db|
            updated_at_tables.each do |updated_at_table|
              sql = db.SQL.new \
                limit: limit
              sql.add <<-SQL
                SELECT rr.repository_id,
                       rr.host,
                       rr.repository_type
                  FROM repository_replicas rr
                  JOIN repository_checksums rc
                    ON rr.repository_id = rc.repository_id
                   AND rr.repository_type = rc.repository_type
                   AND rr.checksum != rc.checksum
              SQL
              if only_network_id
                repository_ids = GitHub::DGit::Util.repo_ids_for(only_network_id)
                return [] if repository_ids.empty?
                sql.add "AND rr.repository_id IN :repository_ids", repository_ids: repository_ids
              end
              if updated_at_table != :ignored
                sql.add "AND :updated_at > NOW() - INTERVAL :recent MINUTE",
                  recent: BAD_CHECKSUMS_INTERVAL,
                  updated_at: GitHub::SQL.LITERAL("#{updated_at_table}.updated_at")
              end
              sql.add "LIMIT :limit"
              results |= sql.results
            end # updated_at_tables
          end # each_network_db
          results
        end # connected_to

        return [] if mismatches_found.empty?

        #
        # Lookup the network_id (source_id) for each of the
        # repository_id entries found in the above queries.
        #
        mismatch_repo_ids = mismatches_found.map(&:first)
        repo_id_to_network_id = repo_ids_to_network_ids(mismatch_repo_ids, include_deleted: false)

        network_ids = []
        if only_network_id
          network_ids = [only_network_id]
        else
          network_ids = repo_id_to_network_id.values.uniq
        end

        return [] if network_ids.empty?

        #
        # Map the set of network_ids to all active network_replicas.
        #
        network_id_and_host_set = ActiveRecord::Base.connected_to(role: :reading) do
          mapped_result = Set.new
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new \
              network_ids: network_ids,
              active: ACTIVE
            sql.add <<-SQL
              SELECT nr.network_id,
                     nr.host
                FROM network_replicas nr
               WHERE nr.network_id IN :network_ids
                 AND nr.state = :active
            SQL
            mapped_result |= sql.results
          end # each_network_db
          mapped_result
        end # connected_to

        #
        # Finally compute the end result list of
        #   [ repo_id, network_id, host, repository_type ]
        #
        result = []
        mismatches_found.each do |repo_id, host, repository_type|
          network_id = repo_id_to_network_id[repo_id]
          if network_id_and_host_set.include? [network_id, host]
            result << [repo_id, network_id, host, repository_type]
          end
        end

        if limit
          result.take(limit)
        else
          result
        end
      end
      track_query_time :get_repo_replicas_with_bad_checksums

      # Undocumented internal API: deeper_checksum_repair
      # Do this if you need to repair more than BAD_CHECKSUMS_INTERVAL
      # minutes worth of damage on a host, e.g., because the host was down
      # or unreachable for too long.
      #
      # host     - the host to repair
      # duration - how long ago the host went down, in minutes.  round up.
      # endtime  - how long ago the host came back up, in minutes.  round down.
      #
      # Duration and endtime can also be used to bracket a particular
      # period in the past to examine.  This can be useful for examining a
      # wide range of times one slice at a time, to avoid overwhelming
      # MySQL or resque.
      #
      # Unlike get_repo_replicas_with_bad_checksums, this function
      # actually queues jobs to perform the repairs.  And it doesn't limit
      # them to 100.  Every hour of outage results in ~500 repair jobs.
      # That's generally OK, though it'll definitely trigger the
      # dgit/repo-repair-rate alert.
      #
      # Example:
      #   ssh fe123            # does not work in lab environments
      #   gh-console --force   # doesn't have to be master; only writing to resque
      #   GitHub::DGit::Maintenance.deeper_checksum_repair('github-dfs1023-cp1-prd', 180)
      #
      # TODO: kick this off automatically based whenever graphite thinks a
      # host has been down too long.
      #
      def self.deeper_checksum_repair(host, duration, endtime = nil)
        repo_id_host_and_repo_types = ActiveRecord::Base.connected_to(role: :reading) do
          result_types = []
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new \
              host: host,
              duration: duration
            sql.add <<-SQL
              SELECT rr.repository_id,
                     rr.host,
                     rr.repository_type
                FROM repository_replicas rr
                JOIN repository_checksums rc
                  ON rr.repository_id = rc.repository_id
                 AND rr.repository_type = rc.repository_type
                 AND rr.checksum != rc.checksum
                 AND rr.host = :host
                 AND rc.updated_at > NOW() - INTERVAL :duration MINUTE
            SQL
            if endtime
              sql.add "AND rc.updated_at <= NOW() - INTERVAL :endtime MINUTE", endtime: endtime
            end
            result_types += sql.results
          end # each_network_db
          result_types
        end # connected_to
        return [] if repo_id_host_and_repo_types.empty?
        repo_ids = repo_id_host_and_repo_types.map(&:first)

        #
        # Lookup the network_id (source_id) for each of the
        # repository_id entries found in the above query.
        #
        repo_id_to_network_id = repo_ids_to_network_ids(repo_ids, include_deleted: false)
        network_ids = repo_id_to_network_id.values.uniq
        return if network_ids.empty?

        #
        # Prune results down to entries that have a network
        # replica in the ACTIVE state on the given host.
        #
        narrowed_network_ids = ActiveRecord::Base.connected_to(role: :reading) do
          narrowed_result = Set.new
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new \
              network_ids: network_ids,
              host: host,
              state: ACTIVE
            sql.add <<-SQL
              SELECT nr.network_id
                FROM network_replicas nr
               WHERE nr.network_id IN :network_ids
                 AND nr.host = :host
                 AND nr.state = :state
            SQL
            narrowed_result |= sql.results.flatten
          end # each_network_db
          narrowed_result
        end # connected_to

        results = []
        repo_id_host_and_repo_types.each do |repo_id, host, repository_type|
          network_id = repo_id_to_network_id[repo_id]
          if narrowed_network_ids.include? network_id
            results << [repo_id, network_id, host, repository_type]
          end
        end

        results.each do |row|
          GitHub::DGit::Maintenance.repair_repo_replica(*row)
        end
      end

      # Get all repo replicas that are active but have a checksum equal to 'creating'.
      # Caller should initiate repairs.
      #
      # Return value is rows like [repo_id, repository_type].
      def self.get_repo_replicas_with_no_checksums(only_network_id: nil, limit: nil)
        limit ||= network_query_limit

        repo_ids_and_types = ActiveRecord::Base.connected_to(role: :reading) do
          results = []
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new \
              old_enough: 2,
              repo_types: GitHub::DGit::RepoType::REPO_ISH,
              limit: limit
            sql.add <<-SQL
              SELECT rc.repository_id,
                     rc.repository_type
                FROM repository_checksums rc
               WHERE rc.checksum='creating'
                 AND rc.repository_type IN :repo_types
                 AND rc.created_at < NOW() - INTERVAL :old_enough MINUTE
            SQL
            if !GitHub.enterprise?
              sql.add "AND rc.updated_at > NOW() - INTERVAL :recent MINUTE",
                recent: BAD_CHECKSUMS_INTERVAL
            end
            sql.add "LIMIT :limit"
            results += sql.results
          end # each_network_db
          results
        end # connected_to

        return repo_ids_and_types unless only_network_id
        return [] if repo_ids_and_types.empty?

        repo_ids = repo_ids_and_types.map(&:first)

        sql = ApplicationRecord::Domain::Repositories.github_sql.new \
          repo_ids: repo_ids,
          network_id: only_network_id
        sql.add <<-SQL
          SELECT r.id
            FROM repositories r
           WHERE r.id IN :repo_ids
             AND r.source_id = :network_id
        SQL
        repo_ids_matching_network_id = Set.new sql.results.flatten

        results = []
        repo_ids_and_types.each do |repo_id, repository_type|
          if repo_ids_matching_network_id.include? repo_id
            results << [repo_id, repository_type]
          end
        end

        results
      end
      track_query_time :get_repo_replicas_with_no_checksums

      # Get all gist replicas that are active but have a checksum equal to 'creating'.
      # Caller should initiate repairs.
      #
      # Return value is an array of gist IDs.
      def self.get_gist_replicas_with_no_checksums(only_gist_id: nil, limit: nil)
        limit ||= gist_query_limit

        gist_ids = ActiveRecord::Base.connected_to(role: :reading) do
          results = []
          GitHub::DGit::DB.each_gist_db do |db|
            sql = db.SQL.new \
              old_enough: 2,
              repo_type: GitHub::DGit::RepoType::GIST,
              limit: limit
            sql.add <<-SQL
              SELECT rc.repository_id
                FROM repository_checksums rc
               WHERE rc.checksum='creating'
                 AND rc.repository_type=:repo_type
                 AND rc.created_at < NOW() - INTERVAL :old_enough MINUTE
            SQL
            if only_gist_id
              sql.add "AND rc.repository_id=:gist_id", gist_id: only_gist_id
            elsif !GitHub.enterprise?
              sql.add "AND rc.updated_at > NOW() - INTERVAL :recent MINUTE",
                recent: BAD_CHECKSUMS_INTERVAL
            end
            sql.add "LIMIT :limit"
            results += sql.results
          end # each_gist_db
          results.flatten
        end # connected_to

        gist_ids
      end
      track_query_time :get_gist_replicas_with_no_checksums

      def self.get_gist_replicas_with_bad_checksums_in_interval(only_gist_id: nil, limit: nil, interval:)
        results = []

        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_gist_db do |db|
            ["rc", "gr"].map do |updated_at_table|
              sql = db.SQL.new \
                active: ACTIVE,
                repo_type: GitHub::DGit::RepoType::GIST
              sql.add <<-SQL
                SELECT gr.gist_id, gr.host FROM gist_replicas gr
                  JOIN repository_checksums rc ON gr.gist_id = rc.repository_id
                   AND rc.repository_type = :repo_type
                   AND rc.checksum != gr.checksum
              SQL
              if !GitHub.enterprise? && only_gist_id.nil?
                sql.add "AND :updated_at > NOW() - INTERVAL :recent MINUTE",
                  recent: interval,
                  updated_at: GitHub::SQL.LITERAL("#{updated_at_table}.updated_at")
              end
              sql.add "WHERE gr.state = :active"
              sql.add "AND gr.gist_id = :only_gist_id", only_gist_id: only_gist_id if only_gist_id
              sql.add "LIMIT :limit", limit: limit
              results |= sql.results
            end # "rc", "gr"
          end # each_gist_db
        end # connected_to

        results
      end

      def self.get_gist_replicas_with_bad_checksums(only_gist_id: nil, limit: nil)
        limit ||= gist_query_limit
        results = []

        # Try multiple times with smaller :updated_at intervals
        # each iteration in case the query is timing out.
        intervals = [BAD_CHECKSUMS_INTERVAL, BAD_CHECKSUMS_INTERVAL / 2, BAD_CHECKSUMS_INTERVAL / 4]
        intervals.each do |interval|
          begin
            results = get_gist_replicas_with_bad_checksums_in_interval(only_gist_id: only_gist_id, limit: limit, interval: interval)
            break
          rescue ActiveRecord::QueryInterruption => e
            raise unless e.mysql_timeout?
            results = []
            app_bucket = (interval == intervals.last) ? "github-dgit" : "github-dgit-debug"
            Failbot.report!(e, app: app_bucket)
          end # begin
        end # each interval

        results.take(limit)
      end
      track_query_time :get_gist_replicas_with_bad_checksums

      # Find hosts that are supposed to have replicas, but they're
      # trying to evacuate, or they don't exist
      # in the `fileservers` table at all.
      def self.get_bad_network_replica_hosts(ctx:)
        ActiveRecord::Base.connected_to(role: :reading) do
          result = []
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new <<-SQL
              SELECT sub.host
                FROM (
                  SELECT DISTINCT host FROM network_replicas
                ) sub
                LEFT JOIN fileservers fs ON sub.host = fs.host
                WHERE IFNULL(fs.evacuating, 1) != 0
            SQL
            if !ctx.voting.nil?
              sql.add("AND fs.non_voting = :voting", voting: !ctx.voting)
            end
            result += sql.results.flatten
          end # each_network_db
          result.uniq!
          tags = ["maint:network", "voting:#{ctx.voting_s}"]
          GitHub.dogstats.gauge("dgit.hosts_draining", result.size, tags: tags)
          result
        end # connected_to
      end
      track_query_time :get_bad_network_replica_hosts

      def self.get_bad_gist_replica_hosts(ctx:)
        ActiveRecord::Base.connected_to(role: :reading) do
          result = []
          GitHub::DGit::DB.each_gist_db do |db|
            sql = db.SQL.new <<-SQL
              SELECT sub.host
                FROM (
                  SELECT DISTINCT host FROM gist_replicas
                ) sub
                LEFT JOIN fileservers fs ON sub.host = fs.host
                WHERE IFNULL(fs.evacuating, 1) != 0
            SQL
            if !ctx.voting.nil?
              sql.add("AND fs.non_voting = :voting", voting: !ctx.voting)
            end
            result += sql.results.flatten
          end # each_gist_db
          result.uniq!
          tags = ["maint:gist", "voting:#{ctx.voting_s}"]
          GitHub.dogstats.gauge("dgit.hosts_draining", result.size, tags: tags)
          result
        end # connected_to
      end
      track_query_time :get_bad_gist_replica_hosts

      # Find hosts that are non-voting.
      def self.get_non_voting_fs
        GitHub::DGit.get_fileservers.reject(&:contains_voting_replicas?)
      end

      # Find and repair networks with replicas on bad hosts
      # Returns network_ids that have been processed
      def self.fix_bad_host_replica_counts(ctx:)
        tags = ["maint:#{ctx.entity}", "phase:evac", "voting:#{ctx.voting_s}"]
        bad_host_replicas = GitHub.dogstats.time("dgit.maintenance.get_bad_replicas", tags: tags) do
          get_bad_replicas_from_bad_hosts(ctx: ctx, limit: nil)
        end
        bad_ids = bad_host_replicas.map(&:first).uniq
        bad_replicas = GitHub::DGit::Routing.all_network_replicas_for_many(bad_ids)
        ctx.load_recently_frozen_or_thawed(bad_ids)
        ctx.load_recent_replicas(bad_ids)
        bad_host_replicas.each do |network_id, count, target_copies, info|
          begin
            GitHub.dogstats.time("dgit.maintenance.fix_bad_replica_count", tags: tags) do
              fix_bad_replica_count(network_id, count, target_copies, bad_replicas[network_id],
                                    ctx.replica_count_helper(**info), ctx: ctx)
            end
          rescue ReplicaCreateError, HostSelectionError => e
            Failbot.report(e, spec: "network/#{network_id}")
          end
        end

        bad_ids
      end

      # Find and repair gists with replicas on bad hosts
      # Returns network_ids that have been processed
      def self.fix_bad_host_gist_replica_counts(ctx:)
        tags = ["maint:#{ctx.entity}", "phase:evac", "voting:#{ctx.voting_s}"]
        bad_host_replicas = GitHub.dogstats.time("dgit.maintenance.get_bad_replicas", tags: tags) do
          get_bad_gist_replica_counts_from_bad_hosts(ctx: ctx, limit: nil)
        end
        bad_ids = bad_host_replicas.map(&:first).uniq
        bad_replicas = GitHub::DGit::Routing.all_gist_replicas_for_many(bad_ids)
        ctx.load_recently_frozen_or_thawed(bad_ids)
        ctx.load_recent_replicas(bad_ids)
        bad_host_replicas.each do |id, count, target_copies, info|
          begin
            GitHub.dogstats.time("dgit.maintenance.fix_bad_replica_count", tags: tags) do
              fix_bad_replica_count(id, count, target_copies, bad_replicas[id],
                                    ctx.replica_count_helper(info), ctx: ctx)
            end
          rescue ReplicaCreateError, HostSelectionError => e
            Failbot.report(e, spec: "gist/#{id}")
          end
        end

        bad_ids
      end

      def self.fix_bad_network_replica_counts(only_network_id:, ctx: NetworkMaintenanceContext.new)
        bad_counts = GitHub.dogstats.time("dgit.maintenance.get_bad_replicas",
                                                 tags: ["maint:#{ctx.entity}", "phase:sliced"]) do
          get_bad_replica_counts(only_network_id: only_network_id, ctx: ctx)
        end
        bad_counts = ensure_still_exists(bad_counts, "repository_networks", ApplicationRecord::Domain::Repositories.github_sql)

        bad_ids = bad_counts.map(&:first).uniq
        ctx.load_recently_frozen_or_thawed(bad_ids)
        ctx.load_recent_replicas(bad_ids)
        bad_replicas = GitHub::DGit::Routing.all_network_replicas_for_many(bad_ids)

        bad_counts.each do |network_id, count, target_copies, info|
          network_replicas = bad_replicas[network_id]
          if network_replicas.empty? && count == 0
            # There are no replicas for this network at all. This will happen if
            # the repository_network row exists, but we don't have any git data
            # for it. This is likely because all the repositories have been
            # removed, but somehow the network didn't get cleaned up.
            #
            # If we tried to fix this, it would fail because there is no source
            # replica.
          else
            begin
              GitHub.dogstats.time("dgit.maintenance.fix_bad_replica_count",
                                   tags: ["maint:#{ctx.entity}", "phase:sliced"]) do
                fix_bad_replica_count(network_id, count, target_copies, network_replicas,
                                      ctx.replica_count_helper(**info), ctx: ctx)
              end
            rescue ReplicaCreateError, HostSelectionError => e
              Failbot.report(e, spec: "network/#{network_id}")
            end
          end
        end

        if !only_network_id
          write_stats_for_bad_counts(bad_counts, "networks")
          report_zeroes(ZeroReplicaNetworks, "Networks", "repository_networks", bad_counts)
        end
      end

      def self.fix_bad_gist_replica_counts(only_gist_id:, ctx:)
        bad_counts = GitHub.dogstats.time("dgit.maintenance.get_bad_replicas",
                                                 tags: ["maint:#{ctx.entity}", "phase:sliced"]) do
          get_bad_gist_replica_counts(only_gist_id: only_gist_id, ctx: ctx)
        end
        bad_counts = ensure_still_exists(bad_counts, "gists", ApplicationRecord::Domain::Gists.github_sql)

        bad_ids = bad_counts.map(&:first).uniq
        bad_replicas = GitHub::DGit::Routing.all_gist_replicas_for_many(bad_ids)
        ctx.load_recently_frozen_or_thawed(bad_ids)
        ctx.load_recent_replicas(bad_ids)

        fixed_zero_replica_gists = []
        bad_counts.each do |id, count, target_copies, info|
          gist_replicas = bad_replicas[id]
          if gist_replicas.empty? && count == 0
            # There are no replicas at all. We should have a "0" count for voting and non-voting.
            # The gist only needs to be fixed once, so queue it up when we see the voting counts
            # come up, and ignore the non-voting case.
            if info[:voting]
              GitHub.dogstats.time("dgit.maintenance.fix_zero_replica_count",
                                   tags: ["maint:#{ctx.entity}", "phase:sliced"]) do
                fix_zero_replica_gist(id)
              end
              fixed_zero_replica_gists << id
            end
          else
            begin
              GitHub.dogstats.time("dgit.maintenance.fix_bad_replica_count",
                                   tags: ["maint:#{ctx.entity}", "phase:sliced"]) do
                fix_bad_replica_count(id, count, target_copies, gist_replicas, ctx.replica_count_helper(**info), ctx: ctx)
              end
            rescue ReplicaCreateError, HostSelectionError => e
              Failbot.report(e, spec: "gist/#{id}")
            end
          end
        end

        if !only_gist_id
          write_stats_for_bad_counts(bad_counts, "gists", "gists-")
          report_zeroes(ZeroReplicaGists, "Gists", "gists", bad_counts, fixed: fixed_zero_replica_gists)
        end
      end


      # Find mis-replicated repo networks (full scan of network_replicas).
      # CREATING and REPAIRING networks are treated optimistically, as if
      # they're about to come back.
      #
      # This means that a failed create or nw-repair that doesn't make it
      # to FAILED or DESTROYING state will stick around until the timeout
      # (1 hour), potentially delaying new-replica-creation operations.
      # The alternative is to be pessimistic and create lots of replicas
      # that turn out not to be needed.  I think repairs will be a lot
      # more frequent than failed repairs, so I'm optimistic here.
      #
      # Returns an array of tuples of [network_id, actual_count, target_count, attrs], where
      # attrs is either {voting: true} or {voting: false}.
      def self.get_bad_replica_counts(only_network_id: nil, limit: nil, ctx: nil)
        limit ||= network_query_limit
        ctx ||= NetworkMaintenanceContext.new

        slice = only_network_id ?
          SliceQuery::OneIdSlice.new(only_network_id, ApplicationRecord::Domain::Repositories) :
          SliceQuery::Slice.new(slice_name: "bad-replica-counts", id_table: "repository_networks", domain_class: ApplicationRecord::Domain::Repositories, query_namespace: query_namespace, max_rss: GitHub.dgit_maint_max_rss)

        results = get_slice_of_bad_replica_counts(slice, ctx: ctx)
        voting_counts, other_counts = limit_bad_replica_results(results, limit: limit, slice: slice)

        (voting_counts + other_counts).uniq.take(limit)
      end
      track_query_time :get_bad_replica_counts

      # Internal.
      #
      # Gets bad replica counts for the given slice of IDs.
      #
      # Returns nil if one of the slice-managed queries times out.
      # Otherwise, returns a hash of info => array of [nw/gist id, count, info]. e.g.
      #
      #   {
      #     {voting: true} => [ [1, 1, voting: true], [2, 4, voting: true] ],
      #     {voting: false} => [ [1, 0, voting: false] ]
      #   }
      def self.get_slice_of_bad_replica_counts(slice, ctx:)
        ids = ctx.get_slice_of_ids(slice)
        return nil if ids.nil?
        return {} if ids.empty?

        replicas = ctx.get_slice_of_replicas(slice)
        return nil if replicas.nil?

        strategy = ctx.make_replication_strategy(slice: slice)
        return nil if strategy.nil?

        bad_counts = Hash.new { |h, k| h[k] = [] }

        ids.each do |id|
          strategy.violations(id, replicas[id] || []).each do |violation|
            info = violation.last
            bad_counts[info].push(violation)
          end
        end

        bad_counts
      end

      # Internal.
      #
      # Puts the bad counts into the right order, applies the limit, and updates
      # the query slice based on the results.
      #
      # * all_bad_counts - the output of get_slice_of_bad_replica_counts
      # * limit - the highest number of items to return
      # * slice - the current query slice, which will be updated
      #
      # Returns two arrays, first the "important" (read: voting) results, then
      # the others. This is so that the caller can insert other results between
      # these two arrays.
      def self.limit_bad_replica_results(all_bad_counts, limit:, slice:)
        # If get_slice_of_bad_replica_counts returned nil, there's nothing to do here.
        return [[], []] if all_bad_counts.nil?

        voting_results = sort_bad_counts(all_bad_counts.delete(voting: true) || []).take(limit)
        if voting_results.size == limit
          next_offset = voting_results.map { |row| row[0] }.max
          slice.set_next_offset next_offset
        else
          slice.move_to_next_offset
        end

        other_results = []
        all_bad_counts.each do |key, counts|
          remaining_limit = limit - voting_results.size - other_results.size
          break if remaining_limit < 1
          other_results += sort_bad_counts(counts).take(remaining_limit)
        end

        [voting_results, other_results]
      end

      def self.sort_bad_counts(counts)
        counts.sort_by { |_, copies, _, _| copies }
      end

      # Gets replica constraint violations for networks on bad hosts.
      # This is not a sliced query, so it returns violations for all eligible
      # networks, up to the specified limit.
      #
      # * context - the current NetworkMaintenanceContext
      # * limit - the highest number of networks to consider
      #
      # Returns an array of tuples of [network_id, actual_count, target_count, attrs],
      # where attrs has voting=true/false or stale=true/false, but not both.  Stale copies
      # are network replicas on bad hosts in excess of the target count.
      def self.get_bad_replicas_from_bad_hosts(ctx: nil, limit: nil)
        ctx ||= NetworkEvacuationMaintenanceContext.new
        limit ||= ctx.query_limit

        return [] if ctx.bad_hosts.empty?

        order_by, dbrole =
          case ctx.evacuate_by
          when EvacuationStrategy::HOST
            ["nr.host", :reading]
          when EvacuationStrategy::ID
            ["nr.network_id", :reading_slow]
          else
            raise "invalid evacuation order"
          end

        results = ActiveRecord::Base.connected_to(role: dbrole) do
          db_results = []
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new \
              limit: limit,
              bad_hosts: ctx.bad_hosts,
              destroying: DESTROYING,
              order_by: GitHub::SQL::Literal.new(order_by)
            sql.add <<-SQL
              SELECT nr.network_id,nr.host,nr.state FROM (
                SELECT network_id,nr.host
                  FROM network_replicas nr
            SQL
            if ctx.evacuate_by == EvacuationStrategy::ID
              sql.add "FORCE INDEX(index_network_replicas_on_host_and_state_and_network_id)"
            end
            sql.add <<-SQL
                  WHERE nr.host in :bad_hosts
                    AND nr.state <> :destroying
                  ORDER BY :order_by ASC
                  LIMIT :limit
                ) sub
                JOIN network_replicas nr ON nr.network_id=sub.network_id
            SQL
            db_results += sql.results
          end # each_network_db
          db_results.group_by(&:first)
        end # connected_to

        return [] if results.empty?

        # Collect the cold_states for the network ids we are considering
        cold_states = Hash.new(0)
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new \
              nw_ids: results.keys
            sql.add <<-SQL
              SELECT cn.network_id, cn.state
                FROM cold_networks cn
               WHERE cn.network_id IN :nw_ids
            SQL
            cold_states.merge!(sql.results.to_h)
          end
        end

        unless results.empty?
          still_extant_networks = Set.new(ActiveRecord::Base.connected_to(role: :reading) do
            ApplicationRecord::Domain::Repositories.github_sql.values(<<-SQL, ids: results.map(&:first))
              SELECT id FROM repository_networks WHERE id IN :ids
            SQL
          end)
          # Orphans are replicas for networks that no longer exist within the app
          results, orphans = results.partition { |id, _| still_extant_networks.include?(id) }
          orphan_ids = orphans.map(&:first).uniq

          # Maybe in the future, this could return violations like a strategy?
          ctx.handle_orphans(orphan_ids)
        end

        strategy = ctx.make_replication_strategy(cold_states: cold_states, include_evacuating: true)
        return [] if strategy.nil?

        faults = []
        results.each do |nw_id, replicas|
          faults += strategy.violations(nw_id, replicas || [])
        end
        faults
      end

      # Counts replicas for a gist, or for the next slice of gists.
      #
      # See get_bad_replica_counts for more about what this is looking for.
      #
      # Returns an array of tuples that looks like:
      #   [network_id, replica_count, target_count, info]
      # where 'info' is a hash with the following keys:
      #   :voting  - boolean, indicating whether the count is for voting replicas or
      #              non-voting replicas
      def self.get_bad_gist_replica_counts(only_gist_id: nil, limit: nil, ctx: nil)
        limit ||= gist_query_limit
        ctx ||= GistMaintenanceContext.new

        slice = only_gist_id ?
          SliceQuery::OneIdSlice.new(only_gist_id, ApplicationRecord::Domain::Gists) :
          SliceQuery::Slice.new(slice_name: "bad-gist-replica-counts", id_table: "gists", domain_class: ApplicationRecord::Domain::Gists, query_namespace: query_namespace, max_rss: GitHub.dgit_maint_max_rss)

        results = get_slice_of_bad_replica_counts(slice, ctx: ctx)
        voting_counts, other_counts = limit_bad_replica_results(results, limit: limit, slice: slice)

        (voting_counts + other_counts).uniq.take(limit)
      end
      track_query_time :get_bad_gist_replica_counts

      def self.get_bad_gist_replica_counts_from_bad_hosts(ctx: nil, limit: nil)
        ctx ||= GistEvacuationMaintenanceContext.new
        limit ||= ctx.query_limit

        return [] if ctx.bad_hosts.empty?

        order_by, dbrole =
          case ctx.evacuate_by
          when EvacuationStrategy::HOST
            ["gr.host", :reading]
          when EvacuationStrategy::ID
            ["gr.gist_id", :reading_slow]
          else
            raise "invalid evacuation order"
          end

        results = ActiveRecord::Base.connected_to(role: dbrole) do
          db_results = []
          GitHub::DGit::DB.each_gist_db do |db|
            sql = db.SQL.new \
              limit: limit,
              bad_hosts: ctx.bad_hosts,
              destroying: DESTROYING,
              order_by: GitHub::SQL::Literal.new(order_by)
            sql.add <<-SQL
              SELECT gr.gist_id,gr.host,gr.state FROM (
                SELECT gist_id,gr.host
                  FROM gist_replicas gr
            SQL
            if ctx.evacuate_by == EvacuationStrategy::ID
              sql.add "FORCE INDEX(index_gist_replicas_on_host_and_state_and_gist_id)"
            end
            sql.add <<-SQL
                  WHERE gr.host in :bad_hosts
                    AND gr.state <> :destroying
                  ORDER BY :order_by ASC
                  LIMIT :limit
                ) sub
                JOIN gist_replicas gr ON gr.gist_id=sub.gist_id
            SQL
            db_results += sql.results
          end # each_gist_db
          db_results.group_by(&:first)
        end # connected_to

        unless results.empty?
          still_extant_gists = Set.new(ActiveRecord::Base.connected_to(role: :reading) do
            ApplicationRecord::Domain::Gists.github_sql.values(<<-SQL, ids: results.map(&:first))
              SELECT id FROM gists WHERE id IN :ids
            SQL
          end)
          # Orphans are replicas of gists that no longer exist within the app
          results, orphans = results.partition { |id, _| still_extant_gists.include?(id) }
          orphan_ids = orphans.map(&:first).uniq

          # Maybe in the future, this could return violations like a strategy?
          ctx.handle_orphans(orphan_ids)
        end

        strategy = ctx.make_replication_strategy(include_evacuating: true)
        return [] if strategy.nil?

        faults = []
        results.each do |gist_id, replicas|
          faults += strategy.violations(gist_id, replicas || [])
        end
        faults
      end

      # Count all network replicas in DGit.
      # An exact count is too expensive, because it's a full scan of the
      # smallest index.  So we just return the database engine's
      # approximate row count for the table.
      #
      # Return value is the approximate count.
      def self.get_network_replicas_count
        ActiveRecord::Base.connected_to(role: :reading) do
          total_count = 0
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new <<-SQL
              SHOW TABLE STATUS LIKE 'network_replicas'
            SQL
            total_count += sql.results.first[4]
          end # each_network_db
          total_count
        end # connected_to
      end
      track_query_time :get_network_replicas_count

      # Get dump-style rows for all the repo replicas in a network.
      #
      # Returns rows like [nwo, repo_id, network_id, cold_storage_state, host, datacenter, fqdn, rack, non_voting, hdd_storage, state, read_weight, online, expected_checksum, actual_checksum, wiki_expected, repo_type]
      def self.dump_one_network(network_id)
        dump(:network, network_id)
      end

      def self.dump_one_gist(gist_id)
        dump(:gist, gist_id)
      end

      def self.dump_gist(gist_id)
        limit = dump_query_limit

        sql = ApplicationRecord::Domain::Gists.github_sql.new \
          gist_id: gist_id
        sql.add <<-SQL
          SELECT CONCAT('gist/', repo_name) AS nwo,
                 g.id AS repo_id,
                 'gist' AS network_id
            FROM gists g
        SQL

        if gist_id
          sql.add "WHERE g.id = :gist_id"
        end
        if limit
          sql.add "LIMIT :limit", limit: limit
        end

        # nwo, repo_id, 'gist'
        nwo_repo_id_network_ids = sql.results

        results = []

        # Use the write_db to make sure we're dumping fresh data.  `dump`
        # is not called frequently, and when it's called, it's often
        # because we've just changed the routes and want to show the
        # result.
        ActiveRecord::Base.connected_to(role: :writing) do
          nwo_repo_id_network_ids.each do |nwo, repo_id, network_id|
            db = GitHub::DGit::DB.for_gist_id(repo_id)
            sql = db.SQL.new \
              db_name: db.name,
              nwo: nwo,
              gist_id: repo_id,
              network_id: network_id,
              repo_type: GitHub::DGit::RepoType::GIST
            sql.add <<-SQL
              SELECT :db_name as db_name,
                     :nwo as nwo,
                     :gist_id as repo_id,
                     :network_id as network_id,
                     NULL as cold_storage_state,
                     gr.host,
                     fs.datacenter,
                     fs.fqdn,
                     fs.rack,
                     fs.non_voting,
                     fs.hdd_storage,
                     gr.state,
                     gr.read_weight,
                     fs.online,
                     rc.checksum AS expected_checksum,
                     gr.checksum AS actual_checksum,
                     :repo_type AS repository_type
                FROM gist_replicas gr
                LEFT JOIN repository_checksums rc
                       ON gr.gist_id = rc.repository_id
                      AND rc.repository_type = :repo_type
                LEFT JOIN fileservers fs
                       ON fs.host = gr.host
               WHERE gr.gist_id = :gist_id
              SQL

            results.concat sql.hash_results
          end
        end

        results
      end

      def self.dump_nongist(type, id)
        limit = dump_query_limit
        identifier = type == :network ? "source_id" : "id"

        sql = ApplicationRecord::Domain::Repositories.github_sql.new(<<-SQL, repo_id: id)
            SELECT owner_id FROM repositories
            WHERE #{identifier} = :repo_id
        SQL

        owner_ids = sql.values

        sql = ApplicationRecord::Domain::Users.github_sql.new(<<-SQL, owner_ids: owner_ids)
          SELECT id, login FROM users
          WHERE id  IN :owner_ids
        SQL

        # create a hash lookup for users mapped to their ids
        users = Hash[sql.results]

        sql = ApplicationRecord::Domain::Repositories.github_sql.new \
          id: id,
          repo: GitHub::DGit::RepoType::REPO,
          wiki: GitHub::DGit::RepoType::WIKI,
          repo_ish: GitHub::DGit::RepoType::REPO_ISH
        sql.add <<-SQL
            SELECT name AS name,
                 r.id AS repo_id,
                 r.source_id AS network_id,
                 r.owner_id
            FROM repositories r
            JOIN repository_networks rn ON rn.id = r.source_id
          SQL

        case type
          when :repo, :wiki
            sql.add "AND r.id = :id"
          when :network
            sql.add "AND r.source_id = :id"
          when :all
          else
            raise "can't dump a #{type.inspect}"
        end

        if limit
          sql.add "LIMIT :limit", limit: limit
        end

        # nwo, repo_id, network_id
        nwo_repo_id_network_ids = sql.results

        nwo_repo_id_network_ids = nwo_repo_id_network_ids.map do |row|
          ["#{users[row[3]]}/#{row[0]}", row[1], row[2]]
        end

        results = []
        repo_types = case type
          when :repo
            [GitHub::DGit::RepoType::REPO]
          when :wiki
            [GitHub::DGit::RepoType::WIKI]
          else
            GitHub::DGit::RepoType::REPO_ISH
        end

        # Use the write_db to make sure we're dumping fresh data.  `dump`
        # is not called frequently, and when it's called, it's often
        # because we've just changed the routes and want to show the
        # result.
        ActiveRecord::Base.connected_to(role: :writing) do
          nwo_repo_id_network_ids.each do |nwo, repo_id, network_id, _|
            db = GitHub::DGit::DB.for_network_id(network_id)
            sql = db.SQL.new \
              db_name: db.name,
              nwo: nwo,
              repo_id: repo_id,
              network_id: network_id,
              id: id,
              repo: GitHub::DGit::RepoType::REPO,
              wiki: GitHub::DGit::RepoType::WIKI,
              repo_types: repo_types
            sql.add <<-SQL
              SELECT :db_name as db_name,
                     if(:repo = rr.repository_type, :nwo, concat(:nwo, '.wiki')) AS nwo,
                     :repo_id AS repo_id,
                     :network_id AS network_id,
                     cn.state as cold_storage_state,
                     rr.host,
                     fs.datacenter,
                     fs.fqdn,
                     fs.rack,
                     fs.non_voting,
                     fs.hdd_storage,
                     nr.state,
                     nr.read_weight,
                     fs.online,
                     if(:repo = rr.repository_type, rc.checksum, wc.checksum) AS expected_checksum,
                     rr.checksum AS actual_checksum,
                     rr.repository_type
                FROM repository_replicas rr
                LEFT JOIN repository_checksums rc
                       ON :repo_id = rc.repository_id AND :repo = rc.repository_type
                LEFT JOIN repository_checksums wc
                       ON :repo_id = wc.repository_id AND :wiki = wc.repository_type
                LEFT JOIN network_replicas nr
                       ON nr.network_id = :network_id AND rr.host = nr.host
                LEFT JOIN fileservers fs
                       ON fs.host = nr.host
                LEFT JOIN cold_networks cn
                       ON cn.network_id = :network_id
               WHERE rr.repository_id = :repo_id AND rr.repository_type IN :repo_types
               ORDER BY rr.repository_id, rr.repository_type, rr.host
              SQL

            results.concat sql.hash_results
          end
        end

        results
      end

      def self.dump(type, id)
        case type
        when :all
          dump_nongist(type, id) + dump_gist(nil)
        when :gist
          dump_gist(id)
        else
          dump_nongist(type, id)
        end
      end

      def self.terminal_table_headings(type, fqdn: false)
        case type
        when :all
          if fqdn
            %w[nwo repo_id nw_id fqdn state rack r_w on? voting? hdd? reposum replsum].freeze
          else
            %w[nwo repo_id nw_id host state dc rack r_w on? voting? hdd? reposum replsum].freeze
          end
        when :network
          if fqdn
            %w[nwo repo_id fqdn state rack r_w on? voting? hdd? reposum replsum].freeze
          else
            %w[nwo repo_id host state dc rack r_w on? voting? hdd? reposum replsum].freeze
          end
        when :gist, :wiki, :repo
          if fqdn
            %w[fqdn state rack r_w on? voting? hdd? replsum].freeze
          else
            %w[host state dc rack r_w on? voting? hdd? replsum].freeze
          end
        end
      end

      def self.terminal_table_rows(type, id)
        dump(type, id).map do |row|
          [
            row["nwo"],
            row["repo_id"],
            row["network_id"],
            row["cold_storage_state"],
            row["fqdn"],
            row["host"],
            GitHub::DGit::STATES[row["state"]].to_s[0..4],
            row["datacenter"],
            row["rack"],
            row["read_weight"],
            row["online"] != 0,
            row["non_voting"] == 0,
            row["hdd_storage"] != 0,
            row["expected_checksum"].to_s[0..6],
            row["actual_checksum"].to_s[0..6] + (row["expected_checksum"] != row["actual_checksum"] ? " [BAD]" : ""),
          ]
        end
      end

      def self.terminal_table(type, id, fqdn: false)
        raw_rows = terminal_table_rows(type, id)

        # Extract nw_id for single-network types.
        case type
        when :all
          nw_id = nil
          rows = raw_rows
        when :gist, :wiki, :repo, :network
          nw_id = raw_rows.map { |r| r[2] }.uniq.first
          raw_rows.each { |r| r.delete_at(2) }
          rows = raw_rows
        end

        # Extract nwo, repo_id, repo_sum for individual types.
        case type
        when :gist, :wiki, :repo
          nwo = raw_rows.map { |r| r[0] }.uniq.first
          repo_id = raw_rows.map { |r| r[1] }.uniq.first
          repo_sum = raw_rows.map { |r| r[13 - 1] }.uniq.first
          cold_storage = raw_rows.map { |r| r[3 - 1] }.uniq.first
          raw_rows.each { |r| r.delete_at(13 - 1) } # expected_checksum
          if fqdn
            raw_rows.each { |r| r.delete_at(7 - 1) }  # datacenter
            raw_rows.each { |r| r.delete_at(5 - 1) }  # host
          else
            raw_rows.each { |r| r.delete_at(4 - 1) }  # fqdn
          end
          raw_rows.each { |r| r.delete_at(3 - 1) }  # cold_storage_state
          raw_rows.each { |r| r.delete_at(1) } # repo_id
          raw_rows.each { |r| r.delete_at(0) } # nwo
          rows = raw_rows
        else
          nwo = nil
          repo_id = nil
          repo_sum = nil
          cold_storage = raw_rows.map { |r| r[3 - 1] }.uniq.first
          if fqdn
            raw_rows.each { |r| r.delete_at(7 - 1) }  # datacenter
            raw_rows.each { |r| r.delete_at(5 - 1) }  # host
          else
            raw_rows.each { |r| r.delete_at(4 - 1) }  # fqdn
          end
          raw_rows.each { |r| r.delete_at(3 - 1) } # cold_storage_state
        end

        if cold_storage
          cold_times = GitHub::DGit::ColdStorage.cold_network_times(nw_id)
        else
          cold_times = nil
        end

        db_name = nil
        case type
        when :gist
          db_name = GitHub::DGit::DB::gist_db_name_for_gist_id(id)
        when :wiki, :repo, :network
          db_name = GitHub::DGit::DB::network_db_name_for_network_id(nw_id)
        end

        {
          db_name: db_name,
          headings: terminal_table_headings(type, fqdn: fqdn),
          rows: rows,
          nw_id: nw_id,
          nwo: nwo,
          repo_id: repo_id,
          repo_sum: repo_sum,
          cold_storage: cold_storage,
          cold_times: cold_times,
        }
      end

      def self.write_stats_for_count(metric, count, tags: [])
        GitHub.dogstats.gauge("dgit.#{metric}", count, tags: tags)
        GitHub.stats.gauge("dgit.all.#{metric}.count", count) if GitHub.enterprise?
      end
      private_class_method :write_stats_for_count

      def self.write_stats_for_bad_counts(bad_counts, metric_group, oldmetric_prefix = "")
        bad_voting_counts, bad_non_voting_counts = bad_counts.partition { |_, _, _, info| info[:voting] }
        write_stats_per_tag(bad_voting_counts, 1, REPLICA_GRAPH_KEYS, "dgit.#{metric_group}.replicas", "dgit.#{oldmetric_prefix}replica-counts", "count") if GitHub.enterprise?
        write_stats_per_tag(bad_non_voting_counts, 1, REPLICA_GRAPH_KEYS, "dgit.#{metric_group}.non-voting-replicas", "dgit.#{oldmetric_prefix}non-voting-replica-counts", "count") if GitHub.enterprise?
      end
      private_class_method :write_stats_for_bad_counts

      def self.write_stats_per_tag(arr, idx, keys, metric, oldmetric, tag) # ignore GraphiteStatsUsageTest
        count_per_key = Hash.new(0)
        keys.each do |key|
          count_per_key[key] = 0
        end
        arr.each do |row|
          count_per_key[row[idx]] += 1
        end
        count_per_key.each do |key, count|
          GitHub.dogstats.gauge(metric, count, tags: ["#{tag}:#{key}"])
          GitHub.stats.gauge("#{oldmetric}.#{key}", count) if GitHub.enterprise?
        end
      end

      def self.fix_bad_replica_count(id, count, target_copies, replicas, helper, ctx:)
        case
          when count < target_copies
            unless ctx.holdoff_replica_creation?(id)
              create_replicas(id, target_copies - count, replicas, helper, ctx: ctx)
            end
          when count > target_copies
            # If there are too many replicas, delete one at random.
            # However, only do it if all replicas are older than
            # MAX_TRANSIENT_TIME, to allow users and evacuation processes
            # ample time to create a new replica and not have it destroyed
            # right away.
            unless ctx.holdoff_replica_destruction?(id)
              # Only consider healthy replicas of the same type (votingness, coldness, etc)
              # for both sanity-checking the count, and finding deletion candidates as
              # non-healthy ones might be being repaired/deleted at the same time
              good_replicas = helper.filter_replicas(replicas).select(&:healthy?)
              hosts = good_replicas.map(&:host)
              removable = good_replicas.select(&:evacuating?).map(&:host)

              if hosts.size <= target_copies
                puts "#{ctx.entity.capitalize} #{id} has #{count} replicas, but only #{hosts.size} are active and online."
                return
              end

              delete_host = removable.sample || hosts.sample

              if ctx.ok_to_queue_job?(delete_host, increment: false)
                # The destroy job is asynchronous, but we mark the replica
                # as DESTROYING immediately.  Otherwise, if the job gets
                # delayed, we could be right back at this point picking
                # another replica for destruction, and end up accidentally
                # queueing destroy jobs for all copies of a network.
                ctx.set_replica_state(id, delete_host, DESTROYING)
                ctx.rebalance_read_weight(id, replicas: replicas)
                ctx.enqueue_destroy_replica(id, delete_host)  # queue a job
              end
            end
        end
      end
      private_class_method :fix_bad_replica_count

      # Queues a job that will try to find replicas on a fileserver for a gist that
      # has no replicas
      def self.fix_zero_replica_gist(gist_id)
        SpokesFixZeroReplicaGistJob.perform_later(gist_id)
      end
      private_class_method :fix_zero_replica_gist

      # Returns true if the network identified by `network_id` has any
      # replicas created in the last `MAX_TRANSIENT_TIME` seconds,
      # regardless of online-ness or state.  This is an indicator that the
      # network is undergoing some sort of migration or repair, and we
      # should allow it to have more than `GitHub.dgit_copies`.
      def self.any_recent_replicas?(network_id)
        ActiveRecord::Base.connected_to(role: :reading) do
          sql = GitHub::DGit::DB.for_network_id(network_id).SQL.new \
            network_id: network_id,
            max_transient_time: MAX_TRANSIENT_TIME
          sql.add <<-SQL
            SELECT COUNT(*)
            FROM network_replicas
            WHERE network_id=:network_id
            AND created_at > NOW() - INTERVAL :max_transient_time SECOND
          SQL
          sql.results.first.first != 0
        end
      end
      private_class_method :any_recent_replicas?
      track_query_time :any_recent_replicas?

      # Returns a set of of the networks in +network_ids+ which have any
      # replicas created in the last `MAX_TRANSIENT_TIME` seconds,
      # regardless of online-ness or state.  This is an indicator that the
      # network is undergoing some sort of migration or repair, and we
      # should allow it to have more than `GitHub.dgit_copies`.
      def self.recent_replicas(network_ids)
        return Set.new if network_ids.empty?

        res = Set.new
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_network_db do |db|
            ids = db.SQL.results(<<-SQL, network_ids: network_ids, max_transient_time: MAX_TRANSIENT_TIME)
            SELECT network_id
              FROM network_replicas
             WHERE network_id IN :network_ids
               AND created_at > NOW() - INTERVAL :max_transient_time SECOND
            SQL
            res |= ids.flatten
          end
        end
        res
      end
      private_class_method :recent_replicas
      track_query_time :recent_replicas

      def self.recent_freeze_or_thaw?(network_id)
        ActiveRecord::Base.connected_to(role: :reading) do
          sql = GitHub::DGit::DB.for_network_id(network_id).SQL.new \
            network_id: network_id,
            cutoff: 60
          sql.add <<-SQL
            SELECT network_id
              FROM cold_networks
             WHERE network_id=:network_id
               AND updated_at > NOW() - INTERVAL :cutoff MINUTE
          SQL
          return true if sql.run.results.size > 0
        end
        false
      end

      # Returns a set of networks which were recently frozen or thawed out of
      # the input list.
      def self.recently_frozen_or_thawed(network_ids)
        return Set.new if network_ids.empty?

        res = Set.new
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_network_db do |db|
            ids = db.SQL.results(<<-SQL, network_ids: network_ids, cutoff: 60)
            SELECT network_id
              FROM cold_networks
             WHERE network_id IN :network_ids
               AND updated_at > NOW() - INTERVAL :cutoff MINUTE
            SQL
            res |= ids.flatten
          end
        end
        res
      end
      private_class_method :recently_frozen_or_thawed
      track_query_time :recently_frozen_or_thawed

      def self.any_recent_gist_replicas?(gist_id)
        ActiveRecord::Base.connected_to(role: :reading) do
          sql = GitHub::DGit::DB.for_gist_id(gist_id).SQL.new \
            gist_id: gist_id,
            max_transient_time: MAX_TRANSIENT_TIME
          sql.add <<-SQL
            SELECT COUNT(*)
              FROM gist_replicas
             WHERE gist_id = :gist_id
               AND created_at > NOW() - INTERVAL :max_transient_time SECOND
          SQL
          sql.results.first.first != 0
        end
      end
      private_class_method :any_recent_gist_replicas?
      track_query_time :any_recent_gist_replicas?

      def self.recent_gist_replicas(gist_ids)
        return Set.new if gist_ids.empty?

        res = Set.new
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_gist_db do |db|
            ids = db.SQL.results(<<-SQL, gist_ids: gist_ids, max_transient_time: MAX_TRANSIENT_TIME)
            SELECT gist_id
              FROM gist_replicas
             WHERE gist_id IN :gist_ids
               AND created_at > NOW() - INTERVAL :max_transient_time SECOND
            SQL
            res |= ids.flatten
          end
        end
        res
      end
      private_class_method :recent_gist_replicas
      track_query_time :recent_gist_replicas

      # Check if all the disks are in balance.  If not, propose a list of
      # repo networks to move.  The list will solve out-of-balance disks,
      # or at least make progress towards solving them.
      #
      # It is safe to run any subset of the jobs returned by this
      # function.
      #
      # The return values from this function will respect
      # replica-placement constraints -- i.e., will keep copies of a repo
      # network from being placed on servers in the same rack.
      #
      # Repeatedly calling this function and acting on its results should
      # coverge toward well balanced disks.  It may not be possible to
      # *reach* well balanced disks for some unreasonable server
      # configurations.  For example, three servers in three racks, of
      # differing sizes, cannot be brought back into balance without
      # violating placement constraints.  In reality, we should have
      # enough servers with few enough constraints that a rebalancing
      # solution will be possible.
      #
      # In theory, this function aims to solve satisfiability and the
      # knapsack problem at the same time.  In practice, it's a greedy
      # approximation that produces fine results quickly for lightly
      # constrained inputs and doesn't do any harm in hard-to-solve
      # cases.
      #
      # Return value is rows like [network_id, from_host, to_host].
      def self.queue_rebalancing_jobs(ctx)
        fileservers = ctx.online_fileservers

        # Find networks that are already on the move, so that they
        # don't get queued to move again.
        already_moved = get_queued_moves_from_resque(fileservers.map(&:name))

        if !GitHub.enterprise?
          remove_dcs = Set.new
          remove_dcs.add("sdc42") if GitHub.flipper[:dgit_no_rebalance_sdc42].enabled?
          remove_dcs.add("va3iad") if GitHub.flipper[:dgit_no_rebalance_va3iad].enabled?
          remove_dcs.add("ac4iad") if GitHub.flipper[:dgit_no_rebalance_ac4iad].enabled?
          remove_dcs.add("ash1iad") if GitHub.flipper[:dgit_no_rebalance_ash1iad].enabled?

          fileservers.reject! { |fs| remove_dcs.include?(fs.datacenter) }
        end

        fileservers.group_by { |fs| [fs.datacenter, fs.contains_voting_replicas?] }.map do |_, fileserver_group|
          _queue_rebalancing_jobs \
            ctx: ctx,
            fileservers: fileserver_group,
            already_moved: already_moved
        end.compact.flatten(1)
      end
      track_query_time :queue_rebalancing_jobs

      # Private.
      def self._queue_rebalancing_jobs(ctx:, fileservers:, already_moved:)
        return [] if fileservers.empty?  # no dgit hosts?!
        fileservers_by_name = Hash[GitHub::DGit.get_fileservers(online_only: false, storage_class: :all).map { |fs| [fs.name, fs] }]

        hosts_not_embargoed = fileservers.select(&:intake_allowed?).map(&:name)
        return [] if hosts_not_embargoed.empty?

        # Find hosts that are out of balance.
        max_host_space = hosts_not_embargoed.map { |h| fileservers_by_name[h].fraction_of_space_available }.max
        min_host_space = fileservers.map(&:fraction_of_space_available).min
        return [] if max_host_space - min_host_space <= MAX_DISK_SPACE_SPREAD && max_host_space <= min_host_space*2

        donors     = fileservers.select { |fs| fs.fraction_of_space_available < max_host_space - MAX_DISK_SPACE_SPREAD || fs.fraction_of_space_available < max_host_space/2 }.map(&:name)
        recipients = fileservers.select { |fs| fs.fraction_of_space_available > min_host_space + MAX_DISK_SPACE_SPREAD || fs.fraction_of_space_available > min_host_space*2 }.map(&:name) \
                     & hosts_not_embargoed
        recipients.delete_if { |host| !ctx.ok_to_queue_job?(host, increment: false) }
        fail if donors.empty?
        return [] if recipients.empty?

        # Eliminate any overlap.  This should be pretty unusual, because
        # it requires 2*MAX_DISK_SPACE_SPREAD between the fullest and
        # emptiest server, and requires that a new too-full server happens
        # before a too-empty server can be corrected or vice versa.
        overlap = donors & recipients
        donors -= overlap

        donors.sort_by!     { |h|  fileservers_by_name[h].weight }
        recipients.sort_by! { |h| -fileservers_by_name[h].weight }

        # We no longer have exact counts of network replicas per host.
        # We have to make do with an approximate count of all network
        # replicas in DGit.
        # We'll even further fudge here and assume that all hosts have the
        # same number of replicas, for the purpose of sampling some
        # networks to move.
        # It'll oversample (a little) on big hosts and undersample
        # (possibly a lot) on small ones.  We'll retry without sampling if
        # a small host kicks back zero potential networks to move.
        network_replicas_count = get_network_replicas_count
        replicas_per_host = network_replicas_count / (fileservers.size+1)

        queued = []
        donors.each do |donor|
          networks = get_some_networks_on(donor, REBALANCING_SAMPLE.to_f / (replicas_per_host+1))
          if networks.empty?
            # Got nothing.  Is this a very small host?  Try again with no sampling.
            networks = get_some_networks_on(donor, nil)
          end

          # Get the replicas for all the to-be-moved networks all at once,
          # rather than having pick_dest_for query them one at a time.
          # hosts_map maps each network ID to the list of hosts where it
          # has replicas.
          hosts_map = Hash.new
          networks.map(&:first).each_slice(1000) do |arr|
            ActiveRecord::Base.connected_to(role: :reading) do
              GitHub::DGit::DB.each_network_db do |db|
                sql = db.SQL.new(<<-SQL, ids: arr)
                  SELECT nr.network_id,nr.host FROM network_replicas nr
                    JOIN fileservers fs on nr.host=fs.host
                    WHERE nr.network_id IN :ids
                SQL
                sql.results.each { |nw_id, host| (hosts_map[nw_id] ||= []) << host }
              end # each_network_db
            end # connected_to
          end # each_slice

          networks.each do |network_id, size_in_mb|
            break unless ctx.ok_to_queue_job?(donor)  # no more from this donor
            next if already_moved[network_id]  # don't move a network twice in one pass
            dest = pick_dest_for(network_id, donor, recipients, fileservers_by_name, hosts_map[network_id])
            next unless dest

            ctx.enqueue_move_replica(network_id, donor, dest)
            queued << [network_id, donor, dest]
            ctx.jobs_per_host[dest] += 1
            ctx.jobs_per_host[donor] += 1
            unless ctx.ok_to_queue_job?(dest, increment: false)
              recipients.delete(dest)
              return queued if recipients.empty?
            end
            already_moved[network_id] = true

            fileservers_by_name[donor].weight += size_in_mb.to_f / fileservers_by_name[donor].size_of_disk_in_mb
            fileservers_by_name[dest].weight  -= size_in_mb.to_f / fileservers_by_name[dest].size_of_disk_in_mb
            if fileservers_by_name[dest].weight <= min_host_space + MAX_DISK_SPACE_SPREAD &&
               fileservers_by_name[dest].weight <= min_host_space*2
                  recipients.delete(dest)
            end
            if fileservers_by_name[donor].weight >= max_host_space - MAX_DISK_SPACE_SPREAD &&
               fileservers_by_name[donor].weight >= max_host_space/2
                  break
            end
          end
        end
        return queued
      end
      private_class_method :_queue_rebalancing_jobs


      def self.get_queued_moves_from_resque(hosts)
        networks = hosts.map do |host|
          ["maint_#{host}", "maint_#{host}_low"].map do |queue|
            GitHub.resque_redis.lrange("resque:queue:#{queue}", 0, 100).map do |job_json|
              Yajl.load(job_json, symbolize_keys: true)
            end.select do |job|
              job[:class] == "GitHub::Jobs::DgitMoveNetworkReplica"
            end.map do |job|
              job[:args].first
            end
          end.flatten.uniq
        end.flatten.uniq
        Hash[networks.map do |nwid|
          [nwid, true]
        end]
      end
      private_class_method :get_queued_moves_from_resque

      # Pick some networks at random that have a replica on `host`, with a
      # probability (for each) of `sample_rate`.  The goal here is to get
      # about REBALANCING_SAMPLE rows, quickly.
      #
      # Also, get the disk usage for each retrieved network.  It'll be
      # useful later.
      #
      # Returns rows like [network_id, disk_usage_in_mb]
      def self.get_some_networks_on(host, sample_rate)
        sampled_row_ids = ActiveRecord::Base.connected_to(role: :reading) do
          sample_results = []
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new(<<-SQL, host: host)
              SELECT nr.id
                FROM network_replicas nr
               WHERE nr.host = :host
             SQL
            if sample_rate
              sql.add "AND RAND() < :rate", rate: sample_rate
            else
              sql.add "LIMIT :limit", limit: REBALANCING_SAMPLE
            end
            sample_results += sql.results.flatten
          end # each_network_db
          sample_results
        end # connected_to

        return [] if sampled_row_ids.empty?

        sampled_network_ids = ActiveRecord::Base.connected_to(role: :reading) do
          sample_results = []
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new \
              active: ACTIVE,
              min_rebalancing_age: MIN_REBALANCING_AGE,
              ids: sampled_row_ids
            sql.add <<-SQL
              SELECT nr.network_id
                FROM network_replicas nr
               WHERE nr.state = :active
                 AND nr.created_at < NOW() - INTERVAL :min_rebalancing_age MINUTE
                 AND nr.id IN :ids
            SQL
            sample_results += sql.results.flatten
          end # each_network_db
          sample_results
        end # connected_to

        return [] if sampled_network_ids.empty?

        ActiveRecord::Base.connected_to(role: :reading) do
          sql = ApplicationRecord::Domain::Repositories.github_sql.new \
            network_ids: sampled_network_ids
          sql.add <<-SQL
            SELECT rn.id,
                   rn.disk_usage
              FROM repository_networks rn
             WHERE rn.id IN :network_ids
          SQL
          sql.results.shuffle.map { |id, du| [id, du.to_i/1024] }
        end # connected_to
      end
      track_query_time :get_some_networks_on

      # Pick a legal destination for a given network.  A legal destination
      # is any host in the `recipients` list that doesn't already have a
      # copy of the network and isn't in the same rack as a host that has
      # (and isn't about to delete) a copy of the network.
      #   - network_id - the network we wish to move
      #   - donor      - the host we wish to move it from
      #   - recipients - an array of hosts to consider as destinations.
      #                  this list is considered in order.
      #   - placement_zones
      #                - a hash mapping hosts to placement_zones (ip_mask or dc/rack)
      def self.pick_dest_for(network_id, donor, recipients, fileservers_by_name, hosts)
        exclude_masks = if GitHub.enterprise?
          []
        else
          (hosts - [donor]).map { |h| fileservers_by_name[h].placement_zone }
        end
        candidates = recipients - hosts
        candidates.delete_if { |h| exclude_masks.include?(fileservers_by_name[h].placement_zone) }
        candidates.first
      end
      private_class_method :pick_dest_for
      track_query_time :pick_dest_for

      # Return a Hash of repo_id -> network_id
      # for the given list of repo IDs.
      # If include_deleted is false, nothing will be returned for repo_ids
      # whose rows are marked non-`active`.
      def self.repo_ids_to_network_ids(repo_ids, include_deleted:)
        sql = ApplicationRecord::Domain::Repositories.github_sql.new \
          repo_ids: repo_ids
        sql.add <<-SQL
          SELECT r.id,
                 r.source_id
            FROM repositories r
           WHERE r.id IN :repo_ids
        SQL
        sql.add "AND r.active" unless include_deleted
        Hash[sql.results] # repo_id -> network_id
      end
      private_class_method :repo_ids_to_network_ids

      # Return the source ID for a given repository ID.
      def self.network_source_id_for(repo_id)
        result = repo_ids_to_network_ids([repo_id], include_deleted: true)
        result.empty? ? nil : result[repo_id]
      end

      def self.delete_repo_replicas_and_checksums(network_id, repo_id, repo_types = GitHub::DGit::RepoType::REPO_ISH)
        db = GitHub::DGit::DB.for_network_id(network_id)
        delete_repo_replicas_and_checksums_for_db(network_id, repo_id, repo_types, db)
      end

      def self.delete_repo_replicas_and_checksums_for_db(network_id, repo_id, repo_types, db)
        unless ((repo_types == GitHub::DGit::RepoType::REPO_ISH) ||
                (repo_types == [GitHub::DGit::RepoType::WIKI]))
          raise ArgumentError, "wrong repo_types given to #{__method__}"
        end

        ActiveRecord::Base.connected_to(role: :writing) do
          db.transaction do
            delete_repo_replicas(network_id, repo_id, repo_types: repo_types, db: db)

            sql = db.SQL.new \
              network_id: network_id,
              repo_id: repo_id,
              repo_types: repo_types
            sql.add <<-SQL
              DELETE FROM repository_checksums
                    WHERE repository_id = :repo_id
                      AND repository_type IN :repo_types
            SQL
            sql.run
          end # transaction
        end # connected_to
      end
      private_class_method :delete_repo_replicas_and_checksums_for_db

      def self.delete_repo_replicas(network_id, repo_id, db:, repo_types: GitHub::DGit::RepoType::REPO_ISH)
        sql = db.SQL.new \
          network_id: network_id,
          repo_id: repo_id,
          repo_types: repo_types
        sql.add <<-SQL
          DELETE FROM repository_replicas
                WHERE repository_id = :repo_id
                  AND repository_type IN :repo_types
        SQL
        sql.run
      end

      def self.delete_wiki_replicas_and_checksums(network_id, repo_id)
        delete_repo_replicas_and_checksums(network_id, repo_id, [GitHub::DGit::RepoType::WIKI])
      end

      def self.delete_network_replicas(network_id)
        db = GitHub::DGit::DB.for_network_id(network_id)
        delete_network_replicas_for_db(network_id, db)
      end

      def self.delete_network_replicas_for_db(network_id, db)
        ActiveRecord::Base.connected_to(role: :writing) do
          db.transaction do
            db.SQL.run("DELETE FROM network_replicas WHERE network_id = :network_id",
                       network_id: network_id)
            GitHub::DGit::ColdStorage.remove_network_state(db, network_id)
          end # transaction
        end # connected_to
      end
      private_class_method :delete_network_replicas_for_db

      def self.delete_network_replica_for_host(network_id, host, ctx: NetworkMaintenanceContext.new)
        ctx.delete_replica_for_host(network_id, host)
      end

      def self.delete_gist_replicas_and_checksums(gist_id)
        gist_db = GitHub::DGit::DB.for_gist_id(gist_id)
        delete_gist_replicas_and_checksums_for_db(gist_id, gist_db)
      end

      def self.delete_gist_replicas_and_checksums_for_db(gist_id, db)
        ActiveRecord::Base.connected_to(role: :writing) do
          db.transaction do
            db.SQL.run(<<-SQL, gist_id: gist_id)
              DELETE FROM gist_replicas
                    WHERE gist_id = :gist_id
            SQL

            db.SQL.run(<<-SQL, gist_id: gist_id, repo_type: GitHub::DGit::RepoType::GIST)
              DELETE FROM repository_checksums
                    WHERE repository_id = :gist_id
                      AND repository_type = :repo_type
            SQL
          end
        end
      end
      private_class_method :delete_gist_replicas_and_checksums_for_db

      def self.delete_gist_replica_for_host(gist_id, host, ctx: GistMaintenanceContext.new)
        ctx.delete_replica_for_host(gist_id, host)
      end

      # Helpers for setting up initial data and tracking hashes.
      # Used by the network and gist maintenance jobs.
      class MaintenanceContext
        # Used when we want to filter out bad hosts per votingness
        attr_accessor :voting

        def initialize(voting: nil)
          @voting = voting
        end

        # Internal.
        def all_fileservers
          @all_fileservers ||= GitHub::DGit.get_fileservers(online_only: false)
        end

        # Return a string describing whether we are selecting voting, non-voting
        # or both kinds of servers. This is suitable for using with stat tags.
        def voting_s
          @voting.nil? ? "both" : voting.to_s
        end

        # All fileservers that don't vote on 3PC transactions.
        def non_voting_fs
          @non_voting_fs ||= all_fileservers.reject(&:contains_voting_replicas?)
        end

        # All non-voting fileservers that are allowed to receive new replicas.
        def non_voting_fs_for_new_replicas
          non_voting_fs.select(&:online?).reject(&:evacuating?).reject(&:embargoed?)
        end

        def online_fileservers
          all_fileservers.select(&:online?)
        end

        # All fileservers that vote on 3PC and that are online.
        def online_voting_fileservers
          @online_voting_fileservers ||= all_fileservers.select(&:contains_voting_replicas?).select(&:online?)
        end

        # Hostnames of voting fileservers.
        def online_voting_hosts
          online_voting_fileservers.map(&:name)
        end

        # All fileservers that are online.
        def online_hosts
          @online_hosts ||= all_fileservers.select(&:online?).map(&:name)
        end

        # All fileservers that are online and evacuating
        def evacuating_hosts
          @evacuating_hosts ||= online_fileservers.select(&:evacuating?).map(&:name)
        end

        # Where we want the work to be enqueued
        def queue_for(host)
          "maint_#{host}"
        end

        # Get a replication strategy
        #
        # This class returns the default strategy (e.g. 3 voting & 3
        # non-voting copies). Subclasses can return a strategy that knows
        # per-network or per-gist tricks, like for cold storage.
        #
        # Returns a strategy object
        def make_replication_strategy(**options)
          # The bad hosts list includes evacuating as well as non-existent host
          # that might have been left over. For the slicing operations we want
          # to exclude the evacuating ones but keep the non-existent ones in as
          # "bad". We also want to make sure that we include the evacuating
          # hosts in one of the "good" or "bad" lists.
          fs, bh =
              if options.fetch(:include_evacuating, false)
                [online_fileservers.reject(&:evacuating?), bad_hosts]
              else
                [online_fileservers, bad_hosts - evacuating_hosts]
              end
          ReplicationStrategy.new(fs, bad_hosts: bh, voting_strategy: options.fetch(:voting_strategy, nil))
        end

        def replica_count_helper(voting: nil, stale: nil, **)
          raise unless [voting, stale].compact.size == 1
          case
            when !voting.nil?
              if voting
                @replica_count_helper_voting ||= FixVotingReplicaCounts.new(self)
              else
                @replica_count_helper_nonvoting ||= FixNonVotingReplicaCounts.new(self)
              end
            when !stale.nil?
              @replica_count_helper_stale ||= FixStaleReplicaCounts.new(self, bad_hosts)
            else
              raise ArgumentError, "unrecognised helper required"
          end
        end

        # Initial count of queued jobs per fileserver.
        def queued_jobs
          @queued_jobs ||= Hash.new do |hash, host|
            # llen returns 0 if the key doesn't exist, so the default value is 0.
            hash[host] = GitHub.resque_redis.llen("resque:queue:#{queue_for(host)}")
          end
        end

        # Count of new jobs per fileserver.
        def jobs_per_host
          @jobs_per_host ||= Hash.new(0)
        end

        # Is it ok to queue something up for this host?
        def ok_to_queue_job?(host, increment: false)
          count = queued_jobs[host] + jobs_per_host[host]
          if count >= MAX_JOBS_PER_HOST
            tags = ["maint:#{entity}", "target_host:#{host}"]
            GitHub.dogstats.increment("dgit.maintenance.queue_full", tags: tags)
            return false
          end
          jobs_per_host[host] += 1 if increment
          true
        end

        def get_slice_of_ids(slice)
          slice.querying do
            ActiveRecord::Base.connected_to(role: :reading) do
              sql = slice.domain_class.github_sql.new \
                table: GitHub::SQL.LITERAL(id_table),
                offset: slice.offset,
                next_offset: slice.next_offset
              sql.add <<-SQL
                SELECT id
                  FROM :table
                 WHERE id > :offset
                   AND id <= :next_offset
              SQL
              sql.results.flatten
            end
          end
        end

        def get_slice_of_replicas(slice)
          replicas = []
          GitHub::DGit::DB.each_network_db do |db|
            results = slice.querying do
              sql = db.SQL.new \
                table: GitHub::SQL.LITERAL(replica_table),
                id_col: GitHub::SQL.LITERAL("#{entity}_id"),
                offset: slice.offset,
                next_offset: slice.next_offset
              sql.add <<-SQL
                SELECT :id_col, host, state
                  FROM :table
                 WHERE :id_col > :offset
                   AND :id_col <= :next_offset
              SQL
              sql.results
            end
            if results.nil?
              return nil
            else
              replicas += results
            end
          end
          replicas = replicas.group_by(&:first)
        end

      end

      class NetworkMaintenanceContext < MaintenanceContext
        def entity
          "network"
        end

        def id_table
          "repository_networks"
        end

        def replica_table
          "network_replicas"
        end

        # All hosts currently considered bad
        def bad_hosts
          @bad_hosts ||= GitHub::DGit::Maintenance.get_bad_network_replica_hosts(ctx: self)
        end

        def get_slice_of_cold_networks(slice)
          aggregate_results = Hash.new(0)
          ActiveRecord::Base.connected_to(role: :reading) do
            slice.querying do
              GitHub::DGit::DB.each_network_db do |db|
                sql = db.SQL.new \
                  offset: slice.offset,
                  next_offset: slice.next_offset
                sql.add <<-SQL
                  SELECT cn.network_id, cn.state
                    FROM cold_networks cn
                   WHERE cn.network_id > :offset
                     AND cn.network_id <= :next_offset
                SQL
                aggregate_results.merge!(sql.results.to_h)
              end
            end
          end
          aggregate_results
        end

        # Returns a cold-storage-aware replication strategy
        def make_replication_strategy(**options)
          slice = options.delete(:slice)
          cold_states = options.delete(:cold_states)

          cold_states ||= get_slice_of_cold_networks(slice)
          return nil if cold_states.nil?

          # For networks that aren't in cold storage, we use the default strategy, augmented
          # with a counter driving copies on HDD servers to zero.  When a network is warmed,
          # this is what cleans up any left-over replicas on HDD servers.
          cfs = GitHub::DGit.get_fileservers(storage_class: :hdd).reject(&:evacuating?).index_by(&:name)
          non_cold_storage_strategy = ReplicationStrategy::Multi.new \
              ReplicationStrategy::ReplicaCounter.new(fileservers: cfs, copies: 0, states: nil, cold_storage: true, datacenter: nil),
              super(**options)

          # If we include the evacuating hosts as bad, then we do not want to
          # include them in the list of fileservers where it's fine to have a
          # replica.
          fs = GitHub::DGit.get_fileservers(storage_class: :all)
          if options.fetch(:include_evacuating, false)
            fs = fs.reject(&:evacuating?)
          end

          # For networks that are in cold storage, we use the cold storage counter injected
          # into the default strategy in place of the normal voting-replicas counter.  This
          # continues to mix up the ideas of datacenters (for cold storage) and voting/non
          # groups of fileservers.  We'll need to revisit this as we continue to evolve our
          # datacenter plans.
          cold_storage_counter = ReplicationStrategy::ColdStorageCounter.new(fileservers: fs, datacenters: GitHub::DGit.cold_storage_dcs)
          cold_storage_strategy = super(**options.merge({voting_strategy: cold_storage_counter}))

          # Finally, compose the two strategies together, based on whether
          # the network of interest is in cold storage or not
          ReplicationStrategy::LazyStrategy.new do |id, r|
            if cold_states[id] == 1
              cold_storage_strategy
            else
              non_cold_storage_strategy
            end
          end
        end

        def replica_count_helper(**options)
          if options.has_key? :cold_storage
            @replica_count_helper_cold ||= Hash.new { |h, k| h[k] = FixColdStorageCounts.new(self, **k) }
            @replica_count_helper_cold[options]
          else
            super
          end
        end

        def all_replicas(network_id)
          GitHub::DGit::Routing.all_network_replicas(network_id)
        end

        def enqueue_create_replica(network_id, host)
          GitHub::Logger.log(fn: "enqueue_create_replica",
                             network_id: network_id,
                             host: host)
          SpokesCreateNetworkReplicaJob.set(queue: queue_for(host)).perform_later(network_id, host)
        end

        def enqueue_move_replica(network_id, from_host, to_host)
          GitHub::Logger.log(fn: "enqueue_move_replica",
                             network_id: network_id,
                             from_host: from_host,
                             to_host: to_host)
          SpokesMoveNetworkReplicaJob.set(queue: queue_for(to_host)).perform_later(network_id, from_host, to_host, Time.now.to_i)
        end

        def enqueue_destroy_replica(network_id, host, inhibit_repairs: false)
          GitHub::Logger.log(fn: "enqueue_destroy_replica",
                             network_id: network_id,
                             host: host)
          set_replica_state(network_id, host, DESTROYING)
          SpokesDestroyNetworkReplicaJob.set(queue: queue_for(host)).perform_later(network_id, host, inhibit_repairs)
        end

        def rebalance_read_weight(network_id, new_host: nil, replicas: nil)
          rebalancer = GitHub::DGit::Rebalancer::Network.new(network_id, new_host: new_host, replicas: replicas)
          rebalancer.rebalance
        end

        def set_replica_state(network_id, host, new_state, prior_state = nil, update_time = true)
          tags = ["kind:network", "new_state:#{new_state}"]
          if prior_state
            tags << "prior_state:#{prior_state}"
          end
          GitHub.dogstats.increment("dgit.set_replica_state", tags: tags)
          ActiveRecord::Base.connected_to(role: :writing) do
            sql = GitHub::DGit::DB.for_network_id(network_id).SQL.new \
              network_id: network_id,
              host: host,
              new_state: new_state
            sql.add <<-SQL
              UPDATE network_replicas
              SET state=:new_state
            SQL
            sql.add(", updated_at=NOW()") if update_time
            sql.add("WHERE network_id=:network_id AND host=:host")
            sql.add("AND state IN :prior", prior: [prior_state].flatten) if prior_state
            sql.run
            return sql.affected_rows == 1
          end
        end

        def delete_replica_for_host(network_id, host)
          db = GitHub::DGit::DB.for_network_id(network_id)
          db.SQL.run(<<-SQL, network_id: network_id, host: host)
            DELETE FROM network_replicas
              WHERE network_id=:network_id AND host=:host
          SQL
        end

        def load_recently_frozen_or_thawed(network_ids)
          @recently_frozen_or_thawed = Maintenance.recently_frozen_or_thawed(network_ids)
        end

        def load_recent_replicas(network_ids)
          @recent_replicas = Maintenance.recent_replicas(network_ids)
        end

        def holdoff_replica_creation?(network_id)
          raise "no frozen or thawed set loaded" unless @recently_frozen_or_thawed
          @recently_frozen_or_thawed.include?(network_id)
        end

        def holdoff_replica_destruction?(network_id)
          raise "no frozen or thawed set loaded" unless @recently_frozen_or_thawed
          raise "no recent replicas set loaded" unless @recent_replicas
          @recent_replicas.include?(network_id) || @recently_frozen_or_thawed.include?(network_id)
        end

        def handle_orphans(orphans)
          GitHub.dogstats.count("dgit.orphan.network", orphans.size)
          # Networks that no longer exist in the app might as well be deleted
          # The bits on disk can be recovered either with the `find-fs-orphans`
          # script or (because we found it on an evacuating host) through the
          # normal evacuation / reprovisioning lifecycle of the fileservers.
          orphans.each do |nw_id|
            Maintenance.delete_network_replicas(nw_id)
          end
        end
      end

      class NetworkEvacuationMaintenanceContext < NetworkMaintenanceContext
        # Whether to evacuate sorting by host or id, see EvacuationStrategy
        attr_accessor :evacuate_by

        def initialize(evacuate_by: EvacuationStrategy::HOST, **kw)
          @evacuate_by = evacuate_by
          super(**kw)
        end

        def queue_for(host)
          "maint_#{host}_low"
        end

        # How many networks should we try to evacuate this run?
        def query_limit
          if !GitHub.enterprise?
            if @voting == false && GitHub.flipper[:dgit_split_evacuation].enabled?
              GitHub::DGit::Maintenance.evac_network_query_nonvoting_limit
            else
              GitHub::DGit::Maintenance.evac_network_query_limit
            end
          else
            GitHub::DGit::Maintenance.network_query_limit
          end
        end
      end

      class GistMaintenanceContext < MaintenanceContext
        def entity
          "gist"
        end

        def id_table
          "gists"
        end

        def replica_table
          "gist_replicas"
        end

        # All hosts currently considered bad
        def bad_hosts
          @bad_hosts ||= GitHub::DGit::Maintenance.get_bad_gist_replica_hosts(ctx: self)
        end

        def all_replicas(gist_id)
          GitHub::DGit::Routing.all_gist_replicas(gist_id)
        end

        def enqueue_create_replica(gist_id, host)
          GitHub::Logger.log(fn: "enqueue_create_replica",
                             gist_id: gist_id,
                             host: host)
          SpokesCreateGistReplicaJob.set(queue: queue_for(host)).perform_later(gist_id, host)
        end

        def enqueue_move_replica(gist_id, from_host, to_host)
          GitHub::Logger.log(fn: "enqueue_move_replica",
                             gist_id: gist_id,
                             from_host: from_host,
                             to_host: to_host)
          SpokesMoveGistReplicaJob.set(queue: queue_for(to_host)).perform_later(gist_id, from_host, to_host, Time.now.to_i)
        end

        def enqueue_destroy_replica(gist_id, host, inhibit_repairs: false)
          GitHub::Logger.log(fn: "enqueue_destroy_replica",
                             gist_id: gist_id,
                             host: host)
          set_replica_state(gist_id, host, DESTROYING)
          SpokesDestroyGistReplicaJob.set(queue: queue_for(host)).perform_later(gist_id, host, inhibit_repairs)
        end

        def rebalance_read_weight(gist_id, replicas: nil)
          rebalancer = Rebalancer::Gist.new(gist_id, replicas: replicas)
          rebalancer.rebalance
        end

        def set_replica_state(gist_id, host, new_state, prior_state = nil, update_time = true)
          tags = ["kind:gist", "new_state:#{new_state}"]
          if prior_state
            tags << "prior_state:#{prior_state}"
          end
          GitHub.dogstats.increment("dgit.set_replica_state", tags: tags)
          ActiveRecord::Base.connected_to(role: :writing) do
            sql = GitHub::DGit::DB.for_gist_id(gist_id).SQL.new \
              gist_id: gist_id,
              host: host,
              new_state: new_state
            sql.add <<-SQL
              UPDATE gist_replicas
              SET state=:new_state
            SQL
            sql.add(", updated_at=NOW()") if update_time
            sql.add("WHERE gist_id=:gist_id AND host=:host")
            sql.add("AND state=:prior", prior: prior_state) if prior_state
            sql.run
            return sql.affected_rows == 1
          end
        end

        def delete_replica_for_host(gist_id, host)
          db = GitHub::DGit::DB.for_gist_id(gist_id)
          db.SQL.run(<<-SQL, gist_id: gist_id, host: host)
            DELETE gist_replicas FROM gist_replicas
              WHERE gist_replicas.gist_id=:gist_id
                AND gist_replicas.host=:host
          SQL
        end

        def load_recently_frozen_or_thawed(gist_ids)
          # no-op
        end

        def load_recent_replicas(gist_ids)
          @recent_replicas = Maintenance.recent_gist_replicas(gist_ids)
        end

        def holdoff_replica_creation?(gist_id)
          false
        end

        def holdoff_replica_destruction?(gist_id)
          raise "no recent replicas set loaded" unless @recent_replicas
          @recent_replicas.include?(gist_id)
        end

        def handle_orphans(orphans)
          GitHub.dogstats.count("dgit.orphan.gist", orphans.size)
          # Gists that no longer exist in the app might as well be deleted
          # The bits on disk can be recovered either with the `find-fs-orphans`
          # script or (because we found it on an evacuating host) through the
          # normal evacuation / reprovisioning lifecycle of the fileservers.
          orphans.each do |gist_id|
            Maintenance.delete_gist_replicas_and_checksums(gist_id)
          end
        end
      end

      class GistEvacuationMaintenanceContext < GistMaintenanceContext
        # Whether to evacuate sorting by host or id, see EvacuationStrategy
        attr_accessor :evacuate_by

        def initialize(evacuate_by: EvacuationStrategy::HOST, **kw)
          @evacuate_by = evacuate_by
          super(**kw)
        end

        def queue_for(host)
          "maint_#{host}_low"
        end

        # How many gists should we try to evacuate this run?
        def query_limit
          if !GitHub.enterprise?
            if @voting == false && GitHub.flipper[:dgit_split_evacuation].enabled?
              GitHub::DGit::Maintenance.evac_gist_query_nonvoting_limit
            else
              GitHub::DGit::Maintenance.evac_gist_query_limit
            end
          else
            GitHub::DGit::Maintenance.gist_query_limit
          end
        end
      end

      module LoadAwarePicker
        def pick_hosts(count, old_replicas)
          filtered_old_replicas = filter_replicas(old_replicas)
          old_fileservers = filtered_old_replicas.map(&:fileserver)
          too_busy_fileservers = []
          ok_fileservers = []
          while count > 0
            candidates = pick_some_candidate_fileservers(count, current_fileservers: old_fileservers, chosen_fileservers: ok_fileservers, too_busy_fileservers: too_busy_fileservers)
            break if candidates.empty?
            candidates.each do |fs|
              if !@ctx || @ctx.ok_to_queue_job?(fs.name, increment: true)
                ok_fileservers << fs
                count -= 1
              else
                too_busy_fileservers << fs
              end
            end
          end
          ok_fileservers.map(&:name)
        end

        def pick_some_candidate_fileservers(count, current_fileservers:, chosen_fileservers:, too_busy_fileservers:)
          excluded_hosts = (current_fileservers + chosen_fileservers + too_busy_fileservers).map(&:name)
          far_from_fs = current_fileservers + chosen_fileservers
          HostPicker.new.pick_fileservers(count, fileserver_pool: fileservers_for_new_replicas, exclusions: excluded_hosts, far_from: far_from_fs, no_raise: true)
        end
      end

      class FixVotingReplicaCounts
        def initialize(ctx)
          @ctx = ctx
        end

        attr_reader :ctx

        def filter_replicas(replicas)
          replicas.select(&:voting?)
        end

        include LoadAwarePicker
        def fileservers_for_new_replicas
          @fs_for_new_replicas ||= GitHub::DGit.get_fileservers(voting_only: true, exclude_embargoed: true)
        end
      end

      class FixNonVotingReplicaCounts
        def initialize(ctx)
          @ctx = ctx
        end

        attr_reader :ctx

        def filter_replicas(replicas)
          replicas.reject(&:voting?)
        end

        include LoadAwarePicker
        def fileservers_for_new_replicas
          @fs_for_new_replicas ||= ctx.non_voting_fs_for_new_replicas
        end

        def pick_some_candidate_fileservers(count, current_fileservers:, chosen_fileservers:, too_busy_fileservers:)
          candidates = super(count, current_fileservers: current_fileservers, chosen_fileservers: chosen_fileservers, too_busy_fileservers: too_busy_fileservers)
          candidates = candidates.group_by(&:datacenter)
          current_datacenters = current_fileservers.map(&:datacenter).uniq
          new_datacenters = chosen_fileservers.map(&:datacenter).uniq

          ok_candidates = []
          candidates.each_key do |dc|
            if current_datacenters.include?(dc)
              ok_candidates += candidates[dc]
            elsif new_datacenters.include?(dc)
              # There's already going to be a new replica in 'dc',
              # so don't add anymore (yet).
            else
              # There aren't any replicas in 'dc' yet, so create the
              # first one now.
              ok_candidates << candidates[dc].first
            end
          end

          ok_candidates
        end
      end

      class FixStaleReplicaCounts
        def initialize(ctx, bad_fs)
          @ctx = ctx
          @bad_fs = bad_fs
        end

        attr_reader :ctx

        def filter_replicas(replicas)
          replicas.select { |r| @bad_fs.include?(r.fileserver.name) }
        end
      end

      class FixColdStorageCounts
        def initialize(ctx, cold_storage:, datacenter:)
          @ctx = ctx
          @cold_storage = cold_storage
          @datacenters = datacenter.nil? ? nil : [datacenter]
          @storage_class = cold_storage ? :hdd : :ssd
        end

        attr_reader :ctx

        def filter_replicas(replicas)
          replicas.select { |r| r.hdd_storage? == @cold_storage }
                  .select { |r| @datacenters.nil? || @datacenters.include?(r.datacenter) }
        end

        include LoadAwarePicker
        def fileservers_for_new_replicas
          @fs_for_new_replicas ||= GitHub::DGit.get_fileservers(exclude_embargoed: true, in_datacenters: @datacenters, storage_class: @storage_class)
        end
      end
    end  # GitHub::DGit::Maintenance
  end  # GitHub::DGit
end  # GitHub
