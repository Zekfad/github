# rubocop:disable Style/FrozenStringLiteralComment

require "socket"
require "github/dgit/maintenance"
require "github/dgit/routing"
require "github/dgit/route"

module GitHub
  class DGit
    # GitRPC's dgit: protocol needs to interact with MySQL tables, to look
    # up routes and to update checksums.  But GitRPC isn't in a part of the
    # namespace that knows about MySQL.  So we hand it an instance of this
    # DGit::Delegate class to do the dirty work.  There's likely to be one
    # instance of DGit::Delegate for each GitRPC::Protocol::DGit object,
    # because DGit::Delegate, as implemented here, has the routes
    # pre-determined.
    #  - id:           repository or gist database ID, according to namespace,
    #                  used for database queries
    #  - shard_path:   shard path for the given repository or gist
    #  - routes:       optional list of GitHub::DGit::Delegate::Repository
    #                  instances to use as the routes
    class Delegate
      UPDATE_BATCH_SIZE = 100

      attr_reader :id, :shard_path

      def initialize(id, shard_path, routes: nil)
        @id, @shard_path = id, shard_path
        @routes = @all_routes = routes

        #
        # Important note about `routes` and `preferred_dc`: at this time
        # of writing there are two call sites which supply pre-computed
        # routes to newly-created Delegate objects: gist creation and
        # GHE-only archived repository restores.  Using a getter which
        # specifies a non-nil `preferred_dc` *will* reset any cached
        # routes that may have been provided that way.  At this writing
        # those two call sites are not concerned about a `preferred_dc`,
        # but if ever they do care, these codepaths will need to be
        # updated accordingly.
        #
        @preferred_dc = nil
      end

      # Part of the cache key for references.
      #
      # This is the spokes checksum. It should be fresh every time this
      # method gets called.
      def repository_reference_key
        _read_checksum_from_db
      end

      # Read the checksum from the master replica.
      #
      # Use this when having the exact current checksum (or as close as
      # possible) is necessary.
      def read_checksum_from_db
        ActiveRecord::Base.connected_to(role: :writing) { _read_checksum_from_db }
      end

      # Look up routes for reading.  Or, more accurately, fetch the
      # already-pre-determined routes.
      #
      # Returns an array of GitRPC::Protocol::DGit::Route objects, sorted by
      # read_affinity.  A read should be directed to the first route in
      # the array.  Additional routes are provided so callers can
      # implement failover.  Callers do not need to implement load
      # balancing; any needed load balancing will be reflected in the
      # order of the returned array.
      #
      # Parameters:
      #  - preferred_dc: caller's preferred datacenter which influences
      #    the ordering of the returned routes.  When nil, the returned
      #    routes are ordered according to the global implicit
      #    `GitHub.datacenter`.
      def get_read_routes(preferred_dc: nil)
        #
        # Clear potentially memoized routes for `preferred_dc`.  There are
        # lots of call sites that implicitly populate this cache but do
        # not necessarily pass down a `preferred_dc`.
        #
        if (preferred_dc != @preferred_dc)
          @all_routes = nil
          @routes = nil
        end

        @routes ||= dgit_routes!(false, preferred_dc: preferred_dc)
        ret = @routes.select do |route|
          !GitHub::DGit.is_offline?(route.original_host) &&
          !GitHub::DGit.is_overloaded?(route.original_host)
        end
        ret.empty? ? @routes : ret
      end

      # Like #get_read_routes, but also include unhealthy (non-ACTIVE or bad-checksum) hosts.
      def get_all_routes(live_only: true, preferred_dc: nil)
        #
        # Clear potentially memoized routes for `preferred_dc`.  There are
        # lots of call sites that implicitly populate this cache but do
        # not necessarily pass down a `preferred_dc`.
        #
        if (preferred_dc != @preferred_dc)
          @all_routes = nil
          @routes = nil
        end

        @all_routes ||= dgit_routes!(true, preferred_dc: preferred_dc)
        return @all_routes unless live_only
        ret = @all_routes.select do |route|
          !GitHub::DGit.is_offline?(route.original_host) &&
          !GitHub::DGit.is_overloaded?(route.original_host)
        end
        ret.empty? ? @all_routes : ret
      end

      # Look up routes for writing.  Or, more accurately, fetch the
      # already-pre-determined routes.
      #
      # Returns an array of GitRPC::Protocol::DGit::Route objects,
      # sorted deterministically as per Route#<=> to avoid dining
      # philosophers deadlocks in 3PC. Writes must be directed to all
      # routes in the array.
      #
      # Parameters:
      #  - preferred_dc: caller's preferred datacenter which influences
      #    the ordering of the returned routes.  When nil, the returned
      #    routes are ordered according to the global implicit
      #    `GitHub.datacenter`.
      def get_write_routes(preferred_dc: nil)
        #
        # Clear potentially memoized routes for `preferred_dc`.  There are
        # lots of call sites that implicitly populate this cache but do
        # not necessarily pass down a `preferred_dc`.
        #
        if (preferred_dc != @preferred_dc)
          @all_routes = nil
          @routes = nil
        end

        @routes ||= dgit_routes!(false, preferred_dc: preferred_dc)

        # Filter for only believed-to-be-online hosts, unless that would
        # put us below a quorum.  Specifically, if one host is failed
        # (especially if its disk is hung), we'd rather skip the write to
        # it completely than hang for 1, 5, or 9 seconds trying to contact
        # it.
        ret = @routes.select do |route|
          !GitHub::DGit.is_offline?(route.original_host) &&
          !GitHub::DGit.is_overloaded?(route.original_host)
        end

        voters = ret.count(&:voting?)
        if voters < write_quorum
          ret = @routes
          voters = ret.count(&:voting?)
        end

        if voters < write_quorum
          msg = "#{voters} voting"
          non_voters = ret.length - voters
          msg << " (and #{non_voters} non-voting)" if non_voters > 0
          msg << " replicas available; #{write_quorum} required"
          raise InsufficientQuorumError, msg
        end
        ret.sort
      end

      # Return the number of replicas that have to agree for write operations.
      def write_quorum
        # FIXME: This should take into *all* replicas, including
        # offline ones. Not doing so means that we could potentially
        # get into a split-brain situation if the repository has more
        # than dgit_copies, but one or more are currently unhealthy.
        # Input `preferred_dc` as `@preferred_dc` such that cached
        # routes aren't inadvertently cleared from unknown call sites.
        @routes ||= dgit_routes!(false, preferred_dc: @preferred_dc)
        GitHub.dgit_quorum(@routes.count(&:voting?))
      end

      # Update checksums for a repo and its replicas as a write
      # operation proceeds.  It's legal to call this function several times.
      # Parameters:
      #  - network_id: network ID for the repository whose checksums are being updated.
      #  - repo_id: identifies the repository whose checksums are being updated.
      #  - replica_checksums: a hash of host => checksum for each host checksum
      #    that should be updated.  The host is associated with one of the
      #    routes returned by #get_write_routes, and the checksum is a SHA1 in
      #    version-prefixed hex40 form.  This parameter may be nil if no rows
      #    in `repository_replicas` need to be updated.
      #  - repo_checksum: a string with a SHA1 in version-prefixed hex40 form.
      #    This value is put in the `repositories` table as the desired checksum
      #    for all replicas of the repo.  This parameter may be nil if
      #    the master checksum doesn't need to be updated.
      def self.update_checksums(network_id, repo_id, is_wiki, replica_checksums, repo_checksum)
        db = GitHub::DGit::DB.for_network_id(network_id)
        repo_type = is_wiki ? GitHub::DGit::RepoType::WIKI : GitHub::DGit::RepoType::REPO
        ActiveRecord::Base.connected_to(role: :writing) do
          db.transaction do
            if replica_checksums && !replica_checksums.empty?
              sql = db.SQL.new network_id: network_id
              sql.add "UPDATE repository_replicas SET updated_at=NOW(), checksum ="
              if replica_checksums.values.uniq.size == 1
                sql.add ":sum", sum: replica_checksums.values.first
              else
                sql.add "CASE host"
                replica_checksums.each do |host, checksum|
                  sql.add "WHEN :host THEN :sum", host: host, sum: checksum
                end
                sql.add "END"
              end
              hosts = replica_checksums.keys
              sql.add "WHERE repository_id=:repo_id AND repository_type=:repository_type AND host IN :hosts",
                repo_id: repo_id, hosts: hosts, repository_type: repo_type
              sql.run
            end

            if repo_checksum
              sql = db.SQL.new <<-SQL, network_id: network_id, checksum: repo_checksum, repo_id: repo_id, repository_type: repo_type
                UPDATE repository_checksums
                  SET checksum=:checksum, updated_at=NOW()
                  WHERE repository_id=:repo_id AND repository_type=:repository_type
              SQL
              sql.run
            end
          end # transaction
        end # connected_to
      end

      # Update checksums for multiple repositories on a single host in a
      # single `UPDATE` query.
      # Parameters:
      #  - network_id: network ID corresponding to the given repositories
      #  - host: the host to update the checksums on; one of the routes
      #    returned by #get_write_routes.
      #  - replica_checksums: a hash of (repo_id, repo_type) => checksum that
      #    should be updated.  The key is a tuple of repo_id and repo_type,
      #    the checksum is a SHA1 in version-prefixed hex40 form.
      def self.update_checksums_for_host(network_id, host, replica_checksums)
        db = GitHub::DGit::DB.for_network_id(network_id)
        checksums_rows = ActiveRecord::Base.connected_to(role: :reading) do
          repo_ids = replica_checksums.map { |(repo_id, _), _| repo_id }
          db.SQL.results(<<-SQL, network_id: network_id, repo_ids: repo_ids, host: host, types: GitHub::DGit::RepoType::REPO_ISH)
            SELECT repository_id, repository_type, checksum
              FROM repository_replicas
              WHERE repository_id IN :repo_ids
                AND repository_type IN :types
                AND host = :host
          SQL
        end
        checksums_now = Hash[checksums_rows.map { |repo_id, repo_type, checksum| [[repo_id, repo_type], checksum] }]
        fail unless checksums_now.size == checksums_rows.size

        GitHub::DGit::RepoType::REPO_ISH.each do |type|
          repos_by_type = replica_checksums.select { |(repo_id, repo_type), checksum| repo_type == type }
          repos_to_change = repos_by_type.select { |key, newsum| checksums_now[key] != newsum }
          repos_to_change.each_slice(UPDATE_BATCH_SIZE) do |repo_batch|
            sql = db.SQL.new network_id: network_id
            sql.add "UPDATE repository_replicas SET updated_at = NOW(), checksum ="
            if repo_batch.size == 1
              sql.add ":sum", sum: repo_batch.first[1]
            else
              sql.add "CASE"
              repo_batch.each do |(repo_id, repo_type), checksum|
                sql.add "WHEN repository_id = :repo_id THEN :sum", repo_id: repo_id, sum: checksum
              end
              sql.add "END"
            end
            sql.add "WHERE repository_id IN :repo_ids AND repository_type = :repo_type AND host = :host",
              repo_ids: repo_batch.map { |(id, t), c| id }, repo_type: type, host: host
            sql.run
          end
        end
      end

      def self.update_gist_checksums(gist_id, replica_checksums, repo_checksum, host_to_activate: nil)
        ActiveRecord::Base.connected_to(role: :writing) do
          gist_db = GitHub::DGit::DB.for_gist_id(gist_id)
          gist_db.transaction do
            if replica_checksums && !replica_checksums.empty?
              sql = gist_db.SQL.new gist_id: gist_id
              sql.add "UPDATE gist_replicas SET updated_at = NOW(), checksum ="
              if replica_checksums.values.uniq.size == 1
                sql.add ":sum", sum: replica_checksums.values.first
              else
                sql.add "CASE host"
                replica_checksums.each do |host, checksum|
                  sql.add "WHEN :host THEN :sum", host: host, sum: checksum
                end
                sql.add "END"
              end
              hosts = replica_checksums.keys
              sql.add "WHERE gist_id = :gist_id AND host IN :hosts", hosts: hosts
              sql.run

              if host_to_activate && (replica_checksums[host_to_activate] == repo_checksum)
                gist_db.SQL.run(<<-SQL, active: GitHub::DGit::ACTIVE, gist_id: gist_id, host: host_to_activate)
                  UPDATE gist_replicas SET state = :active WHERE gist_id = :gist_id AND host = :host
                SQL
              end
            end

            if repo_checksum
              gist_db.SQL.run(<<-SQL, checksum: repo_checksum, gist_id: gist_id, repository_type: GitHub::DGit::RepoType::GIST)
                UPDATE repository_checksums
                  SET checksum=:checksum, updated_at=NOW()
                  WHERE repository_id=:gist_id AND repository_type=:repository_type
              SQL
            end
          end # transaction
        end # connected_to
      end

      def self.checksum_for_repo(network_id, repo_id, is_wiki)
        db = GitHub::DGit::DB.for_network_id(network_id)
        repo_type = is_wiki ? GitHub::DGit::RepoType::WIKI : GitHub::DGit::RepoType::REPO
        db.connection.uncached do
          sql = db.SQL.new \
            network_id: network_id,
            repo_id: repo_id,
            repository_type: repo_type
          sql.add <<-SQL
            SELECT checksum
              FROM repository_checksums
              WHERE repository_id=:repo_id AND repository_type=:repository_type
          SQL
          r = sql.results.first
          r && r.first
        end
      end

      def self.checksum_for_gist(gist_id)
        db = GitHub::DGit::DB.for_gist_id(gist_id)
        db.connection.uncached do
          sql = db.SQL.new \
            gist_id: gist_id,
            repository_type: GitHub::DGit::RepoType::GIST
          sql.add <<-SQL
            SELECT checksum
              FROM repository_checksums
              WHERE repository_id=:gist_id AND repository_type=:repository_type
          SQL
          r = sql.results.first
          r && r.first
        end
      end

      # Called for all failures that GitRPC chooses to suppress.  In
      # particular, any time a replica fails while another one succeeds,
      # GitRPC will return the successful answer and call `on_error` for
      # the failure(s).  Also, any time a GitRPC read call is unable to
      # connect to one or more backends, those connection failures will be
      # reported here.
      #
      # The purpose of this function is to mark the failing node as
      # misbehaving.  In the case of writes, 3PC or recompute_checksums
      # will advance all well-behaved nodes and leave the failed node for
      # repair.  In the case of reads, the node is simply not contacted
      # for a while.
      #
      # In any event, the exception goes to Failbot and graphite so it
      # doesn't get masked completely.
      def on_error(route, error)
        host = route.original_host
        GitHub::DGit.set_offline(host)
        GitHub.stats.increment "dgit.#{host}.rpc-error" if GitHub.enterprise?
        GitHub.dogstats.increment("dgit.rpc-error", tags: ["server:#{host}", "server_datacenter:#{route.datacenter}"])
        GitHub::Logger.log_exception({where: "#{namespace}##{@id}", host: host}, error)
      end

      # Called to report a warning.  GitRPC doesn't have direct access to
      # splunk or Failbot.
      def on_warning(error)
        GitHub::Logger.log_exception({where: "#{namespace}##{@id}"}, error)
      end

      # Called to record or resolve disagreements. The default
      # implementation should recompute checksums; if answers
      # disagree, then replicas may well have diverged.
      #
      # This method is overridden by derived classes
      def on_disagreement(answers, errors)
      end

      # Make GitRPC::Protocol::DGit use same-machine, fakerpc backends for
      # unit tests and replicate-repo.
      def use_fakerpc?
        Rails.test? || !!($0 =~ /replicate-repo/)
      end

    private
      # This default implementation is used for everything except
      # GistCreation:
      def dgit_routes!(all_hosts, preferred_dc: nil)
        #
        # Clear potentially memoized replicas for preferred_dc.  There
        # are lots of call sites that implicitly populate this cache but
        # do not necessarily pass down a `preferred_dc`.
        #
        if (preferred_dc != @preferred_dc)
          @replicas = nil
          @preferred_dc = preferred_dc
        end

        @replicas ||= dgit_replicas(preferred_dc: preferred_dc)
        Failbot.push delegate_replicas: @replicas.map(&:host).inspect

        # If we don't have any replicas or any checksum, this repository is
        # totally unknown to us. This should be a distinct condition from having
        # an insufficient number of healthy replicas.
        if @replicas.empty? && read_checksum_from_db.nil?
          raise NotFoundError, "#{namespace}/#{@id} unknown to spokes"
        end

        healthy_replicas = if all_hosts
          @replicas.select { |r| r.online? && !r.dormant? }
        else
          @replicas.select(&:healthy?)
        end

        if healthy_replicas.size < @replicas.size
          Failbot.push delegate_healthy_hosts: healthy_replicas.map(&:host).inspect
        end

        # Deal with fileservers.empty? now, but defer checking if
        # fileservers.length < quorum until we know if the rpc op is a
        # read or a write.
        raise UnroutedError, explain_failure(@replicas) if healthy_replicas.empty?

        # Make gitrpc routes for the @replicas.
        healthy_replicas.map { |replica| replica.to_route(@shard_path) }
      end

      def explain_failure(replicas)
        csum = replicas.empty? ? "" : replicas.first.expected_checksum
        csum ||= ""

        "no available servers for #{namespace}##{@id}: [#{csum[0..6]}], " +
          replicas.map { |rep|
            %Q{[#{rep.host},#{GitHub::DGit::STATES[rep.state][0]},#{rep.checksum[0..6]},#{rep.online? ? "on" : "off"}]}
          }.join(", ")
      end

      class Repository < Delegate
        attr_reader :network_id

        def initialize(network_id, id, shard_path, routes: nil)
          @network_id = network_id
          super(id, shard_path, routes: routes)
        end

        def namespace
          "repository"
        end

        # Read the repository checksum from the DB. For the answer to be
        # reliable, this method must be called while holding the
        # dgit-state lock and the caller must call this on the master
        # mysql replica.
        def _read_checksum_from_db
          Delegate.checksum_for_repo(@network_id, @id, false)
        end

        def write_checksums_to_db(replica_checksums, repo_checksum)
          GitHub::DGit::threepc_debug "update_checksums #{namespace}: (nw_id: #{@network_id}) #{@id}, " \
                                      "#{replica_checksums.inspect} " \
                                      "#{repo_checksum.inspect}"

          Delegate.update_checksums(@network_id, @id, false, replica_checksums, repo_checksum)
        end

        def on_disagreement(answers, errors)
          SpokesRecomputeChecksumsJob.perform_later(@id, :vote, false)
        end

        private

        def dgit_replicas(preferred_dc: nil)
          Routing::all_repo_replicas(@id, false, preferred_dc: preferred_dc)
        end
      end

      class Wiki < Delegate
        attr_reader :network_id

        def initialize(network_id, id, shard_path)
          @network_id = network_id
          super(id, shard_path)
        end

        def namespace
          "wiki"
        end

        # Read the wiki's checksum from the DB. For the answer to be
        # reliable, this method must be called while holding the
        # dgit-state lock and the caller must call this on the master
        # mysql replica.
        def _read_checksum_from_db
          Delegate.checksum_for_repo(@network_id, @id, true)
        end

        def write_checksums_to_db(replica_checksums, repo_checksum)
          GitHub::DGit::threepc_debug "update_checksums #{namespace}: (nw_id: #{@network_id}) #{@id}, " \
                                      "#{replica_checksums.inspect} " \
                                      "#{repo_checksum.inspect}"

          Delegate.update_checksums(@network_id, @id, true, replica_checksums, repo_checksum)
        end

        def on_disagreement(answers, errors)
          SpokesRecomputeChecksumsJob.perform_later(@id, :vote, true)
        end

        private

        def dgit_replicas(preferred_dc: nil)
          Routing::all_repo_replicas(@id, true, preferred_dc: preferred_dc)
        end
      end

      class Gist < Delegate
        def initialize(id, shard_path)
          super(id, shard_path)
        end

        def namespace
          "gist"
        end

        # Read the gist's checksum from the DB. For the answer to be
        # reliable, this method must be called while holding the
        # dgit-state lock and the caller must call this on the master
        # mysql replica.
        def _read_checksum_from_db
          Delegate.checksum_for_gist(@id)
        end

        def write_checksums_to_db(replica_checksums, repo_checksum)
          GitHub::DGit::threepc_debug "update_checksums #{namespace}: #{@id}, " \
                                      "#{replica_checksums.inspect} " \
                                      "#{repo_checksum.inspect}"

          Delegate.update_gist_checksums(@id, replica_checksums, repo_checksum)
        end

        def on_disagreement(answers, errors)
          SpokesRecomputeGistChecksumsJob.perform_later(@id, :vote)
        end

        private

        def dgit_replicas(preferred_dc: nil)
          Routing::all_gist_replicas(@id, preferred_dc: preferred_dc)
        end
      end

      class GistCreation < Delegate
        def initialize(shard_path, fileservers)
          routes = fileservers.map { |fileserver| fileserver.to_route(shard_path) }

          super(nil, shard_path, routes: routes)
        end

        def namespace
          "gist-creation"
        end

        def _read_checksum_from_db
          # During gist creation, there is no pre-checksum
          nil
        end

        def write_checksums_to_db(replica_checksums, repo_checksum)
          # During gist creation, we don't write the checksums to the DB:
        end

        def on_disagreement(answers, errors)
          # Report disagreements during creation: can't enqueue a recompute
          # checksums job at this stage because there is no such gist ID.
          GitHub::Logger.log message: "disagreement on gist creation",
                             answers: answers,
                             errors: errors
        end

        private

        def dgit_routes!(all_hosts, preferred_dc: nil)
          raise UnroutedError, "routes must be pre-defined"
        end
      end

      class Network < Delegate
        def initialize(id, shard_path)
          super(id, shard_path)
        end

        def namespace
          "network"
        end

        # Networks do not have checksums, but the parent class has methods
        # for accessing it.
        def _read_checksum_from_db
          nil
        end

        private

        def dgit_replicas(preferred_dc: nil)
          Routing::all_network_replicas(@id, preferred_dc: preferred_dc)
        end

        def explain_failure(replicas)
          "no available servers for network##{@id}: " +
            replicas.map { |rep|
              %Q{[#{rep.host},#{GitHub::DGit::STATES[rep.state][0]},#{rep.online? ? "on" : "off"}]}
            }.join(", ")
        end
      end

      module Removal
        def on_disagreement(answers, errors)
          nil
        end

        def get_write_routes
          get_all_routes
        end
      end

      class RepositoryRemoval < Repository
        include Removal
      end

      class WikiRemoval < Wiki
        include Removal
      end
    end
  end
end
