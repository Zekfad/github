# rubocop:disable Style/FrozenStringLiteralComment

# A handful of utility functions for Spokes, including its scripts.
# Please keep this file safe for `require ... config/basic` use in
# scripts, by not using ActiveRecord objects.

require "github/config/mysql"

module GitHub
  class DGit
    class Util
      def self.is_evacuating?(host)
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB::FS::SQL.value("SELECT evacuating FROM fileservers WHERE host=:host", host: host).to_i > 0
        end
      end

      def self.is_online?(host)
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB::FS::SQL.value("SELECT online FROM fileservers WHERE host = :host", host: host).to_i > 0
        end
      end

      # Keep in sync with GitHub::DGit::Fileserver.placement_zone
      def self.placement_zone(host)
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB::FS::SQL.value("SELECT rack FROM fileservers WHERE host = :host", host: host)
        end
      end

      def self.set_fileserver_evacuating(host, reason)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db.each do |db|
          rows_expected += 1
          rows_changed += db.SQL.run(<<-SQL, host: host, reason: reason).affected_rows
            UPDATE fileservers
               SET embargoed = 1, evacuating = 1, evacuating_reason = :reason, embargoed_reason = :reason
             WHERE host = :host
          SQL
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.unset_fileserver_evacuating(host)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db.each do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET evacuating = 0, evacuating_reason = NULL WHERE host = :host",
                                     host: host).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.set_fileserver_embargoed(host, reason)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db.each do |db|
          rows_expected += 1
          rows_changed += db.SQL.run(<<-SQL, host: host, reason: reason).affected_rows
            UPDATE fileservers
               SET embargoed = 1, embargoed_reason = :reason
             WHERE host = :host
          SQL
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.unset_fileserver_embargoed(host)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db.each do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET embargoed = 0, embargoed_reason = NULL WHERE host = :host",
                                     host: host).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.set_fileserver_quiescing(host, reason)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db.each do |db|
          rows_expected += 1
          rows_changed += db.SQL.run(<<-SQL, host: host, reason: reason).affected_rows
            UPDATE fileservers
               SET quiescing = 1, quiescing_reason = :reason
             WHERE host = :host
          SQL
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.unset_fileserver_quiescing(host)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db.each do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET quiescing = 0, quiescing_reason = NULL WHERE host = :host",
                                     host: host).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.set_fileserver_nonvoting(host, non_voting)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET non_voting = :non_voting WHERE host = :host",
                                     host: host, non_voting: non_voting).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.set_fileserver_location(host, datacenter, site, rack = nil)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET datacenter = :datacenter, rack = :rack, site = :site WHERE host = :host",
                                     host: host, datacenter: datacenter, rack: rack || GitHub::SQL::NULL, site: site).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.set_fileserver_online(host)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET online = 1 WHERE host = :host",
                                     host: host).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.set_fileserver_offline(host)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET online = 0 WHERE host = :host",
                                     host: host).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.delete_fileserver(host)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db.each do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("DELETE FROM fileservers WHERE host = :host",
                                            host: host).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.set_fileserver_hdd_storage(host)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET hdd_storage = 1 WHERE host = :host",
                                     host: host).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      def self.set_fileserver_ssd_storage(host)
        rows_changed = rows_expected = 0
        GitHub::DGit::DB.each_fileserver_db do |db|
          rows_expected += 1
          rows_changed += db.SQL.run("UPDATE fileservers SET hdd_storage = 0 WHERE host = :host",
                                     host: host).affected_rows
        end # each_fileserver_db
        rows_changed == rows_expected
      end

      REPLICA_DELETE_BATCH_SIZE = 100 # delete at most this many SQL rows at a time

      def self.delete_gist_replica_records(host)
        done = false
        total_deleted = 0
        GitHub::DGit::DB.each_gist_db do |db|
          while !done
            db.throttle do
              sql = db.SQL.new \
                host: host,
                limit: REPLICA_DELETE_BATCH_SIZE
              sql.add "DELETE FROM gist_replicas WHERE host = :host LIMIT :limit"
              sql.run
              total_deleted += sql.affected_rows
              done = sql.affected_rows < REPLICA_DELETE_BATCH_SIZE
            end # db.throttle
          end # while !done
        end # each_gist_db
        total_deleted
      end

      def self.delete_network_replica_records(host)
        done = false
        total_deleted = 0
        GitHub::DGit::DB.each_network_db do |db|
          while !done
            db.throttle do
              sql = db.SQL.new \
                host: host,
                limit: REPLICA_DELETE_BATCH_SIZE
              sql.add "DELETE FROM network_replicas WHERE host = :host LIMIT :limit"
              sql.run
              total_deleted += sql.affected_rows
              done = sql.affected_rows < REPLICA_DELETE_BATCH_SIZE
            end # db.throttle
          end # while !done
        end # each_network_db
        total_deleted
      end

      def self.delete_repo_replica_records(host)
        done = false
        total_deleted = 0
        GitHub::DGit::DB.each_network_db do |db|
          while !done
            db.throttle do
              sql = db.SQL.new \
                host: host,
                limit: REPLICA_DELETE_BATCH_SIZE
              sql.add "DELETE FROM repository_replicas WHERE host = :host LIMIT :limit"
              sql.run
              total_deleted += sql.affected_rows
              done = sql.affected_rows < REPLICA_DELETE_BATCH_SIZE
            end # db.throttle
          end # while !done
        end # each_network_db
        total_deleted
      end

      def self.remaining_gists_on_host(host)
        total_count = 0
        GitHub::DGit::DB.each_gist_db do |db|
          ActiveRecord::Base.connected_to(role: :reading) do
            count = db.SQL.value("SELECT COUNT(*) FROM gist_replicas WHERE host = :host",
                                 host: host)
            total_count += count unless count.nil?
          end # connected_to
        end # each_gist_db
        total_count
      end

      def self.remaining_networks_on_host(host)
        total_count = 0
        GitHub::DGit::DB.each_network_db do |db|
          ActiveRecord::Base.connected_to(role: :reading) do
            count = db.SQL.value("SELECT COUNT(*) FROM network_replicas WHERE host = :host",
                                 host: host)
            total_count += count unless count.nil?
          end # connected_to
        end # each_network_db
        total_count
      end

      def self.raw_fileserver_rows
        rows = GitHub::DGit::DB::FS::SQL.hash_results(<<-SQL)
          SELECT host, ip, online, embargoed, embargoed_reason, quiescing, quiescing_reason, evacuating, evacuating_reason, datacenter, rack, non_voting, hdd_storage
            FROM fileservers
        SQL
        rows.map { |row| Hash[row.map { |name, col| [name, col.is_a?(Integer) ? col>0 : col] }] }
      end

      def self.raw_fileserver_rows_for_host(host)
        GitHub::DGit::Util.raw_fileserver_rows.select { |row| row["host"] == host }
      end

      # An "inactive" fileserver is one that is offline,
      # or any of embargoed, quiescing, or evacuating.
      def self.raw_fileserver_rows_inactive
        GitHub::DGit::Util.raw_fileserver_rows.select { |row|
          !row["online"] || row["embargoed"] || row["quiescing"] || row["evacuating"]
        }
      end

      def self.raw_fileserver_rows_embargoed
        GitHub::DGit::Util.raw_fileserver_rows.select { |row| row["embargoed"] }
      end

      def self.raw_fileserver_rows_evacuating
        GitHub::DGit::Util.raw_fileserver_rows.select { |row| row["evacuating"] }
      end

      def self.fileserver_hosts_evacuating
        GitHub::DGit::Util.raw_fileserver_rows_evacuating.map { |row| row["host"] }
      end

      def self.raw_fileserver_rows_quiescing
        GitHub::DGit::Util.raw_fileserver_rows.select { |row| row["quiescing"] }
      end

      def self.all_fileserver_hosts
        GitHub::DGit::Util.raw_fileserver_rows.map { |row| row["host"] }
      end

      def self.online_gist_replica_hosts(gist_id)
        GitHub::DGit::DB.for_gist_id(gist_id).SQL.results(<<-SQL, gist_id: gist_id).flatten
          SELECT gist_replicas.host
            FROM gist_replicas, fileservers
           WHERE gist_replicas.gist_id = :gist_id
             AND gist_replicas.host = fileservers.host
             AND fileservers.online
        SQL
      end

      def self.online_network_replica_hosts(network_id)
        GitHub::DGit::DB.for_network_id(network_id).SQL.results(<<-SQL, network_id: network_id).flatten
          SELECT network_replicas.host
            FROM network_replicas, fileservers
           WHERE network_replicas.network_id = :network_id
             AND network_replicas.host = fileservers.host
             AND fileservers.online
        SQL
      end

      def self.gist_replicas_summary(gist_id)
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.for_gist_id(gist_id).SQL.hash_results(<<-SQL, gist_id: gist_id, repo_type: GitHub::DGit::RepoType::GIST)
            SELECT gr.host,
                   gr.state,
                   fs.online,
                   fs.non_voting,
                   rc.checksum AS expected,
                   gr.checksum AS actual
              FROM gist_replicas gr
              LEFT JOIN fileservers fs ON fs.host = gr.host
              LEFT JOIN repository_checksums rc ON gr.gist_id = rc.repository_id
                                               AND rc.repository_type = :repo_type
             WHERE gr.gist_id = :gist_id
          SQL
        end # connected_to
      end

      def self.repo_replicas_summary(network_id, repo_id, repo_type)
        ActiveRecord::Base.connected_to(role: :reading) do
          sql = GitHub::DGit::DB.for_network_id(network_id).SQL.new \
                  network_id: network_id, \
                  repo_id: repo_id, \
                  repo_type: repo_type
          sql.add <<-SQL
            SELECT nr.host,
                   nr.state,
                   fs.online,
                   fs.non_voting,
                   rc.checksum AS expected,
                   rr.checksum AS actual
              FROM network_replicas nr
              LEFT JOIN fileservers fs ON fs.host = nr.host
              LEFT JOIN repository_checksums rc ON rc.repository_id=:repo_id
                                               AND rc.repository_type=:repo_type
              LEFT JOIN repository_replicas rr  ON rr.repository_id=:repo_id
                                               AND rr.repository_type=:repo_type
                                               AND rr.host=nr.host
             WHERE nr.network_id=:network_id
          SQL
          sql.hash_results
        end # connected_to
      end

      def self.gist_ids_for_host(host)
        gist_ids = []
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_gist_db do |db|
            gist_ids << db.SQL.results(
                          "SELECT gist_id FROM gist_replicas WHERE host = :host  AND state <> :destroying",
                          host: host, destroying: GitHub::DGit::DESTROYING).flatten
          end
        end
        gist_ids.flatten.uniq
      end

      def self.network_ids_for_host(host)
        network_ids = []
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::DB.each_network_db do |db|
            network_ids << db.SQL.results("SELECT network_id FROM network_replicas WHERE host = :host AND state <> :destroying",
                                          host: host, destroying: GitHub::DGit::DESTROYING).flatten
          end
        end
        network_ids.flatten.uniq
      end

      def self.network_ids_on_hosts(hosts)
        ActiveRecord::Base.connected_to(role: :reading) do
          result_set = Set.new
          GitHub::DGit::DB.each_network_db do |db|
            sql = db.SQL.new("SELECT network_id FROM network_replicas WHERE host=:host", host: hosts[0])
            hosts[1..-1].each do |h|
              sql.add("AND network_id IN (SELECT network_id FROM network_replicas WHERE host=:host)", host: h)
            end
            result_set << sql.results.flatten
          end # each_network_db
          result_set.to_a.flatten
        end # connected_to
      end

      def self.gist_ids_on_hosts(hosts)
        ActiveRecord::Base.connected_to(role: :reading) do
          result_set = Set.new
          GitHub::DGit::DB.each_gist_db do |db|
            first_host, *rest_of_hosts = hosts
            sql = db.SQL.new("SELECT gist_id FROM gist_replicas WHERE host=:host", host: first_host)
            rest_of_hosts.each do |h|
              sql.add("AND gist_id IN (SELECT gist_id FROM gist_replicas WHERE host=:host)", host: h)
            end
            result_set << sql.results.flatten
          end
          result_set.to_a.flatten
        end
      end

      def self.gist_replica_state_on_host(gist_id, host)
        GitHub::DGit::DB.for_gist_id(gist_id).SQL.results(
          "SELECT state FROM gist_replicas WHERE gist_id = :gist_id AND host = :host",
          gist_id: gist_id, host: host)
      end

      def self.network_replica_state_on_host(network_id, host)
        GitHub::DGit::DB.for_network_id(network_id).SQL.results(
          "SELECT state FROM network_replicas WHERE network_id = :network_id AND host = :host",
          network_id: network_id, host: host)
      end

      def self.set_gist_replicas_checksum(gist_id, checksum)
        GitHub::DGit::DB.for_gist_id(gist_id).SQL.run(
          "UPDATE gist_replicas SET checksum = :checksum, updated_at = :now WHERE gist_id = :gist_id",
          gist_id: gist_id, checksum: checksum, now: GitHub::SQL::NOW)
      end

      def self.set_gist_replicas_checksum_on_host(gist_id, checksum, host)
        GitHub::DGit::DB.for_gist_id(gist_id).SQL.run(
          "UPDATE gist_replicas SET checksum = :checksum, updated_at = :now WHERE gist_id = :gist_id AND host = :host",
          gist_id: gist_id, checksum: checksum, now: GitHub::SQL::NOW, host: host)
      end

      def self.set_repo_replicas_checksum(network_id, repo_id, repo_type, checksum)
        GitHub::DGit::DB.for_network_id(network_id).SQL.run(<<-SQL, network_id: network_id, repo_id: repo_id, repo_type: repo_type, checksum: checksum, now: GitHub::SQL::NOW)
          UPDATE repository_replicas SET checksum = :checksum, updated_at = :now WHERE repository_id = :repo_id AND repository_type = :repo_type
        SQL
      end

      def self.set_repo_replicas_checksum_on_host(network_id, repo_id, repo_type, checksum, host)
        GitHub::DGit::DB.for_network_id(network_id).SQL.run(<<-SQL, network_id: network_id, repo_id: repo_id, repo_type: repo_type, checksum: checksum, now: GitHub::SQL::NOW, host: host)
          UPDATE repository_replicas SET checksum = :checksum, updated_at = :now WHERE repository_id = :repo_id AND repository_type = :repo_type AND host = :host
        SQL
      end

      def self.set_gist_replicas_state(gist_id, state)
        GitHub::DGit::DB.for_gist_id(gist_id).SQL.run(
          "UPDATE gist_replicas SET state = :state, updated_at = :now WHERE gist_id = :gist_id",
          gist_id: gist_id, state: state, now: GitHub::SQL::NOW).affected_rows
      end

      def self.set_gist_replicas_state_on_host(gist_id, state, host)
        GitHub::DGit::DB.for_gist_id(gist_id).SQL.run(
          "UPDATE gist_replicas SET state = :state, updated_at = :now WHERE gist_id = :gist_id AND host = :host",
          gist_id: gist_id, state: state, now: GitHub::SQL::NOW, host: host).affected_rows
      end

      def self.set_network_replicas_state(network_id, state)
        GitHub::DGit::DB.for_network_id(network_id).SQL.run(<<-SQL, network_id: network_id, state: state, now: GitHub::SQL::NOW).affected_rows
          UPDATE network_replicas SET state = :state, updated_at = :now WHERE network_id = :network_id
        SQL
      end

      def self.set_network_replicas_state_on_host(network_id, state, host)
        GitHub::DGit::DB.for_network_id(network_id).SQL.run(<<-SQL, network_id: network_id, state: state, now: GitHub::SQL::NOW, host: host).affected_rows
          UPDATE network_replicas SET state = :state, updated_at = :now WHERE network_id = :network_id AND host = :host
        SQL
      end

      def self.set_network_moving(network_id, value)
        ApplicationRecord::Domain::Repositories.github_sql.run("UPDATE repository_networks SET moving = :value WHERE id = :network_id",
                        network_id: network_id, value: value)
      end

      # Returns the most common checksum, if there's a clear majority.
      # Returns nil otherwise.
      #  - hash = a hash of { host => checksum, host => checksum, ... }
      def self.get_majority(hash)
        sums = hash.values   # [ 'aaa', 'bbb', 'aaa' ]
        return if sums.empty?
        collate = sums.group_by { |x| x }.map { |k, v| [k, v.size] }.sort_by { |x| -x[1] }  # [['aaa', 2], ['bbb', 1]]
        return collate.first.first if collate.size==1                # unanimous agreement
        return collate.first.first if collate[0][1] > collate[1][1]  # clear majority
        return nil                                                   # no majority
      end

      # Returns the unanimous checksum if there is a unanimous one.
      # Returns nil otherwise.
      #  - hash = a hash of { host => checksum, host => checksum, ... }
      def self.get_unanimous(hash)
        sums = hash.values   # [ 'aaa', 'bbb', 'aaa' ]
        return sums.first if sums.uniq.size == 1
      end

      # Return a list of repo IDs for the given network ID.
      def self.repo_ids_for(network_id, include_deleted: true, include_archived: false)
        raise "`include_archived` is an enterprise-only option" if include_archived && !GitHub.enterprise?
        sql = ApplicationRecord::Domain::Repositories.github_sql.new <<-SQL, network_id: network_id
          SELECT id
          FROM repositories
          WHERE source_id = :network_id
        SQL
        sql.add "AND active" unless include_deleted

        # Include archived repos on Enterprise only
        sql.add "UNION SELECT id FROM archived_repositories WHERE source_id = #{network_id}" if include_archived
        sql.results.flatten
      end

      def self.repo_count_for(network_id)
        ApplicationRecord::Domain::Repositories.github_sql.value("SELECT COUNT(*) FROM repositories WHERE source_id = :network_id",
                          network_id: network_id)
      end

      def self.load_datacenter_regions
        ActiveRecord::Base.connected_to(role: :reading) do
          @@datacenter_regions = begin
            sql = GitHub::DGit::DB::DC::SQL.run("SELECT datacenter, region FROM datacenters")
            sql.results.to_h
          rescue ActiveRecord::StatementInvalid
            raise if !GitHub.enterprise?
            # During GHE upgrade, dgit-cluster-restore-routes can get here
            # before migrations have run.  If the table doesn't exist yet,
            # treat it as empty.
            {}
          end
        end
      end

      def self.datacenter_region(dc)
        @@datacenter_regions ||= self.load_datacenter_regions
        @@datacenter_regions[dc]
      end

      def self.maintenance_status(network_id)
        ApplicationRecord::Domain::Repositories.github_sql.value("SELECT maintenance_status FROM repository_networks WHERE id = :network_id",
                          network_id: network_id)
      end

      def self.long_host(str)
        return unless str
        str.sub(/\Adfs-?[0-9a-f]+\z/, 'github-\0')
      end
    end
  end
end
