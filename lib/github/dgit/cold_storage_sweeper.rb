# rubocop:disable Style/FrozenStringLiteralComment

require "github/slice_query"

module GitHub::DGit::ColdStorageSweeper
  include SliceQuery

  def self.areas_of_responsibility
    :dgit
  end

  def self.query_namespace
    "dgit"
  end

  MAX_NETWORKS_PER_INTERVAL = 80
  SLICE_SIZE = 30 # look at 300,000 networks per interval
  PUSHED_AT_CUTOFF_MONTHS = 3
  MIN_HOST_FREE_SPACE_FRACTION = 0.1

  def self.enough_space?
    GitHub::DGit.cold_storage_dcs.each do |dc|
      possible = GitHub::DGit.get_fileservers(exclude_embargoed: true, in_datacenters: [dc], storage_class: :hdd)
                 .select { |x| x.fraction_of_space_available >= MIN_HOST_FREE_SPACE_FRACTION }
      possible_by_rack = possible.group_by(&:rack)
      return false if possible_by_rack.size < 2
    end
    true
  end

  def self.run_sweeper(max_networks: MAX_NETWORKS_PER_INTERVAL)
    if !enough_space?
      GitHub.dogstats.increment("dgit.actions.cold-storage.to-too-full")
      return
    end

    find_networks(max_networks).each do |(network_id, _)|
      if !GitHub::DGit::ColdStorage::mark_network_cold(network_id)
        # Failed to mark the network cold.  Back off.
        break
      end
      GitHub.dogstats.increment("dgit.actions.cold-storage.to")
    end
  end

  def self.find_from_table(table, domain_class, &block)
    slice_name = "dgit-freezes-#{table}"

    start_time, offset, slices = get_query_slice(slice_name)
    slices = SLICE_SIZE
    offset, next_offset = get_contiguous_id_range(table, domain_class, offset, slices)

    results = block_with_timeout(slice_name) do
      ActiveRecord::Base.connected_to(role: :reading) do
        block.call(offset, next_offset)
      end
    end

    if results.nil?
      return []
    end

    # If we were handed fewer ids than expected, we must
    # have hit the end of the table - wrap now
    # c.f. SliceQuery.calculate_adjusted_query_slice
    next_offset = 0 if next_offset - offset < slices * ROWS_PER_SLICE

    if results.size > 0
      # We found some work to do in this id range, so let the next
      # iteration pick up where we're leaving off.
      next_offset = results.last
    end
    put_adjusted_query_slice(slice_name, start_time, offset, next_offset, slices, no_adjustment: true)

    results
  end

  def self.get_slice_of_network_ids(offset, next_offset)
    ActiveRecord::Base.connected_to(role: :reading) do
      sql = RepositoryNetwork.github_sql.new \
        offset: offset,
        next_offset: next_offset,
        cutoff: PUSHED_AT_CUTOFF_MONTHS
      sql.add <<-SQL
        SELECT rn.id as network_id
          FROM repository_networks rn
         WHERE rn.id > :offset
           AND rn.id <= :next_offset
           AND rn.pushed_at < NOW() - INTERVAL :cutoff MONTH
         ORDER BY rn.id
      SQL
      sql.results.flatten
    end
  end

  # We ignore already cold networks (state == 1) and ones marked
  # to never be frozen (state == 2)
  def self.get_slice_of_networks_to_ignore(offset, next_offset)
    aggregate_results = []
    ActiveRecord::Base.connected_to(role: :reading) do
      GitHub::DGit::DB.each_network_db do |db|
        sql = db.SQL.new \
          offset: offset,
          next_offset: next_offset
        sql.add <<-SQL
          SELECT cn.network_id
            FROM cold_networks cn
           WHERE cn.network_id > :offset
             AND cn.network_id <= :next_offset
             AND cn.state IN (1, 2)
        SQL
        aggregate_results += sql.results.flatten
      end
    end
    aggregate_results
  end

  def self.find_networks(max_networks)
    find_from_table("repository_networks", ApplicationRecord::Domain::Repositories) do |offset, next_offset|
      ids = get_slice_of_network_ids(offset, next_offset)
      cold_ids = get_slice_of_networks_to_ignore(offset, next_offset)
      ids -= cold_ids
      ids = ids[0...max_networks]

      ids
    end
  end
end
