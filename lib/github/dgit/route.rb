# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class DGit
    # Quacks like a GitRPC::Protocol::DGit::Route.
    class Route
      DEV_PORTS = (8149..8168).to_a.freeze

      # Internal. See Fileserver#to_route.
      def initialize(fileserver, path, ports: nil, read_weight: nil, quiescing: false)
        @fileserver = fileserver
        @path = path
        @dev_ports = ports
        @read_weight = read_weight
        @quiescing = quiescing
        modify_routes_for_development!
      end

      # The fileserver object associated with this route.
      attr_reader :fileserver

      # The read weight of this route, if it came from a GitHub::DGit::Replica
      attr_reader :read_weight

      # Whether the route is on a fileserver that's quiescing or not
      attr_reader :quiescing

      # Builds a one-off RPC, usually for maintenance.
      # Normal app traffic should use a dgit delegate and GitRPC's dgit protocol.
      def build_maint_rpc
        ::GitRPC.new(rpc_url)
      end

      # The URL to use for GitRPC connections.
      def rpc_url
        if host == GitHub.local_git_host_name
          "file:#{path}"
        elsif Rails.development? || Rails.test?
          "fakerpc:#{path}" # path has already been modified for dev
        else
          "bertrpc://#{resolved_host}:#{port}#{path}" # path has already been modified for dev
        end
      end

      # Sort to avoid dining philosophers.
      def <=>(other)
        # Sort voting replicas before nonvoting replicas:
        [voting? ? 0 : 1, host, port] <=> [other.voting? ? 0 : 1, other.host, other.port]
      end

      def ==(other)
        original_host == other.original_host &&
        host == other.host &&
        port == other.port &&
        path == other.path
      end
      alias_method :eql?, :==

      def hash
        original_host.hash ^ port.hash
      end

      # The route's fileserver's name, according to the database
      def original_host
        fileserver.name
      end

      # The route's actual fileserver name.
      #
      # In dev/test, dgitX is changed to localhost. Everywhere else
      # this is the same as original_host.
      def host
        @host || original_host
      end
      attr_writer :host

      # Return the most specific thing possible that we can connect to.
      #
      # Order of preference goes like this:
      # * IP address
      # * Full hostname
      # * Short hostname
      def resolved_host
        fileserver.ip || GitHub::DNS.auto_fqdn(host)
      end

      def datacenter
        fileserver.datacenter
      end

      def voting?
        fileserver.contains_voting_replicas?
      end

      # The port where ernicorn is listening on the fileserver
      def port
        @port || 8149
      end
      attr_writer :port

      # The path to the repository on disk.
      attr_accessor :path

      # The remote_url, for repo->repo transfers
      def remote_url
        host == GitHub.local_git_host_name ? path : "#{resolved_host}:#{path}"
      end

      private

      def modify_routes_for_development!
        return unless Rails.development? || Rails.test?

        if host =~ /\Adgit(\d+)/
          @host = "localhost"
          @port = (@dev_ports || DEV_PORTS)[$1.to_i - 1]
          @path = DGit.dev_route(@path, original_host)
        end
      end
    end
  end
end
