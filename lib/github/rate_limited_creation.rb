# rubocop:disable Style/FrozenStringLiteralComment

# A simple mixin for AR models that should be rate limited.
#
# class Foo < ActiveRecord::Base
#   belongs_to :user
#   include GitHub::RateLimitedCreation
# end
module GitHub::RateLimitedCreation
  ERROR_MESSAGE = "was submitted too quickly"

  LIMITS = {
    user_minute:          80,
    user_hour:            500,
    installation_minute:  80,
    installation_hour:    500,
  }.freeze

  AUDIT_LOG_EVENT_KEY = :creation_rate_limit_exceeded

  def self.limits
    @limits ||= LIMITS
  end

  # Use custom rate limits for duration of the given block.
  #
  # custom_limits - A Hash of rate limit name Symbols and their corresponding
  #                 Integer values. Valid rate limit names are
  #                 :user_hour, :user_minute, :installation_hour, and
  #                 :installation_minute.
  #
  # Examples
  #
  #   use_custom_limits(:user_minute => 500, :user_hour => 500) do
  #     import_comments(comments, author: @current_user)
  #   end
  #
  # Returns nothing.
  def self.use_custom_limits(custom_limits, &blk)
    original_limits = limits
    @limits = limits.merge(custom_limits)
    yield
  ensure
    @limits = original_limits
  end

  # Disable content creation rate limits for the duration of the given block.
  #
  #     GitHub::RateLimitedCreation.disable_content_creation_rate_limits do
  #       ...
  #     end
  def self.disable_content_creation_rate_limits
    GitHub.content_creation_rate_limiting_enabled, orig =
      false, GitHub.content_creation_rate_limiting_enabled?
    yield
  ensure
    GitHub.content_creation_rate_limiting_enabled = orig
  end

  def check_creation_rate_limit
    return true unless GitHub.content_creation_rate_limiting_enabled?
    return true unless new_record?

    increment_rate_creation_stat("checked")

    if creation_rate_limited?
      errors.add :base, ERROR_MESSAGE
    end
  end

  def creation_rate_limited?(check_without_increment: false)
    @creation_rate_limited ||= {}
    if @creation_rate_limited.key?(check_without_increment)
      return @creation_rate_limited[check_without_increment]
    end

    if content_creation_rate_limit_whitelisted?
      increment_rate_creation_stat("whitelisted")
      return @creation_rate_limited[check_without_increment] = false
    end

    @creation_rate_limited[check_without_increment] =
      user_creation_rate_limit_exceeded?(check_without_increment: check_without_increment)
  end

  def content_creation_rate_limit_whitelisted?
    # In test, we sometimes validate rate-limited items that do not have a user
    # attached after the factory is done. This check for user existence
    # prevents exceptions during the validation step itself; if this happened
    # in "the real world" the nonexistent user will cause the item to be
    # invalid, and we'll let that validation take precedence.
    return true unless self.respond_to?(:user) && self.user
    user.content_creation_rate_limit_whitelisted?
  end

  def user_creation_rate_limit_exceeded?(check_without_increment: false)
    case user
    when Bot
      return false unless user.installation.present?
      check_content_creation_time_rates :installation, user.installation.id,
                                        skip_increment: check_without_increment
    when User
      check_content_creation_time_rates :user, user.id,
                                        skip_increment: check_without_increment
    end
  end

  def check_content_creation_time_rates(scope, id, skip_increment: false)
    check_content_creation_minute_rate(scope, id, skip_increment: skip_increment) ||
      check_content_creation_hour_rate(scope, id, skip_increment: skip_increment)
  end

  def check_content_creation_minute_rate(scope, id, skip_increment: false)
    limit_name = "#{scope}_minute"
    check_content_creation_rate(id, 1.minute, 1.minute, limit_name, skip_increment: skip_increment)
  end

  def check_content_creation_hour_rate(scope, id, skip_increment: false)
    limit_name = "#{scope}_hour"
    check_content_creation_rate(id, 1.hour, 30.minutes, limit_name, skip_increment: skip_increment)
  end

  def check_content_creation_rate(id, ttl, ban_ttl, limit_name, skip_increment: false)
    limit = GitHub::RateLimitedCreation.limits[limit_name.to_sym]
    key = GitHub::RateLimitedCreation.rate_key limit_name, id
    opt = {
      max_tries: limit + 1,
      ttl: ttl,
      ban_ttl: ban_ttl,
      check_without_increment: skip_increment,
      internal: true,
    }
    limiter = RateLimiter.check(key, **opt)
    record_creation_rate_limit_stats(limiter, limit_name) if limiter.at_limit?
    limiter.at_limit?
  end

  def record_creation_rate_limit_stats(rate_limiter, unit)
    increment_rate_creation_stat("per_#{unit}")

    # REF https://github.com/github/ecosystem-apps/issues/356#issuecomment-472147849
    context_user = respond_to?(:user) && user ? user : User.new

    context = creation_rate_limit_log_context(context_user)
              .merge("#{unit}_rate" => rate_limiter.tries)


    GitHub::Logger.log(context.merge(GitHub.context))

    # Only create an audit log event on the first event that's over the threshold
    if rate_limiter.tries == rate_limiter.max_tries
      context_user.instrument AUDIT_LOG_EVENT_KEY, context
    end
  end

  def creation_rate_limit_log_context(context_user)
    log_context_attributes = {
      rate_limited_creation: "#{self.class.to_s.underscore}",
      actor_id: context_user.id,
      actor: context_user.login,
    }

    if defined?(repository)
      log_context_attributes.merge(repo_id: repository.id, repo: repository.nwo)
    end

    log_context_attributes
  end

  def increment_rate_creation_stat(name)
    GitHub.dogstats.increment("rate_limited_creation", tags: ["subject:#{self.class.to_s.underscore}", "name:#{name}"])
  end

  def self.included(klass)
    klass.instance_eval do
      validate :check_creation_rate_limit
    end
  end

  # The memcache key name used for a rate limit
  def self.rate_key(*args)
    (["rate_limited_creation"] + args).join "-"
  end
end
