# frozen_string_literal: true

module GitHub
  module TokenScanning
    class FoundToken
      attr_reader :type, :token, :url, :report_url, :path, :processed, :error, :blob, :commit
      attr_reader :start_line, :end_line, :start_column, :end_column

      # Used to later report what was the last known state for a found
      # token after the processors have run.
      attr_accessor :state

      # Initialize a new FoundToken instance.
      #
      # token_info  - A Hash of data about the found token.
      #               :type         - The String type of the token (Eg.
      #                               "GENERIC_SHA1").
      #               :token        - The String token that was found.
      #               :url          - A URL where the leaked token can be found.
      #               :report_url   - URL to report found token to.
      #               :blob         - The blob where the token was found
      #               :commit       - The commit where the token was found
      #               :path         - Path to the file
      #               :start_line   - Start line number
      #               :end_line     - End line number
      #               :start_column - Start column number
      #               :end_column   - End column number
      #
      # Returns nothing.
      def initialize(type:, token:, url:, report_url:, blob: nil, commit: nil,
                     path: nil, start_line: nil, end_line: nil,
                     start_column: nil, end_column: nil)
        @type       = type
        @token      = token
        @url        = url
        @report_url = report_url
        @path       = path
        @blob       = blob
        @commit     = commit

        @start_line = start_line
        @end_line = end_line
        @start_column = start_column
        @end_column = end_column

        @state = :unknown
        @processed = false
        @error = nil
      end

      def report_stats(stats_tags)
        GitHub.dogstats.increment("token_scan.token", tags: stats_tags + [
          "token_type:#{@type}",
          "result:#{@state}",
          "processed:#{@processed}",
          "error:#{@error || 'nil'}",
        ])
      end

      # Mark this token as processed. This will be used for reporting.
      def mark_processed
        @processed = true
      end

      # Mark this token as error state, and save the error name.
      # used for reporting later in the process.
      def error=(error)
        self.state = :error
        @error = error.class.name.underscore
      end

      def token_hash
        Digest::SHA256.hexdigest(token.to_s)
      end
    end
  end
end
