# frozen_string_literal: true

require "token_scanning"

class GitHub::TokenScanning::Service::Client
  SERVICE_NAME = "token-scanning-service"
  FAILBOT_APP_NAME = "token-scanning-service"

  ERRORS_TO_IGNORE = [
    Timeout::Error,
    Faraday::TimeoutError,
    Faraday::SSLError,
    Faraday::ClientError,
    Faraday::ConnectionFailed,
  ].freeze

  def self.connection
    @connection ||= Faraday.new(url: GitHub.token_scanning_url) do |conn|
      conn.use GitHub::FaradayMiddleware::RequestID
      conn.use GitHub::FaradayMiddleware::HMACAuth, hmac_key: GitHub.token_scanning_hmac_key
      conn.use GitHub::FaradayMiddleware::Datadog, stats: GitHub.dogstats, service_name: SERVICE_NAME
      conn.use GitHub::FaradayMiddleware::Tracer,
        service_name: SERVICE_NAME,
        parent_span: proc { GitHub.tracer.last_span },
        tracer: GitHub.tracer,
        # If we don't specify the operation, it defaults to "POST", in
        # which case LightStep produces an operation that filters the last
        # component of the URL.  Since in Twirp, that's the RPC method
        # name, which we want to keep, we set the operation explicitly
        # ourselves.
        operation: proc { |env| "#{env[:method].to_s.upcase} #{URI(env[:url]).path}" }
      conn.use GitHub::FaradayMiddleware::Staffbar, url: GitHub.token_scanning_url
      conn.use Faraday::Resilient, name: SERVICE_NAME, options: {
        instrumenter: GitHub,
        sleep_window_seconds: 10,
        error_threshold_percentage: 5,
        window_size_in_seconds: 30,
        bucket_size_in_seconds: 5,
      }
      conn.options[:open_timeout] = 0.1 # connection open timeout in seconds.
      conn.options[:timeout] = 1.0 # seconds
      conn.adapter :persistent_excon
    end
  end

  def self.client
    @client ||= ::TokenScanning::Client.new(connection)
  end

  def self.with_error_reporting
    response = yield
    if response.nil?
      Failbot.report(StandardError.new("Nil response"), { app: FAILBOT_APP_NAME })
    elsif response.error && response.error.code != :not_found
      Failbot.report(StandardError.new(response.error.msg), { app: FAILBOT_APP_NAME })
    end
    response
  rescue *ERRORS_TO_IGNORE
    nil
  rescue RuntimeError => e
    Failbot.report(e, { app: FAILBOT_APP_NAME })
    nil
  end

  def self.get_tokens(options)
    with_error_reporting { client.get_tokens(options) }
  end

  def self.get_token(options)
    with_error_reporting { client.get_token(options) }
  end

  def self.resolve_token(options)
    with_error_reporting { client.resolve_token(options) }
  end

  def self.get_token_counts(options)
    with_error_reporting { client.get_token_counts(options) }
  end

  def self.wrap_tokens(tokens, repository)
    tokens.map { |token| wrap_token(token, repository) }
  end

  def self.wrap_token(token, repository)
    GitHub::TokenScanning::Service::Token.new(token, repository)
  end

  def self.to_resolution(resolution)
    resolution = resolution.to_s.to_sym.upcase
    resolved_resolution = ::TokenScanning::Proto::Resolution.resolve(resolution)
    if resolved_resolution != ::TokenScanning::Proto::Resolution::NO_RESOLUTION
      resolved_resolution
    end
  end
end
