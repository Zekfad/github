# frozen_string_literal: true

# Wrapper class for the protobuf token location class returned from the service.
#
# Implements the same methods as `TokenScanResultLocation` so that is can be
# use interchangeably in the views.
class GitHub::TokenScanning::Service::TokenLocation
  attr_reader :location, :repository

  delegate :blob_oid, :commit_oid, :end_column, :end_line, :path, :start_column, :start_line, to: :location

  def initialize(location, repository)
    @location = location
    @repository = repository
  end

  def found_in_archive?
    false
  end

  def exclude_by_path?
    false
  end

  def blob
    return @blob if defined? @blob

    raw_blob = repository.rpc.read_blobs([blob_oid]).first
    @blob = TreeEntry.new(repository, raw_blob.merge("path" => path))
  rescue GitRPC::ObjectMissing
    @blob = nil
  end

  def normalized_start_column
    start_column + 1
  end

  def normalized_end_column
    end_column + 1
  end
end
