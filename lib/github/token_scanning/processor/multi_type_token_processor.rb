# frozen_string_literal: true

module GitHub
  module TokenScanning
    class MultiTypeTokenProcessor < ThirdPartyTokenProcessor
      # Aggregate tokens by file path and check if all provided types are
      # found in the same file. If not, ignore the found tokens for that file.
      def pre_process_target_by_type_group_all
        tokens_to_process = []
        @found_tokens.group_by(&:path).each_value do |tokens|
          if formats == tokens.group_by(&:type).keys.to_set
            tokens_to_process += tokens
          end
        end
        @found_tokens.each do |token|
          unless tokens_to_process.include?(token)
            token.state = :ignored
          end
        end
      end

      # Check to ensure that at least one of the types was found in this scan.
      # Otherwise ignore the all found tokens
      def pre_process_target_by_type_group_single
        types = @found_tokens.group_by(&:type).keys
        if formats != types.to_set
          set_tokens_state(@found_tokens, :ignored)
        end
      end

      # Check that a set of token types was found in the same file.
      # Otherwise ignore.
      def pre_process_by_format_sets
        tokens_to_process = []

        @found_tokens.group_by(&:path).each_value do |tokens|
          file_tokens_type = tokens.collect(&:type).to_set
          pair_match = format_sets.any? do |format_pair|
            format_pair.subset?(file_tokens_type)
          end

          if pair_match
            tokens_to_process += tokens
          end
        end

        @found_tokens.each do |token|
          unless tokens_to_process.include?(token)
            token.state = :ignored
          end
        end
      end
    end
  end
end
