# frozen_string_literal: true

module GitHub
  module TokenScanning
    class HubspotTokenProcessor < MultiTypeTokenProcessor
      FORMATS = Set.new(%w(HUBSPOT_HAPIKEY_NAME_PRESENCE HUBSPOT_HAPIKEY)).freeze
      register_processor_for FORMATS

      # Processes found tokens.
      #
      # We see an incredible number of false positives for Hubspot needs speciail handling
      # as it has two linked patterns for the token.
      # See the pre_process_target method.
      #
      # Returns nothing.
      def process
        pre_process_target_by_type_group_all
        super
      end

      def formats
        FORMATS
      end

    end
  end
end
