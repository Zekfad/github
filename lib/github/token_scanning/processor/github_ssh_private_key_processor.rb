# frozen_string_literal: true

module GitHub
  module TokenScanning
    class GitHubSSHPrivateKeyProcessor < Processor
      include GitHub::TokenScanning::TokenScanningPostProcessingHelper

      class SSHKeyGenAbnormalExitError < StandardError; end

      FORMATS = Set.new(%w(ARMORED_PEM_PRIVATE_KEY)).freeze
      register_processor_for FORMATS

      # Processes found tokens.
      #
      # Returns nothing.
      def process
        return unless process_repo_externally?

        fingerprints = ssh_key_fingerprints(@found_tokens.map(&:token))
        keys = public_keys_for_fingerprints(fingerprints.values.compact)

        @found_tokens.each do |token|
          fingerprint = fingerprints[token.token]
          unverify_and_notify(token, keys[fingerprint])
        end
      end

      # Revoke oauth access and notify the owner.
      #
      # ssh_key - The PublicKey associated with this SSH token, if any.
      #
      # Retruns nothing.
      def unverify_and_notify(token, public_key)
        unless token.url.present?
          token.state = :no_url
          return
        end

        if public_key.nil?
          token.state = :no_public_key
          return
        end

        if public_key_already_unverified?(public_key)
          token.state = :already_unverified
          return
        end

        unverify_and_notify_public_key(public_key, in_gist?, token.url)
        token.state = :unverified
        token.mark_processed
      rescue => e
        token.error = e
        Failbot.report(e)
      end
    end
  end
end
