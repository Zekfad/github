# frozen_string_literal: true

module GitHub
  module TokenScanning
    class MessageBirdTokenProcessor < MultiTypeTokenProcessor
      include GitHub::TokenScanning::TokenScanningPostProcessingHelper

      FORMATS = Set.new(%w(MESSAGEBIRD_NAME_PRESENCE MESSAGEBIRD_TOKEN)).freeze
      register_processor_for FORMATS

      def process
        pre_process_target_by_type_group_all
        pre_process_by_variability
        super
      end

      def pre_process_by_variability
        # Value comes from experimentation by MessageBird
        variability_lower_bound = 4

        @found_tokens.each do |token|
          unless token.type == "MESSAGEBIRD_NAME_PRESENCE"
            trimmed_token = token.token[-25, 25] # Removes prefix of MESSAGEBIRD_TOKEN_2
            unless variability(trimmed_token) > variability_lower_bound
              token.state = :ignored
            end
          end
        end
      end

      def formats
        FORMATS
      end
    end
  end
end
