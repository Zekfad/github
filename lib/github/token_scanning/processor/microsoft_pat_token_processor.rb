# frozen_string_literal: true

module GitHub
  module TokenScanning
    class MicrosoftPATTokenProcessor < ThirdPartyTokenProcessor
      include GitHub::TokenScanning::TokenScanningPostProcessingHelper

      FORMATS = Set.new(%w(MICROSOFT_VSTS_PAT)).freeze

      register_processor_for FORMATS

      # Processes found VSTS_PAT tokens by filtering them out based on a CRC check
      def process
        @found_tokens.each do |token|
          token.state = :ignored unless valid_vsts_pat_crc?(token.token)
        end

        super

      end
    end
  end
end
