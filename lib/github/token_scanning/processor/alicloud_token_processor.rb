# frozen_string_literal: true

module GitHub
  module TokenScanning
    class AliCloudTokenProcessor < MultiTypeTokenProcessor
      FORMAT_SETS = [
        Set.new(%w(ALICLOUD_ACCESS_KEY ALICLOUD_SECRET_KEY)),
        Set.new(%w(ALICLOUD_ACCESS_KEY_LEGACY ALICLOUD_SECRET_KEY)),
      ].freeze
      register_processor_for(FORMAT_SETS.map(&:to_a).flatten.uniq)

      # Processes found tokens differently for public and private repos
      #
      # Returns nothing.
      def process
        if process_repo_externally?
          pre_process_public_target
        else
          pre_process_by_format_sets
        end
        super
      end

      # For public repos, check to ensure that at least one pair of tokens was found together
      # Otherwise ignore the all found tokens that are processed by this processer (don't mark unrelated tokens ignored)
      def pre_process_public_target
        types = @found_tokens.collect(&:type).to_set
        types_set = types.to_set

        pair_match = FORMAT_SETS.any? do |format_pair|
          format_pair.subset?(types_set)
        end

        unless pair_match
          set_tokens_state(@found_tokens, :ignored)
        end
      end

      def format_sets
        FORMAT_SETS
      end

    end
  end
end
