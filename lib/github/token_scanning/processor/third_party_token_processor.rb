# frozen_string_literal: true

module GitHub
  module TokenScanning
    class ThirdPartyTokenProcessor < Processor
      register_default_processor

      class HTTPStatusNotOkError < StandardError
        attr_reader :status
        def initialize(msg = "http status is not 200", status = "nil")
          @status = status
          super(msg)
        end
      end

      MAX_TRIES = 4
      KEY_NAME = "token-scanning-signing-key"
      # The below header names were chosen to be consistent with a pattern
      # established by hookshot for signing hooks.
      PUBLIC_KEY_ID_HEADER = "GITHUB-PUBLIC-KEY-IDENTIFIER"
      PUBLIC_KEY_SIGNATURE_HEADER  = "GITHUB-PUBLIC-KEY-SIGNATURE"


      def self.earthsmoke_key
        GitHub.earthsmoke.low_level_key(KEY_NAME)
      end

      # Processes found tokens.
      #
      # This does nothing by default and *may* be implemented by subclasses.
      #
      # Returns nothing.
      def process
        @found_tokens.group_by(&:report_url).each do |report_url, tokens|
          tokens = tokens.select { |token| token.state != :ignored }
          unless tokens.empty?
            process_group(report_url, tokens)
          end
        end
      end

      def process_group(report_url, tokens)
        if report_url.blank?
          set_tokens_state(tokens, :no_report_url)
          return
        end

        if process_repo_externally?
          notify_external_third_party(report_url, tokens)
        else
          notify_owners_by_email(tokens)
        end

        set_tokens_state(tokens, :notified)
        tokens.each(&:mark_processed)
      rescue Faraday::Error, HTTPStatusNotOkError => e
        set_tokens_error(tokens, e)
        # Report Faraday and non-20X errors to github-external-request bucket to
        # not create noise in main bucket when one of the URLs being reported to
        # might be down or having issues
        Failbot.report(e, app: "github-external-request")
      rescue => e
        set_tokens_error(tokens, e)
        Failbot.report(e)
      end

      def notify_external_third_party(report_url, tokens)
        return if GitHub.skip_secret_scanning_third_party_reporting?

        signature = nil
        public_key_identifier = nil
        body = JSON.dump(
          tokens.map do |t|
            { type: t.type.downcase, token: t.token, url: t.url }
          end,
        )

        # Fail open on signature generation
        begin
          resp = self.class.earthsmoke_key.sign(message: body, raw: true)
          signature = Base64.strict_encode64(resp.signature)
          public_key_identifier = key_identifier(resp.key_version_id)
        rescue ::Earthsmoke::Error => e
          Failbot.report(e)
        end

        with_retries(MAX_TRIES, report_url) do
          with_stats(report_url) do
            resp = faraday.post(report_url) do |req|
              req.headers["Content-Type"] = "application/json"
              req.headers[PUBLIC_KEY_SIGNATURE_HEADER] = signature
              req.headers[PUBLIC_KEY_ID_HEADER] = public_key_identifier
              req.body = body
            end
            # Use an exception to signal higher level logic to retry any HTTP
            # request that didn't result in a HTTP 2xx success status
            unless resp.success?
              raise HTTPStatusNotOkError.new("Expected HTTP 2xx for #{report_url} but got #{resp.status}", resp.status)
            end
            resp
          end
        end
      end

      def notify_owners_by_email(tokens)
        # Only notify private repositories
        unless in_gist?
          # `tokens.first.type` here is used, because all tokens are aggregated by report_url
          # which means they should all have the same type
          AccountMailer.third_party_token_leaked(@target, tokens.map(&:url).uniq, tokens.first.type).deliver_now
        end
      end

      private

      def set_tokens_state(tokens, state)
        tokens.each do |token|
          token.state = state
        end
      end

      def set_tokens_error(tokens, error)
        tokens.each do |token|
          token.error = error
        end
      end

      def faraday
        @faraday ||= Faraday.new(request: {timeout: 5, open_timeout: 5}) do |c|
          c.adapter Faraday.default_adapter
        end
      end

      def with_stats(url)
        t_start = Time.now
        resp = yield
      rescue => e
        error = e
        raise
      ensure
        t_stop = Time.now
        t_delta = t_stop - t_start

        errname = error.nil? ? "nil" : error.class.name.underscore
        status = resp.nil? ? "nil" : resp.status

        GitHub.dogstats.timing("token_scan.webhook.time", t_delta, tags: [
          "error:#{errname}",
          "status:#{status}",
          "url:#{url}",
        ])
      end

      def with_retries(max_tries, url, catching: [Faraday::Error, HTTPStatusNotOkError, URI::InvalidURIError])
        try = 1
        begin
          error = nil
          yield
        rescue *Array(catching) => e
          error = e
          if try < max_tries
            sleep(2 ** try)
            try += 1
            retry
          else
            raise
          end
        rescue => e
          error = e
          raise
        end
      ensure
        errname = error.nil? ? "nil" : error.class.name.underscore
        errstatus = error.nil? || error.class.name != HTTPStatusNotOkError.name ? "nil" : error.status
        GitHub.dogstats.increment("token_scan.webhook.try", tags: [
          "error:#{errname}",
          "try:#{try}",
          "url:#{url}",
          "status:#{errstatus}",
        ])
      end

      def key_identifier(key_version_id)
        key_version = self.class.earthsmoke_key.export_version(key_version_id)
        return unless key_version
        Digest::SHA256.hexdigest(key_version.parsed_key_pair.public_key.to_pem)
      end
    end
  end
end
