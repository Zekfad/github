# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module Billing
      class MeteredLineItemUpdatedProcessor < BaseProcessor
        DEFAULT_GROUP_ID = "metered_line_item_updated_processor"
        DEFAULT_SUBSCRIBE_TO = /github\.billing\.v0\.MeteredLineItemUpdated\Z/

        areas_of_responsibility :gitcoin

        options[:min_bytes] = 1
        options[:max_wait_time] = 0.2.seconds
        options[:session_timeout] = 60.seconds
        options[:start_from_beginning] = false
        options[:max_bytes_per_partition] = 100.kilobytes

        # Public: Configure the Hydro processor
        def setup(**kwargs)
          options[:group_id] ||= DEFAULT_GROUP_ID
          options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

          self.metric_prefix = group_id
          self.dead_letter_topic = "github.billing.v0.MeteredLineItemUpdated.DeadLetter"
        end

        # Public: Process a single Hydro message
        #
        # message - The Hydro message to process
        #
        # Returns nothing
        def process_message(message)
          return message.skip("feature_flag_disabled") unless GitHub.flipper[:metered_line_item_updated_processor].enabled?

          line_item_class = case message.value[:metered_product]
          when :ACTIONS then ::Billing::ActionsUsageLineItem
          when :PACKAGES then ::Billing::PackageRegistry::DataTransferLineItem
          when :STORAGE then ::Billing::SharedStorage::ArtifactAggregation
          else return message.skip("unknown_metered_product")
          end

          # Run query against the primary database to avoid missing a record due to replication lag
          line_item = ActiveRecord::Base.connected_to(role: :writing, prevent_writes: true) do
            line_item_class.find_by(id: message.value[:line_item_id])
          end
          return message.skip("line_item_missing") if line_item.nil?

          policy = ::Billing::Zuora::UsageFile::LineItemSubmissionPolicy.new(line_item)

          if time("submission_policy") { policy.submittable? }
            serializer = ::Billing::Zuora::UsageFile::LineItemSerializer.new(
              line_item: line_item,
              product: message.value[:metered_product],
            )
            csv_data = time("serialize") { serializer.to_csv.chomp }

            payload = {
              metered_product: message.value[:metered_product],
              line_item_id: message.value[:line_item_id],
              csv_data: csv_data,
            }

            GlobalInstrumenter.instrument("billing.serialized_metered_line_item", payload)
          else
            line_item_class.throttle_with_retry(max_retry_count: 5) do
              safe_trigger_heartbeat
              line_item.update!(
                submission_state: :skipped,
                submission_state_reason: policy.reason,
              )
            end
          end
        end

        private

        def error_context_for_message(message)
          message.value.slice(:metered_product, :line_item_id)
        end
      end
    end
  end
end
