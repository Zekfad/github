# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module Billing
      class SerializedMeteredLineItemProcessor < BaseProcessor
        DEFAULT_GROUP_ID = "serialized_metered_line_item_processor"
        DEFAULT_SUBSCRIBE_TO = /github\.billing\.v0\.SerializedMeteredLineItem\Z/
        BUCKET_NAME = "billing-usage-batches"
        MAX_SLEEP_ON_RETRY = 5
        MAX_SHA_MISMATCH_TRIES = 100

        SHA1MismatchError = Class.new(StandardError)

        areas_of_responsibility :gitcoin

        options[:min_bytes] = 1
        options[:max_wait_time] = 0.2.seconds
        options[:session_timeout] = 60.seconds
        options[:start_from_beginning] = false
        options[:max_bytes_per_partition] = 100.kilobytes

        attr_reader :metric_prefix

        def setup(max_file_size: 4.megabytes, bucket_name: BUCKET_NAME, max_sha_retries: MAX_SHA_MISMATCH_TRIES, **kwargs)
          options[:group_id] ||= DEFAULT_GROUP_ID
          options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

          self.metric_prefix = options[:group_id]
          self.dead_letter_topic = "github.billing.v0.SerializedMeteredLineItem.DeadLetter"

          @max_file_size = max_file_size
          @bucket_name = bucket_name
          @sha1 = Digest::SHA1.new
          @max_sha_retries = max_sha_retries
          @batches_processed = 0
        end

        def before_batch(batch)
          # store the result of calling enabled? so that it can be consistently the same value across
          # the batch.
          # NOTE: This really only applies when the feature flag % is anything other than 0 or 100
          @ff_enabled = GitHub.flipper[:serialized_metered_line_item_processor].enabled?
          return unless @ff_enabled

          setup_run(batch)
        end

        def process_message(message)
          return message.skip("feature_flag_disabled") unless @ff_enabled

          if buffer.size >= max_file_size_with_buffer
            record_batch_switch("batch_size", partition)
            finish_batch
          end

          # Make sure we're appending to the buffer
          buffer.seek(0, IO::SEEK_END)

          buffer.puts(message.value[:csv_data].chomp)
          publish_metered_line_item_submitted(message, usage_batch.id)
        end

        def after_batch(_batch)
          @batches_processed += 1
          return unless @ff_enabled

          record_batch_metrics
          upload_to_s3
          check_batch_age
        end

        def after_close
          if @ff_enabled
            upload_to_s3
          end
        end

        private

        attr_reader :partition, :buffer, :usage_batch, :bucket_name, :sha1, :max_sha_retries, :batches_processed

        def error_context_for_message(message)
          message.value.slice(:metered_product, :line_item_id)
        end

        def record_batch_switch(reason, partition)
          GitHub.dogstats.event("#{metric_prefix}.batch_switched", "SerializedMeteredLineItemProcessor started a new Billing::UsageSynchronizationBatch", tags: ["reason:#{reason}", "partition:#{partition}"])
        end

        def setup_run(batch)
          GitHub.dogstats.time("#{metric_prefix}.setup_run") do
            # We need to check whether or not the partition we're currently processing has change
            # due to a kafka rebalance
            # if it has, we need to commit the current work and switch over to the new partition
            # and download that file and continue processing the messages for that partition
            check_partition_change(batch)

            @partition ||= batch.first.partition
            @usage_batch ||= get_or_create_usage_batch
          end
        end

        def check_partition_change(batch)
          # If this is true we need to reset and clean up everything like we have just booted up
          # We likely don't need to worry about uploading to S3, since we do that after each hydro batch
          if partition && partition != batch.first.partition
            record_batch_switch("partition_changed", partition)
            @partition = nil
            @buffer = nil
            @usage_batch = nil
            @batches_processed = 0
          end
        end

        def check_batch_age
          if usage_batch.created_at <= 6.hours.ago
            record_batch_switch("batch_age", partition)
            finish_batch
          end
        end

        def finish_batch
          upload_to_s3

          ::Billing::UsageSynchronizationBatch.throttle_with_retry(max_retry_count: 5) do
            safe_trigger_heartbeat
            usage_batch.ready_to_submit!
          end

          @usage_batch = get_or_create_usage_batch
        end

        def get_or_create_usage_batch
          ActiveRecord::Base.connected_to(role: :writing) do
            # We want the most recent batch that meets the criteria
            usage_batch = ::Billing::UsageSynchronizationBatch.where(
              product: :all,
              status: :building,
              partition: partition,
            ).order(id: :desc).first

            if usage_batch
              # if usage batch exists we want to grab the file from S3 and use that
              # instead of the recently created file
              @buffer = download_from_s3(usage_batch)
              return usage_batch unless @buffer.nil?
            end

            @buffer = initialize_buffer

            today = GitHub::Billing.today
            filename = "#{partition}_#{today}_#{SecureRandom.hex(4)}.csv"

            ::Billing::UsageSynchronizationBatch.throttle_with_retry(max_retry_count: 5) do
              safe_trigger_heartbeat
              ::Billing::UsageSynchronizationBatch.create!(
                product: :all,
                status: :building,
                partition: partition,
                upload_filename: filename,
              )
            end
          end
        end

        def initialize_buffer
          io = StringIO.new
          io.puts(CSV.generate_line(::Billing::Zuora::UsageFile::LineItemSerializer::CSV_HEADERS))
          io
        end

        def upload_to_s3
          digest = sha1.hexdigest(buffer.string)

          object = Aws::S3::Resource.new(client: s3_client).
            bucket(bucket_name).
            object(usage_batch.upload_filename)

          retry_s3_operation("put") do
            object.put(
              body: buffer.string,
              content_type: "text/csv",
            )
          end

          key = "usage-synchronization-batch-#{usage_batch.id}-sha1"
          GitHub.kv.set(key, digest, expires: 1.day.from_now)
        end

        def download_from_s3(usage_batch)
          upload_filename = usage_batch.upload_filename
          object = Aws::S3::Resource.new(client: s3_client).
            bucket(bucket_name).
            object(upload_filename)

          io = StringIO.new
          begin
            retry_s3_operation("get") do
              object.get(response_target: io)

              # check the digest of the file to make sure we
              # have the right s3 file contents.
              # We do this check because of the eventual
              # consistency with the s3 storage model, sometimes
              # older/cached data may be returned.
              key = "usage-synchronization-batch-#{usage_batch.id}-sha1"
              digest_from_kv = GitHub.kv.get(key).value!
              digest_from_s3 = sha1.hexdigest(io.string)

              unless digest_from_kv == digest_from_s3
                raise SHA1MismatchError.new
              end
            rescue Aws::S3::Errors::NoSuchKey
              return nil
            end
          end

          io
        rescue SHA1MismatchError
          return nil
        end

        def s3_client
          GitHub.s3_billing_client
        end

        def publish_metered_line_item_submitted(message, batch_id)
          payload = {
            metered_product: message.value[:metered_product],
            line_item_id: message.value[:line_item_id],
            synchronization_batch_id: batch_id,
          }

          GlobalInstrumenter.instrument("billing.metered_line_item_submitted", payload)
        end

        def max_file_size_with_buffer
          0.95 * @max_file_size
        end

        def retry_s3_operation(operation)
          GitHub.dogstats.time("#{metric_prefix}.s3_operation", tags: ["operation:#{operation}"]) do
            tries = 0
            sha_mismatch_tries = 0
            begin
              yield
            rescue Aws::S3::Errors::NoSuchKey => e
              # We don't want to retry here, just bump the metrics and re-raise so we can handle in the caller
              # If the file doesn't exist, there's no use in retrying
              GitHub.dogstats.increment("#{metric_prefix}.s3_error", tags: ["operation:#{operation}"])
              Failbot.report(e, processor: metric_prefix)
              raise
            rescue Seahorse::Client::NetworkingError, Aws::S3::Errors::ServiceError
              GitHub.dogstats.increment("#{metric_prefix}.s3_error", tags: ["operation:#{operation}"])

              # Prevent heartbeat expiration if we're throttled for a long period of time
              safe_trigger_heartbeat

              tries += 1
              sleep_time = [(0.25 * tries), MAX_SLEEP_ON_RETRY].min # Exponential backoff [0.25, 0.5, 0.75..] with max of MAX_SLEEP_ON_RETRY
              sleep(sleep_time)
              # Retry forever
              retry # rubocop:disable GitHub/UnboundedRetries
            rescue SHA1MismatchError
              GitHub.dogstats.increment("#{metric_prefix}.s3_sha1_mismatch")

              # Prevent heartbeat expiration if we're throttled for a long period of time
              safe_trigger_heartbeat

              tries += 1
              sha_mismatch_tries += 1
              sleep_time = [(0.25 * sha_mismatch_tries), (0.25 * tries), MAX_SLEEP_ON_RETRY].min # Exponential backoff [0.25, 0.5, 0.75..] with max of MAX_SLEEP_ON_RETRY
              sleep(sleep_time)
              if sha_mismatch_tries <= max_sha_retries
                retry
              else
                raise
              end
            end
          end
        end

        def record_batch_metrics
          GitHub.dogstats.gauge("serialized_metered_line_item_processor.buffer_size", buffer.size, tags: ["partition:#{partition}"])

          batch_age_in_ms = (Time.now.to_f - usage_batch.created_at.to_f) * 1000
          GitHub.dogstats.gauge("serialized_metered_line_item_processor.batch_age_in_ms", batch_age_in_ms, tags: ["partition:#{partition}"])

          GitHub.dogstats.gauge("serialized_metered_line_item_processor.batches_processed", batches_processed, tags: ["partition:#{partition}"])
        end
      end
    end
  end
end
