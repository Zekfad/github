# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module SponsorsActivity
      class SponsorshipTierChangeProcessor < Hydro::Processor
        DEFAULT_GROUP_ID = "sponsors_activity_tier_change"
        DEFAULT_SUBSCRIBE_TO = /sponsors.v1.SponsorshipTierChange\Z/
        STATS_NAMESPACE = "sponsors_activity_processor.sponsorship_tier_change"

        ProcessingError = Class.new(StandardError)

        options[:min_bytes] = 1.byte
        options[:max_wait_time] = 1.second
        options[:max_bytes_per_partition] = 1.megabyte

        def initialize(group_id: nil, subscribe_to: nil)
          options[:group_id] = group_id || DEFAULT_GROUP_ID
          options[:subscribe_to] = subscribe_to || DEFAULT_SUBSCRIBE_TO

          Failbot.push(
            stream_processor: self.class.name.underscore,
            group_id: options[:group_id],
            subscribe_to: options[:subscribe_to].inspect,
          )
        end

        def process_with_consumer(batch, consumer)
          GitHub.dogstats.batch do
            GitHub.dogstats.increment("#{STATS_NAMESPACE}.received_batch")

            batch.each do |message|
              GitHub.dogstats.increment("#{STATS_NAMESPACE}.received_message")

              begin
                ::SponsorsActivity.create!(activity_attrs(message))
                consumer.mark_message_as_processed(message)
              rescue ActiveRecord::RecordInvalid => e
                GitHub.dogstats.increment("#{STATS_NAMESPACE}.invalid_message")
                Failbot.report(e, sponsors_activity: activity_attrs(message))
              rescue Freno::Throttler::Error
                GitHub.dogstats.increment("#{STATS_NAMESPACE}.message_throttled")
                raise
              rescue => e
                GitHub.dogstats.increment("#{STATS_NAMESPACE}.failed_message")
                error = ProcessingError.new(e.message)
                error.set_backtrace(e.backtrace)
                Failbot.report(error, cause: e, sponsors_activity: activity_attrs(message))
                consumer.mark_message_as_processed(message)
              end
            end
          end
        end

        def activity_attrs(message)
          sponsorable_id = message.value.dig(:sponsorship, :maintainer, :id)
          sponsor_id = message.value.dig(:sponsorship, :sponsor, :id)
          current_tier_id = message.value.dig(:current_tier, :id)
          previous_tier_id = message.value.dig(:previous_tier, :id)

          {
            timestamp: Time.at(message.timestamp),
            sponsorable_id: sponsorable_id,
            sponsorable_type: ::User.name,
            sponsor_id: sponsor_id,
            sponsor_type: ::User.name,
            sponsors_tier_id: current_tier_id,
            old_sponsors_tier_id: previous_tier_id,
            action: :tier_change,
          }
        end
      end
    end
  end
end
