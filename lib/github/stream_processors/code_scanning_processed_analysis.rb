# frozen_string_literal: true

module GitHub
  module StreamProcessors
    # Once TurboScan has completed the ingestion of an Analysis, it
    # signals on the ProcessedAnalysis topic.
    # We use this to synchronize the check annotations.
    class CodeScanningProcessedAnalysis < Hydro::Processor
      DEFAULT_GROUP_ID = "code_scanning_processed_analysis"
      DEFAULT_SUBSCRIBE_TO = /turboscan.v0.ProcessedAnalysis\Z/

      options[:min_bytes] = 1.megabytes
      options[:max_wait_time] = 0.2.seconds
      # max_bytes_per_partition defines the batch size. We need to
      # pick a value that we can process efficiently.  Our Analysis
      # message should be pretty small (<1KB) so we are saying that we
      # can process 2000 messages in ~30 seconds.
      # I think this is reasonable but should monitor
      options[:max_bytes_per_partition] = 2.megabytes
      options[:start_from_beginning] = false

      def initialize(group_id: nil, subscribe_to: nil)
        options[:group_id] = group_id || DEFAULT_GROUP_ID
        options[:subscribe_to] = subscribe_to || DEFAULT_SUBSCRIBE_TO
      end

      def process_with_consumer(batch, consumer)
        batch.each do |message|
          process_message(message)
        end
      end

      def process_message(message)
        # message is an Analysis
        repo_id = message.value.dig(:repository_id)
        commit_oid = message.value.dig(:commit_oid)
        GitHub::Logger.log({fn: "CodeScanningProcessedAnalysis::process_message",
                            repo_id: repo_id,
                            commit_oid: commit_oid})

        repo = Repository.find_by(id: repo_id)
        check_run_ids = CheckRun.for_code_scanning_analysis(repository: repo, commit_oid: commit_oid).pluck(:id)
        if check_run_ids.present?
          repo.refresh_code_scanning_status(check_run_ids: check_run_ids)
        else
            # Log as error. At this point we should have a CheckRun.
            GitHub::Logger.log({fn: "CodeScanningProcessedAnalysis::process_message",
                repo: repo.id,
                commit: commit_oid,
                msg: "CheckRun not found"})
            Failbot.report(CheckRunError.new("CheckRun not found for repo #{repo.id} and commit #{commit_oid}"), app: "github-turboscan-client")
        end
      rescue StandardError => e
        # We want to continue processing messages even if we fail
        # here. Therefore we log the error and continue. A more
        # detailed set of exceptions here might be desirable, and we
        # should refine this once we get a sense of typical ways in
        # which we fail.
        GitHub::Logger.log({fn: "CodeScanningProcessedAnalysis::process_message",
          repo: repo.id,
          commit: commit_oid,
          exception: e,
          msg: "Failure processing message"})
        Failbot.report(StandardError.new("Failure processing message"), app: "github-turboscan-client")
      end

      class CheckRunError < StandardError
      end

    end
  end
end
