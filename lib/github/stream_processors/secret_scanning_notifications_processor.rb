# frozen_string_literal: true

module GitHub
  module StreamProcessors
    class SecretScanningNotificationsProcessor < BaseProcessor
      DEFAULT_GROUP_ID = "secret_scanning_notifications_processor"
      DEFAULT_SUBSCRIBE_TO = /github\.v1\.TokenScanNotify\Z/

      areas_of_responsibility :token_scanning

      options[:min_bytes] = 1
      options[:max_wait_time] = 0.2.seconds
      options[:max_bytes_per_partition] = 1.megabytes
      options[:session_timeout] = 60.seconds
      options[:start_from_beginning] = false

      def setup(**kwargs)
        options[:group_id] ||= DEFAULT_GROUP_ID
        options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

        self.metric_prefix = group_id
      end

      private

      def process_message(message)
        repo = Repository.find_by_id(message.value[:repository_id])
        return unless repo.present?
        return unless repo.token_scanning_enabled?
        return unless repo.token_scanning_service_enabled?

        users = repo.token_scanning_users_to_notify
        return if users.empty?

        results = message.value[:token_groups]
        scope = message.value[:scope]

        AccountMailer.token_scanning_summary(repo, results, users, scope).deliver_later

        GitHub.dogstats.increment("secret_scanning.notify_hydro_event.processed", tags: ["scope:#{scope}"])
        GitHub::Logger.log(
          fn: "SecretScanningNotificationsProcessor::process_message",
          msg: "TokenScanNotify message processed",
          repo: message.value[:repository_id],
        )
      end
    end
  end
end
