# frozen_string_literal: true

module GitHub
  module StreamProcessors
    # Once TurboScan has completed the ingestion of an Analysis
    # and computed the diff, it signals on the AlertEvent topic
    # all the alert changes.
    class CodeScanningAlertEvent < Hydro::Processor
      DEFAULT_GROUP_ID = "code_scanning_alert_event"
      DEFAULT_SUBSCRIBE_TO = /turboscan.v0.AlertEvent\Z/

      options[:min_bytes] = 1.megabytes
      options[:max_wait_time] = 0.2.seconds
      # max_bytes_per_partition defines the batch size. We need to
      # pick a value that we can process efficiently.  Our Analysis
      # message should be pretty small (<1KB) so we are saying that we
      # can process 2000 messages in ~30 seconds.
      # I think this is reasonable but should monitor
      options[:max_bytes_per_partition] = 2.megabytes
      options[:start_from_beginning] = false

      def initialize(group_id: nil, subscribe_to: nil)
        options[:group_id] = group_id || DEFAULT_GROUP_ID
        options[:subscribe_to] = subscribe_to || DEFAULT_SUBSCRIBE_TO
      end

      def process_with_consumer(batch, consumer)
        batch.each do |message|
          process_message(message)
        end
      end

      # Message will be a AlertEvent.proto
      # see https://github.com/github/hydro-schemas/blob/bd32975dd26848a565ab345a620295ec7218f6ff/proto/hydro/schemas/turboscan/v0/alert_event.proto#L9
      #
      def process_message(message)
        args = args_for_webhook(message.value)

        if args[:action].present?
          event = Hook::Event::CodeScanningAlertEvent.new(args)
          event.attributes[:queued_at] = Time.now.to_f
          event.deliver_later if event.feature_flag_enabled?
        end

        GitHub.dogstats.increment("code_scanning.alert_event.processed", tags: ["action:#{args[:action]}", "event:#{args[:event]}"])
        GitHub::Logger.log(
          args.merge(
            fn: "CodeScanningAlertEvent::process_message",
            msg: "AlertEvent message processed"
          )
        )

      rescue StandardError => e
        args ||= {empty: true}
        # We want to continue processing messages even if we fail
        # here. Therefore we log the error and continue. A more
        # detailed set of exceptions here might be desirable, and we
        # should refine this once we get a sense of typical ways in
        # which we fail.
        GitHub::Logger.log(
          args.merge(
            fn: "CodeScanningAlertEvent::process_message",
            exception: e,
            msg: "Failure processing message")
        )
        Failbot.report(e, app: "github-turboscan-client")
      end

      def args_for_webhook(proto_values)
        action =
          case proto_values[:event]
          when :ALERT_CREATED
            :created
          when :ALERT_REOPENED_BY_USER
            :reopened_by_user
          when :ALERT_CLOSED_BY_USER
            :closed_by_user
          when :ALERT_CLOSED_BECAME_FIXED
            :fixed
          when :ALERT_APPEARED_IN_BRANCH
            :appeared_in_branch
          when :ALERT_REAPPEARED
            :reopened
          else
            nil
          end
        actor_id = proto_values[:actor_id]
        actor_id = nil if actor_id.zero?
        resolution = proto_values[:resolution].downcase
        unless %i(no_resolution false_positive wont_fix used_in_tests).include?(resolution)
          # If not a known resolution type, default to no_resolution
          resolution = :no_resolution
        end

        args = proto_values.slice(:event, :repository_id, :alert_number, :commit_oid, :ref).
                 merge(action: action, actor_id: actor_id, resolution: resolution)
      end
    end
  end
end
