# frozen_string_literal: true

module GitHub
  module StreamProcessors
    # A wrapper around Hydro::Source::Message that allows for state tracking
    class MessageWithResult < SimpleDelegator
      # Public: The reason the message is in a skipped or error status
      # Returns String or Exception
      attr_reader :cause

      # Public: Mark a message as successful
      #
      # Returns nothing
      def success
        @result = :success
      end

      # Public: Whether or not the message was successful
      #
      # Returns Boolean
      def success?
        !error? && !skipped?
      end

      # Public: Marks a message as failed due to an error
      #
      # cause - The Exception that caused the message to fail
      #
      # Returns nothing
      def error(cause = nil)
        @result = :error
        @cause = cause
      end

      # Public: Whether or not the message failed due to an error
      #
      # Returns Boolean
      def error?
        @result == :error
      end

      # Public: Marks a message as skipped
      #
      # cause - The String reason for the message being skipped
      #
      # Returns nothing
      def skip(cause = nil)
        @result = :skipped
        @cause = cause
      end

      # Public: Whether or not the message was skipped
      #
      # Returns Boolean
      def skipped?
        @result == :skipped
      end
    end
  end
end
