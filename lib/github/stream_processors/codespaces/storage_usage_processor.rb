# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module Codespaces
      class StorageUsageProcessor < BaseProcessor
        DEFAULT_GROUP_ID = "codespaces_storage_usage_processor"
        DEFAULT_SUBSCRIBE_TO = /github\.codespaces\.v0\.StorageUsage\Z/

        options[:min_bytes] = 1
        options[:max_wait_time] = 0.2.seconds
        options[:session_timeout] = 60.seconds
        options[:start_from_beginning] = false
        options[:max_bytes_per_partition] = 100.kilobytes

        def setup(**kwargs)
          options[:group_id] ||= DEFAULT_GROUP_ID
          options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

          self.dead_letter_topic = "github.codespaces.v0.StorageUsage.DeadLetter"
        end

        def process_message(message)
          owner = User.find_by(id: message.value[:owner_id])
          if owner.nil?
            return message.skip("missing_owner")
          end

          unless GitHub.flipper[:process_codespaces_storage_usage].enabled?(owner)
            return message.skip("feature_flag_disabled")
          end

          ::Billing::Codespaces::StorageUsageLineItem.throttle_with_retry(max_retry_count: 5) do
            safe_trigger_heartbeat
            ::Billing::Codespaces::StorageUsageLineItemCreator.create(
              owner: owner,
              actor_id: message.value.dig(:actor_id),
              repository_id: message.value.dig(:repository, :id),
              unique_billing_identifier: message.value.dig(:unique_billing_identifier),
              start_time: Time.at(message.value.dig(:start_time, :seconds)),
              end_time: Time.at(message.value.dig(:end_time, :seconds)),
              duration_in_seconds: message.value.dig(:billable_duration_in_seconds) || 0,
              size_in_bytes: message.value.dig(:size_in_bytes) || 0,
              computed_usage: message.value.dig(:computed_usage),
              **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(owner),
            )
          end
        end

        private

        def error_context_for_message(message)
          {
            owner_id: message.value.dig(:owner_id),
            actor_id: message.value.dig(:actor_id),
            repo_id: message.value.dig(:repository, :id),
            unique_billing_identifier: message.value.dig(:unique_billing_identifier),
          }
        end
      end
    end
  end
end
