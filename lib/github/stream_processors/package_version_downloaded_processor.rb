# frozen_string_literal: true

module GitHub
  module StreamProcessors
    class PackageVersionDownloadedProcessor < BaseProcessor
      DEFAULT_GROUP_ID = "package_version_downloaded_processor"
      DEFAULT_SUBSCRIBE_TO = /package_registry\.v0\.PackageVersionDownloaded\Z/

      options[:min_bytes] = 1.bytes
      options[:max_wait_time] = 0.2.seconds
      options[:max_bytes_per_partition] = 100.kilobytes
      options[:session_timeout] = 60.seconds
      options[:start_from_beginning] = false

      def setup(**kwargs)
        options[:group_id] ||= DEFAULT_GROUP_ID
        options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

        self.metric_prefix = group_id
        self.dead_letter_topic = "package_registry.v0.PackageVersionDownloaded.DeadLetter"
      end

      def process_message(message)
        unless GitHub.flipper[:gpr_data_transfer].enabled?
          return message.skip("feature_flag_disabled")
        end

        track_package_download_activity(message, package_id(message), version_id(message), downloaded_at(message))

        create_data_transfer_line_item(message)
      end

      def create_data_transfer_line_item(message)
        if GitHub.flipper[:registry_dont_bill_docker].enabled?
          registry_type = message.value.dig(:package, :registry_type)
          if registry_type == :DOCKER
            return message.skip("docker")
          end
        end

        owner = User.find_by(id: owner_id(message))
        if owner.nil?
          return message.skip("missing_owner")
        end

        if public_repository?(message)
          return message.skip("public_repository")
        end

        if download_originated_from_actions?(message)
          return message.skip("downloaded_from_actions")
        end

        billable_owner_designator = ::Billing::MeteredBillingBillableOwnerDesignator.new(owner)
        billable_owner = billable_owner_designator.billable_owner
        if billable_owner.skip_metered_billing_permission_check_for?(product: :packages)
          return message.skip("metered_permissions_skipped")
        end

        ::Billing::PackageRegistry::DataTransferLineItem.throttle_with_retry(max_retry_count: 5) do
          safe_trigger_heartbeat
          ::Billing::PackageRegistry::DataTransferLineItemCreator.create(
            owner: owner,
            actor_id: message.value.dig(:actor, :id),
            registry_package_id: package_id(message),
            registry_package_version_id: version_id(message),
            size_in_bytes: message.value.dig(:file, :size),
            download_id: message.value[:event_id],
            downloaded_at: downloaded_at(message),
            **billable_owner_designator.to_h,
          )
        end

        notify_later_if_applicable(owner)
      rescue ActiveRecord::RecordNotUnique
      end

      def track_package_download_activity(message, package_id, version_id, downloaded_at)
        return unless package_id && version_id && downloaded_at
        Registry::PackageDownloadActivity.throttle_with_retry(max_retry_count: 5) do
          safe_trigger_heartbeat
          Registry::PackageDownloadActivity.track(
            package_id,
            version_id,
            downloaded_at.change(min: 0, sec: 0),
          )
        end
      rescue ActiveRecord::ActiveRecordError => err
        Failbot.report(err)
        GitHub.dogstats.increment("package_version_downloaded_processor.track_download_error")
      end

      def notify_later_if_applicable(owner)
        MeteredBillingThresholdNotifierJob.perform_later(
          owner: owner,
          product: ::Billing::UsageNotifications::GPR_PRODUCT,
        )
      end

      def owner_id(message)
        message.value.dig(:package, :owner_id)
      end

      def package_id(message)
        message.value.dig(:package, :id)
      end

      def version_id(message)
        message.value.dig(:version, :id)
      end

      def downloaded_at(message)
        message_seconds = message.value.dig(:downloaded_at, :seconds)
        if message_seconds.present?
          Time.at(message_seconds)
        else
          Time.now.utc
        end
      end

      def public_repository?(message)
        repository_visibility(message) == "public"
      end

      def download_originated_from_actions?(message)
        downloaded_from(message) == "actions"
      end

      def repository_visibility(message)
        case message.value.dig(:package, :repository, :visibility)
        when :VISIBILITY_UNKNOWN
          "unknown"
        when :PUBLIC
          "public"
        when :PRIVATE
          "private"
        when :INTERNAL
          "private"
        else
          "unknown"
        end
      end

      def downloaded_from(message)
        if message.value[:via_actions] == "false" || message.value[:via_actions] == false
          "unknown"
        else
          "actions"
        end
      end

      private

      def error_context_for_message(message)
        {
          user_id: owner_id(message),
          package_id: message.value.dig(:package, :id),
        }
      end
    end
  end
end
