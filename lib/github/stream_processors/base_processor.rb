# frozen_string_literal: true

module GitHub
  module StreamProcessors

    class ProcessorPausedError < StandardError; end

    # BaseProcessor provides a common, feature-rich implementation for Hydro
    # stream processors in the same way that ApplicationJob provides a base
    # for background jobs. BaseProcessor provides the following features:
    #
    #   - Areas of responsibility and service mapping
    #   - Exception handling
    #   - Dogstatsd metrics
    #   - Dead-letter routing
    #   - Pause/Resume message processing
    #
    # Sub-classes MUST implement on method: `#process_message`.
    #
    # Sub-classes MAY implement the following methods as well:
    #
    #   - `#setup`
    #   - `#before_batch`
    #   - `#after_batch`
    #   - `#error_context`
    #   - `#error_context_for_message`
    #   - `#publish_dead_letter_message`
    #
    # In addition, sub-classes MAY implement the following `Hydro::Processor`
    # methods:
    #
    #   - `#before_open`
    #   - `#after_close`
    #
    class BaseProcessor < Hydro::Processor
      extend ActiveSupport::DescendantsTracker
      include ActiveSupport::Rescuable
      include GitHub::AreasOfResponsibility
      include GitHub::ServiceMapping
      include GitHub::StreamProcessors::Instrumentation
      include GitHub::StreamProcessors::ProcessorPausing

      rescue_from StandardError, with: :report_error

      # Public: Initialize the stream processor
      #
      # The default initializer will set the Hydro options group_id and
      # subscribe_to if they are provided. It will also call the `#setup`
      # method to allow sub-classes to do additional configuration.
      #
      # Sub-classes can change the group_id and subscribe_to Hydro
      # options in `#setup` if needed. This is especially useful when
      # default values are provided by the sub-class.
      #
      # This method accepts arbitrary keyword arguments and forwards them to
      # the `#setup` method along with group_id and subscribe_to.
      #
      # group_id     - The Hydro consumer group that the processor belongs to
      # subscribe_to - A regular expression describing which Hydro topics the
      #                processor consumers
      def initialize(group_id: nil, subscribe_to: nil, **kwargs)
        options[:group_id] = group_id
        options[:subscribe_to] = subscribe_to

        @stats = GitHub.dogstats

        setup(group_id: group_id, subscribe_to: subscribe_to, **kwargs)
      end

      # Public: Processes a batch of messages from Hydro
      #
      # IMPORTANT: This method SHOULD NOT be overridden by sub-classes
      #
      # This method iterates over a batch of messages from Hydro and calls
      # `#process_message` for each message. `#process_message` can provide
      # feedback -- mostly for instrumentation in Dogstatsd -- about the
      # result of processing a given message.
      #
      # Examples:
      #
      #   def process_message(message)
      #     # If a message was skipped for some reason:
      #     return message.skip("reason_for_skip")
      #
      #     # If a message caused an error
      #     return message.error(RuntimeError.new)
      #
      #     # If a message was successful (messages are assumed successful by
      #     # default so this isn't necessary; but it can be done if desired)
      #     return message.success
      #   end
      #
      # If `#process_message` raises an error, the message will automatically be
      # marked with the error that was rescued.
      #
      # batch    - An enumeration of Hydro::Source::Messages
      # consumer - The Hydro::Consumer which is consuming messages
      #
      # Returns nothing
      def process_with_consumer(batch, consumer)
        raise ::GitHub::StreamProcessors::ProcessorPausedError if paused?

        reset_error_context

        stats.batch do
          time("process_with_consumer") do
            @consumer = consumer
            safe_trigger_heartbeat

            now = Time.now

            instrument_batch_size(batch.size)

            time("before_batch") do
              before_batch(batch)
            end

            time("process_batch") do
              batch.each do |original_message|
                safe_trigger_heartbeat

                message = MessageWithResult.new(original_message)

                latency_ms = (now.to_f - message.timestamp.to_f) * 1_000
                instrument_message_received(latency_ms)

                @current_error_context.merge!(error_context_for_message(message))

                time("process_message") do
                  process_message_with_error_handling(message)
                end

                if message.success?
                  instrument_message_successful
                elsif message.skipped?
                  instrument_message_skipped(message.cause)
                elsif message.error?
                  instrument_message_failed
                  publish_dead_letter_message(message, message.cause)
                end

                consumer.mark_message_as_processed(message)
              end
            end

            reset_error_context

            time("after_batch") do
              after_batch(batch)
            end
          end
        end
      rescue ::GitHub::StreamProcessors::ProcessorPausedError => e
        raise e
      rescue => e
        rescue_with_handler(e) || raise
      end

      protected

      # Protected: The Hydro topic to use, if any, for publishing dead letter messages
      attr_accessor :dead_letter_topic

      # Protected: The custom metric prefix to use, if any, for Dogstatsd metrics
      attr_accessor :metric_prefix

      # Protected: The context for errors sent to Failbot, which includes message-specific context
      attr_reader :current_error_context

      # Protected: The Dogstatsd instance to use for recording stats
      attr_reader :stats

      # Protected: Run processor-specific initialization
      #
      # This method is called by `#initialize`. All of the keyword arguments
      # provided to `#initialize` are forwarded to this method, including
      # group_id and subscribe_to.
      #
      # Sub-classes are not required to implement this method.
      #
      # Returns nothing
      def setup(**kwargs)
        # Implementation optional
      end

      # Protected: Hook to be called before processing a batch of messages
      #
      # This method is called inside of the Hydro consumer loop before a batch
      # of messages is iterated through. It can be used for setup tasks that are
      # necessary for processing.
      #
      # Sub-classes are not required to implement this method.
      #
      # batch - An enumeration of Hydro::Source::Messages
      #
      # Returns nothing
      def before_batch(_batch)
        # Implementation optional
      end

      # Protected: Process an individual message from Hydro
      #
      # This method MUST be implemented by sub-classes
      #
      # Sub-classes are required to implement this method.
      #
      # message - An individual Hydro::Source::Message
      #
      # Returns nothing
      def process_message(_message)
        raise NotImplementedError
      end

      # Protected: Hook to be called after processing a batch of messages
      #
      # This method is called inside of the Hydro consumer loop after a batch
      # of messages is iterated through. It can be used for teardown tasks that
      # are necessary for processing.
      #
      # Sub-classes are not required to implement this method.
      #
      # batch - An enumeration of Hydro::Source::Messages
      #
      # Returns nothing
      def after_batch(_batch)
        # Implementation optional
      end

      # Protected: A Hash of non-message-specific context to be included when
      # exceptions are sent to Failbot
      #
      # For message-specific context, use `#error_context_for_message`.
      #
      # Sub-classes are not required to implement this method.
      #
      # Returns Hash
      def error_context
        {}
      end

      # Protected: A Hash of message-specific context to be included when
      # exceptions are sent to Failbot
      #
      # For context that isn't message specific, use `#error_context`.
      #
      # Sub-classes are not required to implement this method.
      #
      # message - The specific Hydro::Source::Message being processed
      #
      # Returns Hash
      def error_context_for_message(_message)
        {}
      end

      # Protected: Publish a dead letter message when an error is encountered if
      # a dead letter topic is configured
      #
      # This will publish a `github.v1.DeadLetter` message to the configured
      # topic. Sub-classes may override this method if needed to publish a
      # different message schema.
      #
      # message - The Hydro::Source::Message that could not be processed
      # error   - The error that was raised
      #
      # Returns nothing
      def publish_dead_letter_message(message, error)
        return unless dead_letter_topic

        payload = {
          envelope: envelope_generator.generate(message),
          payload: JSON.dump(message.value),
          error_class: error.class.name,
          error_message: error.message,
        }

        Hydro::PublishRetrier.publish(
          payload,
          schema: "github.v1.DeadLetter",
          topic: dead_letter_topic,
        )
      end

      # Protected: The EnvelopeGenerator used for generating Hydro envelopes
      #
      # These envelopes are included in the `github.v1.DeadLetter` message
      # generated by the default implementation of `#publish_dead_letter_message`.
      #
      # Returns GitHub::StreamProcessors::EnvelopeGenerator
      def envelope_generator
        @envelope_generator ||= EnvelopeGenerator.new
      end

      # Protected: Triggers a Kafka consumer heartbeat, which lets Kafka know
      # that the consumer is still alive and processing.
      #
      # Triggering a heartbeat _can_ raise exceptions, for example if Kafka is
      # rebalancing the consumer group. This method swallows such exceptions as
      # they do not need to derail processing of messages.
      #
      # This method is automatically invoked for every message. Sub-classes may
      # wish to invoke this method if doing some processing that could cause a
      # heartbeat timeout.
      #
      # Returns nothing
      def safe_trigger_heartbeat
        if defined?(@last_heartbeat_at)
          time_since_heartbeat_ms = (Time.now.to_f - @last_heartbeat_at.to_f) * 1_000
          instrument_time_since_heartbeat(time_since_heartbeat_ms)
        end
        @last_heartbeat_at = Time.now

        consumer&.trigger_heartbeat
      rescue Hydro::Source::Error, Kafka::RebalanceInProgress, Kafka::HeartbeatError
        # Pass
      end

      private

      # Internal: The Hydro::Consumer which is consuming messages
      attr_reader :consumer

      # Internal: Report an error to Failbot
      #
      # error - The error that was raised
      #
      # Returns nothing
      def report_error(error)
        context = current_error_context.merge(
          "processor" => self.class.name,
          "areas_of_responsibility" => areas_of_responsibility,
          "catalog_service" => logical_service,
        )

        Failbot.report(error, context)
      end

      # Internal: Process a single message with error handling wrappers
      #
      # message - A Hydro::Source::Message to process
      #
      # Returns nothing
      # Raises if no error handler is defined for the error that was rescued
      def process_message_with_error_handling(message)
        process_message(message)
      rescue ::GitHub::StreamProcessors::ProcessorPausedError => e
        raise e
      rescue => e
        message.error(e)
        rescue_with_handler(e) || raise
      end

      # Internal: Resets the error context between messages
      #
      # Returns nothing
      def reset_error_context
        @current_error_context = error_context.deep_dup
      end
    end
  end
end
