# frozen_string_literal: true

module GitHub
  module StreamProcessors
    class ActionsUsageRelay < BaseProcessor
      DEFAULT_GROUP_ID = "actions_usage_relay"
      DEFAULT_SUBSCRIBE_TO = /github\.actions\.v0\.JobExecution\Z/

      options[:min_bytes] = 1
      options[:max_wait_time] = 0.2.seconds
      options[:max_bytes_per_partition] = 100.kilobytes
      options[:session_timeout] = 60.seconds
      options[:start_from_beginning] = false

      def setup(**kwargs)
        options[:group_id] ||= DEFAULT_GROUP_ID
        options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

        self.metric_prefix = group_id
        self.dead_letter_topic = "github.actions.v0.JobExecution.DeadLetter"
      end

      # Create a Billing::ActionsUsageLineItem for the message
      #
      # message - The Hydro::Consumer::ConsumerMessage to process
      #
      # Returns nothing
      def process_message(message)
        owner = User.find_by(id: message.value[:workflow_repository_owner_id])

        if owner.nil?
          return message.skip("missing_owner")
        end

        unless GitHub.flipper[:relay_actions_usage].enabled?(owner)
          return message.skip("feature_flag_disabled")
        end

        if public_repository?(message)
          return message.skip("public_repository")
        end

        if self_hosted?(message)
          return message.skip("self_hosted")
        end

        ::Billing::ActionsUsageLineItem.throttle_with_retry(max_retry_count: 5) do
          safe_trigger_heartbeat
          ::Billing::Actions::UsageLineItemCreator.create(
            job_id: message.value[:job_id],
            check_run_id: message.value[:check_run_id],
            actor_id: message.value[:invoking_user_id],
            repository_id: message.value[:workflow_repository_id],
            duration_in_milliseconds: message.value[:job_execution_billable_ms],
            start_time: Time.at(message.value[:start_time][:seconds]),
            end_time: Time.at(message.value[:end_time][:seconds]),
            owner_id: message.value[:workflow_repository_owner_id],
            job_runtime_environment: message.value[:job_runtime],
            **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(owner),
          )
        end

        notify_later_if_applicable(owner)
      end

      private

      def notify_later_if_applicable(owner)
        MeteredBillingThresholdNotifierJob.perform_later(
          owner: owner,
          product: ::Billing::UsageNotifications::ACTIONS_PRODUCT,
        )
      end

      def public_repository?(message)
        message.value[:workflow_repository_visibility].to_s.upcase == "PUBLIC"
      end

      def self_hosted?(message)
        !!message.value[:self_hosted]
      end

      def error_context_for_message(message)
        {
          billing: {
            owner_id: message.value[:workflow_repository_owner_id],
            repository_id: message.value[:workflow_repository_id],
            job_id: message.value[:job_id],
          },
        }
      end
    end
  end
end
