# frozen_string_literal: true

module GitHub
  module StreamProcessors
    class ResqueRetriesRelay < Hydro::Processor
      DEFAULT_GROUP_ID = "github_resque_retries"
      DEFAULT_SUBSCRIBE_TO = /github.v1.ResqueJobRetries\Z/

      options[:min_bytes] = 5.megabytes
      options[:max_wait_time] = 0.2.seconds
      options[:max_bytes_per_partition] = 25.megabytes

      def initialize(group_id: nil, subscribe_to: nil, filter: nil)
        options[:group_id] = group_id || DEFAULT_GROUP_ID
        options[:subscribe_to] = subscribe_to || DEFAULT_SUBSCRIBE_TO
        @filter = filter
        @first_offsets = {}
        @last_offsets = {}
      end

      def process_with_consumer(batch, consumer)
        GitHub.dogstats.batch do
          GitHub.dogstats.count("resque_retries_relay.batch_size", batch.size)

          GitHub.dogstats.time("resque_retries_relay.process_batch") do
            batch.each do |message|
              record_first_offset(message)

              puts message.value if Rails.env.development?

              next if filter && !filter.call(message)

              begin
                job_class = message.value[:class_name].constantize
              rescue NameError => e
                Failbot.report(e)
                next
              end

              begin
                TimedResque::BufferedTimedResque.retry_with_timed_resque(
                  job_class,
                  Resque.decode(message.value[:args].presence),
                  Time.at(message.value.dig(:retry_at, :seconds)),
                  message.value[:retries],
                  message.value.dig(:guid, :value),
                  message.value[:queue],
                )
              rescue *ResqueJobRelay::PAUSE_PROCESSING_EXCEPTIONS => e
                consumer.commit_offsets
                raise e
              end

              record_last_offset(message)
              consumer.mark_message_as_processed(message)
              instrument_relay_stats(message)
            end
          end
        end
      end

      def after_close
        @last_offsets.each do |partition, offset|
          unless Rails.env.test?
            Rails.logger.warn "Finished processing partition #{partition} at offset #{offset}"
          end
        end
      end

      private

      def record_first_offset(message)
        unless @first_offsets[message.partition]
          @first_offsets[message.partition] = message.offset

          unless Rails.env.test?
            Rails.logger.warn "Began processing partition #{message.partition} at offset #{message.offset}"
          end
        end
      end

      def record_last_offset(message)
        @last_offsets ||= {}
        @last_offsets[message.partition] = message.offset
      end

      attr_reader :filter

      def instrument_relay_stats(message)
        tags = [
          "class_name:#{message.value[:class_name]}",
          "queue:#{message.value[:queue]}",
        ]
        latency_ms = (Time.now.to_f - message.timestamp.to_f) * 1_000
        GitHub.dogstats.increment("resque_retries_relay.count", tags: tags)
        GitHub.dogstats.timing("resque_retries_relay.latency", latency_ms.to_i, tags: tags)
      end
    end
  end
end
