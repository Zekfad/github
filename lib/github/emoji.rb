# frozen_string_literal: true

require "emoji"

module GitHub
  module Emoji
    extend self

    # Yields each Emoji::Character instance
    def each(&block)
      ::Emoji.all.each(&block)
    end

    # Enumerates through all emoji aliases and yields:
    # 1. the alias as String; and
    # 2. its corresponding Emoji::Character instance.
    def each_by_alias
      return to_enum(__method__) unless block_given?
      index = index_by_alias
      index.keys.sort.each do |name|
        yield(name, index.fetch(name))
      end
    end

    def index_by_alias
      ::Emoji.all.each_with_object({}) do |emoji, map|
        emoji.aliases.each do |name|
          map[name] = emoji
        end
      end
    end

    def asset_path(name)
      File.join("emoji", ::Emoji.find_by_alias(name).image_filename)
    end
  end
end
