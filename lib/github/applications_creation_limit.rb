# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module ApplicationsCreationLimit
    include ActiveSupport::Concern

    # The default max number of applications (per type of app) an application owner can create
    DEFAULT_MAX_APPLICATIONS_CREATION_LIMIT = 100

    # Public: Sets an applications creation limit scoped to the application owner and the
    # type of application
    #
    # application           - An instance of OauthApplication or Integration
    # limit                 - Integer, the custom creation limit for the application owner
    #
    # Returns nothing
    def set_custom_applications_limit(application, limit)
      limit = Integer(limit)
      raise ArgumentError.new("limit can't be negative") if limit < 0

      key = applications_limit_key_for(application)
      GitHub.kv.set(key, limit.to_s)
    end

    # Public: Determines the application owner's applications creation limit for the given type
    # of application
    #
    # Returns an integer
    def applications_creation_limit(application)
      key = applications_limit_key_for(application)
      (GitHub.kv.get(key).value! || DEFAULT_MAX_APPLICATIONS_CREATION_LIMIT).to_i
    end

    # Public: Determines if this user has reached the creation limit for the given
    # type of application
    #
    # Returns an boolean
    #
    # TODO: refactor all methods to follow this signature
    # https://github.com/github/github/pull/111065#discussion_r270522525
    def reached_applications_creation_limit?(application_type:)
      return false if GitHub.enterprise?
      application_type.created_by_count(self) >= self.applications_creation_limit(application_type.new)
    end

    private

    def applications_limit_key_for(application)
      "#{self.class.name.pluralize}.#{application.class.name.underscore}.creation_limit.#{self.id}"
    end
  end
end
