# frozen_string_literal: true

module GitHub
  module Cache
    # Rack middleware to enable and reset the local cache before each request
    # and to release the global reference to the local cache after the response
    # has been returned. Typically set up in config.ru.
    class ResetMiddleware
      attr_accessor :app

      def initialize(app)
        @app = app
      end

      def call(env)
        GitHub.cache.enable_local_cache
        app.call(env)
      ensure
        GitHub.cache.local = nil
        GitHub.cache.skip = nil
      end
    end
  end
end
