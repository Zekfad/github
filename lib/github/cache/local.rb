# frozen_string_literal: true

module GitHub
  module Cache
    # Cache mixin keeps a local hash of read and written values. This can reduce
    # roundtrips to memcached when the same cache keys are read multiple times
    # within a single request or job run.
    #
    # The easiest way to enable the local cache for web requests is with a
    # normal before filter or equivalent:
    #
    #      before_action { GitHub.cache.enable_local_cache }
    #
    # The GitHub.cache.local attribute may also be assigned a Hash directly.
    #
    # Note that the local cache is disabled by default. You must call
    # enable_local_cache or assign a Hash explicitly to enable.
    module Local
      attr_accessor :local

      # This is a sentinel value used for remembering the fact
      # that a key that was loaded via `#get_multi` did not
      # exist on the memcached server.
      #
      # This is important because we sometimes need to distinguish
      # between a key not being set, and a key being explicitly set to `nil`.
      MISSING = Object.new.freeze

      # Call to enable the local cache for all subsequent get and get_multi
      # calls. This method should be called early in a web request or other
      # message handling cycle to reset the local cache.
      def enable_local_cache
        @local = {}
      end

      ##
      # Rails MemCache interface overrides

      # Override get to check the local cache first and avoid network round
      # trip. If not in local cache, read from server and store in local cache.
      def get(key, raw = false)
        return super if @local.nil?

        if @local.key?(key)
          read_local_value(key)
        else
          get_multi([key], raw)[key]
        end
      end

      # Override set to also write to local cache.
      def set(key, value, ttl = 0, raw = false)
        write_local_value(key, value) if @local

        super
      end

      # Override add to also write to local cache.
      def add(key, value, ttl = 0, raw = false)
        res = super

        if res && @local
          write_local_value(key, value)
        end

        res
      end

      # Override delete to also remove the key from local cache.
      def delete(key)
        @local.delete(key) if @local
        super
      end

      # Override get_multi to first read values from local cache. Values not
      # found are read with a single get_multi and set into the local cache.
      def get_multi(keys, raw = false)
        return super if @local.nil?

        # read values from local cache and separate from non-local keys
        hits = {}
        misses = keys.reject do |key|
          if @local.key?(key)
            hits[key] = read_local_value(key) unless @local[key].eql?(MISSING)
            true
          end
        end

        # read missing values from memcached, write to local cache, and merge
        if misses.any?
          cached = super(misses, raw)
          misses.each do |key|
            write_local_value(key, cached.fetch(key) { MISSING })
          end
          hits.merge!(cached)
        end

        hits
      end

      def incr(key, value = 1)
        new_value = super
        write_local_value(key, new_value) if @local
        new_value
      end

      def decr(key, value = 1)
        new_value = super
        write_local_value(key, new_value) if @local
        new_value
      end

      def read_local_value(key)
        value = @local[key]
        return nil if value.eql?(MISSING)
        duplicate_value(value)
      end

      def write_local_value(key, value)
        @local[key] = value
        if value.eql?(MISSING)
          nil
        else
          duplicate_value(value)
        end
      end

      def duplicate_value(value)
        if value.respond_to?(:deep_dup)
          value.deep_dup
        else
          value.dup
        end
      rescue TypeError
        value
      end
    end
  end
end
