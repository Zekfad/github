# frozen_string_literal: true

module GitHub
  class Dogstats < Datadog::Statsd
    def self.monotonic_time
      Process.clock_gettime(Process::CLOCK_MONOTONIC)
    end

    def self.duration(start, ending = nil)
      ending ||= case start
      when Float
        monotonic_time
      else
        Time.now
      end

      ((ending - start) * 1_000).round
    end

    module TimingSince
      # Like GitHub.dogstats.timing, but records the duration since a given start
      # time. Useful when you would normally use GitHub.dogstats.time but want to
      # use a different stats key or options for different situations.
      #
      # @param [String] stat stat name
      # @param [Time or Float] start time or monotonic time
      # @param [Hash] opts the options to create the metric with
      # @option opts [Numeric] :sample_rate sample rate, 1 for always
      # @option opts [Array<String>] :tags An array of tags
      def timing_since(stat, start, opts = {})
        timing(stat, GitHub::Dogstats.duration(start), opts)
      end
    end

    module AddTags
      def tags=(tags)
        # Upstream has removed the tags= method for thread safety, but we need it
        # so that we can reset component, etc when they change.
        raise ArgumentError, "tags must be a Array<String>" unless tags.nil? or tags.is_a? Array
        @tags = (tags || []).compact.map! { |tag| escape_tag_content(tag) }
      end

      private
      def escape_tag_content(tag)
        tag = remove_pipes(tag.to_s)
        tag.delete! ","
        tag
      end

      def remove_pipes(msg)
        msg.delete "|"
      end
    end

    include TimingSince
    include AddTags
  end
end
