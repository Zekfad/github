# frozen_string_literal: true

module GitHub
  class Mailchimp
    class WebhookError < MailchimpError
    end
  end
end
