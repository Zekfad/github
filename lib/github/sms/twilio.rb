# frozen_string_literal: true

module GitHub
  module SMS
    class Twilio < Provider
      # Send an SMS.
      #
      # to      - The number to send to.
      # message - The message to send.
      #
      # Returns a GitHub::SMS::Receipt. Raises GitHub::SMS::Error.
      def send_message(to, message)
        twilio_message = client.account.sms.messages.create(
          from: from,
          to: to,
          body: message,
        )
        build_receipt(twilio_message)
      rescue ::Twilio::REST::RequestError => e
        raise_for_code(e.code)
      rescue ::Twilio::REST::ServerError
        raise_for_code(:server)
      rescue Timeout::Error
        raise_for_code(:timeout)
      end

      # Get SMS logs for a given number.
      #
      # to - The number to get logs for.
      #
      # Returns an Array of log messages.
      def log_for(to)
        client.account.sms.messages.list(to: to).map do |msg|
          {
            body: msg.body,
            date: Time.parse(msg.date_sent),
          }
        end
      end

      private
      # Private: Twilio client.
      #
      # Returns a Twilio::REST::Client instance.
      def client
        @client ||= ::Twilio::REST::Client.new(
          GitHub.twilio_sid,
          GitHub.twilio_token,
          retry_limit: 0,
          timeout: TIMEOUT,
        )
      end

      # Private: Build a Receipt from an API response.
      #
      # message - A Twilio::REST::SMS::Message.
      #
      # Returns a GitHub::SMS::Receipt.
      def build_receipt(message)
        Receipt.new(
          provider: self,
          message_id: message.sid,
        )
      end

      # Private: A Hash mapping provider error codes to GitHub::SMS::Error
      # subclasses.
      #
      # Returns a Hash.
      def error_code_mapping
        super.merge(
          10001 => BillingError,
          21614 => NumberNotMobileError,
          21211 => NumberNotValidError,
          21612 => AreaNotSupportedError,
        )
      end
    end
  end
end
