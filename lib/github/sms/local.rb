# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module SMS
    class Local < Provider
      # Send an SMS.
      #
      # to      - The number to send to.
      # message - The message to send.
      #
      # Returns a GitHub::SMS::Receipt. Raises GitHub::SMS::Error.
      def send_message(to, message)
        text = "Message from #{from} to #{to}: #{message}"

        # If we're on MoLo, pop it in their Notification Center
        if TerminalNotifier.available?
          TerminalNotifier.notify(text, title: "GitHub")
        else
          # Otherwise, just log it for now
          Rails.logger.info(text)
        end

        Receipt.new(
          provider: self,
          message_id: SecureRandom.hex,
        )
      end
    end
  end
end
