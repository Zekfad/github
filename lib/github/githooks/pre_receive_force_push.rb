# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::PreReceiveForcePush
  MESSAGE  = "GH003: Sorry, force-pushing to %s is not allowed."

  def initialize(context)
    @context = context
    @reject  = context.config["git.reject_force_push"] || "false"
  end

  def check
    warnings, errors = [], []
    return [warnings, errors] unless rejection_enabled?

    @context.pushed_refs.collect do |before, after, ref|
      next if [before, after].include? GitHub::NULL_OID  # creates/deletes are not force pushes

      # remove invalid byte sequences before attempting to extract branch name from ref
      ref = ref.dup.scrub! unless ref.valid_encoding?

      branch = ref.sub(%r{^refs/heads/}, "")

      if fast_forward_only?(branch) && forced?(before, after)
        errors << "error: #{MESSAGE}" % branch
      end
    end

    [warnings, errors]
  end

  private

  # Internal: Determine whether this represents a force push
  #
  # before - String OID of ref target before the push
  # after  - String OID of ref target after the push
  #
  # Shells out to `git merge-base --is-ancestor`, which returns 0/1
  #
  # Returns Boolean
  def forced?(before, after)
    !POSIX::Spawn.system("git", "merge-base", "--is-ancestor", before, after)
  end

  def default_branch
    @default_branch ||= get_default_branch
  end

  def get_default_branch
    branch = File.read("HEAD").sub(%r{^ref: refs/heads/}, "").strip
    branch = "master" if branch.empty?
    branch
  rescue Errno::ENOENT
    "master"
  end

  # Internal: Determine whether to reject a force push to a given branch
  #
  # branch - String branch name
  #
  # Returns Boolean
  def fast_forward_only?(branch)
    return true if @reject == "all"
    branch == default_branch && @reject == "default"
  end

  # Internal: Determine whether to reject force pushes *at all*
  #
  # Returns Boolean
  def rejection_enabled?
    @reject != "false"
  end
end
