# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::PreReceiveRefs
  MESSAGE  = "GH002: Sorry, branch or tag names consisting of 40 hex characters are not allowed."

  def initialize(context)
    @context = context
  end

  def check
    warnings, errors = [], []

    return [warnings, errors] unless GitHub.reject_sha_like_refs?

    @context.pushed_refs.each do |before, after, ref|
      # remove invalid byte sequences before testing for validity
      ref = ref.dup.scrub! unless ref.valid_encoding?
      if ref =~ GitHub::SHA_LIKE_REF_NAME
        message = "Invalid branch or tag name \"#{$2}\""

        if before == GitHub::NULL_OID
          errors << "error: #{message}"
        elsif after != GitHub::NULL_OID
          warnings << "warning: #{message}"
        end
      end
    end

    if errors.any?
      errors.unshift("error: #{MESSAGE}")
      GitHub.dogstats.increment("git.hooks", tags: ["action:pre_receive", "error:reject_sha_like_ref"])
    elsif warnings.any?
      warnings.unshift("warning: #{MESSAGE}")
    end

    [warnings, errors]
  end
end
