# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module GitHooks
    def self.setproctitle
      repo =
        begin
          "#{File.read("info/nwo").strip}.git"
        rescue
          File.basename(Dir.pwd)
        end
      $0 = "hooks/#{File.basename($0)}.rb [#{repo}]"
    end
  end
end
