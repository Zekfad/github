# rubocop:disable Style/FrozenStringLiteralComment

require "posix/spawn/child"

class GitHub::PreReceiveBlob
  # Default limit in megabytes
  RECEIVE_LIMIT = 100
  MB = 1048576.0

  class PushRejected < StandardError
    def initialize(errors, warnings, refs, files)
      @errors, @warnings = errors, warnings
      @refs = refs.map do |old, new, name|
        "#{name}: #{new}"
      end
      @files = files.map do |name, size|
        "#{name}: #{size} bytes"
      end
    end
    attr_reader :errors, :warnings, :refs, :files
  end

  attr_reader :context
  attr_reader :limit

  def initialize(context)
    @context = context
    @limit = (context.config["git.maxobjectsize"] || RECEIVE_LIMIT).to_i * MB
  end

  def warning_limit
    [@limit / 2, 50 * MB].min
  end

  def humansize(size)
    sprintf("%.2f", size / MB)
  end

  def check
    start     = Time.now
    bad_files = []
    warnings  = []
    errors    = []

    # limit of 0 means unlimited
    return [warnings, errors] if limit == 0
    return [warnings, errors] unless object_log && File.exist?(object_log)

    large_blobs.each do |sha, size|
      name = sha

      if path = sha_to_path[sha]
        name = path
      end

      if size > limit && GitHub.large_blob_rejection_enabled?
        GitHub.dogstats.histogram("git.hooks.size", size, tags: ["action:pre_receive", "type:large_blob", "error:sizes"])
        bad_files << [name, size]
        errors <<
          "error: File #{name} is #{humansize(size)} MB; " +
          "this exceeds #{GitHub.flavor}'s file size limit of #{humansize(limit)} MB"

      elsif size > warning_limit
        GitHub.dogstats.timing("git.hooks.size", size, tags: ["action:pre_receive", "type:large_blob", "error:warn_sizes"])
        warnings <<
          "warning: File #{name} is #{humansize(size)} MB; " +
          "this is larger than #{GitHub.flavor}'s recommended maximum file " +
          "size of #{humansize(warning_limit)} MB"
      end
    end

    unless errors.empty? && warnings.empty?
      if errors.any?
        e = PushRejected.new(errors, warnings, @context.pushed_refs, bad_files)
        Failbot.report e,
          refs: e.refs.join(", "),
          files: e.files.join(", "),
          repo_id: @context.repo_id,
          app: "github-user"

        prefix = "error"
        bucket = errors
      else
        prefix = "warning"
        bucket = warnings
      end

      unless GitHub.enterprise?
        bucket.unshift "#{prefix}: See http://git.io/iEPt8g for more information."
        bucket.unshift "#{prefix}: Trace: #{@context.error_code}" if errors.any?
      end

      bucket.unshift "#{prefix}: GH001: Large files detected. You may want to try Git Large File Storage - https://git-lfs.github.com."
    end

    [warnings, errors]
  ensure
    GitHub.dogstats.timing("git.hooks", ((Time.now - start) * 1000).round, tags: ["action:pre_receive", "type:large_blob"])
  end

  private
  def new_tips
    @context.pushed_refs.map do |_, after, _|
      after unless after == GitHub::NULL_OID
    end.compact
  end

  # Return a hash { sha1 => path } for objects in sha1s for which
  # paths can be determined.
  def find_paths(sha1s, tips)
    to_find = sha1s.map { |sha| "--find=#{sha}" }
    child = POSIX::Spawn::Child.new("git", "rev-list", *to_find,
                                    "--not", "--all",
                                    "--stdin", input: tips.join("\n") + "\n")
    ret = {}
    child.out.each_line do |line|
      sha1, location = line.chomp.split(" ", 2)
      next unless location
      _, path = location.split("/", 2)
      next unless path
      ret[sha1] = path
    end
    ret
  end

  def sha_to_path
    @sha_to_path ||= find_paths(large_blobs.keys, new_tips)
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def object_log
    @object_log ||= begin
      path = context.quarantine_path
      path && File.join(path, ".large-objects")
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def large_blobs
    @large_blobs ||= begin
      list = {}
      File.open(object_log) do |f|
        f.each_line do |line|
          sha, size = line.split(" ")
          list[sha] = size.to_i
        end
      end
      list
    rescue Errno::ENOENT
      {}
    end
  end
end
