# rubocop:disable Style/FrozenStringLiteralComment

require "posix/spawn/child"

class GitHub::PreReceiveGistDirectories

  class PushWithDirectoriesRejected < StandardError
    def initialize(errors, warnings, refs, directories)
      @errors, @warnings = errors, warnings
      @refs = refs.map do |old, new, name|
        "#{name}: #{new}"
      end
      @directories = directories
    end
    attr_reader :errors, :warnings, :refs, :directories
  end

  attr_reader :limit

  def initialize(context)
    @context = context
  end

  def check
    start     = Time.now
    directories = []
    warnings  = []
    errors    = []

    # limit of 0 means unlimited
    return [warnings, errors] if limit == 0

    directories = directory_names(new_oids)

    if directories.any?
      errors << "Gist does not support directories."
      errors << "These are the directories that are causing problems:"
      errors << directories.join(", ")
    end

    unless errors.empty? && warnings.empty?
      e = PushWithDirectoriesRejected.new(errors, warnings, @context.pushed_refs, directories)
      Failbot.report e,
        refs: e.refs.join(", "),
        directories: e.directories.join(", "),
        # Note: `@context.repo_id` is the gist id (contrary to what the variable name might suggest).
        gist_id: @context.repo_id,
        user: @context.user_login,
        app: "github-user"
    end

    [warnings, errors]
  ensure
    GitHub.dogstats.timing("git.hooks", ((Time.now - start) * 1000).round, tags: ["action:pre_receive", "type:gist_directories"])
  end

  def new_oids
    @context.pushed_refs.map do |old, new, ref|
      new unless new == GitHub::NULL_OID
    end.compact
  end

  def directory_names(new_oids)
    dirs = []
    new_oids.each do |new_oid|
      # -d is only trees
      child = POSIX::Spawn::Child.new("git", "ls-tree", "-d", new_oid)
      child.out.each_line do |line|
        # "<mode> <type> <oid> \t<directory_name>"
        space_delimited, directory_name = line.split("\t")
        dirs << directory_name.strip
      end
    end
    dirs
  end

end
