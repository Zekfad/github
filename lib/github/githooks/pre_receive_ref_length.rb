# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::PreReceiveRefLength
  def initialize(context)
    @context = context
  end

  def check
    warnings, errors = [], []

    maxlen = GitHub.maximum_ref_length
    return [warnings, errors] unless maxlen

    @context.pushed_refs.each do |before, after, ref|
      # Using bytesize rather than character count for future-proofing. The
      # `pushes.refs` column is VARCHAR(255) which supports 3-byte (but not
      # 4-byte, so no emoji) unicode characters. This means a byte-based limit
      # is more aggressive than necessary, but also requires less nuance. The
      # column in the eventual replacement table, `reflog_entries.refs`, is
      # VARBINARY(1024), and when the pushes table is removed entirely, this
      # bytesize comparison will be correct.
      if ref.bytesize > maxlen && after != GitHub::NULL_OID
        errors << "ref too long: \"#{ref}\""
      end
    end

    if errors.any?
      errors.unshift("error: GH005: Sorry, refs longer than #{maxlen} bytes are not allowed.")
      GitHub.dogstats.increment("git.hooks", tags: ["action:pre_receive", "error:reject_too_long_ref"])
    end

    [warnings, errors]
  end
end
