# rubocop:disable Style/FrozenStringLiteralComment

class GitHub::PreReceiveRedirect
  def initialize(context)
    @context = context
  end

  # Check for a redirected repository and issue warning
  def check
    warnings, errors = [], []

    if @context.sockstat_var(:repo_redirect)
      warnings << "This repository moved. Please use the new location:\n  #{@context.repository_url}"
    end

    [warnings, errors]
  end
end
