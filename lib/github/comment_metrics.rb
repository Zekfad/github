# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  # Mixin adds method to measure comment metrics
  #
  # Classes that include CommentMetrics MUST have a `body_contains_markdown?`
  # method
  module CommentMetrics
    def measure_comment_metrics
      tags = ["action:create"]

      tags << "type:contains_markdown" if body_contains_markdown?
      tags << "type:mentions_user" if mentioned_users.length > 0
      tags << "type:mentions_issue" if mentioned_issues.length > 0

      GitHub.dogstats.increment("comment", tags: tags)
    end
  end
end
