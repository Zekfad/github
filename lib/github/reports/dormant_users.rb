# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Reports
    class DormantUsers
      def data
        header   = %w(created_at id login email role suspended?
                      last_logged_ip repos ssh_keys org_memberships
                      dormant? last_active raw_login 2fa_enabled?
                      ssh_keys_last_access pats pats_last_access)
        users    = User.dormant_users

        GitHub::CSV.generate do |csv|
          csv << header
          users.each do |u|
            csv << [u.created_at, u.id, u.login, u.email, (u.site_admin? ? "admin" : "user"),
                    u.suspended?, u.last_ip, u.repositories.count, u.public_keys.count,
                    u.organizations.count, u.dormant?, u.last_active, u.raw_login, u.two_factor_authentication_enabled?,
                    u.last_public_key_access_time, u.oauth_accesses.personal_tokens.size, u.last_personal_token_access_time]
          end
        end
      end
    end
  end
end
