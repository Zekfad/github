# frozen_string_literal: true

require "zeitwerk/inflector"

module GitHub
  class ZeitwerkInflector < Zeitwerk::Inflector
    CUSTOM_INFLECTIONS = {
      "github" => "GitHub",

      # Models
      "advisory_db" => "AdvisoryDB",
      "dns" => "DNS",
      "emoji_regex" => "EMOJI_REGEX",
      "ghvfs" => "GHVFS",
      "rsa" => "RSA",
      "saml" => "SAML",
      "saml_mapping_csv_generator" => "SamlMappingCSVGenerator",
      "saml_mapping_csv_updater" => "SamlMappingCSVUpdater",
      "url" => "URL",
      "user_csv_generator" => "UserCSVGenerator",
      "vso_plan" => "VSOPlan",
      "api_result" => "APIResult",

      # Views
      "pages_https_status_view" => "PagesHTTPSStatusView",
      "clone_url_view" => "CloneURLView",
      "share_and_clone_url_view" => "ShareAndCloneURLView",
      "pages_https_view" => "PagesHTTPSView",

      # API
      "auditlogs_api_handler" => "AuditlogsAPIHandler",
      "organizations_scim" => "OrganizationsScim",
      "users_api_handler" => "UsersAPIHandler",
      "businesses_api_handler" => "BusinessesAPIHandler",
      "tokens_api_handler" => "TokensAPIHandler",
      "graph_ql" => "GraphQL",
      "enterprises_scim" => "EnterprisesScim",
      "following_api_handler" => "FollowingAPIHandler",
      "repositories_api_handler" => "RepositoriesAPIHandler",
      "issues_api_handler" => "IssuesAPIHandler",
      "hooks_api_handler" => "HooksAPIHandler",
      "features_api_handler" => "FeaturesAPIHandler",
      "repository_api_handler" => "RepositoryAPIHandler",
      "pull_requests_api_handler" => "PullRequestsAPIHandler",
      "issue_events_api_handler" => "IssueEventsAPIHandler",
      "public_key_api_handler" => "PublicKeyAPIHandler",
      "oauth_access_api_handler" => "OauthAccessAPIHandler",
      "secret_scanning_api_handler" => "SecretScanningAPIHandler",
      "actions_api_handler" => "ActionsAPIHandler",
      "graph_ql_authentication_fingerprint" => "GraphQLAuthenticationFingerprint",
      "enterprise_groups_scim" => "EnterpriseGroupsScim",
      "enterprise_users_scim" => "EnterpriseUsersScim",
      "scim_dependency" => "ScimDependency",
    }

    ROOT = File.expand_path("../../..", __FILE__)

    EXACT_PATHS = {
      "#{ROOT}/app/models/lfs" => "LFS",
      "#{ROOT}/lib/platform/helpers/url.rb" => "Url",
      "#{ROOT}/lib/platform/loaders/kv.rb" => "KV",
      "#{ROOT}/lib/platform/loaders/url.rb" => "Url",
      "#{ROOT}/lib/platform/objects/external_identity_scim_attributes.rb" => "ExternalIdentityScimAttributes",
      "#{ROOT}/lib/platform/provisioning/scim_mapper.rb" => "ScimMapper",
      "#{ROOT}/lib/platform/provisioning/scim_user_data.rb" => "ScimUserData",
      "#{ROOT}/lib/platform/scalars/git_object_id.rb" => "GitObjectID",
      "#{ROOT}/lib/platform/scalars/git_ssh_remote.rb" => "GitSSHRemote",
      "#{ROOT}/lib/platform/scalars/html.rb" => "HTML",
      "#{ROOT}/lib/platform/scalars/uri.rb" => "URI",
      "#{ROOT}/lib/git_signing/gpg.rb" => "GPG",
      "#{ROOT}/lib/git_signing/smime.rb" => "SMIME",
    }

    def camelize(basename, fullpath)
      if override = EXACT_PATHS[fullpath]
        override
      elsif override = CUSTOM_INFLECTIONS[basename]
        override
      else
        basename.camelize
      end
    end
  end
end
