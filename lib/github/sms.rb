# rubocop:disable Style/FrozenStringLiteralComment

require_relative "sms/provider"
require_relative "sms/twilio"
require_relative "sms/nexmo"
require_relative "sms/local"
require_relative "sms/test"
require_relative "sms/error"

module GitHub
  module SMS
    PROVIDERS = {
      production:  [Nexmo, Twilio],
      development:  [Local, Local],
      test:  [Test, TestTwo],
    }.with_indifferent_access

    PROVIDERS_FOR_INDIA = {
      production: [Twilio],
      development: PROVIDERS[:development],
      test: PROVIDERS[:test],
    }.with_indifferent_access

    # Number of times to attempt SMS delivery.
    DELIVERY_ATTEMPTS = 2

    # Valid phone number Regexp (Eg. "+1 7736829478")
    PHONE_NUMBER_REGEX = /\A\+\d+ [1-9]\d+\z/

    # Regexp for splitting out the country code from a phone number.
    PHONE_NUMBER_PARTS_REGEX = /\A\+(\d+) (.+)\z/

    # Rate limit constants used to configure a rate limiter to prevent unbounded
    # SMS messages sent to a given number.
    SMS_RATE_LIMIT_MAX_TRIES = 10
    SMS_RATE_LIMIT_TTL = 10.minutes

    # Public: Try delivering an SMS using a random ordering of available SMS
    # providers. Only try DELIVERY_ATTEMPTS times.
    #
    # to       - The number to send the message to.
    # message  - The message to send.
    # options  - A Hash of optional settings.
    #            :attempts - The number of attempts to make.
    #            :provider - The provider to send the message with or the name
    #                        of the provider.
    #
    # Returns a GitHub::SMS::Receipt. Raises GitHub::SMS::Error.
    def send_message(to, message, options = {})
      to = normalize_for_provider(to)
      attempts = options.fetch(:attempts, DELIVERY_ATTEMPTS)
      provider = get_provider(options[:provider])
      raise RateLimitError if rate_limited?(to)
      provider.send_message(to, message)
    rescue ServerError, NumberNotMobileError, BillingError
      if attempts > 1
        send_message(to, message,
          attempts: (attempts - 1),
          provider: get_alternate_provider(provider)
        )
      else
        raise
      end
    end

    # Public: Get all SMS logs for a recipient.
    #
    # recipient - The number to get logs for.
    # mask_codes - Whether the codes in the messages should be masked.
    #
    # Returns an Array of log messages.
    def log_for(recipient, mask_codes = true)
      providers_for_env.map do |provider|
        log = provider.log_for(recipient)
        if mask_codes
          log.each do |entry|
            unless entry[:body] =~ /github support/i
              entry[:body] = entry[:body].gsub(/[0-9a-f]{6}/i, "*" * 6)
            end
          end
        end
        log
      end.flatten.sort_by do |entry|
        entry[:date]
      end.reverse
    end

    # Get the primary provider to use given an optionally configured provider.
    #
    # provider - A String/Symbol provider name or Provider subclass instance.
    #
    # Returns a Provider instance subclass.
    def get_provider(provider = nil)
      case provider
      when String, Symbol
        find_provider_by_name(provider)
      when Provider
        provider
      when nil
        providers_for_env.first
      end
    end

    # Get the alternate provider to use given an optionally configured primary
    # provider.
    #
    # provider - The Provider or provider name that shouldn't be returned.
    #
    # Return a Provider instance.
    def get_alternate_provider(provider)
      # Resolve String, Symbol, nil providers.
      provider = get_provider(provider) unless provider.is_a?(Provider)

      if providers_for_env.first == provider
        providers_for_env.last
      else
        providers_for_env.first
      end
    end

    # Get a Provider instance by name.
    #
    # name - The Symbol or String name of the provider.
    #
    # Returns a Provider instance.
    def find_provider_by_name(name)
      name = name.to_sym
      providers_for_env.find do |provider|
        provider.provider_name == name
      end
    end

    def select_provider(number, primary_provider:, last_provider:)
      # India currently only works with Nexmo, so we special case this one
      if number_from_india?(number)
        provider_for_india.provider_name
      # If provider is provided then this is likely a "retry" and we should try
      # the alternate SMS provider.
      elsif last_provider.present?
        get_alternate_provider(last_provider).provider_name
      # If this is the first try and the primary 2FA method is SMS, use the provider in the db
      # as we currently only have one DB column to track the preferred SMS
      # provider and we don't want to switch away from a provider we believe
      # has worked well for the primary number.
      elsif primary_provider.present?
        primary_provider
      else
        get_provider.provider_name
      end
    end

    # Public: Memoized random ordering of providers for this environment.
    #
    # Returns an Array of instances of subclasses of SMS::Provider.
    def providers_for_env
      @providers_for_env ||= PROVIDERS[Rails.env].map(&:new)
    end

    # A very hacky way to ensure we use Twilio as the provider for India for now
    # since we do not have Nexmo setup to work with them yet
    def provider_for_india
      @provider_for_india ||= PROVIDERS_FOR_INDIA[Rails.env].first.send(:new)
    end

    # Public: Normalize a phone number.
    # A normalized phone number should start with `+`, followed by the country
    # code, then a space, then the body of the number. Eg. `+1 7736829478`
    #
    # number - A phone number String.
    #
    # Returns a normalized phone number String.
    def normalize_number(number)
      # Make sure we don't have a weird type from params.
      number = number.to_s

      # Split out country code.
      match = number.match(PHONE_NUMBER_PARTS_REGEX)
      raise NumberNotValidError unless match
      country, number = match[1..2]

      # Remove any non-numeric characters from number.
      number.gsub!(/\D/, "")

      # Strip any leading zeros.
      number.sub!(/\A0+/, "")

      # Check that the number isn't empty now that we stripped non-numeric chars
      raise NumberNotValidError if number.empty?

      # Sweet reunion
      "+#{country} #{number}"
    end

    def normalize_for_provider(number)
      parsed = Phonelib.parse(number)
      raise NumberNotValidError unless parsed.valid?
      parsed.e164
    end

    # A hacky method to check for numbers from India, so that we only send them messages via
    # Twilio, until we configure Nexmo to also support India
    def number_from_india?(number)
      parsed_number = Phonelib.parse(number)

      # 91 is the country code for India
      parsed_number&.country_code == "91"
    end

    # Public: Checks if a phone number is correctly formatted.
    #
    # number - A phone number String.
    #
    # Returns boolean.
    def valid_number?(number)
      !!(PHONE_NUMBER_REGEX =~ number)
    end

    # Public: Checks if we should stop sending SMS messages to a given number.
    #
    # number - A phone number String.
    #
    # Returns boolean.
    def rate_limited?(number)
      return false unless number.present?

      options = {
        max_tries: SMS_RATE_LIMIT_MAX_TRIES,
        ttl: SMS_RATE_LIMIT_TTL,
      }

      at_limit = RateLimiter.at_limit?(
        "sms-rate-limit:#{number}", options
      )

      if at_limit
        GitHub.dogstats.increment("sms.rate_limited")
      end
      at_limit
    end

    # Some countries share a country code so we keep this in a nested array
    COUNTRY_CODES = [
      ["+93",   "Afghanistan"],
      ["+358",  "Aland Islands"],
      ["+355",  "Albania"],
      ["+213",  "Algeria"],
      ["+1684", "American Samoa"],
      ["+376",  "Andorra"],
      ["+244",  "Angola"],
      ["+1264", "Anguilla"],
      ["+1268", "Antigua and Barbuda"],
      ["+54",   "Argentina"],
      ["+374",  "Armenia"],
      ["+297",  "Aruba"],
      ["+247",  "Ascension"],
      ["+61",   "Australia"],
      ["+43",   "Austria"],
      ["+994",  "Azerbaijan"],
      ["+1",    "Bahamas"],
      ["+973",  "Bahrain"],
      ["+880",  "Bangladesh"],
      ["+1246", "Barbados"],
      ["+375",  "Belarus"],
      ["+32",   "Belgium"],
      ["+501",  "Belize"],
      ["+229",  "Benin"],
      ["+1441", "Bermuda"],
      ["+975",  "Bhutan"],
      ["+591",  "Bolivia"],
      ["+387",  "Bosnia and Herzegovina"],
      ["+267",  "Botswana"],
      ["+55",   "Brazil"],
      ["+673",  "Brunei"],
      ["+359",  "Bulgaria"],
      ["+226",  "Burkina Faso"],
      ["+257",  "Burundi"],
      ["+855",  "Cambodia"],
      ["+237",  "Cameroon"],
      ["+1",    "Canada"],
      ["+3491", "Canary Islands"],
      ["+238",  "Cape Verde"],
      ["+1345", "Cayman Islands"],
      ["+236",  "Central Africa"],
      ["+235",  "Chad"],
      ["+56",   "Chile"],
      ["+86",   "China"],
      ["+61",   "Christmas Island"],
      ["+61",   "Cocos"],
      ["+57",   "Colombia"],
      ["+269",  "Comoros"],
      ["+242",  "Congo"],
      ["+243",  "Congo, Dem Rep"],
      ["+506",  "Costa Rica"],
      ["+385",  "Croatia"],
      ["+357",  "Cyprus"],
      ["+420",  "Czech Republic"],
      ["+45",   "Denmark"],
      ["+253",  "Djibouti"],
      ["+1767", "Dominica"],
      ["+1",    "Dominican Republic"],
      ["+593",  "Ecuador"],
      ["+20",   "Egypt"],
      ["+503",  "El Salvador"],
      ["+240",  "Equatorial Guinea"],
      ["+291",  "Eritrea"],
      ["+372",  "Estonia"],
      ["+251",  "Ethiopia"],
      ["+298",  "Faroe Islands"],
      ["+679",  "Fiji"],
      ["+358",  "Finland/Aland Islands"],
      ["+33",   "France"],
      ["+594",  "French Guiana"],
      ["+689",  "French Polynesia"],
      ["+241",  "Gabon"],
      ["+220",  "Gambia"],
      ["+995",  "Georgia"],
      ["+49",   "Germany"],
      ["+233",  "Ghana"],
      ["+350",  "Gibraltar"],
      ["+30",   "Greece"],
      ["+299",  "Greenland"],
      ["+1473", "Grenada"],
      ["+590",  "Guadeloupe"],
      ["+1671", "Guam"],
      ["+502",  "Guatemala"],
      ["+224",  "Guinea"],
      ["+592",  "Guyana"],
      ["+509",  "Haiti"],
      ["+504",  "Honduras"],
      ["+852",  "Hong Kong"],
      ["+36",   "Hungary"],
      ["+354",  "Iceland"],
      ["+91",   "India"],
      ["+62",   "Indonesia"],
      ["+98",   "Iran"],
      ["+964",  "Iraq"],
      ["+353",  "Ireland"],
      ["+972",  "Israel"],
      ["+39",   "Italy"],
      ["+225",  "Ivory Coast"],
      ["+1876", "Jamaica"],
      ["+81",   "Japan"],
      ["+962",  "Jordan"],
      ["+7",    "Kazakhstan"],
      ["+254",  "Kenya"],
      ["+850",  "Korea Dem People's Rep"],
      ["+883",  "Kosovo"],
      ["+965",  "Kuwait"],
      ["+996",  "Kyrgyzstan"],
      ["+856",  "Laos PDR"],
      ["+371",  "Latvia"],
      ["+961",  "Lebanon"],
      ["+266",  "Lesotho"],
      ["+231",  "Liberia"],
      ["+218",  "Libya"],
      ["+423",  "Liechtenstein"],
      ["+370",  "Lithuania"],
      ["+352",  "Luxembourg"],
      ["+853",  "Macau"],
      ["+389",  "Macedonia"],
      ["+261",  "Madagascar"],
      ["+265",  "Malawi"],
      ["+60",   "Malaysia"],
      ["+960",  "Maldives"],
      ["+223",  "Mali"],
      ["+356",  "Malta"],
      ["+692",  "Marshall Islands"],
      ["+596",  "Martinique"],
      ["+222",  "Mauritania"],
      ["+230",  "Mauritius"],
      ["+262",  "Mayotte"],
      ["+52",   "Mexico"],
      ["+691",  "Micronesia"],
      ["+373",  "Moldova"],
      ["+377",  "Monaco"],
      ["+976",  "Mongolia"],
      ["+382",  "Montenegro"],
      ["+1664", "Montserrat"],
      ["+212",  "Morocco/Western Sahara"],
      ["+258",  "Mozambique"],
      ["+95",   "Myanmar"],
      ["+264",  "Namibia"],
      ["+977",  "Nepal"],
      ["+31",   "Netherlands"],
      ["+599",  "Netherlands Antilles"],
      ["+687",  "New Caledonia"],
      ["+64",   "New Zealand"],
      ["+505",  "Nicaragua"],
      ["+227",  "Niger"],
      ["+234",  "Nigeria"],
      ["+1670", "Northern Mariana Islands"],
      ["+47",   "Norway"],
      ["+968",  "Oman"],
      ["+92",   "Pakistan"],
      ["+680",  "Palau"],
      ["+970",  "Palestinian Territory"],
      ["+507",  "Panama"],
      ["+595",  "Paraguay"],
      ["+51",   "Peru"],
      ["+63",   "Philippines"],
      ["+48",   "Poland"],
      ["+351",  "Portugal"],
      ["+1787", "Puerto Rico"],
      ["+974",  "Qatar"],
      ["+262",  "Reunion"],
      ["+40",   "Romania"],
      ["+7",    "Russia"],
      ["+250",  "Rwanda"],
      ["+685",  "Samoa"],
      ["+378",  "San Marino"],
      ["+966",  "Saudi Arabia"],
      ["+221",  "Senegal"],
      ["+381",  "Serbia"],
      ["+248",  "Seychelles"],
      ["+232",  "Sierra Leone"],
      ["+65",   "Singapore"],
      ["+421",  "Slovakia"],
      ["+386",  "Slovenia"],
      ["+252",  "Somalia"],
      ["+27",   "South Africa"],
      ["+82",   "South Korea"],
      ["+34",   "Spain"],
      ["+94",   "Sri Lanka"],
      ["+1869", "St Kitts and Nevis"],
      ["+1758", "St Lucia"],
      ["+508",  "St Pierre and Miquelon"],
      ["+1784", "St Vincent Grenadines"],
      ["+249",  "Sudan"],
      ["+597",  "Suriname"],
      ["+268",  "Swaziland"],
      ["+46",   "Sweden"],
      ["+41",   "Switzerland"],
      ["+963",  "Syria"],
      ["+886",  "Taiwan"],
      ["+992",  "Tajikistan"],
      ["+255",  "Tanzania"],
      ["+66",   "Thailand"],
      ["+228",  "Togo"],
      ["+676",  "Tonga"],
      ["+1868", "Trinidad and Tobago"],
      ["+216",  "Tunisia"],
      ["+90",   "Turkey"],
      ["+90",   "Turkish Republic of Northern Cyprus"],
      ["+993",  "Turkmenistan"],
      ["+1649", "Turks and Caicos Islands"],
      ["+688",  "Tuvalu"],
      ["+256",  "Uganda"],
      ["+380",  "Ukraine"],
      ["+971",  "United Arab Emirates"],
      ["+44",   "United Kingdom"],
      ["+1",    "United States"],
      ["+598",  "Uruguay"],
      ["+998",  "Uzbekistan"],
      ["+379",  "Vatican City"],
      ["+58",   "Venezuela"],
      ["+84",   "Vietnam"],
      ["+1284", "Virgin Islands, British"],
      ["+1340", "Virgin Islands, U.S."],
      ["+967",  "Yemen"],
      ["+260",  "Zambia"],
      ["+263",  "Zimbabwe"],
    ]

    # A hash of country codes and names of countries that we support.
    # Based on previously available delivery rates. These are no longer
    # provided by Twilio
    SUPPORTED_COUNTRIES = [
      ["+358", "Aland Islands"],
      ["+213", "Algeria"],
      ["+244", "Angola"],
      ["+1264", "Anguilla"],
      ["+61", "Australia"],
      ["+43", "Austria"],
      ["+1", "Bahamas"],
      ["+973", "Bahrain"],
      ["+880", "Bangladesh"],
      ["+375", "Belarus"],
      ["+32", "Belgium"],
      ["+229", "Benin"],
      ["+591", "Bolivia"],
      ["+387", "Bosnia and Herzegovina"],
      ["+673", "Brunei"],
      ["+359", "Bulgaria"],
      ["+257", "Burundi"],
      ["+855", "Cambodia"],
      ["+1", "Canada"],
      ["+238", "Cape Verde"],
      ["+1345", "Cayman Islands"],
      ["+61", "Christmas Island"],
      ["+61", "Cocos"],
      ["+243", "Congo, Dem Rep"],
      ["+385", "Croatia"],
      ["+357", "Cyprus"],
      ["+420", "Czech Republic"],
      ["+45", "Denmark"],
      ["+1767", "Dominica"],
      ["+1", "Dominican Republic"],
      ["+593", "Ecuador"],
      ["+240", "Equatorial Guinea"],
      ["+372", "Estonia"],
      ["+358", "Finland/Aland Islands"],
      ["+33", "France"],
      ["+220", "Gambia"],
      ["+995", "Georgia"],
      ["+49", "Germany"],
      ["+233", "Ghana"],
      ["+350", "Gibraltar"],
      ["+30", "Greece"],
      ["+502", "Guatemala"],
      ["+592", "Guyana"],
      ["+36", "Hungary"],
      ["+354", "Iceland"],
      ["+62", "Indonesia"],
      ["+91", "India"],
      ["+98", "Iran"],
      ["+353", "Ireland"],
      ["+972", "Israel"],
      ["+39", "Italy"],
      ["+225", "Ivory Coast"],
      ["+1876", "Jamaica"],
      ["+81", "Japan"],
      ["+962", "Jordan"],
      ["+7", "Kazakhstan"],
      ["+965", "Kuwait"],
      ["+371", "Latvia"],
      ["+218", "Libya"],
      ["+423", "Liechtenstein"],
      ["+370", "Lithuania"],
      ["+352", "Luxembourg"],
      ["+261", "Madagascar"],
      ["+265", "Malawi"],
      ["+60", "Malaysia"],
      ["+960", "Maldives"],
      ["+223", "Mali"],
      ["+356", "Malta"],
      ["+230", "Mauritius"],
      ["+52", "Mexico"],
      ["+377", "Monaco"],
      ["+382", "Montenegro"],
      ["+1664", "Montserrat"],
      ["+258", "Mozambique"],
      ["+264", "Namibia"],
      ["+31", "Netherlands"],
      ["+599", "Netherlands Antilles"],
      ["+64", "New Zealand"],
      ["+234", "Nigeria"],
      ["+47", "Norway"],
      ["+63", "Philippines"],
      ["+48", "Poland"],
      ["+351", "Portugal"],
      ["+974", "Qatar"],
      ["+40", "Romania"],
      ["+7", "Russia"],
      ["+250", "Rwanda"],
      ["+221", "Senegal"],
      ["+381", "Serbia"],
      ["+248", "Seychelles"],
      ["+65", "Singapore"],
      ["+421", "Slovakia"],
      ["+386", "Slovenia"],
      ["+27", "South Africa"],
      ["+82", "South Korea"],
      ["+34", "Spain"],
      ["+94", "Sri Lanka"],
      ["+1758", "St Lucia"],
      ["+249", "Sudan"],
      ["+46", "Sweden"],
      ["+41", "Switzerland"],
      ["+886", "Taiwan"],
      ["+255", "Tanzania"],
      ["+228", "Togo"],
      ["+1868", "Trinidad and Tobago"],
      ["+1649", "Turks and Caicos Islands"],
      ["+256", "Uganda"],
      ["+380", "Ukraine"],
      ["+971", "United Arab Emirates"],
      ["+44", "United Kingdom"],
      ["+1", "United States"],
      ["+998", "Uzbekistan"],
      ["+58", "Venezuela"],
    ]

    # Hash of country codes =>  array[code, name] for countries that we don't support.
    UNSUPPORTED_COUNTRY_CODES = COUNTRY_CODES.dup.each_with_object({}) do |pair, hsh|
      unless SUPPORTED_COUNTRIES.include? pair
        hsh[pair.first] ||= []
        hsh[pair.first] << [pair.first, pair.last]
      end
    end

    extend self
  end
end
