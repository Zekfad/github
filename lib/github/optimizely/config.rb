# frozen_string_literal: true

require "erb"
require "yaml"

require "github/optimizely/client"
require "github/optimizely/noop_client"

module GitHub
  module Optimizely
    class Error < StandardError; end

    module Config
      FILENAME="#{Rails.root}/config/optimizely.yml"
      DATAFILE_URL="https://cdn.optimizely.com"
      EVENTS_URL="https://logx.optimizely.com/v1/events"

      def self.build_client
        enabled? ? Client.new : NoopClient.new
      rescue Error => e
        Failbot.report(e)
        NoopClient.new
      end

      def self.config
        @config ||= yaml[Rails.env].deep_symbolize_keys
      end

      def self.yaml
        YAML.load(ERB.new(File.read(FILENAME)).result)
      end

      def self.enabled?
        config[:enabled] && GitHub.online? && !GitHub.enterprise?
      end

      def self.auth_key
        required(:auth_key)
      end

      def self.sdk_key
        required(:sdk_key)
      end

      def self.required(key)
        config[key].presence.tap do |present|
          fail Error, "Missing required optimizely configuration: #{key}." unless present
        end
      end
      private_class_method :required

      def optimizely
        @optimizely ||= Config.build_client
      end

      def optimizely_reset!
        @optimizely = nil
      end
    end
  end

  extend Optimizely::Config
end
