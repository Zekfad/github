# frozen_string_literal: true

require "logger"

require "github/logger"

# This class is an adapter to make the GitHub::Logger/scrolls API compatible with
# Ruby's standard library logger. I'm sad it is not already compatible.
module GitHub
  module Optimizely
    class Logger < SimpleDelegator
      # The Scrolls severity map is backwards.
      # (reversed int ordering and uses different keywords)
      LOGGER_TO_SCROLLS = %i[debug info warning error critical]

      # Scrolls doesn't accept log levels as integers, ala the Logger API,
      # we need to map the Logger int to the Scrolls symbol name.
      def self.scrolls_level_from(severity)
        # default to emergency because Scrolls will throw an exception
        # if you send an unrecognized log level.
        # This way an unknown level will at least be logged; not ignored or thrown.
        LOGGER_TO_SCROLLS[severity] || :emergency
      end

      attr_accessor :progname

      def initialize(logger = ::GitHub::Logger, progname: nil)
        self.progname = progname
        super(logger)
      end

      def log(severity, message = nil, progname = @progname)
        message = yield if message.nil?
        structured = message.is_a?(Hash) ? message : {message: message}

        super({
          # `level` is a special key which will silence messages below the configured level
          severity: self.class.scrolls_level_from(severity),
          progname: progname,
        }.merge(structured))
      end
      alias :add :log

      %i[debug info warn error fatal unknown].each do |level|
        define_method level do |message = nil, &block|
          log(::Logger.const_get(level.upcase), message, &block)
        end
      end
    end
  end
end
