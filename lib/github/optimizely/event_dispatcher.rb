# frozen_string_literal: true

require "logger"
require "net/http"

require "optimizely/logger"

module GitHub
  module Optimizely
    class EventDispatcher
      def initialize(logger = ::Optimizely::NoOpLogger.new)
        @logger = logger
      end

      def dispatch_event(event)
        req = http_request(event)
        @logger.log(::Logger::DEBUG, url: req.uri, headers: req.to_hash, body: req.body)

        GitHub.dogstats.time("optimizely.http", opts = {tags: []}) do
          @logger.log ::Logger::DEBUG, "Dispatching event: #{req.uri}"
          Net::HTTP.new(req.uri.host).request(req) do |res|
            @logger.log ::Logger::INFO, "Event dispatched. Response: #{res.code}"
            opts[:tags] << "response:#{res.code}"
            res.value # raises http errors
          end
        end
      end

      private

      def http_request(event)
        uri = URI(event.url)

        case event.http_verb
        when :get
          uri.query = URI.encode_www_form(event.params)
          Net::HTTP::Get.new(uri, event.headers)
        when :post
          Net::HTTP::Post.new(uri, event.headers).tap do |req|
            req.body = event.params.to_json
          end
        end
      end
    end
  end
end
