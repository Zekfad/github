# frozen_string_literal: true
require "failbot"

module GitHub
  module Optimizely
    class ErrorHandler
      def handle_error(error)
        Failbot.report(error, app: "github-optimizely")
      end
    end
  end
end
