# frozen_string_literal: true

module GitHub
  module AzureServiceBus
    autoload :Client, "github/azure_service_bus/client"
    autoload :Consumer, "github/azure_service_bus/consumer"
    autoload :Executor, "github/azure_service_bus/executor"
    autoload :Message, "github/azure_service_bus/message"
    autoload :SharedAccessSigner, "github/azure_service_bus/shared_access_signer"
  end
end
