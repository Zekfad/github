# frozen_string_literal: true

module GitHub
  # Helper to log the exceptions to Splunk with full, unredacted context
  # before it gets filtered to be sent to Sentry
  class FailbotLogger
    def initialize(enabled: false)
      @enabled = enabled
    end

    # Method to check if we should log exceptions, or just treat this as a no-op
    def enabled?
      @enabled
    end

    # Log an exception to '/var/log/syslog'.
    #
    # NOTE: If this method is called from Failbot.before_report, data's keys are Strings
    def log_exception(data, exception)
      return unless enabled?

      # Force the exception to '/var/log/syslog' so it gets picked up by Splunk,
      # and so that it does not leak to the user via stdout during the pre-receive
      # phase.
      GitHub::Logger.with_destination(GitHub::Config::Logging::Destination::SYSLOG) do
        # Log an exception to Splunk with full, unredacted context before it gets filtered to be sent to Sentry
        #
        # Use log instead of log_exception, becasue log_exception will return early without logging
        # if the Error doesn't have a backtrace, which can happen if you `Failbot.report(Error.new)`
        #
        # In addition, log_exception has logic to extract data out of the exception as key/values, but
        # failbot has already done that for us, so it's already in context.
        GitHub::Logger.log(adjusted_data(data, exception))
      end
    end

    def adjusted_data(data, exception)
      # Ensure failbot_id is always inserted in the new hash first.  Ruby
      # iterates over hashes in insertion order and we want to make sure
      # failbot_id is always first, incase the message is truncated.
      adjusted_data = {}
      adjusted_data["failbot_id"] = data.delete("failbot_id") if data.has_key?("failbot_id")

      # app at this point represents the haystack bucket / sentry project being reported too
      # GitHub::Logger has a different app that represents that app logging, ie github
      # so we need make it have a different key
      # TODO may as well rename this key to something that isn't overloaded?
      adjusted_data["failbot_app"] = data.delete("app") if data.has_key?("app")

      # Remove the stacktrace, but save off the type and value.  The stack trace
      # is generally huge and not very readable in the logs and should always be
      # in Sentry.
      data.delete("exception_detail")
      adjusted_data["exception_type"] = exception.class.name.to_s.gsub("\n", "\\n")
      adjusted_data["exception_value"] = exception.message.to_s.gsub("\n", "\\n")
      if (cause = exception.cause)
        causes = []
        depth = 0
        loop do
          causes.unshift(
            "type" => cause.class.name.to_s.gsub("\n", "\\n"),
            "value" => cause.message.to_s.gsub("\n", "\\n")
          )
          depth += 1
          break unless (cause = cause.cause)
          break if depth > Failbot::MAXIMUM_CAUSE_DEPTH
        end
        adjusted_data["causes"] = causes
      end

      # Each line of a logged data to syslog is treated as its own event.
      # Therefore, we need to make sure to escape newlines and remove quotes
      # Otherwise, each line will be its own event, and be missing the rest of the context
      # This impacts backtrace values the most
      data.each do |key, value|
        adjusted_data[key] = data[key].to_s.gsub("\n", "\\n")
      end

      adjusted_data
    end
  end
end
