# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module LDAP
    require "github/ldap" # this loads classes from github-ldap gem.
    require "github/ldap/search"

    def self.debug_logging_enabled?
      GitHub.cache.fetch("ldap.debug_logging_enabled", ttl: 1.minute) do
        GitHub.config.get("ldap.debug_logging_enabled") == "true"
      end
    end
  end
end
