# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Routers
  autoload :Api, "github/routers/api"

  # Alias the constant while we transition to just using GitHub::Routers::Api
  Router = Api
end
