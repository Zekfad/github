# rubocop:disable Style/FrozenStringLiteralComment

require "open-uri"
require "github/json"
require "resilient/circuit_breaker"

module GitHub
  # Aka GitHub::Jobs (jobs.github.com).
  # "jobs" is already means "background job" here.
  module Positions
    REQUEST_TIMEOUT = 2.0
    HOST_URL = "https://jobs.github.com".freeze

    def self.widget(user)
      cached_request(widget_path(user))
    end

    def self.request(path)
      # Skip fetch unless we're on the internets
      return unless GitHub.online?

      if circuit_breaker.allow_request?
        begin
          json = URI.open("#{HOST_URL}/#{path}", read_timeout: REQUEST_TIMEOUT).read
          circuit_breaker.success

          GitHub::JSON.parse(json)
        rescue Errno::ECONNREFUSED, OpenURI::HTTPError, Timeout::Error, Net::ReadTimeout, SocketError, Yajl::ParseError
          circuit_breaker.failure

          nil
        end
      end
    end
    private_class_method :request

    def self.circuit_breaker
      Resilient::CircuitBreaker.get("positions.json", {
        instrumenter: GitHub,
        sleep_window_seconds:       10,
        request_volume_threshold:   2,
        error_threshold_percentage: 50,
        window_size_in_seconds:     300,
        bucket_size_in_seconds:     30,
      })
    end
    private_class_method :circuit_breaker

    def self.cached_request(path)
      GitHub.cache.fetch("jobs:request:v1:#{path}", ttl: 1.minute, stats_key: "positions.request") do
        request(path)
      end
    end
    private_class_method :cached_request

    def self.widget_path(user)
      params = {}
      params[:location] = user.profile_location if user && user.profile_location.present?
      "positions.json?#{params.to_param}"
    end
    private_class_method :widget_path
  end
end
