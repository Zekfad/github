# rubocop:disable Style/FrozenStringLiteralComment

require "uri"
require "mustermann"

# Rack app responsible for sending API requests to the appropriate Sinatra
# app.  This lives in the GitHub module so that Rails reloading leaves it
# alone.
module GitHub::Routers
  class Api
    API_PATH = "github.api_path".freeze
    DEPRECATED_ROUTE = "deprecated_route".freeze
    QUERY_STRING = "QUERY_STRING".freeze
    PATH_INFO = "PATH_INFO".freeze
    SERVER_NAME = "SERVER_NAME".freeze
    ThisAppKey = "github.this_app_slug".freeze
    ThisUserKey = "github.this_user".freeze
    ThisTeamKey = "github.this_team".freeze
    ThisRepositoryKey = "github.this_repository".freeze
    ThisRepositoryNameWithOwnerKey = "github.this_repository_nwo".freeze
    ProcessCategoryKey = "process.request_category".freeze
    ProcessCategoryDataDogKey = "process.request_category_datadog".freeze
    ProcessCategory = "api".freeze
    Accept = "HTTP_ACCEPT".freeze
    TailingStatusRegex = %r{/repositories/(?<repo_id>\d+)/commits/(?<ref>.*)/(?<status>status(es)?)$}
    # Extend this to other resources by changing the (?<resource>(check-suites|check-runs)) bit like so:
    # (?<resource>(check-suites|check-runs|my-other-resource)).
    AmbiguousCommitsAPIRefRegex = %r{/repositories/(?<repo_id>\d+)/commits/(?<ref>.*)/(?<resource>(check-suites|check-runs))(/$|$)}

    # Regexp to match against SERVER_NAME. If the pattern does # not match,
    # the request is forwarded to the next middleware in the chain.
    # This matches dotcom API requests (i.e. "api.github.com").
    HOST_PATTERN = /(^|\.)api\./

    # Matched against SERVER_NAME for internal API requests.
    INTERNAL_API_PATTERN = /^internal-api\.service\./

    # For some environments, we use a path prefix to determine API calls instead
    # of a host name. This Regexp will be matched against PATH_INFO to extract
    # the actual api path versus the prefix.
    PATH_PREFIX = %r{^(/api/v3)(.*)$}

    # Match GraphQL at the toplevel so it's not nested under v3
    GRAPHQL_PATH_PREFIX = %r{^(/api)(/graphql.*)$}

    def initialize(app)
      @app = app
    end

    # This is where the Rack magic happens.
    #
    # env - The Hash environment of the current Rack request..
    #
    # Returns an Array Rack response.
    def call(env)
      app_for(env).call(env)
    end

    # Internal: Route to the correct Rack app.
    #
    # Returns a Rack middlware
    def app_for(env)
      if env[SERVER_NAME] =~ HOST_PATTERN
        select_app(env)
      elsif env[SERVER_NAME] =~ INTERNAL_API_PATTERN
        select_app(env)
      elsif use_path_prefix? && (env[PATH_INFO] =~ GRAPHQL_PATH_PREFIX || env[PATH_INFO] =~ PATH_PREFIX)
        env[API_PATH]  = $1
        env[PATH_INFO] = $2
        select_app(env)
      elsif Rails.env.development? && env[PATH_INFO].start_with?("/lfs")
        select_app(env)
      elsif Rails.env.test? && env[PATH_INFO] =~ %r{/git/pushes\z|pre-receive-hooks|password_auth}
        select_app(env)
      else
        @app
      end
    end

    # Internal: Should we use an API path prefix in determining what router to call?
    #
    # Returns Boolean
    def use_path_prefix?
      GitHub.use_api_path_prefix?
    end

    # Determines which Rack app to pass the request to.  API requests get a
    # Sinatra app.  Other requests are passed down the chain (to Rails).  In the
    # future we may want to look at the Accept header or ?v param to do better
    # content negotiation for API v4.
    #
    # env - The Hash environment of the current Rack request..
    #
    # Returns a Rack object.
    def select_app(env)
      # mark this as an API request
      env[ProcessCategoryKey] = env[ProcessCategoryDataDogKey] = ProcessCategory
      Failbot.push(request_category: ProcessCategory)

      route_path = routable_path(env)

      app = case route_path
        when %r{^internal/user/\d+/avatars(/|$)},
             %r{^internal/organizations/\d+/avatars(/|$)},
             %r{^internal/oauth-applications/\d+/avatars(/|$)},
             %r{^internal/integrations/\d+/avatars(/|$)},
             %r{^internal/teams/\d+/avatars(/|$)},
             %r{^internal/businesses/\d+/avatars(/|$)},
             %r{^internal/user/avatars(/|$)}
          ::Api::Internal::Avatars

        when %r{^internal/repositories/\d+/media/blobs(/|$)},
             %r{^internal/media(/|$)}
          ::Api::Internal::MediaApp

        when %r{^internal/repositories/\d+/git/(pushes|password_auth)(/|$)},
             %r{^internal/repositories/\d+/wiki/git/pushes(/|$)},
             %r{^internal/repositories/\d+/?$}
          ::Api::Internal::Repositories

        when %r{^internal/gists/\d+/git/pushes(/|$)},
             %r{^internal/gists/\d+/?$}
          ::Api::Internal::Gists

        when %r{^internal/aleph/}
          ::Api::Internal::Aleph

        when %r{^internal/cistern/[\w\d]+/repositories/}
          ::Api::Internal::CisternGitAccess

        when %r{^internal/actions/}
          ::Api::Internal::Actions

        when %r{^internal/assets/user(/|$)}
          ::Api::Internal::AssetsAvatars

        when %r{^internal/assets/avatars(/|$)}
          ::Api::Internal::AssetsAvatars

        when %r{^internal/assets/media(/|$)}
          ::Api::Internal::AssetsMedia

        when %r{^internal/assets/archives(/|$)}
          ::Api::Internal::Assets

        when %r{^internal/assets/quarantined_user_asset(/|$)}
          ::Api::Internal::UserAssets

        when %r{^internal/storage/user/\d+/files(/|$)},
             %r{^internal/storage/github-enterprise-assets/}
          ::Api::Internal::StorageUserFiles

        when %r{^internal/pages/auth}
          ::Api::Internal::Pages

        when %r{^internal/storage/avatars(/|$)}
          ::Api::Internal::StorageAvatars

        when %r{^internal/storage/package_versions/\d+/registry(/|$)},
             %r{^internal/storage/repository/\d+/registry/}
          ::Api::Internal::StorageRegistryFiles

        when %r{^internal/storage/github-enterprise-releases/},
             %r{^internal/storage/releases/\d+/files(/|$)}
          ::Api::Internal::StorageReleaseFiles

        when %r{^internal/storage/render(/|$)}
          ::Api::Internal::StorageRender

        when %r{^internal/storage/raw_lfs/}
          ::Api::Internal::StorageRawLfs

        when %r{^internal/storage/lfs/}
          ::Api::Internal::StorageLfs

        when %r{^internal/spokes/entities/([^/]+)/(\d+)(\.wiki)?/replicas(/|$)}
          ::Api::Internal::Replicas

        when %r{^internal/storage/repositories(/|$)}
          ::Api::Internal::StorageRepositoryFiles

        when %r{^internal/storage/upload-manifest-files(/|$)}
          ::Api::Internal::StorageUploadManifestFiles

        when %r{^internal/storage/marketplace-listing-screenshots(/|$)}
          ::Api::Internal::StorageMarketplaceListingScreenshots

        when %r{^internal/storage/marketplace-listing-images(/|$)}
          ::Api::Internal::StorageMarketplaceListingImages

        when %r{^internal/storage/repository/\d+/images(/|$)}
          ::Api::Internal::StorageRepositoryImages

        when %r{^internal/storage/migrations(/|$)}
          ::Api::Internal::StorageMigrationArchives

        when %r{^internal/storage/oauth_logos(/|$)}
          ::Api::Internal::StorageOauthApplicationLogos

        when %r{^internal/storage/businesses(/|$)}
          ::Api::Internal::StorageEnterpriseInstallationUserAccountsUploads

        when %r{^internal/registry/.*/@.+/.+(/|$)}
          ::Api::Internal::PackageRegistry

        when %r{^internal/porter/}
          ::Api::Internal::PorterCallbacks

        when %r{^internal/lfs/}
          ::Api::Internal::Lfs

        when %r{^internal/email_bounce(/|$)}
          ::Api::Internal::EmailBounce

        when %r{^internal/marketplace/traffic_analytics_ready(/|$)}
          ::Api::Internal::MarketplaceAnalytics

        when %r{^internal/twirp/}
          if GitHub.twirp_enabled?
            ::Api::Internal::Twirp
          else
            ::Api::Root
          end

        when %r{^third-party/mailchimp/webhook(/|$)}
          ::Api::ThirdParty::Mailchimp

        when %r{^lfs/}
          ::Api::Lfs

        when %r{^gists/[^/]+/comments$},
             %r{^gists/([^/]+/)?comments/[^/]+(/|$)}
          ::Api::GistComments

        when %r{^user/\d+/gists(/|$)},
             %r{^gists(/|$)}
          ::Api::Gists

        when %r{^notifications(/|$)},
             %r{^repositories/\d+/(notifications|subscription)}
          ::Api::Notifications

        when %r{^internal/repositories/\d+/package_versions/\d+/registry/.*/policies$},
             %r{^internal/repositories/\d+/registry/.*/policies$},
             %r{^internal/migrations/\d+/archive/.*/policies$}
          ::Api::Internal::MultiPartPolicies

        when %r{^repositories/\d+/releases/\d+/assets/policies$},
             %r{^migrations/\d+/archive/policies$},
             %r{^repositories/\d+/package_versions/\d+/registry/.*/policies$},
             %r{^repositories/\d+/tasks/[^/]+/runs/\d+/log/policies$},
             %r{^runs/\d+/log/policies$},
             %r{^businesses/[^/]+/user-accounts-uploads/policies$}
          ::Api::Policies

        when %r{^businesses/[^/]+/user-accounts-uploads(/|$)}
          ::Api::BusinessEnterpriseInstallationUserAccountsUploads

        when %r{^(user|organizations)/\d+/repos(/|$)},
             %r{^user/repos(/|$)}
          ::Api::Repositories

        when %r{^user/(watched|subscriptions|starred)(/|$)},
             %r{^user/\d+/(watched|subscriptions|starred)(/|$)}
          ::Api::RepositoryActivity

        when %r{^repositories/\d+/collaborators}
          ::Api::RepositoryCollaborators

        when %r{^repositories/\d+/keys}
          ::Api::RepositoryKeys

        when %r{^repositories/\d+/hooks},
             %r{^hooks(/|$)}
          ::Api::RepositoryHooks

        when %r{^repositories/\d+/dispatches}
          ::Api::RepositoryEventDispatches

        when %r{^repositories/\d+/import/issues}
          ::Api::ImportIssues
        when %r{^repositories/\d+/import}
          ::Api::Porter
        when %r{^organizations/\d+/migrations(/|$)}
          ::Api::Migrations
        when %r{^user/migrations(/|$)}
          ::Api::Migrations

        when %r{^organizations/\d+/hooks}
          ::Api::OrganizationHooks

        when %r{^organizations/\d+/pre-receive-hooks}
          ::Api::OrganizationPreReceiveHooks

        when %r{^repositories/\d+/pre-receive-hooks}
          ::Api::RepositoryPreReceiveHooks

        when %r{^internal/pre-receive-hooks/\d+/errors}
          ::Api::Internal::PreReceiveHooks

        when %r{^repositories/\d+/contents},
             %r{^repositories/\d+/readme},
             %r{^repositories/\d+/license},
             %r{^repositories/\d+/(tar|zip)ball}
          ::Api::RepositoryContents

        when %r{^internal/raw/}
          ::Api::Internal::Raw

        when %r{^internal/archive}
          ::Api::Internal::Archive

        when %r{^repositories/\d+/comments/\d+/reactions},
             %r{^reactions/\d+}
          ::Api::Reactions

        when %r{^repositories/\d+/comments},
             %r{^repositories/\d+/commits/.+/comments}
          ::Api::RepositoryCommitComments

       when %r{^repositories/\d+/commits/.+/check-runs(/$|$)},
            %r{^repositories/\d+/check-runs(/|$)},
            %r{^repositories/\d+/check-suites/\d+/check-runs(/|$)}
          ::Api::CheckRuns

       when %r{^repositories/\d+/commits/.+/check-suites(/|$)},
            %r{^repositories/\d+/check-suites(/|$)},
            %r{^repositories/\d+/check-suite-requests(/|$)}
          ::Api::CheckSuites

        when %r{^repositories/\d+/actions/resolve/(?<ref>.*)$}
          ::Api::ActionsResolve

        when %r{^repositories/\d+/actions/runs/\d+/jobs},
             %r{^repositories/\d+/actions/jobs(/|$)}
          ::Api::Jobs

        when %r{^repositories/\d+/actions/artifacts(/|$)},
             %r{^repositories/\d+/actions/runs/\d+/artifacts}
          ::Api::Artifacts

        when %r{^repositories/\d+/actions/workflows/[^/]+/runs(/|$)},
             %r{^repositories/\d+/actions/runs$},
             %r{^repositories/\d+/actions/runs/\d+(/|$)}
          ::Api::WorkflowRuns

        when %r{^repositories/\d+/actions/workflows(/|$)},
             %r{^repositories/\d+/actions/workflows/[^/]+/dispatches$}
          ::Api::Workflows

        when %r{^repositories/\d+/actions/runners(/|$)}
          ::Api::RepositoryRunners

        when %r{^organizations/\d+/actions/runners(/|$)}
          ::Api::OrganizationRunners

        when %r{^enterprises/\d+/actions/runners(/|$)}
          ::Api::EnterpriseRunners

        when %r{^organizations/\d+/actions/runner-groups(/|$)}
          ::Api::OrganizationRunnerGroups

        when %r{^enterprises/\d+/actions/runner-groups(/|$)}
          ::Api::EnterpriseRunnerGroups

        when %r{^repositories/\d+/actions/secrets(/|$)}
          ::Api::ActionsSecrets

        when %r{^organizations/\d+/actions/secrets(/|$)}
          ::Api::OrganizationSecrets

        when %r{^repositories/\d+/deployments/\d+/statuses}
          ::Api::DeploymentStatuses

        when %r{^repositories/\d+/commits}
          ::Api::RepositoryCommits

        when %r{^repositories/\d+/pulls/comments/\d+/reactions}
          ::Api::Reactions

        when %r{^repositories/\d+/pulls/comments},
             %r{^repositories/\d+/pulls/[^/]+/comments}
          ::Api::PullComments

        when %r{^repositories/\d+/pulls/[^/]+/reviews/\d+/(events|dismissals)}
          ::Api::PullRequestReviewEvents

        when %r{^repositories/\d+/pulls/[^/]+/reviews}
          ::Api::PullRequestReviews

        when %r{^repositories/\d+/pulls/[^/]+/requested_reviewers}
          ::Api::PullRequestReviewRequests

        when %r{^repositories/\d+/pulls}
          ::Api::Pulls

        when %r{^repositories/\d+/downloads}
          ::Api::RepositoryDownloads

        when %r{^repositories/\d+/deployments}
          ::Api::Deployments

        when %r{^repositories/\d+/pages}
          ::Api::RepositoryPages

        when %r{^repositories/\d+/vulnerability-alerts}
          ::Api::RepositoryVulnerabilityAlerts

        when %r{^repositories/\d+/automated-security-fixes}
          ::Api::RepositoryAutomatedSecurityFixes

        when %r{^projects/\d+/collaborators}
          ::Api::ProjectCollaborators

        when %r{^repositories/\d+/projects},
             %r{^organizations/\d+/projects},
             %r{^user/\d+/projects},
             %r{^user/projects(/|$)},
             %r{^projects/\d+},
             %r{^projects/columns/\d+},
             %r{^projects/columns/cards/\d+}
          ::Api::Projects

        when %r{^repositories/\d+/releases}
          ::Api::RepositoryReleases

        when %r{^repositories/\d+/stats(/|$)}
          ::Api::RepositoryStats

        when %r{^repositories/\d+/protected_branches}
          ::Api::RepoProtectedBranches

        when %r{^events(/|$)},
             %r{^repositories/\d+/events},
             %r{^networks/[^/]+/[^/]+/events},
             %r{^organizations/\d+/events},
             %r{^user/\d+/(received_)?events}
          ::Api::Events

        when %r{^organizations/\d+/(public_)?members(/[^/]+|$)},
             %r{^organizations/\d+/memberships(/[^/]+|$)}
          ::Api::OrganizationMembers

        when %r{^organizations/\d+/interaction-limits}
          ::Api::InteractionLimits

        when %r{^organizations/\d+/invitations}
          ::Api::OrganizationInvitations

        when %r{^organizations/\d+/outside_collaborators(/[^/]+|$)}
          ::Api::OrganizationOutsideCollaborators

        when %r{^organizations/\d+/team/\d+/discussions/\d+/reactions},
             %r{^organizations/\d+/team/\d+/discussions/\d+/comments/\d+/reactions}
          ::Api::Reactions

        when %r{^organizations/\d+/team/\d+/discussions}
          ::Api::TeamDiscussions

        when %r{^organizations/\d+/team/\d+/projects(/|$)}
          ::Api::OrganizationTeamProjects

        when %r{^organizations/\d+/team/\d+/repos},
             %r{^organizations/\d+/team/\d+/repositories/}
          ::Api::OrganizationTeamRepositories

        when %r{^organizations/\d+/team-sync/groups},
             %r{^organizations/\d+/team/\d+/team-sync/group-mappings},
             %r{^organizations/\d+/team/\d+/team-sync/group-mappings-state}
          ::Api::OrganizationTeamSync

        when %r{^organizations/\d+/team},
             %r{^organizations/\d+/team/\d+$},
             %r{^organizations/\d+/team/\d+/invitations},
             %r{^organizations/\d+/team/\d+/members},
             %r{^organizations/\d+/team/\d+/members/},
             %r{^organizations/\d+/team/\d+/memberships/},
             %r{^organizations/\d+/team/\d+/teams},
             %r{^user/teams},
             %r{^user/memberships/teams}
          ::Api::OrganizationTeams

        when %r{^pull_reminders/migration}
          ::Api::PullRemindersMigration

        when %r{^organizations/\d+/blocks},
             %r{^user/blocks}
          ::Api::UserBlocking

        when %r{^organizations/\d+/issues},
             %r{^user/issues}
          ::Api::Issues

        when %r{^organizations/\d+/installation(/|$)}
          ::Api::Integrations

        when %r{^actions/runner-registration}
          ::Api::ActionsRunnerRegistration

        when %r{^repositories/\d+/actions-runners}
          ::Api::RepositoryActionsRunners

        when %r{^org(s|anizations)?/\d+/credential-authorizations(/|$)}
          ::Api::OrganizationsCredentialAuthorizations

        when %r{^(user|organizations|enterprises)/\d+/settings/billing/}
          unless GitHub.enterprise?
            ::Api::Billing
          else
            ::Api::Root
          end

        when %r{^user/orgs(/|$)},
             %r{^user/\d+/orgs(/|$)},
             %r{^org(s|anizations)?/},
             %r{^organizations},
             %r{^user/memberships/org(s|anizations)}
          ::Api::Organizations

        when %r{^scim/v2/organizations/\d+(/|$)}
          ::Api::OrganizationsScim

        when %r{^scim/v2/enterprises/[^/]+/Users(/|$)}
          ::Api::EnterpriseUsersScim

        when %r{^scim/v2/enterprises/[^/]+/Groups(/|$)}
          ::Api::EnterpriseGroupsScim

        when %r{^interactive_components/.*?(/|$)}
          ::Api::InteractiveComponents

        when %r{^repositories/\d+/branches},
             %r{^repositories/\d+/merges}
          ::Api::RepositoryBranches

        when %r{^repositories/\d+/community/([^/]+)}
          ::Api::RepositoryCommunity

        when %r{^repositories/\d+/interaction-limits}
          ::Api::InteractionLimits

        when %r{^user/repository_invitations(/|$)}
          ::Api::RepositoryInvitations

        when %r{^repositories/\d+/issues/comments/\d+/reactions},
             %r{^repositories/\d+/issues/\d+/reactions}
          ::Api::Reactions

        when %r{^repositories/\d+/issues/([^/]+/)?comments}
          ::Api::IssueComments

        when %r{^repositories/\d+/issues/([^/]+/)?composable_comments}
          ::Api::ComposableComments

        when %r{^repositories/\d+/issues/([^/]+/)?assignees}
          ::Api::IssueAssignees

        when %r{^repositories/\d+/issues/([^/]+/)?events}
          ::Api::IssueEvents

        when %r{^repositories/\d+/issues/([^/]+/)?timeline}
          ::Api::IssueTimeline

        when %r{^repositories/\d+/issues/[^/]+/labels}
          ::Api::Labels
        when %r{^repositories/\d+/issues(/|$)},
             %r{^issues},
             %r{^repositories/\d+/assignees(/|$)}
          ::Api::Issues

        when %r{^repositories/\d+/labels(/|$)}
          ::Api::Labels

        when %r{^repositories/\d+/milestones/[^/]+/labels$}
          ::Api::Labels
        when %r{^repositories/\d+/milestones(/|$)}
          ::Api::Milestones

        when %r{^repositories/\d+/traffic(/|$)}
          ::Api::Traffic

        when %r{^repositories/\d+/git/commits(/|$)}
          ::Api::GitCommits
        when %r{^repositories/\d+/git/trees(/|$)}
          ::Api::GitTrees
        when %r{^repositories/\d+/git/blobs(/|$)}
          ::Api::GitBlobs
        when %r{^repositories/\d+/git/tags(/|$)}
          ::Api::GitTags
        when %r{^repositories/\d+/git/refs(/|$)}
          ::Api::GitRefs
        when %r{^repositories/\d+/git/(ref|matching-refs)/}
          ::Api::GitRefs
        when %r{^repositories/\d+/git}
          ::Api::Git
        when %r{^repositories/\d+/status(es)?(/|$)}
          ::Api::Statuses

        when %r{^repositories/\d+/task(s)?(/|$)},
             %r{^runs(s)?(/|$)}
          ::Api::TaskRuns
        when %r{^repositories/\d+/(watchers|stargazers|subscribers)(/|$)}
          ::Api::RepositoryActivity

        when %r{^repositories/\d+/invitations}
          ::Api::RepositoryInvitations

        when %r{^repositories/\d+/topics(/|$)}
          ::Api::Topics

        when %r{^repositories/\d+/installation}
          ::Api::Integrations

        when %r{^repositories/\d+/content_references}
          ::Api::RepositoryContentReferences
        when %r{^content_references} # Deprecated, please use the one under repos
          ::Api::RepositoryContentReferences

        when %r{^repositories/\d+/code_scanning},
             %r{^repositories/\d+/code-scanning}
          ::Api::RepositoryCodeScanning

        when %r{^repositories/\d+/secret-scanning}
          ::Api::RepositorySecretScanning

        when %r{^repositories/\d+/container-scanning}
          ::Api::RepositoryContainerScanning

        when %r{^repos}
          ::Api::Repositories

        when %r{^search/(users|repositories|code|commits|issues|topics|labels)(/|$)}
          ::Api::Search

        when %r{^graphql(/|$)}
          # Don't match /api/v3 as a path for GraphQL
          if use_path_prefix? && env[API_PATH] == "/api/v3"
            ::Api::Root
          else
            ::Api::GraphQL
          end

        when %r{^staff/stafftools/}
          ::Api::Staff::Entitlements
        when %r{^staff/.*/?coupons?(/|$)}
          ::Api::Staff::Coupons
        when %r{^staff/.*/?emails?(/|$)}
          ::Api::Staff::Emails
        when %r{^staff/repos/}
          ::Api::Staff::Repositories

        when %r{^hub/?$}
          ::Api::Hub

        when %r{^enterprise-installation}
          ::Api::EnterpriseInstallation

        when %r{^enterprise/contractors}
          if GitHub.enterprise?
            ::Api::Enterprise::Contractors
          else
            ::Api::Root
          end
        when %r{^enterprise/stats}
          if GitHub.enterprise?
            ::Api::Enterprise::Stats
          else
            ::Api::Root
          end

        when %r{^enterprise/(github|settings)}
          if GitHub.enterprise?
            ::Api::Enterprise::Settings
          else
            ::Api::Root
          end

        when %r{^enterprise/actions-token}
          if GitHub.enterprise?
            ::Api::Enterprise::Actions
          else
            ::Api::Root
          end

        when %r{^admin/ldap}
          ::Api::Admin::Ldap

        when %r{^admin/user}
          ::Api::Admin::UsersManager

        when %r{^admin/organization}
          ::Api::Admin::OrgsManager

        when %r{^admin/key}
          ::Api::Admin::Keys

        when %r{^admin/token}
          ::Api::Admin::Tokens

        when %r{^admin/pre-receive-environments}
          ::Api::Admin::PreReceiveEnvironments

        when %r{^admin/pre-receive-hooks}
          ::Api::Admin::PreReceiveHooks

        when %r{^admin/hooks}
          ::Api::Admin::Webhooks

        when %r{^authorizations(/|$)}, %r{^legacy/token}
          ::Api::Authorizations

        when %r{^installation/token},
             %r{^installation/repositories(/|$)},
             %r{^installations/\d+/repositories(/|$)},
             %r{^user/installations/\d+/repositories(/|$)}
          ::Api::IntegrationInstallations

        when %r{^installations/}
          ::Api::Integrations

        when %r{^app($)},
             %r{^app/hook/}
          ::Api::Integrations

        when %r{^(app|integration)/installations(/|$)},
             %r{^(app|integration)/global/access_tokens},
             %r{^app/installation-requests},
             %r{^user/\d+/installation(/|$)}
          ::Api::Integrations

        when %r{^app(/)}
          ::Api::GitHubApps

        when %r{^app-manifests}
          ::Api::IntegrationManifests

        when %r{^markdown}
          ::Api::Markdown

        when %r{^feeds(/|$)}
          ::Api::Feeds

        when %r{^languages(/|$)}
          ::Api::Languages

        when %r{^legacy(/|$)}
          ::Api::Legacy

        when %r{^lsp/}
          ::Api::LanguageServer

        when %r{^applications/grants(/|$)}
          ::Api::Grants

        when %r{^applications(/|$)}
          ::Api::Applications

        when %r{^gitignore(/|$)}
          ::Api::GitignoreTemplates

        when %r{^licenses(/|$)}
          ::Api::Licenses

        when %r{^codes_of_conduct(/|$)}
          ::Api::CodesOfConduct

        when %r{^meta(/|$)}
          ::Api::Meta

        when %r{^(octocat|zen)}
          ::Api::MonaLisaOctocat

        when %r{^_private/browser/}
          ::Api::BrowserReporting

        when %r{^marketplace_listing/stubbed(/|$)}
          ::Api::MarketplaceListing::Stubbed

        when %r{^marketplace_listing(/|$)}
          ::Api::MarketplaceListing

        when %r{^user/emails(/|$)},
             %r{^user/email(/|$)},
             %r{^user/public_emails(/|$)}
          ::Api::UserEmails

        when %r{^user/followers(/|$)},
             %r{^user/following(/|$)},
             %r{^user/\d+/followers(/|$)},
             %r{^user/\d+/following(/|$)}
          ::Api::UserFollowers

        when %r{^user/gpg_keys(/|$)},
             %r{^user/\d+/gpg_keys(/|$)}
          ::Api::UserGpgKeys

        when %r{^user/\d+/hovercard$}
          ::Api::UserHovercards

        when %r{^user/keys(/|$)},
             %r{^user/\d+/keys(/|$)}
          ::Api::UserPublicKeys

        when %r{^user}
          ::Api::Users

        when %r{^status}
          ::Api::Status

        when %r{^mobile/upload/policy},
             %r{^mobile/upload/assets/\d+},
             %r{^mobile/upload/repository-files/\d+}
          ::Api::Mobile::Uploads

        when %r{^vscs_internal/user/\d+/codespaces}
          ::Api::Codespaces

        when %r{^vscs_internal/proxy}
          ::Api::Codespaces

        when %r{^workspaces/.*}
          ::Api::Codespaces
        when %r{^vso_internal/.*}
          ::Api::Codespaces

        else
          ::Api::Root
      end

      # Add the API app name to env for better stats gathering
      env["process.api_app"] = app.name.demodulize
      env["process.api.controller"] = app.name
      Rack::ConditionalGet.new(app)
    end

    # Converts the PATH_INFO into a path that is routable for the API.
    #
    # env - The Hash environment of the current Rack request.
    #
    # Returns the String path without a leading slash.
    def routable_path(env)
      path = env[PATH_INFO]

      id_path = convert_actions_org_name(env, path)
      id_path = convert_natural_keys_to_ids(env, id_path)
      id_path = convert_inline_refs_to_tailing_refs(env, id_path)
      id_path = convert_ambiguous_commits_ref_to_sha(env, id_path)

      env[PATH_INFO] = id_path
      env[PATH_INFO][1..-1]
    end

    def convert_actions_org_name(env, path)
      return path unless GitHub.enterprise?
      return path unless GitHub.actions_org && GitHub.actions_org != "actions".freeze

      match = path =~ %r{^/repos/([^/]+)/([^/]+)/actions/resolve/(.*)}
      return path if match.nil?

      org = $1
      org = GitHub.actions_org if org == "actions".freeze
      nwo = "#{org}/#{$2}"

      # Given that we lose access to the original action name through rewriting
      # the path, we should add it to the query string in the env.
      env[QUERY_STRING] = "_action=#{CGI::escape("#{$1}/#{$2}")}"
      env[PATH_INFO] = "/repos/#{nwo}/actions/resolve/#{$3}"
    end

    def convert_natural_keys_to_ids(env, path)
      case path
      when %r{^/repos/([^/]+)/([^/]+)(.*)}
        convert_repository_key_to_id(env, path, $1, $2, $3, "/repositories")
      when %r{^/installations/(\d+)/repos/([^/]+)/([^/]+)(.*)}
        convert_repository_key_to_id(env, path, $2, $3, $4, "/installations/#{$1}/repositories")
      when %r{^/orgs/([^/]+)/teams/([^/]+)/repos/([^/]+)/([^/]+)(.*)}
        convert_organization_and_team_and_repos_key_to_id(env, path, $1, $2, $3, $4, $5, "/organizations", "/team", "/repositories")
      when %r{^/orgs/([^/]+)/teams/([^/]+)(.*)}
        convert_organization_and_team_key_to_id(env, path, $1, $2, $3, "/organizations", "/team")
      when %r{^/orgs/([^/]+)/team/(\d+)/repos/([^/]+)/([^/]+)(.*)}
        convert_organization_and_repos_key_to_id(env, path, $1, $2, $3, $4, $5, "/organizations", "/team", "/repositories")
      when %r{^/orgs/([^/]+)(.*)}
        convert_organization_key_to_id(env, path, $1, $2, "/organizations")
      when %r{^/organizations/(\d+)/teams/([^/]+)/repos/([^/]+)/([^/]+)(.*)}
        convert_team_and_repos_key_to_id(env, path, $1, $2, $3, $4, $5, "/organizations/#{$1}/team", "/repositories")
      when %r{^/organizations/(\d+)/teams/([^/]+)(.*)}
        convert_team_key_to_id(env, path, $1, $2, $3, "/organizations/#{$1}/team")
      when %r{^/organizations/(\d+)/team/(\d+)/repos/([^/]+)/([^/]+)(.*)}
        convert_repository_key_to_id(env, path, $3, $4, $5, "/organizations/#{$1}/team/#{$2}/repositories")
      when %r{^/enterprises/([^/]+)(.*)}
        convert_enterprise_key_to_id(env, path, $1, $2, "/enterprises")
      when %r{^/teams/(\d+)/repos/([^/]+)/([^/]+)(.*)}
        deprecated_route!(
          env,
          deprecation_date: Date.new(2020, 02, 01),
          sunset_date: Date.new(2021, 02, 01),
          info_url: "#{GitHub.developer_help_url}/changes/2020-01-21-moving-the-team-api-endpoints/",
        )
        convert_legacy_team_and_repository_key_to_id(env, path, $1, $2, $3, $4, "/organizations", "/team/#{$1}/repositories")
      when %r{^/teams/(\d+)(.*)}
        deprecated_route!(
          env,
          deprecation_date: Date.new(2020, 02, 01),
          sunset_date: Date.new(2021, 02, 01),
          info_url: "#{GitHub.developer_help_url}/changes/2020-01-21-moving-the-team-api-endpoints/",
        )
        convert_legacy_team_key_to_id(env, path, $1, $2, "/organizations", "/team")
      when %r{^/user/memberships/orgs/([^/]+)(.*)}
        convert_organization_key_to_id(env, path, $1, $2, "/user/memberships/organizations")
      when %r{^/users/([^/]+)(.*)}
        convert_user_key_to_id(env, path, $1, $2, "/user")
      when %r{^/internal/repos/([^/]+)/([^/]+)(.*)}
        convert_repository_key_to_id(env, path, $1, $2, $3, "/internal/repositories")
      when %r{^/internal/repositories/([^/]+)/([^/]+)(.*)/git/pushes\z}
        convert_repository_key_to_id(env, path, $1, $2, "#{$3}/git/pushes", "/internal/repositories")
      when %r{^/internal/repositories/([^/]+)/([^/]+)/?\z}
        convert_repository_key_to_id(env, path, $1, $2, nil, "/internal/repositories")
      when %r{^/internal/gists/([^/]+)/([^/]+)(.*)/git/pushes\z}
        convert_gist_key_to_id(env, path, $1, $2, "#{$3}/git/pushes", "/internal/gists")
      when %r{^/internal/gists/([^/]+)/([^/]+)(.*)\z}
        convert_gist_key_to_id(env, path, $1, $2, $3, "/internal/gists")
      when %r{^/internal/gists/([0-9a-f]{20,})(.*)\z}
        convert_gist_repo_name_to_id(env, path, $1, $2, "/internal/gists")
      when %r{^/internal/orgs/([^/]+)(.*)}
        convert_organization_key_to_id(env, path, $1, $2, "/internal/organizations")
      when %r{^/internal/users/([^/]+)(.*)}
        convert_user_key_to_id(env, path, $1, $2, "/internal/user")
      when %r{^/scim/v2/organizations/([^/]+)(.*)}
        convert_organization_key_to_id(env, path, $1, $2, "/scim/v2/organizations")
      when %r{^/scim/v2/enterprises/([^/]+)(.*)}
        convert_enterprise_key_to_id(env, path, $1, $2, "/scim/v2/enterprises")
      when %r{^/staff/orgs/([^/]+)(.*)}
        convert_organization_key_to_id(env, path, $1, $2, "/staff/organizations")
      when %r{^/staff/users/([^/]+)(.*)}
        convert_user_key_to_id(env, path, $1, $2, "/staff/user")
      when %r{^/admin/users/([^/]+)(.*)}
        convert_user_key_to_id(env, path, $1, $2, "/admin/user")
      when %r{^/admin/organizations/([^/]+)(.*)}
        convert_organization_key_to_id(env, path, $1, $2, "/admin/organization")
      when %r{^/admin/ldap/users/([^/]+)(.*)}
        convert_user_key_to_id(env, path, $1, $2, "/admin/ldap/user")
      when %r{^/apps/([^/]+)(.*)}
        convert_app_slug_to_id(env, path, $1, $2, "/app")
      when %r{^/vscs_internal/user/([^/]+)(.*)}
        convert_user_key_to_id(env, path, $1, $2, "/vscs_internal/user")
      when %r{^/user/([^/]+)/settings/(.*)}
        convert_user_key_to_id(env, path, $1, "/settings/#{$2}", "/user")
      when %r{^/organizations/([^/]+)/settings/(.*)}
        convert_organization_key_to_id(env, path, $1, "/settings/#{$2}", "/organizations")
      when %r{^/enterprises/([^/]+)/settings/(.*)}
        convert_enterprise_key_to_id(env, path, $1, "/settings/#{$2}", "/enterprises")
      else
        path
      end
    end

    def convert_user_key_to_id(env, path, login, extra, prefix)
      login = ::Api::LegacyEncode.decode(login)
      user = env[ThisUserKey] = begin
        ActiveRecord::Base.connected_to(role: :reading) do
          User.find_by_login(login)
        end
      end
      env[PATH_INFO] = "#{prefix}/#{user.try(:id).to_i}#{extra}"
    end

    def convert_repository_key_to_id(env, path, login, name, extra, prefix)
      env[ThisRepositoryNameWithOwnerKey] = "#{login}/#{name}"

      repo = env[ThisRepositoryKey] = begin
        ActiveRecord::Base.connected_to(role: :reading) do
          Repository.nwo(login, name)
        end
      end
      env[PATH_INFO] = "#{prefix}/#{repo.try(:id).to_i}#{extra}"
    end

    def convert_gist_key_to_id(env, path, login, name, extra, prefix)
      gist = begin
        ActiveRecord::Base.connected_to(role: :reading) do
          Gist.with_name_with_owner(login, name)
        end
      end

      env[PATH_INFO] = "#{prefix}/#{gist.try(:id).to_i}#{extra}"
    end

    def convert_gist_repo_name_to_id(env, path, name, extra, prefix)
      gist = begin
        ActiveRecord::Base.connected_to(role: :reading) do
          Gist.find_by_repo_name(name)
        end
      end

      env[PATH_INFO] = "#{prefix}/#{gist.try(:id).to_i}#{extra}"
    end

    def convert_organization_key_to_id(env, path, login, extra, prefix)
      org = env[ThisUserKey] = begin
        ActiveRecord::Base.connected_to(role: :reading) do
          Organization.find_by_login(login)
        end
      end
      env[PATH_INFO] = "#{prefix}/#{org.try(:id).to_i}#{extra}"
    end

    def convert_team_and_repos_key_to_id(env, path, org_id, slug, login, name, extra, team_prefix, repo_prefix)
      ActiveRecord::Base.connected_to(role: :reading) do
        team = env[ThisTeamKey] = Team.find_by(organization_id: org_id, slug: slug)
        repo = env[ThisRepositoryKey] = Repository.nwo(login, name)
        env[PATH_INFO] = "#{team_prefix}/#{team.try(:id).to_i}#{repo_prefix}/#{repo.try(:id).to_i}#{extra}"
      end
    end

    def convert_team_key_to_id(env, path, org_id, slug, extra, prefix)
      ActiveRecord::Base.connected_to(role: :reading) do
        team = env[ThisTeamKey] = Team.find_by(organization_id: org_id, slug: slug)
        env[PATH_INFO] = "#{prefix}/#{team.try(:id).to_i}#{extra}"
      end
    end

    def convert_organization_and_team_and_repos_key_to_id(env, path, org_login, team_slug, repo_login, name, extra, org_prefix, team_prefix, repo_prefix)
      ActiveRecord::Base.connected_to(role: :reading) do
        org = Organization.find_by_login(org_login)
        if org && team = org.teams.find_by_slug(team_slug)
          env[ThisUserKey] = org
          env[ThisTeamKey] = team
          repo = env[ThisRepositoryKey] = Repository.nwo(repo_login, name)
        end
        env[PATH_INFO] = "#{org_prefix}/#{org.try(:id).to_i}#{team_prefix}/#{team.try(:id).to_i}#{repo_prefix}/#{repo.try(:id).to_i}#{extra}"
      end
    end

    def convert_organization_and_repos_key_to_id(env, path, org_login, team_id, repo_login, name, extra, org_prefix, team_prefix, repo_prefix)
      ActiveRecord::Base.connected_to(role: :reading) do
        org = Organization.find_by_login(org_login)
        if org && team = org.teams.find_by_id(team_id)
          env[ThisUserKey] = org
          env[ThisTeamKey] = team
          repo = env[ThisRepositoryKey] = Repository.nwo(repo_login, name)
        end
        env[PATH_INFO] = "#{org_prefix}/#{org.try(:id).to_i}#{team_prefix}/#{team.try(:id).to_i}#{repo_prefix}/#{repo.try(:id).to_i}#{extra}"
      end
    end

    def convert_organization_and_team_key_to_id(env, path, login, slug, extra, org_prefix, team_prefix)
      ActiveRecord::Base.connected_to(role: :reading) do
        org = Organization.find_by_login(login)
        if org && team = org.teams.find_by_slug(slug)
          env[ThisUserKey] = org
          env[ThisTeamKey] = team
        end
        env[PATH_INFO] = "#{org_prefix}/#{org.try(:id).to_i}#{team_prefix}/#{team.try(:id).to_i}#{extra}"
      end
    end

    def convert_legacy_team_key_to_id(env, path, team_id, extra, org_prefix, team_prefix)
      ActiveRecord::Base.connected_to(role: :reading) do
        team = env[ThisTeamKey] = Team.find_by_id(team_id)
        env[PATH_INFO] = "#{org_prefix}/#{team.try(:organization_id).to_i}#{team_prefix}/#{team.try(:id).to_i}#{extra}"
      end
    end

    def convert_legacy_team_and_repository_key_to_id(env, path, team_id, login, name, extra, org_prefix, prefix)
      env[ThisRepositoryNameWithOwnerKey] = "#{login}/#{name}"
      ActiveRecord::Base.connected_to(role: :reading) do
        team = env[ThisTeamKey] = Team.find_by_id(team_id)
        repo = env[ThisRepositoryKey] = Repository.nwo(login, name)
        env[PATH_INFO] = "#{org_prefix}/#{team.try(:organization_id).to_i}#{prefix}/#{repo.try(:id).to_i}#{extra}"
      end
    end

    def convert_enterprise_key_to_id(env, path, slug_or_id, extra, prefix)
      enterprise = env[ThisUserKey] = begin
        ActiveRecord::Base.connected_to(role: :reading) do
          Business.find_by(slug: slug_or_id) || Business.find_by(id: slug_or_id)
        end
      end
      env[PATH_INFO] = "#{prefix}/#{enterprise.try(:id).to_i}#{extra}"
    end

    def convert_app_slug_to_id(env, path, slug, extra, prefix)
      app = env[ThisAppKey] = begin
        ActiveRecord::Base.connected_to(role: :reading) do
          Integration.find_by_slug(slug)
        end
      end
      env[PATH_INFO] = "#{prefix}/#{app.try(:id).to_i}#{extra}"
    end

    # Convert a path like /repositories/123/commits/branch/named/status to
    # /repositories/123/status/branch/named, unless repository 123 has
    # a ref named /branch/named/status.
    def convert_inline_refs_to_tailing_refs(env, path)
      return path unless match = TailingStatusRegex.match(path)
      potential_ref = "#{match[:ref]}/#{match[:status]}"

      repo = env[ThisRepositoryKey] ||= begin
        ActiveRecord::Base.connected_to(role: :reading) do
          Repository.find_by_id match[:repo_id]
        end
      end

      if repo && repo.heads[potential_ref].nil?
        "/repositories/#{match[:repo_id]}/#{match[:status]}/#{match[:ref]}"
      else
        path
      end
    end

    # Several API endpoints accept a `:ref`, which can be a SHA, branch or tag.
    # Branch names can contain `/`, which means that we can end up with ambiguity
    # when a branch name might conflict with one of our API resources.
    #
    # E.g.:
    #
    # GET /repos/:owner/:repo/commits/:ref (Get the SHA-1 of a commit reference)
    # GET /repos/:owner/:repo/commits/:ref/check-suites (List the check suites for a commit reference)
    #
    # Given a branch named, for example, wip/check-suites, the "get a SHA-1" endpoint would be:
    #
    # GET /repos/owner/repo/commits/wip/check-suites
    #
    # The router can not know which of these is meant by just looking at the route pattern.
    #
    # This looks up the ambiguous ref to see if there's an existing branch with that name.
    # If there is, we disambiguate by swapping out the branch name with the SHA, which lets
    # the router unambiguously determine that the request is to the RepositoryCommits API rather
    # than the other API.
    #
    # This is used by the CheckSuites API and the CheckRuns API.
    # and should also refactor the Statuses API to use it.
    def convert_ambiguous_commits_ref_to_sha(env, path)
      return path unless match = AmbiguousCommitsAPIRefRegex.match(path)
      potential_ref = "#{match[:ref]}/#{match[:resource]}"

      repo = env[ThisRepositoryKey] ||= begin
        ActiveRecord::Base.connected_to(role: :reading) do
          Repository.find_by_id match[:repo_id]
        end
      end

      if repo && repo.heads[potential_ref].present?
        sha = repo.heads[potential_ref].sha
        "/repositories/#{match[:repo_id]}/commits/#{sha}"
      elsif repo && repo.heads[match[:ref]].present?
        sha = repo.heads[match[:ref]].sha
        "/repositories/#{match[:repo_id]}/commits/#{sha}/#{match[:resource]}"
      else
        path
      end
    end

    # Marks the current route as deprecated before routing it
    # To a Sinatra App. This enables us to conditionaly deprecate
    # a resource depending on the path used by the client.
    #
    # deprecation_date - The date at which this deprecation started.
    # sunset_date      - The date at which the endpoint may stop responding.
    # info_url         - An absolute URL to an HTML,
    #                    human readable description of the deprecation
    #
    def deprecated_route!(env, deprecation_date:, sunset_date:, info_url:)
      env[DEPRECATED_ROUTE] = {
        deprecation_date: deprecation_date,
        sunset_date: sunset_date,
        info_url: info_url,
      }
    end
  end
end
