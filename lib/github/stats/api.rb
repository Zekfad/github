# frozen_string_literal: true

module GitHub
  module Stats
    module Api
      extend self

      # This allowlist is for routes that want to opt-in to metrics per route.
      # Feel free to add your route here temporarily or permenantly if this is
      # useful, but please don't add anything you don't need since these
      # metrics can be expensive for large numbers of routes.
      ROUTE_KEY_ALLOWLIST = Set[
        # api_root is only allowed for testing purposes.
        "api_root",
        "issues",
        "organizations_organization_id_issues",
        "repositories_repository_id_pulls",
        "user_issues",
        "user_orgs",
        "user_repos",
      ].freeze

      # Public: Throws Api stats at statsd.
      #
      # options - The Hash containing stats to be recorded (default: {}).
      #           :version          - Version of the API (e.g., "beta", "v3", "superman-preview").
      #           :auth             - Type of authentication [anon, basic, token, oauth, apps].
      #           :call             - The identifier/route of this API call, (e.g. "/gists/:id/star')
      #           :method           - The HTTP method (e.g., "GET", "POST", etc)
      #           :elapsed          - The elapsed response time in seconds
      #           :query_time       - The total database time for the request in seconds
      #           :transaction_time - The time elapsed for every database transaction
      #           :status           - HTTP Response code
      #
      # Returns nothing.
      def record(options)
        options[:status] ||= 0

        GitHub.dogstats.increment("api.version.#{options[:version]}")
        GitHub.dogstats.increment("api.auth.#{options[:auth]}")

        GitHub.dogstats.increment("api.status.#{options[:status]}") unless options[:status].zero?

        # Turn route separators and punctuators into `.`, then remove a trailing dot, eg
        # "/repositories/:id/contents/*?" => "api.routes.repositories_id_contents"
        route_key = (options[:call] || "unknown_route").gsub(/\W+/, "_")
        if route_key == "_"
          route_key = "api_root"
        end
        head_idx = route_key.start_with?("_") ? 1 : 0
        tail_idx = route_key.end_with?("_") ? -2 : -1
        route_key = route_key[head_idx..tail_idx]

        tags = ["route_key:#{route_key}", "method:#{options[:method]}", "auth:#{options[:auth]}"]
        unless options[:status].zero?
          tags << "status_range:#{status_range(options[:status])}"
        end

        # api.routes.route.response_time
        if elapsed = options[:elapsed]
          if ROUTE_KEY_ALLOWLIST.include?(route_key)
            GitHub.dogstats.timing("api.routes.response_time", elapsed, tags: tags)
          end
        end

        # api.routes.route.mysql_time
        if query_time = options[:query_time]
          if ROUTE_KEY_ALLOWLIST.include?(route_key)
            GitHub.dogstats.timing("api.routes.mysql_time", query_time, tags: tags)
          end
        end
      end

      private

      # Internal: Given an integer status code, returns a string representing
      # the range that status falls in. E.g., 404 would have a range of 4xx.
      #
      # Returns a string representing the status range or status (if non-integer).
      def status_range(status)
        if status.is_a?(Integer)
          "#{status.to_s[0]}xx"
        else
          "invalid"
        end
      end

    end
  end
end
