# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Pages
    class Theme
      attr_reader :slug, :name

      # Retrieve a theme by its slug
      #
      # slug - the slug of the desired theme
      #
      # Returns the desired theme, or nil if no theme has the specified slug.
      def self.find(slug)
        all.detect { |t| t.slug == slug }
      end

      # Returns all the themes.
      def self.all
        if @all.nil?
          config_file = File.join(Rails.root.to_s, "/config/page_themes.yml")
          @all = YAML.load_file(config_file).map { |info| new info }.compact
        end

        @all
      end

      def initialize(options)
        @slug   = options["slug"]
        @name   = options["name"]
      end

      # Retrieve the paths of all the theme's assets.
      #
      # purpose - what the paths are for. :web or :filesystem
      #
      # Returns the paths for the specified purpose
      def all_asset_paths(purpose = nil)
        [
          javascript_paths(purpose),
          stylesheet_paths(purpose),
          font_paths(purpose),
          image_paths(purpose),
        ].flatten
      end

      # Retrieve the paths of the theme's Javascripts.
      #
      # purpose - what the paths are for. :web or :filesystem
      #
      # Returns the paths for the specified purpose
      def javascript_paths(purpose = nil)
        asset_paths "javascripts", /\.js$/i, purpose
      end

      # Retrieve the paths of the theme's fonts.
      #
      # purpose - what the paths are for. :web or :filesystem
      #
      # Returns the paths for the specified purpose
      def font_paths(purpose = nil)
        asset_paths "fonts", /\.(eot|otf|svg|ttf|woff)$/i, purpose
      end

      # Retrieve the paths of the theme's stylesheets.
      #
      # purpose - what the paths are for. :web or :filesystem
      #
      # Returns the paths for the specified purpose
      def stylesheet_paths(purpose = nil)
        asset_paths "stylesheets", /\.css$/i, purpose
      end

      # Retrieve the paths of the theme's images
      #
      # purpose - what the paths are for. :web or :filesystem
      #
      # Returns the paths for the specified purpose
      def image_paths(purpose = nil)
        asset_paths "images", /\.(gif|jpg|png)$/i, purpose
      end

      # Retrieve the path of the theme's thumbnail
      #
      # purpose - what the path is for. :web or :filesystem
      #
      # Returns the path for the specified purpose
      def thumbnail_path(purpose = nil)
        File.join base_path_components(purpose) + ["thumbnail.png"]
      end

      # Find all the paths for assets matching the params.
      #
      # subdir_name_or_components - subdirectory (string or array) where assets are
      # filter - regex that any matching file should pass
      # purpose - what the paths are for. :web or :filesystem
      def asset_paths(subdir_name_or_components, filter, purpose)
        purpose ||= :web
        subdir = [subdir_name_or_components].flatten
        base_path = File.join(base_path_components(:filesystem) + subdir)

        if File.directory? base_path
          names = Dir.entries(base_path).select { |f| f =~ filter }
        else
          names = []
        end

        names.map { |n| File.join base_path_components(purpose) + subdir + [n] }
      end

      # Retrieve the components for the theme's base path.
      #
      # purpose - what the base path is for. :web or :filesystem
      #
      # Returns an array of path components for the specified purpose.
      def base_path_components(purpose = :web)
        roots = {
          filesystem: "public",
          web: "/",
        }

        [roots[purpose], "themes", slug]
      end

      # Retrieve the relative portion of the specified absolute path.
      #
      # path - absolute path
      #
      # Returns a relative path
      def relative_path_for(path)
        split_path = File.split(path)
        components = []

        until split_path[1] == slug
          components = [split_path[1]] + components
          split_path = File.split(split_path[0])
        end

        File.join components
      end
    end
  end
end
