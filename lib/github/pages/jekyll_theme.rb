# rubocop:disable Style/FrozenStringLiteralComment

# jekyll_theme.rb
# Replacement for APG Pages::Theme
#
# TODO: remove unused crud
#
# Reads /config/pages_jekyll_themes.yml to populate new Jekyll theme chooser
module GitHub
  module Pages
    class JekyllTheme
      attr_reader :slug, :name, :url, :gem, :apg, :apg_dirs, :beta

      # Retrieve a theme by its slug
      #
      # slug - the slug of the desired theme
      #
      # Returns the desired theme, or nil if no theme has the specified slug.
      def self.find(slug)
        all.detect { |theme| theme.slug == slug }
      end

      # Retrieve a theme by its gem
      #
      # gem - the gem of the desired theme
      #
      # Returns the desired theme, or nil if no theme has the specified gem.
      def self.find_gem(gem_name)
        all.detect { |theme| theme.gem == gem_name }
      end

      # Retrieve a theme by matching fragments of html against index.html
      # NOTE: this is only reliable if index.html was generated via APG
      #
      # html - String index.html
      #
      # Returns the first matching JekyllTheme, or nil if no theme matches.
      def self.apg_match(html)
        all.detect { |t| t.apg.present? && html.include?(t.apg) }
      end

      # Returns all the themes.
      def self.all
        if @all.nil?
          json = IO.read File.join(Rails.root.to_s, "/config/pages_jekyll_themes.json")
          data = GitHub::JSON.parse json
          @all = data.map { |info| new info }.compact
        end

        @all
      end

      def initialize(options)
        @slug   = options["slug"]
        @name   = options["name"]
        @url    = "//#{GitHub.pages_themes_hostname}#{options["url"]}"
        @gem    = options["gem"]
        @thumb  = options["thumb"]
        @apg    = options["apg"]
        @apg_dirs  = options["apg_dirs"]
        @beta   = !!options["beta"]
      end

      # Retrieve the web path of the theme's thumbnail
      def thumbnail_path
        return @thumb unless @thumb.blank?
        File.join @url, "thumbnail.png"
      end
    end
  end
end
