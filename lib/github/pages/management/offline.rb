# frozen_string_literal: true

require "logger"
require "github/sql"

module GitHub::Pages::Management
  class Offline


    attr_reader :host, :delegate

    def initialize(host:, delegate:)
      @host = host
      @delegate = delegate
      nil
    end

    def perform
      sql = ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, host: host)
        UPDATE pages_fileservers SET online = 0 WHERE host = :host
      SQL

      if sql.affected_rows == 0
        delegate.log "No such host=#{host}"
        false
      else
        delegate.log "Offlined host=#{host}"
        true
      end
    end
  end
end
