# rubocop:disable Style/FrozenStringLiteralComment

require "posix/spawn"

module GitHub
  module Pages
    class Builder
      class Error < StandardError; end # Generic build error
      class UserError < StandardError; end # Detected, user-caused build error
      class BuildTimeout < StandardError; end # The build timed-out
      class SyncTimeout < StandardError; end # The sync timed-out
      class AuthError < StandardError; end # The build failed to authenticate
      class MissingBuildEnvironment < StandardError; end
      class MissingBuildRevisionError < StandardError; end

      class IncompatibleUserTypeError < UserError; end # This build was started by a bot or org

      class NoInstallationError < StandardError; end
      class GitHubAppError < StandardError; end

      PASSTHROUGH_ERRORS_TO_USER = [Error, UserError, BuildTimeout, SyncTimeout].freeze
      CHECK_FAILURE_TITLE = "GitHub Pages failed to build your site."
      BUILD_TIMEOUT_ERROR = "The GitHub Pages build timed out. Please try again later."

      USERNAME = "git".freeze
      AUTH_ERROR_MESSAGE = "Invalid username or password.".freeze
      BUILD_TRIES = 5 # Maximum number of times we try to clone a Pages branch
      BUILD_SLEEP_TIME = 2 # Number of seconds to wait for DB replicas to catch up to the primary
      COMMAND_TRIES = 5 # Maximum number of times we retry any command
      COMMAND_RETRIES_SLEEP = 2 # Number of seconds to wait before retrying any command.

      def self.areas_of_responsibility
        [:pages]
      end

      # Public: Initialize a new Pages Builder.
      #
      # page         - a Page instance to build
      # pusher       - a User instance who initiated the build
      # build_dir    - a String pathname to a build directory
      # git_ref_name - (named, optional) a String git reference name (e.g. a branch name)
      #                that the builder should check out before executing the compilation process.
      #                If it is not specified, the Builder uses the Repository pages_branch.
      #                Note that this should be an unqualified branch name, e.g. 'gh-pages'.
      #                It should *not* contain 'refs/heads/' or anything similar.
      def initialize(page, pusher, build_dir, git_ref_name: nil)
        @page = page
        @pusher = pusher
        @build_dir = build_dir
        @git_ref_name = git_ref_name || page.source_branch
        @oauth_token = nil
      end

      def should_use_apps?
        return @should_use_apps if defined?(@should_use_apps)

        @should_use_apps = (pages_github_app_enabled?(@page.repository) && pages_github_app_token.present?)
      end

      def feature_enabled?(feature, repository)
        return false if GitHub.enterprise?
        return false if repository.nil?

        @feature_enabled = {}
        key = "#{repository.id}_#{feature}"

        return @feature_enabled[key] if @feature_enabled.key?(key)

        @feature_enabled[key] = GitHub.flipper[feature].enabled?(repository) ||
          GitHub.flipper[feature].enabled?(repository.owner)
      end

      def pages_statuses_enabled?(repository)
        !GitHub.enterprise? && repository.present?
      end

      def pages_features_checks_api_enabled?(repository)
        feature_enabled?(:pages_features_checks_api, repository)
      end

      def pages_github_app_enabled?(repository)
        feature_enabled?(:pages_github_app, repository)
      end

      def pages_beta_image?(repository)
        feature_enabled?(:pages_beta_image, repository) &&
          !repository.nwo.eql?("github/pages.github.com")
      end

      def pages_beta_replication_strategy?(repository)
        feature_enabled?(:pages_beta_replication_strategy, repository) &&
          !repository.nwo.eql?("github/pages.github.com")
      end

      def replication_strategy
        return @replication_strategy if defined?(@replication_strategy)

        # Currently disabled in Enterprise
        return (@replication_strategy = nil) if GitHub.enterprise?

        @replication_strategy = if pages_beta_replication_strategy?(@page.repository)
          GitHub.pages_beta_replication_strategy
        else
          GitHub.pages_replication_strategy
        end
      end

      def write_non_voting_replicas?
        return @write_non_voting_replicas if defined?(@write_non_voting_replicas)

        @write_non_voting_replicas = GitHub.pages_non_voting_replica_count.to_i > 0
      end

      def validate_can_issue_oauth_access!
        return nil if @pusher.present? && @pusher.user?
        raise IncompatibleUserTypeError, "GitHub Pages builds cannot be executed by #{@pusher.class} accounts."
      end

      def pages_oauth_access
        validate_can_issue_oauth_access!
        @pages_oauth_access ||=
          begin
            oauth_access = @pusher.oauth_accesses.find_by_application_id(GitHub.pages_app_id)

            if oauth_access.nil?
              oauth_access = @pusher.oauth_accesses.new(
                application_id: GitHub.pages_app_id,
                application_type: "OauthApplication",
                scopes: ["repo"],
              )
            end

            oauth_access
          end
      end

      def pages_github_app_installation
        return nil unless GitHub.pages_github_app.present?
        @installation ||= IntegrationInstallation
          .with_repository(@page.repository)
          .where(integration_id: GitHub.pages_github_app.id)
          .first
      end

      def pages_github_app_token
        return nil unless pages_github_app_installation.present?
        @app_token ||=
          begin
            result = ScopedIntegrationInstallation::Creator
              .perform(pages_github_app_installation, repositories: [@page.repository])

            if result.failed?
              report_error_to_haystack(
                ScopedIntegrationInstallation::Creator::Result::Error.new(
                  result.error,
                ),
              )
              return nil
            end

            _, token = AuthenticationToken.create_for(result.installation)
            token
          end
      end

      def oauth_token
        return nil if should_use_apps?
        @oauth_token ||=
          begin
            # Older tokens will have a code set that is not needed.
            pages_oauth_access.code = nil
            pages_oauth_access.reset_token
          end
      end

      def sanitize_command(command)
        return if command.empty?
        command.map do |part|
          filter_tokens(part)
        end
      end

      # Generate the oauth token, then wait for replication of the token across DB read replicas.
      # We have a hard requirement on a low replication lag, since we regenerate a new token
      # for every page build. This method gives us a little breathing room when replication delay
      # spikes on our DB cluster.
      def replicate_oauth_token!(max_wait_seconds: 255)
        return 0 unless Rails.production? || Rails.test? || !should_use_apps?

        _ = oauth_token # This resets the token then stores in an instance variable for later.
        waiter = WaitForReplication.new(Timestamp.from_time(Time.now), max_wait_seconds: max_wait_seconds)

        begin
          waiter.wait!
          log(fn: "replicate_oauth_token!", waited: waiter.waited)
        rescue WaitForReplication::DataUnavailable => e
          log(fn: "replicate_oauth_token!", waited: waiter.waited, error_message: e.message)
        end

        waiter.waited
      end

      def report_build_token
        tag = begin
          if should_use_apps?
            "github-app"
          elsif oauth_token.present?
            "oauth"
          else
            raise GitHubAppError, "No GitHub app or OAuth token available."
          end
        end
        GitHub.dogstats.increment("pages.build_token", tags: ["token:#{tag}"])
      end

      def report_github_app_bad_state
        if pages_github_app_enabled?(@page.repository)
          if pages_github_app_installation.blank?
            raise NoInstallationError, "The Pages GitHub app is not installed."
          elsif pages_github_app_token.blank?
            raise GitHubAppError, "Pages GitHub app is installed, but the OAuth token will be used."
          end
        end
      rescue NoInstallationError, GitHubAppError => error
        report_error_to_haystack(error)
      end

      # Creates the remote clone url with a GitHub or OAuth app token.
      #
      # Side effect – reports to datadog which token is used.
      # Side effect – reports to haystack if the Pages GitHub app is installed, but an OAuth app is used.
      # Returns – the remote clone url as a String.
      def clone_url_with_token
        scheme = (GitHub.ssl? ? "https" : "http")
        credentials = should_use_apps? ? "x-access-token:#{pages_github_app_token}" : "#{@pusher.login}:#{oauth_token}"
        path = "#{GitHub.host_name}/#{@page.repository.name_with_owner}.git"
        report_build_token
        report_github_app_bad_state
        "#{scheme}://#{credentials}@#{path}"
      end

      def clone_url
        if Rails.production?
          clone_url_with_token
        else
          repository.internal_remote_url
        end
      end

      def built_revision
        return nil unless File.exist?("#{build_path}/pages-build-version")
        File.read("#{build_path}/pages-build-version").strip
      end

      def call
        start_time = Time.now

        raise MissingBuildEnvironment, "build environment is not available" unless build_environment_available? && build_compatible?

        Failbot.push(
          pages_build_user_path: build_user_path,
          pages_build_path: build_path,
          pages_deployment_id: page_deployment.id,
        )

        repository = @page.repository
        @build = Page::Build.track(@page, pusher: @pusher, page_deployment_id: page_deployment.id)
        Failbot.push page_build_id: page_build_id

        begin
          build_process = page_build
        rescue POSIX::Spawn::TimeoutExceeded, POSIX::Spawn::Child::TimeoutExceeded => e
          report_failed_build_to_user
          raise BuildTimeout, "Page build timed out. Please try again later."
        end

        commit = built_revision
        @build.update(commit: commit)
        Failbot.push(pages_version: commit)
        if commit.to_s.empty?
          raise MissingBuildRevisionError, "could not fetch commit from built site"
        end

        begin
          log_build_step("dpages-sync") { dpages_sync(revision: commit) }
        rescue POSIX::Spawn::TimeoutExceeded, POSIX::Spawn::Child::TimeoutExceeded => e
          report_failed_build_to_user
          raise SyncTimeout, "Page build timed out. Please try again later."
        end

        @build.complete!((Time.now - start_time) * 1_000)

        # Warn-level build feedback
        # CNAME warnings which may result in the site being built someplace other
        # than expected should take preference over good-to-know warnings like Maruku
        if warning_message = @page.cname_error || warning(build_process.err)
          GitHub.dogstats.increment("pages.warnings")
          PagesMailer.build_warning(@pusher, repository, warning_message, @build).deliver_now
        end

        @build
      rescue Object => err
        duration = (Time.now - start_time) * 1_000
        report_error_to_user(err, duration)
        report_error_to_haystack(err, duration: duration)
        @build
      ensure
        if production_environment?
          cleanup_script = ["rm -rf #{build_path}"]
          if GitHub.enterprise?
            if File.exist?("/usr/local/share/enterprise/ghe-pages-wrapper")
              cleanup_script.unshift("sudo /usr/local/share/enterprise/ghe-pages-wrapper")
            end
          else
            cleanup_script.unshift("sudo -u jekyll")
          end
          run([cleanup_script.join(" ")], "failed to cleanup")
        else
          FileUtils.rm_rf build_path
        end
        elapsed = (Time.now - start_time) * 1_000
        GitHub.dogstats.timing("pages.build", elapsed, tags: ["status:#{@build&.status || "unknown"}"])
        GitHub::Logger.log(base_log_data.merge(fn: "page-build", elapsed: elapsed).merge(step_times))
      end

      def report_error_to_user(err, duration)
        unless @build
          GitHub.dogstats.increment("pages.builds", tags: ["status:error"])
          return
        end

        # We want to be careful about what messages are sent back to the user.
        # Here, we send an error back if it is a UserError (already sanitized)
        # or an Error (a generic error that has a hand-crafted error message from us).
        return @build.error!(err, duration) if PASSTHROUGH_ERRORS_TO_USER.include?(err.class)

        error = Error.new("Page build failed.")
        error.set_backtrace(err.backtrace)
        @build.error!(error, duration)
      end

      def report_error_to_haystack(err, options = {})
        app = err.is_a?(UserError) ? "pages-user" : "pages"
        Failbot.report(err, options.merge(app: app))
      end

      def dpages_sync(revision:)
        hosts = Allocator.hosts_for_new_replicas(
          replication_strategy: replication_strategy,
          voting: GitHub.pages_replica_count,
          non_voting: write_non_voting_replicas? ? GitHub.pages_non_voting_replica_count : 0,
        )

        deployment = @page.create_or_find_deployment_for(@git_ref_name)

        successful_hosts = sync_to_replicas(hosts,
          GitHub::Routing.dpages_storage_path(@page.id, revision: revision))

        self.class.update_deployment_replicas(deployment, successful_hosts, revision)

        @page.reload
      end

      def self.current_replicas(page_id)
        ApplicationRecord::Domain::Repositories.github_sql.results(<<-SQL, page_id: page_id)
          SELECT id, page_id, host FROM pages_replicas WHERE page_id = :page_id
        SQL
      end

      def self.current_revision(page_id)
        ApplicationRecord::Domain::Repositories.github_sql.value(<<-SQL, page_id: page_id)
          SELECT built_revision FROM pages WHERE id = :page_id
        SQL
      end

      # Atomic update of revision and replica hosts for a deployment
      # For now, support both the old schema, with built_revision stored on pages table,
      # and the new page_deployments table with a ref_name and revision for branch builds.
      def self.update_deployment_replicas(deployment, hosts, revision)
        # run all of these queries in a transaction so we don't leave pages in a weird state in case of an error
        GitHub::SchemaDomain.allowing_cross_domain_transactions do
          ApplicationRecord::Domain::Repositories.github_sql.transaction do
            unless deployment.branch_build?
              ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, revision: revision, page_id: deployment.page_id)
                UPDATE pages SET built_revision = :revision WHERE id = :page_id
              SQL
            end

            deployment.update({ revision: revision })

            # pages_replicas may have a NULL pages_deployment_id for the main (not branch) build
            if deployment.branch_build?
              ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, page_id: deployment.page_id, deployment_id: deployment.id)
                DELETE FROM pages_replicas WHERE page_id = :page_id
                AND pages_deployment_id = :deployment_id
              SQL
            else
              ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, page_id: deployment.page_id, deployment_id: deployment.id)
                DELETE FROM pages_replicas WHERE page_id = :page_id
                AND (pages_deployment_id IS NULL OR pages_deployment_id = :deployment_id)
              SQL
            end

            rows = GitHub::SQL::ROWS(
              hosts.map { |host|
                [deployment.page_id, deployment.id, host, GitHub::SQL::NOW, GitHub::SQL::NOW]
              },
            )

            ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, rows: rows)
              INSERT INTO pages_replicas (page_id, pages_deployment_id, host, created_at, updated_at)
              VALUES :rows
            SQL
          end
        end
      end

      # TODO - GHE
      # This method was restored from
      # https://github.com/github/github/blob/1c40048624ded6e0184e7a1d4615908d87e9a944/lib/github/pages/builder.rb
      # GitHub::Pages::Builder.update_replicas is called by script/dpages-cluster-import-finalize
      # and used by share/github-backup-utils/ghe-restore-pages-dpages in github/backup-utils.
      # Please fix those to call update_deployment_replicas() instead of update_replicas()
      def self.update_replicas(page_id, replicas, revision)
        ApplicationRecord::Domain::Repositories.github_sql.transaction do
          ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, revision: revision, page_id: page_id)
            UPDATE pages SET built_revision = :revision WHERE id = :page_id
          SQL

          ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, page_id: page_id)
            DELETE FROM pages_replicas WHERE page_id = :page_id
          SQL

          rows = GitHub::SQL::ROWS(
            replicas.map { |replica|
              [page_id, replica, GitHub::SQL::NOW, GitHub::SQL::NOW]
            },
          )

          ApplicationRecord::Domain::Repositories.github_sql.run(<<-SQL, rows: rows)
            INSERT INTO pages_replicas (page_id, host, created_at, updated_at)
            VALUES :rows
          SQL
        end
      end

      # Given two lists of voting and non-voting hosts, attempt to sync the
      # compiled site to the hosts. As each page-build runs, keep a list of
      # those for which it completed successfully. This list is returned
      # for use in updating the database. If a non-voting replica fails to
      # sync, it will simply be pruned from the resulting list and no
      # record of it will be made in the database. A subsequent repair job
      # will ensure it exists.
      #
      # hosts - a Hash containing two keys: :voting and :non_voting, each
      # pointing to a separate array of hosts upon which to create
      # replicas.
      # path - path on disk to which the replica should be synced.
      #
      # Returns a list of hostnames which successfully received replicas.
      def sync_to_replicas(hosts, path)
        system_error_msg = "Unable to sync pages directory."
        sync_proc = proc do |host|
          destination = replica_rsync_destination(host, path)

          run(
            ["#{pages_jekyll_script_dir}/page-sync", build_path, destination],
            system_error_msg,
            "#{system_error_msg} Please try again later.",
            env: sync_execution_environment,
            input: "",
            timeout: 10.minutes.to_i,
          )

          GitHub.dogstats.increment("pages.page_sync", tags: ["destination:#{host}", "status:success"])

          true
        end

        successful_hosts = Array(hosts[:voting]).flatten.select { |host| sync_proc.call(host) }
        if write_non_voting_replicas?
          successful_hosts += Array(hosts[:non_voting]).flatten.select do |host|
            begin
              sync_proc.call(host)
            rescue Error => e
              GitHub.dogstats.increment("pages.page_sync", tags: ["destination:#{host}", "status:error"])
              raise e unless e.to_s.start_with?(system_error_msg)
              # If the sync to a non-voting host fails, do not fail the
              # build. A subsequent run of the repair management operation
              # will ensure a non-voting replica exists.
              Failbot.report(e, app: :pages)
              false
            end
          end
        end

        successful_hosts
      end

      def replica_rsync_destination(host, path)
        if host == GitHub.local_pages_host_name
          path
        else
          "#{host}:#{path}"
        end
      end

      # The path to the scripts in the pages source
      def pages_jekyll_script_dir
        @pages_jekyll_script_dir ||=
          if File.directory?(File.join(GitHub.pages_jekyll_dir, "script"))
            File.join(GitHub.pages_jekyll_dir, "script")
          else
            GitHub.pages_jekyll_dir
          end
      end

      # Determine whether the pages build environment is available.
      #
      # Returns true if the build commands are available, false otherwise.
      # Side-effect: Populates @page_build_usage and @page_build_params
      #              if required by the page to check if build_compatible.
      def build_environment_available?
        return @build_environment_available if defined?(@build_environment_available)
        dir = pages_jekyll_script_dir
        @build_environment_available ||= (
          File.executable?("#{dir}/page-build") &&
          File.executable?("#{dir}/page-sync") &&
          (!@page.subdir_source? ||
            ((@page_build_usage = IO.readlines("#{dir}/page-build")[1]) &&
            (@page_build_params = @page_build_usage.count("<"))))
        )
      end

      # Determine if the deployed page-build is compatible with this page
      # page.source requires extra parameter introduced in github/pages/pull/922
      def build_compatible?
        !@page.subdir_source? || @page_build_params > 3
      end

      # Determine if this is a github.com production environment. This determines
      # whether command isolation, librato config, and remote sync'ing needs to
      # happen.
      def production_environment?
        Rails.production? || Rails.staging?
      end

      # The path to build the pages site at on the current machine. Files in this
      # directory are output from jekyll.
      def build_path
        @build_path ||= "#{build_user_path}/#{repository}"
      end

      # List of non-underscored top level directories under the build root.
      # Since these dirs explicitly appear, they are allowed to override
      # gh-pages subdirs.
      def build_path_directories
        Array(Dir["#{build_path}/*"]).
          map     { |f| File.basename(f) }.
          reject  { |f| f[0] == "_" || !File.directory?("#{build_path}/#{f}") }
      end

      # The process environment used to executed page-build command.
      def build_execution_environment
        basic_execution_environment.merge(command_execution_environment)
      end

      # Ditto for page-sync command.
      def sync_execution_environment
        environment = build_execution_environment
        environment.delete("RSYNC")
        environment
      end

      # Environment variables used for all page-* commands executed to build the
      # pages site.
      def basic_execution_environment
        {
          "GEM_HOME"          => nil,
          "GEM_PATH"          => nil,
          "RUBYLIB"           => nil,
          "TMPDIR"            => @build_dir,
          "BUNDLE_GEMFILE"    => nil,
          "BUNDLE_APP_CONFIG" => nil,
          "RBENV_VERSION"     => nil,
          "PAGES_STATS_HOSTS" => GitHub.stats_whitelist ? "" : (GitHub.stats_hosts || []).join(","),
        }
      end

      # Environment variables that override commands needed to build the site.
      # The page-build and page-sync commands use these so that commands may be
      # locked down to a user or other policy can be enforced.
      def command_execution_environment

        if GitHub.enterprise?
          env = { "PAGES_ENV" => "enterprise", "RBENV_VERSION" => ENV["RBENV_VERSION"] }
          if File.exist?("/usr/local/share/enterprise/ghe-pages-wrapper")
            env.merge!("GHE_FIREJAIL" => "1", "COMMAND_PREFIX" => "sudo /usr/local/share/enterprise/ghe-pages-wrapper")
          end
        elsif production_environment?
          env = { "PAGES_ENV" => "dotcom" }
        else
          env = { "PAGES_ENV" => "development" }
        end

        # Pass the pushers ID, since a user cant be determined from a integration token
        env["PUSHER_ID"] = @pusher.id.to_s

        # Use a filebased Failbot in Enterprise and in development for pages,
        # much like we do for github/github
        if Rails.development? || (GitHub.enterprise? && !Rails.test?)
          env["FAILBOT_BACKEND"] = "file"
          env["FAILBOT_BACKEND_FILE_PATH"] = GitHub.pages_failbot_backend_file_path
        end

        # In development, pass request token and nwo since they aren't passed in the clone_url
        if Rails.development?
          env["REQUEST_TOKEN"]  = should_use_apps? ? pages_github_app_token : oauth_token
          env["REPOSITORY_NWO"] = @page.repository.nwo
        end

        # Add flipper features
        env["PAGES_FEATURES_STATUSES"]   = pages_statuses_enabled?(@page.repository) ? "enabled" : ""
        env["PAGES_FEATURES_CHECKS_API"] = pages_features_checks_api_enabled?(@page.repository) ? "enabled" : ""
        env["PAGES_FEATURES_GITHUB_APP"] = should_use_apps? ? "enabled" : ""
        env["PAGES_BETA_IMAGE"]          = pages_beta_image?(@page.repository) ? "enabled" : ""
        env["PAGES_BRANCH_BUILD"]        = page_deployment.branch_build? ? "true" : "false"

        # Pass the configured pages site size limit in the environment.
        env["PAGES_SIZE_LIMIT"]    = GitHub.pages_site_size_limit.to_s
        env["PAGES_HOSTNAME"]      = GitHub.pages_host_name_v2
        env["GITHUB_HOSTNAME"]     = GitHub.host_name
        env["ASSET_HOST_URL"]      = GitHub.asset_host_url
        env["SUBDOMAIN_ISOLATION"] = GitHub.subdomain_isolation?.to_s
        env["SSL"]                 = GitHub.ssl?.to_s
        env["API_URL"]             = GitHub.api_url
        env["HELP_URL"]            = GitHub.help_url
        env["EARLY_ACCESS"]        = @page.repository.preview_features?.to_s
        env["GITHUB_STAFF"]        = @pusher.preview_features?.to_s
        env["OAUTH_BUILD"]         = Rails.production?.to_s
        env["PAGE_BUILD_ID"]       = page_build_id
        env["PAGE_PREVIEW_URL"]    = page_deployment.preview_url

        env
      end

      # The containing directory used to perform page builds.
      def build_user_path
        @build_user_path ||= [
          @build_dir,
          "pagebuilds",
          repository.owner.to_s.downcase,
        ].join("/")
      end

      # The underlying Repository record for the current page.
      def repository
        @page.repository
      end

      # The Page::Deployment for this build
      def page_deployment
        @page_deployment ||= @page.create_or_find_deployment_for(@git_ref_name)
      end

      # Run the passed in command (with process options) and throw an
      # appropriate exception if the process does not execute successfully.
      #
      # command - The command to execute.
      # system_error_msg - The error message this command returns by default.
      #   This is used to differentiate a UserError from an Error.
      # msg - The error message to use if the error is due to a UserError.
      # options - A Hash of options passed to POSIX::Spawn::Child
      #
      # Returns a POSIX::Spawn::Child if the process executes successfully and
      # throws an appropriate exception otherwise.
      def run(command, system_error_msg = nil, msg = nil, options = {})
        msg ||= system_error_msg

        sanitized_command = sanitize_command(command)

        Failbot.push(
          repo_id: repository.id,
          user: repository.owner.login,
          page_id: @page.id.inspect,
          pages_build_command: sanitized_command.inspect,
          pages_build_options: options.inspect,
          pages_github_app_installation: pages_github_app_installation.present?,
        )

        env     = options.delete(:env) || {}
        options = options.merge(pgroup_kill: true)
        tries   = options.delete(:tries) || (production_environment? ? COMMAND_TRIES : 0)
        log({fn: "page-builder-run", command: command.join(" ")}.merge(env))

        process = POSIX::Spawn::Child.new(env, *(command + [options]))
        (1...tries).each do |try|
          break if process.status.success?
          sleep COMMAND_RETRIES_SLEEP
          process = POSIX::Spawn::Child.new(env, *(command + [options]))
        end

        if !process.status.success?
          error = sanitized_error(process.err) || msg

          stdout = filter_tokens(process.out)
          stderr = filter_tokens(process.err)

          # Scrub fastly api key from logs
          find = /GH_FASTLY_APIKEY=[\S]+/
          replace = "GH_FASTLY_APIKEY=[FASTLY_KEY]"
          stdout = stdout.gsub(find, replace)
          stderr = stderr.gsub(find, replace)
          sanitized_command.each_index do |idx|
            command[idx].gsub!(find, replace)
          end

          Failbot.push(
            pages_stdout: stdout,
            pages_stderr: stderr,
            pages_status: process.status.exitstatus.inspect,
            pages_sanitized_error: error,
            pages_warning: warning(process.err),
          )

          if authentication_error?(process)
            raise AuthError, error
          # Segment system errors we cause from user-caused errors
          elsif error && error.start_with?(system_error_msg)
            raise Error, error
          else
            raise UserError, error
          end
        else
          process
        end
      end

      # Builds the page and retries up to BUILD_TRIES times if it fails due to
      # an authentication failure. Pages explicitly creates an OAuth token, so
      # an authentication error should only happen if there was a race between
      # when the OAuth token was generated and when the current value propogates
      # to the DB replicas. Once we add support for MySQL GTIDs we may
      # reconsider revising the Pages build logic to wait for the replicas to
      # update before using the OAuth token.
      def page_build
        tries ||= BUILD_TRIES

        replicate_oauth_token! # skipped if using GitHub Apps

        timing_key = Page.build_in_docker? ? "page-build-docker" : "page-build"
        log_build_step(timing_key) do
          run(
            @page.subdir_source? ?
              ["#{pages_jekyll_script_dir}/page-build", clone_url, @git_ref_name, @page.source_dir, build_path] :
              ["#{pages_jekyll_script_dir}/page-build", clone_url, @git_ref_name, build_path],
            "Page build failed.",
            "Unable to build page. Please try again later.",
            env: build_execution_environment,
            timeout: 11.minutes.to_i,
            tries: 1,
          )
        end
      rescue AuthError
        tries -= 1
        if tries > 0
          # Give the DB replicas time to update with the current OAuth token
          # value.
          sleep(BUILD_SLEEP_TIME)
          retry
        else
          raise
        end
      end

      def page_build_id
        @build && "page_build:#{@build.id}"
      end

      # Returns whether the process failure was due to an authentication error.
      def authentication_error?(process)
        !process.status.success? && process.err.index(AUTH_ERROR_MESSAGE)
      end

      # Returns the sanitized error as passed from PagesJekyll
      def sanitized_error(output)
        output[/^\+ \e\[31mPagesJekyll Sanitized Error: (.+)\e\[0m$/, 1]
      end

      def filter_tokens(output)
        return if output.nil?
        # if a token is nil, standard gsub will match every character
        if oauth_token.present?
          output = output.gsub(oauth_token.to_s, "[OAUTH TOKEN]")
        end
        if pages_github_app_token.present?
          output = output.gsub(pages_github_app_token.to_s, "[X-ACCESS-TOKEN]")
        end

        output
      end

      def warning(output)
        match = output.match(/^\+ \e\[33mPagesJekyll Warning: (.+?)\e\[0m$/m)
        match[1] if match
      end

      def step_times
        @step_times ||= {}
      end

      def log_build_step(fn)
        start_time = Time.now
        yield
      ensure
        elapsed = (Time.now - start_time) * 1_000
        step_times["#{fn}-time"] = elapsed
        GitHub.dogstats.timing("pages.#{fn}", elapsed)
      end

      def log(data)
        data = data.dup
        data.each_key do |key|
          data[key] = filter_tokens(data[key].to_s)
        end
        GitHub::Logger.log(base_log_data.merge(data))
      end

      def base_log_data
        @base_log_data ||= {
          page_build: page_build_id,
          git_ref_name: @git_ref_name.inspect,
          nwo: repository.name_with_owner,
        }.freeze
      end

      # Report a failed build check run or status.
      def report_failed_build_to_user
        if should_use_apps?
          report_failed_build_check(repo: repository, git_ref_name: @git_ref_name, actor: @pusher)
        elsif pages_statuses_enabled?(repository)
          report_failed_build_status(repo: repository, git_ref_name: @git_ref_name, pages_oauth_access: pages_oauth_access)
        end
      end

      # Report a failed build status for the given repository, git ref, and pages OAuth access.
      def report_failed_build_status(repo:, git_ref_name:, pages_oauth_access:)
        Statuses::Service.create_status(
          repo_id: repo.id,
          data: {
            state: "failure",
            description: BUILD_TIMEOUT_ERROR,
            context: "github/pages",
            sha: repo.refs.find(git_ref_name).target_oid,
            oauth_application_id: pages_oauth_access.application.id,
          },
          user_id: Organization.find_by_login("github"),
        )
      end

      # Report a failed build check run for the given repository, git ref, and actor.
      def report_failed_build_check(repo:, git_ref_name:, actor:)
        check_suite = Checks::Service.find_or_create_check_suite(
          github_app_id: GitHub.pages_github_app.id,
          head_sha: repo.refs.find(git_ref_name).target_oid,
          repo: repo,
        )
        check_suite.check_runs.create!(
          creator_id: actor.id,
          name: GitHub.pages_check_run_name,
          status: :completed,
          conclusion: :timed_out,
          started_at: Time.now.utc,
          completed_at: Time.now.utc,
          details_url: GitHub.pages_help_url,
          title: CHECK_FAILURE_TITLE,
          summary: BUILD_TIMEOUT_ERROR,
        )
      rescue ActiveRecord::RecordInvalid => error
        Failbot.report(error)
        false
      end
    end
  end
end
