# frozen_string_literal: true

require "github/pages/management/delegate"

class GitHub::Pages::GarbageCollector
  class Build

    attr_reader :path, :page_id, :revision

    def initialize(path)
      @path = path

      path_components = path.split("/")

      @page_id = path_components[-2].to_i
      @revision = path_components[-1]
    end
  end

  def self.run!
    gc = new

    GitHub.dogstats.time("pages.gc.runtime") do
      garbage = gc.garbage

      garbage.each do |build|
        next if build.page_id <= 0
        gc.delegate.log "Garbage collecting page_id=#{build.page_id} path=#{build.path}"
        gc.rm_rf(build.path)
      end

      GitHub.dogstats.count("pages.gc.collected", garbage.count)
    end

    nil
  end

  attr_reader :delegate

  def initialize(delegate = nil)
    @delegate = (delegate || GitHub::Pages::Management::Delegate.new(logger: GitHub::Logger))
  end

  def host
    @host ||=
      if GitHub.enterprise?
        GitHub.local_pages_host_name
      else
        Socket.gethostname
      end
  end

  def garbage
    builds_by_page_id = builds.group_by(&:page_id)

    current_deployments = Set.new

    builds_by_page_id.keys.each_slice(1_000) do |page_ids|
      deployed_revisions_with_replica_count(page_ids: page_ids).each do |id, built_revision|
        current_deployments << "#{id}_#{built_revision}"
      end
    end

    builds_by_page_id.flat_map do |page_id, builds|
      builds.reject do |build|
        current_deployments.include?("#{page_id}_#{build.revision}")
      end
    end
  end

  def deployed_revisions_with_replica_count(page_ids:)
    deployed_revisions_using_deployments(page_ids: page_ids) | deployed_revisions_legacy(page_ids: page_ids)
  end

  def deployed_revisions_using_deployments(page_ids:)
    results = ActiveRecord::Base.connected_to(role: :reading) do
      ApplicationRecord::Domain::Repositories.github_sql.results(<<-SQL, page_ids: page_ids, host: host)
        SELECT page_deployments.page_id, page_deployments.revision FROM page_deployments
        INNER JOIN pages_replicas ON (page_deployments.id = pages_replicas.pages_deployment_id)
        INNER JOIN pages ON (page_deployments.page_id = pages.id)
        WHERE page_deployments.page_id IN :page_ids
        AND page_deployments.revision IS NOT NULL
        AND pages_replicas.host = :host
        GROUP BY page_deployments.page_id, page_deployments.revision
        ORDER BY NULL
      SQL
    end
    Set.new(results)
  end

  def deployed_revisions_legacy(page_ids:)
    results = ActiveRecord::Base.connected_to(role: :reading) do
      ApplicationRecord::Domain::Repositories.github_sql.results(<<-SQL, page_ids: page_ids, host: host)
        SELECT pages.id, pages.built_revision FROM pages
        INNER JOIN pages_replicas ON (pages.id = pages_replicas.page_id)
        WHERE pages.id IN :page_ids
        AND pages.built_revision IS NOT NULL
        AND pages_replicas.pages_deployment_id IS NULL
        AND pages_replicas.host = :host
        GROUP BY pages.id, pages.built_revision
        ORDER BY NULL
      SQL
    end
    Set.new(results)
  end

  def builds
    @builds ||= begin
      command = [
        # find the site directories
        "find", GitHub.pages_dir,
        "-mindepth", "6",
        "-maxdepth", "6",
        # only return files older than 1 hour:
        "-mmin", "+60"
      ]

      if command_exist?("ionice")
        # make I/O class 'idle'.
        command = ["ionice", "-c", "3", *command]
      end

      if command_exist?("timeout")
        # timeout after 58 minutes, kill if process doesn't end after 60.
        command = ["timeout", "--kill-after=120", "3480", *command]
      end

      GitHub.dogstats.time("pages.gc.find") do
        IO.popen(command, "r") do |find|
          find.each_line.map do |path|
            Build.new(path.chomp) if GitHub::Routing.dpages_storage_path?(path.chomp)
          end.compact
        end
      end
    end
  end

  def rm_rf(path)
    POSIX::Spawn::Child.new(*rm_rf_command, path)
  end

  def rm_rf_command
    @rm_rf_command ||= begin
      command = ["rm", "-rf", "--"]

      if command_exist?("ionice")
        # make I/O class 'idle'.
        command = ["ionice", "-c", "3", *command]
      end

      if command_exist?("timeout")
        # timeout after 58 minutes, kill if process doesn't end after 60.
        command = ["timeout", "--kill-after=120", "3480", *command]
      end

      command
    end
  end

  def command_exist?(command)
    ENV["PATH"].split(File::PATH_SEPARATOR).each do |path|
      executable_filename = File.join(path, command)
      return true if test("f", executable_filename) && test("x", executable_filename)
    end

    false
  end
end
