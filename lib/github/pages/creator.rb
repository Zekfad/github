# rubocop:disable Style/FrozenStringLiteralComment

require "tilt"
require "erb"
require "generated_pages/page"

module GitHub
  module Pages
    class Creator
      class Context
        # Internal: Generate a GitHub URL with the specified path.
        #
        # paths - Array of strings representing the GitHub path.
        #
        # Returns a GitHub URL string.
        def github_url_for(*paths)
          url = GitHub.ssl? ? "https" : "http"
          url << "://" << GitHub.host_name
          url << "/"   << paths.join("/")
          url
        end
      end

      def initialize(repo, user, params)
        @repo   = repo
        @user   = user
        @params = params
      end

      # Internal: The theme to use when creating the page.
      #
      # Returns the GitHub::Pages::Theme instance, or nil if no theme specified.
      def theme
        if @params[:theme_slug].present?
          GitHub::Pages::Theme.find @params[:theme_slug]
        else
          nil
        end
      end

      # Internal: Renders & returns the markup to be used for the generated page.
      #
      # Returns a string of HTML.
      def content
        ::GeneratedPages::Page.new(locals).render
      end

      # Internal: Creates the gh-pages branch in the current repo and adds all
      # the files (markup and assets) for the generated page to it.
      #
      # Returns nothing.
      def run
        files = {}
        ref = @repo.heads.read(@repo.pages_branch)

        if ref.exist?
          # Try to grab the existing CNAME file
          if cname_blob = @repo.blob(ref.commit.oid, "CNAME")
            # If there's an existing CNAME file, add it to the new index
            files["CNAME"] = cname_blob.data
          end
        end

        if @repo.is_user_pages_repo?
          message = "Replace #{@repo.pages_branch} branch with page content via GitHub"
        else
          message = "Create #{@repo.pages_branch} branch via GitHub"
        end

        theme_files = {}

        if theme.present?
          theme.all_asset_paths(:filesystem).each do |path|
            asset_file = File.open(path)
            theme_files[theme.relative_path_for(path)] = asset_file.read
            asset_file.close
          end
        end

        # Record field values so they can be brought back later
        params_to_serialize = @params.slice(:name, :tagline, :body, :google)
        params_to_serialize[:note] = "Don't delete this file! It's used internally to help with page regeneration."

        # Generate the commit and bump the pages branch
        ref.append_commit(
          {message: message, author: @user},
          @user,
        ) do |files|
          theme_files.each { |name, data| files.add(name, data) }
          files.add("params.json", Yajl::Encoder.encode(params_to_serialize, pretty: true))
          files.add("index.html", content)
        end
      end

      # Internal: Local variables for the page template.
      #
      # Returns a Hash containing the variables.
      def locals
        html = GitHub::HTML::PagesPipeline.to_html(@params[:body])

        locals = @params.merge(
          projectpath: @repo.name_with_owner,
          owner: @repo.owner,
          git_clone_url: @repo.clone_url,
          body_html: html,
          page_type: @repo.is_user_pages_repo? ? "user" : "project",
          repo_id: @repo.id,
        )

        locals.reverse_merge!(
          description: @repo.description,
          theme_color_class: "",
          theme_font_class: "",
        )

        locals[:project] = locals[:name] || @repo.name

        locals[:stylesheet_paths] ||= theme.stylesheet_paths.map do |path|
          theme.relative_path_for path
        end

        locals[:javascript_paths] ||= theme.javascript_paths.map do |path|
          theme.relative_path_for path
        end

        locals[:font_paths]       ||= theme.font_paths.map do |path|
          theme.relative_path_for path
        end

        locals[:asset_path_prefix] = File.join(theme.base_path_components(:web)) + "/"

        locals
      end
    end
  end
end
