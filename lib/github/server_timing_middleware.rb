# frozen_string_literal: true

module GitHub
  # Appends Server-Timing headers to the response for viewing in browser
  # network devtools.
  #
  # https://w3c.github.io/server-timing/
  class ServerTimingMiddleware
    def initialize(app)
      @app = app
    end

    def call(env)
      status, headers, body = @app.call(env)
      stats = env[Rack::ProcessUtilization::ENV_KEY]
      if stats && staff_user_from_env(env)
        value = timings(stats)
        if !value.empty?
          headers["Server-Timing"] = value
        end
      end
      [status, headers, body]
    end

    private

    def timings(stats)
      values = []
      if stats.track_mysql?
        values << ["SQL", stats.mysql_stats[1]]
      end
      if stats.track_redis?
        values << ["Redis", stats.redis_stats[1]]
      end
      if stats.track_graphql?
        values << ["GraphQL", stats.graphql_stats[1]]
      end
      if stats.track_cache?
        values << ["Cache", stats.cache_stats[1]]
      end
      if stats.track_gc?
        values << ["GC", stats.gc_info.time]
      end
      if stats.track_es?
        values << ["Search", stats.es_stats[1]]
      end
      if stats.track_gitrpc?
        values << ["GitRPC", stats.gitrpc_stats[1]]
      end
      if stats.track_render? && stats.render_stats
        values << ["Render", stats.render_stats.total_time]
      end
      if stats.track_cpu?
        cpu, idle, real = stats.cpu_stats
        values << ["Unicorn", cpu + idle]
      end
      values.map { |name, value| "#{name};dur=#{millis(value)}" }.join(",")
    end

    def millis(duration)
      (duration * 1000).round(2)
    end

    def staff_user_from_env(env)
      if cookie = GitHub::StaffOnlyCookie.read(Rack::Request.new(env).cookies)
        cookie.user
      end
    end
  end
end
