# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  # Commit message formatting class. Takes commit messages of all shapes and
  # sizes and ensures a sane subject and body. When the subject exceeds 72
  # characters, it's truncated and the removed portion is moved to the body.
  #
  # This class handles plain text commit messages only and should not be used
  # when the commit message is to be displayed as HTML. Use the
  # CommitMessageHTML class instead.
  class CommitMessage
    include GitHub::UTF8

    ELLIPSIS = "…".freeze
    MAX = 72
    # We always truncate at least 3 characters off the end to avoid creating a
    # body that is just an ellipsis followed by one or two characters (which
    # looks silly).
    TRUNCATED_LENGTH = MAX - 3

    # Create a new CommitMessage with the text message provided. This should be
    # the full commit message.
    def initialize(message)
      @message, @subject, @body = split_message(message)
      @tooltip = @message.rstrip

      if @subject.length > MAX
        @truncated = true
        body_part = "#{ELLIPSIS}#{subject[TRUNCATED_LENGTH..-1]}".rstrip
        @subject = "#{@subject[0, TRUNCATED_LENGTH]}#{ELLIPSIS}"
        @body = [body_part, @body].compact.join("\n\n")
      end
    end

    # The original commit message string without modifications.
    attr_reader :message

    # The commit message string used as title for truncated commit messages.
    attr_reader :tooltip

    # The (possibly truncated) subject of the commit message. This is the first
    # line or the first 69 characters with an ellipsis.
    attr_reader :subject

    # The body part of the message. If the subject was truncated, this will
    # included the removed portion of the subject as well.
    attr_reader :body

    # Does the commit message have a body?
    def body?
      !@body.nil?
    end

    # Was the subject truncated due to being over 72 chars?
    def truncated?
      @truncated
    end

    # The subject and body as a String separated by two lines.
    def to_s
      if @body
        [@subject, @body].join("\n\n")
      else
        @subject
      end
    end

    alias_method :to_str, :to_s

  private
    def split_message(message)
      message = utf8(message.to_s)
      subject, body = message.split(/\n+/, 2)
      subject.rstrip! if subject

      if body
        body.rstrip!
        body = nil if body.empty?
      end

      [message, subject.to_s, body]
    end
  end

  # Like CommitMessage but generates HTML output instead of plain text. The
  # logic for this is very different since the text displayed in an HTML commit
  # messages may be shortened considerably compared to the messages plain text.
  # For instance, the CommitMention filter replaces 40 char SHA1s with 7 char
  # SHA1s with links. We want to truncate based on the *visible* HTML text, not
  # the original text or the HTML markup.
  class CommitMessageHTML < CommitMessage

    LONG_COMMIT_MESSAGE = 51_200

    # Create a new CommitMessage with the text message provided. This should be
    # the full commit message.
    #
    # message - The full plain text commit message as a String.
    # context - The HTML pipeline context hash.
    def initialize(message, context = {}, pipeline = GitHub::Goomba::CommitMessagePipeline)
      @message, @subject, @body_text = split_message(message)
      @context = context
      @context[:location] = Commit.name
      @pipeline = pipeline
      @subject_pipeline = pipeline
      prevent_url_subject_from_being_link
    end

    # The original commit message string without modifications.
    attr_reader :message

    # Does the commit message have a body? True when the subject is truncated
    # and spills over into the body as well as when a proper body is present.
    def body?
      return true if @body_text
      truncated?
    end

    def prevent_url_subject_from_being_link
      @subject_pipeline = GitHub::Goomba::CommitSubjectPipeline if subject_is_url?
    end

    SUBJECT_IS_URL_REGEX = /\A\s*#{URI::DEFAULT_PARSER.make_regexp(%w(http https))}\s*\z/
    def subject_is_url?
      SUBJECT_IS_URL_REGEX.match?(@subject)
    end

    # The body of the commit message, possibly including the removed portion of
    # the message subject.
    #
    # Returns a String of HTML markup or nil when no body is present.
    def async_body
      return @async_body if defined?(@async_body)

      @async_body = Promise.all([async_subject_overflow, async_body_html]).then do |overflow, body_html|
        next "" if overflow.nil? && body_html.nil?
        [overflow, body_html].compact.join("\n\n").rstrip.html_safe # rubocop: disable Rails/OutputSafety
      end
    end

    def body
      async_body.sync
    end

    # The (possibly truncated) subject as a String of HTML markup.
    def async_subject
      return @async_subject if defined?(@async_subject)
      @async_subject = async_split_subject.then(&:first).then(&:html_safe)
    end

    def subject
      async_subject.sync
    end

    def async_truncated?
      async_split_subject.then { |_, overflow| !overflow.nil? }
    end

    def truncated?
      async_truncated?.sync
    end

    # The subject and body as a single HTML string.
    def to_html
      [subject, body].compact.join("\n\n")
    end
    alias to_s to_html

    ##
    # Internal Methods

    # Determine if the subject overflows the maximum length and move excess to
    # the body if so.
    def async_split_subject
      return @async_split_subject if defined?(@async_split_subject)

      @async_split_subject = async_subject_document.then do |doc|
        truncator = HTMLTruncator.new(doc, 72)
        subject = truncator.to_html(wrap: false)
        if node = truncator.remaining.child
          subject_overflow = node.inner_html.strip
          subject_overflow = nil if subject_overflow.empty?
          [subject, subject_overflow]
        else
          [subject, nil]
        end
      end
    end

    def async_subject_document
      return @async_subject_document if defined?(@async_subject_document)

      body_content = HTML::BodyContent.new(@subject, @context, @subject_pipeline)
      @async_subject_document = body_content.async_document
    end

    def async_body_html
      return @async_body_html if defined?(@async_body_html)

      return @async_body_html = Promise.resolve(nil) if @body_text.nil?

      @async_body_html = commit_message_pipeline.async_to_html(@body_text, @context).then do |body|
        body[0, 5] == "<div>" ? body[5...-6] : body # remove <div></div> wrapper
      end
    end

    # Run the body text through the HTML pipeline to linkify and otherwise
    # format. Returns nil with no body.
    def body_html
      async_body_html.sync
    end

    # HTML pipeline used to process the message. When the commit message is
    # excessively large (>50K) only basic plain text formatting is applied.
    def commit_message_pipeline
      if @message.length > LONG_COMMIT_MESSAGE
        GitHub::Goomba::LongCommitMessagePipeline
      else
        @pipeline
      end
    end

    def async_subject_overflow
      async_split_subject.then(&:last)
    end
  end

  module CommitMessageable
    # Public: The CommitMessage (plain text) object.
    def message_text
      @message_text ||= GitHub::CommitMessage.new(message)
    end

    # Public: The (possibly truncated) commit short message as plain text.
    def short_message_text
      message_text.subject
    end

    # Public: The body part of the plain text message. This is anything
    # following the first line feed. Leading and trailing whitespace is
    # stripped.
    def message_body_text
      message_text.body
    end

    # Public: Is there a long part?
    def message_body_text?
      message_body_text && !message_body_text.empty?
    end

    # Public: A CommitMessageHTML object, useful for formatting commit message subject
    # and body with support for truncation and splitting.
    def message_html
      @message_html ||=
        GitHub::CommitMessageHTML.new(message, message_context)
    end

    # Public: Truncated HTML of the commit's short message.
    #
    # Returns the short message HTML as a String. No wrapping element is
    # included, although various inline markup may be present.
    def short_message_html
      async_short_message_html.sync
    end

    def async_short_message_html
      return @async_short_message_html if defined?(@async_short_message_html)

      subject_promise = Platform::Loaders::Cache.fetch(message_cache_key("short_message_html")) do
        message_html.async_subject
      end

      @async_short_message_html = subject_promise.then(&:html_safe)
    end

    # Public: HTML version of the commit's body message.
    #
    # Returns the body message HTML as a String. No wrapping element is
    # included, although various inline markup may be present. The string will
    # be empty when no body is present, nil is never returned.
    def message_body_html
      async_message_body_html.sync
    end

    def async_message_body_html
      return @async_message_body_html if defined?(@async_message_body_html)

      body_promise = Platform::Loaders::Cache.fetch(message_cache_key("message_body_html")) do
        message_html.async_body
      end

      @async_message_body_html = body_promise.then(&:html_safe)
    end

    # Public: Truncated HTML version of the commit's body message.
    #
    # limit - The maximum number of _visibly rendered_ characters allowed.
    #
    # Returns the body message HTML as a String. No wrapping element is
    # included, although various inline markup may be present. The string will
    # be empty when no body is present, nil is never returned.
    def async_truncated_message_body_html(limit)
      async_message_body_html.then do |body_html|
        HTMLTruncator.new(body_html, limit).to_html
      end
    end

    # Does the commit message include an extended body? This can happen when a
    # long message is given OR when a commit message subject is truncated and
    # spills over.
    def message_body_html?
      message_html.body?
    end

    # The HTML Pipeline context hash used when turning commit messages into HTML.
    def message_context
      @message_context ||= {
        entity: repository,
        base_url: GitHub.url,
        asset_proxy: GitHub.image_proxy_url,
        disable_asset_proxy: !GitHub.image_proxy_enabled?,
        whitelist: nil,
        location: self.class.name,
      }
    end

    # The BodyContent object with the fully processed HTML message and resulting
    # context items.
    #
    # Returns a GitHub::HTML::BodyContent object.
    def message_content
      @message_content ||=
        GitHub::HTML::BodyContent.new(message, message_context, GitHub::Goomba::CommitMessagePipeline)
    end

    # The GitHub::HTML Result object.
    def message_result
      message_content.result
    end

    # Generate a message_cache key for memcached.
    def message_cache_key(attribute)
      repo_key = repository && repository.id
      "commit-message:v5:#{attribute}:#{repo_key}:#{oid}"
    end
  end
end
