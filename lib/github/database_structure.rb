# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module DatabaseStructure

    MYSQL1     = "mysql1".freeze
    MYSQL2     = "mysql2".freeze
    MYSQL5     = "mysql5".freeze
    SPOKES     = "spokes".freeze
    COLLAB     = "collab".freeze
    BALLAST    = "ballast".freeze
    NOTIFY     = "notify".freeze
    NOTIFICATIONS_DELIVERIES = "notifications_deliveries".freeze
    NOTIFICATIONS_ENTRIES = "notifications_entries".freeze
    GITBACKUPS = "gitbackups".freeze
    IAM        = "iam".freeze
    IAM_ABILITIES = "iam_abilities".freeze
    REPOSITORIES = "repositories".freeze
    VT         = "vt".freeze
    STRATOCASTER = "stratocaster".freeze
    MEMEX    = "memex".freeze

    ALL_CLUSTERS = [MYSQL1, MYSQL2, MYSQL5, SPOKES, COLLAB, BALLAST, NOTIFY, NOTIFICATIONS_DELIVERIES, NOTIFICATIONS_ENTRIES, GITBACKUPS, IAM, IAM_ABILITIES, REPOSITORIES, VT, STRATOCASTER, MEMEX]

    IAM_ABILITIES_TABLES = [
      "abilities",
    ]

    MYSQL2_TABLES = [
      "custom_inboxes",
      "hidden_users",
      "notification_subscriptions",
      "notification_thread_subscriptions",
      "notification_thread_type_subscriptions",
      "notification_subscription_events",
      "notification_user_settings",
      "mobile_device_tokens",
      "mobile_push_notification_schedules",
      "rollup_summaries",
    ]

    NOTIFICATIONS_DELIVERIES_TABLES = [
      "notification_deliveries",
      "mobile_push_notification_deliveries",
    ]

    NOTIFICATIONS_ENTRIES_TABLES = [
      "notification_entries",
      "spammy_notification_entries",
      "saved_notification_entries",
    ]

    MYSQL5_TABLES = [
      "key_values",
      "stratocaster_indexes",
      "search_index_configurations",
      "search_index_template_configurations",
      "global_stratocaster_indexes",
    ]

    VT_TABLES = []

    SPOKES_TABLES = [
      "cold_networks",
      "datacenters",
      "fileservers",
      "gist_replicas",
      "git_sizer",
      "network_replicas",
      "repository_checksums",
      "repository_replicas",
      "sites",
    ]

    STRATOCASTER_TABLES = [
      "stratocaster_events",
    ]

    GITBACKUPS_TABLES = [
      "disabled_backups",
      "key_versions",
      "wal",
      "gist_bases",
      "gist_incrementals",
      "gist_maintenance",
      "repository_bases",
      "repository_incrementals",
      "repository_maintenance",
      "wiki_bases",
      "wiki_incrementals",
      "wiki_maintenance",
    ]

    COLLAB_TABLES = [
      "abuse_reports",
      "advisory_credits",
      "application_callback_urls",
      "archived_deleted_issues",
      "archived_user_reviewed_files",
      "audit_log_git_event_exports",
      "billing_budgets",
      "billing_disputes",
      "billing_metered_usage_configurations",
      "billing_payouts_ledger_discrepancies",
      "billing_payouts_ledger_entries",
      "billing_prepaid_metered_usage_refills",
      "billing_sales_serve_plan_subscriptions",
      "bulk_dmca_takedowns",
      "bulk_dmca_takedown_repositories",
      "bundled_license_assignments",
      "repository_clones",
      "close_issue_references",
      "codespace_billing_entries",
      "codespaces",
      "codespace_billing_messages",
      "codespaces_compute_usage_line_items",
      "codespaces_storage_usage_line_items",
      "commit_contributions",
      "compromised_passwords",
      "compromised_password_datasources",
      "compromised_password_datasources_passwords",
      "deceased_users",
      "deleted_discussions",
      "deleted_issues",
      "device_authorization_grants",
      "discussion_categories",
      "discussion_comments",
      "discussion_comment_edits",
      "discussion_events",
      "discussion_reactions",
      "discussion_comment_reactions",
      "discussion_spotlights",
      "discussion_transfers",
      "discussions",
      "discussion_edits",
      "discussions_labels",
      "docusign_envelopes",
      "enterprise_contributions",
      "external_identity_attribute_mappings",
      "issue_boost_awards",
      "features",
      "feature_enrollments",
      "fraud_flagged_sponsors",
      "ghvfs_fileservers",
      "ghvfs_replicas",
      "ip_whitelist_entries",
      "issue_boosts",
      "issue_transfers",
      "integration_manifests",
      "key_links",
      "autosaves",
      "autosave_archives",
      "autosave_archive_authors",
      "autosave_checkpoints",
      "autosave_checkpoint_authors",
      "marketplace_blog_posts",
      "repository_code_symbol_indices",
      "code_symbol_definitions",
      "organization_discussion_post_replies",
      "organization_discussion_posts",
      "organization_profiles",
      "pinned_issues",
      "pinned_issue_comments",
      "plan_trials",
      "repository_advisories",
      "repository_advisory_comments",
      "repository_advisory_events",
      "repository_contribution_graph_statuses",
      "repository_network_graphs",
      "epochs",
      "epoch_commits",
      "epoch_operations",
      "actions_usage_aggregations",
      "actions_usage_line_items",
      "usage_synchronization_batches",
      "code_search_indexed_heads",
      "certificates",
      "certificate_trusters",
      "science_events",
      "user_licenses",
      "enterprise_installation_user_accounts",
      "enterprise_installation_user_account_emails",
      "enterprise_installation_user_accounts_uploads",
      "scoped_integration_installations",
      "business_user_accounts",
      "enterprise_agreements",
      "sponsorships",
      "sponsorship_newsletters",
      "ssh_certificate_authorities",
      "ssh_certificate_authorities_users",
      "stripe_connect_accounts",
      "ofac_downgrades",
      "otp_sms_timings",
      "hidden_task_list_items",
      "integration_aliases",
      "user_dashboard_pins",
      "user_reviewed_files",
      "webauthn_user_handles",
      "integration_single_files",
      "sponsorship_newsletter_tiers",
      "sponsors_listings",
      "trade_controls_restrictions",
      "pending_trade_controls_restrictions",
      "sponsors_tiers",
      "sponsors_memberships",
      "composable_comments",
      "interactive_components",
      "ephemeral_notices",
      "interactive_component_interactions",
      "markdown_components",
      "team_member_delegated_review_requests",
      "sponsors_criteria",
      "sponsors_memberships_criteria",
      "two_factor_recovery_requests",
      "user_labels",
      "package_registry_data_transfer_aggregations",
      "package_registry_data_transfer_line_items",
      "pending_automatic_installations",
      "refresh_tokens",
      "attribution_invitations",
      "shared_storage_artifact_events",
      "shared_storage_artifact_aggregations",
      "shared_storage_billable_owners",
      "reminders",
      "reminder_delivery_times",
      "reminder_repository_links",
      "reminder_slack_workspaces",
      "reminder_team_memberships",
      "personal_reminders",
      "reminder_event_subscriptions",
      "reminder_slack_workspace_memberships",
      "review_request_delegation_excluded_members",
      "sponsors_listing_featured_items",
      "email_domain_reputation_records",
      "sponsors_activities",
      "sponsors_activity_metrics",
      "site_scoped_integration_installations",
      "user_seen_features",
      "workspace_plans",
      "workspace_resource_groups",
      "workspaces",
      "metered_usage_exports",
      "sponsors_fraud_reviews",
      "acv_contributors",
      "successor_invitations",
      "sponsors_goals",
      "sponsors_goal_contributions",
      "imports",
      "repository_imports",
      "pull_request_imports",
      "photo_dna_hits",
      "user_personal_profiles",
      "sponsorship_match_bans",
      "merge_queues",
      "merge_queue_entries",
      "merge_groups",
      "merge_group_entries",
      "vss_subscription_events",
    ]

    BALLAST_TABLES = [
      "archived_custom_field_entries",
      "artifacts",
      "authenticated_devices",
      "authentication_records",
      "check_annotations",
      "check_runs",
      "check_steps",
      "check_suites",
      "code_scanning_alerts",
      "collection_items",
      "collection_urls",
      "collection_videos",
      "collections",
      "content_reference_attachments",
      "content_references",
      "custom_field_entries",
      "discussion_post_replies",
      "discussion_posts",
      "docusign_webhooks",
      "organization_domains",
      "pages_embargoed_cnames",
      "push_infos",
      "pushes",
      "reserved_logins",
      "retired_namespaces",
      "slotted_counters",
      "stafftools_roles",
      "statuses",
      "streaming_logs",
      "stripe_webhooks",
      "user_stafftools_roles",
      "web_push_subscriptions",
      "workflows",
      "workflow_runs",
      "zuora_webhooks",
    ]

    NOTIFY_TABLES = [
      "vulnerabilities",
      "vulnerability_references",
      "vulnerable_version_ranges",
      "repository_dependency_updates",
      "repository_vulnerability_alerts",
      "archived_repository_vulnerability_alerts",
      "pending_vulnerabilities",
      "pending_vulnerability_references",
      "pending_vulnerable_version_ranges",
      "team_group_mappings",
      "team_sync_business_tenants",
      "team_sync_tenants",
      "token_scan_statuses",
      "token_scan_results",
      "token_scan_result_locations",
      "vulnerability_alerting_event_subscriptions",
      "vulnerability_alerting_events",
      "vulnerable_version_range_alerting_processes",
    ]

    IAM_TABLES = [
      "abilities_permissions_routing",
      "fine_grained_permissions",
      "permissions",
      "roles",
      "user_roles",
      "role_permissions",
    ]

    TABLES_BEING_MIGRATED = []

    MEMEX_TABLES = [
      "memex_projects",
      "memex_project_columns",
      "memex_project_column_values",
      "memex_project_items",
      "draft_issues",
    ]

    REPOSITORIES_TABLES = [
      "archived_assignments",
      "archived_commit_comments",
      "archived_deployment_statuses",
      "archived_deployments",
      "archived_downloads",
      "archived_issue_comments",
      "archived_issue_event_details",
      "archived_issue_events",
      "archived_issues",
      "archived_labels",
      "archived_milestones",
      "archived_protected_branches",
      "archived_pull_request_review_comments",
      "archived_pull_request_review_threads",
      "archived_pull_request_reviews",
      "archived_pull_request_reviews_review_requests",
      "archived_pull_request_revisions",
      "archived_pull_requests",
      "archived_releases",
      "archived_repositories",
      "archived_repository_checksums",
      "archived_repository_replicas",
      "archived_repository_sequences",
      "archived_repository_wikis",
      "archived_required_status_checks",
      "archived_review_request_reasons",
      "archived_review_requests",
      "assignments",
      "commit_comment_edits",
      "commit_comments",
      "commit_mentions",
      "community_profiles",
      "conversations",
      "cross_references",
      "deployment_statuses",
      "deployments",
      "downloads",
      "duplicate_issues",
      "import_item_errors",
      "import_items",
      "internal_repositories",
      "issue_blob_references",
      "issue_comment_edits",
      "issue_comments",
      "issue_edits",
      "issue_event_details",
      "issue_events",
      "issue_imports",
      "issue_links",
      "issue_priorities",
      "issues",
      "issues_labels",
      "labels",
      "languages",
      "last_seen_pull_request_revisions",
      "marketplace_categories_repository_actions",
      "milestones",
      "mirrors",
      "network_privileges",
      "package_activities",
      "package_download_activities",
      "package_files",
      "package_version_package_files",
      "package_versions",
      "page_builds",
      "page_certificates",
      "page_deployments",
      "pages",
      "pages_fileservers",
      "pages_partitions",
      "pages_replicas",
      "pages_routes",
      "protected_branches",
      "pull_request_conflicts",
      "pull_request_review_comment_edits",
      "pull_request_review_comments",
      "pull_request_review_edits",
      "pull_request_review_threads",
      "pull_request_reviews",
      "pull_request_reviews_review_requests",
      "pull_request_revisions",
      "pull_requests",
      "reactions",
      "registry_package_dependencies",
      "registry_package_files",
      "registry_package_metadata",
      "registry_package_tags",
      "registry_packages",
      "releases",
      "repositories",
      "repository_action_releases",
      "repository_actions",
      "repository_advisory_comment_edits",
      "repository_advisory_edits",
      "repository_invitations",
      "repository_licenses",
      "repository_networks",
      "repository_redirects",
      "repository_sequences",
      "repository_topics",
      "repository_transfers",
      "repository_unlocks",
      "repository_wikis",
      "required_status_checks",
      "review_dismissal_allowances",
      "review_request_reasons",
      "review_requests",
      "tabs",
      "upload_manifests",
    ]

    OUTSIDE_MAIN_CLUSTER_TABLES = MYSQL2_TABLES +
                                  NOTIFICATIONS_DELIVERIES_TABLES +
                                  NOTIFICATIONS_ENTRIES_TABLES +
                                  MYSQL5_TABLES +
                                  SPOKES_TABLES +
                                  GITBACKUPS_TABLES +
                                  COLLAB_TABLES +
                                  BALLAST_TABLES +
                                  NOTIFY_TABLES +
                                  IAM_TABLES +
                                  IAM_ABILITIES_TABLES +
                                  VT_TABLES +
                                  STRATOCASTER_TABLES +
                                  MEMEX_TABLES +
                                  REPOSITORIES_TABLES

    def self.cluster_for_table(table)
      if IAM_ABILITIES_TABLES.include?(table)
        IAM_ABILITIES
      elsif NOTIFICATIONS_DELIVERIES_TABLES.include?(table)
        NOTIFICATIONS_DELIVERIES
      elsif NOTIFICATIONS_ENTRIES_TABLES.include?(table)
        NOTIFICATIONS_ENTRIES
      elsif MYSQL2_TABLES.include?(table)
        MYSQL2
      elsif MYSQL5_TABLES.include?(table)
        MYSQL5
      elsif SPOKES_TABLES.include?(table)
        SPOKES
      elsif GITBACKUPS_TABLES.include?(table)
        GITBACKUPS
      elsif COLLAB_TABLES.include?(table)
        COLLAB
      elsif BALLAST_TABLES.include?(table)
        BALLAST
      elsif NOTIFY_TABLES.include?(table)
        NOTIFY
      elsif IAM_TABLES.include?(table)
        IAM
      elsif VT_TABLES.include?(table)
        VT
      elsif STRATOCASTER_TABLES.include?(table)
        STRATOCASTER
      elsif MEMEX_TABLES.include?(table)
        MEMEX
      elsif REPOSITORIES_TABLES.include?(table)
        REPOSITORIES
      else
        MYSQL1
      end
    end
  end
end
