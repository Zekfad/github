# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  # Helps preload ActiveRecord assocations in collections of records to
  # minimize the number of queries.
  module PrefillAssociations
    BATCH_SIZE = 100

    include Scientist
    extend self

    # Public: Preloads Bot associations.
    #
    # users - An Array of Users that maybe Bots.
    #
    # returns nothing.
    def for_bots(users)
      users.each do |user|
        next unless user.respond_to?(:integration)
        prefill_associations user, [:integration]
        prefill_belongs_to [user.integration], User, :owner_id
      end
    end

    # Public: Prefills CheckSuite associations.
    #
    # check_suites - An Array of CheckSuites
    # repo         - A target Repository for the CheckSuites
    #
    # Returns nothing.
    def for_check_suites(check_suites, repo)
      prefill_belongs_to check_suites, Push, :push_id
      prefill_belongs_to check_suites, User, :creator_id
      prefill_belongs_to check_suites, Integration, :github_app_id

      check_suites.each { |suite| suite.association(:repository).target = repo }
    end

    # Public: Prefills CheckRun associations.
    #
    # check_suites - An Array of CheckRuns
    #
    # Returns nothing.
    def for_check_runs(check_runs)
      prefill_belongs_to check_runs, User, :creator_id
      prefill_belongs_to check_runs, CheckSuite, :check_suite_id
    end

    # Public: Prefills WorkflowRun associations.
    #
    # workflow_runs - An Array of WorkflowRuns
    # repo          - A target Repository for the CheckSuites of the WorkflowRuns
    #
    # Returns nothing.
    def for_workflow_runs(workflow_runs, repo)
      # prefill_belongs_to workflow_runs, Actions::Workflow, :workflow_id

      workflow_runs.each do |workflow_run|
        prefill_associations workflow_run, [:check_suite, :repository]
        prefill_associations workflow_run.check_suite, [:github_app]
      end
    end

    # Public: Preloads IntegrationInstallation associations.
    #
    # installations - An Array of IntegrationInstallations.
    # integration   - An optional Integration. If all installations belong to the same
    #                 integration, we can pass this parameter to help with performance
    #
    # returns nothing.
    def for_integration_installations(installations, integration: nil)
      GitHub.tracer.with_span("GitHub::PrefillAssociations#for_integration_installations") do |span|
        for_installations(installations, integration: integration)
        relations = [:target, :event_records, :user_suspended_by, :version]
        prefill_associations installations, relations
      end
    end

    def for_scoped_integration_installations(installations, integration: nil)
      for_installations(installations, integration: integration)
      prefill_associations installations, [:parent, :version]

      installations.each do |installation|
        prefill_associations [installation.parent], [:target]
      end
    end

    def for_site_scoped_integration_installations(installations, integration: nil)
      for_installations(installations, integration: integration)
      prefill_associations installations, [:target, :latest_version]
    end

    # Do not use directly, it's for the for_*_installations above.
    def for_installations(installations, integration: nil)
      if integration
        installations.each { |installation| installation.association(:integration).target = integration }
      else
        prefill_belongs_to installations, Integration, :integration_id
      end
    end

    # Public: Preloads IntegrationInstallationRequest associations.
    #
    # installations - An Array of IntegrationInstallationRequest records.
    #
    # returns nothing.
    def for_integration_installation_requests(requests)
      prefill_belongs_to requests, User, :requester_id
      prefill_belongs_to requests, User, :target_id
    end

    # Public: Preloads Integration associations.
    #
    # integrations - An Array of Integrations.
    #
    # returns nothing.
    def for_integrations(integrations)
      prefill_belongs_to integrations, User, :owner_id

      integrations.each do |integration|
        prefill_associations integration, [:bot]
        prefill_associations integration, [:latest_version]
      end
    end

    # Public: Preloads Deployment associations.
    #
    # deployments - An Array of Deployments.
    # repo - A target Repository for the Deployments.
    #
    # Returns nothing.
    def for_deployments(deployments, repo = nil)
      if repo
        deployments.each { |d| d.association(:repository).target = repo }
      else
        fill_repositories deployments
      end

      prefill_belongs_to deployments, User, :creator_id
    end

    def for_organization_pending_invitations(invitations)
      prefill_associations invitations, [:inviter, :invitee, { invitee: [:profile]}]
    end

    # Public: Prefills DeploymentStatus associations.
    #
    # deployment_statuses - An Array of DeploymentStatuses
    # repo                - A target Repository for the DeploymentStatuses.
    #
    # Returns nothing.
    def for_deployment_statuses(deployment_statuses, repo = nil)
      prefill_belongs_to deployment_statuses, User, :creator_id
      prefill_belongs_to deployment_statuses, Deployment, :deployment_id

      deployments = deployment_statuses.map(&:deployment)

      if repo
        deployments.each { |d| d.association(:repository).target = repo }
      else
        fill_repositories deployments
      end
    end

    # Public: Prefills GHVFS::Replica associations.
    #
    # replicas - An Array of GHVFS::Replicas
    #
    # Returns nothing.
    def for_ghvfs_replicas(replicas)
      prefill_associations replicas, [:ghvfs_fileserver]
    end

    # Public: Prefills HookEvent associations.
    #
    # hook_events - An Array of HookEvents
    #
    # Returns nothing.
    def for_hooks(hooks)
      prefill_associations hooks, [:event_types, :config_attribute_records, :installation_target]
    end

    # Public: Preloads Project#owner
    #
    # projects - An Array of Projects
    #
    # returns nothing
    def for_projects(projects)
      projects.group_by(&:owner_type).each do |owner_type, projects_of_type|
        prefill_belongs_to projects_of_type, owner_type.constantize, :owner_id
      end
    end

    # Public: Preloads ProjectCard#creator
    #
    # cards - An Array of ProjectCards
    #
    # returns nothing
    def for_project_cards(cards)
      prefill_belongs_to cards.reject(&:redacted?), User, :creator_id
    end

    def for_uploadables(uploadables)
      return unless uploadables.first.respond_to?(:package_version)

      prefill_associations uploadables, [:package_version]
    end

    # Public: Preloads Issue#user, Issue#milestone, Issue#assignee, and Issue#labels.
    #
    # issues - An Array of Issues.
    #
    # Optional keyword args to configure what associations to load.
    #     repository: - Repository instance to preload on all issues.
    #
    # Returns nothing.
    PREFILLS_FOR_ISSUES = [:conversation, :labels, :assignees, :assignments, :pull_request, :milestones, :comment_counts, :close_issue_references]
    def for_issues(issues, repository: nil, only_prefill: PREFILLS_FOR_ISSUES, exclude_prefills: [], current_user: nil)
      only_prefill = only_prefill.map(&:to_sym) - exclude_prefills.map(&:to_sym)

      # Always prefill user and repository
      if repository
        issues.each { |i| i.association(:repository).target = repository }
      else
        fill_repositories issues
      end
      user_ids = prefillable_ids(issues, :user_id)
      user_ids.concat prefillable_ids(issues, :assignee_id) if only_prefill.include?(:assignees)

      prefill_associations issues, [:conversation, :labels, :assignees, :assignments] & only_prefill

      if only_prefill.include?(:pull_request)
        prefill_belongs_to issues, PullRequest, :pull_request_id
      end

      all_milestones = []
      if only_prefill.include?(:milestones)
        # find and prefill all milestones.
        milestone_ids = prefillable_ids(issues, :milestone_id)
        milestone_map = prefillable_ids_to_map(milestone_ids, Milestone)
        prefill_with_map(issues, milestone_map, :milestone_id)

        # Prefill all milestone creators
        milestone_map.each_value do |milestone|
          all_milestones << milestone
          user_ids << milestone.created_by_id
        end

        prefill_belongs_to all_milestones, Repository, :repository_id
      end

      # prefill the user records
      user_map = prefillable_ids_to_map(user_ids, User)
      prefill_with_map all_milestones, user_map, :created_by_id
      for_profiles(user_map.values, user_ids)
      user_keys = [:user_id].tap do |keys|
        keys << :assignee_id if only_prefill.include?(:assignees)
      end
      prefill_with_map(issues, user_map, *user_keys)

      prefill_comment_counts(issues) if only_prefill.include?(:comment_counts)
      prefill_close_issue_reference_counts(issues, current_user) if only_prefill.include?(:close_issue_references)
    end

    # Public: Preloads IssueComment#issue, IssueComment#user,
    # and IssueComment#repository.
    #
    # comments - An Array of Comments.
    #
    # Returns nothing.
    def for_issue_comments(comments)
      prefill_associations comments, [:issue, :user, :performed_via_integration, :repository, { issue: [:pull_request, :repository] }]
    end

    # Public: Preloads conversations and labels for the given Issues
    # in a single query.
    #
    # issues - An Array of Issues.
    #
    # Returns nothing.
    def for_issue_assignees(issues)
      prefill_associations issues, [:assignee, :conversation, :labels]
    end

    # Public: Preloads ImportItem#import_errors.
    #
    # comments - An Array of ImportItem.
    #
    # Returns nothing.
    def for_issue_imports(issue_imports)
      prefill_associations issue_imports, [:import_errors]
    end

    # Public: Preloads the commit authors in push events.
    #
    # events - an Array of Stratocaster::Events
    #
    # Returns nothing.
    def for_stratocaster_event_commit_authors(events)
      events_by_emails = events.select(&:push_event?).group_by(&:commit_author_emails)
      emails = events_by_emails.keys.flatten.uniq

      users_by_email = {}
      emails.each_slice(BATCH_SIZE) do |email_batch|
        users = User.find_by_emails(email_batch)
        users.each do |(email, user)|
          users_by_email[email.downcase] = user
        end
      end

      events_by_emails.each do |emails, email_events|
        emails.each do |email|
          user = users_by_email[email]
          next unless user

          email_events.each { |event| event.set_commit_author_for_email(email, user) }
        end
      end

    end

    # Public: Preloads repos associated with Stratocaster::Event objects.
    #
    # events - an Array of Stratocaster::Events
    #
    # Returns nothing.
    def for_stratocaster_event_repos(events)
      events_by_repo_id = events.group_by(&:repo_id)
      repo_ids = events_by_repo_id.keys.compact
      repos_by_id = {}

      repo_ids.each_slice(BATCH_SIZE) do |repo_id_batch|
        repos = Repository.includes(:owner, :organization, :primary_language).where(id: repo_id_batch)
        repos.each { |repo| repos_by_id[repo.id] = repo }
      end

      events_by_repo_id.each do |repo_id, repo_events|
        next unless repo_id

        repo = repos_by_id[repo_id]
        next unless repo

        repo_events.each { |event| event.repo = repo }
      end
    end

    # Public: Preloads the user who created the event in Stratocaster::Event objects.
    #
    # events - an Array of Stratocaster::Events
    #
    # Returns nothing.
    def for_stratocaster_event_senders(events)
      events_by_sender_id = events.group_by(&:sender_id)
      user_ids = events_by_sender_id.keys.compact

      users_by_id = {}
      user_ids.each_slice(BATCH_SIZE) do |user_id_batch|
        users = User.where(id: user_id_batch).
          includes(:profile) # used in Event#sender_name
        users.each { |user| users_by_id[user.id] = user }
      end

      events_by_sender_id.each do |sender_id, sender_events|
        next unless sender_id

        user = users_by_id[sender_id]
        next unless user

        sender_events.each { |event| event.sender = user }
      end
    end

    # Public: Preloads #issue and #repository associations in
    # IssueEvent objects.
    #
    # Also does further processing on specific kinds of issue events
    #
    # events - an Array of IssueEvents
    #
    # Returns nothing
    def for_issue_events(events)
      prefill_belongs_to events, Issue, :issue_id
      prefill_belongs_to events, Repository, :repository_id
      prefill_belongs_to events, User, :actor_id
      prefill_associations events, [:performed_via_integration]

      prefill_milestones(events)
      prefill_project_events events

      commit_events = events.select(&:commit_id?) + events.select(&:force_push?)
      for_issue_commit_references(commit_events)
    end

    # Public: Preloads #issue and #repository associations in IssueEvent
    # objects, as well as IssueComment, CrossReference, and Commit objects
    # on an Issue or PullRequest timeline.
    def for_issue_timeline(timeline)
      grouped = timeline.group_by do |item|
        case item
        when Platform::Models::PullRequestCommit
          :commits
        when CrossReference
          :cross_references
        when IssueComment
          :issue_comments
        when CommitCommentThread
          :commit_comment_threads
        when PullRequestReview
          :reviews
        when DeprecatedPullRequestReviewThread
          :review_threads
        when PullRequestRevisionMarker
          :pull_request_review_marker
        else
          :events
        end
      end

      for_issue_events(grouped[:events]) if grouped[:events]

      if grouped[:commits]
        Commit.prefill_verified_signature(grouped[:commits].map(&:commit))

        # Do we need this? PullRequest#changed_commits already prefills users
        Commit.prefill_users(grouped[:commits].map(&:commit))
      end

      if grouped[:cross_references]
        prefill_belongs_to grouped[:cross_references], Issue, :source_id
        prefill_belongs_to grouped[:cross_references], User, :actor_id
        prefill_belongs_to grouped[:cross_references].map(&:source), Repository, :repository_id
      end

      if grouped[:issue_comments]
        for_issue_comments grouped[:issue_comments]
      end

      if grouped[:reviews]
        for_pull_request_reviews grouped[:reviews], include_user_content_edit: false
      end

      if grouped[:review_threads]
        grouped[:review_threads].each do |thread|
          for_review_comments(thread.comments, issue: thread.pull.issue, repository: thread.repository)
        end
      end
    end

    # Public: Prefills associations needed for rendering PullRequestReviews.
    #
    # reviews                   - Array of PullRequestReview
    # include_user_content_edit - Boolean indicating whether to prefill
    #                             PullRequestReview#user_content_edit (and the
    #                             edit's editor)
    def for_pull_request_reviews(reviews, include_user_content_edit:)
      prefill_belongs_to reviews, User, :user_id
      prefill_belongs_to reviews, PullRequest, :pull_request_id
      prefill_associations reviews, [:repository]
      if include_user_content_edit
        prefill_associations reviews, { latest_user_content_edit: :editor }
      end
    end

    # Internal: Preloads #commit associations (and that commit's commit_comment)
    # in IssueEvent objects.
    #
    # events - an Array of IssueEvents, which all should be for commits
    #          (i.e. they have `commit_id` set)
    #
    # Returns nothing
    def for_issue_commit_references(events)
      prefill_belongs_to events, Repository, :commit_repository_id

      events.group_by(&:repository_for_commit).each do |repo, repo_events|
        repo_oids = repo_events.collect(&:commit_id).uniq.compact
        commits   = repo.objects.read_all(repo_oids, "commit", skip_bad: true)  # some commits may be gone, GCed

        Commit.prefill_comment_counts(commits, repo)
        commits   = commits.index_by(&:oid)

        repo_events.each do |event|
          event.commit = commits[event.commit_id]
        end
      end
    end

    # Public: Preloads conversations and labels for the given Issues
    # in a single query.
    #
    # issues - An Array of Issues.
    #
    # Returns nothing.
    def for_issue_search(issues)
      prefill_associations issues, [:conversation, :labels]
    end

    # Public: Preloads owner and network for the given Repositories
    # in a single query.
    #
    # issues - An Array of Repositories.
    #
    # Returns nothing.
    def for_repo_search(repos)
      prefill_associations repos, [:owner, :network]
    end

    # Public: Preloads Milestone#user and Milestone#repository.
    #
    # milestones - An Array of Milestones.
    # repo       - Repository instance to preload.
    #
    # Returns nothing.
    def for_milestones(milestones, repo)
      milestones.each { |m| m.association(:repository).target = repo }
      prefill_belongs_to milestones, User, :created_by_id
    end

    # Public: Preload ProtectedBranch records for this list of branches
    #
    # repository - The repository the branches belong to
    # branches   - The list of Ref branches to prefill
    #
    # Returns nothing
    def for_branches(repository, branches)
      branches_by_name = branches.index_by(&:name)

      ProtectedBranch.for_repository_with_branch_names(repository, branches_by_name.keys).each do |branch_name, protected_branch|
        branch = branches_by_name[branch_name]
        branch.protected_branch = protected_branch
      end
    end

    # Public: Preload RequiredStatusCheck records for protected branches
    #
    # protected_branches - The list of protected branches
    #
    # Returns nothing
    def for_protected_branches(protected_branches)
      prefill_associations protected_branches, [:repository, :required_status_checks, :review_dismissal_allowances]
    end

    # Public: Preload ProtectedBranch records for pull requests
    #
    # pulls - The list of pull requests
    #
    # Returns nothing
    def for_pulls_protected_branches(repository, pulls)
      ref_names = pulls.map(&:base_ref_name).uniq
      protected_branches = ProtectedBranch.for_repository_with_branch_names(repository, ref_names)

      pulls.each do |pull|
        pull.protected_base_branch = protected_branches[pull.base_ref_name]
      end
    end

    def for_review_requests(pulls)
      pulls.each do |pull|
        pull.pending_review_requests
      end
    end

    # Public: Preloads PullRequest repository and user associations.
    #
    # pulls   - An Array of PullRequest objects.
    #
    # Optional keyword args to configure what associations to load.
    #     mirror: - Boolean to load Repository Mirror objects.
    #               Default: false.
    #     issues: - Array of Issue objects to bypass the Issue load.
    #
    # Returns nothing.
    def for_pulls(pulls, mirror: false, issues: nil)
      # Repositories

      repos = []
      prefill_belongs_to pulls, Repository,
          :repository_id, :base_repository_id, :head_repository_id do |repositories|
        repos = repositories
      end
      # We'll prefill repo owners later.
      for_repositories(repos, mirror: mirror, owner: false)

      # Reviewers

      prefill_associations pulls, [review_requests: :reviewer]

      # Issues

      issue_map = if issues
        issues.index_by { |i| i.pull_request_id }
      else
        pull_ids = prefillable_ids pulls
        prefillable_ids_to_map pull_ids, Issue, :pull_request_id
      end
      issues ||= issue_map.values

      prefill_associations issues, [:assignees, :conversation]

      pulls.each do |pull|
        issue = issue_map[pull.id]
        issue.association(:repository).target = pull.repository if issue
        pull.association(:issue).target = issue
      end

      # Users

      user_ids = prefillable_ids(pulls, :user_id, :base_user_id, :head_user_id)
      user_ids.concat(prefillable_ids(repos, :owner_id, :organization_id))
      user_ids.concat(prefillable_ids(issues, :user_id))
      user_ids.uniq!
      user_map = prefillable_ids_to_map(user_ids, User)
      prefill_with_map(pulls, user_map, :user_id, :base_user_id, :head_user_id)
      prefill_with_map(repos, user_map, :owner_id, :organization_id)
      prefill_with_map(issues, user_map, :user_id)
    end

    # Public: Preloads #pull_request and #user associations in
    # PullRequestReviewComment objects.
    #
    # comments - An Array of PullRequestReviewComment objects.
    #
    # Optional keyword args to configure what assocations to load.
    #     issue: - Issue to prevent re-fetching the Issue
    #              when preloading PullRequest associations.
    #     repository: - Repository to prevent re-fetching
    #
    # Returns nothing.
    def for_review_comments(comments, issue: nil, repository: nil)
      prefill_belongs_to comments, User, :user_id

      if repository
        comments.each { |c| c.association(:repository).target = repository }
      else
        fill_repositories comments
      end

      prefill_associations comments, latest_user_content_edit: :editor
      prefill_belongs_to comments, PullRequest, :pull_request_id do |pulls|
        if issue
          for_pulls(pulls, issues: [issue])
        else
          for_pulls(pulls)
        end
      end
    end

    # Public: Preloads #invitee, #inviter, and #repository associations in
    # RepositoryInvitation objects.
    #
    # repository_invitations - An Array of RepositoryInvitation objects.
    #
    # Returns nothing.
    def for_repository_invitations(repository_invitations)
      prefill_belongs_to repository_invitations, User, :invitee_id
      prefill_belongs_to repository_invitations, User, :inviter_id
      prefill_belongs_to repository_invitations, Repository, :repository_id
      prefill_belongs_to repository_invitations, Role, :role_id
    end

    # Public: Preloads repositories for Stars
    #
    # stars - An Array of Starrables
    #
    # Returns nothing.
    def for_stars(stars)
      stars.group_by(&:starrable_type).each do |starrable_type, starrables|
        klass = Kernel.const_get(starrable_type)
        prefill_belongs_to starrables, klass, :starrable_id do |starred|
          for_repositories(starred) if starrable_type == "Repository"
        end
      end
    end

    # Public: Preloads Repository#owner, Repository#open_issues_count,
    #         Repository#all_forks_count
    #
    # repos    - An Array of Repositories.
    # mirror   - Boolean whether to prefill Repository#mirror (default: true)
    # owner    - Boolean whether to prefill Repository#owner and
    #            Repository#organization (default: true)
    # template - Boolean whether to prefill Repository#template_repository (default: false)
    #
    # Returns nothing.
    def for_repositories(repos, mirror: true, owner: true, template: false, internal: false)
      return repos if repos.empty?
      repo_ids = prefillable_ids repos

      prefill_repository_owners(repos) if owner
      fill_mirrors(repos, repo_ids) if mirror
      prefill_associations repos, [:network, :page, :parent]
      open_issues_for_repositories(repos, repo_ids)
      fork_count_for_repositories(repos)
      topic_names_for_repositories(repos)
      template_repository_for_repositories(repos) if template
      prefill_internal_repos(repos) if internal

      repos
    end

    def prefill_repository_owners(repos)
      prefill_belongs_to(repos, User, :owner_id, :organization_id)
    end

    def prefill_internal_repos(repos)
      prefill_associations(repos, [:internal_repository])
    end

    # Public: Preloads org business association.
    #
    # org - An Organization
    #
    # Returns nothing.
    #
    # To look up configuration data (e.g. `Organization#organization_projects_enabled?`) on an
    # enterpise org, the business association must be loaded to find parent enterprise config
    # values. Preload it by calling this prior to `deliver` to prevent excessive association loaded
    # test failures from Api::Serializer.
    def for_enterprise_configuration(org)
      org.business
      nil
    end

    # Public: Preloads associations for a user
    #
    # users - An Array of Users.
    #
    # Returns nothing.
    def for_users(users)
      # If there are no users, there's no sense in prefilling any associations
      return if users.empty?

      prefill_associations users, [:profile]
    end

    # Public: Preloads associations for a user's repositories.
    #
    # users - An Array of Users. Ensure the type is homogenous: mixed arrays of users and orgs will yield inaccurate counts.
    #
    # Returns nothing.
    def for_user_repository_counts(users)
      # If there are no users, there's no sense in prefilling any associations
      return if users.empty?

      # If there is only one user being returned, prefilling the associations
      # will result in more queries than it would take to determine the counts
      # through normal means.
      return if users.count == 1

      user_ids = users.map(&:id)

      public_repositories_hash = users.first.public_repositories.where_values_hash.merge(owner_id: user_ids)
      public_repositories_count = Repository.where(public_repositories_hash).group(:owner_id).count

      private_repositories_hash = users.first.private_repositories.where_values_hash.merge(owner_id: user_ids)
      private_repositories_count = if users.first.organization?
        # Internal and advisory workspace repos will be counted among an org's private repos unless we explicitly exclude them.
        biz_ids = Business::OrganizationMembership.where("organization_id IN (?)", user_ids).pluck(:business_id)
        workspace_repo_ids = RepositoryAdvisory.where(owner_id: user_ids).where.not(workspace_repository_id: nil).pluck(:workspace_repository_id)
        private_repositories = Repository.where(private_repositories_hash).without_ids(InternalRepository.select(:repository_id).with_ids(biz_ids, field: "business_id"))
        private_repositories = private_repositories.without_ids(workspace_repo_ids) if workspace_repo_ids.any?
        private_repositories.group(:owner_id).count
      else
        # Individuals cannot own internal or advisory workspace repos so we need not exclude them from user counts.
        Repository.where(private_repositories_hash).group(:owner_id).count
      end

      owned_private_repositories_hash = users.first.owned_private_repositories.where_values_hash.merge(owner_id: user_ids)
      owned_private_repositories_count = Repository.where(owned_private_repositories_hash).group(:owner_id).count

      public_gists_count = Gist.where(public: true, user_id: user_ids).alive.group(:user_id).count
      private_gists_count = Gist.where(public: false, user_id: user_ids).alive.group(:user_id).count

      users.each do |user|
        repository_counts = UserRepositoryCounts.new(user)
        repository_counts.public_repositories = public_repositories_count[user.id] || 0
        repository_counts.private_repositories = private_repositories_count[user.id] || 0
        repository_counts.owned_private_repositories = owned_private_repositories_count[user.id] || 0

        repository_counts.public_gists = public_gists_count[user.id] || 0
        repository_counts.private_gists = private_gists_count[user.id] || 0
        user.repository_counts = repository_counts
      end
    end

    # Public: Preloads Team#organization, Team#organization.profile
    #
    # teams - An Array of Teams.
    #
    # Returns nothing.
    def for_teams(teams)
      prefill_belongs_to teams, Organization, :organization_id
      for_profiles(teams.map(&:organization))
      if GitHub.enterprise?
        fill_ldap_mapping teams
      end
    end

    # Public: Prefills LDAP mappings.
    #
    # commit_statuses - An Array of users.
    #
    # Returns nothing.
    def fill_ldap_mapping(teams)
      prefill_associations teams, :ldap_mapping
    end

    # Public: Preloads Avatar#asset and Avatar#storage_blob.
    #
    # avatars - An Array of Avatar records.
    #
    # Returns nothing.
    def for_avatars(avatars)
      prefill_associations avatars, [:asset, :storage_blob]
    end

    # Public: Preloads Media::Blob#storage_blob.
    #
    # media_blobs - An Array of Media::Blob records.
    #
    # Returns nothing.
    def for_media_blobs(media_blobs)
      prefill_associations media_blobs, [:asset, :storage_blob]
    end

    # Public: Preloads UserAsset#storage_blob.
    #
    # user_assets - An Array of UserAsset records.
    #
    # Returns nothing.
    def for_user_assets(user_assets)
      prefill_associations user_assets, [:storage_blob]
    end

    # Public: Preloads RepositoryFile#storage_blob.
    #
    # media_blobs - An Array of RepositoryFile records.
    #
    # Returns nothing.
    def for_repo_files(repo_files)
      prefill_associations repo_files, [:storage_blob]
    end

    # Public: Preloads Marketplace::ListingScreenshot#storage_blob and #listing.
    #
    # screenshots - An Array of Marketplace::ListingScreenshot records.
    #
    # Returns nothing.
    def for_marketplace_listing_screenshots(screenshots)
      prefill_associations screenshots, [:storage_blob, :listing]
    end

    # Public: Preloads Marketplace::ListingImage#storage_blob and #listing.
    #
    # images - An Array of Marketplace::ListingImage records.
    #
    # Returns nothing.
    def for_marketplace_listing_images(images)
      prefill_associations images, [:storage_blob, :listing]
    end

    # Public: Preloads RepositoryImage#storage_blob and #repository.
    #
    # repo_images - An Array of RepositoryImage records.
    #
    # Returns nothing.
    def for_repository_images(repo_images)
      prefill_associations repo_images, [:storage_blob, :repository]
    end

    # Public: Preloads OauthApplicationLogo#storage_blob.
    #
    # media_blobs - An Array of OauthApplicationLogo records.
    #
    # Returns nothing.
    def for_oauth_logos(oauth_logos)
      prefill_associations oauth_logos, [:storage_blob]
    end

    # Public: Preloads ReleaseAsset#storage_blob.
    #
    # user_assets - An Array of ReleaseAsset records.
    #
    # Returns nothing.
    def for_release_assets(release_assets)
      prefill_associations release_assets, [:release, :repository, :storage_blob]
    end

    # Public: Preloads Registry::File#storage_blob.
    #
    # package_files - An Array of Registry::File records.
    #
    # Returns nothing.
    def for_package_files(package_files)
      prefill_associations package_files, [:storage_blob]
    end

    # Public: Preloads UploadManifestFile#storage_blob.
    #
    # media_blobs - An Array of Media::Blob records.
    #
    # Returns nothing.
    def for_upload_manifest_files(files)
      prefill_associations files, [:storage_blob]
    end

    # Public: Preloads AuditLogEntry#user and AuditLogEntry#actor
    #
    # Returns Array of AuditLogEntrys
    def for_audit_log(entries)
      subject_ids = prefillable_ids(entries, :user_id, :actor_id)
      subject_map = prefillable_ids_to_map(subject_ids, User)

      marketplace_listing_ids = prefillable_ids(entries, :marketplace_listing_id)
      marketplace_listing_map = prefillable_ids_to_map(marketplace_listing_ids,
                                                       Marketplace::Listing)

      org_ids = prefillable_ids(entries, :org_id)
      org_map = prefillable_ids_to_map(org_ids, Organization)

      project_ids = prefillable_ids(entries, :project_id)
      project_map = prefillable_ids_to_map(project_ids, Project)

      issue_comment_ids = prefillable_ids(entries, :issue_comment_id)
      issue_comment_map = prefillable_ids_to_map(issue_comment_ids, IssueComment)

      discussion_post_ids = prefillable_ids(entries, :discussion_post_id)
      discussion_post_map = prefillable_ids_to_map(discussion_post_ids, DiscussionPost)

      discussion_post_reply_ids = prefillable_ids(entries, :discussion_post_reply_id)
      discussion_post_reply_map = prefillable_ids_to_map(
        discussion_post_reply_ids,
        DiscussionPostReply)

      entries.each do |entry|
        entry.user = subject_map[entry.user_id]
        entry.actor = subject_map[entry.actor_id]
        entry.org = org_map[entry.org_id]
        entry.issue_comment = issue_comment_map[entry.issue_comment_id]
        entry.marketplace_listing = marketplace_listing_map[entry.marketplace_listing_id]
        entry.project = project_map[entry.project_id]
        entry.discussion_post = discussion_post_map[entry.discussion_post_id]
        entry.discussion_post_reply = discussion_post_reply_map[entry.discussion_post_reply_id]
      end

      profiles = (entries.map(&:user) + entries.map(&:org) + entries.map(&:actor)).compact
      prefill_associations(profiles, :profile)

      entries
    end

    # Public: Preloads associations for ExternalIdentity
    # objects.
    #
    # external_identities - An Array of ExternalIdentity objects.
    #
    # Returns nothing.
    def for_external_identities(external_identities)
      prefill_associations external_identities, [
        :identity_attribute_records,
        :user,
        { organization_invitation: :teams },
        { provider: :target },
      ]

      external_identities.each do |identity|
        next unless identity.user&.organization?
        user_external_identities = identity.provider.
          external_identities_for_organization(identity.user).
          provisioned_by(:scim).
          includes(:identity_attribute_records)

        identity.prefilled_group_members = user_external_identities
      end

      external_identities
    end

    # Public: Preloads associations for an Organization ExternalIdentity and
    # the ExternalIdentity's of its members
    #
    # external_identity - ExternalIdentity objects for the Organziation
    #
    # Returns nothing.
    def for_group_external_identity(external_identity)
      prefill_associations [external_identity], [
        :identity_attribute_records,
        :user,
        { organization_invitation: :teams },
        { provider: :target },
      ]

      if external_identity.user&.organization?
        member_identities = external_identity.provider.
          external_identities_for_organization(external_identity.user).
          provisioned_by(:scim).
          includes(:identity_attribute_records)

        external_identity.prefilled_group_members = member_identities
      end
    end

    # Public: Preloads Repository#open_issues_count
    #
    # repos    - An Array of Repositories.
    # repo_ids - Optional Array of the Repo IDs if you have them handy.
    #
    # Returns nothing.
    def open_issues_for_repositories(repos, repo_ids = nil)
      return if repos.empty?
      repo_ids ||= prefillable_ids repos
      counts = Issue.not_spammy.where(repository_id: repo_ids, state: "open").group(:repository_id).count
      repos.each do |repo|
        repo.open_issues_count = counts[repo.id].to_i
      end
    end

    # Public: Preloads Repository#all_forks_count
    #
    # repos    - An Array of Repositories.
    #
    # Returns nothing.
    def fork_count_for_repositories(repos)
      return if repos.empty?

      repo_ids = repos.select { |r| r.private? }.map { |r| r.id }
      counts =
        if repo_ids.any?
          Repository.where(parent_id: repo_ids).group(:parent_id).count
        else
          {}
        end

      repos.each do |repo|
        repo.all_forks_count = counts[repo.id].to_i
      end
    end

    # Public: Preloads Repository#template_repository_clone and #template_repository.
    #
    # repos - An Array of Repositories.
    #
    # Returns nothing.
    def template_repository_for_repositories(repos)
      return if repos.empty?

      repo_ids = repos.map(&:id)
      repo_clones_by_repo_id = RepositoryClone.includes(:template_repository).
        where(clone_repository_id: repo_ids).
        map { |repo_clone| [repo_clone.clone_repository_id, repo_clone] }.to_h

      repos.each do |repo|
        if repo_clone = repo_clones_by_repo_id[repo.id]
          repo.template_repository = repo_clone.template_repository
        end
      end
    end

    # Public: Preloads Repository#topic_names
    #
    # repos - An Array of Repositories.
    #
    # Returns nothing.
    def topic_names_for_repositories(repos)
      return if repos.empty?
      repo_ids = repos.map(&:id)

      names_by_repo_id = Hash.new { |h, k| h[k] = [] }
      id_pairs = RepositoryTopic.applied_topic_ids_by_repository(repository_ids: repo_ids).
        pluck(:repository_id, :topic_id)
      topic_ids = id_pairs.map(&:last).uniq

      topic_names_by_id = Hash[Topic.where(id: topic_ids).pluck(:id, :name)]

      id_pairs.each do |repo_id, topic_id|
        names_by_repo_id[repo_id] << topic_names_by_id[topic_id]
      end

      names_by_repo_id.values.each(&:sort!)

      repos.each do |repo|
        repo.topic_names = names_by_repo_id[repo.id]
      end
    end

    # Public: Preloads Repository#repository_license
    #
    # repos    - An Array of Repositories.
    #
    # Returns nothing.
    def fill_licenses(repos)
      repo_ids = prefillable_ids repos
      licenses = prefillable_ids_to_map repo_ids, RepositoryLicense, :repository_id
      repos.each { |repo| repo.association(:repository_license).target = licenses[repo.id] }
    end

    # Public: Preloads Repository#mirror
    #
    # repos    - An Array of Repositories.
    # repo_ids - Optional Array of the Repo IDs if you have them handy.
    #
    # Returns nothing.
    def fill_mirrors(repos, repo_ids = nil)
      repo_ids ||= prefillable_ids repos
      mirror_map = prefillable_ids_to_map repo_ids, Mirror, :repository_id
      repos.each do |repo|
        repo.association(:mirror).target = mirror_map[repo.id]
      end
    end

    # Public: Prefills commit statuses.
    #
    # commit_statuses - An Array of commit statuses.
    #
    # Returns nothing.
    def for_commit_statuses(commit_statuses)
      fill_repositories(commit_statuses)
      prefill_belongs_to commit_statuses, User, :creator_id
      prefill_belongs_to commit_statuses, OauthApplication, :oauth_application_id
    end

    # Public: Prefills repositories for the given records.
    #
    # records - An Array of ActiveRecord::Base models with a repository
    # association.
    #
    # Returns nothing.
    def fill_repositories(records)
      prefill_belongs_to records, Repository, :repository_id
    end

    # Public: Prefills gists for the given records.
    #
    # records - An Array of ActiveRecord::Base models with a gist
    # association.
    #
    # Returns nothing.
    def for_gists(records, prefill_forks: true, prefill_parent: false)
      prefill_belongs_to records, User, :user_id
      gist_ids = records.map(&:id)

      comment_counts =
        if gist_ids.any?
          GistComment.where(gist_id: gist_ids).group(:gist_id).count
        else
          {}
        end

      records.each do |gist|
        gist.comment_count = comment_counts[gist.id] || 0
        for_gists(gist.visible_forks) if prefill_forks
        for_gists([gist.parent]) if prefill_parent && gist.fork? && !gist.parent.nil?
      end
    end

    # Public: Prefills gist comments.
    #
    # comments - An Array of gist comments.
    # gist - Gist instance to preload.
    #
    # Returns nothing.
    def for_gist_comments(comments, gist)
      comments.each { |comment| comment.association(:gist).target = gist }
      prefill_associations comments, :user
      prefill_associations comments, { latest_user_content_edit: :editor }
    end

    # Public: Prefills migrations.
    #
    # comments - An Array of migrations.
    #
    # Returns nothing.
    def for_migrations(migrations)
      prefill_belongs_to migrations, User, :owner_id
      for_repositories migrations.map(&:repositories).flatten
      prefill_associations migrations, :file
    end

    # Public: Preloads Repository#page
    #
    # repos - An Array of Repositories.
    #
    # Returns nothing.
    def fill_pages(repos)
      repo_ids = prefillable_ids repos
      pages    = prefillable_ids_to_map repo_ids, Page, :repository_id
      repos.each { |repo| repo.association(:page).target = pages[repo.id] }
    end

    # Public: Preloads UserEmail#email_roles
    #
    # emails - An Array of emails
    # full   - Optional: prefill the user for each email. Default: False.
    #
    # Returns nothing.
    def fill_email_roles(emails, full: false)
      email_ids      = prefillable_ids emails
      email_roles    = prefillable_has_many_ids_to_map email_ids, EmailRole, :email_id
      emails.each { |email| email.association(:email_roles).target = email_roles[email.id] || [] }

      prefill_belongs_to emails, User, :user_id if full
    end

    # Public: Preloads emails associated with GPGKeys
    #
    # records - An Array of GPG keys
    #
    # Returns nothing.
    def fill_gpg_emails(keys)
      prefill_associations keys, :emails
    end

    # Public: Preloads CommitComment#user, CommitComment#repository
    #
    # comments - An Array of CommitComments.
    #
    # Optional keyword args to configure what assocations to load.
    #     repository: - Repository instance to preload on all comments.
    #
    # Returns nothing.
    def for_comments(comments, repository: nil)
      prefill_belongs_to comments, User, :user_id

      if repository
        comments.each { |c| c.association(:repository).target = repository }
      else
        fill_repositories comments
      end

      prefill_associations comments, { latest_user_content_edit: :editor }
      prefill_associations comments.map(&:user).compact, :profile
    end

    # Public: Preloads the Profiles for the given Users in a single query.
    #
    # users    - An Array of Users that gets flattened.
    # user_ids - Optional Array of the User IDs if you have them handy.
    #
    # Returns nothing.
    def for_profiles(users, user_ids = nil)
      users = users.to_a.flatten.select { |u| !u.association(:profile).loaded? }
      return if users.empty?

      user_ids ||= prefillable_ids(users)
      profiles = Profile.where(user_id: user_ids).order("updated_at").index_by { |p| p.user_id }

      users.each do |user|
        user.association(:profile).target = profiles[user.id]
      end
    end

    # Public: Preloads the UserStatuses and any associated Organizations for the given Users in a
    # single query.
    #
    # users - An Array of Users.
    #
    # Returns nothing.
    def for_user_statuses(users)
      users_to_load_status = users.select { |u| !u.association(:user_status).loaded? }
      return if users_to_load_status.empty?

      user_ids = prefillable_ids(users_to_load_status)
      statuses = UserStatus.includes(:organization).where(user_id: user_ids).index_by(&:user_id)

      users_to_load_status.each do |user|
        user.association(:user_status).target = statuses[user.id]
      end
    end

    # Public: Preloads the TwoFactorCredentials for the given Users in a single
    # query.
    #
    # users - An Array of Users that gets flattened.
    #
    # Returns nothing.
    def for_two_factor_credentials(users)
      users = users.to_a.flatten.
        select { |u| !u.association(:two_factor_credential).loaded? }
      return if users.empty?

      user_ids ||= prefillable_ids(users)
      two_factor_creds = TwoFactorCredential.where(user_id: user_ids).
        index_by(&:user_id)

      users.each do |user|
        user.association(:two_factor_credential).target =
          two_factor_creds[user.id]
      end
    end

    # Public: Preloads the Logos for the given OauthApplications in a single query.
    #
    # applications - An Array of OauthApplications.
    #
    # Returns nothing.
    def for_application_logos(applications)
      applications = applications.flatten.select { |a| !a.association(:logo).loaded? }
      return if applications.empty?

      logo_ids = prefillable_ids(applications, :logo_id)
      logos = OauthApplicationLogo.where(id: logo_ids).index_by(&:id)
      applications.each do |application|
        application.logo = logos[application.logo_id]
      end
    end

    # Public: Preloads StaffNotes#User
    #
    # repos    - An Array of StaffNotes.
    #
    # Returns nothing.
    def for_notes(notes)
      prefill_associations notes, :user
    end

    # Public: Preloads CouponRedemption#coupon
    #
    # repos    - An Array of CouponRedemptions.
    #
    # Returns nothing.
    def for_coupons(coupon_redemptions)
      prefill_associations coupon_redemptions, :coupon
    end

    # This prefills associations that are on or nested within a MemexProjectItem so that we can
    # serialize the given list of memex item efficiently.
    #
    # items - Array<MemexProjectItem>, the items to prefill associations on.
    # columns - Array<MemexProjectColumn>, the columns that the caller intends to serialize. Where
    #   possible, we'll avoid prefilling some associations if we know the caller doesn't need them.
    #
    # Returns nothing.
    def for_memex_project_items(items, columns: [])
      # First prefill the top-level content associations.
      prefill_associations(items, [:content])

      # Then fill in associations for user-defined column values.
      prefill_associations(items, [:memex_project_column_values]) if columns.any?(&:user_defined?)

      # Then collect all the pull requests and issues in separate arrays.
      pull_requests_by_id = {}
      issues = []
      items.each do |i|
        case i.content
        when Issue
          issues << i.content
        when PullRequest
          pull_requests_by_id[i.content_id] = i.content
        end
      end
      pull_requests = pull_requests_by_id.values

      # Load all the issues associated with a pull request.
      pull_request_issues_by_pull_request_id = prefillable_ids_to_map(
        prefillable_ids(pull_requests),
        Issue,
        :pull_request_id
      )

      # Add all the pull request issues to collection of all issues.
      issues += pull_request_issues_by_pull_request_id.values

      # Prefill labels.
      labels = if columns.find { |c| c.name == MemexProjectColumn::LABELS_COLUMN_NAME }
        prefill_associations(issues, [:labels])
        result = issues.map(&:labels).flatten

        # Prefill HTML label names efficiently from memcache.
        Promise.all(result.map(&:async_name_html)).sync

        result
      else
        []
      end

      # Prefill milestones.
      milestones = if columns.find { |c| c.name == MemexProjectColumn::MILESTONE_COLUMN_NAME }
        prefill_associations(issues, [:milestone])
        issues.map(&:milestone).compact
      else
        []
      end

      # Prefill all repositories.
      fill_repositories(issues + pull_requests + labels + milestones)
      repositories = (issues + pull_requests).map(&:repository).uniq
      prefill_repository_owners(repositories)

      # Prefill assignees.
      if columns.find { |c| c.name == MemexProjectColumn::ASSIGNEES_COLUMN_NAME }
        prefill_associations(issues, [:assignees])

        # Prefill the primary_avatar relation on all assignees.
        users = issues.map(&:assignees).flatten
        primary_avatars_by_user_id = PrimaryAvatar
          .where(owner_id: users.map(&:id), owner_type: "User")
          .index_by(&:owner_id)
        users.each { |u| u.primary_avatar = primary_avatars_by_user_id[u.id] }
      end

      # Set the issue association on each pull request to the issues we've just
      # built up (that have all the prefilled data).
      pull_request_issues_by_pull_request_id.each do |pull_id, issue|
        issue.pull_request = pull_requests_by_id[pull_id]
      end

      nil
    end

    # Preload comment counts for issues and if applicable, the associated
    # pull requests
    def prefill_comment_counts(issues)
      return if issues.empty?

      map = issues.index_by { |issue| issue.id }
      sql = ApplicationRecord::Domain::Repositories.github_sql.new(<<-SQL, issue_ids: issues.map(&:id))
        SELECT sub_query.issue_id as id,
          MAX(sub_query.pull_comments) as pull_comments,
          MAX(sub_query.review_comments) as review_comments
        FROM
        (
          SELECT issues.id as issue_id,
            COUNT(DISTINCT(pull_request_review_comments.id)) as pull_comments,
            0 as review_comments
            FROM issues
        LEFT JOIN pull_requests ON issues.pull_request_id = pull_requests.id
        LEFT JOIN pull_request_review_comments ON pull_request_review_comments.pull_request_id = pull_requests.id
                  AND pull_request_review_comments.state = 1
                  AND pull_request_review_comments.body > ''
           WHERE issues.id in :issue_ids
           GROUP BY issues.id
           UNION ALL
           SELECT issues.id as issue_id,
             0 as pull_comments,
             COUNT(DISTINCT(pull_request_reviews.id)) as review_comments
             FROM issues
         LEFT JOIN pull_requests ON issues.pull_request_id = pull_requests.id
         LEFT JOIN pull_request_reviews ON pull_request_reviews.pull_request_id = pull_requests.id
                   AND pull_request_reviews.state != 0
                   AND pull_request_reviews.body > ''
            WHERE issues.id in :issue_ids
            GROUP BY issues.id
       ) as sub_query
       GROUP BY sub_query.issue_id
      SQL

      sql.results.each do |id, pull_comments, review_comments|
        issue = map[id]
        if issue.pull_request?
          issue.pull_request.total_comments = issue.issue_comments_count + pull_comments + review_comments
        end
      end
    end

    def prefill_close_issue_reference_counts(issues, viewer = nil)
      pull_issues, issues = issues.partition(&:pull_request?)

      prefill_close_issue_reference_counts_for_issues(issues, viewer)
      prefill_close_issue_reference_counts_for_pulls(pull_issues, viewer)
    end

    def prefill_close_issue_reference_counts_for_issues(issues, viewer)
      return unless issues.any?

      # Find all the pull_request_ids connected to the issues via close_issue_references
      grouped_issue_sql = ApplicationRecord::Collab.github_sql.new(<<-SQL, issue_ids: issues.map(&:id))
        SELECT issue_id, GROUP_CONCAT(pull_request_id)
        FROM close_issue_references
        WHERE issue_id IN :issue_ids
        GROUP BY issue_id
      SQL

      # Create a hash so results can easily be indexed later
      # Returns { issue_id: [pull_request_id1, pull_request_id2], issue_id2: [pull_request_id_3] }
      grouped_issue_list = grouped_issue_sql.results.to_h.transform_values { |val| val.split(",").map(&:to_i) }

      # Find which of the connected pull requests are deemed viewable
      pull_ids = grouped_issue_list.values.flatten.uniq
      visible_prs_for_issues = Issue.visible_for(Issue.where(pull_request_id: pull_ids).pluck(:id), viewer: viewer).pluck(:pull_request_id)

      # For each issue, find which of its connected PRs are visible
      # and memoize the count on the issue instance.
      issues.each do |issue|
        pull_request_id_list = Array(grouped_issue_list[issue.id])
        issue.close_issue_references_count = (pull_request_id_list & visible_prs_for_issues).count
      end
    end

    def prefill_close_issue_reference_counts_for_pulls(pull_issues, viewer)
      return unless pull_issues.any?

      # Find all the issue_ids connected to the pull requests via close_issue_references
      grouped_prs_sql = ApplicationRecord::Collab.github_sql.new(<<-SQL, pull_request_ids: pull_issues.map(&:pull_request_id))
        SELECT pull_request_id, GROUP_CONCAT(issue_id)
        FROM close_issue_references
        WHERE pull_request_id IN :pull_request_ids
        GROUP BY pull_request_id
      SQL

      # Create a hash so results can easily be indexed later
      # Returns { pull_request_id: [issue_id1, issue_id2], pull_request_id2: [issue_id_3] }
      grouped_pr_list = grouped_prs_sql.results.to_h.transform_values { |val| val.split(",").map(&:to_i) }

      # Find which of the connected issues are deemed viewable
      issue_ids = grouped_pr_list.values.flatten.uniq
      visible_issues_for_prs = Issue.visible_for(issue_ids, viewer: viewer).pluck(:id)

      # For each issue, find which of its connected PRs are visible
      # and memoize the count on the issue instance.
      pull_issues.each do |pull_issue|
        issue_id_list = Array(grouped_pr_list[pull_issue.pull_request_id])
        pull_issue.close_issue_references_count = (issue_id_list & visible_issues_for_prs).count
      end
    end

    # Bulk load the number of open issues for labels.
    #
    # Returns nothing.
    def prefill_issue_counts(labels)
      return if labels.empty?

      # Avoid slow `label_id in (1, 2, n)` clause.
      repo_id = labels.first.repository.id

      sql = ApplicationRecord::Domain::Repositories.github_sql.new(<<-SQL, repo_id: repo_id)
        select label_id, issues.pull_request_id, count(1)
        from issues_labels inner join issues on issues.id = issues_labels.issue_id
        where issues.repository_id = :repo_id
        and issues.state = 'open'
        group by label_id, pull_request_id
      SQL

      issues_counts = Hash.new(0)
      non_pr_issues_counts = Hash.new(0)

      sql.results.each do |(label_id, pr_id, count)|
        issues_counts[label_id] += count
        non_pr_issues_counts[label_id] += count if pr_id.nil?
      end

      labels.each do |label|
        label.issues_count = issues_counts[label.id]
        label.issues_without_pull_requests_count = non_pr_issues_counts[label.id]
      end
    end

    # Public: Preloads PreReceiveEnvironment#hooks_count
    #
    # pre_receive_environments - An Array of PreReceiveEnvironments.
    #
    # Returns nothing.
    def prefill_hooks_counts(pre_receive_environments)
      return if pre_receive_environments.empty?
      environment_ids = pre_receive_environments.map(&:id)
      counts = PreReceiveHook.where(environment_id: environment_ids).group(:environment_id).count
      pre_receive_environments.each { |env| env.hooks_count = counts.fetch(env.id, 0) }
    end

    def prefill_milestones(events)
      return if events.empty?

      repo = events.first.repository
      milestone_titles = events.map(&:milestone_title).compact.uniq
      return if milestone_titles.empty?

      milestones_by_title = repo.milestones.where(title: milestone_titles).index_by(&:title)
      events.each do |event|
        next unless event.milestone_title
        event.milestone = milestones_by_title[event.milestone_title]
      end
    end

    def prefill_project_events(events)
      project_events = events.select { |event| event.subject_type == "Project" }
      return if project_events.empty?

      projects_by_id = {}
      projects = Project.where(id: project_events.map(&:subject_id).uniq)
      projects.each do |project|
        projects_by_id[project.id] = project
      end

      cards_by_id = Set.new(ProjectCard.where(id: project_events.map(&:card_id).uniq).pluck(:id))

      project_events.each do |event|
        event.issue_event_detail.subject = projects_by_id[event.subject_id]
        event.project_card_exists = cards_by_id.include? event.card_id
      end
    end

    # Preload reactions for all items
    def prefill_reactions(items)
      items_by_class_name = items.group_by { |i| i.class.name }
      items_by_class_name.slice!(*Reaction.valid_subject_types)

      return if items_by_class_name.empty?

      sql = Reaction.github_sql.new

      items_by_class_name.each_with_index do |(class_name, items), index|
        # We need to join all the subject clauses with UNIONs, so add a
        # UNION before every subject clause except the first one.
        sql.add(" UNION ") if index > 0

        sql.add <<-SQL
          SELECT *
          FROM reactions
          WHERE
        SQL

        if GitHub.spamminess_check_enabled?
          sql.add <<-SQL
          user_hidden = false AND
          SQL
        end

        subject_ids = items.map(&:id).uniq
        sql.add <<-SQL, subject_type: class_name, subject_ids: subject_ids
          (subject_type = :subject_type AND subject_id IN :subject_ids)
        SQL
      end

      reactions = sql.models(Reaction)

      prefill_belongs_to reactions, User, :user_id

      reactions_by_subject = reactions.group_by { |r| [r.subject_type, r.subject_id] }

      items_by_class_name.values.flatten.each do |item|
        item.association(:reactions).target = reactions_by_subject[[item.class.name, item.id]] || []
      end
    end

    # Preload users for releases and release assets.  Do both since they are
    # usually loaded right by each other.
    def for_releases(repository, releases, assets)
      user_ids = []
      user_ids += prefillable_ids(releases, :author_id) if releases.present?
      user_ids += prefillable_ids(assets, :uploader_id) if assets.present?
      user_ids.uniq!
      return if user_ids.blank?

      users = prefillable_ids_to_map(user_ids, User)
      return unless users.present?

      if releases.present?
        prefill_with_map(releases, users, :author_id)
        release_repository(repository, releases)
      end

      if assets.present?
        SlottedCounterService.prefill(assets)
        prefill_with_map(assets, users, :uploader_id)
        release_repository(repository, assets)
      end
    end

    def release_repository(repository, releases)
      Array(releases).each do |rel|
        rel.association(:repository).target = repository
      end
    end

    def asset_release(release, assets)
      assets.each do |asset|
        asset.association(:release).target = release
      end
    end

    # Public: Preloads PublicKey#user, PublicKey#repository
    #
    # public_keys - An Array of PublicKeys.
    #
    # Returns nothing.
    def for_public_keys(public_keys)
      fill_repositories public_keys
      prefill_belongs_to public_keys, User, :user_id
    end

    # Public: Preloads PreReceiveEnvironment#hooks, PreReceiveEnvironment#hooks_count
    #
    # pre_receive_environments - An Array of PreReceiveEnvironments.
    #
    # Returns nothing.
    def for_pre_receive_environments(pre_receive_environments)
      prefill_associations pre_receive_environments, [:hooks]
      prefill_hooks_counts pre_receive_environments
      nil
    end

    # Public: Preloads PreReceiveHook#targets, PreReceiveHook#repository, PreReceiveHook#environment
    #
    # pre_receive_hooks - An Array of PreReceiveHooks.
    #
    # Returns nothing.
    def for_pre_receive_hooks(pre_receive_hooks)
      prefill_belongs_to pre_receive_hooks, Repository, :repository_id
      prefill_belongs_to pre_receive_hooks, PreReceiveEnvironment, :environment_id
      prefill_associations pre_receive_hooks, [:targets]
      nil
    end

    # Public: Preloads PreReceiveHookTarget#hook
    #
    # pre_receive_hooks - An Array of PreReceiveHookTargets.
    #
    # Returns nothing.
    def for_pre_receive_hook_targets(pre_receive_hook_targets)
      targets = [pre_receive_hook_targets].flatten
      prefill_belongs_to targets, PreReceiveHook, :hook_id
      for_pre_receive_hooks targets.map(&:hook)
      prefill_belongs_to targets.select { |t| "User" == t.hookable_type }, User, :hookable_id
      prefill_belongs_to targets.select { |t| "Repository" == t.hookable_type }, Repository, :hookable_id
      prefill_belongs_to targets.select { |t| "Business" == t.hookable_type }, Business, :hookable_id
      nil
    end

    # Public: Preloads RepoDownloads#repository
    #
    # public_keys - An Array of RepoDownloads.
    #
    # Returns nothing.
    def for_repo_downloads(downloads)
      fill_repositories downloads
    end

    # Public: Preloads OauthAccess#application, OauthAccess#authorization
    #
    # oauth_accesses - An Array of OauthAccess.
    #
    # Returns nothing.
    def for_oauth_accesses(oauth_accesses)
      fill_applications(oauth_accesses)
      prefill_belongs_to oauth_accesses, OauthAuthorization, :authorization_id
    end

    # Public: Preloads CredendialAuthorization#actor
    #
    # credential_authorizations - An Array of CredentialAuthorizations.
    #
    # Returns nothing.
    def for_credential_authorizations(credential_authorizations)
      prefill_belongs_to credential_authorizations, User, :actor_id
      prefill_belongs_to credential_authorizations, Organization, :organization_id
      prefill_belongs_to(
        credential_authorizations.select { |c| "OauthAccess" == c.credential_type },
        OauthAccess,
        :credential_id,
      )
      prefill_belongs_to(
        credential_authorizations.select { |c| "PublicKey" == c.credential_type },
        PublicKey,
        :credential_id,
      )
    end

    # Public: Preloads EnterpriseInstallationUserAccountsUpload#storage_blob.
    #
    # uploads - An Array of EnterpriseInstallationUserAccountsUpload records.
    #
    # Returns nothing.
    def for_enterprise_installation_user_accounts_uploads(uploads)
      prefill_associations uploads, [:storage_blob]
      prefill_belongs_to uploads, Business, :business_id
    end

    # Public: Preloads Ephemeral Notice associations.
    #
    # ephemeral_notices - An Array of Ephemeral Notices.
    #
    # returns nothing.
    def for_ephemeral_notices(ephemeral_notices)
      prefill_belongs_to ephemeral_notices, User, :user_id
    end

    # Public: Prefills OAuth/Integration applications for the given records.
    #
    # records - An Array of ActiveRecord::Base models with an OAuthApplication/Integration
    # association.
    #
    # Returns nothing.
    def fill_applications(records)
      prefill_associations records, [:application]
    end

    # Public: Prefills Codespace associations
    #
    # codespaces - An array of Codespace records
    #
    # Returns nothing.
    def for_codespaces(codespaces)
      prefill_belongs_to codespaces, User, :owner_id
      prefill_belongs_to codespaces, Codespaces::Plan, :plan_id
    end

    # Public: Prefills Codespace associations for dashboard
    #
    # codespaces - An array of Codespace records
    #
    # Returns nothing.
    def for_codespaces_dashboard(codespaces)
      prefill_belongs_to codespaces, User, :owner_id
      prefill_belongs_to codespaces, Repository, :repository_id
      prefill_belongs_to codespaces, PullRequest, :pull_request_id
      prefill_belongs_to codespaces, Codespaces::Plan, :plan_id
    end

    # Public: Prefills Codespace associations for the repository view
    #
    # codespaces - An array of Codespace records
    #
    # Returns nothing.
    def for_codespaces_repository(codespaces)
      prefill_belongs_to codespaces, Repository, :repository_id
    end

    # Efficiently prefills a `belongs_to` attribute
    #
    # collection - An Array of ActiveRecord::Base objects.
    # model      - The Class of the attribute to prefill.
    # attributes - One or more Symbol names of the `belongs_to` attribute.
    #
    # Returns nothing.
    def prefill_belongs_to(collection, model, *attributes, &block)
      ids = prefillable_ids(collection, *attributes)
      map = prefillable_ids_to_map(ids, model, &block)
      prefill_with_map(collection, map, *attributes)
    end

    # Efficiently prefill an ActiveRecord relation. This
    # reuses existing Rails logic to implement this. The
    # relation allows for similar constructs as the Rails
    # :include syntax.
    #
    # collection - An Array of ActiveRecord::Base objects
    # relation   - An Array of Symbols & Hashes (or just a Hash) representing the relationship name(s)
    #
    # Returns nothing.
    def prefill_associations(collection, relation)
      ActiveRecord::Associations::Preloader.new.preload(collection, relation)
    end

    # Fetches all of the IDs in a collection.
    #
    # collection - An Array of ActiveRecord::Base objects.
    # fields     - One or more key field names.  Default: :id.
    #
    # Returns a uniq and compacted Array.
    def prefillable_ids(collection, *fields)
      fields = [:id] if fields.blank?
      ids = []

      collection.each do |item|
        fields.each do |field|
          if field.is_a? Hash
            raise ArgumentError.new("Only one ID mapping should be present") if field.size > 1
            ids << item.send(field.keys.first)
          else
            ids << item.send(field)
          end
        end
      end
      yield ids if block_given?
      ids.uniq!
      ids.compact!
      ids
    end

    # Turns an Array of IDs into a hash mapping ID => record.
    #
    # ids   - An Array of Integer IDs.
    # model - An ActiveRecord::Base subclass.
    # field - The field to query.
    #
    # Returns a Hash.
    def prefillable_ids_to_map(ids, model, field = :id)
      return {} if ids.blank?
      found = model.where(field => ids)
      yield found if block_given?
      found.index_by { |record| record.send(field) }
    end

    # Turns an Array of IDs into a hash mapping ID => [record1, record2].
    # Used for prefilling has_many associations
    #
    # ids   - An Array of Integer IDs.
    # model - An ActiveRecord::Base subclass.
    # field - The field to query.
    #
    # Returns a Hash.
    def prefillable_has_many_ids_to_map(ids, model, field = :id)
      return {} if ids.empty?
      model.where(field => ids).group_by { |record| record.send(field) }
    end

    # Prefills the collection with the given map of values.
    #

    # Prefills the collection with the given map of values.
    #
    # collection - An Array of ActiveRecord::Base objects.
    # map        - A Hash from #prefillable_ids_to_map.
    # fields     - One or more field names to map.
    #
    # Returns nothing.
    def prefill_with_map(collection, map, *fields)
      return if map.blank? || collection.blank?

      fields.map! do |field|
        if field.is_a? Hash
          raise ArgumentError.new("Only one ID mapping should be present") if field.size > 1
          id_field    = field.keys.first
          assoc_name  = field[id_field]

          [id_field, assoc_name]
        else
          [field, field.to_s.chomp("_id")]
        end
      end

      collection.each do |item|
        fields.each do |(id_field, assoc_name)|
          if id = item.send(id_field)
            item.association(assoc_name.to_sym).target = map[id]
          end
        end
      end
    end
  end
end
