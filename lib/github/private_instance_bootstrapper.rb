# frozen_string_literal: true

module GitHub
  # Handles GitHub Private Instance bootstrapping.
  #
  # Tracks a series of steps that are part of the bootstrap process, backed by
  # GitHub::KV storage.
  class PrivateInstanceBootstrapper
    autoload :BootstrapConfiguration, "github/private_instance_bootstrapper/bootstrap_configuration"
    autoload :EnterpriseProfileUpdate, "github/private_instance_bootstrapper/enterprise_profile_update"
    autoload :InternalSupportContact, "github/private_instance_bootstrapper/internal_support_contact"
    autoload :SAMLIdPConfiguration, "github/private_instance_bootstrapper/saml_idp_configuration"
    autoload :PoliciesConfiguration, "github/private_instance_bootstrapper/policies_configuration"
    autoload :CommittedConfiguration, "github/private_instance_bootstrapper/committed_configuration"

    # Public: Is the instance bootstrapped to the point that it can be used?
    #
    # Returns Boolean.
    def bootstrapped?
      bootstrap_settings.values.each do |setting|
        return false if setting.required? && !setting.completed?
      end

      true
    end

    # Public: Can the bootstrap configuration be committed?
    #
    # Returns true when all required bootstrap steps other than the commit step
    # have been completed, otherwise false.
    #
    # Returns Boolean.
    def can_be_committed?
      return false if bootstrapped? # Already bootstrapped and can't commit again.
      bootstrap_settings.select do |key, value|
        return false if required_setting_incomplete?(key)
      end

      true
    end

    # Public: Get the pending required bootstrap setting titles that need to be
    # configured before the bootstrap configuration can be committed.
    #
    # Note that the final commit setting (CommittedConfiguration) is not
    # included in the result.
    #
    # Returns Array of String.
    def pending_required_bootstrap_setting_titles
      bootstrap_settings.select do |key, value|
        required_setting_incomplete?(key)
      end.values.map(&:title).filter_map { |t| t if t.present? }
    end

    # Public: Returns the configuration object for the profile update step of the
    # bootstrapper
    #
    # Returns EnterpriseProfileUpdate.
    def enterprise_profile_update
      bootstrap_setting(EnterpriseProfileUpdate.key)
    end

    # Public: Returns the configuration object for the profile update step of the
    # bootstrapper
    #
    # Returns InternalSupportContact.
    def internal_support_contact
      bootstrap_setting(InternalSupportContact.key)
    end

    # Public: Returns the configuration object for the SAML IdP step of the
    # bootstrapper
    #
    # Returns SAMLIdPConfiguration.
    def saml_idp_configuration
      bootstrap_setting(SAMLIdPConfiguration.key)
    end

    # Public: Returns the configuration object for the policy step of the
    # bootstrapper
    #
    # Returns PoliciesConfiguration.
    def policies_configuration
      bootstrap_setting(PoliciesConfiguration.key)
    end

    # Public: Returns the configuration object for the final commit step of the
    # bootstrapper
    #
    # Returns CommittedConfiguration.
    def committed_configuration
      bootstrap_setting(CommittedConfiguration.key)
    end

    # Destroys the GitHub Private Instance bootstrap configuration.
    #
    # This unbootstraps an instance. Only really to be used in development.
    #
    # Returns Array of destroyed
    # GitHub::PrivateInstanceBootstrapper::BootstrapConfiguration objects.
    def destroy_configuration!
      bootstrap_settings.values.each do |setting|
        setting.destroy!
      end
    end

    private

    # Private: Get a specific BootstrapConfiguration object
    #
    # Returns BootstrapConfiguration.
    def bootstrap_setting(key)
      return bootstrap_settings[key]
    end


    # Private: All BootstrapConfiguration objects
    #
    # Returns Hash.
    def bootstrap_settings
      {
        EnterpriseProfileUpdate.key => EnterpriseProfileUpdate.new,
        InternalSupportContact.key => InternalSupportContact.new,
        SAMLIdPConfiguration.key => SAMLIdPConfiguration.new,
        PoliciesConfiguration.key => PoliciesConfiguration.new,
        CommittedConfiguration.key => CommittedConfiguration.new
      }
    end

    # Private: Determine if a required setting included in bootstrap_settings
    # (indicated by key) is incomplete, from the perspective of being able to
    # commit the bootstrap process.
    #
    # key - String that is the key for the setting in bootstrap_settings
    #
    # Returns Boolean.
    def required_setting_incomplete?(key)
      key != CommittedConfiguration.key &&
        bootstrap_settings[key].required? &&
        !bootstrap_settings[key].completed?
    end
  end
end
