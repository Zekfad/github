# frozen_string_literal: true

module GitHub
  class UnfurlProcessor
    def self.process_urls(user, subject, urls, body_version)
      processor = new(user, subject)
      urls.each { |url| processor.process(url) }

      # Make sure the subject's body hasn't been updated while we were processing these unfurls
      if processor.update_subject? && subject.reload.body_version == body_version
        subject.refresh_body_html
      end
    end

    def initialize(user, subject)
      @user = user
      @subject = subject
      @update_subject = false
    end

    def update_subject?
      @update_subject
    end

    def process(url)
      integrations = IntegrationContentReference.integrations_listening_for(
        repository: @subject.repository,
        url: url,
      )
      return if integrations.empty?

      reference = create_or_find_reference(url)
      return if reference.has_processed_attachment?

      reference.notify_integrations
      @update_subject = true
    end

    private

    def create_or_find_reference(url)
      ContentReference.create!(
        content: @subject,
        user: @user,
        reference: url,
      )
    rescue ActiveRecord::RecordNotUnique
      ContentReference.by_content_and_reference(@subject, url)
    end
  end
end
