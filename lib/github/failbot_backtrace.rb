# frozen_string_literal: true

require "failbot/backtrace"

# An extension of Failbot::Backtrace that recognizes gitrpc stacktraces that
# include a "---- THE WIRE ---" string in a special marker frame that delimits
# the gitrpc backend and frontend.
class GitHub::FailbotBacktrace < Failbot::Backtrace
  def self.parse(backtrace)
    fail ArgumentError, "expected Array, got #{backtrace.class}" unless backtrace.is_a?(Array)

    frames = backtrace.map do |frame|
      GitRPCAwareFrame.parse(frame)
    end

    new(frames)
  end

  class GitRPCAwareFrame < Failbot::Backtrace::Frame
    def self.parse(unparsed_line)
      if unparsed_line == THE_WIRE
        return THE_WIRE_FRAME
      else
        super
      end
    end
  end

  # This string is appended to remote exceptions that occur in gitrpc and are
  # handed over the client server boundary.  It won't parse as a stack frame
  # but we need to recognize it anyway.  See gitrpc/lib/gitrpc/failure.rb
  THE_WIRE = "---- THE WIRE ----"

  # If we encounter the wire boundary, return this preparsed frame.
  THE_WIRE_FRAME = GitRPCAwareFrame.new(THE_WIRE, THE_WIRE, "0", "<gitrpc boundary>")

  # This is the primary thing exported by this file: a backtrace parser that
  # meets the API defined by failbot in
  # https://github.com/github/failbot/pull/136
  module Parser
    EMPTY_ARRAY = [].freeze
    # Takes an Exception instance, returns an array of hashes with the
    # keys that Failbot::ExceptionFormat::Structured expects.
    def self.call(exception)
      if exception.backtrace
        ::GitHub::FailbotBacktrace.parse(exception.backtrace).frames.reverse!.map do |line|
          {
            "filename" => line.file_name,
            "abs_path" => line.abs_path,
            "lineno"   => line.line_number,
            "function" => line.method,
          }
        end
      else
        EMPTY_ARRAY
      end
    end
  end
end
