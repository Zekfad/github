# frozen_string_literal: true
#
# This file exists so we bypass the check that parser does
# on version mismatches, also for small version bumps in Ruby.
#
# Since there is no upstream fix for / support for 2.4.1 / 2.3.4
# at this point we work around it this way.
#
# Also see https://github.com/github/github/pull/75460
/^(?<major>\d+)\.(?<minor>\d+)\./ =~ RUBY_VERSION
require "parser/ruby#{major}#{minor}"
Parser::CurrentRuby = Parser.const_get("Ruby#{major}#{minor}")
