# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module CacheLeakDetector
    class CacheLeak < StandardError; end

    # Raise or report an exception if we're in a caching block.
    #
    # Returns nothing. Raises GitHub::CacheLeakDetector::CacheLeak.
    def no_caching!
      if caching?
        error = CacheLeak.new "sensitive information rendered in cache block"
        error.set_backtrace(caller)
        if Rails.test? || Rails.development?
          raise error
        else
          Failbot.report(error)
        end
      end
    end

    # Note that we are within a caching block.
    #
    # &block - The block to call while remembering that we're caching.
    #
    # Returns the return value of the block.
    def caching
      incr_caching_stack
      yield if block_given?
    ensure
      decr_caching_stack
    end

    # Are we currently within a cache block?
    #
    # Returns boolean.
    def caching?
      caching_stack > 0
    end

    private

    STACK_KEY = :cache_leak_detector_stack

    # The number of levels of caching blocks we're within.
    #
    # Returns an Integer.
    def caching_stack
      Thread.current[STACK_KEY] ||= 0
    end

    # Annotate that we've entered a caching block.
    #
    # Returns an Integer.
    def incr_caching_stack
      Thread.current[STACK_KEY] ||= 0
      Thread.current[STACK_KEY] += 1
    end

    # Annotate that we've exited a caching block.
    #
    # Returns an Integer.
    def decr_caching_stack
      Thread.current[STACK_KEY] -= 1
    end

    extend self
  end
end
