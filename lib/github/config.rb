# rubocop:disable Style/FrozenStringLiteralComment
# rubocop:disable Lint/DuplicateMethods

require "etc"
require "uri"
require "set"
require "socket"

module GitHub
  # GitHub global configuration
  #
  # This is mixed into the GitHub module such that an attribute "blah" defined
  # here is available at GitHub.blah and GitHub.blah=. All GitHub specific global
  # configuration should be defined here and set via one of the files defined
  # below.
  #
  # The load order for config values is (each overriding the last):
  # - Default values defined in this module
  # - Overrides in config/environment.rb
  # - Overrides in config/environments/<env>.rb
  # - Overrides in RAILS_ROOT/config.yml
  #
  # The RAILS_ROOT/config.yml file may set any config attribute defined in this
  # module. It's used primarily to customize FI installations.
  #
  # After boot, these properties should be treated as static and MUST NOT be
  # altered.
  #
  # When adding new configuration items, please document them. Include
  # information on what they're used for, default values, and example values
  # for a couple different environments if possible.
  #
  # NOTE This file is loaded as part of the config/basic.rb lightweight environment.
  # Do not add any additional library requires here.
  module Config
    ELASTICSEARCH_ACCESS_VALUES    = [:allowed, :ignored, :raises] # for validation
    MAX_UI_PAGINATION_PAGE         = 100
    PRIMARY_FS_TEST_DIR            = "/data/repositories/0/lost+found"
    MONITORS_USER_ID               = 1162581 # the user id for our special Monitors account
    MIRROR_PUSHER                  = "hubot" # the login for recording mirror "pushes"
    DGIT_COPIES                    = 3 # default in development and test; usually different in prod
    DESKTOP_FETCH_INTERVAL         = 5 # in minutes
    DOCS_BASE_URL                  = "https://docs.github.com".freeze
    DEVELOPER_BASE_URL             = "https://developer.github.com".freeze
    STAGING_DOMAIN                 = "github-staging-lab.com".freeze
    VSCS_PLATFORM_AUTH_SCHEMA_URL  = "https://aka.ms/vscs-platform-json-schema-extended".freeze

    SANITIZED_VALUE = "[FILTERED]".freeze
    params = %w(
      action category_slug controller direction feature gist_id id
      integration_id listing_slug login name number org
      organization_id owner page per_page person_login project_number repo
      repository repo_name sha since slug sort subject team_slug type
      user_id user_login utf8 client_id
    )
    WHITELISTED_PARAMETERS = /\A(#{params.join("|")})\z/

    # Set of known sites (datacenters) that can be return values of site_from_host.
    # Also update https://github.com/github/hydro-schemas/blob/master/proto/hydro/schemas/hydro/v1/envelope.proto
    # when this list is updated
    SITE_WHITELIST = Set[
      "cp1-iad",
      "ac4-iad",
      "va3-iad",
      "sdc42-sea",
    ]

    CODESPACES_STORAGE_ACCOUNTS = {
      resource_group: "github-codespaces-storage-accounts",
      subscription: "b1b4499a-ba38-45d4-aec8-98f1cd1ef217",
      environments: {
        production: {
          url_pattern: "https://%{account_name}.queue.core.windows.net",
          queue_name: "github-reporting-queue",
        },
        development: {
          url_pattern: "https://%{account_name}.queue.core.windows.net",
          queue_name: "github-reporting-queue",
        },
        test: {
          url_pattern: "https://%{account_name}.example.queue.test",
          queue_name: "github-reporting-queue",
        }
      }
    }

    VSCS_ENVIRONMENTS = {
      production: {
        name: :production,
        api_url: "https://online.visualstudio.com",
        api_version: {
            "Microsoft.VSOnline" => "2019-07-01-preview",
            "Microsoft.Codespaces" => "2020-06-16"
        },
        pfs_auth_host: "auth.apps.codespaces.githubusercontent.com",
        platform_auth_host: "github.dev",
        workbench_url_pattern: "https://%{id}.workspaces.github.com/environment/%{id}",
        integration_alias: :codespaces_production,
      },
      ppe: {
        name: :ppe,
        api_url: "https://online-ppe.core.vsengsaas.visualstudio.com",
        api_version: {
            "Microsoft.VSOnline" => "2019-07-01-beta",
            "Microsoft.Codespaces" => "2020-06-16-beta"
        },
        pfs_auth_host: "auth.apps.ppe.codespaces.githubusercontent.com",
        platform_auth_host: "ppe.github.dev",
        workbench_url_pattern: "https://%{id}.workspaces-ppe.github.com/environment/%{id}",
        integration_alias: :codespaces_ppe,
      },
      development: {
        name: :development,
        api_url: "https://online.dev.core.vsengsaas.visualstudio.com",
        api_version: {
            "Microsoft.VSOnline" => "2019-07-01-alpha",
            "Microsoft.Codespaces" => "2020-06-16-alpha"
        },
        pfs_auth_host: "auth.apps.dev.codespaces.githubusercontent.com",
        platform_auth_host: "dev.github.dev",
        workbench_url_pattern: "https://%{id}.workspaces-dev.github.com/environment/%{id}",
        integration_alias: :codespaces_dev,
      },
    }.freeze

    # Raised on data access when a datastore is configured to be inaccessible,
    # e.g. in a secondary datacenter.
    class UnexpectedDatastoreAccess < StandardError; end

    # The current runtime mode object. Used to determine whether we're running
    # under dotcom or enterprise mode and also allows dynamically switching
    # modes in development server environments.
    #
    # Returns a GitHub::Runtime object.
    def runtime
      @runtime ||= GitHub::Runtime.new
    end

    # Boolean attribute specifying whether the site is in SSL mode. This is
    # typically set true explicitly in production environments.
    #
    # Returns true if SSL is enabled for the site, false otherwise.
    def ssl?
      return @ssl if defined?(@ssl)
      ENV["GH_SSL"]
    end
    attr_writer :ssl
    alias ssl ssl?

    # Public: Return appropriate URI scheme for SSL mode.
    #
    # Returns String.
    def scheme
      ssl? ? "https" : "http"
    end

    # The x509 certificate for this Enterprise installation in PEM format.
    #
    # Returns a String that represents the certificate in PEM format.
    def ssl_certificate
      return @ssl_certificate if defined?(@ssl_certificate)

      if GitHub.enterprise? && (Rails.env.development? || Rails.env.test?)
        fake_cert_pem = "-----BEGIN CERTIFICATE-----\nMIIDlDCCAnygAwIBAgIJAOoPW0ZFhjwuMA0GCSqGSIb3DQEBCwUAMEQxGTAXBgNV\nBAMMEGdpdGh1Yi5sb2NhbGhvc3QxJzAlBgNVBAoMHkdpdEh1YiBTZWxmIFNpZ25l\nZCBDZXJ0aWZpY2F0ZTAeFw0xODAzMTQwODM5MjFaFw0yODAzMTEwODM5MjFaMEQx\nGTAXBgNVBAMMEGdpdGh1Yi5sb2NhbGhvc3QxJzAlBgNVBAoMHkdpdEh1YiBTZWxm\nIFNpZ25lZCBDZXJ0aWZpY2F0ZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC\nggEBALZc6x8cHdb7RfTkcDLx7f5t4MFg3dSCaXsYnledXqYgo6n/CeKXRzoUx+t5\nu7IzLSQ4V+B3nczR8D+Zmt0+T0ta8OBZ/K2vFOVxxK2ZnID4YZkvg6UnWJe0k2El\nzFVM23SclrDF40yETeKXGV3u0Eq4/LR6bEy1FSwCjtorgy6W77boOo9k3768QOTi\nac9clNl27xfAxvKmcfgbl4x0npIfj36X3kGXuqvemWbdP8qwnB6IK/DXahgvzlAJ\nvqMmm9bo8Em2iIKw1abgyfcPflG+/Fu+CwjmH+tohsoMn2JAg9oc+lxENIhtuVtg\nlEFqoHdeTtRIF5K4hotwxnP7NNsCAwEAAaOBiDCBhTAdBgNVHQ4EFgQUH3eO7bQK\nuZX+mxanQz/gnbTmPa4wHwYDVR0jBBgwFoAUH3eO7bQKuZX+mxanQz/gnbTmPa4w\nDAYDVR0TBAUwAwEB/zA1BgNVHREELjAshwR/AAABghBnaXRodWIubG9jYWxob3N0\nghIqLmdpdGh1Yi5sb2NhbGhvc3QwDQYJKoZIhvcNAQELBQADggEBAE9LIBq6pXm9\nDElS4ojDkzCI0oA5mnhFjMq9+IcOrdD2vY+/O0CdxPdXnNtJkPRYenr0S3270/3G\nKdxjyxRabnm6pXS52Ts7j4CwZZC3z/gIczYhLhfWWod28bYyoYEK/xBFBMc0XTcD\n4GPeAVNiz/jgD+ufn/L1iH9/TFHidyX+PfMTPQWAN9hteHnve5PG/oqG7So/GIHU\nVa5UcsrEd5iNTboIJ2FeuSuAtZwSbaK2iRzm4fLL9onW0CeTddLGjZrOvr3ETgqY\nfMnEzRG9Ybt81q7Q6MKA7TNsgbORVSwYUPW3aaR0OfvflNdS1BPSepLVeNsr6L8q\nDYItU51RJac=\n-----END CERTIFICATE-----\n"
        fake_x509 = OpenSSL::X509::Certificate.new(fake_cert_pem)
        # Make sure not_after is well in the future by default
        fake_x509.not_after = 1.year.from_now
        @ssl_certificate = fake_x509.to_pem
      end
    end
    attr_writer :ssl_certificate

    # Is the current environment connected to the public internet? This is
    # useful primarily in development environments where loading gravatars and
    # other external items may be skipped. This should always be set true
    # everywhere except in development and enterprise instances to prevent the
    # slow ping check.
    #
    # Returns true if external internet access is available, false otherwise.
    def online?
      return @online if defined?(@online)
      return false if enterprise?
      return google_reachable? if Rails.development?

      true
    end
    attr_writer :online
    alias online online?

    def google_reachable?
      @online = begin
        Timeout.timeout(1) do
          s = TCPSocket.new("google.com", 80)
          s.close
        end
        true
      rescue Errno::ECONNREFUSED
        true
      rescue Timeout::Error, StandardError
        false
      end
    end

    # The test environment's parallel execution number.
    #
    # Returns the environment number as a string but only in test environments.
    # In other environments this method returns nil.
    def test_environment_number
      Rails.env.test? && (ENV["TEST_ENV_NUMBER"] || "0")
    end

    # Enable gravatar images throughout the site. Some enterprise customers
    # may want to disable this for security reasons.
    #
    # Returns true if gravatar images should be displayed, false otherwise.
    def gravatar_enabled?
      return @gravatar_enabled if defined?(@gravatar_enabled)

      online?
    end
    attr_writer :gravatar_enabled

    # The base URL for all avatars. This server must implement the gravatar API. If passed
    # a string, it will determine which gravatar subdomain to use
    # (see https://github.com/github/github/issues/13687#issuecomment-22726044)
    #
    # string - An optional String that will be hashed to determine which Gravatar subdomain
    #          should be used (0, 1 or 2).
    #
    # Returns a gravatar base url
    def gravatar_url(string = nil)
      return @gravatar_url if defined? @gravatar_url
      subdomain = string ? Zlib.crc32(string.to_s) % 3 : 0
      "https://#{subdomain}.gravatar.com"
    end

    attr_writer :gravatar_url

    # Whether we are using gravatar.com or a custom avatar service.
    #
    # Returns true when gravatar_url ends with .gravatar.com
    def gravatar_service?
      gravatar_url =~ /\.gravatar\.com$/
    end

    # GpgVerify client instance
    #
    # Returns a GpgVerify instance
    def gpg
      @gpg ||= GpgVerify.new(gpgverify_url)
    end
    attr_writer :gpg

    # Host where the gpgverify HTTP service is running.
    #
    # Returns a url String.
    def gpgverify_url
      @gpgverify_url ||= "http://127.0.0.1:8686"
    end
    attr_writer :gpgverify_url

    def git_signing_smime_cert_store
      @git_signing_smime_cert_store ||= OpenSSL::X509::Store.new.tap do |store|
        store.add_file("/etc/ssl/certs/ca-certificates.crt")
      end
    end
    attr_writer :git_signing_smime_cert_store

    # Returns true if repositories require explictly accepted invitations when
    # adding a new member.
    def repo_invites_enabled?
      !GitHub.enterprise?
    end

    # The organization for GitHub Actions. Defaults to "actions".
    # Populated by the ENTERPRISE_ACTIONS_ACTIONS_ORG variable.
    attr_writer :actions_actions_org
    def actions_org
      return @actions_actions_org if defined?(@actions_actions_org)
    end

    # Whether GitHub Actions is enabled.
    # - Always enabled for dotcom.
    # - Disabled for Enterprise by default. Checks for ENTERPRISE_ACTIONS_ENABLED variable.
    def actions_enabled?
      return @actions_enabled if defined?(@actions_enabled)
      @actions_enabled = !enterprise?
    end
    attr_writer :actions_enabled

    # Store the private key for actions secrets. Only relevant for enterprise.
    # Populated by ENTERPRISE_ACTIONS_SECRETS_PRIVATE_KEY env variable.
    attr_accessor :actions_secrets_private_key

    def actions_secrets_public_key
      return @actions_secrets_public_key if defined?(@actions_secrets_public_key)

      unless GitHub.actions_secrets_private_key.empty?
        require "rbnacl"
        require "base64"

        key = RbNaCl::PrivateKey.new(Base64.decode64(GitHub.actions_secrets_private_key))
        @actions_secrets_public_key = Base64.strict_encode64(key.public_key)
      end
    end
    attr_writer :actions_secrets_public_key

    # The repository nwo to use for actions starter workflows.
    # Populated by ENTERPRISE_ACTIONS_STARTER_WORKFLOWS_NWO env variable for enterprise.
    def actions_starter_workflows_nwo
      return @actions_starter_workflows_nwo if defined?(@actions_starter_workflows_nwo)
      @actions_starter_workflows_nwo = "actions/starter-workflows" unless enterprise?
    end
    attr_writer :actions_starter_workflows_nwo

    # Whether GitHub Sponsors is enabled
    def sponsors_enabled?
      # GitHub Sponsors does not make sense as a feature on Enterprise.
      !enterprise?
    end

    # Whether merge queues are enabled
    def merge_queues_enabled?
      # Merge queues are not available on Enterprise.
      !enterprise?
    end

    # Killswitch to disable notifications v2 in enterprise
    # Defaults to false, but can be set with ENTERPRISE_NOTIFICATIONS_V2_DISABLED=true
    def notifications_v2_disabled?
      return @notifications_v2_disabled if defined?(@notifications_v2_disabled)
      @notifications_v2_disabled = false
    end
    attr_writer :notifications_v2_disabled

    # Whether a user can report a piece of content on dotcom
    #
    # Returns Boolean.
    def can_report?
      !enterprise?
    end

    # Whether a hubber needs to provide a reason to view private logs
    #
    # Returns Boolean.
    def hubber_access_prompt_enabled?
      !enterprise?
    end

    # Whether Orgs SAML SSO session (external identity session) enforcement is
    # enabled in this environment.
    #
    # The feature is currently only enabled for GitHub.com (as it would be
    # redundant with GHE's SAML SSO support).
    #
    # Returns Boolean.
    def external_identity_session_enforcement_enabled?
      !enterprise?
    end

    # Whether the /.well-known/security.txt route should be enabled?
    #
    # prodsec would prefer not to have it enabled for enterprise to reduce
    # spammy bug bounty submissions for exposed GHE instances
    def security_dot_txt_enabled?
      !enterprise?
    end

    # Whether we show language and UI controls related to terms of service.
    # Currently excludes enterprise environments.
    #
    # Returns a boolean.
    def terms_of_service_enabled?
      !enterprise?
    end

    # If we are using an external authentication mechanism, we can delegate
    # account lockouts to them.
    #
    # Returns true if account lockouts are enabled, false otherwise.
    def lockouts_enabled?
      (!@auth_limits_disabled || !Rails.test?) && !GitHub.auth.external?
    end

    def auth_limits_disabled=(disabled)
      @auth_limits_disabled = disabled
    end

    # Do we block JSON responses to non-XHR requests?
    def json_xhr_requirement_enabled?
      !Rails.test?
    end

    # The full path to the root directory where repositories are stored.
    def repository_root
      @repository_root ||=
        if Rails.development?
          env = enterprise?? "fi" : Rails.env
          File.expand_path("#{Rails.root}/repositories/#{env}")
        elsif Rails.test?
          # tests get a clean repo root and each parallel test process gets its own
          # directory too based on the TEST_ENV_NUMBER.
          "#{Rails.root}/repositories/test#{ENV['TEST_ENV_NUMBER']}"
        else
          "/data/repositories"
        end
    end
    attr_writer :repository_root

    # The git repository template used when creating new repositories on
    # disk. This contains hooks and any other files that should be present
    # in newly created or forked repositories.
    #
    # Returns the full path to git repo template directory.
    def repository_template
      @repository_template ||=
        if Rails.production?
          if enterprise?
            "/data/github/current/lib/git-core/fi-template".freeze
          else
            "/data/github/current/lib/git-core/template_shadow".freeze
          end
        else
          if enterprise?
            "#{Rails.root}/lib/git-core/fi-template".freeze
          else
            "#{Rails.root}/lib/git-core/template_shadow".freeze
          end
        end
    end
    attr_writer :repository_template

    # Delegated account recovery keys used to sign/verify recovery tokens
    attr_accessor :legacy_account_provider_public_key, :account_provider_private_key,
      :account_provider_symmetric_key, :recovery_provider_public_key,
      :recovery_provider_private_key

    # Earthsmoke configuration
    attr_accessor :earthsmoke_address, :earthsmoke_cert, :earthsmoke_token, :earthsmoke

    def earthsmoke
      return unless earthsmoke_enabled?
      return @earthsmoke if @earthsmoke

      timeout = case
      when Rails.env.test?
        1 # 1s
      when GitHub.foreground?
        0.6 # 600ms
      else
        5 # 5s
      end

      per_retry_timeout = case
      when Rails.env.test?
        0.5 # 500ms
      when GitHub.foreground?
        0.5 # 500ms
      else
        1 # 1s
      end

      # in prod, all hosts should know about GLB cert already
      cert = Rails.env.production? ? nil : earthsmoke_cert

      @earthsmoke = create_earthsmoke_client(
        server: earthsmoke_address,
        ca_file: cert,
        timeout: timeout,
        per_retry_timeout: per_retry_timeout,
      )
    end

    def create_earthsmoke_client(**kwargs)
      per_retry_timeout = kwargs.delete(:per_retry_timeout)

      ::Earthsmoke::Client.new(**kwargs.merge(
        token: earthsmoke_token,
      )).tap do |client|
        # Configure middleware for the earthsmoke client.
        # Order matters here. The first listed is the first to run and last to finish.
        client.use(::Earthsmoke::Middleware::Datadog,
          client: GitHub.dogstats,
          client_name: "github/github",
          extra_tags: ["overall:true"],
        )

        client.use(::Earthsmoke::Middleware::Retries,
          tries: 4,
          per_retry_timeout: per_retry_timeout,
          backoff_multiplier: 0, # no delay before retry
        )

        client.use(GitHub::Earthsmoke::Middleware::EnableTracing)
        client.use(::Earthsmoke::Middleware::Octotracer, client: GitHub.tracer)

        # Second datadog middleware. This runs after the retry middleware, giving us
        # per-retry stats.
        client.use(::Earthsmoke::Middleware::Datadog,
          client: GitHub.dogstats,
          client_name: "github/github",
          extra_tags: ["overall:false"],
        )

        client.use(GitHub::Earthsmoke::Middleware::CircuitBreaker)
        client.use(GitHub::Earthsmoke::Middleware::RequestID)
      end
    end

    def earthsmoke_enabled?
      !GitHub.enterprise?
    end

    def encrypted_attribute_enabled?(klass, destination)
      !GitHub.enterprise? && !klass.encrypted_attribute_key(destination).nil?
    end

    # Used only by the old RepositoriesNetshardTransition.
    def old_gist_repository_template
      @old_gist_repository_template ||=
        if Rails.production?
          "/data/github/current/lib/git-core/gist-template".freeze
        else
          "#{Rails.root}/lib/git-core/gist-template".freeze
        end
    end
    attr_writer :old_gist_repository_template

    def gist3_repository_template
      @gist3_repository_template ||=
        if Rails.production?
          "/data/github/current/lib/git-core/gist-template".freeze
        else
          "#{Rails.root}/lib/git-core/gist-template".freeze
        end
    end
    attr_writer :gist3_repository_template

    # Determine if the app is running under GitHub Enterprise. This is
    # determined by the ENTERPRISE (or FI) environment variable being set.
    #
    # Returns true if running under Enterprise, false otherwise.
    def enterprise?
      return @enterprise if defined?(@enterprise)
      @enterprise = GitHub.runtime.enterprise?
    end
    attr_writer :enterprise
    alias fi? enterprise?

    # Determines if the app is running on a non-clustered Enterprise instance
    # Enterprise non-clustered returns true
    # Enterprise clustered returns false
    # github.com returns false
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def single_instance?
      @single_instance ||= enterprise? && Rails.development?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
    attr_writer :single_instance

    # Determine if the current request is not enterprise
    def dotcom_request?
      !enterprise?
    end

    # Determine if the app should only show the limited Admin Center UI
    #
    # Returns true if running under Enterprise
    def limited_admin_center?
      GitHub.enterprise?
    end

    # The Basic Auth password used to access the Hosted Enterprise environment
    # in production.
    #
    # Defaults to `passworD1`, set in the environment as
    # `HOSTED_BASIC_AUTH_PASSWORD`.
    def hosted_basic_auth_password
      return @hosted_basic_auth_password if defined?(@hosted_basic_auth_password)
      @hosted_basic_auth_password = ENV["HOSTED_BASIC_AUTH_PASSWORD"] || GitHub.default_password
    end

    def default_request_timeout
      @default_request_timeout ||= ENV.fetch("DEFAULT_REQUEST_TIMEOUT", "10").to_i
    end
    attr_writer :default_request_timeout

    # The read timeout for GitRPC
    attr_accessor :gitrpc_timeout

    # The max duration (in seconds) a request can take before it times out.
    def request_timeout(env)
      env ||= {}
      request_method = env["REQUEST_METHOD"].to_s
      request_path = env["REQUEST_PATH"].to_s

      case request_method
      when "GET"
        case request_path
        when %r{\A/stafftools/chargebacks/unrecorded_disputes}
          [30, default_request_timeout].max
        when %r{\A(/api/v\d)?/internal/cistern}
          Api::Internal::CisternGitAccess::CONTROLLER_TIMEOUT
        end
      else
        case request_path
        # These are all request paths that take payment details like credit
        # card params and directly call Braintree APIs. They benefit from a
        # longer timeout. For more context see:
        #
        # https://github.com/github/github/pull/28356
        when %r{\A/account/billing/update_credit_card},
             %r{\A/account/cc_update},
             %r{\A/organizations/([^/]+)(.*)/billing/cc_update},
             %r{\A/organizations/([^/]+)(.*)/billing/update_credit_card},
             %r{\A/stafftools/users/([^/]+)(.*)/change_plan}
          [30, default_request_timeout].max
        when %r{\A/repositories\z},  # Create repo with upsell
             %r{\A/organizations\z}, # Create new paid organization
             %r{\A/join/plan\z},
             %r{\A/redeem/([^/]+)(.*)\z},
             %r{\A/site/custom_sleeptown\z}
          [20, default_request_timeout].max
        when %r{\A/_chatops/spokes}
          [120, default_request_timeout].max
        # This endpoint currently delivers synchronously to the
        # turboscan moda app and so should match the client
        # UPLOAD_TIMEOUT used in `lib/github/turboscan.rb`
        when %r{\A/repositories/[^/]+/code-scanning/analysis\z}
          [30, default_request_timeout].max
        end
      end || default_request_timeout
    end

    # The threshhold for slow web / API requests, in seconds.  Requests that
    # take longer than this threshold are sent to a slow bucket in haystack
    # for reporting.
    def slow_request_threshold
      @slow_request_threshold ||= 5.0
    end
    attr_writer :slow_request_threshold

    # Configures whether we send slow request needles to haystack
    def instrument_slow_requests?
      enterprise?
    end

    # Determine if the app is running on a employee-only unicorn.
    #
    # Return true on staff1.rs
    def employee_unicorn?
      return @employee_unicorn if defined?(@employee_unicorn)
      @employee_unicorn = false
    end
    attr_writer :employee_unicorn

    # Determine if the app is running on the new-style employee-only unicorn.
    #
    # Return true on one of the garage hosts
    def garage_unicorn?
      return @garage_unicorn if defined?(@garage_unicorn)
      @garage_unicorn = false
    end
    attr_writer :garage_unicorn

    # Approved statistics for use in marketing copy.
    #
    # Figures are in the millions (not included for more copy flexibility).
    # Should not be changed without approval from Data & PR teams.

    def mktg_stat_user
      "50"
    end

    def mktg_stat_repository
      "100"
    end

    def mktg_stat_organization
      "2.9"
    end

    def mktg_stat_last_updated
      "August 2019"
    end

    # Return the user-facing name for GitHub based on whether or not
    # they're looking at GitHub.com or an Enterprise installation.
    #
    # Returns a String
    def flavor
      if enterprise?
        "GitHub Enterprise"
      else
        "GitHub"
      end
    end

    # Return the user-facing name for GitHub based on whether or not
    # they're looking at GitHub.com or an Enterprise installation with unified
    # search enabled
    #
    # Returns a String
    def search_flavor
      if enterprise? && !GitHub::Connect.unified_search_enabled?
        "GitHub Enterprise"
      else
        "GitHub"
      end
    end

    # committer_name to be used for web edits/merges/reverts.
    #
    # Returns a String.
    def web_committer_name
      flavor
    end

    # committer_email to be used for web edits/merges/reverts.
    #
    # Returns a String.
    def web_committer_email
      noreply_address
    end

    # Do we sign web commmit with earthsmoke/gpgverify?
    #
    # Returns boolean.
    def web_commit_signing_enabled?
      !enterprise? && (ENV.fetch("GH_COMMIT_SIGNING_UNAVAILABLE", 0) == 0)
    end

    # Send the Expect-CT header?
    #
    # Details: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expect-CT
    #
    # For many Enterprise installations that are network restricted, their TLS
    # certificates will never appear in Certificate Transparency logs. For that
    # reason, don't send the Expect-CT header on Enterprise by default.
    #
    # Returns boolean.
    def certificate_transparency_enabled?
      return @certificate_transparency_enabled if defined?(@certificate_transparency_enabled)
      @certificate_transparency_enabled = !enterprise?
    end
    attr_writer :certificate_transparency_enabled

    # Used to determine whether we verify emails hosted with a disposable email service.
    #
    # Returns boolean.
    def prevent_disposable_email_verification?
      return @prevent_disposable_email_verification if defined?(@prevent_disposable_email_verification)
      @prevent_disposable_email_verification = !enterprise?
    end
    attr_writer :prevent_disposable_email_verification

    # Used to determine the period of time below which an account is always
    # considered active and within which we look for activity when considering
    # how dormant the account is. See User#recently_active? and
    # User#exempt_from_dormancy?
    #
    # Returns a time period.
    def dormancy_threshold
      return @dormancy_threshold if defined?(@dormancy_threshold)
      @dormancy_threshold = (GitHub.enterprise? ? GitHub.enterprise_dormancy_threshold : 12.months)
    end
    attr_writer :dormancy_threshold

    # Is caching of the most recent stratocaster event timestamp for users
    # enabled?
    #
    # Enabled by default on Enterprise, where we archive stratocaster_events
    # records older than a month. With this config setting enabled, we cache
    # users' most recent stratocaster event timestamp in GitHub.kv
    #
    # Disabled for dotcom.
    #
    # Returns true if enabled, otherwise false.
    def stratocaster_event_timestamp_cache_enabled?
      return @stratocaster_event_timestamp_cache_enabled if defined?(@stratocaster_event_timestamp_cache_enabled)
      @stratocaster_event_timestamp_cache_enabled = enterprise?
    end
    attr_writer :stratocaster_event_timestamp_cache_enabled

    # Is the public event timeline delayed? On GitHub.com we delay the public
    # event feed to allow our token scanning logic time to notify providers and
    # for them to act on the results.
    def public_event_timeline_delayed?
      !GitHub.enterprise?
    end


    # Determine if the "Dormant users" page in stafftools should be displayed.
    # This performs fairly intense operations against the entire users table,
    # so it shouldn't be used on .com.
    def bulk_dormant_user_suspension_enabled?
      return @bulk_dormant_user_suspension_enabled if defined?(@bulk_dormant_user_suspension_enabled)
      @bulk_dormant_user_suspension_enabled = GitHub.enterprise?
    end
    attr_writer :bulk_dormant_user_suspension_enabled

    # Trusted ports are used for GitHub Enterprise to allow some diagnostics
    # scripts to hit the API and some other staff-specific routes without
    # authenticating.
    #
    # Returns true if this feature is enabled, false otherwise.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def trusted_ports_enabled?
      @trusted_ports_enabled ||= enterprise?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
    attr_writer :trusted_ports_enabled

    # The delegated account recovery feature allows us to act as an Account Provider
    # and a Recovery Provider. Currently, we will only be acting as an Account Provider
    # but we fake out acting as a recovery provider in dev/test.
    def github_as_recovery_provider_enabled?
      !Rails.env.production?
    end

    def delegated_recovery_enabled?
      !GitHub.enterprise?
    end

    # The ports we implicitly trust. Nginx should only be listening to
    # these ports on localhost.
    #
    # Returns an array of strings.
    def trusted_ports
      @trusted_ports ||= if GitHub.enterprise?
        %w(1337)
      else
        []
      end
    end

    ##
    # Host Names

    # The external GitHub hostname only. No http:// prefix or protocol
    # information is included.
    #
    # NOTE: For staff hosts, this will be the full staff host, including
    # `admin.github.com`, `mtodd.review-lab.github.com`, and `garage.github.com`.
    #
    # Returns the hostname string ("github.com", "github.localhost", etc.) or nil if
    # no host_name has been set.
    def host_name
      @host_name || ENV["GH_HOSTNAME"]
    end
    attr_writer :host_name

    # The main external GitHub hostname only. No http:// prefix or protocol
    # information is included.
    #
    # NOTE: This should exclude subdomains for staff hosts.
    #
    # Returns the hostname string ("github.com", "github.localhost", etc.) or nil if
    # no host_name has been set.
    def host_domain
      @host_domain ||=
        if host_name.present?
          PublicSuffix.domain(host_name)
        else
          "github.com"
        end
    end
    attr_writer :host_domain

    # Determines if emails are sent with the configured noreply address
    # as Enterprise always has, or the GitHub.com style of emails where
    # they are From: a notifications@ address
    attr_accessor :mail_use_noreply_addr

    # Hostname for githubusercontent.com.
    #
    # production:  githubusercontent.com
    # development: githubusercontent.dev
    #
    # Returns String host or nil.
    attr_accessor :user_content_host_name

    # The host name where the API accepts uploads.
    def api_upload_host_name
      @api_upload_host_name ||= if (enterprise? || garage_unicorn?)
        "#{host_name}/api/uploads".freeze
      else
        "uploads.#{host_name}".freeze
      end
    end

    # Choose which type of URL creator we need to return
    #
    # Returns an instance of GitHub::UrlBuilder
    def urls
      default_url_builder
    end

    # Generate URLs for default GitHub instances.
    #
    # Returns a GitHub::UrlBuilder instance
    def default_url_builder
      @default_url_builder ||= GitHub::UrlBuilder.new(host_name: host_name, scheme: scheme)
    end

    # Whether or not the URLs generated for API endpoints should include a path
    # prefix. By default, no prefix is included and API routes are expected to
    # be distinguished from other routes via their hostname.
    #
    # Returns a Boolean.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def use_api_path_prefix?
      @use_api_path_prefix ||= (
        ENV.fetch("USE_API_PATH_PREFIX", "0") == "1" ||
        enterprise? ||
        garage_unicorn?
      )
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # The host name that the API is served from.
    #
    # Retrurns a String.
    def api_host_name
      urls.api_host_name
    end

    # The root path of the API.
    #
    # Returns a String.
    def api_root_path
      urls.api_root_path
    end

    # The URL where the V3 API is served from. This defaults to the configured
    # host_name with an "api." prefix.
    #
    # Returns the API url string (e.g. "https://api.github.com" for production)
    def api_url
      urls.api_url
    end
    attr_writer :api_url

    # The host name that the internal API is served from.
    #
    # Retrurns a String.
    def internal_api_host_name
      urls.internal_api_host_name
    end

    # The root path of the API.
    #
    # Returns a String.
    def internal_api_root_path
      urls.internal_api_root_path
    end

    # The URL where the V3 Internal API is served from.
    # It uses its own pool of k8s resources.
    #
    # Returns the API url string (e.g. "https://api.github.com" for production)
    def internal_api_url
      urls.internal_api_url
    end

    # The URL where the V3 API is served from for githooks.
    #
    # Returns the API url string (e.g. "https://api.github.com" for production)
    def githooks_api_url
      internal_api_url
    end

    # The URL where the GraphQL API is served from. This defaults to the
    # configured host_name with an "api." prefix.
    #
    # Returns the API url string (e.g. "https://api.github.com" for production)
    def graphql_api_url
      urls.graphql_api_url
    end
    attr_writer :graphql_api_url

    def api_upload_prefix
      @api_upload_prefix ||= begin
        host_and_path = api_upload_host_name
        (ssl? ? "https://" : "http://") + host_and_path
      end
    end

    # Determine whether API deprecation headers are.
    #
    # Returns true if enabled, false otherwise.
    def api_deprecation_headers_enabled?
      return @api_deprecation_headers_enabled if defined?(@api_deprecation_headers_enabled)
      @api_deprecation_headers_enabled = !GitHub.enterprise?
    end
    attr_writer :api_deprecation_headers_enabled

    def admin_host_name
      "admin.github.com"
    end
    attr_writer :admin_host_name

    def admin_host?
      !!@is_admin_host
    end
    attr_writer :is_admin_host

    def stafftools_url
      urls.stafftools_url
    end

    def insights_available?
      # a temporary hack to hide insights from all users in 2.19 after last minute
      # go-to-market postponement
      @insights_available ||= false # GitHub.enterprise?
    end
    attr_writer :insights_available

    def insights_url
      return nil unless insights_available?
      GitHub.kv.get("insights_url").value { nil }
    end

    def insights_url=(value)
      return unless insights_available?
      GitHub.kv.set("insights_url", value)
    end

    def admin_frontend_enabled?
      return @admin_frontend_enabled if defined?(@admin_frontend_enabled)
      @admin_frontend_enabled = true
    end
    attr_writer :admin_frontend_enabled

    # The user id for our Monitors account, which is used to display things in the office
    # and often has some special privilege.
    #
    # Returns an integer id.
    def monitors_user_id
      @monitors_user_id || MONITORS_USER_ID
    end

    # The fetch interval in minutes for GitHub Desktop applications.
    #
    # Returns an integer representing minutes
    def desktop_fetch_interval
      @desktop_fetch_interval || DESKTOP_FETCH_INTERVAL
    end
    attr_writer :desktop_fetch_interval

    # The login for recording mirror pushes.
    #
    # Returns the String login name.
    def mirror_pusher
      return @mirror_pusher if defined?(@mirror_pusher)
      @mirror_pusher = MIRROR_PUSHER
    end
    attr_writer :mirror_pusher

    # How many copies (replicas) of each repo network to aim for.
    #
    # Returns a small, odd integer.
    def dgit_copies
      @dgit_copies ||= DGIT_COPIES
    end
    attr_writer :dgit_copies

    # How many non-voting copies of each repo network to aim for.
    #
    # If nil, spokes will not care how many non-voting replicas exist.
    attr_accessor :dgit_non_voting_copies

    # How much RSS will the dgit maintenance scheduler allow itself to use?
    #
    # This size is in bytes. The baseline RSS of a resque worker is between
    # 500MB and 700MB. The dominant query in the maintenance scheduler uses
    # 350 - 400 bytes per network replica in the current query slice. In prod,
    # we typically look at a slice of 200,000 networks at a time, and each
    # has 6 replicas, which is 457MB, so the total process size would be
    # ~1157MB. There's a thread in God that kills any process over 4GB of
    # RSS on slowworker nodes, which is where the maint scheduler runs. So,
    # tell the maint worker to limit itself to 3GB. This should be high
    # enough that mysql query time is the limiting factor on slice size.
    #
    # In GHE, the baseline resque process size is the same. In general, GHE
    # instances have up to two replicas of less than 10,000 networks. In
    # this case, the result set will use fewer than 10 MB of RSS, which
    # isn't significant in a process that's already got 700MB of RSS. Also,
    # on these instances, the query slicer will never limit the queries (the
    # smallest slice size is 10,000 networks). For larger instances, with
    # e.g. 3 replicas of 100,000 networks, the result set size is 114 MB,
    # which means that the total worker RSS would be ~814 MB. So, make the
    # default maint RSS 1GB in GHE, since that will avoid any limiting in
    # all known cases today, but provide some protection for larger instances
    # in the future.
    def dgit_maint_max_rss
      @dgit_maint_max_rss ||=
        begin
          gb = 1024 * 1024 * 1024
          if Rails.test?
            100 * gb # effectively disabled
          elsif GitHub.enterprise?
            gb
          else
            3 * gb
          end
        end
    end
    attr_writer :dgit_maint_max_rss

    # How frequently should the timerd job run to pre-fill the
    # cached disk stats?
    #
    # This should be smaller than dgit_disk_stats_cache_ttl.
    attr_writer :dgit_disk_stats_interval
    def dgit_disk_stats_interval
      @dgit_disk_stats_interval || 60
    end

    # How long should GitHub::DGit.disk_stats and GitHub::DGit.parallel_disk_stats
    # cache their responses for?
    attr_writer :dgit_disk_stats_cache_ttl
    def dgit_disk_stats_cache_ttl
      @dgit_disk_stats_cache_ttl || 90
    end

    # How frequently should the timerd job run to compare
    # fileservers tables between the legacy and new Spokes
    # databases?
    attr_writer :dgit_fileservers_diffjob_interval
    def dgit_fileservers_diffjob_interval
      @dgit_fileservers_diffjob_interval || 60
    end

    # How many copies (replicas) of a repo are needed to accept a write.
    #
    # `ncopies` is an optional indication of the actual number of replicas
    # eligible to vote.  When this is more than the number returned by
    # `dgit_copies`, the quorum may be larger.  For example, the normal
    # number of copies is 3, but in some cases, a repo network might have
    # 4 copies, in which case it takes 3 (not 2) to make a quorum.
    #
    # Returns a strict majority of the target number of copies.
    def dgit_quorum(ncopies = dgit_copies)
      [ncopies, dgit_copies].max/2 + 1
    end

    def dgit_git_daemon_port
      @dgit_git_daemon_port ||= if Rails.development?
        9418
      elsif Rails.test?
        9420 + test_environment_number.to_i
      else # production
        9419
      end
    end
    attr_writer :dgit_git_daemon_port

    def dgit_gitrpcd_port
      @dgit_gitrpcd_port ||= if Rails.development?
        9480
      elsif Rails.test?
        9480 + test_environment_number.to_i
      else # production
        9480
      end
    end
    attr_writer :dgit_gitrpcd_port

    # Should `GitHub::DGit.threepc_debug` output anything?
    def dgit_threepc_debug_enabled?
      return false if Rails.test?
      return @dgit_threepc_debug_enabled if defined?(@dgit_threepc_debug_enabled)
    end
    attr_writer :dgit_threepc_debug_enabled

    def package_registry_cdn_enabled?
      return @package_registry_cdn_enabled if defined?(@package_registry_cdn_enabled)
      @package_registry_cdn_enabled = GitHub.flipper[:package_registry_cdn].enabled?
    end
    attr_writer :package_registry_cdn_enabled

    def package_registry_enabled?
      return @package_registry_enabled if defined?(@package_registry_enabled)
      @package_registry_enabled = !enterprise?
    end
    attr_writer :package_registry_enabled

    # The Gist hostname. No http:// prefix or protocol information is included.
    #
    # Returns the Gist hostname string ("gist.github.com"). Defaults to host_name.
    def gist_host_name
      @gist_host_name ||= host_name
    end
    attr_writer :gist_host_name

    # The Gist Playground hostname. No http:// prefix or protocol information is included.
    #
    # Returns the Gist Playground hostname string ("gist-playground.github.com"). Defaults to host_name.
    def gist_playground_host_name
      @gist_playground_host_name ||= host_name
    end
    attr_writer :gist_playground_host_name

    # The Gist 3 hostname. No http:// prefix or protocol information is included.
    # Until we make the switch to Gist 3, we'll be running both side-by-side.
    #
    # Returns the Gist hostname string ("gist.github.com"). Defaults to host_name.
    def gist3_host_name
      @gist3_host_name ||= host_name
    end
    attr_writer :gist3_host_name

    # Is Gist running under its own subdomain like gist.github.com? If not, it is served from <url>/gists.
    def gist_domain?
      gist_host_name != host_name
    end

    # Is Gist 3 running under its own subdomain like gist3.github.com? If not, it is served from <url>/gists.
    def gist3_domain?
      gist3_host_name != host_name
    end

    # Gist 3 OAuth Client credentials. Used for OAuth authentication in subdomain mode.
    attr_accessor :gist3_oauth_client_id, :gist3_oauth_secret_key

    # The Subversion / Slummin hostname. No http:// prefix or protocol
    # information is included. By default, this is the configured host_name.
    #
    # Returns the hostname string ("github.com", "stg.github.com", etc)
    def subversion_host_name
      @subversion_host_name ||= "#{host_name}".freeze
    end
    attr_writer :subversion_host_name

    # The GitHub users hostname. No http:// prefix or protocol information is
    # included. By default, this is the configured host_name with a "users.noreply."
    # prefix.  This hostname is used for "stealth" emails.
    #
    # Returns the hostname string (ex: "users.noreply.github.com")
    def stealth_email_host_name
      @stealth_email_host_name ||= "users.noreply.#{host_name}".freeze
    end
    attr_writer :stealth_email_host_name

    # The short name of the machine this process is currently executing on. Writing to
    # this config value is not recommended.
    def local_host_name_short
      if GitHub.kube?
        # 'kube-unknown' protects us from processes that didn't set the
        # env var properly, and hopefully also is greppable to find this comment
        long = ENV["KUBE_NODE_HOSTNAME"] || "kube-unknown"
      else
        long = local_host_name
      end
      @local_host_name_short ||= long.split(".", 2).first
    end
    attr_writer :local_host_name_short

    # The name of the machine this process is currently executing on.
    # Explicitly set on Enterprise to the configured node name.
    def local_host_name
      @local_host_name ||= (require "socket"; Socket.gethostname)
    end
    attr_writer :local_host_name

    # The role of the currently running process, request or job. Defaults to
    # :unassigned as a catch all for when the role is not setup. This is
    # explicitly set before web requests, background jobs, etc. to provide
    # context of where an action is happening from (like a sql query).
    # Keep it a Symbol so everyone knows what they are dealing with.
    #
    # Returns a Symbol of the currently set role for this process, request,
    # background job, etc.
    def role
      @role ||= begin
        from_env = ENV["GITHUB_CONFIG_ROLE"]

        if from_env.nil? || from_env.empty?
          :unassigned
        else
          from_env.to_sym
        end
      end
    end

    # Get a role from the host's Sites API role. You might ask "why isn't this
    # the default for #role"? Because there are a bunch of places where we used
    # to set GITHUB_CONFIG_ROLE god config and it may or may not have been picked
    # up by processes. In those cases, we need to be more explicit so that we
    # don't accidentally have mixed metrics.
    def role_from_host
      host_app = server_metadata["app"]
      host_role = server_metadata["role"]

      if host_app == "github"
        case host_role
        when "api", "registryfe", "stafftools", "dfs"
          host_role.to_sym
        when "fe"
          if local_host_name.start_with?("github-fe")
            :fe
          elsif local_host_name.start_with?("github-staff")
            :lab
          else
            :unassigned
          end
        when "staff"
          :lab
        else
          :unassigned
        end
      elsif host_app == "pages"
        case host_role
        when "dfs"
          # pages-dfs's timerd is a github/github process
          :pagesdfs
        else
          :unassigned
        end
      else
        :unassigned
      end
    end

    def role=(role)
      return @role if @role == role

      @role = role
      GitHub.reset_dogtags
      @role
    end

    def internal_api_role?
      GitHub.role.to_s == "internal-api"
    end

    def component
      @component ||= begin
        from_env = ENV["GITHUB_CONFIG_COMPONENT"]

        if from_env.nil? || from_env.empty?
          :unassigned
        else
          from_env.to_sym
        end
      end
    end

    def component=(component)
      return @component if @component == component

      @component = component
      GitHub.reset_dogtags
      @component
    end

    def foreground?
      component == :unicorn
    end

    # The domain of the machine this process is currently executing on. Writing
    # to this config value is not recommended.
    def domain_name
      if kube?
        full_name = ENV["KUBE_NODE_HOSTNAME"] || "kube-unknown.unknown"
      else
        full_name = local_host_name
      end

      @domain_name ||= full_name.split(".", 2).last
    end
    attr_writer :domain_name

    # The short domain of the machine this process is currently executing on.
    # Writing to this config value is not recommended.
    def domain_name_short
      @domain_name_short ||= domain_name.split(".", 2).first
    end
    attr_writer :domain_name_short

    # These local git, pages and storage host names are used to avoid setting
    # the local partitions to `localhost`.
    def local_git_host_name
      @local_git_host_name || "localhost"
    end
    attr_writer :local_git_host_name

    def local_pages_host_name
      @local_pages_host_name || "localhost"
    end
    attr_writer :local_pages_host_name

    def local_storage_host_name
      @local_storage_host_name || "localhost"
    end
    attr_writer :local_storage_host_name

    # PRs content hostname. No http:// prefix or protocol information is included.
    # No default b/c Enterprise might not use a FQDN.
    #
    # Returns String host or nil.
    attr_accessor :prs_content_host_name

    # The PRs content site URL.
    #
    # production:  https://prs.githubusercontent.com
    # garage:      https://prs-garage.githubusercontent.com
    # development: http://prs.githubusercontent.dev
    #
    # Returns the URL string or nil.
    def prs_content_host_url
      @prs_content_host_url ||= prs_content_domain? ? "#{scheme}://#{prs_content_host_name}".freeze : nil
    end
    attr_writer :prs_content_host_url

    # Should pr diffs/patches be served from its own subdomain like prs.githubusercontent.com?
    def prs_content_domain?
      !prs_content_host_name.to_s.empty?
    end

    ##
    # URLs

    # The main GitHub site URL. It returns
    # the http:// or https:// version of the URL based on whether the #ssl
    # attribute is set.
    #
    # Returns the URL string ("https://github.com", "http://github.localhost")
    def url
      urls.url
    end

    # Help URL - Enterprise and production serve Help docs differently
    #
    # Returns the URL string ("https://docs.github.com", "http://docs.github.com/enterprise", etc.)
    def help_url(skip_enterprise: false)
      if !skip_enterprise && GitHub.enterprise?
        "#{enterprise_help_landing_page}/user"
      else
        DOCS_BASE_URL
      end
    end

    # Developer Help URL for the environment.
    #
    # Note: If you want to link to the Developer blog use `developer_blog_url`
    # instead.
    #
    # Returns the Developer Help URL String for the environment.
    def developer_help_url(skip_enterprise: false)
      if !skip_enterprise && GitHub.enterprise?
        "#{DOCS_BASE_URL}/enterprise/#{major_minor_version_number}"
      else
        DOCS_BASE_URL
      end
    end

    # Old developer site URL.
    #
    # The developer site content was moved over to the docs.github.com site
    # except the content located on the blog. At least one test requires the
    # Developer blog base url. Use the developer_site_url for tests that
    # require the developer blog base URL.
    #
    # Returns the Developer site base URL string.
    def developer_site_url
      DEVELOPER_BASE_URL
    end

    # Developer blog URL.
    #
    # The developer blog deliberately isn't included with the GitHub Enterprise
    # versioned documentation. Use this method instead of `developer_help_url`
    # when you want to link to the Developer blog.
    #
    # Returns the Developer Blog URL String.
    def developer_blog_url
      "#{DEVELOPER_BASE_URL}/changes"
    end

    # Enterprise Help Admin URL.
    #
    # Returns the URL string ("https://docs.github.com/enterprise/2.3/admin")
    def enterprise_admin_help_url(skip_version: false)
      "#{enterprise_help_landing_page(skip_version: skip_version)}/admin"
    end

    # Enterprise Help landing page URL.
    #
    # Returns the URL string ("https://docs.github.com/enterprise/2.3")
    def enterprise_help_landing_page(skip_version: false)
      if !skip_version && GitHub.enterprise?
        "#{DOCS_BASE_URL}/enterprise/#{major_minor_version_number}"
      else
        "#{DOCS_BASE_URL}/enterprise"
      end
    end

    # Guides URL
    #
    # Returns the URL string ("https://guides.github.com")
    def guides_url
      "https://guides.github.com"
    end

    # Education URL
    #
    # Returns the URL string ("https://education.github.com", "http://education.github.com/students", etc.)
    def education_url
      "https://education.github.com"
    end

    # GitHub Classroom URL.
    #
    # Returns the URL string ("https://classroom.github.com", "http://classroom.github.com/classrooms/new", etc.)
    def classroom_host
      "https://classroom.github.com"
    end

    # Feature Preview Help URL
    #
    # Returns the URL string
    def feature_preview_help_url
      "#{help_url}/en/github/getting-started-with-github/exploring-early-access-releases-with-feature-preview"
    end

    # Update personal credit card Help URL
    #
    # Returns the URL string
    def personal_cc_help_url
      "#{GitHub.help_url}/articles/updating-your-personal-account-s-credit-card"
    end

    # Update organization credit card Help URL
    #
    # Returns the URL string
    def org_cc_help_url
      "#{GitHub.help_url}/articles/updating-your-organization-s-credit-card"
    end

    # Update personal paypal Help URL
    #
    # Returns the URL string
    def personal_paypal_help_url
      "#{GitHub.help_url}/articles/updating-your-personal-account-s-paypal-information/"
    end

    # Update organization paypal Help URL
    #
    # Returns the URL string
    def org_paypal_help_url
      "#{GitHub.help_url}/articles/updating-your-organization-s-paypal-information"
    end

    # Enterprise accounts Help URL
    #
    # Returns the URL string
    def business_accounts_help_url
      "#{help_url}/articles/about-enterprise-accounts"
    end

    # Trade Controls Help URL
    #
    # Returns the URL string
    def trade_controls_help_url
      "#{help_url}/articles/github-and-trade-controls"
    end

    # Services available URL for Trade Controls Restrictions
    #
    # Returns the URL string
    def trade_controls_services_available_url
      "#{help_url}/en/github/site-policy/github-and-trade-controls#what-is-available-and-not-available"
    end

    # Frequently asked questions URL for Trade Controls Restrictions
    #
    # Returns the URL string
    def trade_controls_faq_url
      "#{trade_controls_help_url}#frequently-asked-questions"
    end

    # Appeals form for Scheduled Organization Trade Controls Restrictions
    #
    # Returns the URL string
    def org_pending_enforcement_appeals_url
      "https://airtable.com/shrB2je5RBkqLEt5D"
    end

    # GitHub privacy statement url
    #
    # Returns the URL string
    def privacy_statement_url
      "#{help_url}/articles/github-privacy-statement"
    end

    def spending_limit_url
      "#{help_url}/github/setting-up-and-managing-billing-and-payments-on-github/managing-your-spending-limit-for-github-actions"
    end

    # The directory containing subschemas for the GitHub API.
    #
    # Return a string path.
    def api_subschema_dir
      Rails.root.join("app/api/schemas/v3/schemas")
    end

    # Enable rate limiting. Disabled by default in development and Enterprise.
    # override defaults with the RATE_LIMITING environment variable.
    def rate_limiting_enabled?
      return @rate_limiting_enabled if defined?(@rate_limiting_enabled)
      @rate_limiting_enabled = ENV["RATE_LIMITING"] ||
        (!Rails.development? && !enterprise?)
    end
    attr_writer :rate_limiting_enabled

    # A list of users that are exempt from rate limits. Exempt means that we
    # apply *very* high rate limits as defined in `app/api/rate_limit_configuration.rb`
    #
    # Returns an array of user names
    def rate_limiting_exempt_users
      @rate_limiting_exempt_users ||= GitHub.enterprise? ? [] : ["hubot".freeze]
    end
    attr_writer :rate_limiting_exempt_users

    # The asset host root URL. This defaults to url and is typically
    # overridden in production environments to enable asset loading from a
    # CDN / separate domain.
    #
    # May return "" if assets are served from the same domain.
    #
    # Returns the string URL ("https://blah.cloudfront.net", "", etc)
    def asset_host_url
      @asset_host_url ||= url
    end

    # Allow asset_host_url to be set as just a hostname.
    def asset_host_url=(url)
      if url.nil? || url == ""
        url = ""
      elsif url && url !~ /^https?:/
        url = "#{scheme}://#{url}"
      end
      @asset_host_url = url
    end

    # The braintree gateway URL, for #csp_connect_sources
    def braintreegateway_url
      @braintreegateway_url ||= if Rails.env.production?
        "https://api.braintreegateway.com"
      else
        "https://api.sandbox.braintreegateway.com"
      end
    end

    # The braintree analytics URL, for #csp_connect_sources
    def braintree_analytics_url
      @braintree_analytics_url ||= if Rails.env.production?
        "https://client-analytics.braintreegateway.com"
      else
        "https://client-analytics.sandbox.braintreegateway.com"
      end
    end

    # The paypal checkout URL for paypal button images, for #csp_image_sources
    def paypal_checkout_url
      @paypal_checkout_url ||= "https://checkout.paypal.com"
    end

    # The asset host to use for all email assets.
    #
    # GitHub.asset_host_url may be nil or blank when assets should be served from
    # the same origin. The mailer asset host is never blank and will fallback to
    # the full origin url.
    #
    # development: http://github.localhost
    # production:  https://github.githubassets.com
    # labs:        https://foo.review-lab.github.com
    #
    # Always returns a full URL String.
    def mailer_asset_host_url
      if asset_host_url.present?
        asset_host_url
      else
        url
      end
    end

    # This returns the identicons host used to fetch the identicons avatar.
    #
    # Returns identicon hostname as a string. This string requires
    # a '/' for joining with path parts.
    def identicons_host
      @identicons_host ||= if GitHub.enterprise?
        GitHub.url
      else
        "https://identicons.github.com"
      end
    end
    attr_writer :identicons_host

    # This returns the identicons host used by the internal api.
    #
    # Returns identicon hostname as a string. This string requires
    # a '/' for joining with path parts.
    def internal_identicons_host
      @internal_identicons_host ||= if GitHub.enterprise?
        GitHub.url
      else
        "identicon:"
      end
    end
    attr_writer :internal_identicons_host

    # The octolytics image host URL (hosted on a CDN), for #csp_img_sources
    def octolytics_collector_url
      if octolytics_collector_host
        "#{scheme}://#{octolytics_collector_host}"
      end
    end

    # The hostname of the CDN that hosts the octolytics JavaScript files
    def octolytics_collector_script_host
      @octolytics_collector_script_host ||=
        octolytics_collector_host ?
          (Rails.env.production? ? "collector-cdn.github.com" : octolytics_collector_host) :
          nil
    end

    # The internal hostname for octolytics_collector. This should be used to
    # send events from inside the GitHub network to avoid transiting the
    # internet. Note the octolytics gem requires https for non-dev environments.
    #
    # Defaults to octolytics_collector_host.
    def octolytics_collector_internal_host
      @octolytics_collector_internal_host ||= octolytics_collector_host
    end

    def varnish_enabled
      return @varnish_enabled if defined?(@varnish_enabled)
      @varnish_enabled = !GitHub.enterprise?
    end
    alias varnish_enabled? varnish_enabled
    attr_writer :varnish_enabled

    # The internal Hookshot hostname.
    #
    # Returns the URL string.
    attr_accessor :hookshot_url, :staging_hookshot_url, :hookshot_go_url, :staging_hookshot_go_url, :hookshot_admin_url
    attr_accessor :hookshot_path

    # Secret token for the Hookshot endpoint: "/hooks/:guid/:id"
    attr_accessor :hookshot_token

    # The OauthApplication#id for porter.
    def porter_app_id
      return @porter_app_id if defined?(@porter_app_id)

      app = OauthApplication.find_by(
        user_id: trusted_oauth_apps_owner,
        name:    "github-importer-production",
      )

      @porter_app_id = if app
        app.id
      elsif Rails.env.development? || Rails.env.test?
        OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
      end
    end
    attr_writer :porter_app_id

    # The URL template of an import in porter.
    attr_accessor :porter_url_template

    # The URL template of an import's stafftools in porter.
    attr_accessor :porter_repository_admin_url_template

    # The URL template of an users's stafftools in porter.
    attr_accessor :porter_user_admin_url_template

    # The URL template for the golden ticket.
    attr_accessor :porter_admin_golden_ticket_url_template

    # The token that unlocks the internal admin URLs.
    def porter_internal_api_token
      @porter_internal_api_token || "porter-development-token"
    end
    attr_writer :porter_internal_api_token

    # Returns true if porter is configured.
    def porter_available?
      porter_url_template.present? &&
        porter_repository_admin_url_template.present? &&
        porter_user_admin_url_template.present? &&
        !enterprise?
    end

    # Valid "flavors" of the `/contact` form
    #
    # Returns an Hash in the form of flavor-key => title
    def contact_form_flavors
      flavors = { "default" => "Get help with GitHub" }

      if GitHub.user_abuse_mitigation_enabled?
        flavors.merge!({
          "report-abuse"   => "Report abuse",
          "report-content" => "Report content",
        })
      end

      unless GitHub.enterprise?
        flavors.merge!({
          "dmca"                 => "Copyright claims (DMCA)",
          "dmca-notice"          => "DMCA takedown notice",
          "dmca-counter-notice"  => "DMCA counter notice",
          "privacy"              => "Privacy concerns",
          "sensitive-content"    => "Request to remove sensitive content",
          "sales"                => "Upgrade Request",
        })
      end

      flavors
    end

    # GitHub's physical office address for inclusion into
    # email footers, invoices, legal docs, etc.
    #
    # multiline - true or false.
    #
    # Returns an Array if multiline. Returns a String otherwise.
    def physical_address(multiline: false)
      address_parts = ["GitHub, Inc. 88 Colin P Kelly Jr Street", "San Francisco, CA 94107"]
      multiline ? address_parts : address_parts.join(", ")
    end

    # Email for Trade Controls appeals messages
    #
    # Returns the email String
    def trade_appeals_email
      @trade_appeals_email ||= "trade-appeals@github.com"
    end
    attr_writer :trade_appeals_email

    # Email for Marketplace messages
    #
    # Returns the email String.
    def marketplace_email
      @marketplace_email ||= "marketplace@github.com"
    end
    attr_writer :marketplace_email

    # Email for GitHub Open Source messages
    #
    # Returns the email String.
    def opensource_email
      @opensource_email ||= "opensource@github.com"
    end
    attr_writer :opensource_email

    # Email for sending out user engagement and learning materials
    #
    # Returns the email String.
    def guides_email
      @guides_email ||= "guides@github.com"
    end
    attr_writer :guides_email

    # Email for business development
    #
    # Returns the email string
    def partnerships_email
      @partnerships_email ||= "partnerships@github.com"
    end
    attr_writer :partnerships_email

    ##
    # Resque config

    # A prefix used for all resque queues. Used to segregate workers in lab
    # on staff1.
    #
    # Returns a string prefix if set, otherwise the empty string.
    def resque_queue_prefix
      @resque_queue_prefix ||= ""
    end
    attr_writer :resque_queue_prefix

    # Resident memory limit for resque worker processes. If memory usage exceeds
    # this value after a job is performed the process is shut down so that a
    # fresh process can take its place.
    #
    # Returns the memory limit in bytes or nil to signify no limit.
    def resque_graceful_memory_limit
      if defined?(@resque_graceful_memory_limit)
        @resque_graceful_memory_limit
      else
        @resque_graceful_memory_limit = nil
      end
    end
    attr_writer :resque_graceful_memory_limit

    # The camo image proxy URL. This is used to rewrite HTTP <img> tag URLs left
    # in user content (like comments and issue bodies) through the SSL image
    # proxy, avoiding browser mixed content warnings.
    #
    # This value defaults to the <asset_host_url>/img when not set explicitly.
    #
    # Returns the image proxy root URL string.
    #
    # See Also:
    #   https://github.com/atmos/camo
    #   https://github.com/github/camo
    def image_proxy_url
      @image_proxy_url ||= "#{asset_host_url}/img".freeze
    end
    attr_writer :image_proxy_url

    # The secret token key used to sign generated image proxy URLs.
    #
    # Returns the key as a String, or nil when no secret is set.
    attr_accessor :image_proxy_key

    # Determine whether img URLs should be rewritten through the camo image
    # proxy. This defaults to true when SSL is enabled and the
    # image_proxy_key value is set.
    #
    # Returns true when img URLs should be rewritten, false otherwise.
    def image_proxy_enabled?
      ssl? && image_proxy_key
    end

    # Domains who'se images will not be proxied through camo when used in user
    # content.
    def image_proxy_host_whitelist
      @image_proxy_host_whitelist ||= begin
        hosts = []

        # Allow any github.com image references. This needs to be cleaned up
        # if we want to remove 'self' from CSP img-src.
        hosts << host_name

        # Allow any references to *.githubusercontent.com sources.
        if user_content_host_name && user_content_host_name != host_name
          hosts << Regexp.new("\\." + Regexp.escape(user_content_host_name) + "\\z")
        end

        hosts
      end
    end

    # Should external images be allowed to be loaded as subresouces outside
    # of our whitelisted set?
    #
    # Enabling this enforces the img-src CSP.
    #
    # Requires Camo image proxy configuration.
    def restrict_external_images?
      return @restrict_external_images if defined?(@restrict_external_images)
      @restrict_external_images = true
    end
    attr_writer :restrict_external_images

    # Should we included our whitelisted set of third party connect hosts in our
    # connect-src CSP directive?
    #
    # Enabling this results in a set of whitelisted third party hosts being
    # added to the connect-src CSP directive.
    def allow_third_party_connect_sources?
      return @allow_third_party_connect_sources if defined?(@allow_third_party_connect_sources)
      @allow_third_party_connect_sources = true
    end
    attr_writer :allow_third_party_connect_sources

    ##
    # Gist

    # Gist is enabled by default at /gist and can be run under a subdomain by
    # setting the Gist.host_name option. Explicitly disabling gist by setting
    # this option false removes all links in the UI.
    def gist_enabled
      return @gist_enabled if defined?(@gist_enabled)
      @gist_enabled = true
    end
    alias gist_enabled? gist_enabled
    attr_writer :gist_enabled

    # Are gists allowed to be created without owners?
    #
    # This defaults to false in DotCom, true in Enterprise.
    def anonymous_gist_creation_enabled
      return @anonymous_gist_creation_enabled if defined?(@anonymous_gist_creation_enabled)
      # This will flip to false as a default after we ship the "warning" PR.
      @anonymous_gist_creation_enabled = GitHub.enterprise?
    end
    alias anonymous_gist_creation_enabled? anonymous_gist_creation_enabled
    attr_writer :anonymous_gist_creation_enabled

    # The main gist raw URL. This value typically isn't written directly but
    # should be read anywhere the raw URL for gist is required. It returns the
    # http:// or https:// version of the raw URL based on whether the #ssl attribute
    # is set.
    #
    # Returns the gist raw URL string ("https://gist.githubusercontent.com",
    # "http://gist.github.localhost", etc)
    def gist_raw_url
      ssl? ? gist_raw_https_url : gist_url
    end

    # The main gist site URL. This value typically isn't written directly but
    # should be read anywhere the root URL for gist is required. It returns the
    # http:// or https:// version of the URL based on whether the #ssl attribute
    # is set.
    #
    # Returns the gist URL string ("https://gist.github.com",
    # "http://gist.github.localhost", etc)
    def gist_url
      ssl? ? gist_https_url : gist_http_url
    end

    # The non-SSL http version of the Gist URL. Not typically used directly. Use
    # gist_url instead so that http vs. https is determined dynamically.
    def gist_http_url
      if gist_domain?
        "http://#{gist_host_name}".freeze
      else
        "#{url}/gist"
      end
    end

    # The https version of the Gist URL. Not typically used directly. Use
    # gist_url instead so that http vs. https is determined dynamically.
    def gist_https_url
      if gist_domain?
        "https://#{gist_host_name}".freeze
      else
        "#{url}/gist"
      end
    end

    # The https version of the raw Gist URL. Not typically used directly. Use
    # gist_raw_url instead so that http vs. https is determined dynamically.
    def gist_raw_https_url
      if gist_domain?
        "https://gist.#{GitHub.user_content_host_name}".freeze
      else
        gist_https_url
      end
    end

    # The URL to GitHub's graphite instance.
    attr_accessor :graphite_url

    ##
    # Caching

    # View fragment cache prefix.
    #
    # Bump this to expire all view caches.
    def fragment_cache_version
      :views10
    end

    # Cache key for task lists
    #
    # Bump this if task list rendering has changed and you need to
    # expire any cached items w/ task lists
    def task_list_cache_version
      :task_list_1
    end

    # Ajax Poller version
    #
    # Bumping this will kill any active poller clients.
    #
    # See also ajax_poll.coffee
    def ajax_poller_version
      "2"
    end

    def audit_log_export_enabled?
      !GitHub.enterprise?
    end

    # In Enterprise cluster environments, contains the primary datacenter
    # attribute.
    attr_accessor :primary_datacenter

    ##
    # elasticsearch

    # Internal: Indicates the configured level of access to Elasticsearch.
    #
    # Allows ENV config to define what level of access to Elasticsearch is
    # available. ENV["ELASTICSEARCH_ACCESS"] can be set to one of:
    #   allowed - Default, all Elasticsearch functionality is available.
    #   ignored - A "soft" disable: the app will do its best to not issue
    #             queries, and will try to display user-friendly error messages
    #             when possible.
    #   raises  - All Elasticsearch access is disabled, and any attempts to
    #             use it will result in an exception.
    #
    # The "ignored" mode is intended for site recovery in a secondary datacenter
    # rather than as a general control rod for normal operation. The "raises"
    # mode is intended for deployment in a read-only datacenter when ES indexes
    # are not available, and where any requests to search indexes are explicitly
    # not supported.
    #
    # Returns the currently configured mode as a symbol.
    # Raises TypeError if an invalid mode is configured.
    def elasticsearch_access
      if !defined?(@elasticsearch_access)
        value = GitHub.environment["ELASTICSEARCH_ACCESS"] || "allowed"
        self.elasticsearch_access = value.to_sym
      end
      @elasticsearch_access
    end

    # Set the access level for Elasticsearch. Used by an enterprise setup script
    # as well as the test suite.
    #
    # Raises TypeError if an invalid mode is provided.
    def elasticsearch_access=(mode)
      if ELASTICSEARCH_ACCESS_VALUES.include?(mode)
        @elasticsearch_access = mode
      else
        raise TypeError, "Invalid elasticsearch access mode: #{mode.inspect}"
      end
    end

    # FIXME rename
    # Convenience query methods for determining elasticsearch_access levels:
    def elasticsearch_access_allowed?
      elasticsearch_access == :allowed
    end

    def elasticsearch_access_ignored?
      elasticsearch_access == :ignored
    end

    def elasticsearch_access_raises?
      elasticsearch_access == :raises
    end

    # The elasticsearch http host and port. This is actually hitting HA Proxy which
    # will load-balance across the cluster.
    attr_accessor :elasticsearch_host

    # The elasticsearch host to use for audit logs.
    attr_accessor :elasticsearch_audit_log_host

    # Returns the URL (as a String) for communicating with ElasticSearch or
    # `nil` if the elasticsearch_host has not been set.
    def elasticsearch_url
      elasticsearch_host ? "http://#{elasticsearch_host}" : nil
    end

    # The elasticsearch query timeout.
    def es_query_timeout
      @es_query_timeout ||= "250ms"
    end
    attr_writer :es_query_timeout

    # The elasticsearch datacenter to use.
    def es_datacenter
      @es_datacenter ||= "default"
    end
    attr_writer :es_datacenter

    # The elasticsearch cluster to use for audit logs.
    def es_audit_log_cluster
      @es_audit_log_cluster ||= "default"
    end
    attr_writer :es_audit_log_cluster

    # The elasticsearch cluster to use for writes for AuditEntry.
    def es_audit_log_cluster_next
      @es_audit_log_cluster_next ||= "default"
    end
    attr_writer :es_audit_log_cluster_next

    # The Hash containing the name / config mapping for the available
    # Elasticsearch clusters. The config looks like this:
    #
    # {url: string, compress_body: bool, es_version: string}
    def es_clusters
      @es_clusters ||= {}
    end
    attr_writer :es_clusters

    # The number of shards to use when creating the `audit_log` index.
    def es_shard_count_for_audit_log
      @es_shard_count_for_audit_log ||= 1
    end
    attr_writer :es_shard_count_for_audit_log

    # The number of shards to use when creating the `code_search` index.
    def es_shard_count_for_code_search
      @es_shard_count_for_code_search ||= 1
    end
    attr_writer :es_shard_count_for_code_search

    # The number of shards to use when creating the `commits` index.
    def es_shard_count_for_commits
      @es_shard_count_for_commits ||= 1
    end
    attr_writer :es_shard_count_for_commits

    # The number of shards to use when creating the `issues` index.
    def es_shard_count_for_issues
      @es_shard_count_for_issues ||= 1
    end
    attr_writer :es_shard_count_for_issues

    # The number of shards to use when creating the `notifications` index.
    def es_shard_count_for_notifications
      @es_shard_count_for_notifications ||= 1
    end
    attr_writer :es_shard_count_for_notifications

    # The number of shards to use when creating the `issues` index.
    def es_shard_count_for_pull_requests
      @es_shard_count_for_pull_requests ||= 1
    end
    attr_writer :es_shard_count_for_pull_requests

    # The number of shards to use when creating the `topics` index.
    def es_shard_count_for_topics
      @es_shard_count_for_topics ||= 1
    end
    attr_writer :es_shard_count_for_topics

    # The number of shards to use when creating the `labels` index.
    def es_shard_count_for_labels
      @es_shard_count_for_labels ||= 1
    end
    attr_writer :es_shard_count_for_labels

    # The number of shards to use when creating the `repos` index.
    def es_shard_count_for_repos
      @es_shard_count_for_repos ||= 1
    end
    attr_writer :es_shard_count_for_repos

    # The number of shards to use when creating the `discussions` index.
    def es_shard_count_for_discussions
      @es_shard_count_for_discussions ||= 1
    end
    attr_writer :es_shard_count_for_discussions

    # The number of shards to use when creating the `team_discussions` index.
    def es_shard_count_for_team_discussions
      @es_shard_count_for_team_discussions ||= 1
    end
    attr_writer :es_shard_count_for_team_discussions

    # The number of shards to use when creating the `users` index.
    def es_shard_count_for_users
      @es_shard_count_for_users ||= 1
    end
    attr_writer :es_shard_count_for_users

    # The number of shards to use when creating the `showcases` index.
    def es_shard_count_for_showcases
      @es_shard_count_for_showcases ||= 1
    end
    attr_writer :es_shard_count_for_showcases

    # The number of shards to use when creating the `gists` index.
    def es_shard_count_for_gists
      @es_shard_count_for_gists ||= 1
    end
    attr_writer :es_shard_count_for_gists

    # The number of shards to use when creating the `job_postings` index.
    def es_shard_count_for_job_postings
      @es_shard_count_for_job_postings ||= 1
    end
    attr_writer :es_shard_count_for_job_postings

    # The number of shards to use when creating the `locations` index.
    def es_shard_count_for_locations
      @es_shard_count_for_locations ||= 1
    end
    attr_writer :es_shard_count_for_locations

    # The number of shards to use when creating the `wikis` index.
    def es_shard_count_for_wikis
      @es_shard_count_for_wikis ||= 1
    end
    attr_writer :es_shard_count_for_wikis

    # The number of shards to use when creating the `projects` index.
    def es_shard_count_for_projects
      @es_shard_count_for_projects ||= 1
    end
    attr_writer :es_shard_count_for_projects

    # The number of shards to use when creating the `non_marketplace_listings` index.
    def es_shard_count_for_non_marketplace_listings
      @es_shard_count_for_non_marketplace_listings ||= 1
    end
    attr_writer :es_shard_count_for_non_marketplace_listings

    # The number of shards to use when creating the `marketplace_listings` index.
    def es_shard_count_for_marketplace_listings
      @es_shard_count_for_marketplace_listings ||= 1
    end
    attr_writer :es_shard_count_for_marketplace_listings

    # The number of shards to use when creating the `repository_actions` index.
    def es_shard_count_for_repository_actions
      @es_shard_count_for_repository_actions ||= 1
    end
    attr_writer :es_shard_count_for_repository_actions

    # The number of shards to use when creating the `registry_packages` index.
    def es_shard_count_for_registry_packages
      @es_shard_count_for_registry_packages ||= 1
    end
    attr_writer :es_shard_count_for_registry_packages

    # The number of shards to use when creating the `rms_packages` index.
    def es_shard_count_for_rms_packages
      @es_shard_count_for_rms_packages ||= 1
    end
    attr_writer :es_shard_count_for_registry_packages

    # The number of shards to use when creating the `github_apps` index.
    def es_shard_count_for_github_apps
      @es_shard_count_for_github_apps ||= 1
    end
    attr_writer :es_shard_count_for_github_apps

    # The number of shards to use when creating the `vulnerabilities` index.
    def es_shard_count_for_vulnerabilities
      @es_shard_count_for_vulnerabilities ||= 1
    end
    attr_writer :es_shard_count_for_vulnerabilities

    # The number of shards to use when creating the `enterprises` index.
    def es_shard_count_for_enterprises
      @es_shard_count_for_enterprises ||= 1
    end
    attr_writer :es_shard_count_for_enterprises

    # The number of shards to use when creating the `workflow_runs` index.
    def es_shard_count_for_workflow_runs
      @es_shard_count_for_workflow_runs ||= 1
    end
    attr_writer :es_shard_count_for_workflow_runs

    # Auto expand replicas for each search index.
    def es_auto_expand_replicas
      @es_auto_expand_replicas ||= false
    end
    attr_writer :es_auto_expand_replicas

    # The number of replicas to maintain for each search index.
    def es_number_of_replicas
      @es_number_of_replicas ||= 2
    end
    attr_writer :es_number_of_replicas

    # The read timeout for Elasticsearch connections in seconds.
    # Defaults to 3 seconds.
    def es_read_timeout
      @es_read_timeout ||= 3
    end
    attr_writer :es_read_timeout

    # The connection open timeout for Elasticsearch connections in seconds.
    # Defaults to 2 seconds.
    def es_open_timeout
      @es_open_timeout ||= 2
    end
    attr_writer :es_open_timeout

    # The maximum file size that we will index in the code-search index. Source
    # code files larger than this limit will _not_ have their contents indexed
    # and searchable. Other meta-data about the source code files (filename,
    # extension, language, etc) will still be searchable.
    def es_max_doc_size
      @es_max_doc_size ||= 384*1024  # 384KB
    end
    attr_writer :es_max_doc_size

    # These are the fields that won't be used to compute a has
    # version of an ES index.
    # e.g: %i(number_of_replicas auto_expand_replicas)
    def es_skip_settings_fields
      @es_skip_settings_fields ||= []
    end
    attr_writer :es_skip_settings_fields

    # Whether the Elasticsearch audit logger should be used.
    # This is only used in Enterprise for now. This
    # will be set by the ENTERPRISE_AUDIT_LOG_ES_LOGGER_ENABLED env variable
    def audit_log_es_logger_enabled?
      @audit_log_es_logger_enabled.nil? ? true : @audit_log_es_logger_enabled
    end
    attr_writer :audit_log_es_logger_enabled

    ##
    # Subversion

    # The Subversion / Slummin root URL. This value typically isn't written
    # directly but should be read anywhere the root URL for the subversion
    # server is required. It returns the http:// or https:// version of the
    # URL based on whether the #ssl config attribute is set.
    #
    # Returns the svn URL string ("https://svn.github.com",
    # "http://svn.github.localhost", etc)
    def subversion_url
      @subversion_url ||= "#{scheme}://#{subversion_host_name}".freeze
    end
    attr_writer :subversion_url

    def site_status_url
      "https://www.githubstatus.com"
    end

    def show_site_status?
      !enterprise?
    end

    # Whether to include Google site verification tags like this into pages:
    #
    # <meta name="google-site-verification" content="...">
    def site_verification_enabled?
      !enterprise?
    end

    ##
    # Google Analytics

    # The Google Analytics account in "UA-XXXXXXX-X" form. This is used in the
    # JavaScript tracking code inserted into pages with analytics enabled.
    attr_accessor :analytics_account
    attr_accessor :gist_analytics_account

    # Determine if Google Analytics tracking information should be inserted into
    # page output. Analytics tracking is considered on if the analytics_account
    # attribute is set.
    #
    # Returns true if tracking is enabled, false otherwise.
    def analytics_enabled?
      !analytics_account.nil?
    end

    # When users view private repos on dotcom, then we don't want to leak the
    # repo name, owner, or file paths to Google Analytics.
    def anonymized_private_repo_analytics?
      !enterprise?
    end

    # Determine if Google Analytics should track ecommerce purchase events
    def track_ecommerce_analytics?
      !enterprise?
    end

    # Get base URL for Google Analytics.
    #
    # Returns String URL or nil if Google Analytics is disabled.
    def google_analytics_url
      if analytics_enabled?
        "#{scheme}://www.google-analytics.com"
      end
    end

    # Third party hosts being added to the connect-src CSP directive.
    #
    # Needs to be enabled via allow_third_party_connect_sources.
    #
    # IMPORTANT!!! CC @github/dotcom-security if you need to change this.
    def third_party_connect_sources
      return @third_party_connect_sources if defined?(@third_party_connect_sources)

      @third_party_connect_sources = [
        google_analytics_url,
        "https://github-cloud.s3.amazonaws.com",
        "https://#{GitHub.s3_repository_file_new_host}",
        "https://#{GitHub.s3_upload_manifest_file_new_host}",
        "https://#{GitHub.s3_user_asset_new_host}",
        GitHub::Optimizely::Config::DATAFILE_URL,
        GitHub::Optimizely::Config::EVENTS_URL,
      ]

      @third_party_connect_sources.freeze
    end

    # Custom setter to freeze the Object before setting it,
    # to prevent future modifications.
    def third_party_connect_sources=(sources)
      @third_party_connect_sources = Array(sources).freeze
    end

    # LiveReload server URL that is added to the connect-src CSP directive in development.
    def livereload_url
      "ws://127.0.0.1:35729/livereload".freeze if Rails.development?
    end

    ##
    # Octolytics, the new traffic analyisis tool.
    # App ID
    attr_accessor :octolytics_app_id
    # Collector host for external clients
    attr_accessor :octolytics_collector_host
    # Collector host for internal clients (inside the GitHub network)
    attr_writer   :octolytics_collector_internal_host
    # Secret, for signing content that passes through the browser (e.g. the user id).
    attr_accessor :octolytics_secret
    # What octolytics environment should be used to pull report data from.
    attr_accessor :octolytics_reporter_env
    # Gist App ID
    attr_accessor :gist_octolytics_app_id
    # Gist Secret, for signing content that passes through the browser (e.g. the user id).
    attr_accessor :gist_octolytics_secret

    # Secret, for signing the visitor meta information
    attr_accessor :visitor_secret

    # Returns true if tracking on analytics is configured, false otherwise.
    def octolytics_enabled?
      octolytics_collector_host.present?
    end

    ##
    # Pond
    attr_accessor :pond_shared_secret

    # The Google Analytics username and password. This is used to fetch traffic
    # data for specific repositories only.
    attr_accessor :analytics_user
    attr_accessor :analytics_pass

    ##
    # Twitter

    # The Twitter OAuth access key as a String.
    attr_accessor :twitter_key

    # The Twitter OAuth secret key as a String.
    attr_accessor :twitter_secret

    ##
    # Billing/Braintree

    # Determine whether billing is enabled. Typically disabled under
    # the enterprise environment. Enable via `ghe-config` by setting
    # BILLING_ENABLED to `1`, and disable with `0`. Defaults to true if
    # no value is set.
    #
    # Returns true if billing is enabled, false otherwise.
    def billing_enabled?
      return false if enterprise?
      return @billing_enabled if defined?(@billing_enabled)
      @billing_enabled = ENV.fetch("BILLING_ENABLED", "1") == "1"
    end
    attr_writer :billing_enabled

    # Determine whether we want to make a request to Braintree for a client token use in PayPal
    # forms. Usually false for the :test environment.
    #
    # Returns true if client tokens are enabled
    def braintree_client_token_enabled?
      billing_enabled? && @braintree_client_token_enabled
    end
    attr_accessor :braintree_client_token_enabled

    # Determine the plan name assigned to users by default. Usually set to
    # "enterprise" under enterprise or "free" for others.
    #
    # See also GitHub::Plan and User#plan.
    #
    # Returns the String plan name.
    def default_plan_name
      return @default_plan_name if defined?(@default_plan_name)
      @default_plan_name =
        if enterprise?
          GitHub::Plan::ENTERPRISE
        else
          GitHub::Plan::FREE
        end
    end
    attr_writer :default_plan_name

    # Restrict signup to the default plan when enabled. Enabled by default
    # under FI environments, disabled everywhere else.
    #
    # Returns true if enabled, false otherwise.
    def enforce_default_plan?
      return @enforce_default_plan if defined?(@enforce_default_plan)
      @enforce_default_plan = enterprise?
    end
    attr_writer :enforce_default_plan

    # Braintree configuration.
    attr_accessor :braintree_environment
    attr_accessor :braintree_merchant_id
    attr_accessor :braintree_public_key
    attr_accessor :braintree_private_key
    attr_accessor :braintree_logger
    attr_accessor :braintree_client_side_encryption_key
    attr_accessor :braintree_host

    # Zuora configuration
    attr_accessor :zuora_rest_server
    attr_accessor :zuora_access_key_id
    attr_accessor :zuora_secret_access_key
    attr_accessor :zuora_client_id
    attr_accessor :zuora_client_secret
    attr_accessor :zuorest_client
    attr_accessor :zuora_lfs_rate_plan_charge_ids
    attr_accessor :zuora_metered_refill_rate_plan_charge_ids
    attr_accessor :zuora_sales_serve_actions_product_charge_ids
    attr_accessor :zuora_sales_serve_packages_product_charge_ids
    attr_accessor :zuora_sales_serve_shared_storage_product_charge_ids
    attr_accessor :zuora_webhook_username
    attr_accessor :zuora_webhook_password
    attr_accessor :zuora_webhook_new_password
    attr_accessor :zuora_payment_page_server
    attr_accessor :zuora_payment_page_uri
    attr_accessor :zuora_payment_page_id
    attr_accessor :zuora_self_serve_payment_page_id
    attr_accessor :zuora_self_serve_join_payment_page_id
    attr_accessor :zuora_self_serve_communication_profile_id
    attr_accessor :zuora_host
    attr_accessor :zuora_self_serve_compact_payment_page_id
    attr_accessor :zuora_self_serve_join_compact_payment_page_id

    # Stripe configuration
    attr_accessor :stripe_api_key
    attr_accessor :stripe_client_id
    attr_accessor :stripe_platform_webhook_secret
    attr_accessor :stripe_connect_webhook_secret
    attr_accessor :stripe_request_identity_secret

    # Apple IAP configuration
    attr_accessor :apple_iap_shared_secret

    # VSS subscription service bus configuration
    attr_accessor :vss_subscription_events_namespace
    attr_accessor :vss_subscription_events_queue_name
    attr_accessor :vss_subscription_events_sas_key_name
    attr_accessor :vss_subscription_events_sas_key

    # DocuSign configuration
    attr_accessor :docusign_webhook_base_url
    attr_accessor :docusign_w9_template_id
    attr_accessor :docusign_w8ben_template_id
    attr_accessor :docusign_w8ben_e_template_id
    attr_accessor :docusign_user_id
    attr_accessor :docusign_client_id
    attr_accessor :docusign_account_id
    attr_accessor :docusign_private_key
    attr_accessor :docusign_connect_secret_key
    attr_accessor :docusign_api_base_path

    # Open Exchange Rates
    attr_accessor :open_exchange_rates_app_id

    # Vimeo client configuration.
    attr_accessor :vimeo_client_id
    attr_accessor :vimeo_client_secret

    # Zendesk client configuration.
    attr_accessor :zendesk_api_url
    attr_accessor :zendesk_api_token
    attr_accessor :zendesk_brand_id
    attr_accessor :zendesk_fields

    # Ghost user login used to replace associated users of Issues-related
    # records (Issue, IssueComment and IssueEvent) when the original user
    # is deleted.
    #
    # Returns the String ghost User login.
    def ghost_user_login
      @ghost_user_login ||= "ghost"
    end

    # Staff user login used to be the actor for staff events
    #
    # Returns the String staff User login
    def staff_user_login
      @staff_user_login ||= "github-staff"
    end

    ##
    # Authentication

    attr_accessor :builtin_auth_fallback

    attr_accessor :cas_url

    attr_accessor :ldap_host, :ldap_port, :ldap_base,
                  :ldap_bind_dn, :ldap_password, :ldap_method,
                  :ldap_search_strategy,
                  :ldap_virtual_attributes, :ldap_virtual_attribute_member,
                  :ldap_recursive_group_search_fallback,
                  :ldap_posix_support,
                  :ldap_sync_enabled,
                  :ldap_user_sync_emails, :ldap_user_sync_keys, :ldap_user_sync_gpg_keys,
                  :ldap_profile_uid, :ldap_profile_name, :ldap_profile_mail,
                  :ldap_profile_key, :ldap_profile_gpg_key

    attr_accessor :saml_sso_url,           # idP http endpoint for sso. We redirect to here with an AuthnRequest.
                  :saml_idp_initiated_sso, # if this is on, responses will not be checked for corresponding requests
                  :saml_disable_admin_demote, # if this is on, admin bit is ignored and users won't promoted or demoted.
                  :saml_issuer,            # idP issuer. Used to validate responses
                  :saml_name_id_format,    # NameID format to use, (:persistent or unspecified). Unspecified should be discouraged
                  :saml_certificate_file,  # idP certificate. Public key to validate idP responses.
                  :saml_signature_method,  # algorithm to use for AuthnRequest signatures.
                  :saml_digest_method,     # algotithm to use for AuthnRequest digests.
                  :saml_sp_pkcs12_file,    # SP keypair for signing AuthnRequests and Metadata.
                  :saml_admin,             # attribute name in saml response for promoting/demoting admins. Default: 'administrator'
                  :saml_username_attr,     # attribute username in saml response for account login name. Default: 'NameID'
                  :saml_profile_name,      # attribute name in saml response for user full name. Default: 'full_name'
                  :saml_profile_mail,      # attribute name in saml response for user emails. Default: 'emails'
                  :saml_profile_ssh_key,   # attribute name in saml response for user public keys. Default: 'public_keys'
                  :saml_profile_gpg_key,   # attribute name in saml response for user GPG keys. Default: 'gpg_keys'

    # Public: Returns the verify_mode value used for SSL communications with
    # LDAP server.
    #
    # See http://ruby-doc.org/stdlib/libdoc/openssl/rdoc/OpenSSL/SSL/SSLContext.html#verify_mode
    # for available verify_mode values
    def ldap_tls_verify_mode
      @ldap_tls_verify_mode ||= OpenSSL::SSL::VERIFY_NONE
    end

    # Public: Sets the TLS verification mode. Value is expected to be an integer
    #
    # See http://ruby-doc.org/stdlib/libdoc/openssl/rdoc/OpenSSL/SSL/SSLContext.html#verify_mode
    # for available verify_mode values
    def ldap_tls_verify_mode=(verify_mode)
      @ldap_tls_verify_mode = verify_mode.to_i
    end

    # Public: Returns true when LDAP authentication is configured and LDAP Sync is enabled.
    # Managed via enterprise-manage.
    def ldap_sync_enabled?
      auth.ldap? && ldap_sync_enabled
    end

    # Authentication adapter options hash. See the adapter implementations under
    # GitHub::Authentication for information on possible options.
    def auth_options
      if auth_mode.to_sym == :saml
        {
          builtin_auth_fallback: builtin_auth_fallback,
          sso_url: saml_sso_url,
          idp_initiated_sso: saml_idp_initiated_sso,
          disable_admin_demote: saml_disable_admin_demote,
          issuer: saml_issuer,
          name_id_format: saml_name_id_format,
          signature_method: saml_signature_method,
          digest_method: saml_digest_method,
          idp_certificate_file: saml_certificate_file,
          sp_pkcs12_file: saml_sp_pkcs12_file,
          admin: saml_admin,
          user_name: saml_username_attr,
          display_name: saml_profile_name,
          emails: saml_profile_mail,
          ssh_keys: saml_profile_ssh_key,
          gpg_keys: saml_profile_gpg_key,
          sp_url: url,
        }
      else
        {
          builtin_auth_fallback: builtin_auth_fallback,
          # Used for SAML metdata, which should be available through non-SAML
          # auth adaptors so that it can be retrieved before fully configuring SAML.
          sp_url: url,
          name_id_format: saml_name_id_format || "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent",
        }
      end
    end

    # Default to true if config is not set and if LDAP is used for authentication
    def reactivate_suspended_user?
      return false if !GitHub.enterprise?
      return false if !GitHub.auth.external?

      unless GitHub.config.get("auth.reactivate-suspended").nil?
        return GitHub.config.enabled?("auth.reactivate-suspended")
      end

      return GitHub.auth.ldap?
    end

    # Default to true if config is not set and if LDAP is used for authentication
    def reactivate_suspended_user_on_sync?
      return false if !GitHub.enterprise?
      return false if !GitHub.auth.external?

      unless GitHub.config.get("auth.reactivate-suspended-on-sync").nil?
        return GitHub.config.enabled?("auth.reactivate-suspended-on-sync")
      end

      # If the config is not set then make it consistent with the reactivate suspended user config.
      # This is to ensure that customers that have reactivate suspended user disabled don't see a change of behavior
      # when upgrading to a GHES version with the new config
      return GitHub.reactivate_suspended_user?
    end

    # Setting to disable password authentication for LDAP for Git operations
    def external_auth_token_required
      return false if !GitHub.enterprise?
      return false if !GitHub.auth.external?
      # External auth mechs other than LDAP do not support password auth
      return true if !GitHub.auth.ldap?

      return @external_auth_token_required if defined?(@external_auth_token_required)
      return false
    end
    attr_writer :external_auth_token_required
    alias :external_auth_token_required? :external_auth_token_required

    # Algorithm to use for the SAML signature.
    #
    # As per the spec, https://www.w3.org/TR/xmldsig-core1/#sec-AlgID, there are
    # several values to cater for with: rsa-sha1 (discouraged), rsa-sha256,
    # rsa-sha384 and rsa-sha512 being the most commonly implemented.
    #
    # Default to rsa-sha256 as this is the recommended replacement for rsa-sha1.
    def saml_signature_method
      @saml_signature_method ||= "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"
    end

    # Allow the setting of one of four options with rsa-sha256 being enforced if
    # an invalid value is given.
    def saml_signature_method=(value)
      @saml_signature_method =
        case value
        when "rsa-sha1"
          "http://www.w3.org/2000/09/xmldsig#rsa-sha1"
        when "rsa-sha384", "rsa-sha512"
          "http://www.w3.org/2001/04/xmldsig-more##{value}"
        else
          "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"
        end
    end

    # Algorithm to use for the SAML digest.
    #
    # As per the spec, https://www.w3.org/TR/xmldsig-core1/#sec-AlgID, there are
    # several values to cater for with: sha1 (discouraged), sha256, and sha512
    # being the most commonly implemented.
    #
    # Default to sha256 as this is the recommended replacement for sha1.
    def saml_digest_method
      @saml_digest_method ||= "http://www.w3.org/2001/04/xmlenc#sha256"
    end

    # Allow the setting of one of five options with sha256 being enforced if
    # an invalid value is given.
    def saml_digest_method=(value)
      @saml_digest_method =
        if value == "sha1"
          "http://www.w3.org/2000/09/xmldsig#sha1"
        elsif value == "sha512"
          "http://www.w3.org/2001/04/xmlenc#sha512"
        else
          "http://www.w3.org/2001/04/xmlenc#sha256"
        end
    end

    def saml_name_id_format=(value)
      @saml_name_id_format =
        if value == "unspecified"
          "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified"
        else
          "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
        end
    end

    def sso_credential_authorization_help_url
      @sso_credential_authorization_help_url ||= GitHub.environment["GITHUB_SSO_CREDENTIAL_AUTHORIZATION_HELP_URL"] ||
        "#{GitHub.help_url}/articles/authenticating-to-a-github-organization-with-saml-single-sign-on/"
    end

    def iam_with_saml_sso_help_url
      @iam_with_saml_sso_help_url ||= GitHub.environment["GITHUB_IAM_WITH_SAML_SSO_HELP_URL"] ||
        GitHub.help_url
    end

    def ldap_base=(ldap_base)
      if !ldap_base.respond_to?(:each)
        # If the domain base list comes from an environment variable we need to split it up.
        ldap_base = String(ldap_base).split(";")
      end
      @ldap_base = ldap_base
    end
    attr_reader :ldap_base

    # Public: LDAP Admin group defines which users are administrators based on
    # group membership.
    def ldap_admin_group=(group)
      @ldap_auth_groups = nil
      @ldap_admin_group = group
    end
    attr_reader :ldap_admin_group

    # Public: LDAP user groups defines membership requirements for
    # authenticating users.
    def ldap_user_groups=(ldap_user_groups)
      if !ldap_user_groups.respond_to?(:each)
        # If the user groups list comes from an environment variable we need to split it up.
        ldap_user_groups = String(ldap_user_groups).split(";")
      end
      ldap_user_groups.compact!
      @ldap_auth_groups = nil
      @ldap_user_groups = ldap_user_groups
    end
    attr_reader :ldap_user_groups

    # Public: LDAP Groups users must belong to in order to successfully
    # authenticate.
    #
    # Returns an empty Array if authentication is not scoped to groups.
    # Returns an Array of group String names, including the admin group.
    def ldap_auth_groups
      return [] if ldap_user_groups.blank?
      @ldap_auth_groups ||= begin
        groups = ldap_user_groups + Array(ldap_admin_group)
        groups.uniq!
        groups.reject!(&:blank?)
        groups
      end
    end

    # Public: LDAP Search Strategy for Team Sync Member Search and
    # authentication restricted group Membership Validation.
    #
    # Defaults to `detect` to force detection of the optimal strategy.
    def ldap_search_strategy
      @ldap_search_strategy ||= "detect"
    end

    # Public: Defines the maximum depth of recursion the Recursive search
    # strategy can descend. Only used to the Recursive strategy.
    #
    # Pulls the value from the `ldap.search_strategy_depth` global config.
    #
    # Returns nil or the configured Integer depth.
    def ldap_search_strategy_depth
      if depth = GitHub.config.get("ldap.search_strategy_depth")
        depth.to_i
      end
    end

    # The LDAP User Sync job interval in Integer of hours.
    # Defaults to every 4 hours. See: LdapUserSyncJob.
    def ldap_user_sync_interval
      @ldap_user_sync_interval ||= 4
    end

    # Set the LDAP User Sync job interval.
    # Requires an interval greater than zero.
    def ldap_user_sync_interval=(interval)
      @ldap_user_sync_interval = interval.to_i if interval.to_i > 0
    end

    # The LDAP Team Sync job interval in Integer of hours.
    # Defaults to every 4 hours. See: LdapTeamSyncJob.
    def ldap_team_sync_interval
      @ldap_team_sync_interval ||= 4
    end

    # Set the LDAP Team Sync job interval.
    # Requires an interval greater than zero.
    def ldap_team_sync_interval=(interval)
      @ldap_team_sync_interval = interval.to_i if interval.to_i > 0
    end

    # The maximum number of concurrent jobs per subject to export
    # git events.
    def audit_git_export_max_concurrent_jobs_subject
      @audit_git_export_max_concurrent_jobs_subject ||= 10
    end

    # Set the maximum number of concurrent jobs per subject to export
    # git events.
    def audit_git_export_max_concurrent_jobs_subject=(jobs)
      @audit_git_export_max_concurrent_jobs_subject = jobs.to_i if jobs.to_i > 0
    end

    # The timeout for audit log jobs that export git events
    def audit_git_export_timeout
      @audit_git_export_timeout_minutes ||= 20.minutes
    end

    # Set the timeout for audit log jobs that export git events
    def audit_git_export_timeout=(timeout)
      @audit_git_export_timeout_minutes = timeout.to_i if timeout.to_i > 0
    end

    # Whether the LDAP server supports virtual attributes like memberOf.
    def ldap_virtual_attributes
      return @ldap_virtual_attributes if defined?(@ldap_virtual_attributes)
      @ldap_virtual_attributes = false
    end

    # Whether the LDAP group membership check should fallback to the slow non
    # virtual attribute implementation when virtual attributes are disabled.
    # This is false by default to prevent bad performance.
    def ldap_recursive_group_search_fallback
      return @ldap_recursive_group_search_fallback if defined?(@ldap_recursive_group_search_fallback)
      @ldap_recursive_group_search_fallback = false
    end

    # Whether the LDAP group membership check should include posixGroup
    # conditions.
    # This is true by default.
    def ldap_posix_support
      return @ldap_posix_support if defined?(@ldap_posix_support)
      @ldap_posix_support = true
    end

    # The amount of time we allow for LDAP authentication requests before timing out
    def ldap_auth_timeout
      @ldap_auth_timeout ||= 10
    end

    # Set the amount of time we allow for LDAP authentication requests before timing out
    def ldap_auth_timeout=(value)
      @ldap_auth_timeout = value if value > 0 && value <= GitHub.default_request_timeout
    end

    def auth_mode
      if private_instance_user_provisioning?
        return :private_instance_runtime
      end

      @auth_mode ||= :default
    end

    def auth_mode=(mode)
      @auth_adaptor = nil
      @auth_mode = mode
    end

    # default password for user
    attr_accessor :default_password

    # Historically, the minimum password length is 7. This is still the default
    # in every environment besides dotcom production where 8 characters are
    # required.
    def password_minimum_length
      @password_minimum_length ||= 7
    end
    attr_writer :password_minimum_length

    def employee_password_minimum_length
      @employee_password_minimum_length ||= password_minimum_length
    end
    attr_writer :employee_password_minimum_length

    def employee_password_minimum_entropy
      @employee_password_minimum_entropy ||= 0
    end
    attr_writer :employee_password_minimum_entropy

    def password_lowercase_requirement
      @password_lowercase_requirement ||= 1
    end
    attr_writer :password_lowercase_requirement

    def password_uppercase_requirement
      @password_uppercase_requirement ||= 0
    end
    attr_writer :password_uppercase_requirement

    def password_digit_requirement
      @password_digit_requirement ||= 1
    end
    attr_writer :password_digit_requirement

    def password_special_character_requirement
      @password_special_character_requirement ||= 0
    end
    attr_writer :password_special_character_requirement


    # Instantiate the Authentication object for handling auth configuration and inquiries.
    #
    # Returns an Authentication.
    def auth_modes
      @auth_modes ||= {
        default: GitHub::Authentication::Default,
        ldap: GitHub::Authentication::LDAP,
        cas: GitHub::Authentication::CAS,
        github_oauth: GitHub::Authentication::GitHubOauth,
        saml: GitHub::Authentication::SAML,
        private_instance_runtime: GitHub::Authentication::PrivateInstanceRuntime,
      }
    end

    # The instantiated Authentication adapter.
    def auth
      @auth_adaptor ||= auth_modes[auth_mode.to_sym].new(auth_options)
    end

    # Is the site admin role managed by an external authentication system
    # (currently either LDAP or SAML)?
    #
    # If managed externally, we generally don't allow manual promoting/demoting
    # site admins.
    #
    # Returns a Boolean.
    def site_admin_role_managed_externally?
      return false if GitHub.private_instance_user_provisioning?
      (GitHub.auth.ldap? && GitHub.ldap_admin_group.present?) ||
        (GitHub.auth.saml? && !GitHub.saml_disable_admin_demote)
    end

    # Determines whether this is the very first time an Enterprise installation
    # is being accessed. This is primarily used to determine whether or not the
    # user being created should be auto-promoted to site admin status (the first
    # user on installations are auto-promoted). You can pass in the env variable
    # ENTERPRISE_FIRST_RUN to emulate this in development (hence the attr_writer).
    #
    # Returns true if no users have been created yet on the installation.
    def enterprise_first_run?
      # This nil check instead of `defined?` allows the attr_writer to reset
      # this memoized value in tests by setting first_run to nil.
      if @first_run.nil?
        license = GitHub.enterprise? ? GitHub::Enterprise.license(sync_global_business: false) : nil
        if license && license.seats_used == 0
          # Don't memoize a true value, this needs to be checked until
          # seats_used is greater than zero.
          true
        else
          # Now that there is at least one seat used, no more checks are
          # required. Memoize the result.
          @first_run = false
        end
      else
        @first_run
      end
    end
    attr_writer :first_run

    # Available log levels.
    RAILS_LOG_LEVELS = [:debug, :info, :warn, :error, :fatal]

    # The log level used by the application. Possible log levels are :debug, :info,
    # :warn, :error, and :fatal. This value is set by github/config/environments/<env>.rb
    # scripts but can be adjusted under enterprise by the ENTERPRISE_RAILS_LOG_LEVEL
    # environment variable or by modifying the RAILS_ROOT/config.yml file.
    #
    # Returns the configured log level as a symbol. If no log level is set
    # explicitly, :info is returned.
    def rails_log_level
      @rails_log_level ||= :info
    end

    # Set the rails_log_level, verifying the value given.
    #
    # value - One of the LOG_LEVEL symbol values or a number between 0 and 4.
    #
    # Raises a TypeError when the value is not a supported log level.
    def rails_log_level=(value)
      value = value.to_i if value.is_a?(String) && value =~ /[0-4]/
      value = value.to_sym if value.is_a?(String)
      value = RAILS_LOG_LEVELS[value] if value.is_a?(Integer)

      if RAILS_LOG_LEVELS.include?(value)
        @rails_log_level = value
      else
        raise TypeError, "Illegal value: #{value.inspect}"
      end
    end

    # Unicorn master/worker attributes
    attr_accessor :unicorn_master_start_time
    attr_accessor :unicorn_worker_start_time
    attr_accessor :unicorn_master_pid
    attr_accessor :unicorn_worker_request_count

    # Path to a `cpu.stat` file as pertains to this unicorn process, computed
    # at unicorn startup time.  May be `nil` for some environments to indicate
    # that the node is not accessible.
    # See also https://www.kernel.org/doc/Documentation/scheduler/sched-bwc.txt.
    attr_accessor :cpu_stat_file

    # Determine whether Private Mode is enabled for GitHub (FI). Private Mode
    # locks down the site- users must be logged in to view and use GitHub, and
    # new user signups are restricted to admins only. This also disables
    # various other functionality for the sake of a private GitHub (such as
    # serving repositories over git://). It also causes all atom feeds to include
    # login/token parameters, just like private feeds (see github/enterprise#244).
    #
    # Returns a Boolean.
    def private_mode
      return @private_mode if defined?(@private_mode)
      @private_mode = GitHub.enterprise? && !(ENV["PRIVATE_MODE"]).to_s.empty?
    end
    attr_writer :private_mode

    # Is Private Mode enabled?
    def private_mode_enabled?
      private_mode
    end

    # Determines whether or not we want to guard actions performed through
    # stafftools under an dedicated staff user.
    #
    # Returns a Boolean.
    def guard_audit_log_staff_actor?
      return @guard_audit_log_staff_actor if defined?(@guard_audit_log_staff_actor)
      @guard_audit_log_staff_actor = !GitHub.enterprise?
    end
    attr_writer :guard_audit_log_staff_actor

    def guarded_audit_log_staff_actor_entry(actor)
      if actor.is_a?(User)
        actor_id = actor.id
        actor = actor.login
      end

      entry = {}
      if guard_audit_log_staff_actor?
        entry[:staff_actor]    = actor    if actor
        entry[:staff_actor_id] = actor_id if actor_id
        entry[:actor]          = User.staff_user.login
        entry[:actor_id]       = User.staff_user.id
      else
        entry[:actor]    = actor    if actor
        entry[:actor_id] = actor_id if actor_id
      end
      entry
    end

    # Guard audit log entries for support operations performed through the console
    def guard_console_staff_actor!
      supportocat = if Rails.development?
          User.find_by_login(ENV["USER"])
        elsif GitHub.enterprise?
          User.find_by_login("ghost")
        else
          User.find_by_login(ENV["SUDO_USER"]) #production shell server
      end
      @guard_console_staff_actor = {actor: supportocat.login, actor_id: supportocat.id}
      IRB.CurrentContext.irb_name = "#{User.staff_user.login}%#{supportocat.login}"
    end

    def guard_console_staff_actor
      @guard_console_staff_actor || {actor: nil, actor_id: nil}
    end
    attr_writer :guard_console_staff_actor

    def guard_console_staff_actor?
      guard_console_staff_actor[:actor]
    end

    # Determine whether subdomain isolation is enabled for GitHub (FI).
    # Subdomain isolation places various components of GitHub (raw, uploads,
    # pages, etc) on their own subdomain. This prevents user controlled content
    # from executing in the same origin as the rest of GitHub. As a result,
    # attacks such as XSS on these subdomains are much less critical as they
    # will be unable to access content on the main GitHub site and will not be
    # able to perform security sensitive operations (accessing repos, etc).
    #
    # Returns a Boolean.
    def subdomain_isolation
      return @subdomain_isolation if defined?(@subdomain_isolation)
      @subdomain_isolation = true
    end
    attr_writer :subdomain_isolation
    alias :subdomain_isolation? :subdomain_isolation

    # Using Private Mode and Subdomains for maximum privacy and security.
    #
    # Causes private mode authentication cookies to be set on *.githubhostname.
    #
    # Enabled when both using Enterprise isolated subdomains and Private Mode.
    def subdomain_private_mode_enabled?
      private_mode_enabled? && subdomain_isolation?
    end

    # Public: Do we set `crossorigin=with-credentials` for asset bundles in
    # this environment?
    #
    # Returns a Boolean.
    def asset_crossorigin_with_credentials?
      enterprise?
    end

    # Secret used for HMACing request information for proxy site detection.
    attr_accessor :proxy_site_detection_secret

    # Determine whether to create repositories in DGit by default
    #
    # Returns true if it's enabled, false otherwise
    def dgit_intake_enabled?
      if defined?(@dgit_intake_enabled)
        return @dgit_intake_enabled
      end
      @dgit_intake_enabled = true
    end
    attr_writer :dgit_intake_enabled

    # ID of the GitHub Enterprise Site Administrator Oauth App ID, which is
    # the owner of Api::Admin::UsersManager tokens used for user impersonation.
    #
    # Returns int ID, or nil if the app doesn't exist and can't be created
    def enterprise_admin_oauth_app_id
      return nil unless GitHub.enterprise?
      @enterprise_admin_oauth_app_id ||=
        if app = OauthApplication.where({
          user_id: GitHub.trusted_apps_owner_id,
          name: "GitHub Site Administrator",
          full_trust: true,
          }).first
          app.id
        elsif app = OauthApplication.register_trusted_application(
          "Site Administrator",
          SecureRandom.hex(10),
          SecureRandom.hex(20),
          "https://developer.github.com/v3/enterprise/users/",
          "https://developer.github.com/v3/enterprise/users/",
          )
          app.id
        else
          nil
        end
    end

    ##
    # Custom Hooks (Enterprise)

    # Location where custom git hooks data lives.
    def custom_hooks_dir
      @custom_hooks_dir ||= if Rails.production?
        "/data/user/git-hooks"
      elsif Rails.test?
        ENV["TEST_HOOK_DIR"] || "#{Rails.root}/git-hooks"
      else
        "#{Rails.root}/git-hooks"
      end
    end
    attr_writer :custom_hooks_dir

    def pre_receive_hooks_enabled?
      GitHub.enterprise?
    end

    # The password cost factor for Bcrypt
    def password_cost
      @password_cost ||= BCrypt::Engine::DEFAULT_COST
    end
    attr_writer :password_cost

    # The key we use to encrypt two-factor secrets in the database
    attr_accessor :two_factor_salt

    # The key we use to encrypt SAML provider recovery keys in the database
    attr_accessor :saml_provider_salt

    # U2F instance for FIDO U2F authentication.
    #
    # Returns a U2F::U2F instance.
    def u2f
      @u2f ||= U2F::U2F.new(u2f_app_id)
    end

    # The app id to use for FIDO U2F. This points to
    # u2f_registrations#trusted_facets which tells U2F devices to trust a set
    # of domains to share registrations.
    #
    # Returns a String URL.
    def u2f_app_id
      "#{url}/u2f/trusted_facets"
    end

    # Calculates whether a security key operation is allowed on the given
    # origin. Whitelisted domains are:
    #
    # - The U2F "trusted facets" domains. For github.com, these are (per
    #   https://github.com/u2f/trusted_facets):
    #   - https://github.com
    #   - https://garage.github.com
    #   - https://admin.github.com
    # - Dynamic lab domains
    #
    # Returns a Boolean.
    def webauthn_allowed_origin?(origin)
      # Ideally we would get the host name of the deployment and pass this to
      # the webauthn library instead of checking the origin ourselves.
      # `GitHub.host_name` is supposed to tell us that host name, but it
      # actually... doesn't (https://github.com/github/github/issues/113982). We
      # could try compute the expected host name from scratch and check `domain`
      # strictly against it, but the methods available to us are broken/brittle.
      # So we settle for a whitelist check.
      uri = Addressable::URI.parse(origin)
      return false unless uri.scheme == scheme
      return true if u2f_trusted_facets.include?(origin)
      # Don't perform lab domain checks for enterprise.
      !enterprise? && !!dynamic_lab_domain?(uri.hostname)
    end

    # The RP ID (relying party ID) to use for webauthn:
    # https://www.w3.org/TR/webauthn/#rp-id
    # This is usually the main domain where GitHub is hosted. However,
    # subdomains of `github.com` also have an RP ID of `github.com` so that
    # security keys can be shared across admin/lab domains.
    #
    # Returns a String URL.
    def webauthn_rp_id
      GitHub.enterprise? ? host_name : host_domain
    end

    # The list of domains that are trusted to share U2F devices. In production
    # this includes staging/admin domains. On enterprise and development this
    # is just the app's URL itself.
    #
    # Returns an Array of String URLs.
    def u2f_trusted_facets
      @u2f_trusted_facets ||= [url].freeze
    end
    attr_writer :u2f_trusted_facets

    # Constant BCrypt salt to be able to get the same hash for the same
    # password. Used for tracking passwords from failed login attempts.
    def constant_bcrypt_salt
      @constant_bcrypt_salt ||= BCrypt::Engine.generate_salt(password_cost)
    end
    attr_writer :constant_bcrypt_salt

    ##
    # Failbot / Exceptions reporting.

    # Failbot customization. Under most environments, the default config
    # that's provided out of the box by the Failbot library is sensible but in
    # some cases (FI), it is useful to override it.
    #
    # Default to writing JSON-encoded exceptions to log/exceptions.log under FI
    # environments. Other environments use whatever the default Failbot config
    # of the current RAILS_ENV is.
    #
    # Returns the Failbot custom config Hash to be passed to Failbot.setup.
    def failbot
      @failbot ||=
        if Rails.test?
          {
            "FAILBOT_BACKEND" => "memory",
          }
        elsif Rails.development? || enterprise?
          {
            "FAILBOT_BACKEND" => "file",
            "FAILBOT_BACKEND_FILE_PATH" => failbot_log_path,
          }
        else
          {
            "FAILBOT_BACKEND" => "json",
            "FAILBOT_BACKEND_JSON_HOST" => "localhost",
            "FAILBOT_BACKEND_JSON_PORT" => 6668,
          }
        end
    end
    attr_writer :failbot

    # Failbot log file location, if applicable.
    def failbot_log_path
      "#{Rails.root}/log/exceptions.log"
    end

    ##
    # Miscellaneous feature flags typically disabled under FI environments.

    # Determine whether the blog is enabled. This is disabled under FI
    # environments but enabled everywhere else.
    #
    # Returns true if it is enabled, false otherwise.
    def blog_enabled?
      return @blog_enabled if defined?(@blog_enabled)
      @blog_enabled = !enterprise?
    end
    attr_writer :blog_enabled

    # GitHub blog URL, if applicable.
    def blog_url
      "https://github.blog".freeze
    end

    # Determine whether to show things related to licensing in GitHub. This is
    # primarily the license picker and auto-license population during repository
    # creation.
    #
    # Returns true if the license picker should be displayed, false otherwise.
    def license_picker_enabled?
      return @license_picker_enabled if defined?(@license_picker_enabled)

      @license_picker_enabled = !enterprise?
    end
    attr_writer :license_picker_enabled

    # Whether community profiles are enabled.
    #
    # Returns true if community profiles are enabled, otherwise false.
    def community_profile_enabled?
      @community_profile_enabled ||= !enterprise?
    end
    attr_writer :community_profile_enabled

    # Whether namespaces should be retired upon user account deletion.
    #
    # Returns true if retired namespaces is enabled, otherwise false.
    def retired_namespaces_enabled?
      @retired_namespaces_enabled ||= !enterprise?
    end
    attr_writer :retired_namespaces_enabled

    def domain_verification_enabled?
      @domain_verification_enabled ||= !enterprise?
    end
    attr_writer :domain_verification_enabled

    # Custom tabs allow repository admins to configure custom repository tabs
    # displayed in the repository navigation, that point to an arbitraty URL.
    # This is enabled under Enterprise environments but disabled everywhere else.
    #
    # Returns true if enabled, false otherwise.
    def custom_tabs_enabled?
      return @custom_tabs_enabled if defined?(@custom_tabs_enabled)
      @custom_tabs_enabled = enterprise?
    end
    attr_writer :custom_tabs_enabled

    # Allow users to enter a short professional bio and mark themselves as being
    # hireable. This is disabled by default under FI environments and enabled
    # everywhere else, altough we're not taking advantage of this data yet.
    #
    # Returns true if enabled, false otherwise.
    def job_profiles_enabled?
      return @job_profiles_enabled if defined?(@job_profiles_enabled)
      @job_profiles_enabled = !enterprise?
    end
    attr_writer :job_profiles_enabled

    # Determine whether restrictions around spammy users are enforced
    # throughout the site. Disabled by default under Enterprise environments, enabled
    # everywhere else.
    #
    # Returns true if enabled, false otherwise.
    def spamminess_check_enabled?
      !enterprise?
    end

    # Is there a single global business for this environment rather than
    # multiple businesses?
    #
    # Returns boolean.
    def single_business_environment?
      enterprise?
    end

    # The default base slug for an enterprise account.
    def default_business_base_slug
      if single_business_environment?
        "global-enterprise".freeze
      else
        "enterprise".freeze
      end
    end

    # Setting to restrict contractors from default access to internal
    # repositories that is normally given with membership to the Business
    # in a single Business environment.
    #
    # This setting is used to restrict contractor access to internally visible
    # repositories. Contractors must be granted explicit access to internally
    # visible repositories (no implicit read given).
    #
    # Contractors are identified by the `EnterpriseAttestation.contractor?`
    # check. An API is provided to manage contractor attestations.
    #
    # Defaults to `false`.
    def restrict_contractors_from_default_access_to_internal_repos?
      return false unless single_business_environment?
      return @restrict_contractors_from_default_access_to_internal_repos if defined?(@restrict_contractors_from_default_access_to_internal_repos)
      @restrict_contractors_from_default_access_to_internal_repos = false
    end
    attr_writer :restrict_contractors_from_default_access_to_internal_repos

    # The global Business object if this is a single business environment
    # otherwise nil.
    def global_business
      return unless single_business_environment?

      Business.take
    end

    # Is this a GitHub Private Instance (GHPI) installation?
    #
    # Returns boolean
    def private_instance?
      return false unless GitHub.enterprise?
      return @private_instance if defined?(@private_instance)
      @private_instance = false
    end
    attr_writer :private_instance

    # Is this GitHub Private Instance (GHPI) a Fedramp installation?
    #
    # Returns boolean
    def fedramp_private_instance?
      return false unless private_instance?
      return @fedramp_private_instance if defined?(@fedramp_private_instance)
      @fedramp_private_instance = false
    end
    attr_writer :fedramp_private_instance

    # Is IdP configuration required during the bootstrap of this GitHub Private
    # Instance installation?
    #
    # Returns Boolean.
    def private_instance_idp_configuration_required?
      return false unless private_instance?
      return @private_instance_idp_configuration_required if defined?(@private_instance_idp_configuration_required)
      @private_instance_idp_configuration_required = false
    end
    attr_writer :private_instance_idp_configuration_required

    # The URL where a GHPI administrator can open a support request.
    #
    # Returns String.
    def private_instance_support_request_url
      "https://enterprise.github.com/tickets/ghpi_ticket?ghe_version=#{GitHub.version_number}&github_product=ghpi"
    end

    # The GitHub Private Instance bootstrapper for this installation.
    #
    # Returns GitHub::PrivateInstanceBootstrapper.
    def private_instance_bootstrapper
      @private_instance_bootstrapper ||= GitHub::PrivateInstanceBootstrapper.new
    end

    # This is the the interface to updating the GHES configuration system from
    # the Rails app.
    #
    # Returns GitHub::EnterpriseConfigurationUpdater.
    def enterprise_configuration_updater
      @enterprise_configuration_updater ||= GitHub::EnterpriseConfigurationUpdater.new
    end

    # GHES Management Console (enterprise-manage) API client.
    #
    # Returns GitHub::EnterpriseManageClient.
    def enterprise_manage_client
      @enterprise_manage_client ||= GitHub::EnterpriseManageClient.create
    end

    # The hash of the GHES Management Console password.
    #
    # This is populated via the ENTERPRISE_MANAGEMENT_CONSOLE_PASSWORD_HASH
    # environment variable which is injected from /data/user/common/secrets.conf
    # on a GHES appliance.
    #
    # Returns String.
    attr_accessor :management_console_password_hash

    # Feature flag indicating whether SAML SSO and SCIM behaviors are enabled.
    #
    # To enable, ensure `GitHub.private_instance?` is enabled.
    #
    # Returns boolean
    def private_instance_user_provisioning?
      private_instance?
    end

    # How long do we memoize spam patterns during Spam checking?
    #
    # We default to 0 if no value is set in the environment.
    #
    # Returns an integer.
    def spam_pattern_memoization_ttl_in_seconds
      return @spam_pattern_memoization_ttl_in_seconds if defined?(@spam_pattern_memoization_ttl_in_seconds)
      @spam_pattern_memoization_ttl_in_seconds = (ENV["SPAM_PATTERNS_MEMOIZED_TTL_IN_SECONDS"] || 0).to_i
    end
    attr_writer :spam_pattern_memoization_ttl_in_seconds

    # Should we report user-creation data to Octolytics?
    #
    # Returns false in enterprise.
    def user_creation_analytics_enabled?
      octolytics_enabled? && !enterprise?
    end

    # Determine whether suspended users are hidden from other users. Enabled
    # by default under all environments.
    #
    # Returns true if enabled, false otherwise.
    def suspended_users_visible?
      return @suspended_users_visible if defined?(@suspended_users_visible)
      @suspended_users_visible = true
    end
    attr_writer :suspended_users_visible

    # Determine whether to show the suspended/spammy alerts at the top of the
    # profile page. Enabled by default for enterprise only
    #
    # Returns true if enabled, false otherwise
    def show_user_profile_alerts?
      return @show_user_profile_alerts if defined?(@show_user_profile_alerts)
      @show_user_profile_alerts = enterprise?
    end
    attr_writer :show_user_profile_alerts

    # Determine whether /admin is enabled. Redirects to Staff Tools when
    # disabled. Disable by default under Enterprise environments, enabled everywhere
    # else.
    #
    # Returns true if enabled, false otherwise.
    def admin_enabled?
      return @admin_enabled if defined?(@admin_enabled)
      @admin_enabled = !enterprise?
    end
    attr_writer :admin_enabled

    # Determine whether /setup is enabled. Enabled by default in Enterprise,
    # disabled for dotcom.
    #
    # Returns true if enabled, false otherwise.
    def management_console_enabled?
      return @management_console_enabled if defined?(@management_console_enabled)
      @management_console_enabled = enterprise?
    end
    attr_writer :management_console_enabled

    # Determine whether Haystack is available. Currently dotcom only.
    #
    # Returns true if haystack is enabled, false otherwise.
    def haystack_enabled?
      return @haystack_enabled if defined?(@haystack_enabled)
      @haystack_enabled = !enterprise?
    end
    attr_writer :haystack_enabled

    # Determine whether the instance wide audit log is enabled. Currently dotcom
    # only.
    #
    # Returns true if enabled, false otherwise.
    def instance_audit_log_enabled?
      return @instance_audit_log_enabled if defined?(@instance_audit_log_enabled)
      @instance_audit_log_enabled = enterprise?
    end
    attr_writer :instance_audit_log_enabled

    # Determine whether reports are enabled. Current only used
    # in Enterprise.
    #
    # Returns true if enabled, false otherwise.
    def reports_enabled?
      return @reports_enabled if defined?(@reports_enabled)
      @reports_enabled = GitHub.enterprise?
    end
    attr_writer :reports_enabled

    # Determine whether large blobs should be rejected or not.
    # We currently don't reject large blobs in Enterprise.
    #
    # Returns true if enabled, false otherwise.
    def large_blob_rejection_enabled?
      return @large_blob_rejection_enabled if defined?(@large_blob_rejection_enabled)
      @large_blob_rejection_enabled = true
    end
    attr_writer :large_blob_rejection_enabled

    # Is the rejection of 40 character hex names for refs enabled?
    def reject_sha_like_refs?
      return @reject_sha_like_refs if defined?(@reject_sha_like_refs)
      @reject_sha_like_refs = true
    end
    attr_writer :reject_sha_like_refs

    # The maximum length of reference names.
    #
    # This is defined as the minimum size for the `pushes.refs` database
    # column.  Refs longer than this will be truncated and cause query
    # warnings.
    def maximum_ref_length
      @maximum_ref_length ||= 255
    end
    attr_writer :maximum_ref_length

    # The maximal number of Git LFS objects that are checked in *one* push.
    # Pushes exceeding that number will only be spot checked.
    def lfs_integrity_max_oids
      @lfs_integrity_max_oids ||= 10000
    end
    attr_writer :lfs_integrity_max_oids

    # Determine whether organization OAuth application policies are available.
    # (This feature is currently available only in dotcom environments.)
    #
    # Returns true if enabled, false otherwise.
    def oauth_application_policies_enabled?
      !enterprise?
    end

    # Determines if orgs can automatically receive a set of
    # beta features (such as Actions/GPR/etc...)
    #
    # Returns true if enabled, false otherwise.
    def organization_beta_enrollment_enabled?
      !enterprise?
    end

    # Determines if the current environment configuration supports
    # token scanning.
    #
    # Returns true if enabled, false otherwise.
    def configuration_supports_token_scanning?
      # This feature is enabled by default for dotcom but setup for enterprise
      return false unless configuration_secret_scanning_enabled?
      # Token scannning occurs when any ref change occurs, which can trigger
      # lots of unnecessary scans in CI.
      return false if Rails.test?
      true
    end

    # Determine if site admins can initiate secret scanning
    # Enabled by default in dotcom, disabled for enterprise, except if explicitly enabled via ghe config apply.
    #
    # Returns true if enabled, false otherwise.
    def configuration_secret_scanning_enabled?
      return @secret_scanning_enabled if defined?(@secret_scanning_enabled)
      @secret_scanning_enabled = !enterprise?
    end
    attr_writer :secret_scanning_enabled

    # Determine the no of scans that can be run per repo at the same time.
    # Enabled by default (with a value of 1), can be changed via ghe config apply.
    #
    # Returns true if enabled, false otherwise.
    def configuration_secret_scanning_max_scans_per_repo
      return @configuration_secret_scanning_max_scans_per_repo if defined?(@configuration_secret_scanning_max_scans_per_repo)
      @configuration_secret_scanning_max_scans_per_repo = 1
    end
    attr_writer :configuration_secret_scanning_max_scans_per_repo

    # Determine the no of backfill scans that can be run at the same time.
    # Enabled by default (with a value of 20), can be changed via ghe config apply.
    #
    # Returns true if enabled, false otherwise.
    def configuration_secret_scanning_max_backfill_scans
      return @configuration_secret_scanning_max_backfill_scans if defined?(@configuration_secret_scanning_max_backfill_scans)
      @configuration_secret_scanning_max_backfill_scans = 20
    end
    attr_writer :configuration_secret_scanning_max_backfill_scans

    # Determine the no of candidate matches that can be returned for a scan.
    # Enabled by default (with a value of 16000), can be changed via ghe config apply.
    #
    # Returns true if enabled, false otherwise.
    def configuration_secret_scanning_max_candidate_matches
      return @configuration_secret_scanning_max_candidate_matches if defined?(@configuration_secret_scanning_max_candidate_matches)
      if enterprise?
        @configuration_secret_scanning_max_candidate_matches = 64000
      else
        @configuration_secret_scanning_max_candidate_matches = 16000
      end
    end
    attr_writer :configuration_secret_scanning_max_candidate_matches

    # Should tokens not be sent to third party URLs?
    #
    # Returns true if enabled, false otherwise.
    def skip_secret_scanning_third_party_reporting?
      return @skip_secret_scanning_third_party_reporting if defined?(@skip_secret_scanning_third_party_reporting)
      @skip_secret_scanning_third_party_reporting = enterprise? || dynamic_lab?
    end
    attr_writer :skip_secret_scanning_third_party_reporting

    # Determine if code scanning is enabled.
    # Enabled by default in dotcom, disabled for enterprise, except if explicitly enabled via ghe config apply.
    #
    # Returns true if enabled, false otherwise.
    def code_scanning_enabled?
      return @code_scanning_enabled if defined?(@code_scanning_enabled)
      @code_scanning_enabled = !enterprise?
    end
    attr_writer :code_scanning_enabled

    # Determine if site admins can enable advanced security (this is temporary till the GHAS licensing SKU experience is figured out)
    # Enabled by default in dotcom, disabled for enterprise, except if explicitly enabled via ghe config apply.
    #
    # Returns true if enabled, false otherwise.
    def configuration_advanced_security_enabled?
      return @advanced_security_enabled if defined?(@advanced_security_enabled)
      @advanced_security_enabled = !enterprise?
    end
    attr_writer :advanced_security_enabled

    # Determine if site admins can initiate a site-wide ssh key audit.
    # Enabled by default in Enterprise, disabled for dotcom.
    #
    # Returns true if enabled, false otherwise.
    def ssh_audit_enabled?
      return @ssh_audit_enabled if defined?(@ssh_audit_enabled)
      @ssh_audit_enabled = enterprise?
    end
    attr_writer :ssh_audit_enabled

    # Determine whether repo transfers require approval.
    #
    # Returns true if enabled, false otherwise.
    def repository_transfer_requests_enabled?
      return @repository_transfer_requests_enabled if defined?(@repository_transfer_requests_enabled)
      @repository_transfer_requests_enabled = !enterprise?
    end
    attr_writer :repository_transfer_requests_enabled

    # Determine whether employee-only features are available. Disabled by
    # default under Enterprise environments, enabled everywhere else.
    #
    # Returns true if enabled, false otherwise.
    def preview_features_enabled?
      return @preview_features_enabled if defined?(@preview_features_enabled)
      @preview_features_enabled = !enterprise?
    end
    attr_writer :preview_features_enabled

    # Whether public-push repositories are enabled.
    # See: https://github.com/github/github/pull/15785
    def public_push_enabled?
      GitHub.enterprise?
    end

    # Determine whether anonymous git access can be enabled on repositories.
    def anonymous_git_access_available?
      GitHub.private_mode_enabled?
    end

    # Whether browser stats collecting is enabled
    def browser_stats_enabled?
      return @browser_stats_enabled if defined?(@browser_stats_enabled)
      @browser_stats_enabled = true
    end
    attr_writer :browser_stats_enabled

    # The API URL to post browser stats to.
    def browser_stats_url
      [api_url, "_private", "browser", "stats"].join("/")
    end

    # The API URL that JavaScript exceptions are reported to.
    def browser_errors_url
      [api_url, "_private", "browser", "errors"].join("/")
    end

    # Is SMTP enabled in this environment?
    #
    # Returns true by default.
    # Can return false in Enterprise environments if an admin has disabled SMTP
    # on the appliance.
    def smtp_enabled?
      return @smtp_enabled if defined?(@smtp_enabled)
      @smtp_enabled = true
    end
    attr_writer :smtp_enabled

    # The SMTP domain name.
    def smtp_domain
      @smtp_domain ||= host_name
    end
    attr_writer :smtp_domain

    # SMTP server address.
    attr_accessor :smtp_address

    # SMTP port.
    def smtp_port
      @smtp_port ||= 25
    end
    attr_writer :smtp_port

    # The secret token used in the generation of the recipient code for
    # reply emails.
    #
    # Returns 'hubbernaut' by default.
    def smtp_secret
      return @smtp_secret if defined?(@smtp_secret)
      @smtp_secret ||= "hubbernaut"
    end
    attr_writer :smtp_secret

    # Additional SMTP configuration.
    attr_accessor :smtp_user_name
    attr_accessor :smtp_password
    attr_accessor :smtp_authentication

    # Default .com to having it disabled because the
    # front-end & worker machines talk to an MTA on localhost
    def smtp_enable_starttls_auto
      enterprise? ? @smtp_enable_starttls_auto : false
    end
    attr_writer :smtp_enable_starttls_auto

    # The name of the session cookie used for rails sessions.
    #
    # Returns '_gh_sess' by default under dotcom mode and '_gh_ent' under
    # enterprise mode.
    def session_key
      @session_key ||=
        if GitHub.runtime.enterprise?
          "_gh_ent"
        else
          "_gh_sess"
        end
    end
    attr_writer :session_key

    # The secret token for both rails and rack session signatures. This value is
    # also used by enterprise-manage's sessions.
    attr_accessor :session_secret

    # The timeout for user sessions, in seconds.
    #
    # Defaults to 2 weeks.
    def user_session_timeout
      @user_session_timeout ||= 2.weeks
    end
    attr_writer :user_session_timeout

    # Window of time between user session access log writes
    #
    # Defaults to 1 day.
    def user_session_access_throttling
      @user_session_access_throttling ||= 1.day
    end
    attr_writer :user_session_access_throttling

    # The secret token used for signing longpoll socket IDs.
    attr_accessor :longpoll_socket_id_secret

    ##
    # Network Graph

    # Maximum number of commits (dots) to show on the Network Graph.
    def network_graph_history_limit
      @network_graph_history_limit ||= enterprise?? 50_000 : 5_000
    end
    attr_writer :network_graph_history_limit

    # Maximum number of forks in a network to show. These are the
    # most popular and active forks that are selected
    def network_graph_fork_limit
      @network_graph_fork_limit ||= 100
    end
    attr_writer :network_graph_fork_limit

    # Maximum number of branches in a single repository to show
    # in the network graph. These are the most recently active
    # branches
    def network_graph_branch_limit
      @network_graph_branch_limit ||= 2_000
    end
    attr_writer :network_graph_branch_limit

    # The limit of how many times an email check request can happen per
    # ip address.
    #
    # Returns a fixnum limit that can be combined with a ttl.
    def email_check_rate_limit
      @email_check_rate_limit ||= enterprise? ? 120 : 5000
    end
    attr_writer :email_check_rate_limit

    # The lifespan of an email check rate limit.
    #
    # Returns a fixnum representing time in seconds.
    def email_check_ttl
      @email_check_ttl ||= enterprise? ? 1.minute : 1.hour
    end
    attr_writer :email_check_ttl

    # The Enterprise configuration id. An integer timestamp representing the
    # last time the configuration chef run was started. This is exposed at
    # /status.json and is useful for determining if the app is running under
    # the expected or newer configuration version.
    def configuration_id
      @configuration_id.to_i rescue nil
    end
    attr_writer :configuration_id

    # The port the git daemon in listening on locally. In production the default
    # git port (9418) is used. On enterprise git_proxy needs to consume 9418 so
    # the daemon must listen on something else.
    #
    # Returns 9418 in production.
    def git_daemon_port
      @git_daemon_port ||= 9418
    end
    attr_writer :git_daemon_port

    # The fingerprints of the host key. Constant on production, unique per
    # server on enterprise.
    #
    # Return the fingerprint of the RSA host key.
    def ssh_host_key_fingerprints
      {
        "MD5_RSA" => "16:27:ac:a5:76:28:2d:36:63:1b:56:4d:eb:df:a6:48",
        "MD5_DSA" => "ad:1c:08:a4:40:e3:6f:9c:f5:66:26:5d:4b:33:5d:8c",
        "SHA256_RSA" => "nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8",
        "SHA256_DSA" => "br9IjFspm1vxR3iA35FWE+4VTyz1hYVLIE2t1/CeyWQ",
      }
    end

    # New accounts are created through the signup page on enterprise instances.
    # Not used by some external auth providers, or when in private mode, or when
    # explicitly disabled during setup.
    #
    # Returns boolean
    def signup_enabled?
      return true unless enterprise?

      !GitHub.private_mode_enabled? && GitHub.auth.signup_enabled? && GitHub.signup_enabled
    end

    # In Enterprise, admins can explicitly disable signups.
    def signup_enabled
      @signup_enabled
    end
    attr_writer :signup_enabled

    # In Enterprise, realtime backups are manually enabled
    def realtime_backups_enabled?
      !!@realtime_backups_enabled
    end
    attr_writer :realtime_backups_enabled

    # In Enterprise we hide the ability to block users and report
    # abuse.
    def user_abuse_mitigation_enabled?
      return @user_abuse_mitigation_enabled if defined?(@user_abuse_mitigation_enabled)
      @user_abuse_mitigation_enabled = !enterprise?
    end
    attr_writer :user_abuse_mitigation_enabled

    # Timeout for long running Git operations in templates (e.g.,
    # rendering diffs and commit lists in pull requests).
    def git_template_timeout
      @git_template_timeout ||= 10
    end
    attr_writer :git_template_timeout

    # Enterprise customers may want to customize the email address used as the
    # noreply sender.
    #
    # Returns an email address as a string. Default is noreply@<host_name>.
    def noreply_address
      @noreply_address ||= "noreply@#{smtp_domain}"
    end
    attr_writer :noreply_address

    # Stealth email is disabled on Enterprise instances.
    def stealth_email_enabled?
      !enterprise?
    end

    # Allow turning on/off email replies to notifications.
    #
    # Defaults to true.
    def email_replies_enabled?
      return @email_replies_enabled if defined?(@email_replies_enabled)
      @email_replies_enabled = true
    end
    attr_writer :email_replies_enabled

    # Email verification is disabled on Enterprise instances
    def email_verification_enabled?
      return false if enterprise?
      true
    end

    # Choosing a commit email is disabled on Enterprise because
    # email verification and commit signing are also disabled on Enterprise.
    def choose_commit_email_enabled?
      !enterprise?
    end

    # Mandatory email verification is turned off in dev and test, and anywhere
    # email verification is disabled.
    #
    # Force with the ENABLE_MANDATORY_VERIFICATION environment variable, e.g.:
    #
    #   ENABLE_MANDATORY_VERIFICATION=1 script/server
    def mandatory_email_verification_enabled?
      return true if !!ENV["ENABLE_MANDATORY_VERIFICATION"]
      email_verification_enabled? && !(Rails.test? || Rails.development?)
    end

    # Email preference center is disabled on Enterprise instances
    # because we don't need to send marketing newsletters to those users.
    def email_preference_center_enabled?
      !enterprise?
    end

    # Determine whether or not to check user email addresses for
    # genericness before counting contributions.
    def email_detect_generic_domains?
      @email_detect_generic_domains ||= !enterprise?
    end
    attr_writer :email_detect_generic_domains

    # The MailChimp integration is is disabled on Enterprise.
    def mailchimp_enabled?
      !enterprise?
    end

    # As is the sendgrid integration
    def sendgrid_enabled?
      !enterprise?
    end

    # We require a password confirmation on signup in Enterprise
    # because email delivery might not be enabled for the instance,
    # which means that a user couldn't reset their password via email.
    def password_confirmation_required?
      enterprise?
    end

    # We ask users identity questions on dotcom during signup.
    # Disabled on Enterprise.
    def user_identification_enabled?
      !enterprise?
    end

    # Live Update XHR Socket poller
    #
    # Can be flipped off if the web socket connections are going nuts.
    def live_updates_enabled?
      return @live_updates_enabled if defined?(@live_updates_enabled)
      @live_updates_enabled = true
    end
    attr_writer :live_updates_enabled

    # Check to see if we are restriced by licenses, in other words if we are an
    # enterprise install.  Determines what to show in the root of stafftools
    def licensed_mode?
      enterprise?
    end

    # The path to the local .ghl file on Enterprise instances.
    def license_path
      return unless enterprise?

      @license_path ||= File.join(enterprise_config_dir, "enterprise.ghl")
    end
    attr_writer :license_path unless Rails.production?

    # Path to the license .gpg key shipped as part of an Enterprise .ova.
    def license_key
      @license_key ||= File.join(enterprise_config_dir, "license.gpg")
    end
    attr_writer :license_key unless Rails.production?

    # Path to the customer .gpg key uploaded as part of the Enterprise license.
    def customer_key
      @customer_key ||= File.join(enterprise_config_dir, "customer.gpg")
    end
    attr_writer :customer_key unless Rails.production?

    # The configuration directory holding Enterprise related files like the
    # .ghl license, customer gpg key, and license gpg key.
    #
    # Returns the path to the config dir for the current Enterprise series.
    def enterprise_config_dir
      "/data/enterprise"
    end

    # Placeholder to abstract the fact that GitHub access control is done
    # through GitHub::AccessControl.
    def access
      Egress::AccessControl
    end

    # Configuration for enforcing active session limits
    def active_session_limit_enabled?
      !enterprise?
    end

    # enterprise-web URL
    attr_accessor :enterprise_web_url

    # enterprise-web admin URL
    attr_accessor :enterprise_web_admin_url

    # enterprise-web HMAC
    attr_accessor :enterprise_web_hmac

    def enterprise_support_url
      "#{enterprise_web_url}/support"
    end

    # Sets the API token for accessing the enterprise-web API
    attr_accessor :enterprise_web_token

    # S3 Access key for alambic
    attr_accessor :s3_alambic_access_key

    # S3 Secret key for alambic
    attr_accessor :s3_alambic_secret_key

    # Sets the URL for accessing Alambic.
    # TODO(storage): deprecated by alambic cluster
    attr_accessor :alambic_url

    # Sets the URL for purging CDN keys.
    attr_accessor :alambic_cdn_url

    # Sets the API token for purging CDN data through Alambic.
    attr_accessor :alambic_cdn_token

    # Sets the URL prefix for uploading assets to the "assets" Alambic service.
    # TODO(storage): deprecated by alambic cluster
    attr_accessor :alambic_uploads_url

    # Sets the URL for accessing the LFS server.
    attr_accessor :lfs_server_url

    # Sets the HMAC key used to verify request bodies sent to the Internal API.
    attr_accessor :internal_api_hmac_key

    # Sets the HMAC keys used to verify GitAuth tokens. The value that should be
    # used for new token should be first.
    def gitauth_token_hmac_keys
      @gitauth_token_hmac_keys ||=
        ENV["GITAUTH_TOKEN_HMAC_KEYS"].to_s.split
    end
    attr_writer :gitauth_token_hmac_keys

    # Sets the HMAC key used to generate an HMAC token to authenticate with the Dependency Graph APIs.
    def dependency_graph_api_hmac_key
      @dependency_graph_api_hmac_key ||=
        ENV["DEPENDENCY_GRAPH_API_HMAC_KEY"].to_s
    end
    attr_writer :dependency_graph_api_hmac_key

    # Gets the Earthsmoke key used to encrypt messages sent to Hydro for processing by Dependency Graph APIs.
    def dependency_graph_api_hydro_key
      earthsmoke.key("dependency-graph-api-hydro-key")
    end
    attr_writer :dependency_graph_api_hydro_key

    def api_internal_pages_hmac_keys
      @api_internal_pages_hmac_keys ||=
        ENV["API_INTERNAL_PAGES_ROUTER_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_pages_hmac_keys

    def api_internal_storage_uploadable_hmac_keys
      @api_internal_storage_uploadable_hmac_keys ||=
        ENV["API_INTERNAL_STORAGE_UPLOADABLE_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_storage_uploadable_hmac_keys

    def api_internal_raw_hmac_keys
      @api_internal_raw_hmac_keys ||=
        ENV["API_INTERNAL_RAW_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_raw_hmac_keys

    def api_internal_multi_part_policies_hmac_keys
      @api_internal_multi_part_policies_hmac_keys ||=
        ENV["API_INTERNAL_MULTI_PART_POLICIES_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_multi_part_policies_hmac_keys

    def api_internal_lfs_hmac_keys
      @api_internal_lfs_hmac_keys ||=
        ENV["API_INTERNAL_LFS_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_lfs_hmac_keys

    def api_internal_media_app_hmac_keys
      @api_internal_media_app_hmac_keys ||=
        ENV["API_INTERNAL_MEDIA_APP_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_media_app_hmac_keys

    def api_internal_marketplace_analytics_hmac_keys
      @api_internal_marketplace_analytics_hmac_keys ||=
        ENV["API_INTERNAL_MARKETPLACE_ANALYTICS_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_marketplace_analytics_hmac_keys

    def api_internal_porter_callbacks_hmac_keys
      @api_internal_porter_callbacks_hmac_keys ||=
        ENV["API_INTERNAL_PORTER_CALLBACKS_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_porter_callbacks_hmac_keys

    def api_internal_email_bounce_hmac_keys
      @api_internal_email_bounce_hmac_keys ||=
        ENV["API_INTERNAL_EMAIL_BOUNCE_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_email_bounce_hmac_keys

    def api_internal_assets_uploadable_hmac_keys
      @api_internal_assets_uploadable_hmac_keys ||=
        ENV["API_INTERNAL_ASSETS_UPLOADABLE_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_assets_uploadable_hmac_keys

    def api_internal_avatars_hmac_keys
      @api_internal_avatars_hmac_keys ||=
        ENV["API_INTERNAL_AVATARS_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_avatars_hmac_keys

    def api_internal_assets_hmac_keys
      @api_internal_assets_hmac_keys ||=
        ENV["API_INTERNAL_ASSETS_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_assets_hmac_keys

    def api_internal_replicas_hmac_keys
      @api_internal_replicas_hmac_keys ||=
        ENV["API_INTERNAL_SPOKESD_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_replicas_hmac_keys

    def api_internal_repositories_hmac_keys
      @api_internal_repositories_hmac_keys ||=
        [
          ENV["API_INTERNAL_REPOSITORIES_HMAC_KEYS"],
          ENV["API_INTERNAL_BABELD_HMAC_KEYS"],
          ENV["API_INTERNAL_SPOKESD_HMAC_KEYS"],
        ].join(" ").split
    end
    attr_writer :api_internal_repositories_hmac_keys

    def api_internal_gists_hmac_keys
      @api_internal_gists_hmac_keys ||=
        [
          ENV["API_INTERNAL_GISTS_HMAC_KEYS"],
          ENV["API_INTERNAL_BABELD_HMAC_KEYS"],
          ENV["API_INTERNAL_SPOKESD_HMAC_KEYS"],
        ].join(" ").split
    end
    attr_writer :api_internal_gists_hmac_keys

    def api_internal_pre_receive_hooks_hmac_keys
      @api_internal_pre_receive_hooks_hmac_keys ||=
        ENV["API_INTERNAL_PRE_RECEIVE_HOOKS_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_pre_receive_hooks_hmac_keys

    def api_internal_aleph_hmac_keys
      @api_internal_aleph_hmac_keys ||=
        ENV["API_INTERNAL_ALEPH_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_aleph_hmac_keys

    def api_internal_search_git_trees_hmac_keys
      @api_internal_search_git_trees_hmac_keys ||=
        ENV["API_INTERNAL_SEARCH_GIT_TREES_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_search_git_trees_hmac_keys

    def api_internal_search_repair_batch_hmac_keys
      @api_internal_search_repair_batch_hmac_keys ||=
        ENV["API_INTERNAL_SEARCH_REPAIR_BATCH_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_search_repair_batch_hmac_keys

    def api_internal_actions_hmac_keys
      @api_internal_actions_hmac_keys ||=
          ENV["API_INTERNAL_ACTIONS_HMAC_KEYS"].to_s.split
    end
    attr_writer :api_internal_actions_hmac_keys

    # Sets the secrets for the spamurai timestamps.
    def spamurai_timestamp_secrets
      return @spamurai_timestamp_secrets if defined?(@spamurai_timestamp_secrets)
      @spamurai_timestamp_secrets = ENV["SPAMURAI_TIMESTAMP_SECRETS"].to_s.split
    end
    attr_writer :spamurai_timestamp_secrets

    # Sets the ?v param on avatar urls.  Changing this will force break all
    # avatar server and browser caches.
    attr_accessor :alambic_avatar_version

    # Sets the ?v param on avatar urls for a portion of URLS
    attr_accessor :alambic_next_avatar_version

    # Sets the ?b param on avatar urls.  Changing this will force break all
    # browser caches.
    attr_accessor :alambic_browser_avatar_version

    # Sets the ?b param on avatar urls for a portion of URLS
    attr_accessor :alambic_next_browser_avatar_version

    # Sets the % chance that the next avatar version is used
    attr_accessor :alambic_next_avatar_chance

    # Sets the string user login
    attr_accessor :alambic_fallback_user

    # Sets the URL path to purge (using GitHub.alambic_cdn_url as the host)
    attr_writer :alambic_cdn_purge_path

    # Sets the number of seconds to delay an avatar purge.
    attr_accessor :alambic_cdn_delay

    # Sets the default path prefix for alambic content.  See the defaults set
    # in the different runtime environments (test, dev, prod, enterprise).
    # TODO(storage): deprecated by alambic cluster
    attr_accessor :alambic_path_prefix

    # Sets whether to force the Media::Blob prefix or use the default one.
    # Defaults to false in production, and true everywhere else.
    # TODO(storage): deprecated by alambic cluster
    attr_accessor :alambic_use_media_prefix

    # Sets the API token for replicating assest through Alambic
    attr_accessor :alambic_replication_token

    # Sets a shared token with github/render for creating RenderBlobs in GHE.
    attr_accessor :render_blob_storage_token

    # Sets a shared token with github/lfs-server for internal api actions
    attr_accessor :lfs_server_token

    attr_accessor :git_lfs_enabled

    def alambic_cdn_purge_path
      @alambic_cdn_purge_path ||= "purge"
    end

    # Gets the CSP host to grant access to Alambic services.
    def alambic_csp_host
      url_origin(alambic_uploads_url)
    end

    # Sets the URL prefix for downloading assets from the "assets" Alambic
    # service.
    #
    # TODO(storage): deprecated by alambic cluster
    #
    # ex: https://alambic-origin.github.com/assets
    attr_accessor :alambic_assets_url

    # Sets the URL prefix for uploading and downloading objects through the
    # "storage" Alambic service.
    attr_accessor :storage_cluster_url

    # Get the storage cluster host for CSP purposes.
    def storage_cluster_host
      url_origin(storage_cluster_url)
    end

    # Sets the URL prefix for downloading objects through the "storage"
    # Alambic service. If nil, fall back to #storage_cluster_url. GHE with
    # Private mode should set this to a route that verifies private mode through
    # cookies, instead of the API.
    attr_accessor :storage_private_mode_url

    # Sets the URL format for the root url for Alambic to replicate objects.
    # Should yield a full URL.
    #
    # ex: "http://%s:8080/storage/replicate"
    #
    #   GitHub.storage_replicate_fmt % "ghe-alambic-fe1"
    #   # => http://ghe-alambic-fe1:8080/storage/replicate
    attr_accessor :storage_replicate_fmt

    # Sets the path for the EnterpriseStorageClusterUpgrade transition.
    attr_accessor :storage_transition_path

    # Gets the number of readonly replicas that are included in a storage upload,
    # but not required for a successful cluster consensus.
    attr_accessor :storage_non_voting_replica_count

    attr_writer :storage_cluster_enabled
    attr_writer :storage_replica_count
    attr_writer :storage_auto_localhost_replica
    attr_writer :storage_legacy_path

    def storage_cluster_enabled?
      @storage_cluster_enabled
    end

    def storage_replica_count
      @storage_replica_count ||= 1
    end

    def storage_auto_localhost_replica?
      if @storage_auto_localhost_replica.nil?
        @storage_auto_localhost_replica = !Rails.production?
      end
      @storage_auto_localhost_replica
    end

    def storage_legacy_path
      @storage_legacy_path ||= File.join(Rails.root, "tmp/objects")
    end

    # Sets the URL prefix for the Avatar proxy
    #
    # ex: https://avatars.github.com
    attr_writer :alambic_avatar_url

    def alambic_assets_host
      url_origin(alambic_assets_url)
    end

    # Gets just the scheme + host + port of a URL for the CSP policy.
    def url_origin(url)
      origin = Addressable::URI.parse(url).origin if url
      origin == "null" ? nil : origin
    end

    # The base URL for all Alambic avatars. If passed a string, it will
    # determine which subdomain to use.
    #
    # string - An optional String that will be hashed to determine which avatar
    #          subdomain should be used (0, 1, 2, or 3).
    #
    # Returns a full avatar url
    def alambic_avatar_url(string = nil)
      return @alambic_avatar_url % nil unless string
      string_s = string.to_s
      n = Zlib.crc32(string_s) % 4
      @alambic_avatar_url % n
    end

    # Get all possible Alambic avatar URLs.
    #
    # Returns a Array of available Alambic avatar URLs. Returns an empty Array
    # if Alambic avatars are disabled.
    def alambic_avatar_urls
      (0..3).map { |n| url_origin(@alambic_avatar_url % n) }.uniq
    end

    def avatar_version(key = nil)
      nxt = alambic_next_avatar_version
      return alambic_avatar_version if !(nxt && next_avatar_version?(key))
      nxt
    end

    def browser_avatar_version(key = nil)
      nxt = alambic_next_browser_avatar_version
      return alambic_browser_avatar_version if !(nxt && next_avatar_version?(key))
      nxt
    end

    def next_avatar_version?(key = nil)
      chance = alambic_next_avatar_chance.to_i
      return false unless chance > 0

      if key
        Zlib.crc32(key) % 100 < chance
      else
        rand(100) < chance
      end
    end

    # Prefetch DNS entries for our asset domains.
    #
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-DNS-Prefetch-Control
    #
    #     <link rel="dns-prefetch" href="//github.githubassets.com">
    #
    # Returns an Array of String host names to prefetch.
    def dns_prefetch_hosts
      @dns_prefetch_hosts ||= [
        asset_host_url,
        *alambic_avatar_urls,
        s3_asset_bucket_host,
        user_images_cdn_url,
      ].select(&:present?).freeze
    end

    # Public - disable NetGraph generation
    # The network graph generation makes the Enterprise VM unusable.
    # Using this flag we can disable the building for each push.
    #
    # Return true if the graph can be built.
    def network_graph_building_enabled?
      !enterprise?
    end

    # Whether any uploads are accepted.
    #
    # Returns true if enabled, false otherwise.
    def uploads_enabled?
      storage_cluster_enabled? || s3_uploads_enabled?
    end

    # Upload assets to S3, so that they may be served by Amazon and not us.
    # Disabled by default under FI environments, enabled everwhere else.
    #
    # Returns true if enabled, false otherwise.
    def s3_uploads_enabled?
      return @s3_uploads_enabled unless @s3_uploads_enabled.nil?
      @s3_uploads_enabled = s3_uploads_enabled!
    end
    attr_writer :s3_uploads_enabled

    def s3_uploads_enabled!
      !enterprise? && online?
    end

    def asset_url_host
      @asset_url_host ||= s3_asset_host
    end
    attr_writer :asset_url_host

    def s3_asset_host
      @s3_asset_host ||= begin
        if s3_environment_config[:asset_host_name]
          "https://#{s3_environment_config[:asset_host_name]}/"
        else
          "#{s3_asset_bucket_host}/"
        end
      end
    end
    attr_writer :s3_asset_host

    def s3_asset_bucket_host
      @s3_asset_bucket_host ||= "https://#{s3_environment_config[:asset_bucket_name]}.s3.amazonaws.com"
    end

    def file_asset_host
      @file_asset_host ||= "/"
    end
    attr_writer :file_asset_host

    # Sets the local file system path to store uploaded files.
    #
    # Returns a String path.
    def file_asset_path
      @file_asset_path ||= if Rails.test?
        (Rails.root + "test/fixtures/assets#{test_environment_number}").to_s
      elsif enterprise? && Rails.production?
        "/data/assets"
      else
        (Rails.root + "public").to_s
      end
    end
    attr_writer :file_asset_path

    # Sets the local file system path to store storage files.
    # This is only used in enterprise.
    #
    # Returns a String path
    def storage_data_path
      @storage_data_path ||= if enterprise?
        "/data/user/storage"
      else
        file_asset_path
      end
    end
    attr_writer :storage_data_path

    # Uri base path to the external assets.
    #
    # Returns a String path
    def asset_base_path
      return @asset_base_path if defined?(@asset_base_path)
      @asset_base_path = "assets"
    end
    attr_writer :asset_base_path

    # Uri base path to the external task logs.
    #
    # Returns a String path
    def task_log_base_path
      return @task_log_base_path if defined?(@task_log_base_path)
      @task_log_base_path = "task-logs"
    end
    attr_writer :task_log_base_path

    # Uri base path to the external releases.
    #
    # Returns a String path
    def release_asset_base_path
      return @release_asset_base_path if defined?(@release_asset_base_path)
      @release_asset_base_path = "releases"
    end
    attr_writer :release_asset_base_path

    # Uri base path to marketplace listing assets.
    #
    # Returns a String path
    def marketplace_listing_asset_base_path
      if defined? @marketplace_listing_asset_base_path
        return @marketplace_listing_asset_base_path
      end
      @marketplace_listing_asset_base_path = "marketplace_listings"
    end
    attr_writer :marketplace_listing_asset_base_path

    # Uri base path to the external showcase assets
    #
    # Returns a String path
    def showcase_asset_base_path
      return @showcase_asset_base_path if defined?(@showcase_asset_base_path)
      @showcase_asset_base_path = "showcases"
    end
    attr_writer :showcase_asset_base_path

    # Configuration attributes to use github.com as SSO for GitHub Enterprise.
    attr_accessor :github_oauth_client_id
    attr_accessor :github_oauth_secret_key
    attr_accessor :github_oauth_organization
    attr_accessor :github_oauth_team

    # Rotate among a few numbers for international recipients.
    def sms_numbers
      @sms_numbers ||= {
        twilio: [
          "4152339579",
          "4152339591",
          "4152339592",
          "4152339559",
          "4152339562",
          "4152339556",
          "4152339119",
          "4152339144",
          "4152339117",
          "4152339138",
          "4152339155",
          "4152339104",
          "4154888210",
          "4154888243",
          "4154888269",
          "4154888273",
          "4154888272",
          "4154888317",
          "4154888313",
          "4154888399",
          "4154888349",
          "4154888318",
        ],
        nexmo: [
          "github",
        ],
        test: [
          "1231231234",
          "2342342345",
          "3453453456",
        ],
        local: [
          "1010101010",
        ],
      }
    end
    attr_writer :sms_numbers

    def sms_short_code
      @sms_short_code ||= 448482
    end
    attr_writer :sms_short_code

    # Twilio API information
    attr_accessor :twilio_sid
    attr_accessor :twilio_token

    # Nexmo API information
    attr_accessor :nexmo_api_key
    attr_accessor :nexmo_api_secret

    # Determine whether site_admin scope is required for API resources that make
    # use of the site_admin scope. (This scope is used by API resources that are
    # only accessible to site admins).
    #
    # site_admin scope is NOT required by default in Enterprise environments. It
    # is required by default everywhere else.
    #
    # Returns a Boolean.
    def require_site_admin_scope
      return @require_site_admin_scope if defined?(@require_site_admin_scope)
      @require_site_admin_scope = (!!ENV["REQUIRE_SITE_ADMIN_SCOPE"] || !enterprise?)
    end
    attr_writer :require_site_admin_scope
    alias :require_site_admin_scope? :require_site_admin_scope

    attr_writer :stafftools_sessions_enabled
    def stafftools_sessions_enabled?
      return @stafftools_sessions_enabled if defined?(@stafftools_sessions_enabled)
      @stafftools_sessions_enabled = !enterprise?
    end

    attr_writer :hookshot_enabled
    def hookshot_enabled?
      @hookshot_enabled ||= !enterprise?
    end

    attr_writer :hook_limit
    def hook_limit
      @hook_limit ||= enterprise? ? 250 : 20
    end

    attr_writer :downloads_enabled
    def downloads_enabled?
      @downloads_enabled ||= !enterprise?
    end

    # Are repositories stored in name-with-owner order on disk?
    attr_writer :nwo_repo_storage
    def nwo_repo_storage?
      enterprise?
    end

    def trusted_oauth_apps_org_name
      if GitHub.enterprise? && !Rails.test?
        "github-enterprise"
      else
        "github"
      end
    end

    # Find the trusted Organization.  This Organization is expected to exist already.
    #
    # Returns an Organization or nil
    def trusted_oauth_apps_owner
      # Use Organization.unscoped to avoid inheriting any temporary scopes.
      # For example, in the following code:
      #
      #     current_user.organizations.oauth_app_policy_met_by(current_app)
      #
      # Organization.find_by_login("github") runs this query:
      #
      #     SELECT `users`.*
      #       FROM `users`
      #       WHERE `users`.`type` IN ('Organization') AND
      #             `users`.`id` IN (<current_user.id>) AND
      #             `users`.`login` = 'github'
      #       LIMIT 1
      #
      # and Organization.unscoped.find_by_login runs this query:
      #
      #    SELECT `users`.*
      #      FROM `users`
      #      WHERE `users`.`type` IN ('Organization') AND
      #            `users`.`login` = 'github'
      #      LIMIT 1
      Organization.unscoped.find_by_login(trusted_oauth_apps_org_name)
    end

    # The database ID of the trusted Apps owner (the GitHub organization). This
    # value is memoized (unlike `trusted_oauth_apps_owner`) because the value
    # should never change during the lifetime of a Rails process.
    #
    # Returns an Integer or nil.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def trusted_apps_owner_id
      @trusted_apps_owner_id ||=
        trusted_oauth_apps_owner&.id
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def trusted_apps_owner_id=(id)
      @trusted_apps_owner_id = id
    end

    # https://support.microsoft.com/en-us/help/4501231/microsoft-account-link-your-github-account
    def microsoft_linked_identity_app_id
      681659
    end

    # Allow TOTPs to be reused within the window for which they are valid?
    #
    # Returns boolean.
    def two_factor_allow_reuse?
      true
    end

    def traffic_graphs_enabled?
      @traffic_graphs_enabled ||= !enterprise?
    end
    attr_writer :traffic_graphs_enabled

    # Whether a (non admin) user can create organizations on the install
    # The user handling bits are in model_settings_dependency.
    #
    # Returns true for .com, uses setting if it exists.
    def user_can_create_organizations?
      !GitHub.enterprise? || GitHub.org_creation_enabled?
    end

    # Whether Enterprise only API functionality should be enabled
    def enterprise_only_api_enabled?
      GitHub.enterprise?
    end

    # Whether or not you can disable git ssh access controls
    def can_disable_git_ssh_access?
      GitHub.enterprise?
    end

    def interaction_limits_enabled?
      !GitHub.enterprise?
    end

    def git_password_auth_supported?
      GitHub.enterprise?
    end

    def api_password_auth_supported?
      GitHub.enterprise?
    end

    def git_repld_enabled?
      return @git_repld_enabled if defined? @git_repld_enabled
      @git_repld_enabled = false
    end
    attr_writer :git_repld_enabled

    # Whether business member invitations can be bypassed.
    #
    # This feature is enabled in the single business environment (GitHub
    # Enterprise) which allows global business admins to add new business
    # admins directly rather than using an invitation flow.
    def bypass_business_member_invites_enabled?
      GitHub.single_business_environment?
    end

    # Whether organization invitations can be bypassed.
    #
    # This feature is enabled on Enterprise in order to
    # allow Owners to add employees directly to their Organizations.
    def bypass_org_invites_enabled?
      GitHub.enterprise?
    end

    # The organization invitation rate limit for new organizations (less than
    # a month old).
    def org_invite_rate_limit_untrusted
      @org_invite_rate_limit_untrusted ||= (ENV["ORG_INVITE_RATE_LIMIT_UNTRUSTED"] || 50).to_i
    end

    # The organization invitation rate limit for organizations older than the
    # required age minimum.
    def org_invite_rate_limit_trusted
      @org_invite_rate_limit_trusted ||= (ENV["ORG_INVITE_RATE_LIMIT_TRUSTED"] || 500).to_i
    end

    # The organization invitation rate limit for organizations on paying plans.
    def org_invite_rate_limit_paying
      @org_invite_rate_limit_paying ||= (ENV["ORG_INVITE_RATE_LIMIT_PAYING"] || 500).to_i
    end

    def repository_invitation_rate_limit
      @repository_invitation_rate_limit ||= (ENV["REPOSITORY_INVITATION_RATE_LIMIT"] || 50).to_i
    end

    # Are user invitations enabled in this environment?
    #
    # User invitations are currently only enabled on Enterprise, as long as
    # the current setup allows built-in users (e.g., built-in auth itself or
    # an external auth with the fallback to built-in users enabled)
    def user_invites_enabled?
      GitHub.enterprise? && GitHub.auth.allow_builtin_users?
    end

    # Devtools don't currently exist in Enterprise
    def devtools_enabled?
      @devtools_enabled = !enterprise?
    end
    attr_writer :devtools_enabled

    # If graphite is disabled, `GitHub.stats` is configured with a client that
    # is a no-op for every operation.
    def graphite_disabled?
      return true if Rails.test?
      ENV["GRAPHITE_DISABLED"] == "1"
    end

    # The list of stats hosts to connect to.
    # Configured in `environments/*.rb`
    attr_accessor :stats_hosts

    # A whitelist of allowed stats keys. Only applied if this is set, otherwise
    # no whitelisting occurs.
    attr_accessor :stats_whitelist

    # Is datadog metric reporting enabled or not. dogstats is configured with a
    # client that noops all calls if this is not true.
    def datadog_enabled?
      return @datadog_enabled if defined?(@datadog_enabled)
      @datadog_enabled = true
    end
    attr_accessor :datadog_enabled

    # String URL of the aleph code navigation service
    attr_accessor :aleph_url

    # String HMAC key required for calling the aleph code navigation service.
    attr_accessor :aleph_api_hmac_key

    # Alpeh code navigation is only available on github.com right now.
    def aleph_code_navigation_enabled?
      return @aleph_code_navigation_enabled if defined? @aleph_code_navigation_enabled
      @aleph_code_navigation_enabled = !GitHub.enterprise?
    end

    # String URL of the treelights syntax highlighting service
    attr_accessor :treelights_url

    # String URL of the turboscan service
    attr_accessor :turboscan_url
    # Sets the HMAC key used to authenticate with the turboscan service
    attr_accessor :turboscan_hmac_key
    # Sets S3 Bucket for uploading files for turboscan
    attr_accessor :turboscan_s3_bucket

    # String URL of the scanitizer service
    attr_accessor :scanitizer_url
    # Sets the HMAC key used to authenticate with the scanitizer service
    attr_accessor :scanitizer_hmac_key

    # Credentials for mobile push notifications.
    attr_accessor :google_fcm_url
    attr_accessor :google_fcm_server_key
    attr_accessor :google_fcm_private_key

    # Configuration for the policy service.
    attr_accessor :policy_service_url
    attr_accessor :policy_service_connection_timeout
    attr_accessor :policy_service_response_timeout
    attr_accessor :policy_service_hmac_key

    # Codespaces
    attr_accessor :codespaces_service_principal
    attr_accessor :codespaces_billing_queues_and_tokens
    attr_accessor :codespaces_subscriptions
    attr_accessor :codespaces_app_key
    attr_accessor :codespaces_app_ppe_key
    attr_accessor :codespaces_app_dev_key
    attr_accessor :codespaces_vscode_desktop_extension_key
    attr_accessor :codespaces_vscs_environment
    attr_accessor :codespaces_per_user_limit

    def codespaces_enabled?
      !GitHub.enterprise? && GitHub.codespaces_service_principal[:client_secret].present?
    end

    # Dependabot configuration
    attr_accessor :dependabot_url
    attr_accessor :dependabot_hmac_key

    # PackageRegistry configuration
    attr_accessor :package_registry_metadata_url
    attr_accessor :package_registry_metadata_hmac_key

    # Token scanning service configuration
    attr_accessor :token_scanning_url
    attr_accessor :token_scanning_hmac_key

    # Should queries over `query_size_reporting_threshold` bytes be
    # reported to haystack?
    def large_queries_reported?
      !GitHub.enterprise? && Rails.production?
    end

    # Size in bytes over which SQL queries should be considered
    # excessively large and reported to haystack.
    def query_size_reporting_threshold
      return @query_size_reporting_threshold if defined? @query_size_reporting_threshold
      @query_size_reporting_threshold = if large_queries_reported?
        1.megabyte
      else
        Float::INFINITY
      end
    end

    # Can users opt in to view repos that have been flagged as
    # objectionable?
    def opt_in_to_restricted_repo_enabled?
      !enterprise?
    end


    # ======================================================================== #
    #                         END OF CONFIG OPTIONS                            #
    # ======================================================================== #

    ##
    # Utilities

    # The environment configuration. In dotcom production, this is backed by the
    # gpanel `.app-config/production.plain` file directly rather than using ENV,
    # and supports per-datacenter prefixes.
    # In all other cases it's simply a wrapper for ENV.
    def environment
      return @environment if @environment
      @environment = Config::PrefixEnvironment.new
    end
    attr_writer :environment

    # Reload the config.yml values and set each config attribute.
    def reload_config
      import_yaml_config "#{Rails.root}/config.yml"
    end

    # Load config written to disk by heaven gPanel support
    def reload_gpanel_config
      gpanel_config = "#{Rails.root}/.app-config/#{Rails.env}.rb"
      if File.file? gpanel_config
        load gpanel_config
        @environment = nil
      end
    end

    # Reset all memoized configuration variables to an undefined state, causing
    # them to be re-established the next time their accessed. This also calls
    # #reload_config to load in config values from the global and environment
    # config.yml files.
    def reset_config!
      instance_variables.each { |varname| remove_instance_variable(varname) }
      # reset ldap so we can reload the config.
      load_environment_config
    end

    # Loads the environment specific config file. The RAILS_ROOT/config.yml
    # values are loaded twice: before the environment config so that global
    # config is initially available for the environment file, and after the
    # environment config so that global values override environment values.
    def load_environment_config
      if defined? Rails.env
        reload_config

        reload_gpanel_config

        # Enterprise doesn't ship production.rb environment file to avoid exposing
        # sensitive credentials.
        load "github/config/environments/#{Rails.env}.rb" unless Rails.production? && enterprise?
        load "github/config/environments/enterprise.rb" if enterprise?

        # load the config.yml values in over the environment specific config.
        reload_config
      else
        raise LoadError, "Rails.env is not set"
      end
    end

    # Imports config values from the YAML file specified into the current
    # config. The config file may include values for any attributes defined in
    # this module.
    #
    # file - The string path to the YAML file to load.
    #
    # Returns nothing.
    # Raises RuntimeError when the config file includes an unknown key.
    #
    # Examples:
    #   The following YAML file would cause the ssl config attribute to be set
    #   true and the host_name attribute to be set to "github.apple.com":
    #
    #   ssl:       true
    #   host_name: github.apple.com
    def import_yaml_config(file)
      load_yaml_config(file).each do |key, value|
        if respond_to?("#{key}=")
          __send__("#{key}=", value)
        else
          fail "unknown config value: #{key.inspect}"
        end
      end
    end

    # Load a YAML file if it exists and return its deserialized contents.
    #
    # Returns a Hash of config values.
    def load_yaml_config(file)
      if !Rails.test? && File.exist?(file)
        require "yaml"
        YAML.load_file(file) || {}
      else
        {}
      end
    end

    # Public: Determine whether the limit in mentioned users in
    # comments/issues/pull request... is enabled or no.
    #
    # Enterprise is the most common case where we want to disable the spam
    # validation because the number of users are determined by the license.
    def prevent_mention_spam?
      !enterprise?
    end

    # Revoke OAuth accesses and SSH keys for new staff?
    def new_staff_precautions?
      !enterprise?
    end

    # We only want to enforce employee requirements when this is *not*
    # Enterprise.
    def require_employee_for_site_admin?
      !GitHub.enterprise?
    end

    def require_two_factor_for_site_admin?
      return @require_two_factor_for_site_admin if defined? @require_two_factor_for_site_admin
      @require_two_factor_for_site_admin = !GitHub.enterprise? && !Rails.development? && !Rails.test? && !GitHub.environment["DISABLE_TWO_FACTOR_FOR_SITE_ADMIN"]
    end
    attr_writer :require_two_factor_for_site_admin

    # No 2FA SMS in enterprise
    def two_factor_sms_enabled?
      !enterprise?
    end

    def sign_in_analysis_enabled?
      !enterprise?
    end

    # If we are tracking authentication records and devices, IP-based rate
    # limits aren't as effective as device verification.
    def web_ip_lockouts_enabled?
      !sign_in_analysis_enabled?
    end


    def weak_password_checking_enabled?
      !enterprise?
    end

    def strict_authentication_records?
      sign_in_analysis_enabled? && Rails.env.test?
    end

    # See https://developer.github.com/changes/2019-11-05-deprecated-passwords-and-authorizations-api/
    def authorization_apis_and_password_authentication_deprecated?
      !GitHub.enterprise?
    end

    # Disable Mirror repos on Enterprise
    def mirrors_enabled?
      !enterprise?
    end

    # Enable unlimited git.maxobjectsize in Enterprise
    def unlimited_max_object_size_enabled?
      enterprise?
    end

    # A large number of users in an assignee list (e.g. in the pull request view)
    # can lead to a timeout. Therefore we limit the number to 1500 on dotcom.
    # This is calculated based on all but the very largest organizations/teams in
    # the installation as of 2015-03-06, plus a couple hundred for wiggle room.
    # Without this limit, the show_menu_content assignees partial won't load in
    # time.
    # On GHES customers have sometimes more users. Since GHES has a higher timeout
    # too we can be more gracious with this limit.
    def assignees_list_limit
      @assignees_list_limit ||= 1500
    end
    attr_writer :assignees_list_limit

    def cluster_web_server?
      return @cluster_web_server if defined? @cluster_web_server
      @cluster_web_server = false
    end
    attr_writer :cluster_web_server

    def cluster_git_server?
      return @cluster_git_server if defined? @cluster_git_server
      @cluster_git_server = false
    end
    attr_writer :cluster_git_server

    def cluster_pages_server?
      return @cluster_pages_server if defined? @cluster_pages_server
      @cluster_pages_server = false
    end
    attr_writer :cluster_pages_server

    # Base IPs for our physical datacenter that are used for
    # multiple purposes such as Git, Web, API & hooks
    def base_ips
      [
        "192.30.252.0/22",
        "185.199.108.0/22",
        "140.82.112.0/20",
      ]
    end

    # IP addresses that service hooks are sent from.
    #
    # This is returned from the API endpoint
    # and listed on the service hooks page.
    def hook_ips
      base_ips
    end

    # IPs used for web access. This is our datacenters plus vPOPs addresses for web traffic.
    def web_ips
      base_ips + [
        # vpc-ap-northeast-1
        "13.114.40.48/32",
        "52.192.72.89/32",
        "52.69.186.44/32",
        # vpc-ap-northeast-2
        "15.164.81.167/32",
        "52.78.231.108/32",
        # vpc-ap-south-1
        "13.234.176.102/32",
        "13.234.210.38/32",
        # vpc-ap-southeast-1
        "13.229.188.59/32",
        "13.250.177.223/32",
        "52.74.223.119/32",
        # vpc-ap-southeast-2
        "13.236.229.21/32",
        "13.237.44.5/32",
        "52.64.108.95/32",
        # vpc-sa-east-1
        "18.228.52.138/32",
        "18.228.67.229/32",
        "18.231.5.6/32",
      ]
    end

    # Normal Git traffic goes over github.com so it's the same as the web UI IPs,
    # except for the git-ssh-over-port-443 IPs (ssh.github.com) which can also be used.
    def git_ips
      web_ips + [
        # vpc-ap-northeast-1
        "18.181.13.223/32",
        "54.238.117.237/32",
        "54.168.17.15/32",
        # vpc-ap-northeast-2
        "3.34.26.58/32",
        "13.125.114.27/32",
        # vpc-ap-south-1
        "3.7.2.84/32",
        "3.6.106.81/32",
        # vpc-ap-southeast-1
        "18.140.96.234/32",
        "18.141.90.153/32",
        "18.138.202.180/32",
        # vpc-ap-southeast-2
        "52.63.152.235/32",
        "3.105.147.174/32",
        "3.106.158.203/32",
        # vpc-sa-east-1
        "54.233.131.104/32",
        "18.231.104.233/32",
        "18.228.167.86/32",
      ]
    end

    # IPs used for API access. This is our datacenters plus vPOPs addresses for API traffic.
    def api_ips
      base_ips + [
        # vpc-ap-northeast-1
        "13.230.158.120/32",
        "18.179.245.253/32",
        "52.69.239.207/32",
        # vpc-ap-northeast-2
        "13.209.163.61/32",
        "54.180.75.25/32",
        # vpc-ap-south-1
        "13.233.76.15/32",
        "13.234.168.60/32",
        # vpc-ap-southeast-1
        "13.250.168.23/32",
        "13.250.94.254/32",
        "54.169.195.247/32",
        # vpc-ap-southeast-2
        "13.236.14.80/32",
        "13.238.54.232/32",
        "52.63.231.178/32",
        # vpc-sa-east-1
        "18.229.199.252/32",
        "54.207.47.76/32",
      ]
    end

    # IP addresses for GitHub Pages' A records.
    def pages_a_record_ips
      [
        "192.30.252.153/32",  # our data center
        "192.30.252.154/32",
        "185.199.108.153/32", # broadcast by Fastly
        "185.199.109.153/32",
        "185.199.110.153/32",
        "185.199.111.153/32",
      ]
    end

    # IP addresses that the importer connects from.
    def porter_worker_ips
      [
        "192.30.252.0/22",  # Porter via our proxy
        "185.199.108.0/22", # Porter via our proxy
        "140.82.112.0/20",   # Porter via our proxy
      ]
    end

    # Public: Is scientist enabled?
    #
    # In enterprise, returns false
    # In production, checks the configured region
    # Otherwise returns true
    def scientist_enabled?
      if GitHub.enterprise?
        false
      elsif Rails.production?
        scientist_region == GitHub.server_region
      else
        true
      end
    end

    # Public: What region is scientist enabled for?
    #
    # Until such time as scientist's back-end storage system is changed
    # to handle multiple region, only run scientist in one region,
    # configured via SCIENTIST_REGION.
    def scientist_region
      GitHub.environment["SCIENTIST_REGION"]
    end

    # Public: Is performance profiling enabled?
    attr_accessor :profiling_enabled
    alias profiling_enabled? profiling_enabled

    # Public: Is the performance stats UI enabled?
    attr_accessor :stats_ui_enabled
    alias stats_ui_enabled? stats_ui_enabled

    # Public: which repository networks do we never want to be considered public?
    #
    # This allows us to provide an extra level of protection to make sure GitHub's
    # most sensitive repositories aren't accidentally turned public.  So, even
    # if visibility is toggled on these, they will always be treated as private
    # repositories.
    def never_public_networks
      return [].freeze if GitHub.enterprise?

      ["github/github", "github/puppet"].freeze
    end

   # Public: the ids of the repository networks we never want to be considered public
   def never_public_network_ids
     return @never_public_network_ids if defined? @never_public_network_ids

     networks = GitHub.never_public_networks
     ids = networks.map { |nwo| Repository.nwo(nwo).try(:network_id) }
     @never_public_network_ids = ids.compact.uniq.freeze
   end

   def reset_never_public_network_ids
     remove_instance_variable "@never_public_network_ids" if defined? @never_public_network_ids
   end

    # Accounts or people that work for GitHub that we don't want
    # to publicly display as staff or be present on the team page.
    def hidden_teamsters
      @hidden_teamsters ||= %w(evilshawn ghmonitor gregose-tmp hubot ice799 maki1022 mayashino monitors StreamingEagle boxen-ci btoews)
    end

    def hidden_teamster?(user)
      hidden_teamsters.include?(user.login)
    end

    def max_ui_pagination_page
      MAX_UI_PAGINATION_PAGE
    end

    # We're using the Shopify/verdict gem for storing associations for A/B tests (and other
    # science experiments on users that have test/control groups). The storage for experiments
    # is pluggable in verdict; currently, we'll write to MySQL tables behind the scenes.
    def user_experiment_storage
      @user_experiment_storage ||= GitHub::UserResearch::ExperimentDatabaseStorage.new
    end

    def user_experiments_enabled?
      return @user_experiments_enabled if defined?(@user_experiments_enabled)
      @user_experiments_enabled = true
    end
    attr_writer :user_experiments_enabled

    def content_creation_rate_limiting_enabled?
      return @content_creation_rate_limiting_enabled if defined?(@content_creation_rate_limiting_enabled)
      @content_creation_rate_limiting_enabled = !GitHub.enterprise?
    end
    attr_writer :content_creation_rate_limiting_enabled

    # Request parameters to filter from logs and exceptions
    #
    # These are filtered by Rails based on a substring match
    def filtered_params
      [:password, :token, :credit_card, :value, :oauth_verifier, :bt_signature, :client_secret]
    end

    # Closure for creating a whitelist for Rails log filtering.
    # We only log things in the whitelist here.
    def filtered_params_proc
      lambda do |key, value|
        unless key =~ WHITELISTED_PARAMETERS
          value.replace(SANITIZED_VALUE) if value.respond_to?(:replace)
        end
      end
    end

    def merge_matrix_pull_request
      if ENV["MERGE_BUTTON_MATRIX_PULL_REQUEST_ID"]
        pull = PullRequest.find(ENV["MERGE_BUTTON_MATRIX_PULL_REQUEST_ID"])
      elsif Rails.env.development?
        repo = Repository.with_name_with_owner("github/linguist")
        pull = repo.pull_requests.last
      end
    end

    def ruby_2_6?
      Gem::Version.new(RUBY_VERSION) >= Gem::Version.new("2.6.0") &&
        Gem::Version.new(RUBY_VERSION) < Gem::Version.new("2.7.0")
    end

    def ruby_2_7?
      Gem::Version.new(RUBY_VERSION) >= Gem::Version.new("2.7.0") &&
        Gem::Version.new(RUBY_VERSION) < Gem::Version.new("2.8.0")
    end

    def rails_6_0?
      rails_version_major == 6 && rails_version_minor == 0
    end

    def rails_6_1?
      rails_version_major == 6 && rails_version_minor == 1
    end

    def rails_version_major
      Rails::VERSION::MAJOR
    end
    private :rails_version_major

    def rails_version_minor
      Rails::VERSION::MINOR
    end
    private :rails_version_minor

    def origin_verification_enabled?
      !enterprise?
    end

    # When SameSite cookie support is first deployed, any initial request that
    # does CSRF validation will fail, since the strict SameSite cookie will not
    # be set yet. This isn't a problem for dotcom, as we currently hide this
    # feature behind a feature flag. So, we will have several weeks to allow all
    # users to start receiving both the normal session cookie AND the new strict
    # SameSite cookie. As a result, we can be confident everyone already has
    # both cookies when we ship the feature. But, on Enterprise, this isn't the
    # case. So, for now, we will disable support. Maybe we can enable it after a
    # few releases and/or if we warn people when they upgrade.
    def same_site_cookie_verification_enabled?
      !enterprise? && same_site_cookie_enabled?
    end

    # Even if we are not validating SameSite cookies (i.e.
    # `same_site_cookie_verification_enabled?` is `false`), we can still set the
    # cookie for future use. However, since we are using cookie prefixes with
    # our SameSite cookie implementation, we choose to only set the SameSite
    # cookie for installations that have SSL enabled.
    def same_site_cookie_enabled?
      ssl?
    end

    # For environments where we control the IPs in use we leverage IP
    # restrictions as a belt and suspenders protection.
    def remote_ip_restrictions_enabled?
      !enterprise?
    end

    def experiments_graphql_enabled?
      return @experiments_graphql_enabled if defined?(@experiments_graphql_enabled)
      @experiments_graphql_enabled = true
    end
    attr_writer :experiments_graphql_enabled

    def flipper_graphql_enabled?
      return @flipper_graphql_enabled if defined?(@flipper_graphql_enabled)
      @flipper_graphql_enabled = true
    end
    attr_writer :flipper_graphql_enabled

    def flipper_ui_enabled?
      return @flipper_ui_enabled if defined?(@flipper_ui_enabled)
      @flipper_ui_enabled = true
    end
    attr_writer :flipper_ui_enabled

    def request_limiting_enabled?
      @request_limiting_enabled
    end
    attr_writer :request_limiting_enabled

    def chatterbox_enabled?
      return @chatterbox_enabled if defined?(@chatterbox_enabled)
      @chatterbox_enabled = true
    end
    attr_writer :chatterbox_enabled

    def allow_cross_origin_referrer_leak?
      !enterprise?
    end

    def read_only?
      return true if Thread.current[:github_read_only_mode]
      return @read_only if defined?(@read_only)

      @read_only = File.exist?(Rails.root.join("tmp/readonly.txt"))
    end
    attr_writer :read_only

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def resque_redis_disabled?
      @resque_redis_disabled ||= GitHub.environment["#{GitHub.role.upcase}_RESQUE_REDIS_DISABLED"] == "1"
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def reset_resque_redis_disabled
      @resque_redis_disabled = nil
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def rate_limiter_redis_disabled?
      @rate_limiter_redis_disabled ||= GitHub.environment["#{GitHub.role.upcase}_RATE_LIMITER_REDIS_DISABLED"] == "1"
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def reset_rate_limiter_redis_disabled
      @rate_limiter_redis_disabled = nil
    end

    # Given a github host, attempt to determine the site. Originally added to
    # allow for tagging the rpc_site on all MySQL queries to aid in determining
    # cross-site queries.
    #
    # Examples:
    #
    #   GitHub.site_from_host("db-mysql-9082342.cp1-iad.github.net") => "cp1-iad"
    #   GitHub.site_from_host("db-mysql-c0c881d.sdc42-sea.github.net") => "sdc42-sea"
    #
    # Returns String representing the site of the host.
    def site_from_host(host)
      site = if host =~ /cp1-prd\.iad\.github\.net/
        "cp1-iad"
      else
        match = /(?<site>[^\.]+)\.github\.net\Z/i.match(host)
        match && match[:site]
      end

      site && SITE_WHITELIST.include?(site) ? site : "unknown"
    end

    def areas_of_responsibility
      return @areas_of_responsibility if defined?(@areas_of_responsibility)
      path = Rails.root.join("docs", "areas-of-responsibility.yaml")
      @areas_of_responsibility = path.exist? ? YAML.load_file(path).freeze : nil
    end

    def codeowners
      return @codeowners if defined?(@codeowners)
      path = Rails.root.join("CODEOWNERS")
      @codeowners = path.exist? ? ::Codeowners::File.new(path.read) : nil
    end

    def serviceowners
      return @serviceowners if defined?(@serviceowners)
      path = Rails.root.join("SERVICEOWNERS")
      @serviceowners = path.exist? ? GitHub::Serviceowners.new(path.read) : nil
    end

    # Is operator mode enabled for all users?
    #
    # When operator mode is enabled, debugging output is displayed to the
    # client during Git push operations.
    def global_operator_mode_enabled?
      !!@global_operator_mode_enabled
    end
    attr_writer :global_operator_mode_enabled

    # Ops gets paged if there's < 10 GB available
    SHARD_SLACK_SPACE = 15.0 * (1 << 30)

    ENTERPRISE_SHARD_SLACK_SPACE = 2 * (1 << 30)

    # The amount of slack disk space required
    # on fileservers, in KB.
    def shard_slack_space
      gb = if GitHub.enterprise?
        ENTERPRISE_SHARD_SLACK_SPACE
      else
        SHARD_SLACK_SPACE
      end

      gb / (1 << 10)
    end

    # What port should we use in the upstream for proxies when pointing mysql
    # in the app at toxiproxy. Checks for env var, then falls back to mysql's
    # default port.
    #
    # NOTE: This is only used for develompment, test and ci to simulate network
    # issues by proxying through toxiproxy.
    def toxiproxy_upstream_mysql_port
      @toxiproxy_upstream_mysql_port ||= ENV.fetch("TOXIPROXY_UPSTREAM_MYSQL_PORT", 3306)
    end
    attr_writer :toxiproxy_upstream_mysql_port

    def cas_host
      url_origin(cas_url)
    end

    # Allow overriding the number of accesses an OAuth application is allowed to
    # create for a given app/user/scope combination.
    def oauth_access_limit_overrides_enabled?
      Rails.production? && !enterprise?
    end

    # Whether or not this server will primarily run gitauth requests
    def gitauth_host?
      !!ENV["GITAUTH"]
    end

    # The number of unicorn workers to run on an enterprise instance.
    def enterprise_unicorn_worker_count
      if gitauth_host?
        (ENV["ENTERPRISE_GITAUTH_WORKERS"] || 1).to_i
      else
        (ENV["ENTERPRISE_GITHUB_WORKERS"] || 4).to_i
      end
    end

    # This host is a staff host
    def staff_host?
      ENV["STAFF_ENVIRONMENT"] != nil || local_host_name.split(".").first =~ /^github-staff\d+/
    end

    # This host is a babeld host
    def babeld_host?
      local_host_name =~ /^github-babel/
    end

    # The parsed contents of /etc/github/metadata.json (a Hash), or nil
    def server_metadata
      return @_parsed_metadata_json if defined?(@_parsed_metadata_json)
      begin
        @_parsed_metadata_json = read_metadata
      rescue
        @_parsed_metadata_json = {}
      end
    end

    # The region where the server is located
    def server_region
      return @server_region if defined?(@server_region)
      @server_region = server_metadata["region"]
    end
    attr_writer :server_region

    # The site where the server is located
    def server_site
      return @server_site if defined?(@server_site)
      @server_site = server_metadata["site"]
    end
    attr_writer :server_site

    # This host is running in production
    def production_host?
      server_metadata["env"] == "production"
    end

    # The number of unicorn workers to spin up for this server.
    def unicorn_worker_count
      if (worker_count = ENV["GH_UNICORN_WORKER_COUNT"])
        return worker_count.to_i
      end

      worker_count = begin
        Integer(File.read("/etc/github/unicorn_worker_count"))
      rescue Errno::ENOENT, Errno::EPERM, ArgumentError
        nil
      end
      unless worker_count.nil?
        return worker_count
      end

      if GitHub.enterprise? && Rails.production?
        return enterprise_unicorn_worker_count
      end

      if staff_host?
        return 3
      end

      if gitauth_host?
        return (ENV["GITAUTH_WORKER_COUNT"] || 1).to_i unless Rails.production?
        return [(3 * Etc.nprocessors), 120].min if babeld_host?
        return 12 if production_host?
        return 2
      end

      if Rails.production?
        return (2 * Etc.nprocessors) if production_host?
        return 4
      end
      2
    end

    # Should unicorn do its check_client_connection thing?
    #   https://engineering.shopify.com/17489012-what-does-your-webserver-do-when-a-user-hits-refresh#axzz2q3YWu2Jr
    def unicorn_check_client_connection?
      Rails.development? || Rails.test?
    end

    def update_protected_branches_setting_enabled?
      return @update_protected_branches_setting_enabled if defined?(@update_protected_branches_setting_enabled)
      false
    end
    attr_writer :update_protected_branches_setting_enabled

    def dependency_graph_enabled?
      return @dependency_graph_enabled if defined?(@dependency_graph_enabled)
      !GitHub.enterprise?
    end
    attr_writer :dependency_graph_enabled

    def dependency_graph_api_url
      ENV["DEPENDENCY_GRAPH_API_URL"] || @dependency_graph_api_url
    end
    attr_writer :dependency_graph_api_url

    def dependency_graph_api_slow_query_url
      # just setting the DEPENDENCY_GRAPH_API_URL environment variable will set both the default and the slow query URL
      ENV["DEPENDENCY_GRAPH_API_SLOW_QUERY_URL"] || @dependency_graph_api_slow_query_url || dependency_graph_api_url
    end
    attr_writer :dependency_graph_api_slow_query_url

    # A kill switch for all Geyser search functionality, regardless of users or
    # repos opted in. This is intentionally not memoized due to reasons
    # outlined in https://github.com/github/pe-search/issues/645
    def geyser_search_enabled?
      GitHub.flipper[:geyser_search_enabled].enabled?
    end

    # A kill switch for all Geyser ingest functionality, regardless of repos
    # opted in. This is intentionally not memoized due to reasons outlined in
    # https://github.com/github/pe-search/issues/645
    def geyser_ingest_enabled?
      GitHub.flipper[:geyser_ingest_enabled].enabled?
    end

    def geyser_search_api_url
      ENV["GEYSER_API_URL"] || @geyser_search_api_url
    end
    attr_writer :geyser_search_api_url

    def geyser_admin_api_url
      ENV["GEYSER_ADMIN_API_URL"] || @geyser_admin_api_url
    end
    attr_writer :geyser_admin_api_url

    def geyser_search_api_hmac_key
      ENV["GEYSER_SEARCH_API_HMAC_KEY"] || @geyser_search_api_hmac_key
    end
    attr_writer :geyser_search_api_hmac_key

    def geyser_admin_api_hmac_key
      ENV["GEYSER_ADMIN_API_HMAC_KEY"] || @geyser_admin_api_hmac_key
    end
    attr_writer :geyser_admin_api_hmac_key

    def geyser_search_api_lab_url
      ENV["GEYSER_API_LAB_URL"] || @geyser_search_api_lab_url
    end
    attr_writer :geyser_search_api_lab_url

    def geyser_admin_api_lab_url
      ENV["GEYSER_ADMIN_API_LAB_URL"] || @geyser_admin_api_lab_url
    end
    attr_writer :geyser_admin_api_lab_url

    def geyser_search_api_lab_hmac_key
      ENV["GEYSER_SEARCH_API_LAB_HMAC_KEY"] || @geyser_search_api_lab_hmac_key
    end
    attr_writer :geyser_search_api_lab_hmac_key

    def geyser_admin_api_lab_hmac_key
      ENV["GEYSER_ADMIN_API_LAB_HMAC_KEY"] || @geyser_admin_api_lab_hmac_key
    end
    attr_writer :geyser_admin_api_lab_hmac_key

    def vulnerability_alerting_and_settings_enabled?
      return @vulnerability_alerting_and_settings_enabled if defined?(@vulnerability_alerting_and_settings_enabled)
      @vulnerability_alerting_and_settings_enabled = !GitHub.enterprise?
    end
    attr_writer :vulnerability_alerting_and_settings_enabled

    def dedicated_alerts_ui_enabled?
      return @dedicated_alerts_ui_enabled if defined? @dedicated_alerts_ui_enabled
      @dedicated_alerts_ui_enabled = !GitHub.enterprise?
    end
    attr_writer :dedicated_alerts_ui_enabled

    def security_advisories_enabled?
      return @security_advisories_enabled if defined? @security_advisories_enabled
      @security_advisories_enabled = !GitHub.enterprise?
    end
    attr_writer :security_advisories_enabled

    def repository_advisories_enabled?
      return @repository_advisories_enabled if defined? @repository_advisories_enabled
      @repository_advisories_enabled = !GitHub.enterprise?
    end

    def global_advisories_enabled?
      return @global_advisories_enabled if defined? @global_advisories_enabled
      @global_advisories_enabled = !GitHub.enterprise?
    end
    attr_writer :global_advisories_enabled

    def dependabot_enabled?
      return @dependabot_enabled if defined? @dependabot_enabled
      @dependabot_enabled = !GitHub.enterprise?
    end

    # Indicates that Vulnerabilities (and VulnerableVersionRanges) can be written outside of syncing with dotcom
    def writable_vulnerabilities_enabled?
      return @writable_vulnerabilities_enabled if defined?(@writable_vulnerabilities_enabled)
      @writable_vulnerabilities = !enterprise?
    end

    # Munger is an application that provides access to data from the data
    # science pipeline: https://github.com/github/munger
    # It is used for things like getting topic suggestions in the GitHub.com
    # environment and doesn't run on Enterprise.
    def munger_available?
      return false if enterprise?
      return true if Rails.env.test?
      munger_url.present?
    end

    # The topics data API is supplied by the github/munger application
    attr_accessor :munger_url

    # Whether to log the call stack when auto_fqdn is called
    attr_accessor :log_auto_fqdn_caller

    # Whether or not this is a Kubernetes cluster. To determine this, we look
    # for a file created by the ServiceAccountAdmissionController.
    # https://kubernetes.io/docs/admin/service-accounts-admin/#service-account-admission-controller
    def kube?
      return @kube if defined?(@kube)
      namespace_file = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
      @kube = File.file?(namespace_file)
    end

    def kubernetes_cluster_name
      read_metadata["kubernetes_cluster_name"]
    end

    def lab_type
      ENV["STAFF_ENVIRONMENT"]
    end

    # Shared ready-state file for the last unicorn worker to indicate starup
    # success and unlock the /status endpoint in kube installations.
    def kube_workers_ready_file
      Rails.root.join("tmp/unicorn-workers.ready")
    end

    # Dynamic labs can be created on-the-fly by users for exploratory testing,
    # and thus don't have a fixed domain. This method can be used to determine
    # whether or not the application is running in such a context, detected by
    # the presence of a DYNAMIC_LAB_NAME and/or DYNAMIC_LAB_NAME environment
    # variable. The *type* of lab can be determined with the lab_type method.
    def dynamic_lab?
      ENV.has_key?("DYNAMIC_LAB_NAME")
    end

    # Dynamic labs are expected to be configured to serve requests on domains
    # like *.(staging|review)-lab.github.com. This method takes a domain string
    # argument and returns true if it looks like a dynamic lab domain. Use this
    # method only in situations where the dynamic_lab? is not sufficient.
    def dynamic_lab_domain?(domain)
      domain && domain.end_with?("-lab.github.com")
    end

    # Returns the root of the lab domain for use in cookies. Like
    # dynamic_lab_domain?, this method is designed to support multiple
    # dynamic labs on domains like *.(staging|review)-lab.github.com.
    def dynamic_lab_cookie_domain(domain)
      lab_domain_match = domain.match(/(\.(\w+)-lab\.github\.com)\z/)
      if lab_domain_match
        return lab_domain_match[1]
      else
        raise ArgumentError
      end
    end

    # The value of the DYNAMIC_LAB_NAME environment variable. Will return an
    # empty string unless dynamic_lab? is true
    def dynamic_lab_name
      ENV.fetch("DYNAMIC_LAB_NAME", "")
    end

    # A temporary measure to assess if we're in a staging-lab. Using this until
    # dynamic_lab? returns true for staging labs
    def staging_lab?
      GitHub.host_domain == STAGING_DOMAIN
    end

    def kubernetes_namespace
      @kubernetes_namespace ||= ENV.fetch("KUBE_NAMESPACE", "unknown")
    end

    def platform_graphql_service_tokens
      @platform_graphql_service_tokens ||= []
    end
    attr_writer :platform_graphql_service_tokens

    def platform_graphql_logging_enabled?
      @platform_graphql_logging_enabled || !GitHub.enterprise?
    end
    attr_writer :platform_graphql_logging_enabled

    def valid_graphql_service_token?(token)
      valid_token = false
      GitHub.platform_graphql_service_tokens.each do |t|
        if SecurityUtils.secure_compare(t, token)
          valid_token = true
        end
      end
      valid_token
    end

    def hookshot_payload_size_limit
      @hookshot_payload_size_limit = ENV.fetch("HOOKSHOT_PAYLOAD_SIZE_LIMIT", 26214400).to_i
    end
    attr_writer :hookshot_payload_size_limit

    def repository_quotas_enabled?
      return false if GitHub.enterprise?
      return @repository_quotas_enabled if defined?(@repository_quotas_enabled)
      @repository_quotas_enabled = true
    end
    attr_writer :repository_quotas_enabled

    # Public: Chunk size for web sockets. Breaks down a collection of web socket
    # related redis operations into a bounded chunk. Note that this number does
    # not equal the exact number of redis operations to pipeline, but rather the
    # collection size to pipeline of which each item in the collection could
    # cause one or more redis operations.
    def web_socket_chunk_size
      @web_socket_chunk_size ||= ENV.fetch("WEB_SOCKET_CHUNK_SIZE", 25_000).to_i
    end
    attr_writer :web_socket_chunk_size

    # Whether webhooks for User events are enabled
    def user_hooks_enabled?
      @user_hooks_enabled.nil? ? enterprise? : @user_hooks_enabled
    end
    attr_writer :user_hooks_enabled

    # Whether ghe-specific org hooks are enabled
    def extended_org_hooks_enabled?
      @extended_org_hooks_enabled.nil? ? enterprise? : @extended_org_hooks_enabled
    end
    attr_writer :extended_org_hooks_enabled

    # Public: The heaven enviroment the app is currently deployed to. Possible values
    # include `production` and `production/canary`. If the env var isn't set then we fall
    # back to the regular rails env.
    def heaven_env
      @heaven_env ||= ENV.fetch("HEAVEN_DEPLOYED_ENV", Rails.env)
    end

    def heaven_env_reset
      @heaven_env = nil
    end

    # Public: Figure out the specific env that the app is currently deployed to. This will
    # return the specific lab env (e.g. "garage", "lab", etc) or the production
    # env (e.g. "production", "prod/canary")
    def deployed_to
      @deployed_to ||=begin
        # kube will always have the correct env set from the heaven deploy
        return GitHub.heaven_env if GitHub.kube?

        # non-kube non-lab envs can use the default heaven env
        return GitHub.heaven_env unless GitHub.role_from_host == :lab

        # non-kube lab envs like garage should look at the sever metadata
        env = server_metadata.fetch("attributes", {})
        env = env.fetch("github", {})
        env.fetch("environment", :unknown)
      end
    end

    # Public: Format deployment environment for Failbot and Sentry
    def failbot_deployed_to
      return @failbot_deployed_to if defined?(@failbot_deployed_to)

      @failbot_deployed_to = GitHub.deployed_to.gsub(/\Aproduction\//, "")
    end

    # Public: Batch size for subscribing users. Breaks down an array of users into a bounded size.
    def subscribed_users_batch_size
      @subscribed_users_batch_size ||= ENV.fetch("SUBSCRIBED_USERS_BATCH_SIZE", 100).to_i
    end

    def deployed_to_reset
      @deployed_to = nil
    end

    # Public: Allows GitHub Enterprise instances to request a connection to
    # dotcom for deeper integration.
    def dotcom_connection_enabled?
      return @dotcom_connection_enabled if defined?(@dotcom_connection_enabled)
      @dotcom_connection_enabled = GitHub.enterprise?
    end
    attr_writer :dotcom_connection_enabled

    # Public: Allows GitHub Enterprise users to connect their account with
    # dotcom after the Enterprise instance has been connected.
    def dotcom_user_connection_enabled?
      return @dotcom_user_connection_enabled if defined?(@dotcom_user_connection_enabled)
      return GitHub.dotcom_private_search_enabled? || GitHub.dotcom_contributions_enabled?
    end
    attr_writer :dotcom_user_connection_enabled

    def chatops_endpoint_enabled?
      return @chatops_endpoint_enabled if defined?(@chatops_endpoint_enabled)
      @chatops_endpoint_enabled = !GitHub.enterprise?
    end
    attr_writer :chatops_endpoint_enabled

    # Public: Allows GitHub Enterprise administrators to enable unified
    # contributions after the Enterprise instance has been connected.
    def dotcom_contributions_configurable?
      return @dotcom_contributions_configurable if defined?(@dotcom_contributions_configurable)
      license = GitHub.enterprise? ? GitHub::Enterprise.license(sync_global_business: false) : nil
      @dotcom_contributions_configurable = license &&
        (license.github_connect_support? ||
         (!license.custom_terms.nil? && license.custom_terms?))
    end
    attr_writer :dotcom_contributions_configurable

    # Public: Allows GitHub Enterprise administrators to enable downloadable
    # Actions after the Enterprise instance has been connected.
    def dotcom_actions_download_configurable?
      return @dotcom_actions_download_configurable if defined?(@dotcom_actions_download_configurable)
      @dotcom_actions_download_configurable = GitHub.actions_enabled?
    end

    def dotcom_host_name
      @dotcom_host_name ||= "github.com"
    end
    attr_writer :dotcom_host_name

    def dotcom_host_protocol
      @dotcom_host_protocol ||= "https"
    end
    attr_writer :dotcom_host_protocol

    def dotcom_api_host_name
      @dotcom_api_host_name ||= "api.github.com"
    end
    attr_writer :dotcom_api_host_name

    def dotcom_upload_host_name
      @dotcom_upload_host_name ||= "uploads.github.com"
    end
    attr_writer :dotcom_upload_host_name

    # Prefix added to REST API calls from Enterprise to dotcom
    # (defaults to empty, but review lab requires "/api/v3")
    def dotcom_rest_api_prefix
      @dotcom_rest_api_prefix ||= ""
    end
    attr_writer :dotcom_rest_api_prefix

    # Prefix added to GraphQL API calls from Enterprise to dotcom
    # (defaults to "/graphql", but review lab requires "/api/graphql")
    def dotcom_graphql_api_prefix
      @dotcom_graphql_api_prefix ||= "/graphql"
    end
    attr_writer :dotcom_graphql_api_prefix

    # Headers added to requests from Enterprise to dotcom
    # (e.g., "User-Agent: <review lab SHA>\nSomeOtherHeader: value")
    def dotcom_request_headers
      @dotcom_request_headers ||= ""
    end
    attr_writer :dotcom_request_headers

    def funcaptcha_enabled?
      !enterprise? && !!funcaptcha_public_key && !!funcaptcha_private_key
    end

    def funcaptcha_public_key
      return @funcaptcha_public_key if defined?(@funcaptcha_public_key)

      @funcaptcha_public_key = ENV["FUNCAPTCHA_PUBLIC_KEY"]
      # Use local development API key if development and key is not provided
      @funcaptcha_public_key ||= Rails.env.development? ? "42F1CA39-6096-9C02-7935-BCC7BAB31D2C" : nil
      @funcaptcha_public_key
    end

    def funcaptcha_private_key
      return @funcaptcha_private_key if defined?(@funcaptcha_private_key)

      @funcaptcha_private_key = ENV["FUNCAPTCHA_PRIVATE_KEY"]
      # Use local ddvelopment API key if development and key is not provided
      @funcaptcha_private_key ||= Rails.env.development? ? "BFE5A88D-E4A3-1CD5-0F97-DF60D07711D9" : nil
      @funcaptcha_private_key
    end

    def dotcom_ssl_verify?
      return @dotcom_ssl_verify if defined?(@dotcom_ssl_verify)
      @dotcom_ssl_verify = true
    end
    attr_writer :dotcom_ssl_verify

    def http_proxy_config
      return @http_proxy_config if defined?(@http_proxy_config)
      @http_proxy_config = ENV["GH_HTTP_PROXY"]
    end
    attr_writer :http_proxy_config

    def no_proxy_config
      return @no_proxy_config if defined?(@no_proxy_config)
      @no_proxy_config = ENV["GH_NO_PROXY"]
    end
    attr_writer :no_proxy_config

    def disable_fast_organization_invitation_creations
      return false if Rails.env.test?
      return false if enterprise?
      true
    end

    def download_everything_button_enabled?
      !enterprise?
    end

    def launch_grpc_ssl_enabled?
      !GitHub.enterprise?
    end

    # Public: An aqueduct client.
    #
    # Returns an Aqueduct::Client.
    def aqueduct
      @aqueduct ||= build_aqueduct_client
    end

    def build_aqueduct_client(app: aqueduct_app_name, url: aqueduct_url)
      ::Aqueduct::Client.new(
        app: app,
        url: url,
        site: GitHub.server_site || "localhost",
        timeout: foreground? ? 2 : 5,
        receive_timeout: 10,
        send_retries: 1,
        client_id: aqueduct_client_id,
        tags: aqueduct_tags,
      ) do |faraday|
        faraday.adapter :persistent_excon,
          tcp_nodelay: true,
          # Publishes instrumentation as
          # `aqueduct.excon.(request|retry|error|response)`
          instrumentor: ActiveSupport::Notifications,
          instrumentor_name: "aqueduct.excon",
          # https://github.com/excon/excon/issues/621
          # Instrumentor is moved to the outermost level so it measures response
          # timings correctly, and further customized to correctly measure
          # response timing.
          middlewares: [
            GitHub::Aqueduct::ExconInstrumentor,
            Excon::Middleware::ResponseParser,
          ]
      end
    end

    def aqueduct_app_name
      if dynamic_lab?
        "github-review-lab"
      else
        "github-#{Rails.env}"
      end
    end

    def aqueduct_client_id
      if GitHub.component == GitHub::Aqueduct::COMPONENT
        "#{Socket.gethostname}:#{Process.pid}"
      elsif GitHub.kube?
        "#{GitHub.kubernetes_cluster_name}:#{Socket.gethostname}"
      else
        "#{GitHub.component}:#{Socket.gethostname}"
      end
    end

    def aqueduct_tags
      host_app = GitHub.server_metadata["app"]
      host_role = GitHub.server_metadata["role"]
      tags = {"app-role" => "#{host_app}-#{host_role}"}
    end

    # Whether or not to raise when an unexpected queue name is encountered when
    # enqueing a job through ActiveJob. This is intended to prevent jobs being
    # queued to queues that aren't being worked by workers. This scenario should
    # ideally be caught in the resque linters, but just in case it's not, this
    # explicit check will give additional guarantees.
    #
    # Raising an exception is disabled in production in order to prevent job
    # loss, but needles are reported as a way to find and eliminate any
    # misconfigured queues.
    def raise_on_unrecognized_queue?
      Rails.test?
    end

    # Whether or not requests are to an environment that supports routing
    # requests to Kubernetes
    def kubernetes_backend_available?
      Rails.production? &&
          !enterprise? &&
          !garage_unicorn?
    end

    attr_accessor :aqueduct_url

    def aqueduct_enabled?
      GitHub.flipper["aqueduct"].enabled?
    end

    def aqueduct_enqueue_for_app_role?(app_role)
      feature_flag = "aqueduct_enqueue_#{app_role.tr('-', '_')}"
      GitHub.flipper[feature_flag].enabled?
    end

  # Determines whether hooks should be delivered via Aqueduct or posted to a Hookshot server via HTTP.
  #
  # Returns false on Enterprise or in development when the
  # DELIVER_HOOKS_VIA_AQUEDUCT environment variable is undefined. Aqueduct is
  # not available on Enterprise, and most development use cases don't require
  # it.
    def deliver_hooks_via_aqueduct?
      if enterprise?
        false
      elsif Rails.env.development? && ENV["DELIVER_HOOKS_VIA_AQUEDUCT"].nil?
        false
      else
        true
      end
    end

    def deliver_hooks_via_resque?
      enterprise? && ENV["ENTERPRISE_ENQUEUE_WEBHOOKS_TO_RESQUE"] == "1"
    end

    def hookshot_go_enabled?
      if enterprise?
        deliver_hooks_via_resque?
      elsif Rails.env.development? && ENV["ENABLE_HOOKSHOT_GO"].nil?
        false
      else
        true
      end
    end

    def enterprise_user_license_list_private_key
      @enterprise_user_license_list_private_key ||= ENV["ENTERPRISE_USER_LICENSE_LIST_PRIVATE_KEY"].to_s
    end
    attr_writer :enterprise_user_license_list_private_key

    def enterprise_user_license_list_public_key
      @enterprise_user_license_list_public_key ||= File.read(Rails.root.join("config/user-license-list.pub"))
    end
    attr_writer :enterprise_user_license_list_public_key

    # Is the IP allow lists feature available?
    #
    # By default, enabled in dotcom mode and disabled in enterprise mode. Can by
    # stealth-enabled in enterprise mode on a GHES appliance using:
    #
    # ghe-config app.github.ip-whitelisting-available 1 && ghe-config-apply
    #
    # Returns Boolean.
    def ip_whitelisting_available?
      return @ip_whitelisting_available if defined?(@ip_whitelisting_available)
      @ip_whitelisting_available = !enterprise?
    end
    attr_writer :ip_whitelisting_available

    # Public: This is the number of days a user is given to accept an invite.
    #
    # Returns Integer.
    def invitation_expiry_period
      7
    end

    # Public: This is the number of days after the invitation expiration date
    # that we want to consider for invitations to appear in "recently expired"
    # invitation scopes such as OrganizationInvitation.recently_expired and
    # BusinessMemberInvitation.recently_expired. This avoids having an unbound
    # filter in the scope, letting us write SQL similar to this instead:
    #
    # created_at BETWEEN invitation_expiry_cutoff.days.ago AND invitation_expiry_period.days.ago
    #
    # Returns Integer.
    def invitation_expiry_cutoff
      invitation_expiry_period + 2
    end

    attr_accessor :qintel_api_key, :qintel_api_secret, :qintel_api_proxy

    # Should FailbotKeyFilter raise when it encounters disallowed keys?
    def raise_on_failbot_filter?
      false
    end

    # Should FailbotKeyTagger convert context keys to sentry tags
    # It should only be enabled in production environment
    def enable_failbot_tags?
      return @enable_failbot_tags if defined?(@enable_failbot_tags)
      @enable_failbot_tags = ENV["GH_ENABLE_FAILBOT_TAGS"].present? || (Rails.env.production? && !GitHub.enterprise?)
    end
    attr_writer :enable_failbot_tags

    # Should Failbot be scrubbed for sensitive data before reporting?
    def enable_failbot_scrubbing?
      true
    end

    # Should FailbotLogger logs execption depend on the environment.
    # It should be only raised in production environment
    def log_failbot_exceptions?
      return @log_failbot_exceptions if defined?(@log_failbot_exceptions)

      @log_failbot_exceptions = ENV["GH_LOG_FAILBOT_EXCEPTIONS"].present? || (Rails.env.production? && !GitHub.enterprise?)
    end
    attr_writer :log_failbot_exceptions

    # Should allow localhost, 127.0.0.1/8 or 0.0.0.0/8 ipaddress range for webhook
    # delivery
    def allow_webhook_loopback_addresses?
      return @allow_webhook_loopback_addresses if defined?(@allow_webhook_loopback_addresses)
      @allow_webhook_loopback_addresses = false
    end
    attr_writer :allow_webhook_loopback_addresses

    # Public: The Stripe Connect dashboard base URL for use with Sponsors.
    #
    # Returns a String.
    def stripe_connect_dashboard_base_url
      return unless GitHub.sponsors_enabled?

      if Rails.production?
        "https://dashboard.stripe.com"
      else
        "https://dashboard.stripe.com/test"
      end
    end

    # The ID for GitHub's Facebook app.
    def facebook_app_id
      1401488693436528
    end

    # The ID for GitHub's iOS app.
    def ios_app_id
      1477376905
    end

    # Determines the execption format for the exeptions
    def failbot_exception_format
      return :haystack if GitHub.enterprise?

      return :structured
    end

    # Only in local dev, drag-and-drop asset upload uses a local alambic URL, e.g.
    # http://alambic.github.localhost/storage/user/7/files/abaea580-9f49-11ea-9b81-d306d6a03db9
    # This flag enables asset uploads to work correctly end-to-end.
    def include_alambic_asset_storage_paths?
      Rails.env.development?
    end

    def bypass_failbot_filter_logic?
      GitHub.enterprise?
    end

    def lazy_load_hydro?
      # Lazy loading should only happen locally since it is certainly
      # needed on CI
      Rails.env.test? && !TestEnv.github_ci?
    end

    def lazy_find_inline_svg_files?
      Rails.development? || Rails.test? && !Rails.configuration.eager_load
    end

    def staffbar_service_info_enabled?
      !GitHub.enterprise?
    end
  end
end

# bring in otherwise untouched modules
require "github/config/datacenter"
require "github/config/prefix_environment"
require "github/config/fastly"
require "github/config/importers"
require "github/config/launch"
require "github/config/legacy_textile_formatting"
require "github/config/dreamlifter"
require "github/config/migration"
require "github/config/noodle"
require "github/config/render"
require "github/config/spokes"
require "github/config/spokesd"
require "github/config/support_link"
require "github/config/logging"
require "github/config/pages"
require "github/config/request_limits"
require "github/config/rate_limits"
require "github/config/opentracing"
require "github/config/hydro"
require "github/config/gitbackupsd"
require "github/config/redis"
require "github/config/group_syncer"
require "github/config/dependabot"
require "github/config/eventer"
require "github/config/twirp"
require "github/config/driftwood"
require "github/config/pull_requests"
require "github/config/s3"
require "github/config/registry"
require "github/config/system_roles"
require "github/config/elasticsearch"
require "github/config/openapi"
require "github/config/splunkhec"
