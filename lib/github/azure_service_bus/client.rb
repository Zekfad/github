# frozen_string_literal: true

module GitHub
  module AzureServiceBus
    class Client
      DEFAULT_TIMEOUT = 60

      def initialize(namespace:, queue_name:, sas_key_name:, sas_key:)
        @host = "#{namespace}.servicebus.windows.net"
        @queue_name = queue_name
        @signer = SharedAccessSigner.new(sas_key_name, sas_key)
      end

      def peek_lock_message(timeout: DEFAULT_TIMEOUT)
        response = call(:post, generate_uri("/#{queue_name}/messages/head", timeout: timeout))
        handle_message_response(response)
      end

      def delete_message(message)
        call(:delete, URI(message.location))
      end

      def read_and_delete_message(timeout: DEFAULT_TIMEOUT)
        response = call(:delete, generate_uri("/#{queue_name}/messages/head", timeout: timeout))
        handle_message_response(response)
      end

      def send_message(message)
        call(:post, generate_uri("/#{queue_name}/messages"), message, { "Content-Type" => "text/plain"})
      end

      def unlock_message(message)
        call(:put, URI(message.location))
      end

      private

      attr_accessor :host, :queue_name, :signer

      def call(method, uri, body = nil, headers = {})
        response = connection.public_send(method, uri) do |request|
          request.body = body if body
          request.headers = headers.merge(
            "Authorization" => signer.authorization_token(request.path)
          )
        end

        raise "Request failed (#{response.status})" if !response.success?
        response
      end

      def connection
        @connection ||= Faraday.new do |builder|
          builder.request :retry, max: 3, methods: [:get, :post, :put, :delete], exceptions: [Faraday::Error]
          builder.adapter Faraday.default_adapter
        end
      end

      def generate_uri(path, query = {})
        URI::HTTPS.build(host: host, path: path, query: query.to_query)
      end

      def handle_message_response(response)
        # If there are still no messages in the queue after `timeout` seconds we get a 204
        if response.status == 204
          nil
        else
          Message.from_response(response)
        end
      end
    end
  end
end
