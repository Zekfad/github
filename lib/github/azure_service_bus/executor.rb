# frozen_string_literal: true

module GitHub
  module AzureServiceBus
    class Executor
      def self.run(**options)
        new(options).run
      end

      def initialize(processor:, consumer:, shutdown_signals: [:INT])
        @processor = processor
        @consumer = consumer
        @shutdown_signals = shutdown_signals
      end

      def run
        puts "Starting up #{processor.class}"

        setup_signal_handlers

        run_processor_loop do
          wait_for_signals
          puts "Shutting down #{processor.class}"
        end

        exit
      end

      private

      attr_reader :processor, :consumer, :shutdown_signals

      def run_processor_loop
        @processor_loop = Thread.new { start }
        @processor_loop.abort_on_exception = true

        yield if block_given?

        shutdown
      end

      def start
        consumer.each_message do |message|
          processor.process(message)
        end
      end

      def shutdown
        consumer.close
        @processor_loop.join
      end

      def setup_signal_handlers
        puts "Waiting for #{shutdown_signal_names.join(', ')} to shutdown"

        @signal_monitor = Thread.new do
          this_thread = Thread.current

          shutdown_signals.each do |signal|
            Signal.trap(signal) do
              @trapped = signal
              this_thread.kill
            end
          end

          sleep
        end
        @signal_monitor.abort_on_exception = true
      end

      def wait_for_signals
        @signal_monitor.join

        puts "Received SIG#{@trapped}"
      end

      def shutdown_signal_names
        shutdown_signals.map { |signal| "SIG#{signal}" }
      end
    end
  end
end
