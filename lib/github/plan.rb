# frozen_string_literal: true

module GitHub
  class Plan
    class Error < StandardError; end

    class UnknownFeatureError < ArgumentError
      def initialize(unknown_feature, allowed_features)
        @unknown_feature = unknown_feature
        @allowed_features = allowed_features
      end

      def message
        "Unknown feature name passed: #{@unknown_feature.inspect}. " \
          "Expecting one of #{@allowed_features.inspect}"
      end
    end

    include Comparable
    include GitHub::Billing::ZuoraDependency

    # For all intents and purposes, we consider this to be the "magic" number to indicate the plan
    # has unlimited private repos.
    UNLIMITED_REPOS   = 9999

    FREE              = "free"
    FREE_WITH_ADDONS  = "free_with_addons"
    ENTERPRISE        = "enterprise"
    BUSINESS          = "business"
    BUSINESS_PLUS     = "business_plus"
    PRO               = "pro"

    # Used in user sign up flow to differentiate user free plan from organization free plan
    TEAM_FREE_PLAN_NAME = "team_free"

    # This is a legacy plan that should never be offered, ever.
    ENGINEYARD        = "engineyard"


    FEATURES = {
      codeowners: "Enables automatic review requests and enforcement based on a CODEOWNERS file.",
      insights: "Enables insights tab for repositories.",
      pages: "Enables Pages site generated from the repository.",
      private_pages: "Enables Pages site generated from the repository to be private.",
      protected_branches: "Enables protected branches.",
      repo_access_export: "Enables the ability to export a list of users with access to a repository in an organization.",
      fine_grained_permissions: "Enables granular permissions beyond read, write, and admin.",
      custom_roles: "Enables granular roles beyond read, write, and admin, with the use of fine grained permissions.",
      repos: "Enables the creation of repositories.",
      wikis: "Enables wikis tab for repositories.",
      draft_prs: "Enables draft pull requests.",
      display_commenter_full_name: "Enables displaying comment author's full name",
      display_verified_domain_emails: "Enables the ability for organization owners to view the verified domain emails of organization members.",
      restrict_notification_delivery: "Enables the ability to restrict email notification delivery to verified domain emails in an organization.",
      audit_log_api: "Enables GraphQL API access to audit log entries.",
      ssh_certificates: "Enables users to authenticate with organization/enterprise managed SSH certificate authorities.",
      ip_whitelisting: "Enables whitelisting of IP addresses for access to resources owned by an enterprise or organization.",
      custom_key_links: "Enables autolink references in the owner's repositories.",
      reminders: "Enables Scheduled and Realtime Reminders for organizations.",
      team_review_requests: "Enables teams to be requested for review on a pull request."
    }.freeze

    LIMITS = {
      collaborators: "The number of collaborators allowed on a repository.",
      repos: "The number of owned repositories allowed.",
      raw_blob_access_expires_in_seconds: "The number in seconds a raw blob will be available.",
      media_blob_max_size: "The maximum size in bytes of an LFS blob.",
      issue_pr_assignees: "The maximum number of users allowed to be assigned to an issue or pull request.",
      manual_review_requests: "The maximum number of users allowed to be requested for review on a pull request.",
    }.freeze

    attr_reader :name, :features, :limits
    attr_writer :receipt_effective_date

    def self.free
      find!("free")
    end

    def self.free_with_addons
      find!("free_with_addons")
    end

    def self.enterprise
      find!("enterprise")
    end

    def self.business
      find!("business")
    end

    def self.business_plus
      find!("business_plus")
    end

    def self.business_plus_252_launch_time
      return @launch_time if @launch_time

      plans_file = plans_file_parsed
      rate_plan = plans_file["plans"].find do |info|
        info["name"] == BUSINESS_PLUS
      end["rate_plans"]["business_plus_252"]
      @launch_time = DateTime.parse(rate_plan["effective_at"]).in_billing_timezone
    end

    def self.pro
      find!("pro")
    end

    # Find a Plan by its name/slug
    #
    # effective_at: (Optional) Date that the user signed up for the plan.
    # Determines which rate they get if a plan has multiple rate_plans.
    # Defaults to today
    #
    # Returns a Plan or nil
    def self.find(name, effective_at: nil)
      target_name = name.to_s.downcase
      plan = self.all.detect { |plan| plan.name.to_s.downcase == target_name }.dup
      plan&.effective_at = effective_at || GitHub::Billing.now
      plan
    end

    # Like find, except we raise if a matching plan isn't found
    def self.find!(name, effective_at: nil)
      find(name, effective_at: effective_at) ||
        raise(Error, "No plan found for #{name}")
    end

    def self.find_org(name)
      org_plans.detect { |plan| plan.name == name }
    end

    def self.find_user(name)
      user_plans.detect { |plan| plan.name == name }
    end

    def self.org_plans
      @org_plans ||= all.select { |p| p.org_plan? }
    end

    def self.default_plan
      find!(GitHub.default_plan_name)
    end

    def self.all_org_plans
      @all_org_plans ||= all.select { |p| (p.org_plan? || p.hidden_org_plan?) && p.name != ENGINEYARD }
    end

    def self.non_free_org_plans
      org_plans.reject { |plan| [FREE, ENTERPRISE].include?(plan.name) }
    end

    def self.all_non_free_org_plans
      all_org_plans.reject { |plan| [FREE, ENTERPRISE].include?(plan.name) }
    end

    def self.user_plans
      @user_plans ||= all.select { |p| p.user_plan? }
    end

    def self.all_user_plans
      @all_user_plans ||= all.
        select { |p| p.user_plan? || p.hidden_user_plan? }.
        reject { |p| %w(pico nano pro).include?(p.name) }
    end

    def self.non_free_user_plans
      user_plans.reject { |plan| [FREE, ENTERPRISE].include?(plan.name) }
    end

    def self.all_non_free_user_plans
      all_user_plans.reject { |plan| [FREE, ENTERPRISE].include?(plan.name) }
    end

    def self.biggest_user_plan
      user_plans.max_by { |plan| plan.cost } or raise GitHub::Billing::Error, "no plans?"
    end

    def self.biggest_org_plan
      org_plans.max_by { |plan| plan.cost } or raise GitHub::Billing::Error, "no plans?"
    end

    def self.names
      all.map { |plan| plan.name }
    end

    def self.per_seat_plans
      all.select { |plan| plan.per_seat? }
    end

    # Public: the names of all free plans
    def self.free_names
      all.select { |plan| plan.free? }.map(&:name)
    end

    def self.name_for_cost(cost)
      if plan = all.reject(&:per_seat?).detect { |plan| plan.cost == cost }
        plan.name
      elsif plan = all.reject(&:per_seat?).detect { |plan| plan.cost == cost/12 }
        "#{plan.name} yearly"
      else
        error = Error.new("no plan matching cost #{cost.inspect}")
        error.set_backtrace(caller)
        Failbot.report(error) rescue nil
        ""
      end
    end

    def self.org_plan_for_discount(discount)
      org_plans.reverse.detect { |plan| !plan.enterprise? && plan.cost <= discount }
    end

    def self.user_plan_for_discount(discount)
      user_plans.reverse.detect { |plan| !plan.enterprise? && plan.cost <= discount }
    end

    # Internal: parse the plans.yml file
    def self.plans_file_parsed
      YAML.load_file(File.join(Rails.root, "config/plans.yml"))
    end

    # Public: all of the available non-enterprise plans.
    #
    # Loads the plans from config/plans.yml and sorts them by cost and name.
    #
    # Returns an Array
    def self.all
      return @all if @all

      plans_file = plans_file_parsed

      @all = plans_file["plans"].map do |info|
        next if info["name"] == ENTERPRISE && !GitHub.enterprise?
        new(info)
      end.compact.sort
    end

    def self.supported_org_plans_for_feature(feature:, visibilities: [:public, :private])
      visibilities.flat_map do |vis|
        all_org_plans.select { |plan| plan.supports?(feature, visibility: vis) }
      end.uniq
    end

    # Internal: initialize a new Plan
    #
    # options - a Hash of data from the plans.yml file.
    #           name       - required name of a plan
    #           orgs       - whether or not the plan is for orgs, default false
    #           cost       - required cost of the plan in dollars
    #           base_units - number of units included in the cost, if applicable
    #           unit_cost  - cost of additional units, if applicable
    #           coupon     - whether or not this plan is for a coupon, default false
    #           hidden     - whether or not the plan should be hidden, default false
    #           legacy     - whether or not the plan is legacy, default false
    #           features   - a list of features accessible to the plan
    #           limits     - a list of features inaccesssible to the plan
    #           github_actions - various options related to GitHub Actions
    #                            included_minutes - the number of minutes included in the plan
    #                            unit_cost        - the cost of additional minutes used
    #           package_registry - options related to GitHub Package Registry
    #                              included_bandwidth_in_gigabytes - GBs of included data transfer
    #           shared_storage - options related to GitHub Shared Storage
    #                             included_megabytes - the number of free megabytes before overages kick in
    #           codespaces - options related to GitHub Codespaces
    #                             included_compute_minutes - Number of compute usage minutes included
    #                             included_storage_gigabytes - Number of storage usage megabytes
    #           feature_flag_overrides - A hash containing overrides of default feature definitions
    #                                    based on enrollment in a given feature flag
    #           pro_badge  - whether or not this plan gives users "pro" badges on their profiles
    def initialize(options)
      @options          = options
      @name             = options.fetch("name")
      @orgs             = options.fetch("orgs", false)
      @coupon           = options.fetch("coupon", false)
      @hidden           = options.fetch("hidden", false)
      @legacy           = options.fetch("legacy", false)
      @features         = options.fetch("features", {})
      @limits           = options.fetch("limits", {})
      @github_actions   = options.fetch("github_actions", {})
      @package_registry = options.fetch("package_registry", {})
      @shared_storage   = options.fetch("shared_storage", {})
      @codespaces       = options.fetch("codespaces", {})
      @rate_plans       = options.fetch("rate_plans", {})
      @org_features     = options.fetch("org_features", {})
      @org_limits       = options.fetch("org_limits", {})
      @pro_badge        = options.fetch("pro_badge", false)
      @feature_flag_overrides = options.fetch("feature_flag_overrides", {})
      nil
    end

    def cost
      fetch_munich_or_default("cost", nil) || (raise KeyError.new(key: "cost"))
    end

    def yearly_cost
      fetch_munich_or_default("yearly_cost", (cost * 12))
    end

    def base_units
      fetch_munich_or_default("base_units", 0)
    end

    def unit_cost
      fetch_munich_or_default("unit_cost", 0) || (raise KeyError.new(key: "unit_cost"))
    end

    def yearly_unit_cost
      effective_plan.fetch("yearly_unit_cost", (unit_cost * 12))
    end

    # TODO remove once munich ships
    private def fetch_munich_or_default(key, default, feature_flag: :munich_pricing)
      original_value = effective_plan.fetch(key, default)
      return original_value if receipt_effective_date < MunichPlan::RELEASE_DATE

      if effective_plan["munich_changes"].present? && GitHub.flipper[feature_flag].enabled?
        effective_plan["munich_changes"].fetch(key, original_value)
      else
        original_value
      end
    end

    # Public: The currently effective rate plan.
    #
    # effective_at: (Optional) The date the user signed up for the plan.
    # Default now.
    #
    # This is the plan whose date is closest to the effective_at without being
    # in the future
    #
    # Returns the effective plan
    def effective_plan
      if @rate_plans.present?
        sorted_dates = @rate_plans.sort_by do |plan, plan_details|
          plan_details["effective_at"]
        end.reverse

        sorted_dates.detect do |plan, plan_details|
          DateTime.parse(plan_details["effective_at"]).in_billing_timezone <=
            effective_at
        end.last
      else
        @options
      end
    end

    private def receipt_effective_date
      @receipt_effective_date || GitHub::Billing.today
    end

    # Public: Date that the user signed up for the plan.
    #
    # Returns nothing
    def effective_at=(effective_at)
      @effective_at = effective_at
    end

    # Public: DateTime the user signed up for the plan
    #
    # Defaults to current time in the Billing Timezone
    def effective_at
      @effective_at || GitHub::Billing.now
    end

    def metered_billing_eligible?
      actions_eligible? || package_registry_eligible? || shared_storage_eligible?
    end

    # Public: Returns whether the plan is eligible for shared storage
    def shared_storage_eligible?
      @shared_storage.present?
    end

    # Public: Returns the number of free megabytes included for the plan
    def shared_storage_included_megabytes
      shared_storage.fetch("included_megabytes", 0)
    end

    private def shared_storage
      fetch_munich_or_default("shared_storage", @shared_storage, feature_flag: :munich_pricing_pro_metered_units)
    end

    # Public: Returns whether the plan is eligible for codespaces
    def codespaces_eligible?
      @codespaces.present?
    end

    # Public: Returns the number of minutes included for Compute usage in codespaces
    def codespaces_included_compute_minutes
      @codespaces.fetch("included_compute_minutes", 0)
    end

    # Public: Returns the number of megabytes storage included for Storage usage in codespaces
    def codespaces_included_storage_gigabytes
      @codespaces.fetch("included_storage_gigabytes", 0)
    end

    # Public: Returns whether the plan is eligible for package registry
    def package_registry_eligible?
      @package_registry.present?
    end

    # Public: Returns the number of gigabytes of data transfer included in the Package Registry for the plan
    def package_registry_included_bandwidth
      package_registry.fetch("included_bandwidth_in_gigabytes", 0)
    end

    private def package_registry
      fetch_munich_or_default("package_registry", @package_registry, feature_flag: :munich_pricing_pro_metered_units)
    end

    # Public Returns whether the plan is eligible for actions
    def actions_eligible?
      @github_actions.present?
    end

    # Public: Returns the number of GitHub Actions minutes included in the plan
    def actions_included_minutes
      @github_actions.fetch("included_minutes", 0)
    end

    def actions_included_private_minutes
      github_actions.fetch("included_private_minutes", 0)
    end

    private def github_actions
      if MunichPlan::ACTIONS_CHANGE_DATE > GitHub::Billing.today
        github_actions_without_munich
      else
        fetch_munich_or_default("github_actions", @github_actions)
      end
    end

    # Internal: Only for MunichPlan consumption
    def github_actions_without_munich
      @github_actions
    end

    # Public: Returns the number of GitHub Actions milliseconds included in the plan
    def actions_included_milliseconds
      actions_included_minutes * 60 * 1000
    end

    # Public: Returns the per-minute unit cost for GitHub Actions in excess of
    # the number of minutes included in the plan
    def actions_overage_unit_cost
      # TODO: this method doesn't really belong here. While Actions V1 pricing was related to the user's plan, V2
      # bases the pricing on the runtime a job was excuted.
      unit_cost = actions_eligible? ? ::Billing::Actions::ZuoraProduct::UNIT_COST : 0
      BigDecimal(unit_cost)
    end

    # Public: Returns the per-minute unit cost in cents for GitHub Actions in excess of
    # the number of minutes included in the plan
    def actions_overage_unit_cost_cents
      # TODO: see actions_overage_unit_cost
      actions_overage_unit_cost * 100
    end

    # Public: Whether or not the current usage is greater than the number of
    # GitHub Actions milliseconds included in the plan
    def over_actions_included_milliseconds?(current_usage:)
      current_usage >= actions_included_milliseconds
    end

    def display_name(account_type = nil)
      if name == FREE_WITH_ADDONS || (account_type == "Organization" && name == "free" && GitHub.flipper[:munich_pricing].enabled?)
        "free"
      elsif pro?
        "pro"
      elsif business?
        "team"
      elsif business_plus?
        "enterprise"
      elsif account_type == "Organization" && name == "free"
        "team for open source"
      else
        name
      end
    end

    def titleized_display_name
      display_name.titleize
    end

    # Display name for Salesforce Business Line.
    def business_line
      if business?
        "GitHub Team"
      elsif business_plus?
        "GitHub Enterprise"
      end
    end

    # Public: Does this plan support a gated feature?
    #
    # Note: Please use `User#plan_supports?` and `Repository#plan_supports?` instead
    # of relying directly on this method.
    #
    # feature      - The feature Symbol to check (e.g. :codeowners)
    # visibility   - (Optional) The visibility scope. :public, :private, or :internal.
    # org          - (Optional) Check against any org-specific overrides.
    # feature_flag - (Optional) Look for any overrides for this feature flag name
    #
    # Returns a Boolean.
    def supports?(feature, visibility: nil, org: false, feature_flag: nil)
      handle_unknown_feature!(feature, FEATURES.keys)

      overrides = feature_overrides(org: org, feature_flag: feature_flag)

      if overrides
        override_setting = find_feature_setting(feature, from: overrides, scope: visibility)
        return !!override_setting unless override_setting.nil?
      end

      !!find_feature_setting(feature, from: features, scope: visibility)
    end

    # Public: Integer limit for gated feature.
    #
    # Note: Please use `User#plan_limit` and `Repository#plan_limit` instead
    # of relying directly on this method.
    #
    # feature      - The feature Symbol to check (e.g. :codeowners)
    # visibility   - (Optional) The visibility scope. :public, :private, or :internal.
    # org          - (Optional) Check against any org-specific overrides.
    # feature_flag - (Optional) Look for any overrides for this feature flag name
    #
    # Returns an Integer.
    def limit(feature, visibility: nil, org: false, feature_flag: nil)
      handle_unknown_feature!(feature, LIMITS.keys)

      overrides = limit_overrides(org: org, feature_flag: feature_flag)

      if overrides
        override_setting = find_feature_setting(feature, from: overrides, scope: visibility)
        return override_setting.to_i unless override_setting.nil?
      end

      find_feature_setting(feature, from: limits, scope: visibility).to_i
    end

    private def feature_overrides(feature_flag: nil, org: false)
      feature_flag_name = feature_flag.to_s

      if feature_flag && @feature_flag_overrides[feature_flag_name]
        if org
          @feature_flag_overrides[feature_flag_name]["org_features"] || @org_features
        else
          @feature_flag_overrides[feature_flag_name]["features"]
        end
      elsif org
        @org_features
      end
    end

    private def limit_overrides(feature_flag: nil, org: false)
      feature_flag_name = feature_flag.to_s

      if feature_flag && @feature_flag_overrides[feature_flag_name]
        if org
          @feature_flag_overrides[feature_flag_name]["org_limits"] || @org_limits
        else
          @feature_flag_overrides[feature_flag_name]["limits"]
        end
      elsif org
        @org_limits
      end
    end

    # Internal: Finds the setting that takes precedence from a given settings group.
    #
    # If scope is provided and a setting exists for that specific feature+scope, it will
    # be returned. Otherwise, the unscoped setting will be returned.
    #
    # Examples:
    #
    #   find_feature_setting(:repos, from: { "repos" => true, "repos.private" => false }, scope: :private)
    #   => false
    #   find_feature_setting(:repos, from: { "repos" => true }, scope: :private)
    #   => true
    #   find_feature_setting(:repos, from: { "repos" => true, "repos.private" => false })
    #   => true
    #   find_feature_setting(:unknown, from: { "repos" => true }, scope: :private)
    #   => nil
    #
    # name      - The setting name Symbol to check (e.g. :codeowners)
    # from      - The settings group hash containing the settings.
    # scope     - (Optional) The scope of the setting (e.g. :public, :private, or :internal).
    #
    # Returns the setting value or nil if no setting exists.
    def find_feature_setting(name, from:, scope: nil)
      candidates = [name.to_s]
      candidates.unshift("#{name}.private") if scope&.to_sym == :internal # Internal repos fall back to 'private' feature scope
      candidates.unshift("#{name}.#{scope}") if scope

      from.values_at(*candidates).compact.first
    end

    def handle_unknown_feature!(feature, supported_features)
      raise UnknownFeatureError.new(feature, supported_features) unless supported_features.include?(feature)
    rescue UnknownFeatureError => e
      if Rails.env.production?
        # Don't raise an error in production, just create a needle.
        Failbot.report(e)
      else
        raise
      end
    end

    # Public: Disk space available in bytes.
    # Note: This is only used for the API and basically meaningless. It used to
    # be the amount of space allotted to users with this plan, in bytes. Now we
    # just show the largest number we had in plans.yml.
    #
    # Returns an Integer
    def space
      999_999_999_999
    end

    # Public: Should this plan be visible to everyone? (High-level plans are hidden)
    #
    # Returns a Boolean
    def hidden?
      @hidden
    end

    # Public: Is this plan given out as part of a coupon?
    #
    # These are used for events such as hackathons, rails rumble, etc.
    #
    # Returns a Boolean
    def coupon?
      @coupon
    end

    # Public: Is this a legacy plan? It shouldn't be used for any new customers.
    #
    # Returns a Boolean
    def legacy?
      @legacy
    end

    # Public: Is this plan for individual users or for organizations?
    #
    # Returns a Boolean
    def orgs?
      @orgs
    end

    # Public: Do users with this plan get a pro badge on their profile?
    #
    # Returns a Boolean
    def pro_badge?
      @pro_badge
    end

    # Public: Compare two plans, by cost and then by name.
    #
    # Returns one of -1, 0, 1
    def <=>(other)
      return nil unless other.is_a?(Plan) || other.is_a?(MunichPlan)

      [cost, @name] <=> [other.cost, other.name]
    end

    # Public: The base cost of this plan in cents
    #
    # Returns an Integer
    def cost_in_cents
      cost * 100
    end

    def yearly_cost_in_cents
      yearly_cost * 100
    end

    def org_plan_or_per_seat?
      !coupon? && orgs? && (per_seat? || !hidden?)
    end

    # Public: Is this plan for organizations only?
    #
    # Returns a Boolean
    def org_plan?
      !coupon? && orgs? && !hidden?
    end

    # Public: Is this plan both hidden and for organizations only?
    #
    # Returns a Boolean
    def hidden_org_plan?
      !coupon? && orgs? && hidden?
    end

    # Public: Does the plan have, effectively, unlimited repositories?
    #
    # Returns a Boolean
    def unlimited?(org: false, feature_flag: nil)
      count = org ? org_repos(feature_flag: feature_flag) : repos(feature_flag: feature_flag)
      count >= UNLIMITED_REPOS
    end

    # Number of private repositories this plans allows for users
    # deprecated: use the limit API instead
    def repos(feature_flag: nil)
      limit(:repos, visibility: :private, feature_flag: feature_flag)
    end

    # Number of private repositories this plan allows for orgs
    def org_repos(feature_flag: nil)
      limit(:repos, visibility: :private, org: true, feature_flag: feature_flag)
    end

    # Public: Is the cost of this plan $0? We ignore plans that have a base
    #         cost of $0.
    #
    # Returns a Boolean
    def free?
      if per_seat? || free_with_addons?
        false
      else
        cost.zero?
      end
    end

    # Public: Is this cost of this plan greater than $0?
    #
    # Returns a Boolean
    def paid?
      !free?
    end

    # Public: Is this the free_with_addons plan.
    #
    # Returns a Boolean
    def free_with_addons?
      name == FREE_WITH_ADDONS
    end

    # Public: Is this the enterprise plan?
    #
    # Returns a Boolean
    def enterprise?
      name == ENTERPRISE
    end

    # Public: Is this one of the per seat plans?
    #
    # Returns a Boolean
    def per_seat?
      name == BUSINESS || name == BUSINESS_PLUS
    end

    # Public: Is this one of the per repository plans?
    #
    # Returns a Boolean
    def per_repository?
      !per_seat? && !free_with_addons? && !free? && !pro?
    end

    # Public: Is this the business plan?
    #
    # Returns a Boolean
    def business?
      name == BUSINESS
    end

    def business_plus?
      name == BUSINESS_PLUS
    end

    # Public: Is this the Pro plan?
    #
    # Returns a Boolean
    def pro?
      name == PRO
    end

    # Public: Is this plan only for Users
    #
    # Returns a Boolean
    def user_plan?
      @name == FREE || (!coupon? && !hidden? && !orgs?)
    end

    # Public: Is this plan only for Organizations
    #
    # Returns a Boolean
    def hidden_user_plan?
      @name == FREE || @name == FREE_WITH_ADDONS || (!coupon? && hidden? && !orgs?)
    end

    # Public: Can this plan be upgraded for more repositories?
    #
    # Returns a Boolean
    def upgradeable?
      if orgs?
        org_repos < Plan.biggest_org_plan.org_repos
      else
        repos < Plan.biggest_user_plan.repos
      end
    end

    # Public: The cost of each additional unit in cents
    #
    # Returns an Integer
    def unit_cost_in_cents
      unit_cost * 100
    end

    def yearly_unit_cost_in_cents
      yearly_unit_cost * 100
    end

    def to_s
      @name
    end

    # legacy plans needed for test:
    def self.micro
      find!("micro")
    end

    def self.bronze
      find!("bronze")
    end

    def self.silver
      find!("silver")
    end

    def self.gold
      find!("gold")
    end

    def self.small
      find!("small")
    end

    def self.medium
      find!("medium")
    end

    def self.large
      find!("large")
    end

    def self.curium
      find!("curium")
    end

    def self.platinum
      find!("platinum")
    end

    def self.diamond
      find!("diamond")
    end

    def self.aluminium
      find!("aluminium")
    end
  end
end
