# frozen_string_literal: true

# Middleware that records the last rack env hash so that the introspect trap can
# report it to failbot.
module GitHub
  class RecordRackEnvMiddleware
    class << self
      attr_accessor :last_env
    end

    def initialize(app)
      @app = app
    end

    def call(env)
      self.class.last_env = env
      @app.call(env)
    end
  end
end
