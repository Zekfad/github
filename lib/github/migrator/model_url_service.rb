# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class ModelUrlService
      include AreasOfResponsibility

      areas_of_responsibility :migration

      class NotImplemented < MigrationError
        include ::GitHub::Migrator::MapWarning
      end

      def initialize(options = nil)
        @options = options || {}
        @cache   = @options[:cache] || Cache.null
      end

      # Public: Get a URL that represents the given model.
      #
      #   url_for_model(Repository.last)
      #   => "https://github.com/github/github"
      #   url_for_model(IssueComment.last)
      #   => "https://github.com/github/github/issues/123456#issuecomment-9876543"
      def url_for_model(model, use_cache: true)
        return unless model
        return uncached_internal_url_for_model(model) unless use_cache

        cache.getset("url_for_model:#{model.class.model_name.singular}:#{model.id}") do
          internal_url_for_model(model)
        end
      end

      # Public: Get a URL that represents the associated model.
      #
      #   url_for_association(Repository.last, :owner)
      #   => "https://github.com/github"
      def url_for_association(model, association_name)
        association = model.association(association_name)
        reflection = association.reflection
        cached_model_url(model_class: association.klass.name, model_id: model[reflection.foreign_key]) do
          if target = Platform::Loaders::ActiveRecordAssociation.load(model, reflection.name).sync
            uncached_internal_url_for_model(target)
          end
        end
      end

      # Public: Find a model given its URL.
      #
      #   model_for_url("https://github.com/github/github")
      #   => #<Repository name: "github", ...>
      def model_for_url(url)
        return unless url

        cache.getset("model_for_url:#{url}") do
          internal_model_for_url(url)
        end
      end

      # Public: Determine whether a url is valid or not.
      #
      #   url_valid?("https://github.com/mattr-")
      #   => false
      #   url_valid?("https://github.com/mattr")
      #   => true
      def url_valid?(url)
        ModelForUrl.new(url, options).valid?
      end

      private

      attr_reader :cache, :options

      def internal_url_for_model(model)
        return unless model

        cached_model_url(model_class: model.class.name, model_id: model.id) do
          uncached_internal_url_for_model(model)
        end
      end

      def uncached_internal_url_for_model(model)
        case model
        when Repository
          File.join(url_for_association(model, :owner), model.name)
        when ProtectedBranch
          File.join(url_for_association(model, :repository), "settings", "branches", UrlHelper.escape_path(model.name))
        when Mannequin
          File.join(GitHub.url, model.source_login)
        when User
          File.join(GitHub.url, model.login)
        when Label
          File.join(url_for_association(model, :repository), "labels", UrlHelper.escape_path(model.name))
        when Milestone
          File.join(url_for_association(model, :repository), "milestones", UrlHelper.escape_path(model.title))
        when Project
          File.join(url_for_association(model, :owner), "projects", model.number.to_s)
        when Issue
          url_for_association(model, :pull_request) ||
            File.join(url_for_association(model, :repository), "issues", model.number.to_s)
        when PullRequest
          File.join(url_for_association(model, :repository), "pull", model.number.to_s)
        when IssueComment
          url_for_association(model, :issue) + "#issuecomment-#{model.id}"
        when PullRequestReview
          url_for_association(model, :pull_request) + "/files#pullrequestreview-#{model.id}"
        when PullRequestReviewComment
          url_for_association(model, :pull_request) + "/files#r#{model.id}"
        when CommitComment
          File.join(url_for_association(model, :repository), "commit", model.commit_id) + "#commitcomment-#{model.id}"
        when IssueEvent
          url_for_association(model, :issue) + "#event-#{model.id}"
        when Team
          org_url = url_for_association(model, :organization)
          (org_url.sub(%r{/[^/]+\z}) { |name| "/orgs#{name}" }) + "/teams/" + UrlHelper.escape_path(model.slug)
        when Release
          File.join(url_for_association(model, :repository), "releases", "tag", model.tag_name)
        when Attachment
          model.asset.storage_external_url
        when RepositoryFile
          model.storage_external_url
        else
          raise NotImplemented.new("not implemented for #{model.class.try(:model_name) || model.class.name}")
        end
      end

      def cached_model_url(model_class:, model_id:)
        cache.getset("cached_model_url:#{model_class}:#{model_id}") do
          yield
        end
      end

      def internal_model_for_url(url)
        ModelForUrl.new(url, options).model
      end
    end

    class ModelForUrl
      class NotImplemented < StandardError; end

      def initialize(url, options = nil)
        @url = url
        options = options || {}
        @cache = options[:cache] || Cache.null
      end

      # Public: The url passed in at initialization.
      #
      # Returns a String.
      attr_reader :url

      attr_reader :cache

      # Public: Get the model represented by the url.
      #
      # Returns an instance of Repository, User, Organization...
      def model
        return team if team_url?
        return user if user_url?
        return repo if repo_url?
        return project if project_url?
        return comment_or_event if comment_or_event_url?
        return issue_pull_label_or_milestone if issue_pull_label_or_milestone_url?
        return release if release_url?

        raise NotImplemented.new("not implemented for #{scrub_url(url)}")
      end

      def valid?
        return true if team_url?
        return valid_user_url? if user_url?
        return true if repo_url?
        return true if comment_or_event_url?
        return true if issue_pull_label_or_milestone_url?
        return true if release_url?
        return true if project_url?
        false
      end

      private

      def uri
        @uri ||= URI.parse(url)
      end

      def parts
        @parts ||= Array(uri.path.split("/")[1..-1])
      end

      def anchor
        @anchor ||= uri.fragment
      end

      def team_url?
        parts[0] == "orgs" && parts[2] == "teams"
      end

      def organization
        @organization ||=
          cache.getset("model_for_url:organization:#{parts[0]}") do
            Organization.find_by_login(parts[0])
          end
      end

      def team_organization
        @organization ||=
          cache.getset("model_for_url:organization:#{parts[1]}") do
            Organization.find_by_login(parts[1])
          end
      end

      def team
        return unless team_organization.present?

        @team ||=
          cache.getset("model_for_url:organization:#{parts[1]}:team:#{parts[3]}") do
            team_organization.teams.find_by_slug(parts[3])
          end
      end

      def user_url?
        parts.length == 1
      end

      def user
        @user ||=
          cache.getset("model_for_url:user:#{parts[0]}") do
            User.find_by_login(parts[0])
          end
      end

      def repo_url?
        parts.length == 2
      end

      def repo
        return unless user.present?

        @repo ||=
          cache.getset("model_for_url:owner:#{parts[0]}:repo:#{parts[1]}") do
            user.find_repo_by_name(parts[1])
          end
      end

      def project
        return unless owner = repo || organization

        owner.projects.where(number: parts.last).first
      end

      def comment_or_event_url?
        anchor.present?
      end

      def comment_or_event
        return unless repo.present?

        @comment_or_event ||= begin
          case
          when match = /\Aissuecomment-(\d+)\z/.match(anchor)
            IssueComment.find(match[1])
          when match = /\Ar(\d+)\z/.match(anchor)
            PullRequestReviewComment.find(match[1])
          when match = /\Acommitcomment-(\d+)\z/.match(anchor)
            CommitComment.find(match[1])
          when match = /\Aevent-(\d+)\z/.match(anchor)
            IssueEvent.find(match[1])
          end
        end
      end

      def issue_pull_label_or_milestone_url?
        parts.length > 3 &&
          ["issues", "pull", "labels", "milestones"].include?(parts[2])
      end

      def project_url?
        parts.length >= 3 &&
          parts.last(2)[0] == "projects"
      end

      def issue_pull_label_or_milestone
        return unless repo.present?

        @issue_pull_label_or_milestone ||= begin
          case parts[2]
          when "issues"
            repo.issues.find_by_number(parts[3])
          when "pull"
            issue = repo.issues.find_by_number(parts[3])
            issue.pull_request
          when "labels"
            repo.labels.find_by_name(parts[3])
          when "milestones"
            repo.milestones.find_by_title(URI.decode_www_form_component(parts[3..-1].join("/")))
          end
        end
      end

      def release_url?
        parts.length == 5 && parts[2] == "releases" && parts[3] == "tag"
      end

      def release
        return unless repo.present?

        repo.releases.find_by_tag_name(parts[4])
      end

      def valid_user_url?
        valid_login? && !blacklisted_login?
      end

      def blacklisted_login?
        User.new(login: parts[0]).login_blacklisted?
      end

      def valid_login?
        !!(parts[0] =~ User::LOGIN_REGEX)
      end

      def scrub_url(url)
        url.gsub(/\d/, "0").gsub(/[a-zA-Z]/, "X")
      end
    end
  end
end
