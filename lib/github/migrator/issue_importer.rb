# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class IssueImporter < GitHub::Migrator::Importer

      def import(attributes, options = {})
        repo = model_from_source_url!(attributes["repository"], attributes)

        issue = Issue.new do |issue|
          issue.repository = repo
          issue.user = user_or_fallback(attributes["user"])
          issue.number = last_part_of_url(attributes["url"]).to_i
          issue.pull_request_id = attributes["pull_request_id"]
          issue.title = attributes["title"]
          issue.body = attributes["body"]
          issue.assignee = model_from_source_url(attributes["assignee"])
          issue.milestone = model_from_source_url!(attributes["milestone"], attributes)
          issue.created_at = attributes["created_at"]
          issue.updated_at = attributes["updated_at"] || attributes["created_at"]
          if attributes["closed_at"]
            issue.closed_at = attributes["closed_at"]
            issue.state = "closed"
          end
        end

        import_model(issue).tap do |imported_issue|
          assignees(attributes).each do |assignee|
            EmptyTriggerAssignment.create(
              assignee: assignee,
              issue: imported_issue,
            )
          end
        end
      end

      # Skip creating assignment events when setting issue assignees. This is
      # because the events are imported later, which results in duplicates.
      class EmptyTriggerAssignment < ::Assignment
        def trigger_assigned_event; end
      end

      private

      def assignees(attributes)
        assignee_urls(attributes).map do |assignee_url|
          model_from_source_url!(assignee_url, attributes)
        end.compact
      end

      def assignee_urls(attributes)
        [
          attributes["assignee"],
          attributes["assignees"],
        ].flatten.uniq.compact
      end
    end
  end
end
