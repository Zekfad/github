# rubocop:disable Style/FrozenStringLiteralComment
module GitHub
  class Migrator
    class MigrationFileUploader
      include AreasOfResponsibility

      areas_of_responsibility :migration

      attr_reader :migration_file, :archive_path, :storage_policy

      def initialize(migration_file:, archive_path:, storage_policy:)
        @migration_file = migration_file
        @archive_path = archive_path
        @storage_policy = storage_policy
      end

      def call
        return upload_s3 if should_upload_s3?
        upload_local
      end

      private

      def should_upload_s3?
        storage_policy.class == ::Storage::S3Policy
      end

      def upload_local
        File.open(archive_path, "rb") do |tarball|
          storage_policy.upload_contents!(tarball)
        end
        true
      end

      def upload_s3
        s3_key = migration_file.storage_s3_key(storage_policy)
        s3_object = aws_s3_object(s3_key)

        begin
          s3_object.upload_file(archive_path,
            multipart_threshold: max_archive_size,
            acl: acl,
          )
        rescue Aws::S3::MultipartUploadError => error
          raise GitHub::Migrator::MultipartUploadError, "Error during multipart upload for "\
            "#{migration_file.class} ##{migration_file.id}: #{error.message}"
        end
      end

      def aws_s3_resource
        Aws::S3::Resource.new(client: migration_file.storage_client)
      end

      def aws_s3_bucket
        aws_s3_resource.bucket(migration_file.storage_s3_bucket)
      end

      def aws_s3_object(key)
        aws_s3_bucket.object(key)
      end

      def acl
        storage_policy.acl
      end

      def max_archive_size
        MigrationFile::MAX_ASSET_SIZE
      end
    end
  end
end
