# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/import"

module GitHub
  class Migrator
    class RepositoryImporter < GitHub::Migrator::Importer
      include RepoHelpers
      include GitHub::Rsync

      # List of additional rsync command options to use during import
      RSYNC_IMPORT_OPTIONS = %w(--whole-file).freeze

      # Public: Used in GitHub::Migrator to raise warnings immediately if the
      # repository has a wiki and the migration schema version is 1.0.0 where
      # the wiki git data wasn't archived correctly.
      #
      # attributes - Hash of attributes for the repository.
      # options    - Hash with `schema_version`.
      #
      # Returns an Array.
      def self.warnings_for(attributes, options = {})
        schema_version = options.fetch(:schema_version)
        if schema_version == "1.0.0" && attributes["wiki_url"].present?
          ["Wiki for #{attributes["url"]} has the wrong content. Use a newer version of gh-migrator to export a new archive."]
        else
          []
        end
      end

      # Public: Imports repository with the following procedure:
      #
      # 1. Create the Repository record and empty bare repo.
      # 2. Build array of importable objects (repo & wiki if present).
      # 3. Migrate svn mappings for each import object if needed.
      # 4. Import each importable object to the appropriate fileservers.
      # 5. Finish preparing the repo by clearing ref cache, setting the default
      #    branch, and locking the repo so that it can be audited before use.
      #
      # attributes - Hash of attributes for the repository.
      # options    - Hash with `target_url` if repository is being renamed.
      #
      # Returns a Repository.
      def import(attributes, options = {})
        unless import_allowed?
          raise "Imports to github.com are not allowed."
        end

        repo = Repository.new do |repo|
          repo.owner              = model_from_source_url!(attributes["owner"], attributes)
          repo.name               = last_part_of_url(options[:target_url]) || attributes["name"]
          repo.description        = attributes["description"]
          repo.public             = import_as_public?(attributes)
          repo.homepage           = attributes["website"]
          repo.has_issues         = attributes["has_issues"]
          repo.has_wiki           = attributes["has_wiki"]
          repo.has_downloads      = attributes["has_downloads"]
          repo.labels             = []
          repo.created_at         = attributes["created_at"]
          repo.created_by_user_id = @actor.id if @actor
        end

        begin
          repo.save!
        rescue ActiveRecord::RecordInvalid => error
          error.message.gsub!(
            "Name already exists on this account",
            "Name (#{repo.name}) already exists on this account"
          )

          raise
        end

        if GitHub.anonymous_git_access_enabled? && attributes["anonymous_access_enabled"]
          repo.enable_anonymous_git_access(@actor)
        end

        Array(attributes["labels"]).each do |label_attributes|
          new_label = repo.labels.create(
            name:       label_attributes["name"],
            color:      label_attributes["color"],
            created_at: label_attributes["created_at"],
          )
          new_label.instrument_creation(context: "repository import") if new_label.persisted?
        end

        Array(attributes["collaborators"]).each do |collaborator|
          user = model_from_source_url(collaborator["user"])
          permission = collaborator["permission"]

          if user && permission
            repo.add_member(user, action: permission.to_sym)
          end
        end

        Array(attributes["webhooks"]).each do |webhook|
          payload_url             = webhook["payload_url"]
          content_type            = webhook["content_type"]
          event_types             = webhook["event_types"]
          enable_ssl_verification = webhook["enable_ssl_verification"]
          active                  = webhook["active"]

          if payload_url && content_type && event_types && active
            repo.hooks.create(
              name:   "web",
              active: active,
              config: {
                "url"          => payload_url,
                "insecure_ssl" => enable_ssl_verification ? "0" : "1",
                "content_type" => content_type,
              },
              events: event_types,
            )
          end
        end

        Array(attributes["public_keys"]).each do |public_key|
          title       = public_key["title"]
          key         = public_key["key"]
          read_only   = public_key["read_only"]
          fingerprint = public_key["fingerprint"]
          created_at  = public_key["created_at"]

          if title && key && fingerprint && created_at
            begin
              repo.public_keys.create!(
                title:      title,
                key:        key,
                read_only:  read_only,
                created_at: created_at,
              )
            rescue ActiveRecord::RecordInvalid => exception
              raise($!) unless exception.record.errors.messages == { key: ["is already in use"] }

              GitHub::Logger.log(
                fn:         "GitHub::Migrator::RepositoryImporter#import",
                class:      exception.class.name,
                model_name: "repository",
                source_url: attributes["url"],
                resolution: "Skipped public key with fingerprint #{fingerprint} (already in use)",
              )
            end
          end
        end

        begin
          # Build array of git repos to import (the repository and maybe its wiki).
          imports = []
          imports << ImportableRepository.new(repo, path_to_bare_repo(attributes["git_url"]), attributes["url"])

          # Only do wiki things if wiki is present in migration archive.
          if wiki_url = attributes["wiki_url"]
            # Prepare the UnsulliedWiki object and RepositoryWiki record.
            repo.unsullied_wiki.setup_git_repository
            RepositoryWiki.create(repository: repo)
            repo.reload

            imports << ImportableWiki.new(repo, path_to_bare_repo(wiki_url), attributes["url"])
          end

          # Migrate svn mappings before importing data to file servers.
          migrate_svn_mappings(imports)

          # Import repository and wiki git data to the appropriate fileservers.
          import_to_dgit_fileservers repo, imports

          # Setting default branch doesn't work without clearing the ref cache
          # first and this seems like a healthy thing to do anyways after rsyncing
          # the git data.
          repo.clear_ref_cache

          # Set the default branch if the serialized repository data has it.
          if default_branch = attributes["default_branch"]
            repo.update_default_branch(default_branch)
          end

          repo.lock_for_migration
          repo.update_disk_usage
          repo
        rescue SvnMappingError, DGitImportError => error
          # Attempt to cleanup failed repository import so that rerunning import
          # for same migration can pick back up at the failed repository and try
          # again rather than failing with class="ActiveRecord::RecordInvalid"
          # message="Validation failed: Name already exists on this account" error.
          repo.destroy

          raise error
        end
      end

      attr_writer :dgit_logger

      private

      # Internal: Logger for dgit import logging.
      def dgit_logger
        @dgit_logger ||= Rails.logger
      end

      # Internal: Determine public/private settings for repository import.
      # If we're importing into Enterprise, read in the setting from archive.
      # If we're importing into GitHub.com, ALWAYS import as private
      def import_as_public?(attributes)
        return false unless GitHub.enterprise?
        !attributes["private"]
      end

      # Internal: Migrates svn mapping to >= v4 which works on dgit and legacy fs.
      #
      # imports - Array of importable objects.
      def migrate_svn_mappings(imports)
        imports.each do |info|
          next unless File.exist?("#{info.source_path}/svn.history.msgpack")

          Dir.chdir(info.source_path) do
            Rails.logger.info "Migrating svn mapping for #{info.source_path}"
            child = POSIX::Spawn::Child.new(Rails.root.join("script/slumlord-env").to_s, "script/migrate-mapping", "--in-place", ".")

            unless child.success?
              raise SvnMappingError.new("mapping migration failed for #{info.source_path}\nOUT:\n#{child.out}\nERR:\n#{child.err}")
            end
          end
        end
      end

      # Internal: Rsync importable objects to non-dgit fileserver.
      #
      # imports - Array with ImportableRepository and ImportableWiki if it exists.
      def rsync_to_fileserver(imports)
        imports.each do |import|
          target = import.record

          rsync \
            from: import.source_path,
            to: local_route?(target) ? target.shard_path : "git@#{target.resolved_host}:#{target.shard_path}",
            exclude: RSYNC_IMPORT_EXCLUDES
        end
      end

      # Internal: Imports importable objects to dgit fileservers with the
      # following procedure:
      #
      # 1. Prepare destination directories, usually the network shard path with
      #    the ".premove" suffix and then prepopulate those directories with
      #    the auto initialized git data.
      # 2. Take the repository offline while it is being updated.
      # 3. Rsync importable data from migration staging area to destinations.
      # 4. Checksum the network across dgit fileservers.
      # 5. Initialize dgit for the network.
      # 6. Move original network out of the way and then move the premove
      #    destination directories to the original network paths.
      # 7. Put the repository back online.
      #
      # repo    - Repository that importable objects are associated with.
      # imports - Array with ImportableRepository and ImportableWiki if it exists.
      def import_to_dgit_fileservers(repo, imports)
        routes = repo.dgit_all_routes

        network_id = repo.network_id
        network_shard_path = GitHub::Routing.nw_storage_path(network_id: network_id).chomp("/")
        premove = ".premove" unless Rails.development? || GitHub.enterprise?

        # prepare rsync target directories
        if premove.present?
          kids = GitHub::DGit::Import::parallelize(dgit_logger, routes) do |route|
            source = source_path(network_shard_path, best_host(route))

            premove_dest = "#{source}#{premove}"

            dgit_logger.info "Copying repo to #{premove_dest}"
            if local_route?(route)
              if !File.exist?(source)
                raise DGitImportError.new("source directory does not exist")
              end

              if File.exist?(premove_dest)
                raise DGitImportError.new("premove directory already exists")
              end

              # copy the (empty) repository network to the .premove location
              ["cp -R #{source} #{premove_dest}"]
            else
              "ssh #{route.resolved_host} gh_migrator_prepare_network_premove #{source}"
            end
          end
          if kids.any? { |host, kid| kid.status.exitstatus != 0 }
            raise DGitImportError.new("premove preparation failure\n#{error_message_from_kids(kids)}")
          end
        end

        # Offline the repository while we move bits to it.
        GitHub::DGit::Import::set_moving(dgit_logger, network_id, 1)

        # Rsync importable data from migration staging area to destinations.
        imports.each do |info|
          target = info.record

          kids = GitHub::DGit::Import::parallelize(dgit_logger, info.destination_routes) do |route|
            dest = "#{source_path(network_shard_path, best_host(route))}#{premove}"

            rsync_command \
              from: info.source_path,
              to: local_route?(route) ? info.destination_path(dest) : "git@#{route.resolved_host}:#{info.destination_path(dest)}",
              options: RSYNC_IMPORT_OPTIONS,
              exclude: RSYNC_IMPORT_EXCLUDES
          end
          if kids.any? { |host, kid| kid.status.exitstatus != 0 }
            GitHub::DGit::Import::set_moving(dgit_logger, network_id, 0)
            raise DGitImportError.new("failed to rsync git data\n#{error_message_from_kids(kids)}")
          end
        end

        # Create `dgit-state` checksum files and capture their contents
        checksums = GitHub::DGit::Import::git_dgit_state_init \
          dgit_logger, "localhost", routes.map(&method(:best_host)), GitHub::DGit::RepoType::REPO,
          network_id, network_shard_path

        if !checksums || !checksums[GitHub::DGit::RepoType::REPO] || !checksums[GitHub::DGit::RepoType::WIKI]
          GitHub::DGit::Import::set_moving(dgit_logger, network_id, 0)
          raise DGitImportError.new("DGit state and checksum initialization failure")
        end

        if premove.present?

          kids = GitHub::DGit::Import::parallelize(dgit_logger, routes) do |route|
            dest = source_path(network_shard_path, best_host(route))

            command = local_route?(route) ? [] : ["ssh", route.resolved_host]
            command + ["mv", dest, "#{dest}.moved"]
          end
          if kids.any? { |host, kid| kid.status.exitstatus != 0 }
            GitHub::DGit::Import::set_moving(dgit_logger, network_id, 0)
            raise DGitImportError.new("network pre-migration move failure\n#{error_message_from_kids(kids)}")
          end

          # Move "<networkid>.premove" to "<networkid>"
          kids = GitHub::DGit::Import::parallelize(dgit_logger, routes) do |route|
            dest = source_path(network_shard_path, best_host(route))

            if local_route?(route)
              ["mv", "#{dest}#{premove}", dest]
            else
              ["ssh", route.resolved_host, "finalise_network_move2", dest]
            end
          end
          if kids.any? { |host, kid| kid.status.exitstatus != 0 }
            GitHub::DGit::Import::set_moving(dgit_logger, network_id, 0)
            raise DGitImportError.new("finalize network move failure\n#{error_message_from_kids(kids)}")
          end
        end

        # set moving=0
        GitHub::DGit::Import::set_moving(dgit_logger, repo.network_id, 0)

        # Clean up .moved folders
        non_local_routes = routes.reject(&method(:local_route?))
        kids = GitHub::DGit::Import::parallelize(dgit_logger, non_local_routes) do |route|
          ["ssh", route.resolved_host, "clean_network", network_id.to_s]
        end
        if kids.any? { |host, kid| kid.status.exitstatus != 0 }
          dgit_logger.info("clean network failure\n#{error_message_from_kids(kids)}")
        end

        # Disable debug checksum logging
        GitHub.dgit_threepc_debug_enabled = false

        # Recompute checksums
        imports.each { |i| i.record.recompute_checksums(:vote) }
      ensure
        # Re-enable debug checksum logging
        GitHub.dgit_threepc_debug_enabled = true
      end

      # Internal: Convert repository or wiki git data url into the full path to
      # the git data on disk.
      #
      # tarball_url - String.
      #
      # Returns a String.
      def path_to_bare_repo(tarball_url)
        # Parses 'tarball://root/repositories/acme/widgets.git' and returns
        # 'repositories/acme/widgets.git'.
        path_in_archive = URI.parse(tarball_url).path[1..-1]

        File.join(migration_path, path_in_archive)
      end

      # Internal: Collect err and out from kids and compile into string.
      #
      # kids - Array of BackgroundChild instances.
      #
      # Returns a String.
      def error_message_from_kids(kids)
        kids.inject([]) { |result, (host, kid)| result << "CMD:\n#{kid.cmd}\nOUT:\n#{kid.out}\nERR:\n#{kid.err}" }.join("\n")
      end

      # Internal: Return network source path appropriate for current environment.
      #
      # network_shard_path - String path.
      # host               - String host where path should exist.
      #
      # Returns a String.
      def source_path(network_shard_path, host)
        if Rails.production?
          network_shard_path
        else
          GitHub::DGit::dev_route(network_shard_path, host)
        end
      end

      # Internal: Determines if the provided route is local
      #
      # route - The route to check against
      #
      # Returns a boolean
      def local_route?(route)
        route.host == GitHub.local_git_host_name
      end

      def best_host(route)
        if local_route?(route)
          route.original_host
        else
          route.resolved_host
        end
      end

      # Internal: List of rsync exclusions.
      #
      # Push (nearly) everything into the new repository.
      # * Exclude 'hooks/' and 'config' so that we keep the target's settings.
      # * Exclude 'info/' and 'description' because they probably have wrong info for the new instance.
      RSYNC_IMPORT_EXCLUDES = %w(/hooks /config /info /description).map(&:freeze).freeze

      class ImportableRepository
        def initialize(repo, source_path, source_url)
          @repo        = repo
          @source_path = source_path
          @source_url  = source_url
        end

        attr_reader :repo, :source_path, :source_url

        def record
          repo
        end

        def destination_routes
          repo.dgit_all_routes
        end

        def destination_path(dest)
          "#{dest}/#{repo.id}.git"
        end
      end

      class ImportableWiki
        def initialize(repo, source_path, source_url)
          @repo        = repo
          @source_path = source_path
          @source_url  = source_url
        end

        attr_reader :repo, :source_path, :source_url

        def record
          repo.unsullied_wiki
        end

        def destination_routes
          repo.dgit_wiki_write_routes # this will raise GitHub::DGit::UnroutedError if no wiki routes are found
        end

        def destination_path(dest)
          "#{dest}/#{repo.id}.wiki.git"
        end
      end
    end
  end
end
