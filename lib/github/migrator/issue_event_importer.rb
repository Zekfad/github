# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class IssueEventImporter < GitHub::Migrator::Importer

      def import(attributes, options = {})
        issue_event = IssueEvent.new do |issue_event|
          issue_event.issue = \
            model_from_source_url!(attributes["issue"], attributes) ||
            model_from_source_url!(attributes["pull_request"], attributes).try(:issue)

          issue_event.actor = user_or_fallback(attributes["actor"])
          issue_event.event = attributes["event"]
          issue_event.created_at = attributes["created_at"]

          if label_name = last_part_of_url(attributes["label"])
            if label = issue_event.issue.repository.labels.where(name: label_name).first
              issue_event.label_id = label.id
            end
          end

          ["label_name", "label_color", "label_text_color",
           "milestone_title", "title_was", "title_is", "deployment_id",
           "ref", "before_commit_oid", "after_commit_oid"].each do |key|

             if value = attributes[key]
               issue_event.send("#{key}=", value)
             end
          end

          issue_event.column_name = attributes["column_name"]
          issue_event.previous_column_name = attributes["previous_column_name"]

          if attributes["subject"]
            issue_event.subject = model_from_source_url!(attributes["subject"], attributes)
          end

          if referencing_issue = attributes["referencing_issue"]
            issue_event.referencing_issue = model_from_source_url!(referencing_issue, attributes)
          end

          if referencing_pull_request = attributes["referencing_pull_request"]
            issue_event.referencing_issue = model_from_source_url!(referencing_pull_request, attributes).issue
          end

          if commit_id = attributes["commit_id"]
            issue_event.commit_id = commit_id
          end

          if commit_repository = attributes["commit_repository"]
            issue_event.commit_repository = model_from_source_url!(commit_repository, attributes)
          end
        end

        imported_issue_event = import_model(issue_event)
        issue_event.issue_event_detail.issue_event_id = imported_issue_event.id
        import_model(issue_event.issue_event_detail)

        imported_issue_event.reload
      rescue GitHub::Migrator::AssociationFailed => error
        GitHub::Logger.log({
          fn: "GitHub::Migrator::IssueEventImporter#import",
          class: error.class.name,
          model_name: "issue_event",
          source_url: attributes["url"],
          resolution: "Skipped import",
        })

        return nil
      end
    end
  end
end
