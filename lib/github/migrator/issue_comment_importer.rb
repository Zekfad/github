# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class IssueCommentImporter < GitHub::Migrator::Importer

      def import(attributes, options = {})
        comment = IssueComment.new do |issue_comment|
          issue = \
            model_from_source_url!(attributes["issue"], attributes) ||
            model_from_source_url!(attributes["pull_request"], attributes).try(:issue)
          issue_comment.issue = issue
          issue_comment.repository = issue.repository
          issue_comment.user = user_or_fallback(attributes["user"])
          issue_comment.body = attributes["body"]
          issue_comment.formatter = attributes["formatter"]
          issue_comment.created_at = attributes["created_at"]
        end

        import_model(comment)
      end
    end
  end
end
