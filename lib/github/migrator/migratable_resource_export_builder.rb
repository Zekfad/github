# rubocop:disable Style/FrozenStringLiteralComment

require_relative "batch_service"

module GitHub
  class Migrator
    class MigratableResourceExportBuilder
      include AreasOfResponsibility

      areas_of_responsibility :migration

      BATCH_SIZE = 50

      def initialize(guid:, model_url_service:, progress: lambda {})
        @guid              = guid
        @model_url_service = model_url_service
        @progress          = progress
      end

      attr_reader :guid, :model_url_service, :progress

      def add(model)
        case model
        when Hash
          user_id = model.fetch(:user_id)
          user_ids.add(user_id)
        else
          batch_insert_migratable_resources.add(model)
        end
      end

      def complete
        user_ids.each_slice(BATCH_SIZE) do |ids|
          User.where(id: ids).each do |user|
            batch_insert_migratable_resources.add(user)
          end
        end

        batch_insert_migratable_resources.complete
      end

      def add_now(model)
        add(model)
        batch_insert_migratable_resources.complete
      end

      private

      delegate :url_for_model, to: :model_url_service

      def user_ids
        @user_ids ||= Set.new
      end

      def batch_insert_migratable_resources
        @batch_insert_migratable_resources ||= BatchService.new(retry_on: ActiveRecord::Deadlocked) do |models|
          values = []
          state = MigratableResource.states[:export]

          models.each_with_index do |model, i|
            model_type = model.class.model_name.singular
            model_id   = model.id
            source_url = url_for_model(model)

            values.push([guid, model_type, model_id, source_url, state])
          end

          ActiveRecord::Base.connected_to(role: :writing) do
            MigratableResource.github_sql.run <<-SQL, values: GitHub::SQL::ROWS(values)
              INSERT IGNORE INTO
                migratable_resources
                (guid,model_type,model_id,source_url,state)
              VALUES
                :values
            SQL
          end

          progress.call(:progress_with_count, models.size)
        end
      end
    end
  end
end
