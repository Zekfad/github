# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    UTF8_ENCODING_STR = "UTF-8".freeze

    class BaseSerializer
      include Encoding
      include AreasOfResponsibility

      attr_reader :options

      areas_of_responsibility :migration

      def initialize(options = nil)
        @options = options || {}
        @model_url_service = @options[:model_url_service]
      end

      # Public: Serialize model attributes to a hash.
      #
      # model - Instance of an AR model.
      #
      # Returns a Hash.
      def serialize(model)
        return unless valid?(model)
        self.model = model
        as_json
      end

      def actor
        options[:actor]
      end

      def owner
        options[:owner]
      end

      # Public: Check if model is valid. Acceptable error messages can be
      # defined in acceptable_error_messages and are subtracted from any
      # validation error messages before determining of the record is valid
      # enough for export.
      #
      # model - Instance of an AR model.
      #
      # Returns true or false.
      def valid?(model)
        # oof. `model.valid?` adds a bunch of queries.
        model.valid?
        validation_error_messages = model.errors.messages
        valid = true

        validation_error_messages.each do |attribute, messages|
          acceptable = Array(acceptable_error_messages[attribute])

          # remove period at the end of validation messages
          messages = messages.map { |m| m.chomp(".") }

          if (messages - acceptable).any?
            valid = false
          end
        end

        valid
      end

      def acceptable_error_messages
        {
          commit_id: ["has been locked"],
          body: ["is too long (maximum is 65535 characters)"],
          head_repository_id: ["can't be blank"],
          base: [
            "Repository has been locked for migration",
            "Repository was archived so is read-only",
            "Repository was archived so is read-only and repository has been locked for migration",
            "Repository was archived so is read-only, unable to create comment because issue is locked, and repository has been locked for migration",
            "Repository was archived so is read-only and unable to create comment because issue is locked",
          ],
        }
      end

      # Public: A scope (or nil) to use when looking up models to export.
      def scope
      end

    private

      attr_accessor :model

      attr_reader :cache

      def time(t)
        t ? t.utc.xmlschema : nil
      end

      def url
        url_for_model(model)
      end

      def created_at
        time(model.created_at)
      end

      def ensure_utf8(content)
        guess_and_transcode(content)
      end

      # Internal: ModelUrlService handles url_for_model and model_for_url lookups.
      # It caches lookups to speed up migrations.
      #
      # Returns a ModelUrlService.
      def model_url_service
        @model_url_service ||= ModelUrlService.new
      end
      delegate :url_for_model, :url_for_association, :model_for_url, to: :model_url_service

      def actor_can_admin?
        actor && actor_is_enterprise_admin? || model.permit?(actor, :admin)
      end

      def actor_is_enterprise_admin?
        GitHub.enterprise? && actor.site_admin?
      end
    end
  end
end
