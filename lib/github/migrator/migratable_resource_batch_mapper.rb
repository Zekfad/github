# rubocop:disable Style/FrozenStringLiteralComment

require_relative "batch_service"

module GitHub
  class Migrator
    class MigratableResourceBatchMapper
      include AreasOfResponsibility

      areas_of_responsibility :migration

      def initialize(guid:)
        @guid = guid
      end

      attr_reader :guid

      def add(migratable_resource)
        batch_insert_migratable_resources.add(migratable_resource)
      end

      def complete
        batch_insert_migratable_resources.complete
      end

      private

      def batch_insert_migratable_resources
        @batch_insert_migratable_resources ||= BatchService.new(retry_on: ActiveRecord::Deadlocked) do |migratable_resources|
          values = []

          migratable_resources.each do |migratable_resource|
            model_type = migratable_resource.model_type
            model_id   = sanitize_nil(migratable_resource.model_id)
            source_url = migratable_resource.source_url
            target_url = sanitize_nil(migratable_resource.target_url)
            warning    = sanitize_nil(migratable_resource.warning)
            state      = MigratableResource.states[migratable_resource.state]

            values.push([guid, model_type, model_id, source_url, target_url, warning, state])
          end

          ActiveRecord::Base.connected_to(role: :writing) do
            MigratableResource.github_sql.run <<-SQL, values: GitHub::SQL::ROWS(values)
              INSERT INTO
                migratable_resources
                (guid,model_type,model_id,source_url,target_url,warning,state)
              VALUES
                :values
              ON DUPLICATE KEY UPDATE
                  model_type = VALUES(model_type),
                  model_id   = VALUES(model_id),
                  target_url = VALUES(target_url),
                  warning    = VALUES(warning),
                  state      = VALUES(state)
            SQL
          end
        end
      end

      def sanitize_nil(value)
        return GitHub::SQL::NULL if value.nil?
        value
      end
    end
  end
end
