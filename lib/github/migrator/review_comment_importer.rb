# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class ReviewCommentImporter < GitHub::Migrator::Importer

      INVALID_FILE_DIFF_REGEX = /\A@@\s-\d+,\d+\s[+]{1}\d+,\d+\s@@\z/i

      def initialize(options = {})
        super
        @in_reply_to_map = {}
      end

      def import(attributes, options = {})
        comment = PullRequestReviewComment.new do |review_comment|
          pull_request = model_from_source_url!(attributes["pull_request"], attributes)
          review_comment.pull_request = pull_request
          review_comment.pull_request_review = model_from_source_url(attributes["pull_request_review"])
          review_comment.repository = pull_request.repository
          review_comment.user = user_or_fallback(attributes["user"])
          review_comment.in_reply_to = model_from_source_url(attributes["in_reply_to"])
          review_comment.body = attributes["body"]
          review_comment.formatter = attributes["formatter"]
          review_comment.diff_hunk = attributes["diff_hunk"]
          review_comment.path = attributes["path"]
          review_comment.position = attributes["position"]
          review_comment.original_position = attributes["original_position"]
          review_comment.commit_id = attributes["commit_id"]
          review_comment.original_commit_id = attributes["original_commit_id"]
          # I know we're not supposed to set state directly, but we don't need
          # events fired for the comment when importing
          review_comment.send(:state=, attributes["state"])
          review_comment.created_at = attributes["created_at"]

          if review_comment.in_reply_to
            review_comment.pull_request_review_thread_id = review_comment.in_reply_to.pull_request_review_thread_id
          else
            review_thread = initialize_review_thread(pull_request.id, review_comment.pull_request_review&.id)
            if review_thread.save
              review_comment.pull_request_review_thread_id = review_thread.id
            end
          end
        end

        import_model(comment).tap do |review_comment|
          # Recalculate position if no position was provided
          recalculate_position(review_comment) unless attributes["position"]
        end
      rescue ActiveModel::RangeError => error
        # TODO: Remove this check and method once archive metadata validation
        # is implemented. See https://github.com/github/data-liberation/issues/344
        if should_log_and_skip?(attributes)
          GitHub::Logger.log_exception({
            at: "raise",
            fn: "GitHub::Migrator::ReviewCommentImporter#import",
            class: error.class.name,
            model_name: "pull_request_review_comment",
            source_url: attributes["url"],
            resolution: "Skipped import: Invalid file diff comment",
          }, error)

          return
        else
          raise error
        end
      end

      private

      def recalculate_position(review_comment)
        review_comment.send(:ensure_synced_with_pull)
      end

      def initialize_review_thread(pull_request_id, pull_request_review_id)
        PullRequestReviewThread.new(
          pull_request_id:        pull_request_id,
          pull_request_review_id: pull_request_review_id,
        )
      end

      # Check to see if we have an invalid file diff comment from
      # an earlier version of GHES
      def invalid_file_diff?(diff)
        !!(diff =~ INVALID_FILE_DIFF_REGEX)
      end

      def should_log_and_skip?(attributes)
        invalid_file_diff?(attributes["diff_hunk"]) &&
          nil_or_zero?(attributes["position"]) &&
          nil_or_zero?(attributes["original_position"])
      end

      def nil_or_zero?(value)
        value.to_i.zero?
      end
    end
  end
end
