# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class OrganizationSerializer < BaseSerializer
      def scope
        Organization.includes(:profile)
      end

      def as_json(options = {})
        {
          type: "organization",
          url: url,
          login: login,
          name: name,
          description: description,
          website: website,
          location: location,
          email: email,
          # members is an N+1 (or worse)
          members: members,
          # owners_team is an N+1
          owners_team: owners_team, # will be deprecated
          webhooks: webhooks,
          created_at: created_at,
        }
      end

    private

      def login
        model.login
      end

      def profile
        model.profile
      end

      def name
        profile.name if profile
      end

      def description
        profile.bio if profile
      end

      def website
        profile.blog if profile
      end

      def location
        profile.location if profile
      end

      def email
        if profile && profile.email.present?
          profile.email
        else
          StealthEmail.new(model).email
        end
      end

      def members
        return [] unless actor_can_admin?
        model.members.order(:created_at).map do |user|
          {
            user: url_for_model(user),
            role: model.role_of(user).type,
            state: model.membership_state_of(user),
          }
        end
      end

      def webhooks
        return [] unless actor_can_admin?
        model.hooks.active.map do |webhook|
          {
            payload_url: webhook.url,
            content_type: webhook.content_type,
            event_types: webhook.events,
            enable_ssl_verification: webhook.insecure_ssl == "0",
            active: true,
          }
        end
      end

      def owners_team
      end
    end
  end
end
