# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class IssueCommentSerializer < BaseSerializer
      def scope
        IssueComment.includes(:user, issue: :pull_request)
      end

      def as_json(options = {})
        hash = {
          type: "issue_comment",
          url: url,
        }

        if pull_request
          hash[:pull_request] = pull_request
        else
          hash[:issue] = issue
        end

        hash.merge!({
          user: user,
          body: body,
          formatter: formatter,
          created_at: created_at,
        })

        hash
      end

    private

      def issue
        url_for_model(model.issue)
      end

      def pull_request
        url_for_model(model.issue.pull_request)
      end

      def user
        url_for_model(model.user)
      end

      def body
        model.body
      end

      def formatter
        model.formatter
      end
    end
  end
end
