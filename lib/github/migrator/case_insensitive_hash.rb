# frozen_string_literal: true

module GitHub
  class Migrator
    class CaseInsensitiveHash < Hash
      include AreasOfResponsibility

      areas_of_responsibility :migration

      def [](key)
        super(key.downcase)
      end

      def []=(key, value)
        super(key.downcase, value)
      end
    end
  end
end
