# rubocop:disable Style/FrozenStringLiteralComment

require "github/migrator/migration_reporter"
require "github/migrator/cache"

# ExportPreparer is a service object that will prepare a Repository
# and its dependencies to be exported.
#
# To prepare the Repository, a MigratableResource  with the migration's
# guid will be created for the Repository and each of its dependent
# models.
#
# The dependencies of a Repository are defined as:
# - The Repository's owner (Organization)
# - All of that Organization's Teams
# - All of that Organization's Members
# - All of that Organization's Projects
# - The Repository's Milestones
# - The Repository's CommitComments
# - The Repository's Issues
# - The Repository's PullRequests
# - All of the PullRequests' ReviewComments
# - All of the Issues' Comments
# - All of the Repository's Projects
#
module GitHub
  class Migrator
    class ExportPreparer
      include AreasOfResponsibility

      areas_of_responsibility :migration

      # Public: Instantiates an ExportPreparer for the given repository and the migration
      # represented by guid.
      #
      # repository (Required) - Repository that will be exported to an archive.
      # guid (Required)       - A unique identifier representing the migration this model
      #                         will be prepared for.
      # options values:
      # events:               - An instance of GitHub::Migrator::Events that will notify its
      #                         subscribers of progress.
      # exclude_attachments   - If true, will not include attachments. Defaults to false.
      # lock_repository       - If true, lock the repository for the duration of the action.
      #                         Defaults to false.
      #
      def initialize(repository:, guid:, options: nil)
        options ||= {}
        @repository           = repository
        @guid                 = guid
        @lock_repository      = options.fetch(:lock_repository, false)
        @exclude_attachments  = options.fetch(:exclude_attachments, false)
        @events               = options.fetch(:events) { GitHub::Migrator::Events::Null.new }
        @cache                = GitHub::Migrator::Cache.new
      end

      # Public: Execute the action. Creates all of the MigratableResources for the
      # given Repository and its dependencies.
      #
      # Raises an exception if any required options are not present.
      #
      # Returns a Hash with a count of each model type that is prepared for exporting.
      def call
        validate
        # Get the top-level repo to be migrated, lock repo if lock was requested
        ActiveRecord::Base.connected_to(role: :writing) do
          repository.lock_for_migration if lock_repository
        end

        owner = repository.owner

        # Create MigratableResources for the Repository and its dependents
        export_builder.add(repository)
        add_repository_collaborators(repository)
        add_owner_teams_and_members(owner)
        add_owner_projects(owner)
        add_protected_branches(repository)
        add_repository_projects(repository)
        add_milestones(repository)
        add_commit_comments(repository)

        issue_ids = repository.issues.without_pull_requests.pluck(:id)
        add_issues(issue_ids)

        pull_request_ids = repository.issues.with_pull_requests.pluck(:pull_request_id)
        add_pull_requests(pull_request_ids)
        add_pull_request_reviews(pull_request_ids)
        add_pull_request_review_comments(pull_request_ids)
        add_pull_request_review_requests(pull_request_ids)

        all_issue_ids = repository.issues.pluck(:id)
        add_issue_comments(all_issue_ids)
        add_issue_events(all_issue_ids, owner)

        add_releases(repository)
        add_repository_files(repository)

        # Finalize
        attachment_migrator.complete
        export_builder.complete

        # Generate report hash and return
        migratable_resources = MigratableResource.for_guid(guid)
        GitHub::Migrator::MigrationReporter.migrator_result(migratable_resources)
      ensure
        cache.clear
      end

      private

      attr_reader :repository, :cache, :events, :guid, :lock_repository, :exclude_attachments

      # validate that required options are present
      def validate
        if repository.nil?
          raise GitHub::Migrator::RepositoryRequired
        end
        if guid.nil?
          raise GitHub::Migrator::GuidRequired
        end
      end

      def export_builder
        @export_builder ||= GitHub::Migrator::MigratableResourceExportBuilder.new \
          guid: guid,
          model_url_service: GitHub::Migrator::ModelUrlService.new(cache: cache),
          progress: lambda { |*x| events.fire(*x) }
      end

      def attachment_migrator
        @attachment_migrator ||=
          if exclude_attachments
            GitHub::Migrator::AttachmentMigrator::Null.new
          else
            GitHub::Migrator::AttachmentMigrator.new(export: export_builder)
          end
      end

      def add_repository_collaborators(repository)
        repository.members.find_each do |user|
          export_builder.add(user)
        end
      end

      def add_owner_teams_and_members(owner)
        export_builder.add(owner)

        if owner.try(:organization?)
          owner.members.find_each do |user|
            export_builder.add(user)
          end

          owner.teams.find_each do |team|
            export_builder.add(team)

            team.members.find_each do |user|
              export_builder.add(user)
            end
          end
        end
      end

      def add_owner_projects(owner)
        if owner.try(:organization?)
          owner.projects.find_each do |project|
            export_builder.add(project)
          end
        end
      end

      def add_issues(issue_ids)
        Issue.with_ids(issue_ids).find_each do |issue|
          export_builder.add(issue)
          export_builder.add(user_id: issue.user_id)
          attachment_migrator.push(issue)
        end
      end

       def add_pull_requests(pull_request_ids)
         PullRequest.includes(:issue).with_ids(pull_request_ids).find_each do |pull_request|
           export_builder.add(pull_request)
           export_builder.add(user_id: pull_request.user_id)
         end
       end

       def add_pull_request_reviews(pull_request_ids)
         PullRequestReview.where(pull_request_id: pull_request_ids).find_each do |review|
           export_builder.add(review)
           export_builder.add(user_id: review.user_id)
         end
       end

       def add_pull_request_review_comments(pull_request_ids)
         PullRequestReviewComment.where(pull_request_id: pull_request_ids).find_each do |review_comment|
           export_builder.add(review_comment)
           export_builder.add(user_id: review_comment.user_id)
         end
       end

       def add_pull_request_review_requests(pull_request_ids)
         ReviewRequest
           .where(pull_request_id: pull_request_ids, reviewer_type: "User")
           .pluck(:reviewer_id)
           .each { |reviewer_id| export_builder.add(user_id: reviewer_id) }
       end

       def add_issue_comments(issue_ids)
         IssueComment.where(issue_id: issue_ids).find_each do |comment|
           export_builder.add(comment)
           attachment_migrator.push(comment)
           export_builder.add(user_id: comment.user_id)
         end
       end

       def add_issue_events(issue_ids, owner)
         # Useful for checking whether a model belongs to a repo in the org.
         owner_repository_ids = owner.repositories.pluck(:id)

         IssueEvent.where(issue_id: issue_ids).includes(:issue).find_each do |event|
           # Skip exporting deployment issue events until we add support.
           # See https://github.com/github/github/issues/98221
           next if event.deployment_id

           if event.commit_repository_id
             next unless owner_repository_ids.include?(event.commit_repository_id)
           end

           export_builder.add(event)
           export_builder.add(user_id: event.actor_id)
         end
       end

      def add_milestones(repository)
        repository.milestones.find_each do |milestone|
          export_builder.add(milestone)
          export_builder.add(user_id: milestone.user_id)
        end
      end

      def add_protected_branches(repository)
        repository.protected_branches.find_each do |protected_branch|
          export_builder.add(protected_branch)
          export_builder.add(user_id: protected_branch.creator_id)
        end
      end

      def add_repository_projects(repository)
        repository.projects.find_each do |project|
          export_builder.add(project)
        end
      end

      def add_commit_comments(repository)
        repository.commit_comments.find_each do |commit_comment|
          export_builder.add(commit_comment)
          export_builder.add(user_id: commit_comment.user_id)
          attachment_migrator.push(commit_comment)
        end
      end

      def add_releases(repository)
        repository.releases.each do |release|
          export_builder.add(release)
          export_builder.add(user_id: release.author_id)
        end
      end

      def add_repository_files(repository)
        repository.repository_files.each do |repository_file|
          export_builder.add(repository_file)
          export_builder.add(user_id: repository_file.uploader_id)
        end
      end
    end
  end
end
