# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Migrator
    class RepositorySerializer < BaseSerializer
      def scope
        Repository.includes(:network, :repository_wiki, :owner, :labels, :projects, :public_keys, :configuration_entries)
      end

      def as_json(options = {})
        hash = {
          type: "repository",
          url: url,
          owner: owner,
          name: name,
          description: description,
          website: website,
          # on github.com, private? costs ~ 5 queries the first time it's called.
          private: private?,
          has_issues: has_issues?,
          has_wiki: has_wiki?,
          has_downloads: has_downloads?,
          labels: labels,
          collaborators: collaborators,
          created_at: created_at,
          git_url: git_url,
          default_branch: default_branch,
          webhooks: webhooks,
          public_keys: public_keys,
        }

        hash[:wiki_url] = wiki_url if has_wiki? && wiki_exists?

        if GitHub.anonymous_git_access_enabled?
          hash[:anonymous_access_enabled] = anonymous_git_access_enabled?
        end

        hash
      end

    private

      def owner
        url_for_model(model.owner)
      end

      def name
        model.name
      end

      def description
        model.description
      end

      def website
        model.homepage
      end

      def private?
        model.private?
      end

      def has_issues?
        model.has_issues?
      end

      def has_wiki?
        model.has_wiki?
      end

      def has_downloads?
        model.has_downloads?
      end

      def labels
        model.labels.map do |label|
          {
            url: url_for_model(label),
            name: label.name,
            color: label.color,
            created_at: time(label.created_at),
          }
        end
      end

      def collaborators
        model.members.map do |member|
          {
            user: url_for_model(member),
            permission: model.access_level_for(member).to_s,
          }
        end
      end

      def git_url
        "tarball://root/repositories/#{model.owner.login}/#{name}.git"
      end

      def wiki_exists?
        model.unsullied_wiki.exist?
      end

      def wiki_url
        "tarball://root/repositories/#{model.owner.login}/#{name}.wiki.git"
      end

      def default_branch
        model.default_branch
      end

      def webhooks
        model.hooks.active.map do |webhook|
          {
            payload_url: webhook.url,
            content_type: webhook.content_type,
            event_types: webhook.events,
            enable_ssl_verification: webhook.insecure_ssl == "0",
            active: true,
          }
        end
      end

      def public_keys
        model.public_keys.map do |public_key|
          {
            title: public_key.title,
            key: public_key.key,
            read_only: public_key.read_only,
            fingerprint: public_key.fingerprint,
            created_at: time(public_key.created_at),
          }
        end
      end

      def anonymous_git_access_enabled?
        model.anonymous_git_access_enabled?
      end
    end
  end
end
