# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module RepoGraph
    class Participation
      class Data
        def initialize(repository)
          @repository = repository
        end

        attr_reader :repository


        def fetch(time: Time.now)
          midnight = time.utc.midnight
          GitHub.cache.fetch(cache_key(midnight)) { fetch!(time: midnight) }
        end

        def fetch!(time:)
          return empty_data unless repository&.default_oid

          user = repository.user
          emails = user.emails.map { |x| x.email }

          repository.rpc.participation_stats(repository.default_oid, emails, time)
        end

        private

        def cache_key(time)
          suffix = "GitHub::Graphs::Participation:v5:#{@repository.id}:#{time.to_i}".to_md5
          "participation-graph:#{suffix}"
        end

        def empty_data
          { "all" => [], "owner" => [] }
        end
      end
    end
  end
end
