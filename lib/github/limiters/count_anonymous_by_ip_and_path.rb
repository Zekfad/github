# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Limiters
    # Limit anonymous requests by client IP and path
    class CountAnonymousByIPAndPath < MemcachedWindow
      include GitHub::Middleware::Constants
      include GitHub::Middleware::AnonymousRequest

      def initialize(limit:, ttl: 60)
        super "anon-ip-path-count", limit: limit, ttl: ttl
      end

      def start(request)
        return OK unless anonymous_request?(request)
        super
      end

      def record_start(request)
        increment_counter(request)
      end

      def key(request)
        [request.host, request.ip, request.path_info].join("-")
      end
    end
  end
end
