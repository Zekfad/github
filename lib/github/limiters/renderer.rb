# frozen_string_literal: true

module GitHub
  module Limiters

    # Public: This middleware tidies up 429 responses.
    class Renderer
      include GitHub::Middleware::Constants

      HTML_PATH = "/public/429#{"-enterprise" if GitHub.enterprise?}.html".freeze
      HTML = [File.read("#{Rails.root}#{HTML_PATH}").freeze].freeze
      JSON = [File.read("#{Rails.root}/public/429.json").freeze].freeze

      ALLOW_BODY = "limiter.renderer.allow_body".freeze

      def initialize(app)
        @app = app
      end

      def call(env)
        status, headers, body = res = @app.call(env)
        return res if status != 429 || allow_body?(env)

        # filter out most headers, we want a minimal 429 (or 403)
        headers.select! { |k, v| allow_header?(k, v) }

        if headers[GH_LIMITED_GROUP] == "api".freeze
          headers[CONTENT_TYPE] = "application/json; charset=utf-8".freeze
          api_version = Api::MediaType.identify_api_version(env["REQUEST_PATH"])
          headers["X-GitHub-Media-Type".freeze] = Api::MediaType.default(api_version).to_http_header

          [403, headers, JSON]
        else
          [429, headers.merge!(CONTENT_TYPE => TEXT_HTML), HTML]
        end
      end

      private

      # Internal: Allow internal, CORS, and rate limiting headers.
      def allow_header?(name, value)
        /\A(?:Access-Control|GH|X)-/ =~ name || name == "Retry-After".freeze
      end

      # Internal: Allow the returned response body (do not replace it).
      def allow_body?(env)
        !!env[ALLOW_BODY]
      end
    end
  end
end
