# rubocop:disable Style/FrozenStringLiteralComment

require "rack"
require "rack/request_logger"
require "github/timeout_middleware"

module GitHub
  module Limiters

    # Public: This middleware kills fascists.
    class Middleware
      include GitHub::Middleware::Constants

      SKIP_LIMIT_CHECKS = "github.limiters.skip".freeze

      # Public: Initialize a limiter middleware.
      #
      # app      - the Rack application this middleware is wrapping.
      # group    - a graphite-safe String group name for these limiters.
      # limiters - zero or more limmiters conforming to GitHub::Limiter's
      #            interface.
      #
      # Group names are used to help determine which set of limiting middleware
      # is active, for example "app" versus "api".
      #
      # Limiter names must be unique Graphite-safe strings and should
      # also conform to the following convention of "prefix-suffix" where:
      #
      #   prefix: the noun or nouns being used to discriminate requests,
      #           e.g. "path", "path-ip", and "ua"
      #   suffix: one of:
      #           - "count" for counting things
      #           - "elapsed" for counting elapsed time
      #           - "concurrent" for tracking concurrency
      #
      def initialize(app, group, *limiters)
        if Limiter::NAME !~ group
          raise ArgumentError, "Bad group name: a-z and '-' only"
        end

        @app = app
        @group = group
        @limiters = limiters
      end

      # Public: Handle a request, returning A minimal 429 response if the
      # request is limited. Translate the 429 to a more user-friendly response
      # somewhere upstream of this middleware.
      #
      # Limited requests add `limited=<middleware-name>/<limiter-name>` to the
      # log context, try `/splunk -1h limited=app/path | top path_info`.
      def call(env)
        if @limiters.empty? || self.class.skip_limit_checks?(env) || ignored?(env)
          return @app.call(env)
        end

        request  = Rack::Request.new(env)
        response = nil
        started  = []

        GitHub.dogstats.increment("limited.tries", tags: ["group:#{@group}"])

        @limiters.each do |limiter|
          state = limiter.start(request)
          started << limiter

          if state.limited?
            method = env[REQUEST_METHOD].downcase
            tags = Array(limiter.tags(request)).concat(["group:#{@group}", "limiter:#{limiter.name}", "method:#{method}"])
            GitHub.dogstats.increment("limited.request", tags: tags)

            # Rate limited requests shed at the middleware layer don't go through the rest of our request
            # code paths. However we do potentially want the user details from the token to provide better
            # support in dealing with rate limit queries. This tries to find the user from the environment
            # credentials and will add that to the log/hydro data if it is found. Failures are ignored.
            user_being_limited = nil

            # Guard against rack data not being available
            creds = if env["rack.input"]
              Api::RequestCredentials.from_env(env)
            else
              nil
            end

            if creds
              api_auth = GitHub::Authentication::Attempt.new(
                allow_integrations:         true,
                allow_user_via_integration: true,
                from:                       :api,
                login:                      creds.login,
                password:                   creds.password,
                otp:                        creds.otp,
                token:                      creds.token,
                request_id:                 env["HTTP_X_GITHUB_REQUEST_ID"],
              )
              if api_auth.credentials_present?
                result = api_auth.result
                if result.success?
                  user_being_limited = result.user.login
                end
              end
            end

            logging_limit_message = "#{@group}/#{limiter.name}"
            if (log_data = env[Rack::RequestLogger::APPLICATION_LOG_DATA])
              log_data["limited"] = logging_limit_message
              log_data["current_user"] = user_being_limited if user_being_limited
            end
            if (hydro_payload = env[GitHub::HydroMiddleware::PAYLOAD])
              hydro_payload["secondary_rate_limit_reason"] = logging_limit_message
              hydro_payload["current_user"] = user_being_limited if user_being_limited
            end

            if enabled?(env)
              headers = {
                CONTENT_TYPE     => TEXT_PLAIN,
                # These headers are stripped by haproxy on the way out:
                GH_LIMITED_BY    => limiter.name,
                GH_LIMITED_GROUP => @group,
              }

              headers[RETRY_AFTER] = state.duration.to_s if state.duration

              response = [429, headers, EMPTY_BODY]
            end

            break
          end

          GitHub::TimeoutMiddleware.notify(env, limiter)
        end

        begin
          return response || @app.call(env)
        ensure
          # If skip_limit_checks is set here, it means the app toggled the flag
          # based on information that wasn't available when this limiter
          # started. Cancel any pending limiters:
          if self.class.skip_limit_checks?(env)
            started.each { |limiter| limiter.cancel(request) }
          else
            started.each { |limiter| limiter.finish(request) }
          end
        end
      end

      # Public: Mark a request to ignore request limit checks.
      #
      # This prevents any limiter middleware from running, and any limiters
      # already in-flight will be canceled.
      #
      # env - a Rack request environment
      #
      # Modifies the given environment to set a flag. Returns nothing.
      def self.skip_limit_checks(env)
        env[SKIP_LIMIT_CHECKS] = true
      end

      # Internal: should limit checks be skipped?
      #
      # env - a Rack request environment
      #
      # Returns true or false.
      def self.skip_limit_checks?(env)
        !!env[SKIP_LIMIT_CHECKS]
      end

      protected

      # Internal: Override this method in a subclass to activate limiting.
      def enabled?(env)
        false
      end

      # Internal: Override this method in a subclass to implement whitelists.
      def ignored?(env)
        false
      end

    end
  end
end
