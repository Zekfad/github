# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Limiters

    # Internal: These paths skip limiting completely.
    #
    # /status is used internally by haproxy to determine if workers are alive.
    # /site/sha is used internally by amen for monitoring
    #
    # Allow otherwise-abusive requests to /login and /login/oauth/*. For
    # example, a shared office with a misconfigured script requiring
    # authentication would not block the rest of the users on the same IP from
    # logging in or authenticating with oauth.
    #
    # Avatar and key files are whitelisted to allow automated scripts like chef
    # to load keys files, and for the atom plugins manager to load avatars
    # without being rate-limited.
    #
    # Login attempts are also rate-limited elsewhere.
    IGNORED_PATHS = %r(\A( /status                  # internal haproxy check
                          |/site/sha                # internal amen check
                          |/login(/oauth/\w+)?      # login/oauth on dotcom
                          |/auth/github(/callback)? # oauth on gist
                          |/[^/]+\.(png|keys)       # avatar/keys files
                         )\Z)x

    # Internal: User-agent for nugget requests
    NUGGET_USER_AGENT = %r(\bnugget/\d+\.\d+\.\d+\b)

    # Public: This subclass whitelists some app paths.
    class AppMiddleware < GitHub::Limiters::Middleware
      class Disabled < AppMiddleware
        # Disabled by default, but with a query string override for testing
        def enabled?(env)
          (query = env[QUERY_STRING]) && query.include?("enable-limiters")
        end
      end

      def initialize(app, *limiters)
        super(app, "app", *limiters)
      end

      protected

      def enabled?(env)
        true
      end

      def ignored?(env)
        super || ignored_path?(env) || nugget_request?(env)
      end

      def ignored_path?(env)
        IGNORED_PATHS =~ env[PATH_INFO]
      end

      # Nugget makes requests to `/` on gist.github.com with a nugget user
      # agent. Make sure those are allowed.
      def nugget_request?(env)
        env[HTTP_USER_AGENT] =~ NUGGET_USER_AGENT
      end
    end
  end
end
