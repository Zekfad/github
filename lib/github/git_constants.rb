# frozen_string_literal: true

module GitHub
  NULL_OID  = ("0" * 40)
  NULL_MODE = ("0" * 6)
  TREE_MODE = "040000"

  EMPTY_TREE_OID = "4b825dc642cb6eb9a060e54bf8d69288fbee4904"

  SHA_LIKE_REF_NAME = /\Arefs\/(heads|tags)\/([0-9a-f]{40}(\/.*)?)\z/
end
