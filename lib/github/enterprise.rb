# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/mysql"
require "github-ds"

module GitHub
  module Enterprise
    autoload :License,     "github/enterprise/license"
    autoload :Middleware,  "github/enterprise/middleware"
    autoload :CronEntry,   "github/enterprise/cron_entry"
    autoload :Certificate, "github/enterprise/certificate"

    # Adds `enterprise?` where included, both as instance and class methods.
    module Behavior
      def enterprise?
        GitHub.enterprise?
      end
      def self.included(target)
        target.extend ClassMethods
      end
      module ClassMethods
        def enterprise?
          GitHub.enterprise?
        end
      end
    end

    # Determine whether a backup-utils backup is in progress. When a backup is
    # in progress no git GC or repack operations may be performed since that can
    # result in backups being captured in an inconsistent state. For more
    # information see:
    # <https://github.com/github/backup-utils/blob/master/share/github-backup-utils/ghe-backup-repositories-rsync>
    def self.backup_in_progress?
      GitHub.enterprise? && File.exist?("#{GitHub.repository_root}/.sync_in_progress")
    end

    # Path to file that signals that a backup-utils backup is in progress. This
    # file is created when ghe-backup starts a backup and is unlinked when the
    # operation is completed.
    def self.backup_in_progress_file
      "#{GitHub.repository_root}/.sync_in_progress"
    end

    # Public - Check the Enterprise maintenance key to see if maintenance is scheduled
    #
    # Returns the formatted time when maintenance will begin, or nil if a maintenance
    # time has not been set.
    def self.maintenance_scheduled
      if maintenance_time = GitHub.kv.get("enterprise:maintenance").value { nil }
        begin
          time = Time.iso8601(maintenance_time)
        rescue ArgumentError
          # Parsing the time using #iso8601 failed
          return nil
        end

        time.in_time_zone.strftime("%A, %B %-d at %k:%M %z")
      end
    end

    # Public - Check if there's an announcement
    #
    # Returns the announcement text
    def self.announcement
      # if kv is down, we default to no announcement as it seems better to
      # return no announcement than to fail showing a page entirely
      if html = GitHub.kv.get("enterprise:announcement").value { nil }
        result = GitHub::Goomba::MarkdownPipeline.call(html, {})
        output = result[:output]
        output = output.html_safe if result[:html_safe] # rubocop:disable Rails/OutputSafety
        output
      end
    end

    # Instantiate and memoize the License object for this Enterprise install
    # using the settings defined in GitHub::Config.
    #
    # Returns a License.
    def self.license(sync_global_business: true)
      if Rails.development? || Rails.test?
        @license ||= LicenseMock.new
      else
        @license ||= License.new(sync_global_business: sync_global_business)
      end
    end

    if Rails.development? || Rails.test?
      class LicenseMock < License
        attr_writer :company, :expire_at, :seats, :perpetual, :unlimited, :evaluation, :github_connect_support, :advanced_security_enabled
        attr_writer :seats_used

        def initialize(
          expire_at: 1.year.from_now.to_datetime,
          seats: 0,
          perpetual: false,
          unlimited: true,
          evaluation: false,
          github_connect_support: false,
          custom_terms: nil,
          advanced_security_enabled: true,
          company: "ACME, Inc")
          @company, @expire_at, @seats, @perpetual, @unlimited, @evaluation, @github_connect_support, @custom_terms, @advanced_security_enabled =
            company, expire_at, seats.to_i, perpetual, unlimited, evaluation, github_connect_support, custom_terms, advanced_security_enabled
        end

        def reload!; true end

        def seats_used
          return @seats_used if @seats_used
          super
        end

        def github_connect_support
          return @github_connect_support if @github_connect_support
          super
        end
        alias github_connect_support? github_connect_support

        def custom_terms
          return @custom_terms
        end

        def custom_terms?
          return !!@custom_terms if @custom_terms
          super
        end

        def license_data
          "ohana means family"
        end
      end

      def self.license_reset!(options = {})
        @license = LicenseMock.new(**options)
      end

      license_reset!
    end

    # Instantiate and memoize the GitHub::Enterprise::Certificate object for
    # this Enterprise installation.
    #
    # Returns a GitHub::Enterprise::Certificate if Enterprise, otherwise nil.
    def self.certificate
      return nil if !GitHub.enterprise? || !GitHub.ssl?
      @certificate ||= Certificate.new(
        OpenSSL::X509::Certificate.new(GitHub.ssl_certificate),
      )
    end

    def self.certificate_reset!(x509_certificate)
      @certificate = Certificate.new(x509_certificate)
    end

    def self.ensure_business!
      GitHub.load_activerecord

      business_count = ApplicationRecord::Domain::Users.github_sql.run(<<-SQL)
        SELECT count(id) FROM businesses
      SQL

      unless business_count.row.first == 0
        puts "Existing enterprise account found. Skipping creating a global enterprise account."
        return
      end

      license = GitHub::Enterprise.license(sync_global_business: false)
      slug = license.company.parameterize.presence || GitHub.default_business_base_slug

      ApplicationRecord::Domain::Users.github_sql.run(<<-SQL, name: license.company, slug: slug, seats: license.seats)
        INSERT IGNORE INTO businesses (id, name, slug, created_at, updated_at, seats)
        VALUES
          (1, :name, :slug, NOW(), NOW(), :seats)
      SQL
    end

    def self.ensure_customer!
      GitHub.load_activerecord

      business_count = ApplicationRecord::Domain::Users.github_sql.run(<<-SQL)
        SELECT count(id) FROM businesses
      SQL

      unless business_count.row.first == 1
        puts "No global enterprise account found. Cannot create a global customer."
        return
      end

      customer_count = ApplicationRecord::Domain::Users.github_sql.run(<<-SQL)
        SELECT count(id) FROM customers
      SQL

      unless customer_count.row.first == 0
        puts "Existing customer record found. Skipping creating a global customer."
        return
      end

      license = GitHub::Enterprise.license(sync_global_business: false)

      ApplicationRecord::Domain::Users.github_sql.run(<<-SQL, name: license.company, seats: license.seats)
        INSERT IGNORE INTO customers (id, name, seats, created_at, updated_at)
        VALUES
          (1, :name, :seats, NOW(), NOW())
      SQL

      ApplicationRecord::Domain::Users.github_sql.run(<<-SQL)
        UPDATE businesses SET customer_id=1 where id=1
      SQL
    end
  end
end
