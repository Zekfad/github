# rubocop:disable Style/FrozenStringLiteralComment

# A generalized, signed authentication token, supporting scopes and expiration.
#
# Examples
#
#   token = SignedAuthToken.generate :user    => someuser,
#                                    :scope   => 'some_scope',
#                                    :expires => 30.seconds.from_now
#   parsed_token = SignedAuthToken.verify :token => token
#                                         :scope => 'some_scope'
#   parsed_token.valid?
#   # => true
#   parsed_token.user
#   # => someuser
#
#   expired_token = SignedAuthToken.generate :user    => someuser,
#                                            :scope   => 'some_scope',
#                                            :expires => 30.seconds.ago
#   parsed_token = SignedAuthToken.verify :token => expired_token
#                                         :scope => 'some_scope'
#   parsed_token.valid?
#   # => false
#   parsed_token.reason
#   # => :expired
#   parsed_token.expired?
#   # => true
#   parsed_token.user
#   # => nil
class GitHub::Authentication::SignedAuthToken
  DEFAULT_EXPIRES = 30.seconds

  attr_accessor :user, :session, :expires, :scope, :data, :reason

  # Checks whether string is in the format of a SignedAuthToken
  #
  # Returns boolean based on validity.
  def self.valid_format?(token)
    !possible_versions(token).empty?
  end

  # Generate a token string from the given options. The data used to generate
  # the token is signed but not encrypted. Don't put sensitive data in the
  # `data` field.
  #
  # user:    - The User to make the token for (optional).
  # session: - The UserSession to make the token for (optional).
  # scope:   - The String scope the token is valid for.
  # expires: - The Time the token will expire.
  # data:    - Arbitrary data to store in the token (any basic type).
  #
  # Returns String token.
  def self.generate(user: nil, session: nil, scope:, expires: nil, data: nil)
    # Ensure that we aren't caching signed auth tokens.
    GitHub::CacheLeakDetector.no_caching!

    raise ArgumentError unless scope.is_a?(String) && !scope.empty?
    raise ArgumentError if user.nil? && session.nil?
    raise ArgumentError unless user.nil? || session.nil?

    kwargs = {
      user: user,
      session: session,
      scope: scope,
      expires: expires,
      data: data,
    }

    # delete nil kwargs so Version3 and Session can use their own default values
    kwargs.delete_if { |_, value| value.nil? }

    if session.nil?
      Version3.generate(**kwargs)
    else
      Session.generate(**kwargs)
    end
  end

  # Verify a token.
  #
  # token: - The String token to verify.
  # scope: - The expected String scope of the token.
  #
  # Returns SignedAuthToken instance.
  def self.verify(token:, scope:)
    instrument(do_verify(token: token, scope: scope))
  end

  def initialize(user: nil, session: nil, expires: nil, scope: nil, data: nil, reason: nil)
    @user    = user
    @session = session
    @expires = expires
    @scope   = scope
    @data    = data
    @reason  = reason
  end

  # Helpers for identifying the reason why a token is invalid.
  #
  # Each method returns a bool.
  %w[bad_token bad_scope bad_login user_suspended expired valid session_expired session_revoked].each do |name|
    define_method("#{name}?") { reason == name.intern }
  end

  # Returns the Installation id from the token data.
  #
  # Returns a Integer or nil.
  def installation_id
    data["installation_id"] if data.is_a?(Hash)
  end

  # Returns the Installation type from the token data.
  #
  # Returns a String or nil.
  def installation_type
    data["installation_type"] if data.is_a?(Hash)
  end

  def version
    :invalid
  end

  # Verify a token.
  #
  # token: - The String token to verify.
  # scope: - The expected String scope of the token.
  #
  # Returns SignedAuthToken instance.
  def self.do_verify(token:, scope:)
    raise ArgumentError unless scope.is_a?(String) && !scope.empty?

    versions = possible_versions(token)
    return bad_token(:bad_token) if versions.empty?

    results = versions.map do |version|
      version.verify(token: token, scope: scope).tap do |result|
        return result if result.valid?
      end
    end

    # Prefer token that wasn't rejected for being invalidly formatted
    best_result = results.find { |i| !i.bad_token? }
    best_result ||= results.first

    return best_result
  end

  # Instrument the result of verifying a token.
  #
  # token - A SignedAuthToken instance.
  #
  # Returns the token.
  def self.instrument(token)
    GitHub.dogstats.increment("authentication.signed_auth_token", tags: [
      "valid:#{token.valid?}",
      "version:#{token.version}",
      "reason:#{token.reason}",
    ])

    token
  end

  # Get the list of versions that this token matches the format of.
  #
  # token - The String token to guess a version for.
  #
  # Returns an Array of SignedAuthToken subclasses.
  def self.possible_versions(token)
    return [] if !token.is_a?(String) || token.empty?

    # Prefer newer token versions by putting them first.
    [
      Version3,
      Session,
      Version2,
      Version2Hex,
      Version1,
    ].select { |version| version.token_regex.match?(token) }
  end

  def self.bad_token(reason)
    new(reason: reason)
  end
end

require_dependency "github/authentication/signed_auth_token/version1"
require_dependency "github/authentication/signed_auth_token/version2"
require_dependency "github/authentication/signed_auth_token/version2_hex"
require_dependency "github/authentication/signed_auth_token/version3"
require_dependency "github/authentication/signed_auth_token/session"
