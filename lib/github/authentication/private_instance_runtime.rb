# frozen_string_literal: true
require "forwardable"

require_relative "default"
require_relative "saml"

module GitHub
  module Authentication
    class PrivateInstanceRuntime
      extend Forwardable

      # These auth modes are safe to delegate to the dynamic adapters.
      #
      # Otherwise we hardcode the auth mode predicate methods (e.g. `cas?`)
      # to false below.
      DELEGATED_AUTH_MODES = [:default, :saml]

      def initialize(config = {})
        @configuration = config
      end

      # Delegate generally-defined behaviors to the configured authentication adapter.
      # NOTE: Keep this list alphabetized.
      def_delegators :adapter,
        :access_token?,
        :access_token_authenticate,
        :allow_builtin_users?,
        :authentication_token?,
        :builtin_auth_attempt?,
        :builtin_auth_fallback?,
        :configuration,
        :constant_salt_bcrypt,
        :custom_authentication_name,
        :enterprise_log,
        :enterprise_logger,
        :external?,
        :external_mapping,
        :external_user?,
        :format_log,
        :instrument,
        :instrumentation_service,
        :log,
        :logout_url,
        :metadata,
        :name,
        :oauth_access_token?,
        :password_and_otp_authenticate,
        :password_authenticate,
        :path,
        :redirect_on_failure?,
        :rails_authenticate,
        :rails_logout,
        :saml?,
        :signup_enabled?,
        :sudo_mode_enabled?,
        :sudo_password_authenticate,
        :suspended_installation_message,
        :two_factor_authentication_allowed?,
        :two_factor_authentication_enabled?,
        :two_factor_org_requirement_allowed?,
        :user_renaming_enabled?,
        :verifiable?

      # Delegate SAML authentication adapter-specific methods.
      def_delegators :adapter,
        :request_tracking?,
        :sp_pkcs12

      # Hard code auth mode checks like `cas?` to false.
      #
      # Skips hard coding delegated auth mode checks since those
      # checks should be delegated to the dynamic auth adapter.
      (AUTH_MODES - DELEGATED_AUTH_MODES).each do |auth_mode|
        define_method "#{auth_mode}?" do
          false
        end
      end

      # Public: Ignore add middleware step in boot.
      #
      # This is required to avoid dynamically computing the authentication adapter at boot time,
      # given that the Business model (and `GitHub.global_business`) cannot be accessed that early.
      def add_middleware(*)
      end

      # Mapping from name of attribute to idP configured attribute name. This
      # will be replaced with constant values rather than configurable values.
      def attribute_mappings
        @attribute_mappings ||= {
          user_name: configuration[:user_name],
          admin: configuration[:admin] || "administrator".freeze,
          emails: configuration[:emails] || "emails".freeze,
          display_name: configuration[:display_name] || "full_name".freeze,
          ssh_keys: configuration[:ssh_keys] || "public_keys".freeze,
          gpg_keys: configuration[:gpg_keys] || "gpg_keys".freeze,
        }
      end

      # Public: Returns true if Profile Names are managed externally
      # Returns false if default authentication is used.
      # Returns true for SAML, since a name is sent by default in AzureAD
      def profile_name_managed_externally?
        # never true for default authentication
        external?
      end

      # Public: Returns true if Emails are managed externally
      # Returns false if default authentication is used.
      # Returns false for SAML, since an email is not sent by default with AzureAD.
      def emails_managed_externally?
        # never true for default authentication

        return false unless external?
        false
      end

      # Public: Returns true if SSH keys are managed externally
      # Returns false if default authentication is used.
      # Returns false for SAML since this is custom functionality
      def ssh_keys_managed_externally?
        # never true for default authentication
        return false unless external?
        false
      end

      # Public: Returns true if Emails are managed externally
      # Returns false if default authentication is used.
      # Returns false for SAML since this is custom functionality
      def gpg_keys_managed_externally?
        return false unless external?
        false
      end

      # Public: Whether changing user profile names via the UI is allowed.
      #
      # True if default authentication is used
      # False if profile name set via SCIM
      # False by default if SAML is enabled
      def user_change_profile_name_enabled?(user)
        return false unless user

        # Default authentication
        return true unless saml_enabled?

        display_name = scim_data_for_user(user)&.display_name

        # A profile name has been specified via SCIM provisioning
        return false if display_name&.present?


        # False by default. This will be revisited once saml_user_data is associated with the ExternalIdentity
        false
      end

      # Public: Whether changing emails via the UI is allowed.
      #
      # True if default authentication is used
      # True by default if SAML is enabled
      def user_change_email_enabled?(user)
        return false unless user

        # Default authentication
        return true unless saml_enabled?

        emails = scim_data_for_user(user)&.emails

        # Emails have been specified via SCIM provisioning
        return false if emails && !emails.empty?

        # True by default. This will be revisited once saml_user_data is associated with the ExternalIdentity
        true
      end

      # Public: Whether changing SSH keys via the UI is allowed.
      #
      # True if default authentication is used
      # True by default if SAML is enabled
      def user_change_ssh_key_enabled?(user)
        return true unless external?
        return false unless user
        true
      end

      # Public: Whether changing GPG keys via the UI is allowed.
      #
      # True if default authentication is used
      # True by default if SAML is enabled
      def user_change_gpg_key_enabled?(user)
        return true unless external?
        return false unless user
        true
      end

      private

      # Internal: Dynamically compute the authentication adapter based on the
      # global business configuration.
      def adapter
        if saml_enabled?
          @saml_adapter ||= build_saml_adapter
        else
          # ensure we reset the SAML adapter in case config changes
          @saml_adapter = nil

          @default_adapter ||= build_default_adapter
        end
      end

      def build_default_adapter
        Default.new(@configuration)
      end

      def build_saml_adapter
        GitHub::Authentication::SAML.new(saml_configuration)
      end

      # Internal: Check if SAML has been configured and enabled.
      def saml_enabled?
        GitHub.global_business&.saml_sso_enabled?
      end

      # Override configuration to pull configuration from Business::SamlProvider.
      #
      # This duplicates GitHub::Config#auth_options because that config would not
      # be set for GHPI processes since the auth mode is not configurable.
      def saml_configuration
        provider = GitHub.global_business.saml_provider
        {
          sp_url: GitHub.url,

          # SSO settings from SAML Provider config
          sso_url: provider.sso_url,
          issuer: provider.issuer,
          idp_certificate: provider.idp_certificate,
          signature_method: provider.signature_method,
          digest_method: provider.digest_method,

          # SSO settings set at boot only
          builtin_auth_fallback: GitHub.builtin_auth_fallback,
          idp_initiated_sso: GitHub.saml_idp_initiated_sso,
          name_id_format: GitHub.saml_name_id_format,
          disable_admin_demote: GitHub.saml_disable_admin_demote,
          sp_pkcs12_file: GitHub.saml_sp_pkcs12_file,

          # attribute mapping
          admin: GitHub.saml_admin,
          user_name: GitHub.saml_username_attr,
          display_name: GitHub.saml_profile_name,
          emails: GitHub.saml_profile_mail,
          ssh_keys: GitHub.saml_profile_ssh_key,
          gpg_keys: GitHub.saml_profile_gpg_key,
        }
      end

      # Returns the SCIM provisioning data for a user in the global business
      # Returns nil if the user is not provisioned via SCIM
      def scim_data_for_user(user)
        return nil unless user
        return nil unless saml_enabled?
        provider = GitHub.global_business.saml_provider

        external_identity = ExternalIdentity.by_provider(provider).linked_to(user).first
        return unless external_identity

        external_identity.scim_user_data
      end
    end
  end
end
