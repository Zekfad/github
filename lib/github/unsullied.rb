# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Unsullied
    autoload :Comparison,      "github/unsullied/comparison"
    autoload :Editor,          "github/unsullied/editor"
    autoload :FilesCollection, "github/unsullied/files_collection"
    autoload :HTML,            "github/unsullied/html"
    autoload :Page,            "github/unsullied/page"
    autoload :PagesCollection, "github/unsullied/pages_collection"
    autoload :Wiki,            "github/unsullied/wiki"
  end
end
