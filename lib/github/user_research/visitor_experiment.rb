# frozen_string_literal: true

module GitHub
  module UserResearch
    # VisitorExperiment defines an experiment, much like UserExperiment,
    # except that it's suitable for use with current_visitor as its
    # subject.
    #
    # This type of experiment needs to be run entirely in the browser,
    # so that it works on cached pages.
    class VisitorExperiment < UserExperiment
      # Raised when enrollment is done in rails for a VisitorExperiment.
      #
      # Enrollment for VisitorExperiment must be done in the browser so
      # that it works for cached pages.
      class ExperimentMustBePerformedInBrowser < StandardError
        def message
          "When the subject of an experiment is 'current_visitor', the variant must be selected in the browser instead of in ruby."
        end
      end

      # Internal: Returns true if the given subject's class is permitted to be used
      # with this experiment. VisitorExperiment may only be used with a visitor.
      def subject_allowed?(subject)
        subject.is_a?(Analytics::Visitor)
      end

      # Internal: Returns true if this experiment's configuration can be serialized
      # into a <meta> tag and handled client side.
      def metadata_allowed?
        # Currently, qualifiers are not supported, and groups must be
        # defined as percentages. Feel free to modify these conditions
        # (and #to_h and the corresponding JS) if your experiment needs
        # different functionality.
        !has_qualifier? && segmenter.is_a?(Verdict::Segmenters::FixedPercentageSegmenter)
      end

      def switch(*)
        raise ExperimentMustBePerformedInBrowser
      end

      def lookup(*)
        raise ExperimentMustBePerformedInBrowser
      end

      def to_h
        {
          handle: handle,
          roll_out_percent: @roll_out_percent,
          groups: groups.map { |_, group| { handle: group.handle, min: group.percentile_range.min, max: group.percentile_range.max } },
        }
      end
    end
  end
end
