# frozen_string_literal: true
module GitHub
  module UserResearch
    # Storage plugin for Shopify/verdict's A/B testing framework that persists
    # experiment group enrollment (e.g. "subject X was enrolled in experiment
    # Y's test group at Z time") in our MySQL database.
    #
    # The two tables currently used are user_experiments, which holds experiment
    # metadata and enforces that experiments are unique on their 'slug', or
    # 'handle' in verdict's lingo, and user_experiment_enrollments.
    class ExperimentDatabaseStorage
      # Internal: Create a new experiment row with the given slug and name.
      def save_experiment(name, slug)
        sql = <<-SQL
        INSERT INTO
          user_experiments
          (name, slug, created_at, updated_at)
        VALUES
          (:name, :slug, :now, :now)
        SQL

        ActiveRecord::Base.connected_to(role: :writing) do
          ApplicationRecord::Domain::UserExperiments.github_sql.new(
            sql,
            name: name,
            slug: slug.to_s,
            now: GitHub::SQL::NOW,
          ).run
        end
      end

      # Internal: Persist the subject's enrollment in the experiment to
      # the database.
      def store_assignment(assignment)
        if assignment.experiment.saved?
          StoreAssignmentJob.perform_later(
            user_experiment_id: assignment.experiment.state.id,
            user_id: assignment.subject_identifier.id & 0xffffffff,
            subject_id: assignment.subject_identifier.id,
            subject_type: assignment.subject_identifier.type,
            subgroup: assignment.to_sym,
          )
        end
      end

      # Internal: Fetch a subject's group assignment (or lack
      # thereof) from the database. Per verdict's documentation,
      # this lookup should be simple and fast; for that purpose,
      # in database-land, there's a compound index on
      # [user_experiment_id, subject_id].
      def retrieve_assignment(experiment, subject_identifier)
        return unless experiment.saved?

        sql = <<-SQL
        SELECT
          subgroup, created_at
        FROM
          user_experiment_enrollments
        WHERE
          user_experiment_id = :experiment_id AND
          subject_id = :subject_id AND
          subject_type = :subject_type
        SQL

        row = ApplicationRecord::Domain::UserExperiments.github_sql.new(
          sql,
          experiment_id: experiment.state.id,
          subject_id: subject_identifier.id,
          subject_type: subject_identifier.type,
        ).row

        # Groups are symbols in verdict.
        unless row.blank?
          Verdict::Assignment.new(
            experiment,
            subject_identifier,
            Verdict::Group.new(experiment, row[0]),
            row[1],
          )
        end
      end

      # Internal: Remove the given subject from the given experiment.
      def remove_assignment(experiment, subject_identifier)
        return unless experiment.saved?

        GitHub.dogstats.increment("user_experiment_enrollment.remove_assignment")

        sql = <<-SQL
        DELETE FROM
          user_experiment_enrollments
        WHERE
          user_experiment_id = :experiment_id AND
          subject_id = :subject_id AND
          subject_type = :subject_type
        SQL

        ActiveRecord::Base.connected_to(role: :writing) do
          ApplicationRecord::Domain::UserExperiments.github_sql.new(
            sql,
            experiment_id: experiment.state.id,
            subject_id: subject_identifier.id,
            subject_type: subject_identifier.type,
          ).run
        end
      end

      # Internal: Delete all assignments for this experiment. This is
      # called behind the scenes by UserExperiment#wrapup. Use with caution!
      def clear_experiment(experiment)
        return unless experiment.saved?

        GitHub.dogstats.increment("user_experiment_enrollment.clear_experiment")

        sql = <<-SQL
        DELETE FROM
          user_experiment_enrollments
        WHERE
          user_experiment_id = :experiment_id
        SQL

        ActiveRecord::Base.connected_to(role: :writing) do
          ApplicationRecord::Domain::UserExperiments.github_sql.new(sql, experiment_id: experiment.state.id).run
        end
      end

      # Internal: Retrieve the experiment's starting time.
      # Note that verdict is a good citizen, and uses UTC timestamps
      # everywhere, a behavior I'd like to preserve.
      def retrieve_start_timestamp(experiment)
        return unless experiment.saved?

        experiment.state.started_at
      end

      # Internal: Store the experiment's starting timestamp in the database.
      # Expect the incoming timestamp to be UTC; use FROM_UNIXTIME
      # because lol timezones and our MySQL is Pacific.
      def store_start_timestamp(experiment, timestamp)
        return unless experiment.saved?

        sql = <<-SQL
        UPDATE
          user_experiments
        SET
          started_at = FROM_UNIXTIME(:started_at)
        WHERE
          id = :experiment_id
        SQL

        ActiveRecord::Base.connected_to(role: :writing) do
          ApplicationRecord::Domain::UserExperiments.github_sql.new(
            sql,
            experiment_id: experiment.state.id,
            started_at: timestamp.to_i,
          ).run
        end
      end

      # Internal: find metadata for an experiment with the given slug. Raises
      # an exception if the experiment is not present.
      #
      # Return a UserExperiment::ExperimentState struct.
      def load_experiment(experiment_slug)
        sql = <<-SQL
          SELECT
            id, name, slug, started_at, finished_at, created_at, updated_at
          FROM
            user_experiments
          WHERE
            slug = :slug
        SQL

        row = ApplicationRecord::Domain::UserExperiments.github_sql.new(sql, slug: experiment_slug).row

        if row.blank?
          raise GitHub::UserResearch::ExperimentNotFound.new(experiment_slug)
        end

        UserExperiment::ExperimentState.new(*row)
      end

      # Public: Find the experiment with the given slug. If the
      # experiment is not persisted in the database, return nil.
      #
      # This method emulates ActiveRecord::Base#find.
      #
      # Returns a UserExperiment::ExperimentState struct.
      def find_experiment(slug)
        begin
          load_experiment(slug)
        rescue GitHub::UserResearch::ExperimentNotFound
          nil
        end
      end
    end
  end
end
