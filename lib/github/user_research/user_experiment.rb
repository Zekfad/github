# rubocop:disable Style/FrozenStringLiteralComment

require "zlib"

module GitHub
  module UserResearch
    # UserExperiment wraps Verdict::Experiment from Shopify's A/B testing
    # gem, adding some logic to persist and retrieve experiment metadata
    # and group assignment to tables in our MySQL database.
    #
    # Define new experiments by adding files in
    # lib/github/user_research/experiments. You'll need a unique slug
    # for your experiment, as well as a human-readable experiment name.
    # Your experiment will also need to provide logic for qualifying
    # subjects for inclusion, and specify how qualified subjects should
    # be divided into one or more test groups.
    #
    # If your experiment is going to be run for visitors (not logged in
    # Users), you'll need to define the experiment with VisitorExperiment
    # instead of this class.
    #
    # Take a look at the existing first run survey for an example.
    #
    # Before you're ready to launch and start tracking enrollment in your experiment,
    # you'll need to explicitly create a row for it in the database by
    # shipping a Transition that calls save!. If you try to enroll subjects
    # in an experiment you haven't saved yet, or otherwise query state such as
    # started_at, you'll get an exception.
    #
    # Once you have an experiment defined with all its parameters and saved
    # in the database, you can check if a subject is qualified, enroll them in the experiment,
    # and find out what group they're in in one shot, using your experiment's
    # unique slug and the switch method:
    #
    # Verdict[:my_experiment_slug].switch(subject, context)
    #
    # It will either return one of your experiment's groups
    # (e.g. :test, :control) for qualified subjects, or nil if the subject is
    # not qualified. If the subject is newly enrolled, that state will be
    # persisted in the database.
    #
    # More detail about defining experiments through verdict can
    # be found at their README:
    # https://github.com/Shopify/verdict
    class UserExperiment < Verdict::Experiment
      attr_reader :name

      CRC32_MAX     = 2 ** 32 - 1
      PERCENT_RANGE = 0..100

      class SubjectIdentifier < Struct.new(:id, :type)
        def to_s
          "#<#{self.class}: type=#{type} id=#{id}>"
        end
      end

      # Public: sets the default storage for experiments to
      # our database-backed storage.
      def initialize(handle, options = {}, &block)
        options[:storage] ||= GitHub.user_experiment_storage
        super(handle, options, &block)

        if @name.blank?
          raise ArgumentError, "You must set an experiment_name when defining an experiment."
        end
      end

      # Public: sets an experiment name; intended to be used in your
      # define block, e.g.:
      #
      # UserExperiment.define :slug do
      #   experiment_name "My awesome variance test"
      #   qualify do |subject, context|
      #     ... etc ...
      #   end
      # end
      def experiment_name(name)
        @name = name
      end

      # Public: sets the roll out percent, which represents the portion (out of
      # 100) of subjects to be considered for enrollment. In general this
      # number can be increased over time, but it should not be reduced because
      # this could cause subjects that have previously been enrolled to
      # effectively be un-enrolled, which could contaminate experiment results.
      # Note that this setting is intended to preempt `#qualify`. If a subject
      # is determined to not be rolled out, no DB interaction will occur when
      # that subject is passed to UserResearch.experiment_variant.
      #
      # A roll out percent of 75, for example, will ensure that in the limit 3
      # out of 4 potential subjects will be considered for enrollment. Note
      # that this setting has no bearing on whether the subject is further
      # disqualified by `#qualify`.
      #
      # Value should be a Numeric between 0 and 100 (inclusive). It defaults to
      # 100 (all subjects considered for enrollment). Floating point values are
      # allowed and their precision retained.
      #
      # Intended to be used in your define block, e.g.:
      #
      # UserExperiment.define :slug do
      #   roll_out_percent 75.0
      #   qualify do |subject, context|
      #     ... etc ...
      #   end
      # end
      def roll_out_percent(roll_out_percent)
        unless PERCENT_RANGE.include?(roll_out_percent)
          raise ArgumentError, "roll_out_percent must be between #{PERCENT_RANGE} (values was #{roll_out_percent.inspect}"
        end

        @roll_out_percent = roll_out_percent
      end

      # Public: determines if a subject is eligible for experiment enrollment
      #
      #
      # :qualify_once - When `true`, subject qualification is evaluated once,
      #                 persisted, and remembered thereafter. This mode is
      #                 appropriate when qualification is based on attributes
      #                 that may change over time or otherwise be
      #                 non-deterministic. It prevents subjects from switching
      #                 between qualified/non-qualified, which prevents
      #                 contamination of experiment results.
      #
      #                 When `false`, subject qualification is not persisted
      #                 and is reevaluated every time the subject is considered
      #                 for inclusion in the experiment, even if they have
      #                 previously been enrolled. This mode is appropriate only
      #                 when there is a need to explicitly control
      #                 qualification externally. For example, if you'd like to
      #                 conduct an experiment on a whitelist of subjects that
      #                 is slowly expanded over time, you could perform a
      #                 lookup in the `qualify` block such as `qualify { |s|
      #                 SubjectWhitelist.include?(s) }`. In this case, subjects
      #                 that are initially unqualified may legitimately
      #                 transition to being qualified later (because they've
      #                 been added to the whitelist). `qualify_once => false`
      #                 is appropriate in this case because we don't want to
      #                 persist this initial lack of qualification
      #                 indefinitely. Note that while it is legitimate for
      #                 subjects to transition from unqualified to qualified,
      #                 care must be taken to ensure that subjects marked as
      #                 qualified never become unqualified, as this could cause
      #                 them to be reported as belonging to an experiment arm,
      #                 but not consistently exposed to the experiment
      #                 treatment.
      #
      #                 TLDR: `qualify_once => false` should be regarded as
      #                 experiments in 'hard mode', where you need to be very
      #                 mindful of the possible qualification states your
      #                 subject may encounter over time. If you are unsure
      #                 about what to set this parameter to, leave the default
      #                 (`true`).
      #
      #                 Default: true
      #
      # Examples
      #
      #   UserExperiment.define :slug do
      #     # Reevaluated with every `UserResearch.experiment_variant` call
      #     qualify do |subject|
      #       subject.active?
      #     end
      #   end
      #
      #   UserExperiment.define :slug do
      #     # Evaluated once on the first `UserResearch.experiment_variant`
      #     # call and remembered *forever*
      #     qualify(:qualify_once => true) do |subject|
      #       subject.created_at <= 1.day.ago
      #     end
      #   end
      def qualify(qualify_once: true, &block)
        @store_unqualified = qualify_once
        super(&block)
      end

      # Internal: Verdict's default is to convert subject ids to strings,
      # but it's actually a little easier for us in practice
      # to preserve them as integers.
      def retrieve_subject_identifier(subject)
        SubjectIdentifier.new(super(subject).to_i, subject.class.name)
      end

      # Public: Persist the experiment's metadata, such as slug, in the
      # database. Raises an exception if another experiment with the same slug
      # already exists, or the storage plugin being used doesn't support
      # persisting experiment metadata.
      def save!
        @subject_storage.save_experiment(name, handle)
      end

      # Public: save experiment metadata as with save! above, but don't raise
      # an error if the experiment's slug already exists.
      def save
        begin
          save!
        rescue ActiveRecord::RecordNotUnique
          # It's ok.
        end
      end

      def save_once
        save unless saved?
      end

      # Public: Return experiment metadata, loading from the database initially,
      # then from in-memory cache unless #reload is called. Raises an exception
      # if experiment storage doesn't support persisted metadata.
      def state
        @state ||= @subject_storage.load_experiment(handle)
      end

      def saved?
        @state ||= @subject_storage.find_experiment(handle)
        !@state.nil?
      end

      # Public: clear any cached experiment metadata, and reload it from the
      # database.
      def reload
        @state = nil
        state
      end

      def subject_qualifies?(subject, context = nil)
        GitHub.user_experiments_enabled? && super
      end

      def switch(subject, context = nil)
        return nil unless GitHub.user_experiments_enabled?
        return nil unless saved?

        super(subject, context)
      end

      def legacy_anonymous_experiment!
        @legacy_anonymous_experiment = true
      end

      # Internal: Returns true if the given subject's class is permitted to be used
      # with this experiment.
      def subject_allowed?(subject)
        return true if @legacy_anonymous_experiment
        !subject.is_a?(Analytics::Visitor)
      end

      # Internal: Returns false, because this type of experiment is not allowed to
      # be handled client side.
      def metadata_allowed?
        false
      end

      # Public: boolean indicating if this subject been cleared to be evaluated
      # for inclusion in this experiment.
      def rolled_out?(subject)
        portion    = (@roll_out_percent || 100).to_f / 100

        # This identifier is composed of a static string, the experiment handle
        # and the subject identifier. The handle is neceessary to ensure that
        # the same subject (i.e. user: 123) is not always in the same relative
        # position with respect to to experiment rollout. The static identifier
        # is a paranoid protection against other code using a CRC32 of
        # handle-subject_id (which would have the potential to introduce
        # experiment bias).
        identifier = "ROLL-OUT-IDENTIFIER-#{handle}-#{retrieve_subject_identifier(subject)}"
        crc32      = Zlib.crc32(identifier.to_s)

        (crc32.to_f / CRC32_MAX) < portion
      end

      # A small struct to hold experiment metadata from the database.
      # Should I just use #values directly from GitHub::SQL instead?
      ExperimentState = Struct.new(
        :id,
        :name,
        :slug,
        :started_at,
        :finished_at,
        :created_at,
        :updated_at,
      )
    end
  end
end
