# frozen_string_literal: true
module GitHub
  module UserResearch
    # Name: MARKETPLACE_VS_APPS_NAV_BAR Experiment
    # Contributors: galessiorob, mscoutermarsh
    # URL: https://frequency.githubapp.com/experiments/marketplace_vs_apps_nav_bar
    #
    # Issue: https://github.com/github/github/issues/88910
    #
    #
    # Experiment groups:
    #   * Control - Status quo, current "Marketplace" link
    #   * apps_text - Link is "Apps" instead of "Marketplace"

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::MARKETPLACE_VS_APPS_NAV_BAR do
      experiment_name "marketplace_vs_apps_nav_bar"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 5

      # Add qualifying conditions for users. If this block evaluates falsey,
      # the subject will be excluded from the experiment.
      qualify do |user|
        !user.spammy? && !user.site_admin?
      end

      # Determine what percentage of qualifying users should be enrolled in each
      # group.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :apps_text, 50
        group :control, :rest
      end
    end
  end
end
