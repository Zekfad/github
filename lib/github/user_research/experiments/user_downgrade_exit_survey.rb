# frozen_string_literal: true

module GitHub
  module UserResearch
    # UserExperiment to measure paid user churn (downgrades)
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::USER_DOWNGRADE_EXIT_SURVEY do
      experiment_name UserExperimentCohort::USER_DOWNGRADE_EXIT_SURVEY.to_s.humanize

      qualify do |user|
        true
      end

      groups do
        group :survey, 50
        group :control, :rest
      end
    end
  end
end
