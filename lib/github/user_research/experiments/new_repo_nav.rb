# frozen_string_literal: true

module GitHub
  module UserResearch
    # UserExperiment to test how users respond to an updated repo navigation and layout.
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::NEW_REPO_NAV do
      experiment_name UserExperimentCohort::NEW_REPO_NAV.to_s.humanize

      # We want new users who aren't marked spammy.
      qualify do |user|
        !user.spammy?
      end

      groups do
        group :new_repo_nav, 5
        group :control, :rest
      end
    end
  end
end
