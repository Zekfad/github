# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module UserResearch
    # INACTIVE: https://github.com/github/github/pull/32566
    #
    # UserExperiment to see if giving the option to show display currencies has any affect on
    # paid conversion for non-US customers.
    # https://github.com/github/measurement/issues/20
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::DISPLAY_CURRENCIES_EXPERIMENT do
      experiment_name UserExperimentCohort::DISPLAY_CURRENCIES_EXPERIMENT.to_s.humanize

      qualify do |user, user_session|
        GitHub::Billing::Currency.currency_of(user_session.location[:country_code]) != "USD"
      end

      groups do
        group :test, 50
        group :control, :rest
      end
    end
  end
end
