# frozen_string_literal: true

module GitHub
  module UserResearch
    # UserExperiment to measure paid org churn (downgrades)
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::ORG_DOWNGRADE_EXIT_SURVEY do
      experiment_name UserExperimentCohort::ORG_DOWNGRADE_EXIT_SURVEY.to_s.humanize

      qualify do |user|
        true
      end

      groups do
        group :survey, 100
      end
    end
  end
end
