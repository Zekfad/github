# frozen_string_literal: true

module GitHub
  module UserResearch
    # Name: Free trial label Experiment
    # Contributors: @galessiorob, @ampinsk, @jeffrafter, @rafer
    # URL: https://frequency.githubapp.com/experiments/free_trial_label
    #
    # Marketplace is rolling out free trials for some of the integrators, we want to
    # test what's the most efficient way of communicating this and driving more
    # conversions. This test aims to define what's the best place for the "free trial"
    # labels on the plan's pricing and setup options.

    # Experiment groups:
    #   * Control - Status quo, listing plans don't indicate "free trial" explicitly.
    #   * with_label - This group will be shown a "free trial" label on the tittle.

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::FREE_TRIAL_LABEL do
      experiment_name "free_trial_label"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 100

      # Add qualifying conditions for users. If this block evaluates falsey,
      # the subject will be excluded from the experiment.
      qualify do |user|
        !user.spammy?
      end

      # Determine what percentage of qualifying users should be enrolled in each
      # group.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :with_label, 50
        group :control, :rest
      end
    end
  end
end
