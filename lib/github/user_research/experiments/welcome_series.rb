# frozen_string_literal: true

module GitHub
  module UserResearch
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::WELCOME_SERIES do
      experiment_name UserExperimentCohort::WELCOME_SERIES.to_s.humanize

      # This block should return true if the subject is qualified to participate
      qualify do |user|
        true
      end

      # Specify the groups and the percentages to be enrolled in the experiment.
      #
      # The control group should continue seeing existing functionality, while
      # the test group(s) should see the new behavior.
      groups do
        group :test, 50
        group :control, :rest
      end
    end
  end
end
