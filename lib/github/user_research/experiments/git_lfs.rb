# frozen_string_literal: true

module GitHub
  module UserResearch
    # UserExperiment to measure impact of Git LFS feature.
    # https://github.com/github/measurement/pull/59
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::GIT_LFS do
      experiment_name UserExperimentCohort::GIT_LFS.to_s.humanize

      # In the Git LFS EAP and can have an active Braintree subscription.
      qualify do |user|
        EarlyAccessMembership.lfs_waitlist.where(user_id: user.id).first
      end

      groups do
        group :test, 50
        group :control, :rest
      end
    end
  end
end
