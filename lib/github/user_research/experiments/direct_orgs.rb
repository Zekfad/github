# frozen_string_literal: true

module GitHub
  module UserResearch
    # See https://github.com/github/measurement/issues/54 for more information.
    #
    # TL;DR to test the impact of direct orgs changes, we will enable the feature
    # using a user experiment behind the scenes. It will allow us to track in the database
    # when an organization received the new functionality, roll new organizations
    # out in a controlled way, and analyze the behavior of the two groups to compare whatever
    # we'd like.
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::DIRECT_ORGS do
      experiment_name UserExperimentCohort::DIRECT_ORGS.to_s.humanize

      # The initial phase is manual enrollment.
      qualify do |org|
        false
      end

      groups do
        group :test, 0           # Direct org experience.
        group :control, :rest    # Current org experience.
      end
    end
  end
end
