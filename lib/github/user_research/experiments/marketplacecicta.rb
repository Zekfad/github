# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module UserResearch
    # Name: MarketplaceCICTA Experiment
    # Contributors: alysonla, mijuhan, rafer, jdmaturen
    # URL: https://frequency.githubapp.com/experiments/marketplacecicta
    #
    # We are trying to A/A test the Marketplace CI CTA
    #
    # Both experiment groups are shown the same variant.
    #
    # Some background on A/A tests: https://www.optimizely.com/optimization-glossary/aa-testing/

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::MARKETPLACECICTA do
      experiment_name "marketplacecicta"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 10

      # Determine what percentage of qualifying users should be enrolled in each
      # group.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :control_2, 50
        group :control, :rest
      end
    end
  end
end
