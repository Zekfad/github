# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module UserResearch
    # INACTIVE: https://github.com/github/github/pull/32569
    #
    # UserExperiment to measure affect of PayPal as an additional payment method.
    # https://github.com/github/measurement/pull/25
    GitHub::UserResearch::UserExperiment.define :paypal do
      experiment_name "Pay with PayPal"

      groups do
        group :test, 95
        group :control, :rest
      end
    end
  end
end
