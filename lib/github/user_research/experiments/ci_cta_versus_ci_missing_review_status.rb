# frozen_string_literal: true
module GitHub
  module UserResearch
    # Name: CI_CTA_VERSUS_CI_MISSING_REVIEW_STATUS Experiment
    # Contributors: galessiorob, mscoutermarsh, jeffrafter, chobberoni
    # URL: https://frequency.githubapp.com/experiments/ci_cta_versus_ci_missing_review_status
    #
    # The current CI CTA banner takes up a lot of space and looks very much like an ad
    # vertisement. Instead of utilizing this "banner" space we're testing adding gaps
    # into the interface where an integration would normally appear, and making the de
    # sign more subtle but still efficient enough to encourage users to use CI.

    # The final goal of exploring user behavior with different placings and designs is to
    #  increase CI conversion, this first experiment's aim is to set a baseline for all following iterations.
    #
    # Experiment groups:
    #   * Control - Status quo, current dismissible banner shown at the bottom of a PR.
    #   * ci_missing_cta - New non-dismissible toned-down banner shown in the middle section of merge message section in a PR page

    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::CI_CTA_VERSUS_CI_MISSING_REVIEW_STATUS do
      experiment_name "ci_cta_versus_ci_missing_review_status"

      # What percent of users should we consider for qualification? This value
      # can be increased, but should generally not be decreased.
      roll_out_percent 100

      # Add qualifying conditions for users. If this block evaluates falsey,
      # the subject will be excluded from the experiment.
      qualify do |user|
        !user.spammy?
      end

      # Determine what percentage of qualifying users should be enrolled in each
      # group.  `:rest` is a special keyword that enrolls every remaining qualifying
      # user after the other percentages have been given.
      groups do
        group :ci_missing_cta, 50
        group :control, :rest
      end
    end
  end
end
