# frozen_string_literal: true

module GitHub
  module UserResearch
    # UserExperiment to measure impact of free private repositories for
    # individuals
    # https://github.com/github/measurement/....
    GitHub::UserResearch::UserExperiment.define UserExperimentCohort::FREE_PRIVATE_REPOSITORIES do
      experiment_name UserExperimentCohort::FREE_PRIVATE_REPOSITORIES.to_s.humanize

      # User accounts must have been created at least a year ago and be on a
      # free plan.
      qualify do |user|
        !user.spammy? &&
          user.created_at <= 1.year.ago &&
          user.plan.free? &&
          user.primary_user_email.verified? &&
          !user.has_an_active_coupon?
      end

      groups do
        group :one_free, 1
        group :three_free, 1
        group :five_free, 1
        group :control, :rest
      end
    end
  end
end
