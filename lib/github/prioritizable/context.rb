# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Prioritizable::Context
    class LockedForRebalance < StandardError; end

    # Public: Have this context's contents been properly prioritized?
    #
    # If this returns false, it's because there's some bad priority data in
    # this context, and we should avoid further prioritizations so we don't
    # exacerbate the issue.
    #
    # Returns a boolean.
    def prioritized?
      return true unless priority_counter_cache

      # Ensure we're using the freshest cached value
      reload if persisted?

      # There are no items to prioritize
      return true if priority_count == 0

      # Check for associated items that haven't been prioritized
      if priority_assoc.size == read_attribute(priority_counter_cache)
        true
      else
        GitHub.dogstats.increment("#{self.class.name.underscore}.bad_priority")
        false
      end
    end

    def priority_count
      if priority_counter_cache # e.g. Milestone#open_issue_count
        read_attribute(priority_counter_cache)
      else
        priority_assoc.where.not(priority: nil).size
      end
    end

    def prioritizable?
      priority_count < GitHub::Prioritizable::MAXIMUM_PRIORITIZABLE_ITEM_COUNT
    end

    def prioritize_dependent!(dependent, before: nil, after: nil, position: nil)
      GitHub.dogstats.time("#{self.class.name.underscore}.prioritize_dependent") do
        record_to_prioritize = priority_finder(dependent: dependent).first
        if !prioritizable?
          # If this context has reached the maximum number of priorities, we only
          # want to allow moving priorities around, not adding new.
          return false unless record_to_prioritize.present?
        end
        record_to_prioritize ||= priority_assoc.new(prioritizable.to_sym => dependent)
        record_to_prioritize.reprioritize(before: before, after: after, position: position, in_context: self)
      end
    end

    def deprioritize_dependent(dependent)
      return unless dependent = priority_finder(dependent: dependent).first

      GitHub.dogstats.time("#{self.class.name.underscore}.deprioritize_dependent") do
        dependent.destroy
        notify_subscribers if respond_to?(:notify_subscribers)
      end
    end

    def lock_for_rebalance
      GitHub.kv.set(locked_for_rebalance_key, true.to_s)
      yield
    ensure
      GitHub.kv.del(locked_for_rebalance_key)
    end

    def locked_for_rebalance?
      !!GitHub.kv.get(locked_for_rebalance_key).value { false }
    end

    def rebalance_job_class
      raise NotImplementedError, "missing rebalance job: #{self.class}"
    end

    def rebalance(actor_id: GitHub.context[:actor_id])
      rebalance_job_class.perform_later(id, actor_id)
    end

    def rebalance!
      GitHub.dogstats.increment("#{self.class.name.underscore}.rebalance_count")

      lock_for_rebalance do
        GitHub.dogstats.time("#{self.class.name.underscore}.rebalance_time") do
          reversed_dependents = priority_assoc.by_priority.reverse

          # Calculate the size that the gap between each dependent's priority
          # should be.
          #
          # We should have one more gap than we have dependents, so we can have
          # a gap before the lowest priority and another after the highest
          # priority.
          #
          # We also want each gap to be as large as possible, so we have as
          # much room between it and the next dependent as possible.
          gap_size = (GitHub::Prioritizable::MAX_PRIORITY_VALUE / (reversed_dependents.size + 1)).floor

          transaction do
            # Get the dependents out of each others' ways.
            priority_assoc.update_all(priority: nil)

            reversed_dependents.each_with_index do |dependent, i|
              new_priority = gap_size * (i + 1)

              dependent.throttle do
                dependent.update_column(:priority, new_priority)
              end
            end
          end
        end
      end
    end

    def self.included(model)
      model.extend ClassMethods
    end

    private

    def priority_assoc
      association(prioritizes_with).scope
    end

    def priority_finder(dependent:)
      prioritizer_klass = association(prioritizes_with).klass

      if dependent.kind_of?(prioritizer_klass)
        return Array.wrap(dependent)
      end

      reflection = prioritizer_klass.reflect_on_association(prioritizable.to_sym)
      foreign_key = reflection.foreign_key

      options = {foreign_key => dependent.id}

      if reflection.options[:polymorphic]
        foreign_key_type = foreign_key.gsub(/_id\Z/, "_type")
        options[foreign_key_type] = dependent.class.name
      end

      priority_assoc.where(options)
    end

    def locked_for_rebalance_key
      "#{self.class.name.underscore}:locked_for_rebalance:#{id}"
    end

    module ClassMethods
      def prioritizes(dependents, with:, by: :priority, counter_cache: nil, inverse_of: nil, conditions: nil)
        define_singleton_method(:prioritizable)           { dependents.to_s.singularize }
        define_singleton_method(:prioritizes_association) { dependents }
        define_singleton_method(:prioritizes_with)        { with }
        define_singleton_method(:priority_counter_cache)  { counter_cache }

        define_method(:prioritizable)           { dependents.to_s.singularize }
        define_method(:prioritizes_association) { dependents }
        define_method(:prioritizes_with)        { with }
        define_method(:priority_counter_cache)  { counter_cache }

        if reflect_on_association(dependents)
          prioritization_class = reflect_on_association(with).klass
          association_class    = reflect_on_association(dependents).klass
          association_params   = if dependents == with
            dependents = dependents.to_s
            { foreign_key: reflections[dependents].foreign_key.to_sym }
          else
            { through: with.to_sym }
          end

          if GitHub.rails_6_0?
            association_params[:source] = prioritizable.to_sym
          else
            # :source is not valid with Rails 6.1 unless a :through option
            # is specified.
            association_params[:source] = prioritizable.to_sym if association_params[:through]
          end

          association_params[:inverse_of] = inverse_of.to_sym if inverse_of

          association_params[:class_name] = association_class.name
          query_modifier = if conditions.present?
            -> { where(conditions).order("#{prioritization_class.table_name}.#{by} DESC") }
          else
            -> { order("#{prioritization_class.table_name}.#{by} DESC") }
          end

          has_many :"prioritized_#{dependents}", query_modifier, **association_params
        end
      end
    end
  end
end
