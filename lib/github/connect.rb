# frozen_string_literal: true

require "github/connect/connect"
require "github/connect/authentication"
require "github/connect/vulnerabilities"

module GitHub
  module Connect
  end
end
