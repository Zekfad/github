# frozen_string_literal: true

module GitHub
  class EnterpriseConfigurationUpdater

    # Public: Set enterprise configuration values based on a settings Hash.
    #
    # The approach is to temporarily persist values in GitHub::KV which can be
    # used within the app while triggering a configuration run on the
    # installation to have the settings persisted in the enterprise
    # configuration system in the background.
    #
    # settings - An Hash containing the settings data to be updated. Must take
    # the following form:
    #
    # {
    #   kv_hash: {
    #     "key-one": "value-one",
    #     "key-two": "value-two"
    #   },
    #   enterprise_hash: {
    #     "enterprise": {
    #       "section": {
    #         "key_one": "value_one",
    #         "key_two": "value_two"
    #       }
    #     }
    #   }
    # }
    #
    # - The kv_hash entry is a Hash containing key value pairs to be written to
    # GitHub::KV.
    # - The enterprise_hash entry is a Hash that is valid input to the
    # PUT /setup/api/settings Management Console API endpoint.
    #
    # Returns Boolean indicating whether the configuration could be set and the
    # configuration run could be triggered.
    def update_configuration!(settings:)
      # Set the GitHub::KV values
      GitHub.kv.mset(settings[:kv_hash], expires: 1.hour.from_now)

      # Set the enterprise configuration values
      if result = GitHub.enterprise_manage_client.set_config(settings[:enterprise_hash])
        # Trigger configuration run
        result = GitHub.enterprise_manage_client.start_config_apply
      end

      result
    end

    # Public: Is there a configuration run currently in progress?
    #
    # Returns true if the Management Console API reports the configuration
    # status as "running", otherwise false.
    #
    # Returns Boolean.
    def config_run_in_progress?
      result = GitHub.enterprise_manage_client.get_config_status
      result[:status] == "running"
    end
  end
end
