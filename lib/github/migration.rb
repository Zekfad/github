# rubocop:disable Style/FrozenStringLiteralComment
# rubocop:disable GitHub/DoNotCallMethodsOnActiveRecordBase

if GitHub.enterprise?
  # ensure all the transitions are loaded
  require "github/legacy_transition"
  require "github/transition"

  transitions_dir = File.join(__FILE__, "..", "transitions")
  Dir["#{transitions_dir}/*.rb"].each do |transition|
    require "github/transitions/#{File.basename(transition, '.rb')}"
  end
end

# Public: This superclass can help you avoid fragile migrations.
class GitHub::Migration < ActiveRecord::Migration[4.2]
  extend GitHub::SafeDatabaseMigrationHelper

  def self.multi_db_enabled?
    !GitHub.enterprise?
  end

  def self.connection_specification_name=(name)
    return unless multi_db_enabled?
    @spec_name = name
  end

  def self.connection_specification_name
    @spec_name
  end

  def migrate(direction)
    if self.class.connection_specification_name
      old_pool = ActiveRecord::Base.connection_handler.retrieve_connection_pool(ActiveRecord::Base.connection_specification_name)

      conn = self.class.connection_specification_name.connection
      if GitHub.rails_6_0?
        config = conn.pool.spec.config
      else
        config = conn.pool.db_config
      end

      ActiveRecord::Base.establish_connection(config)
    end

    super
  ensure
    if old_pool
      if GitHub.rails_6_0?
        ActiveRecord::Base.connection_handler.establish_connection(old_pool.spec.config)
      else
        ActiveRecord::Base.connection_handler.establish_connection(old_pool.db_config)
      end
    end
  end

  def self.use_connection_class(klass)
    self.connection_specification_name = klass
  end
end
