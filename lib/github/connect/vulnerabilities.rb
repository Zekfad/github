# frozen_string_literal: true
require "graphql/client"
require "graphql/client/http"

module GitHub
  module Connect
    class VulnerabilityFetcher < Authenticator
      # @return [Integer, nil] id of the last Vulnerability returned
      attr_accessor :last_vulnerability_id

      # @return [Integer, nil] max number of Vulnerabilities to fetch
      attr_accessor :count

      class APIRequestError < StandardError
      end

      # @return [Boolean] true if it has previously encountered an empty page or results or has already returned count
      #   Vulnerabilities
      def has_next_page
        !@done
      end

      # Gets the next page of Vulnerabilities from /enterprise-installation/vulnerabilities
      #
      # @raise [ConnectionError] when something is wrong with the underlying Faraday connection
      # @raise [APIRequestError] when the api response has a non-200 status
      # @return [Array<Vulnerability>] one page of Vulnerabilities
      def next_page
        return [] unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          uri = URI("/enterprise-installation/vulnerabilities")
          params = {before: last_vulnerability_id, count: fetch_count}.compact
          uri.query = params.to_query unless params.empty?
          enterprise_installation_api(uri.to_s, nil, GitHub::Connect.auth_headers, :get)
        end
        raise APIRequestError unless response.status == 200
        unmarshal_vulnerabilities(response.body).tap do |result|
          if result.empty?
            @done = true
          else
            self.last_vulnerability_id = result.last.id
          end
          @fetched_count = @fetched_count.to_i + result.size
        end
      end

      private

      def system_user
        @system_user ||= User.find_by(login: GitHub.trusted_oauth_apps_org_name)
      end

      def fetch_count
        count.nil? ? count : count - @fetched_count.to_i
      end

      # Unmarshals an array of Vulnerabilities from json returned by /enterprise-installation/vulnerabilities
      #
      # created_by is set to the system user(github-enterprise) because the real create_by user won't likely be present
      #   on an enterprise installation.
      #
      # @raise [ConnectionError] when json can't be parsed.  (see #json_parse)
      # @param [Object] json [String]
      # @return [Array<Vulnerability>] Vulnerabilities from json
      def unmarshal_vulnerabilities(json)
        vuln_hash = json_parse(json, symbolize_keys: true) || []
        vuln_hash.map do |vuln|
          vuln[:identifier] = vuln[:external_identifier]

          Vulnerability.new(
            id: vuln[:id],
            ghsa_id: vuln[:ghsa_id],
            cve_id: vuln[:cve_id],
            white_source_id: vuln[:white_source_id],
            classification: vuln[:classification],
            description: vuln[:description],
            external_reference: vuln[:external_reference],
            platform: vuln[:platform],
            severity: vuln[:severity],
            status: vuln[:status],
            identifier: vuln[:identifier],
            published_at: vuln[:published_at],
            created_at: vuln[:created_at],
            withdrawn_at: vuln[:withdrawn_at],
            simulation: false,
            created_by: system_user,
            vulnerable_version_ranges: vuln[:vulnerable_version_ranges].map do |range|
              VulnerableVersionRange.new(
                vulnerability_id: vuln[:id],
                id: range[:id],
                affects: range[:affects],
                ecosystem: range[:ecosystem],
                fixed_in: range[:fixed_in],
                requirements: range[:requirements],
              )
            end,
          )
        end
      end
    end
  end
end
