# rubocop:disable Style/FrozenStringLiteralComment

require "enterprise/crypto"
require "rubygems/package"
require "rbnacl"
require "github/areas_of_responsibility"

module GitHub
  module Connect
    class Authenticator
      class AuthenticationError < StandardError
      end

      class ConnectionError < StandardError
      end

      class LicenseLoadingError < StandardError
      end

      LICENSE_KEY_PATH = "#{Rails.root}/enterprise/license/gpg/pubring.gpg".freeze
      ENTERPRISE_LICENSE_KEY_PATH = "/data/enterprise/license.gpg".freeze
      CUSTOMER_KEY_PATH = "#{Rails.root}/enterprise/customer/gpg/pubring.gpg".freeze
      ENTERPRISE_CUSTOMER_KEY_PATH = "/data/enterprise/customer.gpg".freeze
      CUSTOMER_SECRET_KEY_PATH = "#{Rails.root}/enterprise/customer/gpg/secring.gpg".freeze
      OAUTH_SCOPES = ["repo", "user"]
      attr_accessor :license_file_path, :public_key, :private_key

      def self.license_key_data
        File.exist?(ENTERPRISE_LICENSE_KEY_PATH) ? File.read(ENTERPRISE_LICENSE_KEY_PATH) : File.read(LICENSE_KEY_PATH)
      end

      def self.license_vault
        @license_vault ||= begin
          vault = ::Enterprise::Crypto::LicenseVault.new(license_key_data)
          vault.load_vault
          vault
        end
      end

      def self.customer_key_data
        File.exist?(ENTERPRISE_CUSTOMER_KEY_PATH) ? File.read(ENTERPRISE_CUSTOMER_KEY_PATH) : File.read(CUSTOMER_KEY_PATH)
      end

      def self.customer_secret_key_data
        File.exist?(CUSTOMER_SECRET_KEY_PATH) ? File.read(CUSTOMER_SECRET_KEY_PATH) : nil
      end

      def self.customer_vault
        @customer_vault ||= begin
          vault = ::Enterprise::Crypto::CustomerVault.new(customer_secret_key_data, customer_key_data)
          vault.load_vault
          vault
        end
      end

      def self.load_vaults
        license_vault && customer_vault
      end

      def generate_keys
        self.private_key = OpenSSL::PKey::RSA.new(IntegrationKey::KEY_LENGTH)
        self.public_key = self.private_key.public_key.to_pem
        return self.private_key.to_pem
      end

      def authentication_request_info
        return nil unless GitHub.enterprise?
        connection = DotcomConnection.new
        generate_keys unless self.public_key.present?
        return {
          server_id: connection.server_id,
          license: Base64.encode64(File.read(self.license_file_path)),
          host_name: GitHub.host_name,
          http_only: !GitHub.ssl?,
          version: GitHub.version_number,
          public_key: self.public_key && Base64.encode64(self.public_key),
        }
      end

      def authentication_request_data
        return nil unless GitHub.enterprise?
        authentication_request_info.to_json
      end

      def request_authentication_token
        return nil unless GitHub.enterprise?
        response = enterprise_installation_api("/enterprise-installation", authentication_request_data)
        return json_parse(response.body)["token"]
      end

      def update_installation_info(token)
        return nil unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          enterprise_installation_api("/enterprise-installation", authentication_request_data, GitHub::Connect.auth_headers, :put)
        end
        return json_parse(response.body)["token"]
      end

      def remove_enterprise_installation
        return nil unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          enterprise_installation_api("/enterprise-installation", nil, GitHub::Connect.auth_headers, :delete)
        end
        GitHub::Connect.report_failure(ApiError.new("Error: '#{response.status} #{response.body}'"), "github-connect-connection") if response.status != 204
        response.status == 204
      end

      def request_permissions(features)
        return nil unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          enterprise_installation_api("/enterprise-installation/permissions", {features: features}.to_json, GitHub::Connect.auth_headers, :post)
        end
        GitHub::Connect.report_failure(ApiError.new("Error: '#{response.status} #{response.body}'"), "github-connect-connection") unless response.success?
        json_parse(response.body).merge("success" => response.success?)
      end

      def remove_all_contributions
        return nil unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          enterprise_installation_api("/enterprise-installation/contributions", nil, GitHub::Connect.auth_headers, :delete)
        end
        GitHub::Connect.report_failure(ApiError.new("Error: '#{response.status} #{response.body}'"), "github-connect-connection") unless response.success?
        response.success?
      end

      def request_oauth_application
        return nil unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          enterprise_installation_api("/enterprise-installation/application", nil, GitHub::Connect.auth_headers, :get)
        end
        return json_parse(response.body)
      end

      def request_oauth_token(code, oauth)
        return nil unless GitHub.enterprise?
        body = oauth.merge({code: code, accept: :json}).to_json
        response = dotcom_request("/login/oauth/access_token", body)

        Rack::Utils.parse_nested_query(response.body)["access_token"]
      rescue Rack::QueryParser::InvalidParameterError
        GitHub::Connect.report_failure(ApiError.new("Parser Error: '#{response.body}'"), "github-connect-connection", "parser")
        # An invalid JSON from dotcom shoud be treated as a connection error
        # (it is often a proxy server error message or something to that effect)
        raise ConnectionError
      end

      def revoke_oauth_token(token, oauth)
        return nil unless GitHub.enterprise?
        response = enterprise_installation_api("/applications/#{oauth["client_id"]}/tokens/#{token}", nil, {}, :delete, [oauth["client_id"], oauth["client_secret"]])
        GitHub::Connect.report_failure(ApiError.new("Error: '#{response.status} #{response.body}'"), "github-connect-connection") if response.status != 204
        response.status == 204
      end

      def request_oauth_user_info(token)
        response = enterprise_installation_api("/user", nil, {"Authorization" => "token #{token}"}, :get)
        data = json_parse(response.body)
      end

      def upload_license_usage(business_id, data)
        return nil unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          enterprise_installation_api("/businesses/#{business_id}/user-accounts-uploads?name=users.json", data, GitHub::Connect.auth_headers, :post, nil, GitHub.dotcom_upload_host_name)
        end
        GitHub::Connect.report_failure(ApiError.new("Error: '#{response.status} #{response.body}'"), "github-connect-connection") unless [200, 201].include?(response.status)
        return json_parse(response.body)
      end

      def complete_license_info_upload(business_id, upload_id)
        return nil unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          enterprise_installation_api("/businesses/#{business_id}/user-accounts-uploads/#{upload_id}/sync", nil, GitHub::Connect.auth_headers, :patch)
        end
        GitHub::Connect.report_failure(ApiError.new("Error: '#{response.status} #{response.body}'"), "github-connect-connection") if response.status != 202
        return json_parse(response.body)
      end

      def license_usage_upload_info(business_id, upload_id)
        return nil unless GitHub.enterprise?
        response = GitHub::Connect.github_app_authenticated do
          enterprise_installation_api("/businesses/#{business_id}/user-accounts-uploads/#{upload_id}", nil, GitHub::Connect.auth_headers, :get)
        end
        GitHub::Connect.report_failure(ApiError.new("Error: '#{response.status} #{response.body}'"), "github-connect-connection") unless [200, 201].include?(response.status)
        return json_parse(response.body)
      end

      def load_license(data)
        @license ||= begin
          license_vault = self.class.license_vault
          customer_vault = self.class.customer_vault

          ctx = GPGME::Ctx.new(license_vault.default_context_options)
          verified = ctx.verify(GPGME::Data.new(data))
          signatures = ctx.verify_result.signatures

          if signatures.all? { |sig| sig.valid? }

            license, public_data, secret_data = ::Enterprise::Crypto::License.new, nil, nil

            Gem::Package::TarReader.new StringIO.new(verified.to_s) do |tar|
              tar.each do |file|
                case file.full_name
                when "metadata.json"
                  license.metadata = JSON.parse(file.read)
                when "gpg/pubring.gpg"
                  public_data = file.read
                when "gpg/secring.gpg"
                  secret_data = file.read
                end
              end
            end

            unless secret_data && public_data && license.metadata
              missing = []
              missing << "metadata.json" unless license.metadata
              missing << "gpg/pubring.gpg" unless public_data
              missing << "gpg/secring.gpg" unless secret_data
              error = LicenseLoadingError.new "License data is missing #{missing.join(", ")}"
              Failbot.report error
              return nil
            end

            fingerprint = customer_vault.import_key(secret_data)
            public_key = ctx.get_key(customer_vault.import_key(public_data), false)
            customer_public_key = ctx.get_key(customer_vault.fingerprint, false)

            ::Enterprise::Crypto::Vault.verify_key_fingerprint!(public_key, fingerprint)
            ::Enterprise::Crypto::Vault.verify_key_signature!(public_key, customer_public_key)

            key = ctx.get_key(fingerprint, true)

            license.customer = ::Enterprise::Crypto::Customer.new(key.name, key.email, key.comment, secret_data, public_data)
            license
          end
        rescue ::Enterprise::Crypto::Error, GPGME::Error => error
          Failbot.report error
          nil
        end
      end

      def valid_license?(data)
        license = load_license(data)
        license && (license.perpetual? || license.expire_at > DateTime.now)
      end

      def encryption_pw(password)
        "ghe-#{password}-encrypted"
      end

      def encrypt_message(message, password)
        ::Enterprise::Crypto.license_vault = self.class.license_vault
        ::Enterprise::Crypto.customer_vault = self.class.customer_vault
        license = ::Enterprise::Crypto::License.load(File.read(license_file_path))
        with_context(armor: true, password: encryption_pw(password), pinentry_mode: GPGME::PINENTRY_MODE_LOOPBACK) do |context|
          raw_data = GPGME::Data.new(message)
          vault = ::Enterprise::Crypto::Vault.new
          context.add_signer(GPGME::Data.new(vault.import_key(license.customer_private_key)))
          encrypted_data = context.encrypt_sign(nil, raw_data)
          encrypted_data.seek(0)
          encrypted_data.read
        end
      end

      def decrypt_message(message, password)
        with_context(password: encryption_pw(password), pinentry_mode: GPGME::PINENTRY_MODE_LOOPBACK) do |context|
          vault = ::Enterprise::Crypto::Vault.new
          signed_data = vault.new_signed_data(message)

          context.import_keys(GPGME::Data.new(vault.import_key(self.class.customer_secret_key_data)))
          decrypted_data = context.decrypt_verify(signed_data)
          vault.check_signatures(context.verify_result.signatures)
          decrypted_data.seek(0)
          decrypted_data.read
        end
      end

      def encrypt_message_v2(message)
        box = RbNaCl::Boxes::Sealed.from_public_key(Base64.decode64(GitHub.enterprise_user_license_list_public_key))
        Base64.encode64 box.box(message)
      end

      def decrypt_message_v2(message)
        box = RbNaCl::Boxes::Sealed.from_private_key(Base64.decode64(GitHub.enterprise_user_license_list_private_key))
        box.open Base64.decode64(message)
      end

      def with_context(options = {})
        vault = ::Enterprise::Crypto::Vault.new
        GPGME::Ctx.new(vault.default_context_options.merge(options)) do |ctx|
          return yield(ctx)
        end
      end

      def dotcom_request(path, body, headers = {}, method = :post)
        hostname = GitHub.dotcom_host_name
        if hostname.ends_with?(".review-lab.github.com")
          # Review labs don't accept non-API dotcom requests without the
          # staffonly cookie, so we'll route these through dotcom
          hostname = "github.com"
          Rails.logger.warn "#{method} #{path} called via #{hostname} instead of #{GitHub.dotcom_host_name}"
        end
        conn = GitHub::Connect.faraday_connection(hostname)
        conn.send(method) do |req|
          GitHub::Connect.github_connect_request(req, path, headers, body, api: false)
        end
      end

      def enterprise_installation_api(path, body, headers = {}, method = :post, basic_auth = nil, host = nil)
        conn = host ? GitHub::Connect.faraday_connection(host) : GitHub::Connect.faraday_connection
        conn.basic_auth(basic_auth[0], basic_auth[1]) if basic_auth
        response = conn.send(method) do |req|
          GitHub::Connect.github_connect_request(req, path, headers, body, github_connect_accept_type: true)
        end
        if response.status == 403
          GitHub::Connect.report_failure(ApiError.new("Error: '#{response.status} #{response.body}'"), "github-connect-connection")
          raise AuthenticationError
        end
        response
      rescue Timeout::Error, Faraday::TimeoutError, Net::OpenTimeout => e
        GitHub::Connect.report_failure(TimeoutError.new(e.message), "github-connect-connection", "timeout")
        raise ConnectionError
      rescue Faraday::Error, Errno::EPIPE, Faraday::ConnectionFailed
        GitHub::Connect.report_failure(ServiceUnavailableError.new("API Inaccessible"), "github-connect-connection")
        raise ConnectionError
      end

      private

      def json_parse(json, options = nil)
        GitHub::JSON.parse(json, options)
      rescue Yajl::ParseError
        GitHub::Connect.report_failure(ApiError.new("Parser Error: '#{json}'"), "github-connect-connection", "parser")
        # An invalid JSON from dotcom shoud be treated as a connection error
        # (it is often a proxy server error message or something to that effect)
        raise ConnectionError
      end
    end
  end
end
