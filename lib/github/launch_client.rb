# frozen_string_literal: true

module GitHub
  module LaunchClient
    Error = Class.new(RuntimeError)
    ServiceUnavailable = Class.new(Error)

    def self.request_metadata
      # Note that keys must be lowercased.
      {
        "x-github-request-id": GitHub.context[:request_id] || "",
      }
    end
  end
end
