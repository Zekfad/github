# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  # This is similar to ActiveRecord's built-in find_each facility, except that
  # it supports chunking on any column - not just the primary key column.
  #
  # It's useful in cases where you want to perform filtering in Ruby land but
  # aren't sure how many records you need to load up.
  #
  # Usage example:
  #
  #   GitHub::SQLCursor.new(
  #     Push.where(:user_id => user_id, :repository_id => repo_id)
  #         .order("created_at DESC")
  #   ).select { |push|
  #     ref_exists?(push.ref)
  #   }.take(10).to_a
  #
  # This will execute series of queries that look like:
  #
  #   SELECT * FROM pushes WHERE user_id = ? AND repository_id = ? ORDER BY created_at DESC LIMIT 50
  #   SELECT * FROM pushes WHERE user_id = ? AND repository_id = ? WHERE created_at > ? ORDER BY created_at DESC LIMIT 100
  #   SELECT * FROM pushes WHERE user_id = ? AND repository_id = ? WHERE created_at > ? ORDER BY created_at DESC LIMIT 200
  #
  # etc. cursoring through the table as necessary.
  #
  # Caveats:
  #
  # - Before using this class with a query, make sure there are good indexes to
  #   support these kinds of queries. If in doubt, hit up @github/database-infrastructure and @github/platform-data and
  #   someone will be happy to assist.
  #
  # - This is not guaranteed to return correct results (some result rows will
  #   be skipped) if your order column is not unique.
  #
  class SQLCursor
    attr_reader :relation, :order_column, :order_direction, :chunk_size, :max_chunk_size

    def initialize(relation, chunk_size: 50, max_chunk_size: 1000)
      @relation = relation
      @chunk_size = chunk_size
      @max_chunk_size = max_chunk_size

      extract_order!(relation)
    end

    def each_row(&block)
      return enum_for(:each_row).lazy unless block_given?

      result_set = select_chunk
      result_set.each(&block)

      while result_set.count == chunk_size
        bound = result_set.last[order_column]
        @chunk_size = [chunk_size * 2, max_chunk_size].min
        result_set = select_chunk(bound: bound)
        result_set.each(&block)
      end
    end

    def inspect
      "#<#{self.class}>"
    end

  private

    def extract_order!(relation)
      if relation.order_values.empty?
        raise "Relation passed to SQLCursor is unordered, please explicitly order it."
      end

      if relation.order_values.length > 1
        raise "Sorry, multiple order columns are not supported."
      end

      unless /\A`?([a-z_0-9]+)`? (ASC|DESC)\z/i =~ relation.order_values[0]
        raise "Could not extract order from relation."
      end

      @order_column = $1
      @order_direction = $2.downcase.intern

      relation.order_values.clear
    end

    def select_chunk(opts = {})
      rel = relation.order("#{order_column} #{order_direction}")

      if opts.key?(:bound)
        sign = order_direction == :desc ? "<" : ">"
        rel = rel.where("#{order_column} #{sign} ?", opts[:bound])
        rel = rel.offset(0)
      end

      rel.limit(chunk_size).to_a
    end
  end
end
