# frozen_string_literal: true

module GitHub
  module FaradayMiddleware
    # Faraday Middleware for exposing timings from external servcies in
    # the staffbar
    class Staffbar < Faraday::Middleware
      GITHUB_INCLUDE_TIMINGS_HEADER = "X-GitHub-Staffbar-Include-Timings"
      GITHUB_TIMINGS_HEADER = "X-GitHub-Staffbar-Timings"

      attr_reader :url

      def initialize(app, options = {})
        super(app)
        @url = options[:url]
      end

      def call(env)
        if GitHub::MysqlInstrumenter.tracking?
          env.request_headers[GITHUB_INCLUDE_TIMINGS_HEADER] = "1"
        end

        res = @app.call(env)
        track_queries(res.headers[GITHUB_TIMINGS_HEADER])
        res
      end

      private

      def track_queries(payload)
        payload = decode_payload(payload)
        queries = Array(payload[:queries])

        return unless queries.present?

        start = Time.now
        queries.each do |timing|
          finish = Time.at(start.to_f + (timing[:duration] / 1_000_000_000.0))
          GitHub::MysqlInstrumenter.track_query(
            timing[:query],
            ResultCount.new(timing[:results]),
            connection_info,
            time_span: TimeSpan.new(start, finish),
          )
          start = finish
        end
      end

      def decode_payload(payload)
        return {} unless payload.present?

        begin
          payload = Base64.strict_decode64(payload)
        rescue ArgumentError
          return {}
        end

        return {} unless payload.present?

        begin
          GitHub::JSON.decode(payload).with_indifferent_access
        rescue Yajl::ParseError
          {}
        end
      end

      def connection_info
        @connection_info ||= ConnectionInfo.new(
          url: url,
          host: Addressable::URI.parse(url).host,
        )
      end

      class ResultCount
        attr_reader :count

        def initialize(count)
          @count = [count, 0].max
        end
      end
    end
  end
end
