# frozen_string_literal: true

require "github/token_scanning/found_token"
require "github/token_scanning/token_scanning_post_processing_helper"

require "github/token_scanning/processor"
require "github/token_scanning/processor/chain_processor"
require "github/token_scanning/processor/github_oauth_token_processor"
require "github/token_scanning/processor/github_ssh_private_key_processor"
require "github/token_scanning/processor/third_party_token_processor"
require "github/token_scanning/processor/multi_type_token_processor"
require "github/token_scanning/processor/amazon_aws_token_processor"
require "github/token_scanning/processor/datadog_token_processor"
require "github/token_scanning/processor/alicloud_token_processor"
require "github/token_scanning/processor/hubspot_token_processor"
require "github/token_scanning/processor/plivo_token_processor"
require "github/token_scanning/processor/microsoft_pat_token_processor"
require "github/token_scanning/processor/microsoft_cred_token_processor"
require "github/token_scanning/processor/messagebird_token_processor"

require "github/token_scanning/service/client"

module GitHub
  module TokenScanning
  end
end
