# rubocop:disable Style/FrozenStringLiteralComment

require "nokogiri"

module GitHub
  module Science
    class HTMLCleaner
      def initialize(root_selector)
        @root_selector = root_selector
      end

      def to_proc
        proc { |x| clean(x) }
      end

      def clean(html)
        doc = Nokogiri::HTML::DocumentFragment.parse(html)

        root = doc.css(@root_selector).first

        clean_authenticity_tokens(root)

        buffer = ""
        render_node(root, buffer)
        buffer
      end

    private
      def clean_authenticity_tokens(el)
        el.css("input[name=authenticity_token]").each do |node|
          node.attribute("value").value = "?"
        end
      end

      def render_node(node, buffer)
        case node
        when Nokogiri::XML::NodeSet
          node.each do |child|
            render_node(child, buffer)
          end

        when Nokogiri::HTML::DocumentFragment
          render_node(node.children, buffer)

        when Nokogiri::XML::Element
          buffer << "<".freeze << node.name

          node.attributes.sort_by { |name, _| name }.each_value do |attribute|
            buffer << attribute.to_html
          end

          buffer << ">\n".freeze

          unless node.attribute("data-html-cleaner-suppress-children")
            render_node(node.children, buffer)
          end

          buffer << "</".freeze << node.name << ">\n".freeze

        when Nokogiri::XML::Text
          cleaned_text = node.text.gsub(/\s+/, " ".freeze).strip

          if !cleaned_text.empty?
            buffer << cleaned_text << "\n".freeze
          end

        when Nokogiri::XML::Comment
          # ignore

        else
          raise TypeError, "Unknown type #{a.class}"
        end
      end
    end
  end
end
