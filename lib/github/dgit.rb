# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/gitrpc"
require "gitrpc/protocol/dgit"
require "github/dgit/constants"
require "github/dgit/error"
require "github/dgit/host_picker"
require "github/dgit/repo_types"
require "github/dgit/states"
require "github/config/memcache"
require "github/dns"

module GitHub
  class DGit
    autoload :Delegate,               "github/dgit/delegate"
    autoload :Maintenance,            "github/dgit/maintenance"
    autoload :Routing,                "github/dgit/routing"
    autoload :ThreePhaseCommitClient, "github/dgit/three_phase_commit_client"
    autoload :Rebalancer,             "github/dgit/rebalancer"
    autoload :Replica3PCClient,       "github/dgit/replica_3pc_client"
    autoload :Util,                   "github/dgit/util"
    autoload :ReadWeightAllocator,    "github/dgit/read_weight_allocator"

    # models
    autoload :Fileserver,             "github/dgit/fileserver"
    autoload :Replica,                "github/dgit/replica"
    autoload :Route,                  "github/dgit/route"

    # connection management
    autoload :DB,                     "github/dgit/sql"
    autoload :SQL,                    "github/dgit/sql"

    # DGit routes in dev are stored in paths like
    #   repositories/development/dgit1/e/nw/ec/cb/c8/3/37.git
    # instead of
    #   repositories/development/e/nw/ec/cb/c8/3/37.git
    def self.dev_route(path, host)
      raise "dev_route called for already dev-mapped path, #{path}" if path =~ /dgit\d/
      if path.start_with?(GitHub.repository_root)
        suffix = path[GitHub.repository_root.length..-1]
        "#{GitHub.repository_root}/#{host}#{suffix}"
      else
        path
      end
    end

    def self.rpc_for_host_path(host, path)
      GitHub::Logger.log(reason: "pr63738".freeze, fn: "rpc_for_host_path".freeze, host: host)
      if host == GitHub.local_git_host_name
        ::GitRPC.new("file:#{path}")
      elsif Rails.development? || Rails.test?
        ::GitRPC.new("fakerpc:#{GitHub::DGit::dev_route(path, host)}")
      else
        ::GitRPC.new("bertrpc://#{host}#{path}")
      end
    end

    # Return an object that can be used to commit a reference update.
    def self.update_refs_coordinator(repository, priority: :high) # FIXME: un-AR this interface, consume NWO + Delegate directly.
      options = {priority: priority}
      options[:coalesce] = true if repository.coalesce_dgit_updates?
      if request_id = GitHub.context[:request_id]
        options[:request_id] = request_id
      end
      actor = repository.is_a?(Unsullied::Wiki) ? repository.repository : repository
      options[:threepc_http] = GitHub.flipper[:threepc_http].enabled?(actor)
      ThreePhaseCommitClient.new(repository.full_name, repository.dgit_delegate, **options)
    end

    def self.with_dgit_lock(repository, checksum_host = :vote, &block)
      tpc = update_refs_coordinator(repository)
      tpc.with_dgit_lock(block, checksum_host)
    end

    # Get all names of fileservers that meet the given criteria.
    def self.get_hosts(**options)
      get_fileservers_rows(**options).map(&:first)
    end

    # Get all hosts that meet the given criteria as Fileserver instances.
    def self.get_fileservers(**options)
      get_fileservers_rows(**options).map { |row|
        Fileserver.new \
          name: row[0],
          ip: row[5],
          datacenter: row[1] || GitHub.default_datacenter,
          rack: row[2] || GitHub.default_rack,
          site: row[9],
          online: row[6] == 1,
          embargoed: row[3] == 1,
          evacuating: row[4] == 1,
          voting: row[7] != 1,
          hdd_storage: row[8] == 1
      }
    end

    # Internal.
    def self.get_fileservers_rows(online_only: true, exclude_embargoed: false, in_datacenters: nil, voting_only: false, storage_class: :ssd)
      raise ArgumentError unless [:hdd, :ssd, :all].include?(storage_class)
      in_datacenters = in_datacenters && Array(in_datacenters)
      in_datacenters_key = in_datacenters ? in_datacenters.join(",") : "any"
      GitHub.cache.fetch("dgit.hosts.v4.#{in_datacenters_key}.#{online_only}.#{exclude_embargoed}.#{voting_only}.#{storage_class}", ttl: 1.minute) do
        sql = GitHub::DGit::DB::FS::SQL.new <<-SQL
          SELECT host, datacenter, rack, embargoed, evacuating, ip, online, non_voting, hdd_storage, site
          FROM fileservers
            WHERE host != 'localhost'
        SQL

        sql.add "AND online = 1" if online_only
        sql.add "AND embargoed = 0 AND evacuating = 0" if exclude_embargoed
        sql.add "AND datacenter IN :datacenters", datacenters: in_datacenters if in_datacenters
        sql.add "AND non_voting = :non_voting", non_voting: 0 if voting_only
        sql.add "AND hdd_storage = 1 " if storage_class == :hdd
        sql.add "AND hdd_storage = 0 " if storage_class == :ssd

        unless GitHub.enterprise?
          pattern = (Rails.development? || Rails.test?) ? "dgit%" : "github-dfs%"
          sql.add <<-SQL, pattern: pattern
            AND host LIKE :pattern
          SQL
        end

        sql.results
      end
    end   # self.get_fileservers_rows

    # Get the fraction of disk space available to repositories.
    # This is:
    #              free_disk_space - slack_disk_space
    #   F = max(0, ----------------------------------- )
    #              total_disk_space - slack_disk_space
    # If we're calling this to place a repository of known size, then
    # subtract that size from the numerator, too.  The idea here is to get
    # a normalized measure of how good an idea it is to move repos onto
    # (or off of) a given host.
    #
    # When F is 0, we need to clear off space.  The higher F is, the more
    # underutilized the disk is, and the more repos we should move onto
    # it.
    #
    # Returns an array with two elements:
    #   - a float between 0.0 and 1.0, the fraction of space available
    #   - the absolute size of the disk in MB
    #
    # If the call fails, return [0, 1] so the host can only be chosen if
    # everyone else also returns zero.
    #
    # Don't run this on a traditional github-fs, where there are 16
    # separate partitions.
    #
    # Optionally accept `rpc_builder`, which is a proc to create a GitRPC
    # object that can talk to the fileserver.
    def self.disk_stats(host, rpc_builder:)
      GitHub.cache.fetch(disk_stats_cache_key(host), ttl: disk_stats_cache_ttl) do
        rpc = rpc_builder.call(GitHub.repository_root)
        interpret_disk_free_space(rpc.disk_free_space)
      end
    rescue SocketError, ::GitRPC::Error
      [0, 1]
    end   # self.disk_stats

    class ParallelDiskStatFileserver
      def initialize(fileserver)
        @fileserver = fileserver
      end

      attr_reader :fileserver

      def cache_key
        @cache_key ||= GitHub::DGit.disk_stats_cache_key(fileserver.name)
      end

      def error_cache_key
        @error_cache_key ||= GitHub::DGit.disk_stats_error_cache_key(fileserver.name)
      end

      def rpc_url
        @rpc_url ||= fileserver.to_route(GitHub.repository_root).rpc_url
      end

      attr_accessor :result
    end

    # Public: Get disk stats for a set of fileservers via GitRPC
    #
    # This does not look for stats in memcache. It does cache any results received.
    def self.fill_disk_stats_cache(fileservers: GitHub::DGit.get_fileservers)
      _parallel_disk_stats(fileservers.map { |fs| ParallelDiskStatFileserver.new(fs) })
    end

    # Public: Get disk stats for a set of fileservers
    #
    # Try to load disk stats from cache. (Good results and errors are cached.) If
    # there are any servers that don't have cached values, query them directly via
    # GitRPC.
    def self.parallel_disk_stats(fileservers)
      fileserver_disk_stats = fileservers.map { |fs| ParallelDiskStatFileserver.new(fs) }

      # Check in memcached for cached results and errors.
      cached = GitHub.cache.get_multi(fileserver_disk_stats.map(&:cache_key) + fileserver_disk_stats.map(&:error_cache_key))
      need_rpc_call = []
      fileserver_disk_stats.each do |fs|
        if val = cached[fs.cache_key]
          fs.result = val
        elsif !cached[fs.error_cache_key]
          need_rpc_call << fs
        end
      end

      # Load the rest via gitrpc.
      unless need_rpc_call.empty?
        _parallel_disk_stats(need_rpc_call)
      end

      fileserver_disk_stats.map { |fs| [fs.fileserver, fs.result || [0, 1]] }
    end

    # Internal: Request disk stats values in parallel
    #
    # fileserver_disk_stats must be an Array of ParallelDiskStatFileserver objects,
    # or an enumerable object whose elements respond to `rpc_url` and have a read/write
    # attribute `result`.
    def self._parallel_disk_stats(fileserver_disk_stats)
      GitHub.dogstats.time("dgit.parallel_disk_stats") do
        answers, errors = ::GitRPC.send_multiple(fileserver_disk_stats.map(&:rpc_url), :disk_free_space, [], timeout: 5, connect_timeout: 3)
        fileserver_disk_stats.each do |fs|
          if answer = answers[fs.rpc_url]
            fs.result = interpret_disk_free_space(answer)
            GitHub.cache.set(fs.cache_key, fs.result, disk_stats_cache_ttl)
          elsif error = errors[fs.rpc_url]
            # Let this fall through and get the default value, which will avoid another failed attempt to query disk_stats.
            # Also report it so that systematic problems will get noticed and fixed.
            Failbot.report! error, app: "github-dgit-debug"
            GitHub.cache.set(fs.error_cache_key, true, disk_stats_cache_ttl)
          end
        end
      end
    end

    # Internal. Parses the output of a GitRPC :disk_free_space call.
    def self.interpret_disk_free_space(freeinfo)
      free_space_mb, total_space_mb = freeinfo
      slack_space_mb = GitHub.shard_slack_space / 1024
      f = (free_space_mb - slack_space_mb).to_f / (total_space_mb - slack_space_mb)
      [[0, f].max, total_space_mb]
    end

    # Internal.
    def self.disk_stats_cache_ttl
      GitHub.dgit_disk_stats_cache_ttl
    end

    # Internal.
    def self.disk_stats_cache_key(host)
      "dgit.disk_stats.#{host}"
    end

    # Internal.
    def self.disk_stats_error_cache_key(host)
      "dgit.disk_stats_error.#{host}"
    end

    # Pick hosts on which to place a replica.
    #   count:      the number of hosts to pick.  This would be 1 if creating a
    #               single replica, or DGIT_COPIES if creating or importing a
    #               repo.
    #   exclusions: hosts not to pick.  For example, when evicting a
    #               replica from a host, put that host in this list.  When
    #               creating a new replica for a network, put the existing
    #               hosts for that network here.  When creating or
    #               importing a repo, use [].
    #
    # This function strictly favors hosts not on the same rack/row as
    # other hosts chosen while the function is running, but it is willing
    # to use same-rack/row hosts rather than return failure.
    #
    # This function weights choices by available disk space, but
    # ultimately picks at random.  That is, if choice A has 60GB free and
    # choice B has 120GB free, then choice B is twice as likely.
    #
    # If DNS is not available, this function degrades to random selection.
    # If DNS is slow, this function may take a while, though caching will
    # help.
    def self.pick_hosts(count, exclusions)
      pick_fileservers(count, exclusions).map { |fs| fs.name }
    end

    # Like pick_hosts, but returns Fileserver objects instead of bare strings.
    def self.pick_fileservers(count, exclusions)
      picker = HostPicker.new
      picker.pick_fileservers(count, exclusions: exclusions, far_from: [])
    end

    # Like pick_fileservers, but for non-voting fileservers.
    def self.pick_non_voting_fileservers(count, exclusions)
      fileservers = GitHub::DGit.get_fileservers(exclude_embargoed: true).reject(&:contains_voting_replicas?)
      picker = HostPicker.new
      picker.pick_fileservers(count, exclusions: exclusions, far_from: [], fileserver_pool: fileservers, no_raise: true)
    end

    def self.pick_non_voting_hosts(count, exclusions)
      pick_non_voting_fileservers(count, exclusions).map(&:name)
    end

    # Takes an array of active hosts, candidates for serving reads.
    #
    # Return a mapping of host to read weight for the provided hosts.
    def self.alloc_read_weight(hosts)
      # only consider online fileservers
      online = hosts & DGit.get_hosts
      # if nothing is online, go ahead and use them all, it doesn't matter
      online = hosts if online.empty?
      # simplistic, pick at random
      reader = online[rand(online.size)]
      Hash[hosts.map do |host|
        [host, host == reader ? 100 : 0]
      end]
    end

    OFFLINE_THRESHOLD = 3

    # In CI we can be running on a very busy host which can mean we consider the
    # servers to be overloaded when we're just in test mode. Make the value when
    # testing high enough we wouldn't expect to see it without the host being on
    # fire.
    if Rails.test?
      EMERGENCY_LOADAVG = 10_000
    else
      EMERGENCY_LOADAVG = 50
    end

    # Check to see if a host is probably offline from the point of view of
    # the local host.  That bit is stored in memcache.  It's set when a
    # gitrpc fails to connect (connection refused or timed out).
    #
    # Offline bits in memcache are set with a TTL of 60 seconds, and so
    # are cleared after that amount of time.
    #
    # Enterprise-only: the offline bit may also be set when
    # DGit.liveness_check fails for the host.  It may also be cleared
    # when DGit.liveness_check succeeds for the host.  The liveness_check
    # job is only run for GHE due to limitations on dot com.
    #
    # The host parameter must be the short hostname, from the fileservers
    # table.
    def self.is_offline?(host)
      offline_count(host) >= OFFLINE_THRESHOLD
    end

    # Mark a host as probably offline, from the point of view of the local
    # host, because an RPC couldn't connect, or a liveness check failed in
    # any way.  This is done with a short timeout in case something
    # happens to the liveness checks.
    #
    # The host parameter must be the short hostname, from the fileservers
    # table.
    #
    # This function is super racy, because there's no lock around the read
    # and write.  I could use GitHub.cache.incr, but it doesn't give me control
    # over the ttl.  The penalty for racing here is pretty small.  Two
    # incrementers at the same time might only increase the count by one.
    # An incrementer racing with a decrementer will go last-write-wins and so
    # may leave the host "offline" even when a check has passed.
    def self.set_offline(host, count = 1)
      val = offline_count(host) + count
      GitHub.cache.set(offline_key(host), val, 1.minute)
      val >= OFFLINE_THRESHOLD
    end

    # Mark a host as back online, from the point of view of the local
    # host.
    #
    # The host parameter must be the short hostname, from the fileservers
    # table.
    def self.set_online(host)
      GitHub.cache.delete(offline_key(host))
    end

    # The key name, in memcache, for indicating that a host is unreachable
    # from the local host.
    def self.offline_key(host)
      "dgit:offline:#{GitHub.local_host_name_short}:#{host}"
    end
    private_class_method :offline_key

    def self.offline_count(host)
      (GitHub.cache.get(offline_key(host)) || 0)
    end
    private_class_method :offline_count

    # Mark the given host as overloaded.
    def self.set_overloaded(host)
      GitHub.cache.set(overloaded_key(host), 1, 1.minute)
    end

    # Return whether the given host has been marked as overloaded.
    def self.is_overloaded?(host)
      ((GitHub.cache.get(overloaded_key(host)) || 0) > 0)
    end

    # Clear whether the given host is marked as overloaded.
    def self.clear_overloaded(host)
      GitHub.cache.delete(overloaded_key(host))
    end

    # The key name, in memcache, for indicating that a host is overloaded.
    def self.overloaded_key(host)
      "dgit:overloaded:#{host}"
    end
    private_class_method :overloaded_key

    # Attempt to contact every DGit host that the fileservers table says
    # is up, even the ones that have their offline key set in memcache.
    # Anything that fails the liveness check sets (or refreshes) the
    # offline key in memcache.  Anything that passes the check clears the
    # offline key.
    # Enterprise-only: see comment in `loadavg_check`.
    LIVENESS_CONNECT_TIMEOUT = 2  # seconds
    LIVENESS_READ_TIMEOUT    = 4  # seconds
    def self.liveness_check(ports: nil)
      # See https://github.com/github/sre-delivery/issues/389: this checker job
      # routine will never be properly supported for GitHub dot com going forward.
      raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?

      fileservers = get_fileservers(storage_class: :all)
      return if fileservers.count(&:contains_voting_replicas?) < 2
      routes = fileservers.map { |fs| fs.to_route("/", ports: ports) }

      up = []
      down = []
      sketchy = []
      start = Time.now
      GitHub.dogstats.time "dgit.liveness_check" do
        handler = ::BERTRPC::MuxHandler.new
        routes.each do |route|
          service = ::BERTRPC::Service.new(route.resolved_host, route.port, LIVENESS_READ_TIMEOUT, LIVENESS_CONNECT_TIMEOUT)
          rpc = service.call.gitrpc
          cs = handler.queue(rpc)
          do_error = proc do |count, metric|
            GitHub.stats.increment "dgit.#{route.original_host}.#{metric}" if GitHub.enterprise?
            GitHub.dogstats.increment("dgit.#{metric}", tags: ["server:#{route.original_host}", "server_datacenter:#{route.datacenter}"])
            if set_offline(route.original_host, count)
              down << route.original_host
            else
              sketchy << route.original_host
            end
          end
          cs.on_complete do |result|
            if result[0] > EMERGENCY_LOADAVG
              do_error.call(OFFLINE_THRESHOLD, "rpc-busy")  # mark offline immediately
            else
              set_online(route.original_host)
              up << route.original_host
            end
          end
          cs.on_error do |raw_error|
            do_error.call(1, "rpc-error")
          end
          rpc.send_message("/", {}, :loadavg, [])
        end
        handler.run(LIVENESS_READ_TIMEOUT, LIVENESS_CONNECT_TIMEOUT)
      end

      if GitHub.enterprise?
        prefix = "dgit.#{GitHub.local_host_name_short}.liveness_check"
        GitHub.stats.timing "#{prefix}.timing",   Time.now - start
        GitHub.stats.gauge  "#{prefix}.up",       up.size
        GitHub.stats.gauge  "#{prefix}.sketchy",  sketchy.size
        GitHub.stats.gauge  "#{prefix}.down",     down.size
      end

      GitHub.dogstats.gauge("dgit.liveness_check", up.size,      tags: ["state:up"])
      GitHub.dogstats.gauge("dgit.liveness_check", sketchy.size, tags: ["state:sketchy"])
      GitHub.dogstats.gauge("dgit.liveness_check", down.size,    tags: ["state:down"])
      if !down.empty?
        GitHub::Logger.log(controller: "DGit",
                           method: "liveness_check",
                           elapsed: Time.now - start,
                           up_count: up.size,
                           sketchy_count: sketchy.size,
                           down_hosts: down.inspect)
      end

      down
    end

    # Poll this fileserver host's current `loadavg` via GitRPC, and
    # save whether it exceeds EMERGENCY_LOADAVG in memcache.  Frontend
    # clients will attempt to route around hosts that have been marked
    # as overloaded in this way.
    #
    # This is a hobbled-together strictly-worse reimplementation of the
    # classic `DGit.liveness_check` method:
    #
    # In the move to Kubernetes, support for running an individual `timerd`
    # instance, one per each frontend host, was accidentally abandoned.  No
    # one involved in the github/github transition realized what the old
    # code was doing or that it was indeed actually running.  And in the new
    # runtime environment, there is no equivalent functionality.  So, this
    # job attempts to approximate the route-around-upon-high-load feature.
    #
    # Notably, the new `loadavg_check` is strictly worse than the old job
    # check functionality because it only stores its results into the
    # site-local (datacenter-local) memcache instance.  One consequence of
    # this is that long-distance connections may be routed without respect
    # to those long-distance fileservers' load average.  There is no
    # supported or straightforward way today to access another site's
    # memcache instance in the current stack.  And it would be questionable
    # to do even if it were straightforward in the code.
    def self.loadavg_check
      raise "dotcom-only method unexpectedly invoked (#{__method__})" if GitHub.enterprise?
      # GHE uses `DGit.liveness_check`.

      GitHub.dogstats.time "dgit.loadavg_check" do
        loadavg = GitHub::DGit::Fileserver.standin(GitHub.local_host_name_short)
                                          .build_maint_rpc("/tmp")
                                          .loadavg

        GitHub::Logger.log(controller: "DGit",
                           method: "loadavg_check",
                           loadavg: "#{loadavg[0]} #{loadavg[1]} #{loadavg[2]}")

        if loadavg[0] > EMERGENCY_LOADAVG
          set_overloaded(GitHub.local_host_name_short)
          Failbot.report!(GitHub::DGit::LoadAvgHigh.new,
                          app: "github-dgit-debug",
                          loadavg: "#{loadavg[0]} #{loadavg[1]} #{loadavg[2]}",
                          fileserver: GitHub.local_host_name_short)
        else
          clear_overloaded(GitHub.local_host_name_short)
        end
      end # "dgit.loadavg_check"
    end

    # Any datacenter with non-embargoed HDD-class storage servers is considered
    # eligible for cold storage
    def self.cold_storage_dcs
      get_fileservers(exclude_embargoed: true, storage_class: :hdd).map(&:datacenter).uniq
    end
  end
end
