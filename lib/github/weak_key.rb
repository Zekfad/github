# frozen_string_literal: true

require "github/ssh"

# This module is called by a script in the enterprise2 repository. This means
# we're stuck needing to expose this API. The functionality has been expanded
# though and moved to GitHub::SSH. Don't use *this* module in the future and
# don't add anything to this file.

module GitHub
  module WeakKey
    def self.weak?(key)
      GitHub::SSH.weak_rsa_key?(key)
    end
  end
end
