# frozen_string_literal: true

module GitHub
  class PrivateInstanceBootstrapper
    class InternalSupportContact < BootstrapConfiguration
      DEFAULTS = { type: "", support_address: "" }.freeze

      class Configuration
        attr_reader :type, :support_address

        def initialize(type:, support_address:)
          @type = type
          @support_address = support_address
        end

        def url
          url? ? support_address : ""
        end

        def email
          email? ? support_address : ""
        end

        def url?
          type == "url"
        end

        def email?
          type == "email"
        end
      end

      def self.key
        "internal-support-contact"
      end

      def title
        "Internal support contact"
      end

      def description
        if completed?
          "You have set up your internal support contact."
        else
          "Add a URL or E-mail for your users to contact your internal support team.
          This will be shown in support links in your enterprise instead of GitHub Support."
        end
      end

      def octicon
        "question"
      end

      def completed?
        completed(key("completed")) == "true"
      end

      def perform(type:, support_address:)
        ensure_valid_type(type)
        persist(type: type, support_address: support_address)
        complete!
      end

      def complete!
        set_completed(key("completed"))
        true
      end

      def key(postfix = nil)
        [self.class.key_prefix, self.class.key, postfix].compact.join("-")
      end

      def configuration
        Configuration.new(**load_configuration)
      end

      def destroy!
        GitHub.kv.mdel([key, key("completed")])
      end

      private

      def load_configuration
        JSON.parse(GitHub.kv.get(key).value { DEFAULTS.to_json }, symbolize_names: true)
      rescue Yajl::ParseError
        return DEFAULTS
      end

      def persist(data)
        GitHub.kv.set(key, data.to_json)
      end

      def ensure_valid_type(type)
        raise ArgumentError, "Type must be 'url' or 'email'" unless %w(url email).include?(type)
      end
    end
  end
end
