# rubocop:disable Style/FrozenStringLiteralComment

require "mail"

module GitHub
  module Mail

    # SendGrid's SMTP API works by sending a list of instructions as encoded
    # JSON into the X-SMTPAPI header. This attribute stores the Hash that gets
    # converted into encoded JSON.
    attr_reader :sendgrid_smtp_api

    # Returns the list of addresses this message should be sent to by
    # collecting the addresses off the to, cc and bcc fields.
    #
    # The method is overridden to allow writing the destinations list
    # explicitly. This allows for sending the message not to the list of
    # recipients on the To, Cc, and Bcc lines but only to the list of addresses
    # provided. This is essential for mailing list style relaying of messages to
    # a group without modifying headers.
    #
    # Example:
    #
    #  mail.to = 'mikel@test.lindsaar.net'
    #  mail.cc = 'sam@test.lindsaar.net'
    #  mail.bcc = 'bob@test.lindsaar.net'
    #  mail.destinations.length #=> 3
    #  mail.destinations.first #=> 'mikel@test.lindsaar.net'
    #
    # Setting destinations:
    #
    #  mail.to = 'mikel@test.lindsaar.net'
    #  mail.destinations = ['ryan@github.com']
    #  mail.destinations #=> ['ryan@github.com']
    def destinations
      if self["destinations"].any?
        self["destinations"]
      else
        super
      end
    end

    # Set the list of destination addresses and the X-GitHub-Recipient-Address
    # header.
    #
    # value - Array of string email addresses. Addresses only, no names or quotes
    #
    def destinations=(value)
      self["destinations"] = value
    end

    # Enables open & click tracking for emails. GitHub's taste favors to only
    # use these in marketing-type emails. Please do not use this for user
    # generated content like notifications.
    #
    # This method relies on SendGrid and the SMTP API:
    # http://sendgrid.com/docs/API_Reference/SMTP_API/index.html
    #
    # Returns true if tracking is enabled.
    def tracking
      return false if self["tracking"].nil?
      return false if self["tracking"].value.nil?
      return false if self["tracking"].value.empty?
      return !!self["tracking"]
    end

    # Set the tracking flag.
    #
    # value - A boolean to enable or disable open & click tracking.
    #
    # Returns nothing.
    def tracking=(value)
      self["tracking"] = !!value
    end

    # Categories are short lowercase strings to tag a type of email. They're
    # useful to compare and contrast open rates and click through in SendGrid.
    #
    # This method relies on SendGrid and the SMTP API:
    # http://sendgrid.com/docs/API_Reference/SMTP_API/index.html
    #
    # Returns a comma separated list of String categories.
    def categories
      self["categories"].present? ? self["categories"] : nil
    end

    # Set categories for this email.
    #
    # list — A comma separated list of lowercase strings as categories.
    #
    # Returns nothing.
    def categories=(list)
      self["categories"] = list.downcase
    end

    # Override getting of the destinations attribute to use the special
    # X-GitHub-Recipient-Address header.
    def [](name)
      if name.to_s == "destinations"
        if h = header["X-GitHub-Recipient-Address"]
          h.value.to_s.split(/, */)
        else
          []
        end
      else
        super
      end
    end

    # Encodes the SMTP API header as JSON. ActionMailer handles line wrapping
    # if given space to do so.
    #
    # http://sendgrid.com/docs/Code_Examples/SMTP_API_Header_Examples/ruby.html
    #
    # Returns a formatted JSON String.
    def encoded_smtp_api_header
      return nil unless sendgrid_smtp_api

      if !(sendgrid_smtp_api["filters"] && sendgrid_smtp_api["filters"].any?) &&
         !(sendgrid_smtp_api["category"] && sendgrid_smtp_api["category"].any?)
        return nil
      end

      json = GitHub::JSON.encode(sendgrid_smtp_api)
      # Add spaces in between {} and ,
      json.gsub!(/(["\]}])([,:])(["\[{])/, '\\1\\2 \\3')

      json
    end

    # Override setting the destinations attribute to use the header.
    def []=(name, value)
      @sendgrid_smtp_api ||= {}

      # Custom logic for SendGrid open & click tracking. See #tracking for
      # more documentation
      if name.to_s == "tracking"
        if !!value
          @sendgrid_smtp_api["filters"] ||= {}
          @sendgrid_smtp_api["filters"]["opentrack"] = {"settings" => {"enable" => 1}}
          @sendgrid_smtp_api["filters"]["clicktrack"] = {"settings" => {"enable" => 1}}
        else
          @sendgrid_smtp_api["filters"]&.remove!("opentrack")
          @sendgrid_smtp_api["filters"]&.remove!("clicktrack")
        end
        header["X-SMTPAPI"] = nil if header["X-SMTPAPI"]
        header["X-SMTPAPI"] = encoded_smtp_api_header
      end

      # Custom logic for SendGrid categories. See #categories for more
      # documentation
      if name.to_s == "categories"
        if value.present?
          @sendgrid_smtp_api["category"] = value.split(",")
        else
          @sendgrid_smtp_api.remove!("category")
        end
        header["X-SMTPAPI"] = nil if header["X-SMTPAPI"]
        header["X-SMTPAPI"] = encoded_smtp_api_header
      end

      # Do not set the destinations attribute, and instead set a different
      # header
      if name.to_s == "destinations"
        if value && value.any?
          header["X-GitHub-Recipient-Address"] = value.join(",")
        else
          header["X-GitHub-Recipient-Address"] = nil
        end
      else
        super
      end
    end
  end
end
