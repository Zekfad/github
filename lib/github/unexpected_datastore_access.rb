# frozen_string_literal: true

module GitHub
  class UnexpectedDatastoreAccess < StandardError
    def areas_of_responsibility
      [:high_availability]
    end

    def failbot_context
      { app: "github-multi-dc" }
    end
  end
end
