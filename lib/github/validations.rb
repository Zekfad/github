# frozen_string_literal: true

require "github/validations/bytesize"
require "github/validations/unicode"
require "github/validations/unicode_3"
require "github/validations/allowed_emoji"
require "github/validations/single_emoji"

module GitHub
  module Validations
  end
end
