# frozen_string_literal: true

module GitHub
  module RouteHelpers
    def gh_create_pull_request_path(repo)
      expand_nwo_from :create_pull_request_path, repo
    end

    def gh_merge_pull_request_path(pull)
      expand_nwo_from :merge_pull_request_path, pull
    end

    def gh_enqueue_pull_request_path(pull)
      expand_nwo_from :enqueue_pull_request_path, pull
    end

    def gh_cleanup_pull_request_path(pull)
      expand_nwo_from :cleanup_pull_request_path, pull
    end

    def gh_undo_cleanup_pull_request_path(pull)
      expand_nwo_from :undo_cleanup_pull_request_path, pull
    end

    def gh_pull_request_diff_path(pull)
      expand_nwo_from :pull_request_diff_path, pull
    end

    def gh_pull_request_patch_path(pull)
      expand_nwo_from :pull_request_patch_path, pull
    end

    def gh_show_pull_request_path(pull)
      expand_nwo_from :show_pull_request_path, pull
    end

    def gh_pull_request_commit_path(pull, commit)
      base = gh_show_pull_request_path(pull)
      "#{ base }/commits/#{ commit }"
    end
  end
end
