# frozen_string_literal: true

module GitHub
  module RouteHelpers

    def gh_stafftools_repository_discussions_path(repo)
      expand_nwo_from :stafftools_repository_discussions_path, repo
    end

    def gh_stafftools_repository_discussion_path(discussion)
      expand_nwo_from :stafftools_repository_discussion_path, discussion
    end

    def gh_database_stafftools_repository_discussion_path(discussion)
      expand_nwo_from :database_stafftools_repository_discussion_path, discussion
    end

    def gh_database_stafftools_repository_discussion_lock_path(discussion)
      expand_nwo_from :lock_stafftools_repository_discussion_path, discussion
    end

    def gh_database_stafftools_repository_discussion_unlock_path(discussion)
      expand_nwo_from :unlock_stafftools_repository_discussion_path, discussion
    end

    def gh_stafftools_repository_discussion_subscriptions_path(discussion)
      expand_nwo_from :stafftools_repository_discussion_subscriptions_path, discussion
    end

    def gh_stafftools_repository_discussion_comments_path(discussion)
      expand_nwo_from :stafftools_repository_discussion_comments_path, discussion
    end

    def gh_stafftools_repository_discussion_comment_path(comment)
      expand_from_discussion_comment :stafftools_repository_discussion_comment_path, comment
    end

    def gh_database_stafftools_repository_discussion_comment_path(comment)
      expand_from_discussion_comment :database_stafftools_repository_discussion_comment_path, comment
    end

    def gh_nested_comments_stafftools_repository_discussion_comment_path(comment)
      expand_from_discussion_comment :nested_comments_stafftools_repository_discussion_comment_path, comment
    end

    private

    def expand_from_discussion_comment(helper, comment)
      discussion = comment.discussion
      repo = discussion.repository
      send(helper, repo.owner, repo, discussion, comment)
    end
  end
end
