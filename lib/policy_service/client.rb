# frozen_string_literal: true

require "proto-policies"

module PolicyService
  class Client
    # An error class used to wrap any errors the Twirp API returns.
    class Error < StandardError; end

    class NotFoundError < Error; end

    SERVICE_NAME = "policy-service"

    def get_policies(organization_id:, status_filter:, resource_type_filter:)
      rpc(:GetPolicies, organization_id: organization_id, status_filter: status_filter, resource_type_filter: resource_type_filter)
    end

    def get_policy(organization_id:, policy_natural_id:, limit:, page:, filter:, order_by:)
      rpc(:GetPolicy, organization_id: organization_id, policy_natural_id: policy_natural_id, limit: limit, page: page, filter: filter, order_by: order_by)
    end

    def get_outcome(organization_id:, policy_natural_id:, resource_name:)
      rpc(:GetOutcome, organization_id: organization_id, policy_natural_id: policy_natural_id, resource_name: resource_name)
    end

    def evaluate_policy(organization_id:, policy_natural_id:)
      rpc(:EvaluatePolicy, organization_id: organization_id, policy_natural_id: policy_natural_id)
    end

    def rpc(method, params)
      response = policies_api.rpc(method, params)

      return response.data if response.error.blank?

      case response.error.code
      when :not_found
        raise NotFoundError, response.error
      else
        raise Error, response.error
      end
    end

    private

    def policies_api
      @policies_api ||= Policies::V1::PoliciesAPIClient.new(connection)
    end

    def connection
      @connection ||= Faraday.new(url: "#{GitHub.policy_service_url}/twirp") do |conn|
        conn.use GitHub::FaradayMiddleware::RequestID
        conn.use GitHub::FaradayMiddleware::HMACAuth, hmac_key: GitHub.policy_service_hmac_key
        conn.use GitHub::FaradayMiddleware::Datadog, stats: GitHub.dogstats, service_name: SERVICE_NAME
        conn.use GitHub::FaradayMiddleware::Tracer,
          service_name: SERVICE_NAME,
          parent_span: proc { GitHub.tracer.last_span },
          tracer: GitHub.tracer,
          # If we don't specify the operation, it defaults to "POST", in
          # which case LightStep produces an operation that filters the last
          # component of the URL.  Since in Twirp, that's the RPC method
          # name, which we want to keep, we set the operation explicitly
          # ourselves.
          operation: proc { |env| "#{env[:method].to_s.upcase} #{URI(env[:url]).path}" }
        conn.use GitHub::FaradayMiddleware::Staffbar, url: GitHub.policy_service_url
        conn.use Faraday::Resilient, name: SERVICE_NAME, options: {
          instrumenter: GitHub,
          sleep_window_seconds: 10,
          error_threshold_percentage: 5,
          window_size_in_seconds: 30,
          bucket_size_in_seconds: 5,
        }
        conn.options[:open_timeout] = GitHub.policy_service_connection_timeout
        conn.options[:timeout] = GitHub.policy_service_response_timeout
        conn.adapter :persistent_excon
      end
    end
  end
end
