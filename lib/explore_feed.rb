# frozen_string_literal: true

module ExploreFeed
  autoload :CustomerStory, "explore_feed/customer_story"
  autoload :Recommendation, "explore_feed/recommendation"
  autoload :RepositoryGoodFirstIssue, "explore_feed/repository_good_first_issue"
  autoload :Trending, "explore_feed/trending"
  autoload :Spotlight, "explore_feed/spotlight"
end
