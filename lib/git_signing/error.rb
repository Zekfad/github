# frozen_string_literal: true

module GitSigning
  class Error < StandardError; end
  class InvalidSignatureError < Error; end
  class OCSPError < Error; end
end
