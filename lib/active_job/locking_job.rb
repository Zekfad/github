# frozen_string_literal: true

module ActiveJob
  module LockingJob
    extend ActiveSupport::Concern

    included do
      attr_writer :lock_key

      class_attribute :_lock_key, instance_accessor: false
      class_attribute :_lock_timeout, instance_accessor: false

      around_enqueue do |job, block|
        unless locking? && self.class.queue_adapter.locked?(job)
          block.call
        end
      end

      around_perform do |job, block|
        begin
          block.call
          job.clear_lock
        rescue => exception
          # The exception handling in the parent may trigger a re-enqueue of the
          # job so we need to make sure the lock is cleared before it runs.
          job.clear_lock
          raise exception
        end
      end
    end

    module ClassMethods
      def lock_key
        self._lock_key
      end

      def lock_timeout
        self._lock_timeout
      end

      def locked_by(key:, timeout:)
        self._lock_key = key
        self._lock_timeout = timeout
      end
    end

    def serialize
      super.tap do |obj|
        obj["lock_key"] = lock_key
      end
    end

    def deserialize(job_data)
      super(job_data)
      self.lock_key = job_data["lock_key"]
    end

    def lock_timeout
      @lock_timeout ||= self.class.lock_timeout.to_i
    end

    def lock_key
      return nil unless lock_timeout > 0 && !self.class.lock_key.nil?
      @lock_key ||= self.class.lock_key.call(self)
    end

    def locking?
      !lock_key.nil? && lock_timeout > 0
    end

    def clear_lock
      self.class.queue_adapter.clear_lock(self)
    end
  end
end
