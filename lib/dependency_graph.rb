# frozen_string_literal: true

module DependencyGraph
  def self.random_requirments
    operator = ["=", "~>", ">", "<", ">="].sample
    "#{operator} #{rand(10)}.#{rand(10)}.#{rand(10)}"
  end

  def self.stubbed_manifests(manifest_repo, repos)
    ::DependencyGraph::Manifest.wrap([
      {
        node: {
          id:           10,
          manifestType: :gemfile,
          filename:     "Gemfile",
          path:         "/",
          repositoryId: manifest_repo.id,
          dependencies: {
            totalCount: 5,
            pageInfo: {
              hasNextPage: false,
              hasPreviousPage: false,
              startCursor: "A",
              endCursor: "Z",
            },
            edges: repos.map { |repo|
              {
                node: {
                  id:              22,
                  requirements:    random_requirments,
                  packageName:     repo.name,
                  packageManager:  "RUBYGEMS",
                  packageId:       "10",
                  repositoryId:    repo.id,
                  hasDependencies: true,
                },
              }
            }.push({
              node: {
                requirements: "2.0.1",
                packageName:  "rake",
                packageId:    "nil",
                repositoryId: nil,
              },
            }),
          },
        },
      }.with_indifferent_access,
      {
        node: {
          id:           11,
          manifestType: :gemfile_lock,
          filename:     "Gemfile.lock",
          path:         "/",
          repositoryId: manifest_repo.id,
          dependencies: {
            totalCount: 5,
            pageInfo: {
              hasNextPage: false,
              hasPreviousPage: false,
              startCursor: "A",
              endCursor: "Z",
            },
            edges: repos.map { |repo|
              {
                node: {
                  id:              22,
                  requirements:    random_requirments,
                  packageName:     repo.name,
                  packageManager:  "RUBYGEMS",
                  packageId:       "10",
                  repositoryId:    repo.id,
                  hasDependencies: true,
                },
              }
            }.push({
              node: {
                requirements: "2.0.1",
                packageName:  "bleh",
                packageId:    "nil",
                repositoryId: nil,
              },
            }),
          },
        },
      }.with_indifferent_access,
    ],
    {first: 10},
    ).push(DependencyGraph::Manifest.exceeds_max_size({
      "filename" => "package-lock.json",
      "path" => "",
      "repositoryId" => manifest_repo.id,
    }))
  end

  def self.stubbed_package_releases(params, repos)
    ::DependencyGraph::PackageRelease.wrap([
      {
        node: {
          packageName: params[:package_name],
          packageManager: params[:package_manager],
          version: params[:requirements].to_s.split(" ").last,
          dependencies: {
            totalCount: 5,
            edges: repos.map { |repo|
              {
                node: {
                  id:              22,
                  requirements:    random_requirments,
                  packageName:     repo.name,
                  packageManager:  "RUBYGEMS",
                  packageId:       "10",
                  repositoryId:    repo.id,
                  hasDependencies: true,
                },
              }
            }.push({
              node: {
                requirements: "2.0.1",
                packageName:  "rake",
                packageId:    "nil",
                repositoryId: nil,
              },
            }),
          },
        },
      }.with_indifferent_access,
    ])
  end

  def self.stubbed_packages(params, repos)
    return [] if params[:dependents_filter].blank?
    dependents = if params[:dependents_filter][:type] == :package
      [
        {
          cursor: "opaque-cursor",
          node: {
            name: "rails",
            repositoryId: repos.first.id,
          },
        },
        {
          cursor: "opaque-cursor",
          node: {
            name: "rake",
            repositoryId: nil,
          },
        },
      ]
    else
      repos.map do |repo|
        {
          cursor: "opaque-cursor",
          node: {
            name: repo.name,
            repositoryId: repo.id,
          },
        }
      end
    end

    packages = ::DependencyGraph::Package.wrap([
      {
        node: {
          id: "abc-123",
          name: "rails",
          packageManager: "RUBYGEMS",
          packageManagerHumanName: "RubyGems",
          abstractRepositoryDependents: {
            totalCount: repos.count,
          },
          abstractPackageDependents: {
            totalCount: 2,
          },
          dependents: {
            edges: dependents,
            pageInfo: {
              hasPreviousPage: false,
              hasNextPage: false,
            },
          },
        },
      }.with_indifferent_access,
      {
        node: {
          id: "def-456",
          name: "actionmailer",
          dependents: {
            edges: [],
            pageInfo: {
              hasPreviousPage: false,
              hasNextPage: false,
            },
          },
        },
      }.with_indifferent_access,
    ])

    if package_id = params[:package_filter][:package_id]
      packages.select! { |package| package.id == package_id }
    end

    packages.take(params.dig(:package_filter, :first) || 30)
  end
end
