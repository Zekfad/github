# rubocop:disable Style/FrozenStringLiteralComment

class S3Sign
  # sha-256 hash of ""
  EMPTY_BODY_HASH = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"

  def self.header(credentials, method, path, sha, now = Time.now.utc)
    Header.new(credentials, method, path, sha, 15.minutes.to_i, now)
  end

  def self.query(credentials, method, path, expires, now = Time.now.utc)
    Query.new(credentials, method, path, "", expires, now)
  end

  attr_reader :query
  attr_reader :expires
  attr_reader :expires_at

  EMPTY_BODY_VERBS = Set.new(%w(GET HEAD DELETE))

  def initialize(credentials, method, path, sha, expires, now)
    @credentials = credentials
    @verb = method
    @path = Pathname.new("/").join(path).to_s
    @sha = if EMPTY_BODY_VERBS.include?(@verb) && sha.blank?
      EMPTY_BODY_HASH
    else
      sha
    end
    @expires = expires
    @now = (now || Time.now).utc
    @expires_at = @now + @expires
    @query = {}
  end

  def time
    @now.strftime("%Y%m%dT%H%M%SZ")
  end

  private

  # convert a flat hash of URI key/value pairs to an escaped string.
  def to_query(q)
    # we want them sorted in capital and alphabetical order, but the query may
    # have symbol keys.
    string_keys = {}
    q.each_key do |key|
      string_keys[key.to_s] = key
    end

    string_keys.keys.sort.map do |key|
      real_key = string_keys[key]
      "#{UrlHelper.escape_path(key)}=#{UrlHelper.escape_path(q[real_key].to_s)}"
    end.join("&")
  end

  def to_query_string(q)
    qs = to_query(q)
    qs.blank? ? qs : "?#{qs}"
  end

  def string_to_sign(request)
    "AWS4-HMAC-SHA256\n%s\n%s/%s/s3/aws4_request\n%s" % [
      @now.strftime("%Y%m%dT%H%M%SZ"),
      @now.strftime("%Y%m%d"),
      "us-east-1",
      Digest::SHA256.hexdigest(request),
    ]
  end

  def signature(sts, now)
    kdate = OpenSSL::HMAC.digest("sha256", "AWS4#{@credentials[:secret]}", now.strftime("%Y%m%d"))
    kregion = OpenSSL::HMAC.digest("sha256", kdate, "us-east-1")
    kservice = OpenSSL::HMAC.digest("sha256", kregion, "s3")
    kcreds = OpenSSL::HMAC.digest("sha256", kservice, "aws4_request")
    return OpenSSL::HMAC.hexdigest("sha256", kcreds, sts)
  end

  def s3_host
    parts = @credentials[:bucket].split("/")
    "%s.s3.amazonaws.com" % [parts[0]]
  end

  def s3_url
    "https://%s%s" % [s3_host, s3_path]
  end

  def s3_path
    parts = @credentials[:bucket].split("/")
    root = ""
    if parts.size > 1
      root = parts[1...-1].join("/")
    end

    return Pathname.new("/").join(root, @path).to_s
  end

  class Header < S3Sign
    def token
      @token ||= token_string(canonical_request)
    end

    def location
      @location ||= s3_url + to_query_string(@query)
    end

    def canonical_request
      @canonical_request ||= begin
        @query.freeze
        "%s\n%s\n%s\nhost:%s\nx-amz-content-sha256:%s\nx-amz-date:%s\n\n%s\n%s" % [
          @verb,
          @path,
          to_query(@query),
          s3_host,
          @sha,
          @now.strftime("%Y%m%dT%H%M%SZ"),
          "host;x-amz-content-sha256;x-amz-date",
          @sha,
        ]
      end
    end

    def to_hash
      {
        "Authorization" => token,
        "x-amz-content-sha256" => @sha,
        "x-amz-date" => time,
      }
    end

    private

    def token_string(c)
      sts = string_to_sign(c)
      sig = signature(sts, @now)
      "AWS4-HMAC-SHA256 Credential=%s/%s/%s/s3/aws4_request,SignedHeaders=%s,Signature=%s" % [
        @credentials[:key],
        @now.strftime("%Y%m%d"),
        "us-east-1",
        "host;x-amz-content-sha256;x-amz-date",
        sig,
      ]
    end
  end

  class Query < S3Sign
    def token
      @token ||= begin
        sts = string_to_sign(canonical_request)
        signature(sts, @now)
      end
    end

    def location
      @location ||= begin
        s3_url + to_query_string(@query.merge("X-Amz-Signature" => token))
      end
    end

    def canonical_request
      @canonical_request ||= begin
        @query.update(
          "X-Amz-Algorithm" => "AWS4-HMAC-SHA256",
          "X-Amz-Credential" => "%s/%s/%s/s3/aws4_request" % [@credentials[:key], @now.strftime("%Y%m%d"), "us-east-1"],
          "X-Amz-Date" => @now.strftime("%Y%m%dT%H%M%SZ"),
          "X-Amz-Expires" => @expires,
          "X-Amz-SignedHeaders" => "host",
        )
        @query.freeze

        "%s\n%s\n%s\nhost:%s\n\nhost\nUNSIGNED-PAYLOAD" % [
          @verb,
          @path,
          to_query(@query),
          s3_host,
        ]
      end
    end
  end
end
