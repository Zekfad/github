# frozen_string_literal: true

module Docusign
  extend ActiveSupport::SecurityUtils

  class MissingEnvelopeIdError < StandardError; end

  STATUS_SENT = "Sent"
  STATUS_DELIVERED = "Delivered"
  STATUS_COMPLETED = "Completed"
  STATUS_VOIDED = "Voided"

  def self.signature_valid?(signature:, payload:)
    digest = OpenSSL::HMAC.digest("SHA256", GitHub.docusign_connect_secret_key, payload)
    base_64_hash = Base64.strict_encode64(digest)
    secure_compare(base_64_hash, signature)
  end
end
