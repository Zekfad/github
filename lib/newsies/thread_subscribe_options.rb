# frozen_string_literal: true

module Newsies
  # Internal: Used for the ThreadSubscriptionManager#subscribe method.
  class ThreadSubscribeOptions < Options.new(:reason, :force)
    def self.from_unknown(value)
      fill(reason: value.to_s)
    end

    def reason
      s = self[:reason].to_s
      s.empty? ? nil : s
    end

    alias force? force
  end
end
