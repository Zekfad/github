# frozen_string_literal: true

module Newsies
  class WebHandler
    def handler_key
      :web
    end

    # Public: Handles the delivery for the User.
    #
    # delivery   - A Newsies::Delivery.
    # settings   - A Newsies::Settings object of the user receiving this
    #              notification.
    # event_time - Time of the notification generating event
    # reason     - (Optional) Symbol subscription reason for the user.
    # root_job_enqueued_at - Time the parent job was enqueued.
    #
    # Returns Newsies::Responses::Boolean instance.
    def deliver(delivery, settings, event_time:, reason: nil, root_job_enqueued_at: nil)
      return false unless deliver?(delivery.comment, settings)

      Newsies::NotificationEntry.insert(
        settings.id,
        delivery.summary,
        reason: reason,
        event_time: event_time,
      )

      settings.user&.notify_web_notifications_changed_socket_subscribers(Newsies::NotificationEntry.default_live_updates_wait)

      DeliveryLogger.log_to_splunk_and_hydro(
        delivery,
        user: settings.user,
        reason: reason,
        handler_key: handler_key,
        root_job_enqueued_at: root_job_enqueued_at,
      )
      true
    end

    private

    # Internal: Should this be delivered?
    #
    # comment  - A comment to be delivered
    # settings - A Newsies::Settings object of the user receiving this
    #            notification
    #
    # Returns Boolean
    def deliver?(comment, settings)
      return settings.vulnerability_web? if comment.is_a?(RepositoryVulnerabilityAlert::WebNotification)
      return true if comment.is_a?(CheckSuiteEventNotification) && deliver_check_suite_notification?(comment, settings)
      return true unless comment.notifications_author
      comment.notifications_author.id != settings.id
    end

    def deliver_check_suite_notification?(comment, settings)
      settings.continuous_integration_web? && comment.deliver?(settings)
    end
  end
end
