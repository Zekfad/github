# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  class EmailHandler
    MessageClasses = [
      Newsies::Emails::AdvisoryCredit,
      Newsies::Emails::CheckSuiteEventNotification,
      Newsies::Emails::CommitComment,
      Newsies::Emails::CommitMention,
      Newsies::Emails::ComposableComment,
      Newsies::Emails::DiscussionPost,
      Newsies::Emails::DiscussionPostReply,
      Newsies::Emails::Discussion,
      Newsies::Emails::DiscussionComment,
      Newsies::Emails::GistComment,
      Newsies::Emails::Issue,
      Newsies::Emails::IssueComment,
      Newsies::Emails::IssueEventNotification,
      Newsies::Emails::PullRequest,
      Newsies::Emails::PullRequestComment,
      Newsies::Emails::PullRequestPushNotification,
      Newsies::Emails::PullRequestReview,
      Newsies::Emails::PullRequestReviewComment,
      Newsies::Emails::Release,
      Newsies::Emails::RepositoryAdvisory,
      Newsies::Emails::RepositoryAdvisoryComment,
      Newsies::Emails::RepositoryInvitation,
      Newsies::Emails::RepositoryVulnerabilityAlert,
    ]

    # Public: Handles the delivery of an email for the User.
    #
    # delivery - A Newsies::Delivery object.
    # settings - A Newsies::Settings object of the user receiving this
    #            notification.
    # options  - A Hash with message-specific options
    #            :reason - Symbol reason for sending the email:
    #              :mention, :team_mention, :assign, :author, nil
    #
    # Returns true if delivered, or false.
    def deliver(delivery, settings, options = nil)
      message = message_for(delivery, settings, options)
      return false unless message.deliverable?
      return false if message.blocked_by_organization_restriction?

      # Returns a Mail::Message to be delivered.
      #
      # If the mailer method called prevents delivery for any reason, an
      # instance of ActionMailer::Base::NullMail is returned instead. Calling
      # deliver_now on this object returns nil and does nothing.
      mail = ::NewsiesMailer.notification(message)

      # Returns the mail if delivered or nil otherwise.
      delivered = !!mail.deliver_now

      message.after_delivery(mail) if delivered
      report_sender_mismatch(delivery, settings, mail)

      DeliveryLogger.log_to_splunk_and_hydro(
        delivery,
        user: settings.user,
        reason: options[:reason],
        handler_key: handler_key,
        root_job_enqueued_at: options[:root_job_enqueued_at],
        email: message.recipient_email,
      ) if delivered

      delivered
    end

    # Internal: Initialize a message for the given comment.
    #
    # delivery - The delivery with a comment
    # settings - An instance of Newsies::Settings
    #
    # Returns instance of a Newsies::Emails::Message subclass.
    def message_for(delivery, settings, options = nil)
      message_class(delivery.comment).new(delivery, settings, options)
    end

    # Internal: Determines which Message class to use for the given comment.
    #
    #  comment - An object with a matching Newsies::Emails::* class
    def message_class(comment)
      MessageClasses.detect { |c| c.matches?(comment) } ||
        raise(ArgumentError, "No message class defined for #{Newsies::Comment.to_type(comment)}")
    end

    # Internal: Checks to see if the delivered mail object matches the
    # Newsies Delivery. This method currently only checks for agreement between
    # the email sender and the notification object's user.
    #
    # delivery - A Newsies::Delivery instance.
    # settings - A Newsies::Settings object of the user receiving this
    #            notification.
    # mail     - A GitHub::Mail instance.
    #
    # Returns nothing. Reports to Failbot if malformed.
    def report_sender_mismatch(delivery, settings, mail)
      # If no email was delivered or the notification object has no user,
      # there's nothing to verify.
      return unless mail
      return unless sender = delivery.comment.try(:user)

      quoted_name = ApplicationMailer::Helpers.quote_user_name(sender)
      from_header = mail[:from].value

      return if from_header.starts_with?(quoted_name)

      err = MailSenderMismatch.new("Expected mail for comment: #{delivery.comment.id} sent to #{settings.id} to be sent from user id: #{sender.id}")
      Failbot.report_user_error err
    end

    # Public: Gets the key that identifies this Handler.
    #
    # Returns a Symbol.
    def handler_key
      :email
    end

    class MailSenderMismatch < RuntimeError; end
  end
end
