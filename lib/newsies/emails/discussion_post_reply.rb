# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class DiscussionPostReply < Newsies::Emails::Message
      include ActionView::Helpers::TextHelper

      alias :team :notifications_list
      alias :discussion_post_reply :comment

      def self.matches?(comment)
        comment.is_a?(::DiscussionPostReply)
      end

      def subject
        "Re: #{DiscussionPost.subject_for(discussion_post_reply.discussion_post)}"
      end

      def in_reply_to
        discussion_post_reply.discussion_post.message_id
      end

      # Override Gmail ViewAction link to read "View Discussion" (rather than "View Discussion Post").
      def conversation_type
        "Discussion"
      end
    end
  end
end
