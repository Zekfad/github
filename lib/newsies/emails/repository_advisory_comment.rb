# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class RepositoryAdvisoryComment < Newsies::Emails::Message
      def self.matches?(comment)
        comment.is_a?(::RepositoryAdvisoryComment)
      end

      def in_reply_to
        comment.repository_advisory.message_id
      end

      def subject
        "Re: [#{repository.name_with_owner}] #{comment.repository_advisory.title} (#{comment.repository_advisory.ghsa_id})"
      end

      def reason_in_words
        "You are receiving this because you are either an administrator on #{repository.name_with_owner}, or a collaborator on #{comment.repository_advisory.ghsa_id}."
      end
    end
  end
end
