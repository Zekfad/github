# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class CommitMention < Newsies::Emails::Message
      def self.matches?(comment)
        comment.is_a?(::CommitMention)
      end

      delegate :commit, to: :comment

      def subject
        "[#{repository.name_with_owner}] #{commit.short_message} (#{comment.commit_id[0, 7]})"
      end

      def content
        commit.message
      end

      def content_html
        commit.message_body_html
      end

      def url
        "#{repository.permalink}/commit/#{comment.commit_id}"
      end

      def message_id
        "<#{repository.name_with_owner}/commit/#{comment.commit_id}@#{GitHub.host_name}>"
      end
    end
  end
end
