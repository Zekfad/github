# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class IssueEventNotification < Newsies::Emails::Message
      def self.matches?(comment)
        comment.is_a?(::IssueEventNotification)
      end

      delegate :issue, to: :comment

      def subject
        "Re: [#{repository.name_with_owner}] #{issue.title}#{issue_subject_suffix}"
      end

      def in_reply_to
        if issue.pull_request?
          issue.pull_request.message_id
        else
          issue.message_id
        end
      end
    end
  end
end
