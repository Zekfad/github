# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class GistComment < Newsies::Emails::Message
      def self.matches?(comment)
        comment.is_a?(::GistComment)
      end

      def subject
        "Re: #{comment.gist.name_with_title}"
      end

      def in_reply_to
        comment.gist.message_id
      end

      def content_header_html
        content_tag(:strong, "@#{comment.user.login}") + " commented on this gist." + tag("hr")
      end
    end
  end
end
