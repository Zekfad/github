# frozen_string_literal: true

module Newsies
  module Emails
    class Discussion < Newsies::Emails::Message
      alias :discussion :comment

      def self.matches?(comment)
        comment.is_a?(::Discussion)
      end

      def subject
        "[#{repository.name_with_owner}] #{discussion.title} (##{discussion.number})"
      end
    end
  end
end
