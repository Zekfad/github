# frozen_string_literal: true

module Newsies
  class PushNotification
    include ActiveModel::Validations

    MAX_BODY_LENGTH = 500.bytes
    DEFAULT_TTL_IN_SECONDS = 1.day.to_i

    validates :title, :url, :subject_id, :ttl, presence: true, allow_blank: false
    validates :body, length: { maximum: MAX_BODY_LENGTH }

    attr_reader :title, :body, :url, :subject_id, :subtitle, :ttl, :type

    def initialize(title:, body:, url:, subject_id:, subtitle: nil, type: nil, ttl: DEFAULT_TTL_IN_SECONDS)
      @title = title
      @body = body
      @url = url
      @subject_id = subject_id
      @subtitle = subtitle
      @ttl = ttl
      @type = type
    end

    def google_fcm_payload
      {
        notification: {
          title: title,
          subtitle: subtitle,
          body: body,
          sound: "default",
        }.compact,
        data: {
          url: url,
          subject_id: subject_id,
        },
        time_to_live: ttl,
      }
    end

    def google_fcm_v1_payload
      {
        android: {
          data: {
            id: subject_id,
            title: title,
            subtitle: subtitle,
            body: body,
            type: type,
            url: url
          }.compact,
          ttl: "#{ttl}s",
        },
        apns: {
          payload: {
            aps: {
              alert: {
                title: title,
                subtitle: subtitle,
                body: body,
              }.compact,
              sound: "default",
            }
          },
          headers: {
            # apple expects this value to be a utc date represented in epoch seconds
            "apns-expiration" => "#{Time.now.utc.to_i + ttl}"
          }
        },
        data: {
          url: url,
          subject_id: subject_id,
        },
      }
    end
  end
end
