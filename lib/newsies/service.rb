# rubocop:disable Style/FrozenStringLiteralComment

require "newsies/subscriber_set"

require "newsies/objects/list"
require "newsies/objects/thread"
require "newsies/objects/comment"
require "newsies/managers/web"
require "newsies/handlers/email_handler"
require "newsies/handlers/mobile_push_handler"
require "newsies/handlers/web_handler"
require "newsies/managers/notification_delivery_store"

module Newsies

  # The external Newsies object.  This wires subscription backends,
  # and also manages the backend for Settings.
  # This is the interface accessed by all outside code.
  class Service
    include Reasons

    attr_reader :handlers, :web

    if Rails.test?
      # Needed temporarily for low level unit tests
      attr_reader :list_subs, :thread_subs
    end

    DEFAULT_EMAIL_ONLY_NOTIFICATIONS_CUTOFF_DATE = Time.utc(2016, 01, 07).freeze
    DEFAULT_AUTO_SUBSCRIBE_CUTOFF_DATE = Time.utc(2016, 07, 27).freeze

    def initialize
      @settings_store = SettingsStore.new
      @web = Web.new
      @handlers = [WebHandler.new, MobilePushHandler.new, EmailHandler.new]
    end

    # This is the external endpoint for triggering notification delivery.
    #
    # object                  - The object that is generating the notification.
    # event_time              - (Required) Time that the notification generating object was created/update
    #                           This should be the time that _this_ notification is relevant for.
    # recipient_ids           - (Optional) Array<Integer> of user IDs for those who should receive the
    #                           notification. deliver the notifications to. Default of `nil` causes us to
    #                           deliver notifications to all computed subscribers of the given object.
    # reason                  - (Optional) String or Symbol defining the reason for which the notification is
    #                           being sent. Default of `nil` causes the reason to be derived from the
    #                           subscription record(s) for the notification recipient.
    # direct_mention_user_ids - If any users are direct mentioned in the notification they should be explictly
    #                           listed so that we trigger (if relevant) mobile push notifications for them
    # priority                - Priority of this notification, :low or :high, defaults :high
    #
    # Returns nothing.
    def trigger(object, event_time:, recipient_ids: nil, reason: nil, direct_mention_user_ids: nil, priority: nil)
      return unless ::GitHub.send_notifications?

      author = object.try(:notifications_author)
      job_class = DeliverNotificationsJob
      job_options = {
        enqueued_at: Time.now.to_f,
        direct_mention_user_ids: direct_mention_user_ids.presence,
        event_time: event_time,
        priority: priority
      }.compact

      if author && GitHub.flipper[:kubernetes_notification_delivery].enabled?(author)
        job_class = KubernetesDeliverNotificationsJob
        job_options = job_options.merge(enforce_interruption: true)
      end

      # Wait 10 seconds to give async spam checks time to finish https://github.com/github/notifications/issues/296
      if GitHub::SpamChecker.external_spamminess_check_enabled?
        job_class = job_class.set(wait: GitHub::SpamChecker::DELAY_FOR_EXTERNAL_CHECKS)
      end

      job_class.perform_later(
        object.class.name,
        object.id,
        recipient_ids,
        reason,
        **job_options,
      )

      nil
    end

    # Public: Get and update the settings for a user.
    #
    # user - The User whose settings we are going to get and update.
    #
    # Yields Newsies::Settings instance for modification prior to update.
    #
    # Returns Newsies::Responses::Boolean instance with value of true if found
    # and updated or false if not found or found but not updated.
    def get_and_update_settings(user, &block)
      Responses::Boolean.new do
        settings = settings(user).value!
        yield settings
        update_settings(user, settings).value!
      end
    end

    # Public: Get the settings or default settings for a user.
    #
    # user - The User whose settings we are going to get.
    #
    # Returns Newsies::Responses::Settings instance.
    def settings(user)
      Responses::Settings.new {
        settings = @settings_store.get(user)
        if settings.new?
          default_user_settings(user)
        else
          settings.user = user
          settings
        end
      }
    end

    # Public: Update a users settings to the settings provided.
    #
    # user - The User whose settings we are going to update.
    #
    # Returns Newsies::Responses::Boolean instance.
    def update_settings(user, settings)
      Responses::Boolean.new { @settings_store.set(user, settings) }
    end

    def async_subscribe_users_to_repository(repo_id, user_ids, repository_creator_id = nil)
      AutoSubscribeUsersToRepositoryJob.perform_later(repo_id, user_ids, repository_creator_id)
    end

    # Public: Enqueues an instance of Newsies::AutoSubscribeUserToRepositoriesJob to auto subscribe
    # a given user to the list of repositories.
    #
    # The job belongs to the notifications queue, which can be paused in case of the notifications DB cluster
    # being down. Ideally this would happen automatically in case of a failure.
    #
    # user   - A User or a Newsies::Settings object.  If a User is
    #          given, fetch the Newsies::Settings with #settings.
    # repository_ids - An Array of repository_ids
    # notify - Boolean that determines if the user is notified.
    #
    # Returns nothing
    def async_auto_subscribe(user, repository_ids, notify = true)
      Newsies::AutoSubscribeUserToRepositoriesJob.perform_later(user.id, repository_ids, notify)
    end

    # Public: Subscribes a User to a Repository, unless they are ignoring it.
    #
    #     kyle = User.find_by_login("kneath")
    #     github = Repository.nwo 'github/github'
    #     GitHub.newsies.autosubscribe kyle, github
    #
    # user   - A User or a Newsies::Settings object.  If a User is
    #          given, fetch the Newsies::Settings with #settings.
    # repo   - A Repository.
    # notify - Boolean that determines if the user is notified.
    #
    # Returns true if the user was auto subscribed, or false.
    def auto_subscribe(user, repo, notify = true, auto_subscribe_to_own_repos = false)
      Responses::Boolean.new {
        GitHub.dogstats.time("newsies.auto_subscribe") do
          result = false

          user_object = user.is_a?(User) ? user : User.find(user.id)
          # Auto-subscribing to forks is an abuse vector
          next false if repo.fork? && !GitHub.enterprise?

          # Don't subscribe users if ignoring
          all_lists = [repo, repo.parent, repo.root]
          all_lists.compact!
          all_lists.uniq!
          ignored = ListSubscription.ignored(user.id, Newsies::List.to_objects(all_lists))
          next false if ignored.any?

          newsies_list = Newsies::List.to_object(repo)

          # Don't subscribe users if they are already subscribed to a subset of thread types
          next false if ThreadTypeSubscription.for_user(user.id).for_list(newsies_list).any?

          settings = user.respond_to?(:auto_subscribe?) ? user : settings(user).value!

          if auto_subscribe_to_own_repos || settings.try(:auto_subscribe?)
            notify = false if user.id == repo.owner_id

            ApplicationRecord::Domain::Notifications.transaction do
              ListSubscription.auto_subscribe(user.id, newsies_list, notify)
              ThreadTypeSubscription.unsubscribe_from_all_thread_types_for_list(user.id, newsies_list)
              async_notify_list_subscription_state_change(user.id, newsies_list)
              result = true
            end
          end

          result
        end
      }
    end

    # Public: Yields a user setting and Array of repositories that the user has
    # not been notified of and marks them as notified.
    #
    # block - A block taking a Newsies::Settings object and Array of
    #         Repository instances.
    #
    # Returns Newsies::Response.
    def notify_auto_subscribed(&block)
      Response.new {
        ListSubscription.subscriptions_to_notify(list_type: "Repository") do |user_id, lists|
          list_ids = lists.map(&:id)
          if user = User.find_by_id(user_id)
            begin
              repositories = Repository.includes(:owner).where(id: list_ids)
              block.call(settings(user).value!, repositories)
            ensure
              missing_list_ids = list_ids - repositories.map(&:id)
              missing_lists = missing_list_ids.map { |id| Newsies::List.new("Repository", id) }
              ListSubscription.unsubscribe(user_id, missing_lists)
            end
          else
            lists = list_ids.map { |id| Newsies::List.new("Repository", id) }
            ListSubscription.unsubscribe(user_id, lists)
          end
        end
      }
    end

    # Public: Marks the user as having been notified about the given lists.
    #
    # user - A User.
    # list - An Array of list objects.
    #
    # Returns Newsies::Response with nil value.
    def mark_lists_as_notified(user, lists)
      Response.new { ListSubscription.notified(user.id, Newsies::List.to_objects(lists)) }
    end

    # Public: Subscribe a user to a thread.
    #
    # user    - A User.
    # list    - A list (ie: Repository).
    # thread  - A thread (ie: Issue, Commit, etc.).
    # reason  - Optional String, Symbol or Hash reason
    #           (ie: "mention", :mention, {:reason => :mention, :force => true}).
    #
    # Returns Newsies::Responses::Boolean.
    def subscribe_to_thread(user, list, thread, reason = nil, events = [])
      Responses::Boolean.new do
        GitHub.dogstats.time("newsies.thread_subscribe") do
          next false unless user&.newsies_enabled?

          newsies_list = Newsies::List.to_object(list)
          list_subscription_status = ListSubscription.status(user.id, newsies_list)

          next false if list_subscription_status.ignored?

          unless list.try(:readable_by?, user)
            GitHub.dogstats.increment(
              "newsies.delivery.user_not_subscribable",
              tags: ["class:#{Newsies::Thread.to_type(thread).underscore}"])
          end

          options = ThreadSubscribeOptions.from(reason)
          options.reason = reason_sym = valid_reason_from(options.reason)
          options.force = forced_reason?(reason_sym) unless reason.is_a?(Hash)
          newsies_thread = Newsies::Thread.to_object(thread, list: newsies_list)
          ThreadSubscription.subscribe(user.id, newsies_thread, options, events)

          true
        end
      end
    end

    # Public: Subscribe an array of users to a thread.
    #
    # user - An array of Users.
    # list - A list (ie: Repository).
    # thread - A thread (ie: Issue, Commit, etc.).
    # reason - Optional String, Symbol or Hash reason
    #          (ie: "mention", :mention, {:reason => :mention, :force => true}).
    #
    # Returns Newsies::Responses::Boolean if the operation succeeds even if any user in the list can't be subscribed
    def subscribe_all_to_thread(users, list, thread, reason = nil)
      Responses::Boolean.new do
        GitHub.dogstats.time("newsies.thread_subscribe_all") do
          responses = subscription_status_all(users, list).value!
          subscribable_users = users.select.with_index do |user, index|
            next false unless user && user.newsies_enabled?
            response = responses[index]
            next false if response.ignored?
            true
          end

          options = ThreadSubscribeOptions.from(reason)
          options.reason = reason_sym = valid_reason_from(options.reason)
          options.force = forced_reason?(reason_sym) unless reason.is_a?(Hash)
          newsies_list = Newsies::List.to_object(list)
          newsies_thread = Newsies::Thread.to_object(thread, list: newsies_list)
          subscribable_users.each_slice(GitHub.subscribed_users_batch_size).each do |users|
            ThreadSubscription.subscribe_all(users.map(&:id), newsies_thread, options)
          end
          true
        end
      end
    end

    # Currently this is only used by commit comments to asyncronously subscribe
    # for the :author, or :comment reasons, no team is needed
    def async_subscribe_to_thread(user_id, repository_id, commit_oid, reason = nil)
      SubscribeUserToThreadJob.perform_later(user_id, repository_id, commit_oid, reason)
    end

    # Public: Subscribe a user to a list. See the background job
    # SubscribeToListNotificationsJob to call this asynchronously.
    #
    # user - A User.
    # list - A list (ie: Repository, Team).
    #
    # Returns Newsies::Responses::Boolean.
    def subscribe_to_list(user, list)
      Responses::Boolean.new do
        GitHub.dogstats.time("newsies.list_subscribe") do
          next false unless user && user.newsies_enabled?

          newsies_list = Newsies::List.to_object(list)
          ListSubscription.subscribe(user.id, newsies_list)
          ThreadTypeSubscription.unsubscribe_from_all_thread_types_for_list(user.id, newsies_list)
          async_notify_list_subscription_state_change(user.id, newsies_list)
          true
        end
      end
    end

    # Public: Subscribe a user to a thread type, will remove any existing
    #         watching/ignored entry for the list
    #
    # user        - A User.
    # list        - A list (i.e. Repository, Team)
    # thread_type - A class of the thread type, i.e. Release
    #
    # Return Newsies::Responses::Boolean
    def subscribe_to_thread_type(user, list, thread_type_class)
      Responses::Boolean.new do
        GitHub.dogstats.time("newsies.thread_type_subscribe") do
          next false unless user&.newsies_enabled?
          newsies_list = Newsies::List.to_object(list)

          ListSubscription.unsubscribe(user.id, [newsies_list])
          ThreadTypeSubscription.subscribe_to_thread_type(
            user.id,
            newsies_list,
            Newsies::Object.type_from_class(thread_type_class),
          )
          async_notify_list_subscription_state_change(user.id, newsies_list)
        end
      end
    end

    # Public: Checks the subscription status for the user and the list or
    # thread object.
    #
    # user    - A User.
    # list    - A list (Repository or Team).
    # thread  - A thread (Issue, Discussion, or Commit).
    #
    # Returns Newsies::Responses::Subscription instance.
    def subscription_status(user, list, thread = nil)
      Responses::Subscription.new do
        GitHub.dogstats.time("newsies.subscription_status") do
          newsies_list = Newsies::List.to_object(list)

          if thread
            newsies_thread = Newsies::Thread.to_object(thread, list: newsies_list)
            ThreadSubscription.status(user.id, newsies_thread)
          else
            list_subscription = ListSubscription.status(user.id, newsies_list)
            list_subscription.thread_types = ThreadTypeSubscription.subscribed_thread_types(user.id, newsies_list)
            list_subscription
          end
        end
      end
    end

    # Public: Checks the subscription status for an array of users and the list or
    # thread object.
    #
    # user    - An array of Users.
    # list    - A list (Repository or Team).
    # thread  - A thread (Issue, Discussion, or Commit).
    #
    # Returns Newsies::Responses::Subscription instance wrapping an array of Subscription
    def subscription_status_all(users, list, thread = nil)
      Responses::Array.new do
        statuses = []
        if thread
          users.each_slice(GitHub.subscribed_users_batch_size).each do |batched_users|
            newsies_list = Newsies::List.to_object(list)
            newsies_thread = Newsies::Thread.to_object(thread, list: newsies_list)
            statuses.push(*ThreadSubscription.status_all(batched_users.map(&:id), newsies_thread))
          end
        else
          users.each_slice(GitHub.subscribed_users_batch_size).each do |batched_users|
            newsies_list = Newsies::List.to_object(list)
            statuses.push(*ListSubscription.status_all(batched_users.map(&:id), newsies_list))
          end
        end
        statuses
      end
    end

    # Public: retrieves the subscription status for a user and some list ids.
    #
    # user     - A User.
    # list_ids - the Entity ids to check
    #
    # Returns Newsies::Responses::Array instance.
    def list_subscriptions(user, list_ids)
      Responses::Array.new {
        lists = list_ids.compact.uniq.map { |id|
          Newsies::List.new("Repository", id)
        }
        ListSubscription.subscriptions_find_many(user.id, lists)
      }
    end

    # Public: Get the subscriptions for the given user and lists.
    #
    # user_id - A User id.
    # list - the lists to scope to.
    #
    # Returns Newsies::Responses::Array instance.
    def user_list_subscriptions(user_id, lists)
      Responses::Array.new {
        GitHub.dogstats.time("newsies.user_list_subscriptions") do
          Newsies::ListSubscription.for_user(user_id).for_lists(lists)
        end
      }
    end

    # Public: Get the subscriptions for the given user and threads.
    #
    # user_id - A User id.
    # threads - the threads to scope to.
    #
    # Returns Newsies::Responses::Array instance.
    def user_thread_subscriptions(user_id, threads)
      Responses::Array.new {
        GitHub.dogstats.time("newsies.user_thread_subscriptions") do
          Newsies::ThreadSubscription.for_user(user_id).for_threads(threads)
        end
      }
    end

    # Public: Get the subscriptions for the given user and types for the
    # given threads.
    #
    # user_id - A User id.
    # threads - the threads to scope the returned types to.
    #
    # Returns Newsies::Responses::Array instance.
    def user_thread_type_subscriptions(user_id, threads)
      Responses::Array.new {
        GitHub.dogstats.time("newsies.user_thread_type_subscriptions") do
          Newsies::ThreadTypeSubscription.for_user(user_id).for_threads(threads)
        end
      }
    end

    # Public: Unsubscribes a User from an List or Thread.
    #
    # user - A User.
    # list - An List or Array of List objects of a single type (if no thread is
    #        given).
    # thread - Optional thread (Issue, Discussion, or Commit)
    #
    # Returns Newsies::Response instance.
    def unsubscribe(user, list, thread = nil)
      Response.new {
        if thread
          GitHub.dogstats.time("newsies.thread_unsubscribe") do
            newsies_list = Newsies::List.to_object(list)
            newsies_thread = Newsies::Thread.to_object(thread, list: newsies_list)
            ThreadSubscription.unsubscribe_thread(user.id, newsies_thread)
          end
        else
          GitHub.dogstats.time("newsies.list_unsubscribe") do
            lists = Newsies::List.to_objects(Array(list))
            ListSubscription.unsubscribe(user.id, lists)
            ThreadTypeSubscription
              .unsubscribe_from_all_thread_types_for_multiple_lists(user.id, lists)
            lists.each do |newsies_list|
              async_notify_list_subscription_state_change(user.id, newsies_list)
            end
          end
        end
      }
    end

    # Public: Delete specific thread subscriptions for a given user id.
    #
    # user_id                 - A User id.
    # thread_subscription_ids - An array of thread subscription ids.
    #
    # Returns Newsies::Response instance.
    def delete_thread_subscriptions(user_id:, thread_subscription_ids:)
      Response.new do
        GitHub.dogstats.time("newsies.delete_thread_subscriptions") do
          ThreadSubscription
            .for_user(user_id)
            .where(id: thread_subscription_ids)
            .in_batches(of: ThreadSubscription::DELETE_BATCH_SIZE)
            .each(&:destroy_all)
        end

        nil
      end
    end

    # Public: Enqueues a job to delete all data in newsies
    #         for a given user and multiple lists
    #
    # user_id - Integer user id to cleanup
    # lists   - An array of list objects (Repository or Team) or array of Newsies::List objects
    def async_delete_all_for_user_and_lists(user_id, lists)
      lists.each_slice(DeleteAllForUserAndListsJob::MAX_LIST_SIZE) do |batched_lists|
        list_hashes = batched_lists.map do |list|
          { type: List.to_type(list), id: List.to_id(list) }
        end
        DeleteAllForUserAndListsJob.perform_later(user_id, list_hashes)
      end
    end

    # Public: Enqueues a job to delete all data in newsies for a given user
    def async_delete_all_for_user(user_id)
      DeleteAllForUserJob.perform_later(user_id)
    end

    # Public: Enqueues a job to delete all of a user's non-ignoring list subscriptions.
    #
    # user_id   - User ID for which list subscriptions should be deleted.
    # list_type - String type of list subscriptions to delete e.g. "Repository".
    def async_delete_list_subscriptions_for_user(user_id, list_type)
      DeleteListSubscriptionsForUserJob.perform_later(user_id, list_type)
    end

    # Public: Enqueues a job to delete all data in newsies
    #         for a given list and multiple users
    #
    # list     - A list (e.g. Repository or Team) to clean up for
    # user_ids - An array of user_ids
    def async_delete_all_for_list_and_users(list, user_ids)
      user_ids.each_slice(DeleteAllForListAndUsersJob::MAX_LIST_SIZE) do |batched_user_ids|
        newsies_list = List.to_object(list)
        DeleteAllForListAndUsersJob.perform_later(newsies_list.type, newsies_list.id, batched_user_ids)
      end
    end

    # Public: Enqueues a job to delete all data in newsies for a given list
    #
    # list     - A list (e.g. Repository or Team) to clean up for
    def async_delete_all_for_list(list)
      newsies_list = List.to_object(list)
      DeleteAllForListJob.perform_later(newsies_list.type, newsies_list.id)
    end

    # Public: Enqueues a job to delete all data in newsies for a given thread
    #
    # list     - A list (e.g. Repository or Team) to clean up for
    # thread   - A thread (e.g. Issue or DiscussionPost) to clean up for
    # options  - optional Hash of options, see DeleteAllForThreadJob for available options
    def async_delete_all_for_thread(list, thread, options = {})
      newsies_list = List.to_object(list)
      newsies_thread = Thread.to_object(thread)

      DeleteAllForThreadJob.perform_later(newsies_list.type, newsies_list.id, newsies_thread.type, newsies_thread.id, **options)
    end

    def async_destroy_mobile_device_tokens(user_id)
      DestroyMobileDeviceTokensJob.perform_later(user_id)
    end

    # Public: Ignores the list for the user. See the background job
    # IgnoreListNotificationsJob to call this asynchronously.
    #
    # user - A User.
    # list - A Repository.
    #
    # Returns Newsies::Response instance.
    def ignore_list(user, list)
      if list.nil?
        raise ArgumentError, "list was nil (user: #{user.id})"
      end

      Response.new {
        newsies_list = Newsies::List.to_object(list)

        GitHub.dogstats.time("newsies.ignore_list") do
          ListSubscription.ignore(user.id, newsies_list)
          ThreadTypeSubscription.unsubscribe_from_all_thread_types_for_list(user.id, newsies_list)
          async_notify_list_subscription_state_change(user.id, newsies_list)
        end
      }
    end

    # Public: Ignores the thread for the user.
    #
    # user - A User.
    # list - A Repository.
    # thread - A Thread (aka Issue, PullRequest, etc.).
    #
    # Returns Newsies::Response instance.
    def ignore_thread(user, list, thread)
      if list.nil?
        raise ArgumentError, "list was nil (user: #{user.id})"
      end

      if thread.nil?
        raise ArgumentError, "thread was nil (user: #{user.id}, list: #{list.id})"
      end

      Response.new {
        GitHub.dogstats.time("newsies.ignore_thread") do
          newsies_list = Newsies::List.to_object(list)
          newsies_thread = Newsies::Thread.to_object(thread, list: newsies_list)
          ThreadSubscription.ignore(user.id, newsies_thread)
        end
      }
    end

    # Public: Checks to see which threads are ignored.
    #
    # user           - A User object.
    # summary_hashes - Array of Summary hashes.
    #
    # Returns a Newsies::Responses::Set of ignored summary id's.
    def ignored(user, summary_hashes)
      threads_by_summary_id = {}
      summary_hashes.each do |summary_hash|
        summary_id = Integer(summary_hash[:id])

        list_type = summary_hash[:list][:type]
        list_id = summary_hash[:list][:id]

        thread_type = summary_hash[:thread][:type]
        thread_id = summary_hash[:thread][:id]

        newsies_list = Newsies::List.new(list_type, list_id)
        newsies_thread = Newsies::Thread.new(thread_type, thread_id, list: newsies_list)

        threads_by_summary_id[summary_id] = newsies_thread
      end

      summary_ids_by_list_key = {}
      threads_by_summary_id.each do |summary_id, newsies_thread|
        summary_ids_by_list_key[newsies_thread.list_key] = summary_id
      end

      newsies_threads = threads_by_summary_id.values
      Responses::Set.new {
        set = Set.new
        subscriptions = ThreadSubscription.ignored(user, newsies_threads)
        subscriptions.each do |subscription|
          if summary_id = summary_ids_by_list_key[subscription.thread.list_key]
            set.add(summary_id)
          end
        end
        set
      }
    end

    # Public: Lists subscriptions that a User has.
    #
    #     # all subscribed and ignored
    #     GitHub.newsies.subscriptions(kyle)
    #
    #     # all ignored
    #     GitHub.newsies.subscriptions(kyle, set: :ignored)
    #
    #     # all subscribed
    #     GitHub.newsies.subscriptions(kyle, set: :subscribed)
    #
    # user      - A User.
    # set       - Optional value to specify the set of subscriptions:
    #             :all        - All subscriptions and ignores (default).
    #             :subscribed - All subscriptions.
    #             :ignored    - All ignores.
    # page      - An optional Integer for which page to return.
    # per_page  - An optional Integer for how many results to return per page.
    # sort      - An optional String or Symbol direction to sort (:asc, "asc", :desc, "desc").
    #
    # Returns Newsies::Responses::Array of Newsies::Subscription instances.
    def subscriptions(user, set: nil, page: nil, per_page: nil, sort: nil, list_type: "Repository")
      Responses::Array.new {
        ListSubscription.subscriptions(
          user.id,
          list_type: list_type,
          set: set,
          page: page,
          per_page: per_page,
          sort: sort,
        )
      }
    end

    # Public: Returns subscriptions for a given user using cursor-based pagination.
    #
    # user    - A User.
    # options - Optional Hash. See ListSubscription#subscriptions_by_cursor
    #           for the supported options.
    def subscriptions_by_cursor(user, options = {})
      Responses::Array.new {
        ListSubscription.subscriptions_by_cursor(user.id, options)
      }
    end

    # Public: Returns a page of thread subscriptions for a given user.
    #
    # user_id         - Database ID of user.
    # cursor          - Integer ID value that marks the start of the page to return. If omitted,
    #                   the returned page will begin with the most recently created subscription.
    # direction       - A Symbol (either :asc or :desc) describing the direction of pagination.
    # limit           - Integer describing the number of items to include in the page.
    # reason          - A subscription reason to which results should be restricted.
    # include_ignored - A Boolean for whether or not to include rows with `ignored=true`.
    # list_type       - An optional string list type. Required if list_id is specified.
    # list_id         - An optional string list id.
    #
    # Returns a Newsies::Responses::Array.
    def thread_subscriptions_by_cursor(
        user_id:,
        cursor: nil,
        direction: :desc,
        limit: 20,
        reason: "manual",
        include_ignored: false,
        list_type: nil,
        list_id: nil
    )
      Responses::PageWithPreviousAndNextFlags.new do
        GitHub.dogstats.time("newsies.thread_subscriptions_by_cursor") do
          scope = ThreadSubscription
            .for_user(user_id)
            .cursor_paginate(
              cursor: cursor,
              direction: direction,
              limit: limit,
              reveal_next_page: true,
              reveal_previous_page: true,
            )

          scope = scope.for_reason(reason) if reason
          scope = scope.excluding_ignored unless include_ignored
          if list_type
            scope = scope.where(list_type: list_type)
            scope = scope.where(list_id: list_id) if list_id
          end
          scope = scope.force_index
          page = scope.to_a

          has_previous_page = false
          if cursor && page.first&.id == cursor
            has_previous_page = true
            page = page.drop(1)
          end

          has_next_page = false
          if page.length > limit
            has_next_page = true
            page = page.take(limit)
          end

          PageWithPreviousAndNextFlags.new(
            page: page,
            has_next_page: has_next_page,
            has_previous_page: has_previous_page,
          )
        end
      end
    end

    def count_thread_subscriptions(user_id:, reason: "manual", include_ignored: false, list_type: nil, list_id: nil, count_limit: 1001)
      Responses::Count.new do
        start_time = GitHub::Dogstats.monotonic_time

        scope = ThreadSubscription
          .for_user(user_id)
          .order(id: :asc)

        scope = scope.for_reason(reason) if reason
        scope = scope.excluding_ignored unless include_ignored
        if list_type
          scope = scope.where(list_type: list_type)
          scope = scope.where(list_id: list_id) if list_id
        end

        scope = scope.force_index

        above_limit = count_limit && scope.offset(count_limit).exists?

        result = ApproximateCount.new(
          above_limit ? count_limit : scope.count,
          accurate: !above_limit)

        GitHub.dogstats.timing_since(
          "newsies.count_thread_subscriptions",
          start_time,
          tags: ["above_limit:#{!!above_limit}"])

        result
      end
    end

    def count_thread_type_subscriptions(user_id, list_type: "Repository", thread_type: "Release")
      Responses::Count.new do
        GitHub.dogstats.time("newsies.count_thread_type_subscriptions") do
          ThreadTypeSubscription
            .for_user(user_id)
            .for_thread_type(thread_type)
            .where(list_type: list_type)
            .count
        end
      end
    end

    # Public: Returns a batch of thread subscription lists for a given user.
    # The returned Array will be tuples of Newsies::List and total counts.
    #
    # user_id         - Database ID of user.
    # include_ignored - Whether or not to include ignored subscriptions in the
    #                   result.
    # list_type       - An optional string list type.
    #
    # Returns a Newsies::Response.
    def thread_subscription_lists(user_id:, include_ignored: false, list_type: nil)
      Responses::Array.new do
        GitHub.dogstats.time("newsies.thread_subscription_lists") do
          scope = ThreadSubscription
            .for_user(user_id)
            .group(:list_id, :list_type)

          scope = scope.excluding_ignored unless include_ignored
          scope = scope.where(list_type: list_type) if list_type

          scope.size.to_a.map do |(list_id, list_type), count|
            [Newsies::List.new(list_type, list_id), count]
          end
        end
      end
    end

    # Public: Wrapper for subscriptions that returns array of repositories as
    # the response value instead of array of subscriptions. Also does some
    # cleanup for subscriptions where the repository doesn't exist, has been
    # marked deleted or the user does not have permission to.
    #
    # user             - A User.
    # set              - Optional value to specify the set of subscriptions:
    #                      :all            - All list subscriptions and ignores (default).
    #                      :subscribed     - All list subscriptions.
    #                      :releases_only  - All thread-type subscriptions for Releases.
    #                      :ignored        - All ignores.
    # page             - An optional Integer for which page to return.
    # per_page         - An optional Integer for how many results to return per page.
    # excluding        - An optional Array of list_ids to be excluded on the MySQL side.
    # sort             - An optional String or Symbol direction to sort (:asc, "asc", :desc, "desc").
    # visible_repo_ids - The set of repositories that the user can access. If omitted this will be
    #                    computed afresh in this method.
    #
    # Returns Newsies::Responses::Array of Repository instances.
    def subscribed_repositories(user, set: nil, page: nil, per_page: nil, excluding: nil, sort: nil, visible_repo_ids: nil)
      Responses::Array.new do
        good = []
        bad_ids = Set.new
        excluding = Array(excluding).map { |id| Newsies::List.new(::Repository.name, id) }

        begin
          subscriptions = if set == :releases_only
            ThreadTypeSubscription.subscriptions(
              user.id,
              page: page,
              per_page: per_page,
              excluding: excluding,
              sort: sort,
            )
          else
            ListSubscription.subscriptions(
              user.id,
              set: set,
              page: page,
              per_page: per_page,
              excluding: excluding,
              sort: sort,
              list_type: ::Repository.name,
            )
          end

          # FIXME: this repair likely should be some sort of automated task or
          # a layer up higher as the newsies service shouldn't know this much
          # about repositories
          repository_ids = subscriptions.map(&:list_id).uniq
          repositories = ::Repository.includes(:owner).where(id: repository_ids)
          repositories_by_id = repositories.index_by(&:id)

          # If we weren't passed a list of visible repo IDs, grab the ids of the private repos accessible to the user
          if visible_repo_ids.nil?
            private_repository_ids = repositories.reject(&:public?).map(&:id)
            visible_repo_ids = Set.new(user.associated_repository_ids(include_oauth_restriction: false, repository_ids: private_repository_ids))
          end

          subscriptions.each do |subscription|
            repo = repositories_by_id[subscription.list_id]

            if repo.nil? || repo.deleted
              bad_ids << subscription.list_id
              next
            end

            if repo.public? || visible_repo_ids.include?(repo.id)
              good << repo
            else
              bad_ids << subscription.list_id
            end
          end
        ensure
          if bad_ids.present?
            CleanupListNotificationsJob.perform_later(user.id, ::Repository.name, bad_ids.to_a)
            GitHub.dogstats.count(
              "newsies.cleanup.inaccessible_lists",
              bad_ids.length,
              tags: ["method:subscribed_repositories"],
            )
          end
        end

        good
      end
    end

    # Public: Count a User's subscriptions.
    #
    #     # all subscribed and ignored
    #     GitHub.newsies.count_subscriptions(kyle)
    #
    #     # all ignored
    #     GitHub.newsies.count_subscriptions(kyle, set: :ignored)
    #
    #     # all subscribed
    #     GitHub.newsies.count_subscriptions(kyle, set: :subscribed)
    #
    # user - A User.
    # set  - Optional value to specify the set of subscriptions:
    #        :all        - All subscriptions and ignores (default).
    #        :subscribed - All subscriptions.
    #        :ignored    - All ignores.
    # excluding - An Array of id's to exclude.
    # list_type - String that matches the excluded list id's type. Defaults to
    #             "Repository" for backwards compatibility.
    #
    # Returns a Newsies::Responses::Count instance.
    def count_subscriptions(user, set: nil, excluding: nil, list_type: "Repository")
      raise ArgumentError, "list_type must be present" unless list_type.present?

      Responses::Count.new {
        if excluding
          excluding = excluding.map { |id| Newsies::List.new(list_type, id) }
        end
        ListSubscription.count_subscriptions(user.id, list_type: list_type, set: set, excluding: excluding)
      }
    end

    # Public: List Subscribers for a List.
    #
    # list    - A List.
    # options - An optional or Hash.
    #           :page - Integer page number.  Default: 1
    #
    # Returns a Newsies::Responses::Array instance.
    def subscribers(list, options = {})
      Responses::Array.new {
        newsies_list = Newsies::List.to_object(list)
        subscribers = ListSubscription.subscribers(newsies_list, options)
        user_ids = subscribers.map { |sub| sub.user_id.to_i }
        users = users_by_ids(user_ids).index_by { |u| u.id }
        found = []

        cleaned_up = 0;

        user_ids.each do |user_id|
          if user = users[user_id]
            found << user
          else
            # If user doesn't exist, cleanup their subscriptions
            async_delete_all_for_user(user_id)
            cleaned_up += 1
          end
        end

        GitHub.dogstats.count("newsies.cleanup.deleted_users", cleaned_up, tags: ["method:subscribers"]) if cleaned_up > 0
        found
      }
    end

    def subscriber_ids_by_cursor(list, options = {})
      newsies_list = Newsies::List.to_object(list)
      Responses::Array.new do
        subscribers = ListSubscription.subscribers_by_cursor(newsies_list, options)
        subscribers.map { |subscriber| subscriber.user_id.to_i }
      end
    end

    # Public: Count users subscribing to an Entity.
    #
    # list - A Repository or Team
    #
    # Returns a Newsies::Responses::Count instance.
    def count_subscribers(list, options = {})
      Responses::Count.new {
        newsies_list = Newsies::List.to_object(list)
        ListSubscription.count(newsies_list, options[:exclude_spammy_users])
      }
    end

    # Internal: Initializes a Newsies::SubscriberSet for the notification delivery.
    #
    # list   - A List (Repository, Team, etc.).
    # thread - A Thread (Issue, Commit, DiscussionPost, etc.)
    #
    # Returns a Newsies::Response wrapping a Newsies::SubscriberSet or Newsies::EmptySubscriberSet
    # in case it failed due to underlying storage unavailability
    def subscriber_set_for(list:, thread: nil, explicit_subscribers: nil)
      Response.new(Newsies::EmptySubscriberSet.instance) do
        newsies_list = Newsies::List.to_object(list)
        newsies_thread = if thread
          Newsies::Thread.to_object(thread, list: newsies_list)
        end

        Newsies::SubscriberSet.new(
          list: newsies_list,
          thread: newsies_thread,
          explicit_subscribers: explicit_subscribers)
      end
    end

    # Public: Lists subscribers for a list.
    #
    # list   - A Repository or a Team.
    # thread - Optional thread (Issue, Discussion, or Commit)
    #
    # Yields a Newsies::Subscriber.
    # Returns Newsies::Response.
    def each_subscriber(list, thread = nil, comment = nil, &block)
      Response.new { subscribers_with_preloaded_settings(list, thread).value!.each(&block) }
    end

    # Public: Load default settings for a user.
    #
    # user - A User object
    #
    # Returns a Newsies::Settings
    def default_user_settings(user)
      Newsies::Settings.new(user.id).tap do |settings|
        delivery_type = default_user_notification_delivery_type(user)
        settings.participating_settings.replace delivery_type
        settings.subscribed_settings.replace delivery_type

        settings.auto_subscribe = user.created_at < DEFAULT_AUTO_SUBSCRIBE_CUTOFF_DATE
        settings.email(:global, user.email)
        settings.user = user
      end
    end

    def web_notifications_enabled?(user)
      settings = settings(user)
      settings.settings_enabled_for?(Newsies::HANDLER_WEB)
    end

    # Public
    def handler_keys
      handlers.map { |h| h.handler_key }
    end

    # Public: Loads the Thread object for the given Repository.
    #
    # repository - A Repository.
    # klass      - String class name for the Thread.
    # id         - String or Integer ID of the Thread.
    # actor      - User trying to load the thread.
    #
    # Returns a Thread object (Issue, Discussion, or Commit).
    def thread(repository, klass, id, actor:)
      return unless repository
      case klass.to_s
      when /issue/i
        repository.issues.find_by_id(id.to_i)
      when /commit/i
        return if !repository.commits.exist?(id)
        repository.commits.find(id)
      when /discussion/i
        repository.discussions.find_by_id(id)
      end
    end

    # Public: Find all subscribed threads for the given User and Repository.
    #
    # user  - A User.
    # lists - An Array of lists.
    # thread_type - String class name for the Thread.
    #
    # Returns an Newsies::Response::Array instance
    def subscribed_threads(user, lists, thread_type, force_user_id_index = false)
      Responses::Array.new {
        newsies_lists = Newsies::List.to_objects(Array(lists))
        ThreadSubscription.subscribed_threads(user.id, newsies_lists, thread_type, force_user_id_index)
      }
    end

    # Public: Get the email address that emails should go to
    # for a given user and organization.
    #
    # user - a User
    # org  - an Organization or nil
    #
    # Returns a Newsies::Response instance
    #   If an org is passed and the user has specified an email
    #   address for that org, it will be returned. Otherwise the
    #   user's default/primary email address is returned.
    def email(user, org = nil)
      Newsies::Response.new do
        org = nil unless org.is_a?(Organization)
        settings(user).value!.email(org || :global).address
      end
    end

    def token(action, user, id, extra_data = {})
      Newsies::Authentication.token(action, user, id, extra_data)
    end

    def user_and_id_from_token(type, token)
      Newsies::Authentication.user_and_id_from_token(type, token)
    end

    def valid_unsubscribe_token?(token, user, repository)
      Newsies::Authentication.valid_unsubscribe_token?(token, user, repository)
    end

    # Takes a symbol from Newsies::Service#valid_reason_from and makes
    # it fit into a sentence lead with "because ______". Used to give people
    # a reason they subscribed to something in plain ol English.
    def reason_in_words(reason, email: false)
      case reason
      when "comment"                  then "you commented"
      when "author"                   then "you authored the thread"
      when "mention"                  then "you were mentioned"
      when "team_mention"             then "#{reference_individual(email)} on a team that was mentioned"
      when "assign"                   then "you were assigned"
      when "review_requested"         then "your review was requested"
      when "state_change"             then "you modified the open/close state"
      when "security_alert"           then "you have alerting access"
      when "ci_activity"              then "this workflow ran on your branch"
      when "security_advisory_credit" then "you were given credit for contributing to a Security Advisory"
      else "#{reference_individual(email)} subscribed to this thread" # manual, catch-all
      end
    end

    # Public: Do a bulk load of Settings for a list of IDs.
    #
    # user_ids - Array of Integer User IDs.
    #
    # Returns a Newsies::Responses::Array instance.
    def load_user_settings(user_ids)
      users = users_by_ids(user_ids)

      Newsies::Responses::Array.new {
        user_ids = users.map { |u| u.id }

        settings = @settings_store.get_all(*user_ids).
          reject { |settings| settings.new? }.
          index_by { |u| u.id }

        users.map do |user|
          setting = settings[user.id] || default_user_settings(user)
          setting.user = user
          setting
        end
      }
    end

    def bulk_loaded_subscribers(subscribers)
      return Responses::Array.new if subscribers.blank?

      Responses::Array.new do
        user_ids = subscribers.map { |s| s.user_id.to_i }
        settings = load_user_settings(user_ids).value!.index_by { |s| s.id }

        subscribers.each do |sub|
            subscriber_settings = settings[sub.user_id]
            sub.settings = subscriber_settings
            sub.user = subscriber_settings&.user
        end
      end
    end

    def inspect
      "<#%s:%d>" % [self.class, object_id]
    end

    # List subscribers for a list and preloads their notification settings.
    #
    # list                  - A Repository or a Team.
    # thread                - Optional thread (Issue, Commit, DiscussionPost, etc.)
    # explicit_subscribers  - Optional list of users to be subscribed, that
    #                         overwrite the stored subscriptions
    #
    # Returns a Newsies::Responses::Array with Newsies::Subscriber objects.
    def subscribers_with_preloaded_settings(list, thread = nil, explicit_subscribers = nil)
      subscriber_set = subscriber_set_for(
        list: list,
        thread: thread,
        explicit_subscribers: explicit_subscribers,
      ).value!
      bulk_loaded_subscribers(subscriber_set.subscribers)
    end

    # user_id      - Integer User ID.
    # device_token - String device token.
    # service      - (Optional) Symbol for the platform notification service for which we should
    #                search for the token. Defaults to :fcm.
    #
    # Returns Newsies::Response<MobileDeviceToken>
    def mobile_device_token(user_id, device_token, service: :fcm)
      Response.new do
        MobileDeviceToken.find_by(
          user_id: user_id,
          service: service,
          device_token: device_token,
        )
      end
    end

    # user_id - Integer User ID.
    # service - (Optional) Symbol for the platform notification service to which to restrict
    #           results. Passing `nil` will remove the filter on the `service` column.
    #           Defaults to :fcm.
    #
    # Returns Newsies::Responses::Array<MobileDeviceToken>
    def mobile_device_tokens(user_id, service: :fcm)
      Responses::Array.new do
        raise ArgumentError.new("`user_id` cannot be nil") unless user_id.present?
        MobileDeviceToken
          .where({ user_id: user_id, service: service.presence }.compact)
          .order(id: :desc)
          .all
      end
    end

    # Creates a new device token for the given user, or returns the existing one if one matches
    # the given attributes exactly.
    #
    # In the case that we find an existing device token, we touch the `updated_at` timestamp in
    # order to track the faux creation. Note that otherwise MobileDeviceToken objects are immutable.
    #
    # user_id      - Integer User ID.
    # device_token - String device token.
    # device_name  - (Optional) String name of the device new token.
    # service      - (Optional) Symbol for the platform notification service for which to create the
    #                new token. Defaults to :fcm.
    #
    # Returns a Newsies::Response<MobileDeviceToken> object, which may or may not be persisted.
    def add_mobile_device_token(user_id:, device_token:, device_name: nil, service: :fcm)
      Response.new do
        existing_token = MobileDeviceToken.find_by(
          user_id: user_id,
          service: service,
          device_token: device_token,
        )

        if existing_token&.update(device_name: device_name)
          unless existing_token.changed?
            existing_token.touch
            existing_token.reload
          end
        end

        if existing_token
          existing_token
        else
          MobileDeviceToken.create(user_id: user_id, service: service, device_token: device_token, device_name: device_name)
        end
      end
    end

    # id - Integer MobileDeviceToken ID.
    #
    # Returns Newsies::Response<MobileDeviceToken> object for destroyed token, which may or may not
    # have actually been destroyed, or nil if it doesn't exist.
    def destroy_mobile_device_token(id)
      Response.new do
        begin
          MobileDeviceToken.destroy(id)
        rescue ActiveRecord::RecordNotFound
        end
      end
    end

    # user_id - Integer User ID.
    # service - (Optional) Symbol for the platform notification service to which to restrict
    #           the deletion query. Passing `nil` will cause tokens for all services to be deleted.
    #           Defaults to :fcm.
    #
    # Returns Newsies::Responses::Array<MobileDeviceToken> for destroyed tokens, which may or
    # may not have actually been destroyed depending on validation errors.
    def destroy_mobile_device_tokens(user_id, service: :fcm)
      Responses::Array.new do
        raise ArgumentError.new("`user_id` cannot be nil") unless user_id.present?
        MobileDeviceToken.destroy_by(**{user_id: user_id, service: service.presence}.compact)
      end
    end

    # Enqueues a background job that will update the spam status of all notifications
    # related to the given thread
    #
    # list    - A list object e.g. a Repository, Team, etc.
    # thread  - A thread object e.g an Issue, Commit, DiscussionPost, etc.
    def async_update_notifications_spam_status(list, thread)
      newsies_list = List.to_object(list)
      newsies_thread = Thread.to_object(thread)

      UpdateNotificationsSpamStatusJob.perform_later(newsies_list.type, newsies_list.id, newsies_thread.type, newsies_thread.id)
    end

    # Enqueues a background job that will cleanup notifications for a thread which has been
    # found to be inaccessible by a user
    #
    # user_id     - Integer User ID that the thread was found to be inaccessible for
    # list_type   - String class name of the list the inaccessible thread is in (e.g. "Repository")
    # list_id     - String/Integer id of the list the inaccessible thread is in
    # thread_type - String class name of the inaccessible thread (e.g. "Issue")
    # thread_type - String/Integer thread id of the inaccessible thread
    def async_cleanup_inaccessible_thread_for_user(user_id, list_type, list_id, thread_type, thread_id)
      CleanupInaccessibleThreadForUserJob.perform_later(user_id, list_type, list_id, thread_type, thread_id)
    end

    private

    # Internal: default handlers for a user
    def default_user_notification_delivery_type(user)
      if user.recently_created?(DEFAULT_EMAIL_ONLY_NOTIFICATIONS_CUTOFF_DATE)
        [Newsies::HANDLER_EMAIL]
      else
        [Newsies::HANDLER_EMAIL, Newsies::HANDLER_WEB]
      end
    end


    # Internal: Get a real User from a User or Settings object.
    def real_user(user_or_settings)
      if user_or_settings.is_a?(User)
        user_or_settings
      else
        user_or_settings.user
      end
    end

    # Determines if the reason should overwrite existing reasons.
    #
    # reason - Symbol reason from #valid_reason_from.
    #
    # Returns a Boolean.
    def forced_reason?(reason)
      !Newsies::Reasons::LowPriority.include?(reason)
    end

    # Internal: This function identifies if a notification is being sent through
    # the UI or through an email, and changes the way a person is addressed.
    # Apparently, sending email bodies with apostrophes ruins everything.
    #
    # Returns a String.
    def reference_individual(email)
      email ? "you are" : "you’re"
    end

    def users_by_ids(ids)
      User.includes(:primary_user_email).where(id: ids)
    end

    def async_notify_list_subscription_state_change(user_id, newsies_list)
      Newsies::NotifyListSubscriptionStatusChangeJob.perform_later(user_id, newsies_list.type, newsies_list.id)
    end
  end
end
