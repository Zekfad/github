# Newsies

Newsies is the system that powers GitHub notifications.

## Overview

Accessed via `GitHub.newsies`, implemented in the `Newsies::Service` class.

### NotificationsContent

The logic to create subscriptions and trigger notifications lives inside of the [`NotificationsContent`](https://github.com/github/github/blob/master/app/models/notifications_content.rb) module, which itself is composed of three modules:

* [`Summarizable`](https://github.com/github/github/blob/master/app/models/summarizable.rb)
* [`SubscribableThread`](https://github.com/github/github/blob/master/app/models/subscribable_thread.rb)
* [`Mentionable`](https://github.com/github/github/blob/master/app/models/mentionable.rb)

The `Summarizable` module handles delivering notifications to subscribers (it also denormalizes data about the source of the notification as part of that process). Notifications triggered via `Summarizable#deliver_notifications` pass `self` as the source of the notification.

The `SubscribableThread` module handles the general ability to subscribe to, unsubscribe from, and ignore any model it is mixed into. `SubscribableThread` models must implement the `notifications_thread`, `notifications_author`, and `async_notifications_list` methods. As the `notifications_thread` method hints at, it treats all objects it is mixed into like a threaded conversation.

The `Mentionable` module handles subscribing and delivering notifications for users mentioned in a conversation.  It is tightly coupled to `Subscribable` in that it depends on the `subscribe` method.

Notification behavior is handled automatically via a series of ActiveRecord callbacks. Models can include the `NotificationsContent::WithoutCallbacks` if they wish to handle the lifecycle of notifications manually, or `NotificationsContent::WithCallbacks` if they wish for notifications to be handled automatically.

The models that implement notification functionality are:

| Model | `notifications_thread` | `notifications_list` | `NotificationsContent::WithCallbacks` | `NotificationsContent::WithoutCallbacks` |
| ----- | -------------- | ------------- | ------ | --- |
| `CommitComment` | `#commit` | `#repository` | x | |
| `CommitMention` | `#commit` | `#repository` | x | |
| `DiscussionPost` | `#self` | `#team` | x | |
| `DiscussionPostReply` | `#discussion_post` | `#team` | x | |
| `Issue` | `#self` | `#repository` | x | |
| `IssueComment` | `#issue` | `#repository` | x | |
| `PullRequestReview` | `#issue` | `#repository` | | x |
| `PullRequestReviewComment` | `#issue` | `#repository` | x | |
| `Release` | `#self` | `#repository` | | x |
| `RepositoryAdvisory` | `#self` | `#repository` | x | |
| `RepositoryVulnerabilityAlert` | `#self` | `#repository` | | x |

There are some notable exceptions to how notifications are handled across the various models:

- `Issue` overrides `Summarizable#deliver_notifications` and triggers the notification with the pull request as the source if the issue is attached to a pull request.

- [Some (but not all)](https://github.com/github/github/blob/a5bbe5def13f05fd546f49c502e61462c1a9d328/app/models/issue_event.rb#L110-L120) `IssueEvent`s trigger subscription via their issue's `subscribe` method and update rollup summaries and trigger notifications directly rather than going through `Summarizable`. Unlike `Summarizable` models, issue events don't pass themselves as the source of the notification; they create an [`IssueEventNotification` and pass that instead](https://github.com/github/github/blob/a5bbe5def13f05fd546f49c502e61462c1a9d328/app/models/issue_event.rb#L1094).

- Similarly, `PullRequestPushNotification`s are created as part of the post-receive job and trigger notifications directly, passing themselves ([ref](https://github.com/github/github/blob/a5bbe5def13f05fd546f49c502e61462c1a9d328/app/models/push.rb#L231)).


### Threads

A notification thread is an object that notifies users when activity occurs. All notifications belong to a "thread." A notification thread is an object that generates notifications for users. Notification threads also act as the "subject" for subsequent activity that might occur on that object (e.g., comments on an Issue). This behavior allows us to group notifications in the web UI. The most common notification thread types are:

- Issues (the thread for a PullRequest is just the Issue)
- Commits (you can comment on individual commits)
- DiscussionPosts (aka Team Discussions)

There are a few other, slightly less common notification thread types:

- Releases (we notify watchers of a repo when a new Release is created)
- RepositoryInvitations

### Lists

Every notification also belongs to a list, which acts as a secondary grouping mechanism for notifications. Notifications lists are currently either a `Repository` or a `Team`. You can see the list grouping manifest in the web UI, where we group notifications by the repo and/or team on the left column.

### RollupSummaries

Every notification also has a `RollupSummary`, which lets us denormalize a common set of information for every notification and minimize reads on the main `mysql1` cluster. As activity occurs over the lifecycle of a notification thread (e.g., comments on Issues and PRs) the corresponding `RollupSummary` will be updated to capture data about the participants and comments on the thread.

Here's an example of the data we capture in a RollupSummary for a PullRequest with a review request and a comment:

```ruby
{
 "authors"=>{"548"=>"mikesea", "3"=>"defunkt"},
 "items"=>
  {"Issue:3979"=>
    {"permalink"=>"http://github.localhost/mikesea/test-repo/pull/1",
     "created_at"=>"1516210269",
     "user_id"=>"548",
     "body"=>
      "Include more useful details in the description.\r\n" +
      "\r\n" +
      "/cc @defunkt"},
   "IssueEventNotification:22090"=>
    {"permalink"=>
      "http://github.localhost/mikesea/test-repo/pull/1#event-22090",
     "created_at"=>"1516210429",
     "user_id"=>"548",
     "body"=>
      "@mikesea requested your review on: mikesea/test-repo#1 Update README.md."},
   "IssueComment:13962"=>
    {"permalink"=>
      "http://github.localhost/mikesea/test-repo/pull/1#issuecomment-13962",
     "created_at"=>"1516210519",
     "user_id"=>"3",
     "body"=>"Yes!"}},
 "updated_at"=>2018-01-17 17:35:22 UTC,
 "is_pull_request"=>true,
 "title"=>"Update README.md",
 "issue_number"=>1,
 "number"=>1,
 "creator_id"=>"548",
 "issue_state"=>"open"
}
```

## MySQL Schema

Subscriptions are split into two tables:

#### For watching/ignoring a list (repository).

```
notification_subscriptions:
  user_id
  list_type
  list_id
  ignored
  notified
  created_at
```

Some example entries:

```sql
mysql> select * from notification_subscriptions order by created_at desc limit 4;
+----+---------+------------+---------+---------+----------+---------------------+
| id | user_id | list_type  | list_id | ignored | notified | created_at          |
+----+---------+------------+---------+---------+----------+---------------------+
|  1 |       9 | Repository |       1 |       0 |        1 | 2018-01-08 12:10:13 |
|  2 |      15 | Repository |       2 |       0 |        1 | 2018-01-08 12:10:13 |
|  3 |      28 | Team       |       3 |       0 |        1 | 2018-01-08 12:10:13 |
|  4 |      13 | Team       |       4 |       0 |        1 | 2018-01-08 12:10:13 |
+----+---------+------------+---------+---------+----------+---------------------+
```

#### For subscribing to/ignoring a thread (Issue, PR, Commit comments) in a list.

```
notification_thread_subscriptions:
  user_id
  list_type
  list_id
  ignored
  reason
  thread_key
  created_at
```

Thread_key is "ObjectType;id". Some example entries:

```sql
mysql> select * from notification_thread_subscriptions order by created_at desc;
+-----------+---------+------------+---------+---------+------------------+---------------------+---------------------+
| id        | user_id | list_type  | list_id | ignored | reason           | thread_key          | created_at          |
+-----------+---------+------------+---------+---------+------------------+---------------------+---------------------+
| 844311484 | 1243633 | Repository |       3 |       0 | review_requested | Issue;287586382     | 2018-01-10 21:39:16 |
| 843750677 | 1243633 | Repository |       3 |       0 | team_mention     | Issue;273343491     | 2018-01-10 17:32:36 |
| 800182441 | 1243633 | Team       | 2007905 |       0 | team_mention     | DiscussionPost;7007 | 2017-12-12 23:42:24 |
| 793420214 | 1243633 | Team       | 2533509 |       0 | mention          | DiscussionPost;6229 | 2017-12-08 20:30:34 |
+-----------+---------+------------+---------+---------+------------------+---------------------+---------------------+
```

#### Web notifications, 1 row per thread per user. Only populated if the user has enabled web notifications.

```
notification_entries:
  id
  user_id
  summary_id
  list_type
  list_id
  thread_key
  unread
  reason
  updated_at
  last_read_at
```

Reason is copied from the subscription reason. The subscription reason is the reason you were first notified, unless a more important reason overwrote it later in the thread.

Thread key is "list_id;ObjectType;id". Some example entries:

```sql
mysql> select * from notification_entries order by updated_at desc limit 4;
+-------------+---------+------------+------------+---------+-----------------------------+--------+------------------+---------------------+---------------------+
| id          | user_id | summary_id | list_type  | list_id | thread_key                  | unread | reason           | updated_at          | last_read_at        |
+-------------+---------+------------+------------+---------+-----------------------------+--------+------------------+---------------------+---------------------+
| 34752076746 | 1243633 |  285917412 | Repository |       3 | 3;Issue;282244603           |      1 | review_requested | 2018-01-11 19:57:25 | 2018-01-11 19:45:11 |
| 34679791345 | 1243633 |  288639203 | Repository |       3 | 3;Issue;284624753           |      1 | team_mention     | 2018-01-11 19:45:02 | 2018-01-09 23:37:52 |
| 34743281365 | 1243633 |  291179549 | Repository |       3 | 3;Issue;286839272           |      1 | team_mention     | 2018-01-11 19:34:57 | 2018-01-10 21:03:18 |
| 34895976362 | 1243633 |  292325739 | Team       | 2445281 | 2445281;DiscussionPost;9714 |      1 | subscribed       | 2018-01-11 19:28:18 | NULL                |
+-------------+---------+------------+------------+---------+-----------------------------+--------+------------------+---------------------+---------------------+
```

#### Delivery of notifications (email and web); a log of each individual web and email notification that has been delivered.

```
notification_deliveries
  id
  delivered_at
  list_id
  thread_key
  comment_key
  user_id
  handler
  reason
```

Where `comment_key` is the reference to the source object that triggered the notification. Some example entries:

```sql
console_ro2 12:15:19 mysql2-slow: github_production> select * from notification_deliveries order by delivered_at desc;
+-------------+---------------------+---------+---------------------+-----------------------------------------------------------+---------+---------+------------------+
| id          | delivered_at        | list_id | thread_key          | comment_key                                               | user_id | handler | reason           |
+-------------+---------------------+---------+---------------------+-----------------------------------------------------------+---------+---------+------------------+
| 58263947514 | 2018-01-11 19:57:27 |       3 | Issue;282244603     | PullRequestPushNotification;Pull#158467222Push#2248410240 | 1243633 | email   | review_requested |
| 58263947515 | 2018-01-11 19:57:27 |       3 | Issue;282244603     | PullRequestPushNotification;Pull#158467222Push#2248410240 | 1243633 | web     | review_requested |
| 58262917216 | 2018-01-11 19:45:05 |       3 | Issue;284624753     | PullRequestReview;88284268                                | 1243633 | email   | team_mention     |
| 58262917217 | 2018-01-11 19:45:05 |       3 | Issue;284624753     | PullRequestReview;88284268                                | 1243633 | web     | team_mention     |
| 58262058639 | 2018-01-11 19:34:59 |       3 | Issue;286839272     | PullRequestReview;88281323                                | 1243633 | email   | team_mention     |
| 58262058640 | 2018-01-11 19:34:59 |       3 | Issue;286839272     | PullRequestReview;88281323                                | 1243633 | web     | team_mention     |
| 58261441743 | 2018-01-11 19:28:29 | 2445281 | DiscussionPost;9714 | DiscussionPostReply;18407                                 | 1243633 | email   | list             |
| 58261441744 | 2018-01-11 19:28:29 | 2445281 | DiscussionPost;9714 | DiscussionPostReply;18407                                 | 1243633 | web     | list             |
| 58260752819 | 2018-01-11 19:21:18 |       3 | Issue;286839272     | IssueComment;357033073                                    | 1243633 | email   | team_mention     |
| 58260752820 | 2018-01-11 19:21:18 |       3 | Issue;286839272     | IssueComment;357033073                                    | 1243633 | web     | team_mention     |
| 58260636851 | 2018-01-11 19:20:14 |       3 | Issue;286839272     | PullRequestReview;88276917                                | 1243633 | email   | team_mention     |
| 58260636852 | 2018-01-11 19:20:14 |       3 | Issue;286839272     | PullRequestReview;88276917                                | 1243633 | web     | team_mention     |
| 58260385688 | 2018-01-11 19:17:37 |       3 | Issue;286839272     | PullRequestPushNotification;Pull#161693374Push#2248318839 | 1243633 | email   | team_mention     |
| 58260385689 | 2018-01-11 19:17:37 |       3 | Issue;286839272     | PullRequestPushNotification;Pull#161693374Push#2248318839 | 1243633 | web     | team_mention     |
| 58260114799 | 2018-01-11 19:14:38 |       3 | Issue;286839272     | PullRequestPushNotification;Pull#161693374Push#2248311968 | 1243633 | email   | team_mention     |
| 58260114800 | 2018-01-11 19:14:38 |       3 | Issue;286839272     | PullRequestPushNotification;Pull#161693374Push#2248311968 | 1243633 | web     | team_mention     |
| 58259535259 | 2018-01-11 19:08:19 |       3 | Issue;252502958     | IssueComment;357029377                                    | 1243633 | email   | team_mention     |
| 58259535260 | 2018-01-11 19:08:19 |       3 | Issue;252502958     | IssueComment;357029377                                    | 1243633 | web     | team_mention     |
| 58259462912 | 2018-01-11 19:07:10 |       3 | Issue;286839272     | PullRequestReview;88270655                                | 1243633 | web     | team_mention     |
| 58259462911 | 2018-01-11 19:07:10 |       3 | Issue;286839272     | PullRequestReview;88270655                                | 1243633 | email   | team_mention     |
+-------------+---------------------+---------+---------------------+-----------------------------------------------------------+---------+---------+------------------+
```

#### Summary data to display on web notifications page built when notification is generated:

```
rollup_summaries
  id
  list_type
  list_id
  raw_data
  thread_key
```

Where `raw_data` is a serialized_attributes column that stores metadata about the notification to minimize the need to read data from the main MySQL cluster to display a notification.

#### Settings for notifications, email, etc.

```
notification_user_settings
  id
  raw_data
  auto_subscribe
  auto_subscribe_teams
  notify_own_via_email
  participating_web
  participating_email
  subscribed_web
  subscribed_email
  notify_comment_email
  notify_pull_request_review_email
  notify_pull_request_push_email
  vulnerability_ui_alert
  vulnerability_cli
  vulnerability_web
  vulnerability_email
```

### Data retention

  * `notification_entries`
    * unread, read and archived: 5 months
    * saved: no policy
  * `notification_deliveries`
    * 1 month

[source](https://github.com/github/puppet/blob/ba11720e2d1918d8a252b74837efa64620dd297d/hieradata/mysql/cluster/notifications/master.yaml)

## Issues

* tightly coupled to threaded convos in repositories; inflexible for other types of notifications
* reason is per thread, rather than per notification; can't distinguish between that reason occurring in the first comment vs. in the most recent comment
* overly abstracted backend in lib/newsies that reeks of YAGNI - reimplements persistence layer, uses strange patterns like thread_key in mysql for portability to Redis, etc.
