# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class NotificationsEntries < Base
    self.abstract_class = true

    connects_to database: { writing: :notifications_entries_primary, reading: :notifications_entries_readonly }
  end
end
