# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Mysql5 < Base
    self.abstract_class = true

    connects_to database: { writing: :kv_primary, reading: :kv_readonly }
  end
end
