# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Repositories < Base
    self.abstract_class = true

    connects_to database: { writing: :repositories_primary, reading: :repositories_readonly, reading_slow: :repositories_readonly_slow }

    def self.gtid_tracking_enabled?
      true
    end

    def self.throttler_cluster_name
      config = if GitHub.rails_6_0?
        connection.pool.spec.config
      else
        connection.pool.db_config.configuration_hash
      end

      repositories_hosts = [
        GitHub.environment["MYSQL_REPOSITORIES_WRITER_HOST"],
        GitHub.environment["MYSQL_REPOSITORIES_READER_HOST"]
      ]

      if repositories_hosts.include?(config[:host])
        super
      else
        Mysql1.throttler_cluster_name
      end
    end
  end
end
