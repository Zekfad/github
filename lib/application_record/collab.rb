# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Collab < Base
    self.abstract_class = true

    connects_to database: { writing: :collab_primary, reading: :collab_readonly }
  end
end
