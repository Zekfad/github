# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Ballast < Base
    self.abstract_class = true

    connects_to database: { writing: :ballast_primary, reading: :ballast_readonly }
  end
end
