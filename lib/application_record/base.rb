# frozen_string_literal: true

require "github/areas_of_responsibility"
require "active_record_has_many_split_through"

module ApplicationRecord
  class Base < ActiveRecord::Base
    self.abstract_class = true

    DEFAULT_LIVE_UPDATES_WAIT_TIME_MILLIS = 5000

    autoload :GitHubSQLBuilder, "application_record/base/github_sql_builder"
    autoload :ThrottlerBuilder, "application_record/base/throttler_builder"
    autoload :BaseHelpers,      "application_record/base/base_helpers"

    include GitHub::AreasOfResponsibility
    include GitHub::Relay::GlobalIdentification

    include ThrottlerBuilder::DelegateMethods
    include BaseHelpers::Helpers

    # Scope for querying when a field matches one of many possible values
    # You can use `where(field: [value1, value2])` as well, but when your array
    # is very large this performs better by avoiding Arel object allocations.
    scope :with_ids, ->(ids, field: "#{quoted_table_name}.#{quoted_primary_key}") { ids.any? ? where("#{field} IN (?)", ids) : none }
    singleton_class.send(:alias_method, :with_primary_keys, :with_ids)

    # Scope for querying when a field does not match one of many possible values
    # You can use `where(field: [value1, value2])` as well, but when your array
    # is very large this performs better by avoiding Arel object allocations.
    scope :without_ids, ->(ids, field: "#{quoted_table_name}.#{quoted_primary_key}") { ids.any? ? where("#{field} NOT IN (?)", ids) : self }

    # Tell mysql to kill the query if it runs too long.  Intended as a safety
    # valve to make sure that inadvertently slow queries time out quickly
    # rather than in the timeout middleware where visibility is poor.
    scope :limit_execution_time, -> (limit_ms: 500) { optimizer_hints("MAX_EXECUTION_TIME(#{limit_ms})") }

    def self.belongs_to(name, scope = nil, **options)
      define_async_reflection_association(name)
      super
    end

    def self.has_one(name, scope = nil, **options)
      define_async_reflection_association(name)
      super
    end

    def self.has_many(name, scope = nil, **options, &block)
      define_async_reflection_association(name)
      super
    end

    def self.has_and_belongs_to_many(name, scope = nil, **options, &block)
      define_async_reflection_association(name)
      super
    end

    def self.define_async_reflection_association(name)
      generated_async_reflection_associations.module_eval do
        define_method("async_#{name}") do
          ::Platform::Loaders::ActiveRecordAssociation.load(self, name)
        end
      end
    end

    def self.generated_async_reflection_associations
      :GeneratedAsyncReflectionAssociations.yield_self do |name|
        const_defined?(name, false) ? const_get(name) : const_set(name, Module.new.tap { |mod| include mod })
      end
    end
    private_class_method :generated_async_reflection_associations

    def self.scoped
      all
    end

    def reset_memoized_attributes
    end

    def reload(*)
      reset_memoized_attributes
      super
    end

    def self.cluster_names
      [cluster_name]
    end

    def cluster_names
      self.class.cluster_names
    end

    def self.cluster_name
      return @cluster_name if defined?(@cluster_name)

      @cluster_name = ancestors.find { |ancestor|
        ancestor.is_a?(Class) && ancestor.superclass == ApplicationRecord::Base
      }.name.demodulize.underscore.to_sym
    end

    def cluster_name
      self.class.cluster_name
    end

    def self.throttler_cluster_names
      [throttler_cluster_name]
    end

    def throttler_cluster_names
      self.class.throttler_cluster_names
    end

    def self.throttler_cluster_name
      cluster_name
    end

    def throttler_cluster_name
      self.class.throttler_cluster_name
    end

    def self.default_github_sql_options
      {}
    end

    def self.github_sql
      GitHubSQLBuilder.new(connection, default_github_sql_options)
    end

    def github_sql
      self.class.github_sql
    end

    def self.github_sql_descending_batched_between(start:, finish:, batch_size: 1000)
      GitHub::SQL::DescendingBatchedBetween.new(start: start, finish: finish, batch_size: batch_size, query_builder: github_sql) # rubocop:disable GitHub/DoNotCallMethodsOnGitHubSQLDescendingBatchedBetween
    end

    def self.github_sql_batched_between(start:, finish:, batch_size: 1000)
      GitHub::SQL::BatchedBetween.new(start: start, finish: finish, batch_size: batch_size, query_builder: github_sql) # rubocop:disable GitHub/DoNotCallMethodsOnGitHubSQLBatchedBetween
    end

    def self.github_sql_batched(start: 0, limit: 1000)
      GitHub::SQL::Batched.new(start: start, limit: limit, query_builder: github_sql) # rubocop:disable GitHub/DoNotCallMethodsOnGitHubSQLBatched
    end

    def self.gtid_tracking_enabled?
      false
    end

    def self.default_live_updates_wait
      delay = throttler_cluster_names.map do |name|
        Freno.client.replication_delay(store_name: name)
      end
      delay.max * 1000 + 100
    rescue Freno::Error => e
      Failbot.report(e) if GitHub.environment.fetch("GH_FRENO_UNAVAILABLE", 0) == 0
      DEFAULT_LIVE_UPDATES_WAIT_TIME_MILLIS
    end

    def default_live_updates_wait
      # We only want to memoize this this value in single instances.
      # Never move this memoization into the class level method.
      @live_updates_wait ||= begin
        self.class.default_live_updates_wait
      end
    end
  end
end
