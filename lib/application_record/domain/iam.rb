# frozen_string_literal: true

require_relative "../iam"

module ApplicationRecord
  module Domain
    class Iam < ApplicationRecord::Iam
      self.abstract_class = true
    end
  end
end
