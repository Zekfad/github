# frozen_string_literal: true

require_relative "../memex"

module ApplicationRecord
  module Domain
    class Memexes < ApplicationRecord::Memex
      self.abstract_class = true
    end
  end
end
