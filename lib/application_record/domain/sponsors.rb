# frozen_string_literal: true

require_relative "../collab"

module ApplicationRecord
  module Domain
    class Sponsors < ApplicationRecord::Collab
      self.abstract_class = true
    end
  end
end
