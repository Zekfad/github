# frozen_string_literal: true

require_relative "../repositories"

module ApplicationRecord
  module Domain
    class Repositories < ApplicationRecord::Repositories
      self.abstract_class = true
    end
  end
end
