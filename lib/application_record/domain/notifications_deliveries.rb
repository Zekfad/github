# frozen_string_literal: true

require_relative "../mysql2"
require_relative "../base/utc_record"

module ApplicationRecord
  module Domain
    class NotificationsDeliveries < ApplicationRecord::NotificationsDeliveries
      include ApplicationRecord::UTCRecord

      self.abstract_class = true
    end
  end
end
