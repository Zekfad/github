# frozen_string_literal: true

require_relative "../ballast"

module ApplicationRecord
  module Domain
    class Explore < ApplicationRecord::Ballast
      self.abstract_class = true
    end
  end
end
