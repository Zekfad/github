# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Mysql2 < Base
    self.abstract_class = true

    connects_to database: { writing: :notifications_primary, reading: :notifications_readonly }
  end
end
