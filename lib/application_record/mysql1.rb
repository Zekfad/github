# frozen_string_literal: true

require_relative "base"

module ApplicationRecord
  class Mysql1 < Base
    self.abstract_class = true

    connects_to_mysql1

    def self.gtid_tracking_enabled?
      true
    end
  end
end
