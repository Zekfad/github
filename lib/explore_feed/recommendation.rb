# frozen_string_literal: true

module ExploreFeed
  module Recommendation
    autoload :Developer, "explore_feed/recommendation/developer"
    autoload :Topic, "explore_feed/recommendation/topic"
  end
end
