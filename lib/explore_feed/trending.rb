# frozen_string_literal: true

module ExploreFeed
  module Trending
    autoload :Developer, "explore_feed/trending/developer"
    autoload :Repository, "explore_feed/trending/repository"
  end
end
