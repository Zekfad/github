# frozen_string_literal: true
# rubocop:disable Lint/RescueException

require "drb"
require "drb/unix" unless Gem.win_platform?

module ActiveSupport
  module Testing
    class Parallelization # :nodoc:
      class Worker
        MAX_FAILING_TESTS = 3

        def initialize(number:, url:)
          @number = number
          @worker_id = SecureRandom.uuid
          @url = url
          @to_record = []
          @failures = 0
        end

        def start
          fork do
            DRb.stop_service

            set_process_title("(starting)")

            @host = Socket.gethostname
            @queue_server = DRbObject.new_with_uri(@url)
            @queue_server.start_worker(@worker_id, @worker_id, @host)

            begin
              after_fork
            rescue => setup_exception; end

            if setup_exception
              @queue_server.record_exception(@worker_id, setup_exception)
            else
              work_from_queue
            end
          rescue Exception => e
            puts e
            puts e.backtrace
            @queue_server.record_exception(@worker_id, e)
            raise
          ensure
            set_process_title("(stopping)")

            run_cleanup
            @queue_server.stop_worker(@worker_id)

            Process.exit!
          end
        end

        private

        def after_fork
          Parallelization.after_fork_hooks.each do |cb|
            cb.call(@number)
          end
        end

        def run_cleanup
          Parallelization.run_cleanup_hooks.each do |cb|
            cb.call(@number)
          end
        end

        def run_job(klass, method, reporter)
          set_process_title("#{klass}##{method}")

          result = klass.with_info_handler reporter do
            Minitest.run_one_method(klass, method)
          end

          @failures += 1 if !result.passed? && !result.skipped?

          safe_record(reporter, result)

          set_process_title("(idle)")
        end

        def run_jobs(jobs)
          jobs.each do |job|
            if max_failures_reached?
              skip_job(*job)
            else
              run_job(*job)
            end
          end

          begin
            GitHub::SetupAndTeardown.last_suite_run&.run_teardown_once
          rescue => teardown_exception; end
        end

        def work_from_queue
          while jobs = @queue_server.pop(@worker_id)
            run_suite(jobs)
          end
        end

        def run_suite(jobs)
          fork_for_suite do
            run_jobs(jobs)
            bulk_record
          end
        end

        def fork_for_suite
          suite_pid = fork do
            begin
              if GitHub.rails_6_0? && defined?(ActiveRecord)
                # This code is copying the database connections from the parent
                # process to the child process.
                #
                # This is extremely dangerous because database connections aren't
                # safe to share between processes.
                #
                # However, because the parent process doesn't execute anything
                # in parallel with the child process and waits for the child process
                # to die before it continues, this is ok.
                #
                # Without this change we can't reconnect to the old connections and
                # stale data is left behind in the database, causing inconsistent errors.
                owner_to_pool = ActiveRecord::Base.
                  connection_handler.
                  instance_variable_get(:@owner_to_pool)
                owner_to_pool[Process.pid] = owner_to_pool[Process.ppid]
              else
                # Rails 6.1 replaced owner_to_pool with owner_to_role which doesn't
                # key by Process.pid. Instead 6.1 calls `Role.discard_pools!` in a
                # ForkTracker.after_fork callback. This may eliminate the problems
                # we previously experienced that required the pools to be copied
                # across processes.
              end

              yield
            rescue Exception => e
              STDERR.puts e
              STDERR.puts e.backtrace
              raise
            ensure
              Process.exit!
            end
          end
          Process.waitpid suite_pid
        end

        def skip_job(klass, method, reporter)
          result = Minitest::Result.from(klass.new(method))
          result.time = 0
          safe_record(reporter, result)
        end

        def safe_record(reporter, result)
          result.extras[:end_time] = Time.now
          result.extras[:hostname] = @host
          result.extras[:worker_id] = @worker_id
          result.extras[:remote_worker_number] = ENV["TEST_QUEUE_WORKER_ID"] || 0

          @to_record << [reporter, result]
        end

        def bulk_record
          begin
            @queue_server.record(@worker_id, @to_record)

            @queue_server.record_collector_data(
              ParallelCollector.descendants.index_with do |klass|
                klass.instance.retrieve_data
              end.transform_keys(&:name)
            )
          rescue DRb::DRbConnError
            result.failures.each do |failure|
              failure.exception = DRb::DRbRemoteError.new(failure.exception)
            end
          end
        end

        def set_process_title(status)
          Process.setproctitle("Rails test worker #{@number} - #{status}")
        end

        def max_failures_reached?
          @failures >= MAX_FAILING_TESTS
        end
      end
    end
  end
end
