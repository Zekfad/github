# frozen_string_literal: true

module Faraday
  class Resilient < Faraday::Middleware
    def self.tripped?(res)
      res.status == 502 && res.headers[:resilient] == "yes"
    end

    def initialize(app, options)
      super(app)
      @name = options[:name]
      @options = options[:options]
      @registry = options[:registry]
    end

    def circuit_breaker
      ::Resilient::CircuitBreaker.get(@name, @options, @registry)
    end

    def call(env)
      env.clear_body if env.needs_body?

      circuit = circuit_breaker
      if circuit.allow_request?
        call_request(env, circuit)
      else
        tripped_request(env, circuit)
      end
    end

    def call_request(env, circuit)
      res = @app.call(env)
      if res.status < 200 || res.status > 499
        circuit.failure
      else
        circuit.success
      end

      return res
    rescue
      circuit.failure
      raise
    end

    def tripped_request(env, circuit)
      env[:resilient_circuit] = circuit
      env.status = 502
      env.response_headers = Utils::Headers.new
      env.response_headers["Resilient"] = "yes"
      env.response = Response.new.tap do |res|
        res.finish(env) unless env.parallel?
      end
    end
  end
end
