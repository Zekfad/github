# frozen_string_literal: true

module GitAuth
  class Ref
    attr_reader :encoded_name, :before, :after, :status, :decision
    attr_accessor :message, :error
    def initialize(encoded_name, before, after, status = nil)
      @encoded_name, @before, @after, @status = encoded_name, before, after, status
      @decision = :undecided
    end

    def decoded_name
      @decoded_name ||= Addressable::URI.unescape(encoded_name)
    end

    def allow!
      @decision = :allow
    end

    def allow?
      decision == :allow
    end

    def disallow!(message)
      @decision = :disallow
      @message = message
    end

    def undecided?
      decision == :undecided
    end

    def payload
      if status
        [decoded_name, before, after, status]
      else
        [decoded_name, before, after]
      end
    end

    def ok!
      self.message ||= "ok #{before} #{after}"
    end

    def failed!
      self.message ||= "failed"
    end
  end
end
