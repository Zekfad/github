# frozen_string_literal: true

module GitAuth
  # Note that this class uses ApplicationRecord::Domain in order to
  # avoid relying on ActiveRecord. This is the first step in decoupling
  # gitauth from the github/github, and the lowest level of raw SQL
  # that is currently acceptable in the github/github codebase.
  class SSHKey
    def self.with_fingerprint(fingerprint)
      data = ApplicationRecord::Domain::Users.github_sql.hash_results(<<-SQL, fingerprint: fingerprint).first
        SELECT id, `key`, accessed_at, fingerprint, unverification_reason,
        repository_id, user_id, creator_id, verifier_id, oauth_authorization_id,
        created_by, verified_at, read_only
        FROM public_keys
        WHERE fingerprint=:fingerprint
      SQL
      new data if !!data
    end

    attr_reader :id, :key, :accessed_at, :fingerprint, :unverification_reason,
      :repository_id, :user_id, :creator_id, :verifier_id, :oauth_authorization_id,
      :created_by, :verified_at, :read_only
    def initialize(attributes)
      attributes.each do |name, value|
        instance_variable_set("@#{name}", value)
      end
    end

    # Returns [result status Symbol, encoded member or error String]
    def verify(public_key, ssh_required:)
      return [:unknown_key, "Unknown SSH Key"] if key != public_key

      unless ssh_key_user || repository
        return [:unknown_username, "Unknown username"]
      end

      if ssh_key_user && ssh_required
        return [:org_requires_cert, "Organization requires SSH certificate"]
      end

      [:ok, member]
    end

    def type
      user_id ? :user : :repo
    end

    def member
      if user_id
        "user:#{user_id}:#{ssh_key_user.login}"
      else
        "repo:#{repository_id}:#{repository.nwo}"
      end
    end

    def created_by_unknown?
      created_by == "unknown"
    end

    def read_only?
      # It seems to return different values in different
      # environments.
      [1, "1", true].include?(read_only)
    end

    def verified?
      !verified_at.nil?
    end

    # For now, look up the existing Rails user.
    # This gets re-assigned to 'member_object',
    # which gets used in big and complicated ways.
    def user
      @user ||= User.find_by(id: user_id)
    end

    # If we don't need a whole user object, just an id and login,
    # then let's go with the cheaper lookup.
    def ssh_key_user
      @ssh_key_user ||= GitAuth::Login.find(user_id)
    end

    # For now, look up the existing Rails oauth app.
    # This gets used in the OAuthApplicationPolicy,
    # among other things, which I'm not yet ready
    # to decouple.
    def oauth_application
      return @oauth_application if defined?(@oauth_application)
      return nil unless created_by_oauth_application?

      case oauth_authorization.application_type
      when "OauthApplication"
        @oauth_application = OauthApplication.find_by(id: application_id)
      when "Integration"
        @oauth_application = Integration.find_by(id: application_id)
      end
    end

    def verifier
      @verifier ||= GitAuth::Login.find(verifier_id)
    end

    def creator
      @creator ||= GitAuth::Login.find(creator_id)
    end

    def repository
      @repository ||= GitAuth::SSHKey::Repository.find(repository_id)
    end

    def deploy_key?
      !!repository_id && !!repository
    end

    def created_by_oauth_application?
      oauth_authorization_id && application_id && application_id != OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
    end

    def authorization_active_for_orgs?(orgs)
      org_ids = orgs.map(&:id)
      return false if org_ids.empty?
      count = ApplicationRecord::Domain::Users.github_sql.value(<<-SQL, organization_ids: org_ids, public_key_id: id)
        SELECT COUNT(id)
        FROM organization_credential_authorizations
        WHERE revoked_by_id IS NULL
          AND credential_type = 'PublicKey'
          AND organization_id IN :organization_ids
          AND credential_id = :public_key_id
      SQL
      count >= 1
    end

    private

    def oauth_authorization
      @oauth_authorization ||= GitAuth::SSHKey::OauthAuthorization.find(oauth_authorization_id)
    end

    def application_id
      oauth_authorization.application_id
    end
  end
end
