# frozen_string_literal: true

module GitAuth
  class Refs
    include Enumerable

    def initialize(raw_refs)
      @refs_by_decoded_name = {}
      Array(raw_refs).each do |raw_ref|
        ref = GitAuth::Ref.new(*raw_ref)
        @refs_by_decoded_name[ref.decoded_name] = ref
      end
    end

    def each(&block)
      refs_by_decoded_name.each do |_, ref|
        yield ref
      end
    end

    def [](key)
      refs_by_decoded_name[key]
    end

    def payload
      Hash[self.map { |ref| [ref.encoded_name, ref.message] }]
    end

    def error_message
      self.map(&:error).compact.join("")
    end

    private
    attr_reader :refs_by_decoded_name
  end
end
