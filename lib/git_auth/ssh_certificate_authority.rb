# frozen_string_literal: true

module GitAuth
  class SSHCertificateAuthority
    # Certificate extension names are suffixed with the "originating author or
    # organisation's domain name".
    LOGIN_EXTENSIONS = ["login@github.com"]
    LOGIN_EXTENSIONS.unshift("login@#{GitHub.host_name}") if GitHub.enterprise?

    # We support any type of cert other than DSA.
    SUPPORTED_CERT_ALGOS = [
      SSHData::Certificate::ALGO_RSA,
      SSHData::Certificate::ALGO_ECDSA256,
      SSHData::Certificate::ALGO_ECDSA384,
      SSHData::Certificate::ALGO_ECDSA521,
      SSHData::Certificate::ALGO_ED25519,
    ]

    # Critical options that we know how to handle. Others result in an error.
    SUPPORTED_CRITICAL_OPTIONS = [
      SSHData::Certificate::CRITICAL_OPTION_SOURCE_ADDRESS,
    ]

    # Validate an SSH certificate. This involves checking a bunch of fields on the
    # cert, verifying its signature, and looking up the corresponding CA and user.
    #
    # certificate - A String SSH certificate in OpenSSH format.
    #
    # Returns an Array. The first element is a Symbol describing the result of
    # validation. The second element is the User associated with the cert. The
    # third is the SshCertificateAuthority that issued the cert.
    def self.validate_certificate(certificate, ip: "none")
      # Parse the certificate. This also verifies the cert's signature.
      parsed = begin
                 SSHData::Certificate.parse_openssh(certificate)
               rescue SSHData::Error
                 return [:parse_error, nil, nil]
               end

      if parsed.type != SSHData::Certificate::TYPE_USER
        # Certs can be issued for hosts or users. We want user certs
        return [:host_cert, nil, nil]
      elsif SUPPORTED_CERT_ALGOS.exclude?(parsed.algo)
        # We don't support DSA keys
        return [:unsupported_algo, nil, nil]
      elsif GitHub::SSH.blacklisted_fingerprint?(parsed.public_key.fingerprint(md5: true))
        return [:blacklisted, nil, nil]
      elsif weak_rsa_key?(parsed.public_key)
        return [:weak_rsa, nil, nil]
      elsif Time.now < parsed.valid_after
        # Cert isn't valid yet
        return [:too_early, nil, nil]
      elsif Time.now > parsed.valid_before
        # Cert is expired
        return [:too_late,  nil, nil]
      elsif !parsed.allowed_source_address?(ip)
        # The certificate whitelists IP addresses. This one isn't included.
        return [:bad_ip, nil, nil]
      elsif (parsed.critical_options.keys - SUPPORTED_CRITICAL_OPTIONS).any?
        # The certificate includes critical options that we don't support
        return [:critical_opts, nil, nil]
      end

      login_exts = parsed.extensions.values_at(*LOGIN_EXTENSIONS).compact
      return [:no_login, nil, nil] if login_exts.empty?

      # lookup users from extension values, preferring user from GHES-specific
      # extension, but always falling back to main github.com extension.

      logins = ApplicationRecord::Domain::Users.github_sql.values(<<-SQL, logins: login_exts)
        SELECT login
        FROM users
        WHERE login IN :logins
        AND type = 'User'
      SQL

      user_login = (login_exts & logins).first
      return [:no_user, nil, nil] if user_login.nil?

      ca_fpr = Base64.decode64(parsed.ca_key.fingerprint)

      ca = SshCertificateAuthority.where(fingerprint: ca_fpr).first
      ca = with_model_object(ca)

      return [:no_ca, nil, nil] if ca.nil?
      return [:bad_plan, nil, nil] unless ca.owner_is_eligible_for_feature?

      return [:ok, user_login, ca]
    end

    # Is the given RSA key too weak to be used?
    #
    # parsed - An SSHData::PublicKey::Base subclass instance.
    #
    # Returns boolean.
    def self.weak_rsa_key?(parsed)
      return false unless parsed.algo == SSHData::PublicKey::ALGO_RSA
      parsed.openssl.params["n"].num_bits < 2048 || GitHub::SSH.weak_rsa_key?(parsed.openssl)
    end
    private_class_method :weak_rsa_key?

    def self.with_model_object(ca)
      return unless ca

      ca_id = ca.id

      owner_type, owner_id = ApplicationRecord::Collab.github_sql.results(<<-SQL, id: ca_id).first
        SELECT owner_type, owner_id
        FROM ssh_certificate_authorities
        WHERE id = :id
      SQL

      new(
        owner_is_eligible_for_feature: ca.owner_is_eligible_for_feature?,
        owner_name_and_type: ca.owner_name_and_type,
        id: ca_id,
        owner_type: owner_type,
        owner_id: owner_id,
      )
    end

    attr_reader :owner_name_and_type, :id, :owner_type, :owner_id

    def initialize(owner_is_eligible_for_feature:, owner_name_and_type:, id:, owner_type:, owner_id:)
      @owner_is_eligible_for_feature = owner_is_eligible_for_feature
      @owner_name_and_type = owner_name_and_type
      @id = id
      @owner_type = owner_type
      @owner_id = owner_id
    end

    def owner_is_eligible_for_feature?
      @owner_is_eligible_for_feature
    end

    # Check if this CA is owned by an org with a given id,
    # or a business associated with the org.
    #
    # org_id - the id of the Organization
    #
    # Returns boolean.
    def owned_by_organization?(org_id)
      if owner_type == "Business"
        ApplicationRecord::Domain::Users.github_sql.value(<<-SQL, business_id: owner_id, organization_id: org_id)
          SELECT 1
          FROM business_organization_memberships
          WHERE business_id = :business_id
          AND organization_id = :organization_id
        SQL
      else
        owner_id == org_id
      end
    end

    # Is this CA owned by the same entity as owns the given repository?
    #
    # repo - A Repository instance.
    #
    # Returns boolean.
    def owned_by_repo_owner?(repo)
      return false unless repo.owner.is_a?(Organization)

      owned_by_organization?(repo.owner_id)
    end
  end
end
