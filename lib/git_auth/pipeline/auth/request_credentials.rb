# frozen_string_literal: true

module GitAuth
  class Pipeline
    module Auth
      # Anything that RequestCredentials knows how to handle.
      class RequestCredentials < AuthenticationMethod
        class HTTPStatusNotOkError < StandardError; end
        MAX_RETRIES = 3

        def process
          creds = Api::RequestCredentials.new(login: input.member, password: input.password)
          creds.initialize_token_from_login_or_password

          auth_attempt = GitHub::Authentication::Attempt.new(
            allow_integrations:         true,
            allow_user_via_integration: true,
            from:                       :git,
            login:                      creds.login,
            password:                   creds.password,
            token:                      creds.token,
            ip:                         input.ip,
            request_id:                 input.request_id,
            repo:                       input.target_path,
            action:                     input.action,
            protocol:                   input.protocol,
          )

          if auth_attempt.result.failure_reason == :git_with_password_auth
            return result.fail_with :git_with_password_auth
          elsif auth_attempt.result.failure_reason == :weak_password
            return result.fail_with :weak_password
          elsif auth_attempt.result.failure_reason == :external_auth_token_required
            return result.fail_with :external_auth_token_required
          elsif auth_attempt.result.failure_reason == :ldap_timeout
            return result.fail_with :ldap_timeout
          elsif auth_attempt.result.suspended_failure?
            return result.fail_with :suspended
          end

          if auth_attempt.result.success?
            if should_notify_of_basic_auth_deprecation?(auth_attempt)
              notify_about_basic_auth_deprecation(auth_attempt.result.user)
            end

            result.user = auth_attempt.result.user
            # Apparently token can be an empty string.
            result.token = auth_attempt.token if auth_attempt.token_present?
            return result.success!
          end
        end

        private

        def applicable?
          !!input.password && GitHub::UTF8.valid_unicode3?(input.member)
        end

        def should_notify_of_basic_auth_deprecation?(attempt)
          return false if GitHub.git_password_auth_supported?
          return false if input.repository.nil?
          return false unless GitHub.githooks_api_url
          result = attempt.result

          result.user &&
            attempt.password_attempt? &&
            result.correct_password? &&
            result.success? &&
            !GitHub.flipper[:opt_out_notify_about_git_basic_auth_deprecation].enabled?(result.user) &&
            GitHub.flipper[:notify_about_git_basic_auth_deprecation].enabled?(result.user) &&
            GitHub::ActionRestraint.perform?(
              "git_basic_auth_check",
              interval: 1.month,
              user_id: result.user.id,
            )
        end

        def notify_about_basic_auth_deprecation(user)
          options = {
            # Time to sleep after we trip the circuit
            sleep_window_seconds: 300,
            request_volume_threshold: 5,
          }
          circuit_breaker = Resilient::CircuitBreaker.get("git_deprecation", options)

          return unless circuit_breaker.allow_request?

          resource_type = input.repository.human_name
          if resource_type == "repository" && input.target_path.ends_with?(".wiki.git")
            resource_type = "wiki"
          end

          retry_count = 0
          body = {
            user_id: user.id,
            user_agent: input.original_user_agent,
            resource_type: resource_type,
          }.to_json

          timestamp = Time.now.to_i.to_s
          request_hmac_key = GitHub.api_internal_repositories_hmac_keys.first.to_s
          body_hmac_key = GitHub.internal_api_hmac_key.to_s
          req_hmac_sign = OpenSSL::HMAC.hexdigest("sha256", request_hmac_key, timestamp)
          body_hmac_sign = OpenSSL::HMAC.hexdigest("sha256", body_hmac_key, body)

          path = "/internal/repositories/#{input.repository.id}/git/password_auth"
          connection = Faraday.new(url: "#{GitHub.githooks_api_url}#{path}", request: {
            timeout: 1,
          })

          begin
            resp = connection.post do |req|
              req.headers["Content-Type"] = "application/json"
              req.headers["Request-HMAC"] = "#{timestamp}.#{req_hmac_sign}"
              req.headers["Content-HMAC"] = "sha256 #{body_hmac_sign}"
              req.headers["User-Agent"] = GitHub.current_sha
              req.body = body
            end

            if resp.status != 202
              raise HTTPStatusNotOkError.new("Expected HTTP 202 for #{path} but got #{resp.status} with headers: #{resp.headers} and body: #{resp.body}.")
            end
            circuit_breaker.success
          rescue Faraday::Error, HTTPStatusNotOkError => exception
            if (retry_count += 1) <= MAX_RETRIES
              sleep(0.5)
              retry
            else
              Failbot.report!(exception, app: "github")
              circuit_breaker.failure
            end
          end
        end
      end
    end
  end
end
