# rubocop:disable Style/FrozenStringLiteralComment

# This is a client library for the Munger API service,
# which lives in https://github.com/github/munger
module Munger
  class Client
    class MungerError < StandardError; end

    TRENDING_PERIOD_MAPPING = {
      daily: "daily",
      weekly: "weekly",
      monthly: "monthly",
    }.freeze

    attr_reader :options, :faraday

    def initialize(host, options = {})
      @options = options.reverse_merge({
        timeout_threshold: 2,
      })

      @faraday = build_faraday(host)
    end

    def vulnerability_detections(language_id)
      return unless GitHub.munger_available?

      response = get "/languages/#{language_id}/vulnerability_detections"
      json = parse_response(response)
      return unless json && json.key?("vulnerability_detections")

      detections = json["vulnerability_detections"]
      detections.map do |hsh|
        AdvisoryDB::MungerDetection.new(hsh)
      end
    end

    # Public: Returns repository recommendations, or nil, for a user. Recommendations are
    # unfiltered, meaning they may contain spammy or private repositories, repositories that no
    # longer exist, and repositories the user has already dismissed.
    #
    # user - a User instance
    # page - which page of results to load; an Integer
    # per_page - how many results to load per page; an Integer
    # use_new_url - A temporary keyword arg to allow for feature flag flipping
    # where this method is called
    # (from the caller: GitHub.flipper[:use_new_munger_repo_recommendation_endpoint].enabled?)
    # fallback - A way to signal if we need to fallback to the non user
    # specific repositotory recommendations
    #
    # Returns an Array of RepositoryRecommendation instances, or nil.
    def repository_recommendations(user, page:, per_page:, use_new_url: false, fallback: false)
      return unless GitHub.munger_available?

      query = "page=#{page.to_i}&per_page=#{per_page.to_i}"

      url = if use_new_url
        if fallback
          "/features/jazz_fallback_repository_recommendations?#{query}"
        else
          pagination_query = "paginate_by=recommendations&#{query}"

          "/features/jazz_user_repository_recommendations/#{user.id}?#{pagination_query}"
        end
      else
        "/users/#{user.id}/repository_recommendations?#{query}"
      end

      response = GitHub.dogstats.time("repositories.request.repository_recommendations") { get url }
      json = parse_response(response, user: user)

      if use_new_url && fallback && json && json.is_a?(Array)
        RepositoryRecommendation.recommendations_from_json(user, { "recommendations" => json })
      elsif json && json.key?("recommendations")
        RepositoryRecommendation.recommendations_from_json(user, json)
      end
    end

    # Public: Returns a collection of good first issues, or nil, for
    # a specific repository id.
    #
    # repo_id - a Repository id
    #
    # Returns an Array of JSON objects, or nil.
    def good_first_issues_for_repo_id(repo_id)
      return unless GitHub.munger_available?

      response = GitHub.dogstats.time("issues.request.multi_good_first_issues") do
        get("/features/first_issues", "repository_id" => [repo_id])
      end

      json = parse_response(response)

      if json.is_a?(Array) && json.first&.key?("issues")
        json.first
      else
       { "issues" => [] }
      end
    end

    # Public: Returns trending developers, or nil.
    #
    # period - Symbol, [:daily, :weekly, :monthly]
    #
    # page - Int, what page to return in the context of the per_page constraint.
    #
    # per_page - Int, how many results per page should be returned.
    #
    # language_id - Int, return only trending developers with a primary repository for the given
    # language id.
    #
    # Returns an array of JSON objects with user information
    def trending_developers(period: nil, per_page: nil, page: nil, language_id: nil)
      return unless GitHub.munger_available?

      query_params = trending_query_params(page: page, per_page: per_page, language_id: language_id)

      period_url_path = if TRENDING_PERIOD_MAPPING[period].present?
        "/#{TRENDING_PERIOD_MAPPING[period]}"
      end

      response = GitHub.dogstats.time("trending.request.developers") do
        get "/trending_developers#{period_url_path}", query_params
      end

      json = parse_response(response)
      return unless json

      if json.is_a?(Hash)
        return unless json.keys.any? { |key| key.in? TRENDING_PERIOD_MAPPING.values }
      elsif json.is_a?(Array)
        return unless json.first&.key?("user_id")
      end

      json
    end

    # Public: Returns trending repositories, or nil.
    #
    # language_id - Int, return only repositories with the given language id
    #
    # page - Int, what page to return in the context of the per_page constraint.
    #
    # per_page - Int, how many results per page should be returned.
    #
    # period - Symbol, [:daily, :weekly, :monthly]
    #
    # spoken_language_code - String, the ISO 639-1 Code, representing the spoken
    # language
    #
    # Returns an array of JSON objects with repository information
    def trending_repositories(
      language_id: nil,
      page: nil,
      per_page: nil,
      period: nil,
      spoken_language_code: nil
    )
      return unless GitHub.munger_available?

      query_params = trending_query_params(page: page, per_page: per_page, language_id: language_id)
      query_params = query_params.merge({ spoken_language_code: spoken_language_code }.compact)

      period_url_path = if TRENDING_PERIOD_MAPPING[period].present?
        "/#{TRENDING_PERIOD_MAPPING[period]}"
      end

      response = GitHub.dogstats.time("trending.request.repositories") do
        get "/trending_repositories#{period_url_path}", query_params
      end

      json = parse_response(response)
      return unless json

      if json.is_a?(Hash)
        return unless json.keys.any? { |key| key.in? TRENDING_PERIOD_MAPPING.values }
      elsif json.is_a?(Array)
        return unless json.first&.key?("repository_id")
      end

      json
    end

    # Public: Returns recommended topics for a specific user id, or nil.
    #
    # user_id - Integer, this is the user id you want to receive topic recommendations
    # back for.
    #
    # Returns an array of JSON objects with topic information containing the following keys:
    #
    # id    - The topic's database id.
    # name  - The topic's name.
    # score - A decimal confidence score of the recommendation, between 0 and 1.
    def topic_recommendations(user_id)
      return unless GitHub.munger_available?

      response = GitHub.dogstats.time("recommendations.request.topics") do
        get "/features/topic_recommendations/#{user_id}"
      end

      json = parse_response(response)
      return unless json && json.is_a?(Hash)
      return unless json.keys.any? { |key| key == "recommendations" }

      json
    end

    # Public: Returns an Array of DataTopic instances or nil for the given Repository.
    #
    # Given a Repository, this returns a list of DataTopic instances for topics
    # that match that Repository. Returns nil if an error occurs.
    def topics_for_repository(repository)
      return [] unless GitHub.munger_available?
      response = GitHub.dogstats.time("topics.request.topics_for_repository") do
        get "/repositories/#{repository.id}/topics"
      end

      json = parse_response(response, repository: repository)
      return unless json && json.key?("topics")

      repository_topics = json["topics"]
      repository_topics.map do |t|
        DataTopic.new(name: t["topic_name"],
                      search_score: t["repository_score"],
                      suggestion_score: t["topic_score"])
      end
    end

    # Public: Given a source topic String, like "ruby", this returns a list of
    # DataTopic instances that are related to the source topic.
    #
    # Returns an Array of DataTopic instances, or nil if an error occurs.
    def related_topics(source_topic)
      return [] unless GitHub.munger_available?
      response = GitHub.dogstats.time("topics.request.related_topics") do
        get "/topics/#{CGI.escape(source_topic)}/related_topics"
      end

      json = parse_response(response, source_topic: source_topic)
      return unless json && json.key?("topics")

      related_topics = json["topics"]
      return [] unless related_topics

      related_topics.map do |t|
        DataTopic.new(name: t["topic_name"], suggestion_score: t["weight"])
      end
    end

    # Public: Returns an Array of DataNavigationDestination instances or nil for the given User.
    #
    # Given a User, this returns a list of DataNavigationDestination instances for
    # navigations destinations that the User has recently visited. Returns nil if an error occurs.
    def navigation_destinations(user)
      return unless GitHub.munger_available?

      response = GitHub.dogstats.time("munger_client.users.request.navigation_destinations") do
        get "/users/#{user.id}/navigation_destinations"
      end

      json = parse_response(response, user: user)
      return unless json && json.key?("navigation_destinations")

      navigation_destinations = json["navigation_destinations"]
      navigation_destinations.map do |n|
        DataNavigationDestination.new(
          type: n["target_type"],
          id: n["target_id"],
          name: n["target_name"],
          visit_count: n["visit_count"],
          last_visited_at: n["last_visited_at"],
          generated_at: n["generated_at"],
        )
      end
    end


    # Public: Returns recommended developers for a specific user id, or nil.
    #
    # user_id - Integer, this is the user id you want to receive recommended developers
    # back for.
    #
    # Returns an array of JSON objects with developer information containing the following keys:
    #
    # reasons - Recommendation reasons
    # topic id - The topic's database id.
    # relevant users - The list of recommendated users
    def recommended_developers(user_id)
      return [] unless GitHub.munger_available?

      response = GitHub.dogstats.time("recommendations.request.developers") do
        get "/features/follow_recommendations/#{user_id}"
      end

      json = parse_response(response)
      return unless json && json.is_a?(Hash)
      return unless json.key?("recommendations")

      json
    end

    # Public: Returns a list of notification unsubscribe recommendations for the given user.
    #
    # Returns Array<DataUnsubscribeSuggestion>, or nil if an error occurs.
    def notifications_unsubscribe_suggestions(user)
      return unless GitHub.munger_available?

      response = GitHub.dogstats.time("munger_client.users.request.notifications_unsubscribe_suggestions") do
        get "/features/unsubscribe_suggestions/#{user.id}"
      end

      json = parse_response(response, user: user)
      return unless json && json.key?("candidates")

      unsubscribe_suggestions = json["candidates"]
      unsubscribe_suggestions.map do |suggestion|
        DataUnsubscribeSuggestion.new(
          snapshot_date: json["snapshot_date"],
          user_id: json["user_id"],
          repository_id: suggestion["repository_id"],
          score: suggestion["score"],
          algorithm_version: json["algorithm_version"]
        )
      end
    end

    private

    # Private: Parses a Faraday response body, rescues any parsing errors,
    # and reports the error to Failbot.
    #
    # response - a Faraday::Response object
    # error_metadata - a Hash of metadata that should go to Failbot on error
    #
    # Returns JSON.
    def parse_response(response, **error_metadata)
      return unless response
      GitHub::JSON.parse(response.body)
    rescue Yajl::ParseError => boom
      Failbot.report(boom, error_metadata)
      nil
    end

    def get(path, params = {})
      return unless circuit_breaker.allow_request?

      client = faraday

      result = client.get path do |req|
        req.params.update params
        req.options.timeout = options[:timeout_threshold]
        req.options.open_timeout = options[:timeout_threshold]
        req.options.params_encoder = Faraday::FlatParamsEncoder

        if request_id = GitHub.context[:request_id]
          req.headers["X-GitHub-Request-Id"] = request_id
        end
      end

      if result.success? || result.status == 404
        if result.status == 404
          Failbot.report(
            MungerError.new("Unsuccessful response (#{result.status}) from #{path}."),
            app: "github-user",
            path: path,
            params: params,
          )
        end

        circuit_breaker.success
        result
      else
        raise MungerError, "Unsuccessful response (#{result.status}) from #{path}."
      end
    rescue MungerError, Faraday::Error => boom
      circuit_breaker.failure
      Failbot.report(boom, app: "github-user", path: path, params: params)
      nil
    end

    def circuit_breaker
      Resilient::CircuitBreaker.get("munger",
        instrumenter: GitHub,
        sleep_window_seconds:       600,
        request_volume_threshold:   2,
        error_threshold_percentage: 50,
        window_size_in_seconds:     300,
        bucket_size_in_seconds:     30,
      )
    end

    def build_faraday(url)
      options = { url: url }

      Faraday.new(options) do |b|
        b.adapter :excon
      end
    end

    def trending_query_params(page:, per_page:, language_id: nil)
      if page.present? && per_page.present?
        {
          page: page,
          per_page: per_page,
          primary_language_id: language_id,
        }.compact
      else
        { primary_language_id: language_id }.compact
      end
    end
  end
end
