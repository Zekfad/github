# frozen_string_literal: true

module PackageRegistry
  class PackageVersion < SimpleDelegator

    def initialize(version)
      @version = version
      super
    end

    # version.name will return the tag if it is available,
    # otherwise it will return the sha
    def version
      @version.name
    end

    def uri
      tag_name.blank? ? "@#{digest}" : ":#{tag_name}"
    end

    def created_at
      epoch_micros = @version.created_at.nanos / 10 ** 6
      Time.at(@version.created_at.seconds, epoch_micros)
    end

    def updated_at
      epoch_micros = @version.updated_at.nanos / 10 ** 6
      Time.at(@version.updated_at.seconds, epoch_micros)
    end

    def license
      return nil unless metadata

      case @version.ecosystem.downcase.to_sym
      when :container
        return metadata.labels&.licenses
      end
    end

    def metadata
      case @version.ecosystem.downcase.to_sym
      when :container
        @version.containerMetadata
      end
    end

    private

    def tag_name
      metadata&.tag&.name
    end

    def digest
      metadata&.tag&.digest
    end
  end
end
