# frozen_string_literal: true

module PackageRegistry
  class PackageMetadata
    attr_reader :package
    attr_reader :package_versions
    attr_reader :latest_version

    delegate :id, :owner, :package_type, :name, :repository, :visibility, to: :package

    def initialize(resp)
      @package = PackageRegistry::Package.new(resp.package)
      @package.latest_version = PackageRegistry::PackageVersion.new(resp.latest_version)
      @latest_version = PackageRegistry::PackageVersion.new(resp.latest_version)

      versions = resp.try(:versions) || []
      @package_versions = versions.map { |v| PackageRegistry::PackageVersion.new(v) }
    end
  end
end
