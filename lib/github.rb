# frozen_string_literal: true

# GitHub libraries and configuration.
#
# This module is loaded very early during initialization
# (config/preinitializer.rb) so care must be taken not to load too much or
# assume the environment is configured when this file is required. It should
# basically set up autoloads and gtfo.

# Autoloads are prohibited in config code.

require "github/runtime"
require "github/url_builder"
require "github/config"
require "github/version"
require "github/git_constants"
require "github/robots"
require "github/config/context"

module GitHub
  autoload :Aqueduct,             "github/aqueduct"
  autoload :ApplicationsCreationLimit, "github/applications_creation_limit"
  autoload :Aleph,                "github/aleph"
  autoload :Assertions,           "github/assertions"
  autoload :Authentication,       "github/authentication"
  autoload :BackgroundDependentDeletes, "github/background_dependent_deletes"
  autoload :Backups,              "github/backups"
  autoload :Base32,               "github/base32"
  autoload :BatchRepositoryTransfer, "github/batch_repository_transfer"
  autoload :Billing,              "github/billing"
  autoload :BrowserStatsHelper,   "github/browser_stats_helper"
  autoload :BytesizeTokenizer,    "github/bytesize_tokenizer"
  autoload :Cache,                "github/cache"
  autoload :CacheLeakDetector,    "github/cache_leak_detector"
  autoload :CacheLock,            "github/cache_lock"
  autoload :Chatterbox,           "github/chatterbox"
  autoload :Colorize,             "github/colorize"
  autoload :CommentMetrics,       "github/comment_metrics"
  autoload :CommitMessage,        "github/commit_message"
  autoload :CommitMessageHTML,    "github/commit_message"
  autoload :CPU,                  "github/cpu"
  autoload :Connect,              "github/connect"
  autoload :CSV,                  "github/csv"
  autoload :DarrrrEarthsmokeEncryptor, "github/darrrr_earthsmoke_encryptor"
  autoload :DataQualityError,     "github/data_quality_error"
  autoload :DGit,                 "github/dgit"
  autoload :Diff,                 "github/diff"
  autoload :DNS,                  "github/dns"
  autoload :Dogstats,             "github/dogstats"
  autoload :Earthsmoke,           "github/earthsmoke"
  autoload :Email,                "github/email"
  autoload :Encoding,             "github/encoding"
  autoload :EnterpriseWeb,        "github/enterprise_web"
  autoload :ErbImplementation,    "github/erb_implementation"
  autoload :Experiment,           "github/experiment"
  autoload :FaradayMiddleware,    "github/faraday_middleware"
  autoload :FastRenderEnhancer,   "github/fast_render_enhancer"
  autoload :FeatureFlag,          "github/feature_flag"
  autoload :FlipperActor,         "github/flipper_actor"
  autoload :FormattedStackLocation, "github/formatted_stack_location"
  autoload :Hex,                  "github/hex"
  autoload :HydroMiddleware,      "github/hydro_middleware"
  autoload :JavaScriptErrorReporter, "github/java_script_error_reporter"
  autoload :MinimizeComment,      "github/minimize_comment"
  autoload :InvalidParametersMiddleware, "github/invalid_parameters_middleware"
  autoload :Location,             "github/location"
  autoload :GitFile,              "github/git_file"
  autoload :Greenhouse,           "github/greenhouse"
  autoload :HTML,                 "github/html"
  autoload :HTMLSafeString,       "github/html_safe_string"
  autoload :JSON,                 "github/json"
  autoload :FailsafeJSON,         "github/json"
  autoload :Limiter,              "github/limiter"
  autoload :Limiters,             "github/limiters"
  autoload :Logger,               "github/logger"
  autoload :Mail,                 "github/mail"
  autoload :MentionDiff,          "github/mention_diff"
  autoload :Memory,               "github/memory"
  autoload :Middleware,           "github/middleware"
  autoload :Migrator,             "github/migrator"
  autoload :MysqlInstrumenter,    "github/mysql_instrumenter"
  autoload :OCSP,                 "github/ocsp"
  autoload :Options,              "github/options"
  autoload :Pages,                "github/pages"
  autoload :Plan,                 "github/plan"
  autoload :Positions,            "github/positions"
  autoload :PrefillAssociations,  "github/prefill_associations"
  autoload :PreReceiveHookEntry,  "github/pre_receive_hook_entry"
  autoload :ProxyDetection,       "github/proxy_detection"
  autoload :Qintel,               "github/qintel"
  autoload :RateLimitedCreation,  "github/rate_limited_creation"
  autoload :RefShaPathExtractor,  "github/ref_sha_path_extractor"
  autoload :Relay,                "github/relay"
  autoload :RenameColumn,         "github/rename_column"
  autoload :RepoCreator,          "github/repo_creator"
  autoload :RepoGraph,            "github/repo_graph"
  autoload :RepoPermissions,      "github/repo_permissions"
  autoload :Reports,              "github/reports"
  autoload :RepositoryProtocolSelector, "github/repository_protocol_selector"
  autoload :Resources,            "github/resources"
  autoload :RouteHelpers,         "github/route_helpers"
  autoload :Routers,              "github/routers"
  autoload :Routing,              "github/routing"
  autoload :SchemaDomain,         "github/schema_domain"
  autoload :SourceMapResolver,    "github/source_map_resolver"
  autoload :StackFrameTaggedError, "github/stack_frame_tagged_error"
  autoload :FailbotKeyFilter,     "github/failbot_key_filter"
  autoload :FailbotKeyTagger,     "github/failbot_key_tagger"
  autoload :FailbotScrubber,      "github/failbot_scrubber"
  autoload :SimplePagination,     "github/simple_pagination"
  autoload :SMS,                  "github/sms"
  autoload :SSH,                  "github/ssh"
  autoload :SshKeyCleanup,        "github/ssh_key_cleanup"
  autoload :SpamChecker,          "github/spam_checker"
  autoload :Spokes,               "github/spokes"
  autoload :Stats,                "github/stats"
  autoload :Timer,                "github/timer"
  autoload :TimezoneTimestamp,    "github/timezone_timestamp"
  autoload :TokenScanning,        "github/token_scanning"
  autoload :Treelights,           "github/treelights"
  autoload :TwitterService,       "github/twitter_service"
  autoload :UIError,              "github/ui_error"
  autoload :UsageBilling,         "github/usage_billing"
  autoload :UserContent,          "github/user_content"
  autoload :UtcTimestamp,         "github/utc_timestamp"
  autoload :UTF8,                 "github/utf8"
  autoload :Unsullied,            "github/unsullied"
  autoload :Validations,          "github/validations"
  autoload :WeakKey,              "github/weak_key"
  autoload :FailbotLogger,        "github/failbot_logger"
  autoload :ZeitwerkInflector,    "github/zeitwerk_inflector"

  module DataStructures
    autoload :PersistentAtomicQueue, "github/data_structures/persistent_atomic_queue"
  end

  module CSP
    autoload :Policy, "github/csp/policy"
  end

  module Importer
    autoload :Issues, "github/importer/issues"
  end

  module Jobs
    Dir[File.expand_path("../../jobs/*.rb", __FILE__)].each do |file|
      file.sub!(/\.(rb|class)$/, "")
      class_name = File.basename(file).gsub(/(?:^|_)(.)/) { $1.upcase }
      autoload class_name.to_sym, file
    end
  end

  module StreamProcessors
    Dir[File.expand_path("../../stream_processors/*.rb", __FILE__)].each do |file|
      file.sub!(/\.(rb|class)$/, "")
      class_name = File.basename(file).gsub(/(?:^|_)(.)/) { $1.upcase }
      autoload class_name.to_sym, file
    end
  end

  # mixin config attributes and load RAILS_ROOT/config.yml if it exists.
  extend GitHub::Config
  load_environment_config

  # mixin version attributes
  extend GitHub::Version

  # Autoload enterprise after reloading the configuration to have the method
  # `enterprise?` available.
  #
  # Load the module in development so the Runtime switcher works properly.
  autoload :Enterprise, "github/enterprise"
  autoload :LDAP,       "github/ldap_dependency"

  # XXX HEY! Please don't add config values here. yeah I know you were going to
  # because I keep having to move that stuff to lib/github/config.rb
  # (GitHub::Config). It's mixed into the module level GitHub object and has
  # certain standards and conventions for documentation and organization.

  # Load up our global logging context
  GitHub::Logger.setup
end
