# frozen_string_literal: true

module Permissions
  class SystemRoles
    class ReconcileError < StandardError ; end
    attr_accessor :verbose, :dry_run, :purge

    # This library provides the mechanism to reconcile the system roles found in the DB
    # with the system roles defined by configuration.
    def initialize(config)
      @config_fgp = config
    end

    # Public: the fine grained permissions which can be assigned to custom roles.
    #
    # Returns an Array of strings.
    def custom_role_permissions
      @custom_role_permissions ||= @config_fgp.dig("custom_roles_data", "fine_grained_permissions") || []
    end

    # Public: reconcile the system roles defined by configuration with the
    #         roles currently present in the DB. By default it will add missing
    #         role, fine_grained_permissions, and role_permission records.
    #         Unless specified, it will only remove role records present in the DB
    #         but not defined in configuration.
    #
    # - purge: wether to remove fine_grained_permissions and role_permissions
    #          that are not defined in configuraiton. Boolean, defaults to false.
    #
    # - dry_run: if true, go through the reconciliation without adding/removing records.
    def reconcile(purge: false, dry_run: false, verbose: false)

      @purge = purge
      @dry_run = dry_run
      @verbose = verbose

      log "starting system role reconciliation"
      log "running in dry_run mode" if dry_run
      if purge?
        log "purge flag was enabled, records present in the DB but not in the system config will be removed"
      else
        log "purge flag is disabled, records present in the DB but not in the system config will not be removed"
      end

      FineGrainedPermission.transaction do
        Role.transaction(requires_new: true) do
          RolePermission.transaction(requires_new: true) do

            begin
              reconcile_roles
              reconcile_base_roles if base_roles?
              reconcile_permissions
              reconcile_custom_roles_permissions if custom_roles_enabled?
            rescue ActiveRecord::RecordInvalid => e
              puts e.inspect
              puts "Rolling back."
              raise ActiveRecord::Rollback
            end

          end
        end
      end
    end

    private

    # Internal: reconcile the role records. Note that the reconciliation of the
    #           base role attribute is done separatedly.
    def reconcile_roles
      roles_to_remove.each do |role|
        if purge?
          log "removing role: #{role.name}"
          role.destroy unless dry_run?
        else
          log "if purge = true, would remove: #{role.name}"
        end
      end

      roles_to_add.each do |role|
        log "adding role: #{role.name}"
        role.save! unless dry_run?
      end
    end

    # Internal: reconcile the base_role attribute of system roles found in the DB to match the configuration.
    #           Depends on reconcile_roles having already been run so dependencies are in place.
    def reconcile_base_roles
      return unless base_roles?

      Role.presets.each do |role|
        system_base_role = system_base_role_for(role.name)

        if role.base_role&.name != system_base_role

          # avoid 1 network jump to the DB if the base role should be nil
          if system_base_role.nil?
            log "updating base role of #{role.name} nil"
            role.update!(base_role_id: nil) unless dry_run?
            next
          end

          log "updating base role of #{role.name} from #{role.base_role&.name} to #{system_base_role}"
          unless dry_run?
            id = Role.presets.find_by!(name: system_base_role).id
            role.update!(base_role_id: id)
          end
        end
      end
    end

    # Internal: reconcile the fine_grained_permission and role_permission records in the DB
    #           to match the configuration.
    #           Depends on reconcile_roles having already been run so dependencies are in place.
    def reconcile_permissions
      add_missing_permissions
      add_missing_role_permissions

      remove_extra_permissions
      remove_extra_role_permissions
    end

    # Internal: mark all fine grained permission that should be used for custom roles as custom_roles_enabled
    def reconcile_custom_roles_permissions
      permissions = custom_role_permissions.sort

      # If we have the same number, no custom roles permissions need updating
      return if FineGrainedPermission.custom_roles_enabled.map(&:action).sort == permissions

      # Only grab FGPs to update that need updating
      db_permissions = FineGrainedPermission.where(action: permissions, custom_roles_enabled: false)

      db_permissions.map(&:action).each { |fgp| log "enabling FGP to be used in custom roles: #{fgp}" }

      unless dry_run?
        db_permissions.update(custom_roles_enabled: true)

        if permissions != FineGrainedPermission.custom_roles_enabled.map(&:action).sort
          raise ReconcileError.new("Custom Roles permissions reconciled with mismatches present")
        end
      end
    end

    # Internal: add fine_grained_permissions records defined by configuration, but not found in the DB.
    def add_missing_permissions
      missing_permissions = system_permissions - db_permissions

      missing_permissions.each do |fgp_name|
        log "adding FGP: #{fgp_name}"
        FineGrainedPermission.create!(action: fgp_name) unless dry_run?
      end
    end

    # Internal: add role_permissions records defined by configuration, but not found in the DB.
    #           Depends on reconcile_roles and add_missing_permissions having already been run
    #           so dependencies are in place.
    def add_missing_role_permissions
      fgps = FineGrainedPermission.all

      Role.presets.each do |role|
        system_role_fgps = system_permissions_for_role(role.name)

        # the role has no FGPs associated with it in the configuration
        next unless system_role_fgps.present?

        missing_fgps = system_role_fgps - role.permissions.map(&:action)

        missing_fgps.each do |fgp_name|
          log "adding FGP #{fgp_name} to role #{role.name}"

          unless dry_run?
            fgp = fgps.find_by!(action: fgp_name)

            RolePermission.create!(
              role_id: role.id,
              fine_grained_permission_id: fgp.id,
              action: fgp.action)
          end
        end
      end
    end

    # Internal: remove FGP records which are present in the DB but are not defined in the configuration.
    #           Depends on add_missing_permissions having already been run so dependencies are in place.
    def remove_extra_permissions
      permissions_to_remove = db_permissions - system_permissions

      permissions_to_remove.each do |fgp|
        if purge?
          log "removing FGP: #{fgp}"
        else
          log "if purge = true, would remove FGP: #{fgp}"
        end
      end

      return unless purge?

      FineGrainedPermission.where(action: permissions_to_remove).destroy_all unless dry_run?
    end

    # Internal: remove role_permission records which are present in the DB but are not defined in the configuration.
    #           Depends on add_missing_role_permissions having already been run so dependencies are in place.
    def remove_extra_role_permissions
      Role.presets.each do |role|
        system_role_fgps = system_permissions_for_role(role.name)

        if system_role_fgps.nil?
          if purge?
            log "removing all FGPs from role #{role.name}"
            role.permissions.destroy_all unless dry_run?
          else
            log "if purge = true, would remove all FGPs from role: #{role.name}"
          end

          next
        end

        role.permissions.each do |role_permission|
          next if system_role_fgps.include?(role_permission.action)

          if purge?
            log "removing FGP #{role_permission.action} from role #{role.name}"
            role_permission.destroy! unless dry_run?
          else
            log "if purge = true, would remove FGP #{role_permission.action} from role: #{role.name}"
          end
        end

      end
    end

    # Internal: the list of system roles present in the DB, but not present in the configuration file.
    #           Hence to be deleted from the DB.
    #
    # Returns: an Array of Roles
    def roles_to_remove
      db_roles.reject { |role| system_roles.include?(role) }.values
    end

    # Internal: the list of system roles present in the configuration file, but not present in the DB.
    #           Hence to be added to the DB.
    #
    # Returns: an Array of Roles
    def roles_to_add
      system_roles.each_with_object([]) do |system_role_name, result|
        next if db_role_names.include?(system_role_name)

        result << Role.new(name: system_role_name)
      end
    end

    # Internal: the system role names defined by configuration.
    #
    # Returns: an Array of Strings
    def system_roles
      @config_fgp["system_roles"].keys
    end

    # Internal: the system role's base role name defined by configuration.
    #
    #  - role_name: a String
    #
    # Returns: a String
    def system_base_role_for(role_name)
      @config_fgp.dig("system_roles", role_name, "base")
    end

    # Internal: a list of all the Fine Grained Permission as defined by configuration.
    #
    # Returns: an Array of FGP names
    def system_permissions
      return @fgps if @fgps

      fgps = system_roles.each_with_object([]) do |role, result|
        role_fgps = system_permissions_for_role(role)
        next if role_fgps.nil?

        role_fgps.each { |fgp| result << fgp }
      end

      @fgps = fgps.uniq
    end

    # Internal: the list of Fine Grained Permissions for a given Role.
    #
    # - role_name: a String
    #
    # Returns an Array of Strings or nil if no role is found
    def system_permissions_for_role(role_name)
      @config_fgp.dig("system_roles", role_name, "permissions")
    end

    # Internal: the list of preset roles currently found in the DB.
    #
    # Returns: a Hash of Roles indexed by the role name.
    def db_roles
      Role.presets.index_by(&:name)
    end

    # Internal: the name of the preset roles currently found in the DB.
    #
    # Returns: an Array of Strings
    def db_role_names
      db_roles.keys
    end

    # Internal: the list of FGPs currently found in the DB.
    #
    # Returns: an Array of Strings
    def db_permissions
      FineGrainedPermission.pluck(:action)
    end

    # Internal: Check if the base_roles migration has been run.
    #
    # Returns a Boolean
    def base_roles?
      Role.has_attribute?(:base_role_id)
    end

    # Internal: Check if the custom_roles_enabled migration has been run.
    #
    # Returns a Boolean
    def custom_roles_enabled?
      FineGrainedPermission.has_attribute?(:custom_roles_enabled)
    end

    def log(msg)
      puts msg if verbose?
    end

    def purge?
      @purge
    end

    def dry_run?
      @dry_run
    end

    def verbose?
      @verbose
    end
  end
end
