# rubocop:disable Style/FrozenStringLiteralComment

require "rack/utils"

module Rack
  # Fixes Rails setting Content-Type on 304 Not Modified responses and the like.
  # Rack::Lint chokes on those responses in development and we really shouldn't
  # be sending it back in production either.
  class ContentTypeCleaner
    STATUS_CODES = Rack::Utils::STATUS_WITH_NO_ENTITY_BODY

    attr_accessor :app

    def initialize(app)
      @app = app
    end

    def call(env)
      status, headers, body = app.call(env)
      if STATUS_CODES.include?(status.to_i)
        headers.delete("Content-Type")
        headers.delete("Content-Length")
      end
      [status.to_i, headers, body]
    end
  end
end
