# rubocop:disable Style/FrozenStringLiteralComment

module Rack
  # Middleware that tracks the amount of time this process spends processing
  # requests, as opposed to being idle waiting for a connection. Statistics
  # are dumped to rack.errors every 5 minutes.
  #
  # NOTE This middleware is not thread safe. It should only be used when
  # rack.multiprocess is true and rack.multithread is false.
  class ProcessUtilization

    NPLUS_ONE_THRESHOLD = 500

    DEFAULT_STATSD_SAMPLE_RATE = 0.2

    class SlowRequest < StandardError
      def initialize(action, cpu, idle, real)
        super("#{action} Real: %.2fms (CPU: %.2fms / Idle: %.2fms)" % [real, cpu, idle])
      end
    end

    class StatsCollector
      attr_reader :shards

      def initialize
        clear
        @shards = []
      end

      def clear
        @counts  = Hash.new { |h, k| h[k] = [] }
        @timings = Hash.new { |h, k| h[k] = [] }
        @gauges = Hash.new { |h, k| h[k] = [] }
      end

      def keys
        @timings.keys + @counts.keys + @gauges.keys
      end

      ## statsd compat api

      def batch
        yield
      end

      def increment(name, sample_rate = 1)
        count(name, 1)
      end

      def decrement(name, sample_rate = 1)
        count(name, -1)
      end

      def histogram(name, value, sample_rate = 1)
        timing(name, value, sample_rate)
      end

      def timing(name, value, sample_rate = 1)
        @timings[name.sub("unicorn.benchmark.", "")] << value
      end

      def gauge(name, value, sample_rate = 1)
        @gauges[name.sub("unicorn.benchmark.", "")] << value
      end

      def count(name, value, sample_rate = 1)
        @counts[name.sub("unicorn.benchmark.", "")] << value
      end

      def time(name, sample_rate = 1)
        start = Time.now
        yield
      ensure
        timing(name, (Time.now - start) * 1000)
      end

      def add_shard(*)
      end

      def enable_buffering(*)
      end

      def disable_buffering
      end

      def flush_all
      end

      def namespace=(*)
      end

      ## reporting api

      def [](name)
        if @timings.has_key?(name)
          @timings[name]
        elsif @counts.has_key?(name)
          @counts[name]
        else
          @gauges[name]
        end
      end

      def get(name)
        ((ary = self[name]) && ary[0]) || 0
      end
    end

    class GCStats
      def initialize
        @gc_count       = GC.count
        @minor_gc_count = GC.stat(:minor_gc_count)
        @major_gc_count = GC.stat(:major_gc_count)
        @allocated_objs = total_allocated_objects
      end

      def enabled?
        true
      end

      def eden_pages
        GC.stat(:heap_eden_pages)
      end

      def live_slots
        GC.stat(:heap_live_slots)
      end

      def free_slots
        GC.stat(:heap_free_slots)
      end

      def reset
        GC::Profiler.enable unless GC::Profiler.enabled?
        GC::Profiler.clear
        initialize
      end

      def total_allocated_objects
        GC.stat(:total_allocated_objects)
      end

      def count
        GC.count - @gc_count
      end

      def time
        GC::Profiler.total_time
      end

      def major_count
        GC.stat(:major_gc_count) - @major_gc_count
      end

      def minor_count
        GC.stat(:minor_gc_count) - @minor_gc_count
      end

      # Object stats since last `reset`.
      #
      # Returns a Number of:
      #   number of allocations since `reset`
      def allocations
        total_allocated_objects - @allocated_objs
      end

      # GC stats about the current request.
      #
      # Returns an Array of:
      #   calls - Number of calls to the garbage collection
      #   time  - Time in seconds spent in GC
      #   major - Number of major GCs
      #   minor - Number of minor GCs
      def gc_stats
        [count, time, major_count, minor_count]
      end
    end

    class ElastomerTracker
      def self.build
        if defined? Elastomer
          new
        else
          NullTracker.new
        end
      end

      def initialize
        Elastomer::QueryStats.instance.track = true
      end

      def enabled?
        true
      end

      def count
        stats.count
      end

      def time
        stats.time
      end

      def reset
        stats.clear
      end

      private

      def stats
        Elastomer::QueryStats.instance
      end
    end

    class NullTracker
      def enabled?
        false
      end

      def reset
      end
    end

    class << self
      # The instance of this middleware in a single-threaded production server.
      # Useful for fetching stats about the current request:
      #
      #   o = Rack::ProcessUtilization.singleton
      #   time, calls = o.gc_stats if o.track_gc?
      attr_accessor :singleton
    end

    attr_reader :gc_info, :stats

    def initialize(app, domain, revision, opts = {})
      @app = app
      @domain = domain
      @revision = revision
      @window = opts[:window] || 100
      @horizon = nil
      @active_time = nil
      @requests = nil
      @total_requests = 0
      @worker_number = nil

      setup_stats(opts[:stats], opts[:dogstats])

      self.class.singleton = self
    end

    def setup_stats(stats, dogstats)
      @stats = stats
      @gc_info = NullTracker.new
      @elastomer_tracker = NullTracker.new
      unless @stats
        @track_mysql = @track_graphql = @track_gitrpc = @track_redis = @track_cache = @track_memcached = false
        @track_render = @track_activerecord = false
        @track_tested_features = false
        return
      end

      @dogstats = dogstats
      return unless @dogstats

      @gc_info = GCStats.new

      @hostname ||= GitHub.local_host_name_short
      @track_cpu             = true if defined?(::Process.clock_gettime)
      @track_throttled_cpu   = true unless (GitHub.cpu_stat_file.nil? || GitHub.cpu_stat_file.empty?)
      @track_mysql           = defined?(GitHub::MysqlInstrumenter) && GitHub::MysqlInstrumenter.respond_to?(:query_count)
      @track_graphql         = defined?(Platform)
      @track_gitrpc          = defined?(GitRPCLogSubscriber) && GitRPCLogSubscriber.respond_to?(:rpc_count)
      @track_redis           = defined?(Redis::Client) && Redis::Client.respond_to?(:query_count)
      @track_cache           = defined?(GitHub::Cache::Client) && GitHub::Cache::Client.respond_to?(:query_count)
      @track_memcached       = defined?(Memcached::Rails) && Memcached::Rails.respond_to?(:query_count)
      @track_render          = defined?(ActionView::Template) && ActionView::Template.respond_to?(:template_trace)
      @track_activerecord    = defined?(ActiveRecord::Base) && ActiveRecord::Base.respond_to?(:obj_count)
      @track_tested_features = defined?(FlipperSubscriber) && FlipperSubscriber.respond_to?(:tested_features)
      @elastomer_tracker     = ElastomerTracker.build
      @track_job_stats       = defined?(GitHub::JobStats)
      @track_aqueduct_stats  = defined?(GitHub::Aqueduct::Job)
    end

    # the app's domain name - shown in proctitle
    attr_accessor :domain

    # the currently running git revision as a 7-sha
    attr_accessor :revision

    # time when we began sampling. this is reset every once in a while so
    # averages don't skew over time.
    attr_accessor :horizon

    # total number of requests that have been processed by this worker since
    # the horizon time.
    attr_accessor :requests

    # decimal number of seconds the worker has been active within a request
    # since the horizon time.
    attr_accessor :active_time

    # total requests processed by this worker process since it started
    attr_accessor :total_requests

    # the unicorn worker number
    attr_accessor :worker_number

    # the amount of time since the horizon
    def horizon_time
      Time.now - horizon
    end

    # decimal number of seconds this process has been active since the horizon
    # time. This is the inverse of the active time.
    def idle_time
      horizon_time - active_time
    end

    # percentage of time this process has been active since the horizon time.
    def percentage_active
      (active_time / horizon_time) * 100
    end

    # percentage of time this process has been idle since the horizon time.
    def percentage_idle
      (idle_time / horizon_time) * 100
    end

    # number of requests processed per second since the horizon
    def requests_per_second
      requests / horizon_time
    end

    # average response time since the horizon in milliseconds
    def average_response_time
      (active_time / requests.to_f) * 1000
    end

    # called exactly once before the first request is processed by a worker
    def first_request
      reset_horizon
      record_worker_number
    end

    def track_gc?
      @gc_info.enabled?
    end

    # reset various counters before the new request
    def reset_stats
      @gc_info.reset

      if track_mysql?
        GitHub::MysqlInstrumenter.reset_stats
      end
      if track_graphql?
        Platform::GlobalScope.reset!
      end
      if track_gitrpc?
        GitRPCLogSubscriber.reset_stats
      end
      if track_cache?
        GitHub::Cache::Client.tap { |cache| cache.query_count = cache.query_time = 0; cache.query_keys = Hash.new(0) }
      end
      if track_memcached?
        Memcached::Rails.tap { |memcache| memcache.query_count = memcache.query_time = 0 }
      end
      if track_redis?
        Redis::Client.reset_stats
      end
      if track_es?
        @elastomer_tracker.reset
      end
      if track_activerecord?
        QueryCacheLogSubscriber.reset_stats
        ActiveRecord::Base.tap { |ar| ar.obj_count = 0; ar.obj_types.clear }
      end
      @start = Time.now
      if track_cpu?
        @cputime = Process.clock_gettime(Process::CLOCK_PROCESS_CPUTIME_ID)
      end
      if track_throttled_cpu?
        @throttled_cpu_ns_start = cpu_throttled_stats
        @throttled_cpu_ns_end = 0.0
        @throttled_cpu_ms = 0.0
      end

      if track_tested_features?
        FlipperSubscriber.reset_stats
      end

      if track_job_stats?
        GitHub::JobStats.reset
      end
      if track_aqueduct_stats?
        GitHub::Aqueduct::Job.reset_stats
      end
    end

    # resets the horizon and all dependent variables
    def reset_horizon
      @horizon = Time.now
      @active_time = 0.0
      @requests = 0
    end

    # extracts the worker number from the unicorn procline
    def record_worker_number
      if $0 =~ /^.* worker\[(\d+)\].*$/
        @worker_number = $1.to_i
      else
        @worker_number = nil
      end
    end

    # the generated procline
    def procline
      "unicorn %s[%s] worker[%02d]: %5d reqs, %4.1f req/s, %4dms avg, %5.1f%% util" % [
        domain,
        revision,
        worker_number || 99,
        total_requests,
        requests_per_second,
        average_response_time,
        percentage_active,
      ]
    end

    def path_parameters
      @env && @env["action_dispatch.request.path_parameters"]
    end

    def api_parameters
      return nil unless @env
      app = @env["process.api_app"]
      route = @env["sinatra.route"]
      return nil unless app && route
      { app: app, route: route }
    end

    # called in `call` to compute the time the request spent waiting to be
    # serviced by a unicorn
    def request_wait_time(env)
      return nil unless env["HTTP_X_REQUEST_START"]
      nginx_start = env["HTTP_X_REQUEST_START"].sub("t=", "").to_f
      return @start.to_f - nginx_start if nginx_start > Float::MIN
    end

    # called immediately after a request to record statistics, update the
    # procline, and dump information to the logfile
    def record_request(response_size, status_code, env)
      now = Time.now
      diff = (now - @start)
      @active_time += diff
      @requests += 1

      if @stats
        if track_cpu?
          cpu, idle, real = cpu_stats
        end
        if track_throttled_cpu?
          @throttled_cpu_ns_end = cpu_throttled_stats
          @throttled_cpu_ms = ((@throttled_cpu_ns_end - @throttled_cpu_ns_start) / 1000000.0).round(3)
        end

        if track_activerecord?
          ar_count, ar_types, query_cache_hits = activerecord_stats
        else
          ar_count = ar_types = query_cache_hits = -1
        end

        if track_mysql?
          mysql_count, mysql_time, query_counts = mysql_stats
        else
          mysql_count = mysql_time = -1
          query_counts = Hash.new(0)
        end

        if track_gitrpc?
          gitrpc_count, gitrpc_time, _ = gitrpc_stats
        else
          gitrpc_count = gitrpc_time = -1
        end

        rails_version = GitHub.rails_version_key
        record_dogstats rails_version, diff, response_size, status_code, env

        request_categories = [request_category]

        # Keep tracking "browser_*" under "browser" too to keep our
        # historical data valid. (Ditto for "anon_*".)
        if match = request_category.match(/\A(browser|anon)_/)
          request_categories << match[1]
        end

        # Track all robot requests under a single umbrella category, too
        if request_category.to_s.start_with?("robot-")
          request_categories << "robot"
        end

        request_categories.each do |request_category|
          record_stats request_category, diff, response_size, status_code
        end

        if track_cpu? && GitHub.instrument_slow_requests?
          stats = stats_for_failbot_context.merge(response_size: response_size)
          record_request_stats(cpu, idle, real, query_counts, stats)
        end

        @dogstats.batch do
          DynSampler.update_maps.each do |metric|
            tags = ["category:#{metric[:category]}"]
            @dogstats.gauge "dynsampler.keys", metric[:keys], tags: tags, sample_rate: 0.001
            @dogstats.gauge "dynsampler.sample_rate_low", metric[:low], tags: tags, sample_rate: 0.001
            @dogstats.gauge "dynsampler.sample_rate_high", metric[:high], tags: tags, sample_rate: 0.001
          end
        end
      end

      $0 = procline
      reset_horizon if now - horizon > @window
    rescue => boom
      warn "ProcessUtilization#record_request failed: #{boom.inspect}\n#{boom.backtrace.join("\n")}"
    end

    # Public: Returns all statistics as a Hash suitable for inclusion in a
    # Failbot exception context.
    def stats_for_failbot_context
      stats = {}

      stats[:time_cpu], stats[:time_idle], stats[:time_real] = cpu_stats if track_cpu?
      stats[:time_cpu_throttled_ms] = @throttled_cpu_ms if track_throttled_cpu?
      if track_activerecord?
        ar_count, ar_types, query_cache_hits = activerecord_stats
        stats[:count_activerecord] = ar_count
        stats[:activerecord_objects] = format_count_hash(ar_types)
        stats[:activerecord_query_cache_hits] = query_cache_hits
      end
      stats[:count_allocations] = @gc_info.allocations if @gc_info.enabled?
      stats[:count_cache], stats[:time_cache] = cache_stats if track_cache?
      stats[:count_es], stats[:time_es] = es_stats if track_es?
      stats[:count_gc], stats[:time_gc] = @gc_info.gc_stats if @gc_info.enabled?
      if track_gitrpc?
        stats[:count_gitrpc], stats[:time_gitrpc] = gitrpc_stats
        stats[:gitrpc_calls] = format_gitrpc_call_stats(GitRPCLogSubscriber.rpc_call_stats)
      end
      stats[:count_memcached], stats[:time_memcached] = memcached_stats if track_memcached?

      if track_mysql?
        stats[:count_mysql], stats[:time_mysql], query_counts = mysql_stats
        stats[:query_counts] = format_count_hash(query_counts)
      end

      stats[:count_redis], stats[:time_redis] = redis_stats if track_redis?

      if track_graphql?
        stats[:graphql_queries] = format_graphql_queries(Platform::GlobalScope.queries.map { |q| q[:string] })
      end

      # Round times to the nearest millisecond.
      stats.each do |key, value|
        next unless key.to_s.start_with?("time_")
        stats[key] = value.round(3)
      end

      stats
    end

    def format_graphql_queries(queries)
      queries.join("#{'-' * 40}\n")
    end

    def format_count_hash(hash)
      self.class.format_count_hash(hash)
    end

    def self.format_count_hash(hash)
      lines = hash.sort_by { |key, count| -count }.map do |key, count|
        "%7d | #{key}\n" % count
      end
      lines.join("")
    end

    def format_gitrpc_call_stats(call_stats)
      return "" if call_stats.empty?
      rows = call_stats
        .sort_by { |command, stats| -stats.time }
        .map     { |command, stats| sprintf("%7.3f | %7d | %s\n", stats.time, stats.count, command) }
      sprintf("%7s | %7s | %s\n%s", "Seconds", "Count", "Command", rows.join(""))
    end

    def record_request_stats(cpu, idle, real, query_counts, opts = {})
      if real > GitHub.slow_request_threshold
        klass = SlowRequest
        if params = path_parameters
          app = "github-slow-request"
          rollup = "#{@env[GitHub::TaggingHelper::PROCESS_REQUEST_CATEGORY]}:#{params[:controller]}##{params[:action]}"
        elsif params = api_parameters
          app = "github-slow-api-request"
          rollup = "#{params[:app]} #{params[:route]}"
        else
          return
        end

        req = klass.new(rollup, cpu * 1000, idle * 1000, real * 1000)

        opts = opts.merge(
          app: app,
          controller_action: rollup,
          rollup: Digest::MD5.hexdigest("#{req.class}#{rollup}"),
        )

        Failbot.report!(req, opts)
      end
    end

    def record_stats(category, real, response_size, status_code)
      @stats.increment "unicorn.#{category}.status_code.#{status_code}.count"

      if track_cpu? && @cputime
        cpu, idle, _ = cpu_stats

        @stats.timing "unicorn.#{category}.cpu_time", cpu * 1000
        @stats.timing "unicorn.#{category}.idle_time", idle * 1000
      end

      if @gc_info.enabled?
        @stats.timing "unicorn.#{category}.gc.allocations", @gc_info.allocations
        @stats.count  "unicorn.#{category}.gc.collections", @gc_info.count
        @stats.timing "unicorn.#{category}.gc.time", @gc_info.time * 1000
        @stats.count  "unicorn.#{category}.gc.major", @gc_info.major_count
        @stats.count  "unicorn.#{category}.gc.minor", @gc_info.minor_count
      end

      if track_mysql?
        mysql_count, mysql_time = mysql_stats
        if mysql_count > 0
          @stats.count  "unicorn.#{category}.mysql.queries", mysql_count
          @stats.timing "unicorn.#{category}.mysql.time", mysql_time * 1000
        end
      end

      if track_gitrpc?
        gitrpc_count, gitrpc_time = gitrpc_stats
        if gitrpc_count > 0
          @stats.count  "unicorn.#{category}.gitrpc.rpcs", gitrpc_count
          @stats.timing "unicorn.#{category}.gitrpc.time", gitrpc_time * 1000
        end
      end

      if track_cache?
        cache_count, cache_time = cache_stats
        if cache_count > 0
          @stats.count  "unicorn.#{category}.cache.queries", cache_count
          @stats.timing "unicorn.#{category}.cache.time", cache_time * 1000
        end
      end

      if track_memcached?
        memcached_count, memcached_time = memcached_stats
        if memcached_count > 0
          @stats.count  "unicorn.#{category}.memcached.queries", memcached_count
          @stats.timing "unicorn.#{category}.memcached.time", memcached_time * 1000
        end
      end

      if track_redis?
        redis_count, redis_time = redis_stats
        if redis_count > 0
          @stats.count  "unicorn.#{category}.redis.queries", redis_count
          @stats.timing "unicorn.#{category}.redis.time", redis_time * 1000
        end
      end

      if track_es?
        es_count, es_time = es_stats
        if es_count > 0
          @stats.count  "unicorn.#{category}.es.queries", es_count
          @stats.timing "unicorn.#{category}.es.time", es_time * 1000
        end
      end

      if track_activerecord?
        ar_count, ar_types, query_cache_hits = activerecord_stats
        if ar_count > 0
          @stats.count  "unicorn.#{category}.activerecord.objs", ar_count
        end
      end

      @stats.timing("unicorn.#{category}.memrss", GitHub::Memory.memrss)
      @stats.timing("unicorn.#{category}.requests_per_second", requests_per_second)

      if track_cpu?
        @stats.timing("unicorn.#{category}.response_time", real * 1000)
      end

      @stats.timing("unicorn.#{category}.response_size", response_size)
    end

    def record_dogstats(rails_version, real, response_size, status_code, env)
      rails_version ||= GitHub::TaggingHelper::RAILS_DEFAULT
      tags = datadog_tags
      controller = GitHub::TaggingHelper.controller(env)
      action = GitHub::TaggingHelper.action(env)

      ca_tags = tags.clone
      if controller && action
        ca_tags += ["#{GitHub::TaggingHelper::CONTROLLER_TAG}:#{controller}"]
        if statsd_tag_actions?
          ca_tags += ["#{GitHub::TaggingHelper::ACTION_TAG}:#{action}"]
        else
          # Lump non-tagged actions under a generic value.  This allows
          # comparing tagged actions against all others without exploding
          # cardinality.
          ca_tags += ["#{GitHub::TaggingHelper::ACTION_TAG}:untagged"]
        end
      end

      @dogstats.batch do |dogstats|
        if @gc_info.enabled?
          dogstats.distribution "request.dist.gc.allocations", @gc_info.allocations, tags: ca_tags
          dogstats.distribution "request.dist.gc.count", @gc_info.major_count, tags: ca_tags + ["level:major"]
          dogstats.distribution "request.dist.gc.count", @gc_info.minor_count, tags: ca_tags + ["level:minor"]

          dogstats.distribution "request.dist.gc.collections", @gc_info.count, tags: ca_tags
          dogstats.distribution "request.dist.gc.time", @gc_info.time * 1000, tags: ca_tags
          dogstats.gauge "ruby.gc.eden_pages", @gc_info.eden_pages, tags: ca_tags, sample_rate: statsd_sample_rate
          dogstats.gauge "ruby.gc.live_slots", @gc_info.live_slots, tags: ca_tags, sample_rate: statsd_sample_rate
          dogstats.gauge "ruby.gc.free_slots", @gc_info.free_slots, tags: ca_tags, sample_rate: statsd_sample_rate
        end

        if track_mysql?
          mysql_count, mysql_time = mysql_stats
          mysql_tags = tags + [
            "rpc_store:mysql",
          ]
          dogstats.histogram "request.rpc.queries", mysql_count, tags: mysql_tags, sample_rate: statsd_sample_rate
          dogstats.timing "request.rpc.time", mysql_time * 1000, tags: mysql_tags, sample_rate: statsd_sample_rate
          dogstats.distribution "request.rpc.dist.time", mysql_time * 1000, tags: mysql_tags

          mysql_primary_count = GitHub::MysqlInstrumenter.primary_query_count
          mysql_primary_tags = tags + [
            "method:#{env['REQUEST_METHOD']}",
          ]
          dogstats.histogram "request.mysql_primary.queries", mysql_primary_count, tags: mysql_primary_tags, sample_rate: statsd_sample_rate
        end

        if track_gitrpc?
          gitrpc_count, gitrpc_time = gitrpc_stats
          gitrpc_tags = tags + [
            "rpc_store:gitrpc",
          ]
          dogstats.histogram "request.rpc.queries", gitrpc_count, tags: gitrpc_tags, sample_rate: statsd_sample_rate
          dogstats.timing "request.rpc.time", gitrpc_time * 1000, tags: gitrpc_tags, sample_rate: statsd_sample_rate
          dogstats.distribution "request.rpc.dist.time", gitrpc_time * 1000, tags: gitrpc_tags
        end

        if track_memcached?
          memcached_count, memcached_time = memcached_stats
          memcached_tags = tags + [
            "rpc_store:memcached",
          ]
          dogstats.histogram "request.rpc.queries", memcached_count, tags: memcached_tags, sample_rate: statsd_sample_rate
          dogstats.timing "request.rpc.time", memcached_time * 1000, tags: memcached_tags, sample_rate: statsd_sample_rate
          dogstats.distribution "request.rpc.dist.time", memcached_time * 1000, tags: memcached_tags
        end

        if track_redis?
          redis_count, redis_time = redis_stats
          redis_tags = tags + [
            "rpc_store:redis",
          ]
          dogstats.histogram "request.rpc.queries", redis_count, tags: redis_tags, sample_rate: statsd_sample_rate
          dogstats.timing "request.rpc.time", redis_time * 1000, tags: redis_tags, sample_rate: statsd_sample_rate
          dogstats.distribution "request.rpc.dist.time", redis_time * 1000, tags: redis_tags
        end

        if track_es?
          es_count, es_time = es_stats
          es_tags = tags + [
            "rpc_store:elasticsearch",
          ]
          dogstats.histogram "request.rpc.queries", es_count, tags: es_tags, sample_rate: statsd_sample_rate
          dogstats.timing "request.rpc.time", es_time * 1000, tags: es_tags, sample_rate: statsd_sample_rate
          dogstats.distribution "request.rpc.dist.time", es_time * 1000, tags: es_tags
        end

        if track_activerecord?
          ar_count, ar_types, query_cache_hits = activerecord_stats
          dogstats.histogram "request.activerecord.query_cache_hits", query_cache_hits, tags: ca_tags, sample_rate: statsd_sample_rate
          dogstats.histogram "request.activerecord.objects", ar_count, tags: ca_tags, sample_rate: statsd_sample_rate
        end

        dogstats.histogram "response.size", response_size, tags: ca_tags, sample_rate: statsd_sample_rate

        dogstats.histogram "request.memory_rss", GitHub::Memory.memrss, sample_rate: statsd_sample_rate
        dogstats.histogram "request.per_second", requests_per_second, sample_rate: statsd_sample_rate

        if queue_time = env[GitHub::TaggingHelper::REQ_WAIT_TIME]
          queue_ms = queue_time * 1_000
          dogstats.distribution "request.queued.time", queue_ms, tags: ca_tags
        end

        if track_job_stats?
          dogstats.histogram("request.jobs_enqueued", GitHub::JobStats.enqueued.values.sum)
        end
      end
    end

    # The request's category. This may be set at any point during the request to
    # log stats under a separate category. Values include: browser, api, robot-#{type},
    # raw, feed, and other.
    #
    # To change the category, write to the process.request_category key in the Rack
    # env.
    def request_category
      @env && @env[GitHub::TaggingHelper::PROCESS_REQUEST_CATEGORY] || GitHub::TaggingHelper::CATEGORY_DEFAULT
    end

    def request_category_datadog
      @env && @env[GitHub::TaggingHelper::PROCESS_REQUEST_CATEGORY_DATADOG] || GitHub::TaggingHelper::CATEGORY_DEFAULT
    end

    def statsd_sample_rate
      (@env && @env[GitHub::TaggingHelper::STATSD_SAMPLE_RATE] || DEFAULT_STATSD_SAMPLE_RATE).to_f
    end

    def statsd_tag_actions?
      @env && @env[GitHub::TaggingHelper::STATSD_TAG_ACTIONS] || false
    end

    def datadog_tags
      tags = []

      category = request_category_datadog
      tags << "category:#{category}"

      if @env
        if @env.key?(GitHub::TaggingHelper::PROCESS_REQUEST_PJAX)
          tags << "pjax:#{@env[GitHub::TaggingHelper::PROCESS_REQUEST_PJAX] || "false"}"
        end

        if @env.key?(GitHub::TaggingHelper::PROCESS_REQUEST_LOGGED_IN)
          tags << "logged_in:#{@env[GitHub::TaggingHelper::PROCESS_REQUEST_LOGGED_IN]}"
        end
      end

      tags.freeze
    end

    def track_cpu?
      @track_cpu
    end

    # CPU usage stats for the current request.
    #
    # Returns an Array of
    #   cpu  - Time spent on the CPU
    #   idle - Time spent in I/O
    #   real - Total time (idle + cpu_time)
    def cpu_stats
      cpu = Process.clock_gettime(Process::CLOCK_PROCESS_CPUTIME_ID) - @cputime
      real = Time.now - @start
      idle = real - cpu

      [cpu, idle, real]
    end

    def track_throttled_cpu?
      @track_throttled_cpu
    end

    # CPU throttled stats for the current request.  Here in
    # Rack::ProcessUtilization this is only used for including
    # the value in Failbot context.
    #
    # Returns a float in nanoseconds
    def cpu_throttled_stats
      _, _, throttled_time_ns = GitHub::CPU::cpu_throttled_stats
      throttled_time_ns
    end

    def track_mysql?
      @track_mysql
    end

    # MySQL stats for the current request.
    #
    # Returns an Array of
    #   query_count - Number of mysql queries performed
    #   query_time  - A Float time in seconds spent querying
    def mysql_stats
      mysql = GitHub::MysqlInstrumenter
      [mysql.query_count, mysql.query_time, mysql.query_counts]
    end

    def track_graphql?
      @track_graphql
    end

    def graphql_stats
      graphql = Platform::GlobalScope
      [graphql.query_count, graphql.query_time]
    end

    def track_gitrpc?
      @track_gitrpc
    end

    # GitRPC stats for the current request.
    #
    # Returns an Array of
    #   rpc_count - Number of rpc queries performed
    #   rpc_time  - A Float time in seconds spent querying
    #   rpc_calls - An Array of EventTrace objects
    def gitrpc_stats
      [
        GitRPCLogSubscriber.rpc_count,
        GitRPCLogSubscriber.rpc_time,
        GitRPCLogSubscriber.rpc_calls,
      ]
    end

    def track_redis?
      @track_redis
    end

    # Redis stats for current request.
    def redis_stats
      redis = Redis::Client
      [redis.query_count, redis.query_time, redis.queries]
    end

    def track_es?
      @elastomer_tracker.enabled?
    end

    # ElasticSearch stats for the current request.
    #
    # Returns an Array of
    #   count - Number of elasticsearch queries performed
    #   time  - An Integer time in seconds spent querying
    def es_stats
      [@elastomer_tracker.count, @elastomer_tracker.time / 1000.0]
    end

    def track_cache?
      @track_cache
    end

    # GitHub::Cache stats for current request.
    def cache_stats
      cache = GitHub::Cache::Client
      [cache.query_count, cache.query_time]
    end

    def track_memcached?
      @track_memcached
    end

    # Memcached stats for current request.
    def memcached_stats
      memcached = Memcached::Rails
      [memcached.query_count, memcached.query_time]
    end

    def track_activerecord?
      @track_activerecord
    end

    def track_glb?
      @glb_via.present?
    end

    def track_job_stats?
      @track_job_stats
    end

    def track_aqueduct_stats?
      @track_aqueduct_stats
    end

    # Converts a header like:
    #   hostname=glb-proxy12-cp1-prd.iad.github.net t=2; hostname=github-fe-e841886.sdc42-sea.github.net
    #
    # into an array of hashes like this:
    #   [
    #     {"hostname" => "glb-proxy12-cp1-prd.iad.github.net", "t" => "2"},
    #     {"hostname" => "github-fe-e841886.sdc42-sea.github.net"},
    #   ]
    #
    # Returns Array of Hashes.
    def glb_via
      return [] unless track_glb?
      hops = @glb_via.split(",".freeze)
      hops.map { |hop|
        key_value_pairs = hop.split(" ".freeze)
        key_value_pairs.inject({}) do |hash, key_value_pair|
          key, value = key_value_pair.split("=".freeze, 2)
          hash[key.strip] = value.strip
          hash
        end
      }
    end

    # AR stats for the current request.
    #
    # Returns an Array of
    #   obj_count  - Number of AR objects initialized
    #   obj_types  - Hash of ModelName:count entries
    #   cache_hits - Number of AR query cache hits
    def activerecord_stats
      ar = ActiveRecord::Base
      [
        ar.obj_count,
        ar.obj_types,
        QueryCacheLogSubscriber.cache_hit_count,
      ]
    end

    def track_render?
      @track_render
    end

    # Public: Provides the names of features that have been queried to
    # determine if they are `enabled?`
    #
    # Returns a Hash in the form of { String => Boolean }
    def tested_features
      return unless track_tested_features?
      FlipperSubscriber.tested_features
    end

    # Public: provides the names of current enabled features
    def enabled_features
      return unless track_tested_features?
      tested_features.select { |feature, enabled| enabled }.map(&:first)
    end

    def track_tested_features?
      @track_tested_features
    end

    # Template rendering stats for the current request.
    #
    # Returns a Rack::Bug::TemplatesPanel::Trace
    def render_stats
      ActionView::Template.template_trace
    end

    # Body wrapper. Yields the body's bytesize to the block when body is
    # closed. This is used to signal when a response is fully finished
    # processing.
    class Body
      def initialize(body, &block)
        @body = body
        @block = block
        @bytesize = 0
      end

      def each
        @body.each do |content|
          @bytesize += content.bytesize
          yield content
        end
      end

      def close
        @body.close if @body.respond_to?(:close)
        @block.call(@bytesize)
        nil
      end
    end

    ENV_KEY = "github.rack.process_utilization"

    # Rack entry point.
    def call(env)
      env[ENV_KEY] = self

      # Skip stats and procline on warmup requests in unicorn master
      return @app.call(env) if GitHub.unicorn_master_pid == Process.pid

      @env = env
      reset_stats

      @total_requests += 1
      first_request if @total_requests == 1

      env[GitHub::TaggingHelper::PROCESS_REQUEST_CATEGORY]           = nil
      env[GitHub::TaggingHelper::PROCESS_REQUEST_CATEGORY_DATADOG]   = nil
      env[GitHub::TaggingHelper::PROCESS_REQUEST_PJAX]               = nil
      env[GitHub::TaggingHelper::PROCESS_REQUEST_LOGGED_IN]          = nil
      env[GitHub::TaggingHelper::PROCESS_REQUEST_ROBOT_NAME]         = nil
      env[GitHub::TaggingHelper::REQ_WAIT_TIME]                      = request_wait_time(env)
      env["process.request_start"]                                   = @start.to_f
      env["process.total_requests"]                                  = total_requests
      env[GitHub::TaggingHelper::STATSD_SAMPLE_RATE]                 = DEFAULT_STATSD_SAMPLE_RATE
      env[GitHub::TaggingHelper::STATSD_TAG_ACTIONS]                 = false
      GitHub.unicorn_worker_request_count                            = total_requests

      @glb_via = env["HTTP_X_GLB_VIA"]

      status, headers, body = @app.call(env)
      body = Body.new(body) do |response_size|
        record_request(response_size, status, env)
      end
      [status, headers, body]
    end
  end
end
