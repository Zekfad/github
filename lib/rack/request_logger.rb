# rubocop:disable Style/FrozenStringLiteralComment

module Rack
  # Rack::RequestLogger is a drop in replacement for Rack::CommonLogger and is
  # setup to pull relevent information from the env and use GitHub::Logger to
  # get request data into our logging pipeline.
  #
  # This is middleware due to simplicity that comes with putting this here
  # rather than in API and the Rails app seperately. Those two entry points
  # should focus on user data logging rather than request logging.
  class RequestLogger

    APPLICATION_LOG_DATA = "APPLICATION_LOG_DATA".freeze

    def initialize(app)
      @app = app
    end

    def call(env)
      began_at = Time.now
      env[APPLICATION_LOG_DATA] = GitHub::Logger.empty
      status, header, body = @app.call(env)
      header = Utils::HeaderHash.new(header)
      elapsed = (Time.now - began_at)
      body = BodyProxy.new(body) { self.class.log(env, status, header, elapsed) }
      [status, header, body]
    end

    # Ignore logging requests containing `/assets` as the path_info in dev
    def self.asset_path_in_development?(env)
      Rails.development? && env["PATH_INFO"] =~ /^\/assets/
    end

    # Helper methods to determine if the current request is being made by a robot,
    # and if so, which one.
    def self.robot?(request)
      GitHub.robot?(request.user_agent.to_s)
    end

    def self.robot_type(request)
      GitHub.robot_type(request.user_agent.to_s)
    end

    def self.sensitive_params_filter
      @sensitive_params_filter ||= GitHub::ParameterFilter.create
    end

    def self.filter_params(hash)
      return "" if hash.blank?
      "?#{sensitive_params_filter.filter(hash).to_param}"
    end

    # Logs data for a Request using GitHub::Logger.
    #
    # env     - Rack env or Hash.
    # status  - Number HTTP status for the response.
    # elapsed - Float elapsed response time in seconds.
    # data    - Optional hash of additional logging info.
    #
    # Returns nothing.
    def self.log(env, status, header, elapsed, data = {})
      # Silence /assets in Development.
      return if asset_path_in_development? env

      request = Rack::Request.new(env)

      # Populated by application.
      app_data = env[APPLICATION_LOG_DATA]

      url = "#{request.base_url}#{request.path}#{filter_params(request.GET)}"

      context = {
        "now"                  => Time.now.iso8601,
        "request_id"           => Rack::RequestId.get(env),
        "datacenter"           => GitHub.datacenter,
        "site"                 => GitHub.server_site,
        "region"               => GitHub.server_region,
        "server_id"            => Rack::ServerId.get(env),
        "remote_address"       => request.ip,
        "request_method"       => request.request_method.downcase,
        # not `host` because it would conflict with syslog:
        "request_host"         => request.host,
        "path_info"            => "\"#{request.path_info}\"",
        "content_length"       => header["Content-Length"] || 0,
        "content_type"         => header["Content-Type"],
        "user_agent"           => request.user_agent,
        "accept"               => env["HTTP_ACCEPT"],
        "language"             => env["HTTP_ACCEPT_LANGUAGE"],
        "referer"              => env["HTTP_REFERER"],
        "x_requested_with"     => env["HTTP_X_REQUESTED_WITH"],
        "status"               => status.to_s[0..3],
        "elapsed"              => elapsed,
        "url"                  => url,
        "request_wait_time"    => env[GitHub::TaggingHelper::REQ_WAIT_TIME],
        "worker_request_count" => GitHub.unicorn_worker_request_count,
        "request_category"     => env["process.request_category"] || "other",
      }.reject { |k, v| v.nil? }

      context["robot"] = robot_type(request) if robot?(request)

      if route = Api::App.route_pattern(env)
        context["route"] = route
      end

      log_data = app_data.merge(context).merge(data.stringify_keys)

      GitHub::Logger.log(log_data)
    end
  end
end
