# rubocop:disable Style/FrozenStringLiteralComment

module Rack
  class MalformedRequestHandler

    def initialize(app)
      @app = app
    end

    def call(env)
      drop_invalid_cookies(env)
      @app.call(env)
    rescue ArgumentError => boom
      # work around https://github.com/github/github/issues/39783
      if boom.to_s =~ /^invalid %-encoding/
        [400, {"Content-Type" => "text/plain"}, ["400 Bad Request\n"]]
      else
        GitHub::Logger.log_exception({method: "MalformedRequestHandler.call"}, boom)
        raise ArgumentError.new("malformed request") # prevent PII leakage
      end
    end

    # Newer rack versions try to parse cookies as form-url-encoded for some
    # reason, when it should be treating them as opaque. Sometimes cookies have a
    # '%' in them, which confuses the parser, which raises an `ArgumentError`,
    # which causes the app the throw a 500, which is bad.
    def drop_invalid_cookies(env)
      if env["HTTP_COOKIE"]
        valid_cookies = []

        env["HTTP_COOKIE"].split(/[;,] */n).each do |cookie|
          name, value = cookie.split("=", 2)
          valid_cookies << cookie if understandable_by_rack?(value)
        end

        env["HTTP_COOKIE"] = valid_cookies.join("; ")
      end
    end

    protected

    # Stolen from URI.decode_www_form_component, so we can avoid a slow raise &
    # rescue.
    URI_DECODE_VALIDATION_REGEX = /\A(?:%[0-9a-fA-F]{2}|[^%])*\z/
    def understandable_by_rack?(cookie_value)
      URI_DECODE_VALIDATION_REGEX =~ cookie_value
    end
  end
end
