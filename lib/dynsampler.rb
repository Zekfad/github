# frozen_string_literal: true

# This is based on the https://github.com/travis-ci/dynsampler-rb, which is
# based on the Honeycomb.io dynamic sampling golang library.  It's been modified
# remove locking and threading and return sample rates from 0.0 to 1.0, instead
# of 1/x numbers.  It also now has set of class methods to make it easier to
# manage global instances across requests.

### Comment from the original source
# AvgSampleWithMin implements Sampler and attempts to average a given sample
# rate, with a minimum number of events per second (i.e. it will reduce
# sampling if it would end up sending fewer than the mininum number of events).
# This method attempts to get the best of the normal average sample rate
# method, without the failings it shows on the low end of total traffic
# throughput
#
# Keys that occur only once within ClearFrequencySec will always have a sample
# rate of 1. Keys that occur more frequently will be sampled on a logarithmic
# curve. In other words, every key will be represented at least once per
# ClearFrequencySec and more frequent keys will have their sample rate
# increased proportionally to wind up with the goal sample rate.
###

class DynSampler
  attr_accessor :saved_sample_rates, :current_counts, :have_data

  @@categories = {}

  def self.get_sampler(category, **options)
    @@categories[category] ||= self.new(**options)
  end

  def self.update_maps(force: false)
    metrics = []
    @@categories.each_pair do |name, category|
      cat_metrics = category.update_maps(force: force)
      cat_metrics[:category] = name
      metrics << cat_metrics
    end
    return metrics
  end

  def initialize(goal_sample_rate: 0.1, min_events_per_sec: 1, clear_frequency_sec: 30)
    @external_sample_rate = goal_sample_rate
    @goal_sample_rate = (1/goal_sample_rate)
    @min_events_per_sec = min_events_per_sec
    @clear_frequency_sec = clear_frequency_sec

    @saved_sample_rates = {}
    @current_counts = {}

    @have_data = false

    @next_clear = Time.now + @clear_frequency_sec
  end

  def sample_rate(key)
    @current_counts[key] ||= 0
    @current_counts[key] += 1

    if @have_data
      reciprocal(@saved_sample_rates[key] || 1)
    else
      1.0
    end
  end

  def update_maps(force: false)
    if !force && @next_clear > Time.now
      return { keys: 0, high: 1.0, low: 1.0}
    end

    @next_clear = Time.now + @clear_frequency_sec

    tmp_counts = @current_counts
    @current_counts = {}

    new_saved_sample_rates = {}

    num_keys = tmp_counts.size
    if num_keys == 0
      @saved_sample_rates = {}
      return { keys: 0, high: 1.0, low: 1.0}
    end

    sum_events = tmp_counts.values.reduce(0, :+)
    goal_count = sum_events.to_f/@goal_sample_rate.to_f

    if sum_events < @min_events_per_sec * @clear_frequency_sec
      new_saved_sample_rates = tmp_counts.keys.map { |k| [k, 1] }.to_h
      @saved_sample_rates = new_saved_sample_rates
      return { keys: @saved_sample_rates.length, high: 1.0, low: 1.0}
    end

    log_sum = tmp_counts.values.map { |v| Math.log10(v.to_f) }.reduce(0.0, :+)
    # If log_sum is zero, that means every event has happened exactly once.
    # Dividing by zero will cause problems further down when we try to to
    # multiply the NaN that results from log10(count).  Set the log_sum to -1 to
    # make sure log_sum is valid, but loses that comparison.
    log_sum = -1 if log_sum == 0
    goal_ratio = goal_count/log_sum


    keys = tmp_counts.keys.sort

    keys_remaining = keys.size
    extra = 0.0
    keys.each do |key|
      count = tmp_counts[key].to_f
      goal_for_key = [1, Math.log10(count)*goal_ratio].max

      extra_for_key = extra / keys_remaining.to_f
      goal_for_key += extra_for_key

      extra -= extra_for_key
      keys_remaining -= 1

      if count <= goal_for_key
        new_saved_sample_rates[key] = 1
        extra += goal_for_key - count
      else
        new_saved_sample_rates[key] = (count / goal_for_key).ceil.to_i
        extra += goal_for_key - (count / new_saved_sample_rates[key].to_f)
      end
    end

    @saved_sample_rates = new_saved_sample_rates
    @have_data = true

    low = reciprocal(@saved_sample_rates.values.sort.first)
    high = reciprocal(@saved_sample_rates.values.sort.last)
    return { keys: @saved_sample_rates.length, low: low, high: high}
  end

  def reciprocal(x)
    r = 1.0 / x
    if r < 0.1
      r.round(3)
    else
      r.round(2)
    end
  end
end
