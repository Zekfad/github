# frozen_string_literal: true

module Flipper
  class Groups
    def self.register_groups
      ::Flipper.register(:preview_features) do |actor|
        actor.respond_to?(:preview_features?) && actor.preview_features?
      end

      ::Flipper.register(:bounty_hunter_target) do |actor|
        actor.respond_to?(:bounty_hunter_target?) && actor.bounty_hunter_target?
      end

      ::Flipper.register(:maintainers_early_access) do |actor|
        actor.respond_to?(:maintainers_early_access?) && actor.maintainers_early_access?
      end

      ::Flipper.register(:opted_in) do |actor, context|
        actor.respond_to?(:beta_feature_enabled?) &&
          context.respond_to?(:feature_name) &&
          context.feature_name.present? &&
          actor.beta_feature_enabled?(context.feature_name)
      end

      ::Flipper.register(:integrators_early_access) do |actor|
        actor.respond_to?(:integrators_early_access?) && actor.integrators_early_access?
      end

      ::Flipper.register(:early_access_enabled) do |gate, context|
        next unless context.respond_to?(:feature_name)

        actor = context.thing.thing
        feature_name = context.feature_name

        result = GitHub.cache.fetch(EarlyAccessMembership.member_enabled_key(feature_name, actor), ttl: EarlyAccessMembership::FEATURE_CHECK_TTL) do
          [EarlyAccessMembership.member_enabled?(feature_name, actor)]
        end

        result.first
      end
    end

    def self.unregister_groups
      ::Flipper.unregister_groups
    end

    def self.preview_features
      ::Flipper.group(:preview_features)
    end
  end
end
