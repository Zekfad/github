# rubocop:disable Style/FrozenStringLiteralComment

require "set"

module Flipper
  module Adapters
    class Mysql
      include Flipper::Adapter

      ACTOR_GATE_KEY = Flipper::Gates::Actor.new.key

      attr_reader :name

      def initialize(exclude_actors_for: [])
        @name = :mysql
        @exclude_actors_for = exclude_actors_for
      end

      # Public: return the set of known features.
      def features
        ActiveRecord::Base.connected_to(role: :reading) do
          rows = ApplicationRecord::Domain::Features.github_sql.results <<-SQL
            SELECT name FROM flipper_features
          SQL
          rows.flatten.to_set
        end
      end

      # Public: Adds a feature to the set of known features.
      #
      # Returns true
      def add(feature)
        feature_id(feature)
        true
      end

      # Public: Removes a feature from the set of known features and clears
      # all the values for the feature.
      #
      # Returns true
      def remove(feature)
        ::FlipperFeature.transaction do
          clear(feature)
          binds = {
            name: feature.key.to_s,
          }
          ApplicationRecord::Domain::Features.github_sql.run <<-SQL, binds
            DELETE FROM flipper_features WHERE name = :name
          SQL
        end
        true
      end

      # Public: Clears all the gate values for a feature.
      #
      # Returns true
      def clear(feature)
        binds = {
          name: feature.key.to_s,
        }
        ApplicationRecord::Domain::Features.github_sql.run <<-SQL, binds
          DELETE flipper_gates
          FROM flipper_gates
          INNER JOIN flipper_features ON flipper_features.id = flipper_gates.flipper_feature_id
          WHERE flipper_features.name = :name
        SQL
        true
      end

      # Public: Gets the values for all gates for a given feature.
      #
      # feature - a Flipper::Feature
      #
      # Returns a Hash of Flipper::Gate#key => value.
      def get(feature)
        ActiveRecord::Base.connected_to(role: :reading) do
          binds = {
            name: feature.key.to_s,
          }
          db_gates = ApplicationRecord::Domain::Features.github_sql.new <<-SQL, binds
            SELECT
              flipper_gates.name AS name,
              flipper_gates.value AS value
            FROM flipper_gates
            INNER JOIN flipper_features ON flipper_features.id = flipper_gates.flipper_feature_id
            WHERE flipper_features.name = :name
          SQL
          if exclude_actors?(feature)
            db_gates.add <<-SQL, actor: ACTOR_GATE_KEY
              AND flipper_gates.name <> :actor
            SQL
          end

          result_for_feature(feature, db_gates.results)
        end
      end

      # Public: Gets the values for all gates for provided features.
      #
      # features - an Array of Flipper::Feature's
      #
      # Returns a Hash of Hashes (gate key => gate value(s)).
      def get_multi(features)
        ActiveRecord::Base.connected_to(role: :reading) do
          binds = {
            names: features.map(&:key),
          }
          db_gates = ApplicationRecord::Domain::Features.github_sql.new <<-SQL, binds
            SELECT
              flipper_gates.name AS name,
              flipper_gates.value AS value,
              flipper_features.name AS feature_name
            FROM flipper_gates
            INNER JOIN flipper_features ON flipper_features.id = flipper_gates.flipper_feature_id
            WHERE flipper_features.name IN :names
          SQL
          if features.any? { |feature| exclude_actors?(feature) }
            db_gates.add <<-SQL, exclude_actors_for: @exclude_actors_for, actor: ACTOR_GATE_KEY
              AND (flipper_features.name NOT IN :exclude_actors_for OR flipper_gates.name <> :actor)
            SQL
          end
          grouped_db_gates = db_gates.results.group_by { |_, _, feature_name| feature_name }
          result = {}
          features.each do |feature|
            result[feature.key] = result_for_feature(feature, grouped_db_gates[feature.key])
          end
          result
        end
      end

      # Public: Gets the values for all gates for all features.
      #
      # features - an Array of Flipper::Feature's
      #
      # Returns a Hash of Hashes (gate key => gate value(s)).
      def get_all
        ActiveRecord::Base.connected_to(role: :reading) do
          db_gates = ApplicationRecord::Domain::Features.github_sql.new <<-SQL
            SELECT
              flipper_gates.name AS name,
              flipper_gates.value AS value,
              flipper_features.name AS feature_name
            FROM flipper_features
            LEFT JOIN flipper_gates ON flipper_gates.flipper_feature_id = flipper_features.id
          SQL
          unless @exclude_actors_for.empty?
            db_gates.add <<-SQL, exclude_actors_for: @exclude_actors_for, actor: ACTOR_GATE_KEY
              AND (flipper_features.name NOT IN :exclude_actors_for OR flipper_gates.name <> :actor)
            SQL
          end
          grouped_db_gates = db_gates.results.group_by { |_, _, feature_name| feature_name }
          result = Hash.new { |hash, key|
            hash[key] = result_for_feature(Flipper::Feature.new(key, self), [])
          }
          grouped_db_gates.each_key do |key|
            feature = Flipper::Feature.new(key, self)
            result[feature.key] = result_for_feature(feature, grouped_db_gates[feature.key])
          end
          result
        end
      end

      # Public: enable a gate for a thing
      #
      # feature - The Flipper::Feature for the gate.
      # gate - The Flipper::Gate to disable.
      # thing - The Flipper::Type being disabled for the gate.
      #
      # Returns true
      def enable(feature, gate, thing)
        feature_id = feature_id(feature)
        binds = {
          feature_id: feature_id,
          name: gate.key.to_s,
          value: thing.value.to_s,
        }
        ::FlipperFeature.transaction do
          case gate.data_type
          when :boolean
            clear(feature)
          when :integer
            ApplicationRecord::Domain::Features.github_sql.run(<<-SQL, binds)
              DELETE FROM flipper_gates
              WHERE
                flipper_gates.flipper_feature_id = :feature_id AND
                flipper_gates.name = :name
            SQL
          when :set
          else
            unsupported_data_type gate.data_type
          end

          ApplicationRecord::Domain::Features.github_sql.run(<<-SQL, binds)
            INSERT INTO
              flipper_gates (flipper_feature_id, name, value, created_at, updated_at)
            VALUES
              (:feature_id, :name, :value, NOW(), NOW())
            ON DUPLICATE KEY UPDATE updated_at = NOW()
          SQL
        end

        true
      end

      # Public: Disables a gate for a given thing.
      #
      # feature - The Flipper::Feature for the gate.
      # gate - The Flipper::Gate to disable.
      # thing - The Flipper::Type being disabled for the gate.
      #
      # Returns true.
      def disable(feature, gate, thing)
        feature_id = feature_id(feature)
        binds = {
          feature_id: feature_id,
          name: gate.key.to_s,
          value: thing.value.to_s,
        }
        ::FlipperFeature.transaction do
          case gate.data_type
          when :boolean
            clear(feature)
          when :integer
            ApplicationRecord::Domain::Features.github_sql.run(<<-SQL, binds)
              DELETE FROM flipper_gates
              WHERE
                flipper_gates.flipper_feature_id = :feature_id AND
                flipper_gates.name = :name
            SQL

            ApplicationRecord::Domain::Features.github_sql.run(<<-SQL, binds)
              INSERT INTO
                flipper_gates (flipper_feature_id, name, value, created_at, updated_at)
              VALUES
                (:feature_id, :name, :value, NOW(), NOW())
              ON DUPLICATE KEY UPDATE updated_at = NOW()
            SQL
          when :set
            ApplicationRecord::Domain::Features.github_sql.run(<<-SQL, binds)
              DELETE FROM flipper_gates
              WHERE
                flipper_gates.flipper_feature_id = :feature_id AND
                flipper_gates.name = :name AND
                flipper_gates.value = :value
            SQL
          else
            unsupported_data_type gate.data_type
          end
        end

        true
      end

      # Public. Look up a feature value for the given actor.
      def feature_enabled?(feature_key, actor_id)
        ActiveRecord::Base.connected_to(role: :reading) do
          binds = {
            feature_name: feature_key,
            gate_name: "actors",
            value: actor_id,
          }
          res = ApplicationRecord::Domain::Features.github_sql.run(<<-SQL, binds)
            SELECT 1
            FROM flipper_gates
            INNER JOIN flipper_features ON flipper_features.id = flipper_gates.flipper_feature_id
            WHERE
              flipper_features.name = :feature_name AND
              flipper_gates.name = :gate_name AND
              flipper_gates.value = :value
          SQL
          res.value?
        end
      end

      def actors_value(feature_key)
        ActiveRecord::Base.connected_to(role: :reading) do
          binds = {
            name: feature_key.to_s,
            actor: ACTOR_GATE_KEY,
          }
          db_gates = ApplicationRecord::Domain::Features.github_sql.new <<-SQL, binds
            SELECT flipper_gates.value AS value
            FROM flipper_gates
            INNER JOIN flipper_features ON flipper_features.id = flipper_gates.flipper_feature_id
            WHERE flipper_features.name = :name
              AND flipper_gates.name = :actor
          SQL
          db_gates.values.flatten
        end
      end

      private

      # Private: Find a persisted feature by the "feature.key", and return the
      # id. If the feature does not exist, create it.
      #
      # Returns the Integer id
      def feature_id(feature)
        feature_id = ApplicationRecord::Domain::Features.github_sql.value <<-SQL, name: feature.key.to_s
          SELECT id FROM flipper_features WHERE name = :name LIMIT 1
        SQL

        return feature_id unless feature_id.nil?

        result = ApplicationRecord::Domain::Features.github_sql.run(<<-SQL, name: feature.key.to_s)
          INSERT INTO flipper_features (name, created_at, updated_at)
          VALUES (:name, NOW(), NOW())
          ON DUPLICATE KEY UPDATE updated_at = NOW()
        SQL
        result.last_insert_id
      end

      # Private: Convert gate rows from the database to the flipper gate hash.
      #
      # Returns Hash of gate key => gate value.
      def result_for_feature(feature, db_gates)
        db_gates ||= []
        db_gates_by_name = db_gates.group_by { |row| row[0] }

        result = {}
        feature.gates.each do |gate|
          result[gate.key] =
            case gate.data_type
            when :boolean, :integer
              rows = db_gates_by_name[gate.key.to_s]
              if rows && rows[0]
                rows[0][1]
              end
            when :set
              rows = db_gates_by_name[gate.key.to_s] || []
              values = rows.map { |row| row[1] }
              values.to_set
            else
              unsupported_data_type gate.data_type
            end
        end
        result
      end

      # Private
      def unsupported_data_type(data_type)
        raise "#{data_type} is not supported by this adapter"
      end

      # Private
      def exclude_actors?(feature)
        @exclude_actors_for.include?(feature.key)
      end
    end
  end
end
