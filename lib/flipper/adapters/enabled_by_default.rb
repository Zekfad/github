# frozen_string_literal: true

module Flipper
  module Adapters
    # Adapter decorator that makes features default to enabled if not otherwise
    # specified.
    class EnabledByDefault
      include ::Flipper::Adapter

      attr_reader :name

      def initialize(adapter)
        @adapter = adapter
        @name = :enabled_by_default
        @blacklist = Set.new
      end

      # Public: Prevent a feature from being enabled by default by adding it to
      # the blacklist.
      #
      # feature_name - The String or Symbol name of the feature to blacklist
      #
      # Returns nothing.
      def blacklist(feature_name)
        @blacklist << feature_name.to_s
      end

      # Internal: Standard Adapter#get method, with features enabled by
      # default.
      def get(feature)
        if !@blacklist.include?(feature.key) && !features.include?(feature.key)
          ActiveRecord::Base.connected_to(role: :writing) do
            add(feature)
            gate = feature.gate(:boolean)
            enable(feature, gate, gate.wrap(Flipper::Types::Boolean.new(true)))
          end
        end
        @adapter.get(feature)
      end

      # Public: The set of known features.
      def features
        @adapter.features
      end

      # Public: Adds a feature to the set of known features.
      def add(feature)
        @adapter.add(feature)
      end

      # Public: Removes a feature from the set of known features and clears
      # all the values for the feature.
      def remove(feature)
        @adapter.remove(feature)
      end

      # Public: Clears all the gate values for a feature.
      def clear(feature)
        @adapter.clear(feature)
      end

      # Public
      def enable(feature, gate, thing)
        @adapter.enable(feature, gate, thing)
      end

      # Public
      def disable(feature, gate, thing)
        @adapter.disable(feature, gate, thing)
      end

      # Public
      def feature_enabled?(feature_key, actor_id)
        @adapter.feature_enabled?(feature_key, actor_id)
      end

      # Public
      def actors_value(feature_key)
        @adapter.actors_value(feature_key)
      end
    end
  end
end
