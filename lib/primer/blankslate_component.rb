# frozen_string_literal: true

module Primer
  class BlankslateComponent < Primer::Component

    def initialize(
      # required
      title:,

      # optional
      icon: "",
      image_src: "",
      image_alt: " ",
      description: "",
      button_text: "",
      button_url: "",
      link_text: "",
      link_url: "",

      **kwargs
    )
      @kwargs = kwargs
      @kwargs[:tag] = :div
      @kwargs[:classes] = class_names(
        @kwargs[:classes],
        "blankslate blankslate-narrow",
      )

      @icon = icon
      @image_src = image_src
      @image_alt = image_alt
      @title = title
      @description = description
      @button_text = button_text
      @button_url = button_url
      @link_text = link_text
      @link_url = link_url
    end

  end
end
