Stratocaster is the service that powers the event feeds in GitHub.

Please refer to [the event feeds documentation](../../docs/event-feeds.md) for a technical and functional description of Stratocaster.
