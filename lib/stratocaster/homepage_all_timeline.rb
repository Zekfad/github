# frozen_string_literal: true

module Stratocaster
  class HomepageAllTimeline < Stratocaster::Timeline

    attr_reader :homepage_public_timeline

    # Create an instance of the timeline.
    #
    # timeline - String key representing an homepage_all_timeline
    # viewer   - User instance viewing the timeline
    def initialize(homepage_all_timeline, viewer = nil)
      @homepage_public_timeline  = TimelineTypes.convert_homepage_all_to_homepage_public(homepage_all_timeline)
      super(homepage_all_timeline, viewer)
    end

    def visible_events
      return @visible_events if defined?(@visible_events)

      event_ids = homepage_private_ids + homepage_public_ids
      sorted_event_ids = event_ids.uniq.sort.reverse.first(::Stratocaster::DEFAULT_INDEX_SIZE)

      unfiltered_events = stratocaster_response { GitHub.stratocaster.ids_to_events(sorted_event_ids) }.items
      GitHub::PrefillAssociations.for_stratocaster_event_senders(unfiltered_events)
      GitHub::PrefillAssociations.for_stratocaster_event_repos(unfiltered_events)
      filtered_events = filtered_events(unfiltered_events, filters: filters)

      @visible_events = filtered_events
    end

    private

    def homepage_private_ids
      @homepage_private_ids ||= stratocaster_response { GitHub.stratocaster.ids(homepage_private_timeline) }.items
    end

    def homepage_public_ids
      @homepage_public_ids ||= stratocaster_response { GitHub.stratocaster.ids(homepage_public_timeline) }.items
    end

    def homepage_private_timeline
      timeline
    end

    def filters
      if GitHub.enterprise?
        basic_filters
      else
        filters = basic_filters
        filters.delete(Filters::LabeledEventsFilter)
        filters.delete(Filters::FollowEventsFilter)
        filters.delete(Filters::SponsorEventsFilter)
        filters << Filters::WorkflowEventsFilter
        filters << Filters::DeletedReposFilter
        filters << Filters::DeletedActorsFilter
        filters
      end
    end
  end
end
