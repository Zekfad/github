# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/notifications/v0/entities/notification_handler.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_enum "hydro.schemas.github.notifications.v0.entities.NotificationHandler" do
    value :UNKNOWN, 0
    value :EMAIL, 1
    value :WEB, 2
    value :MOBILE_PUSH, 3
  end
end

module Hydro
  module Schemas
    module Github
      module Notifications
        module V0
          module Entities
            NotificationHandler = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.notifications.v0.entities.NotificationHandler").enummodule
          end
        end
      end
    end
  end
end
