# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/pull_request_multiline_select.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/pull_request_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.PullRequestMultilineSelect" do
    optional :actor, :message, 1, "hydro.schemas.github.v1.entities.User"
    optional :repository, :message, 2, "hydro.schemas.github.v1.entities.Repository"
    optional :pull_request, :message, 3, "hydro.schemas.github.v1.entities.PullRequest"
    optional :starting_diff_position, :string, 4
    optional :ending_diff_position, :string, 5
    optional :line_count, :uint32, 6
    optional :diff_type, :enum, 7, "hydro.schemas.github.v1.PullRequestMultilineSelect.DiffType"
    optional :whitespace_ignored, :bool, 8
  end
  add_enum "hydro.schemas.github.v1.PullRequestMultilineSelect.DiffType" do
    value :UNIFIED, 0
    value :SPLIT, 1
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        PullRequestMultilineSelect = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.PullRequestMultilineSelect").msgclass
        PullRequestMultilineSelect::DiffType = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.PullRequestMultilineSelect.DiffType").enummodule
      end
    end
  end
end
