# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/entities/integration.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.entities.Integration" do
    optional :id, :uint32, 1
    optional :name, :string, 2
    optional :domain, :string, 3
    optional :url, :string, 4
    optional :callback_url, :string, 5
    optional :public, :bool, 6
    optional :created_at, :message, 7, "google.protobuf.Timestamp"
    optional :owner_id, :uint32, 8
    optional :bot_id, :uint32, 9
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        module Entities
          Integration = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.Integration").msgclass
        end
      end
    end
  end
end
