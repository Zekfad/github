# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/entities/repository_page.proto

require 'google/protobuf'

require 'google/protobuf/wrappers_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.entities.RepositoryPage" do
    optional :id, :uint64, 1
    optional :repository, :message, 2, "hydro.schemas.github.v1.entities.Repository"
    optional :cname, :message, 3, "google.protobuf.StringValue"
    optional :has_custom_404, :message, 4, "google.protobuf.BoolValue"
    optional :status, :enum, 5, "hydro.schemas.github.v1.entities.RepositoryPage.PageBuildStatus"
    optional :built_revision, :message, 6, "google.protobuf.StringValue"
    optional :source_ref_name, :message, 7, "google.protobuf.StringValue"
    optional :source_subdir, :message, 8, "google.protobuf.StringValue"
    optional :https_redirect, :message, 9, "google.protobuf.BoolValue"
    optional :hsts_max_age, :uint64, 10
    optional :hsts_include_sub_domains, :message, 11, "google.protobuf.BoolValue"
    optional :hsts_preload, :message, 12, "google.protobuf.BoolValue"
  end
  add_enum "hydro.schemas.github.v1.entities.RepositoryPage.PageBuildStatus" do
    value :BUILD_STATUS_UNKNOWN, 0
    value :BUILT, 1
    value :BUILDING, 2
    value :ERRORED, 3
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        module Entities
          RepositoryPage = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.RepositoryPage").msgclass
          RepositoryPage::PageBuildStatus = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.RepositoryPage.PageBuildStatus").enummodule
        end
      end
    end
  end
end
