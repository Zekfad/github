# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/entities/feature.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.entities.Feature" do
    optional :id, :uint32, 1
    optional :public_name, :string, 2
    optional :slug, :string, 3
    optional :flipper_feature_id, :uint32, 4
    optional :enrolled_by_default, :bool, 5
    optional :created_at, :message, 6, "google.protobuf.Timestamp"
    optional :published_at, :message, 7, "google.protobuf.Timestamp"
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        module Entities
          Feature = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.Feature").msgclass
        end
      end
    end
  end
end
