# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/entities/business.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.entities.Business" do
    optional :id, :uint64, 1
    optional :slug, :string, 2
    optional :global_relay_id, :string, 3
    optional :created_at, :message, 4, "google.protobuf.Timestamp"
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        module Entities
          Business = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.Business").msgclass
        end
      end
    end
  end
end
