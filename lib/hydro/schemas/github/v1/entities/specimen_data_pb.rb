# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/entities/specimen_data.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.entities.SpecimenData" do
    optional :raw_data, :string, 1
    optional :clean_text, :string, 2
    optional :has_repeated_links, :bool, 3
    optional :nothing_but_image_links, :bool, 4
    optional :uses_spacing_tricks, :bool, 5
    optional :clean_data, :string, 6
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        module Entities
          SpecimenData = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.SpecimenData").msgclass
        end
      end
    end
  end
end
