# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/repository_pages_visibility_change.proto

require 'google/protobuf'

require 'google/protobuf/wrappers_pb'
require 'hydro/schemas/github/v1/entities/repository_page_pb'
require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.RepositoryPagesVisibilityChange" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :public, :message, 2, "google.protobuf.BoolValue"
    optional :page, :message, 3, "hydro.schemas.github.v1.entities.RepositoryPage"
    optional :actor, :message, 4, "hydro.schemas.github.v1.entities.User"
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        RepositoryPagesVisibilityChange = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.RepositoryPagesVisibilityChange").msgclass
      end
    end
  end
end
