# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/repository_image_destroy.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/repository_image_pb'
require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.RepositoryImageDestroy" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :destroyed_repository_image, :message, 3, "hydro.schemas.github.v1.entities.RepositoryImage"
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        RepositoryImageDestroy = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.RepositoryImageDestroy").msgclass
      end
    end
  end
end
