# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/pull_request_local_checkout.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.PullRequestLocalCheckout" do
    optional :actor, :message, 1, "hydro.schemas.github.v1.entities.User"
    optional :action, :enum, 2, "hydro.schemas.github.v1.PullRequestLocalCheckout.Action"
    optional :client_type, :enum, 3, "hydro.schemas.github.v1.PullRequestLocalCheckout.ClientType"
  end
  add_enum "hydro.schemas.github.v1.PullRequestLocalCheckout.Action" do
    value :EXPAND, 0
    value :SELECT, 1
    value :COPY, 2
    value :OPEN, 3
  end
  add_enum "hydro.schemas.github.v1.PullRequestLocalCheckout.ClientType" do
    value :ANY, 0
    value :GIT, 1
    value :DESKTOP, 2
    value :CLI, 3
    value :CODESPACES, 4
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        PullRequestLocalCheckout = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.PullRequestLocalCheckout").msgclass
        PullRequestLocalCheckout::Action = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.PullRequestLocalCheckout.Action").enummodule
        PullRequestLocalCheckout::ClientType = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.PullRequestLocalCheckout.ClientType").enummodule
      end
    end
  end
end
