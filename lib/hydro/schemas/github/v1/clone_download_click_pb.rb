# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/clone_download_click.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/gist_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.CloneDownloadClick" do
    optional :actor, :message, 1, "hydro.schemas.github.v1.entities.User"
    optional :feature_clicked, :enum, 2, "hydro.schemas.github.v1.CloneDownloadClick.FeatureClicked"
    optional :repository, :message, 3, "hydro.schemas.github.v1.entities.Repository"
    optional :owner, :message, 4, "hydro.schemas.github.v1.entities.User"
    optional :request_context, :message, 5, "hydro.schemas.github.v1.entities.RequestContext"
    optional :gist, :message, 6, "hydro.schemas.github.v1.entities.Gist"
    optional :git_repository_type, :enum, 7, "hydro.schemas.github.v1.CloneDownloadClick.GitRepositoryType"
  end
  add_enum "hydro.schemas.github.v1.CloneDownloadClick.FeatureClicked" do
    value :UNKNOWN, 0
    value :OPEN_IN_DESKTOP, 1
    value :OPEN_IN_VISUAL_STUDIO, 2
    value :OPEN_IN_XCODE, 3
    value :DOWNLOAD_ZIP, 4
    value :COPY_URL, 5
    value :USE_SSH, 6
    value :USE_HTTPS, 7
    value :SHARE, 8
    value :EMBED, 9
  end
  add_enum "hydro.schemas.github.v1.CloneDownloadClick.GitRepositoryType" do
    value :REPOSITORY, 0
    value :GIST, 1
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        CloneDownloadClick = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.CloneDownloadClick").msgclass
        CloneDownloadClick::FeatureClicked = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.CloneDownloadClick.FeatureClicked").enummodule
        CloneDownloadClick::GitRepositoryType = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.CloneDownloadClick.GitRepositoryType").enummodule
      end
    end
  end
end
