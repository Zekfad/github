# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/related_issues_meta_at_issue_creation.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/issue_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.RelatedIssuesMetaAtIssueCreation" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :repository, :message, 3, "hydro.schemas.github.v1.entities.Repository"
    optional :issue, :message, 4, "hydro.schemas.github.v1.entities.Issue"
    optional :number_of_related_issues, :uint32, 5
    optional :title_length, :uint32, 6
    optional :menu_hidden, :bool, 7
    optional :session_key, :string, 8
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        RelatedIssuesMetaAtIssueCreation = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.RelatedIssuesMetaAtIssueCreation").msgclass
      end
    end
  end
end
