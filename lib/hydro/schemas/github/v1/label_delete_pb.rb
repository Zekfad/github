# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/label_delete.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/label_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.LabelDelete" do
    optional :actor, :message, 1, "hydro.schemas.github.v1.entities.User"
    optional :label, :message, 2, "hydro.schemas.github.v1.entities.Label"
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        LabelDelete = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.LabelDelete").msgclass
      end
    end
  end
end
