# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/profile_readme_action.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/specimen_data_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.ProfileReadmeAction" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :occurred_at, :message, 2, "google.protobuf.Timestamp"
    optional :change_type, :enum, 3, "hydro.schemas.github.v1.ProfileReadmeAction.ChangeType"
    optional :actor, :message, 4, "hydro.schemas.github.v1.entities.User"
    optional :repository, :message, 5, "hydro.schemas.github.v1.entities.Repository"
    optional :owner, :message, 6, "hydro.schemas.github.v1.entities.User"
    optional :readme_body, :message, 7, "hydro.schemas.github.v1.entities.SpecimenData"
  end
  add_enum "hydro.schemas.github.v1.ProfileReadmeAction.ChangeType" do
    value :CHANGE_TYPE_UNKNOWN, 0
    value :CREATE, 1
    value :UPDATE, 2
    value :DELETE, 3
    value :REPO_VISIBILITY_CHANGED_TO_PUBLIC, 4
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        ProfileReadmeAction = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.ProfileReadmeAction").msgclass
        ProfileReadmeAction::ChangeType = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.ProfileReadmeAction.ChangeType").enummodule
      end
    end
  end
end
