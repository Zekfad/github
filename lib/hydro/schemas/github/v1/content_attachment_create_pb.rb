# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/content_attachment_create.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/integration_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.ContentAttachmentCreate" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :integration, :message, 3, "hydro.schemas.github.v1.entities.Integration"
    optional :content_context, :enum, 4, "hydro.schemas.github.v1.ContentAttachmentCreate.ContentContext"
    optional :content_id, :uint64, 5
    optional :content_title, :string, 6
    optional :repository, :message, 7, "hydro.schemas.github.v1.entities.Repository"
    optional :content_reference_url, :string, 8
    optional :content_body, :string, 9
  end
  add_enum "hydro.schemas.github.v1.ContentAttachmentCreate.ContentContext" do
    value :CONTENT_CONTEXT_UNKNOWN, 0
    value :ISSUE_BODY, 1
    value :ISSUE_COMMENT, 2
    value :PR_BODY, 3
    value :PR_COMMENT, 4
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        ContentAttachmentCreate = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.ContentAttachmentCreate").msgclass
        ContentAttachmentCreate::ContentContext = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.ContentAttachmentCreate.ContentContext").enummodule
      end
    end
  end
end
