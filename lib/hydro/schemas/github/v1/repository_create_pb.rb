# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/repository_create.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.RepositoryCreate" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :repository, :message, 3, "hydro.schemas.github.v1.entities.Repository"
    optional :init_with_readme, :bool, 4
    optional :license_template, :string, 5
    optional :gitignore_template, :string, 6
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        RepositoryCreate = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.RepositoryCreate").msgclass
      end
    end
  end
end
