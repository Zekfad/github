# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/eventer/v0/counter_update.proto

require 'google/protobuf'

require 'hydro/schemas/github/eventer/v0/entities/increment_counter_pb'
require 'hydro/schemas/github/eventer/v0/entities/set_counter_pb'
require 'hydro/schemas/github/eventer/v0/entities/delete_counters_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.eventer.v0.CounterUpdate" do
    oneof :action do
      optional :increment_counter, :message, 1, "hydro.schemas.github.eventer.v0.entities.IncrementCounter"
      optional :set_counter, :message, 2, "hydro.schemas.github.eventer.v0.entities.SetCounter"
      optional :delete_counters, :message, 3, "hydro.schemas.github.eventer.v0.entities.DeleteCounters"
    end
  end
end

module Hydro
  module Schemas
    module Github
      module Eventer
        module V0
          CounterUpdate = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.eventer.v0.CounterUpdate").msgclass
        end
      end
    end
  end
end
