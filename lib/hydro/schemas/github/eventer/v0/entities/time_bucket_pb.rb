# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/eventer/v0/entities/time_bucket.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.eventer.v0.entities.TimeBucket" do
    optional :start, :message, 1, "google.protobuf.Timestamp"
    optional :size, :enum, 2, "hydro.schemas.github.eventer.v0.entities.TimeBucket.TimeBucketSize"
  end
  add_enum "hydro.schemas.github.eventer.v0.entities.TimeBucket.TimeBucketSize" do
    value :UNKNOWN, 0
    value :HOUR, 1
    value :DAY, 2
  end
end

module Hydro
  module Schemas
    module Github
      module Eventer
        module V0
          module Entities
            TimeBucket = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.eventer.v0.entities.TimeBucket").msgclass
            TimeBucket::TimeBucketSize = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.eventer.v0.entities.TimeBucket.TimeBucketSize").enummodule
          end
        end
      end
    end
  end
end
