# Intentionally left empty as a no-op for `require 'hydro/schemas/hydro/v1/envelope_pb'`.
#
# The hydro-client-ruby gem should take care of loading `Hydro::Schemas::Hydro::V1::Envelope`,
# and we always call `require "hydro"` before loading schemas from lib/hydro/schemas.
