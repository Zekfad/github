# frozen_string_literal: true

module PagesCheckReRun
  class Service
    CHECK_FAILURE_TITLE = "GitHub Pages failed to build your site."

    def self.areas_of_responsibility
      [:pages]
    end

    def self.run(check_run_id, actor_id)
      return false unless GitHub.pages_github_app.present?
      check_run = CheckRun.find_by(id: check_run_id)
      actor = User.find_by(id: actor_id)

      return false unless check_run && actor
      check_suite = check_run.check_suite
      integration_id = check_suite.github_app.id

      return false unless integration_id == GitHub.pages_github_app.id
      Failbot.push(
        actor_id: actor_id,
        check_run_id: check_run_id,
        integration_id: integration_id,
      )
      check_suite.repository.rebuild_pages(actor, check_suite.head_sha)

      GitHub.dogstats.increment("pages.github_check_run.re_run", tags: ["status:success"])
      true
    rescue Page::PageBuildFailed => error
      Failbot.report(error)
      create_failed_check_run(check_suite, actor_id, error.message)
      GitHub.dogstats.increment("pages.github_check_run.re_run", tags: ["status:failed"])
      false
    end

    def self.create_failed_check_run(check_suite, actor_id, error)
      check_suite.check_runs.create!(
        creator_id: actor_id,
        name: GitHub.pages_check_run_name,
        status: :completed,
        conclusion: :failure,
        started_at: Time.now.utc,
        completed_at: Time.now.utc,
        details_url: GitHub.pages_help_url,
        title: CHECK_FAILURE_TITLE,
        summary: error,
      )
      true
    rescue ActiveRecord::RecordInvalid => error
      Failbot.report(error)
      false
    end
  end
end
