# rubocop:disable Style/FrozenStringLiteralComment

require "github/redis/mutex_group"

module Elastomer

  # This superclass provides uniform functionality for all the search index
  # repair jobs as well as some nify class-level helper methods to make
  # writing repair jobs super simple.
  #
  #   class RepairUsersIndex < ::Elastomer::Repair
  #     reconcile 'user',
  #       :fields => %w[updated_at],
  #       :limit  => 250,
  #       :reject => :spammy?
  #   end
  #
  # And to queue up one of these repair jobs, you give it the name of an
  # already existing index.
  #
  #   job = RepairUsersIndex.new 'users-3'
  #   job.enable.start(8)
  #
  # And now User models will be processed in batches of 250 at a time, and the
  # 'users-3' search index will be reconciled with the database. Eight workers
  # will be used in order to process the work in parallel.
  #
  # Issues and milestones live in the same search index, so we can reconcile
  # multiple ActiveRecord models in the same repair job.
  #
  #   class RepairIssuesIndex < ::Elastomer::Repair
  #     reconcile 'issue',
  #       :fields     => %w[updated_at state],
  #       :conditions => 'pull_request_id IS NULL',
  #       :include    => [:repository, :comments],
  #       :limit      => 100,
  #       :reject     => :spammy?
  #
  #     reconcile 'milestone'
  #       :fields => %w[updated_at state],
  #       :limit  => 100,
  #       :reject => :spammy?
  #   end
  #
  class Repair < ::GitHub::Jobs::Job
    include GitHub::ServiceMapping

    ENABLED_KEY = "enabled".freeze
    ELAPSED_KEY = "elapsed".freeze
    STARTED_KEY = "started".freeze

    attr_reader :index_name
    attr_reader :cluster_name
    attr_reader :group_key
    attr_reader :reconcilers

    # Create a new repair job.
    #
    # name - The name of the index being repaired.
    # opts - Options Hash
    #        'cluster' - cluster name
    #
    def initialize(name, opts = {})
      @job_args     = [name, opts]
      @index_name   = name
      @cluster_name = opts["cluster"] || opts[:cluster] || Elastomer.router.cluster_for_index(index_name)
      @group_key    = "#{self.class.name}/#{index_name}"

      @reconcilers = self.class.reconcilers.map do |hash|
        Reconciler.new(hash.merge(
          index: index,
          group_key: group_key,
          redis: redis,
        ))
      end
    end

    # Start up one or more background jobs to perform the repair task.
    #
    # count - The number of background jobs to start.
    #
    # Returns `true` if this is the first call to start for this job.
    def start(count = 1)
      count.times { requeue }
      redis.hsetnx(group_key, STARTED_KEY, Time.now.iso8601)
      GitHub.dogstats.count("search.repair.workers", count, { tags: ["index:#{index_name}"] })
    end

    # Perform the actual work of reconciling the search index with the canoncial
    # data source. This method will check the `enabled` flag and only perform
    # work if the flag is true. This method will enqueue another repair job into
    # resque if there are more iterations to perform.
    #
    # Returns this repair job instance.
    def perform
      return if disabled?
      repair!
    ensure
      requeue if enabled? && !finished?
    end

    # Perform the actual work of the repairing the search index. This method
    # will execute without checking the `enabled` flag. Only one iteration of
    # the repair job is performed.
    #
    # Use the `perform` method if you want to automatically enqueue another
    # repair job when this one completes. The `perform` method adheres to the
    # `enabled` semantics and will not perform work if the repair job has been
    # disabled.
    #
    # Returns this repair job instance.
    def repair!
      start = Time.now

      ActiveRecord::Base.connected_to(role: :reading) do
        reconcilers.each { |r| r.reconcile unless r.finished? }
      end

      elapsed = Time.now - start
      redis.hincrbyfloat(group_key, ELAPSED_KEY, elapsed)

      GitHub.dogstats.timing("search.repair.elapsed", (elapsed * 1000).round, { tags: tags })

      self
    rescue StandardError => boom
      Failbot.report(boom.with_redacting!)
    end

    # Returns `true` if all the reconcilers report that they have finished
    # processing all models. Returns `false` if any reconciler has more models
    # to process. Returns `nil` if there are no reconcilers.
    def finished?
      keys = reconcilers.map { |r| r.finished_key }
      return if keys.empty?

      redis.hmget(group_key, *keys).all?
    end

    # Returns the progress through the repair job - a floating point number
    # between 0.0 and 100.0.
    def progress
      return 100.0 if finished?

      values = reconcilers.map { |r| r.progress }
      values.compact!
      values.min
    end

    # Estimate the time when the repair job will complete. If the job is already
    # finished, then the time the job finished is returend.
    #
    # Returns the estimated Time when the job will complete.
    def estimated_completion_time
      return stats[:finished] if finished?

      p = progress
      return nil unless p && p > 0

      started = stats[:started]
      estimated_duration = (Time.now - started) * (100.0 / p)
      started + estimated_duration
    end

    # Retrieve stats for this repair job. The stats are returned as a single
    # Hash with the following keys
    #
    #   :started  - the Time when the job was first started
    #   :finished - the Time when the job completed or `false` if it is still running
    #   :elapsed  - time in seconds spent processing by the worker(s)
    #   :total    - the number of AR models that have been checked
    #   :added    - the number of documents added to the search index
    #   :updated  - the number of documents updated in the search index
    #   :removed  - the number of documents removed from the search index
    #   :error    - the number of errors encountered while repairing the search index
    #
    # Returns the stats Hash.
    def stats
      keys = [STARTED_KEY, ELAPSED_KEY]
      reconcilers.each { |r| keys.concat(r.keys) }

      ary = redis.hmget(group_key, *keys)

      hash = {
        total: 0,
        added: 0,
        updated: 0,
        removed: 0,
        error: 0,
        finished: [],
      }
      hash[:started] = ary.shift
      hash[:started] = Time.parse(hash[:started]) unless hash[:started].nil?
      hash[:elapsed] = ary.shift.to_f

      ary.each_slice(6) do |total, added, updated, removed, error, finished|
        hash[:total]   += total.to_i
        hash[:added]   += added.to_i
        hash[:updated] += updated.to_i
        hash[:removed] += removed.to_i
        hash[:error]   += error.to_i
        hash[:finished] << (finished.nil? ? nil : Time.parse(finished))
      end

      hash[:finished] = hash[:finished].include?(nil) ? false : hash[:finished].max

      hash
    end

    # Clears out all job state from redis effectively stopping all workers and
    # losing all repair progress.
    #
    # Returns this repair job instance.
    def reset!
      redis.del(group_key)
      self
    end

    # Returns `true` if the repair job group key exists in Redis. This
    # information lets us know if a repair job is already in place for the
    # search index.
    def exists?
      redis.exists(group_key)
    end

    # Returns `true` if the repair job is enabled. When disabled, the
    # reconcilers will not perform any work. This is like pausing the repair
    # job.
    def enabled?
      1 == redis.hget(group_key, ENABLED_KEY).to_i
    end

    # Returns `true` if the repair job is disabled.
    def disabled?
      !enabled?
    end

    # Returns `true` if the repair job has been paused. The job exists but it
    # is not currently enabled.
    def paused?
      exists? && disabled?
    end

    # Returns `true` if the repair job is active. The job is enabled but it is
    # not yet finished.
    def active?
      enabled? && !finished?
    end

    # Enable the repair job.
    #
    # Returns this repair job instance.
    def enable
      redis.hset(group_key, ENABLED_KEY, 1)
      self
    end

    # Disable (or pause) the repair job.
    #
    # Returns this repair job instance.
    def disable
      redis.hset(group_key, ENABLED_KEY, 0)
      self
    end
    alias :pause :disable

    # Returns the Elastomer::Index instance that will be repaired. This index
    # is determined via the `index_name` and our Elastomer environment.
    def index
      return @index if defined? @index

      index_class = Elastomer.env.lookup_index(index_name)
      @index = index_class.new(index_name, cluster_name)
    end

    # Returns the number of workers that are actively processing this repair
    # job. This number does _not_ include workers that are queued.
    def working
      Resque.working.count do |worker|
        payload = worker.job["payload"] rescue nil
        payload && payload["class"] == self.class.name \
                && payload["args"].first == @index_name
      end
    end

    # Internal: Put another job on the Resque queue.
    #
    # Returns this job instance.
    def requeue
      self.class.active_job_class.perform_later(*@job_args)
      self
    end

    # Internal: Return the redis connection to use for requests.
    def redis
      GitHub.resque_redis
    end
    private :redis

    # The task of Reconciler is to iterate over ActiveRecord models and ensure
    # that their representations in the search index are up to date. Documents
    # will be added and removed from the search index; others will be updated.
    # Whatever it takes to make the search index look like the database.
    #
    # The up-to-dateness of a search document is determined by comparing
    # fields from the document with the corresponding attribute fields from
    # the ActiveRecord model. By default this is the `updated_at` field, but
    # any number of fields can be specified. The only restriction is that the
    # field be accessible via the AR `model['field-name']` method. So fields
    # and counts from join models will not work.
    #
    # The Reconciler uses a shared mutex to maintain state. Multiple threads
    # and processes can work on reconciling - even processes on separate
    # machines.
    class Reconciler
      #### Required (these must be supplied via the initializer)

      # The Elastomer::Index instance being reconciled against the DB.
      attr_reader :index

      # The type of data being reconciled. The AR model class is derived from
      # this value. It should be the document type as found in the :index.
      attr_reader :type

      # The Redis hash key where settings for this reconciler will be stored.
      attr_reader :group_key

      #### Defaults Provided

      # ElasticSearch document type (if different from the AR type)
      attr_reader :es_type

      # The array of attribute fields used to determine if a search record
      # needs to be updated (defaults to %w[updated_at]).
      attr_reader :fields

      # The number of AR models to retrieve per iteration (default is 100).
      attr_reader :limit

      # The Redis connection to use (default is GitHub.resque_redis).
      attr_reader :redis
      private :redis

      # ElasticSearch bulk request size in bytes (default is 500kb).
      attr_reader :request_size

      # Extra SQL conditions for restricting the selected AR models. Can pass a
      # String directly or use a Proc to build the string. The Proc is called
      # with one argument, the current reconciler.
      attr_reader :conditions

      # Extra SQL joins for restricting the selected AR models.
      attr_reader :joins

      # Eager load these models
      attr_reader :ar_includes

      # AR models will not be indexed if any of these methods return true.
      # Array of method symbols.
      attr_reader :reject

      # AR models will only be indexed if all of these methods return true.
      # Array of method symbols.
      attr_reader :accept

      #### Derived from the type

      # The AR model class to reconcile against.
      attr_reader :model_class

      # Can pass a Proc to build custom ActiveRecord query before fetching rows.
      attr_reader :model_scope

      # Various redis keys
      attr_reader :offset_key
      attr_reader :total_key
      attr_reader :add_key
      attr_reader :update_key
      attr_reader :remove_key
      attr_reader :error_key
      attr_reader :finished_key
      attr_reader :mutex_key

      # Create a new Reconciler that will reconcile the state of a single
      # AR model type in the database with the documents in the search index.
      # Specific fields will be compared to determine if the two
      # representations are out of sync.
      #
      # The database will not be changed by this process. Only the search
      # index will be updated.
      #
      # The reconcile progress is tracked via several Redis keys. Multiple
      # reconcilers can be run simultaneously, and they will coordinate work
      # via the Redis keys and a shared mutex.
      #
      # opts - Options Hash
      #        :index        - Elastomer::Index to reconcile
      #        :type         - AR document type to reconcile
      #        :model_scope  - Customize AR scope using a Proc (optional)
      #        :es_type      - ES document type (default to `:type`)
      #        :group_key    - redis hash key
      #        :fields       - fields to check for reconciliation
      #        :limit        - number of AR models to fetch
      #        :redis        - the redis connection to use
      #        :request_size - ElasticSearch bulk request size in bytes
      #        :conditions   - extra SQL conditions on the AR model
      #        :joins        - extra SQL joins to use on the AR model
      #        :include      - eager load these AR model assocations (Array of symbols)
      #        :accept       - AR model methods that must be true (Array of symbols)
      #        :reject       - AR model methods that must be false (Array of symbols)
      #
      def initialize(opts)
        @index = opts.fetch(:index)
        @type = opts.fetch(:type)
        @group_key = opts.fetch(:group_key)
        @model_class = opts[:model_class]
        if @model_class.nil?
          @model_class = type.tr("-", "_").classify.constantize
        end
        @model_scope = opts[:model_scope]
        @es_type = opts.fetch(:es_type, @type)

        @fields = opts.fetch(:fields, %w[updated_at])
        @limit = opts.fetch(:limit, 100)
        @redis = opts.fetch(:redis, GitHub.resque_redis)
        @request_size = opts.fetch(:request_size, 512.kilobytes)
        @conditions = opts.fetch(:conditions, nil)
        @joins = opts.fetch(:joins, nil)
        @ar_includes  = opts.fetch(:include, nil)

        @accept       = Array(opts[:accept])
        @reject       = Array(opts[:reject])

        @offset_key   = "#@type/offset"
        @total_key    = "#@type/total"
        @add_key      = "#@type/add"
        @update_key   = "#@type/update"
        @remove_key   = "#@type/remove"
        @error_key    = "#@type/error"
        @finished_key = "#@type/finished"
        @mutex_key    = "#@type/mutex"
      end

      # Returns the array of stats keys.
      def keys
        [total_key, add_key, update_key, remove_key, error_key, finished_key]
      end

      # Reset the reconciler by clearing all redis keys and removing the
      # mutex.
      #
      # Returns this reconciler.
      def reset!
        redis.synchronize {
          redis._client.call([:hdel, group_key, offset_key, *keys])
        }
        mutex.unlock!
        remove_instance_variable(:@models) if defined? @models
        self
      end

      # Returns `true` if there are no more models to operate on. Returns
      # `false` if there are more models.
      def finished?
        redis.hexists(group_key, finished_key)
      end

      # Internal: Set the finished timestamp in redis to the current time.
      def finish!
        redis.hsetnx(group_key, finished_key, Time.now.iso8601)
      end

      # Returns the progress through the reconciliation task - a floating
      # point number between 0.0 and 100.0. This is the ratio of the current
      # model ID offset versus the largest model ID.
      def progress
        last = last_id
        return 0.0 unless last > 0
        (get_offset.to_f / last.to_f) * 100.0
      end

      # Returns the last model ID from the database. This information is used
      # in determing the percent complete value for this reconciler.
      def last_id
        model_class.maximum(:id)
      end

      # Get the last model ID offset from redis.
      #
      # Returns an offset Integer.
      def get_offset
        redis.hget(group_key, offset_key).to_i
      end

      # Internal: Set the offset in redis. This should only be called while
      # guarded by a shared mutex. See the `models` method further down.
      #
      # Returns a redis success / error code.
      def set_offset(offset)
        redis.hset(group_key, offset_key, offset)
      end

      # Reconcile the state of the documents from the search index with what
      # is in the database. The steps for the reconciliation process are as
      # follows:
      #
      # * get the model reconcile fields from the database
      # * get the document reconcile fields from the search index
      # * compile a list of models to remove from the search index
      # * compile a list of models to update in the search index
      # * perform these operations in a bulk indexing step
      #
      # Returns the Array of model ids to update and remove.
      def reconcile
        return if models.nil?
        return finish! if models.empty?

        add, update, remove, metadata = generate_actions
        upsert = []

        unless add.empty?
          increment_stats(add_key, add.size)
          upsert.concat add
        end

        unless update.empty?
          increment_stats(update_key, update.size)
          upsert.concat update
        end

        increment_stats(remove_key, remove.size) unless remove.empty?
        increment_stats(total_key, models.size)

        update_search_index(upsert, remove, metadata)

        [add, update, remove]  # returning these for debugging purposes

      rescue StandardError, TimeoutError => boom
        Failbot.report(boom.with_redacting!)
      end

      # Internal: Perform the bulk indexing operations to bering the search
      # index in sync with the database records.
      #
      # upsert   - Array of model IDs to update / add
      # remove   - Array of model IDs to remove
      # metadata - Hash of metadata information for each Elasticsearch document
      #
      # Returns the result of the bulk indexing operation.
      def update_search_index(upsert, remove, metadata)
        models_hash = models.index_by(&:id)
        adapter_class = ::Elastomer.env.lookup_adapter(es_type)

        results = index.bulk(request_size: request_size) do |bulk|
          upsert.each do |id|
            begin
              model = models_hash[id] || id
              adapter = adapter_class.create(model)
              if doc = adapter.to_hash
                results = bulk.index(doc)
                check_for_errors(results)
              end
            rescue Elastomer::Client::Error => boom
              Failbot.report boom,
                bulk_action: :index,
                index: index.name,
                document_type: es_type,
                document_id: id
            end
          end

          remove.each do |id|
            begin
              routing = metadata[id] && metadata[id][:routing]
              results = bulk.delete(_id: id, _type: es_type, _routing: routing)
              check_for_errors(results)
            rescue Elastomer::Client::Error => boom
              Failbot.report boom,
                bulk_action: :delete,
                index: index.name,
                document_type: es_type,
                document_id: id
            end
          end
        end
        check_for_errors(results)
      end

      # Internal: Check for errors in the bulk indexing response and report
      # error metrics to our stats endpoint.
      #
      # Returns the `results` passed in to the method
      def check_for_errors(results)
        return results if results.nil? || results["errors"] == false

        errors = 0
        results["items"].each do |item|
          action = item.keys.first
          data   = item.values.first
          next unless data.has_key?("error")
          errors += 1
        end

        increment_stats(error_key, errors) if errors > 0
        results
      end

      # Internal: Helper method that will increment a stats counter in redis
      # identified.
      #
      # field - The redis field holding the stat
      # size  - The increment size
      #
      # Returns the new stat counter value.
      def increment_stats(field, size)
        redis.hincrby(group_key, field, size)

        key = field.split("/").last

        GitHub.dogstats.count("search.repair", size, { tags: ["key:#{key}"] })
      end

      # Internal: Given two hashes containing record IDs and their reconcile
      # fields, return three Arrays. The first Array contains all the model
      # IDs that need to be added to the search index. The second Array
      # contains all the model IDs that need to be updated in the search
      # index. The third Array contains all the model IDs that need to be
      # removed from the search index.
      #
      # Returns the Array of model IDs to add, update, remove and metadata about
      # existing Elasticsearch documents.
      def generate_actions
        db_hash = lookup_from_db
        es_hash = lookup_from_es

        db_keys = db_hash.keys
        es_keys = es_hash.keys

        add    = db_keys - es_keys
        remove = es_keys - db_keys

        update = []
        (db_keys & es_keys).each do |key|
          update << key if db_hash[key] != es_hash[key][:fields]
        end

        metadata = {}
        es_hash.each { |id, hash| metadata[id] = hash[:metadata] }

        [add, update, remove, metadata]
      end

      # Internal: Take all the model objects read from the database and return
      # a Hash of the IDs and the reconcile fields.
      #
      # Returns a Hash of ID / reconcile field pairs.
      def lookup_from_db
        return {} if models.empty?

        hash = Hash.new
        models.each do |model|
          next if     reject.any? { |method| model.send(method) }
          next unless accept.all? { |method| model.send(method) }

          ary = fields.map { |name| model[name] }
          hash[model.id] = (ary.length == 1 ? ary.first : ary)
        end
        hash
      end

      # Internal: Take the first and last model IDs from the models list and
      # query all the documents that exist in this range of IDs. Return the a
      # Hash containing the ID and corresponding reconcile fields as read from
      # the search index.
      #
      # Returns a Hash of ID / document metadata & reconcile field pairs.
      def lookup_from_es
        hash = Hash.new
        return hash if models.empty?

        id_range.each_slice(2_000) do |id_ary|
          query = {
            query: { ids: { type: es_type, values: id_ary }},
            _source: fields,
            size: id_ary.length,
          }

          results = index.search(query, {type: es_type})

          results["hits"]["hits"].each do |doc|
            id = doc["_id"].to_i
            source = doc["_source"]

            hash[id] = {
              fields: nil,
              metadata: {
                id:      id,
                type:    doc["_type"],
                index:   doc["_index"],
                routing: doc["_routing"],
              },
            }

            next if source.blank?

            ary = fields.map do |name|
              val = source[name]
              val = Time.parse(val) if val && name =~ /_at\Z/i
              val
            end
            hash[id][:fields] = (ary.length == 1 ? ary.first : ary)
          end
        end

        hash
      end

      # Internal: Read the next set of ActiveRecord models to operate on. This
      # will lock the redis mutex and update the offset when the models are
      # loaded.
      #
      # Returns the Array of ActiveRecord model instances.
      def models
        return @models if defined? @models
        @models = nil
        @id_range = nil

        @models = mutex.lock do
          offset = get_offset

          conds = "#{model_class.table_name}.id > #{model_class.connection.quote(offset)}"
          order = "#{model_class.table_name}.id ASC"
          if conditions.present?
            if conditions.respond_to?(:call)
              conds << " AND #{conditions.call(self)}"
            else
              conds << " AND #{conditions}"
            end
          end
          includes = ar_includes if ar_includes.present?

          scope = model_class
          if model_scope.respond_to?(:call)
            scope = model_scope.call(scope)
          end
          ary = scope.where(conds).limit(limit).order(order).joins(joins).preload(includes)
          unless ary.empty?
            set_offset(ary.last.id)
            @id_range = (offset+1)..(ary.last.id)
          end
          ary
        end
      rescue GitHub::Redis::Mutex::LockError
        GitHub.dogstats.increment("search.repair.lock_error", { tags: ["index:#{index.name}"] })
        nil
      end

      # Internal: The model ID range our ElasticSearch query should use.
      attr_reader :id_range

      # Internal: Return a shared redis mutex to coordinate access to the
      # shared Repository timestamp.
      #
      # Returns a GitHub::Redis::Mutex instance
      def mutex
        @mutex ||= GitHub::Redis::MutexGroup.new \
                     group_key, mutex_key,
                     timeout: 60, wait: 10, sleep: 1
      end
    end

    module ClassMethods
      # Add a document type to be reconciled in the search index.
      #
      # type - The document type to reconcile
      # opts - Options for the Reconciler
      #
      # Returns nil.
      def reconcile(type, opts = {})
        reconcilers << opts.merge(type: type)
        nil
      end

      # Create a new repair job and perform the work.
      #
      # name - The name of the index being repaired.
      # opts - Options Hash
      #        'cluster' - cluster name
      #
      # Returns the repair job instance.
      def perform(name, opts = {})
        self.new(name, opts).perform
      end

      # Returns the queue where the repair job will run. You can override this
      # if needed, but most repair jobs should run from the :index_bulk queue.
      def queue
        :index_bulk
      end

      # Internal: the Array of reconciler configuration setttings.
      def reconcilers
        @reconcilers ||= []
      end
    end

    def self.inherited(job)
      job.extend ClassMethods
    end

    # No-op method to make testing easier.
    def self.reconcilers
      []
    end

  end
end
