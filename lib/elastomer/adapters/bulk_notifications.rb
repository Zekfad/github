# frozen_string_literal: true

module Elastomer::Adapters
  class BulkNotifications < Elastomer::Adapter
    def self.areas_of_responsibility
      [:notifications]
    end

    # We cache list and thread objects as we iterate notification entries.
    # It's possible in some cases that if there are a lot of notification entries
    # with different list/threads this caching is not very effective and would create a lot of memory
    # bloat. This parameter is how many objects to hold on to before discarding them and starting again.
    MAX_CACHE_SIZE = 100

    # Public: Returns the name of the Index class responsible for storing the
    # generated documents. This adapter is not intended to be used directly,
    # so we return `nil` for the Index class name.
    def self.index_name
      nil
    end

    # For compatibility with with existing adapter instantiation
    def self.create(*args)
      new(*args)
    end

    def initialize(notification_entries_iterator)
      @notification_entries_iterator = notification_entries_iterator
    end

    def each(&block)
      ActiveRecord::Base.connected_to(role: :reading) do
        @notification_entries_iterator.each_record do |notification_entry|
          rollup_summary = notification_entry.rollup_summary
          list = list_from_rollup_summary(rollup_summary)
          thread = thread_from_rollup_summary(rollup_summary)

          doc = Elastomer::Adapters::Notification.create(notification_entry, rollup_summary: rollup_summary, list: list, thread: thread)
          block.call(:index, doc.to_hash) if doc.to_hash.present?
        end
      end
    end

    # Raises RuntimeError because you shouldn't use this method.
    def to_hash
      raise RuntimeError, "Bulk notification indexing does not support single hash indexing - use the `each` method."
    end

    private

    # Cache list from rollup summary list type + id to prevent refetching
    def list_from_rollup_summary(rollup_summary)
      @list_by_type_and_id ||= {}

      # Reset cache if it's got too big to avoid memory bloat
      @list_by_type_and_id = {} if @list_by_type_and_id.size > MAX_CACHE_SIZE

      @list_by_type_and_id[rollup_summary.newsies_list.key] ||= rollup_summary.list
    end

    # Cache thread from rollup summary thread key to prevent refetching
    def thread_from_rollup_summary(rollup_summary)
      @thread_by_key ||= {}

      # Reset cache if it's got too big to avoid memory bloat
      @thread_by_key = {} if @thread_by_key.size > MAX_CACHE_SIZE

      @thread_by_key[rollup_summary.newsies_thread.key] ||= rollup_summary.thread
    end
  end
end
