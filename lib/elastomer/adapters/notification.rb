# frozen_string_literal: true

module Elastomer::Adapters
  class Notification < ::Elastomer::Adapter
    def self.areas_of_responsibility
      [:notifications]
    end

    # Public: Returns the name of the Index class responsible for storing the
    # generated documents.
    def self.index_name
      "Notifications"
    end

    # Public: Returns the database cluster name used to wait for replication
    # delay before indexing or deleting the adapter from Elasticsearch.
    def self.mysql_cluster
      ::Newsies::NotificationEntry.throttler_cluster_name
    end

    def initialize(*args)
      super(*args)

      @rollup_summary = options[:rollup_summary]
      @list = options[:list]
      @thread = options[:thread]
    end

    # Public: Accessor for the data model instance. If the `document_id` does
    # not map to any row in the database, then `nil` is returned.
    #
    # Returns the data model instance.
    #
    def model
      @model ||= ::Newsies::NotificationEntry.includes(:rollup_summary).find_by_id(document_id)
    end

    # Document routing information used to co-locate all notifications for a given
    # user on a single shard in the search index.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def document_routing
      @document_routing ||= model&.user_id
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Public: Construct a document suitable for indexing in ElasticSearch and
    # return it as a Hash.
    #
    # Returns the ElasticSearch document as a Hash.
    def to_hash
      if model.nil?
        raise Elastomer::ModelMissing, "The data model has not been set, or the document ID does not exist in the database."
      end

      return @hash if defined? @hash
      @hash = nil

      rollup_summary = model.rollup_summary

      # Return if the list or thread no longer exists
      return unless list && thread

      @hash = {
        _id:         document_id.to_s,
        _type:       document_type,
        _routing:    document_routing,
        user_id:     model.user_id.to_s,
        owner_id:    list_owner_id(rollup_summary).to_s,
        list_type:   model.list_type,
        list_id:     model.list_id.to_s,
        thread_type: thread_type(rollup_summary),
        thread_id:   rollup_summary.newsies_thread.id.to_s,
        updated_at:  model.updated_at,
        reason:      model.reason,
        thread_meta: thread_meta(rollup_summary),
      }

      @hash
    end

    def thread_type(rollup_summary)
      return rollup_summary.newsies_thread.type unless rollup_summary.newsies_thread.type == "Issue"

      # Legacy newsies implementation means that we set the thread type to Issue
      # if the thread is a pull request. We are special casing this here to make filtering by pull request
      # behave as expected.
      thread.pull_request? ? "PullRequest" : "Issue"
    end

    # Internal: Given a Newsies::NotificationEntry, return a hash of thread meta
    #
    # Returns a Hash
    def thread_meta(rollup_summary)
      { author_id: thread.try(:user_id)&.to_s }.compact
    end

    # Internal: Given a RollupSummary, return the id of the owner of the thread.
    #
    # Returns a ID
    def list_owner_id(rollup_summary)
      list.try(:owner_id) || list.try(:organization_id) || list.id
    end

    def rollup_summary
      @rollup_summary ||= model.rollup_summary
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def list
      @list ||= rollup_summary&.list
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def thread
      @thread ||= rollup_summary&.thread
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  end
end
