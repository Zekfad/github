# rubocop:disable Style/FrozenStringLiteralComment

module Elastomer::Indexes

  # The Showcase index contains documents for showcase collections.
  #
  class Showcases < ::Elastomer::Index

    # Defines the mappings for the 'showcase_collection' document type.
    #
    # Returns the Hash containing the document type mappings.
    #
    def self.mappings
      {
        showcase_collection: {
          _all: { enabled: false },
          properties: {
            name: { type: "string", analyzer: "texty" },
            slug: { type: "string", index: "not_analyzed" },
            body: { type: "string", analyzer: "texty" },
            published: { type: "boolean" },
            created_at: { type: "date" },
            updated_at: { type: "date" },
            items: {
              type: "object",
              properties: {
                item_id: { type: "integer" },
                item_name: { type: "string", analyzer: "texty" },
                item_description: { type: "string", analyzer: "texty" },
                body: { type: "string", analyzer: "texty" },
                created_at: { type: "date" },
                updated_at: { type: "date" },
              },
            },
          },
        },
      }
    end

    # Settings for a Showcase search index.
    #
    # Returns the Hash containing the settings for this index.
    #
    def self.settings
      settings = {
        index: {
          number_of_shards: GitHub.es_shard_count_for_showcases,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end
  end
end
