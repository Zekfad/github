# rubocop:disable Style/FrozenStringLiteralComment

module Elastomer::Indexes

  # The PullRequests index contains documents for pull requests (and their
  # comments).
  #
  class PullRequests < ::Elastomer::Index

    # Defines the mapping for the 'pull_request' document type.
    #
    # Returns the Hash containing the document type mappings.
    def self.mappings
      {
        pull_request: {
          _all: { enabled: false },
          _routing: { required: true },
          properties: {
            title: {
              type: "multi_field",
              fields: {
                title: { type: "string", analyzer: "texty" },
                ngram: { type: "string", analyzer: "index_ngram_text", search_analyzer: "search_ngram_text" },
              },
            },
            body:               { type: "string", analyzer: "texty" },
            issue_id:           { type: "integer" },
            author_id:          { type: "integer" },
            num_comments:       { type: "integer" },
            num_reactions:      { type: "integer" },
            repo_id:            { type: "integer" },
            business_id:        { type: "integer" },
            network_id:         { type: "integer" },
            public:             { type: "boolean" },
            archived:           { type: "boolean" },
            state:              { type: "string", index: "not_analyzed" },
            number:             { type: "integer" },
            labels:             { type: "string", index: "not_analyzed" },
            language:           { type: "string", index: "not_analyzed" },
            language_id:        { type: "integer", doc_values: true },
            head_ref:           { type: "string", index: "not_analyzed" },
            base_ref:           { type: "string", index: "not_analyzed" },
            created_at:         { type: "date" },
            updated_at:         { type: "date" },
            closed_at:          { type: "date" },
            locked_at:          { type: "date" },
            locked:             { type: "boolean" },
            assignee_id:        { type: "integer" },
            requested_reviewer_ids: { type: "integer" },
            requested_reviewer_team_ids: { type: "integer" },
            reviewer_ids:       { type: "integer" },
            review_status:      { type: "string", index: "not_analyzed" },
            milestone_num:      { type: "integer" },
            milestone_title:    { type: "string", index: "not_analyzed" },
            project_ids:        { type: "integer" },
            mentioned_user_ids: { type: "integer" },
            mentioned_team_ids: { type: "integer" },
            participating_user_ids: { type: "integer" },
            has_closing_reference: { type: "boolean" },
            status:             { type: "string", index: "not_analyzed" },
            merged:             { type: "boolean" },
            merged_at:          { type: "date" },
            mergeability:       { type: "string", index: "not_analyzed" },
            merge_commit:       { type: "string", index: "not_analyzed" },
            additions:          { type: "integer" },
            deletions:          { type: "integer" },
            changed_files:      { type: "integer" },
            num_commits:        { type: "integer" },
            commits:            { type: "string", index: "not_analyzed" },
            draft:              { type: "boolean" },
            reactions: {
              type: "object",
              properties: {
                "+1":          { type: "integer" },
                "-1":          { type: "integer" },
                smile:         { type: "integer" },
                thinking_face: { type: "integer" },
                heart:         { type: "integer" },
                tada:          { type: "integer" },
              },
            },

            comments: {
              type: "object",
              properties: {
                comment_id:   { type: "integer" },
                comment_type: { type: "string", index: "not_analyzed" },
                body:         { type: "string", analyzer: "texty" },
                author_id:    { type: "integer" },
                created_at:   { type: "date" },
                updated_at:   { type: "date" },
                dead:         { type: "boolean" },
                reactions: {
                  type: "object",
                  properties: {
                    "+1":          { type: "integer" },
                    "-1":          { type: "integer" },
                    smile:         { type: "integer" },
                    thinking_face: { type: "integer" },
                    heart:         { type: "integer" },
                    tada:          { type: "integer" },
                  },
                },
              },
            },
          },
        },
      }
    end

    # Settings for a PullRequests search index.
    #
    # Returns the Hash containing the settings for this index.
    #
    def self.settings
      settings = {
        index: {
          number_of_shards: GitHub.es_shard_count_for_pull_requests,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            index_ngram_text: {
              tokenizer: "standard",
              filter: %w[lowercase standard ngram_text],
            },
            search_ngram_text: {
              tokenizer: "standard",
              filter: %w[lowercase standard],
            },
          },
          filter: {
            ngram_text: {
              type: "edgeNGram",
              min_gram: 2,
              max_gram: 20,
              side: "front",
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end

    # Returns the Array of valid aliases for this index type.
    def self.aliases
      [::Elastomer.env.logical_index_name(self),
        ::Elastomer.env.index_name("issues-search")]
    end

    # Purge all documents for the given user from this search index. This will
    # remove all pull requests where the user is (a) the author of the
    # document or (b) has commented on the document.
    #
    # user - The User for whom all search records will be purged.
    #
    # Returns this search index.
    def purge_user(user)
      begin
        delete_by_query(
            {term: {author_id: user.id}}, type: "pull_request"
        )
      rescue StandardError => boom
        Failbot.report(boom.with_redacting!)
      end

      user.interacted_pull_request_ids.each do |pull_request_id|
        begin
          store Elastomer::Adapters::PullRequest.create(pull_request_id)
        rescue StandardError => boom
          Failbot.report(boom.with_redacting!, pull_request_id: pull_request_id)
        end
      end

      self
    end

    # Restore all documents for given user in this search index. Any pull
    # request or will be re-indexed where the user is (a) the author or (b)
    # has commented on the item.
    #
    # user - The User for whom all search records will be restored.
    #
    # Returns this search index.
    def restore_user(user)
      user.interacted_pull_request_ids.each do |pull_request_id|
        begin
          store Elastomer::Adapters::PullRequest.create(pull_request_id)
        rescue StandardError => boom
          Failbot.report(boom.with_redacting!, pull_request_id: pull_request_id)
        end
      end

      self
    end

    # Search the Pull Request index for merged PRs including a commit
    #
    # commit - Commit object to search for
    # limit  - number of hits to return at most
    #          (optional, defaults to 10)
    #
    # Returns a Hash with keys:
    #   :hits      - Array of hit Hashes
    #   :timed_out - whether the search timed out
    def search_merged_including_commit(commit, limit = 10)
      query = {
        query: { constant_score: {
          filter: { bool: { must: [
            { term: { commits: commit.oid, _cache: false }},
            { terms: { repo_id: [commit.repository&.id.to_i, commit.repository&.parent_id].compact }},
            { term: { merged: true }},
          ]}},
        }},
        _source: %w[merged_at repo_id],
        sort: [{ merged_at: "asc" }],
        size: limit,
      }

      result = search(query,
        type: "pull_request",
        read_timeout: GitHub.enterprise? ? GitHub.es_read_timeout : 2,  # don't take more than 2 seconds
      )

      {
        hits:      result["hits"]["hits"],
        timed_out: result["timed_out"],
      }
    end

  end  # PullRequests
end  # Elastomer::Indexes
