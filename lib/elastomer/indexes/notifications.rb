# frozen_string_literal: true

module Elastomer::Indexes
  # The Notifications index contains documents for notifications. These documents are
  #
  # These documents are searchable within this index.
  class Notifications < ::Elastomer::Index
    def self.areas_of_responsibility
      [:notifications]
    end

    # Defines the mappings for the 'notification' document type.
    #
    # Returns the Hash containing the document type mappings.
    def self.mappings
      {
        notification: {
          _all: { enabled: false },
          _routing: { required: true },
          properties: {
            user_id: { type: "string", index: "not_analyzed" },
            owner_id: { type: "string", index: "not_analyzed" },
            list_type: { type: "string", index: "not_analyzed" },
            list_id: { type: "string", index: "not_analyzed" },
            thread_type: { type: "string", index: "not_analyzed" },
            thread_id: { type: "string", index: "not_analyzed" },
            reason: { type: "string", index: "not_analyzed" },
            updated_at: { type: "date" },
            thread_meta: {
              type: "object",
              properties: {
                author_id: { type: "string", index: "not_analyzed" },
              },
            },
          },
        },
      }
    end

    # Settings for a Notifications search index.
    #
    # Returns the Hash containing the settings for this index.
    def self.settings
      {
        index: {
          number_of_shards: GitHub.es_shard_count_for_notifications,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
      }
    end
  end
end
