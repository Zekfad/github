# frozen_string_literal: true

module Elastomer::Indexes

  # The RMS index contains documents for packages managed by the Registry Metadata Service (RMS).
  # These documents are searchable within this index.
  #
  class RmsPackages < ::Elastomer::Index

    # Defines the mappings for the 'rms_package' document type.
    #
    # Returns the Hash containing the document type mappings.
    #
    def self.mappings
      {
        registry_package: {
          _all: { enabled: false },
          properties: {
            name: {
              type: "multi_field",
              fields: {
                name:  { type: "string", analyzer: "texty" },
                ngram: { type: "string", analyzer: "index_ngram_text", search_analyzer: "search_ngram_text" },
                raw:   { type: "string", index: "not_analyzed" },
              },
            },
            namespace:    { type: "string" },
            owner_id:     { type: "integer" },
            public:       { type: "boolean" },
            summary:      { type: "string", analyzer: "texty" },
            body:        { type: "string", analyzer: "texty" },
            package_type: { type: "string", index: "not_analyzed" },
            downloads:    { type: "integer" },
            version: {
              type: "object",
              properties: {
                version:    { type: "string", index: "not_analyzed" },
                digest:    { type: "string", index: "not_analyzed" },
                latest:     { type: "boolean" },
                created_at: { type: "date" },
                updated_at: { type: "date" },
              },
            },
            applied_topics: { type: "string", analyzer: "lowercase" },
            created_at:     { type: "date" },
            updated_at:     { type: "date" },
          },
        },
      }
    end

    # Settings for a RMS Package search index.
    #
    # Returns the Hash containing the settings for this index.
    #
    def self.settings
      settings = {
        index: {
          number_of_shards:   GitHub.es_shard_count_for_rms_packages,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            index_ngram_text: {
              tokenizer: "standard",
              filter:    %w[lowercase standard ngram_text],
            },
            search_ngram_text: {
              tokenizer: "standard",
              filter:    %w[lowercase standard],
            },
            lowercase: {
              tokenizer: "whitespace",
              filter: %w[lowercase],
            },
          },
          filter: {
            ngram_text: {
              type:     "edgeNGram",
              min_gram: 1,
              max_gram: 20,
              side:     "front",
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end

    # Returns the Array of valid aliases for this index type.
    def self.aliases
      [::Elastomer.env.logical_index_name(self), ::Elastomer.env.index_name("packages")]
    end
  end
end
