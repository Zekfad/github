# rubocop:disable Style/FrozenStringLiteralComment

module Elastomer::Indexes

  # The AuditLog index contains documents for audit log entries. These documents
  # are searchable within this index.
  class AuditLog < ::Elastomer::Index
    # The Lucene limit for each indexable field is 32,766 bytes. Since
    # we support emojis and emojis can be 4 bytes we need to make sure we can
    # fit all characters. 32,766 / 4 = 8,191
    IGNORE_ABOVE_LIMIT = 8191

    def self.slicer
      ::Elastomer::Slicers::AuditLogDateSlicer.new(index: self)
    end

    def self.sliced?
      true
    end

    # Defines the mappings for the 'audit_entry' document type.
    #
    # Returns the Hash containing the document type mappings.
    def self.mappings
      {
        audit_entry: {
          _all: {
            enabled: false,
          },
          dynamic: false,
          dynamic_templates: [
            {
              strings: {
                match_mapping_type: "string",
                mapping: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
              },
            },
          ],
          properties: {
            :@timestamp => {
              type: "date",
            },
            :operation_type => {
              type: "string",
              index: "not_analyzed",
              ignore_above: IGNORE_ABOVE_LIMIT,
            },
            :actor_id => {
              type: "long",
            },
            :actor => {
              type: "string",
              index: "not_analyzed",
              ignore_above: IGNORE_ABOVE_LIMIT,
            },
            :business => {
              type: "string",
              analyzer: "lowercase",
            },
            :business_id => {
              type: "long",
            },
            :staff_actor => {
              type: "string",
              index: "not_analyzed",
              ignore_above: IGNORE_ABOVE_LIMIT,
            },
            :staff_actor_id => {
              type: "long",
            },
            :oauth_app_id => {
              type: "long",
            },
            :action => {
              type: "string",
              norms: {
                enabled: false,
              },
              analyzer: "action",
              search_analyzer: "keyword",
              index_options: "docs",
            },
            :user_id => {
              type: "long",
            },
            :user => {
              type: "string",
              analyzer: "lowercase",
            },
            :repo_id => {
              type: "long",
            },
            :repo => {
              type: "string",
              index: "not_analyzed",
              ignore_above: IGNORE_ABOVE_LIMIT,
            },
            :actor_ip => {
              type: "string",
              index: "not_analyzed",
              ignore_above: IGNORE_ABOVE_LIMIT,
            },
            :actor_location => {
              properties: {
                country_code: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                country_code3: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                country_name: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                region: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                region_name: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                city: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                postal_code: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                location: {
                  type: "geo_point",
                },
                dma_code: {
                  type: "long",
                },
                area_code: {
                  type: "long",
                },
              },
            },
            :created_at => {
              type: "date",
            },
            :from => {
              type: "string",
              index: "not_analyzed",
              ignore_above: IGNORE_ABOVE_LIMIT,
            },
            :note => {
              type: "string",
              index: "no",
            },
            :org => {
              type: "string",
              analyzer: "lowercase",
            },
            :org_id => {
              type: "long",
            },
            :device_cookie => {
              type: "string",
              index: "not_analyzed",
              ignore_above: IGNORE_ABOVE_LIMIT,
            },
            :raw_data => {
              type: "string",
              index: "no",
            },
            :data => {
              type: "object",
              dynamic: true,
              properties: {
                id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                areas_of_responsibility: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                oauth_scopes: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                application_name: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                scopes: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                team: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                title: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                transfer_from: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                transfer_to: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                visibility: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                access: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                public_key_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                key: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                fingerprint: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                auth: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                token: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                card_transactions: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                request_method: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                old_name: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                old_login: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                old_user: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                state: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                error: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                error_messages: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                reason: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                email: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                fork_source: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                fork_parent: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                query_string: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                name: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                actor_session: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                transaction_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                controller: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                version: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                request_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                plan: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                ip_address: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                pull_request: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                owners: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                disabling_reason: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                gist_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                oauth_access_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                application_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                team_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                oauth_application_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                issue_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                issue_comment_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                key_repo_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                key_user_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                old_user_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                comment_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                subject_id: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                two_factor: {
                  type: "boolean",
                },
                ldap_mapped: {
                  type: "boolean",
                },
                successful: {
                  type: "boolean",
                },
                whitelist: {
                  type: "boolean",
                },
                disabled_at: {
                  type: "date",
                },
                disabled_by: {
                  type: "object",
                  enabled: "false",
                },
                enabled_by: {
                  type: "object",
                  enabled: "false",
                },
                change: {
                  type: "object",
                  enabled: "false",
                },
                gist: {
                  type: "object",
                  enabled: "false",
                },
                timing: {
                  properties: {
                    start: {
                      type: "long",
                    },
                    end: {
                      type: "long",
                    },
                    duration: {
                      type: "float",
                    },
                  },
                },
                _invalid: {
                  type: "boolean",
                },
                _invalid_actor: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                _invalid_reason: {
                  type: "string",
                  index: "not_analyzed",
                  ignore_above: IGNORE_ABOVE_LIMIT,
                },
                _invalid_at: {
                  type: "date",
                },
              },
            },
          },
        },
        transition: {
          _all: {
            enabled: false,
          },
          dynamic: false,
          properties: {
            transition: {
              type: "string",
              index: "not_analyzed",
            },
            state: {
              type: "string",
              index: "not_analyzed",
            },
            doc_count: {
              type: "long",
            },
            started_at: {
              type: "date",
            },
            finished_at: {
              type: "date",
            },
          },
        },
      }
    end

    # Settings for an AuditLog search index.
    #
    # Returns the Hash containing the settings for this index.
    def self.settings
      {
        index: {
          mapper: {
            dynamic: false,
          },
          number_of_shards: GitHub.es_shard_count_for_audit_log,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          translog: {
            durability: "request",
          },
        },
        analysis: {
          analyzer: {
            action: {
              type: "custom",
              tokenizer: "action_path",
            },
            lowercase: {
              tokenizer: "whitespace",
              filter: %w[lowercase],
            },
          },
          tokenizer: {
            action_path: {
              type: "path_hierarchy",
              delimiter: ".",
            },
          },
        },
      }
    end

    # Initialize a new AuditLog index.
    #
    # name    - The index name as a String or Symbol
    # cluster - The cluster name as a String or Symbol
    #
    def initialize(name = nil, cluster = nil)
      name = self.class.index_name if name.nil?
      cluster = GitHub.es_audit_log_cluster if cluster.nil?

      super(name, cluster)
    end

    # Internal: Builds the request body for updating the index template with the
    # index's settings and mappings.
    #
    # Returns Hash.
    def template_data
      if Rails.test?
        super.merge(order: 1, aliases: {})
      else
        super.merge \
          order: 4,
          aliases: {self.class.logical_index_name => {}}
      end
    end

  end  # AuditLog
end  # Elastomer::Indexes
