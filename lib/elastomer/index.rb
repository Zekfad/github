# rubocop:disable Style/FrozenStringLiteralComment

require "forwardable"

module Elastomer

  # The Index class understands data Adapter objects. Along with providing
  # access to the Elastomer::Client methods, it provides the `store` and
  # `remove` methods that work with the adapters to extract documents from
  # various data sources and store those documents in ElasticSearch.
  class Index
    include GitHub::AreasOfResponsibility
    extend Forwardable

    # This is a special mapping type we use solely for storing metadata about
    # the search index. This is the least horrible of all the hacks.
    INDEX_META = "index-meta".freeze

    # Sub-classes will override this method to provide index metadata.
    def self.metadata() nil; end

    # Sub-classes will override this method to provide settings.
    def self.settings() nil; end

    # Sub-classes will override this method to provide mappings.
    def self.mappings() nil; end

    # Returns an instance of the search index configured with the name and
    # cluster settings for the primary search index.
    def self.primary
      settings = ::Elastomer.router.primary(self)
      self.new(settings.name, settings.cluster) if settings
    end

    # Create a new Index that will search and store documents in the
    # specific ElasticSearch index.
    #
    # name    - The index name as a String or Symbol
    # cluster - The cluster name as a String or Symbol
    #
    def initialize(name = nil, cluster = nil)
      name = self.class.index_name if name.nil?
      cluster = ::Elastomer.router.cluster_for_index(name) if cluster.nil?

      @cluster_name = cluster
      @client = ::Elastomer.router.client(@cluster_name)
      @index = @client.index(name)
      @docs = @index.docs
      @template = @client.template(name)
      @cluster = @client.cluster
      @bulk_request_size = 512.kilobytes
    end

    # COMPATIBILITY - delegate back to :refresh from elastomer-client after ES5 is in place!
    def_delegators :@index,
        :name, :exists?, :mapping, :update_mapping, :settings, :update_settings,
        :open, :close, :analyze, :bulk, :scan, :scroll

    # Elastomer::Client instance
    attr_reader :client

    # API methods for working with the ES index.
    attr_reader :index

    # API methods for working with storing and querying documents in the index.
    attr_reader :docs

    # API methods for working with the ES template.
    attr_reader :template

    # API methods for working with the ES cluster.
    attr_reader :cluster

    # The name of the cluster where this index resides
    attr_reader :cluster_name

    # The default bulk request size for this index
    attr_reader :bulk_request_size

    # COMPATIBILITY - overrides elastomer-client refresh method; remove when ES5 upgrade is complete
    def refresh(params = {})
      if client.strict_params? || client.version_support.strict_request_params?
        params = params.reject { |k, _| k.to_sym == :timeout }
      end
      index.refresh(params)
    end

    # Returns the Time when the index was created.
    #
    # Raises Elastomer::Client::RequestError if the index does not exist.
    def creation_date
      return @creation_date if defined? @creation_date
      @creation_date = nil

      settings = @index.get_settings.values.first
      epoch_in_millis = settings["settings"]["index"]["creation_date"]
      return if epoch_in_millis.nil?

      epoch = Integer(epoch_in_millis) / 1000.0
      @creation_date = Time.at(epoch)
    end

    # Add an alias to this index.
    #
    # alias_name - The alias name as a String
    #
    # Returns the response body as a Hash.
    def add_alias(alias_name)
      @cluster.update_aliases add: {index: @index.name, alias: alias_name}
    end

    # Remove an alias from this index.
    #
    # alias_name - The alias name as a String
    #
    # Returns the response body as a Hash.
    def remove_alias(alias_name)
      @cluster.update_aliases remove: {index: @index.name, alias: alias_name}
    end

    # Returns the list of all aliases (an Array of Strings) associated with
    # this index.
    def get_aliases
      h = @index.get_aliases[@index.name]
      return [] unless h.key? "aliases"
      h["aliases"].keys
    end

    # Get the index configuration for this index.
    #
    # Returns an IndexConfig instance.
    def get_config
      config = Elastomer.router.get_index_config(name)

      if config.nil?
        metadata = get_metadata || {}
        config = Elastomer::Router::IndexConfig.new \
          name: name,
          cluster: cluster_name,
          version: metadata["version"]
      end

      config
    end

    # Update the index configuration for this index.
    #
    # hash - The Hash of values to update.
    #
    # Returns an IndexConfig instance.
    def update_config(hash)
      config = get_config
      config.update!(hash)
      Elastomer.router.update_index_config(config)
      config
    end

    # Fetch the metadata hash from the search index mappings. This will make a
    # call to the search cluster for this information.
    #
    # Returns the metadata Hash.
    def get_metadata
      Elastomer::Index.get_metadata @index
    end

    # Returns the SHA1 version string of the index stored in the ES settings
    # for the index. `nil` is returned if the index does not exist or has no
    # version string.
    def version
      return unless index.exists?

      version = get_metadata["version"]
      version.freeze unless version.nil?
    end

    # Create the index on the ElasticSearch cluster. If the class method
    # `mappings` exists it will be called and the returned value will be used
    # as the document type mappings for the index. Likewise, if the class
    # method `settings` exists it will be called and the returned value will
    # be used for the index settings during creation.
    #
    # opts - The options Hash
    #        :mappings - the Hash containing the document type mappings
    #        :settings - the Hash containing the index settings
    #        :metadata - the Hash containing index metadata (version, etc.)
    #
    # Returns the create operation result Hash from the ElasticSearch cluster.
    def create(opts = {})
      SearchIndexManager.create_index \
          name:        self.name,
          cluster:     self.cluster_name,
          index_class: self.class,
          mappings: opts[:mappings],
          settings: opts[:settings],
          metadata: opts[:metadata]
    end

    # Internal: [COMPATIBILITY] Adjust the mappings to conform to ES 5.x style
    # when underlying cluster version is >= 5.0.0; leave untouched otherwise.
    # ^ can be removed when ES 5.x upgrade is complete.
    def conform_to_es5(mappings)
      SearchIndexManager.conform_to_es5(mappings, @client.semantic_version)
    end

    # Delete this index from the ElasticSearch cluster.
    #
    # Returns the delete operation result Hash from the ElasticSearch cluster.
    def delete(options = {})
      SearchIndexManager.delete_index \
          name:        self.name,
          cluster:     self.cluster_name,
          index_class: self.class
    end

    # Perform a search request using the `query` document and the `params` hash.
    #
    # query  - The query Hash or JSON String
    # params - The request params Hash
    #          :type    - A single type String or an Array of type Strings
    #          :routing - A single routing String or an Array of routing Strings
    #          :search_type - The type of search to perform
    #
    # See the ElasticSearch documentation
    # (http://www.elasticsearch.org/guide/reference/api/search/search-type.html)
    # for an explanation of the search type.
    #
    # Returns a Hash containing the search results.
    # Raises an Elastomer::Client::Error upon failure.
    def search(query, params = {})
      GitHub.tracer.with_span("elastomer query", tags: { index_name: self.name, cluster: self.cluster_name }) do |span|
        @docs.search(query, params)
      end
    rescue ::Elastomer::Client::Error => boom
      # NOTE: Failbot does not like large exception messages and will not forward them to haystack
      boom.message.slice!(1024..-1)
      raise boom
    end
    alias :query :search

    # Count the number of documents in the search index that match the given
    # query.
    #
    # query  - The query Hash or JSON String.
    # params - The request params Hash.
    #          :type    - A single type String or an Array of type Strings
    #          :routing - A single routing String or an Array of routing Strings
    #
    # Returns the number of matching documents.
    # Raises a Elastomer::Client::Error upon failure.
    def count(query, params = {})
      hash = @docs.count(query, params)
      hash["count"].to_i
    rescue ::Elastomer::Client::Error => boom
      # NOTE: Failbot does not like large exception messages and will not forward them to haystack
      boom.message.slice!(1024..-1)
      raise boom
    end
    alias :count_query :count

    # Count the number of documents in the search index that match the given
    # query. Uses the #search method in order to pass a timeout to Elasticsearch.
    #
    # query  - The query Hash or JSON String.
    # params - The request params Hash.
    #          :type    - A single type String or an Array of type Strings
    #          :routing - A single routing String or an Array of routing Strings
    #
    # Returns a Hash containing the search results.
    # Raises a Elastomer::Client::Error upon failure.
    def count_with_timeout(query, params = {})
      @docs.search(query, params.merge(size: 0))
    rescue ::Elastomer::Client::Error => boom
      # NOTE: Failbot does not like large exception messages and will not forward them to haystack
      boom.message.slice!(1024..-1)
      raise boom
    end

    # Given a data adapter object, this method will generate a document hash
    # from the adapter and store it in the search index. If the adapter
    # supports the `each` method, then multiple documents will be generated
    # and stored via bulk indexing.
    #
    # adapter - An Adapter instance
    # params  - Parameters Hash
    #
    # Returns the response body as a Hash.
    def store(adapter, params = {})
      adapter.index = self
      params = adapter.params.merge(params)

      if adapter.respond_to?(:each)
        # initiate a bulk transaction
        response = @index.bulk(request_size: bulk_request_size) do |bulk|
          start = Time.now
          adapter.each do |action, document|
            begin
              case action
              when :index, "index", :store, "store"
                start = fetch_timing(name, adapter.document_type, start)
                bulk.index document
              when :delete, "delete", :remove, "remove"
                bulk.delete document
              end
            rescue Elastomer::Client::Error => err
              Failbot.report err,
                bulk_action: action,
                index: name,
                document_type: adapter.document_type,
                document_id: adapter.document_id
            end
          end
        end

        count_bulk_indexing_errors(response)
        response
      else
        start = Time.now
        doc = adapter.to_hash
        return if doc.nil?

        fetch_timing(name, adapter.document_type, start)
        @docs.index(doc.dup, params)
      end
    end

    # Internal: Record timing statistics for how long it took to fetch a
    # document from the original data source.
    #
    # index - The name of the index
    # type  - The name of the document type
    # start - The start Time of the fetch process
    #
    # Returns a new start Time
    def fetch_timing(index, type, start)
      GitHub.dogstats.timing("search.fetch", ((Time.now - start) * 1000).round, { tags: ["index:" + index.to_s, "type:" + type.to_s] })
      Time.now
    end

    # Internal: Record failure metrics for a bulk indexing response if it has
    # errors.
    #
    # response - the bulk response from Elasticsearch (may be nil if response
    #            fails entirely)
    def count_bulk_indexing_errors(response)
      if response.present? && response.fetch("errors")
        items = response.fetch("items")
        failures = items.count { |item| !(200..299).cover?(item.values.first.fetch("status")) }
        GitHub.dogstats.count("rpc.elasticsearch.error", failures, tags: %W[rpc_operation:bulk index:#{index.name}])
      end
    end

    # Remove documents from the search index based on the `document_type` and
    # `id` from the adapter. If the adapter has a `delete_query` method, then
    # it will be used to remove multiple documents from the search index.
    #
    # adapter - An Adapter instance
    #
    # Returns the response body as a Hash.
    def remove(adapter)
      adapter.index = self

      if adapter.respond_to? :delete_query
        query, opts = adapter.delete_query
        opts[:type] = adapter.document_type unless opts.key? :type

        delete_by_query(query, opts)
      else
        params = {
          type: adapter.document_type,
          id:   adapter.document_id,
        }
        params[:routing] = adapter.document_routing if adapter.document_routing
        @docs.delete(params)
      end
    end

    # Delete documents from this index based on a query. This method supports
    # both the "request body" query and the "URI request" query.
    #
    # query - The query body as a Hash
    # opts  - Options Hash
    #
    # Examples
    #
    #   # request body query
    #   delete_by_query({:query => {:match_all => {}}}, :type => 'tweet')
    #
    #   # same thing but using the URI request method
    #   delete_by_query(:q => '*:*', :type => 'tweet')
    #
    # Returns the response body as a hash
    def delete_by_query(query, opts = nil)
      if opts.nil? && query.key?(:q)
        opts, query = query, nil
      end

      if query && !query.key?(:query)
        query = {query: query}
      end

      @docs.delete_by_query(query, opts)
    end

    # Public: The template identifier for creating and deleting the template.
    # Can be overridden in subclasses.
    #
    # Returns String.
    def template_id
      name
    end

    # Internal: Builds the request body for updating the index template with the
    # index's settings and mappings.
    #
    # Returns Hash.
    def template_data
      data = {
        template: "#{template_id}-*",
        settings: self.class.settings,
        mappings: self.class.mappings,
      }

      # COMPATIBILTY - remove after ES 5.x upgrade
      data[:mappings] = conform_to_es5(data[:mappings])

      data
    end

    # Internal: Fetch the metadata hash from the search index mappings. This
    # will make a call to the search cluster for this information.
    #
    # index - Elastomer::Client::Index instance (or anything that provides a
    #         `mapping` method).
    #
    # Returns the metadata Hash.
    def self.get_metadata(index)
      hash = index.mapping(type: INDEX_META)
      # Support extra 'mappings' hash level in ES1
      hash = hash[index.name]["mappings"] if hash.key?(index.name)
      (hash[INDEX_META] && hash[INDEX_META]["_meta"]) || {}
    rescue Elastomer::Client::IndexNotFoundError
      return {}
    end

    # Default name for the current search index. This is generated from the
    # index class name and the current Rails environment.
    #
    # Examples
    #
    #   Elastomer::Indexes::Issues.index_name
    #   #=> 'issues-test'
    #   # Rails.env == 'test'
    #
    #   Elastomer::Indexes::CodeSearch.index_name
    #   #=> 'code-search'
    #   # Rails.env == 'production'
    #
    def self.index_name
      slicer.index
    end

    # The logical index name independent of the test environment settings.
    #
    # Examples
    #
    #   Elastomer::Indexes::Issues.logical_index_name
    #   #=> 'issues-test'
    #   # Rails.env == 'test'
    #
    #   Elastomer::Indexes::CodeSearch.logical_index_name
    #   #=> 'code-search'
    #   # Rails.env == 'production'
    #
    def self.logical_index_name
      slicer.index
    end

    # Internal: Helper method that will validate a specific index name against
    # this index. This check can be used to ensure that we don't perform
    # indexing operations against the wrong index.
    #
    # name  - The index name to validate
    #
    # Returns `true` if the index name is valid for this index
    def self.valid_index_name?(name)
      s = slicer
      s.fullname = name
      logical_index_name == s.index
    rescue ArgumentError
      return false
    end

    # Internal: Returns the Slicer instance used to provide name analysis and
    # generation for this particular Index class. We use a method here so that
    # subclasses of Index can provide their own Slicer inmplementations.
    #
    # Returns a Slicer instance with the `index` configured for this Index class.
    def self.slicer
      Slicer.new(index: self)
    end

    # Public: Returns `true` if this index is sliced into smaller range or
    # date-based indices. The audit log is a good example of a date-based,
    # sliced index.
    def self.sliced?
      false
    end

    # Public: Constructs a slice name based on the given value using the Slicer
    # configured for this Index class.
    def self.slice_from_value(value)
      return nil if value.nil?

      value = value.slice_value if value.is_a?(::Elastomer::Adapter)
      slicer.slice_from_value(value)
    end

  end
end
