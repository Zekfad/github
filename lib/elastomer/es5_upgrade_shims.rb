# rubocop:disable Style/FrozenStringLiteralComment

# COMPATIBILITY
#
# This is the cleanest way to handle breaking req parameter changes in ES across 2.x -> 5.x
# without silently ignoring bad params in the client shims. This allows other end users to
# intercept bad param exceptions from the client and handle as they wish, while we shim
# our codebase for the upgrade here in a way that works for us.
#
# TODO: remove this WHOLE FILE along with the other COMPATIBILITY shims in gh/gh once the ES5 upgrade is complete
module Elastomer
  class Client

    # COMPATIBILITY: fixes native delete-by-q only available to ES5
    # no version check as native calls only route to ES5 clusters
    def native_delete_by_query(query, parameters = {})
      shimmed = parameters.reject { |k, _| k.to_sym == :read_timeout }
      NativeDeleteByQuery.new(self, query, shimmed).execute
    end

    module CountWithShim
      # COMPATIBILITY - :timeout param has been removed directly from search query builder
      # TODO: remove use of :search_type = "count" in audit_log query builder, use explicit _count query on ES5
      PARAMS_TO_REMOVE = %i[search_type].freeze

      def search(query, params = {})
        start = Time.now
        query, params = extract_params(query) if params.nil?
        shimmed_query = query

        GitHub.tracer.with_span("ES5 Query Shim", tags: {method: "search", index: name}) do
          if client.version_support.es_version_5_x?
            shimmed_query = strip_cache_key(shimmed_query)
          end
        end

        GitHub.dogstats.timing("search.es5_shim.search.elapsed", ((Time.now - start) * 1000).round, {tags: ["index:" + name]})
        super(shimmed_query, params)
      end

      def count(query, params = nil)
        start = Time.now
        query, params = extract_params(query) if params.nil?
        shimmed_query = query
        shimmed_params = params

        GitHub.tracer.with_span("ES5 Query Shim", tags: {method: "count", index: name}) do
          if client.strict_params? || client.version_support.strict_request_params?
            shimmed_params = shimmed_params.reject { |k, _| PARAMS_TO_REMOVE.include? k.to_sym }
            shimmed_query = strip_cache_key(shimmed_query)
            if shimmed_query.is_a? Hash
              shimmed_query = shimmed_query.select { |k, _| k.to_sym == :query }
            end
          end
        end

        GitHub.dogstats.timing("search.es5_shim.count.elapsed", ((Time.now - start) * 1000).round, {tags: ["index:" + name]})
        super(shimmed_query, shimmed_params)
      end

      private

      # remove the _cache_key entry in query hash (deprecated now, removed in 6.x)
      def strip_cache_key(x)
        if x.is_a? Hash
          x.each_with_object({}) do |(k, v), out|
            out[k] = strip_cache_key(v) unless k.to_sym == :_cache_key
          end
        elsif x.is_a? Array
          x.map do |v|
            strip_cache_key(v)
          end
        else
          x
        end
      end
    end # end CountWithShim

    Docs.send(:prepend, CountWithShim)
  end # end Client
end # end Elastomer
