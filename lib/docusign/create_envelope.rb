# frozen_string_literal: true

require "docusign_esign"

module Docusign
  class CreateEnvelope
    def self.call(name:, email:, subject:, template_id:)
      new(
        name: name,
        email: email,
        subject: subject,
        template_id: template_id,
      ).call
    end

    def initialize(name:, email:, subject:, template_id:)
      @name = name
      @email = email
      @subject = subject
      @template_id = template_id
    end

    def call
      DocuSign_eSign::EnvelopesApi.new(API.client)
        .create_envelope(GitHub.docusign_account_id, envelope_configuration)
    end

    private

    attr_reader :name, :email, :template_id, :subject

    def envelope_configuration
      DocuSign_eSign::EnvelopeDefinition.new \
        emailSubject: subject,
        status: ::Docusign::STATUS_SENT,
        templateId: template_id,
        templateRoles: [signer],
        eventNotification: webhook_event
    end

    def signer
      DocuSign_eSign::TemplateRole.new \
        email: email,
        clientUserId: GitHub.docusign_user_id,
        name: name,
        roleName: "Signer"
    end

    def webhook_event
      {
        "url": "#{GitHub.docusign_webhook_base_url}/billing/docusign/webhook",
        "includeDocuments": "false",
        "includeDocumentFields": "false",
        "includeHMAC": "true",
        "loggingEnabled": "true",
        "envelopeEvents": [
          {"envelopeEventStatusCode": "sent"},
          {"envelopeEventStatusCode": "delivered"},
          {"envelopeEventStatusCode": "completed"},
          {"envelopeEventStatusCode": "declined"},
          {"envelopeEventStatusCode": "voided"},
        ],
      }
    end
  end
end
