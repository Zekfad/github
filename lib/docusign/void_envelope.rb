# frozen_string_literal: true

require "docusign_esign"

module Docusign
  class VoidEnvelope
    def self.call(envelope_id:, reason:)
      new(
        envelope_id: envelope_id,
        reason: reason,
      ).call
    end

    def initialize(envelope_id:, reason:)
      @envelope_id = envelope_id
      @reason = reason
    end

    def call
      options = { status: "voided", voidedReason: reason }
      DocuSign_eSign::EnvelopesApi.new(API.client)
        .update(GitHub.docusign_account_id, envelope_id, options)
    end

    private

    attr_reader :envelope_id, :reason
  end
end
