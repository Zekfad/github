# frozen_string_literal: true

module Docusign
  class WebhookHandler
    InvalidSignature = Class.new(StandardError)
    PayloadParseError = Class.new(StandardError)

    # Public: Instantiate a new handler by creating an incoming Docusign event
    # payload - Docusign webhook payload
    #
    # Returns [WebhookHandler] instance
    def self.handle(payload:, signature:)
      validate_signature(signature: signature, payload: payload)
      new(payload: payload).handle
    rescue Nokogiri::XML::SyntaxError
      GitHub.dogstats.increment("docusign.webhook.payload_parse_error")
      raise PayloadParseError
    end

    def initialize(payload:)
      @payload = Hash.from_xml(payload)
      @fingerprint = fingerprint_for(payload)
    end

    # Public: Handles an incoming Docusign webhook event
    #
    # Returns [WebhookHandler] instance
    def handle
      GitHub.dogstats.increment("docusign.webhook")

      webhook = DocusignWebhook.create!(
        fingerprint: fingerprint,
        payload: truncate_payload(payload),
      )

      DocusignWebhookJob.perform_later(webhook)

      self
    end

    def self.validate_signature(signature:, payload:)
      signature_is_valid = ::Docusign.signature_valid? \
        signature: signature,
        payload: payload

      unless signature_is_valid
        GitHub.dogstats.increment("docusign.webhook.signature_verification_error")
        raise InvalidSignature
      end
    end

    private

    attr_reader :payload, :fingerprint

    def truncate_payload(payload)
      payload["DocuSignEnvelopeInformation"]["EnvelopeStatus"].delete("RecipientStatuses")
      payload["DocuSignEnvelopeInformation"]["EnvelopeStatus"].delete("CustomFields")
      payload
    end

    def fingerprint_for(payload)
      OpenSSL::HMAC.hexdigest(
        OpenSSL::Digest.new("sha256"),
        "#{envelope_id}:#{time_generated}",
        payload,
      )
    end

    def envelope_id
      @payload.dig \
        "DocuSignEnvelopeInformation",
        "EnvelopeStatus",
        "EnvelopeID"
    end

    def time_generated
      @payload.dig \
        "DocuSignEnvelopeInformation",
        "EnvelopeStatus",
        "TimeGenerated"
    end
  end
end
