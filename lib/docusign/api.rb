# frozen_string_literal: true

require "docusign_esign"

module Docusign
  class API
    TOKEN_TTL = 1.hour

    class << self
      def client
        client = DocuSign_eSign::ApiClient.new(configuration)
        client.base_path = GitHub.docusign_api_base_path
        set_access_token(client)
        client
      end

      private

      def set_access_token(client)
        token = fetch_token_if_needed(client)
        client.set_default_header("Authorization", "Bearer #{token.access_token}")
      end

      def fetch_token_if_needed(client)
        GitHub.cache.fetch(cache_key, ttl: TOKEN_TTL) do
          GitHub.dogstats.increment("docusign", tags: ["action:fetch_token"])
          client.request_jwt_user_token(
            GitHub.docusign_client_id,
            GitHub.docusign_user_id,
            GitHub.docusign_private_key,
            TOKEN_TTL,
          )
        end
      end

      def configuration
        configuration = DocuSign_eSign::Configuration.new
        configuration.host = GitHub.docusign_api_base_path
        configuration
      end

      def cache_key
        "docusign:#{GitHub.docusign_user_id}:token"
      end
    end
  end
end
