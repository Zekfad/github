# frozen_string_literal: true

require "github/config/hydro"

# A Resque proxy that adds Hydro buffering functionality. Buffering allows us
# to enqueue jobs in the event of a redis outage and catch up automatically
# once redis has recovered.
module Resque
  module BufferedResque
    extend self

    HYDRO_PUBLISH_PATH = :hydro_path
    RESQUE_PUBLISH_PATH = :resque_path

    # A list of idempotent jobs that should exercise both Resque and Hydro
    # publish paths.
    DUAL_PUBLISH = [
      GitHub::Jobs::GistDelete.name,
      GistDeleteJob.name,
    ]

    # Public: A wrapper around `Resque.enqueue_to` that optionally routes jobs
    # through Hydro to protect against Redis outages. Callers can specify a
    # preferred publish path: Resque with a Hydro fallback (the default) or
    # Hydro with a Resque fallback.
    #
    # [ dotcom -> hydro -> relay process -> resque ]
    # The Hydro publish path is encouraged for jobs that can tolerate higher
    # latency (occasionally on the order of seconds) between enqueue time and
    # execution. This might include scheduled jobs or jobs that don't trigger
    # webhooks or UI updates. If this path fails, it will fall back to Resque.
    #
    # [ dotcom -> resque ]
    # The Resque publish path is encouraged for jobs that need low latency
    # between enqueue time and execution. This might jobs that are involved in
    # interactions like webhooks or UI updates. If this path fails (e.g. during
    # a redis outage), it will fall back to the Hydro path. When redis recovers,
    # Hydro will take care of pushing any buffered jobs.
    #
    # queue          - The String name of the Resque queue.
    # queueable      - The job class.
    # publish_path   - Either HYDRO_PUBLISH_PATH or RESQUE_PUBLISH_PATH. This
    #                  explicit arg should only be used in environments that
    #                  don't support feature flagging. Passing nil will use
    #                  the 'enqueue_to_hydro' feature flag to choose the publish
    #                  path.
    # job_identifier - The String name of the class that actually executes a
    #                  job if enqueued from a framework that uses a generic
    #                  job class wrapper (e.g. ActiveJob).
    #
    # Returns a Boolean to indiciate if the enqueue operation was successful.
    def enqueue_to(queue, job_class, *args, publish_path: nil, job_identifier: nil)
      job = Job.new(
        queue: queue,
        resque_job_class: job_class,
        job_class_identifier: job_identifier,
        args: args,
        publish_path: publish_path,
      )

      if job.enqueue_to_hydro?
        success = job.enqueue_to_hydro.success?

        if !success || job.dual_publish?
          success = job.enqueue_to_resque
        end
      else
        begin
          success = job.enqueue_to_resque
        rescue *GitHub::Config::Redis::REDIS_DOWN_EXCEPTIONS => e
          GitHub.dogstats.increment("buffered_resque.redis.errors", {
            tags: job.tags + ["error:#{e.class}"],
          })
          result = job.enqueue_to_hydro
          success = result.success? || raise(result.error)
        end
      end

      success
    end

    def enqueue_to_hydro?(job_class_identifier)
      Job.enqueue_to_hydro?(job_class_identifier)
    end

    class Job
      attr_reader :queue, :args

      # Internal: Some jobs are idempotent and can exercise both publish paths.
      def self.dual_publish?(job_class_identifier)
        DUAL_PUBLISH.include?(job_class_identifier.to_s)
      end

      # Public: Should this job class be queued via hydro?
      #
      # job_identifier - The String name of the class that actually executes a
      #                  job. For Resque jobs, this should be the name of the
      #                  job class. For ActiveJobs, this should be the name of
      #                  the ActiveJob subclass (i.e. not the Resque wrapper).
      #
      # Returns a Boolean.
      def self.enqueue_to_hydro?(job_class_identifier)
        dual_publish?(job_class_identifier)
      end

      def initialize(queue:, resque_job_class:, args:, job_class_identifier: nil, publish_path: nil)
        @queue = queue
        @resque_job_class = resque_job_class
        @args = args
        @job_class_identifier = (job_class_identifier || resque_job_class).to_s
        @publish_path = publish_path.nil? ? choose_publish_path : publish_path
      end

      def enqueue_to_hydro?
        @publish_path == HYDRO_PUBLISH_PATH
      end

      def dual_publish?
        self.class.dual_publish?(job_class_identifier)
      end

      # Internal: Publish a job via the Hydro path. Jobs are published as hydro
      # events and then a relay consumer process pulls jobs from Hydro and pushes
      # them to Resque. If redis goes down, the relay process will pause and
      # resume when redis service has been restored.
      def enqueue_to_hydro
        result = GitHub.dogstats.time("buffered_resque.hydro.publish_time", tags: tags) do
          GitHub.sync_hydro_publisher.publish({
            queue: queue.to_s,
            class_name: resque_job_class.to_s,
            job_identifier: job_class_identifier_if_necessary,
            args: args.to_json,
            retries: retries,
            request_id: GitHub.context[:request_id],
            current_ref: GitHub.current_ref,
          },
          schema: "github.v1.ResqueJob",
          topic: hydro_topic,
          )
        end

        report_hydro_enqueue_error(result.error) unless result.success?

        result
      rescue => e
        report_hydro_enqueue_error(e)
        Hydro::Sink::Result.failure(e)
      end

      # Internal: Publish a job via the standard Resque path. Jobs are enqueued
      # directly to redis.
      def enqueue_to_resque
        GitHub.dogstats.time("buffered_resque.resque.publish_time", tags: tags) do
          Resque.enqueue_to(queue, resque_job_class, *args)
        end
      end

      def tags
        ["class:#{job_class_identifier.to_s.underscore}", "queue:#{queue}"]
      end

      private

      attr_reader :resque_job_class, :job_class_identifier

      def hydro_topic
        "review-lab.v1.ResqueJob" if GitHub.dynamic_lab?
      end

      def report_hydro_enqueue_error(error)
        Failbot.report(error, app: "buffered-resque")

        GitHub.dogstats.increment("buffered_resque.hydro.errors", {
          tags: tags.push("error:#{error.class}"),
        })
      end

      # Internal: Selects a publish path if the caller didn't specify a path
      # explicitly.
      def choose_publish_path
        if self.class.enqueue_to_hydro?(job_class_identifier)
          HYDRO_PUBLISH_PATH
        else
          RESQUE_PUBLISH_PATH
        end
      end

      # Internal: We only bother passing along a job class identifier when
      # the identifier differs from the job class.
      #
      # Returns a String identifier or nil.
      def job_class_identifier_if_necessary
        job_class_identifier != resque_job_class.to_s ? job_class_identifier : nil
      end

      def retries
        if args.last.kind_of?(Hash)
          args.last[:retries] || args.last["retries"] || 0
        end
      end
    end

    # An actor representing a job class.
    #
    # The flipper_id includes the class name so it integrates with our flipper
    # actor storage and the internal GraphQL API.
    class JobClassActor
      attr_reader :flipper_id

      def self.find_by_id(id) # rubocop:disable GitHub/FindByDef
        new(id)
      end

      def initialize(job_class_name)
        @flipper_id = "#{self.class.name}:#{job_class_name}"
      end

      def to_s
        @flipper_id
      end
    end
  end
end
