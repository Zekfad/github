# rubocop:disable Style/FrozenStringLiteralComment

module Porter
  # Accesses an internal API in porter to get information to show in stafftools.
  class StafftoolsClient
    extend Porter::Urls

    def self.get_import_status(repository:, env:)
      repository_url = porter_admin_repository_url(
        repository: repository,
        options:    { json: true },
      )

      options = {
        repository_url: repository_url,
        request_id:     env["HTTP_X_GITHUB_REQUEST_ID"],
      }

      new(options).get_status
    end

    def initialize(options)
      @repository_url = options.fetch(:repository_url)
      @request_id = options.fetch(:request_id)
      @auth_token = options.fetch(:internal_api_token) { GitHub.porter_internal_api_token }
      @timeout = options.fetch(:timeout, 1)
    end

    attr_reader :repository_url, :request_id, :auth_token, :timeout

    def get_status
      GitHub::Timer.timeout(timeout, Faraday::TimeoutError) do
        response = faraday.get(repository_url)
        GitHub::Logger.log({
          fn: "PorterStafftoolsClient#get_status",
          url: repository_url,
          response: response.status,
        })
        if response.success?
          json = GitHub::JSON.load(response.body)
          if json["found"]
            [:ok, json.slice("status", "failed_step")]
          else
            [:not_found, nil]
          end
        else
          nil
        end
      end
    rescue => e
      Failbot.report!(e, porter_url: repository_url, areas_of_responsibility: [:porter])
      nil
    end

    private

    def faraday
      @faraday ||= build_faraday
    end

    def build_faraday
      options = {
        url: repository_url,
        headers: {
          "Accept" => "application/vnd.porter.v1+json",
          "X-GitHub-Request-Id" => request_id,
        },
        request: {
          timeout: timeout,
          open_timeout: 5,
        },
      }
      Faraday.new(options) do |c|
        c.basic_auth(auth_token, "github")
        c.adapter Faraday.default_adapter
      end
    end
  end
end
