# rubocop:disable Style/FrozenStringLiteralComment

module Porter
  class ImportedDomainsClient
    include Porter::Urls

    def initialize(internal_api_token: GitHub.porter_internal_api_token, timeout: 1)
      @auth_token = internal_api_token
      @timeout    = timeout
    end

    attr_reader :auth_token, :timeout

    def imports_csv(user_ids, headers: false)
      response = faraday.get(golden_ticket_url, user_ids: user_ids.join(","), headers: headers)
      if response.success?
        return response.body
      else
        raise "Unable to get import data (#{response.status})\n#{response.body}"
      end
    end

    private

    def golden_ticket_url
      porter_admin_golden_ticket_url
    end

    def faraday
      @faraday ||= build_faraday
    end

    def build_faraday
      options = {
        headers: {
          "Accept" => "application/vnd.porter.v1+json",
        },
        request: {
          timeout: timeout,
          open_timeout: 5,
        },
      }
      Faraday.new(options) do |c|
        c.basic_auth(auth_token, "github")
        c.adapter Faraday.default_adapter
      end
    end
  end
end
