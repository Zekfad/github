# frozen_string_literal: true

# Copyright (c) 2016 Lorefnon
# Copyright (c) 2010-2014 Vladimir Dobriakov
# Copyright (c) 2008-2009 Vodafone
# Copyright (c) 2007-2008 Ryan Allen, FlashDen Pty Ltd

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

module Workflow
  class WorkflowDefinitionError < StandardError; end
  class NoTransitionAllowed < StandardError; end

  def self.included(base)
    base.extend(ClassMethods)
    base.include(InstanceMethods)

    base.before_validation :write_initial_state
  end

  module ClassMethods
    attr_reader :workflow_spec

    def workflow(column, &specification)
      @workflow_state_column_name = column
      @workflow_spec = Specification.new(Hash.new, &specification)
      define_workflow_methods
      define_scopes

      define_method("#{@workflow_state_column_name}=") do |val|
        matching_state = self.class.workflow_spec.states.select { |k, v| v.name.to_s == val.to_s }.values.first
        val = matching_state.value if matching_state
        super(val)
      end
    end

    def workflow_column
      @workflow_state_column_name
    end

    private

    def define_scopes
      @workflow_spec.states.values.each do |state|
        define_singleton_method("with_#{state}_state") do
          where(workflow_column.to_sym => state.value)
        end

        define_singleton_method("without_#{state}_state") do
          where.not(workflow_column.to_sym => state.value)
        end

        define_method("without_#{state}_state") do
          where.not(self.class.workflow_column.to_sym => state.value)
        end
      end
    end

    def define_workflow_methods
      @workflow_spec.states.values.each do |state|
        state_name = state.name
        module_eval do
          define_method "#{state_name}?" do
            state_name == current_state.name
          end
        end

        unique_events = state.events.values.flatten.uniq do |event|
          [:name, :transitions_to, :meta, :action].map { |m| event.send(m) }
        end

        unique_events.each do |event|
          event_name = event.name
          module_eval do
            define_method "#{event_name}!".to_sym do |*args, **kwargs|
              process_event!(event_name, *args, **kwargs)
            end

            define_method "can_#{event_name}?" do
              !!current_state.events.fetch(event_name, []).detect do |event|
                event.condition_applicable?(self) && event
              end
            end
          end
        end
      end
    end
  end

  module InstanceMethods
    def current_state
      loaded_state_value = read_attribute(self.class.workflow_column)
      if loaded_state_value
        loaded_state_name = self.class.workflow_spec.states.select { |k, v| v.value.to_s == loaded_state_value.to_s }.keys.first
      end

      res = self.class.workflow_spec.states[loaded_state_name.to_sym] if loaded_state_name
      res || self.class.workflow_spec.initial_state
    end

    def write_initial_state
      write_attribute(self.class.workflow_column, current_state.value)
    end

    def process_event!(name, *args, **kwargs)
      event = current_state.events.fetch(name, []).detect do |event|
        event.condition_applicable?(self) && event
      end

      if event.nil?
        return run_on_unavailable_transition(current_state, name, *args, **kwargs)
      end

      @halted_because = nil
      @halted = false

      check_transition(event)

      from = current_state
      to_state = self.class.workflow_spec.states[event.transitions_to]
      to_value = to_state.value

      run_before_transition(from, to_state, name, *args, **kwargs)
      return false if @halted

      return_value = run_action(event.action, *args, **kwargs) || run_action_callback(event.name, *args, **kwargs)

      return false if @halted

      run_on_transition(from, to_state, name, *args, **kwargs)

      run_on_exit(from, to_state, name, *args)

      transition_value = persist_workflow_state(to_value)

      run_on_entry(to_state, from, name, *args, **kwargs)

      run_after_transition(from, to_state, name, *args, **kwargs)

      return_value.nil? ? transition_value : return_value
    end

    def persist_workflow_state(new_value)
      update_column(self.class.workflow_column, new_value)
    end


    def halt(reason = nil)
      @halted_because = reason
      @halted = true
    end

    def halt!(reason = nil)
      @halted_because = reason
      @halted = true
      raise TransitionHalted.new(reason)
    end

    def halted?
      @halted
    end

    def halted_because
      @halted_because
    end

    def check_transition(event)
      # Create a meaningful error message instead of
      # "undefined method `on_entry' for nil:NilClass"
      # Reported by Kyle Burton
      if !self.class.workflow_spec.states[event.transitions_to]
        raise WorkflowError.new("Event[#{event.name}]'s " +
                                "transitions_to[#{event.transitions_to}] is not a declared state.")
      end
    end

    def run_before_transition(from, to, event, *args, **kwargs)
      instance_exec(from.name, to.name, event, *args, **kwargs, &self.class.workflow_spec.before_transition_proc) if
        self.class.workflow_spec.before_transition_proc
    end

    def run_on_unavailable_transition(from, to_name, *args, **kwargs)
      if !self.class.workflow_spec.on_unavailable_transition_proc || !instance_exec(from.name, to_name.to_sym, *args, &self.class.workflow_spec.on_unavailable_transition_proc)
        raise NoTransitionAllowed.new("There is no event #{to_name.to_sym} defined for the #{current_state} state")
      end
    end

    def run_on_transition(from, to, event, *args, **kwargs)
      instance_exec(from.name, to.name, event, *args, **kwargs, &self.class.workflow_spec.on_transition_proc) if self.class.workflow_spec.on_transition_proc
    end

    def run_after_transition(from, to, event, *args, **kwargs)
      instance_exec(from.name, to.name, event, *args, **kwargs, &self.class.workflow_spec.after_transition_proc) if
        self.class.workflow_spec.after_transition_proc
    end

    def run_action(action, *args, **kwargs)
      instance_exec(*args, **kwargs, &action) if action
    end

    def has_callback?(action)
      # 1. public callback method or
      # 2. protected method somewhere in the class hierarchy or
      # 3. private in the immediate class (parent classes ignored)
      action = action.to_sym
      self.respond_to?(action) ||
        self.class.protected_method_defined?(action) ||
        self.private_methods(false).map(&:to_sym).include?(action)
    end

    def run_action_callback(action_name, *args, **kwargs)
      action = action_name.to_sym
      self.send(action, *args, **kwargs) if has_callback?(action)
    end

    def run_on_entry(state, prior_state, triggering_event, *args, **kwargs)
      if state.on_entry
        instance_exec(prior_state.name, triggering_event, *args, **kwargs, &state.on_entry)
      else
        hook_name = "on_#{state}_entry"
        self.send hook_name, prior_state, triggering_event, *args, **kwargs if has_callback?(hook_name)
      end
    end

    def run_on_exit(state, new_state, triggering_event, *args, **kwargs)
      if state
        if state.on_exit
          instance_exec(new_state.name, triggering_event, *args, **kwargs, &state.on_exit)
        else
          hook_name = "on_#{state}_exit"
          self.send hook_name, new_state, triggering_event, *args, **kwargs if has_callback?(hook_name)
        end
      end
    end
  end

  class Event
    attr_accessor :name, :transitions_to, :meta, :action, :condition

    def initialize(name, transitions_to, condition = nil, meta = {}, &action)
      @name = name
      @transitions_to = transitions_to.to_sym
      @meta = meta
      @action = action
      @condition =
        if condition.nil? || condition.is_a?(Symbol) || condition.respond_to?(:call)
          condition
        else
          raise TypeError, "condition must be nil, an instance method name symbol or a callable (eg. a proc or lambda)"
        end
    end

    def condition_applicable?(object)
      if condition
        object.send(condition)
      else
        true
      end
    end

    def to_s
      @name.to_s
    end
  end

  class Specification
    attr_accessor :states, :initial_state, :meta,
                  :on_transition_proc, :before_transition_proc,
                  :after_transition_proc, :on_unavailable_transition_proc

    def initialize(meta = {}, &specification)
      @states = Hash.new
      @meta = meta
      instance_eval(&specification)
    end

    def state_names
      @states.keys
    end

    private

    def state(name, value = nil, options = {}, &events_and_etc)
      value ||= name

      new_state = Workflow::State.new(name, value, self, options[:meta])
      @initial_state = new_state if @states.empty?
      @states[name] = new_state
      @scoped_state = new_state
      instance_eval(&events_and_etc) if events_and_etc
    end

    def event(name, transitions_to:, **kwargs, &action)
      @scoped_state.events[name] ||= []
      @scoped_state.events[name] << Workflow::Event.new(name, transitions_to, kwargs[:if], (kwargs[:meta] || {}), &action)
    end

    def on_entry(&proc)
      @scoped_state.on_entry = proc
    end

    def on_exit(&proc)
      @scoped_state.on_exit = proc
    end

    def after_transition(&proc)
      @after_transition_proc = proc
    end

    def before_transition(&proc)
      @before_transition_proc = proc
    end

    def on_transition(&proc)
      @on_transition_proc = proc
    end

    def on_unavailable_transition(&proc)
      @on_unavailable_transition_proc = proc
    end
  end

  class State
    include Comparable
    attr_accessor :name, :value, :events, :meta, :on_entry, :on_exit

    def initialize(name, value, spec, meta = {})
      @name, @value, @spec, @events, @meta = name, value, spec, Hash.new, meta
    end

    def <=>(other_state)
      states = @spec.states.keys
      raise ArgumentError, "state `#{other_state}' does not exist" unless states.include?(other_state.to_sym)
      states.index(self.to_sym) <=> states.index(other_state.to_sym)
    end

    def to_s
      "#{name}"
    end

    def to_sym
      name.to_sym
    end
  end
end
