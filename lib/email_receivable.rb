# rubocop:disable Style/FrozenStringLiteralComment

module EmailReceivable
  def logged_email
    if @logged_email.nil?
      @logged_email = formatter == :email &&
        CommentEmail.where("comment_id=? and comment_type=?", id, self.class.name).first
    end
    @logged_email
  end

  def formatter
    fmt = read_attribute(:formatter)
    fmt.blank? ? :markdown : fmt.to_sym
  end

  def created_via_email
    formatter == :email
  end
end
