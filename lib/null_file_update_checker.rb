# frozen_string_literal: true

# A stub FileUpdateChecker which assumes that the FS will never change
# We should be able to remove this in Rails 6.1
class NullFileUpdateChecker
  def initialize(files, dirs, &block)
    @block = block
  end

  def execute
    @block.call
  end

  def updated?
    false
  end

  def execute_if_updated
    false
  end
end
