# frozen_string_literal: true

require "json"

class AssetBundles
  def self.get
    @@cache ||= self.new
  end

  def self.cache_clear
    @@cache = nil
  end

  class CachePerRequest
    def initialize(app)
      @app = app
    end

    def call(env)
      AssetBundles::cache_clear
      @app.call(env)
    end
  end

  def initialize(assets: "public/assets", package: "package.json")
    @package_path = Rails.root.join(package)
    assets = Rails.root.join(assets)
    @manifest_path = assets.join("manifest.json").to_s
    @integrities_path = assets.join("integrities").to_s
  end

  def data
    return @data if defined? @data
    @data = if File.exist?(@manifest_path)
      JSON.parse(File.read(@manifest_path))
    end
  end

  def integrities
    return @integrities if defined? @integrities
    @integrities = if File.exist?(@integrities_path)
      Hash[
        File.read(@integrities_path)
          .split("\n")
          .map { |line| line.split(" ").reverse }
      ]
    end
  end

  def tests
    @tests ||= package.fetch("test-bundles").values.freeze
  end

  # Finds the top-level script bundles produced by Rollup during
  # compilation. Chunk files, output from Rollup's code splitting algorithm,
  # are not listed here.
  #
  # Examples
  #
  #   AssetBundles.new.bundles
  #   # => ["github.js", "projects.js", ...]
  #
  # Returns an Array<String> of bundle names.
  def bundles
    @bundles ||= package.fetch("main-bundles").values.freeze
  end

  # Finds the curated, named chunk files we maintain to avoid dynamically
  # splitting code into many small files. These chunk files are served with
  # static script tags just like top-level bundles so the browser fetches them
  # in parallel.
  #
  # Examples
  #
  #   AssetBundles.new.named_chunks
  #   # => ["frameworks.js", "vendor.js", ...]
  #
  # Returns Array<String> of curated chunk file names.
  def named_chunks
    @named_chunks ||= package.fetch("named-chunks").keys.freeze
  end

  # Finds the chunk files output from Rollup's code splitting algorithm. Every
  # script listed in public/assests/manifest.json is either a top-level bundle
  # or a chunk file.
  #
  # Examples
  #
  #   AssetBundles.new.dynamic_chunks
  #   # => ["randomColor.js", ...]
  #
  # Returns Array<String> of chunk file names.
  def dynamic_chunks
    @dynamic_chunks ||= data.keys.select do |bundle|
      bundle.ends_with?(".js") &&
        !named_chunks.include?(bundle) &&
        !bundles.include?(bundle) &&
        !tests.include?(bundle)
    end.freeze
  end

  def bundle_exists?(name)
    if name.end_with?(".js")
      bundles.include?(name)
    else
      data.key?(name)
    end
  end

  # Expands the script bundle's short name into a fingerprinted file name
  # to be served in production. The fingerprint is a content addressable hash
  # of the file's content.
  #
  # source - The bundle's short name (e.g. github.js).
  #
  # Examples
  #
  #   AssetBundles.new.expand_bundle_name("github.js")
  #   # => "github-bootstrap-deadbeef.js"
  #
  # Returns a fingerprinted file name.
  def expand_bundle_name(source)
    if Rails.env.development?
      source
    elsif data
      return data.fetch(source) do
        if Rails.env.test? && ENV["GITHUB_CI"].nil?
          # When running tests locally, asset_dir might be in a mixed state
          source
        else
          raise KeyError.new("key %p not found in %p" % [
            source,
            @manifest_path.sub("#{Rails.root}/", ""),
          ])
        end
      end
    else
      return source
    end
  end

  # Returns the bundle's subresource integrity (SRI) hash to be included in
  # a `<script>` tag.
  #
  # The browser will not execute the script if the declared integrity hash
  # does not match the hash it calculates from the bytes served. This
  # prevents a CDN host from changing script contents after upload.
  #
  # filename - The bundle's short name (e.g. github.js)
  #
  # Returns an SRI hash String for the bundle.
  def integrity(filename)
    if integrities
      integrities[filename] || integrities[expand_bundle_name(filename)]
    end
  end

  # Returns the script bundle's full URL on the CDN.
  #
  # source - A String bundle short name to expand (e.g. github.js).
  #
  # Examples
  #
  #   AssetBundles.new.bundle_url("github.js")
  #   # => "https://github.githubassets.com/assets/github-deadbeef.js"
  #   # => "http://assets.github.localhost/assets/github.js"
  #
  # Returns a URL string.
  def bundle_url(source)
    File.join(GitHub.asset_host_url, "assets", expand_bundle_name(source))
  end

  private

  def package
    @package ||= JSON.parse(File.read(@package_path))
  end
end
