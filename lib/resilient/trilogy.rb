# rubocop:disable Style/FrozenStringLiteralComment

require "resilient/circuit_breaker"
require "trilogy"
require "delegate"

module Resilient
  class Trilogy < SimpleDelegator
    # Base error class for errors. Inherit from Trilogy::Error so any rescues of
    # that in the code base that already exist will "just work".
    Error = Class.new(::Trilogy::Error)
    CircuitOpenError = Class.new(Error)

    # Whitelist of queries that are not protected.
    QueryWhitelist = Regexp.union(
      /\A\s*ROLLBACK/i,
      /\A\s*COMMIT/i,
      /\A\s*RELEASE\s+SAVEPOINT/i,
    )

    # Public: The Hash passed to Trilogy.new.
    attr_reader :trilogy_options

    # Public: The Hash passed to Resilient::CircuitBreaker::Properties.
    attr_reader :resilient_properties

    # Internal: The circuit breaker protecting network calls.
    attr_reader :circuit_breaker

    # Public: Initializes a new Resilient::Trilogy wrapper and the Trilogy instance
    # it wraps.
    #
    # trilogy_options - The Hash (symbol keys) of options to pass to Trilogy.new.
    # resilient_properties - The Hash (symbol keys) of options to pass to the
    #                     Resilient::CircuitBreaker::Properties.
    def initialize(trilogy_options:, resilient_properties:)
      @trilogy_options = trilogy_options
      @resilient_properties = resilient_properties

      name = @resilient_properties.fetch(:name) {
        raise ArgumentError, "resilient_properties requires name"
      }

      @circuit_breaker = Resilient::CircuitBreaker.get(name, @resilient_properties)

      acquire {
        @trilogy = ::Trilogy.new(@trilogy_options)
        super(@trilogy)
      }
    end

    def query(*args)
      if query_whitelisted?(*args)
        @trilogy.query(*args)
      else
        acquire { @trilogy.query(*args) }
      end
    end

    def change_db(*args)
      acquire { @trilogy.change_db(*args) }
    end

    def ping
      acquire { @trilogy.ping }
    end

    private

    def query_whitelisted?(sql, *)
      QueryWhitelist =~ sql
    rescue ArgumentError
      # If the sql contains invalid data for the encoding, we will get an
      # ArgumentError.  Rescue it and return false (the query *is not*
      # whitelisted)
      return false unless sql.valid_encoding?
      raise
    end

    def acquire(&block)
      if @circuit_breaker.allow_request?
        begin
          result = yield
          @circuit_breaker.success
          result
        rescue ::SystemCallError => boom
          @circuit_breaker.failure
          raise
        rescue ::Trilogy::Error => boom
          if trilogy_adapter_lost_connection_exception?(boom)
            @circuit_breaker.failure
          else
            @circuit_breaker.success
          end
          raise
        rescue => boom
          @circuit_breaker.success
          raise
        end
      else
        raise Resilient::Trilogy::CircuitOpenError
      end
    end

    def trilogy_adapter_lost_connection_exception?(exception)
      trilogy_adapter_exception = TrilogyAdapter::LostConnectionExceptionTranslator.
        new(exception, exception.message, nil).
        translate

      ActiveRecord::ConnectionAdapters::TrilogyAdapter::LOST_CONNECTION_ERRORS.
        include?(trilogy_adapter_exception.class)
    end
  end
end
