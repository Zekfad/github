# rubocop:disable Style/FrozenStringLiteralComment

module LikeQuery
  # Query for values with the specified prefix.
  #
  # column - The column to query.
  # prefix - The value prefix to query for.
  #
  # Returns an ActiveRecord::Relation
  def with_prefix(column, prefix)
    where("#{column} LIKE ?", "#{ActiveRecord::Base.sanitize_sql_like(prefix)}%")
  end

  # Query for values with the specified suffix.
  #
  # column - The column to query.
  # suffix - The value suffix to query for.
  #
  # Returns an ActiveRecord::Relation
  def with_suffix(column, suffix)
    where("#{column} LIKE ?", "%#{ActiveRecord::Base.sanitize_sql_like(suffix)}")
  end

  # Query for values with the specified substring.
  #
  # column    - The column to query.
  # substring - The value substring to query for.
  #
  # Returns an ActiveRecord::Relation
  def with_substring(column, substring)
    where("#{column} LIKE ?", "%#{ActiveRecord::Base.sanitize_sql_like(substring)}%")
  end
end
