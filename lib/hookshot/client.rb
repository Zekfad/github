# rubocop:disable Style/FrozenStringLiteralComment

require "mochilo"

module Hookshot
  class Client
    include Scientist

    CONTENT_TYPE = "application/vnd.hookshot+zpack".freeze
    RAW_CONTENT_TYPE = "application/vnd.hookshot+json".freeze
    PARENTS_USING_STAGING = {
      "repository-7550011" => "github/hookshot",
      "repository-66868732" => "github/hookfire",
      "repository-75867826" => "api-playground/hookshot-staging",
    }.freeze

    class HookshotGoFaradayError < StandardError; end

    attr_reader :options, :faraday, :parent

    def self.secure_token(guid, hook_id, expires)
      OpenSSL::HMAC.hexdigest("sha1", GitHub.hookshot_token, "%d/%s/%s" % [
        expires, guid, hook_id])
    end

    def self.secure_token_matches?(token, guid, hook_id, expires)
      return false unless expires.to_i >= Time.now.to_i
      actual = secure_token(guid, hook_id, expires.to_i)
      SecurityUtils.secure_compare(token, actual)
    end

    def self.for_parent(parent)
      host = if PARENTS_USING_STAGING.key?(parent) && GitHub.staging_hookshot_url.present?
        GitHub.staging_hookshot_url
      else
        GitHub.hookshot_url
      end

      self.new host, parent: parent
    end

    def initialize(host, options = {})
      @parent  = options.delete(:parent)
      @options = options.reverse_merge({
        timeout_threshold: 60,
      })

      @faraday = build_faraday(host)
    end

    def deliver(payload)
      start = Time.now
      path = File.join(String(GitHub.hookshot_path), "/hooks")
      payload = payload.with_indifferent_access
      delivery_guid = payload[:guid]

      expires = 1.minute.from_now.to_i

      response = post(path,
                      payload,
                      { token: Hookshot::Client.secure_token(delivery_guid, :delivery, expires),
                        expires: expires},
                        encode_payload: true,
                        use_go: GitHub.hookshot_go_enabled?,
                      )
      res_status, response_body = response.status, response.body.try(:strip)

      [res_status, response_body]
    ensure
      ms = ((Time.now - start) * 1000).round
      GitHub.dogstats.timing("rpc.hookshot.time", ms, tags: ["rpc_operation:deliver", "status:#{res_status}"])
    end

    def redeliver(delivery_guid, hook_id, hook_data)
      start = Time.now
      path = "/logs/%s/%s/%s/redeliver" % [parent, delivery_guid, hook_id]
      path = File.join(String(GitHub.hookshot_path), path)

      expires = 1.minute.from_now.to_i
      response = post(path,
                      hook_data,
                      { token: Hookshot::Client.secure_token(delivery_guid, hook_id, expires),
                        expires: expires },
                      encode_payload: true)

      res_status, response_body = response.status, response.body.try(:strip)
    ensure
      ms = ((Time.now - start) * 1000).round
      GitHub.dogstats.timing("rpc.hookshot.time", ms, tags: ["rpc_operation:redeliver", "status:#{res_status}"])
    end

    def logs_for_hook(hook_id, params = {})
      start = Time.now
      path = File.join(String(GitHub.hookshot_path), "/logs")

      expires = 2.minutes.from_now.to_i
      response = get path, params.merge(hook_id: hook_id, token: Hookshot::Client.secure_token(nil, hook_id, expires), expires: expires), use_go: false

      res_status, response_body = get_status_and_body(response)
    ensure
      ms = ((Time.now - start) * 1000).round
      GitHub.dogstats.timing("rpc.hookshot.time", ms, tags: ["rpc_operation:logs_for_hook", "status:#{res_status}"])
    end

    def deliveries_for_hook(hook_id, params = {})
      res_status = nil
      start = Time.now
      path = File.join(String(GitHub.hookshot_path), "/deliveries")

      expires = 2.minutes.from_now.to_i
      request_args = params.merge(hook_id: hook_id, token: Hookshot::Client.secure_token(params.with_indifferent_access["guid"], hook_id, expires), expires: expires)
      response = get path, request_args, use_go: GitHub.hookshot_go_enabled?
      res_status, response_body = response.status, JSON.parse(response.body)
    ensure
      ms = ((Time.now - start) * 1000).round
      GitHub.dogstats.timing("rpc.hookshot.time", ms, tags: ["rpc_operation:deliveries_for_hook", "status:#{res_status}"])
    end

    def create_redelivery_attempt(delivery_id, hook_id, config)
      start = Time.now
      path  = File.join(String(GitHub.hookshot_path), "/deliveries/#{delivery_id}/attempts")

      expires = 2.minutes.from_now.to_i
      token   = Hookshot::Client.secure_token(nil, hook_id, expires)

      if GitHub.hookshot_go_enabled?
        hook = Hook.find_by!(id: hook_id)

        payload = {
          hook_id: hook.id,
          parent: hook.hookshot_parent_id,
          hook_data: config,
          hook_configuration: {
            needs_public_key_signature: Hook::DeliverySystem.needs_public_key_signature?(hook)
          },
          github_request_id: GitHub.context[:request_id],
          enqueued_at: (Time.now.to_f * 1_000).round
        }
      else
        payload = { config: config }
      end

      response = post(
        path,
        payload,
        { token: token, expires: expires, hook_id: hook_id },
        encode_payload: false,
        use_go: GitHub.hookshot_go_enabled?
      )

      if response.body.empty?
        body = response.body
      else
        body = JSON.parse(response.body)
      end

      res_status, response_body = response.status, body
    ensure
      ms = ((Time.now - start) * 1000).round
      GitHub.dogstats.timing("rpc.hookshot.time", ms, tags: ["rpc_operation:create_redelivery_attempt", "status:#{res_status}"])
    end

    def delivery_for_hook(delivery_id, hook_id, params = {})
      res_status = nil
      start = Time.now
      path = File.join(String(GitHub.hookshot_path), "/deliveries/#{delivery_id}")
      expires = 2.minutes.from_now.to_i
      request_args = params.merge(hook_id: hook_id, token: Hookshot::Client.secure_token(nil, hook_id, expires), expires: expires)

      response = get path, request_args, use_go: GitHub.hookshot_go_enabled?
      res_status, response_body = response.status, JSON.parse(response.body)
    ensure
      ms = ((Time.now - start) * 1000).round
      GitHub.dogstats.timing("rpc.hookshot.time", ms, tags: ["rpc_operation:delivery_for_hook", "status:#{res_status}"])
    end

    def statuses_for_hooks(hook_ids)
      res_status = nil
      start = Time.now
      path = File.join(String(GitHub.hookshot_path), "/statuses")

      expires = 1.minute.from_now.to_i
      request_args = { hook_ids: hook_ids.join(","), token: Hookshot::Client.secure_token(nil, nil, expires), expires: expires }

      if GitHub.hookshot_go_enabled?
        response = get path, request_args, use_go: true
        res_status, response_body = response.status, response.body
        response_body = JSON.parse(response_body) if res_status == 200
        [res_status, response_body]
      else
        response = get path, request_args, use_go: false
        res_status, response_body = get_status_and_body(response)
      end
    ensure
      ms = ((Time.now - start) * 1000).round
      GitHub.dogstats.timing("rpc.hookshot.time", ms, tags: ["rpc_operation:statuses_for_hooks", "status:#{res_status}"])
    end

    private

    def faraday_for_go
      @faraday_go ||= if PARENTS_USING_STAGING.key?(parent) && GitHub.staging_hookshot_go_url.present?
        build_faraday(GitHub.staging_hookshot_go_url)
      else
        build_faraday(GitHub.hookshot_go_url)
      end
    end

    def get(path, params = {}, use_go:)
      faraday_client = use_go ? faraday_for_go : faraday
      GitHub::Timer.timeout(options[:timeout_threshold], Faraday::TimeoutError) do
        faraday_client.get path do |req|
          if GitHub.context[:request_id]
            req.headers["X-GitHub-Request-Id".freeze] = GitHub.context[:request_id]
          end

          if GitHub.tracer.last_span.present?
            GitHub.tracer.last_span.when_enabled do |span|
              GitHub.tracer.inject(span.context, OpenTracing::FORMAT_TEXT_MAP, req.headers)
            end
          end

          req.params.update params
        end
      end
    end

    def payload_size_headers(full_payload, encoded_payload)
      size = full_payload.to_json.bytesize
      string_to_sign = [size, encoded_payload].join("/")
      token = OpenSSL::HMAC.hexdigest("sha1", GitHub.hookshot_token, string_to_sign)
      {
        "X-GitHub-Content-Length" => size.to_s,
        "X-GitHub-Content-Length-Token" => token,
      }
    end

    def post_with_faraday_client(faraday_client, path, params, body, payload, encode_payload)
      faraday_client.post path do |req|
        req.headers[:content_type] = encode_payload ? CONTENT_TYPE : RAW_CONTENT_TYPE
        req.body = body
        req.headers.update(payload_size_headers(payload, body))

        if GitHub.context[:request_id]
          req.headers["X-GitHub-Request-Id".freeze] = GitHub.context[:request_id]
        end

        if GitHub.tracer.last_span.present?
          GitHub.tracer.last_span.when_enabled do |span|
            GitHub.tracer.inject(span.context, OpenTracing::FORMAT_TEXT_MAP, req.headers)
          end
        end

        req.params.update params
      end
    end

    def post(path, payload, params = {}, encode_payload:, use_go: false)
      GitHub::Timer.timeout(options[:timeout_threshold], Faraday::TimeoutError) do

        if use_go
          body = encode_payload ? encode_body(payload, use_go: true) : payload.to_json
          result = post_with_faraday_client(faraday_for_go, path, params, body, payload, encode_payload)
        else
          body = encode_payload ? encode_body(payload) : payload.to_json
          result = post_with_faraday_client(faraday, path, params, body, payload, encode_payload)
        end

        result
      end
    end

    def encode_body(payload, use_go: false)
      # Log the bytesize of the raw payload before encoding it
      action = payload["payload"] && payload["payload"]["action"] ? payload["payload"]["action"] : nil
      event_type = payload["event"]
      event_tag = "#{event_type}"
      event_tag += "_#{action}" if action
      tags = ["event:#{event_tag}", "event_type:#{event_type}"]
      payload_serializer = use_go ? go_serializer : serializer
      payload =  use_go ? payload.to_json : payload

      GitHub.dogstats.histogram("hooks.raw_payload_size".freeze, payload.to_json.bytesize, tags: tags)

      GitHub.dogstats.time("hooks.time", tags: tags + ["operation:encode_payload"]) do
        payload_serializer.dump(payload).tap do |encoded|
          GitHub.dogstats.histogram("hooks.payload_size", encoded.size, tags: tags)
        end
      end
    end

    def decode_body(encoded)
      GitHub.dogstats.time("hookshot.decode") do
        GitHub.dogstats.count("hookshot.encoded_size", encoded.size)
        serializer.load(encoded)
      end
    end

    def serializer
      @serializer ||= Coders.compose(Mochilo, Coders::ZSTREAM, default: {})
    end

    def go_serializer
      @go_serializer ||= Coders.compose(Coders::ZSTREAM, default: {})
    end

    def build_faraday(url)
      options = {
        url: url,
        # i hate to turn ssl verify off, but...
        # a) only hookshot is ssl
        # 2) it's only on staging, and we don't have proper certs setup yet
        ssl: {verify: false},
        request: {
          timeout: self.options[:timeout_threshold],
          open_timeout: 5,
        },
      }

      Faraday.new(options) do |b|
        b.request :url_encoded
        b.adapter :net_http
      end
    end

    def get_status_and_body(response)
      res_status, response_body = response.status, response.body
      response_body = decode_body(response_body) if res_status == 200
      [res_status, response_body]
    end
  end
end
