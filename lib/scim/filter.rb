# rubocop:disable Style/FrozenStringLiteralComment

module SCIM
  module Filter
    class InvalidFilterError < StandardError
      def message
        "Only single 'eq' operators are supported."
      end
    end

    INTERNAL_ID_ATTRIBUTE = "id".freeze

    # Internal: Pattern used for single attribute equal queries.
    #
    # http://rubular.com/r/2x14s33IFQ
    #
    # Example usage:
    #
    #   EQ_FILTER_REGEX.match('userName eq "hubot"')
    #   # => #<MatchData name:"userName" value:"hubot">
    EQ_FILTER_REGEX = %r{
      \A                # Beginning of string
      (?<name>\w+)      # [:name] Attribute name (any word character)
      \seq\s            # Equal operator
      "                 # Open quote
      (?<value>[^"]+)   # [:value] Attribute value (anything but a close quote)
      "                 # Close quote
      \z                # End of string
    }xi                 # x: ignore whitepace, i: ignore case

    # Public: Takes a SCIM filter and applies its conditions to the specified
    # ExternalIdentity scope.
    #
    # Currently we only support single conditions using the equal operator.
    # If the passed filter is not supported, a 'none' scope will be returned.
    #
    # filter    - The String SCIM filter
    # scope     - (Optional) The scoped ExternalIdentity relation to apply
    #             the filter to.
    #
    # Returns a scoped ExternalIdentity relation.
    def self.apply(filter, scope: ExternalIdentity.scoped)
      name, value = parse_eq_filter(filter)
      raise InvalidFilterError unless name && value

      # Filters can either be on IdP provided attributes or an internal
      # identifier we gave the SCIM client during provisioning. In our
      # case, we use the identity's guid as our internal identifier.
      if name == INTERNAL_ID_ATTRIBUTE
        scope.where(guid: value)
      else
        user_data = Platform::Provisioning::UserData.new
        user_data.append(name, value)

        scope.by_scim_user_data(user_data)
      end
    end

    # Internal: Extracts attribute name and value to filter on from a SCIM
    # string. If filter is not a simple equals query, nothing will be returned.
    #
    # Examples:
    #
    #   parse_eq_filter('userName eq "hubot"')
    #   # => [ "userName", "hubot" ]
    #
    #   parse_eq_filter('userName eq "hubot" and emails eq "hubot@github.com"')
    #   # => nil
    #
    #   parse_eq_filter('emails co "hubot"')
    #   # => nil
    #
    # Returns a Array containing parsed name and value if matched or nothing.
    def self.parse_eq_filter(filter)
      match = EQ_FILTER_REGEX.match(filter)
      match && [match[:name], match[:value]]
    end

  end
end
