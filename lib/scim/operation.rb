# frozen_string_literal: true

module SCIM
  class Operation
    # Defines regexes for determining attribute type. Knowing the type
    # tells us how to go about setting or updating the attribute.
    SINGLE_VALUE_ATTRIBUTE_REGEX     = /\A(userName|externalId)\z/
    BOOLEAN_VALUE_ATTRIBUTE_REGEX    = /\A(active)\z/
    COMPLEX_VALUE_ATTRIBUTE_REGEX    = /\A(name)\z/
    COMPLEX_VALUE_SUBATTRIBUTE_REGEX = /\Aname\.(given|family)Name\z/
    MULTI_VALUE_ATTRIBUTE_REGEX      = /\A(emails)\z/
    GROUPS_VALUE_ATTRIBUTE_REGEX     = /\A(groups|http:\/\/schemas.microsoft.com\/ws\/2008\/06\/identity\/claims\/groups)\z/

    # Internal: Applies a SCIM operation to the user data.
    # Does not mutate the passed in UserData object.
    #
    # Example:
    #
    #   apply user_data \
    #     "op" => "add",
    #     "path" => "userName",
    #     "value" => "new-user-name"
    #
    # input_user_data - The UserData to apply the operation to. This
    #                   object will not be mutated.
    # operation       - A SCIM operation Hash.
    #
    # Returns the copy of UserData with the operation applied.
    def self.apply(input_user_data, operation)
      expand_operation(operation).each_with_object(input_user_data.dup) do |op, user_data|
        case op["op"]
        when "add"
          apply_add_operation(user_data, op)
        when "remove"
          apply_remove_operation(user_data, op)
        when "replace"
          apply_replace_operation(user_data, op)
        end
      end
    end

    # Internal: Applies a SCIM 'add' operation to the user data.
    #
    # https://tools.ietf.org/html/rfc7644#section-3.5.2.1
    #
    # Example:
    #
    #   apply_add_operation user_data, \
    #     "op" => "add",
    #     "path" => "userName",
    #     "value" => "new-user-name"
    #
    # user_data   - The UserData to apply the operation to.
    # operation   - A SCIM operation Hash.
    #
    # Returns the UserData with the operation applied.
    def self.apply_add_operation(user_data, operation)
      return user_data unless operation["op"] == "add"

      case path = operation["path"]
      when SINGLE_VALUE_ATTRIBUTE_REGEX
        user_data.replace(path, operation["value"])
      when BOOLEAN_VALUE_ATTRIBUTE_REGEX
        user_data.replace(path, operation["value"].to_s)
      when COMPLEX_VALUE_ATTRIBUTE_REGEX
        operation["value"].each do |subattribute, value|
          user_data.replace("#{ path }.#{ subattribute }", value)
        end
      when COMPLEX_VALUE_SUBATTRIBUTE_REGEX
        user_data.replace(path, operation["value"])
      when MULTI_VALUE_ATTRIBUTE_REGEX, GROUPS_VALUE_ATTRIBUTE_REGEX
        operation["value"].each do |value|
          value = Hash[*value][operation["path"]] if value.is_a?(Array)

          value = [value] unless value.is_a?(Array)
          value.each do |val|
            demote_primary_multi_value(user_data, path) if val["primary"]

            user_data.append(path, val["value"], val.except("value"))
          end
        end
      end

      return user_data
    end

    # Internal: Applies a SCIM 'remove' operation to the user data.
    #
    # https://tools.ietf.org/html/rfc7644#section-3.5.2.2
    #
    # Example:
    #
    #   apply_remove_operation user_data, \
    #     "op" => "remove",
    #     "path" => "name.familyName",
    #
    # user_data   - The UserData to apply the operation to.
    # operation   - A SCIM operation Hash.
    #
    # Returns the UserData with the operation applied.
    def self.apply_remove_operation(user_data, operation)
      return user_data unless operation["op"] == "remove"

      case path = operation["path"]
      when SINGLE_VALUE_ATTRIBUTE_REGEX
        user_data.delete(path)
      when BOOLEAN_VALUE_ATTRIBUTE_REGEX
        user_data.delete(path)
      when COMPLEX_VALUE_ATTRIBUTE_REGEX
        user_data.delete_if { |attr| attr["name"].starts_with?("#{ path }.") }
      when COMPLEX_VALUE_SUBATTRIBUTE_REGEX
        user_data.delete(path)
      when MULTI_VALUE_ATTRIBUTE_REGEX
        user_data.delete_all(path)
      when GROUPS_VALUE_ATTRIBUTE_REGEX
        groups_key = operation.dig("meta", "groups_path_key") || "groups"
        groups_to_remove = operation.dig("value", groups_key).map { |group| group.dig("value") }
        user_data.delete_if { |attr| attr["name"] == groups_key && groups_to_remove.include?(attr["value"]) }
      end

      return user_data
    end

    # Internal: Applies a SCIM 'replace' operation to the user data.
    #
    # https://tools.ietf.org/html/rfc7644#section-3.5.2.3
    #
    # Example:
    #
    #   apply_add_operations user_data, \
    #     "op" => "replace",
    #     "path" => "emails",
    #     "value" => [
    #       { "value" => "new-email@example.com" }
    #     ]
    #
    # user_data   - The UserData to apply the operation to.
    # operation   - A SCIM operation Hash.
    #
    # Returns the UserData with the operation applied.
    def self.apply_replace_operation(user_data, operation)
      return user_data unless operation["op"] == "replace"

      case path = operation["path"]
      when SINGLE_VALUE_ATTRIBUTE_REGEX
        user_data.replace(path, operation["value"])
      when BOOLEAN_VALUE_ATTRIBUTE_REGEX
        user_data.replace(path, operation["value"].to_s)
      when COMPLEX_VALUE_ATTRIBUTE_REGEX
        user_data.delete_if { |attr| attr["name"].starts_with?("#{ path }.") }
        operation["value"].each do |subattribute, value|
          user_data.append("#{ path }.#{ subattribute }", value)
        end
      when COMPLEX_VALUE_SUBATTRIBUTE_REGEX
        user_data.replace(path, operation["value"])
      when MULTI_VALUE_ATTRIBUTE_REGEX, GROUPS_VALUE_ATTRIBUTE_REGEX
        user_data.delete_all(path)
        operation["value"].each do |value|
          user_data.append(path, value["value"], value.except("value"))
        end
      end

      return user_data
    end

    # Internal: Expands bundled SCIM operations into a series of simple operations. This
    # allows the apply logic to focus on a single format.
    #
    # SCIM allows bundling multiple "add" or "replace" operations into a single operation hash.
    # For example the following two operation payloads are equivalent:
    #
    # # Bundled
    #
    #   {"Operations": [
    #     {
    #       "op":"add",
    #       "value": {
    #         "emails":[
    #           { "value":"babs@jensen.org", "type":"home" }
    #         ],
    #         "nickname":"Babs"
    #       }
    #     }
    #   ]}
    #
    # # Expanded
    #   {"Operations":[
    #     {
    #       "op":"add",
    #       "path": "emails",
    #       "value":[
    #         { "value":"babs@jensen.org", "type":"home" }
    #       ]
    #     },
    #     {
    #       "op": "add",
    #       "path": "nickname",
    #       "value": "Babs"
    #     }
    #   ]}
    #
    # Returns an Array of single operation Hashes.
    def self.expand_operation(operation)
      # AAD sends ops titleized, `Add` versus `add`
      operation["op"] = operation.dig("op")&.downcase
      if %w(add replace).include?(operation["op"]) && operation["path"].blank?
        operation["value"].map do |path, value|
          {
            "op"    => operation["op"],
            "path"  => path,
            "value" => value,
          }
        end
      else
        [operation]
      end
    end

    # Internal: Demotes the current primary item, if any, for a multi-value attribute.
    #
    # user_data  - The UserData containing the attributes.
    # attribute  - The multi-value attribute name.
    #
    # Returns nothing.
    def self.demote_primary_multi_value(user_data, attribute)
      existing_primary = user_data.fetch_all(attribute).detect { |attr| attr["metadata"]["primary"] }

      existing_primary["metadata"].delete("primary") if existing_primary
    end
  end
end
