# frozen_string_literal: true

module SecurityScriptsDependencies
  class ResetPasswords < GitHub::Transition

    attr_reader :dry_run, :verbose, :data, :batch_size

    def initialize(dry_run: true, verbose: false, data: [], batch_size: 10, **_)
      @dry_run = dry_run
      @verbose = verbose
      @data = data # ex [{user_id: 1, other: :data}, {user_id: 2, other: :data}]
      @batch_size = batch_size
    end

    def perform
      log "Start password resets"
      total_count = 0

      readonly do
        throttle do
          data.each_slice(batch_size) do |batch|
            grouped_batch = batch.group_by { |item| item[:user_id].to_i }
            users = User.where(id: grouped_batch.keys).includes(:sessions).to_a

            users.each do |user|
              reset_password user
              user_data = grouped_batch[user.id].first.clone
              user_data[:user] = user
              user_data[:login] = user.login

              yield user_data if block_given?
            end

            total_count += users.count
            log "Processed #{total_count} users so far" if verbose
          end
        end
      end

      log "Finished password resets, processed #{total_count} records."
    end

    def reset_password(user)
      success = if dry_run
        true
      else
        ActiveRecord::Base.connected_to(role: :writing) { user.set_random_password(actor: nil) }
      end

      return unless verbose

      if success
        log "Reset #{user}'s password"
      else
        log "Error resetting #{user}'s password: #{user.errors.full_messages.to_sentence}"
      end
    end

    def log(message)
      m = dry_run ? "Dry run: #{message}" : message
      Rails.logger.debug m
      return if Rails.env.test?
      puts "[#{Time.now.iso8601.sub(/\A-\d+:\d+\z/, '')}] #{m}"
    end
  end
end
