# frozen_string_literal: true
require_relative "api_schema/config"
require_relative "api_schema/collection"
require_relative "api_schema/resource"
require "ecma-re-validator"

module ApiSchema
end
