# frozen_string_literal: true

class SlowQueryReporter
  if GitHub.enterprise?
    SLOW_QUERY_THRESHOLD = 5.0
    SLOW_QUERY_ALERT_THRESHOLD = 5.0
  else
    SLOW_QUERY_THRESHOLD = 0.75
    SLOW_QUERY_ALERT_THRESHOLD = 2.0
  end

  SLOW_QUERY_KILL_THRESHOLD = 20.0

  attr_reader :sql, :duration_seconds, :connection_info

  def initialize(sql, duration_seconds, connection_info)
    @sql = sql
    @duration_seconds = duration_seconds
    @connection_info = connection_info
  end

  def report
    return if duration_seconds < SLOW_QUERY_THRESHOLD
    return if SlowQueryLogger.should_skip?(trace)
    report_to_failbot
    report_to_dogstats
  end

  private

  def report_to_failbot
    sql = GitHub::SQL::Digester.digest_sql(self.sql)

    error = SlowQueryLogger::SlowQuery.new(sql, duration_seconds)
    error.set_backtrace(trace)

    buckets = %w[github-slow-query]
    buckets += %w[github-slow-query-alert] if duration_seconds >= SLOW_QUERY_ALERT_THRESHOLD

    buckets.each do |bucket|
      Failbot.report!(
        error,
        app: bucket,
        sql_sanitized: sql,
        rollup: Digest::MD5.hexdigest(sql),
        database_connection: connection_info.url,
        connected_host: connection_info.host,
      )
    end
  end

  def report_to_dogstats
    tags = ["rpc_host:#{connection_info.host}"]

    GitHub.dogstats.timing("rpc.mysql.slow_query.time", duration_seconds, tags: tags)

    if duration_seconds >= SLOW_QUERY_KILL_THRESHOLD
      GitHub.dogstats.increment("rpc.mysql.slow_query.kill_count", tags: tags)
    end
  end

  def trace
    @trace ||= caller.dup
  end
end
