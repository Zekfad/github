# frozen_string_literal: true
module ApiRoutes
  class DefaultEndpoint
    include Comparable

    attr_reader :verb, :aors, :codeowners, :service_mapping

    def initialize(verb, pattern, params, aors: nil, codeowners: nil, service_mapping: nil)
      @verb = verb
      @pattern = pattern
      @params = params.dup
      @aors = aors || []
      @codeowners = codeowners || []
      @service_mapping = service_mapping

      @aors = @aors.to_a.join("|")
      @codeowners = @codeowners.to_a.map { |owner| owner.gsub(/\A@github\//, "") }.join("|")
    end

    def to_s
      "#{verb} #{route}"
    end

    def route
      @route ||= routify(pattern, params)
    end

    def <=>(other)
      to_s <=> other.to_s
    end

    private

    attr_reader :pattern, :params

    def routify(pattern, params)
      # When introspecting Sinatra routes, the route pattern is provided as a regex.
      # This turns the string-representation of the regex into a more human-readable
      # route in the format: GET /a/b/:c/d
      pattern.to_s
      .sub("(?-mix:^\\", "")
      .sub("(?-mix:\\A", "")
      .sub("(?-mix:", "")
      .sub("\\z)", "")
      .sub("$)", "")
      .gsub("(?:\\-|%2[Dd])", "-")
      .gsub("\\/", "/")
      .gsub("([^\/?#]+)") { ":#{params.shift}" } # insert params into route
    end
  end
end
