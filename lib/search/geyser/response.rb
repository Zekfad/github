# frozen_string_literal: true

require "twirp"

module Search
  module Geyser
    class Response
      include GitHub::AreasOfResponsibility
      areas_of_responsibility :code_search

      SecurityViolation = Class.new(StandardError)

      # Public: Return an empty response
      def self.empty(error_code = nil, error_message = nil)
        error = Twirp::Error.new(error_code, error_message) if error_code.present? && error_message.present?
        empty_response = Twirp::ClientResp.new(nil, error)
        new(empty_response)
      end

      attr_reader :client_response, :data, :error

      # client_response - A Twirp::ClientResp response from the Geyser client with two properties:
      #                   data: a Github::Geyser::Rpc::SearchResponse, or nil
      #                   error: a Twirp::Error, or nil
      def initialize(client_response)
        @client_response = client_response
        @data = client_response.data
        @error = client_response.error
      end

      # Public: the error code returned with the client response
      def error_code
        error&.code
      end

      # Public: the error message returned with the client response
      def error_message
        error&.msg
      end
    end
  end
end
