# frozen_string_literal: true

module Search
  module Geyser

    class Client
      include GitHub::AreasOfResponsibility

      areas_of_responsibility :code_search

      REQUEST_TIMEOUT = 2.0
      SERVICE_NAME = "geyser"
      MAX_RETRIES = 3

      RequestError          = Class.new(StandardError)
      TimeoutError          = Class.new(StandardError)
      InvalidGeyserEnvError = Class.new(StandardError)

      # Public: Initializes the Client singleton.
      def initialize
        raise NotImplementedError, "#{self.class.name} must implement #initialize"
      end

      protected

      def connection(url, hmac_keys)
        @http_client ||= Faraday.new(url: url) do |conn|
          conn.use GitHub::FaradayMiddleware::RequestID
          conn.use GitHub::FaradayMiddleware::HMACAuth, hmac_key: hmac_keys
          conn.use GitHub::FaradayMiddleware::Tracer,
            service_name: SERVICE_NAME,
            parent_span: proc { GitHub.tracer.last_span },
            tracer: GitHub.tracer
          conn.request :retry, max: MAX_RETRIES, retry_statuses: [503]
          conn.adapter :persistent_excon
          conn.options[:timeout] = REQUEST_TIMEOUT
          conn.options[:open_timeout] = REQUEST_TIMEOUT
        end
      end

      def client_request(&block)
        yield
      rescue Faraday::TimeoutError => e
        GitHub::Logger.log_exception({fn: "#{self.class.name}#connection", error: e.message}, e)
        raise TimeoutError.new
      rescue Faraday::ClientError => e
        GitHub::Logger.log_exception({fn: "#{self.class.name}#connection", error: e.message}, e)
        raise RequestError.new
      end
    end
  end
end
