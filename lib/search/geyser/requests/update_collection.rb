# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class UpdateCollection

        attr_reader :query

        # options - A hash of options for Github::Geyser::Rpc::UpdateCollectionRequest
        #           :name         - The full name of the target Collection to update
        #           :primary      - The updated primary status to set on the Collection
        def initialize(options = {})
          @query = Github::Geyser::Rpc::UpdateCollectionRequest.new(
            name: options.fetch(:name, ""),
            primary: wrap_primary(options),
          )
        end

        private

        def wrap_primary(opts)
          return nil if opts[:primary].nil?

          case opts[:primary].to_s.downcase
          when "true"
            Google::Protobuf::BoolValue.new(value: true)
          else
            Google::Protobuf::BoolValue.new(value: false)
          end
        end
      end
    end
  end
end
