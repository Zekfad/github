# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class KillSwitch
        attr_reader :request

        # desired_state - one of :ACTIVE or :PAUSED
        # options - A hash of options for Github::Geyser::Rpc::KillSwitchRequest
        #           :scope  - a string for filtering pod pause to only a subset of deployed
        #                    Geyser job distributor sites/regions (UNUSED)
        #           :reason - a string for backend logging describing what motivated the
        #                    use of the Geyser job distributor kill switch
        def initialize(desired_state, options = {})
          @request = Github::Geyser::Rpc::KillSwitchRequest.new(
            desired: desired_state,
            reason: options.fetch(:reason, ""),
            scope: options.fetch(:scope, ""),
          )
        end
      end
    end
  end
end
