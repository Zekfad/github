# frozen_string_literal: true

module Search
  module Geyser
    module Responses
      class KillSwitch < Response
        # Public: bool success flag from the response data, or nil
        def success
          data&.success
        end

        # Public: string error message from the response data, or nil
        def error_message
          data&.error_message
        end

        # Public: string scope label indicating the filter applied at the job
        # distributor pods to only pause event consumption and consumer group
        # membership for a subset of the full deployment, or nil; not to be
        # confused with "search_scope"; not currently respected on Geyser side
        def scope
          data&.scope
        end

        # Public: a Github::Geyser::Rpc::SwitchState enum value indicating the
        # state of the kill switch after our update request was processed, or nil
        def current
          data&.current
        end
      end
    end
  end
end
