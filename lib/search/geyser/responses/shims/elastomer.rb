# frozen_string_literal: true

module Search
  module Geyser
    module Responses
      module Shims
        class Elastomer
          attr_accessor :result

          def initialize(result)
            @result = result
          end

          def to_h
            {
              "_index"    => "geyser_code_search",
              "_type"     => "code",
              "_id"       => result.id,
              "_score"    => 0, # not used anywhere other than for logging
              "_model"    => nil, # added later during hydration to avoid n+1
              "highlight" => highlights,
              "_source"   => {
                "path"        => path,
                "filename"    => filename,
                "extension"   => extension,
                "file"        => result.content,
                "file_size"   => result.content_size,
                "repo_id"     => result.repository_id,
                "public"      => result.visibility == :PUBLIC,
                "language_id" => language_id,
                "blob_sha"    => result.blob_oid,
                "commit_sha"  => result.commit_oid,
                "timestamp"   => timestamp,
                "fork"        => false, # TODO once Geyser reports this
              }
            }
          end

          private

          def path
            File.dirname(result.path).delete_suffix("/")
          end

          def filename
            File.basename(result.path)
          end

          def extension
            File.extname(result.path)
          end

          # language_id is a Google::Protobuf::Int32Value
          def language_id
            result.language_id&.value
          end

          # indexed_at is a Google::Protobuf::Timestamp
          def timestamp
            result.indexed_at&.to_time&.utc&.strftime("%Y-%m-%dT%H:%M:%S%z")
          end

          # highlights is a repeated Github::Geyser::Rpc::HighlightFragment
          def highlights
            # Each entry is a Github::Geyser::Rpc::HighlightFragment
            result.highlights.inject({}) do |h, highlight|
              # field will be one of content, path, or filename. There will
              # only ever be one of any given field.
              field = highlight.field == "content" ? "file" : highlight.field
              h[field] = highlight.fragments
              h
            end
          end
        end
      end
    end
  end
end
