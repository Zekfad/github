# frozen_string_literal: true

module Search
  module Geyser
    module Responses
      class Search < Response
        # Public: a Github::Geyser::Rpc::Meta from the meta portion of the client response data, or nil
        def meta
          data&.meta
        end

        # Public: an array of Github::Geyser::Rpc::CodeResults from the results portion of the client response data, or nil
        def results
          data&.results
        end

        # Public: an array of Github::Geyser::Rpc::LanguageCounts from the languages portion of the client response data, or nil
        def languages
          data&.languages
        end
      end
    end
  end
end
