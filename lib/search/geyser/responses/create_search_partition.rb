# frozen_string_literal: true

module Search
  module Geyser
    module Responses
      class CreateSearchPartition < Response
        # Public: a Github::Search::Geyser::Rpc::SearchPartition
        def search_partition
          data&.search_partition
        end
      end
    end
  end
end
