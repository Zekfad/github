# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Clients
      class Search < Client
        attr_reader :geyser_env
        attr_reader :client

        # Public: Initializes the SearchClient
        def initialize(genv = :production)
          @geyser_env = genv
          @client = Github::Geyser::Rpc::SearchClient.new(connection)
        end

        # Public: returns the connection to the Geyser admin server, scoped to Geyser env
        def connection
          case geyser_env
          when :lab
            super(GitHub.geyser_search_api_lab_url, GitHub.geyser_search_api_lab_hmac_key)
          when :production
            super(GitHub.geyser_search_api_url, GitHub.geyser_search_api_hmac_key)
          else
            raise InvalidGeyserEnvError.new
          end
        end

        # Public: calls the #search method on the underlying Geyser client.
        #
        # request - a Github::Geyser::Rpc::SearchRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def search(request)
          client_request do
            client.search(request)
          end
        end
      end
    end
  end
end
