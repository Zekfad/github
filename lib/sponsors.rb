# frozen_string_literal: true

module Sponsors
  MATCHING_LIMIT_AMOUNT_IN_CENTS = 5000_00
  SUPPORTED_REGIONS_COUNT = 32
  FUNDING_YML_DOCS_URL="#{GitHub.help_url}/articles/displaying-a-sponsor-button-in-your-repository"
end
