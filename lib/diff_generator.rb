# frozen_string_literal: true

class DiffGenerator
  include DiffHelper
  include ActionView::Helpers::TagHelper

  # original_text - The "deletion" line.
  # new_text - The "addition" line.
  # path (optional)- The path of the file. This used to infer language and generate
  #                  lines that are highlighted for the correct syntax.
  def initialize(original_text, new_text, path)
    @original_text = original_text
    @new_text = new_text
    @path = path
  end

  def generate_diff
    # Treat trailing newline as a new line
    new_lines = "#{new_text}".split("\n", -1)
    original_lines = "#{original_text}".split("\n")

    # if original_lines is empty, this means the suggestion is deleting a
    # newline, which should be rendered as an empty deletion.
    original_lines << "" if original_lines.empty?

    if new_lines.length == original_lines.length
      diff_lines = original_lines.zip(new_lines)
      deletions, additions = diff_lines.map do |original_line, new_line|
        original_diff_line = GitHub::Diff::Line.new(type: :deletion, text: original_line || "")
        new_diff_line = GitHub::Diff::Line.new(type: :addition, text: new_line || "")

        original_diff_line.related_line = new_diff_line
        new_diff_line.related_line = original_diff_line

        generate_html(new_diff_line, original_diff_line)
      end.transpose
    elsif original_lines.length == 1
      original_diff_line = GitHub::Diff::Line.new(type: :deletion, text: original_lines.first)
      first_new_diff_line = GitHub::Diff::Line.new(type: :addition, text: new_lines.shift || "")

      original_diff_line.related_line = first_new_diff_line
      first_new_diff_line.related_line = original_diff_line

      deletion, addition = generate_html(first_new_diff_line, original_diff_line)

      additional_additions = new_lines.map do |line|
        syntax_highlight_filter(line)
      end

      additions = [] if new_lines.empty?
      additions ||= [addition, additional_additions].flatten
      deletions = [deletion]
    else
      additions = new_lines.map do |line|
        syntax_highlight_filter(line)
      end

      deletions = original_lines.map do |line|
        syntax_highlight_filter(line)
      end
    end

    return [deletions, additions]
  end

  private

  attr_reader :original_text, :new_text, :path

  def generate_html(new_line, original_line)
    addition_intra_line = highlighted_line(new_line, original_line)
    deletion_intra_line = highlighted_line(original_line, new_line)

    [deletion_intra_line, addition_intra_line]
  end

  def highlighted_line(line, related_line)
    if valid_language_scope
      html = syntax_highlight_filter(line.text)
      related_html = syntax_highlight_filter(related_line.text)
      mark_intra_line_changes_html(line, html, related_html, prefixed: false)
    else
      mark_intra_line_changes(line)
    end
  end

  def language_scope
    return unless path

    @language_scope ||= begin
      ext_name = path.split(".").last
      language = Linguist::Language[ext_name] || Linguist::Language.find_by_extension(path)&.first
      language&.tm_scope
    end
  end

  def valid_language_scope
    return @valid_language_scope if defined?(@valid_language_scope)
    @valid_language_scope = language_scope&.!= "none"
  end

  def syntax_highlight_filter(text)
    return text unless language_scope

    lines = GitHub::Colorize.highlight_one(language_scope, text, code_snippet: true, fallback_to_plain: false)
    return text unless lines

    safe_join(lines)
  rescue GitHub::Colorize::RPCError
    text
  end
end
