# frozen_string_literal: true

module Apps
  class Internal
    class Pages
      INTEGRATION_NAME = "GitHub Pages"
      OAUTH_APP_NAME = "GitHub Pages"

      INTEGRATION = {
        alias: :pages,
        id: ->() { GitHub.pages_github_app&.id },
        capabilities: {
          auto_upgrade_permissions: true,
          enforce_internal_access_on_token_generation: false,
          ip_whitelist_exempt: true,
          user_installable: false,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

      OAUTH = {
        alias: :pages_oauth,
        id: ->(*) { GitHub.pages_app_id },
        capabilities: {
          can_auto_approve_oauth_authorization: true,
        },
        can_auto_install: {},
        owners: [],
        properties: {},
      }
    end
  end
end
