# frozen_string_literal: true

module Apps
  class Internal
    class CodeScanning
      APP_NAME = "GitHub Code Scanning"

      def self.id_finder(app_name)
        ->() {
          Integration.find_by(
            owner_id: GitHub.trusted_apps_owner_id,
            name: app_name,
          )&.id
        }
      end

      PRODUCTION = {
        alias: :code_scanning,
        id: id_finder(APP_NAME),
        capabilities: {
          enforce_internal_access_on_token_generation: false,
          user_installable: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/dsp-code-scanning"],
      }

      PERMISSIONS = {
        "checks"               => :write,
        "contents"             => :read,
        "metadata"             => :read,
      }

      def self.seed_database!
        return if Apps::Internal.integration(:code_scanning).present?

        integration_attributes = {
          owner: GitHub.trusted_oauth_apps_owner,
          name: APP_NAME,
          url: "https://github.com/",
          public: true,
          default_permissions: PERMISSIONS,
          full_trust: true,
          skip_restrict_names_with_github_validation: true,
          skip_generate_slug: true,
        }
        app = Integration.create!(integration_attributes)

        app
      end
    end
  end
end
