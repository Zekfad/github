# frozen_string_literal: true

module Apps
  class Internal
    class GitHubSpamurai
      OAUTH_APP_NAME = "GitHub Spamurai Next"

      PRODUCTION = {
        alias: :github_spamurai_next,
        id: ->() {
          OauthApplication.find_by(
            user: GitHub.trusted_oauth_apps_owner,
            name: OAUTH_APP_NAME,
          )&.id
        },
        capabilities: {
          can_auto_approve_oauth_authorization: true # https://github.com/github/ecosystem-apps/issues/472
        },
        can_auto_install: {},
        owners: [],
        properties: {},
      }
    end
  end
end
