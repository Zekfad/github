# frozen_string_literal: true

module Apps
  class Internal
    class GitHubImporter
      OAUTH_APP_NAME = "github-importer-production"

      def self.id_finder(name)
        ->() {
          OauthApplication.find_by(
            user_id: GitHub.trusted_apps_owner_id,
            name: name,
          )&.id
        }
      end

      PRODUCTION = {
        alias: :github_importer,
        id: id_finder(OAUTH_APP_NAME),
        capabilities: {
          can_auto_approve_oauth_authorization: true,
          saml_sso_required: false,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }
    end
  end
end
