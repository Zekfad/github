# frozen_string_literal: true

module Apps
  class Internal
    class PullPanda
      ID = ENV.fetch("PULL_PANDA_ID", 7964)

      PRODUCTION = {
        alias: :pull_panda,
        id: ->() { ID },
        capabilities: {
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

    end
  end
end
