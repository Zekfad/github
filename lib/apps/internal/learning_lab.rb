# frozen_string_literal: true

module Apps
  class Internal
    class LearningLab
      APP_NAME = "GitHub Learning Lab"
      STAGING_APP_NAME = "Galileo"

      def self.id_finder(app_name)
        ->() {
          return nil if GitHub.enterprise?

          Integration.find_by(
            owner_id: GitHub.trusted_apps_owner_id,
            name: app_name,
          )&.id
        }
      end

      PRODUCTION = {
        alias: :github_learning_lab,
        id: id_finder(APP_NAME),
        capabilities: {
          enforce_internal_access_on_token_generation: false,
          ip_whitelist_exempt: true,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

      STAGING = {
        alias: :galileo,
        id: id_finder(STAGING_APP_NAME),
        capabilities: {
          enforce_internal_access_on_token_generation: false,
          ip_whitelist_exempt: true,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

    end
  end
end
