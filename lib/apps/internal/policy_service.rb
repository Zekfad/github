# frozen_string_literal: true

module Apps
  class Internal
    class PolicyService
      APP_NAME = "Policy Service"
      DEV_APP_NAME = "Policy Service (dev)"

      def self.id_finder(app_name)
        ->() {
          return nil if GitHub.enterprise?

          Integration.find_by(
            owner_id: GitHub.trusted_apps_owner_id,
            name: app_name,
          )&.id
        }
      end

      DEV = {
        alias: :github_policy_service_dev,
        id: id_finder(DEV_APP_NAME),
        capabilities: {
          auto_upgrade_permissions: true,
          enforce_internal_access_on_token_generation: false,
          installed_globally: true,
          skip_version_update_audit_log: true,
          user_installable: false,
        },
        properties: {
          accessible_targets: {
            "anthophila" => [],
            "dsp-testing" => [],
            "TheNewDanger" => [],
          }
        },
        can_auto_install: {},
        owners: [],
      }

      PRODUCTION = {
        alias: :github_policy_service,
        id: id_finder(APP_NAME),
        capabilities: {
          abuse_limit_multiplier: true, # https://github.com/github/dsp-policy/issues/144
          auto_upgrade_permissions: true,
          enforce_internal_access_on_token_generation: true,
          installed_globally: true,
          skip_version_update_audit_log: true,
          user_installable: false,
        },
        properties: {
          accessible_targets: {
            "anthophila" => [],
            "datreeio" => [],
            "dsp-testing" => [],
            "fidelityinvestments" => [],
            "github" => [],
            "googleapis" => [],
            "octodemo" => [],
            "project-c12" => [],
            "rft" => [],
            "salesforce" => [],
            "TheNewDanger" => [],
          }
        },
        can_auto_install: {},
        owners: [],
      }
    end
  end
end
