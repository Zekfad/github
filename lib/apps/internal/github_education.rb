# frozen_string_literal: true

module Apps
  class Internal
    class GitHubEducation
      APP_NAME = "GitHub Education"
      DEV_APP_NAME = "GitHub Education (development)"
      STAGING_APP_NAME = "GitHub Education (staging)"

      def self.id_finder(name)
        ->() {
          OauthApplication.where(
            user_id: GitHub.trusted_oauth_apps_owner,
            name: name
          ).pluck(:id).first
        }
      end

      PRODUCTION = {
        alias: :github_education,
        id: id_finder(APP_NAME),
        capabilities: {
          can_auto_approve_oauth_authorization: true
        },
        can_auto_install: {},
        owners: [],
        properties: {},
      }

      DEVELOPMENT = {
        alias: :github_education_development,
        id: id_finder(DEV_APP_NAME),
        capabilities: {
          can_auto_approve_oauth_authorization: true
        },
        can_auto_install: {},
        owners: [],
        properties: {},
      }

      STAGING = {
        alias: :github_education_staging,
        id: id_finder(STAGING_APP_NAME),
        capabilities: {
          can_auto_approve_oauth_authorization: true
        },
        can_auto_install: {},
        owners: [],
        properties: {},
      }

      OAUTH_APPS = [
        PRODUCTION,
        DEVELOPMENT,
        STAGING,
      ]
    end
  end
end
