# frozen_string_literal: true

module Apps
  class Internal
    class Gist
      OAUTH_APP_NAME = "Gist"
      DEV_OAUTH_APP_NAME = "Gist (dev)"
      PLAYGROUND_OAUTH_APP_NAME = "Gist Playground"
      ENTERPRISE_OAUTH_APP_NAME = "GitHub Gist"

      def self.production_id_finder
        ->() {
          app_name = GitHub.enterprise? ? ENTERPRISE_OAUTH_APP_NAME : OAUTH_APP_NAME

          OauthApplication.find_by(
            user: GitHub.trusted_oauth_apps_owner,
            name: app_name,
            key: GitHub.gist3_oauth_client_id
          )&.id
        }
      end

      PRODUCTION = {
        alias: :gist,
        id: production_id_finder,
        capabilities: {
          can_auto_approve_oauth_authorization: true # https://github.com/github/ecosystem-apps/issues/472
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

      DEVELOPMENT = {
        alias: :gist_dev,
        id: ->() {
          OauthApplication.find_by(
            user: GitHub.trusted_oauth_apps_owner,
            name: DEV_OAUTH_APP_NAME
          )&.id
        },
        capabilities: {
          can_auto_approve_oauth_authorization: true # https://github.com/github/ecosystem-apps/issues/472
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

      PLAYGROUND = {
        alias: :gist_playground,
        id: ->() {
          OauthApplication.find_by(
            user: GitHub.trusted_oauth_apps_owner,
            name: PLAYGROUND_OAUTH_APP_NAME
          )&.id
        },
        capabilities: {
          can_auto_approve_oauth_authorization: true # https://github.com/github/ecosystem-apps/issues/472
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

      OAUTH_APPS = [
        PRODUCTION,
        DEVELOPMENT,
        PLAYGROUND
      ]

    end
  end
end
