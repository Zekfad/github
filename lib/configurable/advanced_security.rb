# frozen_string_literal: true

# Whether a customer (enterprise account) has purchased GitHub Advanced Security
module Configurable
  module AdvancedSecurity
    KEY = "advanced_security".freeze

    def enable_advanced_security(actor:)
      config.enable(KEY, actor)
    end

    def disable_advanced_security(actor:)
      config.disable(KEY, actor)
    end

    def advanced_security_enabled?
      return false if new_record?
      config.enabled?(KEY)
    end

    # Allow advanced_security_enabled to be set like other active record attributes.
    # Advanced security will only be able to be set by staff so try to get
    # current_user else default to backup_actor.
    def advanced_security_enabled=(value)
      backup_actor = GitHub.enterprise? ? User.ghost : User.staff_user
      actor = User.find_by(id: GitHub.context[:actor_id]) || backup_actor
      entry = configuration_entries.find_by(name: KEY) || configuration_entries.new(name: KEY)
      cast_value = ActiveModel::Type::Boolean.new.cast(value) ? Configuration::TRUE : Configuration::FALSE
      entry.final   = false
      entry.value   = cast_value
      entry.updater = actor

      if persisted?
        entry.save!
        config.reset
      end

      cast_value
    end
  end
end
