# frozen_string_literal: true

module Configurable
  module AllowPrivateRepositoryForking
    KEY = "allow_private_repository_forking".freeze

    def allow_private_repository_forking_disabled_by_inherited_policy?
      !allow_private_repository_forking? && !config.local?(KEY)
    end

    def allow_private_repository_forking(force: false, actor:)
      changed = config.enable!(KEY, actor, force)
      return unless changed

      GitHub.dogstats.increment("private_repository_forking.enable")
      GitHub.instrument(
        "private_repository_forking.enable",
        instrumentation_payload(actor))
    end

    def block_private_repository_forking(force: true, actor:)
      changed = config.disable!(KEY, actor, force)
      return unless changed

      GitHub.dogstats.increment("private_repository_forking.disable")
      GitHub.instrument(
        "private_repository_forking.disable",
        instrumentation_payload(actor))
    end

    def clear_private_repository_forking_setting(actor:)
      changed = config.delete(KEY, actor)
      return unless changed

      GitHub.dogstats.increment("private_repository_forking.clear")
      GitHub.instrument(
        "private_repository_forking.clear",
        instrumentation_payload(actor))
    end

    def private_repository_forking_configurable?
      return true unless is_a?(Repository)

      in_organization? && private?
    end

    def allow_private_repository_forking?
      return true unless private_repository_forking_configurable?

      if is_a?(Repository) && !config.local?(KEY)
        # In the case of a forked repository, we need to check the organization
        # instead of the configuration_owner (User) for cascading
        organization.allow_private_repository_forking?
      else
        config.enabled?(KEY)
      end
    end

    # Is the setting enforced by a policy
    #
    # Returns a Boolean, true when the setting is enforced by policy, otherwise
    # false.
    def allow_private_repository_forking_policy?
      !!config.final?(KEY)
    end

    private

    def instrumentation_payload(actor)
      payload = { user: actor }

      if self.is_a?(Repository)
        payload[:repo] = self
        payload[:org] = self.organization if self.organization
      elsif self.is_a?(Organization)
        payload[:org] = self
        payload[:business] = self.business if self.business
      elsif self.is_a?(Business)
        payload[:business] = self
      end

      payload
    end
  end
end
