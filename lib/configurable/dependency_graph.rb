# rubocop:disable Style/FrozenStringLiteralComment

module Configurable
  module DependencyGraph

    ENABLED_KEY = "dependency_graph.enabled".freeze
    DISABLED_KEY = "dependency_graph.disabled".freeze

    def enable_dependency_graph(actor:)
      return unless GitHub.dependency_graph_enabled?
      return if dependency_graph_enabled?

      config.enable(ENABLED_KEY, actor)
      config.delete(DISABLED_KEY, actor)
      GitHub.instrument("repository_dependency_graph.enable", instrumentation_payload(actor))
    end

    def disable_dependency_graph(actor:)
      return unless dependency_graph_enabled?
      config.enable(DISABLED_KEY, actor)
      config.delete(ENABLED_KEY, actor)
      clear_manifests_from_dependency_graph
      GitHub.instrument("repository_dependency_graph.disable", instrumentation_payload(actor))
    end

    def dependency_graph_enabled?
      return unless GitHub.dependency_graph_enabled?
      if GitHub.enterprise?
        return GitHub.dotcom_connection_enabled? && GitHub.ghe_content_analysis_enabled?
      end

      if public? && !fork?
        !config.enabled?(DISABLED_KEY)
      else
        config.enabled?(ENABLED_KEY)
      end
    end

    def instrumentation_payload(user)
      payload = {
        user: user,
        repo: self,
      }

      if in_organization?
        payload[:org] = organization
      end

      payload
    end
  end
end
