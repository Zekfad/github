# frozen_string_literal: true

module Configurable
  module TokenScanning
    USER_ENABLED_KEY = "token_scanning.user_enabled".freeze
    STAFF_DISABLED_KEY = "token_scanning.disabled".freeze

    def enable_token_scanning(actor:, use_staff_key: false)
      if use_staff_key
        config.delete(STAFF_DISABLED_KEY, actor)
      else
        config.enable(USER_ENABLED_KEY, actor)
      end
      GitHub.instrument("repository_secret_scanning.enable", secret_scanning_instrumentation_payload(actor, use_staff_key))
    end

    def disable_token_scanning(actor:, use_staff_key: false)
      if use_staff_key
        config.enable(STAFF_DISABLED_KEY, actor)
      else
        config.delete(USER_ENABLED_KEY, actor)
      end
      GitHub.instrument("repository_secret_scanning.disable", secret_scanning_instrumentation_payload(actor, use_staff_key))
    end

    # Indicates whether token scanning was manually disabled
    # by staff through StaffTools
    # or by a user through repo settings.
    def token_scanning_manually_disabled?
      !config.enabled?(USER_ENABLED_KEY) || config.enabled?(STAFF_DISABLED_KEY)
    end

    # Indicates if token scanning was manually disabled by staff through StaffTools.
    def token_scanning_staff_disabled?
      config.enabled?(STAFF_DISABLED_KEY)
    end

    def secret_scanning_instrumentation_payload(actor, use_staff_key)
      payload = {
        user: actor,
        repo: self,
        use_staff_key: use_staff_key
      }

      if in_organization?
        payload[:org] = organization
      end

      payload
    end
  end
end
