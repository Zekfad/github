# frozen_string_literal: true

module Configurable
  module OktaTeamSyncBeta
    def enable_okta_team_sync_beta
      GitHub.flipper[:okta_team_sync].enable(self)
    end

    def disable_okta_team_sync_beta
      GitHub.flipper[:okta_team_sync].disable(self)
    end

    def okta_team_sync_beta_enabled?
      GitHub.flipper[:okta_team_sync].enabled?(self)
    end
  end
end
