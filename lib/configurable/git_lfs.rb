# rubocop:disable Style/FrozenStringLiteralComment

module Configurable
  module GitLfs
    def enable_git_lfs(actor)
      if GitHub.git_lfs_enabled
        config.delete(KEY, actor)
        config.delete(OLDKEY, actor)
      else
        config.enable(KEY, actor)
      end
    end

    def disable_git_lfs(actor)
      config.disable(OLDKEY, actor) unless GitHub.git_lfs_enabled
      config.disable(KEY, actor)
    end

    def git_lfs_config_enabled?
      if GitHub.git_lfs_enabled
        config.get(Configurable::GitLfs::KEY) != false
      else
        config.enabled?(Configurable::GitLfs::KEY) || config.enabled?(Configurable::GitLfs::OLDKEY)
      end
    end

    def can_enable_lfs_in_archives?
      return false unless git_lfs_config_enabled?
      GitHub.flipper[:codeload_use_librarian].enabled?(self)
    end

    def lfs_in_archives_enabled?
      return false unless can_enable_lfs_in_archives?
      config.enabled?(Configurable::GitLfs::ARCHIVE_KEY)
    end

    def enable_lfs_in_archives(actor)
      config.enable(ARCHIVE_KEY, actor)
    end

    def disable_lfs_in_archives(actor)
      config.disable(ARCHIVE_KEY, actor)
    end

    KEY = "git-lfs".freeze
    OLDKEY = "git-media".freeze
    ARCHIVE_KEY = "git-lfs-in-archives".freeze
  end
end
