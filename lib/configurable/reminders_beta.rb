# frozen_string_literal: true

module Configurable
  module RemindersBeta
    FLAGS = %i(scheduled_reminders_frontend)
    EMAIL_WAIT_TIME = 5.minutes

    def reminders_enabled?
      return false if GitHub.enterprise?

      FLAGS.all? do |flag|
        GitHub.flipper[flag].enabled?(self)
      end
    end

    def enable_reminders
      FLAGS.each do |flag|
        GitHub.flipper[flag].enable(self)
      end

      membership = EarlyAccessMembership.
        reminders_waitlist.
        find_by!(member: self)

      OnboardRemindersJob.set(wait: EMAIL_WAIT_TIME).perform_later(membership)
    end

    def disable_reminders
      FLAGS.each do |flag|
        GitHub.flipper[flag].disable(self)
      end
    end
  end
end
