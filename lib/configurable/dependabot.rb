# frozen_string_literal: true

module Configurable
  module Dependabot
    VULNERABILITIES_ENABLED_KEY = "repository_dependency_updates.vulnerabilities.enabled".freeze
    VULNERABILITIES_DISABLED_KEY = "repository_dependency_updates.vulnerabilities.disabled".freeze

    def enable_vulnerability_updates(actor:)
      return unless vulnerability_updates_disabled?

      config.delete(VULNERABILITIES_DISABLED_KEY, actor)
      GlobalInstrumenter.instrument(VULNERABILITIES_ENABLED_KEY, instrumentation_payload(actor))
      true
    end

    def disable_vulnerability_updates(actor:)
      return unless vulnerability_updates_enabled?
      config.enable(VULNERABILITIES_DISABLED_KEY, actor)

      GlobalInstrumenter.instrument(VULNERABILITIES_DISABLED_KEY, instrumentation_payload(actor))
      true
    end

    def vulnerability_updates_enabled?
      !config.enabled?(VULNERABILITIES_DISABLED_KEY)
    end

    def vulnerability_updates_disabled?
      config.enabled?(VULNERABILITIES_DISABLED_KEY)
    end

    def instrumentation_payload(actor)
      payload = {
        user: actor,
        repository: nil,
        organization: self,
      }

      payload
    end
  end
end
