# rubocop:disable Style/FrozenStringLiteralComment

module Configurable
  module ContentAnalysis
    extend ActiveSupport::Concern

    KEY = "repo_content_analysis".freeze
    WHERE_ID_LIMIT = 10_000

    ScopeTooBroadError = Class.new(StandardError)

    class_methods do
      def with_content_analysis_enabled
        if GitHub.enterprise?
          if GitHub.dotcom_connection_enabled? && GitHub.ghe_content_analysis_enabled?
            return all
          else
            return none
          end
        end

        result = all

        # Remove any LIMIT clause to be reapplied at the end. We'll scope
        # repositories by an array of IDs and some IDs from that array will be
        # disqualified in Ruby memory. So we need to get a superset of
        # potential IDs in order to respect the original LIMIT clause.
        original_limit_value = result.limit_value
        result.limit_value = nil

        # If this relation isn't sufficiently scoped, we'll end up dealing with
        # too many repository IDs at once. Ultimately, we scope repositories by
        # an array of IDs so the size of that array must be reasonable.
        if result.limit(WHERE_ID_LIMIT + 1).count > WHERE_ID_LIMIT
          raise ScopeTooBroadError, <<~MSG.squish
            A collection of repositories must be scoped using WHERE conditions
            to return #{WHERE_ID_LIMIT} records or fewer before calling the
            Repository.with_content_analysis_enabled scope.
            MSG
        end

        # Rather than querying twice, once for public and once for private
        # repostories, we fetch the data we care about just once.
        public_ids = []
        private_ids = []
        result.pluck(:id, :public).each do |id, is_public|
          if is_public
            public_ids << id
          else
            private_ids << id
          end
        end

        # For private repositories, a configuration entry must exist to enable
        # content analysis.
        explicitly_enabled_private_ids = Configuration::Entry.
          where(target_type: name, name: KEY).
          with_ids(private_ids, field: :target_id).
          pluck(:target_id)

        enabled_ids = public_ids + explicitly_enabled_private_ids

        result.with_ids(enabled_ids).limit(original_limit_value)
      end
    end

    def enable_content_analysis(actor:)
      return if content_analysis_enabled?
      config.enable(KEY, actor)
      send_manifests_to_dependency_graph
      GitHub.instrument("repository_content_analysis.enable", instrumentation_payload(actor))
    end

    def disable_content_analysis(actor:)
      return unless content_analysis_enabled?
      config.delete(KEY, actor)
      clear_manifests_from_dependency_graph
      GitHub.instrument("repository_content_analysis.disable", instrumentation_payload(actor))
    end

    def ensure_manifest_is_initialized
      return if dependency_manifests_detected?

      send_manifests_to_dependency_graph
    end

    def send_manifests_to_dependency_graph
      RepositoryDependencyManifestInitializationJob.perform_later(id)
    end

    def clear_manifests_from_dependency_graph
      RepositoryDependencyClearDependencies.perform_later(id)
    end

    def content_analysis_enabled?
      if GitHub.enterprise?
        return GitHub.dotcom_connection_enabled? && GitHub.ghe_content_analysis_enabled?
      end

      return true if public?
      config.enabled?(KEY)
    end

    def instrumentation_payload(user)
      payload = {
        user: user,
        repo: self,
      }

      if in_organization?
        payload[:org] = organization
      end

      payload
    end
  end
end
