# frozen_string_literal: true

module Configurable
  module RepositoryDependencyUpdates
    VULNERABILITIES_ENABLED_KEY = "repository_dependency_updates.vulnerabilities.enabled".freeze
    VULNERABILITIES_DISABLED_KEY = "repository_dependency_updates.vulnerabilities.disabled".freeze

    def enable_vulnerability_updates(actor:, enroll: true)
      config.enable(VULNERABILITIES_ENABLED_KEY, actor)
      config.delete(VULNERABILITIES_DISABLED_KEY, actor)

      ::Dependabot.enroll_for_security_updates(repository: self) if enroll

      GlobalInstrumenter.instrument(VULNERABILITIES_ENABLED_KEY, instrumentation_payload(actor))
      true
    end

    def disable_vulnerability_updates(actor:)
      config.enable(VULNERABILITIES_DISABLED_KEY, actor)
      config.delete(VULNERABILITIES_ENABLED_KEY, actor)

      GlobalInstrumenter.instrument(VULNERABILITIES_DISABLED_KEY, instrumentation_payload(actor))
      true
    end

    def vulnerability_updates_enabled?
      config.enabled?(VULNERABILITIES_ENABLED_KEY)
    end

    # In the event both keys are set, `enabled` takes precident since `disabled`
    # may be set at an owner level but overridden local to one repository.
    def vulnerability_updates_disabled?
      config.enabled?(VULNERABILITIES_DISABLED_KEY) &&
        !config.enabled?(VULNERABILITIES_ENABLED_KEY)
    end

    def vulnerability_updates_configured?
      vulnerability_updates_enabled? || vulnerability_updates_disabled?
    end

    def instrumentation_payload(user)
      payload = {
        user: user,
        repository: self,
      }

      if in_organization?
        payload[:org] = organization
      end

      payload
    end
  end
end
