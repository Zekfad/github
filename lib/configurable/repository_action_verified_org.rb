# frozen_string_literal: true

module Configurable
  module RepositoryActionVerifiedOrg
    KEY = "actions_marketplace_verified_org".freeze

    # Verifies the organization for repo actions. Re-indexes all repo actions from this org
    #
    # actor - user doing the action
    def verify_for_repo_actions(actor = nil)
      config.enable(KEY, actor)
      RepositoryAction.owned_by(login).each(&:synchronize_search_index)
    end

    # Removes verification from the organization for repo actions. Re-indexes all repo actions from this org
    #
    # actor - user doing the action
    def unverify_for_repo_actions(actor)
      config.disable(KEY, actor)
      RepositoryAction.owned_by(login).each(&:synchronize_search_index)
    end

    # Gets a boolean flag indicating if repo actions is enabled for this org
    #
    # Returns: boolean
    def verified_for_repo_actions?
      config.enabled?(KEY)
    end

    # Gets a relation of the organizations that are verified for repo actions
    #
    # Returns: ActiveRecord::Relation<Organization>
    def self.verified_for_repo_actions
      org_ids = Configuration::Entry.where(
        name: KEY,
        value: Configuration::TRUE,
        target_type: "User",
      ).pluck(:target_id)
      Organization.with_ids(org_ids).order(login: :asc)
    end
  end
end
