# rubocop:disable Style/FrozenStringLiteralComment

# Whether searches on dotcom are enabled
# (on GHE instances that are properly connected to dotcom)
module Configurable
  module DotcomSearch
    KEY = "dotcom_search".freeze

    def enable_dotcom_search(actor)
      config.enable(KEY, actor)
    end

    def disable_dotcom_search(actor)
      config.disable(KEY, actor)
      disable_dotcom_private_search(actor)
    end

    def dotcom_search_enabled?
      config.enabled?(KEY)
    end
  end
end
