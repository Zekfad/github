# rubocop:disable Style/FrozenStringLiteralComment

module Configurable
  module OrgCreation
    KEY = "disable_user_org_creation".freeze

    # Default is enable
    # thus we need to delete the disable key to enable org creation
    def enable_org_creation(actor)
      config.delete(KEY, actor)
    end

    def disable_org_creation(actor)
      config.enable(KEY, actor)
    end

    def org_creation_enabled?
      !config.enabled?(KEY)
    end

  end
end
