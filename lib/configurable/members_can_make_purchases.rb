# frozen_string_literal: true

module Configurable
  module MembersCanMakePurchases
    KEY = "disable_members_can_make_purchases"

    def allow_members_can_make_purchases(actor:)
      raise ArgumentError, "members_can_make_purchases can only be configured on a Business" unless self.is_a?(Business)

      changed = config.delete(KEY, actor)
      return unless changed

      GitHub.dogstats.increment("members_can_make_purchases.enable")
      GitHub.instrument(
          "members_can_make_purchases.enable",
          { user: actor, business: self })
    end

    def disallow_members_can_make_purchases(force: false, actor:)
      raise ArgumentError, "members_can_make_purchases can only be configued on a Business" unless self.is_a?(Business)

      changed = config.enable!(KEY, actor, force)
      return unless changed

      GitHub.dogstats.increment("members_can_make_purchases.disable")
      GitHub.instrument(
          "members_can_make_purchases.disable",
          { user: actor, business: self })
    end

    def members_can_make_purchases?
      !config.enabled?(KEY)
    end
  end
end
