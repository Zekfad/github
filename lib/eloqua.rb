# frozen_string_literal: true

module Eloqua
  class << self
    def cache_provider=(cache_provider)
      @cache_provider = cache_provider
    end

    def cache_provider
      @cache_provider || NullCache.new
    end

    class NullCache
      def fetch(*_args)
        yield
      end
    end
  end
end
