# rubocop:disable Style/FrozenStringLiteralComment

module Configurable
  autoload :AnonymousGitAccess, "configurable/anonymous_git_access"
  autoload :OrgMembershipVisibility, "configurable/org_membership_visibility"
  autoload :GitLfs, "configurable/git_lfs"
  autoload :ContentAnalysis, "configurable/content_analysis"
  autoload :Dependabot, "configurable/dependabot"
  autoload :RemindersBeta, "configurable/reminders_beta"
  autoload :DotcomDownloadActionsArchive, "configurable/dotcom_download_actions_archive"
  autoload :DisplayCommenterFullName, "configurable/display_commenter_full_name"
  autoload :OrgCreation, "configurable/org_creation"
  autoload :ActionExecutionCapabilities, "configurable/action_execution_capabilities"
  autoload :AllowAutoDeletingBranches, "configurable/allow_auto_deleting_branches"
  autoload :ArchiveProgramOptOut, "configurable/archive_program_opt_out"
  autoload :RepositoryDependencyUpdates, "configurable/repository_dependency_updates"
  autoload :TokenScanning, "configurable/token_scanning"
  autoload :IpWhitelistingEnabled, "configurable/ip_whitelisting_enabled"
  autoload :TwoFactorRequired, "configurable/two_factor_required"
  autoload :ForcePushRejection, "configurable/force_push_rejection"
  autoload :TieredReporting, "configurable/tiered_reporting"
  autoload :DisableTeamDiscussions, "configurable/disable_team_discussions"
  autoload :DefaultNewRepoBranch, "configurable/default_new_repo_branch"
  autoload :EnterpriseDormancyThreshold, "configurable/enterprise_dormancy_threshold"
  autoload :Ssh, "configurable/ssh"
  autoload :Showcase, "configurable/showcase"
  autoload :DotcomPrivateSearch, "configurable/dotcom_private_search"
  autoload :RepositoryFundingLinks, "configurable/repository_funding_links"
  autoload :RepositoryExportedToUrl, "configurable/repository_exported_to_url"
  autoload :DisableOrganizationProjects, "configurable/disable_organization_projects"
  autoload :ReadersCanCreateDiscussions, "configurable/readers_can_create_discussions"
  autoload :RepositoryActionVerifiedOrg, "configurable/repository_action_verified_org"
  autoload :MembersCanUpdateProtectedBranches, "configurable/members_can_update_protected_branches"
  autoload :MembersCanCreateTeams, "configurable/members_can_create_teams"
  autoload :CrossRepoConflictEditor, "configurable/cross_repo_conflict_editor"
  autoload :DotcomSearch, "configurable/dotcom_search"
  autoload :MaxObjectSize, "configurable/max_object_size"
  autoload :DotcomUserLicenseUsageUpload, "configurable/dotcom_user_license_usage_upload"
  autoload :MembersCanInviteOutsideCollaborators, "configurable/members_can_invite_outside_collaborators"
  autoload :SecuritySettings, "configurable/security_settings"
  autoload :DisableRepositoryProjects, "configurable/disable_repository_projects"
  autoload :DefaultRepoVisibility, "configurable/default_repo_visibility"
  autoload :CodeScanning, "configurable/code_scanning"
  autoload :OktaTeamSyncBeta, "configurable/okta_team_sync_beta"
  autoload :MembersCanMakePurchases, "configurable/members_can_make_purchases"
  autoload :SuggestedProtocol, "configurable/suggested_protocol"
  autoload :DependencyGraph, "configurable/dependency_graph"
  autoload :MembersCanViewDependencyInsights, "configurable/members_can_view_dependency_insights"
  autoload :MembersCanDeleteRepositories, "configurable/members_can_delete_repositories"
  autoload :MembersCanPublishPackages, "configurable/members_can_publish_packages"
  autoload :MembersCanCreateRepositories, "configurable/members_can_create_repositories"
  autoload :DotcomContributions, "configurable/dotcom_contributions"
  autoload :MembersCanDeleteIssues, "configurable/members_can_delete_issues"
  autoload :PackageAvailability, "configurable/package_availability"
  autoload :ForkPrWorkflowsPolicy, "configurable/fork_pr_workflows_policy"
  autoload :RestrictNotificationDelivery, "configurable/restrict_notification_delivery"
  autoload :OperatorMode, "configurable/operator_mode"
  autoload :ActionsAllowedEntities, "configurable/actions_allowed_entities"
  autoload :ResellerCustomer, "configurable/reseller_customer"
  autoload :ActionsAllowedByOwner, "configurable/actions_allowed_by_owner"
  autoload :RepositoryVulnerabilityAlerts, "configurable/repository_vulnerability_alerts"
  autoload :GheContentAnalysis, "configurable/ghe_content_analysis"
  autoload :SshCertificateRequirement, "configurable/ssh_certificate_requirement"
  autoload :IpWhitelistingAppAccessEnabled, "configurable/ip_whitelisting_app_access_enabled"
  autoload :ActionInvocation, "configurable/action_invocation"
  autoload :MembersCanChangeRepoVisibility, "configurable/members_can_change_repo_visibility"
  autoload :DefaultRepositoryPermission, "configurable/default_repository_permission"
  autoload :AdvancedSecurity, "configurable/advanced_security"
  autoload :UsedBy, "configurable/used_by"
  autoload :AllowPrivateRepositoryForking, "configurable/allow_private_repository_forking"
  autoload :PackageRegistry, "configurable/package_registry"
  autoload :AnonymousGitAccessLock, "configurable/anonymous_git_access_lock"
  autoload :SmsForTwoFactorAuth, "configurable/sms_for_two_factor_auth"
  autoload :DiskQuota, "configurable/disk_quota"

  extend ActiveSupport::Concern

  included do
    if respond_to?(:has_many)
      has_many :configuration_entries,
        as: :target,
        class_name: "Configuration::Entry",
        dependent: :destroy
    end
  end

  # Public: Get the configuration, for getting/setting/listing values.
  #
  # Returns a Configuration object
  def config
    @_configuration ||= ::Configuration.new self
  end

  # Public: Entry a setting would come from if current setting is cleared.
  #
  # name - String entry name
  #
  # Returns a Configuration::Entry or nil
  def configuration_default_entry(name)
    return unless configuration_owner
    configuration_owner.config.entries[name]
  end

  # Public: Where a setting would come from if current setting is cleared.
  #
  # name - String entry name
  #
  # Returns a Configurable or nil
  def configuration_default_entry_owner(name)
    entry = configuration_default_entry(name)
    entry && entry.target
  end

  # Public: What a setting would be if current setting is cleared.
  #
  # name - String entry name
  #
  # Returns a string or nil
  def configuration_default_entry_value(name)
    entry = configuration_default_entry(name)
    entry && entry.value
  end

  # Public: Clear memoized config, then calls super if defined
  #
  # Returns whatever super would return or self if there is no super
  def reload(*args)
    reset_config
    defined?(super) ? super : self
  end

  # Internal: Clear the cached Configuration object
  #
  # Returns nothing
  def reset_config
    @_configuration = nil
  end

  # Internal: Define the "owner", for use in cascading configuration values
  #
  # Should return a Configurable or nil
  def configuration_owner
    nil
  end

  # Internal: Define the chain of "owners", for use in cascading configuration values
  # THIS METHOD SHOULD NOT BE OVERRIDDEN
  #
  # Returns an array of Configurables and/or nil
  def configuration_owners
    return [] unless configuration_owner

    configuration_owner.configuration_owners + [configuration_owner]
  end

  # Internal: Load `configuration_owner` in an asynchronous context.
  #
  # Returns a promise resolving to `configuration_owner`
  def async_configuration_owner
    Promise.resolve(configuration_owner)
  end

  # Internal: Defines the chain of configuration owners in an asynchronous context.
  # This is an asynchronous implementation of `configuration_owners`.
  # THIS METHOD SHOULD NOT BE OVERRIDDEN
  #
  # Returns a promise resolving to an array of Configurables and/or nil
  def async_configuration_owners
    async_configuration_owner.then do |owner|
      next [] unless owner

      owner.async_configuration_owners.then do |ancestors|
        ancestors + [owner]
      end
    end
  end

  # Internal: The ID in use for associating configuration entries
  #
  # Can be overridden (see GitHub.configuration_entry_id, below), but should
  # easily be left alone in practice
  #
  # Returns a Integer
  def configuration_entry_id
    id
  end

  # Internal: The type in use for associating configuration entries
  #
  # Can be overridden (see GitHub.configuration_entry_type, below), but should
  # easily be left alone in practice
  #
  # Returns a String
  def configuration_entry_type
    self.class.name
  end

  # Async helpers for Configurable
  # usage: `extend Configurable::Async`
  module Async
    # Internal: defines a new method as `async_{method}` that uses a platform
    # loader to asynchronously load a synchronous `Configurable` method
    def async_configurable(*methods)
      methods.each do |method|
        define_method "async_#{method}" do
          ::Platform::Loaders::Configuration.load(self, method)
        end
      end
    end
  end
end
