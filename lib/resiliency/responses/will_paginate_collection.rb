# frozen_string_literal: true

module Resiliency
  module Responses
    class WillPaginateCollection < Response
      def initialize(&block)
        collection = WillPaginate::Collection.create(1, 1, 0) { |pager|
          pager.replace([])
        }
        super(collection)
      end
    end
  end
end
