# frozen_string_literal: true

module DependencyGraph
  class DynamicManifestsQuery < Query
    REQUEST_TIMEOUT = 20.0

    # Attemped to dynamically parse a manifest for a package manager still in
    # preview without passing `preview: true`.
    StillInPreviewError = Class.new(ArgumentError)

    def self.default_backend
      @default_backend ||= ::DependencyGraph::Client.new(connection: DependencyGraph::Client.slow_query_connection, timeout: REQUEST_TIMEOUT)
    end

    def initialize(files:, backend: nil, with_dependencies: false, include_vulnerabilities: false, preview: false)
      @files                   = files
      @with_dependencies       = with_dependencies
      @include_vulnerabilities = include_vulnerabilities
      @preview                 = preview
      super(backend: backend)
    end

    def results
      manifests.map do |manifests|
        manifests
          .reject { |manifest| exclude?(manifest) }
          .sort_by { |manifest| [manifest.depth, manifest.filename] }
      end.rescue(&method(:handle_api_error))
    end

    def query
      files.map.with_index do |file, i|
        field(:parsedManifest, {
          alias: "file_#{i}",
          arguments: [
            argument(:repositoryId, file.repository_id),
            argument(:filename, file.filename),
            argument(:path, file.path),
            argument(:encoded, file.encoded),
            argument(:preview, preview),
          ],
          selections: [
            field(:repositoryId),
            field(:manifestType),
            field(:filename),
            field(:path),
            field(:isVendored),
            field(:dependencies, {
              selections: [
                field(:repositoryId),
                field(:packageName),
                field(:packageLabel),
                field(:packageManager),
                field(:requirements),
                field(:hasDependencies),
              ].tap do |selections|
                if include_vulnerabilities?
                  selections << field(:vulnerableVersionRanges, {
                    selections: [
                      field(:edges, {
                        selections: [
                          field(:isContained),
                          field(:node, {
                            selections: [
                              field(:githubId),
                            ],
                          }),
                        ],
                      }),
                    ],
                  })
                end
              end,
            }),
          ],
        })
      end
    end

    private

    attr_reader :files, :preview

    def handle_api_error(error)
      if error.is_a?(Client::ApiError) && error.message =~ /still in preview/i
        GitHub.dogstats.increment("dependency_graph.preview.error.count")
        GitHub::Result.error(StillInPreviewError.new(error.message))
      else
        GitHub::Result.error(error)
      end
    end

    def manifests
      execute_query.map do |result|
        result["data"].map do |_, attrs|
          dependencies = attrs["dependencies"].map do |dependency|
            {"node" => dependency}
          end

          Manifest.new(attrs.merge({
            "dependenciesCount" => attrs["dependencies"].count,
            "dependencies" => {
              "edges" => dependencies,
            },
          }))
        end
      end
    end

    def exclude?(manifest)
      with_dependencies? && manifest.dependencies.none?
    end

    def with_dependencies?
      @with_dependencies
    end

    def include_vulnerabilities?
      @include_vulnerabilities
    end
  end
end
