# frozen_string_literal: true

module DependencyGraph
  class Dependent
    attr_reader :cursor

    def self.wrap(dependents)
      dependents.map { |attrs| new(attrs["node"], cursor: attrs["cursor"]) }
    end

    def initialize(attrs, cursor: nil)
      @attrs  = attrs
      @cursor = cursor
    end

    def repository_id
      @attrs["repositoryId"]
    end

    def name
      @attrs["name"]
    end

    def manifest_path
      File.join([
        @attrs["manifestPath"],
        @attrs["manifestFilename"],
      ].reject(&:blank?))
    end

    def requirements
      @attrs["requirements"]
    end

    def platform_type_name
      "DependencyGraphDependent"
    end
  end
end
