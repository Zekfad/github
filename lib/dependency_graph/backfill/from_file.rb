# frozen_string_literal: true

require "csv"

module DependencyGraph
  module Backfill
    class FromFile
      attr_accessor :argument_error, :input_file, :manifest_types, :maximum_jobs, :start_at_id

      def initialize(file_path:, manifest_types:, maximum_jobs: nil, start_at_id: nil)
        @file_path = file_path

        unless file_path.blank?
          begin
            @input_file = CSV.read(file_path)
          rescue Errno::ENOENT
            @error_reading_file = true
          end
        end

        @manifest_types = Array(manifest_types).map(&:to_sym)
        @maximum_jobs = maximum_jobs&.to_i
        @start_at_id = start_at_id&.to_i
      end

      # Public: Validate the arguments to ensure we're confident we can run.
      #   Sets an argument_error if false so you can know what went wrong.
      #
      # Returns boolean.
      def valid_arguments?
        if manifest_types.blank?
          @argument_error = "Missing manifest types"
          return false
        elsif @file_path.blank?
          @argument_error = "Missing file path"
          return false
        elsif @error_reading_file
          @argument_error = "Error reading file"
          return false
        end

        true
      end

      def enqueue_jobs
        return { success: false, error: argument_error } unless valid_arguments?

        enqueued_count = 0

        repo_ids = prepare_input
        repo_ids.each do |repo_id|
          DependencyGraphManifestBackfillJob.perform_later(repo_id, manifest_types)
          enqueued_count += 1

          puts "Enqueued repo #{repo_id}"
        end

        { success: true, enqueued_count: enqueued_count }
      end

      private

      # Private: Ensure that the input we parsed from the file matches our expectations.
      #   - Transform the input Array to ensure it's flat, uniq and full of Integers
      #   - Apply whatever rules were passed in to the script regarding where to start.
      #
      # Returns an Array
      def prepare_input
        ids_for_processing = @input_file.flatten.map(&:to_i).sort.uniq

        if start_at_id
          start_id_index = ids_for_processing.find_index(start_at_id)
          ids_for_processing = ids_for_processing[start_id_index..-1]
        end

        if maximum_jobs
          ids_for_processing = ids_for_processing.take(maximum_jobs)
        end

        ids_for_processing
      end
    end
  end
end
