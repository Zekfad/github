# frozen_string_literal: true

module DependencyGraph
  module Backfill
    class PrivateRepositories
      attr_accessor :argument_error, :manifest_types, :maximum_jobs, :start_at_id

      def initialize(manifest_types:, maximum_jobs: nil, start_at_id: nil)
        @manifest_types = Array(manifest_types).map(&:to_sym)
        @maximum_jobs = maximum_jobs&.to_i
        @start_at_id = start_at_id&.to_i
      end

      # Public: Validate the arguments to ensure we're confident we can run.
      #   Sets an argument_error if false so you can know what went wrong.
      #
      # Returns boolean.
      def valid_arguments?
        if manifest_types.blank?
          @argument_error = "Missing manifest types"
          return false
        end

        true
      end

      def enqueue_jobs
        return { success: false, error: argument_error } unless valid_arguments?

        enqueued_count = 0

        Configuration::Entry.where(name: "dependency_graph.enabled", value: "true").find_each(batch_size: 100, start: start_at_id) do |config_entry|
          break if maximum_jobs && enqueued_count >= maximum_jobs
          repository_id = config_entry.target_id.to_i

          # Enqueue the repository ID we found to our backfill.
          DependencyGraphManifestBackfillJob.perform_later(repository_id, manifest_types)
          enqueued_count += 1
        end

        { success: true, enqueued_count: enqueued_count }
      end
    end
  end
end
