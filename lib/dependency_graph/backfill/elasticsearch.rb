# frozen_string_literal: true

module DependencyGraph
  module Backfill
    class Elasticsearch
      attr_accessor :argument_error, :manifest_filename, :manifest_type

      def initialize(manifest_filename:, manifest_type:)
        @manifest_filename = manifest_filename&.downcase
        @manifest_type = manifest_type&.to_sym
      end

      # Public: Validate the arguments to ensure we're confident we can run.
      #   Sets an argument_error if false so you can know what went wrong.
      #
      # Returns boolean.
      def valid_arguments?
        if manifest_filename.blank?
          @argument_error = "Missing manifest filename"
          return false
        elsif manifest_type.blank?
          @argument_error = "Missing manifest type"
          return false
        elsif DependencyManifestFile.corresponding_manifest_type(path: manifest_filename) != manifest_type
          @argument_error = "Manifest filename and manifest type do not match"
          return false
        end

        true
      end

      # Public: Queries ElasticSearch for public repository IDs containing a specific filename.
      #
      # Returns Hash:
      #   - success - Bool - If the run was successful.
      #   - error - String - Why we weren't able to run. Contains argument errors if applicable.
      #   - enqueued_count - Integer - Number of enqueued jobs, if run was successful.
      def enqueue_jobs
        return { success: false, error: argument_error } unless valid_arguments?

        enqueued_count = 0

        index = Elastomer::Indexes::CodeSearch.new

        query = {
          query: {
            bool: {
              filter: {
                bool: {
                  must: [
                    { term: { filename: manifest_filename } },
                    { term: { public: true } },
                  ],
                },
              },
            },
          },
          _source: %w(filename repo_id),
        }

        scan = index.scan(query, type: "code", size: 100, scroll: "1m")

        scan.each_document do |doc|
          repository_id = doc["_source"]["repo_id"].to_i

          # Enqueue the repository ID we found to our backfill.
          DependencyGraphManifestBackfillJob.perform_later(repository_id, manifest_type)
          enqueued_count += 1
        end

        { success: true, enqueued_count: enqueued_count }
      end
    end
  end
end
