# frozen_string_literal: true

module DependencyGraph
  class UpdateManifestsMutation < Query
    def initialize(input:, backend: nil)
      @input = input
      super(backend: backend)
    end

    def execute
      backend.post <<~MUTATION, input: input
        mutation UpdateRepositoryManifests($input: UpdateRepositoryManifestsInput!) {
          updateRepositoryManifests(input: $input) {
            clientMutationId
          }
        }
      MUTATION
    end

    def execute!
      result = execute

      unless result.ok?
        raise DependencyGraph::Client::ApiError.new(result.error.message)
      end

      result
    end

    private

    attr_reader :input
  end
end
