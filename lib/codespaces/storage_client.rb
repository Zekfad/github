# frozen_string_literal: true

require "net/http"
require "uri"
require "json"
require "codespaces"

module Codespaces
  # Public: Interface to the Azure Storage queues
  class StorageClient < Client
    class Message
      attr_reader :id, :pop_receipt, :body

      def initialize(id:, pop_receipt:, body:)
        @id, @pop_receipt, @body = id, pop_receipt, body
      end

      def event_id
        body["id"]
      end
    end

    OAUTH_URL = "https://login.microsoftonline.com/"
    STORAGE_OAUTH_RESOURCE = "https://storage.azure.com/"

    attr_reader :storage_url, :queue_name

    def initialize(account_name:, **kwargs)
      @account_name = account_name
      @storage_url =
        GitHub::Config::CODESPACES_STORAGE_ACCOUNTS[:environments][Rails.env.to_sym][:url_pattern] % {account_name: account_name}
      @queue_name = GitHub::Config::CODESPACES_STORAGE_ACCOUNTS[:environments][Rails.env.to_sym][:queue_name]
      super **kwargs
    end

    # Gets messages from the environment-specific queue for in the Storage queue named `account_name`
    #
    # Returns an Array of Nokogiri::XML::NodeSet QueueMessage objects
    #
    # See https://docs.microsoft.com/en-us/rest/api/storageservices/get-messages#sample-response
    def get_messages
      with_log_context(storage_url: storage_url, queue_name: queue_name) do
        messages = parsed_storage_api(
          :get,
          "/#{queue_name}/messages",
          tags: ["codespaces_storage_client:get_messages,queue_name:#{queue_name}"]
        )

        messages.xpath("//QueueMessage").map do |message|
          Message.new(
            id: message.xpath("MessageId").text,
            pop_receipt: message.xpath("PopReceipt").text,
            body: GitHub::JSON.parse(Base64.decode64(message.xpath("MessageText").text))
          )
        end
      end
    end

    def delete_message(message_id:, pop_receipt:)
      with_log_context(storage_url: storage_url, queue_name: queue_name) do
        storage_api(
          :delete,
          "/#{queue_name}/messages/#{message_id}?popreceipt=#{pop_receipt}",
          body: nil,
          tags: ["codespaces_storage_client:delete_message,queue_name:#{queue_name}"]
        )
      end
    end

    def correlation_request_id(resp)
      resp.headers["x-ms-request-id"] if resp&.headers
    end

    private

    def storage_api(method, path, body: {}, tags:)
      start_time = GitHub::Dogstats.monotonic_time
      path = add_sas_key_to_path(path)

      resp = if method == :get
        storage_connection.get(path, body)
      else
        storage_connection.run_request(method, path, body, nil)
      end

      request_url = storage_url + path
      log_response(method, request_url, resp)

      caller_base_label = caller_locations(1, 1)[0].base_label
      all_tags = ["caller:#{caller_base_label}", "status:#{resp.status}"].concat(tags)
      GitHub.dogstats.timing_since("codespaces.client.arm_api.response", start_time, tags: all_tags)

      if resp.success?
        resp
      else
        raise BadResponseError, request_err_message("Bad response", method, request_url, resp)
      end
    rescue Faraday::TimeoutError
      raise TimeoutError, request_err_message("Timeout exceeded", method, request_url, resp)
    rescue Faraday::ConnectionFailed => e
      raise ConnectionFailed, request_err_message("Connection failed: #{e.message}", method, request_url, resp)
    end

    def parsed_storage_api(method, path, body: {}, tags:)
      resp = storage_api(method, path, body: body, tags: tags)
      Nokogiri::XML(resp.body) { |config| config.strict }
    rescue Nokogiri::XML::SyntaxError
      raise BadResponseError, request_err_message("Bad response", method, request_url, resp, "invalid XML")
    end

    def arm_client
      @arm_client ||= Codespaces::ArmClient.new(resource_provider: nil)
    end

    def fetch_sas_key
      @accounts ||= arm_client.list_storage_account_names
      @accounts[@account_name]
    end

    def add_sas_key_to_path(path)
      sas_key = fetch_sas_key
      raise "No SAS key found for the given account name: '#{@account_name}'" if sas_key.nil?

      u = URI(path)
      query_args = URI.decode_www_form(u.query || "") + URI.decode_www_form(sas_key)
      u.query = URI.encode_www_form(query_args)
      u.to_s
    end

    def storage_connection
      @storage_connection ||= connection_for(storage_url, retries: 3)
    end
  end
end
