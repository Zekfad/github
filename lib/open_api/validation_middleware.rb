# frozen_string_literal: true

module OpenApi
  class ValidationMiddleware
    def initialize(app)
      @app = app
    end

    def call(env)
      return @app.call(env) if GitHub.context[:openapi_skip_validation]

      env["openapi.validation_errors"] = []

      request   = Rack::Request.new(env)
      validator = OpenApi::Validator.new(OpenApi.description, request)

      request_errors = validator.validate_request

      status, headers, body = @app.call(env)

      response_errors = validator.validate_response(status, headers, body)

      # We append sinatra env related info here, after the `@app.call` so we have
      # access to it.
      env["openapi.validation_errors"] += append_env_to_errors(request_errors, env, status)
      env["openapi.validation_errors"] += append_env_to_errors(response_errors, env, status)

      [status, headers, body]
    end

    private

    def append_env_to_errors(errors, env, status)
      errors.map do |err|
        err.merge(
          github_route: env["sinatra.route"] || env["github.api.route"],
          github_controller: GitHub.context[:controller] || "UNKNOWN",
          request_status: status.to_s,
        )
      end
    end
  end
end
