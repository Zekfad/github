# frozen_string_literal: true

module OpenApi
  class Router
    class RequestOperation
      attr_reader :description, :parameters, :errors

      def initialize(description: nil, parameters: nil, errors: [])
        @description = description
        @parameters = parameters
        @errors = errors
      end

      def exists?
        @errors.empty?
      end
    end

    class Match
      attr_reader :path, :parameters

      def initialize(description_path, parameters)
        @path = description_path
        @parameters = parameters
      end

      def concrete_segments
        @path.split("/") | @parameters.keys
      end

      def templated?
        @parameters.any?
      end
    end

    def initialize(schema, request)
      @schema = schema
      @request = request
    end

    def request_operation
      # enterprise will request the api with /api/v3 prefix, but we don't
      # want to pass that when looking for a path in the description.
      request_path = @request.path.sub(/^\/api\/v3/, "")
      matches = @schema.paths.keys.map do |path|
        match_path(request_path: request_path, description_path: path)
      end.compact

      if matches.empty?
        return RequestOperation.new(
          errors: [{
            message: "Routing error for `#{@request.request_method} #{request_path}`: Could not find any path matching in OpenAPI description."
          }]
        )
      end

      # prefer higher concrete segment count if parameter count is equal
      best_match = if (matches.map { |m| m.parameters.count }.uniq.count == 1)
        matches.max_by { |m| m.concrete_segments.count }
      else
        matches.min_by { |m| m.parameters.count }
      end

      method    = @request.request_method.downcase
      operation = @schema.operation(method, best_match.path)

      if operation
        return RequestOperation.new(
          description: operation,
          parameters: best_match.parameters
        )
      else
        return RequestOperation.new(errors: [{
          message: "Routing error for `#{@request.request_method} #{request_path}`: Path was found but no operation for method could be found."
        }])
      end
    end

    private

    def match_path(request_path:, description_path:)
      request_path_fragments     = request_path.split("/")
      description_path_fragments = description_path.split("/")
      multisegment_param         = description_path.match(%r({[a-zA-Z]+\+}))

      parameters = {}

      if multisegment_param
        concrete_trailing_params     = []
        multisegment_param_fragments = []
        position_of_multisegment_param = description_path_fragments.index(multisegment_param[0])
        trailing_concrete_param_amount = description_path_fragments.length - (position_of_multisegment_param + 1)

        trailing_concrete_param_amount.times do
          concrete_trailing_params << description_path_fragments.pop
          request_path_fragments.pop
        end

        return unless request_path.end_with?(concrete_trailing_params.reverse.join("/"))

        description_path_fragments.delete_at(position_of_multisegment_param)

        while request_path_fragments.length > description_path_fragments.length
          multisegment_param_fragments  << request_path_fragments.pop
        end

        parameters[extract_template_parameter_name(multisegment_param[0])] = multisegment_param_fragments.reverse.join("/")

        request_path_fragments += concrete_trailing_params
        description_path_fragments += concrete_trailing_params
      end


      # No match if the two paths don't have the same amount of fragments
      return if (request_path_fragments.size != description_path_fragments.size)

      while request_path_fragments.size > 0
        # Compare each fragment one at a time to find a match
        request_path_fragment = request_path_fragments.shift
        description_path_fragment = description_path_fragments.shift

        if templateParam = extract_template_parameter_name(description_path_fragment)
          parameters[templateParam] = request_path_fragment
        else
          if request_path_fragment != description_path_fragment
            return nil
          end
        end
      end

      return Match.new(description_path, parameters)
    end

    def extract_template_parameter_name(path_fragment)
      matches = path_fragment.match(/{(.*)}/)
      return unless matches
      matches[1]
    end

    private_constant :Match
  end
end
