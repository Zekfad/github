# frozen_string_literal: true

module OpenApi
  module Description
    module_function

    def load_from_yaml(yaml)
      root = YAML.load(yaml)
      OpenApi::Description::Root.new(root)
    end

    def load_from_file(schema_path)
      root = YAML.load_file(schema_path)
      recursively_dereference(root, schema_path)
      OpenApi::Description::Root.new(root)
    end

    def load_as_hash(schema_path)
      root_path = Rails.root.join(schema_path).to_s
      root = YAML.load_file(root_path)
      recursively_dereference(root, root_path)
      root
    end

    # This modifies values in place which is a bit icky.
    # Consider building new values instead.
    def recursively_dereference(value, source_path)
      case value
      when Hash
        if value.key?("$ref")
          if value.size != 1 && !value["$ref"].include?("x-previews")
            raise "$ref is only allowed as the _only_ key in an object (found: #{value.inspect})"
          end
          ref_target = value.delete("$ref")
          if ref_target.start_with?("#")
            raise "TODO: implement #-refs"
          else
            # OpenAPI refs are relative to the _current file_,
            # so we have to know what file contains this ref.
            if source_path.include?("#")
              raise "TODO: implement refs with # in them"
            end
            current_dir = File.dirname(source_path)
            ref_path = File.expand_path(ref_target, current_dir)
            ref_value = YAML.load_file(ref_path)
            recursively_dereference(ref_value, ref_path)
            value.merge!(ref_value)
          end
        else
          value.each do |k, v|
            recursively_dereference(v, source_path)
          end
        end
      when Array
        value.each { |v| recursively_dereference(v, source_path) }
      else
        value
      end
    end
  end
end
