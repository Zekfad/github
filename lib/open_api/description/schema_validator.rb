  # frozen_string_literal: true

module OpenApi
  module Description
    class SchemaValidator
      ONE_OF_MATCH        = "ONE_OF_MATCH_ERROR"
      ANY_OF_MATCH        = "ANY_OF_MATCH_ERROR"
      UNEXPECTED_PROPERTY = "UNEXPECTED_PROPERTY"
      MISSING_PROPERTY    = "MISSING_PROPERTY"
      INVALID_SCHEMA_TYPE = "INVALID_SCHEMA_TYPE"

      RUBY_CLASS_TO_OPENAPI_TYPE = {
        "hash"       => "object",
        "nilclass"   => "null",
        "trueclass"  => "boolean",
        "falseclass" => "boolean",
      }

      def self.ruby_type_to_open_api(given)
        # Replace some Ruby-specific class names with JSON-oriented words
        default_openapi_type = given.class.name.downcase
        RUBY_CLASS_TO_OPENAPI_TYPE[default_openapi_type] || default_openapi_type
      end

      def self.inherited(klass)
        @validation_classes ||= {}
        @validation_classes[klass.schema_type] = klass
      end

      def self.for(type)
        @validation_classes[type]
      end

      def self.schema_type
        name.split("::").last.split("Validator").last.underscore.to_sym
      end

      class NotImplementedYetError < StandardError; end

      # Entrypoint from Schema
      def self.validate(value:, schema:, path: [], coerce: true)
        new(value: value, schema: schema, path: path, coerce: coerce).validate
      end

      attr_reader :value, :schema, :path, :coerce
      def initialize(value:, schema:, path:, coerce: true)
        @value, @schema, @path, @coerce = value, schema, path, coerce
      end

      def validate
        unless type_validator_klass
          result = Result.new
          result.add_errors({
            code: INVALID_SCHEMA_TYPE,
            message: "Not implemented yet: #{schema.type.inspect}, could not validate schema type (schema: #{schema.raw.inspect})",
            type: schema.type.inspect
          })
          return result
        end

        if value.nil?
          result = Result.new

          if !schema.nullable?
            result.add_errors(type_error(path, schema.type, value))
          end

          return result
        end

        type_validator_klass.validate(value: value, schema: schema, path: path, coerce: coerce)
      end

      private

      def type_error(path, expected, given)
        SchemaTypeError.new(path, expected, given).to_h
      end

      def type_validator_klass
        self.class.for(schema.type&.to_sym) || self.class.for(schema.subschema_keyword&.underscore&.to_sym)
      end
    end

    class Result
      attr_reader :errors

      def initialize
        @errors = []
      end

      def add_errors(errors)
        @errors |= Array.wrap(errors)
      end

      def to_a
        @errors
      end
    end

    class SchemaTypeError
      TYPE_MISMATCH = "TYPE_MISMATCH"

      attr_reader :expected, :given

      def initialize(path, expected, given)
        @path, @expected, @given = path, expected, given
      end

      def given_type
        SchemaValidator.ruby_type_to_open_api(given)
      end

      def message
        "Expected #{expected || 'schema'} but received #{given_type} (#{given_inspect})"
      end

      def to_h
        {
          code: TYPE_MISMATCH,
          expected: expected,
          given: given_type,
          message: message,
          path: @path
        }
      end

      private

      attr_reader :path

      def given_inspect
        given.nil? ? "null" : given.inspect
      end
    end

    class OneOfValidator < SchemaValidator
      def validate
        result = Result.new
        validated_schemas_count = 0
        child_errors = []
        schema.one_of.each_with_index do |possible_schema, index|
          recurse_result = SchemaValidator.validate(value: value, schema: possible_schema, path: path + ["oneOf/#{index}"], coerce: coerce)
          if recurse_result.errors.none?
            validated_schemas_count += 1
            child_errors << recurse_result.errors
          end
        end

        if validated_schemas_count == 0
          result.add_errors({
            code: ONE_OF_MATCH,
            path: path.join("/"),
            message: "Failed to satisfy any of the allowed schemas",
            child_errors: child_errors.flatten,
          })
        elsif validated_schemas_count > 1
          result.add_errors({
            code: ONE_OF_MATCH,
            path: path.join("/"),
            message: "Validated by more than one schema, but only one schema was expected to be valid.",
            child_errors: child_errors.flatten,
          })
        end

        result
      end
    end

    class AnyOfValidator < SchemaValidator
      def validate
        result = Result.new
        recurse_results = schema.any_of.each_with_index.map do |possible_schema, index|
          SchemaValidator.validate(value: value, schema: possible_schema, path: path + ["anyOf/#{index}"], coerce: coerce)
        end

        if  recurse_results.all? { |r| r.errors.any? }
          result.add_errors({
            code: ANY_OF_MATCH,
            path: path.join("/"),
            message: "Failed to satisfy any of the allowed schemas",
            child_errors: recurse_results.map(&:errors).flatten,
          })
        end

        result
      end
    end

    class AllOfValidator < SchemaValidator
      def validate
        result = Result.new
        schema.all_of.each_with_index do |possible_schema, index|
          if possible_schema.type != "object"
            raise NotImplementedYetError, "`allOf` is only implemented with `type: object`; improve the implementation if you need something else. (Got: #{possible_schema.raw.inspect})"
          end
          recurse_result = SchemaValidator.validate(value: value, schema: possible_schema, path: path + ["allOf/#{index}"], coerce: coerce)
          result.add_errors(recurse_result.errors)
        end

        result
      end
    end

    class StringValidator < SchemaValidator
      def validate
        result = Result.new

        return result if value.nil? && schema.nullable?

        result.add_errors(type_error(path, "string", value)) if !value.is_a?(String)
        result
      end
    end

    class NumberValidator < SchemaValidator
      def validate
        result = Result.new

        return result if value.nil? && schema.nullable?

        result.add_errors(type_error(path, "number", value)) if !value.is_a?(Numeric)
        result
      end
    end

    class ArrayValidator < SchemaValidator
      def validate
        result = Result.new

        return result if value.nil? && schema.nullable?

        if value.is_a?(Array)
          item_schema = schema.items
          value.each_with_index do |v, idx|
            recurse_result = SchemaValidator.validate(schema: item_schema, value: v, path: path + ["items"], coerce: coerce)
            result.add_errors(recurse_result.errors)
          end
        else
          result.add_errors(type_error(path, "array", value))
        end

        result
      end
    end

    class ObjectValidator < SchemaValidator
      def validate
        result = Result.new

        return result if value.nil? && schema.nullable?

        if value.is_a?(Hash)
          expected_members = schema.properties

          additional_keys = value.keys - expected_members.keys
          if additional_keys.any?
            additional_properties_schema = schema.additional_properties
            # Default is `additionalProperties: true`
            case additional_properties_schema
            when false
              additional_keys.map do |key|
                property_value = value.fetch(key)
                property_type = SchemaValidator.ruby_type_to_open_api(property_value)

                result.add_errors({
                  code: UNEXPECTED_PROPERTY,
                  value: property_value.inspect,
                  type: property_type,
                  path: (path + ["properties", key]).join("/"),
                  message: "This key (\"#{key}\") with a type of #{property_type} is not expected, it should be removed.",
                })
              end
            when true, {}, nil
              # This means that anything goes
            else
              # A schema was given for any additional keys
              additional_keys.each do |additional_key|
                recurse_result = SchemaValidator.validate(
                  schema: additional_properties_schema,
                  value: value[additional_key],
                  path: path + ["properties", additional_key],
                  coerce: coerce
                )

                recurse_result.errors.each { |err| result.add_errors(err) }
              end
            end
          end

          missing_required_keys = (schema.required || []) - value.keys
          if missing_required_keys.any?
            missing_required_keys.map do |key|
              result.add_errors({
                code: MISSING_PROPERTY,
                path: (path + ["properties", key]).join("/"),
                message: "This key (\"#{key}\") is required, but not present. Include it in the object.",
              })
            end
          end

          expected_members.each do |name, key_spec|
            if value.key?(name)
              recurse_result = SchemaValidator.validate(schema: key_spec, value: value[name], path: path + ["properties", name], coerce: coerce)
              recurse_result.errors.each { |err| result.add_errors(err) }
            end
          end
        else
          result.add_errors(type_error(path, "object", value))
        end

        result
      end
    end

    class BooleanValidator < SchemaValidator
      def validate
        result = Result.new

        return result if value.nil? && schema.nullable?

        unless value == true || value == false
          result.add_errors(type_error(path, "boolean", value))
        end

        result
      end
    end

    class IntegerValidator < SchemaValidator
      def validate
        result = Result.new

        return result if value.nil? && schema.nullable?

        coerced = if coerce
          begin
            Integer(value)
          rescue ArgumentError, TypeError
            nil
          end
        else
          value
        end

        if !coerced.is_a?(Integer)
          result.add_errors(type_error(path, "integer", value))
        end

        result
      end
    end
  end
end
