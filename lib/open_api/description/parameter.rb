# frozen_string_literal: true

module OpenApi
  module Description
    class Parameter
      attr_reader :raw

      def initialize(parameter, index)
        @raw    = parameter
        @schema = Schema.new(@raw["schema"])
        @index = index
      end

      def validate(value)
        @schema.validate(value).to_a.map do |error|
          error.merge(path: [json_pointer, "schema", error[:path]].join("/"))
        end
      end

      def required?
        @raw["required"]
      end

      def name
        @raw["name"]
      end

      def json_pointer
        "/parameters/#{@index}"
      end
    end
  end
end
