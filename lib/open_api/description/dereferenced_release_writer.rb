# frozen_string_literal: true

module OpenApi
  module Description
    class DereferencedReleaseWriter < ReleaseWriter
      def path(format: :json)
        OpenApi.root.join(
          "generated",
          File.basename(@release.filename, ".yaml"),
          "dereferenced",
          File.basename(@release.filename, ".yaml") << ".deref.#{format}"
        )
      end

      def content
        return @content if defined?(@content)
        @content = Marshal.load(Marshal.dump(@release.content))
        expand(@content, OpenApi.root.join(@release.filename))
        replace_variables(@content)
        @content
      end

      def serialize(content, format:)
        if format == :json
          JSON.pretty_generate(content)
        elsif format == :yaml
          YAML.dump(content)
        else
          raise ArgumentError, "Unhandled format #{format}"
        end
      end

      private

      # Private: Recursively expand $ref values in a datastructure, relative to a path,
      #          stripping any disallowed extended properties.
      #
      # value - The data
      # source_path - a Pathname of the source file, used to resolve relative references
      #
      # Modifications are made in-place.
      def expand(value, source_path)
        # Expand
        case value
        when Hash
          OpenApi::Description::Overlay.apply(value, @release)
          strip_extended_properties!(value)

          if value.key?("$ref")
            if value.size != 1 && !value["$ref"].include?("components/x-previews")
              raise "$ref is only allowed as the _only_ key in an object (found: #{value.inspect})"
            end
            # Mark this file as expanded for any subsequent expansions
            ref_target = value.delete("$ref")
            if ref_target.start_with?("#")
              raise ArgumentError, "Inline components not supported: #{ref_target}"
            else
              ref_path = source_path.dirname.join(ref_target)
              if ref_path.exist?
                ref_value = YAML.load_file(ref_path)
                expand(ref_value, ref_path)
                value.merge!(ref_value)
              else
                raise ArgumentError, "Bad $ref #{ref_target.inspect} from #{source_path}"
              end
            end
          else
            value.each do |k, v|
              expand(v, source_path)
            end
          end
        when Array
          value.each { |v| expand(v, source_path) }
        else
          value
        end
      end
    end
  end
end
