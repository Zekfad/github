# frozen_string_literal: true

module OpenApi
  module Description
    class ReleaseWriter
      VARIABLE_REGEX = /\${(\w+)}/

      def self.write(release, format: :json)
        new(release).write(format: format)
      end

      def self.content(release)
        new(release).content
      end

      def initialize(release)
        @release = release
      end

      # Path to the output file
      def path
        raise NotImplementedError, "Must be implemented"
      end

      # Build/reformat release content
      def content
        raise NotImplementedError, "Must be implemented"
      end

      # Serialize to write
      def serialize(content)
        raise NotImplementedError, "Must be implemented"
      end

      def write(format:)
        # Ensure the directory exists
        out_path = path(format: format)
        out_path.dirname.mkpath
        # Write the output
        wrote_bytes = out_path.write(serialize(content, format: format))
        # Return some stats for reporting
        [out_path.relative_path_from(Rails.root), wrote_bytes]
      end

      # Private: Strip any disallowed x- properties.
      #
      # value - a Hash (node in the document)
      #
      # Makes changes in-place.
      def strip_extended_properties!(value)
        value.reject! { |k, v| k.start_with?("x-") && k != "x-github" }
      end

      private

      # Recursively replaces variables ${var} in place
      def replace_variables(value)
        if value.is_a?(Hash)
          value.each do |key, v|
            replace_variables(v)
          end
        elsif value.is_a?(Array)
          value.each { |v| replace_variables(v) }
        elsif value.is_a?(String)
          matches = value.scan(VARIABLE_REGEX).flatten
          matches.each do |match|
            variable_value = @release.variables[match]
            if variable_value.nil?
              raise "Release must configure variable #{match}"
            end
            value.gsub!(/\${#{match}}/, variable_value)
          end
        end
      end
    end
  end
end
