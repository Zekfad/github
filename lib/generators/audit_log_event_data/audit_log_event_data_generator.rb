# frozen_string_literal: true

class AuditLogEventDataGenerator < Rails::Generators::NamedBase
  argument :attributes, type: :array, default: [], banner: "field:type field:type"

  class_option :view, type: :boolean, default: true, desc: "Generate partials to render this action in views"
  class_option :org_audit_log, type: :boolean, default: true, desc: "Render this action in the organization audit log"

  def create_event_data_file
    create_file "lib/platform/objects/audit_log/#{file_name}_event_data.rb", <<~FILE
      # frozen_string_literal: true

      module Platform
        module Objects
          module AuditLog
            class #{class_name}EventData < Platform::Objects::Base
              description "Metadata for an audit entry with action #{action_name}"
              visibility :under_development
              #{minimum_accepted_scopes}

              areas_of_responsibility :audit_log

              # Determine whether the viewer can access this object via the API (called internally).
              # This is where Egress checks for OAuth scopes and GitHub Apps go.
              # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
              def self.async_api_can_access?(permission, audit_entry)
                # Access to this object is managed by its parent AuditEntry.
                true
              end

              # Determine whether the viewer can see this object (called internally).
              # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
              def self.async_viewer_can_see?(permission, audit_entry)
                # Access to this object is managed by its parent AuditEntry.
                true
              end

      #{fields_declarations}
            end
          end
        end
      end
    FILE
  end

  def create_event_data_test_file
    create_file "test/lib/platform/objects/audit_log/#{file_name}_event_data_test.rb", <<~FILE
      # frozen_string_literal: true

      require_relative "../../../../test_helper"

      class Platform::Objects::AuditLog::#{class_name}EventDataTest < GitHub::TestCase
        include Platform::InterfaceHelpers

        fixtures do
          # TODO: Setup users, orgs, businesses. These are used by auth checks.
          @actor = create(:user)

          @audit_entry = AuditEntry.insert(
            document_id: "audit",
            document: {
              action: "#{action_name}",
              actor: @actor.login,
              actor_id: @actor.id,
              data: {
      #{indent(fields_values_hash, 10)}
              }
            },
            created_at: AuditEntry.milliseconds_to_time(1542219809799),
            shard_key: "1",
          )
        end

        INTERNAL_QUERY = <<~'GRAPHQL'
          query($id: ID!) {
            node(id: $id) {
              ... on AuditEntry {
                action
                actorDatabaseId
                actorLogin
                createdAt
                # TODO: list all relevant AuditEntry fields

                data {
                  ... on #{class_name}EventData {
      #{indent(fields_as_camelcase.join("\n"), 14)}
                  }
                }
              }
            }
          }
        GRAPHQL

        PUBLIC_QUERY = <<~'GRAPHQL'
          query($id: ID!) {
            node(id: $id) {
              ... on AuditEntry {
                action
                actorLogin
                createdAt
                # TODO: list all relevant AuditEntry fields that are public

                data {
                  ... on #{class_name}EventData {
                    # TODO: remove internal only fields from this query
      #{indent(fields_as_camelcase.join("\n"), 14)}
                  }
                }
              }
            }
          }
        GRAPHQL

        test "internal queries" do
          res = execute_query(INTERNAL_QUERY, :$id => @audit_entry.global_relay_id, :target => :internal, :viewer => @actor)
          assert_equal "#{action_name}", res.data["node"]["action"]
          assert_equal @actor.id, res.data["node"]["actorDatabaseId"]
          assert_equal @actor.login, res.data["node"]["actorLogin"]
          assert_equal "2018-11-14T18:23:29.799Z", res.data["node"]["createdAt"]
      #{indent(fields_assertions, 4)}
        end

        test "returns errors when querying internal fields with public target" do
          # This test will not run until AuditEntry, AuditEntryData, and
          # #{class_name}EventData all have public visibility. We introduce it as a
          # form of future-proofing to ensure that we do not leak internal fields
          # when these objects are made public.
          #
          # In order to run this test locally, open up the three objects listed above
          # and replace `visibility :under_development` with `visibility :public`.
          # Do not commit this change.
          if Platform::Objects::AuditLog::#{class_name}EventData.visibility.include?(:public)
            errors = execute_query_with_errors(INTERNAL_QUERY, :$id => @audit_entry.global_relay_id, :target => :public, :viewer => @actor)
            assert_equal 1, errors.count
            assert errors[0].has_value? "Field 'actorDatabaseId' doesn't exist on type 'AuditEntry'"
          end
        end

        test "public queries" do
          # This targets the internal API as the objects queried are still under
          # under_development. It will be updated to target: :public when the objects
          # are productionized.
          res = execute_query(PUBLIC_QUERY, :$id => @audit_entry.global_relay_id, :target => :internal, :viewer => @actor)
          assert_equal "#{action_name}", res.data["node"]["action"]
          assert_equal @actor.login, res.data["node"]["actorLogin"]
          assert_equal "2018-11-14T18:23:29.799Z", res.data["node"]["createdAt"]
      #{indent(fields_assertions, 4)}
        end
      end
    FILE
  end

  # Adds newly generated EventData object to AuditEntryData union
  def add_event_data_to_union
    new_type = "Objects::AuditLog::#{class_name}EventData,\n"
    sentinel = /possible_types\(\s*\n/m

    in_root do
      inject_into_file "lib/platform/unions/audit_entry_data.rb", indent(new_type, 8), after: sentinel, verbose: false, force: false
    end
  end

  # Creates new partial and adds it to the audit log action view.
  def create_view_file
    if options[:view]
      create_file "app/views/audit_log/platform_actions/_#{file_name}.html.erb", <<~VIEW
        <%graphql
          fragment Fragment on AuditEntry {
            data {
              ... on #{class_name}EventData {
                # TODO: required fields
              }
            }
          }
        %>
        <% entry = Views::AuditLog::PlatformActions::#{class_name}::Fragment.new(entry) %>

        # TODO: add view
      VIEW

      case_statement = "<% when \"#{action_name}\" %><%= render partial: \"audit_log/platform_actions/#{singular_name}\", locals: { entry: entry } %>\n"
      sentinel = /<% case action %>\s*\n/m
      in_root do
        inject_into_file "app/views/audit_log/_platform_action.html.erb", case_statement, after: sentinel, verbose: false, force: false
      end
    end
  end

  # Adds new fragment to the GraphQL query in the platform audit log entry view.
  # This enables the partial to be rendered in the org audit log.
  def add_partial_to_org_audit_log
    if options[:org_audit_log]
      fragment = "...Views::AuditLog::PlatformActions::#{class_name}::Fragment\n"
      sentinel = /...Views::AuditLog::PlatformActions::RepoCreate::Fragment*\s*\n/m
      in_root do
        inject_into_file "app/views/orgs/audit_log/_platform_audit_log_entry.html.erb", indent(fragment, 4), after: sentinel, verbose: false, force: false
      end
    end
  end

  # Adds an integration test to verify the new partial is rendered correctly
  # in the org audit log.
  def add_org_audit_log_integration_test
    if options[:org_audit_log]
      sentinel = "  # GraphQL view backed partials tests #\n"
      view_test = <<~TEST
        context "#{action_name}" do
          test "uses graphql views to render partial" do
            with_es_refresh do
              log({
                _document_id: "#{file_name}_doc",
                action: "#{action_name}",
                actor: @owner.login,
                actor_id: @owner.id,
                actor_location: { country_name: "USA" },
                org: @org.login,
                org_id: @org.id,
                data: {
        #{indent(fields_values_hash, 10)}
                }
              })
            end

            GitHub.flipper[:org_audit_log_via_graphql].enable

            as @owner
            get "/organizations/\#{@org}/settings/audit-log", params: { q: "action:#{action_name}" }
            assert_response :success
            assert_select(audit_results_selector) do |dom_node|
              assert_select "a[href=\\"/organizations/\#{@org}/settings/audit-log?q=action%3A#{action_name}\\"]", text: "#{action_name}"
              # TODO: assertions specific to the partial for this action
              # assert_match "expected text", dom_node.text.squish
            end
          end
        end
      TEST

      in_root do
        inject_into_file "test/integration/orgs/audit_log_controller_test.rb", indent(view_test, 2), after: sentinel, verbose: false, force: false
      end
    end
  end

  private

  def fields_declarations
    attributes.map do |attr|
      if attr.type.downcase == :url
        <<-URL_FIELD_DEFINITION
        url_fields prefix: #{attr.name.chomp("_url").inspect}, description: \"Please give me a clear description!\" do |audit_entry|
          # TODO: build an Addressable::Template to return a URL
          # template = Addressable::Template.new("/organizations/{org}")
          # template.expand(org: audit_entry.get(:org))
        end
        URL_FIELD_DEFINITION
      else
        <<-FIELD_DEFINITION
        field :#{attr.name}, #{attr.type.capitalize}, null: true, description: \"Please give me a clear description!\"
        def #{attr.name}
          @object.get :#{attr.name}
        end
        FIELD_DEFINITION
      end
    end.join("\n")
  end

  def fields_as_camelcase
    attributes.map { |attr| attr.name.camelize(:lower) }
  end

  def fields_assertions
    fields_as_camelcase.map { |field| "assert_equal value, res.data[\"node\"][\"data\"][\"#{field}\"]" }.join("\n")
  end

  def fields_values_hash
    attributes.map { |attr| "#{attr.name}: value" }.join(",\n")
  end

  def action_name
    name
  end

  def file_name
    name.gsub(".", "_")
  end

  def minimum_accepted_scopes
    scopes = []

    scopes << "read:user" if AuditLogEntry.user_action_names.include? action_name
    scopes << "admin:org" if AuditLogEntry.organization_action_names.include? action_name
    scopes << "admin:enterprise" if AuditLogEntry.business_action_names.include? action_name

    if scopes.empty?
      "scopeless_tokens_as_minimum"
    else
      "minimum_accepted_scopes [#{scopes.map { |scope| "\"#{scope}\"" }.join(", ")}]"
    end
  end
end
