# frozen_string_literal: true
namespace :vulnerabilities_sync do
  task :run => :environment do
    VulnerabilitySyncWithDotcom.perform_now
  end
end
