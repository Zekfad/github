# frozen_string_literal: true

namespace :api do
  namespace :schema do
    desc "Normalize JSON schema files"
    task :fixup do
      require_relative "../api_schema/json_file"
      Dir.glob("app/api/schemas/v3/schemas/*json").each do |path|
        file = ApiSchema::JSONFile.new(path)
        file.data["id"] = file.canonical_id

        Array(file.data["links"]).each do |link|
          link.delete("description")
        end

        next if file.normalized?

        File.open(path, "w") do |f|
          f.puts JSON.pretty_generate(file.data)
        end
      end
    end
  end

  # Sample output is a header (listing the class name) followed by
  # the verb + endpoint. For example:
  #
  # ### Within Api::Git
  #
  #     GET /repositories/:repository_id/git
  desc "List all the API endpoints with their verbs."
  task :routes => :environment do
    raise(StandardError, "You can only run this task in dotcom development mode") unless Rails.env.development? && !GitHub.enterprise?

    mode = ENV["MODE"] || "default"

    if mode == "modes" || mode == "help"
      puts <<-HELP
USAGE: [MODE=<mode>] [NAMESPACE=Api::<namespace>] bin/rake api:routes

Available modes:
  * modes             This. Show what modes are available.
  * help              This. Show what modes are available.
  * default           List available routes, grouped by API namespace.
  * audit             List available routes with additional details about GitHub Apps in markdown tables per API namespace.
  * doc-urls          List endpoints that are missing documentation URLs.
  * permissions       List endpoints by their granular permissions, formatted for developer documentation site.
  * statistics        Show summary of endpoint availability for GitHub Apps.
  * user-to-server    List of endpoints enabled for user requests via GitHub Apps, formatted for developer documentation site.
  * wip-pr            Print a template for a WIP pull request that enables endpoints in an API namespace for GitHub Apps.
  * ownership         List available routes with ownership information in csv format
      HELP
      exit 0
    end

    if mode != "default"
      # Only require these gems when we know we need rubocop and parser
      require "rubocop"
      require "github/parser_require_wrapper"
    end

    report = ApiRoutes::DefaultReport
    case ENV["MODE"]
    when "audit"
      report = ApiRoutes::AuditReport
    when "user-to-server"
      report = ApiRoutes::UserToServerReport
    when "doc-urls"
      report = ApiRoutes::MissingDocumentationUrlsReport
    when "permissions"
      report = ApiRoutes::GranularPermissionsReport
    when "statistics"
      report = ApiRoutes::StatisticsReport
    when "ownership"
      report = ApiRoutes::OwnershipReport
    when "wip-pr"
      raise(StandardError, "Please define the NAMESPACE environment variable (e.g. Api::Repositories)") if ENV["NAMESPACE"].to_s.empty?
      report = ApiRoutes::WipPrReport
    end

    SKIPPABLE_FILES = [
      %r{app/api/serializer},
      %r{app/api/access_control},
    ]
    glob = File.join(Rails.root, "app", "api", "**", "*.rb")
    Dir[glob].sort.each do |file|
      next if SKIPPABLE_FILES.any? { |s| file.match(s) }
       require_dependency file
    end

    report.new(ApiRoutes::Namespaces.in(Api::App, collector: report.collector, filter: ENV["NAMESPACE"])).print
  end
end
