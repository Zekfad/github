# frozen_string_literal: true

namespace :enterprise do
  namespace :actions do
    task :create , [:app_url, :webhook_url, :webhook_secret, :insecure_webhook, :client_key, :client_secret, :public_key_file] => [:environment] do |t, args|
      return unless GitHub.enterprise?
      public_key = File.read(args[:public_key_file])

      Apps::Internal::Actions.seed_database!(
        app_url: args[:app_url],
        webhook_url: args[:webhook_url],
        webhook_secret: args[:webhook_secret],
        insecure_ssl: args[:insecure_webhook],
        client_key: args[:client_key],
        client_secret: args[:client_secret],
        public_key: public_key)
    end
  end
end
