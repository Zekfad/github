# frozen_string_literal: true

# Task to populate the DependencyGraphManifestBackfillJob queue by searching ElasticSearch for specific manfiests.
#   Examples:
#
#     ### Finding Public Repo IDs that contain a file in ElasticSearch ###
#     #
#     # Just run it without specifying an ES Cluster or Index (for local development)
#     bin/rake dependency_graph_manifest_backfill:enqueue_public_from_es[gemfile,"Gemfile"]
#
#     # Specify a desired ES Cluster or Index
#     ES_CLUSTER=some-other-cluster ES_INDEX=super-code-search bin/rake dependency_graph_manifest_backfill:enqueue_public_from_es[gemfile,"Gemfile"]
#
#     ### Enqueue all private repositories with Dependency Graph enabled ###
#     #
#     bin/rake dependency_graph_manifest_backfill:enqueue_private MANIFEST_TYPES='composer_json,composer_lock'
#
###############################################################################
#
#                              *** DANGER ***
#
# This script uses Elasticsearch's "scan and scroll" query, documented here:
# https://www.elastic.co/guide/en/elasticsearch/reference/5.6/search-request-scroll.html
#
# Scan and scroll queries maintain a cursor on the server side throughout the
# session and this cursor also holds old shard fragments active at Scroll ID
# initialization time without releasing them for cleanup until the session ends.
#
# This script is intended for use on our ES5 clusters, contact #pe-search if you're working with ES2
#
# This can be DANGEROUS FOR THE CLUSTER. Please tell #pe-search when you're planning
# on running this script so they can advise and have a look at the query, your params, etc.
#
# * If you are considering running more than one instance of the script in parallel, talk to #pe-search first!
# * Don't get too creative updating the scroll cursor size or timeout without review.
# * Do add filter clauses to the query to be as specific in your filtering as possible.
#
# Stop running the script if:
# * Your scroll ID or queries are timing out or returning errors!
# * Haystack buckets "github" and "github-timeouts" start getting active with Elastomer errors
# * DD graphs or alerts blow up: https://app.datadoghq.com/dashboard/i2q-kyx-ngr/searchquerymetrics
#
###############################################################################

ES_USAGE_EXAMPLE = "bin/rake dependency_graph_manifest_backfill:enqueue_public_from_es[package_json,\"package.json\"]".freeze
PRIV_USAGE_EXAMPLE = "bin/rake dependency_graph_manifest_backfill:enqueue_private MANIFEST_TYPES='composer_json,composer_lock'".freeze
FILE_USAGE_EXAMPLE = "bin/rake dependency_graph_manifest_backfill:enqueue_file MANIFEST_TYPES='composer_json,composer_lock' FILE_PATH='/home/zackfern/ids.csv'".freeze

namespace :dependency_graph_manifest_backfill do
  ## Enqueue from Elasticsearch
  #
  # This mechanism talks directly to Elasticsearch in order to enqueue repository IDs from a search query.
  # This mechanism *does not* support finding multiple manifests at the same time, given that the query is
  # dependent on a single file name.
  #
  desc "Find the ID of all public repos containing a certain filename and enqueue them for a specific manifest."
  task :enqueue_public_from_es, [:manifest_type, :manifest_filename] => [:environment] do |task, args|
    es_backfiller = DependencyGraph::Backfill::Elasticsearch.new(
      manifest_filename: args[:manifest_filename],
      manifest_type: args[:manifest_type],
    )

    unless es_backfiller.valid_arguments?
      puts es_backfiller.argument_error
      puts "Usage example: #{ES_USAGE_EXAMPLE}"
      exit
    end

    output = es_backfiller.enqueue_jobs

    if output[:success]
      puts "Enqueued #{output[:enqueued_count]} repositories to the DependencyGraphManifestBackfillJob"
    else
      puts "Something went wrong when trying to enqueue DependencyGraphManifestBackfillJobs. #{output.inspect}"
    end
  end

  ## Enqueue all repositories opted-in to Dependency Graph, most of which are private.
  #
  # This mechanism finds all of the repository IDs which have a configuration option on them which enables the Dependency
  # Graph and Content Analysis. This mechanism supports finding multiple manifests at the same time. This is recommended
  # given that we'll be iterating over a lot of repos, so we might as well get as much done at one time as we can!
  #
  desc "Enqueue the ID of all private repos that have Dependency Graph enabled for a specific manifest."
  task :enqueue_private => [:environment] do |task, args|
    manifest_types = parse_types_input(ENV.fetch("MANIFEST_TYPES", nil))
    private_backfiller = DependencyGraph::Backfill::PrivateRepositories.new(
      manifest_types: manifest_types,
      maximum_jobs: ENV.fetch("MAXIMUM_JOBS", nil),
      start_at_id: ENV.fetch("START_AT_ID", nil),
    )

    unless private_backfiller.valid_arguments?
      puts private_backfiller.argument_error
      puts "Usage example: #{PRIV_USAGE_EXAMPLE}"
      exit
    end

    output = private_backfiller.enqueue_jobs

    if output[:success]
      puts "Enqueued #{output[:enqueued_count]} repositories to the DependencyGraphManifestBackfillJob"
    else
      puts "Something went wrong when trying to enqueue DependencyGraphManifestBackfillJobs. #{output.inspect}"
    end
  end

  desc "Enqueue all of the IDs in a specific CSV file."
  task :enqueue_file => [:environment] do
    manifest_types = parse_types_input(ENV.fetch("MANIFEST_TYPES", nil))
    file_path = ENV.fetch("FILE_PATH", nil)
    file_backfiller = DependencyGraph::Backfill::FromFile.new(
      file_path: file_path,
      manifest_types: manifest_types,
      maximum_jobs: ENV.fetch("MAXIMUM_JOBS", nil),
      start_at_id: ENV.fetch("START_AT_ID", nil),
    )

    unless file_backfiller.valid_arguments?
      puts file_backfiller.argument_error
      puts "Usage example: #{FILE_USAGE_EXAMPLE}"
      exit
    end

    output = file_backfiller.enqueue_jobs

    if output[:success]
      puts "Enqueued #{output[:enqueued_count]} repositories to the DependencyGraphManifestBackfillJob"
    else
      puts "Something went wrong when trying to enqueue DependencyGraphManifestBackfillJobs. #{output.inspect}"
    end
  end
end

# Helper method to take input from the user and split it into an Array of Symbols for our backfill classes.
# Returns nil or an Array
def parse_types_input(types)
  return if types.nil?
  types.split(",").collect { |type| type.strip.to_sym }
end
