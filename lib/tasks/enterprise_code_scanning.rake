# frozen_string_literal: true

namespace :enterprise do
  namespace :code_scanning do
    task :create , [] => [:environment] do |t, args|
      return unless GitHub.enterprise?
      Apps::Internal::CodeScanning.seed_database!
    end
  end
end
