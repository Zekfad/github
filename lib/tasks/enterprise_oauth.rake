# frozen_string_literal: true
namespace :enterprise do
  namespace :oauth do
    # Attach OAuth applications to the instance of GitHub running inside the Enterprise VM.
    #
    # app: is the name of the application to register.
    # client_id: is the generated auth client id. It can be found at /etc/github/APP_NAME_client_id.
    # secret: is the generated auth client secret. It can be found at /etc/github/APP_NAME_client_secret.
    #
    # Usage:
    #
    # Call this task from your cookbooks. This is an example with gist:
    #
    # client_id = "$(cat /etc/github/gist_oauth_client_id)"
    # secret    = "$(cat /etc/github/gist_oauth_secret)"
    #
    # bash 'setup gist oauth app' do
    #   source <<-CODE
    # . #{node.github.env_sh}
    # bin/rake "enterprise:oauth:add[gist,#{client_id},#{secret}]"
    # CODE
    # end
    #
    desc "create oauth app on the trusted user"
    task :add, [:app, :client_id, :secret, :url, :callback_url] => [:environment, "db:create_org"] do |t, args|
      if GitHub.enterprise?
        OauthApplication.register_trusted_application(args[:app], args[:client_id], args[:secret], args[:url], args[:callback_url])
      end
    end
  end
end
