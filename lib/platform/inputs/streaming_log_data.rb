# frozen_string_literal: true

module Platform
  module Inputs
    class StreamingLogData < Platform::Inputs::Base
      areas_of_responsibility :checks
      description "Streaming log metadata."

      argument :url, Scalars::URI, "The streaming log url.", required: true
    end
  end
end
