# frozen_string_literal: true

module Platform
  module Inputs
    class BrowserStat < Platform::Inputs::Base
      description "A web browser stat."

      visibility :internal

      argument :timestamp, Int, "UNIX timestamp of stat in milliseconds.", required: false
      argument :logged_in, Boolean, "True if a user is signed in with a session.", default_value: false, required: false

      argument :request_id, String, "Request UUID associated with reported stats.", required: false
      argument :request_url, Scalars::URI, "Request URL associated with reported stats.", required: false

      argument :pjax_duration, Integer, "Request's PJAX duration in milliseconds", required: false

      argument :downloaded_bundles, [String], "A list of asset bundles downloaded.", required: false

      argument :navigation_timings, [Inputs::BrowserPerformanceNavigationTiming], <<~MD, required: false
        A list of `PerformanceNavigationTiming` data retrieved via `performance.getEntriesByType('navigation')`.
      MD

      argument :resource_timings, [Inputs::BrowserPerformanceResourceTiming], <<~MD, required: false
        A list of `PerformanceResourceTiming` data retrieved via `performance.getEntriesByType('resource')`.
      MD

      argument :web_vital_timings, [Inputs::BrowserPerformanceWebVitalTiming], <<~MD, required: false
        A list of Web Vitals measurements.
      MD

      argument :increment_key, Enums::BrowserIncrementKey, "Increments DataDog counter.", required: false

      argument :hydro_event_payload, String, "A serialized Hydro event payload.", required: false
      argument :hydro_event_hmac, String, "A HMAC of the serialized Hydro event payload.", required: false
      argument :visitor_payload, String, "A serialized visitor payload.", required: false
      argument :visitor_hmac, String, "A HMAC of the serialized visitor payload.", required: false
      argument :hydro_client_context, String, "A serialized client information object", required: false
    end
  end
end
