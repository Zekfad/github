# frozen_string_literal: true

module Platform
  module Inputs
    class SponsorsMembershipOrder < Platform::Inputs::Base
      description "Ordering options for Sponsors membership connections."
      visibility :internal

      argument :field, Enums::SponsorsMembershipOrderField, "The field to order memberships by.", required: true
      argument :direction, Enums::OrderDirection, "The ordering direction.", required: true
    end
  end
end
