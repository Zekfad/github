# frozen_string_literal: true

module Platform
  module Inputs
    class FileChange < Platform::Inputs::Base
      graphql_name "FileChange"
      description "Describes a change to a file to be applied in a new commit."
      visibility :internal

      argument :is_deletion, Boolean, "True if we are removing the file.", required: false, default_value: false

      argument :path, String, "The file path for this content.", required: true

      argument :content, String, "The new content of this file.", required: false

      argument :new_path, String, "The path to which an existing file will be moved.", required: false
    end
  end
end
