# frozen_string_literal: true
module Platform
  module Inputs
    class Base < GraphQL::Schema::InputObject
      extend Platform::Objects::Base::AreasOfResponsibility
      extend Platform::Objects::Base::FeatureFlag
      extend Platform::Objects::Base::MobileOnly
      extend Platform::Objects::Base::Visibility
      extend Platform::Objects::Base::Previewable

      argument_class Platform::Objects::Base::Argument
    end
  end
end
