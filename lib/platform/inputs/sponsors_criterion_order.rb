# frozen_string_literal: true

module Platform
  module Inputs
    class SponsorsCriterionOrder < Platform::Inputs::Base
      description "Ordering options for Sponsors criteria connections."
      visibility :internal

      argument :field, Enums::SponsorsCriterionOrderField, "The field to order criteria by.", required: true
      argument :direction, Enums::OrderDirection, "The ordering direction.", required: true
    end
  end
end
