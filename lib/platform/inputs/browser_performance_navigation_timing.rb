# frozen_string_literal: true

module Platform
  module Inputs
    class BrowserPerformanceNavigationTiming < Platform::Inputs::Base
      description <<~MD
        https://w3c.github.io/navigation-timing/
      MD

      visibility :internal

      argument :name, Scalars::URI, <<~MD, required: true
        The name attribute must return the DOMString value of the address of the current document.
      MD

      argument :entry_type, String, <<~MD, required: true
        The entryType attribute must return the DOMString "navigation".
      MD

      argument :start_time, Float, <<~MD, required: true
        The startTime attribute must return a DOMHighResTimeStamp with a time value of 0.
      MD

      argument :duration, Float, <<~MD, required: true
        The duration attribute must return a DOMHighResTimeStamp equal to the difference between loadEventEnd and startTime, respectively.
      MD

      argument :initiator_type, String, <<~MD, required: false
        The initiatorType attribute must return the DOMString "navigation".
      MD

      argument :next_hop_protocol, String, <<~MD, required: false
        Returns the network protocol used to fetch the resource, as identified by the ALPN Protocol ID.
      MD

      argument :worker_start, Float, <<~MD, required: false
        The workerStart attribute must return the time immediately before the user agent ran the worker.
      MD

      argument :redirect_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-redirectstart
      MD

      argument :redirect_end, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-redirectend
      MD

      argument :fetch_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-fetchstart
      MD

      argument :domain_lookup_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-domainlookupstart
      MD

      argument :domain_lookup_end, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-domainlookupend
      MD

      argument :connect_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-connectstart
      MD

      argument :connect_end, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-connectend
      MD

      argument :secure_connection_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-secureconnectionstart
      MD

      argument :request_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-requeststart
      MD

      argument :response_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-responsestart
      MD

      argument :response_end, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-responseend
      MD

      argument :transfer_size, Integer, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-transfersize
      MD

      argument :encoded_body_size, Integer, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-encodedbodysize
      MD

      argument :decoded_body_size, Integer, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-decodedbodysize
      MD

      argument :unload_event_start, Float, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :unload_event_end, Float, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :dom_interactive, Float, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :dom_content_loaded_event_start, Float, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :dom_content_loaded_event_end, Float, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :dom_complete, Float, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :load_event_start, Float, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :load_event_end, Float, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :type, String, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD

      argument :redirect_count, Integer, <<~MD, required: false
        https://w3c.github.io/navigation-timing/#sec-PerformanceNavigationTiming
      MD
    end
  end
end
