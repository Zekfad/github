# frozen_string_literal: true

module Platform
  module Inputs
    class SponsorsListingFilters < Platform::Inputs::Base
      description "Filtering options for Sponsors listing connections."
      visibility :internal

      argument :states, [Enums::SponsorsListingState], description: "The states to filter Sponsors listings by.", required: false
      argument :type, [Enums::SponsorableType], description: "The sponsorable type to filter listings by.", required: false
    end
  end
end
