# frozen_string_literal: true

module Platform
  module Inputs
    class SponsorsListingOrder < Platform::Inputs::Base
      description "Ordering options for Sponsors listing connections."
      visibility :internal

      argument :field, Enums::SponsorsListingOrderField, "The field to order Sponsors listings by.", required: true
      argument :direction, Enums::OrderDirection, "The ordering direction.", required: true
    end
  end
end
