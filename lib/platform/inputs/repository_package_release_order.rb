# frozen_string_literal: true

module Platform
  module Inputs
    class RepositoryPackageReleaseOrder  < Platform::Inputs::Base
      visibility :internal

      description "Ways in which lists of repository package releases can be ordered upon return."

      argument :field, Enums::RepositoryPackageReleaseOrderField, "The field in which to order repository package releases by.", required: true
      argument :direction, Enums::OrderDirection, "The direction in which to order repository package releases by the specified field.", required: true
    end
  end
end
