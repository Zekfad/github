# frozen_string_literal: true

module Platform
  module Inputs
    class SponsorshipNewsletterOrder < Platform::Inputs::Base
      description "Ordering options for sponsorship update connections."
      visibility :internal

      argument :field, Enums::SponsorshipNewsletterOrderField,
        "The field to order sponsorship updates by.", required: true
      argument :direction, Enums::OrderDirection, "The ordering direction.", required: true
    end
  end
end
