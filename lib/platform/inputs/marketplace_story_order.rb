# frozen_string_literal: true

module Platform
  module Inputs
    class MarketplaceStoryOrder < Platform::Inputs::Base
      areas_of_responsibility :marketplace
      description "Ordering options for Marketplace story connections."
      visibility :internal

      argument :field, Enums::MarketplaceStoryOrderField, "The field to order stories by.", required: true
      argument :direction, Enums::OrderDirection, "The ordering direction.", required: true
    end
  end
end
