# frozen_string_literal: true

module Platform
  module Inputs
    class AbuseReportOrder < Platform::Inputs::Base
      areas_of_responsibility :community_and_safety

      description "Ordering options for abuse report connections."
      visibility :internal

      argument :field, Enums::AbuseReportOrderField, "The field to order abuse reports by.",
        required: true
      argument :direction, Enums::OrderDirection, "The ordering direction.", required: true
    end
  end
end
