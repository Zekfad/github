# frozen_string_literal: true

module Platform
  module Inputs
    class SecurityIncidentResponse < Platform::Inputs::Base
      description "Specifies a user and optional extra data for incident remediations. See argument list for possible remediations."
      visibility :internal

      argument :user_id, ID, "Global Relay ID of a User account.", required: false
      argument :users, [Inputs::SecurityIncidentUser], "Database ID and notification template data for Users.", required: false
      argument :notify, Inputs::SecurityIncidentNotification, "Fields for sending a notification. Excluding this means no notifications are sent.", required: false
      argument :staffnote, String, "Optional staffnote text", required: false

      # Available Remediations
      argument :remove_repository_recommendations, [Integer], "A list of Repository database IDs to opt out of being recommended to other users.", required: false
      argument :remove_repository_stars, [Integer], "A list of Repository database IDs to remove this user's stars from.", required: false
      argument :reset_password, Boolean, "Reset this user's password?", required: false, default_value: false
      argument :revoke_oauth_authorizations, [Integer], "A list of OAuth Application database IDs to revoke authorizations for", required: false
      argument :revoke_oauth_tokens, [Integer], "A list of OAuth Access/Personal Access Token database IDs to revoke", required: false
    end
  end
end
