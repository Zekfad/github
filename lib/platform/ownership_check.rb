# frozen_string_literal: true
require "codeowners"

module Platform
  class OwnershipCheck
    # Load info from `CODEOWNERS` using the `github/codeowners` gem
    # @return [Codeowners::File]
    def self.load_codeowners
      # Explicitly pass in `pwd` -- otherwise it raises
      # `Errno::ENOENT: No such file or directory - getcwd` 🤷‍♂️
      codeowners_path = File.expand_path("./CODEOWNERS", Dir.pwd)
      codeowners_text = File.read(codeowners_path)
      Codeowners::File.new(codeowners_text)
    end

    def initialize
      @codeowners_file = self.class.load_codeowners
      # We'll use this to identify teams
      @all_reviewers = @codeowners_file.rules.map { |rule| rule.owners.map(&:identifier) }.flatten.uniq

      # Find out which files are owned by ecosystem-api (or ecosystem-api-reviewers)
      team_name_substring = "ecosystem-api"
      team_rules = @codeowners_file.rules.select { |rule| rule.owners.any? { |o| o.identifier.include?(team_name_substring) } }
      # Figure out which directories contain ecosystem-api-owned files.
      # (This check allows us to skip checking directories like `app/views` and `vendor/`)
      team_patterns = team_rules.map { |r| r.pattern.pattern }
      team_folders = team_patterns.map { |pat| pat.split("/").first }.uniq.sort
      all_file_paths = Dir.glob("{#{team_folders.join(",")}}/**/*.*")
      # Some of the "folders" were actually files, eg `.graphql-doctor.yaml`
      all_file_paths += team_folders.select { |f| f.include?(".") }
      # Filter it down to files owned exclusively by ecosystem-api,
      @ecosystem_api_file_paths = all_file_paths.select { |fp|
        owning_teams = @codeowners_file.for(fp).keys.map(&:identifier)
        owning_teams.size == 1 && owning_teams.first.include?(team_name_substring)
      }
    end

    attr_reader :codeowners_file

    def suggestions
      @suggestions ||= @ecosystem_api_file_paths
        .map { |fp| find_alternative_owners(fp) }
        .map { |suggestions| merge_suggestions_by_owner(suggestions) }
        .compact
    end

    # Modify `CODEOWNERS` according to this class's suggestions.
    # @return [void]
    def autocorrect
      Autocorrect.call(self)
      # The file text has changed, so reload it
      @codeowners_file = self.class.load_codeowners
      nil
    end

    def suggestion_report
      Report.generate(self, @all_reviewers, @ecosystem_api_file_paths)
    end

    private

    # @return [<Hash>]
    def find_alternative_owners(file_path)
      suggestions = find_alternative_owners_by_codeowners(file_path) +
        find_alternative_owners_by_aor(file_path) +
        find_alternative_owners_by_route_owners(file_path)

      # Get desperate, start making things up
      if suggestions.empty?
        suggestions = find_alternative_owners_by_resource_name(file_path)
      end

      # Remove teams that are normalized out
      suggestions.each { |s|
        s[:owners] = s[:owners].map { |t| normalize_team(t) }.compact

        if s[:owners].any? { |t| !t.end_with?("-reviewers") }
          raise Platform::Errors::Internal, "CODEOWNERS teams must end with reviewers, but some owners here don't:  #{s.inspect}"
        end
      }
      suggestions.select { |s| s[:owners].any? }
    end

    def merge_suggestions_by_owner(suggestions)
      if suggestions.empty?
        nil
      else
        merged_suggestion = {
          path: suggestions[0][:path],
          # Team names pointing to comments
          owners: Hash.new { |h, k| h[k] = [] }
        }

        suggestions.each do |suggestion|
          merged_teams = suggestion[:owners].sort
          merged_teams.each do |team|
            merged_suggestion[:owners][team] << suggestion[:comment]
          end
        end

        if merged_suggestion[:owners].empty?
          raise Platform::Errors::Internal, "Invariant: Suggestions should always have owners (#{suggestions})"
        end

        merged_suggestion
      end
    end

    def normalize_team(team)
      if @all_reviewers.include?("#{team}-reviewers")
        "#{team}-reviewers"
      elsif CODEOWNERS_REPLACEMENT_TEAM.key?(team)
        CODEOWNERS_REPLACEMENT_TEAM[team]
      else
        team
      end
    end

    # These teams were inferred from code, but weren't already in CODEOWNERS.
    # This is a manual mapping to existing code owners.
    #
    # Some teams map to `nil` -- if the team doesn't end in `-reviewers`,
    # our build doesn't allow it, and I couldn't find a better team.
    CODEOWNERS_REPLACEMENT_TEAM = {
      "@github/profiles-and-dashboards" => "@github/communities-sparkle-reviewers",
      "@github/profile" => "@github/communities-sparkle-reviewers",
      "@github/profiles" => "@github/communities-sparkle-reviewers",
      "@github/ecosystem-primitives" => "@github/ce-extensibility-reviewers",
      "@github/marketplace-eng" => "@github/ce-extensibility-reviewers",
      "@github/ee-search" => "@github/pe-search-reviewers",
      "@github/code-search" => "@github/pe-search-reviewers",
      "@github/identity" => "@github/iam-reviewers",
      "@github/pe-issues-projects" => "@github/issues-reviewers",
      "@github/data-liberation" => "@github/import-export-code-reviewers",
      "@github/mobile" => "@github/mobile-api-reviewers",
      "@github/pe-mobile" => "@github/mobile-api-reviewers",
      "@github/stafftools" => nil, # was "@github/enterprise-tools"
      "@github/ee-osn" => "@github/dsp-dependency-graph-reviewers",
      "@github/experience-engineering-work" => "@github/dashboard-reviewers",
      "@github/ee-systems" => "@github/pe-repos-reviewers",
      "@github/render" => "@github/pe-repos-reviewers",
      "@github/lfs" => "@github/git-protocols-reviewers",
      "@github/releases" => "@github/pe-repos-reviewers",
      "@github/avatars" => "@github/communities-sparkle-reviewerss",
      "@github/data-engineering" => nil,
      "@github/containerscrew" => nil,
    }

    def find_alternative_owners_by_codeowners(fp)
      # It's a bit silly to check codeowners _again_, but sometimes
      # this method is called for files that weren't in the original list
      # (eg, `app/api/licenses.rb` checks the codeowner of `app/models/license.rb`)
      codeowners_rules = @codeowners_file.for(fp)
      codeowners_for_file = codeowners_rules.keys.map(&:identifier)
      # This team is already the owner of these files
      codeowners_for_file.delete("@github/ecosystem-api-reviewers")
      if codeowners_for_file.any?
        [
          {
            owners: codeowners_for_file,
            comment: "Codeowner for #{fp}",
            path: fp
          }
        ]
      else
        []
      end
    end

    TEAM_FOR_AOR = {
      "actions" => "@github/c2c-actions-experience-reviewers",
      "analytics" => nil,
      "api" => nil, # don't re-recommend this team
      "avatars" => "@github/communities-sparkle-reviewers",
      "ce_extensibility" => "@github/ce-extensibility-reviewers",
      "checks" => "@github/ce-extensibility-reviewers",
      "code_collab" => "@github/pe-pull-requests-reviewers",
      "codespaces" => "@github/codespaces-reviewers",
      "commit_statuses" => "@github/ce-extensibility-reviewers",
      "community_and_safety" => "@github/communities-heart-reviewers",
      "data_infrastructure" => "@github/data-infrastructure-reviewers",
      "downloads" => "@github/pe-repos-reviewers",
      "ecosystem_api" => nil,
      "ecosystem_apps" => "@github/ecosystem-apps-reviewers",
      "ecosystem_integrations" => "@github/ecosystem-apps-reviewers",
      "ecosystem_primitives" => "@github/ce-extensibility-reviewers",
      "email" => nil,
      "enterprise_only" => "@github/admin-experience-reviewers",
      "external_auth" => "@github/iam-experience-reviewers",
      "gist" => "@github/pe-repos-reviewers",
      "git" => "@github/git-storage-reviewers",
      "gitcoin" => "@github/gitcoin-reviewers",
      "github_flavored_markdown" => "@github/pe-repos-reviewers",
      "high_availability" => nil,
      "iam" => "@github/iam-reviewers",
      "iam_experience" => "@github/iam-experience-reviewers",
      "import_api" => "@github/import-export-code-reviewers",
      "issues" => "@github/issues-reviewers",
      "languages" => "@github/pe-repos-reviewers",
      "lfs" => "@github/git-protocols-reviewers",
      "live_updates" => "@github/web-systems-reviewers",
      "marketplace" => "@github/ce-extensibility-reviewers",
      "migration" => "@github/import-export-code-reviewers",
      "mobile" => "@github/mobile-api-reviewers",
      "news_feeds" => "@github/dashboard-reviewers",
      "notifications" => "@github/notifications-reviewers",
      "orgs" => "@github/teams-and-orgs-reviewers",
      "pages" => "@github/pages-reviewers",
      "pe_repos" => "@github/pe-repos-reviewers",
      "porter" => "@github/import-export-code-reviewers",
      "projects" => "@github/projects-reviewers",
      "pull_requests" => "@github/pe-pull-requests-reviewers",
      "raw" => "@github/pe-repos-reviewers",
      "releases" => "@github/pe-repos-reviewers",
      "rendering" => "@github/pe-repos-reviewers",
      "repositories" => "@github/pe-repos-reviewers",
      "search" => "@github/pe-search-reviewers",
      "security_advisories" => "@github/dsp-security-workflows-reviewers",
      "stafftools" => nil, # was "@github/enterprise-tools", couldn't find a better owner
      "teams" => "@github/teams-and-orgs-reviewers",
      "traffic_graphs" => "@github/pe-repos-reviewers",
      "unassigned" => nil,
      "admin_experience" => "@github/admin-experience-reviewers",
      "user_auth" => "@github/prodsec-reviewers",
      "user_growth" => "@github/site-design-reviewers",
      "user_profile" => "@github/communities-sparkle-reviewers",
      "vulnerability_alerts" => "@github/dsp-security-workflows-reviewers",
      "webhook" => "@github/ecosystem-events-reviewers",
      "workspaces" => "@github/workspaces-reviewers",
    }

    # Scan the file for `areas_of_responsibility` calls, and use them to identify a team.
    def find_alternative_owners_by_aor(fp)
      # this file has a test class that calls areas_of_responsability, which has
      # nothing to do with the actual AOR.  Its actually the API team's code,
      # but since we are ignoring :api in TEAM_FOR_AOR it doesn't get counted in
      # this logic.  Just excluded manually...
      return [] if fp == "test/integration/api/app_test.rb"

      content = File.read(fp)
      find_areas_of_responsibility = /areas_of_responsibility[\( ](?<aors>(?::[a-z_\-0-9]+(?:, )?)+)/
      possible_owners_match = find_areas_of_responsibility.match(content)
      if possible_owners_match && (aors = possible_owners_match[:aors])
        new_owners = aors
          .split(",")
          .map { |t| t.sub(":", "").strip }
          .map { |t|
            TEAM_FOR_AOR.fetch(t) {
              warn("No team mapping for AOR #{t.inspect}; add one to TEAM_FOR_AOR."); nil
            }
          }
          .compact
          .sort

        if new_owners.any?
          [
            {
              owners: new_owners,
              comment: "Areas of responsibility in #{fp}",
              path: fp,
            }
          ]
        else
          []
        end
      else
        []
      end
    end

    # Scan the file for `@route_owner` declarations, and use them to find a team
    def find_alternative_owners_by_route_owners(fp)
      contents = File.read(fp)
      new_owners = contents.scan(/@route_owner = "([@a-z\/_\-]+)"/)
        .map(&:first)
        .uniq

      # no need to re-recommend this team
      new_owners.delete("@github/ecosystem-api")

      if new_owners.any?
        [
          {
            owners: new_owners,
            comment: "`@route_owner` in #{fp}",
            path: fp,
          }
        ]
      else
        []
      end
    end

    # Lookup a recommendation for the corresponding API endpoint file
    def find_alternative_owners_by_resource_name(fp)
      if fp.include?("app/api/serializer")
        # eg `app/api/serializer/issues_dependency.rb` => `issue`
        resource_name = fp.sub("app/api/serializer/", "").sub("_dependency.rb", "")
        resource_name = resource_name.singularize
      elsif fp.include?("app/api/schemas/v3")
        # eg `app/api/schemas/v3/schemas/blob.json` => `blob`
        resource_name = fp
          .sub("app/api/schemas/v3/schemas/", "")
          .sub(".json", "")
          .sub("-simple", "")
          .sub("legacy-", "")
        # eg `commit-status` => `commit_status`
        resource_name = resource_name.underscore
      end

      if resource_name
        corresponding_api_name = "app/api/#{resource_name.pluralize}.rb"
        api_owners = find_alternative_owners_in_corresponding_file(fp, corresponding_api_name)
        if api_owners.any?
          api_owners
        else
          corresponding_model_name = "app/models/#{resource_name}.rb"
          find_alternative_owners_in_corresponding_file(fp, corresponding_model_name)
        end
      else
        []
      end
    end

    def find_alternative_owners_in_corresponding_file(original_file_name, corresponding_file_name)
      if File.exist?(corresponding_file_name)
        suggestions = find_alternative_owners(corresponding_file_name)
        suggestions.each { |o| o[:path] = original_file_name }
        suggestions
      else
        []
      end
    end
  end
end
