# frozen_string_literal: true
module Platform
  class GlobalScope
    @@mutation    = false
    @@origin      = nil
    @@queries     = []
    @@tracers     = []
    @@accessed_objects = {}

    def self.reset!
      @@mutation = false
      @@origin   = nil
      @@queries  = []
      @@tracers  = []
      @@accessed_objects = {}
      Platform::LoaderTracker.reset!
    end

    def self.query_count
      @@queries.count
    end

    def self.query_time
      return 0.0 if @@queries.blank?
      @@queries.reduce(0.0) { |acc, hash| acc + hash[:stats][:real] } / 1000
    end

    def self.mutation
      @@mutation
    end

    def self.mutation=(boolean)
      @@mutation = boolean
    end

    def self.mutation?
      !!@@mutation
    end

    def self.tracers
      @@tracers
    end

    def self.queries
      @@queries
    end

    def self.origin
      @@origin
    end

    def self.origin=(origin)
      @@origin = origin
    end

    def self.origin_api?
      Platform::ORIGIN_API == @@origin
    end

    def self.origin_rest_api?
      Platform::ORIGIN_REST_API == @@origin
    end

    def self.origin_internal?
      Platform::ORIGIN_INTERNAL == @@origin
    end

    def self.origin_manual_execution?
      Platform::ORIGIN_MANUAL_EXECUTION == @@origin
    end

    def self.accessed_objects
      @@accessed_objects
    end
  end
end
