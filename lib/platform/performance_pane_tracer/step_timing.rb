# frozen_string_literal: true

module Platform
  class PerformancePaneTracer
    # A StepTiming wraps some part of execution with stats.
    #
    # For field timings, it has _two_ kinds of events:
    # - Calling the resolve function of a field; OR
    # - Calling `#sync` on a returned promise.
    #
    # When the timing reflects a `#sync` call, `#lazy?` returns true.
    #
    # Corresponding timings may be matched by `#path`: matching timings
    # have the same path value, but one has `lazy: false`, the other has `lazy: true`.
    class StepTiming
      attr_reader :start_offset, :duration, :name, :path
      attr_reader :gitrpc_calls, :mysql_calls,
        :memcached_calls_count, :memcached_calls_time,
        :redis_calls, :elastomer_calls, :mysql_cache_hits,
        :cpu_time, :allocated_objects, :allocated_objects_count, :lines

      def initialize(start_offset:, duration:, lazy:, path:, name:, stats:)
        @start_offset = start_offset
        @duration = duration
        @lazy = lazy
        @path = path
        @name = name
        @gitrpc_calls, @mysql_calls, @memcached_calls_count, @memcached_calls_time, @redis_calls, @elastomer_calls, @mysql_cache_hits, @cpu_time, @allocated_objects, @allocated_objects_count, @lines = stats
      end

      # @return [Boolean] True when this timing reflects `Promise#sync` time
      def lazy?
        @lazy
      end
    end
  end
end
