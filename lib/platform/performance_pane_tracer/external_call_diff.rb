# frozen_string_literal: true

module Platform
  class PerformancePaneTracer
    # This class keeps a running total of external calls that have been recorded by
    # the application.
    #
    # When it's initialized, it logs the initial state of a bunch of accumulators.
    # Then, when you call `#diff`, it:
    #
    # - Compares the current values of those accumulators against its own last-observed values
    # - Extracts any differences
    # - Updates its own accumulators
    # - Returns the difference since the last call
    #
    # This way, `#diff` can tell you what calls happened since the last call.
    class ExternalCallDiff
      # Used for empty diff responses
      NO_CALLS = [].freeze

      # Read a bunch of global values to get the initial state of the system
      def initialize
        # Let's keep track of the starting values of these,
        # then we can keep an eye on how they increment over time
        @gitrpc_count = GitRPCLogSubscriber.rpc_calls.size
        @mysql_count = GitHub::MysqlInstrumenter.queries.size
        # These don't get to `MysqlInstrumenter`, so we have to track them separately
        @mysql_cache_hits_count = QueryCacheLogSubscriber.cache_hits.size
        # Memcached::Rails doesn't give is op-level stats, but we can get this:
        # (TBH maybe we can use GitHub.tracer? But I don't think it's worth it yet)
        @memcached_count = Memcached::Rails.query_count
        @memcached_time = Memcached::Rails.query_time
        # Redis might have some queries before us
        @redis_count = Redis::Client.queries.size
        # If elastomer is tracking, both `count` and `to_a` will have the same contents
        @elastomer_count = Elastomer::QueryStats.instance.count
      end

      # Diff the state of this object against global accumulators, then:
      # - Return the diff since that's what happened since our last check
      # - Update our accumulators to reflect those changes
      # @return [Array<Object>] A bunch of random stats
      def diff
        # See if more rpc calls have been made; if so, read them and return them in the diff
        gitrpc_count_diff = GitRPCLogSubscriber.rpc_calls.size - @gitrpc_count
        if gitrpc_count_diff > 0
          gitrpc_calls = GitRPCLogSubscriber.rpc_calls[@gitrpc_count, gitrpc_count_diff]
          @gitrpc_count = GitRPCLogSubscriber.rpc_calls.size
        else
          gitrpc_calls = NO_CALLS
        end

        # See if more SQL calls have been made; diff them if so
        new_mysql_count = GitHub::MysqlInstrumenter.queries.size
        if new_mysql_count > @mysql_count
          mysql_count_diff = new_mysql_count - @mysql_count
          mysql_calls = GitHub::MysqlInstrumenter.queries[@mysql_count, mysql_count_diff]
          @mysql_count = new_mysql_count
        else
          mysql_calls = NO_CALLS
        end

        new_mysql_cache_hits_count = QueryCacheLogSubscriber.cache_hits.size
        if new_mysql_cache_hits_count > @mysql_cache_hits_count
          cache_hits_diff = new_mysql_cache_hits_count - @mysql_cache_hits_count
          new_mysql_cache_hits = QueryCacheLogSubscriber.cache_hits[@mysql_cache_hits_count, cache_hits_diff]
          @mysql_cache_hits_count = new_mysql_cache_hits_count
        else
          new_mysql_cache_hits = NO_CALLS
        end

        # See if any Memcached activity
        if Memcached::Rails.query_count > @memcached_count
          new_memcached_count = Memcached::Rails.query_count
          new_memcached_time = Memcached::Rails.query_time
          memcached_count_diff = new_memcached_count - @memcached_count
          memcached_time_diff = new_memcached_time - @memcached_time
          @memcached_count = new_memcached_count
          @memcached_time = new_memcached_time
        else
          memcached_count_diff = 0
          memcached_time_diff = 0
        end

        new_redis_count = Redis::Client.queries.size
        if new_redis_count > @redis_count
          redis_count_diff = new_redis_count - @redis_count
          redis_calls = Redis::Client.queries[@redis_count, redis_count_diff]
          @redis_count = new_redis_count
        else
          redis_calls = NO_CALLS
        end

        new_elastomer_count = Elastomer::QueryStats.instance.count
        if new_elastomer_count > @elastomer_count
          elastomer_count_diff = new_elastomer_count - @elastomer_count
          elastomer_calls = Elastomer::QueryStats.instance.to_a[@elastomer_count, elastomer_count_diff]
          @elastomer_count = new_elastomer_count
        else
          elastomer_calls = NO_CALLS
        end

        return [gitrpc_calls, mysql_calls, memcached_count_diff, memcached_time_diff, redis_calls, elastomer_calls, new_mysql_cache_hits]
      end
    end
  end
end
