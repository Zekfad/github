# frozen_string_literal: true

module Platform
  module Scalars
    class Base < GraphQL::Schema::Scalar
      extend Platform::Objects::Base::Visibility
      extend Platform::Objects::Base::AreasOfResponsibility
      extend Platform::Objects::Base::Previewable
    end
  end
end
