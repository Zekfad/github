# frozen_string_literal: true
# rubocop:disable GitHub/UsePlatformErrors

module Platform
  module Authorization
    class SAML
      include Scientist

      # Create a Platform::Authorization::SAML object.
      #
      # user - User representing the actor for the request.
      # session - UserSession representing the actor session for the request.
      # remote_token_auth - Boolean representing whether remote token auth is
      #   being used for this request. Defaults to false.
      #   See GitHub::Authentication::SignedAuthToken.
      def initialize(user: nil, session: nil, remote_token_auth: false)
        if user && session
          raise ArgumentError, "You must supply user or session, not both"
        end

        @session = session
        @user = user || session&.user
        @remote_token_auth = remote_token_auth

        if (oauth_access = @user&.oauth_access)
          @oauth_access = oauth_access
        end
      end

      def authorized_organization_ids
        case
        when @session
          authorized_org_ids_from_session
        when @oauth_access
          authorized_org_ids_from_oauth
        else
          []
        end
      end

      def protected_organization_ids
        return [] if @user.blank?

        if (oauth_access = @user.oauth_access)
          return [] unless oauth_access.saml_enforceable?
        end

        return [] if @remote_token_auth
        return [] if @user.is_a?(Bot)

        return saml_org_ids if saml_org_ids.empty?

        saml_org_ids - authorized_organization_ids
      end

      def protected_targets
        protected_organizations(include_businesses: true).map do |org|
          if org.saml_enabled_on_business?
             org.business
          else
            org
          end
        end.uniq
      end

      def protected_organizations(include_businesses: false)
        query = Organization.where(id: protected_organization_ids)

        if include_businesses
          query = query.includes(:business)
        end

        query
      end

      def protected_business_ids
        protected_organizations(include_businesses: true).map do |org|
          org.business.id if org.saml_enabled_on_business?
        end.compact.uniq
      end

      def authorized_organizations(include_businesses: false)
        query = Organization.where(id: authorized_organization_ids)

        if include_businesses
          query = query.includes(:business)
        end

        query
      end

      # Return the "SAML orgs" for the user.
      #
      # "SAML orgs" are defined as those where either:
      # - The org has a SAML provider configured and enforced.
      # - The org has a SAML provider configured and the user already has
      #   an external identity.
      # - The org has an owning business that has a SAML provider configured
      #   and the user has an external identity.
      #
      # Returns an ActiveRecord::Relation containing the "SAML orgs".
      def saml_organizations
        Organization.where id: saml_org_ids
      end

      private

      # From a user_session, get all active external_identity_sessions that are
      # associated, either via a SAML provider at the org level or at the owning
      # business level.
      #
      # These external_identity_sessions can map back up to the organizations
      # for which they are valid. From there, we have a list of orgs that the
      # user's session is allowed to access.
      def authorized_org_ids_from_session
        @authorized_org_ids_from_session ||=
          begin
            organizations_scope = @session.user.organizations
            authzd_saml_org_ids = organizations_scope.
                                    joins({ saml_provider: { external_identities: :sessions }}).
                                    where("external_identity_sessions.user_session_id = ?", @session.id).
                                    where("external_identity_sessions.expires_at > ?", Time.now)

            authzd_business_saml_org_ids = organizations_scope.
                                             joins({ business: { saml_provider: { external_identities: :sessions } } }).
                                             where("external_identity_sessions.user_session_id = ?", @session.id).
                                             where("external_identity_sessions.expires_at > ?", Time.now)

            organizations_scope.
              where(id: authzd_saml_org_ids.select(:id)).or(
                organizations_scope.where(
                  id: authzd_business_saml_org_ids.select(:id)
                )
              ).pluck(:id)
          end
      end

      # From an OAuth token, get all organizations for which the token has been
      # explicitly whitelisted.
      def authorized_org_ids_from_oauth
        Organization::CredentialAuthorization.active.
          by_credential(credential: @oauth_access).
          where(organization_id: saml_org_ids).
          pluck(:organization_id)
      end

      # Find all of the user's organizations using any of the these criteria:
      #
      # - Where the org has a SAML provider configured and enforced.
      # - Where the org has a SAML provider configured and the user already has
      #   an external identity.
      # - Where the org has an owning business that has a SAML provider configured
      #   and the user has an external identity.
      def saml_org_ids
        return @saml_org_ids if defined?(@saml_org_ids)

        # Anonymous access falls back to existing access control checks
        return Organization.none if @user.nil?

        # GHPI enables a SAML provider on the global business, but is exempt from this enforcement
        return Organization.none if GitHub.private_instance_user_provisioning?

        @saml_org_ids ||= begin
          org_ids = @user.organization_ids

          # Select organizations with their configured SAML providers
          # and join the user's external identities when present
          org_join_clause = <<-SQL.squish
            INNER JOIN organization_saml_providers
              ON organization_saml_providers.organization_id = users.id
            LEFT JOIN external_identities
              ON external_identities.provider_id = organization_saml_providers.id
                AND external_identities.provider_type = "Organization::SamlProvider"
          SQL

          # Find only organizations the user is a direct member of
          # and only those organizations that enforce SSO or any the
          # member has an external identity linked with
          org_conditions = <<-SQL.squish
            users.id IN (:organization_ids)
            AND (
              organization_saml_providers.enforced = 1
              OR external_identities.user_id = :user_id
            )
          SQL

          direct_org_ids =
            Organization.joins(org_join_clause).
              where(org_conditions, organization_ids: org_ids, user_id: @user.id)

          # Select organizations where the owning business has a configured SAML
          # provider and join the user's external identities when present
          business_join_clause = <<-SQL.squish
            INNER JOIN business_organization_memberships
              ON business_organization_memberships.organization_id = users.id
            INNER JOIN business_saml_providers
              ON business_saml_providers.business_id = business_organization_memberships.business_id
            LEFT JOIN external_identities
              ON external_identities.provider_id = business_saml_providers.id
                AND external_identities.provider_type = "Business::SamlProvider"
          SQL

          # Find only organizations the user is a direct member of
          # and only those organizations belonging to a business with a SAML
          # provider where the member has an external identity linked.
          business_conditions = <<-SQL.squish
            users.id IN (:organization_ids)
            AND external_identities.user_id = :user_id
          SQL

          business_org_ids =
            Organization.joins(business_join_clause).
              where(business_conditions, organization_ids: org_ids, user_id: @user.id)

          organizations_scope = Organization.where(id: org_ids)
          organizations_scope.
            where(id: direct_org_ids.select(:id)).or(
              organizations_scope.where(
                id: business_org_ids.select(:id)
              )
            ).pluck(:id)
        end
      end
    end
  end
end
