# frozen_string_literal: true

module Platform
  module Authorization
    class IntegrationAuthorizer
      #
      # Finder dependencies
      #
      # These methdos return data, and may memoize results, but shouldn't alter
      # state in a way to change AuthZ results.
      #

      delegate :current_user, :current_integration, :current_integration_installation,
        :current_repo_loaded?, :current_repo, to: :@app

      # These are private methods so we can't access them from @app, their values
      # are set on initialize.
      attr_reader :graphql_request, :repo_nwo_from_path, :current_resource_owner
      alias graphql_request? graphql_request

      #
      # Mutation dependencies
      #
      # These methods can alter the AuthZ result.
      #

      delegate :access_grant, :set_forbidden_message, to: :@app

      def initialize(app:, graphql_request:, repo_nwo_from_path:, current_resource_owner:)
        @app = app
        @graphql_request = graphql_request
        @repo_nwo_from_path = repo_nwo_from_path
        @current_resource_owner = current_resource_owner
      end

      def integration_bot_request_allowed?(verb, options = {})
        # Dashboards are user-specific, and are never available to installations.
        return false if options[:resource].is_a?(Platform::DashboardResource)

        # on endpoint that allows integrations
        if options[:allow_integrations]
          integration_authorized = access_grant(verb, options).access_allowed?

          # integration installed on repo, but installation does not have access
          if !integration_authorized && current_integration_installation_can_see_repo?
            set_forbidden_message("Resource not accessible by integration")
            false
          else
            integration_authorized
          end
        else
          # on endpoint that doesn't allow integrations
          if options[:forbid] || current_integration_installation_can_see_repo?
            set_forbidden_message("Resource not accessible by integration")
          end
          false
        end
      end

      def integration_user_request_allowed?(verb, options = {})
        options                       = options.dup
        user_allowed_via_integration  = options.fetch(:allow_user_via_integration, false)

        # From https://github.com/github/github/pull/84396
        # This flag is just to enforce whether an installation is needed to get
        # a particular resource.
        #
        # See https://github.com/github/github/pull/84396#issuecomment-370054554
        # for a full explanation.
        options[:approved_integration_required] = true if options[:approved_integration_required].nil?

        resource = options[:resource] || options[:repo]

        if resource.nil?
          if options.key?(:resource) || options.key?(:repo)
            return false
          else
            message = ":resource or :repo is required for integration user requests"
            error   = Platform::Errors::ResourceMissing.new(message)

            unless Rails.env.production?
              raise Platform::Errors::ResourceMissing, message
            else
              Failbot.report(error)
              return false
            end
          end
        end

        # A GistComment is not backed by abilities so any of the checks we
        # would normally do won't work. So for the time being we're going to
        # just return false.
        return false if resource.is_a?(GistComment)

        if resource.is_a?(Platform::DashboardResource)
          # Dashboards will probably eventually be enabled for users.
          # If they are, then the endpoint needs to do filtering, but
          # we don't have to check whether the "resource" is readable by
          # the user and/or the integration.
          # By definition, the dashboard is readable by the user, and the
          # filtering will have to take the integration into account.
          set_forbidden_message("Resource not accessible by integration")
          return user_allowed_via_integration
        end

        readable_by_user = resource.readable_by?(current_user)

        readable_by_integration = if current_integration_installed_globally?
          current_global_integration_can_see_resource?(resource)
        else
          current_integration_can_see_resource?(resource)
        end

        # If the forbid option is set, then we always want to return a 403 rather than a 404.
        if options[:forbid]
          set_forbidden_message("Resource not accessible by integration")
        end

        # Bail out with a 404 so that we don't leak existence to the
        # user via the integration.
        return false if !readable_by_user

        # Bail out with a 404 so that we don't leak existence to the
        # integration.
        return false if !readable_by_integration

        integration_access_allowed = if current_integration_installed_globally?
          global_integration_access_allowed?(verb, options)
        else
          integration_access_allowed?(verb, options)
        end

        # Beyond this point we're not leaking existence.
        if !user_allowed_via_integration || !integration_access_allowed || !access_grant(verb, options).access_allowed?
          set_forbidden_message("Resource not accessible by integration")
          return false
        end

        true
      end

      # Private: Determines whether the current_integration_installation can know if the current_repo exists
      #
      # NOTE: This _heavily_ relies on the fact that we default to true.
      # The logic needs to be cleaned up to not rely that.
      #
      # Please ping @tarebyte if you want to refactor this logic.
      #
      # Returns a Boolean.
      def current_integration_installation_can_see_repo?
        return true unless (current_repo_loaded? || repo_nwo_from_path.present?)
        return true unless current_repo.try(:private?)
        return true unless current_integration_installation.present?

        current_integration_installation.repository_ids.include?(current_repo.id)
      end

      def current_integration_can_see_resource?(resource)
        resource = authz_resource_parent(resource)

        Failbot.push(current_integration: current_integration, current_integration_installation: current_integration_installation)

        # Special edgecases that don't fit the normal AuthZ patterns.
        case resource
        when DiscussionPost, DiscussionPostReply
          return current_integration_can_see_resource?(current_resource_owner)
        when GpgKey, Platform::PublicResource, Platform::GraphqlUserResource, PublicKey
          return true
        when Integration
          return true if resource.public?
          return resource == current_integration
        when Project, Team, Codespace
          return current_integration_can_see_resource?(resource.owner)
        when Repository
          return true if resource.public?
        when User
          return resource == current_user if resource.user? && !graphql_request?
        end

        installation_found = case resource
        when Business, Organization, User
          current_integration.installations.with_target(resource).exists?
        when Repository
          current_integration.installations.with_repository(resource).exists?
        else
          false
        end

        return false unless installation_found
        return installation_found unless current_integration_installation

        # To try and maintain the consistency of 404's vs 403's
        # we want to compare the installation found vs the installation
        # we have loaded.
        #
        # We use the parent installation because the only
        # installations that will be loaded at this point
        # are ScopedIntegrationInstallations.
        integration_installation_for(resource) == current_parent_integration_installation
      end

      def current_global_integration_can_see_resource?(resource)
        resource = authz_resource_parent(resource)

        # Lean on the SiteScopedIntegrationInstallation record if we have loaded one.
        if current_integration_installation
          return current_global_integration_with_installation_can_see_resource?(resource)
        end

        Failbot.push(current_integration: current_integration)

        case resource
        when DiscussionPost, DiscussionPostReply
          current_global_integration_can_see_resource?(current_resource_owner)
        when GpgKey, Platform::PublicResource, Platform::GraphqlUserResource, PublicKey
          return true
        when Integration
          return true if resource.public?
          resource == current_integration
        when Business, Organization
          true
        when Project, Team, Codespace
          current_global_integration_can_see_resource?(resource.owner)
        when Repository
          return true if resource.public?
          current_integration.latest_version.any_permissions_of_type?(Repository)
        when User
          return true if graphql_request?
          resource == current_user
        else
          false
        end
      end

      # If we are using a Global App and a have loaded a SiteScopedIntegrationInstallation
      # via the user-to-server flow, we can lean on the site scoped installation to determine
      # if we have access to the given resource.
      def current_global_integration_with_installation_can_see_resource?(resource)
        Failbot.push(current_integration: current_integration, current_integration_installation: current_integration_installation)

        case resource
        when DiscussionPost, DiscussionPostReply
          current_global_integration_with_installation_can_see_resource?(current_resource_owner)
        when GpgKey, Platform::PublicResource, Platform::GraphqlUserResource, PublicKey
          return true
        when Integration
          return true if resource.public?
          resource == current_integration
        when Business, Organization
          resource == current_integration_installation.target
        when Project, Team, Codespace
          current_global_integration_with_installation_can_see_resource?(resource.owner)
        when Repository
          return true if resource.public?
          current_integration_installation.repository_ids.include?(resource.id)
        when User
          resource == (graphql_request? ? current_integration_installation.target : current_user)
        else
          false
        end
      end

      def integration_access_allowed?(verb, options)
        installation_required = options.fetch(:installation_required, true)
        return true unless installation_required

        installation = if (current_repo_loaded? || repo_nwo_from_path.present?)
          return true if !options[:approved_integration_required] && current_repo.public?

          if verb == :create_fork
            # We're already checking if the user and the integration both can read the repository
            # earlier in the process. If we don't add this logic, then it will check this again,
            # instead of checking what we need to know: whether or not the integration is
            # allowed to create a repository in the target account or organization.
            # We need to know if the integration is allowed to create a repository onto the target.
            target = options[:organization] ? options[:organization] : current_user
            integration_installation_for(target)
          else
            integration_installation_for(current_repo)
          end
        else
          # Non-repo specific requests, where the given installation belongs to the current Integration
          # See https://github.com/github/platform-integrations/issues/201

          installation = options[:installation]
          if installation.present?
            return installation.integration_id == current_integration.id
          end

          resource = options[:resource]

          # Special case resources that are not bound to an installation,
          # but are needed for AuthZ to be successful.
          if resource.is_a?(PublicResource) || resource.is_a?(Platform::GraphqlUserResource)
            return true
          end

          # In the event that an Organization _and_ User are in play,
          # rely on the Organization having the IntegrationInstallation.
          #
          # See https://github.com/github/ecosystem-apps/issues/560
          if resource.is_a?(User) && options[:organization].is_a?(Organization)
            resource = options[:organization]
          end

          integration_installation_for(resource)
        end

        # In the event we've preloaded an installation during the AuthZ
        # process, go ahead and set it here.
        #
        # We don't set it at the top because there are special return cases,
        # that take precedent.
        if current_integration_installation
          installation = current_integration_installation
        end

        return false unless installation

        installation_options        = options.dup
        installation_options[:user] = fetch_bot_user(installation)

        access_grant(verb, installation_options).access_allowed?
      end

      def global_integration_access_allowed?(verb, options)
        installation_required = options.fetch(:installation_required, true)
        return true unless installation_required

        installation = current_integration_installation
        installation ||= GlobalIntegrationInstallation.new(current_integration)

        return false unless installation

        installation_options        = options.dup
        installation_options[:user] = installation.bot

        access_grant(verb, installation_options).access_allowed?
      end

      # Private: Find the correct IntegrationInstallation
      # record for a given AuthZ `resource`.
      #
      # Returns an IntegrationInstallation or nil.
      def integration_installation_for(resource)
        resource = authz_resource_parent(resource)

        case resource
        when DiscussionPost, DiscussionPostReply
          integration_installation_for(current_resource_owner)
        when Business, Organization, User
          current_integration.installations.find_by(target: resource)
        when Project, Team, Codespace
          integration_installation_for(resource.owner)
        when Repository
          current_integration.installations.with_repository(resource).first
        else
          nil
        end
      end

      # Private: Get the parent resource against which AuthZ checks should be
      # run. In most cases this will be repository in which the resource lives.
      #
      # Returns a resource.
      def authz_resource_parent(resource)
        case resource
        when Codespace
          # Codespaces have a backing repository, but we want to look at the
          # codespace itself for authz, not the backing repository.
          resource
        else
          resource.try(:repository) || resource
        end
      end

      def current_integration_installed_globally?
        Apps::Internal.capable?(:installed_globally, app: current_integration)
      end

      # Public: Fetches the parent integration installation for the current request.
      #
      # Returns an IntegrationInstallation or nil.
      # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
      def current_parent_integration_installation
        @current_parent_integration_installation ||= case current_integration_installation
        when ScopedIntegrationInstallation
          current_integration_installation.parent
        end
      end
      # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

      # Private: Load the Bot for the given IntegrationInstallation
      #
      # Returns a Bot.
      def fetch_bot_user(installation)
        Platform::Loaders::BotByInstallation.load(installation.id).sync
      end

    end
  end
end
