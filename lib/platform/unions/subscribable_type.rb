# frozen_string_literal: true

module Platform
  module Unions
    class SubscribableType < Platform::Unions::Base
      description "The subscribable type for certain billing concepts"
      visibility :internal

      possible_types(
        Objects::MarketplaceListingPlan,
        Objects::SponsorsTier,
      )
    end
  end
end
