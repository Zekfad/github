# rubocop:disable Style/FrozenStringLiteralComment

module Platform
  module Unions
    class Account < Platform::Unions::Base
      description "Users, organizations, and enterprise accounts."
      visibility :internal

      possible_types(
        Objects::User,
        Objects::Organization,
        Objects::Enterprise,
        Objects::Bot,
        Objects::Mannequin,
      )
    end
  end
end
