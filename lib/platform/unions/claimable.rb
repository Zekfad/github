# rubocop:disable Style/FrozenStringLiteralComment

module Platform
  module Unions
    class Claimable < Platform::Unions::Base
      description "An object which can have its data claimed or claim data from another."
      areas_of_responsibility :migration
      feature_flag :mannequin_claiming

      possible_types(
        Objects::User,
        Objects::Mannequin,
      )
    end
  end
end
