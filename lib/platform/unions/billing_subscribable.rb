# frozen_string_literal: true

module Platform
  module Unions
    class BillingSubscribable < Platform::Unions::Base
      description "Objects that a user can subscribe to and be billed for."
      visibility :internal

      possible_types(
        Objects::MarketplaceListingPlan,
        Objects::SponsorsTier,
      )
    end
  end
end
