# frozen_string_literal: true

module Platform
  module Unions
    class Component < Platform::Unions::Base
      description "A composable experience primitive."
      areas_of_responsibility :ecosytem_primitives
      visibility :under_development

      possible_types(
        Objects::InteractiveComponent,
        Objects::MarkdownComponent,
      )
    end
  end
end
