# frozen_string_literal: true

module Platform
  module Unions
    class Base < GraphQL::Schema::Union
      extend Platform::Objects::Base::Visibility
      extend Platform::Objects::Base::AreasOfResponsibility
      extend Platform::Objects::Base::FeatureFlag
      extend Platform::Objects::Base::MobileOnly
      extend Platform::Objects::Base::ClassBasedEdgeType
      extend Platform::Objects::Base::Previewable
    end
  end
end
