# frozen_string_literal: true
require "objspace"

module Platform
  # Implement the `GraphQL::Tracing` API to recording timing & external calls
  # during different query phases and during each field execution.
  class PerformancePaneTracer
    # Used in a few spots when nothing was profiled:
    NO_PROFILE = {}.freeze

    # Durations are in seconds
    # Total duration:
    attr_reader :duration
    # Total objects allocated:
    attr_reader :gc_objects
    # Duration of each phase:
    attr_reader :lexing_duration, :parsing_duration, :validation_duration,
      :analysis_duration, :query_duration, :platform_setup_duration, :platform_teardown_duration
    # Field, authorize, and resolve_type times:
    attr_reader :step_timings, :step_duration
    # Phase times
    attr_reader :phase_timings
    # @return [GraphQL::Query] The query we're tracing
    attr_reader :query

    attr_reader :field_profile_path, :field_profile_type

    def initialize(field_profile_path:, field_profile_type:)
      @phase_timings = []
      @step_timings = []
      # Prepare the profile object, massaging some input:
      @detailed_profile = DetailedProfile.new(
        profile_path: field_profile_path ? field_profile_path.split(".") : [],
        profile_type: field_profile_type,
      )
      @field_profile_path = field_profile_path
      @field_profile_type = field_profile_type
      # Initialize the external call diff, so we can track external calls
      # during resolution
      @external_call_diff = ExternalCallDiff.new
    end

    def field_was_profiled?
      @detailed_profile.was_profiled?
    end

    def display_query
      DisplayQuery.new(
        tracer: self,
      )
    end

    # This is the GraphQL-Ruby API, let's split it out to make it easier to use.
    # Each trace event `key` is split into the matching method below.
    # (See `GraphQL::Tracing` for possible keys.)
    def trace(key, data)
      public_send(:"trace_#{key}", data) { yield }
    end

    def trace_authorized(data)
      name = "#{data[:type].graphql_name}.authorized?"
      path = data[:path] + [name]
      timing, result = step_timing(path: path, name: name, lazy: false) {
        yield
      }
      @step_timings << timing
      result
    end

    def trace_authorized_lazy(data)
      name = "#{data[:type].graphql_name}.authorized?"
      path = data[:path] + [name]
      timing, result = step_timing(path: path, name: name, lazy: true) {
        yield
      }
      @step_timings << timing
      result
    end

    def trace_resolve_type(data)
      name = "#{data[:type].graphql_name}.resolve_type"
      path = data[:path] + [name]
      timing, result = step_timing(path: path, name: name, lazy: false) {
        yield
      }
      @step_timings << timing
      result
    end

    def trace_resolve_type_lazy(data)
      name = "#{data[:type].graphql_name}.resolve_type"
      path = data[:path] + [name]
      timing, result = step_timing(path: path, name: name, lazy: false) {
        yield
      }
      @step_timings << timing
      result
    end

    def trace_platform_execute(_data)
      @start_time = get_time
      initial_gc_objects = GC.stat(:total_allocated_objects)
      result = nil
      GitRPCLogSubscriber.with_track do
        GitHub::MysqlInstrumenter.with_track do
          QueryCacheLogSubscriber.with_track do
            previous_redis_track = Redis::Client.track
            previous_es_track = Elastomer::QueryStats.instance.track
            begin
              Redis::Client.track = true
              Elastomer::QueryStats.instance.track = true
              result = yield
              @finish_time = get_time
            ensure
              Redis::Client.track = previous_redis_track
              Elastomer::QueryStats.instance.track = previous_es_track
            end
          end
        end
      end
      @gc_objects = GC.stat(:total_allocated_objects) - initial_gc_objects

      prepare_aggregates
      result
    end

    def trace_execute_multiplex(data)
      # The query wasn't available in `initialize`, but it is here,
      # so we should make some recordings.
      if !@query
        @query = data[:multiplex].queries[0]
        # This wasn't available when the profile was initialized, so add it now.
        @detailed_profile.query = @query
        Platform::GlobalScope.tracers << self
      end
      @graphql_start_time ||= get_time
      result = yield
      @graphql_finish_time = get_time
      result
    end

    # This records time spent in `GraphQL::Language::Lexer`
    def trace_lex(_data)
      timing, result = step_timing(name: "Lexing", path: ["Lexing"]) { yield }
      @phase_timings << timing
      result
    end

    # This records time spent in `GraphQL::Language::Parser`
    def trace_parse(_data)
      timing, result = step_timing(name: "Parsing", path: ["Parsing"]) { yield }
      @phase_timings << timing
      result
    end

    def trace_validate(_data)
      timing, result = step_timing(name: "Validation", path: ["Validation"]) { yield }
      @phase_timings << timing
      result
    end

    # Measure all time spent in Analyzers
    def trace_analyze_multiplex(_data)
      timing, result = step_timing(name: "Analysis", path: ["Analysis"]) { yield }
      @phase_timings << timing
      result
    end

    # We get all the data we need from `trace_analyze_multiplex`,
    # so this just yields
    def trace_analyze_query(_data)
      yield
    end

    # This measure execution time before _any_ promises are returned.
    # It only makes sense  with `trace_execute_query_lazy` times below.
    def trace_execute_query(data)
      @query_eager_start_time = get_time
      result = yield
      @query_eager_finish_time = get_time
      result
    end

    # This measures times _after_ any Promises are returned
    # (and it includes eager and lazy time _after_ that). It only makes sense
    # with `trace_execute_query` times above.
    def trace_execute_query_lazy(_data)
      @query_lazy_start_time = get_time
      result = yield
      @query_lazy_finish_time = get_time
      result
    end

    # This is time spent in the _initial_ resolve call, before any promises are returned.
    def trace_execute_field(data)
      field = data[:field]
      path = data[:path]
      timing, result = step_timing(path: path, name: field.path) {
        yield
      }
      @step_timings << timing
      result
    end

    # This is time spent calling `#sync` on returned promises.
    def trace_execute_field_lazy(data)
      field = data[:field]
      path = data[:path]
      timing, result = step_timing(path: path, name: field.path, lazy: true) {
        yield
      }
      @step_timings << timing
      result
    end

    private

    # Wrap the block with a bunch of tracking.
    # Extracted here so it can be reused for fields and phases.
    def step_timing(name:, path:, lazy: false)
      # Prepare some variables so they're available outside the block below
      start = nil
      total = nil
      cpu_start = nil
      cpu_total = nil
      allocated_objects_count = nil

      result, allocated_objects, lines = @detailed_profile.with_detailed_profile(path) {
        # Get starting time (wall clock & CPU time)
        start = get_time
        cpu_start = get_time(clock: Process::CLOCK_PROCESS_CPUTIME_ID)
        # See how many objects were _previously_ allocated
        initial_objects_count = GC.stat(:total_allocated_objects)
        res = yield
        # Diff current CPU time against the start time
        cpu_total = get_time(clock: Process::CLOCK_PROCESS_CPUTIME_ID) - cpu_start
        # Diff current allocated objects total against the initial
        allocated_objects_count = GC.stat(:total_allocated_objects) - initial_objects_count
        # Diff current wall clock time against start
        total = get_time - start
        res
      }

      # Read the changes since our last diff
      stats_diff = @external_call_diff.diff
      # Add some local measurements to the array of results
      stats_diff << cpu_total << allocated_objects << allocated_objects_count << lines

      # Capture everything in a timing object
      timing = StepTiming.new(
        start_offset: start - @start_time,
        duration: total,
        name: name,
        path: path,
        lazy: lazy,
        stats: stats_diff,
      )

      # Return the timing and the original result of `yield`
      return timing, result
    end

    # Set a bunch of ivars of aggregate stats.
    # @return [void]
    def prepare_aggregates
      @duration = @finish_time - @start_time

      @step_timings.sort_by!(&:start_offset)
      @step_duration = @step_timings.reduce(0) { |memo, item| memo + item.duration }

      # `@duration` includes some platform setup, this is only GraphQL time:
      @graphql_duration = @graphql_finish_time - @graphql_start_time
      @platform_setup_duration = @graphql_start_time - @start_time
      @platform_teardown_duration = @finish_time - @graphql_finish_time

      # If there was an error, these might not have been initialized:
      @query_eager_start_time ||= @graphql_start_time
      @query_eager_finish_time ||= @query_eager_start_time

      # Merge eager & lazy times into `query_duration`
      query_eager_duration = @query_eager_finish_time - @query_eager_start_time
      query_lazy_duration = @query_lazy_finish_time - @query_lazy_start_time
      @query_duration = query_eager_duration + query_lazy_duration
      # Make sure we don't accidentally record stuff _after_ this:
      freeze
    end

    # Returns time in seconds
    def get_time(clock: Process::CLOCK_MONOTONIC)
      Process.clock_gettime(clock)
    end
  end
end
