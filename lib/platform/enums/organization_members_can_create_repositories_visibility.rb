# frozen_string_literal: true

module Platform
  module Enums
    class OrganizationMembersCanCreateRepositoriesVisibility < Platform::Enums::Base

      description "Repository visibilities values for the members can create repositories setting on an organization."
      areas_of_responsibility :admin_experience, :repositories

      # Added to support granular repo permissions in the enterprise policy admin UI.
      # Could be made public when we're prepared to update docs and probably deprecate
      # members_can_create_repositories_setting_organizations.
      visibility :internal

      value "PUBLIC", "Members can create public repositories."
      value "PRIVATE", "Members can create private repositories."
      value "INTERNAL", "Members can create internal repositories."
      value "NONE", "Members can not create repositories."
    end
  end
end
