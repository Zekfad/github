# frozen_string_literal: true

module Platform
  module Enums
    class MobileAppType < Platform::Enums::Base
      feature_flag :pe_mobile
      areas_of_responsibility :mobile
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]
      description "Represents the different mobile applications."

      value "ANDROID", "Event came from Android app", value: "android"
      value "IOS", "Event came from iOS app", value: "ios"
    end
  end
end
