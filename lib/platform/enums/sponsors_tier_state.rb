# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsTierState < Platform::Enums::Base
      description "The possible states of a Sponsors tier."
      visibility :under_development

      value "DRAFT", "The sponsored developer is drafting the tier.", value: :draft
      value "PUBLISHED", "The sponsored developer has published the tier.", value: :published
      value "RETIRED", "The sponsored developer has retired the tier.", value: :retired
    end
  end
end
