# frozen_string_literal: true

module Platform
  module Enums
    class ActionExecutionCapability < Platform::Enums::Base
      description "The possible capabilities for action executions."
      visibility :internal
      areas_of_responsibility :actions

      value "DISABLED", "All action executions are disabled.", value: Configurable::ActionExecutionCapabilities::DISABLED
      value "ALL_ACTIONS", "All action executions are enabled.", value: Configurable::ActionExecutionCapabilities::ALL_ACTIONS
      value "LOCAL_ACTIONS_ONLY", "Only actions defined within the repo are allowed.", value: Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS
    end
  end
end
