# frozen_string_literal: true

module Platform
  module Enums
    class AuthenticationCredentialType < Platform::Enums::Base
      description "The type of credential used to authenticate."
      visibility :internal

      value "PASSWORD", "A password was used to authenticate", value: "password"
    end
  end
end
