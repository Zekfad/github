# frozen_string_literal: true

module Platform
  module Enums
    class IssueAndPullRequestOrderField < Platform::Enums::Base
      description "Properties by which issue and pull request connections can be ordered."
      feature_flag :pe_mobile

      value "CREATED_AT", "Order issues and pull_requests by creation time", value: "created"
      value "UPDATED_AT", "Order issues and pull_requests by update time", value: "updated"
      value "COMMENTS", "Order issues and pull_requests by comment count", value: "comments"
    end
  end
end
