# frozen_string_literal: true

module Platform
  module Enums
    class NewsletterType < Platform::Enums::Base
      description "Represents the type of newsletter to send."

      visibility :internal

      value "EXPLORE", "The GitHub Explore newsletter.", value: "explore"
      value "VULNERABILITY", "The GitHub vulnerability newsletter.", value: "vulnerability"
    end
  end
end
