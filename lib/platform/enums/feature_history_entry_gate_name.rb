# frozen_string_literal: true

module Platform
  module Enums
    class FeatureHistoryEntryGateName < Platform::Enums::Base
      description "The possible feature flag gate names."
      visibility :internal

      value "BOOLEAN", "The feature is enabled/disabled for everyone", value: :boolean
      value "PERCENTAGE_OF_TIME", "The feature is enabled/disabled for some percentage of calls", value: :percentage_of_time
      value "PERCENTAGE_OF_ACTORS", "The feature is enabled/disabled for some percentage of actors", value: :percentage_of_actors
      value "ACTOR", "The feature is enabled/disabled for a given actor.", value: :actor
      value "GROUP", "The feature is enabled/disabled for a given group.", value: :group
    end
  end
end
