# frozen_string_literal: true

module Platform
  module Enums
    class RepositoryPackageReleaseOrderField < Platform::Enums::Base
      visibility :internal

      description "Properties by which repository package releases can be ordered."

      value "PUBLISHED_ON", "Order package releases by publish time"
      value "UPDATED_AT", "Order package releases by recently updated"
      value "DEPENDENTS", "Order package releases by number of repositories that depend on it"
      value "VULNERABILITIES", "Order package releases by number of vulnerabilities"
    end
  end
end
