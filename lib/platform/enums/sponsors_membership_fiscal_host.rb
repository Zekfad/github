# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsMembershipFiscalHost < Platform::Enums::Base
      description "The fiscal host of a Sponsors membership."
      visibility :internal

      value "NONE", "The membership doesn't use a fiscal host",
        value: ::SponsorsMembership.fiscal_hosts[:none]

      value "OPEN_SOURCE_COLLECTIVE", "The membership users Open Source Collective as fiscal host",
        value: ::SponsorsMembership.fiscal_hosts[:open_source_collective]

      value "SOFTWARE_FREEDOM_CONSERVANCY", "The membership uses Software Freedom Conservancy as fiscal host",
        value: ::SponsorsMembership.fiscal_hosts[:software_freedom_conservancy]

      value "NUMFOCUS", "The membership uses NumFOCUS as fiscal host",
        value: ::SponsorsMembership.fiscal_hosts[:numfocus]

      value "OTHER", "The membership uses a fiscal host that we don't recognize",
        value: ::SponsorsMembership.fiscal_hosts[:other]
    end
  end
end
