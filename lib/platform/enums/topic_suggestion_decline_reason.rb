# frozen_string_literal: true

module Platform
  module Enums
    class TopicSuggestionDeclineReason < Platform::Enums::Base
      description "Reason that the suggested topic is declined."

      value "NOT_RELEVANT", "The suggested topic is not relevant to the repository.",
        value: :not_relevant
      value "TOO_SPECIFIC", "The suggested topic is too specific for the repository " +
                            "(e.g. #ruby-on-rails-version-4-2-1).", value: :too_specific
      value "PERSONAL_PREFERENCE", "The viewer does not like the suggested topic.",
        value: :personal_preference
      value "TOO_GENERAL", "The suggested topic is too general for the repository.",
        value: :too_general
    end
  end
end
