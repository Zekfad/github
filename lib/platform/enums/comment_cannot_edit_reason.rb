# frozen_string_literal: true

module Platform
  module Enums
    class CommentCannotEditReason < Platform::Enums::Base
      description "The possible errors that will prevent a user from editting a comment."

      value "INSUFFICIENT_ACCESS", "You must be the author or have write access to this repository to edit this comment.", value: :insufficient_access
      value "LOCKED", "Unable to create comment because issue is locked.", value: :locked
      value "LOGIN_REQUIRED", "You must be logged in to edit this comment.", value: :login_required
      value "MAINTENANCE", "Repository is under maintenance.", value: :maintenance
      value "VERIFIED_EMAIL_REQUIRED", "At least one email address must be verified to edit this comment.", value: :verified_email_required
    end
  end
end
