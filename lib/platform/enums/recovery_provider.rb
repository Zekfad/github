# frozen_string_literal: true

module Platform
  module Enums
    class RecoveryProvider < Platform::Enums::Base
      description "Origin of approved recovery providers."

      visibility :internal

      value "FACEBOOK", "The Facebook recovery provider origin", value: "https://www.facebook.com"
      value "GITHUB_DEV", "The local, development recovery provider origin", value: "http://github.localhost" unless Rails.env.production?
      value "LOCAL_DEV", "The local, development recovery provider origin", value: "http://localhost:9292" unless Rails.env.production?
      value "EXAMPLE_PROVIDER", "The local, test recovery provider origin", value: "https://example-provider.org" unless Rails.env.production?
    end
  end
end
