# frozen_string_literal: true

module Platform
  module Enums
    class UserAssociatedRecordType < Platform::Enums::Base
      description "The possible user-associated record types."
      visibility :internal

      value "COMMIT_COMMENT", "Commit comment.", value: :commit_comments
      value "FAN", "Fan (aka follower).", value: :followeds
      value "GIST", "Gist.", value: :gists
      value "GIST_COMMENT", "Gist comment.", value: :gist_comments
      value "HERO", "Hero (aka following).", value: :followings
      value "ISSUE", "Issue.", value: :issues
      value "ISSUE_COMMENT", "Issue comment.", value: :issue_comments
      value "PULL", "Pull Request.", value: :pull_requests
      value "REPO", "Repository.", value: :repositories
      value "STAR", "Star.", value: :stars
    end
  end
end
