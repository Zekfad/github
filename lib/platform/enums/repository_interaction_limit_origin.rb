# frozen_string_literal: true

module Platform
  module Enums
    class RepositoryInteractionLimitOrigin < Platform::Enums::Base
      description "Indicates where an interaction limit is configured."

      visibility :under_development, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      value "REPOSITORY", "A limit that is configured at the repository level.", value: :repository
      value "ORGANIZATION", "A limit that is configured at the organization level.", value: :organization
    end
  end
end
