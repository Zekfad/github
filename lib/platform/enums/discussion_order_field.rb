# frozen_string_literal: true

module Platform
  module Enums
    class DiscussionOrderField < Platform::Enums::Base
      description "Properties by which discussion connections can be ordered."
      areas_of_responsibility :discussions
      visibility :under_development

      value "CREATED_AT", "Order discussions by creation time.", value: "created_at"
      value "UPDATED_AT", "Order discussions by update time.", value: "updated_at"
      value "COMMENT_COUNT", "Order discussions by how many comments they received.",
        value: "comment_count"
      value "SCORE", "Order discussions by how many times they have been bumped.",
        value: "score"
    end
  end
end
