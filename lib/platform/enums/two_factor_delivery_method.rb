# frozen_string_literal: true

module Platform
  module Enums
    class TwoFactorDeliveryMethod < Platform::Enums::Base
      description "The method by which two factor TOTP codes are used."
      visibility :internal

      value "SMS", "TOTP codes are sent on-demand to mobile devices.", value: "sms"
      value "APP", "The TOTP seed is stored on in a user's authenticator app.", value: "app"
    end
  end
end
