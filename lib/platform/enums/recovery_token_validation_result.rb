# frozen_string_literal: true

module Platform
  module Enums
    class RecoveryTokenValidationResult < Platform::Enums::Base
      description "Possible token states."

      visibility :internal

      value "SIGNATURE_ONLY_SUCCESS", "The token signatures is valid but the inner token might not be", value: :signature_only_success
      value "SUCCESS", "The token appears to be valid", value: :success
      value "LEGACY_SUCCESS", "The token appears to be valid", value: :legacy_success

      # Unsuccessful recovery attempts
      value "EARTHSMOKE_DISABLED", "Earthsmoke has been deliberatedly disabled for this user", value: :earthsmoke_disabled
      value "SECRET_USER_ID_MISMATCH", "The secret value does not match what is in our database", value: :secret_user_id_mismatch
      value "EARTHSMOKE_SECRET_USER_ID_MISMATCH", "The secret value does not match what is in our database", value: :earthsmoke_secret_user_id_mismatch
      value "NO_CONFIRMED_TOKENS_MATCHED", "No matching token ID could be found", value: :no_confirmed_tokens_matched
      value "UNVERIFIABLE_TOKEN", "The token signatures or format appear to be invalid", value: :unverifiable_token
      value "EARTHSMOKE_SERVER_ERROR", "The token secret could not be decoded because of an earthsmoke error", value: :earthsmoke_server_error
      value "BAD_LEGACY_DECODE", "The token could not be decoded for some unknown reason", value: :bad_legacy_decode
    end
  end
end
