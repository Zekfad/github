# frozen_string_literal: true

module Platform
  module Enums
    class FeatureState < Platform::Enums::Base
      description "The possible feature flag states."
      visibility :internal

      value "CONDITIONAL", "These features are only enabled on a conditional basis, either by percentage of random, percentage of actors, individual users, or specific groups.", value: :conditional
      value "ENABLED", "These features are enabled for everyone.", value: :enabled
      value "DISABLED", "These features are turned off for everyone.", value: :disabled
    end
  end
end
