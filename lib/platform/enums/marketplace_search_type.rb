# frozen_string_literal: true

module Platform
  module Enums
    class MarketplaceSearchType < Platform::Enums::Base
      description "Represents available types of result you can get back from a Marketplace search."
      visibility :internal
      areas_of_responsibility :marketplace

      value "MARKETPLACE", "GitHub Marketplace listings", value: "marketplace"
      value "MARKETPLACE_TOOLS", "GitHub Marketplace listings and actions", value: "marketplace-tools"
      value "MARKETPLACE_ACTIONS", "GitHub Marketplace actions", value: "repository-action"
      value "NON_MARKETPLACE", "Works with GitHub listings", value: "works-with-github"
    end
  end
end
