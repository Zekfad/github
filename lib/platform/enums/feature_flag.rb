# frozen_string_literal: true

module Platform
  module Enums
    # Client Side Feature Flags
    #
    # ## Good uses of client side feature flags
    #
    # * Dark shipping a performance change or installing a stealth network
    # request for load testing
    # * Testing a new JS library
    # * A/B testing UI changes
    # * Enabling new UI to be rendered in a view
    #
    # ## Remember to enforce feature checks at the data access layer
    #
    # These feature flags can only prevent UI from displaying data, however,
    # they do not enforce access. When new data types and mutations are added,
    # those APIs must also check the internal feature flag to ensure new data is
    # not exposed to the user.
    #
    # ## Naming Conventions
    #
    # The public enum MAY be an uppercased value of the internal feature flag
    # name.
    #
    # However, if the name references a highly sensitive upcoming product
    # launch, consider using a codename as the enum value. The internal symbol
    # value will remain private and actually be used to test `feature_enabled?`.
    #
    class FeatureFlag < Platform::Enums::Base
      visibility :internal

      description <<~MD
        Set of client side exposed feature flags.
      MD

      value "SPONSORS" do
        value :sponsors
        description <<~MD
          Visibility for GitHub Sponsors platform. Owned by #communities.

          CC: @github/communities-sparkle-reviewers
        MD
      end

      value "CORPORATE_SPONSORS_CREDIT_CARD" do
        value :corporate_sponsors_credit_card
        description <<~MD
          Allows users to sponsor on behalf of an organization as part of Phase 1 of Corporate Sponsors

          github/sponsors#1293

          CC: @github/communities-sparkle-reviewers
        MD
      end

      value "MARKETPLACE_PENDING_INSTALLATIONS" do
        value :marketplace_pending_installations
        description <<~MD
          Display Marketplace purchases that have not been properly installed.

          Project board: https://github.com/orgs/github/projects/768

          CC: @github/marketplace-eng

          https://github.com/devtools/feature_flags/marketplace_pending_installations
        MD
      end

      value "SEARCH_SUGGESTIONS" do
        value :search_suggestions
        description <<~MD
          Advanced search autocomplete

          CC: @koddsson

          https://github.com/devtools/feature_flags/search_suggestions
        MD
      end

      value "NON_BLOCKING_CSS" do
        value :non_blocking_css
        description <<~MD
          Only load frameworks.css as blocking resources for performance gains.

          CC: @github/web-systems
        MD
      end

      value "GITHUB_APPS_IN_MARKETPLACE_SEARCH" do
        value :github_apps_in_marketplace_search
        description <<~MD
          Include the top ~500 GitHub Apps by installation count in Marketplace Search results

          CC: @github/marketplace-eng
        MD
      end

      value "JS_IN_HEAD" do
        value :js_in_head
        description <<~MD
          Load JavaScript via <script> tags in the <head> tag rather then the end of the <body>.

          CC: @github/web-systems
        MD
      end

      value "JS_HTTP_CACHE_HEADERS" do
        value :js_http_cache_headers
        description <<~MD
          Rely on HTTP caching for pretech PJAX behaviors.

          CC: @github/web-systems
        MD
      end

      value "ACTIONS_WORKFLOW_RUN" do
        value :actions_workflow_run_webhook
        description <<~MD
          Support a new `workflow_run` event for GitHub Actions.

          CC: @github/c2c-actions-experience
        MD
      end

      value "ACTIONS_PULL_REQUEST_TARGET" do
        value :actions_pull_request_target
        description <<~MD
          Support a new `pull_request_target` event for GitHub Actions.

          CC: @github/c2c-actions-experience
        MD
      end

      value "ACTIONS_DEFAULT_BRANCH_WARNING" do
        value :actions_default_branch_warning
        description <<~MD
          Enables a warning for using `master` in workflow files for actions owned by the `actions` org. e.g. `actions/checkout@master`

          CC: @github/c2c-actions-experience
        MD
      end
    end
  end
end
