# frozen_string_literal: true

module Platform
  module Enums
    class RepositoryLockReason < Platform::Enums::Base
      description "The possible reasons a given repository could be in a locked state."

      value "MOVING", "The repository is locked due to a move.", value: "moving"
      value "BILLING", "The repository is locked due to a billing related reason.", value: "billing"
      value "RENAME", "The repository is locked due to a rename.", value: "rename"
      value "MIGRATING", "The repository is locked due to a migration.", value: "migrating"
    end
  end
end
