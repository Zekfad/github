# frozen_string_literal: true

module Platform
  module Enums
    class PlanFeature < Platform::Enums::Base
      visibility :under_development
      areas_of_responsibility :gitcoin

      description <<~MD
        Set of features which are supported based on the billing plan.
      MD

      GitHub::Plan::FEATURES.each do |name, desc|
        value name.to_s.upcase do
          value name
          description desc
        end
      end
    end
  end
end
