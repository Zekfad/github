# frozen_string_literal: true

module Platform
  module Enums
    class RepositoryInteractionLimitExpiry < Platform::Enums::Base
      description "The length for a repository interaction limit to be enabled for."

      visibility :internal

      value "ONE_DAY", "The interaction limit will expire after 1 day.", value: :one_day
      value "ONE_WEEK", "The interaction limit will expire after 1 week.", value: :one_week
    end
  end
end
