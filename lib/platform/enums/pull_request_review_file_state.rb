# frozen_string_literal: true

module Platform
  module Enums
    class PullRequestReviewFileState < Platform::Enums::Base
      description "The possible states of a pull request review file."
      areas_of_responsibility :pull_requests

      visibility :under_development

      value "PENDING", "A file status that is part of a pending review", value: "pending"
      value "SUBMITTED", "A file status that is part of a submitted review", value: "submitted"
    end
  end
end
