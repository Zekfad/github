# frozen_string_literal: true

module Platform
  module Enums
    class Base < GraphQL::Schema::Enum
      class EnumValue < GraphQL::Schema::EnumValue
        include Platform::Objects::Base::AreasOfResponsibility
        include Platform::Objects::Base::Deprecated
        include Platform::Objects::Base::Visibility
        include Platform::Objects::Base::FeatureFlag
        include Platform::Objects::Base::MobileOnly
        include Platform::Objects::Base::Previewable

        def initialize(*args, visibility: nil, feature_flag: nil, **kwargs, &block)
          visibility_from_config(visibility)

          self.feature_flag(feature_flag) if feature_flag

          super(*args, **kwargs, &block)
        end
      end
      extend Platform::Objects::Base::AreasOfResponsibility
      extend Platform::Objects::Base::FeatureFlag
      extend Platform::Objects::Base::MobileOnly
      extend Platform::Objects::Base::Visibility
      extend Platform::Objects::Base::Previewable

      enum_value_class(EnumValue)

      def self.value(*args, **kwargs)
        super
      end

      # @return [Array<String>] The possible GraphQL values for this enum
      def self.graphql_values
        values.keys
      end

      # Ensures the given string is properly formatted to be used as an enum value.
      #
      # - str: A String supposed to be used as an enum value.
      #
      # Returns a String that can be used as a enum value.
      def self.convert_string_to_enum_value(str)
        str.parameterize.underscore.upcase
      end

      def self.default_graphql_name
        @default_graphql_name ||= name.split("::").last
      end
    end
  end
end
