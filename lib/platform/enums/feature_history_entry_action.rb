# frozen_string_literal: true

module Platform
  module Enums
    class FeatureHistoryEntryAction < Platform::Enums::Base
      description "The possible actions for a feature."
      visibility :internal

      value "CREATED", "The feature was created", value: :create
      value "DESTROY", "The feature was destroyed", value: :destroy
      value "ENABLED", "The feature was enabled.", value: :enable
      value "DISABLED", "These feature was disabled", value: :disable
    end
  end
end
