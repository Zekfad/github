# frozen_string_literal: true

module Platform
  module Enums
    class MobileSubjectType < Platform::Enums::Base
      feature_flag :pe_mobile
      areas_of_responsibility :mobile
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]
      description "Represents the different mobile subject types."

      value "REPOSITORY", "The subject of the event is a repository.", value: "repository"
      value "PULL_REQUEST", "The subject of the event is a pull request.", value: "pull_request"
      value "ISSUE", "The subject of the event is a issue.", value: "issue"
      value "USER", "The subject of the event is a user.", value: "user"
      value "ORGANIZATION", "The subject of the event is a organization.", value: "organization"
      value "COMMIT", "The subject of the event is a commit.", value: "commit"
      value "GIST", "The subject of the event is a gist.", value: "gist"
      value "TEAM_DISCUSSION", "The subject of the event is a team discussion.", value: "team_discussion"
      value "DISCUSSION", "The subject of the event is a discussion.", value: "discussion"
      value "CHECK_SUITE", "The subject of the event is a check suite.", value: "check_suite"
      value "RELEASE", "The subject of the event is a release.", value: "release"
      value "REPOSITORY_ADVISORY", "The subject of the event is a repository advisory.", value: "repository_advisory"
      value "REPOSITORY_VULNERABILITY_ALERT", "The subject of the event is a repository vulnerablity alert.", value: "repository_vulnerability_alert"
    end
  end
end
