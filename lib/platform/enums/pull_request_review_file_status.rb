# frozen_string_literal: true

module Platform
  module Enums
    class PullRequestReviewFileStatus < Platform::Enums::Base
      description "The possible status of a pull request review file."
      areas_of_responsibility :pull_requests

      visibility :under_development

      value "APPROVED", "A file status approving the changes.", value: "approved"
      value "CHANGES_REQUESTED", "A file status requesting changes.", value: "changes_requested"
    end
  end
end
