# frozen_string_literal: true

module Platform
  module Enums
    class UserLicenseType < Platform::Enums::Base
      description "The possible types of user licenses."
      visibility :internal
      areas_of_responsibility :gitcoin

      UserLicense.license_types.each_key do |type|
        value type.upcase, "#{type.humanize} licenses", value: type
      end
    end
  end
end
