# frozen_string_literal: true

module Platform
  module Enums
    class SponsorshipNewsletterOrderField < Platform::Enums::Base
      description "Properties by which sponsorship update connections can be ordered."
      visibility :internal

      value "CREATED_AT", "Order sponsorship updates by creation time.", value: "created_at"
    end
  end
end
