# frozen_string_literal: true

module Platform
  module Enums
    class SpamuraiAccountClassification < Platform::Enums::Base
      description "Spamurai account classification."
      visibility :internal

      value "HAMMY", "Hammy.", value: "hammy"
      value "SPAMMY", "Spammy.", value: "spammy"
      value "UNKNOWN", "Unknown.", value: "unknown"
    end
  end
end
