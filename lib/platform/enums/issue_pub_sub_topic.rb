# frozen_string_literal: true

module Platform
  module Enums
    class IssuePubSubTopic < Platform::Enums::Base
      description "The possible PubSub channels for an issue."

      value "UPDATED", "The channel ID for observing issue updates.", value: "updated"
      value "TIMELINE", "The channel ID for updating items on the issue timeline.", value: "timeline"
      value "STATE", "The channel ID for observing issue state updates.", value: "state"
    end
  end
end
