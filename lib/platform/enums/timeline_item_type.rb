# frozen_string_literal: true

module Platform
  module Enums
    class TimelineItemType < Platform::Enums::Base
      graphql_name "TimelineItemType"
      description "The possible item types for a timeline."

      Unions::PullRequestTimelineItems.possible_types.each do |type|
        value type.graphql_name.underscore.upcase, type.description, value: type
      end
    end
  end
end
