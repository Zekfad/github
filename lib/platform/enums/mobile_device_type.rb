# frozen_string_literal: true

module Platform
  module Enums
    class MobileDeviceType < Platform::Enums::Base
      feature_flag :pe_mobile
      areas_of_responsibility :mobile
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]
      description "Represents the different mobile devices."

      value "PHONE", "Event came from a phone device.", value: "phone"
      value "TABLET", "Event came from a tablet device.", value: "tablet"
    end
  end
end
