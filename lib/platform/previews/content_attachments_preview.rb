# frozen_string_literal: true

class Platform::Previews::ContentAttachmentsPreview < Platform::Preview
  title "Create content attachments"

  description "This preview adds support for creating content attachments."

  owning_teams "@github/ce-extensibility"

  toggled_by :"corsair-preview"

  toggled_on [
  "Mutation.createContentAttachment",
]
end
