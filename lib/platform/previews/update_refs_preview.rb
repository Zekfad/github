# frozen_string_literal: true

class Platform::Previews::UpdateRefsPreview < Platform::Preview
  title "UpdateRefsPreview - Update multiple refs in a single operation."

  description "This preview adds support for updating multiple refs in a single operation."

  owning_teams "@github/pe-repos"

  toggled_by :"update-refs-preview"

  toggled_on ["Mutation.updateRefs", "GitRefname", "RefUpdate", "UpdateRefsInput", "UpdateRefsPayload"]
end
