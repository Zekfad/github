# frozen_string_literal: true

class Platform::Previews::TeamReviewAssignmentsPreview < Platform::Preview
  title "Team Review Assignments Preview"

  description "This preview adds support for updating the settings for team review assignment."
  owning_teams "@github/pe-pull-requests"

  toggled_by :"stone-crop-preview"

  toggled_on [
    "Mutation.updateTeamReviewAssignment",
    "UpdateTeamReviewAssignmentInput",
    "TeamReviewAssignmentAlgorithm",
    "Team.reviewRequestDelegationEnabled",
    "Team.reviewRequestDelegationAlgorithm",
    "Team.reviewRequestDelegationMemberCount",
    "Team.reviewRequestDelegationNotifyTeam",
  ]
end
