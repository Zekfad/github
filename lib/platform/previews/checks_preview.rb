# frozen_string_literal: true

class Platform::Previews::ChecksPreview < Platform::Preview
  title "Checks"

  description "This preview adds support for reading checks created by GitHub Apps."

  owning_teams "@github/ce-extensibility"

  toggled_by :"antiope-preview"

  toggled_on [
  "CheckAnnotationRange",
  "CheckAnnotationPosition",
  "CheckAnnotationSpan",
  "CheckAnnotation",
  "CheckAnnotationConnection.nodes",
  "CheckAnnotationData",
  "CheckAnnotationEdge.node",
  "CheckAnnotationLevel",
  "CheckConclusionState",
  "CheckStatusState",
  "CheckSuiteAutoTriggerPreference",
  "CheckRun",
  "CheckRunConnection.nodes",
  "CheckRunEdge.node",
  "CheckRunAction",
  "CheckRunFilter",
  "CheckRunOutput",
  "CheckRunOutputImage",
  "CheckRunType",
  "CheckSuite",
  "CheckSuiteConnection.nodes",
  "CheckSuiteEdge.node",
  "CheckSuiteFilter",
  "CreateCheckRunInput",
  "CreateCheckRunPayload",
  "CreateCheckSuiteInput",
  "CreateCheckSuitePayload",
  "Commit.checkSuites",
  "Mutation.createCheckRun",
  "Mutation.createCheckSuite",
  "Mutation.rerequestCheckSuite",
  "Mutation.updateCheckRun",
  "Mutation.updateCheckSuitePreferences",
  "Push",
  "RequestableCheckStatusState",
  "RerequestCheckSuiteInput",
  "RerequestCheckSuitePayload",
  "UpdateCheckRunInput",
  "UpdateCheckRunPayload",
  "UpdateCheckSuitePayload.checkSuite",
  "UpdateCheckSuitePreferencesInput",
  "UpdateCheckSuitePreferencesPayload",
]
end
