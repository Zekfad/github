# frozen_string_literal: true

class Platform::Previews::DeletePackageVersionPreview < Platform::Preview
  title "Access to package version deletion"

  description "This preview adds support for the DeletePackageVersion mutation which enables deletion of private package versions."

  owning_teams "@github/pe-package-registry"

  toggled_by :"package-deletes-preview"

  toggled_on [
    "Mutation.deletePackageVersion",
  ]
end
