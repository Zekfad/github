# frozen_string_literal: true

class Platform::Previews::LabelsPreview < Platform::Preview
  title "Labels Preview"

  description "This preview adds support for adding, updating, creating and deleting labels."

  owning_teams "@github/pe-pull-requests"

  toggled_by :"bane-preview"

  toggled_on [
    "Mutation.createLabel",
    "CreateLabelPayload",
    "CreateLabelInput",
    "Mutation.deleteLabel",
    "DeleteLabelPayload",
    "DeleteLabelInput",
    "Mutation.updateLabel",
    "UpdateLabelPayload",
    "UpdateLabelInput",
  ]
end
