# frozen_string_literal: true

class Platform::Previews::ProjectEventDetailsPreview < Platform::Preview
  title "Project Event Details"

  description "This preview adds project, project card, and project column details to project-related issue events."

  owning_teams "@github/github-projects"

  toggled_by :"starfox-preview"

  toggled_on [
    "AddedToProjectEvent.project",
    "AddedToProjectEvent.projectCard",
    "AddedToProjectEvent.projectColumnName",
    "ConvertedNoteToIssueEvent.project",
    "ConvertedNoteToIssueEvent.projectCard",
    "ConvertedNoteToIssueEvent.projectColumnName",
    "MovedColumnsInProjectEvent.project",
    "MovedColumnsInProjectEvent.projectCard",
    "MovedColumnsInProjectEvent.projectColumnName",
    "MovedColumnsInProjectEvent.previousProjectColumnName",
    "RemovedFromProjectEvent.project",
    "RemovedFromProjectEvent.projectColumnName",
  ]
end
