# frozen_string_literal: true

class Platform::Previews::MergeInfoPreview < Platform::Preview
  title "MergeInfoPreview - More detailed information about a pull request's merge state."

  description "This preview adds support for accessing fields that provide more detailed information about a pull request's merge state."

  owning_teams "@github/pe-pull-requests"

  toggled_by :"merge-info-preview"

  toggled_on ["PullRequest.canBeRebased", "PullRequest.mergeStateStatus"]
end
