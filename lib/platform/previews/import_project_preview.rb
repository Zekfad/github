# frozen_string_literal: true

class Platform::Previews::ImportProjectPreview < Platform::Preview
  title "Import Project"

  description "This preview adds support for importing projects."

  owning_teams "@github/pe-issues-projects"

  toggled_by :"slothette-preview"

  toggled_on [
    "Mutation.importProject",
  ]
end
