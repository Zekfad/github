# frozen_string_literal: true

module Platform
  class Response
    class QueryInstrumenter
      def initialize(query, internal_error: nil)
        @query = query
        @validation_errors = query.validation_errors
        @analysis_errors = query.analysis_errors
        @execution_errors = @query.context.errors
        @internal_error = internal_error
      end

      def to_graphql_query_event
        # Only Instrument ORIGIN_API for now.
        return unless @query.context[:origin] == Platform::ORIGIN_API

        viewer = @query.context[:viewer]

        oauth_app = if viewer && viewer.using_personal_access_token?
          viewer.oauth_access.safe_app
        else
          @query.context[:oauth_app]
        end

        integration = @query.context[:integration]

        schema_version = if @query.context[:target] == :public
          Platform::Schema::PUBLIC_SHA
        else
          Platform::Schema::INTERNAL_SHA
        end

        query_string = @query.context[:scrubbed_query] || @query.context[:query_string]
        query_tracker = @query.context[:query_tracker]

        GlobalInstrumenter.instrument(
          Platform::QUERY_EVENT_KEY,
          {
            query_string: query_string,
            app: oauth_app || integration,
            viewer: viewer,
            origin: @query.context[:origin],
            target: @query.context[:target],
            schema_version: schema_version,
            dotcom_sha: GitHub.current_sha,
            operation_type:  @query.selected_operation&.operation_type,
            selected_operation: @query.selected_operation_name,
            valid: @query.valid?,
            errors: serialized_errors_for_instrumentation,
            accessed_objects: query_tracker.accessed_objects.values,
            query_hash: query_tracker.query_hash,
            variables_hash: query_tracker.variables_hash,
            cpu_time_ms: query_tracker.clock_times[:cpu].round,
            idle_time_ms: query_tracker.clock_times[:idle].round,
            mysql_count: query_tracker.mysql_count,
            mysql_time_ms: query_tracker.performance_data_total_sql.round,
            gitrpc_count: query_tracker.gitrpc_count,
            gitrpc_time_ms: query_tracker.gitrpc_time_ms.round,
            timed_out: false,
            node_count: @query.context[:node_count_total],
            complexity_cost: @query.context[:cost_total],
          },
        )
      end

      def serialized_errors_for_instrumentation
        serialized_errors = []

        if @internal_error
          serialized_errors << {
            type: "INTERNAL",
            code: @internal_error.class.name.demodulize.downcase.underscore,
            message: @internal_error.to_s,
          }
        end

        @validation_errors.each do |e|
          # Some special cases are from the GraphQL ruby gem (see validation_pipeline.rb
          # in the gem). These get added to the validation_errors array but do not come
          # from a root of StaticValidation::Error hence missing code information.
          code = if e.is_a?(GraphQL::StaticValidation::Error) && e.respond_to?(:code)
            e.code.underscore
          elsif e.is_a? GraphQL::Query::OperationNameMissingError
            "operation_name_missing"
          elsif e.is_a? GraphQL::Query::VariableValidationError
            "variable_validation"
          end

          code = e.class.name.demodulize.downcase.underscore unless code

          serialized_errors << {
            type: "VALIDATION",
            code: code,
            message: e.message,
            path: path(e),
            locations: locations(e),
          }
        end

        @analysis_errors.each do |e|
          code = if e.respond_to?(:type)
            e.type.downcase
          else
            e.class.name.demodulize.downcase.underscore
          end

          serialized_errors << {
            type: "ANALYSIS",
            code: code,
            message: e.message,
            path: path(e),
            locations: locations(e),
          }
        end

        @execution_errors.each do |e|
          code = if e.respond_to?(:type)
            e.type.downcase
          else
            e.class.name.demodulize.downcase.underscore
          end

          serialized_errors << {
            type: "EXECUTION",
            code: code,
            message: e.message,
            # Not all errors have a path component - e.g. GraphQL::InvalidNullError
            path: path(e),
            locations: locations(e),
          }
        end

        serialized_errors
      end

      def path(error)
        (error.try(:path) || []).map(&:to_s)
      end

      def locations(error)
        # Locations is a private API on some errors,
        # and sometimes only accessible through `to_h`
        # TODO: fix this in graphql-ruby
        if error.respond_to?(:locations, true)
          error.send(:locations).map(&:stringify_keys)
        else
          (error.to_h["locations"] || []).map(&:stringify_keys)
        end
      end
    end
  end
end
