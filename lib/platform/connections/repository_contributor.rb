# frozen_string_literal: true

module Platform
  module Connections
    class RepositoryContributor < Connections::Base
      edge_type(Edges::RepositoryContributor)
      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :user_profile

      total_count_field

      def nodes
        @object.edge_nodes.map do |user_contribution|
          if user_contribution.is_a? CommitContribution
            user_contribution.async_user
          else
            user_contribution.user
          end
        end
      end
    end
  end
end
