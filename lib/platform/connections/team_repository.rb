# frozen_string_literal: true

module Platform
  module Connections
    class TeamRepository < Connections::Base
      total_count_field
    end
  end
end
