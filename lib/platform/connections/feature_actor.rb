# frozen_string_literal: true

module Platform
  module Connections
    class FeatureActor < Connections::Base
      # TODO don't require this, it's needed because of `Interfaces`
      edge_type Interfaces::FeatureActor.edge_type, node_type: Interfaces::FeatureActor
      description "A list of feature actors."
      visibility :internal

      total_count_field
    end
  end
end
