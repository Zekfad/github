# frozen_string_literal: true

module Platform
  module Connections
    class TeamProject < Connections::Base
      edge_type(Edges::TeamProject, edge_class: Models::TeamProjectEdge)

      total_count_field
    end
  end
end
