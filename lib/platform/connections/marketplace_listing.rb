# frozen_string_literal: true

module Platform
  module Connections
    class MarketplaceListing < Connections::Base
      description "Look up Marketplace Listings"
      areas_of_responsibility :marketplace

      total_count_field
    end
  end
end
