# frozen_string_literal: true

module Platform
  module Connections
    class ProjectTeam < Connections::Base
      edge_type(Edges::ProjectTeam, edge_class: Models::ProjectTeamEdge)
      total_count_field
    end
  end
end
