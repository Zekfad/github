# frozen_string_literal: true

module Platform
  module Connections
    class Base < Platform::Objects::Base
      extend Forwardable
      def_delegators :@object, :cursor_from_node, :parent

      include GitHub::Relay::GlobalIdentification

      def self.async_viewer_can_see?(*)
        true # filter edges/nodes instead
      end

      def self.async_api_can_access?(*)
        true # filter edges/nodes instead
      end

      class << self
        attr_reader :edge_class
      end

      # Configure this connection to return `edges` and `nodes` based on `edge_type_class`.
      #
      # This method will use the inputs to create:
      # - `edges` field
      # - `nodes` field
      # - description
      #
      # It's called when you subclass this base connection, trying to use the
      # class name to set defaults. You can call it again in the class definition
      # to override the default (or provide a value, if the default lookup failed).
      def self.edge_type(edge_type_class, edge_class: GraphQL::Relay::Edge, node_type: nil)
        @edge_class = edge_class

        field :edges, [edge_type_class, null: true],
          null: true,
          description: "A list of edges.",
          edge_class: edge_class

        if node_type.nil?
          if edge_type_class.is_a?(Class)
            node_type = edge_type_class.fields["node"].type
          elsif edge_type_class.is_a?(GraphQL::ObjectType)
            # This was created with `.edge_type`
            node_type = Platform::Objects.const_get(edge_type_class.name.sub("Edge", ""))
          else
            raise ArgumentError, "Can't get node type from edge type: #{edge_type_class}" # rubocop:disable GitHub/UsePlatformErrors
          end
        end

        if node_type.respond_to?(:of_type)
          node_type = node_type.of_type
        end

        field :nodes, [node_type, null: true],
          null: true,
          description: "A list of nodes."

        description("The connection type for #{node_type.graphql_name}.")

        if node_type.respond_to?(:feature_flag) && node_type.feature_flag
          self.feature_flag(node_type.feature_flag)
          edge_type_class.feature_flag(node_type.feature_flag)
        end

        if node_type.respond_to?(:visibility) &&
            node_type.visibility.present? &&
            edge_type_class.default_visibility?

          node_type.environment_visibilities.each do |env, visibilities|
            self.visibility(visibilities, environments: [env])
            edge_type_class.visibility(visibilities, environments: [env])
          end
        end

        if node_type.respond_to?(:areas_of_responsibility) && node_type.areas_of_responsibility.present?
          self.areas_of_responsibility(node_type.areas_of_responsibility)
          edge_type_class.areas_of_responsibility(node_type.areas_of_responsibility)
        end
      end

      def self.inherited(child_class)
        super
        # The parent `node` and child `nodes` will be scoped
        child_class.scopeless_tokens_as_minimum
        # The class is named just like the object type that the connection wraps,
        # but that will be a naming conflict, so append `Connection` to the class name
        # for graphql purposes.
        node_class_name = child_class.name
        if node_class_name.nil?
          # Anonymous classes don't have a name, assume `edge_type`
          # will be called later.
          return
        end

        type_name = node_class_name.split("::").last
        child_class.graphql_name("#{type_name}Connection")

        # Use `require_dependency` so that the types will be loaded, if they exist.
        # Otherwise, `const_get` may reach a top-level constant (eg, `::UserContentEdit` instead of `Platform::Objects::UserContentEdit`).
        # That behavior is removed in Ruby 2.5, then we can remove these require_dependency calls too.
        begin
          require_dependency "platform/edges/#{type_name.underscore}"
          # Look for a custom edge whose name matches this connection's name
          wrapped_edge_class = Platform::Edges.const_get(type_name)
          wrapped_node_class = wrapped_edge_class.fields["node"].type
        rescue LoadError => err
          begin
            require_dependency "platform/objects/#{type_name.underscore}"
            # If there isn't one, look for an object whose name matches this name.
            wrapped_node_class = Platform::Objects.const_get(type_name)
            wrapped_edge_class = wrapped_node_class.edge_type
          rescue LoadError => err
            # Assume that `edge_type` will be called later
          end
        end

        # If a default could be found using constant lookups, generate the fields for it.
        if wrapped_edge_class
          if wrapped_edge_class.is_a?(GraphQL::ObjectType) || (wrapped_edge_class.is_a?(Class) && wrapped_edge_class < Platform::Edges::Base)
            child_class.edge_type(wrapped_edge_class, node_type: wrapped_node_class)
          else
            raise TypeError, "Missed edge type lookup, didn't find a type definition: #{type_name.inspect} => #{wrapped_edge_class.inspect}"  # rubocop:disable GitHub/UsePlatformErrors
          end
        end
      end

      field :page_info, GraphQL::Types::Relay::PageInfo, null: false, description: "Information to aid in pagination."

      # By default this calls through to the ConnectionWrapper's edge nodes method,
      # but sometimes you need to override it to support the `nodes` field
      def nodes
        @object.edge_nodes
      end

      def edges
        context.schema.after_lazy(object.edge_nodes) do |nodes|
          # Support local connection overrides which may have their own edge class.
          # (The goal is to migrate all connections here soon, see https://github.com/rmosolgo/graphql-ruby/pull/2143)
          edge_class = object.class.respond_to?(:edge_class) ? object.class.edge_class : self.class.edge_class
          nodes.map { |n| edge_class.new(n, object) }
        end
      end

      # Use this method to add the default total_count field to your connection.
      # For historical reasons, some connections don't have this field, so we
      # can't add it to the base class ... yet ... ?
      def self.total_count_field(description: nil)
        field :total_count, Integer, null: false,
          description: description || "Identifies the total count of items in the connection."
      end

      # A default implementation that tries to infer an efficient total
      # from the current object. Don't hesitate to override it if you can
      # provide a better implementation in your own connection
      def total_count
        case object
        when Platform::ConnectionWrappers::RepositoryPackageReleases
          object.total_count
        when GraphQL::Relay::ArrayConnection,
             Platform::ConnectionWrappers::StableArray,
             Platform::ConnectionWrappers::ManifestDependenciesConnection
          object.nodes.count
        when Platform::ConnectionWrappers::Promise,
             Platform::ConnectionWrappers::ElasticSearchQuery,
             Platform::ConnectionWrappers::Timeline,
             Platform::ConnectionWrappers::ProjectCards,
             Platform::ConnectionWrappers::Relation,
             Platform::ConnectionWrappers::ExperimentRelation,
             Platform::ConnectionWrappers::Notifications,
             Platform::ConnectionWrappers::NotificationThreadSubscriptions,
             Platform::ConnectionWrappers::AuditLogQuery,
             Platform::ConnectionWrappers::MilestoneMembersByPriority,
             Platform::ConnectionWrappers::DriftwoodQuery,
             Platform::ConnectionWrappers::StarredRepositories,
             Platform::ConnectionWrappers::TeamRepositories
          object.total_count
        when Platform::ConnectionWrappers::SearchQuery
          object.search_results.total
        when Platform::ConnectionWrappers::RemoteRelation,
             Platform::ArrayWrapper
          object.count
        when Platform::ConnectionWrappers::Newsies
          object.relation.count
        when Platform::ConnectionWrappers::CommitHistory
          repo = object.repository
          commit = object.parent

          object.git_args.then { |args|
            if args.empty?
              0
            else
              # We can ignore `max` and `skip` here, because we're not getting
              # a subset, but the total number of commits given the arguments
              repo.rpc.count_revision_history(commit.oid, args.except(:max, :skip))
            end
          }
        else
          raise Platform::Errors::Type, "Unknown type #{object.class}, can't create count."
        end
      end
    end
  end
end
