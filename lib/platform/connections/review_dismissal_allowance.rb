# frozen_string_literal: true

module Platform
  module Connections
    class ReviewDismissalAllowance < Connections::Base
      total_count_field
    end
  end
end
