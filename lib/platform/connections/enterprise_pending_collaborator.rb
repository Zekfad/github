# frozen_string_literal: true

module Platform
  module Connections
    class EnterprisePendingCollaborator < Connections::Base
      edge_type Edges::EnterprisePendingCollaborator

      total_count_field
    end
  end
end
