# frozen_string_literal: true

module Platform
  module Connections
    class Mannequin < Connections::Base
      areas_of_responsibility :migration
      description "A list of mannequins."
      edge_type(Edges::Mannequin)

      total_count_field
    end
  end
end
