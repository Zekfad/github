# frozen_string_literal: true

module Platform
  module Connections
    class FeatureHistoryEntry < Connections::Base
      description "A list of audit log entries for a feature."
      minimum_accepted_scopes ["devtools"]
      visibility :internal
      total_count_field
    end
  end
end
