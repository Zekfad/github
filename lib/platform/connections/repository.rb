# frozen_string_literal: true

module Platform
  module Connections
    class Repository < Connections::Base
      description "A list of repositories owned by the subject."

      total_count_field

      field :total_disk_usage, Integer, description: "The total size in kilobytes of all repositories in the connection.", null: false

      def total_disk_usage
        if @object.respond_to?(:relation) # Relation connection wrapper
          @object.relation.sum(:disk_usage)
        else # ArrayWrapper connection wrapper
          @object.nodes.sum(&:disk_usage)
        end
      end
    end
  end
end
