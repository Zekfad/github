# frozen_string_literal: true

module Platform
  module Connections
    class MarketplaceListingPlan < Connections::Base
      description "A list of the payment plans for this Marketplace listing."
      visibility :internal
      areas_of_responsibility :marketplace

      total_count_field
    end
  end
end
