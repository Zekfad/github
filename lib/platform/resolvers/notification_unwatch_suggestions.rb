# frozen_string_literal: true

module Platform
  module Resolvers
    class NotificationUnwatchSuggestions < Resolvers::Base
      include Helpers::Newsies

      type Connections.define(Objects::NotificationUnwatchSuggestion), null: false

      def resolve
        user = context[:viewer]
        return Promise.resolve(ArrayWrapper.new([])) unless user

        # Grab unwatch suggestions from munger - results are limited to 10.
        suggestions_hash = begin
          GitHub.munger.notifications_unsubscribe_suggestions(user)&.index_by(&:repository_id)
        rescue => e
          Failbot.report(e, user: user)
          nil
        end
        return Promise.resolve(ArrayWrapper.new([])) unless suggestions_hash.present?

        # The user could have unsubscribed from the list between when the munger suggestions were created
        # and when the user receives the suggestions. In order to make sure that we don't show suggestions for
        # out of date subscriptions we ensure that we only return suggestions where the user still has a subscription
        # to the repository.
        lists = suggestions_hash.keys[0..9].map do |repository_id|
          Newsies::List.new(Newsies::List.type_from_class(Repository), repository_id)
        end

        user_subscriptions_for_suggestions = handle_newsies_service_unavailable do
          Newsies::ListSubscription
            .for_user(user)
            .for_lists(lists)
            .excluding_ignored
        end
        return Promise.resolve(ArrayWrapper.new([])) if user_subscriptions_for_suggestions.empty?

        async_notification_unwatch_suggestions = user_subscriptions_for_suggestions.map do |subscription|
          suggestion = suggestions_hash[subscription.list_id]

          notification_unwatch_suggestion = Platform::Models::NotificationUnwatchSuggestion.new(
            snapshot_date: suggestion.snapshot_date,
            user_id: suggestion.user_id,
            repository_id: suggestion.repository_id,
            algorithm_version: suggestion.algorithm_version,
            score: suggestion.score
          )

          notification_unwatch_suggestion.async_readable_by?(user).then do |readable|
            next notification_unwatch_suggestion if readable
          end
        end

        Promise.all(async_notification_unwatch_suggestions).then do |suggestions|
          ArrayWrapper.new(suggestions.compact.sort_by(&:score).reverse)
        end
      end
    end
  end
end
