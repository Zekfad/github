# frozen_string_literal: true

module Platform
  module Resolvers
    class IssueComments < Resolvers::Base

      argument :since, Scalars::DateTime, "List issue comments since given date", visibility: :under_development, required: false

      type Connections.define(Objects::IssueComment), null: false

      def resolve(**arguments)
        case object
        when PullRequest
          object.async_issue.then do |issue|
            build_issue_relation(issue, arguments[:since])
          end
        when Issue
          build_issue_relation(object, arguments[:since])
        when User
          relation = object.issue_comments

          relation = context[:permission].filter_permissible_repository_resources(object, relation, resource: "issues")

          relation.filter_spam_for(context[:viewer]).joins(:repository).merge(Repository.filter_spam_and_disabled_for(context[:viewer]))
        end
      end
      private

      def build_issue_relation(issue, since)
        if context[:permission].typed_can_access?("Issue", issue)
          relation = issue.comments.filter_spam_for(context[:viewer]).joins(:repository).merge(Repository.filter_spam_and_disabled_for(context[:viewer]))

          relation = relation.since(since) if since

          relation
        else
          ::IssueComment.none
        end
      end
    end
  end
end
