# frozen_string_literal: true

module Platform
  module Resolvers
    class Timeline < Resolvers::Base

      argument :since, Scalars::DateTime, "Allows filtering timeline events by a `since` timestamp.", required: false

      class << self
        attr_accessor :union_type
      end

      def resolve(since: nil)
        # fetch timeline for the viewer, reject any non-known types to preserve response
        # and ensure we don't expose internal types

        # begin by batch-loading and caching objects that we're very likely to return.
        (object.is_a?(Issue) ? Promise.resolve(object) : object.async_issue).then do |issue|
          Promise.all([issue.async_comments, issue.async_composable_comments, issue.async_events]).then do
            known_types = self.class.union_type.possible_types.reject { |type|
              context[:target] != :internal && type.visibility == :internal
            }.map(&:graphql_name)
            timeline = Platform::LoaderTracker.ignore_association_loads {
              object.timeline_for(context[:viewer], since: since)
            }

            items = timeline.map { |item|
              if item.is_a?(Platform::Models::PullRequestCommit)
                item.commit.tap do |commit|
                  commit.repository = object.repository
                end
              elsif item.is_a?(::DeprecatedPullRequestReviewThread)
                if GitHub.flipper[:use_review_thread_ar_model].enabled?(item.repository)
                  item.activerecord_pull_request_review_thread
                else
                  Platform::Models::PullRequestReviewThread.from_review_comments(item.comments)
                end
              else
                item
              end
            }.select { |item|
              type_name = Platform::Helpers::NodeIdentification.type_name_from_object(item)
              known_type = known_types.include?(type_name)
              GitHub.dogstats.increment("platform.types.missing_issue_events.count", tags: ["event:#{type_name.downcase}"]) unless known_type

              known_type
            }.freeze # dup above and re-freeze

            ArrayWrapper.new(items)
          end
        end
      end
    end
  end
end
