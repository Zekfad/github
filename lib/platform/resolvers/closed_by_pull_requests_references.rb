# frozen_string_literal: true

module Platform
  module Resolvers
    class ClosedByPullRequestsReferences < Resolvers::Base
      type Connections.define(Objects::PullRequest), null: true

      argument :include_closed_prs, Boolean, "Include closed PRs in results", required: false, default_value: false
      argument :order_by_state, Boolean, "Return results ordered by state", required: false, default_value: false

      def resolve(include_closed_prs: false, order_by_state: false, **arguments)
        @object.async_closed_by_pull_requests_references_for(
          viewer: @context[:viewer],
          include_closed_prs: include_closed_prs,
          order_by_state: order_by_state,
          unauthorized_organization_ids: @context[:unauthorized_organization_ids],
        ).then do |accessible_pulls|
          ArrayWrapper.new(accessible_pulls.compact)
        end
      end
    end
  end
end
