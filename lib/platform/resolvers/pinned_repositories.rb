# frozen_string_literal: true

module Platform
  module Resolvers
    class PinnedRepositories < Platform::Resolvers::Repositories
      def fetch_repositories(owner, _, _, scope:, order_by:)
        # Owner can be RepositoryShowcase, User, Organization, etc
        owner.pinned_repositories.active.public_scope.merge(scope)
      end
    end
  end
end
