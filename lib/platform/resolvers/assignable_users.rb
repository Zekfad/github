# frozen_string_literal: true

module Platform
  module Resolvers
    class AssignableUsers < Platform::Resolvers::Users
      argument :query, String, "Filters users with query on user name and login", required: false

      def resolve(**arguments)
        context[:permission].async_can_get_full_repo?(object).then do |can_get_full_repo|
          if can_get_full_repo
            Promise.all([
              object.async_owner,
              object.async_parent,
              object.async_organization,
            ]).then do
              scope = filter_spam(object.available_assignees.order("login"))

              query = ActiveRecord::Base.sanitize_sql_like(
                arguments[:query].to_s.strip.downcase,
              )

              if scope && query.present?
                scope = scope.joins("LEFT JOIN profiles ON profiles.user_id = users.id")
                             .where(["users.login LIKE ? OR profiles.name LIKE ?", "%#{query}%", "%#{query}%"])
              end

              scope
            end
          else
            ArrayWrapper.new([])
          end
        end
      end
    end
  end
end
