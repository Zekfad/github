# frozen_string_literal: true

module Platform
  module Resolvers
    class LabelPullRequests < Resolvers::PullRequests
      filter_by_argument

      def fetch_pull_requests(label, _, _)
        scope = label.repository.pull_requests.labeled(label.id)

        context[:permission].async_can_list_pull_requests?(label.repository).then do |can_list_pull_requests|
          can_list_pull_requests ? scope : scope.joins(:repository).where(["repositories.public = true"])
        end

      end

      def query_components
        [[:label, object.name]]
      end

      def async_repository
        object.async_repository
      end
    end
  end
end
