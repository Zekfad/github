# frozen_string_literal: true

module Platform
  module Resolvers
    class Labels < Resolvers::Base
      type Connections.define(Objects::Label), null: true

      argument :order_by, Inputs::LabelOrder, "Ordering options for labels returned from the connection.", required: false,
        default_value: { field: "created_at", direction: "ASC" }

      def resolve(**arguments)
        return ::Label.none unless has_permission?(object, context)
        # This arg is added by one of the fields, this will probably be broken
        query = arguments[:query]

        labels = if query.present? && object.is_a?(Repository)
          search_labels(query, repository: object)
        else
          list_labels(object).order("#{arguments[:order_by][:field]} #{arguments[:order_by][:direction]}")
        end

        labels
      end

      private

      def has_permission?(owner, context)
        case owner
        when Repository
          context[:permission].async_can_list_repo_labels?(owner).sync
        when PullRequest
          context[:permission].typed_can_access?("PullRequest", owner)
        when Issue
          context[:permission].typed_can_access?("Issue", owner)
        end
      end

      def list_labels(owner)
        case owner
        when Repository
          owner.labels.scoped
        when PullRequest
          owner.async_issue.then do |issue|
            issue.labels.scoped
          end.sync
        when Issue
          owner.labels.scoped
        end
      end

      def search_labels(phrase, repository:)
        Search::Queries::LabelQuery.new(phrase: phrase, source_fields: false,
                                        repo_id: repository.id)
      end
    end
  end
end
