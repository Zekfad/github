# frozen_string_literal: true

module Platform
  module Resolvers
    class EnterpriseAdmins < Resolvers::Base

      type Connections::EnterpriseAdministrator, null: false

      argument :query, String, "The search string to look for.", required: false
      argument :role, Enums::EnterpriseAdministratorRole, "The role to filter by.", required: false
      argument :order_by, Inputs::EnterpriseMemberOrder,
        "Ordering options for administrators returned from the connection.",
        required: false, default_value: { field: "login", direction: "ASC" }

      def resolve(query: nil, order_by: nil, role: nil)
        object.admins(query: query,
                      order_by_field: order_by&.dig(:field),
                      order_by_direction: order_by&.dig(:direction),
                      role: role_from_enum_value(role),
        )
      end

      private

      def role_from_enum_value(role)
        return nil unless role.present?

        case role
        when Platform::Enums::EnterpriseAdministratorRole.values["OWNER"].value
          :owner
        when Platform::Enums::EnterpriseAdministratorRole.values["BILLING_MANAGER"].value
          :billing_manager
        else
          raise Platform::Errors::Internal, "Unexpected role: #{role}"
        end
      end
    end
  end
end
