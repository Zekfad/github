# frozen_string_literal: true

module Platform
  module Resolvers
    class ActorActor < Resolvers::Actor
      def async_actor
        object.async_actor
      end
    end
  end
end
