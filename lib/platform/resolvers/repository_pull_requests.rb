# frozen_string_literal: true

module Platform
  module Resolvers
    class RepositoryPullRequests < Resolvers::PullRequests

      filter_by_argument

      def fetch_pull_requests(repo, _, _)
        scope = repo.pull_requests.not_spammy

        context[:permission].async_can_list_pull_requests?(repo).then do |can_list_pull_requests|
          can_list_pull_requests ? scope : scope.joins(:repository).where(["repositories.public = true"])
        end

      end

      def query_components
        []
      end

      def async_repository
        Promise.resolve(object)
      end
    end
  end
end
