# frozen_string_literal: true

module Platform
  module Resolvers
    class PendingMarketplaceChanges < Resolvers::Base
      argument :listing_slug, String, "The slug of the Marketplace listing we want changes for.", required: false

      type Connections::PendingMarketplaceChange, null: false

      def resolve(**arguments)
        changes = object.pending_subscription_item_changes.non_free_trial
        changes = changes.joins_marketplace_listings.where("marketplace_listings.slug = ?", arguments[:listing_slug]) if arguments[:listing_slug]
        changes
      end
    end
  end
end
