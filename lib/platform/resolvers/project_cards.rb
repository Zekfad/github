# frozen_string_literal: true

module Platform
  module Resolvers
    class ProjectCards < Resolvers::Base
      extend Platform::Objects::Base::Field::ManualConnectionArguments

      MAX_PAGE_SIZE = 100

      def self.field_options
        # Defer the loading of `ProjectCard` to work around cyclical dependency
        super.merge(
          type: Connections.define(Objects::ProjectCard),
          null: false,
          # We're implementing connections by hand here for some reason
          connection: false,
        )
      end

      has_connection_arguments

      argument :archived_states, [Enums::ProjectCardArchivedState, null: true],
        required: false,
        description: "A list of archived states to filter the cards by",
        default_value: [:archived, :not_archived]

      def resolve(**arguments)
        Platform::ConnectionWrappers::ProjectCards.new(
          Platform::Helpers::ProjectCards.new(object, arguments, context),
          arguments,
          max_page_size: MAX_PAGE_SIZE,
          parent: object,
          context: context,
        )
      end
    end
  end
end
