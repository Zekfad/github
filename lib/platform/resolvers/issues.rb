# frozen_string_literal: true

module Platform
  module Resolvers
    class Issues < Resolvers::Base

      argument :order_by, Inputs::IssueOrder, "Ordering options for issues returned from the connection.", required: false
      argument :labels, [String], "A list of label names to filter the pull requests by.", required: false
      argument :states,  [Enums::IssueState], "A list of states to filter the issues by.", required: false
      argument :filter_by, Inputs::IssueFilters, "Filtering options for issues returned from the connection.", required: false

      type Connections.define(Objects::Issue), null: false

      def resolve(**arguments)
        case object
        when User
          scope = object.issues

          scope = context[:permission].filter_permissible_repository_resources(object, scope, resource: "issues")

          scope = apply_args(scope, arguments)

          # The state condition here helps MySQL pick the correct index.
          scope.without_pull_requests.filter_spam_for(context[:viewer]).where(state: ::Issue::States)
        when Label
          object.async_repository.then do |repo|
            scope = repo.issues.labeled(object)
            if (arguments[:filter_by] && arguments[:filter_by][:labels]) || arguments[:labels]
              # TODO: deprecate in favor of filter_by namespacing https://github.com/github/github/pull/95997#discussion_r210916544
              name = (arguments[:filter_by] && arguments[:filter_by][:labels]) ? arguments[:filter_by][:labels] : arguments[:labels]

              # This is slightly odd, but I promise it makes sense. If we hit this case, it means we have a query like:
              # query {
              #   repository(owner: "dotnet", name: "coreclr") {
              #     label(name: "area-CodeGen") {
              #       issues(labels: ["tenet-performance", "optimization"], first: 10) {
              #       ...
              # Which is functionally looking for issues that are _inheriting_ from a label, as well as filtering by
              #  _additional_ (and potentially different) Labels inside of that one Repository.
              # If you imagine the parent Label as 'Help Wanted' and the arguments to the connection as the labels
              # 'Front End' and 'Web', we're looking for Labels matching: 'Help Wanted' AND ('Front End' OR 'Web')
              #
              # In order to do this, we utilize the scope set above which is ALL issues tagged 'Help Wanted'
              # and add in a sub-select for all of the issues that are 'Front End' OR 'Web'
              #
              # In effect, this is an INTERSECT query between 'Help Wanted' and ('Front End' OR 'Web'), but done
              # without INTERSECT because it's not a MySQL feature.
              child_issues = repo.issues.joins(:labels).merge(Label.where(name: name)).select("issues.id")
              scope = scope.where("issues.id IN (#{child_issues.to_sql})")
            end

            if !context[:permission].can_list_issues?(object, repo)
              scope = scope.joins(:repository).merge(Repository.public_scope)
            end

            # Note that we skip the :labels argument here, because we've already applied it above
            # If there was no labels argument, it doesn't really matter that we're skipping it
            apply_args(scope, arguments, except: [:labels]).without_pull_requests.filter_spam_for(context[:viewer])
          end
        when Repository
          repository  = object
          scope       = object.issues

          repository.async_owner.then do |owner|
            if !context[:permission].can_list_issues?(owner, repository)
              scope = scope.joins(:repository).merge(Repository.public_scope)
            end

            scope = scope.excluding_issues_in_repos_with_issues_disabled
            scope = apply_args(scope, arguments)

            scope.without_pull_requests.filter_spam_for(context[:viewer]).distinct
          end
        when Milestone
          scope = object
                    .issues
                    .without_pull_requests
                    .joins("LEFT JOIN `issue_priorities` ON `issue_priorities`.`issue_id` = `issues`.`id`")
                    .order("issue_priorities.priority DESC, issues.id ASC")

          scope = apply_args(scope, arguments)

          scope = scope.filter_spam_for(context[:viewer])
          if arguments[:order_by].blank?
            # Use a custom connection wrapper here since it's sorted by a join table, `issue_priorities`
            scope = scope.select("issues.*, issue_priorities.priority as issue_priority")
            ConnectionWrappers::MilestoneMembersByPriority.new(scope)
          else
            scope
          end
        else
          raise Platform::Errors::Internal, "Unexpected issues owner #{object.class}"
        end
      end

      def apply_args(scope, arguments, except: [])
        if arguments[:order_by]
          scope = scope.reorder("issues.#{arguments[:order_by][:field]} #{arguments[:order_by][:direction]}")
        end

        # TODO: deprecate in favor of filter_by namespacing https://github.com/github/github/pull/95997#discussion_r210916544
        if arguments[:states]
          scope = scope.where(state: arguments[:states])
        end

        if arguments[:labels] && !except.include?(:labels)
          scope = scope.joins(:labels).merge(Label.where(name: arguments[:labels]))
        end

        if arguments[:filter_by]
          if arguments[:filter_by][:states]
            scope = scope.where(state: arguments[:filter_by][:states])
          end

          if arguments[:filter_by][:labels] && !except.include?(:labels)
            scope = scope.joins(:labels).merge(Label.where(name: arguments[:filter_by][:labels]))
          end

          if arguments[:filter_by][:since]
            scope = scope.since(arguments[:filter_by][:since])
          end

          if arguments[:filter_by].key?(:assignee)
            case arguments[:filter_by][:assignee]
            when Search::Filter::WILDCARD
              scope = scope.assigned
            when nil
              scope = scope.unassigned
            else
              scope = scope.assigned_to(arguments[:filter_by][:assignee])
            end
          end

          if arguments[:filter_by][:mentioned]
            user = User.find_by_login(arguments[:filter_by][:mentioned])
            scope = scope.mentioning(user)
          end

          if arguments[:filter_by][:created_by]
            scope = scope.created_by(arguments[:filter_by][:created_by])
          end

          if arguments[:filter_by].key?(:milestone)
            case arguments[:filter_by][:milestone]
            when Search::Filter::WILDCARD
              scope = scope.with_any_milestone
            when nil
              scope = scope.with_no_milestone
            else
              scope = scope.for_milestone(arguments[:filter_by][:milestone])
            end
          end

          if arguments[:filter_by][:viewer_subscribed]
            repo_ids = scope.pluck(:repository_id)
            lists = repo_ids.map { |id| ::Newsies::List.new(Repository.name, id) }
            threads_response = GitHub.newsies.subscribed_threads(context[:viewer], lists, "Issue")

            if threads_response.failed?
              raise Platform::Errors::ServiceUnavailable, "Notifications are not available"
            end

            scope = scope.from_ids(threads_response.map(&:thread_id))
          end
        end

        scope

      rescue Issue::AssigneeInvalid => e
        raise Platform::Errors::Unprocessable, "Could not find an assignee with the login '#{e.assignee}'."
      end
    end
  end
end
