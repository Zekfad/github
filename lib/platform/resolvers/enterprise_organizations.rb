# frozen_string_literal: true

module Platform
  module Resolvers
    class EnterpriseOrganizations < Resolvers::Base
      type Connections.define(Objects::Organization), null: false
      argument :query, String, "The search string to look for.", required: false
      argument :order_by, Inputs::OrganizationOrder,
        "Ordering options for organizations returned from the connection.",
        required: false, default_value: { field: "login", direction: "ASC" }

      def resolve(query: nil, order_by: nil)
        object.filtered_organizations \
          query: query,
          order_by_field: order_by&.dig(:field) || "login",
          order_by_direction: order_by&.dig(:direction) || "ASC"
      end
    end
  end
end
