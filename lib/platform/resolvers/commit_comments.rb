# frozen_string_literal: true

module Platform
  module Resolvers
    class CommitComments < Resolvers::Base
      type Connections::CommitComment, null: false

      def resolve
        commit_comment_promise = case object
        when Repository
          context[:permission].async_can_list_commit_comments?(object).then do |can_list_commit_comments|
            if can_list_commit_comments
              ::CommitComment.where(repository_id: object.id).filter_spam_for(context[:viewer])
            else
              ::CommitComment.none
            end
          end
        when CommitCommentThread
          visible_comments = object.comments.reject do |comment|
            comment.hide_from_user?(context[:viewer])
          end
          Promise.resolve(ArrayWrapper.new(visible_comments))
        when User
          relation = object.commit_comments

          relation = context[:permission].filter_permissible_repository_resources(object, relation, resource: "contents")

          Promise.resolve(relation.filter_spam_for(context[:viewer]))
        else
          relation = ::CommitComment.none

          if object.respond_to?(:repository)
            context[:permission].async_can_list_commit_comments?(object.repository).then do |can_list_commit_comments|
              if can_list_commit_comments
                relation = ::CommitComment.where({
                  repository_id: object.repository.id,
                  commit_id: object.oid,
                }).filter_spam_for(context[:viewer])
              else
                relation
              end
            end
          else
            Promise.resolve(relation)
          end
        end

        commit_comment_promise.then { |commit_comment| commit_comment }
      end
    end
  end
end
