# frozen_string_literal: true

module Platform
  module Errors
    class ResourceMissing < StandardError; end
  end
end
