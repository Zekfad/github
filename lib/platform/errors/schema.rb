# frozen_string_literal: true

module Platform
  module Errors
    # Schema definition error
    class Schema < StandardError; end
  end
end
