# frozen_string_literal: true

module Platform
  module Errors
    class Parse < StandardError; end
  end
end
