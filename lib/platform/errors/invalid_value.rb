# frozen_string_literal: true

module Platform
  module Errors
    class InvalidValue < Errors::Internal
    end
  end
end
