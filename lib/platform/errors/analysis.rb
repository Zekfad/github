# frozen_string_literal: true

module Platform
  module Errors
    class Analysis < GraphQL::AnalysisError
      attr_reader :type

      def initialize(type, *args, **options)
        @type = type.upcase
        super(*args, **options)
      end

      def to_h
        super.merge({
          "type" => type,
        })
      end
    end
  end
end
