# frozen_string_literal: true

module Platform
  module Errors
    class NotImplemented < Internal; end
  end
end
