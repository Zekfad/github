# frozen_string_literal: true

module Platform
  module Mutations
    class CancelSubscriptionItem < Platform::Mutations::Base
      description "Cancel a given subscription item for an account."

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :id, ID, "The ID of the subscription item to cancel.", required: true, loads: Objects::SubscriptionItem, as: :item
      argument :immediately_cancel, Boolean, "Whether to immediately cancel or schedule pending downgrade", required: true

      field :subscription_item, Objects::SubscriptionItem, "The cancelled subscription item.", null: true
      field :subscription, Objects::Subscription, "The subscription tied to the cancelled item's account.", null: true
      field :on_free_trial, Boolean, "The cancelled subscription item's free trial state, before cancellation.", null: true

      def resolve(item:, immediately_cancel:)
        raise Errors::Unprocessable.new("This item has already been cancelled.") if item.cancelled?
        unless actor_has_permission?(item)
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to cancel this subscription.")
        end


        on_free_trial = item.on_free_trial?
        item.cancel!(actor: context[:viewer], force: cancel_immediately?(immediately_cancel))

        {
          subscription_item: item,
          subscription: item.account.plan_subscription,
          on_free_trial: on_free_trial,
        }
      end

      private

      # Internal: whether to cancel immediately or schedule for the next billing date
      #
      # Staff (from stafftools) and feature-flagged users cancel immediately rather than
      # deferring to the end of the billing period. If a staff user cancels through the
      # public dotcom UI, it is scheduled just as it would be for a non-staff user.
      #
      # Returns Boolean
      def cancel_immediately?(immediately_cancel)
        GitHub.flipper[:immediate_marketplace_cancellation].enabled?(context[:viewer]) ||
          (context[:viewer].site_admin? && immediately_cancel)
      end

      # Internal: Does the current actor have permission to cancel
      #
      # Returns Boolean
      def actor_has_permission?(item)
        context[:viewer].site_admin? || item.adminable_by?(context[:viewer])
      end
    end
  end
end
