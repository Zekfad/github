# frozen_string_literal: true

module Platform
  module Mutations
    class SuspendAccounts < Platform::Mutations::Base
      description "Suspend a set of accounts."

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :account_ids, [ID], "The global relay id of accounts to suspend.", required: true, loads: Platform::Unions::Account
      argument :reason, String, "Reason for suspending accounts.", required: true
      argument :origin, String, "The system that performed this classification.", required: false, default_value: "origin_unknown"

      field :accounts, [Unions::Account], "The suspended accounts.", null: true

      def resolve(accounts:, **inputs)
        unless self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to suspend accounts."
        end

        accounts.each do |account|
          account.suspend(inputs[:reason], actor: context[:viewer], origin: inputs[:origin])
        end

        { accounts: accounts }
      end
    end
  end
end
