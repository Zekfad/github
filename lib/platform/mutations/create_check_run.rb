# frozen_string_literal: true

module Platform
  module Mutations
    class CreateCheckRun < Platform::Mutations::Base
      include Platform::Helpers::CommitValidation

      description "Create a check run."

      minimum_accepted_scopes ["public_repo"]

      limit_actors_to [:github_app]

      argument :repository_id, ID, "The node ID of the repository.", required: true, loads: Objects::Repository, as: :repo
      argument :name, String, "The name of the check.", required: true
      argument :display_name, String, "Optional name that, if defined, will be used in the UI instead of the name.", required: false, visibility: :internal
      argument :head_sha, Scalars::GitObjectID, "The SHA of the head commit.", required: true

      argument :check_suite_id, ID, "The node ID of the CheckSuite to create this under.", visibility: :internal, required: false, loads: Objects::CheckSuite, as: :check_suite
      argument :details_url, Scalars::URI, "The URL of the integrator's site that has the full details of the check.", required: false
      argument :external_id, String, "A reference for the run on the integrator's system.", required: false
      argument :status, Enums::RequestableCheckStatusState, "The current status.", required: false
      argument :started_at, Scalars::DateTime, "The time that the check run began.", required: false
      argument :conclusion, Enums::CheckConclusionState, "The final conclusion of the check.", required: false
      argument :completed_at, Scalars::DateTime, "The time that the check run finished.", required: false
      argument :output, Inputs::CheckRunOutput, "Descriptive details about the run.", required: false
      argument :actions, [Inputs::CheckRunAction], "Possible further actions the integrator can perform, which a user may trigger.", required: false
      argument :steps, [Inputs::CheckStepData], "Steps this check run will execute", visibility: :internal, required: false
      argument :completed_log, Inputs::CompletedLogData, "The completed log information", visibility: :internal, required: false
      argument :number, Int, "The topological order of the check run within the check suite.", visibility: :internal, required: false
      argument :streaming_log, Inputs::StreamingLogData, "Streaming log information", visibility: :internal, required: false

      error_fields

      field :check_run, Objects::CheckRun, "The newly created check run.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repo:, **inputs)
        permission.async_owner_if_org(repo).then do |org|
          permission.access_allowed?(:write_check_run, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(repo:, execution_errors:, **inputs)
        if inputs[:output] && inputs[:output][:annotations] && inputs[:output][:annotations].size > Inputs::CheckAnnotationData::MAXIMUM_PER_REQUEST
          return {
            check_run: nil,
            errors: [{
              "path" => ["input", "output", "annotations"],
              "message" => "Annotations exceeds a maximum quantity of #{Inputs::CheckAnnotationData::MAXIMUM_PER_REQUEST}",
            }],
          }
        end

        check_run_data = {}

        head_sha = inputs[:head_sha]

        find_commit!(repo, head_sha)
        check_suite = inputs[:check_suite] || fetch_check_suite(head_sha: head_sha, repo: repo)
        if check_suite.head_sha != head_sha || check_suite.repository != repo
          raise Platform::Errors::Unprocessable.new("The Check Suite does not match the SHA and Repository provided")
        end

        if inputs[:conclusion] == "stale"
          raise Platform::Errors::Validation.new("You cannot set a check run with `conclusion: stale`. `stale` is for internal use only.")
        end

        check_run_data[:check_suite_id] = check_suite.id

        check_run_data[:started_at] = (inputs[:started_at] || Time.now)

        check_run_data[:status] = inputs[:status]
        check_run_data[:conclusion] = inputs[:conclusion]
        check_run_data[:status] = "completed" if inputs[:conclusion]

        check_run_data[:details_url] = inputs[:details_url].to_s if inputs[:details_url].present?
        check_run_data[:completed_at] = inputs[:completed_at]
        check_run_data[:external_id] = inputs[:external_id]
        check_run_data[:name] = inputs[:name]
        check_run_data[:display_name] = inputs[:display_name]

        image_data = []
        annotation_data = []
        if inputs[:output].present?
          inputs[:output].each do |output_key, output_value|
            if output_key == :images
              next unless inputs[:output][:images]
              inputs[:output][:images].each do |image_hash|
                image_hash = image_hash.to_h
                # need to force conversion from `Addressable::URI`
                image_hash[:image_url] = image_hash[:image_url].to_s
                image_data << image_hash.to_h
              end
            elsif output_key == :annotations
              next unless inputs[:output][:annotations]
              annotation_data = inputs[:output][:annotations].map do |annotation|
                Inputs::CheckAnnotationData.hash_from_input(annotation)
              end
            else
              check_run_data[output_key] = output_value
            end
          end
        end

        truncator = CheckRun::Truncator.new(check_run_data[:text])
        check_run_data[:text] = truncator.truncate

        check_run = CheckRun.new(check_run_data)
        check_run.images = image_data
        check_run.annotations.build(annotation_data)

        actions = inputs[:actions]&.map do |action|
          CheckRunAction.new(action[:label], action[:identifier], action[:description])
        end

        check_run.actions = (actions || [])

        if inputs[:steps]
          inputs[:steps].each do |input_step|
            input_step_attributes = input_step.to_h

            if completed_log = input_step_attributes[:completed_log]
              input_step_attributes[:completed_log_url] = completed_log[:url]
              input_step_attributes[:completed_log_lines] = completed_log[:lines]
              input_step_attributes.except!(:completed_log)
            end

            check_run.steps.build(input_step_attributes)
          end
        end

        if inputs[:completed_log].present?
          check_run.completed_log_url = inputs[:completed_log][:url]
          check_run.completed_log_lines = inputs[:completed_log][:lines]
        end
        check_run.number = inputs[:number] if inputs[:number].present?
        check_run.streaming_log_url = inputs[:streaming_log][:url] if inputs[:streaming_log]

        if check_run.save
          if truncator.truncated?
            truncator.report_silent_truncation!(
              check_run: check_run,
              repo: repo,
              integration: context[:integration],
              type: "an existing",
            )
          end
          { check_run: check_run, errors: [] }
        else
          if truncator.truncated?
            truncator.report_silent_truncation!(
              check_run: check_run,
              repo: repo,
              integration: context[:integration],
              type: "an existing",
            )
          end
          Platform::UserErrors.append_legacy_mutation_model_errors_to_context(check_run, execution_errors)
          check_run_translate_hash = {
                                       summary: ["output", "summary"],
                                       annotations: ["output", "annotations"],
                                     }
          check_run_action_path_prefix = ["input", "actions"]
          { check_run: nil, errors: Platform::UserErrors.mutation_errors_for_model(check_run, translate: check_run_translate_hash) + Platform::UserErrors.mutation_errors_for_models(check_run.actions, path_prefix: check_run_action_path_prefix) }
        end
      end

      private

      def fetch_check_suite(head_sha:, repo:)
        check_suite = find_check_suite(repo, head_sha, context[:integration])
        return check_suite if check_suite.present?

        check_suite = create_check_suite(repo, head_sha, context[:integration])
        return check_suite if check_suite.valid?

        raise Platform::Errors::Unprocessable.new(check_suite.errors.full_messages.join(", "))
      end

      def find_check_suite(repo, head_sha, integration)
        attributes = { head_sha: head_sha, github_app_id: integration.id }
        repo.check_suites.find_by(attributes)
      end

      def create_check_suite(repo, head_sha, integration)
        attributes = { head_sha: head_sha, github_app_id: integration.id }
        push = Push.where(after: head_sha, repository_id: repo.id).first
        attributes = attributes.merge(push_id: push&.id)
        repo.check_suites.create(attributes)
      end
    end
  end
end
