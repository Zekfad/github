# frozen_string_literal: true

module Platform
  module Mutations
    class EnableFeature < Platform::Mutations::Base
      description "Enables a feature flag."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :id, ID, "The id of the feature flag.", required: true, loads: Objects::Feature, as: :feature

      field :feature, Objects::Feature, "The feature being enabled.", null: true

      def resolve(feature:, **inputs)

        if feature && feature.enable
          { feature: feature }
        else
          raise Errors::Unprocessable.new("Could not enable feature.")
        end
      end
    end
  end
end
