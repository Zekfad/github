# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateTeam < Platform::Mutations::Base
      description "Updates an existing team."
      visibility :internal

      minimum_accepted_scopes ["write:org"]

      argument :team_id, ID, "The Team ID to update.", required: true, loads: Objects::Team
      argument :name, String, "The name of team.", required: false
      argument :description, String, "The description of team.", required: false
      argument :privacy, Enums::TeamPrivacy, "The level of privacy the team has.", required: false
      argument :parent_team_id, ID, "The parent team ID.", required: false, loads: Objects::Team
      argument :ldap_dn, String, "String for LDAP distinguished name.", required: false, camelize: false
      argument :permission, Enums::LegacyTeamPermission, Enums::LegacyTeamPermission.description, visibility: :internal, required: false
      argument :group_mappings, [Inputs::GroupMapping, null: true], "A list of external groups to map the team to.", required: false, visibility: :under_development

      field :team, Objects::Team, "The updated team.", null: true

      def resolve(team:, **inputs)
        actor = context[:actor]
        viewer = context[:viewer]

        unless team.updatable_by?(actor)
          raise Errors::Forbidden.new("#{viewer} does not have permission to update this team.")
        end

        # Attributes updated if necessary
        attributes = {}
        attributes[:name] = inputs[:name] if inputs[:name]
        attributes[:description] = inputs[:description] if inputs[:description]
        attributes[:ldap_dn] = inputs[:ldap_dn] if inputs[:ldap_dn]
        attributes[:updater] = actor
        attributes[:permission] = inputs[:permission].downcase if inputs[:permission]
        attributes[:privacy] = inputs[:privacy] if inputs[:privacy]
        requests_to_cancel = []

        group_mappings = Array(inputs[:group_mappings]).map(&:to_h) if inputs[:group_mappings]

        # Includes some update to the parent team
        if inputs.key?(:parent_team)
          new_parent_team = inputs[:parent_team]
          if new_parent_team
            identical_requests = TeamChangeParentRequest.identical_to(
              new_parent_team.id,
              team.id,
            )
            if new_parent_team.id == team.parent_team_id || new_parent_team.updatable_by?(actor)
              attributes[:parent_team_id] = new_parent_team.id
              requests_to_cancel = identical_requests.pending
            else
              if identical_requests.empty?
                GitHub::Logger.log(
                  msg: "TeamChangeParentRequest required",
                  team_id: team.id,
                  parent_team_id: new_parent_team.id,
                  actor_id: actor.id,
                )
                create_team_change_parent_request_initiated_by_child!(new_parent_team, team, actor)
              end

              # Here we assume that if the TeamChangeParentRequest was approved
              # once, it should be considered approved now. So update
              # this_team's parent, setting it as the new_parent_team.
              if identical_requests.any?(&:approved_at)
                GitHub::Logger.log(
                  msg: "new parent_team has already been approved",
                  team_id: team.id,
                  parent_team_id: new_parent_team.id,
                  actor_id: actor.id,
                )
                attributes[:parent_team_id] = new_parent_team.id
              end
            end
          else
            GitHub::Logger.log(
              msg: "setting parent_team_id to nil",
              team_id: team.id,
              actor_id: actor.id,
            )
            attributes[:parent_team_id] = nil
          end
        end

        # Includes some update to the privacy
        attributes[:privacy] = inputs[:privacy] if inputs[:privacy]

        if attributes[:privacy] == "secret"
          requests_to_cancel += TeamChangeParentRequest.involving(team).pending
        end

        Team::Editor.update_team(team, group_mappings: group_mappings, **attributes)

        if team.errors.empty?
          Array(requests_to_cancel).uniq.each do |request|
            request.cancel(actor: actor)
          end

          { team: team }
        else
          raise Errors::Unprocessable.new(team.errors.full_messages.join(", "))
        end
      end

      def create_team_change_parent_request_initiated_by_child!(parent_team, child_team, requester)
        request = ::TeamChangeParentRequest.create_initiated_by_child!(
          parent_team: parent_team,
          child_team: child_team,
          requester: requester,
        )
      rescue ActiveRecord::RecordInvalid => e
        raise Errors::Validation.new("There was an issue with your request")
      end
    end
  end
end
