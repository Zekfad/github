# frozen_string_literal: true

module Platform
  module Mutations
    class UpdatePendingPlanChange < Platform::Mutations::Base
      description "Update a pending plan change for an account."

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :login, String, "The login of the account you want to apply changes to.", required: true
      argument :cancel_data_packs, Boolean, "Do you want to cancel any pending data pack changes?.", required: false
      argument :cancel_plan, Boolean, "Do you want to cancel any pending plan changes?.", required: false
      argument :cancel_plan_duration, Boolean, "Do you want to cancel any pending billing duration changes?.", required: false
      argument :cancel_seats, Boolean, "Do you want to cancel any pending changes to the seat count?.", required: false

      field :pending_cycle, Objects::PendingCycle, "The pending changes for the next billing cycle.", null: true

      def resolve(**inputs)
        account = Loaders::ActiveRecord.load(::User, inputs[:login], column: :login).sync

        raise Errors::Unprocessable.new("There are no pending changes to update.") unless account.async_pending_cycle_change.sync
        if Billing::PlanTrial.exists?(pending_plan_change: account.pending_cycle_change)
          raise Errors::Unprocessable.new("Changes revert plans at the end of a trial cannot be modified.")
        end

        unless context[:viewer]&.site_admin? || account.adminable_by?(context[:viewer]) || (account.organization? && account.billing_manager?(context[:viewer]))
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to manage this account.")
        end

        account.pending_cycle_change.update_attribute(:data_packs, nil) if inputs[:cancel_data_packs]
        account.pending_cycle_change.update_attribute(:plan, nil) if inputs[:cancel_plan]
        account.pending_cycle_change.update_attribute(:plan_duration, nil) if inputs[:cancel_plan_duration]
        account.pending_cycle_change.update_attribute(:seats, nil) if inputs[:cancel_seats]

        { pending_cycle: account.pending_cycle }
      end
    end
  end
end
