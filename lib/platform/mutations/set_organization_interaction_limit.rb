# frozen_string_literal: true

module Platform
  module Mutations
    class SetOrganizationInteractionLimit < Platform::Mutations::Base
      description "Set an organization level interaction limit for an organization's public repositories."

      visibility :under_development, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      minimum_accepted_scopes ["admin:org"]

      argument :organization_id, ID, "The ID of the organization to set a limit for.", required: true, loads: Objects::Organization, as: :org
      argument :limit, Enums::RepositoryInteractionLimit, "The limit to set.", required: true
      argument :expiry, Enums::RepositoryInteractionLimitExpiry, "When this limit should expire.", visibility: :internal, required: false
      argument :is_staff_actor, Boolean, "Indicates if the actor should be guarded.", visibility: :internal, default_value: false, required: false

      field :organization, Objects::Organization, "The organization that the interaction limit was set for.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, org:, **inputs)
        permission.access_allowed?(:set_organization_interaction_limits,
          resource: org, current_org: org, current_repo: nil,
          allow_integrations: true, allow_user_via_integration: true)
      end

      def resolve(org:, **inputs)
        unless org.can_set_interaction_limits?(context[:viewer]) || context[:actor].can_have_granular_permissions?
          if context[:viewer].site_admin?
            # Require the site_admin scope for a site admin if this is being called from the API.
            if Platform.requires_scope?(context[:origin]) && !context[:permission].has_scope?("site_admin")
              raise Errors::Forbidden.new "You must use a token with the `site_admin` scope to perform site admin actions."
            end
          else
            raise Errors::Forbidden.new "You are not authorized to set interaction limits for this organization."
          end
        end

        ability = RepositoryInteractionAbility.new(:organization, org)

        if inputs[:expiry].present?
          raise Errors::Forbidden.new "Only site admins can specify an expiry." unless context[:viewer].site_admin?

          expiry = inputs[:expiry]
        else
          expiry = :one_day
        end

        ability.set_ability(inputs[:limit], context[:viewer], expiry, staff_actor: inputs[:is_staff_actor])

        { organization: org }
      end
    end
  end
end
