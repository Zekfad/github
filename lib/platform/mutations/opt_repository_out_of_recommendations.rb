# frozen_string_literal: true

module Platform
  module Mutations
    class OptRepositoryOutOfRecommendations < Platform::Mutations::Base
      description "Blocks a repository from being recommended to other users."
      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      argument :repository_id, ID, "The Node ID of the repository.", required: true, loads: Objects::Repository

      field :repository, Objects::Repository, "The repository that was opted out.", null: true

      def resolve(repository:, **inputs)
        unless context[:viewer].site_admin?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to opt " +
                                         "repositories out of being recommended.")
        end

        recommendation = RepositoryRecommendation.new(repository: repository)
        recommendation.opt_out(actor: context[:viewer])

        { repository: repository }
      end
    end
  end
end
