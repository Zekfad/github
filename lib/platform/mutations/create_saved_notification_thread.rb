# frozen_string_literal: true

module Platform
  module Mutations
    class CreateSavedNotificationThread < Platform::Mutations::Base
      description "Adds a saved notification thread."
      feature_flag :pe_mobile
      minimum_accepted_scopes ["notifications"]

      argument :id, ID, "The notification thread's id.", required: true, loads: Objects::NotificationThread, as: :notification_thread

      field :success, Boolean, "Did the operation succeed?", null: true
      field :viewer, Objects::User, "The user that the saved notification belongs to.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, notification_thread:, **inputs)
        notification_thread.async_thread.then do |thread|
          permission.access_allowed?(:create_saved_notification_thread, resource: thread, current_repo: nil, current_org: nil, allow_integrations: false, allow_user_via_integration: false)
        end
      end

      def resolve(notification_thread:, **inputs)
        notification_thread.async_thread.then do |thread|
          response = GitHub.newsies.web.save_thread(context[:viewer], thread)

          raise Errors::Unprocessable.new("Unable to save notification thread.") unless response.success?

          {
            success: true,
            viewer: context[:viewer],
          }
        end
      end
    end
  end
end
