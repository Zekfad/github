# frozen_string_literal: true

module Platform
  module Mutations
    class OptRepositoryIntoRecommendations < Platform::Mutations::Base
      description <<~DESCRIPTION
        Allows a repository to be recommended to other users, provided it is neither spammy nor
        private.
      DESCRIPTION
      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      argument :repository_id, ID, "The Node ID of the repository.", required: true, loads: Objects::Repository

      field :repository, Objects::Repository, "The repository that was opted in.", null: true

      def resolve(repository:, **inputs)
        unless context[:viewer].site_admin?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to opt " +
                                         "repositories into being recommended.")
        end
        recommendation = RepositoryRecommendation.new(repository: repository)

        if recommendation.opt_in(actor: context[:viewer])
          { repository: repository }
        else
          raise Errors::Unprocessable.new("Could not opt the repository into recommendations at " +
                                          "this time.")
        end
      end
    end
  end
end
