# frozen_string_literal: true

module Platform
  module Mutations
    class LockLockable < Platform::Mutations::Base
      description "Lock a lockable object"

      minimum_accepted_scopes ["public_repo"]

      argument :lockable_id, ID, "ID of the issue or pull request to be locked.", required: true, loads: Interfaces::Lockable
      argument :lock_reason, Enums::LockReason, "A reason for why the issue or pull request will be locked.", required: false

      field :locked_record, Interfaces::Lockable, "The item that was locked.", null: true
      field :actor, Interfaces::Actor, "Identifies the actor who performed the event.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, lockable:, **inputs)
        if lockable.is_a?(PullRequest)
          permission.async_repo_and_org_owner(lockable.issue).then do |repo, org|
            permission.access_allowed?(:lock_issue, repo: repo, resource: lockable.issue, current_org: org, allow_integrations: true, allow_user_via_integration: true)
          end
        else
          permission.async_repo_and_org_owner(lockable).then do |repo, org|
            permission.access_allowed?(:lock_issue, repo: repo, resource: lockable, current_org: org, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      def resolve(lockable:, **inputs)
        record = lockable
        lockable_type = record.is_a?(PullRequest) ? "pull request" : "issue"
        issue = record.is_a?(PullRequest) ? record.issue : record
        reason = inputs[:lock_reason]
        viewer = context[:viewer]

        issue.async_repository.then do |repository|
          if record.is_a?(Issue) && !repository.has_issues?
            raise Errors::Unprocessable::IssuesDisabled.new
          end

          if repository.locked_on_migration?
            raise Errors::Unprocessable::RepositoryMigration.new
          end

          if repository.archived?
            raise Errors::Unprocessable::RepositoryArchived.new
          end

          issue.async_lockable_by?(viewer).then do |can_lock|
            unless can_lock
              raise Errors::Forbidden.new("#{viewer} cannot lock that #{lockable_type}.")
            end

            success = if issue.locked?
              # the issue is already locked so we succeeded in keeping it locked!
              true
            else
              issue.lock(viewer, reason)
            end

            if success
              {
                locked_record: record,
                actor: context[:viewer],
              }
            else
              raise Errors::Unprocessable.new("Could not lock the #{lockable_type}.")
            end
          end
        end
      end
    end
  end
end
