# frozen_string_literal: true

module Platform
  module Mutations
    class DependencyGraphReassignPackage < Platform::Mutations::Base
      description "Reassigns a dependency graph package to a new GitHub repository"

      areas_of_responsibility :dependency_graph

      minimum_accepted_scopes ["site_admin"]

      visibility :internal

      argument :package_manager, String, "The name of the package manager.", required: true
      argument :package_name, String, "The name of the package.", required: true
      argument :target_repository, String, "The target repository.", required: true

      field :client_mutation_id, ID, "The client mutation ID", null: true

      def resolve(package_manager:, package_name:, target_repository:)
        target = Repository.with_name_with_owner(target_repository)
        raise Errors::Validation.new("Invalid target repository #{target_repository}") unless target

        mutation = DependencyGraph::ReassignPackageMutation.new(
          package_manager: package_manager,
          package_name: package_name,
          repository_id: target.id,
        )

        mutation.execute
          .map { |result| {} }
          .value { |error|
            raise GraphQL::ExecutionError.new(error.message) # rubocop:disable GitHub/UsePlatformErrors
          }
      end
    end
  end
end
