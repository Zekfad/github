# frozen_string_literal: true

module Platform
  module Mutations
    class SignMarketplaceAgreement < Platform::Mutations::Base
      description "Lets the current user agree to the specified GitHub Marketplace terms."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :listingID, ID, "The ID of the Marketplace listing for which the agreement is being signed.", required: true,
        as: :listing_id
      argument :agreementID, ID, "Sign the agreement with the given ID.", required: true,
        as: :agreement_id

      field :marketplace_listing, Objects::MarketplaceListing, "The Marketplace listing.", null: true

      def resolve(**inputs)
        listing = Helpers::NodeIdentification.
          typed_object_from_id([Objects::MarketplaceListing], inputs[:listing_id], context)
        agreement = Helpers::NodeIdentification.
          typed_object_from_id([Objects::MarketplaceAgreement], inputs[:agreement_id], context)

        if agreement.integrator?
          unless listing.can_sign_integrator_agreement?(context[:viewer], agreement: agreement)
            raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to sign the" +
                                           "GitHub #{agreement.name} for the specified listing.")
          end
        elsif agreement.end_user?
          unless listing.can_sign_end_user_agreement?(context[:viewer], agreement: agreement)
            raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to sign the" +
                                           "GitHub #{agreement.name} for the specified listing.")
          end
        end

        if listing.sign_agreement(context[:viewer], agreement: agreement)
          { marketplace_listing: listing }
        else
          raise Errors::Unprocessable.new("Could not sign the GitHub #{agreement.name}.")
        end
      end
    end
  end
end
