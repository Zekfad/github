# frozen_string_literal: true

module Platform
  module Mutations
    class UnsuspendAccounts < Platform::Mutations::Base
      description "Unsuspend a set of accounts."

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :account_ids, [ID], "The global relay id of accounts to unsuspend.", required: true, loads: Platform::Unions::Account
      argument :reason, String, "Reason for unsuspending accounts.", required: true
      argument :origin, String, "The system that performed this classification.", required: false, default_value: "origin_unknown"

      field :accounts, [Unions::Account], "The unsuspended accounts.", null: true

      def resolve(accounts:, **inputs)
        unless self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to unsuspend accounts."
        end

        accounts.each do |account|
          account.unsuspend(inputs[:reason], actor: context[:viewer], origin: inputs[:origin])
        end

        { accounts: accounts }
      end
    end
  end
end
