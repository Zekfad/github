# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateEnterpriseMembersCanInviteCollaboratorsSetting < Platform::Mutations::Base
      description "Sets whether members can invite collaborators are enabled for an enterprise."

      minimum_accepted_scopes ["admin:enterprise"]

      argument :enterprise_id, ID, "The ID of the enterprise on which to set the members can invite collaborators setting.", required: true, loads: Objects::Enterprise
      argument :setting_value, Enums::EnterpriseEnabledDisabledSettingValue, "The value for the members can invite collaborators setting on the enterprise.", required: true

      field :enterprise, Objects::Enterprise, "The enterprise with the updated members can invite collaborators setting.", null: true
      field :message, String, "A message confirming the result of updating the members can invite collaborators setting.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, enterprise:, **inputs)
        permission.access_allowed?(:administer_business, resource: enterprise, repo: nil, organization: nil, allow_integrations: false, allow_user_via_integration: false)
      end

      def resolve(enterprise:, **inputs)
        viewer = context[:viewer]

        unless enterprise.owner?(viewer)
          raise Errors::Forbidden.new("#{viewer} does not have permission to set the members can invite collaborators setting on this enterprise account.")
        end

        message = ""
        case inputs[:setting_value]
        when Platform::Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
          enterprise.allow_members_can_invite_outside_collaborators(actor: viewer, force: true)
          message = "Members can now invite outside collaborators."
        when Platform::Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
          enterprise.disallow_members_can_invite_outside_collaborators(actor: viewer, force: true)
          message = "Members can no longer invite outside collaborators."
        when Platform::Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
          enterprise.clear_members_can_invite_outside_collaborators(actor: viewer)
          message = "Organization administrators can now change this setting for individual organizations."
        end

        {
          enterprise: enterprise,
          message: message,
        }
      end
    end
  end
end
