# frozen_string_literal: true

module Platform
  module Mutations
    class MergeBranch < Platform::Mutations::Base
      include Helpers::GitRefs

      description "Merge a head into a branch."
      areas_of_responsibility :repositories
      minimum_accepted_scopes ["public_repo"]

      argument :repository_id, ID, "The Node ID of the Repository containing the base branch that will be modified.", required: true, loads: Objects::Repository
      argument :base , String, "The name of the base branch that the provided head will be merged into.", required: true
      argument :head, String, "The head to merge into the base branch. This can be a branch name or a commit GitObjectID.",  required: true
      argument :commit_message, String, "Message to use for the merge commit. If omitted, a default will be used.", required: false

      field :merge_commit, Objects::Commit, "The resulting merge Commit.", null: true

      def self.async_api_can_modify?(permission, repository:, **_)
        permission.async_owner_if_org(repository).then do |org|
          permission.access_allowed?(:merge_branch,
            resource: repository,
            current_repo: repository,
            current_org: org,
            allow_integrations: true,
            allow_user_via_integration: true,
          )
        end
      end

      def resolve(repository:, base:, head:, commit_message: nil)
        base_branch = repository.heads.find(base)
        raise Errors::NotFound.new("No such base.") unless base_branch

        prohibit_apps_modifying_blacklisted_files!(repository, base, head)
        merge_commit, error, error_message = base_branch.merge(context[:viewer], head, {
          commit_message: commit_message,
          reflog_data: build_reflog_hash(context: context, via: "MergeBranch mutation"),
        })
        if merge_commit
          { merge_commit: merge_commit }
        else
          raise Errors::Unprocessable.new("Failed to merge: #{error_message.inspect}")
        end
      end

      private

      # Required because we use in-band signaling for Actions, and we
      # need to protect against malicious apps merging files that result
      # in secret disclosure.
      # See https://github.com/github/github/pull/107074
      def prohibit_apps_modifying_blacklisted_files!(repo, base, head)
        org = context[:permission].async_owner_if_org(repo).sync
        diff = GitHub::Diff.new(repo, base, head)
        diff.summary.deltas.map do |delta|
          file = delta.new_file
          unless RefUpdatesPolicy.path_updateable_for_app?(repo, file.path, file.oid)
            allowed = false
            begin
              # Calling `access_allowed?` with a bot viewer and
              # `allow_integrations:false` raises Platform::Errors::Forbidden
              # rather than returning false as you might expect, which
              # explains the awkward exception-mediated control flow.
              allowed = context[:permission].access_allowed?(
                :write_file,
                resource: repo,
                current_repo: repo,
                current_org: org,
                path: file.path,
                allow_integrations: false,
                allow_user_via_integration: false,
              )
            rescue Platform::Errors::Forbidden
              allowed = false
            end
            raise Errors::Unauthorized::Write.new(file.path) unless allowed
          end
        end
      rescue GitRPC::InvalidFullOid
        raise Errors::Unprocessable.new("Failed to merge: \"Head does not exist\"")
      end
    end
  end
end
