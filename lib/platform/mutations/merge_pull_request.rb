# frozen_string_literal: true

module Platform
  module Mutations
    class MergePullRequest < Platform::Mutations::Base
      description "Merge a pull request."

      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :pull_requests

      argument :pull_request_id, ID, "ID of the pull request to be merged.", required: true, loads: Objects::PullRequest
      argument :commit_headline, String, "Commit headline to use for the merge commit; if omitted, a default message will be used.", required: false
      argument :commit_body, String, "Commit body to use for the merge commit; if omitted, a default message will be used", required: false
      argument :expected_head_oid, Scalars::GitObjectID, "OID that the pull request head ref must match to allow merge; if omitted, no check is performed.", required: false
      argument :merge_method, Enums::PullRequestMergeMethod, "The merge method to use. If omitted, defaults to 'MERGE'", required: false, default_value: :merge

      error_fields
      field :pull_request, Objects::PullRequest, "The pull request that was merged.", null: true
      field :actor, Interfaces::Actor, "Identifies the actor who performed the event.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        pull        = inputs[:pull_request]
        permission.async_repo_and_org_owner(pull).then do |repo, org|
          pull.async_issue.then do |issue|
            permission.access_allowed?(:merge_pull_request, repo: repo, resource: pull, current_org: org, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      def resolve(execution_errors:, **inputs)
        pull        = inputs[:pull_request]
        repository  = pull.repository

        context[:permission].authorize_content(:pull_request, :merge, repo: repository)

        if !context[:actor].can_have_granular_permissions? && !pull.async_viewer_can_update?(context[:viewer]).sync
          message = "#{context[:viewer]} does not have permission to update the pull request #{pull.global_relay_id}."
          raise Errors::Forbidden.new(message)
        end

        begin
          merged, status, failure = pull.merge(context[:viewer],
            message_title: inputs[:commit_headline],
            message: inputs[:commit_body],
            method: inputs[:merge_method] || :merge,
            expected_head: inputs[:expected_head_oid],
            reflog_data: {
              real_ip: context[:ip],
              repo_name: repository.name_with_owner,
              repo_public: repository.public?,
              user_login: context[:viewer].login,
              user_agent: context[:user_agent],
              pr_author_login: pull.safe_user.login,
              via: "pull request merge graphql mutation",
              from: GitHub.context[:from],
            },
          )
        rescue Git::Ref::HookFailed => e
          message = "Could not merge because a Git pre-receive hook failed.\n\n#{e.message}"
          raise Errors::Unprocessable.new(message)
        end

        if merged
          {
            pull_request: pull,
            actor: context[:viewer],
            errors: [],
          }
        else
          raise Errors::Unprocessable.new(status)
        end
      end
    end
  end
end
