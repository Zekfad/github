# frozen_string_literal: true

module Platform
  module Mutations
    class ClassifyAccountsAsSpammy < Platform::Mutations::Base
      description "Classify a set of accounts as spammy."

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :account_ids, [ID], "The global relay id of accounts to classify spammy.", required: true
      argument :reason, String, "Reason for marking accounts spammy.", required: true
      argument :hard_flag, Boolean, "Hard flag accounts as spammy.", required: false
      argument :suspend, Boolean, "Suspend accounts.", required: false
      argument :classify_owned_orgs_as_spammy, Boolean, required: false,
        description: "When classifying/suspending a user, also classify/suspend all owned organizations."
      argument :classify_org_owners_as_spammy, Boolean, required: false,
        description: "When classifying/suspending an organization, also classify/suspend its owners."
      argument :origin, String, "The system that performed this classification.", required: false, default_value: "origin_unknown"
      argument :rule_name, String, "The rule that performed this classification.", required: false
      argument :rule_version, String, "The rule that performed this classification.", required: false
      argument :instrument_abuse_classification, Boolean, "Instrument the abuse classification hydro event.", required: false, default_value: true

      field :accounts, [Unions::Account], "The accounts marked spammy.", null: true

      def resolve(**inputs)
        unless self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to classify accounts spammy."
        end

        account_promises = inputs[:account_ids].map do |gid|
          Platform::Helpers::NodeIdentification.async_typed_object_from_id \
            Platform::Unions::Account.possible_types, gid, context
        end

        Promise.all(account_promises).then do |accounts|
          accounts.each do |account|
            classify_suspend_account(
              account: account,
              inputs: inputs,
              instrument_abuse_classification: inputs[:instrument_abuse_classification],
              spammy_reason: inputs[:reason],
            )

            if account.organization? && inputs[:classify_org_owners_as_spammy]
              classify_suspend_accounts(
                accounts: account.admins,
                inputs: inputs,
                instrument_abuse_classification: true,
                spammy_reason: "#{inputs[:reason]} - flagged with organization",
              )
            end

            if account.user? && inputs[:classify_owned_orgs_as_spammy]
              classify_suspend_accounts(
                accounts: account.owned_organizations,
                inputs: inputs,
                instrument_abuse_classification: true,
                spammy_reason: "#{inputs[:reason]} - flagged with owner",
              )
            end
          end

          { accounts: accounts }
        end.sync
      end

      def classify_suspend_accounts(accounts:, inputs:, instrument_abuse_classification:, spammy_reason:)
        accounts.in_batches(of: 100) do |batch|
          batch.each do |account|
            classify_suspend_account(
              account: account,
              inputs: inputs,
              instrument_abuse_classification: instrument_abuse_classification,
              spammy_reason: spammy_reason,
            )
          end
        end
      end

      # Classify and maybe suspend the account.
      def classify_suspend_account(account:, inputs:, instrument_abuse_classification:, spammy_reason:)
        # Grab existing statuses so we can decide if we need to send an abuse classification event.
        not_previously_spammy = !account.spammy?
        not_previously_suspended = !account.suspended?
        serialized_previous_classification = ::Hydro::EntitySerializer.user_spammy_classification(account)
        serialized_previous_spammy_reason = ::Hydro::EntitySerializer.user_spammy_reason(account)
        serialized_previously_suspended = ::Hydro::EntitySerializer.user_suspended(account)

        account.mark_as_spammy({
          actor: context[:viewer],
          reason: spammy_reason,
          origin: inputs[:origin],
          hard_flag: inputs[:hard_flag],
          instrument_abuse_classification: false,
        })

        if inputs[:suspend]
          account.suspend(
            spammy_reason,
            instrument_abuse_classification: false,
          )
        end

        classification_changed = account.spammy? && not_previously_spammy
        suspended_status_changed = account.suspended? && not_previously_suspended

        # If the status has changed and we want to instrument these events, then send them.
        # instrument_abuse_classification being false means that another service has published the event.
        if (classification_changed || suspended_status_changed) && instrument_abuse_classification
          GlobalInstrumenter.instrument "abuse_classification.publish",
            actor: context[:viewer],
            account: account,
            origin: inputs[:origin],
            queue_action: :QUEUE_ACTION_NONE,
            rule_name: inputs[:rule_name],
            rule_version: inputs[:rule_version],
            serialized_previous_classification: serialized_previous_classification,
            serialized_previous_spammy_reason: serialized_previous_spammy_reason,
            serialized_previously_suspended: serialized_previously_suspended
        end
      end
    end
  end
end
