# frozen_string_literal: true

module Platform
  module Mutations
    class RequestMarketplaceListingApproval < Platform::Mutations::Base
      description "Request a draft listing be reviewed by GitHub for display in the Marketplace."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :slug, String, <<~DESCRIPTION, required: true
        Select the listing that matches this slug. It's the short name of the listing used in
        its URL.
      DESCRIPTION

      field :marketplace_listing, Objects::MarketplaceListing, "The updated marketplace listing.", null: true

      def resolve(**inputs)
        listing = Loaders::ActiveRecord.load(::Marketplace::Listing, inputs[:slug],
                                             column: :slug).sync

        unless listing.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to submit the " +
                                         "listing for review by GitHub.")
        end

        unless listing.verified_approval_requestable_by?(context[:viewer])
          raise Errors::Validation.new("Marketplace listing cannot be submitted for review.")
        end

        listing.request_verified_approval!(context[:viewer]) && listing.save

        { marketplace_listing: listing }
      end
    end
  end
end
