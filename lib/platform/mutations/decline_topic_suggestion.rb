# frozen_string_literal: true

module Platform
  module Mutations
    class DeclineTopicSuggestion < Platform::Mutations::Base
      description "Rejects a suggested topic for the repository."

      minimum_accepted_scopes ["public_repo"]

      visibility :public, environments: [:dotcom]

      argument :repository_id, ID, "The Node ID of the repository.", required: true, loads: Objects::Repository
      argument :name, String, "The name of the suggested topic.", required: true
      argument :reason, Enums::TopicSuggestionDeclineReason, "The reason why the suggested topic is declined.", required: true

      field :topic, Objects::Topic, "The declined topic.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repository:, **inputs)
        repo = repository
        permission.async_owner_if_org(repo).then do |org|
          permission.access_allowed?(:edit_repo, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(repository:, **inputs)

        unless repository.can_manage_topics?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to " +
                                         "decline topic suggestions for #{repository.nwo}.")
        end

        unless  repository.topic_suggestions_enabled?
          raise Errors::Unprocessable.new("Topic suggestions are not supported " +
                                          "for #{repository.nwo}.")
        end

        repository_topic = repository.decline_topic(inputs[:name], reason: inputs[:reason],
                                                    user: context[:viewer])
        if repository_topic
          { topic: repository_topic.topic }
        else
          raise Errors::Unprocessable.new("Could not decline that topic " +
                                          "suggestion at this time for #{repository.nwo}.")
        end
      end
    end
  end
end
