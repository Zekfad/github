# frozen_string_literal: true

module Platform
  module Mutations
    class ReportBrowserMetrics < Platform::Mutations::Base
      description "Internal collection endpoint for various web browser metrics."

      visibility :internal
      scopeless_tokens_as_minimum
      allow_anonymous_viewer true

      argument :stats, [Inputs::BrowserStat], "A collection of browser stats.", required: true

      def resolve(**inputs)
        stats = inputs[:stats].map { |s| s.to_h }
        GitHub::BrowserStatsHelper.report_metrics(stats, user_agent: context[:user_agent])
        nil
      end
    end
  end
end
