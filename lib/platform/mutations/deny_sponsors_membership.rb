# frozen_string_literal: true

module Platform
  module Mutations
    class DenySponsorsMembership < Platform::Mutations::Base
      description "Denies the Sponsors membership for a user"

      visibility :internal
      minimum_accepted_scopes ["biztools"]

      argument :sponsors_membership_id, ID, "The ID of the Sponsors membership to deny", required: true, loads: Objects::SponsorsMembership

      field :sponsors_membership, Objects::SponsorsMembership, "The denied Sponsors membership", null: true

      def resolve(sponsors_membership:)
        unless context[:viewer].can_admin_sponsors_listings?
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to deny the Sponsors membership"
        end

        if sponsors_membership.denied? || sponsors_membership.deny!
          { sponsors_membership: sponsors_membership.reload }
        else
          raise Errors::Unprocessable.new "An error occurred when denying the Sponsors membership for #{sponsors_membership.sponsorable.login}"
        end
      end
    end
  end
end
