# frozen_string_literal: true

module Platform
  module Mutations
    class CreateCustomInbox < Platform::Mutations::Base
      include Helpers::Newsies

      description "Create a new custom notifications inbox for the current viewer."
      minimum_accepted_scopes ["notifications"]
      visibility :under_development
      areas_of_responsibility :notifications

      argument :name, String, "The name of the new inbox.", required: true
      argument :query_string, String, "The search query used to filter the inbox.", required: true

      field :custom_inbox, Objects::NotificationFilter, "The new custom inbox.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        permission.access_allowed?(
          :create_custom_inbox,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      def resolve(**kwargs)
        inbox = unpack_newsies_response!(
          GitHub.newsies.web.create_custom_inbox(user_id: context[:viewer].id, **kwargs),
        )

        unless inbox.persisted?
          raise Errors::Unprocessable.new(inbox.errors.full_messages.to_sentence)
        end

        {
          custom_inbox: inbox,
        }
      end
    end
  end
end
