# frozen_string_literal: true

module Platform
  module Mutations
    class ClearExperiment < Platform::Mutations::Base
      description "Clears a experiment."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :id, ID, "The id of the experiment.", required: true

      field :experiment, Objects::Experiment, "The experiment that was just cleared.", null: true

      def resolve(**inputs)
        raise Errors::Unprocessable.new("You don't have permissions to perform that action.") if !Experiment.viewer_can_read?(context[:viewer])
        experiment = Platform::Helpers::NodeIdentification.typed_object_from_id(
          [Objects::Experiment], inputs[:id], context
        )

        if experiment && experiment.clear
          { experiment: experiment }
        else
          raise Errors::Unprocessable.new("Could not clear experiment.")
        end
      end
    end
  end
end
