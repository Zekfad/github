# frozen_string_literal: true

module Platform
  module Mutations
    class EnableFeatureTimePercentage < Platform::Mutations::Base
      description "Enables a feature flag for a percentage of time."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :id, ID, "The id of the feature flag.", required: true, loads: Objects::Feature, as: :feature
      argument :percentage, Float, "The percentage expressed as a value 0-100.", required: true

      field :feature, Objects::Feature, "The feature being enabled.", null: true

      def resolve(feature:, **inputs)

        if feature
          feature.enable_percentage_of_time(inputs[:percentage])
          { feature: feature }
        else
          raise Errors::Unprocessable.new("Could not enable feature.")
        end
      end
    end
  end
end
