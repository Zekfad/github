# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteDiscussion < Platform::Mutations::Base
      description "Delete a discussion and all of its replies."
      visibility :under_development
      areas_of_responsibility :discussions

      minimum_accepted_scopes ["public_repo"]

      argument :id, ID, "The id of the discussion to delete.", required: true,
        loads: Objects::Discussion, as: :discussion

      field :discussion, Objects::Discussion, "The discussion that was just deleted.", null: true

      def resolve(discussion:, **inputs)
        if !context[:actor].can_have_granular_permissions? &&
           !discussion.async_viewer_can_delete?(context[:viewer]).sync
          raise Errors::Forbidden.new("Viewer not authorized")
        end

        unless discussion
          raise Errors::Unprocessable.new("Invalid discussion.")
        end

        deleter = ::DiscussionDeleter.new(discussion)

        if deleter.delete(context[:actor])
          { discussion: discussion }
        else
          raise Errors::Unprocessable.new("Could not delete discussion.")
        end
      end
    end
  end
end
