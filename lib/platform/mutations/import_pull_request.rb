# frozen_string_literal: true

module Platform
  module Mutations
    class ImportPullRequest < Platform::Mutations::Base
      feature_flag :import_api
      minimum_accepted_scopes ["admin:org"]
      areas_of_responsibility :import_api
      description "Imports pull request data to a migrated repository."

      argument :repository_id, ID, description: "The Node ID of the repository.", required: true, loads: Objects::Repository
      argument :base_ref_name, String, description: <<~DESCRIPTION, required: true
        The name of the branch you want your changes pulled into. This should be an existing branch
        on the current repository. You cannot update the base branch on a pull request to point
        to another repository.
      DESCRIPTION
      argument :head_ref_name, String, description: <<~DESCRIPTION, required: true
        The name of the branch where your changes are implemented. For cross-repository pull requests
        in the same network, namespace `head_ref_name` with a user like this: `username:branch`.
      DESCRIPTION
      argument :title, String, description: "The title of the pull request.", required: true
      argument :body, String, description: "The text contents of the pull request description.", required: false
      argument :maintainer_can_modify, Boolean, description: "Indicates whether maintainers can modify the pull request.", required: false, default_value: true
      argument :draft, Boolean, description: "Indicates whether this pull request should be a draft.", required: false, default_value: false
      argument :number, Integer, description: "Identifies the pull request number.", required: false
      argument :merged_at, Scalars::DateTime, description: "Indicates the date and time when the pull request was merged.", required: false
      argument :created_at, Scalars::DateTime, description: "Indicates the date and time when the pull request was created.", required: false
      argument :import_id, ID, "ID of the import", required: true, loads: Objects::Import

      field :pull_request, Objects::PullRequest, "The new pull request.", null: true

      def self.async_api_can_modify?(permission, repository:, **inputs)
        permission.async_owner_if_org(repository).then do |org|
          permission.access_allowed? :migration_import, resource: org, current_repo: repository, organization: org, allow_integrations: false, allow_user_via_integration: false
        end
      end

      def resolve(repository:, import:, **inputs)
        unless repository.owner.organization?
          raise Errors::Forbidden.new("This mutation can only import into repositories belonging to an organization.")
        end

        organization = repository.owner

        unless organization.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to import to #{organization.login}.")
        end

        if ref_name_invalid?(inputs[:base_ref_name])
          raise Errors::Validation.new("The baseRefName should contain the branch name only, without a repository owner scope.")
        end

        if ref_name_invalid?(inputs[:head_ref_name])
          raise Errors::Validation.new("The headRefName should contain the branch name only, without a repository owner scope.")
        end

        if import != repository.import
          raise Errors::Unprocessable.new("The provided importId should be same as the migrated repository importId.")
        end

        pull = GitHub.importing do
          PullRequest.create_for(repository,
            user: context[:viewer],
            base: inputs[:base_ref_name],
            head: inputs[:head_ref_name],
            title: inputs[:title],
            body: inputs[:body],
            maintainer_can_modify: inputs[:maintainer_can_modify],
            draft: inputs[:draft]
          )
        end

        pull.import = import

        pull.created_at = inputs[:created_at] if inputs[:created_at]
        pull.merged_at = inputs[:merged_at] if inputs[:merged_at]
        pull.issue.number = inputs[:number] if inputs[:number]

        if pull.save
          {
            pull_request: pull,
            errors: [],
          }
        else
          raise Errors::Unprocessable.new("Could not import the pull request: #{pull.errors.full_messages.join(", ")}")
        end
      end

      private

      def ref_name_invalid?(ref_name)
        ref_name.include?(":") || ref_name.starts_with?("refs/head")
      end
    end
  end
end
