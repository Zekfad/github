# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteRepositoryImage < Platform::Mutations::Base
      description "Deletes a repository image."
      visibility :under_development
      areas_of_responsibility :repositories
      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The ID of the repository image to delete.", required: true,
        loads: Objects::RepositoryImage, as: :image

      field :repository, Objects::Repository, "The repository from which the image was deleted.",
        null: true

      def resolve(image:, **inputs)
        unless image.deletable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to remove " +
                                      "images from this repository.")
        end

        image.destroyer = context[:viewer]

        if image.destroy
          { repository: image.repository }
        else
          message = "Could not delete repository image: #{image.errors.full_messages.to_sentence}"
          raise Errors::Unprocessable.new(message)
        end
      end
    end
  end
end
