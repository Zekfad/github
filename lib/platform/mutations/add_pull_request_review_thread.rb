# frozen_string_literal: true

module Platform
  module Mutations
    class AddPullRequestReviewThread < Platform::Mutations::Base
      description "Adds a new thread to a pending Pull Request Review."

      minimum_accepted_scopes ["public_repo"]

      argument :path, String, "Path to the file being commented on.", required: true
      argument :body, String, "Body of the thread's first comment.", required: true
      argument :pull_request_id, ID, "The node ID of the pull request reviewing", required: false, loads: Objects::PullRequest, as: :pull
      argument :pull_request_review_id, ID, "The Node ID of the review to modify.", required: false, loads: Objects::PullRequestReview, as: :review
      argument :diff_range, Inputs::DiffRange, "The diff range on which the thread was left. Default is from the base of the pull request to the head of the review.", required: false
      argument :line, Integer, "The line of the blob to which the thread refers. The end of the line range for multi-line comments.", required: true
      argument :side, Enums::DiffSide, "The side of the diff on which the line resides. For multi-line comments, this is the side for the end of the line range.", default_value: :right, required: false
      argument :start_line, Integer, "The first line of the range to which the comment refers.", required: false
      argument :start_side, Enums::DiffSide, "The side of the diff on which the start line resides.", default_value: :right, required: false
      argument :submit_review, Boolean, "True to mark this review as submitted", required: false, default_value: false, visibility: :internal

      error_fields
      field :thread, Objects::PullRequestReviewThread, "The newly created thread.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        if inputs[:review]
          inputs[:review].async_pull_request.then do |pull|
            permission.async_repo_and_org_owner(pull).then do |repo, org|
              permission.access_allowed?(:create_pull_request_comment, repo: repo, current_org: org, resource: pull, allow_integrations: true, allow_user_via_integration: true)
            end
          end
        elsif inputs[:pull]
          permission.async_repo_and_org_owner(inputs[:pull]).then do |repo, org|
            permission.access_allowed?(:create_pull_request_comment, repo: repo, current_org: org, resource: inputs[:pull], allow_integrations: true, allow_user_via_integration: true)
          end
        else
          false
        end
      end

      def resolve(**inputs)
        author = context[:viewer]

        if inputs[:review]
          review = inputs[:review]
        elsif inputs[:pull]
          review = inputs[:pull].pending_review_for(user: context[:viewer])
        else
          raise Errors::Validation.new("Review or Pull Request required.")
        end

        thread = review.build_thread

        review.async_pull_request.then do |pull|
          async_diff = if inputs[:diff_range]
            pull.async_diff(**inputs[:diff_range].to_h)
          else
            pull.async_diff
          end

          async_diff.then do |diff|
            comment = thread.build_first_comment(
              user: author,
              diff: diff,
              body: inputs[:body],
              path: inputs[:path],
              line: inputs[:line],
              side: inputs[:side]&.downcase&.to_sym || :right,
              start_line: inputs[:start_line],
              start_side: inputs[:start_side]&.downcase&.to_sym || :right,
            )

            if thread.save
              review.comment! if inputs[:submit_review]

              unless GitHub.flipper[:use_review_thread_ar_model].enabled?(pull.repository)
                thread = Platform::Models::PullRequestReviewThread.from_review_comments([comment], id_version: 2)
              end
              { thread: thread, errors: [] }
            else
              # this just says there is a problem with the comments and is useless from the perspective of integrators
              thread.errors.delete(:review_comments)
              client_errors = Platform::UserErrors.mutation_errors_for_model(thread) + comment_client_errors(comment)
              { thread: nil, errors: client_errors }
            end
          end
        end
      end

      CLIENT_ERRORS_MAPPING = {
        pull_request_review_id: ["pullRequestReviewId"],
        pull_request_review_thread_id: ["pullRequestReviewId"],
        start_commit_oid: ["diffRange", "startCommitOid"],
        end_commit_oid: ["diffRange", "endCommitOid"],
        base_commit_oid: ["diffRange", "baseCommitOid"],
        line: ["line"],
        start_line: ["startLine"],
        body: ["body"],
        path: ["path"],
      }.freeze

      def comment_client_errors(comment)
        client_errors = []

        CLIENT_ERRORS_MAPPING.each_pair do |ar_attribute, path|
          if comment.errors.include?(ar_attribute)
            client_errors += build_client_errors(path, comment.errors[ar_attribute])
          end
        end

        client_errors
      end

      def build_client_errors(path, messages, prefix: "input")
        attribute = path.last.underscore
        messages.map do |message|
          {
            path: ["input", path].flatten,
            message: "#{attribute.titleize.downcase.capitalize} #{message}",
            attribute: attribute,
            short_message: message,
          }
        end
      end
    end
  end
end
