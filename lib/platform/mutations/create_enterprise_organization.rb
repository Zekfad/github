# frozen_string_literal: true

module Platform
  module Mutations
    class CreateEnterpriseOrganization < Platform::Mutations::Base
      description "Creates an organization as part of an enterprise account."
      minimum_accepted_scopes ["admin:enterprise"]

      # This mutation can only be run by end-users in the Enterprise Cloud environment.
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      argument :enterprise_id, ID, "The ID of the enterprise owning the new organization.", required: true, loads: Objects::Enterprise
      argument :login, String, "The login of the new organization.", required: true
      argument :profile_name, String, "The profile name of the new organization.", required: true
      argument :billing_email, String, "The email used for sending billing receipts.", required: true
      argument :admin_logins, [String], "The logins for the administrators of the new organization.", required: true

      field :enterprise, Objects::Enterprise, "The enterprise that owns the created organization.", null: true
      field :organization, Objects::Organization, "The organization that was created.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, enterprise:, **inputs)
        permission.access_allowed?(:administer_business, resource: enterprise, repo: nil, organization: nil, allow_integrations: false, allow_user_via_integration: false)
      end

      def resolve(enterprise:, **inputs)
        viewer = context[:viewer]
        unless enterprise.owner?(viewer)
          raise Errors::Forbidden.new("#{viewer} does not have permission to create organizations for this enterprise.")
        end

        result = Organization::Creator.perform \
          context[:viewer],
          GitHub::Plan.free,
          inputs.merge(company_name: enterprise.name),
          business_owned: true,
          business: enterprise

        if result.success?
          {
            enterprise: enterprise,
            organization: result.organization,
          }
        else
          error_message = result.organization.errors.full_messages.to_sentence
          error_message ||= result.error_message
          raise Errors::Unprocessable.new(error_message)
        end
      end
    end
  end
end
