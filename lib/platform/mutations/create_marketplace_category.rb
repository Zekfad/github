# frozen_string_literal: true

module Platform
  module Mutations
    class CreateMarketplaceCategory < Platform::Mutations::Base
      description "Creates a new Marketplace category."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["site_admin"]

      argument :name, String, "The name of the new category.", required: true
      argument :description, String, "A description of the new category.", required: false
      argument :how_it_works, String, "A technical description of how apps in the new category work with GitHub.", required: false
      argument :is_filter, Boolean, "Whether the category is a filter-type.", required: false
      argument :is_navigation_visible, Boolean, "Whether the category is to be used in Marketplace navigation menus.", required: false
      argument :is_featured, Boolean, "Whether the category is featured on the Marketplace homepage.", required: false
      argument :featured_position, Integer, "The position of the category on the Marketplace homepage.", required: false
      argument :sub_categories, [String], "A list of sub category names.", required: false

      field :marketplace_category, Objects::MarketplaceCategory, "The new Marketplace category.", null: true

      def resolve(**inputs)
        unless context[:viewer].can_admin_marketplace_listings?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to create a " +
                                         "Marketplace category.")
        end

        category = Marketplace::Category.new(name: inputs[:name],
                                             description: inputs[:description],
                                             how_it_works: inputs[:how_it_works])

        if inputs.key?(:is_filter)
          category.acts_as_filter = inputs[:is_filter]
        end

        if inputs.key?(:is_navigation_visible)
          category.navigation_visible = inputs[:is_navigation_visible]
        end

        if inputs.key?(:is_featured)
          category.featured = inputs[:is_featured]
        end

        if inputs.key?(:featured_position)
          category.featured_position = inputs[:featured_position]
        end

        if inputs.key?(:sub_categories)
          category.sub_categories = sub_categories(inputs[:sub_categories])
        end

        if category.save
          { marketplace_category: category }
        else
          errors = category.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not create the Marketplace category: #{errors}")
        end
      end

      private

      def sub_categories(category_names)
        category_names.reject(&:blank?).map do |name|
          load_sub_category(name).tap do |loaded_category|
            next if loaded_category
            raise Errors::Validation.new("No such Marketplace sub category exists: #{name}")
          end
        end
      end

      def load_sub_category(name)
        Loaders::ActiveRecord
          .load(::Marketplace::Category, name, column: :name)
          .sync
      end
    end
  end
end
