# frozen_string_literal: true

module Platform
  module Mutations
    class ReplaceAssigneesForAssignable < Platform::Mutations::Base
      description "Replaces all assignees for assignable object."

      visibility :internal
      minimum_accepted_scopes ["public_repo"]

      argument :assignable_id, ID, "The id of the assignable object to replace the assignees for.", required: true, loads: Interfaces::Assignable
      argument :assignee_ids, [ID], "The ids of the users to replace the exisiting assignees.", required: true, loads: Objects::User

      error_fields
      field :assignable, Interfaces::Assignable, "The item that was assigned.", null: true

      def resolve(assignable:, assignees:, **inputs)
        record = assignable
        issue = record.is_a?(PullRequest) ? record.issue : record

        unless issue.assignable_by?(actor: context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to manage assignees in this repository.")
        end

        issue.async_repository.then do |repository|
          if record.is_a?(Issue) && !repository.has_issues?
            raise Errors::Unprocessable::IssuesDisabled.new
          end

          if repository.locked_on_migration?
            raise Errors::Unprocessable::RepositoryMigration.new
          end

          if repository.archived?
            raise Errors::Unprocessable::RepositoryArchived.new
          end

          # Used to respect blocks
          GitHub.context.push(actor_id: @context[:viewer].id) do
            issue.assignees = assignees
          end

          if GitHub::SchemaDomain.allowing_cross_domain_transactions { issue.save }
            {
              assignable: record,
              errors: [],
            }
          else
            {
              assignable: nil,
              errors: Platform::UserErrors.mutation_errors_for_model(issue),
            }
          end
        end
      end
    end
  end
end
