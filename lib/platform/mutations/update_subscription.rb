# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateSubscription < Platform::Mutations::Base
      description "Updates the state for subscribable subjects."

      minimum_accepted_scopes ["notifications"]

      argument :subscribable_id, ID, "The Node ID of the subscribable object to modify.", required: true, loads: Interfaces::Subscribable
      argument :state, Enums::SubscriptionState, "The new state of the subscription.", required: true

      field :subscribable, Interfaces::Subscribable, "The input subscribable entity.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, subscribable:, **inputs)
        if subscribable.is_a?(::Repository)
          permission.async_owner_if_org(subscribable).then do |org|
            permission.access_allowed?(:v4_update_repo_notifications, current_repo: subscribable, resource: subscribable, current_org: org, allow_integrations: false, allow_user_via_integration: false)
          end
        elsif subscribable.is_a?(::Team) || subscribable.is_a?(::DiscussionPost)
          subscribable.async_organization.then do |org|
            permission.access_allowed?(:v4_update_thread_notifications, current_org: org, resource: org, current_repo: nil, allow_integrations: false, allow_user_via_integration: false)
          end
        else
          current_repo = subscribable.repository
          permission.async_owner_if_org(current_repo).then do |org|
            permission.access_allowed?(:v4_update_thread_notifications, resource: current_repo, current_repo: current_repo, current_org: org, allow_integrations: false, allow_user_via_integration: false)
          end
        end
      end

      def resolve(subscribable:, **inputs)
        if subscribable.is_a?(::PullRequest)
          subscribable = subscribable.issue
        end

        case subscribable
        when ::Repository
          case inputs[:state]
          when "unsubscribed"
            context[:viewer].unwatch_repo(subscribable)
          when "subscribed"
            context[:viewer].watch_repo(subscribable)
          when "ignored"
            context[:viewer].ignore_repo(subscribable)
          when "releases_only"
            context[:viewer].subscribe_to_releases_only(subscribable)
          end
        when ::Commit
          case inputs[:state]
          when "unsubscribed"
            GitHub.newsies.unsubscribe(context[:viewer], subscribable.repository, subscribable)
          when "subscribed"
            GitHub.newsies.subscribe_to_thread(context[:viewer], subscribable.repository, subscribable, :manual)
          when "ignored"
            GitHub.newsies.ignore_thread(context[:viewer], subscribable.notifications_list, subscribable)
          end
        when ::Issue, ::PullRequest, ::Discussion
          case inputs[:state]
          when "unsubscribed"
            subscribable.unsubscribe(context[:viewer])
          when "subscribed"
            subscribable.subscribe(context[:viewer], :manual)
          when "ignored"
            subscribable.ignore(context[:viewer])
          end
        when ::Team
          case inputs[:state]
          when "unsubscribed"
            GitHub.newsies.unsubscribe(context[:viewer], subscribable)
          when "subscribed"
            GitHub.newsies.subscribe_to_list(context[:viewer], subscribable)
          when "ignored"
            GitHub.newsies.ignore_list(context[:viewer], subscribable)
          end
        when ::DiscussionPost
          case inputs[:state]
          when "unsubscribed"
            subscribable.unsubscribe(context[:viewer])
          when "subscribed"
            subscribable.subscribe(context[:viewer], :manual)
          when "ignored"
            GitHub.newsies.ignore_thread(context[:viewer], subscribable.notifications_list, subscribable)
          end
        end

        { subscribable: subscribable }
      end
    end
  end
end
