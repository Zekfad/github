# frozen_string_literal: true

module Platform
  module Mutations
    class UpdatePullRequestBranch < Platform::Mutations::Base
      description "Merge HEAD from upstream branch into pull request branch"

      feature_flag :pe_mobile

      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :pull_requests

      argument :pull_request_id, ID, description: "The Node ID of the pull request.", required: true, loads: Objects::PullRequest
      argument :expected_head_oid, Scalars::GitObjectID, description: "The head ref oid for the upstream branch.", required: false

      field :pull_request, Objects::PullRequest, "The updated pull request.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, pull_request:, **inputs)
        permission.async_repo_and_org_owner(pull_request).then do |repo, org|
          permission.access_allowed? :update_pull_request, repo: repo, current_org: org, resource: pull_request, allow_integrations: true, allow_user_via_integration: true
        end
      end

      def resolve(pull_request:, expected_head_oid: nil)
        context[:permission].authorize_content(:pull_request, :update, repo: pull_request.repository)

        unless pull_request.async_viewer_can_update?(context[:viewer]).sync
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to update this pull request.")
        end

        begin
          pull_request.merge_base_into_head(
            user: context[:viewer],
            author_email: context[:viewer]&.default_author_email(pull_request.repository, pull_request.head_sha),
            expected_head_oid: expected_head_oid || pull_request.head_sha
          )
        rescue PullRequest::RefMismatch
          raise Errors::Unprocessable.new("head sha didn't match the current head ref.")
        rescue PullRequest::MergeConflictError, PullRequest::PermissionError => e
          raise Errors::Unprocessable.new(e.message)
        end

        { pull_request: pull_request }
      end
    end
  end
end
