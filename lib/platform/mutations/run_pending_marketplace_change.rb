# frozen_string_literal: true

module Platform
  module Mutations
    class RunPendingMarketplaceChange < Platform::Mutations::Base
      description "Run a pending change for a marketplace purchase."
      visibility :internal
      areas_of_responsibility :gitcoin

      minimum_accepted_scopes ["site_admin"]

      argument :id, ID, "The ID of the pending marketplace change you wish to apply.", required: true

      field :pending_marketplace_change, Objects::PendingMarketplaceChange, "The pending change for the marketplace purchase.", null: true

      def resolve(**inputs)
        change = Loaders::ActiveRecord.load(::Billing::PendingSubscriptionItemChange, inputs[:id].to_i, column: :id).sync

        unless context[:viewer]&.site_admin? || change.user.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to manage this account.")
        end

        unless change.listing.draft?
          raise Errors::Forbidden.new("#{context[:viewer]} can't run pending changes for non-draft listings.")
        end

        change.run
        change.destroy

        { pending_marketplace_change: change }
      end
    end
  end
end
