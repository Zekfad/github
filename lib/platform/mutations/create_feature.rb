# frozen_string_literal: true

module Platform
  module Mutations
    class CreateFeature < Platform::Mutations::Base
      description "Creates a feature flag."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :name, String, "The name of the feature to be used in code", required: true
      argument :description, String, "A brief description of the feature.", required: false

      error_fields
      field :feature, Objects::Feature, "The new feature.", null: true

      def resolve(**inputs)
        feature = FlipperFeature.new(name: inputs[:name], description: inputs[:description])
        if feature.save
          { feature: feature, errors: [] }
        else
          {
            feature: nil,
            errors: Platform::UserErrors.mutation_errors_for_model(feature),
          }
        end
      end
    end
  end
end
