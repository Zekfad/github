# frozen_string_literal: true

module Platform
  module Mutations
    class AcceptSponsorsMembership < Platform::Mutations::Base
      description "Accepts the Sponsors membership for a user"

      visibility :internal
      minimum_accepted_scopes ["biztools"]

      argument :sponsors_membership_id, ID, "The ID of the Sponsors membership to accept", required: true, loads: Objects::SponsorsMembership

      field :sponsors_membership, Objects::SponsorsMembership, "The accepted Sponsors membership", null: true

      def resolve(sponsors_membership:)
        unless context[:viewer].can_admin_sponsors_listings?
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to accept the Sponsors membership"
        end

        result = Sponsors::AcceptSponsorsMembership.call(
          sponsors_membership: sponsors_membership,
          actor: context[:viewer],
        )

        if result.success?
          { sponsors_membership: sponsors_membership.reload }
        else
          raise Errors::Unprocessable.new \
            "An error occurred when accepting the Sponsors membership for #{sponsors_membership.sponsorable.login}: #{result.errors.to_sentence}"
        end
      end
    end
  end
end
