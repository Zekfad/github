# frozen_string_literal: true

module Platform
  module Mutations
    class AddMobileDeviceToken < Platform::Mutations::Base
      include Helpers::Newsies

      description "Associate a mobile device token with the current viewer."
      minimum_accepted_scopes ["user"]
      areas_of_responsibility :notifications
      mobile_only true

      argument :service, Enums::PushNotificationService, "The push notification service that issued the device token.", required: true
      argument :device_token, String, "The device token.", required: true
      argument :device_name, String, "The name of the device.", required: false

      field :success, Boolean, "Did the operation succeed?", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        permission.access_allowed?(
          :update_user_notification_settings,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      def resolve(**kwargs)
        token = unpack_newsies_response!(
          GitHub.newsies.add_mobile_device_token(user_id: context[:viewer].id, **kwargs),
        )

        unless token.persisted?
          raise Errors::Unprocessable.new(token.errors.full_messages.to_sentence)
        end

        { success: true }
      end
    end
  end
end
