# frozen_string_literal: true

module Platform
  module Mutations
    module PackagesPrerequisites
      extend self

      # Internal: Checks that a repository / owner and a viewer can use GitHub Packages.
      #
      # Explicitly does *not* check authn/authz. That is done in individual mutations.
      #
      # Returns a Hash shaped like a PackagesMutationResponse
      def check_repository_prerequisites(repository:, actor:, storage_requested: nil, feature_flags: [])
        success = true
        user_safe_status = :ok
        user_safe_message = "Request completed successfully."
        error_type = "none"

        owner = repository.owner
        billing_permission = Billing::PackageRegistryPermission.new(owner)

        case
        when repository.locked?
          success = false
          user_safe_status = :forbidden
          user_safe_message = "Repository \"#{repository.nwo}\" is locked and can't accept package uploads."
          error_type = "repository_locked"
        when repository.disabled? || repository.access.disabled?
          success = false
          user_safe_status = :forbidden
          user_safe_message = "Repository \"#{repository.nwo}\" is disabled and can't accept package uploads."
          error_type = "repository_disabled"
        when repository.archived?
          success = false
          user_safe_status = :forbidden
          user_safe_message = "Repository \"#{repository.nwo}\" is archived and can't accept package uploads."
          error_type = "repository_archived"
        when owner.spammy?
          success = false
          user_safe_status = :forbidden
          user_safe_message = "Repository \"#{repository.nwo}\" can't accept package uploads."
          error_type = "repository_spammy"
        when owner.disabled?
          success = false
          user_safe_status = :forbidden
          user_safe_message = "The \"#{owner.login}\" account is disabled and can't accept package uploads."
          error_type = "account_disabled"
        when actor.spammy?
          success = false
          user_safe_status = :forbidden
          user_safe_message = "Repository \"#{repository.nwo}\" can't accept package uploads."
          error_type = "user_spammy"
        when actor.blocked_by?(owner)
          success = false
          user_safe_status = :forbidden
          user_safe_message = "You are blocked from publishing packages to the \"#{owner.login}\" account."
          error_type = "user_blocked"
        when !billing_permission.allowed?(public: repository.public?)
          success = false
          user_safe_status = :forbidden
          user_safe_message = billing_permission.status[:error][:message] || "Please verify the billing status for this account."
          error_type = \
          case billing_permission.status[:error][:reason]
          when "DISABLED" then "account_disabled"
          when "PLAN_INELIGIBLE" then "plan_ineligible"
          when "TRADE_RESTRICTED_ORGANIZATION" then "trade_restriction"
          end
        when storage_requested
          GitHub.dogstats.time "packages.check_repository_prerequisites.storage_requested" do
            if GitHub.flipper[:registry_enforce_storage_limits].enabled?
              unless billing_permission.storage_allowed?(bytes: storage_requested, public: repository.public?)
                success = false
                user_safe_status = :forbidden
                user_safe_message = "This operation would exceed the storage allotment for this account."
                error_type = "resource_limited"
              end
            end
          end
        end

        if success && feature_flags.any?
          feature_flags.each do |flag|
            unless GitHub.flipper[flag].enabled?(owner)
              success = false
              user_safe_status = :forbidden
              user_safe_message = "This GitHub Package Registry early access feature is not enabled for the \"#{owner.login}\" account."
              error_type = "feature_flag"
              next
            end
          end
        end

        return {
          success: success,
          user_safe_status: user_safe_status,
          user_safe_message: user_safe_message,
          error_type: error_type,
          validation_errors: [],
        }
      end
    end
  end
end
