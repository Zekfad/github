# frozen_string_literal: true

module Platform
  module Mutations
    class RejectMarketplaceListing < Platform::Mutations::Base
      description "Reject a Marketplace listing that has been submitted for approval, either " +
                  "returning it to draft state or rejecting it permanently."

      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The ID of the Marketplace listing to update.", required: true, loads: Objects::MarketplaceListing, as: :listing
      argument :state, Enums::MarketplaceListingState, "The state the listing should be returned to.", required: true
      argument :message, String, "A custom message for the integrator.", required: false

      field :marketplace_listing, Objects::MarketplaceListing, "The updated marketplace listing.", null: true

      def resolve(listing:, **inputs)
        unless context[:viewer].can_admin_marketplace_listings?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission " +
                                         "to reject the listing.")
        end

        case inputs[:state]
        when "draft"
          unless listing.can_redraft?
            raise Errors::Validation.new("Listing cannot be moved to draft state.")
          end

          listing.redraft!(context[:viewer], inputs[:message])
        when "unverified"
          unless listing.can_redraft?
            raise Errors::Validation.new("Listing cannot be moved to unverified state.")
          end

          listing.redraft!(context[:viewer], inputs[:message])
        when "rejected"
          unless listing.can_reject?
            raise Errors::Validation.new("Listing cannot be rejected.")
          end

          listing.reject!(context[:viewer], inputs[:message])
        else
          raise Errors::Validation.new("Cannot reject a listing by moving it to the '#{inputs[:state]}' state.")
        end

        { marketplace_listing: listing }
      end
    end
  end
end
