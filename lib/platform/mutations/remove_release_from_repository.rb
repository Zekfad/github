# frozen_string_literal: true

module Platform
  module Mutations
    class RemoveReleaseFromRepository < Platform::Mutations::Base
      visibility :internal
      description "Removes a release from a repository."

      minimum_accepted_scopes ["repo"]

      argument :release_id, ID, "The Release ID to delete.", required: true, loads: Objects::Release
      argument :repository_id, ID, "The ID of the repository containing the release", required: true, loads: Objects::Repository

      field :release, Objects::Release, "The new release object.", null: true

      def resolve(release:, repository:, **inputs)

        raise Errors::Forbidden.new("#{context[:viewer]} does not have write access to this repository.") unless repository.writable_by?(context[:viewer])

        release.destroy
        { release: release }
      end
    end
  end
end
