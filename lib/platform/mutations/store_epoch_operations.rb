# frozen_string_literal: true

module Platform
  module Mutations
    class StoreEpochOperations < Platform::Mutations::Base
      description "Stores changes made to a commit during an epoch."

      feature_flag :epochs
      areas_of_responsibility :epochs

      minimum_accepted_scopes ["repo"]

      argument :ref_id, ID, "The ID of the ref for the branch on which these changes are based.", required: true, loads: Objects::Ref
      argument :base_commit_oid, String, "The base commit oid for the changes.", required: true
      argument :operations, [String], "The operations which are being stored including buffer deletions, additions and changes.", required: true

      error_fields

      field :operations, [Platform::Objects::EpochOperation], "The stored operations.", null: true

      def self.async_api_can_modify?(permission, ref:, **inputs)
        repo = ref.repository
        permission.async_owner_if_org(repo).then do |org|
          permission.access_allowed?(:write_file, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(ref:, **inputs)
        begin
          operations = Epoch.store_operations(
            author: context[:viewer],
            ref: ref,
            base_commit_oid: inputs[:base_commit_oid],
            operations: inputs[:operations],
          )

          { operations: operations, errors: [] }
        rescue Git::Ref::UpdateFailed => e
          fail Errors::Forbidden, e.message
        rescue Git::Ref::UpdateError => e
          fail Errors::Forbidden, e.message
        end
      end
    end
  end
end
