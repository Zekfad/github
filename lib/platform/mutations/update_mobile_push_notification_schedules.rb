# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateMobilePushNotificationSchedules < Platform::Mutations::Base
      include Helpers::Newsies

      description "Update existing mobile push notification schedules."
      minimum_accepted_scopes ["user"]
      feature_flag :pe_mobile
      areas_of_responsibility :notifications

      argument :ids, [ID], "The Node IDs of the schedules to be updated. Limit: #{Newsies::MobilePushNotificationSchedule::WDAY.count}", required: true,
        loads: Objects::MobilePushNotificationSchedule, as: :schedules
      argument :starts_at, Scalars::DateTime, "The start time for the schedule.", required: true
      argument :ends_at, Scalars::DateTime, "The end time for the schedule.", required: true

      error_fields
      field :mobile_push_notification_schedules, [Objects::MobilePushNotificationSchedule], "The updated mobile push notification schedules.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        permission.access_allowed?(
          :update_user_notification_settings,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      def resolve(schedules:, starts_at:, ends_at:)
        limit = Newsies::MobilePushNotificationSchedule::WDAY.count
        if schedules.count > limit
          return {
            mobile_push_notification_schedules: [],
            errors: [{
              "path" => ["input", "ids"],
              "message" => "The request exceeded limit of #{limit} ids.",
            }],
          }
        end

        updated_schedules = handle_newsies_service_unavailable do
          schedules.each { |schedule| schedule.update(start_time: starts_at, end_time: ends_at) }
        end

        valid_schedules = updated_schedules.select(&:valid?)
        invalid_schedules = schedules - valid_schedules
        error_messages = invalid_schedules.map { |schedule| schedule.errors.full_messages.join(", ") }
        {
          mobile_push_notification_schedules: valid_schedules.map(&:reload),
          errors: invalid_schedules.map do |schedule|
            Platform::UserErrors.mutation_errors_for_model(schedule, translate: { day: "ids" })
          end.flatten
        }
      end
    end
  end
end
