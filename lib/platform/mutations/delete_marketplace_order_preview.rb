# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteMarketplaceOrderPreview < Platform::Mutations::Base
      description "Deletes an order preview for a Marketplace user/listing"
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :order_preview_id, ID, "The order preview ID to delete", required: true

      field :is_destroyed, Boolean, "Whether or not the order preview was successfully destroyed.", null: true

      def resolve(**inputs)
        preview = Platform::Helpers::NodeIdentification.typed_object_from_id(
          [Objects::MarketplaceOrderPreview],
          inputs[:order_preview_id],
          context,
        )

        if preview.user == context[:viewer]
          GitHub.dogstats.increment("marketplace.order_previews.deleted", tags: ["notified:#{preview.notification_sent?}"])
          GitHub.instrument("marketplace_order_preview.deleted", {
            dimensions: {
              user_id: context[:viewer].id,
            },
            order_preview_viewed_at: preview.viewed_at,
            order_preview_email_notification_sent_at: preview.email_notification_sent_at,
            account_id: preview.account_id,
            marketplace_listing_id: preview.marketplace_listing_id,
            marketplace_listing_plan_id: preview.marketplace_listing_plan_id,
          })
          preview.destroy
        else
          raise Errors::Forbidden.new("Order previews may only be deleted by their user.")
        end

        { is_destroyed: preview.destroyed? }
      end
    end
  end
end
