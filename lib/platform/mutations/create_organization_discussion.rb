# frozen_string_literal: true

module Platform
  module Mutations
    class CreateOrganizationDiscussion < Platform::Mutations::Base
      description "Creates a new organization discussion."
      visibility :under_development

      minimum_accepted_scopes ["write:discussion"]

      argument :organization_id, ID, "The ID of the organization to which the discussion belongs.", required: true, loads: Objects::Organization
      argument :title, String, "The title of the discussion.", required: true
      argument :body, String, "The content of the discussion.", required: true
      argument :private, Boolean, "If true or not specified, restricts the visiblity of this discussion to organization members and organization admins. If false, allows any user to view this discussion.", required: false

      field :discussion, Objects::OrganizationDiscussion, "The new discussion.", null: true

      def resolve(organization:, **inputs)
        unless organization.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new(
            "#{context[:viewer]} can't create a discussion without being an admin of " +
            "#{organization.login}.")
        end

        raise Errors::Validation.new("Title can't be blank") if inputs[:title]&.strip.blank?

        discussion = organization.discussion_posts.build(
          title: inputs[:title],
          body: inputs[:body],
          private: inputs[:private].nil? ? true : inputs[:private],
          user: context[:viewer])

        if discussion.save
          { discussion: discussion }
        else
          raise Errors::Validation.new(discussion.errors.full_messages.to_sentence)
        end
      end
    end
  end
end
