# frozen_string_literal: true
module Platform
  module Mutations
    class UpdateIssue < Platform::Mutations::Base
      description "Updates an Issue."

      minimum_accepted_scopes ["public_repo"]

      argument :id, ID, "The ID of the Issue to modify.", required: true, loads: Objects::Issue, as: :issue
      argument :title, String, "The title for the issue.", required: false
      argument :body, String, "The body for the issue description.", required: false
      argument :assignee_ids, [ID], "An array of Node IDs of users for this issue.", required: false, loads: Objects::User
      argument :milestone_id, ID, "The Node ID of the milestone for this issue.", required: false, loads: Objects::Milestone
      argument :label_ids, [ID], "An array of Node IDs of labels for this issue.", required: false, loads: Objects::Label
      argument :state, Enums::IssueState, "The desired issue state.", required: false
      argument :project_ids, [ID], "An array of Node IDs for projects associated with this issue.", required: false

      error_fields
      field :issue, Objects::Issue, "The issue.", null: true
      field :actor, Interfaces::Actor, "Identifies the actor who performed the event.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        issue = inputs[:issue]
        permission.async_repo_and_org_owner(issue).then do |repo, org|
          permission.access_allowed?(:edit_issue, resource: issue, repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(execution_errors:, **inputs)
        issue = inputs[:issue]
        repository = issue.repository

        context[:permission].authorize_content(:issue, :update, issue: issue, repo: repository)

        if !context[:actor].can_have_granular_permissions? && !issue.async_viewer_can_update?(context[:viewer]).sync
          message = "#{context[:viewer]} does not have permission to update the issue #{issue.global_relay_id}."
          raise Errors::Forbidden.new(message)
        end

        unless repository.has_issues?
          raise Errors::Forbidden, "Repository has issues disabled."
        end

        if context[:permission].integration_user_request?
          issue.performed_via_integration = context[:integration]
          issue.modifying_integration = context[:integration]
        end

        previous_title = issue.title
        previous_body = issue.body

        attributes = {}
        attributes[:title] = inputs[:title] if inputs.key?(:title)

        # The viewer must have write access to assign assignees, milestone, and labels.
        if repository.pushable_by?(context[:viewer]) || repository.resources.issues.writable_by?(context[:viewer])
          if inputs.key?(:milestone)
            attributes[:milestone] = inputs[:milestone]
          end

          if inputs.key?(:labels)
            labels = inputs[:labels]
          end

          if inputs.key?(:assignees)
            attributes[:assignees] = inputs[:assignees] || []
          end

          if inputs.key?(:project_ids)
            visible_cards = issue.visible_cards_for(context[:viewer])
            visible_project_ids = visible_cards.map(&:project).map(&:global_relay_id)
            project_ids_to_add = inputs[:project_ids] - visible_project_ids
            projects_to_add = project_ids_to_add.map do |id|
              Platform::Helpers::NodeIdentification.typed_object_from_id([Platform::Objects::Project], id, context)
            end

            projects_to_add.each do |project|
              if project.writable_by?(context[:viewer])
                add_card_helper = Platform::Helpers::AddProjectCard.new(project, context)
                add_card_helper.check_permissions
                add_card_helper.check_issue_project_owner(issue)
                issue.cards.build(creator: context[:viewer], project: project, content: issue)
              else
                raise Errors::Forbidden.new("You don't have permission to add to project with id '#{project.global_relay_id}'.")
              end
            end

            cards_to_delete = visible_cards.select do |card|
              inputs[:project_ids].none?(card.project.global_relay_id)
            end

            cards_to_delete.each do |card|
              if card.writable_by?(context[:viewer])
                delete_card_helper = Platform::Helpers::DeleteProjectCard.new(card, context)
                delete_card_helper.delete_card
              end
            end
          end
        end

        saved = \
          case inputs[:state]
          when "closed"
            issue.close(context[:viewer], attributes)
          when "open"
            issue.open(context[:viewer], attributes)
          else
            GitHub::SchemaDomain.allowing_cross_domain_transactions do
              issue.cards.transaction do
                issue.update(attributes)
              end
            end
          end

        if inputs.key?(:body)
          issue.update_body(inputs[:body], context[:viewer])
        end

        if saved || issue.valid?
          issue.replace_labels(labels) unless labels.nil?

          issue.instrument_hydro_update_event(
            actor: context[:viewer],
            updater: context[:viewer],
            repo: repository,
            prev_title: previous_title,
            prev_body: previous_body,
          )

          {
            issue: issue,
            actor: context[:viewer],
            errors: [],
          }
        else
          Platform::UserErrors.append_legacy_mutation_model_errors_to_context(issue, execution_errors)

          {
            issue: nil,
            errors: Platform::UserErrors.mutation_errors_for_model(issue),
          }
        end
      end

    end
  end
end
