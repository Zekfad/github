# frozen_string_literal: true

module Platform
  module Mutations
    class ClearMobileDeviceTokens < Platform::Mutations::Base
      include Helpers::Newsies

      description "Deletes all mobile device tokens for a user."
      minimum_accepted_scopes ["site_admin"]
      visibility :under_development
      areas_of_responsibility :notifications

      argument :user_id, ID, "ID the user from whom to clear device tokens.", required: true, loads: Objects::User
      argument :service, Enums::PushNotificationService, "The push notification service that issued the device tokens.", required: false

      field :success, Boolean, "Did the operation succeed?", null: true

      def self.async_api_can_modify?(permission, **inputs)
        viewer_is_site_admin?(permission.viewer, name)
      end

      def resolve(user:, service: nil)
        destroyed_tokens = unpack_newsies_response!(
          GitHub.newsies.destroy_mobile_device_tokens(user.id, service: service&.downcase&.to_sym),
        )

        unless destroyed_tokens.all?(&:destroyed?)
          errors_by_token_id = destroyed_tokens.reduce({}) do |memo, token|
            unless token.destroyed?
              memo[token.device_token] = token.errors.full_messages.to_sentence
            end
            memo
          end
          raise Errors::Unprocessable.new(errors_by_token_id.to_json)
        end

        { success: true }
      end
    end
  end
end
