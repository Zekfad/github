# frozen_string_literal: true

module Platform
  module Mutations
    class CreateNewsletterSubscription < Platform::Mutations::Base
      description "Creates a newsletter subscription for a user"

      visibility :internal
      allow_anonymous_viewer true

      minimum_accepted_scopes ["repo"]

      argument :user_id, Integer, "The database id of the user", required: true
      argument :newsletter, String, "The name of the newsletter", required: true
      argument :kind, String, "The interval/kind of newsletter subscription", required: true

      field :newsletter_subscription, Objects::NewsletterSubscription, "The newsletter subscription", null: true

      def resolve(**inputs)
        user = Loaders::ActiveRecord.load(::User, inputs[:user_id]).sync

        subscription = user.newsletter_subscriptions.create(
          name: inputs[:newsletter],
          kind: inputs[:kind],
          auto_subscribed: true,
          subscribed_at: Time.now,
          active: true,
        )

        { newsletter_subscription: subscription }
      end
    end
  end
end
