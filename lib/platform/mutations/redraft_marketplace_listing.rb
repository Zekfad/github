# frozen_string_literal: true

module Platform
  module Mutations
    class RedraftMarketplaceListing < Platform::Mutations::Base
      description "Return a Marketplace listing that has been submitted for " +
                  "approval back to draft state."

      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :slug, String, "Select the listing that matches this slug. It's the short name of the listing used in its URL.", required: true


      field :marketplace_listing, Objects::MarketplaceListing, "The updated marketplace listing.", null: true

      def resolve(**inputs)
        listing = Loaders::ActiveRecord.load(::Marketplace::Listing, inputs[:slug],
                                             column: :slug).sync

        unless listing.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to return the " +
                                         "Marketplace listing to draft state.")
        end

        unless listing.can_redraft?
          raise Errors::Validation.new("Marketplace listing cannot be moved to draft state.")
        end

        listing.redraft!(context[:viewer])

        { marketplace_listing: listing }
      end
    end
  end
end
