# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteFeature < Platform::Mutations::Base
      description "Deletes a feature flag."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :id, ID, "The id of the feature flag.", required: true, loads: Objects::Feature, as: :feature

      def resolve(feature:, **inputs)

        if feature && feature.destroy
          {}
        else
          raise Errors::Unprocessable.new("Could not delete feature.")
        end
      end
    end
  end
end
