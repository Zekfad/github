# frozen_string_literal: true

module Platform
  module Mutations
    class CreateRepositoryImport < Platform::Mutations::Base
      feature_flag :import_api
      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :import_api
      description "Create a new imported repository."

      argument :import_id, ID, "ID of the import", required: true, loads: Objects::Import
      argument :visibility, Enums::RepositoryVisibility,
        "Indicates the repository's visibility level.", required: true
      argument :name, String, "The name of the new repository.", required: true
      argument :owner_id, ID, "The ID of the owner for the new repository.", required: false,
        loads: Interfaces::RepositoryOwner

      field :repository, Objects::Repository, "The new repository.", null: true

      def self.async_api_can_modify?(permission, owner: nil, **params)
        owner ||= permission.viewer
        org = owner if owner.organization?
        permission.access_allowed?(:import_api_create_repo,
          resource: owner,
          allow_integrations: true,
          allow_user_via_integration: true,
          current_repo: nil,
          organization: org,
        )
      end

      def resolve(import:, visibility:, name:, owner: nil)
        unless GitHub.flipper[:import_api].enabled?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer].login} is not authorized to call import APIs.")
        end

        owner ||= context[:viewer]
        if owner.user? && owner != context[:viewer]
          raise Errors::Forbidden.new("#{context[:viewer]} cannot create a repository for " \
                                      "#{owner}.")
        end

        upgrade_to_bigger_plan = false
        repo_attributes = {
          import: import,
          visibility: visibility,
          name: name
        }

        result = Repository.handle_creation(
          context[:viewer],
          owner.login,
          upgrade_to_bigger_plan,
          repo_attributes)

        if result.success?
          { repository: result.repository }
        else
          if !result.allowed
            raise Errors::Forbidden.new(
              "#{context[:viewer]} cannot create a repository for " \
              "#{owner}.")
          else
            error = result.repository.errors.full_messages.join(", ").presence ||
              result.error_message
            raise Errors::Unprocessable.new(error)
          end
        end
      end
    end
  end
end
