# frozen_string_literal: true

module Platform
  module Mutations
    class AcceptEnterpriseAdministratorInvitation < Platform::Mutations::Base
      description "Accepts a pending invitation for a user to become an administrator of an enterprise."

      # This mutation can only be run by end-users in the Enterprise Cloud environment.
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      scopeless_tokens_as_minimum

      argument :invitation_id, ID, "The id of the invitation being accepted", required: true, loads: Objects::EnterpriseAdministratorInvitation

      field :invitation, Objects::EnterpriseAdministratorInvitation, "The invitation that was accepted.", null: true
      field :message, String, "A message confirming the result of accepting an administrator invitation.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, invitation:)
        invitation.async_business.then do |business|
          next true if invitation.email.present?
          permission.viewer.is_a?(User) && permission.viewer.id == invitation.invitee_id
        end
      end

      def resolve(invitation:)
        invitation.accept acceptor: context[:viewer]

        {
          invitation: invitation,
          message: "You are now #{invitation.role_for_message} of #{invitation.business.name}.",
        }
      rescue BusinessMemberInvitation::ExpiredError
        raise Errors::Unprocessable.new("This invitation has already expired.")
      rescue BusinessMemberInvitation::AlreadyAcceptedError
        raise Errors::Unprocessable.new("This invitation has already been accepted.")
      rescue BusinessMemberInvitation::InvalidAcceptorError
        raise Errors::Unprocessable.new("Viewer cannot accept an invitation when they are not the invitee.")
      end
    end
  end
end
