# frozen_string_literal: true

module Platform
  module Mutations
    class AddReleaseToRepository < Platform::Mutations::Base
      description "Adds a release version to a repository."
      visibility :internal
      minimum_accepted_scopes ["repo"]

      argument :name, String, "Identifies the title of the release.", required: false
      argument :tag_name, String, "Identifies the git tag of the release.", required: true
      argument :description, String, "Identifies the description of the release.", required: false
      argument :author_id, ID, "The author of the release", required: true
      argument :repository_id, ID, "The ID of the repository containing the release", required: true
      argument :prerelease, Boolean, "Whether or not the release is a prerelease", required: false
      argument :draft, Boolean, "True if the release is a draft", required: false

      field :release, Objects::Release, "The new release object.", null: true

      def resolve(**inputs)
        author = User.where(id: inputs[:author_id]).first
        raise Errors::Forbidden.new("User #{inputs[:author_id]} does not exist.") unless author

        repository = author.repositories.where(id: inputs[:repository_id]).first
        raise Errors::Forbidden.new("Repository #{inputs[:repository_id]} does not exist.") unless repository
        raise Errors::Forbidden.new("#{context[:viewer]} does not have write access to this repository.") unless repository.writable_by?(context[:viewer])

        release = repository.releases.where(tag_name: inputs[:tag_name]).first
        raise Errors::Validation.new("Release with tag \"#{inputs[:tag_name]}\" already exists on the repository.") if release

        release = Release.new(
          name: inputs[:name],
          tag_name: inputs[:tag_name],
          body: inputs[:description],
          author_id: inputs[:author_id],
          repository_id: inputs[:repository_id],
          prerelease: inputs[:prerelease] || false,
        )

        release.draft = inputs[:draft] || false
        release.save!

        { release: release }
      end
    end
  end
end
