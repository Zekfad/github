# frozen_string_literal: true

module Platform
  module Mutations
    class IncrementRegistryPackageDownloadCount < Platform::Mutations::Base
      description "increments the download count for a package by 1."
      visibility :internal
      minimum_accepted_scopes ["read:packages"]

      argument :package_version_id, ID, "The Package Version ID to log a download.", required: true, loads: Objects::PackageVersion

      field :package_version_id, ID, "The Package Version ID to log a download.", null: true

      def resolve(package_version:, **inputs)
        raise Errors::Validation.new("Version not found.") unless package_version

        Registry::PackageDownloadActivity.track(package_version.registry_package_id, package_version.id, Time.now.change(min: 0, sec: 0))

        publish_download_hydro_event package_version

        { package_version_id: package_version.global_relay_id }
      end

      def publish_download_hydro_event(package_version)
        params = {
          repository: package_version.package&.repository,
          registry_package_id: package_version.package&.id,
          version: package_version.version,
          actor: context[:viewer],
          action: "DOWNLOADED",
          user_agent: GitHub.context[:user_agent].to_s,
        }

        GlobalInstrumenter.instrument("package.downloaded", params)
      end
    end
  end
end
