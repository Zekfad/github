# frozen_string_literal: true

module Platform
  module Mutations
    class AddPullRequestReviewComment < Platform::Mutations::Base
      description "Adds a comment to a review."

      minimum_accepted_scopes ["public_repo"]

      argument :pull_request_id, ID, "The node ID of the pull request reviewing", required: false, loads: Objects::PullRequest, as: :pull
      argument :pull_request_review_id, ID, "The Node ID of the review to modify.", required: false, loads: Objects::PullRequestReview, as: :review
      argument :commitOID, Scalars::GitObjectID, "The SHA of the commit to comment on.", required: false,
        as: :commit_oid
      argument :body, String, "The text of the comment.", required: true
      argument :path, String, "The relative path of the file to comment on.", required: false
      argument :position, Integer, "The line index in the diff to comment on.", required: false
      argument :in_reply_to, ID, "The comment id to reply to.", required: false, loads: Objects::PullRequestReviewComment

      field :comment_edge, Objects::PullRequestReviewComment.edge_type, "The edge from the review's comment connection.", null: true

      field :comment, Objects::PullRequestReviewComment, "The newly created comment.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        if inputs[:review]
          inputs[:review].async_pull_request.then do |pull|
            permission.async_repo_and_org_owner(pull).then do |repo, org|
              permission.access_allowed?(:create_pull_request_comment, repo: repo, current_org: org, resource: pull, allow_integrations: true, allow_user_via_integration: true)
            end
          end
        elsif inputs[:pull]
          permission.async_repo_and_org_owner(inputs[:pull]).then do |repo, org|
            permission.access_allowed?(:create_pull_request_comment, repo: repo, current_org: org, resource: inputs[:pull], allow_integrations: true, allow_user_via_integration: true)
          end
        else
          false
        end
      end

      def resolve(**inputs)
        if inputs[:review]
          review = inputs[:review]
        elsif inputs[:pull]
          review = inputs[:pull].pending_review_for(user: context[:viewer])
        else
          raise Errors::Validation.new("Review or Pull Request required.")
        end

        if !review.pending?
          raise Errors::Validation.new("Review has already been submitted.")
        end
        end_commit_oid = inputs[:commit_oid] || review.pull_request.head_sha
        pull_comparison = PullRequest::Comparison.find(
          pull: review.pull_request,
          start_commit_oid: review.pull_request.merge_base,
          end_commit_oid: end_commit_oid,
          base_commit_oid: review.pull_request.merge_base,
        )

        unless pull_comparison
          raise Errors::Validation.new("The commitOID is not part of the pull request.")
        end

        thread = comment = nil

        Platform::LoaderTracker.ignore_association_loads do
          if !inputs[:in_reply_to]
            thread, comment = review.build_thread_with_comment(
              user: context[:viewer],
              body: inputs[:body],
              diff: pull_comparison.diffs,
              position: inputs[:position].to_i,
              path: inputs[:path],
            )
          else
            parent = inputs[:in_reply_to]
            if parent.pull_request_id != review.pull_request_id
              raise Errors::Validation.new("The inReplyTo comment is not part of the pull request.")
            end

            thread = parent.pull_request_review_thread
            comment = thread.build_reply(
              pull_request_review: review,
              user: context[:viewer],
              body: inputs[:body],
            )
          end
        end

        PullRequestReview.transaction do
          raise ActiveRecord::Rollback unless thread.save && comment.save # rubocop:disable GitHub/UsePlatformErrors
        end

        if comment.persisted?
          relation = review.review_comments.scoped
          klass = GraphQL::Relay::BaseConnection.connection_for_nodes(relation)
          connection = klass.new(relation, {})
          {
            comment: comment,
            comment_edge: GraphQL::Relay::Edge.new(comment, connection),
          }
        else
          raise Errors::Unprocessable.new(comment.errors.full_messages.to_sentence)
        end
      end
    end
  end
end
