# frozen_string_literal: true
require "venice"

module Platform
  module Mutations
    class CreateMobileSubscription < Platform::Mutations::Base
      description "Creates a subscription representing a in-app purchase"

      feature_flag :pe_mobile
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]
      minimum_accepted_scopes ["user"]

      argument :apple_receipt, String, "The base64 encoded reciept for the apple in-app purchase", required: true

      field :success, Boolean, "Whether or not the subscription was successful", null: true

      def self.async_api_can_modify?(permission, **inputs)
        permission.access_allowed?(:update_user, resource: permission.viewer,
                                   current_repo: nil, current_org: nil,
                                   allow_integrations: false, allow_user_via_integration: false)
      end

      def resolve(apple_receipt:)
        if receipt = Venice::Receipt.verify(apple_receipt, { shared_secret: GitHub.apple_iap_shared_secret })
          response = receipt.original_json_response
          latest_receipt = response.dig("latest_receipt") || apple_receipt
          transaction_id = response.dig("latest_receipt_info", 0, "original_transaction_id") || response.dig("receipt", "original_transaction_id")

          unless transaction_id
            raise Errors::Unprocessable.new("Apple transaction id non existant.")
          end

          if Billing::PlanSubscription.exists?(apple_transaction_id: transaction_id)
            raise Errors::Unprocessable.new("This Apple subscription is already tied to another GitHub account.")
          end

          account = context[:viewer]
          account.seats = 0
          account.plan_duration = User::MONTHLY_PLAN
          account.plan = GitHub::Plan.pro

          if account.disabled?
            account.disabled = false
          end

          unless account.save
            raise Errors::Unprocessable.new("Account invalid.")
          end

          # create customer
          unless account.customer
            service = Billing::CreateCustomer.perform(account, { apple_iap_subscription: true })

            unless service.success?
              raise Errors::Unprocessable.new("Customer could not be created successfully.")
            end
          end

          if account.plan_subscription
            account.plan_subscription.update(apple_receipt_id: latest_receipt, apple_transaction_id: transaction_id)
            SynchronizePlanSubscriptionJob.perform_later({"user_id" => account.id})
          else
            # after_commit that ends up calling SynchronizePlanSubscriptionJob.perform
            account.create_plan_subscription!(customer: account.customer, apple_receipt_id: latest_receipt, apple_transaction_id: transaction_id)
          end

          { success: true }
        else
          raise Errors::Unprocessable.new("Apple receipt is not valid.")
        end
      end
    end
  end
end
