# frozen_string_literal: true

module Platform
  module Mutations
    class DisableSponsorsMatch < Platform::Mutations::Base
      description "Disables match for the specified Sponsors listing"

      visibility :internal
      minimum_accepted_scopes ["biztools"]

      argument :sponsors_listing_id, ID, "The ID of the Sponsors listing to disable match for", required: true, loads: Objects::SponsorsListing
      argument :reason, String, "The reason for disabling match for this listing", required: true

      field :sponsors_listing, Objects::SponsorsListing, "The Sponsors listing that match was disabled for", null: true

      def resolve(sponsors_listing:, reason:)
        unless context[:viewer].can_admin_sponsors_listings?
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to disable sponsors match"
        end

        begin
          sponsors_listing.disable_sponsors_match!(actor: context[:viewer], reason: reason)
          { sponsors_listing: sponsors_listing }
        rescue ActiveRecord::RecordInvalid => error
          raise Errors::Unprocessable.new("Failed to disable match: #{error.message}")
        end
      end
    end
  end
end
