# frozen_string_literal: true

module Platform
  module Mutations
    class AddPullRequestToMergeQueue < Platform::Mutations::Base
      description "Adds a pull request to a merge queue"
      areas_of_responsibility :pull_requests

      minimum_accepted_scopes ["repo_deployment"]
      visibility :internal

      argument :pull_request_id, ID, "The Node ID of the pull request to add.", required: true, loads: Objects::PullRequest, as: :pull_request
      argument :repository_id, ID, "The Node ID of the repository.", required: true, loads: Objects::Repository, as: :repository
      argument :branch, String, "Which branch's merge queue to add to.", required: true

      field :pull_request, Objects::PullRequest, "The pull request.", null: true
      field :merge_queue_entry, Objects::MergeQueueEntry, "The newly created merge queue entry", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repository:, **inputs)
        permission.async_owner_if_org(repository).then do |org|
          permission.access_allowed? :write_deployment,
            resource: repository,
            current_repo: repository,
            current_org: org,
            allow_integrations: true,
            allow_user_via_integration: true
        end
      end

      def resolve(pull_request:, repository:, branch:, **inputs)
        context[:permission].authorize_content(:deployment, :create, repository: repository)

        merge_queue = repository.merge_queue_for(branch: branch)
        entry = merge_queue.enqueue(pull_request: pull_request, enqueuer: context[:viewer])

        {
          pull_request: pull_request,
          merge_queue_entry: entry
        }
      end
    end
  end
end
