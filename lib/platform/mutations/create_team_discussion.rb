# frozen_string_literal: true

module Platform
  module Mutations
    class CreateTeamDiscussion < Platform::Mutations::Base
      description "Creates a new team discussion."

      minimum_accepted_scopes ["write:discussion"]

      argument :team_id, ID, "The ID of the team to which the discussion belongs.", required: true, loads: Objects::Team
      argument :title, String, "The title of the discussion.", required: true
      argument :body, String, "The content of the discussion.", required: true
      argument :private, Boolean, "If true, restricts the visiblity of this discussion to team members and organization admins. If false or not specified, allows any organization member to view this discussion.", required: false

      field :team_discussion, Objects::TeamDiscussion, "The new discussion.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, team:, **inputs)
        team.async_organization.then do |org|
          permission.access_allowed?(
            :create_team_discussion,
            resource: team,
            organization: org,
            current_repo: nil,
            allow_integrations: true,
            allow_user_via_integration: true)
        end
      end

      def resolve(team:, **inputs)
        if inputs[:private] && !(team.adminable_by?(context[:viewer]) ||
          Team.member_of?(team.id, context[:viewer].id))

          raise Errors::Forbidden.new(
            "#{context[:viewer]} can't create a private discussion without being a member of " +
            "#{team.name_with_owner}.")
        end

        raise Errors::Validation.new("Title can't be blank") if inputs[:title]&.strip.blank?

        discussion = team.discussion_posts.build(
          title: inputs[:title],
          body: inputs[:body],
          private: inputs[:private] || false,
          user: context[:viewer])

        if discussion.save
          { team_discussion: discussion }
        else
          raise Errors::Validation.new(discussion.errors.full_messages.to_sentence)
        end
      end
    end
  end
end
