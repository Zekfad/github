# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteBranchProtectionRule < Platform::Mutations::Base
      description "Delete a branch protection rule"

      minimum_accepted_scopes ["public_repo"]

      argument :branch_protection_rule_id, ID, "The global relay id of the branch protection rule to be deleted.", required: true, loads: Objects::BranchProtectionRule, as: :protected_branch

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, protected_branch:, **inputs)
        protected_branch.async_repository.then do |repository|
          permission.async_owner_if_org(repository).then do |org|
            permission.access_allowed? :update_branch_protection, resource: repository, current_repo: repository, current_org: org, allow_integrations: true, allow_user_via_integration: true
          end
        end
      end

      def resolve(protected_branch:, **inputs)
        protected_branch.async_repository.then do |repository|
          raise Errors::Forbidden.new("Upgrade to GitHub Pro or make this repository public to enable this feature.") unless repository.plan_supports?(:protected_branches)
          raise Errors::Forbidden.new("Branch protection deleting is disabled on this repository.") unless repository.can_update_protected_branches?(context[:viewer])

          protected_branch.destroy

          {}
        end
      end
    end
  end
end
