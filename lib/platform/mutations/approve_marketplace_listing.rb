# frozen_string_literal: true

module Platform
  module Mutations
    class ApproveMarketplaceListing < Platform::Mutations::Base
      description "Approve a Marketplace listing so that it becomes publicly visible."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The ID of the Marketplace listing to update.", required: true, loads: Objects::MarketplaceListing, as: :listing
      argument :message, String, "A custom message for the integrator.", required: false

      field :marketplace_listing, Objects::MarketplaceListing, "The updated marketplace listing.", null: true

      def resolve(listing:, **inputs)
        unless context[:viewer].can_admin_marketplace_listings?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to approve the listing.")
        end

        unless listing.can_approve?
          raise Errors::Validation.new("Listing cannot be approved.")
        end

        listing.approve!(context[:viewer], inputs[:message])

        { marketplace_listing: listing }
      end
    end
  end
end
