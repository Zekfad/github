# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateCustomInbox < Platform::Mutations::Base
      include Helpers::Newsies

      description "Update a custom inbox for the current viewer."
      minimum_accepted_scopes ["notifications"]
      visibility :under_development
      areas_of_responsibility :notifications

      argument :custom_inbox_id, ID, "The ID of the custom inbox to update.", required: true, loads: Objects::NotificationFilter
      argument :name, String, "The name of the new inbox.", required: false
      argument :query_string, String, "The search query used to filter the inbox.", required: false

      field :custom_inbox, Objects::NotificationFilter, "The updated custom inbox.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        permission.access_allowed?(
          :update_custom_inbox,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      def resolve(custom_inbox:, **inputs)
        updated_inbox = unpack_newsies_response!(
          GitHub.newsies.web.update_custom_inbox(id: custom_inbox.id, **inputs),
        )

        unless updated_inbox.valid?
          raise Errors::Unprocessable.new(updated_inbox.errors.full_messages.to_sentence)
        end

        { custom_inbox: updated_inbox }
      end
    end
  end
end
