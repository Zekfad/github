# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateNetworkPrivilege < Platform::Mutations::Base
      description "Updates a network privilege on a repository"

      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      argument :no_index, Boolean, "Whether or not the repo has been de-indexed by Google", required: false

      argument :require_login, Boolean, "Whether or not the repo has been locked to GitHub users only", required: false

      argument :require_opt_in, Boolean, "Whether or not the repo requires opt-in before viweing", required: false

      argument :collaborators_only, Boolean, "Whether or not the repo has been locked to collaborators only", required: false

      argument :is_hidden_from_discovery, Boolean, "Whether or not the repo has been hidden from discovery pages", required: false

      argument :repository_id, ID, "ID of the repository", required: true, loads: Objects::Repository

      field :repository, Objects::Repository, "The repository", null: true

      def resolve(repository:, **inputs)

        unless context[:viewer].site_admin?
          raise Errors::Forbidden.new("viewer must be staff")
        end

        raise Errors::Unprocessable.new("Repository must be public") unless repository.public?

        if inputs.key?(:no_index)
          repository.set_network_privilege(:noindex, inputs[:no_index])
        end

        if inputs.key?(:require_login)
          repository.set_network_privilege(:require_login, inputs[:require_login])
        end

        if inputs.key?(:require_opt_in)
          repository.set_network_privilege(:require_opt_in, inputs[:require_opt_in])
        end

        if inputs.key?(:collaborators_only)
          repository.set_network_privilege(:collaborators_only, inputs[:collaborators_only])
        end

        if inputs.key?(:is_hidden_from_discovery)
          repository.set_network_privilege(:hide_from_discovery, inputs[:is_hidden_from_discovery])
        end

        { repository: repository }
      end
    end
  end
end
