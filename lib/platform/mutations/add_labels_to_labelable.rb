# frozen_string_literal: true

module Platform
  module Mutations
    class AddLabelsToLabelable < Platform::Mutations::Base
      description "Adds labels to a labelable object."

      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :issues

      argument :labelable_id, ID, "The id of the labelable object to add labels to.", required: true, loads: Interfaces::Labelable
      argument :label_ids, [ID], "The ids of the labels to add.", required: true, loads: Objects::Label

      error_fields
      field :labelable, Interfaces::Labelable, "The item that was labeled.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, labelable:, **inputs)
        permission.async_repo_and_org_owner(labelable).then do |repo, org|
          permission.access_allowed?(:add_label, repo: repo, resource: labelable, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(labelable:, labels:, execution_errors:, **inputs)
        record = labelable
        issue = record.is_a?(PullRequest) ? record.issue : record

        unless issue.labelable_by?(actor: context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to add labels in this repository.")
        end

        issue.async_repository.then do |repository|
          if record.is_a?(Issue) && !repository.has_issues?
            raise Errors::Unprocessable::IssuesDisabled.new
          end

          context[:permission].authorize_content(:issue, :update, repo: repository)

          if @context[:permission].integration_user_request?
            issue.modifying_integration = @context[:integration]
          end

          issue.add_labels(labels)

          if issue.valid?
            {
              labelable: record,
              errors: [],
            }
          else
            Platform::UserErrors.append_legacy_mutation_model_errors_to_context(issue, execution_errors)

            {
              labelable: nil,
              errors: Platform::UserErrors.mutation_errors_for_model(issue),
            }
          end
        end
      end
    end
  end
end
