# frozen_string_literal: true

module Platform
  module Mutations
    class DisableRepositories < Platform::Mutations::Base
      description "Disable repositories. ex for TOS violations"

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :repository_ids, [ID], "The global relay ids of repositories to disable.", required: true, loads: Objects::Repository, as: :repositories
      argument :reason, String, "Reason for disabling the repositories", required: true
      argument :instructions, String, "Message emailed to owner", required: false

      field :repositories, [Objects::Repository], "The disabled repositories.", null: true

      def resolve(repositories:, **inputs)
        unless self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to disable repositories."
        end

        repositories.map(&:access).each do |repository_access|
          next if repository_access.disabled?
          repository_access.disable(
            inputs[:reason],
            context[:viewer],
            instructions: inputs[:instructions],
          )
        end

        { repositories: repositories }
      end
    end
  end
end
