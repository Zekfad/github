# frozen_string_literal: true

module Platform
  module Mutations
    class DisableFeatureGroup < Platform::Mutations::Base
      description "Adds a feature flag for a group."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :feature_id, ID, "The id of the feature flag.", required: true, loads: Objects::Feature
      argument :group, Enums::FeatureGroup, "The feature group to disable", required: true

      field :feature, Objects::Feature, "The feature", null: true

      def resolve(feature:, **inputs)

        if feature
          feature.disable_group(inputs[:group])
          { feature: feature }
        else
          raise Errors::Unprocessable.new("Could not disable group for feature.")
        end
      end
    end
  end
end
