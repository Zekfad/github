# frozen_string_literal: true

module Platform
  module Mutations
    class CreateMobilePushNotificationSchedules < Platform::Mutations::Base
      include Helpers::Newsies

      description "Creates mobile push notification schedules."
      minimum_accepted_scopes ["user"]
      feature_flag :pe_mobile
      areas_of_responsibility :notifications

      argument :days, [Enums::DayOfWeek], "The days of the week for the schedule.", required: true
      argument :starts_at, Scalars::DateTime, "The start time for the schedule.", required: true
      argument :ends_at, Scalars::DateTime, "The end time for the schedule.", required: true

      error_fields
      field :mobile_push_notification_schedules, [Objects::MobilePushNotificationSchedule], "The new mobile push notification schedules.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, _)
        permission.access_allowed?(
          :update_user_notification_settings,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      def resolve(days:, starts_at:, ends_at:, execution_errors:)
        if days.uniq != days
          return {
            mobile_push_notification_schedules: [],
            errors: [{
              "path" => ["input", "days"],
              "message" => "Multiple schedules cannot be created for the same day",
            }],
          }
        end

        attributes = days.map do |day|
          {
            day: day,
            start_time: starts_at,
            end_time: ends_at,
            user_id: @context[:viewer].id
          }
        end

        schedules = handle_newsies_service_unavailable do
          ::Newsies::MobilePushNotificationSchedule.create(attributes)
        end

        valid_schedules = schedules.select(&:persisted?)
        invalid_schedules = schedules - valid_schedules
        error_messages = invalid_schedules.map { |schedule| schedule.errors.full_messages.join(", ") }

        return {
          mobile_push_notification_schedules: valid_schedules.map(&:reload),
          errors: invalid_schedules.map do |schedule|
            Platform::UserErrors.mutation_errors_for_model(schedule, translate: { day: "days" })
          end.flatten
        }
      end
    end
  end
end
