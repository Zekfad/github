# frozen_string_literal: true

module Platform
  module Mutations
    class ClassifyAccountsAsHammy < Platform::Mutations::Base
      description "Classify a set of accounts as hammy."

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :account_ids, [ID], "The global relay id of accounts to classify hammy.", required: true, loads: Unions::Account
      argument :origin, String, "The system that performed this classification.", required: false, default_value: "origin_unknown"
      argument :rule_name, String, "The rule that performed this classification.", required: false
      argument :rule_version, String, "The rule that performed this classification.", required: false

      field :accounts, [Unions::Account], "The accounts marked hammy.", null: true
      argument :instrument_abuse_classification, Boolean, "Instrument the abuse classification hydro event.", required: false, default_value: true

      def resolve(accounts:, **inputs)
        unless self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to classify accounts hammy."
        end

        accounts.each do |account|
          account.whitelist_from_spam_flag(
            actor: context[:viewer],
            origin: inputs[:origin],
            instrument_abuse_classification: inputs[:instrument_abuse_classification],
            rule_name: inputs[:rule_name],
            rule_version: inputs[:rule_version],
          )
        end

        { accounts: accounts }
      end
    end
  end
end
