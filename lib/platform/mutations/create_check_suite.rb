# frozen_string_literal: true

module Platform
  module Mutations
    class CreateCheckSuite < Platform::Mutations::Base
      class NoPushError < StandardError; end

      include Platform::Helpers::CommitValidation

      description "Create a check suite"

      minimum_accepted_scopes ["public_repo"]

      limit_actors_to [:github_app]

      argument :repository_id, ID, "The Node ID of the repository.", required: true, loads: Objects::Repository, as: :repo
      argument :name, String, "The creator-supplied name for the CheckSuite. Currently only used by GitHub Actions, where it is the workflow name.", required: false, visibility: :internal
      argument :event, String, "The event that prompted this check suite, e.g 'pull_request'. Only used by GitHub Actions.", required: false, visibility: :internal
      argument :action, String, "The action of the webhook event, e.g 'merged', 'closed', 'reopened'. Only used by GitHub Actions.", required: false, visibility: :internal
      argument :head_sha, Scalars::GitObjectID, "The SHA of the head commit.", required: true
      argument :head_branch, String, "The branch name associated with this check suite.", required: false, visibility: :internal
      argument :head_repository_id, ID, "The Node ID of the repository where the head_sha belongs to.", required: false, visibility: :internal
      argument :rerequestable, Boolean, "Whether or not the check suite should be re-requestable.", visibility: :under_development, required: false, default_value: true
      argument :check_runs_rerunnable, Boolean, "Whether ot not individual check runs in the suite should be re-runnable.", visibility: :under_development, required: false, default_value: true
      argument :explicit_completion, Boolean, "Wheather or not the conclusion must be calculated as soon as all existing checks have completed.", visibility: :internal, required: false
      argument :creator_id, ID, "The Node ID of the user (could be a bot) that triggered the check suite creation.", visibility: :internal, required: false, loads: Unions::Account
      argument :external_id, String, "Used by integrators with which can create >1 check suite per sha to create suites idempotently", visibility: :internal, required: false
      argument :annotations, [Inputs::CheckAnnotationData], "The annotations that are made as part of the check suite.", visibility: :internal, required: false, default_value: []
      argument :workflow_file_path, String, "The workflow file path", visibility: :internal, required: false
      argument :trigger_id, ID, "The entity that triggered the workflow run.", visibility: :internal, required: false, loads: Interfaces::Trigger, as: :trigger

      error_fields
      field :check_suite, Objects::CheckSuite, "The newly created check suite.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repo:, **inputs)
        permission.async_owner_if_org(repo).then do |org|
          permission.access_allowed?(:write_check_suite, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(repo:, execution_errors:, **inputs)
        head_sha = inputs[:head_sha]
        head_repository_id = inputs[:head_repository_id]

        if head_repository_id
          # The head repository may be a fork to which the GitHub Actions app
          # token doesn't have direct access, so we look up directly by
          # database ID.
          _, repo_id = Platform::Helpers::NodeIdentification.from_global_id(head_repository_id)
          head_repo = Repository.find_by!(id: repo_id)
        end

        find_commit!(repo, head_sha)

        annotations = inputs[:annotations] || []

        if annotations.size > Inputs::CheckAnnotationData::MAXIMUM_PER_REQUEST
          return {
            check_suite: nil,
            errors: [{
              path: ["input", "annotations"],
              message: "Annotations exceeds a maximum quantity of #{Inputs::CheckAnnotationData::MAXIMUM_PER_REQUEST}",
            }],
          }
        end

        push = Push.where(after: head_sha, repository_id: (head_repo || repo).id).first
        head_branch = inputs[:head_branch].presence || push&.branch_name

        new_check_suite_attrs = {
          name: inputs[:name],
          event: inputs[:event],
          action: inputs[:action],
          push: push,
          head_sha: head_sha,
          head_branch: head_branch,
          head_repository_id: head_repo&.id,
          github_app: context[:integration],
          repository: repo,
          external_id: inputs[:external_id],
          rerequestable: inputs.key?(:rerequestable) ? inputs[:rerequestable] : true,
          check_runs_rerunnable: inputs.key?(:check_runs_rerunnable) ? inputs[:check_runs_rerunnable] : true,
          explicit_completion: inputs.key?(:explicit_completion) ? inputs[:explicit_completion] : false,
          creator: inputs[:creator],
          workflow_file_path: inputs[:workflow_file_path],
          trigger: inputs[:event] == "push" ? push : inputs[:trigger],
        }

        new_check_suite_attrs[:annotations] = annotations.map do |annotation|
          CheckAnnotation.new(Inputs::CheckAnnotationData.hash_from_input(annotation))
        end

        result = CheckSuite.find_or_create_for_integrator(new_check_suite_attrs)
        if result.success?
          { check_suite: result.record, errors: [] }
        else
          if result.duplicate_for_sha?
            raise Platform::Errors::Unprocessable.new("A check suite already exists for this sha")
          end
          Platform::UserErrors.append_legacy_mutation_model_errors_to_context(result.record, execution_errors)
          { check_suite: nil, errors: Platform::UserErrors.mutation_errors_for_model(result.record)  }
        end
      end
    end
  end
end
