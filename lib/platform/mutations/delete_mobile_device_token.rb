# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteMobileDeviceToken < Platform::Mutations::Base
      include Helpers::Newsies

      description "Delete a mobile device token."
      minimum_accepted_scopes ["user"]
      mobile_only true
      areas_of_responsibility :notifications

      argument :service, Enums::PushNotificationService, "The push notification service that issued the device token.", required: true
      argument :device_token, String, "The device token to delete.", required: true

      field :success, Boolean, "Did the operation succeed?", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, *_)
        permission.access_allowed?(
          :update_user_notification_settings,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      def resolve(service:, device_token:)
        existing_token = unpack_newsies_response!(
          GitHub.newsies.mobile_device_token(context[:viewer].id, device_token, service: service),
        )

        unless existing_token
          raise Platform::Errors::NotFound.new(
            "Could not find #{service} device token for current viewer.",
          )
        end

        destroyed_token = unpack_newsies_response!(
          GitHub.newsies.destroy_mobile_device_token(existing_token.id),
        )

        unless destroyed_token.destroyed?
          raise Errors::Unprocessable.new(destroyed_token.errors.full_messages.to_sentence)
        end

        { success: true }
      end
    end
  end
end
