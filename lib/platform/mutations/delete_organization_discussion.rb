# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteOrganizationDiscussion < Platform::Mutations::Base
      description "Deletes an organization discussion."
      minimum_accepted_scopes ["write:discussion"]
      visibility :under_development
      areas_of_responsibility :orgs

      argument :id, ID, "The discussion ID to delete.", required: true, loads: Objects::OrganizationDiscussion, as: :discussion

      def resolve(discussion:, **inputs)
        unless discussion.async_viewer_can_delete?(context[:viewer]).sync
          message = (
            "#{context[:viewer]} does not have permission to delete the discussion " +
            "'#{discussion.global_relay_id}'.")
          raise Errors::Forbidden.new(message)
        end

        unless discussion.destroy
          raise Errors::Validation.new(discussion.errors.full_messages.to_sentence)
        end

        {}
      end
    end
  end
end
