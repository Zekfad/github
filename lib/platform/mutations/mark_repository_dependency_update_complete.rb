# frozen_string_literal: true

module Platform
  module Mutations
    class MarkRepositoryDependencyUpdateComplete < Platform::Mutations::Base
      description "Updates the status of a RepositoryDependencyUpdate."

      areas_of_responsibility :dependabot

      visibility :internal, environments: [:dotcom]
      minimum_accepted_scopes ["repo"]

      argument :database_id, Integer, "The database id of Repository Dependency Update that has completed.", required: true
      argument :pull_request_number, Integer, "The Pull Request number to associate with the update object.", required: true

      error_fields

      # This mutation should only be available to the Dependabot GitHub App
      def self.async_api_can_modify?(permission, **inputs)
        permission.viewer == GitHub.dependabot_github_app.bot
      end

      def resolve(database_id:, pull_request_number:, **inputs)
        repository_dependency_update = Loaders::ActiveRecord.load(::RepositoryDependencyUpdate, database_id).sync

        unless repository_dependency_update
          raise Errors::Unprocessable.new("Update ID '#{database_id}' does not exist.")
        end

        unless repository_dependency_update.repository
          raise Errors::Unprocessable.new("Repository ID '#{repository_dependency_update.repository_id}' does not exist.")
        end

        unless repository_dependency_update.repository.dependabot_installed?
          raise Errors::Unprocessable.new("The Dependabot app is not installed on Repository ID '#{repository_dependency_update.repository_id}'.")
        end

        pull_request = load_pull_request(repository_dependency_update.repository_id,
                                         pull_request_number)

        unless pull_request
          raise Errors::Unprocessable.new("Pull Request ##{pull_request_number} does not exist.")
        end

        if repository_dependency_update.complete?
          raise Errors::Unprocessable.new("Update ID '#{repository_dependency_update.id}' has already been marked as '#{repository_dependency_update.state}'.")
        end

        repository_dependency_update.mark_as_complete(pull_request: pull_request)

        {
          errors: Platform::UserErrors.mutation_errors_for_model(repository_dependency_update),
        }
      end

      private

      def load_pull_request(repository_id, number)
        Loaders::PullRequestByNumber.load(repository_id, number).sync
      end
    end
  end
end
