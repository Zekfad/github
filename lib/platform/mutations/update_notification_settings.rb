# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateNotificationSettings < Platform::Mutations::Base
      include Helpers::Newsies

      description "Updates the user's notification settings."
      minimum_accepted_scopes ["user"]
      feature_flag :pe_mobile
      areas_of_responsibility :notifications

      argument :gets_participating_web,
        Boolean,
        "Does the viewer get web notifications for threads in which they are participating?",
        required: false

      argument :gets_watching_web,
        Boolean,
        "Does the viewer get web notifications for threads which they are watching or to which they are subscribed?",
        required: false

      argument :gets_vulnerability_alerts_web,
        Boolean,
        "Does the viewer get web notifications for vulnerability alerts?",
        required: false

      argument :gets_direct_mention_mobile_push,
        Boolean,
        "Does the viewer get mobile push notifications for comments in which they are directly mentioned?",
        required: false

      field :success, Boolean, "Did the operation succeed?", null: true
      field :viewer, Objects::User, "The user whose settings were updated.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        user = permission.viewer
        permission.access_allowed?(
          :update_user_notification_settings,
          resource: user,
          current_org: nil,
          current_repo: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      def resolve(**inputs)
        viewer = context[:viewer]
        gets_participating_web = inputs[:gets_participating_web]
        gets_watching_web = inputs[:gets_watching_web]
        gets_vulnerability_alerts_web = inputs[:gets_vulnerability_alerts_web]
        gets_direct_mention_mobile_push = inputs[:gets_direct_mention_mobile_push]

        response = GitHub.newsies.get_and_update_settings(viewer) do |settings|
          settings.subscribed_web = gets_watching_web unless gets_watching_web.nil?
          settings.participating_web = gets_participating_web unless gets_participating_web.nil?
          settings.vulnerability_web = gets_vulnerability_alerts_web unless gets_vulnerability_alerts_web.nil?
          settings.vulnerability_web = gets_vulnerability_alerts_web unless gets_vulnerability_alerts_web.nil?
          settings.direct_mention_mobile_push = gets_direct_mention_mobile_push unless gets_direct_mention_mobile_push.nil?
        end

        unpack_newsies_response!(response)

        {
          success: true,
          viewer: context[:viewer],
        }
      end
    end
  end
end
