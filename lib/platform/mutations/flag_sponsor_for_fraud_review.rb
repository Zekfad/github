# frozen_string_literal: true

module Platform
  module Mutations
    class FlagSponsorForFraudReview < Platform::Mutations::Base
      description "Flags a sponsor for manual fraud review"
      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      argument :sponsors_listing_id,
        ID,
        description: "ID of the listing to flag this sponsor for",
        loads: Objects::SponsorsListing,
        required: true

      argument :sponsor_id,
        ID,
        description: "ID of the sponsor to flag",
        loads: Interfaces::Sponsorable,
        required: true

      argument :matched_current_client_id,
        String,
        description: "The matching client ID, if the sponsor's current client ID matched one of the sponsorable's historical client IDs.",
        required: false

      argument :matched_current_ip,
        String,
        description: "The matching IP address, if the sponsor's current IP address matched one of the sponsorable's historical IP addresses.",
        required: false

      argument :matched_historical_ip,
        String,
        description: "The matching IP address, if the sponsor's historical IP addresses matched one of the sponsorable's historical IP addresses.",
        required: false

      argument :matched_historical_client_id,
        String,
        description: "The matching client ID, if the sponsor's historical client IDs matched one of the sponsorable's historical client IDs.",
        required: false

      argument :matched_current_ip_region_and_user_agent,
        String,
        description: "The matching IP region and user agent, if the sponsor's current IP region and user agent matched the sponsorable's.",
        required: false

      field :fraud_flagged_sponsor, Objects::FraudFlaggedSponsor, description: "The created fraud flag entry.", null: true

      def resolve(sponsors_listing:, sponsor:, **inputs)
        unless context[:viewer].can_admin_sponsors_listings?
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to flag sponsors"
        end

        FraudFlaggedSponsor.retry_on_find_or_create_error do
          fraud_review = sponsors_listing.sponsors_fraud_reviews.find_by(state: :pending)
          fraud_review ||= sponsors_listing.sponsors_fraud_reviews.build
          entry = fraud_review.fraud_flagged_sponsors.find_by(sponsor: sponsor)
          entry ||= fraud_review.fraud_flagged_sponsors.build(sponsor: sponsor)

          entry.matched_current_client_id = inputs[:matched_current_client_id] if inputs[:matched_current_client_id].present?
          entry.matched_current_ip = inputs[:matched_current_ip] if inputs[:matched_current_ip].present?
          entry.matched_historical_ip = inputs[:matched_historical_ip] if inputs[:matched_historical_ip].present?
          entry.matched_historical_client_id = inputs[:matched_histoical_client_id] if inputs[:matched_histoical_client_id].present?
          entry.matched_current_ip_region_and_user_agent = inputs[:matched_current_ip_region_and_user_agent] if inputs[:matched_current_ip_region_and_user_agent].present?

          if fraud_review.save
            { fraud_flagged_sponsor: entry }
          else
            raise Errors::Unprocessable.new(fraud_review.errors.full_messages.to_sentence)
          end
        end
      end
    end
  end
end
