# frozen_string_literal: true

module Platform
  module Mutations
    class ApproveSponsorsListing < Platform::Mutations::Base
      description "Approve a Sponsors listing so that it becomes publicly visible."

      visibility :internal
      minimum_accepted_scopes ["user"]

      argument :id, ID, "The ID of the Sponsors listing to approve.", required: true, loads: Objects::SponsorsListing, as: :listing

      field :sponsors_listing, Objects::SponsorsListing, "The updated Sponsors listing.", null: true

      def resolve(listing:, **inputs)
        result = Sponsors::ApproveSponsorsListing.call(
          sponsors_listing: listing,
          actor: context[:viewer],
        )

        if result.success?
          { sponsors_listing: result.sponsors_listing }
        else
          raise result.platform_error.new(result.errors.to_sentence) # rubocop:disable GitHub/UsePlatformErrors
        end
      end
    end
  end
end
