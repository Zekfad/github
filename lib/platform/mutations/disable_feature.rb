# frozen_string_literal: true

module Platform
  module Mutations
    class DisableFeature < Platform::Mutations::Base
      description "Disables a feature flag."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :id, ID, "The id of the feature flag.", required: true, loads: Objects::Feature, as: :feature

      field :feature, Objects::Feature, "The feature being disabled.", null: true

      def resolve(feature:, **inputs)

        if feature && feature.disable
          { feature: feature }
        else
          raise Errors::Unprocessable.new("Could not disable feature.")
        end
      end
    end
  end
end
