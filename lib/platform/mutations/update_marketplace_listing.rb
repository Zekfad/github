# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateMarketplaceListing < Platform::Mutations::Base
      description "Allows updating a Marketplace listing."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :slug, String, "Select the listing that matches this slug. It's the short name of the listing used in its URL.", required: true
      argument :logo_background_color, String, "The hex color code, without the leading '#', for the logo background.", required: false
      argument :name, String, "The listing's full name.", required: false
      argument :short_description, String, "The listing's short description.", required: false
      argument :full_description, String, "The listing's full description.", required: false
      argument :extended_description, String, "The listing's extended description.", required: false
      argument :how_it_works, String, "A technical description of how this listing's application works with GitHub.", required: false
      argument :privacy_policy_url, String, "A link to the listing's privacy policy.", required: false
      argument :terms_of_service_url, String, "A link to the listing's terms of service.", required: false
      argument :company_url, String, "A link to the listing owner's company site.", required: false
      argument :status_url, String, "A link to the listing's status page.", required: false
      argument :support_url, String, "A link to the listing's support site.", required: false
      argument :documentation_url, String, "A link to the listing's documentation.", required: false
      argument :pricing_url, String, "A link to the listing's detailed pricing.", required: false
      argument :installation_url, String, "A link to install the listing's application.", required: false
      argument :technical_email, String, "Email address for the listing's technical contact.", required: false
      argument :marketing_email, String, "Email address for the listing's marketing contact.", required: false
      argument :finance_email, String, "Email address for the listing's finance contact.", required: false
      argument :security_email, String, "Email address for the listing's security contact.", required: false
      argument :primary_category_name, String, "The name of the category that best describes the listing.", required: false
      argument :secondary_category_name, String, <<~DESCRIPTION, required: false
          The name of an alternate category that describes the listing. Pass 'none' to remove the
          secondary category from this listing.
        DESCRIPTION

      argument :filter_categories, [String], "A list of filter-type categories.", required: false, visibility: :internal

      argument :is_light_text, Boolean, "Whether light text is used when displaying text on the hero card.", required: false
      argument :is_by_github, Boolean, "Whether this listing is owned by GitHub", required: false, visibility: :internal
      argument :has_direct_billing, Boolean, "Whether the listing can create direct billing plans.", required: false, visibility: :internal
      argument :supported_language_names, [String, null: true], "The names of supported programming languages.", required: false
      argument :oauthApplicationDatabaseID, Integer, <<~DESCRIPTION, required: false, as: :oauth_application_database_id
        The ID of the OAuth application to list, if you want to change the backing product for this
        listing. Can only be modified by site admins.
      DESCRIPTION
      argument :appID, ID, <<~DESCRIPTION, required: false, loads: Objects::App, as: :app
        The ID of the integration to list, if you want to change the backing product for this
        listing. Can only be modified by site admins.
      DESCRIPTION
      argument :heroCardBackgroundImageDatabaseID, Integer, visibility: :internal, description: "The database ID of the hero card background image.", required: false,
        as: :hero_card_background_image_database_id
      argument :featured_at, Scalars::DateTime, visibility: :internal, description: "Identifies the date and time when the listing is featured on the Marketplace homepage.", required: false

      field :marketplace_listing, Objects::MarketplaceListing, "The updated marketplace listing.", null: true

      def resolve(**inputs)
        listing = Loaders::ActiveRecord.load(::Marketplace::Listing, inputs[:slug],
                                             column: :slug).sync

        unless listing.present?
          raise Errors::NotFound.new("No listing found with slug #{inputs[:slug]}.")
        end

        viewer_can_edit = listing.allowed_to_edit?(context[:viewer])
        viewer_is_admin = context[:viewer].can_admin_marketplace_listings?

        unless viewer_can_edit || viewer_is_admin
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to update the listing.")
        end

        attrs = {}

        primary_category = if inputs[:primary_category_name].present?
          category = Loaders::ActiveRecord.load(::Marketplace::Category,
                                                inputs[:primary_category_name], column: :name).sync
          unless category
            raise Errors::Validation.new("No such Marketplace category exists: " +
                                         inputs[:primary_category_name])
          end
          category
        end

        secondary_category = if inputs[:secondary_category_name] == "none"
          "none"
        elsif inputs[:secondary_category_name].present?
          category = Loaders::ActiveRecord.load(::Marketplace::Category,
                                                inputs[:secondary_category_name], column: :name).sync
          unless category
            raise Errors::Validation.new("No such Marketplace category exists: " +
                                         inputs[:secondary_category_name])
          end
          category
        end

        # Integrators can update text fields, URLs, supported languages, and categories
        if listing.allowed_to_edit?(context[:viewer])
          model_and_input_attributes = {
            bgcolor: :logo_background_color,
            short_description: :short_description,
            name: :name,
            full_description: :full_description,
            extended_description: :extended_description,
            privacy_policy_url: :privacy_policy_url,
            tos_url: :terms_of_service_url,
            company_url: :company_url,
            status_url: :status_url,
            support_url: :support_url,
            technical_email: :technical_email,
            marketing_email: :marketing_email,
            finance_email: :finance_email,
            security_email: :security_email,
            documentation_url: :documentation_url,
            pricing_url: :pricing_url,
            installation_url: :installation_url,
            how_it_works: :how_it_works,
            hero_card_background_image_id: :hero_card_background_image_database_id,
          }

          model_and_input_attributes.each do |model_attr, input_attr|
            if inputs[input_attr]
              listing.assign_attributes(model_attr => inputs[input_attr].to_s)
            end
          end

          unless inputs[:is_light_text].nil?
            listing.light_text = inputs[:is_light_text]
          end

          if inputs[:supported_language_names].present?
            supported_language_names = inputs[:supported_language_names]

            supported_languages = LanguageName.lookup_by_names(supported_language_names).compact

            listing.languages = supported_languages
          end
        end

        # Bizdevs have ability to change attributes that listing owners do not
        if viewer_is_admin
          listing.skip_draft_validation = true

          if inputs[:oauth_application_database_id]
            attrs[:listable_type] = OauthApplication.name
            attrs[:listable_id] = inputs[:oauth_application_database_id]
          elsif inputs[:app]
            attrs[:listable] = inputs[:app]
          end

          if inputs.key?(:has_direct_billing)
            listing.direct_billing_enabled = inputs[:has_direct_billing]
          end

          if inputs.key?(:is_by_github)
            listing.by_github = inputs[:is_by_github]
          end

          if inputs.key?(:featured_at)
            listing.featured_at = inputs[:featured_at]
            updated_featured_at = listing.featured_at_changed?
          end
        end

        original_categories = listing.regular_categories.to_a
        original_filters = listing.filter_categories.to_a

        new_categories = []
        if primary_category
          new_categories << primary_category
          listing.primary_category_id = primary_category.id
        else
          new_categories << original_categories.first
        end

        if secondary_category == "none"
          listing.secondary_category_id = nil
        elsif secondary_category
          new_categories << secondary_category
          listing.secondary_category_id = secondary_category.id
        elsif original_categories.size > 1
          new_categories << original_categories.second
        end

        new_filters = original_filters
        if viewer_is_admin && inputs.key?(:filter_categories)
          filter_categories = inputs[:filter_categories].reject(&:blank?).map do |filter_name|
            Loaders::ActiveRecord
              .load(::Marketplace::Category, filter_name, column: :name)
              .sync
          end

          new_filters = filter_categories.compact
        end

        # NOTE: we care about the order of regular categories to make the
        # primary vs secondary distinction. Filters can be in any order.
        categories_updated = (new_categories.map(&:id) != original_categories.map(&:id)) ||
          (new_filters.map(&:id).sort != original_filters.map(&:id).sort)

        listing.categories = new_categories + new_filters if categories_updated
        listing.assign_attributes(attrs)

        if listing.save
          # Create an audit log event to show a bizdev changed the category on a listing
          if categories_updated && viewer_is_admin
            listing.instrument_category_changed(actor: context[:viewer])
          end

          if updated_featured_at
            listing.instrument_featured_at_changed(actor: context[:viewer])
          end

          { marketplace_listing: listing }
        else
          errors = listing.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not update the listing: #{errors}")
        end
      end
    end
  end
end
