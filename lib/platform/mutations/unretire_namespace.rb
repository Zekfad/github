# frozen_string_literal: true

module Platform
  module Mutations
    class UnretireNamespace < Platform::Mutations::Base
      description "Unretires a namespace making it available to users."

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :owner_login, String, "The owning user or organization's login", required: true
      argument :name, String, "The retired repository name", required: true

      field :owner, Objects::User, "The owning user or organization", null: true

      def resolve(**inputs)
        unless context[:viewer].site_admin?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to unretire a namespace.")
        end

        retired = RetiredNamespace.find_by owner: inputs[:owner_login].downcase, name: inputs[:name].downcase
        namespace = [inputs[:owner_login], inputs[:name]].map(&:downcase).join("/")
        raise Errors::Unprocessable.new("Namespace '#{namespace}' is not retired") unless retired

        { owner: retired.owner } if retired.destroy
      end
    end
  end
end
