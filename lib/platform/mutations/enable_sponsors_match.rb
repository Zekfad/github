# frozen_string_literal: true

module Platform
  module Mutations
    class EnableSponsorsMatch < Platform::Mutations::Base
      description "Enables match for the specified Sponsors listing"

      visibility :internal
      minimum_accepted_scopes ["biztools"]

      argument :sponsors_listing_id, ID, "The ID of the Sponsors listing to enable match for", required: true, loads: Objects::SponsorsListing

      field :sponsors_listing, Objects::SponsorsListing, "The Sponsors listing that match was enabled for", null: true

      def resolve(sponsors_listing:)
        unless context[:viewer].can_admin_sponsors_listings?
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to enable sponsors match"
        end

        begin
          sponsors_listing.enable_sponsors_match!(actor: context[:viewer])
          { sponsors_listing: sponsors_listing }
        rescue ActiveRecord::RecordInvalid => error
          raise Errors::Unprocessable.new("Failed to enable match: #{error.message}")
        end
      end
    end
  end
end
