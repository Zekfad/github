# frozen_string_literal: true
module Platform
  class VisitorWithPathTracking < GraphQL::Language::Visitor
    def initialize(document)
      super

      @current_path = []

      push_node = -> (node, parent) { @current_path << node.name }
      pop_node = -> (node, parent) { @current_path.pop }

      node_types = [
        GraphQL::Language::Nodes::ObjectTypeDefinition,
        GraphQL::Language::Nodes::InterfaceTypeDefinition,
        GraphQL::Language::Nodes::FieldDefinition,
        GraphQL::Language::Nodes::InputValueDefinition,
        GraphQL::Language::Nodes::UnionTypeDefinition,
        GraphQL::Language::Nodes::ScalarTypeDefinition,
        GraphQL::Language::Nodes::EnumTypeDefinition,
        GraphQL::Language::Nodes::EnumValueDefinition,
        GraphQL::Language::Nodes::InputObjectTypeDefinition,
      ]

      node_types.each do |node_type|
        self[node_type].enter << push_node
        self[node_type].leave << pop_node
      end

      self[GraphQL::Language::Nodes::DirectiveDefinition].enter << -> (node, parent) { @current_path << "@#{node.name}" }
      self[GraphQL::Language::Nodes::DirectiveDefinition].leave << pop_node

      self[GraphQL::Language::Nodes::Directive].enter << -> (node, parent) { @current_path << "@#{node.name}" }
      self[GraphQL::Language::Nodes::Directive].leave << pop_node
    end

    def current_path
      @current_path.join(".")
    end
  end
end
