# frozen_string_literal: true

module Platform
  module Helpers
    class FeatureHistoryQuery
      def initialize(feature, viewer:)
        @feature = feature
        @viewer = viewer
      end

      def query_hash
        {
          phrase: "data.feature_name:#{@feature.name}",
          current_user: @viewer,
        }
      end

      def query
        @query ||= ::Audit::Driftwood::Query.new_stafftools_query(query_hash)
      end
    end
  end
end
