# frozen_string_literal: true

module Platform
  module Helpers
    module DelegatedAccountRecovery
      def self.record_recovery_stats(response)
        tags = case response[:validation_result]
        when :signature_only_success
          ["recover_account_return:success", "earthsmoke:unavailable_error"]
        when *::DelegatedAccountRecoveryController::RECOVERY_SUCCESS_STATES
          ["recover_account_return:success"]
        when :unverifiable_token
          ["recover_account_return:invalid_token"]
        when :secret_user_id_mismatch, :earthsmoke_secret_user_id_mismatch
          ["recover_account_return:secret_user_id_mismatch"]
        when :no_confirmed_tokens_matched
          ["recover_account_return:no_confirmed_tokens_matched"]
        when :earthsmoke_disabled
          ["recover_account_return:earthsmoke_disabled"]
        when :bad_legacy_decode
          ["recover_account_return:legacy_bad_decode"]
        when :earthsmoke_server_error
          ["recover_account_return:earthsmoke_server_error", "earthsmoke:server_error"]
        else
          raise Errors::Validation.new("Unknown result: #{response[:validation_result]}/")
        end

        GitHub.dogstats.increment("delegated_account_recovery_token", tags: tags)
      end

      # attempts to decode the secret in the verified token.
      #
      # parsed_token: a Darrrr::RecoveryToken instance
      # countersigned_token_id: the ID of the token that wrapped parsed_token
      # persisted_token: the DelegatedRecoveryToken model instance associated with parsed_token
      #
      # returns a symbol representing the result of the secret verification
      def self.verify_secret(parsed_token, countersigned_token, persisted_token)
        begin
          decoded_secret = parsed_token.decode(persisted_token: persisted_token)
        rescue Darrrr::DelegatedRecoveryError
          return :bad_legacy_decode
        rescue ::Earthsmoke::UnavailableError, ::Earthsmoke::TimeoutError
          return :signature_only_success
        rescue GRPC::BadStatus
          return :earthsmoke_server_error
        end

        if persisted_token.user_id.to_s == decoded_secret
          persisted_token.mark_recovered(countersigned_token.options, countersigned_token.token_id.to_hex)
          if countersigned_token.options == DelegatedRecoveryToken::LOW_FRICTION_PROOF
            GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["recovery:low_friction"])
          else
            GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["recovery:standard"])
          end

          if persisted_token.legacy_crypto?
            :legacy_success
          else
            :success
          end
        else
          # indicator of attack
          persisted_token.instrument(:recover_error, countersigned_token_id: countersigned_token.token_id.to_hex)

          if persisted_token.legacy_crypto?
            :secret_user_id_mismatch
          else
            :earthsmoke_secret_user_id_mismatch
          end
        end
      end
    end
  end
end
