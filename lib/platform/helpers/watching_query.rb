# frozen_string_literal: true

module Platform
  module Helpers
    class WatchingQuery

      attr_accessor :cursor, :cursor_direction, :limit, :viewer, :user
      attr_reader :model_to_be_queried

      def initialize(viewer, user)
        @viewer = viewer
        @user = user
        @model_to_be_queried = ::Repository
        @response = nil
      end

      def fetch!
        list_type = "Repository"
        lists_to_exclude = excluded_repo_ids.map { |id|
          ::Newsies::List.new(list_type, id)
        }
        options = pagination_args.merge({
          list_type: list_type,
          excluding: lists_to_exclude,
        })
        @response = GitHub.newsies.subscriptions_by_cursor(user, options)

        if @response.failed?
          raise Platform::Errors::ServiceUnavailable.new("Watchers and watching features are currently unavailable. Please try again later.")
        else
          @response.map(&:list_id)
        end
      end

      def count
        fetch! unless @response
        @response.count
      end

      private

      def excluded_repo_ids
        repository_ids_to_hide = if viewer
          if viewer.using_auth_via_integration?
            integration  = viewer.oauth_access.application
            installation = viewer.oauth_access.installation

            accessible_repo_ids_for_integration = integration.accessible_repository_ids(
              current_integration_installation: installation,
              repository_ids: viewer.associated_repository_ids,
              permissions: Repository::Resources.subject_types,
            )

            user.associated_repository_ids - accessible_repo_ids_for_integration
          else
            user.associated_repository_ids - viewer.associated_repository_ids
          end
        else
          user.associated_repository_ids
        end

        Repository.with_ids(repository_ids_to_hide).private_scope.ids
      end

      def pagination_args
        {
          pagination_type: :cursor,
          cursor: cursor,
          cursor_direction:  cursor_direction,
          limit: limit,
        }
      end
    end
  end
end
