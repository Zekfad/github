# frozen_string_literal: true

module Platform
  module Helpers
    module NodeIdentification
      class << self
        def type_name_from_object(object)
          object.platform_type_name
        end

        def from_global_id(global_id)
          decoded_global_id = nil
          malformed_global_id = false
          begin
            decoded_global_id = Base64.strict_decode64(global_id)
          rescue ArgumentError
            malformed_global_id = true
            decoded_global_id = Base64.decode64(global_id)
          end

          length_string, rest = decoded_global_id.split(":", 2)

          unless rest
            raise Platform::Errors::NotFound, "Could not resolve to a node with the global id of '#{global_id}'"
          end

          if malformed_global_id
            GitHub.dogstats.increment("platform.malformed_global_id")
          end

          length = length_string.to_i

          [rest[0..length - 1], rest[length..-1]]
        end

        def to_global_id(type_name, id)
          Base64.strict_encode64(["0", type_name.length, ":", type_name, id.to_s].join)
        end

        # Warning! This method might return _anything_.
        # If a user reverse-engineers a gid, they might fetch any object.
        # Prefer `typed_object_from_id` when you want an object of a certain type.
        def untyped_object_from_id(gid, permission:)
          type_name, id = from_global_id(gid)
          type = Platform::Schema.get_type(type_name)
          return Promise.resolve(nil) unless type

          type.load_from_global_id(id).then { |object|
            # Support a special case: `Objects.async_find_record_by_id`
            # will return a PullRequest when the `id` loads an Issue
            # which is actually a pull request. In that case,
            # we should update our type info accordingly.
            if object.is_a?(::PullRequest) && type_name == "Issue"
              type_name = "PullRequest"
            elsif object.is_a?(::Audit::Elastic::Hit)
              type_name = "AuditLog::#{type_name}"
            end

            if object
              permission.typed_can_access?(type_name, object).then { |accessible|
                if accessible
                  permission.typed_can_see?(type_name, object).then { |readable|
                    if readable
                      # We don't want to load spammy users by ID directly, although
                      # they _may_ be loaded in some contexts!
                      if type_name != "User" || !object.hide_from_user?(permission.viewer)
                        object
                      else
                        raise Platform::Errors::NotFound, "Could not resolve to a node with the global id of '#{gid}'."
                      end
                    else # wasn't readable
                      raise Platform::Errors::NotFound, "Could not resolve to a node with the global id of '#{gid}'."
                    end
                  }
                else # didn't have the right API access
                  raise Errors::NotFound, "Could not resolve to a node with the global id of '#{gid}'."
                end
              }
            else # didn't find it with the given global ID
              raise Errors::NotFound, "Could not resolve to a node with the global id of '#{gid}'."
            end
          }
        end

        # Load an object from `gid`, then assert that it is one of `possible_types`
        def typed_object_from_id(possible_types, gid, context)
          async_typed_object_from_id(possible_types, gid, context).sync
        end

        # A promise of `typed_object_from_id`
        def async_typed_object_from_id(possible_types, gid, context)
          Platform::Schema.object_from_id(gid, context).then do |object|
            type_name = type_name_from_object(object)
            actual_type = Platform::Schema.get_type(type_name)
            # Normalize old-style defns and new-style defns
            possible_types = Array(possible_types)

            if possible_types.any? { |type| type == actual_type || actual_type.interfaces.include?(type) }
              object
            else
              if context[:target] != :internal
                possible_types = possible_types.reject { |t| t.visibility == :internal }
              end
              type_phrase = possible_types.map(&:graphql_name).to_sentence(two_words_connector: " or ", last_word_connector: ", or ")
              raise Errors::NotFound, "Could not resolve to #{type_phrase} node with the global id of '#{gid}'"
            end
          end
        end
      end
    end
  end
end
