# frozen_string_literal: true

module Platform
  module EarthsmokeKeys
    CUSTOM_TASKS   = "custom-tasks-key".freeze
    HOOKSHOT_SIGNING = "hookshot-signature-key".freeze
  end
end
