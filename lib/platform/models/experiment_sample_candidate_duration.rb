# frozen_string_literal: true
#
class Platform::Models::ExperimentSampleCandidateDuration
  attr_reader :name, :duration, :difference

  def initialize(name:, duration:, difference:)
    @name = name
    @duration = duration
    @difference = difference
  end

  def platform_type_name
    "ExperimentCandidateDuration"
  end
end
