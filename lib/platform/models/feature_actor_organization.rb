# frozen_string_literal: true

module Platform
  module Models
    class FeatureActorOrganization
      include GitHub::Relay::GlobalIdentification

      def initialize(organization)
        @organization = organization
      end

      def id
        @organization.id
      end

      def login
        @organization.login
      end

      def async_integration
        @organization.async_integration
      end

      def primary_avatar_url(*args)
        @organization.primary_avatar_url(*args)
      end

      def async_profile
        @organization.async_profile
      end

      def flipper_id
        @organization.flipper_id
      end
    end
  end
end
