# frozen_string_literal: true

class Platform::Models::Diff
  def initialize(head_repo_id:, base_repo:, start_ref_or_oid:, end_ref_or_oid:, base_commit_oid: nil,
                 algorithm: GitRPC::Diff::ALGORITHM_DEFAULT, use_summary:, timeout: nil, compare_repo:)
    @base_repo = base_repo
    @head_repo_id = head_repo_id
    @algorithm = algorithm

    @diff = GitHub::Diff.new(compare_repo, start_ref_or_oid, end_ref_or_oid, base_sha: @base_commit_oid,
                                                                             use_summary: @use_summary,
                                                                             timeout: @timeout)
  end

  attr_reader :base_repo, :algorithm, :diff

  # just to be clear, the base repo is the "acting" repo
  alias repo base_repo

  delegate :entries, :deltas, :changed_files, :additions, :deletions, :changes, to: :diff

  def global_relay_id
    id_array = [
      base_repo.id,
      diff.parsed_sha1,
      @head_repo_id,
      diff.parsed_sha2,
      diff.base_sha,
      algorithm,
    ]
    Platform::Helpers::NodeIdentification.to_global_id("Diff", id_array.compact.join(":"))
  end
end
