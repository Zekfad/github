# frozen_string_literal: true
require "delegate"

class Platform::Models::BranchProtectionRule < SimpleDelegator
  include GitHub::Relay::GlobalIdentification
end
