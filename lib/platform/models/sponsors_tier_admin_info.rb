# frozen_string_literal: true

class Platform::Models::SponsorsTierAdminInfo
  attr_reader :tier

  def initialize(tier)
    @tier = tier
  end
end
