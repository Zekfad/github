# frozen_string_literal: true

class Platform::Models::Patch
  LARGE_DIFF_LINES = 500

  # diff           - An instance of Platform::Models::Diff to which this patch belongs
  # delta_or_entry - An instance of either a GitRPC::Diff::Delta or GitHub::Diff::Entry which this
  #                  patch wraps
  def initialize(diff, delta_or_entry)
    @diff    = diff
    @subject = delta_or_entry
  end

  delegate :additions, :binary?, :too_big?, :changes, :deletions, :similarity, :submodule?, :unicode_text, to: :subject

  attr_reader :diff

  def is_large_diff?
    too_big? || subject.line_count > LARGE_DIFF_LINES
  end

  def new_tree_entry
    if delta?
      ::TreeEntry.new(diff.repo, info_from_tree_node(subject.new_file))
    else
      ::TreeEntry.new(diff.repo, new_info_from_diff_entry)
    end
  end

  def old_tree_entry
    if delta?
      ::TreeEntry.new(diff.repo, info_from_tree_node(subject.old_file))
    else
      ::TreeEntry.new(diff.repo, old_info_from_diff_entry)
    end
  end

  def status
    status = subject.status_label.to_sym

    return :deleted if status == :removed

    status
  end

  def text
    GitHub::Encoding.try_guess_and_transcode(subject.text)
  end

  def blob_url
    blob_or_raw_url(type: "blob")
  end

  def raw_url
    blob_or_raw_url(type: "raw")
  end

  def contents_url
    sha, path = diff_sha_and_path(subject)
    repo = diff.repo.name_with_owner

    url = "#{GitHub.api_url}/repos/#{repo}/contents/#{path}?ref=#{sha}"
    Addressable::URI.encode(url)
  end

  def async_diff_lines(syntax_highlighted_diffs_enabled:)
    repo = diff.repo
    async_warm_syntax_highlighted_diff = if syntax_highlighted_diffs_enabled
      Platform::Loaders::WarmSyntaxHighlightedDiffCache.load(repo, nil, subject)
    else
      Promise.resolve(nil)
    end

    async_warm_syntax_highlighted_diff.then do
      lines = SyntaxHighlightedDiff.new(repo).colorized_lines(subject)
      if lines
        lines.each(&:freeze)
        lines.freeze
      end

      subject.enumerator.map do |line|
        {
          type: line.type,
          text: line.text,
          html: lines ? lines[line.position] : ERB::Util.html_escape(line.text),
          position: line.position,
          left: line.left == -1 ? nil : line.left,
          right: line.right == -1 ? nil : line.right,
          no_newline_at_end: line.nonewline?,
        }
      end
    end
  end

  private

  attr_reader :subject

  def blob_or_raw_url(type:)
    return nil if subject.submodule?

    sha, path = diff_sha_and_path(subject)
    repo = diff.repo.name_with_owner

    url = "#{GitHub.url}/#{repo}/#{type}/#{sha}/#{path}"
    Addressable::URI.encode(url)
  end

  def diff_sha_and_path(subject)
    if delta?
      a = subject.old_file
      b = subject.new_file

      subject.deleted? ? [a.oid, a.path] : [b.oid, b.path]
    else
      subject.deleted? ? [subject.a_sha, subject.a_path] : [subject.b_sha, subject.b_path]
    end
  end

  def delta?
    subject.is_a?(GitRPC::Diff::Delta)
  end

  def info_from_tree_node(node)
    { "path" => GitHub::Encoding.try_guess_and_transcode(node.path), "mode" => node.mode, "oid" => node.oid, "type" => "blob" }
  end

  def new_info_from_diff_entry
    { "path" => subject.b_path || "", "mode" => subject.b_mode, "oid" => subject.b_blob, "type" => "blob" }
  end

  def old_info_from_diff_entry
    { "path" => subject.a_path || "", "mode" => subject.a_mode, "oid" => subject.a_blob, "type" => "blob" }
  end
end
