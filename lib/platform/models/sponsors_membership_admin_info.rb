# frozen_string_literal: true

class Platform::Models::SponsorsMembershipAdminInfo
  attr_reader :membership

  def initialize(membership)
    @membership = membership
  end
end
