# frozen_string_literal: true

module Platform
  module Models
    class FeatureActorFinder
      def initialize(type, identifier)
        @type, @identifier = type, identifier
      end

      def actor
        case @type
        when "USER"
          ::User.find_by_login(@identifier)
        when "REPOSITORY"
          ::Repository.with_name_with_owner(@identifier)
        when "ENTERPRISE"
          ::Business.find_by(slug: @identifier)
        when "OAUTH_APPLICATION"
          ::OauthApplication.find_by_key(@identifier)
        when "APP"
          ::Integration.find_by_id(@identifier)
        when "HOST"
          GitHub::FlipperHost.new(@identifier)
        when "ROLE"
          GitHub::FlipperRole.new(@identifier)
        when "FLIPPER_ID"
          GitHub::FlipperActor.from_flipper_id(@identifier)
        end
      end
    end
  end
end
