# frozen_string_literal: true

class Platform::Models::AccountStafftoolsInfo
  attr_reader :account

  def initialize(account)
    @account = account
  end

  def platform_type_name
    # Bot / User / Organization
    account.class.name + "StafftoolsInfo"
  end
end
