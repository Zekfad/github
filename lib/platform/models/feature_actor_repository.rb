# frozen_string_literal: true

module Platform
  module Models
    class FeatureActorRepository
      include GitHub::Relay::GlobalIdentification

      def initialize(repository)
        @repository = repository
      end

      def id
        @repository.id
      end

      def name_with_owner
        @repository.name_with_owner
      end

      def private?
        @repository.private?
      end

      def name
        @repository.name
      end

      def owner_id
        @repository.owner_id
      end

      def flipper_id
        @repository.flipper_id
      end
    end
  end
end
