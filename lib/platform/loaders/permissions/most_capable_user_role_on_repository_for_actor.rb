# frozen_string_literal: true

module Platform
  module Loaders
    module Permissions
      class MostCapableUserRoleOnRepositoryForActor < Platform::Loader
        def self.load(actor:, repo:)
          # Note: we pass `actor` here instead of `actor_id` because for `Team`
          # instances we also need to be able to look up `id_and_ancestor_ids`
          # and prefer not to pass an `id` and look it back up inside of `fetch`
          self.for(actor.class).load([actor, repo.id])
        end

        def initialize(actor_class)
          @actor_class = actor_class
        end

        # entries is an Array of [actor (User, Team), repo.id] that we're trying to look up
        def fetch(entries)
          actors, repo_ids = entries.transpose

          # Collect all relevant user roles for actors
          relevant_actors_by_actor_id = relevant_actors_by_actor_id_for(actors)
          user_roles = UserRole.where(
            actor_type: @actor_class, actor_id: relevant_actors_by_actor_id.keys,
            target_type: Repository, target_id: repo_ids
          )

          # Collect all relevant user roles for teams actor is a part of
          relevant_actors_by_team_id = relevant_actors_by_team_id_for(actors)
          if relevant_actors_by_team_id.any?
            user_roles += UserRole.where(
              actor_type: Team, actor_id: relevant_actors_by_team_id.keys,
              target_type: Repository, target_id: repo_ids
            )
          end

          # Choose the most capable role per actor/repo
          select_most_capable_roles(user_roles, relevant_actors_by_actor_id, relevant_actors_by_team_id)
        end

        private

        # Select the most capable role per [actor, repo_id] combination
        def select_most_capable_roles(user_roles, relevant_actors_by_actor_id, relevant_actors_by_team_id)
          promises = user_roles.map(&:async_role)
          results = Promise.all(promises).sync
          roles = results.index_by(&:id)

          user_roles.each_with_object({}) do |user_role, result|
            # If the role is for this actor type consider all actors
            if user_role.actor_type == @actor_class.name
              relevant_actors_by_actor_id[user_role.actor_id].each do |actor|
                assign_user_role_if_greater(result, roles, actor, user_role)
              end
            end

            # If the role is for a team also consider all teams
            if user_role.actor_type == Team.name
              relevant_actors_by_team_id[user_role.actor_id].each do |actor|
                assign_user_role_if_greater(result, roles, actor, user_role)
              end
            end
          end
        end

        # Look at a user_role and see see if the role is greater than the currently
        # recorded one for this actor. If it is, update the hash appropriately
        def assign_user_role_if_greater(result, roles, actor, user_role)
          key = [actor, user_role.target_id]

          role = roles[user_role.role_id]
          recorded_user_role = result[key]

          if recorded_user_role.nil? || role.action_rank > roles[recorded_user_role.role_id].action_rank
            result[key] = user_role
          end
        end

        # Mapping of which actors (Team, User) will be relevant to a given team_id (Team)
        def relevant_actors_by_team_id_for(actors)
          relevant_actors_by_team_id = Hash.new { |h, k| h[k] = [] }

          if @actor_class == User
            team_id_promises = actors.map do |actor|
              Platform::Loaders::UserTeams.load(actor, with_ancestors: true).then do |team_ids|
                team_ids.each do |team_id|
                  relevant_actors_by_team_id[team_id] << actor
                end
              end
            end

            Promise.all(team_id_promises).sync
          end

          relevant_actors_by_team_id
        end

        # Mapping of which actors (Team, User) will be relevant to a given actor_id (Team, User)
        def relevant_actors_by_actor_id_for(actors)
          relevant_actors_by_actor_id = Hash.new { |h, k| h[k] = [] }

          actors.each do |actor|
            actor_ids = actor.is_a?(Team) ? actor.id_and_ancestor_ids : [actor.id]
            actor_ids.each do |id|
              relevant_actors_by_actor_id[id] << actor
            end
          end

          relevant_actors_by_actor_id
        end
      end
    end
  end
end
