#frozen_string_literal: true

module Platform
  module Loaders
    class UserBlockedCheck < Platform::Loader
      def self.load(user_id, ignored_id)
        self.for.load([user_id, ignored_id])
      end

      def fetch(user_id_and_ignored_ids)
        conditions = []
        values = []

        user_id_and_ignored_ids.group_by(&:first).each do |(user_id, users_and_ids)|
          conditions << "(user_id = ? AND ignored_id IN (?))"
          values << user_id << users_and_ids.map(&:last)
        end

        scope = IgnoredUser.where([conditions.join(" OR "), *values])

        selected_attributes = [:user_id, :ignored_id]
        results = scope.pluck(*selected_attributes)

        results.each_with_object(Hash.new { false }) do |(user_id, ignored_id), user_and_ignored_ids|
          user_and_ignored_ids[[user_id, ignored_id]] = true
        end
      end
    end
  end
end
