# frozen_string_literal: true

module Platform
  module Loaders
    # does this sponsor sponsor any of the sponsorable_ids
    class IsSponsoringCheck < Platform::Loader
      def self.load(sponsor_id:, sponsorable_id:, include_private: false, invert: false)
        self.for(sponsor_id, include_private: include_private, invert: invert).load(sponsorable_id)
      end

      def initialize(sponsor_id, include_private:, invert:)
        @sponsor_id = sponsor_id
        @include_private = include_private
        @invert = invert
      end

      def fetch(sponsorable_ids)
        sponsorable_ids = sponsorable_ids.compact

        sponsorships = Sponsorship.active
        sponsorships = sponsorships.privacy_public unless @include_private

        sponsorships = sponsorships.where(sponsor_id: @sponsor_id)
        sponsorships = sponsorships.where(sponsorable_id: sponsorable_ids) if sponsorable_ids.any?

        if sponsorable_ids.empty?
          @invert ? Hash.new(sponsorships.none?) : Hash.new(sponsorships.any?)
        else
          results = @invert ? Hash.new(true) : Hash.new(false)

          sponsorships.pluck(:sponsorable_id).each_with_object(results) do |sponsorable_id, result|
            result[sponsorable_id] = @invert ? false : true
          end
        end
      end
    end
  end
end
