# frozen_string_literal: true

module Platform
  module Loaders
    class RepositoryByNwo < Platform::Loader
      def self.load(name_with_owner)
        self.for.load(name_with_owner.downcase)
      end

      def fetch(names_with_owners)
        ::Repository.with_names_with_owners(names_with_owners).index_by { |repo| repo.name_with_owner.downcase }
      end
    end
  end
end
