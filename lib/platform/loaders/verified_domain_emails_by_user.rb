# frozen_string_literal: true

module Platform
  module Loaders
    class VerifiedDomainEmailsByUser < Platform::Loader
      def self.load(organization, user_id)
        async_verified_domains_for(organization).then do |domains|
          self.for(domains).load(user_id)
        end
      end

      def initialize(domains)
        @domains = domains
      end

      def fetch(user_ids)
        results = {}

        if @domains.any?
          results = UserEmail.joins(
            "LEFT JOIN email_roles " \
            "ON user_emails.id = email_roles.email_id " \
            "AND email_roles.role IN ('hard_bounce', 'soft_bounce', 'stealth')",
          ).where(
            email_roles: { id: nil },
            state: "verified",
            user_id: user_ids,
            normalized_domain: @domains,
          ).group_by(&:user_id)
        end

        user_ids.each_with_object(results) do |user_id, result|
          result[user_id] ||= []
        end
      end

      def self.async_verified_domains_for(organization)
        organization.async_verified_domains.then { |domains| domains.map(&:domain) }
      end
    end
  end
end
