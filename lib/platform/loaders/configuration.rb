# frozen_string_literal: true

module Platform
  module Loaders
    class Configuration < Platform::Loader
      def self.load(object, method)
        self.for(object).load(method)
      end

      def self.load_many(object, methods)
        self.for(object).load_many(methods)
      end

      def initialize(object)
        @object = object
      end

      def load(key)
        @object.async_configuration_owners.then { super(key) }
      end

      def fetch(methods)
        methods.each_with_object({}) do |method, values|
          values[method] = @object.send(method)
        end
      end
    end
  end
end
