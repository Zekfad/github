# frozen_string_literal: true

module Platform
  module Loaders
    class ActiveRecord < Platform::Loader
      def self.load(model, id, column: :id, security_violation_behaviour: :raise, case_sensitive: true)
        self.for(model, column, case_sensitive: case_sensitive).load(id, security_violation_behaviour: security_violation_behaviour)
      end

      def self.load_all(model, ids, column: :id, security_violation_behaviour: :raise, case_sensitive: true)
        loader = self.for(model, column, case_sensitive: case_sensitive)
        Promise.all(ids.map { |id| loader.load(id, security_violation_behaviour: security_violation_behaviour) })
      end

      def self.load_relation(relation)
        load_all(relation.klass, relation.pluck(:id))
      end

      def load(id, security_violation_behaviour: :raise)
        super(id).then { |object|
          # The Platform layer mostly relies on transitive access control (ie. if
          # the user has access to entity A, and entity B is accessible from entity
          # A, then they must have access to entity B).
          #
          # Care is taken where repositories are exposed to hide repositories that
          # the user does not have access to, but this is a last-ditch effort to
          # prevent accidental exposure if we miss a spot.
          #
          if object.is_a?(::Repository)
            Platform::Security::RepositoryAccess.async_guard(object, security_violation_behaviour: security_violation_behaviour)
          else
            object
          end
        }
      end

      def initialize(model, column = :id, case_sensitive: true)
        @model = model
        @column = column
        @case_sensitive = case_sensitive
        @case_insensitive_record_map = {}
      end

      def populate_case_insensitive_record_map!(ids)
        ids.each do |id|
          if id.is_a?(String)
            @case_insensitive_record_map[id.downcase] ||= []
            @case_insensitive_record_map[id.downcase] << id
          end
        end
      end

      def record_id_case_variants(id)
        if id.is_a?(String) && !@case_sensitive
          @case_insensitive_record_map[id.downcase] || [id]
        else
          [id]
        end
      end

      def fetch(ids)
        populate_case_insensitive_record_map!(ids) unless @case_sensitive
        record_map = {}

        records = @model.where(@column => ids).to_a

        records.each do |record|
          record_id_case_variants(record[@column]).each do |id|
            # Prefer first record if there's a column collision.
            record_map[id] ||= record
          end
        end

        record_map
      end

      def dog_tags
        ["model:#{@model.name.underscore}"]
      end
    end
  end
end
