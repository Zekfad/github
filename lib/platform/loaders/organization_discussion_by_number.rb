# frozen_string_literal: true

module Platform
  module Loaders
    class OrganizationDiscussionByNumber < Platform::Loader
      def self.load(org_id, number)
        self.for(org_id).load(number)
      end

      def self.load_all(org_id, numbers)
        loader = self.for(org_id)
        Promise.all(numbers.map { |number| loader.load(number) })
      end

      def initialize(org_id)
        @org_id = org_id
      end

      def fetch(numbers)
        ::OrganizationDiscussionPost.where(organization_id: @org_id, number: numbers).
          index_by(&:number)
      end
    end
  end
end
