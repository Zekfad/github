# frozen_string_literal: true

module Platform
  module Loaders
    class RedirectedRepositoryByNwo < Platform::Loader
      def self.load(nwo)
        self.for.load(nwo)
      end

      def self.load_all(nwos)
        loader = self.for
        Promise.all(nwos.map { |nwo| loader.load(nwo) })
      end

      def fetch(names)
        ::RepositoryRedirect
          .includes(:repository)
          .where(repository_name: names)
          .order("repository_redirects.created_at DESC, repository_redirects.id DESC")
          .group_by(&:repository_name)
          .map { |name, rrs| [name, rrs.first.repository] }
          .to_h
      end
    end
  end
end
