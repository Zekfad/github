# frozen_string_literal: true

module Platform
  module Loaders
    class ProjectEventVisibleCheck < Platform::Loader
      include Scientist

      def self.load(viewer, issue_event_id)
        self.for(viewer).load(issue_event_id)
      end

      def initialize(viewer)
        @viewer = viewer
      end

      def fetch(issue_event_ids)
        project_id_by_issue_event_id = ::IssueEventDetail.where({
          issue_event_id: issue_event_ids,
          subject_type: "Project",
        }).pluck(:issue_event_id, :subject_id).to_h

        project_ids = project_id_by_issue_event_id.values.uniq
        return Hash.new(false) if project_ids.empty?

        ability_project_ids = []

        if @viewer
          ability_project_ids.concat(direct_project_ids(project_ids))
          ability_project_ids.concat(adminable_project_ids(project_ids))
          ability_project_ids.concat(indirect_via_children_project_ids(project_ids))
        end

        project_ids_scope = Project.where(id: project_ids)

        if @viewer && ability_project_ids.any?
          project_ids_scope = project_ids_scope.where("owner_type = 'Repository' OR (owner_type IN  ('Organization', 'User') AND (id IN (?) OR owner_id = ? OR public = 1))", ability_project_ids, @viewer.id)
        elsif @viewer
          project_ids_scope = project_ids_scope.where("owner_type = 'Repository' OR (owner_type IN  ('Organization', 'User') AND (owner_id = ? OR public = 1))", @viewer.id)
        else
          project_ids_scope = project_ids_scope.where("owner_type = 'Repository' OR (owner_type IN  ('Organization', 'User') AND public = 1)")
        end

        project_ids_with_owner = project_ids_scope.pluck(:id, :owner_type, :owner_id)

        return Hash.new(false) if project_ids_with_owner.empty?

        visible_project_ids = Set.new

        Promise.all(project_ids_with_owner.map { |(project_id, owner_type, owner_id)|
          async_project_enabled = case owner_type
          when "User"
            # User owned projects are always enabled
            Promise.resolve(true)
          when "Repository"
            RepositoryProjectsEnabledCheck.load(owner_id)
          when "Organization"
            OrganizationProjectsEnabledCheck.load(owner_id)
          end

          async_project_enabled.then do |enabled|
            visible_project_ids << project_id if enabled
          end
        }).sync

        issue_event_ids.map { |issue_event_id|
          if (project_id = project_id_by_issue_event_id[issue_event_id])
            [issue_event_id, visible_project_ids.include?(project_id)]
          else
            [issue_event_id, true]
          end
        }.to_h
      end

      private

      def adminable_project_ids(project_ids)
        org_owned_project_org_ids = Project.where(owner_type: "Organization", id: project_ids).pluck(:owner_id)
        project_orgs_viewer_admins = ::Ability.
                                            where(actor_id: @viewer.id,
                                                  actor_type: "User",
                                                  subject_type: "Organization",
                                                  subject_id: org_owned_project_org_ids,
                                                  action: ::Ability.actions[:admin])
                                            .pluck(:subject_id)
        projects_user_can_admin = Project.where(id: project_ids, owner_id: project_orgs_viewer_admins).pluck(:id)
      end

      def direct_project_ids(project_ids)
        ::Ability.direct.
          where(actor_id: @viewer.id,
                actor_type: "User",
                subject_id: project_ids,
                subject_type: "Project").
                distinct.pluck(:subject_id)
      end

      def indirect_via_children_project_ids(project_ids)
        ::Ability.indirect_via_children.
          where(actor_id: @viewer.id,
                actor_type: "User",
                children: { subject_id: project_ids,
                            subject_type: "Project" })
                .distinct.pluck("children.subject_id")
      end
    end
  end
end
