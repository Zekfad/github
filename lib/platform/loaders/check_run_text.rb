# frozen_string_literal: true

module Platform
  module Loaders
    class CheckRunText < Platform::Loader

      SUPPORTED_COLUMNS = [:text, :summary].freeze

      def self.load(check_run_id, column: :text)
        raise Platform::Errors::InvalidValue, "unsupported column '#{column}'" unless SUPPORTED_COLUMNS.include?(column)
        self.for(column).load(check_run_id)
      end

      attr_reader :column

      def initialize(column)
        @column = column
      end

      def fetch(ids)
        CheckRun.where(id: ids).pluck(:id, column).to_h
      end
    end
  end
end
