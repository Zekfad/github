# frozen_string_literal: true

module Platform
  module Loaders
    class SponsorsMembershipCheck < Platform::Loader
      def self.load(sponsorable_id)
        self.for.load(sponsorable_id)
      end

      def fetch(sponsorable_ids)
        accepted_memberships = SponsorsMembership.with_accepted_state.
          where(sponsorable_id: sponsorable_ids).
          pluck(:sponsorable_id)

        accepted_memberships.each_with_object(Hash.new(false)) do |sponsorable_id, result|
          result[sponsorable_id] = true
        end
      end
    end
  end
end
