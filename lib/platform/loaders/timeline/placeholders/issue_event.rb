# frozen_string_literal: true

module Platform
  module Loaders
    module Timeline
      module Placeholders
        class IssueEvent < Platform::Loader

          def self.load(issue_id, viewer, visible_events_only: false, requested_issue_event_type_names: [])
            self.for(viewer, visible_events_only, requested_issue_event_type_names).load(issue_id)
          end

          def initialize(viewer, visible_events_only, requested_issue_event_type_names)
            @viewer = viewer
            @visible_events_only = visible_events_only
            @requested_issue_event_type_names = requested_issue_event_type_names
          end

          def fetch(issue_ids)
            GitHub.dogstats.time("platform.loaders.timeline_placeholders", {
              tags: ["loader:issue_event"],
            }) do
              fetch_placeholders(issue_ids)
            end
          end

          private

          attr_reader :viewer, :visible_events_only, :requested_issue_event_type_names

          def fetch_placeholders(issue_ids)
            requested_event_column_names = requested_issue_event_type_names.map do |type_name|
              ::IssueEvent.platform_type_name_to_column(type_name)
            end

            query = ::IssueEvent.visible_in_issue_timeline_query_for(
              viewer,
              issue_ids: issue_ids,
              selected_attributes: [:issue_id, :id, :created_at, :event, :commit_id, :actor_id, :commit_repository_id],
              visible_events_only: visible_events_only,
              requested_events: requested_event_column_names,
            )
            rows = ::IssueEvent.connection.select_rows(query)

            results_by_issue_id = Hash.new { |hash, key| hash[key] = [] }

            Promise.all(rows.map { |(issue_id, id, created_at, event, commit_id, actor_id, commit_repository_id)|
              async_event_visible = UserSpammyCheck.load(actor_id, viewer).then do |spammy|
                next false if spammy

                async_actor_blocked_by_viewer = if actor_id && viewer
                  UserBlockedCheck.load(viewer.id, actor_id)
                else
                  Promise.resolve(false)
                end

                async_actor_blocked_by_viewer.then do |blocked|
                  next false if blocked

                  case event
                  when *::IssueEvent::PROJECT_EVENTS
                    ProjectEventVisibleCheck.load(viewer, id)
                  when "referenced"
                    next true unless commit_repository_id

                    RepositoryVisibleCheck.load(viewer, commit_repository_id, resource: "contents").then do |visible|
                      next true if visible

                      UnlockedRepositoryCheck.load(viewer, commit_repository_id)
                    end
                  when *::IssueEvent::CROSS_ISSUE_SUBJECT_EVENTS
                    IssueSubjectEventVisibleCheck.load(viewer, id)
                  when "user_blocked"
                    next true unless GitHub.spamminess_check_enabled?

                    IssueEventSpammySubjectVisibleCheck.load(viewer: viewer, event_id: id)
                  else
                    true
                  end
                end
              end

              async_event_visible.then do |visible|
                next unless visible

                results_by_issue_id[issue_id] << ::Timeline::Placeholder::IssueEvent.new(
                  id: id,
                  sort_datetimes: [created_at],
                  event_name: event,
                  commit_oid: commit_id,
                )
              end
            }).sync

            results_by_issue_id
          end
        end
      end
    end
  end
end
