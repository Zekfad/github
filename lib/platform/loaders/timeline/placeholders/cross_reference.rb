# frozen_string_literal: true

module Platform
  module Loaders
    module Timeline
      module Placeholders
        class CrossReference < Platform::Loader
          include Scientist

          def self.load(issue_id, viewer)
            self.for(viewer).load(issue_id)
          end

          def initialize(viewer)
            @viewer = viewer
          end

          def fetch(issue_ids)
            GitHub.dogstats.time("platform.loaders.timeline_placeholders", {
              tags: ["loader:cross_reference"],
            }) do
              fetch_placeholders(issue_ids)
            end
          end

          private

          attr_reader :viewer

          def fetch_placeholders(issue_ids)
            scope = ::CrossReference.where(target_type: "Issue", target_id: issue_ids, source_type: "Issue").
              with_valid_source_issue.
              with_valid_target_issue.
              with_spammy_source_issues_hidden_for(viewer).
              with_issues_from_spammy_repositories_hidden_for(viewer).
              created_before_target_conversation_was_locked.
              filter_spam_for(viewer)

            results = scope.pluck(
              Arel.sql("cross_references.target_id"),
              Arel.sql("cross_references.id"),
              Arel.sql("cross_references.created_at"),
              Arel.sql("cross_references.actor_id"),
              Arel.sql("cross_references.source_id"),
              Arel.sql("source_issues.pull_request_id"),
              Arel.sql("source_issues.repository_id"),
              Arel.sql("target_issues.repository_id"),
            )

            results_by_issue_id = Hash.new { |hash, key| hash[key] = [] }

            Promise.all(results.map { |(target_id, id, created_at, actor_id, source_id, pull_request_id, source_repository_id, target_repository_id)|
              # Use security_violation_behaviour: :nil to prevent loading any repos
              # the viewer doesn't have access to see
              Platform::Loaders::ActiveRecord.load_all(::Repository, [source_repository_id, target_repository_id], security_violation_behaviour: :nil).then do |source_repository, target_repository|
                next unless source_repository
                next if source_repository.disabled_at && (viewer.nil? || !viewer.site_admin?)
                next if pull_request_id.nil? && !source_repository.has_issues

                Promise.all([
                  Platform::Loaders::UserBlockedCheck.load(target_repository.owner_id, actor_id),
                  viewer ? Platform::Loaders::UserBlockedCheck.load(viewer.id, actor_id) : Promise.resolve(false),
                ]).then { |blocked_by_target_repository_owner, blocked_by_viewer|
                  next false if blocked_by_target_repository_owner || blocked_by_viewer
                  next false unless source_repository.public? || viewer

                  results_by_issue_id[target_id] << ::Timeline::Placeholder::CrossReference.new(
                    id: id,
                    sort_datetimes: [created_at],
                  )
                }
              end
            }).sync

            results_by_issue_id
          end
        end
      end
    end
  end
end
