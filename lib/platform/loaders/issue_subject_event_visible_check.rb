# frozen_string_literal: true

module Platform
  module Loaders
    class IssueSubjectEventVisibleCheck < Platform::Loader
      def self.load(viewer, issue_event_id)
        self.for(viewer).load(issue_event_id)
      end

      def initialize(viewer)
        @viewer = viewer
      end

      def fetch(issue_event_ids)
        subject_issue_id_by_issue_event_id = ::IssueEventDetail.where({
          issue_event_id: issue_event_ids,
          subject_type: "Issue",
        }).pluck(:issue_event_id, :subject_id).to_h

        result = Hash.new(false)

        Promise.all(subject_issue_id_by_issue_event_id.map { |issue_event_id, subject_id|
          Platform::Loaders::ActiveRecord.load(::Issue, subject_id).then do |issue|
            next false unless issue
            issue.async_repository.then do |repo|
              unless issue.violated_oap?(@viewer, repo)
                issue.async_readable_by?(@viewer).then do |visible|
                  result[issue_event_id] = visible
                end
              end
            end
          end
        }).sync

        result
      end
    end
  end
end
