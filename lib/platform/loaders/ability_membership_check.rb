#frozen_string_literal: true

module Platform
  module Loaders
    class AbilityMembershipCheck < Platform::Loader
      def self.load(subject_type, subject_id, actor_type, actor_id)
        self.for.load([subject_type, subject_id, actor_type, actor_id])
      end

      def fetch(subjects_and_actors)
        conditions = []
        values = []

        subjects_and_actors.each do |(subject_type, subject_id, actor_type, actor_id)|
          conditions << "(subject_type = ? AND subject_id = ? AND actor_type = ? AND actor_id = ?)"
          values << subject_type << subject_id << actor_type << actor_id
        end

        scope = ::Ability.direct.where([conditions.join(" OR "), *values])

        selected_attributes = [:subject_type, :subject_id, :actor_type, :actor_id]
        results = scope.pluck(*selected_attributes)

        results.each_with_object(Hash.new { false }) do |(subject_type, subject_id, actor_type, actor_id), hash|
          hash[[subject_type, subject_id, actor_type, actor_id]] = true
        end
      end
    end
  end
end
