#frozen_string_literal: true

module Platform
  module Loaders
    class UnlockedRepositoryCheck < Platform::Loader
      def self.load(user, repository_id)
        self.for(user).load(repository_id)
      end

      def initialize(user)
        @user = user
      end

      def fetch(repository_ids)
        # For extra caution, return false immediately if the user is not staff or Enterprise admin
        result = repository_ids.map { |repository_id| [repository_id, false] }.to_h
        return result unless user&.site_admin?

        scope = RepositoryUnlock.active_for_user(user).where(repository_id: repository_ids)

        if GitHub.enterprise?
          scope.each do |unlock|
            result[unlock.repository_id] = true
          end
        else
          scope.preload(:staff_access_grant).each do |unlock|
            result[unlock.repository_id] = true if unlock.staff_access_grant&.active?
          end
        end

        result
      end

      private
      attr_reader :user
    end
  end
end
