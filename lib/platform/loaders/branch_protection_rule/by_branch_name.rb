# frozen_string_literal: true

module Platform
  module Loaders
    module BranchProtectionRule
      class ByBranchName < Platform::Loader
        def self.load(repository, branch_name)
          self.for(repository).load(branch_name)
        end

        def initialize(repository)
          @repository = repository
        end

        def fetch(branch_names)
          protected_branches = ProtectedBranch.where(repository_id: @repository.id).each do |protected_branch|
            protected_branch.association(:repository).target = @repository
          end

          ordered_protected_branches = protected_branches.sort_by do |protected_branch|
            [protected_branch.wildcard_rule? ? 1 : 0, protected_branch.id]
          end

          branch_names.map { |branch_name|
            matching_protected_branch = ordered_protected_branches.find do |protected_branch|
              protected_branch.matches?(branch_name)
            end

            [branch_name, matching_protected_branch]
          }.to_h
        end
      end
    end
  end
end
