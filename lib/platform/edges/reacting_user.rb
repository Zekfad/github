# frozen_string_literal: true

module Platform
  module Edges
    class ReactingUser < Edges::Base
      description "Represents a user that's made a reaction."

      def self.authorized?(edge, context)
        edge.node.async_user.then do |user|
          self.node_type.authorized?(user, context)
        end
      end

      field :node, Objects::User, null: false

      def node
        object.node.async_user
      end

      field :reacted_at, Scalars::DateTime, description: "The moment when the user made the reaction.", null: false

      def reacted_at
        @object.node.created_at
      end
    end
  end
end
