# frozen_string_literal: true
module Platform
  module Edges
    class Base < Platform::Objects::Base
      description "An edge in a connection."

      # Since Edges are wrappers around some other object,
      # authorize them based on that other object.
      #
      # Authorize the wrapped object as if it was wrapped by
      # the node type (which it will be, after/if it passes this check).
      def self.authorized?(object, ctx)
        if node_type < Objects::Base
          node_object_type = node_type
        else
          # Disambiguate connections whose `.node_type` is a Union or Interface
          node_object_type = ctx.schema.resolve_type(self.node_type, object.node, ctx)
        end
        # Call the hook for that concrete class
        node_object_type.authorized?(object.node, ctx)
      end

      def self.inherited(child_class)
        super
        # If the class isn't an anonymous class, let's try to guess which
        # GraphQL type it comes from.
        if child_class.name
          # The class is named just like the object type that the edge wraps,
          # but that will be a naming conflict, so append `Edge` to the class name
          # for graphql purposes.
          wrapped_type_name = child_class.name.split("::").last
          child_class.node_type_name(wrapped_type_name)
        end

        child_class.scopeless_tokens_as_minimum
      end

      # Use the wrapped type name to:
      # - name this type
      # - Add a `node` field
      def self.node_type_name(wrapped_type_name)
        graphql_name("#{wrapped_type_name}Edge")
        # Add a default `node` field
        field :node, "Platform::Objects::#{wrapped_type_name}", null: true, description: "The item at the end of the edge."
      end

      # Like `node_type_name`, but using a reference to the Class itself
      # (works with anonymous classes)
      def self.node_type(wrapped_type_class = nil)
        if wrapped_type_class
          graphql_name("#{wrapped_type_class.graphql_name}Edge") if self.name.nil?
          field :node, wrapped_type_class, null: true, description: "The item at the end of the edge."
          @node_type = wrapped_type_class
        else
          @node_type ||= self.fields["node"].type.unwrap
        end
      end

      field :cursor, String,
        null: false,
        description: "A cursor for use in pagination."
    end
  end
end
