# frozen_string_literal: true

module Platform
  module Edges
    class Mannequin < Edges::Base
      areas_of_responsibility :migration

      description "Represents a mannequin."

      node_type Objects::Mannequin
      visibility :internal
    end
  end
end
