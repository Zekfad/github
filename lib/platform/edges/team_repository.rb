# frozen_string_literal: true

module Platform
  module Edges
    class TeamRepository < Edges::Base
      description "Represents a team repository."

      minimum_accepted_scopes ["read:org"]

      field :node, Objects::Repository, null: false

      url_fields prefix: "team_repository", description: "The HTTP URL for this team's repository", visibility: :internal do |edge|
        repo = edge.node
        team = edge.parent
        team.async_organization.then do |org|
          template = Addressable::Template.new("/orgs/{org}/teams/{team}/repositories/{repo}")
          template.expand org: org.login, team: team.slug, repo: repo.id
        end
      end

      field :permission, Enums::RepositoryPermission, description: "The permission level the team has on the repository", null: false do
        mark_for_upcoming_type_change(
          start_date: Date.new(2020, 05, 06),
          reason: "This field may return additional values",
          new_type: "String",
          owner: "oneill38",
        )
      end

      def permission
        repo = @object.node
        team = @object.parent
        team.async_most_capable_action_or_role_for(repo)
      end

      field :inherited_permission_origin, Objects::Team, visibility: :internal, description: "The parent team that grants inherited permission to this repository", null: true

      def inherited_permission_origin
        async_inherited_action_or_role.then do |action_or_role|
          next unless action_or_role
          action_or_role.async_actor
        end
      end

      field :inherited_permission, Enums::RepositoryPermission, visibility: :internal, description: "The inherited permission level the team has on the repository", null: true do
        mark_for_upcoming_type_change(
          start_date: Date.new(2020, 05, 06),
          reason: "This field may return additional values",
          new_type: "String",
          owner: "oneill38",
        )
      end

      def inherited_permission
        async_inherited_action_or_role.then do |action_or_role|
          next unless action_or_role
          if action_or_role.is_a?(UserRole) && action_or_role.role.custom?
            action_or_role.role.base_role.name
          else
            action_or_role.action_name
          end
        end
      end

      def async_inherited_action_or_role
        repo = @object.node
        team = @object.parent
        return unless team.ancestor_ids.present?

        promises = [
          Loaders::MostCapableInheritedTeamRepositoryAbilities.load(team: team, repo: repo),
          Loaders::Permissions::MostCapableInheritedTeamRepositoryUserRole.load(team: team, repo: repo),
        ]

        Promise.all(promises).then do |ability, user_role|
          next unless ability
          next ability unless user_role.present?

          user_role.async_role.then do |role|
            if role.action_rank > Ability::ACTION_RANKING[ability.action.to_sym]
              user_role
            else
              ability
            end
          end
        end
      end
    end
  end
end
