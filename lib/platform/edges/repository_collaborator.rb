# frozen_string_literal: true

module Platform
  module Edges
    class RepositoryCollaborator < Edges::Base
      description "Represents a user who is a collaborator of a repository."

      minimum_accepted_scopes ["public_repo"]

      field :node, Objects::User, null: false

      field :permission, Enums::RepositoryPermission, description: "The permission the user has on the repository.", null: false do
        mark_for_upcoming_type_change(
          start_date: Date.new(2020, 05, 06),
          reason: "This field may return additional values",
          new_type: "String",
          owner: "oneill38",
        )
      end

      def permission
        user = @object.node
        repository = @object.parent

        repository.async_action_or_role_level_for(user, include_custom_roles: false).then do |permission|
          permission.to_s
        end
      end

      field :permission_sources, [Objects::PermissionSource], minimum_accepted_scopes: ["admin:org"], visibility: :public, description: "A list of sources for the user's access to the repository.", null: true

      def permission_sources
        repository = @object.parent
        user = @object.node
        repository.async_owner.then do |owner|
          grants = repository.permission_sources(user, context[:viewer])
          grants.each { |g| g["organization"] = owner }
        end
      end
    end
  end
end
