# frozen_string_literal: true
module Platform
  module Batch
    class Setup < GraphQL::Batch::SetupMultiplex
      REQUEST_EXECUTOR_KEY = :GRAPHQL_BATCH_REQUEST_EXECUTOR

      def before_multiplex(_)
        # here we may have already an executor, being used for the current request
        # We save it in a thread local variable, and we'll reset it when we're done
        # batching the graphql request
        existing = GraphQL::Batch::Executor.current
        Thread.current[REQUEST_EXECUTOR_KEY] = existing
        GraphQL::Batch::Executor.current = nil
        super
      end

      def after_multiplex(_)
        super
        # Set the GraphQL Batch Executor to what is was
        # before the graphql query started
        existing = Thread.current[REQUEST_EXECUTOR_KEY]
        GraphQL::Batch::Executor.current = existing
      end

      def instrument(type, field)
        # The logic which was previously here is now in Mutations::Base,
        # but this object is still hooked up as a field instrumenter,
        # so we'll implement it as a no-op
        field
      end
    end
  end
end
