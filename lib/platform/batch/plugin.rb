# frozen_string_literal: true

module Platform
  module Batch
    module Plugin
      def self.use(schema)
        instrumentation = Platform::Batch::Setup.new(schema, executor_class: Platform::Batch::GraphqlExecutor)
        schema.instrument(:multiplex, instrumentation)
        schema.lazy_resolve(::Promise, :sync)
      end
    end
  end
end
