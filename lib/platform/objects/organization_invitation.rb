# frozen_string_literal: true

module Platform
  module Objects
    class OrganizationInvitation < Platform::Objects::Base
      description "An Invitation for a user to an organization."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, org_invitation)
        org_invitation.async_organization.then do |org|
          permission.access_allowed?(
            :v4_read_org_invitations,
            resource: org,
            current_org: org,
            current_repo: nil,
            allow_integrations: true,
            allow_user_via_integration: true,
          )
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return true if permission.viewer.site_admin?

        object.async_organization.then do |org|
          org.async_adminable_by?(permission.viewer).then do |org_adminable|
            next true if org_adminable
            if permission.viewer.try(:installation)
              next true if org.resources.members.readable_by?(permission.viewer)
            end

            org.async_business.then do |business|
              next true if business&.owner?(permission.viewer)

              object.async_teams.then do |teams|
                Promise
                  .all(teams.map { |single_team| single_team.async_adminable_by?(permission.viewer) })
                  .then { |team_adminables| team_adminables.any? }
              end
            end
          end
        end
      end

      minimum_accepted_scopes ["admin:org", "admin:enterprise"]

      implements Platform::Interfaces::Node

      global_id_field :id

      database_id_field(visibility: :internal)

      created_at_field

      field :organization, Objects::Organization, method: :async_organization, description: "The organization the invite is for", null: false

      field :email, String, description: "The email address of the user invited to the organization.", null: true

      def email
        return @object.email unless @object.email.nil?
        @object.async_invitee.then do |invitee|
          invitee.async_profile.then do |profile|
            profile&.email.presence
          end
        end
      end

      field :invitation_type, Enums::OrganizationInvitationType, description: "The type of invitation that was sent (e.g. email, user).", null: false

      def invitation_type
        @object.email.present? ? :email : :user
      end

      field :role, Enums::OrganizationInvitationRole, "The user's pending role in the organization (e.g. member, owner).", null: false

      field :inviter, Objects::User, method: :async_inviter, description: "The user who created the invitation.", null: false

      field :invitee, Objects::User, method: :async_invitee, description: "The user who was invited to the organization.", null: true

      field :teams, Connections.define(Objects::Team), numeric_pagination_enabled: true, visibility: :internal, description: "A list of teams the user will be added to", null: false, connection: true

      def teams(**numeric_pagination_args)
        @object.teams.scoped
      end
    end
  end
end
