# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsActivity < Platform::Objects::Base
      description "Events related to sponsorship activity."

      visibility :under_development

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      # Ability check for a SponsorsActivity object
      #
      # Maintainers may see their own activity, and sponsors admins may
      # see all activity.
      def self.async_viewer_can_see?(permission, object)
        if permission.viewer.nil?
          false
        elsif object.sponsorable_is_User? && object.sponsorable_id == permission.viewer.id
          true
        else
          permission.viewer.can_admin_sponsors_listings?
        end
      end

      minimum_accepted_scopes ["user"]

      implements Platform::Interfaces::Node

      created_at_field
      updated_at_field
      global_id_field :id

      field :is_new_sponsorship, Boolean, "Is this event a new sponsorship?", null: false, method: :new_sponsorship?
      field :is_cancelled_sponsorship, Boolean, "Is this event a cancelled sponsorship?", null: false, method: :cancelled_sponsorship?
      field :is_tier_change, Boolean, "Is this event a tier change?", null: false, method: :tier_change?
      field :is_refund, Boolean, "Is this event a refund?", null: false, method: :refund?

      field :timestamp, Scalars::DateTime, description: "The timestamp of this event", null: true
      field :sponsor, Platform::Unions::Account, description: "The sponsor who performed this activity", null: true
      field :sponsors_tier, Objects::SponsorsTier, description: "The associated sponsorship tier", null: true
      field :old_sponsors_tier, Objects::SponsorsTier, description: "The old sponsorship tier for tier change events", null: true
    end
  end
end
