# frozen_string_literal: true

module Platform
  module Objects
    class Query < Platform::Objects::Base
      # This custom field will _skip_ resolution if we've detected
      # that this is a `rateLimit(dryRun: true)` request.
      # (But it still resolves the `rateLimit` fields in that case.)
      class QueryRootField < Platform::Objects::Base::Field
        def resolve(obj, args, ctx)
          if ctx[:rate_limit_dry_run] && name != "rateLimit"
            ctx.skip
          else
            super
          end
        end
      end
      field_class QueryRootField

      description "The query root of GitHub's GraphQL interface."
      areas_of_responsibility :ecosystem_api

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      # Required because Query.relay returns `Query`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      def self.async_api_can_access?(_permission, _object)
        true
      end

      field :node, Platform::Interfaces::Node, null: true,
        description: "Fetches an object given its ID." do
          argument :id, ID, required: true, description: "ID of the object."
        end

      def node(id:)
        context.schema.object_from_id(id, context)
      end

      field :nodes, [Platform::Interfaces::Node, null: true],
        null: false,
        description: "Lookup nodes by a list of IDs." do
        argument :ids, [ID], "The list of node IDs.", required: true
      end

      def nodes(ids:)
        if Platform.unsafe_origin?(@context[:origin]) && ids.size > 100
          raise Errors::ArgumentLimit, "You may not provide more than 100 node ids; you provided #{ids.size}."
        end

        handle_node = ->(n) { n }
        # An error handler so that some IDs can be `NOT_FOUND` and replaced with `nil`
        # while others are correctly returned.
        # (Without this handler, a single raised `NOT_FOUND` would nullify the whole response)
        handle_error = -> (err) {
          if err.is_a?(GraphQL::ExecutionError)
            # Let GraphQL-Ruby handle this error & add it to the response
            err
          else
            # This error should cause a crash, it's an internal error of some kind
            raise err # rubocop:disable GitHub/UsePlatformErrors
          end
        }

        # For each of the given IDs, load an object by that ID
        ids.map do |id|
          Promise.resolve(nil).then {
            Platform::Helpers::NodeIdentification.untyped_object_from_id(id, permission: @context[:permission])
          }.then(handle_node, handle_error)
        end
      end

      field :resource, Interfaces::UniformResourceLocatable, description: "Lookup resource by a URL.", null: true, areas_of_responsibility: :ecosystem_api do
        argument :url, Scalars::URI, "The URL.", required: true
      end

      def resource(**arguments)
        url = arguments[:url]
        begin
          params = GitHub::Application.routes.recognize_path(url.path, method: :get)
        rescue ActionController::RoutingError, TypeError
          params = {}
        end

        types = {
          ["files", "disambiguate"] => Objects::Repository,
          ["issues", "show"] => Objects::Issue,
          ["milestones", "show"] => Objects::Milestone,
          ["orgs/team_discussions", "show"] => Objects::TeamDiscussion,
          ["pull_requests", "show"] => Objects::PullRequest,
          ["commit", "show"] => Objects::Commit,
          ["releases", "show"] => Objects::Release,
          ["profiles", "show"] => Objects::User,
        }

        type = types[[params[:controller], params[:action]]]
        object = type.respond_to?(:load_from_params) ?
          type.load_from_params(params) :
          nil

        Promise.resolve(object).then { |object|
          if object
            context[:permission].typed_can_see?(type, object).then { |readable|
              readable ? object : nil
            }
          end
        }
      end

      field :viewer, Objects::User, description: "The currently authenticated user.", null: false

      def viewer
        @context[:viewer]
      end

      field :requester, Unions::Requester, description: "Who the query was requested by.", null: false, visibility: :under_development

      def requester
        case @context[:viewer]
        when ::User
          Platform::Models::RequestingUser.new(@context[:viewer])
        end
      end

      field :rate_limit, Objects::RateLimit, description: "The client's rate limit information.", null: true do
        argument :dry_run, Boolean, "If true, calculate the cost for the query without evaluating it", default_value: false, required: false
      end

      def rate_limit(**arguments)
        if GitHub.rate_limiting_enabled? && @context[:cost_limiter].present?
          @context[:cost_limiter].rate_limit
        end
      end

      field :temp_nullable_viewer, Objects::User, visibility: :internal, description: "A (temporary) nullable version of viewer", null: true do
      end

      def temp_nullable_viewer
        @context[:viewer]
      end

      field :account, Platform::Unions::Account, visibility: :internal, description: "Lookup an account by database ID.", null: true do
        argument :account_id, Integer, "The account's database ID.", required: true
      end

      def account(**arguments)
        Loaders::ActiveRecord.load(::User, arguments[:account_id], column: :id).then do |account|
          if account && !account.hide_from_user?(@context[:viewer])
            if account.user? || account.organization?
              account
            end
          end
        end
      end

      field :user, Objects::User, description: "Lookup a user by login.", null: true do
        argument :login, String, "The user's login.", required: true
      end

      def user(**arguments)
        Loaders::ActiveRecord.load(::User, arguments[:login], column: :login, case_sensitive: false).then do |user|
          if user && !user.hide_from_user?(@context[:viewer]) && user.user?
            user
          else

            raise Errors::NotFound, "Could not resolve to a User with the login of '#{arguments[:login]}'."
          end
        end
      end

      field :users, resolver: Platform::Resolvers::AllUsers, visibility: { public: { environments: [:enterprise] } }, description: "A list of users.", connection: true

      field :organization, Objects::Organization, description: "Lookup a organization by login.", null: true do
        argument :login, String, "The organization's login.", required: true
      end

      def organization(**arguments)
        Loaders::ActiveRecord.load(::User, arguments[:login], column: :login, case_sensitive: false).then do |user|
          if user && user.organization? && !user.hide_from_user?(@context[:viewer])
            user
          else
            raise Errors::NotFound, "Could not resolve to an Organization with the login of '#{arguments[:login]}'."
          end
        end
      end

      field :enterprise, Objects::Enterprise, description: "Look up an enterprise by URL slug.", null: true do
        argument :slug, String, "The enterprise URL slug.", required: true
        argument :invitation_token, String, "The enterprise invitation token.", required: false
      end

      def enterprise(**arguments)
        Loaders::ActiveRecord.load(::Business, arguments[:slug], column: :slug, case_sensitive: false).then do |business|
          unless business
            raise Errors::NotFound, "Could not resolve to a Business with the URL slug of '#{arguments[:slug]}'."
          end

          Models::Enterprise.new(business, ::BusinessMemberInvitation.digest_token(arguments[:invitation_token]))
        end
      end

      field :enterprise_administrator_invitation, Objects::EnterpriseAdministratorInvitation,
        visibility: { public: { environments: [:dotcom] }, internal: { environments: [:enterprise] } },
        description: "Look up a pending enterprise administrator invitation by invitee, enterprise and role.", null: true do
        argument :user_login, String, "The login of the user invited to join the business.", required: true
        argument :enterprise_slug, String, "The slug of the enterprise the user was invited to join.", required: true
        argument :role, Enums::EnterpriseAdministratorRole, "The role for the business member invitation.", required: true
      end

      def enterprise_administrator_invitation(**arguments)
        Promise.all([
          Loaders::ActiveRecord.load(::Business, arguments[:enterprise_slug], column: :slug, case_sensitive: false),
          Loaders::ActiveRecord.load(::User, arguments[:user_login], column: :login, case_sensitive: false),
        ]).then do |business, user|
          if business.nil?
            raise Errors::NotFound, "Could not resolve to an enterprise with the URL slug of '#{arguments[:enterprise_slug]}'"
          elsif user.nil?
            raise Errors::NotFound, "Could not resolve to a user with the login '#{arguments[:user_login]}'"
          end

          invitation = ::BusinessMemberInvitation.pending.find_by(role: arguments[:role], business: business, invitee: user)
          if invitation.nil?
            raise Errors::NotFound, "Could not resolve to a pending invitation for #{user.login} to join #{business.name} in the #{arguments[:role]} role."
          end

          invitation
        end
      end

      field :enterprise_administrator_invitation_by_token, Objects::EnterpriseAdministratorInvitation,
        visibility: { public: { environments: [:dotcom] }, internal: { environments: [:enterprise] } },
        description: "Look up a pending enterprise administrator invitation by invitation token.", null: true do
        argument :invitation_token, String, "The invitation token sent with the invitation email.", required: true
      end

      def enterprise_administrator_invitation_by_token(**arguments)
        hashed_token = ::BusinessMemberInvitation.digest_token(arguments[:invitation_token])
        Loaders::ActiveRecord.load(::BusinessMemberInvitation, hashed_token, column: :hashed_token, case_sensitive: true).then do |invitation|
          if invitation.nil? || !invitation.pending?
            raise Errors::NotFound, "Could not resolve to a pending invitation for token #{arguments[:invitation_token]}."
          end

          invitation
        end
      end

      field :repository_action_by_slug, Objects::RepositoryAction, description: "Lookup a single GitHub Action by slug.", null: true do
        argument :slug, String, "Select the action that matches this slug.", required: true
      end

      def repository_action_by_slug(**arguments)
        ::RepositoryAction.find_by(slug: arguments[:slug])
      end

      field :repository_action, Objects::RepositoryAction, description: "Lookup a single GitHub Action", null: true do
        argument :id, ID, "Select the action that matches this id.", required: true
      end

      def repository_action(**arguments)
        ::RepositoryAction.find_by(id: arguments[:id])
      end

      field :repository_actions, Connections.define(RepositoryAction, name: "RepositoryActions", visibility: :internal), visibility: :internal,
        description: "A list of repository actions.", null: true, connection: true do
        argument :featured_only, Boolean, "Select only actions that are currently featured.", default_value: false, visibility: :internal, required: false
        argument :writable_only, Boolean, "Select only actions that the Viewer has write access to.", default_value: false, visibility: :internal, required: false
        argument :public_only, Boolean, "Select only actions that are publically accessible.", default_value: false, visibility: :internal, required: false
        argument :order_by, Inputs::RepositoryActionOrder, "Ordering options for the returned repository actions.", required: false,
          default_value: { field: "created_at", direction: "DESC" }
        argument :filter_by, Inputs::RepositoryActionFilters, "Return only actions that match all given filters.", visibility: :internal, required: false, default_value: {}
      end

      def repository_actions(**arguments)
        actions = ::RepositoryAction.discoverable
        actions = actions.featured if arguments[:featured_only]

        if arguments[:order_by]
          field = arguments[:order_by][:field]
          direction = arguments[:order_by][:direction]
          actions = actions.order("repository_actions.#{field} #{direction}")
        end

        if arguments[:filter_by]
          if (featured_val = arguments[:filter_by][:featured]).present?
            actions = actions.where(featured: featured_val)
          end

          if (action_name = arguments[:filter_by][:name]).present?
            actions = actions.with_name(action_name)
          end

          if (category_slug = arguments[:filter_by][:category]).present?
            actions = actions.with_category(category_slug)
          end

          if (action_state = arguments[:filter_by][:state]).present?
            actions = actions.with_state(action_state)
          end

          if (owner_name = arguments[:filter_by][:owner]).present?
            actions = actions.owned_by(owner_name)
          end
        end

        actions = if arguments[:public_only] || @context[:viewer].nil?
          actions.joins(:repository).where("repositories.public = TRUE")
        elsif arguments[:writable_only]
          associated_ids = @context[:viewer].associated_repository_ids(min_action: :write)
          actions.joins(:repository).where("repositories.id IN (?)", associated_ids)
        else
          associated_ids = @context[:viewer].associated_repository_ids(min_action: :read)
          actions.joins(:repository).where("repositories.public = TRUE OR repositories.id IN (?)", associated_ids)
        end

        actions
      end

      field :repository_owner, Interfaces::RepositoryOwner, description: "Lookup a repository owner (ie. either a User or an Organization) by login.", null: true do
        argument :login, String, "The username to lookup the owner by.", required: true
      end

      def repository_owner(**arguments)
        Loaders::ActiveRecord.load(::User, arguments[:login], column: :login, case_sensitive: false).then do |user|
          @context[:permission].typed_can_see?("User", user).then do |owner_readable|
            if user && owner_readable && !user.hide_from_user?(@context[:viewer])
              user
            else
              if GitHub.flipper[:error_for_not_found_repo_owner].enabled?(viewer)
                raise Errors::NotFound, "Could not resolve to a User or Organization with the login of '#{arguments[:login]}'."
              else
                nil
              end
            end
          end
        end
      end

      field :organizations, resolver: Resolvers::Organizations, visibility: { public: { environments: [:enterprise] } }, description: "A list of organizations.", connection: true

      field :public_repositories, Connections::Repository, visibility: :internal, description: <<~DESCRIPTION, null: false, connection: true do
          Look up multiple public repositories by their owners and names, as well as by database ID.
        DESCRIPTION

        argument :names_with_owners, [String], "A list of repository owners and names, e.g., github/linguist.", required: false
        argument :database_ids, [Integer], "A list of repository database IDs.", required: false
      end

      def public_repositories(**arguments)
        conditions = []
        args = []

        if arguments[:database_ids].present?
          conditions << "repositories.id IN (?)"
          args << arguments[:database_ids]
        end

        if arguments[:names_with_owners].present?
          owners = arguments[:names_with_owners].map do |nwo|
            owner, name = nwo.split("/")
            owner
          end

          users = ::User.where(login: owners.uniq).all
          users_hash = users.inject({}) do |hash, user|
            hash[user.login] = user.id
            hash
          end

          arguments[:names_with_owners].each do |nwo|
            conditions << "(repositories.owner_id = ? AND repositories.name = ?)"
            owner, name = nwo.split("/")
            args << users_hash[owner]
            args << name
          end
        end

        repositories = ::Repository.public_scope
        repositories = repositories.where(conditions.join(" OR "), *args) if conditions.present?
        repositories.filter_spam_and_disabled_for(@context[:viewer])
      end

      field :staff_accessed_repository, Objects::StaffAccessedRepository, null: true,
        description: "A way for GitHub staff members to see a narrow subset of data about any repository in the system",
        visibility: :internal do
          argument :owner, String, "The login field of a user or organization", required: true
          argument :name, String, "The name of the repository", required: true
        end

      # This field does _not_ use `Platform::Security::RepositoryAccess` because it returns a
      # `StaffAccessedRepository`, which only allows staff and only exposes a few repository fields
      def staff_accessed_repository(name:, owner:)
        Loaders::ActiveRecord.load(::User, owner, column: :login, case_sensitive: false).then do |owner|
          could_not_resolve_owner = "Could not resolve to a User with the username '#{owner}'."
          could_not_resolve_repo  = "Could not resolve to a Repository."
          if owner.blank?
            raise Errors::NotFound, could_not_resolve_owner
          end
          @context[:permission].typed_can_see?("User", owner).then do |owner_readable|
            if !owner_readable
              raise Errors::NotFound, could_not_resolve_owner
            end

            repo = owner.find_repo_by_name(name)

            if repo.blank?
              raise Errors::NotFound, could_not_resolve_repo
            end

            @context[:permission].typed_can_see?("Repository", repo).then do |repo_readable|
              repo_readable ? repo : raise(Errors::NotFound, could_not_resolve_repo)
            end
          end
        end
      end

      field :repository, Objects::Repository, description: "Lookup a given repository by the owner and repository name.", null: true do
        argument :owner, String, "The login field of a user or organization", required: true
        argument :name, String, "The name of the repository", required: true
      end

      def repository(**arguments)
        Platform::Helpers::RepositoryByNwo.async_repository_with_owner(
          permission: @context[:permission],
          viewer: @context[:viewer],
          login: arguments[:owner],
          name: arguments[:name],
          follow_repo_redirect: true,
        )
      end

      field :package_owner, Interfaces::PackageOwner, visibility: :internal, description: "Lookup a registry package owner", null: true do
        argument :login, String, "The name to lookup the owner by.", required: true
      end

      def package_owner(**arguments)
        Loaders::ActiveRecord.load(::User, arguments[:login], column: :login, case_sensitive: false).then do |user|
          next unless user
          @context[:permission].typed_can_see?("User", user).then do |owner_readable|
            if owner_readable && !user.hide_from_user?(@context[:viewer])
              user
            else
              nil
            end
          end
        end
      end

      field :package_search, Interfaces::PackageSearch, visibility: :internal, description: "Search packages under a registry package owner", null: true do
        argument :login, String, "The name to lookup the owner by.", required: true
      end

      def package_search(**arguments)
        Loaders::ActiveRecord.load(::User, arguments[:login], column: :login, case_sensitive: false).then do |user|
          next unless user
          @context[:permission].typed_can_see?("User", user).then do |owner_readable|
            if owner_readable && !user.hide_from_user?(@context[:viewer])
              user
            else
              nil
            end
          end
        end
      end

      field :search, Connections::SearchResultItem, description: "Perform a search across resources.", null: false, connection: true do
        argument :query, String, "The search string to look for.", required: true
        argument :type, Enums::SearchType, "The types of search items to search within.", required: true
        argument :aggregations, Boolean, "Calculate aggregations. This arg must be true for `languageAggregations` to be returned.", default_value: false, visibility: :internal, required: false
      end

      def search(**arguments)
        query_class = case arguments[:type]
                when "Issues";       ::Search::Queries::IssueQuery
                when "Repositories"; ::Search::Queries::RepoQuery
                when "Users";        ::Search::Queries::UserQuery
                when "UserLogin";    ::Search::Queries::UserLoginQuery
        end

        query_class.new \
          allow_insecure_user_to_server_app_query: !GitHub.flipper[:secure_user_to_server_search].enabled?,
          current_user: @context[:viewer],
          user_session: @context[:user_session],
          remote_ip: @context[:ip],
          current_installation: @context[:installation],
          aggregations: arguments[:aggregations],
          phrase: arguments[:query],
          highlight: ::Search::OffsetHighlighter.defaults,
          context: "graphql-search",
          normalizer: ->(results) do
            results.each do |result|
              ::Search::OffsetHighlighter.transform result["highlight"]
            end
          end
      end

      field :jobs, [Objects::JobDepartment], visibility: :internal, description: "Get list of open positions for working at GitHub.", null: false

      def jobs
        GitHub::Greenhouse.full_time_jobs
      end

      field :internships, [Objects::JobPosition], visibility: :internal, description: "Get list of open internship positions for GitHub.", null: false

      def internships
        GitHub::Greenhouse.internships
      end

      field :marketing_user_count, Integer, visibility: :internal, description: "Get an estimate number of users for marketing purposes.", null: false

      def marketing_user_count
        GitHub::MarketingStats.user_millions * 1_000_000
      end

      field :marketing_repository_count, Integer, visibility: :internal, description: "Get an estimate number of repositories for marketing purposes.", null: false

      def marketing_repository_count
        GitHub::MarketingStats.repository_millions * 1_000_000
      end

      field :marketing_paid_team_accounts_count, Integer, visibility: :internal, description: "Get an estimate number of paid team accounts for marketing purposes.", null: false

      def marketing_paid_team_accounts_count
        GitHub::MarketingStats.paid_orgs_count
      end

      field :employees_count, Integer, visibility: :internal, description: "Get the number of public GitHub employees.", null: false

      def employees_count
        hidden = %w(maki1022)
        ::User.employees.count + ::User.interns.count + hidden.count
      end

      field :enabled_features, [Enums::FeatureFlag], visibility: :internal, description: "Get set of client side enabled feature flags for viewer.", null: false

      def enabled_features
        possible_flags = Platform::Enums::FeatureFlag.values.values.map(&:value)
        loaded_features = GitHub.flipper.preload(possible_flags)
        actor = GitHub.enterprise? ? nil : @context[:viewer]

        features = loaded_features.select { |feature| feature.enabled?(actor) }

        feature_flags = features.map { |feature| feature.name.to_sym }
        feature_flags
      end

      field :stafftools_info, Objects::StafftoolsInfo, visibility: :internal, description: "Stafftools information.", null: true

      def stafftools_info
        if Platform::Objects::Base::SiteAdminCheck.viewer_is_site_admin?(context[:viewer], self.class.name)
          @context[:viewer] # just need to return something other than nil
        else
          nil
        end
      end

      field :biztools_info, Objects::BiztoolsInfo, visibility: :internal, description: "Biztools information.", null: true

      def biztools_info
        return if GitHub.enterprise?
        return unless context[:viewer].present?
        return unless context[:viewer].biztools_user? || context[:viewer].site_admin?

        @context[:viewer] # just need to return something other than nil
      end

      field :account_from_database_id, Unions::Account, null: true, minimum_accepted_scopes: ["site_admin"] do
        description "Account from database id."
        visibility :internal
        argument :database_id, Int, required: true, description: "The database id of the account."
      end

      def account_from_database_id(database_id:)
        if Platform::Objects::Base::SiteAdminCheck.viewer_is_site_admin?(context[:viewer], self.class.name)
          Loaders::ActiveRecord.load(::User, database_id)
        else
          nil
        end
      end

      field :accounts_from_last_ip, Connections.define(Unions::Account), null: true, minimum_accepted_scopes: ["site_admin"] do
        description "Lookup user, organization, and bot accounts from last IP."
        visibility :internal
        argument :ip, String, required: true, description: "IPv4 network address."
        argument :prefix, Enums::NetworkPrefix, required: false, description: Enums::NetworkPrefix.description, default_value: 32
      end

      def accounts_from_last_ip(ip:, prefix: nil)
        if Platform::Objects::Base::SiteAdminCheck.viewer_is_site_admin?(context[:viewer], self.class.name)
          ::User.by_ip_with_prefix(ip, prefix: prefix)
        else
          nil
        end
      end

      field :relay, Query, description: "Hack to workaround https://github.com/facebook/relay/issues/112 re-exposing the root query object", null: false

      def relay
        self
      end

      field :latest_unread_broadcast, Objects::BlogBroadcast, visibility: :internal, description: "Find the most recent unread broadcast post for the viewer.", null: true

      ESSENTIAL_BROADCAST_KEYS = %w[
        date_published
        title
        url
        content_html
      ].freeze

      def latest_unread_broadcast
        return nil unless GitHub.blog_enabled?
        return nil unless @context[:viewer]

        broadcasts = Platform::Helpers::BlogBroadcast.json_feed
        return nil if broadcasts.blank?

        last_read_id = @context[:viewer].last_read_broadcast_id
        latest_broadcast = broadcasts.first

        ESSENTIAL_BROADCAST_KEYS.each do |key|
          return nil if latest_broadcast[key].blank?
        end

        id = DateTime.parse(latest_broadcast["date_published"]).to_i
        return nil if last_read_id && last_read_id >= id

        Platform::Models::BlogBroadcast.new(
          id: id,
          title: latest_broadcast["title"],
          url: latest_broadcast["url"],
          content: latest_broadcast["content_html"],
        )
      end

      field :unread_broadcasts_count, Integer, visibility: :internal, description: "Get the number of unread broadcast posts (or -1 if unknown) for the viewer.", null: false

      def unread_broadcasts_count
        return 0 unless GitHub.blog_enabled?
        return 0 unless @context[:viewer]

        broadcasts = Platform::Helpers::BlogBroadcast.json_feed
        return 0 if broadcasts.blank?

        broadcasts.reject! do |broadcast|
          ESSENTIAL_BROADCAST_KEYS.find { |k| broadcast[k].blank? }
        end
        return 0 if broadcasts.blank?

        last_read_id = @context[:viewer].last_read_broadcast_id
        return -1 unless last_read_id

        broadcasts_ids = broadcasts.map do |broadcast|
          DateTime.parse(broadcast["date_published"]).to_i
        end

        return  0 if last_read_id >= broadcasts_ids.first
        return -1 if last_read_id <  broadcasts_ids.last
        [last_read_id, *broadcasts_ids].sort.reverse.index(last_read_id)
      end

      field :viewer_can_create_posts, Boolean, visibility: :internal, description: "Is the viewer allowed to create new blog posts.", null: false

      def viewer_can_create_posts
        return false unless GitHub.blog_enabled?
        return false unless @context[:viewer]
        @context[:viewer].blogger?
      end

      field :integration_listing, Platform::Objects::IntegrationListing, visibility: :internal, description: "Lookup a single Integration listing", null: true do
        argument :slug, String, "Select the listing which matches this slug", required: true
      end

      def integration_listing(**arguments)
        query = ::IntegrationListing

        query = if @context[:viewer] && @context[:viewer].site_admin?
          query.draft_and_published
        else
          query.published
        end

        query.find_by_slug(arguments[:slug])
      end

      field :integration_listings, Connections.define(Platform::Objects::IntegrationListing), visibility: :internal, description: "Lookup Integration listings", null: false, connection: true do
        argument :slug, String, "Select only listings with the given category slug.", required: false
        argument :query, String, "Select listings whose name or description matches the query.", required: false
      end

      def integration_listings(**arguments)
        query = ::IntegrationListing.
          with_feature(arguments[:slug]).
          matches_name_or_description(arguments[:query])

        if @context[:viewer] && @context[:viewer].site_admin?
          query.draft_and_published
        else
          query.published
        end
      end

      field :integration_categories, [Objects::IntegrationFeature], visibility: :internal, description: "Get alphabetically sorted list of Integration categories", null: false do
        argument :slug, String, "An optional category slug to select a specific category.", required: false
        argument :exclude_empty, Boolean, "Exclude categories with no Works with GitHub listings.", required: false
      end

      def integration_categories(**arguments)
        categories = ::IntegrationFeature
        categories = categories.with_non_marketplace_listings if arguments[:exclude_empty]

        if slug = arguments[:slug]
          categories = categories.where(slug: slug)
        end

        categories.at_least_visible.order(:name)
      end

      # TODO: replace areas_of_responsibility with map_to_service equivalent for GitHub Sponsors team
      field :sponsors_listing, Objects::SponsorsListing, description: "Look up a single Sponsors Listing", null: true do
        argument :slug, String, "Select the Sponsors listing which matches this slug", required: true
        deprecated(
          start_date: Date.new(2019, 10, 29),
          reason: "`Query.sponsorsListing` will be removed.",
          superseded_by: "Use `Sponsorable.sponsorsListing` instead.",
          owner: "antn",
        )
      end

      def sponsors_listing(**arguments)
        if Rails.env.test? || Rails.env.development?
          raise Platform::Errors::Unprocessable, "Query.sponsorsListing is deprecated, please use Sponsorable.sponsorsListing"
        end

        promise = Loaders::ActiveRecord.load(::SponsorsListing, arguments[:slug], column: :slug)
        promise.then do |listing|
          if listing
            listing.async_sponsorable.then do
              @context[:permission].typed_can_see?("SponsorsListing", listing).then do |listing_readable|
                listing if listing_readable
              end
            end
          end
        end
      end

      field :marketplace_listing, Objects::MarketplaceListing,
        description: "Look up a single Marketplace listing", null: true, areas_of_responsibility: :marketplace do
        argument :slug, String, "Select the listing that matches this slug. It's the short name of the listing used in its URL.", required: true
      end

      def marketplace_listing(**arguments)
        promise = Loaders::ActiveRecord.load(::Marketplace::Listing, arguments[:slug], column: :slug)
        promise.then do |listing|
          if listing
            listing.async_owner.then do
              @context[:permission].typed_can_see?("MarketplaceListing", listing).then do |listing_readable|
                listing if listing_readable
              end
            end
          end
        end
      end

      field :marketplace_end_user_agreement, Objects::MarketplaceAgreement,
        areas_of_responsibility: :marketplace,
        visibility:              :internal,
        description:             "Get the latest version of the GitHub Marketplace Terms of Service.",
        null:                    true

      def marketplace_end_user_agreement
        ::Marketplace::Agreement.latest_for_end_users
      end

      field :marketplace_integrator_agreement, Objects::MarketplaceAgreement,
        areas_of_responsibility: :marketplace,
        visibility:              :internal,
        description:             "Get the latest version of the GitHub Marketplace Developer Agreement.",
        null:                    true

      def marketplace_integrator_agreement
        ::Marketplace::Agreement.latest_for_integrators
      end

      field :marketplace_agreements, Connections.define(Objects::MarketplaceAgreement),
        areas_of_responsibility: :marketplace,
        visibility:              :internal,
        description:             "Look up Marketplace agreements sorted with the latest version first.",
        null:                    false,
        connection:              true do
        argument :for_integrators, Boolean, <<~DESCRIPTION, required: false
          Select only integrator agreements. Defaults to including both integrator and end-user
          agreements. Set to false to select only end-user agreements.
        DESCRIPTION
      end

      def marketplace_agreements(**arguments)
        agreements = ::Marketplace::Agreement
        if arguments[:for_integrators] == false
          agreements = agreements.end_user
        elsif arguments[:for_integrators]
          agreements = agreements.integrator
        end
        agreements.latest
      end

      field :marketplace_stories, Connections.define(Objects::MarketplaceStory),
        areas_of_responsibility: :marketplace,
        visibility: :internal,
        description: "A collection of blog posts related to the Marketplace.",
        null: false,
        connection: true do
          argument :featured_only, Boolean, "Whether or not we only want featured blog posts", required: false, default_value: false
          argument :order_by, Inputs::MarketplaceStoryOrder, "Ordering options for the returned stories", required: false,
            default_value: { field: "published_at", direction: "DESC" }
        end

      def marketplace_stories(**arguments)
        stories = ::Marketplace::Story.all

        if arguments[:featured_only]
          stories = stories.where(featured: true)
        end

        if arguments[:order_by].present?
          field = arguments[:order_by][:field]
          direction = arguments[:order_by][:direction]
          stories = stories.unscope(:order).order("#{field} #{direction}")
        end

        ArrayWrapper.new(stories)
      end

      field :non_marketplace_listings, Connections.define(Objects::NonMarketplaceListing),
        null: false,
        visibility: :internal,
        description: "Look up Works with GitHub listings" do
          argument :category_slug, String, "Select only listings with the given category.", required: false
          argument :query, String, "Select listings whose name or description matches the query.", required: false
          argument :viewer_can_admin, Boolean, "Select listings to which the authenticated user has admin access.", required: false
          argument :viewer_can_edit, Boolean, "Select listings that the authenticated user can edit.", required: false
          argument :creator_login, String, "Select listings submitted by the specified user.", required: false
          argument :enterprise_compatible_only, Boolean, "Select listings that are compatible with GitHub Enterprise.", required: false
        end

      def non_marketplace_listings(**arguments)
        listings = ::NonMarketplaceListing.includes(:integration_listing)

        if (slug = arguments[:category_slug]).present?
          listings = listings.with_category(slug)
        end

        if (query = arguments[:query]).present?
          listings = listings.matches_name_or_description(query)
        end

        if arguments[:enterprise_compatible_only]
          listings = listings.enterprise_compatible
        end

        admin_only = arguments[:viewer_can_admin]
        viewer_is_admin = @context[:viewer] &&
          @context[:viewer].can_admin_non_marketplace_listings?

        if (creator_login = arguments[:creator_login]).present?
          specified_user_is_viewer = @context[:viewer] && creator_login == @context[:viewer].login

          if specified_user_is_viewer || viewer_is_admin
            listings = listings.created_by(creator_login)
          end
        end

        if admin_only && viewer_is_admin
          listings.order_by_most_recent
        else
          if @context[:viewer] && arguments[:viewer_can_edit]
            listings.created_by(@context[:viewer].login).with_draft_state.order_by_most_recent
          else
            listings.with_approved_state.order_by_name
          end
        end
      end

      field :marketplace_search, Connections::SearchResultItem,
        areas_of_responsibility: :marketplace,
        visibility:              :internal,
        description:             "Search GitHub Marketplace and Works with GitHub.",
        null:                    false,
        connection:              true do
        argument :query, String, "Select listings that match this text.", required: true
        argument :category_slug, String, "Search listings within the category with this slug.", required: false
        argument :enterprise_compatible_only, Boolean, "Include only listings that are compatible with GitHub Enterprise.", required: false
        argument :type, Enums::MarketplaceSearchType, <<~DESCRIPTION, required: false
          What kind of search results to return. Defaults to including both GitHub Marketplace and
          Works with GitHub listings.
        DESCRIPTION
        argument :with_free_trials_only, Boolean, "Select only listings that offer a free trial.", required: false
        argument :verification_state, Enums::MarketplaceVerificationState, "Select the listings in this verification state that are visible to the viewer.", required: false
      end

      def marketplace_search(query:, category_slug: nil, enterprise_compatible_only: nil, type: nil, with_free_trials_only: nil, verification_state: nil)
        promise = if category_slug.present?
          Loaders::ActiveRecord.load(::Marketplace::Category, category_slug, column: :slug).then do |mkt_category|
            Loaders::ActiveRecord.load(::IntegrationFeature, category_slug, column: :slug).then do |wwg_category|
              query += %Q( category:"#{wwg_category.name}") if wwg_category

              if mkt_category
                query += %Q( category:"#{mkt_category.name}" )

                # nb: here we query for sub categories so that when looking at a
                # parent category we also include the items tied to sub
                # categories
                mkt_category.async_sub_categories.then do |sub_categories|
                  sub_categories.each { |sub_category| query += %Q( category:"#{sub_category.name}" ) }
                end
              end
            end
          end
        else
          Promise.resolve(true)
        end

        case verification_state
        when "verified"
          query += %Q( state:verified )
        when "unverified"
          query += %Q( state:unverified state:verification_pending_from_unverified )
        end

        promise.then do
          Search::Queries::MarketplaceQuery.new \
            current_user: @context[:viewer],
            phrase: query,
            type: type,
            offers_free_trial: with_free_trials_only,
            enterprise_compatible: enterprise_compatible_only,
            verification_state: verification_state,
            context: "graphql-marketplace-search",
            highlight: ::Search::OffsetHighlighter.defaults
        end
      end

      field :marketplace_listings, Connections::MarketplaceListing,
        areas_of_responsibility: :marketplace,
        description:             "Look up Marketplace listings",
        null:                    false,
        connection:              true do
        argument :category_slug, String, "Select only listings with the given category.", required: false
        argument :use_topic_aliases, Boolean, "Also check topic aliases for the category slug", required: false
        argument :query, String, "Select listings whose name or description matches the query.", visibility: :internal, required: false
        argument :viewer_can_admin, Boolean, <<~DESCRIPTION, required: false
            Select listings to which user has admin access. If omitted, listings visible to the
            viewer are returned.
          DESCRIPTION
        argument :admin_id, ID, "Select listings that can be administered by the specified user.", required: false
        argument :organization_id, ID, "Select listings for products owned by the specified organization.", required: false
        argument :all_states, Boolean, <<~DESCRIPTION, required: false
            Select listings visible to the viewer even if they are not approved. If omitted or
            false, only approved listings will be returned.
          DESCRIPTION
        argument :state, Enums::MarketplaceListingState, "Select the listings in this state that are visible to the viewer.", required: false
        argument :states, [Enums::MarketplaceListingState], "Select the listings in these states that are visible to the viewer.", required: false, visibility: :under_development
        argument :slugs, [String, null: true], "Select the listings with these slugs, if they are visible to the viewer.", required: false
        argument :primary_category_only, Boolean, "Select only listings where the primary category matches the given category slug.", default_value: false, required: false
        argument :with_free_trials_only, Boolean, "Select only listings that offer a free trial.", default_value: false, required: false
        argument :featured_only, Boolean, "Select only listings that are currently featured.", default_value: false, visibility: :internal, required: false
        argument :order_by, Inputs::MarketplaceListingOrder, "Ordering options for the returned listings.", required: false, default_value: { field: "id", direction: "DESC" }
        argument :sponsorable_only, Boolean, "Select only GitHub Sponsors listings. Excluded if false.", default_value: false, visibility: :internal, required: false
      end

      def marketplace_listings(**arguments)
        viewer = @context[:viewer]

        listings = ::Marketplace::Listing.scoped

        if arguments[:order_by].present?
          field = arguments[:order_by][:field]
          direction = arguments[:order_by][:direction]
          listings = listings.unscope(:order).order("#{field} #{direction}")
        end

        if arguments[:with_free_trials_only]
          listings = listings.with_free_trial(state: :published)
        end

        if arguments[:featured_only]
          listings = listings.featured
        end

        listings = if arguments[:sponsorable_only]
          listings.sponsorable
        else
          listings.non_sponsorable
        end

        if (slug = arguments[:category_slug]).present?
          if arguments[:use_topic_aliases]
            aliased_slug = Marketplace::Category.slug_for_topic(slug)
          end

          listings = listings.with_category(aliased_slug || slug, primary_category_only: arguments[:primary_category_only])
        end

        if (query = arguments[:query]).present?
          listings = listings.matches_name_or_description(query)
        end

        if arguments[:viewer_can_admin]
          if viewer
            listings = listings.editable_by(viewer)
          else
            # Anonymous user cannot admin any listings, so make sure they get an empty list
            listings = listings.where("false")
          end
        end

        if viewer && viewer.can_admin_marketplace_listings?
          # For an admin using biztools: allow filtering to just a particular user
          if (admin_id = arguments[:admin_id]).present?
            user = Helpers::NodeIdentification.
              typed_object_from_id([Objects::User], admin_id, @context)
            listings = listings.editable_by(user)
          end

          # For an admin using biztools: allow filtering to just a particular org
          if (org_id = arguments[:organization_id]).present?
            org = Helpers::NodeIdentification.
              typed_object_from_id([Objects::Organization], org_id, @context)
            listings = listings.for_org(org)
          end
        end

        if (arguments[:all_states] || arguments[:state].present? || arguments[:states].present?) && viewer
          # It may be legit to allow an empty array here, though GraphQL treats that as non present
          if (states = arguments[:states].presence)
            listings = listings.with_states(states)
          elsif (state = arguments[:state]).present?
            listings = listings.with_state(state)
          end

          # Unless you're an admin, you can only see listings regardless of state if
          # they're listings you own, or if they're approved.
          unless viewer.can_admin_marketplace_listings?
            listings = listings.visible_to(viewer)
          end
        elsif !viewer || !arguments[:viewer_can_admin]
          # Default to listings that are public.
          listings = listings.publicly_listed
        end

        listings = listings.where(slug: arguments[:slugs]) if arguments[:slugs].present?

        listings
      end

      field :trending_marketplace_apps, [Objects::MarketplaceListing],
        areas_of_responsibility: :marketplace,
        visibility:              :internal,
        description:             "Returns trending Marketplace apps based on their installation count.",
        null:                    false

      def trending_marketplace_apps
        Platform::Loaders::KV.load("marketplace/trending_apps").then do |raw|
          if raw
            trending_ids = JSON.parse(raw).map { |app| app["id"] }

            Loaders::ActiveRecord.load_all(::Marketplace::Listing, trending_ids).then do |listings|
              listings.select { |listing| listing.is_marketplace? && listing.verified? }
            end
          else
            ::Marketplace::Listing.none
          end
        end
      end

      field :recommended_marketplace_apps, [Objects::MarketplaceListing],
        areas_of_responsibility: :marketplace,
        visibility:              :internal,
        description:             "Returns recommended Marketplace apps",
        null:                    false

      def recommended_marketplace_apps
        Platform::Loaders::KV.load("marketplace/recommendations").then do |raw|
          if raw
            recommended_ids = JSON.parse(raw)
            Loaders::ActiveRecord.load_all(::Marketplace::Listing, recommended_ids).then do |listings|
              listings.select(&:verified?)
            end
          else
            ::Marketplace::Listing.none
          end
        end
      end

      field :marketplace_category, Objects::MarketplaceCategory,
        areas_of_responsibility: :marketplace,
        description:             "Look up a Marketplace category by its slug.",
        null:                    true do
        argument :slug, String, "The URL slug of the category.", required: true
        argument :use_topic_aliases, Boolean, "Also check topic aliases for the category slug", required: false
      end

      def marketplace_category(**arguments)
        slug = arguments[:slug]

        return unless slug.present?

        if arguments[:use_topic_aliases]
          aliased_slug = Marketplace::Category.slug_for_topic(slug)
        end

        Loaders::ActiveRecord.load(::Marketplace::Category, aliased_slug || slug, column: :slug)
      end

      field :marketplace_categories, [Objects::MarketplaceCategory],
        areas_of_responsibility: :marketplace,
        description:             "Get alphabetically sorted list of Marketplace categories",
        null:                    false do
        argument :include_categories, [String], "Return only the specified categories.", required: false
        argument :exclude_empty, Boolean, "Exclude categories with no listings.", required: false
        argument :exclude_subcategories, Boolean, "Returns top level categories only, excluding any subcategories.", required: false
        argument :exclude_filter_categories, Boolean, "Exclude filter-type categories.", required: false, default_value: false, visibility: :under_development
        argument :subcategory_candidates_for, String, "Return categories that can be used as subcategories for the given category slug.", required: false, visibility: :internal
        argument :for_navigation, Boolean, "Only return categories that are visible in side navigation and have listings.", visibility: :internal, required: false, default_value: false
      end

      def marketplace_categories(**arguments)
        categories = ::Marketplace::Category.order(:name)
        categories = categories.not_sponsors_only
        categories = categories.where(slug: arguments[:include_categories]) if arguments[:include_categories]
        categories = categories.with_listings if arguments[:exclude_empty]
        categories = categories.top_level if arguments[:exclude_subcategories]
        categories = categories.where(acts_as_filter: false) if arguments[:exclude_filter_categories]
        categories = categories.subcategory_candidates(for_optional_slug: arguments[:subcategory_candidates_for]) if arguments.key?(:subcategory_candidates_for)
        categories = categories.navigation_visible.with_listings if arguments[:for_navigation]
        categories
      end

      field :featured_marketplace_categories, [Objects::MarketplaceCategory],
        areas_of_responsibility: :marketplace,
        description:             "A list of categories that are featured on the Marketplace homepage",
        null:                    false,
        visibility:              :internal

      def featured_marketplace_categories
        ::Marketplace::Category.featured
      end

      field :marketplace_purchase, Objects::SubscriptionItem,
        areas_of_responsibility: :marketplace,
        visibility:              :internal,
        description:             "Get subscription details for an account's purchase of a Marketplace listing.",
        null:                    true do
        argument :account_id, Integer, "The database ID of the User or Organization account.", required: true
        argument :marketplace_listing_id, Integer, "The database ID of the Marketplace listing.", required: true
      end

      def marketplace_purchase(account_id:, marketplace_listing_id:)
        Loaders::ActiveRecord.load(::User, account_id, column: :id).then do |account|
          account.async_plan_subscription.then do |plan_subscription|
            if plan_subscription
              listing_plans_ids = Marketplace::ListingPlan.where(
                marketplace_listing_id: marketplace_listing_id,
              ).pluck(:id)
              plan_subscription
                .active_subscription_items
                .for_marketplace_listing_plans(listing_plans_ids)
                .first
            else
              nil
            end
          end
        end
      end

      field :collection, Objects::ExploreCollection, description: "Lookup a collection by slug.", null: true do
        argument :slug, String, "The collection's slug.", required: true
      end

      def collection(**arguments)
        Loaders::ActiveRecord.load(::ExploreCollection, arguments[:slug], column: :slug).then do |collection|
          if collection
            collection
          else
            raise Errors::NotFound, "Could not resolve to a Collection with the slug of '#{arguments[:slug]}'."
          end
        end
      end

      field :topic, Objects::Topic, description: "Look up a topic by name.", null: true do
        argument :name, String, "The topic's name.", required: true
      end

      def topic(**arguments)
        Objects::Topic.load_from_global_id(arguments[:name])
      end

      field :popular_topics, [Objects::Topic], visibility: :internal, description: "Returns a list of popular topics featured on GitHub.", null: false do
        argument :limit, Integer, "How many popular topics to return.", default_value: 10, required: false
      end

      def popular_topics(**arguments)
        ::Topic.not_flagged.popular_on_public_repositories(arguments[:limit])
      end

      field :recent_topics, [Objects::Topic], visibility: :internal, description: <<~DESCRIPTION, null: false do
          Returns a list of recently created topics on GitHub that have been applied to at least
          one repository.
        DESCRIPTION

        argument :limit, Integer, "How many recent topics to return.", default_value: 10, required: false
      end

      def recent_topics(**arguments)
        topic_ids = ::RepositoryTopic.applied.on_public_repositories.newest_first.limit(100).
          pluck(:topic_id)
        ::Topic.newest_first.not_flagged.where(id: topic_ids).limit(arguments[:limit])
      end

      field :applied_topics, [Objects::Topic], visibility: :internal, description: "Returns a list of applied topics filtered by `query` if any.", null: false do
        argument :query, String, "Query to search applied topics by", required: false
        argument :limit, Integer, "How many applied topics to return.", default_value: 10, required: false
      end

      def applied_topics(**arguments)
        ::Topic.suggestions_for_autocomplete(query: arguments[:query], limit: arguments[:limit])
      end

      field :featured_topics_sample, [Objects::Topic], visibility: :internal, description: "Returns a few randomly chosen featured, curated topics.", null: false do
        argument :limit, Integer, "How many topics to return.", default_value: 3, required: false
      end

      def featured_topics_sample(**arguments)
        topics = ::Topic.not_flagged.curated.featured.with_logo.newest_first.limit(1_000).
          sample(arguments[:limit])
        ArrayWrapper.new(topics.shuffle)
      end

      field :feature, Objects::Feature, visibility: :internal, description: "Find feature flag by name.", null: true do
        argument :name, String, "The feature name.", required: true
      end

      def feature(**arguments)
        ::FlipperFeature.async_viewer_can_read?(@context[:viewer]).then do |can_read|
          raise Errors::Forbidden.new("Viewer is unauthorized") if !can_read
          ::FlipperFeature.find_by_name(arguments[:name])
        end
      end

      field :features, Connections::Feature, visibility: :internal, description: "Returns a list of feature flags.", null: false, connection: true do
        argument :state, Enums::FeatureState, "State to filter by.", required: false
        argument :query, String, "A query for filtering features by name.", required: false
        argument :with_code_use, Boolean, "Select only feature flags which are referenced in the codebase.", default_value: false, required: false
        argument :without_code_use, Boolean, "Select only feature flags which are not referenced in the codebase.", default_value: false, required: false
        argument :order_by, Inputs::FeatureOrder, "Ordering options for the returned features.", required: false,
          default_value: { field: "name", direction: "ASC" }
      end

      def features(**arguments)
        ::FlipperFeature.async_viewer_can_read?(@context[:viewer]).then do |can_read|
          raise Errors::Forbidden.new("Viewer is unauthorized") if !can_read
          if arguments[:query].present?
            scope = FlipperFeature.matches_name_or_description(arguments[:query])
          else
            scope = case arguments[:state]
            when :enabled
              FlipperFeature.enabled
            when :conditional
              FlipperFeature.conditional
            when :disabled
              FlipperFeature.disabled
            else
              FlipperFeature.scoped
            end
          end

          if arguments[:order_by].present?
            field = arguments[:order_by][:field]
            direction = arguments[:order_by][:direction]
            scope = scope.unscope(:order).order("#{field} #{direction}")
          end

          features = scope

          if arguments[:with_code_use] || arguments[:without_code_use]
            features = features.select { |f| arguments[:with_code_use] ? f.code_usage.any? : f.code_usage.empty? }
            ArrayWrapper.new(features)
          else
            features
          end
        end
      end

      field :feature_history, Connections::FeatureHistoryEntry, visibility: :internal, description: "Returns recent feature flag changes.", null: false, connection: false do
        argument :first, Integer, "How many records to return", default_value: 20, required: false
        argument :include_actors, Boolean, "Whether or not to include individual actor flips", default_value: false, required: false
        argument :feature, String, "A specific feature flag to query by", required: false
      end

      def feature_history(**arguments)
        ::FlipperFeature.async_viewer_can_read?(@context[:viewer]).then do |can_read|
          raise Errors::Forbidden.new("Viewer is unauthorized") if !can_read

          search_args = ["(action:feature.enable OR action:feature.disable)"]
          search_args << "-data.gate_name:actor" unless arguments[:include_actors]
          search_args << "data.feature_name:#{arguments[:feature]}" unless arguments[:feature].blank?

          entries = Audit::Driftwood::Query.new_stafftools_query(
            phrase: search_args.join(" AND "),
            current_user: @context[:viewer],
            per_page: arguments[:first],
          ).execute

          ConnectionWrappers::ArrayWrapper.new(
            ArrayWrapper.new(entries.results),
            arguments,
            parent: @object,
            context: @context,
          )
        end
      end

      field :toggleable_feature, Objects::ToggleableFeature, description: "Find toggleable feature by slug.", null: true, minimum_accepted_scopes: ["devtools"], visibility: :internal, areas_of_responsibility: :feature_lifecycle do
        argument :slug, String, "The toggleable feature slug.", required: true
      end

      def toggleable_feature(**arguments)
        ::Feature.find_by(slug: arguments[:slug])
      end

      field :toggleable_features, Connections.define(Objects::ToggleableFeature), description: "Returns a list of toggleable features.", null: true, minimum_accepted_scopes: ["devtools"], visibility: :internal, areas_of_responsibility: :feature_lifecycle

      def toggleable_features
        ::Feature.all
      end

      field :experiment, Objects::Experiment, visibility: :internal, description: "Find experiment by name.", null: true do
        argument :name, String, "The feature name.", required: true
      end

      def experiment(**arguments)
        ::Experiment.async_viewer_can_read?(@context[:viewer]).then do |can_read|
          raise Errors::Forbidden.new("Viewer is unauthorized") if !can_read
          ::Experiment.find_by_name(arguments[:name])
        end
      end

      field :experiments, Connections::Experiment, visibility: :internal, description: "Returns a list of code experiments.", null: false, connection: true do
        argument :active, Boolean, "Return active experiemtns if true.", required: true
      end

      def experiments(**arguments)
        ::Experiment.async_viewer_can_read?(@context[:viewer]).then do |can_read|
          raise Errors::Forbidden.new("Viewer is unauthorized") if !can_read
          active, inactive = ::Experiment.order(:name).partition(&:active?)
          if arguments[:active]
            ArrayWrapper.new(active)
          else
            ArrayWrapper.new(inactive)
          end
        end
      end

      field :topics, Connections.define(Objects::Topic), visibility: :internal, description: "Returns a list of the topics on GitHub.", null: false, connection: true do
        argument :featured, Boolean, "Set to true to include only the topics featured on GitHub.", required: false
        argument :curated, Boolean, "Set to true to include only the topics that have additional content, such as a " \
          "description.", default_value: false, required: false
        argument :query, String, "A query for filtering topics by name.", required: false
        argument :order_by, Inputs::TopicOrder, "Ordering options for the returned topics.", required: false
        argument :names, [String, null: true], "A list of topic names to include.", required: false
      end

      def topics(**arguments)
        topics = ::Topic.not_flagged
        topics = topics.curated if arguments[:curated]

        if arguments[:names].present?
          names = arguments[:names].select { |name| Topic.valid_name?(name) }
          topics = topics.where(name: names)
        end

        if arguments[:featured]
          topics = topics.featured
        elsif arguments[:featured] == false # as opposed to nil
          topics = topics.non_featured
        end

        if arguments[:query].present?
          topics = topics.with_name_like(arguments[:query])
        end

        if arguments[:order_by]
          field = arguments[:order_by][:field]
          direction = arguments[:order_by][:direction]
          topics = topics.order("topics.#{field} #{direction}")
        end

        topics
      end

      # TODO Before going public, fix `display_names` -> `displayNames` (remove `camelize: false`)
      field :explore_collections, Connections.define(Objects::ExploreCollection), visibility: :internal, description: "Returns a list of the collections on GitHub Explore.", null: false, connection: true do
        argument :featured, Boolean, "Set to true to include only the collections featured on GitHub.", required: false
        argument :query, String, "A query for filtering collections by name.", required: false
        argument :order_by, Inputs::ExploreCollectionOrder, "Ordering options for the returned collections.", required: false
        argument :slugs, [String, null: true], "A list of collection slugs to include.", required: false
        argument :display_names, [String, null: true], "A list of collection names to include.", required: false, camelize: false
      end

      def explore_collections(**arguments)
        collections = ::ExploreCollection.limit(1_000)

        if arguments[:slugs].present?
         slugs = arguments[:slugs].select { |slug| ::ExploreCollection.valid_slug?(slug) }
         collections = collections.where(slug: slugs)
        end

        if arguments[:display_names].present?
         display_names = arguments[:display_names].select { |name| ::ExploreCollection.valid_name?(name) }
         collections = collections.where(display_name: display_names)
        end

        if arguments[:featured]
         collections = collections.featured
        elsif arguments[:featured] == false # as opposed to nil
         collections = collections.non_featured
        end

        if arguments[:query].present?
         collections = collections.with_name_like(arguments[:query])
        end

        if arguments[:order_by]
         field = arguments[:order_by][:field]
         direction = arguments[:order_by][:direction]
         collections = collections.order("collections.#{field} #{direction}")
        end

        collections
      end

      field :featured_collections_sample, [Objects::ExploreCollection], visibility: :internal, description: "Returns a few randomly chosen featured, curated collections.", null: false do
        argument :limit, Integer, "How many collections to return.", default_value: 3, required: false
      end

      def featured_collections_sample(**arguments)
        collections = ::ExploreCollection.featured.limit(1_000).
          sample(arguments[:limit])
        ArrayWrapper.new(collections.shuffle)
      end

      field :trending_developers, [Objects::User, null: true], feature_flag: :pe_mobile, description: "A list of trending developers", null: true

      def trending_developers
        ExploreFeed::Trending::Developer.all
      end

      field :trending_repositories, [Objects::Repository, null: true], feature_flag: :pe_mobile, description: "A list of trending repositories", null: true

      def trending_repositories
        ExploreFeed::Trending::Repository.all
      end

      field :code_of_conduct, Objects::CodeOfConduct, description: "Look up a code of conduct by its key", null: true do
        argument :key, String, "The code of conduct's key", required: true
      end

      def code_of_conduct(**arguments)
        ::CodeOfConduct.find_by_key(arguments[:key])
      end

      field :codes_of_conduct, [Objects::CodeOfConduct, null: true], description: "Look up a code of conduct by its key", null: true

      def codes_of_conduct
        ::CodeOfConduct.recommended
      end

      field :license, Objects::License, description: "Look up an open source license by its key", null: true do
        argument :key, String, "The license's downcased SPDX ID", required: true
      end

      def license(**arguments)
        ::License.find_by_key(arguments[:key])
      end

      field :licenses, [Objects::License, null: true], description: "Return a list of known open source licenses", null: false

      def licenses
        ::License.all
      end

      field :page_certificate, Objects::PageCertificate, description: "Fetch a page certificate by domain", null: false, visibility: :internal do
        argument :domain, String, "The domain of the page certificate", required: true
      end

      def page_certificate(**arguments)
        Loaders::ActiveRecord.load(::Page::Certificate, arguments[:domain], column: :domain).then do |certificate|
          unless certificate
            raise Errors::NotFound, "Could not find a page certificate with domain #{arguments[:domain]}"
          end

          certificate
        end
      end

      field :dependency_graph_packages, Connections.define(Objects::DependencyGraphPackage), visibility: :internal, description: "A package indexed in dependency graph", areas_of_responsibility: :dependency_graph, null: false do
        argument :names, [String], "The package name", required: false
        argument :package_manager, String, "The package manager", required: false
        argument :limit, Integer, "Total number of packages to return", required: false
      end

      def dependency_graph_packages(**arguments)
        Loaders::Dependencies.load_packages(
          package_filter: {
            first:           arguments[:first],
            names:           arguments[:names],
            package_manager: arguments[:package_manager],
          }).then { |result| ArrayWrapper.new(result.value!) }
      end

      field :dependency_graph_unmapped_packages, Connections.define(Objects::DependencyGraphPackage), visibility: :internal, description: "A package indexed in dependency graph that is not mapped to a repository", areas_of_responsibility: :dependency_graph, null: false, extras: [:execution_errors] do
        argument :names, [String], "The package name", required: false
        argument :package_manager, String, "The package manager", required: false
        argument :limit, Integer, "Total number of unmapped packages to return", required: false
      end

      def dependency_graph_unmapped_packages(**arguments)
        Loaders::Dependencies.load_unmapped_packages(
          package_filter: {
            first:            arguments[:first],
            names:           arguments[:names],
            package_manager: arguments[:package_manager],
          }).then { |result| ArrayWrapper.new(result.value!) }
      end

      field :dependency_graph_package_releases, Connections.define(Objects::DependencyGraphPackageRelease), null: false,
        visibility: :internal,
        connection: false, # we'll apply a wrapper manually in the resolver
        description: "A package release indexed in the dependency graph",
        areas_of_responsibility: :dependency_graph,
        extras: [:execution_errors] do
          has_connection_arguments
          argument :package_name, String, "The package name", required: false
          argument :package_manager, String, "The package manager", required: false
          argument :requirements, String, "The version bounds", required: false
          argument :include_dependencies, Boolean, "Flag to indicate that dependencies should be eagerly loaded", required: false
          argument :default_to_latest, Boolean, "Flag to return the latest release if none match the requirements", required: false, default_value: true
          argument :include_unpublished, Boolean, "Flag to return unpublished package releases", required: false, default_value: false
          argument :dependencies_first, Integer, "Number of dependencies to fetch", required: false
          argument :dependencies_after, String, "Cursor to paginate dependencies", required: false
      end

      def dependency_graph_package_releases(execution_errors:, **arguments)
        Loaders::Dependencies.load_package_releases(
          release_filter: {
            first:           arguments[:first],
            package_name:    arguments[:package_name],
            package_manager: arguments[:package_manager],
            requirements:    arguments[:requirements],
            default_to_latest: arguments[:default_to_latest],
            include_unpublished: arguments[:include_unpublished],
            # We only try to access sub-dependencies via direct dependencies.
            # If we're here and trying to load sub-dependencies for a package
            # manager that's still under preview, we can safely assume that the
            # originally fetched preview dependency was authorized and viewable.
            #
            # This information is not senstitive. All information returned, even
            # for preview dependencies, is publicly accessible through the
            # package manager itself.
            preview: true,
          },
          dependencies_filter: {
            first: arguments[:dependencies_first],
            after: arguments[:dependencies_after],
          },
          include_dependencies: !!arguments[:include_dependencies],
        ).then { |result|
          if result.ok?
            wrapper = ArrayWrapper.new(result.value!)
          else
            Failbot.report(result.error, app: "github-dependency-graph")
            wrapper = ArrayWrapper.new
            message = case result.error
            when ::DependencyGraph::Client::TimeoutError
              "timedout"
            when ::DependencyGraph::Client::ServiceUnavailableError, ::DependencyGraph::Client::ApiError, StandardError
              "unavailable"
            end
            execution_errors.add(message)
          end
          ConnectionWrappers::ArrayWrapper.new(
            wrapper,
            arguments,
            parent: object,
            context: context,
          )
        }
      end

      field :surveys, Connections.define(Objects::Survey), visibility: :internal, description: "Returns all the Survey objects", null: false, connection: true

      def surveys
        ArrayWrapper.new(::Survey.all)
      end

      field :survey, Objects::Survey, visibility: :internal, description: "Returns a single survey", null: true do
        argument :slug, String, "Slug of the survey", required: true
      end

      def survey(**arguments)
        if survey = ::Survey.find_by_slug(arguments[:slug])
          survey
        else
          raise Errors::NotFound, "Could not find a survey with slug #{arguments[:slug]}"
        end
      end

      field :meta, Objects::GitHubMetadata, description: "Return information about the GitHub instance", null: false

      def meta
        {
          verifiable_password_authentication: GitHub.auth.verifiable?,
          github_services_sha: Hook::Service.sha,
          hooks: GitHub.hook_ips,
          git: GitHub.git_ips,
          pages: GitHub.pages_a_record_ips,
          importer: GitHub.porter_worker_ips,
          installed_version: GitHub.version_number,
        }
      end

      field :suggested_navigation_destinations, Connections::NavigationDestination,
        description: "A list of suggested navigation destinations for the viewer.",
        visibility: :internal,
        null: false,
        connection: true do

        argument :page_views, [String], "Sorted list of recent page views. Each page should follow the following pattern `type:path`", required: false
      end

      def suggested_navigation_destinations(page_views: [])
        return ArrayWrapper.new([]) unless @context[:viewer]

        client_suggestions = page_views.map do |page_key|
          Platform::Models::NavigationSuggestion.new(
            page_key: page_key,
          )
        end

        munger_suggestions = GitHub.munger.navigation_destinations(@context[:viewer]) || []
        munger_suggestions = munger_suggestions.map do |destination|
          Platform::Models::NavigationSuggestion.new(
            page_key: "#{destination.type.downcase}:#{destination.name}",
            last_visited_at: destination.last_visited_at,
            visit_count: destination.visit_count,
            generated_at: destination.generated_at,
          )
        end

        suggestions = client_suggestions | munger_suggestions

        # If munger is unavailable (i.e. Network error / GHE) use repositories contributed to.
        fallback_suggestions = []
        if munger_suggestions.empty?
          fallback_suggestions = @context[:viewer].repositories_contributed_to(
            viewer: @context[:viewer],
            limit: 20,
            exclude_owned: false,
            since: 15.days.ago,
          ).map { |repo| Platform::Models::NavigationSuggestion.new(destination: repo) }
        end

        promises = suggestions.map do |page_view|

          promise = case page_view.page_key
          when /^repository\:([^\/]+)\/([^\/]+)$/
            owner_login = $1
            repo_name = $2
            Loaders::ActiveRecord.load(::User, owner_login, column: :login).then do |owner|
              next unless owner
              Loaders::RepositoryByName.load(owner.id, repo_name)
            end
          when /^team\:([a-z0-9-]+)\/([\w-]+)/
            org_login = $1
            team_slug = $2
            Loaders::ActiveRecord.load(::Organization, org_login, column: :login).then do |org|
              next unless org
              Loaders::TeamBySlug.load(org, team_slug)
            end
          when /^project\:([^\/]+)\/([^\/]+)\/(\d*)$/
            owner_login = $1
            repo_name = $2
            project_number = $3.to_i
            Loaders::ActiveRecord.load(::User, owner_login, column: :login).then do |owner|
              next unless owner
              Loaders::RepositoryByName.load(owner.id, repo_name).then do |repo|
                next unless repo
                repo.async_projects_enabled?.then do |projects_enabled|
                  next unless projects_enabled

                  Loaders::ProjectByNumber.load(repo, project_number)
                end
              end
            end
          when /^project\:([^\/]+)\/([^\/]+)$/
            owner_login = $1
            project_number = $2.to_i
            Loaders::ActiveRecord.load(::Organization, owner_login, column: :login).then do |owner|
              next unless owner
              owner.async_projects_enabled?.then do |projects_enabled|
                next unless projects_enabled

                Loaders::ProjectByNumber.load(owner, project_number)
              end
            end
          end

          promise&.then do |object|
            next unless object
            type_name = Helpers::NodeIdentification.type_name_from_object(object)
            context[:permission].typed_can_see?(type_name, object).then do |can_read|
              next unless can_read
              page_view.destination = object
              page_view
            end
          end
        end

        Promise.all(promises).then do |suggestions|
          suggestions += fallback_suggestions
          ArrayWrapper.new(
            suggestions.compact.uniq { |suggestion| suggestion.destination.global_relay_id },
          )
        end
      end

      field :security_advisory, Objects::SecurityAdvisory, description: "Fetch a Security Advisory by its GHSA ID", null: true do
        visibility :public

        argument :ghsa_id, String, "GitHub Security Advisory ID.", required: true
      end

      def security_advisory(**arguments)
        Loaders::ActiveRecord.load(::SecurityAdvisory.disclosed, arguments[:ghsa_id], column: :ghsa_id).then do |advisory|
          unless advisory && advisory.visible_publicly?
            raise Errors::NotFound, "Could not resolve to a Security Advisory with ID '#{arguments[:ghsa_id]}'"
          end

          advisory
        end
      end

      field :security_advisories, resolver: Resolvers::SecurityAdvisories,
                                  description: "GitHub Security Advisories",
                                  null: false,
                                  areas_of_responsibility: :security_advisories do
                                    visibility :public
                                  end

      field :security_vulnerabilities, resolver: Resolvers::SecurityVulnerabilities,
                                       description: "Software Vulnerabilities documented by GitHub Security Advisories",
                                       null: false,
                                       areas_of_responsibility: :security_advisories do
                                         visibility :public
                                       end
    end
  end
end
