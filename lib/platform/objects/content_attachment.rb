# frozen_string_literal: true

module Platform
  module Objects
    class ContentAttachment < Platform::Objects::Base
      model_name "ContentReferenceAttachment"
      description "A content attachment"
      areas_of_responsibility :ce_extensibility

      scopeless_tokens_as_minimum

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, content_attachment)
        # Only the integration that created the
        # content attachment should be able to view it

        permission.viewer.integration == content_attachment.integration
      end

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, content_attachment)
        # Only the integration that created the
        # content attachment should be able to view it

        permission.viewer.integration == content_attachment.integration
      end

      global_id_field :id

      database_id_field null: false

      field :title, String, description: "The title of the content attachment.", null: false

      field :body, String, description: "The body text of the content attachment. This parameter supports markdown.", null: false

      field :content_reference, Objects::ContentReference, description: "The content reference that the content attachment is attached to.", null: false
    end
  end
end
