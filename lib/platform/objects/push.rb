# frozen_string_literal: true

module Platform
  module Objects
    class Push < Platform::Objects::Base
      description "A Git push."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, push)
        permission.async_repo_and_org_owner(push).then do |repo, org|
          permission.access_allowed?(:get_push, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, push)
        push.async_repository.then do |repo|
          repo.resources.contents.readable_by?(permission.viewer) || \
          repo.resources.checks.writable_by?(permission.viewer)
        end
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::Trigger

      global_id_field :id

      field :repository, Repository, "The repository that was pushed to", method: :async_repository, null: false
      field :pusher, User, "The user who pushed", method: :async_pusher, null: false

      field :permalink, Scalars::URI, description: "The permalink for this push.", null: false

      def permalink
        @object.async_repository.then do
          Addressable::URI.parse(@object.permalink)
        end
      end

      field :previous_sha, Scalars::GitObjectID, description: "The SHA before the push", method: :before, null: true

      field :next_sha, Scalars::GitObjectID, description: "The SHA after the push", method: :after, null: true
    end
  end
end
