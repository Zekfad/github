# frozen_string_literal: true

module Platform
  module Objects
    class ContentReference < Platform::Objects::Base
      description "A content reference"
      areas_of_responsibility :ce_extensibility

      scopeless_tokens_as_minimum

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, content_reference)
        content_reference.async_repository.then do |repo|
          permission.access_allowed?(
            :read_content_reference,
            resource: content_reference,
            current_repo: repo,
            current_org: nil,
            allow_integrations: true,
            allow_user_via_integration: false,
          )
        end
      end

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, content_reference)
        content_reference.async_repository.then do |repo|
          permission.access_allowed?(
            :read_content_reference,
            resource: content_reference,
            current_repo: repo,
            current_org: nil,
            allow_integrations: true,
            allow_user_via_integration: false,
          )
        end
      end

      global_id_field :id

      database_id_field null: false

      field :reference, String, description: "The reference of the content reference.", null: false
    end
  end
end
