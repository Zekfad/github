# frozen_string_literal: true

module Platform
  module Objects
    class PackageFile < Platform::Objects::Base
      model_name "Registry::File"
      description "A file in a package version."

      minimum_accepted_scopes ["read:packages"]

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, package_file)
        package_file.async_package_version.then do |package_version|
          permission.typed_can_access?("PackageVersion", package_version)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_package_version.then { |package_version|
          permission.typed_can_see?("PackageVersion", package_version)
        }
      end

      implements Platform::Interfaces::Node

      global_id_field :id

      database_id_field(visibility: :internal)

      updated_at_field

      field :guid, String, "A unique identifier for this file.", null: true, visibility: :internal
      field :md5, String, "MD5 hash of the file.", null: true
      field :name, String, description: "Name of the file.", null: false
      field :sha1, String, "SHA1 hash of the file.", null: true
      field :sha256, String, "SHA256 hash of the file.", null: true
      field :size, Integer, "Size of the file in bytes.", null: true
      field :sri, String, "Identifies the subresource integrity (SRI).", method: :sri_512, null: true, visibility: :internal

      field :package_version, Objects::PackageVersion, description: "The package version this file belongs to.", null: true

      def package_version
          Loaders::PackageFiles.get_version(@object.package_version_id)
      end

      def name
        @object.async_package_version.then { |version|
          version.async_package.then {
            @object.name
          }
        }
      end

      field :billing_allowed, Boolean, description: "whether billing gives you access to the file", null: false, visibility: :internal

      def billing_allowed
        return true unless GitHub.billing_enabled? # Skip billing checks if billing is disabled.
        @object.async_package_version.then { |package_version|
          package_version.async_package.then { |package|
            package.async_repository.then { |repo|
              repo.async_owner.then { |owner|
                owner.async_budgets.then do
                  permission = Billing::PackageRegistryPermission.new(owner)
                  status = permission.status
                  unless status[:allowed]
                    next false
                  end

                  unless Platform::Helpers::ViaActions.request_via_actions?(context: context, log_metric: true, usage_bytes: @object.size)
                    if GitHub.flipper[:registry_enforce_bandwidth_limits].enabled? && !permission.download_allowed?(bytes: @object.size, public: repo.public?)
                      next false
                    end
                  end

                  true
                end
              }
            }
          }
        }
      end

      field :url, Scalars::URI, description: "URL to download the asset.", null: true

      def url
        viewer = @context[:viewer]
        @object.async_package_version.then { |package_version|
          package_version.async_package.then { |package|
            package.async_repository.then { |repo|
              repo.async_owner.then { |owner|
                owner.async_budgets.then do
                  permission = Billing::PackageRegistryPermission.new(owner)
                  status = permission.status

                  next nil unless status[:allowed]

                  unless Platform::Helpers::ViaActions.request_via_actions?(context: context, log_metric: true, usage_bytes: @object.size)
                    if GitHub.flipper[:registry_enforce_bandwidth_limits].enabled? && !permission.download_allowed?(bytes: @object.size, public: repo.public?)
                      next nil
                    end
                  end

                  @object.get_cdn_url(@object.url(actor: viewer))
                end
              }
            }
          }
        }
      end

      field :metadata_url, Scalars::URI, description: "URL to download the asset metadata.", null: true, visibility: :internal

      def metadata_url
        viewer = @context[:viewer]
        @object.async_package_version.then { |package_version|
          package_version.async_package.then { |package|
            package.async_repository.then {
              @object.metadata_url(actor: viewer)
            }
          }
        }
      end
    end
  end
end
