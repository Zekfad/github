# frozen_string_literal: true

module Platform
  module Objects
    class SavedReply < Platform::Objects::Base
      description "A Saved Reply is text a user can use to reply quickly."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, reply)
        reply.async_user.then do |user|
          permission.access_allowed?(:v4_read_user_private, user: user, resource: user, current_repo: nil, current_org: nil, allow_integration: false, allow_user_via_integration: false)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_viewer(object)
      end

      scopeless_tokens_as_minimum

      areas_of_responsibility :user_profile

      implements Platform::Interfaces::Node

      global_id_field :id

      database_id_field

      field :user, Interfaces::Actor, method: :async_user, description: "The user that saved this reply.", null: true
      field :title, String, "The title of the saved reply.", null: false
      field :body, String, "The body of the saved reply.", null: false
      field :bodyHTML, Scalars::HTML, description: "The saved reply body rendered to HTML.", null: false
      def body_html
        markdown = CommonMarker.render_html(@object.body || "", [:UNSAFE, :GITHUB_PRE_LANG], %i[tagfilter table strikethrough autolink])
        GitHub::HTML::SimplePipeline.to_html(markdown)
      end
    end
  end
end
