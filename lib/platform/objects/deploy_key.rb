# frozen_string_literal: true

module Platform
  module Objects
    class DeployKey < Platform::Objects::Base
      model_name "PublicKey"
      description "A repository deploy key."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, key)
        permission.typed_can_access?("PublicKey", key)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_repository.then do |repo|
          permission.typed_can_see?("Repository", repo)
        end
      end

      minimum_accepted_scopes ["public_repo"]

      implements Platform::Interfaces::Node
      global_id_field :id

      field :key, String, "The deploy key.", null: false
      field :title, String, "The deploy key title.", null: false
      field :verified, Boolean, "Whether or not the deploy key has been verified.", method: :verified?, null: false
      field :read_only, Boolean, "Whether or not the deploy key is read only.", null: false
      created_at_field
    end
  end
end
