# frozen_string_literal: true

module Platform
  module Objects
    class AppPermission < Platform::Objects::Base
      areas_of_responsibility :ecosystem_apps

      description "Permission access for an App"
      visibility :under_development

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      # Ability check for an Integration permission
      def self.async_viewer_can_see?(permission, object)
        true
      end

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        permission.hidden_from_public?(self) # Update this authorization if we ever go public with this object
      end

      scopeless_tokens_as_minimum

      field :resource, String,                          description: "The type of resource the app has access to",                visibility: :under_development, null: false
      field :access,   Enums::AppPermissionAccessLevel, description: "The level of access the app has to the specified resource", visibility: :under_development, null: false
    end
  end
end
