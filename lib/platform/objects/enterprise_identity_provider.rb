# frozen_string_literal: true

module Platform
  module Objects
    class EnterpriseIdentityProvider < Platform::Objects::Base
      model_name "Business::SamlProvider"
      description "An identity provider configured to provision identities for an enterprise."

      minimum_accepted_scopes ["admin:enterprise"]

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        object.async_target.then do |target|
          permission.access_allowed?(:view_enterprise_external_identities, resource: target, organization: nil, repo: nil, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        # Let site admins view all identity providers
        return true if permission.viewer.site_admin?

        object.async_target.then do |target|
          target.owner? permission.viewer
        end
      end

      implements Platform::Interfaces::Node

      global_id_field :id

      field :enterprise, Objects::Enterprise, method: :async_target, description: "The enterprise this identity provider belongs to.", null: true

      field :external_identities, resolver: Platform::Resolvers::ExternalIdentities, description: "ExternalIdentities provisioned by this identity provider.", connection: true

      field :sso_url, Scalars::URI, description: "The URL endpoint for the identity provider's SAML SSO.", null: true

      field :issuer, String, description: "The Issuer Entity ID for the SAML identity provider.", null: true

      field :idp_certificate, Scalars::X509Certificate, description: "The x509 certificate used by the identity provider to sign assertions and responses.", null: true

      field :signature_method, Enums::SamlSignatureAlgorithm, description: "The signature algorithm used to sign SAML requests for the identity provider.", null: true

      field :digest_method, Enums::SamlDigestAlgorithm, description: "The digest algorithm used to sign SAML requests for the identity provider.", null: true

      field :recovery_codes, [String], description: "Recovery codes that can be used by admins to access the enterprise if the identity provider is unavailable.", null: true

      def recovery_codes
        return [] unless object.recovery_codes_available?
        # Deliberately resolve using the version of the recovery codes formatted as "nnnnn-nnnnn"
        object.formatted_recovery_codes
      end

      field :provisioning_enabled, Boolean, visibility: :under_development, description: "Has user provisioning been enabled for the enterprise", null: false

      field :saml_deprovisioning_enabled, Boolean, visibility: :under_development, description: "Has SAML user deprovisioning been enabled for the enterprise", null: false
    end
  end
end
