# frozen_string_literal: true

module Platform
  module Objects
    class Mutation < Platform::Objects::Base
      # Don't return a promise from this, because we want GraphQL-Ruby to run these fields right away
      def self.authorized?(*)
        super.sync # rubocop:disable GitHub/DontSyncInsideFields
      end

      def self.async_viewer_can_see?(*)
        true
      end

      def self.async_api_can_access?(*)
        true
      end

      description "The root query for implementing GraphQL mutations."

      field :add_reaction, mutation: Mutations::AddReaction
      field :remove_reaction, mutation: Mutations::RemoveReaction
      field :update_subscription, mutation: Mutations::UpdateSubscription

      # Collections
      field :update_explore_collection, mutation: Mutations::UpdateExploreCollection

      # Comments
      field :add_comment, mutation: Mutations::AddComment
      field :minimize_comment, mutation: Mutations::MinimizeComment
      field :unminimize_comment, mutation: Mutations::UnminimizeComment
      field :delete_comment, mutation: Mutations::DeleteComment
      field :update_issue_comment, mutation: Mutations::UpdateIssueComment

      # Delegated Recovery
      field :add_recovery_token, mutation: Mutations::AddRecoveryToken
      field :delete_recovery_token, mutation: Mutations::DeleteRecoveryToken
      field :confirm_recovery_token, mutation: Mutations::ConfirmRecoveryToken
      field :verify_recovery_token, mutation: Mutations::VerifyRecoveryToken

      # Newsies
      field :mark_notification_subject_as_read, mutation: Mutations::MarkNotificationSubjectAsRead
      field :mark_notification_as_done, mutation: Mutations::MarkNotificationAsDone
      field :mark_notification_as_undone, mutation: Mutations::MarkNotificationAsUndone
      field :mark_notification_as_unread, mutation: Mutations::MarkNotificationAsUnread
      field :mark_notification_as_read, mutation: Mutations::MarkNotificationAsRead
      field :create_saved_notification_thread, mutation: Mutations::CreateSavedNotificationThread
      field :delete_saved_notification_thread, mutation: Mutations::DeleteSavedNotificationThread
      field :create_custom_inbox, mutation: Mutations::CreateCustomInbox
      field :update_custom_inbox, mutation: Mutations::UpdateCustomInbox
      field :delete_custom_inbox, mutation: Mutations::DeleteCustomInbox
      field :add_mobile_device_token, mutation: Mutations::AddMobileDeviceToken
      field :delete_mobile_device_token, mutation: Mutations::DeleteMobileDeviceToken
      field :clear_mobile_device_tokens, mutation: Mutations::ClearMobileDeviceTokens
      field :update_notification_settings, mutation: Mutations::UpdateNotificationSettings
      field :update_email_route, mutation: Mutations::UpdateEmailRoute
      field :create_mobile_push_notification_schedules, mutation: Mutations::CreateMobilePushNotificationSchedules
      field :delete_mobile_push_notification_schedule, mutation: Mutations::DeleteMobilePushNotificationSchedule
      field :update_mobile_push_notification_schedules, mutation: Mutations::UpdateMobilePushNotificationSchedules

      # Check suites!
      field :create_check_suite, mutation: Mutations::CreateCheckSuite
      field :rerequest_check_suite, mutation: Mutations::RerequestCheckSuite
      field :update_check_suite_preferences, mutation: Mutations::UpdateCheckSuitePreferences
      field :update_check_suite, mutation: Mutations::UpdateCheckSuite
      field :create_check_run, mutation: Mutations::CreateCheckRun
      field :update_check_run, mutation: Mutations::UpdateCheckRun

      # Gists
      # field :update_gist, mutation: Mutations::UpdateGist
      # If you are adding the update_gist mutation, please include
      # the following in your mutation.
      #
      # ```ruby
      # previous_gist_payload = pre_updated_gist.hydro_payload
      # # update happens
      # updated_gist.instrument_hydro_update_event(actor: current_user, previous_gist_payload: previous_gist_payload)
      # ```
      #
      # For more details, read through this pr:
      # https://github.com/github/github/pull/102597

      # Projects
      field :create_project, mutation: Mutations::CreateProject
      field :update_project, mutation: Mutations::UpdateProject
      field :delete_project, mutation: Mutations::DeleteProject
      field :clone_project, mutation: Mutations::CloneProject
      field :import_project, mutation: Mutations::ImportProject
      field :add_project_column, mutation: Mutations::AddProjectColumn
      field :move_project_column, mutation: Mutations::MoveProjectColumn
      field :update_project_column, mutation: Mutations::UpdateProjectColumn
      field :delete_project_column, mutation: Mutations::DeleteProjectColumn
      field :add_project_card, mutation: Mutations::AddProjectCard
      field :update_project_card, mutation: Mutations::UpdateProjectCard
      field :move_project_card, mutation: Mutations::MoveProjectCard
      field :archive_project_card, mutation: Mutations::ArchiveProjectCard
      field :unarchive_project_card, mutation: Mutations::UnarchiveProjectCard
      field :delete_project_card, mutation: Mutations::DeleteProjectCard
      field :add_project_workflow, mutation: Mutations::AddProjectWorkflow
      field :update_project_workflow, mutation: Mutations::UpdateProjectWorkflow
      field :delete_project_workflow, mutation: Mutations::DeleteProjectWorkflow
      field :add_project_collaborator, mutation: Mutations::AddProjectCollaborator
      field :update_project_collaborator, mutation: Mutations::UpdateProjectCollaborator
      field :remove_project_collaborator, mutation: Mutations::RemoveProjectCollaborator
      field :link_repository_to_project, mutation: Mutations::LinkRepositoryToProject
      field :unlink_repository_from_project, mutation: Mutations::UnlinkRepositoryFromProject
      field :convert_project_card_note_to_issue, mutation: Mutations::ConvertProjectCardNoteToIssue

      # Labels
      field :create_label, mutation: Mutations::CreateLabel
      field :delete_label, mutation: Mutations::DeleteLabel
      field :update_label, mutation: Mutations::UpdateLabel
      field :delete_label_by_name, mutation: Mutations::DeleteLabelByName
      field :update_label_by_name, mutation: Mutations::UpdateLabelByName

      # Issues
      field :unmark_issue_as_duplicate, mutation: Mutations::UnmarkIssueAsDuplicate
      field :lock_lockable, mutation: Mutations::LockLockable
      field :unlock_lockable, mutation: Mutations::UnlockLockable
      field :add_assignees_to_assignable, mutation: Mutations::AddAssigneesToAssignable
      field :remove_assignees_from_assignable, mutation: Mutations::RemoveAssigneesFromAssignable
      field :replace_assignees_for_assignable, mutation: Mutations::ReplaceAssigneesForAssignable
      field :add_labels_to_labelable, mutation: Mutations::AddLabelsToLabelable
      field :add_or_create_labels_to_labelable, mutation: Mutations::AddOrCreateLabelsToLabelable
      field :create_issue, mutation: Mutations::CreateIssue
      field :clear_labels_from_labelable, mutation: Mutations::ClearLabelsFromLabelable
      field :remove_labels_from_labelable, mutation: Mutations::RemoveLabelsFromLabelable
      field :replace_labels_for_labelable, mutation: Mutations::ReplaceLabelsForLabelable
      field :close_issue, mutation: Mutations::CloseIssue
      field :reopen_issue, mutation: Mutations::ReopenIssue
      field :transfer_issue, mutation: Mutations::TransferIssue
      field :delete_issue_comment, mutation: Mutations::DeleteIssueComment
      field :update_issue, mutation: Mutations::UpdateIssue
      field :delete_issue, mutation: Mutations::DeleteIssue
      field :pin_issue, mutation: Mutations::PinIssue
      field :unpin_issue, mutation: Mutations::UnpinIssue

      # Pull Request
      field :create_pull_request, mutation: Mutations::CreatePullRequest
      field :update_pull_request, mutation: Mutations::UpdatePullRequest
      field :update_pull_request_branch, mutation: Mutations::UpdatePullRequestBranch
      field :close_pull_request, mutation: Mutations::ClosePullRequest
      field :reopen_pull_request, mutation: Mutations::ReopenPullRequest
      field :mark_pull_request_ready_for_review, mutation: Mutations::MarkPullRequestReadyForReview
      field :merge_pull_request, mutation: Mutations::MergePullRequest
      field :delete_pull_request_review_comment, mutation: Mutations::DeletePullRequestReviewComment
      field :mark_file_as_viewed, mutation: Mutations::MarkFileAsViewed
      field :unmark_file_as_viewed, mutation: Mutations::UnmarkFileAsViewed
      field :convert_pull_request_to_draft, mutation: Mutations::ConvertPullRequestToDraft

      # Pull Request Reviews
      field :add_pull_request_review, mutation: Mutations::AddPullRequestReview
      field :submit_pull_request_review, mutation: Mutations::SubmitPullRequestReview
      field :update_pull_request_review, mutation: Mutations::UpdatePullRequestReview
      field :dismiss_pull_request_review, mutation: Mutations::DismissPullRequestReview
      field :delete_pull_request_review, mutation: Mutations::DeletePullRequestReview

      field :resolve_review_thread, mutation: Mutations::ResolveReviewThread
      field :unresolve_review_thread, mutation: Mutations::UnresolveReviewThread

      # Pull Request Review Comments
      field :add_pull_request_review_comment, mutation: Mutations::AddPullRequestReviewComment
      field :update_pull_request_review_comment, mutation: Mutations::UpdatePullRequestReviewComment
      field :delete_pull_request_review_comment, mutation: Mutations::DeletePullRequestReviewComment

      # Pull Request Review Threads
      field :add_pull_request_review_thread, mutation: Mutations::AddPullRequestReviewThread
      field :add_pull_request_review_thread_reply, mutation: Mutations::AddPullRequestReviewThreadReply

      # Merge queue
      field :addPullRequestToMergeQueue, mutation: Mutations::AddPullRequestToMergeQueue
      field :removeEntryFromMergeQueue, mutation: Mutations::RemoveEntryFromMergeQueue

      # IP allow list
      field :update_ip_allow_list_enabled_setting, mutation: Mutations::UpdateIpAllowListEnabledSetting
      field :create_ip_allow_list_entry, mutation: Mutations::CreateIpAllowListEntry
      field :update_ip_allow_list_entry, mutation: Mutations::UpdateIpAllowListEntry
      field :delete_ip_allow_list_entry, mutation: Mutations::DeleteIpAllowListEntry

      # Enterprise accounts
      field :update_enterprise_profile, mutation: Mutations::UpdateEnterpriseProfile
      field :invite_enterprise_admin, mutation: Mutations::InviteEnterpriseAdmin
      field :accept_enterprise_administrator_invitation, mutation: Mutations::AcceptEnterpriseAdministratorInvitation
      field :cancel_enterprise_admin_invitation, mutation: Mutations::CancelEnterpriseAdminInvitation
      field :confirm_enterprise_organization_invitation, mutation: Mutations::ConfirmEnterpriseOrganizationInvitation
      field :add_enterprise_admin, mutation: Mutations::AddEnterpriseAdmin
      field :remove_enterprise_admin, mutation: Mutations::RemoveEnterpriseAdmin
      field :remove_enterprise_member, mutation: Mutations::RemoveEnterpriseMember
      field :invite_enterprise_organization, mutation: Mutations::InviteEnterpriseOrganization
      field :accept_enterprise_organization_invitation, mutation: Mutations::AcceptEnterpriseOrganizationInvitation
      field :cancel_enterprise_organization_invitation, mutation: Mutations::CancelEnterpriseOrganizationInvitation
      field :remove_enterprise_organization, mutation: Mutations::RemoveEnterpriseOrganization
      field :create_enterprise_organization, mutation: Mutations::CreateEnterpriseOrganization
      field :set_enterprise_identity_provider, mutation: Mutations::SetEnterpriseIdentityProvider
      field :set_enterprise_user_provisioning_settings, mutation: Mutations::SetEnterpriseUserProvisioningSettings
      field :remove_enterprise_identity_provider, mutation: Mutations::RemoveEnterpriseIdentityProvider
      field :regenerate_enterprise_identity_provider_recovery_codes, mutation: Mutations::RegenerateEnterpriseIdentityProviderRecoveryCodes
      field :update_enterprise_members_can_create_repositories_setting, mutation: Mutations::UpdateEnterpriseMembersCanCreateRepositoriesSetting
      field :update_enterprise_allow_private_repository_forking_setting, mutation: Mutations::UpdateEnterpriseAllowPrivateRepositoryForkingSetting
      field :update_enterprise_default_repository_permission_setting, mutation: Mutations::UpdateEnterpriseDefaultRepositoryPermissionSetting
      field :update_enterprise_team_discussions_setting, mutation: Mutations::UpdateEnterpriseTeamDiscussionsSetting
      field :update_enterprise_organization_projects_setting, mutation: Mutations::UpdateEnterpriseOrganizationProjectsSetting
      field :update_enterprise_repository_projects_setting, mutation: Mutations::UpdateEnterpriseRepositoryProjectsSetting
      field :update_enterprise_members_can_change_repository_visibility_setting, mutation: Mutations::UpdateEnterpriseMembersCanChangeRepositoryVisibilitySetting
      field :update_enterprise_members_can_invite_collaborators_setting, mutation: Mutations::UpdateEnterpriseMembersCanInviteCollaboratorsSetting
      field :update_enterprise_members_can_delete_repositories_setting, mutation: Mutations::UpdateEnterpriseMembersCanDeleteRepositoriesSetting
      field :update_enterprise_members_can_make_purchases_setting, mutation: Mutations::UpdateEnterpriseMembersCanMakePurchasesSetting
      field :update_enterprise_two_factor_authentication_required_setting, mutation: Mutations::UpdateEnterpriseTwoFactorAuthenticationRequiredSetting
      field :update_enterprise_members_can_update_protected_branches_setting, mutation: Mutations::UpdateEnterpriseMembersCanUpdateProtectedBranchesSetting
      field :update_enterprise_members_can_delete_issues_setting, mutation: Mutations::UpdateEnterpriseMembersCanDeleteIssuesSetting
      field :update_enterprise_members_can_view_dependency_insights_setting, mutation: Mutations::UpdateEnterpriseMembersCanViewDependencyInsightsSetting
      field :update_enterprise_action_execution_capability_setting, mutation: Mutations::UpdateEnterpriseActionExecutionCapabilitySetting
      field :update_enterprise_administrator_role, mutation: Mutations::UpdateEnterpriseAdministratorRole

      # Organization
      field :remove_outside_collaborator, mutation: Mutations::RemoveOutsideCollaborator
      field :grant_oap_for_marketplace_listing, mutation: Mutations::GrantOapForMarketplaceListing
      field :invite_to_organization, mutation: Mutations::InviteToOrganization
      field :add_organization_domain, mutation: Mutations::AddOrganizationDomain
      field :delete_organization_domain, mutation: Mutations::DeleteOrganizationDomain
      field :regenerate_organization_domain_token, mutation: Mutations::RegenerateOrganizationDomainToken
      field :verify_organization_domain, mutation: Mutations::VerifyOrganizationDomain
      field :enable_organization_notification_restriction, mutation: Mutations::EnableOrganizationNotificationRestriction
      field :disable_organization_notification_restriction, mutation: Mutations::DisableOrganizationNotificationRestriction

      # Organization discussions
      field :create_organization_discussion, mutation: Mutations::CreateOrganizationDiscussion
      field :update_organization_discussion, mutation: Mutations::UpdateOrganizationDiscussion
      field :delete_organization_discussion, mutation: Mutations::DeleteOrganizationDiscussion

      # Organization discussion comments
      field :create_organization_discussion_comment, mutation: Mutations::CreateOrganizationDiscussionComment
      field :update_organization_discussion_comment, mutation: Mutations::UpdateOrganizationDiscussionComment
      field :delete_organization_discussion_comment, mutation: Mutations::DeleteOrganizationDiscussionComment

      # Review Requests
      field :request_reviews, mutation: Mutations::RequestReviews

      # Releases
      field :add_release_to_repository, mutation: Mutations::AddReleaseToRepository
      field :remove_release_from_repository, mutation: Mutations::RemoveReleaseFromRepository

      # Packages
      field :delete_package_version, mutation: Mutations::DeletePackageVersion
      field :increment_registry_package_download_count, mutation: Mutations::IncrementRegistryPackageDownloadCount
      field :create_package_version, mutation: Mutations::CreatePackageVersion
      field :update_package_version, mutation: Mutations::UpdatePackageVersion
      field :create_package_file, mutation: Mutations::CreatePackageFile
      field :update_package_file, mutation: Mutations::UpdatePackageFile
      field :create_package_version_metadata, mutation: Mutations::CreatePackageVersionMetadata
      field :add_package_tag, mutation: Mutations::AddPackageTag
      field :delete_package_tag, mutation: Mutations::DeletePackageTag

      # Repository
      field :transfer_repository, mutation: Mutations::TransferRepository
      field :create_commit_on_ref, mutation: Mutations::CreateCommitOnRef
      field :clone_template_repository, mutation: Mutations::CloneTemplateRepository
      field :create_repository, mutation: Mutations::CreateRepository
      field :update_repository, mutation: Mutations::UpdateRepository
      field :create_ref, mutation: Mutations::CreateRef
      field :update_ref, mutation: Mutations::UpdateRef
      field :delete_ref, mutation: Mutations::DeleteRef
      field :update_refs, mutation: Mutations::UpdateRefs
      field :merge_branch, mutation: Mutations::MergeBranch

      # Repository images
      field :delete_repository_image, mutation: Mutations::DeleteRepositoryImage

      # RepositoryAction
      field :update_repository_action, mutation: Mutations::UpdateRepositoryAction
      field :delist_repository_action, mutation: Mutations::DelistRepositoryAction

      # Spamurai
      field :classify_accounts_as_hammy, mutation: Mutations::ClassifyAccountsAsHammy
      field :classify_accounts_as_spammy, mutation: Mutations::ClassifyAccountsAsSpammy
      field :clear_account_classifications, mutation: Mutations::ClearAccountClassifications
      field :suspend_accounts, mutation: Mutations::SuspendAccounts
      field :unsuspend_accounts, mutation: Mutations::UnsuspendAccounts
      field :set_has_used_anonymizing_proxy, mutation: Mutations::SetHasUsedAnonymizingProxy

      # Security
      field :reset_passwords, mutation: Mutations::ResetPasswords
      field :resolve_security_incident, mutation: Mutations::ResolveSecurityIncident

      # Stars
      field :add_star, mutation: Mutations::AddStar
      field :remove_star, mutation: Mutations::RemoveStar

      # Stafftools
      field :update_network_privilege, mutation: Mutations::UpdateNetworkPrivilege
      field :disable_repositories, mutation: Mutations::DisableRepositories
      field :block_accounts_action_invocation, mutation: Mutations::BlockAccountsActionInvocation
      field :unblock_accounts_action_invocation, mutation: Mutations::UnblockAccountsActionInvocation
      field :accept_sponsors_membership, mutation: Mutations::AcceptSponsorsMembership
      field :deny_sponsors_membership, mutation: Mutations::DenySponsorsMembership
      field :revert_sponsors_membership_to_submitted, mutation: Mutations::RevertSponsorsMembershipToSubmitted
      field :update_sponsors_memberships_criterion, mutation: Mutations::UpdateSponsorsMembershipsCriterion
      field :disable_sponsors_match, mutation: Mutations::DisableSponsorsMatch
      field :enable_sponsors_match, mutation: Mutations::EnableSponsorsMatch
      field :update_sponsors_membership, mutation: Mutations::UpdateSponsorsMembership
      field :flag_sponsor_for_fraud_review, mutation: Mutations::FlagSponsorForFraudReview

      # Topics
      field :accept_topic_suggestion, mutation: Mutations::AcceptTopicSuggestion
      field :decline_topic_suggestion, mutation: Mutations::DeclineTopicSuggestion
      field :update_topics, mutation: Mutations::UpdateTopics
      field :update_topic, mutation: Mutations::UpdateTopic

      # Teams
      field :create_team, mutation: Mutations::CreateTeam
      field :update_team, mutation: Mutations::UpdateTeam
      field :delete_team, mutation: Mutations::DeleteTeam
      field :update_team_review_assignment, mutation: Mutations::UpdateTeamReviewAssignment

      # TeamDiscussion
      field :create_team_discussion, mutation: Mutations::CreateTeamDiscussion
      field :update_team_discussion, mutation: Mutations::UpdateTeamDiscussion
      field :delete_team_discussion, mutation: Mutations::DeleteTeamDiscussion

      # TeamDiscussionComment
      field :create_team_discussion_comment, mutation: Mutations::CreateTeamDiscussionComment
      field :update_team_discussion_comment, mutation: Mutations::UpdateTeamDiscussionComment
      field :delete_team_discussion_comment, mutation: Mutations::DeleteTeamDiscussionComment

      # Team Member
      field :add_team_member, mutation: Mutations::AddTeamMember
      field :update_team_member, mutation: Mutations::UpdateTeamMember
      field :remove_team_member, mutation: Mutations::RemoveTeamMember

      # Team Repository
      field :update_team_repository, mutation: Mutations::UpdateTeamRepository
      field :update_teams_repository, mutation: Mutations::UpdateTeamsRepository

      # Team Project
      field :add_team_project, mutation: Mutations::AddTeamProject
      field :update_team_project, mutation: Mutations::UpdateTeamProject
      field :remove_team_project, mutation: Mutations::RemoveTeamProject

      # Team Change Parent Requests
      field :approve_pending_team_change_parent_request, mutation: Mutations::ApprovePendingTeamChangeParentRequest
      field :cancel_pending_team_change_parent_request, mutation: Mutations::CancelPendingTeamChangeParentRequest
      field :create_parent_initiated_team_change_parent_request, mutation: Mutations::CreateParentInitiatedTeamChangeParentRequest

      # Dependency Graph
      field :dependency_graph_reassign_package, mutation: Mutations::DependencyGraphReassignPackage

      # Marketplace agreements
      field :create_marketplace_agreement, mutation: Mutations::CreateMarketplaceAgreement
      field :sign_marketplace_agreement, mutation: Mutations::SignMarketplaceAgreement

      # Marketplace categories
      field :create_marketplace_category, mutation: Mutations::CreateMarketplaceCategory
      field :update_marketplace_category, mutation: Mutations::UpdateMarketplaceCategory

      # Marketplace stories
      field :update_marketplace_story, mutation: Mutations::UpdateMarketplaceStory

      # Marketplace listings
      field :create_marketplace_listing, mutation: Mutations::CreateMarketplaceListing
      field :update_marketplace_listing, mutation: Mutations::UpdateMarketplaceListing
      field :approve_marketplace_listing, mutation: Mutations::ApproveMarketplaceListing
      field :reject_marketplace_listing, mutation: Mutations::RejectMarketplaceListing
      field :redraft_marketplace_listing, mutation: Mutations::RedraftMarketplaceListing
      field :delist_marketplace_listing, mutation: Mutations::DelistMarketplaceListing
      field :request_marketplace_listing_approval, mutation: Mutations::RequestMarketplaceListingApproval
      field :request_unverified_marketplace_listing_approval, mutation: Mutations::RequestUnverifiedMarketplaceListingApproval
      field :request_verified_marketplace_listing_approval, mutation: Mutations::RequestVerifiedMarketplaceListingApproval

      # Marketplace listing plans
      field :create_marketplace_listing_plan, mutation: Mutations::CreateMarketplaceListingPlan
      field :update_marketplace_listing_plan, mutation: Mutations::UpdateMarketplaceListingPlan
      field :delete_marketplace_listing_plan, mutation: Mutations::DeleteMarketplaceListingPlan
      field :retire_marketplace_listing_plan, mutation: Mutations::RetireMarketplaceListingPlan
      field :publish_marketplace_listing_plan, mutation: Mutations::PublishMarketplaceListingPlan

      # Marketplace listing plan bullets
      field :create_marketplace_listing_plan_bullet, mutation: Mutations::CreateMarketplaceListingPlanBullet
      field :update_marketplace_listing_plan_bullet, mutation: Mutations::UpdateMarketplaceListingPlanBullet
      field :delete_marketplace_listing_plan_bullet, mutation: Mutations::DeleteMarketplaceListingPlanBullet

      # Marketplace listing screenshots
      field :update_marketplace_listing_screenshot, mutation: Mutations::UpdateMarketplaceListingScreenshot
      field :resequence_marketplace_listing_screenshot, mutation: Mutations::ResequenceMarketplaceListingScreenshot
      field :delete_marketplace_listing_screenshot, mutation: Mutations::DeleteMarketplaceListingScreenshot

      # Marketplace order previews
      field :update_marketplace_order_preview, mutation: Mutations::UpdateMarketplaceOrderPreview
      field :delete_marketplace_order_preview, mutation: Mutations::DeleteMarketplaceOrderPreview
      field :record_marketplace_retargeting_notifications, mutation: Mutations::RecordMarketplaceRetargetingNotifications

      # Works with GitHub listings
      field :reject_non_marketplace_listing, mutation: Mutations::RejectNonMarketplaceListing
      field :approve_non_marketplace_listing, mutation: Mutations::ApproveNonMarketplaceListing
      field :delist_non_marketplace_listing, mutation: Mutations::DelistNonMarketplaceListing
      field :update_non_marketplace_listing, mutation: Mutations::UpdateNonMarketplaceListing

      # Billing suscription items
      field :create_subscription_item, mutation: Mutations::CreateSubscriptionItem
      field :update_subscription_item, mutation: Mutations::UpdateSubscriptionItem
      field :cancel_subscription_item, mutation: Mutations::CancelSubscriptionItem

      # Works with GitHub listings
      field :create_non_marketplace_listing, mutation: Mutations::CreateNonMarketplaceListing

      # Billing actions
      field :update_pending_plan_change, mutation: Mutations::UpdatePendingPlanChange
      field :cancel_pending_marketplace_change, mutation: Mutations::CancelPendingMarketplaceChange
      field :run_pending_marketplace_change, mutation: Mutations::RunPendingMarketplaceChange

      # Import/Export
      field :start_import, mutation: Mutations::StartImport
      field :prepare_import, mutation: Mutations::PrepareImport
      field :add_import_mapping, mutation: Mutations::AddImportMapping
      field :perform_import, mutation: Mutations::PerformImport
      field :unlock_imported_repositories, mutation: Mutations::UnlockImportedRepositories
      field :create_attribution_invitation, mutation: Mutations::CreateAttributionInvitation

      # Browser Stats
      field :report_browser_metrics, mutation: Mutations::ReportBrowserMetrics
      field :report_browser_error, mutation: Mutations::ReportBrowserError

      # Repository recommendations
      field :dismiss_repository_recommendation, mutation: Mutations::DismissRepositoryRecommendation
      field :restore_repository_recommendation, mutation: Mutations::RestoreRepositoryRecommendation
      field :opt_repository_out_of_recommendations, mutation: Mutations::OptRepositoryOutOfRecommendations
      field :opt_repository_into_recommendations, mutation: Mutations::OptRepositoryIntoRecommendations

      # Pending Vulnerabilities
      field :add_pending_vulnerability,          mutation: Mutations::AddPendingVulnerability
      field :add_pending_vulnerable_version_range, mutation: Mutations::AddPendingVulnerableVersionRange

      # Features
      field :create_feature, mutation: Mutations::CreateFeature
      field :update_feature, mutation: Mutations::UpdateFeature
      field :delete_feature, mutation: Mutations::DeleteFeature
      field :disable_feature, mutation: Mutations::DisableFeature
      field :enable_feature, mutation: Mutations::EnableFeature
      field :enable_feature_actor_percentage, mutation: Mutations::EnableFeatureActorPercentage
      field :enable_feature_time_percentage, mutation: Mutations::EnableFeatureTimePercentage
      field :enable_feature_group, mutation: Mutations::EnableFeatureGroup
      field :enable_feature_actor, mutation: Mutations::EnableFeatureActor
      field :disable_feature_group, mutation: Mutations::DisableFeatureGroup
      field :disable_feature_actor, mutation: Mutations::DisableFeatureActor

      field :enable_beta_feature, mutation: Mutations::EnableBetaFeature
      field :disable_beta_feature, mutation: Mutations::DisableBetaFeature

      # Experiments
      field :create_experiment, mutation: Mutations::CreateExperiment
      field :delete_experiment, mutation: Mutations::DeleteExperiment
      field :clear_experiment, mutation: Mutations::ClearExperiment
      field :enable_experiment_sampling, mutation: Mutations::EnableExperimentSampling
      field :disable_experiment_sampling, mutation: Mutations::DisableExperimentSampling
      field :adjust_experiment_percentage, mutation: Mutations::AdjustExperimentPercentage

      # ToggleableFeatures
      field :enroll_in_toggleable_feature, mutation: Mutations::EnrollInToggleableFeature
      field :unenroll_in_toggleable_feature, mutation: Mutations::UnenrollInToggleableFeature

      # Newsletters
      field :send_preview_newsletter, mutation: Mutations::SendPreviewNewsletter
      field :create_newsletter_subscription, mutation: Mutations::CreateNewsletterSubscription

      # Integration Category
      field :createIntegrationCategory, mutation: Mutations::CreateIntegrationCategory
      field :updateIntegrationCategory, mutation: Mutations::UpdateIntegrationCategory

      # Retired Namespaces
      field :retire_namespace, mutation: Mutations::RetireNamespace
      field :unretire_namespace, mutation: Mutations::UnretireNamespace

      # Deployments
      field :create_deployment, mutation: Mutations::CreateDeployment
      field :delete_deployment, mutation: Mutations::DeleteDeployment
      field :create_deployment_status, mutation: Mutations::CreateDeploymentStatus

      field :createBranchProtectionRule, mutation: Mutations::CreateBranchProtectionRule
      field :updateBranchProtectionRule, mutation: Mutations::UpdateBranchProtectionRule
      field :deleteBranchProtectionRule, mutation: Mutations::DeleteBranchProtectionRule

      # Interaction limits
      field :set_repository_interaction_limit, mutation: Mutations::SetRepositoryInteractionLimit
      field :set_organization_interaction_limit, mutation: Mutations::SetOrganizationInteractionLimit

      # Suggested changes
      field :apply_suggested_changes, mutation: Mutations::ApplySuggestedChanges

      # Users
      field :change_user_status, mutation: Mutations::ChangeUserStatus
      field :reorder_profile_pins, mutation: Mutations::ReorderProfilePins
      field :set_profile_pins, mutation: Mutations::SetProfilePins
      field :dismiss_notice, mutation: Mutations::DismissNotice
      field :follow_user, mutation: Mutations::FollowUser
      field :unfollow_user, mutation: Mutations::UnfollowUser

      # Mobile
      field :mobile_events_update, mutation: Mutations::MobileEventsUpdate
      field :update_user_mobile_time_zone, mutation: Mutations::UpdateUserMobileTimeZone
      field :create_mobile_subscription, mutation: Mutations::CreateMobileSubscription

      # Epochs
      field :store_epoch_operations, mutation: Mutations::StoreEpochOperations

      # Content attachments
      field :create_content_attachment, mutation: Mutations::CreateContentAttachment

      # GitHub Sponsors
      field :create_sponsors_listing,    mutation: Mutations::CreateSponsorsListing
      field :approve_sponsors_listing,   mutation: Mutations::ApproveSponsorsListing
      field :unpublish_sponsors_listing, mutation: Mutations::UnpublishSponsorsListing

      # Dashboard
      field :create_user_dashboard_pin, mutation: Mutations::CreateUserDashboardPin
      field :delete_user_dashboard_pin, mutation: Mutations::DeleteUserDashboardPin
      field :update_user_dashboard_pins, mutation: Mutations::UpdateUserDashboardPins
      field :set_user_dashboard_pins, mutation: Mutations::SetUserDashboardPins
      field :reorder_dashboard_pins, mutation: Mutations::ReorderDashboardPins

      # Dependabot
      field :mark_repository_dependency_update_complete, mutation: Mutations::MarkRepositoryDependencyUpdateComplete
      field :mark_repository_dependency_update_errored, mutation: Mutations::MarkRepositoryDependencyUpdateErrored

      # Abuse Reports
      field :report_content, mutation: Mutations::ReportContent

      # Repository archiving
      field :archive_repository, mutation: Mutations::ArchiveRepository
      field :unarchive_repository, mutation: Mutations::UnarchiveRepository

      # Import APIS
      field :create_import, mutation: Mutations::CreateImport
      field :create_repository_import, mutation: Mutations::CreateRepositoryImport
      field :import_pull_request, mutation: Mutations::ImportPullRequest

      # Discussions
      field :delete_discussion, mutation: Mutations::DeleteDiscussion
    end
  end
end
