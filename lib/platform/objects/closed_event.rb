# frozen_string_literal: true

module Platform
  module Objects
    class ClosedEvent < Platform::Objects::Base
      model_name "IssueEvent"
      description "Represents a 'closed' event on any `Closable`."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, closed_event)
        permission.belongs_to_issue_event(closed_event)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_issue(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      implements Interfaces::TimelineEvent

      implements Interfaces::PerformableViaApp

      implements Interfaces::UniformResourceLocatable

      global_id_field :id

      field :closable, Interfaces::Closable, "Object that was closed.", method: :async_issue_or_pull_request, null: false

      field :closer, Unions::Closer, "Object which triggered the creation of this event.", null: true

      def closer
        @object.async_visible_closer(@context[:permission])
      end

      url_fields description: "The HTTP URL for this closed event." do |event|
        event.async_path_uri
      end
    end
  end
end
