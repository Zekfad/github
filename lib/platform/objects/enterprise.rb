# frozen_string_literal: true

module Platform
  module Objects
    class Enterprise < Platform::Objects::Base
      model_name "Business"

      minimum_accepted_scopes ["read:enterprise"]

      description "An account to manage multiple organizations with consolidated policy and billing."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        permission.access_allowed?(:view_business_profile, resource: object,
          current_repo: nil, current_org: nil,
          allow_integrations: false, allow_user_via_integration: false)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return true if permission.viewer&.site_admin?
        object.readable_by?(permission.viewer)
      end


      global_id_field :id

      implements Platform::Interfaces::Node

      database_id_field

      created_at_field

      field :name, String, description: "The name of the enterprise.", null: false

      field :slug, String, description: "The URL-friendly identifier for the enterprise.", null: false

      field :description, String, description: "The description of the enterprise.", null: true

      field :description_html, Scalars::HTML, description: "The description of the enterprise as HTML.", null: false

      def description_html
        if @object.description.present?
          GitHub::Goomba::ProfileBioPipeline.to_html(@object.description, {})
        else
          GitHub::HTMLSafeString::EMPTY
        end
      end

      field :website_url, Scalars::URI, description: "The URL of the enterprise website.", null: true

      field :location, String, description: "The location of the enterprise.", null: true

      url_fields description: "The HTTP URL for this enterprise." do |business|
        template = Addressable::Template.new("/enterprises/{slug}")
        template.expand slug: business.slug
      end

      field :avatar_url, Scalars::URI, description: "A URL pointing to the enterprise's public avatar.", null: false do
        argument :size, Integer, "The size of the resulting square image.", required: false
      end

      def avatar_url(**arguments)
        @object.primary_avatar_url(arguments[:size])
      end

      field :members, resolver: Resolvers::EnterpriseMembers, description: "A list of users who are members of this enterprise.", connection: true

      field :organizations, resolver: Resolvers::EnterpriseOrganizations, description: "A list of organizations that belong to this enterprise.", connection: true

      field :viewer_is_admin, Boolean, null: false, description: "Is the current viewer an admin of this enterprise?"
      def viewer_is_admin
        @object.owner?(@context[:viewer])
      end

      field :owner_info, Objects::EnterpriseOwnerInfo, description: "Enterprise information only visible to enterprise owners.", null: true

      def owner_info
        return unless @object.owner?(@context[:viewer]) || @context[:viewer]&.site_admin?
        @object
      end

      field :billing_info, Objects::EnterpriseBillingInfo, description: "Enterprise billing information visible to enterprise billing managers.", null: true

      def billing_info
        return unless @object.owner?(@context[:viewer]) ||
          @object.billing_manager?(@context[:viewer]) ||
          @context[:viewer]&.site_admin?
        @object
      end

      field :user_accounts, Connections.define(Objects::EnterpriseUserAccount),
        description: "A list of user accounts on this enterprise.",
        connection: true, null: false

      def user_accounts
        @object.async_user_accounts.then do |result|
          ArrayWrapper.new(result)
        end
      end
    end
  end
end
