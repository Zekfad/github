# frozen_string_literal: true

module Platform
  module Objects
    class Costarrer < Platform::Objects::Base
      description "Costarrer"
      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        # This is internal data, anyone who can see this type can see the data
        true
      end

      field :costarrer_stars_count, Integer, description: "The number of stars considered in the costarrer calculations.", null: false
      field :similarity, Float, description: "Costarrer jaccard similarity coefficient.", null: false
      field :account, Unions::Account, description: "The associated account.", null: true

      def account
        Loaders::ActiveRecord.load(::User, @object[:costarrer_id])
      end
    end
  end
end
