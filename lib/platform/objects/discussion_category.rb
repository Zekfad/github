# frozen_string_literal: true

module Platform
  module Objects
    class DiscussionCategory < Platform::Objects::Base
      description "A category for discussions in a repository."
      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :discussions
      visibility :under_development

      feature_flag :discussions

      implements Platform::Interfaces::Node
      implements Interfaces::RepositoryNode

      # Internal: Determine whether the viewer can access this object via the API.
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, category)
        permission.async_repo_and_org_owner(category).then do |repo, org|
          permission.access_allowed?(
            :get_discussion_category,
            resource: category,
            current_org: org,
            repo: repo,
            allow_integrations: false,
            allow_user_via_integration: false)
        end
      end

      # Internal: Determine whether the viewer can see this object.
      # Returns `true`, `false`, or a `Promise` resolving to `true` or `false`.
      def self.async_viewer_can_see?(permission, category)
        category.async_readable_by?(permission.viewer)
      end

      global_id_field :id

      created_at_field

      updated_at_field

      field :name, String, "The name of this category.", null: false
      field :description, String, description: "A description of this category.", null: true
      field :emoji, String, "An emoji representing this category.", null: true
    end
  end
end
