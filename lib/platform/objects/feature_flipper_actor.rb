# frozen_string_literal: true

module Platform
  module Objects
    class FeatureFlipperActor < Platform::Objects::Base
      description "A piece of code that checks a feature flag"
      visibility :internal

      scopeless_tokens_as_minimum

      global_id_field :id

      implements Platform::Interfaces::Node

      field :name, String, description: "A human readable name for this group (ex maintainers_early_access)", null: false

      # Can't use `method: :to_s` option because it hits this class's `#to_s` method, not `@object.to_s`
      def name
        @object.to_s
      end

      def self.load_from_global_id(id)
        Promise.resolve(Flipper::Groups::Group.new(id))
      end
    end
  end
end
