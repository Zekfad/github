# frozen_string_literal: true

module Platform
  module Objects
    class TwoFactorCredential < Platform::Objects::Base
      description "A two-factor authentication enrollment."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_viewer(object)
      end

      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :delivery_method, Enums::TwoFactorDeliveryMethod, "The method used for obtaining challenges.", null: false
      field :number_of_remaining_codes, Int, description: "The number of unused recovery codes.", null: false
      field :sms_number, String, description: "The phone number used to deliver SMS codes (if any)", null: true
      field :backup_sms_number, String, description: "The phone number where backup codes can be delivered", null: true
    end
  end
end
