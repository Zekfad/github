# frozen_string_literal: true

module Platform
  module Objects
    class User < Platform::Objects::Base
      include Objects::Base::RecordObjectAccess
      include Helpers::Newsies

      description "A user is an individual's account on GitHub that owns repositories and can make new content."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, user)
        permission.access_allowed?(:read_user_public, resource: Platform::PublicResource.new, current_repo: nil, current_org: nil,  allow_integrations: true, allow_user_via_integration: true)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::Actor
      implements Interfaces::AvatarOwner
      implements Interfaces::PackageOwner
      implements Interfaces::PackageSearch
      implements Interfaces::ProjectOwner
      implements Interfaces::RepositoryDiscussionAuthor
      implements Interfaces::RepositoryDiscussionCommentAuthor
      implements Interfaces::RepositoryOwner
      implements Interfaces::MarketplaceListingOwner
      implements Interfaces::UniformResourceLocatable
      implements Interfaces::Billable
      implements Interfaces::PlanOwner
      implements Interfaces::FeatureFlaggable
      implements Interfaces::ProfileOwner
      implements Interfaces::Sponsorable

      global_id_field :id

      database_id_field

      created_at_field
      updated_at_field

      field :login, String, "The username used to login.", null: false

      url_fields description: "The HTTP URL for this user" do |user|
        template = Addressable::Template.new("/{login}")
        template.expand login: user.login
      end

      url_fields prefix: :update, description: "The update URL for this user", visibility: :internal do |user|
        template = Addressable::Template.new("/users/{login}")
        template.expand login: user.login
      end

      url_fields prefix: :projects, description: "The HTTP URL listing user's projects" do |user|
        template = Addressable::Template.new("/users/{login}/projects")
        template.expand login: user.login
      end

      field :status, Objects::UserStatus,
        description: "The user's description of what they're currently doing.", null: true

      def status
        @object.async_status_visible_to(@context[:viewer])
      end

      field :email, String, minimum_accepted_scopes: ["user:email", "read:user"], description: "The user's publicly visible profile email.", null: false

      def email
        @object.async_profile.then do
          target = @object.existing_primary_email_role&.public? ? nil : @object

          accessible = @context[:permission].can_view_user_email?(@object, target)

          if accessible
            @object.publicly_visible_email(logged_in: !@context[:viewer].nil?) || ""
          else
            ""
          end
        end
      end

      field :is_primary_email_role_public, Boolean, visibility: :internal, description: "Whether the primary email role is public", null: false

      def is_primary_email_role_public
        !!@object.existing_primary_email_role&.public?
      end

      field :possible_profile_emails, [String], visibility: :internal, description: "The user's verified email addresses", null: false

      def self.profile_field(field_name)
        field(field_name, String, null: true, description: "The user's public profile #{field_name}.")
        define_method(field_name) do
          @object.async_profile.then do |profile|
            profile.try(field_name)
          end
        end
      end

      profile_field(:name)
      profile_field(:location)
      profile_field(:bio)
      profile_field(:company)

      field :private_email, String, visibility: :internal, description: "The user's profile email without regard to privacy setting, for internal use only.", null: false

      def private_email
        @object.async_profile.then do |profile|
          profile && profile.email || ""
        end
      end

      field :show_profile_readme, Boolean,
        feature_flag: :pe_mobile,
        description: "Whether or not a user's profile readme is currently visible",
        null: false,
        method: :async_profile_readme_visible?

      field :configuration_repository, Objects::Repository,
        feature_flag: :pe_mobile,
        description: "The user's profile configuration repository",
        null: true,
        method: :async_configuration_repository

      field :profile_readme, Objects::RepositoryReadme,
        feature_flag: :pe_mobile,
        description: "The user's profile readme.",
        null: true

      def profile_readme
        Promise.all([
          @object.async_profile_readme_visible?,
          @object.async_profile_readme
        ]).then do |is_visible, readme|
          next unless is_visible
          readme
        end
      end


      field :no_verified_email, Boolean, visibility: :internal, description: "The user posseses no verified email address", null: false

      def no_verified_email
        ::UserEmail.where(user_id: @object.id).verified.none?
      end

      field :bio_html, Scalars::HTML,
        description: "The user's public profile bio as HTML.",
        method: :async_profile_bio_html,
        null: false

      field :company_html, Scalars::HTML,
        description: "The user's public profile company as HTML.",
        method: :async_profile_company_html,
        null: false

      field :website_url, Scalars::URI, description: "A URL pointing to the user's public website/blog.", null: true

      def website_url
        @object.async_profile.then do |profile|
          profile.try(:blog)
        end
      end

      field :twitter_username, String, description: "The user's Twitter username.", null: true

      def twitter_username
        @object.async_profile.then do |profile|
          profile.try(:twitter_username)
        end
      end

      field :twitter_url, Scalars::URI, description: "A URL pointing to the user's Twitter profile", null: true, visibility: :internal

      def twitter_url
        @object.async_profile.then do |profile|
          profile.try(:twitter_url)
        end
      end

      field :has_ever_contributed, Boolean, null: false, visibility: :internal,
        description: "Determine if the user has ever created a repository, made a commit " \
                     "contribution, opened an issue, opened a pull request, or left a pull " \
                     "request review.", method: :any_contributions_ever?

      field :avatar_url, Scalars::URI, description: "A URL pointing to the user's public avatar.", null: false do
        argument :size, Integer, "The size of the resulting square image.", required: false
      end

      def avatar_url(**arguments)
        preload_promise =
          # TODO we need to remove this condition. The `Bot` object should be responsible
          # for the avatarUrl of bot records. If a ::Bot somehow is being resolved into
          # here, then the field that marshaled it needs to be updated (perhaps to Actor)
          if @object.is_a?(::Bot)
            @object.async_integration.then(&:async_owner)
          else
            Promise.resolve(nil)
          end

        preload_promise.then do
          @object.async_primary_avatar.then do
            @object.primary_avatar_url(arguments[:size])
          end
        end
      end

      field :is_hireable, Boolean, description: "Whether or not the user has marked themselves as for hire.", null: false

      def is_hireable
        @object.async_profile.then do |profile|
          !!(profile && profile.hireable?)
        end
      end

      field :display_staff_badge, Boolean, visibility: :internal, description: "Whether or not staff have marked to display staff badge.", null: false

      def display_staff_badge
        @object.async_profile.then do |profile|
          profile.try(:display_staff_badge?) || false
        end
      end

      field :show_staff_badge_on_profile, Boolean, feature_flag: :pe_mobile, description: "Whether or not the staff badge should be displayed on profile and hovercard.", null: false

      def show_staff_badge_on_profile
        @object.async_profile.then do |profile|
          !!@object.show_staff_badge_to?(context[:viewer])
        end
      end

      field :show_pro_plan_badge_on_profile, Boolean, feature_flag: :pe_mobile, description: "Whether or not the pro badge should be displayed on profile and hovercard.", null: false, method: :has_pro_plan_badge?

      field :can_have_pro_badge, Boolean, visibility: :internal, description: "Whether or not the user can display a pro badge on their profile.", null: false, method: :can_have_pro_badge?

      field :is_pro_plan, Boolean, feature_flag: :pe_mobile, description: "Whether or not the user has an active pro plan.", null: true

      def is_pro_plan
        if is_viewer
          @object.plan.pro?
        else
          nil
        end
      end

      field :team_app_url, Scalars::URI, visibility: :internal, description: "The team app URL for this employee", null: true

      field :is_bounty_hunter, Boolean, "Whether or not this user is a participant in the GitHub Security Bug Bounty.", method: :bounty_hunter?, null: false
      field :is_campus_expert, Boolean, "Whether or not this user is a participant in the GitHub Campus Experts Program.", method: :campus_expert?, null: false

      field :is_spammy, Boolean, visibility: :internal, description: "Whether or not the user is spammy.", null: false, method: :spammy?

      field :is_hammy, Boolean, visibility: :internal, description: "Whether or not the user is hammy.", null: false, method: :hammy?

      field :viewer_can_follow, Boolean, description: "Whether or not the viewer is able to follow the user.", null: false

      def viewer_can_follow
        viewer = @context[:viewer]
        viewer.nil? ? false : Loaders::CanFollowCheck.load(viewer.id, @object.id)
      end

      field :is_employee, Boolean, description: "Whether or not this user is a GitHub employee.", null: false

      def is_employee
        @object.employee? && !GitHub.hidden_teamster?(@object)
      end

      field :via_actions, Boolean, description: "Whether the viewer was authenticated via the GitHub Actions integration.", null: true, visibility: :internal

      def via_actions
        return nil unless is_viewer
        Platform::Helpers::ViaActions.request_via_actions?(context: context)
      end

      field :is_developer_program_member, Boolean, description: "Whether or not this user is a GitHub Developer Program member.", null: false

      def is_developer_program_member
        @object.async_developer_program_membership.then do |membership|
          !!membership.try(:active?)
        end
      end

      field :is_prerelease_agreement_signed, Boolean, description: "Whether or not this user has signed the GitHub Prerelease Program agreement.", null: false do
        visibility :internal, environments: [:dotcom]
      end

      def is_prerelease_agreement_signed
        @object.async_prerelease_agreement.then do |agreement|
          @object.prerelease_agreement_signed?
        end
      end

      field :is_org_prerelease_agreement_signed, Boolean, method: :async_org_prerelease_agreement_signed?, description: "Whether this user belongs to an org that has signed the GitHub Prerelease Program agreement.", null: false do
        visibility :internal, environments: [:dotcom]
      end

      field :is_site_admin, Boolean, description: "Whether or not this user is a site administrator.", null: false

      def is_site_admin
        @object.async_two_factor_credential.then {
          @object.site_admin?
        }
      end

      field :viewer_is_following, Boolean, description: "Whether or not this user is followed by the viewer.", null: false

      def viewer_is_following
        viewer = @context[:viewer]
        viewer.nil? ? false : Loaders::IsFollowingCheck.load(viewer.id, @object.id)
      end

      field :is_blocked_by_viewer, Boolean, "Whether or not this user is blocked by the viewer", null: false, visibility: :internal

      def is_blocked_by_viewer
        @object.blocked_by?(@context[:viewer])
      end

      field :is_account_successor_for_viewer, Boolean, description: "Whether or not this user is the viewer's account successor", null: false, visibility: :under_development

      def is_account_successor_for_viewer
        @object.account_successor_for?(@context[:viewer])
      end

      field :is_viewer, Boolean, description: "Whether or not this user is the viewing user.", null: false

      def is_viewer
        @object == @context[:viewer]
      end

      field :has_dismissed_notice, Boolean, visibility: :under_development, description: "Whether or not this user has dismissed the given notice", null: true do
        argument :notice, String, "Name of notice to check if dismissed", required: true
      end

      def has_dismissed_notice(**arguments)
        if is_viewer
          @object.dismissed_notice?(arguments[:notice])
        end
      end

      field :has_email, Boolean, visibility: :internal, description: "Whether or not this user has a profile email, which may or may not be visible to the viewing user.", null: false

      def has_email
        @object.async_profile.then do
          @object.publicly_visible_email(logged_in: true).present?
        end
      end

      field :show_private_contribution_count, Boolean, visibility: :under_development,
        description: "Does the user want their contributions to private repositories to be shown in the contributions graph and also summarized in the activity list on their profile?",
        null: false

      def show_private_contribution_count
        profile_settings = @object.profile_settings
        profile_settings.show_private_contribution_count?
      end

      field :activity_overview_enabled, Boolean, visibility: :under_development,
        description: "Does the user want the 'Activity overview' section shown on their profile?",
        null: false

      def activity_overview_enabled
        profile_settings = @object.profile_settings
        profile_settings.activity_overview_enabled?
      end

      field :action_invocation_blocked, Boolean, "Indicates if action invocation is blocked for this user", method: :action_invocation_blocked?, batch_load_configuration: true, null: false, visibility: :internal

      field :stafftools_info, Objects::UserStafftoolsInfo, visibility: :internal, description: "User information only visible to site admin", null: true

      def stafftools_info
        if self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          Models::AccountStafftoolsInfo.new(@object)
        else
          nil
        end
      end

      field :suspended_at, Scalars::DateTime, minimum_accepted_scopes: ["site_admin"], description: "Identifies the date and time when the user was suspended.", null: true do
        visibility :public, environments: [:enterprise]
        visibility :internal, environments: [:dotcom]
      end

      # This is needed for the `GET /user` REST API. It would be nicer to
      # expose a `Repository.collaborators` connection and grab the
      # `totalCount` from that instead, but doing that would require a way to
      # sum the `totalCount` values from all repositories. For now we'll just
      # use an internal field that gives us the data the REST API needs.
      field :collaborators_count, Integer, visibility: :internal, description: "The number of unique users who have access to this user's private repositories.", null: false

      # It could be nice to make this public someday. Presumably some
      # Enterprise users will want it, and it could just be null on dotcom.
      field :ldapDN, String, description: "The user's LDAP distinguished name.", null: true, visibility: :internal

      def ldap_dn
        @object.async_ldap_mapping.then do
          @object.ldap_dn
        end
      end

      field :is_large_bot_account, Boolean,
        visibility: :internal,
        description: "Returns true if the user is a large bot account.",
        null: false,
        method: :large_bot_account?

      field :plan, Objects::Plan, visibility: :internal, description: "The user's billing plan.", null: true

      def plan
        if is_viewer
          @object.plan
        else
          nil
        end
      end

      field :duration, String, visibility: :internal, description: "The user's billing cycle for their plan subscription e.g 'month', 'year'.", null: true

      def duration
        if is_viewer
          @context[:viewer]&.plan_duration || :month
        else
          nil
        end
      end

      field :has_apple_iap_subscription, Boolean, feature_flag: :pe_mobile, description: "Indicates if a user's plan subscription is an apple iap subscription", null: false

      def has_apple_iap_subscription
        @object.async_plan_subscription.then do
          @object.apple_iap_subscription?
        end
      end

      field :has_two_factor_authentication_enabled, Boolean, visibility: :internal, description: "Indicates if the user has enabled two factor authentication.", null: true

      def has_two_factor_authentication_enabled
        if is_viewer
          @object.async_two_factor_credential.then do
            @object.two_factor_authentication_enabled?
          end
        else
          nil
        end
      end

      field :analytics_tracking_id, String, description: "The unique analytics tracking ID for this user", null: true do
        visibility :internal, environments: [:dotcom]
      end

      field :organization, Objects::Organization, description: "Find an organization by its login that the user belongs to.", null: true do
        argument :login, String, "The login of the organization to find.", required: true
      end

      def organization(**arguments)
        scope = @object.organizations

        org = scope.find_by_login(arguments[:login])
        return nil unless org
        public_member = org.public_member?(@object)
        return nil unless public_member || org.member_or_can_view_members?(@context[:viewer])

        if public_member
          @context[:permission].can_get_public_org_member? ? org : nil
        else
          @context[:permission].can_get_private_org_member?(@object, resource: "members") ? org : nil
        end
      end

      field :contributions_collection, ContributionsCollection, description: "The collection of contributions this user has made to different repositories.", null: false do
        argument :organizationID, ID, "The ID of the organization used to filter contributions.", required: false,
          as: :organization_id # for legacy reasons, this is `organizationID` in GraphQL instead of `organizationId`
        argument :contribution_types, [Enums::ContributionsCollectionContributionType, null: true], <<~DESCRIPTION, required: false
          If provided, include only the specified types of contributions.
          Defaults to all types except issue comments (created repositories, commits, issues, pull requests,
          pull request reviews, joined GitHub, joined an organization, and anonymized contributions to GHE)
        DESCRIPTION
        argument :from, Scalars::DateTime, "Only contributions made at this time or later will be counted. If omitted, defaults to a year ago.", required: false
        argument :to, Scalars::DateTime, "Only contributions made before and up to and including this time will be counted. If omitted, defaults to the current time.", required: false
      end

      def contributions_collection(**arguments)
        organization = if arguments[:organization_id]
          Helpers::NodeIdentification.typed_object_from_id([Objects::Organization],
                                                           arguments[:organization_id], @context)
        end

        time_range = build_and_validate_contributions_time_range(**arguments.slice(:to, :from))

        viewer = @context[:viewer] if @context[:permission].can_access_private_contributions?(@object)
        Loaders::ContributionCollector.load(
          user: @object,
          time_range: time_range,
          viewer: viewer,
          organization_id: organization&.id,
          contribution_classes: arguments[:contribution_types],
          excluded_organization_ids: @context[:unauthorized_organization_ids],
        )
      end

      field :received_reviews, Connections.define(Objects::PullRequestReview), visibility: :internal, description: "Pull request reviews received by this user", null: false, connection: true

      def received_reviews
        return ArrayWrapper.new([]) unless is_viewer

        ::PullRequestReview
          .submitted
          .joins(:pull_request)
          .where(pull_requests: { user_id: @object.id })
          .order("pull_request_reviews.submitted_at DESC")
          .filter_spam_for(@context[:viewer])
      end

      field :saved_replies, Connections.define(Objects::SavedReply), description: "Replies this user has saved", null: true, connection: true do
        argument :order_by, Inputs::SavedReplyOrder, "The field to order saved replies by.", required: false, default_value: { field: "updated_at", direction: "DESC" }
      end

      def saved_replies(**arguments)
        return ArrayWrapper.new([]) unless is_viewer

        if order_by = arguments[:order_by]
          ::SavedReply.where(user_id: @object.id).order "saved_replies.#{order_by[:field]} #{order_by[:direction]}"
        else
          ::SavedReply.where(user_id: @object.id)
        end
      end

      # NOTE: If you want to make this public, you'll need to fix the permission here:
      #
      # https://github.com/github/github/blob/698c661e629f44ac17e91aeea431d79f74c0a7f1/lib/platform/authorization/permission.rb#L683
      #
      # While this field is only being used by the app, it's fine to include all of the reviews requested
      # of the viewer. If apps are going to be requesting them on behalf of users, it'll need better permissions.
      #
      # Since this is only visible to `viewer`, spam filtering is a no-op
      field :reviews, Connections.define(Objects::PullRequestReview), visibility: :internal, description: "Pull request reviews created by this user", null: false, connection: true, exempt_from_spam_filter_check: true

      def reviews
        return ArrayWrapper.new([]) unless is_viewer

        ::PullRequestReview.submitted.where(user_id: @object.id).order("pull_request_reviews.submitted_at DESC")
      end

      # Since this is only visible to `viewer`, spam filtering is a no-op
      field :review_requests, Connections.define(Objects::ReviewRequest), visibility: :internal, description: "Review requests for this user", null: false, connection: true, exempt_from_spam_filter_check: true

      def review_requests
        return ArrayWrapper.new([]) unless is_viewer

        ::ReviewRequest.where(reviewer_id: @object.id).order("created_at DESC")
      end

      field :repositories_contributed_to, resolver: Resolvers::RepositoriesContributedTo, description: "A list of repositories that the user recently contributed to.", connection: true

      field :actionUsableRepositories, resolver: Resolvers::ActionUsableRepositories, visibility: :internal, description: "A list of repositories that the user can add an action to.", connection: true

      # We're going to bypass GraphQL-Ruby's built-in connection support
      # and manually return a connection object, so use `connection: false`
      field :repository_recommendations, Connections::RepositoryRecommendation, null: false,
        feature_flag: :pe_mobile,
        connection: false do
          has_connection_arguments
          description <<~DESCRIPTION
            Returns recommendations for repositories that GitHub thinks the current viewer would find
            interesting.
          DESCRIPTION
      end

      def repository_recommendations(**arguments)
        user_to_fetch = if @context[:viewer] && @context[:viewer] == @object
          @object
        end
        ConnectionWrappers::RepositoryRecommendationConnection.new(user_to_fetch, arguments)
      end

      field :recent_interactions, [Objects::RecentInteraction], feature_flag: :pe_mobile, description: "Objects the user has recently interacted with.", null: false do
        argument :types, [Enums::InteractableType, null: true], "Filter the objects the user has interacted with to just these types.", default_value: [:issue, :pull_request], required: false
        argument :limit, Integer, "How many recent interactions to return.", default_value: 10, required: false
        argument :since, Scalars::DateTime, <<~DESCRIPTION, required: false
            Cutoff time in the past. Only records with an interaction since then will be considered.
            Defaults to one week ago.
          DESCRIPTION
        argument :organizationID, ID,
          "Optional ID of an organization to use to filter the activity returned.",
          required: false,
          as: :organization_id # for legacy reasons, this is `organizationID` in GraphQL instead of `organizationId`
      end

      def recent_interactions(**arguments)
        result = if is_viewer
          types = arguments[:types]
          since = arguments[:since] || 2.weeks.ago
          limit = arguments[:limit]
          org_global_id = arguments[:organization_id]

          org = if org_global_id
            Helpers::NodeIdentification.typed_object_from_id([Objects::Organization], org_global_id,
                                                             @context)
          end

          if types.include?(:issue) || types.include?(:pull_request)
            fetcher = ::Issue::RecentInteractions.new(
              @context[:viewer],
              since: since,
              types: types,
              organization_id: org.try(:id),
              excluded_organization_ids: @context[:unauthorized_organization_ids],
            )
            fetcher.fetch(limit: limit)
          else
            []
          end
        else
          []
        end

        ArrayWrapper.new(result)
      end

      # Inject field here to call the resolver in isolation, ick.
      field :top_repositories, Connections.define(Objects::Repository), description: <<~DESCRIPTION, null: false, connection: true, extras: [:itself] do
          Repositories the user has contributed to, ordered by contribution rank, plus repositories the user has created
        DESCRIPTION

        argument :order_by, Inputs::RepositoryOrder, "Ordering options for repositories returned from the connection", required: true
        argument :since, Scalars::DateTime, "How far back in time to fetch contributed repositories", required: false
      end

      def top_repositories(itself:, **arguments)
        resolver = Platform::Resolvers::Repositories.new(object: @context[:viewer], context: @context, field: itself)
        user_repos = resolver.resolve(order_by: arguments[:order_by], affiliations: [:owned, :direct], owner_affiliations: [:owned, :direct]).to_a
        ranked_repos = ranked_contributed_repositories(
          include_issue_comments: true,
          exclude_owned: false,
          since: arguments[:since],
        )
        all_repos = ranked_repos + user_repos
        ArrayWrapper.new(all_repos.uniq(&:id))
      end

      field :ranked_contributed_repositories, Connections.define(Objects::Repository), visibility: :internal, description: <<~DESCRIPTION, null: false, connection: true do
          Repositories the user has contributed to, ordered by recent contributions.
        DESCRIPTION

        argument :exclude_owned, Boolean, "Exclude repositories owned by this user.", default_value: true, required: false
        argument :include_issue_comments, Boolean, "Whether or not to include issue comments for ranking contributions", required: false, default_value: false
        argument :weighted, Boolean, "Whether or not to weight the ranking for the contributions based on recency and contribution type", required: false, default_value: true
        argument :since, Scalars::DateTime, "How far back in time to fetch contributed repositories", required: false

        # Keeping this internal for now. Would like to combine with contributedRepositories
        # connection, but the ContributedRepositories function must return an ActiveRecord relation
        # or you get an error from scopes being tacked on later. Want to preserve the rank ordering
        # that #ranked_contributed_repositories gives us, but it returns an array of repositories
        # and not a relation. See Slack discussion:
        # https://github.slack.com/archives/C44NVGUBB/p1501778272737309
      end

      def ranked_contributed_repositories(**arguments)
        if @object.large_bot_account?
          # FIXME: This quick exit for the large_bot_account can be removed once we
          # have solved the performance issues surrounding contribution graphs.
          #
          # see https://github.com/github/core-app/issues/84
          ArrayWrapper.new([])
        else
          since = arguments[:since] || 1.year.ago
          since = 1.year.ago if since < 1.year.ago
          since = 1.year.ago if since > Time.zone.now

          repositories = @object.repositories_contributed_to(
            viewer: @context[:viewer],
            limit: nil,
            exclude_owned: arguments[:exclude_owned],
            include_issue_comments: arguments[:include_issue_comments],
            since: since,
            weighted: arguments[:weighted],
          )

          if context[:unauthorized_organization_ids].present?
            repositories.reject! { |repo| context[:unauthorized_organization_ids].include?(repo.owner_id) }
          end

          ArrayWrapper.new(repositories.compact.select(&:active?))
        end
      end

      field :organizations_contributed_to, Connections.define(Objects::Organization), visibility: :under_development, connection: true, null: false, description: "A ranked list of organizations this user has contributed to" do
        argument :to, Scalars::DateTime, "Only contributions made before and up this time will be counted. If omitted, defaults to the current time.", required: false
        argument :from, Scalars::DateTime, "Only contributions made at this time or later will be counted. If omitted, defaults to a year ago.", required: false
        argument :selectedOrganizationID, ID, "An optional ID for an organization that will be included in the results even if no contributions were made to that organization.", required: false,
          as: :selected_organization_id
      end

      def organizations_contributed_to(**arguments)
        time_range = build_and_validate_contributions_time_range(**arguments.slice(:to, :from))

        viewer = @context[:viewer] if @context[:permission].can_access_private_contributions?(@object)
        collector_args = {
          user: @object,
          time_range: time_range,
          viewer: viewer,
          organization_id: nil,
          excluded_organization_ids: @context[:unauthorized_organization_ids],
        }

        Loaders::ContributionCollector.load(**collector_args).then do |collector|
          org = if arguments[:selected_organization_id]
            Helpers::NodeIdentification.
              typed_object_from_id([Objects::Organization], arguments[:selected_organization_id],
                                   context)
          end
          collector.organizations_contributed_to(selected_organization_id: org&.id)
        end
      end

      field :commit_comments, resolver: Resolvers::CommitComments, description: "A list of commit comments made by this user.", connection: true

      field :pull_requests, resolver: Resolvers::UserPullRequests, description: "A list of pull requests associated with this user.", connection: true

      field :issues_and_pull_requests, resolver: Resolvers::IssuesAndPullRequests, description: "A list of pull requests and issues associated with this user.", feature_flag: :pe_mobile, connection: true

      field :issues, resolver: Resolvers::Issues, description: "A list of issues associated with this user.", connection: true

      field :gist_comments, resolver: Resolvers::GistComments, description: "A list of gist comments made by this user.", connection: true

      field :issue_comments, resolver: Resolvers::IssueComments, description: "A list of issue comments made by this user.", connection: true

      field :public_keys, Connections.define(Objects::PublicKey),
        null: false,
        description: "A list of public keys associated with this user."

      def public_keys
        if context[:permission].access_allowed?(:read_user_public, resource: Platform::PublicResource.new, current_repo: nil, current_org: nil, allow_integrations: true, allow_user_via_integration: true)
          object.public_keys.scoped
        else
          ::PublicKey.none
        end
      end

      field :following, Connections::Following, description: "A list of users the given user is following.", null: false, connection: true do
        argument :user_database_ids, [Integer, null: true], "Optional list of user IDs to filter results. If provided, only following users in this list will be returned",
          visibility: :internal, required: false
        argument :order_by, Inputs::FollowOrder, "How to order the followed users. Defaults to most recently followed users first.", required: false,
          visibility: :under_development, default_value: { field: "followed_at", direction: "DESC" }
      end

      def following(user_database_ids: nil, order_by: nil)
        followings = ::Following.for(@object, viewer: @context[:viewer], order_by: order_by,
                                     type: :followed_user)
        followings = followings.where("following_id IN (?)", user_database_ids) if user_database_ids
        followings
      end

      field :followers, Connections::Follower, description: "A list of users the given user is followed by.", null: false, connection: true do
        argument :order_by, Inputs::FollowOrder, "How to order the followers. Defaults to most recent followers first.", required: false,
          visibility: :under_development, default_value: { field: "followed_at", direction: "DESC" }
      end

      def followers(order_by: nil)
        ::Following.for(@object, viewer: @context[:viewer], order_by: order_by,
                        type: :follower)
      end

      field :mutual_followers, Connections::User, null: false, connection: true, visibility: :under_development, description: "A list of users who are followed by both you and the current user" do
        argument :order_by, Inputs::FollowOrder, "Ordering options for mutual followers returned from the connection. Defaults to most recent followers first.", required: false,
          visibility: :under_development, default_value: { field: "followed_at", direction: "DESC" }
      end

      def mutual_followers(order_by:)
        @object.mutual_followers(viewer: context[:viewer], order_by: order_by)
      end

      field :gist, Objects::Gist, description: "Find gist by repo name.", null: true do
        argument :name, String, "The gist name to find.", required: true
      end

      def gist(**arguments)
        Loaders::ActiveRecord.load(::Gist, arguments[:name], column: :repo_name)
      end

      field :gists, resolver: Resolvers::Gists, description: "A list of the Gists the user has created.", connection: true

      field :watching, resolver: Resolvers::WatchedRepositories, description: "A list of repositories the given user is watching.", connection: true

      field :organizations, resolver: Resolvers::Organizations, description: "A list of organizations the user belongs to.", connection: true

      field :member_organizations, [Organization], visibility: :under_development, null: false,
          description: <<~DESCRIPTION do
            A list of the organizations this user belongs to, including those the user is a billing
            manager of if the viewer is the user.
          DESCRIPTION
        argument :limit, Integer, required: false, default_value: 10,
          description: "How many organizations to return."
      end

      def member_organizations(**arguments)
        limit = arguments[:limit].clamp(1, 100)

        if is_viewer
          orgs_belonged_to = @object.organizations.includes(:profile).limit(limit).
            filter_spam_for(@context[:viewer])
          orgs_managed = @object.billing_manager_organizations.includes(:profile).limit(limit).
              filter_spam_for(@context[:viewer])
          all_orgs = orgs_belonged_to | orgs_managed
          all_orgs.take(limit)
        else
          @object.public_organizations.includes(:profile).limit(limit).
            filter_spam_for(@context[:viewer])
        end
      end

      # Since this is only visible to `viewer`, spam filtering is a no-op
      field :owned_organizations, Connections.define(Objects::Organization), visibility: :internal, exempt_from_spam_filter_check: true, null: false, connection: true, description: <<~DESCRIPTION do
          A list of the organizations the user owns. Returns an empty list for a user other than
          the current viewer.
        DESCRIPTION

        argument :only_non_business_organizations, Boolean, required: false, default_value: false,
          description: "When true, response only includes Organizations that don't belong to a Business"
      end

      def owned_organizations(**arguments)
        if is_viewer
          organizations = @object.owned_organizations

          if arguments[:only_non_business_organizations]
            organizations = organizations
              .joins("LEFT OUTER JOIN business_organization_memberships ON business_organization_memberships.organization_id = users.id")
              .where("business_organization_memberships.id IS NULL")
          end

          organizations
        else
          Platform::ArrayWrapper.new([])
        end
      end

      field :enterprises, Connections.define(Objects::Enterprise), "A list of enterprises that the user belongs to.",
        null: false, connection: true,
        visibility: {
          internal: { environments: [:enterprise] },
          under_development: { environments: [:dotcom] },
        } do
        argument :order_by, Inputs::EnterpriseOrder,
          "Ordering options for the User's enterprises.",
          required: false, default_value: { field: "name", direction: "ASC" }

        argument :membership_type, Platform::Enums::EnterpriseMembershipType,
          "Filter enterprises returned based on the user's membership type.",
          required: false, default_value: :all
      end

      def enterprises(**arguments)
        businesses = @object.businesses(membership_type: arguments[:membership_type])

        if order_by = arguments[:order_by]
          businesses = businesses.order "businesses.#{order_by[:field]} #{order_by[:direction]}"
        end

        businesses
      end

      field :owned_organization, Objects::Organization, visibility: :internal, description: "Look up an organization owned by the current viewer that has the given login.", null: true do
        argument :login, String, "The organization's login.", required: true
      end

      def owned_organization(**arguments)
        if @context[:viewer] == @object
          @object.owned_organizations.where(login: arguments[:login]).first
        end
      end

      field :hovercard, Objects::Hovercard, description: "The hovercard information for this user in a given context", null: false do
        argument :primary_subject_id, ID, "The ID of the subject to get the hovercard in the context of", required: false
      end

      def hovercard(primary_subject_id: nil)
        viewer = @context[:viewer]

        primary_subject = nil
        if primary_subject_id
          begin
            primary_subject = Helpers::NodeIdentification.typed_object_from_id([Objects::Issue, Objects::SponsorsListing, Objects::PullRequest, Objects::Organization, Objects::Repository], primary_subject_id, @context)
          rescue Errors::NotFound
            raise Errors::NotFound, "Could not find the primary subject"
          end
        end

        ::UserHovercard.new(@object, primary_subject, viewer: viewer)
      end

      field :teams, Connections.define(Objects::Team), visibility: :internal, null: false, connection: true, description: <<~DESCRIPTION do
        A list of the teams the user belongs to that are visible to the viewer.
      DESCRIPTION
        argument :order_by, Inputs::TeamOrder,
          "Ordering options for teams returned from the connection. If omitted, teams will be ranked based on the user's activity within them.",
          required: false
        argument :organizationID, ID,
          "Optional ID of an organization to use to filter the teams returned.", required: false, as: :organization_id
      end

      def teams(**arguments)
         @object.async_visible_teams_for(context[:viewer]).then do |scope|
          if order_by = arguments[:order_by]
            order = "#{order_by[:field]} #{order_by[:direction]}"
            scope = scope.order(order)
          else
            scope = ::Team.ranked_for(@object, scope: scope)
          end

          if org_global_id = arguments[:organization_id]
            Helpers::NodeIdentification.async_typed_object_from_id(Objects::Organization, org_global_id, context).then do |org|
              if org
                scope.owned_by(org)
              else
                scope
              end
            end
          else
            scope
          end
         end
      end

      field :starred_repositories, resolver: Resolvers::StarredRepositories, description: "Repositories the user has starred.", null: false, connection: false

      field :starred_topics, visibility: :under_development, resolver: Resolvers::StarredTopics,
        description: "Topics the user has starred.", null: false, connection: true

      field :adminable_apps, Connections.define(Objects::App), visibility: :internal, description: "A list of GitHub Apps managed by this user.", null: false, connection: true do
        argument :exclude_marketplace_listings, Boolean, <<~DESCRIPTION, required: false
            Filters apps to exclude those that have a Marketplace listing. If omitted,
            integrations that are in the Marketplace will be included.
          DESCRIPTION
        argument :public_only, Boolean, <<~DESCRIPTION, required: false
            Filters apps so only public apps are returned. If omitted or false, both
            public and internal apps will be returned.
          DESCRIPTION
      end

      def adminable_apps(**arguments)
        if is_viewer
          scope = ::Integration.adminable_by(@context[:viewer]).not_for_github_connect
          scope = scope.not_in_marketplace if arguments[:exclude_marketplace_listings]
          scope = scope.public if arguments[:public_only]
          scope = scope.where.not(owner_id: @context[:unauthorized_organization_ids])
          scope.order("integrations.updated_at DESC")
        else
          Platform::ArrayWrapper.new([])
        end
      end

      field :adminable_oauth_applications, Connections.define(Objects::OauthApplication), visibility: :internal, description: "A list of OAuth applications managed by this User.", null: false, connection: true do
        argument :exclude_marketplace_listings, Boolean, "Filters OAuth applications to exclude those that have a Marketplace listing. If omitted, OAuth applications that are in the Marketplace will be included.", required: false
      end

      def adminable_oauth_applications(**arguments)
        if is_viewer
          scope = ::OauthApplication.adminable_by(@context[:viewer])
          scope = scope.not_in_marketplace if arguments[:exclude_marketplace_listings]
          scope = scope.where.not(user_id: @context[:unauthorized_organization_ids])
          scope.order("oauth_applications.updated_at DESC")
        else
          Platform::ArrayWrapper.new([])
        end
      end

      field :adminable_actions, Connections.define(Objects::RepositoryAction), visibility: :internal, description: "A list of actions managed by this User.", null: false, connection: true do
        argument :exclude_marketplace_listings, Boolean, "Filters actions to exclude those that have a Marketplace listing. If omitted, actions that are in the Marketplace will be included.", required: false, default_value: false
        argument :order_by, Inputs::RepositoryActionOrder, "Ordering options for actions returned from the connection.", required: false, default_value: { field: "updated_at", direction: "DESC" }
      end

      def adminable_actions(**arguments)
        if is_viewer
          scope = ::RepositoryAction.adminable_by(@context[:viewer])
          scope = scope.not_in_marketplace if arguments[:exclude_marketplace_listings]
          scope = scope.joins(:repository).where.not(repositories: { owner_id: @context[:unauthorized_organization_ids] })
          scope.order("repository_actions.#{arguments[:order_by][:field]} #{arguments[:order_by][:direction]}")
        else
          Platform::ArrayWrapper.new([])
        end
      end

      field :delegated_recovery_tokens, Connections.define(Objects::DelegatedRecoveryToken), visibility: :internal, description: "A list of recovery tokens for a user", null: false, connection: true do
        argument :providers, [String, null: true], "filter by a list of providers", required: true
      end

      def delegated_recovery_tokens(**arguments)
        if is_viewer
          @object.delegated_recovery_tokens.confirmed.where(provider: arguments[:providers])
        else
          ::DelegatedRecoveryToken.none
        end
      end

      field :authentication_records, Connections.define(Objects::AuthenticationRecord), null: false, connection: true do
        description "A list of successful logins for a user"
        argument :since, Scalars::DateTime, "return records created after a certain time", required: true
        visibility :internal
      end

      def authentication_records(since:, country_code: nil, octolytics_id: nil)
        if is_viewer
          @object.authentication_records.recent(since)
        else
          ::AuthenticationRecord.none
        end
      end

      field :order_preview, Objects::MarketplaceOrderPreview, visibility: :internal, description: "The user's order preview for a Marketplace listing.", null: true do
        argument :listing_slug, String, "The slug for the Marketplace listing", required: true
      end

      def order_preview(**arguments)
        Loaders::ActiveRecord.load(::Marketplace::Listing, arguments[:listing_slug], column: :slug).then do |listing|
          next unless listing

          Marketplace::OrderPreview.
            with_valid_listing_data.
            where(user_id: @object.id, marketplace_listing_id: listing.id).
            first
        end
      end

      field :order_previews, Platform::Connections.define(Objects::MarketplaceOrderPreview), visibility: :internal, description: "The user's pending Marketplace orders", null: false, connection: true do
        argument :only_retargeting_notice_triggered, Boolean, "Only returns order previews for which a retargeting notice has been triggered", required: false, default_value: false
      end

      def order_previews(**arguments)
        relation = @object.marketplace_order_previews.with_valid_listing_data.order(:viewed_at)
        relation = relation.retargeting_notice_triggered if arguments[:only_retargeting_notice_triggered]
        relation
      end

      field :marketplace_subscriptions, Platform::Connections.define(Objects::SubscriptionItem), numeric_pagination_enabled: true, visibility: :internal, description: "A list of Marketplace subscriptions for a user.", null: false, connection: true do
        argument :marketplace_listing_id, ID, "Limit subscriptions to a particular Marketplace listing", required: false
      end

      def marketplace_subscriptions(**arguments)
        return ArrayWrapper.new([]) unless is_viewer

        subscription_items = Billing::SubscriptionItem.with_marketplace_listing_plans_type
          .joins(:plan_subscription)
          .where(plan_subscriptions: {user_id: @object.user_or_org_account_ids}).active

        if arguments[:marketplace_listing_id]
          marketplace_listing = Platform::Helpers::NodeIdentification.typed_object_from_id(
            [Objects::MarketplaceListing],
            arguments[:marketplace_listing_id],
            @context,
          )

          listing_plans_ids = Marketplace::ListingPlan.where(
            marketplace_listing_id: marketplace_listing.id,
          ).pluck(:id)

          subscription_items = subscription_items.with_ids(listing_plans_ids, field: "subscribable_id")
        end

        subscription_items
      end

      field :pending_marketplace_installations, Platform::Connections.define(Objects::SubscriptionItem), visibility: :internal, description: "Subscription items for apps that are not installed", null: false do
        argument :only_notice_triggered, Boolean, "Only returns pending installations for which a notice has been triggered", required: false, default_value: false
        has_connection_arguments
      end

      def pending_marketplace_installations(only_notice_triggered: false)
        return Billing::SubscriptionItem.none unless is_viewer

        @object.pending_marketplace_installations(only_notice_triggered: only_notice_triggered)
      end

      field :two_factor_credential, Objects::TwoFactorCredential, visibility: :internal, description: "The two-factor enrollment information", null: true

      def two_factor_credential
        if is_viewer
          @object.async_two_factor_credential
        else
          nil
        end
      end

      field :user_sessions, Connections.define(Objects::UserSession), visibility: :internal, description: "The currently logged in user sessions for the viewer", null: false, connection: true do
        argument :recent, Boolean, "Limit sessions to recently used, active sessions", required: false
      end

      def user_sessions(recent: false)
        if is_viewer
          if recent
            @object.sessions.user_facing.recent
          else
            @object.sessions.user_facing
          end
        else
          ::UserSession.none
        end
      end

      field :personal_access_tokens, Connections.define(Objects::PersonalAccessToken), visibility: :internal, description: "Personal access tokens for the current viewer", null: false, connection: true

      def personal_access_tokens
        if is_viewer
          @object.oauth_authorizations.personal_tokens
        else
          ::OauthAuthorization.none
        end
      end

      field :third_party_oauth_authorizations, Connections.define(Objects::ThirdPartyOauthAuthorization), visibility: :internal, description: "3rd party OAuth Authorizations for the current viewer", null: false, connection: true

      def third_party_oauth_authorizations
        if is_viewer
          @object.oauth_authorizations.third_party
        else
          ::OauthAuthorization.none
        end
      end

      field :github_app_authorizations, Connections.define(Objects::GitHubAppAuthorization), visibility: :internal, description: "3rd party OAuth Authorizations for the current viewer", null: false, connection: true

      def github_app_authorizations
        if is_viewer
          @object.oauth_authorizations.github_apps
        else
          ::OauthAuthorization.none
        end
      end

      field(
        :mobile_device_tokens,
        Connections.define(Objects::MobileDeviceToken),
        minimum_accepted_scopes: ["site_admin"],
        visibility: :under_development,
        areas_of_responsibility: :notifications,
        null: false,
      ) do
        description <<-DESCRIPTION
          A list of mobile device tokens associated with this user.
          Returns an empty list for any user who is not a site admin.
        DESCRIPTION

        argument :service, Enums::PushNotificationService, "Only return tokens that were issued by this particular push notification service.", required: false
      end

      def mobile_device_tokens(service: nil)
        unless self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          return ArrayWrapper.new([])
        end

        ArrayWrapper.new(
          unpack_newsies_response!(
            GitHub.newsies.mobile_device_tokens(@object.id, service: service),
          ),
        )
      end

      field :mobile_push_notification_schedules, Connections.define(Objects::MobilePushNotificationSchedule), feature_flag: :pe_mobile, null: false,
        description: "A list of mobile push notification schedules associated with this user."

      def mobile_push_notification_schedules
        return ArrayWrapper.new([]) unless @object == context[:viewer]

        schedules = handle_newsies_service_unavailable do
          Newsies::MobilePushNotificationSchedule
            .where(user_id: @object.id)
            .order(day: :asc)
            .all
        end

        ArrayWrapper.new(schedules)
      end

      field :notification_threads, Connections::NotificationThread, minimum_accepted_scopes: ["notifications"], feature_flag: :pe_mobile, null: false do
        description <<~DESCRIPTION
          A list of notification threads for the viewer. Returns an empty list for a user other than
          the current viewer.

          Combining both filterBy and query arguments will result in an error.
          Please favor using the query argument.
        DESCRIPTION

        argument :filter_by, Inputs::NotificationThreadFilters, "Filtering options for notifications. Will soon be deprecated.", required: false
        argument :query, String, "The search string to look for. If no is:read, is:unread, or is:done qualifiers are included in the query, results will include read and unread notification threads by default.", required: false
      end

      def notification_threads(filter_by: nil, query: nil)
        # If _anything_ is set for both arguments, we raise.
        raise Errors::ArgumentError, "Cannot combine filterBy and query arguments." if !filter_by.nil? && !query.nil?

        return Platform::ArrayWrapper.new([]) unless context[:permission].can_list_user_notifications?(@object)

        if !query
          return Platform::Helpers::NotificationThreadsQuery.new({
            filter_by: filter_by,
            unauthorized_organization_ids: @context[:unauthorized_organization_ids],
            internal_request: Platform.safe_origin?(@context[:origin]),
          }, @context[:viewer])
        end

        parsed_query = Search::Queries::NotificationsQuery.new(query: query, viewer: @context[:viewer])
        if !parsed_query.supported_by_mysql? && GitHub.flipper[:notifications_search].enabled?(@context[:viewer])
          ::Search::Queries::NotificationSearchQuery.new(
            current_user: @context[:viewer],
            phrase: query,
            unauthorized_organization_ids: @context[:unauthorized_organization_ids],
          )
        else
          return Platform::Helpers::NotificationThreadsQuery.new({
            query: query,
            unauthorized_organization_ids: @context[:unauthorized_organization_ids],
            internal_request: Platform.safe_origin?(@context[:origin]),
          }, @context[:viewer])
        end
      end

      field :notification_filters, Connections.define(Platform::Objects::NotificationFilter), feature_flag: :pe_mobile, areas_of_responsibility: :notifications, minimum_accepted_scopes: ["notifications"], null: false do
        description <<~DESCRIPTION
          A list of notification filters for the viewer. Returns an empty list for any user
          other than the current viewer.
        DESCRIPTION
      end

      def notification_filters
        return ArrayWrapper.new([]) unless context[:permission].can_list_user_notifications?(@object)
        ArrayWrapper.new(unpack_newsies_response!(GitHub.newsies.web.all_custom_inboxes(@object)))
      end

      field :notification_settings, Objects::NotificationSettings, description: "The viewer's notification settings", feature_flag: :pe_mobile, null: true

      def notification_settings
        if @object == context[:viewer]
          @object.async_primary_user_email.then do
            unpack_newsies_response!(GitHub.newsies.settings(@object))
          end
        else
          nil
        end
      end

      field :notification_lists_with_thread_count, Connections.define(Objects::NotificationListWithThreadCount), minimum_accepted_scopes: ["notifications"], feature_flag: :pe_mobile, null: false do
        description <<~DESCRIPTION
          A list of notification lists the viewer has received a notification for. Returns an empty list for a user other than
          the current viewer.
        DESCRIPTION

        argument :statuses, [Platform::Enums::NotificationStatus], "Only return lists which have at least one notification thread with a status in this list", required: false
        argument :list_types, [Platform::Enums::NotificationThreadSubscriptionListType], "Only return lists where the list type is in the list", required: false
      end

      def notification_lists_with_thread_count(statuses: [], list_types: [])
        return ArrayWrapper.new([]) unless context[:permission].can_list_user_notifications?(@object)

        # Filter lists by notification status. Default to ALL status types by default.
        options = {
          statuses: statuses.presence,
          list_type: list_types.presence,
        }.compact

        counts_by_list = unpack_newsies_response!(GitHub.newsies.web.counts_by_list(@object, options))
                          .sort_by { |(_, _, count_unread)| -count_unread }

        readable_lists_with_thread_count_promises = counts_by_list
          .map do |(list, count_all, count_unread)|
            notification_list_with_thread_count = Platform::Models::NotificationListWithThreadCount.new(
              list: list,
              count: count_all,
              unread_count: count_unread,
              user: @object,
            )

            notification_list_with_thread_count.async_readable_by?(@context[:viewer], unauthorized_organization_ids: @context[:unauthorized_organization_ids]).then do |readable|
              next notification_list_with_thread_count if readable
            end
          end

        Promise.all(readable_lists_with_thread_count_promises).then do |readable_lists_with_thread_count|
          ArrayWrapper.new(readable_lists_with_thread_count.compact)
        end
      end

      field :allows_marketing_newsletters, Boolean, visibility: :internal, description: "Whether marketing newsletters may be sent to this user", null: false

      def allows_marketing_newsletters
        NewsletterPreference.marketing?(user: @object) || false
      end

      field :newsletter_subscription, Objects::NewsletterSubscription, visibility: :internal, description: "The user's subscription to a newsletter.", null: true do
        argument :newsletter, String, "The newsletter name.", required: true
      end

      def newsletter_subscription(newsletter:)
        @object.newsletter_subscriptions.find_by(name: newsletter)
      end

      field :unsubscribe_token, String, visibility: :internal, description: "Get the token to unsubscribe this user from a newsletter", null: false do
        argument :newsletter, String, "The newsletter name.", required: true
      end

      def unsubscribe_token(newsletter:)
        ::NewsletterSubscription.get_unsubscribe_token(@object, newsletter)
      end

      def self.load_from_params(params)
        Loaders::ActiveRecord.load(::User, params[:user_id], column: :login)
      end

      field :organization_verified_domain_emails, [String], description: "Verified email addresses that match verified domains for a specified organization the user is a member of.", null: false do
        argument :login, String, description: "The login of the organization to match verified domains from.", required: true
      end

      def organization_verified_domain_emails(**arguments)
        Loaders::ActiveRecord.load(::Organization, arguments[:login], column: :login).then do |organization|
          next [] unless organization
          next [] unless GitHub.domain_verification_enabled?
          next [] unless organization.plan_supports?(:display_verified_domain_emails)

          organization.terms_of_service.async_name.then do |tos_name|
            next [] unless [::Organization::TermsOfService::CORPORATE, ::Organization::TermsOfService::CUSTOM].include? tos_name

            organization.async_adminable_by?(context[:viewer]).then do |can_administer|
              next [] unless can_administer

              @object.async_verified_domain_emails_for(organization).then do |emails|
                emails.map(&:email)
              end
            end
          end
        end
      end

      field :toggleable_features, Connections.define(Objects::ToggleableFeature), description: "Features that the user can enroll and unenroll in.", null: true, visibility: :under_development, areas_of_responsibility: :feature_lifecycle

      def toggleable_features
        if is_viewer
          context[:viewer].available_prerelease_features
        end
      end

      field :can_create_organization_discussion, Boolean,
        "Can the user post discussions to the given organization?",
        visibility: :under_development, null: false do
          argument :login, String,
            description: "The login of the organization to check permission.", required: true
        end

      def can_create_organization_discussion(login:)
        unless GitHub.flipper[:organization_discussions].enabled?(context[:viewer])
          return false
        end

        org_promise = Loaders::ActiveRecord.load(::Organization, login, column: :login)
        org_promise.then do |org|
          if org
            org.adminable_by?(context[:viewer])
          else
            false
          end
        end
      end

      field :can_admin_marketplace_listings, Boolean, "Can the user administer Marketplace listings.", null: false, visibility: :internal, method: :can_admin_marketplace_listings?

      field :dashboard_pinned_items, resolver: Resolvers::DashboardPinnedItems,
        description: "A list of items this user has pinned to their dashboard.",
        connection: true, feature_flag: :pe_mobile

      field :dashboard_pinned_items_remaining, Int, null: false,
        description: "Returns how many more items this user can pin to their dashboard.",
        visibility: :internal

      field :mobile_time_zone, String, description: "The user's mobile time zone.", feature_flag: :pe_mobile, null: false

      def mobile_time_zone
        @object.async_mobile_time_zone.then(&:name)
      end

      field :advisory_credits, Connections.define(Objects::AdvisoryCredit), "Credits given to the user for collaborating on security advisories", null: false, areas_of_responsibility: :security_advisories, visibility: :under_development do
        argument :order_by, Inputs::AdvisoryCreditOrder, "Ordering options for the returned credits.", required: false, default_value: { field: "id" }
      end

      def advisory_credits(order_by:)
        object.advisory_credits.accepted.on_public_repository_advisories
      end

      private

      def validate_date_range(date_range, max_interval:, message:)
        start_date = date_range.first
        end_date = date_range.last
        date_interval = (end_date - start_date).abs

        unless date_interval / max_interval <= 1
          raise Errors::Validation, message
        end
      end

      def build_and_validate_contributions_time_range(from: nil, to: nil)
        # We want to return nil in this case so Loaders::ContributionCollector can apply defaults
        # and thereby share instances of the Collector when `from` and `to` are not passed in the
        # query.
        return unless from || to

        time_range = if from && to
          from..to
        elsif from
          from..(from + 1.year)
        elsif to
          (to - 1.year)..to
        end

        # 371 days for Calendar's range of: (to - 1.year).beginning_of_week(:sunday)
        # Then add a day for leap years
        validate_date_range(time_range, max_interval: 372.days,
          message: "The total time spanned by 'from' and 'to' must not exceed 1 year")

        time_range
      end
    end
  end
end
