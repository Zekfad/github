# frozen_string_literal: true

module Platform
  module Objects
    class StaffAccessedRepository < Platform::Objects::Base
      implements Platform::Interfaces::RepositoryInfo
      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      description <<~MD
        This object may be fetched by GitHub staff, and it
        shows data about a repository, even if the repo is private.
        `Platform::Interfaces::RepositoryInfo` serves as a whitelist of fields
        which are visible to staff users, along with any extra fields added here.
      MD

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        if permission.origin == ORIGIN_INTERNAL || permission.origin == ORIGIN_MANUAL_EXECUTION
          true
        elsif Platform::Objects::Base::SiteAdminCheck.viewer_is_site_admin?(permission.viewer, self.class.name)
          # Let's not serve this private repo data over the API if we can avoid it.
          # Raise an error instead of returning `nil` so that the client can debug
          raise Errors::Execution.new("API", "StaffAccessedRepository isn't available via the GraphQL API")
        else
          raise Errors::Internal, "Invariant: Somehow a non-staff user tried to access `staffAccessedRepository`, this should be impossible."
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        Platform::Objects::Base::SiteAdminCheck.viewer_is_site_admin?(permission.viewer, self.class.name) &&
          permission.typed_can_see?("Repository", object)
      end

      field :id, ID, null: false, method: :global_relay_id

      field :stafftools_info, Objects::RepositoryStafftoolsInfo, null: true,
        description: "Fields that are only visible to site admins."

      def stafftools_info
        if self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          Models::RepositoryStafftoolsInfo.new(object)
        else
          nil
        end
      end
    end
  end
end
