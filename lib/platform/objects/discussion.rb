# frozen_string_literal: true

module Platform
  module Objects
    class Discussion < Platform::Objects::Base
      description "A discussion in a repository."
      areas_of_responsibility :discussions
      visibility :under_development

      scopeless_tokens_as_minimum
      feature_flag :discussions

      implements Platform::Interfaces::Node
      implements Interfaces::Updatable
      implements Interfaces::Deletable
      implements Interfaces::RepositoryNode
      implements Interfaces::Subscribable
      implements Interfaces::AbuseReportable
      implements Interfaces::Blockable
      implements Interfaces::Reactable

      global_id_field :id
      database_id_field

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, discussion)
        permission.async_repo_and_org_owner(discussion).then do |repo, org|
          permission.access_allowed?(
            :show_discussion,
            resource: discussion,
            repo: repo,
            current_org: org,
            allow_integrations: true,
            allow_user_via_integration: false,
          )
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, discussion)
        discussion.async_readable_by?(permission.viewer)
      end

      url_fields description: "The URL for this discussion." do |discussion|
        discussion.async_path_uri
      end

      field :number, Integer, "The number identifying this discussion within the repository.",
        null: false
      field :title, String, "The title of this discussion.", null: false
      field :body, String, description: "The main text of the discussion post.", null: false
      field :user, Objects::User, description: "The user that started the discussion.",
        null: true, method: :async_user
      field :chosen_comment, Objects::DiscussionComment,
        description: "The comment chosen as this discussion's answer, if any.", null: true,
        method: :async_chosen_comment
      field :category, Objects::DiscussionCategory,
        description: "The category for this discussion, if any.", null: true,
        method: :async_category

      created_at_field
      updated_at_field

      field(:comments, Connections.define(Objects::DiscussionComment),
        "The replies to the discussion.", null: false
      ) do
        argument :order_by, Inputs::DiscussionCommentOrder,
          "Ordering options for discussion comments returned from the connection.", required: false,
          default_value: { field: "created_at", direction: "ASC" }
      end

      def comments(order_by: nil)
        scope = @object.comments.filter_spam_for(@context[:viewer])

        if order_by
          field = order_by[:field]
          direction = order_by[:direction]
          scope = scope.reorder("discussion_comments.#{field} #{direction}")
        end

        scope
      end

      def body
        @object.body || ""
      end
    end
  end
end
