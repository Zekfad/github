# frozen_string_literal: true
module Platform
  module Objects
    class SurveySummarizedAnswer < Platform::Objects::Base
      description "A percentage of answers for a choice on a survey question"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer && permission.viewer.site_admin?
      end

      minimum_accepted_scopes ["devtools"]
      visibility :internal

      field :text, String, "Text of the choice", null: false
      field :percent, Integer, "Percentage of selections for said choice", null: false
    end
  end
end
