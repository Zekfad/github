# frozen_string_literal: true

module Platform
  module Objects
    class RepositoryAdvisory < Platform::Objects::Base
      description "A maintainer advisory for dependents of a repository"
      areas_of_responsibility :security_advisories
      minimum_accepted_scopes ["public_repo"]

      feature_flag :pe_mobile

      implements Platform::Interfaces::Node
      implements Interfaces::Comment
      implements Interfaces::MayBeInternal
      implements Interfaces::Reactable
      implements Interfaces::UniformResourceLocatable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, security_advisory)
        # this check is only here for SAML enforcement (side
        # effect of `access_allowed` via `current_org`).
        permission.async_repo_and_org_owner(security_advisory).then do |repo, org|
          permission.access_allowed?(
            :saml_via_graphql, # returns false
            resource: security_advisory,
            current_org: org,
            current_repo: repo,
            allow_integrations: false,
            allow_user_via_integration: false)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.load_repo_and_owner(object).then do
          object.readable_by?(permission.viewer)
        end
      end

      def self.load_from_global_id(ghsa_id)
        Loaders::ActiveRecord.load(::RepositoryAdvisory, ghsa_id, column: :ghsa_id)
      end

      global_id_field :id
      database_id_field

      url_fields description: "The URL for this advisory" do |advisory|
        advisory.async_path_uri
      end

      field :ghsa_id, String, "The GitHub Security Advisory ID", null: false
      field :title, String, "The plaintext title of the advisory", null: false
      field :description, String, "A plaintext description of the advisory", null: false
      field :comments, Connections.define(Objects::RepositoryAdvisoryComment), "The advisory discussion comments", null: false

      field :state, Platform::Enums::RepositoryAdvisoryState, "The current state of the advisory", null: false, visibility: :internal

      # Interfaces::Comment

      # It's not possible to create the initial "comment" of a repository
      # advisory via email.
      def created_via_email
        false
      end

      # Interfaces::MayBeInternal

      # The initial repository advisory body is always written before
      # advisory publication, so it should always be internal.
      def is_internal
        true
      end
    end
  end
end
