# frozen_string_literal: true

module Platform
  module Objects
    class PackageFileUploadInfo < Platform::Objects::Base
      description "Information required to upload content to a package file."

      visibility :internal
      minimum_accepted_scopes ["write:packages"]

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      field :upload_url, Scalars::URI, "Identifies the upload URL for the file.", null: true
      field :form_fields, [FormHeader], "Identifies the form fields to send to the upload server.", null: true
      field :headers, [FormHeader], "Identifies the headers to send to the upload server.", null: true
    end
  end
end
