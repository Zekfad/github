# frozen_string_literal: true

module Platform
  module Objects
    class Reaction < Platform::Objects::Base
      description "An emoji reaction to a particular piece of content."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, reaction)
        reaction.async_subject.then do |reaction_subject|
          permission.rewrite_reaction_subject(reaction_subject).then do |subject_type, subject|
            permission.typed_can_access?(subject_type, subject)
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_user.then { |user|
          if user && user.hide_from_user?(permission.viewer)
            false
          else
            object.async_subject.then { |subject|
              permission.rewrite_reaction_subject(subject).then do |subject_type, subject|
                permission.typed_can_see?(subject_type, subject)
              end
            }
          end
        }
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      global_id_field :id

      field :content, Enums::ReactionContent, "Identifies the emoji reaction.", null: false

      created_at_field

      database_id_field

      field :reactable, Interfaces::Reactable, description: "The reactable piece of content", null: false

      def reactable
        @object.async_subject.then do |subject|
          if subject.try(:pull_request_id).present?
            subject.async_pull_request
          else
            subject
          end
        end
      end

      field :user, User, method: :async_user, description: "Identifies the user who created this reaction.", null: true
    end
  end
end
