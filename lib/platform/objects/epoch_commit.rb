# frozen_string_literal: true

module Platform
  module Objects
    class EpochCommit < Objects::Base
      description "A commit that occurs as a result of the operations contained within an epoch"

      scopeless_tokens_as_minimum

      global_id_field :id

      feature_flag :epochs
      areas_of_responsibility :epochs

      def self.async_api_can_access?(permission, epoch_commit)
        epoch_commit.async_epoch.then do |epoch|
          permission.async_repo_and_org_owner(epoch).then do |repo, org|
            permission.access_allowed?(:get_ref, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      def self.async_viewer_can_see?(permission, epoch_commit)
        epoch_commit.async_epoch.then do |epoch|
          permission.belongs_to_git_object(epoch)
        end
      end

      field :epoch, Objects::Epoch, "The epoch for this commit", null: true
      field :repository, Repository, "The repository the epoch belongs to", null: true
      field :commit_oid, String, "The commit oid that is the child of this epoch.", null: false
      field :created_at, Scalars::DateTime, "When the commit was created", null: false
    end
  end
end
