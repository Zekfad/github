# frozen_string_literal: true

module Platform
  module Objects
    class MarketplaceStory < Platform::Objects::Base
      areas_of_responsibility :marketplace

      model_name "Marketplace::Story"
      description "A Marketplace-related story, such as a blog post."

      scopeless_tokens_as_minimum
      visibility :internal

      implements Platform::Interfaces::Node

      def self.async_viewer_can_see?(_permission, _object)
        true
      end

      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self) # Update this authorization if we ever go public with this object
      end

      global_id_field :id

      database_id_field(visibility: :internal)

      field :title, String, "The title of the story", null: false
      field :url, Scalars::URI, "URL to the content", null: false
      field :author, String, "The author of the story", null: false
      field :description, String, "A brief description of the story", null: false
      field :featured, Boolean, "Whether or not the story should show up on the Marketplace homepage", null: false
      field :published_at, Scalars::DateTime, "When the story was published", null: false
    end
  end
end
