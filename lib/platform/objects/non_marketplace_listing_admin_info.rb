# frozen_string_literal: true

module Platform
  module Objects
    class NonMarketplaceListingAdminInfo < Platform::Objects::Base
      description "Works with GitHub listing information only visible to site administrators."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer.can_admin_non_marketplace_listings?
      end

      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      field :state, String, description: "The listing's current state.", null: false

      def state
        @object.listing.current_state.name
      end

      field :integration_listing, Platform::Objects::IntegrationListing, description: "The Integrations listing this Works with GitHub listing originally came from.", null: true

      def integration_listing
        if id = @object.listing.integration_listing_id
          Loaders::ActiveRecord.load(::IntegrationListing, id)
        end
      end
    end
  end
end
