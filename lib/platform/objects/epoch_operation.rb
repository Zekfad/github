# frozen_string_literal: true

module Platform
  module Objects
    class EpochOperation < Objects::Base
      description "A single operation performed by an author during an epoch"

      scopeless_tokens_as_minimum

      global_id_field :id

      feature_flag :epochs
      areas_of_responsibility :epochs

      def self.async_api_can_access?(permission, epoch_operation)
        epoch_operation.async_epoch.then do |epoch|
          permission.async_repo_and_org_owner(epoch).then do |repo, org|
            permission.access_allowed?(:get_ref, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      def self.async_viewer_can_see?(permission, epoch_operation)
        epoch_operation.async_epoch.then do |epoch|
          permission.belongs_to_git_object(epoch)
        end
      end

      field :epoch, Objects::Epoch, "The epoch for this operation", null: true
      field :operation, String, "The operation which encodes the change including buffer deletions, additions and changes.", null: false
      field :author, Objects::User, "The author of the operation", method: :async_author, null: true
      field :created_at, Scalars::DateTime, "When the operation was created", null: false
    end
  end
end
