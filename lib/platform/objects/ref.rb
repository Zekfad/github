# frozen_string_literal: true

module Platform
  module Objects
    class Ref < Objects::Base
      description "Represents a Git reference."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, ref)
        repo = ref.repository
        Promise.all([repo.async_owner, repo.async_parent]).then do |owner, _|
          org = owner.organization? ? owner : nil
          permission.access_allowed?(:get_ref, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_git_object(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      global_id_field :id

      field :repository, Repository, "The repository the ref belongs to.", null: false
      field :name, String, "The ref name.", null: false
      def name
        @object.name.force_encoding("UTF-8")
      end

      field :prefix, String, "The ref's prefix, such as `refs/heads/` or `refs/tags/`.", null: false

      field :target, Interfaces::GitObject, "The object the ref points to. Returns null when object does not exist.", null: true

      def target
        Loaders::GitObject.load(@object.repository, @object.target_oid)
      end

      field :directory, Objects::CommittishDirectory, "Look up directory under the commit tree", null: true do
        argument :path, String, "The file path", required: false
      end

      def directory(path: nil)
        revision.async_load_directory(path: path)
      end

      field :file, Objects::CommittishFile, "Look up file under the commit tree.", null: true do
        argument :path, String, "The file path.", required: true
      end

      def file(path:)
        revision.async_load_file(path: path)
      end

      field :associatedPullRequests, resolver: Resolvers::RefPullRequests, description: "A list of pull requests with this ref as the head ref.", null: false

      field :files, Connections.define(Objects::CommittishFile), "Look up a list of files under the commit tree.", null: false do
        argument :paths, [String], "An array of paths in the repository like \"README.md\", \"test.rb\", ...", required: true
      end

      def files(paths:)
        ref = @object
        normalized_paths = paths.map { |p| p.sub(/\A\//, "") }

        files = normalized_paths.map do |path|
          next unless tree_entry = ref.repository.tree_entry(ref.target_oid, path)
          Models::CommittishFile.new(revision, tree_entry, path, ref.target_oid)
        end

        ArrayWrapper.new(files.compact)
      end

      def revision
        @revision ||= Models::CommitRevision.new(@object.repository, @object.name)
      end

      field :epochs, Connections.define(Objects::Epoch), "List of epochs associated with a commit tree.", null: true, feature_flag: :epochs do
        argument :base_commit_oid, String, "Filter by base commit.", required: true
      end

      def epochs(base_commit_oid:)
        @epochs ||= ::Epoch.where(repository: @object.repository, ref: @object.qualified_name, base_commit_oid: base_commit_oid)
      end

      def self.load_from_global_id(id)
        repo_id, name = id.split(":", 2)

        Loaders::ActiveRecord.load(::Repository, repo_id.to_i, security_violation_behaviour: :nil).then do |repo|
          if repo
            repo.async_network.then { repo.refs.find(name) }
          end
        end
      end

      field :isWorkflowFilePresent, Boolean, visibility: :internal, description: "Does a workflow file exist on the ref?", null: false

      def is_workflow_file_present
        object.repository.workflow_file_present?(@object.name)
      end

      field :branch_protection_rule, Objects::BranchProtectionRule, feature_flag: :pe_mobile, description: "Branch protection rules for this ref", null: true

      def branch_protection_rule
        return nil unless @object.protected_branch
        Platform::Models::BranchProtectionRule.new(@object.protected_branch)
      end

      field :ref_update_rule, Objects::RefUpdateRule,
        feature_flag: :pe_mobile,
        description: "Branch protection rules that are viewable by non-admins",
        method: :protected_branch,
        null: true
    end
  end
end
