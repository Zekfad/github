# frozen_string_literal: true

module Platform
  module Objects
    class App < Platform::Objects::Base
      model_name "Integration"
      description "A GitHub App."

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      # Ability check for an Integration object
      def self.async_viewer_can_see?(permission, object)
        object.async_readable_by?(permission.viewer).then do |is_readable|
          next true if is_readable
          next false unless permission.viewer

          # App is listed in the Marketplace and the user can manage Marketplace listings
          object.async_marketplace_listing.then do |marketplace_listing|
            next false unless marketplace_listing
            permission.viewer.can_admin_marketplace_listings?
          end
        end
      end

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, integration)
        integration.async_owner.then do |owner|
          org = owner.is_a?(::Organization) ? owner : nil
          permission.access_allowed?(:github_app_viewer,
            resource: integration,
            current_org: org,
            current_repo: nil,
            allow_integrations: true,
            allow_user_via_integration: true,
            installation_required: false,
            # In order to keep the nice messaging that GraphQL has
            # provided we are enabling or disabling OAP here
            # instead of turning it off entirely and leaning on
            # the access control.
            enforce_oauth_app_policy: integration.internal?,
          )
        end
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      implements Interfaces::FeatureActor
      implements Interfaces::FeatureFlaggable

      implements Interfaces::MarketplaceIntegratable

      global_id_field :id

      created_at_field

      updated_at_field

      database_id_field

      field :name, String, "The name of the app.", null: false

      field :description, String, "The description of the app.", null: true

      field :description_html, Scalars::HTML, description: "The app's description rendered to HTML.", visibility: :internal, null: true do
        argument :truncate, Integer, "Truncate the description at the specified length", required: false
      end

      def description_html(truncate: nil)
        Platform::Helpers::AppContent.html_for(
          @object, :description, { current_user: @context[:viewer] }, truncate: truncate
        )
      end

      field :short_description_html, Scalars::HTML, description: "A shortened version of the app's description rendered to HTML.", visibility: :internal, null: true do
        argument :truncate, Integer, "Truncate the description at the specified length", required: true
      end

      def short_description_html(truncate:)
        # Try to break at the end of the first sentence
        sentence_break = @object.description.index(/\.\s+/)
        if sentence_break.present? && sentence_break <= truncate
          short_description = @object.description[0..sentence_break]
          truncate = nil
        else
          short_description = @object.description
        end

        rendered_description = Platform::Helpers::AppContent.html_for(
          @object, :short_description, { current_user: @context[:viewer] }, truncate: truncate, value: short_description
        )
      end

      field :slug, String, "A slug based on the name of the app for use in URLs.", null: false

      field :url, Scalars::URI, "The URL to the app's homepage.", null: false

      field :marketplace_listing, Objects::MarketplaceListing, method: :async_marketplace_listing,
        description: "The Marketplace Listing for this App", null: true, visibility: :under_development

      field :logo_url, Scalars::URI, description: "A URL pointing to the app's logo.", null: false do
        argument :size, Integer, "The size of the resulting image.", required: false
      end

      def logo_url(size: nil)
        @object.async_owner.then { @object.preferred_avatar_url(size: size) }
      end

      field :logo_background_color, String, "The hex color code, without the leading '#', for the logo background.", method: :bgcolor, null: false

      field :owner, Unions::AppOwner, method: :async_owner, description: "The owner of this app.", null: false

      url_fields description: "The HTTP URL for this app's stafftools page", prefix: "stafftools", visibility: :internal do |app|
        app.async_owner.then do |owner|
          template = Addressable::Template.new("/stafftools/users/{owner}/apps/{slug}")
          template.expand owner: owner.login, slug: app.slug
        end
      end

      url_fields description: "The HTTP URL for this app", prefix: "html", visibility: :internal do |app|
        app.async_bot.then do |bot|
          Addressable::URI.parse("/#{bot.to_param}")
        end
      end

      field :installed_repositories, Connections.define(Objects::Repository), visibility: :internal, description: "The repositories this app is installed on for a specified account", null: false do
        argument :account, String, "The target account's login.", required: true
      end

      field :default_permissions, [Objects::AppPermission], description: "The permissions requested by this app on installation", visibility: :under_development, null: false

      def default_permissions
        @object.async_default_permissions.then do |default_permissions|
          default_permissions.to_a.map do |resource, access|
            { resource: resource, access: access.to_s }
          end
        end
      end

      field :default_events, [String], method: :async_default_events, description: "The webhook events this app subscribes to", visibility: :under_development, null: false

      def installed_repositories(**arguments)
        Platform::Loaders::ActiveRecord.load(::User, arguments[:account], column: :login, case_sensitive: false).then do |account|

          return ArrayWrapper.new([]) unless account

          # If we are ever going to include installations for apps that are
          # per-repo install required, we'll need to update this to count the
          # repositories associated to _all_ installations instead of just the
          # one
          installation = ::IntegrationInstallation.find_by(integration_id: @object.id, target_id: account.id, target_type: account.class.base_class)

          return ArrayWrapper.new([]) unless installation

          installation.repositories.filter_spam_and_disabled_for(context[:viewer])
        end
      end
    end
  end
end
