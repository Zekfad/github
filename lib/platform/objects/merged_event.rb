# frozen_string_literal: true

module Platform
  module Objects
    class MergedEvent < Platform::Objects::Base
      model_name "IssueEvent"
      description "Represents a 'merged' event on a given pull request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, merged_event)
        permission.belongs_to_issue_event(merged_event)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_issue(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      implements Interfaces::TimelineEvent

      implements Interfaces::PerformableViaApp

      implements Interfaces::UniformResourceLocatable

      global_id_field :id

      field :pull_request, Objects::PullRequest, "PullRequest referenced by event.", method: :async_issue_or_pull_request, null: false

      field :commit, Objects::Commit, description: "Identifies the commit associated with the `merge` event.", null: true

      def commit
        @object.async_repository.then { |repository|
          Loaders::GitObject.load(repository, @object.commit_id, expected_type: :commit)
        }
      end

      field :merge_ref_name, String, description: "Identifies the name of the Ref associated with the `merge` event.", null: false

      def merge_ref_name
        @object.async_issue.then { |issue|
          issue.async_pull_request.then { |pull_request|
            pull_request.base_ref.force_encoding("utf-8")
          }
        }
      end

      field :merge_ref, Ref, description: "Identifies the Ref associated with the `merge` event.", null: true

      def merge_ref
        pull_request_promise = Loaders::ActiveRecord.load(::Issue, @object.issue_id).then { |issue|
          Loaders::ActiveRecord.load(::PullRequest, issue.pull_request_id)
        }

        repository_promise = Loaders::ActiveRecord.load(::Repository, @object.repository_id)

        Promise.all([pull_request_promise, repository_promise]).then { |(pull_request, repository)|
          repository.async_network.then do
            repository.heads.find(pull_request.base_ref)
          end
        }
      end

      # TODO leave this as internal until PullRequest#async_pushable_repo_for is fully batched.
      field :viewer_can_revert, Boolean, visibility: :internal, description: "Check if the current user can revert this merge commit.", null: false

      def viewer_can_revert
        @object.async_revertable_by?(@context[:viewer])
      end

      field :show_merge_tip, Boolean, visibility: :internal, description: "Check if the current user needs to be shown the merge tip in the PR timeline.", null: false

      def show_merge_tip
        @object.async_show_merge_tip_for?(@context[:viewer])
      end

      url_fields description: "The HTTP URL for this merged event." do |event|
        event.async_path_uri
      end
    end
  end
end
