# frozen_string_literal: true
module Platform
  module Objects
    class Base < GraphQL::Schema::Object
      module Visibility
        ALL_ENVIRONMENTS = GraphQLExtensions::VisibilityForBuiltIns::ALL_ENVIRONMENTS
        DEFAULT_VISIBILITIES = GraphQLExtensions::VisibilityForBuiltIns::DEFAULT_VISIBILITIES
        DEFAULT_ENVIRONMENT_VISIBILITIES = GraphQLExtensions::VisibilityForBuiltIns::DEFAULT_ENVIRONMENT_VISIBILITIES

        def generate_input_type
          apply_visibility_to_class(super)
        end

        def generate_payload_type
          apply_visibility_to_class(super)
        end

        def visibility(desired_visibility = nil, environments: ALL_ENVIRONMENTS)
          return if desired_visibility == []
          desired_visibility = Array(desired_visibility)

          if desired_visibility.present?
            @visibilities ||= {}
            environments.each do |env|
              @visibilities[env] = desired_visibility.dup

              if desired_visibility.include?(:public) || desired_visibility.include?(:under_development)
                @visibilities[env] << :internal
              end

              @visibilities[env].flatten!
              @visibilities[env].uniq!
            end

            # assume that the schema member is not visible to any environment not mentioned
            other_environments = ALL_ENVIRONMENTS - environments
            other_environments.each do |env|
              @visibilities[env] ||= []
            end
          else
            visibility_for(GitHub.runtime.current.to_sym)
          end
        end

        def environment_visibilities
          @visibilities || DEFAULT_ENVIRONMENT_VISIBILITIES
        end

        def visibility_for(env)
          (@visibilities && @visibilities[env]) || DEFAULT_VISIBILITIES
        end

        def default_visibility?
          environment_visibilities == DEFAULT_ENVIRONMENT_VISIBILITIES
        end

        private

        def visibility_from_config(visibility_config)
          case visibility_config
          when nil
            # Do nothing
          when Hash
            visibility_config.each do |vis_level, options|
              visibility(vis_level, **options)
            end
          when Symbol
            visibility(visibility_config)
          else
            raise ArgumentError, "Visibility must be Symbol or Hash, not: #{visibility_config.inspect}"  # rubocop:disable GitHub/UsePlatformErrors
          end
        end

        def apply_visibility_to_class(type_class)
          if @visibilities
            @visibilities.each do |env, config|
              type_class.visibility(config, environments: [env])
            end
          end
          type_class
        end
      end
    end
  end
end
