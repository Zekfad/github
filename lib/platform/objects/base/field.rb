# frozen_string_literal: true

module Platform
  module Objects
    class Base < GraphQL::Schema::Object
      class Field < GraphQL::Schema::Field
        module ManualConnectionArguments
          # A helper for cases when we use `connection: false`
          # to bypass GraphQL-Ruby's built-in connection wrappers
          def has_connection_arguments
            # This matches https://github.com/rmosolgo/graphql-ruby/blob/1.9-dev/lib/graphql/schema/field/connection_extension.rb
            argument :after, "String", "Returns the elements in the list that come after the specified cursor.", required: false
            argument :before, "String", "Returns the elements in the list that come before the specified cursor.", required: false
            argument :first, "Int", "Returns the first _n_ elements from the list.", required: false
            argument :last, "Int", "Returns the last _n_ elements from the list.", required: false
          end
        end
        include ManualConnectionArguments
        include Platform::Objects::Base::Deprecated
        include Platform::Objects::Base::MarkForUpcomingTypeChange
        include Platform::Objects::Base::AreasOfResponsibility
        include Platform::Objects::Base::Visibility
        include Platform::Objects::Base::Previewable

        argument_class Platform::Objects::Base::Argument

        # In local environments, make sure AR::Relations have been filtered for spam and disabled repositories
        if Rails.env.test? || Rails.env.development?
          prepend(Platform::SpamFilterCheck::ResolveWrapper)
          prepend(Platform::DisabledRepositoryFilterCheck::ResolveWrapper)
        end

        def initialize(**kwargs, &block)
          @edge_class = kwargs.delete(:edge_class)
          @numeric_pagination_enabled = kwargs.delete(:numeric_pagination_enabled)
          @minimum_accepted_scopes = kwargs.delete(:minimum_accepted_scopes)
          @feature_flag = kwargs.delete(:feature_flag)
          @exempt_from_spam_filter_check = kwargs.delete(:exempt_from_spam_filter_check)
          @batch_load_configuration = kwargs.delete(:batch_load_configuration)
          visibility_from_config(kwargs.delete(:visibility))

          if aor = kwargs.delete(:areas_of_responsibility)
            areas_of_responsibility(aor)
          end

          super(**kwargs, &block)

          # Capitalize HTML as an acronym
          if kwargs[:name].to_s =~ /_html/
            @name = @name.sub("Html", "HTML")
          end

          if @numeric_pagination_enabled
            argument(:numeric_page, Integer, required: false,
              description: "Paginate by numeric page for API v3",
              visibility: :internal
            )
          end

          if connection?
            pagination_extension = CheckPaginationExtension.new(field: self, options: nil)
            # Insert this _before_ the connection tooling, so that we have access to pagination args
            @extensions.insert(1, pagination_extension)
          end
        end

        # @return [nil, Array<String>] If present, the required OAuth scopes for accessing this field
        attr_reader :minimum_accepted_scopes

        # @return [nil, Symbol] If present, an HTTP header which must be present to access this field
        attr_accessor :feature_flag

        # @return [nil, true] If true, this field's ActiveRecord::Relation's _won't_ be checked in test/development
        #  to see whether or not they have spam filtering applied. (Use this for lists which _should_ include spammy items.)
        attr_reader :exempt_from_spam_filter_check

        # This is called _during_ `#resolve_field`, but only
        # after authorization has been run.
        def public_send_field(obj, args, ctx)
          if @batch_load_configuration
            Loaders::Configuration.load(obj.object, method_sym)
          else
            super
          end
        end
      end
    end
  end
end
