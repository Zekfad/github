# frozen_string_literal: true

module Platform
  module Objects
    class FeatureActorFlipperId < Platform::Objects::Base
      description <<~MD
        Represents an entity that's opted into a feature flag. While
        we show additional information about some actors (such as Repository
        visibility and User avatars), we also support the ability to just
        opt something in to a feature with its flipper ID.
      MD

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, *)
        permission.viewer && (permission.viewer.site_admin? || permission.viewer.github_developer?)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node
      implements Interfaces::FeatureActor

      global_id_field :id

      field :flipper_id, String, description: "The flipper id opted in", null: false

      def self.load_from_global_id(id)
        Promise.resolve(Models::FeatureActorFlipperId.new(id))
      end
    end
  end
end
