# frozen_string_literal: true

module Platform
  module Objects
    class DiffLine < Platform::Objects::Base
      description "Represents a line of a diff between two commits objects."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      feature_flag :pe_mobile

      minimum_accepted_scopes ["repo"]

      field :type, Enums::DiffLineType, description: "Type of this line.", null: false

      field :text, String, description: "Plain text contents of this line.", null: false

      field :html, String, description: "HTML formatted contents of this line.", null: false

      field :position, Integer, description: "Position of this line in the diff.", null: false

      field :left, Integer, description: "Left side line number.", null: true

      field :right, Integer, description: "Right side line number.", null: true

      field :is_missing_newline_at_end, Boolean, description: "Indicates whether this line is missing a newline character at the end.", method: :no_newline_at_end, null: false
    end
  end
end
