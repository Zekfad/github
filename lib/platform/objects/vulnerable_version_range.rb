# frozen_string_literal: true

module Platform
  module Objects
    class VulnerableVersionRange < Platform::Objects::Base
      description "An affected version range for a given vulnerability"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        permission.hidden_from_public?(self) # Update this authorization if we ever go public with this object
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      areas_of_responsibility :security_advisories

      database_id_field

      field :affects, String, "The affected package", null: false

      field :ecosystem, String, "The package manager of the package", null: true

      field :requirements, String, "The serialized range of affected versions", null: false

      field :fixed_in, String, "The fixed version", null: true

      field :currently_processing_alerts, Boolean, "Whether a job is currently running", method: :currently_processing_alerts?, null: true

      field :total_alerts_processed, Integer, "How many alerts have been created by the last job", null: true

      field :estimated_affected_repository_count, Integer, "The estimated number of repositories with dependencies within the range", null: true

      def estimated_affected_repository_count
        Loaders::Dependencies.load_estimated_dependent_repository_count({
          dependents_filter: {
            package_manager: @object.ecosystem,
            package_name:    @object.affects,
            requirements:    @object.requirements,
            # This query is only available to site admins so there is no
            # need to restrict the ability to see an estimated affected
            # repository count, even if the vulnerability's platform is
            # still behind preview.
            preview:         true,
          },
        })
      end
    end
  end
end
