# frozen_string_literal: true

module Platform
  module Objects
    class DependencyGraphUnmappedPackage < Platform::Objects::Base
      description "An unmapped package"

      areas_of_responsibility :dependency_graph

      visibility :internal

      field :package_id, String, "The internal dependency graph ID", method: :id, null: true

      field :name, String, "The package name", null: true

      field :package_manager, String, "The package manager enum value", null: true

      field :package_manager_human_name, String, "The human name of the package manager", null: true
    end
  end
end
