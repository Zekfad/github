# frozen_string_literal: true

module Platform
  module Objects
    class ValidationError < Platform::Objects::Base
      description "An error due to invalid inputs to a mutation."
      areas_of_responsibility :ecosystem_api

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(_permission, _object)
        # No special API permissions
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, obj)
        true
      end

      implements Platform::Interfaces::UserError
      scopeless_tokens_as_minimum

      # All users can see validation errors
      def self.authorized?(*)
        true
      end
    end
  end
end
