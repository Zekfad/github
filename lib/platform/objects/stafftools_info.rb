# frozen_string_literal: true

module Platform
  module Objects
    class StafftoolsInfo < Platform::Objects::Base
      description "Stafftools information for site admins."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        Platform::Objects::Base::SiteAdminCheck.viewer_is_site_admin?(permission.viewer, self.class.name)
      end

      # TODO before going public, fix `camelize: false` arguments below to be camelized
      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      field :accounts, Connections.define(Unions::Account), description: "Lookup user, organization, and bot accounts, oldest to newest.", null: true, connection: true

      def accounts
        ::User.oldest_to_newest
      end

      field :accounts_for_logins, [Unions::Account], description: "Lookup user, organization, and bot accounts by login.", null: true do
        argument :logins, [String], "The logins of the users, organizations, or bots. Limit 100.", required: true
      end

      def accounts_for_logins(logins:)
        raise Errors::Validation, "Only up to 100 logins is supported." if logins.size > 100

        ::User.with_logins(logins).limit(100)
      end

      field :accounts_from_last_ip, Connections.define(Unions::Account), description: "Lookup user, organization, and bot accounts from last IP.", null: true, connection: true do
        argument :ip, String, "IPv4 network address.", required: true
        argument :prefix, Enums::NetworkPrefix, Enums::NetworkPrefix.description, default_value: 32, required: false
      end

      def accounts_from_last_ip(**arguments)
        ::User.by_ip_with_prefix(arguments[:ip], prefix: arguments[:prefix])
      end

      field :enterprises, Connections.define(Objects::Enterprise), visibility: :internal, description: "Fetch all enterprise accounts.", null: false, connection: true

      def enterprises
        ::Business.by_slug
      end

      field :account_from_database_id, Unions::Account, description: "Account from database id.", null: true do
        argument :database_id, Integer, "Account database id.", required: true
      end

      def account_from_database_id(**arguments)
        Loaders::ActiveRecord.load(::User, arguments[:database_id])
      end

      field :pending_vulnerabilities, Connections.define(Objects::PendingVulnerability), max_page_size: 250, visibility: :internal, description: "Fetch all pending vulnerabilities", null: false, connection: true do
        argument :ecosystem, Enums::VulnerabilityPlatform, "Ecosystem of the pending vulnerabilities to load", required: false
        argument :status, Enums::PendingVulnerabilityStatus, "Status of the pending vulnerabilities to load", required: false
        argument :severities, [Enums::VulnerabilitySeverity, null: true], "Severities of the pending vulnerabilities to load", required: false
        argument :identifier_query, String, "Partial identifier of the pending vulnerabilities to load", required: false, camelize: false
        argument :affects_query, String, "Partial package name of the pending vulnerabilities to load", required: false, camelize: false
        argument :order_by, Inputs::VulnerabilityOrder, "Ordering options for the returned vulnerabilities.", required: false
        argument :simulation, Boolean, "Return only internal-test Pending Vulnerabilities", required: false
        argument :source_query, String, "Source of the pending vulnerabilities", required: false, camelize: false
      end

      def pending_vulnerabilities(**args)
        vulns = ::PendingVulnerability.order("pending_vulnerabilities.created_at desc")
        vulns = vulns.external_identifier_like(args[:identifier_query]) if args[:identifier_query]
        vulns = vulns.has_ecosystem(args[:ecosystem]) if args[:ecosystem]
        vulns = vulns.severity(args[:severities]) if args[:severities]
        vulns = vulns.status(args[:status]) if args[:status]
        vulns = vulns.affects_like(args[:affects_query]) if args[:affects_query]
        vulns = vulns.where(simulation: true) if args[:simulation]
        vulns = vulns.source_like(args[:source_query]) if args[:source_query]

        if args[:order_by]
          field = args[:order_by][:field]
          direction = args[:order_by][:direction]
          vulns = vulns.reorder("pending_vulnerabilities.#{field} #{direction}")
        end

        ArrayWrapper.new(vulns)
      end

      field :pending_vulnerability, Objects::PendingVulnerability, visibility: :internal, description: "Fetch a pending vulnerability by internal database ID", null: false do
        argument :database_id, Integer, "Pending vulnerability database id.", required: true
      end

      def pending_vulnerability(**arguments)
        ::PendingVulnerability.find(arguments[:database_id])
      end

      field :pending_vulnerable_version_range, Objects::PendingVulnerableVersionRange, visibility: :internal, description: "Fetch a pending vulnerable version range by ID", null: false do
        argument :database_id, Integer, "Pending Vulnerable version range database id.", required: true
      end

      def pending_vulnerable_version_range(**arguments)
        ::PendingVulnerableVersionRange.find(arguments[:database_id])
      end

      field :vulnerabilities, Connections.define(Objects::Vulnerability), null: false do
        description "Fetch all vulnerabilities"
        visibility :internal

        argument :ecosystem, Enums::VulnerabilityPlatform, "Ecosystem of the vulnerabilities to load", required: false
        argument :status, Enums::VulnerabilityStatus, "Status of the vulnerabilities to load", required: false
        argument :severities, [Enums::VulnerabilitySeverity, null: true], "Severities of the vulnerabilities to load", required: false
        argument :identifier_query, String, "Partial identifier of the vulnerabilities to load", required: false, camelize: false
        argument :affects_query, String, "Partial package name of the vulnerabilities to load", required: false, camelize: false
        argument :order_by, Inputs::VulnerabilityOrder, "Ordering options for the returned vulnerabilities.", required: false
        argument :simulation, Boolean, "Return only internal-test Vulnerabilities", required: false
        argument :source_query, String, "Source of the vulnerabilities", required: false, camelize: false
      end

      def vulnerabilities(**args)
        vulns = ::Vulnerability.includes(:vulnerable_version_ranges).order("vulnerabilities.created_at desc")
        vulns = vulns.has_ecosystem(args[:ecosystem]) if args[:ecosystem]
        vulns = vulns.severity(args[:severities]) if args[:severities]
        vulns = vulns.status(args[:status]) if args[:status]
        vulns = vulns.identifier_like(args[:identifier_query]) if args[:identifier_query]
        vulns = vulns.affects_like(args[:affects_query]) if args[:affects_query]
        vulns = vulns.where(simulation: true) if args[:simulation]
        vulns = vulns.source_like(args[:source_query]) if args[:source_query]

        # handle sorting options
        if args[:order_by]
          field = args[:order_by][:field]
          direction = args[:order_by][:direction]

          # tho attribute is 'external_identifier', column on
          # vulns is 'identifier'

          if field == "external_identifier"
            field = "identifier"
          end

          vulns = vulns.reorder("vulnerabilities.#{field} #{direction}")
        end

        ArrayWrapper.new(vulns)
      end

      field :vulnerability, Objects::Vulnerability, visibility: :internal, description: "Fetch a vulnerability by ID", null: false do
        argument :database_id, Integer, "Vulnerability database id.", required: true
      end

      def vulnerability(**arguments)
        ::Vulnerability.find(arguments[:database_id])
      end

      field :vulnerable_version_range, Objects::VulnerableVersionRange, visibility: :internal, description: "Fetch a vulnerable version range by ID", null: false do
        argument :database_id, Integer, "Vulnerable version range database id.", required: true
      end

      def vulnerable_version_range(**arguments)
        ::VulnerableVersionRange.find(arguments[:database_id])
      end

      field :early_access_memberships, Connections.define(Objects::EarlyAccessMembership), visibility: :internal, description: "Submitted user requests to join early access programs", connection: true, null: true do
        argument :feature, Enums::EarlyAccessMembershipFeature,
          description: "The early access feature to get memberships for.",
          required: true
        argument :filter, Enums::EarlyAccessMembershipFilter,
          description: "The condition to filter memberships by.",
          required: false,
          default_value: :all
        argument :query, String,
          description: "The login of a user to search for a membership for.",
          required: false
        argument :order_by, Inputs::EarlyAccessMembershipOrder,
          description: "Ordering options for memberships returned from the connection.",
          required: false,
          default_value: { field: "created_at", direction: "ASC" }
      end

      def early_access_memberships(**arguments)
        scope = ::EarlyAccessMembership.where(feature_slug: arguments[:feature])

        case arguments[:filter]
        when :pending
          scope = scope.where(feature_enabled: false)
        when :accepted
          scope = scope.where(feature_enabled: true)
        end

        if arguments[:query]
          query = ActiveRecord::Base.sanitize_sql_like(arguments[:query].strip)

          # Get the ids for the matching members of eligible types
          business_ids = ::Business.where(["slug LIKE :query", { query: "%#{query}%" }]).pluck(:id)
          user_ids = ::User.where(["login LIKE :query", { query: "%#{query}%" }]).pluck(:id)

          scope = scope.where(
            member_type: "Business", member_id: business_ids,
          ).or(
            scope.where(
              member_type: "User", member_id: user_ids,
            ),
          )
        end

        order_by = arguments[:order_by]
        scope.order("early_access_memberships.#{order_by[:field]} #{order_by[:direction]}")
      end

      field :email_domain_reputation, Objects::SpamuraiReputation, visibility: :internal, description: "Reputation data for an email domain.", null: false do
        argument :address, String, description: "Email address or domain name.", required: true
        argument :address_type, Enums::EmailDomainAddressType, description: "Lookup address type.", default_value: :email_domain, required: false
      end

      def email_domain_reputation(**arguments)
        EmailDomainReputationRecord.reputation(arguments[:address], address_type: arguments[:address_type])
      end

      field :email_domain_metadata, Objects::EmailDomainMetadata, visibility: :internal, description: "Metadata for an email domain.", null: false do
        argument :address, String, description: "Email address or domain name.", required: true
        argument :address_type, Enums::EmailDomainAddressType, description: "Lookup address type.", default_value: :email_domain, required: false
      end

      def email_domain_metadata(**arguments)
        EmailDomainReputationRecord.metadata(arguments[:address], address_type: arguments[:address_type])
      end

      field :accounts_for_email_domain, Connections.define(Unions::Account), description: "Lookup user accounts by email domain.", null: true, connection: true do
        argument :address, String, description: "Email address or domain name.", required: true
        argument :include_spammy, Boolean, description: "Include spammy accounts.", required: false, default_value: false
      end

      def accounts_for_email_domain(**arguments)
        email_domain = EmailDomainReputationRecord.normalize_domain(arguments[:address])

        scope = ::User.oldest_to_newest.joins(:emails).where("user_emails.normalized_domain = ?", email_domain)

        unless arguments[:include_spammy]
          scope = scope.not_spammy
        end

        scope
      end

      field :accounts_for_email_pattern, Connections.define(Unions::Account), description: "Lookup user accounts by email pattern.", null: true, connection: true do
        argument :pattern, String, description: "Email pattern.", required: true
        argument :days, Integer, description: "Number of days to look back.", required: false, default_value: 3
        argument :include_spammy, Boolean, description: "Include spammy accounts.", required: false, default_value: false
      end

      def accounts_for_email_pattern(**arguments)
        scope = ::User.oldest_to_newest.joins(:emails)
        scope = scope.where("user_emails.email LIKE ?", arguments[:pattern])
        scope = scope.where("user_emails.created_at > ?", arguments[:days].days.ago)

        unless arguments[:include_spammy]
          scope = scope.not_spammy
        end

        scope
      end

      field :accounts_for_emails, [Unions::Account], description: "Lookup user for email addresses.", null: true do
        argument :addresses, [String], "User email addresses. Limit 100.", required: true
      end

      def accounts_for_emails(**arguments)
        raise Errors::Validation, "Only up to 100 emails is supported." if arguments[:addresses].size > 100

        ::User.oldest_to_newest.joins(:emails).where("user_emails.email IN (?)", arguments[:addresses]).limit(100)
      end
    end
  end
end
