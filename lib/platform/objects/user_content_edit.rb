# frozen_string_literal: true

module Platform
  module Objects
    class UserContentEdit < Platform::Objects::Base
      description "An edit on user content"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        # use the user content this edit is on to determine access
        object.async_user_content.then do |user_content|
          class_name = Platform::Helpers::NodeIdentification.type_name_from_object(user_content)
          permission.typed_can_access?(class_name, user_content)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_user_content.then do |user_content|
          content_type_name = Platform::Helpers::NodeIdentification.type_name_from_object(user_content)
          next false unless permission.typed_can_see?(content_type_name, user_content)

          object.async_viewer_can_read?(permission.viewer)
        end
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      global_id_field :id

      created_at_field
      updated_at_field
      deleted_at_field
      field :edited_at, Scalars::DateTime, "When this content was edited", null: false

      field :editor, description: "The actor who edited this content", resolver: Resolvers::ActorEditor
      field :deleted_by, description: "The actor who deleted this content", resolver: Resolvers::ActorDeletedBy
      field :diff, String, "A summary of the changes for this edit", method: :safe_diff, null: true

      # This method special cases edits on gist comments in order to protect
      # secret gists. Gists aren't backed by abilities, and the only thing
      # protecting them is the secret-ness of the gist's "repo name". We don't
      # a user to be able to load the edit history of a secret gist comment
      # just because they have the UserContentEdit's id.
      def self.load_from_global_id(id)
        if id.include?(":")
          gist_repo_name_or_type, content_edit_id = id.split(":", 2)

          if ::UserContentEditable.is_custom_edit_class?(gist_repo_name_or_type)
            Loaders::ActiveRecord.load("::#{gist_repo_name_or_type}".constantize, content_edit_id.to_i)
          else
            Promise.all([
              Loaders::ActiveRecord.load(::Gist, gist_repo_name_or_type, column: :repo_name),
              Loaders::ActiveRecord.load(::UserContentEdit, content_edit_id.to_i),
            ]).then { |gist, edit|
              next nil unless edit
              next nil unless gist
              edit.async_user_content.then do |comment|
                # make sure the gist the comment is on is the same as the gist
                # in the global relay id
                if comment && comment.gist_id == gist.id
                  edit
                else
                  nil
                end
              end
            }
          end
        else
          async_edit = Promise.all([
            Platform::Loaders::ActiveRecord.load(::CommitCommentEdit, id.to_i, column: :user_content_edit_id),
            Platform::Loaders::ActiveRecord.load(::IssueEdit, id.to_i, column: :user_content_edit_id),
            Platform::Loaders::ActiveRecord.load(::IssueCommentEdit, id.to_i, column: :user_content_edit_id),
            Platform::Loaders::ActiveRecord.load(::PullRequestReviewEdit, id.to_i, column: :user_content_edit_id),
            Platform::Loaders::ActiveRecord.load(::PullRequestReviewCommentEdit, id.to_i, column: :user_content_edit_id),
            Platform::Loaders::ActiveRecord.load(::RepositoryAdvisoryEdit, id.to_i, column: :user_content_edit_id),
            Platform::Loaders::ActiveRecord.load(::RepositoryAdvisoryCommentEdit, id.to_i, column: :user_content_edit_id),
          ]).then do |results|
            results.compact.first || Platform::Loaders::ActiveRecord.load(::UserContentEdit, id.to_i)
          end

          async_edit.then do |edit|
            next nil unless edit

            edit.async_user_content.then do |content|
              # don't leak existence of secret gists - they aren't enumerable
              # by UserContentEdit id
              if content.is_a? ::GistComment
                nil
              else
                edit
              end
            end
          end
        end
      end
    end
  end
end
