# frozen_string_literal: true

module Platform
  module Objects
    class ReviewStatusHovercardContext < Objects::Base
      implements Interfaces::HovercardContext
      description <<~DESCRIPTION
        A hovercard context with a message describing the current code review state of the pull
        request.
      DESCRIPTION
      areas_of_responsibility :user_profile
      scopeless_tokens_as_minimum

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, ctx)
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      field :review_decision, Enums::PullRequestReviewDecision, null: true,
        description: "The current status of the pull request with respect to code review."
    end
  end
end
