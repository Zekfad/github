# frozen_string_literal: true

module Platform
  module Objects
    module AuditLog
      class OrgRestoreMemberMembershipTeamAuditEntryData < Platform::Objects::Base
        description "Metadata for a team membership for org.restore_member actions"

        minimum_accepted_scopes ["admin:org", "admin:enterprise"]

        areas_of_responsibility :audit_log, :orgs

        # Determine whether the viewer can access this object via the API (called internally).
        # This is where Egress checks for OAuth scopes and GitHub Apps go.
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_api_can_access?(permission, audit_entry)
          # Access to this object is managed by its parent OrgRestoreMemberAuditEntry.
          true
        end

        # Determine whether the viewer can see this object (called internally).
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_viewer_can_see?(permission, audit_entry)
          # Access to this object is managed by its parent OrgRestoreMemberAuditEntry.
          true
        end

        implements Platform::Interfaces::AuditLog::TeamAuditEntryData
      end
    end
  end
end
