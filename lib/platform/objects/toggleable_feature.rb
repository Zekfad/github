# frozen_string_literal: true

module Platform
  module Objects
    class ToggleableFeature < Platform::Objects::Base
      model_name "Feature"
      description "A feature that is toggleable by an actor"
      implements Platform::Interfaces::Node

      areas_of_responsibility :feature_lifecycle

      global_id_field :id
      visibility :under_development

      scopeless_tokens_as_minimum

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, user)
        permission.access_allowed?(:read_toggleable_feature, resource: Platform::PublicResource.new, current_repo: nil, current_org: nil, allow_integrations: false, allow_user_via_integration: false)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return true if permission.viewer&.site_admin?
        return true if permission.viewer&.github_developer?

        object.async_flipper_feature.then do
          object.async_viewer_can_read?(permission.viewer)
        end
      end

      field :public_name, String, "The public name for the toggleable feature.", null: false
      field :slug, String, "The slug for the toggleable feature.", null: false, visibility: :internal
      field :description, String, "The description for the toggleable feature.", null: true
      field :feedback_url, Scalars::URI, description: "A URL pointing to the feedback link for the toggleable feature.", null: true
      field :enrolled_by_default, Boolean, description: "When true, all users who have access to the feature (via Flipper gates, or if no FlipperFeature is associated) are enrolled by default and must opt out to turn off.", null: false, visibility: :internal
      field :flipper_feature, Objects::Feature, description: "The associated flipper feature.", null: true, visibility: :internal
      field :image_url, Scalars::URI, description: "The associated image URL.", null: true, visibility: :internal, method: :image_link
      field :documentation_url, Scalars::URI, description: "The associated documentation URL.", null: true, visibility: :internal, method: :documentation_link
      field :viewer_is_enrolled, Boolean, "Whether the viewer is enrolled in the feature.", null: false
      field :viewer_opted_out, Boolean, "Whether the viewer has explicitly opted out of the feature.", null: false

      def flipper_feature
        Platform::Loaders::ActiveRecordAssociation.load(@object, :flipper_feature).then do
          @object.flipper_feature
        end
      end

      def viewer_is_enrolled
        context[:viewer].beta_feature_enabled?(@object)
      end

      def viewer_opted_out
        !@object.enrollments.for_enrollee(context[:viewer]).where(enrolled: false).blank?
      end
    end
  end
end
