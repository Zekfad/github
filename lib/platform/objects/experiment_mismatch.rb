# frozen_string_literal: true

module Platform
  module Objects
    class ExperimentMismatch < Platform::Objects::Base
      description "A mismatch in a code experiment."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        ::Experiment.viewer_can_read?(permission.viewer)
      end

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      field :control_value, String, "The control value for the mismatch", null: true
      field :backtrace, [String], "The backtrace for the mismatch", null: false
      field :raw_data_lines, [String], "The raw data lines for the mismatch", null: false

      def candidate_values
        @object.candidate_values_with_truncation.map do |name, value, is_truncated, exception|
          Platform::Models::ExperimentMismatchCandidateValue.new(
            name: name,
            value: value,
            is_truncated: is_truncated,
            exception: exception,
          )
        end
      end
      field :candidate_values, [Objects::ExperimentMismatchCandidateValue], "The candidate values for the mismatch", null: false

      def diff_lines_by_candidate
        @object.diff_lines_by_candidate.map do |name, diff_lines|
          Platform::Models::ExperimentMismatchDiffLinesByCandidate.new(name: name, diff_lines: diff_lines)
        end
      end
      field :diff_lines_by_candidate, [Objects::ExperimentMismatchDiffLine], "The diff lines for the mismatch", null: false
    end
  end
end
