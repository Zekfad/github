# frozen_string_literal: true

module Platform
  module Objects
    class UserLicense < Platform::Objects::Base
      description "A user license is records information about how a user is licensed on a particular business."
      visibility :internal
      areas_of_responsibility :gitcoin
      minimum_accepted_scopes ["read:enterprise"]

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        permission.access_allowed?(:view_business_profile, resource: object.business,
          current_repo: nil, current_org: nil,
          allow_integrations: false, allow_user_via_integration: false)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return true if permission.viewer&.site_admin?
        object.business.readable_by?(permission.viewer)
      end

      global_id_field :id

      database_id_field

      field :license_type, Platform::Enums::UserLicenseType, description: "The type of license granted.", null: false
    end
  end
end
