# frozen_string_literal: true

module Platform
  module Objects
    class OrganizationDiscussion < Platform::Objects::Base
      model_name "OrganizationDiscussionPost"
      description "An organization discussion."
      visibility :under_development
      minimum_accepted_scopes ["read:org"]
      areas_of_responsibility :orgs

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, post)
        post.async_organization.then do |org|
          access = if post.public?
            :list_public_members
          else
            :get_org_private
          end
          permission.access_allowed?(access, resource: org, allow_integrations: true,
                                     allow_user_via_integration: true, current_repo: nil,
                                     current_org: org)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, post)
        post.async_readable_by?(permission.viewer)
      end

      global_id_field :id
      database_id_field
      implements Platform::Interfaces::Node
      implements Interfaces::Comment
      implements Interfaces::Deletable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment

      url_fields description: "The HTTP URL for this discussion" do |post|
        post.async_organization.then do |org|
          template = Addressable::Template.new("/orgs/{org}/announcements/{post}")
          template.expand org: org.login, post: post.number
        end
      end

      field :author_association, Enums::CommentAuthorAssociation,
        description: "Author's association with the discussion's organization.", null: false

      def author_association
        # This association is defined by `Interfaces::Comment` and is related
        # to a repository. An `OrganizationDiscussionPost` is not associated to any repository,
        # but to an organization. As currently only organization members can create posts,
        # we return `:member` by default.
        :member
      end

      field :number, Integer, "Identifies the discussion within its organization.", null: false
      field :is_pinned, Boolean, "Whether or not the discussion is pinned.", null: false,
        method: :pinned?
      field :is_private, Boolean, "Whether or not the discussion is only visible to organization " \
                                  "members. Public discussions are visible to everyone.",
        null: false, method: :private?
      field :title, String, description: "The title of the discussion", null: false

      def title
        @object.title || GitHub::HTMLSafeString::EMPTY
      end

      url_fields prefix: "comments", description: "The HTTP URL for discussion comments." do |post|
        post.async_organization.then do |org|
          template = Addressable::Template.new("/orgs/{org}/announcements/{post}/comments")
          template.expand org: org.login, post: post.number
        end
      end

      field :comments, Connections.define(Objects::OrganizationDiscussionComment),
        numeric_pagination_enabled: true, description: "A list of comments on this discussion.",
        null: false, connection: true, areas_of_responsibility: :orgs do
          argument :from_comment, Integer, "When provided, filters the connection such that " \
                                           "results begin with the comment with this number.",
            required: false
        end

      def comments(from_comment: nil)
        replies = @object.replies.order("organization_discussion_post_replies.number ASC")

        if (number = from_comment)
          replies = replies.where("organization_discussion_post_replies.number >= ?", number)
        end

        replies
      end

      field :body_version, String, description: "Identifies the discussion body hash.", null: false

      field :organization, Objects::Organization, method: :async_organization,
        description: "The organization that defines the context of this discussion.", null: false

      field :viewer_can_pin, Boolean,
        description: "Whether or not the current viewer can pin this discussion.", null: false

      def viewer_can_pin
        @object.async_viewer_can_pin?(@context[:viewer])
      end
    end
  end
end
