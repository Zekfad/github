# frozen_string_literal: true

module Platform
  module Objects
    class MarkdownComponent < Platform::Objects::Base
      areas_of_responsibility :ce_extensibility

      description "Represents an app-created markdown component which is typically used in conjunction with other component types."

      visibility :under_development

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, composable_comment)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        # Uncommenting the line below leads to some exception telling me to use a
        # platform loader. So that's a TODO
        # permission.belongs_to_repository(object)
        permission.hidden_from_public?(self)
      end

      scopeless_tokens_as_minimum


      global_id_field :id

      database_id_field

      field :body, String, "The raw text of the markdown component.", null: false

      field :bodyHTML, Scalars::HTML, description: "The body rendered to HTML.", null: false

      def body_html
        @object.async_body_html.then do |body_html|
          body_html || GitHub::HTMLSafeString::EMPTY
        end
      end
    end
  end
end
