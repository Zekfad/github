# frozen_string_literal: true

module Platform
  module Objects
    class Comparison < Platform::Objects::Base
      description "Represents a comparison between two commit revisions."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        Promise.all([
          permission.typed_can_see?("Repository", object.head_repo),
          permission.typed_can_see?("Repository", object.base_repo),
        ]).then do |can_see_head, can_see_base|
          can_see_head && can_see_base
        end
      end

      # TODO commitComments is not N+1 proof and needs to be before making this object
      # publicly available.
      visibility :internal

      minimum_accepted_scopes ["repo"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :commit_comments, Connections.define(Objects::CommitComment), description: "The comments associated with this comparison's commits.", null: false, connection: true

      def commit_comments
        @object.async_visible_comments_for(@context[:viewer])
      end

      field :commits, Connections::ComparisonCommit, max_page_size: 250, description: "The commits which compose this comparison.", null: false, connection: true

      def commits
        @object.async_commits.then do |commits|
          ArrayWrapper.new(commits)
        end
      end

      def self.load_from_global_id(id)
        GitHub::Comparison.load_from_global_id(id)
      end
    end
  end
end
