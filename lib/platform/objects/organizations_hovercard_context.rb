# frozen_string_literal: true

module Platform
  module Objects
    class OrganizationsHovercardContext < Objects::Base
      description "An organization list hovercard context"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, ctx)
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      scopeless_tokens_as_minimum

      implements Interfaces::HovercardContext

      field :relevantOrganizations, Connections.define(Objects::Organization), connection: true, null: false, resolver: Resolvers::Organizations do
        description "Organizations this user is a member of that are relevant"
      end

      field :totalOrganizationCount, Integer, null: false, method: :total_organization_count do
        description "The total number of organizations this user is in"
      end
    end
  end
end
