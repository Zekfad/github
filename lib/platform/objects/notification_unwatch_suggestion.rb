# frozen_string_literal: true

module Platform
  module Objects
    class NotificationUnwatchSuggestion < Platform::Objects::Base
      description "Represents a notification unwatch suggestion for the viewer."
      minimum_accepted_scopes ["notifications"]
      visibility :internal
      areas_of_responsibility :notifications

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # Only allow access from queries against the internal schema.
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_readable_by?(permission.viewer)
      end

      field :snapshot_date, String, description: "The last time the suggestions for a user were generated.", null: false
      field :algorithm_version, String, description: "The version of the ML algorithm the suggestions were generated from.", null: false
      field :score, Float, description: "The strength of the suggestion from the ML algorithm.", null: false
      field :repository, Objects::Repository, description: "The repository", null: false, method: :async_repository
    end
  end
end
