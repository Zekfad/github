# frozen_string_literal: true

module Platform
  module Objects
    class Blob < Platform::Objects::Base
      description "Represents a Git blob."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, blob)
        repo = blob.repository
        permission.async_owner_if_org(repo).then do |org|
          permission.access_allowed?(:get_blob, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_git_object(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::GitObject

      global_id_field :id

      field :byte_size, Integer, "Byte size of Blob object", method: :size, null: false

      field :is_binary, Boolean, "Indicates whether the Blob is binary or text. Returns null if unable to determine the encoding.", method: :binary?, null: true

      field :is_truncated, Boolean, "Indicates whether the contents is truncated", method: :truncated?, null: false

      field :text, String, description: "UTF8 text data or null if the Blob is binary", null: true

      def text
        if @object.binary?
          nil
        else
          @object.data
        end
      end

      def self.load_from_global_id(id)
        Interfaces::GitObject.load_from_global_id(id)
      end
    end
  end
end
