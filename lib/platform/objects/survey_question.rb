# frozen_string_literal: true

module Platform
  module Objects
    class SurveyQuestion < Platform::Objects::Base
      description "A single question on a survey"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer && permission.viewer.site_admin?
      end

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :text, String, "Question asked", null: true
      field :short_text, String, "Short version of question", null: true
      field :hidden, Boolean, "Is this question show?", null: false

     field :display_order, Integer, description: "Display position of this question", null: false

     field :choices, Connections.define(Platform::Objects::SurveyChoice), description: "Possible answers to the question", null: true, connection: true

      def choices
        @object.async_choices.then do |choices|
          ArrayWrapper.new(choices)
        end
      end

      field :other_choice, Platform::Objects::SurveyChoice, description: "An other choice, along for user string input", null: true

      def other_choice
        @object.async_choices.then do |_|
          @object.other_choice
        end
      end

      field :other_answers_summary, [String, null: true], description: "All other answers", null: false

      def other_answers_summary
        @object.async_choices.then do |_|
          summary = @object.other_answers_summary
          if summary.nil?
            []
          else
            summary
          end
        end
      end

      field :answers, Connections.define(Platform::Objects::SurveyAnswer), description: "Answers to this question", null: true, connection: true

      def answers
        @object.async_answers.then do |answers|
          ArrayWrapper.new(answers)
        end
      end

      field :summarized_answers, Connections.define(Platform::Objects::SurveySummarizedAnswer), description: "Returns a summary of the response offering the percent of selections for each choice", null: true, connection: true

      def summarized_answers
        ArrayWrapper.new(@object.summarized_answers.map { |summary| OpenStruct.new(summary) })
      end
    end
  end
end
