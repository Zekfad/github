# frozen_string_literal: true

module Platform
  module Objects
    class AddedToMergeQueueEvent < Platform::Objects::Base
      model_name "IssueEvent"
      description "Represents a 'added_to_merge_queue' event on a given pull request."
      areas_of_responsibility :pull_requests

      def self.async_api_can_access?(permission, event)
        permission.belongs_to_issue_event(event)
      end

      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_issue(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::TimelineEvent
      implements Interfaces::PerformableViaApp

      global_id_field :id
      visibility :internal

      field :pull_request, Objects::PullRequest, "PullRequest referenced by event.", method: :async_issue_or_pull_request, null: true
      field :enqueuer, Objects::User, "The user who added this Pull Request to the merge queue", method: :async_actor, null: true

      url_fields description: "The HTTP URL for this event.", visibility: :internal do |event|
        event.async_path_uri
      end
    end
  end
end
