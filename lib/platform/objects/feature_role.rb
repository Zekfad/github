# frozen_string_literal: true

module Platform
  module Objects
    class FeatureRole < Platform::Objects::Base
      description "Represents a role that has a feature enabled"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, *)
        permission.viewer && (permission.viewer.site_admin? || permission.viewer.github_developer?)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node
      implements Interfaces::FeatureActor

      global_id_field :id

      field :name, String, description: "A human readable name for this role (ex github-lowworker)", null: false

      # Can't use `method: :to_s` option because it hits this class's `#to_s` method, not `@object.to_s`
      def name
        @object.to_s
      end

      def self.load_from_global_id(id)
        Promise.resolve(GitHub::FlipperRole.new(id))
      end
    end
  end
end
