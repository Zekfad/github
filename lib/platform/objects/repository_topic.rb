# frozen_string_literal: true

module Platform
  module Objects
    class RepositoryTopic < Platform::Objects::Base
      description "A repository-topic connects a repository to a topic."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, repo_topic)
        repo_topic.async_repository.then do |repo|
          permission.typed_can_access?("Repository", repo)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_repository(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::UniformResourceLocatable

      global_id_field :id

      url_fields description: "The HTTP URL for this repository-topic." do |repo_topic, arguments, context|
        repo_topic.async_topic.then do |topic|
          if GitHub.enterprise?
            repo_topic.async_repository.then do |repo|
              repo.async_owner.then do |owner|
                template_args = { topic: topic.name }
                template = "/search?q=topic%3A{topic}"
                if owner.organization?
                  template += "+org%3A{org}"
                  template_args[:org] = owner.login
                end
                template += "+fork%3Atrue" if repo.fork?
                template += "&type=Repositories"
                Addressable::Template.new(template).expand template_args
              end
            end
          else
            Addressable::Template.new("/topics/{topic}").expand(topic: topic.name)
          end
        end
      end

      field :topic, Objects::Topic, method: :async_topic, description: "The topic.", null: false

      field :stafftools_info, Objects::RepositoryTopicStafftoolsInfo, description: "Fields that are only visible to site admins.", null: true

      def stafftools_info
        if self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          Models::RepositoryTopicStafftoolsInfo.new(@object)
        else
          nil
        end
      end
    end
  end
end
