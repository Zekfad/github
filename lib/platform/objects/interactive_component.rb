# frozen_string_literal: true

module Platform
  module Objects
    class InteractiveComponent < Platform::Objects::Base
      areas_of_responsibility :ce_extensibility

      description "Represents an app-created interactive component with which a user can interact."

      visibility :under_development

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, composable_comment)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        # Uncommenting the line below leads to an exception telling me to use a
        # platform loader. So that's a TODO
        # permission.belongs_to_repository(object)
        permission.hidden_from_public?(self)
      end

      scopeless_tokens_as_minimum

      global_id_field :id

      database_id_field

      field :elements, String, "The declarative elements of the interactive component.", null: false

      field :container, Interfaces::InteractiveComponentContainer, description: "The parent entity that contains the interactive component", null: false

      def container
        Loaders::ActiveRecord.load(@object.container_type.constantize, object.container_id)
      end

      field :latest_ephemeral_notice, Platform::Objects::EphemeralNotice, description: "The latest ephemeral notice related to this interactive component", null: true

      def latest_ephemeral_notice
        notice = object.latest_ephemeral_notice(@context[:viewer])
        return nil if notice&.expired?
        notice
      end
    end
  end
end
