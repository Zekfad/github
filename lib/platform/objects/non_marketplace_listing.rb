# frozen_string_literal: true

module Platform
  module Objects
    class NonMarketplaceListing < Platform::Objects::Base
      description "A listing in the Works with GitHub directory."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return true if object.approved?
        return false unless permission.viewer

        object.async_creator.then do
          object.adminable_by?(permission.viewer)
        end
      end

      visibility :internal

      minimum_accepted_scopes ["repo"]

      implements Platform::Interfaces::Node

      global_id_field :id
      database_id_field
      field :name, String, "The listing's full name.", null: false
      field :description, String, "The listing's description.", null: false
      field :normalized_description, String, "The listing's description without a trailing period or ampersands.", null: false
      field :url, Scalars::URI, "A link to the product.", null: false
      field :is_approved, Boolean, "Whether this listing has been approved for display in Works with GitHub.", method: :approved?, null: false
      field :is_draft, Boolean, "Whether this listing is still a draft that is not publicly visible in Works with GitHub.", method: :draft?, null: false
      field :is_rejected, Boolean, "Whether this listing has been rejected by GitHub for display in Works with GitHub.", method: :rejected?, null: false
      field :is_delisted, Boolean, "Whether this listing has been removed from Works with GitHub.", method: :delisted?, null: false
      field :is_enterprise_compatible, Boolean, "Whether this listing is compatible with GitHub Enterprise.", method: :enterprise_compatible?, null: false

      field :admin_info, Objects::NonMarketplaceListingAdminInfo, description: "Listing information only visible to site administrators.", null: true

      def admin_info
        if @context[:viewer] && @context[:viewer].can_admin_non_marketplace_listings?
          Models::NonMarketplaceListingAdminInfo.new(@object)
        else
          nil
        end
      end

      field :logo_url, Scalars::URI, description: "The URL for the listing's logo.", null: true do
        argument :size, Integer, "The size in pixels of the resulting square image.", default_value: 400, required: false
      end

      def logo_url(**arguments)
        if @object.primary_avatar
          @object.primary_avatar_url(arguments[:size])
        else
          @object.async_integration_listing.then do |integration_listing|
            if integration_listing
              integration_listing.async_integration.then do |integration|
                integration.preferred_avatar_url(size: arguments[:size])
              end
            else
              @object.async_creator.then do |creator|
                creator.primary_avatar_url(arguments[:size])
              end
            end
          end
        end
      end

      field :creator, Objects::User, description: "The user who submitted this listing.", null: false

      def creator
        Loaders::ActiveRecord.load(::User, @object.creator_id)
      end

      field :category, Objects::IntegrationFeature, description: "The category that best describes the listing.", null: false

      def category
        Loaders::ActiveRecord.load(::IntegrationFeature, @object.integration_feature_id)
      end

      field :viewer_can_edit, Boolean, description: "Can the current viewer edit this Works with GitHub listing.", null: false

      def viewer_can_edit
        @object.async_creator.then do
          @object.editable_by?(@context[:viewer])
        end
      end

      field :viewer_can_approve, Boolean, description: "Can the current viewer approve this Works with GitHub listing.", null: false

      def viewer_can_approve
        return false if @context[:viewer].blank?
        return false unless @object.can_approve?

        @context[:viewer].can_admin_non_marketplace_listings?
      end

      field :viewer_can_delist, Boolean, description: "Can the current viewer delist this Works with GitHub listing.", null: false

      def viewer_can_delist
        return false if @context[:viewer].blank?
        return false unless @object.can_delist?

        @context[:viewer].can_admin_non_marketplace_listings?
      end

      field :viewer_can_reject, Boolean, description: "Can the current viewer reject this Works with GitHub listing.", null: false

      def viewer_can_reject
        return false if @context[:viewer].blank?
        return false unless @object.can_reject?

        @context[:viewer].can_admin_non_marketplace_listings?
      end
    end
  end
end
