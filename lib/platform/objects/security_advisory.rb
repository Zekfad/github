# frozen_string_literal: true

module Platform
  module Objects
    class SecurityAdvisory < Platform::Objects::Base
      visibility :public

      implements Platform::Interfaces::Node

      description "A GitHub Security Advisory"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, security_advisory)
        security_advisory.async_api_visible?
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, security_advisory)
        security_advisory.async_api_visible?
      end

      scopeless_tokens_as_minimum

      areas_of_responsibility :security_advisories

      global_id_field :id

      database_id_field
      field :ghsa_id, String, "The GitHub Security Advisory ID", null: false
      field :summary, String, "A short plaintext summary of the advisory", null: false
      field :description, String, "This is a long plaintext description of the advisory", null: false
      field :severity, Enums::SecurityAdvisorySeverity, "The severity of the advisory", null: false
      field :permalink, Scalars::URI, "The permalink for the advisory", null: true
      field :credits, "Credits associated with this Advisory", resolver: Resolvers::SecurityAdvisoryCredits, null: false

      field :identifiers, [SecurityAdvisoryIdentifier], "A list of identifiers for this advisory", null: false
      field :references, [SecurityAdvisoryReference], "A list of references for this advisory", null: false
      field :published_at, Scalars::DateTime, "When the advisory was published", null: false
      field :updated_at, Scalars::DateTime, "When the advisory was last updated", null: false
      field :withdrawn_at, Scalars::DateTime, "When the advisory was withdrawn, if it has been withdrawn", null: true

      # the reason for using a String instead of an Enum for origin is to avoid leaking details about who we are planning to get advisories from before we are ready
      field :origin, String, "The organization that originated the advisory", null: false

      field :vulnerabilities, "Vulnerabilities associated with this Advisory",
                              resolver: Resolvers::SecurityVulnerabilities,
                              null: false

      def self.load_from_global_id(ghsa_id)
        Loaders::ActiveRecord.load(::SecurityAdvisory.disclosed.always_visible_publicly, ghsa_id, column: :ghsa_id)
      end
    end
  end
end
