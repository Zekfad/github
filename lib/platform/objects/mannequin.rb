# frozen_string_literal: true

module Platform
  module Objects
    class Mannequin < Platform::Objects::Base
      areas_of_responsibility :migration

      description "A placeholder user for attribution of imported data on GitHub."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, mannequin)
        permission.typed_can_access?("User", mannequin)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        !object.hide_from_user?(permission.viewer)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      implements Interfaces::Actor

      implements Interfaces::UniformResourceLocatable

      global_id_field :id

      database_id_field

      created_at_field
      updated_at_field


      field :email, String, "The mannequin's email on the source instance.", null: true

      field :avatar_url, Scalars::URI, description: "A URL pointing to the GitHub App's public avatar.", null: false do
        argument :size, Integer, "The size of the resulting square image.", required: false
      end

      def avatar_url(size: nil)
        ::User.ghost.primary_avatar_url(size)
      end
    end
  end
end
