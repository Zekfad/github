# frozen_string_literal: true

module Platform
  module Objects
    class EnterpriseOwnerInfo < Platform::Objects::Base
      description "Enterprise information only visible to enterprise owners."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, business)
        permission.access_allowed?(:administer_business, resource: business, repo: nil, organization: nil, allow_integrations: false, allow_user_via_integration: false)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.owner?(permission.viewer) || permission.viewer&.site_admin?
      end

      minimum_accepted_scopes ["admin:enterprise"]


      # MEMBERS FIELDS

      field :admins, resolver: Resolvers::EnterpriseAdmins, description: "A list of all of the administrators for this enterprise.", connection: true, exempt_from_spam_filter_check: true
      field :pending_admin_invitations, resolver: Resolvers::EnterpriseAdministratorInvitations, description: "A list of pending administrator invitations for the enterprise.", connection: true
      field :pending_member_invitations, resolver: Resolvers::EnterprisePendingMemberInvitations, description: "A list of pending member invitations for organizations in the enterprise.", connection: true
      field :outside_collaborators, resolver: Resolvers::EnterpriseOutsideCollaborators, description: "A list of outside collaborators across the repositories in the enterprise.", connection: true

      field :pending_collaborators, resolver: Resolvers::EnterprisePendingCollaborators,
        description: "A list of pending collaborators across the repositories in the enterprise.", connection: true,
        deprecated: {
          start_date: Date.new(2020, 5, 18),
          reason: "Repository invitations can now be associated with an email, not only an invitee.",
          superseded_by: "Use the `pendingCollaboratorInvitations` field instead.",
          owner: "jdennes",
        }

      field :pending_collaborator_invitations, resolver: Resolvers::EnterprisePendingCollaboratorInvitations, description: "A list of pending collaborator invitations across the repositories in the enterprise.", connection: true

      field :organization_invitations, Connections.define(Objects::EnterpriseOrganizationInvitation), description: "A list of invitations for organizations to join this enterprise.", null: true, connection: true do
        visibility :under_development
        argument :status, Enums::EnterpriseOrganizationInvitationStatus, description: "If supplied, only returns invitations in that state.", required: false
        argument :order_by, Inputs::EnterpriseOrganizationInvitationOrder, description: "Ordering options for EnterpriseOrganizationInvitations returned from the connection.", required: false, default_value: { field: "created_at", direction: "ASC" }
      end

      def organization_invitations(args = {})
        invites = @object.organization_invitations
        invites = invites.with_status(args[:status]) if args[:status]

        if args[:order_by]
          field = args[:order_by][:field]
          direction = args[:order_by][:direction]
          invites = invites.reorder("business_organization_invitations.#{field} #{direction}")
        end

        invites
      end

      # Business settings fields:

      field :default_repository_permission_setting, Enums::EnterpriseDefaultRepositoryPermissionSettingValue,
        description: "The setting value for base repository permissions for organizations in this enterprise.", null: false

      def default_repository_permission_setting
        if !@object.default_repository_permission_policy?
          return Enums::EnterpriseDefaultRepositoryPermissionSettingValue.values["NO_POLICY"].value
        end

        case @object.default_repository_permission
        when :admin
          Enums::EnterpriseDefaultRepositoryPermissionSettingValue.values["ADMIN"].value
        when :write
          Enums::EnterpriseDefaultRepositoryPermissionSettingValue.values["WRITE"].value
        when :read
          Enums::EnterpriseDefaultRepositoryPermissionSettingValue.values["READ"].value
        when :none
          Enums::EnterpriseDefaultRepositoryPermissionSettingValue.values["NONE"].value
        end
      end

      field :default_repository_permission_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided default repository permission.", connection: true, null: false do
        argument :value, Enums::DefaultRepositoryPermissionField, "The permission to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def default_repository_permission_setting_organizations(value:, order_by:)
        object.default_repository_permission_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :is_updating_default_repository_permission, Boolean, method: :updating_default_repository_permission?,
        batch_load_configuration: true,
        description: "Whether or not the default repository permission is currently being updated.", null: false

      field :team_discussions_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether team discussions are enabled for organizations in this enterprise.", null: false

      def team_discussions_setting
        if !@object.team_discussions_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.team_discussions_allowed?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :team_discussions_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided team discussions setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def team_discussions_setting_organizations(value:, order_by:)
        object.team_discussions_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :organization_projects_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether organization projects are enabled for organizations in this enterprise.", null: false

      def organization_projects_setting
        if !@object.organization_projects_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.organization_projects_enabled?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :organization_projects_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided organization projects setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def organization_projects_setting_organizations(value:, order_by:)
        object.organization_projects_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :repository_projects_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether repository projects are enabled in this enterprise.", null: false

      def repository_projects_setting
        if !@object.repository_projects_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.repository_projects_enabled?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :repository_projects_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided repository projects setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def repository_projects_setting_organizations(value:, order_by:)
        object.repository_projects_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :members_can_change_repository_visibility_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether organization members with admin permissions on a repository can change repository visibility.", null: false

      def members_can_change_repository_visibility_setting
        if !@object.members_can_change_repo_visibility_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.members_can_change_repo_visibility?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :members_can_change_repository_visibility_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided can change repository visibility setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def members_can_change_repository_visibility_setting_organizations(value:, order_by:)
        object.members_can_change_repository_visibility_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :members_can_invite_collaborators_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether members of organizations in the enterprise can invite outside collaborators.", null: false

      def members_can_invite_collaborators_setting
        if !@object.members_can_invite_outside_collaborators_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.members_can_invite_outside_collaborators?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :members_can_invite_collaborators_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided members can invite collaborators setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def members_can_invite_collaborators_setting_organizations(value:, order_by:)
        object.members_can_invite_collaborators_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :members_can_create_repositories_setting, Enums::EnterpriseMembersCanCreateRepositoriesSettingValue,
        description: "The setting value for whether members of organizations in the enterprise can create repositories.", null: true

      def members_can_create_repositories_setting
        unless @object.members_can_create_repositories_policy?
          return Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["NO_POLICY"].value
        end

        # Backward-compatible values, wherein the state of internal repos is ignored.
        if @object.members_can_create_public_repositories? && @object.members_can_create_private_repositories?
          Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["ALL"].value
        elsif @object.members_can_create_public_repositories? && !@object.members_can_create_private_repositories?
          Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["PUBLIC"].value
        elsif !@object.members_can_create_public_repositories? && @object.members_can_create_private_repositories?
          Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["PRIVATE"].value
        else
          Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["DISABLED"].value
        end
      end

      field :members_can_create_public_repositories_setting, Boolean,
        description: "The setting value for whether members of organizations in the enterprise can create public repositories.", null: true

      def members_can_create_public_repositories_setting
        return nil unless @object.members_can_create_repositories_policy?
        return @object.members_can_create_public_repositories?
      end

      field :members_can_create_private_repositories_setting, Boolean,
        description: "The setting value for whether members of organizations in the enterprise can create private repositories.", null: true

      def members_can_create_private_repositories_setting
        return nil unless @object.members_can_create_repositories_policy?
        return @object.members_can_create_private_repositories?
      end

      field :members_can_create_internal_repositories_setting, Boolean,
        description: "The setting value for whether members of organizations in the enterprise can create internal repositories.", null: true

      def members_can_create_internal_repositories_setting
        return nil unless @object.members_can_create_repositories_policy?
        return @object.members_can_create_internal_repositories?
      end

      field :members_can_create_repositories_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided repository creation setting value.", connection: true, null: false do
        argument :value, Enums::OrganizationMembersCanCreateRepositoriesSettingValue, "The setting to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def members_can_create_repositories_setting_organizations(value:, order_by:)
        object.members_can_create_repositories_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :members_can_update_protected_branches_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether members with admin permissions for repositories can update protected branches.", null: false

      def members_can_update_protected_branches_setting
        if !@object.members_can_update_protected_branches_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.members_can_update_protected_branches?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :members_can_update_protected_branches_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided members can update protected branches setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def members_can_update_protected_branches_setting_organizations(value:, order_by:)
        object.members_can_update_protected_branches_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :members_can_delete_repositories_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether members with admin permissions for repositories can delete or transfer repositories.", null: false

      def members_can_delete_repositories_setting
        if !@object.members_can_delete_repositories_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.members_can_delete_repositories?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :members_can_delete_repositories_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided members can delete repositories setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def members_can_delete_repositories_setting_organizations(value:, order_by:)
        object.members_can_delete_repositories_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :members_can_delete_issues_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether members with admin permissions for repositories can delete issues.", null: false

      def members_can_delete_issues_setting
        if !@object.members_can_delete_issues_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.members_can_delete_issues?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :members_can_delete_issues_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided members can delete issues setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def members_can_delete_issues_setting_organizations(value:, order_by:)
        object.members_can_delete_issues_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :members_can_make_purchases_setting, Enums::EnterpriseMembersCanMakePurchasesSettingValue , description: "Indicates whether members of this enterprise's organizations can purchase additional services for those organizations.", null: false

      def members_can_make_purchases_setting
        if @object.members_can_make_purchases?
          Enums::EnterpriseMembersCanMakePurchasesSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseMembersCanMakePurchasesSettingValue.values["DISABLED"].value
        end
      end

      field :allow_private_repository_forking_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether private repository forking is enabled for repositories in organizations in this enterprise.", null: false

      def allow_private_repository_forking_setting
        if !@object.allow_private_repository_forking_policy?
          Platform::Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.allow_private_repository_forking?
          Platform::Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Platform::Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :allow_private_repository_forking_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided private repository forking setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def allow_private_repository_forking_setting_organizations(value:, order_by:)
        object.allow_private_repository_forking_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :action_execution_capability_setting, Enums::ActionExecutionCapabilitySetting, description: "The setting value for action execution capabilities of a repository.", null: false do
        visibility :under_development
      end

      def action_execution_capability_setting
        return Enums::ActionExecutionCapabilitySetting.values["NO_POLICY"].value unless @object.action_execution_capabilities_policy?

        return @object.action_execution_capabilities && @object.action_execution_capabilities.downcase
      end

      field :action_execution_capability_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided action execution capabilities setting value.", connection: true, null: false do
        argument :value, Enums::ActionExecutionCapability, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def action_execution_capability_setting_organizations(value:, order_by:)
        object.action_execution_capability_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :members_can_view_dependency_insights_setting, Enums::EnterpriseEnabledDisabledSettingValue, description: "The setting value for whether members can view dependency insights.", null: false

      def members_can_view_dependency_insights_setting
        if !@object.members_can_view_dependency_insights_policy?
          Enums::EnterpriseEnabledDisabledSettingValue.values["NO_POLICY"].value
        elsif @object.members_can_view_dependency_insights?
          Enums::EnterpriseEnabledDisabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledDisabledSettingValue.values["DISABLED"].value
        end
      end

      field :members_can_view_dependency_insights_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the provided members can view dependency insights setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def members_can_view_dependency_insights_setting_organizations(value:, order_by:)
        object.members_can_view_dependency_insights_setting_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      # fields to help drive the business 2FA confirmation modal
      field :two_factor_required_setting, Enums::EnterpriseEnabledSettingValue, description: "The setting value for whether the enterprise requires two-factor authentication for its organizations and users.", null: false

      def two_factor_required_setting
        if @object.two_factor_requirement_enabled?
          Enums::EnterpriseEnabledSettingValue.values["ENABLED"].value
        else
          Enums::EnterpriseEnabledSettingValue.values["NO_POLICY"].value
        end
      end

      field :two_factor_required_setting_organizations, Connections.define(Objects::Organization), description: "A list of enterprise organizations configured with the two-factor authentication setting value.", connection: true, null: false do
        argument :value, Boolean, "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
                 "Ordering options for organizations with this setting.",
                 required: false, default_value: { field: "login", direction: "ASC" }
      end

      def two_factor_required_setting_organizations(value:, order_by:)
        object.two_factor_required_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      end

      field :affiliated_users_with_two_factor_disabled_exist, Boolean, description: "Whether or not affiliated users with two-factor authentication disabled exist in the enterprise.", null: false

      def affiliated_users_with_two_factor_disabled_exist
        @object.async_organizations.then do
          @object.affiliated_users_with_two_factor_disabled_exist?
        end
      end

      field :affiliated_users_with_two_factor_disabled, resolver: Resolvers::EnterpriseAffiliatedUsersWithTwoFactorDisabled, description: "A list of users in the enterprise who currently have two-factor authentication disabled.", connection: true, exempt_from_spam_filter_check: true

      field :is_updating_two_factor_requirement, Boolean, description: "Whether the two-factor authentication requirement is currently being enforced.", null: false

      def is_updating_two_factor_requirement
        @object.async_organizations.then do
          @object.updating_two_factor_requirement?
        end
      end

      field :saml_identity_provider, Objects::EnterpriseIdentityProvider,
        method: :async_saml_provider,
        description: "The SAML Identity Provider for the enterprise.", null: true

      field :saml_identity_provider_setting_organizations, Connections.define(Objects::Organization),
        description: "A list of enterprise organizations configured with the SAML single sign-on setting value.",
        connection: true, null: false do
        argument :value, Enums::IdentityProviderConfigurationState,
          "The setting value to find organizations for.", required: true
        argument :order_by, Inputs::OrganizationOrder,
          "Ordering options for organizations with this setting.",
          required: false, default_value: { field: "login", direction: "ASC" }
      end

      def saml_identity_provider_setting_organizations(value:, order_by:)
        object.saml_configured_organizations \
          value: value,
          order_by_field: order_by&.dig(:field),
          order_by_direction: order_by&.dig(:direction)
      rescue ArgumentError => e
        raise Platform::Errors::Internal, e.message
      end

      field :enterprise_server_installations, Connections.define(Objects::EnterpriseServerInstallation),
        description: "Enterprise Server installations owned by the enterprise.",
        visibility: { public: { environments: [:dotcom] }, internal: { environments: [:enterprise] } },
        connection: true, null: false do
        argument :connected_only, Boolean,
          "Whether or not to only return installations discovered via GitHub Connect.",
          required: false, default_value: false
        argument :order_by, Inputs::EnterpriseServerInstallationOrder,
          "Ordering options for Enterprise Server installations returned.",
          required: false, default_value: { field: "host_name", direction: "ASC" }
      end

      def enterprise_server_installations(connected_only: false, order_by: nil)
        return EnterpriseInstallation.none unless context[:permission].can_list_enterprise_installations?(object)

        installations = object.enterprise_installations

        unless order_by.nil?
          installations = installations.order "enterprise_installations.#{order_by[:field]} #{order_by[:direction]}"
        end

        if connected_only
          installations = ArrayWrapper.new(installations.select(&:connected?))
        end
        installations
      end

      field :ip_allow_list_enabled_setting,
        Enums::IpAllowListEnabledSettingValue,
        description: "The setting value for whether the enterprise has an IP allow list enabled.",
        null: false

      def ip_allow_list_enabled_setting
        if @object.ip_whitelisting_enabled?
          Enums::IpAllowListEnabledSettingValue.values["ENABLED"].value
        else
          Enums::IpAllowListEnabledSettingValue.values["DISABLED"].value
        end
      end

      field :ip_allow_list_entries, Connections.define(Objects::IpAllowListEntry),
        description: "The IP addresses that are allowed to access resources owned by the enterprise.",
        connection: true, null: false do
        argument :order_by, Inputs::IpAllowListEntryOrder,
          "Ordering options for IP allow list entries returned.",
          required: false, default_value: { field: "whitelisted_value", direction: "ASC" }
      end

      def ip_allow_list_entries(order_by: nil)
        entries = object.ip_whitelist_entries

        unless order_by.nil?
          entries = entries.order "ip_whitelist_entries.#{order_by[:field]} #{order_by[:direction]}"
        end

        entries
      end
    end
  end
end
