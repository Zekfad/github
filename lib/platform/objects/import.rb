# frozen_string_literal: true

module Platform
  module Objects
    class Import < Platform::Objects::Base
      feature_flag :import_api
      areas_of_responsibility :import_api

      description "An import to GitHub"

      minimum_accepted_scopes ["read:user", "public_repo"]

      def self.async_api_can_access?(permission, import)
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, import)
        true
      end

      implements Platform::Interfaces::Node

      global_id_field :id
      created_at_field

      field :creator, Interfaces::Actor, "The user that created the Import", null: false
      field :repositories, Connections.define(Objects::Repository), null: false,
        description: "The repositories associated with this Import" do
        argument :order_by, Inputs::RepositoryOrder,
          "Ordering options for repository",
          required: false, default_value: { field: "created_at", direction: "ASC" }
      end
      def repositories(order_by: nil)
        Platform::Loaders::ActiveRecordAssociation.load(@object, :repositories).then do
          repositories = @object.repositories
          unless order_by.nil?
            repositories = repositories.order("repositories.#{order_by[:field]} #{order_by[:direction]}")
          end

          repositories
        end
      end
    end
  end
end
