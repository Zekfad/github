# frozen_string_literal: true

module Platform
  module Objects
    class CommunityProfile < Platform::Objects::Base
      description "Information about a repository's community engagement."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_repository(object)
      end

      visibility :internal

      scopeless_tokens_as_minimum

      field :has_code_of_conduct, Boolean, "Whether the repository includes a code of conduct.", null: true

      field :has_contributing, Boolean, null: true, method: :async_contributing?, description: <<~DESCRIPTION
        Whether the repository includes instructions describing how to contribute to the
        repository.
      DESCRIPTION

      field :has_license, Boolean, "Whether the repository includes a license.", null: true, method: :async_license?

      field :has_readme, Boolean, "Whether the repository includes a README file.", null: true, method: :async_readme?

      field :has_outside_contributors, Boolean, null: true, description: <<~DESCRIPTION
        Returns true if a non-member or non-collaborator of the repository has made contributions
        to it.
      DESCRIPTION

      field :has_documentation, Boolean, "Returns true if the repository has a website or a docs/ directory.", method: :has_docs, null: false
      field :has_description, Boolean, "Returns true if the repository has a description.", null: false, method: :description?
      field :has_issue_opened_by_non_collaborator, Boolean, "Returns true if a non-member or non-collaborator has opened an issue in the repository.", null: false
      field :has_pull_request_or_issue_template, Boolean, "Returns true if the repository has a template for opening issues or pull requests.", method: :async_pr_or_issue_template?, null: false
      field :help_wanted_issues_count, Integer, "Returns a count of how many open issues in the repository have the label 'help wanted'.", null: false

      field :good_first_issue_issues_count, Integer, null: false, description: <<~DESCRIPTION
        Returns a count of how many open issues in the repository have the label 'good first
        issue'.
      DESCRIPTION
    end
  end
end
