# frozen_string_literal: true

module Platform
  module Objects
    class DelegatedRecoveryToken < Platform::Objects::Base
      description "Represents a recovery token that can be used as part of the Delegated Account Recovery feature"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_viewer(object)
      end

      visibility :internal

      implements Platform::Interfaces::Node

      minimum_accepted_scopes [
        "repo",
      ]

      global_id_field :id

      field :token_id, String, description: "The external identifier shared with a recovery provider.", null: false
      field :provider, String, "The recovery provider for which the token was issued.", null: false

      created_at_field(description: "When the token was created")

      field :confirmed_at, Scalars::DateTime, description: "When the token was confirmed saved at the recovery provider.", null: true

      field :recovered_at, Scalars::DateTime, description: "The last time the token was used in a successful recovery.", null: true

      field :owner, User, method: :async_user, description: "Identifies the user who created the token.", null: true
    end
  end
end
