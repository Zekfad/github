# frozen_string_literal: true

module Platform
  module Objects
    class DeploymentStatus < Platform::Objects::Base
      description "Describes the status of a given deployment attempt."
      areas_of_responsibility :deployments_api

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, deployment_status)
        deployment_status.async_deployment.then do |deployment|
          permission.typed_can_access?("Deployment", deployment)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_deployment.then { |deployment|
          permission.typed_can_see?("Deployment", deployment)
        }
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::PerformableViaApp

      global_id_field :id

      field :creator, Interfaces::Actor, "Identifies the actor who triggered the deployment.", method: :async_creator, null: false

      field :deployment, Deployment, "Identifies the deployment associated with status.", null: false
      field :state, Enums::DeploymentStatusState, "Identifies the current state of the deployment.", null: false
      field :description, String, "Identifies the description of the deployment.", null: true
      field :environment_url, Scalars::URI, "Identifies the environment URL of the deployment.", null: true
      created_at_field
      updated_at_field
      field :environment, String, "Identifies the environment of the deployment at the time of this deployment status", null: true
      field :log_url, Scalars::URI, "Identifies the log URL of the deployment.", null: true do
        # can't use force_utf8_encoding in the model because this field is actually renamed
      end

      def log_url
        @object.log_url.dup&.force_encoding("UTF-8")
      end
    end
  end
end
