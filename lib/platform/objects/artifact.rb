# frozen_string_literal: true

module Platform
  module Objects
    class Artifact < Platform::Objects::Base
      description "An artifact from a check suite."
      areas_of_responsibility :checks
      visibility :internal

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # No special API permissions
        permission.hidden_from_public?(self) # Update this authorization if we ever go public with this object
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_check_suite.then do |check_suite|
          permission.typed_can_see?("CheckSuite", check_suite)
        end
      end

      scopeless_tokens_as_minimum

      database_id_field

      field :name, String, "The artifact's name.", null: false
      field :source_url, Scalars::URI, "The full URL to download all files in the artifact.", null: false
      field :size, Int, "The size of the artifact in bytes.", null: false
      field :expired, Boolean, "Whether or not the artifact has expired.", method: :expired?, null: false
    end
  end
end
