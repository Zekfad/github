# frozen_string_literal: true

module Platform
  module Objects
    class ExperimentBacktrace < Platform::Objects::Base
      description "Represents a experiment back stack trace."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        ::Experiment.viewer_can_read?(permission.viewer)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      field :github_link, String, "The link to this stacktrace file in GitHub.", null: false
      field :relative_file_path, String, "The relative file path to this stacktrace.", null: false
    end
  end
end
