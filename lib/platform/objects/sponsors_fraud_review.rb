# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsFraudReview < Platform::Objects::Base
      description "A fraud review for a Sponsors listing."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer.can_admin_sponsors_memberships?
      end

      visibility :internal

      minimum_accepted_scopes ["site_admin"]
      implements Platform::Interfaces::Node
      global_id_field :id

      created_at_field
      updated_at_field

      field :sponsors_listing,
        Objects::SponsorsListing,
        method: :async_sponsors_listing,
        description: "The sponsors listing that this fraud review is for.",
        null: false

      field :state,
        Enums::SponsorsFraudReviewState,
        description: "The current state of this fraud review.",
        null: false

      def state
        @object.state.to_sym
      end

      field :reviewer,
        Objects::User,
        method: :async_reviewer,
        description: "The staff member that reviewed this fraud review.",
        null: true

      field :reviewed_at,
        Scalars::DateTime,
        description: "The time this fraud review was reviewed by a staff member.",
        null: true

      url_fields description: "The HTTP URL for this fraud review" do |review|
        template = Addressable::Template.new("/stafftools/sponsors/fraud_reviews/{id}")
        template.expand id: review.id
      end
    end
  end
end
