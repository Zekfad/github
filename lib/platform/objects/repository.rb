# frozen_string_literal: true

module Platform
  module Objects

    class Repository < Platform::Objects::Base
      include Objects::Base::RecordObjectAccess

      description "A repository contains the content for a project."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, repo)
        return true if repo.public?
        Promise.all([repo.async_owner, repo.async_parent, repo.async_internal_repository]).then { |owner, _, _|
          if owner.is_a?(::Organization)
            current_org = owner
          end

          next true if permission.access_allowed?(:v4_get_repo, repo: repo, current_org: current_org, resource: repo, from_invitation: false, allow_integrations: true, allow_user_via_integration: true)

          Loaders::RepositoryInvitation.load(permission.viewer, repo).then do |invitation|
            # in this case, the invited user cannot read the metadata yet, but a subset is ok.
            next false unless invitation.present?

            permission.access_allowed?(:v4_get_repo, repo: repo, current_org: current_org, resource: repo, from_invitation: true, allow_integrations: true, allow_user_via_integration: false)
          end
        }.then do |access_allowed|
          if GitHub.flipper[:gql_repo_trade_restriction_block].enabled?
            if access_allowed && permission.external_request? && repo.trade_restricted?
              # Only raise if the viewer has the right permissions
              raise Errors::TradeControls.new(repo.trade_restriction_api_error_message(permission.viewer))
            end
          end

          access_allowed
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_visible_and_readable_by?(permission.viewer)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::ActionExecutionSettings
      implements Interfaces::ProjectOwner
      implements Interfaces::PackageOwner
      implements Interfaces::PackageSearch
      implements Interfaces::Subscribable
      implements Interfaces::Starrable
      implements Interfaces::UniformResourceLocatable
      implements Interfaces::RepositoryInfo
      implements Interfaces::FeatureFlaggable
      implements Interfaces::ForkPrWorkflowsPolicy

      global_id_field :id

      database_id_field

      field :network, Objects::RepositoryNetwork, method: :async_network, visibility: :internal, description: "The repository network.", null: true

      field :readme, Objects::RepositoryReadme, method: :async_preferred_readme, feature_flag: :pe_mobile, description: "The repository readme.", null: true do
        argument :ref_name, String, "The ref name used to return the associated readme.", required: false
      end

      field :template_repository, Repository,
            description: "The repository from which this repository was generated, if any.", null: true,
            method: :async_template_repository

      field :parent, Repository, description: "The repository parent, if this is a fork.", null: true

      def parent
        @object.async_parent.then do |parent|
          if parent&.disabled_at.nil?
            Loaders::ActiveRecord.load(::Repository, @object.parent_id, security_violation_behaviour: :nil)
          else
            nil
          end
        end
      end

      field :is_empty, Boolean, method: :async_empty?, description: "Returns whether or not this repository is empty.", null: false

      field :is_disabled, Boolean, method: :async_disabled?, description: "Returns whether or not this repository disabled.", null: false

      field :actions_plan_owner, Objects::ActionsPlanOwner, method: :async_actions_plan_owner, visibility: :internal, description: "Returns the entity used by Actions Runtime for limiting build concurrency.", null: true

      field :is_actions_usage_allowed, Boolean, visibility: :internal, description: "Returns whether or not an action can be run at this time.", null: false

      def is_actions_usage_allowed
        @object.async_owner.then do |owner|
          Billing::ActionsPermission.new(owner).usage_allowed?(public: @object.public?)
        end
      end

      field :is_actions_storage_allowed, Boolean, visibility: :internal, description: "Returns whether or not the given amount of bytes would go over storage limits.", null: false

      def is_actions_storage_allowed
        @object.async_owner.then do |owner|
          Billing::ActionsPermission.new(owner).storage_allowed?(public: @object.public?)
        end
      end

      # Used by Launch App to determine if Actions can run for this repo.
      field :is_actions_eligible, Boolean, visibility: :internal, description: "Repo's eligibility to use Actions", null: false

      def is_actions_eligible
        @object.async_owner.then do |owner|
          Billing::ActionsPermission.new(owner).allowed?(public: @object.public?)
        end
      end

      field :has_listable_action, Boolean, visibility: :internal, method: :listable_action?, null: false, description: "Returns whether or not the repository has an Action that is listable on the Marketplace."

      field :has_action_at_root, Boolean, visibility: :internal, null: false, description: "Returns whether or not the repository has an action YAML file in its root."

      def has_action_at_root
        @object.action_at_root.present?
      end

      field :listed_action, Objects::RepositoryAction, method: :async_listed_action, null: true, visibility: :internal, description: "If one exists, the Action listed on the Marketplace for the repository."

      field :community_profile, Objects::CommunityProfile, method: :async_community_profile, visibility: :internal, description: "Information about the repository's community engagement.", null: true

      field :forks, resolver: Resolvers::Repositories, description: "A list of direct forked repositories.", connection: true

      field :commit_comments, resolver: Resolvers::CommitComments, description: "A list of commit comments associated with the repository.", connection: true

      field :installed_apps, Connections.define(App, name: "InstalledApp", edge_type: Edges::InstalledApp, visibility: :internal), visibility: :internal, description: "A list of installed GitHub Apps", null: false, connection: true

      def installed_apps
        Loaders::IntegrationInstallation::RepositoryForViewer.load(@object, @context[:viewer])
      end

      field :installed_app_installations, Connections.define(Objects::IntegrationInstallation, name: "InstalledAppInstallations", visibility: :internal), visibility: :internal,
            description: "A list of GitHub App Installations", null: false, connection: true, areas_of_responsibility: :ecosystem_apps

      def installed_app_installations
        Loaders::IntegrationInstallation::RepositoryForViewer.load(@object, @context[:viewer])
      end

      field :contributors, Connections::RepositoryContributor, visibility: :internal,
            description: "A list of users who have contributed to this repository", null: false,
            connection: true

      def contributors
        Loaders::ActiveRecordAssociation.load(@object, :network).then do
          contributors_with_counts = @object.contributors(viewer: @context[:viewer])
          if contributors_with_counts.computed?
            # [['defunkt', 1], ['mojombo', 1]]
            contributors_array = contributors_with_counts.value
            contributor_objects = contributors_array.map do |(user, count)|
              Platform::Models::UserContribution.new(user, count)
            end
            ArrayWrapper.new(contributor_objects)
          else
            ArrayWrapper.new
          end
        end
      end

      field :community_contributors_collection, Objects::CommunityContributorsCollection, visibility: :internal,
            description: "A list of users who have contributed to this repository through dependences", null: false

      def community_contributors_collection
        Loaders::Dependencies.load_repository_dependencies({repository_id: @object.repository.id}).then do |result|
          result_values = if result.ok?
            result.value!
          else
            Failbot.report(result.error, app: "github-dependency-graph", repo_id: @object.id)
            {
                direct_dependencies: [],
                transitive_dependencies: [],
            }
          end

          ::CommunityContributor.new(
              viewer: @context[:viewer],
              direct_dependency_ids: result_values[:direct_dependencies],
              transitive_dependency_ids: result_values[:transitive_dependencies],
          )
        end
      end

      field :top_contributors, [User], visibility: :under_development,
            description: "Users who have made the most commits to this repository", null: false do
        argument :limit, Integer, "How many contributors to return.", required: false,
                 default_value: 5
      end

      def top_contributors(limit:)
        raise Errors::ArgumentLimit, "Only up to 100 contributors is supported." if limit > 100

        users = @object.top_contributors(limit: limit, viewer: @context[:viewer])
        ArrayWrapper.new(users)
      end

      field :deploy_keys, Connections.define(Objects::DeployKey), description: "A list of deploy keys that are on this repository.", null: false, connection: true

      def deploy_keys
        if @context[:permission].has_admin_resource_read_access?(resource: :deploy_keys, repo: @object)
          context[:permission].async_owner_if_org(object).then do |org|
            if context[:permission].access_allowed?(:list_repo_keys, repo: object, resource: object, current_org: org, allow_integrations: true, allow_user_via_integration: true)
              @object.async_public_keys.then do |public_keys|
                ArrayWrapper.new(public_keys)
              end
            else
              ::PublicKey.none
            end
          end
        else
          ::PublicKey.none
        end
      end

      field :admin_info, Objects::RepositoryAdminInfo, description: "Fields that are only visible to repo administrators.", null: true

      def admin_info
        Promise.all([@object.async_parent, @object.async_owner]).then do
          if @context[:viewer] && @object.adminable_by?(@context[:viewer])
            Models::RepositoryAdminInfo.new(@object)
          else
            nil
          end
        end
      end

      field :stafftools_info, Objects::RepositoryStafftoolsInfo, description: "Fields that are only visible to site admins.", null: true

      def stafftools_info
        if self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          Models::RepositoryStafftoolsInfo.new(@object)
        else
          nil
        end
      end

      field :interaction_ability, Objects::RepositoryInteractionAbility, description: "The interaction ability settings for this repository.", null: true

      def interaction_ability
        @object.async_can_set_interaction_limits?(context[:viewer]).then do |visible|
          if visible
            Platform::Models::RepositoryInteractionAbility.new(:repository, @object)
          else
            nil
          end
        end
      end

      field :plan_supports, Boolean, null: false, visibility: :under_development do
        description "Returns whether or not a repository's plan supports a feature."
        argument :feature, Enums::PlanFeature, "The plan feature to check", required: true
      end

      def plan_supports(feature:)
        Promise.all([@object.async_internal_repository, @object.async_business]).then do
          @object.async_plan_supports?(feature)
        end
      end

      field :plan_limit, Integer, null: false, feature_flag: :pe_mobile do
        description "Returns the limit for the repository's billing plan."
        argument :feature, Enums::PlanFeatureLimit, "The limit for the plan feature", required: true
      end

      def plan_limit(feature:)
        @object.async_internal_repository.then do
          @object.async_plan_limit(feature)
        end
      end

      field :viewer_is_plan_owner, Boolean, null: false, visibility: :internal, description: "Returns whether or not the current viewer is the repository's billing plan owner."

      def viewer_is_plan_owner
        return false unless context[:viewer]

        @object.async_plan_owner.then do |plan_owner|
          context[:viewer] == plan_owner
        end
      end

      field :deleted_issue, Objects::DeletedIssue, visibility: :internal, description: "Returns a single deleted issue from the current repository by number.", null: true do
        argument :number, Integer, "The number for the issue to be returned.", required: true
      end

      def deleted_issue(**arguments)
        @object.deleted_issues.find_by_number(arguments[:number])
      end

      field :issue_or_pull_request, Unions::IssueOrPullRequest, description: "Returns a single issue-like object from the current repository by number.", null: true do
        argument :number, Integer, "The number for the issue to be returned.", required: true
      end

      def issue_or_pull_request(**arguments)
        Loaders::IssueishByNumber.load(@object.id, arguments[:number]).then do |issueish|
          not_found_message = "Could not resolve to an issue or pull request with the number of #{arguments[:number]}."

          unless issueish
            raise Errors::NotFound, not_found_message
          end

          # IssueishByNumber loader doesn't do spam checks, so we do that here
          graphql_type_name = Platform::Helpers::NodeIdentification.type_name_from_object(issueish)
          @context[:permission].typed_can_see?(graphql_type_name, issueish).then do |readable|
            if readable
              issueish
            else
              raise Errors::NotFound, not_found_message
            end
          end
        end
      end

      field :issue, Objects::Issue, null: true do
        description "Returns a single issue from the current repository by number."
        argument :number, Integer, "The number for the issue to be returned.", required: true
      end

      def issue(number:)
        Loaders::IssueByNumber.load(@object.id, number).then do |issue|
          not_found_message = "Could not resolve to an Issue with the number of #{number}."

          if issue.nil? || issue.pull_request_id
            raise Errors::NotFound, not_found_message
          end

          # IssueByNumber loader doesn't do spam checks, so we do that here
          @context[:permission].typed_can_see?("Issue", issue).then do |readable|
            if readable
              issue
            else
              raise Errors::NotFound, not_found_message
            end
          end
        end
      end

      field :issues, resolver: Resolvers::Issues, description: "A list of issues that have been opened in the repository.", connection: true

      field :is_pinned, Boolean, visibility: :under_development, null: false,
            description: "Returns whether this repository is pinned to the profile of the specified repository owner." do
        argument :profile_owner_id, ID, "The ID of the owner of the profile you want to check.",
                 required: true
      end

      def is_pinned(profile_owner_id:)
        profile_owner = Helpers::NodeIdentification.
            typed_object_from_id([Interfaces::ProfileOwner], profile_owner_id, @context)
        profile_owner.pinned_repository?(@object)
      end

      field :pinned_issues, Connections.define(Objects::PinnedIssue), description: "A list of pinned issues for this repository.", null: true, connection: true

      def pinned_issues
        @object.async_pinned_issues.then do |pinned_issues|
          ArrayWrapper.new(pinned_issues)
        end
      end

      field :similar_issues, Connections.define(Objects::Issue), description: "A list of issues similar to a given query in the context of the repository.", null: false, connection: true, visibility: :under_development do
        argument :query, String, "The query to find similar issues.", required: true
      end

      def similar_issues(query:)
        @object.async_name_with_owner.then do |name_with_owner|
          # remove qualifiers from the string.
          # https://github.com/github/github/issues/99788
          sanitized_query = query.gsub(/@|:/, " ")
          phrase = "#{sanitized_query} repo:#{name_with_owner}"

          ::Search::Queries::SimilarIssueQuery.new \
            current_user: @context[:viewer],
            user_session: @context[:user_session],
            current_installation: @context[:installation],
            phrase: phrase,
            context: "graphql-related-issues"
        end
      end

      field :branch_protection_rules, Connections.define(Objects::BranchProtectionRule), minimum_accepted_scopes: ["public_repo"], description: "A list of branch protection rules for this repository.", null: false, connection: true

      def branch_protection_rules
        Promise.all([@object.async_business, @object.async_internal_repository]).then do
          has_access = @context[:permission].has_admin_resource_read_access?(resource: :protected_branches, repo: @object)
          plan_supports = @object.plan_supports?(:protected_branches)
          next StableArrayWrapper.new([]) unless has_access && plan_supports

          @object.async_protected_branches.then do |protected_branches|
            result = protected_branches.map do |protected_branch|
              Platform::Models::BranchProtectionRule.new(protected_branch)
            end

            StableArrayWrapper.new(result)
          end
        end
      end

      field :pull_request, Objects::PullRequest, description: "Returns a single pull request from the current repository by number.", null: true do
        argument :number, Integer, "The number for the pull request to be returned.", required: true
      end

      def pull_request(**arguments)
        Loaders::PullRequestByNumber.load(@object.id, arguments[:number]).then do |pull|
          if !pull || pull.hide_from_user?(@context[:viewer])
            raise Errors::NotFound, "Could not resolve to a PullRequest with the number of #{arguments[:number]}."
          end

          pull
        end
      end

      field :pull_requests, resolver: Resolvers::RepositoryPullRequests, description: "A list of pull requests that have been opened in the repository.", connection: true

      field :milestone, Milestone, description: "Returns a single milestone from the current repository by number.", null: true do
        argument :number, Integer, "The number for the milestone to be returned.", required: true
      end

      def milestone(**arguments)
        Loaders::MilestoneByNumber.load(@object.id, arguments[:number])
      end

      field :milestones, Connections.define(Objects::Milestone), description: "A list of milestones associated with the repository.", null: true, connection: true do
        argument :states, [Enums::MilestoneState], "Filter by the state of the milestones.", required: false
        argument :order_by, Inputs::MilestoneOrder, "Ordering options for milestones.", required: false
        argument :query, String, "Filters milestones with a query on the title", required: false
      end

      def milestones(**arguments)
        context[:permission].async_can_list_milestones?(@object).then do |can_list_milestones|
          if can_list_milestones
            milestones = @object.milestones

            if arguments[:states]
              milestones = milestones.where(state: arguments[:states])
            end

            if arguments[:query]
              query = ActiveRecord::Base.sanitize_sql_like(
                arguments[:query].to_s.strip.downcase
              )
              # Since title is a mediumblob column, we are not able to case-insensitive query on it using LIKE.
              # To fix that, first cast it to a UTF-8 string (whose collation is case-insensitive).
              milestones = milestones.where("CAST(title AS CHAR CHARACTER SET utf8mb4) LIKE ?", "%#{query}%") if query.present?
            end

            if order_by = arguments[:order_by]
              milestones = milestones.sorted_by(order_by[:field], order_by[:direction].downcase)
            end

            milestones.filter_spam_for(@context[:viewer])
          else # Not permitted to list milestones
            ::Milestone.none
          end
        end
      end

      field :label, Label, description: "Returns a single label by name", null: true do
        argument :name, String, "Label name", required: true
      end

      def label(**arguments)
        Loaders::LabelByName.load(@object.id, arguments[:name])
      end

      field :good_first_issue_label, Label, visibility: :internal, description: "Returns the label this repository uses to denote good issues for new users.", null: true

      field :help_wanted_label, Label, visibility: :internal, description: "Returns the label this repository uses to denote issues that need help.", null: true

      field :code_of_conduct, CodeOfConduct, description: "Returns the code of conduct for this repository", null: true

      def code_of_conduct
        @object.async_disabled_access_reason.then do
          if @object.detect_code_of_conduct
            @object.code_of_conduct
          else
            nil
          end
        end
      end

      field :watchers, resolver: Resolvers::Watchers, description: "A list of users watching the repository.", connection: true

      field :collaborators, Connections::RepositoryCollaborator, resolver: Resolvers::RepositoryCollaborators, description: "A list of collaborators associated with the repository.", null: true, connection: false do
        has_connection_arguments
      end

      field :labels, resolver: Resolvers::Labels, description: "A list of labels associated with the repository." do
        argument :query, String, "If provided, searches labels by name and description.", required: false
      end

      field :languages, Connections::Language, description: "A list containing a breakdown of the language composition of the repository.", null: true, connection: true do
        argument :order_by, Inputs::LanguageOrder, "Order for connection", required: false
      end

      def languages(order_by: nil)
        context[:permission].async_owner_if_org(@object).then do |org|
          if context[:permission].access_allowed?(:list_languages, resource: @object, current_repo: @object, current_org: org, allow_integrations: true, allow_user_via_integration: true)
            scope = ::Language.where(repository_id: @object.id)

            if order_by
              scope = scope.order("languages.#{order_by[:field]} #{order_by[:direction]}")
            end

            scope
          else
            ::Language.none
          end
        end

      end

      field :viewer_permission, Enums::RepositoryPermission, description: "The users permission level on the repository. Will return null if authenticated as an GitHub App.", null: true

      def viewer_permission
        if @context[:viewer]&.user?
          @object.async_organization.then do
            @object.async_parent.then do
              permission = @object.access_level_for(@context[:viewer])

              permission ? permission.to_s : nil
            end
          end
        end
      end

      field :viewer_can_see_commenter_full_name, Boolean, description: "Can the viewer see comment author's full name", null: false, visibility: :under_development

      def viewer_can_see_commenter_full_name
        return false if !@context[:viewer]&.user?
        return false unless @object.private?

        @object.async_owner.then do |owner|
          if owner.organization?
            @object.async_organization.then do |org|
              owner.async_business.then do
                org.display_commenter_full_name_for_repo?(visibility: @object.visibility.to_sym, viewer: @context[:viewer])
              end
            end
          else
            false
          end
        end
      end

      field :primary_language, Language, method: :async_primary_language, description: "The primary language of the repository's code.", null: true

      field :has_heads, Boolean, visibility: :internal, description: "Returns a boolean indicating if the repository has branches or not.", null: false

      def has_heads
        @object.async_network.then do
          @object.heads.count > 0
        end
      end

      field :branches_including_default_first, [String], visibility: :internal, description: "Return the list of branches in the repository including the default one (first).", null: false

      def branches_including_default_first
        @object.async_network.then do
          @object.heads.refs_with_default_first.map do |branch|
            branch.name.force_encoding("UTF-8")
          end
        end
      end

      field :ref, Objects::Ref, description: "Fetch a given ref from the repository", null: true do
        argument :qualified_name, String, "The ref to retrieve. Fully qualified matches are checked in order (`refs/heads/master`) before falling back onto checks for short name matches (`master`).", required: true
      end

      def ref(**arguments)
        @object.async_network.then do
          @object.refs.find(arguments[:qualified_name])
        end
      end

      field :refs, Connections::Ref, resolver: Resolvers::Refs, description: "Fetch a list of refs from the repository", null: true, connection: true do
        argument :ref_prefix, String, "A ref name prefix like `refs/heads/`, `refs/tags/`, etc.", required: true
        argument :direction, Enums::OrderDirection, "DEPRECATED: use orderBy. The ordering direction.", required: false
        argument :order_by, Inputs::RefOrder, "Ordering options for refs returned from the connection.", required: false
      end

      field :commit_revision, Objects::CommitRevision, visibility: :internal, description: <<~MD, null: true do
          Find commit by extended SHA-1 syntax.

          Returns nothing if revision syntax is invalid.
      MD

        argument :name, String, <<~MD, required: true
          The name that can be resolved to a commit.

          See CommitRevision.name for examples.
        MD
      end

      def commit_revision(**arguments)
        Models::CommitRevision.find(@object, arguments[:name])
      end

      field :object, Interfaces::GitObject, description: "A Git object in the repository", null: true, resolver_method: :git_object do
        argument :oid, Scalars::GitObjectID, "The Git object ID", required: false

        argument :expression, String, "A Git revision expression suitable for rev-parse", required: false
      end

      def git_object(**arguments)
        context.scoped_merge!(root_commit_arguments: arguments)
        if arguments[:oid]
          oid = arguments[:oid]
          Platform::Loaders::GitObject.load(@object, oid)
        elsif arguments[:expression]
          # TODO - GitRPC doesn't have any batch rev-parse functionality at
          # the moment. When we implement that this should move to a loader.
          @object.async_network.then {
            if oid = @object.ref_to_sha(arguments[:expression])
              path_prefix = extract_path_prefix_from_expression(arguments[:expression])
              Platform::Loaders::GitObject.load(@object, oid, path_prefix: path_prefix)
            end
          }
        end
      end

      field :mentionable_users, resolver: Resolvers::MentionableUsers, description: "A list of Users that can be mentioned in the context of the repository.", connection: true

      field :assignable_users, resolver: Resolvers::AssignableUsers, description: "A list of users that can be assigned to issues in this repository.", connection: true, numeric_pagination_enabled: true

      field :vulnerability_alerts, Connections.define(Objects::RepositoryVulnerabilityAlert), description: "A list of vulnerability alerts that are on this repository.", null: true, connection: true

      def vulnerability_alerts
        Promise.all([@object.async_parent, @object.async_owner]).then do
          if @context[:permission].can_read_vulnerability_alerts?(repo: @object)
            @object.repository_vulnerability_alerts.scoped
          else
            ::RepositoryVulnerabilityAlert.none
          end
        end
      end

      field :dependency_graph_manifests, Connections.define(Objects::DependencyGraphManifest), null: true,
            connection: false, # disable built-in connection wrapper because we're paginating ourselves.
            description: "A list of dependency manifests contained in the repository",
            areas_of_responsibility: :dependency_graph,
            extras: [:execution_errors] do
        has_connection_arguments
        argument :with_dependencies, Boolean, "Flag to scope to only manifests with dependencies", required: false
        argument :include_dependencies, Boolean, "Flag to indicate that dependencies should be eagerly loaded", required: false, default_value: true, visibility: :internal
        argument :dependencies_first, Integer, "Number of dependencies to fetch", required: false
        argument :dependencies_after, String, "Cursor to paginate dependencies", required: false
        argument :dependencies_prefers, [String, null: true], "Dependencies to return first", required: false, visibility: :internal
        argument :package_manager, String, "Package manager to scope to", required: false, visibility: :internal
        argument :package_name, String, "Package name to scope to", required: false, visibility: :internal
      end

      def dependency_graph_manifests(execution_errors:, **arguments)
        @context[:permission].async_can_list_dependency_graphs?(@object).then do |can_list_dependency_graphs|
          if !can_list_dependency_graphs
            # Not permitted to list dependency_graphs, return empty
            return ConnectionWrappers::ArrayWrapper.new(ArrayWrapper.new, arguments, parent: object, context: context)
          end

          @object.async_network.then do
            @object.async_owner.then do
              manifest_filter = {
                  repository_id: @object.id,
                  first: arguments[:first],
                  with_dependencies: !!arguments[:with_dependencies],
                  preview: @object.dependency_graph_preview?,
              }
              if arguments[:package_name]
                manifest_filter[:package_name] = arguments[:package_name]
              end
              if arguments[:package_manager]
                manifest_filter[:package_manager] = arguments[:package_manager]
              end

              Loaders::Dependencies.load_manifests(@object, {
                  manifest_filter: manifest_filter,
                  dependencies_filter: {
                      first: arguments[:dependencies_first],
                      after: arguments[:dependencies_after],
                      prefer: arguments[:dependencies_prefers],
                  },
                  include_dependencies: !!arguments[:include_dependencies],
              }).then do |result|
                items = result
                            .map { |value| ArrayWrapper.new(value) }
                            .value { |error|
                              message = case error
                              when ::Repository::ManifestsNotDetectedError
                                "loading"
                              when ::DependencyGraph::Client::TimeoutError
                                "timedout"
                              when ::DependencyGraph::Client::ServiceUnavailableError, ::DependencyGraph::Client::ApiError, StandardError
                                "unavailable"
                              end
                              execution_errors.add(message)
                              ArrayWrapper.new
                            }
                # Apply a connection wrapper manually
                ConnectionWrappers::ArrayWrapper.new(
                    items,
                    arguments,
                    parent: object,
                    context: context,
                )
              end
            end
          end
        end
      end

      field :dependency_graph_packages, Connections.define(Objects::DependencyGraphPackage), null: true,
            visibility: :internal,
            description: "A list of packages contained in the repository",
            areas_of_responsibility: :dependency_graph,
            connection: false, # Opt out of this since we're implementing the connection directly
            extras: [:execution_errors] do
        has_connection_arguments
        argument :package_id, String, "The internal dependency graph package ID", required: false
        argument :include_dependents, Boolean, "Flag to indicate that dependents should be eagerly loaded", required: false
        argument :include_dependent_counts, Boolean, "Flag to indicate that dependent count should be loaded", required: false
        argument :dependent_type, Enums::DependencyGraphDependentType, "The type of dependent to query", required: false
        argument :dependents_first, Integer, "Number of dependents to fetch", required: false
        argument :dependents_after, String, "Cursor to paginate dependents", required: false
        argument :dependents_last, Integer, "Number of dependents to fetch", required: false
        argument :dependents_before, String, "Cursor to paginate dependents", required: false
        argument :debug, Boolean, "Debug packages data", required: false
      end

      def dependency_graph_packages(execution_errors:, **arguments)
        @object.async_owner.then do
          Loaders::Dependencies.load_packages({
                                                  package_filter: {
                                                      repository_id: @object.id,
                                                      package_id: arguments[:package_id],
                                                      first: arguments[:first],
                                                      debug: !!arguments[:debug],
                                                      preview: @object.dependency_graph_preview?,
                                                  },
                                                  dependents_filter: {
                                                      type: arguments[:dependent_type],
                                                      first: arguments[:dependents_first],
                                                      after: arguments[:dependents_after],
                                                      last: arguments[:dependents_last],
                                                      before: arguments[:dependents_before],
                                                  },
                                                  include_dependents: !!arguments[:include_dependents],
                                                  include_dependent_counts: !!arguments[:include_dependent_counts],
                                              }).then do |result|
            array_wrapper = result
                                .map { |value| ArrayWrapper.new(value) }
                                .value { |error|
                                  execution_errors.add(error.message)
                                  ArrayWrapper.new
                                }
            # Manually instantiate a connection since we skipped
            # the built-in wrapping with `connection: false` above
            Platform::ConnectionWrappers::ArrayWrapper.new(
                array_wrapper,
                arguments,
                parent: object,
                context: context,
            )
          end
        end
      end

      field :primary_page_deployment, Objects::PageDeployment, visibility: :internal, description: "The GitHub Pages site associated with the primary Pages source branch.", null: true

      def primary_page_deployment
        @object.async_page.then do |page|
          next unless page
          page.async_primary_deployment
        end
      end

      field :page_deployments, Connections.define(PageDeployment, visibility: :internal), visibility: :internal, description: "The GitHub Pages sites associated with this repository.", null: false, connection: true

      def page_deployments
        @object.async_page.then do |page|
          # No page? No problem. Return an empty array.
          next ArrayWrapper.new unless page
          page.async_deployments.then do |deployments|
            @object.page.deployments.scoped
          end
        end
      end

      field :gh_pages_error, Boolean, visibility: :internal, null: true, description: "The build status for the last page deployment"

      def gh_pages_error
        @object.async_page.then do |page|
          page&.builds&.first&.error?
        end
      end

      field :gh_pages_error_message, String, visibility: :internal, null: true, description: "The error message for the last page build"

      def gh_pages_error_message
        @object.async_page.then do |page|
          next unless page && page.builds.any?
          page.builds.first.try(:error)
        end
      end

      field :has_page, Boolean, description: "does this repo have a page", null: false, visibility: :internal

      def has_page
        @object.async_page.then do |page|
          !!page
        end
      end

      field :page_source, String, description: "source for a Repository's page", null: true, visibility: :internal

      def page_source
        @object.async_page.then do |page|
          next unless page
          page.source_branch.force_encoding("UTF-8")
        end
      end

      field :page_source_directory, String, description: "source directory for a Repository's page", null: true, visibility: :internal

      def page_source_directory
        @object.async_page.then do |page|
          next unless page
          page.source_dir
        end
      end

      field :is_user_pages_repo, Boolean, description: "is this a user pages repository", visibility: :internal, null: false, method: :async_is_user_pages_repo?

      field :cname_error, String, description: "The cname error for the associated page build", visibility: :internal, null: true

      def cname_error
        @object.async_page.then do |page|
          next unless page
          page.cname_error(page.cname)
        end
      end

      field :gh_pages_url, Scalars::URI, description: "the gh pages URL for this repo", visibility: :internal, null: true, method: :async_gh_pages_url

      url_fields prefix: :network, visibility: :under_development, description: "The HTTP URL listing the repository's network" do |repository|
        Loaders::ActiveRecord.load(::User, repository.owner_id).then do |user|
          template = Addressable::Template.new("/{user}/{repo}/network/members")
          template.expand user: user.login, repo: repository.name
        end
      end

      url_fields prefix: :new_project, visibility: :internal, description: "The HTTP URL to create new projects" do |repository|
        Loaders::ActiveRecord.load(::User, repository.owner_id).then do |user|
          template = Addressable::Template.new("/{user}/{repo}/projects/new")
          template.expand user: user.login, repo: repository.name
        end
      end

      url_fields prefix: :participation_graph, visibility: :internal, description: "The HTTP URL to get a participation graph" do |repository|
        Loaders::ActiveRecord.load(::User, repository.owner_id).then do |user|
          template = Addressable::Template.new("/{user}/{repo}/graphs/participation")
          template.expand user: user.login, repo: repository.name
        end
      end

      url_fields prefix: :projects, description: "The HTTP URL listing the repository's projects" do |repository|
        Loaders::ActiveRecord.load(::User, repository.owner_id).then do |user|
          template = Addressable::Template.new("/{user}/{repo}/projects")
          template.expand user: user.login, repo: repository.name
        end
      end

      url_fields prefix: :stargazers, visibility: :under_development, description: "The HTTP URL listing users who have starred the repository" do |repository|
        Loaders::ActiveRecord.load(::User, repository.owner_id).then do |user|
          template = Addressable::Template.new("/{user}/{repo}/stargazers")
          template.expand user: user.login, repo: repository.name
        end
      end

      url_fields prefix: :releases, visibility: :under_development, description: "The HTTP URL listing releases for this repository" do |repository|
        Loaders::ActiveRecord.load(::User, repository.owner_id).then do |user|
          template = Addressable::Template.new("/{user}/{repo}/releases")
          template.expand user: user.login, repo: repository.name
        end
      end

      field :releases, Connections.define(Objects::Release), description: "List of releases which are dependent on this repository.", null: false, connection: true do
        argument :order_by, Inputs::ReleaseOrder, "Order for connection", required: false
      end

      def releases(**arguments)
        context[:permission].async_can_list_releases?(@object).then do |can_list_releases|
          if can_list_releases
            @object.async_organization.then do
              @object.async_parent.then do

                perm = @object.access_level_for(@context[:viewer])

                scope = @object.releases.scoped

                if order_by = arguments[:order_by]
                  scope = scope.order("releases.#{order_by[:field]} #{order_by[:direction]}")
                end

                unless perm == :admin || perm == :write
                  scope = scope.where.not(state: "draft")
                end

                scope
              end
            end
          else # Not permitted to list releases
            ::Release.none
          end
        end
      end

      field :release, Objects::Release, description: "Lookup a single release given various criteria.", null: true do
        argument :tag_name, String, "The name of the Tag the Release was created from", required: true
      end

      def release(**arguments)
        @object.releases.find_by_tag_name(arguments[:tag_name])
      end

      field :default_merge_queue, Objects::MergeQueue, description: "The merge queue for this repository", null: true, visibility: :internal, method: :async_default_merge_queue

      field :repository_topics, Connections.define(Objects::RepositoryTopic), numeric_pagination_enabled: true, description: "A list of applied repository-topic associations for this repository.", null: false, connection: true

      def repository_topics(**arguments)
        @object.applied_repository_topics.scoped
      end

      field :disk_usage, Integer, description: "The number of kilobytes this repository occupies on disk.", null: true

      def disk_usage
        @context[:permission].async_can_get_full_repo?(@object).then do |can_get_full_repo|
          if can_get_full_repo
            @object.disk_usage
          else
            nil
          end
        end
      end

      field :viewer_can_update_topics, Boolean, description: "Indicates whether the viewer can update the topics of this repository.", null: false

      def viewer_can_update_topics
        Promise.all([@object.async_parent, @object.async_owner]).then do
          !@object.locked_on_migration? &&
              !@object.archived? &&
              @object.adminable_by?(@context[:viewer])
        end
      end

      field :viewer_can_administer, Boolean, description: "Indicates whether the viewer has admin permissions on this repository.", null: false

      def viewer_can_administer
        @object.async_adminable_by?(@context[:viewer])
      end

      field :viewer_can_interact, Boolean, visibility: :under_development,
            description: "Indicates whether the current user can interact according to repository interaction limits.", null: false

      def viewer_can_interact
        return false unless @context[:viewer]
        ::User::InteractionAbility.async_interaction_allowed?(@context[:viewer].id, @object)
      end

      field :viewer_can_push, Boolean, visibility: :under_development,
            description: "Indicates whether the current user has push permissions on this repository.", null: false

      def viewer_can_push
        return false unless @context[:viewer]
        @object.async_pushable_by?(@context[:viewer])
      end

      field :viewer_blocked_by_owner, Boolean, visibility: :internal,
            description: "Indicates whether the current user has been blocked by the repository owner.", null: false

      def viewer_blocked_by_owner
        return false if !@context[:viewer] || @context[:viewer].id == @object.owner_id
        @object.async_owner.then { |owner| @context[:viewer].async_blocked_by?(owner) }
      end

      field :exported_to_url, Scalars::URI, visibility: :internal, description: "The URL to where this repository has been exported to or null if the repository is not locked for migration.", null: true

      def exported_to_url
        if @object.locked_on_migration?
          @object.async_configuration_owners.then do
            @object.get_repository_exported_to_url
          end
        end
      end

      field :network_present, Boolean, visibility: :internal, description: "Whether or not this repository's network is present on disk.", null: false

      def network_present
        @object.async_network.then {
          @object.network.present?
        }
      end

      field :default_branch_ref, Objects::Ref, description: "The Ref associated with the repository's default branch.", null: true, method: :async_default_branch_ref

      field :deployments, Connections.define(Objects::Deployment), description: "Deployments associated with the repository", null: false, connection: true, resolver: Resolvers::RepositoryDeployments

      field :squash_merge_allowed, Boolean, method: :squash_merge_allowed?, description: "Whether or not squash-merging is enabled on this repository.", null: false
      field :rebase_merge_allowed, Boolean, method: :rebase_merge_allowed?, description: "Whether or not rebase-merging is enabled on this repository.", null: false
      field :merge_commit_allowed, Boolean, method: :merge_commit_allowed?, description: "Whether or not PRs are merged with a merge commit on this repository.", null: false
      field :delete_branch_on_merge, Boolean, method: :async_delete_branch_on_merge?, description: "Whether or not branches are automatically deleted when merged in this repository.", null: false

      url_fields prefix: :clone, description: "The URL to clone this repository", visibility: :internal do |repository|
        Loaders::ActiveRecord.load(::User, repository.owner_id).then do |user|
          Addressable::URI.parse(repository.clone_url)
        end
      end

      field :ssh_url, Scalars::GitSSHRemote, description: "The SSH URL to clone this repository", null: false

      def ssh_url
        @object.async_owner.then do |owner|
          owner.async_business.then do
            @object.ssh_url
          end
        end
      end

      field :git_url, Scalars::URI, visibility: :under_development, description: "The Git URL to clone this repository", null: false

      def git_url
        @object.async_owner.then do |user|
          Addressable::URI.parse(@object.gitweb_url)
        end
      end

      url_fields prefix: :svn, description: "The SVN URL to clone this repository", visibility: :internal do |repository|
        repository.async_owner.then do |user|
          Addressable::URI.parse(repository.svn_url)
        end
      end

      field :viewer_can_toggle_wiki, Boolean, visibility: :under_development, description: "If the viewer has permissions to toggle the wiki", null: false

      def viewer_can_toggle_wiki
        @object.async_can_toggle_wiki?(context[:viewer])
      end

      field :viewer_can_toggle_projects, Boolean, visibility: :under_development, description: "If the viewer has permissions to toggle the Projects feature on this repository", null: false

      def viewer_can_toggle_projects
        @object.async_can_toggle_projects?(context[:viewer])
      end

      field :can_enable_projects, Boolean, visibility: :under_development, description: "If Projects can be enabled for this repository", null: false, method: :async_can_enable_projects?

      field :viewer_can_toggle_page_settings, Boolean, visibility: :internal, description: "If the viewer has permissions to toggle the page settings", null: false

      def viewer_can_toggle_page_settings
        @object.async_can_toggle_page_settings?(context[:viewer])
      end

      field :viewer_can_set_interaction_limits, Boolean, visibility: :under_development, description: "If the viewer has permissions to set interaction limits on this repository", null: false

      def viewer_can_set_interaction_limits
        @object.async_can_set_interaction_limits?(context[:viewer])
      end

      field :viewer_can_manage_webhooks, Boolean, visibility: :under_development, description: "If the viewer has permissions to manage webhooks on this repository", null: false

      def viewer_can_manage_webhooks
        @object.async_can_manage_webhooks?(context[:viewer])
      end

      field :viewer_can_manage_deploy_keys, Boolean, visibility: :under_development, description: "If the viewer has permissions to manage deploy keys for this repository", null: false

      def viewer_can_manage_deploy_keys
        @object.async_can_manage_deploy_keys?(context[:viewer])
      end

      field :repo_type_icon, String, visibility: :internal, description: "The name of the octicon to use for this repository", null: false

      def repo_type_icon
        @object.async_mirror.then do |mirror|
          @object.repo_type_icon
        end
      end

      field :viewer_can_toggle_merge_types, Boolean, visibility: :internal, description: "If the viewer has permissions to toggle the merge settings", null: false

      def viewer_can_toggle_merge_types
        @object.async_can_toggle_merge_settings?(context[:viewer])
      end

      field :viewer_can_set_social_preview, Boolean, visibility: :internal, description: "If the viewer has permissions to set the Social preview", null: false

      def viewer_can_set_social_preview
        @object.async_can_set_social_preview?(context[:viewer])
      end

      # NOTE: This field should never be made public. The information it provides is accessible
      #  via the Commit's history connection. Currently that connection is uncached however,
      #  so until we add caching to list_revision_history_multiple we need to use this method
      #  instead as the additional traffic would be rather high.
      field :latest_commit, Objects::Commit, visibility: :internal, description: "The latest commit for the given path on the given ref.", null: true do
        argument :path, String, "The path to search for the latest commit. Defaults to the root.", required: false
        argument :ref_name, String, "The ref to search for the latest commit. Defaults to the default branch for the repository.", required: false
      end

      def latest_commit(ref_name: nil, path: nil)
        @object.async_last_touched(ref_name, path)
      end

      def self.load_from_global_id(id)
        Loaders::ActiveRecord.load(::Repository, id.to_i, security_violation_behaviour: :nil)
      end

      def self.load_from_params(params)
        Objects::User.load_from_params(user_id: params[:user_id]).then do |user|
          user && user.find_repo_by_name(params[:repository])
        end
      end

      field :organization, Objects::Organization, visibility: :under_development, description: "The organization this repository belongs to.", null: true, method: :async_organization

      field :organization_database_id, Integer, visibility: :internal, description: "The database id of the organization this repository belongs to.", null: true, method: :organization_id

      field :temp_clone_token, String, minimum_accepted_scopes: ["public_repo"], description: "Temporary authentication token for cloning this repository.", null: true

      def temp_clone_token
        @context[:permission].async_can_get_repo_temp_clone_token?(@object).then do |can_get_repo_temp_clone_token|
          if can_get_repo_temp_clone_token && (current_viewer = @context[:viewer]) # @context[:viewer] can't be nil
            @object.temp_clone_token(current_viewer)
          else
            nil
          end
        end
      end

      field :used_by_enabled, Boolean, description: "Is the Used By button displayed on the repository", null: true, visibility: :internal

      def used_by_enabled
        @object.async_configuration_owners.then do
          @object.used_by_enabled?
        end
      end

      field :actions_filter_diff, ActionsFilterDiff, description: "Diff required for actions path filtering", null: true, visibility: :internal do
        argument :head_sha, Scalars::GitObjectID, "The head sha for the comparison", required: true
        argument :base_sha, Scalars::GitObjectID, "The base sha for the comparison", required: true
        argument :ref, String, "The full ref of the head, e.g refs/heads/topic-branch", required: false
        argument :pull_request, ID, "The PR related to the compare", required: false
      end

      def actions_filter_diff(**args)
        ActionsFilterDiff.generate_async(@object, **args)
      end

      field :used_by_package_id, String, description: "Dependency Graph Package ID specified by user for display in Used By button", null: true, visibility: :internal

      def used_by_package_id
        @object.async_configuration_owners.then do
          @object.used_by_package_id
        end
      end

      field :abuse_reported_to_maintainer, Connections.define(Objects::AbuseReport), description: "The abuse reports made to the maintainers of this repository.", connection: true, null: false do
        argument :order_by, Inputs::AbuseReportOrder, "Ordering options for abuse reports returned from the connection.", required: false,
          default_value: { field: "created_at", direction: "DESC" }
        argument :filter, Enums::AbuseReportFilter, "Filter abuse reports by whether they've been marked as resolved.", required: false,
          default_value: "all"
      end

      def abuse_reported_to_maintainer(order_by:, filter:)
        scope = @object.abuse_reported_to_maintainer

        if filter == "resolved"
          scope = scope.where(resolved: true)
        elsif filter == "unresolved"
          scope = scope.where(resolved: false)
        end

        table = scope.table_name

        scope = scope.order("#{table}.#{order_by[:field]} #{order_by[:direction]}")
        scope
      end

      field :commit_is_in_branch_or_tag, Boolean, visibility: :internal, description: "Whether the commit is reachable in this repository.", null: true do
        argument :oid, Scalars::GitObjectID, "The commit's Git object ID", required: true
      end

      def commit_is_in_branch_or_tag(**arguments)
        @object.is_commit_in_branch_or_tag?(arguments[:oid])
      end

      field :funding_links, [Objects::FundingLink],
        description: "The funding links for this repository",
        null: false

      def funding_links
        @object.async_has_funding_file?.then do |has_funding|
          next [] unless has_funding
          results = []

          @object.funding_links.validated_config.each do |key, value|
            platform = ::FundingPlatforms::ALL[key.to_sym]

            Array(value).each do |url|
              results << Platform::Models::FundingLink.new(
                repository: @object,
                platform: platform.key,
                url: "#{platform.url}#{url}",
              )
            end
          end

          results
        end
      end

      field :is_user_configuration_repository, Boolean,
        feature_flag: :pe_mobile,
        description: "Is this repository a user configuration repository?",
        null: false,
        method: :async_user_configuration_repository?

      field :autocompleted_topic_names, [String, null: true], visibility: :internal, description: "A list of suggested topic names matching the given query for this repository.", null: false do
        argument :query, String, "String to match topic suggestions.", required: false
      end

      def autocompleted_topic_names(**arguments)
        @object.async_can_manage_topics?(@context[:viewer]).then do |can_manage_topics|
          next [] unless can_manage_topics

          query = arguments[:query]

          suggestions = []
          queried_suggestions = []

          if @object.topic_suggestions_enabled?
            suggestions = @object.suggested_topic_names || []
          end

          if query.present?
            suggestions.select! { |name| name.start_with?(query) }
            queried_suggestions = ::Topic.suggestions_for_autocomplete(query: query, limit: 10).pluck(:name)
          end

          suggestions.concat(queried_suggestions)

          associated_topic_ids = ::Topic.where(id: ::RepositoryTopic.applied_to(
            repository_ids: [@object.id],
          ).pluck(:topic_id))
          associated_topics = ::Topic.where(id: associated_topic_ids).pluck(:name)

          suggestions.uniq - associated_topics
        end
      end

      field :suggested_topic_names, [String, null: true], visibility: :internal, description: "A list of machine learning-suggested topic names for this repository.", null: false do
        argument :first, Integer, "Returns the first _n_ names from the topic suggestions.", required: false
      end

      def suggested_topic_names(**arguments)
        @object.async_can_manage_topics?(@context[:viewer]).then do |can_manage_topics|
          next [] unless can_manage_topics

          topic_names = @object.suggested_topic_names

          if topic_names
            if arguments[:first]
              topic_names.take(arguments[:first])
            else
              topic_names
            end
          else
            []
          end
        end
      end

      field :topic_suggestions_enabled, Boolean, visibility: :internal, description: "Returns a Boolean indicating whether machine learning-suggested topics are supported for this repository.", null: false

      def topic_suggestions_enabled
        @object.topic_suggestions_enabled?
      end

      field :issue_templates, [IssueTemplate], feature_flag: :pe_mobile, description: "Returns a list of issue templates associated to the repository", null: true

      def issue_templates
        @object.async_preferred_issue_templates.then do |templates|
          templates.valid_templates
        end
      end

      field :issue_comment_templates, [IssueCommentTemplate], feature_flag: :structured_issue_comment_templates, description: "Returns a list of valid issue comment templates", visibility: :under_development, null: false

      def issue_comment_templates
        return [] unless GitHub.flipper[:structured_issue_comment_templates].enabled?(@object)
        @object.issue_comment_templates.valid_templates
      end

      field :contact_links, [RepositoryContactLink], feature_flag: :pe_mobile, description: "Returns a list of contact links associated to the repository", null: true

      def contact_links
        @object.async_preferred_issue_templates.then do |templates|
          config = templates.issue_template_config
          config.contact_links
        end
      end

      field :is_blank_issues_enabled, Boolean, feature_flag: :pe_mobile, description: "Returns true if blank issue creation is allowed", null: false

      def is_blank_issues_enabled
        @object.async_preferred_issue_templates.then do |templates|
          config = templates.issue_template_config
          config.blank_issues_enabled?
        end
      end

      field :is_security_policy_enabled, Boolean, feature_flag: :pe_mobile, description: "Returns true if this repository has a security policy", null: true

      def is_security_policy_enabled
        @object.security_policy.exists?
      end

      field :security_policy_url, Scalars::URI, feature_flag: :pe_mobile, description: "The security policy URL.", null: true

      def security_policy_url
        return unless @object.security_policy.exists?

        Loaders::ActiveRecord.load(::User, @object.owner_id).then do |user|
          template = Addressable::Template.new("#{GitHub.url}/{user}/{repo}/security/policy")
          template.expand user: user.login, repo: @object.name
        end
      end

      field :submodules, Connections.define(Objects::Submodule), description: "Returns a list of all submodules in this repository parsed from the .gitmodules file as of the default branch's HEAD commit.", null: false, connection: true

      def submodules
        @object.async_default_branch_ref.then do |ref|
          if !ref
            # async_default_branch_ref returned nil, just return an empty array.
            ArrayWrapper.new([])
          else
            @object.async_submodules(ref.target_oid).then do |submodules|
              ArrayWrapper.new(submodules.values)
            end
          end
        end
      end

      field :import, Objects::Import, null: true, method: :async_import, feature_flag: :import_api, description: "Returns the import associated with this repository"

      private

      # <rev>:<path> but NOT :/<commit text matcher>
      COLON_WITH_PATH = %r{:((?!/).*)\Z}

      def extract_path_prefix_from_expression(expression)
        if match = COLON_WITH_PATH.match(expression)
          # This expression fetches a tree using the `master:path/to/subtree` syntax
          return match[1]
        end
      end
    end
  end
end
