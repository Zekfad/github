# frozen_string_literal: true

module Platform
  module Objects
    class MergeQueueEntry < Platform::Objects::Base
      description "Entries in a MergeQueue"

      implements Platform::Interfaces::Node
      global_id_field :id
      visibility :internal
      areas_of_responsibility :pull_requests

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, deployment)
        permission.async_repo_and_org_owner(deployment).then do |repo, org|
          permission.access_allowed?(:read_deployment, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, deployment)
        permission.belongs_to_repository(deployment)
      end

      scopeless_tokens_as_minimum

      field :pull_request, Objects::PullRequest, description: "The pull request that will be added to a merge group", null: true, method: :async_pull_request
      field :isSolo, Boolean, description: "Does this pull request need to be deployed on its own", null: false, method: :solo?
    end
  end
end
