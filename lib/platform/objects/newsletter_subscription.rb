# frozen_string_literal: true

module Platform
  module Objects
    class NewsletterSubscription < Platform::Objects::Base
      description "A user's subscription to a newsletter."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, subscription)
        subscription.async_user.then do |user|
          permission.access_allowed?(:read_user_notification_settings, resource: user, current_repo: nil, current_org: nil, allow_integrations: false, allow_user_via_integration: false)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        if permission.viewer.present?
          permission.viewer.id == object.user_id
        else
          true # Internal requests from background jobs
        end
      end

      visibility :internal

      minimum_accepted_scopes ["repo"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :is_active, Boolean, "Whether the subscription is active.", method: :active?, null: false
    end
  end
end
