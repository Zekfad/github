# frozen_string_literal: true

module Platform
  module Objects
    class HeadRefDeletedEvent < Platform::Objects::Base
      model_name "IssueEvent"
      description "Represents a 'head_ref_deleted' event on a given pull request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, head_ref_deleted_event)
        permission.belongs_to_issue_event(head_ref_deleted_event)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_issue(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      implements Interfaces::TimelineEvent

      implements Interfaces::PerformableViaApp

      global_id_field :id

      field :pull_request, Objects::PullRequest, "PullRequest referenced by event.", method: :async_issue_or_pull_request, null: false

      field :head_ref_name, String, description: "Identifies the name of the Ref associated with the `head_ref_deleted` event.", null: false

      def head_ref_name
        @object.async_issue.then { |issue|
          issue.async_pull_request.then { |pull_request|
            pull_request.head_ref.force_encoding("utf-8")
          }
        }
      end

      field :head_ref, Ref, description: "Identifies the Ref associated with the `head_ref_deleted` event.", null: true

      def head_ref
        @object.async_issue.then { |issue|
          issue.async_pull_request.then { |pull_request|
            pull_request.async_head_repository.then { |repository|
              repository.async_network.then {
                repository.heads.find(pull_request.head_ref)
              }
            }
          }
        }
      end
    end
  end
end
