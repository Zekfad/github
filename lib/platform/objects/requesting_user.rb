# frozen_string_literal: true

module Platform
  module Objects
    class RequestingUser < Platform::Objects::Base
      include Helpers::Newsies

      description "The currently authenticated user making the request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, requesting_user)
        permission.access_allowed?(:v4_read_user_private, user: permission.viewer, resource: requesting_user.user, current_repo: nil, current_org: nil, allow_integration: false, allow_user_via_integration: false, allow_integrations: false)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.user == permission.viewer
      end

      visibility :under_development
      areas_of_responsibility :ecosystem_api

      minimum_accepted_scopes ["read:user"]

      field :user, Objects::User, description: "The underlying user object.", null: true, method: :user

      field :plan, Objects::Plan, description: "The current user's billing plan.", null: true

      field :teams, Connections.define(Objects::Team), visibility: :internal, description: "A list of the teams the current viewer belongs to across organizations.", null: false, connection: true do
        argument :order_by, Inputs::TeamOrder, "Ordering options for teams returned from the connection", required: false
      end

      def teams(**arguments)
        # Join to organizations so we exclude teams whose organizations have been deleted:
        scope = @object.teams.joins(:organization)

        if arguments[:order_by]
          order = "#{arguments[:order_by][:field]} #{arguments[:order_by][:direction]}"
          scope = scope.order(order)
        end
        scope
      end

      field :adminable_apps, Connections.define(Objects::App), visibility: :internal, description: "A list of GitHub Apps managed by this user.", null: false, connection: true do
        argument :exclude_marketplace_listings, Boolean, <<~DESCRIPTION, required: false
            Filters apps to exclude those that have a Marketplace listing. If omitted,
            integrations that are in the Marketplace will be included.
          DESCRIPTION
        argument :public_only, Boolean, <<~DESCRIPTION, required: false
            Filters apps so only public apps are returned. If omitted or false, both
            public and internal apps will be returned.
          DESCRIPTION
      end

      def adminable_apps(**arguments)
        scope = ::Integration.adminable_by(@object.user)
        scope = scope.not_in_marketplace if arguments[:exclude_marketplace_listings]
        scope = scope.public if arguments[:public_only]
        scope = scope.where.not(owner_id: @context[:unauthorized_organization_ids])
        scope.order("integrations.updated_at DESC")
      end

      field :notification_thread_subscription_lists, Connections.define(Unions::NotificationsList), visibility: :under_development, description: "A list of subscription lists.", null: false, connection: false do
        has_connection_arguments
        argument :list_type, Enums::NotificationThreadSubscriptionListType, "List type to scope to", required: false, default_value: "Repository"
      end

      def notification_thread_subscription_lists(**arguments)
        subscription_lists = unpack_newsies_response!(
          GitHub.newsies.thread_subscription_lists(
            user_id: @context[:viewer].id,
            list_type: arguments[:list_type],
          ),
        )

        lists = subscription_lists.sort_by(&:second).take(arguments[:first])

        promises = lists.map do |list, count|
          Platform::Loaders::ActiveRecord.load(list.type.constantize, list.id, security_violation_behaviour: :nil).then do |list|
            next unless list
            next if @context[:unauthorized_organization_ids]&.include?(list.owner_id)

            list.async_readable_by?(@context[:viewer]).then do |readable|
              list if readable
            end
          end
        end

        Promise.all(promises).then do |readable_lists|
          ConnectionWrappers::ArrayWrapper.new(
            ArrayWrapper.new(readable_lists.compact),
            arguments,
            parent: object,
            context: context,
            max_page_size: arguments[:first],
          )
        end
      end

      field :notification_thread_subscriptions, Connections.define(Objects::NotificationThreadSubscription), visibility: :under_development, description: "A list of subscriptions to a thread (e.g. an Issue) that allow the user to receive notifications.", null: false, connection: true do
        argument :order_by, Inputs::NotificationSubscriptionOrder,
                 "Ordering options for notification subscriptions.",
                 required: false, default_value: { field: "id", direction: "DESC" }
        argument :reason, Enums::NotificationReason,
                 "Reason for notification subscriptions.",
                 required: false
        argument :list_id, ID,
                 "List id to filter notification subscriptions to",
                 required: false
        argument :list_type,
                 Enums::NotificationThreadSubscriptionListType,
                 "List type to scope to",
                 required: false, default_value: "Repository"
      end

      def notification_thread_subscriptions(**arguments)
        if arguments[:list_id] && arguments[:list_type]
          begin
            list_type = Objects.const_get(arguments[:list_type])
            list = Helpers::NodeIdentification.typed_object_from_id(list_type, arguments[:list_id], @context)
            newsies_list = ::Newsies::List.to_object(list)
          rescue Errors::NotFound
            # Ignore and don't scope the subscription to a list id
          end
        end

        Helpers::NotificationThreadSubscriptionsQuery.new(
          viewer: @context[:viewer],
          direction: arguments[:order_by][:direction].downcase.to_sym,
          reason: arguments[:reason],
          list_type: newsies_list.present? ? newsies_list.type : arguments[:list_type],
          list_id: newsies_list.present? ? newsies_list.id : nil,
        )
      end

      field :notification_unwatch_suggestions, resolver: Resolvers::NotificationUnwatchSuggestions, description: "A list of unwatch suggestions", visibility: :internal, null: false, areas_of_responsibility: :notifications, connection: true

      field :repository_package_releases, Connections.define(Objects::DependencyGraphPackageReleaseDependent), visibility: :under_development, description: "A list of packages utilized by an organization and count of how many repositories depend on those packages", areas_of_responsibility: :dependency_graph, null: false, connection: false, extras: [:execution_errors] do
        has_connection_arguments
        argument :owner_ids, [Integer], "Array of owner ids to find package releases by", required: true
        argument :package_manager, String, "Package manager to scope to", required: false
        argument :package_name, String, "Package name to scope to", required: false
        argument :exact_match, Boolean, "Match package name precisely", required: false, default_value: false
        argument :package_version, String, "Package version to scope to", required: false
        argument :only_vulnerable_packages, Boolean, "Only include packages with active vulnerabilities", required: false, default_value: false
        argument :sort_by, Inputs::RepositoryPackageReleaseOrder, "How to sort returned packages", required: false, default_value: { field: "DEPENDENTS", direction: "DESC" }
        argument :severity, String, "Only include packages with vulnerabilities with this severity", required: false
        argument :licenses, [String], "Only include packages with these licenses", required: false
        argument :dependent_name, String, "String search to filter dependents by", required: false
        argument :dependents_after, String, "Returns the dependents that come after the specified cursor.", required: false
        argument :dependents_before, String, "Returns the dependents that come before the specified cursor.", required: false
        argument :dependents_first, Integer, "Returns the first _n_ dependents.", required: false
        argument :dependents_last, Integer, "Returns the last _n_ dependents.", required: false
        argument :include_dependent_version_counts, Boolean, "Include the lower/upper version counts", required: false, default_value: false
      end

      def repository_package_releases(execution_errors:, **arguments)
        release_filter = {
          first: arguments[:first],
          last: arguments[:last],
          before: arguments[:before],
          after: arguments[:after],
          package_name: arguments[:package_name],
          exact_match: arguments[:exact_match],
          package_version: arguments[:package_version],
          only_vulnerable_packages: arguments[:only_vulnerable_packages],
          owner_ids: arguments[:owner_ids],
        }

        if arguments[:dependents_first] || arguments[:dependents_last] || arguments[:include_dependent_version_counts]
          dependents_filter = {
            first: arguments[:dependents_first],
            last: arguments[:dependents_last],
            before: arguments[:dependents_before],
            after: arguments[:dependents_after],
            owner_ids: arguments[:owner_ids],
            dependent_name: arguments[:dependent_name],
          }
        end

        if arguments[:package_manager]
          release_filter[:package_manager] = arguments[:package_manager]
        end

        if arguments[:licenses]
          release_filter[:licenses] = arguments[:licenses]
        end

        if arguments[:severity]
          release_filter[:severity] = arguments[:severity]
        end

        # Because dependency-graph-api utilizes enums instead of a sort_by field and direction,
        # we need to translate what we've got to a single string value.
        if arguments[:sort_by]
          field = arguments[:sort_by][:field]
          direction = arguments[:sort_by][:direction]
          if field == "PUBLISHED_ON" && direction == "DESC"
            release_filter[:sort_by] = "NEWEST"
          elsif field == "PUBLISHED_ON" && direction == "ASC"
            release_filter[:sort_by] = "OLDEST"
          elsif field == "UPDATED_AT" && direction == "DESC"
            release_filter[:sort_by] = "RECENTLY_UPDATED"
          elsif field == "UPDATED_AT" && direction == "ASC"
            release_filter[:sort_by] = "LEAST_RECENTLY_UPDATED"
          elsif field == "DEPENDENTS" && direction == "DESC"
            release_filter[:sort_by] = "MOST_DEPENDENTS"
          elsif field == "DEPENDENTS" && direction == "ASC"
            release_filter[:sort_by] = "LEAST_DEPENDENTS"
          elsif field == "VULNERABILITIES" && direction == "DESC"
            release_filter[:sort_by] = "MOST_VULNERABILITIES"
          elsif field == "VULNERABILITIES" && direction == "ASC"
            release_filter[:sort_by] = "LEAST_VULNERABILITIES"
          end
        end

        params = {
          release_filter: release_filter,
          dependents_filter: dependents_filter,
          include_dependent_version_counts: arguments[:include_dependent_version_counts],
        }

        Loaders::Dependencies.load_repository_package_releases(params).then do |result|
          if result.ok?
            result = result.value!
            wrapper = ConnectionWrappers::RepositoryPackageReleases.new(
              ArrayWrapper.new(result[:dependents]),
              arguments,
              parent: object,
              context: context,
            )

            # Forward pagination/count info from dependency graph query results
            wrapper.has_next_page = result[:page_info]["hasNextPage"]
            wrapper.has_previous_page = result[:page_info]["hasPreviousPage"]
            wrapper.start_cursor = result[:page_info]["startCursor"]
            wrapper.end_cursor = result[:page_info]["endCursor"]
            wrapper.total_count = result[:total_count]

            wrapper
          else
            Failbot.report(result.error, app: "github-dependency-graph", dependency_insights: true, owner_ids: arguments[:owner_ids])
            message = case result.error
            when ::DependencyGraph::Client::TimeoutError
              "timedout"
            when ::DependencyGraph::Client::ServiceUnavailableError, ::DependencyGraph::Client::ApiError, StandardError
              "unavailable"
            end
            execution_errors.add(message)
            ConnectionWrappers::ArrayWrapper.new(ArrayWrapper.new, arguments, parent: object, context: context)
          end
        end
      end

      field :repository_package_release_licenses, [DependencyGraphRepositoryPackageReleaseLicense], description: "Get the license stats for all repository package release", areas_of_responsibility: :dependency_graph, visibility: :under_development, null: true do
        argument :owner_ids, [Integer], "Array of organization ids to filter package releases by", required: true
        argument :package_manager, String, "Package manager to scope to", required: false
        argument :package_name, String, "Package name to scope to", required: false
        argument :package_version, String, "Package version to scope to", required: false
        argument :only_vulnerable_packages, Boolean, "Only include packages with active vulnerabilities", required: false, default_value: false
        argument :severity, String, "Only include packages with vulnerabilities with this severity", required: false
        argument :exact_match, Boolean, "Match package name precisely", required: false, default_value: false
      end

      def repository_package_release_licenses(**arguments)
        release_filter = {
          package_name: arguments[:package_name],
          package_version: arguments[:package_version],
          only_vulnerable_packages: arguments[:only_vulnerable_packages],
          owner_ids: arguments[:owner_ids],
          exact_match: arguments[:exact_match],
        }

        if arguments[:package_manager]
          release_filter[:package_manager] = arguments[:package_manager]
        end

        if arguments[:severity]
          release_filter[:severity] = arguments[:severity]
        end

        Loaders::Dependencies.load_repository_package_release_licenses(release_filter: release_filter)
      end

      field :repository_package_release_vulnerabilities, [DependencyGraphRepositoryPackageReleaseVulnerability], description: "Get the vulnerability stats for all repository package release", areas_of_responsibility: :dependency_graph,  visibility: :under_development, null: true do
        argument :owner_ids, [Integer], "Array of organization ids to filter package releases by", required: true
        argument :package_manager, String, "Package manager to scope to", required: false
        argument :package_name, String, "Package name to scope to", required: false
        argument :package_version, String, "Package version to scope to", required: false
        argument :exact_match, Boolean, "Match package name precisely", required: false, default_value: false
        argument :licenses, [String], "Only include packages with these licenses", required: false
      end

      def repository_package_release_vulnerabilities(**arguments)
        release_filter = {
          package_name: arguments[:package_name],
          package_version: arguments[:package_version],
          owner_ids: arguments[:owner_ids],
          exact_match: arguments[:exact_match],
        }

        if arguments[:package_manager]
          release_filter[:package_manager] = arguments[:package_manager]
        end

        if arguments[:licenses]
          release_filter[:licenses] = arguments[:licenses]
        end

        Loaders::Dependencies.load_repository_package_release_vulnerabilities(release_filter: release_filter)
      end

      field :package_release_vulnerabilities, [SecurityVulnerability, null: true], description: "Get the vulnerabilities for a package release", areas_of_responsibility: :dependency_graph, visibility: :under_development, null: true, extras: [:execution_errors] do
        argument :package_manager, String, "Package manager to scope to", required: true
        argument :package_name, String, "Package name to scope to", required: true
        argument :contains_version, String, "Package version to scope to", required: true
      end

      def package_release_vulnerabilities(execution_errors:, **arguments)
        vulnerabilities_filter = {
          package_name: arguments[:package_name],
          package_manager: arguments[:package_manager],
          contains_version: arguments[:contains_version],
        }

        Loaders::Dependencies.load_package_release_vulnerabilities(vulnerabilities_filter: vulnerabilities_filter).then do |result|
          if result.ok?
            Platform::Loaders::ActiveRecord.load_all(::SecurityVulnerabilityForceVisibility, result.value!)
          else
            Failbot.report(result.error, app: "github-dependency-graph", dependency_insights: true)
            message = case result.error
            when ::DependencyGraph::Client::TimeoutError
              "timedout"
            when ::DependencyGraph::Client::ServiceUnavailableError, ::DependencyGraph::Client::ApiError, StandardError
              "unavailable"
            end
            execution_errors.add(message)
            []
          end
        end
      end
    end
  end
end
