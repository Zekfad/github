# frozen_string_literal: true

module Platform
  module Objects
    class PullRequestReviewComment < Platform::Objects::Base
      description "A review comment associated with a given repository pull request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, prr_comment)
        permission.load_pull_and_issue(prr_comment).then do |pull|
          permission.async_repo_and_org_owner(prr_comment).then do |repo, org|
            pull.repository = repo # avoid association load down the line
            permission.access_allowed?(:get_pull_request_comment, repo: repo, resource: pull, current_org: org, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_repository.then do |repository|
          if repository.hide_from_user?(permission.viewer)
            false
          else
            permission.belongs_to_pull_request(object)
          end
        end
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::Blockable
      implements Interfaces::Comment
      implements Interfaces::Deletable
      implements Interfaces::Minimizable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment
      implements Interfaces::Reactable
      implements Interfaces::Reportable
      implements Interfaces::AbuseReportable
      implements Interfaces::RepositoryNode
      global_id_field :id
      database_id_field

      url_fields description: "The HTTP URL permalink for this review comment." do |pr_review_comment|
        pr_review_comment.async_path_uri
      end

      # TODO: define_url_field should be used here but doesn't yet support nullable values.
      #   When that method is updated, this definition should be migrated which will
      #   automatically add the corresponding definition for *Url.
      field :original_diff_resource_path, Scalars::URI, visibility: :internal, method: :async_original_diff_path_uri, description: "The HTTP URL permalink for this review comment positioned in the original diff.", null: true

      # TODO: define_url_field should be used here but doesn't yet support nullable values.
      #   When that method is updated, this definition should be migrated which will
      #   automatically add the corresponding definition for *Url.
      field :current_diff_resource_path, Scalars::URI, visibility: :internal, method: :async_current_diff_path_uri, description: "The HTTP URL permalink for this review comment positioned in the current diff.", null: true

      url_fields \
        prefix: :update,
        visibility: :internal,
        deprecated: {
          start_date: Date.new(2018, 2, 1),
          reason: "Object-specific update endpoints will be removed.",
          superseded_by: nil,
          owner: "xuorig",
        },
        description: "The HTTP URL to the endpoint for updating this review comment." do |pr_review_comment|
        pr_review_comment.async_update_path_uri
      end

      field :pull_request, Objects::PullRequest, method: :async_pull_request, description: "The pull request associated with this review comment.", null: false

      field :pull_request_review, Objects::PullRequestReview, method: :async_pull_request_review, description: "The pull request review associated with this review comment.", null: true

      field :commit, Objects::Commit, description: "Identifies the commit associated with the comment.", null: true, method: :async_commit
      field :original_commit, Objects::Commit, description: "Identifies the original commit associated with the comment.", null: true, method: :async_original_commit

      field :position, Integer, "The line index in the diff to which the comment applies.", null: true
      field :original_position, Integer, "The original line index in the diff to which the comment applies.", null: false
      field :body, String, "The comment body of this review comment.", null: false
      field :body_version, String, visibility: :internal, description: "The comment body hash of this review comment.", null: false
      field :diff_hunk, String, "The diff hunk to which the comment applies.", null: false
      field :path, String, "The path to which the comment applies.", null: false
      field :created_at, Scalars::DateTime, "Identifies when the comment was created.", null: false
      field :updated_at, Scalars::DateTime, "Identifies when the comment was last updated.", null: false

      field :published_at, Scalars::DateTime, "Identifies when the comment was published at.", method: :async_submitted_at, null: true
      field :drafted_at, Scalars::DateTime, "Identifies when the comment was created in a draft state.", method: :created_at, null: false
      field :state, Enums::PullRequestReviewCommentState, "Identifies the state of the comment.", null: false

      field :body_text, String, method: :async_body_text, description: "The comment body of this review comment rendered as plain text.", null: false
      field :body_html, Scalars::HTML, description: "The body rendered to HTML.", null: false do
        argument :hide_code_blobs, Boolean, "Whether or not to include the HTML for code blocks", required: false, default_value: false, feature_flag: :pe_mobile
        argument :render_suggested_changes_as_text, Boolean, "Whether or not to include the HTML for suggested changes", required: false, default_value: false, feature_flag: :pe_mobile
      end

      def body_html(hide_code_blobs: false, render_suggested_changes_as_text: false)
        @object.async_repository.then do |repo|
          repo.async_owner.then do
            repo.async_network.then do
              if hide_code_blobs || render_suggested_changes_as_text
                @object.async_body_html!(context: {hide_code_blobs: hide_code_blobs, render_suggested_changes_as_text: render_suggested_changes_as_text}).then do |body_html|
                  body_html || GitHub::HTMLSafeString::EMPTY
                end
              else
                @object.async_body_html.then do |body_html|
                  body_html || GitHub::HTMLSafeString::EMPTY
                end
              end
            end
          end
        end
      end

      field :websocket, String, visibility: :internal, description: "The websocket channel ID for live updates.", null: false do
        argument :channel, Enums::PullRequestPubSubTopic, "The channel to use.", required: true
      end

      def websocket(**arguments)
        case arguments[:channel]
        when "updated"
          GitHub::WebSocket::Channels.pull_request(@object)
        end
      end

      field :thread, PullRequestReviewThread, feature_flag: :pe_mobile, description: "The thread this comment was posted in.", null: true

      def thread
        if GitHub.flipper[:use_review_thread_ar_model].enabled?(@object.repository)
          @object.async_pull_request_review_thread
        else
          @object.async_pull_request_review_thread.then do |review_thread|
            review_thread.async_review_comments.then do |comments|
              Platform::Models::PullRequestReviewThread.from_review_comments(comments, id_version: 2)
            end
          end
        end
      end

      field :reply_to, PullRequestReviewComment, description: "The comment this is a reply to.", null: true

      def reply_to
        if @object.reply?
          Loaders::ActiveRecord.load(::PullRequestReviewComment, @object.reply_to_id).then do |reply|
            reply&.spammy? ? nil : reply
          end
        end
      end

      field :outdated, Boolean, method: :outdated?, description: "Identifies when the comment body is outdated", null: false

      field :selection_contains_deletions, Boolean, visibility: :internal, method: :async_selection_contains_deletions, description: "Is this comment on a range that contains deletions", null: false
    end
  end
end
