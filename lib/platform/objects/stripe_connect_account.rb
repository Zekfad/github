# frozen_string_literal: true

module Platform
  module Objects
    class StripeConnectAccount < Platform::Objects::Base
      model_name "Billing::StripeConnect::Account"
      description "A Stripe Connect account record."
      areas_of_responsibility :gitcoin
      minimum_accepted_scopes ["user"]
      visibility :internal

      implements Platform::Interfaces::Node

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, account)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      # Ability check for a SponsorsListing object
      def self.async_viewer_can_see?(permission, object)
        return false if permission.viewer.nil?
        return true if permission.viewer.can_admin_sponsors_listings?
        object.async_belongs_to?(permission.viewer)
      end

      field :stripe_account_id, String, "The Stripe provided account identifier",
        visibility: :internal, null: false

      field :account_details, Objects::StripeAccountDetails, "The account details for this stripe account",
        visibility: :internal, null: true

      def account_details
        details = @object.stripe_account_details
        return if details.empty?
        ::Stripe::Account.construct_from(details)
      end

      field :is_verified, Boolean, "Whether or not the account has been verified by Stripe",
        visibility: :internal, method: :verified?, null: false
    end
  end
end
