# frozen_string_literal: true

module Platform
  module Objects
    class PullRequest < Platform::Objects::Base
      description "A repository pull request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, pull)
        permission.async_repo_and_org_owner(pull).then do |repo, org|
          pull.async_issue.then do |issue|
            permission.access_allowed?(:get_pull_request, repo: repo, resource: pull, current_org: org, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_hide_from_user?(permission.viewer).then do |hide_from_user|
          if hide_from_user
            false
          else
            permission.belongs_to_repository(object)
          end
        end
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::Assignable
      implements Interfaces::Closable
      implements Interfaces::Comment
      implements Interfaces::Commentable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment
      implements Interfaces::Labelable
      implements Interfaces::Lockable
      implements Interfaces::PerformableViaApp
      implements Interfaces::Reactable
      implements Interfaces::Reportable
      implements Interfaces::Blockable
      implements Interfaces::AbuseReportable
      implements Interfaces::RepositoryNode
      implements Interfaces::Subscribable
      implements Interfaces::UniformResourceLocatable
      implements Interfaces::Trigger

      global_id_field :id

      url_fields(description: "The HTTP URL for this pull request.") { |pull| pull.async_path_uri }

      database_id_field

      field :number, Integer, description: "Identifies the pull request number.", null: false

      def number
        @object.async_issue.then(&:number)
      end

      field :participants, resolver: Resolvers::Participants, description: "A list of Users that are participating in the Pull Request conversation.", connection: true

      field :mentionable_items, resolver: Resolvers::MentionableItems, description: "A list of mentionable items that can be mentioned in the context of this pull request.",
        connection: true, feature_flag: :pe_mobile, null: true

      field :base_ref, Ref, description: "Identifies the base Ref associated with the pull request.", null: true

      def base_ref
        @object.async_base_repository.then do |repository|
          repository.async_network.then {
            repository.heads.find(@object.base_ref)
          }
        end
      end

      field :base_ref_name, String, description: "Identifies the name of the base Ref associated with the pull request, even if the ref has been deleted.", null: false

      def base_ref_name
        @object.base_ref.dup.force_encoding("utf-8")
      end

      field :base_ref_oid, Scalars::GitObjectID,
        description: "Identifies the oid of the base ref associated with the pull request, even if the ref has been deleted.",
        null: false,
        method: :base_sha

      field :base_repository, Repository,
        description: "The repository associated with this pull request's base Ref.",
        null: true,
        method: :async_base_repository

      field :closed_by, Interfaces::Actor, visibility: :internal, description: "The actor who closed the pull request.", null: true

      def closed_by
        @object.async_issue.then(&:async_closed_by)
      end

      field :closing_issues_references, resolver: Resolvers::ClosingIssueReferences,
        null: true, description: "List of issues that were may be closed by this pull request",
        connection: true, visibility: :internal do
          argument :order_by, Inputs::IssueOrder, "Ordering options for issues returned from the connection", required: false
      end

      field :is_read_by_viewer, Boolean, null: true, description: "Is this pull request read by the viewer", feature_flag: :pe_mobile

      def is_read_by_viewer
        if viewer = @context[:viewer]
          !Conversation.read_for(viewer, [@object.issue]).empty?
        else
          true
        end
      end

      field :head_ref, Ref, description: "Identifies the head Ref associated with the pull request.", null: true

      def head_ref
        Loaders::ActiveRecord.load(::Repository, @object.head_repository_id, security_violation_behaviour: :nil).then do |repository|
          if repository.nil? # repo has been destroyed after the PR was opened, or repo is not accessible
            nil
          else
            repository.async_network.then {
              repository.heads.find(@object.head_ref)
            }
          end
        end
      end

      field :head_ref_name, String, description: "Identifies the name of the head Ref associated with the pull request, even if the ref has been deleted.", null: false

      def head_ref_name
        @object.head_ref.dup.force_encoding("utf-8")
      end

      field :head_ref_oid, Scalars::GitObjectID,
        description: "Identifies the oid of the head ref associated with the pull request, even if the ref has been deleted.",
        null: false,
        method: :head_sha

      field :head_repository, Repository, description: "The repository associated with this pull request's head Ref.", null: true

      def head_repository
        Loaders::ActiveRecord.load(::Repository, @object.head_repository_id, security_violation_behaviour: :nil)
      end

       field :head_repository_owner, Interfaces::RepositoryOwner, description: "The owner of the repository associated with this pull request's head Ref.", null: true

      def head_repository_owner
        @object.async_head_user.then do |user|
          if user&.spammy?
            nil
          else
            user
          end
        end
      end

      field :is_cross_repository, Boolean, method: :cross_repo?, description: "The head and base repositories are different.", null: false

      field :is_draft, Boolean, description: "Identifies if the pull request is a draft.", null: false, method: :draft?

      field :state, Enums::PullRequestState, description: "Identifies the state of the pull request.", null: false

      def state
        if @object.merged_at
          "merged"
        else
          Loaders::ActiveRecord.load(::Issue, @object.id, column: :pull_request_id).then(&:state)
        end
      end

      field :maintainer_can_modify, Boolean, description: "Indicates whether maintainers can modify the pull request.", null: false

      def maintainer_can_modify
        @object.async_issue.then do
          @object.fork_collab_granted?
        end
      end

      field :milestone, Objects::Milestone, description: "Identifies the milestone associated with the pull request.", null: true

      def milestone
        @object.async_issue.then(&:async_milestone)
      end

      field :issue_database_id, Integer, visibility: :internal, description: "The pull request issue's database ID. This is a legacy requirement of the REST API and should never be exposed publicly.", null: false

      def issue_database_id
        @object.async_issue.then(&:id)
      end

      field :title, String, description: "Identifies the pull request title.", null: false

      def title
        @object.async_issue.then(&:title)
      end

      field :task_list_item_count, Integer, visibility: :internal, description: "Number of tasks in the pull request's task list", null: false do
        argument :statuses, [Enums::TaskListItemStatus, null: true], "Limit the count to tasks in the specified statuses.", required: false
      end

      def task_list_item_count(**arguments)
        @object.async_issue.then do |issue|
          issue.async_task_list_item_count(*arguments[:statuses])
        end
      end

      field :diff, Objects::Diff, feature_flag: :pe_mobile, description: "Identifies a diff over two commits within this pull request.", null: true, extras: [:lookahead] do
        argument :timeout, Integer, "How long to allow for loading the diff.", required: false
      end

      def diff(lookahead:, timeout: nil)
        @object.async_compare_repository.then do |compare_repo|
          @object.async_merge_base.then do |merge_base_oid|
            next unless merge_base_oid
            @context.scoped_merge!(root_commit_arguments: {oid: @object.head_sha})

            use_summary = Helpers::Diff.use_summary?(lookahead)
            Helpers::Diff.for(head_repo_id:     @object.head_repository_id,
                              base_repo_id:     @object.base_repository_id,
                              start_ref_or_oid: merge_base_oid,
                              end_ref_or_oid:   @object.head_sha,
                              base_commit_oid:  merge_base_oid,
                              use_summary:      use_summary,
                              timeout:          timeout,
                              compare_repo:     compare_repo)
          end
        end
      end

      field :files, Connections.define(Objects::PullRequestChangedFile), description: "Lists the files changed within this pull request.", null: true

      def files
        @object.async_historical_comparison.then do |comparison|
          diff = comparison.init_diffs
          diff.use_summary = true

          next unless diff.summary.available?

          files = diff.summary.deltas.map do |delta|
            ::PullRequest::ChangedFile.new(
              path: delta.path,

              # these can be null in the case of binary files. Our API can't handle that so let's
              # set them to 0 instead.
              additions: delta.additions || 0,
              deletions: delta.deletions || 0,
              repository: @object.repository,
              pull_request: @object,
            )
          end

          ArrayWrapper.new(files)
        end
      end

      field :body_html, Scalars::HTML, description: "The body rendered to HTML.", null: false do
        argument :hide_code_blobs, Boolean, "Whether or not to include the HTML for code blobs", required: false, default_value: false, feature_flag: :pe_mobile
        argument :render_suggested_changes_as_text, Boolean, "Whether or not to include the HTML for suggested changes", required: false, default_value: false, feature_flag: :pe_mobile
      end

      def body_html(hide_code_blobs: false, render_suggested_changes_as_text: false)
        @object.async_issue.then do |issue|
          issue.async_repository.then do |repo|
            repo.async_owner.then do
              repo.async_network.then do
                if hide_code_blobs
                  issue.async_body_html!(context: {hide_code_blobs: hide_code_blobs}).then do |body_html|
                    body_html || GitHub::HTMLSafeString::EMPTY
                  end
                else
                  @object.async_body_html.then do |body_html|
                    body_html || GitHub::HTMLSafeString::EMPTY
                  end
                end
              end
            end
          end
        end
      end

      field :body_version, String, visibility: :internal, description: "Identifies the body hash of the pull request.", null: false

      def body_version
        @object.async_issue.then(&:body_version)
      end

      field :editor, Interfaces::Actor, description: "The actor who edited this pull request's body.", null: true

      def editor
        @object.async_issue.then do |issue|
          ::Platform::Helpers::Editor.for(editable: issue, context: @context)
        end
      end

      field :last_edited_at, Scalars::DateTime, description: "The moment the editor made the last edit", null: true

      def last_edited_at
        @object.async_issue.then do |issue|
          issue.async_latest_user_content_edit.then do |user_content_edit|
            user_content_edit.edited_at if user_content_edit
          end
        end
      end

      field :closed, Boolean, description: "`true` if the pull request is closed", null: false

      def closed
        @object.async_issue.then do
          @object.closed?
        end
      end

      field :locked, Boolean, description: "`true` if the pull request is locked", null: false

      def locked
        @object.async_issue.then do |issue|
          issue.async_conversation.then {
            issue.async_repository.then {
              issue.locked?
            }
          }
        end
      end

      field :mergeable, Enums::MergeableState, description: "Whether or not the pull request can be merged based on the existence of merge conflicts.", null: false

      def mergeable
        Loaders::PullRequestMergesCleanly.load(@object.id).then { |merges_cleanly|
          if merges_cleanly == true
            "mergeable"
          elsif merges_cleanly == false
            "unmergeable"
          else
            "unknown"
          end
        }
      end

      field :merge_state_status, Enums::MergeStateStatus, description: "Detailed information about the current pull request merge state status.", null: false

      def merge_state_status
        Promise.all([@object.async_repository, @object.async_base_repository, @object.async_head_repository]).then do |repositories|
          networks = repositories.compact.map(&:async_network)
          users = [@object.async_base_user, @object.async_head_user, @object.async_user]

          Promise.all(networks + users).then do
            # Only enqueue this job for our mobile apps
            if GitHub.flipper[:pe_mobile].enabled?(context[:oauth_app])
              @object.enqueue_mergeable_update
            end

            @object.merge_state(viewer: @context[:viewer]).async_status
          end
        end
      end

      field :merged, Boolean, method: :merged?, description: "Whether or not the pull request was merged.", null: false

      field :merged_at, Scalars::DateTime, description: "The date and time that the pull request was merged.", null: true

      field :merged_by, Interfaces::Actor, description: "The actor who merged the pull request.", null: true

      def merged_by
        return unless @object.merged?

        @object.async_issue.then do |issue|
          issue.async_events.then do |events|
            merge_events = events.select(&:merge?)
            next if merge_events.empty?

            merge_events.last.async_actor.then do |actor|
              if actor&.spammy?
                nil
              else
                actor
              end
            end
          end
        end
      end

      field :merge_commit, Objects::Commit, description: "The commit that was created when this pull request was merged.", null: true

      def merge_commit
        return nil unless @object.merged?

        @object.async_repository.then { |repository|
          Loaders::GitObject.load(repository, @object.merge_commit_sha, expected_type: :commit)
        }
      end

      field :potential_merge_commit, Objects::Commit, description: "The commit that GitHub automatically generated to test if this pull request could be merged. This field will not return a value if the pull request is merged, or if the test merge commit is still being generated. See the `mergeable` field for more details on the mergeability of the pull request.", null: true

      def potential_merge_commit
        return nil if @object.merged?

        Loaders::PullRequestMergesCleanly.load(@object.id).then { |merges_cleanly|
          if merges_cleanly && @object.merge_commit_sha
            Loaders::ActiveRecord.load(::Repository, @object.base_repository_id).then { |base_repo|
              Loaders::GitObject.load(base_repo, @object.merge_commit_sha, expected_type: :commit)
            }
          end
        }
      end

      field :comment, Objects::IssueComment,
          description: "Find a particular comment on this pull request.",
          visibility: :under_development, null: true do
        argument :database_id, Int, "Look up comment by its database ID.", required: true
      end

      def comment(database_id:)
        @object.async_issue.then do |issue|
          if context[:permission].typed_can_access?("Issue", issue)
            issue.comments.filter_spam_for(@context[:viewer]).where(id: database_id).first
          end
        end
      end

      field :review_comment, Objects::PullRequestReviewComment,
          description: "Find a particular comment in a review on this pull request.",
          visibility: :under_development, null: true do
        argument :database_id, Int, "Look up comment by its database ID.", required: true
      end

      def review_comment(database_id:)
        @object.review_comments.filter_spam_for(@context[:viewer]).where(id: database_id).first
      end

      field :review, Objects::PullRequestReview,
          description: "Find a particular code review of this pull request.",
          visibility: :under_development, null: true do
        argument :database_id, Int, "Look up review by its database ID.", required: true
      end

      def review(database_id:)
        reviews.where(id: database_id).first
      end

      field :comments, resolver: Resolvers::IssueComments, description: "A list of comments associated with the pull request.", connection: true, numeric_pagination_enabled: true

      field :total_comments_count, Integer, null: true, visibility: :under_development,
        description: "Returns a count of how many comments this pull request has received."

      def total_comments_count
        @object.async_issue.then do
          @object.issue_comments_count
        end
      end

      field :review_threads, Connections.define(Objects::PullRequestReviewThread),
        description: "The list of all review threads for this pull request.",
        connection: true,
        null: false

      def review_threads(**arguments)
        Loaders::PullRequest::ReviewThreads.load(@object.id, @context[:viewer]).then do |threads|
          if GitHub.flipper[:use_review_thread_ar_model].enabled?(@object.repository)
            StableArrayWrapper.new(threads)
          else
            Promise.all(threads.map(&:async_to_platform_thread)).then do |platform_threads|
              StableArrayWrapper.new(platform_threads)
            end
          end
        end
      end

      field :latest_opinionated_reviews, Connections.define(Objects::PullRequestReview), feature_flag: :pe_mobile, description: "A list of latest reviews per user associated with the pull request.", null: true, connection: true do
        argument :writers_only, Boolean, "Only return reviews from user who have write access to the repository", required: false, default_value: false
      end

      def latest_opinionated_reviews(**arguments)
        ArrayWrapper.new(@object.latest_enforced_reviews(writers_only: arguments[:writers_only]))
      end

      field :latest_reviews, Connections.define(Objects::PullRequestReview), feature_flag: :pe_mobile, description: "A list of latest reviews per user associated with the pull request that are not also pending review.", null: true, connection: true

      def latest_reviews
        @object.async_user.then do
          ArrayWrapper.new(@object.visible_sidebar_reviews(@context[:viewer]))
        end
      end

      field :reviews, Connections.define(Objects::PullRequestReview), numeric_pagination_enabled: true, description: "A list of reviews associated with the pull request.", null: true, connection: true do
        argument :states, [Enums::PullRequestReviewState], "A list of states to filter the reviews.", required: false
        argument :author, String, "Filter by author of the review.", required: false
      end

      def reviews(**arguments)
        scope = @object.latest_reviews_for(@context[:viewer])
        if arguments[:states]
          states = arguments[:states].map { |name| ::PullRequestReview.state_value(name) }
          scope = scope.where(state: states)
        end

        if arguments[:author]
          scope = scope.where(user_id: ::User.where(login: arguments[:author]).ids)
        end

        scope
      end

      field :commits, Connections::PullRequestCommit, max_page_size: 250, description: "A list of commits present in this pull request's head branch not present in the base branch.", null: false, connection: true

      def commits
        Promise.all([
          @object.async_repository,
          @object.async_head_repository,
          @object.async_base_repository,
          @object.async_changed_commits,
        ]).then do |repository, head_repository, base_repository, commits|
            pull_request_commits = commits.map do |commit|
              commit.repository = repository
              commit.repositories = [head_repository, base_repository].compact
              Models::PullRequestCommit.new(@object, commit)
            end

          ArrayWrapper.new(pull_request_commits)
        end
      end

      field :is_corrupt, Boolean, visibility: :internal, description: "Whether or not the pull request is corrupt.", null: false

      def is_corrupt
        @object.async_changed_commits.then do
          @object.corrupt?
        end
      end

      field :review_requests, Connections.define(Objects::ReviewRequest), description: "A list of review requests associated with the pull request.", null: true, connection: true

      def review_requests
        @object.review_requests.pending.scoped
      end

      field :can_be_rebased, Boolean, description: "Whether or not the pull request is rebaseable.", null: false

      def can_be_rebased
        @object.async_repository.then do |repository|
          Promise.all([repository.async_network, @object.async_issue]).then do
            Loaders::PullRequestMergesCleanly.load(@object.id).then do |merges_cleanly|
              (merges_cleanly == true) && @object.rebase_safe?
            end
          end
        end
      end

      field :review_decision, Enums::PullRequestReviewDecision,
        null: true,
        description: "The current status of this pull request with respect to code review."

      def review_decision
        @object.async_repository.then do |repository|
          repository.async_internal_repository.then do
            @object.async_review_decision(viewer: @context[:viewer])
          end
        end
      end

      # TODO N+1 for queries loading lots of merge events. May want to keep this field internal.
      # see https://github.com/github/github/pull/74977#discussion_r123785568 for discussion
      # on making this batch loaded.
      field :status_at_merge, Objects::Status, visibility: :internal, method: :async_status_at_merge, description: "The status contexts belonging to the last commit before merge that were created before the merge.", null: true

      field :timeline,
        resolver: Platform::Resolvers::PullRequestTimeline,
        description: "A list of events, comments, commits, etc. associated with the pull request.",
        deprecated: {
          start_date: Date.new(2020, 4, 22),
          reason: "`timeline` will be removed",
          superseded_by: "Use PullRequest.timelineItems instead.",
          owner: "mikesea",
        }

      # New, fully batched timeline powered by `Timeline::PullRequestTimeline`. We do not
      # intend to publish it with this name!
      #
      # TODO: Make the timeline paginateable and publish it by merging it into `timeline`.
      field :timeline_items,
        resolver: Platform::Resolvers::PullRequestTimelineItems,
        connection: true,
        max_page_size: 250,
        description: "A list of events, comments, commits, etc. associated with the pull request."

      field :suggested_reviewers, [SuggestedReviewer, null: true], description: "A list of reviewer suggestions based on commit history and past review comments.", null: false

      def suggested_reviewers
        Platform::LoaderTracker.ignore_association_loads do
          if @object.suggested_reviewers_available?
            ArrayWrapper.new(@object.suggested_reviewers)
          else
            ArrayWrapper.new([])
          end
        end
      end

      field :websocket, String, visibility: :internal, description: "The websocket channel ID for live updates.", null: false do
        argument :channel, Enums::PullRequestPubSubTopic, "The channel to use.", required: true
      end

      def websocket(**arguments)
        case arguments[:channel]
        when "updated"
          GitHub::WebSocket::Channels.pull_request(@object)
        when "head_ref"
          @object.async_repository.then do |repository|
            GitHub::WebSocket::Channels.branch(repository, @object.display_head_ref_name)
          end
        when "timeline"
          GitHub::WebSocket::Channels.pull_request_timeline(@object)
        when "state"
          GitHub::WebSocket::Channels.pull_request_state(@object)
        end
      end

      field :project_cards, resolver: Resolvers::ProjectCards, description: "List of project cards associated with this pull request."

      url_fields prefix: :revert, description: "The HTTP URL for reverting this pull request." do |pull|
        pull.async_revert_path_uri
      end

      url_fields prefix: :checks, description: "The HTTP URL for the checks of this pull request." do |pull|
        pull.async_repository.then do |repository|
          repository.async_owner.then do |owner|
            template = Addressable::Template.new("/{user}/{repo}/pull/{pull_number}/checks")
            template.expand user: owner.login, repo: repository.name, pull_number: pull.number
          end
        end
      end

      url_fields prefix: :restore_head_ref, description: "The HTTP URL for restoring this pull request's head ref.", visibility: :internal do |pull|
        pull.async_restore_head_ref_path_uri
      end

      field :viewer_can_restore_head_ref, Boolean, "Check if the viewer can restore the deleted head ref.", visibility: :internal, null: false

      def viewer_can_restore_head_ref
        @object.async_head_ref_restorable_by?(@context[:viewer])
      end

      field :viewer_can_delete_head_ref, Boolean, "Check if the viewer can restore the deleted head ref.", feature_flag: :pe_mobile, null: false

      def viewer_can_delete_head_ref
        @object.async_head_ref_deleteable_by?(@context[:viewer]).then do |deleteable|
          next true if deleteable

          @object.async_head_ref_deleteable_after_updating_dependents?(@context[:viewer])
        end
      end

      field :viewer_has_pending_review, Boolean, visibility: :internal, description: "Check if the viewer has a pending review on this pull request.", null: false

      def viewer_has_pending_review
        @object.async_pending_review_by?(@context[:viewer])
      end

      field :additions, Integer, description: "The number of additions in this pull request.", null: false, method: :async_additions

      field :deletions, Integer, description: "The number of deletions in this pull request.", null: false, method: :async_deletions

      field :changed_files, Integer, description: "The number of changed files in this pull request.", null: false, method: :async_changed_files

      # This should be kept internal until the tooltip dismissal action is on a user controller instead.
      # There is nothing inherently making this function dependent on pull requests.
      url_fields(prefix: :dismiss_merge_tip,
                       description: "The HTTP URL to dismiss merge tips for the viewer.",
                       visibility:  :internal) do |pull|
        pull.async_dismiss_merge_tip_path_uri
      end

      field :permalink, Scalars::URI, description: "The permalink to the pull request.", null: false

      def permalink
        @object.async_repository.then do
          @object.async_issue.then do
            Addressable::URI.parse(@object.permalink)
          end
        end
      end

      field :viewer_can_apply_suggestion, Boolean, description: "Whether or not the viewer can apply suggestion.", null: false

      def viewer_can_apply_suggestion
        @object.suggested_change_applicable_by?(context[:viewer])
      end

      def self.load_from_params(params)
        Objects::Repository.load_from_params(params).then do |repository|
          repository && Loaders::PullRequestByNumber.load(repository.id, params[:id].to_i)
        end
      end

      field :hovercard, Objects::Hovercard, description: "The hovercard information for this issue", null: false do
        argument :include_notification_contexts, Boolean, "Whether or not to include notification contexts", required: false, default_value: true
      end

      def hovercard(include_notification_contexts: true)
        ::IssueOrPullRequestHovercard.new(@object, viewer: @context[:viewer], include_notifications: include_notification_contexts)
      end

      field :updated_at, Platform::Scalars::DateTime, null: false, description: "Identifies the date and time when the object was last updated."

      def updated_at
        @object.async_issue.then do |issue|
          [@object.updated_at, issue.updated_at].max
        end
      end
    end
  end
end
