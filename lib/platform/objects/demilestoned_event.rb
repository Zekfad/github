# frozen_string_literal: true

module Platform
  module Objects
    class DemilestonedEvent < Platform::Objects::Base
      model_name "IssueEvent"
      description "Represents a 'demilestoned' event on a given issue or pull request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, demilestoned_event)
        permission.belongs_to_issue_event(demilestoned_event)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_issue(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      implements Interfaces::TimelineEvent

      implements Interfaces::PerformableViaApp

      global_id_field :id

      field :subject, Unions::MilestoneItem, "Object referenced by event.", method: :async_issue_or_pull_request, null: false

      field :milestone_title, String, "Identifies the milestone title associated with the 'demilestoned' event.", null: false

      def milestone_title
        @object.async_issue_event_detail.then do |issue_event_detail|
          issue_event_detail.milestone_title
        end
      end

      field :milestone, Milestone, "The milestone associated with this event.", visibility: :internal, method: :async_milestone, null: true
    end
  end
end
