# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class ArrayWrapper < GraphQL::Relay::ArrayConnection
      include Platform::ConnectionWrappers::GetLimitedArg

      def sliced_nodes
        @sliced_nodes ||= if page = get_limited_arg(:numeric_page)
          per_page = @paginator.first
          last_page = (nodes.size / per_page.to_f).ceil

          if page <= last_page
            offset = (per_page * (page - 1))
            nodes.drop(offset)
          else
            []
          end
        else
          super
        end
      end
    end
  end
end
