# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class StableArray < GraphQL::Relay::BaseConnection
      include Platform::ConnectionWrappers::GetLimitedArg

      def cursor_from_node(item)
        CursorGenerator.generate_cursor(data_from_node(item), version: :v2)
      end

      def page_count
        paged_nodes.size
      end

      def filtered_count
        sliced_nodes.size
      end

      def before_focus_count
        return 0 unless focus

        sliced_nodes.index(paged_nodes.first) || 0
      end

      def after_focus_count
        return 0 unless focus

        index = sliced_nodes.index(paged_nodes.first)
        return 0 unless index

        sliced_nodes.size - index - 1
      end

      def has_previous_page
        return false unless sorted_nodes.any?

        if last
          sliced_nodes.count > last
        elsif after
          after_cursor_data = data_from_cursor(after)

          result = after_cursor_data <=> node_to_cursor_data[sorted_nodes.first]
          !result.nil? && result >= 0
        else
          false
        end
      end

      def has_next_page
        return false unless sorted_nodes.any?

        if first
          sliced_nodes.count > first
        elsif max_page_size
          sliced_nodes.count > max_page_size
        elsif before
          before_cursor_data = data_from_cursor(before)

          result = before_cursor_data <=> node_to_cursor_data[sorted_nodes.last]
          !result.nil? && result <= 0
        else
          false
        end
      end

      private

      def data_from_node(item)
        nodes.sort_by_proc.call(item).map { |value|
          case value
          when Time
            value.utc.iso8601
          else
            value
          end
        }
      end

      def data_from_cursor(cursor)
        decoded = Base64.urlsafe_decode64(cursor)

        if decoded.starts_with?(CursorGenerator::CURSOR_IDENTIFIER)
          CursorGenerator.resolve_cursor(cursor)
        else
          # We want to keep compatibility with cursor generated from `ArrayConnection`
          nodes[decoded.to_i] && data_from_node(nodes[decoded.to_i])
        end
      end

      def first
        return @first if defined? @first

        @first = get_limited_arg(:first)
        @first = max_page_size if @first && max_page_size && @first > max_page_size
        @first
      end

      def skip
        return @skip if defined? @skip

        @skip = get_limited_arg(:skip)
      end

      def last
        return @last if defined? @last

        @last = get_limited_arg(:last)
        @last = max_page_size if @last && max_page_size && @last > max_page_size
        @last
      end

      def focus
        arguments[:focus]
      end

      def find_focused_node(items)
        return unless focus

        items.find { |item| item.global_relay_id == focus }
      end

      # apply first / last limit results
      def paged_nodes
        @paged_nodes ||= begin
          items = sliced_nodes

          if focus
            items = [find_focused_node(items)]
          else
            items = items.drop(skip) if skip
            items = items.first(first) if first
            items = items.last(last) if last
            items = items.first(max_page_size) if max_page_size && !first && !last
          end

          items
        end
      end

      # Apply cursors to edges
      def sliced_nodes
        return @sliced_nodes if defined?(@sliced_nodes)

        @sliced_nodes = sorted_nodes

        if page = get_limited_arg(:numeric_page)
          return @sliced_nodes if page == 1

          per_page = @paginator.first
          last_page = (nodes.size / per_page.to_f).ceil

          if page <= last_page
            offset = (per_page * (page - 1))
            @sliced_nodes = @sliced_nodes.drop(offset)
          else
            @sliced_nodes = []
          end
        elsif after || before
          if after
            if after_cursor_data = data_from_cursor(after)
              @sliced_nodes = @sliced_nodes.drop_while do |node|
                result = (node_to_cursor_data[node] <=> after_cursor_data)
                result && result <= 0
              end
            else
              @sliced_nodes = []
            end
          end

          if before && (before_cursor_data = data_from_cursor(before))
            @sliced_nodes = @sliced_nodes.take_while do |node|
              result = (node_to_cursor_data[node] <=> before_cursor_data)
              result.nil? || result < 0
            end
          end
        end

        @sliced_nodes
      end

      def sorted_nodes
        return @sorted_nodes if defined?(@sorted_nodes)

        @sorted_nodes = nodes.sort_by { |node| node_to_cursor_data[node] }
      end

      def node_to_cursor_data
        return @node_to_cursor_data if defined?(@node_to_cursor_data)

        @node_to_cursor_data = Hash[nodes.map { |node| [node, data_from_node(node)] }]
      end
    end
  end
end
