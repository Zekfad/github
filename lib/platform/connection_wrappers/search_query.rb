# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class SearchQuery < GraphQL::Relay::BaseConnection
      include CursorGenerator

      class Edge
        attr_reader :node, :cursor, :highlights

        def initialize(node, cursor, highlights)
          @node, @cursor, @highlights = node, cursor, highlights
        end
      end

      def edges
        paged_nodes_and_highlights.map do |item|
          Edge.new(item[:node], cursor_from_node(item[:node]), item[:highlights])
        end
      end

      def cursor_from_node(item)
        offset = starting_offset + paged_nodes.index(item) + 1
        Platform::ConnectionWrappers::CursorGenerator.generate_cursor(offset)
      end

      def has_next_page
        next_offset = starting_offset + limit
        next_page_not_at_max_offset = (next_offset + per_page) <= Search::Query::MAX_OFFSET
        total_count > next_offset && next_page_not_at_max_offset
      end

      def has_previous_page
        starting_offset > 0
      end

      # Public: Returns a Search::Results instance for the current query.
      def search_results
        @search_results ||= begin
          nodes.execute
        rescue ::Search::Queries::IssueQuery::InsecureUserToServerAppQuery => error
          ::Search::Results.empty(error_message: error.message)
        end
      end

      # A cached count of this search, for API use and internal calculations
      def total_count
        @total_count ||= begin
          nodes.count
        rescue ::Search::Queries::IssueQuery::InsecureUserToServerAppQuery
          0
        end
      end

      # The offset of the first item in the results. This field along with
      # total count and the nodes size can be used when rendering details about the
      # current page of search results being viewed such as "26-50 of 100".
      def first_item_offset
        starting_offset + 1
      end

      private

      def paged_nodes
        @paged_nodes ||= paged_nodes_and_highlights.map { |item| item[:node] }
      end

      # apply first / last limit results
      def paged_nodes_and_highlights
        @paged_nodes_and_highlights ||= begin
          query_helper = sliced_nodes

          if per_page
            query_helper.per_page = per_page
          end

          if previous_offset + per_page > Search::Query::MAX_OFFSET
            []
          else
            query_helper.execute.map do |item|
              node = case item["_type"]
                     when "repository";              item["_model"]
                     when "issue";                   item["_model"]
                     when "pull_request";            item["_model"].pull_request
                     when "user";                    item["_model"]
                     when "non_marketplace_listing"; item["_model"]
                     when "notification";            item["_notification_thread"]
                     when "marketplace_listing";     item["_model"]
                     when "repository_action";       item["_model"]
                     when "label";                   item["_model"]
                     when "github_app";              item["_model"]
              end

              {
                node: node,
                highlights: item["highlight"],
              }
            end

          end
        rescue ::Search::Queries::IssueQuery::InsecureUserToServerAppQuery
          []
        end
      end

      # Apply cursors to edges
      def sliced_nodes
        @sliced_nodes ||= begin
          query_helper = nodes
          query_helper.configure_page_and_offset(offset: starting_offset)
          query_helper
        end
      end

      def offset_from_cursor(cursor)
        resolved_cursor = Platform::ConnectionWrappers::CursorGenerator.resolve_cursor(cursor)

        if resolved_cursor.respond_to?(:to_i)
          resolved_cursor.to_i
        else
          0 # go back to the first result if we don't recognize the cursor
        end
      end

      def starting_offset
        @initial_offset ||= begin
          if before
            [previous_offset, 0].max
          else
            previous_offset
          end
        end
      end

      # Offset from the previous selection, if there was one
      # Otherwise, zero
      def previous_offset
        @previous_offset ||= if after
          offset_from_cursor(after)
        elsif before
          offset_from_cursor(before) - last.to_i - 1
        else
          0
        end
      end

      def per_page
        return @per_page if defined? @per_page

        @per_page = if limit || max_page_size
          [limit, max_page_size].compact.min
        end
      end

      def limit
        @limit ||= if first
          first.to_i
        else
          if previous_offset < 0
            previous_offset + last.to_i
          else
            last.to_i
          end
        end
      end
    end
  end
end
