# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class TeamRepositories < GraphQL::Pagination::Connection
      class OrderedByIdCursor
        def self.decode(opaque)
          new *Platform::ConnectionWrappers::CursorGenerator.resolve_cursor(opaque)
        end

        def initialize(repository_id)
          @repository_id = repository_id
        end

        attr_reader :repository_id

        include Comparable

        def <=>(other)
          @repository_id <=> other.repository_id
        end

        def encode
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([@repository_id], version: :v2)
        end
      end

      # Cursor for ordering by `CREATED_AT`, `UPDATED_AT`, `PUSHED_AT`
      class OrderedByDateTimeCursor < OrderedByIdCursor
        def self.decode(opaque)
          values = Platform::ConnectionWrappers::CursorGenerator.resolve_cursor(opaque)

          new DateTime.iso8601(values[0].to_s), values[1]
        end

        def initialize(datetime, repository_id)
          super repository_id

          @datetime = datetime
        end

        attr_reader :datetime

        def <=>(other)
          result = @datetime <=> other.datetime

          if result.nil?
            @datetime.nil? ? -1 : 1
          elsif result.zero?
            super other
          else
            result
          end
        end

        def encode
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([@datetime&.iso8601, @repository_id], version: :v2)
        end
      end

      # Cursor for ordering by `STARGAZERS`
      class OrderedByWatcherCountCursor < OrderedByIdCursor
        def initialize(watcher_count, repository_id)
          super repository_id

          @watcher_count = watcher_count
        end

        attr_reader :watcher_count

        def <=>(other)
          result = @watcher_count <=> other.watcher_count
          return result unless result.zero?

          super other
        end

        def encode
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([@watcher_count, @repository_id], version: :v2)
        end
      end

      # Cursor for ordering by `PERMISSION`
      class OrderedByPermissionCursor < OrderedByIdCursor
        def initialize(action, repository_id)
          super repository_id

          @action = action
        end

        attr_reader :action

        def <=>(other)
          result = @action <=> other.action
          return result unless result.zero?

          super other
        end

        def encode
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([@action, @repository_id], version: :v2)
        end
      end

      # Cursor for ordering by `NAME`
      class OrderedByNameCursor < OrderedByIdCursor
        def initialize(repository_name, repository_id)
          super repository_id

          @repository_name = repository_name
        end

        attr_reader :repository_name

        def <=>(other)
          result = @repository_name.casecmp(other.repository_name)
          return result unless result.zero?

          super other
        end

        def encode
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([@repository_name, @repository_id], version: :v2)
        end
      end

      # TODO: Drop once we upgrade to `grapqhl@1.11.0`
      class Edge
        attr_reader :node

        def initialize(node, connection)
          @connection = connection
          @node = node
        end

        def parent
          @connection.parent
        end

        def cursor
          @cursor ||= @connection.cursor_for(@node)
        end
      end

      def initialize(items, field:, affiliation:, order_by: nil, query: nil, **args)
        super items, max_page_size: Paginator.new(args, field, nil).max_per_page, **args

        @affiliation = affiliation
        @order_by = order_by || { field: "id", direction: "ASC" }
        @query = query

        @loaded = false
      end

      def parent
        @items
      end

      def nodes
        @nodes ||= if cursors_for_current_page.any?
          Platform::Loaders::ActiveRecord.load_all(::Repository, cursors_for_current_page.map(&:repository_id))
        else
          ::Promise.resolve([])
        end
      end

      def has_next_page
        load_data

        @has_next_page
      end

      def has_previous_page
        load_data

        @has_previous_page
      end

      def start_cursor
        cursors_for_current_page.first&.encode
      end

      def end_cursor
        cursors_for_current_page.last&.encode
      end

      def cursor_for(node)
        case @order_by[:field]
        when "watcher_count"
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.watcher_count, node.id], version: :v2)
        when "created_at"
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.created_at.iso8601, node.id], version: :v2)
        when "updated_at"
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.updated_at.iso8601, node.id], version: :v2)
        when "pushed_at"
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.pushed_at.iso8601, node.id], version: :v2)
        when "name"
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.name, node.id], version: :v2)
        when "action"
          action = Ability.where(actor_id: @items.id, actor_type: "Team", subject_id: node.id, subject_type: "Repository").pick(:action)

          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([action && Ability.actions[action], node.id], version: :v2)
        else
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.id], version: :v2)
        end
      end

      def total_count
        cursors_for_given_order.size
      end

      private

      def cursors_for_given_order
        @cursors ||= case @order_by[:field]
        when "action"
          cursors_for_action_order
        when "created_at"
          cursors_for_column_order(:created_at, OrderedByDateTimeCursor)
        when "pushed_at"
          cursors_for_column_order(:pushed_at, OrderedByDateTimeCursor)
        when "updated_at"
          cursors_for_column_order(:updated_at, OrderedByDateTimeCursor)
        when "name"
          cursors_for_column_order(:name, OrderedByNameCursor)
        when "watcher_count"
          cursors_for_column_order(:watcher_count, OrderedByWatcherCountCursor)
        else
          cursors_for_id_order
        end
      end

      def expanded_team_ids_for_affiliation
        case @affiliation
        when :immediate
          [@items.id]
        when :inherited
          @items.ancestor_ids
        when :all
          @items.id_and_ancestor_ids
        end
      end

      def affiliated_abilities_scope
        Ability.where(
          actor_type: "Team",
          actor_id: expanded_team_ids_for_affiliation,
          subject_type: "Repository",
          priority: Ability.priorities[:direct],
        )
      end

      def cursors_for_action_order
        ordered_action_by_repository_id = affiliated_abilities_scope.
          order(action: @order_by[:direction], subject_id: @order_by[:direction]).
          pluck(:subject_id, :action).
          each_with_object({}) { |(subject_id, action), result| result[subject_id] = Ability.actions[action] }

        ordered_repository_ids = ordered_action_by_repository_id.keys

        public_repository_ids = []
        private_repository_ids = []

        scope_for_org_repositories.pluck(:public, :id).each do |public, id|
          if public
            public_repository_ids << id
          else
            private_repository_ids << id
          end
        end

        # If there is a viewer, `scope_for_org_repositories` will include private repositories and we need
        # to filter ids down to those actually accessible by the viewer
        ordered_repository_ids &= public_repository_ids + accessible_repository_ids_from_private_repository_ids(private_repository_ids)

        if (oauth_app = context[:oauth_app])
          ordered_repository_ids &= ::Repository.oauth_app_policy_approved_repository_ids(
            repository_ids: ordered_repository_ids, app: oauth_app,
          )
        end

        ordered_repository_ids.map do |repository_id|
          OrderedByPermissionCursor.new(ordered_action_by_repository_id[repository_id], repository_id)
        end
      end

      def cursors_for_id_order
        ordered_repository_ids = []
        public_repository_ids = []
        private_repository_ids = []

        scope_for_org_repositories.order(id: @order_by[:direction]).pluck(:public, :id).each do |public, id|
          ordered_repository_ids << id

          if public
            public_repository_ids << id
          else
            private_repository_ids << id
          end
        end

        repository_ids_accessible_by_team = affiliated_abilities_scope.pluck(:subject_id)

        public_repository_ids &= repository_ids_accessible_by_team
        private_repository_ids &= repository_ids_accessible_by_team

        ordered_repository_ids &= public_repository_ids + accessible_repository_ids_from_private_repository_ids(private_repository_ids)

        if (oauth_app = context[:oauth_app])
          ordered_repository_ids &= ::Repository.oauth_app_policy_approved_repository_ids(
            repository_ids: ordered_repository_ids, app: oauth_app,
          )
        end

        ordered_repository_ids.map do |repository_id|
          OrderedByIdCursor.new(repository_id)
        end
      end

      def cursors_for_column_order(column, cursor_klass)
        ordered_column_by_repository_id = {}

        public_repository_ids = []
        private_repository_ids = []

        scope_for_org_repositories.order(column => @order_by[:direction], :id => @order_by[:direction]).pluck(:public, :id, column).each do |public, id, data|
          ordered_column_by_repository_id[id] = data

          if public
            public_repository_ids << id
          else
            private_repository_ids << id
          end
        end

        ordered_repository_ids = ordered_column_by_repository_id.keys

        repository_ids_accessible_by_team = affiliated_abilities_scope.pluck(:subject_id)

        public_repository_ids &= repository_ids_accessible_by_team
        private_repository_ids &= repository_ids_accessible_by_team

        ordered_repository_ids &= public_repository_ids + accessible_repository_ids_from_private_repository_ids(private_repository_ids)

        if (oauth_app = context[:oauth_app])
          ordered_repository_ids &= ::Repository.oauth_app_policy_approved_repository_ids(
            repository_ids: ordered_repository_ids, app: oauth_app,
          )
        end

        ordered_repository_ids.map do |repository_id|
          cursor_klass.new(ordered_column_by_repository_id[repository_id], repository_id)
        end
      end

      # For a given list of private repository ids, return ids for those repositories
      # that can be accessed by the current viewer
      def accessible_repository_ids_from_private_repository_ids(private_repository_ids)
        return [] unless context[:viewer]
        return private_repository_ids if private_repository_ids.empty?
        return private_repository_ids if GitHub.enterprise? && context[:viewer].site_admin?

        if context[:viewer].using_auth_via_integration?
          integration  = context[:viewer].oauth_access.application
          installation = context[:viewer].oauth_access.installation

          integration.accessible_repository_ids(
            current_integration_installation: installation,
            repository_ids: private_repository_ids,
          )
        else
          accessible_repository_ids = context[:viewer].associated_repository_ids(
            repository_ids: private_repository_ids,

            # OAuth app restrictions are handled in the `cursor_for_*` methods
            include_oauth_restriction: false,

            # All repositories we're interested in are owned by the org, so we don't need to include
            # repositories owned by the viewer (:owned)
            including: [:direct, :indirect],

            # We're only interested in repositories owned by the org
            include_indirect_forks: false,
            include_oopfs: false
          )

          organization = @items.organization
          if organization.supports_internal_repositories? && context[:viewer].business_ids.include?(organization.business.id)
            accessible_repository_ids |= private_repository_ids & organization.internal_repositories.ids
          end

          accessible_repository_ids
        end
      end

      def scope_for_org_repositories
        scope = ::Repository.active.where(owner: @items.organization, organization: @items.organization)

        query = ActiveRecord::Base.sanitize_sql_like(@query.to_s.strip.downcase)
        if query.present?
          scope = scope.where(["repositories.name LIKE ?", "%#{query}%"])
        end

        # If we know there is no viewer, we can ignore any private repositories
        scope = scope.public_scope unless context[:viewer]
        scope = scope.filter_spam_and_disabled_for(context[:viewer])
        scope
      end

      def cursors_type
        case @order_by[:field]
        when "pushed_at", "created_at", "updated_at"
          OrderedByDateTimeCursor
        when "watcher_count"
          OrderedByWatcherCountCursor
        when "action"
          OrderedByPermissionCursor
        when "name"
          OrderedByNameCursor
        else
          OrderedByIdCursor
        end
      end

      def cursors_for_current_page
        load_data

        @cursors_for_current_page
      end

      def load_data
        return if @loaded
        @loaded = true

        @has_next_page = @has_previous_page = false

        @cursors_for_current_page = cursors_for_given_order

        if before
          cursor = cursors_type.decode(before)

          cursor_offset = if @order_by[:direction] == "ASC"
            @cursors_for_current_page.bsearch_index { |c| c >= cursor }
          else
            @cursors_for_current_page.bsearch_index { |c| c <= cursor }
          end

          if cursor_offset
            @has_next_page = cursor_offset < @cursors_for_current_page.length
            @cursors_for_current_page = @cursors_for_current_page[0...cursor_offset]
          end
        end

        if after
          cursor = cursors_type.decode(after)

          cursor_offset = if @order_by[:direction] == "ASC"
            @cursors_for_current_page.bsearch_index { |c| c > cursor }
          else
            @cursors_for_current_page.bsearch_index { |c| c < cursor }
          end

          if cursor_offset
            @has_previous_page = cursor_offset > 0
            @cursors_for_current_page = @cursors_for_current_page[cursor_offset..-1]
          else
            @cursors_for_current_page = []
          end
        end

        if first && @cursors_for_current_page.size > first
          @cursors_for_current_page = @cursors_for_current_page.first(first)
          @has_next_page = true
        end

        if last && @cursors_for_current_page.size > last
          @cursors_for_current_page = @cursors_for_current_page.last(last)
          @has_previous_page = true
        end
      end
    end
  end
end
