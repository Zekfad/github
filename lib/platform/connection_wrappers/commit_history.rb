# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class CommitHistory
      include CursorGenerator

      class Edge
        attr_reader :node, :cursor

        def initialize(node, cursor)
          @node, @cursor = node, cursor
        end
      end

      attr_accessor :parent, :repository

      def initialize(repository, commit_oid, arguments, field: nil, max_per_page: 100, context: nil)
        @repository = repository
        @arguments = arguments
        @paginator = Paginator.new(arguments, field, max_per_page)

        if arguments[:last]
          @cursor = arguments[:before]
          if !@cursor
            raise Errors::MissingBackwardsPaginationArgument.new(field)
          else
            commit_oid, skip = self.class.decode_cursor(commit_oid, @cursor)
            skip -= arguments[:last]
          end
        else
          @cursor = arguments[:after]
          commit_oid, skip = self.class.decode_cursor(commit_oid, @cursor)
          if @cursor
            # We want to see _after_ the cursor, so increment the skip to bypass that commit
            skip += 1
          end
        end

        @commit_oid = commit_oid
        @skip = skip
        @limit = determine_limit(max_per_page, arguments[:first], arguments[:last])
      end

      def edges
        @edges ||= results.then { |oids|
          Loaders::GitObject.load_all(@repository, oids, expected_type: :commit).then { |commits|
            commits.each_with_index.map { |commit, index|
              Edge.new(commit, self.class.encode_cursor(@commit_oid, @skip + index))
            }
          }
        }
      end

      def edge_nodes
        self.edges.then { |edges| edges.map(&:node) }
      end

      def page_info
        @page_info ||= edges.then do |e|
          PageInfo.new(
            has_next_page: @has_next_page,
            has_previous_page: @skip > 0,
            start_cursor: e.first.try(:cursor),
            end_cursor: e.last.try(:cursor),
          )
        end
      end

      def self.encode_cursor(commit_oid, offset)
        "#{commit_oid} #{offset}"
      end

      def self.decode_cursor(commit_oid, cursor)
        return [commit_oid, 0] if !cursor

        if cursor =~ /\A([0-9a-f]{40}) (\d+)\z/i
          # a 40-char hex-encoded OID, followed by a space and an offset
          return [$1.downcase, $2.to_i]
        end

        decoded = CursorGenerator.resolve_cursor(cursor)

        case decoded
        when /\A(.{20})\+(\d+)\z/m
          # an OID packed to 20 bytes, followed by a "+" and an offset
          [$1.unpack("H40").first, $2.to_i]
        when /\A(\d+)\z/
          # a bare offset
          [commit_oid, $1.to_i]
        else
          raise Errors::Cursor.new(cursor)
        end
      end

      def git_args
        regexp_ignore_case = fixed_strings = false
        fetch_author_emails.then { |author_emails|
          if author_emails == []
            @has_next_page = false
            []
          else
            since_time = @arguments[:since] ? @arguments[:since].iso8601 : nil
            until_time = @arguments[:until] ? @arguments[:until].iso8601 : nil

            regexp_ignore_case = fixed_strings = true
            if author_emails
              author_emails.each_index do |i|
                if ::StealthEmail::STEALTH_EMAIL_REGEX =~ author_emails[i]
                  author_emails[i] = "#{$1}\+.*@#{GitHub.stealth_email_host_name}"
                  fixed_strings = false # we need to be able to do wildcard matching
                end
              end
            end

            if @skip < 0
              # The client is trying to paginate before the first entry, which is impossible.
              # So just use the check to see if there are any results at all.
              skip_results = 0
              max_results = 1
            else
              # Select up to one extra,
              # that way we can tell if there are more pages
              max_results = @limit + 1
              skip_results = @skip
            end

            {
              max: max_results,
              skip: skip_results,
              authors: author_emails || [],
              regexp_ignore_case: regexp_ignore_case,
              fixed_strings: fixed_strings,
              since_time: since_time,
              until_time: until_time,
              path: @arguments[:path] || nil,
            }
          end
        }
      end

    private
      def results
        @results ||= results!
      end

      def results!
        git_args.then { |args|
          if args.empty?
            []
          else
            begin
              oids = @repository.rpc.list_revision_history_multiple([@commit_oid], args)
            rescue GitRPC::ObjectMissing
              raise Errors::Cursor.new(@cursor)
            end
            if oids.length > @limit || @skip < 0
              @has_next_page = true
              # Remove the extra oid, which was checking for next page
              oids.pop
            else
              @has_next_page = false
            end
            oids
          end
        }
      end

      # Returns a promise of author emails based on the author argument passed
      # to the connection.
      #
      # A promise of nil indicates that no author filtering was requested.
      #
      # A promise of an empty array indicates that either no email addresses
      # were passed or that the user passed has no email addresses. In this
      # we return no commits. This deviates from Git's command-line behaviour,
      # where the absence of --author arguments means that no filtering is
      # performed.
      #
      # This will also return an empty array if a valid User-type Cursor is passed,
      # but the User cannot be found
      def fetch_author_emails
        if !@arguments[:author]
          return ::Promise.resolve(nil)
        end

        if @arguments[:author][:id]
          type_name, id = Platform::Helpers::NodeIdentification.from_global_id(@arguments[:author][:id])

          return ::Promise.resolve([]) unless %w[User Bot].include? type_name

          return Loaders::UserEmails.load(id.to_i)
        end

        if @arguments[:author][:emails]
          return ::Promise.resolve(@arguments[:author][:emails])
        end

        return ::Promise.resolve(nil)
      end

      # Get the smallest, non-nil number
      def determine_limit(max_page_size, first, last)
        limit = max_page_size
        if first && first < limit
          limit = first
        end
        if last && last < limit
          limit = last
        end
        limit
      end
    end
  end
end
