# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class Relation
      attr_reader :relation, :parent, :arguments, :max_page_size, :field

      def initialize(relation, arguments, field: nil, max_page_size: nil, parent: nil, context: nil)
        @arguments = arguments
        @paginator = Paginator.new(arguments, field, max_page_size)
        @parent = parent
        @relation, @orderings = Ordering.from_relation(relation)
        @field = field
      end

      # Public: Generate a cursor from an ActiveRecord object
      #
      # item - An ActiveRecord::Base instance (or Promise that resolves to one)
      #
      # Returns a Promise that resolves to a String cursor value
      def cursor_from_node(item)
        ::Promise.resolve(item).then do |item_sync|
          fetch_ordering(item_sync).then { |result| encode_cursor(result) }
        end
      end

      def edge_nodes
        @edge_nodes ||= ::Promise.all(results.map { |id| Loaders::ActiveRecord.load(@relation.klass, id) })
      end

      def page_info
        @page_info ||= begin
          page_info_promise = edge_nodes.then do |synced_nodes|
            start_cursor_promise = cursor_or_nil_from_node(synced_nodes.first)
            end_cursor_promise = cursor_or_nil_from_node(synced_nodes.last)
            ::Promise.all([start_cursor_promise, end_cursor_promise]).then do |start_cursor, end_cursor|
              PageInfo.new(connection: self, start_cursor: start_cursor, end_cursor: end_cursor)
            end
          end

          page_info_promise.sync
        end
      end

      # Zero out the expected results. Used in situations where the API AuthZ
      # check failed, and the resolvers are still expecting data to come through.
      # They should receive nothing, though.
      def none
        @edge_nodes = ::Promise.all([])
        self
      end

      def has_next_page
        # ensure initial data was loaded:
        results
        if !defined?(@has_next_page)
          @has_next_page = before ? inverse_relation_exists? : false
        end
        @has_next_page
      end

      def has_previous_page
        # ensure initial data was loaded:
        results
        if !defined?(@has_previous_page)
          @has_previous_page = after ? inverse_relation_exists? : false
        end
        @has_previous_page
      end

      def total_count
        # When taking the total count, remove the `ORDER BY` clause
        # because it doesn't change the outcome but it _does_
        # make the query harder for MySQL to run.
        # (Rails ends up making a `SELECT COUNT(*) FROM (...) AS subquery_for_count`)
        @total_count ||= @relation.unscope(:order).count
      end

    private

      def cursor_or_nil_from_node(synced_node)
        if synced_node
          cursor_from_node(synced_node)
        else
          ::Promise.resolve(nil)
        end
      end

      # Returns the `after` cursor argument or nil.
      #
      # If a `numericPage` is present, compute the equivalent `after` cursor
      # from the relation.
      def after
        return @after if defined?(@after)

        if page = @arguments[:numeric_page]
          return if page == 1

          per_page = @paginator.first
          last_page = (total_count / per_page.to_f).ceil

          page_containing_after_cursor  = page - 1
          offset_of_after_cursor        = (page_containing_after_cursor * per_page) - 1
          last_record_on_previous_page  = record_at_offset(page, last_page, offset_of_after_cursor)

          if last_record_on_previous_page.present?
            return @after = cursor_from_node(last_record_on_previous_page).sync
          end
        end

        @after = @arguments[:after]
      end

      # Returns the `before` cursor argument or nil.
      def before
        @arguments[:before]
      end

      # Returns an array of IDs
      def results
        @results ||= begin
          sliced_relation = apply_cursors(
            @relation,
            after: after,
            before: before,
            inverse: false,
          )

          apply_limits(
            sliced_relation,
            first: @paginator.first,
            last: @paginator.last,
          )
        end
      end

      # Apply `.where` conditions to `relation` so that it is constrained
      # within `after` and `before` cursors.
      #
      # If `inverse` is true, build a relation on the "other side" of the cursor,
      # including the cursor itself, with a reverse ordering.
      def apply_cursors(relation, after:, before:, inverse:)
        if after
          values = decode_cursor(after)
          cursor_direction = inverse ? :before : :after
          conditions = Condition.build(@orderings, values, cursor_direction, equal_to: inverse)
          relation = relation.where(conditions)
        end

        if before
          values = decode_cursor(before)
          cursor_direction = inverse ? :after : :before
          conditions = Condition.build(@orderings, values, cursor_direction, equal_to: inverse)
          relation = relation.where(conditions)
        end

        if inverse
          relation = relation.reverse_order
        end

        relation
      end

      # Look for items on the _other side_ of the cursor.
      # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
      def inverse_relation_exists?
        @inverse_relation_exists ||= apply_cursors(@relation, after: after, before: before, inverse: true).exists?
      end
      # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

      def apply_limits(relation, first:, last:)
        if first
          ids = relation.limit(first + 1).pluck(:id)
        elsif last
          ids = relation.reverse_order.limit(last + 1).pluck(:id).reverse
        else
          raise Errors::Internal, "must limit connection with either first or last"
        end

        if first
          @has_next_page = ids.count > first
          ids = ids.first(first)
        end

        if last
          @has_previous_page = ids.count > last
          ids = ids.last(last)
        end

        ids
      end

      def encode_cursor(cursor_values)
        CursorGenerator.generate_cursor(cursor_values, version: :v2)
      end

      def decode_cursor(opaque)
        decoded_cursor = CursorGenerator.resolve_cursor(opaque)

        # Recent cursors have more than one value
        if decoded_cursor.is_a?(Array)
          decoded_cursor
        else
          # Old cursors don't have a primary key
          [decoded_cursor, nil]
        end
      end

      # Private: Get values for the cursor based on orderings and the result record
      #
      # result - An ActiveRecord::Base instance the cursor will be based on
      #
      # Returns a Promise that resolves to an array of ordering values,
      # including a primary key tiebreaker
      def fetch_ordering(result)
        ::Promise.all(
          @orderings.map { |o| o.apply_to(result) },
        )
      end

      def record_at_offset(page, last_page, offset)
        if page <= last_page
          @relation.offset(offset).first
        else
          # FIXME: can we make this return a `NullRelation`?
          @relation.last
        end
      end
    end
  end
end
