# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class RemoteRelation < Relation
      attr_reader :count

      def initialize(remote_relation, arguments, field: nil, max_page_size: nil, parent: nil, context: nil)
        @count = remote_relation.count
        super(remote_relation.relation, arguments, field: field, max_page_size: max_page_size, parent: parent)
      end
    end
  end
end
