# frozen_string_literal: true

module Platform
  module Provisioning
    class SamlUserData < UserData
      user_data_reader name_id: "NameID"

      # This constant contains mappings between method names that will be created on a class and
      # how the values are stored in the attributes.
      # We are storing actual attribute names from a document with their values, this mapping consolidates
      # access to same attributes between SAML and SCIM user data.
      # Most of the time the attribute map will contain a single value for an attribute map, however
      # if an array is present the values in an array provide an override.  First existing attribute value
      # from an array map will be returned.
      #
      # Example:
      #   emails: ["emails", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"]
      #   #=> will check "emails" first and return a value for an attribute if the mapping was found
      #       otherwise if will continue and check "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"
      DEFAULT_ATTRIBUTE_MAPPINGS = {
        user_name: [
          "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name",
          "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress",
          "username",
          "NameID"
        ],
        external_id: [
          "http://schemas.microsoft.com/identity/claims/objectidentifier",
          "http://schemas.auth0.com/oid"
        ],
        display_name: [
          "http://schemas.microsoft.com/identity/claims/displayname",
          "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name",
          "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress",
          "full_name"
        ],
        emails: [
          "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress",
          "emails"
        ],
        given_name: "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname",
        family_name: "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname",
        active: "http://schemas.auth0.com/account_enabled",
        ssh_keys: "public_keys",
        gpg_keys: "gpg_keys",
        admin: "administrator",
      }

      # we will accept group data in attributes with one of these names
      GROUP_ATTRIBUTE_NAMES = [
        # "http://schemas.xmlsoap.org/claims/Group",
        "groups",
        "http://schemas.microsoft.com/ws/2008/06/identity/claims/groups",
      ]

      # These attributes are considered safe to persist.
      #
      # Unsafe attributes include (temporary) access tokens, unneeded personal
      # data.
      SAFE_LIST = [
        # AzureAD
        "http://schemas.microsoft.com/identity/claims/tenantid",
        "http://schemas.microsoft.com/identity/claims/objectidentifier",
        "http://schemas.microsoft.com/identity/claims/displayname",
        "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname",
        "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname",
        "http://schemas.auth0.com/oid",
        "http://schemas.auth0.com/account_enabled",

        # Generic
        "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn",
        "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress",
        "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name",

        # Groups
        GROUP_ATTRIBUTE_NAMES,
      ].flatten

      # Public: Instantiates a new UserData instance from the provided
      # SAML response.
      #
      # saml_assertion        - A SAML::Message::Response object containing the user data.
      # attribute_mapping     - a hash containing mapping of attributes, readers will be generated for
      # configurable_attrs    - An optional array of attributes from the Assertion to persist.
      #                           These can be configured by the administrator for Enterprise Server
      #
      # Returns a AttributeMappedUserData
      def self.load(saml_assertion, attribute_mapping: nil, persist_attributes: false, configurable_attrs: [])
        ret_user_data = self.new.tap do |user_data|
          user_data.append "NameID", saml_assertion.name_id,
            "Format" => saml_assertion.name_id_format

          if persist_attributes
            saml_assertion.attributes.each do |key, values|
              next unless SAFE_LIST.include?(key) || configurable_attrs.include?(key)
              values.each do |value|
                user_data.append key, value
              end
            end
          end
        end

        Platform::Provisioning::AttributeMappedUserData.new(ret_user_data, mapping: attribute_mapping)
      end

      # Public: Provides the default mapping for user data
      #
      # Returns a hash of default mappings for user data
      def default_attribute_mapping
        DEFAULT_ATTRIBUTE_MAPPINGS
      end
    end
  end
end
