# frozen_string_literal: true

# Represents an error occurring during provisioning

module Platform::Provisioning
  class Error
    ADD_MEMBER_ERROR = :add_member_error
    IDENTITY_NOT_FOUND_ERROR = :identity_not_found_error
    INTERNAL_ERROR = :internal_error
    INVALID_IDENTITY_ERROR = :invalid_identity_error
    INVITE_IDENTITY_ERROR = :invite_identity_error
    TARGET_AT_SEAT_LIMIT_ERROR = :target_at_seat_limit_error
    TWO_FACTOR_REQUIREMENT_NOT_MET_ERROR = :two_factor_requirement_not_met_error
    FORBIDDEN_ERROR = :forbidden_error
    RECONCILER_ERROR = :reconciler_error

    DEFAULT_MESSAGES = {
      add_member_error: "Unable to add user as an organization member",
      target_at_seat_limit_error: "There are no available seats",
      identity_not_found_error: "Unable to find the provisioned identity",
      internal_error: "An error occurred provisioning your account. Please try again",
      invalid_identity_error: "The identity was invalid",
      invite_identity_error: "Error sending invitation to provisioned identity",
      two_factor_requirement_not_met_error: "You must have two factor authentication enabled to join this organization",
      forbidden_error: "The action you are trying to perform is forbidden",
      reconciler_error: "Error syncing required user account attributes",
    }

    attr_reader :reason, :message

    def initialize(reason:, message: nil, fatal: false)
      @reason = reason
      @message = message || default_message_for_reason(reason)
      @fatal = fatal
    end

    def to_s
      message
    end

    # Public: is current error fatal?
    #
    # Returns true or false
    def fatal?
      @fatal
    end

    # Convenience methods for quickly constructing errors of different types.
    # Each takes a `message` kwarg to override the default of the error.

    def self.add_member(message: nil)
      new(reason: ADD_MEMBER_ERROR, message: message)
    end

    def self.identity_not_found(message: nil)
      new(reason: IDENTITY_NOT_FOUND_ERROR, message: message)
    end

    def self.internal_error(message: nil)
      new(reason: INTERNAL_ERROR, message: message)
    end

    def self.invalid_identity(message: nil)
      new(reason: INVALID_IDENTITY_ERROR, message: message)
    end

    def self.invite_identity(message: nil)
      new(reason: INVITE_IDENTITY_ERROR, message: message)
    end

    def self.two_factor_requirement_not_met(message: nil)
      new(reason: TWO_FACTOR_REQUIREMENT_NOT_MET_ERROR, message: message)
    end

    def self.target_at_seat_limit(message: nil)
      new(reason: TARGET_AT_SEAT_LIMIT_ERROR, message: message)
    end

    def self.forbidden_error(message: nil)
      new(reason: FORBIDDEN_ERROR, message: message)
    end

    def self.reconciler_error(message: nil, fatal: false)
      new(reason: RECONCILER_ERROR, message: message, fatal: fatal)
    end

    # Public: a Platform::Provisioning::Errors instance are the same if
    # * the error message is exactly the same
    # * the reason is exactly the same
    # ##########################
    def ==(other_error)
      eql?(other_error)
    end

    def eql?(other_error)
      @reason == other_error.reason && @message == other_error.message
    end

    def hash
      [@reason, @message].hash
    end

    # Failbot code expects things it's asked to report about to have a `backtrace` method:
    # https://github.com/github/failbot/blob/d764aed3c1d7462cbc84c6660c150608c3ab7dc4/lib/failbot.rb#L242
    def backtrace
      [@message]
    end

    private

    def default_message_for_reason(reason)
      DEFAULT_MESSAGES.fetch(reason)
    end
  end
end
