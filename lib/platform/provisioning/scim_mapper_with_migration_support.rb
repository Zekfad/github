# frozen_string_literal: true

# A specialized version of ScimMapper, used when we may be trying to migrate an existing
# ExternalIdentity to a new Saml provider. Will happen when an org or an enterprise changes
# its SAML provider configuration or when an org, with SAML enabled, joins an Enterprise where
# SAML is enabled. Without migrating the identities, SCIM messages run the risk of kicking out
# and re-inviting org members.
module Platform
  module Provisioning
    module SCIMMapperWithMigrationSupport

      # Builds a new or finds existing identity from SCIM user data
      #
      # Can find a previously provisioned identity that matches the given user_data and migrate it.
      # The user parameter is still not used here, but the returned ExternalIdentity will have
      # user_id set, if the previous identity we'll be migrating had it set.
      #
      # Returns: an ExternalIdentity (never nil, though may be un-saved)
      def self.find_or_build_identity(target:, user: nil, user_data:)
        identity = ScimMapper.find_or_build_identity(target: target, user: user, user_data: user_data)

        # there was an existing identity from this provider, so return it
        return identity if identity.persisted?

        identity_to_migrate = find_identity_for_migration(target: target, user_data: user_data)

        identity_to_migrate || identity
      end

      def self.set_user_data(identity:, user_data:)
        ScimMapper.set_user_data(identity: identity, user_data: user_data)
      end

      # Finds a previously provisioned identity for the given user_data. If we find one provisioned
      # by the current SamlProvider (likely means the org or enterprise has updated its SAML
      # settings), return it. If target is an Enterprise, and we find an identity provisioned by
      # a provider of one of its organizations, make a copy of it. We don't want to change the
      # org-provisioned identity, since it might be used again (if the org's SAML is ever
      # re-enabled: org leaves the Enterprise, or the Enterprise disables SAML)
      #
      # Returns: an ExternalIdentity (can be nil)
      def self.find_identity_for_migration(target:, user_data:)
        return nil unless target.saml_sso_enabled?
        provider = target.saml_provider

        # Look up an ExternalIdentity for this provider that was provisioned before the provider
        # was last updated. If the SAML configuration for an org/enterprise is changed, its
        # SamlProvider object (and all of its associated ExternalIdentity's) will persist, but
        # will be updated. So, look up identities whose `created_at` timestamp is before the
        # provider's `updated_at` timestamp. Happens when the IdP thinks it's starting from scratch
        # and needs to re-provision all the identities.
        external_identity = provider.external_identities.
          by_identifier(user_data, mapping: provider.identity_mapping,
                        identifier_attributes: provider.identity_mapping.scim_attribute).
          where("`external_identities`.created_at < ?", provider.updated_at).
          first

        return external_identity unless external_identity.nil?
        return nil unless provider.is_a?(Business::SamlProvider)

        org_saml_provider_ids = Organization::SamlProvider.
          where(organization_id: target.organization_ids).pluck(:id)
        return nil if org_saml_provider_ids.empty?

        # Look up ExternalIdentity's provisioned by SamlProvider's of the Enterprise's member orgs.
        # If we find one, that likely means that it's associated with a user who's already a member
        # of the org, so we can just use it and leave the member in place, instead of removing and
        # re-inviting them (if Enterprise Membership Provisioning is enabled and the IdP is
        # configured to talk to the /Groups SCIM endpoint)
        org_identity_to_migrate = ExternalIdentity.
          where(provider_id: org_saml_provider_ids, provider_type: Organization::SamlProvider).
          by_identifier(user_data, mapping: provider.identity_mapping).
          first

        return nil if org_identity_to_migrate.nil?

        # Don't use the identity directly - make a copy and leave the original in place, it case it
        # needs to be used again (if org SAML gets re-enabled)
        external_identity = ExternalIdentity.new(
          user_id: org_identity_to_migrate.user_id,
          provider_id: provider.id,
          provider_type: Business::SamlProvider
        )
        # Grab any SAML data from the old identity; let the current operation set the new SCIM data
        if org_identity_to_migrate.saml_user_data.any?
          external_identity.saml_user_data = org_identity_to_migrate.saml_user_data.dup
        end

        external_identity
      end
    end
  end
end
