# frozen_string_literal: true

# Defines the attributes that should be equivalent between SAML and SCIM
module Platform::Provisioning
  class IdentityMapping

    def saml_attribute
      "NameID"
    end

    def scim_attribute
      "userName"
    end

    def identifier_attributes
      [scim_attribute, saml_attribute]
    end

    # Look up the identifier value from the given Platform::Provisioning::UserData
    # Prefers the scim_attribute
    def value_from_user_data(user_data)
      value   = user_data.fetch(scim_attribute, {})["value"]
      value ||= user_data.fetch(saml_attribute, {})["value"]
      value
    end
  end
end
