# frozen_string_literal: true
# rubocop:disable GitHub/UsePlatformErrors

module Platform
  module Provisioning
    class UserDataReconciler

      # Standard error messages
      LOGIN_CONFLICT = "Another user already owns the account. Please have your administrator check the authentication log."
      MISSING_LOGIN = "The response did not contain either a valid NameID or a valid configured Username attribute."

      attr_reader :user, :user_data

      # Result of a reconciliation.  Can contain a list of errors.  First fatal error will stop processing
      class Result
        attr_reader :user

        def initialize(user:, errors:)
          @user = user
          @errors = errors
        end

        # Public: Was the result of reconciliation successful?
        #
        # Returns true or false
        def success?
          errors.empty?
        end

        # Public: Check if any of the errors were designated as fatal by the reconciler
        #
        # Returns true or false
        def has_fatal?
          errors.any? { |k, v| v.fatal? }
        end

        # Public: Attribute reader for errors
        #
        # Returns errors when the value for error is not nil
        def errors
          @errors.reject { |k, v| v.nil? }
        end

        # Public: Error message reader for a given reconciler
        #
        # Returns error message for a given reconciler
        def error_message(message_for)
          @errors[message_for].message unless @errors[message_for].nil?
        end

        # Public: Convert a hash of errors to an array of errors
        #
        # Returns an array of errors
        def error_values
          errors.values
        end
      end

      # Public: Create a UserDataReconciler class by passing user and a user data
      def initialize(user, user_data, is_private_instance:)
        @user = user
        @user_data = user_data
        @is_private_instance = is_private_instance
      end

      # Public: A class reconcile method for user and a user data
      #
      # Returns Result of reconciliation
      def self.reconcile(user, user_data, is_private_instance: false)
        raise ArgumentError, "expected user to be not blank" if user.nil?
        raise ArgumentError, "expected user_data to be not blank" if user_data.nil?

        new(user, user_data, is_private_instance: is_private_instance).reconcile
      end

      # Internal: A reconcile method, reconciles each value.
      #           First fatal error will stop further reconciliation.
      #
      # Returns a Result of a reconciliation
      def reconcile
        errors = {}

        errors[:user_name] = reconcile_user_name
        errors[:admin] = reconcile_admin
        errors[:display_name] = reconcile_display_name
        errors[:emails] = reconcile_emails
        errors[:ssh_keys] = reconcile_ssh_keys
        errors[:gpg_keys] = reconcile_gpg_keys

        Result.new(user: user, errors: errors)
      end

      private

      # Internal: Global configuration check for disabling admin functionality
      #
      # Returns true or false
      def disable_admin_demote?
        GitHub.auth.configuration[:disable_admin_demote]
      end

      # Public: Is the debug logging enabled for SAML or SCIM
      #
      # Returns true if debug logging is enabled, otherwise false
      def debug_logging_enabled?
        GitHub.config.get("saml.debug_logging_enabled") == "true"
      end

      # Private: Reconcile user name based on the value in user_data
      #
      # Returns nothing or an error condition
      def reconcile_user_name
        # This is just a precaution, this error should never fire, since the user should not be selected in
        # the code calling the reconciler.  Unless it is a patch and the selection of a user is changed to use
        # object identifier.  Setting user name to empty should result in a fatal error.
        return Error.reconciler_error(message: MISSING_LOGIN, fatal: true) if user_data.user_name.blank?

        login = User.standardize_login(user_data.user_name)

        # If the user login is the same short circuit and exit
        return if user.login.downcase == login.downcase

        # check if the user_name/login is already used, fatal error if it is
        dup_user = User.find_by_login(login)
        return Error.reconciler_error(message: LOGIN_CONFLICT, fatal: true) unless dup_user.nil?

        log(at: "Changing user login to #{login}", login: user.login)

        error = save_transaction(user) do
          success = user.rename(login)
          Error.reconciler_error(message: user.errors.full_messages) unless success
        end

        return error if error
      end

      # Private: Reconciles a User's admin status based on the admin entry in the user data.
      #
      # Returns nothing or an error when setting access failed
      def reconcile_admin
        return if disable_admin_demote?

        value = user_data.admin

        # If the value was not set do nothing, return
        return if value.blank?

        # return if no changes are necessary
        return if value&.index("true") && user.site_admin? || value&.index("false") && !user.site_admin?

        error = save_transaction(user) do
          success = false
          if value&.index("true")
            type = "promote user to admin"
            log(at: type, login: user.login)
            success = user.grant_site_admin_access("saml/scim single sign-on administrator promotion")
          else
            type = "demote user from admin"
            log(at: type, login: user.login)
            success = user.revoke_privileged_access("saml/scim single sign-on administrator demotion")
          end

          Error.reconciler_error(message: "Unable to #{type}") unless success
        end

        return error if error
      end

      # Private: Reconcile a User's display name (profile  name) based on the display_name attribute in user data.
      #
      # Returns nothing or an error when setting profile name failed
      def reconcile_display_name
        profile = user.profile || user.build_profile
        display_name = user_data.display_name.blank? ? user_data.user_name : user_data.display_name
        return if user.profile_name == display_name

        error = save_transaction(user) do
          log(at: "changing user display name to #{display_name}", login: user.login)
          user.profile_name = display_name
        end

        return error if error
      end

      # Private: Reconcile emails distinguishing between private instance and enterprise server
      #
      # Returns nothing or an error when setting emails failed
      def reconcile_emails
        if @is_private_instance
          reconcile_private_instance_emails
        else
          reconcile_enterprise_server_emails
        end
      end

      # Private: Reconcile a User's emails based on the emails from user data for enterprise server
      #
      # Returns nothing or an error when setting emails failed
      def reconcile_enterprise_server_emails
        # get assertion emails
        emails = user_data.emails

        # get current emails
        current_downcased_emails = user.emails.map(&:email).map(&:downcase)

        # compare emails
        new_emails = emails.reject { |email| current_downcased_emails.include? email.downcase }

        # short circuit if user's emails match user data entry's emails
        return if new_emails.empty?

        # we want to save all changes as a single transaction
        new_emails.each do |email|
          log(at: "adding user email name to #{email}", login: user.login)
          user.add_email(
            email,
            rebuild_contributions: false,
          )
        end

        # rebuild contributions once for all emails added instead of for each
        # one independently
        user.rebuild_contributions
        nil
      end

      # Private: Reconcile a User's emails based on the emails from user data for private instance
      #
      # Returns nothing or an error when setting emails failed
      def reconcile_private_instance_emails
        emails = user_data.emails.map(&:downcase)
        primary_email = user_data.primary_email.presence&.downcase || emails.first

        # short circuit if user's emails match user data entry's emails
        return if emails_synced?(primary_email, emails)

        # we want to save all changes as a single transaction
        error = save_transaction(user) do
          # clear out existing emails completely
          user.email_roles.delete_all
          user.emails.destroy_all

          user_data.emails.each do |email|
            log(at: "adding user email name to #{email}", login: user.login)
            user_email = user.add_email(
              email,
              is_primary: primary_email == email.downcase,
              rebuild_contributions: false,
            )

            break Error.reconciler_error(message: user_email.errors.full_messages) unless user_email.valid?
          end
        end

        return error if error

        # rebuild contributions once for all emails added instead of for each
        # one independently
        user.rebuild_contributions
        nil
      end

      # Internal: Reconcile a User's public keys.
      #
      # Returns nothing.
      def reconcile_ssh_keys
        keys = user_data.ssh_keys

        fingerprints = keys.each_with_object({}) do |k, result|
          parsed_key =
            begin
              SSHData::PublicKey.parse(k)
            rescue SSHData::Error
              log(at: "failed to decode public key", login: user.login)
              nil
            end

          next unless parsed_key

          fingerprint = parsed_key.fingerprint(md5: true)
          result[fingerprint] = k
        end

        new_fingerprints = fingerprints.keys - user.public_keys.pluck(:fingerprint)

        errors = []
        new_fingerprints.each do |f|
          save_error = save_transaction(user) do
            if public_keys = user.add_public_key(fingerprints[f])
              public_key = public_keys.first
              public_key.verify(user)  # verify new keys released by idP
            end
          end
          errors += [save_error] if save_error
        end

        return if errors.count == 0
        Error.reconciler_error(message: errors.values.collect { |v| v.message }.join("\n"))
      end

      # Internal: Reconcile a User's GPG keys.
      #
      # Returns nothing.
      def reconcile_gpg_keys
        keys = user_data.gpg_keys

        errors = []
        keys.each do |k|
          save_error = save_transaction(user) do
            user.gpg_keys.create_from_armored_public_key(k)
          end
          errors += [save_error] if save_error
        end

        return if errors.count == 0
        Error.reconciler_error(message: errors.values.collect { |v| v.message }.join("\n"))
      end

      # Private: Verifies that the User's emails match the user data entry's
      # emails.
      #
      # Returns true if the primary email matches up and the rest of the emails
      # are accounted for.
      def emails_synced?(data_primary, data_emails)
        # gather emails as Strings, separating the primary
        data_rest = data_emails - [data_primary]
        user_primary, *user_rest = user.emails.primary_first.map(&:email).map(&:downcase)

        # compare the primaries as Strings, and the rest as Sets (order doesn't matter)
        primary_match = data_primary == user_primary
        rest_match    = Set.new(data_rest) == Set.new(user_rest)

        # validate both primary and rest match between user data and User record
        primary_match && rest_match
      end

      # Private: Login method that checks if the login was enabled
      #
      # Returns nothing
      def log(data)
        return unless debug_logging_enabled?
        GitHub.auth.log(data)
      end

      # Private: Save current user data, rollback if errors were found
      #
      # Returns nothing or a fatal error
      def save_transaction(obj)
        error = nil

        obj.transaction(requires_new: true) do
          begin
            error = yield if block_given?
            raise ActiveRecord::Rollback if error && error.is_a?(Error)

            return if obj.save
          rescue ActiveRecord::RecordInvalid
            # revert back to previous value to restore state since it failed to rename
            error = Error.reconciler_error(message: obj.errors.first)
            raise ActiveRecord::Rollback
          end
        end

        return error if error.is_a?(Error)
      end
    end
  end
end
