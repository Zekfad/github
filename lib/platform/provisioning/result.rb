# frozen_string_literal: true

module Platform::Provisioning
  class Result
    attr_reader :external_identity, :errors, :pending

    def initialize(external_identity: nil, errors: [], pending: false)
      @external_identity = external_identity
      @errors = Platform::Provisioning::Errors.new(errors)
      @pending = pending
    end

    def self.target_at_seat_limit_error(external_identity: nil, message: nil)
      new(
        external_identity: external_identity,
        errors: Error.target_at_seat_limit(message: message),
      )
    end

    def self.success(external_identity: nil)
      new(external_identity: external_identity)
    end

    # Indicates that provisioning has not yet completed.
    # E.g. organization membership is being restored in the background.
    def self.pending(external_identity: nil)
      new(external_identity: external_identity, pending: true)
    end

    def success?
      errors.none?
    end

    def pending?
      errors.none? && !!pending
    end

    def internal_error?
      errors.any? { |e| e.reason == Error::INTERNAL_ERROR }
    end

    def invalid_identity_error?
      errors.any? { |e| e.reason == Error::INVALID_IDENTITY_ERROR }
    end

    def add_member_error?
      errors.any? { |e| e.reason == Error::ADD_MEMBER_ERROR }
    end

    def identity_not_found_error?
      errors.any? { |e| e.reason == Error::IDENTITY_NOT_FOUND_ERROR }
    end

    def invite_identity_error?
      errors.any? { |e| e.reason == Error::INVITE_IDENTITY_ERROR }
    end

    def target_at_seat_limit_error?
      errors.any? { |e| e.reason == Error::TARGET_AT_SEAT_LIMIT_ERROR }
    end

    def two_factor_requirement_not_met_error?
      errors.any? { |e| e.reason == Error::TWO_FACTOR_REQUIREMENT_NOT_MET_ERROR }
    end

    def forbidden_error?
      errors.any? { |e| e.reason == Error::FORBIDDEN_ERROR }
    end

    # Public: The error messages (if any) that resulted from the validation.
    #
    # Returns an Array of Strings.
    def error_messages
      errors.map(&:message)
    end

    # Public: Determine the user associated with the external_identity
    #
    # Returns a User
    def user
      external_identity&.user
    end
  end
end
