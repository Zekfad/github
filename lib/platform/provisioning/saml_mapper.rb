# frozen_string_literal: true

# Mapper responsible for taking UserData provided by the identity provider
# via SAML protocol and mapping that to an ExternalIdentity.
module Platform::Provisioning
  module SamlMapper

    # Public: Finds or builds an external identity for a user and an Organization or Business
    # as specified by the passed provisioning user data.
    #
    # target        - The Organization or Business which the identity will be under.
    # user          - (Optional) The User to whom the identity will be linked.
    # user_data     - The Platform::Provisioning::UserData which specifies any
    #                 information about the user provided by the IdP.
    #
    # Returns an ExternalIdentity.
    def self.find_or_build_identity(target:, user: nil, user_data:)
      if user
        identity_for_user(target: target, user: user, user_data: user_data)
      else
        identity_for_anon(target: target, user_data: user_data)
      end
    end

    # Internal: Finds or builds an external identity for an Organization or Business
    # as specified by the passed provisioning user data.
    #
    # If a provisioned identity already exists for the specified name_id, it is returned
    # even if it is already provisioned and linked to a user. We only care if we have
    # a record of the identity. This is useful for cases where an anonymous user goes
    # through the single sign on flow since they are able to sign in after going through
    # the flow.
    #
    # If no existing identity exists, a new one will be built.
    #
    # target        - The Organization or Business which the identity will be under.
    # user_data     - The Platform::Provisioning::UserData which specifies any
    #                 information about the user provided by the IdP.
    #
    # Returns an ExternalIdentity.
    def self.identity_for_anon(target:, user_data:)
      provider = target.external_identity_session_owner.saml_provider

      identity = ExternalIdentity.by_provider(provider).
        by_identifier(user_data, mapping: provider.identity_mapping).first

      identity ||= ExternalIdentity.new(provider: provider)

      identity
    end

    # Internal: Finds or builds an external identity for a user and an Organization or Business
    # as specified by the passed provisioning user data.
    #
    # If an identity already exists for this user, it is returned even if the
    # name_id doesn't match the one provided by user_data. We'll update the identity
    # to use the latest name_id provided by the identity provider.
    #
    # If an identity already exists for the specified name_id which is not yet
    # linked to a user, link it to this user and return it.
    #
    # Otherwise build a brand new identity for this user using the provided
    # UserData.
    #
    # target        - The Organization or Business which the identity will be under.
    # user          - The User whom the identity will be linked.
    # user_data     - The Platform::Provisioning::UserData which specifies any
    #                 information about the user provided by the IdP.
    #
    # Returns an ExternalIdentity.
    def self.identity_for_user(target:, user:, user_data:)
      provider = target.external_identity_session_owner.saml_provider

      # Look for an identity provisioned by SCIM which matches the user's emails first.
      identity = link_unlinked_scim_identity!(provider: provider, user: user, user_data: user_data)

      # Existing linked identity
      identity ||= ExternalIdentity.by_provider(provider).
        linked_to(user).first

      identity ||= ExternalIdentity.by_provider(provider).unlinked.
        by_identifier(user_data, mapping: provider.identity_mapping).first

      identity ||= ExternalIdentity.new(provider: provider, user: user)

      identity.user = user
      identity
    end

    def self.link_unlinked_scim_identity!(provider:, user:, user_data:)
      if identity = ExternalIdentity.by_provider(provider).unlinked.
        by_scim_emails(UserEmail.safe_bulk_normalize(users: user, verified: true)).first
        # If the user already had an identity in the org via manual SSO,
        # linking this identity will fail since it's a duplicate. Remove any
        # existing identities first before linking this one.
        ExternalIdentity.unlink(provider: provider, user: user)

        # Also remove any unlinked identities which have the same identifier
        # and make sure we do not remove the currently selected candidate identity
        ExternalIdentity.by_provider(provider).unlinked
          .by_identifier(user_data, mapping: provider.identity_mapping)
          .where.not(id: identity.id)
          .destroy_all
      end

      identity
    end

    def self.set_user_data(identity:, user_data:)
      identity.saml_user_data = user_data

      return identity unless identity.provider.is_a?(Business::SamlProvider)

      scim_groups_wrapper = Platform::Provisioning::GroupsUserDataWrapper.new(identity.scim_user_data)
      return identity if scim_groups_wrapper.groups.empty?

      group_ids = identity.provider.group_ids(user_data: identity.saml_user_data)
      identity.scim_user_data = \
        scim_groups_wrapper.sync_group_attributes(valid_group_ids: group_ids)

      identity
    end
  end
end
