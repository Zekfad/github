# frozen_string_literal: true

module Platform
  module Interfaces
    module Updatable
      include Platform::Interfaces::Base
      description "Entities that can be updated."

      field :viewer_can_update, Boolean, description: "Check if the current viewer can update this object.", null: false

      def viewer_can_update
        @object.async_viewer_can_update?(@context[:viewer])
      end
    end
  end
end
