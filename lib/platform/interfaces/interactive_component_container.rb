# frozen_string_literal: true
module Platform
  module Interfaces
    module InteractiveComponentContainer
      include Platform::Interfaces::Base

      areas_of_responsibility :ce_extensibility

      description "An object that can have interactive components."

      visibility :under_development

      field :is_launch_app, Boolean, description: "Boolean flag determining whether the integration that made this container is the launch app.", null: false

      def is_launch_app
        @object.async_integration.then do |integration|
          return false unless integration
          integration.launch_github_app? || integration.launch_lab_github_app?
        end
      end

      field :viewer_interaction, Objects::ViewerInteraction, "Indicates how the viewer may interact with the composable comment", null: false

      def viewer_interaction
        @object.async_integration.then do |integration|
          if integration.nil?
            next { can_interact: false, error: "This GitHub App has been deleted" }
          end

          case @object.associated_domain
          when InteractiveComponent::Container::ASSOCIATED_DOMAINS[:repo]
            check_integration_installed_for_repo(integration)
          end
        end
      end

      private def check_integration_installed_for_repo(integration)
        @object.async_repository.then do |repository|
          integration.async_installation_for(repository).then do |installation|
            if installation.nil?
              next { can_interact: false, error: "This GitHub App is no longer installed" }
            end
            { can_interact: true, error: nil }
          end
        end
      end
    end
  end
end
