# frozen_string_literal: true

module Platform
  module Interfaces
    module AccountStafftoolsInfo
      include Platform::Interfaces::Base
      description "Common stafftools info fields for user and organization accounts."
      visibility :internal

      field :last_ip, String, description: "The ip address for the account recorded when they last logged in.", null: true

      def last_ip
        @object.account.last_ip
      end

      field :ip_neighbors, Connections.define(Unions::Account), description: "The accounts that show share the same lastIp", null: true, connection: true do
        argument :prefix, Enums::NetworkPrefix, Enums::NetworkPrefix.description, default_value: 24, required: false
      end

      def ip_neighbors(**arguments)
        ::User.by_ip_with_prefix(@object.account.last_ip, prefix: arguments[:prefix])
      end

      field :ip_neighbors_count, Integer, description: "The number of accounts with the same last ip.", null: false

      def ip_neighbors_count
        last_ip = @object.account.last_ip
        return 0 if last_ip.nil?

        cache = @context.namespace(:ip_neighbor_counts)
        cache[last_ip] ||= ::User.by_ip(last_ip).count
      end

      field :spammy_ip_neighbors_count, Integer, description: "The number of spammy accounts with the same last ip.", null: false

      def spammy_ip_neighbors_count
        last_ip = @object.account.last_ip
        return 0 if last_ip.nil?

        cache = @context.namespace(:spammy_ip_neighbor_counts)
        cache[last_ip] ||= ::User.spammy.by_ip(last_ip).count
      end

      field :network_address32_reputation, Objects::SpamuraiReputation, description: "IP address reputation.", null: true

      def network_address32_reputation
        last_ip = @object.account.last_ip
        return if last_ip.nil?

        prefix = 32
        cache = @context.namespace(:network_address_32_counts)
        cache[last_ip] ||= {
          sample_size: ::User.by_ip_with_prefix(last_ip, prefix: prefix).count,
          not_spammy_sample_size: ::User.not_spammy.by_ip_with_prefix(last_ip, prefix: prefix).count,
        }

        Spam::Reputation.generate(
          sample_size: cache[last_ip][:sample_size],
          not_spammy_sample_size: cache[last_ip][:not_spammy_sample_size]
        )
      end

      field :network_address24_reputation, Objects::SpamuraiReputation, description: "Class C reputation.", null: true

      def network_address24_reputation
        last_ip = @object.account.last_ip
        return if last_ip.nil?

        prefix = 24
        cache = @context.namespace(:network_address_24_counts)
        cache[last_ip] ||= {
          sample_size: ::User.by_ip_with_prefix(last_ip, prefix: prefix).count,
          not_spammy_sample_size: ::User.not_spammy.by_ip_with_prefix(last_ip, prefix: prefix).count,
        }

        Spam::Reputation.generate(
          sample_size: cache[last_ip][:sample_size],
          not_spammy_sample_size: cache[last_ip][:not_spammy_sample_size]
        )
      end

      field :is_spammy, Boolean, description: "Is the account spammy.", null: false

      def is_spammy
        @object.account.spammy?
      end

      field :spammy_reason, String, description: "The spammy reason.", null: true

      def spammy_reason
        @object.account.spammy_reason
      end

      field :is_hammy, Boolean, description: "Is the account hammy.", null: false

      def is_hammy
        @object.account.hammy?
      end

      field :is_never_spammy, Boolean, description: "Can this account be marked as spammy.", null: false

      def is_never_spammy
        @object.account.never_spammy?
      end

      field :is_suspended, Boolean, description: "Is the account suspended.", null: false

      def is_suspended
        @object.account.suspended?
      end

      field :has_paid_plan, Boolean, description: "Does account have a paid plan.", null: false

      def has_paid_plan
        @object.account.paid_plan?
      end

      field :is_gift_account, Boolean, description: "Is the account classified as a gift account.", null: false

      def is_gift_account
        @object.account.gift?
      end

      field :has_blacklisted_payment_method, Boolean, description: "Does the account have a blacklisted payment method.", null: false

      def has_blacklisted_payment_method
        BlacklistedPaymentMethod.where(user_id: @object.account.id).exists?
      end

      field :has_matching_last_ip_spam_pattern, Boolean, description: "Account has matching spam pattern on last ip.", null: false

      def has_matching_last_ip_spam_pattern
        account = @object.account
        return false if account.last_ip.nil?

        @context[:last_ip_spam_patterns_regexp] ||= SpamPattern.last_ip_patterns_regexp

        @context[:last_ip_spam_patterns_regexp].match?(account.last_ip)
      end

      field :has_newer_non_spammy_ip_neighbor, Boolean, description: "Account has newer non-spammy ip neighbor.", null: false

      def has_newer_non_spammy_ip_neighbor
        account = @object.account
        return false if account.last_ip.nil?
        ::User.not_spammy.by_ip(account.last_ip).where("id > ?", account.id).exists?
      end

      field :lfs_repositories, Connections::Repository, description: "The repositories for this account that have LFS objects.", null: false, connection: true

      def lfs_repositories
        Loaders::LfsRepositories.load(@object.account.id).then do |repos|
          ArrayWrapper.new(repos.sort_by(&:id))
        end
      end

      field :lfs_networks_by_usage, Connections::Repository, description: "The networks for this account that have LFS usage, .", null: true, connection: true

      def lfs_networks_by_usage
        Loaders::LfsNetworksByUsage.load(@object.account.id)
      end

      field :time_zone, String, description: "The account time zone.", null: true

      def time_zone
        ActiveSupport::TimeZone[@object.account.time_zone_name.to_s]
      end

      field :staff_notes, Connections.define(Objects::StaffNote), "Staff notes for account.",
        null: true, connection: true

      def staff_notes
        @object.account.staff_notes
      end

      field :profile, Objects::Profile, "Account profile.", null: true

      def profile
        @object.account.async_profile
      end

      field :is_trade_restricted, Boolean, description: "Indicates if the account is subject to trade restrictions.", null: false

      def is_trade_restricted
        @object.account.has_any_trade_restrictions?
      end

      field :owned_repositories_count, Integer, description: "The number of repositories this account owns.", null: false do
        argument :visibility, Enums::RepositoryPrivacy, "If non-null, filters repositories according to visibility.", required: false
      end

      def owned_repositories_count(**arguments)
        if arguments[:visibility] == "private"
          @object.account.private_repositories.count
        elsif arguments[:visibility] == "public"
          @object.account.public_repositories.count
        else
          @object.account.repositories.count
        end
      end

      field :associated_repositories_count, Integer, description: "The number of repositories this account is asssociated with.", null: false do
        argument :affiliations, [Enums::RepositoryAffiliation, null: true],
          description: "Array of owner's affiliation options for repository count. For example, OWNER will include only repositories that the organization or user being viewed owns.",
          required: false,
          default_value: [:owned, :direct]

        argument :visibility, Enums::RepositoryPrivacy,
          description: "If non-null, filters repositories according to visibility.",
          required: false
      end

      def associated_repositories_count(affiliations:, visibility: nil)
        scope = if visibility == "private"
          ::Repository.private_scope
        elsif visibility == "public"
          ::Repository.public_scope
        else
          ::Repository
        end

        scope
          .from("repositories FORCE INDEX (PRIMARY)")
          .where(id: @object.account.associated_repository_ids(including: affiliations))
          .count
      end

      field :has_actually_paid_money, Boolean, description: "Has actually paid money to GitHub at some point.", null: false

      def has_actually_paid_money
        return false if @object.account.type == "Bot"

        # The account is high value and doesn't pay through the normal billing
        # setup.
        return true if @object.account.invoiced?

        # A chargeback should not be counted as a successful transaction for
        # our purposes here.
        #
        # Presumably, successful statuses could change over time. By including
        # all success statuses by default (and removing just the one we care
        # about), we're erring on the side of giving accounts the benefit of
        # the doubt. Given that this field may be used to determine if an
        # account should be spamflagged, erring in this direction seems
        # preferable to adding false positives.
        successful_statuses = Billing::BillingTransactionStatuses::SUCCESS
          .except(:charged_back)
          .values

        rows = Billing::BillingTransaction.github_sql.results(<<-SQL, user_id: @object.account.id, successful_statuses: successful_statuses)
          SELECT sale.id
          FROM billing_transactions AS sale
          LEFT JOIN billing_transactions AS refund
          ON sale.transaction_id = refund.sale_transaction_id
          WHERE sale.user_id = :user_id
          AND refund.id IS NULL
          AND sale.amount_in_cents > 0
          AND sale.transaction_type != 'refund'
          AND sale.last_status IN :successful_statuses
          LIMIT 1
        SQL

        rows.present?
      end
    end
  end
end
