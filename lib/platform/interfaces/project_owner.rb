# frozen_string_literal: true

module Platform
  module Interfaces
    module ProjectOwner
      include Platform::Interfaces::Base
      description "Represents an owner of a Project."

      def self.add_project_owner_fields(interface_defn, visibility:)
        interface_defn.module_eval do
          global_id_field :id

          field :project, Objects::Project, minimum_accepted_scopes: ["public_repo"], description: "Find project by number.", null: true , visibility: visibility do
            argument :number, Integer, "The project number to find.", required: true
          end

          def project(**arguments)
            Loaders::ProjectByNumber.load(@object, arguments[:number])
          end

          field :viewer_can_create_projects, Boolean, description: "Can the current viewer create new projects on this owner.", null: false, visibility: visibility

          def viewer_can_create_projects
            # TODO: Use batch ability loader to check permissions
            result = @context[:viewer] && Project.viewer_can_create_projects?(project_owner: @object, viewer: @context[:viewer])
            result ? true : false
          end

          field :projects, resolver: Resolvers::Projects, minimum_accepted_scopes: ["public_repo"], numeric_pagination_enabled: true, visibility: visibility, description: "A list of projects under the owner.", connection: true

          url_fields prefix: :projects, visibility: visibility, description: "The HTTP URL listing owners projects"
          url_fields prefix: :new_project, visibility: :internal, description: "The HTTP URL to create new projects"
        end
      end

      add_project_owner_fields(self, visibility: :public)
    end
  end
end
