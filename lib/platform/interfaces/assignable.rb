# frozen_string_literal: true

module Platform
  module Interfaces
    module Assignable
      include Platform::Interfaces::Base
      description "An object that can have users assigned to it."

      field :assignees, resolver: Resolvers::Assignees, description: "A list of Users assigned to this object.", connection: true
    end
  end
end
