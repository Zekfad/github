# frozen_string_literal: true

module Platform
  module Interfaces
    module Sponsorable
      include Interfaces::Base

      description "Entities that can be sponsored through GitHub Sponsors"

      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      field :sponsors_listing, Objects::SponsorsListing, description: "The GitHub Sponsors listing for this user.", null: true

      def sponsors_listing
        @object.async_sponsors_listing.then do |listing|
          next unless listing.present?

          listing.async_readable_by?(@context[:viewer]).then do |readable_by|
            next unless readable_by
            listing
          end
        end
      end

      field :has_sponsors_listing, Boolean, visibility: :internal, description: "True if the user has a GitHub Sponsors listing.", null: false, method: :async_has_sponsors_listing

      field :sponsors, Connections::Sponsor, visibility: :internal, description: "List of sponsors for this user.", null: false, connection: true do
        argument :tier_id, ID, "If given, will filter for sponsors on the given tier", required: false
      end

      def sponsors(tier_id: nil)
        include_private = @object == @context[:viewer]
        tier = Platform::Helpers::NodeIdentification.typed_object_from_id(Objects::SponsorsTier, tier_id, @context) if tier_id

        @object.async_sponsors(include_private: include_private, tiers: tier)
      end

      field :sponsoring, Connections::Sponsor, visibility: :internal, description: "List of maintainers this user is sponsoring.", null: false, connection: true

      def sponsoring
        include_private = @object == @context[:viewer]

        @object.async_sponsoring(include_private: include_private)
      end

      field :is_public_github_sponsor, Boolean, visibility: :internal, description: "True if the user is publicly sponsoring any maintainers", null: false, method: :async_is_public_github_sponsor?

      field :viewer_is_sponsoring, Boolean, visibility: :internal, description: "True if the viewer is sponsoring this user.", null: false

      def viewer_is_sponsoring
        return false unless @context[:viewer]

        # already_sponsored_by? batches on sponsorable_id
        @object.async_already_sponsored_by?(@context[:viewer])
      end

      field :is_sponsored_by, Boolean, visibility: :under_development, description: "True if the account is publicly sponsoring this user.", null: false do
        argument :account, String, "The target account's login.", required: true
      end

      def is_sponsored_by(account:)
        return false unless GitHub.flipper[:is_sponsored_by_query].enabled?

        Loaders::ActiveRecord.load(::User, account, column: :login, case_sensitive: false).then do |account|
          next false unless account.present?

          Platform::Loaders::IsSponsoringCheck.load(
            sponsor_id: account.id,
            sponsorable_id: @object.id,
            include_private: false
          )
        end
      end

      field :is_sponsoring_viewer, Boolean, visibility: :internal, description: "True if the viewer is sponsored by this user.", null: false

      def is_sponsoring_viewer
        return false unless @context[:viewer]

        # IsSponsorCheck batches on sponsor_id
        Platform::Loaders::IsSponsorCheck.load(
          sponsorable_id: @context[:viewer].id,
          sponsor_id: @object.id,
          include_private: true
        )
      end

      field :viewer_can_sponsor, Boolean, visibility: :internal, description: "Whether or not the viewer is able to sponsor this user.", null: false

      def viewer_can_sponsor
        @object.async_sponsorable_by?(@context[:viewer])
      end

      field :sponsorship_updates, Connections.define(Objects::SponsorshipNewsletter), visibility: :internal, description: "List of sponsorship updates sent to sponsors.", null: false, connection: true do
        argument :order_by, Inputs::SponsorshipNewsletterOrder,
          "Ordering options for sponsorship updates returned from the connection.", required: false,
          default_value: { field: "created_at", direction: "DESC" }
      end

      def sponsorship_updates(order_by: nil)
        field = order_by[:field]
        direction = order_by[:direction]
        @object.sponsorship_newsletters.order("sponsorship_newsletters.#{field} #{direction}")
      end

      field :sponsorship_for_viewer_as_sponsor, Objects::Sponsorship, visibility: :internal, description: "The viewer's sponsorship of this object.", null: true

      def sponsorship_for_viewer_as_sponsor
        return nil unless @context[:viewer]

        @context[:viewer].sponsorship_as_sponsor_for(@object)
      end

      field :sponsorship_for_viewer_as_maintainer, Objects::Sponsorship, visibility: :internal, description: "This object's sponsorship of the viewer.", null: true

      def sponsorship_for_viewer_as_maintainer
        return nil unless @context[:viewer]

        @object.sponsorship_as_sponsor_for(@context[:viewer])
      end

      field :sponsorships_as_maintainer, Connections.define(::Platform::Objects::Sponsorship), connection: true, description: "This object's sponsorships as the maintainer.", null: false do
        argument :include_private, Boolean,
          "Whether or not to include private sponsorships in the result set", required: false,
          default_value: false
        argument :order_by, Inputs::SponsorshipOrder,
          "Ordering options for sponsorships returned from this connection. If left blank, the sponsorships will be ordered based on relevancy to the viewer.", required: false
        argument :tier_id, ID, "If given, will filter for sponsorships on the given tier", required: false, visibility: :internal
      end

      def sponsorships_as_maintainer(include_private: false, order_by: nil, tier_id: nil)
        tier = Platform::Helpers::NodeIdentification.typed_object_from_id(Objects::SponsorsTier, tier_id, @context) if tier_id
        Platform::Loaders::SponsorshipsAsSponsorable.load(@object.id, viewer: @context[:viewer], include_private: include_private, order_by: order_by, tier: tier).then do |sponsorships|
          sponsorships ||= Sponsorship.none
          ArrayWrapper.new(sponsorships)
        end
      end

      field :sponsorships_as_sponsor, Connections.define(::Platform::Objects::Sponsorship), connection: true, description: "This object's sponsorships as the sponsor.", null: false do
        argument :order_by, Inputs::SponsorshipOrder,
          "Ordering options for sponsorships returned from this connection. If left blank, the sponsorships will be ordered based on relevancy to the viewer.", required: false
      end

      def sponsorships_as_sponsor(order_by: nil)
        Platform::Loaders::SponsorshipsAsSponsor.load(@object.id, viewer: @context[:viewer], order_by: order_by).then do |sponsorships|
          sponsorships ||= Sponsorship.none
          ArrayWrapper.new(sponsorships)
        end
      end

      field :sponsors_membership, Objects::SponsorsMembership, visibility: :internal,
        description: "The sponsors membership for this object", null: true

      def sponsors_membership
        if @context[:viewer] == @object || @object.adminable_by?(@context[:viewer]) || @context[:viewer]&.can_admin_sponsors_memberships?
          @object.async_sponsors_membership
        else
          Promise.resolve(nil)
        end
      end

      field :sponsors_activities, Connections.define(Objects::SponsorsActivity), visibility: :internal, description: "Sponsors activity for this maintainer.", null: false, connection: true do
        argument :period, Enums::SponsorsActivityPeriod, "The time period to include.", required: true
      end

      def sponsors_activities(period:)
        if @context[:viewer] == @object || @object.adminable_by?(@context[:viewer]) || @context[:viewer]&.can_admin_sponsors_memberships?
          @object.sponsors_activities.for_period(period).by_timestamp
        else
          ArrayWrapper.new([])
        end
      end

      field :is_sponsors_program_member, Boolean,
        description: "Indicates if this object is a member of the Sponsors program",
        visibility: :internal,
        null: false,
        method: :async_sponsors_program_member?
    end
  end
end
