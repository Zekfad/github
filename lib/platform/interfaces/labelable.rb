# frozen_string_literal: true

module Platform
  module Interfaces
    module Labelable
      include Platform::Interfaces::Base
      description "An object that can have labels assigned to it."

      field :labels, resolver: Resolvers::Labels, description: "A list of labels associated with the object.", connection: true, numeric_pagination_enabled: true
    end
  end
end
