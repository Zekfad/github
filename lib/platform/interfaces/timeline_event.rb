# frozen_string_literal: true

module Platform
  module Interfaces
    module TimelineEvent
      include Platform::Interfaces::Base
      description "Represents an event on the timeline of an issue or pull request."
      visibility :internal

      global_id_field :id

      # TODO add explanatory comment with link
      def self.add_fields(type_defn)
        type_defn.field :actor, Interfaces::Actor, "Identifies the actor who performed the event.", null: true
        type_defn.created_at_field
      end

      add_fields(self)
      def self.included(child_class)
        add_fields(child_class)
      end

      database_id_field(visibility: :internal)

      def actor
        return @object.async_actor unless @object.respond_to?(:async_can_view_actor?)

        @object.async_can_view_actor?(@context[:viewer]).then do |can_view_actor|
          next @object.async_actor if can_view_actor

          @object.async_repository.then do |repo|
            repo.async_owner.then do |owner|
              next owner if owner.is_a?(::Organization)
              User.ghost
            end
          end
        end
      end
    end
  end
end
