# frozen_string_literal: true

module Platform
  module Interfaces
    module RepositoryDiscussionCommentAuthor
      include Platform::Interfaces::Base

      description "Represents an author of discussion comments in repositories."
      areas_of_responsibility :discussions
      visibility :under_development

      field :repository_discussion_comments, Connections.define(Platform::Objects::DiscussionComment),
          description: "Discussion comments this user has authored.", null: false, connection: true do
        argument :order_by, Inputs::DiscussionCommentOrder,
          "Ordering options for discussions returned from the connection.", required: false,
          default_value: { field: "created_at", direction: "DESC" }
        argument :repository_id, ID, "Filter discussion comments to only those in a specific repository.",
          required: false
      end

      def repository_discussion_comments(order_by: nil, repository_id: nil)
        scope = DiscussionComment.preload(:user).filter_spam_for(@context[:viewer])

        if order_by
          field = order_by[:field]
          direction = order_by[:direction]
          scope = scope.order(field => direction)
        end

        if repository_id
          Helpers::NodeIdentification.async_typed_object_from_id(
            [Objects::Repository], repository_id, @context
          ).then do |repo|
            scope.for_repository(repo)
          end
        else
          visible_repository_ids = @context[:viewer].associated_repository_ids
          repositories_commented_in_by_viewer = DiscussionComment.
            for_user(@object).
            distinct.
            pluck(:repository_id)

          public_repo_ids = Repository.where(id: repositories_commented_in_by_viewer, public: true).pluck(:id)

          scope.where(user_id: @object, repository_id: visible_repository_ids | public_repo_ids)
        end
      end
    end
  end
end
