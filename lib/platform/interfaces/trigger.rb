# frozen_string_literal: true

module Platform
  module Interfaces
    module Trigger
      include Platform::Interfaces::Base
      description "Entities that can trigger a workflow run."
      areas_of_responsibility :actions
      visibility :internal

      global_id_field :id
    end
  end
end
