# frozen_string_literal: true
module Platform
  module Interfaces
    module Base
      include GraphQL::Schema::Interface
      field_class Objects::Base::Field
      # Add helpers to interface definition classes:
      definition_methods do
        include Platform::Helpers::Url
        include Platform::Objects::Base::AreasOfResponsibility
        include Platform::Objects::Base::FeatureFlag
        include Platform::Objects::Base::MobileOnly
        include Platform::Objects::Base::LimitActorsTo
        include Platform::Objects::Base::Visibility
        include Platform::Objects::Base::ClassBasedEdgeType
        include Platform::Objects::Base::FieldShortcuts
        include Platform::Objects::Base::Previewable

      end
    end
  end
end
