# frozen_string_literal: true

module Platform
  module Interfaces
    module Actor
      include Interfaces::Base
      description "Represents an object which can take actions on GitHub. Typically a User or Bot."

      field :login, String, "The username of the actor.", null: false, method: :display_login
      field :name, String, "The name of the actor.", null: true, method: :name, visibility: :under_development

      field :avatarUrl, Scalars::URI, "A URL pointing to the actor's public avatar.", null: false do
        argument :size, Integer, "The size of the resulting square image.", required: false
      end

      url_fields description: "The HTTP URL for this actor."
    end
  end
end
