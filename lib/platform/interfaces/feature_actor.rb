# frozen_string_literal: true

module Platform
  module Interfaces
    module FeatureActor
      include Platform::Interfaces::Base
      description "An object that can be a feature actor."
      visibility :internal

      field :flipper_id, String, description: "The flipper ID for this object", visibility: :internal, null: false
    end
  end
end
