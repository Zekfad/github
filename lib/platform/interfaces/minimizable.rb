# frozen_string_literal: true

module Platform
  module Interfaces
    module Minimizable
      include Platform::Interfaces::Base
      description "Entities that can be minimized."
      visibility :public

      field :viewer_can_minimize, Boolean, description: "Check if the current viewer can minimize this object.", null: false

      def viewer_can_minimize
        @object.async_minimizable_by?(@context[:viewer])
      end

      field :viewer_can_see_minimize_button, Boolean, description: "Check if the viewer should see the minimize button in the UI.", visibility: :internal, null: false

      def viewer_can_see_minimize_button
        @object.async_minimizable_by?(@context[:viewer]).then do |can_minimize|
          # Return false if object is not part of a repo
          next false unless @object.respond_to?(:repository)

          # Return result if actor isn't a site admin
          next can_minimize unless @context[:viewer].try(:site_admin?)

          # If the user is a site admin, only show them the minimize button if they can push to the repo
          @object.async_repository.then do |repo|
            repo.async_pushable_by?(@context[:viewer]).then do |can_push|
              can_push && can_minimize
            end
          end
        end
      end

      field :viewer_can_see_unminimize_button, Boolean, description: "Check if the viewer can see unminimize button in the UI.", visibility: :internal, null: false

      def viewer_can_see_unminimize_button
        @object.async_unminimizable_by?(@context[:viewer]).then do |can_unminimize|
          # Return false if object is not part of a repo
          next false unless @object.respond_to?(:repository)

          # Return result if actor isn't a site admin
          next can_unminimize unless @context[:viewer].try(:site_admin?)

          # If the user is a site admin, only show them the unminimize button if they can push to the repo
          @object.async_repository.then do |repo|
            repo.async_pushable_by?(@context[:viewer]).then do |can_push|
              can_push && can_unminimize
            end
          end
        end
      end


      field :is_minimized, Boolean, description: "Returns whether or not a comment has been minimized.", null: false, method: :minimized?

      field :minimized_reason, String, description: "Returns why the comment was minimized.", null: true
    end
  end
end
