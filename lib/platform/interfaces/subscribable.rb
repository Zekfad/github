# frozen_string_literal: true

module Platform
  module Interfaces
    module Subscribable
      include Platform::Interfaces::Base
      description "Entities that can be subscribed to for web and email notifications."

      field :id, ID, method: :global_relay_id, null: false

      field :viewer_subscription, Enums::SubscriptionState,
        null: true,
        description: "Identifies if the viewer is watching, not watching, or ignoring the subscribable entity.",
        extras: [:execution_errors]

      def viewer_subscription(execution_errors:)
        return "unsubscribed" if @context[:viewer].nil?

        @object.async_subscription_status(@context[:viewer]).then do |subscription_status_response|
          if subscription_status_response.failed?
            raise Platform::Errors::ServiceUnavailable.new("Subscriptions are currently unavailable. Please try again later.")
          end

          subscription = subscription_status_response.value
          if subscription.subscribed?
            "subscribed"
          elsif subscription.thread_type_only?(::Release) &&
            (@context[:target] == :internal || GitHub.flipper[:pe_mobile].enabled?(@context[:oauth_app]) || GitHub.flipper[:pe_mobile].enabled?(@context[:viewer]))
            "releases_only"
          elsif subscription.participation_only?
            "unsubscribed"
          elsif subscription.ignored?
            "ignored"
          end
        end
      end

      field :viewer_can_subscribe, Boolean, description: "Check if the viewer is able to change their subscription status for the repository.", null: false

      def viewer_can_subscribe
        return false if @context[:viewer].nil?

        @object.async_subscription_status(@context[:viewer]).then(&:success?)
      end

      field :viewer_can_unsubscribe, Boolean, visibility: :internal, description: "Check if the viewer should be able to unsubscribe from this Subscribable.", null: false

      def viewer_can_unsubscribe
        return false unless @context[:viewer]

        async_thread_subscription_status_response = @object
          .async_subscription_status(@context[:viewer])

        async_list_subscription_status_response = @object
          .async_notifications_list
          .then do |notifications_list|

          if @object == notifications_list
            # Although this is equivalent to the else branch, don't compute it again.
            async_thread_subscription_status_response
          else
            notifications_list.async_subscription_status(@context[:viewer])
          end
        end

        Promise
          .all([
            async_thread_subscription_status_response,
            async_list_subscription_status_response,
          ])
          .then do |responses|

          next false unless responses.all?(&:success?)

          thread_sub_status = responses.first.value
          list_sub_status = responses.second.value

          (thread_sub_status.subscribed? || list_sub_status.subscribed?) &&
            !thread_sub_status.ignored?
        end
      end
    end
  end
end
