# frozen_string_literal: true

module Platform
  class OwnershipCheck
    module Autocorrect
      def self.call(ownership_check)
        sorted_suggestions_by_owners = ownership_check.suggestions
          .sort_by { |s| "#{s[:path]}#{s[:owners].keys.sort.join}" }
          .group_by { |s| s[:owners].keys }

        sorted_suggestions_by_owners.each do |(new_owners, suggestions)|
          longest_suggestion_indent_size = suggestions.map { |s| s[:path].size }.max + 1

          if longest_suggestion_indent_size.odd?
            # Add one so that, after a whitespace, the team name will start on a tab break
            longest_suggestion_indent_size += 1
          end

          # Load this file fresh, because a previous iteration may have changed the text:
          codeowners_file = OwnershipCheck.load_codeowners

          if new_owners.empty?
            raise(Platform::Errors::Internal, "Invariant: suggestion with no owners: #{suggestion}; it shouldn't have been in the list of `.suggestions` to start with")
          end

          matching_rules = codeowners_file.rules.select { |r| r.owners.map(&:identifier) == new_owners }
          matching_lines = matching_rules.map(&:line).sort
          # Find a big swath of files belonging to these owners and recommend to add them there
          prev_line_no = 0
          current_streak = 0
          last_streak_ended_at = nil

          matching_lines.each do |line_no|
            if line_no == prev_line_no + 1
              current_streak += 1
            else
              current_streak = 0
            end
            if current_streak > 3
              last_streak_ended_at = line_no
            end
            prev_line_no = line_no
          end

          text = File.read("CODEOWNERS")
          lines = text.split("\n")

          insert_at = last_streak_ended_at || matching_lines.last || lines.size

          team_name_indent = if matching_lines.any?
            # Try to make this new line look nice next to the previous line
            prev_line_text = lines[insert_at - 1]
            chars_before_team_name = prev_line_text.index("@") || 0 # sometimes we get a blank line here
            [chars_before_team_name, longest_suggestion_indent_size].max
          else
            longest_suggestion_indent_size
          end

          new_lines = []
          suggestions.each do |suggestion|
            filepath = suggestion[:path]

            padding = team_name_indent - filepath.size

            new_lines << "#{filepath}#{" " * padding}#{new_owners.join(" ")}"
          end

          # There are no existing rules for these owners, we're appending this to the end of the file.
          if matching_lines.empty?
            # Add a blank line to separate from the last rule in the file
            new_lines.unshift("")
          end

          lines[insert_at , 0] = new_lines
          new_file_body = lines.join("\n") + "\n"

          File.write("CODEOWNERS", new_file_body)
        end
      end
    end
  end
end
