# frozen_string_literal: true

module Platform
  class OwnershipCheck
    module Report
      def self.generate(ownership_check, all_reviewers, ecosystem_api_file_paths)
        # Re-group suggestions by the new suggested owner, so that teams can review their files easily.
        suggestions_by_owner = Hash.new { |h, owner_name| h[owner_name] = Hash.new { |h2, file_name| h2[file_name] = [] } }
        ownership_check.suggestions.each do |suggestion|
          path = suggestion[:path]
          suggestion[:owners].each do |(owner, comments)|
            suggestions_by_owner[owner][path] << suggestion
          end
        end

        report = <<~MARKDOWN.dup
        # Ownership report

        This report has three parts:

        - Codeowners Updates: Suggested ownership, team-by-team
        - New Codeowners: Teams that were not previously in CODEOWNERS (should a previous CODEOWNERS team have been used here instead?)
        - No Suggestions: Files which didn't have any new suggestion

        ## Codeowners Updates

        These teams are suggested as codeowners of the following files for the given reasons.
        MARKDOWN

        # For each new owner, render a section that lists the newly-attributed files
        # and includes the reasons they were attributed, as well as other newly-attributed owners.
        suggestions_by_owner.sort_by { |k, v| k }.each do |(owner, suggestions_by_path)|
          # Add a header for this team
          report << "\n### `#{owner}`\n\n"

          suggestions_by_path.each do |(path, suggestions)|
            combined_comments = suggestions.map { |s| s[:owners][owner] }.flatten.join("; ")
            combined_owners = suggestions.map { |s| s[:owners].keys }.flatten.uniq

            # Add a line for the attributed file, with reasons
            report << "- `#{path}`: #{combined_comments}"
            # If there are other newly-attributed owners, mention them
            if combined_owners.size > 1
              other_owners = combined_owners.dup
              other_owners.delete(owner)
              report << " (along with #{other_owners.map { |o| "`#{o}`" }.join(", ")})"
            end
            report << "\n"
          end
        end

        # List teams that weren't previously in CODEOWNERS -- this might be a clue that some team has been wrongly identified.
        report << "\n## New Codeowners\n\nThese teams were not previously in CODEOWNERS. Should they have been added? If not, a fix to `Platform::OwnershipCheck` may be required.\n\n"

        suggested_owners = suggestions_by_owner.keys
        new_owners = suggested_owners - all_reviewers
        new_owners.sort!
        new_owners.each do |new_owning_team|
          report << "- `#{new_owning_team}`\n"
        end

        report << "\n## No Suggestions\n\nNo suggested owners were found for these files. Perhaps the files need updating, or `Platform::OwnershipCheck` needs new logic. (Or, maybe they don't have any other owners.)\n\n"

        # Assume that we should own everything in these folders; remove noise from report
        ecosystem_api_dirs = [
          "app/api/description/backfill_operations",
          "app/api/middleware",
          "lib/platform/analyzers",
          "lib/platform/connection_wrappers",
          "lib/platform/errors",
          "lib/graphql_extensions"
        ]

        paths_with_suggestions = ownership_check.suggestions.map { |s| s[:path] }.uniq
        paths_without_suggestions = ecosystem_api_file_paths - paths_with_suggestions
        # Remove whitelisted dirs
        paths_without_suggestions.reject! { |fp| ecosystem_api_dirs.any? { |dir| fp.start_with?(dir) } }

        ecosystem_api_dirs.each do |dir|
          report << "- #{dir} (whitelisted)\n"
        end

        paths_without_suggestions.each do |fp|
          report << "- #{fp}\n"
        end
        report
      end
    end
  end
end
