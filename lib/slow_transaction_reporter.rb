# frozen_string_literal: true

module SlowTransactionLogger
  class SlowTransactionError < StandardError; end
end

class SlowTransactionReporter
  if GitHub.enterprise?
    SLOW_TRANSACTION_THRESHOLD_MS = 5_000.0
  else
    SLOW_TRANSACTION_THRESHOLD_MS = 100.0
  end

  attr_reader :times, :trace, :queries

  def initialize(transaction_time, queries, trace = caller)
    @times = Times.new(transaction_time, queries)
    @trace = trace
    @queries = queries
  end

  def report
    return if times.transaction_time < SLOW_TRANSACTION_THRESHOLD_MS
    return if SlowQueryLogger.should_skip?(trace)

    report_to_failbot
    report_to_dogstats
  end

  private

  def report_to_failbot
    error = SlowTransactionLogger::SlowTransactionError.new
    error.set_backtrace(trace)

    Failbot.report!(
      error,
      class: (GitHub.context[:from] || error.class).to_s.gsub("GitHub::", ""),
      message: failbot_message,
      rollup: Digest::MD5.hexdigest("#{error.class}#{trace.join("")}"),
      app: "github-slow-transaction",
      transaction_time_ms: times.transaction_time,
      transaction_query_time_ms: times.query_time,
      transaction_query_count: queries.length,
      sql_sanitized: formatted_queries,
    )
  end

  def failbot_message
    app_caller = trace.
      select { |line| line.include?("#{Rails.root}/app/") }.
      reject { |line| line.include?("#{Rails.root}/app/subscribers/trilogy/") }.
      first&.gsub("#{Rails.root}/", "") || "unknown"

    "at #{app_caller} (took #{times.transaction_time.to_i}ms = #{times.query_time.to_i}ms + #{times.idle_time.to_i}ms idle)"
  end

  def formatted_queries
    queries.map do |query|
      "%7.3f | #{GitHub::SQL::Digester.digest_sql(query[:sql])} | %7.3fms" %
      [query[:elapsed_time], query[:duration]]
    end.join("\n")
  end

  def report_to_dogstats
    GitHub.dogstats.timing("rpc.mysql.slow_transaction.time", times.transaction_time)
    GitHub.dogstats.timing("rpc.mysql.slow_transaction.idle_time", times.idle_time)
  end

  class Times
    attr_reader :transaction_time, :query_time, :idle_time

    def initialize(transaction_time, queries)
      @transaction_time = transaction_time
      @query_time = queries.map { |query| query[:duration] }.sum
      @idle_time ||= transaction_time - query_time
    end
  end
end
