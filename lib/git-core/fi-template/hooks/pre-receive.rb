#!/usr/bin/env ruby
require "github/githooks/procline"
GitHub::GitHooks.setproctitle

require "github"
require "github/config/failbot"
require "github/config/stats"

require "github/githooks/push_context"

babeld_mode = (ENV["GIT_SOCKSTAT_VAR_babeld_hook"] == "bool:true")
quarantine_only = (ENV["GIT_SOCKSTAT_VAR_quarantine_only"] == "1")
custom_hooks_only = !!ENV["CUSTOM_HOOKS_ONLY"]

$stdin.set_encoding ::Encoding::ASCII_8BIT
context = GitHub::PushContext.new($stdin.read)
$stderr.puts("Operator mode enabled.") if context.user_has_operator_mode?

if ENV["DEBUG_GIT_HOOKS"] == "1"
  puts "DEBUG start"
  puts "DEBUG hook_name=pre-receive"
  puts "DEBUG babeld_mode=#{babeld_mode}"
  puts "DEBUG quarantine_only=#{quarantine_only}"
  puts "DEBUG custom_hooks_only=#{custom_hooks_only}"
  puts "DEBUG GIT_QUARANTINE_PATH=#{ENV["GIT_QUARANTINE_PATH"]}" unless ENV["GIT_QUARANTINE_PATH"].nil?
  puts "DEBUG stop"
end

case
when babeld_mode && !quarantine_only && !custom_hooks_only
  require "github/githooks/pre_receive_redirect"
  require "github/githooks/pre_receive_refs"
  require "github/githooks/pre_receive_ref_length"
  require "github/githooks/pre_receive_force_push"
  require "github/githooks/pre_receive_lfs_integrity"
  require "github/githooks/pre_receive_custom_hooks"

  context.check(
    GitHub::PreReceiveRedirect,
    GitHub::PreReceiveRefs,
    GitHub::PreReceiveRefLength,
    GitHub::PreReceiveForcePush,
    GitHub::PreReceiveLFSIntegrity,
    GitHub::PreReceiveCustomHooks,
  )
when !babeld_mode && quarantine_only && !custom_hooks_only
  require "github/githooks/pre_receive_blob"

  context.check(GitHub::PreReceiveBlob)
when !babeld_mode && !quarantine_only && custom_hooks_only
  require "github/githooks/pre_receive_custom_hooks"

  context.check(GitHub::PreReceiveCustomHooks)
else
  # Only the above combinations of modes are allowed.
  context.mode_error(
    babeld_mode: babeld_mode,
    quarantine_only: quarantine_only,
    custom_hooks_only: custom_hooks_only,
  )
end
