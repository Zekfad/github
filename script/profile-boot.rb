#!/usr/bin/env ruby
# frozen_string_literal: true

require "./config/basic"
require "./config/environment"

GC.start

stats = GC.stat
memrss = GitHub::Memory.memrss
all_objects = ObjectSpace.each_object.to_a

def print_table(table)
  table = table.map { |k, v| [k.to_s, v.to_s] }
  maxw = table.map(&:first).map(&:size).max
  maxw = [maxw, 100].min

  fmt = "%#{maxw}s | %s"
  table.each do |k, v|
    puts fmt % [k, v]
  end
end

NORMALIZE_ROOTS = [
  "#{RbConfig::CONFIG["rubylibdir"]}/",
  "#{Gem.dir}/gems/",
  "#{ENV["RAILS_ROOT"]}/",
]

def normalize_file(f)
  return f unless f
  NORMALIZE_ROOTS.each do |root|
    return f[root.size, 9999] if f.starts_with?(root)
  end
  f
end

puts
puts "Total memory:"
puts "#{(memrss / 1024.0 / 1024.0).round}MB (#{memrss} bytes)"

puts
puts "GC statistics:"
print_table GC.stat

puts
puts "Object usage:"
print_table ObjectSpace.count_objects

puts
puts "Top classes:"
print_table all_objects.group_by(&:class).transform_values(&:count).sort_by(&:last).last(20)

puts
puts "Top strings"
print_table all_objects.grep(String).group_by(&:itself).transform_values(&:count).sort_by(&:last).last(20).to_h.transform_keys(&:inspect)

# Allow GC of all_objects
all_objects = nil
GC.start

puts
puts "GC Profile"
GC::Profiler.enable
50_000_000.times { String.new }
GC::Profiler.report
