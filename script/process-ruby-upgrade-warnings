#!/usr/bin/env ruby
# frozen_string_literal: true

if ARGV.empty?
  STDERR.puts "USAGE: bin/process-ruby-upgrade-warnings <path/to/warning-files/dir>"
  exit 1
end

require "codeowners"
require "fileutils"

DELIMITER       = "*^.^*"
CODEOWNERS_FILE = File.expand_path("../../CODEOWNERS", __FILE__)
CODEOWNERS      = Codeowners::File.new(File.read(CODEOWNERS_FILE))

dir = ARGV.first.chomp("/")

class Message
  attr_accessor :owner
  attr_reader :message, :source_file, :line_number, :paths
  def initialize(message)
    @message = message
    @source_file, @line_number = message.split(" ").find { |part| part.include?("rb:") }.to_s.split(":")
    @paths = []
  end

  def type
    if message.include?("URI.")
      :uri
    elsif message.start_with?("[", "- ", "::1")
      :ignored
    elsif message.include?("Rails 6.1")
      :ignored
    elsif message.include?("assert_nil")
      :nils
    elsif message.include?("Using the last argument") || message.include?("Passing the keyword argument") || message.include?("Use keyword argument")
      :kwarg
    else
      :other
    end
  end
end

warnings = {}
Dir["#{dir}/warning*.txt"].each do |file|
  lines = File.read(file).split("\n")
  lines.each do |line|
    next if line.include?("The called method")
    message, origin = line.split(DELIMITER)
    warnings[message] ||= Message.new(message)
    warnings[message].paths << origin if origin
  end
end
warnings = warnings.values

warnings.each do |warning|
  next if warning.owner

  source_file = warning.source_file
  next unless source_file

  if %w(/rotp /webauthn /earthsmoke).any? { |path| source_file.include?(path) }
    warning.owner = "prodsec-reviewers"
  elsif source_file.include?("/stripe")
    warning.owner = "gitcoin-reviewers"
  elsif source_file.include?("/group_syncer")
    warning.owner = "external-identities"
  elsif %w(/rack /minitest /factory-bot).any? { |path| source_file.include?(path) }
    warning.owner = "ruby-architecture-reviewers"
  elsif source_file.include?("/flipper")
    warning.owner = "feature-lifecycle-reviewers"
  elsif source_file.include?("/view-component")
    warning.owner = "view-component-reviewers"
  elsif source_file.include?("/graphql-batch")
    warning.owner = "ecosystem-api-reviewers"
  end

  # assume primary owner is listed first, with "interested parties" after that.
  warning.owner ||= CODEOWNERS.for(source_file).keys.first.to_s.gsub("@github/", "")
  warning.owner = "unknown" if warning.owner.empty?
end

counts_by_owner = {}
warnings.reject { |warning| warning.source_file.nil? }.group_by { |warning| warning.owner }.each do |owner, warnings|
  File.open(File.join(dir, "team-#{owner}.txt"), "w") do |f|
    warnings.group_by { |warning| warning.source_file }.each do |source_file, warnings|
      msg = <<~MSG
      - [ ] `#{source_file}`
        - **warnings**
      #{warnings.map(&:message).uniq.sort.map { |message| "    - %s" % message.gsub("#{source_file}:", "Line ") }.join("\n")}
      MSG
      unless source_file.end_with?("_test.rb")
        msg += <<~MSG
          - **test suites that trigger these warnings**
        #{warnings.flat_map(&:paths).uniq.sort.map { |path| "    - %s" % path }.join("\n")}
        MSG
      end

      f.puts msg
    end
  end
  counts_by_owner[owner] = warnings.count
end

File.open(File.join(dir, "_stats.txt"), "w") do |f|
  f.puts "Total warnings: %d" % warnings.sum { |warning| warning.paths.count }
  f.puts "Total unique warnings: %d" % warnings.count
  counts_by_owner.sort_by { |owner, count| owner }.each do |owner, count|
    f.puts "%s: %d" % [owner, count]
  end
end

warnings.group_by { |warning| warning.type }.each do |type, warnings|
  File.open(File.join(dir, "type-#{type}.txt"), "w") do |f|
    f.puts warnings.map(&:message).join("\n")
  end
end
