#!/usr/bin/env ruby
# frozen_string_literal: true

# To run directly (using gh-screen):
#
#   $ cd /data/github/current
#
# Test run:
#
#   $ bin/safe-ruby script/send_security_incident_notification -v -d \
#     -c /path/to/data.csv -s '[GitHub] Security Incident' -t /path/to/template.mustache
#
# This time for real:
#
#   $ bin/safe-ruby script/send_security_incident_notification -v \
#     -c /path/to/data.csv -s '[GitHub] Security Incident' -t /path/to/template.mustache \
#     | tee -a /tmp/sin.log
#
# Example mustache template:
#
#   Hi @{{login}}, we're emailing to inform you of a security incident.
#
#   Your token {{token}} has been compromised.
#
#   Thanks,
#   Your friends at GitHub
#
# Example csv:
#
#   user_id,token
#   331,abcd
#   331,a1b2
#   326,nnnn

require File.expand_path("../../config/environment", __FILE__)
require "slop"
require "csv"

options = Slop.new(strict: true) do
  on "c=", "csv", "Path to csv with user_id column and optional other columns", required: true
  on "s=", "subject", "Incident notification email subject", required: true
  on "t=", "template", "Path to incident notification mustache template", required: true
  on "f=", "from", "Reply to email address for this notification", required: true
  on "p=", "pat", "A personal access token for the user sending the notification", required: true
  on "r=", "reason", "The reason for this notification", required: true
  on "d", "dry_run", "Don't save or update any data, just log changes"
  on "v", "verbose", "Log verbose output"
end

class SendSecurityIncidentNotificationError < StandardError; end

begin
  # parse cli args
  options.parse
  options_hash = options.to_hash

  # handle auth
  attempt = GitHub::Authentication::Attempt.new(
    from: :send_security_incident_notification,
    token: options_hash.delete(:pat),
  )
  result = attempt.result
  unless result.success? && result.user.employee?
    raise SendSecurityIncidentNotificationError.new("Not authorized")
  end
  user = result.user

  # log user and reason to #notices
  reason = options_hash.delete(:reason)
  message = <<-AUDIT
#{user.login} ran bin/send_security_incident_notification with the following options:
```
  csv_path: #{options_hash[:csv].inspect}
  template_path: #{options_hash[:template].inspect}
  from_email: #{options_hash[:from].inspect}
  subject: #{options_hash[:subject].inspect}
  reason: #{reason.inspect}
  dry_run: #{options_hash[:dry_run] ? 'true' : 'false'}
  verbose: #{options_hash[:verbose] ? 'true' : 'false'}
```
AUDIT
  if Rails.env.production?
    GitHub::Chatterbox.client.say "#notices", message
  else
    puts message
  end

  # load the csv
  csv_path = options_hash.delete(:csv)
  file = File.read(csv_path)
  csv = CSV.parse(file, headers: true, header_converters: :symbol)
  data = csv.map(&:to_hash)

  # load the template
  template_path = options_hash.delete(:template)
  options_hash[:template] = File.read(template_path)

  # run the notifier
  notifier = GitHub::SendSecurityIncidentNotification.new(options_hash)
  notifier.perform(data)
rescue Slop::Error, SendSecurityIncidentNotificationError => error
  puts error.message
  puts options # print help
end
