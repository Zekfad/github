const {Project, TypeGuards} = require('ts-morph')

const paths = process.argv.slice(2)
const project = new Project({tsConfigFilePath: 'tsconfig.json'})
let exitCode = 0

if (paths) {
  project.addSourceFilesAtPaths(paths)
}

function isExported(node) {
  return TypeGuards.isExportableNode(node) && node.isExported()
}

const violations: {path: string; linenumber: number; functionName: string}[] = []

function checkNode(node) {
  if (!TypeGuards.isReferenceFindableNode(node)) return

  const file = node.getSourceFile()
  if (node.findReferencesAsNodes().filter(n => n.getSourceFile() !== file).length === 0) {
    exitCode = 1
    const path = file.getFilePath()
    const linenumber = node.getStartLineNumber()
    const functionName = TypeGuards.hasName(node) ? node.getName() : node.getText()
    violations.push({path, linenumber, functionName})
  }
}

for (const file of project.getSourceFiles()) {
  file.forEachChild(child => {
    if (TypeGuards.isVariableStatement(child)) {
      if (isExported(child)) {
        for (const node of child.getDeclarations()) {
          checkNode(node)
        }
      }
    } else if (isExported(child)) {
      checkNode(child)
    }
  })
}

const rootDir = `${process.cwd()}/`
for (const {path, linenumber, functionName} of violations) {
  console.log(`${path.replace(rootDir, '')}:${linenumber}`)
  console.log(`  ${functionName} is exported but not imported anywhere`)
}

process.exit(exitCode)
