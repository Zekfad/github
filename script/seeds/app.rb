# frozen_string_literal: true

require "thor"
require_relative Rails.root.join("script/seeds/runner")
Seeds::Runner.require_runners

# Do not require anything else here.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class App < Thor
    desc "actions", Seeds::Runner::Actions.help.lines.first
    long_desc Seeds::Runner::Actions.help
    method_option :workflow_run_count, type: :numeric, aliases: "-c", desc: "# of workflows to make (default 20)", default: 20
    def actions
      Seeds::Runner::Actions.execute(options)
    end

    desc "check_run", Seeds::Runner::CheckRun.help.lines.first
    long_desc Seeds::Runner::CheckRun.help
    method_option :nwo, type: :string, aliases: "-r", desc: "NWO of repo to use", required: true
    method_option :pr, type: :string, desc: "The PR number for which to make checks", required: true
    method_option :interactive, aliases: "-i", type: :boolean, desc: "Whether to use an interactive mode", required: false
    method_option :fresh_push, aliases: "-fp", type: :boolean, desc: "Whether to create a fresh push first", required: false
    method_option :check_outcome, aliases: "-c", type: :string, desc: "Which outcome the check should be", enum: %w(green yellow red), required: false
    def check_run
      Seeds::Runner::CheckRun.execute(options)
    end

    desc "checks", Seeds::Runner::Checks.help.lines.first
    long_desc Seeds::Runner::Checks.help
    def checks
      Seeds::Runner::Checks.execute(options)
    end

    desc "composable_comments", Seeds::Runner::ComposableComments.help.lines.first
    long_desc Seeds::Runner::ComposableComments.help
    def composable_comments
      Seeds::Runner::ComposableComments.execute(options)
    end

    desc "deployments", Seeds::Runner::Deployments.help.lines.first
    method_option :nwo, type: :string, aliases: "-r", desc: "NWO of repo to use"
    method_option :pr, type: :string, desc: "The PR number for which to make checks"
    method_option :integration_slug, aliases: "-i", type: :string, desc: "The slug of the integration you wish to attribute the deployments to"
    long_desc Seeds::Runner::Deployments.help
    def deployments
      Seeds::Runner::Deployments.execute(options)
    end

    desc "dmca_strike_repos", Seeds::Runner::DmcaStrikeRepos.help.lines.first
    long_desc Seeds::Runner::DmcaStrikeRepos.help
    def dmca_strike_repos
      Seeds::Runner::DmcaStrikeRepos.execute(options)
    end

    desc "internal_integrations", Seeds::Runner::InternalIntegrations.help.lines.first
    long_desc Seeds::Runner::InternalIntegrations.help
    def internal_integrations
      Seeds::Runner::InternalIntegrations.execute(options)
    end

    desc "models_for_export", Seeds::Runner::ModelsForExport.help.lines.first
    long_desc Seeds::Runner::ModelsForExport.help
    method_option :count, type: :numeric, aliases: "-n", desc: "The number of models to make", default: 10
    def models_for_export
      Seeds::Runner::ModelsForExport.execute(options)
    end

    desc "reminders", Seeds::Runner::Reminders.help.lines.first
    long_desc Seeds::Runner::Reminders.help
    def reminders
      Seeds::Runner::Reminders.execute(options)
    end

    desc "repository_access_management", Seeds::Runner::RepositoryAccessManagement.help.lines.first
    long_desc Seeds::Runner::RepositoryAccessManagement.help
    def repository_access_management
      Seeds::Runner::RepositoryAccessManagement.execute(options)
    end

    desc "team_sync_integration", Seeds::Runner::TeamSyncIntegration.help.lines.first
    long_desc Seeds::Runner::TeamSyncIntegration.help
    # method_option :nwo, type: :string, aliases: "-r", desc: "NWO of repo to use", required: true
    def team_sync_integration
      Seeds::Runner::TeamSyncIntegration.execute(options)
    end

    desc "organizations", Seeds::Runner::Organizations.help.lines.first
    long_desc Seeds::Runner::Organizations.help
    method_option :org_name, type: :string, aliases: "-n", desc: "Organization Name", required: true
    method_option :email_domain, type: :string, aliases: "-d", desc: "Domain for User Accounts", required: false
    method_option :logins, type: :string, aliases: "-l", desc: "Comma-separated list of User logins and email addresses", required: false
    def organizations
      Seeds::Runner::Organizations.execute(options)
    end

    desc "organization_invitations", Seeds::Runner::OrganizationInvitations.help.lines.first
    long_desc Seeds::Runner::OrganizationInvitations.help
    method_option :org_name, type: :string, aliases: "-n", desc: "Organization Name", required: true
    method_option :invitee, type: :string, aliases: "-i", desc: "Invitee login", required: false
    method_option :email, type: :string, aliases: "-e", desc: "Invitee email", required: false
    method_option :role, type: :string, aliases: "-r", desc: "Invitee role", required: false
    def organization_invitations
      Seeds::Runner::OrganizationInvitations.execute(options)
    end

    desc "code_scanning", Seeds::Runner::CodeScanning.help.lines.first
    long_desc Seeds::Runner::CodeScanning.help
    method_option :name, type: :string, aliases: "-n", desc: "Give custom name to the repository (optional)", required: false
    method_option :turboscan_path, type: :string, aliases: "-p", desc: "Give local path to turbo-scan repo", default: "../turbo-scan"
    method_option :repo_path, type: :string, aliases: "-r", desc: "Give path to premade repo (can be local .git folder or remote location)", default: "script/seeds/data/code_scanning/default.git"
    method_option :organization, type: :boolean, aliases: "-o", desc: "Create an Organization owned repository", default: false
    method_option :verbose, type: :boolean, aliases: "-v", desc: "Include output from `parse-sarif` for debugging purposes.", default: false
    def code_scanning
      Seeds::Runner::CodeScanning.execute(options)
    end

    desc "marketplace_listing", Seeds::Runner::MarketplaceListing.help.lines.first
    long_desc Seeds::Runner::MarketplaceListing.help
    method_option :count, type: :numeric, aliases: "-n", desc: "Number of listings to create", default: 1
    method_option :login, type: :string, aliases: "-l", desc: "The owner of the integration/oauth application", required: false
    def marketplace_listing
      Seeds::Runner::MarketplaceListing.execute(options)
    end

    desc "actions_repos", Seeds::Runner::ActionsRepos.help.lines.first
    long_desc Seeds::Runner::ActionsRepos.help
    method_option :count, type: :numeric, aliases: "-n", desc: "Number of repos to create. Will max out at the number of public, unarchived repos in the Actions org on production"
    method_option :login, type: :string, aliases: "-l", desc: "The user to use for this operation, will be the admin of the Actions org", required: false
    def actions_repos
      Seeds::Runner::ActionsRepos.execute(options)
    end

    desc "policy_service", Seeds::Runner::PolicyService.help.lines.first
    long_desc Seeds::Runner::PolicyService.help
    def policy_service
      Seeds::Runner::PolicyService.execute(options)
    end

    desc "discussions", Seeds::Runner::Discussions.help.lines.first
    long_desc Seeds::Runner::Discussions.help
    method_option :repo, type: :string, aliases: "-r", desc: "Name-with-owner (monalisa/repo-name) of new or existing repository to populate with discussions", default: nil
    method_option :"create-participants", type: :boolean, desc: "Create new users as participants", default: false
    method_option :"participant-count", type: :numeric, desc: "Number of verified users to create as participants", default: 10
    method_option :"discussion-count", type: :numeric, desc: "Exact number of discussions to generate per repo", default: 10
    method_option :"comment-count", type: :numeric, desc: "Exact number of top-level comments to generate for each discussion"
    method_option :"comment-min", type: :numeric, desc: "Minimum number of top-level comments to generate for each discussion"
    method_option :"comment-max", type: :numeric, desc: "Maximum number of top-level comments to generate for each discussion"
    method_option :"thread-count", type: :numeric, desc: "Exact number of child comments to generate for each comment"
    method_option :"thread-min", type: :numeric, desc: "Minimum number of child comments to generate for each comment"
    method_option :"thread-max", type: :numeric, desc: "Maximum number of child comments to generate for each comment"
    def discussions
      Seeds::Runner::Discussions.execute(options)
    end

    desc "enterprise_account_with_bundled_licenses", Seeds::Runner::EnterpriseAccountWithBundledLicenses.help.lines.first
    long_desc Seeds::Runner::EnterpriseAccountWithBundledLicenses.help
    method_option :administrator_login, type: :string, aliases: "-a", desc: "The user to make the administrator of the new enterprise account", required: false
    def enterprise_account_with_bundled_licenses
      Seeds::Runner::EnterpriseAccountWithBundledLicenses.execute(options)
    end

    desc "console", "start a console with seeds objects and runners loaded"
    def console
      require_relative Rails.root.join("script/seeds/objects")
      load Rails.root.join("script/console")
    end

    desc "generate NAME", "generate a new runner or object"
    method_option :type, type: :string, enum: %w(runner object), default: "runner"
    def generate(name)
      Seeds::Runner::Generate.run(name, options) # Don't use execute so we don't run setup
    end
  end
end
