# frozen_string_literal: true

require_relative "../runner"
# Do not require anything else here. If you need something for your runner, put that in `self.run`.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class Runner
    class ComposableComments < Seeds::Runner
      def self.help
        <<~EOF
        Seed composable-comments on a PR and an Issue

        Seeding
        - Ensures github/hub exists
        - Ensures github/hub/master exists
        - Ensures a GitHub app exist
        - Ensures apps are installed for github/hub
        - Creates a PR against master & adds a comopsable comment
        - Creates an issue and adds a comopsable comment
        EOF
      end

      def self.run(options = {})
        Seeds::Objects::FeatureFlag.toggle(action: "enable", feature_flag: "interactions_api")

        mona = Seeds::Objects::User.monalisa
        hub_repo = Seeds::Objects::Repository.hub_repo
        integration = Seeds::Objects::Integration.create(
          repo: hub_repo,
          owner: hub_repo.owner,
          app_name: Faker::App.name
        )

        issue = Seeds::Objects::Issue.create(actor: mona, repo: hub_repo)
        pr = Seeds::Objects::PullRequest.create(repo: hub_repo, committer: mona)

        c1 = Seeds::Objects::ComposableComment.create(issue: issue, integration: integration)
        c2 = Seeds::Objects::ComposableComment.create(issue: pr.issue, integration: integration)

        puts "Created comment on issue #{c1.issue.url}"
        puts "Created comment on issue #{c2.issue.url}"
      end
    end
  end
end
