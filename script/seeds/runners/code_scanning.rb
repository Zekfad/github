# frozen_string_literal: true

require_relative "../runner"
# Do not require anything else here. If you need something for your runner, put that in `self.run`.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class Runner
    class CodeScanning < Seeds::Runner
      def self.help
        <<~HELP
        Create a repo with seeded alerts for code scanning development

        - enables feature flag on owner (user or organization)
        - by default creates new repo (or creates/updates named one)
        - runs turboscan `parse-sarif` tool to populate local database with alerts
        - opens PR from branch into master

        HELP
      end

      def self.run(options = {})
        puts "\n"

        @bin_path = "#{options[:turboscan_path]}/bin/parse-sarif"
        @env_vars = "MYSQL_SKEEMA_PATH='#{options[:turboscan_path]}/schemas/.skeema'"

        unless self.parse_sarif_present?
          abort "Cannot find executable at #{@bin_path}.\nCheck provided path and perhaps run `make bin/parse-sarif`"
        end

        @verbose = options[:verbose]

        @mona = Seeds::Objects::User.monalisa

        @owner = options[:organization] ? Seeds::Objects::Organization.github : @mona

        if @owner.organization?
          @owner.enable_feature(:advanced_security_private_beta)
        else
          @owner.enable_feature(:advanced_security_public_beta)
        end

        print "Creating/updating example repo for code scanning..."
        @repo = Seeds::Objects::Repository.restore_premade_repo(
          location_premade_git: options[:repo_path],
          owner_name: @owner,
          repo_name: options[:name] || "codescan-repo-with-alerts-#{Time.now.to_i}",
          is_public: !@owner.organization?,
        )
        puts " ✅\n\n"

        @repo.refs.each do |ref|

          if ref.name == "master"

            # for master we process the sarif from all commits, going from oldest to newest
            # this allows us to populate turboscan with an appropriate history for the repo,
            # giving (eg) resolved alerts and timeline events in the example repo
            self.process_branch(ref)

          elsif ref.name.start_with? "protected_"

            @repo.protect_branch(ref.name, creator: @mona)

            # we process all commits since this branch diverged from master
            self.process_branch(ref, excluded_oids = [@repo.refs["master"].commit.oid])

          elsif ref.name.start_with? "pr_"

            self.process_branch(ref, excluded_oids = [@repo.refs["master"].commit.oid])

            pull_request = self.create_pr(@repo, ref,
              title: "Change the makeup of alerts",
              body: "This PR changes the alerts present.",
              base: "master"
            )

            self.create_annotations(pull_request: pull_request)
          else

            self.process_branch(ref, excluded_oids = [@repo.refs["master"].commit.oid])

          end

        end

        puts "\n"
        puts "Your new repository is ready:\n"
        puts "    http://#{GitHub.host_name}/#{@repo.nwo}"
        puts "\n"
      end

      def self.process_branch(ref, excluded_oids = [])

        # all the parent commits from the target ref are processed in order from oldest to newest,
        # except for those that are also present in the history of the excluded oids
        @repo.commits.except(heads=[ref.commit.oid], exclude=excluded_oids).reverse_each do |commit|
          self.process_sarif(commit, ref.name)
        end

      end

      def self.process_sarif(commit, ref_name)

        print "Processing sarif files for #{ref_name}..."

        require "tempfile"

        sarifs = []

        # get content of all SARIF files repo (blob_by_id works even for huge files)
        commit.repository.tree_entries(commit.tree_oid, "", recursive: true)[1].each do |entry|
          sarifs << commit.repository.blob_by_oid(entry.oid).data if File.extname(entry.name).casecmp?(".sarif")
        end

        if sarifs.count > 0
          # turbo-scan treats each new sarif file as a replacement for previous results,
          # hence we combine all the sarif files by concatenating the 'runs' arrays
          combined_sarif = JSON.parse(sarifs[0])
          sarifs.drop(1).each do |content|
            combined_sarif["runs"] += JSON.parse(content)["runs"]
          end
          file = Tempfile.new("concatenated_sarif")
          file.write(combined_sarif.to_json)
          file.close

          command = "#{@env_vars} #{@bin_path} -repo #{commit.repository.id} -ref refs/heads/#{ref_name} -commit #{commit.oid} #{file.path}"

          unless call_turboscan(command, @verbose)
            abort "\n\n🚨  Processing of sample SARIF data failed.🚨"
          end
        end

        puts " ✅\n\n"
      end

      def self.parse_sarif_present?
        Pathname.new(@bin_path).executable?
      end

      def self.call_turboscan(command, verbose)

        if verbose
          puts command+"\n"
          succeeded = system(command)
        else
          # call the command in a manner that suppresses output
          `#{command}`; succeeded=$?.success?
        end
        succeeded
      end

      def self.create_pr(
        repo, ref,
        user: @mona,
        title: "Change the makeup of alerts",
        body: "This PR changes the alerts present.",
        base: "master"
      )
        print "Creating PR merging #{ref.name} into master..."
        pull_request = PullRequest.create_for!(repo,
          user: user,
          title: title,
          body: body,
          head: ref.name,
          base: base,
        )
        puts " ✅\n\n"
        pull_request
      end

      def self.create_annotations(pull_request:)
        print "Creating PR annotations..."
        Seeds::Objects::Integration.create_code_scanning_integration
        check_run = ::CheckRun.create_for_code_scanning_analysis(
          repository: pull_request.repository,
          commit_oid: pull_request.head_sha,
          tool_names: [::CodeScanning::Tool.default_tool_name],
        ).first
        check_run.check_suite.update(head_branch: pull_request.head_ref)
        CreateCodeScanningAnnotationsJob.perform_now(check_run_id: check_run.id)
        puts " ✅\n\n"
      end
    end
  end
end
