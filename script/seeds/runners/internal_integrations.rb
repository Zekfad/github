# frozen_string_literal: true

require_relative "../runner"
# Do not require anything else here. If you need something for your runner, put that in `self.run`.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class Runner
    class InternalIntegrations < Seeds::Runner
      def self.help
        <<~EOF
        Create Internal GitHub-Owned Integrations.
        Creates Slack and Pull Panda.

        Override their URLs by using the env vars SLACK_INTEGRATION_URL and PULL_PANDA_INTEGRATION_URL.

        Force deletion and recreation of the integrations with FORCE=true
        EOF
      end

      def self.run(options = {})
        Seeds::Objects::Integration.create_pull_panda_integration
        Seeds::Objects::Integration.create_slack_integration
      end
    end
  end
end
