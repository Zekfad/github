Seeds
---

Seeds are a fantastic resource for developers. They allow us to easily inject data into our local environments. These are only super effective when people can create new ones for new projects, manipulate the existing ones, and understand the seeding logic with ease.

This folder encompasses some helpers and object wrappers that make this really easy. All the the `objects` folder is unit tested as well, so you don't have to worry about those breaking with changes!

## How to make a seed?

1. Run `bin/seed generate NAME --type runner` (or `--type object`)
2. Complete any thing it asks and fill out the resulting files
3. You MUST test every runners' and objects' effects, side effects and outputs. Together we can ensure this is a reliable seed framework.

### FAQ & Protips

- You can test your runner by running `.execute`, which is defined in the `Seeds::Runner` base class. This will also run a setup script
  - e.g. `assert_differences("::Repository.count" => 1, "::MyObject.count" => 3) { Seeds::Runners::MyRunner.execute(options) }`
- `Seeds::Objects` has 2 errors to use throughout for flow control: `ActionFailed` and `CreateFailed`. They both inherit from `ObjectError` for easier catching.
    - Use these errors to help kick you out and show the user an error. (`Seeds::Run` catches the `Objects::ObjectError` and exits)
    - E.g.
    ```ruby
    org = Seeds::Objects::Organization.create(...)
    raise Objects::CreateFailed, "org didn't save!" unless org.persisted?
    ```
- Avoid using the `faker` gem in any field that has validation. The faker gem has a pretty rigid way of creating fake data and they may not always be valid for our rules. This means we could introduce flaky seeds and tests as an object may fail to be created that you've relied on or asserted to be present.
- Don't wrap a model if you're just passing the same parameters to the model. Seeds are useful when they wrap new functionality and default data for you. E.g. This is not recommended
  ```ruby
  class Seeds::Objects::MyObject
    def create(params)
      ::MyObject.create(params)
    end
  end
  ```
