# frozen_string_literal: true

module Seeds
  module Objects
    class PullRequestReview
      POSSIBLE_REVIEW_STATES = %i(
        approved
        changes_requested
        commented
        dismissed
      ).freeze

      def self.create(pull_request:, user:, state: POSSIBLE_REVIEW_STATES.sample)
        ::PullRequestReview.create(
          pull_request: pull_request,
          user: user,
          state: state,
          head_sha: pull_request.head_sha,
          body: Faker::Lorem.paragraph,
          submitted_at: Time.now
        )
      end
    end
  end
end
