# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class Commit
      def self.create(repo:, message: "A Message", committer:, branch_name: "master", files: {"File.md" => "My Content"})
        metadata = {
          message: message,
          committer: committer,
          author: committer
        }

        ref = repo.refs.find_or_build("refs/heads/#{branch_name}")
        commit = ref.append_commit(metadata, committer) do |changes|
          files.each do |name, content|
            changes.add(name, content)
          end
        end

        commit
      end
    end
  end
end
