# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class Push
      def self.create(committer:, repo:, branch_name:, files:, message: "Example commit message for checks")
        metadata = {
          message: message,
          committer: committer,
          author: committer,
        }

        original = Resque.inline
        ref = Seeds::Objects::Repository.branch(repo, branch_name)
        begin
          Resque.inline = true
          after_commit = ref.append_commit(metadata, committer) do |changes|
            files.each do |name, content|
              changes.add(name, content)
            end
          end
        ensure
          Resque.inline = original
        end

        push = ::Push.find_by(
          repository_id: repo.id,
          pusher_id: committer.id,
          ref: ref.qualified_name,
          after: after_commit.oid
        )
        raise Objects::CreateFailed, "Push was not created from commit" if push.nil?
        push
      end
    end
  end
end
