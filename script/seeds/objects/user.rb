# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class User
      def self.monalisa
        create(login: "monalisa", email: "octocat@github.com")
      end

      def self.two_factor_enable!(user:)
        if (cred = user.two_factor_credential)
          return cred
        end

        two_factor_cred = TwoFactorCredential.new(user: user)
        two_factor_cred.generate_secrets!
        two_factor_cred.save!
        two_factor_cred
      end

      def self.add_stafftools(user)
        Seeds::Objects::Organization.github(admin: user)

        user.clear_employee_memo
        user.update(gh_role: "staff")
        unless user.stafftools_roles.any? { |role| role.is_a?(StafftoolsRole) }
          user.stafftools_roles << StafftoolsRole.new(name: "super-admin")
        end

        github = Seeds::Objects::Organization.github
        employee_team = Seeds::Objects::Team.create!(org: github, name: "Employees")
        employee_team.add_member(user)

        user
      end

      def self.create(login:, email: nil)
        user = ::User.find_by(login: login)
        return user if user

        email ||= "#{login}@github.com"
        user = ::User.create(
          login: login,
          password: GitHub.default_password,
          email: email,
          require_email_verification: false,
          billing_attempts: 0,
          plan: "medium",
        )
        raise Objects::CreateFailed, user.errors.full_messages.to_sentence unless user.valid?
        user.emails.map(&:verify!)
        user
      end

      def self.create_pat(user:, scopes:)
        normalized_scopes = begin
          normalized_scopes = OauthAccess.normalize_scopes(scopes)
          if user.site_admin? && scopes&.include?("site_admin")
            normalized_scopes << "site_admin"
          end
          # Ensure the correct order and remove any duplicate scopes.
          OauthAccess.normalize_scopes(normalized_scopes, visibility: :all)
        end

        token, hashed_token = OauthAccess.random_token_pair
        # Build instance before saving it so we track the GTID properly
        # of the save operation.
        last_operations = DatabaseSelector::LastOperations.from_token(token)
        access = user.oauth_accesses.build do |acc|
          acc.application_id = ::OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
          acc.application_type = ::OauthApplication::PERSONAL_TOKENS_APPLICATION_TYPE
          acc.description = "Seeds generated PAT at #{DateTime.now.iso8601} (##{user.oauth_accesses.count})"
          acc.scopes = normalized_scopes
          acc.token_last_eight = token.last(8)
          acc.hashed_token = hashed_token
        end
        access.save!

        [token, access]
      end
    end
  end
end
