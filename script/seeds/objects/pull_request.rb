# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class PullRequest
      def self.create(repo:, committer:, commit: nil)
        base_commit = Seeds::Objects::Commit.create(
          repo: repo,
          committer: committer,
          branch_name: "master",
          files: {"file.md" => "# File\n\nthis is a file"},
          message: "New commit - add file.md.",
        )
        base_ref = repo.refs.first

        # Make a new commit
        commit = repo.commits.create({ message: "A contribution", committer: committer }, base_commit.oid) do |files|
          files.add "New File", "This is a new file"
        end

        # Create the head ref
        branch_name = SecureRandom.hex
        head_ref = repo.refs.create("refs/heads/branch-#{branch_name}", commit.oid, committer)

        # Create the pull
        ::PullRequest.create_for!(repo,
                                  user: committer,
                                  title: "New Pull Request",
                                  body: "This adds a new file to the repo.",
                                  head: head_ref.name,
                                  base: base_ref.name)
      end
    end
  end
end
