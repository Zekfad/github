# frozen_string_literal: true

module Seeds
  module Objects
    class PullRequestReviewComment
      def self.create(user:, pull_request_review:)
        review_thread = ::PullRequestReviewThread.create(
          pull_request_review: pull_request_review,
          pull_request: pull_request_review.pull_request
        )

        pull_request = pull_request_review.pull_request
        head_oid = pull_request.head_sha
        diff = pull_request.async_diff(end_commit_oid: head_oid).sync
        path = diff.deltas.first.path.dup
        original_position = 1

        end_position_data = ::PullRequestReviewComment::LegacyPositionData.new(
          diff: diff,
          comment_path: path,
          diff_position: original_position
        )

        ::PullRequestReviewComment.create(
          pull_request: pull_request,
          pull_request_review_thread: review_thread,
          user: user,
          body: Faker::Lorem.paragraph,
          original_position: original_position,
          end_position_data: end_position_data,
          path: path
        )
      end
    end
  end
end
