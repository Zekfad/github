# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class Integration
      def self.create_slack_integration(
        owner: Objects::Organization.github,
        integration_url: ENV.fetch("SLACK_INTEGRATION_URL", "http://localhost:4567"),
        force: ENV.fetch("FORCE", "false") == "true"
      )
        integration = Apps::Internal.integration(:slack)
        if integration
          if force
            integration.destroy
          else
            return integration
          end
        end

        permissions = {
          "checks" => :read,
          "contents" => :read,
          "deployments" => :write,
          "issues" => :write,
          "metadata" => :read,
          "pull_requests" => :write,
          "statuses" => :read
        }

        events = %w(
          check_run
          check_suite
          commit_comment
          create
          delete
          deployment
          deployment_status
          issue_comment
          issues
          public
          pull_request
          pull_request_review
          pull_request_review_comment
          push
          release
          reminder
          repository
          status
        )

        Seeds::Objects::Integration.create(
          owner: owner,
          app_name: "slack",
          id: Apps::Internal::Slack::PRODUCTION[:id].call,
          permissions: permissions,
          events: events,
          integration_url: integration_url,
          callback_path: "/github/oauth/callback",
          webhook_path: "/github/events",
          setup_path: "/github/setup"
        )
      end

      def self.create_pull_panda_integration(
        owner: Objects::Organization.github,
        integration_url: ENV.fetch("PULL_PANDA_INTEGRATION_URL", "http://localhost:3011")
      )
        integration = Apps::Internal.integration(:pull_panda)
        return integration if integration

        Seeds::Objects::Integration.create(
          id: Apps::Internal::PullPanda::ID,
          app_name: "Pull Panda",
          owner: owner,
          integration_url: integration_url
        )
      end

      def self.create_team_sync_integration(
        owner: Objects::Organization.github,
        integration_url: ENV.fetch("TEAM_SYNC_INTEGRATION_URL", "http://example.com"),
        id: nil
      )
        integration = Apps::Internal.integration(:team_sync)
        if id.present? && integration.present? && integration.id != id
         raise Objects::CreateFailed, "Integration already existed with id #{integration.id}, which is not the supplied id #{id}"
        end
        return integration if integration

        Seeds::Objects::Integration.create(
          id: id,
          app_name: Apps::Internal::TeamSync::APP_NAME,
          owner: owner,
          permissions: default_permissions.merge({ "members" => :write }),
          integration_url: integration_url
        ).tap do |integration|
          integration.update(full_trust: true)
        end
      end

      def self.create_code_scanning_integration(
        owner: GitHub.trusted_oauth_apps_owner,
        integration_url: "https://github.com/features/security"
      )
        integration = Apps::Internal.integration(:code_scanning)
        return integration if integration.present?

        Seeds::Objects::Integration.create(
          app_name: Apps::Internal::CodeScanning::APP_NAME,
          owner: owner,
          integration_url: integration_url,
          skip_restrict_names_with_github_validation: true
        ).tap do |integration|
          integration.update(full_trust: true)
        end
      end

      def self.create(owner:, app_name:, repo: nil, id: nil, permissions: default_permissions,
          events: default_events, integration_url: "http://localhost:4567",
          callback_path: "/github/callback", webhook_path: "/github/events", setup_path: "/github/setup",
          skip_restrict_names_with_github_validation: false)
        repo ||= []
        slug = app_name.parameterize

        integration = ::Integration.find_by(slug: slug)

        # Handle issues with integration alias
        if integration.nil? && (integration_alias = IntegrationAlias.find_by(slug: slug))
          integration = integration_alias.integration
          integration_alias.destroy if integration.nil?
        end

        # Make sure the owner of the integration we found is the id we specified.
        # If it isn't this means we have a conflict and we need to bail
        if id && integration_by_id = ::Integration.find_by(id: id)
          if integration_by_id != integration || integration_by_id.owner != owner
            raise Objects::CreateFailed, <<~EOF
            == What went wrong:
            Existing integration has the ID "#{integration&.id}"", the slug "#{integration&.slug}", and is owned by "#{integration&.owner}".
            Expecting integration to have ID #{id}, the slug #{slug}, and be owned by #{owner}.

            == How do I fix it?
            Make sure the ID, slug, and owner match the exiting integration, or choose a different option.
            EOF
          end
        end

        # Make sure the owner of the integration we found is the owner we specified.
        # If it isn't this means we have a conflict and we need to bail
        if integration && integration.owner != owner
          raise Objects::CreateFailed, <<~EOF
          == What went wrong:
          Existing integration has the slug #{slug}, but was not owned by #{owner}.
          It was owned by: #{integration.owner}.

          == How do I fix it?
          Choose a different slug or specify #{integration.owner} as the owner, not #{owner}
          EOF
        end

        installer = owner.is_a?(::Organization) ? owner.admin : owner
        if integration
          install(integration: integration, repo: repo, installer: installer, target: owner)
          return integration
        end

        # If we have integration associations already (from a previous integration), delete them
        if id
          ::IntegrationInstallation.where(integration_id: id).delete_all
          ::IntegrationInstallTrigger.where(integration_id: id).delete_all
          ::IntegrationKey.where(integration_id: id).delete_all
          ::IntegrationTransfer.where(integration_id: id).delete_all

          listings = ::IntegrationListing.where(integration_id: id)
          ::IntegrationListingFeature.where(integration_listing: listings).delete_all
          IntegrationListingLanguageName.where(integration_listing: listings).delete_all
          listings.delete_all

          requests = ::IntegrationInstallationRequest.where(integration_id: id)
          ::IntegrationInstallationRequestRepository.where(integration_installation_request: requests).delete_all
          requests.delete_all

          versions = ::IntegrationVersion.where(integration_id: id)
          ::IntegrationSingleFile.where(integration_version_id: versions.map(&:id)).delete_all
          ::IntegrationContentReference.where(integration_version: versions).delete_all
          versions.delete_all
        end

        params = {
          owner: owner,
          name: app_name,
          slug: app_name.parameterize,
          url: integration_url,
          public: true,
          default_permissions: permissions,
          default_events: events,
          hook_attributes: { url: File.join(integration_url, webhook_path), secret: "foo123" },
          callback_url: File.join(integration_url, callback_path),
          setup_url: File.join(integration_url, setup_path),
          skip_restrict_names_with_github_validation: skip_restrict_names_with_github_validation,
        }
        params[:id] = id if id

        # Make sure a bot is assigned if it exists already
        bot = ::Bot.find_by_slug(params[:slug])
        params[:bot] = bot if bot

        integration = ::Integration.create(params)
        raise Objects::CreateFailed, integration.errors.full_messages.to_sentence unless integration.persisted?
        install(integration: integration, repo: repo, installer: installer, target: owner)
        integration
      end

      def self.default_events
        %w(pull_request check_suite check_run)
      end

      def self.default_permissions
        {
          "statuses" => :write,
          "contents" => :write,
          "pull_requests" => :write,
          "issues" => :write,
          "checks" => :write,
          "metadata" => :read,
        }
      end

      def self.install(integration:, repo:, target:, installer:)
        result = integration.install_on(target, repositories: repo, installer: installer, version: integration.latest_version)
        raise(Objects::CreateFailed, result.error) unless result.success?
        result.installation
      end
    end
  end
end
