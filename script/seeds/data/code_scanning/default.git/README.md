# Default repo for local code scanning development

The features that this repo currently supports are...

- A `master` branch that contains various alerts, some of which are open at `HEAD` and some that have been closed (fixed in an earlier commit).
- An example of a path query.
- An example of a query that makes use of related locations.
- A feature branch that changes the state of alerts present, creating some and fixing others.
- An open PR corresponding to the feature branch.
	- NB the PR does not yet show any analysis, as PR integration is a work-in-progress
