#!/usr/bin/env ruby
#/ Usage: ruby build-load-path-cache.rb [path/to/standalone/bundler/setup.rb]
#/ Generate a fast load path setup file. This preprocesses the standalone
#/ bundler/setup.rb file generated by bundle install and writes it to
#/ config/load_path.rb.
#/
#/ The load path generated by this script is used by all binstubs and anything
#/ that require config/basic. It's generated by script/bootstrap.rb as part of
#/ general environment prep.
require "rbconfig"
require "find"
require "fileutils"

# figure out the vendor/gems subpath for this ruby version
root = File.expand_path("../..", __FILE__)
vendor_gems = "vendor/gems/#{RUBY_VERSION}"

# preprocess the bundle/setup.rb file
ruby_version = RbConfig::CONFIG["ruby_version"]
setup_file = "#{vendor_gems}/bundler/setup.rb"
setup_source = File.read(setup_file)
setup_source.gsub!(/^\$:/, "setup_paths")
setup_source.gsub!(/__FILE__/, "'#{setup_file}'")
setup_paths = []
eval(setup_source) # rubocop:disable Security/Eval

# remove any load path entries whose dirs don't exist
setup_paths.reject! { |path| !File.directory?(path) }
setup_paths.unshift "#{root}/lib"

load_paths = [
  *setup_paths,                    # gem ruby libs and c exts
  RbConfig::CONFIG["rubylibdir"],  # ruby stdlibs
  RbConfig::CONFIG["rubyarchdir"], # ruby built-in c exts
]

setup_paths = setup_paths.map do |path|
  File.expand_path(path).sub(root, "")
end

# create the load_path.rb file
setup_fast_file = "config/load_path.rb"
File.open("#{setup_fast_file}+", "w") do |file|
  file.puts "# Load path file generated by #$0\n"

  file.puts "if !defined?(LOAD_PATH_SETUP)"
  file.puts "  $:.unshift("
  setup_paths.each do |setup_path|
    file.puts %Q{    ENV["RAILS_ROOT"] + #{setup_path.inspect},}
  end
  file.puts "  )"
  file.puts "  LOAD_PATH_SETUP = true"
  file.puts "end"
end
File.rename("#{setup_fast_file}+", setup_fast_file)
