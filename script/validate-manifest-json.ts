import {promises as fs} from 'fs'
import * as path from 'path'
import {createHash} from 'crypto'

const allowedExtraFiles = new Set(['manifest.json', 'integrities', 'timings'])

async function main() {
  const dir = path.resolve(process.argv[2] || 'public/assets')
  const manifest: Set<string> = new Set(Object.values(JSON.parse(await fs.readFile(path.join(dir, 'manifest.json'), 'utf-8'))))
  const seen: Set<string> = new Set()
  const files: string[] = []
  const errors: Error[] = []
  for(const file of await fs.readdir(dir)) {
    // Ignore map files
    if (file.endsWith('.map')) {
      seen.add(file)
      continue
    }

    if (allowedExtraFiles.has(file)) {
      seen.add(file)
      continue
    }

    // Check the manifest has the entry
    if (manifest.has(file)) {
      manifest.delete(file)
      seen.add(file)
      continue
    }

    const pathName = path.join(dir, file)
    const stat = await fs.lstat(pathName)
    if (stat.isSymbolicLink()) {
      const linked = await fs.readlink(pathName)
      if (manifest.has(linked)) {
        manifest.delete(linked)
        seen.add(linked)
      }
      if (!seen.has(linked)) {
        errors.push(new Error(`Saw extraneous symlink: ${file} -> ${linked}`))
        continue
      }
      seen.add(file)
    }

    if (!seen.has(file)) {
      errors.push(new Error(`Saw file not in manifest.json: ${file}`))
    }
  }

  const remainder = Array.from(manifest).map(file => new Error(`Did not see file listed in manifest: ${file}`))
  const allErrors = [...errors, ...remainder]
  for (const error of allErrors) {
    console.error(error.message)
  }
  console.log(`Scanned ${seen.size} files. Manifest check ${allErrors.length ? 'failed' : 'passed'}.`)
  process.exit(allErrors.length)
}

main().catch(e => {
  console.error(e)
  process.exit(1)
})
