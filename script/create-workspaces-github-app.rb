#!/usr/bin/env safe-ruby
# frozen_string_literal: true

require_relative "../config/environment"

unless Rails.env.development?
  raise "This is a dev only script"
end

class CreateWorkspacesGitHubApp
  APPS = [{
    name: "production",
    key: GitHub.codespaces_app_key,
  }, {
    name: "ppe",
    key: GitHub.codespaces_app_ppe_key,
  }, {
    name: "dev",
    key: GitHub.codespaces_app_dev_key,
  }]

  def call
    APPS.each do |app|
      name = "GitHub Workspaces #{app[:name]}"

      if existing = Integration.find_by(key: app[:key])
        puts "Deleting existing app \"#{name}\"..."
        existing.destroy!
      end

      puts "Creating app \"#{name}\"..."

      integration = Integration.create!(
        key: app[:key],
        owner: GitHub.trusted_oauth_apps_owner,
        name: name,
        url: "http://github.localhost",
        callback_url: "http://github.localhost/fake",
        default_permissions: {
          "metadata" => :read,
          "contents" => :write,
          "pull_requests" => :write,
        },
        public: false,
        hook_attributes: {},
        skip_restrict_names_with_github_validation: true,
        skip_generate_slug: false,
        no_repo_permissions_allowed: false,
        full_trust: false,
      )

      puts "Enabling as a global app..."

      GitHub.flipper[:global_apps].enable(integration)
    end
  end
end

if __FILE__==$0
  CreateWorkspacesGitHubApp.new.call
  puts "Integrations created!"
end
