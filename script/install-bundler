#!/bin/bash
#/ Usage: build-ruby
#/ Uninstall bundler and install correct version

set -e

BUNDLER_VERSION="2.2.0.dev"

# ruby sometimes ships with a version of bundler newer than the github version
# the newer version will take precedence when running commands like `bundle
# exec ruby -e "0"`. With this being a default gem, we cannot use `gem
# uninstall bundler -v x.x.x` so we manually delete the built in bundler if and
# only if the version is newer than our target version
function remove_ruby_bundler() {
  local prefix="${1}"
  local built_in_bundler="${2}"

  if [ -e "$prefix/lib/ruby/*.*.0/bundler.rb" ]; then
    echo "removing bundler shipped with ruby ($built_in_bundler)"
    rm "$prefix/lib/ruby/*.*.0/bundler.rb"
    rm -rf "$prefix/lib/ruby/*.*.0/bundler"
    rm -rf "$prefix/lib/ruby/gems/*.*.0/gems/bundler-$built_in_bundler"
    rm -f "$prefix/lib/ruby/gems/*.*.0/specifications/default/bundler-$built_in_bundler.gemspec"
  fi
}

function install_github_bundler() {
  local prefix=${1}
  local bundler_dir=${2}
  local installed_version="$($prefix/bin/bundle --version | awk '{ print $3 }')"
  [[ $installed_version == $BUNDLER_VERSION ]] && return

  ( cd "${bundler_dir}" && "${prefix}/bin/gem" build bundler.gemspec )

  (
    unset GEM_HOME
    unset GEM_PATH
    "${prefix}/bin/gem" install --no-document --force --default "${bundler_dir}/bundler-$BUNDLER_VERSION.gem"
    "${prefix}/bin/gem" install --no-document --force "${bundler_dir}/bundler-$BUNDLER_VERSION.gem"
  )

  local needs_older="$(ruby -e "puts Gem::Version.new('$BUNDLER_VERSION') < Gem::Version.new('$installed_version') ? 1 : 0")"
  remove_ruby_bundler $prefix $installed_version
}

install_github_bundler $1 $2
