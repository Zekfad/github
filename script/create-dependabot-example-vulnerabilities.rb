#!/usr/bin/env safe-ruby
# frozen_string_literal: true

require_relative "../config/environment"
require "faker"

unless Rails.env.development?
  abort "This can only be run in development"
end

class CreateExampleVulnerabilities
  SEEDS = [
    {
      identifier: "TEST-0000-0001",
      ecosystem: "RubyGems",
      severity: "critical",
      versions: [
        {
          package_name: "octokit",
          vulnerable_versions: ">= 4.10.0, < 4.14.0",
          fixed_in: "4.14.0",
        },
        {
          package_name: "octokit",
          vulnerable_version: ">= 3.0.0, < 3.2.0",
          fixed_in: nil,
        },
      ],
    },
    {
      identifier: "TEST-0000-0002",
      ecosystem: "RubyGems",
      severity: "moderate",
      versions: [
        {
          package_name: "multipart-post",
          vulnerable_versions: ">= 2.0.0",
          fixed_in: nil,
        },
      ],
    },
    {
      identifier: "TEST-0000-0003",
      ecosystem: "RubyGems",
      severity: "high",
      versions: [
        {
          package_name: "faraday",
          vulnerable_versions: "> 0.1.0, < 0.15.4",
          fixed_in: "0.15.3",
        },
      ],
    },
    {
      identifier: "TEST-0000-0004",
      ecosystem: "RubyGems",
      severity: "low",
      versions: [
        {
          package_name: "octokit",
          vulnerable_versions: "> 4.10.0",
          fixed_in: nil,
        },
      ],
    },
  ]

  def create_vulnerabilities
    SEEDS.each do |params|
      next if Vulnerability.find_by(identifier: params[:identifier])

      vulnerability = Vulnerability.create!(
        ghsa_id: new_ghas_id,
        identifier: params[:identifier],
        platform: params[:ecosystem],
        severity: params[:severity],
        description: new_description,
        classification: "other",
        status: "published",
        created_by: User.ghost,
        published_by: User.ghost,
        published_at: Time.now.utc,
      )

      params[:versions].each do |version|
        vulnerability.vulnerable_version_ranges.create(
          affects: version[:package_name],
          ecosystem: params[:ecosystem],
          requirements: version[:vulnerable_versions],
          fixed_in: version[:fixed_in],
        )
      end
    end
  end

  def call
    create_vulnerabilities
  end

  private

  def vulnerability_class
    ::Vulnerability
  end

  def new_ghas_id
    ::AdvisoryDB::GhsaIdGenerator.generate_unique_ghsa_id
  end

  def new_description
    Faker::Lorem.paragraphs(number: 4).join("\n\n")
  end
end

if __FILE__==$0
  CreateExampleVulnerabilities.new.call
end
