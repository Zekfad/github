#!/bin/bash
# Set CI environment variables
#
# To add these environment variables to your script, add this line:
# source script/ci/env.sh
#
export USER="${USER:-example}"

# GH_CI_RUNTIME is the name of the build pipeline check (e.g., "github-all-features")
export GH_CI_RUNTIME="${JOB_NAME}"

# setup environment
export RAILS_ENV="test"
export RACK_ENV="$RAILS_ENV"
export RAILS_ROOT=$(cd "$(dirname "${BASH_SOURCE[0]}")"/../.. && pwd)
export RACK_ROOT="$RAILS_ROOT"
export PATH="/usr/local/share/nodenv/shims:$PATH"
export PATH="${RAILS_ROOT}/bin:${RAILS_ROOT}/vendor/ruby/$(config/ruby-version)/bin:$PATH"
export NODENV_VERSION="v0.10.21"
export GITHUB_CI=1

# Clean out the ruby environment
export RUBYLIB=
export RUBYOPT=

# If either are set, make sure both are set
if [ -n "${ENTERPRISE+x}" -o -n "${FI+x}" ]; then
  export FI=1
  export ENTERPRISE=1
fi
