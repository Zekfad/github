# frozen_string_literal: true

module Search::Serializers::Geyser
  module V1
    extend self

    # Serialize an array of ParsedNodes back into a query string.
    #
    # parsed_query - An array of ParsedNodes
    #
    # Returns a string
    def serialize(parsed_query)
      # ParsedNodes know how to serialize themselves, so we can just call to_s
      # on everything and join them together.
      parsed_query.map(&:to_s).join(" ")
    end
  end
end
