# rubocop:disable Style/FrozenStringLiteralComment

module Search
  module Queries

    class WorkflowRunQuery < ::Search::Query

      # The set of fields that can be queried when performing a workflow run search
      FIELDS = [:sort, :status, :conclusion, :branch, :name, :event, :action, :workflow, :creator, :pusher, :is, :actor].freeze

      # The mapping of workflow_run-facing sort values to the corresponding values used
      # in the search index.
      SORT_MAPPINGS = {
        "created"      => "created_at"
      }.freeze

      # The default sort ordering to use in the absence of a query or any
      # other sort information
      DEFAULT_SORT = %w[created desc].freeze

      # Construct a WorkflowRunQuery. The query can be restricted to a single
      # repository by providing a :repo_id option.
      #
      # opts - The options Hash.
      #   :repo_id - The repository id to limit the query for
      #   :lab - Whether or not it should search only for lab workflow runs
      #
      def initialize(opts = {}, &block)
        super(opts, &block)
        @index = Elastomer::Indexes::WorkflowRuns.new
        @repo_id = opts.fetch(:repo_id, nil)
        @lab = opts.fetch(:lab, nil)
        @workflow_id = opts.fetch(:workflow_id, nil)
      end

      # Internal: Returns a Hash that will be passed as URL params for the query.
      def query_params
        { type: "workflow_run" }
      end

      # Internal: Returns the Array of field names that will be queried.
      def query_fields
        return @query_fields if defined? @query_fields
        @query_fields = []

        search_in.each { |field|
          case field
          when "name";  @query_fields << "name^1.2"
          end
        }

        @query_fields = %w[name^1.2 status] if @query_fields.empty?
        @query_fields
      end

      # Internal: Constructs the actual `:query` portion of the query
      # document. This will later be wrapped in a filtered query if search
      # qualifiers were also used.
      #
      # Returns the search query Hash.
      def query_doc
        return if escaped_query.empty?

        query = { query_string: {
          query: escaped_query,
          fields: query_fields,
          default_operator: "AND",
          use_dis_max: true,
          analyzer: "lowercase",
        }}

        query = { function_score: {
          query: query,
          score_mode: "multiply",
          functions: [{
            field_value_factor: {
              field: "rank",
              missing: 1,
            },
          }],
        }}

        query
      end

      # Internal: Returns the sorting options Array.
      def build_sort
        return if sort.blank?

        build_sort_section(sort, "_score", SORT_MAPPINGS)
      end

      # Returns the default sort ordering to use in the absence of a query or
      # any other sort information.
      def default_sort
        DEFAULT_SORT
      end

      # Internal: Returns the Hash of filter hashes.
      def filter_hash
        return @filter_hash if defined? @filter_hash

        filters = {}

        filters[:name]        = builder.query_filter(:name) { |str| str.downcase }
        filters[:repo_id]     = builder.repository_filter(current_user, @repo_id, nil, user_session: user_session, ip: remote_ip)
        filters[:status]      = builder.query_filter(:status)
        filters[:conclusion]  = builder.query_filter(:conclusion)
        filters[:workflow_name] = builder.query_filter(:workflow_name, :workflow)
        filters[:head_branch] = builder.query_filter(:head_branch, :branch)
        filters[:event]       = builder.query_filter(:event)
        filters[:action]      = builder.query_filter(:action)
        filters[:creator]     = builder.query_filter(:creator)
        filters[:pusher]      = builder.query_filter(:pusher)
        filters[:is]          = builder.query_filter(:is)
        filters[:actor]       = builder.query_filter(:actor)
        filters[:title]       = builder.query_filter(:title)

        unless @lab.nil?
          qualifiers[:lab].clear.must(@lab)
          filters[:lab] = builder.term_filter(:lab)
        end

        unless @workflow_id.nil?
          qualifiers[:workflow_id].clear.must(@workflow_id)
          filters[:workflow_id] = builder.term_filter(:workflow_id)
        end

        @filter_hash = filters
      end

      def valid_query?
        return false unless super

        if escaped_query.empty?
          if filter_hash.empty?
            @invalid_reason = ::Search::Query::REASON_EMPTY_QUERY
            return false
          end
        end

        true
      end

      # Internal: Prune results and then run the `normalizer` on the results
      # if one was provided.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the normalized Array of hits.
      def normalize(results)
        prune_results(results) unless @results_pruned
        super(results)
      end

      # Internal: Take the array of workflow run results and remove those that
      # no longer exist in the database or have been flagged as spammy. We
      # will only perform this pruning in test and production; development
      # mode is for testing out everything.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the pruned Array of hits.
      def prune_results(results)
        workflow_run_ids = results.map { |h| h["_id"] }
        workflow_runs = Actions::WorkflowRun.where(id: workflow_run_ids).includes(:check_suite, :trigger, :workflow, :repository).index_by(&:id)
        error_reported = false

        results.delete_if do |h|
          workflow_run_id = h["_id"].to_i
          workflow_run = workflow_runs[workflow_run_id]

          if h["_type"] != "workflow_run"
            unless error_reported
              GitHub.dogstats.increment("search.query.errors.type", { tags: ["index:" + index.name.to_s] })
              error_reported = true
            end
            true
          elsif workflow_run.nil?
            RemoveFromSearchIndexJob.perform_later("workflow_run", workflow_run_id)
            true
          else
            if out_of_sync(workflow_run)
              Search.add_to_search_index("workflow_run", workflow_run.id)
              GitHub.dogstats.increment("workflow_runs.out_of_sync")
              true
            else
              h["_model"] = workflow_run
              false
            end
          end
        end
      end

      def out_of_sync(workflow_run)
        params = Search::Queries::WorkflowRunQuery.parse(@phrase)
        params.each do |(key, value)|
          filters_conclusion = key == :conclusion || (key == :is && CheckSuite.conclusions.keys.include?(value))
          return true if filters_conclusion && workflow_run.conclusion != value

          filters_status = key == :status || (key == :is && CheckSuite.statuses.keys.include?(value))
          return true if filters_status && workflow_run.status != value
        end
        false
      end

    end  # WorkflowRunQuery
  end  # Queries
end  # Search
