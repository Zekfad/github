# frozen_string_literal: true
require "set"

module Search
  module Queries
    class NotificationSearchQuery < ::Search::Query
      areas_of_responsibility :notifications

      attr_accessor :unauthorized_organization_ids

      # The set of fields that can be queried when performing a user search
      FIELDS = [:author, :is, :owner, :reason, :repo, :sort, :team].freeze

      UNIQ_FIELDS = [:sort].freeze

      ES_UNSUPPORTED_VALUE_TO_DB_VALUE_ENUM = {
        done: :archived,
        unread: :inbox_unread,
        read: :inbox_read
      }.freeze

      # Maps the qualifier strings to the value stored in the document
      TYPE_QUALIFIER_MAPPINGS = {
        "check-suite"                    => "CheckSuite",
        "commit"                         => "Commit",
        "gist"                           => "Gist",
        "issue"                          => "Issue",
        "pr"                             => "PullRequest",
        "pull-request"                   => "PullRequest",
        "release"                        => "Release",
        "repository-invitation"          => "RepositoryInvitation",
        "repository-vulnerability-alert" => "RepositoryVulnerabilityAlert",
        "repository-advisory"            => "RepositoryAdvisory",
        "team-discussion"                => "DiscussionPost",
        "discussion"                     => "Discussion",
      }

      # The mapping of user-facing sort values to the corresponding values used
      # in the search index.
      SORT_MAPPINGS = {
        "updated" => "updated_at",
      }.freeze

      # The default sort ordering to use in the absence of a query or any
      # other sort information
      DEFAULT_SORT = %w[updated desc].freeze

      # Default number of results to obtain from ES
      DEFAULT_PER_PAGE = 500

      # The total number of search results that will ever be returned is
      # capped. The farther you offset into a set of search results, the more
      # work the search index has to do. This adversely affects performance;
      # hence it is capped. /deal with it
      MAX_OFFSET = 500

      def initialize(opts = {}, &block)
        super(opts, &block)

        @index = Elastomer::Indexes::Notifications.new
        @unauthorized_organization_ids = opts[:unauthorized_organization_ids] || []
      end

      def query_params
        { type: "notification" }
      end

      def qualifiers=(value)
        super(value)

        qualifiers[:user_id].clear.must(current_user.id.to_s)

        # Map the `is:` qualifier values to the intenal must ES mappings
        if qualifiers.key?(:is) && qualifiers[:is].must?
          qualifiers[:is].must.each do |value|
            if TYPE_QUALIFIER_MAPPINGS.keys.include?(value.downcase)
              qualifiers[:thread_type].must(TYPE_QUALIFIER_MAPPINGS[value.downcase])
            end
          end
        end

        # Map the `-is:` qualifier values to the intenal must_not ES mappings
        if qualifiers.key?(:is) && qualifiers[:is].must_not?
          qualifiers[:is].must_not.each do |value|
            if TYPE_QUALIFIER_MAPPINGS.keys.include?(value.downcase)
              qualifiers[:thread_type].must_not(TYPE_QUALIFIER_MAPPINGS[value.downcase])
            end
          end
        end
      end

      def filter_hash
        return @filter_hash if defined? @filter_hash

        filters = {}

        filters[:user_id] = builder.term_filter(:user_id)
        filters[:thread_type] = builder.term_filter(:thread_type)
        filters[:owner_id] = Search::Filters::NotificationOwnerFilter.new(
          qualifiers: qualifiers,
          unauthorized_organization_ids: unauthorized_organization_ids,
        )
        filters[:"thread_meta.author_id"] = builder.user_filter(:"thread_meta.author_id", :author)

        # The list qualifier handles the :repo and :team qualifiers for us
        filters[:list] = Search::Filters::NotificationListFilter.new(
          qualifiers: qualifiers,
          current_user: current_user,
        )

        filters[:reason] = builder.term_filter(:reason) { |str| str.gsub(/-/, "_") }

        @filter_hash = filters
      end

      # Internal: Returns the Array of advanced search qualifiers supported by
      # this query type.
      def qualifier_fields
        FIELDS
      end

      # Returns the default sort ordering to use in the absence of a query or
      # any other sort information.
      def default_sort
        DEFAULT_SORT
      end

      # Internal: Returns the sorting options Array.
      def build_sort
        return if sort.blank?

        build_sort_section(sort, SORT_MAPPINGS)
      end

      # Internal: Prune results and then run the `normalizer` on the results
      # if one was provided.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the normalized Array of hits.
      def normalize(results)
        prune_results(results) unless @results_pruned
        super(results)
      end

      # Internal: Take the array of repository results and remove those that
      # no longer exist in the database, and filters :is values
      # against the DB when not supported by ES
      #
      # results - An Array of hits from the search index.
      #
      # Returns the pruned Array of hits.
      def prune_results(results)
        notification_ids = results.map { |h| h["_id"] }
        notifications = GitHub.newsies.web.all_by_notification_entry_ids_and_statuses(current_user, notification_ids, es_unsupported_statuses, offset: offset, limit: per_page)
                                          .index_by { |f| f[:notification_entry_id] }

        results.delete_if do |result|
          notification_id = result["_id"].to_i
          notification = notifications[notification_id]

          if notification
            result["_notification_thread"] = Platform::Models::NotificationThread.new(notification, user: current_user)
            false
          else
            true
          end
        end
      end

      def query_document
        # We overide the DEFAULT_PER_PAGE value here so that we can obtain more results from ES
        # and if using is:{read|unread|done} then filter and paginate them via MySQL.
        doc = super
        doc[:size] = DEFAULT_PER_PAGE
        doc[:from] = 0
        doc
      end

      private
      def es_unsupported_statuses
        statuses = [:inbox_read, :inbox_unread] # Set default statuses to filter by
        if qualifiers.key?(:is) && qualifiers[:is].must.present?
          unsupported_values = qualifiers[:is].must.map(&:to_sym).select { |v| ES_UNSUPPORTED_VALUE_TO_DB_VALUE_ENUM.keys.include?(v.downcase) }
          statuses = unsupported_values.map { |s| ES_UNSUPPORTED_VALUE_TO_DB_VALUE_ENUM[s] } unless unsupported_values.empty?
        end

        statuses
      end
    end
  end
end
