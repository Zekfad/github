# frozen_string_literal: true

module Search
  module Queries
    class GeyserCodeQuery < ::Search::Query
      areas_of_responsibility :code_search

      attr_reader :dark_ship, :language, :request_id, :repo_id, :search_scope

      # Construct a GeyserCodeQuery for a single repository. The query can be
      # restricted to a single language by providing a :language option.
      #
      # opts - the options hash for Search::Query.
      #   :current_user - The user executing the search (required).
      #   :dark_ship    - If true, query is dark traffic from legacy search (optional).
      #   :language     - The Linguist::Language to filter on (optional).
      #   :normalizer   - A lambda to normalize results for display (optional).
      #   :raw_phrase   - The raw search term (required).
      #   :repo_id      - The Repository ID to filter by (optional if global scope).
      #   :request_id   - The Request ID for tracing (optional).
      #   :search_scope - The query scope, one of :standard or :global (required).
      def initialize(opts = {}, &block)
        super(opts, &block)

        # @current_user, @normalizer, @raw_phrase assignment in super class
        @dark_ship    = opts[:dark_ship] ? true : false
        @language     = opts.fetch(:language, nil)
        @repo_id      = opts.fetch(:repo_id, 0) # Default 0 for global darkship
        @request_id   = opts.fetch(:request_id, nil)
        @search_scope = opts.fetch(:search_scope, :standard)
      end

      def execute
        return GeyserResults.empty unless enabled?
        unless valid_query?
          GitHub.dogstats.increment("search.query.reject", tags: datadog_tags)
          return GeyserResults.empty
        end
        return @results if defined?(@results)

        results = nil
        GitHub.dogstats.increment("search.query.total", tags: datadog_tags)
        GitHub.dogstats.time("search.query.time", tags: datadog_tags) do
          response = Geyser.search(user_query,
                                   [repo_id],
                                   current_user.id,
                                   lab_scoped: false, # routes to Geyser lab env if true; only prod queries here
                                   options: query_options)
          results = GeyserResults.new(response, page: page,
                                                per_page: per_page,
                                                max_offset: max_offset)

          # Transform the results to elastomer-like hashes
          results.results = transform(
            # Prune, hydrate, and validate
            prune_results(
              # Raw results from Geyser are a Google::Protobuf::RepeatedField;
              # force them to a vanilla array for the benfit of later ops.
              results.results.to_a
            )
          )

          # We don't need to capture "query" or "params" since this code path
          # never produces them
          instrument_execute(nil, nil, results)
          log_query("execute")

          # Normalize the transformed results
          results.results = normalize(results.results)
        end

        if results.error?
          GitHub.dogstats.increment("search.query.errors", tags: datadog_tags(error_code: results.error_code))
        else
          GitHub.dogstats.increment("search.query.success", tags: datadog_tags)
        end

        if results.assignment_not_found? && search_scope != :global
          emit_repair_pushed(repo_id)
        end

        @results = results
      rescue Search::Geyser::Client::RequestError, Search::Geyser::Client::TimeoutError => e
        GitHub.dogstats.increment("search.query.errors", tags: datadog_tags(exception: e))

        env = {
          page: page,
          repository_id: repo_id,
          request_id: request_id,
        }
        env[:app] = "github-user" unless e.is_a?(Search::Geyser::Client::TimeoutError)

        # Search::Geyser::Client::RequestError and Search::Geyser::Client::TimeoutError contain
        # no connection info. They are raised manually with no message in lib/search/geyser/client.rb
        Failbot.report(e, env)

        log_query("execute.error", e)

        error_code = e.class.name.demodulize.underscore
        GeyserResults.empty("geyser_#{error_code}", "Geyser service unreachable")
      end

      # Public: Perform a count query against the index and return the
      # number of matching documents.
      #
      # We do not currently have a separate count endpoint in Geyser, so
      # we'll use the normal search endpoint and return the total from the
      # meta. If we already executed the query we'll used the memoized results.
      # Otherwise we execute it now.
      #
      # Returns the number of matching documents.
      def count
        return 0 unless enabled?
        return 0 unless valid_query?

        unless defined?(@results)
          @query_type_count = true
          execute
          @query_type_count = false
        end
        @results.total
      end

      # Needed for backwards compatibility because Elastomer-based searchables
      # would call `count_with_timeout` on the elastomer index client
      alias :count_with_timeout :count

      # Public: Boolean flag used to enable or disable the query. When disabled
      # the execute method will not perform the query. Instead, the empty
      # GeyserResults object will be returned for all queries.
      #
      # Returns true when Geyser search is enabled.
      def enabled?
        GitHub.geyser_search_enabled?
      end

      # Internal: Take the array of code results and remove those for which
      # the repository no longer exists or has been flagged as spammy. We will
      # only perform this pruning in test and production; development mode is
      # for testing out everything.
      #
      # results - An array of Github::Geyser::Rpc::CodeResults from the results portion of the client response data
      #
      # Returns the pruned Array of results.
      def prune_results(results)
        prune!(results)
        validate!(results)
        results
      end

      # Internal: Take the array of code results and remove those for which
      # the repository no longer exists or has been flagged as spammy. We will
      # only perform this pruning in test and production; development mode is
      # for testing out everything.
      #
      # results - An array of Github::Geyser::Rpc::CodeResults from the results portion of the client response data
      #
      # Returns nil.
      def prune!(results)
        repositories = repositories_from_results(results)
        repairs = { DELETE: Set.new, OWNER: Set.new, VISIBILITY: Set.new }

        results.delete_if do |result|
          repository_id = result.repository_id
          next true if repairs[:DELETE].include?(repository_id) ||
              repairs[:OWNER].include?(repository_id) ||
              repairs[:VISIBILITY].include?(repository_id)

          repository = repositories[repository_id]
          if repository.nil?
            repairs[:DELETE] << repository_id
            true
          elsif !repository.geyser_indexing_enabled?
            repairs[:DELETE] << repository_id
            true
          elsif repository.owner_id != result.owner_id
            repairs[:OWNER] << repository_id
            true
          elsif repository.visibility.to_s.downcase != result.visibility.to_s.downcase
            repairs[:VISIBILITY] << repository_id
            true
          else
            false
          end
        end

        emit_repair_events(repairs, repositories)
      end

      # Internal: Validate that the user is allowed to see the given search results
      #
      # results - An array of Github::Geyser::Rpc::CodeResults from the results portion of the client response data
      #
      # Returns nil or raises a Search::Geyser::Response::SecurityViolation exception
      def validate!(results)
        repositories = repositories_from_results(results)

        requests = repositories.map do |_id, repository|
          {
            action: :search_repository,
            subject: repository,
            actor: current_user,
          }
        end
        response = ::Permissions::Enforcer.batch_authorize(requests: requests)

        requests.each do |request|
          unless response[request].decision.allow?
            repository = request[:subject]
            GitHub.dogstats.increment("search.query.errors.security", tags: datadog_tags)
            raise Search::Geyser::Response::SecurityViolation,
              "[#{repository.id}] '#{repository.nwo}' should not be viewable by the user"
          end
        end

        nil
      end

      # Internal: Transform the results to look like they came from Elastomer
      #
      # results - An array of Github::Geyser::Rpc::CodeResults from the results portion of the client response data
      #
      # Returns an Elastomer-like results Hash, hydrated with the repo model
      def transform(results)
        repositories = repositories_from_results(results)

        results.map do |result|
          repository_id = result.repository_id
          repository = repositories[repository_id]
          shim = Search::Geyser::Responses::Shims::Elastomer.new(result)
          shim.to_h.merge("_model" => repository)
        end
      end

      ## Instrumentation

      # Internal: Instruments a hydro event for search execution
      #
      # _query - Not used, but required to be defined for historical purposes
      # _params - Not used, but required to be defined for historical purposes
      # results - a Search::GeyserResults object from an executed query
      def instrument_execute(_query, _params, results)
        payload = {
          actor: current_user,
          query: user_query,
          search_type: ["code"],
          search_context: context,
          results: results.results_for_hydro,
          total_results: results.total,
          search_server_took_ms: results.time,
          search_server_timed_out: results.timed_out,
          max_score: 0, # Not applicable to Geyser CodeResults
          page_number: page || 1,
          per_page: per_page,
          es_query: nil, # Not applicable to Geyser CodeResults
          variant: dark_ship ? "geyser_dark_ship" : "geyser",
          # TODO: search_scope: <one of "standard" or "global">
        }
        payload[:originating_request_id] = originating_request_id if !originating_request_id.blank?

        GlobalInstrumenter.instrument "search.execute", payload
      end

      # Internal: populate the es_query child protobuf for the search.execute event
      #
      # _query - Not used, but required to be defined for historical purposes
      # _params - Not used, but required to be defined for historical purposes
      #
      # Only defined for Geyser because some integration tests for QueryHelper expect it.
      def instrument_query(_query, _params)
        nil
      end

      # Internal: Merge the default query params and the class-defined query params.
      #
      # Returns a Hash that will be passed as the URL params for the query.
      #
      # Only defined for Geyser because some integration tests for QueryHelper expect it.
      def build_query_params
        {}
      end

      # Calculates a cache key for this query's result set.
      #
      # Examples
      #
      #   cache_key
      #   #=> 'search/queries/code_query/<some_sha>'
      #
      # Returns a String identifying the query with its options.
      def cache_key
        sha = Digest::SHA1.new
        sha.update(user_query)
        sha.update(repo_id.to_s) if search_scope != :global
        sha.update(current_user.id.to_s)
        sha.update(query_options.except(:request_id).to_s)
        "#{self.class.name.underscore}:#{sha.hexdigest}:v#{search_content_cache_version}"
      end

      # Calculates a cache key for count queries.
      #
      # Examples
      #
      #   count_cache_key
      #   #=> 'search/queries/code_query/<some_sha>/count'
      #
      # Returns a String identifying the count query with its options.
      #
      # Since Geyser uses the same query for results and counts at the moment this returns the
      # same thing as cache_key
      def count_cache_key
        cache_key
      end

      # Calculates a cache key for count_with_timeout queries.
      #
      # Examples
      #
      #   count_with_timeout_cache_key
      #   #=> 'search/queries/code_query/<some_sha>/count_with_timeout'
      #
      # Returns a String identifying the count query with its options.
      #
      # Since Geyser uses the same query for results and counts at the moment this returns the
      # same thing as cache_key
      def count_with_timeout_cache_key
        cache_key
      end

      # Internal: returns the unparsed phrase provided to the query
      def user_query
        @user_query ||= Search::Serializers::Geyser::V1.serialize(transformed_query)
      end

      def parsed_query
        @parsed_query ||= Search::Parsers::Geyser::V1.parse(@raw_phrase)
      end

      # Unlike other searchables, Geyser is an intermediate search service
      # that abstracts away the underlying search engine. It therefore also
      # does its own query parsing. All we need to do is handle some basic
      # transformation of abstract qualifiers to real dotcom database IDs and
      # provide a list of authorized repositories for the user based on the
      # scope of the query.
      def transformed_query
        return @transformed_query if defined?(@transformed_query)
        @transformed_query = Search::Transformers::Geyser::V1.transform(parsed_query)
        transform_language!
        @transformed_query
      end

      # Internal: Helper method that will validate a query string.
      #
      # Returns true for a valid query; false for an invalid query.
      def valid_query?
        # In classic code search, queries are limited to 128 characters in
        # length, BUT that does not include qualifiers - only terms remaining
        # after qualifiers are separated out. Here we are only parsing _some_
        # qualifiers so we don't know the length of only default terms. Looking
        # at stats of typical query-length for repo scoped searches, the p99
        # is < 100 while the p99.5 > 1000. 500 is a moderate compromise.
        if @raw_phrase.length > 500
          @invalid_reason = "The search is longer than 500 characters."
          return false
        end

        # Fail fast if any of the parsed nodes could not be transformed
        invalid_node = transformed_query.detect { |node| !node.valid? }
        if invalid_node
          @invalid_reason = invalid_node.invalid_reason
          return false
        end

        true
      end

      private

      def query_options
        options = {
          dark_ship: dark_ship,
          page_number: page,
          page_size: per_page,
          request_id: request_id,
          search_scope: search_scope,
          version: :GITHUB_SYNTAX_V0,
        }
        options.merge(sorting_options)
      end

      # Sort preference can come from a URL param or as a qualifier
      def sorting_options
        if defined?(@sort) and @sort.present?
          # Sort option was passed in as a tuple
          {
            sort_type: sort_type(@sort[0]),
            sort_direction: sort_direction(@sort[1])
          }
        else
          # Check for a sort qualifier in the form of `sort:type-order`.
          # This is passive, if present we will leave the qualifier in place.
          # We only care about the first sort qualifier.
          sort_qualifier = @parsed_query.detect { |q| q.field?(:sort) }
          if sort_qualifier.present? && sort_qualifier.value =~ /\A\w+(-\w+)?\z/
            type, direction = sort_qualifier.value.split("-")
            # Accomodate `sort:type` qualfiers too by assuming desc order
            direction = "desc" if direction.nil?
            {
              sort_type: sort_type(type),
              sort_direction: sort_direction(direction)
            }
          else
            {}
          end
        end
      end

      def sort_type(type)
        case type.to_s.downcase
        when "indexed"
          :TimeIndexed
        when "score"
          :BestMatch
        else
          nil
        end
      end

      def sort_direction(direction)
        case direction.to_s.downcase
        when "desc"
          :DESC
        when "asc"
          :ASC
        else
          nil
        end
      end

      # Like sorting, a language filter can come from a URL param or as a
      # qualifier. Unlike sorting, we no longer use the deprecated language_id
      # protobuf field and instead rely on the grammar parser discovering it in
      # the user query which allows for language-only queries. While we try to
      # be as non-destructive with the user's original query as possible in
      # order to preserve the original intent, this is an exception to that
      # rule when a language filter is provided via URL param.
      def transform_language!
        # Any language filter provided via URL will have already been converted
        # to a Linguist::Language instance.
        return unless language.is_a?(Linguist::Language) && language.language_id

        # See if there are any language qualfiers, which after transformation
        # will be language_id filters.
        language_ids = @transformed_query.select { |q| q.field?(:language_id) }

        # See if the same language is already represented from a qualifier.
        return if language_ids.detect { |q| q.value == language.language_id }

        # If we've made it this far, add a new qualifier to represent the filter
        node = Search::Parsers::Geyser::V1::ParsedNode.new(:language_id,
                                                           language.language_id)
        @transformed_query << node
      end

      # Private: Maintains a list of repositories referenced by the result set. Fetches all
      # referenced repositories at once up front and then removes unused repositories as
      # results are pruned away.
      #
      # results - An array of result-like objects that respond to `:repository_id`
      #
      # Returns a hash of repositories for the results, indexed by ID
      def repositories_from_results(results)
        repository_ids = results.map(&:repository_id)
        @repositories_from_results ||= {}
        @repositories_from_results.delete_if { |id| !repository_ids.include?(id) }
        ids = repository_ids - @repositories_from_results.keys
        repositories = Repository.with_ids(ids).includes(:owner).index_by(&:id)
        @repositories_from_results.merge!(repositories)
      end

      def emit_repair_pushed(repository_id)
        repository = Repository.find_by(id: repository_id)
        return if repository.nil?

        payload = {
          change: :REPAIR_PUSHED,
          repository: repository,
          updated_at: Time.now.utc,
          ref: "refs/heads/#{repository.default_branch}",
        }
        GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
        GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:repair_pushed"])
      end

      def emit_repair_events(repairs, repositories)
        repairs[:DELETE].each do |repository_id|
          payload = {
            change: :REPAIR_DELETED,
            repository_id: repository_id,
          }
          GlobalInstrumenter.instrument("search_indexing.repository_deleted", payload)
          GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:repair_deleted"])
        end

        repairs[:OWNER].each do |repository_id|
          repository = repositories[repository_id]
          payload = {
              change: :REPAIR_OWNER,
              repository: repository,
              updated_at: Time.now.utc,
              ref: "refs/heads/#{repository.default_branch}",
          }
          GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
          GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:repair_owner"])
        end

        repairs[:VISIBILITY].each do |repository_id|
          repository = repositories[repository_id]
          payload = {
            change: :REPAIR_VISIBILITY,
            repository: repository,
            updated_at: Time.now.utc,
            ref: "refs/heads/#{repository.default_branch}",
          }
          GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
          GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:repair_visibility"])
        end

        nil
      end

      # Internal: Version for the memcache'd version of search content.
      # This is overridden for Geyser to keep separate from Elastomer cache version changes.
      #
      # Returns the cache version
      def search_content_cache_version
        1
      end

      # Internal: Return an Array of DataDog tags.
      #
      # exception - optional exception instance. Prefered over error_code if provided.
      # error_code - optional Twirp error_code from Geyser. See https://github.com/twitchtv/twirp/blob/master/docs/errors.md
      def datadog_tags(exception: nil, error_code: nil)
        tags = ["index:geyser_code_search", "dark_ship:#{dark_ship}", "search_scope:#{search_scope}"]
        if exception.present?
          tags << "error:#{stats_name(exception)}"
        elsif error_code.present?
          tags << "error:#{error_code}"
        end
        query_type = defined?(@query_type_count) && @query_type_count ? "count" : "search"
        tags << "query_type:#{query_type}"
        tags
      end
    end
  end
end
