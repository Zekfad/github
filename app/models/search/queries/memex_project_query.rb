# frozen_string_literal: true

module Search
  module Queries
    # This intentionally does not inherit from Search::Query because this query
    # is not executed by Elasticsearch, so we need different functionality than
    # usual from this class.
    class MemexProjectQuery
      attr_reader :filters, :full_text_query_terms, :raw_query

      def initialize(query, viewer: nil)
        # Use an OrderedHash to preserve user-entered order of filters in `stringify`.
        @filters = ActiveSupport::OrderedHash.new
        @full_text_query_terms = []
        @raw_query = if query.present?
          query.strip
        else
          self.class.default_query(viewer: viewer)
        end

        @raw_query.split(/\s+/).each do |token|
          if token.include?(":")
            qualifier, value = token.split(":", 2)
            qualifier = "creator" if qualifier == "author" # Alias/normalize a common alternative.
            @filters[qualifier.to_sym] = value
          else
            @full_text_query_terms << token
          end
        end
      end

      def self.default_query(viewer: nil)
        ["is:open", viewer && "creator:#{viewer}"].compact.join(" ")
      end

      def state_filter
        return @state_filter if defined?(@state_filter)
        @state_filter = if %w(open closed).include?(@filters[:is]&.downcase)
          @filters[:is].downcase
        else
          nil
        end
      end

      def creator_filter
        @filters[:creator]
      end

      def non_default_query?(viewer: nil)
        stringify != self.class.default_query(viewer: viewer)
      end

      def stringify
        return @stringified if defined?(@stringified)

        normalized_filters = @filters.reduce([]) do |memo, (qualifier, value)|
          memo << "#{qualifier}:#{value}"
        end
        @stringified = (normalized_filters + full_text_query_terms).join(" ").strip
      end
    end
  end
end
