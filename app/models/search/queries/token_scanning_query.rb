# frozen_string_literal: true

module Search
  module Queries
    class TokenScanningQuery < ::Search::Query
      FIELDS = [:type, :sort, :is].freeze
      UNIQ_FIELDS = [:sort, :is].freeze

      attr_reader :parsed, :raw

      def initialize(query:)
        @raw = query.to_s.strip
        @parsed = self.class.parse(@raw)
      end

      def stringify(type: nil, type_state: true, sort: nil, sort_state: true, resolved: false)
        new_query = parsed.dup
        new_query = toggle_qualifier(new_query, :type, type.to_s.downcase, type_state) if type.present?
        new_query = toggle_qualifier(new_query, :sort, sort, sort_state) if sort.present?
        new_query = toggle_qualifier(new_query, :is, "closed", resolved)
        self.class.stringify(new_query).presence
      end

      def types
        qualifier_values(:type)
      end

      def sort
        qualifier_values(:sort).first
      end

      def resolved?
        qualifier_values(:is).any?("closed")
      end

      private

      def qualifiers
        @qualifiers ||= parsed.select { |component| qualifier?(component) }
      end

      def qualifier_values(name)
        values = qualifiers.select do |component|
          component.first == name
        end
        values.map(&:second)
      end

      def qualifier?(component)
        # Select non-negated qualifiers such as qualifier:value
        component.is_a?(Array) && component.third != true
      end

      def matches_qualifier?(component, name, value)
        qualifier?(component) && component.first == name && value.casecmp?(component.second)
      end

      def toggle_qualifier(query, name, value, state)
        new_query = query.dup

        if state
          new_query << [name, value] unless new_query.include?([name, value])
        else
          new_query.reject! do |component|
            matches_qualifier?(component, name, value)
          end
        end

        new_query
      end
    end
  end
end
