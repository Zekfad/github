# frozen_string_literal: true

module Search
  module Filters
    # The NonMarketplaceListingFilter is critical for ensuring that users only see the
    # private Works with GitHub data that they are allowed to see. Any documents in
    # the search index that contain private data have two fields associated
    # with them: a `state` field and a `listing_id` field.
    #
    # Anyone is allowed to see documents where `state` is `approved`. For
    # everything else, the user needs to have permission to see the document.
    # This is determined by performing database queries and then returning a
    # list of private Works with GitHub listing IDs the user is allowed to see. This list is
    # used as a terms filter against the `listing_id` field.
    class NonMarketplaceListingFilter < ::Search::Filter
      attr_reader :current_user

      # Create a new NonMarketplaceListingFilter.
      #
      # opts - The options Hash
      #   :current_user - the currently logged in User
      #
      def initialize(opts = {})
        super(opts)
        @field = :_id
        @current_user = options.fetch(:current_user, nil)
        bool_collection  # initialize our boolean collection
      end

      # Returns nil or a filter Hash.
      def must
        if bool_collection.should?
          {
            bool: {
              should: [
                { term: { state: "approved" } },
                build_term_filter(field, bool_collection.should),
              ],
            },
          }
        else
          { term: { state: "approved" } }
        end
      end

      # Returns nil or a filter Hash.
      def must_not
        build_term_filter(field, bool_collection.must_not)
      end

      # A `should` filter is not applicable to the NonMarketplaceListing filter.
      #
      # Returns nil
      def should; end

      # This filter will never be blank.
      #
      # Returns false
      def blank?
        false
      end

      # Override the superclass method and make it into a noop.
      #
      # Returns nil
      def build(values); end

      # For validating search results, this filter provides the set of all
      # accessible NonMarketplaceListing IDs that were allowed by the search criteria.
      # Results can be validated against this set of NonMarketplaceListing IDs.
      #
      # Returns an array containing the accessible NonMarketplaceListing IDs.
      def accessible_listing_ids
        NonMarketplaceListing.adminable_by(current_user).pluck(:id)
      end

      # Internal: Create the boolean collection that will be used by the
      # NonMarketplaceListing filter. We include the current user's private
      # NonMarketplaceListing IDs.
      #
      # Returns this filter's boolean collection.
      def bool_collection
        return @bool_collection if defined? @bool_collection
        @bool_collection = ::Search::ParsedQuery::BoolCollection.new
        @bool_collection.should(accessible_listing_ids)
        @bool_collection
      end

      # Interal: Override the superclass method and make it a noop
      # implementation. Mapping the boolean collection is not applicable for
      # the NonMarketplaceListing filter.
      #
      # Returns this filter's boolean collection.
      def map_bool_collection
        bool_collection
      end
    end  # NonMarketplaceListingFilter
  end  # Filters
end  # Search
