# rubocop:disable Style/FrozenStringLiteralComment

module Search
  module Filters

    # The UserFilter is used to limit search results to a particular user or
    # collection of users. This class encapsulates the logic of looking up
    # users and generating ElasticSearch term filters.
    class UserFilter < TermFilter

      # Create a new UserFilter.
      #
      # opts - The options Hash
      #   :field - the document field containing the user IDs
      #
      def initialize(opts = {})
        super(opts)

        @field ||= :user_id

        map_bool_collection
      end

      # This reason can be used when the user filter is invalid.
      def invalid_reason
        "The listed users cannot be searched either because the users do not exist or you do not have permission to view the users."
      end

      # Internal: Take the boolean collection and convert all the values into
      # user IDs if possible.
      #
      # Returns this filter's boolean colleciton.
      def map_bool_collection
        return bool_collection if defined? @mapped
        @mapped = true

        # save these off for our validity check
        must_flag     = bool_collection.must?
        must_not_flag = bool_collection.must_not?

        # convert login strings to user IDs
        hash = logins
        bool_collection.map_all! do |login|
          login.is_a?(Symbol) ? login : hash[login.downcase]
        end

        bool_collection.uniq!

        # did we specify invalid user IDs?
        @valid = (must_flag     == bool_collection.must?) &&
                 (must_not_flag == bool_collection.must_not?)

        # prune `must_not` repository IDs from the `must` and `should` lists
        bool_collection.intersect!
        bool_collection
      end

      # Internal: Convert the `login` strings from the boolean collection into
      # user IDs. The logins will be downcased, and the returned hash will be
      # keyed by these downcased login strings.
      #
      # Returns a Hash mapping user logins to IDs.
      def logins
        logins = bool_collection.all
        return {} if logins.empty?

        ary = logins.map do |login|
          next if login.is_a?(Symbol)

          login = login.downcase

          # Convert `author:app/login` to `login[bot]` so we find Bot users in
          # the same query as other users.
          login.match(%r{\Aapp/(.+)}) do |m|
            login = "#{m[1]}#{Bot::LOGIN_SUFFIX}"
          end

          login
        end

        ary = User.select("id, login").where(login: ary.compact, spammy: false)

        ary.inject(Hash.new) do |hash, user|
          login = user.login.downcase
          login = Bot.query_filter_from_login(login) if login.end_with?(Bot::LOGIN_SUFFIX)
          hash[login.downcase] = user.id

          hash
        end
      end

    end  # UserFilter
  end  # Filters
end  # Search
