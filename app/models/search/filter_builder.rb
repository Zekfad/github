# rubocop:disable Style/FrozenStringLiteralComment

module Search

  # The FilterBuilder is a helper class that handles the grunt work of
  # transforming values stored in the qualifier terms Hash into actual, usable
  # Filter instances.
  class FilterBuilder
    attr_reader :qualifiers

    # Create a new FilterBuilder.
    #
    # qualifiers - The Hash of parsed qualifier terms.
    #
    def initialize(qualifiers)
      @qualifiers = qualifiers
    end

    # Build the positive and negative PrefixFilter instances.
    #
    # If the values stored in the filter field hashes are not in the correct
    # format, then a block can be given to this method. The values in the
    # filter field hashes will be passed to this block, and the returned
    # values from the block will be used in the generated filters.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def prefix_filter(field, *args, &block)
      build(Filters::PrefixFilter, field, *args, &block)
    end

    # Build the positive and negative HashFilter instances.
    #
    # If the values stored in the filter field hashes are not in the correct
    # format, then a block can be given to this method. The values in the
    # filter field hashes will be passed to this block, and the returned
    # values from the block will be used in the generated filters.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def sha_filter(field, *args, &block)
      build(Filters::ShaFilter, field, *args, &block)
    end

    # Build the positive and negative QueryFilter instances.
    #
    # If the values stored in the filter field hashes are not in the correct
    # format, then a block can be given to this method. The values in the
    # filter field hashes will be passed to this block, and the returned
    # values from the block will be used in the generated filters.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def query_filter(field, *args, &block)
      build(Filters::QueryFilter, field, *args, &block)
    end

    # Build the positive and negative RangeFilter instances.
    #
    # If the values stored in the filter field hashes are not in the correct
    # format, then a block can be given to this method. The values in the
    # filter field hashes will be passed to this block, and the returned
    # values from the block will be used in the generated filters.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def range_filter(field, *args, &block)
      build(Filters::RangeFilter, field, *args, &block)
    end

    # Build the positive and negative DateRangeFilter instances.
    #
    # If the values stored in the filter field hashes are not in the correct
    # format, then a block can be given to this method. The values in the
    # filter field hashes will be passed to this block, and the returned
    # values from the block will be used in the generated filters.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def date_range_filter(field, *args, &block)
      build(Filters::DateRangeFilter, field, *args, &block)
    end

    # Build the positive and negative TermFilter instances.
    #
    # If the values stored in the filter field hashes are not in the correct
    # format, then a block can be given to this method. The values in the
    # filter field hashes will be passed to this block, and the returned
    # values from the block will be used in the generated filters.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def term_filter(field, *args, &block)
      build(Filters::TermFilter, field, *args, &block)
    end

    # Build the positive and negative ActionFilter instances.
    #
    # If the values stored in the filter field hashes are not in the correct
    # format, then a block can be given to this method. The values in the
    # filter field hashes will be passed to this block, and the returned
    # values from the block will be used in the generated filters.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def action_filter(field, *args, &block)
      build(Filters::ActionFilter, field, *args, &block)
    end

    # Build a NonMarketplaceListingFilter.
    #
    # current_user - The current logged in User.
    #
    # Returns a NonMarketplaceListingFilter instance.
    def non_marketplace_listing_filter(current_user)
      Search::Filters::NonMarketplaceListingFilter.new(qualifiers: qualifiers,
                                                       current_user: current_user)
    end

    # Build a MarketplaceListingFilter. Includes Marketplace::Listing and NonMarketplaceListing IDs.
    #
    # current_user - The current logged in User.
    #
    # Returns a MarketplaceListingFilter instance.
    def marketplace_listing_filter(current_user)
      Search::Filters::MarketplaceListingFilter.new(qualifiers: qualifiers,
                                                    current_user: current_user)
    end

    # Build a RepositoryFilter.
    #
    # current_user - The current logged in User.
    # repo_id      - Restrict the filter to just this repository ID
    # resource     - Restrict the filter to repositories where the user has
    #                access to the specified resource.
    # ip           - A String representing the user's IP address from which the
    #                search originated.
    # field        - The name of the field in the index that holds the
    #                repository ID. Defaults to :repo_id.
    #
    # Returns a RepositoryFilter instance.
    def repository_filter(current_user, repo_id = nil, resource = nil, user_session: nil, ip: nil, field: :repo_id)
      Search::Filters::RepositoryFilter.new \
          qualifiers: qualifiers,
          current_user: current_user,
          user_session: user_session,
          ip: ip,
          repo_id: repo_id,
          resource: resource,
          field: field
    end

    # Build a RegistryPackagesFilter.
    #
    # current_user - The current logged in User.
    # repo_id      - Restrict the filter to just this repository ID
    # resource     - Restrict the filter to repositories where the user has
    #                access to the specified resource.
    #
    # Returns a RegistryPackagesFilter instance.
    def registry_packages_filter(current_user, owner_id = nil, repo_id = nil, resource = nil, user_session: nil)
      Search::Filters::RegistryPackagesFilter.new \
          qualifiers: qualifiers,
          current_user: current_user,
          user_session: user_session,
          owner_id: owner_id,
          repo_id: repo_id,
          resource: resource
    end

    # Build a ProjectOwnerFilter
    #
    # owner_id - Restrict the filter to just this owner ID
    # owner_type - The type of owner the ID maps to
    #
    #
    # Returns a ProjectOwnerFilter instance.
    def project_owner_filter(owner_id:, owner_type:)
      Search::Filters::ProjectOwnerFilter.new \
          qualifiers: qualifiers,
          owner_id: owner_id,
          owner_type: owner_type
    end

    # Build a ProjectFilter
    #
    # owner_id     - The ID of the org or user whose projects we're searching.
    # current_user - The current logged in User.
    #
    # Returns a ProjectFilter.
    def project_filter(owner_id, current_user)
      Search::Filters::ProjectFilter.new(
        qualifiers: qualifiers,
        owner_id: owner_id,
        current_user: current_user,
      )
    end

    # Build a ProjectLinkedRepositoryFilter
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         qualifiers hash
    #
    # Returns the UserFilter.
    def project_linked_repository_filter(field, *args)
      build(Filters::ProjectLinkedRepositoryFilter, field, *args)
    end

    # Build a StarSearchFilter
    #
    # current_user - The current logged in user
    #
    # Returns a StarSearchFilter instance
    def star_filter(current_user, star_user)
      Search::Filters::StarSearchFilter.new \
          qualifiers: qualifiers,
          current_user: current_user,
          star_user: star_user
    end

    # Build a GistFilter.
    #
    # current_user - The current logged in User.
    #
    # Returns a RepositoryFilter instance.
    def gist_filter(current_user)
      Search::Filters::GistFilter.new \
          qualifiers: qualifiers,
          current_user: current_user
    end

    # Build a UserFilter instance. The values from the positive and negative
    # filter field hashes should be user login Strings. These logins are used
    # to lookup the user in the database and retrieve the ID.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         qualifiers hash
    #
    # Returns the UserFilter.
    def user_filter(field, *args)
      build(Filters::UserFilter, field, *args)
    end

    # Build a Discussions::CategoryFilter instance. The values from the positive and
    # negative filter field hashes should be category name Strings. These names
    # are used to lookup the category in the database and retrieve the ID.
    #
    # field - The filter field name as a Symbol.
    # args  - An optional list of filter field names as found in the
    #         qualifiers hash
    #
    # Returns the Discussions::CategoryFilter.
    def discussions_category_filter(field, *args)
      build(Filters::Discussions::CategoryFilter, field, *args)
    end

    # Build a TeamFilter.
    #
    # current_user - The current logged in User.
    # field - The document field containing the team IDs.
    # args  - An optional list of filter field names as found in the
    #         qualifiers list.
    #
    # Returns a TeamFilter instance.
    def team_filter(current_user, field, *args)
      Search::Filters::TeamFilter.new \
          field: field,
          keys: args,
          qualifiers: qualifiers,
          current_user: current_user
    end

    def review_request_filter(field, *args)
      build(Filters::ReviewRequestFilter, field, *args)
    end
    # Build an InvolvesFilter instance. This filter type operates on multiple
    # fields using the same filter construct for each field. The values from
    # the positive and negative filter field hashes should be user login
    # Strings. These logins are used to lookup the user in the database and
    # retrieve the ID.
    #
    # fields - The Array of fields to filter on.
    # args   - An optional list of filter field names as found in the
    #          qualifiers hash
    #
    # Returns the InvolvesFilter.
    def involves_filter(fields, *args)
      build(Filters::InvolvesFilter, fields, *args)
    end

    # Create filters for language terms. The field name for the filter is
    # required. Also required is the method name to call on the
    # Linguist::Language for obtaining the correct language value to filter
    # on.
    #
    # field  - The filter field name as a Symbol.
    # method - The Linguist::Language method name as a Symbol.
    # args   - An optional list of filter field names as found in the
    #          positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def language_filter(field, method, *args)
      method = method.to_sym

      build(Filters::TermFilter, field, *args) do |language|
        case language
        when String
          alias_name = language.downcase.gsub(/\s/, "-")
          Linguist::Language[alias_name].try(method)
        when Linguist::Language
          language.try(method)
        end
      end
    end

    # Create filters for license terms. The field name for the filter is
    # required.
    #
    # field  - The filter field name as a Symbol.
    # args   - An optional list of filter field names as found in the
    #          positive/negative filter hashes.
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def license_filter(field, *args)
      Search::Filters::LicenseFilter.new(
          field: field,
          keys: args,
          qualifiers: qualifiers,
      )
    end

    # Create a filter for milestone titles and/or numbers.
    def milestone_filter(*args)
      Search::Filters::MilestoneFilter.new \
          keys: args,
          qualifiers: qualifiers
    end

    # Create a filter for issue project numbers
    def issue_project_filter(field, current_user:)
      Search::Filters::IssueProjectFilter.new(
        keys: [field],
        qualifiers: qualifiers,
        current_user: current_user,
      )
    end

    # Internal: Build the desired positive and negative Filter instances.
    # Required are the name of the filter field and the type of filter to
    # create.
    #
    # If the values stored in the filter field hashes are not in the correct
    # format, then a block can be given to this method. The values in the
    # filter field hashes will be passed to this block, and the returned
    # values from the block will be used in the generated filters.
    #
    # filter_class - The Filter class to instantiate.
    # field        - The filter field name as a Symbol.
    # args         - An optional list of filter field names as found in the
    #                positive/negative filter hashes.
    #
    # Examples
    #
    #   build( TermFilter, :label )
    #   #=> [positive_filter, negative_filter]
    #
    #   build( TermFilter, :user_id, :user ) { |login| User.find_by_login(login).try(:id) }
    #   #=> [positive_filter, negative_filter]
    #
    #   build( RangeFilter, :updated_at, :updated, :pushed )
    #   #=> [positive_filter, negative_filter]
    #
    # Returns a two element Array containing the positive Filter and the
    # negative Filter.
    def build(filter_class, field, *args, &block)
      opts = args.extract_options!

      opts[:field] = field
      opts[:keys]  = args
      opts[:qualifiers] = qualifiers

      filter = filter_class.new opts
      filter.map_bool_collection(&block)
      filter
    end

  end  # FilterBuilder
end  # Search
