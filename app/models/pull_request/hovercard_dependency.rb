# frozen_string_literal: true

module PullRequest::HovercardDependency
  extend ActiveSupport::Concern

  include UserHovercard::SubjectDefinition

  def user_hovercard_parent
    repository
  end

  def related_teams
    [
      *codeowners.teams,
      *review_requests.teams,
      *issue.comments.flat_map(&:mentioned_teams),
      *review_comments.flat_map(&:mentioned_teams),
    ]
  end

  included do
    define_user_hovercard_context :creator, ->(user, viewer, descendant_subjects:) do
      next unless issue && readable_by?(viewer)

      if user_id == user.id
        first_in = if user.pull_requests.minimum(:id) == self.id
          "(their first ever)"
        elsif user.pull_requests.for_organization(repository.organization_id).minimum(:id) == self.id
          "(their first in @#{repository.organization})"
        elsif user.pull_requests.for_repository(repository_id).minimum(:id) == self.id
          "(their first in #{repository.nwo})"
        end

        message = ["Opened this pull request", first_in].compact.join(" ")
        Hovercard::Contexts::Custom.new(message, "git-pull-request")
      end
    end

    define_user_hovercard_context :reviewer, ->(user, viewer, descendant_subjects:) do
      next unless readable_by?(viewer)

      reviews = visible_sidebar_reviews(viewer).select { |r| r.user == user }
      next if reviews.empty?

      visible_review_teams = Promise.all(
        reviews.map { |r| r.async_on_behalf_of_visible_teams_for(viewer) },
      ).sync.flatten.uniq

      UserHovercard::Contexts::PullRequestReviewer.new(teams: visible_review_teams)
    end

    define_user_hovercard_context :codeowner, ->(user, viewer, descendant_subjects:) do
      next unless readable_by?(viewer)

      owned_paths = codeowners.paths_for_owner(user)
      if owned_paths.any?
        Hovercard::Contexts::Custom.new("Code owner of #{owned_paths.count} #{"file".pluralize(owned_paths.count)} in this pull request", "shield-lock")
      end
    end

    define_user_hovercard_context :recency, ->(user, viewer, descendant_subjects:) do
      next unless readable_by?(viewer)

      max_age = 3.months
      suggested_reviewers = PullRequest::SuggestedReviewers.new(self, max_comment_age: max_age, max_commit_age: max_age).find(limit: nil)
      suggested_reviewer = suggested_reviewers.detect { |sr| sr.user == user }
      next unless suggested_reviewer

      Hovercard::Contexts::Custom.new(suggested_reviewer.description, "clock")
    end
  end
end
