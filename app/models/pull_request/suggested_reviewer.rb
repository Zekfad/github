# frozen_string_literal: true

class PullRequest
  class SuggestedReviewer
    include Comparable

    attr_reader :user, :pull, :author, :commenter

    def initialize(user:, pull:, author: 0.0, commenter: 0.0)
      @user = user
      @pull = pull
      @author = author
      @commenter = commenter
    end

    def author?
      @author > 0
    end

    def commenter?
      @commenter > 0
    end

    def merge(other)
      self.class.new(
        user: user,
        pull: pull,
        author: @author + other.author,
        commenter: @commenter + other.commenter,
      )
    end

    def score
      @author + @commenter
    end

    def description
      if author? && commenter?
        "Recently edited and reviewed changes to these files"
      elsif author?
        "Recently edited these files"
      else
        "Recently reviewed these files"
      end
    end

    def <=>(other)
      if other.is_a?(self.class)
        score <=> other.score
      end
    end
  end
end
