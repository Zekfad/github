# frozen_string_literal: true

module PullRequest::ChecksDependency
  # Internal: what are the matching CheckSuites for the given sha on this PR?
  #
  # sha  - The commit sha to filter CheckSuites by. Defaults to the last commit
  #        on this PR.
  #
  # Returns an ActiveRecord::Relation of CheckSuite objects.
  def matching_check_suites(head_sha: changed_commits.last.oid)
    repository.check_suites.where(head_sha: head_sha, hidden: false)
  end

  # The number of unique check runs for the matching check suites for this PR
  #
  # Returns an integer.
  def latest_check_runs_count
    return 0 unless changed_commits.last
    matching_check_suites.collect(&:latest_check_runs_count).sum
  end
end
