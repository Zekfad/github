# frozen_string_literal: true

class PullRequest
  class Prepare
    include MethodTiming

    def initialize(pull:, base_oid: pull.mergeable_base_sha, priority: :high, skip_rebase: false)
      @pull = pull
      @base_oid = base_oid
      @priority = priority
      @skip_rebase = skip_rebase
    end

    # Public: Run all operations that are required before this PR can be merged.
    #
    # This will prepare a preliminary merge commit and rebase commit. It will
    # also update the externally readable `merge` ref and the hidden rebase ref.
    #
    # A rebase will only be prepared if the merge commit creation was successful.
    #
    # Returns a 2-element array. The first element corresponds to the
    # `mergeability` flag. `true` signals the preparation was successful and the
    # PR can be merged and `false` means we encountered a merge conflict
    # or another error that prevented merge commit creation.
    # The second element is the merge commit sha, if the preparation was
    # successful, and `nil` otherwise.
    def perform(conflict_resolutions: nil)
      if (merge_commit = prepare_merge(conflict_resolutions: conflict_resolutions))
        rebase_commit_oid = prepare_rebase(merge_commit) unless @skip_rebase

        prepare_refs(merge_commit.oid, rebase_commit_oid)

        record_rebase_metrics(merge_commit, rebase_commit_oid) if rebase_commit_oid
      end

      return !!merge_commit, merge_commit.try(:oid)
    end
    time_method :perform, key: "pullrequest.merge.prepare"

    private

    attr_reader :pull, :base_oid, :priority

    def repository
      pull.repository
    end

    def record_rebase_metrics(merge_commit, rebase_commit_oid)
      return if merge_commit.nil? || rebase_commit_oid.nil?

      rebased_commit = repository.commits.find(rebase_commit_oid)
      return if rebased_commit.nil?

      if rebased_commit.tree_oid != merge_commit.tree_oid
        GitHub.dogstats.increment("pull_request", tags: ["action:merge_prepare_rebase_unsafe"])
      end
    rescue GitRPC::Error
    end

    def prepare_refs(merge_commit_oid, rebase_commit_oid)
      repository.batch_write_refs(pull.safe_user, [
        [pull.merge_ref, nil, merge_commit_oid],
        [pull.rebase_ref, nil, rebase_commit_oid || GitHub::NULL_OID]
      ], priority: priority, no_custom_hooks: true)
    end

    # Prepare a merge by creating a preliminary merge commit
    def prepare_merge(conflict_resolutions: nil)
      merge_commit, error, details = GitHub.dogstats.time("pull_request", tags: ["action:merge_commit_creation"]) do
        repository.commits.create_merge_commit(
          pull.safe_user,
          base_oid,
          pull.mergeable_head_sha,
          resolve_conflicts: conflict_resolutions,
        )
      end

      if details
        if error == :merge_conflict
          pull.store_conflicts(details)
        end
      end

      merge_commit
    end
    time_method :prepare_merge, key: "pullrequest.merge.prepare_merge"

    # Maximum time that we want to spend on preparing a rebase.
    REBASE_TIMEOUT = 7.seconds

    # Prepare a rebase for the given merge commit.
    # Returns the final oid for the sequence of commits that have been rebased, or nil if there was an issue.
    def prepare_rebase(merge_commit)
      safe_user = pull.safe_user

      committer = {
        name: safe_user.git_author_name,
        email: safe_user.git_author_email,
        time: safe_user.time_zone.now,
      }

      merge_base_sha, merge_head_sha = merge_commit.parent_oids

      begin
        repository.rpc.rebase(merge_head_sha, merge_base_sha, committer, timeout: REBASE_TIMEOUT)
      rescue GitRPC::Backend::RebaseTimeout, GitRPC::GitmonClient::AbortError, GitRPC::NetworkError
        GitHub.dogstats.increment("pull_request", tags: ["action:merge_prepare_rebase_timeout"])
        nil
      rescue GitRPC::Protocol::DGit::ResponseError => e
        # Treat split timeouts--some nodes timed out, others succeeded--as
        # equivalent to a unanimous timeout, rather than propagating a
        # messy ResponseError.
        if e.errors.all? { |route, error|
             error.is_a?(GitRPC::Backend::RebaseTimeout) ||
               error.is_a?(GitRPC::GitmonClient::AbortError) ||
               error.is_a?(GitRPC::Timeout) ||
               error.is_a?(BERTRPC::ProtocolError) ||
               !route.voting?
           }
          GitHub.dogstats.increment("pull_request", tags: ["action:merge_prepare_rebase_timeout"])
          nil
        else
          raise
        end
      end
    end
    time_method :prepare_rebase, key: "pullrequest.merge.prepare_rebase"
  end
end
