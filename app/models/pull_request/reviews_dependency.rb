# rubocop:disable Style/FrozenStringLiteralComment

module PullRequest::ReviewsDependency
  def latest_reviews_for(viewer)
    reviews.for_viewer(viewer).filter_spam_for(viewer).order(:submitted_at, :created_at)
  end

  # When creating a new review we skip touching the pull request. This is done to ensure we don't
  # trigger UI updates on the pull request before we are able to modify the newly created pending review.
  def pending_review_for(user:, head_sha: nil)
    head_sha ||= self.head_sha

    existing_review = reviews.with_pending_state.where(user_id: user.id).first
    existing_review || GitHub::SchemaDomain.allowing_cross_domain_transactions do
      PullRequest.no_touching { reviews.with_pending_state.create(user_id: user.id, head_sha: head_sha) }
    end
  end

  def async_status_at_merge
    return Promise.resolve(nil) unless merged?

    async_repository.then do |repo|
      commit_statuses = Statuses::Service.statuses_at_merge(repo_id: repository.id, sha: head_sha, merged_at: merged_at)
      check_runs = CheckRun.check_runs_at_merge(repository_id: repository.id, sha: head_sha, merged_at: merged_at)
      if commit_statuses.any? || check_runs.any?
        ::CombinedStatus.new(repo, head_sha, statuses: commit_statuses, check_runs: check_runs)
      end
    end
  end

  def async_pending_review_by?(user)
    return Promise.resolve(false) unless user

    Platform::Loaders::PendingPullRequestReviewCheck.load(id, user.id)
  end

  def pending_review_by?(user)
    latest_pending_review_for(user).present?
  end

  def latest_pending_review_for(user)
    @latest_pending_review_for ||= Hash.new do |h, k|
      h[k] = reviews.where(
               user_id: k.id,
               state: PullRequestReview.state_value(:pending),
             ).first
    end
    @latest_pending_review_for[user]
  end

  def latest_non_pending_review_for(user)
    reviews.where(
      user_id: user.id,
      state: [
        PullRequestReview.state_value(:approved),
        PullRequestReview.state_value(:commented),
        PullRequestReview.state_value(:changes_requested),
        PullRequestReview.state_value(:dismissed),
      ],
    ).order("created_at desc").first
  end

  def latest_enforced_review_for(user)
    return unless user

    reviews = latest_enforced_reviews(writers_only: false).select do |review|
      review.user_id == user.id
    end

    reviews.max_by(&:created_at)
  end

  # Public: Finds enforcable reviews that should be considered stale. A review
  # is only considered stale if:
  #
  # - the PR is still open
  # - 'dismiss stale reviews' has been enabled for the base branch
  # - the review is an approval
  # - the review was made for an older version
  #
  # Returns an Array of PullRequestReviews
  def stale_enforced_reviews(base_changed:)
    return [] unless open?
    return [] unless protected_base_branch&.dismiss_stale_reviews_on_push?

    latest_enforced_reviews(writers_only: false).select do |review|
      review.approved? && (base_changed || review.head_sha != self.head_sha)
    end
  end

  def stale_file_reviews(before:, after:)
    return [] unless open?
    return [] unless user_reviewed_files.not_dismissed.any?

    changed_paths = current_pull_comparison(before: before, after: after).diffs.summary.deltas.map(&:path).uniq
    all_paths = changed_paths_for_codeowners
    return [] if codeowners_paths_load_error?

    stale_file_reviews = []

    user_reviewed_files.not_dismissed.group_by(&:filepath).each do |path, reviewed_files|
      if changed_paths.include?(path) || !all_paths.include?(path)
        stale_file_reviews.push(*reviewed_files)
      end
    end

    stale_file_reviews
  end

  # Public: Return the commit OIDs / head SHAs for any commit the user
  #         submitted a review on for the given pull request.
  #
  # user - The user who submitted reviews.
  # candidate_oids - Optional. If present, limits the returned OIDs to be
  #                  from the given list of OIDs.
  #
  # Returns and Array of OID strings.
  def reviewed_commit_oids(user:, candidate_oids: nil, limit: 200)
    return [] unless user
    return [] if candidate_oids&.empty?
    arel = reviews.submitted.
      where(user_id: user.id).
      select("DISTINCT head_sha").
      limit(limit)
    arel = arel.where(head_sha: candidate_oids) if candidate_oids&.any?
    arel.pluck(:head_sha)
  end

  # Public: Returns a list of the most recent review that each user has left,
  # as long as the review's status is approved or rejected.
  #
  # Includes dismissed in the query so that we can filter those out after.
  # This is so that we consider a most recent review which is dismissed
  # to be essentially ignored in the merge area.
  #
  # writers_only - Boolean indicating whether only consider reviews of users with
  #                write access to the pull request's base repository
  #
  # Returns Array of PullRequestReviews
  def latest_enforced_reviews(writers_only:)
    return @latest_enforced_reviews[writers_only] if defined?(@latest_enforced_reviews)

    @latest_enforced_reviews = Hash.new do |hash, key|
      reviews = PullRequestReview::EnforcedLoader.new(
        repository: repository,
        pull_request: self,
        writers_only: key,
      ).execute

      hash[key] = reviews
    end

    @latest_enforced_reviews[writers_only]
  end

  def latest_enforced_reviews_count_for(review_state)
    latest_enforced_reviews(writers_only: true).count do |review|
      review.state == PullRequestReview.state_value(review_state)
    end
  end

  # This is used for indexing in search
  #
  # Returns Strings describing the current state of the review policy in the merge box
  def review_merge_states(viewer: nil)
    policy_decision = cached_merge_state(viewer: viewer).pull_request_review_policy_decision
    states = []

    if policy_decision.changes_requested?
      states << "changes_requested"
    elsif policy_decision.approved?
      states << "approved"
    else
      states << "required" if policy_decision.more_reviews_required?
      states << "none" unless policy_decision.has_reviews?
    end

    states
  end

  # Internal: Dismisses any currently approved reviews which no longer include the
  # latest changes.
  #
  # actor   - The User who performed the push or merge which triggered the dismissal.
  #
  # Returns nothing.
  def dismiss_stale_reviews(actor:, base_changed:)
    reason = base_changed ? { message: "The base branch was changed." } : { via_commit_oid: head_sha }
    stale_enforced_reviews(base_changed: base_changed).each do |review|
      review.dismiss!(actor, **reason)
    end
  end

  def dismiss_file_reviews(before:, after:, actor:)
    reviewed_files = user_reviewed_files.where(id: stale_file_reviews(before: before, after: after).map(&:id))
    reviewed_files.update_all(dismissed: true)

    reviewed_files.each do |reviewed_file|
      GlobalInstrumenter.instrument("pull_request_file.dismissed", {
        repository: repository,
        pull_request: self,
        actor: actor,
        file_path: reviewed_file.filepath,
        action: "DISMISSED",
      })
    end
  end

  # Internal: Determines if we should dismiss stale reviews during synchronize!.
  # We only want to do that if the base protected branch is configured for it and
  # there are changes which we can show in the diff.
  #
  # before        - The commit OID for the comparison before the changes.
  # after         - The commit OID for the comparison after the changes.
  # base_changed  - (Optional) A Boolean indicating if the base branch for the PR was changed.
  #
  # Returns a boolean.
  def dismiss_stale_reviews?(before:, after:, base_changed: false)
    stale_enforced_reviews(base_changed: base_changed).any? && (base_changed || reviewable_changes?(before: before, after: after))
  end

  def dismiss_file_reviews?(before:, after:, base_changed: false)
    stale_file_reviews(before: before, after: after).any? && (base_changed || reviewable_changes?(before: before, after: after))
  end

  # Internal: Determines if there are any reviewable changes between two commit OIDs.
  # Changes are only reviewable if we can produce a diff. For example, empty commits do
  # not constitute reviewable changes. Neither does merging the base branch back into
  # the head branch.
  #
  # before  - The commit OID for the comparison before the changes.
  # after   - The commit OID for the comparison after the changes.
  #
  # Returns a boolean.
  def reviewable_changes?(before:, after:)
    return false if before == after

    # Consider force pushes reviewable changes
    return true unless compare_repository.rpc.descendant_of?(after, before)

    !current_pull_comparison(before: before, after: after)&.diffs.empty?
  end

  def approved_with_no_changes_requested?
    policy_decision = merge_state.pull_request_review_policy_decision
    policy_decision.approved? && !policy_decision.changes_requested?
  end

  # Internal: Returns the users who have submitted a review for a pull request.
  #
  # Returns an ActiveRecord::Relation of Users.
  def submitted_reviewers
    User.where(id: reviews.submitted.pluck(:user_id).uniq)
  end

  private def current_pull_comparison(before:, after:)
    merge_base = compare_repository.best_merge_base base_sha, after

    @current_pull_comparison ||= PullRequest::Comparison.find \
      pull: self,
      start_commit_oid: before,
      end_commit_oid: after,
      base_commit_oid: merge_base
  end
end
