# rubocop:disable Style/FrozenStringLiteralComment

module PullRequest::FindersDependency
  extend ActiveSupport::Concern

  included do
    # Public: Filter to include only open PullRequests
    #
    # Examples
    #
    #   PullRequest.open.all
    #   # => [ <...list of open PullRequest objects ...> ]
    #
    # This is a named scope.
    scope :open_pulls, -> { includes(:issue).joins(:issue).where(issues: {state: "open"}) }

    # Public: Filter to include only draft PullRequests
    scope :drafts, -> { where(work_in_progress: true) }

    # Public: Filter to include only closed PullRequests
    #
    # Examples
    #
    #   PullRequest.closed.all
    #   # => [ <...list of closed PullRequest objects ...> ]
    #
    # This is a named scope.
    scope :closed_pulls, -> { includes(:issue).where(issues: {state: "closed"}) }

    # Public: Filter PullRequests by head repository and ref.
    #
    # repo - Repository instance for the head repository
    # ref  - Head ref for the pull request
    #
    # Examples
    #
    #   PullRequest.for_head_repo_and_head_ref(@repo, "new-feature").all
    #   # => [ <...list of PullRequest objects from the repo with the head ref ...> ]
    #
    # This is a named scope.
    scope :for_head_repo_and_head_ref, -> (repo, ref) {
      where(head_repository: repo, head_ref: Git::Ref.permutations(ref))
    }

    # Public: Filter PullRequests by base ref.
    #
    # ref  - Base ref for the pull request
    #
    # Examples
    #
    #   PullRequest.for_base_ref("develop").all
    #   # => [ <...list of PullRequest objects with the given base ref ...> ]
    #
    # This is a named scope.
    scope :for_base_ref, -> (ref) {
      where(base_ref: Git::Ref.permutations(ref))
    }

    # Public: Filter and order PullRequests by creation timestamp.
    #
    # direction - Optional direction for sort ('asc' or 'desc'), defaults to 'asc'
    #
    # Examples
    #
    #   PullRequest.by_creation.all
    #   # => [ <...list of PullRequest objects, ordered by decreasing creation timestamp...> ]
    #
    #   PullRequest.by_creation('asc').all
    #   # => [ <...list of PullRequest objects, ordered by increasing creation timestamp...> ]
    #
    # This is a named scope.
    scope :by_creation, -> (*direction) {
      which = direction.empty? ? "asc" : direction.first
      order("pull_requests.created_at" << (which == "asc" ? " ASC" : " DESC"))
    }

    # Public: Filter and order PullRequests by update timestamp.
    #
    # direction - Optional direction for sort ('asc' or 'desc'), defaults to 'asc'
    #
    # Examples
    #
    #   PullRequest.by_updates.all
    #   # => [ <...list of PullRequest objects, ordered by decreasing update timestamp...> ]
    #
    #   PullRequest.by_updates('asc').all
    #   # => [ <...list of PullRequest objects, ordered by increasing update timestamp...> ]
    #
    # This is a named scope.
    scope :by_updates, -> (*direction) {
      which = direction.empty? ? "asc" : direction.first
      order("pull_requests.updated_at" << (which == "asc" ? " ASC" : " DESC"))
    }

    # Public: Filter and order PullRequests by popularity, which is the number of
    # comments made on the issue associated with the PullRequest.
    #
    # direction - Optional direction for sort ('asc' or 'desc'), defaults to 'asc'
    #
    # Examples
    #
    #   PullRequest.by_popularity.all
    #   # => [ <...list of PullRequest objects, ordered by decreasing popularity...> ]
    #
    #   PullRequest.by_popularity('asc').all
    #   # => [ <...list of PullRequest objects, ordered by increasing popularity...> ]
    #
    # This is a named scope.
    scope :by_popularity, -> (*direction) {
      which = direction.empty? ? "asc" : direction.first
      joins(:issue).order("issues.issue_comments_count" << (which == "asc" ? " ASC" : " DESC"))
    }

    # Public: Filter and order PullRequests by longevity.  Longevity is the duration
    # between creation and last update.
    #
    # direction - Optional direction for sort ('asc' or 'desc'), defaults to 'asc'
    #
    # Examples
    #
    #   PullRequest.by_longevity.all
    #   # => [ <...list of PullRequest objects, ordered by decreasing longevity...> ]
    #
    #   PullRequest.by_longevity('asc').all
    #   # => [ <...list of PullRequest objects, ordered by increasing longevity...> ]
    #
    # This is a named scope.
    scope :by_longevity, -> (*direction) {
      which = direction.empty? ? "asc" : direction.first
      order(Arel.sql("DATEDIFF(pull_requests.updated_at, pull_requests.created_at)" << (which == "asc" ? " ASC" : " DESC")))
    }

    # Public: Filter PullRequest results by excluding certain states
    #
    # exclude - Optional comma-separated list of PullRequest#state values to exclude
    #
    # Examples
    #
    #   PullRequest.excluding_states('open').all
    #   # => [ <...list of PullRequest objects, none of which are open...> ]
    #
    #   PullRequest.excluding_states('open,closed').all
    #   # => [ <...list of PullRequest objects, without any open or closed PullRequests...>]
    #
    # This is a named scope.
    scope :excluding_states, -> (*exclude) {
      which = exclude.first.to_s.split(",")
      exclusions = which.any? ? which.compact : ["closed"]

      conditions = []
      conditions.push("issues.state = 'closed'") if exclusions.include?("open")
      conditions.push("issues.state = 'open'") if exclusions.include?("closed")

      # slow query performance patch https://github.com/github/github/issues/80649#issuecomment-362265661
      joins(:issue).where("`issues`.`repository_id` = `pull_requests`.`repository_id`").
        where(conditions.join(" AND "))
    }
  end

  class_methods do
    # Public: Skip PullRequest results by an offset, limit result set size
    #
    # options    - optional Hash of arguments
    #              :offset   - Optional number of results to skip, defaults to 0
    #              :per_page - Optional number of results to return, defaults to 100
    #
    # Examples
    #
    #   PullRequest.offset(:offset => 1). all
    #   # => [ <...list of PullRequest objects, result numbers 2-101> ]
    #
    #   PullRequest.offset(). all
    #   # => [ <...list of PullRequest objects, result numbers >= 1-100> ]
    #
    #   PullRequest.offset(:offset => 100, :per_page => 20). all
    #   # => [ <...list of PullRequest objects, result numbers >= 101-120> ]
    #
    # Converted from a named scope because `offset` is defined in Rails and
    # raises an error because of conflicting scopes on Rails 4.1.
    def offset(*args)
      params   = args.shift || {}
      offset_v = params[:offset].to_i
      per_page = params[:per_page].to_i

      offset(offset_v).limit((per_page > 0) ? per_page : 100)
    end

    # Public: return a list of filtered and ordered PullRequests matching the passed
    # option criteria.
    #
    # options - optional hash of parameters which will specify how the PullRequests are
    #           to be ordered and filtered (default: {}):
    #           :sort      - which sorting type to use (optional); valid values include
    #                        'created', 'updated', 'popularity', and 'long-running'; if
    #                        no :sort value is specified, 'created' is assumed, with a
    #                        :direction value of 'desc'
    #           :direction - should results be ordered in ascending or descending
    #                        order? (optional)  Valid values are 'asc' and 'desc',
    #                        defaults to 'asc'.
    #           :state     - Scope to 'open' or 'closed' state. (defaults to nil)
    #           :exclude   - Comma-separated list of PullRequest issue states to
    #                        exclude, defaults to 'closed'.
    #
    # Examples
    #
    #   PullRequest.filtered_and_ordered
    #   # => [ <...list of PullRequest objects ...> ]
    #
    #   PullRequest.filtered_and_ordered(:sort => 'long-running', 'direction' => 'asc')
    #   # => [ <...list of PullRequest objects ...> ]
    #
    #   PullRequest.filtered_and_ordered(:state => 'open')
    #   # => [ <...list of PullRequest objects ...> ]
    def filtered_and_ordered(options = {})
      if (sort = options[:sort]).blank?
        sort = "created"
        direction = options[:direction] || "desc"
      end

      direction ||= options[:direction] || "asc"
      exclude     = options[:exclude]   || "closed"

      case options[:state]
      when "open"
        exclude = "closed"
      when /close/
        exclude = "open"
      when "all"
        exclude = nil
      end

      pulls = scoped
      pulls = pulls.excluding_states(exclude) if exclude

      case sort
      when "updated"
        pulls.by_updates(direction)
      when "popularity"
        pulls.by_popularity(direction)
      when "long-running", "longevity"
        pulls.by_longevity(direction)
      else
        # default is descending order by creation
        pulls.by_creation(direction)
      end
    end

    # Load issue + review comment count for each pull request given and set on the
    # pull request's total_comments attribute.
    #
    # pulls - Array of PullRequest objects.
    #
    # Returns pulls.
    def attach_comment_counts(pulls)
      counts = {}
      find_comment_counts(pulls).each { |id, count| counts[id] = count }
      pulls.each { |pull| pull.total_comments = counts[pull.id] || 0 }
    end

    # Retrieve statuses and set for each pull request
    def attach_statuses(repo, pulls)
      statuses = PullRequest::MergeStatus.merge_statuses_for_pulls(repo, pulls)
      pulls.each do |pull|
        pull_status = statuses[pull.head_sha]
        next unless pull_status
        pull.combined_status = pull_status
      end
    end

    def find_comment_counts(pulls)
      return [] if pulls.empty?

      sql = github_sql.new(<<-SQL, pull_ids: pulls.map(&:id))
        SELECT pull_requests.id,
               COUNT(DISTINCT issue_comments.id) + COUNT(DISTINCT pull_request_review_comments.id)
          FROM pull_requests
          JOIN issues ON issues.pull_request_id = pull_requests.id
     LEFT JOIN issue_comments ON issue_comments.issue_id = issues.id
     LEFT JOIN pull_request_review_comments ON pull_request_review_comments.pull_request_id = pull_requests.id
         WHERE pull_requests.id in :pull_ids
         GROUP BY pull_requests.id
      SQL
      sql.results
    end

    # Returns a Pull Request by number and repository, or nil if none found
    #
    # Options currently just supports :include to do eager loading (only the :issue
    # is eager loaded by default)
    def with_number_and_repo(number, repo, options = {})
      includes = options[:include] || :issue
      includes(includes).
        joins(:issue).
        readonly(false). # :joins would cause the returned record to be readonly w/o this
        where("issues.repository_id = ? and issues.number = ?", repo.id, number).
        first
    end
  end
end
