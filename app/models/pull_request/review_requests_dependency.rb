# rubocop:disable Style/FrozenStringLiteralComment

module PullRequest::ReviewRequestsDependency
  extend ActiveSupport::Concern

  MAX_MANUAL_REVIEW_REQUESTS = 15
  MAX_REVIEW_REQUESTS_FROM_CODE_OWNERS = 100

  # Public: Return the review request limit from the repo's plan configuration
  #
  # Returns an integer
  def manual_review_requests_limit
    @review_requests_limit ||= if plan_manual_review_requests_limit_enabled?
      repository.plan_limit(:manual_review_requests)
    else
      MAX_MANUAL_REVIEW_REQUESTS
    end
  end

  def single_manual_review_request?
    manual_review_requests_limit == 1
  end

  # Public: Get a promise that returns the status of this pull request with respect to code review.
  #
  # viewer - a User or nil
  #
  # Returns a promise.
  def async_review_decision(viewer:)
    promises = [async_base_user, async_head_user, async_base_repository, async_user]

    Promise.all(promises).then do
      base_repository.async_internal_repository.then do
        policy_decision = cached_merge_state(viewer: viewer).pull_request_review_policy_decision

        if policy_decision.changes_requested?
          :changes_requested
        elsif policy_decision.approved?
          :approved
        elsif policy_decision.more_reviews_required?

          if protected_base_branch&.pull_request_reviews_required?
            :review_required
          end
        end
      end
    end
  end

  # Public: Get the status of this pull request with respect to code review.
  #
  # viewer - a User or nil
  #
  # Returns :changes_requested, :approved, :review_required, or nil.
  def review_decision(viewer:)
    async_review_decision(viewer: viewer).sync
  end

  # Internal: Create an review_requested event for a new reviewer.
  #
  # reviewer - The User who was asked to reviewer
  # actor - Optional. The User who made the request.
  #
  # Returns nothing
  def trigger_review_requested_event(review_request, actor: nil)
    issue.ignore_duplicate_records do
      events.create(event: "review_requested", actor_id: actor&.id || issue.modifying_user.id, subject: review_request.reviewer, review_request_id: review_request.id)
    end
  end

  # Internal: Create an review_request_removed event when a reviewer is removed unless the remover was a review creation.
  #
  # reviewer - The User whose request was removed
  # actor - Optional. The User who made the request.
  #
  # Returns nothing
  def trigger_review_request_removed_event(reviewer, actor: nil)
    issue.ignore_duplicate_records do
      events.create(event: "review_request_removed", actor_id: actor&.id || issue.modifying_user.id, subject: reviewer)
    end
  end

  # Public: get the request for this user or team on this review
  #
  # Returns a ReviewRequest or nil if none exist for the given actor
  def direct_review_request_for(actor)
    pending_review_requests_by_reviewer[actor]
  end

  # Internal: returns a cached mapping of reviewers to their review requests
  def pending_review_requests_by_reviewer
    return @pending_review_requests_by_reviewer if defined?(@pending_review_requests_by_reviewer)

    @pending_review_requests_by_reviewer = pending_review_requests.to_a.index_by(&:reviewer)
  end

  # Public: Is review request from this user or team on this pull_request
  #
  # actor - a user or team
  # requests - an optional list of review requests to filter through
  # teams - an optional list of teams to consider; will be calculated from the pending team review
  #         requests on this pull request if omitted
  #
  # Returns a Boolean
  def review_requested_for?(actor, requests: nil, teams: nil)
    review_requests_for(actor, requests: requests, teams: teams).any?
  end

  # Public: What teams (that this user is a member of) have received review
  # requests for this pull request that have been fulfilled
  #
  # Returns a collection of ReviewRequests
  def team_requests_on_behalf_of(user)
    review_requests.fulfilled
      .type_teams
      .order("id DESC")
      .select { |request|
        Team.member_of?(request.reviewer_id, user.id, immediate_only: false)
      }
      .uniq(&:reviewer_id)
  end

  # Public: Are there any pending review requests for teams that the viewer
  # is a member of
  #
  # Returns a Boolean
  def can_fulfill_a_pending_team_review_request?(user)
    # any? will make sure we don't call `Team.member_of?` more than we need to
    review_requests.pending.type_teams.any? do |request|
      Team.member_of?(request.reviewer_id, user.id, immediate_only: false)
    end
  end

  # Public: Returns the pending review requests for this pull request.
  def pending_review_requests
    return review_requests if new_record?

    review_requests.pending.preload(:reviewer)
  end

  # Public: get the requests for this actor on this review
  #
  # actor - a user or team
  # requests - an optional list of review requests to filter through
  # teams - an optional list of teams to consider; will be calculated from the pending team review
  #         requests on this pull request if omitted
  #
  # Returns a list of ReviewRequests
  def review_requests_for(actor, requests: nil, teams: nil)
    return [] if actor.nil?
    return [] if self.user == actor

    requests ||= pending_review_requests
    teams ||= requests.teams
    actors = [actor]

    teams.each do |team|
      if Team.member_of?(team.id, actor.id, immediate_only: false)
        actors.push(team)
      end
    end

    requests.select { |request| actors.include?(request.reviewer) }
  end

  def review_request_removable?(review_request)
    return true unless protected_base_branch&.require_code_owner_review?

    # If code owner reviews are enforced, requests to code owners cannot be
    # removed, unless the person requested is no longer a CODEOWNER (i.e. those
    # changes were removed)

    # Return early if this wasn't a codeowners request.
    return true if review_request.reasons.none?(&:codeowners?)

    # If the codeowners object isn't initialized with this pull's
    # changed paths successfully it will appear to fail open, so
    # verify that diff paths were properly loaded after checking
    # that the reviewer is a codeowner.
    !codeowners.include?(review_request.reviewer) && !codeowners_paths_load_error?
  end

  # Public: Set the full list of reviewers for this pr.
  #
  # reviewers - The full Array of Users and Teams to request review from.
  #             Removes any existing requests unless the reviewer is passed.
  # actor - The User setting the review requests
  # limit - Total number of allowed reviewers to request.
  # should_save - Whether to save the parent record immediately. Default: true.
  # re_request - Is this a re-request of an existing reviewer. Default: false.
  # via_delegation - Is this the result of Team review delegation. If so we'll
  #                  trust that the actor is allowed. The actor may not be a
  #                  repository collaborator if they came via a fork pull and
  #                  CODEOWNERS. Default: false.
  def request_review_from(actor:, reviewers: [], limit: manual_review_requests_limit, should_save: true, re_request: false, via_delegation: false, append: false)
    return unless via_delegation ||
      can_request_review?(actor) ||
      (re_request && can_re_request_review?(actor))

    allowed_reviewers =
      if via_delegation || can_request_team_review?(actor)
        reviewers
      else
        reviewers.reject { |reviewer| reviewer.is_a?(Team) }
      end

    pending_reviewers = allowed_reviewers.to_a.first(limit)

    if append
      review_requests.pending_reviewers = review_requests.pending_reviewers + pending_reviewers
    else
      review_requests.pending_reviewers = pending_reviewers
    end

    GitHub::SchemaDomain.allowing_cross_domain_transactions { save } if should_save
  end

  # Public: Returns a list of the most recent review that each user has left,
  # as long as the review's status is not pending.
  #
  # Returns Array of PullRequestReviews
  def latest_reviews_not_requested
    # !user to protect against ghost users
    return [] if new_record? || !user
    @latest_reviews_not_requested ||= begin
      # Group-wise maximum against id column rather than updated_at because
      # http://bugs.mysql.com/bug.php?id=54784
      sql = <<-SQL
        SELECT r1.*
        FROM pull_request_reviews r1
        INNER JOIN
        (
          SELECT max(id) as id
          FROM pull_request_reviews
          WHERE pull_request_id = :pull_request_id
          AND state <> :state
          AND user_id <> :pull_user_id
          AND user_id NOT IN (
            SELECT review_requests.reviewer_id
            FROM review_requests
            LEFT OUTER JOIN pull_request_reviews_review_requests on pull_request_reviews_review_requests.review_request_id = review_requests.id
            WHERE review_requests.pull_request_id = :pull_request_id
              AND review_requests.reviewer_type = :type
              AND review_requests.dismissed_at IS NULL
              AND pull_request_reviews_review_requests.review_request_id IS NULL
          )
          GROUP BY user_id
        ) r2
        ON r1.id = r2.id
        ORDER BY r1.state ASC
      SQL

      reviews = PullRequestReview.github_sql.new(sql,
        pull_request_id: id,
        pull_user_id: user.id,
        type: "User",
        state: PullRequestReview.state_value(:pending),
      ).models(PullRequestReview)

      reviews.each { |record| record.association(:pull_request).target = self }
      reviews
    end
  end

  class_methods do
    # Public: Returns a count per PR of non-stale fulfilled reviews unique by user
    #
    # Returns Hash of PR id's pointing to the count of fulfilled reviews
    # for example,  {12 => 3, 24 => 2} where 12 and 24 are PR id's and they have
    # 3 and 2 fulfilled reviews, respectively.
    def latest_fulfilled_reviews_count_for(pull_request_ids: [])
      sql = <<-SQL
        SELECT r1.pull_request_id, COUNT(r1.id) AS fulfilled_reviews_count
        FROM pull_request_reviews r1
        INNER JOIN
        (
          SELECT max(pull_request_reviews.id) as id
          FROM pull_request_reviews
          INNER JOIN pull_requests ON pull_requests.id = pull_request_reviews.pull_request_id
          WHERE pull_request_id IN :pull_request_ids
          AND state <> :state
          AND pull_request_reviews.user_id <> pull_requests.user_id
          AND pull_request_reviews.user_id NOT IN (
            SELECT review_requests.reviewer_id
            FROM review_requests
            LEFT OUTER JOIN pull_request_reviews_review_requests on pull_request_reviews_review_requests.review_request_id = review_requests.id
            WHERE review_requests.pull_request_id IN :pull_request_ids
              AND review_requests.reviewer_type = 'User'
              AND review_requests.dismissed_at IS NULL
              AND pull_request_reviews_review_requests.review_request_id IS NULL
          )
          GROUP BY pull_request_reviews.user_id, pull_request_reviews.pull_request_id
        ) r2
        ON r1.id = r2.id
        GROUP BY r1.pull_request_id
        LIMIT 1000
      SQL

      query = PullRequestReview.github_sql.new(sql,
        pull_request_ids: pull_request_ids,
        state: PullRequestReview.state_value(:pending),
      )

      query.results.to_h
    end
  end

  # Order by current user, requested reviewers, then other reviewers sorted alphabetically.
  #
  # current_user - the viewing user
  # requests - an optional list of review requests to filter through
  #
  # Returns Array of Users and Teams.
  def sorted_reviewers(current_user, requests: nil)
    users = available_review_users.filter_spam_for(current_user).sort_by { |u| u.login.downcase }

    requests ||= pending_review_requests
    reviewers = requests.map(&:reviewer)
    users -= reviewers
    users = reviewers + users

    if current_user && users.delete(current_user)
      users.unshift(current_user)
    end

    teams = if can_request_team_review?(current_user)
      (available_review_teams - reviewers).sort_by { |t| t.name.downcase }
    else
      []
    end

    (users + teams).compact
  end

  # Check to see what users can be requested
  # includes users that collaborators (:read or :write)
  #
  # Returns an array of Users.
  def available_review_users
    @available_reviewe_users ||= begin
      User.where(id: available_review_user_ids).includes(:profile)
    end
  end

  def available_review_user_ids(filter: nil)
    filter_ids = filter.map(&:id) if filter
    privileged_ids = issue.user_ids_with_privileged_access(actor_ids_filter: filter_ids).
      select { |id| id != user&.id }.compact
    privileged_ids - suspended_user_ids(privileged_ids)
  end

  def suspended_user_ids(privileged_ids)
    return [] if privileged_ids.empty?
    User.where(id: privileged_ids).suspended.pluck(:id)
  end

  # Check to see what teams can be requested
  #
  # Returns an array of Teams.
  def available_review_teams(filter: nil)
    return [] unless repository.in_organization?

    @available_review_teams ||= {}
    @available_review_teams[filter] ||= if filter
      filter_ids = filter.map(&:id)
      repository.teams(immediate_only: false).closed.
        where(organization_id: repository.organization.id, id: filter_ids)
    else
      repository.teams(immediate_only: false).closed.
        where(organization_id: repository.organization.id)
    end
  end

  # Public: Filters a list of users down to only those who are allowed to
  # be requested for review.
  #
  # mixed_reviewers   - An Array of Users and/or Teams to request review from.
  #
  # Returns an Array of Users and/or Teams.
  def filter_allowed_reviewers(mixed_reviewers, actor: nil)
    users, teams = mixed_reviewers.partition { |r| r.is_a?(User) }

    allowed_user_ids = available_review_user_ids(filter: users)
    allowed_users = users.select { |u| allowed_user_ids.include?(u.id) }

    allowed_teams = teams.select { |t| available_review_teams(filter: teams).include?(t) }

    if actor.nil? || can_request_team_review?(actor)
      allowed_users + allowed_teams
    else
      allowed_users
    end
  end

  def can_request_review_from?(reviewer)
    filter_allowed_reviewers([reviewer]).include?(reviewer)
  end

  def can_request_team_review?(actor)
    return false unless repository.in_organization?
    return false unless repository.plan_supports?(:team_review_requests)
    return false unless actor.present?

    if actor.can_have_granular_permissions?
      repository.organization.resources.members.readable_by?(actor)
    else
      # repo collaborators cannot request team review
      repository.organization.direct_or_team_member?(actor)
    end
  end

  # Only return reviews that are not spammy
  #
  # Returns Array of PullRequestReviews
  def visible_sidebar_reviews(viewer)
    latest_reviews_not_requested.reject { |review| review.hide_from_user?(viewer) }
  end

  # Public: Can the user request a review?
  #
  # Returns Boolean
  def can_request_review?(user)
    return false unless user

    science "request_pr_review_no_custom_roles" do |e|
      e.use do
        ::Permissions::Enforcer.authorize(
          action: :request_pr_review,
          actor: user,
          subject: self,
          context: { version: 2 },
        ).allow?
      end

      e.try do
        ::Permissions::Enforcer.authorize(
          action: :request_pr_review,
          actor: user,
          subject: self,
          context: { version: 3 },
        ).allow?
      end
    end

  end

  # Public: Can a user re-request a review? When a review request is
  # auto-generated by CODEOWNERS, the PR author should be able to re-request
  # review when they have read-only access.
  #
  # Returns a Boolean.
  def can_re_request_review?(requester)
    requester == user || can_request_review?(requester)
  end

  # Public: Should reviewer suggestions be returned for this pull request?
  #
  # user - the currently authenticated user
  #
  # Returns a Boolean.
  def include_reviewer_suggestions?(user: nil)
    can_request_review?(user) && suggested_reviewers_available? && suggested_reviewers.any?
  end

  # Check who requested this user for review
  # user - the user who is being requested
  #
  # Returns User
  def user_requesting(user)
    @request_events ||= events.select(&:review_requested?).select do |event|
      if event.subject.is_a?(Team)
        event.subject.member?(user)
      else
        event.subject == user
      end
    end
    @request_events.last&.actor
  end

  # Should we show suggestions for this pull request?
  def suggested_reviewers_available?
    (!persisted? || open?) && PullRequest::SuggestedReviewers.new(self).available?
  end

  # Suggest users to review this pull request.
  #
  # requests - an optional list of review requests to filter through
  # teams - an optional list of teams to consider; will be calculated from the pending team review
  #         requests on this pull request if omitted
  #
  # Returns an Array of SuggestedReviewer.
  def suggested_reviewers(requests: nil, teams: nil)
    @suggested_reviewers ||= begin
      suggester = PullRequest::SuggestedReviewers.new(self)
      suggestions = suggester.find(excluding: user)
      suggestions.reject do |suggestion|
        review_requested_for?(suggestion.user, requests: requests, teams: teams)
      end
    end
  end

  # Public: Request review from codeowners
  # If protected branches turned on also require the requests
  #
  # actor - the User doing the requesting
  # should_save - we only need to save the PR if it is being created for the first time
  #               see bug reported in https://github.com/github/github/pull/76198
  def request_review_from_codeowners(actor, should_save: false)
    return if codeowners!.none?

    # Don't request review if owner has already left a review
    owners_to_request = codeowners.reject { |owner| code_owner_review_fulfilled?(owner) }

    # Don't request review if owner has already dismissed an automated request
    owners_to_request = owners_to_request.reject { |owner| dismissed_automated_request_for?(owner) }

    # Limit the maximum number of code owners we'll request review from
    owners_to_request = owners_to_request.first(MAX_REVIEW_REQUESTS_FROM_CODE_OWNERS)

    owners_to_request.each do |owner|
      rules = codeowners.rules_by_owner[owner].uniq
      reasons = rules.map do |rule|
        { tree_oid: codeowners.tree_oid, path: codeowners.path, line: rule.line, pattern: rule.pattern.to_s }
      end

      review_requests.add_pending_reviewer_with_reasons(owner, reasons: { codeowners: reasons })
    end

    self.save if should_save
  end

  def code_owner_review_fulfilled?(code_owner)
    if code_owner.is_a?(User)
      !!latest_non_pending_review_for(code_owner)
    else
      review_requests.fulfilled.for(code_owner).any?
    end
  end

  def dismissed_automated_request_for?(reviewer)
    dismissed_automated_reviewers.include?(reviewer)
  end

  def dismissed_automated_reviewers
    @dismissed_reviewers ||= unscoped_review_requests.dismissed.automated.reviewers
  end

  def team_review_requests_for(reviewers)
    review_requests.pending.where(reviewer_id: reviewers.map(&:id), reviewer_type: "Team")
  end

  def user_review_requests_for(reviewers)
    review_requests.pending.where(reviewer_id: reviewers.map(&:id), reviewer_type: "User")
  end

  private def plan_manual_review_requests_limit_enabled?
    GitHub.flipper[:plan_manual_review_requests_limit].enabled?(repository)
  end
end
