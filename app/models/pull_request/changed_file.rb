# frozen_string_literal: true

class PullRequest
  class ChangedFile
    attr_reader :path, :additions, :deletions, :repository, :pull_request

    def initialize(path:, additions:, deletions:, repository:, pull_request:)
      @path = path
      @additions = additions
      @deletions = deletions
      @repository = repository
      @pull_request = pull_request
    end

    def async_repository
      Promise.resolve(repository)
    end

    def platform_type_name
      "PullRequestChangedFile"
    end
  end
end
