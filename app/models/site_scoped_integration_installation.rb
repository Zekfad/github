# frozen_string_literal: true

class SiteScopedIntegrationInstallation < ApplicationRecord::Collab
  areas_of_responsibility :ecosystem_apps

  VALID_TARGET_TYPES = %w(User).freeze

  include Ability::Actor
  include AuthenticationTokenable
  include Instrumentation::Model
  include IntegrationInstallable

  after_commit :instrument_creation, on: :create

  belongs_to :integration, optional: false
  belongs_to :target, polymorphic: true

  has_one :latest_version,
    class_name: "IntegrationVersion",
    through: :integration

  alias_attribute :version, :latest_version

  validates_presence_of :integration

  validates_presence_of :target
  validates :target_type, inclusion: VALID_TARGET_TYPES

  delegate :single_file_name, to: :version

  attribute :expires_at, :utc_timestamp

  # SiteScopedIntegrationInstallations default to the global api rate limit
  def rate_limit
    GitHub.api_default_rate_limit
  end

  # Returns a human readable string for use in audit logs.
  def name
    "site_scoped_integration_installation-#{id}"
  end

  def event_context(prefix: event_prefix)
    {
      prefix => name,
      "#{prefix}_id".to_sym => id,
    }
  end

  def event_payload
    {}.tap do |payload|
      payload[event_prefix]        = self
      payload[:integration]        = integration
      payload[target.event_prefix] = target
      payload[:permissions]        = permissions
    end
  end

  def suspended?
    false
  end

  def user_suspended?
    false
  end

  def integrator_suspended?
    false
  end

  private

  def instrument_creation
    instrument :create
  end
end
