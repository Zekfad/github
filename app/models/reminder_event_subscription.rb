# frozen_string_literal: true

class ReminderEventSubscription < ApplicationRecord::Collab
  extend GitHub::Encoding
  force_utf8_encoding :options

  belongs_to :subscriber, polymorphic: true

  enum event_type: {
    review_request: 1,
    team_review_request: 2,
    review_submission: 3,
    comment: 4,
    comment_reply: 5,
    mention: 6,
    assignment: 7,
    pull_request_opened: 8,
    pull_request_labeled: 9,
    pull_request_merged: 10,
    merge_conflict: 11,
    check_failure: 12,
  }

  EVENT_TYPES_PERMITTING_OPTIONS = [
    :check_failure,
    :pull_request_labeled,
  ]

  def parsed_options
    options.to_s.split(",").map(&:downcase).map(&:strip)
  end

  def options_includes?(target)
    parsed_options.include?(target.to_s.downcase)
  end
end
