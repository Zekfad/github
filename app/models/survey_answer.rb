# rubocop:disable Style/FrozenStringLiteralComment

require "csv"

class SurveyAnswer < ApplicationRecord::Domain::Surveys
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  belongs_to :choice, class_name: "SurveyChoice", foreign_key: "choice_id"
  validates_presence_of :choice

  belongs_to :question, class_name: "SurveyQuestion", foreign_key: "question_id"
  validates_presence_of :question

  validate :choice_must_belong_to_question

  belongs_to :survey
  validates_presence_of :survey

  belongs_to :user
  validates_presence_of :user

  belongs_to :survey_group

  after_create_commit :instrument_create

  def self.most_recent_first
    order(:id).reverse_order
  end

  def self.for(user)
    where(user_id: user)
  end

  # Internal: Instrument on after_create.
  def instrument_create
    instrument :create
  end

  def selections=(selections)
    selections = Array.wrap(selections.presence).sort
    self.other_text = CSV.generate_line(selections, row_sep: "")
  end

  def selections
    CSV.parse(other_text.to_s).first || Array.new
  end

  def normalized_other_text
    other_text.to_s.strip
  end

  # other_text is a varbinary column to support wide multibyte characters (like
  #   emoji..) however the value comes back as ASCII-8BIT, which can cause
  #   encoding mismatches such as this one:
  #   https://haystack.githubapp.com/github/needles/zTmgb03tjKGguX4v633rEw
  def other_text
    text = self.read_attribute(:other_text)
    text.force_encoding("UTF-8") unless text.nil?
    text
  end

  def self.save_as_group(user_id, survey_id, answers)
    SurveyGroup.transaction do
      group = SurveyGroup.create!({survey_id: survey_id, user_id: user_id})
      (answers || []).map do |answer|
        SurveyAnswer.create!({
          user_id: user_id,
          survey_id: survey_id,
          survey_group_id: group.id,
          question_id: answer[:question_id],
          choice_id: answer[:choice_id],
          other_text: answer[:other_text],
        })
      end
      group
    end
  rescue ActiveRecord::RecordInvalid
    false
  end

  private
  # Default attributes for auditing
  def event_payload
    { survey_slug: survey.slug }
  end

  def choice_must_belong_to_question
    return if question_id == choice&.question_id
    errors.add(:choice, "is invalid for question")
  end
end
