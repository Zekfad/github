# rubocop:disable Style/FrozenStringLiteralComment

# Represents a portion of an IssueTimeline. Large timelines are broken up into
# slices to avoid rendering too many items at once and timing out.
class IssueTimelineSlice
  # Create an IssueTimelineSlice
  #
  # timeline - the IssueTimeline
  # viewer   - User viewing the timeline
  # first    - optional Integer index. The slice will start at this index.
  # last     - optional Integer index. The slice will not extend beyond this index.
  # limit    - optional Integer count. In general, the slice will be no larger
  #            than this limit. However, if limiting to this size would result
  #            in a remaining slice that is smaller than MINIMUM_SLICE_SIZE,
  #            those remaining items will be included in this slice instead.
  # new_items_count - optional Integer count. If specified, this many new items
  #                   will be included in the slice (with the remaining of
  #                   `limit` being used for oldest items as usual).
  # focus_type - the anchor name prefix. example: in issuecomment-123, the type is issuecomment.
  #              this is expected to be received from the clicked anchor link.
  # focus_value - some identifying value for the specific instance of the type being focused by the anchor.
  #               for example, in issuecomment-123 it will be 123.
  # focus_padding - optional Integer count. If passed, this many items will be
  #                 included in the slice on either side of the focused item.
  #                 Defaults to 5.
  def initialize(timeline, viewer:, first: nil, last: nil, limit: nil, new_items_count: nil, focus_type: nil,
    focus_value: nil, focus_padding: nil)
    @timeline = timeline
    @viewer = viewer
    @first = first
    @last = last
    @limit = limit
    @new_items_count = new_items_count || 0
    @focus_type = focus_type
    @focus_value = focus_value
    @focus_padding = focus_padding || 5
  end

  # Public: Get the items in this slice
  #
  # Items are retrieved from IssueTimeline#timeline_for, then sliced based on
  # the `first`, `last`, and `limit` parameters passed to #initialize. Items
  # before `first` and after `last` are removed, then the first (i.e., oldest)
  # `limit` items are returned. (See #limited? for cases where more than
  # `limit` items could be returned.)
  #
  # Returns an Array of timeline items.
  def items
    if focus_value
      focused_items
    elsif limited?
      limited_items
    else
      unlimited_items
    end
  end

  # Public: Was the limit applied to this slice?
  #
  # I.e., are there more items to load in a subsequent slice?
  def limited?
    limit && unlimited_items.size >= limit + MINIMUM_SLICE_SIZE
  end

  def focused_index
    val = focus_value.to_i
    case focus_type
    when "issuecomment"
      item_index { |item| item.is_a?(IssueComment) && item.id == val }

    # yes, this essentially means discussion-diff and discussion are the same. This is ok, because they are the
    # the same. Discussion diff ids refer to the first comment of a Fake PR thread, discussion refers to any comment.
    when "discussion-diff", "discussion"
      item_index { |item| item.is_a?(DeprecatedPullRequestReviewThread) && item.comments.map(&:id).include?(val) }
    when "commits-pushed"
      item_index { |item| item.is_a?(Commit) && item.abbreviated_oid == focus_value }
    when "ref-commit"
      item_index { |item| item.is_a?(IssueEvent) && item.abbreviated_oid == focus_value }
    when "event"
      item_index { |item| item.is_a?(IssueEvent) && item.id == val }
    when "commitcomment"
      item_index { |item| item.is_a?(CommitCommentThread) && item.comments.map(&:id).include?(val) }
    when "ref-issue", "ref-pullrequest"
      item_index { |item| item.is_a?(CrossReference) && item.source_id == val }
    end
  end

  private

  # When a user loads more timeline items, it's annoying if loading only brings
  # in a small number of items. They should just be included in the previous
  # batch of items instead. This constant defines the smallest slice we're
  # willing to send to the user; anything smaller than this just gets included
  # in the previous batch. (Sometimes this is called "soft truncation".)
  MINIMUM_SLICE_SIZE = 2

  attr_reader :timeline, :viewer, :first, :last, :limit, :new_items_count, :focus_value, :focus_type, :focus_padding

  def all_items
    @all_items ||= timeline.timeline_for(viewer)
  end

  def unlimited_items
    @unlimited_items ||= if first || last
      all_items[(first || 0)..(last || -1)] || []
    else
      all_items
    end
  end

  def limited_items
    @limited_items ||= begin
      old_items = unlimited_items.first(limit - new_items_count)
      new_items = new_items_count > 0 ? unlimited_items.last(new_items_count) : []

      marker_first = (first || 0) + old_items.size
      marker_last = (last || (all_items.size - 1)) - new_items.size
      marker = IssueTimelineProgressiveDisclosureMarker.new(timeline, first: marker_first, last: marker_last, limit: limit, focus_padding: focus_padding)

      items = old_items
      items << marker
      items.concat(new_items)

      items
    end
  end

  def focused_items
    @focused_items ||= focused_items!
  end

  def item_index(&block)
    unlimited_items.find_index(&block)
  end

  def focused_items!
    index = focused_index
    return [] unless index

    focus_first = [index - focus_padding, 0].max
    if focus_first < MINIMUM_SLICE_SIZE
      focus_first = 0
    end

    focus_last = [index + focus_padding, unlimited_items.size - 1].min
    if focus_last + MINIMUM_SLICE_SIZE >= unlimited_items.size
      focus_last = unlimited_items.size - 1
    end

    items = []
    if focus_first > 0
      items << IssueTimelineProgressiveDisclosureMarker.new(timeline, first: first, last: first + focus_first - 1, limit: limit, focus_padding: focus_padding)
    end
    items.concat(unlimited_items[focus_first..focus_last])
    if focus_last < unlimited_items.size - 1
      items << IssueTimelineProgressiveDisclosureMarker.new(timeline, first: first + focus_last + 1, last: last, limit: limit, focus_padding: focus_padding)
    end

    items
  end
end
