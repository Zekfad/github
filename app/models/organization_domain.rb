# frozen_string_literal: true

class OrganizationDomain < ApplicationRecord::Ballast
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model

  belongs_to :organization

  before_validation :normalize_domain
  validates_presence_of :organization, :domain
  validates_inclusion_of :verified, in: [true, false]
  validates_uniqueness_of :domain, scope: :organization, message: "has already been claimed by your organization", case_sensitive: false
  validate :valid_domain, :not_homograph

  after_create :generate_verification_token
  before_destroy :ensure_domain_not_required_for_policy

  scope :verified, -> { where(verified: true) }
  scope :unverified, -> { where(verified: false) }

  VERIFY_ATTEMPTS_LIMIT = 10
  VERIFY_ATTEMPTS_LIMIT_TTL = 1.hour

  DNS_RESOLVER_OPTIONS = {
    retry_times: 2,
    query_timeout: 5,
    dnssec: false,
    do_caching: false,
  }.freeze

  def self.normalize_domain(domain)
    return unless domain
    normalized_domain = domain.dup
    normalized_domain.prepend("http://") unless normalized_domain =~ %r{\Ahttps?://}
    Addressable::IDNA.to_unicode(Addressable::URI.parse(normalized_domain).normalize.host)
  rescue Addressable::URI::InvalidURIError
    normalized_domain
  end

  def token_key(org_id = organization_id)
    "domain_verification.#{org_id}.#{domain}"
  end

  def rate_limit_key
    "verify_org_domain_attempt.#{organization_id}.#{domain}"
  end

  def async_verification_token
    Platform::Loaders::ActiveRecord.load(::Organization, organization_id.to_i).then do |org|
      verification_token(org.id)
    end
  end

  def verification_token(org_id = organization_id)
    GitHub.kv.get(token_key(org_id)).value { nil }
  end

  def generate_verification_token
    if self.verified?
      errors.add(:base, "Domain is already verified.")
      false
    elsif verification_token.present?
      errors.add(:base, "Verification token is already present.")
      false
    else
      token = SecureRandom.hex(5)
      GitHub.kv.set(token_key, token, expires: 1.week.from_now)
      true
    end
  end

  def async_token_expiration_time
    Platform::Loaders::ActiveRecord.load(::Organization, organization_id.to_i).then do |org|
      GitHub.kv.ttl(token_key(org.id)).value { nil }
    end
  end

  def async_dns_host_name
    Platform::Loaders::ActiveRecord.load(::Organization, organization_id.to_i).then do |org|
      "_github-challenge-#{org.login}.#{punycode_encoded_domain}"
    end
  end

  def dns_host_name
    async_dns_host_name.sync
  end

  def async_host_name_found?
    async_dns_records.then do |records|
      records.any?
    end
  rescue Dnsruby::ResolvTimeout, Dnsruby::ServFail
    Promise.resolve(false)
  end

  def host_name_found?
    async_host_name_found?.sync
  end

  def async_verification_token_found?
    async_dns_records.then do |records|
      records.any? { |r| r.strings.first == verification_token }
    end
  rescue Dnsruby::ResolvTimeout, Dnsruby::ServFail
    Promise.resolve(false)
  end

  def verification_token_found?
    async_verification_token_found?.sync
  end

  def verify
    if verified?
      errors.add(:base, "Domain has already been verified.")
      return false
    end

    if rate_limited?
      errors.add(:base, "You've reached the maximum number of verification attempts. Please try again later.")
      return false
    end

    if verification_dns_record_exists?
      update_attribute(:verified, true)
      GitHub.kv.del(token_key)
      DomainVerificationNoticeJob.perform_later(organization, domain)
      true
    else
      false
    end
  rescue Dnsruby::ResolvError
    errors.add(:base, "We couldn't find the TXT record. Note that DNS changes can take up to 72 hours.")
    false
  rescue Dnsruby::ResolvTimeout, Dnsruby::ServFail
    errors.add(:base, "An error occurred while verifying this DNS record.")
    false
  end

  def unverify
    unless verified?
      errors.add(:base, "Cannot unverify a domain that is not verified.")
      return false
    end

    if async_required_for_policy_enforcement?.sync
      errors.add(:base, "Cannot unverify a domain that is required to enforce an organization policy.")
      return false
    end

    GitHub.kv.del(token_key)
    update_attribute(:verified, false)
    true
  end

  def adminable_by?(viewer)
    return false unless viewer && GitHub.domain_verification_enabled?
    return true if viewer.site_admin?
    organization.adminable_by?(viewer)
  end

  def default_resolver
    return @default_resolver if @default_resolver
    @default_resolver = Dnsruby::Resolver.new(DNS_RESOLVER_OPTIONS)
  end

  def authoritative_nameservers
    return @authoritative_nameservers if @authoritative_nameservers
    # Find authoritative nameservers for this domain
    @authoritative_nameservers = default_resolver.query(punycode_encoded_domain, Dnsruby::Types::NS).answer.map do |record|
      # For some reason this query can return records that aren't nameservers, skip those.
      next record.nsdname.to_s if record.type == Dnsruby::Types::NS
    end.compact
    # Include default nameservers since the domain-specific nameservers aren't always reliable
    @authoritative_nameservers.push(*Dnsruby::Config.default_config_hash[:nameserver])
  end

  def dns_resolver
    return @dns_resolver if @dns_resolver
    # Create a new dns resolver based on the domain-specific and default nameservers
    @dns_resolver = Dnsruby::Resolver.new(
      DNS_RESOLVER_OPTIONS.merge(nameservers: authoritative_nameservers),
    )
  end

  def instrument_create(actor)
    instrument :create, actor: actor
    GitHub.dogstats.increment("organization_domain.create")
  end

  def instrument_verify(actor, staff_actor = false)
    if staff_actor
      guarded_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
      GitHub.instrument("staff.verify_domain", event_payload.merge(guarded_actor))
    else
      instrument :verify, actor: actor
    end
    GitHub.dogstats.increment("organization_domain.verify")
  end

  # Only staff members can unverify domains
  def instrument_unverify(actor)
    guarded_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
    GitHub.instrument("staff.unverify_domain", event_payload.merge(guarded_actor))
    GitHub.dogstats.increment("organization_domain.unverify")
  end

  def instrument_destroy(actor)
    instrument :destroy, actor: actor
    GitHub.dogstats.increment("organization_domain.destroy")
  end

  def punycode_encoded_domain
    Addressable::IDNA.to_ascii(domain)
  end

  # Public: Is this domain required to exist to enforce a policy in an organization?
  #
  # Returns a Promise<Boolean>.
  def async_required_for_policy_enforcement?
    return Promise.resolve(false) unless verified?

    async_organization.then do |organization|
      promises = [
                  organization.async_restrict_notifications_to_verified_domains?,
                  organization.async_verified_domains,
                 ]

      Promise.all(promises).then do |notification_restriction_enabled, verified_domains|
        # If notifications are restricted and this is the only verified domain,
        # this domain is required to exist for that policy to function.
        notification_restriction_enabled && verified_domains.count == 1
      end
    end
  end

  def required_for_policy_enforcement?
    async_required_for_policy_enforcement?.sync
  end

  # Public: Disables any enforcement policies that depend on this domain.
  #
  # actor - The User who is disabling the policies.
  #
  # Returns a Boolean.
  def disable_dependent_policies(actor:)
    return true unless organization.restrict_notifications_to_verified_domains?

    organization.disable_notification_restrictions(actor: actor)
  end

  private

  def async_dns_records
    Platform::Loaders::ActiveRecord.load(::Organization, organization_id.to_i).then do |org|
      dns_record_url = "_github-challenge-#{org.login}.#{punycode_encoded_domain}"
      begin
        dns_resolver.query(dns_record_url, Dnsruby::Types::TXT).answer.select do |record|
          record.type == Dnsruby::Types::TXT
        end
      rescue Dnsruby::ResolvError, Dnsruby::ResolvTimeout, Dnsruby::ServFail
        []
      end
    end
  end

  def rate_limited?
    return unless GitHub.rate_limiting_enabled?
    RateLimiter.at_limit?(
      rate_limit_key,
      { max_tries: VERIFY_ATTEMPTS_LIMIT, amount: 1, ttl: VERIFY_ATTEMPTS_LIMIT_TTL },
    )
  end

  def verification_dns_record_exists?
    dns_record_url = "_github-challenge-#{organization.login}.#{punycode_encoded_domain}"
    records = dns_resolver.query(dns_record_url, Dnsruby::Types::TXT).answer.select do |record|
      record.type == Dnsruby::Types::TXT
    end
    if records.empty?
      errors.add(:base, "We couldn't find the TXT record. Note that DNS changes can take up to 72 hours.")
      false
    elsif records.any? { |r| r.strings.first == verification_token }
      true
    else
      errors.add(:base, "The TXT record did not match the verification code. Note that DNS changes can take up to 72 hours.")
      false
    end
  end

  def normalize_domain
    self.domain = OrganizationDomain.normalize_domain(domain)
  end

  def valid_domain
    unless domain && PublicSuffix.valid?(domain, default_rule: nil, ignore_private: true)
      errors.add(:domain, "is not a valid public domain.")
    end
  end

  def not_homograph
    if domain && HomographDetector.homograph_attack?("http://" + Addressable::IDNA.to_unicode(domain))
      errors.add(:domain, "is not eligible for verification, as it is a potential homograph.")
    end
  end

  def ensure_domain_not_required_for_policy
    if async_required_for_policy_enforcement?.sync
      errors.add(:domain, "is required to enforce an organization policy and cannot be deleted")
      throw :abort
    end
  end

  def event_payload
    {
      organization: organization,
      organization_domain: self,
      domain_name: self.domain,
    }
  end
end
