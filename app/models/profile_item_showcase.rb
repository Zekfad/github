# frozen_string_literal: true

# Public: Represents repositories and gists owned by the profile page owner, to be shown
# on the profile page. Will be either the pinned repositories and gists if the owner has
# any, or the "most popular" repositories otherwise.
class ProfileItemShowcase
  def initialize(user:, viewer:)
    @user = user
    @viewer = viewer
  end

  # Public: Returns true if the profile owner has selected any repositories or gists to show on
  # their profile.
  def has_pinned_items?
    pinned_items.size > 0
  end

  # Public: The items in the showcase, using the owner's pinned repositories and gists if
  # present, falling back to the most popular repositories based on stargazer count.
  #
  # Returns an Array of Repositories and/or Gists.
  def items
    if pinned_items.any?
      pinned_items
    else
      popular_repositories
    end
  end

  private

  def pinned_items
    @pinned_items ||= @user.pinned_items(viewer: @viewer)
  end

  def popular_repositories
    @user.public_repositories.active.
      filter_spam_and_disabled_for(@viewer).
      order("watcher_count DESC").
      order("created_at ASC")
  end
end
