# frozen_string_literal: true

class KeyLink < ApplicationRecord::Collab
  belongs_to :owner, polymorphic: true

  # Every key prefix needs to match this regex
  KEY_PREFIX_VALIDATION_RE = %r{\A[a-z][a-z0-9\-:/#]{1,13}[a-z\-:/#]\z}i

  URL_NUMBER_TEMPLATE = "<num>"

  MAX_PER_OWNER = {
    Repository: 50,
  }

  validates :key_prefix,
    presence: true,
    uniqueness: { scope: :owner, case_sensitive: false },
    length: { minimum: 3, maximum: 15 }
  validates :url_template,
    presence: true
  validates :owner_type,
    presence: true,
    inclusion: { in: %w[Repository] }
  validates_format_of :key_prefix, with: %r{\A[a-z]}i,
    message: "must begin with a letter"
  validates_format_of :key_prefix, without: %r{[0-9]\Z},
    message: "must not end with a number"
  validates_format_of :key_prefix, with: KEY_PREFIX_VALIDATION_RE,
    message: "must only contain letters, numbers, or -:/#"
  validate :ensure_url_template_valid
  validate :validate_key_link_count, on: :create

  after_commit :instrument_creation_event, on: :create

  def url(number)
    url_template
      .sub(URL_NUMBER_TEMPLATE, number)
  end

  def example_key
    "#{key_prefix}123"
  end

  def example_url
    url("123")
  end

  private

  def ensure_url_template_valid
    return if url_template.blank?
    if url_template.match(/\s/)
      return errors.add(:url_template, "must not contain white space")
    end
    if url_template.match(/[\"\']/)
      return errors.add(:url_template, "must not contain quotes")
    end
    uri = Addressable::URI.parse(url_template)
    unless uri.absolute?
      return errors.add(:url_template, "must be an absolute URL")
    end
    unless ["http", "https"].include?(uri.scheme)
      errors.add(:url_template, "must be an http(s) URL")
    end
    unless url_template.include?(URL_NUMBER_TEMPLATE)
      errors.add(:url_template,
        "is missing a #{KeyLink::URL_NUMBER_TEMPLATE} token")
    end
  rescue Addressable::URI::InvalidURIError
    errors.add(:url_template, "is invalid")
  end

  def validate_key_link_count
    max = MAX_PER_OWNER[owner.class.name.to_sym]
    if owner && KeyLink.where(owner: owner).count >= max
      errors.add(:base, "A maximum of #{max} autolink references may be created per #{owner.class.name}")
    end
  end

  def instrument_creation_event
    GitHub.dogstats.increment("key_links", tags: ["action:create", "valid:true"])
    GlobalInstrumenter.instrument("key_link.create", {
      actor: User.find_by(id: GitHub.context[:actor_id]),
      key_link: self,
    })
  end

end
