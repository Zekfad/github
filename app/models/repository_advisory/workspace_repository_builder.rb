# frozen_string_literal: true

module RepositoryAdvisory::WorkspaceRepositoryBuilder
  FIELDS_COPIED_FROM_REPOSITORY = %w(
    name
    updated_at
    pushed_at
    pushed_at_usec
    disk_usage
    primary_language_name_id
  ).freeze

  # Public: Builds a security workspace for a repository advisory
  #
  # The security workspace is a `Repository`. The returned object is unsaved.
  #
  # Returns Repository
  def self.perform(advisory, actor)
    raise ArgumentError, "actor cannot be nil" unless actor

    repository = advisory.repository
    attributes = repository.attributes.slice(*FIELDS_COPIED_FROM_REPOSITORY)

    # The name of any repository (including a workspace repository) must be
    # 100 bytes or fewer. To make room for a 19-character GHSA ID and a hyphen
    # to separate it, we need to truncate the original repository's name to
    # 80 characters.
    truncated_repository_name = repository.name.truncate(80, omission: "")
    ghsa_id_suffix = advisory.ghsa_id.downcase
    name = "#{truncated_repository_name}-#{ghsa_id_suffix}"

    advisory.build_workspace_repository(attributes.merge({
      name: name,
      private: true,
      owner: repository.owner,
      parent: nil,
      network: nil,
      created_by_user_id: actor.id,
      has_issues: false,
      has_wiki: false,
      has_downloads: false,
      has_projects: false,
      repository_license: repository.repository_license&.dup,
    }))
  end
end
