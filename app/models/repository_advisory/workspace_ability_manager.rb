# frozen_string_literal: true

# This class co-ordinates the inheritence of abilities from
# the parent repository and advisory unto newly created workspaces
class RepositoryAdvisory::WorkspaceAbilityManager
  include GitHub::AreasOfResponsibility

  areas_of_responsibility :repositories, :security_advisories

  # Adds the #apply_abilities method to directly write to the Ability table
  include Ability::Grant::ApplyAbilities

  def initialize(workspace)
    @workspace = workspace
  end

  # Important: This method should be *only* be called from a background job
  #            as we need to ensure we throttle on the abilities table due
  #            to the number of rows potentially involved
  def perform
    inherit_abilities(@workspace, @workspace.parent_advisory_repository, :admin)
    inherit_abilities(@workspace, @workspace.parent_advisory, :write)
  end

  private

  def inherit_abilities(workspace, subject, action)
    workspace_abilities = inheritable_abilities(subject, action).map do |inheritable_ability|
      [
        inheritable_ability.actor_id,
        inheritable_ability.actor_type,
        Ability.actions[inheritable_ability.action],
        workspace.ability_id,
        workspace.ability_type,
        Ability.priorities[inheritable_ability.priority],
        inheritable_ability.parent_id,
        GitHub::SQL::NOW,
        GitHub::SQL::NOW,
      ]
    end

    # Apply abilities directly as a batch for performance
    apply_abilities(workspace_abilities, stats_key: :direct)
  end

  def inheritable_abilities(subject, action)
    Authorization.service.direct_abilities_on_subject(subject: subject,
                                                      action: action)
  end
end
