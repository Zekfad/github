# frozen_string_literal: true

# Provides an interface to manage all Workspaces
# belonging to a given Repository#id
class RepositoryAdvisory::WorkspaceRepositoryManager
  def initialize(repository_id)
    @repository_id = repository_id
  end

  def self.grant(actor, repository:, action:)
    if action == :admin
      new(repository.id).grant_all_workspaces(actor, action: :admin)
    else
      # Workspaces only support admin actions,so in the event the actor is
      # being granted :read or :write, we should instead attempt to revoke
      # any :admin ability the may already have
      revoke(actor, repository: repository)
    end
  end

  def self.revoke(actor, repository:)
    new(repository.id).revoke_all_workspaces(actor)
  end

  def self.transfer_ownership(repository:)
    new(repository.id).transfer_ownership
  end

  def self.hide_and_remove_all(actor, repository:)
    new(repository.id).hide_and_remove_all_workspaces(actor)
  end

  # Grants abilities on all Advisory Workspaces
  def grant_all_workspaces(actor, action:)
    return if repository_is_workspace?(@repository_id)

    workspaces.map do |workspace|
      if actor.is_a? Team
        workspace.add_team(actor, action: :admin)
      else
        workspace.add_member(actor, action: :admin)
      end
    end
  end

  # Revokes abilities on all Advisory Workspaces
  def revoke_all_workspaces(actor)
    return if repository_is_workspace?(@repository_id)

    workspaces.map do |workspace|
      if actor.is_a? Team
        workspace.remove_team(actor)
      else
        workspace.remove_member(actor)
      end
    end
  end

  # Enqueues a job to change ownership of every workspace owned by @repository
  #
  # This is intended to be called from a background job in order to fan out
  # a single transfer job per workspace for atomicity.
  def transfer_ownership
    return if repository_is_workspace?(@repository_id)

    workspaces.pluck(:id).map do |workspace_repository_id|
      TransferWorkspaceJob.perform_later(workspace_repository_id)
    end
  end

  # Performs workspace removal by synchronously hiding it and enqueuing
  # the normal delete job to complete archival.
  def hide_and_remove_all_workspaces(actor)
    return if repository_is_workspace?(@repository_id)

    workspaces.map do |workspace_repository|
      # Remove workspaces, but don't count instrument them as deletions
      workspace_repository.remove(actor, instrument: false)
    end
  end

  private

  def advisories
    RepositoryAdvisory.where(repository_id: @repository_id)
  end

  def workspaces
    Repository.where(id: advisories.pluck(:workspace_repository_id))
  end

  def repository_is_workspace?(repository_id)
    RepositoryAdvisory.where(workspace_repository_id: repository_id).any?
  end
end
