# frozen_string_literal: true

class SponsorsGoalContribution < ApplicationRecord::Collab
  belongs_to :goal,
    required: true,
    class_name: :SponsorsGoal,
    foreign_key: :sponsors_goal_id

  belongs_to :tier,
    required: true,
    class_name: :SponsorsTier,
    foreign_key: :sponsors_tier_id

  belongs_to :sponsor,
    required: true,
    class_name: :User
end
