# frozen_string_literal: true

class IpWhitelistEntry < ApplicationRecord::Collab
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  belongs_to :owner, polymorphic: true

  # actor_ip does not map to a database column. It should be set with a String
  # value representing the actor's IP address when creating/updating/destroying
  # an IpWhitelistEntry if you want to prevent the actor being locked out of the
  # account that owns the entry.
  #
  # For example:
  #
  # entry = owner.ip_whitelist_entries.new(entry_params)
  # entry.actor_ip = request.remote_ip
  # if entry.save ...
  attr_accessor :actor_ip

  validates :owner, presence: true
  validate :valid_owner_type
  validate :valid_plan_for_owner

  validates :whitelisted_value, presence: true
  validate :valid_whitelisted_value
  validate :prevent_actor_being_locked_out
  before_destroy -> { prevent_actor_being_locked_out(context: :destroy) }, prepend: true

  before_save :set_range_values
  after_create_commit :instrument_create
  after_update_commit :instrument_update
  after_destroy_commit :instrument_destroy

  def platform_type_name
    "IpAllowListEntry"
  end

  # Scope that returns the IP whitelist entries owned by object or by
  # the owners of object.
  #
  # object - An Organization or Business for which the IP whitelist entries
  # should be returned.
  #
  # Returns ActiveRecord::Relation.
  scope :usable_for, ->(object) {
    case object
    when Organization
      # Organizations only inherit the IP allow list entries from an owning
      # enterprise account when an IP allow list is *enabled* on the owning
      # enterprise account.
      if object.business&.ip_whitelisting_enabled?
        usable_for(object.business).or(where(
          owner_type: object.class.base_class.name,
          owner_id: object.id,
        ))
      else
        where(
          owner_type: object.class.base_class.name,
          owner_id: object.id,
        )
      end
    when Business
      where(
        owner_type: object.class.base_class.name,
        owner_id: object.id,
      )
    when Integration
      where(
        owner_type: object.class.base_class.name,
        owner_id: object.id,
      )
    when IntegrationInstallation
      where(
        owner_type: object.integration.class.base_class.name,
        owner_id: object.integration.id,
      )
    when Array
      where(
        owner_type: Integration.base_class.name,
        owner_id: object,
      )
    else
      where("1=0")
    end
  }

  scope :installed_for, ->(object) {
    case object
    when Organization
      usable_for(IntegrationInstallation.where(target_id: object.id, target_type: "User").map(&:integration_id))
    when Business
      usable_for(IntegrationInstallation.where(target_id: object.id, target_type: "Business").map(&:integration_id))
    end
  }

  # Scope that returns only IP whitelist entries that are active.
  #
  # Returns ActiveRecord::Relation.
  scope :active, -> { where(active: true) }

  # Scope that returns only IP whitelist entries that match the given IP
  # address.
  #
  # ip - String representing a single IPv4 or IPv6 address.
  #
  # Returns ActiveRecord::Relation.
  scope :matching_ip, ->(ip) {
    where <<-SQL, ip: ip
      INET6_ATON(:ip)
      BETWEEN `ip_whitelist_entries`.`range_from`
      AND     `ip_whitelist_entries`.`range_to`
    SQL
  }

  # Public: Returns a String containing the name and type of the owner for use
  # in copy that refers to the owner owning/managing the IP whitelist entry.
  #
  # Returns String.
  def owner_name_and_type
    if owner_type == "Business"
      "#{owner.name} enterprise"
    elsif owner_type == "Integration"
      "#{owner.name} GitHub App"
    else
      "#{owner.safe_profile_name} organization"
    end
  end

  # Public: Returns a Boolean indicating whether this whitelist entry includes a
  # specific IP address?
  #
  # ip - A String representing a single IP address.
  #
  # Returns Boolean.
  def includes?(ip)
    block = IPAddr.new(whitelisted_value)
    block.include?(IPAddr.new(ip))
  end

  # Public: Is a potential IP Whitelist entry owner eligible for the IP
  # whitelisting feature?
  #
  # IP whitelisting is available to enterprise accounts and organization
  # accounts on the Enterprise plan (internally `business_plus`).
  #
  # owner - A potential owner of an IP whitelist entry. Currently expected to be
  # a Business or Organization.
  #
  # Returns Boolean.
  def self.eligible_for_ip_whitelisting?(owner)
    owner.is_a?(::Business) ||
    (owner.is_a?(::Organization) && owner&.plan_supports?(:ip_whitelisting)) ||
    owner.is_a?(::Integration)
  end

  # Public: Is the owner of this entry eligible for the IP whitelisting feature?
  #
  # Returns Boolean.
  def owner_eligible_for_ip_whitelisting?
    self.class.eligible_for_ip_whitelisting?(owner)
  end

  def event_prefix
    :ip_allow_list_entry
  end

  def event_context(prefix: event_prefix)
    {
      prefix => whitelisted_value,
      "#{prefix}_id".to_sym => id,
    }
  end

  def event_payload
    {
      :ip_allow_list_entry => self,
      :ip_allow_list_entry_name => name,
      :active => active?,
      owner.event_prefix => owner,
    }
  end

  # Public: Is the IP address included in the Array of IpWhitelistEntries?
  #
  # ip - A String representing an IP address.
  # entries - An Array of IpWhitelistEntry objects.
  #
  # Returns Boolean.
  def self.ip_included_in_entries?(ip:, entries:)
    return false if ip.blank? || entries.blank?

    entries.each do |entry|
      return true if entry.includes?(ip)
    end

    false
  end

  private

  # Check that the owner is an Organization or Business. Don't use the Rails
  # `inclusion` validation because `owner_type` is actually "User" for orgs.
  def valid_owner_type
    return if [Organization, Business, Integration].include?(owner.class)
    errors.add(:owner, "must be an organization or enterprise account")
  end

  # Check that the owner has an eligible plan for IP whitelisting.
  def valid_plan_for_owner
    unless owner_eligible_for_ip_whitelisting?
      errors.add(:owner, "doesn't have an eligible plan for an IP allow list")
    end
  end

  # Check that whitelisted_value is valid. It should be a single IPv4/IPv6
  # address (Examples: "192.168.103.201","2001:db8:0:ffff:ffff:ffff:ffff:ffff")
  # or a range of IPv4/IPv6 addresses in CIDR notation (Examples:
  # "192.168.100.0/22", "2001:db8::/48").
  def valid_whitelisted_value
    begin
      IPAddr.new self.whitelisted_value
    rescue IPAddr::AddressFamilyError, IPAddr::InvalidAddressError, IPAddr::InvalidPrefixError
      errors.add(:whitelisted_value, "must be a valid IP address or range of addresses in CIDR notation")
    end
  end

  # Set range_from and range_to to the network byte ordered string form of the
  # first and last IP addresses in the range.
  def set_range_values
    ip_addr = IPAddr.new self.whitelisted_value
    range = ip_addr.to_range
    self.range_from = range.first.hton
    self.range_to = range.last.hton
  end

  def instrument_create
    instrument :create
    actor = User.find_by(id: GitHub.context[:actor_id])
    GlobalInstrumenter.instrument("ip_allow_list_entry.create", {
      entry: self, actor: actor
    })
  end

  def instrument_update
    instrument :update
    actor = User.find_by(id: GitHub.context[:actor_id])
    GlobalInstrumenter.instrument("ip_allow_list_entry.update", {
      entry: self, actor: actor
    })
  end

  def instrument_destroy
    instrument :destroy
    actor = User.find_by(id: GitHub.context[:actor_id])
    GlobalInstrumenter.instrument("ip_allow_list_entry.destroy", {
      entry: self, actor: actor
    })
  end

  # Check that creating/updating/destroying the entry will not lock the actor
  # out based on the presence of actor_ip.
  #
  # context - Optional Symbol indicating the context. Currently only accepted
  #   as `:destroy` to allow this method to be shared between `validates` and
  #   `before_destroy`.
  #
  # Returns nothing.
  def prevent_actor_being_locked_out(context: nil)
    return unless actor_ip.present?
    return unless owner&.ip_whitelisting_enabled?

    # Set up the active entries that would result from this
    # create/update/destroy.
    active_entries = IpWhitelistEntry.usable_for(owner).active.to_a
    if new_record? && active?
      active_entries << self
    elsif persisted?
      active_entries.delete_if { |entry| entry.id == self.id }
      if active? && context != :destroy
        active_entries << self
      end
    end

    return if IpWhitelistEntry.ip_included_in_entries? \
      ip: actor_ip, entries: active_entries

    errors.add(:base, "This change would prevent you from accessing the account from your current IP address")

    throw :abort
  end
end
