# rubocop:disable Style/FrozenStringLiteralComment

# Tracks the metadata for the content addressable asset store in the Alambic
# service.  Each Asset stores the basic immutable metadata.  Some of it is
# always set (size, sha1, md5, and sha-256).  Other attributes may be specific
# to a certain file type (image width and height).  An Asset may be shared
# between multiple uploadable models through the Asset::Reference association.
class Asset < ApplicationRecord::Domain::Assets
  areas_of_responsibility :lfs

  # work around Asset::Status referring to ::Status rather than autoloading
  # app/models/asset/status.rb:
  require_dependency "asset/status"

  # Defers #size to Asset in models that need it just for validations.
  module SizeHelper
    def size
      asset ? asset.size : 0
    end

    def size_before_type_cast
      asset ? asset.size_before_type_cast : 0
    end
  end

  ACCEPTED_METADATA = [:width, :height]

  has_many :archives, class_name: "Asset::Archive"
  has_many :references, class_name: "Asset::Reference"

  validates_presence_of :oid, :size
  validates_numericality_of :size, greater_than: -1
  validates_length_of :oid, is: 64

  # All Assets are stored in Alambic according to the sha-256 hash of the
  # contents.
  validates_uniqueness_of :oid, case_sensitive: true
  validate :meta_data_is_immutable

  # Public: Uploads a feature with the given oid and meta data, and associate it
  # with an asset.
  def self.upload(uploadable, oid, meta)
    transaction do
      asset = find_or_init_with_meta(meta.merge(oid: oid))
      uploadable ||= yield asset
      asset.reference!(uploadable)
      uploadable.after_upload(meta)
    end

    uploadable
  end

  def self.store(uploadable, meta = nil)
    meta ||= {}
    meta[:oid] ||= uploadable.oid
    meta[:size] ||= uploadable.size
    asset = where(oid: uploadable.oid).first_or_initialize(oid: uploadable.oid)

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      transaction do
        asset.update_meta(meta)
        yield asset if block_given?
        asset.reference!(uploadable)
      end
    end

    asset
  end

  # Public: References the given uploadable object to this Asset.  Any
  # Asset::Archive references are deleted too.
  def reference!(uploadable)
    ActiveRecord::Base.connected_to(role: :writing) do
      sql = Asset::Reference.github_sql.new(
        asset_id: id,
        uploadable_id: uploadable.id,
        uploadable_type: uploadable.class.base_class.name,
        created_at: current_time_from_proper_timezone,
      )

      transaction do
        sql.run %Q(
          INSERT INTO asset_references
            (asset_id, uploadable_id, uploadable_type, created_at)
          VALUES
            (:asset_id, :uploadable_id, :uploadable_type, :created_at)
          ON DUPLICATE KEY UPDATE
            asset_id = asset_id
        )

        unarchive!(uploadable)
      end
    end
  end

  # Public: Remove the reference of the given uploadable from this Asset.  If
  # there are no more references, create an Asset::Archive reference so the Asset
  # can be purged later.
  def dereference!(uploadable)
    ref = references.where(
      uploadable_type: uploadable.class.base_class.name,
      uploadable_id: uploadable.id,
    ).first

    transaction do
      ref.destroy if ref
      archive!(uploadable) if uploadable.archive?(self)
    end
  end

  # Internal: Creates an Asset::Archive reference for this Asset, scheduling it to
  # be purged later (if no other references are created).
  #
  # Note: NOT run in a transaction.  This should only be called from the
  # transaction inside #dereference!
  def archive!(uploadable)
    ActiveRecord::Base.connected_to(role: :writing) do
      sql = Asset::Archive.github_sql.new(
        asset_id: id,
        path_prefix: uploadable.alambic_path_prefix,
        created_at: current_time_from_proper_timezone,
      )

      sql.run %Q(
        INSERT INTO asset_archives
          (asset_id, path_prefix, created_at)
        VALUES
          (:asset_id, :path_prefix, :created_at)
        ON DUPLICATE KEY UPDATE
          created_at = :created_at
      )

      reference! archives.order("id DESC").first
    end
  end

  # Internal: Remove any Asset::Archive references for this Asset.
  #
  # Note: NOT run in a transaction.  This should only be called from the
  # transaction inside #reference!
  def unarchive!(uploadable)
    return if uploadable.is_a?(Asset::Archive)
    return unless arc = archives.where(path_prefix: uploadable.alambic_path_prefix).first
    dereference!(arc)
    arc.destroy
  end

  # Public: Remove any Asset::Archive references, and delete this asset if it
  # has no other references.
  def purge!(archives)
    archives = archives.select { |arc| arc.asset_id == id }
    return false if archives.blank?

    transaction do
      Asset::Reference.purge(self, archives)
      Asset::Archive.where(id: archives).delete_all

      if references.count == 0
        destroy
        true
      else
        false
      end
    end
  end

  def image_dimensions?
    width.to_i + height.to_i > 0
  end

  # Public: Returns true if none of the meta data has been set.
  def metadata_empty?
    REQUIRED_METADATA.all? { |k| send(k).nil? }
  end

  # Public: Returns true if all of the meta data has been set.
  def metadata_finished?
    REQUIRED_METADATA.all? { |k| send(k) }
  end

  def self.find_or_init_with_meta(meta)
    oid = meta[:oid]

    ActiveRecord::Base.connected_to(role: :writing) do
      sql = github_sql.new(
        oid: oid,
        size: meta[:size].to_i,
        width: meta[:width].to_i,
        height: meta[:height].to_i,
        created_at: new.send(:current_time_from_proper_timezone),
      )

      transaction do
        sql.run %Q(
          INSERT INTO assets
            (oid, size, width, height, created_at)
          VALUES
            (:oid, :size, :width, :height, :created_at)
          ON DUPLICATE KEY UPDATE
            oid = oid
        )
      end
    end

    find_by_oid(oid)
  end

  # Public: Saves the meta data.  Asset meta data should be fully set during
  # creation, and never updated.
  #
  # meta - Hash of the asset's meta data.
  #
  # Returns nothing.
  def update_meta(meta)
    if set_meta(meta)
      raise ActiveRecord::RecordInvalid, self if invalid?

      ActiveRecord::Base.connected_to(role: :writing) do
        sql = github_sql.new(
          oid: meta[:oid] || oid,
          size: (meta[:size] || size).to_i,
          width: (meta[:width] || width).to_i,
          height: (meta[:height] || height).to_i,
          created_at: GitHub::SQL::NOW,
        )

        transaction do
          sql.run %Q(
            INSERT INTO assets
              (oid, size, width, height, created_at)
            VALUES
              (:oid, :size, :width, :height, :created_at)
            ON DUPLICATE KEY UPDATE
              oid = oid
          )

          # Since we're not using AR's save, if we're acting on an
          # unsaved object, it will not have an id set. So we ask for the one
          # we just geneated by OID
          if self.id.nil?
            sql = "SELECT id FROM assets WHERE oid = :oid"
            self.id = github_sql.values(sql, oid: meta[:oid] || oid).first
          end
        end
      end
    end
  end

  def set_meta(meta)
    return if meta.blank?

    if size = meta[:size]
      self.size = size
    end

    self.width = meta[:width] if width.to_i.zero?
    self.height = meta[:height] if height.to_i.zero?
    changed?
  end

  def set_meta_data(meta, key)
    return unless value = meta[key]
    send("#{key}=", value)
  end

  def meta_data_is_immutable
    old, updated = size_change
    if updated && old && old != updated
      errors.add(:size, "cannot be changed")
    end
  end
end
