# frozen_string_literal: true

class FundingPlatforms
  ALL = {
    github: FundingPlatform::GitHub,
    patreon: FundingPlatform::Patreon,
    open_collective: FundingPlatform::OpenCollective,
    ko_fi: FundingPlatform::KoFi,
    tidelift: FundingPlatform::Tidelift,
    community_bridge: FundingPlatform::CommunityBridge,
    liberapay: FundingPlatform::Liberapay,
    issuehunt: FundingPlatform::IssueHunt,
    otechie: FundingPlatform::Otechie,
    custom: FundingPlatform::Custom,
  }

  def self.find(key)
    return unless key.respond_to?(:to_sym)
    ALL[key.to_sym]
  end

  def self.to_hydro(platform, account)
    {
      platform_type: platform.key.to_s.upcase,
      platform_url: platform.url ? "#{platform.url}#{account}" : account,
    }
  end
end
