# frozen_string_literal: true

class MergeGroupEntry < ApplicationRecord::Collab
  belongs_to :queue, class_name: :MergeQueue, foreign_key: :merge_queue_id
  belongs_to :group, class_name: :MergeGroup, foreign_key: :merge_group_id
  belongs_to :queue_entry, class_name: :MergeQueueEntry, foreign_key: :merge_queue_entry_id

  enum state: {
    pending: 0,
    conflicts: 1,
    check_errors: 2,
    mergeable: 3
  }
end
