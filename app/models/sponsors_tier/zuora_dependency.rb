# frozen_string_literal: true

module SponsorsTier::ZuoraDependency
  ZUORA_PRODUCT_CATEGORY = "sponsorships"
  ZUORA_PRODUCT_TYPE = "sponsorable.sponsors_tier"

  def product_uuid(billing_cycle)
    ::Billing::ProductUUID.find_by(product_type: ZUORA_PRODUCT_TYPE, product_key: id, billing_cycle: billing_cycle)
  end

  # Public: Returns the ProductRatePlanId for this tier and the given cycle in Zuora
  #
  # cycle: "year" or "month" - the billing cycle we want the zuora id for
  #
  # Returns String
  def zuora_id(cycle:)
    product_uuid(cycle).zuora_product_rate_plan_id
  end

  def zuora_charge_ids(cycle:)
    product_uuid(cycle).zuora_product_rate_plan_charge_ids
  end

  def zuora_product_id(cycle:)
    product_uuid(cycle).zuora_product_id
  end

  def zuora_product_name
    "#{sponsors_listing.slug}: #{name}"
  end

  # Public: The charges tied to this tier that will be synced to Zuora
  # These charges are always on a flat rate plan for Sponsors tiers.
  #
  # Returns Array
  def zuora_charges
    [flat_charge]
  end

  # Public: Creates a unique slug based on sponsorable_type and sponsorable_id
  #
  # This provides us a unique, non-changing slug for our product in zuora.
  # Previously, marketplace would use the name and slugified version of the
  # name for custom values in Zuora product listings. With sponsors, we
  # have much more potential for the login and name of the user
  # (or repo name, or org name, to consider the future) to change,
  # so we need to use the sponsorable type and id to meet the aformentioned
  # criteria.
  #
  # Returns a parameterized string
  def zuora_slug
    "sponsors-#{sponsorable_type}-#{sponsorable_id}".parameterize
  end

  # Public: Syncs the tier to Zuora if its ProductUUID records don't exist, and will generate a ProductUUID
  # for each type of billing cycle
  #
  # Returns Array
  def sync_to_zuora
    GitHub::Billing::ZuoraProduct.create \
      product_type: ZUORA_PRODUCT_TYPE,
      product_key: id.to_s,
      product_name: zuora_product_name,
      charges: zuora_charges,
      custom_product_params: custom_product_params
  end

  private

  def flat_charge
    {
      type: :flat,
      prices: { year: yearly_price_in_dollars, month: monthly_price_in_dollars },
    }
  end

  def custom_product_params
    {
      ProductCategory__c: ZUORA_PRODUCT_CATEGORY,
      MaintainerName__c: sponsors_listing.slug,
      MaintainerSlug__c: zuora_slug,
    }
  end
end
