# rubocop:disable Style/FrozenStringLiteralComment

class ReviewRequest < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification

  areas_of_responsibility :pull_requests
  belongs_to :pull_request, touch: true
  belongs_to :reviewer, polymorphic: true
  has_and_belongs_to_many :pull_request_reviews
  has_many   :reasons,
    autosave: true,
    class_name: "ReviewRequestReason",
    dependent: :delete_all,
    inverse_of: :review_request

  validates :pull_request, presence: true
  validates :reviewer, presence: true
  validate  :ensure_requested_team_is_in_valid_org
  validate  :ensure_requested_reviewer_is_a_collaborator, unless: :dismissed?
  validate  :one_pending_request_per_reviewer_and_pull_request

  after_create_commit :instrument_create
  after_create :trigger_review_requested_event, unless: :deferred?
  after_update :trigger_review_request_removed_event, if: :recently_dismissed?, unless: :deferred?
  after_commit :delegate_to_members, on: :create, unless: :deferred?
  after_commit :notify_state_changed, on: :create
  after_commit :notify_pull_request_socket_subscribers
  after_commit :synchronize_search_index

  after_create :set_deferred_flag, if: :deferred?
  after_commit :clear_deferred_flag, on: :update, if: :recently_dismissed?

  delegate :repository, to: :pull_request

  scope :dismissed, -> { where("review_requests.dismissed_at IS NOT NULL") }
  scope :not_dismissed, -> { where("review_requests.dismissed_at IS NULL") }
  scope :type_users, -> { where(reviewer_type: "User") }
  scope :type_teams, -> { where(reviewer_type: "Team") }

  # Public: Finds all requests for one or more reviewers.
  #
  # mixed_reviewers   - One or more reviewers (Users and/or Teams)
  #
  # Returns an ActiveRecord::Relation.
  def self.for(*mixed_reviewers)
    by_type = mixed_reviewers.flatten.group_by { |r| r.class.name }

    conditions = by_type.map do |type, reviewers|
      sanitize_sql_for_conditions(["reviewer_type = ? AND reviewer_id = ?", type, reviewers])
    end

    where(conditions.join(" OR "))
  end

  def self.automated
    where(<<~SQL)
      EXISTS (
        SELECT 1 FROM review_request_reasons
        WHERE
          review_request_reasons.review_request_id = review_requests.id
      )
    SQL
  end

  def self.users
    user_ids = where(reviewer_type: "User").pluck(:reviewer_id)
    return User.none if user_ids.empty?
    User.where(id: user_ids)
  end

  def self.teams
    team_ids = where(reviewer_type: "Team").pluck(:reviewer_id)
    return Team.none if team_ids.empty?
    Team.where(id: team_ids)
  end

  def self.reviewers
    user_reqs, team_reqs = pluck(:reviewer_type, :reviewer_id).partition { |request| request.first == "User" }

    user_ids = user_reqs.map(&:last)
    team_ids = team_reqs.map(&:last)

    [].tap do |reviewers|
      reviewers.concat User.where(id: user_ids) if user_ids.any?
      reviewers.concat Team.where(id: team_ids) if team_ids.any?
    end
  end

  def self.pending
    where(<<-SQL)
      NOT EXISTS (
          SELECT 1 FROM pull_request_reviews_review_requests
          WHERE pull_request_reviews_review_requests.review_request_id = review_requests.id
        )
    SQL
  end

  def self.fulfilled
    where(<<-SQL)
      EXISTS (
          SELECT 1 FROM pull_request_reviews_review_requests
          WHERE pull_request_reviews_review_requests.review_request_id = review_requests.id
        )
    SQL
  end

  # Internal: Used to eager load the GitHub.kv-based deferred flag
  #           for a list of ReviewRequests.
  #
  # This method is temporary while GitHub.kv is used and will be removed
  # once the field is moved to a database column.
  #
  # Returns nothing.
  def self.load_deferred_values(requests)
    keys = requests.map { |req| req.send(:deferred_flag_key) }
    values = GitHub.kv.mget(keys).value { [] }
    requests.each_with_index do |req, ix|
      req.deferred = values[ix] == "1"
    end
  end

  # Public: Setter for the deferred Boolean property.
  #
  # Returns the Boolean-ness of the assigned value.
  def deferred=(value)
    @deferred = !!value
  end

  # Public: Was this request marked as deferred?
  #
  # Returns the value set through the setter, or checks to
  # see if a corresponding key has been set in GitHub.kv.
  #
  # Returns a Boolean.
  def deferred?
    return @deferred if defined?(@deferred)
    @deferred = GitHub.kv.get(deferred_flag_key).value { nil } == "1"
  end

  private def deferred_flag_key
    "#{self.class.name.underscore}-#{id}-deferred"
  end

  private def clear_deferred_flag
    GitHub.kv.del(deferred_flag_key)
  end

  # NOTE: This is a temporary implementaion during staff shipping and will be
  # replaced by a proper database column before GA.
  private def set_deferred_flag
    GitHub.kv.set(deferred_flag_key, "1", expires: 60.days.from_now)
  end

  # Public: Marks a deferred request as ready and triggers appropriate actions.
  #
  # Returns early if the request has been dismissed or has been fulfilled.
  #
  # Will trigger the creation of a review_requested issue event and its related
  # notifications and subscriptions. Will trigger team review request delegation
  # if configured. Sets the deferred property to false.
  #
  # Returns nothing.
  def ready!(user:)
    clear_deferred_flag
    self.deferred = false

    return if dismissed?
    return unless pending?

    transaction do
      trigger_review_requested_event(actor: user)
      delegate_to_members
    end
  end

  # Required for GraphQL abilities loader.
  def hide_from_user?(viewer)
    false
  end

  def pending?
    self.pull_request_reviews.empty?
  end

  def notify_pull_request_socket_subscribers
    pull_request.notify_socket_subscribers
  end

  def synchronize_search_index
    pull_request.synchronize_search_index
  end

  def trigger_review_requested_event(actor: nil)
    pull_request.trigger_review_requested_event(self, actor: actor)
  end

  def dismiss
    self.dismissed_at ||= Time.now
  end

  def dismissed?
    dismissed_at?
  end

  def recently_dismissed?
    dismissed? && saved_change_to_dismissed_at?
  end

  # Only trigger an request_remove event if the issue is still around.
  # We don't want this to fire when the request is being destroyed
  # as a result of an issue being destroyed.
  def trigger_review_request_removed_event
    return unless pull_request.present? && reviewer.present?
    pull_request.trigger_review_request_removed_event(reviewer)
  end

  def ensure_requested_reviewer_is_a_collaborator
    return if reviewer.is_a?(User) && reviewer.ghost?

    if pull_request.user == reviewer
      errors.add :reviewer, "cannot be PR author"
    elsif !pull_request.can_request_review_from?(reviewer)
      errors.add :reviewer, "must be a collaborator"
    end
  end

  def ensure_requested_team_is_in_valid_org
    return if reviewer.is_a?(User)

    if !pull_request.repository.in_organization?
      errors.add :reviewer, "must be in an organization"
    elsif pull_request.repository.organization != reviewer.organization
      errors.add :reviewer, "must be in same organization"
    end
  end

  def one_pending_request_per_reviewer_and_pull_request
    dupes = self.class.pending.not_dismissed.where \
      pull_request_id: pull_request_id,
      reviewer_id: reviewer_id,
      reviewer_type: reviewer_type
    dupes = dupes.where("id <> ?", id) if persisted?
    if dupes.exists?
      errors.add :reviewer, "can only have one pending request per pull request"
    end
  end

  # Public: Builds new reason objects.
  #
  # by_type   - A Hash of reasons grouped by their type.
  #
  # Example
  #
  #   request.reasons_by_type = {
  #     codeowners: [{tree_oid: "0123456", path: "CODEOWNERS", line: 42, pattern: "*"}]
  #   }
  #   => <#ReviewRequestReason codeowners_tree_oid: "0123456", codeowners_path: "CODEOWNERS" …>
  def reasons_by_type=(by_type)
    by_type.each do |type, reasons_array|
      reasons_array.each do |reason_attributes|
        typed_attrs = reason_attributes.reduce({}) do |coll, (attr, value)|
          coll.merge "#{type}_#{attr}" =>  value
        end

        reasons.build(typed_attrs)
      end
    end
  end

  def async_codeowner_reason
    return @async_codeowner_reason if defined?(@async_codeowner_reason)

    @async_codeowner_reason = async_reasons.then do |reasons|
      reasons.find(&:codeowners?)
    end
  end

  def async_as_codeowner?
    async_codeowner_reason.then(&:present?)
  end

  def async_codeowners_path_uri
    async_codeowner_reason.then do |reason|
      reason&.async_codeowners_path_uri
    end
  end

  def async_codeowners_file
    async_codeowner_reason.then do |reason|
      reason&.async_codeowners_file
    end
  end

  private

  # Track requested reviewers so we can match them with suggested reviewers.
  def instrument_create
    GitHub.instrument("review_request.create", {
      dimensions: {
        pull_request_id: pull_request.id,
        actor_id: pull_request.safe_user.id,
        actor_login: pull_request.safe_user.login,
        repository_id: pull_request.repository.id,
        repository_name: pull_request.repository.name,
        repository_has_license: !!pull_request.repository.license,
        repository_public: pull_request.repository.public?,
        repository_owner_type: pull_request.repository.owner.class.name.downcase,
        repository_owner_id: pull_request.repository.owner.id,
        repository_owner_login: pull_request.repository.owner.login,
      },
      context: {
        requested_reviewer_id: reviewer.id,
        requested_reviewer_login: reviewer.name,
      },
    })
  end

  def notify_state_changed
    channel = GitHub::WebSocket::Channels.pull_request_review_state(pull_request)
    GitHub::WebSocket.notify_repository_channel(
      pull_request.repository,
      channel,
      {
        wait: pull_request.default_live_updates_wait,
        pull_request_id: pull_request.id,
      },
    )
  end

  # Perform review request delegation if this review request targets a Team and
  # the team has enabled the setting.
  def delegate_to_members
    return unless reviewer.is_a?(Team)

    if reviewer.review_request_delegation_enabled?
      result = Team::ReviewRequestDelegation.delegate_to_members(
        actor: pull_request.issue.modifying_user,
        pull_request: pull_request,
        team: reviewer,
        strategy: reviewer.review_request_delegation_algorithm.to_sym,
        needed_reviewers: reviewer.review_request_delegation_member_count)

      if !result.success? && !reviewer.review_request_delegation_notify_team?
        pull_request.issue.subscribe_all(
          pull_request.issue.subscribable_team_members(reviewer),
          "review_requested")
      end
    end
  end
end
