# rubocop:disable Style/FrozenStringLiteralComment

# Keeps track of membership in an early access waitlist for new features.
class EarlyAccessMembership < ApplicationRecord::Domain::Users
  include GitHub::Relay::GlobalIdentification

  VALID_MEMBER_TYPES = %w(User Business)
  FEATURE_CHECK_TTL = 30

  # Public: The User, Organization, or Business that has signed up for the early
  # access waitlist.
  belongs_to :member, polymorphic: true, foreign_key: "member_id"
  validates_presence_of :member
  validates :member_type, presence: true, inclusion: { in: VALID_MEMBER_TYPES }
  validates_uniqueness_of :member_id, scope: [:member_type, :feature_slug]

  # Public: The User that signed up for early access. For individuals this will
  # always be equal to user. For Organizations or Businesses, this is the User
  # that performed the registration for the org/enterprise account.
  belongs_to :actor, class_name: "User"
  validates_presence_of :actor
  validate :actor_must_be_a_user
  validate :actor_can_admin_user

  # Public: The survey to be filled out as part of signing up for early access.
  belongs_to :survey
  validates_presence_of :survey

  # Public: Unique slug for the name of the feature for which the member is on
  # the waitlist.
  validates_presence_of  :feature_slug
  validates_inclusion_of(
    :feature_slug,
    in: [
      "code_scanning",
      "github_sponsors",
      "mobile_preview_waitlist",
      "okta_team_sync",
      "reminders",
      "team_sync",
      "testing_only",
      "workspaces",
    ],
  )

  # Public: Early access waitlist for GitHub Sponsors.
  scope :sponsors_waitlist, -> { where(feature_slug: "github_sponsors") }

  # Public: Early access waitlist for GitHub team synchronization.
  scope :team_sync_waitlist, -> { where(feature_slug: "team_sync") }

  # Public: Early access waitlist for GitHub team synchronization.
  scope :okta_team_sync_waitlist, -> { where(feature_slug: "okta_team_sync") }

  # Public: Early access waitlist for Scheduled reminders.
  scope :reminders_waitlist, -> { where(feature_slug: "reminders") }

  # Public: Early access waitlist for GitHub mobile beta
  scope :mobile_beta_waitlist, -> { where(feature_slug: "mobile_preview_waitlist") }

  # Public: Early access waitlist for GitHub code scanning
  scope :code_scanning_waitlist, -> { where(feature_slug: "code_scanning") }

  # Public: Early access waitlist for Workspaces
  scope :workspaces_waitlist, -> { where(feature_slug: "workspaces") }

  # Public: String CSV of email, profile_name, type, login, registered_at for
  # people on the early access list for the feature. who have NOT YET been invited to use
  # the feature.
  #
  #
  # Returns a CSV formatted string.
  def self.feature_email_list(scope, registered_at)
    recipients = Set.new

    memberships = scope.
      joins(:actor).
      includes(:actor, actor: [:primary_user_email, :profile]).
      where(["
        early_access_memberships.created_at >= :created_at",
        created_at: registered_at]).
      map do |membership|
        recipients << membership.actor if membership.actor
      end

    header = %w(Email FullName EncodedUserId UserId).to_csv
    recipients.map do |user|
      [
        user.email,
        user.safe_profile_name,
        User.encode_id(user.id),
        user.id,
      ].to_csv
    end.unshift(header).join
  end

  # Public: Is the member on the waitlist for the feature with the
  # specified slug?
  #
  # feature_slug - The slug for the feature whose waitlist we're checking.
  # account      - A User or Organization.
  #
  # Returns truthy if the account is on the waitlist.
  def self.on_waitlist?(feature_slug, account)
    exists?(feature_slug: feature_slug, member_id: account.id)
  end

  # Public: Is the member on the waitlist for the feature with the
  # specified slug and granted access?
  #
  # This doesn't use `exists?` because it's used by feature flag checks
  # that may be in hot code paths. The additional performance is worth it.
  #
  # feature_slug - The slug for the feature whose waitlist we're checking.
  # account      - A User or Organization.
  #
  # Returns truthy if the account is on the waitlist and enabled.
  def self.member_enabled?(feature_slug, account)
    return false unless account.is_a?(User)

    ActiveRecord::Base.connected_to(role: :reading) do
      binds = {
        member_id: account.id,
        member_type: account.class.name,
        feature_slug: feature_slug,
      }

      res = ApplicationRecord::Domain::Users.github_sql.run(<<-SQL, binds)
        SELECT 1
        FROM early_access_memberships
        WHERE
          member_id = :member_id AND
          member_type = :member_type AND
          feature_slug = :feature_slug AND
          feature_enabled = 1
      SQL

      res.value?
    end
  end

  # Public: Returns a cache key for the provided feature and user.
  # This is used by flipper to cache the results of a member_enabled?
  # to avoid re-querying on every enabled? check.
  #
  # feature_slug - The slug of the feature whose waitlist we're checking.
  # account      - A User or Organization
  #
  # Returns the cache key as a string
  def self.member_enabled_key(feature_slug, account)
    "early_access_membership:#{feature_slug}:#{account.class}:#{account.id}"
  end

  # Public: Has this account already answered the waitlist survey for the
  # specified feature? Surveys can be filled out personally or on behalf of an
  # organization or enterprise account. Each individual should only answer a
  # survey once.
  #
  # feature_slug - The slug for the feature whose survey we're checking.
  # actor        - A User.
  #
  # Returns truthy if the actor has answered the survey.
  def self.answered_survey?(feature_slug, actor)
    return false unless actor
    exists?(feature_slug: feature_slug, actor_id: actor.id)
  end

  # Public: Save a list of answers to survey questions as part of early access
  # registration.
  #
  # answers - Array of Hashes in the form:
  #          [{:question_id => 1, :choice_id => 2, :other_text => ""}, ...]
  #
  # Returns nothing.
  def save_with_survey_answers(answers)
    return save if self.class.answered_survey?(feature_slug, actor)

    answers ||= []

    survey_answers = answers.map do |answer|
      SurveyAnswer.new \
        user_id: actor.id,
        survey_id: survey.id,
        question_id: answer[:question_id],
        choice_id: answer[:choice_id],
        other_text: answer[:other_text]
    end

    if Rails.development? || (survey_answers.size > 0 && survey_answers.all?(&:valid?))
      transaction do
        survey_answers.each(&:save!)
        save
      end
    else
      errors.add(:base, "all questions are required")
      false
    end
  end

  private

  def actor_must_be_a_user
    unless actor && actor.user?
      errors.add(:actor, "must be a user")
    end
  end

  def actor_can_admin_user
    return unless actor && member
    if member.is_a?(Business) && !member.adminable_by?(actor)
      errors.add(:actor, "must be an owner to register enterprise")
    elsif member.is_a?(Organization) && member.organization? && !member.adminable_by?(actor)
      errors.add(:actor, "must be an admin to register organization")
    elsif member.is_a?(User) && member.user? && member != actor
      errors.add(:actor, "can only register their own user")
    end
  end
end
