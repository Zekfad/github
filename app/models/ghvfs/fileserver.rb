# frozen_string_literal: true
class GHVFS::Fileserver < ApplicationRecord::Collab
  self.table_name = "ghvfs_fileservers"

  areas_of_responsibility :git_protocols

  validates :host, presence: true
  validates :online, inclusion: { in: [true, false] }
  validates :embargoed, inclusion: { in: [true, false] }
  validates :evacuating, inclusion: { in: [true, false] }
  validates :ip, presence: true

  def self.add_or_update(options)
    query = <<-SQL
    INSERT INTO ghvfs_fileservers (host, online, embargoed, evacuating, datacenter, ip)
    VALUES (:host, :online, :embargoed, :evacuating, :datacenter, :ip)
    ON DUPLICATE KEY UPDATE
      online = :online,
      embargoed = :embargoed,
      evacuating = :evacuating,
      ip = :ip
    SQL
    options[:datacenter] = GitHub::SQL::NULL if options[:datacenter].nil?
    github_sql.run(query, options)
  end
end
