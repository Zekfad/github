# rubocop:disable Style/FrozenStringLiteralComment

class IntegrationKey < ApplicationRecord::Domain::Integrations
  areas_of_responsibility :ecosystem_apps
  KEY_LENGTH = 2048

  belongs_to :integration
  belongs_to :creator, class_name: "User"

  validates :integration, presence: true
  validates :creator, presence: true

  before_validation :generate_rsa_key, on: :create

  before_destroy :prevent_last_key_deletion

  attr_accessor :private_key
  attr_accessor :skip_generate_key

  def public_key
    @public_key ||= OpenSSL::PKey::RSA.new(public_pem)
  end

  # Match two characters (captured) followed by a third (captured).
  # Use with `gsub` to insert `:` between groups of two characters.
  # http://rubular.com/r/YSlzPIR6cy
  FINGERPRINT_PATTERN = /(.{2})(?=.)/

  # Public: Compute the fingerprint of the integration's public key.
  #
  # Matches the output of `openssl sha1 -c` given a DER-formatted public key.
  #
  # Taken from https://serverfault.com/a/697634/459549:
  #
  #   $ openssl rsa -in path_to_private_key -pubout -outform DER | openssl sha1 -c
  #
  # Returns a fingerprint String.
  def fingerprint
    OpenSSL::Digest::SHA1.hexdigest(public_key.to_der).gsub(FINGERPRINT_PATTERN, '\1:\2')
  end

  private

  def generate_rsa_key
    return if skip_generate_key && !self.public_pem.blank?
    self.private_key = OpenSSL::PKey::RSA.new(KEY_LENGTH)
    self.public_pem = private_key.public_key.to_pem
  end

  def prevent_last_key_deletion
    return if destroyed_by_association.present?
    if integration.public_keys.where("id <> ?", id).none?
      errors.add :base, "You cannot delete the only private key. Generate a new key first."

      throw :abort
    end
  end
end
