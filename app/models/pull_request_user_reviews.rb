# frozen_string_literal: true

class PullRequestUserReviews
  def initialize(pull_request, user)
    @pull_request, @user = pull_request, user
  end

  def reviewed?(path)
    reviewed_paths.include?(path)
  end

  def dismissed?(path)
    dismissed_paths.include?(path)
  end

  private

  def reviewed_paths
    return [] unless @pull_request && @user&.reviewed_files

    @reviewed_paths ||=
      force_encoding(@user.reviewed_files.not_dismissed.where(pull_request_id: @pull_request.id).pluck(:filepath))
  end

  def dismissed_paths
    return [] unless @pull_request && @user&.reviewed_files

    @dismissed_paths ||=
      force_encoding(@user.reviewed_files.dismissed.where(pull_request_id: @pull_request.id).pluck(:filepath))
  end

  def force_encoding(paths)
    paths.map { |path| path.dup.force_encoding(Encoding::UTF_8) }
  end
end
