# frozen_string_literal: true

module Settings
  class AuditLogEntry < ::AuditLogEntry
    METADATA_WHITELIST      = %w(actor user repo created_at note)
    SELF_METADATA_WHITELIST = METADATA_WHITELIST + %w(actor_ip)
  end
end
