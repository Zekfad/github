# frozen_string_literal: true

class CheckAnnotation < ApplicationRecord::Ballast
  extend GitHub::Encoding
  include GitHub::UTF8

  force_utf8_encoding :message, :filename

  areas_of_responsibility :checks

  belongs_to :check_run, inverse_of: :annotations
  belongs_to :check_suite, inverse_of: :annotations

  has_one :code_scanning_alert, dependent: :destroy
  accepts_nested_attributes_for :code_scanning_alert

  alias_attribute :path, :filename
  alias_attribute :annotation_level, :warning_level

  validates_presence_of :path
  validates_presence_of :start_line
  validates_presence_of :end_line
  validates_presence_of :annotation_level
  validates_presence_of :message

  validate :range_validation

  MAX_PER_REQUEST = 25

  MAX_READ_LIMIT = 1_000

  LINE_PADDING = 3

  def github_app
    check_suite.present? ? check_suite.github_app : check_run.github_app
  end

  def title
    utf8(super || default_title)
  end

  def blob_href
    "#{GitHub.url}/#{check_run.repository.name_with_owner}/blob/#{check_run.head_sha}/#{path}"
  end

  def range_validation
    if start_line.present? && end_line.present?
      if start_line > end_line
        errors.add(:start_line, "must be less than or equal to `end_line`")
      end

      if end_line < start_line
        errors.add(:end_line, "must be greater than or equal to `start_line`")
      end
    end

    if start_column.present?
      if !same_line?
        errors.add(:start_column, "can't be provided across multiple lines")
      end
      if end_column.blank?
        errors.add(:end_column, "must be provided if `start_column` is provided")
      elsif start_column > end_column
        errors.add(:start_column, "must be less than or equal to `end_column`")
      end
    end

    if end_column.present?
      if !same_line?
        errors.add(:end_column, "can't be provided across multiple lines")
      end
      if end_column.blank?
        errors.add(:start_column, "must be provided if `end_column` is provided")
      elsif end_column < start_column
        errors.add(:end_column, "must be greater than or equal to `start_column`")
      end
    end
  end

  def line_range
    (start_line - LINE_PADDING)..(end_line + LINE_PADDING)
  end

  private

  def default_title
    line_format = [start_line, end_line].uniq.map { |line| "L#{line}" }.join("-")
    "#{filename}##{line_format}"
  end

  def same_line?
    (end_line - start_line).zero?
  end
end
