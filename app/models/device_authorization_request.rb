# frozen_string_literal: true

class DeviceAuthorizationRequest
  def self.areas_of_responsibility
    [:ecosystem_apps]
  end

  attr_reader :application, :ip_address, :params

  APPLICATION_SUSPENDED = "#{GitHub.developer_help_url}/apps/managing-oauth-apps/troubleshooting-authorization-request-errors/#application-suspended"
  VERIFICATION_URI      = "#{GitHub.url}/login/device"

  def self.process(application, params, ip_address)
    new(application, params, ip_address).process
  end

  def initialize(application, params, ip_address)
    @application = application
    @ip_address  = ip_address
    @params      = params
  end

  def process
    error_response = case
    when application.suspended?
      {
        error: :application_suspended,
        error_description: "Your application has been suspended. #{GitHub.support_link_text}.",
        error_uri: APPLICATION_SUSPENDED,
      }
    when feature_not_enabled?
      {
        error: :unauthorized_client,
        error_description: "This client is not authorized to use the requested grant type.",
        error_uri: GitHub.developer_help_url,
      }
    when invalid_scopes?
      {
        error: :invalid_scope,
        error_description: invalid_scopes_error_description,
        error_uri: GitHub.developer_help_url,
      }
    end

    return error_response if error_response

    grant, error_response = create_device_authorization_grant
    return error_response if error_response

    device_code, error_response = redeem_device_code(grant)
    return error_response if error_response

    {
      device_code: device_code,
      user_code: grant.user_code,
      verification_uri: VERIFICATION_URI,
      expires_in: grant.expires_in,
      interval: ::DeviceAuthorizationGrant::INTERVAL
    }
  end

  private

  def create_device_authorization_grant
    DeviceAuthorizationGrant.create!(application: application, scopes: scopes, ip: ip_address)
  rescue ActiveRecord::RecordInvalid
    return [nil, {
      error: :request_failed,
      error_description: "The request failed to be processed, please try again",
    }]
  end

  def feature_not_enabled?
    !GitHub.flipper[:device_authorization_grant_opt_in].enabled?(application)
  end

  def invalid_scopes
    @_invalid_scopes ||= if !params.key?(:scope) || application.is_a?(Integration)
      []
    else
      names = params[:scope]

      # Take the `scope` param and clean it up
      # without filtering it.
      #
      # From https://github.com/github/egress/blob/722a29f4acfebf66615bd5c675fb31f7935d41c8/lib/egress/dsl.rb
      #
      # Clean, dedupe, filter.
      names = names.to_s.split(/,|\A|\s/).delete_if do |name|
        name.strip!; name.downcase!
        name.nil? || name.empty?
      end.uniq.sort

      OauthAccess.invalid_scopes(names)
    end
  end

  def invalid_scopes?
    invalid_scopes.any?
  end

  def invalid_scopes_error_description
    "The scopes requested are invalid: #{invalid_scopes.to_sentence}."
  end

  def redeem_device_code(device_authorization_grant)
    device_code = device_authorization_grant.redeem_device_code!

    [device_code, nil]
  rescue ActiveRecord::RecordInvalid
    return [nil, {
      error: :request_failed,
      error_description: "The request failed to be processed, please try again",
    }]
  end

  def scopes
    return @scopes if defined?(@scopes)
    @scopes = OauthAccess.normalize_scopes(params[:scope])

    # Stop users from requesting valid hidden scopes on dotcom.
    # See https://github.com/github/appsec-reviews/issues/482 for more details.
    unless GitHub.enterprise?
      valid_hidden_scopes = OauthAccess.valid_hidden_scopes(nil)
      @scopes.reject! { |scope| valid_hidden_scopes[scope] == false }
    end

    @scopes
  end
end
