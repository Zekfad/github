# frozen_string_literal: true

# Securely connect an organization and user to a Slack workspace via the Slack integration.
class ReminderSlackWorkspaceConnector
  CLOCK_SKEW_LEEWAY = 5.minutes
  VERIFIER_EXPIRATION = 1.hour
  VERIFIER_ID = "reminders_state"

  class Error < StandardError
    def code
      self.class.name.underscore.gsub(/_error\z/, "")
    end
  end

  JwtError                     = Class.new(Error)
  StateMissingError            = Class.new(Error)
  StateMismatchError           = Class.new(Error)
  WorkspaceParamsMissingError  = Class.new(Error)
  InsufficientPermissionsError = Class.new(Error)
  SlackIntegrationError        = Class.new(Error)

  def self.encrypt(state)
    Rails.application.message_verifier(VERIFIER_ID).generate(state, expires_in: VERIFIER_EXPIRATION)
  end

  def self.decrypt(encrypted_state)
    Rails.application.message_verifier(VERIFIER_ID).verified(encrypted_state)
  end

  attr_reader :user, :organization
  attr_accessor :workspace, :referring_path, :error
  def initialize(user:, organization:)
    @user = user
    @organization = organization
  end

  def authorization_url(referring_path:, redirect_url:)
    state = {
      "referring_path" => referring_path,
      "user_id" => user.id,
      "organization_id" => organization.id,
      "salt" => SecureRandom.uuid,
      "time" => Time.now.utc.iso8601,
    }

    url = URI(self.class.slack_integration.url)
    url.path = "/slack/oauth/login/both"
    url.query = {
      redirect_uri: redirect_url,
      github_org: organization,
      github_org_id: organization.id,
      state: self.class.encrypt(state),
    }.to_query

    url.to_s
  end

  def connect(jwt)
    workspace_params, _ = begin
      JWT.decode(jwt, self.class.slack_integration.secret, true, {
        verify_iat: true,
        verify_iss: true,
        leeway: CLOCK_SKEW_LEEWAY,
      })
    rescue JWT::DecodeError, JWT::ExpiredSignature
      raise JwtError
    end

    # state is implicitly signed within the JWT
    # Verify the same user/org that left in the original redirect from authorize action
    state = self.class.decrypt(workspace_params["state"])
    self.referring_path = state && state["referring_path"]

    raise SlackIntegrationError.new(workspace_params["error"]) if workspace_params["error"]
    raise StateMissingError if state.nil?
    raise StateMismatchError if state["user_id"] != user.id || state["organization_id"] != organization.id
    raise WorkspaceParamsMissingError if workspace_params["workspace_id"].blank? || workspace_params["workspace_name"].blank?
    raise InsufficientPermissionsError unless sufficient_permissions?(workspace_params["workspace_id"])

    if state["time"]
      delta = Time.now.utc - Time.parse(state["time"])
      GitHub.dogstats.timing("reminders.slack_callback.time", delta)
    end

    self.workspace = ReminderSlackWorkspace.create_or_update_workspace(
      name: workspace_params["workspace_name"],
      remindable: organization,
      slack_id: workspace_params["workspace_id"],
    )

    ReminderSlackWorkspaceMembership.create_or_update_membership(
      user_id: user.id,
      reminder_slack_workspace_id: workspace.id,
    )

    if GitHub.flipper[:scheduled_reminders_migration].enabled?(organization)
      Reminders::TriggerUserMigrationJob.perform_later(
        user,
        organization: organization,
        workspace_id: workspace_params["workspace_id"]
      )
    end
  end

  def self.generate_jwt(payload, expires_in: nil)
    payload["exp"] = expires_in.from_now.to_i if expires_in

    JWT.encode(payload, slack_integration.secret, "HS256")
  end

  def self.slack_integration
    Apps::Internal.integration(:slack) || raise(ArgumentError, "Slack Integration did not exist")
  end

  private

  def sufficient_permissions?(workspace_slack_id)
    return true if organization.adminable_by?(user)

    workspace_already_exists = ReminderSlackWorkspace.where(
      remindable: organization,
      slack_id: workspace_slack_id,
    ).exists?

    organization.member?(user) && workspace_already_exists
  end
end
