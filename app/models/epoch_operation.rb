# frozen_string_literal: true

# A single operation performed by an author during an epoch
class EpochOperation < ApplicationRecord::Collab
  areas_of_responsibility :epochs

  belongs_to :author, class_name: "User"
  belongs_to :epoch

  validates :author, presence: true
  validates :epoch, presence: true
  validates :operation, presence: true
end
