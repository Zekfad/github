# rubocop:disable Style/FrozenStringLiteralComment

class UserEmail < ApplicationRecord::Domain::Users
  include Instrumentation::Model
  include GitHub::Validations
  include Spam::Spammable
  include GitHub::Relay::GlobalIdentification

  GENERIC_DOMAIN_REGEXP = Regexp.union(
    /[@.](example|test|fake|none|vagrant)\.?(com|net|org|vm)?\z/i,
    /[@.]local\.?(host)?\z/i, # catches "joe@localhost", "joe@me.local.host", "joe@host.local"
  )

  TLD_REGEX = /\.[[:word:]-]{2,}\z/

  # Used to match email 'like' addresses that end in a UUID, a common occurance
  # with commits created via a subversion client using our subversion bridge.
  UUID_REGEX = /[\w]{8}(-[\w]{4}){3}-[\w]{12}\z/

  BOT_REGEX = /#{ Regexp.quote Bot::LOGIN_SUFFIX }/

  include UserEmail::MarketingDependency
  include UserEmail::DisposableEmailsDependency

  States = %w( unverified verified )
  attr_writer :allow_stealth
  belongs_to :user
  has_many :email_roles, dependent: :destroy, foreign_key: "email_id"

  has_many :integration_installations,
    foreign_key: :contact_email_id

  has_one :sponsors_membership,
    foreign_key: :contact_email_id

  setup_spammable(:user)

  before_validation :strip_spaces
  before_validation :set_default_state
  before_validation :must_end_with_tld_or_uuid, on: :create, unless: -> { GitHub.enterprise? }

  validates_length_of     :email, within: 3..100
  validate :cannot_create_duplicate_emails
  validates_format_of     :email,
    with: User::EMAIL_REGEX,
    message: "does not look like an email address"
  validates :email, unicode3: true
  validates_inclusion_of  :state, in: States
  validate :cannot_add_github_stealth_emails, unless: :allow_stealth?, on: :create
  validate :cannot_verify_disposable_emails, on: :update, if: :state_changed?

  before_save :set_deobfuscated_email, if: :will_save_change_to_email?
  before_save :set_normalized_domain, if: :will_save_change_to_email?
  after_create :enqueue_set_vertical
  after_create :mark_as_suppressed!, if: :user_suppressed?
  after_update :enqueue_set_vertical, if: :saved_change_to_email?
  after_commit :enqueue_check_for_spam, if: :persisted?
  after_commit :process_email_domain_for_reputation_data, on: [:create, :destroy]
  after_commit :update_gpg_key_emails
  before_destroy :remove_profile_email
  after_create :instrument_disposable_email_created, if: :disposable?

  alias_attribute :to_s, :email
  scope :unverified, -> { where("state <> ? or state IS NULL", "verified") }
  scope :verified, -> { user_entered_emails.where("state = ?", "verified") }
  scope :visible, -> { includes(:email_roles).where("email_roles.role is null or email_roles.public = true").references(:email_roles) }
  # user_entered_emails excludes any emails GitHub adds for system purposes, such as the stealth email
  scope :user_entered_emails, -> { exclude_roles("stealth") }
  scope :not_bouncing, -> { exclude_roles("hard_bounce", "soft_bounce") }
  scope :all_bouncing, -> { includes(:email_roles).where("email_roles.role='hard_bounce'").references(:email_roles) }
  # This is the closest thing we have to 'ordered by created_at', since there aren't timestamps on UserEmails
  scope :ordered_by_id, -> { order(:id) }
  scope :include_roles, -> { includes(:email_roles) }
  # Pull primary emails first (emails with other roles or no assigned role will be sorted beneath)
  scope :primary_first, -> { includes(:email_roles).order(Arel.sql(%q{CASE email_roles.role WHEN "primary" THEN "Z" ELSE email_roles.role END DESC})).references(:email_roles) }
  scope :excluding_ids, -> (*email_ids) { where.not(id: email_ids) }
  scope :contactable, -> { user_entered_emails.verified.primary_first }
  scope :primary, -> { joins(:email_roles).merge(EmailRole.primary) }
  scope :backup, -> { joins(:email_roles).merge(EmailRole.backup) }

  delegate :visibility, :public, :public?, to: :primary_role, allow_nil: true

  extend GitHub::Encoding
  force_utf8_encoding :email, :deobfuscated_email

  # Public: Normalizes email address based on known rules for top
  # providers. It downcases, removes sub-addresses, and performs
  # any local-part normalization.
  #
  # Returns a String.
  def self.normalize(address)
    UserEmail::Normalization.new(address).normalized_address
  end

  # Public: Normalizes email addresses of any number of users and emails.
  # It is safe because it rescues normalization errors caused by passing bad
  # email addresses, leaving you with a list of safe email addresses.
  #
  # Returns an Array of Strings.
  def self.safe_bulk_normalize(users: [], emails: [], verified: true, include_private_emails: true)
    user_emails = email_addresses_for(users, verified: verified, include_private_emails: include_private_emails)
    emails = (user_emails + Array.wrap(emails)).map do |email|
      begin
        normalize(email)
      rescue ArgumentError
      end
    end

    emails.compact
  end

  # Internal: Given a list of users returns a single array containing all email
  # addresses belonging to those users.
  #
  # verified - A Boolean indicating if only verified emails should be returned
  # include_private_emails - A Boolean indicating if private emails should be returned
  #
  # Returns an Array of Strings.
  def self.email_addresses_for(users, verified: false, include_private_emails: true)
    return [] unless users
    Array.wrap(users).flat_map do |user|
      scope = user.emails

      if verified
        scope = scope.verified
      end

      unless include_private_emails
        scope = scope.visible
      end

      scope.map(&:email)
    end
  end

  # Public: List of existing email addresses matching the provided email
  #
  # email: String email
  # exclude_id: Optional UserEmail ID to ignore
  #
  # Returns ActiveRecord Relation
  def self.duplicates(email, exclude_id: nil)
    return [] unless GitHub::UTF8.valid_email?(email)
    dupes = self.where(email: email)
    dupes = dupes.where("user_emails.id <> ?", exclude_id) if exclude_id
    dupes
  end

  # Public: is the email a duplicate of any email in the system?
  #
  # email: String email
  #
  # Returns Boolean
  def self.duplicate?(email)
    duplicates(email).any?
  end

  # Public: is this UserEmail a duplicate of any email in the system?
  #
  # Returns Boolean
  def duplicate?
    self.class.duplicates(email, exclude_id: id).any?
  end

  # Internal: find a single email with the specified String role
  #   ...wish we could do this with a scope, but scopes can't return a single record
  def self.with_role(role)
    include_roles.where("email_roles.role" => role).references(:email_roles).first
  end

  # Internal: Returns the Array of emails that can be used for notifications
  #
  #   This is all verified emails if email verification is enabled and the
  #   user has one or more verified emails. Otherwise, this is all user
  #   entered emails.
  def self.notifiable
    if GitHub.email_verification_enabled?
      verified_emails = not_bouncing.user_entered_emails.verified
      verified_emails.any? ? verified_emails : not_bouncing.user_entered_emails
    else
      not_bouncing.user_entered_emails
    end
  end

  # Public: Check if an email is using a generic domain
  #
  # Used to prevent commit blame using terribly generic email addresses. We do
  # this so that the next person who commits as "root@localhost" or
  # "chris@test.com" doesn't turn around and email support asking why some
  # random person named Chris has access and is committing to their repo.
  #
  #   email - a String containing the email address to test
  #
  # Returns true if the email's domain is generic.
  # This check is disabled (and will always return false) on Enterprise
  # unless specifically enabled by an administrator.
  def self.generic_domain?(email)
    return false unless GitHub.email_detect_generic_domains?
    email =~ GENERIC_DOMAIN_REGEXP
  end

  def self.exclude_roles(*roles)
    subquery = EmailRole.with_roles(*roles).select(:email_id).to_sql
    where("#{table_name}.id NOT IN (#{subquery})")
  end

  # Public: finds a UserEmail for verification belonging to `owned_by` or an
  # organization that is `adminable_by?` owner.
  #
  # For UserEmails not associated directly to `owned_by`, checks that:
  #   - the owner of the UserEmail record is an Organization
  #   - owned_by is an Admin of that Organization
  #
  # Returns nil or a UserEmail record
  def self.find_email_for_verification(id, owned_by:)
    email = owned_by.emails.find_by_id(id)
    return email if email

    if (email = find_by_id(id))
      return nil unless email.user.organization?
      return nil unless email.user.adminable_by?(owned_by)
    end
    email
  end

  # Public: Toggle the visibility of this primary email
  def toggle_visibility
    instrument_toggle_visibility
    transaction do
      primary_role.toggle_visibility
      user.profile.update_attribute(:email, nil) if private? && user.profile
      create_stealth_email_if_needed
      save!
    end
  end

  def remove_profile_email
    return unless user.profile
    if email == user.profile.email
      user.profile.update_attribute(:email, nil)
    end
  end

  # Public: Mark an email as bouncing.  Can either be a hard or soft bounce.
  # Will default to hard if not provided.
  #
  # Hard bounces are permenent, and the email will typically not be reenabled.
  # Soft bounces are temporary, usually due to deferral from the mail server, and
  # will be reenabled.
  #
  # Returns the EmailRole if successful, nil otherwise
  def mark_as_bouncing!(hard_or_soft: :soft, source: :local, status: nil, reason: nil)
    role = "#{hard_or_soft}_bounce"
    return if role?(role)
    role = email_roles.create!(role: role, user: user)
    if role.hard_bounce?
      instrument :hard_bounce, source: source, status: status, reason: reason
      unverify!
    end
    role
  end

  # Internal: mark an email as unverified
  def unverify!
    update! state: "unverified", verified_at: nil
    instrument_unverify
  end

  # Public: Mark an email as suppressed.
  #
  # Unsubscribes the email address from our MailChimp
  # list and ensures the email will not be sent any more
  # marketing email.
  #
  # Returns nothing.
  def mark_as_suppressed!
    return if suppressed? || stealth?
    email_roles.create!(role: "suppressed", user: user)

    if GitHub.mailchimp_enabled?
      MailchimpUnsubscribeJob.perform_later(user.id, self.email)
    end
  end

  # Public: Is the user on the suppression list?
  #
  # Returns a Boolean.
  def user_suppressed?
    SuppressionList.includes_user?(user)
  end

  # Public: Return a map of email address to User instance
  #
  # emails - Array of Strings.
  # limit  - Integer indicating the maximum number of email addresses that this
  #          method should process from the given Array (default: 500).
  #
  # Returns a Hash of :email => User.
  def self.user_map(emails, limit: 500)
    emails = Array(emails).
      compact.
      uniq.
      select { |e| GitHub::UTF8.valid_email?(e) }

    emails = emails.first(limit) if limit

    return {} if emails.empty?

    GitHub.dogstats.histogram("user_email.user_map.emails_count", emails.length)

    user_emails = GitHub.dogstats.time("user_email.user_map.find_emails") do
      UserEmail.where(email: emails).to_a
    end

    GitHub.dogstats.histogram("user_email.user_map.emails_found_count", user_emails.length)

    GitHub.dogstats.time("user_email_duration", tags: ["action:user_map.prefill_users"]) do
      GitHub::PrefillAssociations.prefill_belongs_to(user_emails, User, :user_id)
    end

    user_emails.each_with_object({}) do |user_email, map|
      map[user_email.email.downcase] = user_email.user
    end
  end

  # Internal: Create a stealth email if the current email got marked as 'private' -
  # Delete stealth email if the current email got marked as 'public' and it uses the old stealth format
  # We only do this if there isn't a stealth email already.
  def create_stealth_email_if_needed
    if public?
      user.emails.select { |e| e.role?("stealth") && e.email !~ StealthEmail::STEALTH_EMAIL_REGEX }.each(&:destroy)
    end

    if private? && user.emails.none? { |e| e.role?("stealth") }
      StealthEmail.new(user).save!
    end
  end

  # Internal: should we make this email stealth?
  def allow_stealth?
    @allow_stealth
  end
  private :allow_stealth?

  # Internal: ensure users can't add github stealth emails, whether or not they actually have one
  def cannot_add_github_stealth_emails
    if StealthEmail.stealthy_email?(email)
      errors.add(:email, "cannot add #{email} - use private email address toggle")
    end
  end

  # Internal: strip any whitespace
  def strip_spaces
    self.email = email.strip
  end

  # Public: Does this email have the specified role?
  #
  # role: String name of the EmailRole
  #
  # Returns the EmailRole if found, otherwise nil
  def role?(role)
    email_roles.where(role: role).first
  end

  def primary_role
    role?("primary")
  end

  # Public: Does this email have an EmailRole indicating it is primary?
  def primary_role?
    !!primary_role
  end

  def backup_role
    role?("backup")
  end

  # Public: Does this email have an EmailRole indicating it is backup?
  def backup_role?
    !!backup_role
  end

  def private?
    !public?
  end

  # Public: Does this email have an EmailRole indicating it is either hard bouncing or soft bouncing?
  #
  # Returns the EmailRole if found, or Nil if the email is not bouncing
  def bouncing?
    role?("hard_bounce") || role?("soft_bounce")
  end
  alias_method :bouncing, :bouncing?

  # Shadow 'primary' from the ActiveRecord with the new implementation
  alias_method :primary?, :primary_role?
  alias_method :primary, :primary_role?

  # Public: Does this email have an EmailRole indicated it is suppressed?
  # Returns a Boolean.
  def suppressed?
    email_roles.suppressed.exists?
  end

  # Public: Does this email have a stealth EmailRole?
  # Returns a Boolean.
  def stealth?
    email_roles.stealth.exists?
  end

  # Public: Is this email the only verified address the user has?
  # Returns a Boolean.
  def only_verified_email?
    user.emails.user_entered_emails.verified.excluding_ids(self).none?
  end

  # Public: Is the email the last user-entered email for the user?
  # Returns a Boolean.
  def last_email?
    user.emails.user_entered_emails.size == 1
  end

  # Internal: set a default state if none exists
  def set_default_state
    return if state?
    self.state = "unverified"
  end

  def verified?
    self.state == "verified"
  end

  def unverified?
    self.state == "unverified"
  end

  def cannot_create_duplicate_emails
    errors.add(:email, "is taken") if duplicate?
  end

  # Internal: Does the email end with a Top Level Domain or a UUID (used in
  # subversion commits)?
  #
  # email: email String
  #
  # Returns a boolean.
  def must_end_with_tld_or_uuid
    return if (email =~ TLD_REGEX).present? || (email =~ UUID_REGEX).present?
    errors.add(:email, "does not look like an email address")
  end

  # Internal: Generate token and send verification email, but only
  # if this email has not already been verified
  def request_verification(requested_by: nil, redirect: nil)
    return false if !valid? || verified? || !try_to_verify?
    set_verification_token!

    # This prevents the reminder email verification mailer from being sent if a user requests the email verification
    # email to be sent
    kv_key = "user_id.#{user_id}.user_signup_followup_job"

    if user.organization?
      AccountMailer.organization_email_verification(self, requested_by: requested_by, redirect: redirect).deliver_later
    else
      GitHub.kv.set(kv_key, "true", expires: 2.days.from_now)
      SignupsMailer.email_verification(self).deliver_later
    end

    instrument_request_verification
    true
  end

  # Internal: Send a reminder verification email if user did not verify after initial email
  def request_verification_reminder
    return false if !valid? || verified? || !try_to_verify? || user.organization?
    set_verification_token!

    SignupsReminderMailer.email_verification(self).deliver_later

    instrument_request_verification_reminder
    true
  end

  # Internal: Set the verification_token to a unique thingy
  def set_verification_token
    self.verification_token = SecureRandom.hex(20)
  end

  def set_verification_token!
    set_verification_token && save!
  end

  # Internal: Clear the verification_token
  def clear_verification_token
    self.verification_token = nil
  end

  # Internal: check token and verify the email if it matches
  #
  # Return Boolean indicating if the verification succeeded
  def confirm_verification(token)
    return false unless token
    return false unless verification_token

    previous_primary_email = user.primary_user_email

    if SecurityUtils.secure_compare(verification_token, token)
      clear_verification_token
      verify!
      set_as_primary if only_verified_email?
      instrument_confirm_verification(previous_primary_email)
      true
    end
  end

  # Public: Will we try to verify this email or is it too generic?
  # i.e. "user@domain", "user@domain.local", or "user@disposable-email.biz"
  def try_to_verify?
    return false if disposable? || domain.nil?
    parts = domain.split(".")
    parts.length > 1 && parts.last != "local"
  end

  # Internal: Sets this email as verified
  #
  # Returns nothing.
  def verify!
    mark_as_verified
    self.email_roles.where(role: ["hard_bounce", "soft_bounce"]).destroy_all
    save!
  end

  # Internal: Mark an email as verified.
  #
  # Returns nothing.
  def mark_as_verified
    self.state = "verified"
    self.verified_at = Time.current
  end

  # Internal: Instrument the email verification request.
  #
  # Returns nothing.
  def instrument_request_verification
    GitHub.dogstats.increment "user_email", tags: ["action:verification.request"]
    instrument :request_verification
  end

  # Internal: Instrument the email verification remindeer request.
  #
  # Returns nothing.
  def instrument_request_verification_reminder
    GitHub.dogstats.increment "user_email_reminder", tags: ["action:verification.request.reminder"]
    instrument :request_verification_reminder
  end

  # Internal: Instrument the email verification confirmation.
  #
  # Returns nothing.
  def instrument_confirm_verification(previous_primary_email)
    GitHub.dogstats.increment "user_email", tags: ["action:verification.confirm"]
    instrument :confirm_verification, verified_at: verified_at
    GlobalInstrumenter.instrument "user_email.verify", {
      user: user,
      # Keep actor here so event has same shape as user.add_email, and in case
      # actor could be other than user in another context, such as if we add
      # the ability to verify an email in staff tools
      actor: user,
      previous_primary_email: previous_primary_email,
      current_primary_email: user.primary_user_email,
      verified_email: self,
    }
  end

  # Internal: Instrument email bounces.
  #
  # Returns nothing.
  def instrument_unverify
    GitHub.dogstats.increment "user_email", tags: ["action:verification.unverify"]
    instrument :unverify
  end

  def instrument_toggle_visibility
    if public?
      GitHub.dogstats.increment "user.email.privacy.visibility_toggle.on"
      instrument :toggle_visibility, visibility: "hidden"
    else
      GitHub.dogstats.increment "user.email.privacy.visibility_toggle.off"
      instrument :toggle_visibility, visibility: "visible"
    end
  end

  # Internal: Prefix for auditing / instrumentation
  def event_prefix
    :user_email
  end

  def event_context(prefix: :email)
    {
      prefix => email,
      "#{prefix}_id".to_sym => id,
    }
  end

  # Internal: Default attributes for auditing
  def event_payload
    {
      state: state,
      note: email,
      user: user,
      actor: user,
    }.merge(event_context)
  end

  # The email currently gets checked through User#check_for_spam,
  # so just kick that off.
  def enqueue_check_for_spam
    if previous_changes.include?("email") && user && primary?
      CheckForSpamJob.enqueue(user)
    end
  end

  # See EmailDomainReputationRecord for more details.
  def process_email_domain_for_reputation_data
    EmailDomainReputationRecord.process_later(normalized_domain)
  end

  def self.deobfuscate(email)
    return "" unless email.present?

    mailbox, domain = GitHub::SpamChecker.get_email_parts(email)

    # r.a.l.ph@gmail.com or r.a.l-p-h@gmail.com
    mailbox.tr! ".-", ""

    # ralph+1234ed1@gmail.com
    if mailbox =~ /(\w+)\+.*/
      mailbox = Regexp.last_match(1)
    end

    [mailbox, domain].compact.join("@")
  end

  def set_deobfuscated_email
    self.deobfuscated_email = UserEmail.deobfuscate(email)
    true               # Let's just make sure we don't stop the show here
  end

  def set_normalized_domain
    self.normalized_domain = OrganizationDomain.normalize_domain(domain)
  end

  # Is this email a valid education email address
  def education?
    GitHub::EducationEmail.probably_valid? email
  end

  def enqueue_set_vertical
    UserSetVerticalJob.perform_later(user.id) unless GitHub.enterprise? || user.nil?
  end

  # Update any associated GpgKeyEmails.
  #
  # Returns nothing.
  def update_gpg_key_emails
    # TODO Are emails immutable? Can remove this if so.
    if old_email = previous_changes["email"].try(:first)
      GpgKeyEmail.github_sql.run(<<-SQL, uid: user_id, email: old_email)
        UPDATE gpg_key_emails
        INNER JOIN gpg_keys ON gpg_key_emails.gpg_key_id = gpg_keys.id
        SET user_email_id = NULL
        WHERE gpg_keys.user_id = :uid
        AND gpg_key_emails.email = :email
      SQL
    end

    value = destroyed? ? GitHub::SQL::NULL : self.id
    GpgKeyEmail.github_sql.run(<<-SQL, value: value, uid: user_id, email: email)
      UPDATE gpg_key_emails
      INNER JOIN gpg_keys ON gpg_key_emails.gpg_key_id = gpg_keys.id
      SET user_email_id = :value
      WHERE gpg_keys.user_id = :uid
      AND gpg_key_emails.email = :email
    SQL
  end

  # The email address's domain
  def domain
    return @domain if defined? @domain
    _, domain = GitHub::SpamChecker.get_email_parts(email)
    @domain = domain ? domain.downcase : nil
  end

  # Public: Check if an email has been generated for a Bot.
  #
  # Used to identity a commit author's email address as coming from an Integration's Bot.
  #
  #   email - a String containing the email address to test
  #
  # Returns true if the email has been generated for a Bot.
  def self.belongs_to_a_bot?(email)
    BOT_REGEX.match?(email) && StealthEmail.stealthy_email?(email)
  end

  private

  def set_as_primary
    user.set_primary_email!(self)
  end
end
