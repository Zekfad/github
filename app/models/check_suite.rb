# frozen_string_literal: true
require "github-launch"

class CheckSuite < ApplicationRecord::Ballast
  class NotRerequestableError < StandardError; end
  class AlreadyRerunningError < NotRerequestableError; end

  extend GitHub::BackgroundDependentDeletes

  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model

  include CheckSuite::ActionsDependency

  areas_of_responsibility :checks

  extend GitHub::Encoding
  force_utf8_encoding :head_branch, :name, :action, :workflow_file_path

  # This threshold is a good starting point. Over time and through data analysis,
  # we will reduce/increase this threshold to best serve the community.
  DEFAULT_STALE_THRESHOLD = 2.weeks

  belongs_to :push
  belongs_to :creator, class_name: "User"
  belongs_to :repository
  belongs_to :github_app, class_name: "Integration"
  belongs_to :head_repository, class_name: "Repository"

  has_many :check_runs
  destroy_dependents_in_background :check_runs
  has_many :artifacts
  destroy_dependents_in_background :artifacts
  has_many :annotations, class_name: "CheckAnnotation", inverse_of: :check_suite
  destroy_dependents_in_background :annotations
  has_one :workflow_run, class_name: "Actions::WorkflowRun", inverse_of: :check_suite

  validates_presence_of :github_app_id
  validates_presence_of :repository_id
  validates_presence_of :head_sha
  validates_length_of :external_id, maximum: 64

  after_commit :instrument_completion
  before_save :instrument_status_changed
  before_save :calculate_hidden_flag
  before_create :set_started_at
  after_save :copy_check_suite_fields
  before_destroy :delete_logs_from_file_storage, if: -> { actions_app? }

  after_commit :deliver_workflow_run_complete_notifications, on: [:update]
  after_commit :deliver_notifications, on: [:create, :update]
  after_save_commit :measure_completion
  after_commit :notify_socket_subscribers, on: [:update], if: -> { saved_change_to_attribute?(:completed_log_url) || completed_with_conclusion_change? }
  after_commit :create_workflow_run, on: [:create]

  scope :for_ids, ->(ids) { where(id: ids) }
  scope :for_app_id, ->(id) { where(github_app_id: id) }
  scope :for_app_ids, ->(*ids) { where(github_app_id: ids) }
  scope :group_by_name, -> { group(:name) }
  scope :with_check_run_named, ->(name) { joins(:check_runs).where("check_runs.name = ?", name) }
  scope :most_recent, -> { order("id DESC").limit(25) }
  scope :incomplete_and_older_than_stale_threshold, -> (start_time: Time.zone.now) { where.not(status: :completed).where("updated_at < ?", start_time - DEFAULT_STALE_THRESHOLD) }

  enum status: CheckRun.statuses
  enum conclusion: CheckRun.conclusions

  delegate :name, to: :github_app, prefix: true
  delegate :pusher, :pusher_id, to: :push, allow_nil: true

  # Transient field used to pass the trigger value that will be assigned to the workflow run
  # after the check suite is created
  attr_accessor :trigger

  def self.request(repository:, head_sha:, actor:)
    installations_with_access = IntegrationInstallation.with_resources_on(subject: repository, resources: "checks", min_action: :write)
    return if installations_with_access.blank?

    push = Push.where(after: head_sha, repository_id: repository.id).first

    installations_with_access.each do |installation|
      attrs = {
        github_app_id: installation.integration_id,
        head_sha: head_sha,
        repository_id: repository.id,
      }

      existing_check_suite = CheckSuite.find_by(attrs)
      if existing_check_suite
        existing_check_suite.rerequest(actor: actor)
      else
        attrs.merge!(
          head_branch: push&.branch_name,
          push_id: push&.id,
        )
        check_suite = CheckSuite.create!(attrs)
        check_suite.request(actor: actor)
      end
    end
  end

  def self.find_or_create_for_integrator(attrs)
    record = new(attrs)

    if record.external_id.present?
      existing = record.repository.check_suites.where(
        external_id: record.external_id,
        github_app_id: record.github_app.id,
      ).first

      return IntegratorCreateResult.new(existing, existing: true) if existing
    end

    if !record.github_app.multiple_check_suites_per_sha_enabled?
      same_sha = record.repository.check_suites.where(
        head_sha: record.head_sha,
        github_app_id: record.github_app.id,
      ).exists?

      if same_sha
        record.errors.add(:head_sha, IntegratorCreateResult::DUPLICATE_SHA_MESSAGE)
        return IntegratorCreateResult.new(record)
      end
    end

    record.save

    IntegratorCreateResult.new(record)
  end

  def event_payload
    {
      event_prefix => self,
    }
  end

  def deliver_workflow_run_complete_notifications
    return unless completed_with_conclusion_change? && workflow_run
    # Instrument that the WorkflowRun was requested for downstream consumers.
    notify_workflow_run(workflow_run_id: workflow_run.id, creator: creator, action: :completed)
  end

  def instrument_completion
    if completed_with_conclusion_change?
      instrument :complete
    end
  end

  def measure_completion
    if completed_with_conclusion_change?
      tags = ["conclusion:#{conclusion}", "github_app:#{well_known_app_identifier}"]

      if actions_app?
        # Determine if this was a re-run or not. We don't have an explicit attribute for
        # this just yet, but when we re-run we do create new check runs and hide the old
        # ones. So if we have duplicate check runs for this check suite, it's a re-run.
        if latest_check_runs_count != check_runs.count
          tags << "rerun:true"
        else
          tags << "rerun:false"
        end
      end

      GitHub.dogstats.distribution(
        "check_suite.completed",
        (Time.now.utc.to_i - created_at.to_i) * 1000,
        tags: tags,
      )
    end
  end

  def instrument_status_changed
    return unless status_changed?

    GlobalInstrumenter.instrument "check_suite.status_changed", {
      check_suite_id: id,
      previous_status: status_was.to_sym,
      current_status: status.to_sym,
      repository_id: repository_id,
      head_sha: head_sha,
      conclusion: conclusion,
      app: github_app,
    }
  end

  def rerunnable?
    return false unless check_runs_rerunnable? || rerequestable?
    return false unless completed?
    return false unless actions_app?
    return false if expired_workflow_run?
    true
  end

  def rerequest(actor:, only_failed_check_runs: false, only_failed_check_suites: false)
    raise NotRerequestableError unless rerequestable

    if only_failed_check_runs
      raise NotRerequestableError unless check_runs_rerunnable

      latest_check_runs.each do |run|
        if run.failed?
          run.rerequest(actor: actor)
        end
      end
    else
      raise AlreadyRerunningError if (only_failed_check_suites && !completed?) || (actions_app? && !completed?)
      raise ExpiredWorkflowRunError if expired_workflow_run?
      raise NotRerequestableError if only_failed_check_suites && !failed?
      instrument :rerequest, event_payload.merge(actor_id: actor.id)

      reset
    end
  end

  def request(actor: nil)
    instrument :request, event_payload.merge(
      actor_id: actor&.id,
    )
  end

  def reset
    update(status: :queued, conclusion: nil, started_at: Time.zone.now, completed_at: nil, cancelled_at: nil)
    reset_artifacts
  end

  def completed?
    conclusion.present?
  end

  def failed?
    completed? && StatusCheckRollup::FAILURE_AND_INCOMPLETE_STATES.include?(conclusion)
  end

  # Calculate the rollup values for status and conclusion
  # to represent the entire suite.
  # Based on the lowest hierarchical check run status and conclusion,
  # for all checks in the suite.
  #
  # Returns nothing.
  def set_rollup_values!
    # Make sure all methods use the same array of values to avoid inconsistencies
    # and to be sure that they are read from the same db connection
    check_runs = CheckRun.where(id: fetch_latest_check_run_ids)

    if explicit_completion
      update_after_check_run_change_for_explicit
    else
      rollup_status = calculate_rollup_status(check_runs)
      # The Conclusion, as per documentation, should always be nil in the case that the suite is not complete
      rollup_conclusion = rollup_status == "completed" ? calculate_rollup_conclusion(check_runs) : nil

      # Set the completed_at value to the current time only if the new conclusion is not nil, but the previous one was
      completed_at = calculate_completed_at(rollup_conclusion)

      update(conclusion: rollup_conclusion, status: rollup_status, completed_at: completed_at)
    end

    notify_socket_subscribers
  end

  def duration
    return (completed_at || started_at) - started_at if started_at.present?
    updated_at - created_at
  end

  private def calculate_completed_at(rollup_conclusion)
    return nil if rollup_conclusion.nil?
    return Time.zone.now if conclusion.nil? # conclusion has changed from nil to not-nil

    self.completed_at
  end

  private def update_after_check_run_change_for_explicit
    # Explicitly completed check suites have their conclusion set when marked complete
    return if status == "completed"

    rollup_status = calculate_rollup_status(check_runs)
    if rollup_status == "completed"
      # We must not mark check suites with explicit_completion as completed
      # except after a call to set_complete_explicitly!. So even though all check runs
      # have completed, we keep the status to in_progress / requested
      update status: check_runs.any? ? "in_progress" : "requested"
    else
      update status: rollup_status
    end
  end

  # Marks the check suite as complete, with a known conclusion. Used for integrations that have
  # more knowledge of the state than we do. For instance it might know a suite is going
  # to fail because of a check run that isn't yet in our DB. This avoid check suites visually
  # flapping between success and another state, and multiple notifications/emails being sent.
  def set_complete_explicitly!(conclusion)
    return unless explicit_completion

    update(conclusion: conclusion, status: "completed", completed_at: Time.zone.now)
    notify_socket_subscribers
  end

  # Calculate the status to represent the entire suite.
  # Based on the lowest hierarchical check run status, for all checks in the suite.
  #
  # Returns a string.
  private def calculate_rollup_status(check_runs)
    lowest_run = check_runs.min_by { |run| self.class.statuses[run.status] }
    lowest_run&.status || "requested"
  end

  # Calculate the conclusion to represent the entire suite.
  # Based on the lowest hierarchical check run conclusion, for all checks in the suite.
  #
  # Returns a string or nil.
  private def calculate_rollup_conclusion(check_runs)
    lowest_run = check_runs.
      concluded.
      min_by { |run| CONCLUSIONS_HIERARCHY.index(run.conclusion) }

    lowest_run&.conclusion
  end

  CONCLUSIONS_HIERARCHY = %w(action_required stale timed_out failure cancelled success neutral skipped)

  # Returns the latest check runs for each name.
  #
  # Returns an a CheckRun ActiveRecord::Relation type.
  # If given a block, will pass the unloaded relation into the block as an argument.
  def latest_check_runs
    ActiveRecord::Base.connected_to(role: :reading) do
      latest_check_run_ids = fetch_latest_check_run_ids
      check_runs = latest_check_run_ids.empty? ? CheckRun.none : CheckRun.where(id: latest_check_run_ids)
      check_runs = yield(check_runs) if block_given?
      check_runs.load
    end
  end

  def latest_check_runs_count
    ActiveRecord::Base.connected_to(role: :reading) do
      fetch_latest_check_run_ids.uniq.size
    end
  end

  # Public: Finds open pull requests matching the check suite. A pull
  # request matches a check suite if it contains the head_sha, and
  # its head branch is the head branch of the pull request.
  #
  # Returns an ActiveRecord Relation of PullRequests.
  def matching_pull_requests(viewer = nil)
    return PullRequest.none unless head_branch
    return PullRequest.none unless commit

    all_branch_names   = branch_names(commit)
    branch_names       = all_branch_names.delete(head_branch)

    open_pull_requests = ActiveRecord::Base.connected_to(role: :reading) do
      open_pull_requests(Array(branch_names)).filter_spam_for(viewer)
    end
  rescue GitRPC::ObjectMissing
    PullRequest.none
  end

  def commit
    if repository.commits.exist?(head_sha)
      repository.commits.find(head_sha)
    end
  end

  def short_head_sha
    head_sha.first(Commit::ABBREVIATED_OID_LENGTH)
  end

  def channel
    GitHub::WebSocket::Channels.check_suite(self)
  end

  def async_readable_by?(actor)
    async_repository.then do |repository|
      repository.async_readable_by?(actor)
    end
  end

  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  def permalink
    return workflow_run.permalink if workflow_run
    "#{repository.permalink(include_host: true)}/commit/#{head_sha}/checks?check_suite_id=#{id}"
  end

  def humanized_status
    status.humanize.downcase
  end

  def log_object
    {
      check_suite_id: id,
      check_suite_status: status,
      check_suite_conclusion: conclusion,
      check_suite_created_at: created_at,
      check_suite_updated_at: updated_at,
    }
  end

  def cancelable?
    !completed? && workflow_run? && workflow_file_path?
  end

  def cancel
    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      Failbot.push(check_suite_status: status)

      check_suite_id = GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: global_relay_id)
      grpc_request = GitHub::Launch::Services::Deploy::WorkflowCancelRequest.new(check_suite_id: check_suite_id)

      if github_app.launch_lab_github_app?
        GitHub.launch_lab_deployer.workflow_cancel(grpc_request)
      else
        GitHub.launch_deployer.workflow_cancel(grpc_request)
      end
    end

    if result.call_succeeded?
      update(cancelled_at: Time.zone.now)
    end

    result
  end

  def head_repository_and_branch_name
    return head_branch unless head_repository.present? && head_repository_id != repository_id
    "#{head_repository.owner.login}:#{head_branch}"
  end

  def creator
    super || User.ghost
  end

  def github_app
    super || Integration.ghost
  end

  def user_visible?
    check_runs.any? || explicit_completion || actions_app? # actions app used to make check suites without explicit completion
  end

  def create_workflow_run
    return unless actions_app?

    # Some users use the GITHUB_TOKEN we provide in workflows to create check suites.
    # These check suites appear as created by the launch app, but we don't want to create
    # a workflow run for them. So we check if the check suite has a workflow file path.
    # This field is internal, so cannot be set when the check suite is created by a user.
    return unless workflow_file_path.present?

    workflow = Actions::Workflow.create_or_update_workflow(workflow_file_path, name, repository, nil, event)
    workflow_run = Actions::WorkflowRun.create(check_suite: self, workflow: workflow, trigger: self.trigger)
    notify_workflow_run(workflow_run_id: workflow_run.id, creator: creator, action: :requested)
  end

  def delete_logs_from_file_storage
    check_suite_global_relay_id = Platform::Helpers::NodeIdentification.to_global_id("CheckSuite", id)

    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Artifactsexchange::DeleteBuildLogsRequest.new(
        check_suite_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: check_suite_global_relay_id),
      )
      GitHub.launch_artifacts_exchange_for_check_suite(self).delete_build_logs(grpc_request)
    end

    unless result.call_succeeded?
      raise "Could not delete the logs from file storage"
    end
  end

  def delete_logs(actor:)
    if GitHub.flipper[:delete_logs_from_file_storage].enabled?(repository)
      delete_logs_from_file_storage
    end

    check_runs.each do |check_run|
      check_run.delete_logs
    end

    # This is the last thing since this is the field we use to render the button or not to delete logs.
    # So if the deletion fails in the middle, the button will still be shown
    update(completed_log_url: nil)

    GitHub.instrument "checks.delete_logs", actor: actor, repo: repository, check_suite: self
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def code_scanning_app?
    @is_code_scanning_app ||= if github_app_id.present?
      github_app_id == Apps::Internal.integration_id(:code_scanning)
    else
      false
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def has_reruns
    return false unless started_at
    started_at > created_at
  end

  private

  def completed_with_conclusion_change?
    completed? && saved_change_to_attribute?(:conclusion)
  end

  def fetch_latest_check_run_ids
    binds = {
      head_shas: Array(head_sha),
      repository_id: repository_id,
      check_suites_id: id,
    }

    ids = github_sql.values <<-SQL, binds
      SELECT MAX(check_runs.id)
        FROM check_runs
          JOIN check_suites
          ON check_runs.check_suite_id = check_suites.id
        WHERE check_suites.head_sha IN :head_shas
          AND check_suites.repository_id = :repository_id
          AND check_suites.id = :check_suites_id
        GROUP BY check_runs.name, check_runs.check_suite_id
    SQL
  end

  def notify_socket_subscribers
    data = if new_record?
      {
        timestamp: created_at,
        wait: default_live_updates_wait,
        reason: "check_suite ##{id} created: #{status}",
        log_archive: completed_log_url.present?,
      }
    else
      {
        timestamp: updated_at,
        wait: default_live_updates_wait,
        reason: "check_suite ##{id} updated: #{status}",
        log_archive: completed_log_url.present?,
      }
    end

    GitHub::WebSocket.notify_repository_channel(repository, channel, data)
  end

  # Internal: Memoized list of branch names from GitRPC.
  def branch_names(commit)
    @branch_names ||= repository.rpc.branch_contains(commit.oid)
  rescue GitRPC::ObjectMissing
    []
  end

  # Internal: Any open pull request involving the branches that contain this
  # commit.
  def open_pull_requests(branch_names, limit: 100)
    @open_pull_requests ||= repository.pull_requests_as_head.open_pulls.where(head_ref: Git::Ref.permutations(branch_names)).order(id: :desc).limit(limit)
  end

  # Non-code Actions check suites need to be hidden in most places in the UI
  # to not appear in the merge box in a PR or in the status rollup in a commit
  def calculate_hidden_flag
    self.hidden = event.present? && !VISIBLE_ACTIONS_EVENTS.include?(event)
  end

  def set_started_at
    self.started_at = self.created_at
  end

  # When resetting a suite, we remove any existing Artifacts to avoid duplicates.
  def reset_artifacts
    deleted_artifacts = artifacts.includes(check_suite: [:repository]).destroy_all
    notify_socket_subscribers if deleted_artifacts.count > 0
  end

  def copy_check_suite_fields
    if workflow_run
      workflow_run.copy_check_suite_fields
      workflow_run.save
    end
  end

  # Internal: If check suite was created by a known app, return friendly identifier for capturing telemetry
  def well_known_app_identifier
    return :unknown unless github_app_id

    if actions_app?
      :actions
    else
      case github_app_id
        when 67;    :travis
        when 254;   :codecov
        when 13473; :netlify
        when 18001; :pages
        when 9426;  :azure
        when 3414;  :wip
        when 12526; :sonarcloud
        when 2740;  :renovate
        when 8329;  :now
        when 10529; :google_cloud
        when 14084; :appveyor
        when 11006; :app_center
        when 8306, 10530, 13170; :shopify
        when 31620; :commit_message_linter
        when 12131; :reviewdog
        when 1861;  :dco
        when 3598;  :hound
        when 17324; :lgtm
        when 9695;  :gcb_poc
        when 10562; :mergify
        when 10509; :github_ci
        when 22585; :trilogy
        else;       :other
      end
    end
  end
end
