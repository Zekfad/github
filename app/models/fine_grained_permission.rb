# frozen_string_literal: true

class FineGrainedPermission < ApplicationRecord::Iam
  validates_presence_of :action
  validates_uniqueness_of :action, case_sensitive: false

  has_many :role_permissions, dependent: :destroy
  scope :custom_roles_enabled, -> { where(custom_roles_enabled: true) }

  # Public: returns the input FGPs minus any FGP which is currently disabled.
  # This is a temporary method, to be used during the rollout of the new FGPs.
  # See https://github.com/github/iam/issues/1321 for more information.
  #
  # - fgps: a Symbol or String Array of FGP names.
  #
  # Returns an Array of symbols
  def self.only_enabled_fgps(fgps)
    fgps.map(&:to_sym) - disabled_fgps
  end

  # Public: the list of new FGPs which are currently disabled.
  # This is a temporary method, to be used during the rollout of the new FGPs.
  # See https://github.com/github/iam/issues/1321 for more information.
  #
  # Returns an Array of symbols
  def self.disabled_fgps
    disabled_fgps = []
    disabled_fgps << :delete_issue unless GitHub.flipper[:fgp_delete_issue].enabled?
    disabled_fgps << :manage_deploy_keys unless GitHub.flipper[:fgp_manage_deploy_keys].enabled?
    disabled_fgps << :manage_webhooks unless GitHub.flipper[:fgp_webhook].enabled?
    # these are temporarily disabled for staff-shipping
    disabled_fgps << :manage_topics
    disabled_fgps << :remove_label
    disabled_fgps << :remove_assignee

    disabled_fgps
  end
end
