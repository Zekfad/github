# frozen_string_literal: true

class NewsletterPreview
  include GitHub::AreasOfResponsibility
  areas_of_responsibility :devtools

  include ActiveModel::Model
  include ActiveModel::Validations::Callbacks

  validate :login_is_user
  validates_inclusion_of :period, in: ["one_off", "daily", "weekly", "monthly", "fortnightly"]
  validates_inclusion_of :type, in: ["explore", "vulnerability"]

  attr_accessor :recipient, :period, :type

  def initialize(recipient, period, type)
    @recipient = recipient
    @period = period
    @type = type
  end

  def login_is_user
    if recipient.match(User::EMAIL_REGEX)
      @email = recipient
      login = ApplicationMailer::DEVTOOLS_TEST_USER_LOGIN
    else
      login = recipient
    end

    if login.blank?
      errors.add(:recipient, "can't be blank")
    else
      @user = User.find_by_login(login)
      errors.add(:recipient, "couldn't find a user with login or email: #{login}") if @user.nil?
    end
  end

  def deliver
    NewsletterPreviewJob.perform_later(type, @user.id, period, @email)
  end
end
