# frozen_string_literal: true
class StafftoolsRole < ApplicationRecord::Ballast
  has_many :user_stafftools_roles, dependent: :destroy

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: true }

  def users
    # Users live in a different DB, so has_many through won't work
    @users ||= User.where(id: user_stafftools_roles.pluck(:user_id))
  end
end
