# frozen_string_literal: true

class RefreshToken < ApplicationRecord::Collab
  TOKEN_BYTES = 40

  # Use SHA256 for hashing tokens
  HASHED_TOKEN_DIGEST = Digest::SHA256

  DEFAULT_EXPIRY = 6.months
  MAX_EXPIRY = 6.months

  PREFIX = "r1"

  # Public: The Regexp describing the format of an RefreshToken's string
  # token value.
  TOKEN_PATTERN_R1 = %r{
    \A             # start
    #{PREFIX}      # the token format version
    \.             # a period
    [a-f0-9]{40}   # the random portion of the token
    \z             # end
  }xi

  GRANT_TYPE = "refresh_token"

  attribute :expires_at, :utc_timestamp

  attr_accessor :expiry, :token

  belongs_to :refreshable, polymorphic: true

  def self.valid_grant_type?(type)
    GRANT_TYPE == type
  end

  # Public: Generate a new random token for the access.
  #
  # Returns a base64 String.
  def self.random_token
    suffix = SecureRandom.hex(TOKEN_BYTES)
    "#{PREFIX}.#{suffix}"
  end

  # Public: Hash token for server side persistence.
  #
  # token - A String
  #
  # Returns hashed base64 String.
  def self.hash_token(token)
    HASHED_TOKEN_DIGEST.base64digest(token)
  end

  def self.random_hashed_token
    hash_token(random_token)
  end

  before_save :set_hashed_token
  before_save :set_expires_at

  scope :active, -> { where("expires_at > ?", Time.now.to_i) }

  def self.for_plaintext_token(token)
    return nil if token.nil? || token.empty?

    active.find_by(hashed_token: hash_token(token))
  end

  def redeem
    transaction do
      refreshable.transaction do
        reset_token
        access_token = refreshable.reset_token(bump_expiration: true)

        [access_token, token]
      end
    end
  end

  def expired?
    Time.now > expires_at
  end

  def expires_in
    return 0 if expires_at.nil?
    return DEFAULT_EXPIRY.to_i if new_record?

    expires_at.to_i - Time.now.utc.to_i
  end

  # Public: Reset the refresh token.
  #
  # Returns String token.
  def reset_token
    self.hashed_token = nil
    self.expires_at = nil

    save!

    token
  end

  private

  def set_hashed_token
    return if hashed_token

    self.token = self.class.random_token

    self.hashed_token = self.class.hash_token(token)
  end

  def set_expires_at
    return if expires_at
    self.expiry = DEFAULT_EXPIRY if expiry && expiry > MAX_EXPIRY
    self.expiry ||= DEFAULT_EXPIRY

    self.expires_at ||= expiry.from_now
  end
end
