# frozen_string_literal: true

class DiscussionEvent < ApplicationRecord::Domain::Discussions
  belongs_to :repository, required: true
  belongs_to :discussion, required: true
  belongs_to :actor, class_name: "User"
  belongs_to :comment, class_name: "DiscussionComment"

  has_one :discussion_transfer, foreign_key: "new_discussion_event_id"

  before_validation :set_repository, on: :create

  enum event_type: {
    locked: 0,
    unlocked: 1,
    answer_marked: 2,
    answer_unmarked: 3,
    transferred: 4,
  }

  validates :event_type, presence: true

  scope :by_actor, ->(actor) { where(actor_id: actor) }
  scope :for_discussion, ->(discussion) { where(discussion_id: discussion) }
  scope :for_organization, ->(org) { joins(:discussion).merge(Discussion.for_organization(org)) }
  scope :for_comment, ->(comment) { where(comment_id: comment) }
  scope :for_repository, ->(repo) { where(repository_id: repo) }
  scope :for_comment_authored_by, ->(user) do
    joins(:comment).merge(DiscussionComment.for_user(user))
  end
  scope :still_marked_as_answer, -> do
    answer_marked.joins(:discussion).where("discussions.chosen_comment_id = discussion_events.comment_id")
  end
  scope :newest_first, -> { order("discussion_events.id DESC") }

  # Public: Get the repositories that discussions used to belong to, for the given 'transferred'
  # discussion events, when the given viewer has permission to see those repositories.
  #
  # events - a list of DiscussionEvents
  # viewer - the current authenticated User or nil
  #
  # Returns a Hash of DiscussionEvent ID => Repository/nil.
  def self.old_repositories_for_transfer_events(events:, viewer:)
    result = {}
    promises = events.map do |event|
      event.async_old_repository_for(viewer)
    end
    old_repos = Promise.all(promises).sync
    events.each_with_index do |event, i|
      result[event.id] = old_repos[i]
    end
    result
  end

  # Public: Get the previous repository the discussion belonged to before it was transferred,
  # if this is a transfer event and the given viewer has permission to see that repository.
  #
  # viewer - a User or nil
  #
  # Returns a Promise that resolves to a Repository or nil.
  def async_old_repository_for(viewer)
    return Promise.resolve(nil) unless transferred?

    async_discussion_transfer.then do |transfer|
      next unless transfer

      transfer.async_old_repository.then do |old_repo|
        next unless old_repo

        old_repo.async_readable_by?(viewer).then do |is_readable|
          old_repo if is_readable
        end
      end
    end
  end

  def comment_author
    comment&.user
  end

  def safe_actor
    actor || User.ghost
  end

  def marked_or_unmarked_answer?
    answer_marked? || answer_unmarked?
  end

  def readable_by?(viewer)
    async_readable_by?(viewer).sync
  end

  def async_readable_by?(viewer)
    async_comment.then do |comment|
      if comment
        comment.async_readable_by?(viewer).then do |comment_readable|
          next false unless comment_readable
          async_actor_visible_to?(viewer)
        end
      else
        async_discussion.then do |discussion|
          next false unless discussion

          discussion.async_readable_by?(viewer).then do |discussion_readable|
            next false unless discussion_readable
            async_actor_visible_to?(viewer)
          end
        end
      end
    end
  end

  private

  def async_actor_visible_to?(viewer)
    async_actor.then do |actor|
      next true unless actor
      next false if actor.hide_from_user?(viewer)

      actor.async_blocked_by?(viewer).then do |blocked_by_viewer|
        !blocked_by_viewer
      end
    end
  end

  def set_repository
    return unless discussion
    self.repository = discussion.repository
  end
end
