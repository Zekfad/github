# frozen_string_literal: true

class OrganizationDiscussionPostReply < ApplicationRecord::Collab
  belongs_to :user
  belongs_to :discussion_post, class_name: "OrganizationDiscussionPost",
    foreign_key: "organization_discussion_post_id"

  include OrganizationDiscussionItem

  attr_readonly :number

  before_create :set_number

  def organization=(organization)
    discussion_post.organization = organization
  end

  def organization
    discussion_post.organization
  end

  def async_organization
    async_discussion_post.then do |discussion_post|
      discussion_post.async_organization
    end
  end

  def async_readable_by?(user)
    Promise.resolve(false) unless user
    async_discussion_post.then { |discussion| discussion.async_readable_by?(user) }
  end

  def platform_type_name
    "OrganizationDiscussionComment"
  end

  private

  # Does the given actor have write access to org discussions?
  #
  # actor - Either an IntegrationInstallation or a Bot.
  #
  # Returns a Promise of a Boolean.
  def async_is_integration_with_write_access?(actor)
    Promise.resolve(false) unless actor
    async_discussion_post.then do |discussion|
      discussion.async_is_integration_with_write_access?(actor)
    end
  end

  def set_number
    Sequence.create(discussion_post) unless Sequence.exists?(discussion_post)
    self.number = Sequence.next(discussion_post)
  end
end
