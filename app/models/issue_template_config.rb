# frozen_string_literal: true

class IssueTemplateConfig
  attr_reader :config, :repository

  def initialize(repository:, data:)
    @repository = repository
    @config = read_config(data)
  end

  # if blank_issues_enabled is neither true nor false, default to true
  def blank_issues_enabled?
    return false if config["blank_issues_enabled"] == false
    true
  end

  def configured?
    config.present?
  end

  def contact_links
    return [] if config.empty?
    return [] unless links = config["contact_links"]
    return [] unless links.is_a?(Array)

    links.each_with_object([]) do |link, result|
      return [] unless link.is_a?(Hash)
      contact_link = ContactLink.new(name: link["name"], about: link["about"], url: link["url"], repository: @repository)
      result << contact_link if contact_link.valid?
    end
  end

  class ContactLink
    include EscapeHelper
    attr_reader :name, :about, :repository, :url

    def initialize(name:, about:, url:, repository:)
      @name = name
      @about = about
      @url =  url
      @repository = repository
    end

    def valid?
      name.present? && about.present? && url_valid?
    end

    def async_repository
      Promise.resolve(repository)
    end

    private

    def url_valid?
      safe_link = safe_uri(url)
      if safe_link
        uri = Addressable::URI.parse(url)
        UrlHelper.valid_host?(uri.host)
      else
        false
      end

    rescue Addressable::URI::InvalidURIError
      false
    end
  end

  private

  def read_config(data)
    return {} unless data
    begin
      config = YAML.safe_load(data) || {}
    rescue Psych::BadAlias, Psych::DisallowedClass, Psych::SyntaxError => boom
      return {}
    end
  end
end
