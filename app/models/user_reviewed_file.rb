# frozen_string_literal: true

class UserReviewedFile < ApplicationRecord::Collab
  areas_of_responsibility :pull_requests

  include GitHub::Relay::GlobalIdentification

  belongs_to :pull_request
  belongs_to :user
  has_one :repository, through: :pull_request

  validates :filepath, presence: true
  validates :head_sha, presence: true
  validates :pull_request, presence: true
  validates :user, presence: true
  validates_uniqueness_of :filepath, scope: [:pull_request, :user], case_sensitive: true
  validate :filepath_part_of_pr

  scope :dismissed, -> { where("user_reviewed_files.dismissed IS TRUE") }
  scope :not_dismissed, -> { where("user_reviewed_files.dismissed IS FALSE") }

  def self.for(pull_request)
    where(pull_request_id: pull_request.id)
  end

  def filepath_part_of_pr
    pr_paths = pull_request.changed_paths_for_codeowners.map do |path|
      path.dup.force_encoding(Encoding::UTF_8)
    end

    unless pr_paths.include?(filepath)
      errors.add(:filepath, "must be part of pull request")
    end
  end
end
