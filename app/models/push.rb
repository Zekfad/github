# rubocop:disable Style/FrozenStringLiteralComment

class Push < ApplicationRecord::Ballast
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  extend GitHub::Encoding
  force_utf8_encoding :ref

  has_many :check_suites
  has_many :workflow_runs, as: :trigger

  # Helper for figuring out the commits involved in the push.
  #
  # Requires:
  #
  #   before     - String SHA of the ref before the push.
  #   after      - String SHA of the ref after the push.
  #   ref        - String name of the ref: "refs/heads/master"
  #   repository - The Repository instance for this Push.
  #
  module CommitsHelper
    LARGE_PUSH_THRESHOLD = 2048 # 2k commits

    def commits
      @commits ||= begin
        oids = if deleted?
          []
        elsif created?
          repository.rpc.rev_list(after, reverse: true)
        else
          repository.rpc.rev_list(after, exclude_oids: before, reverse: true)
        end
        oids = oids[oids.size - 1000, 1000] if oids.size > 1000
        repository.commits.find(oids)
      rescue GitRPC::ObjectMissing
        return []
      end
    end

    def ref_is_tag?
      ref.to_s.start_with?("refs/tags/")
    end

    def ref_is_branch?
      ref.to_s.start_with?("refs/heads/")
    end

    def branch_or_tag?
      ref.to_s =~ %r{\Arefs/(?:heads|tags)/}
    end

    def is_note?
      ref.to_s.start_with?("refs/notes/")
    end

    def branch_name
      @branch_name ||= ref.to_s.sub(%r{\Arefs/(?:heads|tags)/}, "")
    end

    alias tag_name branch_name

    def branch_head
      @branch_head ||= "#{ref}@{#{after}}"
    end

    # The branch was created on this push.
    def created?
      before == GitHub::NULL_OID
    end

    # The branch was deleted on this push.
    def deleted?
      after == GitHub::NULL_OID
    end

    # The branch was force-pushed.
    def forced?
      before != merge_base_commit_sha
    end

    # Check if push was a non-fast-forward update.
    #
    # A non-fast-forward means history was lost from the target ref.
    # Ref creations are considered fast-forwards because they only
    # append history.
    #
    # Returns true when update was a non-fast-forward, otherwise false
    # when updated was a fast-forward.
    def non_fast_forward?
      before != GitHub::NULL_OID && before != merge_base_commit_sha
    end

    def large_push?
      commits_pushed_count > LARGE_PUSH_THRESHOLD
    end

    # Common ancestor commit for before and after revs.
    def merge_base_commit_sha
      @merge_base_commit_sha ||= begin
        repository.rpc.merge_base(before, after)
      rescue GitRPC::InvalidObject, GitRPC::InvalidFullOid
        ""
      end
    end

    def commits_pushed_count
      @commits_pushed_count ||=
        case
        when deleted?
          0
        when created?
          begin
            repository.rpc.distinct_commits(branch_head, count: true, new_syntax: true)
          rescue GitRPC::CommandFailed
            0
          end
        else
          begin
            repository.rpc.distinct_commits(branch_head, exclude: [before], count: true, new_syntax: true)
          rescue GitRPC::CommandFailed
            0
          end
        end
    end

    def distinct_commits_pushed
      @distinct_commits_pushed ||= begin
        oids = begin
          repository.rpc.distinct_commits(branch_head, exclude: [before], new_syntax: true)
        rescue GitRPC::CommandFailed
          []
        end
        return [] if oids.blank?
        repository.commits.find(oids).reverse
      end
    end

    def distinct_set
      @distinct_set ||= Set.new(distinct_commits_pushed.map(&:oid))
    end

    def distinct_commit?(commit)
      distinct_set.include?(commit.oid)
    end

    def commits_pushed
      @commits_pushed ||=
        case
        when deleted?
          []
        when created?
          verify_valid_before_and_after_values
          oids = begin
            repository.rpc.distinct_commits(branch_head, new_syntax: true)
          rescue GitRPC::CommandFailed
            []
          end
          repository.commits.find(oids).reverse
        when forced?
          verify_valid_before_and_after_values
          distinct_commits_pushed
        else
          verify_valid_before_and_after_values
          oids = repository.rpc.rev_list(after, exclude_oids: before, reverse: true)
          oids = oids[oids.size - 1000, 1000] if oids.size > 1000
          repository.commits.find(oids)
        end
    end

    class InvalidPushData < StandardError
    end

    def verify_valid_before_and_after_values
      if !GitRPC::Util.valid_full_sha1?(before) || !GitRPC::Util.valid_full_sha1?(after)
        raise InvalidPushData, "Invalid before or after commit oids. Must be 40 char SHA1s."
      end
    end
  end

  include CommitsHelper

  cattr_accessor :commit_threshold
  self.commit_threshold = 20

  belongs_to  :repository
  belongs_to  :pusher, class_name: "User"

  # On create we also save data about this push elsewhere
  after_commit :deliver_notifications, on: :create
  after_commit :track_commit_contributions, on: :create
  after_commit :instrument_push, on: :create
  after_commit :track_push_interaction, on: :create
  after_commit :failbot_null_pusher, on: :create
  after_commit :enqueue_set_license, on: :create, if: :license_changed?
  after_commit :enqueue_community_health_check, on: :create, if: :should_enqueue_community_health_check?
  after_commit :enqueue_bulk_community_health_check, on: :create, if: :should_enqueue_bulk_community_health_check?
  after_commit :enqueue_dependency_manifest_changed_event, on: :create
  after_commit :instrument_push_to_eventer, on: :create
  after_commit :create_check_suites, on: :create, unless: :deleted?
  after_commit :instrument_dependency_graph_snapshot_request, on: :create

  attr_writer :commits

  def self.find_by_id(id)  # rubocop:disable GitHub/FindByDef
    return nil if id < 1
    select("`id`, `repository_id`, `pusher_id`, `before`, `after`, `ref`, `created_at`, `updated_at`").
      where(id: id).
      first
  end

  def self.latest_for(repository, pusher, date, limit = nil)
    where("repository_id = ? AND pusher_id = ? AND created_at >= ?", repository, pusher, date)
      .limit(limit)
      .order("created_at desc")
  end

  def self.latest(repository, date, limit = nil)
    where("repository_id = ? AND created_at >= ?", repository, date)
      .limit(limit)
      .order("created_at desc")
  end

  # Getting the url for a push
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                push.permalink(include_host: false) => `/github/github/compare/sd0979...9sd8fh`
  #
  def permalink(include_host: true)
    "#{repository.permalink(include_host: include_host)}/compare/#{before[0, 10]}...#{after[0, 10]}"
  end
  alias_method :url, :permalink

  def commits_summary
    commits.first(Push.commit_threshold).map do |commit|
      [commit.oid, commit.author_email, commit.message, commit.author_name, distinct_commit?(commit)]
    end
  end

  # Public: Deliver notifications about this push to the relevant users.
  def deliver_notifications(direct_mention_user_ids: nil)
    PullRequestPushNotification.notifications_for(self).each do |pr_push_notification|
      GitHub.newsies.trigger(
        pr_push_notification,
        direct_mention_user_ids: direct_mention_user_ids,
        event_time: pr_push_notification.created_at,
      )
    end
  end
  alias_method :notifications_list, :repository

  def notifications_author
    pusher
  end

  # Public: Finds open pull requests matching the push. A pull
  # request matches a push if its head ref is the branch that was pushed to.
  #
  # Returns a scope for the matching PullRequests.
  def matching_open_pull_requests_to_update
    GitHub.dogstats.time("push", tags: ["action:find_open_pulls"]) do
      refs = Git::Ref.permutations(ref)
      pulls = PullRequest.open_pulls.where(head_repository_id: repository_id, head_ref: refs)

      pulls.reject do |pull|
        pull_comparison = PullRequest::Comparison.find \
          pull: pull,
          start_commit_oid: before,
          end_commit_oid: after,
          base_commit_oid: pull.compare_repository.best_merge_base(pull.base_sha, after)

        # Make sure the before OID is included in the PRs list of changed commits.
        # Otherwise we won't be able to render a diff for the notification.
        reject = pull.spammy? || !pull.changed_commit_oids.include?(before)

        reject ||= pull.draft? && pull.base_repository&.pull_request_revisions_enabled?

        GitHub.dogstats.time("push", tags: ["action:diff_changed_check"]) do
          reject || pull_comparison.nil? || pull_comparison.diffs.empty?
        end
      end
    end
  end

  # Track contributions as needed. The CommitContribution#backfill method will queue up a
  # backfill job if backfill is necessary (i.e., the repo commit contributions is zero and
  # the repo is not a fork). Otherwise that method returns false, in which case we queue up a
  # contributions-track-push job (which fills in the commit contribution data just for this push).
  def track_commit_contributions
    CommitContribution.backfill(repository) || CommitContribution.track_push(self)
  end

  def instrument_push
    AutomaticAppInstallation.trigger(
      type: :file_added,
      originator: self,
      actor: pusher,
    )

    GlobalInstrumenter.instrument("repository.push", {
        repository: repository,
        owner: repository.owner,
        actor: pusher,
        before: before,
        after: after,
        ref: ref,
        changed_files: changed_files || [],
        forced: forced?,
        large: large_push?,
        commit_count: commits_pushed_count,
        commit_oids: (large_push? ? [] : commits.map(&:oid)),
        branch_protection_rule: branch_protection_rule,
        feature_flags: repository.alephd_push_feature_flags }
    )

    if funding_file_changed?
      GitHub.instrument("sponsors.repo_funding_links_file_action", {
        repository: repository,
        actor: pusher,
      })
      GlobalInstrumenter.instrument("sponsors.repo_funding_links_file_action", {
        repository: repository,
        owner: repository.owner,
        actor: pusher,
        change_type: funding_change_type,
        platforms: repository.funding_links_to_hydro,
      })
    end

    if readme_changed? && repository.user_configuration_repository?
      GlobalInstrumenter.instrument("user.profile_readme_action", {
        repository: repository,
        owner: repository.owner,
        actor: pusher,
        change_type: readme_change.change_type,
        readme_body: repository.preferred_readme&.data
      })
    end

    if deleted?
      instrument "refs.destroy"
    elsif created?
      instrument "refs.create"
    else
      instrument :create
    end
  end

  def instrument_push_to_eventer
    Eventer::Push.instrument_push(self)
  end

  def event_payload
    {
      event_prefix => self,
      :repo   => repository,
      :pusher => pusher,
      :ref    => ref,
      :before => before,
      :after  => after,
    }
  end

  def track_push_interaction
    return unless pusher
    instrument :push, prefix: :git
  end

  # Track pushes being recorded with no pusher
  class NullPusherError < StandardError
  end

  def failbot_null_pusher
    return if pusher

    e = NullPusherError.new("no pusher set for push #{id}")
    e.set_backtrace(caller)

    Failbot.report_user_error e,
      repo: repository.id,
      fatal: "NO (just reporting)"
  end

  # Public: Changed files, interpreted as additions, deletions, modifications, renames, etc.
  #
  # decompose_renames - Boolean, when true, treats file renaming as an addition and deletion.
  #
  # See https://git-scm.com/docs/git-diff#_raw_output_format to understand
  # the splitting logic.
  #
  # Returns an Array of ChangedFiles.
  def changed_files(decompose_renames: false)
    @changed_files ||= {}
    @changed_files[decompose_renames] ||= begin
      return [] unless repository
      lines = repository.rpc.raw_diff(before, after)
      return [] if lines.empty?
      [].tap do |changes|
        lines.each do |line|
          rest, path, new_path = line.split("\t")
          _, _, previous_oid, oid, change_type, _ = rest.split

          if decompose_renames && change_type.starts_with?(ChangedFile::RENAMING)
            changes << ChangedFile.new(
              repository: repository,
              ref: ref,
              previous_oid: previous_oid,
              oid: GitHub::NULL_OID,
              change_type: ChangedFile::DELETION,
              path: path,
            )
            changes << ChangedFile.new(
              repository: repository,
              ref: ref,
              previous_oid: GitHub::NULL_OID,
              oid: oid,
              change_type: ChangedFile::ADDITION,
              path: new_path,
            )
          else
            if new_path
              previous_path = path
              path = new_path
            end
            changes << ChangedFile.new(
              repository: repository,
              ref: ref,
              previous_oid: previous_oid,
              oid: oid,
              change_type: change_type,
              path: path,
              previous_path: previous_path,
            )
          end
        end
      end
    end
  rescue ::GitRPC::InvalidRepository
    return nil
  end

  def initial_commit?
    created? && !deleted?
  end

  def on_default_branch?
    return unless ref_is_branch?
    branch_name == repository.default_branch
  end

  def should_enqueue_community_health_check?
    return false unless on_default_branch?

    code_of_conduct_changed? || contributing_changed? || license_changed? || readme_changed? || issue_template_changed? || pull_request_template_changed? || docs_changed? || repository.community_profile && !repository.community_profile.has_outside_contributors
  end

  # Public: Should we queue a bulk health check job because of this push?
  #
  # This is used to update the community profiles of all public, non-fork
  # repositories in an organization if eligible files in global health files
  # repository are changed.
  #
  # Returns a Boolean.
  def should_enqueue_bulk_community_health_check?
    return false unless on_default_branch?
    return false unless repository.global_health_files_repository?

    code_of_conduct_changed? ||
    contributing_changed?    ||
    issue_template_changed?  ||
    pull_request_template_changed?
  end

  def code_of_conduct_changed?
    return false unless repository.detect_code_of_conduct?
    return false if initial_commit?
    return false unless changed_files
    changed_files.any? { |file| PreferredFile.valid_path?(type: :code_of_conduct, path: file.path) }
  end

  def contributing_changed?
    return false unless changed_files
    changed_files.any? { |file| PreferredFile.valid_path?(type: :contributing, path: file.path) }
  end

  def dependency_manifest_changed?
    return false unless repository.content_analysis_enabled?
    return false unless changed_files
    return @dependency_manifest_changed if defined?(@dependency_manifest_changed)

    GitHub.dogstats.time("push.dependency_manifest_changed_check") do
      @dependency_manifest_changed = changed_files.any? do |file|
        DependencyManifestFile.recognized_path?(path: file.path)
      end
    end
  end

  def license_changed?
    RepositoryLicense.push_changed_license?(self)
  end

  def readme_change
    @readme_change ||= begin
      Array(changed_files).detect { |file| PreferredFile.valid_path?(type: :readme, path: file.path) }
    end
  end

  def readme_changed?
    readme_change.present?
  end

  def issue_template_changed?
    return false unless changed_files
    changed_files.any? { |file| PreferredFile.valid_path?(type: :issue_template, path: file.path) }
  end

  def pull_request_template_changed?
    return false unless changed_files
    changed_files.any? { |file| PreferredFile.valid_path?(type: :pull_request_template, path: file.path) }
  end

  def docs_changed?
    return false unless changed_files
    changed_files.any? { |file| file.path.include?("docs/") }
  end

  def funding_change_type
    return false unless changed_files
    @funding_change_type ||= changed_files.detect do |file|
      PreferredFile.valid_path?(type: :funding, path: file.path)
    end&.change_type
  end

  def funding_file_changed?
    funding_change_type.present?
  end

  def enqueue_dependency_manifest_changed_event
    return unless on_default_branch?

    if initial_commit? || repository.detect_dependency_manifests?
      RepositoryDependencyManifestInitializationJob.perform_later(repository.id)
    elsif dependency_manifest_changed?
      RepositoryDependencyManifestChangedJob.perform_later(id)
    end
  end

  def instrument_dependency_graph_snapshot_request
    if dependency_manifest_changed? &&
      !GitHub.enterprise? &&
      GitHub.flipper[:dependency_snapshots].enabled?(repository)

      manifest_files = changed_files(decompose_renames: true).reduce([]) do |manifest_files, file|
        next manifest_files unless DependencyManifestFile.recognized_path?(path: file.path)
        next manifest_files if file.deletion?

        manifest_files << {
          filename: File.basename(file.path),
          path: File.dirname(file.path).sub(/\A\.\z/, ""),
          blob_oid: file.oid
        }
      end

      GlobalInstrumenter.instrument("dependency_graph.request_snapshot", {
        push_id: id,
        before_sha: before,
        sha: after,
        ref: ref,
        pushed_at: created_at,
        owner_name: repository.owner_login,
        repository: repository,
        manifest_files: manifest_files,
      })
    end
  end

  def enqueue_set_license
    RepositorySetLicenseJob.perform_later(repository)
  end

  def enqueue_community_health_check
    CommunityProfile.enqueue_health_check_job(repository)
  end

  def enqueue_bulk_community_health_check
    BulkRepositorySetCommunityHealthFlagsJob.perform_later(repository.owner)
  end

  def create_check_suites
    return if skip_checks?
    CreateCheckSuitesJob.enqueue(push_id: id)
  end

  def skip_checks?
    begin
      commits.last&.skip_checks&.downcase == "true"
    rescue GitRPC::ObjectMissing
      false
    end
  end

  def request_checks?
    begin
      commits.last&.request_checks&.downcase == "true"
    rescue GitRPC::ObjectMissing
      false
    end
  end

  def branch_protection_rule
    return unless ref_is_branch?

    ProtectedBranch.for_repository_with_branch_name(repository, branch_name)
  end

  class ChangedFile
    # https://git-scm.com/docs/git-diff#_raw_output_format
    ADDITION = "A"
    DELETION = "D"
    RENAMING = "R"
    MODIFYING = "M"

    attr_accessor :repository, :ref, :previous_oid, :oid, :change_type, :path, :previous_path, :size

    BigExtensions = Set.new(%w(.dmg .rpm .tar .gz .deb .zip .gz .bz2 .exe .msi .iso))

    def initialize(repository: nil, ref: nil, previous_oid: nil, oid: nil, change_type: nil, path: nil, previous_path: nil)
      @repository = repository
      @ref = ref
      @previous_oid = previous_oid
      @oid = oid
      @change_type = change_type
      @path = path
      @previous_path = previous_path
    end

    alias sha oid
    alias before previous_oid

    def addition?
      ADDITION == change_type
    end

    def deletion?
      DELETION == change_type
    end

    def renaming?
      change_type.starts_with?(RENAMING)
    end

    def big?
      sha != GitHub::NULL_OID &&
        BigExtensions.include?(File.extname(path.to_s))
    end
  end
end
