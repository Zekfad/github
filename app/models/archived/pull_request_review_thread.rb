# frozen_string_literal: true

class Archived::PullRequestReviewThread < ApplicationRecord::Domain::Repositories
  include Archived::Base

  has_many :review_comments, class_name: "Archived::PullRequestReviewComment", dependent: :delete_all
end
