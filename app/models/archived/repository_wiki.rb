# rubocop:disable Style/FrozenStringLiteralComment

class Archived::RepositoryWiki < ApplicationRecord::Domain::Repositories
  include Archived::Base

  belongs_to :repository, class_name: "Archived::Repository"

  def shard_path
    repository.wiki_shard_path
  end

  def unsullied_wiki
    repository.unsullied_wiki
  end

  # dgit_spec is the canonical description for a repository entity within
  # dgit/spokes. It avoids using any PII and encodes network membership.
  def dgit_spec
    "#{network_id}/#{id}.wiki"
  end
end
