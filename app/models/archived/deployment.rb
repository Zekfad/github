# rubocop:disable Style/FrozenStringLiteralComment

class Archived::Deployment < ApplicationRecord::Domain::Repositories
  include Archived::Base

  has_many :deployment_statuses, class_name: "Archived::DeploymentStatus", dependent: :delete_all
end
