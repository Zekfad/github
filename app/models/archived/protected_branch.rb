# rubocop:disable Style/FrozenStringLiteralComment

class Archived::ProtectedBranch < ApplicationRecord::Domain::Repositories
  include Archived::Base

  has_many :required_status_checks, class_name: "Archived::RequiredStatusCheck", dependent: :delete_all
end
