# frozen_string_literal: true

class Archived::RequiredStatusCheck < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
