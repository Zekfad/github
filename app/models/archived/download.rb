# frozen_string_literal: true

class Archived::Download < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
