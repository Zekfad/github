# frozen_string_literal: true

class Archived::PullRequestReview < ApplicationRecord::Domain::Repositories
  include Archived::Base

  has_many :review_threads, class_name: "Archived::PullRequestReviewThread", dependent: :delete_all
end
