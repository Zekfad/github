# rubocop:disable Style/FrozenStringLiteralComment

class Archived::Gist < ApplicationRecord::Domain::Gists
  include Archived::Base
  include ::Gist::Backup
  include ::Gist::LegalHold

  ANONYMOUS_USERNAME = "anonymous"

  belongs_to :user

  # Archive records associations. These must be defined in order for the
  # archive/restore operations to cascade to other record types. Just add new
  # associations following the conventions below.
  has_many :gist_comments,        class_name: "Archived::GistComment",       dependent: :delete_all

  def to_param
    repo_name
  end

  # Public: Finds a gist given an name with owner
  #
  # owner     - The String login of the gist owner
  #             OR the full NWO string if repo_name is omitted.
  # repo_name - (optional) The String repo name for the Gist.
  #
  # Returns a Gist or nil if not found
  def self.with_name_with_owner(owner, repo_name = nil)
    owner, repo_name = owner.to_s.split("/") if repo_name.nil?
    return nil unless owner.present? && repo_name.present?

    if owner == ANONYMOUS_USERNAME
      where("user_id IS NULL AND repo_name = ?", repo_name).first
    else
      where(repo_name: repo_name, user_id: User.where(login: owner).pluck(:id)).first
    end
  end

  def user_param
    user ? user.login : ANONYMOUS_USERNAME
  end

  def name_with_owner(separator = "/")
    [user_param, repo_name].join(separator)
  end

  def visibility
    self["public"] ? "public" : "secret"
  end

  def anonymous?
    user_id.nil?
  end

  # Hard-code what we had as host attribute in case anything depends on this
  # existing.
  def route
    "dgit."
  end

  def shard_path
    GitHub::Routing.gist_storage_path(repo_name: repo_name)
  end

  # Limited use GitRPC::Client object for interacting with the git repository.
  # This should be used for creating / deleting the repository only.
  def rpc
    return @rpc if @rpc
    @rpc = GitRPC.new("chimney-transitional://#{host}#{shard_path}")
  end

  # Check if the repository directly exists on disk on the storage server. This
  # always makes an RPC call and should be used sparingly. Just because a
  # repository exists does not necessarily mean it has any branches, tags, or
  # other objects. The #empty? method is often a much better check for whether a
  # repository is in a useful state.
  #
  # If the repository isn't routed, returns false since there's no repo to check.
  #
  # Returns true when the repository exists.
  def exists_on_disk?
    host && shard_path && rpc.exist?
  end

  # For stafftools compatibility?
  def active?
    false
  end

  # Error thrown when we fail to delete a repository from the backups
  class DeleteError < StandardError
    attr_reader :data

    def initialize(data)
      @data = data
    end
  end

  def repository_spec
    "gist/#{repo_name}"
  end

  # Find archived records that are "purgeable" based on the age of the
  # updated_at attribute. Those repositories whose owner has a legal hold on
  # them are not candidates for pruning.
  #
  # limit - Maximum number of records to return.
  # age   - Time when records should be considered stale.
  #
  # Returns an Array of purgeable Archived::Gist objects.
  def self.find_stale_records(limit, age)
    ActiveRecord::Base.connected_to(role: :reading_slow) do
      stale_records = where("archived_gists.updated_at < ?", age).
        order("archived_gists.updated_at ASC").
        limit(limit).to_a

      return stale_records if stale_records.empty?

      user_ids_with_legal_hold = LegalHold.distinct.pluck(:user_id)
      stale_records.reject do |record|
        user_ids_with_legal_hold.include?(record.user_id)
      end
    end
  end

  # Purge archived records that are stale. By default this destroys the 10
  # oldest archived record that are more than 1 month old. Destroying the
  # records also removes git repositories from archive storage.
  #
  # limit - Maximum number of records to return.
  # age   - Time when records should be considered stale.
  #
  # Returns nothing.
  def self.purge_stale_records(limit = 10, age = 1.month.ago)
    records = find_stale_records(limit, age)
    records.each do |record|
      begin
        throttle do
          record.purge
        end
      rescue GitBackups::DeleteError => e
        # Errors are expected to happen and expected to be transient. We'll try to
        # delete this one the next time the job runs and we want to keep making
        # progress on the rest of the list
        GitHub::Logger.log({fn: "Archived::Gist.purge_stale_records",
                            spec: record.repository_spec,
                            message: e.message})
        next
      end
    end
  end

  def purge
    delete_backup
    destroy
  end

end
