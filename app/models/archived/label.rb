# frozen_string_literal: true

class Archived::Label < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
