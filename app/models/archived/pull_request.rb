# rubocop:disable Style/FrozenStringLiteralComment

class Archived::PullRequest < ApplicationRecord::Domain::Repositories
  include Archived::Base

  # You'll need to add a `has_many :through` association to Archived::Repository for this to work with Archived::Repository#restore
  has_many :reviews, class_name: "Archived::PullRequestReview", dependent: :delete_all
  has_many :review_threads, class_name: "Archived::PullRequestReviewThread", dependent: :delete_all
  has_many :review_comments, class_name: "Archived::PullRequestReviewComment", dependent: :delete_all
  has_many :revisions, class_name: "Archived::PullRequestRevision", dependent: :delete_all
end
