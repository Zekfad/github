# rubocop:disable Style/FrozenStringLiteralComment

class Archived::IssueEvent < ApplicationRecord::Domain::Repositories
  include Archived::Base

  belongs_to :repository, class_name: "Archived::Repository"
  has_one :issue_event_detail, class_name: "Archived::IssueEventDetail", dependent: :delete

  delegate *::IssueEvent::EVENT_DETAIL_ATTRS, to: :issue_event_detail, allow_nil: true

  #TODO: Remove this once data has been transitioned to the new table:
  # `archived_issue_event_details`
  serialize :raw_data, Coders::Handler.new(Coders::IssueEventCoder, compressor: GitHub::ZPack)
end
