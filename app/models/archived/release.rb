# frozen_string_literal: true

class Archived::Release < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
