# frozen_string_literal: true

class Archived::HookEventSubscription < ApplicationRecord::Domain::Hooks
  include Archived::Base
end
