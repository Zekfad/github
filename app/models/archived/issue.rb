# rubocop:disable Style/FrozenStringLiteralComment

class Archived::Issue < ApplicationRecord::Domain::Repositories
  include Archived::Base

  has_many :assignments, class_name: "Archived::Assignment", dependent: :delete_all

  has_many :cards,
    -> { where(content_type: "Issue") },
    foreign_key: :content_id,
    class_name: "Archived::ProjectCard",
    dependent: :delete_all
end
