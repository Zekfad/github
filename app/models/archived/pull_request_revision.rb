# frozen_string_literal: true

class Archived::PullRequestRevision < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
