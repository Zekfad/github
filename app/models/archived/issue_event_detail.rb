# rubocop:disable Style/FrozenStringLiteralComment

class Archived::IssueEventDetail < ApplicationRecord::Domain::Repositories
  include Archived::Base
  belongs_to :issue_event, class_name: "Archived::IssueEvent"
end
