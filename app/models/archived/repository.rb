# rubocop:disable Style/FrozenStringLiteralComment

class Archived::Repository < ApplicationRecord::Domain::Repositories
  include Archived::Base

  include ::Repository::StorageAdapter
  include ::Repository::Backup
  include ::Repository::LegalHold
  extend GitHub::BackgroundDependentDeletes

  belongs_to :owner, class_name: "User"

  # Archive records associations. These must be defined in order for the
  # archive/restore operations to cascade to other record types. Just add new
  # associations following the conventions below.
  has_many :commit_comments,      class_name: "Archived::CommitComment",     dependent: :delete_all
  has_many :downloads,            class_name: "Archived::Download",          dependent: :delete_all
  has_many :pull_requests,        class_name: "Archived::PullRequest"
  destroy_dependents_in_background :pull_requests
  has_many :repository_wikis,     class_name: "Archived::RepositoryWiki",    dependent: :delete_all
  has_many :issues,               class_name: "Archived::Issue"
  destroy_dependents_in_background :issues
  has_many :labels,               class_name: "Archived::Label"
  destroy_dependents_in_background :labels
  has_many :issue_comments,       class_name: "Archived::IssueComment"
  destroy_dependents_in_background :issue_comments
  has_many :issue_events,         class_name: "Archived::IssueEvent"
  destroy_dependents_in_background :issue_events
  has_many :issue_event_details,  through: :issue_events
  has_many :milestones,           class_name: "Archived::Milestone",         dependent: :delete_all
  has_many :releases,             class_name: "Archived::Release",           dependent: :delete_all
  has_many :release_assets,       class_name: "Archived::ReleaseAsset",      dependent: :delete_all

  has_one  :repository_sequence,  class_name: "Archived::RepositorySequence", dependent: :delete

  has_many :protected_branches,   class_name: "Archived::ProtectedBranch"
  destroy_dependents_in_background :protected_branches

  has_many :required_status_checks, through: :protected_branches, class_name: "Archived::RequiredStatusCheck"

  has_many :projects,
    -> { where(owner_type: "Repository") },
    class_name: "Archived::Project",
    dependent: :destroy,
    foreign_key: :owner_id

  has_many :columns, through: :projects, class_name: "Archived::ProjectColumn"
  has_many :issue_cards, through: :issues, class_name: "Archived::ProjectCard", source: :cards
  has_many :note_cards,
    -> { where(content_id: nil) },
    through: :projects,
    class_name: "Archived::ProjectCard",
    source: :cards

  has_many :project_workflows, through: :projects, class_name: "Archived::ProjectWorkflow"
  has_many :project_workflow_actions, through: :projects, class_name: "Archived::ProjectWorkflowAction"

  has_many :deployments, class_name: "Archived::Deployment"
  destroy_dependents_in_background :deployments
  has_many :deployment_statuses, through: :deployments
  destroy_dependents_in_background :deployment_statuses

  has_many :hooks,
    -> { where(installation_target_type: "Repository") },
    class_name: "Archived::Hook",
    dependent: :destroy,
    foreign_key: :installation_target_id

  has_many :hook_config_attributes, through: :hooks
  has_many :hook_event_types,     through: :hooks

  has_many :assignments, through: :issues
  has_many :reviews, through: :pull_requests
  has_many :review_threads, through: :pull_requests
  has_many :review_comments, through: :pull_requests
  has_many :revisions, through: :pull_requests

  has_many :project_repository_links, class_name: "Archived::ProjectRepositoryLink", dependent: :delete_all

  serialize :raw_data, Coders::Handler.new(Coders::RepositoryCoder)
  delegate *Coders::RepositoryCoder.members, to: :raw_data

  scope :owned_by, lambda { |user_or_org| where(owner_id: user_or_org.id) }
  # We do not want to allow users to self-service restoration of repos that were parents or forks of other repos
  # this helps to set scope.
  # Functionally. we are checking for the existence of any Repository attributed to the RepositoryNetwork
  # Since repository networks have many repositories and archived repositories which all share a child parent
  # relationship, if there are ANY Repository's associated with the network, we know that we will not be
  # able to accurately predict the expected location / parentage within a network when a repo is restored.
  scope :network_safe_restoreable, -> { where(parent_id: nil).joins("LEFT OUTER JOIN repository_networks rn ON archived_repositories.source_id = rn.id LEFT OUTER JOIN repositories r ON rn.id = r.source_id")
    .where("r.id IS NULL")
  }

  validate :ensure_repo_absent, on: :restore
  validate :ensure_repo_with_same_name_absent, on: :restore
  validate :ensure_archive_not_in_progress, on: :restore
  validate :ensure_no_fork_in_network, on: :restore

  BATCH_SIZE = 100
  REPO_RESTORE_THRESHOLD = 60

  def self.archive(repo)
    if GitHub.enterprise?
      network_id = repo.network.id
      rrs = GitHub::DGit::Routing.all_repo_replicas(repo.id)
      rrs.each do |rr|
        sql = GitHub::DGit::DB.for_network_id(network_id).SQL.new \
          network_id: network_id,
          repo_id: repo.id,
          repo_type: GitHub::DGit::RepoType::REPO,
          host: rr.host,
          checksum: rr.checksum,
          created_at: rr.created_at,
          updated_at: GitHub::SQL::NOW
        sql.run <<-SQL
          INSERT IGNORE INTO archived_repository_replicas (repository_id, repository_type, host, checksum, created_at, updated_at)
          VALUES (:repo_id, :repo_type, :host, :checksum, :created_at, :updated_at)
        SQL
      end

      wrs = GitHub::DGit::Routing.all_repo_replicas(repo.id, true)
      wrs.each do |wr|
        sql = GitHub::DGit::DB.for_network_id(network_id).SQL.new \
          network_id: network_id,
          repo_id: repo.id,
          repo_type: GitHub::DGit::RepoType::WIKI,
          host: wr.host,
          checksum: wr.checksum,
          created_at: wr.created_at,
          updated_at: GitHub::SQL::NOW
        sql.run <<-SQL
          INSERT IGNORE INTO archived_repository_replicas (repository_id, repository_type, host, checksum, created_at, updated_at)
          VALUES (:repo_id, :repo_type, :host, :checksum, :created_at, :updated_at)
        SQL
      end
    end

    super(repo)
  end

  # Emulates Repository#name_with_owner.
  def name_with_owner
    "#{owner}/#{name}"
  end
  alias nwo name_with_owner

  # dgit_spec is the canonical description for a repository entity within
  # dgit/spokes. It avoids using any PII and encodes network membership.
  def dgit_spec(wiki: false)
    if wiki || dgit_repo_type == GitHub::DGit::RepoType::WIKI
      "#{network_id}/#{id}.wiki"
    else
      "#{network_id}/#{id}"
    end
  end

  def readonly_name_with_owner(separator = "/")
    ActiveRecord::Base.connected_to(role: :reading) { name_with_owner }
  end

  def repository_spec
    "#{network_id}/#{id}"
  end

  def deleted_by_staff?
    !!deleted_by&.staff?
  end

  def deleted_before_time(time)
    Time.at(deleted_at) <= time
  end

  def not_stale
    Time.at(deleted_at) > ::RepositoryPurgeArchivedJob.expiration_period.ago
  end

  # safe_to_restore is currently based on a time since deleted
  # this should be changed as we improve the tracking of
  # background jobs associated with a repository delete job
  # There are also cases where an Archived::Repository will not be purged
  # at the regular interval (legal hold etc) and in cases where a repo
  # was deleted greater than the purge date, we see errors.
  # Anything older than the purge date should not be displayed to avoid that issue
  def safe_to_restore
    deleted_before_time(REPO_RESTORE_THRESHOLD.minutes.ago) && not_stale
  end

  def has_current_restore_job?
    job = GitHub::Jobs::RepositoryRestore.new(id)
    job.message.present?
  end

  # Public: Lock this repository.
  #
  # Used to lock before restoring, so we can toggle visibility or extract from the network
  def lock_excluding_descendants!
    update locked: true, lock_reason: "migrating"
    auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
    GitHub.instrument "staff.repo_lock",
      auditing_actor.merge(
        reason: ("restoring"),
        repo: Repository.new(id: self.id, name: self.name, owner: self.owner))
    true
  end

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the ghost user.
  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

  # User that deleted this repository. This is stored as a serialized
  # attribute when the record is marked as deleted.
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def deleted_by
    @deleted_by ||= deleted_by_user_id && User.find_by_id(deleted_by_user_id)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # Remove the repository from archive storage. See Repository::StorageAdapter
  # for the behavior here, which is different on dotcom and enterprise.
  def purge_disk_storage
    storage_adapter.purge unless ::Repository.find_by_id(self.id)
  end

  # The repository's network_id. This is an alias for source_id.
  def network_id
    source_id
  end

  # RepositoryNetwork for this archived repository. If the network no longer
  # exists because all repositories are archived, an unsaved network is returned
  # with basic storage attributes set based on the network_id.
  def network
    @network ||=
      if network = RepositoryNetwork.find_by_id(network_id)
        network
      else
        network = RepositoryNetwork.new(network_id: network_id)
        network.set_storage_attributes(false)
        network
      end
  end

  # Path to where the archived repository is stored on disk.
  def shard_path
    fail "no shard_path for repo without a network" unless network_id
    fail "no shard_path for repo without an id" unless id

    if network = RepositoryNetwork.find_by_id(network_id)
      "#{network.storage_path}/#{id}.git"
    else
      # Network is no longer available, reconstruct manually. This is needed
      # so there's no query done for available fileservers, which would happen on
      # network.set_storage_attributes if network.storage_path is immediately used.
      base = GitHub::Routing.nw_storage_path(network_id: network_id)
      "#{base}/#{id}.git"
    end
  end

  alias_method :original_shard_path, :shard_path

  # Hard-code to nil instead of network.host as an rpc method still wants to
  # access this.
  def host
    nil
  end

  # Limited use GitRPC::Client object for interacting with the git repository.
  def rpc
    return @rpc if @rpc
    @rpc = build_rpc
  end

  # Even more limited use GitRPC::Client object for removing the git repository.
  def removal_rpc
    return @removal_rpc if @removal_rpc
    @removal_rpc = build_rpc(for_removal: true)
  end

  def build_rpc(for_removal: false)
    if GitHub.enterprise? && GitHub::DGit::Enterprise.archived_replicas(network_id, self.id).count > 0
      delegate_class = for_removal ?
        GitHub::DGit::Delegate::RepositoryRemoval :
        GitHub::DGit::Delegate::Repository
      delegate = delegate_class.new(network_id, id, shard_path, routes: dgit_routes)
      built_rpc = GitRPC.new(
        "dgit:/",
        delegate: delegate,
      )
    else
      built_rpc = GitRPC.new(
        "dgit:/",
        delegate: GitHub::DGit::Delegate::Network.new(network.id, shard_path),
      )
    end
    setup_rpc_options(built_rpc)
  end

  def setup_rpc_options(rpc)
    rpc.options[:info] = { real_ip: GitHub.context[:actor_ip], repo_id: self.id }
    rpc
  end
  private :setup_rpc_options

  def dgit_routes
    GitHub::DGit::Enterprise.archived_replicas(network_id, self.id).map do |rep|
      rep.fileserver.to_route(shard_path)
    end
  end

  def exists_on_disk?
    rpc.exist?
  end

  # Path to where the archived wiki repository is stored on disk.
  def wiki_shard_path
    path = shard_path
    path.sub(/\.git$/, ".wiki.git")
  end

  # Limited use GitRPC::Client object for interacting with the archived wiki's
  # git repository. This should be used for creating / deleting the repository
  # only.
  def wiki_rpc!
    wiki_rpc = ::GitRPC.new("chimney-transitional://#{host}#{wiki_shard_path}")
    setup_rpc_options(wiki_rpc)
  end

  def wiki_removal_rpc
    wiki_rpc!
  end

  # Purge archived records that are stale. By default this destroys the 10
  # oldest archived record that are more than 1 month old. Destroying the
  # records also removes git repositories from archive storage.
  #
  # limit - Maximum number of records to return.
  # age   - Time when records should be considered stale.
  #
  # Returns nothing.
  def self.purge_stale_records(limit = 10, age = 1.month.ago)
    records = find_stale_records(limit, age)
    records.each do |record|
      begin
        Failbot.push(spec: record.repository_spec)
        throttle do
          record.purge
        end
      rescue GitBackups::DeleteError => e
        # Errors are expected to happen and expected to be transient. We'll try to
        # delete this one the next time the job runs and we want to keep making
        # progress on the rest of the list
        GitHub::Logger.log({fn: "Archived::Repository.purge_stale_records",
                            spec: record.repository_spec,
                            message: e.message})
        next
      end
    end
  end

  def purge
    touch
    purge_disk_storage
    purge_stars
    GitHub::SchemaDomain.allowing_cross_domain_transactions { destroy }
  end

  def purge_stars
    scope = Star.where({
      starrable_id: self.id,
      starrable_type: "Repository",
    }).limit(BATCH_SIZE)

    catch :done do
      loop do
        Star.throttle do
          throw :done if scope.delete_all.zero?
        end
      end
    end
  end

  # Find archived records that are "purgeable" based on the age of the
  # updated_at attribute. Those repositories whose owner has a legal hold on
  # them are not candidates for pruning.
  #
  # limit - Maximum number of records to return.
  # age   - Time when records should be considered stale.
  #
  # Returns an Array of purgeable Archived::Repository objects.
  def self.find_stale_records(limit, age)
    ActiveRecord::Base.connected_to(role: :reading_slow) do
      stale_records = where("archived_repositories.updated_at < ?", age).
        order("archived_repositories.updated_at ASC").
        limit(limit).to_a

      return stale_records if stale_records.empty?

      user_ids_with_legal_hold = LegalHold.distinct.pluck(:user_id)
      stale_records.reject do |record|
        user_ids_with_legal_hold.include?(record.owner_id)
      end
    end
  end

  def unsullied_wiki(force = false)
    @unsullied_wiki = nil if force
    @unsullied_wiki ||= begin
      GitHub::Unsullied::Wiki.new(self)
    end
  end

  def initialize_wiki(*)
  end

  private

  def ensure_repo_absent
    if Repository.find_by(id: id, deleted: false)
      errors.add(:repo_exists, "Repository already restored.")
      return false
    end

    true
  end

  def ensure_repo_with_same_name_absent
    if owner.repositories.find_by_name(name)
      errors.add(:repo_with_same_name_exists, "Repository already exists on this account.")
      return false
    end

    true
  end

  def ensure_archive_not_in_progress
    archive_job_status = GitHub::Jobs::RepositoryDelete.status(id)
    if archive_job_status&.started?
      errors.add(:archive_in_progress, "Cannot restore this repository while a deletion is in progress.")
      return false
    end

    true
  end

  def ensure_no_fork_in_network
    if network && network.root && network.find_fork_for(owner).present?
      errors.add(:fork_exists_in_network, "This account has an existing fork in the deleted repository's network.")
      return false
    end

    true
  end
end
