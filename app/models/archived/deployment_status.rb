# frozen_string_literal: true

class Archived::DeploymentStatus < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
