# frozen_string_literal: true

class ReservedLogin < ApplicationRecord::Ballast
  include Instrumentation::Model

  validates_presence_of :login
  validates_format_of :login, with: User::LOGIN_REGEX
  validates_uniqueness_of :login, message: "has already been reserved", case_sensitive: false

  before_save :already_reserved?

  before_validation :downcase_login
  after_destroy_commit :instrument_destroy
  after_create_commit :instrument_create

  default_scope { order(login: :asc) }

  class << self
    def reserved?(login)
      hardcoded?(login) || ReservedLogin.exists?(login: login.downcase)
    end

    def hardcoded?(login)
      GitHub::DeniedLogins.include? login.downcase
    end
  end

  def hardcoded?
    return @hardcoded if defined? @hardcoded
    @hardcoded = ReservedLogin.hardcoded? login
  end

  def add_info(reason = nil , user)
    self.reason = reason
    self.user = user
  end

  private

  def downcase_login
    self.login = self.login.dup.downcase if self.login
  end

  def already_reserved?
    errors.add :base, "Login #{login} is invalid" if ReservedLogin.reserved?(login)
  end

  def instrument_create
    instrument :create, login: login, reason: Audit.context[:reason]
  end

  def instrument_destroy
    instrument :destroy, login: login
  end
end
