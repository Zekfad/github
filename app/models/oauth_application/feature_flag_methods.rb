# rubocop:disable Style/FrozenStringLiteralComment

# See https://githubber.com/article/technology/dotcom/feature-flags
module OauthApplication::FeatureFlagMethods
  # Does this OAuth Application belong to the "github" org or to a GitHub
  # staff member?
  #
  # Returns a Boolean.
  def preview_features?
    return false unless GitHub.preview_features_enabled?
    github_owned?
  end

  # Is codesearch disabled for the current OAuth Application? This feature
  # flag is used to block applications that are negatively impacting the
  # Elasticsearch cluster.
  #
  # see https://github.com/devtools/feature_flags/disable_codesearch
  #
  # Returns a Boolean.
  def codesearch_disabled?
    GitHub.flipper[:disable_codesearch].enabled?(self)
  end
end
