# frozen_string_literal: true

# Alias the class while we are renaming to RequiredStatusCheck
ProtectedBranchStatus = RequiredStatusCheck
