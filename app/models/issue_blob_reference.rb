# frozen_string_literal: true

class IssueBlobReference < ApplicationRecord::Domain::Repositories
  belongs_to :issue

  validates_presence_of :blob_oid, :commit_oid, :issue_id, :filepath, :range_start, :range_end
  validates_numericality_of :range_start, greater_than: 0
  validates_numericality_of :range_end, greater_than: 0
  validates :blob_oid, length: { is: 40 }
  validates :commit_oid, length: { is: 40 }

  validate :ensure_valid_blob, if: :can_validate_blob?
  validate :ensure_valid_commit, if: :can_validate_commit?
  validate :ensure_valid_range, if: :can_validate_range?

  def ensure_valid_blob
    repo = self.issue.repository
    if !repo.rpc.object_exists?(self.blob_oid)
      errors.add :blob_oid, "must be a valid and visible blob"
    end
  end

  def ensure_valid_commit
    repo = self.issue.repository
    if !repo.commits.exist?(self.commit_oid)
      errors.add :commit_oid, "must be a valid and visible commit"
    end
  end

  def ensure_valid_range
    if self.range_start > self.range_end
      errors.add :range_start, "cannot be greater than range_end"
    end
  end

  def can_validate_blob?
    self.issue && !self.blob_oid.nil?
  end

  def can_validate_commit?
    self.issue && !self.commit_oid.nil?
  end

  def can_validate_range?
    !self.range_start.nil? && !self.range_end.nil?
  end

  # Internal: Report data about the issue_blob_reference creation to Octolytics.
  #
  # issue_blob_reference - The IssueBlobReference that has been created.
  #
  # Returns nothing.
  def enqueue_report_creation(referencer)
    context = {
      user_id: referencer.id.to_s,
      user_login: referencer.login,
    }
    IssueBlobReferenceCreationJob.perform_later(id, context)
  end
end
