# frozen_string_literal: true
class Integration
  class PermissionsEditor
    attr_reader :integration, :new_permissions_and_events

    # Result from the IntegrationPermissionsEditor
    class Result
      class Error < StandardError; end

      def self.success(integration) new(:success, integration: integration) end
      def self.failed(error) new(:failed, error: error) end

      attr_reader :error, :integration

      def initialize(status, integration: nil, error: nil)
        @status      = status
        @integration = integration
        @error       = error
      end

      def success?
        @status == :success
      end

      def failed?
        @status == :failed
      end
    end

    def self.perform(integration:, permissions_and_events:)
      new(integration: integration, permissions_and_events: permissions_and_events).perform
    end

    def initialize(integration:, permissions_and_events:)
      @integration                = integration
      @new_permissions_and_events = permissions_and_events
    end

    def perform
      old_version = integration.latest_version

      if integration.update(new_permissions_and_events)
        version = integration.reload.latest_version
        UpgradeIntegrationInstallationVersionJob.perform_later(integration.id, old_version.number, version.number)
      else
        raise Result::Error, integration.errors.full_messages.join("\n")
      end

      Result.success(integration)
    rescue Result::Error => e
      Result.failed e.message
    end
  end
end
