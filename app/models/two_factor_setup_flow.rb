# rubocop:disable Style/FrozenStringLiteralComment

class TwoFactorSetupFlow
  include GitHub::AreasOfResponsibility

  attr_accessor :current_user, :tmp_two_factor_credential

  def initialize(current_user)
    @current_user = current_user
    #instantiate tmp_two_factor_credential using the secret/recovery_secret
    #found in kv. If there aren't any values in kv, then the user hasn't started
    #the setup flow yet and a secret/recovery secret will be generated for them
    #in the flow (or preserved from a previous credential).
    @tmp_two_factor_credential = get_tmp_two_factor_credential(secret, recovery_secret, provider)
  end

  # TwoFactorSetupFlow is created in places where we aren't sure that the user
  # has even initiated two-factor setup. After kicking off two-factor setup
  # all of the below values should be present.
  def pending?
    secret.present? && recovery_secret.present? && type.present?
  end

  #set new type/secret/recovery values and delete old sms/otp_verified values
  def new_setup(type)
    @tmp_two_factor_credential = get_tmp_two_factor_credential
    @tmp_two_factor_credential.generate_secrets!
    GitHub.kv.set("two_factor_setup_type:#{current_user.id}", "#{type}", expires: 1.hour.from_now)
    GitHub.kv.set("two_factor_setup_secret:#{current_user.id}", "#{@tmp_two_factor_credential.secret}", expires: 1.hour.from_now)
    GitHub.kv.set("two_factor_setup_recovery_secret:#{current_user.id}", "#{@tmp_two_factor_credential.plaintext_recovery_secret}", expires: 1.hour.from_now)
    GitHub.kv.del("two_factor_setup_sms_number:#{current_user.id}")
    GitHub.kv.del("two_factor_setup_provider:#{current_user.id}")
  end

  def clear_kv_values
    GitHub.kv.del("two_factor_setup_type:#{current_user.id}")
    GitHub.kv.del("two_factor_setup_secret:#{current_user.id}")
    GitHub.kv.del("two_factor_setup_recovery_secret:#{current_user.id}")
    GitHub.kv.del("two_factor_setup_sms_number:#{current_user.id}")
    GitHub.kv.del("two_factor_setup_provider:#{current_user.id}")
  end

  def secret
    GitHub.kv.get("two_factor_setup_secret:#{current_user.id}").value!
  end

  def recovery_secret
    GitHub.kv.get("two_factor_setup_recovery_secret:#{current_user.id}").value!
  end

  def sms_number
    GitHub.kv.get("two_factor_setup_sms_number:#{current_user.id}").value!
  end

  def save_sms_number(number)
    GitHub.kv.set("two_factor_setup_sms_number:#{current_user.id}", "#{number}", expires: 1.hour.from_now)
    @tmp_two_factor_credential.sms_number = number
  end

  def provider
    GitHub.kv.get("two_factor_setup_provider:#{current_user.id}").value!
  end

  def save_provider(provider)
    GitHub.kv.set("two_factor_setup_provider:#{current_user.id}", "#{provider.provider_name}", expires: 1.hour.from_now)
    @tmp_two_factor_credential.provider = provider.provider_name
  end

  def type
    GitHub.kv.get("two_factor_setup_type:#{current_user.id}").value!
  end

  def get_tmp_two_factor_credential(secret = false, recovery_secret = false, provider = nil)
    old_credential = current_user.two_factor_credential
    credential = TwoFactorCredential.new(user_id: current_user.id)
    credential.secret = secret if secret
    credential.plaintext_recovery_secret = recovery_secret if recovery_secret
    credential.provider = provider if provider
    credential.recovery_used_bitfield = 0

    # Preserve the user's previous backup SMS number to the new credential,
    # unless the user is a GitHub employee.
    if old_credential
      @reconfiguring_two_factor_authentication = true
      credential.backup_sms_number = old_credential.backup_sms_number unless current_user.employee?
    end
    credential
  end

  def enable_two_factor
    tmp_two_factor_credential.recovery_codes_viewed = true

    cred_saved = false
    User.transaction do
      current_user.add_or_replace_two_factor_credential tmp_two_factor_credential
      cred_saved = if type == "sms"
        if current_user.two_factor_credential.configure_with_sms(sms_number)

          # We have already validated the number by sending a confirmation code via
          # Twilio, but it is possible that they manually messed with the form since
          # then, so we should validate it again with a final message.
          message = "You have successfully configured #{GitHub.flavor} two-factor " +
            "authentication. You will receive two-factor codes at " +
            "this number."
          GitHub::SMS.send_message(sms_number, message)
          true
        else
          false
        end
      else
        current_user.two_factor_credential.configure_with_app
      end
    end

    if cred_saved
      AccountMailer.two_factor_enable(current_user.two_factor_credential, @reconfiguring_two_factor_authentication).deliver_later
      clear_kv_values
    end
    cred_saved
  end
end
