# frozen_string_literal: true

module SponsorsListing::ZuoraDependency
  include GitHub::Billing::ZuoraProduct::ZuoraSettings

  ZUORA_PRODUCT_CATEGORY = "sponsorships"
  ZUORA_PRODUCT_TYPE = "sponsorable.sponsors_listing"

  # The unit cost for each rate plan will be 1 USD.
  # We will leverage chargeOverrides to change the sponsorship amount.
  UNIT_COST = "1"

  def sync_to_zuora
    return unless GitHub.billing_enabled?
    return unless approved?

    create_zuora_product
    create_rate_plans
  end

  def product_key(billing_cycle:)
    "#{id}-#{billing_cycle}"
  end

  def zuora_slug
    "sponsors-#{sponsorable.class}-#{sponsorable_id}".parameterize
  end

  private

  attr_reader :zuora_product_id

  def create_zuora_product
    result = GitHub.zuorest_client.create_product(
      {
        Name: slug,
        EffectiveStartDate: GitHub::Billing.today.to_s,
        EffectiveEndDate: EFFECTIVE_END_DATE,
        ProductCategory__c: ZUORA_PRODUCT_CATEGORY,
        MaintainerName__c: sponsorable.login,
        MaintainerSlug__c: zuora_slug,
      },
    )
    @zuora_product_id = result["Id"]
  end

  # Metadata on one-time, monthly recurring, and yearly recurring rate plan charges
  def rate_plan_charges
    [
      {
        billing_cycle: "one_time",
        charge_type: "OneTime",
        charge_name: "#{zuora_slug}: One-Time",
        plan_name: "#{zuora_slug}: One-Time",
      },
      {
        billing_cycle: User::MONTHLY_PLAN,
        charge_type: "Recurring",
        charge_name: "#{zuora_slug}: Recurring #{User::MONTHLY_PLAN}",
        plan_name: "#{zuora_slug}: Recurring #{User::MONTHLY_PLAN}",
      },
      {
        billing_cycle: User::YEARLY_PLAN,
        charge_type: "Recurring",
        charge_name: "#{zuora_slug}: Recurring annual", # for parity
        plan_name: "#{zuora_slug}: Recurring #{User::YEARLY_PLAN}",
      },
    ]
  end

  def create_rate_plans
    rate_plan_charges.each do |options|
      billing_cycle = options.fetch(:billing_cycle)
      charge_type   = options.fetch(:charge_type)
      charge_name   = options.fetch(:charge_name)
      plan_name     = options.fetch(:plan_name)

      product_key = product_key(billing_cycle: billing_cycle)
      next if Billing::ProductUUID.exists?(product_type: ZUORA_PRODUCT_TYPE, product_key: product_key)

      zuora_product_rate_plan = create_zuora_product_rate_plan(plan_name)
      zuora_product_rate_charges = create_zuora_product_rate_plan_charges(
        charge_name, billing_cycle, charge_type, zuora_product_rate_plan["Id"]
      )

      Billing::ProductUUID.create!(
        product_type: ZUORA_PRODUCT_TYPE,
        product_key: product_key,
        billing_cycle: billing_cycle,
        zuora_product_id: zuora_product_id,
        zuora_product_rate_plan_id: zuora_product_rate_plan["Id"],
        zuora_product_rate_plan_charge_ids: {
          flat: zuora_product_rate_charges[0]["Id"],
        },
      )
    end
  end

  def create_zuora_product_rate_plan(plan_name)
    GitHub.zuorest_client.create_product_rate_plan(
      EffectiveStartDate: GitHub::Billing.today.to_s,
      EffectiveEndDate: EFFECTIVE_END_DATE,
      Name: plan_name,
      ProductId: zuora_product_id,
    )
  end

  def create_zuora_product_rate_plan_charges(charge_name, billing_cycle, charge_type, rate_plan_id)
    billing_period = billing_cycle == User::YEARLY_PLAN ? "Annual" : "Month"

    GitHub.zuorest_client.create_action(
      type: "ProductRatePlanCharge",
      objects: [
        default_zuora_rate_plan_charge_params.merge(
          BillingPeriod: billing_period, # This will be Month for One Time as no other values are applicable
          ChargeType: charge_type,
          Name: charge_name,
          ProductRatePlanId: rate_plan_id,
        )
      ]
    )
  end

  def default_zuora_rate_plan_charge_params
    {
      ChargeModel: "Flat Fee Pricing", # We will modify prices through overrides
      DeferredRevenueAccount: DEFERRED_REVENUE_ACCOUNT,
      RecognizedRevenueAccount: RECOGNIZED_REVENUE_ACCOUNT,
      TaxCode: TAX_CODE,
      TaxMode: "TaxExclusive",
      Taxable: false,
      TriggerEvent: "ContractEffective",
      ProductRatePlanChargeTierData: {
        ProductRatePlanChargeTier: [
          {
            Currency: "USD",
            Price: UNIT_COST,
          },
        ],
      },
    }
  end
end
