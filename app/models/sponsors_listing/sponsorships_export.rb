# frozen_string_literal: true

class SponsorsListing::SponsorshipsExport
  include ActionView::Helpers::NumberHelper
  include ActiveModel::Validations

  # We have Sponsors transaction records starting this year.
  START_YEAR = 2018

  attr_reader :sponsors_listing, :year, :month, :format, :contact_email, :content

  validates :sponsors_listing, presence: true
  validates :year, inclusion: { in: Date.today.year.downto(START_YEAR), message: "%{value} is not a valid year" }
  validates :month, inclusion: { in: Date::MONTHNAMES[1..12], message: "%{value} is not a valid month" }
  validates :format, inclusion: { in: %w(json csv), message: "%{value} is not a valid format" }
  validates :contact_email, presence: { message: "no contact email for your Sponsors account" }

  def initialize(sponsors_listing:, year:, month:, format:)
    @sponsors_listing = sponsors_listing
    @year = year.to_i
    @month = month.to_s
    @format = format.to_s.downcase
    @contact_email = sponsors_listing&.sponsors_membership&.contact_email_address
    @content = ""
  end

  def start_export_job
    return unless valid?

    ExportSponsorshipsJob.perform_later(
      @sponsors_listing.sponsorable,
      year: year,
      month: month,
      format: format,
    )
  end

  def fetch_content
    return unless valid?
    json? ? fetch_content_as_json : fetch_content_as_csv
  end

  def filename
    if json?
      "#{base_filename}.json"
    else
      "#{base_filename}.csv"
    end
  end

  def mime_type
    if json?
      "application/json"
    else
      "text/csv"
    end
  end

  private

  CSV_HEADERS = [
    "Sponsor Handle",
    "Sponsor Profile Name",
    "Sponsor Public Email",
    "Sponsorship Started On",
    "Is Public?",
    "Is Yearly?",
    "Transaction ID",
    "Tier Name",
    "Tier Monthly Amount",
    "Processed Amount",
    "Is Prorated?",
    "Status",
    "Transaction Date",
  ].freeze

  def fetch_content_as_json
    sponsorships.map do |ship|
      {
        sponsor_handle: ship.sponsor.login,
        sponsor_profile_name: ship.sponsor.profile_name,
        sponsor_public_email: ship.sponsor.publicly_visible_email(logged_in: true),
        sponsorship_started_on: ship.created_at,
        is_public: ship.privacy_public?,
        is_yearly: ship.sponsor.yearly_plan?,
        transactions: transactions_for(ship.sponsor).map do |line_item|
          {
            transaction_id: line_item.billing_transaction.transaction_id,
            tier_name: line_item.subscribable.name,
            tier_monthly_amount: number_to_currency(line_item.subscribable.monthly_price_in_dollars),
            processed_amount: number_to_currency(line_item.amount_in_cents / 100),
            is_prorated: line_item.billing_transaction.prorated_charge?,
            status: line_item.billing_transaction.last_status,
            transaction_date: line_item.created_at,
          }
        end,
      }
    end.to_json
  end

  def fetch_content_as_csv
    GitHub::CSV.generate(encoding: Encoding::UTF_8) do |csv|
      csv << CSV_HEADERS

      sponsorships.each do |ship|
        transactions_for(ship.sponsor).each do |line_item|
          csv << [
            ship.sponsor.login,
            ship.sponsor.profile_name,
            ship.sponsor.publicly_visible_email(logged_in: true),
            ship.created_at,
            ship.privacy_public?,
            ship.sponsor.yearly_plan?,
            line_item.billing_transaction.transaction_id,
            line_item.subscribable.name,
            number_to_currency(line_item.subscribable.monthly_price_in_dollars),
            number_to_currency(line_item.amount_in_cents / 100),
            line_item.billing_transaction.prorated_charge?,
            line_item.billing_transaction.last_status,
            line_item.created_at,
          ]
        end
      end
    end
  end

  def tiers
    @tiers ||= sponsors_listing.sponsors_tiers.to_a
  end

  def sponsorships
    @sponsorships ||= sponsors_listing
      .sponsorable
      .sponsorships_as_sponsorable
      .preload(:sponsor)
  end

  def transactions_for(sponsor)
    Billing::BillingTransaction::LineItem
      .includes(:billing_transaction)
      .preload(:subscribable)
      .where(billing_transactions: { user_id: sponsor.id })
      .where(subscribable: tiers)
      .where(created_at: time_range)
      .order(created_at: :desc)
  end

  def base_filename
    "#{sponsors_listing.sponsorable.login}-sponsorships-#{month}-#{year}"
  end

  def time_range
    return @time_range if defined?(@time_range)

    parsed_date = DateTime.strptime("#{month} #{year}", "%B %Y")
    @time_range = parsed_date.beginning_of_month..parsed_date.end_of_month
  end

  def json?
    format == "json"
  end
end
