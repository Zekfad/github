# frozen_string_literal: true

# Code scanning specific functionality for check runs
module CheckRun::CodeScanningDependency
  CODE_SCANNING_DEFAULT_CHECK_RUN_NAME = "Results"

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def create_for_code_scanning_analysis(repository:, commit_oid:, tool_names: nil)
      check_suite = Checks::Service.find_or_create_check_suite(
        head_sha: commit_oid,
        github_app_id: Apps::Internal.integration_id(:code_scanning),
        repo: repository,
      )
      return unless check_suite.present?

      if check_suite.name.blank?
        check_suite.update(
          name: "Code scanning",
          check_runs_rerunnable: false,
          rerequestable: false,
        )
      end

      check_run_names = if tool_names.present?
        tool_names.map { |tool_name| CodeScanning::Tool.from_name_or_display_name(tool_name).display_name }
      else
        [CODE_SCANNING_DEFAULT_CHECK_RUN_NAME]
      end

      check_runs = check_run_names.uniq.map do |name|
        attrs = { name: name }
        retry_on_find_or_create_error do
          check_suite.check_runs.find_by(attrs) || check_suite.check_runs.create(attrs)
        end
      end

      check_runs.compact
    end

    def for_code_scanning_analysis(repository:, commit_oid:)
      check_suite = repository.check_suites.find_by(
        head_sha: commit_oid,
        github_app_id: Apps::Internal.integration_id(:code_scanning),
      )
      if check_suite.present?
        check_suite.latest_check_runs
      else
        none
      end
    end
  end

  def code_scanning_tool_name
    if name != CODE_SCANNING_DEFAULT_CHECK_RUN_NAME
      CodeScanning::Tool.from_name_or_display_name(name).name
    end
  end

  def update_for_code_scanning_diff!(new_alerts, fixed_alerts, base_ref_name, errors)
    summarizer = CodeScanningDiffSummarizer.new(repository, new_alerts, fixed_alerts, base_ref_name, errors)
    self.summary = summarizer.summary
    self.title = summarizer.title
    self.conclusion = summarizer.conclusion
    self.save!
  end

  class CodeScanningDiffSummarizer
    include ActionView::Helpers::NumberHelper

    attr_reader :repository, :new_alerts, :fixed_alerts, :base_ref_name

    def initialize(repository, new_alerts, fixed_alerts, base_ref_name, errors)
      @repository = repository
      @new_alerts = new_alerts
      @fixed_alerts = fixed_alerts
      @base_ref_name = base_ref_name
      @diff_errors = errors
    end

    def conclusion
      error_count > 0 ?
        "failure" :
        @diff_errors.size > 0 ?
          "neutral" :
          "success"
    end

    def title
      segments = []
      segments << "#{number_with_delimiter(error_count)} #{"error".pluralize(error_count)}" if error_count > 0
      segments << "#{number_with_delimiter(warning_count)} #{"warning".pluralize(warning_count)}" if warning_count > 0
      segments << "#{number_with_delimiter(note_count)} #{"note".pluralize(note_count)}" if note_count > 0
      segments << "#{number_with_delimiter(fixed_count)} #{"fix".pluralize(fixed_count)}" if fixed_count > 0

      if @diff_errors.present?
        @diff_errors[0]
      elsif segments.present?
        segments.join(", ")
      else
        "No new alerts"
      end
    end

    def summary
      sections = []
      sections << "### Analysis overview"

      if @diff_errors.present?
        sections += @diff_errors
      elsif new_alerts.empty? && fixed_alerts.empty?
        sections << "No new or fixed alerts were detected in the analysis."
      else
        sections << new_alerts_summary
        sections << fixed_alerts_summary
      end

      sections.compact.join("\n\n").strip
    end

    private

    def error_count
      @error_count ||= new_alerts.count { |alert| alert.rule_severity == :ERROR }
    end

    def warning_count
      @warning_count ||= new_alerts.count { |alert| alert.rule_severity == :WARNING }
    end

    def note_count
      @note_count ||= new_alerts.count { |alert| alert.rule_severity == :NOTE }
    end

    def fixed_count
      fixed_alerts.count
    end

    def new_count
      new_alerts.count
    end

    def result_path(alert)
      # include branch scope if we have it available
      query = base_ref_name.present? ? Search::Queries::CodeScanningQuery.stringify([[:ref, base_ref_name]]) : nil
      Rails.application.routes.url_helpers.repository_code_scanning_result_path(repository.owner, repository, number: alert.number, query: query)
    end

    def fixed_alerts_summary
      return unless fixed_alerts.present?

      fix_links = fixed_alerts.map do |alert|
        "  * [#{alert.rule_short_description}](#{result_path(alert)}) (#{alert.location.file_path}#L#{alert.location.start_line})"
      end

      <<~FIXES.strip
      #### #{number_with_delimiter(fixed_count)} fixed #{"alert".pluralize(fixed_count)}

      #{fix_links.join("\n")}
      FIXES
    end

    def new_alerts_summary
      return "No new alerts were detected in the analysis." if new_alerts.empty?

      segments = []
      segments << "#{number_with_delimiter(error_count)} #{"error".pluralize(error_count)}" if error_count > 0
      segments << "#{number_with_delimiter(warning_count)} #{"warning".pluralize(warning_count)}" if warning_count > 0
      segments << "#{number_with_delimiter(note_count)} #{"note".pluralize(note_count)}" if note_count > 0

      "#{number_with_delimiter(new_count)} new #{"alert".pluralize(new_count)} (#{segments.join(", ")}) #{"was".pluralize(new_count)} detected in the analysis."
    end
  end
end
