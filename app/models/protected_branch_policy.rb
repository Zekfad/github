# rubocop:disable Style/FrozenStringLiteralComment

class ProtectedBranchPolicy
  include GitHub::AreasOfResponsibility
  areas_of_responsibility :protected_branches

  include MethodTiming

  # How many commits to load and signatures to verify at a time.
  SIGNATURE_VERIFICATION_BATCH_SIZE = 1000

  # Public: Check the branch protection policy for one or more ref updates
  #
  # repository            - The Repository, Gist, or Unsullied::Wiki whose refs
  #                         are being updated
  # ref_updates           - Array of Git::Ref::Updates
  # actor                 - User or PublicKey performing the update action
  #
  # Returns an Array of ProtectedBranchPolicy::Decision, one for each item
  # in ref_updates.
  def self.check(repository, ref_updates, actor)
    new(repository, ref_updates, actor).check
  end

  # Check protected branch policy for a single RefUpdate.
  #
  # Returns a single ProtectedBranchPolicy::Decision
  def self.check_one(repository, ref_update, actor, prepared_review_policy_decision = nil)
    policy = new(repository, [ref_update], actor)

    if prepared_review_policy_decision
      policy.prepared_review_policy_decisions << prepared_review_policy_decision
    end

    policy.check.first
  end

  def initialize(repository, ref_updates, actor)
    @repository = repository
    @ref_updates = ref_updates
    @actor = actor

    @results = {}
    @remaining_updates = ref_updates.index_by(&:refname)
    @prepared_review_policy_decisions = []

    @repo_flags = {}
  end

  def check
    return all_success unless repository.supports_protected_branches?

    validate_actor

    GitHub.dogstats.time("protected_branch_policy.time", tags: ["action:check"]) do
      check_non_protected_branch_policy
      check_authorization_policy
      check_deletion_policy
      check_non_fast_forward_policy
      check_review_and_status_check_policies
      check_required_signatures
      check_merge_commits

      remaining_updates.each do |ref_update|
        allow(ref_update)
      end

      final_results
    end
  rescue GitRPC::ObjectMissing
    remaining_updates.each do |ref_update|
      deny(ref_update, :object_missing)
    end
    final_results
  end

  attr_reader :prepared_review_policy_decisions

  private

  attr_reader :repository, :actor

  # Returns an Array of ref update tuples which have yet to be decided upon.
  def remaining_updates
    @remaining_updates.values
  end

  # Returns an Array of ProtectedBranchPolicy::Decisions, one for each item in
  # ref_updates. Do not call this until all decisions have been made.
  def final_results
    @ref_updates.map { |ref_update| @results.fetch(ref_update.refname) }
  end

  # Returns an Array of ProtectedBranchPolicy::Decisions with state success
  def all_success
    @ref_updates.map { |ref_update| Decision.success(ref_update) }
  end

  # Allow this ref update. No other policies will be checked for this update.
  #
  # ref_update - ref update tuple (see self.check)
  #
  # Returns nothing
  def allow(ref_update)
    can_override_status_checks = can_override_status_checks?(ref_update)
    can_override_review_policy = can_override_review_policy?(ref_update)
    can_override_required_signatures = can_override_required_signatures?(ref_update)
    can_override_required_linear_history = can_override_required_linear_history?(ref_update)

    record(ref_update, Decision.success(ref_update,
      can_override_status_checks: can_override_status_checks,
      can_override_review_policy: can_override_review_policy,
      can_override_required_signatures: can_override_required_signatures,
      can_override_required_linear_history: can_override_required_linear_history))
  end

  # Deny this ref update. No other policies will be checked for this update.
  #
  # ref_update  - Git::Ref::Update
  # reason_code - Symbol identifying the reason
  #
  # Returns nothing
  def deny(ref_update, reason_code)
    can_override_status_checks = can_override_status_checks?(ref_update)
    can_override_review_policy = can_override_review_policy?(ref_update)
    can_override_required_signatures = can_override_required_signatures?(ref_update)
    can_override_required_linear_history = can_override_required_linear_history?(ref_update)

    message = failure_message(reason_code)
    reason = ProtectedBranchPolicy::Reason.new(code: reason_code, message: message)

    instrumentation_payload = instrumentation_payload_for(ref_update, reasons: [reason])

    decision = Decision.new(ref_update,
      policy_fulfilled: false,
      can_override_status_checks: can_override_status_checks,
      can_override_review_policy: can_override_review_policy,
      can_override_required_signatures: can_override_required_signatures,
      can_override_required_linear_history: can_override_required_linear_history,
      reasons: [reason],
      instrumentation_payload: instrumentation_payload)

    record(ref_update, decision)
  end

  # Deny this ref update with a set of decisions containing reasons - this uses
  # named args, which is the preferred API going forward here.
  #
  # ref_update - Git::Ref::Update
  # decisions  - Array of RequiredStatusCheck::Policy::Decisions
  #              PullRequestReview::Policy::Decisions containing reasons
  #              of failure to fulfill the checks
  #
  # Returns nothing.
  def deny_with_decisions(ref_update, decisions:)
    can_override_status_checks = can_override_status_checks?(ref_update)
    can_override_review_policy = can_override_review_policy?(ref_update)
    can_override_required_signatures = can_override_required_signatures?(ref_update)
    can_override_required_linear_history = can_override_required_linear_history?(ref_update)
    reasons = decisions.map(&:reason)

    instrumentation_payload = instrumentation_payload_for(ref_update,
      reasons: reasons, additional_payloads: decisions.map(&:instrumentation_payload))

    decision = Decision.new(ref_update,
      policy_fulfilled: false,
      can_override_status_checks: can_override_status_checks,
      can_override_review_policy: can_override_review_policy,
      can_override_required_signatures: can_override_required_signatures,
      can_override_required_linear_history: can_override_required_linear_history,
      reasons: reasons,
      instrumentation_payload: instrumentation_payload)

    record(ref_update, decision)
  end

  # Records a ProtectedBranchPolicy::Decision.
  #
  # The ref update is removed from remaining_updates.
  #
  # Returns nothing.
  def record(ref_update, result)
    refname = ref_update.refname
    before = ref_update.before_oid
    after = ref_update.after_oid

    if @results.key?(refname)
      fail "Trying to record decision #{result.message} for ref update updating from #{before} to #{after} but already had recorded #{@results[refname].message}"
    end
    unless @remaining_updates.key?(refname)
      fail "Trying to record decision #{result.message} for ref update updating from #{before} to #{after} but it wasn't present in remaining_updates"
    end

    @results[refname] = result
    @remaining_updates.delete(refname)
  end

  def validate_actor
    if !actor.is_a?(User) && !actor.is_a?(PublicKey)
      raise TypeError, "expected actor to be a User or PublicKey, but was #{actor.class}"
    end
  end

  def protected_branch(ref_update)
    return unless repository.supports_protected_branches?
    protected_branches_by_refname[ref_update.refname]
  end

  def protected_branches_by_refname
    return @protected_branches_by_refname if defined?(@protected_branches_by_refname)
    @protected_branches_by_refname = protected_branches_by_refname!
  end

  def protected_branches_by_refname!
    unqualified_refnames = []

    remaining_updates.each do |ref_update|
      ref = repository.heads[ref_update.refname]
      next unless ref.present?

      unqualified_refnames << ref.name
    end

    protections_by_unqualified_name = ProtectedBranch.for_repository_with_branch_names(repository, unqualified_refnames)
    protections_by_unqualified_name.transform_keys { |unqualified_refname| "refs/heads/#{unqualified_refname}" }
  end

  def check_non_protected_branch_policy
    remaining_updates.each do |ref_update|
      allow(ref_update) unless protected_branch(ref_update)
    end
  end

  def check_authorization_policy
    remaining_updates.each do |ref_update|
      branch = protected_branch(ref_update)
      next if branch.nil?

      unless branch.authorized?(actor)
        deny(ref_update, :unauthorized)
      end
    end
  end
  time_method :check_authorization_policy, key: "protected_branch.policy_check.authorization.duration"

  def check_deletion_policy
    remaining_updates.each do |ref_update|
      if ref_update.deletion?
        if protected_branch(ref_update)&.block_deletions_enabled?
          deny(ref_update, :deletion)
        else
          allow(ref_update)
        end
      end
    end
  end

  def check_non_fast_forward_policy
    ref_updates_requiring_descendants_check = remaining_updates.select do |ref_update|
      !ref_update.creation? && !ref_update.deletion? && ref_update.fast_forward.nil?
    end

    if ref_updates_requiring_descendants_check.any?
      updates_by_commit_and_ancestor = ref_updates_requiring_descendants_check.group_by do |ref_update|
        [ref_update.after_oid, ref_update.before_oid]
      end
      repository.rpc.descendant_of(updates_by_commit_and_ancestor.keys).each do |commit_and_ancestor, is_descendant|
        updates_by_commit_and_ancestor[commit_and_ancestor].each do |ref_update|
          ref_update.fast_forward = is_descendant
        end
      end
    end

    remaining_updates.each do |ref_update|
      next if ref_update.creation? || ref_update.deletion? || ref_update.fast_forward
      next if !protected_branch(ref_update)&.block_force_pushes_enabled?

      deny(ref_update, :force_push)
    end
  end
  time_method :check_non_fast_forward_policy, key: "protected_branch.policy_check.non_fast_forward.duration"

  # Check to see if protected branches have their PullRequestReview and
  # RequiredStatusCheck policies met
  def check_review_and_status_check_policies
    review_decisions = PullRequestReview::Policy.check(repository, remaining_updates_for_review_policy, protected_branches_by_refname, actor: actor)
    status_check_decisions = RequiredStatusCheck::Policy.check(repository, remaining_updates, protected_branches_by_refname)

    decisions = prepared_review_policy_decisions + review_decisions + status_check_decisions
    decisions_by_ref_update = decisions.group_by(&:ref_update)

    remaining_updates.each do |ref_update|
      decisions = decisions_by_ref_update[ref_update]
      next if decisions.all?(&:policy_fulfilled?)

      unfulfilled_decisions = decisions.reject(&:policy_fulfilled?)
      deny_with_decisions(ref_update, decisions: unfulfilled_decisions)
    end
  end

  def check_required_signatures
    # ignore updates to non-protected branches.
    updates = remaining_updates.select do |ref_update|
      protected_branch(ref_update)&.required_signatures_enabled?
    end
    return if updates.empty?

    # find oids of commits in ref-update.
    oids_by_update = updates.each_with_object({}) do |ref_update, h|
      h[ref_update] = if ref_update.creation?
        repository.rpc.rev_list(ref_update.after_oid)
      else
        repository.rpc.rev_list(ref_update.after_oid,
          exclude_oids: ref_update.before_oid,
        )
      end
    end

    # load commits.
    oids = oids_by_update.values.flatten.uniq
    batches = oids.each_slice(SIGNATURE_VERIFICATION_BATCH_SIZE)
    commit_by_oid = batches.each_with_object({}) do |slice, h|
      repository.commits.find(slice).each { |c| h[c.oid] = c }
    end
    commits_by_update = oids_by_update.each_with_object({}) do |(ref_update, oids), h|
      h[ref_update] = commit_by_oid.values_at(*oids)
    end

    # quickly deny ref-updates with a commit having no signature.
    commits_by_update.keep_if do |ref_update, commits|
      if commits.all?(&:has_signature?)
        true
      else
        instrument_required_signatures_check(allowed: false, reasons: [GitSigning::UNSIGNED])

        deny(ref_update, :invalid_signature)

        false
      end
    end
    return if commits_by_update.empty?

    # verify signatures.
    commits = commits_by_update.values.flatten
    commits.each_slice(SIGNATURE_VERIFICATION_BATCH_SIZE) do |slice|
      Commit.prefill_verified_signature(commits)
    end

    # deny ref-updates with a commit having invalid signature.
    commits_by_update.each do |ref_update, commits|
      allowed = commits.all?(&:verified_signature?)

      instrument_required_signatures_check(allowed: allowed, reasons:
        commits.map(&:signature_verification_reason)
      )

      deny(ref_update, :invalid_signature) unless allowed
    end
  end

  def check_merge_commits
    # ignore updates to non-protected branches, branches that are not blocking merge commits, or branch deletions
    updates = remaining_updates.select do |ref_update|
      protected_branch(ref_update)&.required_linear_history_enabled?
    end
    return if updates.empty?

    updates.each do |update|
      # list any merge commits in this ref update's commit range.
      # deny the update if any results are returned.
      opts = {merges: true, limit: 1}
      opts[:exclude_oids] = update.before_oid unless update.creation?

      oids = repository.rpc.rev_list(update.after_oid, **opts)
      deny(update, :merge_commit) unless oids.empty?
    end
  end
  time_method :check_merge_commits, key: "protected_branch.policy_check.block_merge_commits.duration"

  def instrument_required_signatures_check(allowed:, reasons: [])
    tags = GitSigning::REASONS.map { |r| "#{r}:#{reasons.include?(r)}" }
    tags << allowed ? "result:allow" : "result:deny"

    GitHub.dogstats.increment("protected_branch.required_signatures_check", tags: tags)
  end

  def remaining_updates_for_review_policy
    prepared_ref_updates = prepared_review_policy_decisions.map(&:ref_update)

    remaining_updates - prepared_ref_updates
  end

  def can_override_status_checks?(ref_update)
    branch = protected_branch(ref_update)
    return false unless branch.present?

    branch.can_override_status_checks?(actor: actor)
  end

  def can_override_review_policy?(ref_update)
    branch = protected_branch(ref_update)
    return false unless branch.present?

    branch.can_override_review_policy?(actor: actor)
  end

  def can_override_required_signatures?(ref_update)
    branch = protected_branch(ref_update)
    return false unless branch.present?

    branch.can_override_required_signatures?(actor: actor)
  end

  def can_override_required_linear_history?(ref_update)
    branch = protected_branch(ref_update)
    return false unless branch.present?

    branch.can_override_required_linear_history?(actor: actor)
  end

  def failure_message(reason_code)
    case reason_code
    when :force_push
      "Cannot force-push to this protected branch"
    when :deletion
      "Cannot delete this protected branch"
    when :unauthorized
      "You're not authorized to push to this branch. Visit #{GitHub.help_url}/articles/about-protected-branches/ for more information."
    when :invalid_signature
      "Commits must have valid signatures."
    when :merge_commit
      "This branch must not contain merge commits."
    when :object_missing
      "One or more commits is missing."
    end
  end

  def repo_feature_flag(flag_name)
    if !@repo_flags.has_key?(flag_name)
      @repo_flags[flag_name] = GitHub.flipper[flag_name].enabled?(repository)
    end
    @repo_flags[flag_name]
  end

  def instrumentation_payload_for(ref_update, reasons:, additional_payloads: [])
    payload = {
      branch: ref_update.refname,
      repo: repository,
      before: ref_update.before_oid,
      after: ref_update.after_oid,
    }

    if actor.is_a?(PublicKey)
      payload[:actor] = actor.verifier
      payload[:deploy_key_fingerprint] = actor.fingerprint
    else
      payload[:actor] = actor
    end

    payload[:reasons] = reasons.sort_by(&:code).map do |reason|
      {
        code: reason.code,
        message: reason.message,
      }
    end

    if ref_update.respond_to?(:policy_commit_oid)
      payload[:policy] = ref_update.policy_commit_oid
    end

    if repository.in_organization?
      payload[:org] = repository.organization
    end

    additional_payloads.each do |additional_payload|
      payload.merge!(additional_payload)
    end

    payload
  end
end
