# frozen_string_literal: true

class ReminderPullRequestFilterBase
  def base_scope(repo_ids: nil)
    repo_scope = Repository.active.not_archived_scope.owned_by_org(reminder.remindable)
    repo_scope = repo_scope.public_scope unless reminder.supports_private_repos?
    repo_scope = repo_scope.where(id: repo_ids) if repo_ids

    # Using FORCE INDEX because MySQL optimizer will intermittently decide a `issues` table scan is the best plan.
    # For more details, see: https://github.com/github/database-infrastructure/issues/2381
    PullRequest.
      joins("INNER JOIN `issues` FORCE INDEX(`repository_id_and_state_and_pull_request_id_and_user`) ON `issues`.`pull_request_id` = `pull_requests`.`id`").
      joins("INNER JOIN `repositories` ON `repositories`.`id` = `issues`.`repository_id`").
      where(issues: { state: "open" }).
      merge(repo_scope)
  end
end
