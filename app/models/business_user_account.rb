# frozen_string_literal: true

# A user account on a business.
#
# Belongs to a single Business.
# Belongs to a single User.
# Has many EnterpriseInstallationUserAccounts.
class BusinessUserAccount < ApplicationRecord::Collab
  include GitHub::Relay::GlobalIdentification

  belongs_to :user
  belongs_to :business
  has_many :enterprise_installation_user_accounts
  has_many :enterprise_installation_user_account_emails,
    through: :enterprise_installation_user_accounts,
    source: :emails

  has_many :avatars, through: :user

  validates :business, presence: true
  validates_uniqueness_of :user, scope: :business, allow_nil: true

  before_save :update_login, if: :user_id_changed?
  after_commit :snapshot_license_state

  # Orphaned records don't have any associated cloud or server user accounts
  # `BusinessUserAccount.orphaned.delete_all` is run during enterprise installation
  # user account import, which does not trigger destroy hooks.
  scope :orphaned, -> { left_joins(:enterprise_installation_user_accounts).where(user_id: nil, enterprise_installation_user_accounts: { id: nil }) }

  def self.remove_members(user_ids)
    BusinessUserAccount.transaction do
      where(user_id: user_ids).update_all(user_id: nil)
      orphaned.destroy_all
    end
  end

  def name
    if user
      user.profile_name
    else
      # user could have multiple enterprise_installation_user_accounts with different profile_names or logins
      # for now, sort and take the first profile_name
      enterprise_installation_user_accounts.map(&:profile_name).compact.sort.first
    end
  end

  def avatar_url(size = nil)
    user&.primary_avatar_url(size)
  end

  def platform_type_name
    "EnterpriseUserAccount"
  end

  # Public: Get the enterprise installations this BusinessUserAccount is associated with.
  #
  # All arguments optional.
  #
  # query:              - The user-provided query string with any filters (example: `role`) removed,
  #                       matched against the installation host name and/or customer name
  # order_by_field      - a String specifying the sort field
  # order_by_direction  - a String specifying the sort direction
  # role                - filter enterprise installations by the user's role. either `member` or `owner`
  #
  # Returns an ActiveRecord::Relation
  def user_enterprise_installations(query: nil, order_by_field: nil, order_by_direction: nil, role: nil)
    user_accounts = enterprise_installation_user_accounts

    unless role.nil? || user_accounts.empty?
      selected_role_is_site_admin = (role == "owner")

      user_accounts = user_accounts.select do |user_account|
        user_account.site_admin == selected_role_is_site_admin
      end
    end

    scope = business
      .enterprise_installations
      .where(id: user_accounts.map(&:enterprise_installation_id))

    query = ActiveRecord::Base.sanitize_sql_like(query.to_s.strip.downcase)
    if query.present?
      scope = scope.where(["host_name LIKE :query OR customer_name LIKE :query", { query: "%#{query}%" }])
    end

    if order_by_field.present? && order_by_direction.present?
      scope = scope.order("#{order_by_field} #{order_by_direction}")
    end

    scope
  end

  # Public: Get the enterprise organizations that this BusinessUserAccount is a member of.
  #
  # viewer - required, the user who is requesting to list the BusinessUserAccount's organization memberships
  #
  # optional arguments
  #
  # query:              - The user-provided query string with any filters (example: `role`) removed
  # order_by_field      - a String specifying the sort field
  # order_by_direction  - a String specifying the sort direction
  # org_member_type     - filter to return organizations based on the user's membership type (:admin, :member_without_admin, :all)
  #                       default: :all
  #
  # Returns an ActiveRecord::Relation
  def enterprise_organizations(viewer, query: nil, order_by_field: nil, order_by_direction: nil, org_member_type: :all)
    # server-only members won't have a dotcom User or Organizations they're a part of
    return Organization.none if user.nil?

    scope = business.organizations.where(id: user.organizations)

    query = ActiveRecord::Base.sanitize_sql_like(query.to_s.strip.downcase)
    if query.present?
      scope = scope.includes(:profile)
        .where(["users.login LIKE :query OR profiles.name LIKE :query", { query: "%#{query}%" }])
        .references(:profile)
    end

    unless business.owner?(viewer) || viewer.site_admin?
      viewable_org_ids = user.public_organizations.pluck(:id) | viewer.organizations.pluck(:id)
      scope = scope.where(id: viewable_org_ids)
    end

    unless org_member_type == :all
      viewable_org_ids = business
        .organizations_for_member(
          user,
          type: org_member_type,
        ).pluck(:id)

      scope = scope.where(id: viewable_org_ids)
    end

    if order_by_field.present? && order_by_direction.present?
      scope = scope.order("users.#{order_by_field} #{order_by_direction}")
    end

    scope.distinct
  end

  # Public: Get the org member type Symbol based on a role String.
  #
  # role - String, either "member" or "owner"
  #
  # Returns Symbol
  def self.org_member_type_from_role(role)
    case role
    when "owner"
      :admin
    when "member"
      :member_without_admin
    else
      :all
    end
  end

  private

  def update_login
    self.login = if user
      user.login
    else
      enterprise_installation_user_account_emails.where(primary: true).limit(1).pluck(:email).first || ""
    end
  end

  def snapshot_license_state
    Billing::SnapshotLicensesJob.perform_later(business)
  end
end
