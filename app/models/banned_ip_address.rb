# rubocop:disable Style/FrozenStringLiteralComment

require "resolv"

class BannedIpAddress < ApplicationRecord::Domain::BannedIps
  validates_uniqueness_of :ip, case_sensitive: false
  validates_format_of :ip, with: Regexp.union(Resolv::IPv4::Regex, Resolv::IPv6::Regex)
  validates :reason, presence: true, allow_blank: false
  validate :dont_ban_ourselves
  validate :under_maximum_records

  # Over time banned IPs will be allowed to receive traffic again
  scope :recent, -> { where(BannedIpAddress.arel_table[:updated_at].gteq(TTL.ago)) }

  GITHUB_CIDR_BLOCK = IPAddress("192.30.252.0/22")
  FASTLY_IPS = [
    IPAddress("23.235.32.0/20"),
    IPAddress("43.249.72.0/22"),
    IPAddress("103.244.50.0/24"),
    IPAddress("103.245.222.0/23"),
    IPAddress("103.245.224.0/24"),
    IPAddress("104.156.80.0/20"),
    IPAddress("151.101.0.0/16"),
    IPAddress("157.52.64.0/18"),
    IPAddress("172.111.64.0/18"),
    IPAddress("185.31.16.0/22"),
    IPAddress("199.27.72.0/21"),
    IPAddress("199.232.0.0/16"),
    IPAddress("202.21.128.0/24"),
    IPAddress("203.57.145.0/24"),
  ]

  HUBBER_OFFICE_IPS = [
    IPAddress("4.53.133.38"),     #HQ
    IPAddress("192.195.83.114"),  #HQ Backup
    IPAddress("206.168.224.82"),  #Boulder
    IPAddress("206.168.224.83"),  #Boulder
    IPAddress("182.168.135.161"), #Tokyo
  ]

  # Duration that bans last. After the TTL period, IPs will no longer be blocked
  TTL = 1.year

  # Used to throttle database touches
  UPDATE_FREQUENCY = 1.hour

  # Because banned IP lookups occur on every request and the number of records
  # is trivial, we cache the entire list in memcache. If the list grows, we
  # should ensure the caching strategy will scale as well.
  MAXIMUM_RECORDS = 50

  # Attempts to block an IP.
  #
  # If the IP is a known blocked address, we touch the record. If the IP is
  # unkown, we create and ban it.
  #
  # Return the BannedIpAddress created or updated
  def self.block_unless_whitelisted(ip, reason, user = nil)
    require_employee!(user)
    banned_ip = find_by_ip(ip)

    if banned_ip # already blocked or whitelisted
      if banned_ip.whitelisted?
        banned_ip.errors.add(:base, "IP already whitelisted")
      else
        banned_ip.errors.add(:base, "IP already banned")
      end

      if banned_ip.banned? && banned_ip.updated_at < UPDATE_FREQUENCY.ago
        banned_ip.touch
      end
      banned_ip
    else
      new(ip: ip).tap do |banned_ip|
        banned_ip.ban(reason, user)
      end
    end
  end

  # By setting banned to false instead of deleting the record, we are
  # whitelisting the IP for future use since you can't block a known IP.
  def whitelist(reason, user)
    self.class.require_employee!(user)
    update(banned: false, reason: self.class.reason_with_context(reason, user))
  end

  def ban(reason, user)
    self.class.require_employee!(user)
    update(banned: true, reason: self.class.reason_with_context(reason, user))
  end

  def whitelisted?
    self.persisted? && !self.banned?
  end

  def self.reason_with_context(reason, user)
    [user, reason].compact.join(": ")
  end

  def self.require_employee!(user)
    if user && !user.employee?
      raise RuntimeError, "Non-employee attempted to ban address #{user}"
    end
  end

  def self.permanently_allowed_ip?(ip)
    if IPAddress.valid?(ip)
      parsed_ip = IPAddress(ip)
      private_ip?(parsed_ip) || github_ip?(parsed_ip) || fastly_ip?(parsed_ip)
    end
  end

  # there is no #private? on ipv6 records.
  def self.private_ip?(parsed_ip)
    parsed_ip.ipv4? && parsed_ip.private?
  end

  def self.github_ip?(parsed_ip)
    parsed_ip.ipv4? && (GITHUB_CIDR_BLOCK.include?(parsed_ip) || HUBBER_OFFICE_IPS.include?(parsed_ip))
  end

  def self.fastly_ip?(parsed_ip)
    parsed_ip.ipv4? && FASTLY_IPS.any? { |cidr_block| cidr_block.include?(parsed_ip) }
  end

  private def dont_ban_ourselves
    if self.class.permanently_allowed_ip?(self.ip)
      errors.add(:ip, "Cannot ban permanently whitelisted IP addresses")
      GitHub.dogstats.increment("banned_ip.report", tags: ["operation:ban", "error:tried-to-whitelisted-ip"])
    end
  end

  private def under_maximum_records
    if BannedIpAddress.count >= MAXIMUM_RECORDS
      errors.add(:base, "A cap of #{MAXIMUM_RECORDS} records was placed on this table. Ask prodsec about raising this limit.")
    end
  end
end
