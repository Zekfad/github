# frozen_string_literal: true

class MemexProjectColumnValue < ApplicationRecord::Domain::Memexes
  include GitHub::Validations
  extend GitHub::Encoding

  force_utf8_encoding :value

  TEXT_VALUE_BYTESIZE_LIMIT = 1024

  belongs_to :memex_project_column, inverse_of: :memex_project_column_values
  belongs_to :memex_project_item, inverse_of: :memex_project_column_values
  belongs_to :creator, class_name: "User"

  validates :memex_project_column, presence: true
  validates :memex_project_item, presence: true
  validates :creator, presence: true
  validates :value, presence: true
  validates :value, bytesize: { maximum: TEXT_VALUE_BYTESIZE_LIMIT }, unicode: true, if: :text_value?

  private

  def text_value?
    memex_project_column&.text?
  end
end
