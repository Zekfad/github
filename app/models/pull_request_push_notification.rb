# rubocop:disable Style/FrozenStringLiteralComment

class PullRequestPushNotification
  include GitHub::UserContent

  COMPOUND_ID_REGEX = /\APull#(?<pull_id>\d+)Push#(?<push_id>\d+)\z/

  attr_reader :pull_request, :push

  delegate :pusher, :new_record?, :created_at, to: :push

  # Repository must come from the pull request as opposed to the push since
  # it acts as the notification list. In the case of PRs from forks, we don't
  # to notify the users subscribed to the fork but the repo the pull request
  # belongs to.
  delegate :repository, :issue, to: :pull_request

  # Tell Newsies where to get the information it needs.
  alias_method :user, :pusher
  alias_method :entity, :repository
  alias_method :notifications_list, :repository
  alias_method :notifications_thread, :issue
  alias_method :notifications_author, :user

  def async_entity
    Promise.resolve(entity)
  end

  # Public: Notify each open pull request for the given push record. Called
  # after creating the push record during the post-receive job.
  #
  # push - Required Push record to notify about.
  #
  # Returns an array of PullRequestPushNotifications to deliver.
  def self.notifications_for(push)
    GitHub.dogstats.time("pull_request_push_notification", tags: ["action:notifications_for"]) do
      push.matching_open_pull_requests_to_update.map do |pull|
        new(pull_request: pull, push: push)
      end
    end
  end

  # Public: Instantiates a PullRequestPushNotification given a compound key
  # which includes the PullRequest and Push record IDs. Called by the
  # deliver-notifications job to find the notification instance.
  #
  # compound_id - Required compound ID String.
  #
  # Returns a PullRequestPushNotification or nil if not found.
  def self.find_by_id(compound_id)  # rubocop:disable GitHub/FindByDef
    return unless match = COMPOUND_ID_REGEX.match(compound_id.to_s)

    pull = PullRequest.find_by_id(match[:pull_id].to_i)
    push = Push.find_by_id(match[:push_id].to_i)
    return unless pull && push

    new(pull_request: pull, push: push)
  end

  def initialize(pull_request:, push:)
    @pull_request = pull_request
    @push = push
  end

  def id
    "Pull##{ pull_request.id }Push##{ push.id }"
  end

  def permalink
    "#{ pull_request.permalink }/files/#{ push.before }..#{ push.after}"
  end

  def user_id
    user.try(:id)
  end

  def message_id
    "<#{repository.name_with_owner}/pull/#{issue.number}/push/#{push.id}@#{GitHub.urls.host_name}>"
  end

  def commits
    return [] if push.large_push?
    pull_comparison.commits
  end

  def diff_entries
    return [] if push.large_push?
    pull_comparison.diffs.entries
  end

  def body
    login = "@#{user.login}" if user.present?
    "#{login || "Somebody"} pushed #{commits.length} #{"commit".pluralize(commits.size)}."
  end

  private

  def pull_comparison
    return @pull_comparison if defined?(@pull_comparison)

    merge_base_oid = pull_request.compare_repository.best_merge_base(pull_request.base_sha, push.after)

    @pull_comparison = PullRequest::Comparison.find \
      pull: pull_request,
      start_commit_oid: push.before,
      end_commit_oid: push.after,
      base_commit_oid: merge_base_oid
  end
end
