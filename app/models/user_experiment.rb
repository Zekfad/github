# rubocop:disable Style/FrozenStringLiteralComment

class UserExperiment < ApplicationRecord::Domain::UserExperiments
  has_many :enrollments, class_name: "UserExperimentEnrollment"

  # Is the experiment currently running?
  #
  # Returns a Boolean.
  def active?
    started_at.present? && finished_at.nil?
  end

  # Has the experiment finished running?
  #
  # Returns a Boolean.
  def finished?
    finished_at.present?
  end

  # Has the experiment started running?
  #
  # Returns a Boolean.
  def started?
    started_at.present?
  end

  # Duration of the experiment in days
  #
  # Returns a Float.
  def duration_in_days
    return 0 unless started_at.present? && finished_at.present?
    (finished_at - started_at) / 86400 # days
  end

  # Public: Returns a GitHub::UserResearch::UserExperiment object
  # for the given slug.
  def configuration
    GitHub::UserResearch.experiment(self.slug)
  end
end
