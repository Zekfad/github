# frozen_string_literal: true

class SponsorsGoal < ApplicationRecord::Collab
  include Instrumentation::Model
  include Workflow

  extend GitHub::Encoding
  force_utf8_encoding :description

  enum kind: {
    total_sponsors_count: 0,
    monthly_sponsorship_amount: 1,
  }

  belongs_to :listing,
    required: true,
    class_name: :SponsorsListing,
    foreign_key: :sponsors_listing_id

  has_many :contributions,
    dependent: :destroy,
    class_name: :SponsorsGoalContribution

  scope :with_states, ->(*states) do
    state_values = states.map { |state| workflow_spec.states[state.to_sym]&.value }
    where(state: state_values)
  end

  scope :active, -> { with_states(:active) }
  scope :completed, -> { with_states(:completed) }

  # The `target_value` column has a limit of 4 bytes to store a **signed** integer.
  # That means that the maximum number we can represent is:
  #
  # 8 (bits per byte) * 4 (bytes) = 32
  # 32 bits - 1 (to represent the sign: negative vs positive) = 31
  # 2**31 = 2_147_483_648
  #
  # NOTE: We don't have a reason to limit this number other than to avoid an exception.
  # We have the ability to increase the byte limit in the future if needed.
  MAX_TARGET_VALUE = 2_147_483_648

  # We want to limit goal descriptions so they are concise and have a better UI treatment.
  # This was an arbitrary limit chosen based on the current value of
  # SponsorsTier::MAX_DESCRIPTION_LENGTH. It can be increased if necessary.
  MAX_DESCRIPTION_LENGTH = 750

  validates :target_value, presence: true, numericality: {
    only_integer: true,
    greater_than: 0,
    less_than: MAX_TARGET_VALUE,
  }

  validates :description, presence: true, length: { maximum: MAX_DESCRIPTION_LENGTH }
  validate :only_one_active_goal, if: :active?
  validate :not_already_completed, if: :active?

  workflow :state do
    state :active, 0 do
      event :complete, transitions_to: :completed, if: :achieved?
      event :retire, transitions_to: :retired
    end

    state :completed, 1
    state :retired, 2
  end

  def current_value
    @current_value ||= if monthly_sponsorship_amount?
      listing.subscription_value / 100
    else
      listing.sponsorable.sponsorships_as_sponsorable.active.count
    end
  end

  def percent_complete
    return 0 if target_value.to_i.zero?

    percent = current_value * 100 / target_value
    # encourage users by rounding up to 1% if current value is at least 1
    percent = 1 if current_value.positive? && percent == 0

    [100, percent].min
  end

  def title
    if monthly_sponsorship_amount?
      "#{Billing::Money.new(target_value*100).format(no_cents: true)} per month"
    else
      "#{target_value} #{"sponsor".pluralize(target_value)}"
    end
  end

  def formatted_current_value
    if monthly_sponsorship_amount?
      Billing::Money.new(current_value*100).format(no_cents: true)
    else
      current_value
    end
  end

  private

  # Whether the target value has been reached. This is meant to be a private method
  # used only to manage state transitions. Public calls should rely on `can_complete?`
  # or check if goal state is `completed?`.
  def achieved?
    return false if listing.blank?
    return false if target_value.blank?

    if monthly_sponsorship_amount?
      (listing.subscription_value / 100) >= target_value
    else
      listing.sponsorable.sponsorships_as_sponsorable.active.count >= target_value
    end
  end

  def complete(*args, **kwargs)
    record_contributions
    touch(:completed_at)

    GlobalInstrumenter.instrument("sponsors.goal_event", {
      listing: listing,
      goal: self,
      action: :COMPLETED,
    })
  end

  def retire(*args, **kwargs)
    record_contributions
    touch(:retired_at)
  end

  def record_contributions
    sponsorable = listing.sponsorable
    sponsorships = sponsorable
      .sponsorships_as_sponsorable
      .active
      .order(created_at: :asc)
      .pluck(:sponsor_id, :subscribable_id)

    now = Time.current
    contribution_rows = sponsorships.map do |sponsor_id, tier_id|
      {
        sponsors_goal_id: id,
        sponsor_id: sponsor_id,
        sponsors_tier_id: tier_id,
        created_at: now,
        updated_at: now,
      }
    end

    contributions.upsert_all(contribution_rows) if contribution_rows.any?
  end

  def only_one_active_goal
    return if listing.blank?
    return if listing.active_goal.blank?
    return if listing.active_goal.id == id

    errors.add(:base, "Only one active goal can exist at a time.")
  end

  def not_already_completed
    return if listing.blank?
    return if listing.sponsorable.blank?
    return unless achieved?

    errors.add(:base, %{
      You already achieved this goal. Way to go!
      Try setting a different goal target.
    }.squish)
  end
end
