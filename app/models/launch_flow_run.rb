# frozen_string_literal: true

class LaunchFlowRun
  include GitHub::Relay::GlobalIdentification

  def initialize(uuid:)
    @uuid = uuid
  end

  attr_reader :uuid

  def id
    uuid
  end
end
