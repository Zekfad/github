# frozen_string_literal: true

class Page::FileServer < ApplicationRecord::Domain::Repositories
  self.table_name = :pages_fileservers
end
