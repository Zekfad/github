# frozen_string_literal: true

require "github/pages/domain_health_checker"

class Page::Certificate < ApplicationRecord::Domain::Repositories
  include GitHub::CacheLock
  include GitHub::Relay::GlobalIdentification

  Error = Class.new(RuntimeError)

  # Errors that, when encountered during our flow, can be recovered from.
  RecoverableError = Class.new(Error)

  # Errors that, when encountered during our flow, cannot be recovered from.
  FatalError                 = Class.new(Error)
  BlacklistedError           = Class.new(FatalError)
  FastlyNotEnabledError      = Class.new(FatalError)
  MissingDataError           = Class.new(FatalError)
  AttemptedDomainChangeError = Class.new(FatalError)
  LockedError                = Class.new(FatalError)
  InvalidAuthorizationError  = Class.new(FatalError)
  DomainError                = Class.new(FatalError)

  # How long before a cert's expiration we renew it.
  RENEWAL_WINDOW = 30.days

  def self.areas_of_responsibility
    [:pages]
  end

  PEM_REXP = /[-]+BEGIN CERTIFICATE[-]+.+?[-]+END CERTIFICATE[-]+/m

  STATE_DESCRIPTIONS = {
    new:                   "This domain was recently added. The certificate request process will begin shortly.",
    authorization_created: "Authorization created",
    authorization_pending: "Authorization verification pending.",
    authorized:            "Domain authorization succeeded.",
    authorization_revoked: "Authorization has been revoked.",
    issued:                "The certificate has been successfully issued.",
    uploaded:              "The certificate has been uploaded and is awaiting approval.",
    approved:              "The certificate has been approved.",
    errored:               "An error occurred.",
    bad_authz:             "The ACME authorization is in a bad state. We need to start over.",
  }.freeze

  enum state: {
    new:                   0,
    authorization_created: 1,
    authorization_pending: 2,
    authorized:            3,
    authorization_revoked: 4,
    issued:                5,
    uploaded:              6,
    approved:              7,
    errored:               8,
    bad_authz:             9,
  }, _prefix: true

  def self.state_description(state)
    STATE_DESCRIPTIONS[state.to_sym]
  end

  def state_description
    self.class.state_description(current_state)
  end

  validates_presence_of :domain

  before_validation :fail_on_domain_change, on: :update
  before_validation :set_initial_state

  after_commit :begin_flow, on: [:create]

  before_destroy :delete_certificate_from_fastly

  # Is this domain configured correctly for us to obtain a certificate?
  #
  # domain_name - A String domain name.
  #
  # Returns boolean.
  def self.eligible?(domain_name)
    return false if domain_name.blank?
    return false if blacklisted?(domain_name)
    GitHub::Pages::DomainHealthChecker.new(domain_name).https_eligible?
  end

  # Is this domain blacklisted?
  #
  # domain_name - a String domain name.
  #
  # Returns true if the domain is blacklisted and a certificate must not be issued.
  def self.blacklisted?(domain_name)
    GitHub.acme_blacklist.any? { |entry| domain_name.end_with?(entry) }
  end

  # Internal: An unencoded string used to generate a global identifier for use
  # with GraphQL and Relay. See #global_relay_id below.
  def global_id
    "#{self.id}:#{Digest::MD5.hexdigest(self.domain)}"
  end

  # Enqueue a job to begin the cert issuance/upload flow.
  #
  # Returns nothing.
  def begin_flow
    PageCertificateWorkJob.enqueue(id)
  end
  alias resume_flow begin_flow

  def preflight_acme!
    return unless GitHub.acme_enabled?
    GitHub.acme.nonces.clear
  end

  # Request a domain authorization from the ACME server.
  #
  # Returns nothing.
  def request_authorization
    check_domain!

    preflight_acme!

    order =  GitHub.acme.new_order(identifiers: [domain])
    authorization = order.authorizations.first
    challenge = authorization.http

    update_state(:authorization_created,
      order_url: order.url,
      authorization_url: authorization.url,
      challenge_path: "/#{challenge.filename}",
      challenge_response: challenge.file_content,
    )

    return :proceed
  end

  # Request that the ACME server verify our authorization by making a request to
  # the domain.
  #
  # Returns nothing.
  def request_authorization_verification
    check_domain!

    preflight_acme!

    authorization = GitHub.acme.authorization(url: authorization_url)
    authorization.http.request_validation

    update_state(:authorization_pending)

    return :proceed
  end

  # Check if our authorization has been verified.
  #
  # Returns nothing.
  def check_authorization_verification
    preflight_acme!

    authorization = begin
      GitHub.acme.authorization(url: authorization_url)
    rescue Acme::Client::Error::NotFound
      # LE deletes pending authorizations after a days. At this point, we just
      # have to start over.
      update_state(:bad_authz)
      return :proceed
    end

    case authorization.http.status
    when "valid"
      # Our authorization has been verified. We can move on to requesting a
      # certificate.
      update_state(:authorized)
      return :proceed
    when "invalid"
      update_state(:bad_authz)
      return :proceed
    when "pending"
      # The CA hasn't made the verification request yet. Check again later.
      return :retry
    when "revoked"
      # We were authorized, but that's expired. Start over.
      update_state(:authorization_revoked)
      return :proceed
    else
      # Could be "processing", which isn't currently used. Could be or
      # "deactivated", meaning that we explicitly canceled our authorization,
      # which should never happen. Either way, we'll retry to see if it resolves
      # itself.
      return :retry
    end
  end

  # Request that a certificate be issued.
  #
  # Returns nothing.
  def request_certificate
    preflight_acme!

    # reference status from https://tools.ietf.org/html/draft-ietf-acme-acme-12#section-7.1.6
    order = GitHub.acme.order(url: order_url)
    if order.status == "valid"
      update_state(:issued,
        expires_at: OpenSSL::X509::Certificate.new(order.certificate).not_after,
        cert_chain: order.certificate,
        fastly_privkey_id: GitHub.fastly_private_key_id,
      )
    elsif order.status == "processing"
      # Certificate is under processing, check later.
      return :retry
    elsif order.status == "invalid"
      update_state(:bad_authz)
    elsif order.status == "ready"
      csr = GitHub.acme_cert_key.csr(subject: {common_name: domain})
      order.finalize(csr: csr.parsed)
      update_state(:authorized,
        earthsmoke_key_version_id: csr.key_version_id
      )
    end
    :proceed
  end

  # A certificate has been issued. Upload it to Fastly.
  #
  # Returns nothing.
  def upload_certificate
    raise FastlyNotEnabledError unless GitHub.fastly_enabled?

    unless pem_chain = parsed_detail["cert_chain"]
      # This should never happen.
      raise MissingDataError, "Missing cert chain"
    end

    chain = pem_chain.scan(PEM_REXP)
    cert_body = chain.shift
    cert_intermediate_body = chain.join("\n")

    if cert_body.nil? || cert_intermediate_body.nil?
      raise MissingDataError, "Missing cert or intermediate"
    end

    response = if fastly_certificate_id
      GitHub.dogstats.time("github.fastly.rpc", tags: ["method:update_certificate"]) do
        GitHub.fastly.update_certificate(Fastly::Certificate.new({
            certificate_id:           fastly_certificate_id,
            certificate:              cert_body,
            certificate_intermediate: cert_intermediate_body,
            key_file:                 fastly_privkey_id,
          }),
        )
      end
    else
      GitHub.dogstats.time("github.fastly.rpc", tags: ["method:upload_certificate"]) do
        GitHub.fastly.upload_certificate(Fastly::Certificate.new({
            certificate:              cert_body,
            certificate_intermediate: cert_intermediate_body,
            key_file:                 fastly_privkey_id,
          }),
        )
      end
    end

    if response.approved
      update_state(:approved, fastly_certificate_id: response.certificate_id)
      GitHub.dogstats.increment("pages.certificates", { state: "approved"})
      return :halt
    else
      update_state(:uploaded, fastly_certificate_id: response.certificate_id)
      GitHub.dogstats.increment("pages.certificates", { state: "uploaded"})
      return :proceed
    end
  end

  # The certificate has been uploaded to Fastly. Check if it's been approved.
  #
  # Returns nothing.
  def check_uploaded_certificate
    raise FastlyNotEnabledError unless GitHub.fastly_enabled?

    unless fastly_certificate_id
      # This should never happen.
      raise MissingDataError, "Missing Fastly certificate ID"
    end

    cert = GitHub.dogstats.time("github.fastly.rpc", tags: ["method:get_certificate"]) do
      GitHub.fastly.get_certificate(certificate_id: fastly_certificate_id)
    end

    if cert.approved
      update_state(:approved)
      return :halt
    else
      return :retry
    end
  end

  # Can TLS be terminated with this certificate?
  #
  # Returns boolean.
  def usable?
    return false unless expires_at
    current_state == :approved && expires_at > Time.now
  end

  # Attempt to lock this record and call the given block.
  #
  # Returns the return value from the provided block. Raises LockedError if a
  # lock is already held elsewhere.
  def with_lock
    raise LockedError unless lock

    begin
      yield
    ensure
      unlock
    end
  end

  # Lock this record while performing work to prevent multiple jobs from
  # operating on the same record at the same time.
  #
  # Returns true if the lock was attained, false otherwise.
  def lock
    cache_lock_obtain("page-certificate:#{id}:lock")
  end

  # Unlock this record once done performing work.
  #
  # Returns nothing.
  def unlock
    cache_lock_release("page-certificate:#{id}:lock")
  end

  # Deletes the certificate from Fastly's servers & sets state such that re-upload is possible.
  #
  # If all is well, returns nothing.
  # Raises Fastly::CertificateDeletionError if it fails.
  def delete_certificate
    return unless fastly_certificate_id

    delete_certificate_from_fastly

    # This will move it back to a state where it could be re-issued & uploaded.
    update_state(:authorization_pending, fastly_certificate_id: nil)

    nil
  end

  # Whether the certificate is due for renewal.
  def needs_renewal?
    return unless expires_at
    expires_at < RENEWAL_WINDOW.from_now
  end

  # Reset the certificate back to the beginning of the flow.
  def reset_flow
    update_state(:bad_authz)
    return :proceed
  end

  def current_state
    state.to_sym
  end

  def platform_type_name
    "PageCertificate"
  end

  private

  def check_domain!
    raise BlacklistedError if self.class.blacklisted?(domain)
    raise DomainError unless self.class.eligible?(domain)
  end

  def update_state(state, other_fields = {})
    new_attrs = {state: state}

    [
      :expires_at,
      :challenge_path,
      :challenge_response,
      :earthsmoke_key_version_id,
      :fastly_privkey_id,
      :authorization_url,
      :certificate_url,
      :fastly_certificate_id,
      :order_url
    ].each do |attr|
      if other_fields.key?(attr)
        new_attrs[attr] = other_fields.delete(attr)
      end
    end

    new_attrs[:state_detail] = other_fields.to_json

    update!(new_attrs)
  end

  # We store a JSON serialized Hash of temporary state in the #state_detail
  # field. This method is a helper for accessing that data.
  #
  # Returns a Hash.
  def parsed_detail
    state_detail.nil? ? {} : JSON.parse(state_detail)
  end

  def set_initial_state
    self.state ||= :new
  end

  # Deletes the certificate from Fastly's servers.
  # Should ONLY be called on its own by Page::Certificate#destroy.
  #
  # If all is well, returns nothing.
  # Raises Fastly::CertificateDeletionError if it fails.
  def delete_certificate_from_fastly
    return unless fastly_certificate_id

    GitHub.dogstats.time("github.fastly.rpc", tags: ["method:delete_certificate"]) do
      GitHub.fastly.delete_certificate(certificate_id: fastly_certificate_id)
    end
  end

  # Internal: Raise an error if the domain is changed.
  #
  # Returns nothing.
  def fail_on_domain_change
    if domain_changed?
      raise AttemptedDomainChangeError
    end
  end
end
