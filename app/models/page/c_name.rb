# rubocop:disable Style/FrozenStringLiteralComment

class Page::CName
  attr_reader :page, :cname

  DOMAIN_BLACKLIST = %w{
    hackathonhowto.com
    githubflow.com
    py.codeconf.com
    man.github.com
    campus.github.com
    octoverse.github.com
    wyg.io
    enterprise-debs.github.com
    developers.github.com
    svnhub.com
    de61f2c9db59488d7d5418e44fd18148fdfaca68-cname-testing.github.com
    gameoff.github.com
    integrations-directory-staging.github.com
    mac.github.com
    windows.github.com
    github.co.jp
    octodex.github.com
    octicons.github.com
    codeconf.com
    octoc.at
    git-merge.com
    blog.speakerdeck.com
    git-lfs.github.com
    pages.github.com
    guides.github.com
    hubot.github.com
    og.github.com
    githubuniverse.com
    government.github.com
    hub.github.com
    githubengineering.com
    desktop.github.com
    services.github.com
    choosealicense.com
    developer.github.com
    vpn-ca.iad.github.com
    bounty.github.com
    help.github.com
    bear.ly
    b.logicalawesome.com
  }.map { |domain| [domain, "www.#{domain}"] }.flatten

  def initialize(page, cname)
    @page = page
    @cname = cname
  end

  # Public: Do the validation.  Will raise a InvalidCNAME
  # error if the CName is invalid.  Otherwise returns true.
  def validate
    validate_format
    validate_not_ip
    validate_uniqueness
    validate_not_github_com_cname
    validate_not_github_owned_pages
    true
  end

  # Check that the CNAME is in a valid format
  #
  # This is class level because it needs no state to check things.
  # It just does the Regexp validation.
  #
  # Raises InvalidCNAME if invalid
  def self.validate_format(cname)
    return if cname.blank?
    if cname !~ /\A[\w\d][\w\d\-\.]+[\w\d\-]\z/ || !cname.include?(".")
      msg = "The CNAME `#{cname}` is not properly formatted. "
      msg << "See #{self.cname_not_properly_formatted_url} for more information."
      raise Page::InvalidCNAME, msg
    end
  end

  def self.cname_not_properly_formatted_url
    "#{GitHub.help_url}/articles/troubleshooting-custom-domains/#github-repository-setup-errors"
  end

  private

  def validate_format
    self.class.validate_format(cname)
  end

  def validate_not_ip
    if cname =~ /\A\d+\.\d+\.\d+\.\d+\z/
      msg = "Your CNAME cannot be an IP address. "
      msg << "See #{self.class.cname_not_properly_formatted_url} for more information."
      raise Page::InvalidCNAME, msg
    end
  end

  # Ensure this page's CNAME isn't already taken.
  #
  # Raises InvalidCNAME
  def validate_uniqueness
    return if cname.blank?

    GitHub.dogstats.time "pages.check_cname" do
      # we need to check uniqueness of alt CNAMEs and second order alt CNAMEs
      # in both directions to prevent a case where two sites both have the same
      # alt CNAME. for example, if site A has a CNAME of example.com, and site
      # B has a CNAME of www.www.example.com, this will cause a conflict as
      # www.example.com will be the alt CNAME for both sites.

      cnames = [
        # direct CNAME conflict:
        cname,
        # www alt already exists for another CNAME
        "www.#{cname}",
        # this cname's alt conflicts with the www-less alt for a double-www'd cname
        "www.www.#{cname}",
      ]

      # www-less alt already exists for another CNAME
      if cname.start_with?("www.")
        cnames << cname[4..-1]
      end

      # this cname's www-less alt conflicts with the www'd alt for another cname
      if cname.start_with?("www.www.")
        cnames << cname[8..-1]
      end

      if Page.where(cname: cnames).any? { |p| p != page }
        raise Page::InvalidCNAME, cname_in_use_message
      end
    end
  end

  def cname_taken_help_url
    "#{GitHub.help_url}/articles/troubleshooting-custom-domains#cname-errors"
  end

  def cname_help_url
    "#{GitHub.help_url}/articles/setting-up-your-pages-site-repository/"
  end

  def validate_not_github_com_cname
    return unless cname.present?

    return if page.owner.login == "github"

    last_two = cname.split(".")[-2..-1].join(".")
    return unless %w(github.com github.io githubapp.com github.page).include?(last_two)

    repo_name = "#{page.owner}.#{GitHub.pages_host_name_v2}"
    names = [repo_name, "#{page.owner}.#{GitHub.pages_host_name_v1}"]

    if names.include?(cname) && names.include?(page.repository.name)
      raise Page::InvalidCNAME, "Your CNAME file was ignored because this repository is automatically hosted from #{repo_name} already. See #{cname_help_url}"
    else
      raise Page::InvalidCNAME, "You cannot use CNAMEs ending with github.io, github.com, or github.page. Instead, create a repository named #{repo_name}. See #{cname_help_url}"
    end
  end

  # Sometimes we have DNS pointed to pages where there is no backing pages site.
  # This allows people to squat on GitHub-owned domains. Sometimes this can be
  # convincing phishing attacks, but most of the time they're just a slight
  # annoyance.
  def validate_not_github_owned_pages
    if DOMAIN_BLACKLIST.include?(cname) && !github_owned_page?
      raise Page::InvalidCNAME, cname_in_use_message
    end
  end

  def github_owned_page?
    page.owner.login == "github"
  end

  def cname_in_use_message
    "The CNAME `#{cname}` is already taken. Check out #{cname_taken_help_url} for more information."
  end
end
