# frozen_string_literal: true

module Repository::PlanDependency
  # Public: Does the root repository plan support a gated feature?
  #
  # Note: We use plan_owner to check permissions and limits instead of owner.
  #       The repository permissions and limits needs to respect the parent repo
  #       permission when forked.
  # Note: We will fallback to a free tier if the `plan_owner` is disabled. That's
  #       what the `fallback_to_free: true` is for. That means, that when checking
  #       for supports/limit api on a repository, we will give the 'free plan'
  #       results if the `plan_owner` was disabled(i.e.: Billing issues)
  #       This fallback is important, because we will be gating repository
  #       functionality with this APIs. Thus, if `plan_owner` stop paying,
  #       it defaults back to the free tier permissions.
  #
  # feature - The feature Symbol to check (e.g. :codeowners)
  #
  # Returns a Boolean.
  def plan_supports?(feature, visibility: self.visibility)
    !!plan_owner&.plan_supports?(feature, visibility: visibility, fallback_to_free: true)
  end

  def async_plan_supports?(feature, visibility: async_visibility)
    visibility = Promise.resolve(visibility)
    Promise.all([async_plan_owner, visibility]).then do |plan_owner, visibility|
      !!plan_owner&.plan_supports?(feature, visibility: visibility, fallback_to_free: true)
    end
  end

  # Public: The root repository plan's owner limit for a gated feature.
  #
  # feature - The feature Symbol to check (e.g. :collaborators)
  #
  # See notes for `plans_supports?` method above.
  #
  # Returns an Integer.
  def plan_limit(feature, visibility: self.visibility)
    plan_owner&.plan_limit(feature, visibility: visibility, fallback_to_free: true).to_i
  end

  def async_plan_limit(feature, visibility: self.visibility)
    async_plan_owner.then do |plan_owner|
      plan_owner&.plan_limit(feature, visibility: visibility, fallback_to_free: true).to_i
    end
  end

  def has_seat_for?(user)
    !at_seat_limit? || member_ids.include?(user.id) || has_invitation_for?(user)
  end

  def filled_seats
    members_count + invitees.count
  end

  def seats
    plan_limit(:collaborators)
  end

  def private_collaborator_seats
    plan_limit(:collaborators, visibility: :private)
  end

  def available_seats
    [seats - filled_seats, 0].max
  end

  def available_private_seats
    private_collaborator_seats - filled_seats
  end

  def at_seat_limit?
    available_seats < 1
  end

  def part_of_unlimited_plan?
    plan_owner&.has_unlimited_private_repositories?
  end

  def custom_roles_supported?
    in_organization? &&
      GitHub.flipper[:custom_roles].enabled?(organization) &&
      plan_supports?(:custom_roles)
  end

  def fine_grained_permissions_supported?
    plan_supports?(:fine_grained_permissions)
  end

  def next_plan
    owner.organization? ? "GitHub Team" : "GitHub Pro"
  end
end
