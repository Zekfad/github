# frozen_string_literal: true

module Repository::NetworkPrivilegeDependency
  extend ActiveSupport::Concern

  included do
    has_one :network_privilege, class_name: "Stafftools::NetworkPrivilege", dependent: :destroy
  end

  # Public: Is this repository prevented from being indexed by search engines?
  #
  # Returns a Promise<Boolean>.
  def async_noindex?
    async_network_privilege.then do |privilege|
      next false unless privilege
      privilege.noindex?
    end
  end

  # Public: Is this repository prevented from being indexed by search engines?
  #
  # Returns a Boolean.
  def noindex?
    async_noindex?.sync
  end

  def set_network_privilege(type, value)
    self.network_privilege ||= Stafftools::NetworkPrivilege.new
    self.network_privilege[type] = value
    self.network_privilege.save!

    if type == :no_index
      context = event_context.merge({ hide_from_google: network_privilege[type] })
    else
      context = event_context.merge({ type => network_privilege[type] })
    end

    instrument type, context
  end

  # Public: Are users required to login to view this repository?
  #         Stafftools can revoke privilege of easy visibility.
  #
  # Returns a Promise<Boolean>.
  def async_require_login?
    async_network_privilege.then do |privilege|
      next false unless privilege
      privilege.require_login?
    end
  end

  # Public: Are users required to login to view this repository?
  #         Stafftools can revoke privilege of easy visibility.
  #
  # Returns a Boolean.
  def require_login?
    async_require_login?.sync
  end

  # Public: Are users required to opt-in to view this repository?
  #         Stafftools can revoke privilege of easy visibility.
  #
  # Returns a Promise<Boolean>.
  def async_require_opt_in?
    async_network_privilege.then do |privilege|
      next false unless privilege
      privilege.require_opt_in?
    end
  end

  # Public: Are users required to opt-in to view this repository?
  #         Stafftools can revoke privilege of easy visibility.
  #
  # Returns a Boolean.
  def require_opt_in?
    async_require_opt_in?.sync
  end

  # Public: Are only collaborators able to view this repository?
  #         Stafftools can revoke privilege of easy visibility.
  #
  # Returns a Promise<Boolean>.
  def async_collaborators_only?
    async_network_privilege.then do |privilege|
      next false unless privilege
      privilege.collaborators_only?
    end
  end

  # Public: Are only collaborators able to view this repository?
  #         Stafftools can revoke privilege of easy visibility.
  #
  # Returns a Boolean.
  def collaborators_only?
    async_collaborators_only?.sync
  end

  # Public: Is this repository hidden from the `/discover` and `/explore` pages?
  #
  # Returns a Promise<Boolean>.
  def async_is_hidden_from_discovery?
    async_network_privilege.then do |privilege|
      next false unless privilege
      privilege.hide_from_discovery?
    end
  end

  # Public: Is this repository hidden from the `/discover` and `/explore` pages?
  #
  # Returns a Boolean.
  def is_hidden_from_discovery?
    async_is_hidden_from_discovery?.sync
  end
end
