# rubocop:disable Style/FrozenStringLiteralComment

module Repository::Backup
  include GitBackups

  def backups_enabled?
    GitHub.realtime_backups_enabled?
  end

  # Perform a backup via gitbackups
  def backup_ng!
    if backups_enabled? && exists_on_disk?
      _run_backup_ng(false)
    end
  end

  # Queue a job to run git-backup on the repository
  def async_backup(backfill = false)
    return unless backups_enabled?
    return if backfill # this comes from the legacy sweeper

    async_backup_delayed(2.minutes)
  end

  # Queue a delayed job to back up the repository. Use this when updating our
  # own book-keeping branches.
  #
  # Legacy backups are still enqueued immediately.
  def async_backup_delayed(delay = 10.minutes)
    return unless backups_enabled?

    guid = GitHub::Jobs::RepositoryBackupNg.lock(id, "repository")
    at = Time.now.utc + delay.to_i
    GitHub.timed_resque.retry_at(GitHub::Jobs::RepositoryBackupNg, [id, :repository], at, 0, guid)
  end

  # Perform a backup via gitbackups
  def backup_wiki_ng!
    if backups_enabled? && unsullied_wiki && unsullied_wiki.exist?
      _run_backup_ng(true)
    end
  end

  # Queue a job to run git-backup-wiki on the repository
  def async_backup_wiki
    return unless backups_enabled?
    return if repository_wiki.nil?

    RepositoryBackupNgJob.perform_later(id, :wiki)
  end
end
