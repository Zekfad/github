# rubocop:disable Style/FrozenStringLiteralComment

# Allow us to set certain features based on if the repo is enabled for
# "preview_features" - this is most likely for private GitHub repos
#
# See app/models/user/feature_flags_dependency
# See also: http://martinfowler.com/bliki/FeatureToggle.html
module Repository::FeatureFlagsDependency
  def preview_features?
    return false unless GitHub.preview_features_enabled?
    (owner && owner.name == "github") && private?
  end

  # Public: Whether to show a dot indicating new activity on the Discussions tab
  # in a repository.
  def discussions_activity_indicator_enabled?
    if defined?(@discussions_activity_indicator_enabled)
      return @discussions_activity_indicator_enabled
    end
    @discussions_activity_indicator_enabled =
      GitHub.flipper[:discussions_activity_indicator].enabled?(self)
  end

  # Public: Are discussion notifications disabled for this repository?
  def disable_discussions_notifications_flag_enabled?
    if defined?(@disable_discussions_notifications_flag_enabled)
      return @disable_discussions_notifications_flag_enabled
    end
    @disable_discussions_notifications_flag_enabled =
      # Only return true if the repository has been explicitly added to the
      # actor's list. This disables the ability to fully enable the feature,
      # removing the risk of disabling discussion notifications to all
      # repositories at once.
      GitHub.flipper[:disable_discussions_notifications].actors_value.include?(self.flipper_id)
  end

  # Public: Is the Discussions feature enabled for this repository?
  def discussions_enabled?
    GitHub.flipper[:discussions_for_repo_users].enabled?(self)
  end

  # Public: Is the merge queue feature enabled for this repository?
  def merge_queue_enabled?
    GitHub.merge_queues_enabled? && GitHub.flipper[:merge_queue].enabled?(self)
  end

  # Public: Are Pull Request Revisions enabled for this repository?
  #
  # Returns a Boolean.
  def pull_request_revisions_enabled?
    return @pull_request_revisions_enabled if defined?(@pull_request_revisions_enabled)
    @pull_request_revisions_enabled = \
      plan_supports?(:draft_prs) &&
      (GitHub.flipper[:pull_request_revisions].enabled?(self) ||
      GitHub.flipper[:pull_request_revisions].enabled?(owner))
  end

  # Public: Should we use git rev-list ordering for pull request commits?
  #
  # Returns a Boolean.
  def use_rev_list_ordering?
    return @use_rev_list_ordering if defined?(@use_rev_list_ordering)
    @use_rev_list_ordering = GitHub.flipper[:use_rev_list_ordering].enabled?(self)
  end

  def prerelease_feature_enabled?(name)
    # Always disabled in Enterprise environments
    return false unless GitHub.preview_features_enabled?

    # If feature is explicitly enabled on this repo
    return true if GitHub.flipper[name].enabled?(self)

    # Always disable on public repos
    return false if public?

    # If the owner is opted in, enable for all of their private repos
    return true if private? && GitHub.flipper[name].enabled?(owner)

    # Otherwise the feature is disabled
    return false
  end

  # Bounty hunters try to attack repositories under the @GitHubBounty org to
  # find security vulnerabilities. The GitHubBounty/welcome repository is
  # excluded because it's used for communication with bounty hunters.
  def bounty_hunter_target?
    return false unless GitHub.preview_features_enabled?
    return false unless private?
    owner && owner.name == "GitHubBounty" && name != "welcome"
  end

  # Is the current repository enabled for aleph indexing
  def alephd_indexing_enabled?
    return false if alephd_skip_indexing?

    language_supported_by_alephd? && alephd_language_indexing_enabled?
  end

  def alephd_darkship_indexing_enabled?
    return false if alephd_skip_indexing?
    return false if alephd_indexing_enabled?

    darkship_language_supported_by_alephd? && alephd_darkship_language_indexing_enabled?
  end

  # Whether the current repository or its owner is enabled for geyser indexing. Note that this
  # does not check whether the user accessing the repo is opted in to EMS nor whether EMS is offline.
  # Also includes information which we can use during ingestion operations to report on why a
  # repository was rejected.
  def geyser_indexing_enabled_verbose
    @geyser_indexing_enabled_verbose ||= if !code_is_searchable?
      { determination: false, reason: "code is not searchable" }
    elsif geyser_denylisted?
      { determination: false, reason: "repository in denylist" }
    elsif !GitHub.flipper[:geyser_index].enabled?(self) && !GitHub.flipper[:geyser_index].enabled?(self.owner)
      { determination: false, reason: "repository not in allowlist" }
    else
      { determination: true, reason: nil }
    end
  end

  # Return the boolean determination from `geyser_indexing_enabled_verbose`
  def geyser_indexing_enabled?
    geyser_indexing_enabled_verbose[:determination]
  end

  # Whether the current repository or its owner is enabled for geyser indexing.
  # Skips filtering on conditions that would short circuit based on disabled status
  def geyser_indexing_mutable_access_enabled?
    # handle subset of repo_is_searchable? conditions
    return if network.nil? || network.broken? || !routed?

    # handle subset of code_is_searchable? conditions
    return if empty? || (fork? && !popular_fork?)

    return false if geyser_denylisted?

    GitHub.flipper[:geyser_index].enabled?(self) ||
      GitHub.flipper[:geyser_index].enabled?(self.owner)
  end

  # Return true if this repository is eligible for ingest into Cistern (git cache: https://github.com/github/cistern)
  #
  # All Geyser-enabled repositories are eligible for Cistern ingest because
  # Cistern ingest is required for Geyser indexing. To fully disable Cistern
  # ingest, disable `geyser_index` and `cistern_ingest` feature flags.
  def cistern_ingest_enabled?
    geyser_indexing_enabled? ||
    GitHub.flipper[:cistern_ingest].enabled?(self) ||
      GitHub.flipper[:cistern_ingest].enabled?(self.owner)
  end

  def structured_issue_comment_templates_enabled?
    return @structured_issue_comment_templates_enabled if defined?(@structured_issue_comment_templates_enabled)
    @structured_issue_comment_templates_enabled = GitHub.flipper[:structured_issue_comment_templates].enabled?(self)
  end

  # Feature flags to include in the post_receive push event
  def post_receive_instrumentation_feature_flags
    flags = alephd_push_feature_flags

    if cistern_ingest_enabled?
      flags << "cistern_ingest"
    end

    if token_scanning_service_ingest_enabled?
      flags << "token_scanning_service_ingest"
    end

    flags
  end

  def token_scanning_service_ingest_enabled?
    return false unless GitHub.flipper[:token_scanning_service].enabled?(self)
    return false unless cistern_ingest_enabled?
    return true if public?
    token_scanning_enabled?
  end

  def alephd_push_feature_flags
    return [] unless GitHub.flipper[:alephd_indexing].enabled?
    return [] unless (alephd_indexing_enabled? || alephd_darkship_indexing_enabled?)

    ["alephd_push_events"]
  end

  def alephd_language_indexing_enabled?
    language_percentages.map(&:first).any? { |language| GitHub.flipper[GitHub::Aleph.convert_language_name(language)].enabled?(self) }
  end

  def alephd_darkship_language_indexing_enabled?
    language_percentages.map(&:first).any? { |language| GitHub.flipper[GitHub::Aleph.convert_darkship_language_name(language)].enabled?(self) }
  end

  def language_supported_by_alephd?
    language_percentages.map(&:first).any? { |language| GitHub.flipper[GitHub::Aleph.convert_language_name(language)].exist? }
  end

  def darkship_language_supported_by_alephd?
    language_percentages.map(&:first).any? { |language| GitHub.flipper[GitHub::Aleph.convert_darkship_language_name(language)].exist? }
  end

  # Should aleph indexing be skipped for the current repository?
  def alephd_skip_indexing?
    GitHub.flipper[:alephd_skip_indexing].enabled?(self) ||
      GitHub.flipper[:alephd_skip_indexing].enabled?(self.owner)
  end

  # Is the current repository denylisted for geyser search indexing (will not be indexed)?
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def geyser_denylisted?
    @geyser_denylisted ||= begin
      GitHub.flipper[:geyser_denylist].enabled?(self) ||
        GitHub.flipper[:geyser_denylist].enabled?(self.owner)
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  module FeatureFlagMethods
    def async_math_filter_enabled?
      async_owner.then { math_filter_enabled? }
    end

    # https://github.com/github/projects/pull/17
    # Questions? ask @github/math
    def math_filter_enabled?
      preview_features?
    end
  end

  include FeatureFlagMethods
end
