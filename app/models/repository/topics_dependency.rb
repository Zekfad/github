# rubocop:disable Style/FrozenStringLiteralComment

module Repository::TopicsDependency
  extend ActiveSupport::Concern

  included do
    has_many :repository_topics, dependent: :destroy

    has_many :applied_repository_topics,
      -> { where(state: RepositoryTopic.applied_state_values) },
      class_name: "RepositoryTopic"

    has_many :topics, through: :applied_repository_topics, split: true
  end

  # Public: Returns an Array of String topics that the Munger service suggests
  # for this repository. Omits any suggestions that the user has already declined
  # or applied to the repository.
  #
  # Returns an Array of Strings on success or nil on error.
  def suggested_topic_names
    suggested_names = suggested_topic_names_from_munger
    return unless suggested_names

    # Remove existing tags on this repo
    suggested_names -= Topic.where(id: repository_topics.pluck(:topic_id)).pluck(:name)

    Topic.filter_flagged_names(suggested_names)
  end

  # Public: Apply the given topic names to this repository. If any topics on the repository
  # are omitted from this list, they will be removed.
  #
  # Returns true if all topics on the repository were updated to match the given list, or false
  # on error.
  def replace_topics(topic_names, user:)
    unless topic_names.present?
      applied_repository_topics.destroy_all
      invalidate_cache
      return true
    end

    remove_omitted_topics(topic_names)

    suggested_names = suggested_topic_names_from_munger || []
    results = topic_names.map do |name|
      state = suggested_names.include?(name) ? :suggested : :created
      Topic.apply_to_repository(name, repository: self, state: state, user: user)
    end

    invalidate_cache
    results.all?
  end

  def invalid_topic_names
    @invalid_topic_names ||= []
  end

  def update_topics(topic_names, user:)
    invalid_names = topic_names.reject { |name| Topic.valid_name?(name) }
    if invalid_names.any?
      error_message = "Topics must start with a lowercase letter or number, consist of #{Topic::MAX_NAME_LENGTH} characters or less, and can include hyphens."
      errors.add(:repository_topics, error_message)
      invalid_topic_names.concat(invalid_names)
      return false
    end

    limit = ::RepositoryTopic::LIMIT_PER_REPOSITORY
    if topic_names.size > limit
      error_message = "A repository cannot have more than #{limit} topics."
      errors.add(:repository_topics, error_message)
      invalid_topic_names.concat(topic_names[limit..-1])
      return false
    end

    replace_topics(topic_names, user: user)
  end

  # Public: Remove all topics from this repository whose name is not in the given list.
  #
  # Returns nothing.
  def remove_omitted_topics(names_to_keep)
    normalized_names = names_to_keep.map { |name| Topic.normalize(name) }

    topics_to_remove = topics
    if normalized_names.present?
      topics_to_remove = topics_to_remove.where(ActiveRecord::Base.sanitize_sql(["name NOT IN (?)", normalized_names]))
    end
    topics_to_remove = topics_to_remove.pluck(:id)

    ActiveRecord::Base.connected_to(role: :writing) do
      applied_repository_topics.where(topic_id: topics_to_remove).destroy_all
    end
  end

  # Public: Reject a topic by the given name for this Repository, for the given reason.
  #
  # name   - String topic, e.g., "node-js"
  # reason - Symbol or String reason for rejecting the topic; choose from :not_relevant,
  #          :too_specific, :too_general, and :personal_preference.
  # user   - User that declined the topic
  #
  # Returns a RepositoryTopic object when successful, and false otherwise.
  def decline_topic(name, reason:, user:)
    state = "declined_#{reason}".to_sym
    Topic.apply_to_repository(name, repository: self, state: state, user: user)
  end

  # Public: Are Munger-suggested topics supported for this repository?
  def topic_suggestions_enabled?
    !GitHub.enterprise? && !private? && !fork?
  end

  # Public: Returns an Array of String names of the Topics on this Repository.
  def topic_names
    @topic_names ||= topics.order(:name).pluck(:name)
  end

  # This attribute is for preloading topic names via GitHub::PrefillAssociations#for_repositories.
  attr_writer :topic_names

  private

  def suggested_topic_names_from_munger
    data_topics = GitHub.munger.topics_for_repository(self)
    return unless data_topics

    ordered_data_topics = data_topics.sort_by(&:suggestion_score).reverse
    normalized_topics = ordered_data_topics.map { |topic| Topic.normalize(topic.name) }
    normalized_topics.select { |name| Topic.valid_name?(name) }.uniq
  end

  def invalidate_cache
    touch
  end
end
