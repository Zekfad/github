# frozen_string_literal: true

module Repository::Sequence
  # Internal: Ensures a sequence is created for the repository.
  #
  # Returns nothing.
  def create_sequence
    ::Sequence.create(self)
    true
  end

  def self.included(base)
    base.send :extend, ClassMethods
  end

  module ClassMethods
    def setup_sequence
      after_create :create_sequence
    end
  end
end
