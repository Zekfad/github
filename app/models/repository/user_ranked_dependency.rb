# frozen_string_literal: true

module Repository::UserRankedDependency
  extend ActiveSupport::Concern
  include UserRanked::Scope

  class_methods do
    def compute_ranked_ids(user:)
      ranked_repos = user.repositories_contributed_to(
        limit: nil,
        exclude_owned: false,
        include_issue_comments: true,
        since: 1.year.ago,
        weighted: false,
        viewer: user,
      )

      ranked_repos.map(&:id)
    end
  end
end
