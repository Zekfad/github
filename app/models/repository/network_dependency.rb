# rubocop:disable Style/FrozenStringLiteralComment

# Network Dependency
#
# Holds convenience methods on Repository to deal with manipulating the
# associated RepositoryNetwork.
module Repository::NetworkDependency
  # Exception raised when an attempt is made to access the root repository but
  # no corresponding network record exists for this repository. This is a
  # serious data integrity error.
  class NetworkMissingError < RuntimeError
    def initialize(repo_id)
      super "Repository ##{repo_id} doesn't have a network"
    end
  end

  def set_network(network)
    self.network_id = network.id
    reset_git_cache
    if has_wiki?
      unsullied_wiki(true)
    end
  end

  def update_network(network)
    set_network(network)
    save

    sync_routes_from_network
  end

  def sync_routes_from_network
    # The new network is probably not on the same hosts as the old one.
    # So update the moved repo (and optional wiki) to match its new
    # network.
    nw_reader = GitHub::DGit::Routing.preferred_reader_for_network(network.id)

    GitHub::DGit::Maintenance.delete_repo_replicas_and_checksums(network.id, id)

    GitHub::DGit::Maintenance.insert_placeholder_replicas_and_checksums(
      GitHub::DGit::RepoType::REPO, id, network.id)

    repo_rr = GitHub::DGit::Routing.repo_replica_for_host(id, nw_reader)
    unless repo_rr.nil?
      repo_rr_rpc = repo_rr.to_route(original_shard_path).build_maint_rpc
      if repo_rr_rpc.exist?
        GitHub::DGit::Maintenance.recompute_checksums(self, nw_reader)
      end
    end

    refresh_wiki = !self.repository_wiki.nil?
    if refresh_wiki
      GitHub::DGit::Maintenance.insert_placeholder_replicas_and_checksums(
        GitHub::DGit::RepoType::WIKI, id, network.id)

      wiki_rr = GitHub::DGit::Routing.wiki_replica_for_host(id, nw_reader)
      unless wiki_rr.nil?
        wiki_rr_rpc = wiki_rr.to_route(wiki_shard_path).build_maint_rpc
        if wiki_rr_rpc.exist?
          GitHub::DGit::Maintenance.recompute_checksums(self, nw_reader, is_wiki: true)
        end
      end
    end

    dgit_reload_routes!
  end

  def sanitize_network_structure!(actor:)
    return unless private_fork?
    return unless parent # no parent means it's not a fork and the `parent_id` is pointing at nothing

    is_oopf = owner.organization? && GitHub.flipper[:disallow_oopfs].enabled?(actor)
    # Nested forks are always disallowed and must be reparented or extracted.
    is_nested_fork = parent.fork? && GitHub.flipper[:disallow_nested_private_forks].enabled?(actor)

    if is_oopf || is_nested_fork
      if network.root.valid_fork?(self)
        reparent!(network.root)
      else
        extract!
      end
    end
  end

  def valid_fork?(fork)
    return readable_by?(fork.owner) if fork.owner.user?
    !fork.invalid_org_owned_private_fork?
  end

  def invalid_org_owned_private_fork?
    return false unless parent
    return false unless owner.organization?
    return false unless private?
    !owner.same_business?(parent.owner)
  end

  def same_business?(repo)
    owner.organization? &&
    repo.owner.organization? &&
    owner.business &&
    repo.owner.business == owner.business
  end

  # Extract this repository and all forks into a new or existing network. This
  # method is fragile. If it fails, repositories may be left in a weird state.
  # It should be used sparingly and only run via RepositoryNetworkOperationsJob.
  #
  # network_id - The integer id of the network to transfer all repositories to.
  #              This defaults to nil for a new network.
  #
  # Returns nothing.
  def extract!(network_id = nil)
    network.extract!(self, new_network_id: network_id)
  end

  def new_extract!
    network.new_extract!(self)
  end

  def enough_space_to_extract?
    network.enough_space_to_extract?(self)
  end

  def enough_space_to_detach?
    network.enough_space_to_extract?(self, 1)
  end

  # Detach a repository, by pulling it and only it out into a whole new
  # network. The detached repository's child repositories remain in the original
  # network, but reparented under some other repository. The repository is moved
  # to a new location on disk (possibly over the network).
  def detach!
    network.detach!(self)
  end

  # Reattach a repository to its parent network, if it exists
  def reattach!
    network.reattach!
  end

  def can_attach_to?(repo)
    network.can_attach_to?(repo.network)
  end

  def attach_to!(repo, async: false)
    network.attach_to!(repo.network, async: async)
  end

  # Makes this repository the root of the network. This makes the existing
  # root repository a fork of this repository and moves the old root's forks
  # onto this repository. The plan owner for the network is also adjusted to
  # this repository's owner.
  #
  # Returns true if the operation was completed; false if the repo is already
  # root or the owner is at the private repo limit.
  def make_network_root!
    network.make_root!(self)
  end

  # Is this repository the only repository in the network?
  def sole_repo_in_network?
    network.repositories.count == 1
  end

  # The repository at the root of this network. The root repository is
  # maintained on the RepositoryNetwork this repository belongs to. Root
  # repositories don't have a parent and are typically the first repository in
  # the network.
  #
  # Returns a Repository instance.
  def root
    if network.nil?
      raise NetworkMissingError.new(self.id)
    elsif network.root_id == id
      self
    else
      network.root
    end
  end

  # Determine if this is the network root repo.
  def network_root?
    id == network.root_id
  end

  def network_broken?
    async_network_broken?.sync
  end

  def async_network_broken?
    async_network.then do |network|
      raise NetworkMissingError.new(self.id) if network.nil?
      network.maintenance_status == "broken"
    end
  end

  # Run "git janitor --fix" on this repository and its wiki (if it
  # exists). This cleans up obsolete files and some other kinds of
  # problems in the repository, raising a Repository::CommandFailed
  # exception if there are any problems in the repo that cannot be
  # fixed. Used before network extraction so that "git copy-fork"
  # never has to deal with obsolete files.
  def janitor_fix
    res = rpc.janitor(fix: true, type: "fork", ignore_hooks: !Rails.production?)
    if !res["ok"]
      raise Repository::CommandFailed.new("janitor", res["status"], res["out"] + res["err"])
    end

    if wiki_exists_on_disk?
      res = unsullied_wiki.rpc.janitor(fix: true, type: "wiki")
      if !res["ok"]
        raise Repository::CommandFailed.new("janitor", res["status"], res["out"] + res["err"])
      end
    end
  end

  # Intended to be called from RepositoryNetworkOperationsJob, this performs
  # the common search re-indexing logic when a repo moves to a different network.
  def reindex_after_network_operation(was_root, was_fork, old_network)
    reload

    repository_and_descendants.each do |repo|
      repo.reindex_issues(true)
      repo.reindex_discussions(true)
      repo.reindex_pull_requests(true)
    end

    if !was_fork && fork?
      # the Repository#synchronize_search_index hook will purge the code from the search index
      CommitContribution.clear_contributions(self)
    elsif was_fork && !fork?
      reindex_code
      reindex_commits
      CommitContribution.backfill!(self)
    end

    # the old network will be gone if the network operation was :reattach or :attach_to
    old_network = begin
      old_network.reload
    rescue ActiveRecord::RecordNotFound
      nil
    end

    # if the repo was the root of the old network and the old
    # network is still around, a new root will have been elected
    if old_network && was_root && network != old_network
      old_network.root.reindex_code
      old_network.root.reindex_commits
      CommitContribution.backfill(old_network.root)
    end
  end
end
