# rubocop:disable Style/FrozenStringLiteralComment

module Repository::AbilityDependency
  extend ActiveSupport::Concern

  included do
    after_create :add_members_with_default_repository_permissions
  end

  VALID_USER_REPO_ACTIONS = %i(write).freeze
  VALID_ORG_REPO_ACTIONS_AND_ROLES = %i(read write admin triage maintain).freeze

  class_methods do
    # Stores in the in-process PermissionCache each of the direct abilities
    # between a set of users and a set of repositories.
    def preload_repository_permissions(users:, repositories:)
      return unless users.present? && users.all?(&:persisted?)
      batch_size = 500
      user_ids = users.map(&:id)
      repository_ids = repositories.map(&:id)

      # Clear current value
      repository_ids.each do |repository_id|
        user_ids.each do |user_id|
          cache_key = repository_permission_cache_key(user_id, repository_id)
          PermissionCache.set(cache_key, nil)
        end
      end

      repository_ids.each_slice(batch_size) do |repository_ids|
        abilities = Authorization.service.most_capable_abilities_between_multiple_actors_and_subjects(
          actor_type: User,
          actor_ids: user_ids,
          subject_type: Repository,
          subjects: repositories,
        )

        # Set new values for each ability record
        abilities.each do |ability|
          cache_key = repository_permission_cache_key(ability.actor_id, ability.subject_id)
          PermissionCache.set(cache_key, ability)
        end
      end
    end

    def repository_permission_cache_key(user_id, repository_id)
      "RepositoryPermissions:User:#{user_id}:Repository:#{repository_id}"
    end

    # Public: Converts a Repository permission to the equivalent Ability action.
    #
    # action - Symbol or String action (pull, push, or admin).
    #
    # Returns an Ability action symbol
    def permission_to_action(action)
      case action && action.to_s.strip
      when "pull"
        :read
      when "triage"
        :triage
      when "push"
        :write
      when "maintain"
        :maintain
      when "admin"
        :admin
      else
        raise ArgumentError, "Invalid permission passed"
      end
    end

    # Given a user and a set of repository id's,
    # return the repository ids the user has (admin) access to due to the fact that
    # they are an admin of the repo's owning organization
    #
    # Returns an Array of id's
    def accessible_via_org_admin(user, repo_ids)
      # create a map between repo id and owning org id
      repos = Repository.where(id: repo_ids).includes(:parent, :owner)
      repo_to_org = repos.each_with_object({}) do |repo, hsh|
        if owning_org_id = repo.owning_organization_id
          hsh[repo.id] = owning_org_id
        end
      end

      owning_organization_ids = repo_to_org.values
      return [] if owning_organization_ids.empty?

      # orgs the user admins that are owners of the repos provided
      repo_owning_orgs_user_admins = Ability.user_admin_on_organization(
        actor_id: user.id,
        subject_id: owning_organization_ids.uniq,
      ).pluck(:subject_id)

      return [] if repo_owning_orgs_user_admins.empty?

      repo_to_org.reduce([]) do |adminable_repo_ids, (repo_id, owning_org_id)|
        adminable_repo_ids << repo_id if repo_owning_orgs_user_admins.include?(owning_org_id)
        adminable_repo_ids
      end
    end
  end

  def ability_description
    name_with_owner
  end

  def owning_organization_id
    # FORK SCENARIOS
    #
    # public repo is forked:
    # - org-owned repo --> user account - no org admins have access
    # - org-owned repo --> org account - fork's org admins have access
    #   - we need to use fork.owner_id instead of fork.organization_id
    #   - because if the fork's parent repo is transferred to another org
    #   - the parent repo's organization_id isn't updated
    #
    # private repo is forked:
    # org-owned repo --> user owner - parent repo org admins have access
    # org-owned repo --> org owner - new org admins have access
    # user-owned repo --> org owner - new org admins have access
    # user-owned repo --> user owner - n/a no orgs involved
    if fork?
      return if owner&.user? && parent&.public?
      return parent&.organization_id if owner&.user?
      return owner_id if owner&.organization?
    end
    # if it is not a fork at all,
    # we want to know the current repo's organization_id or owner.id
    # (which should be the same)
    if owner&.organization?
      # sometimes organization_id isn't populated after a user is transformed
      # to an organization for the repos they own
      organization_id || owner.id
    end
  end

  def owning_organization_type
    "Organization"
  end

  # Internal: Used by Abilities to ensure that only users or teams can be
  # granted abilities on a Repository.
  def grant?(actor, action)
    (actor.is_a?(User) || actor.is_a?(Team)) && super
  end

  # Internal: The minimum ability to grant to a new member.
  def baseline
    public? ? :write : :read
  end

  # Public: allow a team access to this repo.
  def add_team(team, action:)
    grant team, action
    # Ensure we update Vulnerability Alert access
    vulnerability_manager.update_user_or_team(team, action: action)
    RepositoryAdvisory::WorkspaceRepositoryManager.grant(team, repository: self, action: action)
    # Ensure we remove access to Protected Branches when team looses write access
    if action == :read
      ProtectedBranch::AbilityRepositoryManager.revoke(team, repository: self)
    end
  end

  # Public: remove access to this repo from a team.
  def remove_team(team)
    # Ensure we remove access to Vulnerability Alerts
    vulnerability_manager.revoke_user_or_team(team)
    revoke team
    RepositoryAdvisory::WorkspaceRepositoryManager.revoke(team, repository: self)
    # Ensure we remove access to Protected Branches
    ProtectedBranch::AbilityRepositoryManager.revoke(team, repository: self)
  end

  def readable_by?(actor, include_child_teams: false)
    if include_child_teams && actor.is_a?(Team)
       actor.direct_or_inherited_repo_ids.include?(id)
    else
      super(actor)
    end
  end

  def writable_by?(actor, include_child_teams: false)
    if include_child_teams && actor.is_a?(Team)
      actor.direct_or_inherited_repo_ids(action: [:write, :admin]).include?(id)
    else
      super(actor)
    end
  end

  # Public: Repository's access control consults more than just abilities.
  #
  # actor  - a User or Team, though it's generally a User.
  # action - a :read, :write, or :admin Symbol
  #
  # Returns a Boolean
  def permit?(actor, action)
    GitHub.dogstats.time("repository", tags: ["action:permit"]) do
      async_permit?(actor, action).sync
    end
  end

  # Public: Repository's access control consults more than just abilities.
  #
  # actor  - a User or Team, though it's generally a User.
  # action - a :read, :write, or :admin Symbol
  #
  # Returns Promise<bool>
  def async_permit?(actor, action)
    actor = actor.ability_delegate

    return Promise.resolve(false) if new_record?
    return Promise.resolve(false) if destroyed?
    return Promise.resolve(false) if deleted?
    return Promise.resolve(false) if actor&.ability_type == "Organization"

    # everything can read public repositories when private mode is disabled
    # an actor is always required when private mode is enabled
    return Promise.resolve(true) if public? && action == :read && (!GitHub.private_mode_enabled? || actor.present?)

    # an actor is required
    return Promise.resolve(false) if !actor

    # an actor is operating on something it owns
    return Promise.resolve(true) if actor&.ability_type == "User" && actor.id == owner_id

    chain = Promise.resolve
    if action == :read
      chain = chain.then do |value|
        next value if value
        # A user has access to an internal repository as a member of a business (this hits the DB)
        async_can_read_internal_repo_via_business_membership(actor)
      end
    end

    chain = chain.then do |value|
      next value if value

      # the actor's abilities allow it (this hits the DB), or
      super.then do |result|
        next true if result

        # the actor has unlocked the repo, or
        next true if (actor&.ability_type == "User" && actor.has_unlocked_repository?(self))

        # public push is enabled on a public repo (Enterprise only), or
        next true if (GitHub.public_push_enabled? && :write == action && public_push? && public?)

        # the actor is trying to read a fork of a private repo that's owned by
        # an org that the actor is an owner of
        actor&.ability_type == "User" && :read == action && async_owner_of_parent_org?(actor)
      end
    end
  end

  private def async_can_read_internal_repo_via_business_membership(actor)
    # A user has access to an internal repository as a member of a business (this hits the DB)
    async_internal_repository.then do |internal_repository|
      business_ids_promise = (actor&.ability_type == "User" && internal_repository) ? actor.async_business_ids : Promise.resolve([])

      business_ids_promise.then do |business_ids|
        internal_repository && business_ids.include?(internal_repository.business_id)
      end
    end
  end

  # Centralizes the logic used in Platform::Objects::Repository.async_viewer_can_see? so that
  # anyone can check permissions comprehensively on a repository loaded via an association to some
  # other object.
  #
  # See https://github.com/github/github/pull/109897/files#r265126550.
  #
  # Returns Promise<Boolean>.
  def async_visible_and_readable_by?(viewer)
    return Promise.resolve(true) if viewer&.site_admin? # Staff can read all metadata.

    return Promise.resolve(false) if access.disabled? # Don't show DMCA-takedown repos.
    return Promise.resolve(false) if access.tos_violation? || access.disabled_by_admin? # Don't show tos violated repos.

    Platform::Loaders::ActiveRecordAssociation.load(self, user_association_for_spammy).then do
      next false if hide_from_user?(viewer)

      resources.metadata.async_readable_by?(viewer).then do |readable|
        next true if readable
        next false unless viewer

        adminable_by?(viewer) || has_invitation_for?(viewer)
      end
    end
  end

  def visible_and_readable_by?(viewer)
    async_visible_and_readable_by?(viewer).sync
  end

  def async_access_level_for(actor, include_employee_granted_permissions: true)
    return Promise.resolve(nil) if new_record? || destroyed? || deleted?
    return Promise.resolve(nil) if actor.is_a?(User) && !actor.user?

    if actor.nil? || actor.new_record?
      # no actor is required if it's public and private mode is disabled
      if public? && !GitHub.private_mode_enabled?
        return Promise.resolve(:read)
      else
        return Promise.resolve(nil)
      end
    end

    # an actor is operating on something it owns
    return Promise.resolve(:admin) if actor.class == User && actor.id == owner_id

    chain = Promise.resolve

    if actor.class == User && include_employee_granted_permissions
      chain = chain.then do |value|
        next value if value

        # the actor has unlocked the repo for admin access
        actor.async_has_unlocked_repository?(self).then do |has_unlocked_repository|
          :admin if has_unlocked_repository
        end
      end
    end

    chain = chain.then do |value|
      next value if value

      actions = Set.new

      # the actor's abilities allow it.
      # This hits the DB if there's no information in the permission cache
      # which is currently being populated by `self.preload_repository_permissions`
      ability = PermissionCache.fetch(self.class.repository_permission_cache_key(actor.id, self.id)) do
        Authorization.service.most_capable_ability_between(actor: actor, subject: self)
      end

      actions << ability.action.to_sym if ability

      # public push is enabled on a public repo (Enterprise only)
      actions << :write if public_push?

      # public repos get read by default
      actions << :read if public?

      # return the max level if present
      level = actions.max_by { |action| Ability.actions[action] }
      next level if level

      async_owner_of_parent_org?(actor).then do |owner_of_parent_org|
        # the actor is trying to read a fork of a private repo that's owned by an
        # org that the actor is an owner of
        :read if owner_of_parent_org
      end
    end

    chain = chain.then do |value|
      next value if value
      # A user has access to an internal repository as a member of a business (this hits the DB)
      async_can_read_internal_repo_via_business_membership(actor).then do |can_read|
        :read if can_read
      end
    end
  end

  def cached_async_adminable_by?(user)
    @async_adminable_by_cache ||= {}
    @async_adminable_by_cache[user&.id] ||= async_adminable_by?(user)
  end

  def access_level_for(actor)
    async_access_level_for(actor).sync
  end

  # Public: calculate the direct role for the actor on the repository.
  # The role can be any of :read, :triage, :write, :maintain, :admin.
  #
  # Returns a Symbol or nil
  def direct_role_for(actor)
    direct_roles_for([actor], actor_type: actor.ability_type)[actor]
  end

  # Public: calculate the direct roles for the given actors on the repository.
  # The role can be any of :read, :triage, :write, :maintain, :admin.
  # If the actor doesn't have direct role over the repo, it won't be part of the result Hash.
  #
  # - actors: Enumerable of objects.
  # - actor_type: the type of the input actors. Must be a valid Ability.actor_type.
  #
  # Returns a Hash{actor => Symbol}
  def direct_roles_for(actors, actor_type: "Team")
    return {} if actors.blank?

    actor_ids = actors.map(&:ability_id)

    actors_abilities = Ability.where(
      actor_id: actor_ids,
      actor_type: actor_type,
      subject_id: self.id,
      subject_type: "Repository",
      priority: Ability.priorities[:direct],
    ).index_by(&:actor_id)

    actors_roles = UserRole.where(
      actor_id: actor_ids,
      actor_type: actor_type,
      target_id: self.id,
      target_type: "Repository",
    ).index_by(&:actor_id)

    actors.each_with_object({}) do |actor, result|
      actor_ability = actors_abilities[actor.id]
      next if actor_ability.nil?

      action = actor_ability.action.to_sym
      ability_rank = Ability::ACTION_RANKING[action]

      actor_role = actors_roles[actor.id]
      if actor_role.nil?
        result[actor] = action
        next
      end

      role = actor_role.role

      # A custom role will always take priority over a system role
      if role.custom? || role.action_rank > ability_rank
        result[actor] = role.name.to_sym
      else
        result[actor] = action
      end
    end
  end

  # Public: Is the specified user an owner of the parent repo's owning
  # organization?
  #
  # actor - User to check org ownership of.
  #
  # Returns a boolean
  def owner_of_parent_org?(actor)
    in_organization? && plan_owner.organization? && plan_owner.adminable_by?(actor)
  end

  def async_owner_of_parent_org?(actor)
    async_in_organization?.then do |in_organization|
      in_organization && plan_owner.organization? && plan_owner.adminable_by?(actor)
    end
  end

  # Public: Get all the IDs of users with privileged access to this repository.
  #
  # min_action: - Optional minimum action to restrict which grants are examined.
  #               Default is :read.
  #
  # Note: "Privileged access" is an Abilities term that refers to access gained
  # through any method other than the repository being public. A random user
  # who can see a public repository doesn't have privileged access to it, but
  # if they're added to a team that grants access to the repository (even if
  # it's still just read-only access), they now have privileged access to it.
  #
  # Returns an Array of Integers.
  def user_ids_with_privileged_access(min_action: :read, actor_ids_filter: nil)
    user_ids = actor_ids(type: User, min_action: min_action, actor_ids_filter: actor_ids_filter)

    # If this repo is user-owned, that user always has privileged access to it.
    if owner && owner.user? && (actor_ids_filter.nil? || actor_ids_filter.include?(owner_id))
      user_ids |= [owner_id]
    end

    user_ids
  end

  # Public: Get all of the IDs of users with direct access to this repository
  # that are also organization members.
  # action: - optional Symbol action (:read, :write, :admin) to limit actions
  # actor_ids: - optional Array filter to limit the user_ids we check
  #
  # NOTE: This explicitly omits outside collaborators; we select the intersection of
  # repository members and organization members here.
  #
  # For a user owned repository, this returns an empty array. All user owned repository
  # collaborators are outside collaborators
  #
  # Returns [id Integer...]
  def direct_org_member_ids(action: nil, actor_ids: nil)
    return [] unless owner&.organization?
    ability_action = nil
    role_to_filter_out = nil
    role_member_ids_to_remove = []
    role = nil

    # Figure out if action is specified, and if it's a legacy Ability action or a pre-defined
    # fine grained permission role
    if action.present?
      if ::Ability.actions[action]
        ability_action = action
        # :read and :write need to filter out :triage and :maintain, respectively
        role_to_filter_out = Role.find_by_name(::Role::BASE_ACTION_FOR_ROLE[ability_action])
      else
        role = Role.find_by_name(action)
      end

      # If neither ability_action nor role are defined, a valid action or role wasn't provided
      unless (ability_action || role)
        raise ArgumentError, "Invalid action for Repository#outside_collaborator_member_ids"
      end
    end

    # if a role is specified only query UserRole and return
    if role
      role_members = UserRole.where(role: role, target: self, actor_type: "User")
      # if actor_ids is specified, we only need the intersection of role_member_ids and actor_ids
      role_member_ids_to_remove = role_members.with_ids(Array.wrap(actor_ids), field: "actor_id") if actor_ids.present?

      return owner.member_ids(actor_ids: role_members.pluck(:actor_id))
    end

    direct_org_member_ids = direct_org_member_ability_scope(action: action, actor_ids: actor_ids).pluck(:actor_id)

    if role_to_filter_out
      role_member_ids_to_remove = UserRole.where(role: role_to_filter_out, target: self, actor_type: "User").pluck(:actor_id)
    end

    direct_org_member_ids - role_member_ids_to_remove
  end

  def direct_org_member_ability_scope(action: nil, actor_ids: nil)
    scope = Ability
      .annotate("abilities-join-audited")
      .joins("INNER JOIN abilities org ON org.actor_id = abilities.actor_id")
      .where(
        subject_id:   self.id,
        subject_type: "Repository",
        actor_type:   "User",
        priority:     Ability.priorities[:direct])
      .where("org.subject_type = 'Organization' and org.subject_id = :organization_id", organization_id: owner.id)

    if action.present?
      scope = scope.where(action: Ability.actions[action])
    end

    if actor_ids
      actor_ids = Array(actor_ids)
      scope = scope.with_ids(actor_ids, field: "abilities.actor_id")
    end

    scope.distinct
  end

  # Public: Get all of the IDs of organization outside collaborators with access to this repository.
  #
  # action: - optional Symbol action (:read, :write, :admin) to limit actions
  # actor_ids: - optional Array filter to limit the user_ids we check
  # Returns [id Integer...]
  def outside_collaborator_member_ids(action: nil, actor_ids: nil)
    return self.member_ids(action: action, actor_ids: actor_ids) unless owner&.organization?

    ability_action = nil
    role_to_filter_out = nil
    role_member_ids_to_remove = []
    role = nil

    # Figure out if action is specified, and if it's a legacy Ability action or a pre-defined
    # fine grained permission role
    if action.present?
      if ::Ability.actions[action]
        ability_action = action
        # :read and :write need to filter out :triage and :maintain, respectively
        role_to_filter_out = Role.find_by_name(::Role::BASE_ACTION_FOR_ROLE[ability_action])
      else
        role = Role.find_by_name(action)
      end

      # If neither ability_action and role are defined, a valid action or role wasn't provided
      unless (ability_action || role)
        raise ArgumentError, "Invalid action for Repository#outside_collaborator_member_ids"
      end
    end

    if role
      role_member_ids = UserRole.where(role: role, target: self, actor_type: "User")
      # if actor_ids is specified, we only need the intersection of role_team_ids and actor_ids
      role_member_ids = role_member_ids.with_ids(Array.wrap(actor_ids), field: "actor_id") if actor_ids.present?

      return role_member_ids.pluck(:actor_id) - owner.member_ids
    end

    if role_to_filter_out
      role_member_ids_to_remove = UserRole.where(role: role_to_filter_out, target: self, actor_type: "User").pluck(:actor_id) if role_to_filter_out
    end

    outside_collaborator_member_ids = outside_collaborator_ability_scope(action: action, actor_ids: actor_ids).pluck(:actor_id)
    outside_collaborator_member_ids - role_member_ids_to_remove
  end

  def outside_collaborator_ability_scope(action: nil, actor_ids: nil)
    org_join = ActiveRecord::Base.sanitize_sql_for_conditions(["LEFT JOIN abilities org
      ON org.actor_id       = abilities.actor_id
      AND org.subject_type  = 'Organization'
      AND org.subject_id    = :organization_id", organization_id: owner.id])
    scope = Ability
      .annotate("abilities-join-audited")
      .joins(org_join)
      .where(
        subject_id:   self.id,
        subject_type: "Repository",
        actor_type:   "User",
        priority:     Ability.priorities[:direct])
      .where("org.actor_id IS NULL")

    if action.present?
      scope = scope.where(action: Ability.actions[action])
    end

    if actor_ids
      actor_ids = Array(actor_ids)
      scope = scope.with_ids(actor_ids, field: "abilities.actor_id")
    end

    scope.distinct
  end

  # Checks if user is a member of the repo or of the owning org
  def async_has_access?(actor)
    self.async_owner.then do |owner|
      next true if owner == actor

      self.async_member?(actor).then do |member|
        next true if member
        next owner.async_member?(actor) if owner.is_a?(Organization)
        next false
      end
    end
  end

  # Internal: Add organization members with default repository permissions
  #
  # This happens via the organization `dependent_added` callback
  #
  # Returns nothing
  def add_members_with_default_repository_permissions
    return unless in_organization?

    organization.dependent_added(self)
  end

  # Public: Should default repository permissions be propagated from
  #   the owning organization to this repository?
  #
  # Returns Boolean
  def receives_organization_default_repository_permission?
    !advisory_workspace?
  end

end
