# frozen_string_literal: true

module Repository::RepositoryFundingLinksDependency
  extend ActiveSupport::Concern
  include Configurable::RepositoryFundingLinks

  # True if the User or Organization that owns the repository has enabled
  # the funding links and is in the feature flag.
  def can_enable_repository_funding_links?
    GitHub.sponsors_enabled? && !has_any_trade_restrictions?
  end

  # True if the repository can use funding links and the user can write
  # a funding file
  def can_write_funding_file?
    return false unless can_enable_repository_funding_links?

    # Handle e.g. the case where .github is already a file that exists
    # so a new .github directory cannot be created
    valid_file_path?(funding_links_path)
  end

  # True if the repository can use funding links and has a funding file
  def has_funding_file?
    async_has_funding_file?.sync
  end

  # Public: Does this repository have a funding links file?
  #
  # Returns a Promise<Boolean>.
  def async_has_funding_file?
    async_preferred_funding.then { |funding_file| funding_file.present? }
  end

  # True if the repository's funding file is inherited from a different repo
  def inheriting_global_funding_file?
    return false if global_health_files_repository? # we ARE the global funding repo
    return false unless has_funding_file?
    preferred_funding.repository != self
  end

  # True if the repository has a local funding file AND a global funding file in its org
  def overriding_global_funding_file?
    return false if global_health_files_repository? # we ARE the global funding repo
    return false unless global_preferred_funding.present?
    preferred_funding != global_preferred_funding
  end

  # Returns FUNDING.yml in the root if the repo is a .github org repo.
  # Otherwise, returns .github/FUNDING.yml where .github is a directory.
  def funding_links_path
    global_health_files_repository? ? FundingLinks::FILENAME : FundingLinks::PATH
  end

  def funding_links
    @funding_links ||= FundingLinks.for(repository: self)
  end

  def funding_links_to_hydro
    @funding_links_to_hydro ||= begin
      funding_links.validated_config.each_with_object(Array.new) do |links, result|
        platform_key, value = links
        accounts = Array(value)

        if platform = FundingPlatforms.find(platform_key)
          accounts.each do |account|
            result << FundingPlatforms.to_hydro(platform, account)
          end
        end
      end.compact
    end
  end

  def has_funding_platforms?
    return false unless has_funding_file?

    funding_links.validated_config.any?
  end

  def funding_links_stafftools_kv_prefix
    "stafftools_repository_funding_links_disabled_#{self.id}"
  end

  def funding_links_stafftools_disabled?
    ActiveRecord::Type::Boolean.new.cast(
      GitHub.kv.get(funding_links_stafftools_kv_prefix)
               .value { false },
    )
  end

  def global_funding_file_repository_funding_links_enabled?
    global_repository = preferred_funding&.repository
    return false if global_repository.blank?
    return true if global_repository.repository_funding_links_explicitly_enabled?

    if global_repository.repository_funding_links_unset? &&
      !global_repository.fork? &&
       global_repository.has_funding_file?
      return true
    end

    false
  end

  def repository_funding_links_enabled?
    return true if repository_funding_links_explicitly_enabled?

    if repository_funding_links_unset?
      if inheriting_global_funding_file? && global_funding_file_repository_funding_links_enabled?
        return true
      end

      if !fork? && has_funding_file?
        return true
      end
    end

    false
  end

  def show_sponsor_button?
    return false unless GitHub.sponsors_enabled?
    return false if funding_links_stafftools_disabled?
    return false if has_any_trade_restrictions?
    return false unless repository_funding_links_enabled?
    return false if global_health_files_repository?
    return false unless has_funding_file?

    true
  end
end
