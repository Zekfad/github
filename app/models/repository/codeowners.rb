# frozen_string_literal: true

require "codeowners"

class Repository
  # Represents the OWNERS file in a repository. Owners are responsible for
  # reviewing pull requests that change files in directories they own. An
  # owner's review can be required before merging into a protected branch
  # is allowed.
  class Codeowners
    include Enumerable

    attr_reader :repository, :ref, :file
    attr_accessor :paths

    delegate :parsed?, to: :file, allow_nil: true

    def initialize(repository, ref: nil, paths: [])
      @repository = repository
      @ref = ref || repository.default_branch
      @paths = paths

      @tree_entry = PreferredFile.find(
        directory: @repository.directory(@ref),
        type: :codeowners,
      )
      if @tree_entry
        file_content = GitHub::Encoding.strip_bom(@tree_entry.data.to_s)
        @file = ::Codeowners::File.new(file_content)
      end

      @rules_for_paths = {}
    end

    def path
      @tree_entry && @tree_entry.path
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def tree_oid
      @tree_oid ||= repository.refs.find(ref)&.target_oid
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def each(&block)
      owners.each(&block)
    end

    # Public: Returns a Hash of owners (Users and/or Teams) mapped to the rules
    # declaring them so.
    #
    # TODO: return rules as Hashes, Let's keep Codeowners gem classes constrained to this file.
    #
    # Example:
    #
    #   codeowners.rules_by_owner
    #   =>  {
    #         <#User> => [
    #           <#Codeowners::Rule line: 1 pattern: <#Owners::Pattern to_s: "*">>,
    #           <#Codeowners::Rule line: 42 pattern: <#Owners::Pattern to_s: "*.js">>,
    #         ],
    #         <#Team> => [
    #           <#Codeowners::Rule line: 42 pattern: <#Owners::Pattern to_s: "*.js">>,
    #         ]
    #       }
    #
    # Returns a Hash.
    def rules_by_owner
      return {} unless parsed?
      return {} unless repository.plan_supports?(:codeowners)
      return @rules_by_owner if @rules_by_owner

      owners_for = owners_from_file

      by_username = users_by_username(logins(owners_for))
      by_email    = users_by_email(emails(owners_for))
      by_teamname = teams_by_teamname(team_slugs(owners_for))

      found_by_owner = {}
      inject_found_owners!(by_username, source: owners_for, target: found_by_owner)
      inject_found_owners!(by_email,    source: owners_for, target: found_by_owner)
      inject_found_owners!(by_teamname, source: owners_for, target: found_by_owner)

      allowed_owners = filter_owners_with_write_access(found_by_owner.keys)
      @rules_by_owner = found_by_owner.slice(*allowed_owners)
    end

    def owners_by_rule
      return @owners_by_rule if @owners_by_rule

      by_owner = rules_by_owner
      owners = by_owner.keys
      rules = by_owner.values.flatten.uniq

      # Order by CODEOWNERS file precedence.
      rules.sort_by! { |rule| -rule.line }

      @owners_by_rule = rules.each_with_object({}) do |rule, by_rule|
        by_rule[rule] = owners.select { |owner| by_owner[owner].include?(rule) }
      end
    end

    def include?(owner)
      rules_by_owner.key?(owner)
    end

    # Public: Determines which owners are affected by changes to these paths.
    #
    # Returns an Array of Users and Teams.
    def owners
      rules_by_owner.keys
    end

    def users
      owners.select { |owner| owner.is_a?(User) }
    end

    def teams
      owners.select { |owner| owner.is_a?(Team) }
    end

    # Public: Return the paths that are owned by the given owner, including
    #         any teams they are a member of if the given owner is a User.
    #
    # owner - A User or Team.
    #
    # Returns an Array of file path Strings.
    def paths_for_owner(owner)
      return [] unless owner

      @paths_for_owner ||= {}
      @paths_for_owner[owner.id] ||= direct_paths_for_owner(owner).tap do |paths|
        paths.concat(paths_for_user_from_teams(owner)) if owner.respond_to?(:login)
      end
    end

    def owned_by?(owner:, path:)
      paths_for_owner(owner).include?(path)
    end

    def owners_for_path(path, unknown_path_value: [])
      @owners_for_path ||= paths.each_with_object({}) do |filepath, result|
        result[filepath] ||= []

        rule, owners = owners_by_rule.find { |rule, _| rule.pattern.match?(filepath) }
        if rule.present?
          @rules_for_paths[filepath] ||= rule
          result[filepath] = Array.wrap(owners)
        end
      end
      @owners_for_path[path] || unknown_path_value
    end

    def rule_for_path(path)
      @rules_for_paths[path] ||= begin
        rule, _ = owners_by_rule.find { |rule, _| rule.pattern.match?(path) }
        rule
      end
    end

    private

    def owners_from_file
      file.for_with_tree(paths)
    end

    def logins(owners)
      usernames = owners.keys.select(&:username?).map(&:identifier)
      usernames.map { |identifier| identifier[1..-1] }
    end

    def users_by_username(logins)
      users = User.where(login: logins).order(:login)

      users.inject({}) do |by_username, user|
        username = "@#{user.login}"
        by_username.merge username => user
      end
    end

    def emails(owners)
      owners.keys.select(&:email?).map(&:identifier)
    end

    def users_by_email(emails)
      emails = emails.select { |e| GitHub::UTF8.valid_email?(e) }
      emails.uniq!

      user_emails = UserEmail.where(email: emails).includes(:user)
      user_emails = user_emails.verified if GitHub.email_verification_enabled?

      user_emails.each_with_object({}) do |user_email, result|
        result[user_email.email] = user_email.user
      end
    end

    # PRIVATE: Finds teams mentioned in the owners file. Ignores teams if the
    # repository is not in an organization OR if the team belongs to an outside
    # organization.
    #
    # owners - An Array of Codeowners::Owner objects
    #
    # Returns an Array of team slugs.
    def team_slugs(owners)
      return [] unless repository.owner.organization?

      combined_slugs = owners.keys.select(&:teamname?).map(&:identifier)
      combined_slugs = combined_slugs.map { |identifier| identifier[1..-1] }

      combined_slugs.each_with_object([]) do |combined, org_teams|
        org, slug = combined.split("/", 2)

        org_teams << slug if org.casecmp?(repository.owner.login)
      end
    end

    def find_teams(slugs)
      return [] unless repository.owner.organization?
      return [] unless slugs.any?

      repository.owner.teams.where(slug: slugs).order(:slug)
    end

    def teams_by_teamname(slugs)
      teams = find_teams(slugs)

      teams.inject({}) do |by_teamname, team|
        teamname = "@#{team.combined_slug}"
        by_teamname.merge teamname => team
      end
    end

    def inject_found_owners!(found_by_identifier, source:, target:)
      found_by_identifier.each_with_object(target) do |(identifier, user_or_team), coll|
        _, rules = source.find { |(owner, _)| owner.identifier.casecmp?(identifier) }
        next unless rules

        coll[user_or_team] ||= []
        coll[user_or_team] += rules
      end
    end

    def filter_owners_with_write_access(users_and_teams)
      users, teams = users_and_teams.partition { |owner| owner.is_a?(User) }

      allowed_user_ids = repository.user_ids_with_privileged_access(min_action: :write, actor_ids_filter: users.map(&:id))
      allowed_team_ids = team_ids_with_direct_or_indirect_write_access(teams)

      users_and_teams.select do |owner|
        if owner.is_a?(User)
          allowed_user_ids.include?(owner.id)
        else
          allowed_team_ids.include?(owner.id)
        end
      end
    end

    def direct_paths_for_owner(owner)
      rules = rules_by_owner[owner]
      return [] unless rules

      paths.select do |path|
        rules.any? { |r| r.pattern.match?(path) }
      end
    end

    def paths_for_user_from_teams(user)
      return [] unless user.present?

      team_rules = []

      user_team_ids = user.team_ids(with_ancestors: true)
      return [] unless user_team_ids.any?

      teams.each do |team|
        next unless user_team_ids.include?(team.id)
        next unless rules = rules_by_owner[team]
        team_rules += rules
      end

      return [] unless team_rules.any?

      paths.select do |path|
        team_rules.any? { |r| r.pattern.match?(path) }
      end
    end

    # Private: Given a list of teams, returns an Array of IDs for the teams
    # which have write access. Either directly or indirectly from a parent team.
    #
    # teams     - An Array of Teams.
    #
    # Returns an Array of Team IDs.
    def team_ids_with_direct_or_indirect_write_access(teams)
      return [] unless repository.in_organization?
      return [] if teams.empty?

      ancestor_map = teams.each_with_object({}) do |team, map|
        map[team.id] = team.ancestor_ids
      end

      ids_with_write_access = repository.actor_ids \
        type: Team,
        min_action: :write,
        actor_ids_filter: ancestor_map.flatten(2)

      ancestor_map.each_with_object([]) do |(team_id, ancestor_ids), has_access|
        direct_access = ids_with_write_access.include?(team_id)
        indirect_access = (ancestor_ids & ids_with_write_access).any?

        has_access << team_id if direct_access || indirect_access
      end
    end
  end
end
