# frozen_string_literal: true

module Repository::TokenScanningDependency
  extend ActiveSupport::Concern

  included do
    has_one :token_scan_status, dependent: :destroy
    accepts_nested_attributes_for :token_scan_status
    has_many :token_scan_results
    destroy_dependents_in_background :token_scan_results
  end

  # Used for min-bar public check for user agnostic job level checks outside of UI, and a small set of special case UI paths.
  def token_scanning_enabled?
    return false unless token_scanning_ux_enabled?
    !token_scanning_manually_disabled?
  end

  # Covers common pre-req checks. Only public as an injection point for UTs.
  def token_scanning_ux_enabled?
    return false unless token_scanning_ux_feature_enabled?
    !token_scanning_staff_disabled?
  end

  # Covers the most basic of common pre-req checks.
  private def token_scanning_ux_feature_enabled?
    # check if the configuration for the current environment support scanning
    # both are on by default for dotcom and needs to be enabled for enterprise
    return false unless GitHub.configuration_supports_token_scanning? && GitHub.configuration_advanced_security_enabled?

    if !GitHub.enterprise?
      # we dont support UI for public repos in dotcom
      return false if process_found_tokens_externally?

      # if the org is opted into advanced security beta in dotcom, we allow the UX.
      return true if owner.organization? && owner.advanced_security_private_beta_enabled?
    end

     # billing/license check (applies to both GHES and dotcom). We only support for orgs not users in case of GHES as well.
     owner.organization? && owner.advanced_security_enabled?
  end

  # Used to regulate Stafftools token scanning experience.
  # Unlike admin, staff checks do not gate on concerns such as user authorization.
  def show_token_scanning_stafftools_ui?
    token_scanning_ux_feature_enabled?
  end

  # describes if the token scanning admin or report experience is reachable by user.
  def show_token_scanning_ui?(user)
    return false unless token_scanning_ux_enabled?

    # Tie token scanning visibility to being authorized for vulnerability alerts
    vulnerability_alerts_authorized_for?(user)
  end

  # describes if token scanning results are presented.
  def show_token_scanning_results?(user)
    return false unless show_token_scanning_ui?(user)
    !has_pending_backfill?
  end

  # determines if a token should be marked excluded based on the config file
  def token_in_config_excluded_path?(token_path)
    return false if !config_file

    @token_scanning_config ||= TokenScanningConfigurationFile.new(@token_scanning_yml_file)
    @token_scanning_config.ignore_path?(token_path)
  end

  def config_file
    @token_scanning_yml_file ||= PreferredFile.find(directory: self.root_directory, type: :token_scanning_configuration, subdirectories: [".github"])
  end

  def process_found_tokens_externally?
    !!public?
  end

  # Get the users to notify of the token scan results found.
  #
  # This currently selects all users with access to repository vulnerability
  # alerts that meet either of these two conditions:
  #
  #   - The user is subscribed to email-based vulnerability alert digests
  #   - The user is subscribed to email-based vulnerability alert notifications
  #
  # Returns an array of User objects to deliver notifications to
  def token_scanning_users_to_notify
    return [] unless token_scanning_enabled?

    User.where(id: vulnerability_manager.authorized_user_ids).select do |user|
      next true if NewsletterSubscription.active.where(user: user, name: "vulnerability").exists?
      settings = GitHub.newsies.settings(user)
      settings.success? && settings.vulnerability_email
    end
  end

  # Encapsulation function for view level read-side checks to verify that the repo doesn't need a backfill scan.
  # This implementation is responsible for consistency about how we enforce backfill checks, and ensure legacy repos are not left behind without scans.
  def has_pending_backfill?
    # The following flag is an off by default pressure valve to allow specific reports to be shown if a backfill scan is blocked.
    return false if GitHub.flipper[:ignore_token_scanning_backfill].enabled?(self)
    entry = self.token_scan_status
    if (entry.nil? && self.token_scanning_enabled?)
      # Token scan status entries should be materialized before user is able to reach report page, if token scanning is enabled.
      GitHub.dogstats.increment("token_scan.backfill.entry_missing")
      # At this step, ensure a status entry is present, so legacy repo will be scanned.
      # This covers for repos which were enabled for token scanning before admin event integration, and which were not covered by an org-backfill.
      ActiveRecord::Base.connected_to(role: :writing) do
        self.ensure_current_token_scan_status_entry!
      end
    end
    # if we don't have an entry, or it isn't completed the repo needs to be backfilled.
    entry.nil? || !entry.completed?
  end

  # Used for re-evaluating and updating scan state of repos for backfill coverage/cleanup
  # as they transition into and out of eligibility for private token scanning.
  def ensure_current_token_scan_status_entry!
    if self.token_scanning_enabled?
      if self.token_scan_status && self.token_scan_status.scan_state == "non_qualifying_repo"
        self.token_scan_status.destroy
        self.reload
      end
      TokenScanStatus.ensure_status_entry_for_repo!(self)
    else
      if self.token_scan_status && self.token_scan_status.scan_state != "non_qualifying_repo"
        self.token_scan_status.update!(scan_state: :non_qualifying_repo)
      end
    end

    # Backfill and cleanup requires eventual processing by the scheduler which runs on regular interval timer in production.
    # For local development environments which do not maintain timers, we request an on-demand scheduler pass, to approximate normal processing.
    #
    # Note: If we want to move away from this, there are a couple alternatives:
    #
    # 1- Require devs to manually queue up the scheduler when making admin changes to a repo
    #   This approach would be very burdensome for day to day dev-testing, without providing a meaningful product quality benefit.
    #
    # 2- Bypass requirement for backfill scans being completed in dev environments to show token scan results.
    #   This approach creates a significant functionality gap between production and dev of an important and non-trivial behavior which needs to stay visibile.
    if Rails.development?
      TokenScanningSchedulerJob.perform_later
    end
  end

  def token_scanning_service_enabled?
    GitHub.flipper[:token_scanning_service].enabled?(self)
  end

  def use_token_scanning_service?(user)
    token_scanning_service_enabled? && GitHub.flipper[:token_scanning_service].enabled?(user)
  end

  def token_scanning_service_unresolved_count
    return @token_scanning_service_unresolved_count if defined? @token_scanning_service_unresolved_count

    @token_scanning_service_unresolved_count = GitHub::TokenScanning::Service::Client.get_token_counts(
      repository_id: self.id,
    )&.data&.unresolved_count || 0
  end
end
