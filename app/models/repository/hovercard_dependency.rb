# frozen_string_literal: true

module Repository::HovercardDependency
  extend ActiveSupport::Concern
  include UserHovercard::SubjectDefinition

  def user_hovercard_parent
    async_organization.sync
  end

  HOVERCARD_COMMIT_WINDOWS = {day: 1.day, week: 1.week, month: 1.month}

  included do
    define_user_hovercard_context :owner, ->(user, viewer, descendant_subjects:) do
      if readable_by?(viewer)
        Hovercard::Contexts::Custom.new("Owns this repository", "repo") if owner_id == user.id
      end
    end

    define_user_hovercard_context :contributions, ->(user, viewer, descendant_subjects:) do
      next unless readable_by?(viewer)

      # the scope for the user commit contributions in this repo
      commit_contribution_scope = commit_contributions.where(user_id: user.id)
      next if commit_contribution_scope.none?

      # go through each window in order and choose the first that passes the commit cutoff
      fact = HOVERCARD_COMMIT_WINDOWS.lazy.map do |duration_name, duration|
        if commit_contribution_scope.where("committed_date > ?", duration.ago).any?
          " in the past #{duration_name}"
        end
      end.detect(&:present?)

      Hovercard::Contexts::Custom.new("Committed to this repository#{fact}", "git-commit")
    end
  end
end
