# frozen_string_literal: true

# Template and cloning-specific functionality for repositories
module Repository::TemplateDependency
  extend ActiveSupport::Concern

  included do
    scope :templates, -> { where(template: true) }

    scope :templates_relevant_to, ->(user) do
      relevant_owner_ids = [user.id]
      relevant_owner_ids.concat(user.organization_ids) if user.user?

      recent_template_ids = user.recently_used_template_repository_ids

      if recent_template_ids.any?
        templates.where("repositories.owner_id IN (?) OR repositories.id IN (?)", relevant_owner_ids,
                        recent_template_ids)
      else
        templates.where(owner_id: relevant_owner_ids)
      end
    end
  end

  # Public: The repository from which this repository was cloned, if any.
  #
  # Returns a Repository or nil.
  def template_repository
    @template_repository ||= async_template_repository.sync
  end

  # This attribute is for preloading template repositories via
  # GitHub::PrefillAssociations#for_repositories.
  attr_writer :template_repository

  def async_template_repository
    async_template_repository_clone.then do |tmpl_repo_clone|
      if tmpl_repo_clone
        tmpl_repo_clone.async_template_repository.then do |tmpl_repo|
          tmpl_repo if tmpl_repo&.disabled_at.nil?
        end
      end
    end
  end

  # Public: Whether this repository is not yet finished being cloned from a template repository.
  #
  # Returns a Boolean.
  def cloning_from_template?
    template_repository_clone&.cloning? || clone_errored?
  end

  # Public: Whether this repository could not finish being cloned from a template repository because
  # an error occurred.
  #
  # Returns a Boolean.
  def clone_errored?
    template_repository_clone&.error?
  end

  # Public: The reason this repository could not be cloned from a template, if it was generated from
  # a template and failed in the process.
  #
  # Returns a RepositoryClone.error_reason_codes (or nil).
  def clone_error_reason_code
    template_repository_clone&.error_reason_code
  end

  # Public: The owner of the template repository this repository was cloned from
  #
  # Returns a User, Organization, or nil.
  def clone_owner
    template_repository_clone&.template_owner
  end


  # Public: Whether this repo was cloned from a template repository and is finished being cloned.
  #
  # Returns a Boolean.
  def finished_cloning_from_template?
    template_repository_clone&.finished?
  end
end
