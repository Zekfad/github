# frozen_string_literal: true

module Repository::DependabotDependency
  extend ActiveSupport::Concern
  include Configurable::RepositoryDependencyUpdates
  include Configurable::RepositoryVulnerabilityAlerts

  included do
    has_many :dependency_updates, class_name: :RepositoryDependencyUpdate,
      inverse_of: :repository
    destroy_dependents_in_background :dependency_updates
  end

  # TODO: Remove after deploy to handle missing methods in mailer jobs
  def dependabot_visible_to?(actor)
    automated_security_updates_visible_to(actor)
  end

  # TODO: Remove after deploy to handle missing methods in mailer jobs
  def dependabot_configurable_by?(actor)
    automated_security_updates_configurable_by?(actor)
  end

  # Whether the give actor is allowed to opt in/out of Dependabot security updates in the UI and
  # the API
  def automated_security_updates_configurable_by?(actor)
    automated_security_updates_visible_to?(actor) && vulnerability_alerts_enabled?
  end

  # Whether to show Dependabot security updates to the given actor in the UI
  def automated_security_updates_visible_to?(actor)
    return false unless GitHub.dependabot_enabled?
    return false unless actor

    automated_security_updates_authorized_for?(actor)
  end

  # Whether the given actor *would* be authorized to see ASU
  # for this repository
  def automated_security_updates_authorized_for?(actor)
    vulnerability_alerts_authorized_for?(actor)
  end

  def dependabot_installed?
    dependabot_install.present?
  end

  # Whether to show Dependabot (scheduled updates beta) to the given actor in
  # the UI
  def automated_dependency_updates_visible_to?(actor)
    return false unless GitHub.dependency_graph_enabled?
    return false unless GitHub.dependabot_enabled?
    return false unless actor

    repository.writable_by?(actor)
  end

  def dependabot_api_error_pages_enabled?
    GitHub.flipper[:dependabot_api_error_pages].enabled?(repository) ||
      GitHub.flipper[:dependabot_api_error_pages].enabled?(repository.owner)
  end

  def dependabot_install
    return @dependabot_install if defined?(@dependabot_install)

    reload_dependabot_install
  end

  def reload_dependabot_install
    return @dependabot_install = nil unless GitHub.dependabot_github_app

    @dependabot_install = IntegrationInstallation.
      with_repository(self).
      find_by(integration_id: GitHub.dependabot_github_app.id)
  end
end
