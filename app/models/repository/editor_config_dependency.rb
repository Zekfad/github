# frozen_string_literal: true

module Repository::EditorConfigDependency
  def load_editor_config(commit, paths)
    rpc.fetch_editor_config(commit.tree_oid, Array(paths), timeout: 0.1)
  rescue => e
    Failbot.report(e)
    {}
  end
end
