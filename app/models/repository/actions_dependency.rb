# frozen_string_literal: true
# actions-specific functionality for repositories
module Repository::ActionsDependency
  extend ActiveSupport::Concern

  LEGACY_ACTIONS_COLOR_LABEL="com.github.actions.color"
  LEGACY_ACTIONS_DESCRIPTION_LABEL="com.github.actions.description"
  LEGACY_ACTIONS_ICON_LABEL="com.github.actions.icon"
  LEGACY_ACTIONS_NAME_LABEL="com.github.actions.name"

  LEGACY_ACTION_LABELS=[LEGACY_ACTIONS_COLOR_LABEL, LEGACY_ACTIONS_DESCRIPTION_LABEL, LEGACY_ACTIONS_NAME_LABEL, LEGACY_ACTIONS_ICON_LABEL]

  WORKFLOW_FILE_REGEX=/\A\.github\/(?:workflows(-lab)?\/[^\/]+\.ya?ml)\z/

  included do
    has_many :actions, class_name: "RepositoryAction", dependent: :destroy
    has_one :listed_action, -> { where(state: :listed) }, class_name: "RepositoryAction"

    scope :featured_actions, -> { actions.where(featured: true) }
  end

  def action_at_root
    actions.where(path: "action.yml").or(actions.where(path: "action.yaml")).first
  end

  def listable_action?
    public? && action_at_root.present?
  end

  def actions_app_installed?
    ActiveRecord::Base.connected_to(role: :reading) do
      return false unless GitHub.launch_github_app

      GitHub.launch_github_app.installations_on(owner).with_repository(self).any?
    end
  end

  def enable_actions_app
    # REF https://github.com/github/github/pull/124773#discussion_r326824344
    installer = owner.organization? ? owner.admins.first : owner

    owner_installation = GitHub.launch_github_app.installations_on(owner).first

    result = if owner_installation.present?
      IntegrationInstallation::Editor.append(
        owner_installation,
        repositories: [self],
        editor: installer,
      )
    else
      GitHub.launch_github_app.install_on(
        owner,
        repositories: [self],
        installer: installer,
      )
    end

    if GitHub.flipper[:actions_persist_existing_workflows].enabled?(owner)
      self.persist_existing_workflows
    end

    result
  end

  # dispatch_enabled? returns true if this repository can be used with the
  # repository_dispatch event type.
  def dispatch_enabled?
    GitHub.actions_enabled?
  end

  def async_actions_plan_owner
    self.async_owner.then do |owner|
      if owner.organization?
        owner.async_business.then do |business|
          ActionsPlanOwner.new(business || owner)
        end
      else
        ActionsPlanOwner.new(owner)
      end
    end
  end

  def action(base_path)
    # We assume the base_path is a folder path.
    path = base_path + "/Dockerfile"

    # "" as a base_path maps to the Dockerfile at the root of the repo.
    path = "Dockerfile" if base_path.to_s.empty?

    self.actions.find_by(path: path)
  end

  def dispatch_event(actor_id, event_type, client_payload = nil)
    input = {
      actor_id: actor_id,
      action: event_type,
      repository_id: id,
      branch: self.default_branch,
      client_payload: client_payload,
    }

    GitHub.instrument("repository_dispatch.create", input) if dispatch_enabled?
  end

  def dispatch_workflow_event(actor_id, workflow_path, ref, inputs = nil)
    input = {
      actor_id: actor_id,
      repository_id: id,
      ref: ref,
      workflow: workflow_path,
      inputs: inputs
    }
    GitHub.instrument("workflow_dispatch", input) if GitHub.actions_enabled?
  end

  def create_or_update_action(action_config)
    action = self.actions.where(path: action_config[:path]).first

    action_params = action_config.slice(:name, :path, :description, :icon_name, :color)

    # Create a new action if one doesn't exist.
    unless action
      return self.actions.create(action_params)
    end

    if action.unlisted?
      # Auto update an existing action if it's not Listed in Marketplace.
      action.assign_attributes(action_params)
      action.save if action.changed?
    end
  end

  def get_action_config_from_metadata_file(metadata_file)
    get_action_config_from_yml_file(metadata_file)
  end

  def runner_registration_token_scope
    "RegisterActionsRunner:#{id}"
  end

  # We want to split this token into create and delete in the future
  def runner_creation_token_scope
    runner_registration_token_scope
  end

  def runner_deletion_token_scope
    runner_registration_token_scope
  end

  def update_repository_actions
    return unless GitHub.actions_enabled?
    files_changed_on_head_matching_names(["action.yml", "action.yaml"]).each do |yml_file|
      action_config = get_action_config_from_yml_file(yml_file)
      # Move to the next entry if this isn't an Action.
      next unless action_config[:is_action]
      create_or_update_action(action_config)
    end
  end

  def update_repository_workflows(ref, before, after)
    return unless GitHub.actions_enabled?

    before = nil if before == "0" * 40
    changed_files = rpc.native_read_diff_toc(before, after, nil)

    if ref == "refs/heads/#{default_branch}"
      # For now: we want to create, update, and remove workflow entities only when they
      # are pushed to the default branch
      changed_files.each do |changed_file|
        path = changed_file.path
        next unless path.match?(WORKFLOW_FILE_REGEX)

        renamed = changed_file.status == Push::ChangedFile::RENAMING
        deleted = changed_file.status == Push::ChangedFile::DELETION
        if renamed || deleted
          # Treat rename as deletion of the old workflow
          workflow = Actions::Workflow.find_by(repository: self, path: changed_file.old_file.path)
          workflow.update(state: "deleted") if workflow
        end

        added_or_modified = changed_file.status == Push::ChangedFile::ADDITION || changed_file.status == Push::ChangedFile::MODIFYING
        if added_or_modified || renamed
          parsed_workflow = Actions::ParsedWorkflow.parse_from_yaml(self, path)
          next unless parsed_workflow

          workflow = Actions::Workflow.find_by(repository: self, path: path)
          if workflow
            # Update an existing workflow
            workflow.update(name: parsed_workflow.name)
          else
            # Create a new workflow
            attrs = { path: path, repository: self, state: :active, name: parsed_workflow.name }
            workflow = Actions::Workflow.new(attrs)
            workflow.save
          end
        end
      end
    end
  end

  private

  # Parse the Action metadata file and return a hash with config
  def get_action_config_from_yml_file(yml_file)
    config = {
      is_action: false,
    }

    begin
      file_data = YAML.safe_load(yml_file.data)
    rescue Psych::Exception
      return config
    end

    return config unless file_data.is_a?(Hash)
    file_data = file_data.with_indifferent_access

    config.update(
      path: yml_file.path,
      is_action: file_data[:name].present?,
      name: file_data[:name],
      description: file_data[:description],
      icon_name: file_data.dig(:branding, :icon),
      color: file_data.dig(:branding, :color),
    )
  end

  def files_changed_on_head_matching_names(filenames)
    head = ref_to_sha(default_branch)
    return [] if head.nil?

    entries = tree_entries(head, nil, recursive: true)[1]
    entries.select do |ent|
      ent.blob? && filenames.include?(ent.name)
    end
  end

  def delist_actions
    actions.each { |action| action.delisted! if action.listed? }
  end
end
