# frozen_string_literal: true

module Repository::DependenciesDependency
  class ManifestsNotDetectedError < StandardError; end

  # Upper bound on manifest file paths that will be persisted.
  MAX_MANIFEST_FILES_DEFAULT = 20
  # There are some repositories that we need to parse more than 20
  # manifests for, e.g. public repos that contain the code/manifests
  # to open-source libraries we want to map in the dependency graph
  # The upper bound we choose is controlled by the
  # `:dependency_graph_max_manifests_high` feature flag.
  MAX_MANIFEST_FILES_HIGH = 600
  # Maximum number of tree entry paths to scan for manifest file paths.
  MAX_FILES_FOR_DETECTION = 20_000

  # Public: Have the dependency_manifest_paths been detected and
  #         persisted?
  #
  # Returns a Boolean.
  def detect_dependency_manifests?
    return false if dependency_manifests_detected?
    true
  end

  def dependency_manifests_detected?
    return true if GitHub.kv.exists(dependency_manifests_key).value!

  end

  # Public: Is this repository opted in to the Dependnecy Graph preview? The
  #         Dependency Graph keeps new language support under a preview flag,
  #         and this method tells us whether the current repository is opted in.
  #
  # Returns a Boolean.
  def dependency_graph_preview?
    GitHub.flipper[:dependency_graph_preview].enabled?(self) || GitHub.flipper[:dependency_graph_preview].enabled?(self.owner)
  end

  # Public: Does this repository support detection of vendored dependencies
  # (i.e., Vintage)?
  #
  # Returns a Boolean.
  def vintage_enabled?
    GitHub.flipper[:vintage].enabled?(self) || GitHub.flipper[:vintage].enabled?(self.owner)
  end

  # Public: Should we scan all *.js files in this repository?
  #
  # Returns a Boolean
  def vintage_all_js?
    GitHub.flipper[:vintage_all_js].enabled?(self) || GitHub.flipper[:vintage_all_js].enabled?(self.owner)
  end

  # Internal: Maximum number of manfifests we'll parse for this repo.
  #           There's a default value, but we make exceptions via feature flag
  # Returns an Integer
  def max_manifest_files
    if GitHub.flipper[:dependency_graph_max_manifests_high].enabled?(self) || GitHub.flipper[:dependency_graph_max_manifests_high].enabled?(self.owner)
      MAX_MANIFEST_FILES_HIGH
    else
      MAX_MANIFEST_FILES_DEFAULT
    end
  end

  # Public: Get a manifest detector for this repository
  #
  # Returns a ManifestDetector
  def manifest_detector
    DependencyManifestFile::ManifestDetector.new(vintage_enabled: vintage_enabled?, vintage_all_js: vintage_all_js?)
  end

  # Public: Get the manifest descriptor for a given file, if that file
  # represents a manifest supported by the Dependency Graph.
  #
  # path - A String representing the path to the file in the repository.
  # oid  - A String representing the blob OID of the file's content.
  #
  # Returns a ManifestDescriptor if a manifest is detected, otherwise nil.
  def dependency_manifest_descriptor_for(path, oid)
    content = rpc.read_full_blob(oid)["data"]
    manifest_detector.descriptor_for(path, oid, content)
  end

  # Public: Scan the current Repository for paths recognized as
  #         dependency manifest files paths and cache them.
  #
  # Uses Repository#default_oid for path scanning.
  #
  # Finds up to 20 manifest files paths and persists them into
  # GitHub::kv.
  #
  # Returns an Array of String paths.
  def detect_dependency_manifests
    return unless default_oid
    return unless default_oid != GitHub::NULL_OID
    return unless content_analysis_enabled?

    total_number_of_manifests = 0
    manifest_descriptors = []

    manifests = tree_files_sorted.select { |path| manifest_detector.recognized_path?(path: path) }

    manifests[0...max_manifest_files].each do |path|
      blob_oid = rpc.read_blob_oids([[default_oid, path]]).first
      if descriptor = dependency_manifest_descriptor_for(path, blob_oid)
        manifest_descriptors << descriptor
      end
    end

    if manifests.count > max_manifest_files
      GitHub::Logger.log(where: "dependencies_dependency",
                         const: "max_manifest_files",
                         const_value: max_manifest_files,
                         repo_count: manifests.count,
                         repo_id: id)

      GitHub.instrument "repo.max_manifests_hit_on_detect", amount: manifests.count
    end

    if manifest_descriptors.empty?
      update_dependency_manifests
      return
    end

    serialized_descriptors_by_path = Hash[
      manifest_descriptors.map { |descriptor| [descriptor.path, descriptor.serialize] }
    ]

    update_dependency_manifests(modified: serialized_descriptors_by_path)
  end

  # Public: Update the repository's currently persisted manifest files paths.
  #
  # modified - A Hash of path String => descriptor Hash tuples that will be
  #            added.
  # removed  - An Array of String paths that will be cleared.
  # clear    - A Boolean, if true will cause the stored paths to be cleared.
  #
  # Persists manifest files paths for the Repository by serializing them to GitHub::kv.
  #
  # Returns nothing.
  def update_dependency_manifests(modified: {}, removed: [], clear: false)
    if clear
      updated_paths_with_descriptors = {}
    else
      updated_paths_with_descriptors = dependency_manifest_paths_with_descriptors.merge(modified)
      updated_paths_with_descriptors = updated_paths_with_descriptors.except(*removed)
      GitHub.instrument "repo.max_manifests_hit_on_update", amount: updated_paths_with_descriptors.count if updated_paths_with_descriptors.count >= max_manifest_files
      updated_paths_with_descriptors = Hash[updated_paths_with_descriptors.first(max_manifest_files)]
    end

    json = GitHub::JSON.dump(updated_paths_with_descriptors)
    GitHub.kv.set(dependency_manifests_key, json, expires: 90.days.from_now)

    self.dependency_manifest_paths_with_descriptors = updated_paths_with_descriptors
  end

  # Private: Internal Hash of paths to descriptors for dependency manifest
  # files.
  #
  # Last-persisted Hash from update_dependency_manifests.
  #
  # Returns a Hash of path Strings to descriptor Hashes.
  private def dependency_manifest_paths_with_descriptors
    if defined?(@dependency_manifest_paths_with_descriptors)
      return @dependency_manifest_paths_with_descriptors
    end

    raw_value = GitHub.kv.get(dependency_manifests_key).value!
    hash = GitHub::JSON.parse(raw_value || "{}")
    hash = hash.transform_values do |v|
      v["category"] = v["category"].to_sym if v["category"]
      v.deep_symbolize_keys
    end

    self.dependency_manifest_paths_with_descriptors = hash
    @dependency_manifest_paths_with_descriptors
  rescue GitHub::JSON::ParseError => error
    Failbot.report(error, app: "github-dependency-graph", repo_id: id)
    self.dependency_manifest_paths_with_descriptors = {}
  end

  def dependency_manifest_paths_with_descriptors=(paths_with_descriptors)
    @dependency_manifest_paths_with_descriptors = paths_with_descriptors.uniq { |path, descriptor| descriptor }.to_h
  end

  # Public: Return descriptor Hashes for each manifest path in the repository.
  #
  # Will return an empty Array if the paths haven't explicitly been set.
  #
  # Returns an Array of descriptor Hashes.
  def dependency_manifest_descriptors
    dependency_manifest_paths_with_descriptors.values
  end

  # Public: Return the repository's currently persisted manifest files paths.
  #
  # Will return an empty Array if the paths haven't explicitly been set.
  #
  # Paths are serialized to GitHub::kv.
  #
  # Returns an Array of String paths.
  def dependency_manifest_paths
    dependency_manifest_paths_with_descriptors.keys
  end

  # Public: Return ManifestBlob objects for each manifest path in the
  #         repository.
  #
  # at_paths - An Array of paths to find blobs for.
  #            Default: Repository#dependency_manifest_paths.
  #
  # Returns an Array of DependencyManifestFile::ManifestBlobs.
  def dependency_manifest_blobs(at_paths: dependency_manifest_paths)
    return @dependency_manifest_blobs if defined?(@dependency_manifest_blobs)

    @dependency_manifest_blobs = dependency_manifest_paths_with_descriptors.slice(*at_paths).map do |path, serialized_descriptor|
      DependencyManifestFile::ManifestBlob.from_serialized_descriptor(self, path, serialized_descriptor)
    end
  end

  private

  def dependency_manifests_key
    branch_name = default_branch
    "repository:#{id}:#{branch_name}:dependency_manifest_paths:v2"
  end

  def tree_files_sorted
    files = rpc.tree_file_list(default_oid).select(&:valid_encoding?)

    if files.count >= MAX_FILES_FOR_DETECTION
      GitHub.instrument "repo.max_files_for_detection_hit_on_tree_files"
    end

    files.sort_by { |path| [path.count("/"), path] }
  end
end
