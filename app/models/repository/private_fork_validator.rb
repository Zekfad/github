# frozen_string_literal: true

class Repository::PrivateForkValidator < ActiveModel::Validator

  def validate(repo)
    if GitHub.flipper[:disallow_nested_private_forks].enabled?(repo.owner)
      if repo.parent&.private_fork?
        repo.errors.add(:base, "parent cannot be a private fork")
      end
    end

    if GitHub.flipper[:disallow_oopfs].enabled?(repo.owner)
      if repo.invalid_org_owned_private_fork?
        if repo.owner.business
          repo.errors.add(:base, "private forks owned by organizations must belong to the same business")
        else
          repo.errors.add(:base, "private fork cannot be owned by an organization")
        end
      end
    end
  end
end
