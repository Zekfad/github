# frozen_string_literal: true

# Public: Repository functionality to support the Discussions feature
# (note, this is not Team Discussions).
#
# Slack: #discussions
# Repo: github/discussions
# Team: @github/discussions-reviewers
module Repository::DiscussionsDependency
  extend ActiveSupport::Concern

  included do
    scope :with_discussions_enabled, -> { where(has_discussions: true) }
  end

  # Public: Get a key used to store the time at which the specified user last viewed the
  # Discussions tab of this repository.
  #
  # user - a User
  #
  # Returns a String.
  def discussions_last_viewed_for_key(user)
    "discussions-view-#{id}-#{user.id}"
  end

  # Public: Get a timestamp indicating when the specified user last viewed the Discussions
  # tab for this repository.
  #
  # user - a User
  #
  # Returns a DateTime or nil.
  def discussions_last_viewed_for(user)
    key = discussions_last_viewed_for_key(user)
    timestamp = GitHub.kv.get(key).value { nil }
    return unless timestamp

    begin
      DateTime.iso8601(timestamp)
    rescue ArgumentError, TypeError
      GitHub.kv.del(key)
      nil
    end
  end

  # Public: Record the current timestamp for the given user as the time at which they
  # last viewed the Discussions tab for this repository.
  #
  # user - a User
  #
  # Returns nothing.
  def update_discussions_last_viewed_for(user)
    key = discussions_last_viewed_for_key(user)
    timestamp = Time.zone.now.utc.iso8601
    GitHub.kv.set(key, timestamp)
  end

  # Public: Does this repository have discussions that the given user hasn't seen?
  # Either new and completely unread, or that have been updated since the user
  # last viewed them.
  #
  # viewer - a User or nil
  #
  # Returns a Boolean.
  def has_unread_discussions?(viewer)
    return false unless has_discussions?
    return nonspammy_discussions? unless viewer
    return true if unread_discussion_notifications?(viewer)

    timestamp = discussions_last_viewed_for(viewer)

    if timestamp
      updated_discussions = discussions.filter_spam_for(viewer).updated_since(timestamp)
      updated_discussions.exists?
    else
      nonspammy_discussions?(viewer)
    end
  end

  def enable_discussions(actor)
    return true if has_discussions?

    if update(has_discussions: true)
      GlobalInstrumenter.instrument "repository.enable_discussions", repository: self,
        actor: actor
      true
    else
      false
    end
  end

  def disable_discussions(actor)
    return true unless has_discussions?

    if update(has_discussions: false)
      GlobalInstrumenter.instrument "repository.disable_discussions", repository: self,
        actor: actor
      true
    else
      false
    end
  end

  # Public: Determine if more than read access is required to create
  # discussions in this repository.
  #
  # Note this is for the Discussion model, not team discussions.
  #
  # Returns a Boolean.
  def discussion_creation_requires_explicit_permission?
    # User-owned repos always support creating discussions if you only have
    # read access to the repository
    return false unless in_organization?

    # Check if the organization that owns this repository requires more
    # than read access to create discussions
    organization && !organization.readers_can_create_discussions?
  end

  # Private: Populate the initial value of :has_discussions?. Discussions are
  # initially enabled for organization-owned Repositories, but disabled for
  # user-owned Repositories.
  #
  # Must be triggered after the before_validate callback so that in_organization?
  # reports correctly.
  def initialize_has_discussions
    return if has_discussions_came_from_user?

    self.has_discussions = in_organization?
  end

  # Public: Can discussions be showcased on this repository?
  def discussion_spotlights_enabled?
    GitHub.flipper[:discussion_spotlights].enabled?(self)
  end

  def can_convert_issues_to_discussions?(actor)
    # Only authenticated users can convert issues to discussions
    return false unless actor

    # Only users with a verified email address can convert issues to discussions
    return false if actor.should_verify_email?

    # Spammy and suspended users can't convert issues to discussions
    return false if actor.spammy? || actor.suspended?

    # Repository that's writable, not locked for migration, not archived
    return false unless active? && writable?

    # Ensure the feature flag is turned on and the repo has the setting
    return false unless show_discussions?

    # The user needs write access to the repository
    return false unless writable_by?(actor) || has_triage_role?(actor)

    true
  end

  # Public: Populate this repo with an initial list of DiscussionCategories.
  def populate_initial_discussion_categories
    return unless has_discussions?
    return if discussion_categories.any?

    discussion_categories.create!(DiscussionCategory.initial_categories)
  end

  # Public: Determine whether or not Discussions-related features should be
  # available to a given user on this repository. Accounts for both feature flag
  # enablement and per-repository configuration.
  #
  # user - The currently viewing user. Nil for anonymous access.
  #
  # Returns a boolean.
  def show_discussions?
    has_discussions? && discussions_enabled?
  end

  private

  def nonspammy_discussions?(viewer = nil)
    discussions.filter_spam_for(viewer).exists?
  end

  # Private: Does the given user have any unread notifications for discussions in
  # this repository?
  #
  # viewer - a User
  #
  # Returns a Boolean.
  def unread_discussion_notifications?(viewer)
    query_options = {
      statuses: ["inbox_unread"],
      list: self,
      thread_type: Newsies::Thread.type_from_class(Discussion),
    }
    response = GitHub.newsies.web.exist(viewer, query_options)
    return false if response.failed?

    response.value
  end
end
