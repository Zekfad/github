# rubocop:disable Style/FrozenStringLiteralComment

# Language-analysis related methods
module Repository::LanguageAnalysisDependency
  # Public: the language analysis for this repository
  def language_analysis
    @language_analysis ||= LanguageAnalysis.new(self)
  end

  # Public: re-analyze this repository's language information
  def analyze_languages
    LanguageAnalyzer.new(repository).analyze
  end

  def copy_language_stats_from_parent
    attributes_for_clone = %w(language_name_id size total_size public)
    cloned_languages = parent.languages.map do |language|
      cloned_attributes = language.attributes.slice(*attributes_for_clone)
      self.languages.build(cloned_attributes)
    end

    GitHub::SchemaDomain.allowing_cross_domain_transactions { update(languages: cloned_languages) }
  end

  def enqueue_analyze_language_breakdown
    RepositoryUpdateLanguageStatsJob.perform_later(id)
  end

  # Retrieve a list of languages and percentages for this repository.
  #
  # Uses a read-through cache.
  #
  # Returns an Array of [ ["language_name", percent ], ... ]
  # in descending order by percentage.
  def language_percentages
    GitHub.cache.fetch(languages_summary_cache_key, ttl: 1.day, stats_key: "repo_top_languages.cache") do
      language_analysis.language_percentages
    end
  end

  # Does the repository include content written in a given language?
  #
  # language_name - the String name of the language
  #
  # Returns a Boolean.
  def includes_files_in_language?(language_name)
    language_percentages.map { |name, _| name.downcase }.include?(language_name.downcase)
  end

  # Calculate the per-file language breakdown for this repository
  #
  # oid - the object id (sha-ish) to analyze, defaults to the default oid.
  #
  # Returns a Hash of { "language_name" => [ "filename", ... ], ... }
  def files_by_language(oid = nil)
    commit_oid = oid || default_oid
    return Hash.new unless commit_oid

    GitHub.dogstats.time "repository", tags: ["action:files_by_language"] do
      incremental = true # always build on previous results when possible
      repository.rpc.language_breakdown_by_file(commit_oid, incremental)
    end
  end

  # Public: the language sizes for this repository as a Hash.
  #
  # Returns a Hash of { "language_name" => size, ... }
  def language_breakdown
    sizes = language_analysis.language_sizes
    Hash[*sizes.flatten]
  end

  # Public: calculate a language size analysis using linguist and GitRPC.
  #
  # commit_oid - The commit oid to analyze. Required.
  #
  # Returns a Hash of { "LanguageName" => size, ... }
  def language_size_analysis(commit_oid)
    GitHub.dogstats.time "repository", tags: ["action:language_size_analysis"] do
      # Use the previous scan as a starting point for the next one, analyzing
      # only the files that have changed since then:
      incremental = true
      repository.rpc.language_stats(commit_oid, incremental)
    end
  end

  # Internal: cache key for languages summary as shown on repository pages.
  def languages_summary_cache_key
    fail if new_record?
    ["repository", id, pushed_at.to_i, "languages-summary", "v9"].compact.join(":")
  end

  # Public: invalidate the rpc language cache.
  def invalidate_languages_rpc_cache
    repository.rpc.clear_language_cache
  end

  # Condense languages <= 1% into Other if there is more than one language that meets this condition.
  # If there is only 1 language that meets this condition, just list it as is.
  def top_languages_summarized
    percents = language_percentages

    if percents.length > 6
      percents, other = percents.take(6), percents.drop(6)
    else
      percents, other = percents.partition { |lang, percent| percent > 1 }
    end

    if other.length == 1
      if other.first[1] > 0.0
        percents += other
      end
    else
      total = other.sum { |k, v| v }
      if total > 0
        percents << ["Other", total.round(1)]
      end
    end
    percents
  end
end
