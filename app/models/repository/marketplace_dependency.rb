# frozen_string_literal: true

# Marketplace integration related methods
# - Detects type of repository for suggesting relevant Marketplace apps
module Repository::MarketplaceDependency
  # File names and extensions that indicate the presence of a mobile app.
  MOBILE_NAMES = %w(AndroidManifest.xml android ios).freeze
  MOBILE_EXTENSIONS = %w(.csproj .fsproj .sln .xcodeproj .xcworkspace).freeze

  def mobile_key
    "mobile-repo:#{id}"
  end

  def docker_file_key
    "docker-file-repo:#{id}"
  end

  def is_mobile?
    value = GitHub.kv.get(mobile_key).value { nil }
    value == "1"
  end

  def has_docker_file?
    value = GitHub.kv.get(docker_file_key).value { nil }
    value == "1"
  end

  def clear_mobile_status
    if GitHub.kv.exists(mobile_key).value { false }
      GitHub.kv.del(mobile_key)
      GitHub.dogstats.increment("repository.mobile.clear")
    else
      GitHub.dogstats.increment("repository.mobile.untracked")
    end
  end

  def clear_docker_file_status
    if GitHub.kv.exists(docker_file_key).value { false }
      GitHub.kv.del(docker_file_key)
      GitHub.dogstats.increment("repository.docker_file.clear")
    else
      GitHub.dogstats.increment("repository.docker_file.untracked")
    end
  end

  # This mobile scan is meant to be lightweight, and will likely generate
  # false positives. In the context of showing mobile vs non-mobile CTA
  # links that's OK, but long term we should investigate more comprehensive
  # options.
  def set_mobile_status
    head = ref_to_sha(default_branch)
    return "0" if head.nil?

    entries = tree_entries(head, nil, recursive: true)[1]
    mobile_token = entries.detect do |ent|
      ent.name.in?(MOBILE_NAMES) ||
      File.extname(ent.name).in?(MOBILE_EXTENSIONS)
    end

    cached_status = GitHub.kv.get(mobile_key).value { nil }
    current_status = mobile_token.nil? ? "0" : "1"

    if current_status != cached_status
      GitHub.kv.set(mobile_key, current_status)
      event = cached_status.nil? ? "set" : "change"
      GitHub.dogstats.increment("repository.mobile.#{event}", tags: ["status:#{current_status}"])
    else
      GitHub.dogstats.increment("repository.mobile.unchanged")
    end

    current_status
  end

  def set_docker_file_status
    head = ref_to_sha(default_branch)
    return "0" if head.nil?

    entries = tree_entries(head, nil, recursive: false)[1]
    docker_files = entries.detect do |ent|
      ent.name == "Dockerfile"
    end

    cached_status = GitHub.kv.get(docker_file_key).value { nil }
    current_status = docker_files.nil? ? "0" : "1"

    if current_status != cached_status
      GitHub.kv.set(docker_file_key, current_status)
      event = cached_status.nil? ? "set" : "change"
      GitHub.dogstats.increment("repository.dockerfile.#{event}", tags: ["status:#{current_status}"])
    else
      GitHub.dogstats.increment("repository.dockerfile.unchanged")
    end

    current_status
  end
end
