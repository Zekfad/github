# frozen_string_literal: true

module Repository::RestoreDependency
  extend ActiveSupport::Concern

  class_methods do
    # Public: Restores the archived record.
    #
    # id    - Integer ID of the Repository.
    # actor - optional User who restored the repo
    #
    # Returns the restored Repository.
    def restore(id, actor = nil)
      # Mark restoration as started
      record_restore_state("started")

      # Return if repository already exists
      if repo = find_by(id: id, deleted: false)
        record_restore_state("failed", "repo_exists")
        return repo
      end

      # find the archived record
      archived = Archived::Repository.find(id)

      # Validate that archived record is ok to restore
      unless archived.valid?(:restore)
        if GitHub.rails_6_0?
          record_restore_state("failed", archived.errors.keys.first)
        else
          record_restore_state("failed", archived.errors.attribute_names.first)
        end
        return
      end

      # restore the records and massage them a bit. the block runs within the same
      # transaction as the entire restore operation, and receives the newly
      # restored record.
      repository =
        archived.restore do |repo|
          # some records in the archived_repositories table have messed up deleted
          # values. we need to make sure the record is marked deleted = 1
          # immediately after restoring or various subsequent logic will fail.
          # See #unhide for example.
          repo.update(deleted: true, active: nil, owner_login: repo.owner.login)

          # restore admin-on-owner grants:
          if repo.in_organization?
            repo.organization.dependent_added repo
          end

          repo.unhide
        end

      # This expects the repo to be visible so it's deliberately done after Repository#unhide.
      repository.restore_git_repository

      # The restored repository may not conform to the new rules around private forks. This
      # method will extract or reparent as needed. Extract requires git data to be in place
      # so this must happen after Repository#restore_git_repository.
      repository.sanitize_network_structure!(actor: actor)

      # restore any archived media objects
      repository.network&.restore

      # reset the git refs cache and whatnot now that the repository is back in
      # business
      repository.reset_git_cache

      # re-index the repository and its resources for search
      repository.instrument_search_restore
      repository.reindex_all

      # instrument, publish to hydro and statsd
      repository.instrument_restore(initiated_by, actor)
      record_restore_state("success")

      repository

      # Rescue the following specific exceptions so we can record the failure reason
      # and increment the count
    rescue Repository::StorageAdapter::RemoteShardedStorageAdapter::RestoreError => exception
      record_restore_state("failed", exception.class.name)
      Failbot.push repo_id: id
      GitHub::Logger.log_exception({repo_id: id, exception_data: exception.data}, exception)
      raise
    rescue => exception
      record_restore_state("failed", exception.class.name)
      raise
    end

    def initiated_by
      GitHub.context[:from].presence || GitHub.context[:job].presence || "unknown"
    end

    def record_restore_state(state, error = "")
      tags = ["state:#{state}", "initiated_by:#{initiated_by}"]
      tags.push("error:#{error}") if error.present?
      GitHub.dogstats.increment("repo.restore", tags: tags)
    end
  end

  def restore_git_repository
    repository.enterprise_restore_to_dgit_from_archived_record if GitHub.enterprise?

    # In enterprise mode, we store the replica records for an archived repository
    # in a table called `archived_repository_replicas`. This allows us, on restore
    # to restore these associations as the git repository is not deleted until a
    # purge occurs.
    #
    # In dotcom development we of course don't have archived_repository_replicas
    # despite using the same storage adapter and just leaving the repo contents on
    # disk when we archive the repo.
    #
    # Instead, we call `initialize_replicas_from_network` which randomly assigns three
    # of the four dgit "fileservers" used in local development. Since there are four
    # hosts and we create three replicas, there is a chance that there will be a mismatch
    # and we'll only have two of three spokes replicas correct. Therefore we follow
    # up the initialization with a maintenance job which synchronizes the three
    # replicas once again.
    #
    # Ideally we might want to someday unify the interface between enterprise, dotcom and dotcom
    # development here.
    if !GitHub.enterprise? && Rails.development?
      GitHub::DGit::Maintenance.insert_placeholder_replicas_and_checksums(
        GitHub::DGit::RepoType::REPO, id, network_id)
      GitHub::DGit::Maintenance.check_one_network(network_id)
    end

    # restore the on disk repository and wiki first
    storage_adapter.restore

    # if the storage adapter didn't create the repository, initialize it with a
    # default empty configuration
    setup_missing_git_repository
  end


  def enterprise_restore_to_dgit_from_archived_record
    raise "enterprise-only method unexpectedly invoked (#{__method__})" unless GitHub.enterprise?

    # Also restore Wiki replicas if the restored repository has a Wiki
    repo_types = [dgit_repo_type]
    repo_types << GitHub::DGit::RepoType::WIKI if repository.has_wiki? && repository.repository_wiki
    has_wiki = (repo_types.include? GitHub::DGit::RepoType::WIKI)

    repo_types.each do |repo_type|
      is_wiki = (repo_type == GitHub::DGit::RepoType::WIKI)
      arrs = GitHub::DGit::Enterprise.archived_replicas(network.id, id, [repo_type])
      online_replica = arrs.find(&:online?)
      raise GitHub::Jobs::RepositoryRestore::Error, "No available file server found" if online_replica.nil?
      dest_fs = online_replica.fileserver

      GitHub::DGit::Maintenance.insert_restore_placeholder_replica_and_checksum(
        id, network.id, is_wiki, dest_fs)

      GitHub::Logger.log(at: "EnterpriseRestoreToDGitFromArchivedRecord",
                         dest_fs: dest_fs.name,
                         repo_id: id,
                         dgit_repo_type: repo_type,
                         nwo: nwo,
                         now: Time.now.utc.iso8601)

      GitHub::DGit::Maintenance.recompute_checksums(
        self, dest_fs.name, is_wiki: is_wiki, force_dgit_init: true)
    end

    # A mismatch between network replica & repository replica records can happen if the network already existed and we
    # only restore the repository.
    nr_hostnames = GitHub::DGit::Routing.hosts_for_network(network.id)
    rr_hostnames = GitHub::DGit::Routing.hosts_for_repo(id)
    if nr_hostnames.to_set != rr_hostnames.to_set
      # Ensure that for each repository in the network we have a `repository_replica` record for each `network_replica`.
      GitHub::DGit::Maintenance.sync_network_replicas(network.id)
    end

    # Schedule jobs to fix up missing network replicas and bad repo checksums
    GitHub::DGit::Maintenance.check_one_network(network.id)

    GitHub::DGit::Enterprise.delete_archived_replicas_and_checksums(network.id, self.id, false)
    GitHub::DGit::Enterprise.delete_archived_replicas_and_checksums(network.id, self.id, true) if has_wiki
  end

  # Restore a repository currently marked as deleted to an active state. Ensures
  # that all related objects are present and valid and that parent relationships
  # are valid.
  #
  # The unhide operation performs all database operations within a transaction.
  # If the record cannot be returned to a good state, all changes are rolled
  # back and the record will still be marked as deleted.
  #
  # Returns nothing.
  # Raises RuntimeError if the record is not marked as deleted or if the owner
  # no longer exists.
  def unhide
    fail "Refusing to unhide repository not marked as deleted" if active?
    fail "Refusing to unhide repository with no owner" if owner.nil?

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      transaction do
        # fix up attributes that may be stale since the repository was hidden
        unhide_parent
        unhide_network
        update_organization
        unhide_repository_advisories_on_restore

        # mark record as not deleted only after the record is in a good state
        update(deleted: false, active: true)

        # TODO re-enable validation when https://github.com/github/github/pull/141281 ships
        save!(validate: false)
      end
    end
  end

  # Internal: ensure a valid parent attribute is set. This checks that the
  # existing parent_id points to a valid repository in the same network or
  # adjusts it to the root repo or nil if not. Called from #unhide within a
  # transaction.
  def unhide_parent
    if parent && parent.active? && parent.network_id == network_id
      # leave parent attribute alone, it looks good
    elsif network && network.root && network.root != self
      self.parent = network.root
    else
      self.parent = nil
    end
  end

  # Internal: ensure a valid repository_networks record exists for this record.
  # Called from #unhide within a transaction. The new network must be created
  # with the original network_id stored on the archived record or the repository
  # location on disk will change and mess with git restores.
  def unhide_network
    return if network
    create_network(root: self, network_id: network_id)

    # The network needs initialization in dotcom. In enterprise, the records are already
    # initialized since we just leave things on disk when we delete them.
    network.needs_dgit_initialization_after_commit = false if GitHub.enterprise?
  end

  def instrument_restore(initiated_by, actor)
    staff_action = !initiated_by.include?("repos/restore")
    payload = restore_payload(actor, staff: staff_action)
    action = staff_action ? "staff.repo_restore" : "repo.restore"

    GitHub.instrument action, payload

    # Instruments a push to kafka topic when a repository is restored
    GlobalInstrumenter.instrument("repository.restored", {
      restored_repository: self,
      actor: actor,
    })
  end

  # Instruments a restored repo as a RESTORED event for Geyser search
  def instrument_search_restore
    payload = {
      change: :RESTORED,
      repository: self,
      owner_name: self.owner.name,
      updated_at: Time.now.utc,
      ref: "refs/heads/#{self.default_branch}",
    }

    GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
    GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:restored"])
  end

  def restore_payload(actor, staff: false)
    payload = { repo: self }
    payload.merge!(restore_actor_payload(actor, staff: staff))
    payload.merge!(owner.event_context)
  end

  def restore_actor_payload(actor, staff:)
    if actor && !staff
      { actor: actor.login, actor_id: actor.id }
    else
      GitHub.guarded_audit_log_staff_actor_entry(actor)
    end
  end
end
