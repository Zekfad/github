# frozen_string_literal: true

module Repository::CheckDependency
  # CheckAnnotations for the given sha
  #
  # Returns an ActiveRecord::Relation
  def annotations_for(sha:, filenames: [], limit:)
    latest_run_ids = CheckRun.latest_ids_for_sha_and_repository_id(sha, self.id)
    annotations = CheckAnnotation.where(check_run_id: latest_run_ids)

    if limit.present?
      annotations = annotations.limit(limit)
    end

    return annotations unless filenames.any?

    annotations.where(filename: filenames)
  end

  # Does this repository have applications that will produce code checks on it?
  #
  # Returns boolean.
  def has_apps_that_write_checks?
    return @installations_with_access.any? if defined?(@installations_with_access)

    @installations_with_access ||= IntegrationInstallation.with_resources_on(
      subject: self,
      resources: "checks",
      min_action: :write,
    )

    @installations_with_access.any?
  end

  # Is this repository configured to create check suites automatically
  # for the given app? Defaults to true.
  #
  # app_id: - The id of a GitHub App.
  #
  # Returns boolean.
  def auto_trigger_checks_for?(app_id:)
    return false if [GitHub.launch_github_app&.id, GitHub.launch_lab_github_app&.id].include?(app_id)
    value = GitHub.kv.get(auto_trigger_key(app_id)).value { nil }
    value != "false"
  end

  # Set the preference on whether to automatically create check suites
  # for the given app.
  #
  # app:    - The GitHub App.
  # value:  - The string to represent the app's preference: 'true' or 'false'.
  #
  # Returns nothing.
  def set_auto_trigger_checks(actor:, app:, value:)
    GitHub.kv.set(auto_trigger_key(app.id), value.to_s)

    payload = {
      prefix: :checks,
      actor: actor,
      app: app,
    }

    if value == "true"
      instrument :auto_trigger_enabled, payload
    else
      instrument :auto_trigger_disabled, payload
    end
  end

  # What are application's preferences for check suites on this repo?
  #
  # Examples
  #
  #   check_suite_preferences
  #   # => { :auto_trigger_checks =>
  #            [{ :app => #<Integration id: 2, owner_id: 340, bot_id: 5346, name: "Super-CI"...>,
  #               :setting => true}]}
  #
  # Returns a Hash.
  def check_suite_preferences
    preferences = { auto_trigger_checks: [] }

    installations_with_access = IntegrationInstallation.with_resources_on(subject: self, resources: "checks", min_action: :write)
    installations_with_access.each_with_object(preferences[:auto_trigger_checks]) do |installation, memo|
      app = Integration.find(installation.integration_id)

      memo << {
        app: app,
        setting: auto_trigger_checks_for?(app_id: app.id),
      }
    end

    preferences
  end

  private
  def auto_trigger_key(app_id)
    "checks.auto_trigger_checks.#{id}.#{app_id}"
  end
end
