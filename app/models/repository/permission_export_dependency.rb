# frozen_string_literal: true

module Repository::PermissionExportDependency
  # Returns a list of grants that the user has received from a team membership
  def team_grants(user)
    access = Organization::RepositoryPermissions.new(self, user)
    access.all_team_abilities_for_repository.map do |ability|
      {
        "source" => Platform::Loaders::ActiveRecord.load(Team, ability.actor_id).sync,
        "permission" => ability.action,
      }
    end
  end

  # Returns a list of grants that the user has received through their org membership
  # Users who have access to a repository through org grants are org members or admins
  def org_grants(user)
    org_abilities = Ability.where(
      subject_type: "Organization", subject_id: owner.id, actor_type: "User", actor_id: user.id,
    )
    org_abilities.map do |ability|
      {
        "source" => owner,
        "permission" => ability.action,
      }
    end
  end

  def repo_grants(user)
    repo_abilities = Ability.where(
      subject_type: "Repository",
      subject_id: id,
      actor_type: "User",
      actor_id: user.id,
    ).where(priority: [Ability.priorities[:direct], Ability.priorities[:indirect]])

    if owning_organization_id
      repo_org_admin_abilities = Ability.where(
        subject_type: "Organization",
        subject_id: owning_organization_id,
        actor_type: "User",
        actor_id: user.id,
        action: Ability.actions[:admin],
      )

      repo_abilities = repo_abilities + repo_org_admin_abilities
    end

    repo_abilities.map do |ability|
      {
        "source" => self,
        "permission" => ability.action,
      }
    end
  end

  # Returns a list of all sources of a user's access to this repository and the permission level granted
  # Possible permission levels are "admin", "write", and "read"
  # Possible sources are Team, Organization, and Repository objects
  def permission_sources(user, viewer)
    return [] unless owner.organization? && owner.adminable_by?(viewer)
    org_grants(user) + repo_grants(user) + team_grants(user)
  end
end
