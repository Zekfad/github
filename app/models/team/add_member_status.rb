# frozen_string_literal: true

# Public: A response is created when a user is added to a team.
class Team::AddMemberStatus
  MESSAGES = {
    blocked: "You are blocked from joining this organization. ",
    no_seat: "There are no available seats in this organization. ",
    no_2fa: "You do not satisfy the 2FA requirements of this organization. ",
    no_saml_sso: "You do not satisfy the SAML SSO requirements of this organization. ",
    dupe: "You are already a member. ",
    pending_cycle_no_seat: "There are no available seats in this organization during the pending cycle. ",
    no_permission: "Your inviter does not have permission to invite you. ",
    not_user: "You are not a valid user for this organization. ",
    trade_controls_restricted: "#{::TradeControls::Notices::Plaintext.org_invite_restricted} ",
  }

  # Internal: Successful statuses. FIX: The API needs duplicate member adds to
  # be successful, but I think we should take another look later.
  SUCCESSFUL = [:dupe, :success]

  # Public: A Symbol. :success, :blocked, :no_seat, :no_2fa, :no_saml_sso, :dupe, or :org.
  attr_reader :status

  def initialize(status)
    @status = status
    freeze
  end

  private :initialize

  # Public: Did an error occur while adding a member?
  def error?
    !success?
  end

  # Public: Was a member successfully added?
  def success?
    SUCCESSFUL.include?(status)
  end

  # Public: Describe the error if any.
  def message
    MESSAGES[status]
  end

  # Public: User isn't permitted to join a team.
  BLOCKED = new(:blocked)

  # Public: Team can't be added to due to org level trade controls restrictions
  TRADE_CONTROLS_RESTRICTED = new(:trade_controls_restricted)

  # Public: User isn't permitted to join because there isn't an available seat.
  NO_SEAT = new(:no_seat)

  # Public: User isn't permitted to join because there will not be an available seat after the orgs pending downgrade.
  PENDING_CYCLE_NO_SEAT = new(:pending_cycle_no_seat)

  # Public: User isn't permitted to join because they don't satisfy the 2FA requirement.
  NO_2FA = new(:no_2fa)

  # Public: User isn't permitted to join vecause they don't satisfy the SAML SSO requirement.
  NO_SAML_SSO = new(:no_saml_sso)

  # Public: User is already a member of a team.
  DUPE = new(:dupe)

  # Public: User is not a human user, and can't join any team.
  NOT_USER = new(:not_user)

  # Public: It's all good.
  SUCCESS = new(:success)

  # Public: Inviter doesn't have permission to invite
  NO_PERMISSION = new(:no_permission)
end
