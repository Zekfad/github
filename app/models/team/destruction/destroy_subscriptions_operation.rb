# frozen_string_literal: true
module Team::Destruction

  # Unsubscribes all given users from the given teams, if they are not
  # a (direct or indirect) member.
  #
  class DestroySubscriptionsOperation

    def initialize(member_ids, team_ids)
      @member_ids = member_ids
      @team_ids = team_ids
    end

    def execute
      GitHub.dogstats.time("team.destruction.destroy_subscriptions_operation.duration") do
        @member_ids.each do |member_id|
          promises = @team_ids.map do |team_id|
            Platform::Loaders::IsTeamMemberCheck.load(member_id, team_id).then do |is_member|
              Team.new.tap { |team| team.id = team_id } unless is_member
            end
          end

          teams_to_be_removed_from = Promise.all(promises).sync.compact
          GitHub.newsies.async_delete_all_for_user_and_lists(member_id, teams_to_be_removed_from) unless teams_to_be_removed_from.empty?
        end
      end
    end
  end
end
