# frozen_string_literal: true

class Team::Membership
  # Represents a team that no longer exists, for when we need something that
  # quacks like a team
  class PseudoTeam
    attr_reader :id, :name, :organization

    def initialize(id, name, organization, legacy_owners: false)
      @id = id.to_i
      @name = name
      @organization = organization
      @legacy_owners = legacy_owners
    end

    def legacy_owners?
      !!@legacy_owners
    end

    def ability_delegate
      self
    end

    def ability_id
      id
    end

    def ability_type
      "Team"
    end

    def subject?
      true
    end

    def participates?
      true
    end
  end

  RemoveTeamMembershipError = Class.new(StandardError)

  attr_reader :team, :repo_ids, :organization, :member_ids

  def initialize(team, member_ids = [], repo_ids = [], send_notification: false, team_destroyed: false)
    @team = team
    @organization = team.organization
    @repo_ids = Array(repo_ids)
    @member_ids = Array(member_ids)
    @team_destroyed = team_destroyed
    @send_notification = send_notification
  end

  # Public: Revoke the membership ability between a team and a user, wrapped in
  # a transaction, optionally yielding to a provided block to execute code
  # inside the transaction.
  #
  # block     - Optional: run code inside the database transaction for each
  # member
  #
  # Returns nothing.
  def remove(queue_delete_jobs: true)
    User.where(id: member_ids).find_each(batch_size: 500) do |member|
      return unless member

      Ability.throttle do
        success = false
        begin
          Ability.transaction do
            Ability.revoke(member, team)
            yield member if block_given?
            success = true
          end
        ensure
          unless success
            # there was a transaction rollback
            Failbot.report!(RemoveTeamMembershipError.new("Failed to revoke membership to team #{team.id} for member #{member}"),
              app: "github-abilities",
              org_id: team.organization.id,
              team_id: team.id,
              user: member,
            )
          end
        end
      end

      if organization.present?
        # No need to queue this job if the owning organization has been deleted
        if queue_delete_jobs
          Team.queue_clear_team_memberships(repo_ids, organization.id, { user: member.id })
        end

        # Team removal notifactions can't be sent without an organization
        send_removal_notification(member)
      end
    end

    if queue_delete_jobs
      Team.queue_fork_cleanup(repo_ids, member_ids)
    end
  end

  private

  def has_repositories?
    repo_ids.any?
  end

  def send_notification?
    !!@send_notification
  end

  def team_destroyed?
    !!@team_destroyed
  end

  # Internal: send a team removal email notification to the current member.
  #
  # Returns nothing.
  def send_removal_notification(member)
    if has_repositories? && send_notification?
      OrganizationMailer.removed_from_team(
        member,
        team.name,
        team.organization,
        legacy_owner: team.legacy_owners?,
        team_destroyed: team_destroyed?,
      ).deliver_later
    end
  end
end
