# frozen_string_literal: true
module Team::ParentChange

  Error = Class.new(StandardError)
  CannotAcquireLockError = Class.new(Error)

  class RecalculationFailedAfterSeveralRetries < Error
    attr_reader :org_id, :last_error, :retries

    def initialize(org_id, retries, last_error)
      super("Failed after retrying #{retries} times to recalculate abilities after a team's parent changed inside Organization##{org_id}. Proceeding to repair the abilities for the org.")
      @org_id = org_id
      @retries = retries
      @last_error = last_error
    end
  end

  class RecalculationOfSubscriptionsFailedAfterSeveralRetries < Error
    attr_reader :org_id, :last_error, :retries

    def initialize(org_id, retries, last_error)
      super("#{retries} attempts to recalculate subscriptions after a team's parent changed inside Organization##{org_id} failed.")
      @org_id = org_id
      @retries = retries
      @last_error = last_error
    end
  end
end
