# frozen_string_literal: true

module Team::RoleBasedPermissionsDependency
  # Public: Returns the most capable action that this team has on a repository.
  #
  # repository - The Repository to check access for.
  #
  # Returns a Promise<String|nil>.
  def async_most_capable_action_or_role_for(repository)
    promises = [
      Platform::Loaders::MostCapableTeamRepositoryAbilities.load(team: self, repo: repository),
      async_role_for(repository),
    ]

    Promise.all(promises).then do |ability, role|
      next unless ability.present?
      ability_action = ability.action_name
      next ability_action unless role.present?

      if role.action_rank > Ability::ACTION_RANKING[ability.action.to_sym]
        role.name
      else
        ability.action_name
      end
    end
  end

  # Public: Returns the most capable Role that this team has on a repository.
  #
  # repository - The Repository to check access for.
  #
  # Returns a Promise<Role|nil>.
  def async_role_for(repository)
    repository.async_owner.then do |owner|
      next unless owner.organization?

      Platform::Loaders::Permissions::MostCapableUserRoleOnRepositoryForActor.load(
        actor: self,
        repo: repository,
      ).then do |most_capable_role|
        next unless most_capable_role.present?
        most_capable_role.async_role.then do |role|
          role.async_base_role.then do |base_role|
            if role.custom?
              base_role
            else
              role
            end
          end
        end
      end
    end
  end
end
