# rubocop:disable Style/FrozenStringLiteralComment

class Team
  class Creator
    def self.service
      GitHub.ldap_sync_enabled? ? LdapCreator : self
    end

    def self.create_team(creator, org, attrs, maintainers:, group_mappings: [])
      service.new(creator, org).create(attrs, maintainers: maintainers, group_mappings: group_mappings)
    end

    def initialize(creator, org)
      @creator, @org = creator, org
    end

    def create(attrs, maintainers:, group_mappings:)
      team = @org.teams.build(Team.team_attributes_hash(attrs))
      team.parent_team_id = attrs[:parent_team_id] if attrs.key?(:parent_team_id)

      if @creator.present?
        team.creator_id = @creator.id
      end

      yield(team) if block_given?

      if @creator.present? && @creator.can_have_granular_permissions?
        unless team.organization.resources.members.writable_by?(@creator)
          team.errors.add(:base, "This GitHub App doesn't have permissions to create teams")
          return team
        end
      end

      parent_team = Team.find_by_id(team.parent_team_id)
      cannot_admin_parent_team = @creator && parent_team && !can_admin_team?(parent_team, @creator)

      if cannot_admin_parent_team
        if !parent_team.allows_change_parent_requests_from?(team)
          team.errors.add(:base, "This team cannot be requested as a parent team.")
        else
          team.parent_team_id = nil
        end
      end

      # We need to check errors specifically here because they're added
      # in the LdapCreator subclass and aren't real AR validations
      return team unless team.errors.empty? && team.valid?

      team.save!
      add_members_and_maintainers(team, maintainers)

      if @org.team_sync_enabled? && group_mappings.any?
        Team::GroupMapping.batch_update_mappings(team, group_mappings, actor: @creator)
      end

      if cannot_admin_parent_team
        ::TeamChangeParentRequest.create_initiated_by_child!(
          parent_team: parent_team,
          child_team: team,
          requester: @creator,
        )
      end

      team
    end

    def add_initial_members(team, members)
      members.each do |member|
        team.add_member member
      end
    end

    private

    def add_members_and_maintainers(team, maintainers)
      maintainers = ensure_default_maintainer(maintainers, @creator)
      add_initial_members(team, maintainers) unless maintainers.blank?

      unless team.ldap_mapped?
        maintainers.each do |member|
          team.promote_maintainer(member)
        end
      end
    end

    def ensure_default_maintainer(maintainers, creator)
      maintainers = Array.wrap(maintainers)
      maintainers.push(creator) if creator.user?
      maintainers.uniq
    end

    def can_admin_team?(team, actor)
      if actor.can_have_granular_permissions?
        team.creator == actor
      else
        team.adminable_by?(actor)
      end
    end
  end


  class LdapCreator < Creator
    def create(attrs, maintainers:, group_mappings:)
      super do |team|
        if (ldap_dn = attrs[:ldap_dn]).present?
          if @org.resources.organization_administration.writable_by?(@creator)
            if group = ldap_group(ldap_dn)
              team.build_ldap_mapping(dn: group.dn)

              team.name       = default_name(group)       unless team.name?
              team.permission = default_permission(group) unless team.permission?
            else
              team.add_invalid_group_error(ldap_dn)
            end
          else
            team.add_non_owner_group_error
          end
        end
      end
    end

    # Skip adding the creator as a member.
    # After creating the ldap mapping a callback is fired to sync the ldap members.
    def add_initial_members(team, members)
      if team.ldap_mapped?
        team.enqueue_sync_job
      else
        super
      end
    end

    # Private - Search for the ldap group associated with a new team.
    #
    # dn: is the dn for the group entry.
    #
    # Returns nil if the authentication doesn't use ldap or there is no group.
    # Returns a Net::LDAP::Entry for the group if it exists.
    def ldap_group(dn)
      if dn.present?
        GitHub::LDAP.search.find_group(dn)
      end
    end

    # Internal: Returns the default Team name String if one is not specified
    # based on the LDAP Group's name (CN), failing back to the UID.
    def default_name(group)
      GitHub.auth.profile_info(group, :name).first
    end

    # Internal: Returns the default Team permission String if one is not
    # specified. In this case, we opt for the least permissive option.
    def default_permission(group)
      "pull"
    end
  end
end
