# rubocop:disable Style/FrozenStringLiteralComment

# Writes key events to audit log and reports stats for actions on a
# PaymentMethod.
#
# Audit Log Events:
#
# payment_method.create
# payment_method.update
# payment_method.remove
#
# Payload Hash:
#   :note                          - String human friendly note about the event.
#   :actor                         - User that took the action.
#   :user                          - User that owns this payment method OR
#   :org                           - Organization that owns this payment method.
#   :payment_processor_customer_id - Customer ID in the payment processor.
#   :payment_processor_type        - String "braintree", "zuora".
#   :payment_method                - String "paypal", "card", or "none".
#
#
# Datadog Keys:
#
# "billing.create"
# "billing.update"
# "billing.remove"
module PaymentMethod::InstrumentationDependency
  extend ActiveSupport::Concern

  def event_payload
    hash = {
      payment_processor_customer_id: payment_processor_customer_id,
      payment_processor_type: payment_processor_type,
      payment_method: payment_instrument,
    }

    if customer.business
      hash[:business] = customer.business.slug
      hash[:business_id] = customer.business.id
    elsif user.organization?
      hash[:org] = user
    elsif user.billable?
      hash[:user] = user
    end

    hash
  end

  # Public: Instrument the first time creation of a payment method.
  #
  # :actor - User taking this action.
  #
  # Returns nothing.
  def instrument_create(actor)
    instrument :create, actor: actor, note: event_note("Created")

    GitHub.dogstats.increment("billing.create", tags: [
      "payment_processor:#{payment_processor_type}",
      "payment_instrument:#{payment_instrument}",
      "success:true",
    ])
  end

  # Public: Instrument the update of a payment method.
  #
  # :actor - User taking this action.
  #
  # Returns nothing.
  def instrument_update(actor)
    return unless valid_payment_token?

    instrument :update, actor: actor, note: event_note("Updated")

    GitHub.dogstats.increment("billing.update", tags: [
      "payment_processor:#{payment_processor_type}",
      "payment_instrument:#{payment_instrument}",
      "success:true",
    ])
  end

  # Public: Instrument the failure to update a payment method.
  #
  # :actor - User taking this action.
  # :error_msg - The message given to `Github::Billing::Result.failure`
  #
  # Returns nothing.
  def instrument_update_failure(actor, error_msg)
    instrument :update, actor: actor, note: "Error Updating: #{error_msg}"

    GitHub.dogstats.increment("billing.update", tags: [
      "payment_processor:#{payment_processor_type}",
      "payment_instrument:#{payment_instrument}",
      "success:false",
    ])
  end

  # Public: Instrument removal of payment details.
  #
  # :actor - User taking this action.
  #
  # Returns nothing.
  def instrument_clear(actor)
    instrument :remove, actor: actor, note: "Removed payment details"
    GitHub.dogstats.increment("billing.remove",
      tags: ["payment_processor:#{payment_processor_type}",
      "success:true",
    ])
  end

  private

  def payment_instrument
    if credit_card?
      "card"
    elsif paypal?
      "paypal"
    else
      "none"
    end
  end

  def event_note(prefix)
    if valid_payment_token? && credit_card?
      "#{prefix} credit card"
    elsif valid_payment_token? && paypal?
      "#{prefix} PayPal account"
    elsif valid_payment_token?
      "Valid payment token, unknown payment method"
    else
      "Cleared payment details"
    end
  end
end
