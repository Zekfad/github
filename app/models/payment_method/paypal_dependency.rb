# frozen_string_literal: true

module PaymentMethod::PaypalDependency
  # Public: Email address associated with the PayPal account the user is using
  # to pay with. We cannot use this email for any other purposes and it doesn't
  # need to match the email we have on file for a user.
  # column :paypal_email

  # Public: Boolean if the payment method is PayPal.
  def paypal?
    paypal_email.present? && valid_payment_token?
  end
end
