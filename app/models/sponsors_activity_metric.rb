# frozen_string_literal: true

class SponsorsActivityMetric < ApplicationRecord::Collab
  enum sponsorable_type: [User.name, Organization.name]
  enum metric: [:subscription_value]

  belongs_to :sponsorable, polymorphic: true

  scope :most_recent, -> (as_of:) { where("recorded_on <= ?", as_of.to_date).order(recorded_on: :desc).limit(1) }
  scope :on, -> (date) { where(recorded_on: date.to_date) }
end
