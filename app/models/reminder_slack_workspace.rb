# frozen_string_literal: true

class ReminderSlackWorkspace < ApplicationRecord::Collab
  extend GitHub::Encoding

  belongs_to :remindable, polymorphic: true

  has_many :reminders
  has_many :reminder_slack_workspace_memberships, autosave: true, dependent: :destroy

  validates_presence_of(:name, :slack_id, :remindable)
  validates :slack_id, uniqueness: { scope: [:remindable_type, :remindable_id], case_sensitive: false }

  scope :for_remindable, -> (remindable) { where(remindable: remindable) }

  force_utf8_encoding :name

  def member?(user)
    reminder_slack_workspace_memberships.where(user: user).exists?
  end

  def self.create_or_update_workspace(name:, remindable:, slack_id:)
    # Organizations are Users, so we need base_class to mimic Rails behavior
    result = github_sql.run(<<-SQL, name: name, remindable_type: remindable.class.base_class, remindable_id: remindable.id, slack_id: slack_id)
      INSERT INTO reminder_slack_workspaces (remindable_type, remindable_id, slack_id, name, created_at, updated_at)
      VALUES (:remindable_type, :remindable_id, :slack_id, :name, NOW(), NOW())
      ON DUPLICATE KEY UPDATE
        name = :name,
        updated_at = NOW()
    SQL
    find_by!(remindable: remindable, slack_id: slack_id)
  end
end
