# rubocop:disable Style/FrozenStringLiteralComment

# When a Git client pushes to a repository hosted on GitHub, this class
# determines which ref updates are allowed to proceed and which should be
# denied.
class RefUpdatesPolicy
  include Rails.application.routes.url_helpers

  # We do not want to allow these files to be overwritten / writable from GitHub
  # Apps that are being used in Actions.
  UNWRITABLE_PATHS_FOR_APPS = %w(.github/workflows/ .github/workflows-lab/)

  # The message when aborting an atomic update.
  ATOMIC_FAILURE_REASON = "atomic transaction failed"

  class Decision
    attr_reader :allowed, :ref_update, :short_message, :long_message

    def initialize(allowed, ref_update, short_message = nil, long_message = nil)
      @allowed = allowed
      @ref_update = ref_update
      @short_message = short_message
      @long_message = long_message
    end

    alias_method :allowed?, :allowed
  end

  def self.areas_of_responsibility
    [:actions]
  end

  # Public: Check the policy for one or more ref updates.
  #
  # repository  - The Repository, Gist, or Unsullied::Wiki whose refs are being updated
  # ref_updates - Array of Git::Ref::Updates
  # actor       - The User, PublicKey, or :slumlord who is performing the update
  #
  # Returns an Array of Decisions, one for each element in ref_updates.
  def self.check(repository, ref_updates, actor, **options)
    new(repository, ref_updates, actor, **options).check
  end

  def initialize(repository, ref_updates, actor, atomic: false)
    @repository = repository
    @actor = actor
    @ref_updates = ref_updates

    @decisions = {}
    @remaining_updates = ref_updates.index_by(&:refname)

    @default_branch = repository.default_branch
    @atomic = atomic
  end

  def check
    remaining_updates.each do |ref_update|
      case
      when top_level_ref?(ref_update)
        check_top_level_ref_policy(ref_update)
      when invalid_ref?(ref_update)
        check_top_level_ref_policy(ref_update)
      when private_email_ref?(ref_update)
        check_private_email_policy(ref_update)
      when actor == :slumlord
        check_slumlord_policy(ref_update)
      when hidden_ref?(ref_update)
        check_hidden_ref_policy(ref_update)
      when default_branch?(ref_update)
        check_default_branch_policy(ref_update)
      end

      case
      # These three cases implement a prohibition on apps pushing code to
      # workflows/a.yml using "git push."  There's a similar set of logic in
      # app/api/access_control/roles_dependency.rb's definition of
      # :repo_file_writer that governs writing workflows/a.yml using the API.
      # If you make changes here, consider updating that one, too.
      when auth_via_integration_without_workflow_permission || auth_via_oauth_app_without_workflow_write_scope
        check_bot_actions_prohibitions(ref_update)
      when public_key_from_oauth_without_workflow_write_scope || public_key_created_by_unknown
        check_bot_actions_prohibitions(ref_update)
      end
    end

    check_protected_branches_policy

    final_decisions
  end

  def self.blacklisted_path_for_apps?(path)
    return false unless path.is_a?(String)
    RefUpdatesPolicy::UNWRITABLE_PATHS_FOR_APPS.any? do |unwritable|
      path.start_with? unwritable
    end
  end

  def self.path_updateable_for_app?(repo, path, oid)
    return true unless blacklisted_path_for_apps?(path)
    return true if oid == GitHub::NULL_OID || oid.nil?
    branches = repo.refs.select(&:branch?)
    commit_oids_and_paths = branches.map { |branch| [branch.sha, path] }

    branch_oids_at_path = repo.rpc.read_blob_oids(commit_oids_and_paths, skip_bad: true)
    branch_oids_at_path.include?(oid)
  end

  private

  attr_reader :repository, :actor, :default_branch

  # Returns an Array of ref update tuples which have yet to be decided upon.
  def remaining_updates
    @remaining_updates.values
  end

  # Returns an Array of Decisions, one for each item in ref_updates. Do not
  # call this until all decisions have been made.
  def final_decisions
    unless atomic_transaction_failure?
      return @ref_updates.map { |ref_update| @decisions.fetch(ref_update.refname) }
    end

    @ref_updates.map do |ref_update|
      decision = @decisions.fetch(ref_update.refname)

      # If we're already rejecting this for some other reason, report that
      # reason to the user.
      next decision unless decision.allowed?

      # We're rejecting this ref update because it's an atomic transaction and
      # some other ref update has failed.
      Decision.new(false, ref_update, ATOMIC_FAILURE_REASON, nil)
    end
  end

  def atomic_transaction_failure?
    # It's a failure if we're atomic and any ref update will be rejected.
    @atomic && @ref_updates.any? { |ref_update| !@decisions.fetch(ref_update.refname).allowed? }
  end

  def allow(ref_update)
    record(Decision.new(true, ref_update))
  end

  def deny(ref_update, short_message, long_message = nil)
    record(Decision.new(false, ref_update, short_message, long_message))
  end

  # Records a Decision.
  #
  # The Decision's ref update is removed from remaining_updates.
  #
  # Returns nothing.
  def record(decision)
    refname = decision.ref_update.refname

    if @decisions.key?(refname)
      fail "Decided #{decision.inspect} but already had decided #{@decisions[refname]}"
    end
    unless @remaining_updates.key?(refname)
      fail "Decided #{decision.inspect} for #{refname} but it wasn't present in remaining_updates"
    end

    @decisions[refname] = decision
    @remaining_updates.delete(refname)
  end

  def top_level_ref?(ref_update)
    !ref_update.refname.start_with?("refs/".freeze)
  end

  def invalid_ref?(ref_update)
    !Rugged::Reference.valid_name?(ref_update.refname)
  end

  HIDDEN_REFS = %w[
    refs/pull
    refs/__gh__
  ].freeze

  HIDDEN_REF_PREFIXES = %w[
    refs/pull/
    refs/__gh__/
  ].freeze

  def hidden_ref?(ref_update)
    HIDDEN_REFS.include?(ref_update.refname) || ref_update.refname.start_with?(*HIDDEN_REF_PREFIXES)
  end

  def svn_ref?(ref_update)
    ref_update.refname.start_with?("refs/__gh__/svn/")
  end

  def tag_ref?(ref_update)
    ref_update.refname.start_with?("refs/tags/")
  end

  def private_email_ref?(ref_update)
    return false if !actor.try(:warn_private_email?) || !actor.has_primary_email? || actor.primary_user_email.public?

    if tag_ref?(ref_update)
      return false if ref_update.after_oid == GitHub::NULL_OID
      ref_emails = [repository.objects.read(ref_update.after_oid).try(:author_email)]
    elsif ref_update.after_commit
      ref_emails = [ref_update.after_commit.author_email, ref_update.after_commit.committer_email]
    end

    emails = UserEmail.where(email: ref_emails, user_id: actor.id)

    emails.any? &&
    emails.any? do |email|
      (!email.primary_role? || !email.try(:public?)) &&
      !email.to_s.include?("users.noreply")
    end
  end

  def default_branch?(ref_update)
    ref_update.refname == "refs/heads/#{default_branch}"
  end

  def check_top_level_ref_policy(ref_update)
    deny(ref_update, "funny refname",
         "refusing to create funny ref '#{ref_update.refname}' remotely\n")
  end

  def check_invalid_ref_policy(ref_update)
    deny(ref_update, "funny refname",
         "refusing to create funny ref '#{ref_update.refname}' remotely\n")
  end

  def check_slumlord_policy(ref_update)
    if svn_ref?(ref_update)
      allow(ref_update)
    else
      deny(ref_update, "non-SVN ref update denied")
    end
  end

  def check_bot_actions_prohibitions(ref_update)
    # Refs can point to any git object but Actions can only execute on treeish
    # objects, as there is no concept of a workflow file within them otherwise.
    #
    # Peel the git object to retrieve a commit, tree or nil. We could peel to
    # trees but using commits where possible will reduce the size of the cache
    # for `read_tree_diff` as all other callers use commits.
    #
    # We can use NULL_OID instead of non-treeish objects to produce diffs.
    # e.g.
    #   ref is update from a blob to a commit
    #   diff null_oid and commit to check if a new workflow file will be added
    before_oid = repository.rpc.peel_to_commit_or_tree(ref_update.before_oid) || GitHub::NULL_OID
    after_oid = repository.rpc.peel_to_commit_or_tree(ref_update.after_oid) || GitHub::NULL_OID
    diff = repository.rpc.read_tree_diff(before_oid, after_oid, paths: UNWRITABLE_PATHS_FOR_APPS, diff_trees: true)
    # for every file change in the update, check whether that file can be changed
    diff.each do |diff_entry|
      file = diff_entry["new_file"]
      path = file["path"]
      oid = file["oid"]
      unless RefUpdatesPolicy.path_updateable_for_app?(repository, path, oid)
        return deny(ref_update, bot_action_prohibited_message(path))
      end
    end
  end

  def bot_action_prohibited_message(path)
    # Clients might be depending upon these error messages. Before changing please check with:
    # * GitHub Desktop
    if auth_via_integration_without_workflow_permission
      "refusing to allow a GitHub App to create or update workflow `#{path}` without `workflows` permission"
    elsif auth_via_oauth_app_without_workflow_write_scope || public_key_from_oauth_without_workflow_write_scope
      "refusing to allow an OAuth App to create or update workflow `#{path}` without `workflow` scope"
    else
      "refusing to allow an integration to create or update workflow `#{path}`"
    end
  end

  def check_hidden_ref_policy(ref_update)
    deny(ref_update, "deny updating a hidden ref")
  end

  def check_default_branch_policy(ref_update)
    if ref_update.after_oid == GitHub::NULL_OID
      deny(ref_update, "refusing to delete the current branch: #{ref_update.refname}")
    end
  end

  def check_private_email_policy(ref_update)
    long_message = <<~LONGMESSAGE
      error: GH007: Your push would publish a private email address.
      You can make your email public or disable this protection by visiting:
      #{Rails.application.routes.url_helpers.settings_user_emails_url(host: GitHub.host_name)}
    LONGMESSAGE

    GitHub.dogstats.increment("private_email.push_rejected")
    deny(ref_update, "push declined due to email privacy restrictions", long_message)
  end

  def check_protected_branches_policy
    return if remaining_updates.empty?

    decisions = ProtectedBranchPolicy.check(repository, remaining_updates, actor)

    remaining_updates.zip(decisions).each do |ref_update, decision|
      decision.instrument_decision

      if decision.can_update_ref?
        allow(ref_update)
      else
        long_message = "error: GH006: Protected branch update failed for #{ref_update.refname}.\n"
        long_message << "error: #{decision.message}\n"
        deny(ref_update, "protected branch hook declined", long_message)
      end
    end
  end

  def auth_via_integration_without_workflow_permission
    installation = installation_for_actor
    return false unless installation
    !repository.resources.workflows.writable_by?(installation)
  end

  def installation_for_actor
    return unless actor.is_a?(User)
    return actor.installation if actor.bot?
    return unless actor.using_auth_via_integration?
    IntegrationInstallation.with_repository(repository).where(integration: actor.oauth_access.application).first
  end

  def auth_via_oauth_app_without_workflow_write_scope
    actor.is_a?(User) && actor.using_auth_via_oauth_application? && !actor.oauth_access?("workflow")
  end

  def public_key_from_oauth_without_workflow_write_scope
    actor.is_a?(PublicKey) &&
      actor.created_by_oauth_application? &&
      !actor.oauth_access?("workflow")
  end

  def public_key_created_by_unknown
    actor.is_a?(PublicKey) && actor.created_by_unknown?
  end
end
