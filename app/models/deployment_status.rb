# rubocop:disable Style/FrozenStringLiteralComment

class DeploymentStatus < ApplicationRecord::Domain::Repositories
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  SUCCESS = "success".freeze
  INACTIVE = "inactive".freeze

  PREVIEW_STATES = ["in_progress", "queued"].freeze
  STATES = (PREVIEW_STATES + ["pending", SUCCESS, "failure", INACTIVE, "error"]).freeze

  belongs_to :creator, class_name: "User"
  belongs_to :deployment
  belongs_to :performed_via_integration,
    foreign_key: :performed_by_integration_id,
    class_name: "Integration"

  after_commit :instrument_create, on: :create
  after_commit :instrument_update, on: :update
  after_commit :track_state_stats
  after_commit :trigger_environment_changed_events, on: :create
  after_commit :notify_socket_subscribers, on: :create
  after_commit :update_deployments_latest_status_and_environment, on: :create

  before_save :set_environment

  validates_presence_of :creator_id
  validates_presence_of :deployment_id
  validates_inclusion_of :state, in: STATES
  validates_length_of :description, maximum: 140, allow_nil: true
  validates_format_of :environment_url, :log_url, with: /\Ahttp/,
    on: :create, message: "must use http(s) scheme", allow_blank: true
  validate :limit_per_deployment, on: :create

  extend GitHub::RenameColumn
  rename_column target_url: :log_url

  # Internal: the max number of DeploymentStatuses per Deployment
  def self.max_per_deployment
    100
  end

  def update_deployments_latest_status_and_environment
    deployment.update(latest_deployment_status_id: id, latest_status_state: state, latest_environment: environment)
  end

  # Internal: enforce a max number of DeploymentStatuses per Deployment
  # Fired via a validation
  def limit_per_deployment
    if deployment.statuses.count >= self.class.max_per_deployment
      errors.add :base, "This deployment has reached the maximum number of statuses."
    end
  end

  # Public - check if the state is 'success'
  #
  # Returns true if the state is 'success', otherwise it returns false.
  def succeeded?
    state == SUCCESS
  end

  # Public - get the sha for the Deployment
  #
  # Returns the 40 character sha for the deployment
  def sha
    deployment.sha
  end

  # Public - get the repository for the Deployment
  #
  # Returns the Repository associated with the status
  def repository
    deployment.repository
  end

  # Public - is this the latest status on a deployment?
  def latest_status?
    deployment.latest_status.id == self.id
  end

  private

  def event_payload
    event_context
  end

  def event_context(prefix: event_prefix)
    {
      "#{prefix}_id".to_sym => id,
      "#{prefix}_state".to_sym => state,
    }
  end

  def instrument_create
    instrument :create
  end

  def instrument_update
    instrument :update
  end

  def track_state_stats
    GitHub.dogstats.increment("deployment_status", tags: ["state:#{state}"])
  end

  def notify_socket_subscribers
    deployment.pull_requests.each do |pull_request|
      pull_request.notify_socket_subscribers
    end

    GitHub::WebSocket.notify_deployment_channel(deployment, deployment.dashboard_channel, {
      wait: default_live_updates_wait,
    })
  end

  # If an explicit environment is not set,
  # a DeploymentStatus environment will default to
  #  - The environment of the previous status on the deployment, if it exists
  #  - Otherwise, the environment of the deployment
  def set_environment
    return if self.environment

    previous_status = deployment.statuses.order("id DESC").first

    if previous_status&.environment
      self.environment = previous_status.environment
    else
      self.environment = deployment.environment
    end
  end

  # If this deployment status modified the environment of the deployment,
  # create an issue event.
  def trigger_environment_changed_events
    previous_environment = if deployment.statuses.count == 1
      deployment.environment
    else
      previous_status = deployment.statuses.second
      previous_status.environment
    end

    # No change in environment
    return if previous_environment == environment

    deployment.pull_requests.each do |pull_request|
      pull_request.issue.create_deployment_environment_changed_event(self)
    end
  end
end
