# frozen_string_literal: true

module AppNameValidity
  # Public: Returns true if this object's `name` starts with GitHub or Gist, case insensitive.
  def name_starts_with_github?
    normalized_name = name.downcase.gsub(/\s+/, "")
    normalized_name =~ /\A(?:github|gist).*\z/
  end

  # Public: Returns true if this object's `name` implies the object is a GitHub product.
  def name_implies_github_affiliation?
    normalized_name = name.downcase.gsub(/\s+/, "")
    normalized_name =~ /\A.*(?:from|by)github\z/
  end
end
