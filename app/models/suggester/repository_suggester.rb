# frozen_string_literal: true

module Suggester
  class RepositorySuggester
    SUGGESTION_LIMIT = 1000

    # Create a Suggester to provide suggested objects for a given context.
    #
    # viewer  - User seeking suggestions
    # subject - Commit, Issue, PullRequest, or Repository on which suggestions
    #           are to be made
    #
    # Returns a Suggester instance.
    def initialize(viewer:, subject:)
      @viewer = viewer
      @subject = subject
      @repository = @subject.repository # Note: Repository#repository returns `self`
    end

    def issues
      issues = @repository.issues.suggestions.not_spammy
      issues = issues.where("issues.id != ?", @subject.id) if @subject.is_a?(Issue)
      issues
    end

    def discussions
      discussions = @repository.discussions.suggestions.not_spammy
      discussions = discussions.where.not(id: @subject.id) if @subject.is_a?(Discussion)
      discussions
    end

    def issues_and_discussions
      (issues + discussions).sort { |a, b| b.updated_at <=> a.updated_at }.slice(0, SUGGESTION_LIMIT)
    end

    def mentions
      format = Suggester::MentionSerializer.new(viewer: @viewer)
      format.dump(users, teams)
    end

    private

    def users
      users = []
      if @subject.respond_to?(:participants_for)
        users.concat(@subject.participants_for(@viewer, optimize_repo_access_checks: true))
      end

      users.concat(@repository.mentionable_users_for(
        @viewer,
        limit: Repository::JSON_USER_MENTION_LIMIT,
        fields: ["users.id", :login, :suspended_at, :spammy, :spammy_reason]))

      filter = BlockedUserFilter.new(viewer: @viewer)
      users = users.compact.uniq.reject(&filter)

      GitHub::PrefillAssociations.for_profiles(users)
      GitHub::PrefillAssociations.for_user_statuses(users)
      users
    end

    def teams
      if (org = @repository.organization)
        org.visible_teams_for(@viewer, fields: [:description, :slug, :id, :organization_id])
      else
        []
      end
    end
  end
end
