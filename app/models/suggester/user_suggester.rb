# frozen_string_literal: true

module Suggester
  class UserSuggester
    def initialize(viewer:)
      @viewer = viewer
    end

    def mentions
      to_hash = Suggester::UserSerializer.new(viewer: @viewer)
      users.map(&to_hash)
    end

    private

    def users
      list = []
      list.concat(@viewer.following)
      list.concat(@viewer.organizations)
      list.concat(@viewer.billing_manager_organizations)

      filter = Suggester::BlockedUserFilter.new(viewer: @viewer)
      list = list.reject(&filter)

      GitHub::PrefillAssociations.for_profiles(list)
      GitHub::PrefillAssociations.for_user_statuses(list)
      list
    end
  end
end
