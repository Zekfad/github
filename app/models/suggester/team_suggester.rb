# frozen_string_literal: true

module Suggester
  class TeamSuggester
    def initialize(viewer:, team:)
      @viewer = viewer
      @org = team.organization
    end

    def mentions
      format = Suggester::MentionSerializer.new(viewer: @viewer)
      format.dump(users, teams)
    end

    private

    def users
      filter = BlockedUserFilter.new(viewer: @viewer)
      suggested_users = @org.members.reject(&filter)

      GitHub::PrefillAssociations.for_profiles(suggested_users)
      GitHub::PrefillAssociations.for_user_statuses(suggested_users)
      suggested_users
    end

    def teams
      @org.visible_teams_for(@viewer)
    end
  end
end
