# frozen_string_literal: true

module Suggester
  # Converts User objects into the JSON format expected by the mention
  # suggester protocol. The client-side suggestion menu renders these
  # JSON objects as menu items.
  #
  # Examples
  #
  #   format = Suggester::UserSerializer.new(viewer: user)
  #
  #   format.dump(user)
  #   # => {id, login, …}
  #
  #   users.map(&format)
  #   # => [{id, login, …}, …]
  class UserSerializer
    def initialize(viewer:)
      @viewer = viewer
    end

    def dump(user)
      {
        type: "user",
        id: user.id,
        login: user.login,
        name: name(user),
      }
    end

    def to_proc
      method(:dump).to_proc
    end

    private

    def name(user)
      prefix = user.profile_name
      suffix = user.busy?(viewer: @viewer) ? "(busy)" : nil
      [prefix, suffix].compact.join(" ")
    end
  end
end
