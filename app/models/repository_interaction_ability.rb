# frozen_string_literal: true

class RepositoryInteractionAbility

  include Instrumentation::Model

  # The number of hours a user had to have been created before to be considered
  # a sockpuppet.
  TIME_LIMIT_HOURS = 24

  # The possible interaction limits that can be enabled. These should each have
  # `#{name}_enabled?` and `self.#{name}_enabled?` methods defined in this class.
  INTERACTION_LIMITS = [:sockpuppet_ban, :contributors_only, :collaborators_only]

  TTL_OPTIONS = { one_day: 1.day, one_week: 1.week }

  SOCKPUPPET_KEY = "sockpuppet_disallowed."
  CONTRIBUTORS_KEY = "contributors_only."
  COLLABORATORS_KEY = "collaborators_only."

  attr_reader :type, :object

  # Public: Checks if the sockpuppet ban limit is enabled, which prevents newer
  # accounts from interacting with a repository.
  #
  # type   - The Symbol type that this interaction ban is for (:repository or :organization)
  # object - The Repository or Organization that you are checking.
  #
  # Returns a Boolean.
  def self.sockpuppet_ban_enabled?(type, object)
    async_sockpuppet_ban_enabled?(type, object).sync
  end

  def self.async_sockpuppet_ban_enabled?(type, object)
    new(type, object).async_sockpuppet_ban_enabled?
  end

  # Public: Checks if the contributors only limit is enabled, which ensures that
  # only prior contributors can interact with a repository.
  #
  # type   - The Symbol type that this interaction ban is for (:repository or :organization)
  # object - The Repository or Organization that you are checking.
  #
  # Returns a Boolean.
  def self.contributors_only_enabled?(type, object)
    async_contributors_only_enabled?(type, object).sync
  end

  def self.async_contributors_only_enabled?(type, object)
    new(type, object).async_contributors_only_enabled?
  end

  # Public: Checks if the collaborators only limit is enabled, which ensures that
  # only collaborators can interact with a repository.
  #
  # type   - The Symbol type that this interaction ban is for (:repository or :organization)
  # object - The Repository or Organization that you are checking.
  #
  # Returns a Boolean.
  def self.collaborators_only_enabled?(type, object)
    async_collaborators_only_enabled?(type, object).sync
  end

  def self.async_collaborators_only_enabled?(type, object)
    new(type, object).async_collaborators_only_enabled?
  end

  # Public: Check if a limit should restrict a user from interacting with a repository.
  #
  # limit      - A Symbol interaction limit name; one of INTERACTION_LIMITS
  # repository - The Repository a user is interacting with.
  # user       - The User that is interacting with the repository.
  #
  # Returns a Boolean.
  def self.restricted_by_limit?(limit, repository, user)
    async_restricted_by_limit?(limit, repository, user).sync
  end

  def self.async_restricted_by_limit?(limit, repository, user)
    raise ArgumentError, "Expected limit to be one of #{INTERACTION_LIMITS}, was #{limit}" unless INTERACTION_LIMITS.include?(limit)

    return Promise.resolve(false) if repository.private?

    checks = [
      send("async_#{limit}_enabled?", :repository, repository),
      async_user_exempt?(limit, repository, user),
    ]

    Promise.all(checks).then do |repo_limit_enabled, user_exempt|
      next false if user_exempt

      next true if repo_limit_enabled

      async_organization_limit_enabled?(limit, repository)
    end
  end

  # Public: Check if an interaction limit doesn't apply to a user.
  #
  # limit      - A Symbol interaction limit name; one of INTERACTION_LIMITS
  # repository - The Repository a user is interacting with.
  # user       - The User that is interacting with the repository.
  #
  # Returns a Boolean.
  def self.user_exempt?(limit, repository, user)
    async_user_exempt?(limit, repository, user).sync
  end

  def self.async_user_exempt?(limit, repository, user)
    raise ArgumentError, "Expected limit to be one of #{INTERACTION_LIMITS}, was #{limit}" unless INTERACTION_LIMITS.include?(limit)

    # Bots are exempt from interaction limits.
    return Promise.resolve(true) unless user.user?

    checks = [
      async_user_recently_created?(user),
      repository.async_contributor?(user),
      repository.async_pushable_by?(user),
      repository.async_member?(user),
    ]

    Promise.all(checks).then do |recently_created, contributor, pushable, member|
      case limit
      when :sockpuppet_ban
        !recently_created || contributor || pushable || member
      when :contributors_only
        contributor || pushable || member
      when :collaborators_only
        pushable || member
      end
    end
  end

  # Public: Disable all interaction limits for a repository.
  #
  # type   - The Symbol type that the interaction ban is for (:repository or :organization)
  # object - The Repository or Organization to disable interaction limits for.
  # actor  - The User disabling the interaction limits.
  # staff_actor - A Boolean indicating if audit log events should guard the
  # actor's identity, and log the `github-staff` account instead.
  #
  # Returns nothing.
  def self.disable_all_for(type, object, actor = User.staff_user, staff_actor: false)
    ability = new(type, object)
    ability.toggle_ability(:sockpuppet_ban, actor, staff_actor: staff_actor) if ability.sockpuppet_ban_enabled?
    ability.toggle_ability(:contributors_only, actor, staff_actor: staff_actor) if ability.contributors_only_enabled?
    ability.toggle_ability(:collaborators_only, actor, staff_actor: staff_actor) if ability.collaborators_only_enabled?
  end

  # Public: Check if an object has any limits currently enabled.
  #
  # type   - The Symbol type that this interaction ban is for (:repository or :organization)
  # object - The Repository or Organization that you are checking.
  #
  # Returns a Boolean.
  def self.has_active_limits?(type, object)
    async_has_active_limits?(type, object).sync
  end

  def self.async_has_active_limits?(type, object)
    ability = new(type, object)

    return Promise.resolve(false) if ability.repository? && object.private?

    limit_checks = [
      ability.async_sockpuppet_ban_enabled?,
      ability.async_contributors_only_enabled?,
      ability.async_collaborators_only_enabled?,
    ]

    Promise.all(limit_checks).then do |sockpuppet, contributors, collaborators|
      next true if sockpuppet || contributors || collaborators

      next false unless ability.repository?

      ability.async_has_organization_limit?
    end
  end

  # Public: Initialize a RepositoryInteractionAbility instance.
  #
  # type   - The Symbol type that this interaction ban is for (:repository or :organization)
  # object - The Repository or Organization that you are checking.
  def initialize(type, object)
    @type = type
    @object = object
  end

  # Public: Checks if the sockpuppet ban is enabled.
  #
  # Returns a Boolean.
  def sockpuppet_ban_enabled?
    async_sockpuppet_ban_enabled?.sync
  end

  def async_sockpuppet_ban_enabled?
    async_limit_enabled?(:sockpuppet_ban)
  end

  # Public: Checks if the contributors only limit is enabled.
  #
  # Returns a Boolean.
  def contributors_only_enabled?
    async_contributors_only_enabled?.sync
  end

  def async_contributors_only_enabled?
    async_limit_enabled?(:contributors_only)
  end

  # Public: Checks if the collaborators only limit is enabled.
  #
  # Returns a Boolean.
  def collaborators_only_enabled?
    async_collaborators_only_enabled?.sync
  end

  def async_collaborators_only_enabled?
    async_limit_enabled?(:collaborators_only)
  end

  # Public: The current limit that is active for this object.
  #
  # Returns a Promise<Symbol>.
  def async_local_active_limit
    checks = [
      async_sockpuppet_ban_enabled?,
      async_contributors_only_enabled?,
      async_collaborators_only_enabled?,
    ]

    Promise.all(checks).then do |sockpuppet_ban, contributors_only, collaborators_only|
      if sockpuppet_ban
        :sockpuppet_ban
      elsif contributors_only
        :contributors_only
      elsif collaborators_only
        :collaborators_only
      else
        :no_limit
      end
    end
  end

  # Public: The current limit, either at the repo or org level, that is active
  # for this object.
  #
  # Returns a Promise<Symbol>.
  def async_overall_active_limit
    async_has_organization_limit?.then do |has_org_limit|
      if has_org_limit
        object.async_owner.then do |owner|
          ability = self.class.new(:organization, owner)
          ability.async_local_active_limit
        end
      else
        async_local_active_limit
      end
    end
  end

  # Public: The origin of the currently active limit.
  #
  # Returns a Promise<Symbol>.
  def async_active_limit_origin
    return Promise.resolve(:organization) if organization?

    async_has_organization_limit?.then do |has_org_limit|
      has_org_limit ? :organization : :repository
    end
  end

  # Public: Does this object have an active organization-level limit?
  #
  # Returns a Promise<Boolean>.
  def async_has_organization_limit?
    return Promise.resolve(false) unless repository?

    object.async_owner.then do |owner|
      next false unless owner.organization?

      self.class.async_has_active_limits?(:organization, owner)
    end
  end

  # Public: Get the currently enabled repo or organization limit's expiration.
  #
  # Returns a Promise<DateTime | nil>.
  def async_overall_active_limit_expiry
    async_has_organization_limit?.then do |has_org_limit|
      if has_org_limit
        object.async_owner.then do |owner|
          ability = self.class.new(:organization, owner)
          ability.async_local_active_limit_expiry
        end
      else
        async_local_active_limit_expiry
      end
    end
  end

  # Public: Get the currently enabled interaction limit's expiration.
  #
  # Returns a Promise<DateTime | nil>.
  def async_local_active_limit_expiry
    async_local_active_limit.then do |limit|
      next nil if limit == :no_limit

      async_expiry(limit)
    end
  end

  # Public: Get the currently enabled interaction limit's expiration.
  #
  # Returns a Boolean.
  def local_active_limit_expiry
    async_local_active_limit_expiry.sync
  end

  # Public: Sets an interaction limit.
  #
  # limit - A Symbol interaction limit name; one of INTERACTION_LIMITS
  # actor - The User setting this interaction limit.
  # ttl   - When the interaction limit should expire; one of TTL_OPTIONS.
  # staff_actor - A Boolean indicating if audit log events should guard the
  # actor's identity, and log the `github-staff` account instead.
  #
  # Returns a Boolean.
  def set_ability(limit, actor, ttl = TTL_OPTIONS.keys.first, staff_actor: false)

    if repository? && object.owner.organization?
      # Don't allow enabling repo level limits if an organization interaction
      # limit is already enabled.
      return false if self.class.has_active_limits?(:organization, object.owner)
    end

    # Return early if the limit is already enabled.
    return true if !(limit == :no_limit) && send("#{limit}_enabled?")

    # Disable any existing limits before enabling a new limit.
    # Only one limit should be enabled at a time.
    self.class.disable_all_for(type, object, actor, staff_actor: staff_actor)

    # Return early if we just needed to disable all limits.
    return true if limit == :no_limit

    key = key_for(limit)

    enable_key(key, ttl, actor, staff_actor: staff_actor)

    if organization?
      # Disable repository-level interaction limits for public repositories
      # in the organization, since the org-level limit is set instead.
      DisableRepositoryInteractionLimitsJob.perform_later(object.id, actor.id)
    end

    true
  end

  # Public: Toggle an interaction limit.
  #
  # NOTE: This method will soon be deprecated. Please use `#set_ability` instead.
  #
  # limit - A Symbol interaction limit name; one of INTERACTION_LIMITS
  # actor - The User toggling this interaction limit.
  # ttl   - When the interaction limit should expire; one of TTL_OPTIONS.
  # staff_actor - A Boolean indicating if audit log events should guard the
  # actor's identity, and log the `github-staff` account instead.
  #
  # Returns nothing.
  def toggle_ability(limit, actor, ttl = TTL_OPTIONS.keys.first, staff_actor: false)

    key = key_for(limit)

    if send("#{limit}_enabled?")
      disable_key(key, actor, staff_actor: staff_actor)
    else
      if repository? && object.owner.organization?
        # Don't allow enabling repo level limits if an organization interaction
        # limit is already enabled.
        return if self.class.has_active_limits?(:organization, object.owner)
      end

      # Disable any existing limits before enabling a new limit.
      # Only one limit should be enabled at a time.
      self.class.disable_all_for(type, object, staff_actor: staff_actor)

      enable_key(key, ttl, actor, staff_actor: staff_actor)

      if organization?
        # Disable repository-level interaction limits for public repositories
        # in the organization, since the org-level limit is set instead.
        DisableRepositoryInteractionLimitsJob.perform_later(object.id, actor.id)
      end
    end
  end

  # Public: Is the current interaction ability instance for an organization?
  #
  # Returns a Boolean.
  def organization?
    type == :organization && object.is_a?(Organization)
  end

  # Public: Is the current interaction ability instance for a repository?
  #
  # Returns a Boolean.
  def repository?
    type == :repository && object.is_a?(Repository)
  end

  # Private: Was this user created within the the sockpuppet timeframe?
  #
  # user - the User to check.
  #
  # Returns a Promise<Boolean>.
  def self.async_user_recently_created?(user)
    Promise.resolve((Time.now - user.created_at)/1.hour < TIME_LIMIT_HOURS)
  end
  private_class_method :async_user_recently_created?

  # Private: Check if an organization level interaction limit is enabled.
  #
  # limit      - A Symbol interaction limit name; one of INTERACTION_LIMITS
  # repository - The Repository a user is interacting with.
  #
  # Returns a Promise<Boolean>.
  def self.async_organization_limit_enabled?(limit, repository)
    repository.async_owner.then do |owner|
      next false unless owner.organization?

      send("async_#{limit}_enabled?", :organization, owner)
    end
  end
  private_class_method :async_organization_limit_enabled?

  private

  # Private: The event prefix used for audit log events.
  #
  # Returns a String.
  def event_prefix
    prefix = organization? ? "org" : "repo"
    "#{prefix}.config"
  end

  # Private: Generates the correct KV key for a limit.
  #
  # limit - A Symbol interaction limit name; one of INTERACTION_LIMITS
  #
  # Returns a String.
  def key_for(limit)
    prefix = organization? ? "org." : "repo."

    key = case limit
    when :sockpuppet_ban
      SOCKPUPPET_KEY
    when :contributors_only
      CONTRIBUTORS_KEY
    when :collaborators_only
      COLLABORATORS_KEY
    else
      raise ArgumentError, "Expected limit to be one of #{INTERACTION_LIMITS}, was #{limit}"
    end

    "#{prefix}#{key}#{object.id}"
  end

  # Private: Checks to see if a limit is enabled.
  #
  # limit - A Symbol interaction limit name; one of INTERACTION_LIMITS
  #
  # Returns a Boolean.
  def limit_enabled?(limit)
    async_limit_enabled?(limit).sync
  end

  def async_limit_enabled?(limit)
    key = key_for(limit)

    Platform::Loaders::KV.load(key).then do |value|
      !!value
    end
  end

  # Private: Delete an interaction limit key from KV.
  #
  # key - The String key used to store the interaction limit in KV, e.g. `repo.sockpuppet_disallowed.123`
  # actor - The User deleting this key.
  #
  # Returns a DateTime.
  def get_expiry(limit)
    async_expiry(limit).sync
  end

  def async_expiry(limit)
    key = key_for(limit)

    Platform::Loaders::KV.load(key).then do |expiry|
      if expiry
        DateTime.parse(expiry)
      else
        Promise.resolve(nil)
      end
    end
  end

  # Private: Create a key in KV for an interaction limit.
  #
  # key - The String key used to store the interaction limit in KV, e.g. `repo.sockpuppet_disallowed.123`
  # actor - The User creating this key.
  # staff_actor - A Boolean indicating if audit log events should guard the
  # actor's identity, and log the `github-staff` account instead.
  #
  # Returns nothing.
  def enable_key(key, ttl_key, actor, staff_actor: false)
    action = key_action(key)

    instrument_update("enable_#{action}", actor, staff_actor: staff_actor)
    GitHub.dogstats.increment("#{action}.enable", tags: stats_tags)
    ttl = TTL_OPTIONS[ttl_key].from_now
    GitHub.kv.set(key, ttl.to_s, expires: ttl)
  end

  # Private: Delete an interaction limit key from KV.
  #
  # key - The String key used to store the interaction limit in KV, e.g. `repo.sockpuppet_disallowed.123`
  # actor - The User deleting this key.
  # staff_actor - A Boolean indicating if audit log events should guard the
  # actor's identity, and log the `github-staff` account instead.
  #
  # Returns nothing.
  def disable_key(key, actor, staff_actor: false)
    action = key_action(key)

    instrument_update("disable_#{action}", actor, staff_actor: staff_actor)
    GitHub.dogstats.increment("#{action}.disable", tags: stats_tags)
    GitHub.kv.del(key)
  end

  # Private: Extract the action from the interaction limit key.
  #
  # key - The String key used to store the interaction limit in KV, e.g. `repo.sockpuppet_disallowed.123`
  #
  # Returns a String.
  def key_action(key)
    key = key.split(".")
    key[1]
  end

  # Private: The tags to use when logging DataDog stats.
  #
  # Returns an Array.
  def stats_tags
    organization? ? ["type:org"] : []
  end

  # Private: Instrument the toggling of an interaction limit.
  #
  # action - The action being performed.
  # actor - The User performing the action.
  # staff_actor - A Boolean indicating if audit log events should guard the
  # actor's identity, and log the `github-staff` account instead.
  #
  # Returns nothing.
  def instrument_update(action, actor, staff_actor: false)
    context = object.event_context

    if repository? && object.owner.organization?
      context = context.merge(object.owner.event_context)
    end

    if staff_actor
      guarded_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
      instrument action, context.merge(guarded_actor)
    else
      instrument action, context.merge({ actor: actor })
    end
  end
end
