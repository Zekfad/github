# rubocop:disable Style/FrozenStringLiteralComment

class U2fRegistration < ApplicationRecord::Domain::Users
  include Instrumentation::Model

  MAX_KEYS_PER_USER = 100

  belongs_to :user

  validate :validate_certificate
  validate :validate_public_key
  validate :validate_webauthn_attestation
  validate :validate_keys_per_user
  validate :validate_certificate_or_attestation_present
  validates :key_handle,               presence: true, length: { maximum: 1000 }
  validates :certificate,              length: { maximum: 10000 }
  validates :public_key,               presence: true, length: { maximum: 1000 }
  validates :nickname,                 presence: true, uniqueness: { scope: :user_id, case_sensitive: true }
  validates :counter,                  presence: true
  validates :webauthn_attestation,     length: { maximum: 65535 }
  validates :is_webauthn_registration, inclusion: { in: [true, false] }

  after_create_commit  :instrument_creation
  after_destroy_commit :instrument_deletion

  # Public: Check a signing response against a challenge.
  #
  # challenge - A SignRequest challenge String.
  # response  - A SignResponse.
  #
  # Returns boolean.
  def authenticated?(challenge, response)
    GitHub.u2f.authenticate!(challenge, response, raw_public_key, counter)
    update(counter: response.counter)

    GitHub.dogstats.increment("authentication.u2f", tags: ["result:success"])

    true
  rescue U2F::Error => e
    error_type = e.class.name.split("::").last.underscore
    GitHub.dogstats.increment("authentication.u2f", tags: [
      "result:failure",
      "reason:#{error_type}",
    ])

    false
  end

  # Public: Check a signing response against a challenge.
  #
  # origin    - The web origin of the client response.
  # challenge - A SignRequest challenge String.
  # response  - A WebAuthn::AuthenticatorAssertionResponse.
  #
  # Returns boolean.
  def webauthn_authenticated?(origin, challenge, response)
    unless GitHub.webauthn_allowed_origin?(origin)
      GitHub.dogstats.increment("authentication.webauthn", tags: [
        "result:failure",
        "reason:invalid_origin",
      ])
      return false
    end

    # Signatures for legacy U2F registrations use the U2F app ID instead of the
    # RP ID. Our WebAuthn library doesn't support extensions, so we have to
    # compute this ourselves.
    rp_id = is_webauthn_registration ? GitHub.webauthn_rp_id : GitHub.u2f_app_id

    unless response.valid?(Base64.urlsafe_decode64(challenge),
                           origin,
                           rp_id: rp_id,
                           public_key: raw_public_key,
                           sign_count: counter,
                          )
      GitHub.dogstats.increment("authentication.webauthn", tags: [
        "result:failure",
        "reason:authentication_failed_error",
      ])
      return false
    end

    update(counter: response.authenticator_data.sign_count)

    GitHub.dogstats.increment("authentication.webauthn", tags: ["result:success", "zero_counter:#{response.authenticator_data.sign_count.zero?}"])

    true
  end

  # Public: The raw certificate bytes for this registration.
  #
  # Returns a String.
  def raw_public_key
    @raw_public_key ||= Base64.strict_decode64(public_key)
  end

  # Public: `webauthn` credential descriptor for this registration. This
  # represents the the credential passed to the browser, as described in
  # https://www.w3.org/TR/webauthn/#dom-publickeycredentialrequestoptions-allowcredentials
  #
  # Returns a Hash.
  def webauthn_public_key_credential_hash_for_client
    {
      type: "public-key",
      id: key_handle,
    }
  end

  private
  # Private: Prefix for instrumentation events.
  #
  # Returns a Symbol.
  def event_prefix
    :security_key
  end

  # Private: Instrument creating records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_creation(payload = {})
    GitHub.dogstats.increment "u2f_registration", tags: [
      "action:register",
      "recent_security_checkup:#{user.recently_took_action_on_security_checkup?}",
    ]
    instrument :register, payload.merge(user: user, nickname: nickname)
  end

  # Private: Instrument deleting records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_deletion(payload = {})
    GitHub.dogstats.increment "u2f_registration", tags: ["action:remove"]
    instrument :remove, payload.merge(user: user, nickname: nickname)
  end

  # Validate that the provided certificate can be parsed.
  #
  # Returns nothing.
  def validate_certificate
    return if certificate.nil?
    OpenSSL::X509::Certificate.new(Base64.strict_decode64(certificate))
  rescue OpenSSL::X509::CertificateError, ArgumentError
    errors.add(:certificate, "is invalid")
  end

  # Validate that the provided public key can be parsed.
  #
  # Returns nothing.
  def validate_public_key
    return if public_key.nil?

    begin
      raw = raw_public_key
    rescue ArgumentError
      return errors.add(:public_key, "is invalid")
    end

    # Detect uncompressed ECDSA points used by FIDO U2F. This is going away, as
    # we are migrating data to the COSE format used by WebAuthn.
    if raw_public_key.bytesize == 65 && raw_public_key.bytes.first == 0x04
      begin
        U2F::U2F.public_key_pem(raw_public_key)
      rescue U2F::PublicKeyDecodeError
        return errors.add(:public_key, "is invalid")
      end
    else
      begin
        COSE::Key.deserialize(raw_public_key)
      # Ideally, we'd only have to catch a single type of error, like
      # CBOR::UnpackError. However, it seems that `COSE::Key.deserialize` throws
      # a *lot* of different errors in practice.
      #
      # This list is based on
      # https://github.com/github/github/issues/117575#issuecomment-504136938
      #
      # (Note that `CBOR::MalformedFormatError` is the only CBOR error, but we
      # catch the superclass `CBOR::UnpackError`.)
      rescue ArgumentError,
             CBOR::UnpackError,
             COSE::UnknownKeyType,
             EOFError,
             FloatDomainError,
             IndexError,
             NoMemoryError,
             NoMethodError,
             RegexpError,
             TypeError,
             URI::InvalidURIError,
             COSE::KeyDeserializationError => e

        GitHub.dogstats.increment("authentication.webauthn", tags: [
          "result:failure",
          "reason:#{e.class.name.underscore}",
          "action:validate_public_key"
        ])
        return errors.add(:public_key, "is invalid")
      end
    end
  end

  # Validate that the provided webauthn attestation can be parsed.
  #
  # Returns nothing.
  def validate_webauthn_attestation
    return unless public_key.nil?
    decoded = Base64.strict_decode64(webauthn_attestation)
    parsed = CBOR.decode(decoded)
    # Check that the attestation object has the required fields:
    # https://www.w3.org/TR/webauthn/#sctn-attestation
    ["fmt", "attStmt", "authData"].each { |key|
      if !parsed.has_key?(key)
        errors.add(:webauthn_attestation, "is missing key: #{key}")
        return
      end
    }
  rescue ArgumentError, EOFError
    errors.add(:webauthn_attestation, "is invalid")
  end

  # Check that the user hasn't registered too many keys.
  #
  # Returns nothing.
  def validate_keys_per_user
    max = new_record? ? MAX_KEYS_PER_USER - 1 : MAX_KEYS_PER_USER
    if user.u2f_registrations.count > max
      errors.add(:base, "users can only register #{MAX_KEYS_PER_USER} security keys")
    end
  end

  # Check that registration has a certificate or webauthn attestation, depending
  # on whether it's a webauthn registration:
  #
  # - Webauthn registration: has attestation
  # - U2F registration: has a certificate
  #
  # Returns nothing.
  def validate_certificate_or_attestation_present
    return if is_webauthn_registration && !webauthn_attestation.nil?
    return if !is_webauthn_registration && !certificate.nil?
    errors.add(:base, "security key registration proof (certificate or attestation) is missing")
  end
end
