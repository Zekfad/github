# frozen_string_literal: true

class IntegrationUrlValidator < ActiveModel::Validator
  def validate(record)
    unless IntegrationUrl.valid_callback_url?(record.url)
      record.errors.add(:url, "must be a valid URL")
    end
  end
end
