# frozen_string_literal: true

class DiscussionCommentReaction < ApplicationRecord::Domain::Discussions
  include GitHub::Relay::GlobalIdentification
  include Spam::Spammable

  belongs_to :user, required: true
  belongs_to :discussion_comment, required: true
  alias_method :async_subject, :async_discussion_comment

  has_one :discussion, through: :discussion_comment

  setup_spammable(:user)

  validates :content, inclusion: { in: Emotion.all.map(&:content) }

  class << self
    # Public: Idempotent all-purpose interface for ensuring a discussion
    # comment reaction exists between users and discussion comments.
    # Performs permissions checks and prevents duplicates.
    #
    # user                  - User: The user having this reaction
    # discussion_comment_id - Integer: The ID of the discussion comment of this
    # reaction
    # content               - String: One of the members of Emotion.all.map(&:content)
    #
    # Returns: A DiscussionCommentReaction in the appropriate state for the
    # discussion comment and user.
    def react(user:, discussion_comment_id:, content:)
      discussion_comment = reactable_discussion_comment_for(
        user: user,
        discussion_comment_id: discussion_comment_id,
      )

      if discussion_comment.present?
        existing_reaction = user.discussion_comment_reactions.find_by(
          discussion_comment: discussion_comment,
          content: content,
        )

        if existing_reaction.present?
          DiscussionCommentReaction::RecordStatus.new([existing_reaction, :exists])
        else
          new_discussion_comment_reaction = create(
            user: user,
            discussion_comment: discussion_comment,
            content: content,
          ).tap do |discussion_comment_reaction|
            discussion_comment.notify_socket_subscribers
            discussion_comment.synchronize_search_index

            GitHub.dogstats.increment(
              "discussion_comment_reaction",
              tags: ["action:create", "type:#{content}"],
            )
          end

          DiscussionCommentReaction::RecordStatus.new([new_discussion_comment_reaction, :created])
        end
      else
        Reaction::RecordStatus.new([new, :invalid])
      end
    end

    # Public: Idempotent all-purpose interface for ensuring a reaction DOES NOT
    # exist between users and discussion comments.
    # Performs permissions checks.
    #
    # user                  - User: The user having this reaction
    # discussion_comment_id - Integer: The ID of the discussion comment of this
    # reaction
    # content               - String: One of the members of valid_content
    #
    # Returns: A DiscussionCommentReaction in the appropriate state for the
    # discussion comment and user.
    def unreact(user:, discussion_comment_id:, content:)
      discussion_comment = reactable_discussion_comment_for(
        user: user,
        discussion_comment_id: discussion_comment_id,
      )

      if discussion_comment.present?
        existing_reaction = user.discussion_comment_reactions.find_by(
          discussion_comment: discussion_comment,
          content: content,
        )

        if existing_reaction.present?
          existing_reaction.destroy
          discussion_comment.notify_socket_subscribers
          discussion_comment.synchronize_search_index

          GitHub.dogstats.increment(
            "discussion_comment_reaction",
            tags: ["action:destroy", "type:#{content}"],
          )
          RecordStatus.new([existing_reaction, :deleted])
        else
          unpersisted_discussion_comment_reaction = new(
            user: user,
            discussion_comment: discussion_comment,
            content: content,
          )
          RecordStatus.new([unpersisted_discussion_comment_reaction, :deleted])
        end
      else
        RecordStatus.new([new, :invalid])
      end
    end

    # Public: determine whether a given discussion_comment is "unlocked" for a
    # user
    def discussion_comment_unlocked?(user, discussion_comment)
      async_discussion_comment_unlocked?(user, discussion_comment).sync
    end
    alias_method :subject_unlocked?, :discussion_comment_unlocked?

    def async_discussion_comment_unlocked?(user, discussion_comment)
      return Promise.resolve(false) unless user

      discussion_comment.async_locked_for?(user).then { |locked| !locked }
    end

    def async_viewer_can_react?(viewer, discussion_comment)
      return Promise.resolve(false) unless discussion_comment && viewer

      async_actor_can_react_to?(viewer, discussion_comment)
    end

    private

    # Internal: Does the discussion_comment exist and does the user have
    # permission to react to it?
    #
    # Returns: The DiscussionComment instance
    def reactable_discussion_comment_for(user:, discussion_comment_id:)
      return unless user && discussion_comment_id

      discussion_comment = DiscussionComment.find_by(id: discussion_comment_id)

      if async_viewer_can_react?(user, discussion_comment).sync
        discussion_comment
      end
    end

    def async_actor_can_react_to?(actor, discussion_comment)
      return Promise.resolve(false) if actor.should_verify_email?
      return Promise.resolve(false) if discussion_comment.wiped?

      async_actor_blocked?(actor, discussion_comment).then do |blocked|
        next false if blocked

        async_readable_and_unlocked_for_actor?(actor, discussion_comment)
      end
    end

    def async_actor_blocked?(actor, discussion_comment)
      discussion_comment.async_user.then do |discussion_comment_user|
        # Comment author:
        actor.async_blocked_by?(discussion_comment_user).then do |blocked_by_author|
          next true if blocked_by_author

          discussion_comment.async_discussion.then do |discussion|
            discussion.async_user.then do |discussion_author|
              # Discussion author:
              actor.async_blocked_by?(discussion_author).then do |blocked_by_discussion_author|
                next true if blocked_by_discussion_author

                # Repository owner:
                discussion_comment.async_reaction_admin.then do |admin|
                  actor.async_blocked_by?(admin)
                end
              end
            end
          end
        end
      end
    end

    def async_readable_and_unlocked_for_actor?(actor, discussion_comment)
      async_discussion_comment_unlocked?(actor, discussion_comment).then do |comment_unlocked|
        next false unless comment_unlocked

        discussion_comment.async_readable_by?(actor)
      end
    end
  end

  def emotion
    Emotion.find(content)
  end

  class RecordStatus < SimpleDelegator
    attr_reader :status

    VALID_STATUSES = [:created, :exists, :invalid, :deleted]

    def initialize(reaction_status)
      reaction, @status = *reaction_status
      raise ArgumentError.new("Invalid status: #{@status}") unless VALID_STATUSES.include?(@status)
      super(reaction)
    end

    def created?
      status == :created
    end

    def exists?
      status == :exists
    end

    def platform_type_name
      # This object is a SimpleDelegator to an underlying DiscussionCommentReaction
      "DiscussionCommentReaction"
    end
  end
end
