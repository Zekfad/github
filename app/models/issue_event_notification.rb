# rubocop:disable Style/FrozenStringLiteralComment

# Notifications when significant IssueEvents happen
class IssueEventNotification
  include GitHub::UserContent
  attr_reader :issue_event

  delegate :id, :new_record?, :created_at, :permalink,
    :issue, :repository, :async_repository, :entity, :async_entity, :notifications_list, :repository_for_commit,
    to: :issue_event

  # Public: Find an IssueEvent and create a IssueEventNotification wrapping it.
  # This is typically used by Newsies to actually deliver notifications.
  #
  # id - The IssueEvent primary key id.
  #
  # Returns a IssueEventNotification or nil when no IssueEvent exists for the id.
  def self.find_by_id(id)  # rubocop:disable GitHub/FindByDef
    if issue_event = IssueEvent.find_by_id(id)
      new(issue_event)
    end
  end

  # Public: Create a IssueEventNotification
  #
  # issue_event - The IssueEvent object this notification corresponds to.
  #
  # Returns a IssueEventNotification
  def initialize(issue_event)
    @issue_event = issue_event
  end

  # Public: Register the recipient for this notification
  #
  # id - The User ID for the recipient
  #
  # Returns nothing
  def register_recipient(id)
    @recipient = User.find_by_id(id)
  end
  attr_reader :recipient

  # Public: the body for the notification
  def body
    close_text = nil
    body = nil

    case
    when closed_via_commit?
      body = "#{event_type} ##{issue.number}"
      close_text = issue_event.commit_id
      if repository_for_commit.pullable_by?(recipient)
        close_text = "#{repository_for_commit.name_with_owner}@#{close_text}" if repository_for_commit != repository
      end
    when closed_via_pr?
      body = "#{event_type} ##{issue.number}"
      pr = issue_event.referencing_issue
      if pr.readable_by?(recipient)
        close_text = "##{pr.number}"
        close_text = "#{pr.repository.name_with_owner}#{close_text}" if pr.repository != repository
      end
    when assigned?
      body = "#{event_type} ##{issue.number}"
      body << " to @#{issue_event.actor.login}"
    when review_requested?
      if issue_event.subject&.is_a?(Team)
        body = "@#{issue_event.actor.login} requested review from @#{issue_event.subject} on: #{issue.repository.name_with_owner}##{issue.number} #{issue.title}"
      else
        body = "@#{issue_event.actor.login} requested your review on: #{issue.repository.name_with_owner}##{issue.number} #{issue.title}"
      end

      via_codeowner = issue_event.review_request && issue_event.review_request.reasons.any?(&:codeowners?)
      body += " as a code owner" if via_codeowner
    when ready_for_review?
      body = "@#{issue_event.actor.login} marked a pull request as ready for review: #{issue.repository.name_with_owner}##{issue.number} #{issue.title}"
      body << "\n\n"
      body << "View the latest changes: #{issue_event.permalink}"
      if revision = issue_event&.issue&.pull_request&.current_revision
        body << ".\n"
        body << "View the current revision: #{revision.permalink}"
      end
    when merged_pull_request?
      pr = issue_event.pull_request
      body = "#{event_type} ##{issue.number}"
      body << " into #{pr.display_base_ref_name}" if pr.present? && pr.base_ref_exist?
    else
      body = "#{event_type} ##{issue.number}"
    end

    body << " via #{close_text}" if close_text
    body << "."
    EscapeUtils.escape_html_as_html_safe(body)
  end

  # Interal: Whether we should say this was Merged or Closed
  def event_type
    return "Assigned" if assigned?
    return "Merged"   if merged_pull_request?
    issue_event.event.titlecase
  end

  # Internal: Return Boolean telling us if this close was the result of a commit.
  # i.e. a commit that says "fixes NUM_HERE..."
  def closed_via_commit?
    issue_event.close? && issue_event.commit_id.present?
  end

  # Internal: Return Boolean telling us if this close was the result of a PR.
  # i.e. a PR that says "fixes NUM_HERE..."
  def closed_via_pr?
    issue_event.close? && issue_event.referencing_issue_id.present?
  end

  # Internal: Return a Boolean telling us if this was assigned.
  def assigned?
    issue_event.assigned?
  end

  # Internal: Return a Boolean telling us if this was requested.
  def review_requested?
    issue_event.review_requested?
  end

  # Internal: Return a Boolean telling us if this was ready_for_review.
  def ready_for_review?
    issue_event.ready_for_review?
  end

  def signoff?
    issue_event.event == "signoff"
  end

  def signoff_canceled?
    issue_event.event == "signoff_canceled"
  end

  # Detect if we're closing a PullRequest due to a merge. We use the merge issue
  # event to detect this since the pull request record may not yet be updated
  # when this event is fired. The merge event is guaranteed to be created before
  # the closed event.
  def merged_pull_request?
    issue.pull_request? && issue.events.merges.any?
  end

  # Internal: the "Thread" is the issue. Used by the notifications rollup logic.
  def notifications_thread
    issue
  end

  # Public: the User is the person who created the IssueEvent. This is used as the
  # sender on outbound email notifications.
  #
  # NOTE: in the case where we don't know the closer, we fallback to issue.user
  # (i.e. the creator).
  #
  # DOUBLE NOTE: Due to the (unfortunate) flipped relation of actor_id in issue
  # events, we have to use issue_event.subject explicitly during assigned issue
  # events.
  def user
    if assigned?
      issue_event.subject || issue.user
    else
      issue_event.actor || issue.user
    end
  end

  # Internal: the author is the user.  Used by notifications delivery logic.
  def notifications_author
    user
  end

  # Public: a unique message id for this notification. Used in outgoing
  # email only. This identifier must be unique for each notification sent.
  def message_id
    prefix = issue.pull_request? ? "pull" : "issue"
    "<#{repository.name_with_owner}/#{prefix}/#{issue.number}/issue_event/#{id}@#{GitHub.urls.host_name}>"
  end

  # Internal: user_id - see user
  def user_id
    user && user.id
  end
end
