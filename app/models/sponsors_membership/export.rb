# frozen_string_literal: true

class SponsorsMembership::Export
  attr_reader :filter

  def initialize(filter: :all)
    @filter = filter
  end

  def as_csv
    GitHub::CSV.generate(encoding: Encoding::UTF_8) do |csv|
      # set the headers for the csv
      csv << [
        "state",
        "login",
        "name",
        "email",
        "created_at",
      ]

      memberships.each do |membership|
        user_id = membership.sponsorable_id
        email = membership.sponsorable.user? ? emails[user_id] : membership.sponsorable.billing_email

        csv << [
          membership.current_state.name,
          logins[user_id],
          names[user_id],
          email,
          membership.created_at,
        ]
      end
    end
  end

  def filename
    "sponsors-waitlist-#{filter.downcase}-#{Date.current}.csv"
  end

  private

  def memberships
    @memberships ||= begin
      scope = SponsorsMembership.all

      unless filter == :all
        state = SponsorsMembership.state_value(filter)
        scope = scope.where(state: state)
      end

      scope.includes(:sponsorable)
           .select(:state, :sponsorable_type, :sponsorable_id, :contact_email_id, :created_at)
           .order("sponsors_memberships.created_at ASC")
    end
  end

  def user_ids
    @user_ids ||= memberships.map(&:sponsorable_id)
  end

  def logins
    @logins ||= User.where(id: user_ids).pluck(:id, :login).to_h
  end

  def emails
    @emails ||= begin
      contact_email_ids = memberships.map(&:contact_email_id)
      emails = UserEmail.where(id: contact_email_ids).pluck(:user_id, :email).to_h
    end
  end

  def names
    @names ||= Profile.where(user_id: user_ids).pluck(:user_id, :name).to_h
  end
end
