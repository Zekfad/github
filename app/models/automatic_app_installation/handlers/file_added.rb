# frozen_string_literal: true

class AutomaticAppInstallation
  module Handlers
    class FileAdded < BaseHandler
      alias push originator

      def install_integration
        install_triggers.each do |install_trigger|
          #  If the file added event applies to this App...
          if self.class.should_install_on?(install_trigger, push)
            target_id = push.repository.owner.id
            integration_id = install_trigger.integration.id
            InstallAutomaticIntegrationsJob.perform_later(
              target_id,
              integration_id,
              install_trigger.id,
              Array(push.repository.id),
              {push_id: push.id, enqueued_timestamp: Time.now.to_i},
            )
          end
        end
      end

      def self.after_integration_installed(integration:, installation:, target:, installer:, repositories:, options: {})
        return unless (push_id = options[:push_id] || options["push_id"]).present?
        return unless push = Push.find_by_id(push_id)

        repositories.each do |repo|
          payload = {
            # only deliver event for the newly installed integration
            target_hook: integration.hook,

            repo: repo,
            pusher: push.pusher,
            ref: push.ref,
            before: push.before,
            after: push.after,
            triggered_at: Time.now,
          }

          event = Hook::Event::PushEvent.new(payload)
          delivery_system = Hook::DeliverySystem.new(event)

          delivery_system.generate_push_event_hookshot_payloads
          delivery_system.deliver_push_event_later
        end
      end

      def self.should_install_on?(install_trigger, push)
        return false unless push

        return false unless Apps::Internal.can_auto_install?(
          self,
          install_trigger.integration,
          repo: push.repository,
        )

        trigger_file_changed?(install_trigger, push)
      end

      def self.trigger_file_changed?(install_trigger, push)
        return false if push.changed_files.nil?

        pushed_file_paths = if push.initial_commit?
          # force the Push model to reload commits (prevents stale commits list)
          push.commits = nil

          push.commits_pushed.flat_map do |c|
            c.diff.map(&:path)
          end.uniq
        else
          push.changed_files(decompose_renames: true).map do |file|
            file.addition? ? file.path : nil
          end.compact
        end

        path_regex = Regexp.compile(install_trigger.path)
        pushed_file_paths.any? { |other| path_regex.match?(other) }
      end
    end
  end
end
