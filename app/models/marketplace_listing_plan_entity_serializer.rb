
# frozen_string_literal: true

module MarketplaceListingPlanEntitySerializer
  include PlatformHelper

  EntityQuery = parse_query <<-'GRAPHQL'
    fragment AnalyticsMarketplaceListingPlan on MarketplaceListingPlan {
      __typename
      databaseId
      name
      monthlyPriceInCents
      state
    }
  GRAPHQL

  def self.serialize(entity)
    case entity
    when PlatformTypes::MarketplaceListingPlan
      graphql_serialize(entity)
    else
      ar_serialize(entity)
    end
  end

  def self.ar_serialize(entity)
    # nb: plans have monthly and annual pricing. Serialization here
    # assumes monthly
    {
      plan_id: entity.id,
      plan_name: entity.name,
      plan_price_cents: entity.monthly_price_in_cents,
      renewal_frequency: "MONTHLY",
      current_state: entity.current_state.name,
    }
  end

  def self.graphql_serialize(entity)
    entity = EntityQuery::AnalyticsMarketplaceListingPlan.new(entity)
    {
      plan_id: entity.database_id,
      plan_name: entity.name,
      plan_price_cents: entity.monthly_price_in_cents,
      renewal_frequency: "MONTHLY",
      current_state: entity.state,
    }
  end
end
