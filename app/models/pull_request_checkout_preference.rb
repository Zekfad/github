# frozen_string_literal: true
require "set"

class PullRequestCheckoutPreference
  KEY_PREFIX = "user-pr-checkout-preference"
  ALLOWED_VALUES = Set.new(["desktop", "cli"])
  DESKTOP_PLATFORMS = Set.new(["Windows", "Macintosh"])
  DEFAULT_VALUE = "desktop"

  def initialize(user, platform)
    @user = user
    @platform = platform
    @store = GitHub.kv
  end

  def build_key
    sanitized_platform = case @platform
    when DESKTOP_PLATFORMS then @platform
    else "other"
    end
    "#{KEY_PREFIX}.#{@user.id}.#{sanitized_platform}"
  end

  # Public: Sets the pull request checkout preference for the user.
  #
  # type     - String method of checkout.
  #
  # Returns a Boolean indicating success
  def set(type)
    case type
    when allowed_values
      @store.set(build_key, type)
      true
    else
      false
    end
  end

  # Public: Gets the pull request checkout preference for the user.
  #
  # Returns a String method of checkout
  def get
    value = @store.get(build_key).value { nil }
    case value
    when allowed_values then value
    else DEFAULT_VALUE
    end
  end

  private

  # With the addition of codespaces, we are moving the "Check out locally"
  # dropdown which was previously on the review request banner onto the main PR
  # page on a button labeled "Open with". This will allow more visibility into
  # using GitHub Desktop and CLI apps as well as Codespaces. Users will only
  # see the option to open with a codespace if they are in the beta.
  def allowed_values
    allowed_values = ALLOWED_VALUES.dup
    if Codespaces::Policy.enabled_for_user?(@user)
      allowed_values.add("codespaces")
    end
    allowed_values
  end
end
