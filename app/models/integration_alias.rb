# frozen_string_literal: true

class IntegrationAlias < ApplicationRecord::Collab
  areas_of_responsibility :ecosystem_apps

  belongs_to :integration, required: true

  validates :integration_id, uniqueness: true
  validates :slug, presence: true, uniqueness: { case_sensitive: true }
end
