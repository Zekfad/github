# rubocop:disable Style/FrozenStringLiteralComment

class IssueComment < ApplicationRecord::Domain::Repositories
  include GitHub::UTF8
  include GitHub::UserContent
  include GitHub::Validations
  include GitHub::RateLimitedCreation
  include GitHub::CommentMetrics
  include GitHub::Relay::GlobalIdentification
  include EmailReceivable
  include NotificationsContent::WithCallbacks
  include PushNotifiable
  include Instrumentation::Model
  include Referrer
  include Reaction::Subject::RepositoryContext
  include Spam::Spammable
  include UserContentEditable
  include InteractionBanValidation
  include AuthorAssociable
  include GitHub::MinimizeComment
  include ReferenceableContent
  include Blockable
  include AbuseReportable

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::IssueComment

  belongs_to :user
  belongs_to :repository
  alias_method :entity, :repository
  alias_method :async_entity, :async_repository
  belongs_to :issue, touch: true
  belongs_to :performed_via_integration,
    foreign_key: :performed_by_integration_id,
    class_name: "Integration"
  has_many :workflow_runs, as: :trigger

  has_many :reactions, as: :subject

  setup_spammable(:user)

  before_validation :assign_repository_from_issue, on: :create

  validates_presence_of :issue_id
  validates_presence_of :body, message: "cannot be blank"
  validates :body, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT }, unicode: true
  validate :user_can_interact?, on: :create
  validate :ensure_issue_not_converted_to_discussion

  setup_attachments
  setup_referrer

  after_commit :subscribe_to_issue, on: :create

  after_commit :update_issue_comments_count, on: :create
  after_destroy :update_issue_comments_count

  before_destroy :generate_webhook_payload, unless: :spammy?
  before_destroy :generate_issue_event
  after_commit :queue_webhook_delivery, unless: :spammy?, on: :destroy

  after_commit :instrument_creation, on: :create
  after_commit :instrument_update, on: :update, if: :body_changed_after_commit?
  after_commit :instrument_destruction, on: :destroy

  # spam check on create handled by Newsies::DeliverNotificationsJob
  after_commit :enqueue_check_for_spam, on: :update,  if: :check_for_spam?

  after_commit :measure_comment_metrics, on: :create
  after_commit :synchronize_search_index, on: :update, if: :body_changed_after_commit?
  after_commit :notify_socket_subscribers, on: :update, if: :body_changed_after_commit?

  after_commit :process_content_references, on: [:create, :update], if: :has_content_references?

  after_commit :subscribe_and_notify,            on: :create
  after_commit :update_subscriptions_and_notify, on: :update

  after_commit :update_pull_request_timestamp, on: :update

  validate :validate_comment_is_authorized
  validate :ensure_creator_is_not_blocked, unless: :comment_hidden_changed?

  scope :public_scope,       -> { joins(:repository).where("repositories.public = ?", true) }

  scope :since, -> (time) {
    where("issue_comments.updated_at >= ?", time)
  }

  scope :sorted_by, -> (order_str, direction) {
    opts = { order: "issue_comments.created_at DESC"}

    if !order_str.blank?
      case order_str
      when /created/
        opts[:order] = "issue_comments.created_at"
      when /updated/
        opts[:order] = "issue_comments.updated_at"
      else
        opts[:order] = "issue_comments.created_at"
      end

      opts[:order] << (direction == "asc" ? " ASC" : " DESC")
    end

    order(opts[:order])
  }

  scope :with_issue, -> { joins(:issue) }

  scope :with_pull_request,    -> { joins(:issue).where("issues.pull_request_id IS NOT NULL") }
  scope :without_pull_request, -> { joins(:issue).where("issues.pull_request_id IS NULL") }

  scope :for_organization, ->(organization) { joins(:repository).where(repositories: {organization_id: organization}) }

  enum comment_hidden_by: GitHub::MinimizeComment::ROLES

  scope :excluding_organization_ids, -> (ids) do
    if ids.any?
      joins(:repository).
        where("repositories.organization_id IS NULL OR repositories.organization_id NOT IN (?)", ids)
    else
      scoped
    end
  end

  def notifications_author
    user
  end

  def notifications_thread
    issue
  end

  def async_notifications_list
    async_repository
  end

  def body
    utf8(read_attribute(:body))
  end

  # For quickly fixing those users that have not mastered pasting code into markdown
  def make_pre_block
    update_attribute(:body, body.gsub(/^(.+)$/, '    \1'))
  end

  def readable_by?(user)
    async_readable_by?(user).sync
  end

  def async_readable_by?(user)
    async_issue.then do |issue|
      next false unless issue
      issue.async_readable_by?(user)
    end
  end

  def async_viewer_can_delete?(viewer)
    return Promise.resolve(false) unless viewer
    return Promise.resolve(true) if viewer.site_admin?

    async_viewer_cannot_delete_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_delete_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    Promise.all([async_issue, async_repository, async_user]).then do |issue, repository, user|
      Promise.all([issue.async_conversation, issue.async_pull_request, issue.async_repository, repository.async_owner]).then do
        context = { repo: repository, issue: issue }
        errors = ContentAuthorizer.authorize(viewer, :IssueComment, :delete, context).errors.map(&:symbolic_error_code)

        async_deletable_by?(viewer).then do |deletable_by_viewer|
          errors << :insufficient_access unless deletable_by_viewer
          errors
        end
      end
    end
  end

  def async_deletable_by?(viewer)
    return Promise.resolve(false) unless viewer

    async_repository.then do |repository|
      next false unless repository

      async_user.then do |user|
        # Deleting comments made by ghost users is allowed, but not editing
        user ||= User.ghost
        next true if user == viewer

        repository.async_writable_by?(viewer)
      end
    end
  end

  def async_viewer_can_update?(viewer)
    async_viewer_cannot_update_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_update_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    Promise.all([async_issue, async_repository, async_user]).then do |issue, repository, user|
      Promise.all([issue.async_conversation, issue.async_pull_request, issue.async_repository, repository.async_owner]).then do
        context = { repo: repository, issue: issue }
        errors = ContentAuthorizer.authorize(viewer, :IssueComment, :edit, context).errors.map(&:symbolic_error_code)

        async_editable_by?(viewer).then do |editable_by_viewer|
          errors << :insufficient_access unless editable_by_viewer
          errors
        end
      end
    end
  end

  # TODO: The logic in this method should probably be moved into the `ContentAuthorizer` framework,
  #       but that is not async-aware yet, so this is the best place for this right now.
  #
  #       See `PullRequestReviewComment#async_editable_by?` for a similar check on `PullRequestReviewComment`s.
  def async_editable_by?(viewer)
    return Promise.resolve(false) unless viewer

    async_repository.then do |repository|
      next false unless repository

      async_user.then do |user|
        next false unless user

        repository.async_owner.then do |owner|
          Promise.all([
            user.async_blocked_by?(owner),
            user.async_blocked_by?(viewer),
            viewer.async_blocked_by?(user),
          ]).then do |user_blocked_by_owner, user_blocked_by_viewer, viewer_blocked_by_user|
            next false if user_blocked_by_owner || user_blocked_by_viewer || viewer_blocked_by_user
            next true if user == viewer

            repository.async_writable_by?(viewer)
          end
        end
      end
    end
  end

  def async_minimizable_by?(actor)
    return Promise.resolve(false) unless actor.present?
    return Promise.resolve(true) if actor.site_admin?

    # Users can always minimize their own comments, unless they're restricted
    # by internal App policy...
    return Promise.resolve(true) if unrestricted_actor_minimizing_own_comment?(actor)

    Promise.all([async_issue, async_repository]).then do |issue, repository|
      next false unless repository

      issue.async_pull_request.then do |pull|
        if pull.present?
          repository.resources.pull_requests.writable_by?(actor)
        else
          repository.resources.issues.writable_by?(actor)
        end
      end
    end
  end

  def async_unminimizable_by?(actor)
    return Promise.resolve(false) unless actor.present?
    return Promise.resolve(true) if actor.site_admin?

    async_repository.then do |repo|
      next false unless repo

      repo.async_pushable_by?(actor).then do |is_pushable|
        next is_pushable if minimized_by_maintainer?
        next false unless minimized_by_author?

        is_pushable || actor.id == user_id
      end
    end
  end


  # Absolute permalink URL for this issue comment.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                issue_comment.permalink(include_host: false) => `/github/github/issue/1#issuecomment-2`
  #
  def permalink(include_host: true)
    "#{issue.permalink(include_host: include_host)}##{anchor}"
  end
  alias url permalink

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_issue.then(&:async_path_uri).then do |path_uri|
      path_uri = path_uri.dup
      path_uri.fragment = anchor
      path_uri
    end
  end

  def anchor
    "issuecomment-#{id}"
  end

  # Unique identifier for this issue comment. Used in email messages.
  def message_id
    "<#{repository.name_with_owner}/issues/#{issue.number}/#{id}@#{GitHub.urls.host_name}>"
  end

  # Fallback on the ghost user when the original author's been deleted.
  # See User.ghost for more.
  def safe_user
    user || User.ghost
  end

  def subscribe_mentioned(mentions = mentioned_users, author = user)
    issue.subscribe_mentioned(mentions, author)
  end

  # Internal: Filters out users that should keep a subscription to this thread.
  # This should be called after a comment has been edited, with a mentioned
  # user removed due to a typo.  Remove anyone that hasn't commented already.
  #
  # users - Array of Users.
  #
  # Returns an Array of Users that can be unsubscribed.
  def unsubscribable_users(users)
    issue.unsubscribable_users(users)
  end

  # See IssueTimeline
  def timeline_sort_by
    [created_at]
  end

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the safe_user which we know exists.
  def modifying_user
    if GitHub.context[:from] && GitHub.context[:from].include?("stafftools") && GitHub.guard_audit_log_staff_actor?
      @modifying_user = User.staff_user
    else
      @modifying_user ||= (User.find_by_id(GitHub.context[:actor_id]) || safe_user)
    end
    @modifying_user
  end

  def last_modified_at
    @last_modified_at ||= last_modified_with :user
  end

  # Collect conditions under which we don't bother checking the content
  # of this Issue for spam.
  def skip_spam_check?
    repository.private?      ||   # Leave private repos alone.
    repository.member?(user) ||   # Don't bother repo members.
    user.nil?                ||
    !user.can_be_flagged?    ||   # No point in checking these,
    user.spammy?                  # since nothing will change.
  end

  def check_for_spam?
    persisted? && !skip_spam_check?
  end

  # Public: Checks spamminess of associated issue.
  #
  # Returns a Boolean.
  def belongs_to_spammy_content?
    issue&.spammy?
  end

  # Check the comment for spam
  #
  # options - currently unused
  def check_for_spam(options = {})
    return if skip_spam_check?

    if reason = GitHub::SpamChecker.test_comment(self)
      base_msg = "IssueComment spam (%s): %s" % [permalink, reason]

      if !user.can_be_flagged?
        GitHub::SpamChecker.notify("Regular checks said [%s], but user %s (%d) is NEVER SPAMMY." %
                                     [base_msg, user.login, user.id])
      elsif GitHub::SpamChecker.fairly_active?(user)
        GlobalInstrumenter.instrument(
          "add_account_to_spamurai_queue",
          {
            account_global_relay_id: user.global_relay_id,
            additional_context: "RESQUE_CHECK_FOR_SPAM_ISSUE_COMMENT",
            origin: :RESQUE_CHECK_FOR_SPAM_ISSUE_COMMENT,
            queue_global_relay_id: SpamQueue::POSSIBLE_SPAMMER_QUEUE_GLOBAL_RELAY_ID,
          },
        )
        GitHub.dogstats.increment("spam.active_user_review")
        GitHub::SpamChecker.notify("[%s], but user %s (%d) seems fairly active, so queuing for review." %
                                     [base_msg, user.login, user.id])
      else
        user.safer_mark_as_spammy(reason: base_msg)
        GitHub.dogstats.increment "spam.flagged", tags: ["spam_target:issue_comment"]
      end
    end
  end

  # Public: Issues mentioned in the body as being a duplicate of this one.
  #
  # The GitHub::HTML::IssueMentionFilter must be part of the body pipeline for this
  # information to be extracted.
  #
  # Returns an array of Issue objects.
  def duplicate_issues
    Array(body_result.issues).select { |ref| ref.duplicate? }.map(&:issue).uniq
  end

  def notify_socket_subscribers
    data = {
      timestamp: Time.now.to_i,
      wait: default_live_updates_wait,
      reason: "issue comment ##{id} updated",
      gid: global_relay_id,
    }

    if issue.pull_request?
      channel = GitHub::WebSocket::Channels.pull_request_timeline(issue.pull_request)
      GitHub::WebSocket.notify_pull_request_channel(issue.pull_request, channel, data)
    else
      channel = GitHub::WebSocket::Channels.issue_timeline(issue)
      GitHub::WebSocket.notify_issue_channel(issue, channel, data)
    end
  end

  # Overrides PushNotifiable#push_notification_subtitle
  def push_notification_subtitle
    issue.push_notification_subtitle
  end

private

  def update_issue_comments_count
    return if issue.nil?
    issue.update_issue_comments_count
  end

  # Internal: Callback method executed after every issue comment creation, edit,
  # and deletion to update the pull request's updated_at column.
  def update_pull_request_timestamp
    if issue&.pull_request?
      issue.pull_request&.touch
    end
  end

  def validate_comment_is_authorized
    operation = new_record? ? :create : :update
    authorization = ContentAuthorizer.authorize(modifying_user, :issue_comment, operation,
                                                repo: repository,
                                                issue: issue)

    if authorization.failed?
      errors.add(:base, authorization.error_messages)
    end
  end

  def ensure_creator_is_not_blocked
    return true if issue.repository.pushable_by?(user)
    if user && issue && user.blocked_by?([issue.repository.owner, issue.user])
      errors.add(:user, "is blocked")
    end
  end

  def subscribe_to_issue
    issue.subscribe(user, :comment)
  end

  # Internal: overrides the Referrer to be the issue.
  alias_method :referrer, :issue

  def assign_repository_from_issue
    self.repository = issue.try(:repository)
  end

  # Public: Gets the RollupSummary for this Comment's thread.
  # See Summarizable.
  #
  # Returns Newsies::Response instance.
  def get_notification_summary
    if issue
      list = Newsies::List.new("Repository", repository_id)
      GitHub.newsies.web.find_rollup_summary_by_thread(list, issue)
    end
  end

  def event_payload
    payload = {
      repo:          repository,
      issue:         issue,
      issue_comment: self,
      spammy:        spammy?,
      body:          body,
      allowed:       allowed?,
    }

    if repository.try(:in_organization?)
      payload[:org] = repository.organization
    end

    payload
  end

  def allowed?
    repository.permit?(user, :write)
  end

  def instrument_creation
    instrument :create

    GlobalInstrumenter.instrument "issue_comment.create", {
      actor: user,
      issue: issue,
      issue_creator: issue.user,
      issue_comment: self,
      repository: repository,
      repository_owner: repository.owner,
    }
  end

  def instrument_update
    previous_body = previous_changes[:body].try(:first)
    instrument :update, old_body: previous_body, private_repo: repository.private?

    GlobalInstrumenter.instrument "issue_comment.update", {
      actor: user,
      issue: issue,
      issue_creator: issue.user,
      issue_comment: self,
      previous_body: previous_body,
      repository: repository,
      repository_owner: repository.owner,
    }
  end

  def instrument_destruction
    instrument :destroy,
      author_id:  user.id,
      author: user.login
  end

  # Was the comment body changed in previous_changes? We use this to determine
  # if body changes _only_ after_commit.
  def body_changed_after_commit?
    previous_changes.key?(:body)
  end

  # Tell the owning issue to kick off a search index update
  def synchronize_search_index
    issue.synchronize_search_index
  end

  # Private: Serializes this IssueComment as a webhook payload for any apps
  # that listen for Hook::Event::IssueCommentEvents. Under normal circumstances
  # we deliver webhook events using instrumentation, but this must be called as
  # a before_destroy and uses Hook::Event#generate_payload_and_deliver_later to
  # serialize the webhook payload before the record becomes unavailable.
  def generate_webhook_payload
    if modifying_user&.spammy?
      @delivery_system = nil
      return
    end

    event = Hook::Event::IssueCommentEvent.new issue_comment_id: self.id, action: :deleted, triggered_at: Time.now
    @delivery_system = Hook::DeliverySystem.new(event)
    @delivery_system.generate_hookshot_payloads
  end

  def generate_issue_event
    if issue && modifying_user != user
      issue.events.create \
        event: "comment_deleted",
        actor: modifying_user,
        subject: user
    end
  end

  # Private: Queue the payloads generated above for delivery to Hookshot.
  def queue_webhook_delivery
    raise "`generate_webhook_payload' must be called before `queue_webhook_delivery'" unless defined?(@delivery_system)

    @delivery_system&.deliver_later
  end

  # Internal.
  def max_textile_id
    GitHub.max_textile_issue_comment_id
  end

  def ensure_issue_not_converted_to_discussion
    return unless issue && repository
    return unless repository.discussions_enabled?

    if issue.discussion
      errors.add(:base, "Cannot be modified since the issue has been converted to a discussion.")
    end
  end
end
