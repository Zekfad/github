# frozen_string_literal: true

class SearchIndexConfiguration < ApplicationRecord::Mysql5
  alias_attribute :readable, :is_readable
  alias_attribute :writable, :is_writable
  alias_attribute :primary,  :is_primary
  alias_attribute :name,     :index_type
  alias_attribute :version,  :index_version

  validates_presence_of :fullname, :cluster, :index_type, :index_version
  validates :version_sha, length: { is: 40 }, allow_nil: true
  validate :fullname_is_in_sync

  before_validation :update_defaults_and_propagate_changes

  before_create  :create_index
  before_destroy :delete_index

  def update_defaults_and_propagate_changes
    self.version_sha ||= current_version_sha

    local_slicer = slicer
    if fullname_changed?
      self.index_type    = local_slicer.index
      self.index_version = local_slicer.index_version.to_i
      self.slice_name    = local_slicer.slice
      self.slice_version = local_slicer.slice_version.to_i
    elsif index_name_parts_changed?
      local_slicer.index         = index_type    if index_type_changed?
      local_slicer.index_version = index_version if index_version_changed?
      local_slicer.slice         = slice_name    if slice_name_changed?
      local_slicer.slice_version = slice_version if slice_version_changed?
      self.fullname = local_slicer.fullname
    end
  end

  # Validates that the `fullname` and the various name parts - `index_type`,
  # `index_version`, `slice_name`, `slice_version` - are all in sync with one another
  # before committing this data to the database.
  def fullname_is_in_sync
    slicer = index_class.slicer

    slicer.index         = index_type
    slicer.index_version = index_version == 0 ? nil : index_version
    slicer.slice         = slice_name
    slicer.slice_version = slice_version == 0 ? nil : slice_version

    if fullname != slicer.fullname
      errors.add(:fullname, "not in sync with the index and slice components")
      return false
    end

    true
  end

  # Returns true if any of the name parts have changed.
  def index_name_parts_changed?
    index_type_changed? || index_version_changed? ||
    slice_name_changed? || slice_version_changed?
  end

  # Create the index on the search cluster if it does not already exist.
  #
  # Returns the response from the Elasticsearch cluster
  def create_index
    return if exists?

    # FIXME: this method call will also create an entry in the
    # `elastomer_index_memos` table - we'll need to revist this method call here
    # once that table has been removed
    Elastomer::SearchIndexManager.create_index \
        name:        fullname,
        cluster:     cluster,
        index_class: index_class,
        metadata: {
          read:    is_readable,
          write:   is_writable,
          primary: is_primary,
        }

  rescue Elastomer::Client::Error => err
    Failbot.report(err)
    errors.add(:base, "could not create the index: <#{err.class.name}> #{err.message}")
    return false
  end

  # Deletes the index from the Elasticsearch cluster.
  #
  # force - Boolean used to force deletion of the index even if it is the
  #         primary index
  #
  # Returns the response from the Elasticsearch cluster
  def delete_index(force: false)
    if primary? && !force
      errors.add(:base, "cannot delete the primary index")
      return false
    end
    return unless exists?

    Elastomer::SearchIndexManager.delete_index \
        name:        fullname,
        cluster:     cluster,
        force:       force,
        index_class: index_class

  rescue Elastomer::Client::Error => err
    Failbot.report(err)
    errors.add(:base, "could not delete the index: <#{err.class.name}> #{err.message}")
    return false
  end

  # Computes the current version SHA for the underlying `Elastomer::Index`
  # class based on the mappings and settings found in the index classs itself.
  # This version SHA is stored in the database, and it is used to determine if
  # the index on the search cluster is in sync with the data found here in the
  # source code.
  #
  # Returns a 40 character SHA1 String
  def current_version_sha
    @current_version_sha ||= Elastomer::SearchIndexManager.version \
        mappings:   index_class.mappings,
        settings:   index_class.settings,
        es_version: client.version
  end

  # Returns true if the index exists on the search cluster.
  def exists?
    response = client.head "/#{fullname}"
    response.success?
  end
  alias :exist? :exists?

  # Internal: Returns a Slicer instance configured with the the name of this
  # index.
  def slicer
    slicer = index_class.slicer
    slicer.fullname = fullname
    slicer
  end

  # Internal: Returns the Elastomer index class for this index config.
  def index_class
    @index_class ||= Elastomer.env.lookup_index(fullname)
  end

  # Internal: Returns the Elastomer::Client connected to the desired search
  # cluster.
  def client
    Elastomer.router.client(cluster)
  end

  # Internal: Returns the Elastomer::Index instance configured with the index
  # `fullname` and `cluster`.
  def index
    index_class.new(fullname, cluster)
  end

end
