# frozen_string_literal: true

module Configurable
  module AllowOrganizationCodespaces
    extend Configurable::Async

    KEY = "organization_codespaces.allowed"

    async_configurable :organization_codespaces_allowed?
    def organization_codespaces_allowed?
      config.enabled?(KEY)
    end

    # Allow organization codespaces by ensuring that the allowed key is present.
    def allow_organization_codespaces(force = false, actor:)
      changed = config.enable!(KEY, actor, force)
      return unless changed

      GitHub.dogstats.increment("organization_codespaces.allowed")
      GitHub.instrument(
        "organization_codespaces.allowed",
        instrumentation_payload(actor))
    end

    # Disallow organization codespaces by removing or disabling the allowed key.
    def disallow_organization_codespaces(force = false, actor:)
      changed = if force
        # to force a settings value, the setting value must exist
        config.disable!(KEY, actor, force)
      else
        config.delete(KEY, actor)
      end

      return unless changed

      GitHub.dogstats.increment("organization_codespaces.disallowed")
      GitHub.instrument(
        "organization_codespaces.disallowed",
        instrumentation_payload(actor))
    end

    # Clears the organization codespaces setting, removing any value from inheritance
    # hierarchy
    def clear_organization_codespaces_setting(actor:)
      changed = config.delete(KEY, actor)
      return unless changed

      GitHub.dogstats.increment("organization_codespaces.clear")
      GitHub.instrument(
        "organization_codespaces.clear",
        instrumentation_payload(actor))
    end

    # Returns whether the configuration entry has a policy enforcement
    def organization_codespaces_policy?
      !!config.final?(KEY)
    end

    private def instrumentation_payload(actor)
      payload = { user: actor }

      if self.is_a?(Organization)
        payload[:org] = self
        payload[:business] = self.business if self.business
      elsif self.is_a?(Business)
        payload[:business] = self
      end

      payload
    end
  end
end
