# rubocop:disable Style/FrozenStringLiteralComment

# Behavior that describes how Comments update the Newsies Summary data.
# Models should include this after all other callbacks.  This ensures that the
# notifications going out are for the finalized model.
module Summarizable
  # Provide subclasses a hook for preventing the delivery of notifications.
  # PullRequestReviewComments that belong to an open PullRequestReview should
  # not notify per comment, only when the review is submitted.
  #
  # Returns true if notifications are allowed to be sent for this model creation.
  def deliver_notifications?
    true
  end

  # Public: Should the update_notification_summary happen?
  #
  # See also PullRequestReviewComment#update_notification_summary?
  #
  # Returns Boolean-ish
  def update_notification_summary?
    related_repo_exists_when_defined
  end

  # If the model defines a `:repository` relation, but no `repository` can be found
  # on the instance, do not allow the update callback to run.
  #
  # This prevents methods later in the callback from dereferencing a `nil` and causing
  # exceptions when attempting to call `#repository.*` (e.g. `#repository.permalink`)
  #
  # Returns a Boolean that is false when a repository is expected, but not found
  def related_repo_exists_when_defined
    # If we don't have a repository relation, move on
    return true if self.class.reflect_on_association(:repository).nil?

    !repository.nil?
  end

  # Public: Gets the RollupSummary for this Comment's thread.
  #
  # Returns a RollupSummary.
  def get_notification_summary
    raise NotImplementedError
  end

  # Public: Updates the RollupSummary with the updated contents
  # of this Comment's body.
  #
  # Params:
  #   enqueue:   defaults to true, whether to enqueue a job that repeats
  #              the operation in case that Newsies is down.
  #
  # Returns a Boolean.
  def update_notification_summary(enqueue: true)
    get_response = get_notification_summary
    succeeded = get_response&.success?

    if succeeded && summary = get_response.value
      update_notification_rollup(summary)
      succeeded = Newsies::Response.new { summary.save }.success?
    end

    succeeded
  ensure
    if !succeeded && enqueue
      UpdateNotificationSummaryJob.perform_later(self.class.name, self.id, false, false)
    end
  end

  def update_notification_rollup(summary)
    suffix = summarizable_changed?(:body) ? :changed : :unchanged
    GitHub.dogstats.increment("newsies.rollup", tags: ["type:#{suffix}"])

    summary.summarize_comment(self)
  end

  # Public: Destroys the summarized content in this thread's
  # RollupSummary.
  #
  # Returns nothing.
  def destroy_notification_summary
    get_response = get_notification_summary
    if get_response && get_response.success?
      return unless summary = get_response.value
      summary.delete_item(self)
      Newsies::Response.new { summary.save! }
    end
  end

  # Public: Triggers the job to deliver notifications of this Comment after
  # creation.
  #
  # direct_mention_user_ids - Array<Integer> for IDs of users who were mentioned
  #                           individually in the notification content.
  # event_time              - Time that this notification was created. This is typically the `created_at` time
  #                           for notifications on creation, or `updated_at` if notifying about updates.
  #
  # Returns False if delivery did NOT happen
  # Returns True  if the delivery job was kicked off
  def deliver_notifications(direct_mention_user_ids: nil, event_time: nil)
    GitHub.newsies.trigger(
      self,
      direct_mention_user_ids: direct_mention_user_ids,
      event_time: event_time || created_at,
    )
    true
  end

  # Saves changed attributes in a `before_update` callback, so that `after_commit`
  # callbacks can access it.
  def get_changed_attributes_for_summary
    @summarizable_changes = Set.new(changed)
  end

  # Public: Returns true if any of the given attributes were changed.  Only
  # usable when the Summarizable record is being saved.  Otherwise, use the
  # core ActiveRecord dirty attributes API (#changed).
  def summarizable_changed?(*attrs)
    return unless @summarizable_changes
    attrs.any? { |a| @summarizable_changes.include?(a.to_s) }
  end
end
