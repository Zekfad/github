# rubocop:disable Style/FrozenStringLiteralComment

module Organization::AbilityDependency
  extend ActiveSupport::Concern

  include Ability::Actor
  include Ability::Subject
  include Ability::Membership

  DEPENDENT_UPDATE_LIMIT = Ability::BATCH_SIZE

  # Thrown when someone tries to do something with a non-member of an org when
  # org membership is required.
  class DirectMemberRequiredError < StandardError
    def initialize(organization, user)
      super "#{user.login} isn't a direct member of the #{organization.login} org"
    end
  end

  def permit?(actor, action)
    async_permit?(actor, action).sync
  end

  def async_permit?(actor, action)
    actor = actor.ability_delegate

    return Promise.resolve(false) if !actor.is_a?(User) || actor.new_record?

    super
  end

  def billing_manageable_by?(user)
    adminable_by?(user) || billing_manager?(user)
  end

  # Public: Is this user *only* a billing manager
  # of the organization, having no further
  # relations with the organization?
  #
  # Returns a Boolean.
  def billing_manager_only?(user)
    !direct_or_team_member?(user) && billing_manager?(user)
  end

  def dependents
    dependent_repos = Repository.owned_by_org(self).select(:id).all
    dependent_teams = teams.select(:id).all
    dependent_projects = projects.select(:id).all

    dependent_repos + dependent_teams + dependent_projects
  end

  # Internal: A hash of dependent types and corresponding ids
  # Used by Ability::Grant#update as an alternative to #dependents to increase
  # performance for organizations with many repositories
  def dependent_ids_by_type
    {
      "Team" => teams.pluck(:id),
      "Repository" => Repository.owned_by_org(self).pluck(:id),
      "Project" => projects.pluck(:id),
    }
  end

  # Public: The number of dependents owned by this organization
  def dependents_count
    dependent_ids_by_type.reduce(0) do |count, (_, ids)|
      count = count + ids.size
      count
    end
  end

  # Public: The number of dependents that can safely be updated within a
  # request/response timeout limit
  def dependent_update_limit
    DEPENDENT_UPDATE_LIMIT
  end

  def dependents?
    true
  end

  def dependent_added(dependent)
    super

    return unless dependent.is_a?(Repository)
    return if default_repository_permission == :none

    dependent.add_organization(self, action: default_repository_permission)
  end

  def dependent_removed(dependent)
    super

    return unless dependent.is_a?(Repository)

    dependent.remove_organization(self)
    dependent.remove_inaccessible_forks_for(admins.map(&:id))
  end

  # Public: direct admin members of this organization
  def direct_admins(actor_ids: nil, limit: nil)
    members(action: :admin, actor_ids: actor_ids, limit: limit)
  end

  # Public: ids of the direct admins of this organization
  def direct_admin_ids(actor_ids: nil, limit: nil)
    member_ids(action: :admin, actor_ids: actor_ids, limit: limit)
  end

  # Public: direct members of this organization
  def direct_members
    members
  end

  # Public: ids of the direct members of this organization
  def direct_member_ids
    member_ids
  end

  # Public: Is user a direct member of the Organization?
  def direct_member?(user)
    member?(user)
  end

  # Public: Get all direct abilities for a user in an organization.
  def user_direct_abilities_for_organization_teams_and_repositories(user)
    repo_ids = Repository.where(organization_id: id).pluck(:id)
    team_ids = Team.where(organization_id: id).pluck(:id)

    abilities = Ability.where(actor_type: "User",
                              actor_id: user.id,
                              subject_type: "Organization",
                              subject_id: id,
                              priority: 1).to_a

    team_ids.each_slice(1000) do |ids|
      abilities.concat Ability.where(actor_type: "User",
                                     actor_id: user.id,
                                     subject_type: "Team",
                                     subject_id: ids,
                                     priority: 1)
    end

    repo_ids.each_slice(1000) do |ids|
      abilities.concat Ability.where(actor_type: "User",
                                   actor_id: user.id,
                                   subject_type: "Repository",
                                   subject_id: ids,
                                   priority: 1)
    end

    abilities
  end

  # Internal: Organizations are actors and subjects.
  def connector?
    true
  end

  # Internal: Only users can directly act on organizations.
  def grant?(actor, action)
    actor.is_a?(User) && actor.user?
  end

  # Public: Can a user set interaction limits in this organization?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_can_set_interaction_limits?(actor)
    return Promise.resolve(false) unless actor.present?

    async_adminable_by?(actor)
  end

  def can_set_interaction_limits?(actor)
    async_can_set_interaction_limits?(actor).sync
  end

  # Fetch all of the custom roles for the organization
  def custom_roles
    ::Role.custom_roles_for_org(self)
  end

  # The class name persisted as `target_type` when a UserRole is created
  # with this object as target.
  def user_role_target_type
    "Organization"
  end
end
