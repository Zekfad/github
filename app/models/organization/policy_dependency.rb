# frozen_string_literal: true

module Organization::PolicyDependency
  # Is the given user allowed to view the policy UI for this organization?
  #
  # user - A User
  #
  # Returns a Boolean.
  def policies_enabled_for?(user)
    return false unless GitHub.flipper[:policies].enabled?(self)
    return false unless user.present?
    return false if user.opted_out_of_policy_staff_ship?

    member_of_team_with_policies_enabled?(user)
  end

  private

  def member_of_team_with_policies_enabled?(user)
    @policy_user_cache ||= Hash.new do |hash, user|
      promises = teams_with_policies_enabled.map { |team| team.async_member?(user) }
      hash[user] = Promise.all(promises).sync.any?(true)
    end
    @policy_user_cache[user]
  end

  def teams_with_policies_enabled
    return @teams_with_policies_enabled if defined? @teams_with_policies_enabled

    actor_ids_by_class = FlipperFeature.find_by_name("policies")&.actor_ids_by_class || []
    team_ids = Hash[actor_ids_by_class].fetch(Team, [])
    @teams_with_policies_enabled = teams.with_ids(team_ids)
  end
end
