# frozen_string_literal: true

module Organization::SponsorsDependency
  extend ActiveSupport::Concern

  # Sponsorships created by orgs (as sponsors) are not eligible to be matched by GitHub
  def eligible_for_sponsorship_match?(sponsorable:)
    false
  end

  def sponsors_contact_email
    billing_email
  end

  def sponsors_update_email
    organization_profile&.sponsors_update_email
  end

  def fiscal_host?
    SponsorsMembership::FISCAL_HOST_GITHUB_ORGANIZATION.key?(login)
  end
end
