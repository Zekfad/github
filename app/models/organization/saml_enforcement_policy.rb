# frozen_string_literal: true
class Organization
  class SamlEnforcementPolicy
    attr_reader :organization, :user
    alias_attribute :target, :organization

    def initialize(organization:, user:)
      @organization = organization
      @user = user
    end

    def enforced?
      return @enforced if defined?(@enforced)
      @enforced = calculate_enforced
    end

    private

    def calculate_enforced
      return false unless @organization && @user
      return false unless @organization.saml_sso_enabled?
      return false unless @organization.member?(@user)
      return false if     @organization.user_is_outside_collaborator?(@user.id)
      return false unless @organization.external_identity_session_owner.saml_sso_enforced? ||
                          @organization.saml_provider.external_identities.exists?(user_id: @user.id)

      true
    end
  end
end
