# frozen_string_literal: true

module Organization::EnterpriseDependency
  # Internal: Return all user ids of organization members that are not also
  # members of the organization's business for other reasons, including
  # membership in another business organization, or direct membership to the business
  # as an admin or billing manager
  #
  # Returns a list of user ids that only exist in this business org
  def unique_business_member_ids
    return [] if GitHub.single_business_environment? || business.nil?

    outside_business_orgs = (business.organizations - [self])
    outside_business_member_ids = outside_business_orgs.each_with_object(Set.new) do |organization, ids|
      ids.merge(Organization::LicenseAttributer.new(organization).user_ids)
    end

    outside_business_member_ids.
      merge(business.owners.pluck(:id)).
      merge(business.billing_managers.pluck(:id))

    (Organization::LicenseAttributer.new(self).user_ids - outside_business_member_ids).to_a
  end
end
