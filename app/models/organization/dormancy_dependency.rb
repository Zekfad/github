# rubocop:disable Style/FrozenStringLiteralComment

module Organization::DormancyDependency
  # Org-specific overrides for the active account checks.
  # See app/models/user/dormancy_dependency.rb for more details

  def exempt_from_dormancy?(threshold = GitHub.dormancy_threshold)
    # On Enterprise orgs do not count against the paid seats, so dormancy never
    # needs to be checked for them
    return true if GitHub.enterprise?

    # Paying us earns an exemption
    return true if GitHub.billing_enabled? && plan.paid? && !disabled?

    # If any repos are active (have recent pushes), then the owner user is
    # active too.  We check this to ensure we don't disrupt an active repo that
    # has an otherwise dormant owner.
    return true if repositories.any? { |repo| !repo.dormant?(threshold) }

    # Check collaboration (having many members and at least one active repo)
    has_pushed_repo = repositories.any? { |repo| !repo.never_pushed_to? }
    return true if has_pushed_repo && people.size > 1

    false
  end

  def recently_active?(threshold = GitHub.dormancy_threshold)
    cutoff = Time.now - threshold

    # Creating an account is activity
    return true if created_at >= cutoff

    # Paying us (or attempting to) is activity
    last_bill = last_billing_transaction_time
    return true if last_bill && last_bill >= cutoff

    # Check for dashboard events, the most fundamental indicator of activity
    last_event = last_stratocaster_event_at
    return true if last_event && last_event >= cutoff

    # Finally, check the audit log for any non-failure events
    last_audit_event = last_audit_log_entry_time
    return true if last_audit_event && last_audit_event >= cutoff
  end

  def last_audit_log_entry_time
    query_params = [
      "(actor_id:#{id} OR (org_id:#{id} AND _exists_:org))",
      "-action:staff.*",
    ]
    options = {
      phrase: query_params.join(" "),
      org: self,
    }
    query = Audit::Driftwood::Query.new_stafftools_query(options)
    hits = begin
      query.execute
    rescue
      Search::Results.empty
    end

    if hits.any?
      hit = hits.first
      Time.at(hit["created_at"] / 1000)
    end
  end
end
