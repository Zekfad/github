# frozen_string_literal: true

module Organization::HovercardDependency
  extend ActiveSupport::Concern

  include UserHovercard::SubjectDefinition

  def user_hovercard_parent
  end

  included do
    define_user_hovercard_context :teams, ->(user, viewer, descendant_subjects:) do
      next unless member?(user)

      user_team_ids = user.teams.where(organization_id: id).pluck(:id)
      visible_teams = visible_teams_for(viewer).where(id: user_team_ids)
      next if visible_teams.empty?

      # Teams are related if they are related to one of the subjects
      related_team_ids = descendant_subjects.lazy.map { |s| s.try(:related_teams) }.detect(-> { [] }, &:present?)&.map(&:id)

      # Only show related teams that the viewer can actually see
      related_teams = visible_teams.where(id: related_team_ids)
      UserHovercard::Contexts::OrganizationTeams.new(user: user, related: related_teams, all: visible_teams, organization: self)
    end
  end
end
