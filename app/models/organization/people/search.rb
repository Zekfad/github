# frozen_string_literal: true

# A class to encapsulate logic around searching for members of an
# organization.
class Organization::People::Search
  LOGGING_NAMESPACE = "organization_people_search"

  # - query: Query. The parsed user input.
  #
  # - users: ActiveRecord::Relation. A scope of Users filtered based on the user
  # input.
  def initialize(query:, users:)
    @query = query
    @users = users
  end

  # Public: Search for members based on the query the user supplied. If there is
  # no query string, then just return org members matching the filter criteria.
  # If the org is a mega org, then use a hack to search for org members using
  # GitHub Search. Otherwise, use SQL LIKE as it's good enough.
  #
  # Return: ActiveRecord::Relation of Users.
  def call
    return org_members unless @query.searching?

    time "#{LOGGING_NAMESPACE}.overall" do
      if searching_mega_org? && feature_flagged?
        mega_org_search
      else
        regular_org_search
      end
    end
  end

  private

  def org_members
    @users.with_ids(org_member_ids)
  end

  def feature_flagged?
    GitHub.flipper[:mega_org_search].enabled?(@query.current_user)
      .tap do |flipped|
      log msg: "#{LOGGING_NAMESPACE}.mega_org.disabled" unless flipped
    end
  end

  def searching_mega_org?
    @query.searching? && @query.organization.mega_org?
  end

  def mega_org_search
    log msg: "#{LOGGING_NAMESPACE}.mega_org"
    time "#{LOGGING_NAMESPACE}.mega_org" do
      @users.with_ids(search_hit_user_ids & org_member_ids)
    end
  end

  def regular_org_search
    log msg: "#{LOGGING_NAMESPACE}.regular_org"
    time "#{LOGGING_NAMESPACE}.regular_org" do
      org_members
        .includes(:profile)
        .where(["users.login LIKE :query OR profiles.name LIKE :query", { query: "%#{@query.cleaned_query}%" }])
        .references(:profile)
    end
  end

  # This is a big ugly hack. For huge orgs, using SQL LIKE was not cutting it.
  # So for only our biggest orgs, search via GitHub Search and return only the
  # user_ids of the users who belong to the org in question and match the
  # search.
  #
  # The reason for this is so that we can then compare those ids against
  # org_member_ids from MySQL. This allows us bridge the gap between GitHub
  # Search results and our ActiveRecord scopes.
  def search_hit_user_ids
    page = 1
    has_hit_last_page = false
    ids = []

    until has_hit_last_page do
      search = Search::Queries::OrgMemberQuery.new(
        phrase: @query.cleaned_query,
        org: @query.organization,
        current_user: @query.current_user,
        page: page,
      ).execute

      page += 1
      has_hit_last_page = true unless search.next_page
    end

    ids.concat(
      search
      .results
      .flat_map { |result| result.dig("_source", "user_id") }
      .compact,
    )
  end

  def org_member_ids
    @query.organization.visible_user_ids_for(
      @query.current_user,
      type: visibility_level,
      limit: org_member_limit,
    )
  end

  def org_member_limit
    searching_mega_org? ? nil : Organization::MEGA_ORG_MEMBER_THRESHOLD
  end

  def visibility_level
    return :all unless  @query.role_scope?

    if @query.role == :owner
      :admin
    elsif @query.role == :direct_member
      :member_without_admin
    else
      :all
    end
  end

  def log(**kwargs)
    defaults = { ns: self.class, org: @query.organization.login }
    GitHub::Logger.log(defaults.merge(kwargs))
    yield if block_given?
  end

  def time(metric_name)
    GitHub.dogstats.time(metric_name) { yield }
  end
end
