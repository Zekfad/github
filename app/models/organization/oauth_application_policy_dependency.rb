# rubocop:disable Style/FrozenStringLiteralComment

module Organization::OauthApplicationPolicyDependency
  # OauthApplications that are approved or in the process of being approved by
  # this organization.
  def oauth_applications_requesting_approval(state: nil)
    state = case state
    when :pending
      # OAuth applications that are pending approval by an organization admin.
      OauthApplicationApproval.states[:pending_approval]
    when :approved
      # OAuth applications that have been approved by an organization admin to
      # access this organization's resources.
      OauthApplicationApproval.states[:approved]
    when :denied
      # OAuth applications that an organization admin has explicitly denied
      # permission to access this organization's resources.
      OauthApplicationApproval.states[:denied]
    else
      nil
    end

    scope = self.oauth_application_approvals
    scope = scope.where(state: state) unless state.nil?

    OauthApplication.with_ids(scope.pluck(:application_id)).distinct
  end

  # Public: Enables OAuth app restrictions for this org. This means tokens and
  # SSH keys created by these apps will have no access to org resources unless
  # explicitly approved by an owner.
  def enable_oauth_application_restrictions
    update_attribute :restrict_oauth_applications, true

    instrument :enable_oauth_app_restrictions
  end

  # Public: Disables the OAuth applications restrictions for this org.
  def disable_oauth_application_restrictions
    update_attribute :restrict_oauth_applications, false

    instrument :disable_oauth_app_restrictions
  end

  # Public: Does this org have an OAuth app whitelist?
  def restricts_oauth_applications?
    restrict_oauth_applications?
  end

  # Public: Request approval for an OAuth application to be able to access
  # organization resources.
  #
  # application - An OauthApplication to create an approval request for.
  # requestor   - The User requesting approval for the application.
  #
  # Returns nothing.
  def request_oauth_application_approval(application, requestor:)
    return if application.organization_policy_exempt?
    return if approval_pending_for_oauth_application?(application)
    return if allows_oauth_application?(application)
    return if approval_denied_for_oauth_application?(application)

    approval = oauth_application_approvals.create! application: application,
                                                   requestor: requestor

    instrument :oauth_app_access_requested,
      application_name: application.name,
      application_id: application.id

    GitHub.dogstats.increment "organization_oauth_approval", tags: ["action:requested"]

    OrganizationApplicationAccessRequestedJob.perform_later(requestor.id, approval.id)
  end

  # Public: Approve an OAuth application to be able to access this
  # organization's resources.
  #
  # application - An OauthApplication to approve.
  # approver    - The User approving the application.
  #
  # Returns the OauthApplicationApproval. (Note: this approval is returned as is
  # but maybe be modified asynchronously in a background job)
  def approve_oauth_application(application, approver:)
    return if allows_oauth_application?(application)

    approval = oauth_application_approvals.find_by_application_id(application.id)
    if approval
      approval.state = :approved
      approval.save!
    else
      approval =
        oauth_application_approvals.create! application: application,
                                            requestor: approver,
                                            state: :approved
    end

    instrument :oauth_app_access_approved,
      application_name: application.name,
      application_id: application.id

    GitHub.dogstats.increment "organization_oauth_approval", tags: ["action:approved"]

    OrganizationApplicationAccessApprovedJob.perform_later(approval.id, approver.id)

    approval
  end

  # Public: Deny an OAuth application access to this organization's resources.
  #
  # application - An OauthApplication to deny.
  #
  # Returns the OauthApplicationApproval. (Note: this approval is returned as is
  # but maybe be modified asynchronously in a background job)
  def deny_oauth_application(application)
    approval = oauth_application_approvals.find_by_application_id(application.id)
    return unless approval

    approval.state = :denied
    approval.save!

    RemoveRepoKeysForPolicymakerAndAppJob.perform_later(approval.organization_id, approval.application_id)

    instrument :oauth_app_access_denied,
      application_name: application.name,
      application_id: application.id

    GitHub.dogstats.increment "organization_oauth_approval", tags: ["action:denied"]

    approval
  end

  # Public: Indicates if an OauthApplication is trusted
  # by the org.
  def allows_oauth_application?(oauth_app)
    return true if oauth_app.organization_policy_exempt?
    return true unless restricts_oauth_applications?
    return true if oauth_app.owned_by?(self)

    oauth_application_approvals.
      approved.
      where(application_id: oauth_app.id).
      any?
  end

  # Public: Indicates if an OauthApplication is pending approval by the org.
  def approval_pending_for_oauth_application?(oauth_app)
    return false unless restricts_oauth_applications?

    oauth_application_approvals.pending_approval.exists?(application_id: oauth_app.id)
  end

  # Public: Has the organization explicitly denied a request for the given OAuth
  # application to access the organization's resources?
  #
  # oauth_app - The OauthApplication.
  #
  # Returns a Boolean.
  def approval_denied_for_oauth_application?(oauth_app)
    return false unless restricts_oauth_applications?

    oauth_application_approvals.denied.exists?(application_id: oauth_app.id)
  end

  def can_authenticate_via_oauth?
    false
  end

  def can_authenticate_via_basic_auth?
    false
  end

  def can_authenticate_via_username_password_basic_auth?
    false
  end
end
