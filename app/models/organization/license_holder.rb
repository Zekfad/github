# frozen_string_literal: true

class Organization
  class LicenseHolder
    def initialize(organization)
      @organization = organization
    end

    # The Initial admins are created in an `after_commit` so we need to explicitly account for them here to pick them up
    # in the case we're dealing with an organization during its creation.
    def users
      @_users ||= begin
        LicenseSourcer.invites(organization, organization.pending_non_manager_invited_user_ids) +
          LicenseSourcer.collaborator_invites(organization, organization.private_repo_invitee_ids) +
          LicenseSourcer.members(organization, organization.people_ids) +
          LicenseSourcer.collaborators(organization, organization.user_ids_with_private_repo_access) +
          LicenseSourcer.admins(organization, organization.admins.pluck(:id))
      end
    end

    def emails
      @_emails ||= begin
        LicenseSourcer.invites(organization, organization.pending_invited_non_user_emails) +
        LicenseSourcer.collaborator_invites(organization, organization.private_repo_non_user_invited_emails)
      end
    end

    private

    attr_reader :organization
  end
end
