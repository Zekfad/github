# rubocop:disable Style/FrozenStringLiteralComment

module Organization::BetaFeaturesDependency
  BETA_FEATURES = []
  ELIGIBLE_PLANS = [GitHub::Plan::BUSINESS_PLUS, GitHub::Plan::BUSINESS]

  class MissingBigFeatureError < StandardError; end

  def set_beta_features_for_plan
    return unless GitHub.organization_beta_enrollment_enabled?

    if plan_gets_beta_features?
      enable_beta_features
    end
  end

  private
  def plan_gets_beta_features?
    return true if GitHub.flipper[:trial_org_beta_feature_enrollment].enabled?(self) &&
      Billing::EnterpriseCloudTrial.new(self).active?

    return true if GitHub.flipper[:team_org_beta_feature_enrollment].enabled?(self) &&
      ELIGIBLE_PLANS.include?(self.plan.name)

    false
  end

  def enable_beta_features
    big_features = GitHub::Config::Flipper.big_features

    BETA_FEATURES.each do |feature|
      raise MissingBigFeatureError, "#{feature} is not defined as a big feature in Flipper config" unless big_features.include?(feature.to_s)

      GitHub.flipper[feature].enable_actor(self)
    end
  end
end
