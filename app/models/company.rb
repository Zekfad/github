# frozen_string_literal: true

class Company < ApplicationRecord::Domain::Users
  include GitHub::Validations
  extend GitHub::Encoding

  has_and_belongs_to_many :users

  validates :name, presence: true, uniqueness: { case_sensitive: true }

  force_utf8_encoding :name

  # Check if a given name is valid or not. Note that we allow existing company
  # names
  def self.valid_name?(name)
    company = Company.find_by_name(name)
    if !company
      company = Company.new(name: name)
    end

    company.valid?
  end
end
