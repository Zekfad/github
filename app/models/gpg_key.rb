# rubocop:disable Style/FrozenStringLiteralComment

class GpgKey < ApplicationRecord::Domain::Users
  belongs_to :user
  belongs_to :primary_key, class_name: "GpgKey"
  has_many :subkeys, class_name: "GpgKey", foreign_key: :primary_key_id, dependent: :destroy
  has_many :emails, class_name: "GpgKeyEmail", dependent: :destroy
  has_many :verified_user_emails, through: :emails
  has_many :user_emails, through: :emails

  scope :primary_keys, lambda { where("primary_key_id IS NULL") }
  scope :with_subkeys, lambda { includes(:subkeys) }
  scope :with_emails,  lambda {
    if GitHub.email_verification_enabled?
      includes(:verified_user_emails, primary_key: [:verified_user_emails])
    else
      includes(:user_emails, primary_key: [:user_emails])
    end
  }

  validates(:user_id,
    presence: true,
  )

  validates(:key_id,
    presence: true,
    uniqueness: {scope: :user_id, message: "already exists"},
    length: {is: 8},
  )

  validates(:public_key,
    presence: true,
    uniqueness: {scope: :user_id, message: "already exists"},
    length: {maximum: 100.kilobytes},
  )

  after_destroy_commit :instrument_deletion

  # Create a GpgKey, it's GpgKeyEmails, and subkeys from an armored GPG public
  # key.
  #
  # public_key - An ASCII armored GPG public key String.
  #
  # Returns a GpgKey instance.
  def self.create_from_armored_public_key(public_key)
    decoded = begin
      public_key = public_key.gsub(/^ +/, "").strip
      GitHub.gpg.decode_public_keys([public_key]).first
    rescue GpgVerify::Error => e
      key = all.new
      key.errors.add(:base, e.user_message)
      return key
    end

    primary_key = decoded["primary_key"]

    transaction do
      key = all.new(
        raw_key: public_key,
        public_key: primary_key["raw_data"],
        key_id: primary_key["fingerprint"].byteslice(-8..-1),
        expires_at: expires_at_value(primary_key["expiration_at"]),
        can_sign: primary_key["usage"]["signing"],
        can_encrypt_comms: primary_key["usage"]["encrypt_communications"],
        can_encrypt_storage: primary_key["usage"]["encrypt_storage"],
        can_certify: primary_key["usage"]["certification"],
      )

      # Deduplicate subkeys, taking the most recently created
      subkeys = decoded["subkeys"] || []
      by_fp = subkeys.group_by { |sk| sk["fingerprint"] }
      subkeys = by_fp.values.map do |sks|
        sks.sort_by! { |sk| expires_at_value(sk["created_at"]) }
        sks.last
      end

      if key.save
        key.create_emails(decoded["user_ids"])
        key.create_subkeys(subkeys)
        key.instrument_creation
      end

      key
    end
  end

  # Combine the keys into a single, armored keychain.
  #
  # Returns an ASCII armored String.
  def self.keychain
    missing_raw_key = []
    decode_error = []
    decoded = []

    find_each do |key|
      if key.raw_key.blank?
        missing_raw_key << key.hex_key_id
        next
      end

      begin
        decoded << PgpArmor.decode(key.raw_key).last
      rescue PgpArmor::ArmorError
        decode_error << key.hex_key_id
      end
    end

    headers = {}

    if missing_raw_key.none? && decode_error.none? && decoded.none?
      headers["Note"] = "This user hasn't uploaded any GPG keys."
    end

    if missing_raw_key.any?
      ids = missing_raw_key.join(", ")
      headers["Note"] = "The keys with the following IDs couldn't be exported and need to be reuploaded #{ids}"
    end

    if decode_error.any?
      ids = decode_error.join(", ")
      headers["Error"] = "The keys with the following IDs couldn't be parsed #{ids}"
    end

    # combine keys and re-armor
    PgpArmor.encode(decoded.join,
      block_type: "PGP PUBLIC KEY BLOCK",
      headers: headers,
    )
  end

  # Convert the expires_at from gpgverify to the value we'll store in the db.
  #
  # data - Nil or a [epoch, offset] Array.
  #
  # Returns a Time instance or nil.
  def self.expires_at_value(data)
    return if data.nil?

    # Borrowed from GitRPC: https://github.com/github/github/blob/2ce2dabc6dd010b3dffdd29bca66d9e7f6b8fba4/vendor/gitrpc/lib/gitrpc/util.rb#L125-L149
    t, offset = data
    time = Time.at(t)
    if time.utc_offset != offset
      if offset == 0
        time.utc
      else
        time.localtime(offset)
      end
    end

    time
  end

  # A hex representation of a key id.
  #
  # key_id - The binary String key id to hex encode.
  #
  # Returns a hex String.
  def self.hex_key_id(key_id)
    key_id.unpack("H*").first.upcase
  end

  # Check if this public key is flagged to allow encryption
  #
  # Returns a boolean
  def can_encrypt?
    can_encrypt_comms? || can_encrypt_storage?
  end

  # Check if the public key is expired.
  #
  # Returns boolean.
  def expired?
    expires_at && expires_at < Time.now
  end

  # Verify the signature over the message using this key.
  #
  # message            - String message that was signed.
  # signature          - Binary String signature.
  #
  # Returns boolean.
  def verify(message, signature)
    GitHub.gpg.verify(message, signature, public_key)
  end

  # A hex representation of the key's id.
  #
  # Returns a hex String.
  def hex_key_id
    self.class.hex_key_id(key_id)
  end

  # Is this email address in the public key and is it a verified email address
  # on the user's account?
  #
  # Returns boolean.
  def allowed_email?(email)
    email && allowed_emails.include?(email.downcase)
  end

  # Email addresses that are in the primary key and are verified on the user's
  # account.
  #
  # Returns an Array of downcased email address Strings.
  def allowed_emails
    @allowed_emails ||= begin
      if primary_key?
        emails = if GitHub.email_verification_enabled?
          verified_user_emails.pluck(:email).map(&:downcase)
        else
          user_emails.pluck(:email).map(&:downcase)
        end

        emails << legacy_stealth_email if legacy_stealth_email_exists?

        emails
      else
        primary_key.allowed_emails
      end
    end
  end

  def legacy_stealth_email_exists?
    emails.where(email: legacy_stealth_email).exists?
  end

  # The legacy-format stealth email address for this key's user.
  #
  # Returns a String.
  def legacy_stealth_email
    loaded_user = User.find(user_id)
    StealthEmail.new(loaded_user).legacy_email
  end

  # Is this a primary key, as opposed to a sub key?
  #
  # Returns boolean.
  def primary_key?
    primary_key_id.nil?
  end

  # Is this a sub key, as opposed to a primary key?
  #
  # Returns boolean.
  def subkey?
    !primary_key?
  end

  # Public: GPG keys are accessible to everyone.
  def readable_by?(_)
    true
  end

  # Create GpgKeyEmails for this key.
  #
  # identities - An Array of Hashes, containing an "email" key.
  #
  # Returns nothing.
  def create_emails(identities)
    return if identities.nil?

    addresses = identities.map { |uid| uid["email"] }.reject(&:blank?)
    return if addresses.empty?

    user_emails = Hash.new(GitHub::SQL::NULL)
    user.emails.where(email: addresses).each do |user_email|
      user_emails[user_email.email.downcase] = user_email.id
    end

    rows = addresses.map { |addr| [addr, user_emails[addr.downcase], self.id] }

    GpgKeyEmail.github_sql.run <<-SQL, rows: GitHub::SQL::ROWS(rows)
      INSERT INTO gpg_key_emails
      (email, user_email_id, gpg_key_id)
      VALUES :rows
    SQL
  end

  # Create subkeys for this primary key.
  #
  # subkeys - An Array of Hashes, containing "fingerprint" and "raw_data" keys.
  #
  # Returns nothing.
  def create_subkeys(subkeys)
    return if subkeys.nil? || subkeys.empty?

    now = Time.now

    rows = subkeys.map do |subkey|
      expires_at = self.class.expires_at_value(subkey["expires_at"])
      expires_at ||= GitHub::SQL::NULL
      usage = subkey["usage"]

      [
        self.user_id,
        GitHub::SQL::BINARY(subkey["fingerprint"].byteslice(-8..-1)),
        GitHub::SQL::BINARY(subkey["raw_data"]),
        now,
        now,
        self.id,
        expires_at,
        usage["signing"],
        usage["encrypt_communications"],
        usage["encrypt_storage"],
        usage["certification"],
      ]
    end

    github_sql.run <<-SQL, rows: GitHub::SQL::ROWS(rows)
      INSERT INTO gpg_keys
      (user_id, key_id, public_key, created_at, updated_at, primary_key_id, expires_at, can_sign, can_encrypt_comms, can_encrypt_storage, can_certify)
      VALUES :rows
    SQL
  end

  include Instrumentation::Model

  # Prefix for audit log events.
  #
  # Returns a Symbol.
  def event_prefix
    :gpg_key
  end

  # Data about this key for instrumentation.
  #
  # Returns a Hash.
  def event_payload
    {user: user, key_id: hex_key_id}
  end

  # Instrument that this key was created in the audit log.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_creation(payload = {})
    if primary_key?
      GitHub.dogstats.increment("gpg_key", tags: ["action:create"])
      instrument(:create, payload.merge(
        subkeys: subkeys.map(&:hex_key_id),
        emails: emails.map(&:email),
        expires_at: expires_at,
        can_sign: can_sign,
        can_encrypt_comms: can_encrypt_comms,
        can_encrypt_storage: can_encrypt_storage,
        can_certify: can_certify,
      ))
    end
  end

  # Instrument that this key was deleted in the audit log.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_deletion(payload = {})
    if primary_key?
      GitHub.dogstats.increment("gpg_key", tags: ["action:destroy"])
      instrument(:destroy)
    end
  end
end
