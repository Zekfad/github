# frozen_string_literal: true

# This module implements the methods expected to be present for an object to
# act as both a Newsies/notification "comment" and a "thread." An advisory
# credit fulfills both of those roles.
module AdvisoryCredit::NewsiesAdapter
  extend ActiveSupport::Concern

  def async_notifications_list
    async_repository_advisory.then do |repository_advisory|
      repository_advisory&.async_repository
    end
  end

  alias_method :async_entity, :async_notifications_list

  def notifications_list
    async_notifications_list.sync
  end

  alias_method :entity, :notifications_list

  def async_notifications_thread
    Promise.resolve(self)
  end

  def notifications_thread
    async_notifications_thread.sync
  end

  def async_notifications_author
    async_creator
  end

  def notifications_author
    async_notifications_author.sync
  end

  def async_permalink(**args)
    async_repository_advisory.then do |repository_advisory|
      repository_advisory&.permalink(**args)
    end
  end

  def permalink(**args)
    async_permalink(**args).sync
  end

  def message_id
    "<advisory-credits/#{id}@#{GitHub.urls.host_name}>"
  end

  def get_notification_summary
    GitHub.newsies.web.find_rollup_summary_by_thread(notifications_list, notifications_thread)
  end

  def notification_summary_title
    "Accept credit for contributing to a Security Advisory"
  end

  def notification_summary_body
    if creator
      "#{creator} gave you credit for your contributions to #{ghsa_id}."
    else
      "You were given credit for your contributions to #{ghsa_id}."
    end
  end

  # Send notification to the credited user when a credit is created.
  #
  # Note that this is controlled by RepositoryAdvisory#readable_by? which is
  # true when repository advisory is published _or_ when the credited user has
  # read access to the draft advisory (i.e. a collaborator on the advisory).
  #
  # The deliver_notifications method is also called after the parent repository
  # advisory is published. Newsies prevents duplicate notification deliveries.
  def deliver_notifications
    skip_notification = false

    with_lock do
      skip_notification = !deliver_notifications?
      next if skip_notification

      notified_at = Time.current

      GitHub.newsies.trigger(
        self,
        recipient_ids: [recipient.id],
        reason: :security_advisory_credit,
        event_time: notified_at,
      )

      update!(notified_at: notified_at)
    end

    instrument_event(:notify) unless skip_notification
  end

  def deliver_notifications?
    pending? &&
      !notified? &&
      recipient_id != creator_id &&
      !!recipient &&
      !!repository_advisory&.readable_by?(recipient)
  end
end
