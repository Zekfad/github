# frozen_string_literal: true

# Public: Used to move a Discussion to another Repository, updating
# all the necessary records to do so and recording the move via a
# DiscussionTransfer record.
class DiscussionRepositoryTransferrer
  BATCH_SIZE = 50

  # Public: Used to start the transfer process.
  #
  # discussion - the Discussion to move to a different repository
  # new_repository - the Repository to move the discussion to
  # actor - the User who is initiating the transfer
  #
  # Returns a DiscussionRepositoryTransferrer.
  def self.for_new_transfer(discussion, new_repository:, actor:)
    new(discussion: discussion, new_repository: new_repository, actor: actor)
  end

  # Public: Used to finish an already begun transfer of a discussion.
  #
  # discussion_transfer - the DiscussionTransfer record created when the transfer
  #                       was begun
  #
  # Returns a DiscussionRepositoryTransferrer.
  def self.for_continuing_transfer(discussion_transfer)
    new(discussion_transfer: discussion_transfer)
  end

  attr_reader :old_discussion, :error, :new_repository, :new_discussion,
    :discussion_transfer, :actor, :old_repository

  # discussion - the Discussion to move; required if `discussion_transfer` is nil
  # actor - the currently authenticated User; required if `discussion_transfer` is nil
  # new_repository - the Repository the discussion should be moved to; required
  #                  if `discussion_transfer` is nil
  # discussion_transfer - the DiscussionTransfer record for an already-in-progress
  #                       transfer; required if `new_repository` is nil
  def initialize(discussion: nil, actor: nil, new_repository: nil, discussion_transfer: nil)
    if new_repository.nil? && discussion_transfer.nil?
      raise "Either the new repository or an existing transfer record must be provided"
    end

    if discussion.nil? && discussion_transfer.nil?
      raise "Either the old discussion or an existing transfer record must be provided"
    end

    if discussion_transfer
      @old_discussion = discussion_transfer.old_discussion
      @old_repository = discussion_transfer.old_repository
      @actor = discussion_transfer.actor || User.ghost
      @discussion_transfer = discussion_transfer
      @new_discussion = discussion_transfer.new_discussion
      @new_repository = discussion_transfer.new_repository
    else
      @old_discussion = discussion
      @old_repository = discussion.repository
      @actor = actor
      @discussion_transfer = DiscussionTransfer.new(
        old_discussion: discussion,
        old_repository: discussion.repository,
        new_repository: new_repository,
        actor: actor,
      )
      @new_discussion = nil
      @new_repository = new_repository
    end

    @error = nil
  end

  # Public: Start the transfer process, safe to run synchronously when the user
  # requests a discussion transfer. Will create a new Discussion in the new
  # repository and log that the transfer has started by making a new
  # DiscussionTransfer.
  #
  # Returns a Boolean indicating success.
  def start_transfer
    # Transfer has already begun
    return true if discussion_transfer.persisted?

    @new_discussion = Discussion.new(
      repository: new_repository,
      user: old_discussion.user,
      title: old_discussion.title,
      body: replace_bare_discussion_mentions(old_discussion.body.dup),
      comment_count: old_discussion.comment_count,
      state: :transferring,
      user_hidden: old_discussion.user_hidden,
      created_at: old_discussion.created_at,
      category_id: new_category_id,
      chosen_comment_id: old_discussion.chosen_comment_id,
    )

    unless @new_discussion.save
      @error = error_message("Failed to create a copy of the discussion in " \
        "#{new_repository.name_with_owner}:", @new_discussion)
      return false
    end

    discussion_transfer.new_discussion = @new_discussion

    unless discussion_transfer.save
      @error = error_message("Failed to record discussion transfer:", discussion_transfer)
      return false
    end

    true
  end

  # Public: Allow undoing a transfer that has not yet completed successfully.
  #
  # Returns a Boolean indicating success.
  def revert_started_transfer
    unless discussion_transfer.persisted?
      @error = "Cannot revert a transfer that has not started."
      return false
    end

    if discussion_transfer.done?
      @error = "Cannot revert a transfer that has already finished."
      return false
    end

    unless Discussion.exists?(old_discussion.id)
      @error = "Cannot revert a transfer where discussion in old repository has " \
        "already been deleted."
      return false
    end

    unless new_discussion.destroy
      @error = error_message("Could not delete partially transferred discussion " \
        "from #{new_repository.name_with_owner}:", new_discussion)
      return false
    end

    unless discussion_transfer.destroy
      @error = error_message("Could not delete transfer record:", discussion_transfer)
      return false
    end

    true
  end

  # Public: Finish an already started transfer. Should be run in a background job
  # since it may take some time to complete.
  #
  # staff_user - optional User to use when recording the audit log event
  #              for the transfer
  #
  # Returns a Boolean indicating success.
  def complete_transfer(staff_user = nil)
    unless discussion_transfer.persisted?
      @error = "Cannot complete a discussion transfer that hasn't started."
      return false
    end

    unless discussion_transfer.started?
      @error = "This discussion transfer has already finished."
      return false
    end

    unless old_discussion.can_transfer_to?(new_repository, actor: actor)
      @error = "#{actor} does not have permission to finish transferring this discussion."
      return false
    end

    move_events
    move_comments
    move_user_content_edits
    move_reactions
    copy_subscribers

    transfer_event = new_discussion.events.new(actor: actor, event_type: :transferred)
    unless transfer_event.save
      @error = error_message("Could not record discussion event about transfer:", transfer_event)
      discussion_transfer.errored!
      return false
    end

    new_discussion.state = old_discussion.state
    new_discussion.error_reason = old_discussion.error_reason
    new_discussion.issue_id = old_discussion.issue_id
    unless new_discussion.save
      @error = error_message("Could not save new discussion data:", new_discussion)
      discussion_transfer.errored!
      return false
    end

    discussion_transfer.instrument_transfer_event(staff_user)

    unless old_discussion.destroy
      @error = error_message("Could not delete discussion from old repository:", old_discussion)
      discussion_transfer.errored!
      return false
    end

    discussion_transfer.state = :done
    discussion_transfer.new_discussion_event = transfer_event

    unless discussion_transfer.save
      @error = error_message("Could not mark transfer as complete:", discussion_transfer)
      return false
    end

    new_discussion.synchronize_search_index

    true
  end

  private

  # Private: Get the ID of the new repository-equivalent category to the old discussion's
  # category, if such a category exists.
  #
  # Returns an integer or nil.
  def new_category_id
    old_category = old_discussion.category
    return unless old_category

    new_category = new_repository.discussion_categories.find_by_name(old_category.name)
    new_category&.id
  end

  def move_events
    old_discussion.events.in_batches(of: BATCH_SIZE) do |scope|
      ApplicationRecord::Domain::Discussions.throttle do
        scope.update_all(discussion_id: new_discussion.id, repository_id: new_repository.id)
      end
    end
  end

  def move_comments
    comments = old_discussion.comments

    comments.each do |comment|
      comment.throttle do
        comment.update_column(:body, replace_bare_discussion_mentions(comment.body.dup))
      end
    end

    comments.in_batches(of: BATCH_SIZE) do |scope|
      ApplicationRecord::Domain::Discussions.throttle do
        scope.update_all(discussion_id: new_discussion.id, repository_id: new_repository.id)
      end
    end
  end

  # Private: Replaces bare discussion references `#1` in discussion or comment body with
  # global repo discussion reference `user/project#num`.
  # Inspired by / borrowed from GitHub::HTML::IssueMentionFilter#replace_bare_discussion_mentions
  #
  # body - the body from the `old_discussion`; String or nil
  #
  # Returns a String or nil.
  def replace_bare_discussion_mentions(body)
    return body if body.nil?

    discussion_reference_text = /(?<=\s|^)(gh-|#)(\d+)\b/i

    body.gsub(discussion_reference_text) do |match|
      _pound, number = $1, $2.to_i

      if discussion = old_repository.discussions.find_by(number: number)
        # link to the old repository
        "#{old_repository.name_with_owner}##{number}"
      elsif transfer = DiscussionTransfer.find_from(
        repository: old_repository, number: number, attempts: 1
      )
        transferred_discussion = transfer.new_discussion
        if (transferred_repo = transferred_discussion.repository)
          "#{transferred_repo.name_with_owner}##{transferred_discussion.number}"
        else
          match
        end
      else
        match
      end
    end
  end

  def move_user_content_edits
    old_discussion.user_content_edits.in_batches(of: BATCH_SIZE) do |scope|
      ApplicationRecord::Domain::Discussions.throttle do
        scope.update_all(discussion_id: new_discussion.id)
      end
    end
  end

  def move_reactions
    old_discussion.reactions.in_batches(of: BATCH_SIZE) do |scope|
      ApplicationRecord::Domain::Discussions.throttle do
        scope.update_all(discussion_id: new_discussion.id)
      end
    end
  end

  def copy_subscribers
    # each_subscriber doesn't include ignored subscribers
    old_discussion.each_subscriber.each do |subscriber|
      ApplicationRecord::Domain::Notifications.throttle do
        new_discussion.subscribe(subscriber.user, subscriber.reason)
      end
    end

    users_ignoring_old_discussion.each do |user|
      ApplicationRecord::Domain::Notifications.throttle do
        new_discussion.ignore(user)
      end
    end
  end

  def users_ignoring_old_discussion
    User.where(id: old_discussion_subscriber_set.ignored).select(:id)
  end

  def old_discussion_subscriber_set
    GitHub.newsies.subscriber_set_for(list: old_repository, thread: old_discussion)
  end

  def error_message(prefix, record)
    "#{prefix} #{record.errors.full_messages.to_sentence}"
  end
end
