# frozen_string_literal: true

module TradeControls
  class EmailCompliance
    include Compliance
    include DomainFlagging

    def initialize(organization: nil, email:, **)
      @organization = organization
      @email = email.to_s
      @reason = :email
    end

    # We will remove the organization check once :email_compliance_refactor feature flag
    # has been enabled because we are now using this email compliance check for users only.
    # All organization email checks has been moved to other classes.
    def full_restriction_violation?
      if @organization
        @organization.charged_account? && sanctioned_country.present?
      else
        sanctioned_country.present?
      end
    end

    # We will remove this method once email_compliance_refactor feature flag has been enabled.
    # This is because we are now using this email compliance check for users only and we do not do tier_1 restriction
    # for users.
    # All organization email checks has been moved to other classes.
    def tier_1_restriction_violation?
      return unless @organization
      @organization.uncharged_account? && sanctioned_country.present?
    end

    def to_hydro
      {
        reason: reason,
        country: sanctioned_country.name,
        email: @email,
      }
    end

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(**)
      Context::Expander.expand(email: @email, reason: reason, country: sanctioned_country)
    end

    private

    def domain_field
      @email
    end

    def domain_field_type
      :email
    end
  end
end
