# frozen_string_literal: true

module TradeControls
  class WebsiteUrlCompliance
    include Compliance
    include DomainFlagging

    def initialize(organization:, website_url:, **)
      @organization = organization
      @website_url = website_url.to_s
      @reason = :website_url
    end

    def full_restriction_violation?
      if @organization
        @organization.charged_account? && sanctioned_country.present?
      else
        sanctioned_country.present?
      end
    end

    def tier_1_restriction_violation?
      return unless @organization
      @organization.uncharged_account? && sanctioned_country.present?
    end

    def to_hydro
      {
        reason: reason,
        country: sanctioned_country.name,
        website_url: @website_url,
      }
    end

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(**)
      Context::Expander.expand(website_url: @website_url, reason: reason, country: sanctioned_country)
    end

    private

    def domain_field
      @website_url
    end

    def domain_field_type
      :website_url
    end
  end
end
