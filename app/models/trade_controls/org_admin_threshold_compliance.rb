# frozen_string_literal: true

module TradeControls
  class OrgAdminThresholdCompliance
    BATCH_SIZE = 1000

    include Compliance

    def initialize(organization:, **)
      @organization = organization
      @admin_ids = @organization.admin_ids
      @reason = :organization_admin
    end

    def admins_count
      @admins_count ||= @admin_ids.size
    end

    def count_restricted_admins
      TradeControls::Restriction.count_any_restricted_ids(@admin_ids)
    end

    def trade_restricted_admins_count
      @trade_restricted_admins_count ||= count_restricted_admins
    end

    def current_threshold
      return 0 if (admins_count.zero? || trade_restricted_admins_count.zero?)

      @current_threshold ||= ((trade_restricted_admins_count.to_f / admins_count) * 100).round(2)
    end

    def violation?
      if GitHub.flipper[:org_compliance_refactor].enabled?(@organization)
        return false if @organization.has_any_pending_trade_restriction?
        full_restriction_violation? || tier_0_restriction_violation? || tier_1_restriction_violation?
      else
        if @organization.charged_account?
          current_threshold >= 50
        else
          return false unless GitHub.flipper[:org_admin_flagging].enabled?(@organization)
          current_threshold >= 25
        end
      end
    end

    def full_restriction_violation?
      @organization.charged_account? && current_threshold >= 50
    end

    def tier_1_restriction_violation?
      return false unless GitHub.flipper[:org_admin_flagging].enabled?(@organization)
      @organization.uncharged_account? && current_threshold >= 50
    end

    def tier_0_restriction_violation?
      return false unless GitHub.flipper[:org_admin_flagging].enabled?(@organization)
      @organization.uncharged_account? && current_threshold.between?(25.00, 49.99)
    end

    def minor_threshold_violation?
      @organization.charged_account? && current_threshold.between?(25.00, 49.99)
    end

    def to_hydro
      {
        reason: reason,
        percentage_of_trade_restricted_admins: current_threshold,
      }
    end

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(**)
      Context::Expander.expand(org_admin_threshold: current_threshold, reason: reason)
    end
  end
end
