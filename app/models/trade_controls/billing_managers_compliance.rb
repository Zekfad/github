# frozen_string_literal: true

module TradeControls
  class BillingManagersCompliance

    include Compliance

    def initialize(organization:, **)
      @organization = organization
      @billing_manager_ids = organization.billing_manager_ids
      @reason = :organization_billing_manager
    end

    def number_of_billing_managers
      @number_of_billing_managers ||= @billing_manager_ids.size
    end

    def count_restricted_managers
      TradeControls::Restriction.count_any_restricted_ids(@billing_manager_ids)
    end

    def number_of_trade_restricted_billing_managers
      @number_of_trade_restricted_billing_managers ||= count_restricted_managers
    end

    def current_threshold
      return 0 if (number_of_billing_managers.zero? || number_of_trade_restricted_billing_managers.zero?)

      @current_threshold ||= ((number_of_trade_restricted_billing_managers.to_f / number_of_billing_managers) * 100).round(2)
    end

    def violation?
      if GitHub.flipper[:org_compliance_refactor].enabled?(@organization)
        return false if @organization.has_any_pending_trade_restriction?
        full_restriction_violation? || tier_1_restriction_violation?
      else
        if @organization.charged_account?
          current_threshold >= 50
        else
          current_threshold >= 25
        end
      end
    end

    def full_restriction_violation?
      @organization.charged_account? && current_threshold >= 50
    end

    def tier_1_restriction_violation?
      @organization.uncharged_account? && current_threshold >= 25
    end

    def minor_threshold_violation?
      @organization.charged_account? && current_threshold.between?(25.00, 49.99)
    end

    def to_hydro
      {
        reason: reason,
        percentage_of_trade_restricted_billing_managers: current_threshold,
      }
    end

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(**)
      Context::Expander.expand(percentage_of_trade_restricted_billing_managers: current_threshold, reason: reason)
    end
  end
end
