# frozen_string_literal: true

module TradeControls
  class OrgProfileEmailCompliance
    include OrgEmailCompliance

    def initialize(organization:, **)
      @organization = organization
      @reason = :organization_profile_email
      @email = organization.profile_email
    end
  end
end
