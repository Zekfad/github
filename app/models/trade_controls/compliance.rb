# frozen_string_literal: true

module TradeControls
  module Compliance
    def self.for(**options)
      #TODO guard for mutual exclusion and minimum args

      if options[:actor].present? then ManualCompliance
      elsif options[:email].present? then EmailCompliance
      elsif options[:ip].present? then IpCompliance
      elsif options[:website_url].present? then WebsiteUrlCompliance
      elsif options[:check_billing_managers].present? then BillingManagersCompliance
      elsif options[:check_admin].present? then OrgAdminThresholdCompliance
      elsif options[:check_outside_collaborators].present? then OutsideCollaboratorsCompliance
      elsif options[:check_organization_members].present? then OrganizationMembersCompliance
      elsif options[:check_profile_email].present? then OrgProfileEmailCompliance
      elsif options[:check_billing_email].present? then OrgBillingEmailCompliance
      else NullCompliance
      end.new(**options)
    end

    attr_reader :reason

    def violation?
      false
    end

    # checks if an organization has passed a minor trade restriction threshold
    def minor_threshold_violation?
      false
    end

    # checks if organization has passed a tier 0 restriction threshold
    def tier_0_restriction_violation?
      false
    end

    # checks if organization has passed a tier 1 restriction threshold
    def tier_1_restriction_violation?
      false
    end

    # checks if organization has passed a full restriction threshold
    def full_restriction_violation?
      false
    end

    def restriction_tier
      if minor_threshold_violation?
        :pending
      elsif tier_1_restriction_violation?
        :tier_1
      elsif tier_0_restriction_violation?
        :tier_0
      elsif full_restriction_violation?
        :full
      else
        nil
      end
    end

    def to_hydro
      {}
    end

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(**)
      Context::Expander.expand to_hydro
    end

    def check_and_enforce!
      return unless violation? || minor_threshold_violation?
      case restriction_tier
      when :full
        @organization.trade_controls_restriction.enforce!(compliance: self)
      when :tier_1
        if GitHub.flipper[:automated_org_flagging_1_1].enabled?
          @organization.trade_controls_restriction.tier_1_enforce!(compliance: self)
        else
          @organization.trade_controls_restriction.partially_enforce!(compliance: self)
        end
      when :tier_0
        @organization.trade_controls_restriction.tier_0_enforce!(compliance: self)
      when :pending
        @organization.schedule_pending_enforcement(reason: self.reason, threshold: self.current_threshold)
      end
    end
  end
end
