# frozen_string_literal: true

module TradeControls
  class Restriction < ApplicationRecord::Collab
    include Workflow

    BATCH_SIZE = 1000.freeze
    self.inheritance_column = nil
    belongs_to :user

    enum type: {
      unrestricted: "unrestricted",
      full: "full", # Financial Restriction + Full Feature Restriction => For paying orgs
      # Free orgs
      partial: "partial", # Does what tier_1 does now
      tier_0: "tier_0", # Financial Restriction, no free feature restrictions
      tier_1: "tier_1" # Financial Restriction, FPR restricted
    }

    validates_presence_of :user, :type
    validates_uniqueness_of :user_id
    validates_inclusion_of :type, in: types.keys
    validates_exclusion_of :type, in: %w[partial tier_0 tier_1], unless: -> { user.organization? }

    scope :count_any_restricted_ids, ->(ids) { where(user_id: ids).where.not(type: "unrestricted").find_each(batch_size: BATCH_SIZE).count }

    def tcr_has_organization?
      user.organization?
    end

    workflow(:type) do
      state :unrestricted do
        event :enforce, transitions_to: :full
        event :partially_enforce, transitions_to: :partial, if: :tcr_has_organization?
        event :tier_0_enforce, transitions_to: :tier_0, if: :tcr_has_organization?
        event :tier_1_enforce, transitions_to: :tier_1, if: :tcr_has_organization?
      end

      state :full do
        event :override, transitions_to: :unrestricted
      end

      state :partial do
        event :override, transitions_to: :unrestricted
      end

      state :tier_0 do
        event :override, transitions_to: :unrestricted
        event :tier_1_upgrade, transitions_to: :tier_1
      end

      state :tier_1 do
        event :override, transitions_to: :unrestricted
        event :tier_0_downgrade, transitions_to: :tier_0
      end

      after_transition do |from, to, event, compliance:|
        instrument event,
          compliance: compliance,
          restriction_type: to,
          restriction_type_was: from
      end
    end

    def any?
      !unrestricted?
    end

    def tiered_restriction?
      tier_0? || tier_1?
    end

    def valid_events_with_subsequent_state
      current_state.events.map do |_, (event)|
        [event.transitions_to, event.name] if event.condition_applicable?(self)
      end.compact
    end

    def restriction_event(restriction_type)
      current_state.events.keys.delete(restriction_type.to_sym)
    end

    private

    def on_full_entry(new_state, event, compliance:)
      user.instrument_trade_controls_enforcement(compliance: compliance)
    end

    def on_partial_entry(new_state, event, compliance:)
      user.instrument_trade_controls_enforcement(compliance: compliance)
    end

    def on_tier_0_entry(new_state, event, compliance:)
      user.instrument_trade_controls_enforcement(compliance: compliance)
    end

    def on_tier_1_entry(new_state, event, compliance:)
      user.instrument_trade_controls_enforcement(compliance: compliance)
    end

    def on_unrestricted_entry(new_state, event, compliance:)
      user.instrument_trade_controls_override(compliance: compliance)
    end

    # We deviate from workflow-orchestrator's behavior here. Instead of
    # persisting the state via update_column, we save the new state and
    # run validations by using update. We do this because it's possible
    # that several events have been queued for the same user, and we
    # need validations to run to prevent duplicate records for one user.
    # see note at: https://github.com/lorefnon/workflow-orchestrator/tree/4a96f194faebb01498f7d2827b4bb1fde5f9c239#transition-event-handler
    def persist_workflow_state(new_value)
      update(self.class.workflow_column => new_value)
    end

    concerning :Enforce do

      protected

      # Private: but marked protected to be invoked by workflow gem
      #
      # Enforce the limited access afforded to restricted users.
      def enforce(compliance:)
        suspend_subscription
        zero_out_invoices
        destroy_migrations
        unpublish_private_pages
        schedule_downgrade
        send_enforcement_email
        populate_ofac_flagged_country(compliance)
        clear_pending_restriction
      end

      # Private: but marked protected to be invoked by workflow gem
      #
      # Enforce the less-limited access afforded to restricted orgs.
      def partially_enforce(compliance:)
        # While this is almost 100% duplication, I think it makes sense for now to leave it. There
        # has been feature churn on this project so far, and I'd prefer not to have to untangle
        # things after tangling them.
        suspend_subscription
        zero_out_invoices
        destroy_migrations
        unpublish_private_pages
        schedule_downgrade
        populate_ofac_flagged_country(compliance)
        # N.B. we are not sending an email here on purpose, this was not an oversight
      end

      private

      def suspend_subscription
        SuspendPlanSubscriptionJob.perform_later(user.plan_subscription) if user.plan_subscription
      end

      def tier_0_enforce(compliance:)
        partially_enforce(compliance: compliance)
      end

      def tier_1_enforce(compliance:)
        partially_enforce(compliance: compliance)
      end

      def zero_out_invoices
        Billing::Zuora::ZeroOutInvoices.for_account(user.customer.zuora_account_id) if user.zuora_account?
      end

      def destroy_migrations
        Migration.where(owner: user).find_each do |m|
          MigrationDestroyFileJob.enqueue(m)
        end
      end

      def unpublish_private_pages
        user.unpublish_private_pages
      end

      def schedule_downgrade
        OFACDowngrade.schedule_for(user)
      end

      def send_enforcement_email
        user.send_trade_controls_enforcement_email
      end

      # Private: Stores country/region information for a restriction when we are able to infer the country/region
      # from the violation data.
      #
      # If both country code and region name is available we use both. Otherwise we use which one is available.
      def populate_ofac_flagged_country(compliance)
        hydro_hash = compliance.to_hydro
        country_name = hydro_hash[:country]
        region_name = hydro_hash[:region]

        country = TradeControls::Country.from_braintree(Braintree::Address::CountryNames.find { |c| c[0] == country_name })
        country_code = country.alpha3.presence || country.alpha2
        if country_code.present?
          # Not likely to have both region and country_code at the same time.
          # Currently only happens when sanctioned region is Crimea.
          # We want to use them both if they are available.
          # Also trade_restricted_country_code has a 16 char limit. We could increase
          # the limit if there is any other sanctioned region apart from Crimea with a longer name.
          country_info = if region_name.present?
            region = region_name == "Autonomous Republic of Crimea" ? "Crimea" : region
            "#{country_code} -- #{region}"
          else
            country_code
          end
          update(trade_restricted_country_code: country_info)
        elsif region_name.present?
          region = region_name == "Autonomous Republic of Crimea" ? "Crimea" : region
          update(trade_restricted_country_code: region)
        end
      end

      # Private: Clears all other pending enforcements for an Organization/User since enforcement has taken place.
      def clear_pending_restriction
        if user.has_any_pending_trade_restriction?
          TradeControls::PendingRestriction.pending.where(target_id: user.id).update_all(enforcement_status: "completed")
        end
      end
    end

    concerning :Override do

      protected

      # Private: but marked protected to be invoked by workflow gem
      #
      # Undoes the effects of a user being restricted.
      # This does not prevent re-flagging in the future.
      def override(compliance:)
        resume_subscription
        cancel_scheduled_downgrade
        send_reactivation_email
        unban_sponsors_membership(actor: compliance.actor)
      end

      private

      def resume_subscription
        ResumePlanSubscriptionJob.perform_later(user.plan_subscription) if user.plan_subscription
      end

      def cancel_scheduled_downgrade
        user.scheduled_ofac_downgrade&.destroy
      end

      def send_reactivation_email
        user.send_trade_controls_override_email
      end

      def unban_sponsors_membership(actor:)
        membership = user.sponsors_membership
        return if membership.blank?

        return unless membership.banned?

        user.sponsors_membership.un_ban!(un_banned_by: actor)
      end
    end

    concerning :Upgrade do
      protected

      def tier_1_upgrade(compliance:)
        # we don't do anything yet
      end
    end

    concerning :Downgrade do
      protected

      def tier_0_downgrade(compliance:)
        # we don't do anything yet
      end
    end

    concerning :Instrumentation do
      include Instrumentation::Model

      private

      # Default event_prefix is trade_controls/restriction
      # but the audit-log UI in stafftools doesn't handle slashes well
      def event_prefix
        :trade_controls_restriction
      end

      # Include user in payload by default, keyed appropriately as :user or :org
      def event_payload
        Hash[(user.organization? ? :org : :user), user]
      end
    end
  end
end
