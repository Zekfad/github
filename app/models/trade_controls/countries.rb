# frozen_string_literal: true

module TradeControls
  # Represents a collection of Country instances.
  #
  # If/when Country becomes a db-backed ActiveRecord model, the majority of
  # the Countries interface would become class methods and/or scopes on Country.
  # Thus the api of this class is expected to be similar to (and compatible with)
  # the majority of the AR Relation interface (ie, enumerable, finders, plucking)
  class Countries
    CUBA = Country.from_braintree(Braintree::Address::CountryNames.find { |c| c[1] == "CU" })
    IRAN = Country.from_braintree(Braintree::Address::CountryNames.find { |c| c[1] == "IR" })
    SYRIA = Country.from_braintree(Braintree::Address::CountryNames.find { |c| c[1] == "SY" })
    NORTH_KOREA = Country.from_braintree(Braintree::Address::CountryNames.find { |c| c[1] == "KP" })

    CRIMEA = Country.from_braintree(Braintree::Address::CountryNames.find { |c| c[1] == "UA" })
      .update(region: { name: "Autonomous Republic of Crimea", code: "43"})

    include Enumerable
    delegate :each, to: :@countries

    def initialize(countries)
      @countries = Set.new countries
    end

    # These "property" methods are just shortcuts for mapping out particular
    # properties from the contained Country instances. If Country becomes an
    # AR model, these would just become "plucks"
    def alpha2
      map(&:alpha2)
    end

    def alpha3
      map(&:alpha3)
    end

    def domain
      map(&:domain)
    end

    def name
      map(&:name)
    end

    # This is intentionally similar AR finders because Country may very likely
    # become an AR model and thus have a find_by class method.
    def where(**attributes)
      find_all { |c|
        attributes.all? { |k, v|
          Array(v).include? c.__send__(k)
        }
      }
    end

    # This is intentionally similar AR finders because Country may very likely
    # become an AR model and thus have a find_by class method.
    def find_by(**attributes) # rubocop:disable GitHub/FindByDef
      where(**attributes).first
    end

    # These constants would likely be scopes for pre-set groups of countries,
    # if Country were an AR model.
    BILLING_ADDRESS_DENYLIST = new([CUBA, IRAN, NORTH_KOREA, SYRIA])
    SANCTIONED = new([IRAN, NORTH_KOREA, SYRIA])
    SANCTIONED_REGIONS = new([CRIMEA])

    # This would be a scope+pluck if Country were an AR model
    def self.billing_address_blacklist_alpha3
      @billing_address_blacklist_alpha3 ||= BILLING_ADDRESS_DENYLIST.alpha3
    end

    # This would be a scope if Country were an AR model
    def self.currently_unsanctioned
      @currently_unsanctioned ||=
        Braintree::Address::CountryNames.reject { |(_, _, alpha3, _)|
          billing_address_blacklist_alpha3.include?(alpha3.upcase)
        }
    end
  end
end
