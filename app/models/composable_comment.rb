# frozen_string_literal: true

class ComposableComment < InteractiveComponent::Container
  areas_of_responsibility :ce_extensibility

  include GitHub::UTF8
  include GitHub::UserContent
  include GitHub::Validations
  include GitHub::RateLimitedCreation
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model
  include AuthorAssociable::InteractiveComponentContainer
  include NotificationsContent::WithCallbacks
  include EmailReceivable
  include ComposableComment::NewsiesAdapter

  belongs_to :integration
  belongs_to :issue, touch: true

  has_one :repository, through: :issue
  has_many :interactive_components,
    as: :container,
    validate: true,
    autosave: true,
    dependent: :delete_all

  has_many :markdown_components,
    as: :container,
    validate: true,
    autosave: true,
    dependent: :delete_all

  attr_writer :components
  attr_writer :notifications

  # Refers to position in timeline
  # In the future we'll add :sticky_to_bottom
  enum position: [:inline]

  after_commit :notify_issue_socket_subscribers, on: [:create]
  after_commit :notify_socket_subscribers, on: [:update]

  after_commit :subscribe_and_notify, on: :create

  before_validation :create_components, on: [:create]
  before_validation :update_components, on: [:update]

  validates_presence_of :integration
  validates_presence_of :issue

  validate :at_least_one_component
  validate :no_more_than_ten_components
  validate :valid_component_external_ids

  def associated_domain
    InteractiveComponent::Container::ASSOCIATED_DOMAINS[:repo]
  end

  def async_readable_by?(actor)
    issue.async_readable_by?(actor)
  end

  def async_user
    async_integration.then do |integration|
      integration&.async_bot
    end
  end

  def notify_socket_subscribers
    data = {
      timestamp: Time.now.to_i,
      wait: default_live_updates_wait,
      reason: "composable comment ##{id} updated",
      gid: global_relay_id,
    }

    GitHub::WebSocket.notify_issue_channel(issue, websocket_channel, data)
  end

  def notify_issue_socket_subscribers
    issue.notify_socket_subscribers(notify_associated: false)
  end

  # See IssueTimeline
  def timeline_sort_by
    [created_at]
  end

  def at_least_one_component
    if components.length < 1
      errors.add(:components, "at least 1 component required")
    end
  end

  def no_more_than_ten_components
    if components.length > 10
      errors.add(:components, "no more than 10 components permitted")
    end
  end

  def valid_component_external_ids
    external_id_groups = components.group_by { |comp| comp["external_id"] }
    non_compliant_external_ids = external_id_groups.select do |external_id, group|
      external_id.present? && group.size > 1
    end.keys
    return if non_compliant_external_ids.empty?

    quoted_ids = non_compliant_external_ids.map { |n| "\"#{n}\"" }
    errors.add(:components, "must have unique external ids. #{quoted_ids.to_sentence} "\
      "#{"was".pluralize(non_compliant_external_ids.size)} used more than once.")
  end

  def readable_by?(user)
    async_readable_by?(user).sync
  end

  def components
    # Don't use `without_outdated` scope on interactive_components
    # This goes to the database, but when we're here the components won't be saved
    # This especially impacts _new_ components which won't be persisted yet
    (markdown_components + interactive_components.reject(&:outdated?)).sort_by(&:container_order)
  end

  def create_components
    @components.each_with_index do |component, index|
      case component["type"]
      when "markdown"
        # Saved using :autosave
        markdown_components.build(
          body: component["text"],
          container_order: index,
        )
      when "interactive"
        # Also saved using :autosave
        interactive_components.build(
          external_id:  component["external_id"],
          elements: component["elements"].to_json,
          container_order: index,
        )
      else
        errors.add(:components, "invalid component type: #{component["type"]}")
      end
    end
  end

  def mark_components_for_destruction
    components.each do |component|
      if component.respond_to?(:outdated_at)
        component.update!(outdated_at: Time.now)
      else
        component.mark_for_destruction
      end
    end
  end


  def update_components
    mark_components_for_destruction

    self.touch

    create_components
  end

  def websocket_channel
    GitHub::WebSocket::Channels.composable_comment(self)
  end

  def anchor
    "composablecomment-#{id}"
  end

  def serialized_hash
    Api::Serializer.composable_comment_hash(self)
  end
end
