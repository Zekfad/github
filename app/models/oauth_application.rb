# rubocop:disable Style/FrozenStringLiteralComment

# An OauthApplication is a registered application or services that integrates
# with GitHub. OauthApplications can act on behalf of users that explicitly
# authorize them to do so.
class OauthApplication < ApplicationRecord::Domain::Integrations
  self.ignored_columns = %w(secret)
  include PrimaryAvatar::Model
  include Avatar::List::Model
  include GitHub::FlipperActor
  include OauthAccess::Provider
  include Marketplace::Listable
  include AppNameValidity
  include GitHub::RateLimitedCreation
  extend GitHub::Earthsmoke::EncryptedAttribute

  include OauthApplication::FeatureFlagMethods

  IDENTICON_TYPE = "oauth_app".freeze

  DEFAULT = {
    name: "Personal Access Token",
    url: GitHub.developer_help_url + "/v3/oauth_authorizations/",
    callback_url: "https://github.com",
  }.freeze

  # regex pattern for matching client IDs (hex strings of length 20)
  CLIENT_ID_PATTERN = /\A[0-9a-f]{20}\z/i

  # default error message for an invalid URL
  INVALID_URL_MESSAGE = "must be a valid URL".freeze

  # Public: A special oauth application id that we use to generate personal
  # tokens for scripts and such (id = 0).
  PERSONAL_TOKENS_APPLICATION_ID   = 0.freeze
  PERSONAL_TOKENS_APPLICATION_TYPE = "OauthApplication".freeze
  PERSONAL_TOKENS_CLIENT_ID        = ("0"*20).freeze

  # Our only means of identifying GitHub client apps.
  GITHUB_MAC_CLIENT_ID        = "eac522c6b68c504b2aac".freeze
  GITHUB_WINDOWS_CLIENT_ID    = "fd5f729d309a7bfa8e1b".freeze
  GITHUB_VS_CLIENT_ID         = "a200baed193bb2088a6e".freeze
  GITHUB_DESKTOP_CLIENT_ID    = "de0e3c7e9973e1c4dd77".freeze
  GITHUB_CLI_CLIENT_ID        = "178c6fc778ccc68e1d6a".freeze
  HUB_CLI_CLIENT_ID           = "20a7719cd6246bdf8b48".freeze
  GITHUB_UNITY_PROD_CLIENT_ID = "107b906ff287f62a12a4".freeze
  GITHUB_UNITY_DEV_CLIENT_ID  = "924a97f36926f535e72c".freeze
  GITHUB_VSCODE_CLIENT_ID     = "01ab8ac9400c4e429b23".freeze
  GCMCORE_CLIENT_ID           = "0120e057bd645470c1ed".freeze

  GITHUB_MOBILE_ANDROID_CLIENT_ID = "3f8b8834a91f0caad392".freeze
  GITHUB_MOBILE_IOS_CLIENT_ID = "2cfa9b7a1b57de32dd0d".freeze

  # Our desktop apps (standalone clients and editor integrations)
  # If the app has git capabilities, it should go here
  GIT_GITHUB_APPS_IDS = Set.new([
    GITHUB_MAC_CLIENT_ID,
    GITHUB_WINDOWS_CLIENT_ID,
    GITHUB_DESKTOP_CLIENT_ID,
    GITHUB_CLI_CLIENT_ID,
    HUB_CLI_CLIENT_ID,
    GITHUB_VS_CLIENT_ID,
    GITHUB_UNITY_PROD_CLIENT_ID,
    GITHUB_UNITY_DEV_CLIENT_ID,
    GITHUB_VSCODE_CLIENT_ID,
    GCMCORE_CLIENT_ID,
  ]).freeze

  # Our github-only mobile apps
  GITHUB_MOBILE_IDS = Set.new([
    GITHUB_MOBILE_ANDROID_CLIENT_ID,
    GITHUB_MOBILE_IOS_CLIENT_ID,
  ]).freeze

  CAMPUS_EXPERTS_DOCUMENTATION = "ec0ff7f231f9ea51dcf9".freeze
  CAMPUS_EXPERTS_PROFILE       = "b71214e719d5771ed590".freeze
  GITHUB_CLASSROOM             = "64a051cf1598b9f0658f".freeze

  # Our github oauth apps for education and campus experts
  EDUCATION_APPS_IDS = Set.new([
    CAMPUS_EXPERTS_DOCUMENTATION,
    CAMPUS_EXPERTS_PROFILE,
    GITHUB_CLASSROOM,
  ]).freeze

  # All our client apps
  CLIENT_APPS_IDS = Set.new(GIT_GITHUB_APPS_IDS).merge(GITHUB_MOBILE_IDS).freeze

  # OAuth Applications that are operated but may not be owned
  # by the GitHub organization
  #
  # TODO: Remove this in favor of Apps::Internal registry.
  OPERATED_BY_GITHUB_IDS = Set.new(CLIENT_APPS_IDS).merge(EDUCATION_APPS_IDS).freeze

  def self.default(note = nil, url = nil)
    if (note.present? || url.present?)
      options = {}
      options[:name] = note if note.present?
      options[:url] = url if url.present?
      pseudo(options)
    else
      @default ||= pseudo
    end
  end

  def self.pseudo(options = {})
    oauth_app = new(DEFAULT.merge(options))
    oauth_app.id  = PERSONAL_TOKENS_APPLICATION_ID
    oauth_app.key = PERSONAL_TOKENS_CLIENT_ID

    oauth_app
  end

  # Public: The User record that owns (created) this oauth application.
  belongs_to :user
  validates_presence_of :user_id

  # Public: OauthAccesses granted to this application.
  has_many :accesses,
    -> { where(application_type: "OauthApplication") },
    class_name: "OauthAccess",
    foreign_key: :application_id,
    dependent: :destroy

  # Public: OauthAccesses granted to this application.
  has_many :authorizations,
    -> { where(application_type: "OauthApplication") },
    class_name: "OauthAuthorization",
    foreign_key: :application_id,
    dependent: :destroy

  # Internal: PublicKeys created by this application.
  has_many   :public_keys, through: :accesses

  # Public: Hooks created on behalf of a user by this application.
  has_many   :hooks, dependent: :destroy

  # Public: Logo associated with this oauth application (optional, defaults to owner's avatar).
  belongs_to :logo, class_name: "OauthApplicationLogo", foreign_key: :logo_id, dependent: :destroy

  # Public: Association relationship for OauthApplicationApprovals.
  has_many :approvals,
    class_name: "OauthApplicationApproval",
    foreign_key: :application_id,
    dependent: :delete_all

  # Public: Association relationship for OauthApplicationTransfers.
  has_one :transfer,
    class_name: "OauthApplicationTransfer",
    foreign_key: :application_id,
    dependent: :destroy

  # Public: The listing information for the Integration Directory, if any.
  has_one :integration_listing,
    as: :integration,
    dependent: :destroy

  # Public: String URL of this oauth application.
  # column :url
  validates_presence_of :url
  validate :check_application_url

  # Public: String callback URL of this oauth application.
  # column :callback_url
  validates_presence_of :callback_url
  validate :check_callback_url

  validate :check_callback_urls

  # Public: String name of this oauth application.
  # column :name
  validates_presence_of :name
  validate :restrict_names_with_github
  validate :restrict_name_unicode_chars

  # Public: String description of this oauth application.
  # column :description

  # Public: String client_id of this oauth application.
  # column :key
  validates_presence_of :key
  validates_uniqueness_of :key, case_sensitive: true

  # Public: String client_secret of this oauth application.
  # column :secret
  validates_presence_of :encrypted_secret

  # Public: Integer customer API rate limit for this application.
  # column :rate_limit

  # Public: Boolean flag to specify application is fully trusted. Full trust
  # applications are owned and operated by GitHub, Inc. and allow OauthAccess
  # granting WITHOUT user approval.
  # column :full_trust

  # Public: String host of this application's URL (set automatically).
  # column :domain
  validate :set_domain

  validates_with CreatedApplicationsLimitValidator, on: :create

  encrypted_attribute :encrypted_secret, :plaintext_secret, read_error: :raise, write_error: :raise

  # Public: Integer state of the oauth application.
  # column :state
  #   :active           - Active and allowed to have oauth accesses (default).
  #   :suspended        - Suspended from generating oauth accesses due to abuse
  #                       or security concerns.
  #   :pending_deletion - In the process of being deleted in a background job.
  #                       This state is largely for the UI.
  enum state: { active: 0, suspended: 1, pending_deletion: 2 }


  # Public: Array of String scopes this application can request (full trust
  # applications only right now).
  # array :scopes

  # Public: Array of String callback urls to be validated against redirect_uri
  # during the access token request
  # array :callback_urls
  serialize :raw_data, Coders::Handler.new(Coders::OauthApplicationCoder)
  delegate *Coders::OauthApplicationCoder.members, to: :raw_data

  # Public: Set the required scopes for this application (full trust
  # applications only right now).
  # scopes - Strong comma separated list of scopes (or Array of String scopes).
  #
  # Returns nothing.
  def set_scopes(scopes)
    self.scopes = OauthAccess.normalize_scopes(scopes, visibility: :all)
  end

  before_validation :normalize_bgcolor
  before_validation :generate_key
  before_validation :generate_secret
  after_create_commit  :instrument_creation

  after_update_commit  :instrument_secret_reset, if: :saved_change_to_encrypted_secret?

  after_update_commit  :instrument_suspension,   if: [:state_previously_changed?, :suspended?]
  after_update_commit  :instrument_unsuspension, if: [:state_previously_changed?, :active?]

  # NOTE: Moving instrument_deletion to a commit callback causes test failures in
  # many tests related to missing User models due to the user deletion flow.
  after_destroy :instrument_deletion # rubocop:disable GitHub/AfterCommitCallbackInstrumentation

  # Returns OauthApplications that the given User owns or can manage because they're owned by an
  # Organization for which the given User is an admin.
  scope :adminable_by, ->(user) {
    user_ids = [user.id]

    adminable_org_ids = Ability.user_admin_on_organizations(
      actor_id: user.id,
    ).pluck(:subject_id)
    user_ids = user_ids.concat(adminable_org_ids) if adminable_org_ids.present?

    where(user_id: user_ids)
  }

  # Returns OauthApplications that do not have a Marketplace::Listing.
  scope :not_in_marketplace, -> {
    left_outer_joins(:marketplace_listing).where(marketplace_listings: { listable_id: nil })
  }

  attr_writer :skip_restrict_names_with_github_validation

  # OauthApplications default to the global default rate limit, but an
  # application can be granted a higher rate limit to get more calls. Use
  # #set_temporary_rate_limit to set a 3-day rate limit increase for initial
  # imports and that kind of thing.
  #
  # Returns the rate limit integer value for calls per hour
  def rate_limit
    temporary_rate_limit || self[:rate_limit] || GitHub.api_default_rate_limit
  end

  def temporary_rate_limit
    if temporary_rate_limit_expires_at && temporary_rate_limit_expires_at.future?
      self[:temporary_rate_limit]
    end
  end

  def set_temporary_rate_limit(limit, duration = 3.days)
    update! temporary_rate_limit: limit, temporary_rate_limit_expires_at: duration.from_now
  end

  def using_temporary_rate_limit?
    !temporary_rate_limit.nil?
  end

  # Public: check if a url is a direct match of a url in the
  # callback_urls list. Then also makes sure it's a valid
  # callback url?
  #
  # Returns true if the url matches and is a valid or false
  # otherwise.
  def callback_url_direct_match?(url)
    if callback_urls && callback_urls.include?(url)
      GitHub.dogstats.increment "oauth_application", tags: ["action:callback_url_exact_match"]
      valid_callback_url?(url)
    else
      false
    end
  end

  # OauthApplications can belong to user or organizations. Use this method to
  # be a little more clear.
  #
  # Returns the User or Organization owner of the application
  def owner
    user
  end

  # Public: Set the callback URL for this oauth application
  def callback_url=(url)
    set_callback_urls url
  end

  # Public: Returns the first callback URL from callback_urls if any exist,
  # otherwise it will return the callback_url attribute or fall back to
  # assigning callback_url to url.
  def callback_url
    if callback_urls && callback_urls.any?
      callback_urls.first
    else
      read_attribute(:callback_url) || self.callback_url = url
    end
  end

  # Public: Set the list of callback URLs to be validated against redirect_uri
  # during the access token request.
  #
  # urls - A splat of String urls.
  #
  # Returns nothing.
  def set_callback_urls(*urls)
    @callback_uri = nil
    urls ||= []
    self.callback_urls = urls.flatten.reject { |u| u.blank? }.uniq
    self.callback_urls = [url] if url && callback_urls.length == 0
    write_attribute :callback_url, self.callback_urls.first
  end

  def callback_uri
    return if callback_url.blank?
    @callback_uri ||= Addressable::URI.parse(callback_url)
  end

  # Public: Boolean flag to tell if an application is a full trust OAuth app
  # owned by GitHub.
  #
  # Returns truthy if this application is owned by GitHub, Inc. and
  # has full trust access.
  def github_full_trust?
    full_trust? && github_owned?
  end

  # Public: Boolean flag to tell if an application uses Oauth scopes.
  #
  # Returns true for all OauthApplications.
  def uses_scopes?
    true
  end

  # Public: Boolean flag to tell if an application is an OAuth app
  # owned by GitHub.
  #
  # Returns true if this application is owned by GitHub, Inc.
  def github_owned?
    user_id == GitHub.trusted_apps_owner_id
  end

  # Indicates if this application is a GitHub Desktop client.
  #
  # Returns true if this app's key matches known keys for those apps.
  def github_desktop?
    GIT_GITHUB_APPS_IDS.include? self.key
  end

  # Indicates if this application is a GitHub client app.
  #
  # Returns true if this app's key matches known keys for those apps.
  def github_client_app?
    CLIENT_APPS_IDS.include? self.key
  end

  # Indicates if this application is a GitHub ios app.
  #
  # Returns true if this app's key matches the GitHub ios client ID.
  def github_ios_app?
    return false unless github_owned?
    self.key == GITHUB_MOBILE_IOS_CLIENT_ID
  end

  # Indicates if this application is a GitHub android app.
  #
  # Returns true if this app's key matches the GitHub android client ID.
  def github_android_app?
    return false unless github_owned?
    self.key == GITHUB_MOBILE_ANDROID_CLIENT_ID
  end

  def github_gist?
    return false unless github_owned?
    self.key == GitHub.gist3_oauth_client_id
  end

  # Indicates if the application should be exempted from organization
  # OAuth Application policies.
  #
  # Returns a Boolean.
  def organization_policy_exempt?
    github_client_app? || github_full_trust? # rubocop:disable GitHub/FullTrust
  end

  # Indicates if third party restrictions can be applied to this application.
  # True for all OAuth Applications.
  def third_party_restrictions_applicable?
    true
  end

  # Public: Boolean flag to indicate if the app is owned by a given
  # user or organization.
  #
  # user_or_org - User or Organization instance.
  #
  # Returns a Boolean.
  def owned_by?(user_or_org)
    return false unless user_or_org.is_a?(User)

    self.user_id == user_or_org.id
  end

  def manageable_by?(user)
    owner == user || owner.organization? && owner.adminable_by?(user)
  end

  def user_token_expiration_enabled?
    false
  end

  def generate_secret
    return unless encrypted_secret.nil?
    self.plaintext_secret ||= self.class.generate_secret
  end

  def reset_secret
    self.plaintext_secret = self.class.generate_secret
    self.save
  end

  def self.generate_secret
    SecureRandom.hex(20)
  end

  def async_revoke_tokens
    RemoveOauthAppTokensJob.perform_later(id)
  end

  # Return a human readable string for use in audit logs.
  def to_s
    "#{self.class.name}:#{self.key}"
  end

  # Public: Transfers the application to a target organization and deletes
  # any transfer requests for the application.
  #
  # target - The Organization to transfer the app to.
  # requester  - The User requesting the transfer.
  # responder  - The User completing the transfer.
  #
  # Returns a Boolean.
  def transfer_ownership_to(target, requester:, responder:)
    instrument :transfer, \
      transfer_from: user,
      transfer_to: target,
      requester: requester,
      responder: responder

    self.user = target

    transaction do

      save!
      transfer.destroy if transfer

      approvals.where(organization_id: target.id).destroy_all

      true
    end
  end

  def set_domain
    if url.nil? || !url.valid_encoding?
      self.domain = nil
    else
      self.domain = Addressable::URI.parse(url.to_s).try(:host)
    end
  rescue Addressable::URI::InvalidURIError
  end

  # Public: The number of public keys created by this application.
  #
  # Returns a Integer.
  def public_key_count
    public_keys.count
  end

  # Public: Queues a job to destroy the application.
  #
  # For applications with a large number of OauthAccesses and/or a large number
  # of PublicKeys, destroying the app might take a while. Web/API requests
  # should use this method instead of using #destroy directly.
  #
  # Returns nothing.
  def async_destroy
    self.state = :pending_deletion
    save

    OauthApplicationDeleteJob.perform_later(id)
  end

  include Instrumentation::Model

  def event_context(prefix: event_prefix)
    {
      "#{prefix}".to_sym    => name,
      "#{prefix}_id".to_sym => id,
    }
  end

  # Public: Determine whether the given user is authorized to administer this
  # application.
  #
  # user - A User instance.
  #
  # Returns a Boolean.
  def adminable_by?(user)
    owner = async_user.sync
    owner == user ||
      (owner.organization? && owner.adminable_by?(user))
  end

  def pending_transfer?
    transfer.present?
  end

  # Public: Register GitHub trusted applications.
  # If the application already exists, its attributes are updated.
  # This is mostly used in enterprise configuration processes to register Gist, Hookshot and the native apps in the installation.
  #
  # name - is the application name.
  # client_id - is the client id token.
  # client_secret - is the client secret.
  # url - is the application url. GitHub.url + name by default.
  # callback - is the callback url. GitHub.url + name by default.
  #
  # Returns the new oauth application.
  def self.register_trusted_application(name, client_id, client_secret, url = nil, callback = nil)
    owner_id = GitHub.trusted_apps_owner_id
    host  = Addressable::URI.parse(GitHub.url)

    app_name     = "GitHub #{name}"
    url          = host.join(url || name.downcase)
    callback_url = host.join(callback || url)

    oauth_app = OauthApplication.where(user_id: owner_id, name: app_name).first || OauthApplication.new

    oauth_app.name                   = app_name
    oauth_app.user                   = Organization.find_by(id: owner_id)
    oauth_app.key                    = client_id
    oauth_app.plaintext_secret       = client_secret
    oauth_app.url                    = url.to_s
    oauth_app.callback_url           = callback_url.to_s
    oauth_app.full_trust             = true

    unless oauth_app.save
      fail "Failed to save #{app_name} Oauth Application settings"
    end

    return oauth_app
  end

  # Public: Safely query for an OauthApplication by key
  #
  # Returns an OauthApplication or nil
  def self.find_by_key(key)  # rubocop:disable GitHub/FindByDef
    # Enforce key(s) is/are strings
    if key.is_a?(Array)
      string_key = key.map(&:to_s)
    else
      string_key = key.to_s
    end
    where(key: string_key).first
  end

  # Public: Safely query for an OauthApplication by key
  #
  # Returns an OauthApplication or raises ActiveRecord::RecordNotFound
  # if an OauthApplication is not found
  def self.find_by_key!(key)  # rubocop:disable GitHub/FindByDef
    find_by_key(key) || (raise ActiveRecord::RecordNotFound)
  end

  # Public: Determines the number of OauthApplications created by user
  #
  # Returns an Integer
  def self.created_by_count(user)
    user.oauth_applications.count
  end

  # Public: Get an image URL for the logo that best represents this app, ignoring the current
  # user and whether this app is listed in the Marketplace or not.
  def preferred_avatar_url(size: 80)
    async_preferred_avatar_url(size: size).sync
  end

  def async_preferred_avatar_url(size: 80)
    return @async_preferred_avatar_url[size] if defined?(@async_preferred_avatar_url)

    @async_preferred_avatar_url = Hash.new do |hash, key|
      hash[key] = async_primary_avatar.then do |primary_avatar|
        next primary_avatar_url(key) if primary_avatar

        async_logo.then do |logo|
          next logo.storage_external_url if logo

          Rails.application.routes.url_helpers.app_identicon_path(IDENTICON_TYPE, id)
        end
      end
    end

    @async_preferred_avatar_url[size]
  end

  def primary_avatar_path
    @primary_avatar_path ||= "/oa/#{id}"
  end

  def avatar_editable_by?(user)
    adminable_by?(user)
  end

  def destroy_logo
    return if logo_id.nil?
    logo.destroy
    update(logo_id: nil)
  end

  def keep_old_avatar?
    false
  end

  def handle_previous_avatar(primary)
    super
    destroy_logo
  end

  # Public: Returns an elasticsearch query string that will find all of this
  # applications's audit log events
  #
  # Returns a String which can be passed to elasticsearch as a `query_string`
  def audit_log_query
    # Some audit log entries use application_id and some use oauth_application_id,
    # so we need to search for both with an OR query.
    # See https://github.com/github/github/issues/58540
    "(data.oauth_application_id:#{id} OR data.application_id:#{id})"
  end

  # Public: Indicates if the application should use strict URL valication for
  # registered callback URLs.

  # Note: Prior to adding support for registering multiple callback URLs, we
  # did less strict URL validation, as some applications relied on adding
  # things such as subpaths and subdomains to their callback URL. However, for
  # any application that opts in to multiple callback URLs we will start
  # enforcing strict validation, as our less strict validation has been a
  # source of security issues.
  #
  # Returns a Boolean
  def strict_callback_url_validation?
    callback_urls.to_a.size > 1
  end

  # Public: Indicates if the application is owned by GitHub.
  #
  # TODO: Remove this in favor of Apps::Internal registry.
  #
  # Returns a Boolean.
  def owned_or_operated_by_github?
    return true if owner&.id == GitHub.trusted_apps_owner_id
    OPERATED_BY_GITHUB_IDS.include?(key)
  end

  private

  def normalize_bgcolor
    return unless bgcolor

    self.bgcolor = bgcolor.sub(/\A#/, "")
  end

  def restrict_name_unicode_chars
    if name.present? && !GitHub::UTF8.valid_unicode3?(name)
      errors.add(:name, "should not contain non unicode characters")
    end
  end

  def restrict_names_with_github
    return unless name && user
    return if github_owned? || github_client_app?
    return if user.employee? && user.site_admin?
    return if @skip_restrict_names_with_github_validation

    if name_starts_with_github?
      errors.add(:name, "should not begin with 'GitHub' or 'Gist'")
    end

    if name_implies_github_affiliation?
      errors.add(:name, "should not imply the application is from GitHub")
    end
  end

  # Private: Default attributes for auditing
  def event_payload
    payload = {
      oauth_application: self,
      state: self.class.states[state],
      rate_limit: rate_limit,
      full_trust: full_trust?,
      application_url: url,
      callback_url: callback_url,
    }

    if user.user?
      payload[:user] = user
    else
      payload[:org] = user
    end

    payload
  end

  # Private: Instrument OAuth application creation.
  #
  # Returns nothing.
  def instrument_creation
    instrument :create
  end

  # Private: Instrument OAuth application deletion.
  #
  # Returns nothing.
  def instrument_deletion
    instrument :destroy
  end

  def instrument_suspension
    instrument :suspend
  end

  def instrument_unsuspension
    instrument :unsuspend
  end

  # Private: Instrument resetting the OAuth application secret.
  #
  # Returns nothing.
  def instrument_secret_reset
    instrument :reset_secret
  end

  # Private: Generate a key, the possibility of it colliding with PAT is slim
  def generate_key
    self.key ||= SecureRandom.hex(10)
  end

  # Private: validate application callback_urls are each a String and valid URL.
  def check_callback_urls
    return unless callback_urls.is_a?(Array)
    callback_urls.each_with_index do |url, i|
      unless url.is_a?(String)
        return errors.add(:"callback_url_#{i}", INVALID_URL_MESSAGE)
      end

      unless valid_callback_url?(url)
        errors.add(:"callback_url_#{i}", INVALID_URL_MESSAGE)
      end
    end
  end

  # Private: validate application callback_url is a String and valid URL.
  def check_callback_url
    unless callback_url.is_a?(String)
      return errors.add(:"callback_url", INVALID_URL_MESSAGE)
    end

    unless valid_callback_url?(callback_url)
      errors.add(:"callback_url", INVALID_URL_MESSAGE)
    end
  end

  # Private: validate application url is a String and valid URL.
  def check_application_url
    unless url.is_a?(String)
      return errors.add(:"url", INVALID_URL_MESSAGE)
    end

    unless IntegrationUrl.valid_application_url?(url)
      errors.add(:"url", INVALID_URL_MESSAGE)
    end
  end

  def valid_callback_url?(url)
    # When multiple callback URLs are registered we perform more strict validation.
    options = {}
    if strict_callback_url_validation?
      options[:blocked_query_keys] = OauthUtil::RESERVED_REDIRECT_URI_QUERY_KEYS
      options[:allow_fragment] = false
    end
    IntegrationUrl.valid_callback_url?(url, options)
  end
end
