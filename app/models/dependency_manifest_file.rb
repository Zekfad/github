# frozen_string_literal: true

require "vintage/client"

# Public: Encapsulates some logic related to Dependency Graph interop,
#         specifically whether a local file corresponds to a known manifest type.
class DependencyManifestFile
  GEMFILE_PATTERN = /(\A|\/)(?:gemfile|gems\.rb)\z/i
  GEMFILE_LOCK_PATTERN = /(\A|\/)(?:gemfile\.lock|gems\.locked)\z/i
  GEMSPEC_PATTERN = /\.gemspec\z/i
  PACKAGE_JSON_PATTERN = /(\A|\/)package\.json\z/i
  PACKAGE_LOCK_JSON_PATTERN = /(\A|\/)package-lock\.json\z/i
  YARN_LOCK_PATTERN = /(\A|\/)yarn\.lock\z/i
  PIPFILE_PATTERN = /(\A|\/)pipfile\z/i
  PIPFILE_LOCK_PATTERN = /(\A|\/)pipfile\.lock\z/i
  SETUP_PY_PATTERN = /(\A|\/)setup\.py\z/i
  POM_XML_PATTERN = /(\A|\/)pom\.xml\z/i
  NUSPEC_PATTERN = /\.nuspec\z/i
  CSPROJ_PATTERN = /(\A|\/)(.*)?\.csproj\z/i
  VBPROJ_PATTERN = /(\A|\/)(.*)?\.vbproj\z/i
  VCXPROJ_PATTERN = /(\A|\/)(.*)?\.vcxproj\z/i
  FSPROJ_PATTERN = /(\A|\/)(.*)?\.fsproj\z/i
  PACKAGE_CONFIG_PATTERN = /(\A|\/)packages\.config\z/i
  COMPOSER_LOCK_PATTERN = /(\A|\/)composer\.lock\z/i
  COMPOSER_JSON_PATTERN = /(\A|\/)composer\.json\z/i

  # Regex rule for detecting `requirements` string in python requirements.txt manifest name due to irregular naming:
  # Allow only a hyphen, period or underscore before requirements OR check that requirements is the beginning of the string.
  # Don't allow whitespace after requirements
  # Allow for other things after requirements, like requirements.prod.txt
  REQUIREMENTS_TXT_PATTERN = /(?:\-|\.|\_|\A|\/)requirements[^\s]*\.txt\Z/i

  # More lenient Regex rule for detecting `require` string in python requirements.txt manifest name due to irregular naming:
  # Allow anything except multiple periods OR whitespace before/after require.
  # Allow hyphen or underscore after require followed by non-period and non-whitespace characters.
  # Don't allow require to be a substring, e.g. don't allow "required.txt".
  # Requirements.tx can have a hyphen, underscore or forward slash before and after it.
  REQUIRE_TXT_PATTERN = /(?:[^\s|\.]*)require(?:(?:\-|\_|\/)[^\s|\.]*)?\.txt\Z/i

  ALL_JS_PATTERN = /(\A|\/).*\.js\z/i
  VINTAGE_LIBS = %w(axios debug express lodash moment react react-dom remarkable request underscore vue jquery markdown-to-jsx uuid)

  # Accuracy from Vintage to count files as recognized
  VINTAGE_THRESHOLD = 30

  extend GitHub::UTF8

  # Public: Is this path recognized as a manifest by dependency-graph-api?
  #
  # path - A String representing a file path relative to the repository root.
  #
  # The current list of recognized manifests includes:
  #
  #    Gemfile
  #    Gemfile.lock
  #    <gem-name>.gemspec
  #    package.json
  #    package-lock.json
  #    yarn.lock ~ under feature flag ~
  #    pipfile
  #    setup.py
  #    requirements.txt
  #    <optional-manifest-name>.nuspec
  #    project-name.csproj
  #    project-name.vbproj
  #    packages.config
  #    composer.json
  #    composer.lock
  #
  # They'll be recognized at any point in the repository directory hierarchy
  # except within vendor directories as defined by Linguist.
  #
  # Returns a boolean.
  def self.recognized_path?(path:)
    !!corresponding_manifest_type(path: path)
  end

  # Internal: Maps a manifest file to its package type.
  #
  # path - A String representing a file path relative to the repository root.
  #
  # Compares the base file name of path against known manifest file names.
  #
  # Returns a Symbol or nil.
  def self.corresponding_package_type(path:)
    case corresponding_manifest_type(path: path)
    when :gemfile, :gemfile_lock, :gemspec then :rubygems
    when :package_json, :package_lock_json, :yarn_lock then :npm
    when :requirements_txt, :pipfile, :pipfile_lock, :setup_py then :pip
    when :pom_xml then :maven
    when :composer_json, :composer_lock then :composer
    when :nuspec, :csproj, :vbproj, :fsproj, :vcxproj, :package_config then :nuget
    when :vendored_javascript_dependency then :npm
    end
  end

  def self.corresponding_manifest_type(path:)
    path = utf8(path)

    case path
    # Vendored dependencies that we support (AKA "loose files")
    when ALL_JS_PATTERN then :vendored_javascript_dependency

    # We want to exclude vendored paths *except* for the specific vendored
    # dependencies we support (AKA "loose files"), so this comes after the
    # vendored dependency patterns
    when Linguist::BlobHelper::VendoredRegexp then nil

    # RubyGems
    when GEMFILE_PATTERN then :gemfile
    when GEMFILE_LOCK_PATTERN then :gemfile_lock
    when GEMSPEC_PATTERN then :gemspec

    # npm
    when PACKAGE_JSON_PATTERN then :package_json
    when PACKAGE_LOCK_JSON_PATTERN then :package_lock_json
    when YARN_LOCK_PATTERN then :yarn_lock

    # pip
    when REQUIREMENTS_TXT_PATTERN then :requirements_txt
    when REQUIRE_TXT_PATTERN then :requirements_txt
    when PIPFILE_PATTERN then :pipfile
    when PIPFILE_LOCK_PATTERN then :pipfile_lock
    when SETUP_PY_PATTERN then :setup_py

    # Maven
    when POM_XML_PATTERN then :pom_xml

    # NuGet
    when NUSPEC_PATTERN then :nuspec
    when CSPROJ_PATTERN then :csproj
    when VBPROJ_PATTERN then :vbproj
    when VCXPROJ_PATTERN then :vcxproj
    when FSPROJ_PATTERN then :fsproj
    when PACKAGE_CONFIG_PATTERN then :package_config

    # Composer
    when COMPOSER_JSON_PATTERN then :composer_json
    when COMPOSER_LOCK_PATTERN then :composer_lock
    end
  end

  # Internal: Is the given path a Vintage manifest type (i.e., a file that we'd
  # send to Vintage to see if it's a vendored dependency)?
  #
  # Returns a Boolean.
  def self.vintage_manifest_type?(path:)
    corresponding_manifest_type(path: path) == :vendored_javascript_dependency
  end

  # Public: Maps a package manager to its manifest file.
  #
  # package_manager - A String representing a package manager.
  #
  # Compares the package manager against known package manager names.
  #
  # Returns a String
  def self.corresponding_manifest_file(package_manager:)
    case package_manager.to_sym
    when :MAVEN then "pom.xml"
    when :NPM then "package.json"
    when :NUGET then ".nuspec"
    when :PIP then "requirements.txt"
    when :RUBYGEMS then "Gemfile"
    when :COMPOSER then "composer.json"
    else "package manager"
    end
  end

  # Public: Record dogstats for manifest files touched on a git push.
  #
  # repository - A Repository pushed to.
  # paths - An Enumerable of manifest path Strings.
  # default_branch - Boolean, true if the push was to the repository default
  #                  branch. Default: true.
  # initial_commit - Boolean, true if the push was an initial commit to the
  #                  branch. Default: false.
  #
  # Returns nothing.
  def self.record_repository_manifest_changed_stats(repository:, paths:, default_branch: true, initial_commit: false)
    package_types = paths.map { |p| corresponding_package_type(path: p) }.compact
    return if package_types.empty?

    tags = package_types.map { |t| "package_type:#{t}" }
    tags << "public:#{repository.public?}"
    tags << "fork:#{repository.fork?}"
    tags << "default_branch:#{default_branch}"
    tags << "initial_commit:#{initial_commit}"
    GitHub.dogstats.increment("repository.dependency_manifest_changed", tags: tags)
  end

  # Internal: A ManifestDetector encapsulates the logic that determines whether
  # some repository content represents a manifest supported by the Dependency
  # Graph.
  class ManifestDetector
    def initialize(vintage_enabled: false, vintage_all_js: false)
      @vintage_enabled = vintage_enabled
      @vintage_file_pattern = if vintage_all_js
                               ALL_JS_PATTERN
                              else
                                Regexp.new "(\\A|\\/).*(#{VINTAGE_LIBS.join("|")}).*\\.js\\z", "i"
      end
    end

    def recognized_path?(path:)
      manifest_type = DependencyManifestFile.corresponding_manifest_type(path: path)
      if vintage_enabled?
        !!manifest_type
      else
        manifest_type && manifest_type != :vendored_javascript_dependency
      end
    end

    def descriptor_for(path, oid, content)
      if DependencyManifestFile.vintage_manifest_type?(path: path)
        return nil unless vintage_enabled?
        return nil unless vintage_file_pattern.match? path

        vintage_dependency = vintage_fetch_dependency(path, content)
        return nil if vintage_dependency[:score] < VINTAGE_THRESHOLD

        ManifestDescriptor.new(
          category: :vintage,
          path: path,
          oid: oid,
          name: vintage_dependency["name"],
          version: vintage_dependency["version"],
        )
      else
        ManifestDescriptor.new(category: :standard, path: path, oid: oid)
      end
    end

    private

    def vintage_enabled?
      @vintage_enabled
    end

    def vintage_file_pattern
      @vintage_file_pattern
    end

    def vintage_fetch_dependency(path, content)
      client = Vintage::Client.new

      res = client.scan_file(path, content)

      if res.present?
        res.first.with_indifferent_access
      else
        { name: "", version: "", score: -1 }
      end
    end
  end

  # Internal: A wrapper around repository blobs to normalize
  #           attributes for interaction with the DependencyGraph API.
  class ManifestBlob
    include GitHub::UTF8

    # Construct a ManifestBlob from the given descriptor.
    #
    # repository            - The Repository that contains the desired blob.
    # path                  - The String path to the desired blob.
    # serialized_descriptor - The Hash representing the manifest descriptor,
    #                         in the format used by
    #                         ManifestDescriptor#serialize.
    #
    # Returns a ManifestBlob.
    def self.from_serialized_descriptor(repository, path, serialized_descriptor)
      attrs = {
        path: path,
        repository_id: repository.id,
        oid: serialized_descriptor[:oid],
      }

      case serialized_descriptor[:category]
      when :standard
        blob = repository.rpc.read_blobs([attrs[:oid]]).first
        if blob["truncated"]
          attrs[:exceeds_max_size] = true
          attrs[:content] = if GitHub.enterprise? || repository.owner.business_plus?
            repository.rpc.read_full_blob(attrs[:oid])["data"]
          else
            GitHub.instrument("repo.max_manifest_file_size_hit")
            ""
          end
        else
          attrs[:content] = blob["data"]
        end
      when :vintage
        # This descriptor describes a vendored dependency detected by Vintage.
        # Construct a "synthetic" manifest to allow this depedency to be
        # represented in the Dependency Graph.
        attrs[:content] = {
          name: "",
          version: "",
          dependencies: {
            serialized_descriptor[:name] => serialized_descriptor[:version],
          },
        }.to_json
      end

      DependencyManifestFile::ManifestBlob.new(**attrs)
    end

    attr_reader :path, :filename, :content, :encoded, :oid, :repository_id,
      :exceeds_max_size

    # Public: Initialize a ManifestBlob.
    #
    # path - The String path to the blob.
    # content - The blob's raw String content.
    # oid - The blob's SHA oid.
    # repository_id - The Integer id of the owning Repository.
    #
    # A path of "." will be replaced with "".
    #
    # Encodes the content string as base64 via encoded.
    #
    # Returns a ManifestBlob.
    def initialize(path:, content:, oid:, repository_id:, exceeds_max_size: false)
      @path = File.dirname(path).sub(/\A\.\z/, "")
      @filename = File.basename(path)
      @content = utf8(content.to_s)
      @encoded = Base64.strict_encode64(@content).strip
      @oid = oid
      @repository_id = repository_id
      @exceeds_max_size = exceeds_max_size
    end

    def exceeds_max_size?
      @exceeds_max_size
    end

    def ==(other)
      other.is_a?(ManifestBlob) &&
        path == other.path &&
        repository_id == other.repository_id &&
        oid == other.oid &&
        content == other.content
    end
  end

  # Public: A ManifestDescriptor describes a manifest and its location within
  # a repository. It may describe a traditional manifest file (e.g., Gemfile,
  # package-lock.json), or it may describe a vendored dependency detected by
  # Vintage (e.g., a full copy of jQuery embedded in the repository).
  class ManifestDescriptor
    attr_reader :category, :path, :oid, :name, :version

    def initialize(category:, path:, oid:, name: nil, version: nil)
      @category = category
      @path = path
      @oid = oid
      @name = name
      @version = version
    end

    # Public: Serialize the descriptor for storage.
    #
    # Returns a Hash.
    def serialize
      case category
      when :standard
        { category: category, oid: oid }
      when :vintage
        { category: category, oid: oid, name: name, version: version }
      end
    end
  end
end
