# frozen_string_literal: true

module Permissions
  class QueryRouter

    ACTOR_TYPES_WITH_FINE_GRAINED_PERMISSIONS = [
      "IntegrationInstallation",
      "OauthAuthorization",
      "ScopedIntegrationInstallation",
    ]

    class FgpQueryDetectedError < ArgumentError; end

    def self.for(subject_types:)
      new(subject_types: subject_types)
    end

    def self.model_for(sql)
      if sql.binds[:abilities_or_permissions].value == "abilities"
        Ability
      else
        Permission
      end
    end

    # Public: write an ability record via the passed in block and write an
    # equivalent direct app permission on the subject in the Collab cluster.
    # Use this method to wrap calls to write FGP abilities records for
    # IntegrationInstallations.
    #
    # actor   - Ability::Actor (IntegrationInstallation, OauthAuthorization)
    # subject - Ability::Subject (IntegrationInstallation::AbilityCollection)
    # action  - Symbol: one of :read, :write or :admin.
    # block   - Ruby block that writes ability records for the given subject,
    #           actor and action. This will be run before the permissions table
    #           has been updated.
    #
    # Returns result of calling the given block, or nil.
    def self.dual_write_app_permission(actor:, subject:, action:, &block)
      Ability.transaction do
        Permission.transaction do
          Permissions::Service.grant_ability_derived_permission(
            ability_id: GitHub::SQL::NULL,
            actor: actor,
            subject: subject,
            action: action,
          )
        end
      end
    end

    # Public: update one or more direct app permissions in the Collab cluster
    # and update equivalent ability record via the passed in block.  Use this
    # method to wrap calls to update FGP abilities records for
    # IntegrationInstallations.
    #
    # action      - Symbol: one of :read, :write or :admin.
    # abilities   - Array of Ability records.
    # block       - Ruby block that updates ability records for the given
    #               action, and abilities. This will be run after the
    #               permissions table has been updated.
    #
    # Returns result of calling the given block, or nil.
    def self.dual_update_app_permissions(action:, abilities:, &block)
      Ability.transaction do
        Permission.transaction do
          abilities.each do |ability|
            Permissions::Service.update_permission_action(
              actor_id: ability.actor_id,
              actor_type: ability.actor_type,
              subject_id: ability.subject_id,
              subject_type: ability.subject_type,
              action: Ability.actions[action],
              priority: Ability.priorities[ability.priority],
            )
          end
        end
      end
    end

    # Public: delete direct app permissions in the Collab cluster and delete
    # equivalent ability record via the passed in block. Use this method to
    # wrap calls to delete FGP abilities records for IntegrationInstallations.
    #
    # abilities - Array of Ability records.
    # block     - Ruby block that deletes the given ability records.
    #             This will be run after the permissions table has been updated.
    #
    # Returns result of calling the given block, or nil.
    def self.dual_delete_app_permissions(abilities:, &block)
      Ability.transaction do
        Permission.transaction do
          abilities.each do |ability|
            Permissions::Service.revoke_permissions(
              actor_id: ability.actor_id,
              actor_type: ability.actor_type,
              subject_id: ability.subject_id,
              subject_type: ability.subject_type,
              action: Ability.actions[ability.action],
              priority: Ability.priorities[ability.priority],
            )
          end
        end
      end
    end

    def self.delete_app_permissions_on_actor(actor)
      Permissions::Service.revoke_permissions_granted_on_actor(
        actor_id:   actor.ability_id,
        actor_type: actor.ability_type,
      )
    end

    # Public: delete app permissions for a given subject.
    #
    # subject - The subject Object
    #
    # Returns result of calling given block, or nil.
    def self.delete_app_permissions_on_subject(subject)
      Permission.transaction do
        Permissions::Service.revoke_permissions_granted_on_subject(
          subject_id:    subject.id,
          subject_types: subject.resources.class.all_prefixed_subject_types,
        )
      end
    end

    # Public: A list of all data sources that could possibly store Ability
    # shaped records for the given actor. Use this when you need to query
    # for all abilities records for a given actor, regardless of whether the
    # abilities might be fine-grained, coarse-grained, or both.
    #
    # Returns an Array of Hashes of the form [ { client: , table_name: } ]
    # client is one of Ability.github_sql or Permission.github_sql
    # table_name is a GitHub::SQL::Literal suitable for use in the FROM clause
    # of a SQL statement.
    def self.data_sources_for_actor(actor)
      sources = [
        { client: Ability.github_sql, table_name: GitHub::SQL::Literal.new("abilities") },
      ]

      if can_have_fine_grained_permissions?(actor_type: actor.ability_type)
        sources << { client: Permission.github_sql, table_name: GitHub::SQL::Literal.new("permissions") }
      end

      sources
    end

    # Internal: Tests whether a given subject type matches the pattern we
    # expect for fine-grained permissions, which is a capitalized string with a
    # foward slash in the middle.
    #
    # Returns either match data if it was a fine-grained subject type, or nil
    # if it was not.
    def self.match_fine_grained_subject_type?(subject_type)
      /[A-Z]\w+\/\w*/.match("#{subject_type}")
    end

    # Internal: Only GitHub Apps can have fine grained permissions that are represented
    # by Ability models for now.
    def self.can_have_fine_grained_permissions?(actor_type:)
      ACTOR_TYPES_WITH_FINE_GRAINED_PERMISSIONS.include?(actor_type)
    end

    attr_reader :subject_types

    def initialize(subject_types:)
      @subject_types = Array(subject_types)
    end

    def sql(*args)
      query = nil
      if args.first.is_a?(String)
        query = args.shift
      end
      options = args.shift || {}

      if subject_types_in_collab?
        options[:abilities_or_permissions] = GitHub::SQL::Literal.new("permissions")
        Permission.github_sql.new(query, options)
      else
        options[:abilities_or_permissions] = GitHub::SQL::Literal.new("abilities")
        Ability.github_sql.new(query, options)
      end
    end

    def subject_types_in_collab?
      subject_types.all? do |subject_type|
        next false unless fine_grained?(subject_type)

        collab_subject_types.any? { |type| type.start_with?(subject_type) }
      end
    end

    # Internal: a list of subject types for which abilities records have been
    # fully migrated to the collab/permissions table.
    #
    # NOTE: As of 2019-03-07, *all* fine-grained permissions have been migrated
    # to the permissions table and this list represents all possible Apps
    # related FGP subject types.
    #
    # Returns an Array of subject types in the form: 'Resource/sub_resource'
    def collab_subject_types
      @collab_subject_types ||= Permissions::ResourceRegistry.all_prefixed_subject_types
    end

    # Internal: Does the given subject_type represent a "fine grained"
    # permssion:
    #
    # E.g.  "Repository/issues" => true
    #       "Repository/" => true (wildcard)
    #       "Repository" => false (coarse grained)
    #
    # Returns a Boolean.
    def fine_grained?(subject_type)
      fine_grained_subject_types.include?(subject_type)
    end

    private

    def fine_grained_subject_types
      @_fine_grained_subject_types ||= subject_types.select do |subject_type|
        self.class.match_fine_grained_subject_type?(subject_type)
      end
    end

    def coarse_grained_subject_types
      subject_types - fine_grained_subject_types
    end
  end
end
