# frozen_string_literal: true

module Permissions
  module Attributes
    class RepositoryAdvisory < Default
      def subject_attributes
        super.merge(
          "subject.published"              => participant.published?,
          "subject.repository.id"          => participant.repository_id,
          "subject.repository.owner.id"    => participant.repository&.owner_id,
          "subject.repository.public"      => participant.repository&.public?,
          "subject.repository.internal"    => participant.repository&.internal?,
          "subject.owning_organization.id" => participant.repository&.owning_organization_id,
        )
      end
    end
  end
end
