# frozen_string_literal: true

module Permissions
  module Attributes
    class DiscussionCategory < Default
      def subject_attributes
        super.merge(
          "subject.owner.id"              => participant.repository&.owner_id,
          "subject.repository.id"         => participant.repository_id,
          "subject.repository.writable"   => participant.repository&.writable?,
          "subject.repository.owner.id"   => participant.repository&.owner_id,
          "subject.repository.owner.type" => participant.repository_owner&.class.name,
        )
      end
    end
  end
end
