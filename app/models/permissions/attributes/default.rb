# frozen_string_literal: true
require "authzd-client"

module Permissions
  module Attributes
    class Default
      attr_reader :participant

      def initialize(participant)
        @participant = participant
      end

      def subject_attributes
        subject_type = participant.try(:user_role_target_type)
        subject_type ||= participant.class.name

        attrs = {
          "subject.type" => subject_type,
          "subject.id"   => participant.id,
        }

        if participant.try(:repository)
          attrs["custom_roles.enabled"] = GitHub.flipper[:custom_roles].enabled?(participant.repository&.owner)
        end

        attrs
      end

      def serialized_subject_attributes
        attrs = []
        subject_attributes.each do |key, value|
          attrs << Authzd::Proto::Attribute.wrap(key, value)
        end
        attrs
      end
    end
  end
end
