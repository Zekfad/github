# frozen_string_literal: true

module Permissions
  module Attributes
    class IssueComment < Default
      def subject_attributes
        super.merge(
          "subject.author.id"             => participant.user_id,
          "subject.owner.id"              => participant.repository.owner_id,
          "subject.repository.archived"   => participant.repository.archived?,
          "subject.repository.owner.id"   => participant.repository.owner_id,
          "subject.repository.owner.type" => participant.repository.owner.class.name,
          "subject.repository.id"         => participant.repository.id,
        )
      end
    end
  end
end
