# frozen_string_literal: true

module Permissions
  module Attributes
    class PackageRegistryPackage < Default
    end
  end
end
