# frozen_string_literal: true

module Permissions

  class Service

    GRANT_PERMISSIONS_BATCH_SIZE  = 100
    REVOKE_PERMISSIONS_BATCH_SIZE = 100
    UPDATE_PERMISSIONS_BATCH_SIZE = 100

    def self.async_can?(actor, action, subject)
      ::Platform::Loaders::Permission.load(actor, subject).then { |max_action|
        if max_action
          max_action >= Ability.actions[action]
        else
          false
        end
      }
    end

    def self.grant_permissions(rows = [], stats_key: nil)
      return if rows.empty?

      rows.in_groups_of(GRANT_PERMISSIONS_BATCH_SIZE, false) do |batch_rows|
        ApplicationRecord::Iam.github_sql.run <<-SQL, rows: GitHub::SQL::ROWS(batch_rows)
          INSERT IGNORE INTO permissions
          (actor_id, actor_type, action, subject_id, subject_type,
           priority, parent_id, created_at, updated_at, expires_at)
          VALUES :rows
        SQL

        if stats_key
          GitHub.dogstats.histogram "permissions.granted.#{stats_key}", rows.size
        end
      end

      true
    end

    def self.grant_ability_derived_permission(ability_id:, actor:, subject:, action:)
      attributes = app_attributes(actor: actor, subject: subject, action: action)
      attributes << ability_id

      ApplicationRecord::Iam.github_sql.run <<-SQL, row: GitHub::SQL::ROWS([attributes])
        INSERT IGNORE INTO permissions
        (actor_id, actor_type, action, subject_id, subject_type,
         priority, parent_id, created_at, updated_at, expires_at, ability_id)
        VALUES :row
      SQL

      true
    end

    def self.update_permission_action(actor_id:, actor_type:, subject_id:, subject_type:, action:, priority:)
      args = {
        actor_id: actor_id,
        actor_type: actor_type,
        subject_id: subject_id,
        subject_type: subject_type,
        action: action,
        priority: priority,
      }

      ApplicationRecord::Iam.github_sql.run(<<-SQL, args)
        UPDATE permissions
        SET action = :action
        WHERE actor_id = :actor_id
        AND actor_type = :actor_type
        AND subject_id = :subject_id
        AND subject_type = :subject_type
        AND priority = :priority
      SQL
    end

    def self.update_action_for_permissions(actor_id:, actor_type:, subject_types: [], action:)
      return if subject_types.empty?

      args = {
        actor_id: actor_id,
        actor_type: actor_type,
        subject_types: subject_types,
        action: Ability.actions[action],
      }

      ids = ApplicationRecord::Iam.github_sql.results(<<-SQL, args)
        SELECT id from permissions
        WHERE actor_id = :actor_id
        AND actor_type = :actor_type
        AND subject_type IN :subject_types
      SQL

      ids.flatten(1).in_groups_of(UPDATE_PERMISSIONS_BATCH_SIZE, false) do |batched_ids|
        throttle_writes_in_background_with_retry do
          ApplicationRecord::Iam.github_sql.run(<<-SQL, ids: batched_ids, action: args[:action])
            UPDATE permissions
            SET action = :action
            WHERE id IN :ids
          SQL
        end
      end
    end

    def self.update_expires_at_for_permissions(permission_ids: nil, timestamp: nil)
      return unless timestamp.present?
      Array(permission_ids).in_groups_of(::Permissions::Service::UPDATE_PERMISSIONS_BATCH_SIZE, false) do |batched_ids|
        throttle_writes_in_background_with_retry do
          ApplicationRecord::Iam.github_sql.run <<-SQL, expires_at: timestamp.to_i, ids: batched_ids
            UPDATE permissions
            SET expires_at = :expires_at
            WHERE id IN :ids
          SQL
        end
      end
    end

    def self.revoke_permissions(actor_id:, actor_type:, subject_id:, subject_type:, action:, priority:)
      args = {
        actor_id: actor_id,
        actor_type: actor_type,
        subject_id: subject_id,
        subject_type: subject_type,
        action: action,
        priority: priority,
      }

      ApplicationRecord::Iam.github_sql.run(<<-SQL, args)
        DELETE FROM permissions
        WHERE actor_id = :actor_id
        AND actor_type = :actor_type
        AND subject_id = :subject_id
        AND subject_type = :subject_type
        AND action = :action
        AND priority = :priority
      SQL
    end

    def self.revoke_permissions_granted_on_actor(actor_id:, actor_type:, subject_types: [])
      args = { actor_id: actor_id, actor_type: actor_type }

      sql = <<-SQL
        SELECT id FROM permissions
        WHERE actor_id = :actor_id
        AND actor_type = :actor_type
      SQL

      unless subject_types.empty?
        args[:subject_types] = subject_types

        sql += <<-SQL
          AND subject_type IN :subject_types
        SQL
      end

      ids = ApplicationRecord::Iam.github_sql.results(sql, args)

      ids.flatten(1).in_groups_of(REVOKE_PERMISSIONS_BATCH_SIZE, false) do |batched_ids|
        throttle_writes_in_background_with_retry do
          ApplicationRecord::Iam.github_sql.run(<<-SQL, ids: batched_ids)
            DELETE FROM permissions
            WHERE id IN :ids
          SQL
        end
      end
    end

    def self.revoke_permissions_granted_on_subject(actor_id: nil, actor_type: nil, subject_id:, subject_types:)
      revoke_permissions_granted_on_subjects(
        actor_id:      actor_id,
        actor_type:    actor_type,
        subject_ids:   Array(subject_id),
        subject_types: Array(subject_types),
      )
    end

    def self.revoke_permissions_granted_on_subjects(actor_id: nil, actor_type: nil, subject_ids:, subject_types:)
      args = { subject_types: Array(subject_types) }

      sql = <<-SQL
        DELETE FROM permissions
        WHERE subject_id IN :subject_ids
        AND subject_type IN :subject_types
      SQL

      if actor_id.present? && actor_type.present?
        sql += <<-SQL
          AND actor_id = :actor_id
          AND actor_type = :actor_type
        SQL

        args[:actor_id]   = actor_id
        args[:actor_type] = actor_type
      end

      Array(subject_ids).in_groups_of(REVOKE_PERMISSIONS_BATCH_SIZE, false) do |ids|
        args[:subject_ids] = ids
        ApplicationRecord::Iam.github_sql.run(sql, args)
      end
    end

    def self.actor_ids_granted_permission(actor_type:, subject_type:, subject_ids:, action:)
      args = {
        priority: Ability.priorities[:direct],
        actor_type: actor_type,
        subject_type: subject_type,
        subject_ids: Array(subject_ids),
        action: action,
      }

      ApplicationRecord::Iam.github_sql.values(<<-SQL, args)
        SELECT actor_id
        FROM permissions
        WHERE priority = :priority
        AND actor_type = :actor_type
        AND subject_type = :subject_type
        AND subject_id IN (:subject_ids)
        AND action IN (:action)
      SQL
    end

    def self.subject_ids_granted_permission(actor_ids:, actor_type:, subject_type:, action:)
      args = {
        priority: Ability.priorities[:direct],
        actor_ids: Array(actor_ids),
        actor_type: actor_type,
        subject_type: subject_type,
        action: Array(action),
      }

      ApplicationRecord::Iam.github_sql.values(<<-SQL, args)
        SELECT subject_id
        FROM permissions
        WHERE priority = :priority
        AND actor_id IN (:actor_ids)
        AND actor_type = :actor_type
        AND subject_type = :subject_type
        AND action IN (:action)
      SQL
    end

    def self.has_direct_permission?(actor_id:, actor_type:, subject_type:, subject_ids:, action:)
      actor_ids_granted_permission(
        actor_type: actor_type,
        subject_type: subject_type,
        subject_ids: subject_ids,
        action: action,
      ).include?(actor_id)
    end

    # Public: grant a Permission of action between actor and subject
    #
    # actor    - An IntegrationInstallation
    # subject  - Resource permission is granting access to
    # action   - Action available to acctor on resource
    #
    # Returns nil.
    def self.grant_app_permission(actor:, subject:, action:)
      attributes = app_attributes(actor: actor, subject: subject, action: action)
      grant_permissions([attributes])
    end

    # Public: delete direct app permissions in the Collab cluster.
    # Use this method to wrap calls to delete FGP abilities records for
    # IntegrationInstallations.
    #
    # permissions - Array of Ability records.
    #
    # Returns nil.
    def self.delete_app_permissions(permissions:)
      ApplicationRecord::Iam.transaction do
        permissions.each do |permission|
          Permissions::Service.revoke_permissions(
            actor_id: permission.actor_id,
            actor_type: permission.actor_type,
            subject_id: permission.subject_id,
            subject_type: permission.subject_type,
            action: Ability.actions[permission.action],
            priority: Ability.priorities[permission.priority],
          )
        end
      end
    end

    # Public: builds an array of values representing a SQL row for insertion
    # into the permissions table, for a GitHub App.
    #
    # Returns an Array.
    def self.app_attributes(actor:, subject:, action:, priority: nil, expires_at: nil)
      [
        actor.id,
        actor.ability_type,
        Ability.actions[action],
        subject.ability_id,
        subject.ability_type,
        Ability.priorities[priority || :direct],
        0, # No need for a parent ID that points to an actual ability record.
        GitHub::SQL::NOW,
        GitHub::SQL::NOW,
        (expires_at || GitHub::SQL::NULL),
      ]
    end

    def self.throttle_writes_in_background_with_retry(&block)
      return yield if GitHub.foreground?
      ApplicationRecord::Iam.throttle_writes_with_retry(max_retry_count: 5, &block)
    end
  end
end
