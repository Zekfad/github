# frozen_string_literal: true

module Permissions
  module Granters
    class RoleGranter

      attr_reader :role_name, :actor, :target, :target_type

      BASE_ROLE_TO_ABILITY = {
        "read": :read,
        "triage": :read,
        "write": :write,
        "maintain": :write,
      }.with_indifferent_access

      class GrantFailure < StandardError; end

      # Public: Initializes a RoleGranter.
      #
      # actor - A Team or User to grant or revoke a role for.
      # target - A Repository or Organization grant a role on.
      # role_name - The String name of a Role.
      def initialize(actor:, target: nil, role_name: nil)
        @actor = actor
        @target = target
        @target_type = target&.user_role_target_type
        @role_name = role_name
      end

      # Public: Grants a role for the specified actor on the specified target.
      #
      # Returns a RoleGrantResult.
      # Raises an ArgumentError if required arguments are missing.
      # Raises a GrantFailure if granting was not successful.
      def grant!
        raise_unless_args_present!
        verify_grantable_role!
        result = grant_user_role
        raise GrantFailure.new(result.reason) unless result.success?
        result
      end

      # Public: Revokes a role for the specified actor on the specified target.
      #
      # Returns a RoleGrantResult.
      # Raises an ArgumentError if required arguments are missing.
      # Raises a GrantFailure if revoking was not successful.
      def revoke!
        raise_unless_args_present!
        result = revoke_user_role
        raise GrantFailure.new(result.reason) unless result.success?
        result
      end

      # Public: Revokes a role for the specified actor on the specified target,
      #         if a UserRole exists for the actor on the target.
      #
      # Returns a RoleGrantResult.
      # Raises an ArgumentError if required arguments are missing.
      # Raises a GrantFailure if revoking was not successful.
      def revoke_if_exists!
        raise_unless_args_present!
        result = revoke_user_role_if_exists
        raise GrantFailure.new(result.reason) unless result.success?
        result
      end

      private

      # Private: Validate that all required arguments for granting a role are set.
      #
      # Returns nothing.
      # Raises ArgumentError if required arguments are missing.
      def raise_unless_args_present!
        raise ArgumentError.new("Missing required arguments: #{missing_args.join(', ')}") if missing_args?
      end

      # Private: Checks to see if all required arguments for granting a role are set.
      #
      # Returns a Boolean.
      def missing_args?
        missing_args.any?
      end

      # Private: The required arguments for granting a role that are missing.
      #
      # Returns an Array[Symbol].
      def missing_args
        missing_args = []
        missing_args << :actor unless actor.present?
        missing_args << :target unless target.present?

        missing_args
      end

      # Private: Loads the Role that is to be granted.
      #
      # Returns a Role|nil.
      def role
        @role ||= begin
          return unless role_name.present?

          role = if Role::SYSTEM_ROLES.include?(role_name)
            Platform::Loaders::Permissions::PresetRole.load(name: role_name).sync
          elsif target.present? && target.is_a?(Repository)
            Role.where(name: role_name, owner_id: target.owner.id, owner_type: "Organization").first
          end

          raise ArgumentError.new("Must pass a valid Role") unless role
          raise ArgumentError.new("Owner must be an Organization") if role.owner_must_be_organization? && !target.owner.organization?

          role
        end
      end

      def verify_grantable_role!
        return unless target.owner.organization?
        return unless actor.is_a?(User) && target.owner.direct_member?(actor)
        return unless role.present?

        target_role_name =
          if role.custom?
            role.base_role.name
          else
            role.name
          end

        default_perm = target.owner.default_repository_permission

        if !Role.target_greater_than_other_role?(target: target_role_name, other_role: default_perm)
          raise GrantFailure.new("Invalid role. You cannot assign members a role with lower permission level than the base repository role #{default_perm}.")
        end
      end

      # Private: Loads the existing UserRole for the actor and target.
      #
      # Returns a UserRole|nil.
      def existing_user_role
        @existing_user_role ||=
        if role.present?
          UserRole.find_by(role: role, actor: actor, target_type: target_type, target_id: target.id)
        else
          UserRole.find_by(actor: actor, target_type: target_type, target_id: target.id)
        end
      end

      # Private: Grants a role for the specified user on the specified target.
      # Also grants the associated ability record, if there's a base role associated.
      #
      # Returns a RoleGrantResult.
      def grant_user_role
        Role.transaction do
          user_role = UserRole.new(role: role, actor: actor, target_type: target_type, target_id: target.id)
          role_name = role.custom? ? "custom_role" : role.name

          if role.base_role
            ability_to_grant = BASE_ROLE_TO_ABILITY[role.base_role.name]
            Role.grant_legacy_permissions(actor, ability_to_grant, target)
          end

          if user_role.save!
            GitHub.dogstats.increment("user_roles.granted", tags: ["result:success", "actor_type:#{actor.class.name}", "role:#{role_name}"])
            RoleGrantResult.success!
          else
            GitHub.dogstats.increment("user_roles.granted", tags: ["result:fail", "actor_type:#{actor.class.name}", "role:#{role_name}"])
            RoleGrantResult.failure!(reason: "Failed to grant role: #{role_name} - #{user_role.errors.full_messages.to_sentence}")
          end
        end
      end

      # Private: Revokes a role for the specified user on the specified target.
      #
      # Returns a RoleGrantResult.
      def revoke_user_role
        revoked_role_name = existing_user_role.role.custom? ? "custom_role" : existing_user_role.role.name

        if existing_user_role&.destroy
          GitHub.dogstats.increment("user_roles.revoked", tags: ["role_name:#{revoked_role_name}", "result:success", "actor_type:#{actor.class.name}"])
          RoleGrantResult.success!
        else
          GitHub.dogstats.increment("user_roles.revoked", tags: ["role_name:#{revoked_role_name}", "result:fail", "actor_type:#{actor.class.name}"])
          RoleGrantResult.failure!(reason: "Failed to revoke role: #{role_name}")
        end
      end

      # Private: Revokes a role for the specified user on the specified target,
      #          if the user has already been granted the role.
      #
      #          The returned result will be successful if the user does not
      #          have the role granted, or if revoking the role succeeds. If
      #          revoking the role fails, then the result will be a failure.
      #
      # Returns a RoleGrantResult.
      def revoke_user_role_if_exists
        return RoleGrantResult.success! unless existing_user_role.present?
        revoke_user_role
      end
    end

    class RoleGrantResult
      attr_reader :reason, :success

      def initialize(reason: nil, success:)
        @reason = reason
        @success = success
      end

      def self.failure!(reason:)
        new(reason: reason, success: false)
      end

      def self.success!
        new(success: true)
      end

      def success?
        success
      end
    end
  end
end
