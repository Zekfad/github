# rubocop:disable Style/FrozenStringLiteralComment

require "scientist"

# This class was deprecated in favor of a database-backed
# pull request review thread model that will be introduced
# in the future.
#
# See #79020 and #79074 for more information.
class DeprecatedPullRequestReviewThread
  attr_accessor :start_side

  include GitHub::Relay::GlobalIdentification

  # Experiment for better threads
  extend Scientist

  # Public: Convert a flat list of comments into a list of threads.
  #
  # pull     - The parent PullRequest
  # comments - An Array of PullRequestReviewComment records.
  #
  # Returns an Array of DeprecatedPullRequestReviewThreads.
  def self.group_comments(pull, comments)
    comments_in_threads = split_comments_into_threads(comments)

    Promise.all(comments_in_threads.map { |comments| comments.first.async_original_pull_request_comparison }).sync

    comments_in_threads.map do |comments_in_thread|
      new(
        pull: pull,
        pull_comparison: comments_in_thread.first.original_pull_request_comparison,
        path: comments_in_thread.first.path,
        position: comments_in_thread.first.original_position,
        comments: comments_in_thread,
      )
    end
  end

  # Public: Split a list of comments into multiple lists containing
  # only comments belonging to the same thread.
  #
  # comments - An `Array` of `PullRequestReviewComment` records.
  #
  # Returns an `Array` of `Array`s of `PullRequestReviewComment`s.
  def self.split_comments_into_threads(comments)
    # This will hold the mapping from comment id to a "thread", which at this point
    # is just an Array of `PullRequestReviewComment`. Note that the same array will
    # be shared among all comments that belong to the same thread.
    comment_id_to_thread = {}

    # First, find all legacy comments (those that don't belong to a review)
    legacy_thread_comments, newstyle_comments = comments.partition { |comment| comment.pull_request_review_id.nil? }

    # Group legacy comments according to the legacy grouping logic, doh!
    legacy_thread_comments_in_threads = legacy_thread_comments.group_by(&:thread_grouping_id).values
    legacy_thread_comments_in_threads.each do |thread_comments|
      # Build the association between comments and their thread
      thread_comments.each do |comment|
        comment_id_to_thread[comment.id] = thread_comments
      end
    end

    newstyle_comments.each do |comment|
      # If the comment is a reply, we append it to the existing thread.
      # Otherwise, we just create a new thread.
      comment_id_to_thread[comment.id] = if comment.reply_to_id
        # There might be data quality issues where the replied to comment
        # does no longer exist and we might not actually have a list of thread comments.
        # In this case, we create a new empty list. This will ensure that all replies
        # to the no longer existing comment will still be grouped together.
        (comment_id_to_thread[comment.reply_to_id] ||= []).concat([comment])
      else
        [comment]
      end
    end

    threads = comment_id_to_thread.values.uniq
    threads.map { |comments_in_thread|
      comments_in_thread.sort_by { |comment| [comment.created_at, comment.id] }
    }.sort_by { |comments_in_thread|
      first_comment = comments_in_thread.first
      [first_comment.created_at, first_comment.id]
    }
  end

  # Split a thread into several sub-threads.
  #
  # A single legacy thread, and many new reply threads, may exist on a diff
  # line. The legacy thread contains all comments created in the
  # pre-reply_to_id era when it was assumed every comment on a line belonged
  # to the same thread.
  #
  # A new reply comment should be allowed on a legacy thread, so the threads
  # are built as usual with `group_comments` and then split based on legacy
  # ids or new reply ids.
  #
  # Returns an Array of PullRequestReviewComment.
  def self.split_into_legacy_and_review_threads(comments)
    # Locate first comment, if any, that begins the single legacy thread.
    leader = comments.find(&:legacy_comment?)

    # Build sub-threads from legacy and new-style reply comments.
    comments.group_by do |comment|
      # All legacy comments belong to the single legacy thread on this line.
      if comment.legacy_comment?
        leader.id
      # New-style comments use explicit reply_to_id to identify thread.
      else
        comment.reply_to_id || comment.id
      end
    end.values
  end

  # Public: Convert a flat list of comments into a list of threads using the comments grouping ID.
  #
  # See `PullRequestReviewComment#thread_grouping_id`
  #
  # pull     - The parent PullRequest
  # comments - An Array of PullRequestReviewComment records.
  #
  # Returns an Array of DeprecatedPullRequestReviewThreads.
  def self.build_for(pull, comments)
    comments = comments.sort_by(&:created_at)

    threads = comments.group_by(&:thread_grouping_id).map do |_, comments|
      thread_starting_comment = comments.first

      new(
        pull: pull,
        pull_comparison: thread_starting_comment.original_pull_request_comparison,
        path: thread_starting_comment.path,
        position: thread_starting_comment.original_position,
        comments: comments,
      )
    end

    threads.sort_by(&:created_at)
  end

  # Create a new `DeprecatedPullRequestReviewThread`.
  #
  # pull - optional `PullRequest` that this thread is a part of
  # pull_comparison - `PullRequest::Comparison`
  # path - a String of the filename path
  # position - Integer position of the offset in the diff
  # comments - list of `PullRequestReviewComment`s in this thread
  def initialize(pull: nil, pull_comparison:, path: nil, position: nil,
      line: nil, side: nil, start_diff_line: nil, end_diff_line: nil, comments: [])
    @pull = pull || pull_comparison.pull
    @pull_comparison = pull_comparison

    if path.nil? || path.is_a?(String)
      @path = path
    else
      raise TypeError, "expected path to be a String, but was #{path.class}"
    end

    if position.nil? || position.is_a?(Integer)
      @position = position
    else
      raise TypeError, "expected position to be a Integer, but was #{position.class}"
    end

    @line = line unless line.nil?
    @side = side unless side.nil?

    @start_diff_line = start_diff_line unless start_diff_line.nil?
    @end_diff_line = end_diff_line unless end_diff_line.nil?

    @comments = comments || []
    @comments.each { |c| c.thread = self }
  end

  attr_reader :pull, :pull_comparison, :path, :position
  attr_accessor :comments
  alias_method :pull_request, :pull

  def new_record?
    id.nil?
  end

  def id
    first_comment.try(:id)
  end

  def created_at
    first_comment.try(:created_at)
  end

  # See IssueTimeline
  def timeline_sort_by
    [created_at]
  end

  # Returns the children items for the purposes of rendering thread(s) on a timeline -
  # i.e. on PullRequest#show
  #
  # See also IssueTimeline#child_enumerator
  #
  # Returns PullRequestReviewComments or CommitComments
  def timeline_children
    comments
  end

  # Returns the number of unique authors in this thread.
  def author_count
    @author_count ||= begin
      authors = Set.new
      @comments.each do |c|
        authors << c.user
      end
      authors.size
    end
  end

  def old_style_review_thread?
    comments.first && comments.first.pull_request_review_id.nil?
  end

  # Create a new thread that clones the pull comparison, path, and position
  # of a pre-existing DeprecatedPullRequestReviewThread. Comments are empty so the thread
  # identifies itself as a new record and the `in_reply_to` form field is not sent.
  #
  # TODO This should not be needed when the inline_comment_form partial is cleaned up.
  def clone_without_comments
    self.class.new(
      pull_comparison: pull_comparison,
      path: path,
      position: position,
      line: line,
      side: side,
    )
  end

  def to_param
    id
  end

  def sort_key
    [path, position, id]
  end

  def reply_to_id
    first_comment&.id
  end

  def repository
    pull.repository
  end

  def legacy_thread?
    first_comment.legacy_comment?
  end

  def locked_for?(user)
    pull.locked_for?(user)
  end

  def visible_to?(viewer)
    comments.any? { |comment| comment.visible_to?(viewer) }
  end

  def comments_for(viewer)
    return @comments_for[viewer] if defined?(@comments_for)

    @comments_for = Hash.new do |hash, key|
      hash[key] = comments.select { |comment| comment.visible_to?(key) }
    end
    @comments_for[viewer]
  end

  # Preload the diff_entry data for a collection of threads.
  #
  # diff_entry is used to enable diff expansion and syntax highlighting on the
  # PR discussion page. If diff_entry is nil, diff expansion and syntax
  # highlighting will be disabled.
  #
  # In some cases this can take up a lot of time loading
  # the diffs, so we load as many as we can within the
  # time limit and disable the rest. Live threads are prioritized.
  def self.preload_diff_entries(threads)
    threads = threads.partition(&:live?).flatten

    GitHub::SafeTimer.timeout(1) do |timer|
      while timer.run? && (thread = threads.first)
        thread.diff_entry
        threads.shift
      end
    end
  ensure
    threads.each(&:disable_diff_entry)
  end

  def platform_type_name
    "PullRequestReviewThread"
  end

  def resolved?
    return @resolved if defined?(@resolved)
    @resolved = first_comment.pull_request_review_thread.resolver.present?
  end

  def resolved_by
    if resolved?
      first_comment.pull_request_review_thread.resolver
    end
  end

  def supports_multiple_threads_per_line?
    true
  end

  def ==(other)
    super || (other.instance_of?(self.class) && !id.nil? && id == other.id)
  end
  alias_method :eql?, :==

  def hash
    if id
      self.class.hash ^ id.hash
    else
      super
    end
  end

  # Public: The line number on which this thread is situated. blob position is 0 based offset, this
  # is 1 based offset, like normal file lines.
  def line
    return @line if defined?(@line)

    # Old comments don't have a blob position without extracting it via LegacyPositionData.
    # Ideally this property is populated by calling `async_position_data.then(&:blob_position)`
    # but that doesn't exist yet.
    return nil unless first_comment && blob_position
    @line = blob_position + 1
  end

  # Public: The GitHub::Diff::Line object that a multi-line comment starts on.
  #
  # Returns a GitHub::Diff::Line or nil if the comment failed to be positioned
  #         in the diff, or if the comment is not a multi-line comment.
  def start_diff_line
    return @start_diff_line if defined?(@start_diff_line)

    return nil unless first_comment && blob_position
    @start_diff_line = first_comment.async_start_line.sync
  end

  def start_line_type
    return nil unless start_diff_line
    case start_diff_line.type
    when :addition then "+"
    when :deletion then "-"
    else
      ""
    end
  end

  # Public: The GitHub::Diff::Line object that a multi-line comment ends on.
  #
  # Returns a GitHub::Diff::Line or nil if the comment failed to be positioned
  #         in the diff, or if the comment is not a multi-line comment.
  def end_diff_line
    return @end_diff_line if defined?(@end_diff_line)

    return nil unless first_comment && blob_position
    @end_diff_line = first_comment.async_end_line.sync
  end

  def end_line_type
    return nil unless end_diff_line
    case end_diff_line.type
    when :addition then "+"
    when :deletion then "-"
    else
      ""
    end
  end

  # Public: the side of the diff (removal or addition/context) on which the comment was left.
  def side
    return @side if defined?(@side)

    return nil unless first_comment && blob_position
    @side = left_blob ? :left : :right
  end

  def activerecord_pull_request_review_thread
    first_comment.pull_request_review_thread
  end

  # These seem like they belong on the thread itself rather than on each comment.
  delegate :live?, :outdated?, :pull_request_review, :pull_request_review_id,
    :excerpt_starting_offset, :excerpt_line_enumerator,
    :diff_hunk_lines, :diff_entry, :disable_diff_entry, :current_diff_entry,
    :blob_position, :left_blob, :async_start_line,
    to: :first_comment

  protected def first_comment
    comments.first
  end
end
