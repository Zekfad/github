# frozen_string_literal: true

class TopicImporter
  TOPICS_PATH = "topics"
  REPO_OWNER = "github"
  REPO_NAME = "explore"
  INDEX_FILE_NAME = "index.md"

  attr_reader :errors

  # Public: Returns the name and owner of the repository where topics data is expected to live.
  def self.repository_name_with_owner
    "#{REPO_OWNER}/#{REPO_NAME}"
  end

  def initialize
    @errors = []
    @topics = {}
    @changesets = []
  end

  # Public: Create and update Topic records from the latest data in the github/explore repo. Any
  # topic that is no longer in github/explore will have its curated content wiped.
  #
  # dry_run - set to true if no writes should be made; useful for previewing changes
  # topic_names - a list of String topic names that should be updated; ignored when dry_run is true;
  #               only used to determine setting curated content, not wiping it
  #
  # Returns nothing.
  def import(dry_run:, topic_names: [])
    unless topics_repository
      @errors << "Missing public repository #{self.class.repository_name_with_owner}"
      return
    end

    unless latest_sha
      @errors << "Could not determine latest commit for #{topics_repository.nwo} in " +
                 "#{topics_repository.default_branch} branch"
      return
    end

    topic_dirs_to_update = all_topic_directories
    unless dry_run
      topic_dirs_to_update = topic_dirs_to_update.select { |dir| topic_names.include?(dir.name) }
    end
    topic_dirs_to_update.each do |topic_dir|
      import_topic_from_directory(topic_dir, dry_run: dry_run)
    end

    names_to_keep = topic_names_to_keep(all_topic_directories)
    remove_curated_content_except(names_to_keep: names_to_keep, dry_run: dry_run)
  end

  # Public: Returns topic changesets for new topics that are not yet in the database.
  #
  # Returns an Array of TopicChangesets.
  def new_topic_changesets
    @changesets.select(&:new?)
  end

  # Public: Returns topic changesets for existing topics that are in the database but have changes.
  #
  # Returns an Array of TopicChangesets.
  def updated_topic_changesets
    @changesets.select(&:changed?)
  end

  # Public: Returns a count of how many new topics will be created by this import.
  #
  # Run #import with dry_run: true first.
  #
  # Returns an integer.
  def new_count
    new_topic_changesets.size
  end

  # Public: Returns a count of how many existing topics will be updated by this import.
  #
  # Run #import with dry_run: true first.
  #
  # Returns an integer.
  def updated_count
    updated_topic_changesets.size
  end

  # Public: Returns true if any new topics will be created by this import.
  #
  # Run #import with dry_run: true first.
  #
  # Returns a Boolean.
  def any_new?
    new_count > 0
  end

  # Public: Returns true if any existing topics will be updated by this import.
  #
  # Run #import with dry_run: true first.
  #
  # Returns a Boolean.
  def any_updates?
    updated_count > 0
  end

  # Public: Returns true if any topics will be created or updated by this import.
  #
  # Run #import with dry_run: true first.
  #
  # Returns a Boolean.
  def any_changes?
    any_new? || any_updates?
  end

  private

  def topics_repository
    @topics_repository ||= Repository.public_scope.with_name_with_owner(REPO_OWNER, REPO_NAME)
  end

  # Private: Returns a list of topic names that should not have their curated content removed.
  # Run this after new and updated topics have been determined.
  #
  # topic_dirs - a list of topic directories from the github/explore repository
  #
  # Returns an Array of Strings.
  def topic_names_to_keep(topic_dirs)
    topic_dirs.map(&:name) | @changesets.map { |changeset| changeset.topic.name }
  end

  # Private: Determines which topics are curated but not in the given list of names to keep, and
  # wipes their curated content fields.
  def remove_curated_content_except(names_to_keep:, dry_run:)
    all_curated_topic_names = Topic.curated.pluck(:name)
    topic_names_to_decurate = all_curated_topic_names - names_to_keep
    return if topic_names_to_decurate.empty?

    topic_names_to_decurate.each_slice(100) do |names_batch|
      remove_curated_content_from(names_batch, dry_run: dry_run)
    end
  end

  # Private: Given a list of topic names, this will remove the curated content from those topics.
  def remove_curated_content_from(topic_names, dry_run:)
    topics = Topic.where(name: topic_names)
    if dry_run
      topics.each do |topic|
        changeset = TopicChangeset.new(topic)
        Topic::CURATED_FIELDS.each do |field|
          topic.send("#{field}=", nil)
        end
        topic.featured = false
        changeset.determine_changes
        @changesets << changeset
      end
    else
      nilled_fields = Topic::CURATED_FIELDS.map { |field| [field, nil] }.to_h
      topics.update_all(nilled_fields.merge(featured: false))
    end
  end

  def import_topic_from_directory(topic_dir, dry_run:)
    topic_name = topic_dir.name
    metadata, description = read_topic_index_file(topic_name)

    unless metadata.present? && description.present?
      @errors << "Could not read metadata and description for #{topic_name} " +
                 "from #{INDEX_FILE_NAME}"
      return
    end

    topic = Topic.find_by_name(topic_name) || Topic.new(name: topic_name)
    changeset = TopicChangeset.new(topic)

    topic.description = encode_value_if_present(description)
    topic.created_by = encode_value_if_present(metadata["created_by"])
    topic.display_name = encode_value_if_present(metadata["display_name"])
    topic.logo_url = logo_url_from(topic_name, metadata)
    topic.released = encode_value_if_present(metadata["released"])
    topic.short_description = encode_value_if_present(metadata["short_description"])
    topic.url = metadata["url"]
    topic.wikipedia_url = metadata["wikipedia_url"]
    topic.github_url = metadata["github_url"]

    changeset.determine_changes(metadata)
    anything_to_save = changeset.new? || changeset.changed?
    @changesets << changeset if anything_to_save

    if anything_to_save && !dry_run
      unless topic.save
        @errors.concat topic.errors.full_messages
      end

      topic.update_aliases_from_names(metadata["aliases"] || "")
      topic.update_related_topics_from_names(metadata["related"] || "")
    end

    @topics[topic.name] = topic
  end

  def latest_sha
    @latest_sha ||= topics_repository.heads.find(topics_repository.default_branch).try(:target_oid)
  end

  # Private: Returns a list of topic directories.
  #
  # Returns an Array of TreeEntry instances.
  def all_topic_directories
    return @all_topic_directories if @all_topic_directories
    _id, tree_entries, _truncated = topics_repository.tree_entries(latest_sha, TOPICS_PATH)
    @all_topic_directories = tree_entries.select { |entry| entry.type == "tree" }
  end

  def logo_url_from(topic_name, metadata)
    return unless metadata["logo"]

    topic_dir_path = File.join(TOPICS_PATH, topic_name)
    topic_dir = topics_repository.directory(latest_sha, topic_dir_path)
    return unless topic_dir

    tree_history = topic_dir.tree_history
    commit = tree_history.load_or_calculate_single_entry(metadata["logo"])
    return unless commit

    host = "#{GitHub.scheme}://#{GitHub.urls.raw_host_name}"
    path = "/#{topics_repository.nwo}/#{commit.oid}/#{TOPICS_PATH}/" \
           "#{topic_name}/#{metadata["logo"]}"
    host + path
  end

  def encode_value_if_present(value)
    return unless value.present?

    value = value.to_s

    if value.respond_to?(:force_encoding) && value.encoding != ::Encoding::UTF_8
      value.force_encoding("UTF-8")
    end

    value
  end

  def read_topic_index_file(topic_name)
    path = File.join(TOPICS_PATH, topic_name, INDEX_FILE_NAME)
    entry = topics_repository.blob(latest_sha, path)
    return unless entry && entry.data

    parts = entry.data.split("---", 3)
    return unless parts.length == 3

    _, yaml, body = parts
    metadata = YAML.safe_load(yaml)
    [metadata, body.strip]
  end
end
