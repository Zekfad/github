# rubocop:disable Style/FrozenStringLiteralComment

class StaffNote < ApplicationRecord::Domain::Users
  include GitHub::UserContent

  belongs_to :user
  belongs_to :notable, polymorphic: true

  validates_presence_of :user, :notable, :note
  validates :body, unicode3: true

  alias_attribute :body, :note
  alias_attribute :to_s, :note

  # Pushes this StaffNote to the Audit service.
  #
  # audit_service - Optional Audit::Service.  Default: GitHub.audit.
  #
  # Returns nothing.
  def audit!(audit_service = nil)
    (audit_service || GitHub.audit).log("note") do
      log = { actor_id: user_id, note: note_text}
      case notable
      when Repository then log[:repo_id] = notable_id
      when User then log[:user_id] = notable_id
      end
      log
    end
  end

  def note_text
    YAML.parse(note)[:note].value
  rescue
    note.to_s
  end

  def creator_name
    (user || ::User.ghost).name
  end
end
