# rubocop:disable Style/FrozenStringLiteralComment

class ExternalIdentityAttribute < ApplicationRecord::Domain::Users
  belongs_to :external_identity

  # Internal: Generates a identifying key for the attribute.
  # Used by ExternalIdentityAttribute::AssociationExtension
  # to determine equivalency between identity attribute objects and
  # their Hash representations.
  def self.generate_key(scheme, name, value)
    key = [scheme, name, value].join(":")

    Digest::MD5.hexdigest(key)
  end

  def key
    self.class.generate_key(scheme, name, value)
  end

  # Public: Returns the metadata hash which is parsed from the serialized
  # backing field.
  #
  # Returns a Hash.
  def metadata
    return {} unless metadata_json.present?

    JSON.parse(metadata_json)
  end

  # Public: Saves the given metadata in the serialized JSON field.
  #
  # metadata    - The Hash of metadata to be serialized and stored or `nil` if the
  #               stored metadata should be cleared.
  #
  # Returns the set metadata.
  # Raises ArgumentError if passed metadata isn't a Hash or `nil`.
  def metadata=(metadata)
    case metadata
    when nil
      self.metadata_json = nil
    when Hash
      self.metadata_json = metadata.to_json
    else
      raise ArgumentError, "expected metadata to be nil or a Hash"
    end

    metadata
  end

  # Public: A hash representation of the attribute.
  #
  # Returns a Hash.
  def to_hash
    {
      "name"      => name,
      "value"     => value,
      "metadata"  => metadata,
    }
  end
end
