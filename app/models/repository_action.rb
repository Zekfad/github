# frozen_string_literal: true

class RepositoryAction < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model

  extend GitHub::Encoding
  force_utf8_encoding :name, :description

  self.table_name = "repository_actions"
  belongs_to :repository

  has_many :marketplace_categories_repository_actions, dependent: :destroy
  has_many :categories,
    class_name: "Marketplace::Category",
    through: :marketplace_categories_repository_actions,
    source: :marketplace_category,
    split: true

  has_many :regular_categories,
    -> { where(acts_as_filter: false) },
    class_name: "Marketplace::Category",
    through: :marketplace_categories_repository_actions,
    source: :marketplace_category,
    split: true

  has_many :filter_categories,
    -> { where(acts_as_filter: true) },
    class_name: "Marketplace::Category",
    through: :marketplace_categories_repository_actions,
    source: :marketplace_category,
    split: true

  enum state: {unlisted: 0, listed: 10, delisted: 20}

  MIN_STARS_FOR_SOCIAL_PROOF = 2
  DESCRIPTION_MAX_LENGTH = 125
  ENDING_PUNCTUATION_REGEX = /[\.,!?]+\z/
  # TODO: remove `azure` when `increase_actions_marketplace_visibility` is active for all users
  GITHUB_MICROSOFT_CREATORS = %w(
    actions
    Azure
    azure
    github
    microsoft
  )
  CUSTOM_ICON_PARTNERS = %w(
    actions
    ansible
    atlassian
    aws
    aws-actions
    axosoft
    azure
    blackducksoftware
    buildkite
    cloudflare
    codeclimate
    codecov
    codefresh-io
    coverallsapp
    docker
    getsentry
    google
    googlecloudplatform
    gorillastack
    hashicorp
    jfrog
    launchdarkly
    mablhq
    npm
    snyk
    sonarsource
    twilio-labs
    whitesource
  )
  ACTIONS_ORG_NAME = "actions"

  validates :path, :repository, :name, presence: true
  validates :path, uniqueness: { scope: [:repository_id], case_sensitive: true }
  validates :description, length: { maximum: DESCRIPTION_MAX_LENGTH }, if: :listed?
  validates_format_of :color, with: /\A[0-9a-f]{6}\z/iu
  validates_numericality_of :rank_multiplier, greater_than: 0
  validates_with RepositoryActions::SlugValidator, if: :slug_changed?
  validates :slug, presence: true, if: :listed?
  validate :validate_name_and_slug

  validate :validate_published_release, if: :listed?
  validates :security_email, presence: true, if: [:listed?, :owned_by_org?]

  delegate :owner, to: :repository

  has_many :repository_action_releases
  has_many :releases, through: :repository_action_releases

  has_many :published_action_releases,
    -> { where(published_on_marketplace: true) },
    class_name: "RepositoryActionRelease"
  has_many :published_releases,
    -> { published },
    through: :published_action_releases,
    source: :release

  before_validation :set_color
  before_validation :set_icon_name
  before_validation :remove_description_punctuation
  before_validation :set_slug, if: [:name_changed?, :listed?]

  after_commit :synchronize_search_index

  delegate :owner, to: :repository

  scope :featured, -> { where(featured: true) }

  # Actions inside of .github folders are generally not useful outside of their Repository.
  # This filters them out.
  scope :discoverable, -> { where("`#{self.table_name}`.path NOT LIKE (?)", ".github/%") }

  # Returns RepositoryActions that the given User owns or can manage because they're owned by an
  # Organization for which the given User is an admin.
  scope :adminable_by, -> (user) {
    where(repository_id: user.associated_repository_ids(min_action: :admin))
  }

  # Returns RepositoryActions that have not been listed in the Marketplace
  scope :not_in_marketplace, -> { where.not(state: :listed) }

  scope :with_name, -> (name) { where("CONVERT(repository_actions.name USING utf8mb4) LIKE ?", "%#{name}%") }
  scope :owned_by, -> (login) { joins(:repository).where("repositories.owner_login = ?", login) }
  scope :with_state, -> (state) { where(state: state) }

  scope :with_category, -> (slug) do
    category_ids = Marketplace::Category.for_slug(slug).ids
    joins("INNER JOIN marketplace_categories_repository_actions on marketplace_categories_repository_actions.repository_action_id = repository_actions.id")
      .where("marketplace_categories_repository_actions.marketplace_category_id": category_ids)
      .distinct
  end

  def verified_owner?
    async_verified_owner?.sync
  end

  def async_verified_owner?
    async_repository.then do |repository|
      repository.async_owner.then do |owner|
        owner.verified_for_repo_actions?
      end
    end
  end

  def delist_if_unreleased
    return unless listed? && repository_action_releases.published.empty?
    delisted!
  end

  def listed!
    set_slug
    super

    instrument :listed
  end

  def update_from_config!
    update!(config_from_metadata_file)
  end

  def delisted!
    super

    repository_action_releases.update_all(published_on_marketplace: false)
    instrument :delisted
  end

  # The color as specified in their metadata file will still be what's persisted
  # in the database. This is purely overriding the color as it is read.
  def color
    if partner_action?
      # blue for actions, white for everyone else
      repository && repository.owner_login.downcase == "actions" ? "0366d6" : "ffffff"
    else
      super
    end
  end

  def icon_color
    RepositoryActions::Colors::ICON_COLOR_MAPPING[color]
  end

  def sign_developer_agreement(actor:, agreement:, org: nil)
    return [agreement, false] unless agreement

    unless adminable_by?(actor)
      agreement.errors.add(:base, "Must have write access to sign for this Action")
      return [agreement, false]
    end

    if org.present? && !org.adminable_by?(actor)
      agreement.errors.add(:base, "Must have org admin access to sign for this Action")
      return [agreement, false]
    end

    if org.present?
      [agreement.sign(user: actor, organization: org), agreement]
    else
      [agreement.sign(user: actor), agreement]
    end
  end

  # Public: Returns true if the given legal agreement has been signed by the integrator. Will
  # check for a signature on the latest version of the integrator agreement if no particular
  # version is given. Defaults to false if no appropriate legal agreement exists.
  #
  # agreement - optional; a specific Marketplace::Agreement for integrators; will default to the
  #             latest version if none is specified
  #
  # Returns a Boolean.
  def has_signed_integrator_agreement?(user:, agreement: nil)
    agreement ||= Marketplace::Agreement.latest_for_integrators

    agreement && agreement.integrator? && agreement.signed_by?(user)
  end

  def org_has_signed_integrator_agreement?(org:, agreement: nil)
    return false if org.nil?

    agreement ||= Marketplace::Agreement.latest_for_integrators

    agreement && agreement.integrator? && agreement.signed_for?(org)
  end

  def base_path
    path == File.basename(path) ? "" : File.dirname(path)
  end

  def metadata_file
    oid = repository.ref_to_sha(repository.default_branch)
    repository.tree_entry(oid, path)
  rescue GitRPC::Error
    # GitRPC::InvalidFullOid will happen if default_branch doesn't exist.
    # GitRPC::NoSuchPath will happen if Dockerfile doesn't exist.
    nil
  end

  def config_from_metadata_file
    return {} unless metadata_file_entry = metadata_file
    repository.get_action_config_from_metadata_file(metadata_file_entry).slice(:name, :path, :description, :icon_name, :color)
  end

  def external_uses_path_prefix
    resolved_base_path = base_path
    # base path is an empty string, action is at root of the repo.
    return "#{repository.nwo}@" if resolved_base_path.empty?
    # base_path is a non-empty string, in a folder in the repo.
    "#{repository.nwo}/#{resolved_base_path}@"
  end

  def local_uses_path_prefix
    resolved_base_path = base_path
    # resolved_base_path = "" if resolved_base_path.length == 1
    return resolved_base_path[1..-1] if resolved_base_path.empty?
    resolved_base_path
  end

  def owned_by_org?
    repository.owner.organization?
  end

  def permalink(include_host: true)
    if include_host
      "#{GitHub.url}/marketplace/actions/#{slug}"
    else
      "/marketplace/actions/#{slug}"
    end
  end

  def self.ghost_action(default_title)
    {
      name: default_title,
      color: "ffffff",
      icon_color: "23292e",
      # This is used to identify this magic hash in Platform::Objects::RepositoryAction
      ghost_action: true,
    }
  end

  def self.docker_action
    {
      name: "Docker",
      color: "0366d6",
      icon_name: "package",
      icon_color: "ffffff",
      # This is used to identify this magic hash in Platform::Objects::RepositoryAction
      docker_action: true,
    }
  end

  def self.normalize_path(base_path:)
    return "Dockerfile" if base_path.to_s.empty?
    "#{base_path}/Dockerfile"
  end

  def self.from_external_id(external_id: , repository_id: nil, ghost_allowed: false, default_title: nil)
    found_action = with_external_id(external_id: external_id, repository_id: repository_id)
    found_action ||= ghost_action(default_title) if ghost_allowed
    found_action
  end

  def self.with_external_id(external_id: , repository_id: nil)
    return docker_action if external_id.starts_with?("docker://")
    if external_id.starts_with?("./")
      return unless repository_id

      clean_external_id = external_id.chomp("/")
      clean_external_id.gsub!("./", "")

      file_path = "#{clean_external_id}/Dockerfile"
      # If the external_id starts with "./",
      # it is a local reference to an action within the same repository.
      return RepositoryAction.where(path: file_path, repository_id: repository_id).first
    end

    # anything that is not a local action reference, we need
    # an external_id of the format
    # uses = "<user>/<repo>@<commit-ish>" /* path defaults to "/" */
    # uses = "<user>/<repo>/<path>@<commit-ish>"
    parsed_path = external_id.split("@")
    return unless parsed_path.length == 2

    # If the path_segment doesn't contain a `/`, this is not a valid
    # external_id for a RepositoryAction.
    path_segments = parsed_path[0].split("/")
    return if path_segments.length < 2

    # first two segments will be the repo nwo.
    # Find the repository referenced by this NWO.
    repo = Repository.with_name_with_owner(path_segments[0], path_segments[1])

    # If no repo is available with this name, its an invalid external_id
    return unless repo
    # If there's no additional path segments, this is a reference to
    # an action at the root of the repo.
    return RepositoryAction.where(path: "Dockerfile", repository_id: repo.id).first if path_segments.length == 2

    # Calculate the expected path of the Dockerfile.
    file_path = path_segments[2..-1].join("/") + "/Dockerfile"
    return RepositoryAction.where(path: file_path, repository_id: repo.id).first
  end

  def action_runs
    CheckRun::for_app_id(GitHub.launch_github_app&.id)
    .where("check_runs.external_id like (?) OR (check_runs.external_id LIKE (?)) OR (check_runs.external_id = (?) AND check_suites.repository_id=(?))",
     "#{external_uses_path_prefix}%", "docker://%", "./#{local_uses_path_prefix}", repository_id)
  end

  # Public: Synchronize this action with its representation in the search
  # index. All existing actions that are not delisted get indexed.
  def synchronize_search_index
    if self.destroyed? || self.delisted?
      RemoveFromSearchIndexJob.perform_later("repository_action", self.id)
    else
      Search.add_to_search_index("repository_action", self.id)
    end

    self
  end

  def readme(committish: nil)
    async_readme(committish: committish).sync
  end

  def async_readme(committish:)
    async_repository.then do |repository|
      repository.async_default_branch.then do |branch|
        committish_or_default = committish.presence || repository.default_branch

        PreferredFile.find(
          directory: repository.directory(committish_or_default, base_path),
          subdirectories: %w(.),
          type: :readme,
        )
      end
    end
  end

  def adminable_by?(user)
    user.associated_repository_ids(min_action: :write).include?(repository_id)
  end

  def color_name
    RepositoryActions::Colors.select_by_name_or_hex(color)[:name]
  end

  def set_slug
    self.slug = name.encode("utf-8", undef: :replace, replace: "").parameterize
  end

  def event_payload
    {
      :repository_action => self,
      :repo => repository,
      owner.event_prefix.to_sym => owner,
    }
  end

  def event_context(prefix: event_prefix)
    {
      prefix.to_sym => slug,
      "#{prefix}_id".to_sym => id,
    }
  end

  def default_branch_workflow_snippet
    lines = []
    lines.push("- name: #{name}")
    lines.push("  uses: #{external_uses_path_prefix}#{repository.default_branch}")
    lines.push("")
    lines.join("\n")
  end

  def published_releases_workflow_snippet
    published_releases.limit(5).order("id DESC").map do |release|
      tag_name = release.tag_name
      sha = repository.tags.find(tag_name).commit.oid
      blob = repository.blob(sha, path)

      yaml = begin
        blob.nil? ? {} : YAML.safe_load(blob.data)
      rescue Psych::SyntaxError, Psych::DisallowedClass
        {}
      end

      inputs = yaml["inputs"] || {}

      lines = []
      lines.push("- name: #{name}")
      lines.push("  uses: #{external_uses_path_prefix}#{tag_name}")

      if inputs.any?
        lines.push("  with:")
        inputs.each do |(name, attrs)|
          optional = attrs["required"] ? nil : "optional"
          default = attrs["default"] ? "default is #{attrs["default"]}" : nil
          comment_parts = [optional, default].compact
          comment = comment_parts.any? ? "# #{comment_parts.join(", ")}" : ""

          lines.push("    # #{attrs["description"]}")
          lines.push("    #{name}: #{comment}")
        end
      end

      { release: release, example: lines.join("\n") }
    end
  end

  def latest_tag_or_default_branch
    published_releases.latest.first&.tag_name || repository.default_branch
  end

  private

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def partner_action?
    @_partner_action ||= repository && repository.owner_login.downcase.in?(CUSTOM_ICON_PARTNERS)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def set_color
    primer_color = RepositoryActions::Colors.select_by_name_or_hex(color)

    if primer_color.present?
      self.color = primer_color[:color_hex]
    else
      self.color = RepositoryActions::Colors.generate_default_color(name || "")[:color_hex]
    end
  end

  def remove_description_punctuation
    return unless self.description

    self.description = self.description.gsub(ENDING_PUNCTUATION_REGEX, "")
  end

  # If icon name is invalid, we leave it blank and the UI uses the first letter of the Action Name as the icon
  def set_icon_name
    return unless icon_name

    icon_name.downcase!

    self.icon_name = nil unless RepositoryActions::Icons::NAMES.include? icon_name
  end

  def validate_published_release
    return if repository_action_releases.where(published_on_marketplace: true).exists?

    return errors.add(:state, "Action needs a release to be listed")
  end

  def validate_name_and_slug
    return if slug.blank?

    # For transitioning from Dockerfile -> Action.yml. Same name is fine.
    return unless existing_action = RepositoryAction.find_by(slug: slug)
    return if existing_action.id == id
    return if replacing_existing_action?(existing_action)

    errors.add(:slug, "has already been taken")
  end

  def replacing_existing_action?(existing_action)
    return false unless ["action.yml", "action.yaml"].include?(path)

    existing_action.repository_id == repository_id
  end
end
