# frozen_string_literal: true

class MemexProjectColumn < ApplicationRecord::Domain::Memexes
  include GitHub::Validations
  extend GitHub::Encoding

  force_utf8_encoding :name

  belongs_to :memex_project, inverse_of: :memex_project_columns
  belongs_to :creator, class_name: "User"
  has_many :memex_project_column_values, inverse_of: :memex_project_column

  ASSIGNEES_COLUMN_NAME = "Assignees"
  LABELS_COLUMN_NAME = "Labels"
  MILESTONE_COLUMN_NAME = "Milestone"
  REPOSITORY_COLUMN_NAME = "Repository"
  TITLE_COLUMN_NAME = "Title"

  NEW_COLUMN_NAME = "New Column"

  # These are system-defined columns, so to prevent confusion we disallow users
  # from naming a user-defined column the same thing.
  RESERVED_COLUMN_NAMES = [
    ASSIGNEES_COLUMN_NAME,
    LABELS_COLUMN_NAME,
    MILESTONE_COLUMN_NAME,
    REPOSITORY_COLUMN_NAME,
    TITLE_COLUMN_NAME,
  ]

  NAME_BYTESIZE_LIMIT = 255

  enum data_type: {
    assignees: 0,
    labels: 1,
    milestone: 2,
    repository: 3,
    title: 4,
    text: 60,
  }

  validates :memex_project, presence: true
  validates(:name, presence: true, uniqueness: { scope: :memex_project }, bytesize: { maximum: NAME_BYTESIZE_LIMIT }, unicode: true)
  validate :name_is_not_reserved
  validate :creator_has_verified_email, on: :create
  validates(
    :position,
    presence: true,
    uniqueness: { scope: :memex_project_id },
    numericality: { only_integer: true, greater_than: 0 }
  )
  validates :data_type, presence: true
  validates :user_defined, inclusion: { in: [true, false] }
  validates :visible, inclusion: { in: [true, false] }
  validate :default_columns_persisted, on: :create
  validate :column_can_be_created, on: :create, if: :user_defined?
  validate :attribute_can_be_changed, on: :update, if: :system_defined?

  # Do not modify this list until https://github.com/github/memex/issues/534 is resolved.
  SYSTEM_DEFINED_COLUMNS = [
    { name: TITLE_COLUMN_NAME, visible: true, },
    { name: ASSIGNEES_COLUMN_NAME, visible: true, },
    { name: LABELS_COLUMN_NAME, visible: false, },
    { name: REPOSITORY_COLUMN_NAME, visible: false, },
    { name: MILESTONE_COLUMN_NAME, visible: false, },
  ]

  # Maps a column's data type to its top-level key in a `memex_project_item`'s
  # `value` for use in sorting.
  DATA_TYPE_SORT_ACCESSORS = {
    assignees: :login,
    labels: :name,
    milestone: :title,
    repository: :name,
    title: :title,
    text: :raw,
  }.freeze

  scope :user_defined, -> { where(user_defined: true) }

  def self.default_columns
    # Intentionally cache this value at the class level; we don't need to compute it more than once.
    return @default_columns if defined?(@default_columns)

    @default_columns = SYSTEM_DEFINED_COLUMNS.each_with_index.map do |params, index|
      record = self.new(
        params.merge(
          data_type: params[:name].downcase.to_sym,
          user_defined: false,
          position: index + 1,
        )
      )
      record.readonly!
      record
    end
  end

  def self.default_column(name)
    default_columns.find { |c| c.name == name }
  end

  def content_association
    raise NotImplementedError if user_defined?
    return nil if name == TITLE_COLUMN_NAME
    data_type.to_sym
  end

  def content_sort_key
    DATA_TYPE_SORT_ACCESSORS[data_type.to_sym]
  end

  def hidden?
    !visible?
  end

  def system_defined?
    !user_defined?
  end

  # The serializable ID of this column.
  #
  # For user-defined columns, this will be the ID of the column's row in the
  # database. For system-defined columns, this will be the (reserved) name of
  # the column.
  #
  # Return Integer or String.
  def synthetic_id
    user_defined? ? id : name
  end

  def to_hash(items: [], require_prefilled_associations: true)
    result = {
      data_type: data_type,
      id: synthetic_id,
      name: name,
      position: position,
      user_defined: user_defined,
      visible: visible,
    }

    column_values = items.reduce([]) do |values, item|
      val = item.column_values(
        columns: [self],
        require_prefilled_associations: require_prefilled_associations
      )[0][:value]
      values << { memex_project_item_id: item.id, value: val } if val.present?
      values
    end

    result[:memex_project_column_values] = column_values if column_values.present?
    result
  end

  private

  def name_is_not_reserved
    return unless name
    return unless user_defined?
    if RESERVED_COLUMN_NAMES.include?(name.titlecase)
      errors.add(:name, "cannot have a reserved value")
    end
  end

  def creator_has_verified_email
    return unless GitHub.email_verification_enabled?
    return unless user_defined?
    errors.add(:creator, "must have a verified email address") unless creator&.verified_emails?
  end

  def default_columns_persisted
    return unless user_defined?

    unless memex_project.memex_project_columns.any?(&:system_defined?)
      errors.add(:user_defined, "column cannot be added until default columns have been persisted")
    end
  end

  def column_can_be_created
    if memex_project.memex_project_columns.count >= MemexProject::COLUMN_LIMIT
      errors.add(:base, "Memex projects cannot have more than #{MemexProject::COLUMN_LIMIT} columns")
    end
  end

  def attribute_can_be_changed
    if changed_attributes.keys.include?("name")
      errors.add(:name, "cannot be changed for a system defined column")
    end
  end
end
