# frozen_string_literal: true

# Public: Loads Issue (or Pull Request, conceptually) assocation objects for
#         pre-filling in the new Issue or new PR forms, based on query params.
#
class PrefilledIssueFields
  # See also: Issue::MAX_ASSIGNEES.
  MAX_ASSIGNEES_FROM_PARAMS = 10
  MAX_LABELS_FROM_PARAMS = 20
  MAX_PROJECTS_FROM_PARAMS = 20

  attr_reader :params, :repository, :user

  # Public: Initialize a new PrefilledIssueFields object.
  #
  # params - A StrongParams instance passed from a controller action.
  # repository - The current Repository, where an Issue will exist.
  # user - The current User, responsible for the Issue.
  #
  # Returns a PrefilledIssueFields instance.
  def initialize(params:, repository:, user:)
    @params = params
    @repository = repository
    @user = user
  end

  # Public: A body String from params.
  #
  # Reads from params[:body] as well as params[:permalink].
  #
  # If neither :body nor :permalink is present, returns nil.
  #
  # If one of either is present, returns it. If both are present,
  # returns the :body value then :permalink value separated by a
  # newline.
  #
  # Returns a String or nil.
  def body
    return nil unless params[:body] || params[:permalink]
    [params[:body], params[:permalink]].compact.join("\n\n")
  end

  # Public: Returns the String name of a body template if present in params.
  #
  # Reads from params[:template].
  #
  # Returns a String or nil.
  def template
    params[:template]
  end

  # Public: A title String from params.
  #
  # Read from params[:title].
  #
  # Returns a String or nil.
  def title
    params[:title]
  end

  # Public: A Milestone record from params.
  #
  # Reads one value from params[:milestone].
  #
  # If the value of params[:milestone] looks like a number, will attempt
  # to find the Milestone of that number.
  #
  # If it looks like a text string, will attempt to find the Milestone
  # with that title. The title and param will be downcased before comparing.
  #
  # Returns a Milestone or nil.
  def milestone
    return unless params[:milestone]

    milestone_param = params[:milestone].to_s.strip

    # Attempt to treat milestone param as the milestone number
    if /\A[1-9]\d{0,}\z/ =~ milestone_param
      found = repository.milestones.open_milestones.where(number: milestone_param.to_i).first
      return found if found
    end

    # Fall back to looking up by title; measure the frequency of use for possible removal
    repository.milestones.open_milestones.detect { |m| m.title.downcase == milestone_param.downcase }
  end

  # Public: A collection of assignable User records from query params.
  #
  # Will return up to MAX_ASSIGNEES_FROM_PARAMS records from the current_repository.
  #
  # Reads from params[:assignees] and params[:assignee].
  #
  # For params[:assignees], understands a comma-separated String, such as
  # "&assignees=octocat,hubot".
  #
  # For params[:assignee], expects a single login String.
  #
  # Returns an Array of Users.
  def assignees
    logins = split_params_string(field: :assignees, limit: MAX_ASSIGNEES_FROM_PARAMS)
    if params[:assignee].to_s.present?
      logins << params[:assignee].to_s.strip if logins.count < MAX_ASSIGNEES_FROM_PARAMS
    end
    return [] if logins.empty?

    User.where(id: repository.available_assignee_ids, login: logins).limit(MAX_ASSIGNEES_FROM_PARAMS).to_a
  end

  # Public: A collection of Label records from the repository from query params.
  #
  # Will return up to MAX_LABELS_FROM_PARAMS records from the current_repository.
  #
  # Reads from params[:labels].
  #
  # Understands a comma-separated String, such as "&labels=foo,bar".
  #
  # Returns an Array of Labels.
  def labels
    names = split_params_string(field: :labels, limit: MAX_LABELS_FROM_PARAMS)
    return [] if names.empty?

    repository.labels.where(name: names).limit(MAX_LABELS_FROM_PARAMS).to_a
  end

  # Public: A collection of Project records from the repo / org from query params.
  #
  # Will return up to MAX_PROJECTS_FROM_PARAMS records from the current_repository
  # or its Organization, if the repository belongs to an org.
  #
  # Reads from params[:projects].
  #
  # Expects signifier Strings like the following:
  #
  #    org/123
  #    user/repo/123
  #    org/repo/123
  #
  # Understands a comma-separated String, such as "&projects=org/123,org/repo/456".
  #
  # Returns an Array of Projects.
  def projects
    project_params = split_params_string(field: :projects, limit: MAX_PROJECTS_FROM_PARAMS)
    return Project.none if project_params.empty?

    owner = repository.owner
    parsed_params = project_params.map do |param|
      ProjectQueryParam.new(param: param, repository: repository, owner: owner)
    end

    parsed_params.select!(&:valid?)
    return Project.none if parsed_params.empty?

    repo_owned, non_repo_owned = parsed_params.partition(&:repository_project?)
    projects = []

    if non_repo_owned.present?
      numbers = non_repo_owned.map(&:number)
      projects << owner.writable_projects_for(user).open_projects.where(number: numbers)
    end

    if repo_owned.present?
      numbers = repo_owned.map(&:number)
      projects << repository.writable_projects_for(user).open_projects.where(number: numbers)
    end

    projects = projects.map(&:to_a).flatten

    return Project.none if projects.empty?
    projects
  end

  # Private: Split an incoming string on commas, returning an Array of
  #          non-empty Strings.
  #
  # The returned Array will be up to limit in length.
  #
  # Returns an Array of Strings.
  private def split_params_string(field:, limit: 10)
    return [] unless params[field]
    params[field].to_s.split(",").first(limit).each(&:strip!).select(&:present?)
  end
end
