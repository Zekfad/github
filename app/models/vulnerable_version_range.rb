# frozen_string_literal: true

class VulnerableVersionRange < ApplicationRecord::Notify
  include GitHub::Relay::GlobalIdentification

  areas_of_responsibility :security_advisories

  belongs_to :vulnerability, touch: true
  has_many :repository_vulnerability_alerts, dependent: :delete_all

  PUBLIC_ECOSYSTEMS = ::AdvisoryDB::Ecosystems.api_filter

  validates :vulnerability, presence: true
  validates :ecosystem, inclusion: ::AdvisoryDB::Ecosystems.database_enum, presence: true
  validates :affects, presence: true
  validates :affects, format: { with: /\A\S+\z/ , message: "affects field can not contain whitespace" }

  # Validate requirement strings format
  # The requirement string delineates a _version range_, from a minimum value to a maximum.
  # Requirements strings must look like one of the following:
  # - `= 1.2.3`
  # - `< 1.2`
  # - `<= 234.beta6`
  # - `>= 1.0, < 1.5`
  # white spacing is strictly enforced
  REQUIREMENTS_FORMAT = %r{
    \A
    # Two basic versions, those with the comma, and those without
    (
      (?: # version without comma
        (?<only_operator>=|<|<=|>|>=)
        (?:[ ])
        (?<only_version>\d[^\s,]*)
      )
      |
      (?: # version with comma
        (?<lower_operator>>|>=) # restrict to it starting with greater-than or greater-than-or-equal-to
        (?:[ ])
        (?<lower_version>\d[^\s,]*)
        ,
        (?:[ ])
        (?<upper_operator><|<=) # likewise upper operator must be less-than
        (?:[ ])
        (?<upper_version>\d[^\s,]*)
      )
    )
    \z
  }x
  validates :requirements, presence: true, format: {
    with: REQUIREMENTS_FORMAT,
    message: "not a valid requirements string",
  }

  after_commit :instrument_vulnerability_update,
    if: :instrument_vulnerability_update?

  scope :has_public_ecosystem, -> {
    where(ecosystem: PUBLIC_ECOSYSTEMS)
  }

  scope :disclosed, -> {
    has_public_ecosystem.
      joins(:vulnerability).
      merge(Vulnerability.has_disclosed_status)
  }

  def has_public_ecosystem?
    PUBLIC_ECOSYSTEMS.include?(ecosystem)
  end


  def async_disclosed?
    return Promise.resolve(false) unless has_public_ecosystem?

    async_vulnerability.then do |vulnerability|
      vulnerability.disclosed_status_and_not_simulation?
    end
  end

  def disclosed?
    async_disclosed?.sync
  end

  # Public: Returns whether a job is currently processing alerts for this range.
  def currently_processing_alerts?
    GitHub.kv.exists(began_processing_at_key).value!
  end

  # Public: Returns when alert processing job first started running, if it's
  # still running.
  def began_processing_at
    GitHub.kv.get(began_processing_at_key).value!.to_i
  end

  # Public: Returns the number of repositories processed if a job is currently
  # processing alerts, otherwise returns 0.
  def total_alerts_processed
    GitHub.kv.get(total_alerts_processed_key).value!.to_i
  end

  # Public: Returns whether the number of repositories processed currently
  # exists, either true or false.
  def has_total_alerts_processed?
    GitHub.kv.exists(total_alerts_processed_key).value!
  end

  # Public: Returns the current cursor position for the related job.
  def cursor
    @cursor ||= GitHub.kv.get(cursor_key).value!
  end

  # Public: Sets the began processing timestamp unless it's already set.
  def begin_processing!
    return if currently_processing_alerts?

    GitHub.kv.set(
      began_processing_at_key,
      Time.now.utc.to_i.to_s,
      expires: default_kv_expires_time,
    )
  end

  # Public: Update the processed count with the given number of dependents.
  def increment_total_alerts_processed_count(additional_count)
    total_alerts_processed = GitHub.kv.get(total_alerts_processed_key).value!.to_i
    total_alerts_processed += additional_count

    GitHub.kv.set(
      total_alerts_processed_key,
      total_alerts_processed.to_s,
      expires: default_kv_expires_time,
    )
  end

  # Public: Sets the current cursor position for the related job.
  def cursor=(new_cursor)
    GitHub.kv.set(cursor_key, new_cursor, expires: default_kv_expires_time)
    @cursor = new_cursor
  end

  # Public: Removes the processing timestamp as the job has finished running.
  def done_processing!
    GitHub.kv.del(began_processing_at_key)
  end

  # Public: Creates a VulnerabilityAlertingEvent with a reason of
  # "process_alerts" as well as a VulnerableVersionRangeAlertingProcess record
  # for this VulnerableVersionRange.
  #
  # After creation of these records to track alert processing, the
  # VulnerableVersionRangeCreateVulnerabilityAlertsJob is enqueued to perform
  # the actual processing (creation) of RepositoryVulnerabilityAlert records.
  def process_alerts(actor: nil, skip_email: false)
    unless vulnerability.published?
      raise ArgumentError, "Vulnerability isn't published!"
    end

    vulnerability_alerting_event = nil
    vulnerable_version_range_alerting_process = nil

    VulnerabilityAlertingEvent.transaction do
      vulnerability_alerting_event =
        VulnerabilityAlertingEvent.create!(
          vulnerability_id: vulnerability.id,
          reason: :process_alerts,
          actor_id: actor&.id,
        )

      vulnerable_version_range_alerting_process =
        VulnerableVersionRangeAlertingProcess.create!(
          vulnerability_alerting_event_id: vulnerability_alerting_event.id,
          vulnerable_version_range_id: id,
        )
    end

    VulnerableVersionRangeCreateVulnerabilityAlertsJob.perform_later(
      vulnerable_version_range_alerting_process.id,
      skip_email: skip_email,
    )

    vulnerability_alerting_event
  end

  def is_range?
    requirements =~ /(^\D|[-])/
  end

  private

  def default_kv_expires_time
    (7 * 24).hours.from_now
  end

  def key_prefix
    "jobs.vulnerable-version-range-create-vulnerability-alerts.#{id}."
  end

  def began_processing_at_key
    key_prefix + "began-processing-at"
  end

  def total_alerts_processed_key
    key_prefix + "total-alerts-processed"
  end

  def cursor_key
    key_prefix + "cursor"
  end

  def instrument_vulnerability_update
    vulnerability.touch
    vulnerability.instrument(:update)
  end

  def instrument_vulnerability_update?
    return false unless vulnerability.disclosed?
    return true if destroyed?

    ecosystem_previously_changed? ||
      affects_previously_changed? ||
      fixed_in_previously_changed? ||
      requirements_previously_changed?
  end
end
