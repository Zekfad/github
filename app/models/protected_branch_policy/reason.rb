# rubocop:disable Style/FrozenStringLiteralComment

class ProtectedBranchPolicy
  # A Reason why a policy check was not fulfilled. This class is
  # used in context of a Decision.
  class Reason
    VALID_CODES = %i(
      force_push
      deletion
      unauthorized
      required_status_checks
      review_approved
      review_policy_not_satisfied
      review_policy_not_required
      invalid_signature
      merge_commit
      object_missing
    ).freeze

    # A Symbol identifying the reason
    attr_reader :code

    # A String summary of the reason
    attr_reader :summary

    # A String message giving details about the reason
    attr_reader :message

    # An optional Commit that was the basis for the decision
    attr_reader :basis_commit

    def initialize(code:, summary: nil, message:, basis_commit: nil)
      validate(code: code)

      @code = code
      @summary = summary
      @message = message
      @basis_commit = basis_commit
    end

  private

    def validate(code:)
      return if VALID_CODES.include?(code)

      raise ArgumentError, "code (#{code}) must be one of #{VALID_CODES}"
    end
  end
end
