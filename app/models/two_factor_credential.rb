# rubocop:disable Style/FrozenStringLiteralComment

class TwoFactorCredential < ApplicationRecord::Domain::Users

  self.ignored_columns = %w(recovery_secret)
  include GitHub::Relay::GlobalIdentification
  extend GitHub::Earthsmoke::EncryptedAttribute

  belongs_to :user

  validates_presence_of :secret, :plaintext_recovery_secret, :recovery_used_bitfield, :user_id
  validates_presence_of :sms_number, if: proc { |cred| cred.delivery_method == "sms" }

  validates_format_of :sms_number, :backup_sms_number,
                      with: GitHub::SMS::PHONE_NUMBER_REGEX,
                      allow_nil: true
  validate :no_sms_for_github_employees

  validates_inclusion_of :provider,
    in: GitHub::SMS.providers_for_env.map(&:provider_name).map(&:to_s),
    allow_nil: true

  if GitHub.two_factor_sms_enabled?
    validates_inclusion_of :delivery_method, in: ["sms", "app"]
  else
    validates_inclusion_of :delivery_method, in: ["app"]
  end

  encrypted_attribute :encrypted_recovery_secret, :plaintext_recovery_secret, read_error: :raise, write_error: :raise

  after_create_commit  :instrument_creation
  after_destroy_commit :instrument_deletion
  after_update_commit  :instrument_update, :notify_owner_of_fallback_number_changes

  # The total amount of recovery codes we send to the user
  # for storage. 16 is a nice amount. Smaller is inconvenient for
  # the user; bigger is kind of redundant.
  RECOVERY_CODE_TOTAL_COUNT = 16

  # The length of each recovery code we send to the user, in
  # decimal digits. 8 is a nice amount because it can be
  # displayed as "XXXX XXXX". It could also be set to 10 and
  # displayed as "XXXX XX XXXXX".
  RECOVERY_CODE_LENGTH = 10

  # The length, in base32 characters, of the shared secret we send
  # to the user.
  #
  # WARNING: DO NOT MODIFY. This needs to be 16 to be compatible
  # with the Google Authenticator app.
  TWO_FACTOR_SECRET_LENGTH = 16

  # Number of seconds the client and server timing can drift apart.  This
  # should prevent 2FA seemingly not working due to small timing differences.
  # This should be n*30 to accept codes from n intervals on either side of the
  # current time.
  ALLOWABLE_DRIFT = 90

  # What an OTP should look like
  OTP_REGEX = /\A\d{1,6}\z/

  # What a recovery code should look like
  RECOVERY_CODE_REGEX = /\A\h{#{RECOVERY_CODE_LENGTH}}\z/

  # Number of seconds the user may retry entering the OTP before the login
  # process must restart.
  OTP_RETRY_WINDOW = 900

  # How long we store the timestamp of the last valid OTP (to prevent reuse)
  LAST_OTP_TTL = ALLOWABLE_DRIFT * 2

  def self.normalize_otp(otp)
    otp.to_s.downcase.gsub(/[^\h]/, "")
  end

  def self.recovery_code?(user_input)
    !!user_input.match(RECOVERY_CODE_REGEX)
  end

  # Was this OTP valid at any time, 15 minutes in either direction from now.
  #
  # otp  - A string OTP.
  # time - The time to check around. (default Time.now)
  #
  # Returns true/false.
  def recently_valid_otp?(otp, time = Time.now)
    start = (time - 15.minutes).to_i
    stop  = (time + 15.minutes).to_i
    times = (start..stop).step(30)

    # ROTP::TOTP#at won't give leading zeros.
    otp = otp.gsub /^0/, ""

    times.any? { |t| otp == totp.at(t) }
  end

  # Finds the exact time an OTP was or will be valid, within 24 hours of now
  #
  #   otp  - A string OTP
  #
  # Returns a Time, or nil if it was not valid recently
  # This Time represents the beginning of the 30 second window the code would
  # have been generated in.  It does not account for clock skew we might allow.
  def otp_valid_timestamp(otp)
    time = 24.hours.from_now.to_i
    one_day_ago = 24.hours.ago.to_i

    until time < one_day_ago
      return Time.at(time) if otp == totp.at(time)
      time -= 30
    end
  end

  # Return an array of two-factor recovery codes.
  #
  # WARNING: This array must  be given to the user **only once**,
  # when he activates dual factor authentication.
  #
  # By default we return 16 of these codes; they are one time use
  # only: once a code is used, it doesn't work anymore. They must
  # be used in consecutive order. Instruct the user to cross them
  # out as they are being used.
  #
  # If the user runs out of one-time keys, you can call
  # User#generate_two_factor_recovery! to generate 16 new keys.
  #
  # E.g.
  #
  #   ["01230123", "01230123", ...]
  #
  def recovery_codes(amount = RECOVERY_CODE_TOTAL_COUNT)
    amount.times.map { |n| recovery_code(n) }
  end

  # Recovery codes with a hyphen in the middle to make them easier to read.
  #
  # Returns an Array of Strings.
  def formatted_recovery_codes
    recovery_codes.map do |code|
      "#{code.slice(0, 5)}-#{code.slice(5, 5)}"
    end
  end

  # Return the first unused recovery code
  def first_unused_recovery_code
    n = 0
    until !recovery_code_used? n
      n += 1
    end
    recovery_code n
  end

  # Generate the shared secret for two-factor authentication and
  # the corresponding secret for recovery key generation.
  #
  # WARNING: This must be called **once** every time the User
  # activates two-factor authentication.
  def generate_secrets!
    self.secret ||= self.class.generate_secret
    generate_recovery! if self.plaintext_recovery_secret.nil?
  end

  # Generates the two-factor recovery secret for this account. This
  # secret is the base for generating the recovery keys.
  #
  # This function is called automatically every time a new shared
  # secret is generated: we have a set of recovery keys for each
  # secret.
  #
  # Additionally, this function can be called again if the user
  # runs out of recovery keys.
  #
  # Call User#two_factor_recovery_codes to get the current array
  # of recovery keys.
  def generate_recovery!
    self.plaintext_recovery_secret = self.class.generate_secret
    self.recovery_used_bitfield = 0
    self.recovery_codes_viewed = false
  end

  def self.generate_secret
    Base64.strict_encode64(OpenSSL::Random.random_bytes(32))
  end

  # Return the nth recovery key for dual-factor authentication
  #
  # Recovery keys are by default 8 numeric characters long,
  # returned as strings.
  def recovery_code(n)
    mash = "#{plaintext_recovery_secret}:#{n}:#{GitHub.two_factor_salt}"
    OpenSSL::Digest::SHA1.hexdigest(mash)[0, RECOVERY_CODE_LENGTH]
  end

  def recovery_code_used?(n)
    ((1 << n) & recovery_used_bitfield) != 0
  end

  def recovery_code_mark_used(n)
    bitfield = recovery_used_bitfield | (1 << n)
    AccountMailer.two_factor_recover(self.user, number_of_remaining_codes(bitfield)).deliver_later
    update_attribute(:recovery_used_bitfield, bitfield)
  end

  def recovery_codes_viewed!
    update_attribute(:recovery_codes_viewed, true)
  end

  def verify_totp(otp, allow_reuse:)
    # Always allow reuse if globally configured to.
    allow_reuse ||= GitHub.two_factor_allow_reuse?

    resolve_outstanding_sms_otp(otp) if sms_enabled?
    cache_key = "totp::last_otp_at::#{self.id}"
    last_otp_at = GitHub.cache.get(cache_key)

    verified_at_or_fail = if allow_reuse
      totp.verify(otp, drift_ahead: ALLOWABLE_DRIFT, drift_behind: ALLOWABLE_DRIFT)
    else
      totp.verify(otp, drift_ahead: ALLOWABLE_DRIFT, drift_behind: ALLOWABLE_DRIFT, after: last_otp_at)
    end

    if verified_at_or_fail && !allow_reuse
      GitHub.cache.add(cache_key, verified_at_or_fail, LAST_OTP_TTL)
    end

    GitHub.dogstats.increment("authentication.2fa", tags: [
      verified_at_or_fail ? "result:success" : "result:failure",
    ])

    !!verified_at_or_fail
  end

  # In the most common case, this logic represents someone attempting to reuse
  # a valid OTP but in reality, this represents someone trying to use a code
  # older than the last currently validated otp. Since the window of validity
  # allows from some slack on both ends, it's possible to validate an "older"
  # token while "younger" tokens are still valid.
  def reused_valid_totp?(otp)
    !verify_totp(otp, allow_reuse: false) && totp.verify(otp, drift_ahead: ALLOWABLE_DRIFT, drift_behind: ALLOWABLE_DRIFT)
  end

  def sms_enabled?
    self.sms_number.present? && configured_with_sms?
  end

  def sms_permitted?
    user && !user.employee?
  end

  # Send an SMS containing the current OTP.
  #
  # use_alternate_provider - Try sending the SMS with the alternate provider.
  #
  # Returns nothing.
  def send_otp_sms(use_alternate_provider = false)
    if GitHub.two_factor_sms_enabled?
      raise GitHub::SMS::UnauthorizedRecipientError.new unless sms_permitted?

      otp = totp.now
      message = "#{otp} is your #{GitHub.flavor} authentication code."

      options = {}
      options[:provider] = if use_alternate_provider
        GitHub::SMS.get_alternate_provider(provider)
      else
        GitHub::SMS.get_provider(provider)
      end

      receipt = GitHub::SMS.send_message(sms_number, message, options)
      record_outstanding_sms_otp(otp, receipt)
    end
  end

  # Record this OTP and the time we're sending it.
  #
  # otp     - The OTP we sent.
  # receipt - A GitHub::SMS::Receipt.
  #
  # Returns nothing.
  def record_outstanding_sms_otp(otp, receipt)
    provider = receipt.provider.provider_name

    GitHub.dogstats.increment "two_factor_credential", tags: ["action:sms_timing_sent", "provider:#{provider}", "country_code:#{country_code}"]

    key = sms_timing_key(otp)
    OtpSmsTiming.record_outstanding(key, provider, user.id)
  end

  # Record that we've received an OTP.
  def resolve_outstanding_sms_otp(otp)
    key = sms_timing_key(otp)
    if timing = OtpSmsTiming.by_timing_key(key)
      provider = timing.provider
      time = timing.created_at
      OtpSmsTiming.resolve(timing.id)
      diff = ((Time.now - Time.at(time)) * 1000).to_i
      GitHub.dogstats.timing "two_factor_credential.sms_timing", diff, tags: ["provider:#{provider}", "country_code:#{country_code}", "store:mysql"]
    end
  end

  # Switch the provider to whichever one isn't currently being used.
  #
  # Returns the new provider name.
  def switch_provider
    new_provider = GitHub::SMS.get_alternate_provider(provider)
    self.provider = new_provider.provider_name.to_s
  end

  # Key for redis SMS OTP timing hash.
  def sms_timing_key(otp)
    otp = sprintf("%06d", otp) if otp.is_a?(Integer)
    hash = Digest::SHA256.hexdigest "#{otp}|#{secret}|#{GitHub.two_factor_salt}"
    "#{user.id}|#{hash}"
  end

  # Country code of sms_number if one is configured.
  def country_code
    return unless sms_enabled?
    if match = sms_number.match(GitHub::SMS::PHONE_NUMBER_PARTS_REGEX)
      match[1]
    end
  end

  def configured_with?(possible_delivery_method)
    delivery_method == possible_delivery_method.to_s
  end

  def configured_with_sms?
    configured_with? :sms
  end

  def configured_with_app?
    configured_with? :app
  end

  # Return the mashed shared secret. This is actually the real,
  # 32 byte string that we send to the user as a shared secret. It is
  # generated based on the two-factor secret we keep in the database and
  # a salt loaded from the environment.
  #
  # WARNING: Display this to the user **only once**, when dual factor
  # authentication is activated.
  def mashed_secret
    mashed = "#{secret}:#{GitHub.two_factor_salt}"
    OpenSSL::Digest::SHA1.digest(mashed).each_byte.map do |b|
      ROTP::Base32::CHARS[b % 32]
    end.take(TWO_FACTOR_SECRET_LENGTH).join
  end

  def totp
    ROTP::TOTP.new(mashed_secret, issuer: GitHub.flavor)
  end

  def configure_with_sms(number)
    GitHub.dogstats.increment("two_factor_credential", tags: [
      "action:configure_sms",
      "recent_security_checkup:#{user.recently_took_action_on_security_checkup?}",
    ])
    update(sms_number: number, delivery_method: "sms")
  end

  def configure_with_app
    GitHub.dogstats.increment("two_factor_credential", tags: [
      "action:configure_app",
      "recent_security_checkup:#{user.recently_took_action_on_security_checkup?}",
    ])
    update(sms_number: nil, delivery_method: "app")
  end

  # Produces the URI used by authenticator apps. e.g.
  #
  # otpauth://totp/GitHub:user?secret=<secret>&issuer=GitHub
  #
  # Produces separate URLs across enterprise and non-prod envs to prevent confusion.
  #
  # In Duo's UI:
  # dotcom: GitHub:<username>
  # non-prod/enterprise: GitHub[ Enterprise]:<hostname>/<username>
  def provisioning_url
    if GitHub.enterprise? || !Rails.env.production?
      totp.provisioning_uri("#{GitHub.urls.host_name}/#{user.login}")
    else
      totp.provisioning_uri(user.login)
    end
  end

  def remove_sms_fallback
    GitHub.dogstats.increment("two_factor_credential", tags: ["action:remove_sms_fallback"])
    configure_sms_fallback(nil)
  end

  def configure_sms_fallback(number, provider = nil)
    GitHub.dogstats.increment("two_factor_credential", tags: [
      "action:configure_sms_fallback",
      "recent_security_checkup:#{user.recently_took_action_on_security_checkup?}",
    ])
    attributes = { backup_sms_number: number }
    # provider will only be passed on a retry, and should only be updated if the first form of 2FA is an app
    # we don't want to update the provider if we alredy have a working one for the primary sms number
    attributes[:provider] = provider unless (provider.blank? || configured_with_sms?)
    update(attributes)
  end

  def send_fallback_sms(reason)
    if GitHub.two_factor_sms_enabled?
      raise GitHub::SMS::UnauthorizedRecipientError.new unless sms_permitted?
      instrument_send_fallback_sms(reason)
      GitHub.dogstats.increment("two_factor_credential", tags: ["reason:#{reason}", "action:send_fallback_sms"])
      message = "#{totp.now} is your #{GitHub.flavor} authentication code."

      data = two_factor_sms_backup_provider_data
      provider = GitHub::SMS.select_provider(backup_sms_number, primary_provider: provider, last_provider: data[:provider])
      GitHub.kv.set(
        two_factor_sms_fallback_provider,
        { provider: provider }.to_json,
        expires: 10.minutes.from_now,
      )
      GitHub::SMS.send_message(backup_sms_number, message, provider: provider)
    end
  end

  def two_factor_sms_backup_provider_data
    data = GitHub.kv.get(two_factor_sms_fallback_provider).value { "{}" }
    # The KV block value is only returned in case of an error. If the key is
    # simply expired that isn't considered an error and will return `nil`.
    data ||= "{}"
    data = JSON.parse(data).symbolize_keys
  end

  def two_factor_sms_fallback_provider
    "TwoFactorSMSFallbackProvider:#{id}"
  end

  def fallback_enabled?
    backup_sms_number.present?
  end

  def roll_secrets
    generate_secrets! && save
  end

  def delivery_method_display
    if delivery_method == "sms"
      "SMS message"
    else
      "authenticator application"
    end
  end

  include Instrumentation::Model

  def event_prefix() :two_factor_authentication end

  def event_payload
    { user: self.user }
  end

  # Public: Instrument creating records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_creation(payload = {})
    instrument :enabled, payload
  end

  DESTROY_INSTRUMENT_FIELDS = %w(
    user secret recovery_codes sms_number backup_sms_number
  ).map(&:to_sym)

  # Public: Instrument deleting records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_deletion(payload = {})
    DESTROY_INSTRUMENT_FIELDS.each { |field| payload[field] = send(field) }
    instrument :disabled, payload
  end

  def instrument_update(payload = {})
    if saved_change_to_backup_sms_number?
      previous = backup_sms_number_before_last_save || "none"
      instrument :update_fallback, payload.merge(old_fallback: previous)
    end

    if saved_change_to_encrypted_recovery_secret?
      instrument :recovery_codes_regenerated, payload
    end

    if saved_change_to_provider?
      audit_prefix = :user
      if Audit.context[:from]&.start_with?("stafftools")
        audit_prefix = :staff
      end

      instrument :switch_sms_provider, \
        prefix: audit_prefix,
        provider: provider
    end
  end

  # Public: Instrument when using the fallback to sms option.
  #
  # Returns nothing.
  def instrument_send_fallback_sms(reason)
    case reason
    when :reset
      instrument :password_reset_fallback_sms
    when :sign_in
      instrument :sign_in_fallback_sms
    else
      raise ArgumentError, "expected reason to be :reset or :sign_in"
    end
  end

  def notify_owner_of_fallback_number_changes
    if self.saved_change_to_backup_sms_number?
      if self.backup_sms_number
        AccountMailer.configure_sms_fallback(self, backup_sms_number).deliver_later
      else
        AccountMailer.remove_sms_fallback(self, backup_sms_number_before_last_save).deliver_later
      end
    end
  end

  # It feels dangerous to add a method returning which codes are valid so let's
  # just return the count.
  def number_of_remaining_codes(bitfield = self.recovery_used_bitfield)
    RECOVERY_CODE_TOTAL_COUNT - bitfield.to_s(2).count("1")
  end

  # GitHub Staff should never be allowed to use SMS as a 2FA method/fallback:
  # https://github.com/github/iam/issues/594
  def no_sms_for_github_employees
    return if sms_permitted?
    if sms_enabled?
      errors.add(:sms_number, "can't be set for GitHub employees")
    end
    if fallback_enabled?
      errors.add(:backup_sms_number, "can't be set for GitHub employees")
    end
  end
end
