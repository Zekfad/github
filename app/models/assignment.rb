# rubocop:disable Style/FrozenStringLiteralComment

class Assignment < ApplicationRecord::Domain::Repositories

  belongs_to :issue
  belongs_to :assignee, foreign_key: :assignee_id, class_name: "User"

  validates :issue, :assignee, presence: true
  validates :assignee_id, uniqueness: { scope: [:issue_id] }
  validate  :ensure_assignee_is_a_collaborator

  after_commit :trigger_assigned_event,   on: :create
  after_commit :trigger_unassigned_event, on: :destroy

  scope :for_assignee,  -> (assignee) { where(assignee_id: assignee.id) }
  scope :for_assignees, -> (assignees) { where(assignee_id: assignees.map(&:id)).by_age }
  scope :by_age, -> { order("assignments.created_at ASC") }

  delegate :repository, to: :issue

  def trigger_assigned_event
    issue.trigger_assigned_event(assignee)
  end

  # Only trigger an unassigned event if the issue is still around.
  # We don't want this to fire when the assignment is being destroyed
  # as a result of an issue being destroyed.
  def trigger_unassigned_event
    return unless issue.present? && assignee.present?
    issue.trigger_unassigned_event(assignee)
  end

  def ensure_assignee_is_a_collaborator
    return if assignee.ghost?

    unless issue.assignable_to?(assignee)
      errors.add :assignee, "must be a collaborator"
    end
  end
end
