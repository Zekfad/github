# frozen_string_literal: true

# Mixin for CommitComments, Issues, IssueComments, PullRequests,
# PullRequestReviews, PullRequestReviewComments, and GistComments
# to expose an `#author_association` helper method
module AuthorAssociable
  # Returns the comment's author association (e.g., first time contributor)
  #
  # viewer - the current user, used to scope organization membership discoverability
  #
  # Returns an AuthorAssociation
  def author_association(viewer = nil)
    CommentAuthorAssociation.new(comment: self, viewer: viewer)
  end

  module InteractiveComponentContainer
    # Returns the ComposableComment's author association (e.g., first time contributor)
    #
    # user - the user who is interacting with an interactive component
    #
    # Returns an AuthorAssociation
    def author_association(user = nil)
      InteractiveComponentContainerAuthorAssociation.new(container: self, user: user)
    end
  end
end
