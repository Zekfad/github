# frozen_string_literal: true

class Hook::Payload::DeployKeyPayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
      key: Api::Serializer.public_key_hash(hook_event.key),
    }
  end
end
