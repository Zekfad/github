# frozen_string_literal: true

class Hook::Payload::StarPayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
      starred_at: Api::Serializer.time(hook_event.star&.created_at),
    }
  end
end
