# frozen_string_literal: true

class Hook::Payload::ReleasePayload < Hook::Payload
  version(:v3) do
    {}.tap do |payload|
      payload[:action]  = hook_event.action
      payload[:changes] = hook_event.changes if hook_event.changes
      payload[:release] = Api::Serializer.release_hash(hook_event.release)
    end
  end
end
