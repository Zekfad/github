# frozen_string_literal: true

class Hook::Payload::UserPayload < Hook::Payload
  version(:v3) do
    {
      user: Api::Serializer.user_hash(hook_event.user),
      action: hook_event.action,
    }
  end
end
