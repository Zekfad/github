# frozen_string_literal: true

class Hook::Payload::WorkflowDispatchPayload < Hook::Payload
  version(:v3) do
    {
      ref: hook_event&.ref,
      workflow: hook_event&.workflow,
      inputs: hook_event&.inputs,
    }
  end
end
