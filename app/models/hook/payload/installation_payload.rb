# frozen_string_literal: true

class Hook::Payload::InstallationPayload < Hook::Payload

  version(:v3) do
    {}.tap do |opts|
      opts[:action]       = hook_event.action
      opts[:installation] = installation_hash

      case hook_event.action.to_s
      when "created"
        opts[:repositories] = repositories_array(hook_event.repositories)
        opts[:requester] = Api::Serializer.simple_user_hash(hook_event.requester)
      when "deleted"
        opts[:repositories] = repositories_array(hook_event.repositories)
      end
    end
  end

  private

  def installation_hash
    Api::Serializer.installation_hash(hook_event.installation)
  end

  def repositories_array(repositories)
    return [] if repositories.blank?

    GitHub.dogstats.gauge("installation.payload.repositories.count", repositories.size)
    repositories.inject([]) do |collection, repo|
      collection << Api::Serializer.repository_identifier_hash(repo)
    end
  end
end
