# frozen_string_literal: true

class Hook::Payload::DeploymentPayload < Hook::Payload

  version(:v3) do
    {
      deployment: Api::Serializer.deployment_hash(hook_event.deployment),
      action: hook_event.action,
    }
  end

end
