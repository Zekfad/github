# frozen_string_literal: true

class Hook::Payload::PackagePayload < Hook::Payload

    version(:v3) do
      {
        action: hook_event.action,
        package: Api::Serializer.registry_package_hash(hook_event.package, hook_event.package_version),
      }
    end

end
