# frozen_string_literal: true

class Hook::Payload::InstallationRepositoriesPayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
      installation: installation_hash,
      repository_selection: hook_event.repository_selection,
      repositories_added: repositories_array(hook_event.repositories_added),
      repositories_removed: repositories_array(hook_event.repositories_removed),
      requester: Api::Serializer.simple_user_hash(hook_event.requester)
    }
  end

  private

  def installation_hash
    Api::Serializer.installation_hash(hook_event.installation)
  end

  def repositories_array(repository_ids)
    return [] if repository_ids.blank?

    scope = Repository
      .includes(:network)
      .where(id: repository_ids)
      .select(:id, :name, :owner_login, :public, :source_id)

    repositories = []

    scope.find_in_batches do |batch_of_repositories|
      batch_of_repositories.map do |repository|
        repositories << Api::Serializer.repository_identifier_hash(repository)
      end
    end

    repositories
  end
end
