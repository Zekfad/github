# frozen_string_literal: true

class Hook::Payload::PageBuildPayload < Hook::Payload

  version(:v3) do
    {
      id: hook_event.page_build_id,
      build: Api::Serializer.page_build_hash(hook_event.page_build),
    }
  end

end
