# frozen_string_literal: true

class Hook::Payload::MilestonePayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
      milestone: Api::Serializer.milestone_hash(hook_event.milestone),
    }.merge(changes_payload)
  end
end
