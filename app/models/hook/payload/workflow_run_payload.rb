# frozen_string_literal: true

class Hook::Payload::WorkflowRunPayload < Hook::Payload

  version(:v3) do
    GitHub::PrefillAssociations.for_workflow_runs([hook_event.workflow_run], hook_event.target_repository)
    {
      action: hook_event.action,
      workflow_run: Api::Serializer.workflow_run_hash(hook_event.workflow_run),
      workflow:     Api::Serializer.workflow_hash(hook_event.workflow),
    }
  end

end
