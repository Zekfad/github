# frozen_string_literal: true

class Hook::Payload::CheckSuitePayload < Hook::Payload
  include Formatter

  version(:v3) do
    {}.tap do |opts|
      opts[:action]      = hook_event.action
      check_suite_hash   = Api::Serializer.check_suite_hash(check_suite)
      opts[:check_suite] = check_suite_hash.tap { |hs|
        hs.delete(:repository)
      }
    end
  end

  private

  def check_suite
    hook_event.check_suite
  end
end
