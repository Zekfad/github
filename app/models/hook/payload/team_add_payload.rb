# frozen_string_literal: true

class Hook::Payload::TeamAddPayload < Hook::Payload

  version(:v3) do
    {
      team: Api::Serializer.team_hash(hook_event.team),
      repository: Api::Serializer.repository_hash(hook_event.target_repository),
    }
  end

end
