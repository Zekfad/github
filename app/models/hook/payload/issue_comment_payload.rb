# frozen_string_literal: true

class Hook::Payload::IssueCommentPayload < Hook::Payload

  version(:v3) do
    {}.tap do |payload|
      payload[:action]  = hook_event.action
      payload[:changes] = hook_event.changes if hook_event.changes
      payload[:issue]   = Api::Serializer.issue_hash(hook_event.issue)
      payload[:comment] = Api::Serializer.issue_comment_hash(hook_event.issue_comment)
    end
  end
end
