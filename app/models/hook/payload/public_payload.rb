# frozen_string_literal: true

class Hook::Payload::PublicPayload < Hook::Payload

  version(:v3) do
    {} # repository will be mixed in automatically
  end

end
