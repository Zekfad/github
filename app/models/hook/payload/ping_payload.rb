# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Payload::PingPayload < Hook::Payload

  version(:v3) do
    {
      zen: GitHub.random_zen,
      hook_id: hook_event.hook_id,
      hook: hook_hash,
    }
  end

  private

  def hook_hash
    case hook_event.hook.installation_target
    when Repository
      Api::Serializer.repo_hook_hash(hook_event.hook)
    when Organization
      Api::Serializer.org_hook_hash(hook_event.hook)
    when Business
      Api::Serializer.business_hook_hash(hook_event.hook)
    when Integration
      Api::Serializer.integration_hook_hash(hook_event.hook)
    when Marketplace::Listing
      Api::Serializer.marketplace_listing_hook_hash(hook_event.hook)
    when SponsorsListing
      Api::Serializer.sponsors_listing_hook_hash(hook_event.hook)
    else
      raise ArgumentError.new("A hook must be installed on a Repository, an Organization, a Business, an Integration, Marketplace Listing, or a Sponsors Listing.")
    end
  end

end
