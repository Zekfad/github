# frozen_string_literal: true

class Hook::Payload::ContentReferencePayload < Hook::Payload
  version(:v3) do
    {
      action: hook_event.action,
      content_reference: Api::Serializer.content_reference_hash(hook_event.content_reference),
    }
  end
end
