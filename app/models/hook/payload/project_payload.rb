# frozen_string_literal: true

class Hook::Payload::ProjectPayload < Hook::Payload
  version(:v3) do
    {}.tap do |payload|
      payload[:action]  = hook_event.action
      payload[:project] = Api::Serializer.project_hash(hook_event.project)
      payload[:changes] = hook_event.changes if hook_event.changes
    end
  end
end
