# frozen_string_literal: true

class Hook::Payload
  class Version
    attr_reader :name, :definitions

    def initialize(name, &definition)
      @name, @definitions = name, [definition]
    end

    def apply(event_payload)
      computed_version = definitions.each_with_object({}) do |definition, result|
        # Build up hash by applying each definition block. Successive
        # blocks do not override values created by previous blocks. This
        # ensures that the values are in the same order in which they were
        # defined.
        result.merge!(event_payload.instance_eval(&definition)) do |key, existing_value, append_value|
          existing_value
        end
      end

      # Once a version is computed it should not be mutated
      computed_version.freeze
    end

    def append(other_version)
      @definitions += other_version.definitions if other_version.present?
    end
  end
end
