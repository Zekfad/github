# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Payload
  include Hook::Payload::VersionsDependency

  # Specify default payload values for each version. These
  # will be appended to the payload for the specific event.
  # Values specified in the payload take precedence over
  # these default values.
  defaults(:v3) do
    Hash.new.tap do |defaults|
      defaults[:repository] = Api::Serializer.repository_hash(hook_event.target_repository) if hook_event.target_repository
      defaults[:organization] = Api::Serializer.organization_hash(hook_event.target_organization) if hook_event.target_organization
      defaults[:enterprise] = Api::Serializer.business_hash(hook_event.target_business) if hook_event.target_business
      defaults[:sender] = Api::Serializer.user_hash(hook_event.actor) if hook_event.actor
    end
  end

  # Public: Instantiates the correct Hook::Payload model for the given Hook::Event.
  #
  # hook_event - The Hook::Event model to build a payload for.
  #
  # Returns an instance of a Hook::Payload model.
  # Raises NameError if the Hook::Payload model is not found.
  def self.for_event(hook_event)
    klass = "Hook::Payload::#{hook_event.event_type.camelize}Payload".constantize
    klass.new hook_event
  end

  attr_reader :hook_event

  def initialize(hook_event)
    @hook_event = hook_event
  end

  # Public: Evaluates the specified version against the
  # payload instance. Caches the result to avoid doing
  # duplicate work.
  #
  # version_name - The Symbol name of the requested version (e.g. :v3)
  #
  # Returns a Hash.
  def to_version(version_name)
    @version_cache ||= {}
    return @version_cache[version_name] if @version_cache.key?(version_name)

    event_type = @hook_event.event_type
    tags = GitHub::TaggingHelper.create_hook_event_tags(event_type)

    GitHub.dogstats.time("hooks.payload.apply", tags: tags + ["version_name:#{version_name}"]) do
      version = self.class.version(version_name)
      @version_cache[version_name] = version.apply(self)
    end
  end

  def changes_payload
    return {} unless hook_event.changes

    {
      changes: hook_event.changes,
    }
  end
end
