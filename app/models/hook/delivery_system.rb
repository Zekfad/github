# rubocop:disable Style/FrozenStringLiteralComment

class Hook::DeliverySystem
  class PayloadsNotGenerated < StandardError; end
  class PayloadTooLarge < Hookshot::HookshotError; end

  QUEUES_BY_URL_PREFIX = {
    "codecov.io".."codecov.io" => "codecov",
    "c".."c" => "hookshot-1",
    "slack.github.com".."slack.github.com" => "hookshot-2",
    "s".."t" => "hookshot-0",
    "h".."h" => "hookshot-1",
    "dependabot-api.githubapp.com".."dependabot-api.githubapp.com" => "hookshot-3",
    "launch-receiver.githubapp.com".."launch-receiver.githubapp.com" => "hookshot-4",
  }.freeze
  CATCHALL_QUEUE = "hookshot".freeze
  REDELIVERY_QUEUE = "hookshot-redeliveries".freeze

  # hookshot-go has the following flow:
  # - Receive job from Aqueduct (up to 20s)
  # - Deliver hook (up to 10s)
  # - Ack job to Aqueduct (up to 20s)
  # We add 10s of padding just in case to get 60s before Aqueduct retries
  # a job if it has not received an ack from hookshot-go.
  AQUEDUCT_UNACKED_REDELIVERY_TIMEOUT = 60

  # Public: Instantiates a delivery system instance for the specified hook event
  # and then triggers a delivery.
  #
  # hook_event - The Hook::Event to be delivered.
  #
  # Returns nothing.
  def self.deliver(hook_event)
    new(hook_event).deliver
  end

  # Public: Redelivers the specified past delivery for a given Hook.
  #
  # delivery_guid - The GUID string of the past delivery.
  # hook - The Hook which the redelivery should target.
  #
  # Returns true.
  def self.redeliver(delivery_guid, hook)
    if GitHub.deliver_hooks_via_resque? || GitHub.deliver_hooks_via_aqueduct?
      payload = {
        hook_id: hook.id,
        guid: delivery_guid,
        parent: hook.hookshot_parent_id,
        hook_data: hook.config,
        hook_configuration: {
          needs_public_key_signature: needs_public_key_signature?(hook)
        }
      }

      if GitHub.deliver_hooks_via_resque?
        redelivery_payload = payload.merge(
          github_request_id: GitHub.context[:request_id],
          enqueued_at: (Time.now.to_f * 1_000).round
        )

        Resque.enqueue(HookshotGoWebhookRedeliveriesJob, redelivery_payload)
      else
        post_redelivery_to_aqueduct(payload)
      end
    else
      data = { hook_id: hook.id, guid: delivery_guid }
      hookshot_client = Hookshot::Client.for_parent(hook.hookshot_parent_id)

      status, body = time_and_log_request("hooks.send_to_hookshot.redeliver.time", data) do
        hookshot_client.redeliver(delivery_guid, hook.id, hook.config)
      end

      raise Hookshot::PayloadTooLarge.new(hook.config, hook.config.to_json.bytesize) if status == 413
      raise Hookshot::BadResponseError.new(status) unless status == 200
    end

    true
  end

  # Public: Enqueues a job which will perform a redelivery in the background.
  #
  # delivery_guid - The GUID string of the past delivery.
  # hook - The Hook which the redelivery should target.
  #
  # Returns nothing.
  def self.redeliver_later(delivery_guid, hook)
    RedeliverHooksJob.perform_later(delivery_guid, hook.id)
  end

  # Public: pushes a payload to Hookshot.
  #
  # payload - The Hash payload that should be POSTed to Hookshot
  #
  # Returns nothing.
  def self.post_payload_to_hookshot(payload)
    parent = (payload[:parent] || payload["parent"])
    hookshot_client = Hookshot::Client.for_parent(parent)

    status, body = time_and_log_request("hooks.send_to_hookshot.time", payload) do
      hookshot_client.deliver(payload)
    end

    raise Hookshot::PayloadTooLarge.new(payload, payload.to_json.bytesize) if status == 413
    raise Hookshot::BadResponseError.new(status, body) unless status == 200
  end

  def self.post_to_aqueduct(payload, hook = nil)
    # Queue determinator based on url will be added here
    payload = payload.with_indifferent_access
    queue = queue_for(payload.dig(:hook, :data, :url))
    parent = hook&.hookshot_parent_id || payload[:parent]

    payload = payload.merge(
      github_request_id: GitHub.context[:request_id],
      enqueued_at: (Time.now.to_f * 1_000).round,
    )

    options = {
      queue: queue,
    }

    job_args = build_aqueduct_job(payload, queue)
    client = aqueduct_client_for(parent, **options)
    client.send_job(**job_args)
  end

  def self.build_aqueduct_job(payload, queue)
    job = {
      queue: queue,
      payload: payload.to_json,
    }

    if GitHub.flipper[:reduce_aqueduct_delivery_timeout_for_webhooks].enabled?
      job[:redelivery_timeout_secs] = AQUEDUCT_UNACKED_REDELIVERY_TIMEOUT
    end

    job
  end

  def self.aqueduct_client_for(parent, queue: nil)
    if Hookshot::Client::PARENTS_USING_STAGING.key?(parent)
      self.hookshot_go_staging_aqueduct_client
    else
      self.hookshot_go_default_aqueduct_client
    end
  end

  def self.hookshot_ruby_staging_aqueduct_client
    @hookshot_ruby_staging_aqueduct_client ||= GitHub.build_aqueduct_client(app: "github-staging")
  end

  def self.hookshot_ruby_default_aqueduct_client
    @hookshot_ruby_default_aqueduct_client ||= GitHub.build_aqueduct_client(app: "github-#{Rails.env}")
  end

  def self.hookshot_go_staging_aqueduct_client
    @hookshot_go_staging_aqueduct_client ||= GitHub.build_aqueduct_client(app: "hookshot-staging")
  end

  def self.hookshot_go_default_aqueduct_client
    @hookshot_go_default_aqueduct_client ||= GitHub.build_aqueduct_client(app: "hookshot-#{Rails.env}")
  end

  def self.queue_for(url)
    host = URI(url).host.to_s

    _, potential_queue = QUEUES_BY_URL_PREFIX.find do |range, queue|
      range.any? { |word| host.start_with?(word) }
    end

    potential_queue || CATCHALL_QUEUE
  rescue
    CATCHALL_QUEUE
  end

  def self.post_redelivery_to_aqueduct(redelivery_payload)
    GitHub.dogstats.time("hooks.send_redelivery_to_aqueduct.time") do
      begin
        redelivery_payload = redelivery_payload.with_indifferent_access
        redelivery_payload = redelivery_payload.merge(
          github_request_id: GitHub.context[:request_id],
          enqueued_at: (Time.now.to_f * 1_000).round,
        )
        options = {
          queue: REDELIVERY_QUEUE,
        }
        job_args = build_aqueduct_job(redelivery_payload, REDELIVERY_QUEUE)
        client = aqueduct_client_for(redelivery_payload[:parent], **options)
        client.send_job(**job_args)
      rescue Aqueduct::Client::ClientError => e
        GitHub.dogstats.increment("hooks.send_redelivery_to_aqueduct.error")
        raise e
      end
    end
  end

  attr_reader :hook_event

  def initialize(hook_event)
    @hook_event = hook_event
  end

  # Public: Pushes the event/hook information to hook services
  # for delivery. A single request is made to Hookshot for each parent/version combination for
  # the subscribed hooks.
  #
  # Returns nothing.
  def deliver
    tags = GitHub::TaggingHelper.create_hook_event_tags(hook_event.event_type, hook_event.try(:action))
    tags << "type:jit_hydrated"
    tags << "site:#{GitHub.site}"

    if GitHub.deliver_hooks_via_resque? || GitHub.deliver_hooks_via_aqueduct?
      begin
        generate_hookshot_payloads

        return if @hookshot_deliveries.none?

        @hookshot_deliveries.each do |delivery|
          next if delivery.hookshot_payload.nil?

          if (GitHub.deliver_hooks_via_resque? || GitHub.deliver_hooks_via_aqueduct?) && GitHub::Aqueduct.is_payload_size_large?(delivery.hookshot_payload.to_json.bytesize)
            self.class.post_payload_to_hookshot(delivery.hookshot_payload)
          else
            current_installation = installation_specifics_for(delivery.parent)

            delivery.hooks.each do |hook|
              next if hook_event.attributes[:delivered_hook_ids].include?(hook.id)
              hook_config_hash = delivery.hookshot_payload[:hooks].find { |h| h[:id] == hook.id }
              payload = delivery.hookshot_payload.dup.tap do |hash|
                hash[:hook] = hook_config_hash
                hash.delete(:hooks)
              end
              if GitHub.deliver_hooks_via_resque?
                payload = payload.merge(
                  github_request_id: GitHub.context[:request_id],
                  enqueued_at: (Time.now.to_f * 1_000).round
                )
                Resque.enqueue(HookshotGoWebhooksJob, payload)
              else
                self.class.post_to_aqueduct(payload, hook)
              end
              hook_event.attributes[:delivered_hook_ids] += [hook.id]
            end
          end

          instrument_delivery(delivery)
        end

        tags << "status:success"

        GitHub.dogstats.increment("hooks.delivered_to_aqueduct.per_trigger.count", tags: tags)
      rescue => e
        tags << "status:failure"
        tags << "exception_class:#{e.class.name.underscore}"
        GitHub.dogstats.increment("hooks.delivered_to_aqueduct.per_trigger.count", tags: tags)

        raise e
      end
    else
      begin
        if deliveries.none?
          return
        end

        deliveries.each do |delivery|
          post_to_hook_services(delivery)
        end

        tags << "status:success"
        GitHub.dogstats.increment("hooks.delivered_to_hookshot.per_trigger.count", tags: tags)
      rescue => e
        tags << "status:failure"
        tags << "exception_class:#{e.class.name.underscore}"
        GitHub.dogstats.increment("hooks.delivered_to_hookshot.per_trigger.count", tags: tags)
        raise e
      end
    end
  end

  # Public: Enqueues background jobs to push generated payloads to Hookshot
  # Because this delivery method pushes hook payloads into Redis to enqueue the
  # background job, it should be used with care to avoid a negative impact on
  # the amount of memory available to Redis.
  #
  # Returns nothing.
  def deliver_push_event_later
    unless @payloads_generated
      raise PayloadsNotGenerated, "you must call `generate_hookshot_payloads' " +
        "before calling `deliver_later'"
    end

    return if payloads.none?

    unless GitHub.enterprise?
      PostPushEventToHookshotJob.perform_later(payloads)

      @hookshot_deliveries.each do |delivery|
        instrument_delivery(delivery)
        next if delivery.hookshot_payload.nil?
      end

      tags = GitHub::TaggingHelper.create_hook_event_tags(hook_event.event_type, hook_event.try(:action))
      tags << "site:#{GitHub.site}"
      count = payloads.sum { |payload| payload[:hooks].count }
      GitHub.dogstats.count("hooks.enqueued_per_hook.count", count, tags: tags + ["job:post-push-event-to-hookshot"])
    else
      PostPushEventToHookshotJob.perform_later(payloads)

      tags = GitHub::TaggingHelper.create_hook_event_tags(hook_event.event_type, hook_event.try(:action))
      count = payloads.sum { |payload|  payload[:hooks].count }
      GitHub.dogstats.count("hooks.enqueued_per_hook.count", count, tags: tags + ["job:post-push-event-to-hookshot"])
    end
  end

  # Public: Enqueues background jobs to push generated payloads to Hookshot
  # Because this delivery method pushes hook payloads into Redis to enqueue the
  # background job, it should be used with care to avoid a negative impact on
  # the amount of memory available to Redis.
  #
  # Returns nothing.
  def deliver_later
    unless @payloads_generated
      raise PayloadsNotGenerated, "you must call `generate_hookshot_payloads' " +
        "before calling `deliver_later'"
    end

    return if payloads.none?

    if GitHub.deliver_hooks_via_resque? || GitHub.deliver_hooks_via_aqueduct?
      tags = GitHub::TaggingHelper.create_hook_event_tags(hook_event.event_type, hook_event.try(:action))
      tags << "site:#{GitHub.site}"
      @hookshot_deliveries.each do |delivery|
        next if delivery.hookshot_payload.nil?

        if GitHub::Aqueduct.is_payload_size_large?(delivery.hookshot_payload.to_json.bytesize)
          PostToHookshotJob.perform_later(delivery.hookshot_payload)
          count = delivery.hooks.count
          GitHub.dogstats.count("hooks.enqueued_per_hook.count", count, tags: tags + ["job:post-to-hookshot"])
        else
          if GitHub.deliver_hooks_via_resque?
            delivery.hookshot_payload[:hooks].each do |hook_config|
              hook_payload = delivery.hookshot_payload.dup.tap do |hash|
                hash[:hook] = hook_config
                hash.delete(:hooks)
              end

              payload = hook_payload.merge(
                github_request_id: GitHub.context[:request_id],
                enqueued_at: (Time.now.to_f * 1_000).round,
              )

              Resque.enqueue(HookshotGoWebhooksJob, payload)
            end
          else
            PostHooksToAqueductJob.perform_later(delivery.hookshot_payload)
          end
          GitHub.dogstats.increment("hooks.enqueued_per_hook.count", tags: tags + ["job:post_hooks_to_aqueduct"])
        end

        instrument_delivery(delivery)
      end
    else
      payloads.each do |payload|
        PostToHookshotJob.perform_later(payload)
      end

      tags = GitHub::TaggingHelper.create_hook_event_tags(hook_event.event_type, hook_event.try(:action))
      count = payloads.sum { |payload|  payload[:hooks].count }
      GitHub.dogstats.count("hooks.enqueued_per_hook.count", count, tags: tags + ["job:post-to-hookshot"])
    end
  end

  def payloads
    @hookshot_deliveries.map(&:hookshot_payload).compact
  end
  # Public: Generates the event/hook's payloads for Hookshot delivery. This
  # method is available to front-load payloads before #deliver_later is called.
  # For example, where we may simply call #deliver_later in an after_destroy, we
  # can instead call #generate_hookshot_payloads in a before_destroy and then
  # call #deliver_later in an after_commit, avoiding communication with Redis
  # during a MySQL transaction.
  #
  # Returns nothing.
  def generate_hookshot_payloads
    @hookshot_deliveries = []
    @payloads_generated = true
    return if Rails.env.test? && !Hook.delivers_in_test?
    return unless hook_event.deliverable?
    return if hook_event.target_repository_disallows_hooks?

    tags = GitHub::TaggingHelper.create_hook_event_tags(hook_event.event_type, hook_event.try(:action))
    tags << "operation:generate_hookshot_payloads"
    GitHub.dogstats.time("hooks.time", tags: tags) do
      @hookshot_deliveries = deliveries
      deliveries.each do |delivery|
        begin
          next if delivery.hooks.none?
          payload = hookshot_payload(delivery)
          delivery.hookshot_payload = payload
        rescue PayloadTooLarge => e
          GitHub::Logger.log_exception(hook_event.guid, e)
          Failbot.report(e)
        end
      end
    end
  end

  # Public: Generates the event/hook's payloads for Hookshot delivery. This
  # method is available to front-load payloads before #deliver_later is called.
  # For example, where we may simply call #deliver_later in an after_destroy, we
  # can instead call #generate_hookshot_payloads in a before_destroy and then
  # call #deliver_later in an after_commit, avoiding communication with Redis
  # during a MySQL transaction.
  #
  # Returns nothing.
  def generate_push_event_hookshot_payloads
    if hook_event && !hook_event.is_a?(Hook::Event::PushEvent)
      raise "generate_push_event_hookshot_payloads cannot be called for any event other than a PushEvent"
    end
    # instantiate @deliveries while associated resources are still in the db
    @hookshot_deliveries = []
    @payloads_generated = true
    return if Rails.env.test? && !Hook.delivers_in_test?
    return unless hook_event.deliverable?
    return if hook_event.target_repository_disallows_hooks?

    tags = GitHub::TaggingHelper.create_hook_event_tags(hook_event.event_type, hook_event.try(:action))
    tags << "operation:generate_hookshot_payloads"
    GitHub.dogstats.time("hooks.time", tags: tags) do
      @hookshot_deliveries = deliveries
      deliveries.each do |delivery|
        next if delivery.hooks.none?
        payload = begin
          current_installation = installation_specifics_for(delivery.parent)

          webhook_payload = Hook::Payload::PushPayload.new(hook_event, include_git_data: false)

          payload = {
            parent: delivery.parent,
            guid: delivery.guid,
            event: hook_event.event_type,
            payload: webhook_payload.to_version(:v3).merge(current_installation),
            hooks: delivery.hooks.map do |hook|
              {
                  id: hook.id,
                  service: hook.name,
                  configuration: {
                    needs_public_key_signature: self.class.needs_public_key_signature?(hook),
                  },
                  headers: delivery.headers_for(hook),
                  data: hook.config,
                  callback_url: "#{GitHub.api_url}/hooks/#{delivery.guid}/#{hook.id}",
              }.tap do |hook_data|
                hook_data[:metadata] = {
                  repo_id: delivery&.target_repository&.id,
                  installation_id: current_installation.dig(:installation, :id),
                }
              end
            end,
          }
        end
        delivery.hookshot_payload = payload
      end
    end
  end

  # Builds a Hook::Delivery for each group of hooks. A deliveries hooks
  # must be for the same parent and payload version.
  #
  # Returns an array of Hook::Delivery objects
  def deliveries
    @deliveries ||= grouped_hooks.map do |(parent, version), hooks|
      Hook::Delivery.new(hook_event, parent, version, hooks)
    end
  end

  def self.needs_public_key_signature?(hook)
    target = hook.installation_target

    if target.respond_to?(:flipper_id)
      GitHub.flipper[:public_key_webhook_signing].enabled?(target)
    else
      false
    end
  end

  private

  # Private: Groups the subscribed hooks for the event by parent and payload version.
  # Hookshot can handle multiple hooks per call but expects each call to contain hooks
  # for a single parent and delivery payload.
  #
  # Example
  #
  #   delivery_system.grouped_hooks
  #   => {
  #        ["repository-1", :v3] => [hook1, hook2],
  #        ["repository-1", :v4] => [hook3],
  #        ["organization-42", :v3] => [hook4]
  #      }
  #
  # Returns a Hash with a tuple of [parent_id, version] for keys. The values are Arrays
  # of Hooks for each unique tuple.
  def grouped_hooks
    @grouped_hooks ||= begin
      subscribed_hooks = ActiveRecord::Base.connected_to(role: :reading) { hook_event.subscribed_hooks }

      # don't select service hooks only webhooks
      subscribed_hooks = subscribed_hooks.select(&:webhook?)

      subscribed_hooks.group_by do |hook|
        [hook.hookshot_parent_id, hook.payload_version]
      end
    end
  end

  # Private: The POST payload in the format that Hookshot expects for
  # an incoming delivery trigger.
  #
  # delivery - The Hook::Delivery being delivered
  #
  # Returns a Hash.
  def hookshot_payload(delivery)
    current_installation = installation_specifics_for(delivery.parent)

    payload = {
      parent: delivery.parent,
      guid: delivery.guid,
      event: hook_event.event_type,
      payload: delivery.payload.merge(current_installation),
    }.tap do |hash|
      hash[:hooks] = delivery.hooks.map do |hook|
        hook_config_for(delivery, hook, current_installation)
      end
    end

    payload_size = payload.to_json.bytesize
    tags = GitHub::TaggingHelper.create_hook_event_tags(hook_event.event_type, hook_event.try(:action))
    tags += ["is_large_aqueduct_payload:#{GitHub::Aqueduct.is_payload_size_large?(payload_size)}"]
    GitHub.dogstats.histogram("hooks.hookshot_payload.payload_size", payload_size, tags: tags)

    if payload_size > GitHub.hookshot_payload_size_limit
      delivery.hooks.each do
        GitHub.dogstats.histogram("hooks.hookshot_payload.payload_too_large", payload_size, tags: tags)
      end
      raise PayloadTooLarge, "Payload size of #{payload_size} exceeds limit: #{GitHub.hookshot_payload_size_limit}, will not be delivered to Hookshot."
    end
    payload
  end

  def hook_config_for(delivery, hook, current_installation)
    {
      id: hook.id,
      service: hook.name,
      configuration: {
        needs_public_key_signature: self.class.needs_public_key_signature?(hook),
      },
      headers: delivery.headers_for(hook),
      data: hook.config,
      callback_url: "#{GitHub.api_url}/hooks/#{delivery.guid}/#{hook.id}",
    }.tap do |hook_data|
      hook_data[:metadata] = {
        repo_id: delivery&.target_repository&.id,
        installation_id: current_installation.dig(:installation, :id),
      }
    end
  end

  # Private: POSTs the delivery payload to Hookshot using Hookshot::Client
  #
  # delivery - The Hook::Delivery being delivered
  #
  # Returns nothing. Raises a Hookshot::BadResponseError if the POST to
  # Hookshot was not successful.
  def post_to_hook_services(delivery)
    return if delivery.hooks.none? # All hook deliveries are muted

    begin
      payload = hookshot_payload(delivery)
      self.class.post_payload_to_hookshot(payload)
    rescue PayloadTooLarge => e
      GitHub::Logger.log_exception(hook_event.guid, e)
      Failbot.report(e)
    end
  end

  # Private: Publishes metadata about the delivery of each hook to hydro
  #
  # delivery - The Hook::Delivery that was delivered
  # payload - The payload that was delivered
  #
  # Returns an array of instrumented hooks.
  def instrument_delivery(delivery)
    return if GitHub.enterprise?

    delivery.hooks.each do |hook|
      GlobalInstrumenter.instrument("webhook.delivery_metadata", {
        hook: hook,
        safe_hook_url: safe_hook_url(hook),
        hook_event: hook_event,
        hook_payload: delivery.hookshot_payload,
      })
    end
  end

  # Private: Strips sensitive data from the hook url for logging to hydro
  #
  # hook - the hook we want a safe url for
  #
  # Returns the safe url
  def safe_hook_url(hook)
    url = URI.parse(hook.config["url"])
    url.user, url.password, url.query = nil
    url.to_s
  rescue URI::InvalidURIError
    ""
  end

  def hookshot_client_for(delivery)
    @hookshot_clients ||= {}
    @hookshot_clients[delivery.parent] ||= Hookshot::Client.for_parent(delivery.parent)
  end

  def installation_specifics_for(parent)
    integration_id = parent.sub("integration-", "") if parent.include? "integration-"
    return {} unless integration_id

    subscribed_installation =
      ActiveRecord::Base.connected_to(role: :reading) do
        hook_event.subscribed_installations_for(integration_id).select(:id).first
      end

    if subscribed_installation.present?
      { installation: {
        id: subscribed_installation.id,
        node_id: subscribed_installation.global_relay_id,
        },
      }
    else
      GitHub.dogstats.increment("hooks.subscribed_installation.missing", tags: ["installations:none"])
      {}
    end
  end

  # Private: instruments a DD timing to time requests to Hookshot and log some info before
  # and after each request.
  #
  # metric             - the metric name to send to DD
  # payload (optional) - the actual payload to be sent to hookshot as the first try
  #                      or as a redelivery
  #
  # Returns the result of the yielded block.
  def self.time_and_log_request(metric, payload = nil, tags = [])
    payload ||= {}
    log_data = payload.slice(:event, :guid, :hook_id)
    event = payload[:event]

    if event
      action = payload[:payload].try(:[], :action)
      tags += GitHub::TaggingHelper.create_hook_event_tags(event, action)
    end

    start = Time.now
    status, body = begin
      yield if block_given?
    rescue => e
      tags << "exception:#{e.class.name.underscore}"
      GitHub.dogstats.increment("hooks.delivery_error.count", tags: tags)
      raise e
    end
    ending = Time.now

    tags << "status:#{status}"

    request_ms = (ending - start)*1000
    GitHub.dogstats.timing(metric, request_ms, tags: tags)

    [status, body]
  end
  private_class_method :time_and_log_request
end
