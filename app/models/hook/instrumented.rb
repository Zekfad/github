# frozen_string_literal: true

module Hook::Instrumented
  extend ActiveSupport::Concern
  include Instrumentation::Model

  included do
    after_commit :instrument_create, on: :create
    after_update :instrument_events_changed, if: :saved_change_to_events?
    after_update :instrument_config_changed, if: :saved_change_to_config_attributes?
    after_update :instrument_active_changed, if: :saved_change_to_active?
    after_destroy :instrument_destroy
  end

  private

  def event_prefix
    :hook
  end

  # Instrumentation payload mixed into every call
  def event_payload
    {
      hook: self,
      hook_type: hook_type,
      name: display_name,
      webhook: webhook?,
      config: masked_config,
      events: events,
      active: active?
    }.merge(installation_target_event_payload)
  end

  def instrument_create
    instrument :create,
      creator: creator_name,
      oauth_application_id: oauth_application.try(:id),
      oauth_application: oauth_application.try(:name)
  end

  def instrument_destroy
    instrument :destroy
  end

  def instrument_events_changed
    instrument :events_changed,
      events_were: events_before_last_save
  end

  def instrument_config_changed
    instrument :config_changed,
      config_was: masked_config(config_attributes_before_last_save)
  end

  def instrument_active_changed
    instrument :active_changed,
      active_was: active_before_last_save
  end

  def installation_target_event_payload
    Hash.new.tap do |payload|
      if business_hook?
        payload[:business] = installation_target
      elsif org_hook?
        payload[:org] = installation_target
      elsif repo_hook?
        repo = installation_target
        payload[:repo] = repo
        payload[:org] = repo.organization if repo.in_organization?
        payload[:user] = repo.user if repo.owner&.user?
      end
    end
  end
end
