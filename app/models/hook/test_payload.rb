# rubocop:disable Style/FrozenStringLiteralComment

class Hook::TestPayload

  attr_reader :hook, :errors

  def initialize(hook)
    @hook = hook
    @errors = []
  end

  def repository
    @hook.installation_target if @hook.repo_hook?
  end

  def deliver
    return unless valid?

    repository.test_service(hook)
    true
  end

  def valid?
    clear_errors

    validate_hook_is_a_service
    validate_hook_supports_push
    validate_repo_has_pushes

    errors.empty?
  end

  private

  def clear_errors
    @errors = []
  end

  def validate_hook_is_a_service
    errors << "not supported on webhooks" unless hook.legacy_service?
  end

  def validate_hook_supports_push
    errors << "#{hook.display_name} doesn't support the push event" unless hook.events.include?("push")
  end

  def validate_repo_has_pushes
    errors << "you must push something to #{repository.nwo} before testing this hook" unless repository_has_pushes?
  end

  # Repository must have at least one Push record in order to send a test
  # payload.
  def repository_has_pushes?
    repository && Push.where(repository_id: repository.id).first
  end

end
