# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Event
  include Hook::Event::AttributesDependency
  include Hook::Event::RegistryDependency

  # Public: Returns an event class for a given event type.
  #
  # event_type - the type of event (e.g. push, ping)
  #
  # Returns a Hook::Event class
  # Raises NameError if the Hook::Event class is not found.
  def self.class_for_event_type(event_type)
    "Hook::Event::#{event_type.to_s.camelize}Event".constantize
  end

  # Public: instantiates and returns the correct Hook::Event
  # class for a given event type.
  #
  # event_type - the type of event (e.g. push, ping)
  # attributes - optional attributes to initialize the event with
  #
  # Returns a Hook::Event object
  # Raises NameError if the Hook::Event class is not found.
  def self.for_event_type(event_type, attributes = {})
    class_for_event_type(event_type).new attributes
  end

  # Public: Queues an event for delivery to Hookshot.
  #
  # arguments  - Hash of arguments needed to build the event
  #
  # Returns true if queued.
  def self.queue(attributes = {})
    event = new(attributes)
    event.attributes[:queued_at] = Time.now.to_f
    event.deliver_later
  end

  def initialize(attributes = {})
    attributes = attributes.symbolize_keys
    attributes[:triggered_at] = parse_triggered_at(attributes[:triggered_at])
    attributes[:delivered_hook_ids] ||= []
    super attributes
  end

  # Public: Returns the inferred event type from the event's class name.
  #
  # Examples
  #
  #   Hook::Event::PullRequestEvent.new.event_type => "pull_request"
  #
  # Returns the event type string.
  def event_type
    self.class.event_type
  end

  # Public: Returns if the class event is feature flaged.
  #
  # Returns a boolean
  def feature_flagged?
    self.class.feature_flagged?
  end

  # Public: Returns the actions that are feature flagged.
  #
  # Returns an array.
  def flagged_actions
    self.class.flagged_actions
  end

  # Public: Returns the guid for this event
  #
  def guid
    @guid ||= SimpleUUID::UUID.new(triggered_at).to_guid
  end

  # Public: Returns the repo this event targets if any. This should
  # be overridden in each specific Hook::Event model.
  #
  # Returns a Repository or nil
  def target_repository
  end

  # Public: Returns the organization this event targets if any. Can be
  # overridden by each Hook::Event model. By default it returns the target
  # repo's owner if that owner is an Organization.
  #
  # Returns an Organization or nil
  def target_organization
    return unless target_repository
    return unless target_repository.owner
    return unless target_repository.owner.organization?
    target_repository.owner
  end

  # Public: Returns the business this event targets if any. Can be
  # overridden by each Hook::Event model. By default it returns the target
  # organization's business if it is part of one.
  #
  # Returns a Business or nil
  def target_business
    return GitHub.global_business if GitHub.single_business_environment?
    return unless target_organization

    target_organization.business
  end

  # Public: Returns the listing this event targets if any. This should
  # be overridden in each specific Hook::Event model.
  #
  # Returns a Marketplace::Listing or nil
  def target_marketplace_listing
  end

  # Public: Returns the listing this event targets if any. This should
  # be overridden in each specific Hook::Event model.
  #
  # Returns a SponsorsListing or nil
  def target_sponsors_listing
  end

  # Public: Returns the user who triggered this event if any. This should
  # be overridden in each specific Hook::Event model.
  #
  # Returns a User or nil
  def actor
    raise NotImplementedError
  end

  # Public: Finds all hooks which should be triggered for this event.
  #
  # Returns an Array of Hooks.
  def subscribed_hooks
    @subscribed_hooks ||= begin
      business_hooks  = subscribed_hooks_for_parent(target_business)
      org_hooks  = subscribed_hooks_for_parent(target_organization)
      repo_hooks = subscribed_hooks_for_parent(target_repository)
      integration_hooks = subscribed_integration_hooks
      marketplace_listing_hooks = subscribed_hooks_for_parent(target_marketplace_listing)
      sponsors_listing_hooks = subscribed_hooks_for_parent(target_sponsors_listing)
      integrator_hooks = subscribed_integrator_hooks

      business_hooks | org_hooks | repo_hooks | integration_hooks |
        marketplace_listing_hooks | sponsors_listing_hooks | integrator_hooks
    end
  end

  # Public: Finds and instantiates a payload model for this event.
  #
  # Returns an instance of Hook::Payload
  def payload
    @payload ||= Hook::Payload.for_event(self)
  end

  # Public: Returns the payload hash that should be delivered to the given version.
  #
  # version - The symbol of the desired version (e.g. :v3)
  #
  # Returns a Hash.
  def to_payload_version(version)
    payload.to_version(version)
  end

  # Public: Returns any custom HTTP headers that should be attached to this hook delivery.
  #
  # Returns an Array. Each element is a Hash, with a "name" and "value".
  def headers_for(hook)
    []
  end

  # Internal: Kicks of the delivery process for the event
  # and all of its subscribed hooks.
  #
  # Returns nothing.
  def deliver
    return if Rails.env.test? && !Hook.delivers_in_test?
    return unless feature_flag_enabled?

    if target_repository_disallows_hooks?
      GitHub.dogstats.increment("hooks.target_repository_disallows_hooks")
      return
    end

    if !deliverable?
      GitHub.dogstats.increment("hooks.non_deliverable")
      return
    end

    return if importing?

    Hook::DeliverySystem.deliver(self)
  end

  # Internal: Enqueues a job that will deliver the event in the background.
  #
  # Returns nothing.
  def deliver_later
    return if importing?

    DeliverHookEventJob.perform_later(event_type, attributes)
  end

  # Internal: returns whether or not this event is fit to be delivered.
  # Events which need a mechanism for bailing out in certain cases should
  # us this.
  #
  # Returns Boolean.
  def deliverable?
    true
  end

  # Internal: Fetch the installation of a given Integration whose hook
  # is subscribed to this event.
  #
  # Returns an Array of IntegrationInstallations.
  def subscribed_installations_for(integration_id)
    target = if target_organization
      target_organization
    elsif target_repository
      target_repository.owner
    end

    return IntegrationInstallation.none unless target.present?

    scope = IntegrationInstallation.not_suspended.with_target(target).where(integration_id: integration_id)
    filtered_ids = HookEventSubscription.with_name_and_subscriber(event_type, "IntegrationInstallation", scope.ids).pluck(:subscriber_id)
    scope.with_ids(filtered_ids)
  end

  # Public: Returns whether or not the target item has the appropriate feature
  # flag enabled.
  #
  # Returns a Boolean.
  def feature_flag_enabled?
    return true unless feature_flagged?
    if flagged_actions.present?
      return true unless flagged_actions.include?(self.try(:action))
    end

    if target_organization
      self.class.feature_flag_enabled_for(target_organization)
    elsif target_repository
      self.class.feature_flag_enabled_for(target_repository)
    end
  end

  def target_repository_disallows_hooks?
    target_repository&.disabled? || target_repository&.advisory_workspace?
  end

  private

  def parse_triggered_at(time_or_stamp)
    return Time.now unless time_or_stamp.present?
    if time_or_stamp.kind_of?(Time)
      time_or_stamp
    elsif time_or_stamp.kind_of?(Integer)
      Time.at(time_or_stamp)
    else
      Time.parse(time_or_stamp)
    end
  end

  def subscribed_hooks_for_parent(parent)
    return [] unless parent
    parent.hooks.active.to_a.select { |hook| hook.call?(event_type, action: self.try(:action).try(:to_sym)) }
  end

  # Internal: Fetch all integration hooks that are subscribed to this event.
  #
  # Finds all application hooks where the application has an installation that:
  # - includes the repository associated with this event, or
  # - includes the organization associated with this event, and
  # - includes this event type.
  #
  # Returns an Array of Hooks.
  def subscribed_integration_hooks
    repository_based_installation_ids = if target_repository && ::Integration::Events.repository_event?(event_type)
      # Get the resources that the integrations needs to have access to see the event type
      resources_for_event = Integration::Events.repository_resources_for_event(event_type)

      # Get all integration installation ids on the repo with the required permissions
      IntegrationInstallation.ids_with_resources_on(subject: target_repository, resources: resources_for_event)
    else
      []
    end

    org_based_installation_ids = if target_organization && ::Integration::Events.organization_event?(event_type)
      # Get the resources that the integrations needs to have access to see the event type
      resources_for_event = Integration::Events.organization_resources_for_event(event_type)

      # Get all integration installation ids on the org with the required permissions
      IntegrationInstallation.ids_with_resources_on(subject: target_organization, resources: resources_for_event)
    else
      []
    end

    installation_ids = repository_based_installation_ids | org_based_installation_ids
    return Hook.none if installation_ids.empty?

    # Filter down to integration installation ids that are subscribed to the given event
    installation_ids = HookEventSubscription.with_name_and_subscriber(event_type, "IntegrationInstallation", installation_ids).pluck(:subscriber_id)
    return Hook.none if installation_ids.empty?

    # Map integration installations down to integration ids
    integration_ids = IntegrationInstallation.not_suspended.with_ids(installation_ids).distinct.pluck(:integration_id)
    return Hook.none if integration_ids.empty?

    Hook.where(installation_target_type: Integration.to_s).with_ids(integration_ids, field: :installation_target_id).active
  end

  # Internal: Fetch all integrator hooks that are subscribed to this event.
  #
  # Finds all hooks where the integration is directly subscribed, rather than
  # one of its integration versions or integration installations.
  #
  # Returns an Array of Hooks.
  def subscribed_integrator_hooks
    Hook.subscribed_to_integrator_event(event_type).active
  end

  # Internal: Returns whether or not unreleased hook events are visible to the
  # target organization.
  #
  # Returns a Boolean
  def preview_events_enabled?
    return false unless target_organization

    GitHub.flipper[:preview_hook_events].enabled?(target_organization)
  end

  # Internal: Is this github process in importing mode?
  #
  # Returns a boolean.
  def importing?
    GitHub.importing?
  end
end
