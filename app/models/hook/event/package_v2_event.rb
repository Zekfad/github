# frozen_string_literal: true

class Hook::Event::PackageV2Event < Hook::Event
    supports_targets *DEFAULT_TARGETS

    description "GitHub Packages published or updated in a repository."

    event_attr :registry_package, :version, :action, :actor_id, required: true

    feature_flag :ghcr_beta_webhooks

    # Should be passed in fully qualified from the original webhook request
    def package
      Hydro::Schemas::RegistryMetadata::V0::Entities::Package.new(registry_package)
    end

    # Should be passed in fully qualified from the original webhook request
    def package_version
      Hydro::Schemas::RegistryMetadata::V0::Entities::Version.new(version)
    end

    # The organization the package belongs to. V2 packages are scoped to org, not repository.
    def target_organization
      Organization.find_by(login: package.namespace)
    end

    # The user who performed the action.
    #
    # This will automatically be included in hook payloads.
    def actor
      @actor ||= User.find(actor_id)
    end

    def deliverable?
      package.present? && package_version.present? && actor.present?
    end
end
