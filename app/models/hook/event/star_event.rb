# frozen_string_literal: true

class Hook::Event::StarEvent < Hook::Event

  supports_targets *DEFAULT_TARGETS
  event_attr :user_id, :starred_id, :action, required: true
  event_attr :star_id

  description "A star is created or deleted from a repository."

  def actor
    @actor ||= User.find(user_id)
  end

  def star
    @star ||= Star.find_by_id(star_id)
  end

  def target_repository
    @target_repository ||= Repository.find_by_id(starred_id)
  end
end
