# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Event::IntegrationInstallationRepositoriesEvent < Hook::Event
  supports_targets Integration
  auto_subscribed
  description "Repositories changed on integration installation"

  event_attr :action, :installation_id, required: true
  event_attr :repositories_added, :repositories_removed, :actor_id, :repository_selection

  def actor
    @actor ||= User.find_by(id: actor_id) || User.ghost
  end

  def installation
    @installation ||= IntegrationInstallation.includes(:integration).find(installation_id)
  end

  def integration
    installation.integration
  end

  def subscribed_hooks
    return [] unless integration.subscribable_hook?

    # Dependabot consumes installtion-related events via hydro so we should
    # skip producing hooks since their payload is costly to generate
    return [] if integration.dependabot_github_app?

    [integration.hook]
  end
end
