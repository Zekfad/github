# frozen_string_literal: true

class Hook::Event::CheckRunEvent < Hook::Event
  include Hook::Event::ChecksDependency

  supports_targets(*DEFAULT_TARGETS)
  description "Check run is created, requested, rerequested, or completed."

  event_attr :check_run_id, :action, required: true
  event_attr :specific_app_only, :actor_id, :requested_action

  def check_run
    @check_run ||= CheckRun.find(check_run_id)
  end

  def check_suite
    @check_suite ||= check_run.check_suite
  end
end
