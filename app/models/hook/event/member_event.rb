# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Event::MemberEvent < Hook::Event
  supports_targets Business, *DEFAULT_TARGETS

  display_name "collaborator add, remove, or changed"
  description "Collaborator added to, removed from, or has changed permissions for a repository."

  event_attr :action, :actor_id, :repo_id, :user_id, required: true
  event_attr :changes

  def user
    @user ||= User.find(user_id)
  end

  def target_repository
    @repository ||= Repository.find(repo_id)
  end

  def actor
    return target_repository.owner unless actor_id
    @actor ||= User.find(actor_id)
  end

  def changes
    return unless changes_attr
    return unless permission_changed?

    attrs = {
      permission: {}
    }

    attrs[:permission][:from] = changes_attr[:old_permission] if old_permission_changed?
    attrs[:permission][:to] = changes_attr[:new_permission] if new_permission_changed?

    attrs
  end

  def deliverable?
    target_repository.present?
  end

  private

  def changes_attr
    attributes.with_indifferent_access[:changes]
  end

  def old_permission_changed?
    changes_attr.has_key?(:old_permission)
  end

  def new_permission_changed?
    changes_attr.has_key?(:new_permission)
  end

  def permission_changed?
    old_permission_changed? || new_permission_changed?
  end
end
