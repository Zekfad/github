# rubocop:disable Style/FrozenStringLiteralComment

require "set"

class Hook::Event
  class MissingRequiredAttribute < StandardError
    def initialize(class_name, missing_attr)
      super "#{class_name} fired with a missing required attribute: #{missing_attr}"
    end

    def failbot_context
      {
        "app" => "github-event-dispatch",
      }
    end
  end

  class MissingToBeRequiredAttribute < StandardError
    def initialize(class_name, missing_attr)
      super "#{class_name} fired with a missing attribute that is slated to be required: #{missing_attr}"
    end
  end

  class GhostUserAsActor < StandardError
    def initialize(class_name)
      super "#{class_name} fired with a ghost user as the actor"
    end

    def failbot_context
      {
        "app" => "github-event-dispatch",
      }
    end
  end

  module AttributesDependency
    extend ActiveSupport::Concern

    module ClassMethods
      attr_reader :valid_attribute_keys, :required_attribute_keys, :to_be_required_attribute_keys

      # Private: Used to define the available attributes for a given
      # event. Attributes passed to the Hook::Event on initialization
      # which are not explicitly defined will be ignored.
      #
      # attrs   - the list of attributes to define
      # options - optional event attribute configuration:
      #           :required - if true an error will be raised if
      #           the event is initialized without the required
      #           attributes
      #           :to_be_required - if true a needle will be sent to Haystack
      #           but will not stop the delivery of a webhook
      def event_attr(*attrs)
        options = attrs.extract_options!

        @valid_attribute_keys ||= Set.new
        @required_attribute_keys ||= Set.new
        @to_be_required_attribute_keys ||= Set.new

        @valid_attribute_keys.merge(attrs.map!(&:to_sym))
        @required_attribute_keys.merge(attrs) if options[:required]
        @to_be_required_attribute_keys.merge(attrs) if options[:to_be_required]

        attrs.each do |attr|
          define_method(attr) do
            attributes[attr]
          end

          define_method(:"#{attr}=") do |value|
            attributes[attr] = value
          end
        end
      end
      protected :event_attr

      def inherited(subclass)
        super

        # all events have a triggered_at attribute
        subclass.event_attr :triggered_at, required: true
        subclass.event_attr :delivered_hook_ids
      end
    end # ClassMethods

    attr_reader :attributes

    def initialize(attributes = {})
      @attributes = attributes.symbolize_keys.slice(*self.class.valid_attribute_keys)

      return if Rails.env.test? && !Hook.delivers_in_test?

      validate_required_attributes
      validate_to_be_required_attributes
      report_on_ghost_user
    end

    private

    def validate_required_attributes
      return unless self.class.required_attribute_keys

      self.class.required_attribute_keys.each do |required_attr|
        raise MissingRequiredAttribute.new(self.class.name, required_attr) unless attributes[required_attr].present?
      end
    end

    # Private: Validates the presence of attributes that will be required in
    # the future. If an attribute that has been marked as :to_be_required in the
    # event_attr and we're running in production, a needle will be sent to the
    # github-event-dispatch bucket and delivery of the webhook will proceed
    # without the attribute in the payload. If we're running in development or
    # test, an exception will be raised.
    #
    # Returns nothing.
    def validate_to_be_required_attributes
      return unless self.class.to_be_required_attribute_keys.present?

      self.class.to_be_required_attribute_keys.each do |required_attr|
        if attributes[required_attr].nil?
          error = MissingToBeRequiredAttribute.new(self.class.name, required_attr)

          if Rails.production?
            error.set_backtrace(caller)
            Failbot.report(error, app: "github-event-dispatch")
          else
            raise error
          end
        end
      end
    end

    # Private: Checks to see if the actor_id field exists, and, if it does
    # reports on if it is a ghost user.
    #
    # Returns nothing.
    def report_on_ghost_user
      return unless attributes.key?(:actor_id)
      return if User.ghost.try(:id).nil?

      if attributes[:actor_id] == User.ghost.id
        e = GhostUserAsActor.new(self.class.name)
        e.set_backtrace(caller)
        Failbot.report(e, attributes: attributes)
      end
    end
  end
end
