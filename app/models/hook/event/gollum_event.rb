# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Event::GollumEvent < Hook::Event
  supports_targets *DEFAULT_TARGETS
  display_name "wiki"
  description "Wiki page updated."

  event_attr :actor_id, :repository_id, :updates, required: true

  def repository
    @repository ||= Repository.find(repository_id)
  end
  alias_method :target_repository, :repository

  def actor
    @actor ||= User.find(actor_id)
  end

  def updates
    @updates ||= attributes[:updates].map(&:symbolize_keys)
  end
end
