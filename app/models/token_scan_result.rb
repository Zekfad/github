# frozen_string_literal: true

class TokenScanResult < ApplicationRecord::Notify
  extend GitHub::BackgroundDependentDeletes
  extend GitHub::SimplePagination

  areas_of_responsibility :token_scanning

  belongs_to :repository
  belongs_to :resolver, class_name: "User"

  has_many :token_scan_result_locations, -> {
    order(created_at: :asc, id: :asc)
  }
  alias_method :locations, :token_scan_result_locations
  destroy_dependents_in_background :token_scan_result_locations

  enum resolution: {
    revoked: 0,
    false_positive: 1,
    used_in_tests: 2,
    wont_fix: 3,
    reopened: 4,
  }

  scope :resolved, -> { where.not(resolution: [nil, :reopened]) }
  scope :unresolved, -> { where(resolution: [nil, :reopened]) }
  scope :for_token_types, -> (token_types) { where(token_type: token_types) }
  scope :included, -> { includes(:token_scan_result_locations).where(token_scan_result_locations: { ignore_token: "include" }) }

  scope :newest_first, -> { order(created_at: :desc, id: :desc) }
  scope :oldest_first, -> { order(created_at: :asc, id: :asc) }
  scope :most_recently_updated, -> { order(updated_at: :desc, id: :desc) }
  scope :least_recently_updated, -> { order(updated_at: :asc, id: :asc) }

  validates_presence_of :repository_id, :token_type
  validates_length_of :token_signature, is: 64
  validates_presence_of :resolver_id, :resolved_at, if: :resolution?

  after_create :mark_notify_subscribers

  # Find or create a token scan result for the given repository, token type,
  # and token signature.
  #
  # Returns the result that was either created or already existed.
  def self.create_from_found_token!(repo, token_type, token_signature)
    attributes = { token_type: token_type, token_signature: token_signature }
    retry_on_find_or_create_error do
      repo.token_scan_results.find_by(attributes) || repo.token_scan_results.create!(attributes)
    end
  end

  def first_location
    @first_location ||= included_locations.first
  end

  def included_locations
    @included_locations ||= locations.select { |location| location.include? }
  end

  def resolve!(resolution:, actor:)
    update!(resolution: resolution, resolver_id: actor.id, resolved_at: Time.zone.now)
    if resolution == :reopened
      update_locations_path_ignore
      GitHub.dogstats.increment("token_scanning_reopen_token")
    end
  end

  def resolved?
    resolution? && resolution != "reopened"
  end

  def notify_subscribers?
    @notify_subscribers == true && included_locations.any?
  end

  # Returns true if the location was found in an archive file. For example, zip, xlsx, pptx, docx etc.
  # These locations have start and end line of location set to 0.
  def found_in_archive?
    first_location.present? && first_location.found_in_archive?
  end

  private

  def update_locations_path_ignore
    repository = Repository.find_by_id(repository_id)
    token_scanning_yml_file = PreferredFile.find(directory: repository.root_directory, type: :token_scanning_configuration, subdirectories: [".github"])
    if token_scanning_yml_file
      token_scanning_config = TokenScanningConfigurationFile.new(token_scanning_yml_file)
      if token_scanning_config
        locations.each do |location|
          location.ignore_token = token_scanning_config.ignore_path?(location.path) ? 1 : 0
          location.save
        end
        GitHub.dogstats.increment("token_scanning_reopen_token_all_locations_ignored") if locations.all? { |location| location.exclude_by_path? }
      end
    end
  end

  def mark_notify_subscribers
    @notify_subscribers = true
  end
end
