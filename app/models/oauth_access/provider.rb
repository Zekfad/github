# frozen_string_literal: true

# Public: Interface for finding or creating OauthAccess records.
module OauthAccess::Provider
  extend ActiveSupport::Concern

  # Finds an OauthAccess by its temporary code.
  #
  # code    - String code of a matching OauthAccess.
  #
  # Returns authorized OauthAccess if a matching record is found.
  # Returns nil if nothing is found.
  def access_for_code(code)
    return nil if code.blank? || code == 0
    access = accesses.find_by_code(code.to_s)
    return nil if access && access.created_at < OauthAccess::CODE_EXPIRY.ago
    access
  end

  # Creates an OauthAccess for the given user.  This OauthAccess is still
  # unauthorized (it has a `nil` token), but comes ready with a temporary code.
  #
  # user    - User that is accessing the current OauthApplication.
  # options - Hash of OAuth2-specific options for this user's access permissions.
  #           :scope                      - String comma separated list of granted
  #                                         scopes: "user,repo" (optional).
  #           :requested_scope            - String comma separate list of requested
  #                                         scopes: "user,repo" (optional).
  #           :integration_version_number - String representing the Integration
  #                                         version number that should be assigned
  #                                         to the OAuthAccess
  #           :user_session               - A UserSession for the given `user`.
  # Returns newly created OauthAccess record.
  def grant(user, options = nil)
    options ||= {}

    access = accesses.
      build(user: user).
      grant(options[:scope], options[:requested_scope], options[:integration_version_number])

    # Grant Organization::CredentialAuthorizations for each organization
    # that has an active external identity.
    if (user_session = options[:user_session])
      return access if user != user_session.user

      saml = Platform::Authorization::SAML.new(session: user_session)
      Organization.where(id: saml.authorized_organization_ids).each do |organization|
        ActiveRecord::Base.connected_to(role: :writing) do
          Organization::CredentialAuthorization.grant(organization: organization, credential: access, actor: user)
        end
      end
    end

    access
  end

  # Creates an OauthAccess for the given user with a scoped installation.
  # This OauthAccess is authorized (it has a `nil` token), but comes ready with a temporary code.
  #
  # access  - The OauthAccess that is used for making the subsequent new OauthAccess.
  # options - Hash of options the installation bound to the OauthAccess.
  #           :permissions    - Hash of permissions to limit the installation.
  #                             permissions: { "metadata" => :read } (optional).
  #           :repository_ids - The Array of Integer IDs used to limit the repository
  #                             access on the installation.
  #                             repository_ids: [8,6,7,5] (optional).
  #
  # Returns an Array.
  def grant_scoped_access_from(access, target, permissions: nil, repository_ids: nil)
    if access.installation
      return invalid_token_error
    end

    new_access = grant(access.user)
    _, error   = grant_scoped_installation_on(new_access, target, permissions: permissions, repository_ids: repository_ids)

    if error
      new_access.destroy
      return [nil, error]
    end

    # Bless the new access with a credential authorization to
    # the target that the old one had.
    if (saml_enforcement_policy?(access, target))
      authorization = Organization::CredentialAuthorization.grant(
        organization: organization,
        credential: new_access,
        actor: new_access.user,
      )

      if authorization.nil?
        new_access.destroy
        return token_creation_failure_error
      end
    end

    new_access.reload
    return [new_access, nil]
  end

  # Creates an single repository scoped installation on the given OauthAccess.
  #
  # access  - The OauthAccess that is used for making the subsequent new OauthAccess.
  # options - Hash of options the installation bound to the OauthAccess.
  #           :repository_id - The primary of the ID to scope the installation to.
  #                            repository_id: 1 (optional).
  #
  # Returns an Array.
  def grant_repository_scoped_installation_on(access, repository_id: nil)
    if access.installation
      return invalid_token_error
    end

    unless repository_id
      if Apps::Internal.capable?(:per_repo_user_to_server_tokens_required, app: self)
        return missing_repository_error
      end

      # If a repository_id is not passed and the app doesn't require it,
      # we should silently pass through as nothing failed.
      return [nil, nil]
    end

    repository = Repository.find_by(id: repository_id)
    user       = access.user

    return repository_not_found_error unless repository
    return repository_not_found_error unless repository.readable_by?(user)

    access, error = grant_scoped_installation_on(access, repository.owner, permissions: nil, repository_ids: [repository.id])
    return [nil, error] if error

    access.reload
    [access.installation, nil]
  end

  private

  def create_scoped_installation(target, permissions:, repository_ids:)
    result = if Apps::Internal.capable?(:installed_globally, app: self)
      repositories = repositories_for(installation: GlobalIntegrationInstallation.new(self), repository_ids: repository_ids)
      SiteScopedIntegrationInstallation::Creator.perform(self, target, permissions: permissions, repositories: repositories, expires: false)
    else
      installation = IntegrationInstallation.with_target(target).find_by(integration: self)
      return missing_installation_error unless installation

      repositories = repositories_for(installation: installation, repository_ids: repository_ids)
      ScopedIntegrationInstallation::Creator.perform(installation, permissions: permissions, repositories: repositories, expires: false)
    end

    if result.success?
      return [result.installation, nil]
    end

    installation_failure_error(result)
  end

  def ensure_organization_credential_authorized(access:, target:)
    return unless saml_enforcement_policy?(access, target)

    credential = Organization::CredentialAuthorization.by_organization_credential(
      organization: target,
      credential: access,
    ).first

    return if credential && credential.active?

    sso_required_error
  end

  def grant_scoped_installation_on(access, target, permissions:, repository_ids:)
    unless Apps::Internal.capable?(:per_repo_user_to_server_tokens, app: self)
      return feature_not_enabled_error
    end

    saml_error = ensure_organization_credential_authorized(access: access, target: target)
    return saml_error if saml_error

    installation, error = create_scoped_installation(target, permissions: permissions, repository_ids: repository_ids)
    return [nil, error] if error

    access.update(installation: installation)
    access.reload

    [access, nil]
  end

  def saml_enforcement_policy?(access, target)
    return false unless Apps::Internal.capable?(:saml_sso_required, app: self)
    return false unless target.organization?
    return false unless access.saml_enforceable?

    saml_enforcement_policy = Organization::SamlEnforcementPolicy.new(organization: target, user: access.user)
    return false unless saml_enforcement_policy.enforced?

    saml_enforcement_policy.enforced?
  end

  def repositories_for(installation:, repository_ids:)
    case installation
    when GlobalIntegrationInstallation
      unless repository_ids
        return SiteScopedIntegrationInstallation::Creator::INSTALL_ON_ALL_REPOSITORIES
      end

      Repository.with_ids(repository_ids)
    when IntegrationInstallation
      if repository_ids
        return Repository.with_ids(repository_ids)
      end

      if installation.installed_on_all_repositories?
        return ScopedIntegrationInstallation::Creator::INSTALL_ON_ALL_REPOSITORIES
      end

      installation.repositories
    else
      []
    end
  end

  ##########
  # Errors #
  ##########

  def token_creation_failure_error
    [
      nil,
      {
        error: :token_creation_failure,
        error_description: "We failed to grant the token access to the resource(s) requested.",
        error_uri: "https://githubber.com/article/technology/dotcom/internal-apps",
      }
    ]
  end

  def feature_not_enabled_error
    [
      nil,
      {
        error: :feature_not_enabled,
        error_description: "This feature is not enabled for your GitHub App.",
        error_uri: "https://githubber.com/article/technology/dotcom/internal-apps",
      },
    ]
  end

  def installation_failure_error(result)
    [
      nil,
      {
        error: :installation_creation_failed,
        error_description: result.error,
        error_uri: "https://githubber.com/article/technology/dotcom/internal-apps",
      },
    ]
  end

  def invalid_token_error
    [
      nil,
      {
        error: :invalid_token,
        error_description: "A scoped token cannot create another scoped token.",
        error_uri: "https://githubber.com/article/technology/dotcom/internal-apps",
      },
    ]
  end

  def missing_installation_error
    [
      nil,
      {
        error: :installation_missing_access,
        error_description: "Your app does not have access to the given target.",
        error_uri: "https://githubber.com/article/technology/dotcom/internal-apps",
      },
    ]
  end

  def missing_repository_error
    [
      nil,
      {
        error: :missing_repository,
        error_description: "This application requires that a repository is provided to redeem the OAuth token.",
        error_uri: "https://githubber.com/article/technology/dotcom/internal-apps",
      },
    ]
  end

  def repository_not_found_error
    [
      nil,
      {
        error: :repository_not_found,
        error_description: "The repository requested could not be found.",
        error_uri: "https://githubber.com/article/technology/dotcom/internal-apps",
      },
    ]
  end

  def sso_required_error
    [
      nil,
      {
        error: :sso_required,
        error_description: "Resource protected by Organization SAML enforcement",
        error_uri: "https://githubber.com/article/technology/dotcom/internal-apps",
      },
    ]
  end
end
