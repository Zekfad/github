# frozen_string_literal: true

class DraftIssueReferenceFilter < HTML::Pipeline::Filter
  def initialize(text:, viewer:)
    super("")
    @text = text.strip
    @viewer = viewer
    scan_for_references
  end

  def single_reference
    if @non_reference_text.empty? && @references.length == 1
      @references.first
    else
      nil
    end
  end

  private

  REFERENCE_PATTERNS = Regexp.union(
    GitHub::HTML::IssueMentionFilter::REPO_ISSUE_REFERENCE,
    GitHub::HTML::IssueMentionFilter::FULL_URL_ISSUE_MENTION,
  )

  def scan_for_references
    references = []

    @non_reference_text = @text.gsub(REFERENCE_PATTERNS) do
      # In this loop, we repeatedly identify and replace issue references with
      # the value yielded in each iteration; when we're done, `gsub` returns
      # the final result of all of our subsitutions.

      match = Regexp.last_match

      # REPO_ISSUE_REFERENCE match e.g. "github/memex#123".
      repo_prefix, repo_nwo, repo_issue_number = match[1], match[2], match[3]
      if repo_issue_number && repo_nwo.include?("/")
        # Full name with owner in reference
        references << [repo_nwo, repo_issue_number]

        # Remove nwo and issue number but retain prefix text
        next repo_prefix
      end

      # FULL_URL_ISSUE_MENTION match e.g. "https://github.com/github/memex/issues/123".
      url_nwo, url_issue_number = match[4], match[5]
      if url_issue_number
        references << [url_nwo, url_issue_number]

        # Remove URL
        next ""
      end

      # We should never get here because the above cases should be exhaustive.
      # As a fallback though, return the unmodified match string.
      match[0]
    end

    @references = references.reduce([]) do |valid_references, (repository, number)|
      repository = find_repository(repository)
      next valid_references unless repository&.readable_by?(@viewer)

      issue = repository.issues.includes(:pull_request).find_by_number(number)
      next valid_references unless issue

      valid_references << (issue.pull_request || issue)
    end

    nil
  end
end
