# frozen_string_literal: true

class MemexProjectItemSerializer

  attr_reader :viewer, :memex, :memex_items, :sort_column, :sort_direction

  def initialize(viewer:, memex:, items:, sort_column: nil, direction: nil)
    @viewer = viewer
    @memex = memex
    @memex_items = items
    @sort_column = memex.find_column_by_name_or_id(sort_column)
    @sort_direction = direction
  end

  class Result

    attr_reader :items, :active_sort

    def initialize(items:, active_sort:)
      @items = items
      @active_sort = active_sort
    end

  end

  def result
    @result ||= Result.new(items: sorted_memex_items, active_sort: item_sorter.active_sort)
  end

  private

  def sorted_memex_items
    @sorted_memex_items ||= item_sorter.items
  end

  def serialized_memex_items
    @serialized_memex_items ||= begin
      items_with_redactions =
        MemexProjectItemRedactor.new(viewer: viewer, items: memex_items).items

      serialized_memex_items = items_with_redactions.map do |item|
        item.to_hash(columns: memex.visible_columns)
      end
    end
  end

  def item_sorter
    @item_sorter ||= MemexProjectItemSorter.new(items: serialized_memex_items,
                                                column: sort_column,
                                                direction: sort_direction)
  end

end
