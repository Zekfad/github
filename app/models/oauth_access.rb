# rubocop:disable Style/FrozenStringLiteralComment

# An OauthAccess represents a user giving an OauthApplication access to make
# requests on their behalf. OauthAccesses are scoped to allow granular access
# to resources.
class OauthAccess < ApplicationRecord::Domain::Integrations
  VALID_INSTALLATION_TYPES = %w(ScopedIntegrationInstallation SiteScopedIntegrationInstallation)
  attr_accessor :integration_version_number
  areas_of_responsibility :ecosystem_apps

  include GitHub::UtcTimestamp
  utc_timestamp :expires_at

  # Code/Token entropy stuff
  CODE_BYTES  = 10
  TOKEN_BYTES = 20

  # Require code to be a 20 hex character string
  CODE_PATTERN = /\A[a-f0-9]{20}\z/

  # Require token to be a 40 hex character string
  TOKEN_PATTERN = /\A[a-f0-9]{40}\z/

  # Require token_last_eight to be a 8 hex character string
  TOKEN_LAST_EIGHT_PATTERN = /\A[a-f0-9]{8}\z/

  # Require hashed_token to be a 44 character base64 string (base64 of 32 byte hash)
  HASHED_TOKEN_PATTERN = /\A[A-Za-z0-9+\/=]{44}\z/

  # Use SHA256 for hashing tokens
  HASHED_TOKEN_DIGEST = Digest::SHA256

  DEFAULT_TOKEN_EXPIRY = 8.hours

  # We return a fresh token on every OAuth dance. Since we do not enforce an
  # expiration using OAuth refresh tokens, we need another way to limit
  # unbounded growth of OAuthAccess records over time. We will do this by
  # restricting the number of accesses that can be created for a (user,
  # application, and scope) tuple. By using this tuple we minimize the
  # possibility of scenarios such as the following:
  #  * User performs one OAuth dance with repo scope.
  #  * User performs many more OAuth dances with the default empty scope.
  #  * The repo scope token gets destroyed because it is the oldest.
  MAXIMUM_ACCESSES_FOR_APP = 10

  # A small number of applications have been affected by our newly introduced
  # limits on OAuth accesses (MAXIMUM_ACCESSES_FOR_APP). Since this change was
  # not announced in advance, we are allowing a temporary override for a handful
  # of applications that were designed in a way that relied on, effectively, an
  # infinite number of accesses for a user. The intent is for these developers
  # to update their design to support our new limits. This is a reasonable
  # expectation in so far as most OAuth providers have similar limits on the
  # number of accesses an application can create. This is not meant as a
  # long-term solution for these applications. As a result, the expectation is
  # that an override should only be used for 1-2 months. The below hash maps an
  # `application_id` to the temporary limit. If at all possible, try to keep the
  # limit below 100. Alowing more than 100 accesses may require a transition to
  # destroy excess tokens after the temporary limit is removed.
  MAXIMUM_ACCESSES_FOR_APP_OVERRIDES = {
    # AWS CodePipeline - Added 7/28/2016
    189159 => 100,
    # AWS CodeDeploy - Added 7/28/2016
    140455 => 100,
    # Okta - Added 2/28/2018
    475360 => 100,
    # Azure AD SCIM Provisioning - Added 3/27/2020
    524102 => 100,
  }.freeze

  MAXIMUM_ACCESSES_FOR_EXPIRING_APP = 50

  # Any access looked up using `code` should be considered expired after 10
  # minutes.
  CODE_EXPIRY = 10.minutes

  TOKENS_EXCLUDED_FROM_INTEGRATION_TOKEN = %w[site_admin]

  SITE_ADMIN_SCOPES = %w[site_admin devtools biztools].to_set

  SAML_ENFORCEMENT_DATE = Time.zone.parse("November 8th, 2019 09:00 PST")

  # NOTE: Scope are defined in app/api/access_control.rb

  # Records the association that is being destroyed and destroying this record
  # in the process.
  attr_accessor :destroyed_by_association_parent

  def self.normalize_scopes(scopes, options = nil)
    Api::AccessControl.normalize_scopes(scopes, options)
  end

  def self.valid_scopes(scopes)
    Array(scopes).select { |sc| Api::AccessControl.acceptable_scopes[sc.to_s] }
  end

  def self.invalid_scopes(scopes)
    Array(scopes).reject { |sc| Api::AccessControl.acceptable_scopes[sc.to_s] }
  end

  # Internal: Returns Hash of valid hidden OAuth scopes.
  #
  # user - Optional User object.
  #
  # Hash object will include all hidden scopes as String keys and a Boolean
  # value if the user is eligible to enable the scope.
  def self.valid_hidden_scopes(user)
    {
      "site_admin" => !!user&.site_admin?,
      "devtools" => !!user&.devtools_scope?,
      "biztools" => !!user&.biztools_scope?,
    }
  end

  # Filter an array of scopes to only those that are public
  # on the OAuth authorization form.
  #
  # Returns an Array of String scopes
  def self.filter_public_scopes(scopes = nil)
    Array(scopes).
      select { |sc| Api::AccessControl.public_scopes[sc.to_s] }.
      compact.
      uniq.
      sort
  end

  # Public: Lookup an OauthAccess by token
  #
  # This method hashes token and then looks up the OauthAccess by hashed_token.
  # In the future token will be deprecated and removed from the DB to improve
  # security.
  #
  # Returns OauthAccess for token or nil otherwise.
  def self.find_by_token(token)  # rubocop:disable GitHub/FindByDef
    return unless token.is_a?(String)
    find_by_hashed_token(hash_token(token))
  end

  # Internal - intended only for the SecretScanningAPI: Find OauthAccess records for the given tokens.
  #
  # tokens - An array of String tokens.
  #
  # Returns a Hash with tokens as the keys and OauthAccess records as values
  def self.for_tokens(tokens)
    hashed_tokens = tokens.each_with_object({}) do |token, hash|
      hash[token] = hash_token(token)
    end

    accesses = where(hashed_token: hashed_tokens.values).
      includes(:user).
      index_by(&:hashed_token)

    tokens.each_with_object({}) do |token, hash|
      hash[token] = accesses[hashed_tokens[token]]
    end
  end

  # Public: Lookup up an OauthAccess by token, ensuring it is not expired
  #
  # hashed: whether the token being passed is already hashed.
  #
  # Returns OauthAccess for active tokens or nil  otherwise
  def self.with_active_token(token, hashed: false)
    return if token.blank?

    token = hash_token(token) unless hashed

    access = find_by_hashed_token(token)
    return unless access

    return access unless access.expired?

    nil
  end


  # Public: The User record that granted this access.
  belongs_to :user
  validates_presence_of :user_id
  validate :owner_is_human, on: :create
  validate :owner_meets_email_verification_requirements, on: :create, unless: :application_is_github_full_trust?

  # Public: The OauthApplication or Integration that has been granted access.
  belongs_to :application, polymorphic: true
  # Additional relationships specified with conditions, for internal use,
  # rather than needing to provide these details in explicit JOIN statements,
  # to allow calling methods on these as preloaded polymorphic associations.
  belongs_to :oauth_application, -> { where("oauth_accesses.application_type = ?", "OauthApplication") },
    foreign_key: "application_id"

  belongs_to :integration, -> { where("oauth_accesses.application_type = ?", "Integration") },
    foreign_key: "application_id"

  validates_presence_of :application_id
  validates_presence_of :application_type

  # Public: The OauthApplication that has been granted access.
  belongs_to :authorization, class_name: "OauthAuthorization"

  # Public: PublicKeys created by this access.
  has_many :public_keys

  # Public: authorizations this credential has for an Organization.
  has_many :credential_authorizations,
    class_name: "Organization::CredentialAuthorization",
    as: :credential,
    dependent: :destroy

  has_one :refresh_token, as: :refreshable, dependent: :destroy

  belongs_to :installation, polymorphic: true

  # TODO: Code expiration in 10 minutes (max recommended).
  # Public: String temporary code to be exchanged for a token.
  # column :code
  validates_uniqueness_of :code, scope: :application_id, allow_nil: true, case_sensitive: true
  validates_format_of :code, with: CODE_PATTERN, allow_nil: true

  # Public: String last eight characters of token.
  # column :token_last_eight
  validates_format_of :token_last_eight, with: TOKEN_LAST_EIGHT_PATTERN, allow_nil: true

  # Public: String base64 encoded SHA256 hash of token.
  # column :hashed_token
  validates_uniqueness_of :hashed_token, allow_nil: true, case_sensitive: true
  validates_format_of :hashed_token, with: HASHED_TOKEN_PATTERN, allow_nil: true

  # Public: String description of this access (used for personal access tokens)
  # column :description
  validates_presence_of :description, if: :personal_access_token?
  validates_uniqueness_of :description, scope: [:user_id, :application_id, :fingerprint],
    if: :personal_access_token?, case_sensitive: true

  # All PATs must have a unique authorization. However, we allow `nil` until all
  # existing PATs have been backfilled.
  validates_uniqueness_of :authorization_id, if: :personal_access_token?, allow_nil: true

  # Public: String fingerprint helps OAuth applications distinguish between
  # multiple authorizations for a user for a given app. It is used mostly for
  # desktop/mobile applications that create multiple tokens for a single user,
  # where each token is used on a different physical device that the user owns.
  # column :fingerprint
  before_validation :set_fingerprint
  validates_uniqueness_of :fingerprint, allow_nil: true,
    scope: [:user_id, :application_id], case_sensitive: true

  validates :installation_type, inclusion: VALID_INSTALLATION_TYPES, allow_nil: true

  after_commit :instrument_creation, on: :create

  # NOTE: Due to the usage of scopes_changed? in the callback which delegates to
  # a serialized column, commit callbacks can't be used.
  before_update :instrument_update # rubocop:disable GitHub/AfterCommitCallbackInstrumentation

  after_commit :instrument_deletion, on: :destroy

  validate :scopes_are_allowed
  validate :matches_authorization, if: :authorization

  after_validation :translate_errors

  before_create :create_authorization
  before_create :limit_accesses_for_authorization_for_oauth_app, if: :oauth_application_type?
  before_create :limit_accesses_for_authorization_for_integration, if: :integration_application_type?
  before_save :update_authorization, if: :authorization

  validate :ensure_integration_capabilities, if: :installation

  after_save :destroy_active_credential_authorizations, if: :personal_access_token?
  after_destroy :destroy_authorization, if: :authorization

  delegate :github_full_trust?, to: :application, prefix: "application_is", allow_nil: true

  scope :for_client_id, lambda { |client_id|
    if client_id == OauthApplication::PERSONAL_TOKENS_CLIENT_ID
      where(application_id: OauthApplication::PERSONAL_TOKENS_APPLICATION_ID)
    elsif client_id =~ Integration::KEY_PATTERN_V1
      includes(:integration).where("integrations.key = ?", client_id).references(:integration)
    else
      includes(:oauth_application).where("oauth_applications.key = ?", client_id).references(:oauth_application)
    end
  }

  # Access tokens associated with the generic personal token application id.
  scope :personal_tokens, -> { where(application_id: OauthApplication::PERSONAL_TOKENS_APPLICATION_ID) }

  # OAuth accesses for applications that are true third-party applications
  # (excludes personal tokens).
  scope :third_party, lambda {
    joins(:oauth_application). \
    where("`application_id` != ? AND `oauth_applications`.`full_trust` = 0", OauthApplication::PERSONAL_TOKENS_APPLICATION_ID)
  }

  # OAuth accesses for applications that are GitHub Apps
  # (excludes those for OAuth Applications)
  scope :github_apps, -> {
    joins(:integration). \
    where(application_type: "Integration")
  }

  # OAuth accesses for applications that are full_trust and managed by the GitHub
  # organization (excludes personal tokens).
  scope :github_owned, lambda {
    joins(:oauth_application).
    order("oauth_applications.name ASC").
    where(
      "`application_id` != ? AND `oauth_applications`.`full_trust` = 1 AND `oauth_applications`.`user_id` = ?",
      OauthApplication::PERSONAL_TOKENS_APPLICATION_ID,
      GitHub.trusted_apps_owner_id,
    )
  }

  scope :for_oauth_applications_and_public_integrations, -> {
    joins("LEFT OUTER JOIN integrations ON integrations.id = oauth_accesses.application_id").
    where("((oauth_accesses.application_type = 'Integration' AND integrations.public = true) OR (oauth_accesses.application_type = 'OauthApplication'))")
  }

  # Public: Serialized attributes
  # column :raw_data
  serialize :raw_data, Coders::Handler.new(Coders::OauthAccessCoder)

  # Public: String array of scopes this access has been granted.
  # array :scopes

  # Public: String array of scopes the application originally requested for
  # this access. If this value is not the same as :scopes, the user has
  # edited the oauth access request.
  # array :requested_scopes

  # Public: String informational note (used for personal access tokens) about
  # this access.
  # string :note

  # Public: String information note url (used for tokens created via the API)
  # about this access.
  # string :note_url
  delegate *Coders::OauthAccessCoder.members, to: :raw_data
  delegate :scopes_changed?, to: :raw_data

  # Public: Generate a new random token for the access.
  #
  # Returns a base64 String.
  def self.random_token
    SecureRandom.hex(TOKEN_BYTES)
  end

  # Public: Hash token for server side persistence.
  #
  # token - A String
  #
  # Returns hashed base64 String.
  def self.hash_token(token)
    HASHED_TOKEN_DIGEST.base64digest(token)
  end

  # Public: Generates a new random token pair, unhashed and hashed.
  #
  # Returns base64 String and base64 hashed version of the first String.
  def self.random_token_pair
    token = random_token
    return token, hash_token(token)
  end

   # Public: Generate a new random code for the access.
  #
  # Returns a String.
  def self.random_code
    SecureRandom.hex(CODE_BYTES)
  end

  # Public: Indicates whether this is a personal access token.
  #
  # Returns a Boolean.
  def personal_access_token?
    application_id == OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
  end

  # Gets the attached OauthApplication, or a default OauthApplication for
  # OauthAccess records created through the API.
  #
  # Returns an OauthApplication.
  def safe_app
    app = application if application_id.to_i > 0
    app || (@safe_app ||= OauthApplication.default(description, note_url))
  end

  # Public: Grants the given scopes for this OauthAccess.  Also resets the code
  # and clears token.
  #
  # scopes              - String comma-separated scopes that were granted (optional).
  # requested_scopes    - String comma-separated scopes that were requested (optional).
  #
  # Returns self (an OauthAccess).
  def grant(scopes = nil, requested_scopes = nil, integration_version_number = nil)
    ActiveRecord::Base.connected_to(role: :writing) do

      # Integration accesses do not have OAuth scopes, so we can skip
      # the setting of scopes
      unless self.integration_application_type?
        self.scopes = self.class.normalize_scopes(scopes, visibility: :all)
        self.requested_scopes = self.class.normalize_scopes(requested_scopes, visibility: :all)
      end

      self.code = self.class.random_code
      self.token_last_eight = nil
      self.hashed_token = nil
      self.integration_version_number = integration_version_number # won't be persisted to the database, we need this to correctly honor the user's choice of GitHub App permissions from the OAuth page
      clear_invalid_scopes
      save!
    end
    self
  end

  # Public: Set scopes for this token without resetting the token or code.
  #
  # new_scopes - String or Array of scopes (optional).
  #
  # Returns an OauthAccess.
  def set_scopes(new_scopes = nil)
    ActiveRecord::Base.connected_to(role: :writing) do
      self.requested_scopes = scopes unless self.requested_scopes
      self.scopes = self.class.normalize_scopes(new_scopes, visibility: :all)
      clear_invalid_scopes
      save!
    end

    self
  end

  # Public: Adds scopes to a token, optionally removing existing scopes,
  #         without resetting the token.
  #
  # new_scopes       - String or Array of scopes to add.
  # scopes_to_remove - String or Array of scopes to remove.
  #
  # Returns an OauthAccess.
  def change_scopes(new_scopes, scopes_to_remove = nil)
    ActiveRecord::Base.connected_to(role: :writing) do
      oauth_scopes = access_level
      oauth_scopes -= self.class.normalize_scopes(scopes_to_remove, visibility: :all)
      oauth_scopes << self.class.normalize_scopes(new_scopes, visibility: :all)
      self.scopes   = self.class.normalize_scopes(oauth_scopes, visibility: :all)

      save!
    end
  end

  # Public: Return the list of scopes as a sorted, comma-separated String.
  #
  # Returns a String.
  def scopes_string
    Array(scopes).map { |scope| scope.to_s }.sort.join(",")
  end

  # Public: Gets the full list of approved access levels.  Access levels are
  # just valid scopes as Strings.  Invalid scopes are safely ignored.
  #
  # Returns an Array of Strings.
  def access_level
    @access_level ||= access_level!
  end

  def access_level_set
    @access_level_set ||= Set.new(access_level)
  end

  def scopes?(*scopes)
    return false if self.scopes.nil?
    scopes.any? { |scope| access_level_set.include?(scope.to_s) }
  end

  # Public: Tells if this access token has the original requested scopes
  # available.
  #
  # Returns truthy if the original requested_scopes are identity to the current
  # scopes.
  def original_scopes_granted?
    requested_scopes.nil? || (scopes || []).to_set == (requested_scopes || []).to_set
  end

  # Public: Reset the authorization code on this access.
  #
  # Returns nothing.
  def reset_code
    ActiveRecord::Base.connected_to(role: :writing) do
      # While we ensure all new records have valid scopes, we need to clean
      # up scopes for legacy records that may have invalid scopes stored in the
      # DB.
      clear_invalid_scopes
      self.code = self.class.random_code
      save!
    end
  end

  # Public: Clear the authorization code on this access and sets a new token
  #
  # Returns the token.
  def redeem
    ActiveRecord::Base.connected_to(role: :writing) do
      self.code = nil

      response = if token_refreshable?
        build_refresh_token
        # reset_token calls save! which will persist the refresh_token
        token = reset_token(bump_expiration: token_refreshable?)
        [token, refresh_token.token]
      else
        reset_token
      end

      response
    end
  end

  # Public: Reset the authorization token on this access.
  #
  # Returns String token.
  def reset_token(bump_expiration: false)
    token, hashed_token = self.class.random_token_pair
    # Save this data for instrumentation.
    payload = {
      old_token_last_eight: self.token_last_eight,
      old_hashed_token: self.hashed_token
    }
    ActiveRecord::Base.connected_to(role: :writing) do
      last_operations = DatabaseSelector::LastOperations.from_token(token)
      self.token_last_eight = token.last(8)
      self.hashed_token = hashed_token

      if bump_expiration && token_refreshable?
        reset_expiry
      end

      clear_invalid_scopes
      save!
      last_operations.update_last_write_timestamp
    end
    instrument_regenerate(payload)
    authorization.notify_owner_of_token_regeneration if personal_access_token?
    token
  end

  def reset_expiry
    self.expires_at = expires_in.from_now
  end

  def token_refreshable?
    return false unless integration_application_type?
    application.user_token_expiration_enabled?
  end

  def hashed_token(hex: false)
    return nil unless hashed_token = read_attribute(:hashed_token)
    hex ? Base64.decode64(hashed_token).unpack("H*").first : hashed_token
  end

  def access_level!
    return [] if scopes.blank?
    accesses = []
    scopes.each do |scope|
      scope_s = scope.to_s
      accesses << scope_s if Api::AccessControl.acceptable_scopes.key?(scope_s)
    end
    accesses
  end

  # Public: Return a hexdigest of the user session and authenticating application.
  #
  # Returns a 40 character String token.
  def browser_session_id(user_session)
    return if personal_access_token?
    user_session.secret_hmac.update(application.plaintext_secret).hexdigest
  end

  # Public: Destroy access and instrument the deletion using the provided
  # explanation
  #
  # payload - Symbol of the explanation for deletion. All valid Symbols can be
  # found in OauthUtil::VALID_DESTROY_EXPLANATIONS. These symbols are mapped
  # to a more descriptive explanation that will be shown in the audit log.
  #
  # Returns the result of calling destroy() on the model instance.
  def destroy_with_explanation(explanation)
    unless OauthUtil.destroy_explanation(explanation)
      raise ArgumentError, "invalid destroy explanation: #{explanation}"
    end
    @destroy_explanation = explanation
    destroy
  end

  def expire
    return if expires_at.nil? || expires_at > Time.current
    destroy_with_explanation(:expired)
  end

  def expires_in
    DEFAULT_TOKEN_EXPIRY
  end

  include Instrumentation::Model

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the user that the oauth authorization belongs to.
  def event_actor
    return @event_actor if defined?(@event_actor)
    @event_actor = (User.find_by(id: GitHub.context[:actor_id]) || user)
  end
  def event_prefix() :oauth_access end
  def event_payload
    {
      oauth_access_id: id,
      application_id: safe_app.id,
      application_name: safe_app.name,
      scopes: scopes,
      user: user,
      accessible_org_ids: accessible_organization_ids,
      token_last_eight: token_last_eight,
      hashed_token: hashed_token,
    }
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def accessible_organization_ids
    @accessible_organization_ids ||= if user
      if personal_access_token?
        # A PAT can access all orgs a user can access.
        user.organizations.pluck(:id)
      else
        # Only log the organizations that an authorization can actually access.
        user.organizations.oauth_app_policy_met_by(safe_app).pluck(:id)
      end
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # Public: Instrument creating records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_creation(payload = {})
    instrument :create, payload

    GlobalInstrumenter.instrument "oauth_access.create", {
      actor: user,
      oauth_access: self,
      app: safe_app,
      scopes: scopes,
      accessible_organization_ids: accessible_organization_ids,
    }
  end

  # Public: Instrument updating records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_update(payload = {})
    if scopes_changed? || user_id_changed?
      instrument :update, payload
    end
  end

  # Public: Instrument regenerating the authorization token on this access.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_regenerate(payload = {})
    instrument :regenerate, payload
  end

  # Public: Instrument deleting records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_deletion(payload = {})
    if event_actor&.employee? && event_actor != user
      actor_hash = GitHub.guarded_audit_log_staff_actor_entry(event_actor)
      payload = payload.merge(actor_hash)
    end

    instrument :destroy, payload.merge(
      explanation: @destroy_explanation,
    )
    explanation_key = @destroy_explanation || "none"
    GitHub.dogstats.increment("account_security.oauth_access", tags: ["explanation:#{explanation_key.to_s.gsub(/_/, "-")}", "action:destroy"])
  end

  def note
    description || raw_data.note
  end

  def note=(value)
    write_attribute :description, value
  end

  # Window of time between access logs writes.
  #
  # Returns a duration.
  ACCESS_THROTTLING = 1.week
  ACCESS_CUTOFF_DATE = Time.utc(2014, 3, 6)

  # Public: Register this oauth access token has been used
  #
  # This only updates the timestamp within an interval to prevent a lot
  # of writes if used often in a short period of time.
  #
  # Returns nothing
  def bump
    time = Time.now
    return if bumped_within_throttling_period?(time)

    if GitHub.cache.add(last_accessed_memcache_key, time, ACCESS_THROTTLING.to_i)
      OauthAccessBumpJob.perform_later(id, time.to_i)
    end
  end

  def bump!(time)
    return if bumped_within_throttling_period?

    ActiveRecord::Base.connected_to(role: :writing) do
      threshold = ACCESS_THROTTLING.ago.to_s(:db)
      github_sql.run(<<-SQL, id: id, accessed_at: time, threshold: threshold)
        UPDATE oauth_accesses SET accessed_at = :accessed_at
        WHERE id = :id AND (accessed_at < :threshold OR accessed_at IS NULL)
      SQL
    end

    authorization.bump(time) if authorization

    nil
  end

  # Public: Has this access been bumped in the access throttling period.
  #
  # now - The time to use to determine the end of the throttling period.
  #
  # Returns true if bumped within period else false.
  def bumped_within_throttling_period?(now = Time.now)
    accessed_at && accessed_at > (now - ACCESS_THROTTLING)
  end

  def last_accessed_memcache_key
    "oauth_accesses:last_accessed:#{id}"
  end

  def last_access_time
    accessed_at&.in_time_zone
  end

  def last_access_date
    last_access_time&.to_date
  end

  def integration_application_type?
    application_type == "Integration"
  end

  def oauth_application_type?
    application_type == "OauthApplication" && !personal_access_token?
  end

  def self.instrument_email_verification_required(user:, application_id:)
    GitHub.dogstats.increment "oauth_access", tags: ["action:email-verification", "valid:false", "user_signup_timeframe:#{user.signup_timeframe}"]
    GitHub::Logger.log \
      oauth_application_id: application_id,
      current_user: user.login,
      method: "owner_meets_email_verification_requirements",
      status: "email_verification_required"
  end

  def ability_delegate
    authorization
  end

  def saml_enforceable?
    return true if personal_access_token?
    return false unless Apps::Internal.capable?(:saml_sso_required, app: application)

    # In order to let old tokens work as expected
    # we need to have a cut over date.
    created_at >= SAML_ENFORCEMENT_DATE
  end

  def expired?
    # Flag is not enabled for this Integration
    return false if expires_at.nil?
    return false unless token_refreshable?

    !(expires_at > Time.now)
  end


  private

  def clear_invalid_scopes
    self.scopes = self.class.valid_scopes(scopes)

    valid_hidden_scopes = self.class.valid_hidden_scopes(user)
    self.scopes.reject! { |scope| valid_hidden_scopes[scope] == false }
  end

  def scopes_are_allowed
    bad_scopes = self.class.invalid_scopes(scopes)

    valid_hidden_scopes = self.class.valid_hidden_scopes(user)
    bad_scopes += Array(scopes).select { |scope| valid_hidden_scopes[scope] == false }

    if !bad_scopes.blank?
      errors.add :scopes, "are invalid: #{bad_scopes.to_sentence}"
    end
  end

  def owner_is_human
    errors.add :user, "must be a human" if user && !user.user?
  end

  def owner_meets_email_verification_requirements
    return unless user
    GitHub.dogstats.increment "oauth_access", tags: ["action:email-verification", "valid:true", "user_signup_timeframe:#{user.signup_timeframe}"]
    if user.must_verify_email?
      self.class.instrument_email_verification_required(user: user, application_id: application_id)
      errors.add :user, "must have a verified email address in order to authenticate via OAuth"
    end
  end

  def set_fingerprint
    # If fingerprint is "" then saving the column as NULL is better for allowing
    # DB enforced uniqueness constraints.
    write_attribute(:fingerprint, fingerprint.blank? ? nil : fingerprint)
  end

  def create_authorization
    options = {}.tap do |opts|
      opts[:user]   = user
      opts[:scopes] = scopes

      if personal_access_token?
        opts[:description]              = description
        opts[:is_personal_access_token] = true
      else
        opts[:application] = application
      end
      opts[:integration_version_number] = integration_version_number
    end

    result = OauthAuthorization::Creator.perform(**options)

    if result.success?
      self.authorization = result.authorization
    else
      errors.add(:authorization, result.error)
    end
  end

  # See https://github.com/github/github/issues/21704 for context
  def translate_errors
    if self.errors.include?(:description)
      errors.details[:description].each do |description_error|
        reason = description_error.delete(:error)

        if GitHub.rails_6_0?
          self.errors.add(:note, reason, description_error)
        else
          self.errors.add(:note, reason, **description_error)
        end
      end
      self.errors.delete(:description)
    end
  end

  def update_authorization
    # A PAT can only ever have one set of scopes. So, we replace the
    # authoriztion's scopes entirely.
    if personal_access_token? && scopes_changed?
      self.authorization.scopes = nil
      self.authorization.add_scopes(scopes)
    # Third-party accesses can each have their own set of scopes, so we
    # add the scopes for this access to the authorization.
    elsif scopes_changed?
      self.authorization.add_scopes(scopes)
    end

    if personal_access_token? && description_changed?
      self.authorization.description = description
    end

    self.authorization.save! if self.authorization.changed?
  end

  # Private: callback to destroy associated organization credential
  # authorizations that have not been revoked.
  #
  # Only destroy "active" credential authorizations because revoked records act
  # as a blacklist maintained by the owners of an organization.
  def destroy_active_credential_authorizations
    credential_authorizations.active.destroy_all
  end

  # Private: Deletes associated OauthAuthorization if the authorization has no
  # remaining authorized objects (public keys and accesses).
  #
  # Returns nothing.
  def destroy_authorization
    # If we are being destroyed due to an association :dependent => :destroy
    # then there is no need for us to destroy our parent OauthAuthorization.
    return if destroyed_by_association_parent.is_a?(OauthAuthorization)
    # We delete the associated authorization if all resources associated with
    # the authorization have been removed (i.e. unused) or if we are destroying
    # a personal access token. Unlike regular OAuth authorizations, personal
    # access tokens always only have a single token and all resources associated
    # with the token should be destroyed when the token is destroyed.
    return unless personal_access_token? || authorization.unused?
    if @destroy_explanation.present?
      authorization.destroy_with_explanation(@destroy_explanation)
    else
      authorization.destroy
    end
  end

  def matches_authorization
    if user != authorization.user
      errors.add :user_id, "must be the same user as associated authorization"
    elsif application != authorization.application
      errors.add :application_id, "must be the same user as associated authorization"
    end
  end

  # Private: Deletes the least recently used access if the currently created
  # access exceeds OauthAuthorization::MAXIMUM_ACCESSES_FOR_APP. We do this to
  # prevent an unbounded number of accesses from being created over time.
  #
  # Returns nothing
  def limit_accesses_for_authorization_for_oauth_app
    # We differentiate between accesses with unique scopes so that accesses for
    # different uses do not evict each other.
    normalized_scopes = self.class.normalize_scopes(scopes)
    accesses = authorization.accesses.select do |access|
      self.class.normalize_scopes(access.scopes) == normalized_scopes
    end

    # Temporarily support application specific limit overrides.
    maximum_accesses = if GitHub.oauth_access_limit_overrides_enabled?
      MAXIMUM_ACCESSES_FOR_APP_OVERRIDES.fetch(
        authorization.application_id,
        MAXIMUM_ACCESSES_FOR_APP,
      )
    else
      MAXIMUM_ACCESSES_FOR_APP
    end

    return unless accesses.count >= maximum_accesses

    unused_accesses = accesses.select { |access| access.accessed_at.nil? }

    # We destroy unused accesses when possible. And, if there is
    # more than one unused access, we destroy the oldest one.
    least_recently_accessed = if unused_accesses.present?
      unused_accesses.min_by { |access| access.created_at }
    else
      accesses.min_by { |access| access.accessed_at }
    end
    least_recently_accessed.destroy_with_explanation(:max_for_app)
  end

  # Private: Deletes the least recently used access if the currently created
  # access exceeds the limit appropriate for the Integration.
  #
  # For integrations opted in to expiring tokens
  #   OauthAuthorization::MAXIMUM_ACCESSES_FOR_EXPIRING_APP.
  #
  # For integrations not opted in to expiring tokens
  #   OauthAuthorization::MAXIMUM_ACCESSES_FOR_APP.
  #
  # We do this to prevent an unbounded number of accesses from being created over time.
  #
  # Returns nothing
  def limit_accesses_for_authorization_for_integration
    # Find accesses where a scoped installation is not bound to the record.
    # See https://github.com/github/github/pull/140107 for more details.
    accesses = authorization.accesses.where(installation: nil)
    maximum_accesses = maximum_accesses_for_integration

    return unless accesses.count >= maximum_accesses

    unused_accesses = accesses.select { |access| access.accessed_at.nil? }

    # We destroy unused accesses when possible. And, if there is
    # more than one unused access, we destroy the oldest one.
    least_recently_accessed = if unused_accesses.present?
      unused_accesses.min_by { |access| access.created_at }
    else
      accesses.min_by { |access| access.accessed_at }
    end
    least_recently_accessed.destroy_with_explanation(:max_for_app)
  end

  def maximum_accesses_for_integration
    if GitHub.oauth_access_limit_overrides_enabled?
      if (override = MAXIMUM_ACCESSES_FOR_APP_OVERRIDES[authorization.application_id])
        return override
      end
    end

    token_refreshable? ? MAXIMUM_ACCESSES_FOR_EXPIRING_APP : MAXIMUM_ACCESSES_FOR_APP
  end

  def ensure_integration_capabilities
    return errors.add(:installation, "cannot be set for an OauthApplication") unless integration_application_type?

    unless installation.integration_id == application_id
      return errors.add(:installation, "installation does not belong to the Integration")
    end

    return if Apps::Internal.capable?(:per_repo_user_to_server_tokens, app: application)
    errors.add(:installation, "this feature is not enabled for this Integration")
  end
end
