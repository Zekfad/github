# rubocop:disable Style/FrozenStringLiteralComment

class LdapGroupMember < ApplicationRecord::Domain::Users
  belongs_to :ldap_mapping, primary_key: :dn, foreign_key: :group_dn

  validate :validate_team_mapping

  validates_presence_of :group_dn
  validates_presence_of :member_dn

  private

  def validate_team_mapping
    if !ldap_mapping || !ldap_mapping.team_mapping?
      errors.add(:ldap_mapping, "An LdapOrgMember must be associated with a 'team' LdapMapping.")
    end
  end
end
