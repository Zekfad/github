# rubocop:disable Style/FrozenStringLiteralComment

# This class is used to generate a list of all users on an Enterprise Server
# with the purpose of reporting the data to dotcom for account reconciliation

class UserLicenseListGenerator < UserListGenerator

  attr_reader :include_full_user_info

  # Create a new user license list generator.
  def initialize(include_full_user_info: true, restrict_type: nil)
    @include_full_user_info = include_full_user_info
    # Make :suspended an unsupported restrict_type for this class.
    super(restrict_type: restrict_type == :suspended ? nil : restrict_type)
  end

  private

  # Overrides the user query to return
  #
  #  * id
  #  * login
  #  * primary email address
  #  * all other email addresses
  #  * profile name
  #  * role
  #  * creation_date
  def user_query
    User.github_sql.new <<-SQL
      SELECT
        users.id,
        users.login,
        (SELECT name FROM profiles as up WHERE up.user_id = users.id LIMIT 1) as profile_name,
        IF(users.gh_role='staff', 'admin', 'user') as role,
        users.created_at
      FROM users
      WHERE users.type = 'User'
      AND users.login <> 'ghost'
      AND users.suspended_at IS NULL
    SQL
  end

  # Completes the query results by returning a Hash for each user
  def complete_results(results)
    emails = UserEmail.where(user_id: results.map(&:first)).all

    results.map do |row|
      user_id, login, profile_name, role, created_at = *row
      if include_full_user_info
        {
          user_id: user_id,
          login: login,
          emails: user_emails(emails, user_id),
          profile_name: profile_name,
          site_admin: role == "admin" || role == "staff",
          created_at: created_at.to_s(:db),
        }
      else
        {
          user_id: user_id,
          emails: user_emails(emails, user_id),
        }
      end
    end
  end

  def user_emails(emails, id)
    emails.select { |email| email.user_id == id }.map do |email|
      {
        email: email.email,
        primary: email.primary?,
      }
    end
  end
end
