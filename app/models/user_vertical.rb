# frozen_string_literal: true

class UserVertical < ApplicationRecord::Domain::Users
  belongs_to :user

  validates_associated :user
  validates_presence_of :user_id
  validates_uniqueness_of :user_id
  validates_inclusion_of :vertical_id, in: Vertical.ids

  def vertical
    Vertical.find_by_id(vertical_id)
  end

  def name
    vertical.name
  end
end
