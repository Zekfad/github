# frozen_string_literal: true

class IntegrationListingLanguageName < ApplicationRecord::Domain::Integrations
  areas_of_responsibility :ce_extensibility

  belongs_to :integration_listing
  belongs_to :language_name

end
