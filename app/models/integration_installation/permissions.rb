# frozen_string_literal: true

class IntegrationInstallation
  class Permissions
    include Scientist

    attr_reader :installation, :actor, :action, :options

    class Result
      attr_reader :actor, :target, :action, :result, :reason

      def self.for(check, result:, reason: nil)
        new(check.actor, check.target, check.action, result, reason: reason)
      end

      DEFAULT_REASON = "You do not have permission to %{action} this app on %{target}."

      HUMAN_READABLE_REASONS = {
        requires_org_permissions: "You cannot %{action} apps with organization permissions on %{target}.",
        not_admin_on_subset: "You do not have permission to %{action} this app on %{target}.",
        not_owned_by_target: "Repositories must be owned by %{target}.",
        target_not_organization: "You cannot %{action} apps installed on other user accounts.",
      }.freeze

      def initialize(actor, target, action, result, reason:)
        @actor, @target, @action = actor, target, action
        @result, @reason = result, reason
      end

      def permitted?
        @result
      end

      def error_message
        full_reason = HUMAN_READABLE_REASONS.fetch(reason, DEFAULT_REASON)
        full_reason = full_reason + CONTACT_ORGANIZATION_OWNER_MESSAGE if target.organization?

        I18n.interpolate(full_reason, { action: "modify", target: target })
      end
    end

    def self.check(installation:, actor:, action:, **options)
      new(installation, actor, action, options).check
    end

    def initialize(installation, actor, action, options = {})
      @installation = installation
      @actor        = actor
      @action       = action
      @options      = options
    end

    def check
      case action
      when :admin, :suspend, :unsuspend
        can_admin
      when :update_permissions
        can_update_permissions
      when :install_all_repositories
        can_install_all_repositories
      when :view
        can_view_installation
      when :add_repositories
        can_add(@options[:repositories])
      when :remove_repositories
        can_remove(@options[:repositories])
      when :uninstall
        can_uninstall
      when :auto_upgrade
        can_auto_upgrade(@options[:version])
      else
        result(false, :action_invalid)
      end
    end

    def target
      installation.target
    end

    def integration
      installation.integration
    end

    private

    # Private: Can the given actor perform any action
    # on the IntegrationInstallation.
    #
    # Returns a Result
    def can_admin
      return result(true, :is_admin) if target.adminable_by?(actor)

      result(false, :not_adminable_by)
    end

    def can_update_permissions
      # Ensure that the version belongs to this integration.
      version = @options[:version]
      return result(false, :missing_version) if version.nil?
      return result(false, :incorrect_version) unless version.integration_id == installation.integration_id

      admin_result = can_admin
      return admin_result if admin_result.permitted?

      return result(false, :target_not_organization) unless target.is_a?(Organization)
      return result(false, :not_adminable_by) if version.default_permissions.none?

      if version.permissions_of_type(Organization).any?
        return result(false, :requires_org_permissions)
      end

      can_admin_all_repositories_result = ensure_repositories_adminable(installation.repositories, actor)
      return can_admin_all_repositories_result unless can_admin_all_repositories_result.permitted?

      if version.default_permissions.key?("administration")
        action = version.default_permissions["administration"]

        # Because `administration` allows the creation of repositories
        # we need to ensure that non organization admins can
        # grant `:write` or above.
        #
        # See https://github.com/github/github/issues/107143
        if Ability.actions[action] > Ability.actions[:read]
          return result(false, :not_adminable_by)
        end
      end

      if version.all_permissions_of_type?(Repository)
        return result(true, :can_update)
      end

      result(false, :not_adminable_by)
    end

    # Private: Can the given actor view the IntegrationInstallation, either as an
    # owning admin or a repo admin on any of the repositories.
    #
    # If as an admin on a subset or repos, we only show the repos available.
    #
    # Returns a Result
    def can_view_installation
      admin_result = can_admin
      return admin_result if admin_result.permitted?

      integration_result = Integration::Permissions.check(
        integration: integration,
        actor:       actor,
        action:      :request_installation,
        target:      target,
      )

      result(integration_result.permitted?, integration_result.reason)
    end

    # Private: Can the given actor alter this to be an install on all repos.
    #
    # Returns a Result
    def can_install_all_repositories
      integration_result = Integration::Permissions.check(
        integration:      integration,
        actor:            actor,
        action:           :install,
        target:           target,
        all_repositories: true,
        version:          installation.version,
      )

      result(integration_result.permitted?, integration_result.reason)
    end

    # Private: Can the given actor add the specified repositories to this
    # installation.
    #
    # Returns a Result
    def can_add(repositories = [])
      repository_ownership_result = ensure_repository_ownership(repositories)
      return repository_ownership_result unless repository_ownership_result.permitted?

      requested_ids = repositories.map(&:id)
      repository_ids = installation.repository_ids
      return result(false, :already_installed) if requested_ids.any? { |requested_id| repository_ids.include?(requested_id) }

      if repos_include_advisory_workspace?(repositories)
        unless Apps::Internal.capable?(:can_install_on_security_advisory_repos, app: integration)
          return result(false, :advisory_workspace_repo_included)
        end
      end

      return result(true, :is_admin) if can_admin.permitted?

      ensure_repositories_adminable(repositories, actor)
    end

    # Private: Can the given actor remove the specified repositories from this
    # installation.
    #
    # Returns a Result
    def can_remove(repositories)
      repository_ownership_result = ensure_repository_ownership(repositories)
      return repository_ownership_result unless repository_ownership_result.permitted?

      requested_ids = repositories.map(&:id)
      repository_ids = installation.repository_ids
      return result(false, :not_installed) if (requested_ids - repository_ids).any?

      return result(true, :is_admin) if can_admin.permitted?

      ensure_repositories_adminable(repositories, actor)
    end

    # Private: Can the given actor remove the specified repositories from this
    # installation.
    #
    #  - Can remove if admin
    #  - Can remove if noora can admin all repos and there aren't any org permissions
    #
    # Returns a Result
    def can_uninstall
      return result(true) if can_admin.permitted?
      return result(false, :requires_org_permissions) if requires_organization_installation?

      can_remove(installation.repositories)
    end

    def can_auto_upgrade(new_version)
      return result(false, :missing_version)   unless new_version.present?
      return result(false, :incorrect_version) unless new_version.integration_id == installation.integration_id

      return result(true) if new_version.default_permissions.empty?
      return result(true) if Apps::Internal.capable?(:auto_upgrade_permissions, app: installation.integration)

      diff   = new_version.diff(installation.version)
      target = installation.target

      unless target.is_a?(Business)
        return result(false, :single_file_name_changed) if diff.single_file_name_changed?
        return result(false, :content_references_added) if diff.content_references_added?
      end

      subject_types = case target
      when Business
        Business::Resources.subject_types
      when Organization
        (Repository::Resources.subject_types + Organization::Resources.subject_types)
      when User
        Repository::Resources.subject_types
      end

      return result(false, :permissions_added)    if permissions_part_of_subject_types?(diff.permissions_added, subject_types)
      return result(false, :permissions_upgraded) if permissions_part_of_subject_types?(diff.permissions_upgraded, subject_types)

      result(true)
    end

    # Private: do the referenced permissions require organization approval
    #
    # Returns a Boolean
    def requires_organization_installation?(permissions = nil)
      permissions ||= @installation.permissions
      Organization::Resources.subject_types.any? { |type| permissions.include?(type) }
    end

    def result(value, reason = nil)
      Result.for(self, result: value, reason: reason)
    end

    # does the user have admin ability over all the repositories
    def ensure_repositories_adminable(repositories, actor)
      if all_repositories_adminable?(repositories, actor)
        result(true, :admin_on_selected_repos)
      else
        result(false, :not_admin_on_subset)
      end
    end

    def all_repositories_adminable?(repositories, actor)
      # direct admin abilities on repo
      direct_admin_repo_ids = Authorization.service.most_capable_collaborator_abilities_from_actor(
        actor:        actor,
        subject_type: Repository,
        subject_ids:  repositories.map(&:id),
        min_action: :admin,
      ).pluck(:subject_id)

      # they have an explicit admin ability on all the provided repos
      return true if direct_admin_repo_ids.count == repositories.count

      # do they have admin on all the owning orgs of the remaining repos?
      direct_admin_repos = Repository.where(id: direct_admin_repo_ids)
      remaining_repos = repositories - direct_admin_repos

      repo_owning_org_ids = remaining_repos.map(&:owning_organization_id).uniq
      org_admin_abilities = Ability.user_admin_on_organization(
        actor_id: actor.id,
        subject_id: repo_owning_org_ids,
      )

      org_admin_abilities.count == repo_owning_org_ids.count
    end

    def repos_include_advisory_workspace?(repositories)
      RepositoryAdvisory.where(workspace_repository_id: repositories.map(&:id)).any?
    end

    def ensure_repository_ownership(repositories)
      return result(false, :no_repositories_submitted) unless repositories.present?
      return result(false, :not_owned_by_target) unless repositories.all? { |repository| repository.owner_id == target.id }

      return result(true)
    end

    def permissions_part_of_subject_types?(permissions, subject_types)
      (permissions.keys & subject_types).any?
    end
  end
end
