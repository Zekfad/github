# frozen_string_literal: true

class IntegrationInstallation
  class RepositoryEditor
    include Scientist

    attr_reader :installation, :action, :repositories, :editor

    DEFAULT_ERROR_MSG = "You do not have permission to modify this app on %s. Please contact an Organization Owner."

    class Result
      class Error < StandardError; end

      def self.success(installation) new(:success, installation: installation) end
      def self.failed(error) new(:failed, error: error) end

      attr_reader :error, :installation

      def initialize(status, installation: nil, error: nil)
        @status       = status
        @installation = installation
        @error        = error
      end

      def success?
        @status == :success
      end

      def failed?
        @status == :failed
      end
    end

    def self.perform(installation, action:, repositories:, editor:)
      new(installation, action, repositories, editor).perform
    end

    def initialize(installation, action, repositories, editor)
      @installation, @action = installation, action
      @repositories, @editor = Array(repositories), editor

      @repository_permissions = installation.version.permissions_of_type(Repository)
    end

    def perform
      begin
        case action
        when :add
          add_repositories
        when :remove
          remove_repositories
        when :update
          update_repositories
        else
          Result.failed("Invalid action")
        end
      rescue Result::Error => e
        Result.failed(e.message)
      end
    end

    private

    def add_repositories
      result = IntegrationInstallation::Permissions.check(
        installation: installation,
        actor:        editor,
        action:       :add_repositories,
        repositories: repositories,
      )

      if !result.permitted?
        GitHub.dogstats.increment("integration_installation.update", tags: ["result:failed", "type:repositories", "action:add"])
        raise Result::Error, result.error_message
      end

      ApplicationRecord::Iam.transaction do
        install_on_repositories(repositories)
      end

      UpdateIntegrationInstallationRateLimitJob.enqueue(installation.id)
      GitHub.dogstats.increment("integration_installation.update", tags: ["result:success", "type:repositories", "action:add"])
      instrument_repositories_added(repositories)

      Result.success(installation)
    end

    def remove_repositories
      result = IntegrationInstallation::Permissions.check(
        installation: installation,
        actor:        editor,
        action:       :remove_repositories,
        repositories: repositories,
      )

      if !result.permitted?
        GitHub.dogstats.increment("integration_installation.update", tags: ["result:failed", "type:repositories", "action:remove"])
        raise Result::Error, result.error_message
      end

      repository_ids = repositories.map(&:id)

      # Uninstall the app entirely if the editor is removing all of the repos
      if (installation.repository_ids - repository_ids).empty?
        GitHub.dogstats.increment("integration_installation.update", tags: ["result:success", "type:repositories", "action:update"])
        return Result.success(installation.uninstall(actor: editor))
      end

      ApplicationRecord::Iam.transaction do
        uninstall_from_repositories(repository_ids)
      end

      UpdateIntegrationInstallationRateLimitJob.enqueue(installation.id)
      SyncScopedIntegrationInstallationsJob.perform_later(installation, action: :repositories_removed, repository_ids: repository_ids)

      GitHub.dogstats.increment("integration_installation.update", tags: ["result:success", "type:repositories", "action:remove"])

      instrument_repositories_removed(repository_ids)

      Result.success(installation)
    end

    def update_repositories
      if installation.installed_on_all_repositories?
        GitHub.dogstats.increment("integration_installation.update", tags: ["result:failed", "type:repositories", "action:update"])
        return Result.failed(DEFAULT_ERROR_MSG % installation.target.name)
      end

      repositories_to_add    = filter_addable_repositories(repositories)
      repositories_to_remove = filter_removeable_repositories(repositories)

      ApplicationRecord::Iam.transaction do
        if repositories_to_add.any?
          @repositories = repositories_to_add
          add_repositories
        end

        if repositories_to_remove.any?
          @repositories = repositories_to_remove
          remove_repositories
        end
      end

      Result.success(installation)
    end

    def filter_addable_repositories(repositories)
      return [] if repositories.empty?

      repository_ids           = repositories.map(&:id)
      installed_repository_ids = installation.repository_ids

      ids = repository_ids - installed_repository_ids

      return [] if ids.empty?

      ids &= editor.associated_repository_ids(min_action: :admin, repository_ids: Repository.owned_by(installation.target).ids)
      repositories.select { |repository| ids.include?(repository.id) }
    end

    def filter_removeable_repositories(repositories)
      repository_ids = installation.repository_ids - repositories.map(&:id)
      return [] if repository_ids.empty?

      repository_ids &= editor.associated_repository_ids(min_action: :admin, repository_ids: installation.repository_ids)
      installation.repositories.where(id: repository_ids).select(:id, :owner_id).to_a
    end

    def install_on_repositories(repositories)
      rows = repositories.flat_map do |repository|
        @repository_permissions.map do |resource, action|
          subject = repository.resources.public_send(resource)
          ::Permissions::Service.app_attributes(actor: installation, subject: subject, action: action)
        end
      end

      ::Permissions::Service.grant_permissions(rows)
    end

    def uninstall_from_repositories(repository_ids)
      return if repository_ids.empty?

      ::Permissions::Service.revoke_permissions_granted_on_subjects(
        actor_id:      installation.id,
        actor_type:    "IntegrationInstallation",
        subject_ids:   repository_ids,
        subject_types: Repository::Resources.individual_type_prefixed_subject_types,
      )

      # This removes permissions that were granted
      # such as access to protected branches.
      revoke_child_resources_for(repository_ids)
    end

    def instrument_repositories_added(repositories)
      installation.instrument_repositories_added(repositories.map(&:id), actor: editor, repository_selection: "selected")
    end

    def instrument_repositories_removed(repository_ids)
      installation.instrument_repositories_removed(repository_ids, actor: editor)
    end

    def revoke_child_resources_for(repository_ids)
      revoke_protected_branch_access(repository_ids)
    end

    def revoke_protected_branch_access(repository_ids)
      ProtectedBranch.where(repository_id: repository_ids).in_batches do |relation|
        ::Permissions::Service.revoke_permissions_granted_on_subjects(
          actor_id:      installation.ability_id,
          actor_type:    installation.ability_type,
          subject_ids:   relation.pluck(:id),
          subject_types: ProtectedBranch::Resources.individual_type_prefixed_subject_types,
        )
      end
    end
  end
end
