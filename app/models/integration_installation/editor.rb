# rubocop:disable Style/FrozenStringLiteralComment

class IntegrationInstallation
  class Editor
    attr_reader :installation, :repositories, :target, :permissions, :editor, :pending_request

    # Result from the IntegrationInstallation edit
    class Result
      class Error < StandardError
        attr_reader :reason
        def initialize(message, reason = nil)
          @reason = reason
          super(message)
        end
      end

      def self.success(installation) new(:success, installation: installation) end
      def self.failed(error, reason = nil) new(:failed, error: error, reason: reason) end

      attr_reader :error, :installation, :reason

      def initialize(status, installation: nil, error: nil, reason: nil)
        @status       = status
        @installation = installation
        @error        = error
        @reason       = reason
      end

      def success?
        @status == :success
      end

      def failed?
        @status == :failed
      end
    end

    # Public: Edit an integration's access to repositories
    #
    # integration   - The Integration being installed.
    # repositories: - An Array of Repositories to include in the installation and uninstallation. (Optional.).
    # editor:       - The User performing the installation edit.
    #
    # Returns an IntegrationInstallation::Editor::Result.
    def self.perform(installation, repositories: nil, editor:, pending_request: nil)
      new(installation, repositories: repositories, editor: editor, pending_request: pending_request).perform
    end

    def self.append(installation, repositories: nil, editor:)
      new(installation, repositories: repositories, editor: editor, pending_request: nil).append
    end

    def initialize(installation, repositories:, editor: nil, pending_request: nil)
      @installation         = installation
      @target               = installation.target
      @repositories         = repositories
      @editor               = editor
      @pending_request      = pending_request

      @repository_permissions = installation.version.permissions_of_type(Repository)
    end

    def current_repositories
      @current_repositories ||= installation.repositories
    end

    def requesting_all_repositories?
      @repositories.blank?
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def installed_on_target?
      @installed_on_target ||= installation.installed_on_all_repositories?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def perform
      current_repositories # cache current repositories before making changes.
      validate_editor_has_permission

      ApplicationRecord::Iam.transaction do
        IntegrationInstallation.transaction do
          if requesting_all_repositories?
            install_on_all_repositories
          else
            # We need install before uninstalling to ensure
            # we don't interrupt access to the repositories.
            install_on_repositories
            uninstall_unused_repositories
          end
        end
      end

      UpdateIntegrationInstallationRateLimitJob.enqueue(installation.id)
      GitHub.dogstats.increment("integration_installation.update", tags: ["result:success", "type:permissions"])

      bust_and_set_cache

      Result.success(installation)
    rescue Result::Error => e
      GitHub.dogstats.increment("integration_installation.update", tags: ["result:failed", "type:permissions"])
      Result.failed(e.message, e.reason)
    end

    def append
      return Result.success(installation) if installed_on_target? # This installation covers all repositories
      validate_editor_has_permission

      ApplicationRecord::Iam.transaction do
        install_on_repositories(append: true)
      end

      UpdateIntegrationInstallationRateLimitJob.enqueue(installation.id)
      GitHub.dogstats.increment("integration_installation.update", tags: ["result:success", "type:repositories"])

      Result.success(installation)
    rescue Result::Error => e
      GitHub.dogstats.increment("integration_installation.update", tags: ["result:failed", "type:permissions"])
      Result.failed(e.message, e.reason)
    end

    private

    def bust_and_set_cache
      installation.set_cached_repository_selection
    end

    def validate_editor_has_permission
      if requesting_all_repositories?
        result = IntegrationInstallation::Permissions.check(installation: installation,
                                                            actor:        editor,
                                                            action:      :install_all_repositories)

        return true if result.permitted?
        raise Result::Error.new(result.error_message, result.reason)
      end

      added_repositories = repositories - current_repositories
      removed_repositories = current_repositories - repositories

      if added_repositories.empty? && removed_repositories.empty?
        result = IntegrationInstallation::Permissions.check(installation: installation,
                                                            actor:        editor,
                                                            action:      :install_all_repositories)

        return true if result.permitted?
        raise Result::Error.new(result.error_message, result.reason)
      end

      result = if added_repositories.any?
        IntegrationInstallation::Permissions.check(installation: installation,
                                                   actor:        editor,
                                                   action:       :add_repositories,
                                                   repositories: added_repositories)

      end

      if result.nil? || (result.permitted? && removed_repositories.any?)
        result = IntegrationInstallation::Permissions.check(installation: installation,
                                                            actor:        editor,
                                                            action:       :remove_repositories,
                                                            repositories: removed_repositories)
      end

      return true if result.permitted?
      raise Result::Error.new(result.error_message, result.reason)
    end

    def install_on_all_repositories
      grant_repository_permissions_on_target
      uninstall_from_repositories(current_repositories)

      all_repository_ids = @target.associated_repository_ids(including: :owned, include_indirect_forks: false, include_oopfs: false)
      repository_ids_to_add ||= all_repository_ids - current_repositories.collect(&:id)
      installation.instrument_repositories_added(repository_ids_to_add, actor: editor, repository_selection: "all")
    end

    def install_on_repositories(append: false)
      repositories_to_install   = repositories if installed_on_target? || append
      repositories_to_install ||= repositories - current_repositories

      repositories_to_install = Array(repositories_to_install)

      rows = repositories_to_install.flat_map do |repository|
        @repository_permissions.map do |resource, action|
          subject = repository.resources.public_send(resource)
          ::Permissions::Service.app_attributes(actor: installation, subject: subject, action: action)
        end
      end

      ::Permissions::Service.grant_permissions(rows)
      installation.instrument_repositories_added(repositories_to_install.collect(&:id), actor: editor, repository_selection: "selected", requester_id: pending_request&.requester_id)
    end

    # Private: Remove this integration's access to a set
    # of repositories.
    #
    # repositories - The list of repositories to give remove access to.
    #
    # Examples
    #
    #   uninstall_from_repositories(repositories)
    #
    # Returns nothing.
    def uninstall_from_repositories(repositories)
      return if repositories.empty?

      ::Permissions::Service.revoke_permissions_granted_on_subjects(
        actor_id:      installation.ability_id,
        actor_type:    installation.ability_type,
        subject_ids:   repositories.map(&:id),
        subject_types: Repository::Resources.individual_type_prefixed_subject_types,
      )
    end

    def uninstall_unused_repositories
      removed_repositories = current_repositories - repositories

      # This removes permissions that were granted
      # such as access to protected branches.
      revoke_child_resources_for(removed_repositories)

      if installed_on_target?
        revoke_repository_permissions_on_target
      else
        uninstall_from_repositories(removed_repositories)
      end

      removed_repository_ids = removed_repositories.collect(&:id)

      installation.instrument_repositories_removed(removed_repository_ids, actor: editor)
      SyncScopedIntegrationInstallationsJob.perform_later(installation, action: :repositories_removed, repository_ids: removed_repository_ids)
    end

    def grant_repository_permissions_on_target
      rows = []

      rows = @repository_permissions.map do |resource, action|
        subject = target.repository_resources.public_send(resource.to_s)
        ::Permissions::Service.app_attributes(actor: installation, subject: subject, action: action)
      end

      ::Permissions::Service.grant_permissions(rows)
    end

    def revoke_repository_permissions_on_target
      ::Permissions::Service.revoke_permissions_granted_on_subject(
        actor_id:      installation.ability_id,
        actor_type:    installation.ability_type,
        subject_id:    target.ability_id,
        subject_types: Repository::Resources.all_type_prefixed_subject_types,
      )
    end

    def revoke_child_resources_for(repositories)
      revoke_protected_branch_access(repositories)
    end

    def revoke_protected_branch_access(repositories)
      ProtectedBranch.where(repository: repositories).in_batches do |relation|
        ::Permissions::Service.revoke_permissions_granted_on_subjects(
          actor_id:      installation.ability_id,
          actor_type:    installation.ability_type,
          subject_ids:   relation.pluck(:id),
          subject_types: ProtectedBranch::Resources.all_prefixed_subject_types,
        )
      end
    end
  end
end
