# rubocop:disable Style/FrozenStringLiteralComment

class IntegrationInstallation::AbilityCollection < Ability::Collection
  # Internal: Used by Abilities to ensure that only IntegrationInstallations
  # can be granted abilities on a Collection
  def grant?(actor, action)
    actor.can_have_granular_permissions?
  end

  # Public: access control on the collection
  #
  # actor: - The User to check permission for.
  # action - The Symbol action representing the level of permission to check for.
  #
  # Returns true or false.
  def permit?(actor, action)
    async_permit?(actor, action).sync
  end

  def async_permit?(actor, action)
    async_context_allows_access?(actor: actor, action: action).then do |result|
      result && async_has_coarse_grained_permission?(actor: actor, action: action).then do |result|
        result || async_has_indirect_permission?(actor: actor, action: action).then do |result|
          result || async_has_granular_permission?(actor: actor, action: action)
        end
      end
    end
  end

  private

  def async_context_allows_access?(actor:, action:)
    actor_context = actor.access_context if actor.respond_to?(:access_context)
    return Promise.resolve(true) unless actor_context

    async_has_granular_permission?(actor: actor_context, action: action).then do |result|
      result || async_has_coarse_grained_permission?(actor: actor_context, action: action)
    end
  end

  # Does the actor have access via the parent Repository?
  def async_has_coarse_grained_permission?(actor:, action:)
    case parent
    when PullRequest
      case @name
      when "sarifs"
        parent.async_repository.then do |repository|
          repository.resources.pull_requests.async_permit?(actor, action)
        end
      else
        # It's not enough to have access to the Repository in order to
        # gain access to these resources. Further access is required.
        Promise.resolve(false)
      end
    when Repository
      case @name
      when "actions", "checks", "contents", "content_references", "composable_comments", "deployments",
           "discussions", "issues", "metadata", "packages", "pages", "pull_requests", "repository_projects",
           "single_file", "statuses"

        # Access to the parent Repository will suffice to grant access to other resources.
        parent.async_permit?(actor, action)
      else
        # It's not enough to have access to the parent Repository in order to
        # gain access to these resources. Further access is required.
        Promise.resolve(false)
      end
    else
      # We don't grant access to resources by virtue of having access to the
      # parent for non-Repository parents.
      Promise.resolve(false)
    end
  end

  # Does the actor have access via a special role?
  def async_has_indirect_permission?(actor:, action:)
    return Promise.resolve(false) unless actor&.ability_delegate
    return Promise.resolve(false) if actor.ability_delegate.can_have_granular_permissions?

    case parent
    when Business
      case @name
      when "enterprise_administration"
        return parent.async_adminable_by?(actor)
      end
    when Organization
      if actor
        case @name
        when "members"
          # TODO Does this need to be async?
          return Promise.resolve(parent.direct_or_team_member?(actor))
        when "organization_projects"
          return parent.async_readable_by?(actor)
        when "organization_administration",
             "organization_hooks",
             "organization_pre_receive_hooks",
             "organization_user_blocking",
             "organization_plan"
          return parent.async_adminable_by?(actor)
        end
      end
    when Repository
      case @name
      when "administration", "repository_hooks", "repository_pre_receive_hooks"
        # These resources are granted access solely by the fact that the actor
        # in question is a repository admin, without consideration for whether
        # the actor has access to any other resource.
        return parent.async_adminable_by?(actor)
      when "security_events"
        return parent.async_writable_by?(actor)
      end
    end

    Promise.resolve(false)
  end

  # Does the actor have access via granular permission?
  def async_has_granular_permission?(actor:, action:)
    return Promise.resolve(false) unless actor&.ability_delegate&.can_have_granular_permissions?

    async_has_granular_permission_on_specific_resource?(actor: actor, action: action).then do |result|
      result || async_has_granular_permission_by_default?(actor: actor, action: action)
    end
  end

  # Does the actor have a granular permission on this resource?
  def async_has_granular_permission_on_specific_resource?(actor:, action:)
    if actor.ability_delegate.is_a?(GlobalIntegrationInstallation)
      return actor.ability_delegate.async_can?(collection: self, action: action)
    end

    # As of right now, GitHub Apps are not allowed access
    # in any way to advisory worspace repositories until we figure
    # out the best way forward.
    #
    # See https://github.com/github/pe-repos/issues/130 for more information.
    case parent
    when PullRequest
      parent.async_repository.then do |repository|
        if repository.advisory_workspace?
          return Promise.resolve(false)
        end
      end
    when Repository
      if parent.advisory_workspace?
        return Promise.resolve(false)
      end
    end

    ::Permissions::Service.async_can?(actor, action, self)
  end

  # Does the actor have access via granular permission via a default permission
  # across the entire account?
  def async_has_granular_permission_by_default?(actor:, action:)
    case parent
    when PullRequest
      parent.async_repository.then do |repository|
        !repository.advisory_workspace? && repository.async_owner.then do |owner|
          owner.repository_resources.pull_requests.async_permit?(actor, action)
        end
      end
    when Repository
      # As of right now, GitHub Apps are not allowed access
      # in any way to advisory worspace repositories until we figure
      # out the best way forward.
      #
      # See https://github.com/github/pe-repos/issues/130 for more information.
      return Promise.resolve(false) if parent.advisory_workspace?

      parent.async_owner.then do |owner|
        association_default = owner.repository_resources.send(@name)
        association_default.async_permit?(actor, action)
      end
    else
      return Promise.resolve(false)
    end
  end
end
