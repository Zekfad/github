# frozen_string_literal: true

module IssueEvent::TimelineDependency
  extend ActiveSupport::Concern

  class_methods do
    # Public: SQL query compiling a full list of issue events to show in the timeline
    #
    # viewer              - User who is viewing the timeline
    # issue_ids           - Array of Integers for Issue IDs
    # selected_attributes - Array of attributes to select
    # visible_events_only - Boolean to indicate whether to load valid or visible events
    # exclude_event_types - Array of symbols representing event categories to
    #                       exclude (currently only :projects is supported)
    #
    # Returns String for SQL query. Can be run using `ActiveRecord::Base.connection.select_rows`
    def visible_in_issue_timeline_query_for(viewer, issue_ids:,
                                                 selected_attributes:, visible_events_only: false, exclude_event_types: [], requested_events: [])

      visible_in_timeline_query_for(viewer,
                                    base_scope: where(issue_id: issue_ids),
                                    selected_attributes: selected_attributes,
                                    visible_events_only: visible_events_only,
                                    exclude_event_types: exclude_event_types,
                                    requested_events: requested_events)
    end

    def visible_in_timeline_query_for(viewer, base_scope:, selected_attributes:, visible_events_only: false, exclude_event_types: [], requested_events: [])
      escaped_selected_attributes = selected_attributes.map do |attribute|
        "`issue_events`.`#{attribute}`"
      end

      if requested_events.empty?
        requested_events = IssueEvent::VALID_EVENTS
      end

      scopes = []

      #TODO remove visible_events_only and replace with requested_events see: https://github.com/github/github/pull/96070
      simple_events = visible_events_only ? IssueEvent::SIMPLE_VISIBLE_EVENTS : IssueEvent::SIMPLE_EVENTS
      if (events = simple_events & requested_events).any?
        scopes << base_scope.where(event: events)
      end

      if (events = IssueEvent::LABEL_EVENTS & requested_events).any?
        scopes << base_scope.where(event: events).join_issue_event_detail.where(<<~SQL)
        `issue_event_details`.`label_id` IS NOT NULL OR `issue_event_details`.`label_name` IS NOT NULL
        SQL
      end

      if (events = IssueEvent::PULL_REQUEST_EVENTS & requested_events).any?
        scopes << base_scope.where(event: events).associated_with_pull_request_only
      end

      if requested_events.include?("referenced")
        scopes << base_scope.where(event: "referenced", referencing_issue_id: nil).
          where("`issue_events`.`commit_id` IS NOT NULL").
          without_events_from_spammy_commit_repositories(viewer)
      end

      if requested_events.include?("deployed")
        scopes << base_scope.where(event: "deployed").
          join_issue_event_detail.joins(<<~SQL)
          INNER JOIN `deployments`
            ON `issue_event_details`.`deployment_id` = `deployments`.`id`
        SQL
      end

      if (events = IssueEvent::CROSS_ISSUE_SUBJECT_EVENTS & requested_events).any?
        scopes << base_scope.where(event: events).join_issue_event_detail.
          where("`issue_events`.`referencing_issue_id` IS NULL").
          without_events_from_spammy_subject_repositories(viewer).
          without_events_from_spammy_subjects(viewer).
          where(<<~SQL)
          `issue_event_details`.`subject_id` IS NOT NULL
        SQL
      end

      #TODO remove exclude_event_types and replace with requested_events see: https://github.com/github/github/pull/96070
      if !exclude_event_types.include?(:projects) && (events = IssueEvent::PROJECT_EVENTS & requested_events).any?
        scopes << base_scope.where(event: events)
      end

      if (events = IssueEvent::FORCE_PUSH_EVENTS & requested_events).any?
        scopes << base_scope.where(event: events).
          associated_with_pull_request_only.
          join_commit_repository.
          where(<<~SQL)
          `commit_repositories`.`id` IS NOT NULL
        SQL
      end

      scopes.map { |scope|
        scope.select(escaped_selected_attributes).to_sql
      }.join("\n\nUNION ALL\n\n")
    end
    private :visible_in_timeline_query_for
  end

  included do
    # Public: Join commit repository
    scope :join_commit_repository, -> {
      joins(<<~SQL)
      LEFT OUTER JOIN `repositories` `commit_repositories`
      ON `commit_repositories`.`id` = `issue_events`.`commit_repository_id`
      SQL
    }

    scope :join_issue_event_detail, -> {
      joins(<<~SQL)
      INNER JOIN `issue_event_details`
         ON `issue_event_details`.`issue_event_id` = `issue_events`.`id`
      SQL
    }

    scope :join_subject_issue, -> {
      join_issue_event_detail.joins(<<~SQL)
      INNER JOIN `issues` `subject_issues`
        ON `subject_issues`.`id` = `issue_event_details`.`subject_id`
        AND `issue_event_details`.`subject_type` = 'Issue'
      SQL
    }

    # Public: Join repository of issue if the subject of the issue detail
    # is present and is an issue.
    scope :join_subject_issue_repository, -> {
      join_subject_issue.joins(<<~SQL)
      INNER JOIN `repositories` `subject_issue_repositories`
        ON `subject_issue_repositories`.`id` = `subject_issues`.`repository_id`
      SQL
    }

    # Public: Scope for filtering events from spammy commit repositories
    #
    # viewer - User who is viewing
    scope :without_events_from_spammy_commit_repositories, -> (viewer) {
      next unless GitHub.spamminess_check_enabled?
      next if viewer&.site_admin?

      scope = join_commit_repository

      if viewer
        scope.where(<<~SQL, viewer_id: viewer.id)
        `commit_repositories`.`id` IS NULL OR
        `commit_repositories`.`user_hidden` = 0 OR
        `commit_repositories`.`owner_id` = :viewer_id
        SQL
      else
        scope.where(<<~SQL)
        `commit_repositories`.`id` IS NULL OR
        `commit_repositories`.`user_hidden` = 0
        SQL
      end
    }

    # Public: Scope for filtering events from spammy repositories when an issue is stored in the
    # subject fields of issue_event_details.
    #
    # viewer - User who is viewing
    scope :without_events_from_spammy_subject_repositories, -> (viewer) {
      next unless GitHub.spamminess_check_enabled?
      next if viewer&.site_admin?

      scope = join_subject_issue_repository

      if viewer
        scope.where(<<~SQL, viewer_id: viewer.id)
        `subject_issue_repositories`.`id` IS NULL OR
        `subject_issue_repositories`.`user_hidden` = 0 OR
        `subject_issue_repositories`.`owner_id` = :viewer_id
        SQL
      else
        scope.where(<<~SQL)
        `subject_issue_repositories`.`id` IS NULL OR
        `subject_issue_repositories`.`user_hidden` = 0
        SQL
      end
    }

    # Public: Scope for filtering events from spammy subject authors
    #
    # viewer - User who is viewing
    scope :without_events_from_spammy_subjects, -> (viewer) {
      next unless GitHub.spamminess_check_enabled?
      next if viewer&.site_admin?

      scope = join_subject_issue

      if viewer
        scope.where(<<~SQL, viewer_id: viewer.id)
        `subject_issues`.`user_hidden` = false OR
        `subject_issues`.`user_id` = :viewer_id
        SQL
      else
        scope.where(<<~SQL)
        `subject_issues`.`user_hidden` = false
        SQL
      end
    }

    # Public: Scope for selecting events that belong to an existing pull request
    scope :associated_with_pull_request_only, -> {
      joins(<<~SQL)
      INNER JOIN `issues` ON `issue_events`.`issue_id` = `issues`.`id`
      INNER JOIN `pull_requests` ON `issues`.`pull_request_id` = `pull_requests`.`id`
      SQL
    }
  end
end
