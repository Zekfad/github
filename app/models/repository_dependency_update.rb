# frozen_string_literal: true

# Acts as an entry point for Dependabot updates on a Repository, providing
# an outcome for each request made by the source repository in the form of
# either a Pull Request or a user-facing error message.
class RepositoryDependencyUpdate < ApplicationRecord::Notify
  TIMEOUT_ERROR_TYPE = "timed_out".freeze

  # This value represents our service contract with Dependabot, after which we
  # should abandon jobs as failed.
  DEPENDABOT_MAX_WAIT = 24.hours
  # Manually requested jobs are expected to jump the queue and resolve much more
  # quickly.
  DEPENDABOT_MAX_WAIT_FOR_MANUAL = 40.minutes
  # Dependabot's update runner times out after 30 minutes. Under normal circumstances
  # it should reply with an error shortly after, so this time is set to allow for this
  # plus time to ingest and callback via GraphQL.
  #
  # see: https://github.com/dependabot/update-job-runner/blob/master/handler.go#L26

  DEFAULT_ERROR_TYPE  = "unknown_error".freeze
  DEFAULT_ERROR_TITLE = "Dependabot encountered an unknown error".freeze
  DEFAULT_ERROR_BODY  = <<~MD.chomp.freeze
                          Dependabot encountered an unknown error.

                          We've been notified of the problem, and are working to fix it.
                          MD

  # For repositories that do not check in lockfiles for certain package managers,
  # we will create RepositoryVulnerabilityAlerts that Dependabot cannot update.
  UNSUPPORTED_MANIFEST_PATTERNS = [
    DependencyManifestFile::GEMFILE_PATTERN,
  ]

  areas_of_responsibility :dependabot

  belongs_to :repository
  belongs_to :pull_request, inverse_of: :dependency_updates
  belongs_to :repository_vulnerability_alert

  extend GitHub::Encoding
  force_utf8_encoding :body, :error_body, :error_title

  enum state: %i{
    requested
    complete
    error
  }

  enum reason: %i{
    vulnerability
    upgrade
  }

  enum trigger_type: %i{
    manual
    install
    scan
    push
    dry_run
  }

  validates_presence_of :repository, :manifest_path, :package_name

  # A newly requested update should not a pull request or error content.
  validates_absence_of :pull_request, :error_title, :error_body, :error_type, if: :requested?

  # A completed update must have a pull request assigned.
  validates_presence_of :pull_request, if: :complete?

  # We require an error body and title when put into an errored state.
  validates_presence_of :error_body, :error_title, if: :error?
  validates_absence_of :pull_request, if: :error?

  # A vulnerability update must have an alert assigned at creation.
  validates_presence_of :repository_vulnerability_alert, if: :vulnerability?, on: :create

  validate :pull_request_belongs_to_repository
  validate :manifest_path_is_supported, on: :create
  validate :no_new_dry_run_triggers, on: :create

  scope :visible, -> { where(dry_run: false).where.not(trigger_type: :dry_run) }
  scope :for, ->(path:, package:) { where(manifest_path: path, package_name: package) }
  scope :unresolved_vulnerability, -> {
    vulnerability.
    joins(:repository_vulnerability_alert)
  }

  before_create :set_retry_flag

  after_commit :instrument_creation, on: :create
  after_commit :instrument_update, on: :update
  after_commit :enqueue_update_request_job, on: :create

  # Accepts a Repository to create dependency updates for all open
  # RepositoryVulnerabilityAlerts.
  #
  # repository - A Repository to process
  # trigger:    - The event that triggered this request
  #
  # returns an Array of any persisted RepositoryDependencyUpdate objects
  def self.request_for_repository(repository, trigger:)
    RepositoryDependencyUpdate::RepositoryResolver.new(repository, trigger: trigger).create_updates
  end

  # Accepts a Repository and a dependency to create an update for.
  #
  # repository:    - A Repository to process
  # manifest_path: - The manifest file path in the repository that specifies this dependency
  # package_name:  - The name of the package
  # trigger:       - The event that triggered this request
  #
  # returns a persisted RepositoryDependencyUpdate object, or false if no
  # object can be created
  def self.request_for_dependency(repository:, manifest_path:, package_name:, trigger:)
    RepositoryDependencyUpdate::DependencyResolver.new(repository: repository,
                                                       manifest_path: manifest_path,
                                                       package_name: package_name,
                                                       trigger: trigger).create_update
  end

  # Accepts a manifest path and returns a boolean value if Dependabot is expected
  # to successfully create a Pull Request for the given manifest type.
  def self.manifest_path_supported?(manifest_path)
    UNSUPPORTED_MANIFEST_PATTERNS.none? do |pattern|
      manifest_path.is_a?(String) && manifest_path =~ pattern
    end
  end

  # Returns a symbol representing the package manager involved in this update.
  #
  # NOTE: The returned value is expected to match the Dependabot dictionary:
  # https://github.com/github/dependabot-api/blob/master/app/actions/parse_config_file.rb#L26-L42
  #
  # e.g. :rubygems in github/github is :bundler in github/dependabot-api
  def package_manager
    @package_manager ||= dependabot_package_manager
  end

  # Returns a symbol representing the manifest type involved in this update.
  def manifest_type
    DependencyManifestFile.corresponding_manifest_type(path: manifest_path) || :unknown
  end

  def dry_run?
    dry_run || super
  end

  # Returns true if Dependabot is currently producing or has produced a Pull Request
  def requested_or_proposed?
    requested? || proposed_change_exists?
  end

  # Returns true if Dependabot has created a Pull Request
  def proposed_change_exists?
    complete? && pull_request
  end

  # Returns true if a PR has been created and closed by the user, signalling
  # that Dependabot should ignore this specific fixed version.
  def proposed_change_ignored?
    return false unless proposed_change_exists?

    pull_request.closed? && !pull_request.merged?
  end

  def mark_as_errored(title:, body:, type:)
    update!(state: "error",
            error_title: title,
            error_body: body,
            error_type: type)
  end

  def mark_as_complete(pull_request:)
    update!(state: "complete",
            pull_request: pull_request)
  end

  def hydro_entity_payload
    {
      id: id,
      state: state,
      reason: reason,
      trigger: trigger_type,
      manifest_path: manifest_path,
      package_name: package_name,
      error_title: error_title,
      error_body: error_body,
      error_type: error_type,
      created_at: created_at,
      updated_at: updated_at,
      # We still have rows with `trigger_type` of `dry_run`, so we cannot
      # rely on the attribute until they are cleaned up.
      #
      # See: https://github.com/github/dependabot-api/issues/442
      dry_run: dry_run?,
    }
  end

  # Returns true if the update was set to errored by the cleanup job instead
  # of the Dependabot service.
  def timed_out?
    error_type == "timed_out"
  end

  private

  def dependabot_package_manager
    dg_package_manager = DependencyManifestFile.corresponding_package_type(path: manifest_path)

    return :unknown unless dg_package_manager

    case dg_package_manager
    when :rubygems then :bundler
    when :npm then :npm_and_yarn
    else dg_package_manager
    end
  end

  def instrument_creation
    GitHub.dogstats.increment("repository_dependency_update.created",
                              tags: [
                                "trigger:#{trigger_type}",
                                "package_manager:#{package_manager}",
                                "manifest_type:#{manifest_type}",
                                "retry:#{retry?}",
                              ])

    GlobalInstrumenter.instrument("repository_dependency_update.created", {
      repository_dependency_update: self,
      repository: repository,
    })
  end

  def instrument_update
    return unless saved_change_to_attribute? :state

    duration = Time.now - created_at
    GitHub.dogstats.timing("repository_dependency_update.duration",
                           duration,
                           tags: [
                             "trigger:#{trigger_type}",
                             "package_manager:#{package_manager}",
                             "manifest_type:#{manifest_type}",
                             "state:#{state}",
                             "viable_update:#{viable_update?}",
                             "retry:#{retry?}",
                             "lt_5m:#{duration < 5.minutes}",
                             "lt_20m:#{duration < 20.minutes}",
                             "lt_12h:#{duration < 12.hours}",
                             "lt_24h:#{duration < 24.hours}",
                           ])

    case state
    when "complete"
      instrument_complete
    when "error"
      instrument_error
    end
  end

  def instrument_complete
    GitHub.dogstats.increment("repository_dependency_update.completed",
                              tags: [
                                "trigger:#{trigger_type}",
                                "package_manager:#{package_manager}",
                                "manifest_type:#{manifest_type}",
                                "retry:#{retry?}",
                              ])

    GlobalInstrumenter.instrument("repository_dependency_update.complete", {
      repository_dependency_update: self,
      repository: repository,
      pull_request: pull_request,
    })
  end

  def instrument_error
    GitHub.dogstats.increment("repository_dependency_update.errored",
                              tags: [
                                "trigger:#{trigger_type}",
                                "package_manager:#{package_manager}",
                                "manifest_type:#{manifest_type}",
                                "retry:#{retry?}",
                                "viable_update:#{viable_update?}",
                                "timed_out:#{timed_out?}",
                              ])

    GlobalInstrumenter.instrument("repository_dependency_update.errored", {
      repository_dependency_update: self,
      repository: repository,
    })
  end

  # Returns true if the update is successful or viable to succeed. Excludes
  # expected errors and unsupported features (private registries) from
  # Dependabot's SLO on dependency update success rate.
  def viable_update?
    # - `pull_request_creation_halted` the pull request no longer needs to be
    #   created due to a deactived install or existing pull request
    # - `update_not_possible` e.g. peer/parent dependency constraints prevents
    #   the depedency from being updated to a non-vulnerable version
    expected_errors = %w(
      pull_request_creation_halted
      update_not_possible
    )

    # - `dependency_file_not_supported` when the current version can't be
    #   deteremined (e.g. missing lockfile)
    # - `git_dependencies_not_reachable` fetching dependencies from private git
    #   repositories
    # - `missing_environment_variable` unreachable private PHP dependencies
    # - `private_source_*` private registry credentials missing
    unsupported_errors = %w(
      dependency_file_not_supported
      git_dependencies_not_reachable
      missing_environment_variable
      private_source_not_reachable
      private_source_authentication_failure
      private_source_timed_out
      private_source_certificate_failure
    )

    # The following errors are known error-cases in dependabot-core resulting in
    # a user-facing error message explaining why the update failed.
    user_errors = %w(
      branch_not_found
      dependency_file_not_found
      dependency_file_not_evaluatable
      dependency_file_not_parseable
      dependency_file_not_resolvable
      path_dependencies_not_reachable
      git_dependency_reference_not_found
      go_module_path_mismatch
      bad_gitmodules
      all_versions_ignored
      pull_request_exists_for_latest_version
    )

    (expected_errors + unsupported_errors + user_errors).exclude?(error_type)
  end

  def enqueue_update_request_job
    return unless GitHub.dependabot_enabled?

    if repository.dependabot_installed?
      enqueue_vulnerability_update_in_hydro
    else
      # If the user has requested this update from the UI,
      # or vulerability alert was created, let's go ahead and
      # install Dependabot so we can complete it.
      # This will call back into #enqueue_vulnerability_update_in_hydro after
      # the installation is finished.
      Dependabot.enroll_for_single_update(dependency_update: self)
    end
  end

  def enqueue_vulnerability_update_in_hydro
    return unless vulnerability? && repository_vulnerability_alert

    GlobalInstrumenter.instrument("repository_dependency_update.created.vulnerability", {
      repository_dependency_update: self,
      repository_vulnerability_alert: repository_vulnerability_alert,
      security_advisory: repository_vulnerability_alert.vulnerability.becomes(SecurityAdvisory),
      security_vulnerability: repository_vulnerability_alert.vulnerable_version_range.becomes(SecurityVulnerability),
      github_bot_install_id: repository.dependabot_install.id,
    })

    RepositoryDependencyUpdateCleanupJob.set(wait: maximum_resolution_time).perform_later(id)
  end

  def maximum_resolution_time
    return DEPENDABOT_MAX_WAIT_FOR_MANUAL if manual?

    DEPENDABOT_MAX_WAIT
  end

  # We must ensure we don't associate another Repository's Pull Request by mistake.
  def pull_request_belongs_to_repository
    return unless pull_request && repository

    unless pull_request.repository == repository
      errors.add(:pull_request, "Pull Request must belong to Repository '#{repository.nwo}'")
    end
  end

  def manifest_path_is_supported
    unless self.class.manifest_path_supported?(manifest_path)
      errors.add(:manifest_path, "Dependabot cannot perform upgrades on this manifest file")
    end
  end

  def no_new_dry_run_triggers
    if trigger_type == "dry_run"
      errors.add(:trigger_type, "`dry_run` is no longer supported for new data.")
    end
  end

  def set_retry_flag
    # retry must not be nil, so ensure we prefer false if there is no repository alert
    self.retry = repository_vulnerability_alert&.repository_dependency_updates&.any? || false
  end
end
