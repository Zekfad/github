# rubocop:disable Style/FrozenStringLiteralComment

class MigrationFile < ApplicationRecord::Domain::Migrations
  areas_of_responsibility :migration
  include ::Storage::Uploadable

  belongs_to :migration

  before_destroy :storage_delete_object
  before_validation :set_guid, on: :create
  after_save :update_migration_archive_size

  belongs_to :storage_blob, class_name: "Storage::Blob"
  belongs_to :uploader, class_name: "User", foreign_key: :uploader_id

  validate :storage_ensure_inner_asset
  validates_presence_of :content_type, :name, :uploader

  validate :storage_ensure_asset_size

  add_uploadable_policy_attributes :migration_id, :name, :supports_multi_part_upload, :part_number, :part_sha, :multi_part_upload_id

  scope :older_than, lambda { |time| where("created_at < ?", time) }

  enum state: Storage::Uploadable::STATES

  # These attributes are used when the
  # uploadable is uploaded in parts via a multi-part upload.
  attr_accessor :supports_multi_part_upload, :part_number, :part_sha, :multi_part_upload_id

  def self.storage_new(uploader, blob, meta)
    new(
      storage_blob: blob,
      uploader: uploader,
      content_type: meta[:content_type],
      name: meta[:name],
      size: meta[:size],
      oid: meta[:oid],
      migration_id: meta[:migration_id],
    )
  end

  def self.storage_create(uploader, blob, meta)
    storage_new(uploader, blob, meta).tap do |file|
      file.update!(state: :uploaded)
    end
  end

  def set_multi_part_attributes(state: nil, attributes: {})
    self.supports_multi_part_upload = true
    self.state = state if state.present?

    self.migration_id = attributes["migration_id"] if attributes["migration_id"]
    self.part_number = attributes["part_number"] if attributes["part_number"]
    self.part_sha = attributes["sha256"] if attributes["sha256"]

    if attributes["upload_id"]
      self.multi_part_upload_id = attributes["upload_id"]
    elsif attributes["multi_part_upload_id"]
      self.multi_part_upload_id = attributes["multi_part_upload_id"]
    end
  end

  def set_multi_part_attributes!(**args)
    set_multi_part_attributes(**args)
    save!
  end

  def storage_uploadable_attributes
    { migration_id: migration.id }
  end

  def creation_url
    "#{GitHub.storage_cluster_url}/migrations/" +
      "#{migration.id}/archive"
  end

  def storage_upload_path_info(policy)
    "/internal/storage/migrations/#{migration_id}/archive"
  end

  def storage_policy_api_url
    "/organizations/%d/migrations/%d/archive" % [migration.owner_id, migration_id]
  end

  def storage_cluster_url(policy)
    "#{GitHub.storage_cluster_url}/migrations/" +
      "#{migration_id}/archive/#{guid}"
  end

  def storage_download_path_info(policy)
    "#{storage_upload_path_info(policy)}/#{guid}"
  end

  def storage_external_url(actor = nil)
    url = storage_policy(actor: actor).download_url
    if GitHub.storage_cluster_enabled? && GitHub.storage_private_mode_url
      url = url.sub(GitHub.storage_cluster_url, GitHub.storage_private_mode_url)
    end
    url
  end

  def download_url(actor:)
    storage_policy(actor: actor).download_url
  end

  def storage_policy(actor: nil, repository: nil)
    klass = if GitHub.storage_cluster_enabled?
      ::Storage::ClusterPolicy
    else
      ::Storage::S3Policy
    end
    klass.new(self, actor: actor, repository: nil)
  end

  def storage_s3_key(policy)
    if storage_provider == :s3_production_data
      "#{migration_id}/#{id}"
    else
      "#{GitHub.migration_file_base_path}/#{migration_id}/#{id}"
    end
  end

  def storage_s3_download_query(query)
    query["response-content-disposition"] = filename_content_disposition
    query["response-content-type"] = content_type
  end

  include Instrumentation::Model
  delegate :event_prefix, :event_payload, to: :migration

  def download
    instrument :download
  end

  def self.storage_s3_new_bucket
    "github-#{Rails.env.downcase}-migration-file-2a66d9"
  end

  def self.storage_s3_bucket
    GitHub.s3_environment_config[:asset_bucket_name]
  end

  def filename_content_disposition
    "filename=#{migration.guid}.tar.gz"
  end

  # s3 storage settings

  def storage_s3_bucket
    if storage_provider == :s3_production_data
      self.class.storage_s3_new_bucket
    else
      self.class.storage_s3_bucket
    end
  end

  def storage_s3_access_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_access_key
    else
      GitHub.s3_environment_config[:access_key_id]
    end
  end

  def storage_s3_secret_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_secret_key
    else
      GitHub.s3_environment_config[:secret_access_key]
    end
  end

  def storage_s3_upload_header
    {
      "Content-Type" => content_type,
      "Cache-Control" => "max-age=2592000",
      "x-amz-meta-Surrogate-Control" => "max-age=31557600",
    }
  end

  # Storage client for model. Used for multipart uploads
  def storage_client
    return if GitHub.storage_cluster_enabled?
    GitHub.s3_primary_client
  end

  private

  def update_migration_archive_size
    migration&.update_column :archive_size, size

    # This method shouldn't prevent a MigrationFile from saving or break the
    # callback chain
    # https://apidock.com/rails/ActiveRecord/Callbacks/after_save#656-Return-True
    true
  end
end
