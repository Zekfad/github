# rubocop:disable Style/FrozenStringLiteralComment

module Ability::Graph
  class Validator
    attr_reader :query
    delegate :actors, :subjects, :select_columns, :count_by, :actions, :through, :uniq, :sort, :limit, :count, to: :query

    def initialize(query)
      @query = query
    end

    def self.validate!(query)
      new(query).validate!
    end

    def validate!
      validate_actions!
      validate_actors_and_subjects_present!
      validate_actors_or_subjects_count!
      validate_select_compatibilties!
      validate_count_by_compatibilities!
      validate_count_compatibilities!
      validate_select_columns!
      validate_uniq_compatibilities!
      validate_sort!
      validate_limit!
    end

    private

    def validate_actions!
      unless actions.blank? || actions == :all || actions.all? { |action| Ability.actions.include?(action) }
        raise InvalidQuery, "Actions expected .actions were any of: #{Ability.actions.keys.join(", ")}, but #{actions.join(", ")} provided"
      end
    end

    def validate_actors_and_subjects_present!
      if actors.blank? || subjects.blank?
        raise InvalidQuery, "Query needs to have an actor and a subject"
      end
    end

    def validate_actors_or_subjects_count!
      if actors.size > 1 && subjects.size > 1
        raise InvalidQuery, "Multiple subject and actor queries is not allowed"
      end
    end

    # For the moment we don't allow selecting a subset of the records for indirect abilties
    def validate_select_compatibilties!
      if select_columns.size > 0 && count_by.present?
        raise InvalidQuery, ".get together with .count_by is not allowed"
      end

      if through.present? && !select_columns.all? { |column| Ability::Graph::Query::DIRECT_TO_INDIRECT_MAP.include?(column) }
        invalid_columns = select_columns - Ability::Graph::Query::DIRECT_TO_INDIRECT_MAP.keys
        raise InvalidQuery, ".get together with .through does not support the following columns: #{invalid_columns}"
      end
    end

    # For the moment we don't allow counts on indirect abilities (as there are unions)
    def validate_count_by_compatibilities!
      if count_by.present? && (through.present? || limit.present?)
        raise InvalidQuery, ".count_by together with .through or .limit is not allowed"
      end
    end

    # We don't allow counts and limits or count_by
    def validate_count_compatibilities!
      if count.present? && (count_by.present? || limit.present?)
        raise InvalidQuery, ".count together with .limit or .count_by is not allowed"
      end
    end

    def validate_select_columns!
      unless select_columns.all? { |column| Ability.column_names.include?(column.to_s) }
        raise InvalidQuery, "Columns expected for .get were any of: #{Ability.column_names.join(", ")}, but #{select_columns.join(", ")} provided"
      end
    end

    def validate_uniq_compatibilities!
      if uniq.present? && (!select_columns.present? || select_columns.length > 1)
        raise InvalidQuery, ".uniq is only allowed when a single column is selected using .get"
      end
    end

    def validate_sort!
      if sort.present?
        sort.each do |column, direction|
          unless Ability.column_names.include?(column.to_s)
            raise InvalidQuery, "#{column} used in .sort is not a valid column"
          end
          unless [:asc, :desc].include?(direction)
            raise InvalidQuery, "#{direction} used in .sort is not a valid direction"
          end
        end
      end
    end

    def validate_limit!
      if limit.present? && (!limit.is_a?(Integer) || limit < 1)
        raise InvalidQuery, ".limit needs to be greater than 0, but #{limit} provided"
      end
    end
  end
end
