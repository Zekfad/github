# rubocop:disable Style/FrozenStringLiteralComment

module Ability::Graph
  class Builder

    def initialize(&block)
      @subjects = {}
      @actors = {}
      @actions = []
      @select_columns = []
      @through = []
      @sort = {}
      @count_by = nil
      @count = false
      @uniq = false
      @limit = nil

      block.call(self) if block
    end

    def actions(action_names)
      if action_names == :any || action_names == :all
        @actions = :all
      else
        @actions = Array.wrap(action_names)
      end
    end
    alias_method :action, :actions

    def count
      @count = true
    end

    def count_by(column)
      @count_by = column == :actor ? "actor_id" : "subject_id"
    end

    def first
      limit 1
    end

    # Sets the actors
    def from(actor_type, actor_ids = :all)
      if actor_ids == :all || actor_ids == :any
        @actors[actor_type.to_s] = :all
      else
        @actors[actor_type.to_s] = Array.wrap(actor_ids)
      end
    end

    def get(*columns)
      @select_columns = columns.compact
    end

    def limit(limit)
      @limit = limit
    end

    def through(type)
      @through << type
    end

    def sort(criteria)
      @sort.merge!(criteria)
    end

    # Sets the subjects
    def to(subject_type, subject_ids = :all)
      if subject_ids == :all || subject_ids == :any
        @subjects[subject_type.to_s] = :all
      else
        @subjects[subject_type.to_s] = Array.wrap(subject_ids)
      end
    end

    def uniq
      @uniq = true
    end


    def build
      query = Query.new(actors: @actors, subjects: @subjects, select_columns: @select_columns, count_by: @count_by, actions: @actions, through: @through, uniq: @uniq, sort: @sort, limit: @limit, count: @count)
      Validator.validate!(query)
      query
    end
  end
end
