# frozen_string_literal: true

module Ability::Graph
  InvalidQuery = Class.new(StandardError)
end
