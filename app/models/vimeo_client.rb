# frozen_string_literal: true
require "json"

class VimeoClient
  VIMEO_API_HOST = "https://api.vimeo.com"

  class VimeoClientError < StandardError; end;

  def initialize
    @client = Faraday.new(VIMEO_API_HOST, request: { open_timeout: 1, timeout: 5 })
  end

  def get_video(id)
    @client.authorization(:Bearer, @unauthenticated_token ||= self.generate_unauthenticated_token)
    video_response = @client.get("/videos/#{id}")
    JSON.parse(video_response.body)
  rescue Faraday::ConnectionFailed, Faraday::ClientError,
    Faraday::ResourceNotFound, Faraday::TimeoutError, Faraday::SSLError, ArgumentError => e
    raise VimeoClient::VimeoClientError, "Unable to get Vimeo video"
  end

  # This is used for public vimeo content
  def generate_unauthenticated_token
    begin
      @client.basic_auth(GitHub.vimeo_client_id, GitHub.vimeo_client_secret)
      auth_response = @client.post("/oauth/authorize/client", { "grant_type" => "client_credentials", "scope" => ["public"] })
    rescue Faraday::ConnectionFailed, Faraday::ClientError,
      Faraday::ResourceNotFound, Faraday::TimeoutError, Faraday::SSLError => e
      raise VimeoClient::VimeoClientError, "Unable to create access token"
    end
    JSON.parse(auth_response.body)["access_token"]
  end
end
