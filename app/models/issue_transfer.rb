# frozen_string_literal: true

class IssueTransfer < ApplicationRecord::Collab
  STATES = %w[started done errored]
  REASONS = [
    "Misfiled",
    "Sensitive",
    "Reorganization",
    "Other",
  ]

  ISSUE_EVENT_BLOCKLIST = %w[
    labeled
    unlabeled
    milestoned
    demilestoned
    added_to_project
    moved_columns_in_project
    removed_from_project
    converted_note_to_issue
    connected
    disconnected
  ]

  belongs_to :old_repository, class_name: "Repository", required: true
  belongs_to :old_issue, class_name: "Issue", required: true

  belongs_to :new_repository, class_name: "Repository", required: true
  belongs_to :new_issue, class_name: "Issue", required: true

  belongs_to :actor, class_name: "User"

  before_validation :set_initial_state, on: :create
  before_validation :set_old_issue_number, on: :create

  validates :old_issue_number, presence: true
  validates :state, presence: true, inclusion: { in: STATES }
  validates :reason, inclusion: { in: REASONS, allow_nil: true }
  validate :old_issue_unlocked
  validate :actor_permissions
  validate :repos_not_archived
  validate :repos_have_issues
  validate :transferrable_by_actor
  validate :with_same_owner
  validate :not_transferring_private_to_public

  def self.find_from(repository:, number:, attempts: 10)
    first_transfer = find_by(old_repository_id: repository.id, old_issue_number: number)

    if first_transfer
      attempts.times.inject(first_transfer) do |transfer|
        # Is this the final transfer? Return it!
        return transfer if transfer.new_issue

        # Can we find a new issue transfer? If not, bail out
        return unless next_transfer = find_by(old_issue_id: transfer.new_issue_id)

        next_transfer
      end
    end

    nil
  end

  def async_transfer!
    create_copy_issue unless new_issue
    TransferIssueJob.perform_later(self)
  end

  def transfer!(staff_user = nil)
    create_copy_issue unless new_issue
    complete_transfer(staff_user)
  end

  def complete_transfer(staff_user = nil)
    return unless old_issue
    audit_log_user = (staff_user.present? && staff_user.staff?) ? staff_user : actor

    begin
      old_issue.assignments.each do |assignment|
        assignment.throttle { assignment.update_column(:issue_id, new_issue.id) }
      end

      old_issue.events.each do |event|
        next if ISSUE_EVENT_BLOCKLIST.include?(event.event)

        event.throttle { event.update_columns(issue_id: new_issue.id, repository_id: new_issue.repository_id) }
      end

      old_issue.comments.each do |comment|
        comment.throttle do
          comment.update_columns(issue_id: new_issue.id,
                                 repository_id: new_issue.repository_id,
                                 body: replace_bare_issue_mentions(comment.body.dup))
        end
      end

      old_issue.reactions.each do |reaction|
        reaction.throttle { reaction.update_column(:subject_id, new_issue.id) }
      end

      old_issue.close_issue_references.xref.each do |close_issue_reference|
        close_issue_reference.throttle do
          close_issue_reference.update_columns(issue_id: new_issue.id,
                                               issue_repository_id: new_issue.repository_id)
        end
      end

      # For Issues that belong to Projects, update any of their cards
      old_issue.cards.each do |old_card|
        old_card.throttle { old_card.transfer_issue_card(new_issue.id) }
      end

      # Reload cards we just moved to avoid deleting things with dependent destroys
      old_issue.cards.reload

      # TODO resubscribe
      # issue.each_subscriber.each do |subscriber|
      #   reasons = issue.subscription_reason(subscriber)
      #   new_issue.subscribe(subscriber, reasons?)
      # end

      new_issue.synchronize_search_index

      # Show an event on the new issue
      new_issue.events.create!(actor: actor, event: "transferred", subject: old_repository)

      # Keep the original created_at
      new_issue.update_column(:created_at, old_issue.created_at)

      # Trigger event for audit log
      old_issue.instrument(:transfer,
                           actor: audit_log_user,
                           issue_transfer: self)

      # Set deletion_hook_action to signify this is a transfer event
      old_issue.deletion_hook_action = :transferred

      # Get rid of the old one and sends deletion webhook
      old_issue.destroy

      update_attribute(:state, "done")
    rescue => e
      Failbot.report(e)
      update_attribute(:state, "errored")
      unless Rails.env.production?
        raise e
      end
    end

    nil
  end

  # This is triggered from stafftools for issue transfers that have been stuck
  def retry_transfer
    begin
      Rails.logger.info "Retrying to transfer issue #{self.old_issue_id} starting."

      new_issue.synchronize_search_index

      # Show an event on the new issue
      new_issue.events.create!(actor: actor, event: "transferred", subject: old_repository)

      update_attribute(:state, "done")
      Rails.logger.info "Retrying to transfer issue #{self.old_issue_id} to #{self.new_issue_id} finished."
      rescue => e
        Rails.logger.info "Retrying to transfer issue #{self.old_issue_id} to #{self.new_issue_id} failed."
        Failbot.report(e)
        update_attribute(:state, "errored")
        unless Rails.env.production?
          raise e
        end
    end
  end

  private

  def set_initial_state
    self.state = "started"
  end

  def set_old_issue_number
    self.old_issue_number = old_issue.number
  end

  def create_copy_issue
    self.new_issue = Issue.create!(
      repository: new_repository,
      user_id: old_issue.safe_user.id,
      title: old_issue.title,
      body: replace_bare_issue_mentions(old_issue.body.dup),
      issue_comments_count: old_issue.issue_comments_count,
      state: old_issue.state,
      user_hidden: old_issue.user_hidden,
      performed_by_integration_id: old_issue.performed_by_integration_id,
      transfer: true,
    )

    save!
  end

  # Replaces bare issue references `#1` in issue or comment body with
  # global repo issue reference `user/project#num`
  # Inspired by / borrowed from GitHub::HTML::IssueMentionFilter#replace_bare_issue_mentions
  #
  # @param body [String, nil] - the body from the `old_issue` or issue comment
  #
  # @return [String, nil]
  def replace_bare_issue_mentions(body)
    return body if body.nil?

    issue_reference_text = /(?<=\s|^)(gh-|#)(\d+)\b/i

    body.gsub(issue_reference_text) do |match|
      _pound, number = $1, $2.to_i

      if issue = old_repository.issues.find_by(number: number)
        # link to the old repository
        "#{old_repository.name_with_owner}##{number}"
      elsif issue_transfer = IssueTransfer.find_from(repository: old_repository, number: number, attempts: 1)
        transferred_issue = issue_transfer.new_issue
        "#{transferred_issue.repository.name_with_owner}##{transferred_issue.number}"
      else
        match
      end
    end
  end

  def transferrable_by_actor
    if !old_issue.transferrable_by?(actor)
      errors.add(:old_issue, "is not transferrable")
    end
  end

  def with_same_owner
    unless new_repository.owner_id == old_repository.owner_id
      errors.add(:new_repository, "must have the same owner as the current repository")
    end
  end

  def not_transferring_private_to_public
    if old_repository.private? && new_repository.public?
      errors.add(:old_issue, "cannot be transferred from private repository to public repository")
    end
  end

  def actor_permissions
    acting_user = actor.bot? ? actor.installation : actor
    unless new_repository.resources.contents.writable_by?(acting_user)
      errors.add(:actor, "must have write permissions on new repository")
    end

    unless old_repository.resources.contents.writable_by?(acting_user)
      errors.add(:actor, "must have write permissions on old repository")
    end
  end

  def old_issue_unlocked
    if old_issue.locked?
      errors.add(:old_issue, "cannot be locked")
    end
  end

  def repos_not_archived
    if new_repository.archived?
      errors.add(:new_repository, "must not be archived")
    end

    if old_repository.archived?
      errors.add(:old_repository, "must not be archived")
    end
  end

  def repos_have_issues
    if !new_repository.has_issues?
      errors.add(:new_repository, "must have issues enabled")
    end

    if !old_repository.has_issues?
      errors.add(:old_repository, "must have issues enabled")
    end
  end
end
