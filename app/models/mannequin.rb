# rubocop:disable Style/FrozenStringLiteralComment

##
# Mannequin
#
# Mannequins are models that look like users, but aren't users at all! They're
# used to attribute data that gets imported to GitHub using GitHub::Migrator
# when that data has no existing user that it can be attributed to. We use
# this model to buy back some fidelity and show helpful context such as a
# display name.
class Mannequin < User
  areas_of_responsibility :migration

  before_validation :set_login!, on: :create

  belongs_to :migration, optional: false
  delegate :source_product, to: :migration, allow_nil: true
  alias_attribute :display_login, :source_login
  alias_attribute :to_s, :source_login

  validates_presence_of :source_login

  has_many :emails, dependent: :delete_all, class_name: "MannequinEmail"

  # This is required due to the nonstandard way we do STI and polymorphism
  # between User, Organization, Mannequin, and Project. For more context, check
  # out Project#ensure_correct_owner_type, and Organization's `has_many
  # :projects` line. Once those are made standard, this can be removed.
  has_many :projects, -> { where(owner_type: "Mannequin") },
    foreign_key: :owner_id

  # Mannequins don't have passwords - you can't log in as them.
  def password_required?
    false
  end

  def email_address_required?
    false
  end

  def must_verify_email?
    false
  end

  def receives_confirmation_when_destroyed?
    false
  end

  # Public: A Mannequin can never be authenticated by a password.
  #
  # Returns false
  def authenticated_by_password?(password = nil)
    GitHub.dogstats.increment("user", tags: ["action:mannequin_login_attempt"])
    false
  end

  def path
    "#"
  end

  def mannequin?
    true
  end

  def user?
    false
  end

  def anonymous_user_email
    StealthEmail.new(self).email
  end

  def organization?
    false
  end

  def bot?
    false
  end

  # A Mannequin shouldn't be granted any permissions ever
  def can_be_granted_abilities?
    false
  end

  def organizations
    User.where(id: migration.owner_id)
  end

  # This overrides User#instrument_user_signup so that Mannequins don't
  # publish user.signup hydro events.
  #
  # Returns nothing.

  def instrument_user_signup
    # no=op
  end

  def add_email(email, options = {})
    self.emails.build(email: email)

    save
  end

  def email
    @email ||= emails.last.try(:email)
  end

  def uniqueness_of_email
    # Mannequins don't care about uniqueness of email, so this validation
    # method inherited from User can be a no-op.
  end

  def name
    # no=op
  end

  def never_spammy?
    true
  end

  private

  def set_login!
    self.login = SecureRandom.uuid
  end
end
