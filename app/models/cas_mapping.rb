# frozen_string_literal: true

class CasMapping < ApplicationRecord::Domain::Users
  belongs_to :user
  validates_presence_of :user, :username
  validates_uniqueness_of :username, case_sensitive: true
end
