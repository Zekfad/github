# rubocop:disable Style/FrozenStringLiteralComment

class IssueCommentEdit < ApplicationRecord::Domain::Repositories
  include FilterPipelineHelper
  include Instrumentation::Model
  include UserContentEdit::Core

  belongs_to :issue_comment

  alias_attribute :user_content_id, :issue_comment_id
  alias_method :user_content, :issue_comment
  alias_method :async_user_content, :async_issue_comment

  def user_content_type
    "IssueComment"
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, user_content_edit_id || "IssueCommentEdit:#{id}")
  end
end
