# frozen_string_literal: true

class Discussion
  # Handles the various control flows through Issues. Specifically, it manages
  # things like query parameters and redirection logic.
  class ListControlFlow
    include DiscussionsHelper
    include GitHub::Application.routes.url_helpers

    def initialize(params:, repo:, parsed_discussions_query:)
      @params = params
      @repo = repo
      @parsed_discussions_query = parsed_discussions_query
    end

    # Public: Does the page need to be redirected?
    # This could be due to query state params that need to be merged into the
    # search string query param.
    #
    # Returns a Boolean.
    def needs_redirection?
      has_query_state_params?
    end

    def redirect_path
      if has_query_state_params?
        rewritten_path_for_query_state_params
      end
    end

    private

    attr_reader :params, :parsed_discussions_query, :repo

    def current_repository
      repo
    end

    def has_query_state_params?
      # Will have an author param if user submits form in author filter box
      has_existing_search? && params.key?(:author)
    end

    def has_existing_search?
      params[:discussions_q].present?
    end

    def rewritten_path_for_query_state_params
      replacements = {}

      if author = params.delete(:author)
        replacements[:author] = author
      end

      discussions_search_path(replace: replacements)
    end
  end
end
