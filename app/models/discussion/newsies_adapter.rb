# frozen_string_literal: true

module Discussion::NewsiesAdapter
  include SubscribableThread

  URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/discussions/{number}").freeze

  def async_viewer_can_delete?(viewer)
    return Promise.resolve(false) unless viewer

    # Preload repo and its owner for use by lib/permissions/enforcer.rb for a
    # Discussion subject when checking authzd permissions.
    async_repository_and_owner.then do
      deletable_by?(viewer)
    end
  end

  def author_subscribe_reason
    :author
  end

  def subscribable_by?(candidate)
    notifications_list.public? || notifications_list.readable_by?(candidate)
  end

  def notifications_thread
    self
  end

  def notifications_list
    async_notifications_list.sync
  end

  def async_notifications_list
    async_repository
  end

  def deliver_notifications?
    return false unless open?
    return false unless repository.active?
    return false if repository.disable_discussions_notifications_flag_enabled?

    # If feature is enabled for this repository, we can deliver notifications
    repository.discussions_enabled?
  end

  def get_notification_summary
    return if destroyed?
    list = Newsies::List.new("Repository", repository_id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, self)
  end

  def destroy_notification_summary
    repo = repository || Repository.new.tap { |r| r.id = repository_id }
    GitHub.newsies.async_delete_all_for_thread(repo, self)
  end

  def update_notification_rollup(summary)
    suffix = summarizable_changed?(:title, :body) ? :changed : :unchanged
    GitHub.dogstats.increment("newsies.rollup", tags: ["type:#{suffix}"])

    summary.summarize_discussion(self)
  end

  def notifications_author
    user
  end

  def async_entity
    async_repository
  end

  def entity
    repository
  end

  # Internal: Filters out users that should keep a subscription to this thread.
  # This should be called after a comment has been edited, with a mentioned
  # user removed due to a typo.  Remove anyone that hasn't commented already.
  #
  # users - Array of Users.
  #
  # Returns an Array of User that can be unsubscribed.
  def unsubscribable_users(users)
    commenters = Set.new
    comments.select("DISTINCT user_id").each do |comment|
      commenters << comment.user_id
    end
    commenters.add(user_id)
    users.reject { |user| commenters.include?(user.id) }
  end

  def message_id
    "<#{repository.name_with_owner}/repo-discussions/#{number}@#{GitHub.urls.host_name}>"
  end

  def async_repository_and_owner
    async_repository.then(&:async_owner)
  end

  # Absolute permalink URL for the discussion.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                discussion.permalink(include_host: false) => `/github/github/discussions/1`
  #
  # Returns a String URL to view this discussion.
  def permalink(include_host: true)
    "#{repository.permalink(include_host: include_host)}/discussions/#{to_param}"
  end
  alias_method :url, :permalink

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_repository_and_owner.then do
      URI_TEMPLATE.expand(
        owner:  repository.owner.login,
        name:   repository.name,
        number: number,
      )
    end
  end
end
