# frozen_string_literal: true

module Discussion::TransferAdapter
  # Public: Is this discussion in a state such that it could be transferred
  # to another repository?
  #
  # Returns a Boolean.
  def can_be_transferred?
    open? && repository&.has_discussions? && !repository.archived?
  end

  # Public: Can the given actor transfer this discussion to another repository?
  def transferrable_by?(actor)
    can_be_transferred? && modifiable_by?(actor)
  end

  # Public: Can this discussion be moved to the given repository by the given user?
  #
  # new_repo - a Repository
  # actor - the current User
  #
  # Returns a Boolean.
  def can_transfer_to?(new_repo, actor:)
    # Can't transfer to a repo that doesn't have Discussions turned on
    return false unless new_repo&.has_discussions?

    # Can't transfer to a repo with a different owner
    return false unless repository && new_repo.owner_id == repository.owner_id

    # Can't transfer into an archived repository
    return false if new_repo.archived?

    # User lacks permission on this discussion to transfer it
    return false unless transferrable_by?(actor)

    # User requires write permission on new repo to transfer there
    return false unless new_repo.resources.contents.writable_by?(actor)

    # Can't transfer from private->public repo
    return false if !public? && new_repo.public?

    true
  end

  # Public: Get repositories this discussion could be transferred to.
  #
  # viewer - the currently authenticated User or nil
  # query - optional String to filter repositories by
  #
  # Returns a Promise resolving to a list of Repositories.
  def async_possible_transfer_repositories(viewer:, query: nil)
    async_repository.then do |repository|
      # Grab other repositories by the same owner as this discussion's
      # current repository:
      target_repo_scope = Repository.where(owner_id: repository.owner_id).
        filter_spam_and_disabled_for(viewer).
        with_discussions_enabled.
        not_archived_scope

      target_repo_scope = target_repo_scope.private_scope if repository.private?
      target_repo_scope = target_repo_scope.search(query) if query.present?

      # Exclude this discussion's repository:
      target_repo_scope_ids = target_repo_scope.ids - [repository_id]

      repository_ids = viewer.associated_repository_ids(min_action: :write,
        repository_ids: target_repo_scope_ids)
      repos = Repository.where(id: repository_ids).order(updated_at: :desc)
      repos.select(&:discussions_enabled?)
    end
  end
end
