# frozen_string_literal: true

class SponsorsCriterion < ApplicationRecord::Collab
  include GitHub::Relay::GlobalIdentification

  self.table_name = "sponsors_criteria"

  enum criterion_type: [:checkbox, :text]
  enum applicable_to: [:all, :user, :organization], _prefix: true

  # Slugs should be be underscored and without spaces, and not begin or end
  # with an underscore, e.g. `account_age`
  SLUG_REGEXP = /\A[a-z0-9]+(_[a-z0-9]+)*\z/i

  validates :slug,
    presence: true,
    uniqueness: { case_sensitive: false },
    format: { with: SLUG_REGEXP },
    length: { minimum: 3, maximum: 60 }
  validates :description, presence: true

  has_many :sponsors_memberships_criteria,
    class_name: "SponsorsMembershipsCriterion",
    dependent: :destroy

  scope :automated, -> { where(automated: true) }
  scope :manual, -> { where(automated: false) }

  # Public: Returns the criteria records that are applicable to a sponsorable.
  #
  # sponsorable - The User or Organization to get criteria for.
  #
  # Returns a SponsorsCriterion::ActiveRecord_Relation.
  def self.for(sponsorable)
    return self.none unless sponsorable.present?
    applicable = [:all]

    if sponsorable.organization?
      applicable << :organization
    else
      applicable << :user
    end

    where(active: true, applicable_to: applicable)
  end

  # Public: The context for audit log events involving a SponsorsCriterion.
  #
  # Returns a Hash.
  def event_context(prefix: event_prefix)
    {
      prefix => slug,
      "#{prefix}_id".to_sym => id,
    }
  end
end
