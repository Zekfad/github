# frozen_string_literal: true

# A `Business` represents an "enterprise" or "enterprise account" to end-users.
class Business < ApplicationRecord::Domain::Users
  include GitHub::Relay::GlobalIdentification
  include GitHub::Validations
  extend GitHub::Encoding
  include Ability::Subject
  include Ability::Membership
  include PrimaryAvatar::Model
  include Avatar::List::Model
  include Instrumentation::Model
  include GitHub::FlipperActor
  include GitHub::ApplicationsCreationLimit
  include Configurable::OktaTeamSyncBeta
  include Configurable::DefaultNewRepoBranch
  include Business::ActionsDependency
  include Business::BillingDependency
  include Business::BillingManagementDependency
  include Business::LicenseDependency
  include Business::ConfigurationDependency
  include Business::TwoFactorRequirementDependency
  include Business::SamlSsoDependency
  include Business::PeopleDependency
  include Business::SettingsDependency
  include Business::SupportEntitleeDependency

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::Business

  SLUG_REGEX = /\A[a-z0-9]+(-[a-z0-9]+)*\z/i
  SLUG_VALIDATION_MESSAGE = "may only contain alphanumeric characters or single hyphens, and cannot begin or end with a hyphen"
  MAX_NAME_LENGTH = 60
  MAX_SLUG_LENGTH = 60
  MAX_DESCRIPTION_LENGTH = 160
  MAX_WEBSITE_URL_LENGTH = 255
  MAX_LOCATION_LENGTH = 255
  TERMS_OF_SERVICE_TYPES = ["Corporate", "Custom", "ESA+Education"].freeze
  TERMS_OF_SERVICE_TYPE_DESCRIPTIONS = ["Corporate", "Custom", "Corporate + Education"].freeze
  MAX_TERMS_OF_SERVICE_NOTES_LENGTH = 255
  EXCLUDE_FROM_SINGLE_BUSINESS_ORGS = [GitHub.trusted_oauth_apps_org_name].freeze
  ADMIN_ROLES = {
      owner: "owner",
      billing_manager: "billing_manager",
  }

  # Raised when an attempt to delete the last admin is made.
  class NoAdminsError < StandardError; end

  # Raised when an attempt to create a business in addition to the single global
  # business is made. Only possible when GitHub.single_business_environment?
  class SingleGlobalBusinessError < StandardError; end

  # Raised if an attempt is made to add an admin without 2FA enabled
  # when the business requires 2FA to be enabled.
  class UserHasTwoFactorDisabledError < StandardError; end

  # Raised if trying to change the admin's user role (owner <=> billing manager)
  # but the given user isn't an admin
  class UserNotAnAdminError < StandardError; end

  # Raised when a forbidden attempt is made to remove a member from this Enterprise
  # Example: the user is the last admin in an Organization belonging to the Enterprise
  class ForbiddenRemovalError < StandardError; end

  # Raised when an attempt is made to remove a member from this Enterprise that cannot
  # be processed. Example: The Enterprise can't self-serve
  class InvalidRemovalError < StandardError; end

  belongs_to :customer
  has_many :audit_log_exports, as: :subject, dependent: :destroy
  has_many :audit_log_git_event_exports, as: :subject, dependent: :destroy
  has_many :hooks, as: :installation_target, dependent: :destroy
  has_many :organization_memberships, class_name: "Business::OrganizationMembership"
  has_many :organizations,
    -> {
      # In case the github-enterprise org is added to the single global business,
      # make sure they are never included in business org results.
      return scoped unless GitHub.single_business_environment?
      where.not(login: EXCLUDE_FROM_SINGLE_BUSINESS_ORGS)
    },
    through: :organization_memberships,
    dependent: :destroy

  has_many :credential_authorizations,
    through: :organizations,
    class_name: "Organization::CredentialAuthorization"
  has_many :user_licenses, -> { extending Business::UserLicensesExtension }

  has_one :prerelease_agreement,
    -> { where(member_type: "Business") },
    foreign_key: :member_id,
    as: :member,
    class_name: "PrereleaseProgramMember"

  has_one :saml_provider,
    class_name: "Business::SamlProvider",
    inverse_of: :target,
    dependent: :destroy

  has_one :team_sync_tenant,
    class_name: "TeamSync::BusinessTenant",
    inverse_of: :business,
    dependent: :destroy

  has_many :invitations, class_name: "BusinessMemberInvitation", dependent: :destroy
  has_many :organization_invitations, class_name: "BusinessOrganizationInvitation", dependent: :destroy
  has_many :user_accounts, class_name: "BusinessUserAccount", dependent: :destroy
  has_many :enterprise_agreements, class_name: "Billing::EnterpriseAgreement", dependent: :destroy

  # Public: Installations for an Integration
  has_many :integration_installations, as: :target, dependent: :destroy

  # Public: Integrations owned by this Business
  has_many :integrations,
    dependent: :destroy,
    as: :owner

  has_many :enterprise_installations, as: :owner, dependent: :destroy
  has_many :enterprise_installation_user_accounts_uploads, dependent: :destroy

  has_many :ssh_certificate_authorities, as: :owner, dependent: :destroy

  has_many :ip_whitelist_entries, as: :owner, dependent: :destroy

  has_many :bundled_license_assigments, class_name: "Billing::BundledLicenseAssignment"

  has_many :metered_usage_exports,
    class_name: "Billing::MeteredUsageExport",
    as: :billable_owner

  accepts_nested_attributes_for(:customer, update_only: true)

  before_validation :generate_slug, on: :create
  before_validation :set_default_terms_of_service_company_name
  before_create :ensure_single_global_business, if: -> { GitHub.single_business_environment? }
  after_create :grant_initial_owners
  after_create :create_user_accounts_for_members, unless: -> { GitHub.single_business_environment? }
  after_update :sync_all_organization_billing_settings, if: :synced_billing_settings_changed?
  after_create_commit  :instrument_creation
  after_destroy_commit :instrument_deletion
  after_commit :remove_billing_managers, on: :destroy
  after_commit :remove_support_entitlees, on: :destroy
  after_commit :instrument_update_terms_of_service, on: [:create, :update]
  after_commit :synchronize_search_index

  validates :slug,
    presence: true,
    format: { with: SLUG_REGEX, message: SLUG_VALIDATION_MESSAGE, allow_blank: true },
    uniqueness: { case_sensitive: false, message: "is already taken by another business" },
    if: :slug_changed?
  validates :name, presence: true, length: { maximum: MAX_NAME_LENGTH, allow_blank: true }
  validate  :ensure_initial_owners_are_all_users, on: :create
  validates :terms_of_service_type, presence: true, inclusion: {
    in: TERMS_OF_SERVICE_TYPES, message: "%{value} is not a valid terms of service type"
  }
  validates :terms_of_service_company_name, length: { maximum: MAX_NAME_LENGTH, allow_blank: true }
  validates :terms_of_service_notes, unicode3: true, length: { maximum: MAX_TERMS_OF_SERVICE_NOTES_LENGTH, allow_blank: true }
  validates :seats, presence: true, numericality: true
  validate  :ensure_enough_seats, if: :seats_changed?
  validates :website_url, unicode3: true, length: { maximum: MAX_WEBSITE_URL_LENGTH }
  validate :ensure_website_url_is_valid, if: :website_url?
  validates :location, unicode3: true, length: { maximum: MAX_LOCATION_LENGTH }
  validates :description, length: { maximum: MAX_DESCRIPTION_LENGTH }
  validates_format_of :billing_email,
    with: User::EMAIL_REGEX,
    message: "does not look like an email address",
    allow_blank: true
  validates :enterprise_web_business_id,
    numericality: { only_integer: true, greater_than: 0, allow_nil: true }

  force_utf8_encoding :name, :terms_of_service_company_name, :description

  # Internal: an abstract collection, for the sub-resources of an Business available for abilities
  def resources
    Business::Resources.new(self)
  end

  # Given an organization ID, find the business that the organization belongs to
  # in a single query.
  #
  # org_id - Integer organization ID
  #
  # Returns the owning Business otherwise nil.
  def self.from_org_id(org_id)
    return unless org_id
    joins(:organization_memberships)
      .where(business_organization_memberships: { organization_id: org_id })
      .first
  end

  alias_attribute :to_s, :slug

  def to_param
    slug
  end

  scope :by_slug, -> { order("slug ASC") }

  scope :not_staff_owned, -> { where(staff_owned: false) }

  # Public: Returns filtered organizations for the enterprise.
  #
  # viewer - optional - The User requesting filtered organizations.
  # query - optional - String user-provided query value.
  # order_by_field - optional — String specifying the sort field. Supported: "login", "created_at". Default: "login".
  # order_by_direction - optional - String specifying the sort direction. Supported: "asc", "desc". Default: "asc".
  # can_enable_two_factor - optional - Boolean, only return organizations
  #   that can have two-factor authentication enabled if truthy.
  # team_sync_statuses - options - Array of Symbol, only return organizations
  #   that have a team sync tenant with the appropriate status.
  #
  # Returns ActiveRecord::Relation.
  def filtered_organizations(
    viewer: nil,
    query: nil,
    order_by_field: "login",
    order_by_direction: "asc",
    can_enable_two_factor: nil,
    team_sync_statuses: nil
  )
    if can_enable_two_factor.nil?
      orgs = if viewer && !owner?(viewer)
        organizations_for_member(viewer)
      else
        organizations
      end
    else
      orgs = organizations_can_enable_two_factor_requirement(!!can_enable_two_factor)
    end

    if team_sync_statuses
      orgs = orgs.with_team_sync_status(team_sync_statuses)
    end

    orgs = apply_user_query_filter(orgs, query)

    # Ignore invalid order by field and direction
    order_by_field = "login" unless %w(login created_at).include?(order_by_field.to_s.downcase)
    order_by_direction = "asc" unless %w(asc desc).include?(order_by_direction.to_s.downcase)
    orgs.order(Arel.sql("users.#{order_by_field} #{order_by_direction}"))
  end

  # Public: Returns organizations within the enterprise without any admins.
  #
  # order_by_field - optional — String specifying the sort field. Supported: "login", "created_at". Default: "login".
  # order_by_direction - optional - String specifying the sort direction. Supported: "asc", "desc". Default: "asc".
  #
  # Returns ActiveRecord::Relation.
  def orphaned_organizations(
    order_by_field: "login",
    order_by_direction: "asc"
  )
    owned_orgs_ids = Ability.where(subject_id: organization_ids,
                                   subject_type: "Organization",
                                   actor_type: "User",
                                   action: "admin").pluck(:subject_id).uniq

    orgs = organizations.where(id: organization_ids - owned_orgs_ids)

    # Ignore invalid order by field and direction
    order_by_field = "login" unless %w(login created_at).include?(order_by_field.to_s.downcase)
    order_by_direction = "asc" unless %w(asc desc).include?(order_by_direction.to_s.downcase)
    orgs.order(Arel.sql("users.#{order_by_field} #{order_by_direction}"))
  end

  # Public: Rename the URL slug of the business. If the URL slug is renamed
  # successfully and the new slug is different to the old slug, the
  # business.rename event is instrumented.
  #
  # slug - The String to use as the new URL slug.
  # actor - The User that is the actor renaming the URL slug.
  #
  # Returns a Boolean indicating whether the URL slug was renamed successfully.
  def rename_slug(slug, actor:)
    slug_was = self.slug.dup
    updated = update slug: slug
    if updated && slug_was != slug
      instrument :rename_slug, \
        slug_was: slug_was,
        slug: slug,
        actor: actor
    end
    updated
  end

  # Helper for customer.billing_end_date to return it as a Date in UTC.
  #
  # Handles a nil customer; this might come up in a single business environment
  # case, where there is no associated customer record.
  #
  # Returns a Date or nil.
  def billing_term_ends_on
    if customer&.billing_end_date
      customer.billing_end_date.utc.to_date
    else
      # N.B. we'd like to deprecate this column but until requirements around
      # billing information for the single business is clearer, we're allowing
      # this fallback.
      billing_term_ends_at&.to_date
    end
  end

  def billing_term_ends_at=(value)
    date =
      if value.present? && valid_date?(value)
        value.to_date.to_time(:utc)
      else
        nil
      end

    super(date)

    if customer
      # N.B. this should exist unless we're in a single business environment.
      # until the requirements around the single business and certain billing
      # fields is clearer, we're dual writing this to the business's
      # billing_term_ends_at column as well.
      customer.billing_end_date = date
    end
  end

  def event_prefix
    :business
  end

  def event_context(prefix: event_prefix)
    {
      prefix => slug,
      :"#{prefix}_id" => id,
    }
  end

  def event_payload
    {
      business: self,
      name: name,
    }
  end

  # Public: Allows bulk setting of the initial owners. Only allowed when
  # initially creating the business.
  #
  # Since owners are set via Abilities, actual records will be inserted
  # after create.
  #
  # owners  - An Array of Users to grant admin priviledges to.
  #
  # Returns nothing.
  def owners=(owners)
    if persisted?
      raise "Bulk owner setter only available on new records. Use add_owner or remove_owner instead."
    else
      @owners = owners
    end
  end

  # Public: The users who are owners of this business.
  #
  # For new records, returns the users who will be granted admin rights once
  # the business is created.
  #
  # Returns an Array of Users.
  def owners
    return @owners if defined?(@owners)

    members(action: :admin)
  end

  # Public: Does the given user have owner privileges on this Business?
  #
  # user  - The User to check.
  #
  # Returns a Boolean.
  def owner?(user)
    permit?(user, :admin)
  end

  # Public: Find an unaccepted admin invitation for the user/email.
  #
  # invitee - User to find an invitation for.
  # email   - optional — String email to find an invitation for.
  # role    - optional — a String specifying the role of the admin being invited
  #
  # Returns a BusinessMemberInvitation, or nil when not found.
  def pending_admin_invitation_for(invitee = nil, email: nil, role: nil)
    return if !invitee.present? && !email.present?
    return if invitee.present? && !invitee.is_a?(User)
    return if email.present? && !User.valid_email?(email)

    scope = invitations.pending.with_invitee_or_normalized_email(invitee: invitee, emails: email)
    scope = scope.with_business_role(role) if role.present?
    scope.last
  end

  # Public: Find an unaccepted organization invitation for the organization
  #
  # invitee - Organization to find an invitation for.
  #
  # Returns a BusinessOrganizationInvitation, or nil when not found.
  def pending_organization_invitation_for(invitee = nil)
    return if !invitee.present? || !invitee.is_a?(Organization)

    organization_invitations.pending.find_by(invitee: invitee)
  end

  # Public: Invite a user or email to become an admin of this business.
  # Provided either user or email, not both. An invitation can only be made to
  # either a user or an email address.
  #
  # user    - A User to invite to become a business admin
  # email   - A String email address to invite to become a business admin.
  # inviter - User sending the invitation.
  # role    - String role of the admin being invited
  # stafftools_invite - flag that signifies the invitation is coming from stafftools
  #                     (relaxes the validations by allowing a business owner or a
  #                     site admin to issue an invitation)
  #
  # Returns a BusinessMemberInvitation.
  def invite_admin(user: nil, email: nil, role:, inviter:, stafftools_invite: false)
    pending_admin_invitation_for(user, email: email, role: role) ||
      invitations.create!(invitee: user, email: email, inviter: inviter,
                          role: role, stafftools_invite: stafftools_invite)
  end

  # Public: Grants a user admin privilege on this business.
  #
  # user  - The User to grant privilege to.
  # actor - The User granting the privilege.
  # send_email_notification - Boolean indicating whether the admin is notified
  #   by email. Currently only passed as true when staff directly add admins and
  #   the invitation flow is bypassed.
  #
  # Returns nothing.
  def add_owner(user, actor:, send_email_notification: false)
    raise ArgumentError, "admin must be a User" unless user.try(:user?)
    if two_factor_requirement_enabled? && !user.two_factor_authentication_enabled?
      raise UserHasTwoFactorDisabledError, "User must have two-factor authentication enabled"
    end
    return true if owner?(user)

    grant user, :admin
    add_user_accounts([user.id]) unless GitHub.single_business_environment?
    instrument :add_admin, user: user, actor: actor
    sync_global_business_owner_and_site_admin(user)
    send_admin_added_email_notification(role: :owner, admin: user) if send_email_notification
  end

  # Public: Revokes a user's admin privilege on this business.
  #
  # user    - The User to revoke the privilege from.
  # actor   - The User revoking the privilege.
  # reason  - (Optional) A String stating why the privilege was revoked.
  # send_notification - Whether or not the user should be sent a notification
  #                     about being removed from the business.
  #
  # Returns nothing.
  def remove_owner(user, actor:, reason: nil, send_notification: true)
    return unless owner?(user)
    block_removal_of_last_owner user
    cancel_all_invitations_involving(user)
    revoke user
    remove_user_from_business(user)
    instrument :remove_admin, user: user, actor: actor, reason: reason
    sync_global_business_owner_and_site_admin(user)

    send_admin_removed_email_notification(role: :owner, admin: user, reason: reason&.to_s) if send_notification
  end

  # Public: Cancel all invitations involving the given user, including:
  # - Any pending administrator invitations from and to the user
  # - Any pending organization invitations from the user
  #
  # user - The User for whom all invitations should be cancelled.
  #
  # Returns nothing.
  def cancel_all_invitations_involving(user)
    # Invitations don't exist in single enterprise account env (GHES)
    return if GitHub.bypass_business_member_invites_enabled?

    cancel_admin_invitations_involving(user)
    cancel_organization_invitations_from(user)
  end

  # Internal: Cancel all pending administrator invitations where the
  # inviter or invitee is the given user.
  #
  # user - The User who is the inviter or invitee.
  #
  # Returns nothing.
  def cancel_admin_invitations_involving(user)
    invitations.pending.where(
      "inviter_id = ? OR invitee_id = ?", user.id, user.id
    ).each do |invitation|
      invitation.cancel actor: user
    end
  end

  # Internal: Cancel all pending organization invitations where the
  # inviter is the given user.
  #
  # user - The User who is the inviter for whom invitations should be cancelled.
  #
  # Returns nothing.
  def cancel_organization_invitations_from(user)
    organization_invitations.pending.where(inviter_id: user.id).each do |invitation|
      invitation.cancel user
    end
  end

  # Public: Removes a user from this Enterprise
  #
  # user    - The User to remove
  # actor   - The User removing the User
  # reason  - (Optional) A String indicating why the user was removed
  # send_notification - Whether or not the user should be sent a notification
  #                     about being removed from the Enterprise.
  #
  # Returns nothing.
  def remove_member(user, actor:, reason: nil, send_notification: true)
    if GitHub.single_business_environment?
      raise ForbiddenRemovalError.new("Users cannot be removed from the global enterprise in this environment")
    end

    unless owner?(actor)
      raise ForbiddenRemovalError.new("#{actor} does not have permission to remove a user from this enterprise")
    end

    # direct/indirect member of the enterprise, outside collaborator, or an org billing manager on any org
    unless user_connected_to_enterprise?(user)
      raise InvalidRemovalError.new("User #{user.login} doesn't belong to #{name}")
    end

    if owners == [user]
      raise ForbiddenRemovalError.new(
        "User #{user.login} is the last admin in the #{name} enterprise",
      )
    end

    solely_administered_orgs = organizations.select { |org| org.last_admin?(user) }
    if solely_administered_orgs.any?
      raise ForbiddenRemovalError.new(
        "User #{user.login} is the last admin in these organizations: #{solely_administered_orgs.map(&:name).join(', ')}",
      )
    end

    roles_lost = Set.new
    organizations_removed_from = Set.new
    organizations.each do |organization|
      # remove direct or pending member organization members
      if organization.member?(user) || organization.pending_members.include?(user)
        roles_lost.add :member
        organizations_removed_from.add organization
        organization.remove_member(user, send_notification: false, background_team_remove_member: true)
      end
      # remove access to repos they're outside collaborators on
      if organization.user_is_outside_collaborator?(user.id)
        roles_lost.add :outside_collaborator
        organizations_removed_from.add organization
        organization.remove_outside_collaborator!(user, send_notification: false)
      end
      # remove pending outside collaborator invitations
      if organization.repository_invitations.where(invitee: user).exists?
        roles_lost.add :outside_collaborator
        repository_ids = organization.repositories.pluck(:id)
        RepositoryInvitation.cancel_all_invitations_involving(user: user, repo_ids: repository_ids)
      end
      # remove organization billing managers
      if organization.billing.manager?(user)
        roles_lost.add :billing_manager
        organization.billing.remove_manager(user, actor: actor)
      end
    end

    remove_support_entitlee(user, actor: actor)

    # Remove direct enterprise membership a.k.a. owners/billing managers
    if owner?(user) || pending_admin_invitation_for(user, role: "owner")
      roles_lost.add(:owner)
      if owner?(user)
        remove_owner(user, actor: actor, send_notification: false)
      else
        pending_admin_invitation_for(user, role: "owner")&.cancel(actor: user)
      end
    end

    if billing_manager?(user) || pending_admin_invitation_for(user, role: "billing_manager")
      roles_lost.add(:billing_manager)
      if billing_manager?(user)
        billing.remove_manager(user, actor: actor, send_notification: false)
      else
        pending_admin_invitation_for(user, role: "billing_manager")&.cancel(actor: user)
      end
    end

    send_member_removed_email_notification(user, roles_lost.to_a, organizations_removed_from.to_a) if send_notification
    remove_user_from_business(user)
    instrument :remove_member, user: user, actor: actor, reason: reason
  end

  # Public: Changes the administrator's role on this business
  #
  # admin_user    - The admin whose role is changing.
  # new_role      - The new role for the admin (:owner or :billing_manager).
  # actor         - The User making the change.
  # send_notification - Whether or not the user should be sent a notification
  #                     about their role changing.
  #
  # Returns nothing.
  def change_admin_role(admin_user, new_role:, actor:, send_notification: true)
    new_role = new_role&.to_sym
    unless [:owner, :billing_manager].include?(new_role)
      raise ArgumentError, "new role must be owner or billing_manager"
    end

    case new_role
    when :owner
      return if owner?(admin_user)
      unless billing_manager?(admin_user)
        raise UserNotAnAdminError, "User must already be an Enterprise administrator"
      end

      billing.remove_manager(admin_user, actor: actor, send_notification: false)
      add_owner(admin_user, actor: actor, send_email_notification: false)
    when :billing_manager
      return if billing_manager?(admin_user)
      unless owner?(admin_user)
        raise UserNotAnAdminError, "User must already be an Enterprise administrator"
      end

      remove_owner(admin_user, actor: actor, send_notification: false)
      billing.add_manager(admin_user, actor: actor, send_notification: false)
    end

    send_admin_role_changed_notification(new_role: new_role, admin: admin_user) if send_notification
  end

  # Create BusinessUserAccounts in this Business for the given list of Users,
  # who do not already have a BusinessUserAccount.
  # If we find a BusinessUserAccount corresponding to any of the given users'
  # [verified] email addresses, update that account to be associated with
  # the User record
  #
  # user_ids - Array of Integer representing the IDs of the Users who are
  # joining the Business.
  def add_user_accounts(user_ids)
    # remove user_ids of any Users who alreay have a BusinessUserAccount in this Business
    # that's associated with a User
    user_ids = user_ids.uniq - user_accounts.where(user_id: user_ids).pluck(:user_id)

    # Get verified email addresses for all the passed in Users and find any
    # BusinessUserAccount's that are associated with any of those emails in this
    # Business via an EnterpriseServerUserAccount only (i.e. a server-only member)
    user_emails_hash = UserEmail.verified.where(user_id: user_ids).pluck(:email, :user_id).to_h
    accounts_hash = enterprise_users_from_emails(user_emails_hash.keys)

    # For any server-only BusinessUserAccount's that we found, associate them with the User
    # instead of creating a new BusinessUserAccount
    accounts_hash.each do |email, business_user_account|
      business_user_account.update(user_id: user_emails_hash[email]) if business_user_account.present?
      user_ids.delete(user_emails_hash[email])
    end

    user_ids.each do |user_id|
      user_accounts.create(user_id: user_id)
    end
  end

  # Public - find the BusinessUserAccount for the given user in this Business
  #
  # user  -   User whose account we're looking up
  #
  # Returns: a BusinessUserAccount if one exists, nil otherwise
  def business_user_account_for(user)
    user_accounts.find_by(user_id: user.id)
  end

  # Public: Add an organization to this business.
  #
  # organization - The Organization to add.
  #
  # Returns the Business::OrganizationMembership whether valid or not.
  def add_organization(organization)
    raise ArgumentError, "organization must be an Organization" unless organization.try(:organization?)

    membership = upsert_organization_membership(organization)

    # configure tenant for organization
    if team_sync_enabled? && membership.valid?
      UpdateTeamSyncForBusinessOrganizationJob.perform_later(org_id: organization.id)
    end

    membership
  end

  # Public: Remove an organization from this business.
  #
  # organization - The Organization to remove.
  # is_transfer  - Boolean value to signify if removal was triggered by a transfer
  #
  # Returns nothing.
  def remove_organization(organization, is_transfer: false)
    if remove_one_organization(organization, is_transfer) && team_sync_enabled?
      organization.team_sync_tenant.disable
    end
  end

  # Public: Transfer an organization from this business to another business.
  #
  # organization    - The Organization to transfer
  # target_business - The Business to transfer the organization to
  #
  # Returns the Business::OrganizationMembership whether valid or not.
  def transfer_organization(organization, target_business)
    repo_ids = organization.repositories.pluck(:id)

    transaction do
      remove_organization(organization, is_transfer: true)
      InternalRepository.where(repository_id: repo_ids).update_all(business_id: target_business.id)
      target_business.add_organization(organization)
    end
  end

  private def remove_one_organization(organization, is_transfer)
    valid_transfer_environment = is_transfer || GitHub.single_business_environment?
    organizations.delete(organization)
    organization.remove_internal_repositories unless valid_transfer_environment
  end

  # Public: Returns the list of users that are members of the global business
  # in an Enterprise Server environment.
  #
  # This represents the active users on the installation that are consuming
  # license seats.
  #
  # Returns an ActiveRecord::Relation.
  def single_business_members
    return User.none unless GitHub.single_business_environment?

    User.users_consuming_seats
  end

  # Is the specified user a member of an organization owned by the enterprise
  # account?
  #
  # user - The User to check.
  #
  # Returns Boolean.
  def user_is_member_of_owned_org?(user)
    organization_member_ids(actor_ids: [user.id]).any?
  end

  # Users ID of those who are members of one or more of this business's organizations.
  #
  # action  - [optional] Symbol action (:read, :write, :admin) to limit actions
  # org_ids - [optional] Array of Integers. Only return members belonging to
  #   orgs with database IDs in the provided Array.
  # actor_ids - [optional] Array of Integers. Only return members from a given
  #   subset of Users (with database IDs in the provided Array).
  #
  # Returns an Array of User IDs (integers)
  def organization_member_ids(action: nil, org_ids: nil, actor_ids: nil)
    return [] if organizations.none?

    business_org_abilities(action: action, org_ids: org_ids, actor_ids: actor_ids).
      pluck(:actor_id)
  end

  # Users who are members of one or more of this business's organizations.
  #
  # action  - [optional] Symbol action (:read, :write, :admin) to limit actions
  # org_ids - [optional] Array of Integers. Only return members belonging to
  #   orgs with database IDs in the provided Array.
  # actor_ids - [optional] Array of Integers. Only return members from a given
  #   subset of Users (with database IDs in the provided Array).
  #
  # Returns an ActiveRecord::Relation.
  def organization_members(action: nil, org_ids: nil, actor_ids: nil)
    return User.none if organizations.none?

    User.where(id: organization_member_ids(action: action, org_ids: org_ids, actor_ids: actor_ids))
  end

  # Public: retrieve user_id's of Business owners
  #
  # Returns: an array of user_id's
  def owner_ids
    owners.pluck(:id)
  end

  # Public: retrieves [de-duplicated] user_id's for this business' members and admins; admins
  # include Business owners and billing managers, as well as member organizations' billing managers
  #
  #   Note: the following are not included: server-only users, outside collaborators,
  #   and any users who have only been invited to the business (Business admins,
  #   Organization members, Outside collaborators)
  #
  # Returns: an array of (de-duplicated) user_id's
  def admin_and_oranization_member_ids
    Set.new(owner_ids + billing_manager_ids +
      business_org_abilities(include_billing_managers: true).pluck(:actor_id)).to_a
  end

  # Business organizations that the specified user is a member of
  #
  # user    - Find organizations that this User belongs to.
  # type    - Optional Symbol action to limit access type.
  #   Valid values are: :all, :admin, :member_without_admin.
  # org_ids - Optional Array of Integers. Limit search only to orgs with
  #   database IDs in the provided Array.
  #
  # Returns an ActiveRecord::Relation.
  def organizations_for_member(user, type: :all, org_ids: nil)
    return Organization.none if organizations.none?

    action = case type
    when :all
      nil
    when :admin
      :admin
    when :member_without_admin
      :read
    else
      raise ArgumentError, "Business#organizations_for_member type must be valid"
    end

    organization_ids = business_org_abilities(action: action, org_ids: org_ids, actor_ids: [user.id]).
      pluck(:subject_id)
    Organization.where(id: organization_ids)
  end

  # Generate an Abilities ActiveRecord::Relation for the organizations and
  # members of this Business, using the specified criteria.
  #
  # action  - [optional] Symbol action (:read, :write, :admin) to limit actions
  # org_ids - [optional] Array of Integers. Limit search only to orgs with
  #   database IDs in the provided Array.
  # actor_ids - [optional] Array of Integers. Limit search only to users with
  #   database IDs in the provided Array.
  # include_billing_managers - Flag indicating whether or not we want the call to also return
  #   the id's of organizations' billing managers (false by default)
  #
  # Returns an ActiveRecord::Relation.
  def business_org_abilities(action: nil, org_ids: nil, actor_ids: nil, include_billing_managers: false)
    # Only find members in a subset of orgs if specified.
    subject_ids = if org_ids.present?
      org_ids & organization_ids
    else
      organization_ids
    end

    subject_types = ["Organization"]
    subject_types << "Organization::BillingManagement" if include_billing_managers
    criteria = {
      "actor_type"   => "User",
      "subject_id"   => subject_ids,
      "subject_type" => subject_types,
      "priority"     => Ability.priorities[:direct],
    }

    if action.present?
      raise ArgumentError, "invalid action" unless Ability.actions.include?(action)
      criteria["action"] = Ability.actions[action]
    end

    if actor_ids
      criteria["actor_id"] = actor_ids
    end

    Ability.where(criteria).distinct
  end

  # Returns a comprehensive list of users who could possibly authenticate via a Business' SAML SSO IdP including:
  #
  #   * direct Business members (#members from the Ability::Membership module, currently only Business owners)
  #   * Business billing managers
  #   * Members of any Organization in the Business
  #
  # Returns ActiveRecord::Relation of Users
  def saml_members
    # the order here is important. with `members` first we'll get errors like:
    # - Relation passed to #or must be structurally compatible. Incompatible values: [:joins, :readonly]
    organization_members.or(owners).or(billing_managers).distinct
  end

  def public_organization_member_ids
    User.publicly_belongs_to(organization_ids).pluck(:id)
  end

  # Returns whether the user is a member of the business, e.g.
  # is an admin or is a member of any business organization
  #
  # user: user to check for business membership
  #
  # Returns a Promise that resolves to true if user is a member of the business.
  def async_member?(user)
    return Promise.resolve(false) unless user.is_a?(User) && user.user?
    Promise.all([super, async_organizations]).then do |direct_member, _|
      direct_member || billing_manager?(user) || organization_member_ids(actor_ids: user.id).any?
    end
  end

  # This allows for API compatibility with Organization
  def async_business
    return Promise.resolve(self)
  end

  def outside_collaborators(on_repositories_with_visibility: [:public, :private], include_forks: true)
    user_ids = outside_collaborator_ids(on_repositories_with_visibility: on_repositories_with_visibility, include_forks: include_forks)

    return User.none if user_ids.empty?
    collabs = User.with_ids(user_ids)

    if GitHub.single_business_environment?
      collabs = collabs.where("users.suspended_at IS NULL")
    end

    collabs.distinct
  end

  def outside_collaborator_ids(on_repositories_with_visibility: [:public, :private], include_forks: true)
    visibility = on_repositories_with_visibility.map do |visibility|
      { public: 1, private: 0 }[visibility]
    end.compact

    # if both visibility options are included (public and private), then _all_
    # repos are included and repository_visibility is nil so that it isn't
    # included in the query as a filter
    repository_visibility = visibility.first if visibility.count == 1

    permission_cache_key = ["business_outside_collaborator_ids_without_join", id, \
                            on_repositories_with_visibility, \
                            (:exclude_forks unless include_forks)]

    PermissionCache.fetch permission_cache_key do
      repos_scope = Repository.active
      repos_scope = repos_scope.where(public: repository_visibility) if repository_visibility
      repos_scope = repos_scope.where(parent_id: nil) unless include_forks

      # An user is an outside collaborator for a business if they have a repo-level ability
      # for one of the business's organizations, but that user is not a member of that org.
      # A user can be a member of one of the business's orgs, but still be an outside collaborator
      # if they are invited to a single repo in a different org of the business.
      org_ids_by_repo_id = Hash[repos_scope.where(organization_id: organization_ids).pluck(:id, :organization_id)]
      repo_ids = org_ids_by_repo_id.keys

      return [] if repo_ids.empty?

      org_members = Ability.where(
        subject_id: organization_ids,
        actor_type: "User",
        subject_type: "Organization",
        priority: Ability.priorities[:direct],
      ).pluck(:actor_id, :subject_id)

      # the keys are user IDs and the values are a set of all repo IDs the user has direct access to (not through org access)
      direct_repo_access_by_user_id = Hash.new { |h, k| h[k] = Set.new }
      users_with_direct_repo_access(repo_ids).each { |actor_id, repo_id| direct_repo_access_by_user_id[actor_id] << repo_id }

      # this is a mapping of user IDs to org IDs the user is a member of, but rather just org IDs where the user has direct access to at least one of its repos
      orgs_users_are_repo_collaborators_on = Hash.new { |h, k| h[k] = Set.new }
      direct_repo_access_by_user_id.each do |user_id, repo_ids|
        repo_ids.each { |repo_id| orgs_users_are_repo_collaborators_on[user_id] << org_ids_by_repo_id[repo_id] }
      end

      # this maps user IDs to the orgs they have direct access to
      orgs_users_are_members_of = Hash.new { |h, k| h[k] = Set.new }

      org_members.each do |user_id, org_id|
        orgs_users_are_members_of[user_id] << org_id
      end

      Array.new.tap do |outside_collaborator_ids|
        direct_repo_access_by_user_id.each do |user_id, collaborator_repo_ids|
          # a user is an outside collaborator if they have abilities on at least one repo in an org they aren't a member of
          if (orgs_users_are_repo_collaborators_on[user_id] - orgs_users_are_members_of[user_id]).any?
            outside_collaborator_ids << user_id
          end
        end
      end
    end
  end

  private def users_with_direct_repo_access(repo_ids)
    repo_abilities_scope = Ability.where(
      actor_type: "User",
      subject_type: "Repository",
      priority: Ability.priorities[:direct],
    )

    Array.new.tap do |repo_abilities|
      repo_ids.each_slice(1000) do |rids|
        repo_abilities.concat repo_abilities_scope.where(
          "`abilities`.`subject_id` IN (#{rids.join(', ')})",
        ).distinct.pluck(:actor_id, :subject_id)
      end
    end
  end

  def async_outside_collaborators(**args)
    async_organizations.then { outside_collaborators(**args) }
  end

  # Public: Scope of Users associated with this business, limited by user's ability
  # to see their association with the business.
  #
  # viewer - The User viewing the business.
  # type - What types of org members do we want to see?
  #   :all                  - Show all visible business org users.
  #   :admin                - Only show users who are admins of an org.
  #   :member_without_admin - Only show users who are direct members of
  #                           the org but NOT admins.
  # ignore_visibility - A Boolean indicating whether to ignore whether or not
  #   org memberships are visible to the viewer. Defaults to false.
  # org_ids - Array of Integers. Only return members belonging to orgs with
  #   database IDs in the provided Array.
  #
  # Returns an ActiveRecord::Relation
  def visible_organization_members_for(viewer, type: :all, ignore_visibility: false, org_ids: nil)
    scope = case type
    when :all
      if GitHub.single_business_environment? && org_ids.blank?
        single_business_members
      else
        organization_members(org_ids: org_ids)
      end
    when :admin
      organization_members(action: :admin, org_ids: org_ids)
    when :member_without_admin
      organization_members(action: :read, org_ids: org_ids)
    else
      raise ArgumentError, "Business#visible_organization_members_for type must be valid"
    end

    # Bypass checking which org memberships are visible to the viewer if
    # the viewer is a business admin or ignore_visibility is true.
    bypass_visibility_check = owner?(viewer) || ignore_visibility

    unless bypass_visibility_check
      visible_member_ids = organizations.flat_map { |org| org.visible_user_ids_for(viewer) }.uniq

      scope = scope.where(id: visible_member_ids)
    end

    # In case the `ghost` user gets added to the global business, make sure they
    # are never included in business member results.
    scope = scope.where("login <> ?", GitHub.ghost_user_login)

    scope
  end

  def primary_avatar_path
    "/b/#{id}"
  end

  def avatar_editable_by?(actor)
    owner?(actor)
  end

  # Public: Instrument creating records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_creation(payload = {})
    instrument :create, payload
  end

  # Public: Instrument deleting records.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_deletion(payload = {})
    instrument :delete, payload
  end

  # Public: Instrument changes to terms of service.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_update_terms_of_service(payload = {})
    if saved_change_to_terms_of_service_type? ||
      saved_change_to_terms_of_service_notes? ||
      saved_change_to_terms_of_service_company_name?

      # Instrument a change if the ToS type, notes, or company name changes
      tos_changes = {}
      tos_changes[:terms_of_service_type] = {
        old_value: terms_of_service_type_before_last_save, new_value: terms_of_service_type
      }
      tos_changes[:terms_of_service_notes] = {
        old_value: terms_of_service_notes_before_last_save, new_value: terms_of_service_notes
      }
      tos_changes[:terms_of_service_company_name] = {
        old_value: terms_of_service_company_name_before_last_save, new_value: terms_of_service_company_name
      }

      instrument :update_terms_of_service, payload.merge!(tos_changes)
    end
  end

  # Public: Instrument an import of Enterprise Server license usage, either
  # via a manual upload or via an automatic upload with GitHub Connect.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_import_license_usage(payload = {})
    instrument :import_license_usage, payload
  end

  # Public: Instrument an external identity for a user within this Business'
  # SAML Provider getting revoked
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_external_identity_revoked(payload = {})
    instrument :revoke_external_identity, payload
  end

  # Public: Instrument a user's SAML SSO session getting revoked.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_sso_session_revoked(payload = {})
    instrument :revoke_sso_session, payload
  end

  # Public: Sync some of the values from a GitHub Enterprise license belonging
  # to the business. Currently only relevant in a single global business
  # environment (GitHub Enterprise Server / GitHub Private Instances).
  #
  # license - A GitHub::Enterprise::License belonging to this business.
  #
  # Returns the updated Business if successfully synced otherwise false.
  def sync_enterprise_license(license)
    return unless GitHub.single_business_environment?

    update \
      billing_term_ends_at: license.expire_at,
      seats: license.seats
  end

  # Public: Returns whether the business should be readable by a user
  #
  # user - user to check for visibility
  #
  # Returns true if the viewer should be able to see the business, false otherwise
  def readable_by?(user)
    return true if super

    return false if user.nil?
    return false if !user.is_a?(User) || !user.user?

    # all business members can access the business
    return true if member?(user)

    # users that have a removed member notification can see the business to
    # get access to the notification message
    return true if Business::RemovedMemberNotification.new(self, user).any?

    # users with a pending invitation can access the business
    return true if invitations.pending.where(invitee: user).exists?

    # admins for organizations with a pending invitation to join the business
    # can access the business
    return true if organization_invitations.pending.includes(:invitee).any? { |invite| invite.invitee.adminable_by?(user) }

    false
  end

  def spammy?
    false
  end

  def safe_profile_name
    name.blank? ? slug : name
  end

  def create_user_accounts_for_members
    user_ids = organization_member_ids + owners.pluck(:id) + billing_managers.pluck(:id)
    add_user_accounts(user_ids.uniq)
  end

  # Internal: performs cleanup to finish removing a user from a Business.
  # just a wrapper for remove_users_from_business (below)
  #
  # user - user to be removed from the business
  #
  # Returns nothing
  def remove_user_from_business(user)
    remove_users_from_business([user.id])
  end

  # Internal: performs cleanup for a list of users who are no longer part of the business:
  #  - deletes BusinessUserAccounts
  #  - unlinks ExternalIdentity's (SAML-provisioned ones only, keep SCIM-provisioned identities)
  #  - runs the RemoveBizUserForksJob job to clean up forks
  # (first checks if the users are still connected to the business in any way and only
  #  cleans up the ones who are not)
  #
  # user_ids - the ids of users to be removed from the business
  #
  # Returns nothing.
  def remove_users_from_business(user_ids)
    return if GitHub.single_business_environment?
    return if user_ids.empty?

    business_user_ids = organization_member_ids + owners.pluck(:id) + billing_managers.pluck(:id)
    user_ids_to_remove = user_ids - business_user_ids

    return if user_ids_to_remove.empty?

    user_accounts.remove_members(user_ids_to_remove)

    user_ids_to_remove.each do |user_id|
      RemoveBizUserForksJob.perform_later(biz_id: self.id, belonging_to_user_id: user_id)
    end

    if saml_sso_enabled?
      user_ids_to_deprovision = user_ids - admin_and_oranization_member_ids
      unless user_ids_to_deprovision.empty?
        ExternalIdentity.unlink_saml_identities(provider: saml_provider,
                                                user_ids: user_ids_to_deprovision)
      end
    end
  end

  # Check whether the Dependency Insights feature is enabled and visible
  # to the given user.
  #
  # Returns a Boolean value.
  def dependency_insights_enabled_for?(viewer)
    GitHub.dependency_graph_enabled? && owner?(viewer)
  end

  # Given a list of email addresses, look up any existing BusinessUserAccounts that
  # correspond to any of the given emails and belong to this Business.
  #
  # Note: The email addresses used as the keys in the returned Hash are downcased
  # to support case insensitive lookups by email on the Hash.
  #
  # emails - Array of String representing email addresses.
  #
  # Returns: A Hash of the form: { String => BusinessUserAccount, ... }
  #   where String is a downcased email address.
  def dotcom_users_from_emails(emails)
    user_emails = UserEmail.verified.where(email: emails).to_a
    user_ids = user_emails.map(&:user_id)
    user_accounts = self.user_accounts.where(user_id: user_ids).map { |a|
      [a.user_id, a]
    }.to_h
    user_emails.map { |email| [email.email.downcase, user_accounts[email.user_id]] }.to_h
  end

  # Given a list of email addresses, look up any EnterpriseInstallationUserAccounts
  # (representing GHES users) that correspond to any of the given emails and belong
  # to this Business, but are not yet associated with a GHEC User.
  #
  # Note: The email addresses used as the keys in the returned Hash are downcased
  # to support case insensitive lookups by email on the Hash.
  #
  # emails - Array of String representing email addresses.
  # slice_size - Integer value representing the batch size for user account ids
  # and emails when running queries.
  #
  # Returns: A Hash of the form: { String => BusinessUserAccount, ... }
  #   where String is a downcased email address.
  def enterprise_users_from_emails(emails, slice_size = 1_000)
    installation_ids = enterprise_installations.pluck(:id)
    accounts = EnterpriseInstallationUserAccount.includes(:business_user_account).
        where(enterprise_installation_id: installation_ids)
    user_accounts = accounts.map { |u|
      [u.id, u.business_user_account]
    }.to_h

    # Batch queries to cater for a very large number of user_accounts and emails
    account_emails = []
    user_accounts.keys.each_slice(slice_size) do |slice|
      emails.each_slice(slice_size) do |email_slice|
        account_emails.concat(
          EnterpriseInstallationUserAccountEmail.
            with_ids(slice, field: :enterprise_installation_user_account_id).
            with_ids(email_slice, field: :email)
        )
      end
    end

    account_emails.map { |e|
      [e.email.downcase, user_accounts[e.enterprise_installation_user_account_id]]
    }.to_h
  end

  def connected_enterprise_installations
    enterprise_installations
      .where.not(integration_id: nil)
      .order("host_name ASC")
  end

  def platform_type_name
    "Enterprise"
  end

  # Public: Send a notification to an admin after they are added.
  #
  # role - A Symbol representing the admin role. Currently either :owner or
  #   :billing_manager.
  # admin - The User that was added as an admin.
  #
  def send_admin_added_email_notification(role:, admin:)
    BusinessMailer.added_as_business_admin(self, role, admin).deliver_later
  end

  # Public: Send a notification to an admin after they were removed.
  #
  # role - A Symbol representing the admin role. Currently either :owner or
  #   :billing_manager.
  # admin - The User that was removed as an admin.
  #
  def send_admin_removed_email_notification(role:, admin:, reason: nil)
    BusinessMailer.removed_as_business_admin(self, role, admin, reason).deliver_later
  end

  # Public: Send a notification to a user after they were removed.
  #
  # user - The User that was removed from the enterprise
  # roles_lost - The list of roles the user previously had across the enterprise and its organizations
  # organizations_removed_from - the organizations the user no longer has access to
  #
  def send_member_removed_email_notification(user, roles_lost, organizations_removed_from)
    BusinessMailer.removed_as_business_member(self, user, roles_lost, organizations_removed_from).deliver_later
  end

  # Public: Send a notification to an admin that their role has changed.
  #
  # new_role - A Symbol representing the admin role. Currently either :owner
  #   or :billing_manager.
  # admin - The admin whose role has changed
  #
  def send_admin_role_changed_notification(new_role:, admin:)
    BusinessMailer.admin_role_changed(self, new_role, admin).deliver_later
  end

  # Public: Whether the Business has configured and enabled Team Sync.
  #
  # Returns a Boolean.
  def team_sync_enabled?
    !!team_sync_tenant&.enabled?
  end

  # Also implemented by Organization, this allows config modules that apply to both
  # organizations and businesses (e.g. Configurable::MembersCanCreateRepositories)
  # to call `supports_internal_repositories?` without first checking whether it's
  # an Organization or Business.
  def supports_internal_repositories?
    true
  end

  # Handles the legacy settingValue argument in the
  # UpdateEnterpriseMembersCanCreateRepositoriesSetting mutation.
  #
  # permission - Required. The settingValue field provided to the mutation.
  # actor - Required. The user performing the mutation.
  #
  # Returns an array of enabled visibilities or nil if no policy is set.
  #
  # Note that we only handle legacy settings in this deprecated field. Internal repositories are
  # turned on when unspecified to match the behavior before granular permissions shipped. Setting
  # explicit permissions that include/exclude internal repositories is only possible with the new
  # boolean members_can_create_[public|private|internal]_repositories arguments.
  def legacy_graphql_set_repo_creation_permissions(permission, actor)
    case permission
    when Platform::Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["ALL"].value
      allow_members_can_create_repositories(force: true, actor: actor)
      ["public", "private", "internal"]
    when Platform::Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["PRIVATE"].value
      disallow_members_can_create_public_repositories(force: true, actor: actor)
     ["private", "internal"]
    when Platform::Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["PUBLIC"].value
      allow_members_can_create_repositories_with_visibilities(force: true, actor: actor,
        public_visibility: true,
        private_visibility: false,
        internal_visibility: true)
    when Platform::Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["DISABLED"].value
      disallow_members_can_create_repositories(force: true, actor: actor)
      []
    when Platform::Enums::EnterpriseMembersCanCreateRepositoriesSettingValue.values["NO_POLICY"].value
      clear_members_can_create_repositories(actor: actor)
      nil
    end
  end

  def organization?
    false
  end

  def disabled?
    false
  end

  def searchable?
    !destroyed?
  end

  # Public: Get the description for the terms of service type.
  #
  # Returns String.
  def terms_of_service_type_description
    TERMS_OF_SERVICE_TYPE_DESCRIPTIONS.at(TERMS_OF_SERVICE_TYPES.index(self.terms_of_service_type))
  end

  # Public: Get the terms of service notes as HTML.
  #
  # Returns String.
  def terms_of_service_notes_html
    GitHub::HTML::DescriptionPipeline.to_html(self.terms_of_service_notes)
  end

  # Public: Returns the role attribute of an administrator for use in a message.
  #
  # role - Symbol representing the administrator role. Either :owner or
  #        :billing_manager.
  #
  # Example usage:
  #
  # "Add #{Business.admin_role_for_message(:owner)} to the enterprise."
  #
  # Returns String.
  def self.admin_role_for_message(role)
    case role
    when :owner
      "an owner"
    when :billing_manager
      "a billing manager"
    else
      "an administrator"
    end
  end

  # Public: Returns the instructions to tell owners how to manage enterprise
  # owners when owners are managed via an external authentication system in the
  # GHES environment.
  #
  # operation - Symbol representing the operation for which the instructions are
  #   relevant. Must be either `:add` or `:remove`.
  #
  # Returns String.
  def self.external_auth_system_owner_instructions(operation:)
    unless [:add, :remove].include?(operation)
      raise ArgumentError, "invalid operation: #{operation}"
    end

    auth_system_instructions = if GitHub.auth.ldap?
      if operation == :add
        "adding them to the LDAP admin group '#{GitHub.ldap_admin_group}'"
      elsif operation == :remove
        "removing them from the LDAP admin group '#{GitHub.ldap_admin_group}'"
      end
    elsif GitHub.auth.saml?
      if operation == :add
        "setting their SAML 'administrator' attribute to 'true'"
      elsif operation == :remove
        "removing their SAML 'administrator' attribute"
      end
    end

    <<~MSG.squish
      Owners for the enterprise are managed via #{GitHub.auth.name}.
      #{operation.to_s.titleize} owners by #{auth_system_instructions}.
    MSG
  end

  private

  # Private: Generates a unique URL-friendly slug for this business from its name.
  # If the slug is already taken a numeric suffix will be appended (i.e. "-2")
  # until a unique slug is found.
  #
  # Returns the String slug that was set.
  def generate_slug
    return unless name.present?

    self.slug ||= unique_slug(name.parameterize.presence || GitHub.default_business_base_slug)
  end

  def unique_slug(base, attempt = 1)
    candidate = if attempt == 1
      base
    else
      suffix = "-#{attempt}"
      prefix = base.first(MAX_SLUG_LENGTH - suffix.length)
      prefix + suffix
    end

    if Business.where(slug: candidate).exists?
      unique_slug(base, attempt + 1)
    else
      candidate
    end
  end

  def grant_initial_owners
    return unless defined?(@owners)
    initial_admins = remove_instance_variable(:@owners)

    initial_admins.each { |owner| grant owner, :admin }
  end

  def ensure_initial_owners_are_all_users
    return unless defined?(@owners)
    return if @owners.all? { |owner| owner.try(:user?) }

    errors.add(:owners, "must all be users")
  end

  def ensure_enough_seats
    minimum_seats = consumed_licenses
    return if licenses >= minimum_seats

    errors.add(:seats, "must be equal to or greater than the number of existing billable seats " \
                       "(#{minimum_seats} unique billable #{"seat".pluralize(minimum_seats)})")
  end

  def ensure_website_url_is_valid
    return unless website_url?

    errors.add(:website_url, "is not a valid http(s) URL") unless valid_url?(website_url)
  end

  def valid_url?(test)
    uri = Addressable::URI.parse(test)
    uri = Addressable::URI.parse("http://#{test}") if uri.scheme.blank?

    return false unless uri.scheme =~ /https?/
    return false unless UrlHelper.valid_host?(uri.host)

    true
  rescue Addressable::URI::InvalidURIError => message
    return false
  end

  def block_removal_of_last_owner(user)
    error_message = "Last owner cannot be removed"
    raise Business::NoAdminsError.new(error_message) if owners == [user]
  end

  def set_default_terms_of_service_company_name
    self.terms_of_service_company_name = self.name unless self.terms_of_service_company_name.present?
  end

  def valid_date?(text)
    !!Date.parse(text.to_s)
  rescue ArgumentError
    false
  end

  def ensure_single_global_business
    # A single global business already exists before the current business would
    # be created. Don't allow an additional business to be created.
    if Business.count > 0
      raise SingleGlobalBusinessError, "Only a single global business may exist in this environment"
    end
  end

  # Sync the global business admin status of the user with the site admin
  # status.
  def sync_global_business_owner_and_site_admin(user)
    return unless GitHub.single_business_environment?

    if owner?(user)
      user.grant_site_admin_access "Promoted as admin of single global business" unless user.site_admin?
    else
      user.revoke_privileged_access "Demoted from admin of single global business" if user.site_admin?
    end
  end

  def upsert_organization_membership(organization)
    organization_membership = retry_on_find_or_create_error do
      organization_memberships.find_by(organization: organization) ||
        organization_memberships.create(organization: organization)
    end
    organizations.reload
    organization_membership
  end

  # Private: returns true if the user has any existing/pending connections to the enterprise account
  def user_connected_to_enterprise?(user)
    member?(user) || # direct/indirect member
      outside_collaborators.include?(user) ||
      # org billing manager or pending org member
      organizations.any? { |org| org.billing.member?(user) || org.pending_members.include?(user) } ||
      pending_admin_invitation_for(user, role: "owner") ||
      pending_admin_invitation_for(user, role: "billing_manager")
  end

  def synchronize_search_index
    if searchable?
      Search.add_to_search_index("enterprise", id)
    else
      RemoveFromSearchIndexJob.perform_later("enterprise", id)
    end
  end
end
