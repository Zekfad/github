# frozen_string_literal: true

class IssueTemplates
  class UpdateResult
    attr_reader :error, :pull_request, :hook_output

    def initialize(ok, error, pull_request, hook_output = nil)
      @ok, @error, @pull_request, @hook_output = ok, error, pull_request, hook_output
    end

    def ok?
      @ok
    end
  end

  attr_reader :repository

  LEGACY_TEMPLATE_PATH_REGEX = /\A(\.github\/)?issue(_|-)template\.md\z/i

  def self.valid_path?(string)
    string.starts_with?(".github/ISSUE_TEMPLATE") && File.extname(string) =~ /\A\.md\z/i
  end

  def self.valid_legacy_template_path?(path_string)
    path_string =~ LEGACY_TEMPLATE_PATH_REGEX
  end

  def self.template_directory
    ".github/ISSUE_TEMPLATE"
  end

  def initialize(repository)
    @repository = repository
  end

  def update(templates, branch:, updater:, commit_title:, commit_body:, reflog_data:)
    branch = Git::Ref.normalize(branch)
    commit_oid = repository.ref_to_sha(repository.default_branch)
    ref = repository.refs["refs/heads/#{branch}"]
    is_creating_pr = false

    commit_message = [commit_title, commit_body].reject(&:blank?).join("\n\n")
    commit = repository.commits.create({message: commit_message, author: updater}, commit_oid) do |files|
      templates_by_filename.each_key do |filename|
        files.remove File.join(IssueTemplates.template_directory, filename)
      end
      templates.each do |hash|
        hash.each do |filename, body|
          files.add File.join(IssueTemplates.template_directory, filename), body.gsub(/\r\n/, "\n")
        end
      end
    end

    if commit.diff.empty?
      return UpdateResult.new(false, "The commit cannot be empty", nil, nil)
    end

    if ref.nil?
      begin
        ref = repository.refs.create("refs/heads/#{branch}",
                                     commit_oid.presence || commit.oid,
                                     updater,
                                     reflog_data: reflog_data,
                                    )
        is_creating_pr = ref.name != repository.default_branch
      rescue Git::Ref::ExistsError
        return UpdateResult.new(false, "That branch already exists", nil, nil)
      rescue Git::Ref::InvalidName, Git::Ref::UpdateFailed
        return UpdateResult.new(false, "That branch name is invalid", nil, nil)
      rescue Git::Ref::HookFailed => e
        return UpdateResult.new(false, "That branch could not be created.", nil, e.message)
      end
    end

    begin
      ref.update(commit, updater, reflog_data: reflog_data)

      if is_creating_pr
        pull_request = PullRequest.create_for(repository, {
          base: repository.default_branch,
          head: branch,
          title: commit_title,
          body: commit_body,
          user: updater,
        })

        unless pull_request.valid?
          return UpdateResult.new(false, "Pull request could not be created", pull_request, nil)
        end
      end
    rescue Git::Ref::ProtectedBranchUpdateError
      return UpdateResult.new(false, "That branch is protected", nil, nil)
    rescue Git::Ref::HookFailed => e
      return UpdateResult.new(false, "Issue Templates could not be created.", nil, e.message)
    end

    UpdateResult.new(true, nil, pull_request, nil)
  end

  def [](name)
    templates_by_filename.reverse_merge(legacy_templates_by_filename)[name]
  end

  def find_by_name(name) # rubocop:disable GitHub/FindByDef
    templates_with_legacy.find { |template| template.name == name }
  end

  def any?
    valid_templates.any?
  end

  def valid_templates
    @valid_templates ||= templates.select(&:valid?)
  end

  def templates
    @templates ||= templates_by_filename.values
  end

  def templates_with_legacy
    @templates_with_legacy ||= templates_by_filename.values + legacy_templates_by_filename.values
    @templates_with_legacy.select(&:valid?)
  end

  def legacy_template
    @legacy_template ||= legacy_templates_by_filename.values.first
  end

  def legacy_template?
    legacy_template.present?
  end

  def legacy_template_path
    legacy_template_tree_entries.find { |tree_entry| tree_entry["path"] =~ LEGACY_TEMPLATE_PATH_REGEX }&.path
  end

  def issue_template_config
    @config ||= begin
      entry = template_tree_entries.detect do |tree_entry|
        tree_entry.name =~ /\Aconfig.yml\z/i
      end
      IssueTemplateConfig.new(repository: repository, data: entry&.data)
    end
  end

  private

  def templates_by_filename
    @templates_by_name ||= template_tree_entries.each_with_object({}) do |tree_entry, result|
      next unless File.extname(tree_entry.name) =~ /\A\.md\z/i
      next unless template = IssueTemplate.from_tree_entry(tree_entry)
      result[template.filename] = template
    end
  end

  def legacy_templates_by_filename
    @legacy_templates_by_name ||= legacy_template_tree_entries.each_with_object({}) do |tree_entry, result|
      next unless tree_entry.name =~ /\Aissue(_|-)template\.md\z/i
      data = tree_entry.data
      markdown = <<~MARKDOWN
        ---
        name: Default issue template
        about: default issue template
        ---
        #{data}
      MARKDOWN
      template = IssueTemplate.from_markdown(tree_entry.repository, tree_entry.name, markdown)
      result[template.filename] = template
    end
  end

  def legacy_directories
    [".github/", "."]
  end

  def template_tree_entries
    if @repository.nil?
      return []
    end

    if @repository.access.broken?
      return []
    end

    begin
      directory = @repository.directory(@repository.default_oid, IssueTemplates.template_directory)
      directory&.tree_entries || []
    rescue GitRPC::Error, Repository::CorruptionDetected
      []
    end
  end

  def legacy_template_tree_entries
    if @repository.nil?
      return []
    end

    if @repository.access.broken?
      return []
    end

    begin
      legacy_directories.map do |legacy_directory|
        directory = @repository.directory(@repository.default_oid, legacy_directory)
        directory&.tree_entries || []
      rescue GitRPC::Error, Repository::CorruptionDetected
        []
      end.flatten
    end
  end
end
