# frozen_string_literal: true

class UserRole < ApplicationRecord::Iam

  # Raised if an attempt is made to access the target association where
  # target_type is not an ActiveRecord model. This is expected when a target
  # object is managed by an external application.
  class UnknownTargetClassError < StandardError; end

  VALID_ACTOR_TYPES = %w(Team User)
  VALID_TARGET_TYPES = %w(Repository User Package Organization)

  validates :role_id,
            :actor_id,
            :actor_type,
            :target_id,
            :target_type, presence: true
  validates :actor_id, uniqueness: { scope: [:actor_type, :target_id, :target_type] }
  validates :actor_type, inclusion: { in: VALID_ACTOR_TYPES }
  validates :target_type, inclusion: { in: VALID_TARGET_TYPES }
  validate :target_plan_supports

  belongs_to :role
  belongs_to :actor, polymorphic: true
  belongs_to :target, polymorphic: true

  # Public: The name of this role.
  #
  # Returns a String.
  def action_name
    role.name
  end

  # Public: The object this role targets. Raises UnknownTargetClassError
  # if the target is not an ActiveRecord model and its lifecycle is
  # managed by an external application.
  #
  # Returns User, Organization
  def target
    target_type.constantize.find(target_id)
  rescue NameError
    raise UnknownTargetClassError, "Target type `#{target_type}` cannot be accessed as an association."
  end

  private

  def target_plan_supports
    return true unless role.billable?

    target_owner = target.is_a?(User) ? target : target.owner
    unless target_owner.plan_supports?(:fine_grained_permissions)
      errors.add(:target, "does not have the necessary plan to grant this role")
    end
  end
end
