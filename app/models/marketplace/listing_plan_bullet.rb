# frozen_string_literal: true

class Marketplace::ListingPlanBullet < ApplicationRecord::Domain::Integrations
  include GitHub::Relay::GlobalIdentification

  self.table_name = "marketplace_listing_plan_bullets"

  BULLET_LIMIT_PER_LISTING_PLAN = 4

  belongs_to :listing_plan, class_name: "Marketplace::ListingPlan",
                            foreign_key: "marketplace_listing_plan_id"

  validates :listing_plan, :value, presence: true
  validates :value, uniqueness: { scope: :marketplace_listing_plan_id, case_sensitive: true }
  validate :not_at_bullet_limit

  # Public: Returns true if this bullet can be edited by the given User.
  def allowed_to_edit?(actor)
    listing_plan && listing_plan.allowed_to_edit?(actor)
  end

  def platform_type_name
    "MarketplaceListingPlanBullet"
  end

  private

  def not_at_bullet_limit
    return unless listing_plan

    bullets = listing_plan.bullets
    bullets = bullets.where("id <> ?", id) if persisted?

    if bullets.count >= BULLET_LIMIT_PER_LISTING_PLAN
      errors.add(:listing_plan, "has reached its limit for bullet points.")
    end
  end
end
