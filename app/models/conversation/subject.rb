# rubocop:disable Style/FrozenStringLiteralComment

class Conversation
  module Subject
    def self.included(subject)
      subject.has_one :conversation, as: :subject, dependent: :destroy
      subject.after_create :setup_conversation
      subject.after_create :ensure_valid_number
    end

    # Private: create a conversation for the new subject.
    #
    # Returns the created Conversation.
    def setup_conversation
      self.conversation = Conversation.create!(subject: self)
    rescue ActiveRecord::RecordNotUnique
      self.conversation = Conversation.find_by!(subject: self)
    end

    # Private: extra protection to ensure number was set to a nonzero value.
    #
    # Returns nothing or raises ActiveRecord::Rollback if number is 0.
    def ensure_valid_number
      if number.zero?
        errors.add(:number, "is invalid")
        raise ActiveRecord::Rollback
      end
    end

    def locked?
      async_locked?.sync
    end

    def async_locked?
      async_repository.then do |repository|
        next true if repository.archived?

        async_conversation.then do |conversation|
          next false unless conversation

          conversation.state == "locked"
        end
      end
    end
  end
end
