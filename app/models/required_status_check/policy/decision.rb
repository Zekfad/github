# frozen_string_literal: true

class RequiredStatusCheck < ApplicationRecord::Domain::Repositories
  class Policy
    class Decision
      attr_reader :ref_update, :policy_fulfilled, :reason, :instrumentation_payload

      def initialize(ref_update, policy_fulfilled, reason: nil, instrumentation_payload: {})
        @ref_update = ref_update
        @policy_fulfilled = policy_fulfilled
        @reason = ProtectedBranchPolicy::Reason.new(**reason) if reason
        @instrumentation_payload = instrumentation_payload
      end

      alias_method :policy_fulfilled?, :policy_fulfilled
    end
  end
end
