# frozen_string_literal: true

class RequiredStatusCheck < ApplicationRecord::Domain::Repositories
  class Policy
    class SingleContextPolicy
      # Public: Check the context policy for a single ref update
      #
      # repository            - The Repository, Gist, or Unsullied::Wiki whose refs
      #                         are being updated
      # ref_update            - Git::Ref::Update instance
      # status_check          - RequiredStatusCheck instance
      # strict                - Boolean indicating to use strict or loose policy
      #
      # Returns RequiredStatusCheck::Policy::SingleContextPolicy::Decision
      def self.check_one(repository:, ref_update:, status_check:, strict:)
        new(repository, ref_update, status_check, strict).check
      end

      def initialize(repository, ref_update, status_check, strict)
        @repository = repository
        @ref_update = ref_update
        @status_check = status_check
        @strict = strict
      end

      def check
        strict_policy_decision || loose_policy_decision || fallback_decision
      end

      private

      attr_reader :repository, :status_check, :strict, :ref_update
      alias_method :strict?, :strict

      def strict_policy_decision
        if policy_commit_status_checks.any?
          build_decision(policy_commit_status_checks)
        else
          # The policy commit has no Statuses or CheckRuns. This most commonly
          # occurs when the policy commit is a prospective merge commit for a
          # pull request: since the commit was created by github.com and isn't
          # on any branch, it is very rare for it to have any Statuses. Even
          # though the merge commit has no Statuses, we still want to allow the
          # pull request to be merged if it's up to date and its tip commit has
          # successful status checks (e.g. are passing CI). So, we look at the
          # policy commit's parents to find ones with the same tree (one of
          # which should be the tip commit of the PR) and use those commits'
          # status checks instead.
          return if same_tree_policy_parent_commits.none?

          status_checks = find_status_checks(same_tree_policy_parent_commits)
          build_decision(status_checks)
        end
      end

      def loose_policy_decision
        # Merge commits with no status checks typically occur when merging an
        # out-of-date PR via the Merge button, or when performing a merge on
        # the command line. The loose policy allows these merges to be pushed
        # if the pre-merge commits have passing status checks.
        #
        # We exclude the before commit from the list of parent commits so that
        # people can merge a green PR into a red base branch (e.g., to fix a
        # bug in the base branch).
        return if strict?

        return unless commit_trees_match?
        return unless policy_commit.merge_commit?

        relevant_parent_commits = policy_parent_commits.reject do |commit|
          commit.oid == ref_update.before_oid
        end

        parent_commits_status_checks = find_status_checks(relevant_parent_commits).group_by(&:tree_oid)
        relevant_parent_commits.each do |parent|
          status_checks = parent_commits_status_checks[parent.tree_oid] || []
          decision = build_decision(status_checks)
          return decision unless decision.policy_fulfilled?
        end

        fulfilled_decision
      end

      def policy_commit_status_checks
        return @policy_commit_status_checks if defined? @policy_commit_status_checks

        @policy_commit_status_checks = find_status_checks([policy_commit])
      end

      def find_status_checks(commits)
        shas = commits.map(&:oid)
        find_commit_statuses(shas) + find_check_runs(shas)
      end

      def find_commit_statuses(shas)
        Statuses::Service.current_for(
          repo_id: repository.id,
          contexts: relevant_contexts,
          commit_oids: shas,
        )
      end

      def find_check_runs(shas)
        CheckRun.latest_for_sha_and_event_in_repository(shas, repository.id)
          .map { |check_run| CombinedStatus::CheckRunAdapter.new(check_run) }
          .filter { |check_run| check_run.context == status_check.context }
      end

      def relevant_contexts
        # Special casing for Travis. See https://github.com/github/repos/issues/143 for details
        if status_check.context == RequiredStatusCheck::TRAVIS_SPECIAL_CASE_CONTEXT
          [
            RequiredStatusCheck::TRAVIS_PR_CONTEXT,
            RequiredStatusCheck::TRAVIS_PUSH_CONTEXT,
            RequiredStatusCheck::TRAVIS_SPECIAL_CASE_CONTEXT,
          ]
        else
          [status_check.context]
        end
      end

      def build_decision(status_checks)
        successful_status_checks, unsuccessful_status_checks = status_checks.partition { |status_check|
          StatusCheckRollup::SUCCESS_STATES.include?(status_check.state)
        }

        case
        when unsuccessful_status_checks.any?
          status_check = select_status_to_return(unsuccessful_status_checks)
          unfulfilled_decision(status_check)
        when successful_status_checks.any?
          fulfilled_decision
        else
          fallback_decision
        end
      end

      def fulfilled_decision
        SingleContextPolicy::Decision.new(policy_fulfilled: true)
      end

      def unfulfilled_decision(status_check)
        SingleContextPolicy::Decision.new(policy_fulfilled: false, status_check: status_check)
      end

      def fallback_decision
        unfulfilled_decision(status_check)
      end

      def select_status_to_return(unsuccessful_status_checks)
        unsuccessful_status_checks.first.tap do |unsuccessful_status_check|
          # Special casing for Travis. See https://github.com/github/repos/issues/143 for details
          if status_check.context == RequiredStatusCheck::TRAVIS_SPECIAL_CASE_CONTEXT && unsuccessful_status_checks.size == 2
            unsuccessful_status_check.context = RequiredStatusCheck::TRAVIS_SPECIAL_CASE_CONTEXT
          end
        end
      end

      def commit_trees_match?
        policy_commit.tree_oid == ref_update.after_commit.tree_oid
      end

      # Private: Since only Git::Branch::Updates come with a policy commit but we also handle
      # Git::Ref::Updates, we want to fall back to the after commit for the latter.
      def policy_commit
        if ref_update.respond_to?(:policy_commit)
          ref_update.policy_commit
        else
          ref_update.after_commit
        end
      end

      # Private: The policy commit's parent commits
      def policy_parent_commits
        return @policy_parent_commits if defined? @policy_parent_commits

        @policy_parent_commits = repository.commits.find(policy_commit.parent_oids)
      end

      # Private: The policy commit's parent commits that point to the same tree
      # as the policy commit does.
      #
      # We exclude the before commit from this list so that it cannot interfere in case it
      # points to the same tree.
      def same_tree_policy_parent_commits
        return @same_tree_policy_parent_commits if defined? @same_tree_policy_parent_commits

        @same_tree_policy_parent_commits = policy_parent_commits
            .select { |commit| commit.tree_oid == policy_commit.tree_oid }
            .reject { |commit| commit.oid == ref_update.before_oid }
      end
    end
  end
end
