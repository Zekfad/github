# frozen_string_literal: true

class MergeQueue < ApplicationRecord::Collab
  belongs_to :repository, required: true, inverse_of: :merge_queues
  has_many :entries,
    -> { preload(:enqueuer, :pull_request).order(created_at: :asc) },
    class_name: :MergeQueueEntry,
    dependent: :destroy,
    inverse_of: :queue
  has_many :group_entries, class_name: :MergeGroupEntry, dependent: :destroy
  has_many :groups, class_name: :MergeGroup, dependent: :destroy

  validates :branch, presence: true, uniqueness: { scope: :repository, message: "has an existing queue" }

  def entry_for(pull_request:)
    entries.where(pull_request: pull_request).first
  end

  def active_merge_group
    # Automatic grouping isn't implemented yet, so let's consider the first 3
    # `MergeQueueEntry` records to be in the active `MergeGroup`.
    @active_merge_group ||= MergeGroup.new(
      queue_entries: MergeQueue.prefill_associations(
        entries: entries.first(3),
        repository: repository,
      ),
    )
  end

  def up_next_entries
    # All the `MergeQueueEntry` records that aren't in the `active_merge_group`.
    entries.where.not(id: active_merge_group.queue_entries.map(&:id))
  end

  def failing_up_next_entries
    up_next_entries.reject { |e| e.mergeable? }
  end

  def enqueue(pull_request:, enqueuer:)
    entries.create(
      pull_request: pull_request,
      enqueuer: enqueuer,
    )
  end

  def dequeue(pull_request:, dequeuer:)
    entries.where(pull_request: pull_request).destroy_all
    pull_request.events.create(event: "removed_from_merge_queue", actor: dequeuer)
  end

  def self.prefill_associations(entries:, repository:)
    prs_map = entries.map(&:pull_request).index_by(&:id)
    prs = prs_map.values

    GitHub::PrefillAssociations.for_pulls(prs)
    GitHub::PrefillAssociations.for_pulls_protected_branches(repository, prs)
    PullRequest.attach_statuses(repository, prs)

    entries.each do |e|
      e.association(:pull_request).target = prs_map[e.pull_request_id]
    end
  end
end
