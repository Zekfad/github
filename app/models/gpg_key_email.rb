# rubocop:disable Style/FrozenStringLiteralComment

class GpgKeyEmail < ApplicationRecord::Domain::Users
  belongs_to :gpg_key
  belongs_to :user_email

  belongs_to :verified_user_email,
    -> { where(state: "verified") },
    class_name: "UserEmail",
    foreign_key: :user_email_id
end
