# frozen_string_literal: true

class RecentBranchPushes
  def self.find(repository, user, branch_name)
    new(repository, user, branch_name).find
  end

  def initialize(repository, user, branch_name)
    @repository = repository
    @user = user
    @branch_name = branch_name
  end

  def find
    earliest_allowed_date = 1.hour.ago

    return [] if repository.pushed_at < earliest_allowed_date

    pushes = Push.latest_for(repository, user, earliest_allowed_date)

    pushes.select { |push| push.ref_is_branch? && push.branch_name == branch_name }
  end

  private

  attr_reader :repository, :user, :branch_name
end
