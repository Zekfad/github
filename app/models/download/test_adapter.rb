# frozen_string_literal: true

class Download::TestAdapter < Download::S3Adapter
  class << self
    attr_accessor :deleted, :existing, :permissions_updated, :moves,
      :existing_files

    def move(old_adapter, new_adapter)
      moves << [old_adapter, new_adapter]
    end
  end

  self.deleted = Set.new
  self.existing = Set.new
  self.permissions_updated = Set.new
  self.moves = []
  self.existing_files = {}

  def self.clear
    @deleted.clear
    @existing.clear
    @existing_files = {}
    @permissions_updated.clear
    @moves.clear
    self
  end

  def self.stubbed_file(download, options = {})
    @existing << download
    @existing_files[download] = OpenStruct.new(options)
  end

  def find_file
    self.class.existing_files[@download]
  end

  def file_exists?
    self.class.existing.include? @download
  end

  def delete_file
    self.class.existing.delete @download
    self.class.deleted << @download
  end

  def update_permissions
    self.class.permissions_updated << @download
    self.class.existing << @download
  end
end
