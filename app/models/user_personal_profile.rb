# frozen_string_literal: true

# A user personal profile for financial transactions
class UserPersonalProfile < ApplicationRecord::Collab
  belongs_to :user

  validates :user_id, presence: true, uniqueness: true
  validates :first_name, presence: true
  validates :address1, presence: true
  validates :city, presence: true
  validates :country_code, presence: true

  def fullname
    "#{first_name}" + " #{middle_name}".rstrip + " #{last_name}".rstrip
  end

  def country
    country_info = Braintree::Address::CountryNames.find do |_, alpha2, _, _|
      alpha2 == country_code
    end
    TradeControls::Country.from_braintree(country_info)
  end
end
