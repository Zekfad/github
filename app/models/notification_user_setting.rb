# frozen_string_literal: true

# The model being Newsies::SettingsStore
class NotificationUserSetting < ApplicationRecord::Domain::Notifications

  belongs_to :user

  # Store a class level reference to the handler for use in the
  # `set_for_user` method
  def self.coder_handler
    @coder_handler ||= Coders::Handler.new(Coders::NotificationUserSettingCoder)
  end

  # For backwards compatibility until we can rename the `auto_subscribe` db column
  # to `auto_subscribe_repositories`
  def auto_subscribe_repositories=(value)
    self[:auto_subscribe] = value
  end

  # For backwards compatibility until we can rename the `auto_subscribe` db column
  # to `auto_subscribe_repositories`
  def auto_subscribe_repositories
    self[:auto_subscribe]
  end

  # For backwards compatibility until we can rename the `auto_subscribe` db column
  # to `auto_subscribe_repositories`
  def auto_subscribe_repositories?
    !!self[:auto_subscribe]
  end

  serialize :raw_data, coder_handler
  delegate *Coders::NotificationUserSettingCoder.members, to: :raw_data

  def self.set_for_user(user, settings)
    serializable_hash = Newsies::SettingsSerializer.dump(settings)

    ActiveRecord::Base.connected_to(role: :writing) do
      sql = github_sql.new(
        id: user.is_a?(User) ? user.id : user.to_i,
        raw: GitHub::SQL::BINARY(coder_handler.dump(serializable_hash)),
        auto_subscribe_repositories: serializable_hash[:auto_subscribe_repositories],
        auto_subscribe_teams: serializable_hash[:auto_subscribe_teams],
        notify_own_via_email: serializable_hash[:notify_own_via_email],
        participating_web: serializable_hash[:participating_web],
        participating_email: serializable_hash[:participating_email],
        subscribed_web: serializable_hash[:subscribed_web],
        subscribed_email: serializable_hash[:subscribed_email],
        notify_comment_email: serializable_hash[:notify_comment_email],
        notify_pull_request_review_email: serializable_hash[:notify_pull_request_review_email],
        notify_pull_request_push_email: serializable_hash[:notify_pull_request_push_email],
        vulnerability_ui_alert: serializable_hash[:vulnerability_ui_alert],
        vulnerability_cli: serializable_hash[:vulnerability_cli],
        vulnerability_web: serializable_hash[:vulnerability_web],
        vulnerability_email: serializable_hash[:vulnerability_email],
        continuous_integration_web: serializable_hash[:continuous_integration_web],
        continuous_integration_email: serializable_hash[:continuous_integration_email],
        continuous_integration_failures_only: serializable_hash[:continuous_integration_failures_only],
        direct_mention_mobile_push: serializable_hash[:direct_mention_mobile_push],
      )

      sql.run %Q(
        INSERT INTO notification_user_settings
          (
            id,
            raw_data,
            auto_subscribe,
            auto_subscribe_teams,
            notify_own_via_email,
            participating_web,
            participating_email,
            subscribed_web,
            subscribed_email,
            notify_comment_email,
            notify_pull_request_review_email,
            notify_pull_request_push_email,
            vulnerability_ui_alert,
            vulnerability_cli,
            vulnerability_web,
            vulnerability_email,
            continuous_integration_web,
            continuous_integration_email,
            continuous_integration_failures_only,
            direct_mention_mobile_push
          )
        VALUES
          (
            :id,
            :raw,
            :auto_subscribe_repositories,
            :auto_subscribe_teams,
            :notify_own_via_email,
            :participating_web,
            :participating_email,
            :subscribed_web,
            :subscribed_email,
            :notify_comment_email,
            :notify_pull_request_review_email,
            :notify_pull_request_push_email,
            :vulnerability_ui_alert,
            :vulnerability_cli,
            :vulnerability_web,
            :vulnerability_email,
            :continuous_integration_web,
            :continuous_integration_email,
            :continuous_integration_failures_only,
            :direct_mention_mobile_push
          )
        ON DUPLICATE KEY UPDATE
          raw_data = :raw,
          auto_subscribe = :auto_subscribe_repositories,
          auto_subscribe_teams = :auto_subscribe_teams,
          notify_own_via_email = :notify_own_via_email,
          participating_web = :participating_web,
          participating_email = :participating_email,
          subscribed_web = :subscribed_web,
          subscribed_email = :subscribed_email,
          notify_comment_email = :notify_comment_email,
          notify_pull_request_review_email = :notify_pull_request_review_email,
          notify_pull_request_push_email = :notify_pull_request_push_email,
          vulnerability_ui_alert = :vulnerability_ui_alert,
          vulnerability_cli = :vulnerability_cli,
          vulnerability_web = :vulnerability_web,
          vulnerability_email = :vulnerability_email,
          continuous_integration_web = :continuous_integration_web,
          continuous_integration_email = :continuous_integration_email,
          continuous_integration_failures_only = :continuous_integration_failures_only,
          direct_mention_mobile_push = :direct_mention_mobile_push
      )
    end
  end
end
