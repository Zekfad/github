# frozen_string_literal: true

class EnterpriseInstallationUserAccountsImporter
  attr_reader :business, :installation, :upload_id, :actor

  # Create a new EnterpriseInstallationUserAccountsImporter in preparation for
  # importing and syncing a user accounts upload.
  #
  # business - The Business that owns the Enterprise Server installation.
  # installation - An optional existing EnterpriseInstallation for the sync.
  # upload_id - An Integer representing the ID of an
  # EnterpriseInstallationUserAccountsUpload pointing to a file containing the
  # data to be synced.
  # actor - The User performing the sync.
  def initialize(business:, installation: nil, upload_id:, actor:)
    @business = business
    @installation = installation
    @upload_id = upload_id
    @actor = actor
  end

  # Public: Parses a JSON String of user accounts data sourced from an
  # Enterprise Server installation into a Hash ready to be synchronized.
  #
  # accounts_data - A JSON string representing the user accounts data sourced
  # from an Enterprise Server installation.
  #
  # Returns as Hash.
  def self.parse_user_accounts_data(accounts_data)
    raise ArgumentError, "accounts_data cannot be nil" if accounts_data.nil?

    begin
      result = JSON.parse accounts_data, symbolize_names: true
      if result[:users].instance_of?(String) && result[:instance] && server_id = result[:instance][:server_id]
        result[:users] = decrypt_data(result.delete(:users), server_id)
      end
      result
    rescue JSON::ParserError
      raise ArgumentError, "accounts_data is not valid JSON"
    end
  end

  # Private: Helper method used by parse_user_accounts_data to decrypt encrypted
  # data.
  def self.decrypt_data(data, server_id)
    if data.starts_with?("-----BEGIN PGP MESSAGE-----\n")
      JSON.parse(GitHub::Connect::Authenticator.new.decrypt_message(data, server_id), symbolize_names: true)
    else
      JSON.parse(GitHub::Connect::Authenticator.new.decrypt_message_v2(data), symbolize_names: true)
    end
  end

  # Public: Synchronize an upload of user accounts for an Enterprise Server
  # installation with the given business and installation when provided.
  #
  # Called from SyncEnterpriseServerUserAccountsJob to process an import.
  #
  # The uploaded file must be sourced from an Enterprise Server installation
  # and is expected to be in this format:
  #
  # {
  #   "version": 1,
  #   "instance": {
  #     "license": "owHsuVmu9EyaHlaydJWGrnR...",
  #     "host_name": "github.localhost",
  #     "http_only": true,
  #     "version": "unknown",
  #     "public_key": "key"
  #   },
  #   "users": [
  #     {
  #       "user_id": 12345,
  #       "created_at": "2019-03-11 21:19:53 +0000",
  #       "login": "octocat",
  #       "profile_name": "The Octocat",
  #       "site_admin": true,
  #       "emails": [
  #         {
  #           "email": "octocat@github.com",
  #           "primary": true
  #         },
  #         {
  #           "email": "another@example.com"
  #         }
  #       ]
  #     }
  #   ]
  # }
  #
  # Returns nothing.
  def synchronize_user_accounts_data!
    raise RuntimeError, "business cannot be nil" if @business.nil?
    raise RuntimeError, "upload_id cannot be nil" if @upload_id.nil?
    raise RuntimeError, "actor cannot be nil" if @actor.nil?

    @upload = EnterpriseInstallationUserAccountsUpload.find_by \
      id: @upload_id,
      business_id: @business.id

    unless @upload
      raise RuntimeError, "no upload found with ID `#{@upload_id}` belonging to business with ID `#{@business.id}`"
    end

    @accounts_data = @upload.download_and_read_file(actor: @actor)
    begin
      @accounts_hash = EnterpriseInstallationUserAccountsImporter.parse_user_accounts_data @accounts_data
    rescue ArgumentError => error
      report_failure error
      return
    end

    @installation ||= find_existing_or_create_installation
    @upload.update enterprise_installation: @installation

    begin
      ActiveRecord::Base.transaction do
        # Delete any existing accounts for the installation that don't exist
        # in @accounts_hash.
        delete_missing_accounts

        # Cache all users and business accounts for the emails in @accounts_hash
        build_email_to_user_account_caches

        # Bulk import the user accounts.
        user_accounts_affected_rows = import_user_accounts

        # Bulk import the emails.
        emails_affected_rows = import_emails

        # Delete any business user accounts that are no longer associated to any
        # server or cloud user accounts.  This bypasses any destroy hooks and runs
        # the delete as a single operation
        @business.user_accounts.orphaned.delete_all
      end

      GitHub.dogstats.histogram \
        "enterprise_installation_user_accounts.import.upload_size",
        @upload.size
      GitHub.dogstats.histogram \
        "enterprise_installation_user_accounts.import.accounts_imported",
        @accounts_hash[:users].size

      @upload.sync_success!
      @business.instrument_import_license_usage \
        actor: @actor,
        enterprise_installation: @installation
    rescue Trilogy::Error, GitHub::SQL::BadBind, ActiveRecord::ActiveRecordError => error
      report_failure error
    end
  end

  private

  # 1. Try to find an installation belonging to the business that matches the
  #    provided server_id.
  # 2. Otherwise, create a new installation for the upload.
  def find_existing_or_create_installation
    server_id = @accounts_hash[:instance][:server_id].presence
    if server_id && installation = @business.enterprise_installations.find_by(server_id: server_id)
      return installation
    end

    # Attempt to load the license from @accounts_hash[:instance][:license] and
    # read any data required for creating an EnterpriseInstallation.
    if license = load_license
      customer_name = license.company
      license_public_key = Base64.decode64(license.customer_public_key)
    else
      customer_name = @business.name
      license_public_key = ""
    end

    EnterpriseInstallation.create! \
      owner: @business,
      server_id: @accounts_hash[:instance][:server_id],
      host_name: @accounts_hash[:instance][:host_name],
      customer_name: customer_name,
      license_hash: Digest::SHA256.base64digest(@accounts_hash[:instance][:license]),
      http_only: @accounts_hash[:instance][:http_only],
      version: @accounts_hash[:instance][:version],
      license_public_key: license_public_key
  end

  def load_license
    # An authenticator also acts as a "license reader"
    authenticator = GitHub::Connect::Authenticator.new
    authenticator.load_license(Base64.decode64(@accounts_hash[:instance][:license]))
  end

  def delete_missing_accounts
    remote_user_ids = @accounts_hash[:users].map { |account| account[:user_id] }
    account_ids = @installation.user_accounts.
      where.not(remote_user_id: remote_user_ids).pluck(:id)
    return if account_ids.empty?

    EnterpriseInstallationUserAccount.throttle do
      # Delete the enterprise_installation_user_accounts records
      ApplicationRecord::Collab.github_sql.run(<<-SQL, account_ids: account_ids)
        DELETE FROM enterprise_installation_user_accounts
        WHERE id IN :account_ids
      SQL

      # Delete the associated enterprise_installation_user_account_emails records
      EnterpriseInstallationUserAccountEmail.github_sql.run(<<-SQL, account_ids: account_ids)
        DELETE FROM enterprise_installation_user_account_emails
        WHERE enterprise_installation_user_account_id IN :account_ids
      SQL
    end
  end

  # Sets two instance variables @dotcom_users_by_email and @enterprise_users_by_email
  # Each contains a hash that maps from email address to an existing
  # BusinessUserAccount
  def build_email_to_user_account_caches
    emails = @accounts_hash[:users].flat_map { |u| u[:emails] }
                                   .select { |e| e[:primary] }
                                   .map { |e| e[:email] }

    @dotcom_users_by_email = @business.dotcom_users_from_emails(emails)
    @enterprise_users_by_email = @business.enterprise_users_from_emails(emails)
  end

  # Finds cached BusinessUserAccount for email
  def find_cached_user_account(email)
    @dotcom_users_by_email[email.downcase] || @enterprise_users_by_email[email.downcase]
  end

  # Returns an existing BusinessUserAccount for each of the email addresses
  # or creates a new one if none exists
  def find_existing_or_create_user_account(user)
    email = user[:emails].find { |e| e[:primary] }
    if email
      account = find_cached_user_account(email[:email])
      return account if account
    end

    email = user[:emails].first if !email
    login = email ? email[:email] : user[:login] || "user-#{user[:user_id]}"
    business.user_accounts.create(login: login)
  end

  def import_user_accounts
    accounts_sql = EnterpriseInstallationUserAccount.github_sql.new(<<-SQL)
      INSERT INTO enterprise_installation_user_accounts (
        `enterprise_installation_id`,
        `remote_user_id`,
        `remote_created_at`,
        `login`,
        `profile_name`,
        `site_admin`,
        `business_user_account_id`,
        `created_at`,
        `updated_at`
      ) VALUES
    SQL

    @accounts_hash[:users].each_with_index do |user, index|
      last = (@accounts_hash[:users].length - 1) == index
      user_account = find_existing_or_create_user_account(user)
      values = {
          enterprise_installation_id: @installation.id,
          remote_user_id: user[:user_id],
          remote_created_at: user[:created_at]&.first(19),
          login: user[:login] || user_account.login,
          profile_name: user[:profile_name] || GitHub::SQL::NULL,
          site_admin: user[:site_admin] || false,
          business_user_account_id: user_account.id,
        }
      accounts_sql.add(<<-SQL, values)
        (
          :enterprise_installation_id,
          :remote_user_id,
          #{user[:created_at].nil? ? "NOW()" : ":remote_created_at"},
          :login,
          :profile_name,
          :site_admin,
          :business_user_account_id,
          NOW(),
          NOW()
        )#{"," unless last}
      SQL
    end

    accounts_sql.add(<<-SQL)
      ON DUPLICATE KEY UPDATE
        `remote_created_at` = VALUES(`remote_created_at`),
        `login` = VALUES(`login`),
        `profile_name` = VALUES(`profile_name`),
        `site_admin` = VALUES(`site_admin`),
        `business_user_account_id` = VALUES(`business_user_account_id`),
        `updated_at` = NOW()
    SQL

    ApplicationRecord::Collab.throttle { accounts_sql.run }
    accounts_sql.affected_rows
  end

  def import_emails
    # Map the account ids just inserted/updated to remote_user_ids so we
    # can bulk import emails.
    remote_user_id_to_account_id = \
      @installation.user_accounts.pluck(:remote_user_id, :id).to_h

    emails_sql = EnterpriseInstallationUserAccountEmail.github_sql.new(<<-SQL)
      INSERT INTO enterprise_installation_user_account_emails (
        `enterprise_installation_user_account_id`,
        `email`,
        `primary`,
        `created_at`,
        `updated_at`
      ) VALUES
    SQL

    emails = []
    @accounts_hash[:users].each do |user|
      user[:emails].each do |email|
        primary = email.has_key?(:primary) ? email[:primary] : false
        emails << {
          enterprise_installation_user_account_id: remote_user_id_to_account_id[user[:user_id]],
          email: email[:email],
          primary: primary,
        }
      end
    end

    emails.each_with_index do |email, index|
      last = (emails.length - 1) == index
      values = {
        enterprise_installation_user_account_id: email[:enterprise_installation_user_account_id],
        email: email[:email],
        primary: email[:primary],
      }
      emails_sql.add(<<-SQL, values)
        (
          :enterprise_installation_user_account_id,
          :email,
          :primary,
          NOW(),
          NOW()
        )#{"," unless last}
      SQL
    end

    emails_sql.add(<<-SQL)
      ON DUPLICATE KEY UPDATE
        `primary` = VALUES(`primary`),
        `updated_at` = NOW()
    SQL

    EnterpriseInstallationUserAccountEmail.throttle { emails_sql.run }
    emails_sql.affected_rows
  end

  def report_failure(error)
    # Update the sync state
    @upload.sync_failure!

    Failbot.report(error, app: "github")
    GitHub.dogstats.increment("enterprise_installation_user_accounts.import.error")

    # Update the job status
    job_status = SyncEnterpriseServerUserAccountsJob.status(@business, @upload.id)
    job_status.error! if job_status
  end
end
