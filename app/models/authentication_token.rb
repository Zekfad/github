# rubocop:disable Style/FrozenStringLiteralComment

# Public: An expiring, persisted token for use in making authenticated
# requests.
class AuthenticationToken < ApplicationRecord::Domain::Integrations
  # Public: The Regexp describing the format of an AuthenticationToken's string
  # token value.
  TOKEN_PATTERN_V1 = %r{
    \A             # start
    v1             # the token format version
    \.             # a period
    [a-f0-9]{40}   # the random portion of the token
    \z             # end
  }xi

  # Internal: The number of random bytes to use in order to create tokens with
  # the desired amount of entropy.
  RANDOM_BYTES = 20

  # Internal: The amount of time in seconds before the token is expired.
  EXPIRATION_WINDOW = 1.hour

  # Require token_last_eight to be a 8 hex character string
  TOKEN_LAST_EIGHT_PATTERN = /\A[a-f0-9]{8}\z/

  belongs_to :authenticatable, polymorphic: true, required: true, validate: true
  validates :authenticatable_id, presence: true

  validates_presence_of :hashed_value
  validates_length_of :hashed_value, maximum: 255

  # Public: String last eight characters of token.
  # column :token_last_eight
  validates_format_of :token_last_eight, with: TOKEN_LAST_EIGHT_PATTERN, allow_nil: true

  before_create :initialize_expires_at_timestamp
  attribute :expires_at_timestamp, :utc_timestamp

  scope :with_unhashed_token, ->(token) {
    return none if token.blank?

    hashed_value = self.hash_for(token)
    where(hashed_value: hashed_value)
  }

  scope :active, -> {
    where("authentication_tokens.expires_at_timestamp > ?", Time.zone.now.to_i)
  }

  # Public: Create an AuthenticationToken.
  #
  # authenticatable - The object that this token will belong to.
  #
  # Returns a tuple of the AuthenticationToken and the plaintext token String
  #   for use in making authenticated requests.
  def self.create_for(authenticatable, options = {})
    GitHub.tracer.with_span("AuthenticationToken.create_for") do |span|
      token_value = random_token

      last_operations = DatabaseSelector::LastOperations.from_token(token_value)

      ActiveRecord::Base.connected_to(role: :writing) do
        token_record = create!(options.merge(
          authenticatable: authenticatable,
          hashed_value: hash_for(token_value),
          token_last_eight: token_value.last(8),
        ))

        last_operations.update_last_write_timestamp
        [token_record, token_value]
      end
    end
  end

  # Public: Fetch the hash to use for server-side persistence of the given
  # token.
  #
  # token - A String.
  #
  # Returns hashed base64 String.
  def self.hash_for(token)
    Digest::SHA256.base64digest(token)
  end

  # Internal: Generate a new random token value.
  #
  # Returns a String.
  def self.random_token
    prefix = "v1"
    suffix = SecureRandom.hex(RANDOM_BYTES)

    "#{prefix}.#{suffix}"
  end

  class ExtensionResult
    def self.success; new(:success); end
    def self.failed(reason); new(:failed, reason); end

    REASON_MESSAGES = {
      end_of_life: "This access token's expires_at time cannot be extended further.",
      expires_at_too_long: "This access token's expires_at time cannot be extended by more than 60 minutes.",
      expires_at_in_past: "This access token's expires_at time cannot be in the past."
    }

    attr_reader :reason, :status

    def initialize(status, reason = nil)
      @status = status
      @reason = reason
    end

    def success?
      status == :success
    end

    def failure?
      !success?
    end

    def message
      REASON_MESSAGES.fetch(reason, "Unknown")
    end
  end

  # Internal: extend the expires_at_timestamp for an AuthenticationToken
  # record.
  #
  # record      - AuthenticationToken record to extend.
  # expires_at  - String (Timestamp) the new expires_at timestamp for the
  #               record
  #
  # Returns an ExtensionResult object.
  def self.extend_expires_at(record, expires_at)
    extended_expires_at_timestamp =
      if expires_at.present?
        Time.parse(expires_at)
      else
        EXPIRATION_WINDOW.from_now
      end

    if extended_expires_at_timestamp > (record.created_at + 24.hours)
      return ExtensionResult.failed(:end_of_life)
    elsif extended_expires_at_timestamp > EXPIRATION_WINDOW.from_now
      return ExtensionResult.failed(:expires_at_too_long)
    elsif extended_expires_at_timestamp <= Time.now
      return ExtensionResult.failed(:expires_at_in_past)
    end

    ActiveRecord::Base.connected_to(role: :writing) do
      transaction do
        record.update!(expires_at_timestamp: extended_expires_at_timestamp)
        if record.authenticatable.respond_to?(:extend_expires_at)
          record.authenticatable.extend_expires_at(extended_expires_at_timestamp)
        end
      end
    end

    ExtensionResult.success
  end

  private

  # Private: Sets the expires_at time which is used to automatically
  # invalidate tokens after a certain amount of time.
  #
  # Returns the expiration DateTime.
  def initialize_expires_at_timestamp
    self.expires_at_timestamp ||= EXPIRATION_WINDOW.from_now
  end
end
