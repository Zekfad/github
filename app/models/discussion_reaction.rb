# frozen_string_literal: true

class DiscussionReaction < ApplicationRecord::Domain::Discussions
  include GitHub::Relay::GlobalIdentification
  include Spam::Spammable

  belongs_to :user, required: true
  belongs_to :discussion, required: true
  alias_method :async_subject, :async_discussion

  setup_spammable(:user)

  validates :content, inclusion: { in: Emotion.all.map(&:content) }

  class << self
    # Public: Idempotent all-purpose interface for ensuring a discussion reaction
    # exists between users and discussions.
    # Performs permissions checks and prevents duplicates.
    #
    # user          - User: The user having this reaction
    # discussion_id - Integer: The ID of the discussion of this reaction
    # content       - String: One of the members of Emotion.all.map(&:content)
    #
    # Returns: A DiscussionReaction in the appropriate state for the discussion
    # and user.
    def react(user:, discussion_id:, content:)
      discussion = reactable_discussion_for(user: user, discussion_id: discussion_id)

      if discussion.present?
        existing_reaction = user.discussion_reactions.find_by(
          discussion: discussion,
          content: content,
        )

        if existing_reaction.present?
          RecordStatus.new([existing_reaction, :exists])
        else
          new_discussion_reaction = create(
            user: user,
            discussion: discussion,
            content: content,
          ).tap do |discussion_reaction|
            discussion.notify_socket_subscribers
            discussion.synchronize_search_index

            GitHub.dogstats.increment(
              "discussion_reaction",
              tags: ["action:create", "type:#{content}"],
            )
          end

          RecordStatus.new([new_discussion_reaction, :created])
        end
      else
        RecordStatus.new([new, :invalid])
      end
    end

    # Public: Idempotent all-purpose interface for ensuring a reaction DOES NOT
    # exist between users and discussions.
    # Performs permissions checks.
    #
    # user          - User: The user having this reaction
    # discussion_id - Integer: The ID of the discussion of this reaction
    # content       - String: One of the members of valid_content
    #
    # Returns: A DiscussionReaction in the appropriate state for the discussion
    # and user.
    def unreact(user:, discussion_id:, content:)
      discussion = reactable_discussion_for(user: user, discussion_id: discussion_id)

      if discussion.present?
        existing_reaction = user.discussion_reactions.find_by(
          discussion: discussion,
          content: content,
        )

        if existing_reaction.present?
          transaction do
            existing_reaction.destroy
            discussion.notify_socket_subscribers
            discussion.synchronize_search_index

            GitHub.dogstats.increment(
              "discussion_reaction",
              tags: ["action:destroy", "type:#{content}"],
            )
            RecordStatus.new([existing_reaction, :deleted])
          end
        else
          unpersisted_discussion_reaction = new(
            user: user,
            discussion: discussion,
            content: content,
          )
          RecordStatus.new([unpersisted_discussion_reaction, :deleted])
        end
      else
        RecordStatus.new([new, :invalid])
      end
    end

    # Public: determine whether a given discussion is "unlocked" for a user
    def discussion_unlocked?(user, discussion)
      async_discussion_unlocked?(user, discussion).sync
    end
    alias_method :subject_unlocked?, :discussion_unlocked?

    def async_discussion_unlocked?(user, discussion)
      return Promise.resolve(false) unless user

      discussion.async_locked_for?(user).then { |locked| !locked }
    end

    def async_viewer_can_react?(viewer, discussion)
      return Promise.resolve(false) unless discussion && viewer

      async_actor_can_react_to?(viewer, discussion)
    end

    private

    # Internal: Does the discussion exist and does the user have permission to
    # react to it?
    # Returns: The Discussion instance
    def reactable_discussion_for(user:, discussion_id:)
      return unless user && discussion_id

      discussion = Discussion.find_by(id: discussion_id)

      if async_viewer_can_react?(user, discussion).sync
        discussion
      end
    end

    def async_actor_can_react_to?(actor, discussion)
      return Promise.resolve(false) if actor.must_verify_email?

      async_actor_blocked?(actor, discussion).then do |blocked|
        next false if blocked

        async_readable_and_unlocked_for_actor?(actor, discussion)
      end
    end

    def async_actor_blocked?(actor, discussion)
      discussion.async_user.then do |discussion_user|
        actor.async_blocked_by?(discussion_user).then do |blocked_by_author|
          next true if blocked_by_author

          discussion.async_reaction_admin.then do |admin|
            actor.async_blocked_by?(admin)
          end
        end
      end
    end

    def async_readable_and_unlocked_for_actor?(actor, discussion)
      async_discussion_unlocked?(actor, discussion).then do |discussion_unlocked|
        next false unless discussion_unlocked

        discussion.async_readable_by?(actor)
      end
    end
  end

  def emotion
    Emotion.find(content)
  end

  class RecordStatus < SimpleDelegator
    attr_reader :status

    VALID_STATUSES = [:created, :exists, :invalid, :deleted]

    def initialize(reaction_status)
      reaction, @status = *reaction_status
      raise ArgumentError.new("Invalid status: #{@status}") unless VALID_STATUSES.include?(@status)
      super(reaction)
    end

    def created?
      status == :created
    end

    def exists?
      status == :exists
    end

    def platform_type_name
      # This object is a SimpleDelegator to an underlying DiscussionReaction
      "DiscussionReaction"
    end
  end
end
