# frozen_string_literal: true

class MemexProject < ApplicationRecord::Domain::Memexes
  include GitHub::Validations
  include GitHub::Prioritizable::Context
  extend GitHub::BackgroundDependentDeletes
  extend GitHub::Encoding

  force_utf8_encoding :title

  belongs_to :owner, polymorphic: true
  belongs_to :creator, class_name: "User"

  has_many :memex_project_items, inverse_of: :memex_project
  destroy_dependents_in_background :memex_project_items
  accepts_nested_attributes_for :memex_project_items
  prioritizes :memex_project_items, with: :memex_project_items, by: :priority, inverse_of: :memex_project

  has_many :memex_project_columns, inverse_of: :memex_project

  TITLE_BYTESIZE_LIMIT = 1024
  DEFAULT_TITLE = "Untitled table"
  COLUMN_LIMIT = 50

  before_validation :set_number, on: :create

  validates :creator, presence: true, on: :create
  validates :title, presence: true, allow_nil: true
  validates :title, bytesize: { maximum: TITLE_BYTESIZE_LIMIT }, unicode: true
  validates :owner_type, presence: true, inclusion: { in: %w[Organization User] }
  validates :owner, presence: true, on: :create
  validates :number, presence: true, numericality: { only_integer: true, greater_than: 0 }, on: :create
  validates :number, uniqueness: { scope: [:owner_type, :owner_id] }, on: :create
  validate :creator_has_verified_email, on: :create

  # Atomically creates a new memex with optional first item and first custom column associations.
  #
  # owner - Organization to which the memex will belong for purposes of authorization.
  # creator - User who is creating the new memex.
  # title - Optional String title for the new memex.
  # item_options - Hash containing either (but not both) of the following keys:
  #   draft_issue_title - String title for the draft issue that should be created as the first item
  #   of the new memex.
  #   issue_or_pull - Issue | PullRequest object that will act as the content for the first item
  #   that will be created for the new memex. It is the responsibility of the caller to have checked
  #   that user has access to the content.
  # column_options - Hash containing at least one of the following keys:
  #   name - String name of the column to create as the first custom column in this memex OR the
  #     name of a default column whose position or visiblity we want to change during the creation
  #     process.
  #   data_type - Symbol non-default type of the column to create as the first custom column in this
  #     memex. Note that if the `name` option is provided and matches a non-default column name then
  #     this argument will be ignored (data_type is immutable).
  #   position - Integer 1-based position of the new or default column that will be created with
  #     the memex.
  #   visiblity - Boolean visibility of the new or default column that will be created with the
  #     memex.
  #
  # Returns a MemexProject object that may or may not be valid or persisted.
  def self.create_with_associations(owner:, creator:, title: nil, item_options: nil, column_options: nil)
    memex = owner.memex_projects.build(title: title, creator: creator)
    item = memex.build_item(
      creator: creator,
      draft_issue_title: item_options[:draft_issue_title],
      issue_or_pull: item_options[:issue_or_pull]
    )

    MemexProject.transaction do
      success = memex.save
      success = memex.save_with_lowest_priority!(item).persisted? if success && item

      column = nil
      if success && column_options.values.any? && memex.save_default_columns!
        column = if column_options[:data_type]
          memex.add_user_defined_column(
            name: column_options[:name],
            data_type: column_options[:data_type],
            position: column_options[:position],
            creator: creator,
          )
        else
          existing_column = memex.find_column_by_name_or_id(column_options[:name])
          memex.update_column(
            existing_column,
            name: column_options[:name],
            position: column_options[:position],
            visible: column_options[:visible]
          )
          existing_column
        end
      end

      success = [memex, item, column].compact.all? { |obj| obj.valid? && obj.persisted? }
      raise ActiveRecord::Rollback unless success
    end

    memex
  end

  def build_item(creator:, draft_issue_title: nil, issue_or_pull: nil)
    if issue_or_pull
      memex_project_items.build(
        creator: creator,
        content: issue_or_pull,
        repository_id: issue_or_pull.repository_id
      )
    elsif draft_issue_title
      build_draft_issue(creator: creator, title: draft_issue_title)
    end
  end

  def build_draft_issue(creator:, title:)
    item = memex_project_items.build(creator: creator)

    issue_or_pr = DraftIssueReferenceFilter.new(text: title, viewer: creator).single_reference
    if issue_or_pr
      item.content = issue_or_pr
      item.repository_id = issue_or_pr.repository_id
    else
      item.content = item.build_draft_issue(title: title)
    end

    item
  end

  def add_user_defined_column(name:, data_type:, position:, creator:)
    new_column = build_user_defined_column(name: name, data_type: data_type, creator: creator)

    # If a desired position is provided and is not equal to the default (last) position,
    # insert the new column in the desired position. Otherwise, save the column as is.
    if position && position != new_column.position
      reload # Clear newly created column from cached list of columns.
      insert_column(new_column, position.to_i - 1)
    else
      # If the save is successful, make sure to clear the cached value of `columns`.
      new_column.save && reload
    end

    new_column
  end

  def build_user_defined_column(data_type:, creator:, name: nil)
    # The column is built with the last position by default.
    # Use MemexProject#reorder_columns to place it in the desired position within the memex.
    last_position = columns.length + 1

    memex_project_columns.build(
      name: name || suggested_user_defined_column_name,
      data_type: data_type,
      position:  last_position,
      creator: creator,
      user_defined: true,
      visible: true,
    )
  end

  def update_column(column, name: nil, position: nil, visible: nil)
    column.name = name unless name.nil?
    column.visible = visible unless visible.nil?

    if !position.nil? && column.position != position
      insert_column(columns.delete_at(column.position - 1), position.to_i - 1)
    else
      column.save
    end
  end

  def rebalance_job_class
    RebalanceMemexProjectJob
  end

  # Assigns the given item a priority such that it is the lowest priority item
  # in the memex, and then returns the newly saved item.
  #
  # Note that this can raise due to `GitHub::Prioritizable::Context::LockedForRebalance`
  # in addition to validation errors from calls to `item.save!`.
  #
  # Returns the given MemexProjectItem, which may or may not be valid/persisted.
  def save_with_lowest_priority!(item)
    last_item_scope = prioritized_memex_project_items
    last_item_scope = last_item_scope.where.not(id: item.id) if item.id
    prioritize_dependent!(item, **{after: last_item_scope.last}.compact)
  end

  # Fetch the ordered list of current columns for this memex.
  #
  # Note that this is NOT the same as the `memex_project_columns` method. That
  # method will only ever return persisted columns, whereas this method may
  # return default, unpersisted columns. Furthermore, the result of this method
  # is memoized, so the memex must be reloaded after column changes are made in
  # order for this to show accurate information.
  #
  # Returns Array<MemexProjectColumn>
  def columns
    @columns ||= begin
      non_default_columns = memex_project_columns.order(position: :asc).all.to_a
      non_default_columns.any? ? non_default_columns : MemexProjectColumn.default_columns
    end
  end

  # Retrieves any column by ID or a system-defined column by name.
  # This is useful for finding a default column before it may have been persisted.
  #
  # Returns MemexProjectColumn or nil.
  def find_column_by_name_or_id(identifier)
    return nil unless identifier
    columns.find { |c| c.id.to_s == identifier.to_s || c.system_defined? && c.name == identifier }
  end

  # Override ActiveRecord::Persistence#reload to make sure that we clear the
  # list of cached columns on reload.
  def reload(_options = nil)
    @columns = nil
    super
  end

  def visible_columns
    columns.select(&:visible?)
  end

  def save_default_columns!
    return if memex_project_columns.any?

    column_attributes = MemexProjectColumn.default_columns.each_with_index.map do |c, index|
      c.attributes.except(:id).merge(position: index + 1)
    end
    self.memex_project_columns.build(column_attributes)
    save!
  end

  # Reorder persisted columns such that they match the given column order.
  #
  # reordered_columns - Array<MemexProjectColumn>, all of which should belong to this memex and
  #   should have already been persisted. Note that any other outstanding changes on these objects
  #   will also be persisted.
  #
  # Returns Boolean for whether or not the operation succeeded.
  def reorder_columns(reordered_columns)
    # Bail early if there are other changes to a column that make it invalid.
    return false unless reordered_columns.all?(&:valid?)

    # First move everything out of the way so that we avoid collisions that
    # would upset the uniqueness constraint on `position`.
    MemexProjectColumn
      .where(id: reordered_columns.map(&:id))
      .update_all(["position = position + ?", reordered_columns.length])

    # Then update positions to what they should be. This intentionally uses `update_column` so that
    # we don't need to reload records in order to avoid ActiveRecord short-circuiting an update
    # because it matches an old value from before the `update_all` statement above.
    results = []
    reordered_columns.each_with_index do |c, index|
      success = if c.persisted?
        c.update_column(:position, index + 1)
      else
        c.position = index + 1
        c.save
      end
      results << success
    end

    results.all?
  end

  def to_hash
    {
      closed_at: closed_at&.utc&.iso8601,
      created_at: created_at.utc.iso8601,
      description: description,
      id: id,
      number: number,
      public: public,
      title: title,
      updated_at: updated_at.utc.iso8601,
    }
  end

  def closed?
    closed_at.present?
  end

  def set_number
    return if self.number
    return unless owner
    unless Sequence.exists?(owner)
      Sequence.create(owner, self.class.where(owner_type: owner_type, owner_id: owner_id).count)
    end
    self.number = Sequence.next(owner)
  end

  # Returns a suggested column name for new columns created for this memex.
  #
  # Note this method is not thread-safe, and _can_ result in duplicate suggested
  # names. See https://github.com/github/memex/issues/739 for more details
  #
  # Returns a String.
  def suggested_user_defined_column_name
    user_column_count = memex_project_columns.user_defined.count
    name = MemexProjectColumn::NEW_COLUMN_NAME.dup

    if user_column_count.zero?
      name
    else
      [name, user_column_count + 1].join(" ")
    end
  end

  def display_title
    title || DEFAULT_TITLE
  end

  private

  def creator_has_verified_email
    return unless GitHub.email_verification_enabled?
    errors.add(:creator, "must have a verified email address") unless creator&.verified_emails?
  end

  def insert_column(column, index)
    columns.insert([index, columns.length].min, column)
    reorder_columns(columns)
  end
end
