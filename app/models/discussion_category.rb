# frozen_string_literal: true

class DiscussionCategory < ApplicationRecord::Domain::Discussions
  ANNOUNCEMENTS_NAME = "Announcements"
  GENERAL_NAME = "General"
  include GitHub::Validations
  include GitHub::UTF8
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model
  include Permissions::Attributes::Wrapper

  self.permissions_wrapper_class = Permissions::Attributes::DiscussionCategory

  extend GitHub::Encoding
  force_utf8_encoding :emoji, :name, :description

  belongs_to :repository, required: true
  has_many :discussions, dependent: :nullify

  validates :name, presence: true, unicode: true
  validates :slug, uniqueness: { scope: :repository_id, case_sensitive: true, allow_nil: true }
  validates :emoji, presence: true, single_emoji: true, unicode: true,
    allowed_emoji: true
  validates :description, unicode: true

  before_validation :generate_slug, if: :name_changed?

  # We are temporarily removing the "Announcements" category until we have added
  # more in-depth permissions to control who can post to "Announcements"
  #
  # https://github.com/github/discussions/issues/561
  default_scope { where.not(name: ANNOUNCEMENTS_NAME) }

  alias_attribute :to_s, :name

  # Public: Return a list of DiscussionCategory attributes used to create
  # initial categories when discussions are first enabled on a Repository.
  #
  # An Array of DiscussionCategory attributes in Hash form.
  def self.initial_categories
    [
      {name: GENERAL_NAME, description: "Chat that doesn't fit anywhere else", emoji: ":hash:", is_default: true},
      {name: ANNOUNCEMENTS_NAME, description: "News and important updates", emoji: ":mega:"},
      {name: "Help", description: "Ask the community for help", emoji: ":pray:"},
      {name: "Ideas", description: "Share ideas for new features", emoji: ":thinking:"},
      {name: "Show and tell", description: "Show off something you've made", emoji: ":raised_hands:"},
      {name: "Thanks", description: "Share gratitude with the team", emoji: ":sparkling_heart:"},
      {name: "TIL", description: "Today I learned. What have you learned today?", emoji: ":brain:"},
    ]
  end

  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  def async_readable_by?(actor)
    async_repository.then do |repository|
      next false unless repository&.discussions_enabled?
      repository.async_readable_by?(actor)
    end
  end

  # Public: Returns HTML to render the emoji for this category.
  def emoji_html
    GitHub::Goomba::SimpleDescriptionPipeline.to_html(emoji, base_url: GitHub.url)
  end

  def repository_owner
    repository&.owner
  end

  def event_prefix() :discussion_category end
  def event_payload
    payload = {
      event_prefix => self,
      :name        => name,
      :emoji       => emoji,
      :description => description,
    }

    if repository
      payload[repository.event_prefix] = repository

      if org = repository.organization
        payload[org.event_prefix] = org
      end
    end

    payload
  end

  def <=>(other)
    # Put 'General' first, then sort alphabetically (case-insensitively)
    return -1 if general?
    return 1 if other.general?

    name.downcase <=> other.name.downcase
  end

  def general?
    name == GENERAL_NAME
  end

  # Public: Can the given actor delete the discussion?
  def deletable_by?(actor)
    # Ensure the feature flag is enabled
    return false unless repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :delete_discussion_category,
      actor: actor,
      subject: self,
    )
    response.allow?
  end

  # Public: Can the given actor change the name and emoji of this category?
  def modifiable_by?(actor)
    # Ensure the feature flag is enabled
    return false unless repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :edit_discussion_category,
      actor: actor,
      subject: self,
    )
    response.allow?
  end

  private

  def generate_slug
    # Necessary because this callback runs before the unicode: true validation
    # and .downcase throws an ArgumentError on invalid bytes
    return unless name.valid_encoding?

    self.slug = name.downcase.gsub(/[^\p{Word}]+/, "-").chomp("-")
  end
end
