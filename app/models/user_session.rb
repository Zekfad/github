# rubocop:disable Style/FrozenStringLiteralComment

require "base64"
require "digest/sha2"
require "openssl"
require "securerandom"

class UserSession < ApplicationRecord::Domain::Users
  include GitHub::FlipperActor
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  SUDO_EXPIRY = 2.hours

  # Byte size of client's private key.
  #
  # 36 bytes was arbitrarily choosen by @mastahyeti and @josh when 32 would
  # have probably done.
  KEY_SIZE = 36

  # Size of key when urlsafe Base64'd
  BASE64_KEY_SIZE = 48

  # Pattern to validate incoming Base64'd keys
  BASE64_KEY_RE = /\A[A-Za-z0-9\-_=]{#{BASE64_KEY_SIZE}}\z/

  # Use SHA256 for hashing keys
  HASHED_KEY_DIGEST = Digest::SHA256

  # Require hashed key to be a 44 char base64 string
  HASHED_KEY_RE = /\A[A-Za-z0-9+\/=]{44}\z/

  # Number of random bytes for secret token
  SECRET_BYTES = 32

  # Require secret token to be a 44 char base64 string
  SECRET_RE = /\A[A-Za-z0-9+\/=]{44}\z/

  # Digest to use for any HMAC signed by the secret
  SECRET_HMAC_DIGEST = OpenSSL::Digest::SHA1

  # Number of random bytes for CSRF token
  CSRF_BYTES = 32

  # Require CSRF token to be a 44 char base64 string
  CSRF_TOKEN_RE = /\A[A-Za-z0-9+\/=]{44}\z/

  # Window of time between bumping sudo_enabled_at.
  #
  # Returns a duration.
  SUDO_THROTTLING = 10.minutes

  # Window of time between updating changes in IP
  IP_CHANGE_THROTTLING = 5.minutes

  # How long impersonated sessions should last for.
  #
  # Since these sessions are only temporarily granted to staff to investigate
  # issues, they are short lived compared to regular sessions.
  #
  # Returns a duration.
  IMPERSONATED_EXPIRES = 1.hour

  # For how long sessions should be considered recent.
  #
  # Returns a duration.
  RECENTNESS = 24.hours

  # The number of simultaneous active user sessions a user is allowed.
  LIMIT = 100

  # The number of simultaneous active user sessions a user can have before we
  # enforce the limit.
  #
  # This threshold is distinct from the LIMIT so that we don't trigger
  # enforcement for every new session but allow several to accrue. For some
  # automated users, this is essential to not create a large volume of jobs
  # and database queries.
  LIMIT_ENFORCEMENT_THRESHOLD = 500

  # Reasons why a session is revoked.
  #
  # Returns an Array of String revoke reason identifiers.
  VALID_REVOKE_REASONS = [
    # User explicitly clicked "Sign out"
    "logout",

    # User remotely revoke one of their other sessions from the
    # "Security History" page.
    "user_remote_revoke",

    # When user changes their password, all their other active sessions are
    # revoked.
    "password_changed",

    # The user is deleting their account
    "account_destroy",

    # When staff user switches back to their normal account, the impersonated
    # session is revoked.
    "unimpersonate",

    # A bit of an edge case. If the user logins in or signs up for a new
    # account while they are already signed in as another user.
    "switched_users",

    # For special occasions, when we need to do mass revocation.
    "security_incident",

    # SAML identity provider session expired.
    "saml_expired",

    # Revoke active sessions when a User is suspended.
    "suspended",

    # Authentication changed by administrator.
    "authentication_switch",

    # Previously grouped under "password_change", this is an actual email-based reset
    "password_reset",

    # Previously grouped under "password_change", this password was randomized by staff
    "password_randomized",
  ]

  # http://heartbleed.com/
  HEARTBLEED_INCIDENT = Time.new(2014, 4, 8)

  belongs_to :user
  belongs_to :impersonator, class_name: "User"
  belongs_to :impersonator_session, class_name: "UserSession"

  has_many :external_identity_sessions, dependent: :destroy
  has_many :authentication_records

  before_validation :set_secret, :set_csrf_token, :set_impersonator,
                    :normalize_user_agent
  before_create :set_initial_accessed_at, :set_initial_sudo_enabled_at
  after_create_commit :instrument_create, :instrument_access
  after_commit :instrument_country_change, on: :update, if: :saved_change_to_ip?
  after_save :set_user_time_zone, if: :saved_change_to_time_zone_name?

  validates_presence_of :user_id
  validate :user_present, :user_is_human
  validate :impersonator_and_session_match, :impersonator_is_staff,
           :impersonator_has_sudo, :impersonator_is_not_user

  validates_presence_of :hashed_key
  validates_format_of :hashed_key, with: HASHED_KEY_RE
  validates_format_of :hashed_private_mode_key, with: HASHED_KEY_RE, allow_nil: true
  validates_format_of :secret, with: SECRET_RE
  validates_format_of :csrf_token, with: CSRF_TOKEN_RE
  validates_inclusion_of :revoked_reason, in: VALID_REVOKE_REASONS, allow_nil: true

  default_scope -> { order("accessed_at DESC") }

  # Public: Find all expired sessions.
  #
  # All these sessions are for sure inactive and can safely be pruned from the
  # database to BSS.
  #
  # Returns collection of expired records.
  scope :expired, -> { where("accessed_at < ?", user_session_expiration.ago) }

  # Public: Find all non-expired sessions.
  #
  # All these sessions are valid based on the expiration time period.
  #
  # Returns collection of non-expired records.
  scope :unexpired, -> { where("accessed_at > ?", user_session_expiration.ago) }

  scope :unrevoked, -> { where(revoked_at: nil) }

  scope :not_impersonated, -> { where(arel_table[:impersonator_session_id].eq(nil)) }

  scope :user_facing, -> { unexpired.unrevoked.not_impersonated }

  scope :recent, -> { where("accessed_at > ?", RECENTNESS.ago) }

  # Public: Generate a new random key for the client.
  #
  # Returns a base64 String.
  def self.random_key
    SecureRandom.urlsafe_base64(KEY_SIZE)
  end

  # Public: Hash client key for server side persistence.
  #
  # key - A String
  #
  # Returns hashed base64 String.
  def self.hash_key(key)
    HASHED_KEY_DIGEST.base64digest(key)
  end

  # Public: Generates new random hashed key.
  #
  # Exists mainly for debugging and tests.
  #
  # Returns random hashed base64 String.
  def self.random_hashed_key
    hash_key(random_key)
  end

  # Public: Generates a new random key pair, unhashed and hashed.
  #
  # Returns base64 String and base64 hashed version of the first String.
  def self.random_key_pair
    key = random_key
    return key, hash_key(key)
  end

  # Internal: Authenticate user via signed key.
  #
  # key   - Base64 String key.
  # block - Proc accepting a hashed key to lookup session
  #
  # Returns an active [UserSession, String] or nil.
  def self.authenticate_key(key)
    # Ensure key was unpacked as expected.
    return unless key && key.valid_encoding? && key.match(BASE64_KEY_RE)

    # Lookup session from key.
    return unless session = yield(hash_key(key))

    # Validate session.
    return unless session.active?

    return session, key
  end

  # Public: Authenticate user via unique key.
  #
  # Ensures the returned session is always active and valid. So always use
  # this method over `find_by_hashed_key` for authentication.
  #
  # key - Base64 String key ID
  # hashed_key_column - The respective hashed key column for the provided key (optional).
  #
  # Returns an active [UserSession, String] or nil.
  def self.authenticate(key, hashed_key_column: :hashed_key)
    authenticate_key(key) do |hashed_key|
      unscoped do
        user_session = where(hashed_key_column => hashed_key).first
        return nil unless user_session && user_session.user
        user_session
      end
    end
  end

  # Public: Authenticate user to access Private mode via a signed key.
  #
  # key - Base64 String key ID
  #
  # Returns an active [UserSession, String] or nil.
  def self.authenticate_private_mode(key)
    authenticate_key(key) do |hashed_key|
      unscoped do
        find_by_hashed_private_mode_key(hashed_key)
      end
    end
  end

  # Public: Find all potentially affected sessions due to a security incident.
  #
  # Excludes sessions that are already revoked.
  #
  # incident_resolved_at - Time security incident was resolved. Use a constant
  #                        like UserSession::HEARTBLEED_INCIDENT.
  #
  # Returns a scope of records.
  def self.potentially_compromised(incident_resolved_at)
    where([
      "accessed_at > ? AND revoked_at IS NULL AND created_at < ?",
      user_session_expiration.ago, incident_resolved_at
    ])
  end

  # Public: Revoke potentially affected sessions due to a security incident.
  #
  # incident_resolved_at - Time security incident was resolved. Use a constant
  #                        like UserSession::HEARTBLEED_INCIDENT.
  #
  # inactivity_duration - Duration of recent activity to exclude from
  #                       revocation. May also be set to nil to ignore
  #                       inactivity and revoke all affected sessions.
  #                       (default: 1 hour)
  #
  # Returns Integer of updated sessions.
  def self.revoke_potentially_compromised(incident_resolved_at, inactivity_duration = 1.hour)
    scope = potentially_compromised(incident_resolved_at)
    scope = scope.where(["accessed_at < ?", inactivity_duration.ago]) if inactivity_duration
    scope.revoke_all(:security_incident)
  end

  # Public: HMAC signing instance for session's own secret.
  #
  # Returns new OpenSSL::HMAC instance.
  def secret_hmac
    OpenSSL::HMAC.new(self.secret, SECRET_HMAC_DIGEST.new)
  end

  # Internal: Generate HMAC for lookup key.
  #
  # key       - Raw 36 byte String key
  # timestamp - Integer timestamp
  #
  # Returns 20 byte SHA1 HMAC digest String.
  def generate_signed_key_hmac_digest(key, timestamp)
    self.secret_hmac.update(key.to_s).update(timestamp.to_s).digest
  end

  # Public: Is session active and good to authenticate against.
  #
  # Returns true or false.
  def active?
    self.state == :active
  end

  # Public: Get session expires duration.
  #
  # How long after their last access should sessions expire in seconds.
  #
  # Returns Duration.
  def self.user_session_expiration
    GitHub.user_session_timeout.seconds
  end
  delegate :user_session_expiration, to: "self.class"

  # Public: Get session access log write throttle
  #
  # Window of time between access log writes
  #
  # Defaults to 1.day. Needs to be adjusted if user_session_expiration is set to
  # less than a day.
  #
  # Returns Duration
  def self.access_throttling
    GitHub.user_session_access_throttling.seconds
  end
  delegate :access_throttling, to: "self.class"

  # Public: Get session expires duration.
  #
  # Returns 2 weeks for regular sessions and just an hour for
  # impersonated ones.
  #
  # Returns Duration.
  def expires
    if impersonated?
      IMPERSONATED_EXPIRES
    else
      user_session_expiration
    end
  end

  # Expiry time for this session. Always works in UTC to prevent
  # timezone issues
  def expire_time
    Time.now.utc + expires
  end

  # Public: Is session expired?
  #
  # Returns true or false.
  def expired?
    accessed_at < expires.ago
  end

  # Public: Determine if session has be manually revoked.
  #
  # If the session is impersonated, also check the parent session to see
  # if it was also revoked.
  #
  # Returns true or false.
  def revoked?
    revoked_at.present? ||
      (impersonated? && impersonator_session.revoked?)
  end

  # Public: Determine if a session is anomalous. Omits unrecognized_devices
  # until we ship those alerts.
  #
  # Returns true if the session is anomalous and is from an
  # unrecognized_device_and_location or unrecognized_location; otherwise false.
  def anomalous?
    !!sign_in_record&.anomalous?
  end

  # Public: The previous user session that hasn't expired and controlled by an
  # impersonator.
  #
  # Returns UserSession instance.
  def previous_user_session
    return @previous_user_session if defined?(@previous_user_session)
    @previous_user_session = user.sessions.unexpired.where([
      "user_sessions.impersonator_session_id IS NULL AND user_sessions.revoked_at IS NULL AND user_sessions.id <> ?",
      id,
    ]).first
  end

  # Public: Revoke session so it can no longer be used for authentication.
  #
  # reason - String or Symbol identifier for why the session is being revoked.
  #
  # Returns self.
  def revoke(reason, unverify_device: true)
    reason = reason.to_s
    if !VALID_REVOKE_REASONS.include?(reason)
      raise ArgumentError, "invalid revocation reason: #{reason}"
    end

    unless revoked?
      if unverify_device && %w(logout unimpersonate).exclude?(reason) && !impersonated?
        self.sign_in_record&.authenticated_device&.unverify if GitHub.sign_in_analysis_enabled?
      end
      self.revoked_at = Time.now
      self.revoked_reason = reason.to_s
      save
    end
    self
  end

  # Public: Mass revoke sessions.
  #
  # Updates all sessions that aren't expired or already revoked.
  #
  # reason - String or Symbol identifier for why the session is being revoked.
  #
  # Returns Integer count of updated rows.
  def self.revoke_all(reason)
    if !VALID_REVOKE_REASONS.include?(reason.to_s)
      raise ArgumentError, "invalid revocation reason: #{reason}"
    end

    # Generates the following SQL.
    #
    # UPDATE `user_sessions`
    #   SET `revoked_at` = '2014-04-08 12:00:00',
    #       `revoked_reason` = 'security_incident'
    #   WHERE ((`user_sessions`.`revoked_at` IS NULL) AND
    #          (accessed_at > '2014-03-25 12:00:00'))
    #
    where(["accessed_at > ?", user_session_expiration.ago]).where(revoked_at: nil)
      .update_all({revoked_at: Time.now, revoked_reason: reason.to_s})
  end

  # Public: Returns sessions with a valid web sign in record
  #
  # Retrieves the web sign in record. There should only ever
  # be one web sign in record per session.
  #
  # Returns the matching web sign in record for the session
  def sign_in_record
    return @sign_in_record if defined?(@sign_in_record)
    @sign_in_record = authentication_records.web_sign_ins.first
  end

  # Public: Get session state identifier.
  #
  # Returns Symbol state.
  #   :active  - Active and good for authentication.
  #   :expired - Hasn't been accessed in 2 weeks, no longer good.
  #   :revoked - Manually revoked by user or the system before it had expired.
  #   :invalid - Bad record validation state. Any session in this state
  #              is a symtom of a bug.
  def state
    if expired?
      :expired
    elsif revoked?
      :revoked
    elsif !valid?
      :invalid
    else
      :active
    end
  end

  # Public: Check if session is a staff impersonation of a user.
  #
  # Returns true or false.
  def impersonated?
    impersonator_session_id.present?
  end

  # Public: Check if session has sudo capabilities.
  #
  # Always returns true unless our authentication method supports password
  # verification.
  #
  # Impersonated staff sessions are always in sudo.
  #
  # Returns true or false.
  def sudo?(access_level = :low)
    if !GitHub.auth.sudo_mode_enabled?(user)
      true
    elsif impersonated?
      true
    elsif sudo_enabled_at
      sudo_enabled_at > SUDO_EXPIRY.ago
    else
      false
    end
  end

  # Public: Touch sudo timestamp and enable sudo for another 2 hours.
  #
  # Returns nothing.
  def enable_sudo(access_level = :low)
    if sudo_enabled_at.nil? || sudo_enabled_at < SUDO_THROTTLING.ago
      ActiveRecord::Base.connected_to(role: :writing) do
        update_column :sudo_enabled_at, Time.now
      end
    end
  end

  # Public: Clear sudo timestamp and kill sudo mode.
  #
  # Exists mainly for debugging and tests.
  #
  # Returns nothing.
  def expire_sudo
    ActiveRecord::Base.connected_to(role: :writing) do
      update_column(:sudo_enabled_at, nil)
    end
  end

  # Public: Check is session was accessed within the last hour.
  #
  # Returns true or false.
  def recent?
    accessed_at > RECENTNESS.ago
  end

  # Public: Touch accessed timestamp to note user accessed the session.
  #
  # Writes are throttled to a small window to avoid writes on every page load.
  #
  # request - ActionDispatch::Request to log in request/cookie metadata.
  #
  # returns the result of the update operation (if any)
  def access(request)
    return if new_record?

    assign_attributes_from_request(request)

    if update_session_data?
      GitHub.dogstats.increment("user_session", tags: ["action:access"])
      GitHub.dogstats.time("user_session_duration", tags: ["action:access"]) do
        update_attributes_and_accessed_at
      end
    end
  end

  # Private: Do the actual access work of updating accessed_at and optionally
  # updating metadata from the request
  #
  # returns the result of the update operation
  def update_attributes_and_accessed_at
    if ip_changed?
      GitHub.cache.set(update_cache_key("ip"), true, IP_CHANGE_THROTTLING.to_i)
    end

    saved = ActiveRecord::Base.connected_to(role: :writing) do
      if changed.any?
        self.accessed_at = Time.now
        save
      else
        update_column :accessed_at, Time.now
      end
    end

    GitHub.cache.set(update_cache_key("accessed_at"), true, access_throttling.to_i)

    instrument_access
    saved
  end
  private :update_attributes_and_accessed_at

  def assign_attributes_from_request(request)
    self.ip = request.remote_ip
    self.user_agent = request.user_agent
    if zone = ActiveSupport::TimeZone[request.cookies["tz"].to_s]
      self.time_zone_name = zone.name
    end
  end

  # Public: Get the location of the session's IP.
  #
  # Returns a Hash containing as much location information as was available.
  def location
    return @location if defined? @location
    @location = GitHub::Location.look_up(self.ip)
  end

  # Public: Parse user agent string.
  #
  # Returns UserAgent instance or nil.
  def ua
    return @ua if defined? @ua
    @ua = user_agent ? Browser.new(user_agent) : nil
  end

  # Public: Check if session is a mobile device.
  #
  # Returns a boolean.
  def mobile?
    ua && ua.device.mobile?
  end

  # Public: True if the user's session contains geo-location information,
  # and the country code for that location matches the arugment.
  def from_country?(country_code)
    detailed_location = location
    !detailed_location.nil? && detailed_location[:country_code] == country_code
  end

  def event_context(prefix: :session)
    {
      "#{prefix}_id".to_sym => id,
    }
  end

  # Public: True if value matches a valid session for oauth_application access
  def valid_for_oauth_application?(oauth_application, value)
    oauth_application_secret = secret_hmac.update(oauth_application.plaintext_secret).hexdigest
    SecurityUtils.secure_compare(value, oauth_application_secret)
  end

  # Checks the sign in history to determine if this session is associated
  # with an unrecognized sign in event
  def associated_with_unrecognized_sign_in?
    !!self.authentication_records.web_sign_ins.first&.flagged_reason
  end

  private
    # Internal: Populate secret signing token.
    def set_secret
      self.secret ||= SecureRandom.base64(SECRET_BYTES)
    end

    # Internal: Populate CSRF token.
    def set_csrf_token
      self.csrf_token ||= SecureRandom.base64(CSRF_BYTES)
    end

    # Internal: Initialize the accesed at timestamp.
    def set_initial_accessed_at
      self.accessed_at ||= Time.now
    end

    # Internal: Initialize sudo mode on create.
    def set_initial_sudo_enabled_at
      self.sudo_enabled_at ||= Time.now
    end

    # Internal: Set denormalized impersonator user association from the
    # impersonator session.
    #
    # This is mainly kept for historical purposes. Since sessions are
    # ephemeral, its handy to be able to get the original impersonating
    # user long after the session is pruned from the database.
    #
    # Set the value if its not already. Validation steps will ensure
    # consistency.
    def set_impersonator
      if impersonator_session
        self.impersonator ||= impersonator_session.user
      end
    end

    # Internal: Ensure theres a user associated with the session.
    #
    # UserSessions can not be created for anonymous purposes.
    def user_present
      if user.nil?
        errors.add :user_id, "is not a valid user"
      end
    end

    # Internal: Ensure user is not a User subclass.
    #
    # Organizations and bots can never log in. This also covers the case of
    # transforming a user into an organization. At that time, all user
    # sessions should be revoked, so just in case.
    def user_is_human
      if user && !user.user?
        errors.add :user_id, "is not a human user"
      end
    end

    # Internal: Consistency check the denormalized `impersonator_id` and
    # `impersonator_session_id` fields. The impersonator user must always
    # match the owner of the impersonator session.
    def impersonator_and_session_match
      if impersonator || impersonator_session
        if !impersonator_session
          errors.add :impersonator_session_id, "must be set"
        elsif impersonator != impersonator_session.user
          errors.add :impersonator_id, "doesn't match impersonator session"
        end
      end
    end

    # Internal: Ensure impersonator is staff.
    #
    # Only staff can impersonate other users. If for some reason a user
    # is demoted from staff, all their impersonated sessions would then be
    # invalid.
    def impersonator_is_staff
      if impersonator && !impersonator.site_admin?
        errors.add :impersonator_id, "is not staff"
      end
    end

    # Internal: Ensure impersonator session has sudo.
    def impersonator_has_sudo
      if impersonator_session && !impersonator_session.sudo?
        errors.add :impersonator_session_id, "must have sudo"
      end
    end

    # Internal: Ensure users can not create impersonated sessions for their
    # own account. Just why, really?
    def impersonator_is_not_user
      if impersonator && impersonator == user
        errors.add :impersonator_id, "can not pose as self"
      end
    end

    # Internal: Update associated user's time zone when it changes on the
    # session.
    def set_user_time_zone
      user.time_zone_name = self.time_zone_name

      ActiveRecord::Base.connected_to(role: :writing) do
        user.save
      end
    end

    # Internal: Normalize and clean user agent value before saving it. Since
    # we pull these directly from request headers they may have some garbage.
    def normalize_user_agent
      self.user_agent = clean_string_column_value(self.user_agent) if self.user_agent
    end

    # Internal: Clean string to be inserted into a MySQL TEXT(255) column.
    #
    # Scrubs anyone non-ASCII characters and truncates to 255 characters.
    #
    # str - String to clean
    #
    # Returns a cleaned String.
    def clean_string_column_value(str)
      str.dup.force_encoding("US-ASCII").scrub[0, 255]
    end

    def event_payload
      {
        event_prefix => self,
        :user        => user,
      }
    end

    # Internal: Triggers a "user_session.create" event for logging purposes.
    #
    # Occurs only once when a valid session is created.
    def instrument_create
      instrument :create
    end

    # Internal: Triggers a "user_session.access" event for logging purposes.
    #
    # Occurs any time a makes a request with the associated user session.
    # Note that is call is rated limited a 5 minute window.
    def instrument_access
      instrument :access
    end

    # Internal: Triggers "user_session.country_change" event for logging purposes
    def instrument_country_change
      # We want the audit entry to only be for legitimate actors which may
      # have (un)intentionally changed countries. Bots become too spammy
      return if impersonated?
      return if AuthenticationRecord.user_rate_limited?(self.user)

      last_location = GitHub::Location.look_up(self.ip_before_last_save)
      GitHub.audit.normalize_location(last_location)

      # We only care if the country has changed
      return if last_location[:country_code] == self.location[:country_code]

      instrument :country_change, {
        actor: self.user,
        actor_ip: self.ip,
        actor_ip_was: self.ip_before_last_save,
        actor_location_was: last_location,
      }

      GlobalInstrumenter.instrument("user.user_country_change",
        actor: self.user,
        previous_location: last_location,
        anomalous_session: anomalous?,
      )
    end

    def update_session_data?
      accessed_at_needs_updating? || ip_needs_updating?
    end

    # Check whether this user_session record's accessed_at attribute has been
    # updated or that we've already recently checked for access. We fallback to
    # checking the cache in case there's database replication lag on recently
    # updated sessions.
    def accessed_at_needs_updating?
      self.accessed_at < access_throttling.ago &&
        !GitHub.cache.get(update_cache_key("accessed_at"))
    end

    # Check whether this user_session record's ip attribute has been
    # updated or that we've already recently checked for access. We fallback to
    # checking the cache in case there's database replication lag on recently
    # updated sessions.
    def ip_needs_updating?
      ip_changed? &&
        self.updated_at <= IP_CHANGE_THROTTLING.ago &&
        !GitHub.cache.get(update_cache_key("ip"))
    end

    def update_cache_key(column)
      "v2:user_session:#{id}:update_#{column}"
    end
end
