# frozen_string_literal: true

class MannequinEmail < ApplicationRecord::Domain::Users
  belongs_to :mannequin

  def public?
    false
  end
end
