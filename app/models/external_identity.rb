# rubocop:disable Style/FrozenStringLiteralComment

class ExternalIdentity < ApplicationRecord::Domain::Users
  include GitHub::Relay::GlobalIdentification

  VALID_PROVIDER_TYPES = %w(Organization::SamlProvider Business::SamlProvider)

  EXTERNAL_IDENTIFIER_ATTRIBUTE_NAMES = {
      azuread: "http://schemas.microsoft.com/identity/claims/objectidentifier",
  }.freeze

  attr_readonly :guid
  attr_accessor :prefilled_group_members

  belongs_to :user
  has_one :organization_invitation, dependent: :nullify, inverse_of: :external_identity
  belongs_to :provider, polymorphic: true
  delegate :target, to: :provider

  has_many :sessions,
    class_name: "ExternalIdentitySession",
    dependent: :delete_all

  has_many :user_sessions,
    through: :sessions,
    source: :user_session

  # Internal: Records backing an ExternalIdentities attributes provided
  # by the identity provider. Use the `saml_user_data` to interact
  # with these attributes instead of directly accessing the records.
  has_many :identity_attribute_records,
    class_name: "ExternalIdentityAttribute",
    autosave: true,
    extend: ExternalIdentityAttribute::AssociationExtension

  validate :validate_saml_name_id_is_unique
  validate :validate_scim_user_name_is_unique
  validates :provider, presence: true
  validates :provider_type, inclusion: VALID_PROVIDER_TYPES

  before_create :set_guid
  before_destroy :cancel_pending_invite

  # Public: Finds all identities that are not marked as deleted.
  #
  # NOTE: This scope is only applied for GHES environments. This is primarily because
  # external identities are removed on github.com rather than marked for deletion.
  #
  # Returns a scope.
  scope :not_deleted, -> {
    next unless GitHub.enterprise?
    where(deleted_at: nil)
  }

  # Public: Finds all identities linked to a given User.
  #
  # user    - The User the identities should be linked to.
  #
  # Returns a scope.
  scope :linked_to, -> (user) { where(user_id: user.id) }

  # Public: Finds all identities which are not linked to a User account.
  #
  # Returns a scope.
  scope :unlinked, -> { where(user_id: nil) }

  # Public: Finds all identities linked to a given resource type (User or Organization)
  #  Note: will not return any unlinked identities
  #
  # Returns a scope.
  scope :by_resource_type, -> (resource_type) {
    joins(:user).where(users: { type: resource_type })
  }
  scope :user_identities, -> { by_resource_type("User") }
  scope :group_identities, -> { by_resource_type("Organization") }

  # Public: Returns all the identities, except for the ones linked to Organizations. Gets all the
  # unlinked identities and identities linked to Users.
  #
  # Returns a scope.
  scope :unlinked_or_users, -> {
    identities_scope = joins("LEFT OUTER JOIN users on external_identities.user_id = users.id")
    identities_scope.unlinked.or(identities_scope.where(users: { type: "User" }))
  }

  # Public: Finds ExternalIdentities which are for the given provider.
  #
  # provider  - An instance of the provider (e.g. Organization::SamlProvider)
  #
  # Returns a scope.
  scope :by_provider, -> (provider) {
    where(provider_id: provider.id, provider_type: provider.class.name)
  }

  # Public: Find ExternalIdentities that matches the given identifier
  #
  # user_data - A UserData object containing the identifier attributes.
  # mapping   - a Platform::Provisioning::IdentityMapping object
  # identifier_attributes - (Optional) Specific mapping attributes to be used instead
  #     of the default specified by the IdentityMapping object.
  #
  # Returns a scope.
  scope :by_identifier, -> (user_data, mapping:, identifier_attributes: mapping.identifier_attributes) {
    identifier = mapping.value_from_user_data(user_data).presence
    return none unless identifier

    where(<<-SQL, value: identifier, names: Array.wrap(identifier_attributes))
      EXISTS (
        SELECT 1 FROM external_identity_attributes AS attrs
        WHERE attrs.external_identity_id = external_identities.id
          AND attrs.name IN (:names)
          AND attrs.value = :value
        LIMIT 1
      )
    SQL
  }

  # Public: Find SCIM provisioned ExternalIdentities that match any of the
  # provided emails.
  #
  # # emails - An Array of email addresses.
  #
  # Returns a scope.
  scope :by_scim_emails, -> (emails) {
    return none unless emails.any?
    where(<<-SQL, value: Array.wrap(emails), name: :emails, scheme: :scim)
      EXISTS (
        SELECT 1 FROM external_identity_attributes AS attrs
        WHERE attrs.external_identity_id = external_identities.id
          AND attrs.scheme = :scheme
          AND attrs.name = :name
          AND attrs.value IN (:value)
        LIMIT 1
      )
    SQL
  }

  # Internal: Finds ExternalIdentities which have been provisioned by the
  # specified scheme.
  #
  # scheme    - The scheme name by which the attributes were provided (e.g. :saml).
  #
  # Returns a scope.
  scope :provisioned_by, -> (scheme) {
    where(<<-SQL, scheme: scheme)
      EXISTS (
        SELECT 1 FROM external_identity_attributes AS attrs
        WHERE attrs.external_identity_id = external_identities.id
          AND attrs.scheme = :scheme
        LIMIT 1
      )
    SQL
  }

  # Internal: Finds ExternalIdentities with matching UserData for a given scheme.
  # For an external identity to be returned all specified user_data attribute values
  # must match.
  #
  # Please use scheme specific helpers like `by_saml_user_data` where possible.
  #
  # scheme    - The scheme name by which the attributes were provided (e.g. :saml).
  # user_data - A UserData object containing the required attributes.
  #
  # Returns a scope.
  scope :by_user_data, -> (scheme:, user_data:) {
    user_data.reduce(all) do |conditions, attr|
      conditions.where(<<-SQL, scheme: scheme, name: attr["name"], value: attr["value"])
        EXISTS (
          SELECT 1 FROM external_identity_attributes AS attrs
          WHERE attrs.external_identity_id = external_identities.id
            AND attrs.scheme = :scheme
            AND attrs.name = :name
            AND attrs.value IN (:value)
          LIMIT 1
        )
      SQL
    end
  }

  # Public: Finds ExternalIdentities with matching SAML UserData.
  #
  # user_data - A UserData object containing the required attributes.
  #
  # Returns a scope.
  scope :by_saml_user_data, -> (user_data) {
    by_user_data(scheme: :saml, user_data: user_data)
  }

  # Public: Finds ExternalIdentities with matching SCIM UserData.
  #
  # user_data - A UserData object containing the required attributes.
  #
  # Returns a scope.
  scope :by_scim_user_data, -> (user_data) {
    by_user_data(scheme: :scim, user_data: user_data)
  }

  # Public: Finds ExternalIdentity's that belong to the specified group
  #
  #   group_ids - group identifiers, can be names, guids, or combination of both
  #
  # Returns a scope
  scope :by_group, -> (group_ids) {
    where(<<-SQL, group_ids: group_ids, group_attr_names: Platform::Provisioning::SamlUserData::GROUP_ATTRIBUTE_NAMES)
      EXISTS (
        SELECT 1 FROM external_identity_attributes AS attrs
        WHERE attrs.external_identity_id = external_identities.id
          AND attrs.name IN (:group_attr_names)
          AND attrs.value IN (:group_ids)
        LIMIT 1
      )
    SQL
  }

  scope :scim_filter, -> (filter) {
    SCIM::Filter.apply(filter, scope: all)
  }

  scope :with_attributes, -> { includes(:identity_attribute_records) }

  # Public: Finds ExternalIdentities with SAML UserData NameID matching the
  # given identity.
  #
  # identity - A ExternalIdentity object containing SAML user data.
  #
  # Returns a scope.
  scope :identities_with_same_name_id_as, -> (identity) {
    with_same_name_id = by_provider(identity.provider).
      by_saml_user_data(identity.saml_user_data.slice("NameID"))
    with_same_name_id = with_same_name_id.where.not(id: identity.id) if identity.persisted?
    with_same_name_id
  }

  # Public: removes an external identity for an organization or business member
  #
  # provider    - The identity provider the external identity is for.
  # user        - The User the external identity is for.
  # perform_instrumentation  - Should we record an audit log event for this action?
  # instrumentation_payload  - audit log payload
  def self.unlink(provider:, user:, perform_instrumentation: false, instrumentation_payload: {})
    unlink_users(
      where(
        provider_id: provider.id,
        provider_type: provider.class.name,
        user_id: user.id
      )
    )

    if perform_instrumentation
      provider.target.instrument_external_identity_revoked(instrumentation_payload)
    end
  end

  # Public: removes SAML-provisioned external identities, provisioned by the given provider,
  # for the specified list of users. Does not remove ExternalIdentity's that have any SCIM
  # provisioned attributes.
  #
  # provider    - The identity provider which provisioned the external identities
  # user_ids    - id's of Users whose identities are to be removed
  #
  # Returns: nothing
  def self.unlink_saml_identities(provider:, user_ids:)
    base_scope = ExternalIdentity.
      provisioned_by(:saml).
      includes(:identity_attribute_records).
      where(
        provider_id: provider.id,
        provider_type: provider.class.name,
        user_id: user_ids)
    saml_identity_ids = []
    # ExternalIdentity.provisioned_by(:saml) will return all ExternalIdentity's that have SAML
    # attributes, but it can't return identities that ONLY have SAML-provisioned attributes, so
    # adding this extra loop here to reject any that also have SCIM-provisioned attributes.
    base_scope.in_batches do |external_identities|
      saml_identity_ids += \
        external_identities.to_a.reject do |identity|
          identity.identity_attribute_records.attributes_by_scheme[:scim].any?
        end.map(&:id)
    end

    unlink_users(ExternalIdentity.where(id: saml_identity_ids)) unless saml_identity_ids.empty?
  end

  # Public: unlinks (destroys) the specified external identities. Works in batches of
  # 1000 at a time so the query doesn't time out.
  #
  # external_identities - identities to destroy
  #
  # Returns: nothing
  def self.unlink_users(external_identities)
    external_identities.in_batches do |external_identities|     # load and destroy 1,000 at a time
      external_identities.destroy_all
    end
  end

  # Public: Determines if a user has already linked their account to a given
  # provider.
  #
  # provider    - The identity provider the external identity is for.
  # user        - The User the external identity is for.
  #
  # Returns a boolean.
  def self.linked?(provider:, user:)
    attributes = {
      provider_id: provider.id,
      provider_type: provider.class.name,
      user_id: user.id,
    }
    where(attributes).exists?
  end

  # Public: has the given identity's NameID been taken by an existing external
  # identity?
  #
  # Returns a Boolean.
  def self.name_id_already_taken?(identity)
    identities_with_same_name_id_as(identity).any?
  end

  def self.identity_provider(team)
    provider = team.organization.team_sync_tenant.provider_type.to_sym
    EXTERNAL_IDENTIFIER_ATTRIBUTE_NAMES.fetch(provider) { "externalId" }
  end

  # Public: Returns user data we have on hand provided by SAML.
  #
  # Returns a Platform::Provisioning::SamlUserData instance.
  def saml_user_data
    saml_attrs = identity_attribute_records.attributes_by_scheme["saml"]
    user_data = Platform::Provisioning::SamlUserData.new(saml_attrs)
    Platform::Provisioning::AttributeMappedUserData.new(user_data)
  end

  # Public: Sets SAML identity attributes defined by the given UserData object.
  #
  # user_data   - A Platform::Provisioning::UserData instance.
  #
  # Returns nothing.
  def saml_user_data=(user_data)
    identity_attribute_records.set_scheme_attributes("saml", user_data.to_a)
  end

  # Public: Returns group data we have on hand provided by SAML.
  #
  # Returns a Platform::Provisioning::SamlGroupData instance.
  def saml_group_data
    saml_attrs = identity_attribute_records.attributes_by_scheme["saml"]
    Platform::Provisioning::SamlGroupData.new(saml_attrs)
  end

  # Public: Returns the SCIM user data we have.
  #
  # Returns a Platform::Provisioning::ScimUserData instance.
  def scim_user_data
    scim_attrs = identity_attribute_records.attributes_by_scheme["scim"]
    user_data = Platform::Provisioning::ScimUserData.new(scim_attrs)
    Platform::Provisioning::AttributeMappedUserData.new(user_data)
  end

  # Public: Sets SCIM identity attributes defined by the given UserData object.
  #
  # user_data   - A Platform::Provisioning::UserData instance.
  #
  # Returns nothing.
  def scim_user_data=(user_data)
    identity_attribute_records.set_scheme_attributes("scim", user_data.to_a)
  end

  # Public: Returns the SCIM group data we have.
  #
  # Returns a Platform::Provisioning::SCIMGroupData instance.
  def scim_group_data
    scim_attrs = identity_attribute_records.attributes_by_scheme["scim"]
    Platform::Provisioning::SCIMGroupData.new(scim_attrs)
  end

  # Internal: Prefix for auditing / instrumentation
  def event_prefix
    :external_identity
  end

  def event_context(prefix: event_prefix)
    {
      "#{event_prefix}_guid".to_sym => guid,
      "#{event_prefix}_nameid".to_sym => saml_user_data.name_id,
      "#{event_prefix}_username".to_sym => scim_user_data.user_name,
    }
  end

  private
  def validate_saml_name_id_is_unique
    return unless provider
    return unless name_id = saml_user_data.name_id

    if self.class.name_id_already_taken?(self)
      taken_by = self.class.identities_with_same_name_id_as(self).first.user
      errors.add(:base, "NameID '#{ name_id }' is already linked to #{ taken_by }'s account.")
    end
  end

  def validate_scim_user_name_is_unique
    return unless provider
    return unless user_name = scim_user_data.user_name

    with_same_user_name = self.class.by_provider(provider).
      by_scim_user_data(scim_user_data.slice("userName"))
    with_same_user_name = with_same_user_name.where.not(id: id) if persisted?

    if with_same_user_name.exists?
      taken_by = with_same_user_name.first.user
      errors.add(:base, "External login '#{user_name}' is already linked to #{ taken_by }'s account.")
    end
  end

  def set_guid
    self.guid = SimpleUUID::UUID.new.to_guid
  end

  def cancel_pending_invite
    return unless organization_invitation.try(:pending?)
    organization_invitation.cancel(actor: organization_invitation.organization)
  end
end
