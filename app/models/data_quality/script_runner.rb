# rubocop:disable Style/FrozenStringLiteralComment

# Base runner for data quality scripts.
#
# Data quality scanners must be explicitly registered with the runner so that
# the correct ordering can be guaranteed.
class DataQuality::ScriptRunner < ::DataQuality::Script

  # Internal: the registered script names, classes, and descriptions.
  #
  # name         - the external  name for the data quality script
  # script_class - the script's class
  # description  - the external description of the script
  #
  # Name and description are visible in the help output of the script/data-check
  # script, so consider them customer-facing, at least for enterprise owners.
  def self.scripts
    @scripts ||= {}
  end

  # Public: register a script with the script runner.
  #
  # script_name  - the command-line-friendly script name
  # script_class - the Class of the associated script
  def self.register(script_name, script_class, description)
    scripts[script_name] = [script_class, description]
  end

  register "network-parent", ::DataQuality::RepositoryNetwork::Parent,
    "Find repos with incorrect parent references"
  register "network-root", ::DataQuality::RepositoryNetwork::Root,
    "Find networks with incorrect root repos"
  register "network-mixed", ::DataQuality::RepositoryNetwork::Mixed,
    "Find networks with mixed visibility"

  register "issue-assignment-sync", ::DataQuality::Basic::IssueAssignmentSync,
    "Find assignments out of sync with their issues"

  register "repo-license", ::DataQuality::Basic::RepositoryLicense,
    "Detect licenses in open-source repos"

  # Public: the names of the available scripts.
  def self.script_names
    scripts.keys.dup
  end

  # Public: the names and descriptions of the available scripts
  def self.available_scripts
    scripts.map do |name, value|
      [name, value.last]
    end
  end

  # Internal: the configured scripts to run
  attr_reader :scripts_to_run

  # Public: Initialize a new data quality script runner
  #
  # script   - a String or array of strings for which scan(s) to perform
  # clean:   - true or false, whether or not to clean any results.
  # verbose: - true or false, enable for verbose output
  def initialize(scripts, clean: false, verbose: false)
    scripts = Array(scripts).flatten
    @scripts_to_run = []
    scripts.each do |script|
      if self.class.script_names.include? script
        @scripts_to_run << script
      else
        raise ArgumentError, "Unknown data quality script"
      end
    end

    if @scripts_to_run.empty?
      raise ArgumentError, "Must specify at least one script to run"
    end

    @clean   = clean
    @verbose = verbose
  end

  # Run the specifed script or scripts
  def run
    log "Starting data quality check"

    # Log who's running this and what scan they specified:
    scan_type = scripts_to_run.join(", ")
    msg = "Running #{clean? ? "clean" : "scan" } with scan types: #{scan_type}"
    msg << " as user #{ENV["LOGNAME"]}"
    if user = ENV["SUDO_USER"]
      msg << " on behalf of user #{user}"
    end
    log msg

    scripts_to_run.each do |script_name|
      log "* Scan #{script_name} starting"
      script_class = self.class.scripts[script_name].first
      script_class.new(clean: clean?, verbose: verbose?).run
      log "* Scan #{script_name} finished"
    end
    log "Done."
  end

end
