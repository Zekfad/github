# rubocop:disable Style/FrozenStringLiteralComment

# This data quality script is designed to help debug search indexing problems
# where the indexed documents don't match the database. Specifically, this
# examines the issue state (open or closed), since this is the most obvious
# user-facing and user-reported issue.
#
# It's not meant to be a part of the main data quality run, nor does it have a
# "repair" functionality: use the reindexing jobs for that.
module DataQuality::Search
  class PullRequestIndex < ::DataQuality::Script
    def run
      index = Elastomer::Indexes::PullRequests.new

      records = readonly do
        PullRequest.github_sql.results <<-SQL
          SELECT pr.id, issue.state, pr.updated_at
          FROM pull_requests pr
          JOIN issues issue ON issue.pull_request_id = pr.id
          WHERE issue.updated_at > NOW() - INTERVAL 6 HOUR
          AND issue.updated_at < NOW() - INTERVAL 30 MINUTE
        SQL
      end

      records.each_slice(500) do |slice|
        lookup = {}
        seen = {}
        slice.each do |id, state, updated_at|
          lookup[id] = [state, updated_at.utc]
          seen[id] = false
        end

        query = {
          query: { ids: { type: "pull_request", values: lookup.keys }},
          _source: %w[state updated_at],
          size: lookup.size,
        }

        results = index.search(query, {type: "pull_request"})

        results["hits"]["hits"].each do |doc|
          id      = doc["_id"].to_i
          source  = doc["_source"]
          state   = source["state"]
          updated = Time.parse(source["updated_at"])

          if updated > 30.minutes.ago
            seen[id] = true
            next
          end

          if lookup[id]
            seen[id] = true
            if lookup[id].first != state
              log "* pr #{id} has #{lookup[id].first} in the db, #{state} in ES. db: #{lookup[id].last} ES: #{updated}"
            end
          else
            log "* got unknown pr #{id}, which should never happen."
          end
        end

        seen.reject { |k, v| v }.map(&:first).each do |missing_id|
          log "* pr #{missing_id} is missing from ES"
        end
      end

    end
  end
end
