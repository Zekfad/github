# rubocop:disable Style/FrozenStringLiteralComment

module DataQuality::RepositoryNetwork
  # Finds all repository networks containing repos with
  # differing visibilities and splits them up. Private
  # repos forked from public repos will be extracted into
  # their own new network. Private repos which are the
  # root of an all-public network are detached so that
  # the public network can remain intact. Public forks
  # of private repositories stil remaining after the last
  # two steps are extracted into their own networks.
  #
  # This depends on the parent references being correct for
  # all repos as well as the root reference being correct
  # for all networks. DataQuality::RepositoryNetwork::Parent
  # and DataQuality::RepositoryNetwork::Root should be run
  # prior to this to ensure this does the right thing.
  class Mixed < ::DataQuality::Script
    def run
      extract_private_forks_of_public_repos
      detach_private_roots_with_all_public_forks
      extract_public_forks_of_private_repos
    end

    def extract_private_forks_of_public_repos
      log "* Scanning for private forks of public repos"
      repos = find_private_forks_of_public_repos

      log "  * Found #{repos.size} private forks of public repos"
      repos.each { |repo| debug "    * Repository #{repo.id} will be extracted into its own network" }

      return unless clean? && repos.size > 0

      extract_or_detach_repos(repos, :extract)
    end

    def detach_private_roots_with_all_public_forks
      log "* Scanning for private roots with all-public forks"
      network_ids = find_networks_with_a_private_root_and_public_fork
      detachable_networks = network_ids.each_with_object([]) do |network_id, networks|
        network = ::RepositoryNetwork.find_by_id(network_id)

        if network && (network.repositories - [network.root]).all?(&:public?)
          networks << network
        end
      end

      log "  * Found #{detachable_networks.size} private roots with all-public forks"
      detachable_networks.each { |n| debug "    * Would detach #{n.root.name_with_owner}" }

      return unless clean? && detachable_networks.size > 0

      extract_or_detach_repos(detachable_networks.map(&:root), :detach)
    end

    def extract_public_forks_of_private_repos
      log "* Scanning for public forks of private repositories"
      pub_forks = find_public_forks_of_private_repos
      log "  * Found #{pub_forks.size} public forks of private repositories to extract"
      pub_forks.each { |f| debug "    * Would extract public fork #{f.name_with_owner}" }

      return unless clean? && pub_forks.size > 0

      extract_or_detach_repos(pub_forks, :extract)
    end

    def extract_or_detach_repos(repos, action)
      raise "action can be :extract or :detach" if ![:extract, :detach].include?(action)
      failed_extracts = []
      extracted = 0
      ApplicationRecord::Domain::Repositories.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        repos.each do |repo|
          next unless repo
          begin
            case action
            when :extract
              debug "    * Extracting repository #{repo.id} (#{repo.name_with_owner})"
              repo.extract!
            when :detach
              debug "    * Detaching repository #{repo.id} (#{repo.name_with_owner})"
              repo.detach!
            end
          rescue Exception => e # rubocop:disable Lint/RescueException
            failed_extracts << [repo.id, e]
          end

          # This is normally run after each web request to reset the stack trace objects
          # created by the GitRPC log. Since this is not a web request, we must manually
          # reset the logs so that the stack traces of all GitRPC calls don't hang
          # around after each extract! and cause a memory leak.
          GitRPCLogSubscriber.reset_stats
          extracted += 1
        end
      end

      log "  * #{action.to_s.capitalize}ed #{extracted} repositories into their own networks"

      if !failed_extracts.empty?
        log "  * #{action.capitalize} failed for #{failed_extracts.length} repositories:"
        log failed_extracts
      end
    end

    def find_private_forks_of_public_repos
      readonly do
        Repository.github_sql.new(<<-SQL).models(Repository)
          SELECT repo.*
          FROM repositories repo
          JOIN repositories parent
            ON repo.parent_id = parent.id
          WHERE repo.active = 1
            AND repo.public = 0
            AND parent.public = 1
        SQL
      end
    end

    # assumptions made: root is active (not deleted)
    def find_networks_with_a_private_root_and_public_fork
      readonly do
        Repository.github_sql.values(<<-SQL)
          SELECT DISTINCT root.source_id
          FROM repositories fork
          JOIN repository_networks network
          ON fork.source_id = network.id
          JOIN repositories root
          ON network.root_id = root.id
          WHERE fork.active = 1
          AND fork.parent_id = root.id
          AND fork.public = 1
          AND root.public = 0
        SQL
      end
    end

    def find_public_forks_of_private_repos
      readonly do
        Repository.github_sql.new(<<-SQL).models(Repository)
          SELECT repo.*
          FROM repositories repo
          JOIN repositories parent
            ON repo.parent_id = parent.id
          WHERE repo.active = 1
            AND repo.public = 1
            AND parent.public = 0
        SQL
      end
    end
  end
end
