# frozen_string_literal: true

module DataQuality::RepositoryNetwork
  class DanglingForks < ::DataQuality::Script
    class Fork
      attr_reader :repository, :owner, :parent_repository

      def initialize(repository, owner, parent_repository, logger)
        @repository = repository
        @owner = owner
        @parent_repository = parent_repository
        @logger = logger
      end

      def owner_id
        @owner.id
      end

      def parent_id
        @parent_repository.id
      end

      def repository_id
        repository.id
      end

      def ahead_of_parent?
        return false unless parent_repository&.default_branch_exists? && repository&.default_branch_exists?

        repository.comparison(parent_repository.default_oid, repository.default_oid).ahead?
      rescue GitRPC::ObjectMissing
        log "Comparison failed between #{repository.id}@#{repository.default_oid} and #{parent_repository.id}@#{parent_repository.default_oid}"
        return false
      end

      private

      def log(*args)
        @logger.log(*args)
      end
    end

    def initialize(clean: false, verbose: false, start: nil, finish: nil)
      @start = start || min_id
      @finish = finish || max_id
      super(clean: clean, verbose: verbose)
    end

    def min_id
      @min_id ||= Repository.github_sql.value("SELECT MIN(ID) FROM repositories") || 0
    end

    def max_id
      @max_id ||= Repository.github_sql.value("SELECT MAX(ID) FROM repositories") || 0
    end

    def dangling_repo_batches
      @iterator ||=
        begin
          iter = Repository.github_sql_batched_between(start: @start, finish: @finish, batch_size: 20)

          iter.add <<-SQL
           SELECT DISTINCT forks.id,
                  forks.owner_id,
                  forks.parent_id,
                  parents.owner_id
           FROM repositories AS forks
           INNER JOIN repositories AS parents
             ON parents.id = forks.parent_id

           WHERE forks.parent_id IS NOT NULL
             AND forks.public = false
             AND parents.public = false
             AND forks.id BETWEEN :start AND :last
          SQL
          GitHub::SQL::Readonly.new(iter.batches)
        end
    end

    def run
      removed_fork_ids = Set.new
      total_archived_forks = 0
      total_forks = 0
      could_not_be_deleted = []

      log "Processing fork repo ids #{@start} to #{@finish}"

      GitHub::Throttler::ThrottledEnumerator.new(dangling_repo_batches, throttler: Repository.throttler).each do |rows|
        owner_ids = rows.map(&:second)
        parent_owner_ids = rows.map(&:fourth)
        users_and_orgs = User.where(id: owner_ids + parent_owner_ids)
        user_owners = users_and_orgs.select { |owner| owner.user? }.index_by(&:id)
        org_owners = users_and_orgs.select { |owner| owner.organization? }.index_by(&:id)

        repository_ids = rows.flat_map { |r| [r[0], r[2]] }
        repositories = Repository.where(id: repository_ids).index_by(&:id)

        # Looks up this feature flag up once per batch, which will still allow a quick kill
        # without refetching it on every single row.
        repos_272_log_invalid_forks_enabled = GitHub.flipper[:repos_272_log_invalid_forks].enabled?

        rows.each do |fork_id, fork_owner_id, parent_id, parent_owner_id|
          next if removed_fork_ids.include?(fork_id)
          GitHub.dogstats.increment("dangling_forks.forks_scanned")

          fork = repositories[fork_id]
          parent = repositories[parent_id]
          fork_owner_user = user_owners[fork_owner_id]
          fork_owner_org = org_owners[fork_owner_id]
          parent_owner_org = org_owners[parent_owner_id]
          parent_owner_user = user_owners[parent_owner_id]

          if repos_272_log_invalid_forks_enabled
            InvalidForkChecker.new(fork).log_if_invalid
          end

          next if fork_owner_user.nil? # The fork is org-owned. We never remove org-owned forks.

          next if parent.nil? || parent.permit?(fork_owner_user, :read)

          removed_fork_ids << fork_id
          dangling_fork = Fork.new(fork, fork_owner_user, parent, self)

          # For use in DetectOutOfSyncForks transition
          yield dangling_fork if block_given? && !parent_owner_org.nil?

          total_forks += 1

          if clean?
            begin
              log "owner id:#{fork_owner_id}, parent id:#{parent_id}, fork id:#{fork_id}"
              dangling_fork.repository.remove(nil)
              GitHub.dogstats.increment("dangling_forks.forks_removed")
              total_archived_forks += 1
            rescue StandardError => e
              could_not_be_deleted << fork_id
              GitHub.dogstats.increment("dangling_forks.fork_removal_failures")
              log "Dangling fork archive died on fork ID: #{fork_id}. #{e.class}: #{e.full_message}"
              GitHub::Logger.log(repo: fork_id, exception: e.class, message: e.full_message)
            end
          end
        end
      end

      GitHub.instrument "data_quality.archive_dangling_forks", {
        host_name: GitHub.local_host_name,
        start_repo_id: @start,
        finish_repo_id: @finish,
        clean: clean?,
        total_forks_count: total_forks,
        archived_forks_count: total_archived_forks,
      }

      GitHub.dogstats.gauge("dangling_forks.max_repo_id_scanned", @finish)

      if clean? && could_not_be_deleted.any?
        GitHub::Logger.log(could_not_be_deleted: could_not_be_deleted)
      end
    rescue StandardError => e
      GitHub.dogstats.increment("dangling_forks.unhandled_exceptions")
      log "Unhandled exception scanning repos #{@start} to #{@finish}. #{e.class}: #{e.full_message}"
      raise
    end

    # Temporary, for finding forks to be detached or reparented.
    # See https://github.com/github/pe-repos/issues/272.
    class InvalidForkChecker
      def initialize(fork)
        @fork = fork
      end

      def log_if_invalid
        return if valid?
        GitHub.dogstats.increment("dangling_forks.invalid_fork_logged")
        log_info = [@fork.id,
          @fork.parent_id,
          @fork.source_id,
          @fork.network&.root&.permit?(@fork.owner, :read),
          @fork.disk_usage]
        invalid_fork_logger.info(log_info.join(","))
      end

      # Returns false for forks that are invalid as defined in https://github.com/github/pe-repos/issues/272.
      # No public/private check is necessary because the original query leaves out public forks.
      def valid?
        # TODO: Uncomment check for nested private forks
        # We first want to do a run for OOPFs. We will consider nested private fork as valid for the time being
        # until we ship https://github.com/github/pe-repos/issues/247. At that point we will flip this logic and
        # comment out the check for OOPFs
        # return false if @fork.parent&.fork? # private fork of a fork

        return false if @fork.invalid_org_owned_private_fork?
        true
      end

      def invalid_fork_logger
        @invalid_fork_logger ||= setup_invalid_fork_logger
      end

      def setup_invalid_fork_logger
        log = Logger.new("#{Rails.root}/log/repos-272-invalid-forks.log")
        log.formatter = lambda do |severity, datetime, progname, message|
          "#{message}\n"
        end
        log
      end
    end
  end
end
