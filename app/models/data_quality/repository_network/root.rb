# rubocop:disable Style/FrozenStringLiteralComment

module DataQuality::RepositoryNetwork
  # Data quality scanner to find and correct problems with the roots
  # of repository networks. This scanner assumes that repositories
  # have the parent field set correctly, as it depends on that to find
  # repositories which should be network roots.
  # DataQuality::RepositoryNetwork::Parent should be run prior to this
  # to ensure that the parent field is up to date and correct.
  class Root < ::DataQuality::Script
    def run
      correct_networks_with_no_root
      correct_networks_with_wrong_root
      remove_empty_networks
    end

    # For each of the given networks with a bad root, elect one of the
    # repositories as the new root. The oldest repository in the
    # network is elected.
    def correct_networks_with_no_root
      log "* Scanning for networks with a nonexistent root"
      networks = readonly do
        RepositoryNetwork.github_sql.results <<-SQL
          SELECT network.id, network.root_id
          FROM repository_networks network
          LEFT JOIN repositories root
          ON network.root_id = root.id
          AND root.active = 1
          WHERE root.id IS NULL
        SQL
      end

      log "  * Found #{networks.size} networks with nonexistent root"

      networks.each do |network_id, root_id|
        debug "    * Network #{network_id} refers to nonexistent root #{root_id}"
      end

      return unless clean? && networks.size > 0

      count = 0
      Repository.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        networks.each do |network_id, root_id|
          if repo = Repository.where("source_id = ? AND active = 1", network_id).order("created_at ASC").first
            update_root_to(repo)
            count += 1
          end
        end
      end

      log "  * Updated #{count} networks to correct their root repositories"
    end

    def correct_networks_with_wrong_root
      log "* Scanning for networks with an incorrect root"

      roots = readonly do
        Repository.github_sql.results <<-SQL
          SELECT network.id network_id, repo.id repo_id, network.root_id
          FROM repositories repo
          JOIN repository_networks network
          ON repo.source_id = network.id
          WHERE repo.active = 1
          AND repo.parent_id IS NULL
          AND network.root_id <> repo.id
        SQL
      end

      log "  * Found #{roots.size} networks with incorrect root"

      roots.each do |network_id, repo_id, root_id|
        debug "    * Network #{network_id} refers to incorrect root #{root_id}"
      end

      return unless clean? && roots.size > 0

      log "  * Updating #{roots.size} networks to repair their root references"

      count = 0
      Repository.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        roots.each do |network_id, repo_id, root_id|
          repo = Repository.find(repo_id)
          update_root_to(repo)
          count += 1
        end
      end

      log "   * Updated #{count} networks to repair their root references"
    end

    def remove_empty_networks
      log "* Scanning for empty networks"
      network_ids = readonly do
        RepositoryNetwork.github_sql.values <<-SQL
          SELECT network.id
          FROM repository_networks network
          LEFT JOIN repositories root
          ON network.root_id = root.id
          WHERE root.id IS NULL
          OR root.active = 0
          OR root.source_id <> network.id
        SQL
      end

      log "  * Found #{network_ids.size} empty networks"

      network_ids.each do |network_id|
        debug "    * Network #{network_id} is empty"
      end

      return unless clean? && network_ids.size > 0

      log "  * Removing #{network_ids.size} empty networks"

      count = 0
      ::RepositoryNetwork.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        network_ids.each do |network_id|
          network = ::RepositoryNetwork.find_by_id(network_id)
          if network && network.repositories.empty?
            debug "    * Deleting empty network #{network_id}"
            network.destroy
            count += 1
          end
        end
      end

      log "  * Removed #{count} empty networks"
    end

    def update_root_to(new_root)
      old_root_id = new_root.network.root_id
      debug "    * Repository #{new_root.id} (#{new_root.name_with_owner}) elected as root of network #{new_root.network_id} (old root #{old_root_id})"

      return unless clean?

      new_root.update_attribute :parent_id, nil
      new_root.network.update_attribute :root_id, new_root.id
      forks = new_root.network.repositories.select { |repo| repo.parent_id == old_root_id }
      forks.each { |repo| repo.update_attribute :parent_id, new_root.id }
    end
  end
end
