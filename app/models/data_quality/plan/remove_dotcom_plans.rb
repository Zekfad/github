# rubocop:disable Style/FrozenStringLiteralComment

module DataQuality::Plan
  class RemoveDotcomPlans < ::DataQuality::Script
    def run
      return unless GitHub.enterprise?

      log "* Found #{users_with_bad_plans.size} users with dotcom plans"

      return unless clean? && users_with_bad_plans.size > 0

      log "* Moving #{users_with_bad_plans.size} users to enterprise plans"

      users_with_bad_plans.each do |user_id|
        User.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          sql = User.github_sql.new <<-SQL, user_id: user_id
            UPDATE users
            SET plan = NULL
            WHERE id = :user_id
          SQL

          sql.run
        end
      end
    end

    def users_with_bad_plans
      @users_with_bad_plans ||= \
        User.github_sql.results <<-SQL, plans: ["enterprise", "free"]
          SELECT id FROM users
          WHERE plan NOT IN :plans AND plan IS NOT NULL
        SQL
    end
  end
end
