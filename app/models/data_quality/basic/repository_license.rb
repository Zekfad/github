# rubocop:disable Style/FrozenStringLiteralComment

module DataQuality::Basic
  # Scans and detects open-source licenses in public repositories which don't
  # currently have a valid license.
  #
  # Contact the @github/open-source-licensing team with any questions.
  class RepositoryLicense < ::DataQuality::Script

    SCAN_BATCH_SIZE = 10_000
    BATCH_SIZE = 1000

    def run
      count = 0
      log "Scanning repositories without licenses..."

      max_id = Repository.github_sql.value "SELECT MAX(id) FROM repositories"
      0.step(max_id, SCAN_BATCH_SIZE) do |start|
        percent = (start / max_id.to_f) * 100
        status "%d..%d (%.2f%%)...", start, start+SCAN_BATCH_SIZE, percent

        readonly do
          sql = Repository.github_sql.new <<-SQL, start: start, end: start + SCAN_BATCH_SIZE
            SELECT repositories.id
            FROM repositories
            LEFT JOIN repository_licenses ON repository_licenses.repository_id = repositories.id
            WHERE (repository_licenses.license_id IS NULL
                   OR repository_licenses.license_id = 0)
            AND repositories.public = 1
            AND repositories.id >= :start
            AND repositories.id < :end
          SQL

          ids = sql.values
          count += ids.length
          next if ids.empty?

          scan_repos ids
        end
      end

      log "Scanned #{count} repositories with missing licenses"
    end

    def scan_repos(ids)
      ids.each_slice(BATCH_SIZE) do |repo_ids|

        repos = readonly do
          Repository.where(id: repo_ids).includes(:owner).all
        end

        ActiveRecord::Base.connected_to(role: :writing) do
          repos.each do |repo|
            throttle do
              begin
                set_license(repo)
              rescue GitRPC::RepositoryOffline
                # don't care, continue
              end
            end
          end
        end

      end
    end

    def set_license(repo)
      if clean?
        debug "* Setting license on #{repo.name_with_owner} (#{repo.id})"
        repo.set_license
      else
        debug "* Would set license on #{repo.name_with_owner} (#{repo.id})"
      end
    end

  end

end
