# rubocop:disable Style/FrozenStringLiteralComment

# Run this in a production console (use master if cleaning):
#
#   ActiveRecord::Base.logger = nil
#   runner = DataQuality::Basic::IssueAssignmentSync.new(clean: false, verbose: true)
#   runner.run
#
module DataQuality::Basic

  # Performs the following checks on issue assignments:
  #
  # * Only issues with multi-assignment repos should have more than one assignment
  # * Issue#assignee_id should match one related assignment
  # * Assignments shouldn't be orphaned from issues
  #
  class IssueAssignmentSync < ::DataQuality::Script

    def run
      {}.tap do |results|
        results[:unsynced_issues]            = find_unsynced_issues
        results[:issue_orphaned_assignments] = find_issue_orphaned_assignments

        results[:archived_unsynced_issues]            = find_archived_unsynced_issues
        results[:archived_issue_orphaned_assignments] = find_archived_issue_orphaned_assignments

        if clean?
          results[:unsynced_issues_cleaned]            = clean_unsynced_issues(results[:unsynced_issues])
          results[:issue_orphaned_assignments_cleaned] = clean_orphaned_assignments(results[:issue_orphaned_assignments])

          results[:archived_unsynced_issues_cleaned]            = clean_archived_unsynced_issues(results[:archived_unsynced_issues])
          results[:archived_issue_orphaned_assignments_cleaned] = clean_orphaned_assignments(results[:archived_issue_orphaned_assignments], archived: true)
        end
      end
    end

    def find_unsynced_issues
      log "* Scanning for issues with no matching assignments"

      iterator = GitHub::SQL::SparseDataIterator.new \
        model: Issue,
        sql_select: "issues.id AS issue_id, issues.assignee_id",
        sql_conditions: <<-SQL
          issues.assignee_id IS NOT NULL
          AND NOT EXISTS (
            SELECT 1
            FROM assignments
            WHERE assignments.issue_id = issues.id
            AND assignments.assignee_id = issues.assignee_id
          )
          SQL

      unsynced_issues = iterator.all

      log "  * Found #{unsynced_issues.size} unsynced issues"

      if verbose?
        unsynced_issues.each do |issue_id, assignee_id|
          log "    * Issue #{issue_id} is assigned to #{assignee_id} but no matching assignment was found"
        end
      end

      unsynced_issues.map(&:first)
    end

    def find_archived_unsynced_issues
      log "* Scanning for archived issues with no matching assignments"

      iterator = GitHub::SQL::SparseDataIterator.new \
        model: Issue,
        archived: true,
        sql_select: "archived_issues.id AS issue_id, archived_issues.assignee_id",
        sql_conditions: <<-SQL
          archived_issues.assignee_id IS NOT NULL
          AND NOT EXISTS (
            SELECT 1
            FROM archived_assignments
            WHERE archived_assignments.issue_id = archived_issues.id
            AND archived_assignments.assignee_id = archived_issues.assignee_id
          )
          SQL

      unsynced_issues = iterator.all

      log "  * Found #{unsynced_issues.size} archived unsynced issues"

      if verbose?
        unsynced_issues.each do |issue_id, assignee_id|
          log "    * Archived Issue #{issue_id} is assigned to #{assignee_id} but no matching assignment was found"
        end
      end

      unsynced_issues.map(&:first)
    end

    def find_issue_orphaned_assignments
      log "* Scanning for assignments with no matching issue"

      iterator = GitHub::SQL::SparseDataIterator.new \
        model: Assignment,
        sql_select: "assignments.id AS assignment_id, assignments.issue_id",
        sql_joins: "LEFT JOIN issues ON assignments.issue_id = issues.id",
        sql_conditions: "issues.id IS NULL"

      orphaned_assignments = iterator.all

      log "  * Found #{orphaned_assignments.size} assignments with deleted issues"

      if verbose?
        orphaned_assignments.each do |assignment_id, issue_id|
          log "    * Assignment #{assignment_id} references Issue #{issue_id}, which no longer exists."
        end
      end

      orphaned_assignments.map(&:first)
    end

    def find_archived_issue_orphaned_assignments
      log "* Scanning for archived assignments with no matching archived issue"

      iterator = GitHub::SQL::SparseDataIterator.new \
        model: Assignment,
        archived: true,
        sql_select: "archived_assignments.id AS assignment_id, archived_assignments.issue_id",
        sql_joins: "LEFT JOIN archived_issues ON archived_assignments.issue_id = archived_issues.id",
        sql_conditions: "archived_issues.id IS NULL"

      orphaned_assignments = iterator.all

      log "  * Found #{orphaned_assignments.size} archived assignments with no corresponding archived issue"

      if verbose?
        orphaned_assignments.each do |assignment_id, issue_id|
          log "    * Archived Assignment #{assignment_id} references archived Issue #{issue_id}, which no longer exists."
        end
      end

      orphaned_assignments.map(&:first)
    end

    def clean_unsynced_issues(issue_ids)
      affected_rows = 0

      issue_ids.each_slice(1000) do |slice|
        ActiveRecord::Base.connected_to(role: :writing) do
          results = Assignment.github_sql.run(<<-SQL, issue_ids: slice)
            INSERT INTO assignments
              (assignee_id, issue_id, created_at, updated_at)
            SELECT issues.assignee_id,
              issues.id,
              NOW(),
              NOW()
            FROM issues
            WHERE issues.id IN :issue_ids
          SQL

          affected_rows += results.affected_rows
        end
      end

      affected_rows
    end

    def clean_archived_unsynced_issues(issue_ids)
      affected_rows = 0

      issue_ids.each_slice(1000) do |slice|
        ActiveRecord::Base.connected_to(role: :writing) do
          results = Archived::Assignment.github_sql.run(<<-SQL, issue_ids: slice)
            INSERT INTO archived_assignments
              (assignee_id, issue_id, created_at, updated_at)
            SELECT archived_issues.assignee_id,
              archived_issues.id,
              NOW(),
              NOW()
            FROM archived_issues
            WHERE archived_issues.id IN :issue_ids
          SQL

          affected_rows += results.affected_rows
        end
      end

      affected_rows
    end

    def clean_orphaned_assignments(assignment_ids, archived: false)
      affected_rows = 0
      table = Assignment.table_name.dup
      table.prepend("archived_") if archived

      assignment_ids.each_slice(1000) do |slice|
        ActiveRecord::Base.connected_to(role: :writing) do
          # Interpolating the table name is evil, but we can be trusted. Right?
          results = Assignment.github_sql.run <<-SQL, ids: slice
            DELETE FROM #{table} WHERE id IN :ids
          SQL
          affected_rows += results.affected_rows
        end
      end

      affected_rows
    end
  end
end

# Iterate through all of a table's rows matching a given query, in chunks
# of batch_size. Note that this iterates over the entire table in chunks,
# rather than finding batch_size matches with a LIMIT.
#
# Why would you use this? It gets around an edge case that pops up with
# GitHub::SQL::Iterator on large tables with few matching results. Inherits
# from Transition for readonly/throttle goodness.
#
# NOTE: If this pattern is useful elsewhere, we should move it to a more
# sensible file.
class GitHub::SQL::SparseDataIterator < GitHub::Transition
  attr_reader :model, :table_name, :sql_conditions, :sql_variables, :sql_select, :sql_joins, :batch_size, :max

  def initialize(model:, sql_conditions:, sql_variables: nil, sql_joins: "", sql_select: nil, batch_size: 10_000, archived: false)
    @model          = model
    @table_name     = model.table_name.dup
    @table_name.prepend("archived_") if archived
    @sql_select     = sql_select || "#{table_name}.id"
    @sql_joins      = sql_joins
    @sql_conditions = sql_conditions
    @sql_variables  = sql_variables || {}
    @batch_size     = batch_size
    @max            = current_max
  end

  def batch_count
    @batch_count ||= (max / batch_size) + 1
  end

  def current_max
    readonly do
      model.github_sql.new("SELECT max(id) FROM #{table_name}").value || 0
    end
  end

  def each
    0.step(max, batch_size).with_index do |min_id, batch_number|
      max_id = min_id + batch_size
      percent_complete = (batch_number / batch_count.to_f) * 100

      status "Processing batch %d of %d (%2f%%)", batch_number, batch_count, percent_complete

      variables = sql_variables.merge(min_id: min_id, max_id: max_id)
      query = model.github_sql.new(variables).tap do |sql|
        sql.add <<-SQL
          SELECT #{sql_select}
          FROM #{table_name} #{sql_joins}
          WHERE #{sql_conditions}
          AND #{table_name}.id >= :min_id
          AND #{table_name}.id < :max_id
        SQL
      end

      readonly do
        yield(query.results)
      end
    end
  end

  def all
    [].tap do |all_results|
      each do |batch_results|
        all_results.concat batch_results
      end
    end
  end
end
