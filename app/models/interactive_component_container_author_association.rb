# frozen_string_literal: true

class InteractiveComponentContainerAuthorAssociation < AuthorAssociationBase
  alias :container :resource
  def initialize(container:, user:)
    super(resource: container, user: user)
  end

  def mannequin?
    async_mannequin?
  end

  def async_mannequin?
    Promise.resolve(@user.mannequin?)
  end

  def owner?
    async_owner?.sync
  end

  def async_owner?
    container.async_repository.then do |repository|
      repository.async_organization.then do |organization|
        # Only display owner badge for repositories not owned by organizations
        next false if organization
        repository.async_adminable_by?(@user)
      end
    end
  end

  def member?
    async_member?.sync
  end

  def async_member?
    container.async_repository.then do |repository|
      next false unless repository.public?

      repository.async_organization.then do |organization|
        next false unless organization

        # This will be the integration checking, so check directly rather than scoping to visibility
        organization.member?(@user)
      end
    end
  end

  def collaborator?
    async_collaborator?.sync
  end

  def async_collaborator?
    container.async_repository.then do |repository|
      next true if repository.owner_id == @user.id
      repository.async_member?(@user)
    end
  end

  def contributor?
    async_contributor?.sync
  end

  def async_contributor?
    container.async_repository.then do |repository|
      repository.async_contributor?(@user)
    end
  end

  def first_timer?
    async_first_timer?.sync
  end

  def async_first_timer?
    return Promise.resolve(false) unless container.respond_to?(:issue)
    issue = container.issue

    issue.async_pull_request.then do |pull_request|
      next false unless pull_request

      container.async_repository.then do |repository|
        async_contributor?.then do |result|
          next false if result
          async_pull_request_count(@user).then { |count| count == 1 }
        end
      end
    end
  end

  def first_time_contributor?(sitewide: false)
    async_first_time_contributor?(sitewide: sitewide).sync
  end

  def async_first_time_contributor?(sitewide: false)
    return Promise.resolve(false) unless container.respond_to?(:issue)
    issue = container.issue

    issue.async_pull_request.then do |pull_request|
      next false unless pull_request

      container.async_repository.then do |repository|
        async_contributor?.then do |result|
          next false if result
          async_contributor_pull_request_count(repository: repository, user_id: @user.id).then { |count| count > 0 }
        end
      end
    end
  end
end
