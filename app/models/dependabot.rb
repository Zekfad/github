# frozen_string_literal: true

# The Dependabot module collects service-specific jobs and helpers
module Dependabot
  include GitHub::AreasOfResponsibility
  areas_of_responsibility :dependabot

  # This configuration pattern is set in our installation trigger
  # in stafftools:
  #   https://admin.github.com/stafftools/automatic-apps
  #
  # If you change this pattern, please update the trigger as well!
  CONFIG_FILE_PATH_PATTERN = /\A\.github\/dependabot.ya?ml\z/

  TEMPLATE = <<~YAML.freeze
    # To get started with Dependabot version updates, you'll need to specify which
    # package ecosystems to update and where the package manifests are located.
    # Please see the documentation for all configuration options:
    # https://help.github.com/github/administering-a-repository/configuration-options-for-dependency-updates

    version: 2
    updates:
      - package-ecosystem: "" # See documentation for possible values
        directory: "/" # Location of package manifests
        schedule:
          interval: "daily"
  YAML

  # Checks if a given file path matches Dependabot's expected file path
  def self.recognized_config_path?(path:)
    path =~ CONFIG_FILE_PATH_PATTERN
  end

  # Notifies Dependabot that a repository is now using the Dependency Graph
  def self.dependency_graph_initialized(repository:)
    return unless repository.vulnerability_alerts_enabled?

    # If Dependabot is not available, we should just dispatch the alert update and return
    unless GitHub.dependabot_enabled? && GitHub.dependabot_github_app
      UpdateRepositoryVulnerabilityAlertsJob.enqueue_for_repository(repository)
      return
    end

    automatic_install_check = AutomaticInstallationCheck.new(repository)
    if automatic_install_check.should_install?
      # The Security Alerts jobs will be called once the install is completed.
      AutomaticAppInstallation.trigger(
        type: :dependency_graph_initialized,
        originator: repository,
        actor: repository.owner,
      )

      GitHub.dogstats.increment("dependabot.manifest_detection.install.created")
    else
      if automatic_install_check.should_enable_existing_install?
        repository.enable_vulnerability_updates(actor: GitHub.dependabot_github_app.bot,
                                                enroll: false)
      end

      # Delegate to the Security Alerts job to update the Repository's alerts.
      UpdateRepositoryVulnerabilityAlertsJob.enqueue_for_repository(repository)
    end
  end

  # Initializes Dependabot for the Dependabot security updates feature
  # specifically.
  def self.enroll_for_security_updates(repository:)
    if repository.dependabot_installed?
      # If Dependabot is already installed, we just need to enroll it
      # so we build PRs for any open alerts
      Dependabot::RepositoryEnrollJob.perform_later(repository.id)
    else
      # Otherwise, we need to trigger an installation which will perform
      # the enroll as a callback.
      AutomaticAppInstallation.trigger(
        type: :automatic_security_updates_initialized,
        originator: repository,
        actor: repository.owner,
      )
    end
  end

  # Initilizes Dependabot for a one-off update.
  def self.enroll_for_single_update(dependency_update:)
    AutomaticAppInstallation.trigger(
      type: :dependency_update_requested,
      originator: dependency_update,
      actor: dependency_update.repository,
    )
  end

  # These values map to icons in public/images/icons/dependabot/*.svg
  def self.serialize_package_ecosystem(package_ecosystem:)
    case package_ecosystem
    when :NPM then "npm"
    when :BUNDLER then "bundler"
    when :COMPOSER then "composer"
    when :PIP then "pip"
    when :GOMOD then "gomod"
    when :MAVEN then "maven"
    when :GRADLE then "gradle"
    when :NUGET then "nuget"
    when :CARGO then "cargo"
    when :MIX then "mix"
    when :DOCKER then "docker"
    when :TERRAFORM then "terraform"
    when :GITSUBMODULE then "gitsubmodule"
    when :GITHUB_ACTIONS then "actions"
    when :ELM then "elm"
    else "unknown"
    end
  end

  def self.short_manifest_file_path(full_manifest_path)
    parts = full_manifest_path.split("/")
    short_path = parts.last(2).join("/")
    if parts.count > 2
      short_path.prepend(parts.first, "/…/")
    end
    short_path
  end
end
