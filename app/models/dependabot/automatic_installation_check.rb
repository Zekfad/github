# frozen_string_literal: true

module Dependabot
  class AutomaticInstallationCheck
    include GitHub::AreasOfResponsibility
    areas_of_responsibility :dependabot

    # Dependabot should not be automatically installed on repositories that have
    # installed an application that performs the same or similar function.
    MUTUTALLY_EXCLUSIVE_APPS = {
      dependabot_preview: 2141,
      renovate: 2740,
      greenkeeper: 505,
      depfu: 715,
    }.freeze

    MUTUTALLY_EXCLUSIVE_APP_IDS = MUTUTALLY_EXCLUSIVE_APPS.values

    attr_reader :repository

    def initialize(repository)
      @repository = repository
    end

    def should_install?
      return false if repository.vulnerability_updates_disabled?
      return false if not_required?

      repository_is_eligible? && no_conflicting_installs?
    end

    def should_enable_existing_install?
      return false if repository.vulnerability_updates_disabled?

      repository.dependabot_installed?
    end

    private

    def not_required?
      repository.fork? || repository.dependabot_installed?
    end

    def repository_is_eligible?
      return false if repository.locked? || repository.deleted?

      repository.active? && repository.maintained?
    end

    def no_conflicting_installs?
      IntegrationInstallation.with_repository(repository).
                              where(integration_id: MUTUTALLY_EXCLUSIVE_APP_IDS).
                              none?
    end
  end
end
