# rubocop:disable Style/FrozenStringLiteralComment

# Public: A mixin to use on an ActiveRecord model that both:
#   1. can have Reactions attached to it
#   2. belongs to a Repository
module Reaction::Subject::RepositoryContext
  extend ActiveSupport::Concern
  include Reaction::Subject

  def reaction_admin
    repository.owner
  end

  def async_reaction_admin
    async_repository.then do |repo|
      next false unless repo
      repo.async_owner
    end
  end

  def reaction_path
    async_reaction_path.sync
  end

  def async_reaction_path
    async_repository.then do |repo|
      repo.async_owner.then do |owner|
        Rails.application.routes.url_helpers.update_repository_reaction_path(owner, repo)
      end
    end
  end
end
