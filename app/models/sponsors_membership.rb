# frozen_string_literal: true

class SponsorsMembership < ApplicationRecord::Collab
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model
  include Workflow
  include GitHub::Validations

  extend GitHub::Encoding
  force_utf8_encoding :featured_description, :banned_reason

  self.ignored_columns = %w(featured)

  class InvalidFiscalHostError < StandardError; end

  EMAIL_WAIT_TIME = 5.minutes
  PROCESSING_WAIT_TIME = 5.minutes
  JOINED_WAITLIST_MATCH_DEADLINE = Date.new(2020, 1, 1)
  MAX_FEATURED_DESCRIPTION_LENGTH = 250
  FISCAL_OPTION_BANK = "bank"
  FISCAL_OPTION_HOST = "host"
  FISCAL_OPTIONS = [FISCAL_OPTION_BANK, FISCAL_OPTION_HOST].freeze
  FISCAL_HOST_BILLING_COUNTRY = {
    open_source_collective: "US",
    software_freedom_conservancy: "US",
    numfocus: "US",
  }.freeze
  FISCAL_HOST_GITHUB_ORGANIZATION = {
    "Open-Source-Collective" => :open_source_collective,
    "conservancy" => :software_freedom_conservancy,
    "numfocus" => :numfocus,
  }.freeze

  # Countries that we can auto accept eligible applicants from, if the feature
  # flag is enabled.
  AUTO_ACCEPT_ENABLED_COUNTRIES = ["CZ", "MX"]
  AUTO_ACCEPTABLE_COUNTRIES = Billing::StripeConnect::Account::LOCAL_PAYOUT_COUNTRIES + AUTO_ACCEPT_ENABLED_COUNTRIES

  enum sponsorable_type: {
    User: 0,
    Organization: 1,
  }

  enum featured_state: {
    disabled: 0, # the owner of the account has not granted permission to be featured
    allowed:  1, # the owner of the account has opted-in to be featured
    active:   2, # this account _is_ featured
  }, _prefix: :featured

  enum fiscal_host: {
    none: 0,
    other: 1,
    open_source_collective: 2,
    software_freedom_conservancy: 3,
    numfocus: 4,
  }, _prefix: true

  belongs_to :survey
  has_one :sponsors_listing, dependent: :destroy
  has_many :survey_answers,
    ->(membership) { where(user_id: membership.sponsorable_id) },
    through:   :survey,
    source:    :answers

  belongs_to :sponsorable, polymorphic: true, required: true
  belongs_to :reviewer, class_name: "User", required: false
  belongs_to :contact_email,
    class_name: "UserEmail"

  has_many :sponsors_memberships_criteria,
    class_name: "SponsorsMembershipsCriterion",
    dependent: :destroy,
    autosave: true
  has_many :automated_criteria, -> { automated },
    class_name: "SponsorsMembershipsCriterion"

  belongs_to :banned_by, class_name: "User", foreign_key: "banned_by_id"

  has_many :staff_notes, as: :notable, dependent: :destroy

  validates :sponsorable_id, uniqueness: { scope: :sponsorable_type }
  validate :validate_contact_email, if: :contact_email_id_changed?
  validates :billing_country, inclusion: {
    in: TradeControls::Countries.currently_unsanctioned.map { |_, alpha2, _| alpha2 },
    allow_nil: true,
    message: "is not a valid country code",
  }
  validates :country_of_residence, inclusion: {
    in: TradeControls::Countries.currently_unsanctioned.map { |_, alpha2, _| alpha2 },
    allow_nil: true,
    message: "is not a valid country code",
  }
  validate :can_be_featured?, if: :featured_state_changed?
  validates :featured_description,
    bytesize: {
      maximum: MAX_FEATURED_DESCRIPTION_LENGTH,
      message: "is too long (maximum is #{MAX_FEATURED_DESCRIPTION_LENGTH} characters)",
    },
    allow_blank: true
  validates :fiscal_host, presence: true
  validates :country_of_residence, presence: true, on: :create
  validate :fiscal_host_name_required
  validate :billing_country_required
  validates :legal_name, presence: true, if: -> { legal_name_was.present? }
  validate :legal_name_change_permitted

  # Used to perform conditional validations
  attr_accessor :fiscal_host_validation_enabled, :billing_country_validation_enabled

  # NOTE: the order of these two callbacks matter so that orgs with
  #       fiscal hosts have their country of residence set correctly
  before_validation :set_billing_country_based_on_fiscal_host
  before_validation :set_country_of_residence_for_orgs, on: :create

  before_create :build_manual_criteria
  after_create_commit :refresh_automated_criteria
  after_create_commit :instrument_creation
  after_create_commit :send_waitlist_confirmation, unless: :auto_acceptable?
  after_commit :enqueue_processing_job, on: :update, if: :submitted?

  CRITERION_SLUGS = SponsorsCriterion::Automated::CHECKS.map(&:slug)
  CRITERIA_ALIASES = Hash[CRITERION_SLUGS.map { |slug| [slug, GitHub::SQL::LITERAL("#{slug}_criteria")] }]
  MEMBERSHIP_CRITERIA_ALIASES = Hash[CRITERION_SLUGS.map { |slug| [slug, GitHub::SQL::LITERAL("#{slug}_membership_criteria")] }]
  scope :for_automated_criterion, ->(slug:, met:) do
    raise ArgumentError unless CRITERION_SLUGS.include?(slug)

    query_args = {
      slug:                      slug,
      criteria_alias:            CRITERIA_ALIASES[slug],
      membership_criteria_alias: MEMBERSHIP_CRITERIA_ALIASES[slug],
    }

    join_sql = self.github_sql.new(<<-SQL, **query_args)
      INNER JOIN sponsors_memberships_criteria AS :membership_criteria_alias
      ON :membership_criteria_alias.sponsors_membership_id = sponsors_memberships.id
      INNER JOIN sponsors_criteria AS :criteria_alias
      ON (:criteria_alias.id = :membership_criteria_alias.sponsors_criterion_id AND :criteria_alias.slug = :slug)
    SQL

    joins(join_sql.query).where(query_args[:membership_criteria_alias].value => { met: met })
  end

  scope :with_states, ->(*states) do
    state_values = states.map { |state| state_value(state) }
    where(state: state_values)
  end

  scope :ignored, -> { where.not(ignored_at: nil) }
  scope :not_ignored, -> { where(ignored_at: nil) }

  # Public: Constructs a new instance of SponsorsMembership using a fiscal host
  #
  # Returns a SponsorsMembership
  def self.new_with_fiscal_host(attrs)
    fiscal_host_billing_country = attrs.delete(:fiscal_host_billing_country)

    new(attrs).tap do |membership|
      if membership.fiscal_host_other?
        membership.billing_country = fiscal_host_billing_country
      end

      membership.fiscal_host_validation_enabled = true
      membership.billing_country_validation_enabled = true
    end
  end

  # Public: Constructs a new instance of SponsorsMembership that uses its own bank
  #
  # Returns a SponsorsMembership
  def self.new_with_bank(attrs)
    attrs.delete(:fiscal_host_billing_country)
    attrs.delete(:fiscal_host_name)

    new(attrs.merge(fiscal_host: :none)).tap do |membership|
      membership.billing_country_validation_enabled = true
    end
  end

  # Public: Updates the automated criteria for this membership.
  #
  # Returns a Boolean.
  def refresh_automated_criteria
    SponsorsCriterion::Automated.refresh_for(sponsors_membership: self)
  end

  # Get the Integer value matching a certain state.
  def self.state_value(name)
    workflow_spec.states[name.to_sym]&.value
  end

  workflow :state do
    state :submitted, 0 do
      event :accept, transitions_to: :accepted, if: :country_supported?
      event :deny, transitions_to: :denied
      event :request_manual_review, transitions_to: :pending
      event :ban, transitions_to: :banned
    end

    state :accepted, 1 do
      event :deny, transitions_to: :denied
      event :revert_to_submitted, transitions_to: :submitted, if: :sponsorable_has_no_sponsors_listing?
      event :ban, transitions_to: :banned
    end

    state :pending, 2 do
      event :accept, transitions_to: :accepted, if: :country_supported?
      event :deny, transitions_to: :denied
      event :ban, transitions_to: :banned
    end

    state :denied, 3 do
      event :appeal, transitions_to: :appealed, if: :appeal_allowed?
      event :accept, transitions_to: :accepted, if: :country_supported?
      event :ban, transitions_to: :banned
    end

    state :appealed, 4 do
      event :accept, transitions_to: :accepted, if: :country_supported?
      event :deny, transitions_to: :denied
      event :ban, transitions_to: :banned
    end

    state :banned, 5 do
      event :un_ban, transitions_to: :submitted
      event :ban, transitions_to: :banned
    end

    on_transition do |from, to, event, *args, **kwargs|
      touch(:reviewed_at)
    end
  end

  # Public: Save a list of answers to survey questions as part of Sponsors membership.
  #
  # answers - Array of Hashes in the form:
  #          [{:question_id => 1, :choice_id => 2, :other_text => ""}, ...]
  #
  # fiscal_host_answer - Optional String that represents the fiscal host name
  #
  # Returns nothing.
  def save_with_survey_answers(answers)
    return false unless valid?
    return save if sponsorable.user? && survey_answers.exists?
    return save if sponsorable.organization? && fiscal_host_name_survey_answer && survey_answers.count > 1

    answers ||= []

    survey_answers = answers.map do |answer|
      SurveyAnswer.new \
        user_id: self.sponsorable_id,
        survey_id: self.survey_id,
        question_id: answer[:question_id],
        choice_id: answer[:choice_id],
        other_text: answer[:other_text]
    end

    if Rails.development? || (survey_answers.size > 0 && survey_answers.all?(&:valid?))
      transaction do
        survey_answers.each(&:save!)
        save
      end
    else
      errors.add(:base, "all questions are required")
      false
    end
  end

  def eligible_for_stripe_connect?
    return false unless billing_country.present?

    Billing::StripeConnect::Account
      .supported_countries_for(sponsorable: sponsorable)
      .include?(billing_country)
  end

  # Public: The contact email address to use for this membership.
  #
  # Returns a String.
  def contact_email_address
    async_contact_email_address.sync
  end

  # Public: The contact email address to use for this membership.
  #
  # Returns a Promise<String>.
  def async_contact_email_address
    async_sponsorable.then do |sponsorable|
      if sponsorable.organization? && sponsorable.billing_email.present?
        next sponsorable.billing_email
      end

      async_contact_email.then do |user_email|
        user_email.email
      end
    end
  end

  def full_billing_country
    return unless billing_country
    country_info = Braintree::Address::CountryNames.find do |_, alpha2, _, _|
      alpha2 == billing_country
    end
    TradeControls::Country.from_braintree(country_info).name
  end

  def joined_waitlist_before_match_deadline?
    created_at.before? JOINED_WAITLIST_MATCH_DEADLINE
  end

  def ignore!
    touch(:ignored_at)
  end

  def unignore!
    update!(ignored_at: nil)
  end

  def ignored?
    ignored_at.present?
  end

  # Public: Indicates if this membership has unmet automated health checks.
  #
  # Returns a Boolean.
  def has_unmet_automated_criteria?
    sponsors_memberships_criteria.automated.where(met: false).exists?
  end

  # Public: Indicates if this membership is eligible to be auto-accepted.
  #
  # Returns a Boolean.
  def auto_acceptable?
    return false unless auto_acceptance_enabled?
    return false unless AUTO_ACCEPTABLE_COUNTRIES.include?(billing_country)
    return false unless AUTO_ACCEPTABLE_COUNTRIES.include?(country_of_residence)
    return false if ignored?
    return false if sponsorable.spammy?
    return false if sponsorable.has_any_trade_restrictions?
    return true if sponsorable.organization?

    !has_unmet_automated_criteria?
  end

  # Public: Indicates if the ability to auto accept an application is enabled.
  def auto_acceptance_enabled?
    GitHub.flipper[:sponsors_automated_acceptance].enabled?
  end

  # Override fiscal_host setter so we can raise our own exception.
  def fiscal_host=(value)
    super(value)
  rescue ArgumentError => e
    raise ::SponsorsMembership::InvalidFiscalHostError.new(e.message)
  end

  # Saves the fiscal host name into a SurveyAnswer when the user chooses `other` for their fiscal host
  def fiscal_host_name=(name)
    return if sponsorable.user?
    fiscal_host_name_survey_answer.update(other_text: name)
  end

  def fiscal_host_name
    return if sponsorable.user?
    fiscal_host_name_survey_answer.other_text.to_s
  end

  def unsupported_fiscal_host?
    fiscal_host_none? || fiscal_host_other?
  end

  def set_billing_country_based_on_fiscal_host
    return unless FISCAL_HOST_BILLING_COUNTRY.key?(fiscal_host&.to_sym)
    self.billing_country = FISCAL_HOST_BILLING_COUNTRY[fiscal_host.to_sym]
  end

  def set_country_of_residence_for_orgs
    return unless sponsorable&.organization?
    self.country_of_residence = self.billing_country
  end

  def survey_answers_without_fiscal_host
    survey_answers
      .joins(:question)
      .where.not(survey_questions: { short_text: Sponsors::OrganizationWaitlistSurvey::FISCAL_HOST_QUESTION_SLUG })
  end

  def legal_name_changeable?
    listing = sponsorable.sponsors_listing
    return true if listing.nil?
    !listing.docusign_completed?
  end

  def possible_emails
    return UserEmail.none unless sponsorable&.user?
    sponsorable.emails.user_entered_emails.verified
  end

  private

  def fiscal_host_name_survey_answer
    return if sponsorable.user?
    @fiscal_host_name_survey_answer ||= begin
      question = SurveyQuestion.includes(:choices).where(
        survey_id: self.survey_id,
        short_text: Sponsors::OrganizationWaitlistSurvey::FISCAL_HOST_QUESTION_SLUG,
      ).first

      answer_params = {
        user_id: self.sponsorable_id,
        survey_id: self.survey_id,
        question_id: question.id,
        choice_id: question.choices.first.id,
      }

      answer = SurveyAnswer.find_by(answer_params)

      unless answer
        ActiveRecord::Base.connected_to(role: :writing) do
          answer = SurveyAnswer.create(answer_params)
        end
      end

      answer
    end
  end

  def fiscal_host_name_required
    return unless fiscal_host_validation_enabled

    errors.add(:fiscal_host, "must be provided") if fiscal_host_none?
    errors.add(:fiscal_host_name, "must be provided") if fiscal_host_other? && fiscal_host_name.blank?
  end

  def billing_country_required
    return unless billing_country_validation_enabled
    errors.add(:billing_country, "must be provided") if billing_country.blank?
  end

  # Private: Builds the manual criteria records when creating a membership.
  #
  # Returns nothing.
  def build_manual_criteria
    criteria = SponsorsCriterion.for(sponsorable).manual

    criteria.each do |criterion|
      self.sponsors_memberships_criteria.build(sponsors_criterion: criterion)
    end
  end

  # Private: Called when calling `accept!` for state transition.
  def accept(*args, **kwargs)
    if sponsorable.has_any_trade_restrictions?
      return halt "Trade restricted users are not eligible for GitHub Sponsors"
    end

    OnboardSponsorableJob.set(wait: EMAIL_WAIT_TIME).perform_later(self)
    nil
  end

  # Private: Called when calling `ban!` for state transition.
  def ban(banned_by:, banned_reason:)
    # If the sponsorable has a listing, it must be disabled, which also cancels active subscriptions
    listing = sponsorable.sponsors_listing
    if listing && !listing&.disabled?
      return halt "Unable to disable Sponsors listing" unless listing.disable!(disabled_by: banned_by)
    end

    unless update_columns(banned_by_id: banned_by.id, banned_at: Time.zone.now, banned_reason: banned_reason)
      return halt "Unable to ban Sponsors membership: #{errors.full_messages.join(", ")}"
    end

    actor_hash = if banned_by.staff?
      GitHub.guarded_audit_log_staff_actor_entry(banned_by)
    else
      {actor: banned_by}
    end

    GitHub.instrument("sponsors_memberships_ban.create", actor_hash.merge({
      banned_reason: banned_reason,
      user: sponsorable,
    }))

    nil
  end

  # Private: Called when calling "un_ban!" for state transition.
  def un_ban(un_banned_by:)
    # If the sponsorable has a listing, it must be re-enabled
    if listing = sponsorable.sponsors_listing
      return halt "Unable to re-enable sponsors listing" unless listing.re_enable!
    end

    update!(banned_by: nil, banned_at: nil, banned_reason: nil)

    actor_hash = if un_banned_by.staff?
      GitHub.guarded_audit_log_staff_actor_entry(un_banned_by)
    else
      {actor: un_banned_by}
    end

    GitHub.instrument("sponsors_memberships_ban.destroy", actor_hash.merge({
      user: sponsorable,
    }))

    nil
  end

  # Private: Instrument the creation of a membership for the audit log and hydro.
  def instrument_creation
    GitHub.instrument("sponsors.waitlist_join", {
      user: sponsorable,
    })

    GlobalInstrumenter.instrument("sponsors.waitlist_join", {
      user: sponsorable,
    })
  end

  # Private: Queues a job to email a confirmation to the applicant.
  def send_waitlist_confirmation
    SponsorsMailer.waitlist_confirmation(
      sponsorable: sponsorable,
      waitlist_title: survey.title,
    ).deliver_later
  end

  def validate_contact_email
    return if contact_email.blank?
    return if sponsorable.blank?

    if sponsorable.organization?
      errors.add(:contact_email, "must be not be set for a sponsorable organization")
    elsif !sponsorable.emails.user_entered_emails.verified.include?(contact_email)
      errors.add(:contact_email, "must be verified and belong to sponsorable")
    end
  end

  # Private: Used during `accept` state transition to ensure orgs have a supported country
  def country_supported?
    return true if sponsorable.user?
    eligible_for_stripe_connect?
  end

  # Private: Enqueues a job that decides if this membership should be auto-accepted.
  def enqueue_processing_job
    return unless auto_acceptance_enabled?
    return unless billing_country_previously_changed?
    ProcessSponsorsApplicationJob.set(wait: PROCESSING_WAIT_TIME).perform_later(self)
  end

  def can_be_featured?
    return unless featured_active?
    return if featured_state_was == "allowed" || new_record?
    errors.add(:base, "cannot be featured without the owner's approval")
  end

  def sponsorable_has_no_sponsors_listing?
    sponsorable.sponsors_listing.nil?
  end

  def legal_name_change_permitted
    return if new_record? || !legal_name_changed? || legal_name_changeable?
    errors.add(:legal_name, "cannot be changed")
  end
end
