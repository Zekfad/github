# frozen_string_literal: true

class Codespace < ApplicationRecord::Domain::Codespaces
  include Instrumentation::Model

  # TODO: Rename workspaces table to codespaces
  self.table_name = "workspaces"

  NAME_SUFFIX_LENGTH = 4

  MAX_NAME_GENERATION_ATTEMPTS = 100

  # The name must be a valid domain name label, in both format and length.
  NAME_ALLOWED_CHARS_REGEX = /[a-z0-9\-]/i
  # It can contain "-"s but cannot start or end with them.
  NAME_DISALLOWED_BOUNDARY_CHARS_REGEX = /\A-|-\z/
  MAX_NAME_LENGTH = 63
  NAME_REGEX = /\A[^\-]#{NAME_ALLOWED_CHARS_REGEX}{1,#{MAX_NAME_LENGTH}}[^\-]\z/

  belongs_to :repository, class_name: "::Repository"
  belongs_to :owner, class_name: "::User"
  belongs_to :pull_request, class_name: "::PullRequest"
  belongs_to :plan, class_name: "Codespaces::Plan", validate: true, autosave: true
  has_one :billing_entry,
    validate: true,
    class_name: "Codespaces::BillingEntry",
    foreign_key: :codespace_guid,
    primary_key: :guid

  enum state: %i{
    pending
    provisioning
    provisioned
    failed
  }

  validates :repository, :owner, presence: true
  validates :name, :ref, :oid, presence: true
  validates :plan, presence: true, if: -> { provisioned? }
  validates :guid, length: { is: 36 }, allow_nil: true
  validates :name, format: { with: NAME_REGEX }
  validates :oid, length: { is: 40 }
  validates :location, presence: true
  validates :billing_entry, presence: true, if: -> { provisioned? }

  validate :ensure_owner_is_user, on: :create
  validate :ensure_owner_can_create_codespace, on: :create
  validate :ensure_repo_is_pushable, on: :create
  validate :enforce_per_user_limit, on: :create
  validate :ensure_valid_state_for_guid

  # must come last since other callbacks set attributes used to
  # generate the codespace name.
  before_validation :generate_unique_name, on: :create
  before_validation :set_initial_last_used_at, on: :create
  before_validation :set_codespace_billing_entry, if: -> { provisioned? }

  after_commit :instrument_creation, on: :create
  after_commit :enqueue_deprovision_environment_job, on: :destroy

  scope :by_recently_used, -> { order(last_used_at: :desc) }

  delegate :vscs_target, to: :plan, allow_nil: true

  # Public: Scopes codespaces to only those the viewer has at least pull
  # access on the associated repository.
  #
  # WARNING: This scope should only be called on already narrowly scoped ActiveRelation chains.
  # When multiple conditions are used, add this last in the query chain to limit the scope
  # of the additional queries needed.
  #
  # viewer - The user to scope the codespaces for.
  #
  # Returns an ActiveRelation scope.
  def self.visible_to(viewer)
    target_repository_ids = pluck(:repository_id)
    associated_ids = viewer.associated_repository_ids(repository_ids: target_repository_ids)
    unassociated_ids = target_repository_ids - associated_ids

    visible_repository_ids = Repository.active.public_scope.with_ids(unassociated_ids).
      or(Repository.active.with_ids(associated_ids)).
      pluck(:id)

    if visible_repository_ids.present?
      where(repository_id: visible_repository_ids)
    else
      none
    end
  end

  # Public: Deprovisions the environment for the given codespace.
  #
  # WARNING: This method makes an external API call and should not be called during a request.
  #
  # Returns nothing.
  def self.deprovision_environment(codespace_id, plan, environment_id, owner)
    client = Codespaces::VscsClient.new \
      vscs_target: plan.vscs_target,
      plan_id: plan.vscs_id,
      user: owner,
      resource_provider: plan.resource_provider
    client.delete_environment(environment_id)

    billing_entry = Codespaces::BillingEntry.find_by(
      codespace_guid: environment_id,
      codespace_plan_name: plan.name
    )
    billing_entry.update(codespace_deprovisioned_at: Time.now) if billing_entry

    GitHub.instrument "codespaces.deprovision_environment", \
      owner_id: owner.id,
      owner: owner.login,
      codespace_id: codespace_id,
      plan_id: plan.id,
      environment_id: environment_id
  end

  # TODO: remove this once all codespaces have been backfilled
  def last_used_at
    super || created_at
  end

  # Public: update the records `last_used_at` timestamp. Makes
  # sure we're connected to a 'write' connection since we sometimes
  # update this during a GET request.
  def mark_used!
    ActiveRecord::Base.connected_to(role: :writing) do
      touch(:last_used_at)
    end
  end

  def to_param
    name
  end

  def pinned_to_commit?
    ref == oid && commit
  end

  def commit
    @commit ||= repository.commits.find(oid)
  rescue GitRPC::ObjectMissing
    nil
  end

  def moniker
    return unless persisted? && repository

    if pull_request
      pull_request.permalink
    elsif pinned_to_commit?
      commit.permalink
    elsif ref != repository.default_branch
      "#{repository.permalink}/tree/#{ref}"
    else
      repository.permalink
    end
  end

  def vscs_target_config
    GitHub::Config::VSCS_ENVIRONMENTS[vscs_target]
  end

  def writable_by?(user)
    # TODO: This logic likely needs to check `pushable_by?(user)`
    # but doing so broke existing codespace access since we check
    # this in the controller `verify_authorization` hook.
    user == owner && repository&.pullable_by?(user)
  end

  def readable_by?(user)
    writable_by?(user)
  end

  def ref_for_display
    Git::Ref.value_for_display(ref) if ref
  end

  def stuck_provisioning?
    provisioning? && updated_at < 5.minutes.ago
  end

  private

  def instrument_creation
    GlobalInstrumenter.instrument("codespaces.created", codespace: self)
    instrument :create
  end

  # Do our best to generate a unique name for a codespace with the given
  # properties, ensuring it fits within the max length for a name.
  def generate_unique_name(attempt: 1)
    return if name.present? # don't override a preset name
    return unless repository && owner && ref # don't bother if we're not in a valid state

    raise ArgumentError, "Max attempts exceeded" if attempt > MAX_NAME_GENERATION_ATTEMPTS

    template = "%{codespace_owner}-%{repo_owner}-%{repo_name}-%{suffix}"

    segments = {
      codespace_owner: owner.login,
      repo_owner: owner == repository.owner ? "" : repository.owner.login,
      repo_name: repository.name,
      suffix: SecureRandom.alphanumeric(NAME_SUFFIX_LENGTH),
    }
    segments.each do |key, value|
      segments[key] = value.gsub(/[^#{NAME_ALLOWED_CHARS_REGEX}]/i, "-")
        .downcase
        .gsub(NAME_DISALLOWED_BOUNDARY_CHARS_REGEX, "")
    end

    candidate = format_with_max_length(template: template, segments: segments, max_length: MAX_NAME_LENGTH).squeeze("-")

    self.name = if Codespace.where(name: candidate).exists?
      generate_unique_name(attempt: attempt + 1)
    else
      candidate
    end

    self.slug = self.name
  end

  def format_with_max_length(template:, segments:, max_length:)
    untrimmed = template % segments
    return untrimmed if untrimmed.length <= max_length

    chars_to_trim = untrimmed.length - max_length
    trimmed_segments = segments.to_a

    # trim 5 character off the longest segment until we're at the target
    until chars_to_trim <= 0
      trimmed_segments.sort_by! { |(k, v)| -v.length }
      trimmed_segments.first[1] = trimmed_segments.first[1][0...-5]
      chars_to_trim -= 5
    end

    template % Hash[trimmed_segments]
  end

  def set_initial_last_used_at
    self.last_used_at ||= Time.current
  end

  def set_codespace_billing_entry
    return if billing_entry&.persisted?

    self.build_billing_entry \
      billable_owner: owner,
      codespace_owner: owner,
      codespace_guid: guid,
      codespace_plan_name: plan&.name,
      codespace_created_at: created_at,
      repository: repository
  end

  def ensure_owner_is_user
    return unless owner

    errors.add(:owner, "must be a user") unless owner.try(:user?)
  end

  def ensure_owner_can_create_codespace
    return unless owner && repository

    errors.add(:repository, "may not be used for a codespace") unless Codespaces::Policy.creatable_by_user?(repository, owner)
  end

  def ensure_repo_is_pushable
    return unless GitHub.flipper[:codespaces_fork_repos].enabled?(owner)
    return unless owner && repository && ref

    # Passing the `ref` here will check a specific ref on the repository which is
    # how you account for `fork_collab_allowed?` to the maintainer from a PR.
    errors.add(:repository, "must be pushable by codespace owner") unless repository.pushable_by?(owner, ref: ref)
  end

  def ensure_valid_state_for_guid
    if guid?
      valid_state = state == "provisioned"
      errors.add(:state, "must be 'provisioned' for codespace with a guid") unless valid_state
    else
      valid_state = %w(pending provisioning failed).include?(state)
      errors.add(:state, "must not be 'provisioned' without a guid") unless valid_state
    end
  end

  def enforce_per_user_limit
    return unless owner&.at_codespace_limit?

    errors.add(:base, "Beta limit of #{GitHub.codespaces_per_user_limit} codespaces reached, in order to create a new one, you must remove one of your existing codespaces")
  end

  def enqueue_deprovision_environment_job
    CodespacesDeprovisionEnvironmentJob.perform_later(codespace_id: id, plan: plan, environment_id: guid, owner: owner) if plan && provisioned?
    billing_entry.update(codespace_deleted_at: Time.now) if billing_entry
    GlobalInstrumenter.instrument("codespaces.destroyed", codespace: self)
    instrument :destroy
  end

  def event_prefix
    "codespaces"
  end

  def event_payload
    {
      codespace_id: id,
      repository_id: repository.id,
      repository: repository.nwo,
      pull_request_id: pull_request&.id,
      owner_id: owner.id,
      owner: owner.login,
      name: name,
      slug: slug,
      oid: oid,
      ref: ref,
      location: location,
    }
  end
end
