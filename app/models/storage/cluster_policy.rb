# rubocop:disable Style/FrozenStringLiteralComment

module Storage
  module Uploadable
    def storage_cluster_url(policy)
      Storage.not_implemented!(policy, self, :storage_cluster_url)
    end

    def storage_download_path_info(policy)
      raise NotImplementedError, "#{self.class} needs #storage_download_path_info(policy)"
    end

    def storage_upload_path_info(policy)
      raise NotImplementedError, "#{self.class} needs #storage_upload_path_info(policy)"
    end

    def storage_cluster_download_token(policy, expires: nil)
      if !policy.actor
        raise ArgumentError, "This #{self.class} storage policy needs an actor to create a token for downloads."
      end

      self.class.storage_auth_token(policy.actor, {
        path_info: storage_download_path_info(policy),
      }, expires: expires)
    end

    def storage_cluster_upload_source_url
      @storage_cluster_upload_source_url
    end

    def storage_cluster_upload_source_url=(value)
      @storage_cluster_upload_source_url = value
    end

    def storage_cluster_upload_token_params
      h = {
        size: size,
        content_type: content_type,
      }

      if u = storage_cluster_upload_source_url
        h[:upload_url] = u
        h[:original_type] = "url"
      end

      h
    end

    def storage_cluster_upload_token(policy, expires: nil)
      if !policy.actor
        raise ArgumentError, "This #{self.class} storage policy needs an actor to create a token for uploads."
      end

      params = storage_cluster_upload_token_params.merge(policy.remote_auth_params)
      params[:path_info] = storage_upload_path_info(policy)
      self.class.storage_auth_token(policy.actor, params, expires: expires)
    end

    def storage_download_content_type
      ctype = try(:content_type)
      ctype.present? ? ctype : "application/octet-stream"
    end
  end

  # This policy defines how files are stored and accessed using the Alambic's
  # legacy filesystem adapter.
  class Storage::ClusterPolicy < Storage::AlambicPolicy
    def download_url(query = nil)
      now = Time.now
      query ||= {}
      url = @uploadable.storage_cluster_url(self)
      token = @uploadable.storage_cluster_download_token(self)
      query.update(token: token) if token
      query_string = query.empty? ? "" : "?#{query.to_query}"
      "#{url}#{query_string}"
    ensure
      stats_timing(:download, start: now) if now
    end

    def download_link
      now = Time.now
      link = {
        href: @uploadable.storage_cluster_url(self),
      }

      if token = @uploadable.storage_cluster_download_token(self)
        link[:header] = {
          "Authorization" => "RemoteAuth #{token}",
        }
      else
        link[:href] += "?token=0"
      end

      link
    ensure
      stats_timing(:download, start: now) if now
    end

    private

    def upload_header
      h = {
        "Accept" => "application/vnd.github.assets+json; charset=utf-8",
      }

      if token = @uploadable.storage_cluster_upload_token(self)
        h["GitHub-Remote-Auth"] = token
        if GitHub.enterprise?
          h["Authorization"] = "RemoteAuth #{token}"
        end
      end

      h
    end
  end
end
