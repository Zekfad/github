# rubocop:disable Style/FrozenStringLiteralComment

class Storage::FileServer < ApplicationRecord::Domain::Storage
  self.table_name = :storage_file_servers

  validates_presence_of :host

  has_many :partitions, class_name: "Storage::Partition", foreign_key: :storage_file_server_id
end
