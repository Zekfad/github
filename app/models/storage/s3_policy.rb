# rubocop:disable Style/FrozenStringLiteralComment

module Storage
  module Uploadable
    def storage_s3_key(policy)
      Storage.not_implemented!(policy, self, :storage_s3_key)
    end

    def storage_s3_download_query(q)
    end

    def storage_s3_access
      :private
    end

    def storage_supports_multi_part_upload
      true
    end

    def storage_s3_upload_header
      {
        "Content-Type" => content_type,
      }
    end

    def storage_download_expiration
      5.minutes
    end

    def storage_upload_expiration
      30.minutes
    end

    def storage_s3_access_key
      GitHub.s3_environment_config[:access_key_id]
    end

    def storage_s3_secret_key
      GitHub.s3_environment_config[:secret_access_key]
    end

    def storage_s3_bucket
      GitHub.s3_environment_config[:asset_bucket_name]
    end

    class CopyError < StandardError
    end

    class RetryError < StandardError
      attr_reader :record_ids
      def initialize(record_ids, *args)
        @record_ids = Array(record_ids).map(&:to_i).select { |id| id > 0 }
        super(*args)
      end
    end

    def self.copy_all_to_provider(faraday, storage_provider, uploadables, batch_limit: 1.gigabyte, batch_size: 10, retry_limit: 3, on_result: nil)
      all = uploadables.dup
      batch = []
      retries = {}
      retry_failed = []
      batch_file_size = 0

      loop do
        while all.present? && batch.size < batch_size && batch_file_size < batch_limit
          record = all.shift
          batch << record
          batch_file_size += record.size
        end
        break if batch.blank?

        body, failed = copy_to_provider(faraday, storage_provider, batch)
        on_result&.call(body) if body.present?

        retriable = []
        failed.each do |f|
          num = retries[f.id.to_s].to_i + 1
          if num > retry_limit
            retry_failed << f.id
          else
            retries[f.id.to_s] = num
            retriable << f
          end
        end

        batch = retriable
        batch_file_size = retriable.sum(&:size)
      end

      if retry_failed.present?
        raise RetryError.new(retry_failed, "Too many retries")
      end
    end

    def self.copy_to_provider(faraday, storage_provider, uploadables)
      return [{}, []] if storage_provider.blank? || uploadables.blank?

      reqs = {requests: {}}
      uploadables.each do |u|
        next unless req = u.build_copy_request(storage_provider)
        reqs[:requests][u.id.to_s] = req
      end

      return [{}, []] if reqs[:requests].empty?

      res = faraday.post do |req|
        req.headers[:content_type] = "application/json"
        req.body = GitHub::JSON.encode(reqs)
      end

      if res.status != 200 || res.headers[:content_type] != "application/json"
        GitHub::Logger.log(policy: "S3Policy",
                           method: __method__,
                           response: res)
        raise CopyError, "storage provider copy failed, HTTP #{res.status}"
      end

      uploadmap = uploadables.index_by { |u| u.id.to_s }
      failed = []
      succeeded = []
      body = JSON.parse(res.body)
      reqs = body["requests"] || {}
      reqs.each do |id, res|
        (res["success"] ? succeeded : failed) << uploadmap[id]
      end

      klass = uploadables.first.class

      # save all the succeeded uploads
      if succeeded.present?
        klass.where(id: succeeded.map(&:id)).update_all(storage_provider: storage_provider.to_s)
      end

      # reset #storage_provider state so these can be retried
      if failed.present?
        [body, klass.where(id: failed.map(&:id)).all]
      else
        [body, failed]
      end
    end

    def build_copy_request(storage_provider)
      return if self.storage_provider == storage_provider

      d = storage_policy.download_url
      self.storage_provider = storage_provider
      u = storage_policy.policy_hash

      {
        source: {href: d},
        destination: {href: u[:upload_url], form: u[:form]},
      }
    end

    module ClassMethods
      def storage_s3_hostname
        "#{storage_s3_bucket}.s3.amazonaws.com"
      end

      def storage_s3_new_bucket_host
        "#{storage_s3_new_bucket}.s3.amazonaws.com"
      end
    end
  end

  # This policy defines how files are stored and accessed using S3 directly.
  class S3Policy < Storage::Policy
    def delete_object
      return if Rails.test? && self.class.faraday.nil?
      path = @uploadable.storage_s3_key(self)
      sign = S3Sign.header(s3_credentials, "DELETE", path, nil)
      self.class.faraday.delete do |req|
        req.url("#{upload_url}/#{path}")
        sign.to_hash.each do |key, value|
          req.headers[key] = value
        end
      end
    end

    def download_url(_ = nil)
      now = Time.now
      url, signer = retrieve_url_and_signer
      url
    ensure
      stats_timing(:download, start: now) if now
    end

    def metadata_url(_ = nil)
      now = Time.now
      url, signer = retrieve_url_and_signer("HEAD")
      url
    ensure
      stats_timing(:metadata, start: now) if now
    end

    def download_link
      now = Time.now
      url, signer = retrieve_url_and_signer
      h = {href: download_url}
      if signer
        h[:expires_at] = signer.expires_at.xmlschema
        h[:expires_in] = signer.expires.to_i
      end

      h
    ensure
      stats_timing(:download, start: now) if now
    end

    def multi_part_upload_url_headers(state)
      sign = {}
      path = @uploadable.storage_s3_key(self)
      case state
      when "starter"
        sign = S3Sign.header(s3_credentials, "POST", path, S3Sign::EMPTY_BODY_HASH)
        sign.query[:uploads] = ""

      when "multipart_upload_started"
        sign = S3Sign.header(s3_credentials, "PUT", path, @uploadable.part_sha)
        sign.query[:partNumber] = @uploadable.part_number
        sign.query[:uploadId] = @uploadable.multi_part_upload_id

      when "multipart_upload_list_parts"
        sign = S3Sign.header(s3_credentials, "GET", path, S3Sign::EMPTY_BODY_HASH)
        sign.query[:uploadId] = @uploadable.multi_part_upload_id

      when "multipart_upload_completed"
        sign = S3Sign.header(s3_credentials, "POST", path, @uploadable.part_sha)
        sign.query[:uploadId] = @uploadable.multi_part_upload_id

      end

      { url: sign.location, headers: sign.to_hash}
    end

    def upload_url
      if b = @uploadable.storage_s3_bucket
        "https://#{b}.s3.amazonaws.com"
      else
        GitHub.s3_asset_bucket_host
      end
    end

    def upload_link
      raise NotImplementedError
    end

    def acl
      case access = @uploadable.storage_s3_access
      when :private then "private"
      when :public then "public-read"
      else
        GitHub::Logger.log(policy: "S3Policy",
                           method: __method__,
                           access: access.inspect,
                           uploadable: @uploadable.inspect)
        raise ArgumentError, "Unknown storage access for #{@uploadable.class} #{@uploadable.id}"
      end
    end

    private

    def retrieve_url_and_signer(method = "GET")
      if @uploadable.storage_s3_access == :public
        return ["#{upload_url}/#{@uploadable.storage_s3_key(self)}", nil]
      end

      expiration = @uploadable.storage_download_expiration.to_i
      sign = S3Sign.query(s3_credentials, method, @uploadable.storage_s3_key(self), expiration)
      sign.query[:actor_id] = @actor ? @actor.id : 0
      sign.query[:repo_id] = @repository.is_a?(Repository) ? @repository.id : 0
      @uploadable.storage_s3_download_query(sign.query)
      [sign.location, sign]
    end

    def upload_form
      object_key = @uploadable.storage_s3_key(self)
      upload_policy = build_policy(object_key)
      json_policy = GitHub::JSON.encode(upload_policy)
      encoded_policy = Base64.encode64(json_policy).gsub("\n", "")
      hmac = OpenSSL::HMAC.digest("sha1", @uploadable.storage_s3_secret_key.to_s, encoded_policy)
      policy_signature = Base64.encode64(hmac).gsub("\n", "")

      form = {
        key: object_key,
        AWSAccessKeyId: @uploadable.storage_s3_access_key.to_s,
        acl: acl,
        policy: encoded_policy,
        signature: policy_signature,
      }

      each_s3_header do |key, value|
        form[key] = value
      end

      form
    end

    def same_origin_upload?
      false
    end

    # Generate the policy document to send to S3 in the file upload form. This
    # allows users to upload files to our bucket. Verifies that the file they're
    # actually uploading matches the content type and file size they claim to be
    # saving to the bucket.
    #
    # The keys in this document must match the fields in the file upload  form
    # exactly. No additional form fields may be included in the post.
    #
    # Returns a base 64 encoded JSON String.
    def build_policy(object_key)
      conditions = [
        {bucket: @uploadable.storage_s3_bucket},
        {key: object_key},
        {acl: acl},
        ["content-length-range", @uploadable.size, @uploadable.size],
      ]

      each_s3_header do |key, value|
        conditions << {key => value}
      end

      expiration = @uploadable.storage_upload_expiration
      {
        expiration: expiration.from_now.utc.iso8601,
        conditions: conditions,
      }
    end

    # Returns a Hash of custom headers applied to the S3 file.
    def each_s3_header
      @uploadable.storage_s3_upload_header.each do |key, value|
        next unless valid_s3_header_key?(key)
        yield key, value
      end
    end

    def valid_s3_header_key?(key)
      S3_REST_HEADERS.include?(key) || key =~ /^x-amz-meta-/
    end

    S3_REST_HEADERS = Set.new(%w(
      Cache-Control
      Content-Type
      Content-Disposition
      Content-Encoding
      Expires))

    def s3_credentials
      {
        bucket: @uploadable.storage_s3_bucket,
        key: @uploadable.storage_s3_access_key,
        secret: @uploadable.storage_s3_secret_key,
      }
    end
  end
end
