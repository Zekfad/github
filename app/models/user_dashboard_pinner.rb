# frozen_string_literal: true

# Pins and unpins items to the user's dashboard (https://github.com)
class UserDashboardPinner
  def self.areas_of_responsibility
    [:user_profile]
  end

  # Public: Removes the given items from user's dashboard.
  #
  # items_to_unpin - one or more Repositories or Gists to unpin from the user's dashboard
  # user - a User
  # viewer - the User who is doing the unpinning
  #
  # Returns nothing.
  def self.unpin(*items_to_unpin, user:, viewer:)
    items = user.remove_unpinned_items_from_list(*items_to_unpin, viewer: viewer)

    self.pin(*items, user: user, viewer: viewer)
  end

  # Public: Adds the given items to a user's dashboard.
  #
  # items_to_pin - one or more Repositories or Gists to pin to the user's dashboard
  # user - a User
  # viewer - the User who is doing the unpinning
  #
  # Returns nothing.
  def self.pin(*items_to_pin, user:, viewer:)
    user.pin_items_to_dashboard(items_to_pin, viewer: viewer)
  end
end
