# frozen_string_literal: true

module ReferenceableContent
  extend ActiveSupport::Concern

  def process_content_references
    GitHub.dogstats.increment("testing.content.has_references", tags: ["object_type:#{referenceable_object_type}"])
    ProcessUnfurlsJob.perform_later(editor || user, self, @unfurl_results[:new_urls], body_version)
  end

  def has_content_references?
    body_changed_after_commit? && has_new_content_references?
  end

  def has_new_content_references?
    GitHub.dogstats.time("content_references_pipeline", tags: ["object_type:#{referenceable_object_type}"]) do
      @unfurl_results = GitHub::Goomba::ContentReferencesPipeline.call(body, async_body_context.sync)
      @unfurl_results[:new_urls] && @unfurl_results[:new_urls].count > 0
    end
  end

  def refresh_body_html
    async_body_html_refresh.sync
    notify_socket_subscribers
  end

  private

  def referenceable_object_type
    self.class.name.underscore.downcase
  end
end
