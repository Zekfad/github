# frozen_string_literal: true

module Coders
  class OauthAccessCoder < Coders::Base
    data_accessors \
      :note,
      :note_url,
      :requested_scopes,
      :scopes

    # track changes to the scopes property other props can be added to
    # the defined_attribute_methods definition when needed
    include ActiveModel::Dirty
    define_attribute_methods :scopes

    # changing the scopes will allow the #changed? and the
    # scopes_changed? method evaluate to true
    def scopes=(vals)
      scopes_will_change! unless vals == scopes
      data[:scopes] = vals
    end
  end
end
