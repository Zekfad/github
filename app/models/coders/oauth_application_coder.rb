# frozen_string_literal: true
require "ostruct"

module Coders
  class OauthApplicationCoder < Coders::Base

    data_accessors :callback_urls, :scopes

  end
end
