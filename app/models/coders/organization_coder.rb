# frozen_string_literal: true

module Coders
  class OrganizationCoder < UserCoder
    data_accessors \
      :destroy_owners_team_attempted_at,
      :migrated_all_legacy_contributors_at,
      :updated_team_privacy_at

    def destroy_owners_team_attempted_at
      time(data[:destroy_owners_team_attempted_at])
    end

    def migrated_all_legacy_contributors_at
      time(data[:migrated_all_legacy_contributors_at])
    end

    def updated_team_privacy_at
      time(data[:updated_team_privacy_at])
    end
  end
end
