# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class HelpHubEligibility

    DOGSTATS_ROOT_VALUE = "billing.help_hub_eligibility"

    def initialize(account:)
      @account = account
    end

    # Public: does this user pay us for lfs, packages, actions, or storage?
    #
    # NOTE: This implementation is to be revisited in the future. Our current
    # definition of "pay us" is:
    #
    # * Has paid us in the last two months for metered billing products, regardless of refunds
    # * Will likely pay us at the end of this cycle based on current usage of metered billing products
    # * Has paid us in the last two months (if monthly), or year (if yearly) for LFS
    # * Currently has at least one data pack
    #
    # Returns a Boolean
    def eligible_based_on_usage_products?
      GitHub.dogstats.time(dogstats_root_value) do
        metered_billing_customer? || lfs_customer?
      end
    end

    private

    attr_reader :account

    # Internal: does this org/user pay us for metered billing products?
    #
    # Returns Boolean
    def metered_billing_customer?
      GitHub.dogstats.time("#{dogstats_root_value}.metered_billing_customer_queries") do
        return true if paid_for_metered_billing_products_in_last_two_months?
        return false unless account.has_valid_payment_method? && account.metered_billing_overage_allowed?
        will_be_billed_at_end_of_period_for_metered_products?
      end
    end

    # Internal: has this org/user paid for lfs is the last two months (or year,
    # if on a yearly cycle), or will they pay for it at their upcoming billing date.
    #
    # Returns Boolean
    def lfs_customer?
      GitHub.dogstats.time("#{dogstats_root_value}.lfs_customer_queries") do
        return true if paid_for_lfs_recently?
        return false unless account.has_valid_payment_method? && account.external_subscription?

        # Will they likely be charged for it at next billing date?
        GitHub.dogstats.time("#{dogstats_root_value}.external_subscription_lfs_check") do
          account.plan_subscription.external_subscription.data_packs.to_i > 0
        end
      end
    end

    # Internal: has this org/user paid for data_packs within the last 2 months,
    # if monthly, or the last year, if yearly.
    #
    # Returns Boolean
    def paid_for_lfs_recently?
      # Lets not give a two year buffer
      GitHub.dogstats.time("#{dogstats_root_value}.recent_lfs_usage") do
        cutoff_date = if account.yearly_plan?
          account.previous_billing_date
        else
          account.previous_billing_date(cycles: 2)
        end

        account.billing_transactions
          .sales
          .settled
          .where("created_at >= ?", cutoff_date)
          .where("asset_packs_total >= ?", 1)
          .exists?
      end
    end

    # Internal: Has this user paid for metered usage in the last two cycles?
    #
    # Returns a Boolean
    def paid_for_metered_billing_products_in_last_two_months?
      GitHub.dogstats.time("#{dogstats_root_value}.recent_metered_billing_usage") do
        cutoff_date = ::GitHub::Billing.now - 2.months

        account.line_items.joins(:billing_transaction)
          .where("billing_transaction_line_items.created_at >= ?", cutoff_date)
          .merge(::Billing::BillingTransaction.sales.settled)
          .usage
          .where("billing_transaction_line_items.amount_in_cents != 0")
          .exists?
      end
    end

    # Internal: will this account be billed for usage at the end of the cycle?
    #
    # Returns a Boolean
    def will_be_billed_at_end_of_period_for_metered_products?
      GitHub.dogstats.time("#{dogstats_root_value}.current_metered_usage_will_be_billed") do
        ::Billing::PackageRegistryUsage.new(account, owner: account).paid_gigabytes_used > 0 ||
        ::Billing::SharedStorageUsage.new(account, owner: account).estimated_monthly_paid_megabytes > 0 ||
        ::Billing::ActionsUsage.new(account, owner: account).total_paid_minutes_used > 0
      end
    end

    def dogstats_root_value
      DOGSTATS_ROOT_VALUE
    end
  end
end
