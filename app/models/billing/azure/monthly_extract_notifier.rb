# frozen_string_literal: true

module Billing
  module Azure
    class MonthlyExtractNotifier
      GITCOIN_REPO_NWO = "github/gitcoin"
      INSTRUCTIONS_LINK = "https://github.com/github/gitcoin/blob/master/docs/technical/Metered%20Billing/azure-manual-process.md#report-overages-to-ms-to-support-billing-ea-customers-a-gitcoin-team-member-before-12pm-pacific-will"
      FIRST_RESPONDER_ASSIGN_LABEL = "assign:first-responder"

      def initialize(exporter:, filename:)
        @exporter = exporter
        @filename = filename
      end

      def notify
        create_issue
      end

      private

      attr_reader :exporter, :filename
      delegate :start_on, :end_on, to: :exporter, private: true

      def create_issue
        repository = Repository.nwo(GITCOIN_REPO_NWO)

        return if repository.nil? # Skip if the configured repository doesn't exist

        repository.issues.create!(
          user: User.staff_user,
          title: "Monthly Azure Extract generated (#{start_on.to_s(:rfc822)} - #{end_on.to_s(:rfc822)})",
          body: issue_body,
          labels: repository.labels.where(name: FIRST_RESPONDER_ASSIGN_LABEL)
        )
      end

      def issue_body
        <<~BODY
          The monthly extract has been created and needs to be added to Azure devops for the Azure team to process.

          Please download the #{filename} from [here](#{GitHub::Application.routes.url_helpers.stafftools_azure_metered_billing_extracts_path}) and follow the instructions [here](#{INSTRUCTIONS_LINK}).

          ---

          - Total customers: #{metadata.total_customers}
          - Customers with overages: #{metadata.customers_with_overages}
          - Customers without overages: #{metadata.customers_without_overages}
          - #{consumption_description_for(label: "Mac Actions", meter_id: Billing::Azure::MeteredBillingExport::ACTIONS_MAC_METER_ID)}
          - #{consumption_description_for(label: "Windows Actions", meter_id: Billing::Azure::MeteredBillingExport::ACTIONS_WINDOWS_METER_ID)}
          - #{consumption_description_for(label: "Linux Actions", meter_id: Billing::Azure::MeteredBillingExport::ACTIONS_LINUX_METER_ID)}
          - #{consumption_description_for(label: "Packages Bandwidth", meter_id: Billing::Azure::MeteredBillingExport::PACKAGES_BANDWIDTH_METER_ID)}
          - #{consumption_description_for(label: "Storage", meter_id: Billing::Azure::MeteredBillingExport::STORAGE_METER_ID)}
        BODY
      end

      def consumption_description_for(label:, meter_id:)
        "Total consumption on #{label} meter (#{meter_id}): #{metadata.total_consumption_for(meter_id: meter_id)}"
      end

      def metadata
        @metadata ||= Billing::Azure::ExtractMetadata.new(exporter)
      end
    end
  end
end
