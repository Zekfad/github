# frozen_string_literal: true

module Billing::Azure
  class ActionsOverageAttributer
    def initialize(included_minutes:, line_items:)
      @included_minutes = included_minutes
      @line_items = line_items

      @usage_calculator = RuntimeUsageCalculator.new(included_minutes: included_minutes)
    end

    def breakdown
      @breakdown ||= calculate_breakdown
    end

    private

    attr_reader :included_minutes, :line_items, :usage_calculator

    def calculate_breakdown
      line_items.reorder(:end_time, :id).find_each do |line_item|
        usage_calculator.add_usage(line_item.duration_in_minutes, line_item.duration_multiplier, line_item.job_runtime_environment)
      end

      usage_calculator.overage_counter
    end

    class RuntimeUsageCalculator
      attr_accessor :minutes_consumed, :overage_counter

      def initialize(included_minutes:)
        @included_minutes = included_minutes
        @overage_counter = Hash.new(0)
        @minutes_consumed = 0
      end

      def add_usage(duration_in_minutes, multiplier, runtime)
        multiplied_minutes = duration_in_minutes * multiplier
        self.minutes_consumed += multiplied_minutes

        if over_included_limit?
          overage_minutes = minutes_consumed - included_minutes
          overage_counter[runtime] += [multiplied_minutes, overage_minutes].min / multiplier
        end
      end

      private

      attr_reader :included_minutes

      def over_included_limit?
        minutes_consumed > included_minutes
      end
    end
    private_constant :RuntimeUsageCalculator
  end
end
