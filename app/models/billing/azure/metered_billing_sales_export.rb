# frozen_string_literal: true

module Billing::Azure
  class MeteredBillingSalesExport
    HEADERS = ["account id", "actions spend", "registry spend", "storage spend", "start date", "end date"]

    ACTIONS_METER_IDS = [
      Billing::Azure::MeteredBillingExport::ACTIONS_LINUX_METER_ID,
      Billing::Azure::MeteredBillingExport::ACTIONS_WINDOWS_METER_ID,
      Billing::Azure::MeteredBillingExport::ACTIONS_MAC_METER_ID,
    ]

    def initialize(start_on:, end_on:)
      @start_on = start_on
      @end_on = end_on
    end

    def to_csv
      @_csv ||= CSV.generate(headers: HEADERS, write_headers: true) do |csv|
        azure_export
          .group_by { |record| record["subscriptionId"] }
          .map { |subscription_id, rows| convert_rows_to_csv_row(subscription_id, rows) }
          .compact
          .each { |row| csv << row }
      end
    end

    private

    attr_reader :start_on, :end_on

    def convert_rows_to_csv_row(azure_subscription_id, azure_rows)
      business = Billing::EnterpriseAgreement.find_by(azure_subscription_id: azure_subscription_id).business

      [
        business.id,
        actions_cost_from_rows(azure_rows),
        package_registry_cost_from_rows(azure_rows),
        shared_storage_cost_from_rows(azure_rows),
        start_on.strftime("%D"),
        end_on.strftime("%D"),
      ]
    end

    def actions_cost_from_rows(rows)
      Billing::Money.new(
        (linux_actions_cost(rows) + windows_actions_cost(rows) + mac_actions_cost(rows)) * actions_cost_in_cents,
      )
    end

    def linux_actions_cost(rows)
      row = rows.find { |row| row["meterId"] == Billing::Azure::MeteredBillingExport::ACTIONS_LINUX_METER_ID }

      if row.present?
        row.fetch("quantity", 0) * Billing::ActionsUsageLineItem.billable_rate_for(runtime: :UBUNTU)
      else
        0
      end
    end

    def windows_actions_cost(rows)
      row = rows.find { |row| row["meterId"] == Billing::Azure::MeteredBillingExport::ACTIONS_WINDOWS_METER_ID }

      if row.present?
        row.fetch("quantity", 0) * Billing::ActionsUsageLineItem.billable_rate_for(runtime: :WINDOWS)
      else
        0
      end
    end

    def mac_actions_cost(rows)
      row = rows.find { |row| row["meterId"] == Billing::Azure::MeteredBillingExport::ACTIONS_MAC_METER_ID }

      if row.present?
        row.fetch("quantity", 0) * Billing::ActionsUsageLineItem.billable_rate_for(runtime: :MACOS)
      else
        0
      end
    end

    def package_registry_cost_from_rows(rows)
      packages = rows.find { |row| row["meterId"] == Billing::Azure::MeteredBillingExport::PACKAGES_BANDWIDTH_METER_ID }
      if packages && packages["quantity"].present?
        Billing::Money.new(packages["quantity"] * package_registry_cost_in_cents)
      else
        0
      end
    end

    def shared_storage_cost_from_rows(rows)
      storage = rows.find { |row| row["meterId"] == Billing::Azure::MeteredBillingExport::STORAGE_METER_ID }

      if storage && storage["quantity"].present?
        Billing::Money.new(storage["quantity"] * shared_storage_cost_in_cents)
      else
        0
      end
    end

    def azure_export
      @_azure_export ||= Billing::Azure::MeteredBillingExport.new(start_on: start_on, end_on: end_on).data
    end

    def actions_cost_in_cents
      BigDecimal(Billing::Actions::ZuoraProduct::UNIT_COST) * 100
    end

    def package_registry_cost_in_cents
      BigDecimal(Billing::PackageRegistry::ZuoraProduct::UNIT_COST) * 100
    end

    def shared_storage_cost_in_cents
      BigDecimal(Billing::SharedStorage::ZuoraProduct::UNIT_COST) * 100
    end
  end
end
