# frozen_string_literal: true

module Billing
  module Azure
    class ExtractMetadata
      def initialize(exporter)
        @exporter = exporter
      end

      def total_customers
        @total_customers ||= exporter.azure_billed_businesses_count
      end

      def customers_with_overages
        @customers_with_overages ||= exporter.data.map { |record| record["subscriptionId"] }.uniq.count
      end

      def customers_without_overages
        @customers_without_overages ||= total_customers - customers_with_overages
      end

      def total_consumption_for(meter_id:)
        exporter.data.
          select { |record| record["meterId"] == meter_id }.
          map { |record| record["quantity"] }.
          sum
      end

      private

      attr_reader :exporter
    end
  end
end
