# frozen_string_literal: true

module Billing::Azure
  class MeteredBillingExport
    ACTIONS_LINUX_METER_ID = "3dbfec75-284c-4c89-8c9c-0d395be81a0c"
    ACTIONS_WINDOWS_METER_ID = "cc383714-48b2-46c6-aa9d-62040318c9e0"
    ACTIONS_MAC_METER_ID = "ca27e6bd-82cd-4cca-b015-818d85ae75a9"
    PACKAGES_BANDWIDTH_METER_ID = "1cfdf771-633e-4802-a7bb-edf1a81b2857"
    STORAGE_METER_ID = "832bfa96-c7db-416b-aa5b-6ea89054d493"
    LOCATION = "EUS2"

    attr_reader :start_on, :end_on

    def initialize(start_on:, end_on:, static_event_timestamp: nil)
      @start_on = start_on
      @end_on = end_on
      @static_event_timestamp = static_event_timestamp
    end

    def data
      @data ||= build_data
    end

    def azure_billed_businesses_count
      @azure_billed_businesses_count ||= azure_billed_businesses.count
    end

    private

    attr_reader :static_event_timestamp

    def build_data
      [
        *actions_usage,
        *packages_bandwidth_usage,
        *storage_usage,
      ].compact
    end

    def actions_usage
      azure_billed_businesses.flat_map do |business|
        line_items = Billing::ActionsUsageLineItem
          .where(billable_owner: business)
          .within_dates(start_on, end_on)

        usage_breakdown = Billing::Azure::ActionsOverageAttributer.new(
          included_minutes: business.plan.actions_included_private_minutes,
          line_items: line_items,
        ).breakdown

        [
          build_record(
            business: business,
            quantity: usage_breakdown["UBUNTU"],
            meter_id: ACTIONS_LINUX_METER_ID,
          ),
          build_record(
            business: business,
            quantity: usage_breakdown["WINDOWS"],
            meter_id: ACTIONS_WINDOWS_METER_ID,
          ),
          build_record(
            business: business,
            quantity: usage_breakdown["MACOS"],
            meter_id: ACTIONS_MAC_METER_ID,
          ),
        ]
      end
    end

    def packages_bandwidth_usage
      azure_billed_businesses.map do |business|
        consumption_gigabytes = Billing::PackageRegistry::DataTransferLineItem
          .where(billable_owner: business)
          .within_dates(start_on, end_on)
          .sum(:size_in_bytes) / 1.gigabyte

        build_record(
          business: business,
          quantity: [consumption_gigabytes - business.plan.package_registry_included_bandwidth, 0].max,
          meter_id: PACKAGES_BANDWIDTH_METER_ID,
        )
      end
    end

    def storage_usage
      azure_billed_businesses.map do |business|
        consumption_in_gigabyte_hours = Billing::SharedStorage::ArtifactAggregation.
          where(billable_owner: business).
          within_dates(start_on, end_on).
          private_visibility.
          sum(:aggregate_size_in_bytes) / 1.gigabyte

        consumption_in_gigabyte_per_month = consumption_in_gigabyte_hours / (Billing::SharedStorage::ZuoraProduct::ASSUMED_MONTHLY_DAYS * 24)

        build_record(
          business: business,
          quantity: [consumption_in_gigabyte_per_month - (business.plan.shared_storage_included_megabytes.megabytes / 1.gigabyte), 0].max,
          meter_id: STORAGE_METER_ID,
        )
      end
    end

    def azure_billed_businesses
      @azure_billed_businesses ||= Business.with_azure_subscription.not_staff_owned
    end

    def build_record(business:, quantity:, meter_id:)
      return nil if quantity.zero?

      subscription_id = business.enterprise_agreements.active.pick(:azure_subscription_id)

      Hash[
        "subscriptionId" => subscription_id,
        "quantity" => quantity,
        "meterId" => meter_id,
        "eventDateTime" => static_event_timestamp || Time.current,
        "resourceUri" => "/subscriptions/#{subscription_id}/providers/microsoft.visualstudio/EnterpriseAccount/#{business.slug}",
        "location" => LOCATION,
        "eventId" => SecureRandom.uuid
      ]
    end
  end
end
