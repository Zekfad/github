# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class Receipt
    include ActionView::Helpers::TextHelper

    attr_reader :billing_transaction

    delegate :amount,
             :asset_packs_delta,
             :asset_packs_total,
             :card_number,
             :card_type,
             :created_at,
             :discount_in_cents,
             :line_items,
             :paypal_email,
             :plan_name,
             :old_plan_name,
             :seats_delta,
             :seats_total,
             :transaction_type,
             :user,
             :yearly?,
             :job_posting?,
      to: :billing_transaction

    # Public: Create a Receipt for subscription payment.
    #
    # billing_transaction - BillingTransaction representing the charge for this
    #                       receipt.
    #
    # Returns a new Billing::Receipt.
    def initialize(billing_transaction)
      @billing_transaction = billing_transaction
    end

    # Public: String standarized filename based on the login of the user and
    # date of transaction.
    def pdf_filename
      "github-#{user.login}-receipt-#{purchased_at.strftime("%Y-%m-%d")}.pdf"
    end

    # Public: Returns a PDF rendering of this receipt.
    #
    # show_email_address - Boolean if billing email address should be included
    #                      on the receipt.
    def to_pdf(show_email_address: true)
      PdfRenderer.new(self, show_email_address: show_email_address).render
    end

    # Public: String receipt header.
    def receipt_header
      text = []
      text << "GITHUB RECEIPT -"
      text << (user.user? ? "PERSONAL" : "ORGANIZATION")
      text << (prorated_charge? ? "PURCHASE" : "SUBSCRIPTION")
      text << "-"
      text << user.login
      text.join(" ")
    end

    # Public: String plan summary of items purchased.
    def plan_summary
      case transaction_type
      when "prorate-charge"
        added_seats_text || prorated_yearly_upgrade_text
      when "prorate-seat-charge"
        added_seats_text
      when "prorate-asset-pack-charge"
        added_data_packs_text
      when "prorate-switch-to-seat-charge"
        "Switch to #{plan_display_name} (#{pluralize(seats_total, "seat")})"
      when "job-posting"
        "Job posting"
      when "job-credits"
        "Job posting credits"
      else
        if per_seat?
          switch_to_per_seat_text || per_seat_renewal_text
        else
          repository_plan_text
        end
      end
    end

    def per_seat?
      plan && plan.per_seat?
    end

    def added_seats?
      per_seat? && seats_delta > 0
    end

    # Internal: Boolean if billing_transaction records a switch to per seat pricing.
    def switched_to_per_seat?
      per_seat? && (transaction_type == "prorate-switch-to-seat-charge" || (old_plan && !old_plan.per_seat?))
    end

    # Public: The amount tied to a GitHub plan, e.g. plan price and data packs
    #
    # Returns Billing::Money
    def plan_amount
      Billing::Money.new([amount - usage_amount - marketplace_amount - sponsorship_amount, 0].max)
    end

    def payment_type
      paypal_email ? "PayPal account" : card_type
    end

    def payment_identifier
      paypal_email.presence || card_number
    end

    def transaction_id
      billing_transaction.transaction_id.try(:upcase)
    end

    def purchased_at
      created_at.in_billing_timezone
    end

    def service_ends_on
      service_ends_at.in_billing_timezone.strftime("%Y-%m-%d")
    end

    def vat_code
      user.vat_code
    end

    def extra
      user.billing_extra
    end

    def prorated_charge?
      transaction_type.match(/prorate/)
    end

    def data_packs_addon_text
      if plan_renewal_with_data_packs?
        "#{pluralize(asset_packs_total, "data pack")} (#{data_pack_cost_text})"
      end
    end

    def plan_renewal_with_data_packs?
      !prorated_charge? && asset_packs_delta == 0 && asset_packs_total > 0
    end

    # Public: Select only line items tied to marketplace purchases
    #
    # See https://github.com/github/sponsors/issues/353
    # After sponsors are decoupled from the marketplace, all marketplace
    # line items will be non_sponsorships, so we can change this to use
    # the #marketplace scope.
    #
    # Returns an Array of BillingTransaction::LineItem
    def marketplace_line_items
      line_items.non_sponsorships
    end

    # Public: Select only line items tied to sponsorships
    #
    # Returns an Array of BillingTransaction::LineItem
    def sponsorship_line_items
      line_items.sponsorships
    end

    # Public: Select only line items tied to Actions private usage
    #
    # Returns an Array of BillingTransaction::LineItem
    def actions_line_items
      @actions_line_items ||= line_items.actions_usage
    end

    # Public: Select only line items tied to Package Registry Data Usage
    #
    # Returns an Array of BillingTransaction::LineItem
    def package_registry_transfer_line_items
      @package_registry_transfer_line_items ||= line_items.packages_data_transfer_usage
    end

    # Public: Select only line items tied to Share Storage Usage
    #
    # Returns an Array of BillingTransaction::LineItem
    def shared_storage_line_items
      @shared_storage_line_items ||= line_items.shared_storage_usage
    end

    # Public: Format marketplace line items for display
    #
    # Returns String
    def marketplace_line_items_text
      marketplace_line_items.map do |item|
        "#{item.subscribable.listing.name} - #{item.subscribable.name}"
      end.join("\n")
    end

    # Public: Format sponsorship line items for display
    #
    # Returns String
    def sponsorship_line_items_text
      sponsorship_line_items.map do |item|
        "#{item.subscribable.listing.name} - #{item.subscribable.name}"
      end.join("\n")
    end

    # Public: Format Actions usage line items for display
    #
    # Returns String
    def actions_line_items_text
      if actions_line_items.length == 1
        action_line_items_included_only_text
      elsif actions_line_items.length > 1
        actions_line_items_with_overages_text
      end
    end

    # Public: Format Actions usage line items for display
    #
    # Returns String
    def packages_transfer_line_items_text
      if package_registry_transfer_line_items.length == 1
        packages_transfer_line_items_included_only_text
      elsif package_registry_transfer_line_items.length > 1
        packages_transfer_line_items_with_overages_text
      end
    end

    # Public: Format Shared Storage line items for display
    #
    # Returns String
    def shared_storage_line_items_text
      if shared_storage_line_items.length == 1
        shared_storage_line_items_included_only_text
      elsif shared_storage_line_items.length > 1
        shared_storage_line_items_with_overages_text
      end
    end

    # Public: Sums the amount for marketplace line items
    #
    # Returns Billing::Money
    def marketplace_amount
      Billing::Money.new marketplace_line_items.sum(:amount_in_cents)
    end

    # Public: Sums the amount for sponsorship line items
    #
    # Returns Billing::Money
    def sponsorship_amount
      Billing::Money.new(sponsorship_line_items.sum(:amount_in_cents))
    end

    # Public: Sums the amount for line items associated with usage products
    #
    # Returns Billing::Money
    def usage_amount
      actions_amount + package_registry_transfer_amount + shared_storage_amount
    end

    # Public: Sums the amount for Actions line items
    #
    # Returns Billing::Money
    def actions_amount
      Billing::Money.new actions_line_items.sum(:amount_in_cents)
    end

    # Public: Sums the amount for Package Registry transfer line items
    #
    # Returns Billing::Money
    def package_registry_transfer_amount
      Billing::Money.new package_registry_transfer_line_items.sum(:amount_in_cents)
    end

    # Public: Sums the amount for GitHub Shared Storage line items
    #
    # Returns Billing::Money
    def shared_storage_amount
      Billing::Money.new shared_storage_line_items.sum(:amount_in_cents)
    end

    # Public: Whether we should display the sponsorship line items section
    #
    # Returns Boolean
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def sponsorship_line_items_displayable?
      @sponsorship_line_items_displayable ||= sponsorship_line_items.any?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Public: Whether the receipt is a result of adding/updating
    # a sponsorship mid-cycle.
    #
    # Returns Boolean
    def sponsorship_only_transaction?
      [plan_amount, actions_amount, marketplace_amount].all?(&:zero?) && !sponsorship_amount.zero?
    end

    def sponsorship_only_text
      "We received payment for your #{'sponsorship'.pluralize(sponsorship_line_items.count)}. " \
      "Thanks for your support of Open Source Software!"
    end

    private

    # Private: Description of actions minutes when all minutes are included.
    # When plan does not exist, it will fail gracefully and generate
    # a Failbot report and return description minutes consumed without the total included.
    #
    # Returns String
    def action_line_items_included_only_text
      unless plan
        Failbot.report(
          RuntimeError.new("Missing plan_name in billing transaction with actions usage"),
          billing_transaction_id: billing_transaction.id,
        )

        return actions_line_items_with_overages_text
      end

      item = actions_line_items.first
      "#{item.quantity} of #{plan_actions_included_minutes} included private minutes #{Billing::Money.new(item.amount_in_cents).format(with_currency: true)}"
    end

    def plan_actions_included_minutes
      plan.actions_included_private_minutes
    end

    # Private: Description of shared storage when all cost is included.
    # When plan does not exist, it will fail gracefully and generate
    # a Failbot report and return description with total included.
    #
    # Returns String
    def shared_storage_line_items_included_only_text
      unless plan
        Failbot.report(
          RuntimeError.new("Missing plan_name in billing transaction with shared storage usage"),
          billing_transaction_id: billing_transaction.id,
        )

        return shared_storage_line_items_with_overages_text
      end

      item = shared_storage_line_items.first
      gigabytes = gigabytes_used(item.quantity)
      "#{gigabytes}GB of #{plan_shared_storage_included_gigabytes}GB included storage #{Billing::Money.new(item.amount_in_cents).format(with_currency: true)}"
    end

    # Private: Description of shared storage when there are overages.
    #
    # Returns String
    def shared_storage_line_items_with_overages_text
      shared_storage_line_items.map do |item|
        data_type = item.amount_in_cents == 0 ? "included" : "additional"

        gigabytes = gigabytes_used(item.quantity)
        "#{gigabytes}GB #{data_type} storage #{Billing::Money.new(item.amount_in_cents).format(with_currency: true)}"
      end.join("\n")
    end

    # Private: Description of package data transfer when all cost is included.
    # When plan does not exist, it will fail gracefully and generate
    # a Failbot report and return description with total included.
    #
    # Returns String
    def packages_transfer_line_items_included_only_text
      unless plan
        Failbot.report(
          RuntimeError.new("Missing plan_name in billing transaction with package registry transfer usage"),
          billing_transaction_id: billing_transaction.id,
        )

        return packages_transfer_line_items_with_overages_text
      end

      item = package_registry_transfer_line_items.first
      "#{item.quantity}GB of #{plan.package_registry_included_bandwidth}GB included data transfer out #{Billing::Money.new(item.amount_in_cents).format(with_currency: true)}"
    end

    # Private: Description of data transfer charges when there are overages.
    #
    # Returns String
    def packages_transfer_line_items_with_overages_text
      package_registry_transfer_line_items.map do |item|
        data_type = item.amount_in_cents == 0 ? "included" : "additional"
        "#{item.quantity}GB #{data_type} data transfer out #{Billing::Money.new(item.amount_in_cents).format(with_currency: true)}"
      end.join("\n")
    end

    # Private: Description of actions charges when there are overages.
    #
    # Returns String
    def actions_line_items_with_overages_text
      actions_line_items.map do |item|
        minutes_type = item.amount_in_cents == 0 ? "included" : "additional"
        "#{item.quantity} #{minutes_type} private #{("minute").pluralize(item.quantity)} #{Billing::Money.new(item.amount_in_cents).format(with_currency: true)}"
      end.join("\n")
    end

    # Private: Converts shared storage megabyte hours consumed to gigabytes rounded to 2 decimal places
    #
    # Returns Float
    def gigabytes_used(megabyte_hours)
      megabytes = megabyte_hours / Billing::SharedStorage::ZuoraProduct::ASSUMED_MONTHLY_DAYS / 24

      to_gigabytes(megabytes)
    end

    # Private: Included shared storage in gigabytes rounded to 2 decimal places
    #
    # Returns Float
    def plan_shared_storage_included_gigabytes
      to_gigabytes(plan.shared_storage_included_megabytes)
    end

    def to_gigabytes(size_in_megabytes)
      (size_in_megabytes.megabytes / 1.gigabyte.to_f).round(2)
    end

    def repository_plan_text
      text = []
      text << plan_display_name
      text << "yearly" if yearly?
      text.compact.join(" ")
    end

    def plan_display_name
      (plan ? plan.display_name : plan_name).titleize
    end

    def plan_user_type
      return if plan && plan.pro?

      if user.user?
        "personal"
      else
        "organization"
      end
    end

    def prorated_yearly_upgrade_text
      if old_plan_name
        "#{old_plan_name.humanize} to #{plan_name.humanize}"
      else
        repository_plan_text
      end
    end

    def per_seat_renewal_text
      return business_plan_text if plan.business?

      "#{pluralize(seats_total, "seat")} (#{per_seat_cost_text})"
    end

    def additional_seats
      [seats_total - plan.base_units, 0].max
    end

    def additional_seats_text
      if additional_seats > 0
        "#{pluralize(additional_seats, "additional seat")} (#{per_seat_cost_text})"
      end
    end

    def base_seats_text
      "#{plan.base_units} included seats" if plan.base_units > 0
    end

    def added_data_packs_text
      "#{pluralize(asset_packs_delta, "additional data pack")} (#{data_pack_cost_text})"
    end

    def added_seats_text
      if added_seats?
        "#{pluralize(seats_delta, "additional seat")} (#{per_seat_cost_text})"
      end
    end

    # Internal: String descriptive text about switch to per seat pricing or nil.
    def switch_to_per_seat_text
      if switched_to_per_seat?
        "Switch to #{plan_display_name} (#{pluralize(seats_total, "seat")})"
      end
    end

    def per_seat_cost_text
      prorated_cost_text(Billing::Money.new(plan_unit_cost))
    end

    def data_pack_cost_text
      prorated_cost_text(Asset::Status.data_pack_unit_price * plan_duration_in_months)
    end

    def prorated_cost_text(cost)
      text = []
      text << "#{cost.format(no_cents_if_whole: true)}/#{yearly? ? 'year' : 'month'} each"
      if prorated_charge? && prorated_days > 0
        text << "- prorated for #{pluralize(prorated_days, "day")}"
      end
      text.join(" ")
    end

    def plan
      plan = GitHub::Plan.find(plan_name, effective_at: user.try(:plan_effective_at))
      if purchased_before_munich_launch?
        plan.receipt_effective_date = purchased_at.to_date
        plan
      else
        MunichPlan.wrap(plan, target: user)
      end
    end

    def old_plan
      plan = GitHub::Plan.find(old_plan_name)
      return unless plan

      if purchased_before_munich_launch?
        plan.receipt_effective_date = purchased_at.to_date
        plan
      else
        MunichPlan.wrap(plan, target: user)
      end
    end

    # Internal: Integer plan duration in months
    #
    # Returns an Integer
    def plan_duration_in_months
      yearly? ? 12 : 1
    end

    def plan_unit_cost
      yearly? ? plan.yearly_unit_cost_in_cents : plan.unit_cost_in_cents
    end

    def service_ends_at
      billing_transaction.service_ends_at || (created_at + plan_duration_in_months.months)
    end

    def prorated_days
      return 0 unless prorated_charge?
      # NB - service end day is one day earlier to match Braintree/Zuora calculations
      days = (service_ends_at.to_billing_date - 1.day) - created_at.to_billing_date
      [days.to_i, 0].max
    end

    # Internal: the release date of project munich
    #
    # Returns a Date
    def munich_plan_release_date
      ::MunichPlan::RELEASE_DATE
    end

    # Internal: formatted text outlining seat count and cost, conditionally
    # including the plan display name
    #
    # Returns a String
    def business_plan_text
      if purchased_before_munich_launch?
        [base_seats_text, additional_seats_text].compact.join(" + ")
      else
        "#{plan_display_name}: #{pluralize(seats_total, "seat")} (#{per_seat_cost_text})"
      end
    end

    # Internal: whether the transaction took place before or after
    # the launch of project munich
    #
    # Returns a Boolean
    def purchased_before_munich_launch?
      purchased_at.to_date < munich_plan_release_date
    end
  end
end
