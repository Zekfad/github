# frozen_string_literal: true

module Billing
  module Zuora
    module UsageFile
      # A policy composed of rules for determining if a given metered line item
      # can be submitted to Zuora
      class LineItemSubmissionPolicy
        attr_reader :reason

        RULES = %i[
          unsubmitted
          synchronization_batch_id_not_present
          owner_present
          directly_billed
          private_visibility
          has_subscription_number
          has_charge_number
          allows_overage_billing
          current_accounting_period
          current_metered_billing_cycle
        ].freeze

        # Public: Initialize the LineItemSubmissionPolicy
        #
        # line_item - An instance of a metered line item class
        def initialize(line_item)
          @line_item = line_item

          @owner = line_item.owner
          @billable_owner = line_item.billable_owner
          @metered_billing_configuration = billable_owner&.budget_for(product: line_item.class.product_name)
        end

        # Public: Can the line item be submitted to Zuora?
        #
        # When a line item cannot be submitted to Zuora, the `#reason` method
        # can be used to determine the rule that failed.
        #
        # Returns Boolean
        def submittable?
          RULES.all? do |rule|
            send(rule)
          end
        end

        private

        attr_reader :line_item, :billable_owner, :metered_billing_configuration, :owner

        # If a line item is already submitted, it cannot be submitted again
        # If a line item is skipped, it cannot be submitted
        def unsubmitted
          if line_item.submitted?
            @reason = "already_submitted"
            return false
          end

          if line_item.skipped?
            @reason = "already_skipped"
            return false
          end

          true
        end

        # If a line item already has a synchronization batch ID, that also means
        # that it is already submitted, so it cannot be submitted again
        def synchronization_batch_id_not_present
          if line_item.synchronization_batch_id?
            @reason = "sync_batch_id_present"
            false
          else
            true
          end
        end

        # If the owner of the line item has been deleted, it cannot be submitted
        # since there is no longer a Zuora account to bill
        def owner_present
          unless owner.present?
            @reason = "owner_deleted"
            false
          else
            true
          end
        end

        # If the line item is not directly billed -- e.g. it is billed through a
        # Microsoft enterprise agreement -- then it cannot be submitted to Zuora
        # since it is billed separately
        def directly_billed
          unless line_item.directly_billed?
            @reason = "not_directly_billed"
            false
          else
            true
          end
        end

        # If the line item is not for a private repo, it cannot be submitted
        # because usage is free for public repos
        def private_visibility
          unless line_item.private_visibility?
            @reason = "public_repository"
            false
          else
            true
          end
        end

        # Subscription number is a required field for the batch CSV file, so if
        # it is not present, the line item cannot be submitted to Zuora
        def has_subscription_number
          unless line_item.zuora_subscription_number.present?
            @reason = "missing_subscription_id"
            false
          else
            true
          end
        end

        # Charge number is a required field for the batch CSV file, so if it is
        # not present, the line item cannot be submitted to Zuora
        def has_charge_number
          unless line_item.zuora_charge_number.present?
            @reason = "missing_charge_id"
            false
          else
            true
          end
        end

        # If the owner has not opted into overages, then the line item cannot
        # be submitted to Zuora
        def allows_overage_billing
          if metered_billing_configuration.enforce_spending_limit?
            if metered_billing_configuration.user_configured_spending_limit_in_subunits.zero?
              @reason = "overage_not_allowed"
              return false
            end
          end
          true
        end

        # If the line item occurred in a closed accounting period, then the line
        # item cannot be submitted as it will be rejected by Zuora
        def current_accounting_period
          # Allowed overlap for usage that occurred immediately before the
          # accounting period closed
          return true if line_item.usage_at >= 6.hours.ago

          accounting_period_start = GitHub::Billing.now.beginning_of_month
          if line_item.usage_at < accounting_period_start
            @reason = "closed_accounting_period"
            false
          else
            true
          end
        end

        # If the line item occurred outside of the owner's current metered
        # billing cycle, then it cannot be submitted to Zuora since we only
        # charge for usage in the current metered billing cycle
        def current_metered_billing_cycle
          # Allowed overlap for usage that occurred immediately before the
          # account's metered billing cycle reset
          return true if line_item.usage_at >= 6.hours.ago

          metered_cycle_start = GitHub::Billing.date_in_timezone(billable_owner.first_day_in_metered_cycle)
          if line_item.usage_at < metered_cycle_start
            @reason = "previous_billing_cycle"
            false
          else
            true
          end
        end
      end
    end
  end
end
