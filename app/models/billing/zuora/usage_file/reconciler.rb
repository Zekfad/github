# frozen_string_literal: true

module Billing
  module Zuora
    module UsageFile
      # Class to reconcile metered line items actually submitted to Zuora with
      # the various line item tables in the database
      #
      # This class will update line items to reflect the proper submission_state
      # as well as update the submission_state_reason to "confirmed" to note
      # that the line item actually appeared in a file submitted to Zuora. The
      # class will also publish statistics:
      #
      #   - zuora.usage_file_reconciler.line_items_matched
      #     This is the number of line items submitted to Zuora that matched
      #     exactly to the database
      #
      #   - zuora.usage_file_reconciler.line_items_submission_state_mismatch
      #     This is the number of line items submitted to Zuora that had a
      #     submission_state other than "submitted"
      #
      #   - zuora.usage_file_reconciler.line_item_batch_mismatch
      #     This is the number of line items submitted to Zuora in the
      #     given UsageSynchronizationBatch that are associated to a
      #     different UsageSynchronizationBatch, possibly indicating
      #     duplicate submission
      #
      #   - zuora.usage_file_reconciler.line_items_missing
      #     This is the number of line items submitted to Zuora for which a
      #     matching record in the database could not be found
      #
      #   - zuora.usage_file_reconciler.line_item_unconfirmed
      #     This is the number of line items in the database that remain
      #     unconfirmed after reconciliation, indicating that those line
      #     items did not appear in the file submitted to Zuora
      class Reconciler
        attr_reader :batch, :buffer, :csv, :s3_client, :stats

        # Public: Perform reconciliation for a given UsageSynchronizationBatch
        #
        # usage_synchronization_batch - The batch to reconcile
        #
        # Returns nothing
        def self.perform(usage_synchronization_batch)
          new(usage_synchronization_batch).perform
        end

        # Public: Initialize the Reconciler
        #
        # usage_synchronization_batch - The batch to reconcile
        def initialize(usage_synchronization_batch)
          @batch = usage_synchronization_batch
          @s3_client = GitHub.s3_billing_client
          @stats = GitHub.dogstats
        end

        # Public: Perform the reconciliation
        #
        # Returns nothing
        def perform
          @buffer = StringIO.new
          download_file_from_s3

          @csv = CSV.new(buffer, headers: true)
          csv.each_slice(100) do |slice|
            reconcile_lines(slice)
          end

          publish_unconfirmed_line_item_stats
        end

        private

        def download_file_from_s3
          s3_client.get_object(
            bucket: SubmitZuoraUsageFileJob::BUCKET_NAME,
            key: batch.upload_filename,
            response_target: buffer,
          )
          buffer.rewind
        end

        def reconcile_lines(slice)
          rows_by_product = slice.group_by { |row| normalize_product(row["GITHUB_PRODUCT"]) }
          rows_by_product.each do |product, rows|
            line_item_ids = rows.map { |row| row["GITHUB_ITEM_ID"] }
            line_items = query_base(product).where(id: line_item_ids).to_a

            publish_submission_state_stats(product, line_items)
            publish_synchronization_batch_stats(product, line_items)
            publish_missing_line_items_stats(product, rows, line_items)

            query_base(product).throttle do
              query_base(product).where(id: line_item_ids).update_all(
                submission_state: "submitted",
                submission_state_reason: "confirmed",
                updated_at: Time.now,
              )
            end
          end
        end

        def publish_submission_state_stats(product, line_items)
          line_items_by_submission_state = line_items.each_with_object(Hash.new(0)) do |item, memo|
            memo[item.submission_state] += 1
          end

          stats.count(
            "zuora.usage_file_reconciler.line_items_matched",
            line_items_by_submission_state["submitted"],
            tags: ["product:#{product}"],
          )

          stats.count(
            "zuora.usage_file_reconciler.line_items_submission_state_mismatch",
            line_items_by_submission_state["unsubmitted"],
            tags: ["product:#{product}", "submission_state:unsubmitted"],
          )

          stats.count(
            "zuora.usage_file_reconciler.line_items_submission_state_mismatch",
            line_items_by_submission_state["skipped"],
            tags: ["product:#{product}", "submission_state:skipped"],
          )
        end

        def publish_synchronization_batch_stats(product, line_items)
          line_item_batch_mismatches = line_items.count do |item|
            item.synchronization_batch_id != batch.id
          end

          stats.count(
            "zuora.usage_file_reconciler.line_item_batch_mismatch",
            line_item_batch_mismatches,
            tags: ["product:#{product}"],
          )
        end

        def publish_missing_line_items_stats(product, rows, line_items)
          return if rows.length == line_items.length

          rows_missing_line_items = rows.reject do |row|
            line_items.any? { |item| item.id == row["GITHUB_ITEM_ID"].to_i }
          end

          stats.count(
            "zuora.usage_file_reconciler.line_items_missing",
            rows_missing_line_items.length,
            tags: ["product:#{product}"],
          )
        end

        def publish_unconfirmed_line_item_stats
          %i[actions packages storage].each do |product|
            unconfirmed = query_base(product)
              .submitted
              .where.not(submission_state_reason: "confirmed")
              .where(synchronization_batch: batch)

            stats.count(
              "zuora.usage_file_reconciler.line_item_unconfirmed",
              unconfirmed.count,
              tags: ["product:#{product}"],
            )
          end
        end

        def normalize_product(product)
          product.downcase.sub(/\Agh_/, "").to_sym
        end

        # Public: Metered Line item usage class for the given product symbol key
        #
        # Example
        #   query_base(:actions)
        #   => Billing::ActionsUsageLineItem
        #
        # Returns Class
        # Raises ArgumentError if the class doesn't exist for the product_key
        def query_base(product_key)
          Billing::MeteredBillingProduct.products[product_key]
        end
      end
    end
  end
end
