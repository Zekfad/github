# frozen_string_literal: true

class Billing::Zuora::Import

  # Public: Returns the successful file imports with the associated filename.
  # Successful imports are where the status is not Canceled or Failed
  #
  # name: The filename of the import.
  #
  # Returns Array of Zuora::Import objects
  def self.successful_imports(name:)
    response = GitHub.zuorest_client.query_action queryString: <<-ZOQL
      SELECT Id, Status, Name FROM Import
       WHERE Name = '#{name}'
        AND Status != 'Canceled'
        AND Status != 'Failed'
    ZOQL

    create_imports_from_zuora(response)
  end

  def self.find(id)
    response = GitHub.zuorest_client.get_import(id)
    Billing::Zuora::Import.new(response)
  end

  def initialize(import_attributes)
    @attributes = import_attributes
  end

  def id
    attributes["Id"]
  end

  def status
    attributes["Status"]
  end

  def name
    attributes["Name"]
  end

  def status_reason
    attributes["StatusReason"]
  end

  # Public: Returns url to check the import status
  #
  # Returns String
  def zuora_status_url
    "/v1/usage/#{id}/status"
  end

  private

  attr_reader :attributes

  # Private: Takes a query response from zuora and creates imports
  #
  # Returns an array of ::Billing::Zuora::Import records
  def self.create_imports_from_zuora(response)
    response
      .fetch("records", [])
      .compact
      .map { |record| new(record) }
  end
  private_class_method :create_imports_from_zuora
end
