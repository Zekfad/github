# frozen_string_literal: true

module Billing
  module Zuora
    # Reset a Zuora payment method's number of consecutive failures to zero,
    # allowing the payment method to be charged
    #
    # This is primarily useful when manually resetting billing attempts in
    # Stafftools, since Zuora resets this automatically on successful payment
    class ResetPaymentMethodConsecutiveFailures
      attr_reader :payment_method, :response

      # Initialize a new ResetPaymentMethodConsecutiveFailures
      #
      # payment_method - A PaymentMethod object
      def initialize(payment_method:)
        @payment_method = payment_method
      end

      # Reset the number of consecutive failures on the Zuora payment method
      #
      # Returns ResetPaymentMethodConsecutiveFailures
      def perform
        return self unless payment_method&.on_zuora?

        @response = GitHub.zuorest_client.update_payment_method(
          payment_method.payment_token,
          "NumConsecutiveFailures" => 0,
        )
        self
      end

      # Whether or not the operation was successful
      #
      # Returns Boolean
      def success?
        @response&.dig("Success") == true
      end
    end
  end
end
