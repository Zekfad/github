# frozen_string_literal: true

class Billing::Zuora::SynchronizationError < StandardError; end
