# frozen_string_literal: true

class Billing::Zuora::MarketplaceRollback
  def self.perform(*args); new(*args).perform; end

  attr_reader :error_message, :reset_marketplace_items, :plan_subscription

  delegate \
    :plan_duration,
    :subscription_items,
    :user,
    :zuora_subscription,
    to: :plan_subscription

  delegate \
    :active_rate_plans,
    to: :zuora_subscription

  def initialize(plan_subscription, error_message)
    @error_message = error_message
    @plan_subscription = plan_subscription
    @reset_marketplace_items = false
    @cancelled_sponsorships = false
  end

  def perform
    GitHub.instrument("billing.marketplace_rollback", user: user, error: error_message)

    subscription_items.select(&:billable?).each do |item|
      rate_plan = rate_plan_for(item)

      if rate_plan
        reset_item(item, rate_plan)
      else
        cancel_item(item)
      end
    end

    user.set_sponsorship_rollback_notification if @cancelled_sponsorships && user.user?
    send_failure_notification if reset_marketplace_items
  end

  def marketplace_rate_plans
    active_rate_plans.select do |rate_plan|
      marketplace_product_rate_plan_ids.include? rate_plan[:productRatePlanId]
    end
  end

  def marketplace_product_rate_plan_ids
    @_marketplace_product_rate_plan_ids ||= begin
      product_rate_plan_ids = active_rate_plans.map { |rate_plan| rate_plan[:productRatePlanId] }
      Billing::ProductUUID
        .where(product_type: [Marketplace::ListingPlan::ZUORA_PRODUCT_TYPE, SponsorsTier::ZUORA_PRODUCT_TYPE])
        .where(zuora_product_rate_plan_id: product_rate_plan_ids)
        .pluck(:zuora_product_rate_plan_id)
    end
  end

  def rate_plan_for(item)
    marketplace_rate_plans.detect do |rate_plan|
      rate_plan[:productRatePlanId] == item.subscribable.zuora_id(cycle: plan_duration)
    end
  end

  def cancel_item(item)
    previous_quantity = item.quantity
    item.update_column(:quantity, 0)

    prefix = item.listable_is_sponsorable? ? "sponsorship" : "marketplace_purchase"
    GitHub.instrument "#{prefix}.cancelled",
      subscription_item_id: item.id,
      sender_id: user.id,
      previous_quantity: previous_quantity,
      previous_subscribable_id: item.subscribable.id,
      previous_subscribable_type: item.subscribable.class.name
    @reset_marketplace_items = true
    @cancelled_sponsorships = true if item.listable_is_sponsorable?

    if item.listable_is_sponsorable?
      sponsor = item.account
      sponsorable = item.listing.sponsorable
      sponsorship = Sponsorship.find_by(sponsor: sponsor, sponsorable: sponsorable)
      return unless sponsorship
      sponsorship.update_column(:active, false)

      GlobalInstrumenter.instrument("sponsors.sponsor_sponsorship_cancel", {
        actor: user,
        sponsorship: sponsorship,
        listing: item.listing,
        tier: sponsorship.subscribable,
        action: :CANCEL,
        goal: item.listing.active_goal,
      })
      GitHub.instrument("sponsors.sponsor_sponsorship_cancel", {
        actor: user,
        user: sponsorable,
        sponsorship: sponsorship,
        current_tier_id: item.subscribable.id,
      })
    end
  end

  def reset_item(item, rate_plan)
    previous_quantity = item.quantity
    quantity = quantity_for_rate_plan(rate_plan)
    return if quantity == previous_quantity

    item.update_column(:quantity, quantity)
    prefix = item.listable_is_sponsorable? ? "sponsorship" : "marketplace_purchase"
    GitHub.instrument "#{prefix}.changed",
      subscription_item_id: item.id,
      sender_id: user.id,
      previous_quantity: previous_quantity,
      previous_subscribable_id: item.subscribable.id,
      previous_subscribable_type: item.subscribable.class.name
    @reset_marketplace_items = true
  end

  def quantity_for_rate_plan(rate_plan)
    quantity = rate_plan[:ratePlanCharges].to_a.map { |charge| charge[:quantity].to_i }.max
    [quantity.to_i, 1].max
  end

  def send_failure_notification
    Billing::PlanSubscription::SendFailureNotification.perform \
      plan_subscription,
      marketplace: true,
      message: error_message
  end
end
