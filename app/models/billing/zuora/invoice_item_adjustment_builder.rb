# frozen_string_literal: true

module Billing
  module Zuora
    class InvoiceItemAdjustmentBuilder

      # Public: create the information to send to zuora for zeroing out invoices
      #
      # invoice           - the invoice to be zeroed
      # adjustment_amount - the value we are considering for zeroing out
      #
      # Returns an array of hashes (or an empty array)
      def self.perform(invoice:, adjustment_amount:)
        invoice_id = invoice.invoice_id

        invoice.invoice_items.map do |item|
          current_item_adjustment_amount = [adjustment_amount, item.charge_amount].min
          next if current_item_adjustment_amount <= 0
          adjustment_amount -= current_item_adjustment_amount

          {
            AdjustmentDate: GitHub::Billing.today.to_s,
            Amount: current_item_adjustment_amount,
            InvoiceId: invoice_id,
            SourceId: item.id,
            SourceType: "InvoiceDetail",
            Type: "Credit",
          }

        end.compact
      end
    end
  end
end
