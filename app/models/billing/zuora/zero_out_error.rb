# frozen_string_literal: true

class Billing::Zuora::ZeroOutError < StandardError; end
