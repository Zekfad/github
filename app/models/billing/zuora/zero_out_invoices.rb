# frozen_string_literal: true

module Billing
  module Zuora
    class ZeroOutInvoices

      SUBSCRIPTION_TYPE = "subscription"
      TRANSACTION_TYPE  = "transaction"
      ACCOUNT_TYPE = "account"

      # Public: creates a new instance of ZeroOutInvoices
      #
      # This should really only be used when called from .for_transaction or
      # .for_subscription to ensure you get the correct invoices and "belongs_to" values
      #
      # invoices: the invoices we plan to zero out the balance of
      # invoice_belongs_to_type: String representing the related object.
      # invoice_belongs_to_id: String id for the belongs_to_type record in Zuora
      #
      # Returns an instance of ::Billing::Zuora::ZeroOutInvoices
      def initialize(invoices:, invoice_belongs_to_type:, invoice_belongs_to_id:)
        @invoices                = invoices
        @invoice_belongs_to_type = invoice_belongs_to_type
        @invoice_belongs_to_id   = invoice_belongs_to_id

        @restraint ||= GitHub::Restraint.new
      end

      # Public: Zeroes out the balance of an invoice for a given Payment and refund amount
      #
      # zuora_transaction_id: The ID of the Zuora Payment
      # fetcher: class used to get invoices from Zuora
      #
      # Returns GitHub::Billing::Result
      # Raises ::Billing::Zuora::ZeroOutError if at least one invoice does not zero out
      def self.for_transaction(zuora_transaction_id, fetcher: ::Billing::ZuoraInvoice)
        new(
          invoices: fetcher.invoices_for_transaction(zuora_transaction_id),
          invoice_belongs_to_type: TRANSACTION_TYPE,
          invoice_belongs_to_id: zuora_transaction_id,
        ).zero_out
      end

      # Public: Zeroes out the balance of an invoice for a given subscription
      #
      # zuora_subscription_number: The number of the Zuora Subscription
      # fetcher: class used to get invoices from Zuora
      #
      # Returns GitHub::Billing::Result
      # Raises ::Billing::Zuora::ZeroOutError if at least one invoice does not zero out
      def self.for_subscription(zuora_subscription_number, fetcher: ::Billing::ZuoraInvoice)
        new(
          invoices: fetcher.invoices_for_subscription(zuora_subscription_number),
          invoice_belongs_to_type: SUBSCRIPTION_TYPE,
          invoice_belongs_to_id: zuora_subscription_number,
        ).zero_out
      end

      # Public: Zeroes out the balance of invoices for a given account
      #
      # zuora_account_id: The number of the Zuora Subscription
      # fetcher: class used to get invoices from Zuora
      #
      # Returns GitHub::Billing::Result
      # Raises ::Billing::Zuora::ZeroOutError if at least one invoice does not zero out
      def self.for_account(zuora_account_id, fetcher: ::Billing::ZuoraInvoice)
        new(
          invoices: fetcher.invoices_for_account(zuora_account_id),
          invoice_belongs_to_type: ACCOUNT_TYPE,
          invoice_belongs_to_id: zuora_account_id,
        ).zero_out
      end

      # Public: Sends a message to each invoice to zero out on Zuora
      #
      # While iterating, it will effectively skip any invoices that are cancelled,
      # as adjustments cannot be created for them, and it causes an error, which
      # then erroneously raises a ZeroOutError
      #
      # Returns GitHub::Billing::Result
      # Raises ::Billing::Zuora::ZeroOutError if at least one invoice does not zero out
      def zero_out
        return ::GitHub::Billing::Result.success if invoices.empty?

        results = invoices.map do |invoice|
          if invoice.invoice_id.nil? || invoice.cancelled?
            ::GitHub::Billing::Result.success
          else
            result = zero_out_on_zuora(invoice)
            result[:result]
          end
        end

        process_results(results)
      end

      private

      attr_reader :invoices, :invoice_belongs_to_type, :invoice_belongs_to_id, :restraint

      def lock_invoice(invoice, &block)
        lock_key = "zero-out-zuora-invoice-#{invoice.id}"
        restraint.lock!(lock_key, _n = 1, _ttl = 1.minute, &block)
      end

      # Internal: Prep the invoice adjustments that will be sent to zuora to zero out
      #
      # invoice            - the invoice we are zero-ing out
      # adjustment_builder - the class we're using to build the adjustments
      #
      # Returns a hash of the result and the resulting adjustment_amount
      def zero_out_on_zuora(invoice, adjustment_builder: ::Billing::Zuora::InvoiceItemAdjustmentBuilder)
        adjustment_amount = ::Billing::Money.new(invoice.balance * 100)

        invoice_item_adjustments = adjustment_builder.perform(
          invoice: invoice, adjustment_amount: adjustment_amount,
        )

        return default_response(adjustment_amount: adjustment_amount) if invoice_item_adjustments.empty?

        lock_invoice(invoice) do
          return send_zero_out_to_zuora(adjustments: invoice_item_adjustments, adjustment_amount: adjustment_amount)
        end
      rescue GitHub::Restraint::UnableToLock
        return default_response(adjustment_amount: adjustment_amount)
      end

      # Internal: send the adjustments to zuora
      # NB: This makes a remote call to Zuora
      #
      # adjustments   - an array of hashes that reflect the necessary changes
      # adjustment_amount - the resulting adjustment_amount
      #
      # Returns a hash of the result and the resulting adjustment_amount
      def send_zero_out_to_zuora(adjustments:, adjustment_amount:)
        response = GitHub.zuorest_client.create_action objects: adjustments, type: "InvoiceItemAdjustment"

        { result: GitHub::Billing::Result.from_zuora(response.first), refund_amount: adjustment_amount }
      end

      # Internal: raise an exception if any of the invoices failed to zero out
      #
      # results_from_zuora: an array of results of the zero out operation
      #
      # Returns instance of ::GitHub::Billing::Result
      # Raises ::Billing::Zuora::ZeroOutError if at least one invoice does not zero out
      def process_results(results_from_zuora)
        result = results_from_zuora.detect(&:failed?) || results_from_zuora.first
        raise ::Billing::Zuora::ZeroOutError.new(result.error_message) if result.failed?
        result
      end

      def default_response(adjustment_amount:)
        { result: GitHub::Billing::Result.success, refund_amount: adjustment_amount }
      end
    end
  end
end
