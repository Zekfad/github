# frozen_string_literal: true

class Billing::Zuora::HostedPaymentsPage
  SPAMMY_MESSAGE = "You cannot add or update a payment method because your account has been flagged. If you believe this is a mistake, contact support"
  RSA_SIGNATURE_GENERATION_ERROR = "We are having trouble contacting our credit cards processing provider. Please use PayPal or try again later."

  # Factory method used to return arguments necessary to initialize a Hosted Payments Page
  #
  # page_id - ID of the hosted payments page that will be rendered
  # target    - currently logged in target(user/org) that will view the rendered payment page
  # style   - Zuora specific argument on whether the page will be inline or framed
  # submit_enabled - Whether the submit button should show up
  # account_id - Optional argument of the Zuora account to link the new payment method after verification
  #
  # Returns Hash or HostedPaymentsPage
  def self.params(page_id:, target:, style: "inline", submit_enabled: "true", account_id: nil)
    if target.spammy?
      return error_response(SPAMMY_MESSAGE)
    end

    if target.has_any_trade_restrictions?
      return error_response(TradeControls::Notices::Plaintext.user_account_restricted)
    end

    rsa_signature = ::Billing::Zuora::Signature::RSA.generate(uri: GitHub.zuora_payment_page_uri, page_id: page_id)
    unless rsa_signature
      return error_response(RSA_SIGNATURE_GENERATION_ERROR)
    end

    new(
      page_id: page_id,
      target: target,
      style: style,
      submit_enabled: submit_enabled,
      account_id: account_id,
      rsa_signature: rsa_signature,
    )
  end

  def initialize(page_id:, target:, rsa_signature:, style: "inline", submit_enabled: "true", account_id: nil)
    @page_id = page_id
    @target = target
    @rsa_signature = rsa_signature
    @style = style
    @submit_enabled = submit_enabled
    @account_id = account_id
  end

  def as_json(options = {})
    params = {
      id: page_id,
      key: rsa_signature.key,
      signature: rsa_signature.signature,
      tenantId: rsa_signature.tenant_id,
      token: rsa_signature.token,
      url: GitHub.zuora_payment_page_uri,
      countryBlackList: TradeControls::Countries.billing_address_blacklist_alpha3,
      paymentGateway: ::Billing::Zuora::PaymentGateway.for(target, type: :credit_card),
      style: style,
      submitEnabled: submit_enabled,
    }.merge(prefill_credit_card_details)

    params[:field_accountId] = account_id if account_id

    params
  end

  private_class_method def self.error_response(message)
    {
      error: message,
    }
  end

  private

  attr_reader :page_id, :target, :rsa_signature, :style, :submit_enabled, :account_id

  def prefill_credit_card_details
    params = development_credit_card_details
    if GitHub.flipper[:name_address_collection].enabled?(@target) && @target.user_personal_profile.present?
      params[:prepopulate] ||= {}
      params[:prepopulate].merge!({
        creditCardAddress1: @target.user_personal_profile&.address1,
        creditCardCountry: @target.user_personal_profile&.country&.alpha3,
        creditCardState: @target.user_personal_profile&.region,
        creditCardCity: @target.user_personal_profile&.city,
        creditCardPostalCode: @target.user_personal_profile&.postal_code,
        creditCardHolderName: @target.user_personal_profile&.fullname,
      })
    end
    params
  end

  def development_credit_card_details
    return {} unless Rails.env.development?

    {
      prepopulate: {
        creditCardNumber: rsa_signature.public_encrypt("4111111111111111"),
        creditCardExpirationMonth: rsa_signature.public_encrypt("01"),
        creditCardExpirationYear: rsa_signature.public_encrypt("2040"),
        cardSecurityCode: rsa_signature.public_encrypt("000"),
        creditCardAddress1: "88 Colin P Kelly Jr St",
        creditCardCountry: "USA",
        creditCardState: "California",
        creditCardCity: "San Francisco",
        creditCardPostalCode: "94107",
        creditCardHolderName: "Mona Lisa",
      },
    }
  end
end
