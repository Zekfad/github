# frozen_string_literal: true

class Billing::Zuora::PaymentMethod

  def self.find(id)
    response = GitHub.zuorest_client.get_payment_method(id)
    new(response)
  rescue Zuorest::HttpError
    nil
  end

  def initialize(payment_method_attributes)
    @attributes = payment_method_attributes
  end

  def id
    attributes["Id"]
  end

  def masked_number
    attributes["CreditCardMaskNumber"]
  end

  def expiration_month
    attributes["CreditCardExpirationMonth"]
  end

  def expiration_year
    attributes["CreditCardExpirationYear"]
  end

  def card_type
    attributes["CreditCardType"]
  end

  def type
    attributes["Type"]
  end

  def postal_code
    attributes["CreditCardPostalCode"]
  end

  def state
    attributes["CreditCardState"]
  end

  def paypal_email
    attributes["PaypalEmail"]
  end

  def fingerprint
    if attributes["TokenId"]
      braintree_payment_method_id = JSON.parse(attributes["TokenId"]).values.first
      Braintree::PaymentMethod.find(braintree_payment_method_id).unique_number_identifier
    else
      response = GitHub.zuorest_client.query_action({
        queryString: "select responsestring from paymentmethodtransactionlog where paymentmethodid = '#{id}' limit 1",
      })

      response.dig("records", 0, "ResponseString").match(/fingerprint\"\: \"(\w+)\"/)&.captures&.dig(0)
    end
  rescue Braintree::NotFoundError, Zuorest::HttpError
    nil
  end

  def credit_card?
    type == "CreditCard"
  end

  def paypal?
    !credit_card?
  end

  private

  attr_reader :attributes
end
