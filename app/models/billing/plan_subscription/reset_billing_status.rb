# frozen_string_literal: true

class Billing::PlanSubscription::ResetBillingStatus
  def self.perform(*args, **options); new(*args, **options).perform; end

  def initialize(plan_subscription, balance: 0, next_billing_date:)
    @plan_subscription      = plan_subscription
    @balance                = balance
    @next_billing_date      = next_billing_date.to_s.to_date
    @user                   = plan_subscription.user
  end

  def perform
    plan_subscription.update(balance_in_cents: balance)
    # We're using `update_column` here to avoid spikes in user search indexing.
    #
    # https://github.com/github/availability/issues/658
    user.update_columns(billed_on: next_billing_date, billing_attempts: 0)

    user.enable_or_disable!
    user.rebuild_asset_status
  end

  private

  attr_reader :balance,
              :next_billing_date,
              :plan_subscription,
              :user
end
