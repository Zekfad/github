# rubocop:disable Style/FrozenStringLiteralComment

class Billing::PlanSubscription::CreateBillingTransaction
  def self.perform(*args, **options); new(*args, **options).perform; end

  delegate :seats_delta, :asset_packs_delta, :charge_type,
    to: :charge_type_resolver

  # Initialize a new CreateBillingTransaction
  #
  # plan_subscription     - The user's PlanSubscription
  # service_ends_at       - The date when the user's current billing period ends
  # zuora_transaction     - The associated ZuoraPayment for the transaction
  def initialize(plan_subscription, service_ends_at:, zuora_transaction: nil)
    @plan_subscription      = plan_subscription
    @zuora_transaction      = zuora_transaction
    @service_ends_at        = service_ends_at
  end

  # Public: Create the BillingTransaction based on a Zuora payment transaction
  #
  # Returns a Billing::BillingTransaction
  def perform
    create_from_zuora_transaction
  end

  private

  # Internal: Create a BillingTransaction from a ZuoraPayment object
  #
  # Returns Billing::BillingTransaction
  def create_from_zuora_transaction
    Billing::BillingTransaction.retry_on_find_or_create_error do
      transaction_id = zuora_transaction.reference_id || zuora_transaction.payment_number
      billing_transaction = Billing::BillingTransaction.find_by(user_id: user.id, transaction_id: transaction_id) ||
        Billing::BillingTransaction.new(user_id: user.id, transaction_id: transaction_id)

      billing_transaction.amount_in_cents = zuora_transaction.amount_in_cents
      billing_transaction.created_at = zuora_transaction.created_date
      billing_transaction.service_ends_at = service_ends_at
      billing_transaction.platform = :zuora
      billing_transaction.platform_transaction_id = zuora_transaction.id
      billing_transaction.seats_delta = seats_delta
      billing_transaction.asset_packs_delta = asset_packs_delta

      zuora_transaction.decorate_billing_transaction(billing_transaction)

      # Saves billing_transaction
      billing_transaction.log_recurring_charge(
        user: user,
        invoiced_items: zuora_transaction.invoice_items,
        charge_type: charge_type,
      )

      GitHub.dogstats.count("cream", billing_transaction.amount_in_cents)

      billing_transaction
    end
  end

  def charge_type_resolver
    @charge_type_resolver ||= ChargeTypeResolver.new(
      payment: zuora_transaction,
      recurring_amount: user.payment_amount,
    )
  end

  # Internal: The GitHub user associated with this subscription
  #
  # Returns User|Organization
  def user
    plan_subscription.user
  end

  attr_reader :plan_subscription, :service_ends_at, :zuora_transaction
end
