# rubocop:disable Style/FrozenStringLiteralComment

# Generate the params to pass to Braintree when creating or updating
# a Braintree::Subscription from our Billing::PlanSubscription.
#
# see: https://developers.braintreepayments.com/reference/request/subscription/create/ruby#add_ons
class Billing::PlanSubscription::BraintreeParams
  PER_SEAT_ADDON_ID              = "seat"
  DATA_PACK_ADDON_ID             = "asset_pack"
  DISCOUNT_ID                    = "coupon"

  # Descriptor is what customers will see on their bank statement when they
  # make a purchase. We use dynamic descriptors to add in an encoded user_id so
  # that support can match up a bank statement to a user account (see
  # User#custom_descriptor).
  #
  # NB: The url descriptor field must be 13 characters or fewer and can only
  # contain letters, numbers and periods.
  #
  # References:
  #   https://articles.braintreepayments.com/control-panel/transactions/descriptors
  #   https://halp.githubapp.com/discussions/374feb3ee51f11e49ff1e0b7fe3c8730
  BILLING_DESCRIPTOR_URL = "github.com"

  attr_reader :plan_subscription

  delegate \
    :active_subscription_items,
    :additional_seats,
    :base_price,
    :braintree_subscription,
    :data_packs,
    :data_packs_unit_price,
    :discount,
    :payment_method_token,
    :plan,
    :plan_duration,
    :seats,
    :subscription_items,
    :unit_price,
    :user,
    :yearly_plan?,
    to: :plan_subscription

  delegate :add_ons, :discounts, to: :braintree_subscription, allow_nil: true

  def initialize(plan_subscription:)
    @plan_subscription = plan_subscription
  end

  def create_params
    subscription_params.merge(first_billing_date: GitHub::Billing.today)
  end

  def update_params(prorate_charges: true, revert_subscription_on_proration_failure: false)
    subscription_params.merge(
      options: {
        prorate_charges: prorate_charges,
        revert_subscription_on_proration_failure: revert_subscription_on_proration_failure,
      },
    )
  end

  def subscription_params
    {
      plan_id: plan_id,
      price: base_price.format(symbol: false, delimiter: false),
      discounts: discount_params,
      add_ons: add_on_params,
      payment_method_token: payment_method_token,
      descriptor: {
        name: user.custom_descriptor,
        url: BILLING_DESCRIPTOR_URL,
      },
    }
  end

  def add_on_params
    [additions, updates, removals]
      .compact
      .inject({}, &:merge)
      .tap do |params|
      if params[:remove]
        params[:remove].delete_if do |addon_id|
          params[:update]&.any? { |update| update[:existing_id] == addon_id }
        end
        params.delete(:remove) if params[:remove].empty?
      end
    end
  end

  def discount_params
    if discount > 0
      discounts&.any? ? update_discount : add_discount
    else
      discounts&.any? ? remove_discount : {}
    end
  end

  def credit_params
    {
      price: "0.00",
      add_ons: { update: add_on_credit_params },
      options: { prorate_charges: true },
    }
  end

  private

  def add_on_credit_params
    add_ons.map do |add_on|
      { existing_id: add_on.id, amount: "0.00" }
    end
  end

  def plan_id
    plan_id = plan.name
    plan_id += "_yearly" if yearly_plan?
    plan_id
  end

  def additions
    additions = []
    additions << seat_params      unless has_add_on?(PER_SEAT_ADDON_ID)
    additions << data_pack_params unless has_add_on?(DATA_PACK_ADDON_ID)

    subscription_items.each do |item|
      unless has_add_on?(item.braintree_addon_slug)
        additions << subscription_item_params(item)
      end
    end

    additions.compact!
    { add: additions } if additions.size > 0
  end

  def updates
    updates = []
    updates << seat_params      if has_add_on?(PER_SEAT_ADDON_ID)
    updates << data_pack_params if has_add_on?(DATA_PACK_ADDON_ID)

    subscription_items.each do |item|
      if has_add_on?(item.braintree_addon_slug)
        updates << subscription_item_params(item)
      end
    end

    updates.compact!
    { update: updates } if updates.size > 0
  end

  def removals
    removals = []
    # NB: Per Repo plans will return 0 for additional seats via PlanSubscription
    removals << PER_SEAT_ADDON_ID  if has_add_on?(PER_SEAT_ADDON_ID)  && additional_seats.zero?
    removals << DATA_PACK_ADDON_ID if has_add_on?(DATA_PACK_ADDON_ID) && data_packs.zero?

    subscription_items.each do |item|
      next if active_subscription_items.map(&:listing).include?(item.listing) && item.paid?
      if has_add_on?(item.braintree_addon_slug) && (item.quantity.zero? || !item.paid?)
        removals << item.braintree_addon_slug
      end
    end

    removals.compact!
    { remove: removals } if removals.size > 0
  end

  def subscription_item_params(item)
    return unless item.billable?
    build_add_on_param \
      id: item.braintree_addon_slug,
      quantity: item.quantity,
      amount: item.base_price(duration: plan_duration)
  end

  def seat_params
    return unless plan.per_seat? && additional_seats > 0
    build_add_on_param id: PER_SEAT_ADDON_ID, quantity: additional_seats, amount: unit_price
  end

  def data_pack_params
    return unless data_packs > 0
    build_add_on_param id: DATA_PACK_ADDON_ID, quantity: data_packs, amount: data_packs_unit_price
  end

  # Private: Builds an 'add_on' hash for Braintree.
  #
  # Example:
  #   For 'new' add_ons:
  #     { inherited_from_id: 'seats', quantity: 1, amount: "1.00" }
  #
  #   For 'upgrade' add_ons:
  #     { existing_id: 'seats', quantity: 1, amount: "1.00" }
  #
  # id       - A String of the id for the add_ons, e.g 'seats', 'asset_packs'.
  # quantity - An Integer of the total units for this add_ons.
  # amount   - A Billing::Money of the per-unit/flat rate cost of the add_ons.
  #
  #
  # Returns a Hash
  def build_add_on_param(id:, quantity:, amount:)
    params      = { quantity: quantity, amount: amount.format(symbol: false, delimiter: false) }
    key         = has_add_on?(id) ? :existing_id : :inherited_from_id
    params[key] = id.to_s
    params
  end

  # Private: Checks if a given addon exists for the id
  #
  # Returns a Boolean
  def has_add_on?(id)
    !!(add_ons || []).detect { |addon| addon.id == id.to_s }
  end

  def add_discount
    {
      add: [{ inherited_from_id: DISCOUNT_ID, amount: discount.format(symbol: false, delimiter: false) }],
    }
  end

  def remove_discount
    { remove: [DISCOUNT_ID] }
  end

  def update_discount
    {
      update: [{ existing_id: DISCOUNT_ID, amount: discount.format(symbol: false, delimiter: false) }],
    }
  end
end
