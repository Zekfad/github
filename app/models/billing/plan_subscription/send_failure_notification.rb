# frozen_string_literal: true

class Billing::PlanSubscription::SendFailureNotification
  def self.perform(*args, **options); new(*args, **options).perform; end

  def initialize(plan_subscription, marketplace: false, message:)
    @message = message
    @marketplace = marketplace
    @user    = plan_subscription.user
  end

  def perform
    if user.has_paypal_account?
      BillingNotificationsMailer.paypal_failure(user, message).deliver_later
    elsif user.card_expired?
      BillingNotificationsMailer.cc_expired_failure(user).deliver_later
    elsif marketplace
      BillingNotificationsMailer.marketplace_failure(user, message).deliver_later
    else
      BillingNotificationsMailer.cc_failure(user, message).deliver_later
    end
  end

  private

  attr_reader :user, :message, :marketplace

end
