# frozen_string_literal: true

module Billing
  module PlanSubscription::Synchronization
    # Public: Queues up a SynchronizePlanSubscription job for the user that
    # owns this PlanSubscription.
    #
    # Returns nothing
    def synchronize_later
      SynchronizePlanSubscriptionJob.perform_later({"user_id" => user.id})
    end

    # Synchronize pending plan changes to the associated external subscription
    # for this PlanSubscription.
    #
    # Returns true if successful.
    def synchronize(set_billing_date_today: true, retries: 0)
      return if user.invoiced?
      return if user.organization? && user.business.present?
      return unless zuora_user?

      begin
        attach_orphaned_zuora_subscription if !zuora_subscription_number?

        result = if has_external_subscription?
          PlanSubscription::Synchronizer.update(self)
        elsif should_create_subscription?
          PlanSubscription::Synchronizer.create(self, set_billing_date_today)
        end
      rescue => error
        instrument_synchronization_event(SynchronizationErrorEvent.new(error: error, retries: retries))
        raise
      end

      result_event = SynchronizationResultEvent.new(result: result, retries: retries)
      instrument_synchronization_event(result_event)

      raise Billing::Zuora::SynchronizationError.new(result_event.error_message) if result_event.retryable?

      if result_event.success?
        ::Billing::UpdateSkippedMeteredLineItemsJob.perform_later(billable_owner: user)
      end

      result_event.success?
    end

    # Synchronize pending plan changes to the associated external subscription
    # for this PlanSubscription with a lock to prevent simultaneous updates.
    #
    # Returns true if successful.
    def synchronize_with_lock(**kwargs)
      restraint.lock!(lock_key, _n = 1, _ttl = 3.minutes) do
        reload
        synchronize(**kwargs)
      end
    end

    private

    def should_create_subscription?
      user.has_valid_payment_method? || payment_amount > 0
    end

    # Internal: Instruments event for billing.plan_subscription_synchronize and
    # will also increment GitHub.dogstats if necessary
    #
    # Returns nothing
    def instrument_synchronization_event(event)
      instrumentor = SynchronizationInstrumentor.new(
        plan_subscription: self,
        event: event,
      )

      instrumentor.instrument
    end

    # Internal: The lock key used to prevent simultaneous updates
    #
    # Returns String
    def lock_key
      "user-#{user_id}-synchronization"
    end

    # Internal: The restraint for locking and preventing simultaneous updates
    #
    # Returns GitHub::Restraint
    def restraint
      @restraint ||= GitHub::Restraint.new
    end

    class SynchronizationErrorEvent
      attr_reader :retries

      # Internal: Initialize a SynchronizationErrorEvent.
      #
      # error - An Exception returned from PlanSubscription::Synchronizer#create or #update
      # retries - Number of PlanSubscription::Synchronizer#synchronize retries
      def initialize(error:, retries:)
        @error = error
        @retries = retries
      end

      # Internal: Whether this event was successful.  Since this is an error it always returns false.
      #
      # returns Boolean
      def success?
        false
      end

      # Internal: Whether this event was a failure.  Since this is an error it always returns true.
      #
      # returns Boolean
      def failure?
        true
      end

      # Internal: Message describing this error event
      #
      # returns String
      def error_message
        @error.to_s
      end

      # Internal: Attributes that uniquely describe this event.
      #
      # returns Hash including the error message
      def attributes
        { error: error_message }
      end

      # Internal: Whether this job will be retried automatically after this event
      #
      # returns Boolean
      def retryable?
        GitHub::Jobs::AbstractBraintreeJob::RETRYABLE_ERRORS.include?(@error.class)
      end

      # Internal: Whether to track the first try of this event. We do don't want to count retryable failures initially.
      #
      # returns Boolean
      def track_first_result?
        success? || non_retryable_failure?
      end

      private

      def non_retryable_failure?
        failure? && !retryable?
      end
    end

    class SynchronizationResultEvent
      attr_reader :retries

      # Internal: Initialize a SynchronizationResultEvent.
      #
      # result - A GitHub::Billing::Result returned from PlanSubscription::Synchronizer#create or #update
      # retries - Number of PlanSubscription::Synchronizer#synchronize retries
      def initialize(result:, retries:)
        @result = result
        @retries = retries
      end

      # Internal: Whether this event was successful.
      #
      # returns Boolean
      def success?
        !failure?
      end

      # Internal: Message describing this event
      #
      # returns String
      def failure?
        @result.respond_to?(:failed?) && @result.failed?
      end

      # Internal: Whether this event was a failure.  Since this is an error it always returns true.
      #
      # returns String
      def error_message
        @result.error_message
      end

      # Internal: Attributes that uniquely describe this event.
      #
      # returns Hash including error details on failure
      def attributes
        return {} if success?

        {
          external_result: @result.external_result,
          error: error_message,
        }
      end

      # Internal: Whether this job will be retried automatically after this event
      #
      # returns Boolean
      def retryable?
        failure? && !declined?
      end

      # Internal: Whether to track the first try of this event
      # We don't want to count retryable failures or declines initially.
      #
      # returns Boolean
      def track_first_result?
        success? || (non_retryable_failure? && !declined?)
      end

      private

      def declined?
        @result.respond_to?(:declined?) && @result.declined?
      end

      def non_retryable_failure?
        failure? && !retryable?
      end
    end

    class SynchronizationInstrumentor

      # Internal: Initialize a SynchronizationInstrumentor.
      #
      # plan_subscription - PlanSubscription sent into PlanSubscription::Synchronizer#synchronize
      # event - A SynchronizationResultEvent or SynchronizationErrorEvent
      def initialize(plan_subscription:, event:)
        @plan_subscription = plan_subscription
        @event = event
      end

      # Internal: Will instrument the event to the given plan_subscription.
      #
      # It will instrument each event and only increment the SLO if it's the third retryable error, or
      # the first try when it's a successful, not retryable error, and not a decline.
      def instrument
        @plan_subscription.instrument(:synchronize, **attributes)

        increment_slo if increment_slo?
      end

      private

      def attributes
        @event.attributes.merge(
          success: @event.success?,
          external_subscription_type: @plan_subscription.sync_platform_type,
        )
      end

      def increment_slo?
        @event.track_first_result? || third_failed_retry?
      end

      def increment_slo
        GitHub.dogstats.increment("billing.plan_subscription_synchronize", {
          tags: ["success:#{@event.success?}"],
        })
      end

      def third_failed_retry?
        @event.failure? && @event.retries == 3
      end
    end
  end
end
