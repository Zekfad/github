# frozen_string_literal: true

module Billing
  class PlanSubscription::ZuoraSynchronizer < PlanSubscription::Synchronizer
    attr_reader :plan_subscription
    attr_reader :zuora_response

    DECLINED_MESSAGE = "Your payment method has been declined."
    FAILURE_MESSAGE  = "There was an issue processing your payment method."
    ALREADY_CANCELED = /Only activated subscription can be cancelled\./

    ZUORA_VERSION_HEADER = { "zuora-version" => "211.0" }.freeze


    delegate \
      :plan,
      :user,
      :zuora_params,
      :zuora_subscription,
      :zuora_subscription_number,
      to: :plan_subscription

    delegate \
      :cancel_params,
      :changing_duration?,
      :create_params,
      :rate_plans,
      :update_params,
      to: :zuora_params

    def initialize(plan_subscription, set_billing_date_today = true)
      @plan_subscription = plan_subscription
      @set_billing_date_today = set_billing_date_today
    end

    # Public: Create a new Zuora subscription
    #
    # Success: Stores the new zuora_subscription_number on the  plan subscription record
    # Failure: Increments billing attempts, enters the user into dunning, and sends failure notification
    #
    # Returns GitHub::Billing::Result
    def create
      return GitHub::Billing::Result.failure("trade restricted user") if user.has_any_trade_restrictions?
      result = create_zuora_order
      if result.failed?
        user.increment!(:billing_attempts)

        message = error_response_for(result.error_message)
        user.dun_subscription(message)
        PlanSubscription::SendFailureNotification.perform \
          plan_subscription,
          message: message
      end
      result
    end

    def update
      # If the subscription is in the middle of being migrated, do not update
      # it now; schedule another update for later when the migration will
      # hopefully have finished
      if subscription_in_migration?
        SynchronizePlanSubscriptionJob.set(wait: 2.hours).perform_later("user_id" => user.id)
        return GitHub::Billing::Result.failure("Cannot sync subscription while it is being migrated; will retry in 2 hours")
      end

      result = if plan.free? && !user.metered_billing_overage_allowed?
        cancel_for_downgrade_to_free
      elsif changing_duration?
        cancel_and_recreate
      else
        result = update_subscription
        update_balance

        result
      end

      user.remove_gated_features if result&.success?

      result
    end

    def cancel
      result = cancel_zuora_subscription(zuora_subscription)

      if result.success? || already_canceled?(result)
        plan_subscription&.clear_external_subscription_references
        user&.remove_gated_features
        result = result.success? ? result : GitHub::Billing::Result.success
      end

      result
    end

    private

    def already_canceled?(result)
      result.failed? && result.error_message.match(ALREADY_CANCELED).present?
    end

    def cancel_zuora_subscription(zuora_subscription)
      return GitHub::Billing::Result.success if zuora_subscription.nil? || zuora_subscription.cancelled?

      @zuora_response = zuora_subscription.cancel
      GitHub::Billing::Result.from_zuora(@zuora_response)
    end

    # Internal: Create a subscription through the zuora api
    #
    # Stores the new zuora_subscription_number on the plan_subscription record
    #
    # Returns GitHub::Billing::Result
    def create_zuora_order
      return GitHub::Billing::Result.success if rate_plans.empty?

      update_bill_cycle_day if @set_billing_date_today

      begin
        subscription_number = create_via_subscriptions_endpoint

        result = GitHub::Billing::Result.from_zuora(@zuora_response)
        update_plan_subscription!(subscription_number) if result.success?
        result
      rescue ::Faraday::TimeoutError
        plan_subscription.attach_orphaned_zuora_subscription
        raise
      end
    end

    # Internal: Add the zuora information to the plan_subscription
    #
    # Returns: boolean
    # Raises ActiveRecord::RecordInvalid if the plan_subscription is invalid
    def update_plan_subscription!(subscription_number)
      plan_subscription.zuora_subscription_number = subscription_number
      plan_subscription.save!
      plan_subscription.update_from_zuora_subscription
    end

    # Internal: create a subscription on zuora with the subscriptions endpoint
    #
    # Returns a string
    def create_via_subscriptions_endpoint
      @zuora_response = GitHub.zuorest_client.create_subscription create_params, ZUORA_VERSION_HEADER
      if invoice_id = @zuora_response["invoiceId"] and invoice_id.present?
        CollectZuoraInvoiceJob.perform_later(user.customer.zuora_account_id, invoice_id)
      end
      @zuora_response["subscriptionNumber"]
    end

    def update_bill_cycle_day
      return unless user.customer.zuora?
      result = user.zuora_account.update!({
        BcdSettingOption: "ManualSet",
        BillCycleDay: GitHub::Billing.today.day,
      }).first
      if GitHub::Billing::Result.from_zuora(result).failed?
        Failbot.report(
          StandardError.new("Updating bill cycle day failed"),
          areas_of_responsibility: [:gitcoin],
          catalog_service: :zuora_integration,
          user: user,
          message: result.error_message,
        )
      end
    end

    def update_subscription
      @zuora_response = GitHub.zuorest_client.update_subscription \
        plan_subscription.zuora_subscription_number,
        update_params, ZUORA_VERSION_HEADER

      result = GitHub::Billing::Result.from_zuora(@zuora_response)
      if result.success?
        plan_subscription.update_from_zuora_subscription

        invoice = find_invoice(@zuora_response["invoiceId"])
        if invoice && invoice["Balance"].negative?
          amount = invoice["Balance"].abs
          GitHub.zuorest_client.create_credit_balance_adjustment({
            Type: "Increase",
            Amount: amount,
            Comment: "GitHub - transfer #{amount} to account balance",
            SourceTransactionId: invoice["Id"],
          })
        end
      else
        Billing::Zuora::MarketplaceRollback.perform(plan_subscription, error_response_for(result.error_message))
      end
      result
    end

    def find_invoice(invoice_id)
      return nil unless invoice_id.present?

      GitHub.zuorest_client.get_invoice(invoice_id)
    rescue Zuorest::HttpError
      nil
    end

    # Internal: Update the account balance from Zuora
    #
    # Returns nothing
    def update_balance
      zuora_account = user.customer.zuora_account
      metrics = zuora_account["metrics"] || {}
      balance = metrics["balance"] || 0

      plan_subscription.update(balance_in_cents: (balance * 100).to_i)
    end

    def cancel_and_recreate
      result = plan_subscription.cancel_external_subscription
      if result.failed?
        Failbot.report(
          StandardError.new("Cancellation failed"),
          areas_of_responsibility: [:gitcoin],
          catalog_service: :zuora_integration,
          user: plan_subscription.user,
          zuora_result: result.zuora_result)
      else
        plan_subscription.synchronize
        GitHub::Billing::Result.success
      end

      result
    end

    def error_response_for(message)
      return DECLINED_MESSAGE if message.to_s.downcase.include?("declined")
      FAILURE_MESSAGE
    end

    # Internal: Is this subscription in the middle of being migrated?
    #
    # Returns true if the Zuora account is in the Batch11 billing batch;
    # after the migration is complete and a Catch Up Bill Run is performed,
    # accounts are moved from Batch11 to their permanent home in Batch10
    def subscription_in_migration?
      zuora_account = user.zuora_account
      basic_info = zuora_account["basicInfo"] || {}
      basic_info["batch"] == "Batch11"
    end
  end
end
