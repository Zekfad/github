# frozen_string_literal: true

module Billing
  class ZuoraPayment
    # A wrapper for Zuora Payment objects that were processed by PayPal
    class PaypalPayment
      attr_reader :gateway_state,
        :payment_method_snapshot,
        :processor_response,
        :processor_response_code

      class UnknownGatewayState < StandardError; end

      # Public: Initialize a new ZuoraPayment::PayPal
      #
      # zuora_payment - A Billing::ZuoraPayment object
      def initialize(zuora_payment)
        @gateway_state = zuora_payment.gateway_state
        @payment_method_snapshot = zuora_payment.payment_method_snapshot
        @payment_number = zuora_payment.payment_number
        @reference_id = zuora_payment.reference_id
        @processor_response = zuora_payment.gateway_response
        @processor_response_code = zuora_payment.gateway_response_code
      end

      # Public: Decorate a BillingTransaction object with PayPal details
      #
      # See Billing::ZuoraPayment#decorate_billing_transaction for more
      # information.
      #
      # billing_transaction - A Billing::BillingTransaction object to decorate
      #
      # Returns nothing
      def decorate_billing_transaction(billing_transaction)
        billing_transaction.payment_type = :paypal
        billing_transaction.paypal_email = payment_method_snapshot["PaypalEmail"]
        billing_transaction.last_status = transaction_status
        billing_transaction.transaction_id = transaction_id
      end

      # Public: The Billing::BillingTransactionStatuses status of the transaction
      # based on the normalized state from Zuora
      #
      # Returns Integer
      def transaction_status
        case gateway_state
        when "Submitted"
          Billing::BillingTransactionStatuses::ALL[:settled]
        when "NotSubmitted"
          Billing::BillingTransactionStatuses::ALL[:processor_declined]
        else
          Failbot.report(
            UnknownGatewayState.new("received unknown Paypal gateway state `#{gateway_state}` from Zuora for `#{payment_number}`"),
          )

          nil
        end
      end

      private

      attr_reader :payment_number, :reference_id

      # Private: the reference number for the transaction.
      # Since Paypal does not give Zuora a transaction ID for failed transactions
      # we use Zuora's payment number to reference it
      #
      # Returns String
      def transaction_id
        if transaction_status == Billing::BillingTransactionStatuses::ALL[:settled]
          reference_id
        else
          payment_number
        end
      end
    end
  end
end
