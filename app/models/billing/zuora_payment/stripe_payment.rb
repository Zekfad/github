# frozen_string_literal: true

module Billing
  class ZuoraPayment
    class StripePayment
      attr_reader :transaction_id,
        :processor_response,
        :processor_response_code

      def initialize(zuora_payment)
        @transaction_id = zuora_payment.reference_id
        @processor_response = zuora_payment.gateway_response
        @processor_response_code = zuora_payment.gateway_response_code
        @bank_identification_number = zuora_payment.bank_identification_number
      end

      def decorate_billing_transaction(billing_transaction)
        billing_transaction.payment_type = :credit_card

        unless stripe_transaction
          billing_transaction.last_status = :failed
          return
        end

        billing_transaction.bank_identification_number = bank_identification_number
        billing_transaction.last_four = credit_card_details.last4
        billing_transaction.country_of_issuance = credit_card_details.country
        billing_transaction.last_status = transaction_status
      end

      private

      attr_reader :bank_identification_number

      def stripe_transaction
        @stripe_transaction ||= begin
                                  ::Stripe::Charge.retrieve(transaction_id)
                                rescue ::Stripe::InvalidRequestError
                                  nil
                                end
      end

      def credit_card_details
        stripe_transaction&.payment_method_details&.card
      end

      def transaction_status
        case stripe_transaction&.status
        when "succeeded"
          Billing::BillingTransactionStatuses::ALL[:settled]
        when "pending"
          Billing::BillingTransactionStatuses::ALL[:settlement_pending]
        when "failed"
          Billing::BillingTransactionStatuses::ALL[:processor_declined]
        else
          Failbot.report(
            UnknownGatewayState.new("received unknown Stripe gateway state `#{stripe_transaction.status}`"),
            billing_transaction_id: transaction_id,
          )

          nil
        end
      end
    end
  end
end
