# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class CreateCustomer
    include GitHub::Billing::CreditCard

    attr_accessor :target, :customer, :payment_method, :details

    class BraintreeCreationError < StandardError; end

    # Public: Generate the Network for a paying Account. This will
    # associate the User, PaymentMethod, Customer and CustomerAccount.
    # Existing Customer objects and PaymentMethods, even orphaned ones, will
    # be re-used.
    #
    # target  - A User, Organization, or Business to generate a paying account
    # details - A Hash of payment details:
    #   :zuora_payment_method_id - The payment method ID returned by Zuora's Hosted Payment Page
    #   :coupon_code      - String coupon code (optional).
    #   :billing_extra    - String extra billing information.
    #   :vat_code         - String VAT identification number
    #   :paypal_nonce     - String nonce representing a paypal account (optional).
    #   :billing_address  - The billing address as a Hash of optionally encrypted CC info
    #     * :country_code_alpha3 - Country as a 3-letter abbreviation String
    #     * :region              - Region as a String
    #     * :postal_code         - Postal code as a String
    #
    # Returns a Billing::CreateCustomer
    def self.perform(*args)
      new(*args).perform
    end

    def initialize(target, details = {})
      @target  = target
      @details = details

      if target.is_a?(Business)
        @customer = target.customer || Customer.new(name: target.slug)
      elsif target.business
        @customer = target.business.customer
      else
        @customer = target.customer || Customer.new(name: target.login, vat_code: details[:vat_code])
      end

      if !target.is_a?(Business)
        @payment_method = orphaned_payment_method ||  PaymentMethod.build_from_payment_details(details)
      end
    end

    def perform
      if @details.blank?
        @customer.save!
        attach_customer_to_user
        return self
      end

      @target.check_for_spam
      return self if @target.spammy?

      if @details[:apple_iap_subscription]
        create_remote_customer_without_billing
      else
        create_remote_customer
      end

      self
    end

    def response
      if @target.spammy?
        GitHub::Billing::Result.failure("This account has been flagged. #{GitHub.support_link_text} for further information.")
      else
        if @zuora_response
          result = GitHub::Billing::Result.from_zuora(@zuora_response)
        else
          result = GitHub::Billing::Result.from_braintree(@braintree_response)
        end
        result.record = @customer if result.success?
        result
      end
    end

    def error_message
      response.error_message
    end

    def success?
      return @customer.persisted? unless @details.present?
      @braintree_response&.success? || @zuora_response.to_h[:success]
    end

    private

    def create_payment_event
      if @target.business
        @payment_method.reload.instrument_create @target.business.slug
      else
        @payment_method.reload.instrument_create @target
      end
    end

    def create_remote_customer
      create_zuora_customer

      if success?
        @customer.payment_method = @payment_method
        @customer.save!
        create_payment_event
        attach_customer_to_user

        check_if_blacklisted
        update_user_billing
        log_billing_creation

        # TODO: Charge businesses
        unless @target.business
          @target.recurring_charge
        end
      end
    end

    def create_remote_customer_without_billing
      # create a zuora account without any billing information
      @zuora_response = GitHub.zuorest_client.create_account({
        autoPay: false,
        BusinessSegment__c: "Self-Serve",
        batch: "Batch10",
        SynctoNetSuite__NS: "No",
        billCycleDay: 0,
        communicationProfileId: GitHub.zuora_self_serve_communication_profile_id,
        currency: "USD",
        name: external_name,
        paymentGateway: ::Billing::Zuora::PaymentGateway.for(@target, type: :credit_card),
        billToContact: {
          firstName: external_name,
          lastName: external_name,
          # using GitHub HQ here to represent the billToContact address since zuora
          # requires billing info to create an account
          country: "USA",
          state: "CA",
        },
        taxInfo: {
          exemptStatus: "Yes",
          exemptCertificateId: "N/A",
        },
      }).symbolize_keys
      return unless @zuora_response[:success]

      # update customer information from zoura
      @customer.zuora_account_id = @zuora_response[:accountId]
      @customer.zuora_account_number = @zuora_response[:accountNumber]
      @customer.save!

      attach_customer_to_user
    end

    def log_billing_creation
      if @target.business
        GitHub::Logger.log \
          at: "billing.create_customer.billing_charge",
          business_name: @target.business.name,
          success: success?,
          error: error_message
      else
        GitHub::Logger.log \
          at: "billing.create_customer.billing_charge",
          login: @target.login,
          success: success?,
          error: error_message
      end
    end

    def create_zuora_customer
      @zuora_response = create_zuora_account
      if @zuora_response[:success]
        attach_zuora_account_to_customer
      end
    end

    def external_name
      target.business ? target.business.name : target.login
    end

    def external_email
      if target.business
        target.business.customer&.billing_email&.email
      else
        target.email
      end
    end

    def bt_fields
      target.business ? {} : { user_id: target.id }
    end

    def billed_on
      if target.business
        (target.business.billing_term_ends_on + 1.day).day
      else
        target.billed_on&.day
      end || GitHub::Billing.today.day
    end

    def create_zuora_account
      if details[:paypal_nonce].present?
        zuora_response = GitHub::Billing::ZuoraPaypal.create_account(
          target_details: {
            external_name: external_name,
            external_email: external_email,
            bt_fields: bt_fields,
            billed_on: billed_on,
          },
          paypal_nonce: details[:paypal_nonce],
          billing_address: details[:billing_address],
          vat_code: details[:vat_code],
        )
      else
        zuora_response = GitHub.zuorest_client.create_account({
          autoPay: true,
          BusinessSegment__c: "Self-Serve",
          batch: "Batch10",
          SynctoNetSuite__NS: "No",
          billCycleDay: 0,
          communicationProfileId: GitHub.zuora_self_serve_communication_profile_id,
          currency: "USD",
          name: external_name,
          hpmCreditCardPaymentMethodId: details[:zuora_payment_method_id],
          paymentGateway: ::Billing::Zuora::PaymentGateway.for(@target, type: :credit_card),
          billToContact: {
            firstName: external_name,
            lastName: external_name,
            zipCode: details[:billing_address][:postal_code],
            country: details[:billing_address][:country_code_alpha3],
            state: details[:billing_address][:region],
          },
          taxInfo: {
            exemptStatus: "Yes",
            exemptCertificateId: "N/A",
          },
        })
        return zuora_response.with_indifferent_access unless zuora_response["success"]

        zuora_response[:account_number] = zuora_response["accountNumber"]
        zuora_response[:account_id] = zuora_response["accountId"]
        zuora_response[:payment_method_id] = zuora_response["paymentMethodId"]
        zuora_response[:success] = zuora_response["success"]

        # Create a Braintree customer so we can use it if we update
        # the payment method later on to use PayPal. We are using
        # Braintree's Paypal JS library so it's easier to keep track of the
        # customers by updating their payment methods instead of creating new ones
        braintree_response = Braintree::Customer.create(
          id: zuora_response["accountId"],
          email: external_email,
          first_name: external_name,
          custom_fields: bt_fields,
        )
        unless braintree_response.success?
          Failbot.report(
            BraintreeCreationError.new("Customer creation failed"),
            areas_of_responsibility: [:gitcoin],
            user: @target,
          )
          return { success: false }
        end
      end

      zuora_response
    end

    def attach_zuora_account_to_customer
      payment_method_id = @zuora_response[:payment_method_id]
      if paypal_account = @zuora_response[:paypal_account]
        @payment_method.paypal_email = paypal_account.email
      else
        zuora_payment_method = ::Billing::Zuora::PaymentMethod.find(payment_method_id)
        unless zuora_payment_method
          @zuora_response = { success: false, errors: [{code: "INVALID_VALUE", message: "Payment method not found"}] }
          return
        end

        @payment_method.truncated_number = zuora_payment_method.masked_number
        @payment_method.expiration_month = zuora_payment_method.expiration_month
        @payment_method.expiration_year = zuora_payment_method.expiration_year
        @payment_method.card_type = zuora_payment_method.card_type
        @payment_method.unique_number_identifier = zuora_payment_method.fingerprint
        @payment_method.paypal_email = nil
      end
      @customer.zuora_account_id = @zuora_response[:account_id]
      @customer.zuora_account_number = @zuora_response[:account_number]
      @customer.bill_cycle_day = billed_on

      unless @target.business
        @payment_method.user = target
      end

      @payment_method.payment_processor_type = PaymentMethod.zuora_processor_slug
      @payment_method.payment_processor_customer_id = @zuora_response[:account_id]
      @payment_method.payment_token = payment_method_id
    end

    def orphaned_payment_method
      if @target.business
        PaymentMethod.find_by_customer_id(@customer.id)
      else
        PaymentMethod.find_by_user_id(@target.id)
      end
    end

    def check_if_blacklisted
      if payment_method.blacklisted?
        BlacklistedPaymentMethod.create_from_user(@target)

        serialized_previous_classification = ::Hydro::EntitySerializer.user_spammy_classification(@target)
        serialized_previous_spammy_reason = ::Hydro::EntitySerializer.user_spammy_reason(@target)
        serialized_previously_suspended = ::Hydro::EntitySerializer.user_suspended(@target)
        @target.suspend(
          "Using blacklisted payment method",
          instrument_abuse_classification: false,
        )
        @target.safer_mark_as_spammy(
          reason: "Blacklisted payment method",
          hard_flag: true,
          instrument_abuse_classification: false,
        )

        GlobalInstrumenter.instrument "abuse_classification.publish",
          account: @target,
          queue_action: :QUEUE_ACTION_NONE,
          serialized_previous_classification: serialized_previous_classification,
          serialized_previous_spammy_reason: serialized_previous_spammy_reason,
          serialized_previously_suspended: serialized_previously_suspended
      end
    end

    def attach_customer_to_user
      if target.is_a?(Business)
        customer.business = @target
      else
        # We don't want to override the targets existing customer, or associate a
        # customer with a target that belongs to a business (since it will be set on the business)
        return if @target.customer || @target.business&.customer.present?
        account = @customer.customer_accounts.create(user: @target)
        # NB Need to refresh customer association so we have it's context when we check for
        # valid payment.
        @target.reload_customer
        account.verify!(@target)
      end
    end

    def update_user_billing
      if @details.has_key?(:billing_extra) && !target.business
        @target.billing_extra = @details[:billing_extra]
      end

      @target.transaction do
        @target.unlock_billing! if @target.disabled?
        @target.save!
      end
    end

    def zuora_card_type
      card_type = detect_card_type(@details[:credit_card][:number])

      card_type&.gsub(" ", "")
    end
  end
end
