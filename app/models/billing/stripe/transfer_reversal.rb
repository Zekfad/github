# frozen_string_literal: true

module Billing
  module Stripe
    class TransferReversal
      TransferNotFound = Class.new(StandardError)

      # Nested class responsible for calculating how much of the payment and
      # GitHub match should be reversed for each transfer
      class AmountToReverse
        attr_reader :transfer, :transfer_payment_amount, :transfer_match_amount,
          :max_payment_amount_to_reverse, :max_match_amount_to_reverse

        # Initialize AmountToReverse
        #
        # transfer                      - A Stripe::Transfer object
        # max_payment_amount_to_reverse - The maximum payment amount to reverse (optional)
        # max_match_amount_to_reverse   - The maximum GitHub match amount to reverse (optional)
        def initialize(transfer:, max_payment_amount_to_reverse: nil, max_match_amount_to_reverse: nil)
          @transfer = transfer
          @transfer_payment_amount = transfer.metadata[:payment_amount].to_i
          @transfer_match_amount = transfer.metadata[:match_amount].to_i

          @max_payment_amount_to_reverse = max_payment_amount_to_reverse || transfer_payment_amount
          @max_match_amount_to_reverse = max_match_amount_to_reverse || transfer_match_amount
        end

        # Returns Integer the amount of the payment to reverse
        def payment_amount
          [max_payment_amount_to_reverse, payment_amount_reversible].min
        end

        # Returns Integer the amount of the GitHub match to reverse
        def match_amount
          [max_match_amount_to_reverse, match_amount_reversible].min
        end

        private

        def payment_amount_reversible
          transfer_payment_amount - reversals.sum do |reversal|
            reversal.metadata[:payment_amount_reversed].to_i
          end
        end

        def match_amount_reversible
          transfer_match_amount - reversals.sum do |reversal|
            reversal.metadata[:match_amount_reversed].to_i
          end
        end

        def reversals
          transfer.reversals.data
        end
      end

      # Public: Reverse a transfer to a Stripe Connect account
      #
      # Returns nothing
      def self.perform(**options)
        new(**options).perform
      end

      # Public: Initialize a new ReverseTransfer object
      #
      # sale_transaction_id       - The id of the original Zuora payment
      # stripe_refund_id          - The Stripe id of the refund causing this reversal
      # zuora_refund_id           - The Zuora id of the refund causing this reversal
      # payment_amount_to_reverse - The amount of the cardholder's payment to reverse
      #                             if doing a partial reversal (optional)
      # match_amount_to_reverse   - The amount of the GitHub match to reverse if
      #                             doing a partial reversal (optional)
      def initialize(sale_transaction_id:, stripe_refund_id:, zuora_refund_id:, payment_amount_to_reverse: nil, match_amount_to_reverse: nil)
        @sale_transaction_id = sale_transaction_id
        @stripe_refund_id = stripe_refund_id
        @zuora_refund_id = zuora_refund_id
        @total_payment_amount_to_reverse = payment_amount_to_reverse
        @total_match_amount_to_reverse = match_amount_to_reverse
      end

      # Public: Reverse transfers to maintainers Stripe Connect accounts
      #
      # Returns nothing
      def perform
        transfers_to_reverse.each do |stripe_transfer|
          amount_to_reverse = AmountToReverse.new(
            transfer: stripe_transfer,
            max_payment_amount_to_reverse: total_payment_amount_to_reverse,
            max_match_amount_to_reverse: total_match_amount_to_reverse,
          )

          # We subtract the amounts that are being reversed from the totals in the event
          # that there are multiple transfers, which generally shouldn't happen
          @total_payment_amount_to_reverse -= amount_to_reverse.payment_amount if total_payment_amount_to_reverse
          @total_match_amount_to_reverse -= amount_to_reverse.match_amount if total_match_amount_to_reverse

          # If there's nothing left to reverse, stop doing math
          return if amount_to_reverse.payment_amount.zero? && amount_to_reverse.match_amount.zero?

          reverse_transfer(
            stripe_transfer["id"],
            payment_amount_reversed: amount_to_reverse.payment_amount,
            match_amount_reversed: amount_to_reverse.match_amount,
          )
        end
      end

      private

      attr_reader :sale_transaction_id, :stripe_refund_id, :zuora_refund_id,
        :total_payment_amount_to_reverse, :total_match_amount_to_reverse

      def reverse_transfer(stripe_transfer_id, payment_amount_reversed:, match_amount_reversed:)
        ::Stripe::Transfer.create_reversal(
          stripe_transfer_id,
          amount: (payment_amount_reversed + match_amount_reversed),
          metadata: {
            payment_amount_reversed: payment_amount_reversed,
            match_amount_reversed: match_amount_reversed,
            stripe_refund_id: stripe_refund_id,
            zuora_refund_id: zuora_refund_id,
          },
        )

        # InvalidRequestError means either the transfer is fully reversed or doesn't
        # exist anymore - both scenarios we count as a success since that means
        # that the maintainer does not have the money in their Connect account.
        # Mission accomplished. We rescue and ignore the error.
      rescue ::Stripe::InvalidRequestError
      end

      def transfers_to_reverse
        if sale_transaction_id.nil?
          GitHub.dogstats.increment("stripe.transfer_reversal.failed")
          raise TransferNotFound.new("Reversal failed: An original payment ID is required ")
        end

        transfers = ::Stripe::Transfer.list(transfer_group: sale_transaction_id)["data"]
        if transfers.empty?
          GitHub.dogstats.increment("stripe.transfer_reversal.failed")
          raise TransferNotFound.new("Reversal failed: Transfer not found")
        end

        transfers
      end
    end
  end
end
