# frozen_string_literal: true

module Billing
  module Stripe
    class WebhookHandler
      InvalidSignature = Class.new(StandardError)

      # Public:  Generates a fingerprint for a stripe webhook payload and secret
      #
      # payload - Stripe webhook payload
      # secret - Stripe webhook secret
      #
      # Returns [String] generated fingerprint
      def self.fingerprint_for(payload, secret)
        OpenSSL::HMAC.hexdigest(
          OpenSSL::Digest.new("sha256"), secret, payload
        )
      end

      # Public: Instantiate a new handler by creating an incoming Stripe event
      # payload - Stripe webhook payload
      # signature - Stripe payload signature
      # secret - Stripe webhook secret
      #
      # Returns [WebhookHandler] instance
      def self.handle(payload:, signature:, kind:, secret: nil)
        secret ||= event_secret(kind)
        new(
          webhook_event: ::Stripe::Webhook.construct_event(payload, signature, secret),
          fingerprint: fingerprint_for(payload, secret),
        ).handle
      rescue ::Stripe::SignatureVerificationError => e
        GitHub.dogstats.increment("stripe.webhook.signature_verification_error")
        raise InvalidSignature.new(e.message)
      end

      def initialize(webhook_event:, fingerprint:)
        @webhook_event = webhook_event
        @fingerprint = fingerprint
      end

      # Internal: Determines the correct secret to use, depending on if this is
      # a webhook for a Connect account or our main Stripe account
      #
      # Returns string
      def self.event_secret(kind)
        if kind == :connect
          GitHub.stripe_connect_webhook_secret
        else
          GitHub.stripe_platform_webhook_secret
        end
      end

      # Public: Handles an incoming Sripe webhook event
      #
      # Returns [WebhookHandler] instance
      def handle
        GitHub.dogstats.increment(
          "stripe.webhook",
          tags: ["kind:#{webhook_event_kind}"],
        )

        webhook = StripeWebhook.create!(
          kind: webhook_event_kind,
          status: :pending,
          fingerprint: fingerprint,
          payload: webhook_event_payload,
        )
        StripeWebhookJob.perform_later(webhook)

        self
      end

      private

      attr_reader :webhook_event, :fingerprint

      def webhook_event_payload
        webhook_event.as_json
      end

      def webhook_event_kind
        webhook_event_type.gsub(/\./, "_").to_sym
      end

      def webhook_event_type
        webhook_event.type
      end
    end
  end
end
