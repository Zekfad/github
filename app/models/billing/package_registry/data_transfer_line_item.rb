# frozen_string_literal: true

module Billing
  class PackageRegistry::DataTransferLineItem < ApplicationRecord::Collab
    self.table_name = "package_registry_data_transfer_line_items"

    enum submission_state: {
      unsubmitted: "unsubmitted",
      submitted: "submitted",
      skipped: "skipped",
    }

    UNIT_OF_MEASURE = "Gigabytes"

    belongs_to :owner, class_name: "User", required: true
    belongs_to :actor, class_name: "User", optional: true
    belongs_to :registry_package, class_name: "Registry::Package", optional: true
    belongs_to :registry_package_version, class_name: "Registry::PackageVersion", optional: true
    belongs_to :billable_owner, polymorphic: true, optional: true
    belongs_to :synchronization_batch, class_name: "Billing::UsageSynchronizationBatch", optional: true

    validates :actor_id, presence: true
    validates :billable_owner_id, presence: true
    validates :billable_owner_type, presence: true
    validates :size_in_bytes, presence: true
    validates :download_id, presence: true
    validates :downloaded_at, presence: true
    validates :registry_package_id, presence: true
    validates :registry_package_version_id, presence: true
    validates :submission_state, presence: true

    delegate :account_number, :subscription_number, :charge_number,
      to: :zuora_usage_attributes, prefix: :zuora

    scope :directly_billed, -> { where(directly_billed: true) }
    scope :within_dates, -> (start_on, end_on) {
      where("downloaded_at >= ?", GitHub::Billing.date_in_timezone(start_on)).
        where("downloaded_at < ?", GitHub::Billing.date_in_timezone(end_on) + 1.day)
    }
    scope :after, -> (start_on) {
      where("downloaded_at >= ?", GitHub::Billing.date_in_timezone(start_on))
    }

    def self.product_name
      "packages"
    end

    def self.data_transfer_since_reset_by_package_registry_and_downloaded_at(owner_id)
      owner = User.find_by(id: owner_id)
      return {} unless owner

      where(owner_id: owner_id)
        .where("downloaded_at >= ?", owner.first_day_in_metered_cycle)
        .group(["registry_package_id", "CAST(downloaded_at AS DATE)"])
        .sum(:size_in_bytes)
    end

    # Public: The unit of measure for billing
    #
    # Returns String literal "Gigabytes"
    def unit_of_measure
      UNIT_OF_MEASURE
    end

    # Public: The billable quantity given in minutes rounded up to the nearest minute
    #
    # Returns BigDecimal
    def billable_quantity
      BigDecimal(size_in_bytes) / Numeric::GIGABYTE
    end

    # Public: The time when the usage was incurred. In this case, the time the package was downloaded
    #
    # Returns DateTime
    def start_time
      downloaded_at
    end

    # Public: The time when the usage incurred ended. In this case, the time the package was downloaded
    #
    # Returns DateTime
    def end_time
      downloaded_at
    end

    # Public: Is this usage for a private repository
    #
    # For Packages, we only record line item records for private repositories,
    # so this is always true
    #
    # Returns Boolean
    def private_visibility?
      true
    end

    # Public: When the usage occurred
    #
    # Returns DateTime
    def usage_at
      downloaded_at
    end

    # Public: Publish a MeteredLineItemUpdated Hydro message for this record
    #
    # This method uses Hydro::PublishRetrier since thousands of these messages
    # can be published in a short timeframe, possibly overflowing the Hydro
    # message buffer and dropping messages.
    #
    # Returns nothing
    def publish_metered_line_item_updated_message
      message = {
        metered_product: :PACKAGES,
        line_item_id: id,
      }
      Hydro::PublishRetrier.publish(message, schema: "github.billing.v0.MeteredLineItemUpdated")
    end

    private

    def zuora_usage_attributes
      @zuora_usage_attributes ||= Billing::ZuoraUsageAttributes.new(
        billable_owner: billable_owner,
        usage_type: :packages,
        usage_at: usage_at,
      )
    end
  end
end
