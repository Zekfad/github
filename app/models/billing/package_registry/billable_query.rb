# frozen_string_literal: true

# BillableQuery allows retrieving the billable gigabytes for billable owner after a specific date
class Billing::PackageRegistry::BillableQuery
  include Scientist

  def initialize(billable_owner:, after:, before: nil, owner: nil)
    @root_scope = Billing::PackageRegistry::DataTransferLineItem
      .where(billable_owner: billable_owner)
      .where("downloaded_at >= ?", after)

    @aggregate_root_scope = Billing::PackageRegistry::UsageAggregation
      .where(billable_owner: billable_owner)
      .where(metered_billing_cycle_starts_at: after.to_datetime.beginning_of_day)

    @billable_owner = billable_owner
    @owner = owner

    if owner.present?
      @root_scope = @root_scope.where(owner: owner)
      @aggregate_root_scope = @aggregate_root_scope.where(owner: owner)
    end

    if before.present?
      @root_scope = @root_scope.where("downloaded_at < ?", before)
    end
  end

  # Public: Returns the rounded number of paid gigabytes downloaded
  #
  # Returns Integer
  def paid_gigabytes_used
    rounded_billable_gigabytes_sum_for(paid_relation)
  end

  # Public: Returns the unrounded number of paid gigabytes downloaded
  #
  # Returns Integer
  def unrounded_paid_gigabytes_used
    billable_gigabytes_sum_for(paid_relation)
  end

  private

  attr_reader :root_scope, :aggregate_root_scope, :billable_owner, :owner

  def rounded_billable_gigabytes_sum_for(relation)
    billable_gigabytes_sum_for(relation).ceil
  end

  def billable_gigabytes_sum_for(relation)
    sum =
      if GitHub.flipper[:package_registry_read_from_aggregation].enabled?(billable_owner)
        relation.sum("aggregate_size_in_bytes")
      else
        sum = science "data-transfer-usage-query" do |experiment|
          experiment.context(
            owner: owner,
            billable_owner: billable_owner,
            metered_cycle: billable_owner.first_day_in_metered_cycle,
            experiment_record_count: aggregate_root_scope.count,
            experiment_sql: aggregate_root_scope.to_sql,
            datetime: billable_owner.first_day_in_metered_cycle.to_datetime.beginning_of_day,
          )
          experiment.run_if { GitHub.flipper[:package_registry_usage_aggregation].enabled?(billable_owner) }
          experiment.use { relation.sum("size_in_bytes") }
          experiment.try { aggregate_root_scope.sum("aggregate_size_in_bytes") }
          experiment.ignore do |control, candidate|
            min_threshold = control * 0.99 # min threshold is 1%
            candidate >= min_threshold
          end
        end
      end
    sum.to_f / Numeric::GIGABYTE
  end

  def paid_relation
    if GitHub.flipper[:package_registry_read_from_aggregation].enabled?(billable_owner)
      aggregate_root_scope
    else
      root_scope
    end
  end
end
