# frozen_string_literal: true

module Billing
  module PackageRegistry
    class DataTransferLineItemCreator
      def self.create(**kwargs)
        new.create(**kwargs)
      end

      def create(owner:, actor_id:, registry_package_id:, registry_package_version_id:, size_in_bytes:, download_id:, downloaded_at:, billable_owner_id:, billable_owner_type:, directly_billed:)
        line_item = ::Billing::PackageRegistry::DataTransferLineItem.create!(
          owner: owner,
          actor_id: actor_id,
          registry_package_id: registry_package_id,
          registry_package_version_id: registry_package_version_id,
          size_in_bytes: size_in_bytes,
          download_id: download_id,
          downloaded_at: downloaded_at,
          billable_owner_id: billable_owner_id,
          billable_owner_type: billable_owner_type,
          directly_billed: directly_billed,
          submission_state: :unsubmitted,
        )

        UsageAggregationJob.perform_later(line_item)

        line_item.publish_metered_line_item_updated_message
      rescue ActiveRecord::RecordNotUnique
        # Do nothing
      end
    end
  end
end
