# frozen_string_literal: true

module Billing
  class PackageRegistryPermission
    REASON_MESSAGES = {
      disabled: "Account must be enabled to use the GitHub Package Registry",
      plan_ineligible: "Legacy billing plans can't use GitHub Package Registry",
      trade_restricted_organization: TradeControls::Notices::Plaintext.org_restricted,
    }.freeze

    def initialize(owner)
      if owner.try(:delegate_billing_to_business?)
        @owner = owner.business
      else
        @owner = owner
      end
    end

    # Public: Whether package registry billing is enabled
    #
    # Returns Boolean
    def allowed?(public: false)
      return true if owner_is_github? # Skip billing checks for GitHub org
      return true if public

      status[:allowed]
    end

    # Public: Status of GPR for the owner
    #
    # Returns Hash
    def status
      @_status ||= build_status
    end

    # Public: Should download be allowed based on usage
    #
    # Returns Boolean
    def download_allowed?(bytes:, public:)
      return true unless GitHub.billing_enabled? # Skip billing checks if billing is disabled.
      return true if owner_is_github? # Skip billing checks for GitHub org
      return false if !public && owner_plan_is_package_registry_ineligible?
      return false if metered_billing_permission.trade_controls_apply?(public: public)
      return true if public
      return true if owner.skip_metered_billing_permission_check_for?(product: :packages)

      gigabytes = bytes.to_f / 1.gigabyte
      return true if has_included_private_bandwidth_for?(gigabytes)
      return false if metered_billing_permission.paid_overages_restricted_by_owner_payment_issue?

      metered_billing_permission.usage_allowed?(additional_package_registry_gigabytes: gigabytes)
    end

    # Public: Should storage be allowed based on usage
    #
    # Returns Boolean
    def storage_allowed?(bytes:, public:)
      return true unless GitHub.billing_enabled? # Skip billing checks if billing is disabled.
      return true if owner_is_github? # Skip billing checks for GitHub org
      return false if !public && owner_plan_is_package_registry_ineligible?
      return false if metered_billing_permission.trade_controls_apply?(public: public)
      return true if public
      return true if owner.skip_metered_billing_permission_check_for?(product: :storage)

      megabytes = bytes / 1.megabyte
      return true if has_included_private_storage_for?(megabytes)
      return false if metered_billing_permission.paid_overages_restricted_by_owner_payment_issue?

      metered_billing_permission.usage_allowed?(additional_shared_storage_megabytes: megabytes)
    end

    private

    attr_reader :owner

    def build_status
      return allowed_status unless GitHub.billing_enabled? # Skip billing checks if billing is disabled.

      if metered_billing_permission.fully_trade_restricted_organization_owner?
        not_allowed_status(:trade_restricted_organization)
      elsif !owner.plan.package_registry_eligible?
        not_allowed_status(:plan_ineligible)
      elsif owner.disabled?
        not_allowed_status(:disabled)
      else
        allowed_status
      end
    end

    def allowed_status
      { allowed: true, error: {} }
    end

    def not_allowed_status(reason)
      {
        allowed: false,
        error: {
          reason: reason.to_s.upcase,
          message: REASON_MESSAGES[reason],
        },
      }
    end

    def owner_plan_is_package_registry_ineligible?
      !owner.plan.package_registry_eligible?
    end

    def has_included_private_bandwidth_for?(gigabytes)
      package_registry_usage.billable_gigabytes(additional_gigabytes: gigabytes).zero?
    end

    def has_included_private_storage_for?(megabytes)
      shared_storage_usage.estimated_monthly_paid_megabytes(additional_megabytes: megabytes).zero?
    end

    def shared_storage_usage
      @_shared_storage_usage ||= Billing::SharedStorageUsage.new(owner)
    end

    def package_registry_usage
      @_package_registry_usage ||= PackageRegistryUsage.new(owner)
    end

    def metered_billing_permission
      @_metered_billing_permission ||= Billing::MeteredBillingPermission.new(owner)
    end

    def owner_is_github?
      owner.is_a?(Organization) && owner.login == "github"
    end
  end
end
