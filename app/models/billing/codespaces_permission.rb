# frozen_string_literal: true

module Billing
  class CodespacesPermission
    DOGSTATS_ROOT_VALUE = "billing.codespaces_permission_check"

    def initialize(owner)
      @owner = owner
    end

    # Public: Total codespaces billing usage in whole cents
    #
    # Returns Integer
    def total_usage(additional_minutes: 0,
                    additional_gigabytes: 0)

      GitHub.dogstats.time(DOGSTATS_ROOT_VALUE) do
        total = compute_usage_cost_in_cents(additional_minutes) +
                storage_usage_cost_in_cents(additional_gigabytes)

        total.floor
      end
    end

    # Public: Owners set codespaces usage limit
    #
    # Returns BigDecimal
    def usage_limit
      @_usage_limit ||=
        begin
          config = owner.budget_for(product: :codespaces)
          config.enforce_spending_limit ? config.effective_spending_limit_in_subunits : BigDecimal("Infinity")
        end
    end

    private

    attr_reader :owner

    # Internal: The calculated cost for used computed minutes, in cents
    #
    # Returns Numeric
    def compute_usage_cost_in_cents(additional_minutes)
      GitHub.dogstats.time("#{DOGSTATS_ROOT_VALUE}.compute_usage_cost_calculation") do
        Billing::Codespaces::ComputeUsage.new(owner)
          .paid_minutes_used(additional_minutes: additional_minutes) / 60.0 * Billing::Codespaces::ZuoraProduct::COMPUTE_UNIT_COST_IN_CENTS
      end
    end

    # Internal: The calculated cost for used storage gigabytes, in cents
    #
    # Returns Numeric
    def storage_usage_cost_in_cents(additional_gigabyte_hours)
      GitHub.dogstats.time("#{DOGSTATS_ROOT_VALUE}.storage_usage_cost_calculation") do
        Billing::Codespaces::StorageUsage.new(owner)
          .paid_gigabyte_hours_used(additional_gigabyte_hours: additional_gigabyte_hours) * Billing::Codespaces::ZuoraProduct::STORAGE_UNIT_COST_IN_CENTS
      end
    end
  end
end
