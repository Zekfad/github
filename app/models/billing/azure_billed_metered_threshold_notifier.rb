# frozen_string_literal: true

module Billing
  class AzureBilledMeteredThresholdNotifier
    THRESHOLDS = [75, 90, 100].freeze

    def initialize(owner, product: nil)
      @owner = owner
      @product = product
    end

    def notify_if_applicable
      if owner.enterprise_agreements.where.not(azure_subscription_id: nil).exists?
        notify_paid_usage
      else
        notify_free_usage
      end
    end

    private

    attr_reader :owner, :product

    def notify_paid_usage
      return if effective_spending_limit_in_subunits.zero? || relevant_threshold.nil? || already_sent?

      if relevant_threshold < 100
        Billing::AzureBilledMeteredThresholdMailer.approaching_limit(
          recipients: email_recipients,
          threshold: relevant_threshold,
          billable: owner,
          limit_in_subunits: effective_spending_limit_in_subunits,
        ).deliver_later
      else
        Billing::AzureBilledMeteredThresholdMailer.over_limit(recipients: email_recipients, billable: owner).deliver_later
      end

      record_sending
    end

    def notify_free_usage
      Billing::UsageNotifications.new(owner, product: product).notify_if_applicable(ignore_paid_thresholds: true)
    end

    def already_sent?
      GitHub.kv.exists(unique_key).value { false }
    end

    def record_sending
      ActiveRecord::Base.connected_to(role: :writing) do
        GitHub.kv.set(unique_key, Time.now.iso8601, expires: 60.days.from_now)
      end

      # The two hardcoded values are to make the output consistent with what is found
      # in our other threshold notifiers.
      #
      # In this case we only ever use this method for paid usage limits, so the product is
      # "spending-limit", and it would be a paid threshold. The `#notify_free_usage` method
      # does not call this, instead using the instrumentation found in Billing::UsageNotifications
      payload = {
        business: owner,
        product: "spending-limit",
        threshold_level: relevant_threshold,
        first_day_in_metered_cycle: owner.first_day_in_metered_cycle,
        paid_threshold: true
      }

      GitHub.instrument("billing.metered_usage_email_sent", payload)
    end

    def unique_key
      @unique_key ||= "azure-billed-metered-billing-threshold-notification-#{owner.id}-#{owner.first_day_in_metered_cycle}-#{relevant_threshold}"
    end

    def relevant_threshold
      return @relevant_threshold if defined?(@relevant_threshold)

      @relevant_threshold = THRESHOLDS.filter { |threshold| threshold <= current_percentage }.max
    end

    def current_percentage
      @current_percentage ||= total_usage_amount_in_cents / effective_spending_limit_in_subunits.to_f * 100
    end

    def total_usage_amount_in_cents
      @total_usage_amount_in_cents ||= ::Billing::MeteredBillingPermission.new(owner).total_usage
    end

    def effective_spending_limit_in_subunits
      @effective_spending_limit_in_subunits ||= owner.budget_for(product: product).effective_spending_limit_in_subunits
    end

    def email_recipients
      owner.owners + owner.billing_managers
    end
  end
end
