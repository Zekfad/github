# frozen_string_literal: true

module Billing::MeteredBillable
  extend ActiveSupport::Concern

  included do
    has_many :budgets,
      class_name: "Billing::Budget",
      dependent: :destroy,
      as: :owner
  end

  def metered_billing_overage_allowed?(product: nil)
    if try(:has_any_trade_restrictions?) || (self.is_a?(Business) && has_enterprise_agreement_without_azure_id?)
      return false
    end

    relation = budgets.overage_allowed
    if product
      unless Billing::Budget.valid_product?(product)
        raise ArgumentError, "invalid product argument #{product}"
      end

      relation = relation.where(product: Billing::Budget.product_key_for(product: product))
    end

    relation.exists?
  end

  def budget_for(product:)
    unless Billing::Budget.valid_product?(product)
      raise ArgumentError, "invalid product argument #{product}"
    end
    product_key = Billing::Budget.product_key_for(product: product)

    if try(:has_any_trade_restrictions?) || (self.is_a?(Business) && has_enterprise_agreement_without_azure_id?)
      Billing::Budget.new(owner: self, enforce_spending_limit: true, spending_limit_in_subunits: 0, product: product_key).tap(&:readonly!)
    else
      budgets.find_by(product: product_key) || budgets.build(product: product_key)
    end
  end

  # Internal: This method will override the quota usage checks for this user until their next metered cycle.
  #
  # Only use this method if you exhausted all other possible ways to unblock the user.
  # The method was created to unblock users who are hitting the timeout issue due to too many line items for downloads
  # This will not only stop checking for quota usages during permission checks but also stop recording usage for the user
  #
  # product - One of the three metered billing products `actions`, `packages`, `storage`
  #
  # Returns nothing
  def skip_metered_billing_permission_check_for(product:, expires: first_day_in_next_metered_cycle)
    GitHub.kv.set(skip_metered_billing_permissions_key(product), "true", expires: expires)
  end

  def skip_metered_billing_permission_check_for?(product:)
    GitHub.kv.exists(skip_metered_billing_permissions_key(product)).value!
  end

  private

  def skip_metered_billing_permissions_key(product)
    "skip-metered-check-#{self.class.name}-#{id}-#{product}"
  end

  def has_enterprise_agreement_without_azure_id?
    return false unless self.is_a?(Business)

    agreement = self.enterprise_agreements.active.first
    agreement.present? && agreement.azure_subscription_id.nil?
  end
end
