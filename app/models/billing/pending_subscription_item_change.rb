# frozen_string_literal: true

module Billing
  class PendingSubscriptionItemChange < ApplicationRecord::Domain::Integrations
    include Instrumentation::Model

    self.ignored_columns = %w(marketplace_listing_plan_id)

    areas_of_responsibility :gitcoin

    after_create_commit :instrument_creation

    enum subscribable_type: {
      Marketplace::ListingPlan.name => 0,
      SponsorsTier.name => 1,
    }

    belongs_to :pending_plan_change, class_name: "Billing::PendingPlanChange"
    belongs_to :subscribable, polymorphic: true

    def listing
      async_listing.sync
    end

    def async_listing
      async_subscribable.then do |subscribable|
        subscribable.async_listing
      end
    end

    def subscribable_is_marketplace_listing_plan?
      subscribable_type == Marketplace::ListingPlan.name
    end

    def listable_is_sponsorable?
      listing.listable_is_sponsorable?
    end

    scope :joins_marketplace_listing_plans, -> {
      join_sql = self.github_sql.new(<<-SQL, subscribable_type: subscribable_types[Marketplace::ListingPlan.name])
        INNER JOIN marketplace_listing_plans
        ON (marketplace_listing_plans.id = pending_subscription_item_changes.subscribable_id
        AND pending_subscription_item_changes.subscribable_type = :subscribable_type)
      SQL

      joins(join_sql.query)
    }

    scope :joins_marketplace_listings, -> {
      joins_marketplace_listing_plans.
        joins("JOIN marketplace_listings on marketplace_listings.id = marketplace_listing_plans.marketplace_listing_id")
    }

    scope :for_marketplace_listing_plan, -> (plan) do
      joins_marketplace_listing_plans.where(marketplace_listing_plans: { id: plan.id })
    end

    scope :for_subscribable_listing, -> (listing) do
      if listing.is_a?(Marketplace::Listing)
        pending_changes = for_marketplace_listing(listing.id)

        sponsors_listing = SponsorsListing.find_by(slug: listing.slug)
        pending_changes += for_sponsors_listing(sponsors_listing) if sponsors_listing
      elsif listing.is_a?(SponsorsListing)
        pending_changes = for_sponsors_listing(listing)

        marketplace_listing = listing.find_corresponding_marketplace_listing
        pending_changes += for_marketplace_listing(marketplace_listing) if marketplace_listing
      end

      pending_changes
    end

    scope :for_marketplace_listing, -> (listing_id) do
      joins_marketplace_listing_plans.where(marketplace_listing_plans: { marketplace_listing_id: listing_id })
    end

    scope :for_sponsors_listing, -> (listing) do
      sponsors_tiers = listing.sponsors_tiers.pluck(:id)
      where(subscribable_type: SponsorsTier.name, subscribable_id: sponsors_tiers)
    end

    scope :for_sponsors_tier, -> (tier) do
      where(subscribable_type: subscribable_types[SponsorsTier.name], subscribable_id: tier.id)
    end

    scope :for_sponsors_tiers, -> { where(subscribable_type: SponsorsTier.name) }
    scope :for_sponsors_listing, ->(listing_id) {
      for_sponsors_tiers.
        where(subscribable_id: SponsorsTier.where(sponsors_listing_id: listing_id).pluck(:id))
    }

    scope :trial_ending_in_four_days, -> {
      joins(
        "INNER JOIN pending_plan_changes ON pending_plan_changes.id = pending_subscription_item_changes.pending_plan_change_id").
        where("pending_plan_changes.active_on = ?", (GitHub::Billing.today + 4.days).to_s(:db)).
        where("pending_subscription_item_changes.free_trial = true")
    }
    scope :non_free_trial, -> { where(free_trial: false) }
    scope :free_trial, -> { where(free_trial: true) }
    scope :cancellation, -> { where(quantity: 0) }

    delegate :active_on, to: :pending_plan_change

    def run
      result = Billing::SubscriptionItemUpdater.perform \
        account: user,
        force: true,
        subscribable: subscribable,
        quantity: quantity,
        sender: actor,
        end_free_trial: free_trial

      instrument_run(result: result)
    end

    def price(duration = nil)
      Billing::Money.new((
        listing_plan_base_price *
        quantity *
        service_percent_remaining
      ))
    end

    def listing_plan_base_price
      subscribable.base_price(duration: plan_duration)
    end

    def service_percent_remaining
      days_billed = (effective_next_billing_date - active_on).to_f
      percent = days_billed / user.subscription.duration_in_days
      percent == 0 ? 1 : percent
    end

    def effective_next_billing_date
      if user.next_billing_date >= active_on
        user.next_billing_date
      elsif user.yearly_plan?
        user.next_billing_date + 1.year
      else
        user.next_billing_date + 1.month
      end
    end

    def cancellation?
      quantity == 0
    end

    delegate :actor, :user, to: :pending_plan_change
    delegate :pending_cycle, :plan_subscription, to: :user
    delegate :plan_duration, to: :pending_cycle

    def subscription_item
      plan_subscription.try(:subscription_item_for_listing, listing)
    end

    def platform_type_name
      "PendingMarketplaceChange"
    end

    private

    def event_prefix
      "pending_subscription_change"
    end

    def instrument_creation
      payload = instrument_payload
      payload[:actor] = actor if actor

      if subscription_item
        payload[:marketplace_listing_plan_was] = subscription_item.subscribable.name
        payload[:quantity_was] = subscription_item.quantity
      end

      instrument :create, payload
    end

    def instrument_run(result:)
      payload = instrument_payload

      payload[:success] = result[:success]
      payload[:errors] = result[:errors] unless result[:success]

      instrument :run, payload
    end

    def instrument_payload
      {
        active_on: active_on,
        user: user,
        marketplace_listing_id: listing.id,
        marketplace_listing: listing.try(:name) || listing.try(:slug),
        marketplace_listing_plan: subscribable.name,
        quantity: quantity.to_i,
        free_trial: !!free_trial,
      }
    end
  end
end
