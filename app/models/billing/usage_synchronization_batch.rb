# frozen_string_literal: true

module Billing
  class UsageSynchronizationBatch < ApplicationRecord::Collab
    enum status: {
      building: 0,
      uploading: 1,
      uploaded: 2,
      ready_to_submit: 3,
      submitted: 4,
      completed: 5,
      failed: 6,
      ignored: 7,
    }

    validates :product, presence: true
    validates :status, presence: true
    validates :upload_filename, presence: true

    has_many :actions_usage_line_items,
      dependent: :nullify,
      foreign_key: :synchronization_batch_id

    def mark_uploaded
      update(status: :uploaded)
    end

    def can_submit?
      ready_to_submit? || uploaded?
    end

    def usage_id
      return nil unless zuora_status_url
      # we want the id, which comes before the "/status" in something like "/v1/usage/{id_hex}/status"
      zuora_status_url.split("/")[-2]
    end
  end
end
