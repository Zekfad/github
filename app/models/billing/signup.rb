# frozen_string_literal: true

class Billing::Signup
  include ActiveModel::Validations

  attr_accessor :user, :actor
  attr_reader :plan

  validates :plan, presence: { message: "was not found" }

  validate :user_is_persisted_and_valid
  validate :ensure_no_billing_record_on_user
  with_options if: -> { plan.present? } do
    validate :plan_is_paid
    validate :plan_is_eligible_for_user_type
    validate :user_can_change_to_plan
  end

  def initialize(user:, plan_name:, actor:)
    @user = user
    @plan = GitHub::Plan.find(plan_name)
    @actor = actor
  end

  def customer_already_exists?
    ensure_no_billing_record_on_user

    errors.of_kind?(:user, :already_signed_up)
  end

  private

  def user_is_persisted_and_valid
    if user.new_record? || user.invalid?
      errors.add(:user)
    end
  end

  def ensure_no_billing_record_on_user
    if user.has_billing_record?
      errors.add(:user, :already_signed_up, message: "has already signed up")
    end
  end

  def plan_is_paid
    if plan.free?
      errors.add(:plan, :not_paid, message: "must not be free for sign up")
    end
  end

  def plan_is_eligible_for_user_type
    if user.user? && plan.orgs?
      errors.add(:plan, :invalid_plan_type_for_user, message: "#{plan.display_name} cannot be used for users")
    elsif user.organization? && !plan.orgs?
      errors.add(:plan, :invalid_plan_type_for_user, message: "#{plan.display_name} cannot be used for organizations")
    end
  end

  def user_can_change_to_plan
    if !plan.per_seat? && !user.can_change_plan_to?(plan, actor: actor)
      errors.add(:plan, :invalid_plan_for_user, message: "#{plan} cannot be used for user sign up")
    end
  end
end
