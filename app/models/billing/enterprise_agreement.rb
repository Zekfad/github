# frozen_string_literal: true

module Billing
  class EnterpriseAgreement < ApplicationRecord::Collab
    include GitHub::Relay::GlobalIdentification
    include GitHub::Validations
    extend GitHub::Encoding

    areas_of_responsibility :gitcoin

    belongs_to :business

    CATEGORIES = [["Visual Studio Bundle", "visual_studio_bundle"],
                  ["GitHub Enterprise Unified", "github_enterprise_unified"]]
    STATUSES = [["Active", "active"], ["Ended", "ended"]]

    enum category: { visual_studio_bundle: 0, github_enterprise_unified: 1 }
    enum status: { active: 0, ended: 1 }

    validates :agreement_id, presence: true, uniqueness: { case_sensitive: false }
    validates :business, presence: true
    validates :category, presence: true, inclusion: { in: categories.keys }
    validates :status, presence: true, inclusion: { in: statuses.keys }
    validates :seats, numericality: { equal_to: 0 }, unless: :visual_studio_bundle?
    validates :azure_subscription_id, format: { with: /\A[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}\Z/, allow_blank: true }

    force_utf8_encoding :agreement_id

    after_commit :sync_bundled_license_agreement_business, on: [:create, :update]

    def azure_subscription_id=(value)
      super(value.presence)
    end

    private

    # Queue SyncBundledLicenseAssignmentJob
    # Updates related Billing:BundledLicenseAssignment business
    # to match the Billing::EnterpriseAgreement business
    def sync_bundled_license_agreement_business
      SyncBundledLicenseAssignmentJob.perform_later(self)
    end
  end
end
