# frozen_string_literal: true

module Billing
  module Codespaces
    class StorageUsage
      def initialize(billable_owner, owner: nil)
        @billable_owner = billable_owner
        @plan = billable_owner.plan
        @owner = owner
      end

      # Public: What percentage of the entitlement has been consumed?
      #
      # Returns Integer
      def included_usage_percentage
        return 100 if plan_included_gigabytes.zero?
        percent = (gigabyte_hours_used / Float(plan_included_gigabytes)).round(3)
        [(percent * 100).to_i, 100].min
      end

      # Public: The entitlement for codespaces storage
      #
      # Returns Integer
      def plan_included_gigabytes
        plan.codespaces_included_storage_gigabytes
      end
      alias_method :included_gigabyte_hours, :plan_included_gigabytes

      # Public: The current storage usage
      #
      # Returns Integer
      def gigabyte_hours_used
        billable_query.gigabyte_hours
      end

      # Public: The amount of gigabyte_hours that are above included allotments
      #
      # Returns Integer
      def paid_gigabyte_hours_used(additional_gigabyte_hours: 0)
        [0, gigabyte_hours_used + additional_gigabyte_hours - included_gigabyte_hours].max
      end

      private

      attr_reader :billable_owner, :plan, :owner

      def billable_query
        Billing::Codespaces::StorageUsageBillableQuery.new(billable_owner: billable_owner, after: billable_owner.first_day_in_metered_cycle)
      end
    end
  end
end
