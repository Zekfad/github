# frozen_string_literal: true

module Billing
  module Codespaces
    class ComputeUsageLineItemCreator
      def self.create(**kwargs)
        new.create(**kwargs)
      end

      def create(owner:, actor_id:, repository_id:, unique_billing_identifier:, duration_in_seconds:, start_time:, end_time:, billable_owner_id:, billable_owner_type:, directly_billed:, computed_usage:, sku: "STANDARD_LINUX")
        line_item = ::Billing::Codespaces::ComputeUsageLineItem.new(
          owner: owner,
          actor_id: actor_id,
          repository_id: repository_id,
          unique_billing_identifier: unique_billing_identifier,
          start_time: start_time,
          end_time: end_time,
          duration_in_seconds: duration_in_seconds,
          sku: sku,
          duration_multiplier: ::Billing::Codespaces::ComputeUsageLineItem.duration_multiplier_for(sku: sku),
          billable_owner_type: billable_owner_type,
          billable_owner_id: billable_owner_id,
          directly_billed: directly_billed,
          computed_usage: computed_usage,
          submission_state: :unsubmitted,
        )

        if GitHub.flipper[:publish_codespaces_compute_line_item_updated].enabled?(line_item.billable_owner)
          line_item.save!
          line_item.publish_metered_line_item_updated_message
        else
          line_item.submission_state = :skipped
          line_item.submission_state_reason = "feature_flag_disabled"
          line_item.save!
        end
      rescue ActiveRecord::RecordNotUnique
        # Do nothing
      end
    end
  end
end
