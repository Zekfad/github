# frozen_string_literal: true

module Billing
  module Codespaces
    class StorageUsageLineItemCreator
      def self.create(**kwargs)
        new.create(**kwargs)
      end

      def create(owner:, actor_id:, repository_id:, unique_billing_identifier:, duration_in_seconds:, start_time:, end_time:, billable_owner_id:, billable_owner_type:, directly_billed:, size_in_bytes:, computed_usage:)
        line_item = ::Billing::Codespaces::StorageUsageLineItem.create!(
          owner: owner,
          actor_id: actor_id,
          repository_id: repository_id,
          unique_billing_identifier: unique_billing_identifier,
          start_time: start_time,
          end_time: end_time,
          duration_in_seconds: duration_in_seconds,
          billable_owner_type: billable_owner_type,
          billable_owner_id: billable_owner_id,
          directly_billed: directly_billed,
          size_in_bytes: size_in_bytes,
          submission_state: :unsubmitted,
          computed_usage: computed_usage
        )

        if GitHub.flipper[:publish_codespaces_storage_line_item_updated].enabled?(line_item.billable_owner)
          line_item.save!
          line_item.publish_metered_line_item_updated_message
        else
          line_item.submission_state = :skipped
          line_item.submission_state_reason = "feature_flag_disabled"
          line_item.save!
        end
      rescue ActiveRecord::RecordNotUnique
        # Do nothing
      end
    end
  end
end
