# frozen_string_literal: true

module Billing
  module Codespaces
    class ComputeUsage
      def initialize(billable_owner, owner: nil)
        @billable_owner = billable_owner
        @plan = billable_owner.plan
        @owner = owner
      end

      def included_minutes
        plan.codespaces_included_compute_minutes
      end

      def included_minutes_used
        [total_minutes_used, included_minutes].min
      end

      # Public: The amount of minutes that are above the included minutes
      #
      # additional_minutes: A Numeric to add in to used minutes
      # for the calculation
      #
      # Returns Numeric
      def paid_minutes_used(additional_minutes: 0)
        [0, total_minutes_used + additional_minutes - included_minutes].max
      end

      # Public: Returns the rounded billable minutes executed
      # for the user, both broken down by SKU, and as a total.
      #
      # Example response:
      # { "SKU1"=>20, "SKU2"=>1, "TOTAL"=>21 }
      #
      # Returns Hash
      def minutes_used
        @_minutes_used ||=
          billable_query.hours_of_usage.each_with_object({}) do |(k, v), result|
            result[k] = v * 60
          end
      end

      # Public: What percentage of the included minutes have been consumed?
      #
      # Returns Numeric
      def included_minutes_usage_percentage
        return 100 if included_minutes.zero?
        percent = (included_minutes_used / Float(included_minutes)).round(2)
        [(percent * 100).to_i, 100].min
      end

      # Public: The sum of all billable minutes used across SKUs
      #
      # Returns Integer
      def total_minutes_used
        minutes_used["TOTAL"]
      end

      private

      attr_reader :billable_owner, :plan, :owner

      def billable_query
        @_query ||= Billing::Codespaces::ComputeUsageBillableQuery
          .new(billable_owner: billable_owner, owner: owner, after: billable_owner.first_day_in_metered_cycle)
      end
    end
  end
end
