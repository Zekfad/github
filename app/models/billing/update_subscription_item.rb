# frozen_string_literal: true

module Billing

  # Public: A Plain Old Ruby Object (PORO) used for updating a subscription item
  # representing a purchase.
  class UpdateSubscriptionItem
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    # inputs - Hash containing attributes to create a subscription item
    # inputs[:subscribable] - The listing plan or sponsors tier that was purchased.
    # inputs[:quantity] - How many units of this listing were purchased.
    # inputs[:account] - The account that purchased this.
    # inputs[:grant_oap] (optional) - Whether to grant OAP access to the application.
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(**inputs)
      new(**inputs).call
    end

    def initialize(subscribable:, quantity:, account:, grant_oap: nil, viewer:)
      @subscribable = subscribable
      @quantity     = quantity.to_i
      @account      = account
      @grant_oap    = grant_oap
      @viewer       = viewer
    end

    def call
      if account.blank?
        raise UnprocessableError.new("Account not found")
      end

      unless account.subscription_items_adminable_by?(viewer)
        raise ForbiddenError.new("#{viewer} does not have permission to manage this account (#{account})")
      end

      if account.invoiced? && subscribable.paid?
        error_message = "Invoiced customers cannot purchase paid Marketplace plans at this time. Please contact support if you have any questions."
        raise UnprocessableError.new(error_message)
      elsif subscribable.paid? && !account.has_valid_payment_method?
        raise UnprocessableError.new("Please add a payment method before checking out.")
      end

      unless quantity.to_i > 0
        raise UnprocessableError.new("Quantity must be greater than 0.")
      end

      if account.spammy?
        raise UnprocessableError.new("Your account is flagged and unable to make purchases. Please contact support to have your account reviewed.")
      end

      if grant_oap
        listing = subscribable.listing
        if listing.listable_is_oauth_application?
          account.approve_oauth_application(listing.listable, approver: viewer)
        end
      end

      result = ::Billing::SubscriptionItemUpdater.perform \
        account: account,
        subscribable: subscribable,
        quantity: quantity,
        sender: viewer

      if result[:success]
        { subscription_item: result[:subscription_item] }
      else
        raise UnprocessableError.new("Could not purchase this item: #{result[:errors]}")
      end
    end

    private

    attr_reader :grant_oap, :account, :quantity, :subscribable, :viewer
  end
end
