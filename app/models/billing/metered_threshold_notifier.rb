# frozen_string_literal: true

module Billing
  class MeteredThresholdNotifier
    def initialize(owner:, product:)
      @owner = Billing::MeteredBillingBillableOwnerDesignator.new(owner).billable_owner
      @product = product
    end

    def notify_if_applicable
      if owner.try(:enterprise_agreement?)
        Billing::AzureBilledMeteredThresholdNotifier.new(owner, product: product).notify_if_applicable
      elsif owner.invoiced?
        Billing::InvoicedMeteredThresholdNotifier.new(owner, product: product).notify_if_applicable
      else
        Billing::UsageNotifications.new(owner, product: product).notify_if_applicable
      end
    end

    private

    attr_reader :owner, :product
  end
end
