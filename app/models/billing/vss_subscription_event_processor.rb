# frozen_string_literal: true

module Billing
  class VssSubscriptionEventProcessor
    def self.service_bus_config
      {
        namespace: GitHub.vss_subscription_events_namespace,
        queue_name: GitHub.vss_subscription_events_queue_name,
        sas_key_name: GitHub.vss_subscription_events_sas_key_name,
        sas_key: GitHub.vss_subscription_events_sas_key
      }
    end

    def process(message)
      event = VssSubscriptionEvent.create!(payload: message)
      VssSubscriptionEventProcessingJob.perform_later(event)
    end
  end
end
