# frozen_string_literal: true

module Billing
  module Stafftools
    class MeteredUsageAggregation

      class SharedStorageUsage
        attr_reader :repo_name, :repo_visibility, :total_usage_in_bytes, :aggregated_actions_usage,
          :unaggregated_actions_usage, :aggregated_gpr_usage, :unaggregated_gpr_usage

        def initialize(repo_name, repo_visibility, total_usage_in_bytes, aggregated_actions_usage:, unaggregated_actions_usage:, aggregated_gpr_usage:, unaggregated_gpr_usage:)
          @repo_name = repo_name
          @repo_visibility = repo_visibility
          @total_usage_in_bytes = total_usage_in_bytes
          @aggregated_actions_usage = aggregated_actions_usage
          @unaggregated_actions_usage = unaggregated_actions_usage
          @aggregated_gpr_usage = aggregated_gpr_usage
          @unaggregated_gpr_usage = unaggregated_gpr_usage
        end
      end

      def initialize(owner_id:)
        @owner_id = owner_id
      end

      # Public: Data around upcoming artifact expirations
      #
      # # All dates are in the future
      # # RepoOne id is 1
      # [
      #   {
      #     name: "RepoOne",
      #     size_by_date: {
      #       [1, "2019-12-24"] => 123,
      #       [1, "2019-12-25"] => 124,
      #     },
      #     total_size: 147,
      #   },
      # ]
      #
      # Returns: An Array of Hashes
      def upcoming_actions_artifact_storage_expirations(artifact_event: ::Billing::SharedStorage::ArtifactEvent)
        upcoming_expirations = artifact_event.upcoming_actions_expirations_by_repo_and_effective_at(owner_id)
        actions_artifacts_expiration_fields(upcoming_expirations)
      end

      # Public: Data about package download bandwidth
      #
      # # All dates are in the past
      # # RubyGem1 id is 1
      # [
      #   {
      #     name: "RubyGem1",
      #     bandwidth_by_date: {
      #       [1, "2019-12-24"] => 123,
      #       [1, "2019-12-25"] => 124,
      #     },
      #     total_bandwidth: 147,
      #   },
      # ]
      #
      # Returns: An Array of Hashes
      def packages_bandwidth_since_cycle_reset(data_transfer_line_item: ::Billing::PackageRegistry::DataTransferLineItem)
        recent_usage = data_transfer_line_item.data_transfer_since_reset_by_package_registry_and_downloaded_at(owner_id)
        registry_package_fields(recent_usage)
      end

      # Public: Data about shared storage usage, with repository name and aggregate size in bytes
      #
      # Returns: An Array of SharedStorageUsage instances
      def shared_storage_usage(billable_query: ::Billing::SharedStorage::BillableQuery)
        owner = User.find_by(id: owner_id)
        return {} unless owner

        current_storage = billable_query
          .new(billable_owner: owner, after: owner.first_day_in_metered_cycle, owner: owner)
          .latest_aggregate_size_in_bytes_by_repository_id

        artifact_storage_fields(current_storage, owner)
      end

      # Public: Data about actions usage minutes
      #
      # # All dates are in the past
      # # RepoName id is 1
      # [
      #   {
      #     name: "RepoName",
      #     minutes_per_runtime: {
      #       [1, "UBUNTU"] => 123,
      #       [1, "MACOS"] => 124,
      #     },
      #     total_minutes: 147,
      #   },
      # ]
      #
      # Returns: An Array of Hashes
      def actions_usage_since_cycle_reset(actions_line_item: ::Billing::ActionsUsageLineItem)
        recent_usage = actions_line_item.usage_line_item_by_repository_and_runtime_environment(owner_id)
        repository_actions_fields(recent_usage)
      end

      private

      attr_reader :owner_id

      def repository_actions_fields(recent_usage)
        repos =
          Repository.where(id: recent_usage.keys.map(&:first)).select(:id, :name) +
          Archived::Repository.where(id: recent_usage.keys.map(&:first)).select(:id, :name)

        repos.map do |repo|
          minutes_per_runtime = recent_usage.select do |(repo_id, _runtime), minutes|
            repo.id == repo_id
          end

          {
            name: repo.name,
            minutes_per_runtime: minutes_per_runtime,
            total_minutes: minutes_per_runtime.values.sum,
          }
        end.sort_by { |elem| -1 * elem[:total_minutes] }
      end

      def registry_package_fields(recent_usage)
        package_ids = recent_usage.keys.map(&:first)
        packages = Registry::Package.where(id: package_ids).select(:id, :name)

        package_fields = packages.map do |package|
          bandwidth_per_package = recent_usage.select do |(package_id, _downloaded_at), size_in_bytes|
            package.id == package_id
          end

          {
            name: package.name,
            bandwidth_by_date: bandwidth_per_package,
            total_bandwidth: bandwidth_per_package.values.sum,
          }
        end

        missing_package_ids = package_ids - packages.pluck(:id)
        return package_fields if missing_package_ids.empty?

        deleted_package_recent_usage = recent_usage.
          select { |(package_id, _downloaded_at), _| missing_package_ids.include?(package_id) }

        package_fields << {
          name: "DELETED PACKAGES",
          bandwidth_by_date: deleted_package_recent_usage,
          total_bandwidth: deleted_package_recent_usage.values.sum,
        }

        package_fields.sort_by { |elem| -1 * elem[:total_bandwidth] }
      end

      def actions_artifacts_expiration_fields(upcoming_expirations)
        repo_ids = upcoming_expirations.keys.map(&:first)

        repos =
          Repository.where(id: repo_ids).select(:id, :name) +
          Archived::Repository.where(id: repo_ids).select(:id, :name)

        repos.map do |repo|
          size_by_date = upcoming_expirations.select do |(repo_id, _effective_at), size|
            repo.id == repo_id
          end

          {
            name: repo.name,
            size_by_date: size_by_date,
            total_size: size_by_date.values.sum,
          }
        end.sort_by { |elem| -1 * elem[:total_size] }
      end

      def artifact_storage_fields(current_storage, owner)
        events = ::Billing::SharedStorage::ArtifactEvent.billable_events_by_repo_and_source(owner_id)

        repos =
          Repository.where(id: current_storage.keys).select(:id, :name, :public) +
          Archived::Repository.where(id: current_storage.keys).select(:id, :name, :public)

        repo_info = repos.map do |repo|
          if current_storage[repo.id].to_i > 0
            repo_visibility = repo.public ? "public" : "private"
            build_shared_storage_usage_entry(
              repo.id,
              repo.name,
              repo_visibility,
              current_storage,
              events: events.select { |k, _| k.first == repo.id },
            )
          end
        end

        if current_storage[nil].to_i > 0
          repo_info << build_shared_storage_usage_entry(
            nil,
            nil,
            nil,
            current_storage,
            events: events.select { |k, _| k.first.nil? },
          )
        end

        repo_info.compact.sort_by { |elem| -1 * elem.total_usage_in_bytes.to_i }
      end

      # Internal: Build a shared storage usage record based on this repos usage
      #
      # events is a hash where the key is an array of [repo_id, source, is_aggregated?]
      # and the value is the summed total size in bytes for the grouped attributes.
      #
      # Example of the sum of 1024 for the aggregated events for actions storage on repo 12345:
      # {
      #   [ 12345, "actions", 1 ] => 1024
      # }
      #
      # Returns an instance of SharedStorageUsage
      def build_shared_storage_usage_entry(repo_id, repo_name, repo_visibility, current_storage, events:)
        total_usage = current_storage[repo_id]

        SharedStorageUsage.new(
          repo_name,
          repo_visibility,
          total_usage.to_i,
          aggregated_actions_usage: [events[[repo_id, "actions", 1]].to_i, 0].max,
          aggregated_gpr_usage: [events[[repo_id, "gpr", 1]].to_i, 0].max,
          unaggregated_actions_usage: events[[repo_id, "actions", 0]].to_i,
          unaggregated_gpr_usage: events[[repo_id, "gpr", 0]].to_i,
        )
      end
    end
  end
end
