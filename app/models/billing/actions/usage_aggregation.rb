# frozen_string_literal: true

module Billing
  module Actions
    class UsageAggregation < ApplicationRecord::Collab
      self.table_name = "actions_usage_aggregations"

      belongs_to :owner, class_name: "User", required: true
      belongs_to :repository, optional: true
      belongs_to :billable_owner, polymorphic: true, optional: true

      validates :repository_id, presence: true
      validates :billable_owner_id, presence: true
      validates :billable_owner_type, presence: true
      validates :aggregate_duration_in_milliseconds, presence: true
      validates :metered_billing_cycle_starts_at, presence: true
      validates :job_runtime_environment, presence: true
      validates :duration_multiplier, presence: true

      after_initialize -> { self.aggregate_duration_in_milliseconds ||= 0 }

      # Public: The attributes that will be used for rolling up Actions
      # line items into a UsageAggregation record
      def self.attributes_for_aggregation
        %w[
          owner_id
          repository_id
          billable_owner_type
          billable_owner_id
          job_runtime_environment
          duration_multiplier
        ]
      end

      # Public: The KV key used to prevent duplicate writes to an aggregation
      # record when #recalculate_aggregate is called while line items are
      # being received and updating the aggregate amount incrementally
      def update_deduplication_key
        [
          "actions",
          "aggregation",
          "owner:#{owner_id}",
          "repo:#{repository_id}",
          "billable:#{billable_owner_type}-#{billable_owner_id}",
        ].join("/")
      end

      # Public: The maximum identifier used to prevent duplicate writes to an
      # aggregation record when #recalculate_aggregate is called while line
      # items are being received and updating the aggregate amount incrementally
      #
      # Returns Integer The value of the maximum ID, or 0 if none is set
      def deduplication_max_id
        GitHub.kv.get(update_deduplication_key).value { 0 }.to_i
      end

      # Public: Sets the maximum identifier used to prevent duplicate writes
      # to an aggregation record when #recalculate_aggregate is called while
      # line items are being received and updating the aggregate amount
      # incrementally
      #
      # max_it - The highest ID of a line item record that will be included in
      #          the calculations of #recalculate_aggregate
      #
      # Returns nothing
      def deduplication_max_id=(max_id)
        GitHub.kv.set(update_deduplication_key, max_id.to_s, expires: 1.day.from_now)
      end

      # Public: Recalculates the aggregate usage for the given metered billing
      # cycle and saves the record
      def recalculate_aggregate
        line_item_scope = Billing::ActionsUsageLineItem
          .where(attributes.slice(*self.class.attributes_for_aggregation))
          .where("end_time >= ?", metered_billing_cycle_starts_at.to_date)

        max_id = ActiveRecord::Base.connected_to(role: :reading) do
          line_item_scope.maximum(:id)
        end

        self.deduplication_max_id = max_id

        ActiveRecord::Base.connected_to(role: :reading) do
          self.aggregate_duration_in_milliseconds = line_item_scope
            .where("id <= ?", max_id)
            .sum("CEIL(duration_in_milliseconds / 60000.0) * 60000.0")
        end

        save!
      end

      def recalculate_aggregate_later
        Billing::AggregateRecalculationJob.perform_later(self)
      end
    end
  end
end
