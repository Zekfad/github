# frozen_string_literal: true

module Billing
  module Actions
    class UsageAggregator
      LineItemOutOfBoundsError = Class.new(StandardError)

      attr_reader :line_item

      # Public: Perform Actions usage aggregation for a given line item
      #
      # line_item - The line item to aggregate with other usage
      #
      # Returns nothing
      # Raises LineItemOutOfBoundsError if the line item end time is not
      #   inside the owner's current metered billing cycle
      def self.perform(line_item)
        new(line_item).perform
      end

      # Public: Initialize the aggregator
      #
      # line_item - The line item to aggregate with other usage
      def initialize(line_item)
        @line_item = line_item
      end

      # Public: Perform Actions usage aggregation for a given line item
      #
      # Returns nothing
      # Raises LineItemOutOfBoundsError if the line item end time is not
      #   inside the owner's current metered billing cycle
      def perform
        UsageAggregation.retry_on_find_or_create_error do
          unless metered_billing_cycle.cover?(line_item.end_time)
            raise LineItemOutOfBoundsError,
              "Line item to aggregate does not fall within the current metered billing cycle"
          end

          if aggregation
            # Actions::UsageAggregation#recalculate_aggregate is running and is
            # already including this line item
            return if line_item.id <= aggregation.deduplication_max_id

            UsageAggregation.github_sql.run(<<~SQL, id: aggregation.id, incremental_usage: line_item.billable_milliseconds_without_multiplier)
              UPDATE actions_usage_aggregations
              SET aggregate_duration_in_milliseconds = aggregate_duration_in_milliseconds + :incremental_usage
              WHERE id = :id
            SQL
          else
            UsageAggregation.create!(
              owner: owner,
              repository_id: line_item.repository_id,
              aggregate_duration_in_milliseconds: line_item.billable_milliseconds_without_multiplier,
              metered_billing_cycle_starts_at: metered_billing_cycle_starts_at,
              job_runtime_environment: line_item.job_runtime_environment,
              duration_multiplier: line_item.duration_multiplier,
              **billable_owner_attributes,
            )
          end
        end
      end

      private

      def aggregation
        @aggregation ||= UsageAggregation.find_by(
          owner: owner,
          repository_id: line_item.repository_id,
          billable_owner_type: line_item.billable_owner_type,
          billable_owner_id: line_item.billable_owner_id,
          metered_billing_cycle_starts_at: metered_billing_cycle_starts_at,
          job_runtime_environment: line_item.job_runtime_environment,
        )
      end

      def billable_owner_attributes
        Billing::MeteredBillingBillableOwnerDesignator
          .attributes_for(owner)
          .slice(:billable_owner_type, :billable_owner_id)
      end

      def metered_billing_cycle
        (metered_billing_cycle_starts_at...metered_billing_cycle_ends_at)
      end

      def metered_billing_cycle_starts_at
        owner.first_day_in_metered_cycle.beginning_of_day
      end

      def metered_billing_cycle_ends_at
        owner.first_day_in_next_metered_cycle.beginning_of_day
      end

      def owner
        line_item.owner
      end
    end
  end
end
