# frozen_string_literal: true

# BillableQuery allows retrieving the billable minutes for billable owner after a specific date
class Billing::Actions::BillableQuery
  include Scientist

  def initialize(billable_owner:, after:, before: nil, owner: nil)
    @root_scope = Billing::ActionsUsageLineItem
      .where(billable_owner: billable_owner)
      .where("end_time >= ?", after)

    @aggregate_root_scope = Billing::Actions::UsageAggregation
      .where(billable_owner: billable_owner)
      .where(metered_billing_cycle_starts_at: after.to_datetime.beginning_of_day)

    @billable_owner = billable_owner
    @owner = owner

    if owner.present?
      @root_scope = @root_scope.where(owner: owner)
      @aggregate_root_scope = @aggregate_root_scope.where(owner: owner)
    end

    if before.present?
      @root_scope = @root_scope.where("end_time < ?", before)
    end
  end

  # Public: Returns the rounded billable minutes executed for private repositories
  # for the billable owner, both broken down by job_runtime_environment and as a total.
  #
  # Example response:
  # { "UBUNTU"=>20, "MACOS"=>1, "WINDOWS"=>3, "TOTAL"=>24 }
  #
  # Returns Hash
  def private_billable_minutes
    rounded_billable_minutes_sum_for(root_scope)
  end

  private

  attr_reader :root_scope, :aggregate_root_scope, :billable_owner, :owner

  def rounded_billable_minutes_sum_for(relation)
    if GitHub.flipper[:actions_read_from_aggregation].enabled?(billable_owner)
      aggregate_root_scope
        .group(:job_runtime_environment)
        .sum("CAST((aggregate_duration_in_milliseconds / 60000.0) * duration_multiplier AS SIGNED)")
        .tap { |usage| usage["TOTAL"] = usage.values.sum }
    else
      science "actions-usage-aggregation-reads" do |experiment|
        experiment.context(
          owner: owner,
          billable_owner: billable_owner,
          metered_cycle: billable_owner.first_day_in_metered_cycle,
          candidate_record_count: aggregate_root_scope.count,
          candidate_sql: aggregate_root_scope.to_sql,
          control_record_count: relation.count,
          control_sql: relation.to_sql,
          datetime: billable_owner.first_day_in_metered_cycle.to_datetime.beginning_of_day,
          backtrace: caller.grep(/(app|lib)\//),
        )
        experiment.use {
          relation
            .group(:job_runtime_environment)
            .sum("CAST(CEIL(duration_in_milliseconds / 60000.0) * duration_multiplier AS SIGNED)")
            .tap { |usage| usage["TOTAL"] = usage.values.sum }
        }
        experiment.try {
          aggregate_root_scope
            .group(:job_runtime_environment)
            .sum("CAST((aggregate_duration_in_milliseconds / 60000.0) * duration_multiplier AS SIGNED)")
            .tap { |usage| usage["TOTAL"] = usage.values.sum }
        }
        experiment.compare do |control, candidate|
          control["TOTAL"] == candidate["TOTAL"]
        end
      end
    end
  end
end
