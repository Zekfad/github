# frozen_string_literal: true

module Billing
  module Actions
    # Encapsulates the Zuora product and rate plan properties for billing
    # for GitHub Actions usage
    class ZuoraProduct
      include GitHub::Billing::ZuoraProduct::ZuoraSettings

      class OverageRatePlanCharge
        attr_reader :name, :unit_cost, :included_units, :unit_of_measure

        def initialize(name:, unit_cost:, included_units:, unit_of_measure:)
          @name = name
          @unit_cost = unit_cost
          @included_units = included_units
          @unit_of_measure = unit_of_measure
        end

        def product_uuid
          ::Billing::ProductUUID.find_by(product_type: "github.actions", product_key: name)
        end

        def zuora_id
          product_uuid.zuora_product_rate_plan_id
        end

        def zuora_charge_ids
          product_uuid.zuora_product_rate_plan_charge_ids
        end
      end

      UOM = "Minutes"
      # Base line based on 1 linux minute.
      # $0.008 / minute Linux
      # $0.016 / minute Windows
      # $0.080 / minute MacOS
      UNIT_COST = "0.008"
      UNIT_COST_IN_CENTS = BigDecimal(UNIT_COST) * 100

      PRIVATE_VISIBILITY_RATE_PLAN_CHARGE = OverageRatePlanCharge.new(
        name: "Private Repos Usage",
        unit_cost: UNIT_COST,
        included_units: 0,
        unit_of_measure: UOM,
      )

      # Public: List of rate plan charges that will be synchronized to zuora
      def self.rate_plan_charges
        [PRIVATE_VISIBILITY_RATE_PLAN_CHARGE]
      end

      # Public: Synchronize the Zuora product and rate plans to Zuora
      # Returns nothing
      def self.sync_to_zuora
        new.sync_to_zuora
      end

      # Public: Synchronize the Zuora product and rate plans to Zuora
      # Returns nothing
      def sync_to_zuora
        return unless GitHub.billing_enabled?

        create_product
        create_rate_plans
      end

      private

      attr_reader :product_id

      # Internal: Create the Zuora product
      # Returns nothing
      def create_product
        result = GitHub.zuorest_client.create_product(
          {
            Name: "GitHub Actions",
            EffectiveStartDate: GitHub::Billing.today.to_s,
            EffectiveEndDate: EFFECTIVE_END_DATE,
          },
        )
        @product_id = result["Id"]
      end

      # Internal: Create the Zuora product rate plans and rate plan charges
      # Returns nothing
      def create_rate_plans
        self.class.rate_plan_charges.each do |rate_plan_charge|
          next if Billing::ProductUUID.exists?(product_type: "github.actions", product_key: rate_plan_charge.name)

          product_rate = GitHub.zuorest_client.create_product_rate_plan(
            Name: "GitHub Actions - #{rate_plan_charge.name}",
            EffectiveStartDate: GitHub::Billing.today.to_s,
            EffectiveEndDate: EFFECTIVE_END_DATE,
            ProductId: product_id,
          )

          product_rate_charges = GitHub.zuorest_client.create_action(
            type: "ProductRatePlanCharge",
            objects: [
              {
                BillingPeriod: "Month",
                ChargeType: "Usage",
                DeferredRevenueAccount: DEFERRED_REVENUE_ACCOUNT,
                Name: "GitHub Actions - #{rate_plan_charge.name}",
                ProductRatePlanId: product_rate["Id"],
                RecognizedRevenueAccount: RECOGNIZED_REVENUE_ACCOUNT,
                TaxCode: TAX_CODE,
                TaxMode: "TaxExclusive",
                Taxable: false,
                TriggerEvent: "ContractEffective",
                ChargeModel: "Overage Pricing",
                IncludedUnits: rate_plan_charge.included_units,
                UOM: rate_plan_charge.unit_of_measure,
                ListPrice: rate_plan_charge.unit_cost.to_s,
                ProductRatePlanChargeTierData: {
                  ProductRatePlanChargeTier: [
                    {
                      Currency: "USD",
                      Price: rate_plan_charge.unit_cost.to_s,
                      PriceFormat: "Per Unit",
                    },
                  ],
                },
              },
            ],
          )

          Billing::ProductUUID.create!(
            product_type: "github.actions",
            product_key: rate_plan_charge.name,
            billing_cycle: :month,
            zuora_product_id: product_id,
            zuora_product_rate_plan_id: product_rate["Id"],
            zuora_product_rate_plan_charge_ids: {
              usage: product_rate_charges[0]["Id"],
            },
          )
        end
      end
    end
  end
end
