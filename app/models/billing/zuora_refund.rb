# frozen_string_literal: true

module Billing
  class ZuoraRefund
    def self.process(*args); new(*args).process; end

    attr_reader :refund_amount, :sale_transaction, :zuora_response

    delegate :platform_transaction_id, to: :sale_transaction

    def initialize(sale_transaction, refund_amount)
      @refund_amount = refund_amount
      @sale_transaction = sale_transaction
    end

    def process
      GitHub.dogstats.time("zuora.timing.transaction_refund") { generate_refund }
    rescue Zuorest::HttpError
      return GitHub::Billing::Result.failure("Transaction not found: #{sale_transaction.transaction_id}")
    end

    private

    def generate_refund
      begin
        @zuora_response = GitHub.zuorest_client.create_refund Amount: refund_amount.dollars,
          PaymentId: platform_transaction_id,
          SourceType: "Payment",
          Type: "Electronic"
        response = GitHub::Billing::Result.from_zuora zuora_response
      rescue Faraday::TimeoutError
        zero_out_refund if zuora_payment.refund_amount.to_f > 0
        raise
      end

      return unrefundable_error if response.failed? || refund_failed?
      log_refund && zero_out_refund if response.success?
      response
    end

    def log_refund
      GitHub.dogstats.increment("billing.refund")

      refund_transaction = sale_transaction.dup
      refund_transaction.update \
        amount_in_cents: -1 * refund_amount.cents,
        old_plan_name: sale_transaction.plan_name,
        plan_name: refund_transaction.user&.plan&.name,
        platform_transaction_id: sale_transaction.transaction_id,
        sale_transaction_id: sale_transaction.transaction_id,
        last_status: :settled,
        transaction_id: zuora_refund["ReferenceID"],
        transaction_type: "refund"
    end

    def zero_out_refund
      ::Billing::Zuora::ZeroOutInvoices.for_transaction(platform_transaction_id)
    end

    def zuora_refund_id
      zuora_response["Id"]
    end

    def unrefundable_error
      GitHub::Billing::Result.failure "Cannot refund a transaction unless it is settled."
    end

    def refund_failed?
      zuora_refund["Status"] == "Error"
    end

    def zuora_payment
      @_zuora_payment ||= Billing::ZuoraPayment.find platform_transaction_id
    end

    def zuora_refund
      @_zuora_refund ||= GitHub.zuorest_client.get_refund zuora_refund_id
    end
  end
end
