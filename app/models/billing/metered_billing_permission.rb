# frozen_string_literal: true

module Billing
  class MeteredBillingPermission

    DOGSTATS_ROOT_VALUE = "billing.metered_billing_permission_check"

    def initialize(owner)
      if owner.try(:delegate_billing_to_business?)
        @owner = owner.business
      else
        @owner = owner
      end
    end

    # Public: Whether a metered billing product usage should be allowed based on owners set metered billing usage limit
    #
    # Returns Boolean
    def usage_allowed?(**args)
      # if billing is not enabled, then all calls to this function
      # will return true, as is the case in GHES.
      return true unless GitHub.billing_enabled?
      total_usage(**args) <= usage_limit
    end

    # Public: Total metered billing usage in whole cents
    #
    # Returns Integer
    def total_usage(additional_actions_private_minutes: 0,
                    additional_package_registry_gigabytes: 0,
                    additional_shared_storage_megabytes: 0)

      GitHub.dogstats.time(DOGSTATS_ROOT_VALUE) do
        total = actions_usage_cost_in_cents(additional_actions_private_minutes) +
                package_registry_usage_cost_in_cents(additional_package_registry_gigabytes) +
                storage_usage_cost_in_cents(additional_shared_storage_megabytes)

        total.floor
      end
    end

    # Public: Owners set metered billing usage limit
    #
    # Returns BigDecimal
    def usage_limit
      @_usage_limit ||=
        begin
          # TODO: this will need to check budgets based on a specific product or group of shared products
          config = owner.budget_for(product: :shared)
          config.enforce_spending_limit ? config.effective_spending_limit_in_subunits : BigDecimal("Infinity")
        end
    end

    def trade_controls_apply?(public:)
      return false if business_owner? || !owner.has_any_trade_restrictions?
      return true if !public
      fully_trade_restricted_organization_owner?
    end

    def fully_trade_restricted_organization_owner?
      owner.organization? && owner.has_full_trade_restrictions?
    end

    # Public: Does owner have a payment issue that restricts paid metered billing overages?
    #
    # Returns Boolean
    def paid_overages_restricted_by_owner_payment_issue?
      return false if business_owner?

      owner.billing_attempts >= 2
    end

    private

    attr_reader :owner

    def prepaid_usage?
      Billing::PrepaidMeteredUsageRefill.enabled_for?(owner)
    end

    def actions_usage_cost_in_cents(private_minutes)
      GitHub.dogstats.time("#{DOGSTATS_ROOT_VALUE}.actions_usage_cost_calculation") do
        @_actions_usage ||= Billing::ActionsUsage.new(owner).
          total_paid_minutes_used(additional_private_minutes: private_minutes) * Billing::Actions::ZuoraProduct::UNIT_COST_IN_CENTS
      end
    end

    def package_registry_usage_cost_in_cents(gigabytes)
      GitHub.dogstats.time("#{DOGSTATS_ROOT_VALUE}.package_registry_usage_cost_calculation") do
        @_package_registry_usage ||= Billing::PackageRegistryUsage.new(owner).
          billable_gigabytes(additional_gigabytes: gigabytes) * Billing::PackageRegistry::ZuoraProduct::UNIT_COST_IN_CENTS
      end
    end

    def storage_usage_cost_in_cents(megabytes)
      GitHub.dogstats.time("#{DOGSTATS_ROOT_VALUE}.storage_usage_cost_calculation") do
        @_storage_usage ||= Billing::SharedStorageUsage.new(owner).
          estimated_monthly_paid_megabytes(additional_megabytes: megabytes) * Billing::SharedStorage::ZuoraProduct::UNIT_COST_PER_MB_IN_CENTS
      end
    end

    def business_owner?
      owner.is_a?(Business)
    end
  end
end
