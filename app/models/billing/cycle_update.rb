# rubocop:disable Style/FrozenStringLiteralComment

# CycleUpdate is responsible for dealing with switching billing cycle
# eg, from yearly -> monthly or monthly -> yearly
class Billing::CycleUpdate
  attr_reader :actor, :last_billed_on, :months_left, :days_left, :old_duration,
    :new_duration, :target
  def self.perform(*args); new(*args).perform; end

  # target - User or Org whose cycle should change
  # new_duration - "year" or "month"
  # options      - :actor (optional, defaults to target)
  #                the user making this change (used for tracking)
  def initialize(target, new_duration, options = {})
    @actor        = options[:actor] || target
    @target       = target
    @old_duration = target.plan_duration
    @new_duration = valid_plan_duration(new_duration)

    if target.braintree_subscription?
      @subscription = Billing::Subscription.for_account(target)
      @days_left    = @subscription.service_days_remaining
    else
      @months_left, @last_billed_on = calculate_months_left_and_last_billed_on
    end
  end

  # Public: Updates the account billing cycle and relevant billing attributes
  #
  # Returns instance of GitHub::Billing::Result
  def perform
    return success if target.invoiced? || same_duration?

    if target.external_subscription?
      schedule_change
    else
      target.plan_duration = new_duration
      set_billed_on if was_yearly?
    end

    target.save!

    track_change if should_track_change?
    success
  end

  # Public: The amount (in cents) we owe the user if they switch yearly -> monthly
  #
  # Returns an integer.
  def refund_in_cents
    if target.braintree_subscription?
      -1 * @subscription.price_of_remaining_service
    else
      return 0 if was_monthly? || months_left.zero?

      -1 * months_left * plan_price.cents
    end
  end

  # Public: The date the user will next be billed
  def next_billed_on
    if was_yearly?
      target.new_billed_on(last_billed_on, "month")
    else
      target.next_billing_date
    end
  end

  def plan_price
    Billing::Pricing.new(
      account: target,
      plan: target.plan,
      seats: target.seats,
      plan_duration: new_duration,
      coupon: target.coupon,
    ).discounted
  end

  private

  # Internal: checks if the new duration for billing the user is
  # the same as the old one
  #
  # Returns: Boolean
  def same_duration?
    target.plan_duration == new_duration
  end

  # Internal: offloads processing of subscription changes
  #
  # Returns: instance of GitHub::Billing::Result
  def schedule_change
    ::Billing::ChangeSubscription.perform \
      target,
      plan: target.plan,
      actor: actor,
      plan_duration: new_duration,
      seat_delta: nil
  end

  def calculate_months_left_and_last_billed_on
    today          = GitHub::Billing.today
    til            = target.billed_on || today
    months_left    = 0
    last_billed_on = nil

    return [0, nil] unless was_yearly?
    return [0, til] unless til > today

    while !last_billed_on do
      potential = til << months_left + 1
      last_billed_on = potential if potential <= today
      months_left += 1 unless last_billed_on
    end

    [months_left, last_billed_on]
  end

  def refund_failure
    failure "We were unable to process a refund at this time."
  end

  def set_billed_on
    target.billing_attempts     = 0
    target.billed_on            = target.new_billed_on(last_billed_on)
  end

  delegate :failure, :success,
    to: GitHub::Billing::Result

  def should_track_change?
    target.has_billing_record? && !target.plan.free?
  end

  def track_change
    target.track_plan_duration_change(actor, old_duration)
  end

  def valid_plan_duration(plan_duration)
    plan_duration == User::YEARLY_PLAN ? User::YEARLY_PLAN : User::MONTHLY_PLAN
  end

  def was_yearly?
    old_duration == User::YEARLY_PLAN
  end

  def was_monthly?
    old_duration == User::MONTHLY_PLAN
  end
end
