# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  # Applies the dunning rules to the given account. This allows us to increment
  # the billing attempts appropriately with our concept of billing attempts until
  # we switch over to Braintree's failure count.  This also checks to see if there
  # are additional actions to perform on the account, like disabling and
  # sending out the appropriate notification.
  class DunSubscription
    def self.perform(*args, **options); new(*args, **options).perform; end

    attr_reader :account, :braintree_subscription, :braintree_transaction, :message

    # account                 - User/Organization that needs to be disabled
    #
    # message                 - An optional String to pass in as the error message.
    # braintree_subscription: - An optional BraintreeSubscription associated with the account.
    # braintree_transaction:  - An optional Braintree::Transaction. Passed in from the BrainTree
    #                           webhook as the 'relevant transaction' see
    #                           app/models/billing/plan_subscription/braintree_webhook.rb
    #
    def initialize(account, message: nil, braintree_subscription: nil, braintree_transaction: nil)
      @account                = account
      @braintree_subscription = braintree_subscription
      @braintree_transaction  = braintree_transaction
      @plan_subscription      = account.plan_subscription
      @message                = message || error_message
    end

    def perform
      dun_subscription!
      send_notification!
    end

    private

    # Private: Apply the dunning rules to the subscription and determine
    # if the account should remain enabled or disabled.
    #
    # Returns Boolean
    def dun_subscription!
      # NB: This uses the old behavior to replace a 0 value with 1 billing attempt.
      # This was probably to safeguard any calls to this as a first attempt.
      # (see https://github.com/github/github/blob/ac80608e/app/models/billing/plan_subscription/disable_account.rb#L44
      # compare
      if @account.billing_attempts.to_i == 0
        @account.update_column(:billing_attempts, 1)
      end

      @account.enable_or_disable!

      if @account.disabled?
        @account.cancel_subscription_items!(force: true)

        if @account.zuora_subscription?
          @plan_subscription.cancel_external_subscription
        end
      end
    end

    # Private: Sends a notification about why the account was disabled.
    #
    # Returns Boolean
    def send_notification!
      if @account.billing_attempts == 1
        if @account.has_credit_card?
          if @account.card_expired?
            BillingNotificationsMailer.cc_expired_failure(@account).deliver_later
          else
            BillingNotificationsMailer.cc_failure(@account, message.to_s).deliver_later
          end
        elsif @account.has_paypal_account?
          BillingNotificationsMailer.paypal_failure(@account, message.to_s).deliver_later
        else
          BillingNotificationsMailer.no_payment_failure(@account).deliver_later
        end
      elsif @account.over_billing_attempts_limit?
        BillingNotificationsMailer.over_billing_attempts_limit_failure(account, message.to_s).deliver_later
      end
    end

    # Private: The error message displayed to the User (in email) when their
    # subscription payment fails.
    #
    # Returns a String
    def error_message
      if braintree_transaction && braintree_transaction.processor_response_text.present?
        braintree_transaction.processor_response_text
      else
        "Call your payment provider to resolve this issue."
      end
    end
  end
end
