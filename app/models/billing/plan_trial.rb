# frozen_string_literal: true

module Billing
  class PlanTrial < ApplicationRecord::Collab
    areas_of_responsibility :gitcoin

    belongs_to :user
    belongs_to :pending_plan_change

    def self.active_for?(user, plan)
      !!find_by(user: user, plan: plan)&.active?
    end

    def active?
      pending_plan_change.present? &&
        !pending_plan_change.is_complete?
    end

    def expired_within?(days)
      !active? &&
        expires_on.present? &&
        expires_on > GitHub::Billing.today - days
    end

    def log_plan_change(new_plan:, user_initiated:, active: active?)
      GlobalInstrumenter.instrument(
        "trial.plan_change",
        account: user,
        trial_plan: plan,
        new_plan: new_plan,
        trial_expired: !active,
        user_initiated: user_initiated,
      )
    end

    def enterprise_cloud?
      plan == GitHub::Plan::BUSINESS_PLUS
    end

    private

    def expires_on
      pending_plan_change&.active_on
    end
  end
end
