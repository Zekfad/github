# frozen_string_literal: true

module Billing
  class SchedulePlanChange
    def self.run(**options) new(**options).run end

    BILLING_CYCLE_BUFFER_TIME = 30.minutes

    def initialize(account:, actor:, seats: nil, plan: nil, plan_duration: nil, data_packs: nil, subscribable: nil, subscribable_quantity: nil, active_on: nil, free_trial: false)
      @account = account
      @actor = actor
      @active_on = active_on || account.next_billing_date
      @free_trial = free_trial
      @data_packs = data_packs
      @plan = plan
      @plan_duration = plan_duration
      @seats = seats
      @subscribable = subscribable
      @subscribable_quantity = subscribable_quantity
    end

    def run
      validate_plan_input
      return result if result.failed?

      assign_change_attributes

      if change.save && create_or_update_subscribable_change
        RunPendingPlanChangeJob
          .set(wait_until: schedule_change_at(change))
          .perform_later(change)
        GitHub::Billing::Result.success
      else
        GitHub::Billing::Result.failure error_messages
      end
    end

    private

    attr_writer :change
    attr_accessor :account, :actor, :data_packs,
      :subscribable, :subscribable_quantity, :plan,
      :plan_duration, :result, :seats, :free_trial

    def change
      @change ||=
        if free_trial
          account.pending_plan_changes.build
        else
          account.pending_plan_changes.incomplete.where(active_on: @active_on).first || account.pending_plan_changes.build
        end
    end

    # Private: Time to schedule RunPendingPlanChangeJob
    # To limit the number of concurrent requests, it adds an arbitary 0-59 second offset.
    #
    # Returns DateTime
    def schedule_change_at(change)
      offset_seconds = (change.id % 60).seconds
      change.active_on.to_datetime - BILLING_CYCLE_BUFFER_TIME + offset_seconds
    end

    def assign_change_attributes
      change.plan = plan if changing_plan?
      change.seats = [seats.to_i, base_seats].max if changing_seats? || (changing_plan? && change.plan&.per_seat?)

      change.plan_duration = plan_duration if changing_duration?
      change.active_on = @active_on
      change.actor = actor
      change.data_packs = data_packs if data_packs
    end

    def base_seats
      change.plan&.per_seat? ? change.plan.base_units : 0
    end

    def changing_plan?
      plan && plan != account.plan
    end

    def changing_seats?
      seats && seats != account.seats
    end

    def changing_duration?
      plan_duration && plan_duration != account.plan_duration
    end

    def validate_plan_input
      account.seats = seats
      account.plan = plan.to_s
      account.plan_duration = plan_duration
      self.result = account.valid? ? GitHub::Billing::Result.success : invalid_input_error
      reset_account_state
    end

    def reset_account_state
      account.seats = account.seats_was
      account.plan = account.plan_was
      account.plan_duration = account.plan_duration_was
    end

    def invalid_input_error
      GitHub::Billing::Result.failure account.errors.full_messages.to_sentence
    end

    def create_or_update_subscribable_change
      return true unless subscribable || subscribable_quantity
      pending_subscription_item_change.update \
        free_trial: free_trial,
        subscribable: subscribable,
        quantity: subscribable_quantity
    end

    def pending_subscription_item_change
      @pending_subscription_item_change ||= begin
        change.pending_subscription_item_changes.for_subscribable_listing(subscribable.listing).first \
          || change.pending_subscription_item_changes.build(subscribable: subscribable)
      end
    end

    def error_messages
      change.errors.full_messages.to_sentence
    end
  end
end
