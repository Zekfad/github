# frozen_string_literal: true

class Billing::BundledLicenseAssignment < ApplicationRecord::Collab
  belongs_to :business, optional: true
  belongs_to :user, optional: true

  validates :enterprise_agreement_number, presence: true
  validates :email, presence: true, length: { maximum: 320 }
  validates :subscription_id, presence: true, uniqueness: { case_sensitive: false }
  validates :revoked, inclusion: { in: [false, true] }

  after_commit :set_user_id, on: [:create, :update], if: :saved_change_to_business_id?

  scope :assigned, -> { where.not(user_id: nil) }
  scope :unassigned, -> { where(user_id: nil) }
  scope :orphaned, -> { where(business: nil) }
  scope :for_enterprise_agreement, -> (agreement_number) { where("enterprise_agreement_number = ?", agreement_number) }
  scope :nonrevoked,  -> { where(revoked: false) }

  def assigned?
    user_id?
  end

  private

  def set_user_id
    Billing::SetUserFromBusinessOnBundledLicenseAssignmentJob.perform_later(assignment: self)
  end
end
