# frozen_string_literal: true

# BillableQuery allows retrieving the billable megabyte hours for billable owner after a specific date
class Billing::SharedStorage::BillableQuery
  include Scientist

  def initialize(billable_owner:, after:, before: nil, owner: nil)
    @billable_owner = billable_owner

    @root_scope = Billing::SharedStorage::ArtifactAggregation
      .where(billable_owner: billable_owner)
      .where("aggregate_effective_at >= ?", after)

    if owner.present?
      @root_scope = @root_scope.where(owner: owner)
    end

    if before.present?
      @root_scope = @root_scope.where("aggregate_effective_at < ?", before)
    end
  end

  # Public: Returns the rounded billable megabyte hours stored for private repositories for the billable owner
  #
  # Returns Integer
  def private_billable_megabyte_hours
    rounded_billable_megabyte_hours_sum_for(root_scope.private_visibility)
  end

  # Public: Returns the current billable megabytes stored for private repositories for the billable
  # owner from the most recent aggregation as a rounded integer
  #
  # Returns Integer
  def latest_private_billable_stored_megabytes
    (latest_usage_per_repo_scope.sum(:aggregate_size_in_bytes) / 1.megabyte).to_i
  end

  # Public: Returns the aggregate size in bytes from the latest aggregation, by repository
  #
  # Returns Hash
  def latest_aggregate_size_in_bytes_by_repository_id
    latest_usage_per_repo_scope.pluck(:repository_id, :aggregate_size_in_bytes).to_h
  end

  # Public: Returns the rounded billable megabyte hours stored for public repositories for the billable owner
  #
  # Returns Integer
  def public_billable_megabyte_hours
    rounded_billable_megabyte_hours_sum_for(root_scope.public_visibility)
  end

  private

  attr_reader :root_scope, :billable_owner

  def rounded_billable_megabyte_hours_sum_for(relation)
    (relation.sum("aggregate_size_in_bytes") / Numeric::MEGABYTE).to_i
  end

  def latest_usage_per_repo_scope
    private_usage_scope = root_scope.private_visibility

    date_join_query = private_usage_scope.
      group(:repository_id).
      select(:repository_id, "MAX(aggregate_effective_at) as most_recent_aggregate_effective_at").
      to_sql

    private_usage_scope.joins(<<-SQL)
      INNER JOIN (#{date_join_query}) as date_join
      ON most_recent_aggregate_effective_at = aggregate_effective_at
        AND (
          date_join.repository_id = `shared_storage_artifact_aggregations`.repository_id
          OR (date_join.repository_id IS NULL AND `shared_storage_artifact_aggregations`.repository_id IS NULL)
        )
    SQL
  end

  def latest_size_in_bytes_for_repo(usage_scope:, repo_id:)
    usage_scope
      .where(repository_id: repo_id)
      .order(aggregate_effective_at: :desc)
      .pick(:aggregate_size_in_bytes)
  end
end
