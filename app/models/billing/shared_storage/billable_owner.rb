# frozen_string_literal: true

module Billing::SharedStorage
  class BillableOwner < ApplicationRecord::Collab
    self.table_name = "shared_storage_billable_owners"

    belongs_to :owner, class_name: "User", required: true
    belongs_to :billable_owner, polymorphic: true, required: true
  end
end
