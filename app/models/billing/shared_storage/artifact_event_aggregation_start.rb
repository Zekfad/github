# frozen_string_literal: true

module Billing::SharedStorage
  class ArtifactEventAggregationStart
    NEXT_RUN_KV_KEY = "billing.shared_storage.artifact_event_aggregation.next_run"

    # Start artifact event aggregation for the next cutoff time
    #
    # Loads the appropriate cutoff time from KV, defaulting to the current hour,
    # and enqueues a FanoutAggregationJob to aggregate all artifact events with
    # and effective date before that cutoff time. If the cutoff time in KV is
    # in the future, no FanoutAggregationJob is enqueued as it's not yet time
    # to do any aggregation. In the event that some hours have been missed --
    # that is, the cutoff in KV is more than 1 hour in the past -- then a
    # FanoutAggregationJob is enqueued for each hour that needs to be
    # aggregated, allowing things to self-heal.
    #
    # Returns nothing
    def perform
      cutoff = next_run_time || current_hour
      return if cutoff.future?
      fix_flag_enabled = GitHub.flipper[:billing_fanout_aggregation_fix].enabled?

      until cutoff.future?
        if fix_flag_enabled
          new_cutoff = cutoff + 1.hour
          GitHub.kv.set(NEXT_RUN_KV_KEY, new_cutoff.to_i.to_s)
        end

        FanoutAggregationJob.perform_later(cutoff)
        if fix_flag_enabled
          cutoff = new_cutoff
        else
          cutoff = cutoff + 1.hour
        end
      end

      unless fix_flag_enabled
        GitHub.kv.set(NEXT_RUN_KV_KEY, cutoff.to_i.to_s)
      end
    rescue GitHub::KV::UnavailableError
      # If KV fails, we want to bail and let the timer retry later
      GitHub.dogstats.increment(
        "billing.shared_storage.aggregation_error",
        tags: ["job:start_aggregation_job", "error:kv_unavailable"],
      )
    end

    private

    def next_run_time
      timestamp = GitHub.kv.get(NEXT_RUN_KV_KEY).value!
      Time.at(timestamp.to_i) if timestamp
    end

    def current_hour
      Time.now.beginning_of_hour
    end
  end
end
