# frozen_string_literal: true

module Billing::SharedStorage
  class ArtifactEventAggregationFanout
    attr_reader :cutoff

    AGGREGATE_FIELDS = [:owner_id, :billable_owner_id, :billable_owner_type, :directly_billed].freeze

    def initialize(cutoff:)
      @cutoff = cutoff

      @restraint = GitHub::Restraint.new
    end

    def perform
      restraint.lock!(lock_key, _concurrency = 1, _ttl = 5.minutes) do
        perform_without_lock
      end
    end

    def perform_without_lock
      GitHub.dogstats.count("billing.shared_storage.users_selected", unique_aggregates.count)

      unique_aggregates.each do |aggregate_fields|
        AggregationJob.perform_later(cutoff: cutoff, **aggregate_fields)
      end
    end

    private

    attr_reader :restraint

    def unique_aggregates
      @_unique_aggregates ||= ActiveRecord::Base.connected_to(role: :reading) do
        ArtifactEvent.select(*AGGREGATE_FIELDS).distinct.pluck(*AGGREGATE_FIELDS).map { |row| AGGREGATE_FIELDS.zip(row).to_h }
      end
    end

    def lock_key
      "shared-storage/artifact-event-aggregation-fanout"
    end
  end
end
