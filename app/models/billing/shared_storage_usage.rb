# frozen_string_literal: true

class Billing::SharedStorageUsage
  HOURS_IN_DAY = 24
  HOURS_IN_ASSUMED_MONTH = Billing::SharedStorage::ZuoraProduct::ASSUMED_MONTHLY_DAYS * HOURS_IN_DAY

  def initialize(billable_owner, owner: nil)
    @billable_owner = billable_owner
    @plan = billable_owner.plan
    @owner = owner
  end

  def estimated_monthly_private_megabytes
    (private_megabyte_hours_used + estimated_remaining_private_megabyte_hours_used) / HOURS_IN_ASSUMED_MONTH
  end

  def estimated_monthly_paid_megabytes(additional_megabytes: 0)
    prorated_additional_megabytes = (additional_megabytes * hours_left_in_billing_cycle).to_f / HOURS_IN_ASSUMED_MONTH
    [0, estimated_monthly_private_megabytes + prorated_additional_megabytes - plan_included_megabytes].max
  end

  def paid_usage_percentage
    return 100 if plan_included_megabytes.zero?
    percent = (estimated_monthly_paid_megabytes / Float(plan_included_megabytes)).round(3)
    (percent * 100).to_i
  end

  def included_usage_percentage
    return 100 if plan_included_megabytes.zero?
    percent = (estimated_monthly_private_megabytes / Float(plan_included_megabytes)).round(3)
    [(percent * 100).to_i, 100].min
  end

  def private_megabyte_hours_used
    billable_query.private_billable_megabyte_hours
  end

  def plan_included_megabytes
    plan.shared_storage_included_megabytes
  end

  def latest_private_billable_stored_megabytes
    billable_query.latest_private_billable_stored_megabytes
  end

  def days_left_in_billing_cycle
    hours_left_in_billing_cycle / HOURS_IN_DAY
  end

  private

  attr_reader :billable_owner, :plan, :owner

  def estimated_remaining_private_megabyte_hours_used
    latest_private_billable_stored_megabytes * hours_left_in_billing_cycle
  end

  def hours_left_in_billing_cycle
    HOURS_IN_ASSUMED_MONTH - hours_into_billing_cycle
  end

  def hours_into_billing_cycle
    ((GitHub::Billing.now - billable_owner.first_day_in_metered_cycle.in_time_zone(GitHub::Billing.timezone)) / 1.hour).to_i
  end

  def billable_query
    @_query ||= Billing::SharedStorage::BillableQuery.new(billable_owner: billable_owner, owner: owner, after: billable_owner.first_day_in_metered_cycle)
  end
end
