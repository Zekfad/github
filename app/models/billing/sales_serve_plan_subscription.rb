# frozen_string_literal: true

class Billing::SalesServePlanSubscription < ApplicationRecord::Collab
  self.table_name = "billing_sales_serve_plan_subscriptions"
  serialize :zuora_rate_plan_charges, Hash

  belongs_to :customer

  validates :customer, presence: true
  validates :zuora_subscription_id, presence: true
  validates :zuora_subscription_number, presence: true
  validates :billing_start_date, presence: true

  def zuora_rate_plan_charge_number(product_rate_plan_charge_id:)
    zuora_rate_plan_charges.dig(product_rate_plan_charge_id, :number)
  end
end
