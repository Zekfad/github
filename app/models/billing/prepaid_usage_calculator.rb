# frozen_string_literal: true

module Billing
  class PrepaidUsageCalculator
    def initialize(owner)
      @owner = owner
    end

    def amount_remaining_in_cents
      refill_calculator.remaining_balance_in_cents
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def over_prepaid_limit?
      @_over_prepaid_limit ||= billing_cycle_costs_in_cents >= prepaid_overage_limit
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def billing_cycle_costs_in_cents
      refill_calculator.billing_cycle_costs_in_cents
    end

    def percent_used
      @_percent_used ||= billing_cycle_costs_in_cents / total_purchased_refills_in_cents.to_f * 100
    end

    def total_purchased_refills_in_cents
      @_total_purchased_refills ||= Billing::PrepaidMeteredUsageRefill.total_active_amount_in_cents_for(owner: owner)
    end

    private

    attr_reader :owner

    def refill_calculator
      @_refill_calculator ||= RemainingRefillCalculator.new(owner)
    end

    def prepaid_overage_limit
      @_prepaid_overage_limit ||= total_purchased_refills_in_cents * Billing::PrepaidMeteredUsageRefill::OVERAGE_LIMIT_MULTIPLIER
    end
  end
end
