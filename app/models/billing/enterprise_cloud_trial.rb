# frozen_string_literal: true

module Billing
  class EnterpriseCloudTrial
    INITIAL_SEAT_COUNT = 50
    TRIAL_LENGTH = 14.days
    MAX_TRIAL_LENGTH = 90
    TOS_TYPE = "Evaluation"
    EXPIRATION_MESSAGE_DURATION = 90.days

    include Instrumentation::Model

    delegate(:expired_within?, to: :plan_trial)
    delegate(:id, to: :plan_trial, prefix: true)

    def self.eligible?(org = nil)
      new(org).eligible?
    end

    def initialize(organization)
      @organization = organization
    end

    def create
      return false unless eligible?

      Organization.transaction do
        old_plan = organization.read_attribute(:plan)
        old_seats = organization.seats

        organization.update!(
          plan: GitHub::Plan::BUSINESS_PLUS,
          seats: INITIAL_SEAT_COUNT,
        )

        pending_plan_change = organization.pending_plan_changes.create!(
          plan: old_plan,
          active_on: GitHub::Billing.today + TRIAL_LENGTH,
          seats: old_seats,
        )

        Billing::PlanTrial.create(
          user: organization,
          pending_plan_change: pending_plan_change,
          plan: GitHub::Plan::BUSINESS_PLUS,
        )

        organization.set_beta_features_for_plan

        log_signup(old_plan)

        true
      end
    end

    def deactivate!
      update_terms
      organization.touch
      pending_plan_change&.cancel.tap do
        log_trial_ended
      end
    end

    def eligible?
      return true if organization.nil?
      !PlanTrial.exists?(user: organization, plan: GitHub::Plan::BUSINESS_PLUS)
    end

    def ever_been_in_trial?
      plan_trial.present?
    end

    def started_on
      plan_trial.created_at.in_time_zone(GitHub::Billing.timezone).to_date
    end

    def days_active
      plan_trial_created_on = plan_trial.created_at.in_time_zone(GitHub::Billing.timezone).to_date
      (GitHub::Billing.today - plan_trial_created_on).to_i
    end

    def days_remaining
      return 0 if expired?
      (pending_plan_change.active_on - GitHub::Billing.today).to_i
    end

    def time_remaining
      return 0 if pending_plan_change.nil?
      Time.at(pending_plan_change.active_on.to_time - GitHub::Billing.now)
    end

    def duration_in_days
      return 0 if pending_plan_change.nil?
      (pending_plan_change.active_on - started_on).to_i
    end

    def active?
      return false if organization.nil?

      GitHub.cache.fetch(cache_key) do
        !!plan_trial&.active?
      end
    end

    def expired?
      !active?
    end

    def can_extend_trial?(days = nil)
      return false if pending_plan_change.nil?

      if days.present?
        new_trial_end_date = pending_plan_change.active_on + days
        plan_trial_created_on = plan_trial.created_at.in_time_zone(GitHub::Billing.timezone).to_date
        new_trial_length = (new_trial_end_date - plan_trial_created_on).to_i

        new_trial_length <= MAX_TRIAL_LENGTH
      else
        duration_in_days < MAX_TRIAL_LENGTH
      end
    end

    def extend_trial(days, ignore_max: false)
      if ignore_max || can_extend_trial?(days)
        old_duration = duration_in_days

        pending_plan_change.update(
          is_complete: false,
          active_on: pending_plan_change.active_on + days,
        ).tap do
          log_trial_extension(old_duration: old_duration)
        end
      else
        false
      end
    end

    def expires_on
      return unless ever_been_in_trial?
      pending_plan_change&.active_on
    end

    def ensure_addons_remain_billable_at_trial_expiry
      if active? && pending_plan_change.plan.name == GitHub::Plan::FREE
        pending_plan_change.update(plan: GitHub::Plan::FREE_WITH_ADDONS)
      end
    end

    def ensure_free_plan_at_trial_expiry
      if active? && pending_plan_change.plan.name == GitHub::Plan::FREE_WITH_ADDONS
        pending_plan_change.update(plan: GitHub::Plan::FREE)
      end
    end

    def log_plan_change(new_plan:, user_initiated:, active: nil)
      plan_trial.log_plan_change(new_plan: new_plan, user_initiated: user_initiated, active: active)
    end

    private

    attr_reader :organization

    private *delegate(:pending_plan_change, to: :plan_trial)

    def update_terms
      return false unless ever_been_in_trial?
      return false unless organization.terms_of_service.evaluation?

      organization.terms_of_service.update(
        type: "Corporate",
        actor: User.ghost,
        staff_actor: true,
        change_note: "auto update via enterprise cloud trial",
      )
    end

    def cache_key
      "billing-#{GitHub::Plan::BUSINESS_PLUS}-trial-#{organization.id}-#{GitHub::Billing.today}-#{organization.updated_at}"
    end

    def plan_trial
      @_plan_trial ||= Billing::PlanTrial.find_by(
        user: organization,
        plan: GitHub::Plan::BUSINESS_PLUS,
      )
    end

    def log_signup(old_plan)
      GlobalInstrumenter.instrument("trial.signup", {
        account: @organization,
        previous_plan: old_plan,
        current_plan: organization.plan.name,
      })

      instrument(:trial_start)
    end

    def log_trial_extension(old_duration:)
      GlobalInstrumenter.instrument("trial.extended", {
        account: @organization,
        previous_duration_in_days: old_duration,
        new_duration_in_days: duration_in_days,
      })

      instrument(:trial_extend, {
        duration_in_days_was: old_duration,
        duration_in_days: duration_in_days,
      })
    end

    def log_trial_ended
      instrument(:trial_end)
    end

    def event_prefix
      :billing
    end

    def event_payload
      {
        org: @organization,
        plan: GitHub::Plan::BUSINESS_PLUS,
      }
    end
  end
end
