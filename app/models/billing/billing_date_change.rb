# rubocop:disable Style/FrozenStringLiteralComment

class Billing::BillingDateChange
  attr_reader :account, :new_bill_date, :old_bill_date, :reason

  def initialize(account, new_bill_date, options = {})
    @account       = account
    @old_bill_date = account.billed_on
    @new_bill_date = date(new_bill_date)
    @reason        = options.fetch :reason, "Invoiced account"
  end

  def perform
    return invalid_message if invalid_billing_change?
    result = perform_date_change
  rescue => e
    return result if result && result.failed?
    message = e.message || "error updating billing date"
    return GitHub::Billing::Result.failure(message)
  end

  def amount_due
    @amount_due ||= if account.invoiced?
      Billing::Money.new(0)
    else
      # NB - Changing dates like this is strange, so just make a rough estimate
      # based on effective $/day of current plan for a typical year. This is
      # different than Braintree/Zuora prorate logic.
      average_days_per_month = 365 / 12.0

      Billing::Pricing.new(
        account: account,
        plan: account.plan,
        seats: account.seats,
        plan_duration: User::MONTHLY_PLAN,
      ).discounted * (days_delta / average_days_per_month)
    end
  end

  def days_delta
    return 0 unless old_bill_date && new_bill_date
    (new_bill_date - old_bill_date).to_i
  end

  private

  def perform_date_change
    account.billed_on = new_bill_date
    instrument_change
    update_account
  end

  def instrument_change
    payload = {
      reason: reason,
      old_date: old_bill_date.to_s,
      date: new_bill_date.to_s,
      amount_due: amount_due.format,
    }

    if account.organization?
      payload[:org] = account
    else
      payload[:user] = account
    end

    GitHub.instrument "account.billing_date_change", payload
  end

  def date(input)
    return input if input.is_a?(Date)
    input.to_date
  end

  def update_account
    if account.save
      GitHub::Billing::Result.success
    else
      GitHub::Billing::Result.failure(account.errors.full_messages.join)
    end
  end

  def invalid_message
    GitHub::Billing::Result.failure \
      "#{account} has a pending plan change, so we're unable to change their billing date."
  end

  def invalid_billing_change?
    !!account.pending_cycle_change
  end
end
