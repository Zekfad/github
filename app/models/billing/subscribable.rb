# frozen_string_literal: true

module Billing
  module Subscribable
    extend ActiveSupport::Concern

    def monthly_price_in_dollars
      monthly_price_in_cents.abs / 100
    end

    def yearly_price_in_dollars
      yearly_price_in_cents.abs / 100
    end

    # Public: Returns the base price for this plan/tier.
    #
    # duration - The duration price to return.
    #
    # Returns a Billing::Money
    def base_price(duration: :month)
      Billing::Money.new \
        duration.to_sym == :month ? monthly_price_in_cents : yearly_price_in_cents
    end

    def prorated_total_price(account: nil, quantity: 1)
      return (base_price * quantity) unless account

      # Use Billing::Subscription to figure out how much to prorate
      subscription = ::Billing::Subscription.new(ends: account.billed_on)
      service_remaining = Rational(subscription.service_days_remaining, subscription.duration_in_days)
      service_remaining = 1 if service_remaining.zero?

      subscription_item = ::Billing::SubscriptionItem.new \
        subscribable_type: self.class.name,
        subscribable_id: self.id,
        quantity: quantity
      subscription_item.set_free_trial_ends_on

      Billing::Pricing.new(
        plan_duration: User::MONTHLY_PLAN,
        subscription_item: subscription_item,
        service_remaining: service_remaining,
      ).discounted
    end

    # Public: Returns true if this is a paid plan/tier.
    def paid?
      monthly_price_in_cents != 0 || yearly_price_in_cents != 0
    end

    # Public: Returns true if the given User/Organization can purchase this plan/tier.
    def can_subscribe_with_account?(account)
      return false if for_organizations_only? && account.user?
      return false if for_users_only? && account.organization?

      true
    end

    def account_type_text
      return "personal" if for_users_only?
      "organization" if for_organizations_only?
    end
  end
end
