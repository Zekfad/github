# frozen_string_literal: true

module Billing
  class MeteredBillingProduct

    # Public: Returns hash of each usage line item class by product key
    # If usage class does not exist in hash by key then raises ArgumentError
    #
    #
    # Returns Hash
    def self.products
      return @products if defined?(@products)

      @products = Hash.new do |key|
        raise ArgumentError.new("Unknown metered product: #{key}")
      end

      @products[:actions] = Billing::ActionsUsageLineItem
      @products[:packages] = Billing::PackageRegistry::DataTransferLineItem
      @products[:storage] = Billing::SharedStorage::ArtifactAggregation
      @products[:codespaces_compute] = Billing::Codespaces::ComputeUsageLineItem
      @products[:codespaces_storage] = Billing::Codespaces::StorageUsageLineItem

      @products
    end
  end
end
