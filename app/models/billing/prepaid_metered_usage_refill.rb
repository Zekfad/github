# frozen_string_literal: true

class Billing::PrepaidMeteredUsageRefill < ApplicationRecord::Collab
  include Instrumentation::Model

  self.table_name = "billing_prepaid_metered_usage_refills"
  areas_of_responsibility :gitcoin

  belongs_to :owner, polymorphic: true

  validates :expires_on, presence: true
  validates :amount_in_subunits, presence: true
  validates :currency_code, presence: true
  validates :zuora_rate_plan_charge_id, uniqueness: { case_sensitive: true, allow_nil: true }

  scope :expired, -> { where("expires_on < :now", now: GitHub::Billing.now) }
  scope :active, -> { where("expires_on >= :now", now: GitHub::Billing.now) }

  after_create_commit :log_create_event

  OVERAGE_LIMIT_MULTIPLIER = 1.5

  def self.enabled_for?(owner)
    if owner.is_a?(Business)
      owner.pays_github_directly?
    else
      owner.organization? &&
        owner.invoiced? &&
        !owner.delegate_billing_to_business?
    end
  end

  def self.total_active_amount_in_cents_for(owner:)
    Billing::PrepaidMeteredUsageRefill.active.where(owner: owner).sum(:amount_in_subunits)
  end

  def staff_created?
    zuora_rate_plan_charge_id.blank?
  end

  private

  def event_prefix
    :prepaid_metered_refill
  end

  # Default values to passed to events created by #instrument
  def event_payload
    payload = {
      amount_in_subunits: amount_in_subunits,
      currency_code: currency_code,
      expires_on: expires_on,
      staff_created: zuora_rate_plan_charge_id.nil?,
    }

    if owner.is_a?(Business)
      payload[:business] = owner
    elsif owner.is_a?(Organization)
      payload[:org] = owner
    else
      payload[:user] = owner
    end

    payload
  end

  def log_create_event
    instrument :create
  end
end
