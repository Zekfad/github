# frozen_string_literal: true

module Billing
  class SubscriptionItemUpdater
    def self.perform(**options) new(**options).perform end

    def initialize(account:, force: false, subscribable:, quantity:, sender:, start_free_trial: false,  end_free_trial: false)
      @account = account
      @force = force
      @subscribable = subscribable
      @quantity = quantity.to_i
      @sender = sender
      @start_free_trial = start_free_trial
      @end_free_trial = end_free_trial
      @invalidate_free_trials = false

      if @subscription_item = find_subscription_item
        @previous_subscribable = subscription_item.subscribable
        @previously_on_free_trial = subscription_item.on_free_trial?
        @previous_free_trial_ends_on = subscription_item.free_trial_ends_on
        @previous_quantity = subscription_item.quantity
      end
    end

    # Updates an existing subscription item quantity, or creates a new subscription item with a new subscribable
    # while cancelling the old item
    #
    # account - the account being updated
    # subscribable - the subscribable tied to the new item
    # quantity - the new quantity tied to the item
    # sender - the user making the change (owner or billing manager)
    #
    # Returns Hash - { success: true/false, subscription_item: if_success, errors: if_failure }
    def perform
      return { success: false, errors: ["Account has no subscription item to update"]} unless subscription_item.present?
      return { success: true, subscription_item: subscription_item } if nothing_changed_for_sponsorable_item?

      raise invalid_purchase_error if invalid_purchase?

      if cancelling_free_plan?
        update_or_create_subscription_item
      elsif schedule_change?
        schedule_pending_change
      else
        update_or_create_subscription_item
        update_pending_change
      end

      if save_and_validate_items
        send_purchase_webhook unless @pending_change_scheduled
        send_change_webhook if @pending_change_scheduled && !cancelling?
        update_sponsorship

        instrument_change

        { success: true, subscription_item: subscription_item }
      else
        update_sponsorship

        { success: false, errors: item_errors }
      end
    end

    private

    attr_reader :account, :force, :subscribable, :previous_subscribable,
      :previous_quantity, :quantity, :sender, :start_free_trial,
      :end_free_trial, :previously_on_free_trial, :previous_free_trial_ends_on
    attr_accessor :old_subscription_item, :subscription_item, :invalidate_free_trials

    delegate :plan_subscription, to: :account
    delegate :listing, to: :subscribable

    # Internal: Checks if the subscription_item being "updated" is a sponsorable
    # item, and then whether the quantity and subscribable are unchanged.
    #
    # If nothing changed, we don't want to instrument.
    # This happens when a user updates the privacy setting on a sponsorship.
    # That will be captured in Hydro, but its not a "plan change" as Transaction
    # records are used for.
    #
    # Returns: Boolean
    def nothing_changed_for_sponsorable_item?
      subscription_item.listable_is_sponsorable? &&
        quantity == previous_quantity &&
        subscribable == previous_subscribable
    end

    def update_free_trial
      if subscription_item.on_free_trial? && end_free_trial == true
        subscription_item.free_trial_ends_on = GitHub::Billing.today
      end
    end

    def transfer_free_trial(from: old_subscription_item, to: subscription_item)
      @start_free_trial = true
      to.free_trial_ends_on = from.free_trial_ends_on
      schedule_pending_change
    end

    def find_subscription_item
      plan_subscription&.subscription_item_for_listing(listing)
    end

    def update_or_create_subscription_item
      if subscription_item.subscribable == subscribable
        self.old_subscription_item = nil
        subscription_item.quantity = quantity
        update_free_trial
      else
        self.old_subscription_item = subscription_item
        old_subscription_item.quantity = 0

        if self.subscription_item = plan_subscription.subscription_items.cancelled.where(subscribable: subscribable).first
          subscription_item.quantity = quantity
        else
          self.subscription_item = ::Billing::SubscriptionItem.new \
            subscribable: subscribable,
            quantity: quantity,
            plan_subscription: plan_subscription
        end

        old_pending_item_change = old_pending_subscription_item_change
        old_pending_item_change.destroy if old_pending_item_change && old_pending_item_change.free_trial

        if old_subscription_item.on_free_trial?
          if subscribable.has_free_trial?
            transfer_free_trial
          else
            self.invalidate_free_trials = true
            old_subscription_item.free_trial_ends_on = GitHub::Billing.today
          end
        end

        if subscription_item.eligible_for_free_trial? && !@pending_change_scheduled
          @start_free_trial = true
          schedule_pending_change
        end
      end
    end

    def invalid_purchase?
      invalid_account? && increased_quantity?
    end

    def invalid_account?
      account.disabled? || account.dunning?
    end

    def increased_quantity?
      quantity > subscription_item.quantity
    end

    def invalid_purchase_error
      error_msg = "Your account is currently locked from purchases. Please update your payment information"
      ::Platform::Errors::Unprocessable.new error_msg
    end

    def save_and_validate_items
      ::Billing::SubscriptionItem.transaction do
        old_item_valid = old_subscription_item ? old_subscription_item.save : true
        item_valid = subscription_item.save

        success = old_item_valid && item_valid
        cancel_free_trials if success && invalidate_free_trials

        success
      end
    end

    def cancel_free_trials
      # NOTE: only marketplace offers free trials, so the scope/_id here are as yet unchanged
      listing_plans_ids = Marketplace::ListingPlan.where(
        marketplace_listing_id: subscribable.marketplace_listing_id,
      ).pluck(:id)

      free_trial_ids = plan_subscription
        .subscription_items
        .for_marketplace_listing_plans(listing_plans_ids)
        .where("free_trial_ends_on IS NOT NULL")
        .pluck(:id)

      plan_subscription
        .subscription_items
        .where(id: free_trial_ids)
        .update_all(free_trial_ends_on: GitHub::Billing.today)
    end

    def send_purchase_webhook
      GitHub.instrument "#{event_prefix}.#{purchase_event_type}",
        subscription_item_id: subscription_item.id,
        sender_id: sender.id,
        previous_quantity: previous_quantity,
        previously_on_free_trial: previously_on_free_trial,
        previous_free_trial_ends_on: previous_free_trial_ends_on,
        previous_subscribable_id: previous_subscribable.id,
        previous_subscribable_type: previous_subscribable.class.name
    end

    def send_change_webhook
      # nb: the item_change can be missing if the plan change already ran
      return unless pending_subscription_item_change

      GitHub.instrument "#{event_prefix}.pending_change",
        sender_id: sender.id,
        pending_subscription_item_change_id: pending_subscription_item_change.id,
        subscription_item_id: subscription_item.id
    end

    def item_errors
      old_subscription_item_errors + new_subscription_item_errors
    end

    def old_subscription_item_errors
      return "" unless old_subscription_item
      old_subscription_item.errors.full_messages.join(", ")
    end

    def new_subscription_item_errors
      subscription_item.errors.full_messages.join(", ")
    end

    def is_downgrade?
      op = subscribable.listing.listable_is_sponsorable? ? :>= : :>
      subscription_item.price.send(op, plan_price) || cancelling_active?
    end

    def plan_price
      subscribable.base_price(duration: account.plan_duration) * quantity
    end

    def schedule_pending_change
      @pending_change_scheduled = true
      SchedulePlanChange.run \
        account: account,
        actor: sender,
        active_on: active_on,
        free_trial: start_free_trial,
        subscribable: subscribable,
        subscribable_quantity: quantity
    end

    def pending_subscription_item_change
      @pending_subscription_item_change ||= subscribable.pending_subscription_item_change(account: account)
    end

    def old_pending_subscription_item_change
      return unless old_subscription_item

      # TODO: the old subscribable may be pointing to the corresponding plan/tier
      # While we are decoupling sponsors from the Marketplace, there may be situations
      # where the a subscription item's subscribable is a SponsorsTier and the pending
      # subscription item change's subscribable is a Marketplace::Listing (or vice versa).
      #
      # We should be able to simplify this after we've decoupled.
      # See https://github.com/github/sponsors/issues/353
      corresponding_subscribable = if old_subscription_item.subscribable.is_a?(SponsorsTier)
        old_subscription_item.subscribable.listing_plan
      else
        SponsorsTier.find_by(listing_plan: old_subscription_item.subscribable)
      end

      account.pending_subscription_item_changes.find_by(subscribable: [old_subscription_item.subscribable, corresponding_subscribable].compact)
    end

    def update_pending_change
      return unless account.incomplete_pending_plan_changes.present? && pending_subscription_item_change
      if cancelling_trial?
        trial_pending_plan_change = pending_subscription_item_change.pending_plan_change
        Billing::SubscriptionItem.transaction do
          trial_pending_plan_change.destroy
          subscription_item.update(free_trial_ends_on: GitHub::Billing.today)
        end
      else
        pending_subscription_item_change.update_attribute :quantity, quantity
      end
    end

    def purchase_event_type
      quantity > 0 ? "changed" : "cancelled"
    end

    def event_prefix
      subscription_item.listable_is_sponsorable? ? "sponsorship" : "marketplace_purchase"
    end

    def cancelling_free_plan?
      cancelling? && !subscription_item.subscribable.paid?
    end

    def schedule_change?
      !force && (is_downgrade? || start_free_trial)
    end

    def subscription_item_changed?
      subscription_item.present? && old_subscription_item.present? && subscription_item != old_subscription_item
    end

    def cancelling_trial?
       cancelling? && subscription_item.on_free_trial?
    end

    def cancelling_active?
      cancelling? && !subscription_item.on_free_trial?
    end

    def cancelling?
      quantity == 0
    end

    def active_on
      if start_free_trial && subscription_item.on_free_trial?
        subscription_item.free_trial_ends_on + 1.day
      end
    end

    def instrument_change
      if cancelling? || previous_subscribable != subscribable
        instrument_change_for_sponsors if subscription_item.listable_is_sponsorable?

        GlobalInstrumenter.instrument(
          "billing.subscription_item_change",
          actor_id: sender&.id,
          user_id: account&.id,
          old_subscribable: previous_subscribable,
          old_quantity: previous_quantity,
          new_subscribable: subscribable,
          new_quantity: quantity,
        )
      elsif previous_quantity != quantity
        GlobalInstrumenter.instrument(
          "billing.subscription_item_quantity_change",
          actor_id: sender&.id,
          user_id: account&.id,
          subscribable: subscribable,
          old_quantity: previous_quantity,
          new_quantity: quantity,
        )
      end

      if end_free_trial
        GlobalInstrumenter.instrument(
          "billing.free_trial_conversion",
          actor_id: sender&.id,
          user_id: account&.id,
          listing_plan_id: subscribable&.id,
          quantity: quantity,
        )
      end
    end

    def instrument_change_for_sponsors
      return unless sponsorship = subscription_item.sponsorship
      return unless listing = subscription_item.subscribable.sponsors_listing

      if @pending_change_scheduled
        # A sponsorship downgrade or cancellation was scheduled for the
        # beginning of the sponsor's next billing cycle.
        GlobalInstrumenter.instrument("sponsors.sponsor_sponsorship_pending_change", {
          actor: sender,
          sponsorship: sponsorship,
          listing: listing,
          old_tier: previous_subscribable,
          new_tier: cancelling? ? nil : subscribable,
          action: :CREATE,
          goal: listing.active_goal,
        })

        if cancelling?
          GitHub.instrument("sponsors.sponsor_sponsorship_pending_cancellation", {
            user: sponsorship.sponsorable,
            actor: sender,
            sponsorship: sponsorship,
            current_tier_id: sponsorship.tier.id,
            pending_change_on: pending_subscription_item_change&.active_on,
          })
        else
          GitHub.instrument("sponsors.sponsor_sponsorship_pending_tier_change", {
            user: sponsorship.sponsorable,
            actor: sender,
            sponsorship: sponsorship,
            current_tier_id: sponsorship.tier.id,
            pending_change_tier_id: pending_subscription_item_change&.subscribable&.id,
            pending_change_on: pending_subscription_item_change&.active_on,
          })
        end
      elsif cancelling?
        # The sponsorship was either force-cancelled or cancelled via a previously
        # scheduled pending change.
        GlobalInstrumenter.instrument("sponsors.sponsor_sponsorship_cancel", {
          actor: sender,
          sponsorship: sponsorship,
          listing: listing,
          tier: previous_subscribable,
          action: :CANCEL,
          goal: listing.active_goal,
        })
        GitHub.instrument("sponsors.sponsor_sponsorship_cancel", {
          actor: sender,
          user: sponsorship.sponsorable,
          sponsorship: sponsorship,
          current_tier_id: previous_subscribable.id,
        })
      else
        # The sponsorship was either upgraded via a successful prorated transaction
        # or downgraded via a previously scheduled pending change.
        GlobalInstrumenter.instrument("sponsors.sponsor_sponsorship_tier_change", {
          actor: sender,
          sponsorship: sponsorship,
          listing: listing,
          current_tier: subscribable,
          previous_tier: previous_subscribable,
          goal: listing.active_goal,
        })
        GitHub.instrument("sponsors.sponsor_sponsorship_tier_change", {
          user: listing.sponsorable,
          actor: account,
          sponsorship: sponsorship,
          current_tier_id: subscribable.id,
          previous_tier_id: previous_subscribable.id,
        })
      end
    end

    def update_sponsorship
      return unless subscription_item.listable_is_sponsorable?
      return if @pending_change_scheduled

      sponsor = account
      sponsorable = subscription_item.listing.sponsorable
      sponsorship = Sponsorship.find_by(sponsor: sponsor, sponsorable: sponsorable)

      return if sponsorship.nil?

      subscription_item_to_sync = if subscription_item.errors.empty?
        subscription_item
      else
        old_subscription_item
      end

      if subscription_item_to_sync
        sponsorship.update!(
          subscription_item: subscription_item_to_sync,
          active: subscription_item_to_sync.quantity > 0,
          subscribable: subscription_item_to_sync.subscribable,
        )
      end
    end
  end
end
