# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class ChangeSubscription
    # Public: Change subscription attributes for a subscription account.
    #
    # Must have an existing Customer record if updating the payment method.
    #
    # target          - A User or Organization changing their subscription
    # plan            - String of new plan name (optional)
    # actor           - User performing this action
    # plan_duration   - String duration either "month" or "year". (optional)
    #                   Defaults to existing plan duration
    # seats           - Integer number of seats for this plan. (optional)
    #                   Defaults to existing number of seats
    # payment_details - A Hash of payment details. (optional)
    #   :billing_extra    - String extra billing information.
    #   :paypal_nonce     - String nonce representing a paypal account (optional).
    #   :credit_card      - A Hash of potentially encrypted CC info, including:
    #     * :number           - CC number as a String.
    #     * :expiration_month - Expiration month as a String of form MM
    #     * :expiration_year  - Expiration year as a String of form YY
    #     * :cvv              - CVV as a String (e.g. "420")
    #   :billing_address  - The billing address as a Hash of optionally encrypted CC info
    #     * :country_name    - Country as a String
    #     * :region          - Region as a String
    #     * :postal_code     - Postal code as a String
    #
    # Returns a GitHub::Billing::Result
    def self.perform(*args, **options)
      new(*args, **options).perform
    end

    # Public: Checks to see if the target can make the provided changes.
    def self.can_perform?(target, args)
      new(target, **args).can_perform?
    end

    attr_accessor :target, :plan, :actor, :plan_duration, :previous_plan_duration, :seats, :seat_delta, :payment_details, :result

    def initialize(target, actor:, plan: nil, seats: nil, seat_delta: nil, plan_duration: nil, payment_details: nil)
      @target = target
      @actor = actor

      @plan = find_plan(plan || target.plan.try(:name))
      @seats = seats || (@plan.per_seat? ? target.seats : 0)
      @seat_delta = seat_delta
      @previous_plan_duration = target.plan_duration
      @plan_duration = plan_duration || target.plan_duration
      @payment_details = payment_details || {}
    end

    # Returns a GitHub::Billing::Result
    def perform
      if !target.valid?
        # Be bold if an invalid user gets passed in
        return GitHub::Billing::Result.failure target.errors.full_messages.join(", ")
      elsif !can_perform?
        return result
      elsif changing_pricing_model?
        return change_pricing_model
      elsif seat_delta && !can_change_seat_amount?
        return result
      end

      if is_downgrade?
        return Billing::SchedulePlanChange.run \
          account: target,
          actor: actor,
          seats: seats,
          plan: plan,
          plan_duration: plan_duration
      end

      if (target.spammy? || actor.spammy?) && !is_downgrade?
        return GitHub::Billing::Result.failure "Your account is flagged and unable to make purchases."
      end

      if target.needs_valid_payment_method_to_switch_to_plan?(plan, seats) && !has_payment_details?
        return GitHub::Billing::Result.failure "A credit card or other payment method is required to upgrade to that plan."
      elsif has_payment_details?
        result = create_or_update_payment_method
        return result if result.failed?
      end

      target.seats = seats
      target.plan_duration = plan_duration
      target.plan = plan

      # We need to update the pending plan change with any changes to the target's subscription
      if target.pending_cycle_change
        target.pending_cycle_change.seats = seats if seats
        target.pending_cycle_change.plan_duration = plan_duration if plan_duration
        target.pending_cycle_change.plan = plan if plan
        target.pending_cycle_change.save
      end

      self.plan_changed, self.plan_was = target.plan_changed?, target.plan_was
      self.seats_changed, self.seats_was = target.seats_changed?, target.seats_was

      if target.apple_iap_subscription?
        self.subscription_provider_was = "Apple In-App Purchase"
      end

      target.save
      target.enable!

      # create external subscription if changing plans from Free
      unless target.external_subscription?
        GitHub::Billing.transition_to_external_subscription(target)
      end

      track_changes

      GitHub::Billing::Result.success
    end

    # Public: Checks to see if the user can change to a particular plan by an actor.
    #         If no actor is defined, it assumes the user itself.
    #
    # Returns a Boolean
    def can_perform?
      return can_change_to_pro?        if plan.pro?
      return can_change_to_business?   if plan.business? || plan.business_plus?
      return can_change_to_repository? if plan.per_repository?
      return can_change_to_free?       if free_plan?
      true
    end

    private

    attr_accessor :plan_changed, :plan_was, :seats_changed, :seats_was, :subscription_provider_was

    def can_change_to_pro?
      if target.user? && !target.plan.pro? || can_change_plan_duration?
        true
      elsif target.user? && target.plan == plan
        set_failure!("Already on the Personal plan.")
        false
      else
        set_failure!("Cannot move to a Personal plan.")
        false
      end
    end

    def can_change_to_business?
      if target.organization? && (!target.invoiced? || actor.site_admin? || can_change_plan_duration?)
        true
      else
        set_failure!("Please contact support if you want to migrate to the per-seat pricing plan.")
        false
      end
    end

    def can_change_to_repository?
      upgrade_restriction = Billing::PlanChange::LegacyUpgradeRestriction.new(target: target, actor: actor)
      already_per_repo = target.plan.per_repository?
      # NB: github staff are allowed to move orgs to per_repo plans
      #     that the org's admins wouldn't normally have access to
      staff_admining_an_org = actor.site_admin? && target.organization?

      if !staff_admining_an_org && (!already_per_repo || !upgrade_restriction.allow_plan?(plan))
        verb = already_per_repo ? "upgrade" : "move to"
        set_failure!("Cannot #{verb} a per-repository plan.")
        false
      elsif target.owned_private_repositories.size > plan.limit(:repos, visibility: :private)
        set_failure!("Too many repositories to change to the #{plan.name.capitalize} plan.")
        false
      else
        true
      end
    end

    def can_change_plan_duration?
      changing_plan_duration? &&
        ((plan == target.plan) ||
         (plan.per_repository? == target.plan.per_repository?))
    end

    def can_change_to_free?
      if target.invoiced? && !actor.site_admin?
        set_failure!("Your account is being invoiced. Please contact support to make plan changes.")
        return false
      elsif target.over_repo_seat_limit? && !actor.site_admin?
        set_failure!("Your account can not be downgraded yet because one or more of your private repositories is over the collaborator limit for the free plan. Please make sure that each of the private repositories owned by your account (#{target.login}) below has #{GitHub::Plan.free.limit(:collaborators, visibility: :private)} or fewer collaborators before downgrading your account. Questions?")
        return false
      end
      true
    end

    MAX_SEAT_DELTA = 300

    def can_change_seat_amount?
      if seat_delta == 0
        set_failure!("You must add or remove at least 1 seat.")
        false
      elsif !seat_delta.between?(MAX_SEAT_DELTA * -1, MAX_SEAT_DELTA)
        set_failure!("You can only add or remove up to #{MAX_SEAT_DELTA} seats at a time.")
        false
      else
        true
      end
    end

    # Private: Returns whether the plan duration is being changed.
    #
    # Returns a boolean
    def changing_plan_duration?
      plan_duration != previous_plan_duration
    end

    # Private: Sets the error result with the given message.
    #
    # msg - A String message to return
    #
    # Returns a GitHub::Billing::Result failure
    def set_failure!(msg)
      @result = GitHub::Billing::Result.failure msg
    end

    # Private: Track changes in the horribly named Transactions table and
    #          the Audit Log.
    #
    # Returns nothing
    def track_changes
      if plan_changed
        target.track_plan_change(actor, plan_was, old_subscription_provider: subscription_provider_was)
      end

      if seats_changed
        target.track_seat_change(actor, old_seats: seats_was)
      end
    end

    # Private: Enables or disables the account and individual repos, as applicable
    #
    # Returns nothing
    def enable_or_disable_account
      target.enable_or_disable!
    end

    def create_or_update_payment_method
      if target.has_billing_record?
        GitHub::Billing.update_payment_method(target, payment_details.merge(charge: false))
      else
        GitHub::Billing.create_customer(target, payment_details)
      end
    end

    def has_payment_details?
      GitHub::Billing::PaymentDetails.new(payment_details).valid?
    end

    def find_plan(plan)
      new_plan = GitHub::Plan.find!(plan)
      new_plan = MunichPlan.wrap(new_plan, target: target)

      if (target.data_packs > 0 || target.subscription_items.any?) && new_plan.free?
        plan = GitHub::Plan.find!("free_with_addons")
        MunichPlan.wrap(plan, target: target)
      else
        new_plan
      end
    end

    def changing_pricing_model?
      !free_plan? && (target.plan.per_seat? ^ plan.per_seat?)
    end

    def change_pricing_model
      pricing_model = Billing::PlanChange::PerSeatPricingModel.new(target, new_plan: plan)
      success = pricing_model.switch(actor: actor)
      enable_or_disable_account
      GitHub::Billing::Result.new(success, nil, nil)
    end

    def is_downgrade?
      return false if target.invoiced?
      downgrading_seats? || delayed_duration_change? || downgrading_plan?
    end

    def delayed_duration_change?
      changing_plan_duration? && target.payment_amount > 0
    end

    def downgrading_seats?
      plan.per_seat? && target.seats > seats
    end

    def downgrading_plan?
      changing_plan? && target.undiscounted_payment_difference(plan, seat_count: seats) < 0
    end

    def changing_plan?
      target.plan != plan
    end

    def free_plan?
      plan.free? || plan.free_with_addons?
    end
  end
end
