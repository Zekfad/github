# frozen_string_literal: true

module Billing
  module StripeConnect
    class Account::Oauth
      class AuthenticationError < StandardError; end
      class RequestExpiredError < StandardError; end

      extend ActiveSupport::SecurityUtils

      REQUEST_TIMEOUT_MINUTES = 20
      BENEFICIARY_TRANSFERS_CAPABILITY = "beneficiary_transfers"
      TRANSFERS_CAPABILITY = "transfers"

      class << self
        def authorization_url(request_identifier:, domain:, sponsorable:)
          business_type = if sponsorable.organization?
            "company"
          else
            "individual"
          end

          params = {
            "suggested_capabilities[]": capabilities_for(sponsorable),
            "stripe_user[business_type]": business_type,
            state: request_identifier,
            redirect_uri: "#{domain}/oauth/stripe_connect",
          }
          opts = { express: true }
          ::Stripe::OAuth.authorize_url(params, opts)
        end

        def request_access_token(authorization_code:)
          ::Stripe::OAuth.token(
            client_secret: ::Stripe.api_key,
            code: authorization_code,
            grant_type: "authorization_code",
          )
        end

        def generate_request_state(sponsorable_id)
          current_time_string = Time.now.to_i.to_s
          hmac = generate_hmac(current_time_string)
          "#{hmac}:#{current_time_string}:#{sponsorable_id}"
        end

        def validate_request_state(state)
          hmac, timestamp, sponsorable_id = state.split(":")
          compare = generate_hmac(timestamp)

          unless secure_compare(hmac, compare)
            raise AuthenticationError
          end

          unless (Time.now.to_i - timestamp.to_i) < REQUEST_TIMEOUT_MINUTES.minutes.to_i
            raise RequestExpiredError
          end

          sponsorable_id
        end

        private

        def generate_hmac(timestamp)
          OpenSSL::HMAC.hexdigest("sha256", GitHub.stripe_request_identity_secret, timestamp)
        end

        # Private: The capabilities that this user should have for Stripe Connect.
        #
        # sponsorable - The User that is creating the Stripe Connect account.
        #
        # Returns a String.
        def capabilities_for(sponsorable)
          if GitHub.flipper[:sponsors_force_beneficiary_transfers].enabled?(sponsorable)
            return BENEFICIARY_TRANSFERS_CAPABILITY
          end

          if (membership = sponsorable&.sponsors_membership)
            if Account::CROSS_BORDER_PAYOUT_COUNTRIES.include?(membership.billing_country)
              return BENEFICIARY_TRANSFERS_CAPABILITY
            elsif Account.supported_countries_for(sponsorable: sponsorable).include?(membership.country_of_residence)
              return TRANSFERS_CAPABILITY
            elsif Account.supported_countries_for(sponsorable: sponsorable).include?(membership.billing_country)
              return BENEFICIARY_TRANSFERS_CAPABILITY
            end
          end

          TRANSFERS_CAPABILITY
        end
      end
    end
  end
end
