# frozen_string_literal: true

class Billing::StripeConnect::Account < ApplicationRecord::Collab
  self.table_name = "stripe_connect_accounts"

  areas_of_responsibility :gitcoin

  belongs_to :payable, polymorphic: true
  has_many :ledger_entries, class_name: "Billing::PayoutsLedgerEntry", foreign_key: :stripe_connect_account_id
  has_many :webhooks, foreign_key: :account_id, primary_key: :stripe_account_id, class_name: "Billing::StripeWebhook"

  validates :stripe_account_id, :payable, presence: true
  # TODO: remove `on: :create` once all accounts have been migrated
  # see https://github.com/github/sponsors/issues/1431
  validates :stripe_account_id, uniqueness: true, on: :create
  validates :payable_type, length: { maximum: 32 }

  store :stripe_account_details, coder: JSON

  after_create_commit :set_metadata

  PAYOUT_FAILURE_REASONS = %w(account_closed account_frozen
    bank_account_restricted bank_ownership_changed could_not_process
    debit_not_authorized declined invalid_account_number
    incorrect_account_holder_name no_account unsupported_card
  ).freeze

  LOCAL_PAYOUT_COUNTRIES = ["AT", "AU", "BE", "CA", "CH", "DE", "DK", "EE", "ES",
    "FI", "FR", "GB", "GR", "HK", "IE", "IT", "JP", "LT", "LU", "LV", "NL", "NO",
    "NZ", "PL", "PT", "SE", "SI", "SK", "US"].freeze

  CROSS_BORDER_PAYOUT_COUNTRIES = ["CZ", "IN", "MX", "SG", "MT", "CY"].freeze

  SUPPORTED_COUNTRIES = (LOCAL_PAYOUT_COUNTRIES + CROSS_BORDER_PAYOUT_COUNTRIES).freeze

  SUPPORTED_USER_COUNTRIES = SUPPORTED_COUNTRIES
  SUPPORTED_ORG_COUNTRIES = (SUPPORTED_COUNTRIES - ["IN"]).freeze

  def self.supported_countries_for(sponsorable:)
    if sponsorable&.organization?
      SUPPORTED_ORG_COUNTRIES
    else
      SUPPORTED_USER_COUNTRIES
    end
  end

  def async_belongs_to?(user)
    async_payable.then do |payable|
      owner_promise = if payable.is_a?(Marketplace::Listing)
        payable.async_listable
      else
        payable.async_sponsorable
      end

      owner_promise.then do |owner|
        owner.id == user.id || owner.adminable_by?(user)
      end
    end
  end

  def belongs_to?(user)
    async_belongs_to?(user).sync
  end

  def sponsored_maintainer
    if payable.is_a?(Marketplace::Listing) && payable.listable_is_sponsorable?
      payable.listable
    elsif payable.is_a?(SponsorsListing)
      payable.sponsorable
    end
  end

  def total_match_in_cents
    async_total_match_in_cents.sync
  end

  def async_total_match_in_cents
    ledger_matches = Billing::PayoutsLedgerEntry
      .github_net_match_transactions_for(self.id)

    Promise.resolve(ledger_matches.map(&:amount_in_subunits).sum * -1)
  end

  def stripe_dashboard_url
    base_url = GitHub.stripe_connect_dashboard_base_url
    "#{base_url}/connect/accounts/#{stripe_account_id}"
  end

  def billing_country
    return if stripe_account_details.empty?
    return unless country = stripe_account_from_stripe_account_details.external_accounts.first&.country
    Braintree::Address::CountryNames.find do |(_, alpha2, _, _)|
      alpha2.downcase == country.downcase
    end.first
  end

  # Public: Is this Stripe Connect account verified?
  #
  # Returns a Boolean.
  def verified?
    return false unless stripe_account_details.present?
    individual = stripe_account_details["individual"]

    if individual && individual["verification"]["status"] != "verified"
      return false
    end

    stripe_account_details["requirements"]["eventually_due"].empty?
  end

  # Public: Are automated payouts disabled for this account?
  #
  # Returns a Boolean.
  def automated_payouts_disabled?
    return false unless stripe_account_details.present?
    interval = stripe_account_details["settings"]["payouts"]["schedule"]["interval"]

    interval == "manual"
  end

  # Note: This pattern should not be replicated. If you need to
  # query Stripe data, either use the API or check out
  # https://github.com/github/gitcoin/issues/3829 and talk
  # with @gitcoin
  def latest_payout
    payout_webhooks = webhooks.where(kind: [:payout_created, :payout_failed])
    return unless latest_payout_webhook = payout_webhooks.last
    event = Stripe::Event.construct_from(latest_payout_webhook.payload)
    event.data.object
  end

  # Public: The transfers to this Stripe connect account
  #
  # Returns an Array of ::Billing::Stripe::Transfer
  def stripe_transfers(starting_after = nil, ending_before = nil, limit: Billing::Stripe::Transfer::LIMIT)
    ::Billing::Stripe::Transfer.list(
      destination: stripe_account_id,
      destination_currency: default_currency,
      starting_after: starting_after,
      ending_before: ending_before,
      limit: limit,
    )
  end

  # Public: The default currency of this Stripe connect account
  #
  # Returns String currency code
  def default_currency
    stripe_account_from_stripe_account_details&.default_currency
  end

  # Public: The current balance for this Stripe Connect account.
  #
  # Returns a Billing::StripeConnect::Account::APIResult, where the result
  # is a Stripe::Balance.
  def current_balance
    balance = ::Stripe::Balance.retrieve(stripe_account: stripe_account_id)

    APIResult.success(
      account: self,
      result: balance,
    )
  rescue ::Stripe::OAuth::InvalidRequestError,
         ::Stripe::APIConnectionError,
         ::Stripe::StripeError => e
    Failbot.report(e, app: "github-external-request")
    APIResult.failure(
      account: self,
      error: e,
    )
  end

  # Public: The payouts that this account has received from Stripe.
  #
  # limit - An Integer limit of results to return.
  #
  # Returns a Billing::StripeConnect::Account::APIResult, where the result
  # is an Array[Stripe::Payout].
  def stripe_payouts(limit: 100)
    result = Stripe::Payout.list(
      { limit: limit, expand: ["data.destination"] },
      { stripe_account: stripe_account_id }
    ).data

    APIResult.success(
      account: self,
      result: result,
    )
  rescue ::Stripe::OAuth::InvalidRequestError,
         ::Stripe::APIConnectionError,
         ::Stripe::StripeError => e
    Failbot.report(e, app: "github-external-request")
    APIResult.failure(
      account: self,
      error: e,
    )
  end

  private

  def set_metadata
    SetStripeConnectAccountMetadataJob.perform_later(self)
  end

  def stripe_account_from_stripe_account_details
    return if stripe_account_details.empty?
    Stripe::Account.construct_from(stripe_account_details)
  end
end
