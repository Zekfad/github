# rubocop:disable Style/FrozenStringLiteralComment

# Wrapper class for Braintree subscriptions
#
# DEPRECATION WARNING:
# This class is deprecated with the transition to Zuora. It should not be used
# for new integrations and may be removed in the near future.
class Billing::BraintreeSubscription
  delegate :add_ons,
           :days_past_due,
           :discounts,
           :failure_count,
           :next_billing_period_amount,
           :payment_method_token,
           :plan_id,
           :price,
           :status,
    to: :raw_subscription

  # Find the remote Braintree::Subscription object.
  # NB: This makes a remote call to Braintree.
  #
  # braintree_id - String id of the braintree subscription
  #
  # Returns a Braintree::Subscription or nil
  def self.find(braintree_id)
    new(braintree_id)
  end

  # Retries a charge on a Past Due Braintree::Subscription.
  # SubscriptionChargedSuccessfully webhook fires on success of retry_charge.
  # However, a webhook is NOT fired on failure of retry_charge. Also, this
  # returns an error if subscription is not past due.
  #
  # Returns a Braintree::ErrorResult or Braintree::SuccessfulResult
  def self.retry_charge(plan_subscription)
    retry_result = Braintree::Subscription.retry_charge \
      plan_subscription.braintree_id
    if retry_result.success?
      Braintree::Transaction.submit_for_settlement(retry_result.transaction.id)
    end
    retry_result
  end

  def plan
    GitHub::Plan.find plan_id.gsub(/_yearly/, "")
  end

  def plan_duration
    plan_id.ends_with?("yearly") ? User::YEARLY_PLAN : User::MONTHLY_PLAN
  end

  def first_billing_date
    raw_subscription.first_billing_date.to_date
  end

  def next_billing_date
    raw_subscription.next_billing_date.to_date
  end

  def seats
    add_on_quantity(Billing::PlanSubscription::BraintreeParams::PER_SEAT_ADDON_ID)
  end

  def data_packs
    add_on_quantity(Billing::PlanSubscription::BraintreeParams::DATA_PACK_ADDON_ID)
  end

  def subscription_item_addons
    add_ons.select do |add_on|
      add_on.id.starts_with? \
        Marketplace::Listing::BRAINTREE_ADDON_PREFIX
    end
  end

  # Public: The sum of all discounts on the wrapped BraintreeSubscription
  #
  # Returns BigDecimal discount
  def discount
    discounts.map(&:amount).reduce(0, :+).to_d
  end

  def cost
    price
  end

  def balance
    raw_subscription.balance || 0
  end

  def canceled?
    status == "Canceled"
  end

  def past_due?
    status == "Past Due"
  end

  def active?
    status == "Active"
  end

  def pending?
    status == "Pending"
  end

  def dashboard_url
    "#{GitHub.braintree_host}/merchants/%s/subscriptions/%s" %
      [GitHub.braintree_merchant_id, raw_subscription.id]
  end

  private

  attr_reader :raw_subscription

  def initialize(braintree_id)
    @raw_subscription = Braintree::Subscription.find(braintree_id)
  end

  def add_on_quantity(id)
    add_ons.find { |add_on| add_on.id == id }.try(:quantity).to_i
  end
end
