# frozen_string_literal: true

class Billing::UsageNotifications
  include ActionView::Helpers::NumberHelper
  INFO_THRESHOLD = 75
  WARN_THRESHOLD = 90
  LEVEL_INFO = "info"
  LEVEL_WARN = "warn"
  LEVEL_ERROR = "error"

  ACTIONS_PRODUCT = "actions"
  GPR_PRODUCT = "gpr"
  SHARED_STORAGE_PRODUCT = "storage"

  def initialize(owner, product: nil)
    @owner = owner
    @product = product
    @package_registry_usage = Billing::PackageRegistryUsage.new(owner)
    @shared_storage_usage = Billing::SharedStorageUsage.new(owner)
    @actions_usage = Billing::ActionsUsage.new(owner)
    @metered_billing_permission = Billing::MeteredBillingPermission.new(owner)
  end

  def notify_if_applicable(ignore_paid_thresholds: false)
    return if @owner.plan.legacy?
    return if email_sent?

    if ignore_paid_thresholds
      return if current_level == LEVEL_INFO && info_paid_resources_threshold?
      return if current_level == LEVEL_WARN && warn_paid_resources_threshold?
      return if current_level == LEVEL_ERROR && error_paid_resources_threshold?
    end

    BillingNotificationsMailer.process_resource_usage_level(current_level, @owner, @product).deliver_later

    mark_email_sent
    instrument_send unless current_level.nil?
    remove_non_current_email_sent_keys
    send_invoiced_slack_notification
  end

  def send_invoiced_slack_notification
    return unless @owner.invoiced? && current_level.present?

    Billing::InvoicedMeteredBillingSlackNotifier.new(
      owner: @owner,
      paid: false,
      usage_message: slack_notification_message
    ).send_notification
  end

  def slack_notification_message
    base = if current_level == LEVEL_ERROR && error_free_resources_threshold?
      "used 100% of included services"
    elsif current_level == LEVEL_WARN && warn_free_resources_threshold?
      "used 90% of included services"
    elsif current_level == LEVEL_INFO && info_free_resources_threshold?
      "used 75% of included services"
    else
      "used less than 75% of included services"
    end

    base + "\n#{ACTIONS_PRODUCT.capitalize}: #{free_minutes_usage_percentage}% used\n#{GPR_PRODUCT.upcase}: #{free_data_usage_percentage}% used\n#{SHARED_STORAGE_PRODUCT.capitalize}: #{free_storage_usage_percentage}% used"
  end

  ICON_MAPPING = {
    ACTIONS_PRODUCT => "actions.png",
    GPR_PRODUCT => "packages.png",
    SHARED_STORAGE_PRODUCT => "hosting.png"
  }

  TITLE_MAPPING = {
    ACTIONS_PRODUCT => "GitHub Actions usage",
    GPR_PRODUCT => "GitHub Packages usage",
    SHARED_STORAGE_PRODUCT => "Shared storage usage"
  }

  PROGRESS_BAR_TITLE_MAPPING = {
    ACTIONS_PRODUCT => "Private repository usage",
    GPR_PRODUCT => "Data transfer out",
    SHARED_STORAGE_PRODUCT => "Storage used"
  }

  def mail_icon
    ICON_MAPPING[@product] || "cogs.png"
  end

  def mail_product_title
    TITLE_MAPPING[@product] || "Spending limit usage"
  end

  def show_spending_limit_progress_bar?
    return true if current_level == LEVEL_INFO && info_paid_resources_threshold?
    return true if current_level == LEVEL_WARN && warn_paid_resources_threshold?
    return true if current_level == LEVEL_ERROR && error_paid_resources_threshold?

    false
  end

  def progress_bar_title
    return "Spending limit" if show_spending_limit_progress_bar?

    PROGRESS_BAR_TITLE_MAPPING[@product]
  end

  def progress_bar_details_text
    return paid_progress_bar_text if show_spending_limit_progress_bar?

    return actions_progress_bar_text if @product == ACTIONS_PRODUCT
    return packages_progress_bar_text if @product == GPR_PRODUCT
    return storage_progress_bar_text if @product == SHARED_STORAGE_PRODUCT
  end

  def usage_reset_date_text
    "Your usage will reset on #{@owner.first_day_in_next_metered_cycle.to_s(:long)}"
  end

  def info_resources_threshold?
    return true if info_paid_resources_threshold?
    info_free_resources_threshold?
  end

  def info_resources_mail_subject
    return "You've hit 75% of your spending limit" if info_paid_resources_threshold?
    return "You've used 75% of included services" if info_free_resources_threshold?
  end

  def warn_resources_mail_subject
    return "You've hit 90% of your spending limit" if warn_paid_resources_threshold?
    return "You've used 90% of included services" if warn_free_resources_threshold?
  end

  def error_resources_mail_subject
    return "You've hit 100% of your spending limit" if error_paid_resources_threshold?
    return "You've used 100% of included services" if error_free_resources_threshold?
  end

  def info_resources_threshold_text
    return info_paid_resources_threshold_text if info_paid_resources_threshold?
    return info_free_resources_threshold_text if info_free_resources_threshold?
  end

  def error_resources_threshold?
    return true if error_paid_resources_threshold?
    error_free_resources_threshold?
  end

  def warn_resources_threshold?
    return true if warn_paid_resources_threshold?
    warn_free_resources_threshold?
  end

  def warn_resources_threshold_text
    return warn_paid_resources_threshold_text if warn_paid_resources_threshold?
    return warn_free_resources_threshold_text if warn_free_resources_threshold?
  end

  def disabled_services
    return "GitHub Actions and Packages" if error_paid_resources_threshold?
    return "GitHub #{disabled_free_services.to_sentence}" if error_free_resources_threshold?
  end

  def error_resources_threshold_text
    return error_paid_resources_threshold_text if error_paid_resources_threshold?
    return error_free_resources_threshold_text if error_free_resources_threshold?
  end

  def info_free_resources_threshold?
    # Do not show warnings if user has overage allowed in their metered billing settings.
    return false if overages_enabled_for_product?
    free_data_usage_info? || free_minutes_usage_info? || free_storage_usage_info?
  end

  def info_free_resources_threshold_text
    "You've used 75% of included services for GitHub #{info_threshould_free_resources.to_sentence}"
  end

  def warn_free_resources_threshold?
    # Do not show warnings if user has overage allowed in their metered billing settings.
    return false if overages_enabled_for_product?
    free_data_usage_warn? || free_minutes_usage_warn? || free_storage_usage_warn?
  end

  def warn_free_resources_threshold_text
    "You've used 90% of included services for GitHub #{warn_threshould_free_resources.to_sentence}"
  end

  def error_free_resources_threshold?
    # Do not show errors if user has overage allowed in their metered billing settings.
    return false if overages_enabled_for_product?
    free_data_usage_error? || free_minutes_usage_error? || free_storage_usage_error?
  end

  def error_free_resources_threshold_text
    "You've used 100% of included services for GitHub #{error_threshould_free_resources.to_sentence}"
  end

  def info_paid_resources_threshold?
    # Do not show paid errors if user has not allowed overages in their metered billing settings
    return false unless overages_enabled_for_product?
    paid_resources_usage_info?
  end

  def info_paid_resources_threshold_text
    "You've used 75% of your spending limit for GitHub Actions and Packages"
  end

  def warn_paid_resources_threshold?
    # Do not show paid errors if user has not allowed overages in their metered billing settings
    return false unless overages_enabled_for_product?
    paid_resources_usage_warn?
  end

  def warn_paid_resources_threshold_text
    "You've used 90% of your spending limit for GitHub Actions and Packages"
  end

  def error_paid_resources_threshold?
    # Do not show paid errors if user has not allowed overages in their metered billing settings
    return false unless overages_enabled_for_product?
    paid_resources_usage_error?
  end

  def error_paid_resources_threshold_text
    "You've hit 100% of your spending limit for GitHub Actions and Packages"
  end

  def email_sent?
    return true unless emails_enabled?
    # If there is an issue retrieving the key assume email has been sent
    # as to not spam users with emails when KV is having issues.
    GitHub.kv.exists(email_sent_at_key).value { true }
  end

  def mark_email_sent
    ActiveRecord::Base.connected_to(role: :writing) do
      GitHub.kv.set(email_sent_at_key, Time.now.to_s, expires: 60.days.from_now)
    end
  end

  def instrument_send(level: current_level)
    payload = {
      formatted_class_name => @owner,
      :product => email_sent_at_type,
      :threshold_level => level,
      :first_day_in_metered_cycle => @owner.first_day_in_metered_cycle,
      :paid_threshold => paid_resources_threshold?
    }

    GitHub.instrument("billing.metered_usage_email_sent", payload)
  end

  def emails_enabled?
    GitHub.flipper[:metered_billing_email_notifications].enabled?(@owner)
  end

  # spending-limit-error-user-397-2019-10-01-1
  # actions-info-business-408-2019-10-01
  # WARNING: changing the parts of the key in this method
  # would mean that users may receive notifications as this would essentially
  # bust the email key used to prevent duplicates.
  def email_sent_at_key(level: current_level)
    budget = paid_resources_threshold? ?  @owner.budget_for(product: @product) : nil
    [
      email_sent_at_type,
      level,
      @owner.class.to_s.downcase,
      @owner.id,
      @owner.first_day_in_metered_cycle,
      budget&.product,
      budget&.effective_spending_limit_in_subunits
    ].compact.join("-")
  end

  def email_sent_at_type
    if @product.nil?
      raise "Must set product for email notifications"
    end

    if paid_resources_threshold?
      "spending-limit"
    else
      @product
    end
  end

  def paid_resources_threshold?
    return false unless overages_enabled_for_product?

    info_paid_resources_threshold? || warn_paid_resources_threshold? || error_paid_resources_threshold?
  end

  def include_storage?
    @product == SHARED_STORAGE_PRODUCT || @product.nil?
  end

  def include_actions?
    @product == ACTIONS_PRODUCT || @product.nil?
  end

  def include_gpr?
    @product == GPR_PRODUCT || @product.nil?
  end

  def current_level
    return LEVEL_ERROR if error_resources_threshold?
    return LEVEL_WARN if warn_resources_threshold?
    return LEVEL_INFO if info_resources_threshold?
  end

  private

  def overages_enabled_for_product?
    @owner.metered_billing_overage_allowed?(product: @product)
  end

  def paid_progress_bar_text
    "#{number_to_currency(@metered_billing_permission.total_usage / 100.0, strip_insignificant_zeros: true)} of #{number_to_currency(@metered_billing_permission.usage_limit / 100.0, strip_insignificant_zeros: true)}"
  end

  def actions_progress_bar_text
    "#{number_with_delimiter(@actions_usage.total_private_minutes_used.to_i)} of #{number_with_delimiter(@actions_usage.included_private_minutes)} mins included"
  end

  def packages_progress_bar_text
    "#{number_with_delimiter(@package_registry_usage.total_gigabytes_used)}GB of #{number_with_delimiter(@package_registry_usage.plan_included_bandwidth)}GB included"
  end

  def storage_progress_bar_text
    "#{number_with_delimiter(@shared_storage_usage.estimated_monthly_private_megabytes.to_i)}MB of #{number_with_delimiter(@shared_storage_usage.plan_included_megabytes)}MB included"
  end

  def info_threshould_free_resources
    resources = []
    resources << "Actions"  if free_minutes_usage_info?
    resources << "Packages" if free_data_usage_info?
    resources << "Storage (GitHub Actions and Packages)" if free_storage_usage_info?

    resources
  end

  def warn_threshould_free_resources
    resources = []
    resources << "Actions"  if free_minutes_usage_warn?
    resources << "Packages" if free_data_usage_warn?
    resources << "Storage (GitHub Actions and Packages)" if free_storage_usage_warn?

    resources
  end

  def disabled_free_services
    disabled_services = []
    disabled_services << "Actions"   if (free_storage_usage_error? || free_minutes_usage_error?)
    disabled_services << "Packages"  if (free_storage_usage_error? || free_data_usage_error?)
    disabled_services
  end

  def error_threshould_free_resources
    resources = []
    resources << "Actions"  if free_minutes_usage_error?
    resources << "Packages" if free_data_usage_error?
    resources << "Storage (GitHub Actions and Packages)" if free_storage_usage_error?
    resources
  end

  def free_data_usage_info?
    # GitHub Packages allows for unlimited usage
    # for packages in public repositories.
    # We will only account for private bandwidth usage here.
   free_data_usage_percentage >= INFO_THRESHOLD && include_gpr?
  end

  def free_data_usage_warn?
    # GitHub Packages allows for unlimited usage
    # for packages in public repositories.
    # We will only account for private bandwidth usage here.
   free_data_usage_percentage >= WARN_THRESHOLD && include_gpr?
  end

  def free_data_usage_error?
    # GitHub Packages allows for unlimited usage
    # for packages in public repositories.
    # We will only account for private bandwidth usage here.
   free_data_usage_percentage >= 100 && include_gpr?
  end

  def free_data_usage_percentage
    return @free_data_usage_percentage if defined?(@free_data_usage_percentage)
    @free_data_usage_percentage = @package_registry_usage.private_bandwidth_usage_percentage
  end

  def free_storage_usage_info?
    free_storage_usage_percentage >= INFO_THRESHOLD && include_storage?
  end

  def free_storage_usage_warn?
    free_storage_usage_percentage >= WARN_THRESHOLD && include_storage?
  end

  def free_storage_usage_error?
    free_storage_usage_percentage >= 100 && include_storage?
  end

  def free_storage_usage_percentage
    return @free_storage_usage_percentage if defined?(@free_storage_usage_percentage)
    @free_storage_usage_percentage = @shared_storage_usage.included_usage_percentage
  end

  def free_minutes_usage_info?
    free_minutes_usage_percentage >= INFO_THRESHOLD && include_actions?
  end

  def free_minutes_usage_warn?
    free_minutes_usage_percentage >= WARN_THRESHOLD && include_actions?
  end

  def free_minutes_usage_error?
    free_minutes_usage_percentage >= 100 && include_actions?
  end

  def free_minutes_usage_percentage
    return @free_minutes_usage_percentage if defined?(@free_minutes_usage_percentage)
    @free_minutes_usage_percentage = @actions_usage.included_minutes_usage_percentage
  end

  def paid_resources_usage_info?
    paid_resource_usage_percentage >= INFO_THRESHOLD
  end

  def paid_resources_usage_warn?
    paid_resource_usage_percentage >= WARN_THRESHOLD
  end

  def paid_resources_usage_error?
    paid_resource_usage_percentage >= 100
  end

  def paid_resource_usage_percentage
    return 0 if @metered_billing_permission.total_usage.zero?
    percent = (@metered_billing_permission.total_usage / Float(@metered_billing_permission.usage_limit)).round(2)
    # If the usage limit is 0, but total usage is a non-zero we will get Infinity
    # This could happen if user changes their limit, or when trade controls restricted
    return 100 if percent == Float::INFINITY
    (percent * 100).to_i
  end

  def remove_non_current_email_sent_keys
    keys_to_delete = []

    if current_level.present?
      keys_to_delete << email_sent_at_key(level: nil)
    end

    if current_level != LEVEL_ERROR
      keys_to_delete << email_sent_at_key(level: LEVEL_ERROR)
    end

    if current_level != LEVEL_WARN
      keys_to_delete << email_sent_at_key(level: LEVEL_WARN)
    end

    if current_level != LEVEL_INFO
      keys_to_delete << email_sent_at_key(level: LEVEL_INFO)
    end

    GitHub.kv.mexists(keys_to_delete).map do |values|
      return unless values.any?

      ActiveRecord::Base.connected_to(role: :writing) do
        GitHub.kv.mdel(keys_to_delete)
      end
    end
  end

  # Internal: Format the owner class name to match what audit log expects
  #
  # Returns Symbol
  def formatted_class_name
    downcased_class_name = @owner.class.to_s.downcase
    downcased_class_name == "organization" ? :org : downcased_class_name.to_sym
  end
end
