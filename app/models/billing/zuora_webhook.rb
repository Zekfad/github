# frozen_string_literal: true

module Billing
  class ZuoraWebhook < ApplicationRecord::Ballast
    RECENCY_THRESHOLD = 5.minutes

    SALES_SERVE_KINDS = %w[
      amendment_processed
      subscription_created
    ].freeze

    store :payload, coder: JSON

    scope :ignoring_recent, -> { where("created_at < ?", RECENCY_THRESHOLD.ago) }

    attr_readonly :kind, :account_id, :payload

    enum kind: {
      payment_processed: 0,
      payment_declined: 1,
      payment_refund_processed: 2,
      invoice_posted: 3,
      amendment_processed: 4,
      subscription_created: 5,
      account_updated: 6
    }

    enum status: {
      pending: "pending",
      processed: "processed",
      ignored: "ignored",
      investigating: "investigating"
    }

    validates :status, presence: true, on: :create

    # Public: Which team is responsible for the specified kind of webhook
    #
    # kind - the kind of webhook to determine ownership for
    # Returns string
    def self.responsibility(kind)
      if SALES_SERVE_KINDS.include?(kind.to_s)
        "gitcoin-white-glove"
      else
        "gitcoin-self-serve"
      end
    end

    # Public: The invoice ID in the webhook payload
    #
    # Returns String
    def invoice_id
      payload["InvoiceId"]
    end

    # Public: The payment ID in the webhook payload
    #
    # Depending on the type of webhook, this may be PaymentId or PaymentID in the
    # webhook payload
    #
    # Returns String
    def payment_id
      payload["PaymentId"]
    end

    # Public: The refund ID in the webhook payload
    #
    # Returns String
    def refund_id
      payload["RefundId"]
    end

    # Public: The subscription ID in the webhook payload
    #
    # Returns String
    def subscription_id
      payload["subscription_id"]
    end

    # Public: The class which handles this type of webhook
    #
    # Returns Class
    def handler
      case kind.to_sym
      when :payment_processed then GitHub::Billing::ZuoraWebhook::PaymentProcessed
      when :payment_declined then GitHub::Billing::ZuoraWebhook::PaymentDeclined
      when :payment_refund_processed then GitHub::Billing::ZuoraWebhook::PaymentRefundProcessed
      when :invoice_posted then GitHub::Billing::ZuoraWebhook::InvoicePosted
      when :amendment_processed then GitHub::Billing::ZuoraWebhook::AmendmentProcessed
      when :subscription_created then GitHub::Billing::ZuoraWebhook::SubscriptionCreated
      when :account_updated then GitHub::Billing::ZuoraWebhook::AccountUpdated
      end
    end

    # Public: Perform this webhook and mark it as processed
    #
    # Returns Boolean
    def perform
      return true if processed?

      handler.perform(self)
      update!(status: :processed, processed_at: Time.now)
    end

    # Public: Has this webhook been processed?
    #
    # Returns Boolean
    def processed?
      processed_at.present?
    end

    # Public: The URL linking this webhook to the customer in Zuora
    #
    # Returns string
    def customer_account_url
      "#{GitHub.zuora_host}/apps/CustomerAccount.do?method=view&id=#{account_id}"
    end

    # Public: Which team is responsible for this webhook
    #
    # Returns string
    def responsibility
      self.class.responsibility(kind)
    end

    # Public: The Sales Operations Issue Details for this webhook
    #
    # Returns GitHub::Billing::ZuoraWebhook::SalesOperationsIssueDetails
    def sales_operations_issue_details
      ::GitHub::Billing::ZuoraWebhook::SalesOperationsIssueDetails.new(self)
    end
    alias_method :sales_ops_issue_details, :sales_operations_issue_details

    def is_sales_serve_kind?
      SALES_SERVE_KINDS.include?(kind.to_s)
    end
  end
end
