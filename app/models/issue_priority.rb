# frozen_string_literal: true

class IssuePriority < ApplicationRecord::Domain::Repositories
  include GitHub::Prioritizable

  belongs_to :issue, touch: true
  belongs_to :milestone, touch: true, inverse_of: :issue_priorities

  validates :issue, :milestone, presence: true
  validates :priority,
    presence: true,
    uniqueness: {
      scope: :milestone_id },
    numericality: {
      less_than_or_equal_to: MAX_PRIORITY_VALUE,
      greater_than_or_equal_to: 0 }

  prioritizable_by :priority, subject: :issue, context: :milestone
end
