# frozen_string_literal: true
require "codespaces"

module Codespaces
  class ProvisionPlan
    attr_reader :owner, :location, :vscs_target

    def self.call(owner:, location:, vscs_target: nil)
      new(owner: owner, location: location, vscs_target: vscs_target).call
    end

    def initialize(owner:, location:, vscs_target: nil)
      @owner = owner
      @location = location
      @vscs_target = vscs_target || Codespaces.vscs_target(owner)
    end

    def call
      return existing_provisioned_plan if existing_provisioned_plan

      provision_resource_group!
      create_plan!
    end

    private

    # We need to call this separately / before `create_plan!` since this can
    # raise a similar set of exceptions that we do not want to handle in this
    # class. For example, if plan creation fails we need to ensure we clean up
    # the plan.
    def provision_resource_group!
      @resource_group = Codespaces::ProvisionResourceGroup.call(location: location)
    end

    def create_plan!
      plan = Codespaces::Plan.create!(
        owner: owner,
        resource_group: @resource_group,
        vscs_target: vscs_target,
        resource_provider: Codespaces::Plan::DEFAULT_RESOURCE_PROVIDER
      )
      # TODO: if this fails due to the resource group being over provisioned,
      # provision a new resource_group and retry
      client(plan.resource_provider).create_plan(
        vscs_target: plan.vscs_target,
        plan_id: plan.vscs_id,
        location: location
      )

      plan.instrument :provision
      Codespaces::Azure.report_utilization!

      plan
    rescue Codespaces::Error, Faraday::Error
      plan.destroy!
      raise
    end

    def existing_provisioned_plan
      return @existing_provisioned_plan if defined?(@existing_provisioned_plan)

      @existing_provisioned_plan = Codespaces::Plan.
        in_location(location).
        where(owner: owner, vscs_target: vscs_target, resource_provider: Codespaces::Plan::DEFAULT_RESOURCE_PROVIDER).
        first
    end

    def client(resource_provider)
      @client ||= Codespaces::ArmClient.new(resource_provider: resource_provider)
    end
  end
end
