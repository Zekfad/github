
# frozen_string_literal: true

module Codespaces
  class ResourceUsage
    include ActiveModel::Model

    attr_accessor :billing_entry,
                  :billing_message,
                  :billable_duration_in_seconds,
                  :sku

    validates_presence_of :billing_entry,
                          :billing_message,
                          :billable_duration_in_seconds,
                          :sku

    #
    # These figures & computed_usage logic are obtained from this
    # issue: https://github.com/github/codespaces/issues/779
    #
    UNIT_OF_MEASUREMENT = BigDecimal("3600") # 1 hour in seconds

    validates :sku, inclusion: %w(basicLinux standardLinux premiumLinux)

    validates :billable_duration_in_seconds, numericality: {
        only_integer: true,
        greater_than_or_equal_to: 0,
    }
    validate :validate_billing_period, unless: -> { @billing_message.nil? }

    delegate :repository, to: :billing_entry

    def owner_id
      @billing_entry.billable_owner_id
    end

    def actor_id
      @billing_entry.codespace_owner_id
    end

    def start_time
      @billing_message.period_start
    end

    def end_time
      @billing_message.period_end
    end

    def unique_billing_identifier
      parts = [
          @billing_message.event_id,
          @billing_message.codespace_plan.name,
          @billing_entry.codespace_guid,
          billing_sku
      ]
      parts.join("-")
    end

    def billing_sku
      @sku.underscore.upcase
    end

    def computed_usage
      raise "This must be implemented by your subclass"
    end

    private

    def validate_billing_period
      unless start_time < end_time
        errors.add(:base, "start time must be before end time")
      end
    end
  end
end
