# frozen_string_literal: true

module Codespaces
  class GetTargetRepository
    class UnforkableRepository < StandardError; end

    def self.call(**kwargs)
      new(**kwargs).call
    end

    def initialize(user:, repository:, ref_or_name:)
      @user, @repository = user, repository

      # Normalize `ref_or_name` to `ref`, `ref_name`, and `oid`
      if ref_or_name.is_a? Git::Ref
        @ref = ref_or_name
      else
        # This only works to find the Git::Ref if given a name e.g. "codespace-1"
        @ref = @repository.heads.find(ref_or_name) ||
          # This will find a Git::Ref if we have a SHA...
          @repository.heads.refs_with_default_first.find { |ref| ref.target_oid == ref_or_name }
      end
      # @ref can still be nil here if we were working with a repository that is
      # `empty?` (e.g. it is not fully created yet on disk).
      @ref_name = @ref&.name
      @oid = @ref&.target_oid
    end

    def call
      # Passing the ref here accounts for fork collab permissions
      return [@repository, @ref_name, @oid] unless GitHub.flipper[:codespaces_fork_repos].enabled?(@user)
      return [@repository, @ref_name, @oid] if @repository.pushable_by?(@user, ref: @ref_name)

      # Calling fork here will either create a new fork or find an existing one
      # for the user. If we are creating a new one the fork is initially `empty?`
      # which is checked below.
      forked, reason, errors = @repository.fork(forker: @user)
      raise UnforkableRepository, RepositoryForker.message_from_reason(reason, errors) unless forked && forked.pushable_by?(@user)

      if forked.empty? || @ref.nil?
        # The fork is being created now and should have everything identical to
        # the parent. We can't get the `ref` or `oid` directly from the fork
        # yet since the git repository is created async so we just use them
        # from the parent since they won't change during forking.
        #
        # The only way @ref would be nil here is if we needed to fork off of a
        # repository that was _also_ JUST created and were unable to find the
        # actual underlying Git::Ref in the initializer above. SUPER unlikely
        # but we need to return the base ref_name/oid in that scenario.
        [forked, @ref_name, @oid]
      else
        # Each new forked codespace creates a new branch updated to the ref from
        # its parent repository at the time it was created (except the first time
        # we fork where the base repo's ref name and oid are used to avoid issues
        # with the async nature of forking a repo).
        branch = forked.heads.temp_name(topic: "codespace")
        forked.fetch_commits_from(@ref, new_ref_name: branch, user: @user)
        forked_oid = forked.ref_to_sha(branch)
        [forked, branch, forked_oid]
      end
    end
  end
end
