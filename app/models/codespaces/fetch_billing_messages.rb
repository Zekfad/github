# frozen_string_literal: true
require "json"
require "json-schema"

module Codespaces
  class FetchBillingMessages
    def self.call(**kwargs)
      new(**kwargs).call
    end

    def initialize(
      azure_storage_account_name:,
      error_reporter: Codespaces::ErrorReporter,
      client: Codespaces::StorageClient.new(account_name: azure_storage_account_name)
    )
      @azure_storage_account_name = azure_storage_account_name
      @error_reporter = error_reporter
      @client = client

      @error_reporter.push(storage_account_name: azure_storage_account_name)
    end

    def call
      @client.get_messages.each do |message|
        plan_name = message.body.dig("plan", "name")
        @error_reporter.push(plan_name: plan_name)

        validation_errors = JSON::Validator.fully_validate("app/models/codespaces/schema/vscs_billing_schema.json", message.body);
        if validation_errors.length > 0
          raise Codespaces::Error.new("Failed vscs_billing_schema validation\nMessage Id - #{message.id}\nErrors\n#{validation_errors}")
        end

        plan = Plan.find_by!(name: plan_name)
        Codespaces::BillingMessage.create!(
          codespace_plan_id: plan.id,
          event_id: message.event_id,
          azure_storage_account_name: @azure_storage_account_name,
          payload: message.body
        )
        @client.delete_message(message_id: message.id, pop_receipt: message.pop_receipt)
      rescue ActiveRecord::RecordNotUnique
        # This should indicate that we no longer need this message but perhaps
        # something went wrong w/the previous delete attempt so try again.
        @client.delete_message(message_id: message.id, pop_receipt: message.pop_receipt)
      rescue Codespaces::Error => e
        @error_reporter.report(e)
      rescue ActiveRecord::RecordNotFound => e
        @error_reporter.report(e,
          codespace_vscs_plan_id: plan_name
        )
      end
    end
  end
end
