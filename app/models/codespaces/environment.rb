# frozen_string_literal: true

module Codespaces
  class Environment
    attr_reader :id, :name, :state, :updated_at

    def self.from_json(json)
      # If the ID or timestamp are missing then let's assume this whole thing is
      # garbage.
      return unless id = json["id"]
      return unless updated = json["updated"]
      return unless updated_at = Time.iso8601(updated)

      new(id: id,
          name: json["friendlyName"],
          state: json["state"],
          updated_at: updated_at)
    end

    def initialize(id:, name:, state:, updated_at:)
      @id = id
      @name = name
      @state = state
      @updated_at = updated_at
    end

    def available?
      state == "Available"
    end
  end
end
