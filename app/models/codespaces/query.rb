# frozen_string_literal: true

module Codespaces
  class Query
    ACCESSIBLE_CODESPACES = "accessible"
    MAX_CODESPACES = 20

    attr_reader :current_user, :repository, :ref, :pull_request, :codespaces_context, :unauthorized_saml_targets

    def initialize(
      current_user:,
      repository: nil,
      ref: nil,
      pull_request: nil,
      codespaces_context: nil,
      unauthorized_saml_targets: nil
    )
      raise ArgumentError, "`unauthorized_saml_targets` is required for displaying all accessible codespaces" if codespaces_context == Codespaces::Query::ACCESSIBLE_CODESPACES && unauthorized_saml_targets.nil?

      @current_user, @repository, @ref, @pull_request, @codespaces_context, @unauthorized_saml_targets =
        current_user, repository, ref, pull_request, codespaces_context, Set.new(unauthorized_saml_targets)
    end

    def codespaces
      return @codespaces if defined?(@codespaces)

      scope = current_user

      @codespaces = if show_all_accessible_codespaces?
        scope.accessible_codespaces.by_recently_used.to_a
      elsif ref
        scope_codespaces = scope.accessible_codespaces.by_recently_used
        GitHub::PrefillAssociations.for_codespaces_repository(scope_codespaces)
        scope_codespaces.find_all { |codespace|
          if codespace.repository_id == repository.id
            # If we're viewing the repository for a given codespace then
            # require its ref matches.
            codespace.ref == ref
          else
            # If we're _not_ viewing the same repo then only require that
            # the codespace repo's network is the same as this repo's network.
            # This allows codespaces from forked repos to show up on their
            # parent repository. We can't check the ref because we actually
            # expect the refs to not match due to creating a `codespace-{n}`
            # branch.
            codespace.repository.network_id == repository.network_id
          end
        }.take(MAX_CODESPACES)
      elsif pull_request
        scope.accessible_codespaces.
          where(
            {
              pull_request: pull_request
            }
          ).
          by_recently_used.limit(MAX_CODESPACES).to_a
      else
        []
      end

      @codespaces = reject_plan_owners(@codespaces, owner_ids: unauthorized_saml_targets.pluck(:id))
    end

    def build_codespace
      return @new_codespace if defined?(@new_codespace)

      @new_codespace = current_user.codespaces.build \
        repository: pull_request ? pull_request.head_repository : repository,
        ref: ref,
        pull_request: pull_request
    end

    def show_all_accessible_codespaces?
      codespaces_context == ACCESSIBLE_CODESPACES
    end

    def pull_request_id
      pull_request&.id
    end

    private

    def reject_plan_owners(codespaces, owner_ids:)
      return codespaces unless owner_ids.present?

      codespaces.reject do |codespace|
        owner_ids.include?(codespace.plan.owner_id)
      end
    end
  end
end
