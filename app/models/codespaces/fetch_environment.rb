# frozen_string_literal: true

module Codespaces
  class FetchEnvironment
    PROVISIONING_LOCK_TIMEOUT = 20.seconds

    class Lock
      def initialize(codespace, ttl: PROVISIONING_LOCK_TIMEOUT)
        @ttl = ttl
        @lock_key = "codespaces-provisioning-lock-#{codespace.name}"
      end

      def locked?
        GitHub.kv.exists(@lock_key).value { false }
      end

      def lock!
        GitHub.kv.setnx(@lock_key, "true", expires: @ttl.from_now)
      end

      def unlock!
        GitHub.kv.del(@lock_key)
      end
    end

    attr_reader :codespace, :lock
    delegate :locked?, :lock!, :unlock!, to: :lock, private: true

    def self.call(codespace)
      new(codespace).call
    end

    # The codespace MUST be assigned to a plan already at this point for us to
    # find/create its backing environment.
    def initialize(codespace, lock: Lock.new(codespace))
      @codespace = codespace
      @lock = lock
    end

    # Attempts to find or create (fetch) an environment for this codespace in
    # VSCS. Special retry handling is included here to safeguard against
    # creating duplicate environments on retries.
    #
    # When a timeout is encountered we lock provisioning and raise a specific
    # error class that can be retried independently of other retryable errors.
    # When the job retries we can check for this lock and know that we only
    # want to attempt to read the environment until the lock expires. This puts
    # us in a situation where we end up effectively doing:
    #   1. Creation TimeoutError -> lock! -> raise
    #   2. locked? -> Environment found? -> No -> raise
    #   3. locked? -> Environment found? -> No -> raise
    #   4. locked? -> Environment found? -> No -> raise
    #   5. Found environment to use and complete provisioning -> unlock!
    # There is also a possibility that we will continue retrying and never find
    # an environment with the correct name. This would happen if we timed out
    # initially and the request actually went on to FAIL in VSCS. In that case
    # we would want to retry creation again once the lock expires.
    def call
      if locked?
        # The bang variant here raises if not found.
        Codespaces::FindEnvironment.call!(codespace)
      else
        Codespaces::FindEnvironment.call(codespace) ||
          Codespaces::CreateEnvironment.call(codespace)
      end.tap { unlock! }
    rescue Codespaces::CreateEnvironment::TimeoutError
      # If creation timed out we need to lock fetching this codespace
      # temporarily.
      lock! and raise
    end
  end
end
