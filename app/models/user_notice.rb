# frozen_string_literal: true

class UserNotice
  attr_reader :name, :effective_date, :hide_from_new_user, :decommission_date

  DEFAULT_NOTICES_PATH = Rails.root.join("config", "notices.yml")

  alias hide_from_new_user? hide_from_new_user

  def initialize(options)
    @options            = options
    @name               = options.fetch("name")
    @decommission_date  = options.fetch("decommission_date", nil)
    unless decommissioned?
      @effective_date     = options.fetch("effective_date")
      @hide_from_new_user = options.fetch("hide_from_new_user")
    end
  end

  def self.all
    load_notices unless @all
    @all
  end

  def self.active
    load_notices unless @active
    @active
  end

  def self.find(name)
    self.active.detect { |notice| notice.name.to_s.downcase == name.to_s.downcase }
  end

  def self.load_notices(file_path = DEFAULT_NOTICES_PATH)
    notice_definitions = YAML.safe_load(File.read(file_path))["notices"]

    @all = notice_definitions.map do |info|
      notice = new(info)
      set_constant(notice) unless notice.decommissioned?
      notice
    end.compact

    @active = @all.reject { |notice| notice.decommissioned? }
  end

  def decommissioned?
    decommission_date.present? && Date.parse(decommission_date) < Date.today
  end

  def self.set_constant(notice)
    const_name = notice.name.end_with?("_notice") ? notice.name.upcase : "#{notice.name}_notice".upcase
    UserNotice.const_set(const_name, notice.name) unless UserNotice.const_defined?(const_name.to_sym)
  end
  private_class_method :set_constant

  load_notices
end
