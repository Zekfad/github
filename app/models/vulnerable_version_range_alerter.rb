# frozen_string_literal: true

# see tests for VulnerableVersionRangeCreateVulnerabilityAlerts
class VulnerableVersionRangeAlerter
  DATADOG_PREFIX = "vulnerable_version_range_alerter"
  include GitHub::DatadogHelper

  attr_accessor :process, :event, :range, :skip_email, :query

  def initialize(process, skip_email = false)
    self.process = process # A VulnerableVersionRangeAlertingProcess
    self.event = process.vulnerability_alerting_event
    self.range = process.vulnerable_version_range
    self.skip_email = !!skip_email
    self.query = fetch_dependents_from_range(range)
  end

  # returns alertable_dependents so they can be counted in
  # VulnVersionRange#increment_total_alerts_processed_count
  def process_alertable_repos
    datadog_time(:total_time) do
      self.range.begin_processing!

      dependents =
        datadog_time(:query_time) do
          self.query.results.value!
        end

      alertable_dependents =
        datadog_time(:filter_time) do
          filter_unalertable_repos(dependents)
        end

      datadog_histogram(
        dependent_count: dependents.count,
        alertable_dependent_count: alertable_dependents.count,
      )
      datadog_histogram(alertable_percentage: 100 * alertable_dependents.count / dependents.count) if dependents.any?

      datadog_time(:create_time) do
        create_alerts(range, alertable_dependents)
      end

      alertable_dependents
    end
  end

  def fetch_dependents_from_range(range)
    filter = {
      package_manager: range.ecosystem,
      package_name: range.affects,
      requirements: range.requirements,
      first: 100,
      preview: GitHub.flipper[:vulnerability_alerts_preview_dependencies].enabled?,
    }
    filter[:after] = range.cursor if range.cursor

    backend = DependencyGraph::Client.new(timeout: 10)

    DependencyGraph::AllRepositoriesWithVersionRangeQuery.new(
      dependents_filter: filter,
      backend: backend,
    )
  end

  # Not every dependent is reportable. Filter out dependents with
  # repos that have been archived or do not have alerts turned on.
  def filter_unalertable_repos(dependents)
    grouped_dependents = dependents.group_by(&:repository_id)
    alertable_repo_ids = fetch_alertable_repo_ids(grouped_dependents.keys)

    grouped_dependents.values_at(*alertable_repo_ids).flatten
  end

  def create_alerts(range, alertable_dependents)
    alertable_dependents.each do |dependent|
      datadog_time(:"alert.total_time") do
        alert =
          datadog_time(:"alert.create_time") do
            create_alert_for(range, dependent)
          end

        if alert
          Dependabot::RepositoryVulnerabilityCreatedJob.enqueue(alert)

          datadog_time(:"alert.notification_time") do
            alert.trigger_notifications(
              vulnerability_alerting_event: event,
              skip_email: skip_email,
            )
          end
        end
      end
    end
  end

  def more_to_process?
    self.query.has_next?
  end

  def query_cursor
    self.query.last_cursor
  end

  private

  def fetch_alertable_repo_ids(repo_ids)
    Repository.with_ids(repo_ids).with_vulnerability_alerts_enabled.ids.to_set
  end

  def create_alert_for(range, dependent)
    throttle_start_time = GitHub::Dogstats.monotonic_time

    RepositoryVulnerabilityAlert.throttle_with_retry(max_retry_count: 10) do
      datadog_timing_since("alert.throttle_wait_time": throttle_start_time)
      insert_start_time = GitHub::Dogstats.monotonic_time
      insert_tags = ["result:success"]

      begin
        alert =
          RepositoryVulnerabilityAlert.new(
            vulnerability_id: range.vulnerability.id,
            vulnerable_version_range_id: range.id,
            repository_id: dependent.repository_id,
            vulnerable_manifest_path: dependent.manifest_path,
            vulnerable_requirements: dependent.requirements,
          )
        alert.save!(validate: false)
        alert
      rescue ActiveRecord::RecordNotUnique
        insert_tags = ["result:noop"]
        nil # alert already exists, return nothing
      ensure
        datadog_timing_since("alert.insert_time": insert_start_time, tags: insert_tags)
      end
    end
  rescue Freno::Throttler::Error => error
    datadog_timing_since("alert.throttle_timeout_time": throttle_start_time)
    raise error
  end

  def datadog_tags
    return @datadog_tags if @datadog_tags

    datadog_tags = process.datadog_tags.dup
    datadog_tags << "skip_email:#{skip_email}"

    @datadog_tags = datadog_tags
  end
end
