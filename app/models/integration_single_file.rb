# frozen_string_literal: true

class IntegrationSingleFile < ApplicationRecord::Collab
  belongs_to :version,
    class_name:  "IntegrationVersion",
    foreign_key: :integration_version_id,
    required:    true
  validates :path, presence: true, uniqueness: { scope: :version, case_sensitive: true }
end
