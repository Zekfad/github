# frozen_string_literal: true
require "github-launch"

class Artifact < ApplicationRecord::Ballast
  include Instrumentation::Model

  areas_of_responsibility :checks
  belongs_to :check_suite

  extend GitHub::Encoding
  force_utf8_encoding :name

  validates :source_url, presence: true
  validates :name, presence: true
  validates :size, presence: true

  after_commit :notify_socket_subscribers, on: [:create, :update]
  after_commit :emit_artifact_add_event, on: [:create]
  after_commit :instrument_destruction, on: :destroy

  before_destroy :delete_from_file_storage
  before_destroy :emit_artifact_remove_event

  def expired?
    if expires_at
      expires_at < Time.now
    else
      created_at + 90.days < Time.now
    end
  end

  private

  def notify_socket_subscribers
    data = {
      timestamp: updated_at,
      wait: default_live_updates_wait,
      reason: "artifact created or updated",
    }
    GitHub::WebSocket.notify_repository_channel(check_suite.repository, check_suite.channel, data)
  end

  def delete_from_file_storage
    check_suite_global_relay_id = Platform::Helpers::NodeIdentification.to_global_id("CheckSuite", check_suite_id)

    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Artifactsexchange::DeleteArtifactRequest.new(
        check_suite_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: check_suite_global_relay_id),
        artifact_name: name,
      )
      GitHub.launch_artifacts_exchange_for_check_suite(check_suite).delete_artifact(grpc_request)
    end

    unless result.call_succeeded?
      raise "Could not delete the artifact from file storage"
    end
  end

  def emit_artifact_event(event_type)
    repository = self.check_suite&.repository
    visibility = (repository.public? ? "public" : "private") if repository

    message = {
      artifact_id: self.id,
      artifact_name: self.name,
      artifact_repository_owner_id: repository&.owner_id,
      artifact_repository_id: repository&.id,
      artifact_repository_visibility: visibility,
      artifact_size_in_bytes: self.size,
      check_suite_id: self.check_suite_id,
      check_run_id: self.check_run_id,
      event_type: event_type,
    }

    if event_type == "add"
      message[:expires_at] = self.expires_at
    else
      message[:previously_expired_at] = self.expires_at
    end

    GlobalInstrumenter.instrument("actions.artifact_storage_event", message)
  end

  # This lets Billing know the artifact has been removed. So we can stop billing the customer for it.
  def emit_artifact_remove_event
    emit_artifact_event("remove")
  end

  # This lets Billing know the artifact has been created. So we can start billing the customer for it.
  def emit_artifact_add_event
    emit_artifact_event("add")
  end

  def instrument_destruction
    # Ignore when the artifact is destroyed because the repository was deleted. It'd be too noisy
    return if check_suite.nil?

    actor = User.find_by(id: GitHub.context[:actor_id])
    instrument :destroy, actor: actor, repo: check_suite.repository, artifact: self
  end

end
