# rubocop:disable Style/FrozenStringLiteralComment

class RepositoryFile < ApplicationRecord::Domain::Assets
  include ::Storage::Uploadable
  include SlottedCounterService::Countable

  set_uploadable_policy_path "repository-files"
  add_uploadable_policy_attributes :repository_id

  set_content_types \
    "application/pdf" => ".pdf",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => ".docx",
    "application/vnd.openxmlformats-officedocument.presentationml.presentation" => ".pptx",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => ".xlsx",
    "application/zip" => ".zip",
    "application/x-zip-compressed" => ".zip",
    "application/gzip" => ".gz",
    "application/x-gzip" => ".gz",
    "text/plain" => ".txt",
    "text/x-log" => ".log"

  belongs_to :repository
  belongs_to :uploader, class_name: "User", foreign_key: :uploader_id

  before_validation :infer_content_type
  belongs_to :storage_blob, class_name: "Storage::Blob"
  validate :storage_ensure_inner_asset
  validates_inclusion_of :content_type, in: allowed_content_types
  validates_presence_of :name
  validate :extension_matches_content_type
  validates_inclusion_of :size, in: 1..25.megabytes

  validate :uploader_access, on: :create
  before_destroy :storage_delete_object
  before_create :choose_storage_provider

  enum state: Storage::Uploadable::STATES

  # BEGIN storage settings

  def storage_policy(actor: nil, repository: nil)
    klass = if GitHub.storage_cluster_enabled?
      ::Storage::ClusterPolicy
    else
      ::Storage::S3Policy
    end
    klass.new(self, actor: actor, repository: self.repository)
  end

  def self.storage_new(uploader, blob, meta)
    new(
      storage_blob: blob,
      uploader: uploader,
      content_type: meta[:content_type],
      repository_id: meta[:repository_id],
      name: meta[:name],
      size: meta[:size],
    )
  end

  def self.storage_create(uploader, blob, meta)
    storage_new(uploader, blob, meta).tap do |file|
      file.update(state: :uploaded)
    end
  end

  alias_method :storage_blob_accessible?, :uploaded?

  def storage_transition_ready?
    repository && storage_blob_accessible?
  end

  def creation_url
    "#{GitHub.storage_cluster_url}/repositories/#{repository_id}/files"
  end

  def storage_cluster_url(policy)
    creation_url + "/#{id}"
  end

  def storage_cluster_download_token(policy)
    super unless !GitHub.private_mode_enabled? && repository.public?
  end

  def storage_download_path_info(policy)
    "#{storage_upload_path_info(policy)}/#{id}"
  end

  def storage_upload_path_info(policy)
    "/internal/storage/repositories/#{repository_id}/files"
  end

  def alambic_download_headers(options = nil)
    {
      "Content-Disposition" => "attachment;filename=#{name}",
    }
  end

  def upload_access_grant(actor)
    Api::AccessControl.access_grant(
      verb: :pull,
      user: actor,
      resource: repository,
    )
  end

  # s3 storage settings

  def storage_s3_key(policy)
    if storage_provider == :s3_production_data
      "#{repository_id}/#{id}"
    else
      "assets/repositories/#{repository_id}/#{id}"
    end
  end

  def storage_s3_download_query(query)
    query["response-content-disposition"] = "attachment;#{filename_content_disposition}"
    query["response-content-type"] = content_type
  end

  # END storage settings

  # Public: Absolute permalink URL for this file.
  #
  # Returns a String.
  def permalink
    "#{repository.permalink}/files/#{id}/#{CGI.escape name}"
  end

  # The url to which we redirect requests for the permalink. The file is
  # actually stored in S3. We use permalinks to the Rails server to have a
  # consistent URL stored in issue comment bodies.
  #
  # Returns a String URL.
  def redirect_url(actor: nil)
    storage_policy(actor: actor).download_url
  end

  # Send the permalink back to the JS file uploader to include in the
  # issue comment markdown link.
  #
  # Returns a String URL.
  def url
    permalink
  end

  # Send the permalink back to the JS file uploader to include in the
  # issue comment markdown link.
  #
  # Returns a String URL.
  def storage_external_url(_ = nil)
    permalink
  end

  def self.storage_s3_bucket
    GitHub.s3_environment_config[:asset_bucket_name]
  end

  def self.storage_s3_new_bucket
    GitHub.s3_repository_file_new_bucket
  end

  def self.storage_s3_new_bucket_host
    GitHub.s3_repository_file_new_host
  end

  def storage_s3_bucket
    if storage_provider == :s3_production_data
      self.class.storage_s3_new_bucket
    else
      self.class.storage_s3_bucket
    end
  end

  def storage_s3_access_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_access_key
    else
      GitHub.s3_environment_config[:access_key_id]
    end
  end

  def storage_s3_secret_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_secret_key
    else
      GitHub.s3_environment_config[:secret_access_key]
    end
  end

  def downloads
    slotted_count_with :downloads
  end

  def download
    slotted_count!(:downloads)
  end

  def guid
    # We don't have a guid, but this satisfies an alambic requirement.
  end

  def uploadable_surrogate_key
    "repository-#{repository_id} #{super}"
  end

  private

  def choose_storage_provider
    return if GitHub.storage_cluster_enabled?
    self.storage_provider = :s3_production_data
  end

  def uploader_access
    if !uploader
      errors.add :uploader_id, "is not a valid User"
    elsif !repository
      errors.add :repository_id, "is not a valid repository"
    elsif !repository.pullable_by?(uploader) && !importing?
      errors.add :uploader_id, "does not have read access to #{repository.name_with_owner}"
    end
  end

  def filename_content_disposition
    "filename=#{name}"
  end

  def importing?
    GitHub.importing?
  end
end
