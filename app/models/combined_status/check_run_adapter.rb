# frozen_string_literal: true

require "delegate"

class CombinedStatus
  class CheckRunAdapter < SimpleDelegator
    def state
      (conclusion || status).to_s
    end

    def state_changed_at
      completed_at || started_at || created_at
    end

    # This method is especially important because it's used in protected branch rules
    def context
      visible_name
    end

    def description
      title
    end

    def platform_type_name
      "CheckRun"
    end

    def duration_in_seconds
      return 0 if !completed_at
      return 0 if completed_at > Time.now.utc

      (completed_at - (started_at || completed_at)).floor
    end

    # The view means OAuth Application
    # when it says application.
    def application
      nil
    end

    def target_url
      permalink
    end

    def sha
      head_sha
    end

    # This would be much nicer if it were `application`,
    # but the view won't do the right thing if we do that.
    def creator
      github_app&.bot || User.ghost
    end

    def tree_oid
      check_suite.repository.commits.find(check_suite.head_sha).tree_oid
    end
  end
end
