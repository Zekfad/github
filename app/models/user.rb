# rubocop:disable Style/FrozenStringLiteralComment

class User < ApplicationRecord::Domain::Users
  after_commit :forget_plan_changed_this_save

  include GitHub::Validations

  extend GitHub::Earthsmoke::EncryptedAttribute
  extend GitHub::BackgroundDependentDeletes
  extend GitHub::SimplePagination
  include GitHub::Relay::GlobalIdentification
  include AbuseReportable
  include Configurable::OktaTeamSyncBeta
  include Configurable::ActionInvocation
  include Configurable::Dependabot
  include Configurable::DefaultNewRepoBranch
  include GitHub::ApplicationsCreationLimit
  include Dumpable
  include GitHub::FlipperActor
  include User::AvatarList::Model
  include User::NewsiesAdapter
  include PrimaryAvatar::Model
  include Marketplace::Listable
  include StaffAccessible
  include Instrumentation::Model
  include Scientist
  include SecurityAnalysisDependency

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::User

  attr_accessor :company_name

  # Logins are limited to alphanumerics and hyphens, and must start and end with
  # an alphanumeric. This can basically never change.
  # All sorts of code depends on these restrictions.
  LOGIN_REGEX = /\A[a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\z/
  LOGIN_VALIDATION_MESSAGE = "may only contain alphanumeric characters or single hyphens, and cannot begin or end with a hyphen"
  LOGIN_MAX_LENGTH = 39
  FOLLOWABLE_CONTEXTS = %w(api user_profile).freeze
  PASSPHRASE_LENGTH = 15
  NOT_UNIQUE_LOGIN_MESSAGE = "is not available"

  # We use a **very** loose regex here on purpose. We do not want anything
  # like an RFC-level validation, especially because we explicity allow
  # emails like "johndoe@locahost" for git commit emails.
  #
  # See user_email_test for what this allows and doesn't allow,
  # and make sure to add tests if you change behavior.
  EMAIL_REGEX = /\A[^@\s.\0][^@\s\0]*@\[?[a-z0-9.-]+\]?\z/i

  SPAM_EXEMPTION_KEY = "SkipSpamChecks:User"
  SPAM_EXEMPTION_TTL = 5.minutes

  include User::AbilityDependency
  include User::AdvancedSecurityDependency
  include User::AuthorEmailsDependency
  include User::NoticesDependency
  include User::OrganizationsDependency
  include User::EnterpriseDependency
  include User::IgnoreDependency
  include User::RemoteAuthenticationDependency
  include User::CouponsDependency
  include User::BillingDependency
  include User::RolesDependency
  include User::ContributionsDependency
  include User::TwoFactorAuthenticationDependency
  include User::TwoFactorRequirementDependency
  include User::InstrumentationDependency
  include User::InteractionsDependency
  include User::EnterpriseArOverridesDependency
  include User::TradeControlsDependency
  include User::BillingTransactionsDependency
  include User::ConfigurationDependency
  include User::LdapDependency
  include User::DeveloperProgramDependency
  include User::ExperimentsDependency
  include User::AssociatedRepositoriesDependency
  include User::PinsDependency
  include User::RateLimitWhitelistingDependency
  include User::DormancyDependency
  include User::EmailVerificationDependency
  include User::WikiDependency
  include User::OnboardingDependency
  include User::OauthDependency
  include User::DelegatedAccountRecoveryDependency
  include User::SavedRepliesDependency
  include User::LanguagesDependency
  include User::ThirdPartyAnalyticsDependency
  include User::SecurityCheckupDependency
  include User::ProjectsDependency
  include User::BusinessDependency
  include User::UserRankedDependency
  include User::UserStatusDependency
  include User::SponsorsDependency
  include User::ConfigRepoDependency
  include User::RepositoryTemplateDependency
  include User::PasswordDependency
  include User::ProfileReadmeDependency
  include User::ProfilesDependency
  include User::SignInAnalysisDependency
  include User::DiscussionsDependency
  include User::SuccessorsDependency
  include User::PreReleaseFeaturesMethods
  include User::FeatureFlagMethods
  include User::CodespacesDependency
  include User::MarketplaceDependency
  include User::PolicyDependency

  alias_attribute :to_s, :login
  alias_attribute :to_param, :login
  alias_attribute :name, :login
  alias_attribute :display_login, :login
  alias_attribute :to_i, :id

  validates_presence_of     :email, unless: proc { |user| !user.email_address_required? }
  validates_format_of       :billing_email, with: EMAIL_REGEX, message: "does not look like an email address", allow_nil: true, allow_blank: true
  validates :gravatar_email, unicode3: true
  validates :profile_name, :profile_email, :profile_blog, :profile_company,
            :profile_location, unicode3: true, if: :profile_updated?,
            length: {maximum: 255}
  validates :profile_bio, length: {maximum: Profile::BIO_MAX_LENGTH},
                          if: :limit_bio_length?
  validates :profile_twitter_username,
    length: { maximum: Profile::TWITTER_USERNAME_MAX_LENGTH},
    format: { with: Profile::TWITTER_USERNAME_FORMAT, message: "can only contain letters, numbers, and underscores" },
    allow_nil: true,
    if: :profile_updated?
  validate :profile_email_is_known, if: :profile_updated?

  # Passwords must adhere to these rules:
  #   - Must be at least `GitHub.password_minimum_length` characters long
  #   - Must be at most 72 characters long (anything longer is ignored by BCrypt)
  #   - Can't be your username
  # If the password is less than 16 chars and doesn't contain 2 spaces:
  #   - Must contain 1 lowercase letter
  #   - Must contain 1 number
  # If the password is 16 chars or more and has two spaces, we assume it is a
  # passphrase, and therefore exempt from specific char requirements.
  validates_presence_of     :password,                                 if: :password_required?
  validates_length_of       :password,
    maximum: 72,
    if: :password_required?
  validates_confirmation_of :password,                                 if: :password_required?, message: "doesn't match the password"
  validate                  :enforce_stronger_password
  validate                  :password_not_weak,    if: :password_required?

  # Logins must adhere to these rules:
  #  - Must be unique, case insensitive
  #  - Must be no more than 40 characters long
  #  - Can only contain alphanumeric characters and dashes
  #  - Cannot begin with a dash
  #  - Cannot be one of DeniedLogins
  validates_presence_of     :login
  validates_length_of       :login, within: 1..LOGIN_MAX_LENGTH
  validates_format_of       :login, with: LOGIN_REGEX,
    if: lambda { |user| user.will_save_change_to_login? && user.validates_login_format? },
    message: LOGIN_VALIDATION_MESSAGE

  validate                  :denied_logins, :uniqueness_of_login
  validate                  :uniqueness_of_email, on: :create
  validate                  :staff_must_be_an_employee

  validate :enforce_seat_limit, on: :create, unless: :skip_seat_limit_enforcement?

  encrypted_attribute :weak_password_check_result, :plaintext_weak_password_check_result, read_error: :return_nil, write_error: :return_nil

  before_validation :normalize_profile_twitter_username, if: :profile_updated?

  before_save    :encrypt_password
  before_create  :set_analytics_tracking_id
  before_create  :set_initial_primary_email
  before_save    :set_initial_gravatar_email

  # This is used to force staff==employee validation during any
  # `update_attribute` calls and is defined in roles_dependency.rb.
  before_save    :staff_must_be_an_employee!

  before_create  :mark_for_spam_check
  before_create  :regenerate_token_secret
  before_create  :enable_mandatory_email_verification, if: lambda { GitHub.mandatory_email_verification_enabled? }

  before_update  :regenerate_token_secret, if: :will_save_change_to_password?
  before_update  :clear_spam_flag_if_whitelisted
  after_commit   :instrument_creation, on: :create
  after_commit   :sync_site_admin_and_global_business_admin
  after_save     :update_business_user_account_login, if: :saved_change_to_login?
  after_save     :update_profile
  after_save     :record_plan_change_transaction, if: :saved_change_to_disabled?
  after_commit   :instrument_disabled, if: :saved_change_to_disabled?, on: [:create, :update]

  after_commit   :enqueue_check_for_spam
  after_commit   :synchronize_search_index

  after_update   :enqueue_update_locked_repositories, if: :plan_changed_to_paid?
  after_update   :remove_coupon_on_business_plus_upgrade
  after_update   :instrument_rename, if: :saved_change_to_login?
  after_update   :instrument_last_ip_update, if: :saved_change_to_last_ip?

  # NOTE: Need to declare these before associations so the emails aren't destroyed first.
  before_destroy       :memoize_email_for_instrument_deletion
  before_destroy       :destroy_user_callbacks_before_destroy
  after_destroy_commit :instrument_deletion
  after_destroy_commit :destroy_user_callbacks_after_commit

  scope :by_ip, lambda { |ip| where(last_ip: ip) }
  scope :spammy, -> { where(spammy: true) }
  scope :not_spammy, -> { where(spammy: false) }
  scope :oldest_to_newest, -> { order("id ASC") }
  scope :newest_to_oldest, -> { order("id DESC") }
  scope :by_login, -> { order("login ASC") }

  scope :publicly_belongs_to, ->(org) {
    joins("INNER JOIN `public_org_members` " +
          "ON `public_org_members`.`user_id` = `users`.`id`").
        where(public_org_members: {organization_id: org})
  }

  # Users with a explicitly set marketing mail preference.
  scope :accepts_marketing_mail, -> {
    joins("INNER JOIN newsletter_preferences ON users.id = newsletter_preferences.user_id")
    .where("newsletter_preferences.elected_marketing_at IS NOT NULL")
    .where("newsletter_preferences.elected_transactional_at IS NULL")
  }

  scope :has_verified_email, lambda {
    where("EXISTS (SELECT 1 FROM user_emails where users.id = user_id AND user_emails.state = 'verified')")
  }
  scope :suspended, -> { where("suspended_at IS NOT NULL") }
  scope :not_suspended, -> { where(suspended_at: nil) }
  scope :filter_spam_for, lambda { |viewer|
    return if viewer && viewer.site_admin?

    if viewer
      where("(users.spammy = false) OR users.id = ?", viewer.id)
    else
      where("users.spammy = false")
    end
  }

  scope :exclude, lambda { |*users|
    users.empty? ? nil : where("users.id NOT IN (?)", users.flat_map(&:id))
  }

  scope :shared_with, lambda { |user|
    where("followers.following_id in (
      SELECT users.id FROM users INNER JOIN
      followers ON followers.user_id = ? AND
      followers.following_id = users.id
    )", user.id)
  }

  scope :known_by, lambda { |user|
    where("followers.user_id in (
      SELECT users.id FROM users INNER JOIN
      followers ON followers.user_id = ? AND
      followers.following_id = users.id
    )", user.id)
  }

  scope :following_starred, lambda { |user_id, repository_id, period = nil|
    since = period ? Star.timestamp_for_period(period) : 5.years.ago
    sql_user_id_stars = github_sql.new <<-'SQL', since: since, repository_id: repository_id
      SELECT stars.user_id
      FROM stars
      WHERE starrable_id = :repository_id
      AND starrable_type = 'Repository'
      AND stars.created_at > :since
      order by created_at DESC
    SQL

    star_users_ids = sql_user_id_stars.results
    following_ids = Following.with_ids([user_id], field: :user_id).pluck(:following_id)
    user_ids = star_users_ids.flatten & following_ids
    User.default_scoped.with_ids(user_ids).order("users.login ASC")
  }

  scope :following_at, lambda { |user_id, organization_id|
    order("followers.created_at DESC").
      joins(sanitize_sql_array([
      "INNER JOIN
        followers ON followers.user_id = ? AND followers.following_id = users.id
      INNER JOIN
        public_org_members ON public_org_members.user_id = followers.following_id
        AND public_org_members.organization_id = ?
      ", user_id, organization_id]))
  }

  # time    :renamed_at
  # boolean :renaming
  # string  :_primary_email
  # string  :gravatar_id
  # string  :avatar_uuid
  # time    :hide_jobs_until
  # string  :deleted_by
  # string  :_billing_email
  # boolean :repository_next_participant
  # boolean :repository_navigation_v3_participant
  # string  :raw_login

  # Sticky protocol preferences.  See GitHub::RepositoryProtocolSelector.

  # { "push" => "ssh"
  # , "clone" => "gitweb"
  # }
  # hash :protocols

  # If we have to sync the ldap memberships after creating the user
  # boolean :needs_ldap_memberships_sync

  # Flag for users who have browsed from a known anonymizing proxy (e.g. Tor) at
  # some point.
  # boolean :has_used_anonymizing_proxy
  serialize :raw_data, Coders::Handler.new(Coders::UserCoder)
  delegate *Coders::UserCoder.members, to: :raw_data

  has_many :repositories,
    -> { where(active: true) },
    foreign_key: :owner_id,
    after_add: [:synchronize_search_index],
    after_remove: [:synchronize_search_index] do

    def visible
      where(deleted: false).to_a
    end

    def sorted_visible
      @sorted_visible ||= visible.sort_by { |r| r.name.downcase }
    end
  end

  has_many :deleted_repositories,
    -> { where(active: nil) },
    foreign_key: :owner_id, class_name: "Repository"

  has_many :public_repositories,
    -> { where(public: true, active: true) },
    foreign_key: :owner_id, class_name: "Repository"

  has_many :private_repositories,
    -> { where(public: false, active: true) },
    foreign_key: :owner_id, class_name: "Repository"

  has_many :owned_private_repositories,
    -> { where(public: false, parent_id: nil, active: true,  locked: false) },
    foreign_key: :owner_id,
    class_name: "Repository"

  has_one :most_recent_session,
    -> { where(impersonator_session_id: nil).order("accessed_at DESC") },
    class_name: "UserSession"

  has_many :hidden_task_list_items
  destroy_dependents_in_background :hidden_task_list_items

  has_many :close_issue_references

  # Repositories you own that have been pushed to or created recently
  has_many :recently_updated_owned_repos,
    -> { where(active: true).order("repositories.pushed_at DESC, repositories.created_at DESC") },
    class_name: "Repository",
    foreign_key: :owner_id

  has_many :assets,
    -> { order("id DESC") },
    class_name: "UserAsset"

  # These are records that this user has uploaded an image that has been
  # determined to be illegal or of extreme violence.
  # See github.com/github/schaefer for more
  has_many :photo_dna_hits,
    foreign_key: :uploader_id

  # Public repositories you own that have been pushed to or created recently
  has_many :recently_updated_public_repos,
    -> { where(active: true, public: true).order("repositories.pushed_at DESC, repositories.created_at DESC") },
    class_name: "Repository",
    foreign_key: :owner_id

  has_many :projects, -> { where(owner_type: "User") }, foreign_key: :owner_id

  has_many :memex_projects, -> { where(owner_type: "User") }, foreign_key: :owner_id
  destroy_dependents_in_background :memex_projects

  before_destroy { delete_has_many_association(:stars) }
  has_many :stars, dependent: :delete_all

  has_many :starred_repositories,
    through: :stars,
    source: :starrable,
    source_type: Star::STARRABLE_TYPE_REPOSITORY,
    split: true

  before_destroy { destroy_has_many_association :delivered_repository_invitations }
  has_many :delivered_repository_invitations, foreign_key: "inviter_id", class_name: "RepositoryInvitation", dependent: :destroy
  before_destroy { destroy_has_many_association :received_repository_invitations }
  has_many :received_repository_invitations, foreign_key: "invitee_id", class_name: "RepositoryInvitation", dependent: :destroy
  has_many :invited_repositories, -> { active.distinct },
    through: :received_repository_invitations,
    source: :repository

  has_many :user_interests

  before_destroy { destroy_has_many_association :feature_enrollments }
  has_many :feature_enrollments, as: :enrollee, dependent: :destroy

  has_one :interaction_setting, dependent: :destroy

  has_many :reviews, class_name: "PullRequestReview"
  has_many :reviewed_files, class_name: "UserReviewedFile"

  has_many :repository_recommendation_dismissals

  before_destroy { destroy_has_many_association :sessions }
  has_many :sessions, class_name: "UserSession", dependent: :destroy do
    def active
      all.select(&:active?)
    end

    def inactive
      all.reject(&:active?)
    end
  end

  has_one :cas_mapping, dependent: :destroy

  has_one :saml_session, class_name: "SAML::Session", inverse_of: :user, dependent: :destroy
  has_one :saml_mapping, dependent: :destroy

  before_destroy { destroy_has_many_association :external_identities }
  has_many :external_identities, dependent: :destroy
  has_many :external_identity_sessions, through: :sessions

  # Audit log export records where the user or organization is the subject
  before_destroy { destroy_has_many_association :audit_log_exports }
  has_many :audit_log_exports, as: :subject, dependent: :destroy
  # Audit log export git event records where the user or organization is the subject
  before_destroy { destroy_has_many_association :audit_log_git_event_exports }
  has_many :audit_log_git_event_exports, as: :subject, dependent: :destroy

  before_destroy { destroy_has_many_association :tos_acceptances }
  has_many :tos_acceptances, dependent: :destroy

  before_destroy { destroy_has_many_association :user_licenses }
  has_many :user_licenses, dependent: :destroy

  # Repositories you can write to that have been pushed to or created recently
  def recently_updated_member_repos
    member_repositories.order("repositories.pushed_at DESC, repositories.created_at DESC")
  end

  has_many :showcases, class_name: "Showcase::Collection", foreign_key: :owner_id

  before_destroy { destroy_has_many_association :newsletter_subscriptions }
  has_many :newsletter_subscriptions, dependent: :destroy

  # These associations only exist to get the :dependent => :destroy behavior, so
  # they’re private to prevent external callers from abusing them.
  before_destroy { destroy_has_many_association :organization_invitations }
  has_many :organization_invitations, foreign_key: :invitee_id, dependent: :destroy
  private :organization_invitations

  before_destroy { destroy_has_many_association :team_membership_requests }
  has_many :team_membership_requests, foreign_key: :requester_id, dependent: :destroy
  private :team_membership_requests

  before_destroy { destroy_has_many_association :survey_answers }
  has_many :survey_answers, dependent: :destroy
  before_destroy { destroy_has_many_association :survey_groups }
  has_many :survey_groups, dependent: :destroy

  # Public: CustomerAccount record if this User is being paid for by a Customer
  # (optional).
  has_one :customer_account, dependent: :destroy

  # Public: Customer that pays for this user (optional).
  has_one :customer, through: :customer_account, autosave: true

  has_many :authentication_records
  has_many :authenticated_devices

  # similar to :profile but this is primarily populated and used
  # when the user is about to make a financial transaction
  has_one :user_personal_profile, dependent: :destroy

  # Public: External asset status and usage for repositories this user owns.
  has_one :asset_status,
    -> { lfs },
    class_name: "Asset::Status",
    foreign_key: :owner_id,
    dependent: :destroy

  before_destroy { destroy_has_many_association :inbound_application_transfers }
  has_many :inbound_application_transfers,
    class_name: "OauthApplicationTransfer",
    foreign_key: "target_id",
    dependent: :destroy

  before_destroy { destroy_has_many_association :outbound_application_transfers }
  has_many :outbound_application_transfers,
    class_name: "OauthApplicationTransfer",
    foreign_key: "requester_id",
    dependent: :destroy

  before_destroy { destroy_has_many_association :inbound_integration_transfers }
  has_many :inbound_integration_transfers,
    class_name: "IntegrationTransfer",
    foreign_key: "target_id",
    dependent: :destroy

  before_destroy { destroy_has_many_association :outbound_integration_transfers }
  has_many :outbound_integration_transfers,
    class_name: "IntegrationTransfer",
    foreign_key: "requester_id",
    dependent: :destroy

  before_destroy { destroy_has_many_association :gpg_keys }
  has_many :gpg_keys, dependent: :destroy
  has_many :gpg_key_emails, through: :gpg_keys, source: :emails

  # Public: Integrations owned by this user/org
  before_destroy { destroy_has_many_association :integrations }
  has_many :integrations,
    dependent: :destroy,
    as: :owner

  # Public: Installations for an Integration
  before_destroy { destroy_has_many_association :integration_installations }
  has_many :integration_installations,
    as: :target,
    dependent: :destroy

  # Public: Installation requests for an Integration for this account.
  before_destroy { destroy_has_many_association :integration_installation_requests }
  has_many :integration_installation_requests,
    foreign_key: :target_id,
    dependent: :destroy

  # Public: Requests for integraiton installations.
  before_destroy { destroy_has_many_association :requested_integration_installations }
  has_many :requested_integration_installations,
    class_name: "IntegrationInstallationRequest",
    foreign_key: :requester_id,
    dependent: :destroy

  has_one :prerelease_agreement,
    -> { where(member_type: "User") },
    foreign_key: :member_id,
    as: :member,
    class_name: "PrereleaseProgramMember"

  has_many :packages, foreign_key: "owner_id", class_name: "Registry::Package"
  has_many :package_files, through: :packages

  has_and_belongs_to_many :companies

  has_one :legal_hold

  has_many :retired_namespaces, primary_key: "login", foreign_key: "owner_login"

  before_destroy { destroy_has_many_association :marketplace_order_previews }
  has_many :marketplace_order_previews, class_name: "Marketplace::OrderPreview", dependent: :destroy

  has_many :received_abuse_reports,
    class_name: "AbuseReport",
    foreign_key: :reported_user_id

  before_destroy { destroy_has_many_association :user_roles }
  has_many :user_roles, as: :actor, dependent: :destroy

  before_destroy { destroy_has_many_association :business_user_accounts }
  has_many :business_user_accounts, dependent: :destroy

  has_many :assignments, foreign_key: :assignee_id

  has_many :attachments, foreign_key: :attacher_id

  has_many :issue_events, foreign_key: :actor_id

  has_many :cross_references, foreign_key: :actor_id

  has_many :assigned_issues,
    class_name: "Issue",
    foreign_key: :assignee_id

  has_many :subjected_issue_event_details,
    class_name: "IssueEventDetail",
    as: :subject

  has_many :projects, as: :owner

  has_many :created_projects,
    class_name: "Project",
    foreign_key: :creator_id

  has_many :project_cards, foreign_key: :creator_id

  has_many :pull_request_reviews

  has_many :pull_request_review_comments

  before_destroy { delete_has_many_association(:review_requests) }
  has_many :review_requests, as: :reviewer

  before_destroy { destroy_has_many_association :two_factor_recovery_requests }
  has_many :two_factor_recovery_requests, dependent: :destroy

  has_many :targeted_attribution_invitations,
    class_name: "AttributionInvitation",
    foreign_key: :target_id

  has_many :codespaces,
           foreign_key: :owner_id # dependent codespaces will be deleted by codespace_plans deletion


  has_many :codespace_plans, class_name: "Codespaces::Plan", as: :owner
  destroy_dependents_in_background :codespace_plans

  has_many :metered_usage_exports,
    class_name: "Billing::MeteredUsageExport",
    as: :billable_owner

  before_destroy { destroy_has_many_association :sent_successor_invitations }
  has_many :sent_successor_invitations, class_name: "SuccessorInvitation", foreign_key: :inviter_id, dependent: :destroy

  before_destroy { destroy_has_many_association :received_successor_invitations }
  has_many :received_successor_invitations, class_name: "SuccessorInvitation", foreign_key: :invitee_id, dependent: :destroy

  before_destroy { destroy_has_many_association :successor_invitations }
  has_many :successor_invitations, as: :target, dependent: :destroy

  def reload
    @repository_counts = nil

    # Used to memoize the membership_via_org_ids method in business_dependency.rb
    remove_instance_variable(:@membership_via_org_ids) if defined?(@membership_via_org_ids)
    super
  end

  def change_owner_of!(project:, creator:, old_owner:)
    # On the off chance someone is transferring a note-only or empty project
    # from an Organization to a User, we need to clear the abilities out to
    # avoid transferring teams that don't exist on the User account
    Ability.clear(project) if old_owner.is_a?(Organization)
  end

  # Internal: an abstract collection, for the sub-resources the user
  # available for abilities
  def resources
    User::Resources.new(self)
  end

  # Internal: an abstract collection, for the sub-resources of repositories
  # available for abilities
  def repository_resources
    User::RepoCollection.new(self)
  end

  def member_repositories
    ids = Ability.where(
        subject_type: "Repository",
        actor_id: id,
        actor_type: "User",
        priority: Ability.priorities[:direct],
    ).pluck(:subject_id)
    Repository.active.where(id: ids)
  end

  def outside_collaborator_repositories(business:, on_repositories_with_visibility: [:public, :private])
    repository_visibility = on_repositories_with_visibility.map do |visibility|
      { public: 1, private: 0 }[visibility]
    end.compact

    permission_cache_key = [
      "user_outside_collaborator_repositories",
      id, business.id, on_repositories_with_visibility
    ]

    PermissionCache.fetch permission_cache_key do
      Repository.active.
        where(public: repository_visibility).
        where(id: associated_repository_ids(min_action: :read, including: [:direct])).
        where(organization_id: business.organization_ids - organization_ids)
    end
  end

  # Provide a reason why a hubber's stafftools page was viewed
  # Gives a viewing user one hour to make requests on the viewed hubber's stafftools page
  def set_hubber_access_reason(viewing, reason)
    GitHub.kv.set("user.hubber_access_reason_provided.#{self.id}viewing#{viewing.id}", "true", expires: 1.hour.from_now)
    auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(self)
    payload = auditing_actor.merge(
      user: viewing,
      viewing_reason: reason,
    )
    GitHub.instrument("staff.hubber_access_reason_provided", payload)
  end

  def hubber_access_reason_provided?(viewing)
    GitHub.kv.get("user.hubber_access_reason_provided.#{self.id}viewing#{viewing.id}").value { nil }
  end

  # Public: Was this user created in the last 24 hours (default)?
  def recently_created?(since = 24.hours.ago)
    created_at > since
  end

  def recently_updated_repos(limit: nil)
    owned, member = recently_updated_owned_repos.limit(limit), recently_updated_member_repos.limit(limit)
    repos = (owned + member).sort_by { |r| -(r.try(:pushed_at) || r.created_at).to_i }
    limit.nil?? repos : repos.first(limit)
  end

  def all_repositories_count
    repositories.size + member_repositories.size
  end

  def all_repositories_by_name
    owned, member = recently_updated_owned_repos, recently_updated_member_repos
    (owned + member).sort_by { |r| r.name_with_owner.downcase }
  end

  def all_pages_repositories
    all_repositories_by_name.reject { |r| !r.has_gh_pages? }
  end

  # Rebuild all pages sites belonging to this user.
  #
  # except - Repositories to skip.
  #
  # Returns nothing.
  def rebuild_pages_repositories(*except)
    repos = all_pages_repositories - except
    repos.each { |r| r.rebuild_pages }
  end

  # Find this user's non-cnamed pages
  # and make sure they have valid https_redirect values.
  #
  # Returns nothing.
  def propagate_https_redirect
    recently_updated_owned_repos.each do |repo|
      if (page = repo.page) && page.cname.nil?
        page.set_https_redirect
        page.save if page.https_redirect_changed?
      end
    end
  end

  def visible_starred_repositories
    starred_repositories.where(
      ["repositories.public = ? OR repositories.id IN (?)",
       true, associated_repository_ids])
  end

  has_many :followings,
    after_add: :synchronize_search_index,
    after_remove: :synchronize_search_index
  has_many :following,
    through: :followings
  has_many :followeds,
    class_name: "Following", foreign_key: "following_id"
  has_many :followers,
    through: :followeds, source: :user

  has_many :pull_requests
  before_destroy -> { destroy_has_many_association(:u2f_registrations) }
  has_many :u2f_registrations,    dependent: :destroy
  has_one  :webauthn_user_handle, dependent: :destroy
  before_destroy -> { destroy_has_many_association(:public_keys) }
  has_many :public_keys,          dependent: :destroy, extend: PublicKey::CreationExtension
  before_destroy -> { destroy_has_many_association(:commit_comments) }
  has_many :commit_comments,      dependent: :destroy do
    def public
      joins(:repository).where(repositories: { public: true })
    end
  end
  has_one  :profile,             dependent: :destroy
  has_one  :interaction,         dependent: :destroy

  before_destroy -> { destroy_has_many_association(:issues) }, if: :spammy?
  has_many :issues do
    def public
      joins(:repository).where(repositories: { public: true })
    end
  end
  before_destroy -> { destroy_has_many_association(:issue_comments) }, if: :spammy?
  has_many :issue_comments do
    def public
      joins(:repository).where(repositories: { public: true })
    end
  end
  has_many :discussions
  has_many :discussion_comments

  has_many :releases, foreign_key: :author_id

  has_many :gists
  has_many :gist_comments do
    def public
      joins(:gist).where(gists: { public: true })
    end
  end

  has_many :gist_forks,
    -> { where("gists.parent_id IS NOT NULL AND gists.delete_flag = ?", false) },
    class_name: "Gist",
    foreign_key: :user_id

  has_many :gist_stars

  has_many :starred_gists,
    -> { where(delete_flag: false) },
    through: :gist_stars,
    source: :gist

  before_destroy { delete_has_many_association(:email_roles) }
  has_many :email_roles, dependent: :delete_all

  before_destroy { delete_has_many_association(:emails) }
  has_many :emails, dependent: :delete_all, class_name: "UserEmail"

  has_one :primary_user_email_role,
    -> { where(role: "primary") },
    class_name: "EmailRole"

  has_one :primary_user_email,
    through: :primary_user_email_role,
    source: :email

  has_one :backup_user_email_role,
    -> { where(role: "backup") },
    class_name: "EmailRole"

  has_one :backup_user_email,
    through: :backup_user_email_role,
    source: :email

  has_one :user_vertical, dependent: :destroy

  before_destroy { destroy_has_many_association(:reactions) }
  has_many :reactions, dependent: :destroy
  before_destroy { destroy_has_many_association(:discussion_reactions) }
  has_many :discussion_reactions, dependent: :destroy
  before_destroy { destroy_has_many_association(:discussion_comment_reactions) }
  has_many :discussion_comment_reactions, dependent: :destroy

  before_destroy { destroy_has_many_association(:dashboard_pins) }
  has_many :dashboard_pins, -> { ordered_by_position }, class_name: "UserDashboardPin", dependent: :destroy

  has_many :profile_pins, -> { ordered_by_position }, through: :profile

  has_many :pinned_repositories, -> { public_scope.active }, through: :profile_pins,
     source: :pinned_item, source_type: "Repository", split: true

  has_many :user_seen_features

  before_destroy { destroy_has_many_association(:advisory_credits) }
  has_many :advisory_credits, -> { order(id: :asc) },
    foreign_key: "recipient_id", dependent: :destroy

  BATCH_SIZE = 1000

  def verified_keys
    public_keys.where("verified_at is not null")
  end

  # Maps email addresses to Users
  #
  # emails - An Array of String email addresses.
  #
  # Returns a Hash like {"foo@bar.com" => <User>, ...}
  def self.find_by_emails(emails)  # rubocop:disable GitHub/FindByDef
    emails = Array(emails).select { |e| GitHub::UTF8.valid_email?(e) }

    bot_emails, user_emails = emails.partition { |e| UserEmail.belongs_to_a_bot?(e) }
    bot_hash = Bot.find_by_emails(bot_emails)

    stealth_emails = User.user_id_and_email_from_stealth_emails(user_emails)
    users = User.select("users.*, user_emails.email as user_email, user_emails.state as user_email_state").
            where("user_emails.email IN (?)", user_emails).
            joins("INNER JOIN user_emails ON users.id = user_emails.user_id")
    user_email_hash = Hash[users.map { |user| [user.user_email.downcase, user] }]

    user_email_hash.merge(stealth_emails_hash(stealth_emails)).merge(bot_hash)
  end

  # Finds the stealth ides if they exist in the pass in emails
  # and pulls out the user id from each
  #
  # emails - An Array of String email addresses.
  #
  # Returns an Array like [{id: 1, email: "1+bob@users.noreply.github.com"}, ...]
  def self.user_id_and_email_from_stealth_emails(emails)
    emails = Array(emails)

    logins_by_old_email = emails.each_with_object({}) do |email, hash|
      if match = StealthEmail::OLD_STEALTH_EMAIL_REGEX.match(email)
        hash[email] = match[1]
      end
    end

    old_email_logins = logins_by_old_email.values
    users_by_login = old_email_logins.empty? ? {} : User.where(login: old_email_logins).index_by do |user|
      user.login.downcase
    end

    users_by_old_email = logins_by_old_email.each_with_object({}) do |(email, login), hash|
      # The login from the email may not be of the same case as that in the User
      # record, so we compare them downcased.
      hash[email] = users_by_login[login.downcase]
    end

    emails.map do |e|
      if e =~ StealthEmail::STEALTH_EMAIL_REGEX
        {id: $1.to_i, email: e}
      elsif e =~ StealthEmail::OLD_STEALTH_EMAIL_REGEX
        if user = users_by_old_email[e]
          {id: user.id, email: e}
        end
      end
    end.compact
  end

  # Finds the users associated with each id+login@domain stealth emails
  # and returns it in the format expected by the find_by_emails return
  #
  # stealth_emails - An Array of Hashes of stealth email information
  #
  # Returns a Hash like {"1+bob@users.noreply.github.com" => <User>, ...}
  def self.stealth_emails_hash(stealth_emails)
    email_hash = {}
    if stealth_emails.any?
      # these are always verified
      users = User.select("users.*, 'verified' as user_email_state").
              where("users.id IN (?) and type != 'Organization'", stealth_emails.map { |item| item[:id] })
      if users.any?
        users.each do |user|
          found_stealth_emails = stealth_emails.select { |s| s[:id] == user.id }

          found_stealth_emails.each do |stealth_email|
            email_hash[stealth_email[:email]] = user
          end
        end
      end
    end
    email_hash.compact
  end

  # Map commit objects to Users through the committer's email address.
  #
  # commits   - An Array of Commits.
  # user_type - Symbol defining which user property is used: :author or
  #             :committer.
  #
  # Returns a Hash like {"foo@bar.com" => <User>, ...}
  def self.find_by_commits(commits, user_type = :author)  # rubocop:disable GitHub/FindByDef
    emails = commits.inject([]) do |all, commit|
      if user_type != :author
        all << commit.committer_email
      end
      if user_type != :committer
        all << commit.author_email
      end
      all
    end
    emails.delete_if do |em|
      em = em.dup.to_s
      em.downcase!
      em.strip!
      em.blank?
    end
    emails.uniq!

    return {} if emails.blank?
    find_by_emails(emails)
  end

  # Batch loads all User objects for the author and/or committer emails
  # in the commits Array, and assigns the user attributes for each.
  #
  # commits - Array of Commit objects
  #
  # Returns Array of commits, but with author and committer users filled in.
  def self.attach_to_commits(commits)
    emails = Set.new
    commits.each do |commit|
      emails << commit.author_email    if !commit.author_email.blank?
      emails << commit.committer_email if !commit.committer_email.blank?
    end
    users = find_by_emails(emails.to_a)
    commits.each do |commit|
      author, committer = commit.author_email, commit.committer_email
      commit.author     = (users[author.downcase] if author)
      commit.committer  = (users[committer.downcase] if committer)
    end
  end

  # Loads all Users who are currently considered dormant based on User#dormant?,
  # aren't suspended, and are not staff. This is used primarily in Enterprise
  # to generate reports that are used to determine which users are good candidates
  # for being suspended to free up seats.
  #
  # Returns Array of user objects.
  def self.dormant_users(threshold = GitHub.dormancy_threshold)
    conditions = [
      "login != ?
        AND type = 'User'
        AND suspended_at IS NULL
        AND IFNULL(gh_role, '') != 'staff'",
      GitHub.ghost_user_login,
    ]

    users = []

    # Using #find here rather than #find_each because you can't specify a sort
    # order with #find_each.
    User.where(conditions).order("login ASC").to_a.each do |user|
      users << user if user.dormant?(threshold)
    end

    users
  end

  # Queues a background job to suspend all dormant users. This is currently
  # only allowed under Enterprise.
  #
  # Returns nothing (false if run outside of Enterprise mode).
  def self.suspend_dormant_users(threshold = GitHub.dormancy_threshold)
    # running this on .com would be a big mistake
    return false unless GitHub.enterprise?

    UserSuspendDormantJob.perform_later(threshold.to_i)
  end

  def self.employees
    employees = Team.with_org_name_and_slug("github", "Employees")
    return [] if GitHub.enterprise? || employees.blank?

    employees.members.
      reject { |user| GitHub.hidden_teamster?(user) }
  end

  def self.interns
    interns = Team.with_org_name_and_slug("github", "Interns")
    return [] if GitHub.enterprise? || interns.blank?

    interns.members - employees
  end

  # Internal: users who have been inactive since signup
  #   signup_date - a Time object (e.g. 7.days.ago)
  #
  # Returns an Array of User objects
  def self.inactive_after_signup(signup_date)
    signup_from    = signup_date.beginning_of_day
    signup_to      = signup_date.end_of_day

    sql = github_sql.new(<<-SQL, signup_from: signup_from, signup_to: signup_to)
      SELECT users.* FROM users
      LEFT JOIN
        interactions ON interactions.user_id = users.id
      WHERE
      ( interactions.last_active_at is null OR
        interactions.last_active_at < :signup_to )
      AND
        users.created_at BETWEEN cast(:signup_from as datetime) AND cast(:signup_to as datetime)
      AND
        users.type = 'User'
    SQL

    sql.models(User)
  end

  # Used by the API, GraphQL, and views to retrieve the profile email.
  # For users, the method respects the user's email privacy preferences, and
  # will return `nil` if the user has asked us not to publish their email.
  # Organization's profile emails are always considered public.
  #
  # logged_in - is the current viewing user logged in? Users are required to log
  #             in to see another user's email, unless in Enterprise
  #
  # Returns the user's profile email, as a string, or nil
  def publicly_visible_email(logged_in: false)
    return profile_email.presence if organization?
    return unless logged_in || GitHub.enterprise?
    profile_email.presence if existing_primary_email_role && existing_primary_email_role.public?
  end

  # Public: Determines if the user's email is a valid email address.
  #
  # Returns true if valid, false otherwise.
  def self.valid_email?(email)
    return false if email.blank?
    return false if email.is_a?(User)
    (email =~ User::EMAIL_REGEX).present?
  end

  def safe_profile_name
    profile_name.blank? ? login : profile_name
  end

  # Public: check if user has a personal profile
  #
  # Returns boolean.
  def has_personal_profile?
    user_personal_profile.present?
  end

  # Set the User's primary email
  def email=(new_email)
    return if new_email.blank?
    add_email(new_email.to_s, is_primary: true)
  end

  # Internal: The user's primary email address.
  #
  # See also `outbound_email` and `git_author_email` -
  # client code should prefer those methods
  def email
    @primary_email ||= primary_user_email.try(:email)
  end


  # Internal: The user's backup email address. This email can be used for
  # password resets.
  def backup_email
    backup_user_email.try(:email)
  end

  # Public: The user's 'public facing' email address
  #
  # Use this for any sort of outbound email from GitHub,
  # as it will ensure that stealth emails get used if the user wants them
  def outbound_email
    if use_stealth_email?
      stealth_email.to_s
    else
      email
    end
  end

  # Public: Whether the user has a pro plan badge on their profile/hovercard
  def has_pro_plan_badge?
    return false if employee?

    can_have_pro_badge? && profile_settings.pro_badge_enabled?
  end

  # Public: The email to attribute commits to when a user importing a repository
  # maps an incoming commit author to an existing user on GitHub. Used instead
  # of git_author_email because the attribution is made by the user importing
  # the repository and not necessarily the user that is being attributed.
  #
  # Returns a String.
  def public_attribution_email
    if profile && profile.email.present?
      profile.email
    else
      anonymous_user_email
    end
  end

  # Public: did the user associate the specified email address
  # with this account?
  #
  # Returns Boolean
  def is_known_email?(email_address)
    !!emails.user_entered_emails.find_by_email(email_address)
  end

  # The primary email address with name part as a simple hash.
  def email_info
    {
      "name"    => safe_profile_name,
      "address" => outbound_email,
    }
  end

  # Extract git author info from an object representing a person.
  #
  # person - The person whose git author info we want. Can either be a
  #          User object or a hash with :name and :email keys.
  #
  # Returns an Array that looks like [name, email].
  def self.git_author_info(person)
    if person.is_a?(User)
      [person.git_author_name, person.git_author_email]
    else
      [person[:name], person[:email]]
    end
  end

  def gravatar_id
    return unless email = gravatar_email
    gid = email.to_s.strip.downcase.to_md5
    gid == User::AvatarList::BLANK_GRAVATAR_ID ? nil : gid
  end

  def identicon_id
    @identicon_id ||= id.to_s.to_md5
  end

  # Internal: Set the gravatar email for the
  # user if it is already set.
  def set_gravatar_email(email)
    if emails.size == 1 && gravatar_email.nil?
      self.gravatar_email = email
    end
  end

  # Sets the gravatar email and the gravatar_id cache, as well as the
  # various view caches.
  #
  # email - The String email to set as the Gravatar email.
  #
  # Returns nothing.
  def gravatar_email=(new_email)
    new_email = new_email.to_s.strip
    write_attribute(:gravatar_email, new_email.blank? ? nil : new_email)
  end

  def primary_avatar_path
    @primary_avatar_path ||= "/u/#{id}"
  end

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the ghost user.
  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

  # Public: Adds the given email to the user's email collection.  If this is
  # the user's first email, make sure it becomes their primary email.
  #
  # email - String email address.
  #
  # Returns the (hopefully) created UserEmail...
  #   NOTE: It could be an invalid email that is not persisted!
  def add_email(email, options = {})
    options.reverse_merge! is_primary: false, actor: self,
      rebuild_contributions: true

    added = self.emails.build(email: email)

    if options[:is_primary] || emails.size == 1
      set_primary_email!(added) # also saves self, so will save the built user_email from that
    else
      GitHub.dogstats.increment "user", tags: ["action:add_email", "type:non_primary"]
      save
    end

    # Don't instrument when new users are validating on the signup form
    # Don't instrument invalid emails that won't be saved
    if !new_record? && valid?
      instrument :add_email, actor: options[:actor], email: email, note: email
      GitHub.dogstats.increment "contributions", tags: ["type:build_add_email"]
      rebuild_contributions if options[:rebuild_contributions]

      GlobalInstrumenter.instrument "user.add_email", {
        user: self,
        actor: options[:actor],
        primary_email: primary_user_email,
        added_email: added,
      }

      # This is an unfortunate hack that is need to ensure tests pass. Tests
      # Do things technically impossible, i.e. deleting all email addresses,
      # causing errors in this code. We don't alert on actions taken by
      # users operating on other users' accounts (e.g. stafftools actions)
      if emails.size > 1 && self == options[:actor]
        AccountMailer.email_address_added(added).deliver_later
      end
    end

    added
  end

  # Public: Remove a given email from this user
  #
  # email - String email address
  # options - Hash of options
  #   actor - the User doing the removal; defaults to self
  #
  # Returns the UserEmail requested for deletion
  # Returns false if the removal fails
  def remove_email(email, options = {})
    errors.delete(:base)
    options.reverse_merge! actor: self

    to_remove = emails.find_by_email(email.to_s)
    unless to_remove
      errors.add :base, "Email not found."
      return false
    end
    if to_remove.last_email?
      errors.add :base, "Last email cannot be deleted."
      return false
    end

    if to_remove.sponsors_membership.present?
      message = [
        "Cannot delete email because it is being used as your contact email for GitHub Sponsors.",
      ]

      if sponsors_membership.accepted?
        message << "Please change the email in your GitHub Sponsors settings and try again."
      else
        message << "Please change the email for your GitHub Sponsors waitlist application and try again."
      end

      errors.add(:base, message.join(" "))
      return false
    end

    if to_remove.primary_role?
      new_primary = emails.notifiable.excluding_ids([to_remove, backup_user_email]).first
      unless new_primary
        if GitHub.email_verification_enabled?
          errors.add :base, "Cannot delete verified primary email without another verified email registered."
        else
          errors.add :base, "Cannot delete primary email without another email registered."
        end
        return false
      end

      status = set_primary_email(new_primary)
      unless status.success?
        errors.add :base, status.error
        return false
      end
    end

    emails.destroy(to_remove)

    GitHub.newsies.get_and_update_settings(self) do |settings|
      settings.clear_unverified_emails(notifiable_emails, primary_user_email.to_s)
    end

    instrument :remove_email, email: to_remove.to_s, actor: options[:actor], note: to_remove.to_s
    # Don't alert on actions taken on another's behalf (e.g. stafftools)
    if self == options[:actor]
      AccountMailer.email_address_removed(self, to_remove.to_s).deliver_later
    end
    GitHub.dogstats.increment "user", tags: ["action:remove_email"]
    GitHub.dogstats.increment "contributions", tags: ["action:build_remove_email"]
    rebuild_contributions

    to_remove.destroyed? ? to_remove : false
  end

  # Internal: Return the existing primary email role.
  #
  # Returns nil or a EmailRole
  def existing_primary_email_role
    email_roles.primary.first
  end

  # Internal: Return the existing primary email role.
  #
  # Returns nil or a EmailRole
  def existing_backup_email_role
    email_roles.backup.first
  end

  # Internal: Check if there are existing email roles
  #
  # Validations for roles run regardless of existing email roles,
  # and this will prevent those validations from running if empty
  def no_existing_email_roles?
    return true if email_roles.count == 0
  end

  # Public: Set the given UserEmail as the user's primary email address.
  #
  # email - The email address that should be used as the user's primary email address.
  #
  # Returns a SetPrimaryEmailStatus object, which can be asked about `success?` or `failure?`.
  def set_primary_email(email)
    previous_primary = primary_user_email
    set_primary_email_status = User::SetPrimaryEmailStatus::SUCCESS

    transaction do
      # When a user only wishes to have password resets sent to their primary
      # address, we track that by setting their backup to match their primary
      # email address. So, we need to update it if updating their primary
      # email. Because of how `password_reset_with_primary_email_only?` is
      # implemented, it is critical that we check
      # `password_reset_with_primary_email_only?` and call
      # `set_backup_email(email)` before updating the primary email role
      # below.
      set_backup_email(email) if password_reset_with_primary_email_only?
      primary_role = existing_primary_email_role || email.email_roles.build(role: "primary", user: self)
      primary_role.email = email
      cache_primary_email(email)

      unless new_record?
        GitHub.dogstats.increment "user_email", tags: ["action:save", "type:primary"]
        set_gravatar_email(email)

        [self, email, primary_role].each_with_index do |model, index|
          unless model.save
            set_primary_email_status = User::SetPrimaryEmailStatus.new(invalid_model: model)
            raise ActiveRecord::Rollback
          end
        end
      end
    end

    if set_primary_email_status.success?
      if NewsletterPreference.marketing?(user: self)
        update_mailchimp_email(new_email: email, old_email: previous_primary)
      else
        send_signup_confirmation(new_email: email, old_email: previous_primary)
      end

      reload_primary_user_email
    end

    set_primary_email_status
  end

  # Public: Set the given UserEmail as the user's primary email address, raising an error if the
  # operation fails.
  #
  # email - The email address that should be used as the user's primary email address.
  #
  # Returns the new primary UserEmail model.
  def set_primary_email!(email)
    status = set_primary_email(email)
    status.raise_on_error!
    primary_user_email
  end

  # Public: Set the Email's role to backup.
  #
  # email - The email address that should be used as the user's backup email
  # address.
  #
  # Returns the backup UserEmail.
  def set_backup_email(email)
    backup_role = existing_backup_email_role ||
      email.email_roles.build(role: "backup", user: self)
    backup_role.email = email
    backup_role.save!
    GitHub.dogstats.increment "user_email", tags: ["action:save", "type:backup"]

    reload_backup_user_email
  end

  # Public: Clear any existing Email backup role. This results in our legacy
  # behavior of allowing all verified emails (if any are verified) to be used
  # password resets and all unverified emails for password resets if no emails
  # are verified.
  #
  # Returns nothing.
  def allow_password_reset_with_any_email
    existing_backup_email_role.try(:destroy)
    reload_backup_user_email
    nil
  end

  # Public: Disables any backup email address. Only the primary email address
  # can be used for password resets once the backup email address is disabled.
  #
  # Note: This is implemented by setting the backup email to the user's
  # primary email address. This is kind of quirky, but it dramatically
  # simplifies the state necessary to implement such a feature without adding
  # additional state to the DB.
  #
  # Returns nothing.
  def allow_password_reset_with_primary_email_only
    set_backup_email(primary_user_email)
    nil
  end

  # Internal: Update the user's email address in MailChimp.
  # This method is also used for subscribing a user the first time.
  #
  # new_email - UserEmail
  # old_email - UserEmail. Must match what's stored in MailChimp.
  #
  # Returns true if job is enqueued, nil otherwise.
  def update_mailchimp_email(new_email:, old_email:)
    return unless GitHub.mailchimp_enabled?
    return unless old_email && new_email

    # Subscribe the new email being set as primary.
    MailchimpSubscribeJob.perform_later(new_email.id)

    # Only unsubscribe old email when changing primary emails;
    # for users verifying an email address for the first time,
    # the new_email and old_email will be the same.
    if new_email.email != old_email.email
      MailchimpUnsubscribeJob.perform_later(self.id, old_email.email)
    end
  end

  # Internal: Send a one-time welcome email to emails with a transactional
  # email preference set in order to confirm their signup.
  #
  # new_email - UserEmail
  # old_email - UserEmail.
  #
  # Returns true if job is enqueued, nil otherwise.
  def send_signup_confirmation(new_email:, old_email:)
    return unless GitHub.mailchimp_enabled?
    return unless old_email && new_email
    return if new_record?

    # The new_email and old_email will be the same for users verifying
    # an email address for the first time.
    if new_email.email == old_email.email
      UserSignupConfirmationJob.perform_later(new_email.id)
    end
  end

  # Public: Fixes an account which does not have a primary email set
  def repair_primary_email
    if has_primary_email?
      errors.add(:base, "primary email already set")
      return false
    end

    if emails.empty?
      errors.add(:base, "user has no emails set")
      return false
    end

    set_primary_email! emails.first
  end

  # Public: Returns a String email address for support to use when contacting
  # this user via Halp
  def email_for_halp
    email.to_s
  end

  # Public: Returns a Set of notifiable String email addresses for this user.
  def notifiable_emails
    emails.notifiable.map { |e| e.to_s.downcase }.to_set
  end

  # Internal
  def cache_primary_email(email)
    @primary_email = email.to_s
  end

  # Internal: The gravatar email defaults to their primary email. Fired via
  # before_save callback.
  #
  # We actually only want this to happen during creation, but
  # serialized_attributes get added to the data field during a before_save
  # callback and before_create gets called after that. If we called
  # set_initial_gravatar_email in a before_create callback, the gravatar_id
  # (set by #gravatar_email=) attribute wouldn't be persisted.
  #
  # TODO: This is terrible and should be refactored.
  def set_initial_gravatar_email
    return unless new_record?
    self.gravatar_email = email
  end

  # Internal: Default primary email to the first email. Fired via callback.
  #
  # NOTE: does NOT call set_primary_email -- probably not a good thing
  def set_initial_primary_email
    if email = emails.first
      cache_primary_email(email)
    end
    true
  end

  # Public: Set user's diff preference.
  #
  # view - :unified or :split Symbol
  #
  # Returns nothing.
  def set_diff_preference(view)
    raise ArgumentError, "unknown view type: #{view.inspect}" unless [:unified, :split].include?(view)
    new_split_preferred = (view == :split)

    if self.split_diff_preferred != new_split_preferred
      ActiveRecord::Base.connected_to(role: :writing) do
        update_column :split_diff_preferred, new_split_preferred
      end
    end

    nil
  end

  # Retrieves the user's cached preference for the default query against their
  # notifications inbox at https://github.com/notifications/beta.
  #
  # Returns a String for the user's preferred query or nil if the user has none.
  def preferred_notifications_query
    GitHub.kv.get(preferred_notifications_query_key).value { nil }
  end

  # Caches the user's preferred query string for their notifications inbox at
  # https://github.com/notifications/beta.
  #
  # parsed_query - Search::Queries::NotificationsQuery
  #
  # Returns nil.
  def set_preferred_notifications_query(query_string)
    ActiveRecord::Base.connected_to(role: :writing) do
      if query_string.blank?
        GitHub.kv.del(preferred_notifications_query_key)
      else
        GitHub.kv.set(preferred_notifications_query_key, GitHub::SQL::BINARY(query_string))
      end
    end

    nil
  end

  # Checks to see if the user prefers to view notification inbox grouped by list.
  #
  # Returns boolean.
  def prefer_notifications_grouped_by_list?
    GitHub.kv.exists(notifications_group_by_list_key).value!
  end

  # Sets that the user prefers to view their notification inbox grouped by list.
  #
  # Returns nil.
  def set_prefers_notifications_group_by_list_view
    GitHub.kv.set(notifications_group_by_list_key, "true")
  end

  # Unsets that the user prefers to view their notification inbox grouped by list.
  #
  # Returns nil.
  def unset_prefers_notifications_group_by_list_view
    GitHub.kv.del(notifications_group_by_list_key)
  end

  # Checks to see if the user has dismissed the suggestion to unwatch repositories
  #
  # Returns boolean.
  def dismissed_unwatch_suggestions?
    GitHub.kv.get(notifications_dismissed_unwatch_suggestions_key).value { false }
  end

  # Sets that the user dismissal of unwatch notifications suggestions
  #
  # Returns nil.
  def set_dismissed_unwatch_suggestions
    GitHub.kv.set(notifications_dismissed_unwatch_suggestions_key, "true", expires: 30.days.from_now)
  end

  def public_gists
    gists.alive.are_public
  end

  def private_gists
    gists.alive.are_secret
  end

  def self.find_by_email(email)  # rubocop:disable GitHub/FindByDef
    return nil unless email
    return nil unless GitHub::UTF8.valid_email?(email)

    if UserEmail.belongs_to_a_bot?(email)
      Bot.find_by_email(email)
    elsif email =~ StealthEmail::STEALTH_EMAIL_REGEX
      User.find_by_id($1)
    elsif email =~ StealthEmail::OLD_STEALTH_EMAIL_REGEX
      User.find_by_login($1)
    else
      User.joins(:emails).where("user_emails.email" => email).readonly(false).first
    end
  end

  def self.find_by_login(login)  # rubocop:disable GitHub/FindByDef
    return nil unless login && GitHub::UTF8.valid_unicode3?(login.to_s)
    where(login: login.to_s).first
  end

  def self.find_by_name(login)  # rubocop:disable GitHub/FindByDef
    find_by_login(login)
  end

  def self.find_by_login_or_email(login)  # rubocop:disable GitHub/FindByDef
    if login.nil?
      nil
    elsif login["@"]
      find_by_email(login)
    else
      find_by_login(login)
    end
  end

  # Public: Takes a user or user identifier and returns the actual user object.
  #
  # user_or_identifier - The User or their login/email.
  #
  # Returns a User object or nil if no User exists.
  def self.reify(user_or_identifier)
    return user_or_identifier if user_or_identifier.is_a?(User)

    find_by_login_or_email(user_or_identifier)
  end

  has_many :staff_notes,     as: :notable
  has_many :own_staff_notes, class_name: "StaffNote"

  attr_accessor :password, :old_password, :destroying
  alias :being_destroyed? :destroying

  # Public: Get a user-friendly error message about what's wrong with this User's login.
  #
  # Returns a String.
  def login_error_message
    login_errors = errors[:login].map do |error|
      if error == NOT_UNIQUE_LOGIN_MESSAGE
        "#{login.strip} #{error}" # repeat the duplicate login when saying it's already taken
      else
        error
      end
    end
    field_name = self.class.human_attribute_name(:login)
    "#{field_name} #{login_errors.join(". #{field_name} ")}."
  end

  # Public: Clear any AuthenticationLimits associated with this user's login.
  def clear_auth_limit
    AuthenticationLimit.clear_data(AuthenticationLimit.all_data_for_login(login))
  end

  # Public: Checks the login against a known list of hardcoded logins
  # we want to reserve (such as /blog or /explore), as well as logins reserved by staff.
  # To add to the list, Go to https://admin.github.com/stafftools/reserved_logins
  #
  # Returns boolean.
  def login_reserved?
    ReservedLogin.reserved? login.downcase
  end
  alias_method :login_blacklisted?, :login_reserved?

  # Validation: Ensures the login is not blacklisted.
  #
  # Returns nothing.
  def denied_logins
    return if login.blank?
    if login_blacklisted?
      errors.add(:login, "'#{login}' is unavailable")
      return false
    end
  end

  # Validation: Ensures the uniquness of the login with a faster query than
  # validates_uniqueness_of :login. It doesn't use LOWER(user.login).
  #
  # Returns nothing.
  def uniqueness_of_login
    return true unless will_save_change_to_login?

    if user = User.find_by_login(login)
      return if user == self
      errors.add(:login, NOT_UNIQUE_LOGIN_MESSAGE)
    end
  end

  # Validation: Ensures the uniquness of the email by just trying to find a
  # user that loads with it.
  #
  # Returns nothing.
  def uniqueness_of_email
    if user = User.find_by_email(email)
      return if user == self
      errors.add(:email, "#{email} is already taken")
    end
  end

  # Validation: Prevents users from being created for Enterprise installs
  # when there are no available seats.
  #
  # This is shown to the user, not the administator. Direct users to contact
  # support.
  #
  # Returns nothing.
  def enforce_seat_limit(message = nil)
    return if !GitHub.enterprise? || !user? || login == GitHub.ghost_user_login
    if GitHub::Enterprise.license.reached_seat_limit?
      errors.add :base,
        message ? message : "Could not create your account. Contact your system administrator."
    end
  end

  def self.with_oauth_token(token)
    return if token.blank?
    with_oauth_hashed_token(OauthAccess.hash_token(token))
  end

  def self.with_oauth_hashed_token(hashed_token)
    return if hashed_token.blank?

    if (access = OauthAccess.with_active_token(hashed_token, hashed: true))
      return if !access.personal_access_token? && access.application.nil?

      if (user = access.user) && user.can_authenticate_via_oauth?
        access.bump
        user.oauth_access               = access
        user.set_scopes(access)
        user.oauth_application_id       = access.application_id
        user
      end
    end
  end

  # Internal: set the current oauth access scopes on the User
  # for cases when oauth scopes will be used for permission checks.
  #
  # access - An OauthAccess
  def set_scopes(access)
    unless access.application.is_a? Integration
      self.scopes = access.access_level
    end
  end

  # Attempts to fetch a group of logins in a single query round trip.
  #
  # logins - An Array of String logins.
  #
  # Returns an Array of User objects.
  def self.with_logins(*logins)
    where(login: logins.flatten).all
  end

  # Public: Retrieve this User's popular repositories, based on
  # stargazer count.
  #
  # Returns an Array of Repositories
  def popular_repositories
    PopularRepositories.new(self).repositories
  end

  def repository_counts
    @repository_counts ||= UserRepositoryCounts.new(self)
  end
  attr_writer :repository_counts

  # Unpublishes Pages on any private repos the user owns
  def unpublish_private_pages
    owned_private_repositories.each(&:unpublish_page)
  end

  # Does the user have more collaborators on private repos than allowed on free
  def over_repo_seat_limit?
    return false if organization?

    owned_private_repositories.any? do |repo|
      repo.filled_seats >
        GitHub::Plan.free.limit(:collaborators, visibility: :private, feature_flag: plan_override_feature_flag)
    end
  end

  def reset_private_protected_branches
    if !plan_supports?(:protected_branches, visibility: :private)
      owned_private_repositories.map(&:destroy_protected_branches)
    end
  end

  # Authenticates a User by their login name and unencrypted password, or
  # an OAuth access token and no password or 'x-oauth-basic'
  #
  # Returns the User or nil.
  def self.authenticate(login, password)
    return unless u = find_by_login_or_email(login)

    success, message = u.authenticated_by_password?(password)
    return success ? u : nil, message
  end

  def valid_login?(username)
    if username["@"]
      emails.find_by_email(username).present?
    else
      standardized = User.standardize_login(username)
      standardized.downcase == login.downcase
    end
  end

  # Convert an invalid login to a valid login. This is used to handle OmniAuth
  # logins, which can be a variety of Strings: things with special characters,
  # email addresses, and so on. Email addresses get their domain chopped off
  # the end.
  #
  # login - the possibly-invalid String login
  #
  # Returns a valid String.
  def self.standardize_login(login)
    login = login.split("\\").last # Windows Domains
    login = login.split("@").first # email addresses
    login.gsub(/[^a-z0-9-]/i, "-")
  end

  # Returns a new unpersisted User object with a random password.
  #
  # login - String representing the login for the new user
  # site_admin - Boolean indicating whether the new user should be a site admin
  # default_opts - Hash of params to provide when creating the User object
  #
  # Returns User.
  def self.new_with_random_password(login, site_admin = GitHub.enterprise_first_run?, default_opts = {})
    clean_login = standardize_login(login) unless login.blank?
    password = SecureRandom.hex(32)

    options = {
      raw_login: login,
      login: clean_login,
      password: password,
      password_confirmation: password,
    }

    options.merge!(default_opts)
    options[:gh_role] = "staff" if site_admin

    new(options)
  end

  # Creates a new User with a random password. Used by external authentication
  # to create users that do not use the github password field.
  #
  # login - String representing the login for the new user
  # site_admin - Boolean indicating whether the new user should be a site admin
  # default_opts - Hash of params to provide when creating the User object
  #
  # Returns a User object whether or not the user was successfully created.
  # Callers should check #valid? on the returned object.
  def self.create_with_random_password(login, site_admin = GitHub.enterprise_first_run?, default_opts = {})
    new_with_random_password(login, site_admin, default_opts).tap(&:save)
  end

  # Public: Log out all of the users sessions, including the current one.
  #
  # Do this when the user changes their password.
  #
  # reason - Why user is being logged out.
  #          See UserSession:VALID_REVOKE_REASONS for valid keys.
  #
  # Returns Array of revoked UserSessions.
  def revoke_active_sessions(reason)
    self.sessions.unrevoked.active.map do |session|
      # if necessary, devices will already be unverified prior to session
      # revocation.
      session.revoke(reason, unverify_device: false)
    end
  end

  # Public: Determine if the user has exceeded the active session limit.
  #
  # Returns true if the active session count exceeds the limit.
  def over_active_session_limit?
    sessions.user_facing.count > UserSession::LIMIT
  end

  # Public: Determine if the user has exceeded the active session limit.
  #
  # Returns true if the active session count exceeds the limit.
  def enforce_active_session_limit?
    return false unless GitHub.active_session_limit_enabled?
    sessions.user_facing.count > UserSession::LIMIT_ENFORCEMENT_THRESHOLD
  end

  def self.otp_authenticate(login, otp)
    if u = find_by_login_or_email(login)
      u.valid_otp?(otp) ? u : nil
    end
  end

  def valid_otp?(otp)
    return unless two_factor_authentication_enabled?
    case otp.to_s
    when TwoFactorCredential::OTP_REGEX
      two_factor_credential.verify_totp(otp, allow_reuse: false)
    end
  end

  # Public: Sets the protocol preference for this user.
  #
  # type     - String protocol name.
  # protocol - String protocol type.
  #
  #   user.set_protocol_preference "push", "ssh"
  #
  # Returns nothing.
  def set_protocol_preference(type, protocol)
    if !(protocols && protocols[type] == protocol)
      self.protocols = (protocols || {}).update(type => protocol)
    end
  end

  # Find all Organizations owned by this user that are not paying their bill
  #
  # Returns an array of disabled Organizations
  def disabled_orgs
    @disabled_orgs ||= organizations.select do |org|
      org.disabled? && org.paid_plan? && org.adminable_by?(self)
    end
  end

  # Checks if any of the paid Organizatinos this user owns have been locked
  #
  # Returns a Boolean
  def disabled_orgs?
    disabled_orgs.size > 0
  end

  def disabled_org?(org:)
    disabled_orgs.include?(org)
  end

  # Does this User own an Organization that is missing a billing_email?
  # Returns a Boolean.
  def owns_billingless_org?
    !!billingless_org
  end

  # Organization#billing_email is required, but some orgs don't have one.
  #
  # This method finds Organizations that are:
  #  1. on a paid plan
  #  2. blank billing_email
  #  3. owned by this User
  #
  # Returns the Organization if one is found, or nil if not.
  def billingless_org
    if billingless_org_id.present?
      @billingless_org ||= Organization.find_by_id(billingless_org_id)

      # check that it's still missing the email
      if @billingless_org && @billingless_org.billing_email.blank?
        @billingless_org
      end
    end
  end

  # Cached version of `billingless_org_id!`
  def billingless_org_id
    GitHub.cache.fetch("#{cache_key}:billingless_org_id", stats_key: "user.caching.ns-billingless-org-id") do
      billingless_org_id!
    end
  end

  # Caches an Organization ID for the `billingless_org` method.
  # Returns an Integer ID of an Organization if we found one.
  # Returns nil otherwise.
  def billingless_org_id!
    org = owned_organizations.detect do |o|
      o.billing_email.blank? && o.paid_plan?
    end
    org.id if org
  end


  # Organization#billing_email is required, but some orgs don't have one.
  #
  # Returns the Boolean if org is billingless
  def billingless_org?(org:)
    org.billing_email.blank? && org.paid_plan?
  end

  def path
    to_s
  end

  # The full author name used in git operations.
  #
  # First we attempt to use the safe profile name, and if
  # that does not have a value after stripping illegal
  # characters we use the login.
  #
  # Returns an author name String.
  def git_author_name
    author_name = strip_illegal_git_chars(safe_profile_name)
    author_name.blank? ? login : author_name
  end

  # Public: Returns true if the user has a primary email set.
  def has_primary_email?
    !!primary_user_email
  end

  # Public: Returns true if the user has a backup email set.
  def has_backup_email?
    !!backup_user_email
  end

  # Public: Determines if the user has disabled backup emails.
  #
  # Note: This is implemented by setting the backup email to the user's
  # primary email address. This is kind of quirky, but it dramatically
  # simplifies the state necessary to implement such a feature without adding
  # additional state to the DB.
  #
  # Returns true if the user has backup emails disabled.
  def password_reset_with_primary_email_only?
    has_backup_email? && backup_user_email == primary_user_email
  end

  # Public: The email address to attribute in git operations performed by this user.
  #
  # Returns a String e-mail address
  def git_author_email
    author_email = strip_illegal_git_chars(outbound_email)
    author_email.blank? ? anonymous_user_email : author_email
  end

  # Public: A fully anonymous email for the user with a noreply mask
  # on the current GitHub domain
  #
  # Returns a String e-mail address
  def anonymous_user_email
    (stealth_email || StealthEmail.new(self)).email
  end
  alias stealth_email_string anonymous_user_email

  # Internal: strip illegal git-author characters,
  # ensuring that the username contains at least one
  # printable character
  #
  # Angle brackets and LF are invalid as part of git author info. CR
  # might be tolerated, but why play with fire?
  #
  # Returns a String or nil
  def strip_illegal_git_chars(author)
    author && author.match(/[[:word:]]/) && author.tr("<>\n\r", "")
  end

  # The URL for this user on team (for staff users)
  def team_app_url
    "https://team.githubapp.com/#{login}" if employee?
  end

  # Public: Does this user want to use their stealth user email
  # for stuff done via the web UI?
  # Returns Boolean
  def use_stealth_email?
    primary_user_email && primary_user_email.private?
  end

  # Internal: return user's stealth email
  #
  # Returns a UserEmail
  def stealth_email
    emails.with_role("stealth")
  end
  private :stealth_email

  # Internal: is the user's profile_email "trusted" ?
  #  We defined 'trusted' as an email that the User **also** added to their list of
  #  "private" emails associated with their accounts (i.e. UserEmail)
  #
  # Returns Boolean
  def trusted_profile_email?
    profile_email.present? && emails.exists?(email: profile_email)
  end

  # Should pages owned by this user be regarded as "Official GitHub properties" ?
  def github_owned_pages?
    GitHub.github_owned_pages.include?(login)
  end

  # Public: Returns the GitHub pages hostname for this user
  #
  # For v2 users, this is github.io.  For everyone else, it is github.com.
  # Other models, like Page and Repository, should go through this method
  # for their Pages host name to ensure things work right depending on the feature
  # flag for the Pages migration.
  def pages_host_name
    if github_owned_pages?
      GitHub.pages_host_name_v1
    else
      GitHub.pages_host_name_v2
    end
  end

  # This user's "user pages repo", e.g. defunkt/defunkt.github.com
  # Returns a Repository if one is found, nil otherwise.
  def user_pages_repo
    async_user_pages_repo.sync
  end

  def async_user_pages_repo
    return @async_user_pages_repo if defined?(@async_user_pages_repo)

    @async_user_pages_repo = Platform::Loaders::RepositoryByName.load_all(id, [
      "#{login}.#{GitHub.pages_host_name_v2}",
      "#{login}.#{GitHub.pages_host_name_v1}",
    ]).then do |repo_v2, repo_v1|
      Promise.all([
        repo_v2&.async_is_user_pages_repo?,
        repo_v1&.async_is_user_pages_repo?,
      ]).then do |v2_is_user_page, v1_is_user_page|
        next repo_v2 if v2_is_user_page
        next repo_v1 if v1_is_user_page
        nil
      end
    end
  end

  def async_destroy(actor = nil)
    return unless permit_deletion?(actor)

    self.deleted_at = Time.now
    self.deleted_by = actor.login if actor
    self.deleted = true

    save!

    instrument :async_delete
    UserDeleteJob.perform_later(id, login)

    delete_type = if actor.nil?
      :UNKNOWN
    else
      actor == self ? :SELF : :OTHER
    end

    GlobalInstrumenter.instrument "user.destroy", {
      user: self,
      actor: actor,
      delete_type: delete_type,
      actor_was_staff: actor&.site_admin?,
      email: primary_user_email,
    }

    # Non-User subclasses call this method. Only count real User deletes.
    GitHub.dogstats.increment("user", tags: ["action:destroy"]) if user?
  end

  # Special ghost user used as a fallback for various issues-related records.
  #
  # Returns a User.
  def self.ghost
    return @ghost if @ghost
    # There is only one ghost created
    @ghost = User.find_by_login(GitHub.ghost_user_login)
  end

  # Is this the ghost user?
  #
  # Returns a Boolean.
  def ghost?
    login == GitHub.ghost_user_login
  end

  # Users can't use organization secrets.
  def can_use_org_secrets?
    false
  end

  # Users are users.
  def organization?
    false
  end

  # Users are users.
  def user?
    true
  end

  # Users are users.
  def mannequin?
    false
  end

  # Users are users.
  def bot?
    false
  end

  # Special staff user used as an actor for staff actions
  # so we can use this for the actor and move the staff
  # user into a new special field to help prevent staff
  # identities from becoming public
  #
  # Returns github staff account
  def self.staff_user
    return @staff_user if @staff_user
    @staff_user = User.find_by_login(GitHub.staff_user_login) || User.create_staff_user
  end

  def staff_user?
    login == GitHub.staff_user_login
  end

  # Is this a system account that is important for the application to function
  # properly?
  #
  # System users include the @ghost user as well as the
  # "trusted_oauth_apps_owner" organization (@github on .com and
  # @github-enterprise on Enterprise).
  #
  # Returns a Boolean.
  def system_account?
    ghost? ||
    (organization? && trusted_oauth_apps_owner?)
  end

  # Creates the ghost user if it doesn't already exist.
  def self.create_ghost
    password = SecureRandom.hex

    @ghost = User.where(login: GitHub.ghost_user_login).first_or_initialize(
      login: GitHub.ghost_user_login,
      email: "ghost@github.com",
      plan: GitHub::Plan.find("free").name,
      password: password,
      password_confirmation: password,
    )

    @ghost.save!
    @ghost.disable_all_notifications
    @ghost
  rescue Exception => e # rubocop:disable Lint/RescueException
    warn "failed to create ghost user: #{e.class.inspect}: #{e.message.inspect}"
  end

  # Creates the staff user if it doesn't already exist.
  def self.create_staff_user
    unless GitHub.guard_audit_log_staff_actor?
      raise "staff actor should not be created if not used for auditing"
    end

    @staff_user = User.find_by_login(GitHub.staff_user_login)

    password = SecureRandom.hex

    ActiveRecord::Base.connected_to(role: :writing) do
      @staff_user ||= User.create(
        login: GitHub.staff_user_login,
        email: "#{GitHub.staff_user_login}@github.com",
        plan: GitHub::Plan.find("free").name,
        password: password,
        password_confirmation: password,
      )

      @staff_user.save!
      @staff_user.disable_all_notifications
      @staff_user
    end
  end

  # Find the deploy keys for repositories this user is an admin of.
  # That'll be any repos the user owns and any repos in teams the user
  # has "Admin" access to.
  #
  # Returns an Array of PublicKey instances.
  def deploy_keys
    @deploy_keys ||= deploy_keys!
  end

  def deploy_keys!
    adminable_repo_ids = associated_repository_ids(min_action: :admin)
    PublicKey.where("repository_id IN (?)", adminable_repo_ids)
  end

  def public_keys=(new_keys)
    new_keys = [new_keys].flatten.reject(&:blank?)

    public_keys.to_a.dup.each do |key|
      public_keys.delete(key) unless new_keys.include? key.to_s
      new_keys.delete(key.to_s)
    end

    new_keys.each do |key|
      add_public_key(key)
    end
  end

  def add_public_key(key, title = nil)
    pub_key = PublicKey.new key: key.to_s
    pub_key.user = self
    public_keys << pub_key
  end

  # API-Public
  #
  # subscribes the user to a repository for being notified
  # about updates to it.
  #
  # A User can't watch a repo either if they don't have read access to it.
  # or if the owner is blocking them.
  #
  # In case the notifications cluster is not available we enqueue a job,
  # any call to a GitHub.newsies service will fail, and we enqueue a job
  # for retrying the operation later if we didn't succeed.
  #
  # Params:
  #  - repo, the repository to subscribe to
  #  - enqueue, defaults to true, indicates whether to enqueue a job in
  #    case of failure or not.
  #
  # Returns true if after calling this, the user eventually watches the repository, false
  # otherwise.
  #
  # This means that if the notifications cluster is down, we will enqueue a job
  # for repeating the operation later, and the user will eventually be watching the repo,
  # so it will return true.
  #
  def watch_repo(repo, enqueue: true, retryable: false)
    forbidden = !repo.pullable_by?(self) || repo.owner.blocking?(self)
    return false if forbidden

    status_response = GitHub.newsies.subscription_status(self, repo)
    return true if status_response.success? && status_response.subscribed?
    return true if GitHub.newsies.subscribe_to_list(self, repo).success?
    return false unless enqueue || retryable

    UserWatchRepoJob.perform_later(id, repo.id, enqueue, retryable)
    true
  rescue ActiveRecord::RecordNotUnique
    true
  end

  def subscribe_to_releases_only(repo)
    forbidden = !repo.pullable_by?(self) || repo.owner.blocking?(self)
    return false if forbidden

    response = GitHub.newsies.subscribe_to_thread_type(self, repo, Release)
    response.success?
  end

  # Unwatch a repository.
  def unwatch_repo(repo)
    GitHub.newsies.unsubscribe(self, repo).success?
  end

  # Ignore a repository.
  def ignore_repo(repo)
    if repo.pullable_by?(self)
      response = GitHub.newsies.ignore_list(self, repo)
      response.success?
    else
      unwatch_repo(repo)
    end
  end

  # Deprecated: Is this user watching a repository?. This method is not
  # resilient as it returns only true/false. This means that it will return
  # false if notifications are down even if the user is subscribed.
  #
  # repo - The Repository in quesiton.
  #
  # Returns a Boolean.
  def watching_repo?(repo)
    GitHub.newsies.subscription_status(self, repo).subscribed?
  end

  def can_star?(entity)
    authorization = ContentAuthorizer.authorize(self, :star, :create, starrable: entity)
    # TODO: consider moving all of these checks into the authorizor for consistency
    authorization.passed? && !starred?(entity) && !unreadable?(entity)
  end

  def async_can_star?(entity)
    promises = []
    promises << entity.async_owner if entity.respond_to?(:owner)
    promises << entity.async_parent if entity.respond_to?(:parent)
    Promise.all(promises).then do
      Promise.resolve(can_star?(entity))
    end
  end

  # Star an entity. You can star/unstar any entity you can see.
  #
  # entity - The Repository or Gist in question
  def star(entity, context: "other")
    return false unless can_star?(entity)

    star_object = nil

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      Star.transaction do
        entity.transaction do
          star_object = entity.stars.create!(user_id: self.id)
          entity.update_stargazer_count!
        end
      end
    end

    if entity.instance_of?(::Repository)
      GitHub.instrument "stars.update_count", user: self, starred_id: entity.id, starred_type: Star::STARRABLE_TYPE_REPOSITORY
    end

    Star.instrument_action(viewer: self, entity: entity, action: "star", context: context)

    GlobalInstrumenter.instrument "user.star", {
      actor: self,
      star: star_object,
      entity: entity,
      stars_count: entity.stargazer_count,
      context: (context || "other"),
      owner: entity.try(:owner),
    }

    true
  rescue ActiveRecord::RecordNotUnique
  end

  # Unstars an entity. You can star/unstar any entity you can see.
  #
  # entity - The Repository, Topic, or Gist in question
  #
  # Returns nothing.
  def unstar(entity, actor: self, context: "other")
    star_object = case entity
    when Repository, Topic
      stars.where(starrable: entity).first
    when Gist
      gist_stars.where(gist: entity).first
    else
      nil
    end

    return false unless star_object.present?

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      Star.transaction do
        entity.transaction do
          star_object.destroy
          entity.update_stargazer_count!
        end
      end
    end

    Star.instrument_action(viewer: actor, entity: entity, action: "unstar", context: context)

    GlobalInstrumenter.instrument "user.unstar", {
      actor: actor,
      star: star_object,
      entity: entity,
      stars_count: entity.stargazer_count,
      context: (context || "other"),
      owner: entity.try(:owner),
    }

    true
  end

  # Public: Total number of stars this user has on public repositories.
  # Returns an Integer.
  def starred_public_repositories_count
    count = GitHub.kv.get("user.starred_public_repositories_count.#{id}").value { nil }
    count ? count.to_i : update_starred_public_repositories_count!
  end

  # Public: Total number of stars this user has on private repositories.
  # Returns an Integer.
  def starred_private_repositories_count
    count = GitHub.kv.get("user.starred_private_repositories_count.#{id}").value { nil }
    count ? count.to_i : update_starred_private_repositories_count!
  end

  def starred_topics_count
    stars.where(starrable_type: "Topic").count
  end

  def starred_topics(limit: nil)
    starred_topic_ids = stars.
      where(starrable_type: "Topic").
      order(created_at: :desc).order(:id).
      then { |relation| limit ? relation.limit(limit * 2) : relation }.
      pluck(:starrable_id)

    Topic.with_ids(starred_topic_ids).
      order(Arel.sql("FIELD(id, #{starred_topic_ids.join(", ")})")).
      then { |relation| limit ? relation.limit(limit) : relation }
  end

  # Public: Update and return the cached count of starred
  # public repositories for this user.
  #
  # Returns an Integer.
  def update_starred_public_repositories_count!
    count = self.starred_repositories.public_scope.count
    GitHub.dogstats.increment("user", tags: ["action:update_starred_public_repositories_count"])
    ActiveRecord::Base.connected_to(role: :writing) do
      GitHub.kv.set("user.starred_public_repositories_count.#{id}", count.to_s)
    end
    count
  end

  # Public: Update and return the cached count of starred
  # private repositories for this user.
  #
  # Returns an Integer.
  def update_starred_private_repositories_count!
    count = self.starred_repositories.private_scope.count
    GitHub.dogstats.increment("user", tags: ["action:update_starred_private_repositories_count"])
    ActiveRecord::Base.connected_to(role: :writing) do
      GitHub.kv.set("user.starred_private_repositories_count.#{id}", count.to_s)
    end
    count
  end

  # Has this user starred an entity?
  #
  # entity - The Repository, Gist, or Topic in question.
  #
  # Returns a Boolean.
  def starred?(entity)
    if entity.is_a?(Topic) || entity.is_a?(Repository)
      stars.where(starrable: entity).exists?
    elsif entity.is_a?(Gist)
      starred_gists.include? entity
    else
      false
    end
  end

  # Public: can the actor view the user?
  def readable_by?(actor)
    return super if self.organization?
    self == actor
  end


  # Checks to see if the actor has the appropriate permissions
  # to restore a Archived::Repository
  def can_restore_repository?(actor)
    organization? ? adminable_by?(actor) : actor == self
  end

  # All users the user follows that also starred the same repository.
  #
  # repository_id   - The Repository id (required)
  # period          - The period to fetch stars from (required)
  #
  # Returns an ActiveRecord::Relation of Users
  def following_starred(repository_id, period = nil)
    User.following_starred(self.id, repository_id, period).filter_spam_for(self)
  end

  # Public: Follow a user. You can try to follow a user multiple times without negative side
  # effects.
  #
  # user - the User to follow
  # context - string representing the context in which the follow event was triggered, e.g.,
  #           "api", "user_profile"
  #
  # Returns a Boolean indicating whether the follow worked or not. False means you are already
  # following the user, you have reached the rate limit for following users, or you're not allowed
  # to follow that particular user.
  def follow(user, context: "other")
    unblock(user) if blocking?(user)

    return false if following?(user)
    return false unless can_follow?(user)

    following << user
    user.calculate_followerings_count(cascade: false)
    calculate_followerings_count(cascade: false)
    instrument_follow(user, action: "follow", context: context)

    true
  end

  # Public: Stop following a user.
  #
  # user - the User to stop following
  # context - string representing the context in which the unfollow event was triggered, e.g.,
  #           "api", "user_profile"
  #
  # Returns truthy indicating if unfollow succeeded or not.
  def unfollow(user, context: "other")
    if following?(user) && user != self
      following.destroy(user)
      user.calculate_followerings_count(cascade: false)
      calculate_followerings_count(cascade: false)
      instrument_follow(user, action: "unfollow", context: context)
      true
    end
  end

  def instrument_follow(user, action:, context:)
    context = if FOLLOWABLE_CONTEXTS.include?(context)
      context
    else
      "other"
    end

    GitHub.instrument("user.follow", {
      dimensions: {
        action: action,
        actor_id: id,
        user_id: user.id,
        context: context,
        current_following_count: following_count,
        current_follower_count: followers_count,
        actor_created_at: created_at.strftime("%Y-%m-%d"),
      },
    })
  end

  # Public: Checks if the given User IDs can be followed by the user.
  #
  # user_id - User ID of the potential follower.
  # user_ids - Array of User IDs to check.
  #
  # Returns a hash dedicating whether each user can be followed: user_id => Boolean
  def self.bulk_can_follow_check(user_id, user_ids)
    return Hash.new if user_ids.blank?

    sql = github_sql.new \
      user_id: user_id,
      user_ids: user_ids

    sql.add <<-SQL
      SELECT users.id, users.type, ignored_users.ignored_id
        FROM users
        LEFT OUTER JOIN ignored_users ON
          (users.id = ignored_users.ignored_id AND ignored_users.user_id = :user_id)
          OR (users.id = ignored_users.user_id AND ignored_users.ignored_id = :user_id)
        WHERE users.id IN :user_ids
    SQL

    can_follow_checks = Hash[user_ids.map { |uid| [uid, false] }]
    sql.models(User).each do |user|
      can_be_followed = !user.ignored_id.present? && user.id != user_id && user.user?
      can_follow_checks[user.id] = can_be_followed
    end
    can_follow_checks
  end

  def at_rate_limit?(user)
    potential_following = Following.new(user: self, following: user)
    return potential_following.creation_rate_limited?(check_without_increment: true)
  end

  def can_follow?(user)
    return false if at_rate_limit?(user)

    can_follow = User.bulk_can_follow_check(id, [user.id])
    return can_follow[user.id]
  end

  # nb. the follower/following methods are overridden
  # in Organization because orgs don't follow or have followers

  # Public: Total of users following this user
  #
  # Returns integer of cached followers count
  def followers_count
    count = GitHub.kv.get("user.followers_count.#{id}").value { nil }
    count ? count.to_i : followers_count!
  end

  # Public: Total of users this user follows
  #
  # Returns integer of cached following count
  def following_count
    count = GitHub.kv.get("user.following_count.#{id}").value { nil }
    count ? count.to_i : following_count!
  end

  # Public: Update and return cached count of followers
  #
  # Returns Integer of followers count
  def followers_count!
    sql = github_sql.new <<-'SQL', user_id: id, spammy: false
      SELECT COUNT(*)
      FROM followers
      INNER JOIN users
      ON users.id = followers.user_id
      WHERE followers.following_id = :user_id
      AND users.suspended_at IS NULL
      AND users.spammy = :spammy
    SQL

    unless spammy?
      sql.add <<-SQL
        AND followers.user_hidden = false
      SQL
    end

    count = sql.value

    ActiveRecord::Base.connected_to(role: :writing) do
      GitHub.kv.set("user.followers_count.#{id}", count.to_s)
    end

    synchronize_search_index
    count
  end

  # Public: Update and return cached count of users following
  #
  # Returns Integer of following count
  def following_count!
    sql = github_sql.new <<-'SQL', user_id: id, spammy: false
      SELECT COUNT(*)
      FROM followers
      INNER JOIN users
      ON users.id = followers.following_id
      WHERE followers.user_id = :user_id
      AND users.suspended_at IS NULL
      AND users.spammy = :spammy
    SQL

    unless spammy?
      sql.add <<-SQL
        AND followers.user_hidden = false
      SQL
    end

    count = sql.value

    ActiveRecord::Base.connected_to(role: :writing) do
      GitHub.kv.set("user.following_count.#{id}", count.to_s)
    end

    count
  end

  # Public: Checks if the given User IDs are followed by the user.
  #
  # user_id - User ID of the follower.
  # user_ids - Array of User IDs to check.
  #
  # Returns a hash dedicating whether each user is followed: user_id => Boolean
  def self.bulk_following_check(user_id, user_ids)
    return Hash.new if user_ids.blank?

    following_checks = Hash[user_ids.map { |uid| [uid, false] }]
    Following.where(user_id: user_id, following_id: user_ids).each do |f|
      following_checks[f.following_id] = true
    end
    following_checks
  end

  def following?(user)
    is_following = User.bulk_following_check(id, [user.id])
    return is_following[user.id]
  end

  # All users that user follows that are public members of an organization
  #
  # repository_id     - The Repository id (required)
  # limit (optional)  - The number of users to return
  #
  # Returns an ActiveRecord::Relation of Users
  def following_at(organization_id, limit = 16)
    User.following_at(self.id, organization_id).limit(limit)
  end

  # language (optional) - the Linguist::Language to find the top repo in that language.
  def most_popular_repository(language = nil)
    default_alias = language.default_alias if language && !language.is_a?(String)
    language_id = nil

    if default_alias and language_name = LanguageName.find_by_alias(default_alias)
      language_id = language_name.id
    end

    public_repos_scope = self.public_repositories
    if language_id || language == "Unknown"
      public_repos_scope = public_repos_scope.where(primary_language_name_id: language_id)
    end

    public_repos_scope.limit(1).order("#{Repository.stargazer_count_column} DESC").first
  end

  def fork(repository)
    return [false, :organization] if organization?
    repository.fork(forker: self)
  end

  # Finds this user's fork of a given repository, if one exists.
  #
  # repository - The Repository whose fork you want to find.
  #
  # Returns a Repository if the fork is found.
  # Returns nil if nothing found.
  def my_fork_of(repository)
    repo = repository.find_fork_in_network_for_user(self)
    repo.owner = self if repo
    repo
  end

  def has_forked?(repository)
    !!repository.find_fork_in_network_for_user(self)
  end

  # Find users with logins matching a specific term.
  #
  # Params:
  #
  #    term - The string to search for
  #    options - Options Hash with Symbol keys:
  #    :limit - Max # of results
  #    :friends - Array of users to prefer in the results.
  #    :org - Organization to prefer members from in the results.
  #    :with_orgs - Whether to include orgs. Default: false
  #    :org_member_scope - If supplied with :org, filters users to return only public or all members of an organization, Default: nil
  #    :include_business_orgs - If supplied with :org and :org_member_scope, filters users to return only members of the business, Default: false
  #
  # Returns an Array of Users.
  def self.search(term, options = {})
    limit = (options[:limit] || 30).to_i
    friends = options[:friends] || []
    org = options[:org]
    org_member_scope = options[:org_member_scope]
    include_business_orgs = options[:include_business_orgs]
    with_orgs = options[:with_orgs].nil? ? false : options[:with_orgs]
    exclude_suspended = options[:exclude_suspended].nil? ? false : options[:exclude_suspended]

    if term.to_s.index("@")
      users = User.includes(:profile)
                  .with_prefix("profiles.email", term)
                  .references(:profile)
                  .limit(limit)
      users = users.where(suspended_at: nil) if exclude_suspended
      users = users.to_a
    elsif term.present? # query is invalid if no term to search
      users = if org.present?
        user_query(term, limit: limit, org: org, org_member_scope: org_member_scope, include_business_orgs: include_business_orgs, exclude_suspended: exclude_suspended)
      else
        user_query(term, limit: limit, friends: friends, exclude_suspended: exclude_suspended)
      end
    else
      users = []
    end

    users.unshift(*Array(friends).
      select { |friend| friend.login =~ /^#{Regexp.quote(term.to_s)}/ })

    users.uniq!
    users.compact!

    # Only want real users, unless including organizations
    if with_orgs
      users.select! { |u| u.user? || u.organization? }
    else
      users.select!(&:user?)
    end

    users.first(limit)
  end

  # Search for users in ES directly. No search by email or
  # other features of `User.search.`
  #
  #  term - String term to search for.
  #  limit - Integer limit.
  #  friends - Array of users to prefer in the results.
  #  org - Organization to give preference to members.
  #  org_member_scope - If supplied with org, filters users to return public or all members of an organization
  #  include_business_orgs - If supplied with org and org_member_scope, filters users to return only members of the business
  #
  # Returns an Array of Users.
  def self.user_query(term, friends: [], limit: 30, org: nil, org_member_scope: nil, include_business_orgs: false, exclude_suspended: false)
    query = Search::Queries::UserLoginQuery.new(
      phrase: term,
      per_page: limit,
      source_fields: false,
      friends: friends,
      org: org,
      org_member_scope: org_member_scope,
      include_business_orgs: include_business_orgs,
      exclude_suspended: exclude_suspended,
    )
    query.execute.results.map { |h| h["_model"] }
  end

  def valid_rename?(new_login)
    return false if renaming?

    old_login = login.dup

    if system_account?
      errors.add(:base, "System accounts cannot be renamed.")
      return false
    end

    if new_login == old_login
      errors.add(:login, "must be different.")
      return false
    end

    if self.spammy?
      GitHub::SpamChecker.notify("Spammy user #{id} renaming from #{old_login} to #{new_login}. https://github.com/stafftools/audit_log?query=user_id:#{id}")
      mark_login_as_used
    end

    repos = repositories.pluck(:name).map(&:to_s)
    clashing_namespaces = RetiredNamespace.where(owner_login: new_login, name: repos)

    if clashing_namespaces.any?
      errors.add(:login, "change was not successful. The repository name #{new_login}/#{clashing_namespaces.first.name} has been retired and cannot be reused")
      return false
    end

    self.login = new_login
    valid = valid?
    self.login = old_login

    valid
  end

  # Destructive! Renames a user asynchronously.
  # Check `renaming?` to see if a rename is in progress.
  #
  # new_login - String new login for the user.
  # actor - The user account making the change.
  # Spammy users can rename their accounts only if staff allows it.
  #
  # Returns a Boolean indicating whether the rename started or not.
  #   If false there was either a Validation error or
  #   a rename is already in progress.
  def rename(new_login, actor: self)
    return false if spammy? && !spammy_renaming_overridden? && !actor.site_admin?

    if valid_rename? new_login
      update_attribute(:renaming, true)
      UserRenameJob.perform_later(self, new_login, actor)
      return true
    else
      return false
    end
  end

  # Destructive! Renames a user synchronously.
  # Call it from a background job.
  #
  # new_login - String new login for the user.
  # actor - The user account making the change.
  # Spammy users can rename their accounts only if staff allows it.
  #
  # Returns nothing.
  def rename!(new_login, actor: self)
    return false if spammy? && !spammy_renaming_overridden? && !actor.site_admin?

    old_login = login.dup

    self.login = new_login

    # Test that changing the login works.
    if !valid?
      self.login = old_login
      update_attribute(:renaming, nil)
      instrument :rename_failed, old_login: old_login, new_login: new_login
      return false
    end

    prepare_repos_for_rename(old_login, new_login)

    StealthEmail.new(self).rename!(new_login) if stealth_email

    # Do it.
    update! \
      login: new_login,
      renamed_at: Time.now

    # The rename method is shared between User and Org. Sometimes User relies
    # on the actor being the user that is being renamed.
    if self.organization?
      instrument :rename, old_login: old_login, actor: actor
    else
      instrument :rename, old_login: old_login
    end

    # Update associated records.
    public_keys.reload.each(&:save)
    update_repos_after_rename(old_login, new_login)
  ensure
    # Unlock rename.
    update_attribute(:renaming, nil)
  end

  def prepare_repos_for_rename(old_login, new_login)
    # Queue repo move jobs
    repositories.each do |repo|
      repo.move_downloads old_login, new_login
    end
  end

  def disk_usage
    repositories.where(parent_id: nil).sum(:disk_usage) || 0
  end

  # Public: Find a repository owned by this user
  #
  # Returns a Repository or nil
  def find_repo_by_name(name)
    return nil unless name && GitHub::UTF8.valid_unicode3?(name.to_s)
    repositories.where(name: name.to_s).first.tap do |repo|
      if repo
        repo.association(:owner).target = self
        repo.association(:organization).target = self if self.organization? && repo.organization_id == self.id
      end
    end
  end

  def notifications?
    wants_email?
  end

  def notifications_count
    notifications.count("DISTINCT target_global_id")
  end

  def notify_web_notifications_changed_socket_subscribers(wait)
    channel = GitHub::WebSocket::Channels.notifications_changed(self)

    view = Site::HeaderNotifications.new(current_user: self)

    GitHub::WebSocket.notify_user_channel(id, channel,
      aria_label: view.aria_label,
      ga_click: view.ga_click,
      span_class: view.span_class,
      wait: wait,
    )
  end

  def collaborators_count
    repo_ids = Repository.private_scope.active.unlocked_repos.network_roots.where(owner_id: id).pluck(:id)
    Ability.where(subject_id: repo_ids, subject_type: "Repository", actor_type: "User", priority: Ability.priorities[:direct]).distinct.count(:actor_id)
  end

  def owned_advisory_workspace_repositories
    workspace_repo_ids = RepositoryAdvisory.where(owner_id: id).where.not(workspace_repository_id: nil).pluck(:workspace_repository_id)
    owned_private_repositories.with_ids(workspace_repo_ids).from("`#{Repository.table_name}` FORCE INDEX (PRIMARY)")
  end

  def private_repo_count_for_limit_check
    return @private_repo_count_for_limit_check if defined?(@private_repo_count_for_limit_check)
    count = owned_private_repositories.size
    if count == 0
      @private_repo_count_for_limit_check = count
    else
      @private_repo_count_for_limit_check = count - owned_advisory_workspace_repositories.size
    end
  end

  def at_private_repo_limit?
    return false if has_unlimited_private_repositories?
    private_repo_count_for_limit_check >= plan_limit(:repos, visibility: :private)
  end

  def can_add_private_repo?
    plan_supports?(:repos, visibility: :private) && !at_private_repo_limit?
  end

  # Check if the user is on a plan with private repositories and if they're on
  # a plan with private repositories, check if they've reached the limit
  #
  # Returns a Boolean.
  def at_plan_repo_limit?
    plan_supports?(:repos, visibility: :private) && at_private_repo_limit?
  end

  # Is the default visibility for repositories owned by users private?
  #
  # On Enterprise Server, honor the default repository visibility setting
  # for the installation. Otherwise, check if the user has any active
  # external identity sessions and default to private if yes, in order to
  # lower the risk of IP code leakage.
  # See https://github.com/github/github/issues/144149 for more info.
  #
  # This method is overridden in Organization.
  #
  # Returns a Boolean.
  def private_repo_by_default?
    return ["internal", "private"].include?(GitHub.default_repo_visibility) if GitHub.enterprise?
    external_identity_sessions.active.any?
  end

  def default_repo_visibility
    return "private" if private_repo_by_default?
    "public"
  end

  def over_plan_limit?
    return false if plan.per_seat?
    private_repo_count_for_limit_check > plan_limit(:repos, visibility: :private)
  end

  def over_plan_limit_orgs
    @over_plan_limit_orgs ||=
      owned_organizations.paying.select(&:over_plan_limit?)
  end

  def org_over_plan_limit?
    over_plan_limit_orgs.present?
  end

  # Public: Can this user fork a specific repository?
  #
  # repository - The repo you want to check
  #  new_owner - The User or Organization who will own the new fork. Defaults
  #              to this user, but if you're trying to fork TO an organization
  #              you'll want to pass that in.
  #
  # Returns a Boolean.
  def can_fork?(repository, new_owner = self)
    # this feels wrong, but we don't want forks from blocked users to show in
    # the fork queue or network graphs
    return false if blocked_by?(repository.owner_id)
    return false if repository.owner_id == new_owner.id
    return false if !repository.pullable_by?(self)
    return false unless new_owner.can_create_repository?(self, visibility: repository.visibility)

    repository.allows_forking?
  end

  def self.with_profile_email(email)
    Profile.find_by_email(email).try(:user)
  end

  %w(
    bio
    blog
    company
    display_staff_badge
    email
    hireable
    location
    name
    readme_opt_in
    spoken_language_preference_code
    twitter_username
  ).each do |attribute|
    define_method "profile_#{attribute}" do
      profile.try(attribute)
    end

    define_method "profile_#{attribute}=" do |value|
      @profile_updated = true
      find_or_create_profile.send("#{attribute}=", value)
    end
  end

  {
    bio: GitHub::Goomba::ProfileBioPipeline,
    company: GitHub::Goomba::ProfileCompanyPipeline,
  }.each do |attribute, goomba_pipeline|
    define_method "profile_#{attribute}_html" do
      value = profile.try(attribute)
      if value.present?
        goomba_pipeline.to_html(value, {})
      else
        GitHub::HTMLSafeString::EMPTY
      end
    end

    define_method "async_profile_#{attribute}_html" do
      async_profile.then do |profile|
        value = profile.try(attribute)
        if value.present?
          goomba_pipeline.to_html(value, {})
        else
          GitHub::HTMLSafeString::EMPTY
        end
      end
    end
  end

  def profile_readme_opt_in?
    profile_readme_opt_in == true
  end

  def profile_email_is_known
    return if organization?
    return if profile.email.blank?
    if profile.email_changed? && possible_profile_emails.exclude?(profile.email)
      email_type = GitHub.email_verification_enabled? ? "verified" : "known"
      errors.add(:profile_email, "must be one of the user's #{email_type} email addresses")
    end
  end

  def ignored_upgrade?
    self.upgrade_ignore && (self.upgrade_ignore == plan.name)
  end

  ##
  # Spam stuff

  # This is an after_commit workaround that can be removed after
  # creating a User::Creator service class.
  #
  # See https://github.com/github/github/pull/31471#issuecomment-52836753
  def mark_for_spam_check
    @needs_spam_check = true if new_record?
    true # for good measure
  end

  # Enqueues a job that runs #check_for_spam on this record.
  def enqueue_check_for_spam
    spam_attributes_changed = ["login", "last_ip"].any? { |attr| previous_changes.include?(attr) }
    return unless @needs_spam_check || spam_attributes_changed

    options = {
      last_ip_changed: (@needs_spam_check || previous_changes.include?("last_ip")),
    }

    CheckForSpamJob.enqueue(self, options)
  end

  # Is this user exempt from spam checks for the first N minutes after account creation?
  # This is only used for staff members testing the signup flow.
  # See SignupController#user_exempt_from_spam_checks? for the conditions.
  def exempt_from_spam_check_after_signup?
    GitHub.employee_unicorn? &&
      created_at > SPAM_EXEMPTION_TTL.ago &&
      GitHub.kv.get("#{SPAM_EXEMPTION_KEY}:#{login}").value!
  end

  # Store this user's login in the KV to temporarily exempt them from further spam checks
  # (we are using `login` instead of `id` on purpose here, since we want to set this before the user record is created and has an ID)
  def mark_as_temporarily_exempt_from_spam_checks
    GitHub.dogstats.increment "spam.exempt_from_spam_check"
    GitHub.kv.set("#{SPAM_EXEMPTION_KEY}:#{login}", "1", expires: SPAM_EXEMPTION_TTL.from_now)
  end

  # Check this user for spamminess using `GitHub::SpamChecker`, and mark it as
  # spammy if found guilty.
  #
  # options - :last_ip_changed - whether the last_ip field changed during
  #                              the record update that triggered this spam check
  #
  # Returns nothing.
  def check_for_spam(options = {})
    return if self.spammy

    if reason = GitHub::SpamChecker.test_user(self)
      safer_mark_as_spammy(reason: reason)
    elsif reason = GitHub::SpamChecker.test_user_email(self)
      GitHub::SpamChecker.notify("User #{login} (#{id}): Email considered harmful: #{email} - #{reason}")
      safer_mark_as_spammy(reason: reason)
    elsif options[:last_ip_changed]
      check_associates_for_spam
    end
  end

  def check_associates_for_spam
    return if Spam.ip_safelisted?(last_ip)
    UserAssociatesSpamCheckJob.perform_later(self.id)
    self
  end

  # Flag this user as spammy unless they're old/active enough to warrant a
  # human review first. In that case, queue the user instead.
  def safer_mark_as_spammy(params = {})
    reason    = params[:reason] || "No reason given"
    hard_flag = params[:hard_flag] || is_hard_flag?(reason)
    # If we're already flagged or this is a hard_flag, just move along.
    if self.spammy || hard_flag || !self.can_be_flagged?
      queue_it = false
    else
      queue_it = GitHub::SpamChecker.old_or_active?(self)
    end

    if queue_it
      GlobalInstrumenter.instrument(
        "add_account_to_spamurai_queue",
        {
          account_global_relay_id: self.global_relay_id,
          additional_context: reason,
          queue_global_relay_id: SpamQueue::SUSPICIOUS_OLDER_ACCOUNTS_GLOBAL_RELAY_ID,
          origin: Spam::Origin.call(params[:origin]).upcase,
        },
      )
      GitHub.dogstats.increment("spam.active_user_review")
      GitHub::SpamChecker.notify("User %s (%d) seems fairly active, so queuing for review (Reason: %s)." %
                                 [self.login, self.id, params[:reason]])
    else
      mark_as_spammy(params)
    end
  end

  # Mark this user as spammy.
  #
  # :actor  - The User marking this user as spammy.
  # :reason - The String reason. Defaults to no reason.
  #
  # Returns nothing.
  def mark_as_spammy(params = {})
    actor     = params[:actor]
    reason    = params[:reason] || "No reason given"
    hard_flag = params[:hard_flag] || is_hard_flag?(reason)

    serialized_previous_classification = Hydro::EntitySerializer.user_spammy_classification(self)
    serialized_previous_spammy_reason = Hydro::EntitySerializer.user_spammy_reason(self)
    serialized_previously_suspended = Hydro::EntitySerializer.user_suspended(self)

    # Report some info about blank spammy_reason attempts
    if self.spammy && self.spammy_reason.blank?
      # Report to room that we had a blank spammy_reason
      room_msg = "WARNING: mark_as_spammy called on User with empty spammy_reason: #{self.login} (#{self.id}) by actor #{actor}.  spammy_reason was ''. The new spammy_reason (not being applied) was #{reason}"
      GitHub::SpamChecker.notify(room_msg)
    end

    if reason == ""
      # Report to room that a blank spammy_reason is being submitted
      room_msg = "WARNING: mark_as_spammy called on User #{self.login} (#{self.id}) by actor #{actor}.  Existing spammy_reason is '#{self.spammy_reason}'. The new spammy_reason IS EMPTY ('')"
      GitHub::SpamChecker.notify(room_msg)
    end

    # never_spammy_bailout
    if !hard_flag && !can_be_flagged?
      GitHub.dogstats.increment "spam.staff_actions", tags: ["spam_action:never_spammy_bailout"]
      return
    end

    # If we're already flagged, but this is a hard-flag call, and our current
    # reason isn't a hard-flag, harden it by adding the magic phrase.
    # Return in any case.
    if self.spammy
      if hard_flag && !is_hard_flag?(spammy_reason)
        new_reason = GitHub::SpamChecker.make_hard_reason(spammy_reason)
        update_attribute :spammy_reason, new_reason
      end
      return
    end

    if actor.is_a? User
      reason += " by @#{actor.login}"
    end

    if hard_flag && !is_hard_flag?(reason)
      reason = GitHub::SpamChecker.make_hard_reason(reason)
    end
    previously_spammy = self.spammy
    previous_spammy_reason = self.spammy_reason
    self.spammy = true
    self.spammy_reason = reason
    GitHub::Logger.log(method: "mark_as_spammy",
                       spammy_reason: spammy_reason,
                       actor: actor,
                       current_user: self)
    self.save(validate: false) unless self.new_record?

    CancelActiveActionsWorkflowsJob.perform_later(user: self)
    CodespacesSuspendEnvironmentsJob.perform_later(id)

    normalizer = Spam::SpammyReasonNormalizer.new(reason)
    origin ||= Spam::Origin.call(params[:origin])
    payload = {}
    payload[:previously_spammy] = previously_spammy
    payload[:currently_spammy] = true
    payload[:previous_spammy_reason] = previous_spammy_reason if previous_spammy_reason.present?
    payload[:reason] = reason
    payload[:origin] = origin.upcase
    payload.merge!(normalizer.payload)

    GlobalInstrumenter.instrument "staff.mark_as_spammy", payload.merge({
      actor: actor,
      account: self,
      classification: :SPAMMY,
    })

    if (params[:instrument_abuse_classification].nil? ? true : params[:instrument_abuse_classification])
      GlobalInstrumenter.instrument "abuse_classification.publish",
        actor: actor,
        account: self,
        origin: origin,
        queue_action: :QUEUE_ACTION_NONE,
        serialized_previous_classification: serialized_previous_classification,
        serialized_previous_spammy_reason: serialized_previous_spammy_reason,
        serialized_previously_suspended: serialized_previously_suspended
    end

    payload[:prefix] = :staff
    payload[:user] = self
    payload[:hard_flag] = hard_flag
    payload.merge!(GitHub.guarded_audit_log_staff_actor_entry(actor))
    instrument :mark_as_spammy, payload

    unless self.new_record?
      update_tables_user_hidden
    end
  end

  # Indicates if we should hide this user's content from other users. Considers
  # the user's spammy state.
  #
  # Returns a Boolean.
  def content_hidden?
    spammy?
  end

  # Queues up all user_hidden update jobs.
  def update_tables_user_hidden
    return unless GitHub.spamminess_check_enabled?
    UpdateTableUserHiddenJob.perform_later(self.id, Spam::Spammable.tables_classes_including.keys)
  end

  # Clear this User from spamminess.
  #
  # :actor     - The User marking this user as not spammy.
  # :whitelist - Boolean - should the user be whitelisted as well?
  #
  # Returns nothing.
  def mark_not_spammy(params = {})
    actor     = params[:actor]
    whitelist = params[:whitelist]

    origin ||= Spam::Origin.call(params[:origin])

    ENV["GH_CONSOLE"].present?
    # We need to keep up with our own changed? since spammy_reason is
    # a serialized attribute. Save dem queries.
    save_me = false

    serialized_previous_classification = Hydro::EntitySerializer.user_spammy_classification(self)
    serialized_previous_spammy_reason = Hydro::EntitySerializer.user_spammy_reason(self)
    serialized_previously_suspended = Hydro::EntitySerializer.user_suspended(self)

    old_reason  = self.spammy_reason
    self.spammy_reason = whitelist ? "Not spammy" : nil
    save_me ||= (self.spammy_reason != old_reason)

    normalizer = Spam::SpammyReasonNormalizer.new(old_reason)
    payload = {}
    payload[:previously_spammy] = self.spammy
    payload[:currently_spammy] = false
    payload[:previous_spammy_reason] = old_reason if old_reason.present?
    payload[:origin] = origin.upcase
    payload.merge!(normalizer.payload) if self.spammy

    if (was_spammy = self.spammy)
      self.spammy = false
      save_me = true
    end

    if save_me
      # Don't let other invalid attrs get in the way of updating spamminess
      self.save(validate: false)
    end

    # Log that we came through here, even if we're only whitelisting
    # an already non-spammy user.
    GlobalInstrumenter.instrument "staff.mark_not_spammy", payload.merge({
      actor: actor,
      account: self,
      classification: (whitelist ? :HAMMY : :CLASSIFICATION_UNKNOWN),
    })

    if (params[:instrument_abuse_classification].nil? ? true : params[:instrument_abuse_classification])
      GlobalInstrumenter.instrument "abuse_classification.publish",
        actor: actor,
        account: self,
        origin: origin,
        queue_action: :QUEUE_ACTION_NONE,
        rule_name: params[:rule_name],
        rule_version: params[:rule_version],
        serialized_previous_classification: serialized_previous_classification,
        serialized_previous_spammy_reason: serialized_previous_spammy_reason,
        serialized_previously_suspended: serialized_previously_suspended
    end

    instrument :mark_not_spammy, payload.
      merge({ whitelist: whitelist, prefix: :staff, user: self, note: "Old reason: #{old_reason}" }).
      merge(GitHub.guarded_audit_log_staff_actor_entry(actor))

    # If we are only whitelisting a user already not-spammy, then the 'hidden'
    # state for that user's content should already be handled (even if there
    # was state flapping). So no need to kick off a possibly expensive job.
    if !whitelist || was_spammy
      update_tables_user_hidden
    end

    # Mark a false positive on any matching flagging SpamPatterns
    if was_spammy
      SpamPattern.flag_matches_for(self).each { |sp| sp.record_false_positive! }
    end

    # Tell The Spam Room what happened
    room_msg = "User #{self.login} (#{self.id})"
    room_msg += " [#{self.email}]" unless self.email.blank?
    if was_spammy
      room_msg += " un-flagged (after #{Spam.flagged_interval_in_words(self)})"
      room_msg += " and" if whitelist
    end
    room_msg += " whitelisted" if whitelist
    room_msg += " by #{actor.login}" if actor
    room_msg += "."
    room_msg += " Old reason: '#{old_reason}'" if was_spammy

    # If erebor is whitelisting zillions of legit accounts, spam-notifications
    # doesn't need the noise.
    unless whitelist && !was_spammy &&
           actor && actor.login == ::Spam::SENIOR_ANALYST_LOGIN
      GitHub::SpamChecker.notify(room_msg)
    end
  end

  # Clear this User from spamminess, and whitelist the user from being marked as
  # spammy again. Takes optional params and passes them to `mark_not_spammy`.
  #
  # Returns nothing.
  def whitelist_from_spam_flag(params = {})
    params[:whitelist] ||= true
    mark_not_spammy(params)
  end

  # Determine whether this user has been whitelisted from spamminess,
  # typically via <https://github.com/stafftools>/login.
  #
  # Returns true if the user is whitelisted, false otherwise.
  def spam_whitelisted?
    # I think we really do want an actual boolean return value here.
    # Otherwise it looks very odd in the console.
    spammy_reason.to_s.start_with?("Not spammy")
  end
  alias_method :hammy?, :spam_whitelisted?

  # Add this User's login to the (temporary) tainted login list if the User is
  # spammy, so the spammer can't immediately re-create an account with the same
  # name to keep his links working.
  #
  # Returns nothing.
  def mark_login_as_used
    # We're going to pass in the id of the existing User as the value, mostly
    # to see if it proves useful, since we have to give it *some* value.
    Spam.mark_login_tainted(login, id) if spammy?
  end

  # Is this user spammy?
  #
  # Returns true if the user's `spammy` attribute is true AND User#never_spammy?
  # returns false (to override cases where a never_spammy? user's flag somehow
  # got set). Returns false otherwise.
  def spammy?
    super && !never_spammy?
  end

  # Does this reason indicate a hard flag?
  #
  # reason - String
  #
  # Returns true if it's a hard flag, false otherwise
  def is_hard_flag?(reason)
    reason =~ GitHub::SpamChecker::HARD_SPAM_FLAG_REGEXP
  end

  # Returns true if this user is never spammy, false otherwise. Always
  # returns true when spamminess check is disabled. See
  # `GitHub::Config.spamminess_check_enabled`.
  def never_spammy?
    return true unless GitHub.spamminess_check_enabled?
    return false if is_hard_flag?(spammy_reason)
    !can_be_flagged?
  end

  # Returns true if this user can be flagged as spammy and false otherwise.
  # Whitelisted users can't be flagged spammy (without first clearing the
  # whitelist). Always returns false when spamminess check is disabled.
  # See `GitHub::Config.spamminess_check_enabled`.
  def can_be_flagged?
    return false unless GitHub.spamminess_check_enabled?
    return false if spam_whitelisted?
    return false if employee?
    true
  end

  # Site admins can delete all the things
  # Spammy users and orgs can delete themselves only if stafftools override is set
  # All other users can delete themselves
  # An account cannot be deleted if there is a legal hold on it regardless of
  # the above rules.
  def permit_deletion?(actor = nil)
    actor ||= self
    return false if legal_hold?
    return true if actor.site_admin?
    return false if has_any_trade_restrictions?
    if spammy?
      return false unless self.spammy_deleting_overridden?
    end
    true
  end

  def rate_limit_exempt_user?
    GitHub.rate_limiting_exempt_users.include?(login) && persisted?
  end

  def find_logins
    find_audit_events("user.login")
  end

  def find_login_ip_addresses
    find_logins.collect { |t| t["actor_ip"] }
  end

  # NOTE: Deprecate ip_neighbors eventually in favor of User.by_ip_with_prefix.
  # Public: Returns users that share the same last ip address.
  #   Used by spam/platform-health team.
  #
  # prefix - network address prefix to filter users by (default 24)
  #
  # Returns User Scope
  def ip_neighbors(prefix: 24)
    ip = if prefix == 24 && last_ip =~ /\d+\.\d+\.\d+/
      $~[0] + ".%" # $~[0] grabs the last matched item. ex "1.2.3.%"
    elsif last_ip =~ /\d+\.\d+\.\d+.\d+/
      $~[0] # ex "1.2.3.4"
    else
      nil
    end

    ip ? User.where(["last_ip like ?", ip]) : User.none
  end

  # Public: Returns users at an ip address by direct match (prefix: 32) or class C (prefix: 24).
  # Used by @github/platform-health in spam fighting efforts.
  #
  # ip - String IPv4 address.
  #
  # Returns an ActiveRecord::Relation.
  def self.by_ip_with_prefix(ip, prefix: NETWORK_ADDRESS_PREFIX_32)
    return User.none unless ip.present?
    return by_ip(ip) if prefix == NETWORK_ADDRESS_PREFIX_32
    return User.none if prefix != NETWORK_ADDRESS_PREFIX_24
    ip_parts = ip.match(NETWORK_ADDRESS_PARTS_REGEX)
    return User.none if ip_parts.nil?
    ip_like = "#{ip_parts[1]}.#{ip_parts[2]}.#{ip_parts[3]}.%" # "1.2.3.%"
    from("users USE INDEX (index_users_on_last_ip)").where(["last_ip like ?", ip_like])
  end

  NETWORK_ADDRESS_PREFIX_32 = 32
  NETWORK_ADDRESS_PREFIX_24 = 24
  NETWORK_ADDRESS_PARTS_REGEX = /(\d+)\.(\d+)\.(\d+)\.(\d+)/

  def find_audit_events(event)
    find_audit_events_for_actions([event])
  end

  def find_audit_events_for_actions(actions)
    options = {
      user_id: id,
      allowlist: actions,
      raw: true,
    }
    Audit::Driftwood::Query.new_user_query(options).execute.results
  end

  # Public: Returns an elasticsearch query string that will find all of this
  # user's audit log events
  #
  # Returns a String which can be passed to elasticsearch as a `query_string`
  def audit_log_query
    "(user_id:#{id} OR actor_id:#{id})"
  end

  # Public: Returns an elasticsearch query string that will find all of this
  # user's recent abuse reports
  #
  # Returns a String which can be passed to elasticsearch as a `query_string`
  def recent_abuse_reports_query
    "data.reported_user_id:#{id} action:user.report_abuse created_at:>#{(Time.now - 2.weeks).to_i}"
  end

  # Public: How long should we wait before updating a user's follower and following counts after
  # they change. A user's counts will only be updated at most one time in this many seconds.
  FOLLOW_CALCULATION_INTERVAL_IN_SECONDS = 600 # 10 minutes

  # Public: Enqueue job to update follower and following counts for this user.
  #
  # cascade - Boolean indicating whether the user's followers and followed users should also have
  #           their counts updated
  #
  # Returns a Boolean indicating if a new job was enqueued. A job will only be enqueued once
  # every ten minutes for this user.
  def calculate_followerings_count(cascade: true)
    CalculateFolloweringsCountJob.enqueue_once_per_interval([id, { cascade: cascade }],
      interval: FOLLOW_CALCULATION_INTERVAL_IN_SECONDS)
  end

  # Suspend a user.
  #
  # Returns false if a reason was provided or account suspension is managed
  # externally, true otherwise. Error strings are added to :base if there are
  # any.
  def suspend(reason, actor: nil, instrument_abuse_classification: true, origin: nil)
    errors.delete(:base)
    errors.add(:base, "You have to specify a reason for the suspension.") if reason.blank?

    serialized_previously_suspended = Hydro::EntitySerializer.user_suspended(self)

    return false unless errors[:base].empty?

    update_attribute(:suspended_at, Time.now)
    revoke_active_sessions(:suspended)
    calculate_followerings_count(cascade: true)
    ToggleHiddenUserInNotificationsJob.perform_later(self.id, "hide")
    RecalculateUserDiscussionsJob.perform_later(self) unless GitHub.enterprise?
    instrument :suspend, GitHub.guarded_audit_log_staff_actor_entry(actor).merge(reason: reason)

    if instrument_abuse_classification
      GlobalInstrumenter.instrument "abuse_classification.publish",
        actor: actor,
        account: self,
        queue_action: :QUEUE_ACTION_NONE,
        origin: origin,
        serialized_previously_suspended: serialized_previously_suspended
    end

    true
  end

  # Unsuspend a user.
  #
  # Returns false if a reason was provided or account suspension is managed
  # externally or there are no free seats, true otherwise. Error strings are
  # added to :base if there are any.
  def unsuspend(reason, actor: nil, instrument_abuse_classification: true, origin: nil)
    errors.delete(:base)
    errors.add(:base, "You have to specify a reason for the unsuspension.") if reason.blank?
    enforce_seat_limit "No seats available. Purchase more seats or suspend another user."

    return false unless errors[:base].empty?

    if suspended?
      serialized_previously_suspended = Hydro::EntitySerializer.user_suspended(self)
      update_attribute(:suspended_at, nil)
      calculate_followerings_count(cascade: true)
      ToggleHiddenUserInNotificationsJob.perform_later(self.id, "unhide")
      instrument :unsuspend, GitHub.guarded_audit_log_staff_actor_entry(actor).merge(reason: reason)

      if instrument_abuse_classification
        GlobalInstrumenter.instrument "abuse_classification.publish",
          actor: actor,
          origin: origin,
          queue_action: :QUEUE_ACTION_NONE,
          account: self,
          serialized_previously_suspended: serialized_previously_suspended
      end
    end

    true
  end

  # Is this user suspended?
  #
  # Returns true if so, false otherwise.
  def suspended?
    !!suspended_at
  end

  # Should the delete button show up for this user in stafftools?
  #
  # Returns true if so, false otherwise.
  def deletable?
    return true if GitHub.enterprise?
    !(paid_plan? || orgs_with_private_plan?)
  end

  # Internal: do any Orgs on this User have a private plan?
  def orgs_with_private_plan?
    organizations.any? { |org| org.paid_plan? && !org.disabled? }
  end

  # Should the suspend button show up for this user/org in stafftools?
  #
  # Returns true if so, false otherwise.
  def suspendable?
    !organization? && !site_admin? && !employee?
  end

  # Absolute permalink URL for this user.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                user.permalink(include_host: false) => `/jonrohan`
  #
  def permalink(include_host: true)
    if include_host
      "#{GitHub.url}/#{to_param}"
    else
      "/#{to_param}"
    end
  end

  # Private: Was the associated Profile updated?
  def profile_updated?
    @profile_updated
  end
  private :profile_updated?

  def async_find_or_create_profile
    async_profile.then do |profile|
      if profile
        profile
      else
        ActiveRecord::Base.connected_to(role: :writing) do
          create_profile
        end
      end
    end
  end

  def find_or_create_profile
    async_find_or_create_profile.sync
  end

  # Internal: save Profile if the user has changed their profile fields
  def update_profile
    if profile_updated?
      was_bio_set = profile.bio_changed? && profile.bio_was.nil?
      profile.save
      accept_tos
      @profile_updated = false
      if was_bio_set
        user_type = organization? ? "organization" : "user"
        GitHub.dogstats.increment("user", tags: ["type:#{user_type}", "action:profile_bio_set"])
      end
    end
  end

  # Internal: Sync the site admin status of the user with the global business
  # admin status.
  def sync_site_admin_and_global_business_admin
    return unless GitHub.single_business_environment? && GitHub.global_business
    return unless user?

    if site_admin?
      GitHub.global_business.add_owner(self, actor: nil)
    else
      GitHub.global_business.remove_owner(self, actor: nil, send_notification: false)
    end
  end

  def update_business_user_account_login
    return if GitHub.single_business_environment?
    return unless saved_change_to_login?

    BusinessUserAccount.where(user_id: self.id).update_all(login: self.login)
  end

  # Internal: Does this user's login need to satisfy the standard formatting
  # rules for a user login?
  #
  # Returns a Boolean.
  def validates_login_format?
    true
  end

  def email_address_required?
    # Don't require email addresses when authentication is done
    # using an external source (SAML, LDAP, etc.), since we may
    # not have an email addresses available when creating accounts.
    !GitHub.auth.external?
  end

  # Get user's time zone or return default zone.
  #
  # Returns ActiveSupport::TimeZone.
  def time_zone
    ActiveSupport::TimeZone[time_zone_name.to_s] || Time.zone
  end

  # Set user's time zone.
  #
  # zone - ActiveSupport::TimeZone
  #
  # Returns nothing.
  def time_zone=(zone)
    self.time_zone_name = zone.name
  end

  # Get user's mobile time zone or return default zone.
  #
  # Returns ActiveSupport::TimeZone.
  def mobile_time_zone
    async_mobile_time_zone.sync
  end

  def async_mobile_time_zone
    async_profile.then do |profile|
      mobile_time_zone_name = profile.mobile_time_zone_name
      ActiveSupport::TimeZone[mobile_time_zone_name.to_s] || Time.zone
    end
  end

  # Flag to know whether this user's using GitHub for Mac.
  def mac_app_enabled?
    return unless id
    return @mac_app_enabled if defined?(@mac_app_enabled)

    @mac_app_enabled = ClientApplicationSet.new(id).include?(:github_for_mac)
  end

  def enable_mac_app
    return unless id
    @mac_app_enabled = true

    if ClientApplicationSet.new(id).add(:github_for_mac)
      GitHub.dogstats.increment("app", tags: ["action:install", "type:mac"])
    end

    Interaction.track_mac(self)
  end

  # Flag to know whether this user's using GitHub for Windows.
  def windows_app_enabled?
    return unless id
    return @windows_app_enabled if defined?(@windows_app_enabled)

    @windows_app_enabled =
      ClientApplicationSet.new(id).include?(:github_for_windows)
  end

  def enable_windows_app
    return unless id
    @windows_app_enabled = true

    if ClientApplicationSet.new(id).add(:github_for_windows)
      GitHub.dogstats.increment("app", tags: ["action:install", "type:windows"])
    end

    Interaction.track_windows(self)
  end

  # Flag to know whether this user's using GitHub Desktop TNG.
  def desktop_app_enabled?
    return unless id
    return @desktop_app_enabled if defined?(@desktop_app_enabled)

    @desktop_app_enabled = ClientApplicationSet.new(id).include?(:github_desktop)
  end

  def enable_desktop_app(platform)
    return unless id
    @desktop_app_enabled = true

    if ClientApplicationSet.new(id).add(:github_desktop)
      GitHub.dogstats.increment("desktop.install")
    end

    if platform == :mac
      Interaction.track_desktop_mac(self)
    elsif platform == :windows
      Interaction.track_desktop_windows(self)
    end
  end

  def visual_studio_app_enabled?
    return unless id
    return @visual_studio_app_enabled if defined?(@visual_studio_app_enabled)

    @visual_studio_app_enabled =
      ClientApplicationSet.new(id).include?(:github_for_visual_studio)
  end

  def enable_visual_studio_app
    return unless id
    @visual_studio_app_enabled = true

    if ClientApplicationSet.new(id).add(:github_for_visual_studio)
      GitHub.dogstats.increment("app", tags: ["action:install", "type:visualstudio"])
    end
  end

  def xcode_app_enabled?
    return unless id
    return @xcode_app_enabled if defined?(@xcode_app_enabled)
    @xcode_app_enabled = ClientApplicationSet.new(id).include?(:xcode)
  end

  def enable_xcode_app
    return unless id
    @xcode_app_enabled = true
    if ClientApplicationSet.new(id).add(:xcode)
      GitHub.dogstats.increment("xcode.install")
      GitHub.dogstats.increment("app", tags: ["action:install", "type:xcode"])
    end
  end

  # Does this user have any of the native apps enabled?
  #
  # user - User
  #
  # Returns true if so, and false otherwise.
  def has_app_enabled?
    mac_app_enabled? || windows_app_enabled? || visual_studio_app_enabled? || desktop_app_enabled? || xcode_app_enabled?
  end

  def indicator_mode
    indicator_checker.mode(self)
  end

  def indicator_checker
    @indicator_checker ||= IndicatorChecker.new
  end

  # Public: Disables a user's account and removes all access to GitHub -
  # intended for GitHub staff only when a laptop or other device is lost.
  #
  # Returns Boolean
  def staff_revoke(actor = nil, reason = "Staff revocation")
    return false unless id && (site_admin? || employee?)

    payload = {
      prefix: :staff,
      reason: reason,
    }

    if actor.present?
      payload[:actor] = actor
    end

    instrument :revoke, payload

    suspend(reason)
    public_keys.destroy_all
    oauth_accesses.destroy_all
  end

  # Public: Persist metadata associated with the last user login.
  def save_login_metadata(options = {})
    self.last_ip = options[:ip]
    ActiveRecord::Base.connected_to(role: :writing) do
      save if changed?
    end
  end

  include Stratocaster::EventTarget

  # Public: Generates the Stratocaster event key for a User
  #
  # :type  - A Symbol identifying a sub type, or nil.
  #          :actor                - The private feed of actions this user did.
  #          :actor_public         - The public feed of actions this user did.
  #          :user_public          - The public feed of actions this user is watching.
  #          :user_received_events - The private feed of actions this user is watching,
  #                                  surfaced in the GET /users/:user_id/received_events API endpoint.
  #          :org          - The private feed of organization actions.  Requires
  #                          a :param option.
  # :param - Provides more context for a feed type.  The :org type needs an
  #          Organization.
  #
  # Returns a String key.
  def events_key(options = {})
    type = options[:type] || :actor
    case type
    when :actor then "actor:#{id}"
    when :actor_public then "actor:#{id}:public"
    when :user_public then "user:#{id}:public"
    when :user then "user:#{id}"
    when :user_received_events then "user:#{id}:received_events"
    when :org
      param = options[:param]
      if !param.is_a?(Organization)
        raise ArgumentError, "Need a valid Organization :param: #{options.inspect}"
      end
      "org:#{param.id}"
    end
  end

  # Public: Clear a user's public and private activity
  #
  # Returns true if successful
  def clear_all_timelines
    user_timeline = events_key(type: :user)
    user_public_timeline = events_key(type: :user_public)
    GitHub.stratocaster.clear_timelines(user_timeline, user_public_timeline)
    instrument(:clear_all_timelines)

    true
  end

  def event_prefix() :user end
  def event_payload
    {
      event_prefix => self,
    }
  end

  def event_context(prefix: event_prefix)
    {
      prefix => login,
      "#{prefix}_id".to_sym => id,
    }
  end

  # Track which versions of the Terms of Service the user has accepted
  def accept_tos
    TosAcceptance.github_sql.run <<-SQL, user_id: id, sha: TosAcceptance.current_sha, now: Time.now
      INSERT INTO tos_acceptances (user_id, sha, created_at, updated_at)
      VALUES (:user_id, :sha, :now, :now)
      ON DUPLICATE KEY UPDATE updated_at = :now
    SQL
  end

  # Public: Get an instance for modifying settings on this user's profile page.
  #
  # Returns a User::ProfileSettings.
  def profile_settings
    User::ProfileSettings.new(self)
  end

  # Public: Instrument new user creation.
  #
  # Returns nothing.
  def instrument_creation
    instrument :create, {
      actor: self,
      email: email,
      plan: plan.try(:name),
      solved_interactive_captcha: !!solved_interactive_captcha,
    }

    instrument_user_signup
  end

  def instrument_user_signup
    GlobalInstrumenter.instrument "user.signup", {
      actor: self,
      signup_email: emails.first,
    }
  end

  attr_accessor :solved_interactive_captcha

  # Public: Instrument user login rename.
  def instrument_rename
    previous_login, current_login = saved_changes["login"]

    GlobalInstrumenter.instrument "account.rename", {
      # actor: ..., actor taken from GitHub.context as staff can rename accounts
      account: self,
      previous_login: previous_login,
      current_login: current_login,
    }
  end

  # Public: Instrument last_ip update.
  def instrument_last_ip_update
    previous_last_ip, current_last_ip = saved_changes["last_ip"]

    GlobalInstrumenter.instrument "user.last_ip_update", {
      actor: self,
      previous_last_ip: previous_last_ip,
      current_last_ip: current_last_ip,
    }
  end

  # By the time we instrument the deletion, the user has already been deleted
  # so we won't be able to look at its email unless we memoize it here
  def memoize_email_for_instrument_deletion
    @email_for_instrument_deletion = email || billing_email
  end

  # Public: Instrument user deletion.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_deletion
    instrument :delete, {
      email: @email_for_instrument_deletion,
      plan: GitHub.enterprise? ? "enterprise" : "free",
    }
  end

  # Public: Instrument successful user login.
  #
  # Returns nothing.
  def instrument_login(payload)
    instrument :login, payload.merge(
      user: login,
      user_id: id,
      actor: login,
      actor_id: id,
      two_factor: !!two_factor_authentication_enabled?,
    )
  end

  # Private: Instrument authenticated device status
  #
  # Returns nothing.
  # Public: Instrument the status change of an authenticated device
  # authenticated device approval status.
  #
  # action - The action being performed by the user
  # device - The device the action was performed on
  # actor - The staff user verifying the device on behalf of the user
  # reason - The method by which the device became verified (if applicable)
  #
  # Returns nothing.
  def instrument_unverified_device(action, device, actor: nil, reason: nil)
    payload = {
      actor: actor || self,
      device_id: device.id,
      device_cookie: device.device_id,
      display_name: device.display_name,
      approved_at: device.approved_at,
    }

    payload[:reason] = reason if reason

    instrument action, payload

    tags = ["action:#{action}"]
    tags << "reason:#{reason}" if reason
    GitHub.dogstats.increment("authenticated_device", tags: tags)
  end

  # Public: Instrument successful unexpected user login. This occurs when a web
  # login comes from a country or device (or both) the user has not previously
  # logged in from.
  #
  # authentication_record: An AuthenticationRecord
  #
  # Returns nothing.
  def instrument_unexpected_login(authentication_record)
    reason = authentication_record.flagged_reason
    unless AuthenticationRecord::UNEXPECTED_SIGN_IN_REASONS.include?(reason.to_s)
      raise ArgumentError, "Unrecognized reason: #{reason}"
    end

    payload = {
      country: authentication_record.country_code || "Unknown",
      user_session_id: authentication_record.user_session_id,
    }

    event = if authentication_record.two_factor_partial_sign_in?
      :correct_password
    else
      :sign_in
    end

    instrument "#{event}_from_#{reason}", payload
  end

  # Public: Instrument failed user logins for users that do not exist.  This
  # is a class method since there is no instance of User that corresponds
  # to the associated login attempt.
  #
  # Returns nothing.
  def self.instrument_failed_login(payload = {})
    GitHub.instrument "user.failed_login", payload
  end

  # Public: Instrument failed user login.
  #
  # Returns nothing.
  def instrument_failed_login(payload = {})
    instrument :failed_login, payload.merge(
      user: login,
      actor: login,
      actor_id: id,
      user_id: id,
    )
  end

  # Public: Instrument a partial two factor email followup
  # This occurs when a user has begun the two factor process but
  # never completes the sign in. This is a sign of suspicious behavior
  # and is useful for staff to have access to this information
  #
  # Returns nothing.
  def instrument_partial_two_factor_email_followup(record)
    instrument :partial_two_factor_email_followup, {
      authentication_record_id: record.id,
      flagged_reason: record[:flagged_reason],
      time_of_sign_in: record[:created_at],
    }
  end

  # Public: Instrument disabling/enabling a User for billing
  #
  # Returns nothing
  def instrument_disabled(payload = nil)
    payload = (payload || {}).merge(
      user_id: id,
      login: login  ,
      plan: plan.try(:name),
    )

    instrument (disabled? ? :disabled : :enabled), payload
  end

  # Public: Instrument enabling/disabling User preference for
  # showing private contributions.
  #
  # enabled - Accepts a Boolean
  #
  # Returns nothing.
  def instrument_private_contributions(enabled:, payload: {})
    payload = payload.merge(
      actor: self,
    )

    if enabled
      GitHub.dogstats.increment "user", tags: ["action:show_private_contributions_count"]
      instrument :show_private_contributions_count, payload
    else
      GitHub.dogstats.increment "user", tags: ["action:hide_private_contributions_count"]
      instrument :hide_private_contributions_count, payload
    end
  end

  def record_plan_change_transaction
    transactions.create!(action: disabled? ? "disabled" : "enabled")
  end

  class MissingUserWhenAddingToSearch < RuntimeError; end

  # Public: Determine if this user should be added to the search index. Spammy
  # users are excluded from the search index.
  #
  # Returns true or false.
  def searchable?
    return false if !GitHub.elasticsearch_access_allowed? # is Elasticsearch enabled?
    return false if spammy?                 # is this user spammy
    return false if GitHub.enterprise? && system_account?
    true
  end

  # Public: Synchronize this user with its representation in the search
  # index. If the user is newly created or modified in some fashion, then it
  # will be updated in the search index. If the user has been destroyed, then
  # it will be removed from the search index. This method handles both cases.
  #
  # *args - Anything. Accepted so method can be used as an association callback.
  #
  # Returns this User instance.
  #
  def synchronize_search_index(*args)
    return self unless self.searchable?

    raise MissingUserWhenAddingToSearch unless self.id

    if self.destroyed?
      RemoveFromSearchIndexJob.perform_later("user", self.id)
    else
      Search.add_to_search_index("user", self.id)
    end

    self
  end

  # Internal: queue a job to do tasks needed after signup
  #
  #
  # Returns nothing.
  def queue_signup_tasks
    UserSignupJob.perform_later(id)
  end

  # Internal: Report data about the user creation to Octolytics.
  #
  #
  # Returns nothing.
  def report_creation_to_octolytics
    return unless GitHub.user_creation_analytics_enabled?
    return unless self.user?

    GitHub.analytics.record([{
      event_type: "user_create",
      timestamp: self.created_at,
      dimensions: { id: self.id.to_s },
      measures: {},
      context: { login: self.login, email: self.email },
    }])
  end

  # Public: The last time the user was active. Fit for human consumption.
  #
  # Returns a timestamp or "No activity".
  def last_active
    last_active_timestamp || "No activity"
  end

  # Public: The last date the user was active.
  #
  # Returns a timestamp or nil if no activity
  def last_active_timestamp
    [last_stratocaster_event_at, last_active_session_at].compact.max
  end

  # Public: The last time the user has performed an action within GitHub that
  # generated a Stratocaster event.
  #
  # Returns ActiveSupport::TimeWithZone
  def last_stratocaster_event_at
    return @last_stratocaster_event_at if defined?(@last_stratocaster_event_at)
    @last_stratocaster_event_at = begin
      if GitHub.stratocaster_event_timestamp_cache_enabled? &&
        cached_last_event_at = GitHub.kv.get(stratocaster_event_timestamp_cache_key).value { nil }
        Time.parse(cached_last_event_at).in_time_zone
      else
        recent_event_id = Stratocaster::Response.new { GitHub.stratocaster.ids(events_key) }.items.first
        event = GitHub.stratocaster.get(recent_event_id) if recent_event_id
        event.created_at.in_time_zone if event
      end
    end
  end

  def stratocaster_event_timestamp_cache_key
    "user.last-stratocaster-event-timestamp.#{self.id}"
  end

  # Public: The last time the user had an active session.
  #
  # Returns ActiveSupport::TimeWithZone
  def last_active_session_at
    if session = most_recent_session
      session.accessed_at.in_time_zone
    end
  end

  def claimable?
    last_active_timestamp.nil? && repositories.blank?
  end

  # Public: Check for recent staff note.
  #
  # since - time to consider "recent". Defaults to six months.
  #
  # Returns true if there's a recent note and false otherwise.
  def recent_staff_note?(since = 6.months.ago)
    return false if staff_notes.empty?
    staff_notes.last.created_at > since
  end

  # Public: Returns a bolean indicating if the user has joined in the last month
  def joined_in_last_month?
    created_at > 1.month.ago
  end

  # Public: Returns the number of other users with the same login.
  #
  # Returns count of duplicate account logins
  def duplicate_login_count
    @duplicate_login_count ||= User.where(login: login).count - 1
  end

  # Public: Checks for another user with the same login
  #
  # Returns Boolean
  def has_duplicate_login?
    (duplicate_login_count > 0)
  end

  # Public: Returns the other User objects with the same login.
  #
  # Returns an Array of User objects.
  def duplicate_login_accounts
    @duplicate_login_accounts ||= User.where("login IN (?) AND id NOT IN (?)", self.login, self.id).to_a
  end

  # Public: Returns the numbers of UserEmail records belonging to some
  # other User but share an e-mail address with one of the current
  # user's UserEmail records.
  #
  # Returns Integer count of duplicate e-mail records
  def duplicate_email_count
    @duplicate_email_count ||= UserEmail.count_by_sql(["SELECT COUNT(*) FROM user_emails WHERE email IN (?) AND id NOT IN (?)", self.emails.map(&:email), self.emails.map(&:id)])
  end

  # Public: Returns the UserEmail records that share an e-mail address
  # with this user but don't belong to this user.
  #
  # Returns an Array of UserEmail objects.
  def duplicate_user_emails
    @duplicate_email_accounts ||=
      UserEmail.where("email IN (?) AND id NOT IN (?)",
                      self.emails.map(&:email), self.emails.map(&:id))
  end

  # Public: Checks for another email record that shares its address with
  # one of this particular user's records.
  #
  # Returns Boolean
  def has_duplicate_email?
    (duplicate_email_count > 0)
  end

  # Public: Checks if user's login or e-mail is a duplicate of another user.
  #
  # Returns Boolean
  def has_duplicate_information?
    has_duplicate_login? || has_duplicate_email?
  end

  # Public - Should we be hiding ourselves from this viewing user?
  # If we are spammy, and the user trying to view us is not self,
  # nor is it a staff member, then run and hide.  This is primarily
  # for use in the Users controller to 404 spammy accounts.
  #
  # viewer - a User object
  #
  # Returns a Boolean
  def hide_from_user?(viewer)
    # If we're not spammy or suspended, stand up proud
    return false unless spammy? || suspended?

    # Can't hide from dotcom staff & Enterprise admins
    return false if viewer && viewer.site_admin?

    # If you belong to a spammy organization, should still be able to see
    # that organization.
    if organization?
      return false if adminable_by?(viewer) ||
                      direct_or_team_member?(viewer) ||
                      billing_manager?(viewer)
    end

    # Always hide if spammy users are hidden and the user is spammy
    return true if spammy? && viewer != self

    # Can't hide when suspended users are visible
    return false if GitHub.suspended_users_visible? && suspended?

    # Hide from anonymous users
    return true if viewer.nil?

    # Don't hide from yourself
    viewer != self
  end

  # Public - get all the repositories that have anonymous git access
  # enabled for this user/org
  #
  # Returns: ActiveRelation of repositories
  def anonymous_access_repositories
    public_repositories.with_anonymous_git_access
  end

  # Public - Can the given actor create a repository for this user (self)?
  #
  # A user can create repositories for itself, cannot for other users,
  # and uses Organization logic otherwise.
  #
  # actor - The User or Organization wanting to create a repository.
  #
  # Returns Boolean
  def can_create_repository?(actor, visibility: nil)
    # visibility is here to make it easier to call this whether Organization or User.
    authorization = ContentAuthorizer.authorize(actor, :repo, :create, owner: self)
    authorization.authorized?
  end

  def can_own_repositories?
    true
  end

  def async_url(suffix = "")
    template = Addressable::Template.new("/{login}#{suffix}")
    template.expand login: login
  end

  # Internal: Can this user have granular permissions on resources?
  #
  # Returns false.
  def can_have_granular_permissions?
    false
  end

  # Internal: Can this user have granular user permissions on resources?
  #
  # Returns false.
  def can_have_granular_user_permissions?
    false
  end

  # Internal: Can this User type ever act for an Integration?
  #
  # Should only ever be overwritten by Bot to return true.
  def can_act_for_integration?
    false
  end

  # Checks if the given user should use transfer requests when moving a
  # repository to this user
  #
  # Returns a boolean
  def requires_transfer_requests_from?(user, repository_visibility = nil)
    # repository_visibility is here to match signature of Organization#requires_transfer_requests_from?
    self != user
  end

  # Public: Does the user receive an email when the user's account is destroyed?
  #
  # Returns a Boolean.
  def receives_confirmation_when_destroyed?
    return false if GitHub.enterprise?

    !suspended? && gh_role != "staff_delete" && !spammy?
  end

  # Whether to show the staff badge on the profile. We show the staff badge
  # if the viewer is an employee OR if the user has opted to display the badge
  # publicly
  #
  # Returns true if we should show the staff badge
  def show_staff_badge_to?(viewer)
    return true if login == GitHub.staff_user_login

    employee? && (profile_display_staff_badge || viewer&.employee?)
  end

  # Checks if the user has an education coupon code on their account
  #
  # Returns true if education, otherwise false
  def education?
    coupons.education.any? || emails.any?(&:education?)
  end

  # Checks if the user has an student pack coupon code on their account
  #
  # Returns true if student pack coupon is active, otherwise false
  def student_developer_pack_coupon?
    return false unless has_an_active_coupon?

    /\A#{Coupon::STUDENT_DEVELOPER_PACK_NAME}/.match?(coupon.code)
  end

  # Checks if the user has an .edu email
  def edu_email?
    emails.any?(&:education?)
  end

  # Public: Check if current user account is created within 7 days ago.
  #
  # user: User object to check for
  #
  # Returns a Boolean.
  def is_new?
    created_at >= 7.days.ago
  end

  # Calculates what vertical, if any, a use falls within
  #
  # force_recalculation - skip the value cached in the database calculate live
  #
  # Returns the Vertical, or nil if none
  def vertical(force_recalculation = false)
    return if GitHub.enterprise?
    return user_vertical.vertical unless force_recalculation || user_vertical.nil?

    Vertical.find_by_name :education if education?
  end

  # Calculates the vertical via the .vertical method, saves it to the user_verticals table
  #
  # Forces validation if the vertical changed, which may raise an error
  #
  # Returns the Vertical
  def set_vertical!
    # Don't overrite the vertical if the vertical is already set
    return user_vertical.vertical if user_vertical

    vertical = vertical(true)
    return vertical if user_vertical && vertical == user_vertical

    ActiveRecord::Base.connected_to(role: :writing) do
      if vertical.nil?
        user_vertical.destroy if user_vertical
      elsif user_vertical
        user_vertical.update! vertical_id: vertical.id
      else
        UserVertical.create! vertical_id: vertical.id, user: self
      end
    end

    vertical unless vertical.nil?
  end

  def avatar_editable_by?(user)
    user == self
  end

  # Public - Check whether or not the user's login can be changed.
  #
  # Returns false if authentication doesn't allow renaming.
  # Returns false if authentication is ldap and there is no user mapping.
  # Checks if spammy users are allowed to rename
  # Returns true otherwise.
  def renaming_enabled?
    return if spammy? && !spammy_renaming_overridden?
    GitHub.auth.user_renaming_enabled?(self)
  end

  # Public - Check whether or not the user can change their own display name.
  #
  # Returns false if authentication doesn't allow a user changing their own display name.
  #  - authentication is ldap and user is authenticated via the directory (LDAP mapped).
  # Returns true otherwise.
  def change_profile_name_enabled?
    GitHub.auth.user_change_profile_name_enabled?(self)
  end

  # Public - Check whether or not the user can change their own email addresses.
  #
  # Returns false if authentication doesn't allow a user changing their own email addresses.
  #  - authentication is ldap, ldap sync of emails is enabled and user is authenticated via the directory (LDAP mapped).
  # Returns true otherwise.
  def change_email_enabled?
    GitHub.auth.user_change_email_enabled?(self)
  end

  # Public - Check whether or not the user can change their own SSH keys.
  #
  # Returns false if authentication doesn't allow a user changing their own ssh keys.
  #  - authentication is ldap, ldap sync of ssh keys is enabled and user is authenticated via the directory (LDAP mapped).
  # Returns true otherwise.
  def change_ssh_key_enabled?
    GitHub.auth.user_change_ssh_key_enabled?(self)
  end

  # Public - Check whether or not the user can change their own GPG keys.
  #
  # Returns false if authentication doesn't allow a user changing their own GPG keys.
  #  - authentication is ldap, ldap sync of GPG keys is enabled and user is authenticated via the directory (LDAP mapped).
  # Returns true otherwise.
  def change_gpg_key_enabled?
    GitHub.auth.user_change_gpg_key_enabled?(self)
  end

  # Defaults to false until toggled in stafftools
  def spammy_renaming_overridden?
    Stafftools::SpammyRenameDeleteOverride.overridden?(user: self, toggle_type: :renaming)
  end
  public :spammy_renaming_overridden?

  # Defaults to false until toggled in stafftools
  def spammy_deleting_overridden?
    Stafftools::SpammyRenameDeleteOverride.overridden?(user: self, toggle_type: :deleting)
  end
  public :spammy_deleting_overridden?

  # Defaults to false until toggled in stafftools
  def spammy_orgs_renaming_overridden?
    Stafftools::SpammyRenameDeleteOverride.orgs_overridden?(user: self, toggle_type: :renaming)
  end
  public :spammy_orgs_renaming_overridden?

  # Defaults to false until toggled in stafftools
  def spammy_orgs_deleting_overridden?
    Stafftools::SpammyRenameDeleteOverride.orgs_overridden?(user: self, toggle_type: :deleting)
  end
  public :spammy_orgs_deleting_overridden?

  # Public - determine if the user has ANY unverified public keys.
  #
  # Returns true if any key is unverified
  # Returns false if all keys are verified or there are no keys to verify.
  def unverified_public_keys?
    public_keys.any? { |key| !key.verified? }
  end

  # Public: Check whether or not the user's IP is from a known Tor node.
  #
  # WARNING: You probably (definitely) don't want to call this directly in the
  # request-response cycle. For most use-cases, use `anonymizing_proxy_user?`.
  #
  # Returns a Boolean.
  def from_tor_node?
    !!last_ip && Spam.is_tor_node?(last_ip)
  end

  # Public: Has this user ever had a session from a known anonymizing proxy
  # (e.g. Tor)?
  #
  # Returns a Boolean.
  def anonymizing_proxy_user?
    !!has_used_anonymizing_proxy
  end

  # Public - Which layouts should be used when viewing this in site admin
  #
  # Returns "user" for users, and "organization" for orgs
  def site_admin_context
    "user"
  end

  def devtools_scope?
    site_admin? || github_developer?
  end

  def biztools_scope?
    site_admin? || biztools_user?
  end

  def show_blocked_contributors_warning?
    return false if organization?
    return true unless self.interaction_setting
    self.interaction_setting.show_blocked_contributors_warning
  end

  def rebuild_asset_status(notify: true, manual: false)
    RebuildStorageUsageJob.perform_later(id, {"notify" => notify, "manual" => manual})
  end

  def build_asset_status!
    return if asset_status
    Asset::Status.build_for_owner(:lfs, id)
    association(:asset_status).reset
  end

  def email_spamminess_checks_enabled?
    true
  end

  # Internal: can the user be subscribed to notifications
  #
  # Returns true
  def newsies_enabled?
    true
  end

  # Public: Can the user be assigned to an issue?
  #
  # Returns a Boolean.
  def assignable_to_issues?
    !spammy? && !suspended?
  end

  # Public: Unassign the user from all issues they were assigned to. If there's
  # an ActiveRecord error during this process, it'll be reported to Haystack
  # and execution will continue.
  #
  # scope - Optional scope restricting which issues the user should be
  #         unassigned from.
  #
  # Returns nothing.
  def clear_issue_assignments(scope: Issue)
    scope.assigned_to(self).includes(:assignments).each do |issue|
      issue.remove_assignees(self)

      if self.being_destroyed?
        construct_future_event do
          Hook::Event::IssuesEvent.new(
            action: :unassigned,
            issue_id: issue.id,
            actor_id: actor.try(:id),
            assignee_id: self.id,
            triggered_at: Time.now,
          )
        end
      end

      begin
        GitHub::SchemaDomain.allowing_cross_domain_transactions { issue.save! }
      rescue ActiveRecord::RecordInvalid => error
        log_issue_event_details(issue)
        Failbot.report(error)
      rescue ActiveRecord::ActiveRecordError => error
        Failbot.report(error)
      end
    end
  end

  # Internal: a temporary addition to log more info when saving a user's issue
  # fails. Ref https://github.com/github/github/issues/128215
  private def log_issue_event_details(issue)
    issue.events.reject(&:valid?).each do |event|
      GitHub::Logger.log(
        msg: "clear_issue_assignments.issue.save.fail",
        issue_id: issue.id,
        actor_id: event.actor_id,
        event: event.event,
        repository_id: event.repository_id,
      )
    end
  end

  # Internal: If the current user is being destroyed, we need to assemble various
  # payloads. The user is going to go away, and we need to make sure they are
  # still represented in hooks being fired, either as the actor or the subject.
  #
  # event - Required kwarg indicating which event is being delivered later
  #
  # Returns the current user.
  def construct_future_event(&block)
    raise "Erroneous call to `construct_future_events': User.#{self.id} is not being destroyed" unless self.being_destroyed?
    @delivery_system ||= []
    @delivery_system << Hook::DeliverySystem.new(yield)
    @delivery_system.last.generate_hookshot_payloads
  end

  # Internal - Check if the user can access a given installation.
  def can_access_installation?(installation)
    IntegrationInstallation.with_user(self).include? installation
  end

  # Internal: Does this account have any IntegrationInstallations on all repositories
  #
  # Returns an Array
  def installations_on_all_repositories
    prefix = "#{Repository::Resources::ALL_ABILITY_TYPE_PREFIX}%"

    sql = Permission.github_sql.new(<<-SQL, user_id: self.id, prefix: prefix)
      SELECT actor_id FROM permissions
      WHERE subject_id = :user_id
      AND subject_type LIKE :prefix
    SQL

    actor_ids = sql.results.flatten
    IntegrationInstallation.where(id: actor_ids).distinct
  end

  # Public: Has the user signed the pre-release agreement.
  #
  # Returns a Boolean.
  def prerelease_agreement_signed?
    async_prerelease_agreement_signed?.sync
  end

  def async_prerelease_agreement_signed?
    Platform::Loaders::ActiveRecordAssociation.load(self, :prerelease_agreement).then do |agreement|
      !!agreement
    end
  end

  # Public: have any Orgs on this User signed the prerelease agreement?
  def async_org_prerelease_agreement_signed?
    Promise.all(organizations.map(&:async_prerelease_agreement)).then do |agreements|
      agreements.any? { |x| !x.nil? }
    end
  end

  # Public: Returns true if we have placed a hold on this user so we don't purge
  # their deleted repositories.
  def legal_hold?
    legal_hold.present?
  end

  # Public: place a legal hold on this user so we don't purge their deleted
  # repositories.
  #
  # actor - The user that is placing this legal hold.
  #
  # Returns a Boolean.
  def place_legal_hold(actor:)
    return false if legal_hold?

    hold = build_legal_hold

    if hold.save
      auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
      GitHub.instrument "staff.place_legal_hold", event_context.merge(auditing_actor)
      true
    else
      false
    end
  end

  # Public: clear a legal hold on this user. Their deleted repositories will be
  # eligible for pruning.
  #
  # actor - The user that is placing this legal hold.
  #
  # Returns a Boolean.
  def clear_legal_hold(actor:)
    return false unless legal_hold?

    if legal_hold.destroy
      auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
      GitHub.instrument "staff.clear_legal_hold", event_context.merge(auditing_actor)
      true
    else
      false
    end
  end

  def wants_vulnerability_ui_alerts?
    settings_response = GitHub.newsies.settings(self)
    return true if settings_response.success? && settings_response.vulnerability_ui_alert?
  end

  def wants_vulnerability_cli_notifications?
    settings_response = GitHub.newsies.settings(self)
    return false unless settings_response
    return true if settings_response.success? && settings_response.vulnerability_cli?
  end

  def self.account_deletion_phrase
    "delete my account"
  end

  def edit_history_onboarding_key(content)
    "user.show_edit_history_onboarding.#{self.id}.#{content.class.name}.#{content.id}"
  end

  def never_show_edit_history_onboarding_key
    "user.show_edit_history_onboarding.#{self.id}"
  end

  def enable_edit_history_onboarding(content)
    unless GitHub.kv.get(never_show_edit_history_onboarding_key).value!
      GitHub.kv.set(edit_history_onboarding_key(content), "true")
    end
  end

  def dismiss_edit_history_onboarding(content)
    GitHub.kv.del(edit_history_onboarding_key(content))
    GitHub.kv.set(never_show_edit_history_onboarding_key, "true")
  end

  def show_edit_history_onboarding?(content)
    !!(GitHub.kv.get(edit_history_onboarding_key(content)).value! &&
    !GitHub.kv.get(never_show_edit_history_onboarding_key).value!)
  end

  # Public: Returns a list of a user's repositories visible to the actor
  #
  # actor - The User trying to view repositories.
  #
  def visible_repositories_for(actor)
    if actor != self
      repositories.public_or_accessible_by(actor)
    else
      repositories
    end
  end

  # Public: Can the specified actor view projects owned by this user?
  #
  # actor - The User trying to view projects.
  #
  # Returns a boolean.
  def projects_readable_by?(actor)
    # For now, integrations cannot view user projects.
    actor.nil? || !actor.can_have_granular_permissions?
  end

  # Public: Can the specified actor view projects owned by this user?
  #
  # actor - The User trying to view projects.
  #
  # Returns Promise<bool>
  def async_projects_readable_by?(actor)
    Promise.resolve(projects_readable_by?(actor))
  end

  # Public: Can the specified actor create/edit projects on this user?
  #
  # actor - The User trying to create/edit projects.
  #
  # Returns a boolean.
  def projects_writable_by?(actor)
    # For now, only the user can create projects belonging to them.
    actor == self
  end

  # Public: Can the specified actor create/edit projects on this user?
  #
  # actor - The User trying to create/edit projects.
  #
  # Returns Promise<bool>
  def async_projects_writable_by?(actor)
    Promise.resolve(projects_writable_by?(actor))
  end

  # Public: Can this user display the Pro badge on their profile?
  #
  # Returns a boolean
  def can_have_pro_badge?
    !employee? && plan.pro_badge?
  end

  # Public: Get verified email addresses that this user has that match the
  #         verified domains of a specified organization.
  #
  # organization - The Organization to check verified domains from.
  #
  # Returns a Promise<[UserEmail]>.
  def async_verified_domain_emails_for(organization)
    return Promise.resolve([]) unless GitHub.domain_verification_enabled?

    organization.async_member?(self).then do |is_member|
      next [] unless is_member
      Platform::Loaders::VerifiedDomainEmailsByUser.load(organization, id)
    end
  end

  def memex_column_hash
    {
      avatar_url: primary_avatar_url,
      id: id,
      login: login,
      url: permalink,
    }
  end

  def memex_suggestion_hash(selected:)
    memex_column_hash.merge({
      name: profile_name,
      selected: selected,
    })
  end

  # Public: returns the designated successor for the user
  #
  # Returns a single record of SuccessorInvitation.
  def get_successor_invitation
    @successor_invitation ||= successor_invitations.last
  end

  # Public: Checks whether a user is eligible to be a successor to the actor
  # A successor shouldn't be blocking the actor and vice versa, shouldn't be
  # or suspended at the time of designation
  #
  # Returns a boolean.
  def succeedable_by?(user)
    return false unless user.present?
    user != self && !user.blocking?(self) && !blocking?(user) && !user.suspended?
  end

  # Public: Returns true if this user has accepted an invitation to become
  # the inviter's account successor. Returns false if that invitation has been
  # subsequently declined, canceled, or revoked.
  #
  # Returns a boolean.
  def account_successor_for?(inviter)
    return false unless inviter.present?
    received_successor_invitations.accepted.exists?(inviter: inviter)
  end

  # Public: Returns true if this user has an accepted successor invitation.
  # Returns false if there is no existing accepted invitation.
  #
  # Returns a boolean.
  def has_successor?
   return false if get_successor_invitation.nil?

   get_successor_invitation.accepted?
  end

  def global_health_files_repository
    async_global_health_files_repository.sync
  end

  def async_global_health_files_repository
    Platform::Loaders::GlobalHealthFilesRepository.load(id)
  end

  private

  def update_repos_after_rename(old_login, new_login)
    # Keep the owner_login up to date with the new name
    repositories.update_all(owner_login: new_login)

    # Route all repos to new login.
    repositories.reload.each do |repo|
      GitHub::Spokes.client.write_nwo_file(repo, repo.name_with_owner)

      # Page builds must be triggered by a human. Fetch the last human pusher, if possible.
      builder = repo.gh_pages_rebuilder(self)

      # by this point, the username has renamed, so is_user_pages_repo? is always false
      repo.rebuild_pages(builder) if builder && repo.page && repo.name.downcase != "#{old_login.to_s.downcase}.#{pages_host_name}"
      repo.redirect_from_previous_location("#{old_login}/#{repo.name}")
      repo.public_keys.each(&:save)

      if !old_login.casecmp?(login) && RetiredNamespace.should_retire?(repo)
        RetiredNamespace.retire_redirects!(repo)
      end

      Search.add_to_search_index("repository", repo.id) if repo.repo_is_searchable?
    end
  end

  # Private: returns true if the user can't read the entity.
  def unreadable?(entity)
    if entity.respond_to?(:readable_by?)
      !entity.readable_by?(self)
    end
  end

  # Private: returns true if the entity's owner is ignoring the user.
  def being_ignored?(entity)
    entity.try(:owner)&.ignore?(self)
  end

  def clear_spam_flag_if_whitelisted
    if spammy && never_spammy?
      self.spammy = false
    end
    true
  end

  # Public - Disable all notifications for this user.
  #
  # Returns nil.
  def disable_all_notifications
    GitHub.newsies.get_and_update_settings self do |settings|
      settings.participating_settings.clear
      settings.subscribed_settings.clear
      settings.vulnerability_cli = false
      settings.vulnerability_web = false
      settings.vulnerability_email = false
      settings.continuous_integration_web = false
      settings.continuous_integration_email = false
    end
  end
  public :disable_all_notifications

  def preferred_notifications_query_key
    "user.notifications_query.#{id}"
  end

  def notifications_group_by_list_key
    "user.notifications_group_by_list.#{id}"
  end

  def notifications_dismissed_unwatch_suggestions_key
    "user.notifications_dismissed_unwatch_suggestions.#{id}"
  end

  # Public - Verify a browser session for an application given a key from the OAuth handshake
  #
  # Returns true if found, false otherwise
  def active_browser_session_for_application?(application, browser_session_value)
    sessions.active.any? do |session|
      session.valid_for_oauth_application?(application, browser_session_value)
    end
  end
  public :active_browser_session_for_application?

  # Helper method for transferring all gists to another user. Only for console
  # use until we have time to build out Stafftools interface for transferring.
  def transfer_gists_to(new_owner, verbose = $console)
    gists.each do |gist|
      puts "transferring #{gist.nwo} to #{new_owner}" if verbose
      gist.transfer_to(new_owner)
    end
  end
  public :transfer_gists_to

  # Public - Get the possible email addresses that a user can select from to use
  # as their public profile email address.
  #
  # Returns an Array of Strings representing the email addresses
  def possible_profile_emails
    if GitHub.email_verification_enabled?
      emails.user_entered_emails.verified.map(&:to_s)
    else
      emails.user_entered_emails.map(&:to_s)
    end
  end
  public :possible_profile_emails

  # Internal: Should seat limit enforcement be skipped? Currently used to skip
  # seat limit enforcement during the import process of a migration.
  #
  # Returns false unless GitHub.importing? is true.
  def skip_seat_limit_enforcement?
    GitHub.importing?
  end

  # Public: toggle the rejection of pushes with private email addresses
  def toggle_warn_private_email
    if !self.warn_private_email?
      GitHub.dogstats.increment "user.email.privacy.warning_toggle.on"
    else
      GitHub.dogstats.increment "user.email.privacy.warning_toggle.off"
    end

    instrument :toggle_warn_private_email, warn_private_email: !self.warn_private_email? ? "enabled" : "disabled"
    update!(warn_private_email: !self.warn_private_email?)
  end
  public :toggle_warn_private_email

  # Always reuse the same destroy user callback because we have state that
  # is shared across the callback chain. This is bad, but can't be avoided without
  # serious refactoring.
  def destroy_user_callbacks
    @destroy_callback ||= DestroyUserCallbacks.new
  end

  def destroy_user_callbacks_before_destroy
    self.destroying = true
    destroy_user_callbacks.before_destroy(self)
  end

  def destroy_user_callbacks_after_commit
    destroy_user_callbacks.after_commit(self)
    @delivery_system.map(&:deliver_later) if @delivery_system.present?
  end

  def limit_bio_length?
    user? && profile_updated?
  end

  def reset_memoized_attributes
    remove_instance_variable(:@must_verify_email) if defined?(@must_verify_email)
    remove_instance_variable(:@async_has_unlocked_repository) if defined?(@async_has_unlocked_repository)
    remove_instance_variable(:@dormant) if defined?(@dormant)
    remove_instance_variable(:@private_repo_count_for_limit_check) if defined?(@private_repo_count_for_limit_check)
    remove_instance_variable(:@default_associated_repository_ids) if defined?(@default_associated_repository_ids)
    remove_instance_variable(:@business_admin_ability_ids) if defined?(@business_admin_ability_ids)
    remove_instance_variable(:@business_billing_management_ability_ids) if defined?(@business_billing_management_ability_ids)
    remove_instance_variable(:@async_membership_via_org_ids) if defined?(@async_membership_via_org_ids)
    remove_instance_variable(:@starred_repos_count) if defined?(@starred_repos_count)
    remove_instance_variable(:@following_users_count) if defined?(@following_users_count)
  end

  # Private: Used to check if this user is a member of the gh_role 'staff', but
  # not an employee - except when that's not required to be staff.
  #
  # Returns true if the user is staff but not an employee when they should be.
  #
  # Note: These would certainly be better to live in roles_dependency; but we
  # force before_save callbacks into the base model and I wanted this all
  # together.
  def is_staff_but_not_employee?
    gh_role == "staff" && GitHub.require_employee_for_site_admin? && !employee?
  end

  # Private: Used to enforce that a staff user is also an employee if
  # someone on the console attempts to force the issue with update_attribute.
  #
  # Raises an ArgumentError if this user is not an employee; and that
  # enforcement is appropriate.
  def staff_must_be_an_employee!
    if is_staff_but_not_employee?
      raise ArgumentError, "Staff must also be on the employees team."
    end
  end

  # Private: Custom validation method to prevent accidentally creating staff
  # who are not employees from the console.
  def staff_must_be_an_employee
    if is_staff_but_not_employee?
      errors.add(:gh_role, "staff must also be on the employees team. If this is you, try toggling staff mode with the ` key.")
    end
  end

  def same_business?(other_user)
    false
  end

  # Private: Remove leading @ from submitted Twitter username before persisting
  def normalize_profile_twitter_username
    return unless profile_twitter_username
    self.profile_twitter_username = profile_twitter_username.gsub(/\A@/, "").strip.presence
  end

  MAX_THROTTLE_RETRIES = 5

  private def destroy_has_many_association(association)
    reflection = self.class.reflect_on_association(association)
    klass = reflection.klass

    association = self.send(association)

    if association.loaded?
      # If the association was loaded already, destroy records in small batches
      association.each_slice(BATCH_SIZE) do |records|
        klass.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          association.destroy(records)
        end
      end
    else
      # If the association is not loaded, load and destroy records in small batches
      relation = association.limit(BATCH_SIZE)

      loop do
        records = relation.each { |r| r.destroyed_by_association = reflection }

        destroyed_count = klass.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
          association.destroy(records).count
        end

        relation.reset

        break if destroyed_count < BATCH_SIZE
      end
    end
  end

  private def delete_has_many_association(association)
    reflection = self.class.reflect_on_association(association)
    klass = reflection.klass

    relation = self.send(association).limit(BATCH_SIZE)

    loop do
      deleted_count = klass.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        relation.delete_all
      end

      break if deleted_count < BATCH_SIZE
    end
  end
end
