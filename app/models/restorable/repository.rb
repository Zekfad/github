# rubocop:disable Style/FrozenStringLiteralComment

# Internal: Belongs to a Restorable and stores the watched repositories
# for a user. The user association is stored on a Restorable scenario class
#  like Restorable::OrganizationUser.
#
# Usage:
#
#  > user = User.find_by_login("lizzhale")
#  > repo = Repository.with_name_with_owner("github/linguist")
#  > restorable = Restorable.create
#
#  > Restorable::Repository.backup(:restorable => restorable, :repositories => [repo])
#  > Restorable::Repository.restore(:restorable => restorable, :user => user)
#
# This class and all of it's methods should only ever be used by other
# Restorable classes and specifically Restorable scenario classes/models like
# Restorable::OrganizationUser.
class Restorable
  class Repository < ApplicationRecord::Domain::Restorables
    areas_of_responsibility :restorables

    # A set of common restorable type model helper methods.
    extend TypeHelpers

    belongs_to :restorable

    validates_presence_of :restorable_id, :archived_repository_id
    validates_uniqueness_of :restorable_id, scope: :archived_repository_id

    # Internal: Create a set of repository records.
    #
    # restorable - Restorable parent instance.
    # repositories - An array of repositories.
    def self.backup(restorable:, repositories:)
      save_models(restorable, repositories)
    end

    # Internal: Restore forks for this user.
    #
    # restorable - Restorable parent instance.
    # user - User instance.
    def self.restore(restorable:)
      restorable.restoring(:restorable_repositories)

      measure_restore_time do
        repository_ids = restorable.repositories.pluck(:archived_repository_id)
        archived_repository_ids = Archived::Repository.where(id: repository_ids).pluck(:id)

        archived_repository_ids.each do |archived_repository_id|
          throttle do
            ::Repository.restore(archived_repository_id)
          end
        end
      end

      restorable.restored(:restorable_repositories)
    end

    # Internal: Bulk insert sql.
    #
    # Returns a String.
    def self.insert_ignore_sql
      <<-SQL
        INSERT IGNORE INTO
          restorable_repositories
          (restorable_id,archived_repository_id)
        VALUES
          :values
      SQL
    end

    # Internal: Create an array with needed values from models.
    #
    # restorable_id - Integer id of Restorable.
    # repositories - Array of repositories.
    #
    # Returns an Array of arrays.
    def self.values(restorable_id, repositories)
      repositories.map do |repository|
        [
          restorable_id, # restorable_id
          repository.id, # archived_repository_id
        ]
      end
    end

    # Internal: The metric name for statsd.
    #
    # Returns a Symbol.
    def self.metric_name
      :repository
    end
  end
end
