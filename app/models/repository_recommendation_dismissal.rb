# frozen_string_literal: true

class RepositoryRecommendationDismissal < ApplicationRecord::Domain::Users
  belongs_to :user
  belongs_to :repository

  validates :user, :repository, presence: true
  validates :user_id, uniqueness: { scope: :repository_id }
end
