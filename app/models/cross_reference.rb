# rubocop:disable Style/FrozenStringLiteralComment

# When a `Referrer` object mentions a `Referenceable` object, a
# `CrossReference` is created.
#
# The referrer is the source, the referenceable is the target, and the actor is
# the user that mentioned the target from the source.
class CrossReference < ApplicationRecord::Domain::Repositories
  include Spam::Spammable
  include GitHub::Relay::GlobalIdentification

  belongs_to :source, polymorphic: true, required: true
  belongs_to :target, polymorphic: true, required: true
  belongs_to :actor, class_name: "User", required: true

  validates :target_id, uniqueness: { scope: [:source_id, :source_type, :target_type] }

  setup_spammable(:actor)

  # Scopes for filtering the referencing object type.
  scope :descending, -> { order("created_at DESC") }
  scope :issues,     -> { where(source_type: "Issue") }
  scope :milestones, -> { where(source_type: "Milestone") }
  scope :teams,      -> { where(source_type: "Team") }

  scope :referencing, -> (type) { where(target_type: type) }

  # Return a new Relation that filters out `CrossReference`s
  # which target an issue that no longer exists.
  scope :with_valid_target_issue, -> {
    joins(<<~SQL)
      INNER JOIN `issues` `target_issues` ON
        `cross_references`.`target_id` = `target_issues`.`id` AND
        `cross_references`.`target_type` = 'Issue'
    SQL
  }

  # Return a new Relation that filters out `CrossReference`
  # which were made from an issue that no longer exists.
  scope :with_valid_source_issue, -> {
    joins(<<~SQL)
      INNER JOIN `issues` `source_issues` ON
        `cross_references`.`source_id` = `source_issues`.`id` AND
        `cross_references`.`source_type` = 'Issue'
    SQL
  }

  # Return a new Relation that filters out `CrossReference`s
  # which target a repository that no longer exists.
  scope :with_valid_target_issue_repository, -> {
    with_valid_target_issue.joins(<<~SQL)
      INNER JOIN `repositories` `target_issue_repositories` ON
        `target_issues`.`repository_id` = `target_issue_repositories`.`id`
    SQL
  }

  # Return a new Relation that filters out `CrossReference`s
  # which were made from a repository that no longer exists.
  scope :with_valid_source_issue_repository, -> {
    with_valid_source_issue.joins(<<~SQL)
      INNER JOIN `repositories` `source_issue_repositories` ON
        `source_issues`.`repository_id` = `source_issue_repositories`.`id`
    SQL
  }

  scope :with_spammy_source_issues_hidden_for, ->(viewer) {
    return if !GitHub.spamminess_check_enabled? || viewer&.site_admin?

    if viewer
      with_valid_source_issue.where("`source_issues`.`user_hidden` = false OR `source_issues`.`user_id` = ?", viewer.id)
    else
      with_valid_source_issue.where("`source_issues`.`user_hidden` = false")
    end
  }

  scope :with_issues_from_spammy_repositories_hidden_for, ->(viewer) {
    return if !GitHub.spamminess_check_enabled? || viewer&.site_admin?

    with_valid_source_issue_repository.where("`source_issue_repositories`.`user_hidden` = false")
  }

  # Return a new Relation that selects only `CrossReference`s
  # that were made before the target issue got locked.
  scope :created_before_target_conversation_was_locked, -> {
    with_valid_target_issue.joins(<<~SQL).
      LEFT OUTER JOIN `conversations` `target_conversations` ON
        `target_conversations`.`subject_id` = `target_issues`.`id` AND
        `target_conversations`.`subject_type` = 'Issue' AND
        `target_conversations`.`state` = 'locked'

      LEFT OUTER JOIN `issue_events` `last_locked_events` ON
        `last_locked_events`.`event` = 'locked' AND
        `last_locked_events`.`issue_id` = `target_conversations`.`subject_id`

      LEFT OUTER JOIN `issue_events` `later_locked_events` ON
        `later_locked_events`.`event` = 'locked' AND
        `later_locked_events`.`issue_id` = `target_conversations`.`subject_id` AND
        `later_locked_events`.`id` >  `last_locked_events`.`id`
    SQL
    where(<<~SQL)
      `target_conversations`.`id` IS NULL OR
      `last_locked_events`.`id` IS NULL OR (
        `later_locked_events`.`id` IS NULL AND
        `cross_references`.`created_at` < `last_locked_events`.`created_at`
      )
    SQL
  }

  # Return records based on source
  #
  # Converted from a named scope because `from` is defined in Rails and
  # raises an error because of conflicting scopes on Rails 4.1.
  def self.from(source)
    where(source_id: source, source_type: source.class.name)
  end

  # See IssueTimeline
  def timeline_sort_by
    [created_at]
  end

  def referenced_at
    attributes["referenced_at"] || created_at
  end

  def visible_to?(viewer)
    async_visible_to?(viewer).sync
  end

  def async_source_issue_or_pull_request
    async_source.then do |issue|
      if issue&.pull_request_id
        issue.async_pull_request
      else
        issue
      end
    end
  end

  def async_target_issue_or_pull_request
    async_target.then do |issue|
      if issue&.pull_request_id
        issue.async_pull_request
      else
        issue
      end
    end
  end

  # Public: Will the source close the target when merged?
  #
  # Returns a boolean
  def async_will_close_target?
    Promise.all([
      async_source_issue_or_pull_request,
      async_target_issue_or_pull_request,
    ]).then do |source, target|
      # Only open PRs can close targets
      next false unless source&.open? && target&.open?
      next false unless source.is_a?(PullRequest)
      next false if target.is_a?(PullRequest)

      source.async_repository.then do |source_repo|
        next false unless source_repo
        target.async_close_issue_references.then do
          Promise.resolve(target.may_be_closed_by?(source.id))
        end
      end
    end
  end

  # Public: Is this a reference to an issue/PR on a different repository from the one where it was made?
  #
  # Returns a boolean
  def async_cross_repository?
    Promise.all([
      async_source,
      async_target,
    ]).then do |source, target|
      next unless source && target
      source.repository_id != target.repository_id
    end
  end

  def async_path_uri
    Promise.all([
      async_source,
      async_target,
    ]).then do |source, target|
      next unless source && target
      target.async_path_uri.then do |path|
        noun = source.pull_request_id ? "pullrequest" : "issue"
        event_path = path.dup
        event_path.fragment = "ref-#{noun}-#{source.id}"
        event_path
      end
    end
  end

  def async_visible_to?(viewer)
    async_source.then do |source|
      # This check and the next are needed because we have broken associations
      # (https://github.com/github/github/pull/74589#discussion_r122992323)
      next false unless source

      source.async_repository.then do |repository|
        next false unless repository

        if source.is_a?(Issue)
          next false if !repository.has_issues? && !source.pull_request_id
          source.async_readable_by?(viewer)
        else
          # This code is verified dead (see the previous change), but since the
          # class is polymorphic, we'll keep it here to (🤞) handle non-issue cases.
          repository.async_readable_by(viewer)
        end
      end
    end
  end

  def platform_type_name
    "CrossReferencedEvent"
  end
end
