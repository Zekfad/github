# frozen_string_literal: true

class DiscussionContributorFinder
  def self.contributor_user_ids_for(repository:, user_ids:, viewer:)
    new(repository: repository, user_ids: user_ids, viewer: viewer).contributor_user_ids
  end

  def initialize(repository:, user_ids:, viewer:)
    @repository = repository
    @user_ids = user_ids
    @viewer = viewer
  end

  def contributor_user_ids
    pull_request_user_ids + commit_user_ids
  end

  private

  attr_reader :repository, :user_ids, :viewer

  def pull_request_user_ids
    @_pull_request_user_ids ||= repository.
      pull_requests.
      filter_spam_for(viewer).
      for_user(user_ids).
      distinct.
      pluck(:user_id)
  end

  def commit_user_ids
    @_commit_user_ids ||= begin
      remaining_user_ids = user_ids - pull_request_user_ids

      if remaining_user_ids.any?
        repository.
          commit_contributions.
          for_user(remaining_user_ids).
          distinct.
          pluck(:user_id)
      else
        []
      end
    end
  end
end
