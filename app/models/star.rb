# rubocop:disable Style/FrozenStringLiteralComment

class Star < ApplicationRecord::Domain::Stars
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification
  include Spam::Spammable

  # Blacklisted/excluded from e.g. explore newsletter and /trending
  LEADERBOARD_EXCLUSIONS = [
    1300192,   # octocat/spoon-knife
    1334369,   # resume/resume.github.com
    23082495,  # sebyddd/YouAreAwesome
    28457823,  # FreeCodeCamp/FreeCodeCamp
    32334018,  # letsgetrandy/DICSS
    41103286,  # lbryio/lbry
    135785971, # upend/IF_MS_BUYS_GITHUB_IMMA_OUT
    152327866, # Igglybuff/awesome-piracy
  ].freeze

  if GitHub.enterprise?
    OWNER_EXCLUSIONS = [].freeze
  else
    OWNER_EXCLUSIONS = [
      4312013, # api-playground
    ].freeze
  end

  STARRABLE_TYPE_GIST = "Gist".freeze
  STARRABLE_TYPE_REPOSITORY = "Repository".freeze
  STARRABLE_TYPE_TOPIC = "Topic".freeze

  STARRABLE_TYPES = [
    STARRABLE_TYPE_GIST,
    STARRABLE_TYPE_REPOSITORY,
    STARRABLE_TYPE_TOPIC,
  ].freeze

  # Locations on the site that we show the Star/Unstar buttons
  STARRABLE_CONTEXTS = %w[collections trending repository repo_stargazers user_stars gist
                          discovery_feed api news_feed topic].freeze

  class << self
    attr_accessor :per_page
  end
  self.per_page = 30

  after_create :update_user_star_counter_cache
  after_commit :instrument_create, on: :create
  after_destroy :update_user_star_counter_cache
  after_commit :instrument_destroy, on: :destroy

  belongs_to :user
  belongs_to :starrable, polymorphic: true

  # This `belongs_to` doesn't work on its own, but is useful to make
  # it possible to join Star queries to repositories,
  # e.g. `Star.alive` does `joins(:repository)`
  belongs_to :repository,
    -> { where("stars.starrable_type" => STARRABLE_TYPE_REPOSITORY) },
    foreign_key: "starrable_id"

  setup_spammable(:user)

  validates_presence_of :user
  validates_presence_of :starrable_id, :starrable_type

  validates :starrable_type, inclusion: { in: STARRABLE_TYPES }
  validate :validate_star_is_authorized, on: :create

  scope :newest, -> { order("created_at DESC") }
  scope :repositories, -> { where(starrable_type: STARRABLE_TYPE_REPOSITORY) }
  scope :topics, -> { where(starrable_type: STARRABLE_TYPE_TOPIC) }
  scope :since, -> (date) { where("stars.created_at >= ?", date) }

  # Find the most starred repositories based on who the user follows
  #
  # user    - required, the user who we're looking up their followings
  # period  - daily, weekly, monthly. the time period to look for the stars
  # limit   - how many results to return
  #
  # Returns Array of Stars
  def self.from_following(user, options = {})
    period  = options[:period] || "daily"
    limit = options[:limit] || 25

    following_ids = user.following_ids
    return [] if following_ids.empty?

    repos = ActiveRecord::Base.connected_to(role: :reading) do
      repo_sql = github_sql.new(
        following_ids: following_ids,
        since: timestamp_for_period(period),
        exclude_repos: options[:exclude_repos],
      )

      repo_sql.add <<~SQL
        SELECT COUNT(starrable_id) AS friend_count, starrable_id, starrable_type
        FROM stars
        WHERE user_id IN :following_ids
          AND created_at BETWEEN :since AND NOW()
          AND starrable_type = 'Repository'
          #{ "AND starrable_id NOT IN :exclude_repos" if options[:exclude_repos].present? }
        GROUP BY starrable_type, starrable_id
        ORDER BY friend_count DESC, starrable_id
        LIMIT 1000
      SQL

      rows = repo_sql.hash_results

      repository_ids = rows.map { |row| row["starrable_id"] }
      public_repo_ids = Repository.public_scope.where(id: repository_ids).ids.to_set

      rows.select! { |row| public_repo_ids.include?(row["starrable_id"]) }
      rows.first(limit).map { |row| Star.instantiate(row) }
    end

    topics = ActiveRecord::Base.connected_to(role: :reading) do
      topic_sql = github_sql.new(
        user_id: user.id,
        limit: limit,
        since: timestamp_for_period(period),
        exclude_topics: options[:exclude_topics],
        user_ids: following_ids,
      )

      topic_sql.add <<~SQL
        SELECT COUNT(starrable_id) AS friend_count, starrable_id, starrable_type
        FROM stars
        WHERE user_id IN :user_ids
          AND created_at BETWEEN :since AND NOW()
          AND starrable_type = 'Topic'
          #{ "AND starrable_id NOT IN :exclude_topics" if options[:exclude_topics].present? }
        GROUP BY starrable_type, starrable_id
        ORDER BY friend_count DESC, starrable_id
        LIMIT :limit
      SQL

      topic_sql.models(Star)
    end

    # sort by stargazer_count, with a secondary ordering to keep results consistent
    (repos + topics).sort_by { |star| [star.starrable.stargazer_count, star.starrable_id] }.first(limit)
  end

  # Picks the date range for star queries.
  #
  # period - The time period to go back until (required)
  #
  # Returns ActiveSupport::TimeWithZone
  def self.timestamp_for_period(period)
    case period.to_s
      when "weekly" then 1.week.ago
      when "monthly" then 1.month.ago
      else 1.day.ago
    end
  end

  # Public: Logs an event for Octolytics about a user starring or unstarring something.
  #
  # viewer - the User who starred or unstarred
  # entity - the Repository, Gist, or Topic that was (un)starred
  # action - "star" or "unstar"
  # context - one of STARRABLE_CONTEXTS to indicate where on this site the (un)star button was
  #           clicked
  #
  # Returns nothing.
  def self.instrument_action(viewer:, entity:, action:, context:)
    starred_type = if entity.class.to_s.in?(STARRABLE_TYPES)
      entity.class.to_s
    else
      "other"
    end

    user_id = if entity.instance_of?(Repository)
      entity.owner_id
    elsif entity.instance_of?(Gist)
      entity.user_id
    end

    context = if STARRABLE_CONTEXTS.include?(context)
      context
    else
      "other"
    end

    GitHub.instrument("stars.star", {
      dimensions: {
        starred_type: starred_type,
        context: context,
        actor_id: viewer.id,
        user_id: user_id,
        entity_id: entity.id,
        action: action,
        public: entity.public?,
      },
    })
  end

  def for_topic?
    starrable.is_a?(Topic)
  end

  def for_repository?
    starrable.is_a?(Repository)
  end

  def event_context(prefix: :starred)
    {
      prefix                  => (starrable.kind_of?(Repository) ? starrable.nwo : starrable.to_s),
      "#{prefix}_type".to_sym => starrable_type,
      "#{prefix}_id".to_sym   => starrable_id,
    }
  end

  private

  def event_payload
    {
      starred:    self,
      user:       user,
      spammy:     user.spammy?,
      allowed:    allowed?,
      star_id:    self.id,
    }
  end

  def allowed?
    !!starrable.try(:permit?, user, :write)
  end

  def instrument_create
    instrument(:create, action: :created)
  end

  def instrument_destroy
    instrument(:destroy, action: :deleted)
  end

  # Private: Updates and returns the user's star counts based on the
  # Repository's visibility.
  #
  # Returns nil or an Integer.
  def update_user_star_counter_cache
    return unless (starrable_type == STARRABLE_TYPE_REPOSITORY) && starrable.present?

    if starrable.public?
      user.update_starred_public_repositories_count!
    else
      user.update_starred_private_repositories_count!
    end
  end

  def validate_star_is_authorized
    authorization = ContentAuthorizer.authorize(user, :star, :create, starrable: starrable)

    if authorization.failed?
      errors.add(:base, authorization.error_messages)
    end
  end
end
