# frozen_string_literal: true

module UserEmail::MarketingDependency
  # Public: Should the email be added to MailChimp?
  # Returns a Boolean.
  def should_be_subscribed_in_mailchimp?
    primary? &&
      verified? &&
      marketing_preference? &&
      valid_mailchimp_email?
  end

  private

  # Private: Is the email viewed as valid by MailChimp?
  # Returns a Boolean.
  def valid_mailchimp_email?
    GitHub::Mailchimp.new(self).valid_email?
  end

  # Private: Does the user accept email marketing communication?
  # Returns a Boolean.
  def marketing_preference?
    NewsletterPreference.marketing?(user: user)
  end
end
