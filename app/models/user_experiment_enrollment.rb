# frozen_string_literal: true

class UserExperimentEnrollment < ApplicationRecord::Domain::UserExperiments
  include Instrumentation::Model

  belongs_to :user_experiment
  belongs_to :subject, polymorphic: true

  after_create_commit :instrument_create

  def self.for_subject(subject)
    where(subject_id: subject.id, subject_type: subject.type)
  end

  def subject
    return unless subject_type? && subject_id?

    if subject_klass < ApplicationRecord::Base
      super
    else
      subject_klass.find(subject_id)
    end
  end

  def subject=(subject)
    if subject.is_a?(ActiveRecord::Base)
      super
    else
      self.subject_id   = subject.id
      self.subject_type = subject.class.name
    end
  end

  def instrument_create
    instrument :create
  end

  def subject_is_a?(klass)
    subject_klass <= klass
  end

  private

  def subject_klass
    subject_type.constantize
  end

  def event_prefix
    :user_experiment_enrollment
  end

  # Default attributes for auditing
  def event_payload
    {
      user_experiment_slug: user_experiment.slug,
      subgroup: subgroup&.to_sym,
      subject_id: subject.id,
      subject_type: subject.class.name,
      user_login: subject_login,
    }
  end

  def subject_login
    subject.login if subject.respond_to?(:login)
  end
end
