# frozen_string_literal: true

class ScopedIntegrationInstallation
  module V2
    class Creator
      MISSING_PULL_REQUEST_ACCESS = "This installation does not have Pull Request access, and therefore cannot grant other pull request based permissions."

      attr_reader :parent, :data

      # Result from the ScopedIntegrationInstallation::V2 creation
      class Result
        class Error < StandardError; end

        def self.success(installation) new(:success, installation: installation) end
        def self.failed(error) new(:failed, error: error) end

        attr_reader :error, :installation

        def initialize(status, installation: nil, error: nil)
          @status       = status
          @installation = installation
          @error        = error
        end

        def success?
          @status == :success
        end

        def failed?
          @status == :failed
        end
      end

      def self.perform(parent, data)
        new(parent, data).perform
      end

      def initialize(parent, data)
        @parent = parent
        @data   = data
      end

      def perform
        GitHub.tracer.with_span("ScopedIntegrationInstallation::V2::Creator#perform") do |span|
          validate_repository_permissions!
          validate_pull_request_permissions!

          ApplicationRecord::Iam.transaction do
            ScopedIntegrationInstallation.transaction do
              create_record!
              grant_permissions
            end
          end

          GitHub.dogstats.increment("scoped_integration_installation.create", tags: ["result:success"])
          Result.success(@scoped_installation)
        rescue Result::Error => e
          GitHub.dogstats.increment("scoped_integration_installation.create", tags: ["result:failed"])
          Result.failed e.message
        end
      end

      private

      # From Permissions::Service.app_attributes
      def app_attributes(subject_id:, subject_type:, action:)
        [
          @scoped_installation.ability_id,
          @scoped_installation.ability_type,
          Permission.actions[action],
          subject_id,
          subject_type,
          Ability.priorities[:direct],
          0, # No need for a parent ID that points to an actual ability record.
          GitHub::SQL::NOW,
          GitHub::SQL::NOW,
          expires_at
        ]
      end

      def create_record!
        @scoped_installation = ActiveRecord::Base.connected_to(role: :writing) do
          ScopedIntegrationInstallation.create!(parent: parent)
        end
      end

      def expires_at
        return @expires_at if defined?(@expires_at)
        @expires_at = ::ScopedIntegrationInstallation::Creator::EXPIRATION_WINDOW.from_now
      end

      def grant_permissions
        rows = []

        repositories.each do |repository|
          repo_hash = data["repositories"].find do |repo|
            repo["id"] == repository.id || repo["name"] == repository.name
          end

          next unless repo_hash

          rows.concat(permission_rows_for_repository(repository, repo_hash["permissions"]))
          rows.concat(permission_rows_for_pull_requests(repository, repo_hash))
        end

        ActiveRecord::Base.connected_to(role: :writing) do
          ::Permissions::Service.grant_permissions(rows)
        end
      end

      def integration
        return @integration if defined?(@integration)
        @integration = parent.integration
      end

      def permission_rows_for_pull_requests(repository, data)
        rows = []

        return rows unless data.key?("pull_requests")

        # [ { "number" => 1, "permissions" => { "sarifs" => "write" } } ]
        # => { 1 => { "sarifs" => :write } }
        pulls_and_permissions = data["pull_requests"].each_with_object({}) do |pull, hash|
          hash[pull["number"]] = pull["permissions"].transform_values(&:to_sym)
        end

        pulls = PullRequest.joins(:issue).where(repository: repository).where("issues.number IN (?)", pulls_and_permissions.keys)

        pulls.pluck(:id, :number).each do |id, number|
          permissions = pulls_and_permissions[number]

          permissions.each_pair do |resource, action|
            subject_type = "#{PullRequest::Resources.parent_type}/#{resource}"
            rows << app_attributes(subject_id: id, subject_type: subject_type, action: action)
          end
        end

        rows
      end

      def permission_rows_for_repository(repository, permissions)
        Repository::Resources.filter(permissions).map do |resource, action|
          subject_type = "#{Repository::Resources.parent_type}/#{resource}"
          app_attributes(subject_id: repository.ability_id, subject_type: subject_type, action: action)
        end
      end

      def target
        return @target if defined?(@target)
        @target = parent.target
      end

      def repositories
        return @repositories if defined?(@repositories)

        ids   = []
        names = []

        data["repositories"].each do |repo|
          # JSON schema guarantees that we will either
          # have a name or ID for each repository provided.
          if repo.key?("id")
            ids << repo["id"]
          else
            names << repo["name"]
          end
        end

        @repositories = parent.repositories.where(id: ids).or(
          parent.repositories.where(name: names)
        )

        requested_count = ids.count + names.count

        # If we find a different number of repositories than what
        # was provided go ahead and fail out.
        if requested_count != @repositories.count
          raise Result::Error, ::ScopedIntegrationInstallation::Permissions::Result::HUMAN_READABLE_REASONS[:repositories_not_available_to_target]
        end

        @repositories = @repositories.select(:id, :name)
      end

      # Private: Take the list of repositories and converge all of the requested
      # permissions into a single Hash.
      #
      # Example:
      #
      # [
      #   { "id" => 1, "permissions" => { "metadata" => "read", "contents" => "read" } },
      #   { "name" => "dotfiles", "permissions" => { "metadata" => "read", "contents" => "write" } }
      # ]
      #
      # => { "metadata" => "read", "contents" => "write" }
      #
      # Returns a Hash.
      def repository_permissions
        return @repository_permissions if defined?(@repository_permissions)

        @repository_permissions = data["repositories"].each_with_object({}) do |repo, hash|
          hash.merge!(repo["permissions"]) do |resource, action1, action2|
            Permission.actions[action1].to_i > Permission.actions[action2].to_i ? action1 : action2
          end
        end

        @repository_permissions = @repository_permissions.transform_values!(&:to_sym)
      end

      def validate_pull_request_permissions!
        GitHub.tracer.with_span("ScopedIntegrationInstallation::V2::Creator#validate_pull_request_permissions") do |span|
          resources = []
          actions   = []

          data["repositories"].each do |repo_hash|
            next unless repo_hash.key?("pull_requests")

            # Make sure each pull request is unique
            numbers = repo_hash["pull_requests"].map { |pull| pull["number"] }

            if numbers.uniq.length != numbers.length
              raise(Result::Error, "There is at least one pull request that has multiple entries")
            end

            repo_hash["pull_requests"].each do |pull_hash|
              resources.concat(pull_hash["permissions"].keys)
              actions.concat(pull_hash["permissions"].values)
            end
          end

          resources.uniq!
          actions.uniq!

          return unless resources.any?
          parent_permissions = parent.permissions

          unless parent.permissions.key?("pull_requests")
            raise(Result::Error, MISSING_PULL_REQUEST_ACCESS)
          end

          # Does the resource they are requesting exist?
          unless (resources - PullRequest::Resources.subject_types).empty?
            raise(Result::Error, ::ScopedIntegrationInstallation::Permissions::Result::HUMAN_READABLE_REASONS[:invalid_resource])
          end

          current_access = Permission.actions[parent_permissions["pull_requests"]]

          actions.each do |action|
            action = Permission.actions[action.to_sym]

            # Are they asking for a valid action (read/write/admin)?
            if action.nil?
              raise(Result::Error, ::ScopedIntegrationInstallation::Permissions::Result::HUMAN_READABLE_REASONS[:invalid_action])
            end

            # Make sure we're not escalating privilege.
            if action > current_access
              raise(Result::Error, ::ScopedIntegrationInstallation::Permissions::Result::HUMAN_READABLE_REASONS[:permissions_added_or_upgraded])
            end
          end
        end
      end

      def validate_repository_permissions!
        GitHub.tracer.with_span("ScopedIntegrationInstallation::V2::Creator#validate_repository_permissions") do |span|
          check = ScopedIntegrationInstallation::Permissions.check(
            installation: parent,
            repositories: repositories,
            action:       :create,
            permissions:  repository_permissions,
          )

          raise(Result::Error, check.error_message) unless check.permitted?
        end
      end
    end
  end
end
