# frozen_string_literal: true

class ScopedIntegrationInstallation
  class Creator
    INSTALL_ON_ALL_REPOSITORIES = :all
    DEFAULT_BATCH_SIZE = 1_000
    EXPIRATION_WINDOW = 2.hours

    attr_reader :parent, :repositories, :permissions

    # Result from the ScopedIntegrationInstallation creation
    class Result
      class Error < StandardError; end

      def self.success(installation) new(:success, installation: installation) end
      def self.failed(error) new(:failed, error: error) end

      attr_reader :error, :installation

      def initialize(status, installation: nil, error: nil)
        @status       = status
        @installation = installation
        @error        = error
      end

      def success?
        @status == :success
      end

      def failed?
        @status == :failed
      end
    end

    def self.perform(parent, repositories: [], permissions: {}, expires: true)
      new(parent, repositories: repositories, permissions: permissions, expires: expires).perform
    end

    def initialize(parent, repositories: [], permissions: {}, expires: true)
      @parent       = parent
      @repositories = repositories
      @expires      = expires
      @expiry       = EXPIRATION_WINDOW.from_now

      unless @repositories == INSTALL_ON_ALL_REPOSITORIES
        @repositories = Array(@repositories)
      end

      @permissions = permissions.present? ? permissions : parent.version.default_permissions
    end

    def perform
      GitHub.tracer.with_span("ScopedIntegrationInstallation::Creator#perform") do |span|
        validate_permissions

        ApplicationRecord::Iam.transaction do
          ScopedIntegrationInstallation.transaction do
            create_record

            if installing_on_all_repositories?
              grant_permissions_to_all_repositories
            else
              grant_permissions_to_repositories
            end

            if parent.target.is_a?(Organization)
              grant_organization_permissions
            end
          end
        end

        GitHub.dogstats.increment("scoped_integration_installation.create", tags: ["result:success"])
        Result.success(@scoped_installation)
      rescue Result::Error => e
        GitHub.dogstats.increment("scoped_integration_installation.create", tags: ["result:failed"])
        Result.failed e.message
      end
    end

    private

    def app_attributes
      @app_attributes ||= {}.tap do |opts|
        opts[:actor] = @scoped_installation

        if expires?
          opts[:expires_at] = @expiry.to_i
        end
      end
    end

    def expires?
      @expires
    end

    def validate_permissions
      GitHub.tracer.with_span("ScopedIntegrationInstallation::Creator#validate_permissions") do |span|
        check = ScopedIntegrationInstallation::Permissions.check(
          installation: parent,
          repositories: repositories,
          action:       :create,
          permissions:  permissions,
        )

        raise(Result::Error, check.error_message) unless check.permitted?
      end
    end

    def create_record
      options = { parent: parent }
      options[:expires_at] = @expiry if expires?

      @scoped_installation = ActiveRecord::Base.connected_to(role: :writing) do
        ScopedIntegrationInstallation.create!(options)
      end
    end

    def grant_permissions_to_repositories
      GitHub.tracer.with_span("ScopedIntegrationInstallation::Creator#grant_permissions_to_repositories") do |span|
        span.set_tag("repositories.count", repositories.count)
        GitHub.dogstats.histogram("scoped_integration_installation.repositories.size", repositories.count)

        grant_permissions_on(subjects: repositories, permissions: transient_version.permissions_of_type(Repository))
      end
    end

    def grant_permissions_to_all_repositories
      GitHub.tracer.with_span("ScopedIntegrationInstallation::Creator#grant_permissions_to_all_repositories") do |span|
        rows = transient_version.permissions_of_type(Repository).map do |permission, action|
          subject = parent.target.repository_resources.public_send(permission.to_s)
          ::Permissions::Service.app_attributes(**app_attributes.merge(subject: subject, action: action))
        end

        grant_permissions(rows)
      end
    end

    def grant_organization_permissions
      GitHub.tracer.with_span("ScopedIntegrationInstallation::Creator#grant_organization_permissions") do |span|
        grant_permissions_on(subjects: [@scoped_installation.target], permissions: transient_version.permissions_of_type(Organization))
      end
    end

    def installing_on_all_repositories?
      @repositories == INSTALL_ON_ALL_REPOSITORIES
    end

    def transient_version
      IntegrationVersion.new(
        integration: parent.integration,
        default_permissions: permissions,
        associated_with_scoped_installation: true,
      )
    end

    def grant_permissions_on(subjects:, permissions:)
      rows = subjects.flat_map do |subject|
        permissions.map do |resource, action|
          resource_subject = subject.resources.public_send(resource)
          ::Permissions::Service.app_attributes(**app_attributes.merge(subject: resource_subject, action: action))
        end
      end

      grant_permissions(rows)
    end

    def grant_permissions(rows)
      ActiveRecord::Base.connected_to(role: :writing) do
        ::Permissions::Service.grant_permissions(rows)
      end
    end
  end
end
