# frozen_string_literal: true

class ScopedIntegrationInstallation
  class Permissions
    attr_reader :installation, :repositories, :action, :permissions, :transient_version

    class Result
      attr_reader :result, :reason

      def self.for(result, reason: nil)
        new(result, reason: reason)
      end

      HUMAN_READABLE_REASONS = {
        missing_parent:                       "A parent installation is required.",
        missing_repositories:                 "No repositories were provided.",
        permissions_added_or_upgraded:        "The permissions requested are not granted to this installation.",
        repositories_not_available_to_target: "There is at least one repository that does not exist or is not accessible to the parent installation.",
        invalid_action:                       "There is at least one permission action that is not supported. It should be one of: \"read\", \"write\" or \"admin\".",
        invalid_resource:                     "There is at least one permission resource that is not supported.",
        not_installed_on_all:                 "This installation is not installed on all repositories.",
      }.freeze

      def initialize(result, reason:)
        @result, @reason = result, reason
      end

      def permitted?
        @result
      end

      def error_message
        HUMAN_READABLE_REASONS.fetch(reason)
      end
    end

    def self.check(installation:, repositories: [], action:, permissions:)
      new(installation, repositories, action, permissions: permissions).check
    end

    def initialize(installation, repositories, action, permissions: {})
      @installation = installation
      @repositories = repositories
      @action       = action
      @permissions  = permissions
    end

    def check
      case action
      when :create
        can_create
      else
        result(false, :action_invalid)
      end
    end

    private

    def can_create
      return result(false, :missing_parent) if installation.nil?

      # If there aren't any permissions then we won't be granting anything
      # other than a high rate limited public access token. But it seems like
      # something we should allow.
      return result(true) if permissions.none?

      permissions_check = validate_permissions
      return permissions_check unless permissions_check.permitted?

      # In the event that repository permissions are being requested,
      # go ahead and validate the users input. If the user has requested
      # repositories and didn't ask for repository permissions then it
      # would be a no-opt anyway.
      if repository_permissions_requested?
        if installing_on_all?
          return result(false, :not_installed_on_all) unless installation.installed_on_all_repositories?
        elsif repositories.any?
          requested_repository_ids = repositories.map(&:id)
          installed_repository_ids = installation.repository_ids(repository_ids: requested_repository_ids)

          # Are there any repositories that were requested
          # that are _not_ part accessible to the parent
          # installation?
          if (requested_repository_ids - installed_repository_ids).any?
            return result(false, :repositories_not_available_to_target)
          end
        else
          return result(false, :missing_repositories)
        end
      end

      result(true)
    end

    def validate_permissions
      # Ensure permissions has valid actions
      begin
        @transient_version = installation.integration.versions.build(
          associated_with_scoped_installation: true,
        ).tap { |v| v.default_permissions = permissions }
      rescue ArgumentError
        return result(false, :invalid_action)
      end

      # Ensure the requested permissions are for valid resources
      if !@transient_version.valid? && @transient_version.errors.include?("default_permission_records.resource")
        return result(false, :invalid_resource)
      end

      # Ensure the permissions are not being added or upgraded
      # from the parent installation
      differ = IntegrationVersion::PermissionsDiffer.perform(
        old_version: installation.version,
        new_version: @transient_version,
      )

      unless differ.auto_upgradeable?
        return result(false, :permissions_added_or_upgraded)
      end

      result(true)
    end

    def repository_permissions_requested?
      return false if @transient_version.nil?
      @transient_version.any_permissions_of_type?(Repository)
    end

    def installing_on_all?
      repositories == ScopedIntegrationInstallation::Creator::INSTALL_ON_ALL_REPOSITORIES
    end

    def result(value, reason = nil)
      Result.for(value, reason: reason)
    end
  end
end
