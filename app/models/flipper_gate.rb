# rubocop:disable Style/FrozenStringLiteralComment

class FlipperGate < ApplicationRecord::Domain::Features
  DETERMINISTIC_TYPES = %w[actors boolean groups]
  PERCENTAGE_TYPES = %W[percentage_of_actors percentage_of_time]
  TYPES = DETERMINISTIC_TYPES + PERCENTAGE_TYPES
  validates :name, presence: true, inclusion: TYPES
  validates :value, presence: true
  belongs_to :flipper_feature

  scope :actor_gates, -> {
    where(name: "actors")
  }

  def actor_gate?
    name == "actors"
  end

  # Internal: load the associated actor for this gate
  def actor
    GitHub::FlipperActor.from_flipper_id(value)
  end

  def actor_type
    GitHub::FlipperActor.class_name_from_flipper_id(value)
  end
end
