# frozen_string_literal: true

class CollectionItem < ApplicationRecord::Ballast
  belongs_to :content, polymorphic: true
  belongs_to :collection, class_name: "ExploreCollection", foreign_key: :collection_id,
    inverse_of: :items

  CONTENT_TYPES = %w(
    CollectionUrl
    CollectionVideo
    Repository
    Organization
    User
  ).freeze

  validates :content_type, inclusion: { in: CONTENT_TYPES }
  validates :content, presence: true
  validates :collection, presence: true

  class << self
    def visible
      select do |item|
        if item.content_type.in?(%w(Repository Organization User))
          item.async_content.then do |content|
            content.present? && !content.spammy? && content.public?
          end.sync
        else
          true
        end
      end
    end
  end
end
