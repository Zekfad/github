# frozen_string_literal: true

module UserHovercard::Contexts
  class PullRequestReviewer < Hovercard::Contexts::Base
    attr_reader :teams

    def initialize(teams:)
      @teams = teams
    end

    def message
      if teams.any?
        team_list = hovercard_sentence(teams, max: 3) { |team| "@#{team.combined_slug}" }
        "Left a review on behalf of #{team_list}"
      else
        "Left a review"
      end
    end

    def octicon
      "comment-discussion"
    end

    def platform_type_name
      "GenericHovercardContext"
    end
  end
end
