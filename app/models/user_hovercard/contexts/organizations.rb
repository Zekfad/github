# frozen_string_literal: true

module UserHovercard::Contexts
  class Organizations < Hovercard::Contexts::Base
    attr_reader :related

    def initialize(related:, all:, user:)
      @related = related
      @all = all
      @user = user
    end

    def message
      org_list = hovercard_sentence(highlighted, max: 3, total: total_organization_count) do |org|
        "@#{org}"
      end

      "Member of #{org_list}"
    end

    # Important organizations to show the full name of (in order)
    def highlighted
      return related if related.any?

      Organization.ranked_for(user, scope: all)
    end

    def octicon
      "organization"
    end

    def total_organization_count
      all.count
    end

    def platform_type_name
      "OrganizationsHovercardContext"
    end

    private

    attr_reader :all, :user
  end
end
