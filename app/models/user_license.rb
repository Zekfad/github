# frozen_string_literal: true

class UserLicense < ApplicationRecord::Collab
  include Instrumentation::Model

  LICENSABLE_FIELDS = %w(email user_id).freeze

  areas_of_responsibility :gitcoin

  belongs_to :business
  belongs_to :user

  enum license_type: [:volume, :enterprise]

  after_create_commit :log_license_create_event
  after_update_commit :log_license_update_event
  after_update_commit :log_licensable_update_event
  after_destroy_commit :log_license_destroy_event

  def self.valid_license_type?(lt)
    license_types[lt].present?
  end

  private

  # Default values to passed to events created by #instrument
  def event_payload
    {
      user_license: self,
      user: user,
      email: email,
      license_type: license_type,
      business: business,
    }
  end

  def log_license_create_event
    instrument :create
  end

  def log_license_update_event
    return unless previous_changes.key?(:license_type)

    instrument :update,
      license_type_was: previous_changes[:license_type].first
  end

  def log_licensable_update_event
    return unless (previous_changes.keys & LICENSABLE_FIELDS).any?

    extra_payload = {}
    extra_payload[:email_was] = previous_changes[:email].first if previous_changes.key?(:email)
    if previous_changes.key?(:user_id)
      user_was = User.find_by(id: previous_changes[:user_id].first)
      extra_payload[:user_was] = user_was&.login
      extra_payload[:user_id_was] = previous_changes[:user_id].first
    end

    instrument :licensable_updated, extra_payload
  end

  def log_license_destroy_event
    instrument :destroy
  end
end
