# rubocop:disable Style/FrozenStringLiteralComment

class RepositoryInvitation < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :community_and_safety

  # Raised when a non-admin attempts to set a permission
  class InsufficientAbilities < ArgumentError; end

  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  belongs_to :repository
  belongs_to :invitee, foreign_key: "invitee_id", class_name: "User"
  belongs_to :inviter, foreign_key: "inviter_id", class_name: "User"
  belongs_to :role, foreign_key: "role_id", class_name: "Role"

  validates_uniqueness_of :invitee_id, scope: :repository_id, allow_blank: true, allow_nil: true
  validates_uniqueness_of :email, scope: :repository_id, allow_blank: true, allow_nil: true, case_sensitive: false
  validates_length_of :email, within: 3..100, allow_blank: true
  validates_format_of :email,
    with: User::EMAIL_REGEX,
    message: "does not look like an email address",
    allow_blank: true
  validates :repository_id, :inviter_id, presence: true
  validate :email_must_be_exclusive
  validate :email_must_not_be_prefixed
  validate :trade_controls_restrictions, on: :create
  validate :rate_limit_not_exceeded
  validate :must_be_org_to_use_roles

  before_validation :generate_token, on: :create, if: :email?
  after_create_commit :instrument_creation
  after_destroy :clean_up_notifications_after_destroy
  after_commit :snapshot_license_state

  enum permissions: { read: 0, write: 1, admin: 2, triage: 3, maintain: 4 }

  INVITATION_UPDATE_BATCH_SIZE = 100
  PERMISSIONS = {
    "pull"  => :read,
    "push"  => :write,
    "admin" => :admin,
    "triage" => :triage,
    "maintain" => :maintain,
  }

  scope :excluding_expired, -> do
    where(created_at: GitHub.invitation_expiry_period.days.ago..)
  end

  # Invite a user to a repository, unless they already belong to the organization
  #
  # invitee  - The user to add as a collaborator to this repository.
  # inviter  - The user who is adding the addee as a collaborator.
  # repository - The specified repository.
  #
  # Returns a Hash with
  #   - success
  #   - if successful, invitation Invitation
  #   - if unsuccessful, errors String
  def self.invite_to_repo(invitee, inviter, repository, action: :write)
    org = repository.organization
    member_belongs_to_same_org = org && org.find_direct_or_team_member_by_login(invitee.login)

    if cannot_invite_because_outside_collaborator?(org, inviter, member_belongs_to_same_org)
      verb = GitHub.repo_invites_enabled? ? "invite" : "add"
      message = "Members cannot #{verb} outside collaborators"
      repository.errors.add(:base, :inviting_outside_collaborators, message: message)
      return { success: false, errors: repository.errors }
    end

    if cannot_invite_because_fork_and_not_in_business?(org, repository, invitee)
      verb = GitHub.repo_invites_enabled? ? "invited" : "added"
      message = "Users outside of the business cannot be #{verb}"
      repository.errors.add(:base, :inviting_user_outside_of_business, message: message)
      return { success: false, errors: repository.errors }
    end

    if !GitHub.repo_invites_enabled? || member_belongs_to_same_org
      invite_to_repo_without_confirmation(invitee, inviter, repository, action: action)
    else
      invite_to_repo_with_confirmation(invitee, inviter, repository, action: action)
    end
  end

  def self.invite_to_repo_with_confirmation(invitee, inviter, repository, action: :write)
    return { success: false, errors: repository.errors } unless repository.can_add_user?(invitee, inviter)

    # some invitations will have a custom role
    role_id, permission = fetch_role_and_permission(action, repository)

    invitation = RepositoryInvitation.new(
      repository_id: repository.id,
      inviter_id: inviter.id,
      invitee_id: invitee.id,
      permissions: permission,
      role_id: role_id
    )
    GitHub::SchemaDomain.allowing_cross_domain_transactions { invitation.save! }

    RepositoryCollabInvitationJob.perform_later(invitation.id)
    { success: true, invitation: invitation }
  rescue ActiveRecord::RecordInvalid => e
    { success: false, errors: e.record.errors }
  end

  # Use case: enterprise mode, or when the invitee belongs to the repo's org
  # Invite a user to a repository and skip the step involving the invitee
  # manually accepting or rejecting the invitation.
  #
  # invitee  - The user to add as a collaborator to this repository.
  # inviter  - The user who is adding the addee as a collaborator.
  # repository - The specified repository.
  #
  # Returns hash with a :success key.
  def self.invite_to_repo_without_confirmation(invitee, inviter, repository, action: :write)
    if !repository.two_factor_requirement_met_by?(invitee)
      repository.errors.add(:base, "User must have two-factor authentication enabled")
      { success: false, errors: repository.errors }
    elsif repository.can_add_user?(invitee, inviter)
      repository.add_member_without_validation_or_notifications(
        invitee,
        inviter,
        action: action,
      )

      response = GitHub.newsies.auto_subscribe(invitee, repository)
      GitHub.newsies.async_auto_subscribe(invitee, [repository.id]) if response.failed?
      { success: true }
    else
      { success: false, errors: repository.errors }
    end
  end

  def self.invite_to_repo_by_email(email, inviter, repository, action: :write)
    return { success: false, errors: repository.errors } unless can_invite_with_email?(repository, inviter)

    known_user = User.find_by_email(email)
    return invite_to_repo(known_user, inviter, repository, action: action) if known_user.present?

    previous_invitation = RepositoryInvitation.find_by(email: email, repository: repository)

    if previous_invitation.present?
      invitation = previous_invitation
    else
      # some invitations will have a custom role
      role_id, permission = fetch_role_and_permission(action, repository)

      invitation = RepositoryInvitation.create!(
        repository_id: repository.id,
        inviter: inviter,
        email: email,
        permissions: permission,
        role_id: role_id
      )
    end

    RepositoryMailer.collab_invited(invitation).deliver_later

    { success: true, invitation: invitation }
  end

  # Can we invite a user through email to the repo?
  def self.can_invite_with_email?(repository, inviter)
    owner = repository.owner

    if owner.organization?
      if cannot_invite_because_outside_collaborator?(owner, inviter, false)
        repository.errors.add(:base, "Organization members cannot invite outside collaborators.")
        return false
      end

      if repository.private? && owner.at_seat_limit?
        repository.errors.add(:base, "You must purchase at least one more seat to invite this user as a collaborator.")
        return false
      end
    else
      if repository.private? && repository.at_seat_limit?
        repository.errors.add(:base, "You must upgrade your account to add more collaborators.")
        return false
      end
    end

    true
  end

  def self.permission_by_name_or_label(text)
    permissions[text] || PERMISSIONS[text]
  end

  def self.cannot_invite_because_outside_collaborator?(org, inviter, member_belongs_to_same_org)
    org && !member_belongs_to_same_org && !org.members_can_invite_outside_collaborators? && !org.adminable_by?(inviter)
  end

  # To be added as a collaborator to an internal fork, invitee must be in the business
  # or must be a collaborator on the root.
  #
  # Returns true if we cannot invite the user for this reason.
  def self.cannot_invite_because_fork_and_not_in_business?(org, repo, invitee)
    return unless repo.internal_fork?

    business = repo.root.owner.business

    invitee_in_business = invitee.business_ids.include?(business.id)
    invitee_collaborator_on_root = repo.root.members(actor_ids: invitee.id).any?

    return true unless invitee_in_business || invitee_collaborator_on_root

    false
  end

  scope :involving, ->(repo_ids, user) {
    where(repository_id: Array(repo_ids))
      .where("inviter_id = ? OR invitee_id = ?", user.id, user.id)
  }

  scope :with_roles, ->(role_ids) {
    where(role_id: Array(role_ids))
  }

  def self.cancel_all_invitations_involving(repo_ids:, user:)
    involving(repo_ids, user).each do |invitation|
      invitation.cancel!(actor: user, force: true)
    end
  end

  # Public: Fetch the hash to use for server-side persistence of the given
  # token.
  #
  # token - A string.
  #
  # Returns the hashed base64 String.
  def self.digest_token(token)
    return nil unless token.present?

    Digest::SHA256.base64digest(token.to_s)
  end

  # Add a user to the repository after they have accepted an invitation.
  #
  # Returns boolean.
  def accept!(acceptor: invitee)
    # Don't allow invitations to be accepted if the invite has expired. Delete the invitation
    if invite_expired?
      GitHub::SchemaDomain.allowing_cross_domain_transactions { self.destroy }
      return false
    end

    # Email invitations don't have an invitee stored, and can be
    # accepted by any user with the correct token. For regular invitations,
    # we need to make sure the acceptor and the invitee are the same.
    unless email?
      return false unless acceptor == invitee
    end

    return unless repository.can_add_user?(acceptor, inviter, already_invited: true)

    action =
      if role
        role.name
      else
        self.permissions || :read
      end

    self.repository.add_member_without_validation_or_notifications(
      acceptor,
      inviter,
      action: action,
    )

    assign_user_to_bundled_license_assignment

    instrument_acceptance
    response = GitHub.newsies.auto_subscribe(acceptor, repository)
    GitHub.newsies.async_auto_subscribe(acceptor, [repository.id]) if response.failed?
    GitHub::SchemaDomain.allowing_cross_domain_transactions { self.destroy }
    self.destroyed?
  end

  # Clean up the invitation after a user declines an invitation.
  #
  # Returns boolean.
  def reject!
    instrument_rejection
    GitHub::SchemaDomain.allowing_cross_domain_transactions { self.destroy }
    self.destroyed?
  end

  # Cancel the invitation.
  #
  # actor - The user that is cancelling the invitation.
  #
  # Returns a Boolean.
  def cancel!(actor:, force: false)
    return false unless repository.adminable_by?(actor) || force
    instrument_cancellation(actor: actor)
    GitHub::SchemaDomain.allowing_cross_domain_transactions { self.destroy }
    self.destroyed?
  end

  # Enqueue a job to cancel the invitation.
  #
  # actor - The user that is cancelling the invitation.
  # force - force the invitation cancellation
  #
  def enqueue_cancel_invitation(actor:)
    CancelRepoInvitationJob.perform_later(actor: actor, invitation: self)
  end

  def event_payload
    {
      repo_invitation_id: self.id,
      inviter: inviter,
      invitee: invitee,
      repo: repository,
    }
  end

  def hydro_payload
    {
      actor: inviter,
      invitee: invitee,
      repository: repository,
    }
  end

  def instrument_creation
    role_name = role ? "custom_role" : permissions
    GitHub.dogstats.increment("repository_invitation", tags: ["action:create", "role:#{role_name}"])
    GlobalInstrumenter.instrument "repository.invite", hydro_payload
    instrument(:create)
  end

  def instrument_acceptance
    GitHub.dogstats.increment("repository_invitation", tags: ["action:accept"])
    instrument(:accept)
  end

  def instrument_rejection
    GitHub.dogstats.increment("repository_invitation", tags: ["action:reject"])
    instrument(:reject)
  end

  def instrument_cancellation(actor:)
    instrument(:cancel, event_payload.merge(actor: actor))
  end

  def mark_read
    response = GitHub.newsies.web.rollup_summary_from_repository_invitation(self)
    if response.success?
      response = GitHub.newsies.web.mark_summary_read(self.invitee, response.value)
    end
    response
  end

  def permalink
    return unless self.repository
    link = "#{self.repository.permalink}/invitations"
    link += "?invitation_token=#{self.token}" if self.token.present?
    link
  end

  def permission_string
    if role
      role.name
    else
      self.permissions || "write"
    end
  end

  # Public: Enqueue a jobs to update `batch_size` invitations at a time.
  #
  # invitations   - The invitations to be updated.
  # setter        - The user that is updating the invitation.
  # action        - The permission or role invitation is being updated to.
  # role          - Custom role (default: nil)
  # batch_size    - Batch size to be passed processed.
  #
  def self.batch_enqueue_update_repo_permissions(invitations:, setter: repository.owner, action:, role: nil, batch_size: INVITATION_UPDATE_BATCH_SIZE)
    invitations.each_slice(batch_size) do |invitations_slice|
      BatchUpdateInvitationRepoPermissionsJob.perform_later(invitations_slice, action: action, setter: setter, role: role)
    end
  end

  def enqueue_update_repo_permissions(setter:, action:)
    ability = evaluate_ability(action, repository)
    UpdateInvitationRepoPermissionsJob.perform_later(self, action: ability, setter: setter)
  end

  def evaluate_ability(action, repository)
    ability =
      if Role.for_repo_owner(name: action, repo: repository)
        action
      else
        permission_to_ability(action)
      end

    raise ArgumentError, "Invalid permission #{action}" if ability.nil?
    ability
  end

  def set_permissions(permission, setter)
    raise ArgumentError, "setter is required!" unless setter
    unless repository.resources.administration.writable_by?(setter)
      raise InsufficientAbilities, "#{setter.inspect} lacks the ability to set #{permission.inspect}"
    end

    # if a custom role is present, we want to clear out the old permission and set the role_id
    # if switching to an enum permission from a custom role, we need to clear out the old role_id
    if role = Role.for_repo_owner(name: permission, repo: repository)
      raise ArgumentError, "Permission: #{permission} must be at least equal to organization's default repo permission" unless role.custom_role_assignable?(repo: repository)
      update(permissions: nil, role_id: role.id)
    else
      action = permission_to_ability(permission)
      raise ArgumentError, "Invalid permission #{permission}" if action.nil?
      update(permissions: action, role_id: nil)
    end
  end

  def summary
    "Invitation to join #{self.repository.name_with_owner} from #{self.inviter.login}"
  end

  def notifications_thread
    self
  end

  def notifications_list
    self.repository
  end

  def notifications_author
    self.inviter
  end

  def async_readable_by?(actor)
    return Promise.resolve(false) unless actor

    if invitee_id == actor.id && !actor.can_have_granular_permissions?
      return Promise.resolve(true)
    end

    async_repository.then do |repo|
      next false unless repo
      repo.resources.administration.readable_by?(actor)
    end
  end

  # Public: Determine if the Invitation is readable
  # by a given actor.
  #
  # - The invitee can see their own invitation
  # - Any actor who can admin the repository can see
  #   the invitation
  # - Any IntegrationInstallation with
  #   { "administration" => :read } on the parent
  #   repository
  #
  # Returns a Boolean.
  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  # Public: Checks whether the invite is older than the set expiry period
  #
  # Returns boolean
  def invite_expired?
    self.created_at.before? GitHub.invitation_expiry_period.days.ago
  end

  # Public: Stores the raw token in-memory so it's available for the duration of
  # the request. Stores a hash of the token in the database to be used to lookup
  # invitations by token.
  #
  # Returns the set token.
  def token=(token_value)
    self.hashed_token = RepositoryInvitation.digest_token(token_value.to_s)
    @token = token_value
  end
  attr_reader :token

  # Public: Finds an invitation matching the supplied token.
  #
  # token   - The token String generated by the RepositoryInvitation.
  #
  # Returns an RepositoryInvitation or nil if the invitation could not be
  # found.
  def self.find_by_token(token)  # rubocop:disable GitHub/FindByDef
    return nil unless token.present?

    where(hashed_token: digest_token(token)).first
  end

  # Public: Resets the token and saves the invitation. Since token is only stored
  # in-memory, this is needed to regenerate links for an inviation (e.g. to resend
  # and invitation email)
  #
  # Returns a Boolean indicating if the reset was successful.
  def reset_token
    return false unless email?

    generate_token && save
  end

  # Public: The String representation of the OrganizationInvitation. If the
  # invitation is for a user it will return the user's login. If the
  # invitation is for an email invitation it will return the email.
  #
  # Returns String.
  def email_or_invitee_login
    if email?
      email
    else
      invitee.login
    end
  end

  private

  def clean_up_notifications_after_destroy
    # in rare cases invitee may already have been deleted
    return if !self.invitee

    list = Newsies::List.new("Repository", repository_id)
    thread = Newsies::Thread.to_object(self)
    summary_response = GitHub.newsies.web.find_rollup_summary_by_thread(list, thread)
    if summary_response.success?
      summary = summary_response.value
      GitHub.newsies.web.delete(self.invitee, summary) if summary
    end
  end

  def outstanding_invitations_in_past_24_hours
    RepositoryInvitation.where("repository_id = ? AND created_at >= ?", self.repository_id, Time.zone.now - 24.hours)
  end

  def rate_limit_not_exceeded
    return if GitHub.enterprise?
    return unless repository.present?
    return if RepositoryInvitationRateLimitOverride.overridden?(repository_id)

    # Ignore rate limit when inviting members from the repo's owning organization:
    return if repository.organization && repository.organization.members(actor_ids: invitee_id).any?

    if exceeded_rate_limit?
      errors.add(:rate_limit, "exceeded")
    end
  end

  def exceeded_rate_limit?
    outstanding_invitations_in_past_24_hours.count >= GitHub.repository_invitation_rate_limit
  end

  def must_be_org_to_use_roles
    return unless repository.present?
    return unless permissions.to_s == "triage" || permissions.to_s == "maintain"
    owner = repository.owner
    unless owner.organization?
      errors.add(:permissions, "is invalid")
    end
  end

  def trade_controls_restrictions
    return unless repository.present?
    if repository.trade_restricted?
      errors.add(:base, TradeControls::Notices::Plaintext.org_invite_restricted)
      return false
    end
    true
  end

  def email_must_be_exclusive
    if !invitee_id? && !email?
      errors.add(:base, "invitee or email must be present")
    elsif invitee_id? && email?
      errors.add(:email, "invitee has already been set")
    end
  end

  def email_must_not_be_prefixed
    if email? && email.start_with?("mailto:")
      errors.add(:email, "should not be prefixed")
    end
  end

  def snapshot_license_state
    return unless repository&.licensing_enabled?

    Billing::SnapshotLicensesJob.perform_later(business)
  end

  def business
    return @business if defined? @business
    @business = repository.owner.business
  end

  def assign_user_to_bundled_license_assignment
    return unless business&.volume_licensing_enabled?
    if invitee
      Billing::SetUserFromEmailsOnBundledLicenseAssignmentJob.perform_later(
        business: business, user: invitee, emails: [email]
      )
    end
  end

  # Given a permission, it returns the symbol for the equivalent ability
  # if the input is already in ability format, it is a no_op
  # if the input is not a valid permission or ability, it returns nil
  def permission_to_ability(permission)
    return PERMISSIONS[permission.to_s] if PERMISSIONS.keys.include? permission.to_s
    return permission.to_sym if PERMISSIONS.values.include? permission.to_sym
  end

  # Private: Generates the token used to lookup email invitations.
  def generate_token
    self.token = SecureRandom.hex(20)
  end

  # Private: return the role_id and the permission
  def self.fetch_role_and_permission(action, repository)
    role = Role.for_repo_owner(name: action, repo: repository)
    role_id = role ? role.id : nil

    permission =
      if role_id
        nil
      else
        permission_by_name_or_label(action) || PERMISSIONS["push"]
      end

    return role_id, permission
  end

  private_class_method :fetch_role_and_permission
end
