# rubocop:disable Style/FrozenStringLiteralComment

class Stafftools::RestoreAccount
  # Public: Restore an account using the details provided. This will not save
  # the account after it's been restored. Users will be restored with a random
  # password and Organizations will be restored with a single admin, the
  # current user.
  #
  # current_user - The user performing the restore
  # details      - A hash of account details
  #   :id      - String or Integer id of account
  #   :login   - String login for account
  #   :email   - String email for account
  #   :plan    - String plan name for account
  #   :was_org - String boolean "true" if this account was an Organization
  #
  # Returns the restored account. This will be a User or Organization.
  def self.perform(*args)
    new(*args).perform
  end

  def initialize(current_user, details)
    @current_user = current_user
    @id = details.fetch(:id).to_i
    @login = details.fetch(:login)
    @email = details.fetch(:email)
    @plan = details.fetch(:plan)
    @was_org = (details.fetch(:was_org) == "true")
  end

  def perform
    was_org ? restore_org : restore_user
  end

  private

  attr_reader :current_user, :id, :login, :email, :plan, :was_org

  def restore_user
    password = SecureRandom.hex(32)

    user = User.new \
      login: login,
      email: email,
      plan: plan,
      password: password,
      password_confirmation: password
    user.id = id

    GitHub.instrument "user.recreate", {user: user}

    user
  end

  def restore_org
    org = Organization.new \
      login: login,
      plan: plan,
      billing_email: email.blank? ? "billing@example.com" : email,
      admins: [current_user]
    org.id = id

    GitHub.instrument "org.recreate", {org: org}

    org
  end
end
