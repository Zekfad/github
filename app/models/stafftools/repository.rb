# frozen_string_literal: true

module Stafftools
  class Repository < SimpleDelegator
    def self.for_team(team, page = 1)
      repos = team.repositories_scope.order(:name).preload(
        :owner,
        :mirror,
        network: :root,
        parent: :owner,
      ).paginate(page: page)

      abilities = team.most_capable_abilities_on_subjects(repos.pluck(:id), subject_type: ::Repository)
      teams = ::Team.where(id: abilities.values.map(&:actor_id)).index_by(&:id)

      mapped_repos = repos.map do |repo|
        ability = abilities[repo.id]
        is_inherited = ability.actor_id != team.id
        origin_name = teams[ability.actor_id].name
        new(repo, ability, is_inherited, origin_name)
      end

      Stafftools::PaginatedCollection.create(repos, mapped_repos)
    end

    attr_reader :ability, :is_inherited, :ability_origin

    def initialize(repo, ability, is_inherited, origin_name)
      @ability = ability
      @is_inherited = is_inherited
      @ability_origin = origin_name
      super repo
    end
  end
end
