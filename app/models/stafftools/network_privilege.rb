# frozen_string_literal: true

# In the event that there is a repository that hosts unsavory or
# unsafe content, stafftools is able to revoke certain privileges
# in order to minimize the amount of exposure the repository gets.

class Stafftools::NetworkPrivilege < ApplicationRecord::Domain::Repositories
  include Instrumentation::Model
  areas_of_responsibility :community_and_safety

  belongs_to :repository
  after_save :recalculate_trending_repos, if: :saved_change_to_hide_from_discovery?

  scope :requires_login_or_collaborators_only, -> {
    where("network_privileges.require_login = ? OR " +
          "network_privileges.collaborators_only = ?", true, true)
  }

  scope :hidden_from_discovery, -> { where(hide_from_discovery: true) }

  private

  # Private: Queue jobs to recalculate trending repositories. We do this
  #          when hiding a repository from discovery.
  def recalculate_trending_repos
    ["daily", "weekly", "monthly"].each do |period|
      CalculateTrendingReposJob.perform_later(period)
    end
  end
end
