# frozen_string_literal: true

class Stafftools::DisabledRepositories
  def self.dmca_takedowns(user)
    phrase = "action:staff.disable_repo data.reason:dmca user_id:#{user.id}"
    query = Audit::Driftwood::Query.new_stafftools_query(
      phrase: phrase,
      current_user: user,
    )
    results = query.execute.results
  end

  def self.dmca_restores(user)
    phrase = "action:staff.enable_repo user_id:#{user.id} from:\"stafftools/dmca_takedowns#destroy\""
    query = Audit::Driftwood::Query.new_stafftools_query(
      phrase: phrase,
      current_user: user,
    )
    query.execute.results
  end
end
