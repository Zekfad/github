# rubocop:disable Style/FrozenStringLiteralComment

class Stafftools::Newsies
  def self.users_watching_repository(repository)
    sql = Newsies::ListSubscription.github_sql.new(<<-SQL, list_id: repository.id)
      SELECT user_id
      FROM   notification_subscriptions
      WHERE  list_type = 'Repository'
      AND    list_id = :list_id
      AND    NOT ignored
    SQL

    ids = sql.results.flatten.uniq
    User.where(id: ids)
  end

  def self.users_ignoring_repository(repository)
    sql = Newsies::ListSubscription.github_sql.new(<<-SQL, list_id: repository.id)
      SELECT user_id
      FROM   notification_subscriptions
      WHERE  list_type = 'Repository'
      AND    list_id = :list_id
      AND    ignored IS TRUE
    SQL

    ids = sql.results.uniq
    User.where(id: ids).order("login ASC")
  end

  def self.subscriptions_for_issue(issue)
    sql = Newsies::ThreadSubscription.github_sql.new(<<-SQL, build_sql_params(issue))
      SELECT user_id, reason
      FROM   notification_thread_subscriptions
      WHERE  list_type = 'Repository'
      AND    list_id = :list_id
      AND    thread_key = :thread_key
      AND    ignored IS FALSE
    SQL

    data = sql.results

    reasons = data.index_by { |id, reason| id }
    ids = reasons.keys
    users = User.where(id: ids).order("login ASC")
    users.map do |user|
      reason = reasons[user.id].last
      Stafftools::IssueSubscription.new(user, reason)
    end
  end

  def self.users_ignoring_issue(issue)
    sql = Newsies::ThreadSubscription.github_sql.new(<<-SQL, build_sql_params(issue))
      SELECT user_id
      FROM   notification_thread_subscriptions
      WHERE  list_type = 'Repository'
      AND    list_id = :list_id
      AND    thread_key = :thread_key
      AND    ignored IS TRUE
    SQL

    ids = sql.results.uniq
    User.where(id: ids).order("login ASC")
  end

  def self.build_sql_params(issue_or_discussion)
    {
      list_id: issue_or_discussion.notifications_list.id,
      thread_key: Newsies::Thread.to_key(issue_or_discussion),
    }
  end
  private_class_method :build_sql_params

end
