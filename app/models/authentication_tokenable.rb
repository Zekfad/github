# frozen_string_literal: true

module AuthenticationTokenable
  extend ActiveSupport::Concern

  included do
    has_many :tokens,
      as:         :authenticatable,
      class_name: "AuthenticationToken",
      dependent:  :destroy
  end

  # Public: Generates a new AuthenticationToken for this record and
  # returns the token's plaintext value for use in making authenticated API
  # requests.
  #
  # Returns a String.
  def generate_token
    _, value = AuthenticationToken.create_for(self)

    value
  end
end
