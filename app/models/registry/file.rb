# frozen_string_literal: true

class Registry::File < ApplicationRecord::Domain::Repositories
  self.table_name = :package_files

  include GitHub::Relay::GlobalIdentification

  LARGE_FILE_SIZE_THRESHOLD = 500.megabytes

  belongs_to :package_version, class_name: "Registry::PackageVersion", counter_cache: true
  after_destroy :destroy_version_if_last_file
  after_destroy :emit_destroy_event
  after_save :duplicate_version_in_mapping_table

  has_many :manifest_entries,
    class_name: "Registry::ManifestEntry",
    foreign_key: :package_file_id,
    dependent: :destroy
  has_many :package_versions, through: :manifest_entries

  enum state: Storage::Uploadable::STATES

  def destroy_if_orphaned
    destroy unless self.manifest_entries.select { |m| m.package_version.version != "docker-base-layer" }.any?
  end

  def self.not_found_url
    @not_found_url ||= ENV["PACKAGE_FILES_NOT_FOUND_URL"] || GitHub.urls.registry_url(:notfound)
  end

  def platform_type_name
    "PackageFile"
  end

  private
  def destroy_version_if_last_file
    with_lock do
      package_version.destroy if package_version.files.empty?
    end
  end

  def emit_destroy_event
    return if repository_id.nil?

    # Emit file deleted hydro event for registry to process
    params = {
        artifact_id: guid,
        storage_service: "AWS_S3",
        repository_id: repository_id,
    }
    GlobalInstrumenter.instrument("package_registry.package_file_destroyed", params)
  end

  def duplicate_version_in_mapping_table
    return unless package_version

    with_lock do
      sql = github_sql.new(
        package_file_id: id,
        package_version_id: package_version_id,
       )

      q = <<-SQL
        INSERT INTO package_version_package_files (package_file_id, package_version_id)
        VALUES (:package_file_id, :package_version_id)
        ON DUPLICATE KEY UPDATE
        package_file_id = :package_file_id,
        package_version_id = :package_version_id
      SQL
      ActiveRecord::Base.connected_to(role: :writing) do
       sql.run(q)
      end
    end
  end

  public

  belongs_to :storage_blob, class_name: "Storage::Blob"

  before_validation :set_guid, on: :create
  validate :check_maven_filename
  validates_inclusion_of :size, in: 1..2.gigabytes
  before_save :ensure_filename

  attr_writer :repository
  attr_accessor :registry_package_name
  attr_accessor :registry_package_type
  attr_accessor :version
  attr_accessor :registry_package_id
  attr_accessor :registry_package
  attr_accessor :platform

  def name
    package_type = self.registry_package_type || (self.package_version && self.package_version.package.package_type.to_s)
    package_name = self.registry_package_name || (self.package_version && self.package_version.package.name)
    platform = self.platform || (self.package_version && self.package_version.platform)
    version = self.version || (self.package_version && self.package_version.version)

    case package_type
    when "npm"
      "#{package_name}-#{version}-npm.tgz"
    when "rubygems"
      if platform.blank? || platform == "ruby"
        "#{package_name}-#{version}.gem"
      else
        "#{package_name}-#{version}-#{platform}.gem"
      end
    else
      filename
    end
  end

  def name=(val)
    @name = val
  end

  def set_guid
    self.created_at = Time.now
    self.guid ||= SimpleUUID::UUID.new(created_at).to_guid
  end

  def check_maven_filename
    package_type = self.registry_package_type || (self.registry_package && self.registry_package.package_type.to_s)
    return true unless (package_type == "maven" || package_type == "python")

    # alambic sends the name of the file as `name` attribute.
    # for all our other package types, we autogenerate the filename, but for
    # maven, we have to save the filename that the client sent
    self.filename = @name if self.filename.blank?
    errors.add(:registry_package_version, "has an unspecified maven filename") if self.filename.blank?
  end

  def repository_id
    if package_version_id && package_version.nil?
      Failbot.report("Missing package_version for file #{self.guid} where package_version_id:#{package_version_id} is present.")
    end

    (package_version && package_version.package&.repository_id)
  end

  def repository
    @repository || Repository.find_by_id(repository_id)
  end

  def uploadable_surrogate_key
    "repository-#{repository_id} #{super}"
  end

  def storage_policy(actor: nil, repository: nil)
    ::Storage::S3Policy.new(self, actor: actor)
  end

  def ensure_filename
    self.filename ||= self.name || self.guid
  end

  def content_type
    "application/octet-stream".freeze
  end

  # cluster settings

  def storage_external_url(actor = nil)
    url = storage_policy(actor: actor).download_url
    if GitHub.storage_cluster_enabled? && GitHub.storage_private_mode_url
      url = url.sub(GitHub.storage_cluster_url, GitHub.storage_private_mode_url)
    end

    get_cdn_url(url)
  end

  def get_cdn_url(url)
    # Fastly CDN returns 500s for large files from S3
    # See: https://github.com/github/c2c-package-registry/issues/1366 and
    # https://github.com/github/c2c-package-registry/issues/1355
    skip_cdn = GitHub.flipper[:package_registry_skip_cdn_large_files].enabled? && size >= LARGE_FILE_SIZE_THRESHOLD
    return url if !GitHub.package_registry_cdn_enabled? || skip_cdn

    raise "Package CDN URL is not set" if  (GitHub.packages_cdn_url.nil? || GitHub.packages_cdn_url.empty?)
    parsed = URI.parse(url)
    GitHub.packages_cdn_url.chomp("/") + parsed.path + "?" + parsed.query
  end

  def url(actor: nil)
    storage_policy(actor: actor).download_url
  end

  def metadata_url(actor: nil)
    storage_policy(actor: actor).metadata_url
  end

  def storage_cluster_url(policy)
    "#{GitHub.storage_cluster_url}/repository/#{self.repository_id}/registry/#{id}"
  end

  def storage_cluster_download_token(policy)
    policy.storage_cluster_download_token(policy, nil) unless !GitHub.private_mode_enabled? && package_version.package.repository.public?
  end

  def storage_download_path_info(policy)
    "/internal/storage/repository/#{self.repository_id}/registry/#{id}"
  end

  def storage_download_content_type
    "application/octet-stream"
  end

  # s3 storage settings
  def storage_s3_access_key
    if GitHub.enterprise? || Rails.production?
      GitHub.s3_packages_access_key || GitHub.s3_production_data_access_key
    else
      GitHub.s3_environment_config[:access_key_id]
    end
  end

  def storage_s3_secret_key
    if GitHub.enterprise? || Rails.production?
      GitHub.s3_packages_secret_key || GitHub.s3_production_data_secret_key
    else
      GitHub.s3_environment_config[:secret_access_key]
    end
  end

  def storage_s3_key(policy)
    "#{repository_id}/#{guid}"
  end

  def storage_s3_download_query(query)
    query["response-content-disposition"] = "filename=#{name}"
    query["response-content-type"] = "application/octet-stream"
  end

  def self.storage_s3_bucket
    GitHub.s3_packages_bucket || "github-#{Rails.env.downcase}-registry-package-file-4f11e5"
  end

  def storage_s3_bucket
    self.class.storage_s3_bucket
  end

  def storage_provider
    prov = read_attribute(:storage_provider)
    prov.blank? ? :default : prov.to_sym
  end

  def storage_s3_access
    :private
  end

  def storage_download_expiration
    5.minutes
  end
end
