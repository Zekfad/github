# frozen_string_literal: true

class Registry::Package < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification
  include GitHub::UTF8
  include Registry::PackageDownloadStatsService
  include Configurable::PackageAvailability

  self.table_name = :registry_packages
  class << self
    # Public: Get displayable type names for all publicly supported types.
    #
    # Returns Hash { symbol => String }
    def pretty_type_names
      types = self.enabled_types
      PRETTY_TYPE_NAMES.slice(*types)
    end

    # Public: Get package type from language.
    #
    # Returns symbol or nil.
    def package_type_for_language(language)
      LANGUAGE_TO_PACKAGE_TYPE[language]
    end

    def with_owner_and_name_and_type(owner_id, package_name, package_type)
      Registry::Package.where(owner_id: owner_id, name: package_name, package_type: package_type).first
    end

    def with_name_and_type(package_name, package_type, registry_package_type)
      Registry::Package.where(name: package_name)
        .where("package_type=? OR registry_package_type=?",
              package_types[package_type], registry_package_type).first
    end

    # Deprecated. Can be replaced with just passing the package_type directly
    # but is used in many locations so we're keeping the method around for now.
    def symbolize_package_type(package_type)
      package_type.to_sym if package_types[package_type].present?
    end

    def package_data_for_type(package_type)
      if package_type.is_a? String
        package_type = self.translate_pretty_name_to_package_type(package_type)
      end

      # Owner Ids for all packages published
      total_pkgs_published = Registry::Package.where(package_type: package_type).pluck("owner_id")
      # Distinct owner ids, sorted by descending total package count
      distinct_owner_ids = total_pkgs_published.group_by(&:itself).transform_values(&:count).sort_by { |k, v| v }.reverse.map(&:first)
      distinct_owners_to_enumerate = User.find(distinct_owner_ids.slice(0, Registry::Package::ENUMERATE_COUNT)) # maintains order of ids passed in

      package_type_name = PACKAGE_TYPE_SYM[package_type].to_s
      package_type_pretty_name = self.translate_package_id(package_type)

      Registry::PackageData.new(
        package_type,
        total_pkgs_published.count,
        distinct_owner_ids.length,
        distinct_owners_to_enumerate,
        package_type_name,
        package_type_pretty_name
      )
    end

    def page_logins_by_package_type(package_type, page)
      # Owner Ids for all packages published
      total_pkgs_published = Registry::Package.where(package_type: package_type).pluck("owner_id")
      distinct_owner_ids = total_pkgs_published.group_by(&:itself).transform_values(&:count).sort_by { |k, v| v }.reverse.map(&:first)

      User.find(distinct_owner_ids).paginate(page: page)
    end

    # Gets total download counts for all versions of a package for a set of
    # package ids. Uses a batched query.
    #
    # Returns: Hash { int => int }
    def total_download_counts_for_ids(package_ids)
      batched_downloads_in_range(package_ids)
    end

    # Gets last 30 day download counts for all versions of a package for a set of
    # package ids. Uses a batched query.
    #
    # Returns: Hash { int => int }
    def thirty_day_download_counts_for_ids(package_ids)
      batched_downloads_in_range(package_ids, 30.days.ago.beginning_of_day, Time.now.end_of_day)
    end

    # Public: Given a database id, returns the global_relay_id
    def to_global_relay_id(pkg_id)
      Platform::Helpers::NodeIdentification.to_global_id("Package", pkg_id)
    end

    def batched_downloads_in_range(package_ids, start_time = nil, end_time = nil)
      if start_time && end_time
        # If we put the time condition on the where clause, it'll filter out the
        # empty rows from the left join. So we need to put the time condition on
        # the JOINS ON clause.
        join_condition = "LEFT OUTER JOIN `package_download_activities` ON `package_download_activities`.`package_id` = `registry_packages`.`id` AND (`package_download_activities`.`started_at` BETWEEN ? AND ?)"
        query = self.joins(sanitize_sql_array([join_condition, start_time, end_time]))
      else
        query = self.left_joins(:downloads)
      end

      query = query.where(id: package_ids)
        .select("registry_packages.id, package_download_activities.package_version_id, SUM(package_download_activities.package_download_count) as downloads_count")
        .group("package_download_activities.package_version_id")

      raw_sums_by_version_id = query.each_with_object({}) { |p, h| h[p.package_version_id] = p.downloads_count.to_i if p.package_version_id }
      file_counts_by_version_id = Registry::PackageVersion
        .not_deleted
        .where(id: raw_sums_by_version_id.keys)
        .pluck(:id, :files_count)
        .to_h

      query.each_with_object(Hash.new { |h, k| h[k] = [] }) do |result, h|
        # Return download count of 0 if no package version was found or if
        # the package version has no files.
        file_count = file_counts_by_version_id[result.package_version_id]
        if file_count.nil? || file_count == 0
          h[result.id] = [0]
          next
        end

        version_download_count = raw_sums_by_version_id[result.package_version_id] || 0
        version_downloads = (version_download_count/file_count.to_f).ceil
        h[result.id] << version_downloads
      end.map { |id, counts| [id, counts.reduce(0, :+)] }.to_h
    end
  end

  belongs_to :repository, required: true
  belongs_to :owner, class_name: "User", required: true

  validate :check_registry_package_name

  has_many :package_versions,
    class_name: "Registry::PackageVersion",
    foreign_key: :registry_package_id,
    dependent: :destroy
  has_many :package_files, through: :package_versions, source: :files
  has_many :tags,
    class_name: "Registry::Tag",
    foreign_key: :registry_package_id,
    dependent: :destroy
  has_many :downloads,
    class_name: "Registry::PackageDownloadActivity",
    dependent: :delete_all
  has_many :data_transfer_line_items,
    class_name: "PackageResitry::DataTransferLineItem",
    foreign_key: :registry_package_id

  # The version tagged `latest` or the most recent version if none tagged.
  has_one :latest_version, -> { merge(Registry::PackageVersion.not_deleted.latest) },
    class_name: "Registry::PackageVersion",
    foreign_key: :registry_package_id

  def name_with_owner(separator = "/")
    "#{async_owner.sync}#{separator}#{self.name}"
  end

  before_validation :sync_owner_id
  before_validation :normalize_name_characters

  validates_presence_of :name
  validates_presence_of :package_type
  validates_associated :package_versions

  after_commit :invalidate_cached_repository_packages_count, on: :create
  after_commit :synchronize_search_index

  scope :private_scope, -> { joins(:repository).where("repositories.public = ?", false) }
  scope :public_scope, -> { joins(:repository).where("repositories.public = ?", true) }
  scope :with_active_versions, -> { joins(:package_versions).merge(Registry::PackageVersion.not_deleted).distinct }

  enum package_type: { npm: 0, rubygems: 1, maven: 2, docker: 3, debian: 4, nuget: 5, python: 6 }
  PACKAGE_TYPE_SYM = { 0 => :npm, 1 => :rubygems, 2 => :maven, 3 => :docker, 4 => :debian, 5 => :nuget, 6 => :python }
  PUBLICLY_SUPPORTED_TYPES = %i(npm rubygems maven docker nuget)
  PRETTY_TYPE_NAMES = {
    npm: "npm",
    rubygems: "RubyGems",
    maven: "Maven",
    docker: "Docker",
    debian: "Debian",
    nuget: "NuGet",
    python: "Python",
  }

  LANGUAGE_TO_PACKAGE_TYPE = {
    javascript: :npm,
    ruby: :rubygems,
    java: :maven,
    docker: :docker,
    "c#": :nuget,
    python: :python,
  }

  # Public: Translates an integer id to its Pretty Type Name
  def self.translate_package_id(id)
    PRETTY_TYPE_NAMES[PACKAGE_TYPE_SYM[id]]
  end

  # Internal: An array of strings of all supported Debian architectures
  DEBIAN_ARCHITECTURES = %w(i386 amd64 armel armhf arm64 mips mips64el mipsel ppc64el s390x)

  def self.translate_pretty_name_to_package_type(pretty_name)
    package_types[pretty_name.downcase.to_sym]
  end

  ENUMERATE_COUNT = 2

  def self.supported_package_type_values
    package_types.slice(*PUBLICLY_SUPPORTED_TYPES).values
  end

  def self.enabled_types
    case Configurable::PackageAvailability.package_availability

    when Configurable::PackageAvailability::ENABLED
      return supported_package_type_values.map { |package_type| PACKAGE_TYPE_SYM[package_type] }
    when Configurable::PackageAvailability::MANAGED
      key = "repository:packages:enabled_types"
      enabled_types = GitHub.cache.fetch(key, ttl: 10.seconds) do
        enabled_package_types
      end

      return enabled_types.map { |package_type| PACKAGE_TYPE_SYM[package_type] }
    else
      return []
    end
  end

  # Public: Whether the given user can see this registry package.
  def readable_by?(actor)
    repository && repository.readable_by?(actor)
  end

  # Public: Returns the associated package version matching `version`.
  #
  # Returns: Registry::PackageVersion
  def version_by_version_string(version, include_deleted: false)
    versions = package_versions.where(version: version)
    versions = versions.not_deleted unless include_deleted
    versions.first
  end

  # Public: Returns the associated package version matching `shasum`.
  #
  # Returns: Registry::PackageVersion
  def version_by_sha256(shasum, include_deleted: false)
    versions = package_versions.where(sha256: shasum)
    versions = versions.not_deleted unless include_deleted
    versions.first
  end

  # Public: Returns the associated package version matching `version`, `platform`.
  #
  # Returns: Registry::PackageVersion
  def version_by_platform(version, platform, include_deleted: false)
    versions = package_versions.where(version: version, platform: platform)
    versions = versions.not_deleted unless include_deleted
    versions.first
  end

  def prerelease_versions
    package_versions.joins(:release).where(releases: { prerelease: true })
  end

  def shell_safe_name
    Shellwords.escape(utf8(name))
  end

  def color
    case package_type.to_sym
    when :rubygems
      ruby = Linguist::Language.find_by_name("Ruby")
      ruby.color
    when :npm
      js = Linguist::Language.find_by_name("Javascript")
      js.color
    when :maven
      java = Linguist::Language.find_by_name("Java")
      java.color
    when :docker
      "#046fb3" # Docker whale - https://www.docker.com/brand-guidelines
    when :debian
      "#d61053" # Debian logo - https://www.debian.org/logos/
    when :nuget
      "#2B9CDF" # Nuget logo - https://commons.wikimedia.org/wiki/File:NuGet_project_logo.svg
    when :python
      python = Linguist::Language.find_by_name("Python")
      python.color
    else
      "#ccc"
    end
  end

  def sync_owner_id
    self.owner_id = repository.owner_id
  end

  def transfer(new_owner:, old_owner:, actor:, transferred_at: Time.now.utc)
    update(owner_id: new_owner.id)

    data = {
      actor: actor,
      package: self,
      size: package_versions.not_deleted.joins(:package_files).sum("package_files.size"),
      transferred_at: transferred_at,
      storage_service: "AWS_S3",
      user_agent: GitHub.context[:user_agent].to_s,
      repository: repository,
      previous_owner_id: old_owner.id,
      previous_owner_global_id: old_owner.global_relay_id,
    }

    case old_owner
    when ::Organization
      data[:previous_owner_org] = old_owner
    else
      data[:previous_owner_user] = old_owner
    end

    GlobalInstrumenter.instrument("packages.package_transferred", data)
  end

  def normalize_name_characters
    return unless name_changed?

    normalized_name = utf8(self.name.to_s)
    normalized_name.gsub!(/[^[[:word:]][[:blank:]][[:punct:]]]/, "")
    normalized_name.squish!
    self.name = normalized_name
  end

  def synchronize_search_index
    return self unless GitHub.elasticsearch_access_allowed?

    if destroyed? || package_versions.not_deleted.count.zero? || repository.nil?
      RemoveFromSearchIndexJob.perform_later("registry_package", self.id)
    else
      Search.add_to_search_index("registry_package", self.id)
    end
  end

  def invalidate_cached_repository_packages_count
    GitHub.cache.delete("repository:packages:count:#{repository.id}") if repository.present?
  end

  # Public: Return a Hash of information for the source registry of this package.
  #
  # For the moment, this will simply return a Hash based off of the registry_package_type, however when we have public
  # users of the Package Platform, this will return metadata for their registry.
  #
  # Returns a Hash containing information about the source registry.
  def source_registry
    if self.class.package_types.keys.include?(registry_package_type.to_s)
      registry_path = case registry_package_type
        when "docker" then repository.name_with_owner
        when "npm" then "@#{owner.login}"
        else owner.login
      end

      {
        about_url: "#{GitHub.help_url}/about-github-package-registry",
        name: "GitHub #{registry_package_type} registry",
        type: registry_package_type,
        url: "#{GitHub.urls.registry_url(registry_package_type)}/#{registry_path}",
        vendor: "GitHub Inc"
      }
    end
  end

  def check_registry_package_name
    rp = Registry::Package.with_owner_and_name_and_type(self.repository.owner_id, self.name, self.package_type)

    # registry package not created yet
    return true if rp.nil?

    # registry package created and associated with another repo
    return errors.add(:registry_package, "is already associated with another repository") unless rp.repository.id == self.repository_id

    true
  end

  def platform_type_name
    "Package"
  end

  def can_be_deleted?
    repository.private?
  end

  def active?
    latest_version.present?
  end

  def downloads_in_range(start_time, end_time)
    self.class.batched_downloads_in_range(id, start_time, end_time)[id]
  end

  def visibility
    repository.public_or_private_visibility
  end
end
