# rubocop:disable Style/FrozenStringLiteralComment

class RateLimitKey
  def self.for(obj)
    case obj
    when Bot
      case obj.installation
      when ScopedIntegrationInstallation
        if obj.installation.per_repo_rate_limit?
          "installation-#{obj.installation.integration_installation_id}-#{obj.installation.repository_ids.first}"
        else
          "installation-#{obj.installation.integration_installation_id}"
        end
      else
        "installation-#{obj.installation.id}"
      end
    when User
      "user-#{obj.id}"
    when OauthApplication
      "app-#{obj.key}"
    else
      obj.to_s
    end
  end
end
