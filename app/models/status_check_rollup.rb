# frozen_string_literal: true

class StatusCheckRollup
  REQUIRED_DUCK_TYPE_METHODS = [
    :state,
    :state_changed_at,
    :sort_order,
    :duration_in_seconds,
  ]

  EXPECTED        = "expected".freeze
  REQUESTED       = "requested".freeze
  QUEUED          = "queued".freeze
  PENDING         = "pending".freeze
  IN_PROGRESS     = "in_progress".freeze
  SUCCESS         = "success".freeze
  NEUTRAL         = "neutral".freeze
  SKIPPED         = "skipped".freeze
  FAILURE         = "failure".freeze
  ERROR           = "error".freeze
  CANCELLED       = "cancelled".freeze
  ACTION_REQUIRED = "action_required".freeze
  TIMED_OUT       = "timed_out".freeze
  STALE           = "stale".freeze

  PENDING_STATES = [
    EXPECTED,
    QUEUED,
    PENDING,
    IN_PROGRESS,
  ]

  FAILURE_STATES = [
    FAILURE,
    ERROR,
    ACTION_REQUIRED,
  ]

  INCOMPLETE_STATES = [
    CANCELLED,
    TIMED_OUT,
    STALE,
  ]

  SUCCESS_STATES = [
    SUCCESS,
    NEUTRAL,
    SKIPPED,
  ]

  FAILURE_AND_INCOMPLETE_STATES = FAILURE_STATES + INCOMPLETE_STATES

  STATE_SORT_ORDER = Hash[
    [
      ACTION_REQUIRED,
      TIMED_OUT,
      ERROR,
      FAILURE,
      CANCELLED,
      STALE,
      EXPECTED,
      REQUESTED,
      QUEUED,
      IN_PROGRESS,
      PENDING,
      NEUTRAL,
      SKIPPED,
      SUCCESS,
    ].each_with_index.to_a
  ]
  STATE_SORT_ORDER.default = 0

  # Adjective form of states used to describe statuses and checks in message copy.
  #
  #   2 pending statuses
  #   5 successful checks
  ADJECTIVE_STATES = {
    ERROR   => "errored",
    FAILURE => "failing",
    SUCCESS => "successful",
    STALE   => "marked stale by GitHub",
  }.freeze

  VERB_STATES = {
    REQUESTED       => "requested",
    QUEUED          => "queued",
    PENDING         => "pending",
    IN_PROGRESS     => "in progress",
    SUCCESS         => "succeeded",
    NEUTRAL         => "completed",
    SKIPPED         => "skipped",
    FAILURE         => "failed",
    ERROR           => "errored",
    CANCELLED       => "cancelled",
    ACTION_REQUIRED => "required action",
    TIMED_OUT       => "timed out",
    STALE           => "marked stale by GitHub",
  }

  def self.adjective_state(state)
    (ADJECTIVE_STATES[state.to_s.downcase] || state.to_s.downcase).gsub("_", " ")
  end

  def self.verb_state(state)
    (VERB_STATES[state.to_s.downcase] || state.to_s.downcase).humanize(capitalize: false)
  end

  attr_reader :status_checks
  def initialize(status_checks:)
    @status_checks = status_checks
  end

  def state
    return PENDING if status_checks.empty?
    return FAILURE if status_checks.any? { |status_check| (FAILURE_STATES + INCOMPLETE_STATES).include?(status_check.state) }
    return SUCCESS if status_checks.all? { |status_check| SUCCESS_STATES.include?(status_check.state) }

    PENDING
  end

  def updated_at
    # TODO: do we have a better name than state_changed_at?
    status_checks.map(&:state_changed_at).max
  end
end
