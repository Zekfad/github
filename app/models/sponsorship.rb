# frozen_string_literal: true

class Sponsorship < ApplicationRecord::Collab
  self.table_name = "sponsorships"
  self.ignored_columns = %w(maintainer_type maintainer_id)

  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model

  after_commit :instrument_creation, on: :create
  after_save :check_active_goal_completion

  # We will add Repository and Organization in future.
  enum sponsor_type: {
    User: 0,
  }, _prefix: :sponsor

  # We will add Repository and Organization in future.
  enum sponsorable_type: {
    User: 0,
  }, _prefix: :sponsorable

  # Using the same mapping for SponsorsTier as Billing::SubscriptionItem for consistency
  enum subscribable_type: {
    SponsorsTier.name => 1,
  }, _prefix: :subscribable

  enum privacy_level: {
    public: 0,
    private: 1,
  }, _prefix: :privacy

  scope :active, -> { where(active: true) }

  # These ranking scopes are very similar, and are implemented separately
  # to reduce confusion.
  scope :ranked_by_sponsor, ->(for_user:) {
    return scoped unless for_user

    target_attr = :sponsor_id

    public_users = User.where(id: privacy_public.distinct.pluck(target_attr))
    private_user_ids = privacy_private.distinct.pluck(target_attr)

    return scoped if public_users.empty? && private_user_ids.empty?

    ranked_ids =
      User.ranked_for(for_user, scope: public_users).pluck(:id) + private_user_ids

    scoped.order(Arel.sql("FIELD(sponsorships.#{target_attr}, #{ranked_ids.join(", ")})")).
      order(target_attr)
  }

  scope :ranked_by_sponsorable, ->(for_user:) {
    return scoped unless for_user

    target_attr = :sponsorable_id

    public_users = User.where(id: privacy_public.distinct.pluck(target_attr))
    private_user_ids = privacy_private.distinct.pluck(target_attr)

    return scoped if public_users.empty? && private_user_ids.empty?

    ranked_ids =
      User.ranked_for(for_user, scope: public_users).pluck(:id) + private_user_ids

    scoped.order(Arel.sql("FIELD(sponsorships.#{target_attr}, #{ranked_ids.join(", ")})")).
      order(target_attr)
  }

  belongs_to :sponsor, polymorphic: true
  belongs_to :sponsorable, polymorphic: true
  belongs_to :subscribable, polymorphic: true
  belongs_to :subscription_item, class_name: "Billing::SubscriptionItem", required: true

  validates_presence_of :sponsor_type, :sponsor_id
  validates_presence_of :privacy_level

  validate :ensure_sponsorable_or_sponsor_are_not_blocked, on: :create

  alias_method :tier, :subscribable

  def ensure_sponsorable_or_sponsor_are_not_blocked
    return if sponsorable.blank? || sponsor.blank?

    errors.add :sponsor, "is blocked" if sponsor.blocked_by?(sponsorable)
    errors.add :sponsorable, "is blocked" if sponsorable.blocked_by?(sponsor)
  end

  def event_payload
    {
      maintainer: sponsorable,
      sponsor: sponsor,
      sponsorship: self,
    }
  end

  def instrument_creation
    instrument(:create) if instrument_for_stratocaster?
  end

  # Public: Returns true
  #
  # Used to check whether a user-server GitHub App should receive an
  # "unauthorized" (403) or a "not found" (404) error when accessing a
  # sponsorship through the API.
  def readable_by?(actor)
    true
  end

  def self.all_active_as_sponsorable(sponsorable:, tier_scope: nil)
    sponsorships = Sponsorship
      .preload(:subscription_item)
      .where(sponsorable_id: sponsorable.id)
    active_subscription_items = Billing::SubscriptionItem
      .active
      .where(id: sponsorships.pluck(:subscription_item_id))

    if tier_scope
      active_subscription_items = active_subscription_items.where(subscribable: tier_scope)
    end

    active_subscription_item_ids = active_subscription_items.pluck(:id)
    sponsorships
      .where(subscription_item_id: active_subscription_item_ids)
      .order("created_at DESC")
  end

  def first_for?(sponsorable:)
    return self == Sponsorship.first_for(sponsorable: sponsorable)
  end

  def self.first_for(sponsorable:)
    Sponsorship.where(sponsorable: sponsorable)
      .order("created_at ASC")
      .first
  end

  def pending_tier_change
    async_pending_tier_change.sync
  end

  def async_pending_tier_change
    Promise.all([async_sponsorable, async_sponsor]).then do |sponsorable, sponsor|
      sponsorable.async_sponsors_listing.then do |sponsors_listing|
        next false unless sponsors_listing

        sponsors_tier_ids = SponsorsTier.where(sponsors_listing_id: sponsors_listing.id).pluck(:id)
        sponsorable_type = Billing::PendingSubscriptionItemChange.subscribable_types[SponsorsTier.name]

        sponsor.pending_subscription_item_changes
         .joins(:pending_plan_change)
         .where(
           subscribable_type: sponsorable_type,
           subscribable_id: sponsors_tier_ids)
         .order("pending_plan_changes.active_on ASC")
         .first
      end
    end
  end

  def has_pending_cancellation?
    async_has_pending_cancellation?.sync
  end

  def async_has_pending_cancellation?
    async_pending_tier_change.then do |item_change|
      next false unless item_change
      item_change.cancellation?
    end
  end

  def async_has_pending_downgrade?
    async_pending_tier_change.then do |item_change|
      next false unless item_change
      !item_change.cancellation?
    end
  end

  private

  def instrument_for_stratocaster?
    self.privacy_level == "public"
  end

  def check_active_goal_completion
    active_goal = sponsorable.sponsors_listing&.active_goal

    return if active_goal.blank?
    return unless active_goal.can_complete?

    CompleteSponsorsGoalJob.perform_later(active_goal)
  end
end
