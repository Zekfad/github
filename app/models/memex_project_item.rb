# frozen_string_literal: true

class MemexProjectItem < ApplicationRecord::Domain::Memexes
  include GitHub::Validations
  include GitHub::Prioritizable

  belongs_to :memex_project, inverse_of: :memex_project_items
  belongs_to :creator, class_name: "User"
  belongs_to :content, polymorphic: true
  belongs_to :repository
  has_one :draft_issue, autosave: true, dependent: :destroy
  has_many :memex_project_column_values, inverse_of: :memex_project_item

  prioritizable_by subject: :content, context: :memex_project

  VALID_CONTENT_TYPES = [
    ISSUE_TYPE = Issue.name,
    PULL_REQUEST_TYPE = PullRequest.name,
    DRAFT_ISSUE_TYPE = DraftIssue.name,
  ].freeze

  validates :creator, presence: true, on: :create
  validates :memex_project, presence: true, on: :create
  validates :content_type, presence: true, inclusion: { in: VALID_CONTENT_TYPES }
  validates :content_id, uniqueness: { scope: [:memex_project_id, :content_type], message: "already exists in this project" }
  validates :content, presence: true, on: :create
  validates :repository, presence: true, if: :repository_required?
  validate :creator_has_verified_email, on: :create
  validates(
    :priority,
    uniqueness: { scope: :memex_project_id, allow_nil: true },
    numericality: {
      less_than_or_equal_to: GitHub::Prioritizable::MAX_PRIORITY_VALUE,
      greater_than_or_equal_to: 0,
      allow_nil: true,
    }
  )

  delegate :memex_system_defined_column_value, to: :content
  alias_method :system_defined_column_value, :memex_system_defined_column_value

  # Temporary pagination limit until https://github.com/github/memex/issues/720 is implemented
  PER_PAGE_LIMIT = 500

  REDACTED_ITEM_TYPE = "RedactedItem"
  REDACTED_ITEM_TITLE = "You can't see this item"

  def redact!
    now = Time.zone.now
    self.class.new(
      id: id,
      priority: priority,
      content_id: -1,
      content_type: REDACTED_ITEM_TYPE,
      updated_at: now,
      created_at: now
    )
  end

  # Builds a hash that will ultimately be converted to JSON to represent this
  # object in the internal memex API.
  #
  # columns - Array<MemexProjectColumn> for the columns whose data we should serialize
  # require_prefilled_associations - Whether or not we should raise an exception if we're about to
  #   serialize an association that has not already been prefilled (meaning we're likely to generate
  #   an N+1).
  #
  # Returns a Hash.
  def to_hash(columns: [], require_prefilled_associations: true)
    result = {
      content_id: content_id,
      content_type: content_type,
      id: id,
      priority: priority,
    }

    if columns.present?
      result[:memex_project_column_values] = column_values(
        columns: columns,
        require_prefilled_associations: require_prefilled_associations
      )

      result[:content] = if content_type == REDACTED_ITEM_TYPE
        { id: content_id }
      else
        content.memex_content_hash
      end
    end

    result
  end

  def column_values(columns:, require_prefilled_associations:)
    columns.map do |column|
      value = if content_type == REDACTED_ITEM_TYPE && column.name == MemexProjectColumn::TITLE_COLUMN_NAME
        { title: REDACTED_ITEM_TITLE }
      elsif content_type == REDACTED_ITEM_TYPE
        nil
      elsif column.system_defined?
        system_defined_column_value(
          column,
          require_prefilled_associations: require_prefilled_associations
        )
      else # Implies column.user_defined? is true.
        user_defined_column_value(
          column,
          require_prefilled_associations: require_prefilled_associations
        )
      end

      {
        memex_project_column_id: column.synthetic_id,
        value: value
      }
    end
  end

  # Sets the value of a column.
  #
  # column - MemexProjectColumn whose value we should set
  # new_value - Object representing the updated value. The specific type of this object depends on
  #   the column. For some columns, passing a blank value here will delete the column value if
  #   it exists.
  # actor - User who requested that we set the column value.
  #
  # Returns Boolean for whether or not the update succeeded.
  def set_column_value(column, new_value, actor)
    case column.data_type.to_sym
    when :title
      set_title_column_value(new_value)
    when :text
      set_text_column_value(column, new_value, actor)
    when :assignees
      set_assignees_column_value(new_value)
    when :labels
      set_labels_column_value(new_value)
    when :milestone
      set_milestone_column_value(new_value)
    else
      false
    end
  end

  private

  # Used only in tests to report that a proposed operation is not efficient.
  class AssociationRefused < StandardError
    def initialize
      super(
        "Refusing to map over unloaded :memex_project_column_values association: " +
        "please prefill this association efficiently before calling this method."
      )
    end
  end

  # Retrieves the value of a user-defined column.
  #
  # column - MemexProjectColumn whose value we should retrieve.
  # require_prefilled_associations - Whether or not we should raise an exception if we're about to
  #   retrieve a value before loading the `memex_project_column_values` association (meaning we're
  #   likely to generate an N+1).
  #
  # Returns String representing the value of a user-defined column, or nil if no value exists for
  # that column.
  def user_defined_column_value(column, require_prefilled_associations: true)
    ensure_preloaded_column_values! if require_prefilled_associations
    value_object = memex_project_column_values.find { |v| v.memex_project_column_id == column.id }
    return nil unless value_object

    case column.data_type.to_sym
    when :text
      {
        raw: value_object.value,
        html: GitHub::HTML::MemexTextColumnPipeline.to_html(value_object.value)
      }
    end
  end

  def ensure_preloaded_column_values!
    if (Rails.env.test? && !self.association(:memex_project_column_values).loaded?)
      raise AssociationRefused
    end
  end

  # Sets the value of a MemexProjectColumn whose data_type is :title.
  #
  # Since all columns with that type are system-defined, this really justs sets
  # the title field of the underlying content object.
  #
  # new_value - Hash containing a :title key. The value of that key indicates
  #   the new title for this MemexProjectItem's content.
  #
  # Returns Boolean for whether or not the operation succeeded.
  def set_title_column_value(new_value)
    if new_value&.try(:fetch, :title).present?
      (content.is_a?(PullRequest) ? content.issue : content).update(title: new_value[:title])
    else
      false
    end
  end

  # Sets the value of a column whose data_type is :text.
  #
  # column - MemexProjectColumn whose data_type is :text
  # new_value - String representing the value that this MemexProjectItem should
  #   have in the given column.
  # actor - User who will be set as the initial creator of the custom column value.
  #
  # Returns Boolean for whether or not the operation succeeded.
  def set_text_column_value(column, new_value, actor)
    existing_value = (
      memex_project_column_values.find_by(memex_project_column_id: column.id) ||
      memex_project_column_values.build(memex_project_column_id: column.id, creator: actor)
    )

    if new_value.present?
      existing_value.update(value: new_value)
    else
      existing_value.destroy
      existing_value.destroyed?
    end
  end

  # Sets the value of a column whose data_type is :assignees
  #
  # assignee_ids - Array of user_ids to set as assignees
  #
  # Returns Boolean for whether or not the operation succeeded.
  def set_assignees_column_value(assignee_ids)
    return false unless issue_for_content

    assignees = User.where(id: assignee_ids).to_a
    issue_for_content.assignees = assignees
    issue_for_content.save
  end

  # Sets the value of a column whose data_type is :labels
  #
  # labels_ids - Array of Label IDs to replace labels on the underlying content
  #
  # Returns Boolean for whether or not the operation succeeded.
  def set_labels_column_value(label_ids)
    return false unless issue_for_content

    labels = if label_ids.present?
      Label.where(id: label_ids, repository_id: issue_for_content.repository_id).to_a
    else
      []
    end

    issue_for_content.replace_labels(labels)
  end

  def set_milestone_column_value(milestone_id)
    return false unless issue_for_content

    if milestone_id.present?
      milestone = issue_for_content.repository.milestones.find_by(id: milestone_id)
      return false unless milestone

      issue_for_content.milestone = milestone
    else
      issue_for_content.milestone = nil
    end

    issue_for_content.save
  end

  def issue_for_content
    @issue_for_content ||= \
      case content_type
      when PULL_REQUEST_TYPE
        content.issue
      when ISSUE_TYPE
        content
      else
        nil
      end
  end

  def creator_has_verified_email
    return unless GitHub.email_verification_enabled?
    errors.add(:creator, "must have a verified email address") unless creator&.verified_emails?
  end

  def repository_required?
    [ISSUE_TYPE, PULL_REQUEST_TYPE].include?(content_type)
  end
end
