# frozen_string_literal: true

class FundingLinks
  include EscapeHelper

  DIRECTORY = ".github"
  FILENAME = "FUNDING.yml"
  PATH = "#{DIRECTORY}/#{FILENAME}"

  INVALID_FORMAT_ERROR = "Invalid format provided for '%s'."
  UNKNOWN_PLATFORM_ERROR = "Unknown funding platform '%s'."
  NON_SPONSORABLE_ERROR = "Some users provided are not enrolled in GitHub Sponsors."

  # The maximum number of users to show in the funding links modal
  MAX_GITHUB_SPONSORS_FOR_USERS = 4
  MAX_GITHUB_SPONSORS_FOR_ORGS = 1
  MAX_CUSTOM_URLS = 4

  def self.empty
    new({})
  end

  def self.for(repository: nil, blob: nil)
    if repository.present?
      self.new(FundingLinks::FileParser.load(repository, path: FILENAME, directory: DIRECTORY))
    elsif blob.present?
      self.new(FundingLinks::BlobParser.load(blob: blob))
    else
      self.empty
    end
  end

  def self.template
    FundingPlatforms::ALL.values.each_with_object({}) do |platform, structure|
      structure[platform.key.to_s] = platform.template_placeholder
      # YAML is too quick to add unnecessary quotes everywhere so strip these out.
    end.to_yaml
       .gsub(/: "(.*)"/, ": \\1")
       .sub(/---/, "# These are supported funding model platforms\n")
  end

  attr_reader :config

  def initialize(config)
    @config = config.is_a?(Hash) ? config.compact : Hash.new

    # Modify @config hash by limiting the number of "custom" entries
    # to MAX_CUSTOM_URLS
    if @config.has_key?("custom") && @config["custom"].is_a?(Array)
      @config["custom"] = @config["custom"].take(MAX_CUSTOM_URLS)
    end

    @errors = []
  end

  def sponsors_logins
    return [] if config.empty?
    @sponsors_logins ||= Array(config[FundingPlatform::GitHub.key.to_s])
  end

  # Public: Get the logins of sponsors configured that are sponsorable.
  #
  # Returns an Array[String].
  def valid_sponsors_logins
    return [] if config.empty?
    @valid_sponsors_logins ||= sponsorable_users.map(&:login) + sponsorable_orgs.map(&:login)
  end

  def sponsors_ids
    return @sponsors_ids if defined?(@sponsors_ids)
    return [] if sponsors_logins.empty?

    @sponsors_ids = sponsorable_users.map(&:global_relay_id) + sponsorable_orgs.map(&:global_relay_id)
    if @sponsors_ids.size < sponsors_logins.size
      @errors << NON_SPONSORABLE_ERROR
    end

    @sponsors_ids
  end

  def sponsorable_users_ids
    return @sponsorable_users_ids if defined?(@sponsorable_users_ids)
    return [] if sponsors_logins.empty?

    @sponsorable_users_ids = sponsorable_users.map(&:global_relay_id)
  end

  def sponsorable_orgs_ids
    return @sponsorable_orgs_ids if defined?(@sponsorable_orgs_ids)
    return [] if sponsors_logins.empty?

    @sponsorable_orgs_ids = sponsorable_orgs.map(&:global_relay_id)
  end

  # Public: The sponsorable users that can be displayed in the modal.
  #
  # Returns an Array[User].
  def sponsorable_users
    @sponsorable_users ||= sponsorables(type: :User, limit: MAX_GITHUB_SPONSORS_FOR_USERS)
  end

  # Public: The sponsorable orgs that can be displayed in the modal.
  #
  # Returns an Array[Organization].
  def sponsorable_orgs
    @sponsorable_orgs ||= sponsorables(type: :Organization, limit: MAX_GITHUB_SPONSORS_FOR_ORGS)
  end

  def external_funding_config
    @external_funding_config ||= config.except(FundingPlatform::GitHub.key.to_s)
  end

  def external_funding_accounts
    @external_funding_accounts ||= external_funding_config.select do |platform, account|
      unless FundingPlatforms.find(platform).present?
        @errors << UNKNOWN_PLATFORM_ERROR % platform
        next false
      end

      next false unless validate_accounts_for_platform(account, platform)

      true
    end
  end

  def validated_config
    return {} unless has_valid_platform?
    {
      github: valid_sponsors_logins,
    }.merge(external_funding_accounts)
  end

  # True if there is at least one valid funding platform
  def has_valid_platform?
    sponsors_ids.any? || external_funding_accounts.any?
  end

  def has_errors?
    errors.any?
  end

  def errors
    run_validations

    @errors
  end

  private

  # Return sponsorables based on type (`User` or `Organization`)
  def sponsorables(type:, limit: 1)
    downcased_sponsors_logins = Array(sponsors_logins).map(&:to_s).map(&:downcase)

    User.sponsorable_users_from_logins(downcased_sponsors_logins)
      .where(type: type).limit(limit)
      .sort_by { |org| downcased_sponsors_logins.index(org.login.downcase) }
  end

  # Ensures that funding platforms are validated
  def run_validations
    sponsors_ids
    external_funding_accounts
  end

  # Returns true if all accounts pass validation
  #
  # accounts - Either a single string, or an array of strings
  #            representing a funding platform account
  # platform - The name of the platform
  def validate_accounts_for_platform(accounts, platform)
    custom_key = FundingPlatform::Custom.key.to_s

    # All non-custom platforms must contains a single string value
    if platform != custom_key && !accounts.is_a?(String)
      @errors << INVALID_FORMAT_ERROR % platform
      return false
    end

    Array(accounts).all? do |acc|
      if !acc.is_a?(String)
        @errors << INVALID_FORMAT_ERROR % platform
        next false
      end

      next false unless acc.encoding == Encoding::UTF_8

      if platform == custom_key && safe_uri(acc).blank?
        @errors << INVALID_FORMAT_ERROR % platform
        next false
      end

      true
    end
  end
end
