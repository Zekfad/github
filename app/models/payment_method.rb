# rubocop:disable Style/FrozenStringLiteralComment

# Represents the method by which a user is paying for GitHub and includes a
# flattening of billing details like address information and payment
# instruments (credit card).
class PaymentMethod < ApplicationRecord::Domain::Users
  areas_of_responsibility :gitcoin

  include GitHub::UTF8
  include Instrumentation::Model
  include PaymentMethod::CreditCardDependency
  include PaymentMethod::PaypalDependency
  include PaymentMethod::InstrumentationDependency

  # Public: User/Organization that uses this card to pay for GitHub.
  belongs_to :user

  # Public: Customer that owns this payment method.
  belongs_to :customer

  # A payment should belong to a user or customer
  before_save :validate_presence_of_user_or_customer

  # Public: String customer identifier for the user's record in the payment
  # processors system.
  # column :payment_processor_customer_id
  validates_presence_of :payment_processor_customer_id

  # Public: The payment processor. See GitHub::Billing::PaymentProcessors
  # column :payment_processor_type
  def payment_processor
    @payment_processor ||= GitHub::Billing::PaymentProcessors.create(
        payment_processor_type,
        payment_processor_customer_id,
        gateway: ::Billing::Zuora::PaymentGateway.new(user),
    )
  end

  # Public: Boolean if this is the User's primary payment method.
  # column :primary

  PAYMENT_TOKEN_CLEARED = "payment-token-cleared"

  # Public: String token representing the default payment method and providing
  # the ability to make a sale against this payment instrument.
  # column :payment_token
  validates_presence_of :payment_token

  # Public: Boolean if the payment_token is valid.
  def valid_payment_token?
    payment_token.present? && payment_token != PAYMENT_TOKEN_CLEARED
  end

  # Public: Billing address country in ISO 3166-1 alpha-3 format.
  # column :country
  validates_length_of :country, is: 3, allow_blank: true

  # column :region
  # column :postal_code

  # Public: Integer number of expiration reminders sent. This only applies to
  # credit cards (default: 0).
  # column :expiration_reminders

  # Public: User supplied fields, not to be persisted
  attr_accessor :card_number, :cvv, :paypal_nonce, :encrypted_expiration_month, :encrypted_expiration_year

  # Public: PaymentMethods that are credit cards that are expiring next month.
  scope :expiring_credit_cards, lambda {
    one_month_from_now = GitHub::Billing.today + 1.month

    where("`primary` = 1
      AND truncated_number IS NOT NULL
      AND expiration_reminders = 0
      AND expiration_year = ?
      AND expiration_month = ?",
      one_month_from_now.year,
      one_month_from_now.month,
    )
  }

  def self.zuora_processor_slug
    GitHub::Billing::PaymentProcessors::ZuoraProcessor::SLUG
  end

  def self.braintree_processor_slug
    GitHub::Billing::PaymentProcessors::BraintreeProcessor::SLUG
  end

  # Public: Populates PaymentMethod from the User entry from the payment forms.
  #
  # details - A Hash of payment details:
  #   :charge        - Boolean whether to attempt a recurring charge with this update. Default: true
  #   :billing_extra - String extra billing information.
  #   :paypal_nonce  - String nonce representing a paypal account (optional).
  #   :credit_card   - A Hash of potentially encrypted CC info, including:
  #     * :number           - CC number as a String.
  #     * :expiration_month - Expiration month as a String of form MM
  #     * :expiration_year  - Expiration year as a String of form YY
  #     * :cvv              - CVV as a String (e.g. "420")
  #   :billing_address  - The billing address as a Hash of optionally encrypted CC info
  #     * :country_code_alpha3 - Country as a String
  #     * :region              - Region as a String
  #     * :postal_code         - Postal code as a String
  #
  # Returns a PaymentMethod
  def self.build_from_payment_details(details)
    card    = details[:credit_card] || {}
    address = details[:billing_address] || {}

    payment_method = PaymentMethod.new

    payment_method.country                    = address[:country_code_alpha3]
    payment_method.region                     = address[:region]
    payment_method.postal_code                = address[:postal_code]
    payment_method.card_number                = card[:number]
    payment_method.cvv                        = card[:cvv]
    payment_method.encrypted_expiration_month = card[:expiration_month]
    payment_method.encrypted_expiration_year  = card[:expiration_year]
    payment_method.paypal_nonce               = details[:paypal_nonce]

    payment_method
  end

  # Public: Update payment details.
  #
  # details - A Hash of payment details that includes:
  #           :credit_card     - A Hash of credit card info.
  #           :paypal_nonce    - A String nonce from PayPal.
  #           :billing_address - A Hash of billing address info (usually used with CC)
  #           :actor           - User taking this action.
  #           :zuora_payment_method_id - A Zuora HPM provided payment method ID
  #
  # Returns a Billing::Result object.
  def update_payment_details(details)
    raise ArgumentError if details.blank?

    card    = details[:credit_card] || {}
    address = details[:billing_address] || {}
    actor   = details[:actor] || user

    populate_address(address)

    payment_details = GitHub::Billing::PaymentProcessorPaymentDetails.new \
      token: payment_token,
      login: actor.login,
      has_card_on_file: credit_card?,
      paypal_nonce: details[:paypal_nonce],
      card_number: card[:number],
      expiration_month: card[:expiration_month],
      expiration_year: card[:expiration_year],
      cvv: card[:cvv],
      country_code_alpha3: address[:country_code_alpha3],
      region: address[:region],
      postal_code: address[:postal_code],
      zuora_payment_method_id: details[:zuora_payment_method_id],
      email: user.business ? user.business.name : user.login,
      account_name: user.business ? user.business.customer.billing_email.email : user.email

    lock do
      result = payment_processor.update_payment_details(payment_details) do |record|
        populate_from_record(record)
        save!
        instrument_update(actor)
      end
      instrument_update_failure(actor, result.error_message) unless result.success?
      result
    end
  end

  def blacklisted?
    BlacklistedPaymentMethod.find_by_payment_method self
  end

  # Public: Clears all payment details. It will no longer be possible to
  # process payment for this customer.
  #
  # Returns truthy if all payment details are successfully deleted.
  def clear_payment_details(actor)
    if deleted = payment_processor.clear_payment_details
      self.payment_token            = PAYMENT_TOKEN_CLEARED
      self.expiration_reminders     = 0
      self.truncated_number         = nil
      self.expiration_month         = nil
      self.expiration_year          = nil
      self.card_type                = nil
      self.paypal_email             = nil
      self.unique_number_identifier = nil

      self.save!

      instrument_clear(actor)
    end

    deleted
  end

  # Internal: Populate billing address from a hash.
  #
  # billing_address - A Hash of billing address info:
  #                   :region               - String region.
  #                   :postal_code          - String postal code.
  #                   :country_code_alpha3  - String country.
  #
  # Returns self.
  def populate_address(billing_address)
    return self unless billing_address.present?

    self.region      = billing_address[:region]
    self.postal_code = billing_address[:postal_code]
    self.country     = billing_address[:country_code_alpha3]

    self
  end

  # Internal: Populate self with a PaymentProcessorRecord.
  #
  # Returns self (a PaymentMethod).
  def populate_from_record(record)
    self.payment_processor_customer_id = record.customer_id

    self.payment_token = record.token
    self.expiration_reminders = 0

    # credit card
    self.truncated_number         = record.masked_number
    self.expiration_month         = record.expiration_month
    self.expiration_year          = record.expiration_year
    self.card_type                = record.card_type
    self.unique_number_identifier = record.unique_number_identifier

    # paypal
    self.paypal_email     = record.paypal_email

    self
  end

  # Returns a string summary of this payment method.
  def to_s
    return "invalid payment method" unless valid_payment_token?
    return paypal_email if paypal_email.present?
    "#{card_type} #{formatted_number}"
  end

  # Public: True if this payment method is expiring in the next three weeks.
  #
  # Returns boolean
  def expiring_in_less_than_three_weeks?
    GitHub::Billing.today >=
      Date.new(expiration_year, expiration_month) - 3.weeks
  end

  def on_braintree?
    valid_payment_token? && using_braintree_processor?
  end

  def on_zuora?
    valid_payment_token? && using_zuora_processor?
  end

  def using_braintree_processor?
    payment_processor_type == PaymentMethod.braintree_processor_slug
  end

  def using_zuora_processor?
    payment_processor_type == PaymentMethod.zuora_processor_slug
  end

  private

  def validate_presence_of_user_or_customer
    unless customer || user
      raise ActiveRecord::RecordInvalid.new(self)
    end
  end

  # Private: Semaphore lock for ensuring that actions don't happen concurrently.
  #
  # Takes a block to be called within the semaphore.
  #
  # Returns what the block returns or a failed
  # GitHub::Billing::Result if the lock cannot be obtained.
  def lock
    mutex = GitHub::Redis::Mutex.new(customer.update_payment_method_key)
    begin
      mutex.lock { yield }
    rescue GitHub::Redis::Mutex::LockError
        GitHub::Billing::Result.failure "Your payment details are updating. Please check your billing settings for results."
    end
  end
end
