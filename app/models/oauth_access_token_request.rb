# frozen_string_literal: true

class OauthAccessTokenRequest
  def self.areas_of_responsibility
    [:ecosystem_apps]
  end

  attr_reader :access, :application, :error_response, :log_data, :params

  BAD_VERIFICATION_CODE        = "#{GitHub.developer_help_url}/apps/managing-oauth-apps/troubleshooting-oauth-app-access-token-request-errors/#bad-verification-code"
  INCORRECT_CLIENT_CREDENTIALS = "#{GitHub.developer_help_url}/apps/managing-oauth-apps/troubleshooting-oauth-app-access-token-request-errors/#incorrect-client-credentials"
  REDIRECT_URI_MISMATCH2       = "#{GitHub.developer_help_url}/apps/managing-oauth-apps/troubleshooting-authorization-request-errors/#redirect-uri-mismatch2"
  REDIRECT_URLS                = "#{GitHub.developer_help_url}/apps/building-oauth-apps/authorization-options-for-oauth-apps/#redirect-urls"

  def self.process(application, log_data, params)
    new(application, log_data, params).process
  end

  def initialize(application, log_data, params)
    @application = application
    @log_data    = log_data
    @params      = params
  end

  def process
    case params[:grant_type]
    when DeviceAuthorizationGrant::GRANT_TYPE
      device_authorization_grant = DeviceAuthorizationGrant.find_by(
        hashed_device_code: DeviceAuthorizationGrant.hash_for(params[:device_code]),
        application: @application
      )

      error_response = case
      when device_authorization_grant_disabled?(@application)
        {
          error: :incorrect_client_credentials,
          error_description: "The client_id and/or client_secret passed are incorrect.",
          error_uri: INCORRECT_CLIENT_CREDENTIALS,
        }
      when device_authorization_grant.nil?
        {
          error: :incorrect_device_code,
          error_description: "The device_code provided is not valid.",
          error_uri: GitHub.developer_help_url,
        }
      when device_authorization_grant.access_denied?
        {
          error: :access_denied,
          error_description: "The authorization request was denied.",
          error_uri: GitHub.developer_help_url,
        }
      when device_authorization_grant.expired?
        {
          error: :expired_token,
          error_description: "This 'device_code' has expired.",
          error_uri: GitHub.developer_help_url,
        }
      when !device_authorization_grant.claimed?
        {
          error: :authorization_pending,
          error_description: "The authorization request is still pending.",
          error_uri: GitHub.developer_help_url,
        }
      end

      return log_error(error_response) if error_response
      access = device_authorization_grant.oauth_access
    else
      redirect_uri, error_response = get_redirect_uri(application, params[:redirect_uri])
      return log_error(error_response) if error_response

      access = application.access_for_code(params[:code])

      @refresh_token = RefreshToken.for_plaintext_token(params[:refresh_token])

      application_client_secret, error_response = plaintext_secret_for_application
      return log_error(error_response) if error_response

      error_response = if invalid_client_secret?(application_client_secret, params[:client_secret])
        {
          error: :incorrect_client_credentials,
          error_description: "The client_id and/or client_secret passed are incorrect.",
          error_uri: INCORRECT_CLIENT_CREDENTIALS,
        }
      elsif !OauthUtil.valid_redirect_uri?(application, redirect_uri)
        # TODO: Ensure that the "redirect_uri" parameter is present if the
        # "redirect_uri" parameter was included in the initial authorization
        # request as described in Section 4.1.1, and if included ensure that
        # their values are identical.
        #
        # Also, for us - validate against the list of registered callback URLs.
        #
        {
          error: :redirect_uri_mismatch,
          error_description: "The redirect_uri MUST match the registered callback URL for this application.",
          error_uri: REDIRECT_URI_MISMATCH2,
        }
      elsif access.nil? && !params.has_key?(:refresh_token)
        GitHub.dogstats.increment "oauth", tags: ["action:authorize", "error:bad_code"]
        log_data.merge!(
          verification_code: secret_last_eight(params[:code]),
          passed_callback: params[:redirect_uri],
          oauth_application: application.id,
        )

        {
          error: :bad_verification_code,
          error_description: "The code passed is incorrect or expired.",
          error_uri: BAD_VERIFICATION_CODE,
        }
      elsif params[:refresh_token] && !RefreshToken.valid_grant_type?(params[:grant_type])
        {
          error: :unsupported_grant_type,
          error_description: "You must specify grant_type of refresh_token when using a refresh_token.",
          # FIXME: link to appropriate docs
          error_uri: BAD_VERIFICATION_CODE,
        }
      elsif @refresh_token.nil? && params.has_key?(:refresh_token)
        GitHub.dogstats.increment "oauth", tags: ["action:access_token", "error:bad_refresh_token"]

        log_data.merge!(
          refresh_token: secret_last_eight(params[:refresh_token]),
          passed_callback: params[:redirect_uri],
          oauth_application: application.id,
        )

        {
          error: :bad_refresh_token,
          error_description: "The refresh token passed is incorrect or expired.",
          # FIXME: link to appropriate docs
          error_uri: BAD_VERIFICATION_CODE,
        }
      end
    end

    return log_error(error_response) if error_response

    if @refresh_token
      token, refresh_token = @refresh_token.redeem
      @refresh_token.reload
      access = @refresh_token.refreshable

      return {
        access_token: token,
        expires_in: access.expires_in,
        refresh_token: refresh_token,
        refresh_token_expires_in: @refresh_token&.expires_in,
        token_type: :bearer,
        scope: access.access_level.join(","),
      }
    end

    _, error_response = application.grant_repository_scoped_installation_on(access, repository_id: params[:repository_id])
    return log_error(error_response) if error_response

    token, refresh_token = access.redeem

    response_options = {}.tap do |opts|
      opts[:access_token] = token

      if refresh_tokens_enabled?
        opts[:expires_in]  = access.expires_in
        opts[:refresh_token] = refresh_token
        opts[:refresh_token_expires_in] = access.refresh_token&.expires_in
      end

      opts[:token_type] = :bearer
      opts[:scope] = access.access_level.join(",")

      if (installation = access.installation)
        if installation.repositories.any?
          opts[:repos_url] = Api::Serializer.url("/user/repos")
        end

        opts[:permissions] = installation.permissions
      end
    end

    if should_trigger_auto_install_on_oauth_code_exchange?
      AutomaticAppInstallation.trigger(
        type:       :oauth_code_exchanged,
        actor:      access.user,
        originator: application,
      )
    end

    response_options
  end

  private

  def device_authorization_grant_disabled?(application)
    !GitHub.flipper[:device_authorization_grant_opt_in].enabled?(application)
  end

  def get_redirect_uri(application, redirect_uri)
    # Only normalize applications with one callback URL to maintain legacy
    # behavior for existing applications.
    normalize = !application.strict_callback_url_validation?

    if redirect_uri.blank?
      [application.try(:callback_url), nil]
    elsif normalize
      [Addressable::URI.parse(redirect_uri).normalize.to_s, nil]
    else
      [redirect_uri, nil]
    end
  rescue Addressable::URI::InvalidURIError
    [nil, {
      error: :redirect_uri_invalid,
      error_description: "The redirect_uri MUST be a valid URL.",
      error_uri: REDIRECT_URLS,
    }]
  end

  def invalid_client_secret?(actual, requested)
    !SecurityUtils.secure_compare(actual.to_s, requested.to_s)
  end

  def plaintext_secret_for_application
    [application.plaintext_secret, nil]
  rescue ::Earthsmoke::Error
    [nil, {
      error: :request_timeout,
      error_description: "The request has timed out, please try again.",
    }]
  end

  def refresh_tokens_enabled?
    application.is_a?(Integration) && application.user_token_expiration_enabled?
  end

  def should_trigger_auto_install_on_oauth_code_exchange?
    return false unless application.is_a?(Integration)
    GitHub.flipper[:auto_install_apps_on_oauth_code_exchanged].enabled?(application)
  end

  def secret_last_eight(secret)
    secret = secret.to_s
    return secret if secret.length < 8

    secret.to_s[-8..-1]
  end

  def log_error(error_response)
    log_data[:error] = error_response[:error]
    error_response
  end
end
