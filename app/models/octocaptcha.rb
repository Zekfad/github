# frozen_string_literal: true
class Octocaptcha
  class UnableToLoadCaptcha < StandardError; end

  def areas_of_responsibility
    [:platform_health]
  end

  attr_reader :session, :octocaptcha_session

  delegate :verify, to: :octocaptcha_session
  delegate :solved?, to: :octocaptcha_session
  delegate :show_captcha?, to: :octocaptcha_session
  delegate :token, to: :octocaptcha_session
  delegate :value, to: :octocaptcha_session
  delegate :error, to: :octocaptcha_session
  delegate :funcaptcha_response_body, to: :octocaptcha_session

  def self.octocaptcha_service_configuration(feature_flag)
    ->(config) do
      config.env = Rails.env
      config.is_captcha_disabled = -> do
        return true if !GitHub.funcaptcha_enabled?
        FlipperFeature.find_by_name(feature_flag)&.fully_disabled?
      end
      config.is_captcha_service_disabled =  -> do
        return true if !GitHub.funcaptcha_enabled?
        !FlipperFeature.find_by_name(:octocaptcha)&.fully_enabled?
      end

      config.private_key =  GitHub.funcaptcha_private_key || "<fake-key>"
    end
  end

  OCTOCAPTCHA_PAGES = {
    signup: SpamuraiDevKit::Octocaptcha.new("github", "signup", &(octocaptcha_service_configuration(:octocaptcha_integration))),
    report_abuse: SpamuraiDevKit::Octocaptcha.new("github", "report_abuse", &(octocaptcha_service_configuration(:octocaptcha_report_abuse))),
  }

  def initialize(session, token = nil, page: :signup)
    o = OCTOCAPTCHA_PAGES[page]

    @octocaptcha_session = o.new_session(token: token)
    @session = session
  end

  # Puts the user into test groups for our experiment.
  # source_page        - the page the user started the signup flow from.
  def set_test_group(source_page = nil)
    return if !octocaptcha_dotcom_fully_enabled?

    # Sanitize string and remove : delimeter
    session[:source_page] = source_page.to_s.gsub(":", "") if source_page
    session[:source_page] = "unknown" if session[:source_page].blank?
  end

  def instrument_event(event_name, signup_time: nil, user: nil, email_address: nil)
    return if !octocaptcha_dotcom_fully_enabled?
    tags = []
    tags << "validation_value:#{value}" if value
    tags << "validation_error:#{error}" if error
    tags << "signup_time:#{signup_time}" if signup_time
    tags << "source_page:#{session[:source_page]}" if session[:source_page]
    GitHub.dogstats.increment "octocaptcha.#{event_name}", tags: tags

    payload = {
      occurred_at: Time.now,
      event_type: "octocaptcha_#{event_name}".upcase,
      session_id: session.id&.public_id,
      show_captcha: show_captcha?,
    }

    payload[:validation_value] = value.to_s if value
    payload[:validation_error] = error.to_s if error
    payload[:signup_time] = signup_time if signup_time
    payload[:source_page] = session[:source_page] if session[:source_page]
    payload[:user] = user if user
    payload[:email_address] = email_address if email_address
    payload[:funcaptcha_response] = response_body if value && !(response_body.class < Exception)

    GlobalInstrumenter.instrument("octocaptcha.signup", payload)
  end

  def octocaptcha_dotcom_fully_enabled?
    octocaptcha_session.show_captcha?
  end

  def response_body
    funcaptcha_response_body
  end

  def funcaptcha_session_id
    return "" if !octocaptcha_dotcom_fully_enabled?
    return "" unless response_body.is_a? Hash
    response_body.dig("other", "session") || ""
  end

  def funcaptcha_response
    return {} if !octocaptcha_dotcom_fully_enabled?
    return {} unless response_body.is_a? Hash

    response_body
  end

  def funcaptcha_solved
    return if !octocaptcha_dotcom_fully_enabled?
    value == :solved_captcha
  end

  def solved_interactive_captcha?
    return if !octocaptcha_dotcom_fully_enabled?
    return unless response_body.is_a? Hash
    response_body["solved"] && !(response_body.dig("other", "suppressed") == true)
  end
end
