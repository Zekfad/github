# frozen_string_literal: true

module IntegrationInstallable
  extend ActiveSupport::Concern

  included do
    after_destroy_commit :clear_permissions if respond_to?(:after_destroy)
  end

  # Public: Returns the Bot associated with this installation's
  # Integration, and sets this installation as the Bot's current
  # installation context. The installation context determines the Bot's
  # permissions (i.e., which actions it can take on which Repositories).
  #
  # Returns a Bot.
  def bot
    return if integration.nil?

    integration.bot.tap do |b|
      b.installation = self
    end
  end

  # Internal: Can this installation have granular permissions on resources?
  #
  # Returns true.
  def can_have_granular_permissions?
    true
  end

  # Internal: Can this installation have granular permissions on user resources?
  #
  # Returns false.
  def can_have_granular_user_permissions?
    false
  end

  # Internal: Can this installation ever act for an Integration?
  #
  # Returns true because it can act for an integration indirectly by using a
  # human user's OAuth token.
  def can_act_for_integration?
    true
  end

  def clear_abilities_per_destroyed_record?
    false
  end

  def clear_permissions
    Permissions::QueryRouter.delete_app_permissions_on_actor(self)
  end

  # Internal: Is the installation governed by oauth application policies?
  #
  # Returns false.
  def governed_by_oauth_application_policy?
    false
  end

  # Public: Has this installation been installed on all repositories?
  #
  # min_action: - An optional Symbol representing the minimum ability action
  #               (:read, :write, or :admin) that the user must have for the
  #               repositories. Default is nil, for no min_action
  #               restrictions.
  # resource:   - An optional String to represent a collection, from
  #               Repository::Resources.subject_types, to limit results to an installation with
  #               permissions on that resource.
  #
  # Examples:
  #
  #   installed_on_all_repositories?(resource: "issues")
  #   installed_on_all_repositories?(resource: "pull_requests")
  #   installed_on_all_repositories?(min_action: :write, resource: "contents")
  #
  # Returns Boolean.
  def installed_on_all_repositories?(min_action: :read, resource: nil)
    min_action_int = Ability.actions[min_action]

    subject_types = if resource.present?
      "#{Repository::Resources::ALL_ABILITY_TYPE_PREFIX}/#{resource.downcase}"
    else
      Repository::Resources.all_type_prefixed_subject_types
    end

    args = {
      actor_id:      ability_id,
      actor_type:    ability_type,
      min_action:    min_action_int,
      subject_types: Array(subject_types),
    }

    sql = Permission.github_sql.new(<<-SQL, args)
      SELECT 1 as one
      FROM permissions
      WHERE actor_id = :actor_id
        AND actor_type = :actor_type
        AND action >= :min_action
        AND subject_type IN :subject_types
      LIMIT 1
    SQL

    sql.values.any?
  end

  def installed_on_selected_repositories?
    !installed_on_all_repositories?
  end

  def launch_github_app?
    GitHub.launch_github_app&.id == integration_id
  end

  def launch_lab_github_app?
    GitHub.launch_lab_github_app&.id == integration_id
  end

  # Internal: which repositories does this installation have any permission on
  #
  # min_action: - An optional Symbol representing the minimum ability action
  #               (:read, :write, or :admin) that the user must have for the
  #               returned repositories. Default is nil, for no min_action
  #               restrictions.
  # resource:   - An optional String to represent a collection, from
  #               Repository::Resources.subject_types, to limit results to repositories with
  #               permissions on that resource.
  # repository_ids: - An optional list of candidate repository ids. Only
  #                   repositories that are in this list and are accessible by
  #                   the integration will be returned.
  # Examples:
  #
  #   repository_ids(resource: "issues")
  #   repository_ids(resource: "pull_requests")
  #   repository_ids(min_action: :write, resource: "contents")
  #
  # Returns an Array
  def repository_ids(min_action: nil, resource: nil, repository_ids: nil)
    return [] if resource && Repository::Resources.subject_types.none?(resource.to_s)

    min_action ||= :read

    if installed_on_all_repositories?(min_action: min_action, resource: resource)
      ids = async_target.then do |target|
        scope = target.repositories
        scope = scope.with_ids(repository_ids) if repository_ids
        scope.pluck(:id)
      end.sync
      ids - RepositoryAdvisory.with_ids(ids, field: :workspace_repository_id).pluck(:workspace_repository_id)
    else
      installed_on_individual_repository_ids(min_action: min_action, resource: resource, repository_ids: repository_ids)
    end
  end

  # Internal: Indicates if installation is across all repositories or just selected ones.
  #
  # Returns a String.
  def repository_selection
    installed_on_all_repositories? ? "all" : "selected"
  end

  # Public: which repositories does this installation have any permission on
  #
  # See #repository_ids for detailed docs.
  #
  # Returns a scoped relation for the associated repositories.
  def repositories(min_action: :read, resource: nil)
    repo_ids = repository_ids(min_action: min_action, resource: resource)

    if repo_ids.none?
      Repository.none
    else
      Repository.with_ids(repo_ids)
    end
  end

  # Public: Returns permissions currently assigned to the installation.
  #
  # Returns an Hash of resource Strings to permission Symbols
  def permissions
    permissions = Permission.github_sql.results(<<~SQL, actor_id: ability_id, actor_type: ability_type)
      SELECT DISTINCT subject_type, action
      FROM permissions
      WHERE actor_type = :actor_type
      AND actor_id = :actor_id
    SQL

    permissions.each_with_object({}) do |permission, memo|
      subject_type, action = permission

      resource = subject_type.split("/").last
      memo[resource] = Permission.actions.key(action).to_sym
    end
  end

  # Public: Is this actor a user?
  #
  # Returns false.
  def user?
    false
  end

  # Internal: Indicates the installation actor will never be using basic auth.
  #
  # Returns false.
  def using_basic_auth?
    false
  end

  # Internal: Indicates the installation actor will never be using personal access token.
  #
  # Returns false.
  def using_personal_access_token?
    false
  end

  def abilities(subject_types: [], subject_ids: [])
    params = {
      actor_id: ability_id,
      actor_type: ability_type,
      priority: Ability.priorities[:direct],
    }
    params[:subject_type] = subject_types if subject_types.any?
    params[:subject_id] = subject_ids if subject_ids.any?
    Permission.where(params)
  end

  private

  # Internal: List of repository ids that this installation has been individually installed on.
  #
  # min_action: - An optional Symbol representing the minimum ability action
  #               (:read, :write, or :admin) that the user must have for the
  #               repositories. Default is nil, for no min_action
  #               restrictions.
  # resource:   - An optional String to represent a collection, from
  #               Repository::Resources.subject_types, to limit results to an installation with
  #               permissions on that resource.
  # repository_ids: - An optional list of candidate repository ids. Only
  #                   repositories that are in this list and are accessible by
  #                   the integration will be returned.
  #
  # Examples:
  #
  #   installed_on_individual_repository_ids(resource: "issues")
  #   installed_on_individual_repository_ids(resource: "pull_requests")
  #   installed_on_individual_repository_ids(min_action: 1, resource: "contents")
  #
  # Returns an Array of repository ids
  def installed_on_individual_repository_ids(min_action: :read, resource: nil, repository_ids: nil)

    subject_types = if resource.present?
      "#{Repository::Resources::INDIVIDUAL_ABILITY_TYPE_PREFIX}/#{resource.downcase}"
    else
      Repository::Resources.subject_types.map { |st| "#{Repository::Resources::INDIVIDUAL_ABILITY_TYPE_PREFIX}/#{st}" }
    end

    options = {
      actor_id:      ability_id,
      actor_type:    ability_type,
      min_action:    Ability.actions[min_action],
      subject_types: Array(subject_types),
    }

    repository_ids = Array(repository_ids)

    if repository_ids.any?
      options[:subject_ids] = repository_ids
    end

    sql = Permission.github_sql.new(<<-SQL, options)
      SELECT DISTINCT subject_id
      FROM permissions
      WHERE actor_id = :actor_id
        AND actor_type = :actor_type
        AND action >= :min_action
        AND subject_type IN :subject_types
    SQL

    if repository_ids.any?
      sql.add <<-SQL
        AND subject_id IN :subject_ids
      SQL
    end

    sql.values
  end
end
