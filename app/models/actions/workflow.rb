# frozen_string_literal: true

class Actions::Workflow < ApplicationRecord::Ballast
  class CannotBeEnabledError < StandardError; end
  class NotActiveError < StandardError; end

  self.table_name = "workflows"

  extend GitHub::BackgroundDependentDeletes
  extend GitHub::Encoding
  force_utf8_encoding :name, :path

  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model

  belongs_to :repository

  has_many :workflow_runs, inverse_of: :workflow
  destroy_dependents_in_background :workflow_runs

  enum state: { active: 0, deleted: 1, disabled_fork: 2, disabled_inactivity: 3, disabled_manually: 4 }

  after_create :create_sequence
  before_save :update_timestamps, if: :state_changed?

  scope :most_recent, -> { order(id: :desc) }
  scope :active, -> { where(state: "active") }
  scope :not_deleted, -> { where.not(state: "deleted") }
  scope :lab, -> { where("path LIKE ?", "#{WORKFLOWS_LAB_PATH}%") }
  scope :prod, -> { where("path LIKE ?", "#{WORKFLOWS_PATH}%") }

  WORKFLOWS_PATH = ".github/workflows/"
  WORKFLOWS_LAB_PATH = ".github/workflows-lab/"
  WORKFLOW_PATHS = [WORKFLOWS_PATH, WORKFLOWS_LAB_PATH]
  PLACEHOLDER_NAME = "(Unnamed workflow)"

  # Number of days after a scheduled workflow is marked as disabled in a public repo since last activity
  REPOSITORY_INACTIVITY_THRESHOLD = 60
  INACTIVITY_WARNING_WINDOW = 7
  FAIR_USE_RELEASE_DATE = Date.new(2020, 8, 18) # Future use

  def self.full_path_from_filename(filename)
    filename.start_with?(WORKFLOWS_PATH) ? filename : ".github/workflows/#{filename}"
  end

  # Allows API to find a workflow via ID or filename.
  def self.find_from_id_or_filename(workflow_id)
    path = Actions::Workflow.full_path_from_filename(workflow_id)
    where(id: workflow_id).or(where(path: path)).first
  end

  def filename
    path.split("/").last
  end

  # Used for filtering by workflow name.
  # The (Lab) prefix is important to filter by lab/not-lab in the Actions tab
  def visible_name
    lab? ? "#{name} (Lab)" : name
  end

  def lab?
    path.start_with?(WORKFLOWS_LAB_PATH)
  end

  def billing_usage_line_items(starting_at, ending_at)
    @billing_usage_line_items ||= Billing::ActionsUsageLineItem.within_dates(starting_at, ending_at).where(workflow: self)
  end

  def has_workflow_dispatch_trigger?
    parsed_workflow = Actions::ParsedWorkflow.parse_from_yaml(repository, path)
    parsed_workflow&.has_workflow_dispatch_trigger?
  end

  def disabled?
    disabled_fork? || disabled_inactivity? || disabled_manually?
  end

  def self.create_or_update_workflow(workflow_file_path, name, repository, state, event = nil)
    path = workflow_file_path || ""
    workflow_name = name || ""
    attrs = { path: path, repository_id: repository.id }
    state = state || "active"
    Actions::Workflow.retry_on_find_or_create_error do
      workflow = Actions::Workflow.find_by(attrs) || Actions::Workflow.new(attrs)

      state = "deleted" unless workflow_file_path.start_with?(".github/")
      if GitHub.flipper[:actions_disabled_workflows].enabled?(repository.owner)
        state = "disabled_inactivity" if event == "schedule" && repository.public && workflow.latest_timestamp < REPOSITORY_INACTIVITY_THRESHOLD.days.ago
      end

      workflow.name = workflow_name
      workflow.state = state
      workflow.save!

      if GitHub.flipper[:actions_disabled_workflows].enabled?(repository.owner)
        workflow.send_inactivity_warning if event == "schedule" && repository.public
      end

      workflow
    end
  end

  def enable(actor)
    raise CannotBeEnabledError unless disabled? || churning?

    update(state: "active", enabled_at: Time.zone.now)
    GitHub.instrument "workflows.enable_workflow", actor: actor, repo: repository, workflow: self
  end

  def disable(actor)
    raise NotActiveError unless active?

    update(state: "disabled_manually")
    GitHub.instrument "workflows.disable_workflow", actor: actor, repo: repository, workflow: self
  end

  def send_inactivity_warning
    return if !within_churning_threshold? || inactivity_email_sent?

    ActionsMailer.workflow_inactivity(repository.owner, self).deliver_later
    mark_email_sent
  end

  def latest_timestamp
    [repository.updated_at, enabled_at].compact.max
  end

  def within_churning_threshold?
    latest_timestamp < (REPOSITORY_INACTIVITY_THRESHOLD - INACTIVITY_WARNING_WINDOW).days.ago
  end

  def churning?
    return false unless repository.public
    return false unless within_churning_threshold?

    parsed_workflow = Actions::ParsedWorkflow.parse_from_yaml(repository, path)
    return parsed_workflow&.has_schedule_trigger?
  end

  def delete_if_no_runs
    return if workflow_runs.any?

    GitHub.dogstats.increment("actions.workflow_deleted_no_runs")
    update(state: "deleted")
  end

  private

  def create_sequence
    Sequence.create(self)
  end

  def update_timestamps
    if disabled?
      self.disabled_at = Time.zone.now
      self.enabled_at = nil
    elsif active?
      self.disabled_at = nil
      self.enabled_at = Time.zone.now
    end
  end

  # example key: inactive-workflow-123-4, changing this key could result in duplicate emails
  def workflow_email_inactivity_key
    ["inactive-workflow", repository.id, id].compact.join("-")
  end

  def inactivity_email_sent?
    GitHub.kv.exists(workflow_email_inactivity_key).value { false }
  end

  def mark_email_sent
    ActiveRecord::Base.connected_to(role: :writing) do
      GitHub.kv.set(workflow_email_inactivity_key, "true", expires: REPOSITORY_INACTIVITY_THRESHOLD.days.from_now)
    end
  end
end
