# frozen_string_literal: true
require "grpc"
require "github-launch"

class Actions::RunnerGroup
  SELECTED_TARGETS_ONLY = "selected"

  attr_reader :id, :name, :runners, :visibility, :selected_targets

  def initialize(id:, name:, size: 0, owner_id: nil, runners: [], visibility: :SELECTED, selected_targets: [], default: false)
    @id = id
    @name = name
    @size = size
    @owner_id = owner_id
    @runners = runners
    @visibility = visibility
    @selected_targets = selected_targets
    @default = default
  end

  def self.from_grpc_object(group)
    new(
      id: group.id,
      name: group.name,
      size: group.size,
      owner_id: group.owner_id,
      runners: Actions::Runner.from_grpc_collection(group.runners),
      visibility: group.visibility,
      selected_targets: group.selected_targets,
      default: group.is_default,
    )
  end
  private_class_method :from_grpc_object

  def self.from_grpc_collection(groups)
    groups.map { |runner_group| from_grpc_object(runner_group) }
  end
  private_class_method :from_grpc_collection

  def self.for_entity(entity, include_runners: false)
    resp = GrpcHelper.rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.list_groups(owner: entity, include_runners: include_runners)
    end

    runner_groups = resp&.value&.runner_groups || []
    from_grpc_collection(runner_groups)
  end

  # Internal: Compare two runner groups
  # Non-inherited groups are sorted before inherited
  # Default groups are sorted before other groups
  # Otherwise groups are sorted by name, case-insensitive
  #
  # other - Another RunnerGroup
  #
  # Returns the usual tri-state. See Comparable for details.
  def <=>(other)
    return nil unless other.is_a?(self.class)

    return 1 if inherited? && !other.inherited?
    return -1 if !inherited? && other.inherited?

    return -1 if default? && !other.default?
    return 1 if !default? && other.default?

    @name.downcase <=> other.name.downcase
  end

  def inherited?
    @owner_id&.global_id.present?
  end

  def default?
    # This check against the group id should be removed once Actions Service starts sending default
    @default || @id == 1
  end
end
