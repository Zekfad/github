# frozen_string_literal: true

class Actions::WorkflowRun < ApplicationRecord::Ballast
  class NotDeleteableError < StandardError; end

  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  PLACEHOLDER_TITLE = "(Unknown event)"

  belongs_to :repository
  belongs_to :workflow
  belongs_to :check_suite
  belongs_to :trigger, polymorphic: true

  delegate :duration, :head_repository, :status, :conclusion, :push, :github_app, :created_at, :updated_at, :started_at, :commit, :artifacts, to: :check_suite

  before_create :set_run_number
  before_create :copy_check_suite_fields

  after_commit :notify_socket_subscribers, on: :create

  after_commit :synchronize_search_index

  scope :most_recent, -> { order(id: :desc) }

  def billing_usage_line_items
    @billing_usage_line_items ||= Billing::ActionsUsageLineItem.where(check_run: check_suite.check_runs.pluck(:id))
  end

  def has_billing_data?
    return false if GitHub.enterprise?
    check_suite.completed? && billing_usage_line_items.any?
  end

  # It's possible for a run to not have billing data. And it's unlikely it ever will.
  # - It could be old, before we started recording data (April 5th, 2020)
  # - There could have been an incident effecting hydro or processing
  #
  # For these we show a message to the user that it's unavailable.
  def billing_data_unavailable?
    if check_suite.completed? && check_suite.completed_at
      return check_suite.completed_at < 20.minutes.ago
    end

    false
  end

  # Total duration of seconds as seen by billing for this workflow.
  # This includes all re-runs. Does NOT include the OS multiplier.
  #
  # May be slightly delayed after workflow is complete. Data from a background job.
  def billing_duration_in_seconds
    @billing_duration_in_seconds ||= billing_usage_line_items.sum(&:duration_in_seconds)
  end

  def jobs
    check_suite.check_runs
  end

  def latest_jobs
    check_suite.latest_check_runs
  end

  def expired_logs?
    check_suite.check_runs.any? { |check_run| check_run.expired_logs? }
  end

  def title
    if trigger.is_a? Issue
      return trigger.title
    elsif trigger.is_a? IssueComment
      return trigger.issue.title
    elsif trigger.is_a? PullRequest
      return trigger.title
    elsif trigger.is_a? Push
      return trigger.commits.last.message.truncate(200) if trigger.commits.last&.message.present?
    elsif trigger.is_a? Release
      return trigger.name.present? ? trigger.name : trigger.tag_name
    elsif trigger.is_a? Deployment
      return trigger.environment if trigger.environment.present?
    end

    return check_suite.action if check_suite.event == "repository_dispatch" && check_suite.action.present?

    workflow.name.presence || PLACEHOLDER_TITLE
  end

  def permalink
    "#{check_suite.repository.permalink(include_host: true)}/actions/runs/#{id}"
  end

  # Temporary method to copy fields from the corresponding check suite.
  # In the future these fields will only be in this model and not in the check suite model
  def copy_check_suite_fields
    self.event = check_suite.event
    self.action = check_suite.action
    self.name = check_suite.name
    self.head_branch = check_suite.head_branch
    self.head_sha = check_suite.head_sha
    self.workflow_file_path = check_suite.workflow_file_path
    self.external_id = check_suite.external_id
    self.repository = check_suite.repository
    self.completed_log_url = check_suite.completed_log_url
  end

  ############################################################################
  ## Search

  # Public: Synchronize this workflow run with its representation in the search
  # index. If the workflow run is newly created or modified in some fashion, then
  # it will be updated in the search index. If the workflow run has been
  # destroyed, then it will be removed from the search index. This method
  # handles both cases.
  #
  def synchronize_search_index
    if self.destroyed? || check_suite.nil?
      RemoveFromSearchIndexJob.perform_later("workflow_run", self.id)
    elsif self.workflow_file_path.present?
      Search.add_to_search_index("workflow_run", self.id)
    end
    self
  end

  def self.search(query:, repo: nil, workflow: nil, workflow_id: nil, current_user: nil, remote_ip: nil, user_session: nil, page: 1, per_page: 25, **kargs)
    lab = workflow&.end_with?("(Lab)")
    workflow = workflow.sub(" (Lab)", "") if lab
    query = "workflow:\"#{workflow}\" #{query}" if workflow

    unless query.is_a?(String)
      raise ArgumentError, "query must be a String: #{query.class}"
    end

    hash = {
      phrase: query,
      query: Search::Queries::WorkflowRunQuery.parse(query),
      repo_id: repo&.id,
      aggregations: :state,
      page: page,
      per_page: per_page,
      current_user: current_user,
      remote_ip: remote_ip,
      user_session: user_session,
      source_fields: false,
      lab: lab,
      workflow_id: workflow_id,
    }

    es = ::Search::Queries::WorkflowRunQuery.new(hash).execute

    workflow_runs = WillPaginate::Collection.create(page, per_page, es.total) do |pager|
      pager.replace es.results.map { |result| result["_model"] }
    end

    {
      workflow_runs: workflow_runs,
      total_count: es.total
    }
  rescue Search::Query::MaxOffsetError
    {
      workflow_runs: [],
      total_count: 0
    }
  end

  def workflow_name
    name = workflow.name
    name.present? ? name : Actions::Workflow::PLACEHOLDER_NAME
  end

  def short_head_sha
    head_sha.first(Commit::ABBREVIATED_OID_LENGTH)
  end

  def deleteable?
    return true if check_suite.completed?

    check_suite.created_at < Time.zone.now - CheckSuite::DEFAULT_STALE_THRESHOLD
  end

  def hard_delete(actor:)
    raise NotDeleteableError unless deleteable?

    check_suite.destroy
    destroy

    if GitHub.flipper[:actions_delete_workflow_last].enabled?
      workflow.delete_if_no_runs
    end

    GitHub.instrument "workflows.delete_workflow_run", actor: actor, repo: repository, workflow_run: self
  end

  def workflow_runs_channel
    GitHub::WebSocket::Channels.workflow_runs(repository)
  end

  private
  # Internal: Do the actual WebSocket notification
  def notify_socket_subscribers
    return unless GitHub.flipper[:actions_workflow_live_updates].enabled?

    data =
      {
        timestamp: created_at,
        reason: "workflow_run ##{id} created: #{status}",
        wait: default_live_updates_wait + 3000,
      }

    GitHub::WebSocket.notify_repository_channel(repository, workflow_runs_channel, data)
  end

  def set_run_number
    self.run_number = Sequence.next(workflow)
  end
end
