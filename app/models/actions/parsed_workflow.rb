# frozen_string_literal: true

class Actions::ParsedWorkflow
  WORKFLOW_DISPATCH = "workflow_dispatch"
  SCHEDULE = "schedule"

  def self.parse_from_yaml(repository, path, branch = nil)
    return nil unless repository
    return nil if path.empty?

    workflow_file = repository.workflow_content(repository, branch || repository.default_branch, path)
    return nil unless workflow_file && workflow_file.data


    begin
      content = GitHub::Encoding.strip_bom(workflow_file.data.to_s)
      file_data = YAML.safe_load(content)
    rescue Psych::Exception
      file_data = {}
    end

    file_data = {} unless file_data.is_a?(Hash)

    return Actions::ParsedWorkflow.new(path, file_data)
  end

  def initialize(path, data)
    @path = path
    @data = data
  end

  def name
    @data["name"] || @path
  end

  def has_workflow_dispatch_trigger?
    triggers.key?(WORKFLOW_DISPATCH)
  end

  def has_schedule_trigger?
    triggers.key?(SCHEDULE)
  end

  def workflow_dispatch_inputs
    return nil unless has_workflow_dispatch_trigger?

    trigger = triggers[WORKFLOW_DISPATCH]
    return nil unless trigger

    inputs = trigger["inputs"]
    return nil unless inputs && inputs.is_a?(Hash)

    result = {}

    inputs.each do |key, value|
      # If value is defined, we expect it to be a map in yaml/hash
      return nil if value && !value.is_a?(Hash)

      # None of the values are required, provide a default for each one if it's not set
      result[key] = {
        description: value && value["description"] || "",
        required: value && !!value["required"],
        default: value && value["default"] || "",
      }
    end

    result
  end

  def process_inputs(provided_inputs)
    provided_inputs = provided_inputs || {}
    expected_inputs = workflow_dispatch_inputs || {}

    return nil if provided_inputs.empty? && expected_inputs.empty?

    only_known_inputs_provided = (provided_inputs.keys - expected_inputs.keys).empty?
    raise ArgumentError, "Unexpected inputs provided" unless only_known_inputs_provided

    input_values = {}

    expected_inputs.each do |key, value|
      if provided_inputs[key].present?
        raise ArgumentError, "Invalid value for input '#{key}'" unless provided_inputs[key].is_a?(String)

        input_values[key] = provided_inputs[key]
      elsif value[:default].present?
        input_values[key] = value[:default]
      elsif value[:required]
        raise ArgumentError, "Required input '#{key}' not provided"
      end
    end

    input_values
  end

  private

  def triggers
    @triggers ||= begin
      # Psych parses "on:" as true if it's not quoted as per the YAML 1.1 spec.
      triggers = @data[true] || @data["on"]
      if triggers.is_a?(Array)
        return triggers.map { |x| [x, {}] }.to_h
      elsif triggers.is_a?(Hash)
        return triggers
      elsif triggers.is_a?(String)
        return { triggers => {} }
      end

      {}
    end
  end
end
