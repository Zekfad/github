# frozen_string_literal: true

class SponsorsListing < ApplicationRecord::Collab
  self.ignored_columns = %w(docusign_envelope_id docusign_envelope_status)

  extend GitHub::Encoding
  force_utf8_encoding :short_description, :full_description

  extend GitHub::BackgroundDependentDeletes

  include Workflow
  include GitHub::Relay::GlobalIdentification
  include UserHovercard::SubjectDefinition
  include Instrumentation::Model
  include GitHub::Validations
  include SponsorsListing::DocusignDependency
  include SponsorsListing::StripeConnectLinkErrorDependency
  include SponsorsListing::TransitionDuplicateStripeAccountDependency
  include SponsorsListing::ZuoraDependency

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::SponsorsListing

  enum sponsorable_type: {
    User: 0,
    Organization: 1,
  }

  MAX_FULL_DESCRIPTION_LENGTH = 5_000
  MAX_SHORT_DESCRIPTION_LENGTH = 250
  PAYOUT_PROBATION_DAYS = 90
  MONTHLY_PAYOUT_DAY = 22
  AUTO_APPROVE_WAIT_TIME = 5.minutes

  belongs_to :sponsorable, polymorphic: true, required: true
  belongs_to :sponsors_membership, required: true

  has_one :active_goal, -> { active }, class_name: :SponsorsGoal
  has_many :goals, class_name: :SponsorsGoal, dependent: :destroy
  has_many :sponsors_tiers, dependent: :destroy
  has_many :published_sponsors_tiers, -> { with_published_state }, class_name: :SponsorsTier
  has_many :featured_items, class_name: :SponsorsListingFeaturedItem, dependent: :destroy
  has_many :sponsors_fraud_reviews
  has_many :featured_users, -> { for_users.order(:position) }, class_name: :SponsorsListingFeaturedItem
  accepts_nested_attributes_for :featured_users, allow_destroy: true, limit: SponsorsListingFeaturedItem::FEATURED_USERS_LIMIT_PER_LISTING
  has_many :featured_repos, -> { for_repos.order(:position) }, class_name: :SponsorsListingFeaturedItem
  accepts_nested_attributes_for :featured_repos, allow_destroy: true, limit: SponsorsListingFeaturedItem::FEATURED_REPOS_LIMIT_PER_LISTING

  has_many :hooks, as: :installation_target
  destroy_dependents_in_background :hooks

  has_one :stripe_connect_account, class_name: "Billing::StripeConnect::Account", as: :payable, dependent: :destroy
  delegate :stripe_transfers, to: :stripe_connect_account, prefix: true, allow_nil: true

  delegate :stripe_account_id, to: :stripe_connect_account, allow_nil: true

  # Public: Select SponsorsListing with a slug, short_description, or full_description that
  # matches a query.
  #
  # query - the query string
  scope :matches_slug_or_description, ->(query) do
    sanitized_query = "%#{self.sanitize_sql_like(query)}%"

    where(<<-SQL, sanitized_query: sanitized_query)
      sponsors_listings.slug LIKE :sanitized_query OR
      sponsors_listings.short_description LIKE :sanitized_query OR
      sponsors_listings.full_description LIKE :sanitized_query
    SQL
  end

  scope :on_payout_probation, -> {
    where("payout_probation_started_at IS NOT NULL")
      .where("payout_probation_ended_at IS NULL")
  }

  before_validation :set_slug

  validates :sponsorable_id, uniqueness: { scope: :sponsorable_type }
  validates :slug, presence: true, uniqueness: { case_sensitive: false }
  validates :full_description, presence: true, unless: :draft?
  validates :full_description, length: { maximum: MAX_FULL_DESCRIPTION_LENGTH }
  validates :short_description,
    bytesize: {
      maximum: MAX_SHORT_DESCRIPTION_LENGTH,
      message: "is too long (maximum is #{MAX_SHORT_DESCRIPTION_LENGTH} characters)",
    },
    allow_blank: true

  after_commit :record_match_limit_reached, if: :match_limit_reached_at_previously_changed?

  alias_attribute :name, :slug
  alias_attribute :braintree_addon_slug, :slug
  alias_attribute :listable, :sponsorable
  alias_attribute :owner, :sponsorable
  alias_attribute :async_owner, :async_sponsorable

  # Get the Integer value matching a certain state.
  def self.state_value(name)
    workflow_spec.states[name.to_sym].value
  end

  workflow :state do
    state :draft, 0 do
      event :request_verification, transitions_to: :pending_verification

      # TODO: Once we figure out verification steps, remove this event so that
      # user must be verified before requesting approval.
      event :request_approval, transitions_to: :verified_and_pending_approval, if: :ready_for_submission?
      event :disable, transitions_to: :disabled
    end

    # Verification of identity
    state :pending_verification, 1 do
      event :verify, transitions_to: :verified
      event :fail_verification, transitions_to: :failed_verification
      event :request_approval, transitions_to: :unverified_and_pending_approval, if: :ready_for_submission?
      event :disable, transitions_to: :disabled
    end

    state :verified, 2 do
      event :request_approval, transitions_to: :verified_and_pending_approval, if: :ready_for_submission?
      event :disable, transitions_to: :disabled
    end

    state :unverified_and_pending_approval, 3 do
      event :verify, transitions_to: :verified_and_pending_approval
      event :disable, transitions_to: :disabled
    end

    state :verified_and_pending_approval, 4 do
      event :approve, transitions_to: :approved
      event :disable, transitions_to: :disabled
      event :cancel_approval_request, transitions_to: :draft
    end

    state :failed_verification, 5 do
      event :reverify, transitions_to: :pending_verification #, unless hard_failed_verification?
      event :disable, transitions_to: :disabled
    end

    state :approved, 6 do
      event :unpublish, transitions_to: :verified_and_pending_approval
      event :unpublish_and_unverify, transitions_to: :unverified_and_pending_approval
      event :disable, transitions_to: :disabled
      event :redraft, transitions_to: :draft, if: :no_active_subscription_items?
    end

    state :disabled, 7 do
      event :re_enable, transitions_to: :draft
    end

    on_transition do |from, to, event, *args, **kwargs|
      SponsorsListingZuoraSyncJob.perform_later(self) if to == :approved
    end
  end

  # Public: Is this sponsors listing ready to submit for approval?
  #
  # Returns a Boolean.
  def ready_for_submission?
    full_description.present? &&
      sponsors_tiers.with_states(:published).any? &&
      !verified_and_pending_approval? &&
      stripe_connect_account&.verified?
  end

  # Public: Strip trailing whitespaces on setting short_description
  def short_description=(value)
    super(value.strip) if value
  end

  # Public: Strip carriage returns \r on setting full_description
  def full_description=(value)
    super(remove_windows_line_endings(value)) if value
  end

  # Public: sets the featured users' position based on the params array index
  def set_featured_users(featured_users_params)
    return true unless sponsorable.organization?

    set_featured_items(
      items_params: featured_users_params,
      attributes_key: :featured_users,
    )
  end

  # Public: sets the featured repos' position based on the params array index
  def set_featured_repos(featured_repos_params)
    set_featured_items(
      items_params: featured_repos_params,
      attributes_key: :featured_repos,
    )
  end

  # Public: This method is needed for evaluation on Billing::Pricing.
  # TODO: Remove when de-coupling of SponsorsListing from Marketplace::Listing is complete.
  # See https://github.com/github/sponsors/issues/353 for details
  def is_marketplace?
    false
  end

  # Public: This method is needed for evaluation on Billing::Pricing.
  # TODO: Remove when de-coupling of SponsorsListing from Marketplace::Listing is complete.
  # See https://github.com/github/sponsors/issues/353 for details
  #
  # NOTE: we may not be able to remove this - various code that handles Billing::SubscriptionItem
  # may need to know whether the subscribable is a sponsorable (eg. SubscriptionItemsController#destroy)
  def listable_is_sponsorable?
    true
  end

  # Public: This method is needed for Billing::SubscriptionItem method delegation on listing.
  # TODO: Remove when de-coupling of SponsorsListing from Marketplace::Listing is complete.
  # See https://github.com/github/sponsors/issues/353 for details
  def listable_is_integration?
    false
  end

  # Public: This method is needed for Billing::UpdateSubscriptionItem method listing.
  # TODO: Remove when de-coupling of SponsorsListing from Marketplace::Listing is complete.
  # See https://github.com/github/sponsors/issues/353 for details
  def listable_is_oauth_application?
    false
  end

  # Public: Is this listing in a billable state?
  def billable?
    approved?
  end

  # Public: Returns true if the given actor (User) has admin access to this listing
  def async_adminable_by?(actor)
    return Promise.resolve(false) unless actor.present?

    async_sponsorable.then do |sponsorable|
      allowed_ids = sponsorable.user? ? [sponsorable.id] : sponsorable.direct_admin_ids
      allowed_ids.include?(actor.id)
    end
  end

  # Public: Returns true if the given User has admin access to this listing.
  def adminable_by?(actor)
    async_adminable_by?(actor).sync
  end

  # Public: Indicates if the listing is readable by an actor.
  #
  # Returns a Boolean.
  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  # Public: Indicates if the listing is readable by an actor.
  #
  # Returns a Promise<Boolean>.
  def async_readable_by?(actor)
    Promise.all([
      async_sponsorable,
      async_adminable_by?(actor),
    ]).then do |sponsorable, is_adminable|
      next false if disabled? && !actor.can_admin_sponsors_listings?
      next true if is_adminable
      next true if approved?
      next false if actor.blank?

      actor.can_admin_sponsors_listings?
    end
  end

  # TODO: Need to confirm the logic here. Should we return false on any other states?
  # Public: Returns true if the given User has permission to change the listing.
  def editable_by?(actor)
    return false if failed_verification?

    adminable_by?(actor)
  end

  def stripe_transfers_enabled?
    stripe_connect_account.present?
  end

  # Public: The listing's default plan - a published free trial plan if one exists,
  #         or any published plan otherwise.
  def default_tier
    sponsors_tiers.with_published_state.first
  end

  # TODO: Need to confirm the logic here. When should we allow tiers to be published for a listing?
  # Public: Returns true if tiers are permitted given the listing's state.
  # Prevents maintainers from publishing tiers on listings that have failed verification.
  def published_tiers_allowed?
    !failed_verification?
  end

  # Public: Returns an integer count of how many more tiers can be added
  # to this listing.
  def remaining_tier_count
    SponsorsTier::PUBLISHED_TIER_COUNT_LIMIT_PER_LISTING - sponsors_tiers.with_published_state.count
  end

  # Public: Returns true if this listing has reached the maximum allowed number
  # of published tiers.
  def reached_maximum_tier_count?
    remaining_tier_count <= 0
  end

  # Public: Find the Marketplace::Listing based on this SponsorsListing
  # This is a temporary method used while we decouple Marketplace::Listing
  # Still being used for Billing::BillingTransaction::LineItem#listing=
  def find_corresponding_marketplace_listing
    Marketplace::Listing.sponsorable.find_by slug: self.slug
  end

  # Public: Returns `Billing::SubscriptionItem` associated with this Sponsors listing's tiers.
  def subscription_items
    Billing::SubscriptionItem.for_sponsors_listing(id)
  end

  # Public: Returns the combined monthly recurring value of active sponsorships for this listing.
  def subscription_value
    counts = subscription_items.active.group(:subscribable_id).count
    return 0 if counts.empty?

    sponsors_tiers.select(:id, :monthly_price_in_cents).where(id: counts.keys).inject(0) do |total, tier|
      total + tier.monthly_price_in_cents * counts[tier.id]
    end
  end

  def enable_sponsors_match!(actor:)
    update!(match_disabled: false)
    instrument :enable_match, GitHub.guarded_audit_log_staff_actor_entry(actor)
  end

  def disable_sponsors_match!(actor:, reason:)
    transaction do
      update!(match_disabled: true)
      sponsors_membership.staff_notes.create!(
        note: "Matching was disabled by staff. Reason: #{reason}",
        user: actor,
      )
    end

    payload = GitHub.guarded_audit_log_staff_actor_entry(actor).merge({ reason: reason })
    instrument :disable_match, payload
  end

  def disable_payouts
    return unless stripe_connect_account
    ConfigureStripeAccountJob.perform_later(stripe_connect_account, freeze_payouts: true)
  end

  def disable_payouts!
    return false unless stripe_connect_account
    Sponsors::ConfigureStripeAccount.call(account: stripe_connect_account, freeze_payouts: true)
  end

  def enable_payouts
    return unless stripe_connect_account
    ConfigureStripeAccountJob.perform_later(stripe_connect_account, freeze_payouts: false)
  end

  def enable_payouts!
    return false unless stripe_connect_account
    Sponsors::ConfigureStripeAccount.call(account: stripe_connect_account, freeze_payouts: false)
  end

  # Public: Indicates if this sponsorable is currently within the payout
  #         probation period.
  #
  # Returns a Boolean.
  def on_payout_probation?
    payout_probation_started_at.present? && payout_probation_ended_at.nil?
  end

  # Public: Indicates if the payout probation period has ended for this listing.
  #
  # Returns a Boolean.
  def completed_payout_probation?
    payout_probation_ended_at.present?
  end

  # Public: Is this listing exempt from the payout probation period?
  #
  # Returns a Boolean.
  def exempt_from_payout_probation?
    return true if sponsorable.organization?
    !sponsors_membership.joined_waitlist_before_match_deadline?
  end

  # Public: The next date that this listing can receive a payout.
  #         Is nil if a sponsorable has not yet received a sponsorship.
  #
  # Returns a Date|nil.
  def next_payout_date
    return if payout_probation_started_at.nil?

    start_date = if on_payout_probation?
      [payout_probation_started_at + PAYOUT_PROBATION_DAYS.days, Date.today].max
    else
      latest_payout_date = async_latest_payout_created.sync
      DateTime.now
    end

    eligible_date = if start_date.day > MONTHLY_PAYOUT_DAY || start_date.month == latest_payout_date&.month
      start_date + 1.month
    else
      start_date
    end

    Date.new(eligible_date.year, eligible_date.month, MONTHLY_PAYOUT_DAY)
  end

  # Public: The next date that this listing can receive a payout formatted as "month day".
  #         Is nil if a sponsorable has not yet received a sponsorship.
  #
  # Returns a String|nil.
  def next_payout_date_formatted
    return unless next_date = next_payout_date
    next_date.strftime("%B #{next_date.day.ordinalize}")
  end

  def payout_probation_end_date
    return if payout_probation_started_at.nil?
    return payout_probation_ended_at if completed_payout_probation?
    payout_probation_started_at + PAYOUT_PROBATION_DAYS.days
  end

  def matchable?
    return false if match_disabled?
    return false unless published_in_last_year?
    return false if sponsorable.organization?
    return false unless sponsorable.sponsors_membership.joined_waitlist_before_match_deadline?
    !reached_match_limit?
  end

  def reached_match_limit?
    total_match_in_cents >= Sponsors::MATCHING_LIMIT_AMOUNT_IN_CENTS
  end

  def total_match_in_cents
    @total_match_in_cents ||= async_total_match_in_cents.sync
  end

  def reset_total_match_in_cents!
    @total_match_in_cents = nil
  end

  def async_total_match_in_cents
    async_stripe_connect_account.then do |stripe_account|
      next 0 if stripe_account.nil?
      stripe_account.async_total_match_in_cents
    end
  end

  # Public: Can this listing be approved by an actor?
  #
  # actor - The User that wants to approve the listing.
  #
  # Returns a Boolean.
  def approvable_by?(actor)
    async_approvable_by?(actor).sync
  end

  def async_approvable_by?(actor)
    return Promise.resolve(false) if actor.blank?
    return Promise.resolve(false) unless can_approve?

    async_duplicate_stripe_account?.then do |has_duplicate_stripe|
      next false if has_duplicate_stripe

      actor.can_admin_sponsors_listings?
    end
  end

  # Public: Can this listing be unpublished by an actor?
  #
  # actor - The User that wants to unpublish the listing.
  #
  # Returns a Boolean.
  def unpublishable_by?(actor)
    return false unless actor.present?
    return false unless can_unpublish?

    actor.can_admin_sponsors_listings?
  end

  # Public: Is this listing eligible to be auto approved?
  #
  # Returns a Boolean.
  def auto_approvable?
    return false unless GitHub.flipper[:sponsors_automated_listing_approval].enabled?
    return false if sponsorable.organization?
    return false unless docusign_completed?
    return false unless stripe_connect_account.present?
    return false if sponsorable.sponsors_membership.banned?

    sponsors_tiers.with_published_state.any? &&
    stripe_connect_account.verified?         &&
    full_description.present?
  end

  def published_in_last_year?
    return true if published_at.nil?
    published_at >= 1.year.ago.to_date
  end

  # Public: Is the Stripe Connect account linked to this listing also linked
  #         to another listing? See https://github.com/github/sponsors/issues/1431.
  #
  # Returns a Boolean.
  def duplicate_stripe_account?
    async_duplicate_stripe_account?.sync
  end

  def async_duplicate_stripe_account?
    async_stripe_connect_account.then do |stripe_account|
      next false if stripe_account.blank?

      Billing::StripeConnect::Account.where(
        stripe_account_id: stripe_account.stripe_account_id,
      ).where.not(payable: self).exists?
    end
  end

  def milestone_email_sent?
    milestone_email_sent_at.present?
  end

  def milestone_email_sent!
    update!(milestone_email_sent_at: Time.zone.now)
  end

  private

  def no_active_subscription_items?
    subscription_items.active.none?
  end

  # Private: Get latest payout to ensure correct next payout date.
  def async_latest_payout_created
    async_stripe_connect_account.then do |stripe_account|
      next unless stripe_connect_account

      latest_payout = stripe_connect_account.latest_payout
      next unless latest_payout

      created = latest_payout.created
      next unless created

      @latest_payout_created ||= Time.at(created)
                                     .to_datetime
    end
  end

  # Private: Called when calling `disable!` for state transition.
  def disable(disabled_by:)
    results = subscription_items.active.map do |subscription_item|
      subscription_item.cancel!(actor: disabled_by, force: true)
    end

    return halt "Unable to cancel active subscriptions" if results.any? { |result| !result[:success] }
  end

  # Private: Called when calling `redraft!` for state transition.
  def redraft(*args, **kwargs)
    GitHub.instrument("sponsors.sponsored_developer_redraft", {
      user: sponsorable,
    })

    GlobalInstrumenter.instrument("sponsors.sponsored_developer_redraft", {
      user: sponsorable,
      action: :REDRAFTED,
    })
  end

  # Private: Called when calling `cancel_approval_request!` for state transition.
  def cancel_approval_request(*args, **kwargs)
    GitHub.instrument("sponsors.sponsored_developer_redraft", {
      user: sponsorable,
      redrafted_because: "approval request canceled",
    })

    GlobalInstrumenter.instrument("sponsors.sponsored_developer_redraft", {
      user: sponsorable,
      action: :REDRAFTED,
    })
  end

  # Private: Called after a listing is transitioned to `verified_and_pending_approval`.
  def on_verified_and_pending_approval_entry(previous_state, event, *args, **kwargs)
    SponsorsMailer.approval_request_submitted(
      sponsorable: sponsorable,
    ).deliver_later

    GitHub.instrument("sponsors.sponsored_developer_request_approval", {
      user: sponsorable,
    })

    GlobalInstrumenter.instrument("sponsors.sponsored_developer_request_approval", {
      user: sponsorable,
      action: :REQUESTED_APPROVAL,
    })

    if auto_approvable?
      AutoApproveSponsorsListingJob.set(wait: AUTO_APPROVE_WAIT_TIME).perform_later(self)
    end
  end

  # Private: Called when calling `approve!` for state transition.
  def approve(viewer = nil, **kwargs)
    return halt "Can't approve listing without a verified Stripe account" unless stripe_connect_account&.verified?
    return halt "Can't approve listing with a duplicate Stripe account" if duplicate_stripe_account?

    update_attribute(:published_at, Time.zone.now)
    SponsorsPrimerMailer.listing_approved(sponsorable: sponsorable).deliver_later

    if sponsorable.sponsorships_as_sponsor.active.any?
      CreateMatchDisabledSponsorsActivityJob.perform_later(listing: self)
    end
  end

  def event_payload
    payload = { sponsors_listing: self }
    payload.merge!(sponsorable.event_context)
  end

  # Internal: Set the slug based on sponsorable's login. This is assuming sponsorable is a User.
  #
  # Returns the slug that was set, or nil if slug was set to nil or not set.
  def set_slug
    return unless slug.nil?

    if sponsorable.nil?
      self.slug = nil
    else
      self.slug ||= "sponsors-#{sponsorable.login}"
    end
  end

  # Internal: Replaces any instances of \r\n with \n.
  # Need to do this because forms POST with \r\n and it breaks our character counts.
  #
  # string
  #
  # Returns filtered string.
  def remove_windows_line_endings(value)
    value.gsub("\r\n", "\n")
  end

  # Internal: Sets featured items based on their type and ordered by the array index.
  #
  # Returns true if it's successful, false otherwise.
  def set_featured_items(items_params: [], attributes_key:)
    to_remove = items_params.select { |item| item[:_destroy] }
    to_set = items_params - to_remove

    if to_remove.present?
      return false unless update("#{attributes_key}_attributes": to_remove)
    end

    items_params = Array(to_set).map.with_index do |params, index|
      params.merge(position: index + 1)
    end

    update("#{attributes_key}_attributes": items_params)
  end

  def record_match_limit_reached
    return if match_limit_reached_at.blank?

    GlobalInstrumenter.instrument("sponsors.sponsors_listing_match_limit_reached", {
      listing: self,
    })
    SponsorsMailer.reached_match_cap(sponsorable: sponsorable).deliver_later
  end
end
