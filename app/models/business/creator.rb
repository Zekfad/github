# frozen_string_literal: true

# Creates a new Business, optionally with a single initial organization. If the provided
# organization is changing to the Business Plus plan, also sets up billing for
# that organization. Also responsible for setting up a business's customer relation.
#
# See also SeatsController#switch, Stafftools::BusinessController#create
class Business::Creator
  attr_reader :business_params, :organization, :organization_plan, :error_message, :require_owners
  attr_writer :organization
  attr_writer :organization_plan

  # business_params   - hash of fields to set on new Business
  # organization      - the Organization joining the new Business
  # organization_plan - if the organization is changing plans in this same request, their desired plan
  # require_owners    - flag indicating whether the Business must be created with initial owners
  def initialize(business_params:, organization: nil, organization_plan: nil, require_owners: true)
    @business_params = business_params
    # TODO check if we're already sending customer attributes here? we should be...
    @organization = organization
    @organization_plan = GitHub::Plan.find(
      organization_plan,
      effective_at: organization&.plan_effective_at,
    )
    @organization_plan ||= organization&.plan
    @require_owners = require_owners

    @error_message = nil
    @business = nil
  end

  def valid?
    if organization
      unless business_params.present?
        # An org is changing its plan but isn't
        # upgrading to a business (or already belongs to one). We're "valid"
        # in that we don't have anything to create and save! becomes a no-op
        if !organization.business && organization_plan.business_plus?
          @error_message = "An enterprise account is required for the #{GitHub::Plan.business_plus.titleized_display_name} plan"
          return false
        else
          return true
        end
      end

      unless organization_plan.business_plus?
        @error_message = "Enterprise accounts can only be created on the #{GitHub::Plan.business_plus.titleized_display_name} plan"
        return false
      end

      if organization.business
        @error_message = "Organization already belongs to an enterprise"
        return false
      end
    else
      unless business_params.present?
        @error_message = "Enterprise account information required."
        return false
      end
    end

    if require_owners && owners.blank?
      @error_message = "An Enterprise account needs at least one owner"
      return false
    end

    if business.valid?
      return true
    else
      @error_message = business.errors.full_messages.to_sentence
      return false
    end
  end

  def business_attributes
    business_params.merge(
      customer_attributes: {
        billing_end_date: billing_end_date,
        name: business_params[:name],
        billing_type: business_params.dig(:customer_attributes, :billing_type) || "card",
        billing_attempts: 0,
        term_length: business_params.dig(:customer_attributes, :term_length) || 12,
      })
  end

  def owners
    business_params[:owners]
  end

  def billing_email
    business_params[:billing_email]
  end

  def billing_end_date
    business_params.dig(:customer_attributes, :billing_end_date) || GitHub::Billing.today + 1.year
  end

  def business
    return @business if @business
    return nil unless business_params.present?

    business = Business.new(business_attributes)

    business.seats = business_params.fetch(:seats, organization&.default_seats)

    # N.B. this field will eventually be retired
    business.billing_term_ends_at = billing_end_date
    @business = business
  end

  def save!
    unless valid?
      raise ArgumentError, @error_message
    end
    return unless business

    if organization
      Business.transaction do
        organization.billing_email = billing_email
        organization.save

        business.save!

        new_membership = business.add_organization(organization)
        new_membership.save!
      end
    else
      business.save!
    end
  end
end
