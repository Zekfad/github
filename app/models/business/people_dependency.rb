# frozen_string_literal: true

module Business::PeopleDependency

  ROLE_ORG_MEMBER_TYPE = {
    "member" => :member_without_admin,
    "owner" => :admin,
    nil => :all,
  }.freeze

  # Public: Returns filtered outside collaborators for the enterprise.
  #
  # login               - optional - The user login to find. If passed only exact matches for `users.login` will be returned
  # query               - optional — The user-provided query string with any filters (example: `visibility`) removed
  # order_by_field      - optional — String specifying the sort field. Supported: "login", "created_at". Default: "login".
  # order_by_direction  - optional — String specifying the sort direction. Supported: "asc", "desc". Default: "asc".
  # visibility          - optional — an Array of symbols, only return outside collaborators who have access to
  #                      repositories with this visibility.
  #
  # Returns an ActiveRecord::Relation
  def filtered_outside_collaborators(
    login: nil,
    query: nil,
    order_by_field: "login",
    order_by_direction: "asc",
    visibility: nil
  )
    # for easier handling of filter values from requests we'll assume nil here means "show everything"
    visibility ||= [:public, :private]

    collaborators = outside_collaborators(
      on_repositories_with_visibility: visibility,
    )

    collaborators = if login.present?
      apply_login_filter(collaborators, login)
    else
      apply_user_query_filter(collaborators, query)
    end

    # Ignore invalid order by field and direction
    order_by_field = "login" unless %w(login created_at).include?(order_by_field.to_s.downcase)
    order_by_direction = "asc" unless %w(asc desc).include?(order_by_direction.to_s.downcase)
    collaborators.order(Arel.sql("users.#{order_by_field} #{order_by_direction}"))
  end

  # Public: Returns filtered members of the enterprise.
  #
  # viewer - required - the user requesting a list of filtered enterprise members
  #
  # optional arguments:
  # organization_logins - an Array of organization logins. only return users with organization membership in these orgs.
  # query               - The user-provided query string with any filters (example: `visibility`) removed
  # order_by_field      - String specifying the sort field. Supported: "login", "created_at". Default: "login".
  # order_by_direction  - String specifying the sort direction. Supported: "asc", "desc". Default: "asc".
  # role                - only return users matching this role, either `owner`, `member`, or nil
  # deployment          - only return users who exist in a specific type of deployment. either nil (all), cloud (GHEC), or server (GHES)
  # ignore_org_membership_visibility - optionally ignore visibility rules, used to show _all_ enterprise members in stafftools to non enterprise members
  # license             - only return users whose license is one of :volume, :enterprise, or both (nil)
  #
  # Returns an ActiveRecord::Relation
  def filtered_members(
    viewer,
    organization_logins: nil,
    query: nil,
    order_by_field: "login",
    order_by_direction: "asc",
    role: nil,
    deployment: nil,
    ignore_org_membership_visibility: false,
    license: nil
  )
    org_member_type = ROLE_ORG_MEMBER_TYPE[role]

    users_via_org_membership = visible_organization_members_for(
      viewer,
      type: org_member_type,
      ignore_visibility: ignore_org_membership_visibility,
      org_ids: org_database_ids(organization_logins),
    )

    users_via_org_membership = apply_user_query_filter(users_via_org_membership, query)
    unless owner?(viewer)
      users_via_org_membership = users_via_org_membership.filter_spam_for(viewer)
    end
    users_via_org_membership = users_via_org_membership.distinct

    scope = if GitHub.single_business_environment?
      users_via_org_membership
    else
      business_user_account_scope = case deployment
      when nil
        # all
        cloud_user_accounts = BusinessUserAccount.
          left_joins(:enterprise_installation_user_accounts).
          where(user_id: users_via_org_membership.pluck(:id))
        server_only_user_accounts = enterprise_user_scope(query, role, organization_logins)
        # business user accounts from the cloud, pre-filtered UNION filtered business user accounts from Servers
        cloud_user_accounts.or(server_only_user_accounts).distinct
      when "cloud"
        # business user accounts from the cloud, pre-filtered
        BusinessUserAccount.where(user_id: users_via_org_membership.pluck(:id))
      when "server"
        # filtered business user accounts from Servers
        enterprise_user_scope(query, role, organization_logins).distinct
      end

      business_user_account_scope.where(business_id: id)
    end

    scope = filter_by_license(scope, license)

    # Ignore invalid order by field and direction
    order_by_field = "login" unless %w(login created_at).include?(order_by_field.to_s.downcase)
    order_by_direction = "asc" unless %w(asc desc).include?(order_by_direction.to_s.downcase)
    scope.order(Arel.sql("#{scope.table_name}.#{order_by_field} #{order_by_direction}"))
  end

  # Public: get the administrators (owners and billing managers) for this business.
  #
  # query:              - optional - The user-provided query string with any filters (example: `role`) removed
  # order_by_field      - optional — String specifying the sort field. Supported: "login", "created_at". Default: "login".
  # order_by_direction  - optional — String specifying the sort direction. Supported: "asc", "desc". Default: "asc".
  # role                - optional - only return admins with the given role. Valid values: [:owner, :billing_manager]
  #                                  will raise an ArgumentError if an invalid value for role is provided.
  #                                  nil role will return all the administrators
  #
  # Returns an ActiveRecord::Relation for the business' administrators
  def admins(
      query: nil,
      order_by_field: "login",
      order_by_direction: "asc",
      role: nil)
    admins_scope = admins_with_role(role)
    admins_scope = apply_user_query_filter(admins_scope, query)

    # Ignore invalid order by field and direction
    order_by_field = "login" unless %w(login created_at).include?(order_by_field.to_s.downcase)
    order_by_direction = "asc" unless %w(asc desc).include?(order_by_direction.to_s.downcase)
    admins_scope.order(Arel.sql("users.#{order_by_field} #{order_by_direction}"))
  end

  # Public: retrieve the pending administrator invitations for this business.
  #
  # login               - optional - The user login to find. If passed only exact matches for `users.login` will be returned
  # query:              - optional - The user-provided query string with any filters (example: `role`) removed
  # order_by_field      - optional — String specifying the sort field. Supported: "created_at". Default: "created_at".
  # order_by_direction  - optional — String specifying the sort direction. Supported: "asc", "desc". Default: "desc".
  # role:               - optional - an Array of symbols, only return invitations for the specified role;
  #                                  Valid values: [:owner, :billing_manager]
  #
  # Returns: ActiveRecord::Relation of BusinessMemberInvitation's
  def pending_admin_invitations(
      login: nil,
      query: nil,
      order_by_field: "created_at",
      order_by_direction: "desc",
      role: [:owner, :billing_manager]
  )
    admin_invitations = invitations.pending.
                          left_joins(:invitee).
                          with_business_role(*role)

    query = ActiveRecord::Base.sanitize_sql_like(query.to_s.strip.downcase)
    admin_invitations = if login.present?
      apply_login_filter(admin_invitations, login)
    elsif query.present?
      admin_invitations.left_joins(invitee: :profile)
                              .where(["users.login LIKE :query OR profiles.name LIKE :query OR business_member_invitations.email LIKE :query",
                                      { query: "%#{query}%" }])
                              .references(:users, :profile)
    else
      admin_invitations
    end

    # Ignore invalid order by field and direction
    order_by_field = "created_at" unless %w(created_at).include?(order_by_field.to_s.downcase)
    order_by_direction = "desc" unless %w(asc desc).include?(order_by_direction.to_s.downcase)
    admin_invitations.order("business_member_invitations.#{order_by_field} #{order_by_direction}")
  end

  # Public: Get the pending organization member invitations for any
  # organizations that are part of the business.
  #
  # NOTE: when organization invitations are bypassed and members are added directly to
  # organizations in the Enterprise environment, this method always returns
  # `OrganizationInvitation.none` even if legacy invitations from GHE 2.3 and
  # earlier still exist.
  #
  # login               - optional - The user login to find. If passed only exact matches for `users.login` will be returned
  # query:              - optional - The user-provided query string with any filters (example: `role`) removed
  # order_by_field      - optional — String specifying the sort field. Supported: "created_at". Default: nil.
  # order_by_direction  - optional — String specifying the sort direction. Supported: "asc", "desc". Default: nil.
  #
  # Returns an ActiveRecord::Relation
  def pending_member_invitations(
    login: nil,
    query: nil,
    order_by_field: nil,
    order_by_direction: nil
  )
    if GitHub.bypass_org_invites_enabled?
      OrganizationInvitation.none
    else
      scope = OrganizationInvitation.pending.
        left_joins(:invitee).
        where(organization_id: organization_ids).
        except_with_role(:billing_manager)

      query = ActiveRecord::Base.sanitize_sql_like(query.to_s.strip.downcase)
      scope = if login.present?
        apply_login_filter(scope, login)
      elsif query.present?
        scope.left_joins(invitee: :profile)
          .where(["users.login LIKE :query OR profiles.name LIKE :query OR organization_invitations.email LIKE :query", { query: "%#{query}%" }])
          .references(invitee: :profile)
      else
        scope
      end

      # Ignore invalid order by field and direction if provided
      order_by_field = nil unless %w(created_at).include?(order_by_field.to_s.downcase)
      order_by_direction = nil unless %w(asc desc).include?(order_by_direction.to_s.downcase)
      if order_by_field.present? && order_by_direction.present?
        scope = scope.order(Arel.sql("organization_invitations.#{order_by_field} #{order_by_direction}"))
      end

      scope
    end
  end
  alias_method :pending_invitations, :pending_member_invitations

  # Public: Returns the total count of unique users that have been invited to become members of
  # the enterprise.
  #
  # Returns an Integer
  def unique_pending_member_invitation_count
    invitations = pending_member_invitations
    invitations.where(email: nil).group(:invitee_id).count.size +
      invitations.where.not(email: nil).group(:email).count.size
  end

  # Public: Get the pending repository invitations for any organizations that
  # are part of the business.
  #
  # NOTE: When repository invitations are bypassed and collaborators are added
  # directly to repositories in the enterprise environment, this method always
  # returns `RepositoryInvitation.none`.
  #
  # repository_visibility - Symbol restricting invitations by repository visibility.
  #                         Either :private, :public, or :all. Default: :all
  # login               - optional - String specifying user login to find. If passed only exact matches for `users.login` will be returned.
  # query:              - optional - String specifying user-provided query string.
  # order_by_field      - optional — String specifying the sort field. Supported: "created_at". Default: "created_at".
  # order_by_direction  - optional — String specifying the sort direction. Supported: "asc", "desc". Default: "desc".
  #
  # Returns an ActiveRecord::Relation
  def pending_collaborator_invitations(
    repository_visibility: :all,
    login: nil,
    query: nil,
    order_by_field: "created_at",
    order_by_direction: "desc"
  )
    if GitHub.repo_invites_enabled?
      repository_ids = if repository_visibility == :private
        Repository.where(public: false, active: true, organization_id: organization_ids).pluck(:id)
      elsif repository_visibility == :public
        Repository.where(public: true, active: true, organization_id: organization_ids).pluck(:id)
      else
        Repository.where(active: true, organization_id: organization_ids).pluck(:id)
      end

      scope = RepositoryInvitation.where(repository_id: repository_ids)
      query = ActiveRecord::Base.sanitize_sql_like(query.to_s.strip.downcase)
      scope = if login.present?
        user_id = User.find_by(login: login)&.id
        scope.where(invitee_id: user_id)
      elsif query.present?
        # Cater for the `users` and `repository_invitations` tables being in
        # different database clusters:
        all_invitee_ids = RepositoryInvitation.where([
          "repository_id IN (:repository_ids)", { repository_ids: repository_ids }
        ]).pluck(:invitee_id)
        user_ids = User.includes(:profile).where([
          "users.id IN (:ids) AND (users.login LIKE :query OR profiles.name LIKE :query)",
          { ids: all_invitee_ids, query: "%#{query}%" }
        ]).pluck(:id)
        scope.where([
          "invitee_id IN (:user_ids) OR repository_invitations.email LIKE :query",
          { user_ids: user_ids, query: "%#{query}%" }
        ])
      else
        scope
      end

      # Ignore invalid order by field and direction
      order_by_field = "created_at" unless %w(created_at).include?(order_by_field.to_s.downcase)
      order_by_direction = "desc" unless %w(asc desc).include?(order_by_direction.to_s.downcase)
      scope = scope.order(Arel.sql("repository_invitations.#{order_by_field} #{order_by_direction}"))

      scope
    else
      RepositoryInvitation.none
    end
  end

  # Public: Get the business user accounts that don't have a user ID set but do have a primary
  # email from an enterprise installation.
  #
  # NOTE: When we're in a single business environment this always returns an empty array
  #
  # Returns an ActiveRecord::Relation (of `BusinessUserAccount`s)
  def user_accounts_with_only_emails
    return BusinessUserAccount.none if GitHub.single_business_environment?

    user_accounts.
      joins(:enterprise_installation_user_account_emails).
      where(user_id: nil).
      merge(EnterpriseInstallationUserAccountEmail.primary).
      distinct
  end

  # Public: Get the user IDs for all enterprise installation users that have a user associated to them.
  #
  # NOTE: When we're in a single business environment this always returns an empty array
  #
  # Returns an Array of User IDs
  def enterprise_installation_user_ids
    return [] if GitHub.single_business_environment?

    user_accounts.
      joins(:enterprise_installation_user_accounts).
      where.not(user_id: nil).
      distinct.
      pluck(:user_id)
  end

  private

  def apply_login_filter(scope, login)
    return scope if login.blank?

    scope.where(["users.login = :login", { login: login }])
  end

  # Private: filter the values in the given scope by the specified query string. Return only
  # the admins whose login or name includes the specified string. Return the given scope if
  # query parameter is nil.
  #
  # scope - scope of Business administrators
  # query - filter query string
  #
  # Returns an ActiveRecord::Relation for the business' administrators with the given filter applied
  def apply_user_query_filter(scope, query)
    query = ActiveRecord::Base.sanitize_sql_like(query.to_s.strip.downcase)
    return scope unless query.present?

    scope.includes(:profile)
        .where(["users.login LIKE :query OR profiles.name LIKE :query", { query: "%#{query}%" }])
        .references(:profile)
  end

  # Private: retrieve the administrators of this business who have the specified role
  #
  # role - only return admins with the given role. Valid values: [:owner, :billing_manager]
  #        will raise an ArgumentError if an invalid value for role is provided. nil role will
  #        return all the administrators
  #
  # Returns an ActiveRecord::Relation for the business' administrators
  def admins_with_role(role)
    return owners.or(billing_managers) unless role.present?

    case role.to_sym
    when :owner
      owners
    when :billing_manager
      billing_managers
    else
      raise ArgumentError.new("Unexpected role: #{role}")
    end
  end

  # Private: retrieves Organization ids whose login matches values in `org_logins`
  #
  # Returns an Array of Integer database ids if `org_logins` passed, otherwise nil
  def org_database_ids(org_logins)
    return unless org_logins.present?
    Organization.where(login: org_logins).pluck(:id)
  end

  # Private: builds a scope based on BusinessUserAccounts that allows filtering by the
  # * role (:admin, :member_without_admin, or :all)
  # * deployment (NONE, CLOUD, or SERVER)
  # * organization_logins
  #
  # When organization_logins are passed we're not able to query for users since we don't have
  # any organization name information from GHES to filter by. See inline comments, if you dare, for a deeper
  # explanations of quirks, reasonings, etc.
  #
  # Returns an ActiveRecord::Relation
  def enterprise_user_scope(query, role, organization_logins)
    scope = BusinessUserAccount.left_joins(:enterprise_installation_user_accounts)

    # we currently can't filter server users by organization logins. Nothing to do here if there are org logins passed.
    # return a scope for the UNION query that will effectively result in [] but is structurally compatible so
    # that Arel will do assemble the SQL for us
    return scope.none if organization_logins&.any?

    # filters out BusinessUserAccounts with no enterprise_installation_user_account for the Business
    scope = scope.where.not(enterprise_installation_user_accounts: { id: nil })

    query = ActiveRecord::Base.sanitize_sql_like(query.to_s.strip.downcase)
    return scope unless query.present? || role.present?

    # enterprise_installation_user_accounts.site_admin (Boolean) can be used to filter by Role. possible query values/explanations:
    #   true: user is a site admin on a GHE Server installation but is listed alongside Cloud _members_. they're analogous to Cloud Organization owner/admins.
    #   false: user is a member of a GHE Server installation and is analogous to a Cloud Organization member
    if role.present?
      scope = scope.where(enterprise_installation_user_accounts: { site_admin: (role == "owner") })
    end

    if query.present?
      scope = scope.where(["business_user_accounts.login LIKE :query OR enterprise_installation_user_accounts.profile_name LIKE :query", { query: "%#{query}%" }])
    end

    scope
  end

  # Private: for GHEC with a valid volume license present this will return a new ActiveRecord::Relation which filters
  #   by license type (:volume or :enterprise)
  #
  # Returns an ActiveRecord::Relation
  def filter_by_license(scope, license)
    return scope if GitHub.single_business_environment? || !volume_licensing_enabled? || license.blank?

    if GitHub.flipper[:bundled_license_assignment].enabled?(self)
      scope = scope.joins(<<~SQL)
        LEFT OUTER JOIN `bundled_license_assignments`
          ON `bundled_license_assignments`.`user_id` = `business_user_accounts`.`user_id`
            AND `bundled_license_assignments`.`business_id` = `business_user_accounts`.`business_id`
      SQL

      case license.to_sym
      when :enterprise
        scope.where(bundled_license_assignments: { id: nil })
      when :volume
        scope.where.not(bundled_license_assignments: { id: nil })
      end
    else
      relevant_license_types = [UserLicense.license_types[license]]
      relevant_license_types.push nil if license.to_sym == :enterprise # no user license is implicitly an enterprise license

      scope = scope.joins(<<~SQL)
        LEFT OUTER JOIN `user_licenses`
          ON `user_licenses`.`user_id` = `business_user_accounts`.`user_id`
            AND `user_licenses`.`business_id` = `business_user_accounts`.`business_id`
      SQL
      scope.where(user_licenses: { license_type: relevant_license_types })
    end
  end
end
