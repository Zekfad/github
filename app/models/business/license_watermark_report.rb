# frozen_string_literal: true
class Business::LicenseWatermarkReport
  DEFAULT_RANGE = 30.days
  DATE_FORMAT = "%Y-%m-%d"

  def initialize(business, range_start: nil, range_end: nil)
    @business = business
    @range_start = range_start || (Time.current - DEFAULT_RANGE).strftime(DATE_FORMAT)
    @range_end = range_end || Time.current.strftime(DATE_FORMAT)
  end

  def rows
    GitHub.presto.run_with_names(query) + GitHub.presto.run_with_names(first_day_query)
  end

  private

  attr_reader :business

  def sanitize_date(date)
    Date.strptime(date, DATE_FORMAT).strftime(DATE_FORMAT)
  end

  def range_start
    @_range_start ||= sanitize_date(@range_start)
  end

  def range_end
    @_range_end ||= sanitize_date(@range_end)
  end

  def first_day_query
    %Q(
      SELECT day as day,
              max(filled_enterprise_seats) as enterprise_watermark,
              max(filled_volume_seats) as volume_watermark
      FROM hive_hydro.hydro.github_billing_v0_license_snapshot
      WHERE day <= '#{range_start}' AND business.id = #{business.id}
      GROUP BY day
      ORDER BY day DESC
      LIMIT 1
    )
  end

  def query
    %Q(
      SELECT day as day,
              max(filled_enterprise_seats) as enterprise_watermark,
              max(filled_volume_seats) as volume_watermark
      FROM hive_hydro.hydro.github_billing_v0_license_snapshot
      WHERE day > '#{range_start}' AND day <= '#{range_end}' AND business.id = #{business.id}
      GROUP BY day
      ORDER BY day DESC
    )
  end
end
