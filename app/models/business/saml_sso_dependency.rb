# frozen_string_literal: true

module Business::SamlSsoDependency
  extend ActiveSupport::Concern

  include SamlProviderMembers

  # Public: Is this business configured to use SAML Single Sign-on for
  # its members?
  #
  # Returns a Boolean
  def saml_sso_enabled?
    return false if GitHub.single_business_environment? && !GitHub.private_instance_user_provisioning?
    saml_provider.present? && saml_provider.persisted?
  end

  # Public: Is the given user already linked to the business via the
  # current SAML identity provider?
  #
  # Returns a Boolean
  def saml_sso_requirement_met_by?(user)
    return true unless saml_sso_enabled?
    user && ExternalIdentity.linked?(
      provider: saml_provider,
      user: user,
    )
  end

  # Public: Find the object that contains the relevant SAML provider for the current
  # external identity session.
  #
  # When a business, self is returned
  #
  # Returns self
  def external_identity_session_owner
    self
  end

  # Public: Does this Enterprise Account enforce membership via a SAML identity provider?
  #
  # In GHEC, all Enterprises enforce SAML SSO if it's enabled.
  #
  # In GHPI, all Enterprises do not enforce SAML SSO if it's enabled
  # as all alternate credentials creation would require SAML signed-in user.
  #
  # Returns a Boolean
  def saml_sso_enforced?
    return false if GitHub.private_instance_user_provisioning?
    saml_sso_enabled?
  end

  # Public: Expire all active ExternalIdentitySessions associated with this enterprise
  #
  # Used to force a SAML SSO workflow on users when a change is made to enable GitHub's user
  # deprovisioning settings involving SAML SSO. The reasoning here is that users may have
  # organization memberships not specified in their IdP. Because enabling user deprovisioning
  # would remove them from unauthorized organizations we need to immediately force them to
  # authenticate so we get the most up-to-date SAML assertions mapping their org memberships.
  #
  # Returns nothing of consequence
  def expire_all_enterprise_sessions!(current_user:)
    return unless saml_sso_enabled? && saml_provider.saml_deprovisioning_enabled?

    # enqueue a job to expire all active external identity sessions for the enterprise
    ExpireEnterpriseSessionsJob.perform_later(self)

    # expire current_user's external identity sessions associated with the enterprise
    external_identity = current_user.external_identities.find_by(provider: saml_provider)
    external_identity.sessions.active.update_all(expires_at: 1.minute.ago) if external_identity
  end
end
