# frozen_string_literal: true

class Business::OrganizationMembership < ApplicationRecord::Domain::Users

  self.table_name = "business_organization_memberships"

  belongs_to :business
  belongs_to :organization

  validates :business, presence: true
  validates :organization_id, uniqueness: { message: "is already associated with an enterprise account" }
  validate  :ensure_business_has_enough_seats, on: :create

  after_create_commit :instrument_create
  after_destroy_commit :instrument_destroy

  after_create_commit :migrate_organization_to_business_billing, if: -> { GitHub.billing_enabled? }
  after_create_commit :create_business_user_accounts_for_org_members, unless: -> { GitHub.single_business_environment? }

  after_commit :sync_default_repository_permission

  before_destroy :sync_business_policies
  after_destroy_commit :business_members_cleanup, unless: -> { GitHub.single_business_environment? }
  after_destroy_commit :update_plan_subscription_customer, if: -> { GitHub.billing_enabled? }

  around_save :sync_two_factor_requirement,
              if: -> { organization_id_changed? || business_id_changed? }

  after_commit :update_private_search_on_orgs, on: [:create, :destroy]

  private

  def instrument_create
    business.instrument :add_organization, org: organization
  end

  def instrument_destroy
    business.instrument :remove_organization, org: organization
  end

  def update_private_search_on_orgs
    UpdatePrivateSearchOnEnterpriseOrgsJob.perform_later(self.business.id)
  end

  def ensure_business_has_enough_seats
    return unless business

    # Creating a Business::OrganizationMembership does not create any new
    # users, so this validation should never cause the creation to fail on
    # GitHub Enterprise where the business (instance) never needs enough seats
    # to add orgs to the global business.
    return if GitHub.single_business_environment?

    unless business.has_sufficient_licenses_for_organization?(organization)
      errors.add(:base, "Insufficient seats to add this organization")
    end
  end

  def migrate_organization_to_business_billing
    BusinessOrganizationBillingJob.perform_later(business, organization)
  end

  def create_business_user_accounts_for_org_members
    AddBusinessUserAccountsJob.perform_later(business, Organization::LicenseAttributer.new(organization).user_ids.to_a)
  end

  def update_plan_subscription_customer
    Billing::DetachOrganizationFromBusinessCustomerJob.perform_later(business, organization)
  end

  # Internal: Enqueues a job to do cleanup for users who are no longer part of the business
  #   - delete BusinessUserAccount's
  #   - unlink SAML ExternalIdentity's (SCIM-provisioned identities are preserved)
  #   - run the RemoveBizUserForksJob job to clean up forks
  #
  # Returns nothing
  def business_members_cleanup
    BusinessMembershipCleanupJob.perform_later(
      business,
      organization_id: organization.id,
    )
  end

  # Internal: Sync any default repository permission changes if the business
  # membership changes
  def sync_default_repository_permission
    organization&.sync_default_repository_permission(actor: organization)
  end

  def sync_two_factor_requirement
    organization_2fa_required = organization&.two_factor_requirement_enabled?

    yield

    # if an Organization is added to a Business that requires 2fa, enforce 2fa on the organization
    if business&.two_factor_requirement_enabled? && business&.two_factor_required_policy? &&
      (organization_2fa_required == false)
      EnforceTwoFactorRequirementOnOrganizationJob.perform_later(organization, organization)
    end
  end

  def sync_business_policies
    business.configuration_entries.where(final: true).each do |configuration_entry|
      organization.config.set(configuration_entry.name, configuration_entry.value, organization)
    end
  end
end
