# frozen_string_literal: true

module Business::LicenseDependency
  extend ActiveSupport::Concern

  include BundledLicenseAssignmentHelper
  # Public: Total number of licenses including enterprise licenses and volume
  # licenses if volume licensing is enabled.
  #
  # Returns an Integer
  def licenses
    if bundled_license_assignments_enabled?(self)
      seats.to_i
    else
      seats.to_i + volume_license_cap
    end
  end

  # Public: Returns the unique number of licensed "users" (users and emails)
  #
  # Returns an Integer.
  def consumed_licenses
    if GitHub.single_business_environment?
      ::User.count_seats_used
    elsif bundled_license_assignments_enabled?(self)
      unique_enterprise_users_count
    else
      license_attributer.unique_count
    end
  end

  # Public: Returns the unique number of volume users across all owned
  # organizations.
  #
  # Returns an Integer.
  def unique_volume_users_count
    if bundled_license_assignments_enabled?(self)
      bundled_license_assigments.nonrevoked.count
    else
      user_licenses.volume.where(email: license_attributer.emails).or(
        user_licenses.volume.where(user_id: license_attributer.user_ids)
      ).count
    end
  end

  # Public: Returns the unique number of enterprise users across all owned
  # organizations.
  #
  # Returns an Integer.
  def unique_enterprise_users_count
    if bundled_license_assignments_enabled?(self)
      return license_attributer.unique_count unless volume_licensing_enabled?
      volume_user_ids = (bundled_license_assigments.nonrevoked.assigned.pluck(:user_id)).to_set
      volume_emails = (bundled_license_assigments.nonrevoked.unassigned.pluck(:email)).to_set
      # volume_user_ids and license_attributer.user_ids are overlapping sets so do set subtraction
      user_count = (license_attributer.user_ids - volume_user_ids).count
      # volume_emails and license_attributer.emails are overlapping sets so do set subtraction
      email_count = (license_attributer.emails - volume_emails).count
      ubua_count = license_attributer.unidentified_business_user_account_ids.count
      user_count + email_count + ubua_count
    else
      [0, consumed_licenses - unique_volume_users_count].max
    end
  end

  # Public: Returns the number of available seats across all owned
  # organizations based on the _unique_ number of billable users.
  #
  # Returns an Integer.
  def available_licenses
    [0, licenses - consumed_licenses].max
  end

  def volume_licensing_enabled?
    enterprise_agreements.active.visual_studio_bundle.any?
  end

  def volume_license_cap
    # In the future we might want to support multiple enterprise agreements.
    # Currently, we only support one per business.
    ea = enterprise_agreements.active.visual_studio_bundle.first
    ea.blank? ? 0 : ea.seats
  end

  def has_sufficient_licenses_for?(user, email: nil)
    return false if user.nil? && email.nil?

    return true if user.present? && license_attributer.user_ids.include?(user.id)
    return true if email.present? && license_attributer.emails.include?(email)
    available_licenses > 0
  end

  # Returns the number of additional licenses the customer must purchase to add organization to business
  def additional_licenses_required_for_organization(organization)
    organization_license_attributer = Organization::LicenseAttributer.new(organization)

    total_licenses = (license_attributer.user_ids + organization_license_attributer.user_ids).count +
      (license_attributer.emails + organization_license_attributer.emails).count +
      license_attributer.unidentified_business_user_account_ids.count

    [total_licenses - licenses, 0].max
  end

  # Returns the number of additional licenses that will be consumed by adding organization to business
  def additional_licenses_consumed_by_organization(organization)
    organization_license_attributer = Organization::LicenseAttributer.new(organization)

    new_user_ids = organization_license_attributer.user_ids - license_attributer.user_ids
    new_emails = organization_license_attributer.emails - license_attributer.emails

    new_user_ids.count + new_emails.count
  end

  def has_sufficient_licenses_for_organization?(organization)
    additional_licenses_required_for_organization(organization).zero?
  end

  # Returns the number of additional licenses the customer must purchase to add repository to business
  def additional_licenses_required_for_repository(repository)
    repository_user_ids = repository.member_ids + repository.repository_invitations.pluck(:invitee_id)
    needed_seats = (repository_user_ids - license_attributer.user_ids.to_a).size
    [needed_seats - available_licenses, 0].max
  end

  def license_attributer
    @license_attributer ||= Business::LicenseAttributer.new(self)
  end
end
