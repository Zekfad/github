# frozen_string_literal: true

module Business::UserLicensesExtension
  def ensure_exists(identifying_attributes, other_attributes = Hash.new)
    retry_on_find_or_create_error do
      find_by(identifying_attributes) ||
        create(identifying_attributes.merge(other_attributes).reverse_merge(license_type: :enterprise))
    end
  end
end
