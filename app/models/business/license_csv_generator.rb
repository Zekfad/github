# frozen_string_literal: true

class Business::LicenseCsvGenerator
  include BundledLicenseAssignmentHelper

  FIELDS = %w(
    name
    handle_or_email
    profile_link
    license_type
  ).freeze

  HEADERS = FIELDS.map(&:humanize).freeze

  ENTERPRISE_LICENSE_TYPE = "Enterprise"
  VOLUME_LICENSE_TYPE = "Enterprise with Visual Studio Subscription"

  def initialize(business)
    @business = business
    @attributer = Business::LicenseAttributer.new(business)
    @urls = ViewModel::URLs.new
  end

  def generate
    CSV.generate do |csv|
      csv_rows.each do |row|
        csv << row
      end
    end
  end

  def csv_rows
    [HEADERS] + user_rows + email_rows + unidentified_business_user_account_rows
  end

  private

  attr_reader :business, :attributer, :urls

  def user_rows
    return @_user_rows if defined?(@_user_rows)

    @_user_rows = attributer.user_ids.each_slice(1000).flat_map do |user_ids|
      User.left_outer_joins(:profile).where(id: user_ids).pluck(:id, :login, "profiles.name").map do |(id, login, name)|
        [name, login, urls.user_url(login), license_type_for_user_id(id)]
      end
    end
  end

  def email_rows
    return @_email_rows if defined?(@_email_rows)
    @_email_rows = attributer.emails.each_with_object([]) do |email, rows|
      rows << ["", email, "", license_type_for_email(email)]
    end
  end

  def unidentified_business_user_account_rows
    # unidentified_business_user_account_rows abbrv. ubua_rows
    return @_ubua_rows if defined?(@_ubua_rows)
    user_account_ids = attributer.unidentified_business_user_account_ids
    @_ubua_rows = BusinessUserAccount.includes(enterprise_installation_user_accounts: [:enterprise_installation]).
      where(id: user_account_ids).find_in_batches.each_with_object([]) do |group, rows|
      group.each do |user_account|
        # These ubua's should only have one enterprise installation user account
        # Safe traversal incase of bad data
        enterprise_installation_user_account = user_account&.enterprise_installation_user_accounts&.first
        enterprise_installation = enterprise_installation_user_account.enterprise_installation
        name = "GHES user"
        name += " from #{enterprise_installation.host_name}" if enterprise_installation
        rows << [name, enterprise_installation_user_account.login, "", ENTERPRISE_LICENSE_TYPE]
      end
    end
  end

  def license_type_for_email(email)
    volume_licensed_emails.include?(email) ? VOLUME_LICENSE_TYPE : ENTERPRISE_LICENSE_TYPE
  end

  def volume_licensed_emails
    # Use sets to get O(1) lookups
    if bundled_license_assignments_enabled?(business)
      @_volume_licensed_emails ||= business.bundled_license_assigments.nonrevoked.unassigned.pluck(:email).to_set
    else
      @_volume_licensed_emails ||= business.user_licenses.volume.where.not(email: nil).pluck(:email).to_set
    end
  end

  def license_type_for_user_id(user_id)
    volume_licensed_user_ids.include?(user_id) ? VOLUME_LICENSE_TYPE : ENTERPRISE_LICENSE_TYPE
  end

  def volume_licensed_user_ids
    # Use sets to get O(1) lookups
    if bundled_license_assignments_enabled?(business)
      @_volume_licensed_user_ids ||= business.bundled_license_assigments.nonrevoked.assigned.pluck(:user_id).to_set
    else
      @_volume_licensed_user_ids ||= business.user_licenses.volume.where.not(user_id: nil).pluck(:user_id).to_set
    end
  end
end
