# frozen_string_literal: true

class Business::OrganizationPermission
  def initialize(business, current_user)
    @business = business
    @current_user = current_user
  end

  def can_create_organization?
    # Creating a new organization will create a new license for the current user
    # if they are not already consuming a license
    business.has_sufficient_licenses_for?(current_user)
  end

  private

  attr_reader :business, :current_user
end
