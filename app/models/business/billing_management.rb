# rubocop:disable Style/FrozenStringLiteralComment

class Business::BillingManagement
  include Ability::Subject
  include Ability::Membership

  def initialize(business)
    @business = business
  end

  # Add a billing manager.
  #
  # user        - The User to add as a billing manager
  # actor       - The User adding the billing manager
  # send_notification - Whether or not the user should be sent a notification
  #                     about being added to the business (default: false).
  #
  # Returns nothing.
  def add_manager(user, actor:, send_notification: false)
    grant user, :write
    @business.instrument :add_billing_manager, user: user, actor: actor
    @business.add_user_accounts([user.id]) unless GitHub.single_business_environment?
    GitHub.dogstats.increment("billing.managers.count", tags: ["action:add"])

    @business.send_admin_added_email_notification(role: :billing_manager,
                                                  admin: user) if send_notification
  end

  # Remove a billing manager.
  #
  # user    - A User, who's being removed as a billing manager of the business
  # actor   - The User revoking the privilege.
  # reason  - (Optional) A String stating why the privilege was revoked.
  # send_notification - Whether or not the user should be sent a notification
  #                     about being removed from the business. (default: true)
  #
  # Returns nothing.
  def remove_manager(user, actor:, reason: nil, send_notification: true)
    return unless manager?(user)
    @business.cancel_all_invitations_involving(user)
    revoke user
    @business.remove_user_from_business(user)
    @business.instrument :remove_billing_manager,
      user: user,
      actor: actor,
      reason: reason
    GitHub.dogstats.increment("billing.managers.count", tags: ["action:remove"])

    @business.send_admin_removed_email_notification(role: :billing_manager,
                                                    admin: user,
                                                    reason: reason&.to_s) if send_notification
  end

  def ability_id
    @business.id
  end

  # Returns true if the given user is a billing manager of this business.
  def manager?(user)
    permit?(user, :write)
  end

  # Internal: remove all billing managers, used when destroying a business
  def remove_all_managers
    members.each do |manager|
      remove_manager manager, actor: nil, send_notification: false
    end
  end

  # Internal: See Ability::Subject#grant?
  def grant?(actor, action)
    @business.grant?(actor, action)
  end
end
