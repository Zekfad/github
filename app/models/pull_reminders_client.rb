# frozen_string_literal: true

class PullRemindersClient
  def self.ready_to_migrate(organization)
    new(organization).mark_as_ready_to_migrate
  end

  Error = Class.new(StandardError)

  attr_reader :connection, :host, :organization_id

  def initialize(organization)
    @organization_id = organization.id
    @connection = Faraday.new(
      url: integration.url,
      headers: {
        "Content-Type" => "application/json",
        "Authorization" => "Bearer #{encode_jwt}",
      },
    )
  end

  def mark_as_ready_to_migrate
    parse_response(connection.patch("/migration/#{organization_id}"))
  end

  def send_personal_reminder_failure(id, reason: nil, tip: nil)
    body = {
      reason: reason,
      tip: tip,
    }

    response = connection.post("/migration/#{organization_id}/user_notification_rules/#{id}/failure", body.to_json)
    parse_response(response)
  end

  def send_team_reminder_failure(id, reason: nil, tip: nil)
    body = {
      reason: reason,
      tip: tip,
    }

    response = connection.post("/migration/#{organization_id}/notification_rules/#{id}/failure", body.to_json)
    parse_response(response)
  end

  def team_reminders(slack_channel_id = nil)
    params = { slack_channel_id: slack_channel_id } if slack_channel_id

    response = connection.get("/migration/#{organization_id}/notification_rules", params)
    parse_response(response)
  end

  def personal_reminder(github_user_id = nil)
    response = connection.get("/migration/#{organization_id}/user_notification_rules/#{github_user_id}")
    parse_response(response)
  end

  def mark_team_reminder_as_migrated(id, migrated_url:)
    response = connection.post(
      "/migration/#{organization_id}/notification_rules/#{id}/migrated",
      { migrated_url: migrated_url }.to_json,
    )

    parse_response(response)
  end

  def mark_personal_reminder_as_migrated(github_user_id, migrated_url:)
    response = connection.post(
      "/migration/#{organization_id}/user_notification_rules/#{github_user_id}/migrated",
      { migrated_url: migrated_url }.to_json,
    )

    parse_response(response)
  end

  private

  def parse_response(response)
    raise request_error(response) unless response.success?

    if response.body.empty?
      true
    else
      parse_json(response.body)
    end
  end

  def request_error(response)
    request_information = [
      response.env.method.upcase,
      response.env.url.path,
      response.status,
      response.reason_phrase,
    ]

    Error.new("Request failed: #{request_information.join(" ")}")
  end

  def parse_json(json_string)
    JSON.parse(json_string)
  rescue JSON::ParserError
    raise Error.new("Response body contains invalid JSON")
  end

  def encode_jwt
    issued_at = Time.now
    expires_at = issued_at + 10.minutes
    payload = {
      org_id: organization_id,
      iat: issued_at.to_i,
      exp: expires_at.to_i,
    }

    JWT.encode(payload, integration.secret, "HS256")
  end

  def integration
    @integration ||= Apps::Internal.integration(:pull_panda) || raise("Pull Panda integration not found")
  end
end
