# frozen_string_literal: true

module TradeControls
  def self.table_name_prefix
    "trade_controls_"
  end
end
