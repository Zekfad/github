# frozen_string_literal: true

class CommunityContributor
  attr_reader :direct_dependency_ids, :transitive_dependency_ids, :viewer

  def initialize(viewer:, direct_dependency_ids:, transitive_dependency_ids:)
    @direct_dependency_ids     = direct_dependency_ids
    @transitive_dependency_ids = transitive_dependency_ids
    @viewer                    = viewer
  end
end
