# frozen_string_literal: true

class MemexContentSuggester

  SUGGESTED_REPOSITORIES_LIMIT = 250
  SUGGESTED_ISSUES_AND_PULLS_LIMIT = 500
  SUGGESTED_OPEN_ISSUES_AND_PULLS_LIMIT = 400
  SUGGESTED_CLOSED_ISSUES_AND_PULLS_LIMIT = 100

  attr_reader :organization, :viewer

  def initialize(viewer:, organization:)
    @viewer = viewer
    @organization = organization
  end

  # Builds a list of repository suggestions from those that the user has recently
  # interacted with, followed by those that have been updated recently.
  def repositories
    interactions = recent_interactions
      .uniq(&:repository_id)
      .first(SUGGESTED_REPOSITORIES_LIMIT)
    interactions_by_repository_id = interactions.index_by(&:repository_id)

    recent_repositories = interactions.map do |action|
      interactable = action.interactable

      # Repositories have already been loaded on each interactable's issue, but not necessarily each
      # interactable directly, so to make this map efficient retrieve the interactable's issue.
      issue = interactable.is_a?(PullRequest) ? interactable.issue : interactable

      issue.repository
    end

    other_repositories = []
    if (other_repositories_limit = SUGGESTED_REPOSITORIES_LIMIT - recent_repositories.length) > 0
      other_repositories = organization.visible_repositories_for(viewer)
        .includes(:owner)
        .not_archived_scope
        .where
        .not(id: recent_repositories.map(&:id))
        .recently_updated
        .limit(other_repositories_limit)
    end

    suggestions = recent_repositories.map do |repo|
      repo.memex_suggestion_hash(
        last_interaction_at: interactions_by_repository_id[repo.id].occurred_at
      )
    end
    suggestions += other_repositories.map(&:memex_suggestion_hash)
  end

  # Builds a list of issue and pull request suggestions for a particular
  # repository from those that the have been recently updated in that
  # repository.
  #
  # Additionally:
  #   - Reorder results for maximum relevance based on issues/pulls that the
  #     current user in particular has interacted with.
  #   - If `memex` is provided, filter out issues or pull requests that already
  #     appear in the memex.
  #
  # repository - Repository
  # memex - MemexProject
  def issues_and_prs_for_repository(repository, memex: nil)
    memex_issue_ids = []
    memex_pr_ids = []

    if memex
      memex
        .memex_project_items
        .where(content_type: [Issue.name, PullRequest.name])
        .limit(MemexProjectItem::PER_PAGE_LIMIT)
        .pluck(:content_type, :content_id)
        .each do |content_type, content_id|
          memex_issue_ids << content_id if content_type == Issue.name
          memex_pr_ids << content_id if content_type == PullRequest.name
        end
    end

    recent_issues =
      (
        recent_issues_by_state(
          repository: repository,
          state: :open,
          limit: SUGGESTED_OPEN_ISSUES_AND_PULLS_LIMIT,
          issue_ids_to_exclude: memex_issue_ids,
          pr_ids_to_exclude: memex_pr_ids,
        ) +
        recent_issues_by_state(
          repository: repository,
          state: :closed,
          limit: SUGGESTED_CLOSED_ISSUES_AND_PULLS_LIMIT,
          issue_ids_to_exclude: memex_issue_ids,
          pr_ids_to_exclude: memex_pr_ids,
        )
      ).sort_by { |i| -i.updated_at.to_i }

    interactions_by_type_and_id = recent_interactions.index_by do |i|
      [i.interactable.class.name, i.interactable.id]
    end

    suggestions = sort_issues_by_interactions(
      recent_issues,
      recent_interactions.map(&:interactable)
    ).map do |obj|
        issue = obj
        suggested_type = (issue.pull_request_id ? PullRequest : Issue).name
        suggested_type_id = issue.pull_request_id ? issue.pull_request_id : issue.id

        issue.memex_suggestion_hash(
          last_interaction_at: interactions_by_type_and_id[[suggested_type, suggested_type_id]]&.occurred_at,
          as_pull: !!obj.pull_request_id
        )
    end
  end

  private

  def recent_interactions
    @recent_interactions ||= Issue::RecentInteractions
      .new(
        viewer,
        since: 2.weeks.ago,
        organization_id: organization.id,
      )
      .fetch(limit: 20)
  end

  def recent_issues_by_state(repository:, state:, limit:, issue_ids_to_exclude:, pr_ids_to_exclude:)
    scope = repository
      .issues
      .not_spammy
      .order(updated_at: :desc)
      .limit(limit)

    scope = scope.select(:id, :number, :state, :title, :pull_request_id, :updated_at)

    scope = scope.where.not(id: issue_ids_to_exclude) if issue_ids_to_exclude.any?
    if pr_ids_to_exclude.any?
      scope = scope.where(
        "pull_request_id IS NULL OR pull_request_id NOT IN (?)",
        pr_ids_to_exclude
      )
    end
    scope = scope.where(state: state.to_s) if state

    scope.all
  end

  def sort_issues_by_interactions(issues, interactions)
    rank_by_id = interactions.map { |action| [action.class.name, action.id] }.each_with_index.to_h

    sorted_issues_with_indexes = issues.each_with_index.sort_by do |issue, index|
      type = (issue.pull_request_id ? PullRequest : Issue).name
      id = issue.pull_request_id ? issue.pull_request_id : issue.id
      rank_by_id.fetch([type, id], interactions.length + index)
    end

    sorted_issues_with_indexes.map(&:first)
  end

end
