# frozen_string_literal: true

require "docusign_esign"

module Sponsors
  module Docusign
    class ReassignEnvelope
      class TemplateError < StandardError; end

      def self.call(sponsorable:, envelope:, template_id:, reason:)
        new(
          sponsorable: sponsorable,
          envelope: envelope,
          template_id: template_id,
          reason: reason,
        ).call
      end

      def initialize(sponsorable:, envelope: nil, template_id:, reason:)
        @sponsorable = sponsorable
        @envelope = envelope
        @template_id = template_id
        @reason = reason
      end

      def call
        unless template_valid?
          raise ::Sponsors::Docusign::TemplateError.new(
            "The template ID provided is not valid",
          )
        end

        if envelope&.voidable?
          ::Sponsors::Docusign::VoidEnvelope.call(
            envelope: envelope,
            reason: reason,
          )
        end

        ::Sponsors::Docusign::CreateEnvelope.call(
          sponsorable: sponsorable,
          template_id: template_id,
        )
      end

      private

      attr_reader :sponsorable, :envelope, :template_id, :reason

      def template_valid?
        [
          ::Sponsors::Docusign::Document::W9.template_id,
          ::Sponsors::Docusign::Document::W_8BEN.template_id,
          ::Sponsors::Docusign::Document::W_8BEN_E.template_id,
        ].include?(template_id)
      end
    end
  end
end
