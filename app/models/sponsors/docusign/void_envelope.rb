# frozen_string_literal: true

require "docusign_esign"

module Sponsors
  module Docusign
    class VoidEnvelope
      class InvalidEnvelopeError < StandardError
        def message
          "Invalid DocuSign envelope"
        end
      end

      def self.call(envelope:, reason:)
        new(
          envelope: envelope,
          reason: reason,
        ).call
      end

      def initialize(envelope:, reason:)
        @envelope = envelope
        @reason = reason
      end

      def call
        void_envelope!

        GitHub.dogstats.increment("docusign",
          tags: [
            "source:sponsors",
            "action:void_envelope",
          ])
      end

      private

      attr_reader :envelope, :reason

      def void_envelope!
        ::Docusign::VoidEnvelope.call \
          envelope_id: envelope_id,
          reason: reason

        DocusignEnvelope
          .find_by(envelope_id: envelope_id)
          .update!(
            active: false,
            status: :voided,
            voided_reason: reason,
          )
      end

      def envelope_id
        envelope.envelope_id
      end
    end
  end
end
