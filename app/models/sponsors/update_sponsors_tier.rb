# frozen_string_literal: true

module Sponsors

  # Public: A Plain Old Ruby Object (PORO) used for updating an existing Sponsors tier.
  class UpdateSponsorsTier
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    include ActionView::Helpers::NumberHelper

    # inputs - Hash containing attributes to update a sponsors tier
    # inputs[:tier] - The sponsors tier to update.
    # inputs[:description] - A short description of the sponsorship tier.
    # inputs[:monthly_price_in_cents] - How much this sponsorship tier should cost per month in cents.
    # inputs[:yearly_price_in_cents] - How much this sponsorship tier should cost annually in cents.
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(tier:, description:, monthly_price_in_cents:, yearly_price_in_cents:, viewer:)
      @tier = tier
      @description = description
      @monthly_price_in_cents = monthly_price_in_cents
      @yearly_price_in_cents = yearly_price_in_cents
      @viewer = viewer
    end

    def call
      previous_tier_description = tier.description
      updated_tier = update_tier
      instrument_tier_description_change(previous_tier_description: previous_tier_description)
      updated_tier
    end

    private

    attr_reader :tier, :description, :monthly_price_in_cents, \
                :yearly_price_in_cents, :viewer

    def update_tier
      unless tier.editable_by?(viewer)
        raise ForbiddenError.new("#{viewer} does not have permission to change the Sponsors tier.")
      end

      tier.name = name
      tier.description = description
      tier.monthly_price_in_cents = monthly_price_in_cents
      tier.yearly_price_in_cents = yearly_price_in_cents

      if tier.save
        tier
      else
        errors = tier.errors.full_messages.join(", ")
        raise UnprocessableError.new("Could not update Sponsors tier: #{errors}")
      end
    end

    def instrument_tier_description_change(previous_tier_description:)
      return unless should_instrument_tier_description_change?(previous_tier_description)

      GitHub.instrument("sponsors.sponsored_developer_tier_description_update", {
        user: viewer,
      })
      GlobalInstrumenter.instrument("sponsors.sponsored_developer_tier_description_update", {
        actor: viewer,
        listing: tier.sponsors_listing,
        tier: tier,
        current_tier_description: tier.description,
        previous_tier_description: previous_tier_description,
        sponsors_count_on_tier: sponsors_count_on_tier,
      })
    end

    def should_instrument_tier_description_change?(previous_tier_description)
      tier.published? &&
        tier.sponsors_listing.approved? &&
        (tier.description != previous_tier_description)
    end

    def sponsors_count_on_tier
      tier.subscription_items.active.count
    end

    def name
      "#{number_to_currency(monthly_price_in_cents / 100, precision: 0)} a month"
    end
  end
end
