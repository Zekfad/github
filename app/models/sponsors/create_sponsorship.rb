# frozen_string_literal: true

# Public: A Plain Old Ruby Object (PORO) used for creating a sponsorship
module Sponsors
  class CreateSponsorship
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    SPONSORSHIP_EMAIL_WAIT_TIME = 5.minutes

    # inputs - Hash containing attributes to create a sponsorship
    # inputs[:tier] - The listing plan/sponsors tier that was purchased.
    # inputs[:sponsor] - The user who is sponsoring
    # inputs[:sponsorable] - The user being sponsored
    # inputs[:quantity] - How many units of this listing were purchased.
    # inputs[:privacy_level] - public or private
    # inputs[:email_opt_in] - True if the user has opted in to sponsorship updates
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(tier:, sponsor:, sponsorable:, quantity:, viewer:, privacy_level:, email_opt_in:)
      @tier       = tier
      @sponsor    = sponsor
      @sponsorable = sponsorable
      @quantity   = quantity.to_i
      @viewer     = viewer
      @privacy_level = privacy_level
      @email_opt_in = email_opt_in
    end

    def call
      ActiveRecord::Base.transaction do
        # This is validated at the model level too but we want to provide a better
        # user-visible message that doesn't use the word "block".
        block_exception_message = "You can't perform that action at this time."
        raise ForbiddenError.new(block_exception_message) if sponsor.blocked_by?(sponsorable)
        raise ForbiddenError.new(block_exception_message) if sponsorable.blocked_by?(sponsor)

        # Can't sponsor without a verified email
        if viewer.no_verified_emails?
          raise ForbiddenError.new("You need a verified email address in order to sponsor anyone.")
        end

        unless viewer && tier.published? && tier.listing.listable == sponsorable
          raise ForbiddenError.new("Could not create sponsorship")
        end

        result = Billing::CreateSubscriptionItem.call(
          subscribable: tier,
          quantity: quantity,
          account: sponsor,
          viewer: viewer,
        )
        subscription_item = result[:subscription_item]

        sponsorship = Sponsorship.find_by(sponsor: sponsor, sponsorable: sponsorable) ||
          Sponsorship.new(sponsor: sponsor, sponsorable: sponsorable)

        sponsorship.active = true
        sponsorship.subscribable = tier
        sponsorship.privacy_level = privacy_level
        sponsorship.is_sponsor_opted_in_to_email = email_opt_in
        sponsorship.subscription_item = subscription_item

        unless sponsorship.save
          errors = sponsorship.errors.full_messages.join(", ")
          raise UnprocessableError.new("Could not create sponsorship: #{errors}")
        end

        if listing = sponsorable.sponsors_listing
          unless listing.payout_probation_started_at
            listing.update(payout_probation_started_at: Time.zone.now)
          end
        end

        # TODO: Come back to this when we have hydro set up for SponsorsListing/Tier
        # It's dependent on our de-coupling efforts: https://github.com/github/sponsors/issues/353
        instrument_creation(sponsorship: sponsorship)
        begin
          SendSponsorshipEmailsJob.set(wait: SPONSORSHIP_EMAIL_WAIT_TIME).perform_later(
            sponsorship: sponsorship,
            actor: viewer,
          )
        rescue ArgumentError
          # TODO: temporarily work around `A copy of
          # ActiveJob::QueueAdapters::BufferedResqueAdapter has been removed
          # from the module tree but is still active!` in development.
          # This should be replaced with a better fix when possible.
          raise unless Rails.env.development?
        end
      end
    end

    private

    def instrument_creation(sponsorship:)
      GitHub.instrument("sponsors.sponsor_sponsorship_create", {
        user: sponsorship.sponsorable,
        actor: viewer,
        sponsorship: sponsorship,
        current_tier_id: sponsorship.tier.id,
      })

      matchable = tier.sponsors_listing.matchable? && sponsor.eligible_for_sponsorship_match?(sponsorable: sponsorable)

      GlobalInstrumenter.instrument("sponsors.sponsor_sponsorship_create", {
        actor: viewer,
        sponsorship: sponsorship,
        listing: sponsorship.subscription_item.subscribable.sponsors_listing,
        tier: sponsorship.subscription_item.subscribable,
        matchable: matchable,
        action: :CREATE,
        goal: sponsorable.sponsors_listing.active_goal,
      })
    end

    attr_reader :tier, :sponsor, :sponsorable, :quantity, :viewer, :privacy_level, :email_opt_in
  end
end
