# frozen_string_literal: true

module Sponsors

  # Public: A Plain Old Ruby Object (PORO) used for approving an existing Sponsors listing.
  class ApproveSponsorsListing
    # inputs - Hash containing attributes to approve a Sponsors listing.
    # inputs[:listing] - The SponsorsListing to approve.
    # inputs[:actor] - The User actor that is approving this listing.
    # inputs[:automated] - (optional) A Boolean indicating if this is an
    #                      automated acceptance, defaulting to false.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(sponsors_listing:, actor:, automated: false)
      @sponsors_listing = sponsors_listing
      @actor            = actor
      @automated        = automated
    end

    def call
      if !automated && !actor.can_admin_sponsors_listings?
        error_message = "#{actor.login} does not have permission to approve the listing."

        return Result.failure(
          sponsors_listing: sponsors_listing,
          errors: [error_message],
          platform_error: Platform::Errors::Forbidden,
        )
      end

      if sponsors_listing.approved?
        return Result.success(sponsors_listing: sponsors_listing)
      end

      if automated && !sponsors_listing.auto_approvable?
        error_message = "Listing is not auto approvable."

        return Result.failure(
          sponsors_listing: sponsors_listing,
          errors: [error_message],
          platform_error: Platform::Errors::Unprocessable,
        )
      end

      unless sponsors_listing.can_approve?
        error_message = "Listing cannot be approved."

        return Result.failure(
          sponsors_listing: sponsors_listing,
          errors: [error_message],
          platform_error: Platform::Errors::Unprocessable,
        )
      end

      approve_sponsors_listing
    end

    private

    attr_accessor :sponsors_listing, :actor, :automated

    def approve_sponsors_listing
      if sponsors_listing.approve!(actor)
        instrument_approval
        Result.success(sponsors_listing: sponsors_listing)
      else
        error_message = sponsors_listing.halted_because

        Result.failure(
          sponsors_listing: sponsors_listing,
          errors: [error_message],
          platform_error: Platform::Errors::Unprocessable,
        )
      end
    end

    def instrument_approval
      sponsorable = sponsors_listing.sponsorable
      context = {
        user: sponsorable,
        automated: automated,
      }.merge(GitHub.guarded_audit_log_staff_actor_entry(actor))

      GitHub.instrument("sponsors.sponsored_developer_approve", context)
      GlobalInstrumenter.instrument("sponsors.sponsored_developer_approve", {
        action: "APPROVED",
        user: sponsorable,
      })
    end

    class Result
      attr_reader :sponsors_listing, :success, :errors, :platform_error
      alias_method :success?, :success

      # sponsors_listing - The SponsorsListing that is being approved.
      # success - A Boolean indicating if approving the listing was successful.
      # errors - An Array of any errors that occured when approving the listing.
      # platform_error - The Platform::Errors error that should be rasied if there are any errors.
      def initialize(sponsors_listing:, success:, errors:, platform_error:)
        @sponsors_listing = sponsors_listing
        @success = success
        @errors = errors
        @platform_error = platform_error
      end

      def self.success(sponsors_listing:)
        new(
          sponsors_listing: sponsors_listing,
          success: true,
          errors: [],
          platform_error: nil,
        )
      end

      def self.failure(sponsors_listing:, errors:, platform_error:)
        new(
          sponsors_listing: sponsors_listing,
          success: false,
          errors: errors,
          platform_error: platform_error,
        )
      end
    end
  end
end
