# frozen_string_literal: true

module Sponsors
  class SponsorshipCanceledSurvey < SponsorSurvey
    validate :must_answer_canceled_reason_question

    def canceled_reason_question
      @canceled_reason_question ||= survey.questions.
        find_by(short_text: Sponsors::PostSponsorshipCancelationSurvey::CANCELED_REASON_SHORT_TEXT)
    end

    private

    alias_method :answers, :submitted_answers

    def slug
      Sponsors::PostSponsorshipCancelationSurvey::SLUG
    end

    def instrument_submitted(survey_group)
      GlobalInstrumenter.instrument("sponsors.sponsorship_canceled_survey_submitted", {
        actor: sponsor,
        sponsorable: sponsorable,
        survey_id: survey_group.survey_id,
        survey_group_id: survey_group.id,
        total_accounts_sponsoring: total_accounts_sponsoring,
      })
    end

    def must_answer_canceled_reason_question
      canceled_reason_answers = submitted_answers.
        select { |answer| answer[:question_id].to_i == canceled_reason_question.id }

      return if canceled_reason_answers.any?
      errors.add(:base, "“#{canceled_reason_question.text}” was not answered")
    end
  end
end
