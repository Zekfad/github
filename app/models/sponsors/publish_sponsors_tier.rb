# frozen_string_literal: true

module Sponsors

  # Public: A Plain Old Ruby Object (PORO) used for publishing an existing Sponsors tier.
  class PublishSponsorsTier
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    # inputs - Hash containing attributes to publish a sponsors tier
    # inputs[:tier] - The sponsorship tier to publish.
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(tier:, viewer:)
      @tier = tier
      @viewer = viewer
    end

    def call
      unless tier.adminable_by?(viewer)
        raise ForbiddenError.new("#{viewer} does not have permission to change the tier.")
      end

      if tier.draft?
        if tier.can_publish?
          tier.publish!
        else
          error_messages = [].tap do |messages|
            if sponsors_listing.reached_maximum_tier_count?
              messages << "the listing has reached the limit of published tiers"
            end

            if !sponsors_listing.published_tiers_allowed?
              messages << "the listing must be verified to have published tiers"
            end
          end

          raise UnprocessableError.new("This tier cannot be published: #{error_messages.join(", ")}")
        end
      elsif tier.published?
        raise UnprocessableError.new("This tier has already been published.")
      elsif tier.retired?
        raise UnprocessableError.new("This tier has been retired.")
      end

      tier
    end

    private

    attr_accessor :tier, :viewer

    def sponsors_listing
      tier.sponsors_listing
    end
  end
end
