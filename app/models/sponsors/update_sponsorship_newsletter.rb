# frozen_string_literal: true

# Public: A Plain Old Ruby Object (PORO) used for creating a sponsorship newsletter
module Sponsors
  class UpdateSponsorshipNewsletter
    class UnprocessableError < StandardError; end
    class ValidationError < StandardError; end

    class Result
      attr_reader :success, :newsletter, :error
      alias_method :success?, :success

      def initialize(newsletter:, success:, error:)
        @success = success
        @newsletter = newsletter
        @error = error
      end

      def self.success(newsletter)
        new(
          error: nil,
          success: true,
          newsletter: newsletter,
        )
      end

      def self.failure(error)
        new(
          error: error,
          success: false,
          newsletter: nil,
        )
      end
    end

    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(newsletter:, draft: false, body:, subject:, tier_ids: [])
      @newsletter = newsletter
      @draft = draft
      @body = body
      @subject = subject
      @tier_ids = tier_ids
    end

    def call
      if newsletter.published?
        error = ValidationError.new("You can't update a published sponsorship update.")
        return Result.failure(error)
      end

      newsletter.subject = subject
      newsletter.body = body
      newsletter.state = draft ? :draft : :published

      if tiers = sponsors_listing.sponsors_tiers.where(id: tier_ids)
        begin
          newsletter.sponsors_tiers = tiers
        rescue ActiveRecord::RecordInvalid => error
          error = UnprocessableError.new("Could not update sponsorship update: #{error.message}")
          return Result.failure(error)
        end
      end

      unless newsletter.save
        errors = update.errors.full_messages.join(", ")
        error = UnprocessableError.new("Could not update sponsorship update: #{errors}")
        return Result.failure(error)
      end

      Result.success(newsletter)
    end

    private

    attr_reader :newsletter, :draft, :body, :subject, :tier_ids

    def sponsors_listing
      newsletter.sponsorable.sponsors_listing
    end
  end
end
