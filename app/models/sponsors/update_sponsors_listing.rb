# frozen_string_literal: true

module Sponsors

  # Public: A Plain Old Ruby Object (PORO) used for updating an existing Sponsors listing.
  class UpdateSponsorsListing
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    # inputs - Hash containing attributes to update a Sponsors listing
    # inputs[:slug] - The slug of the listing (SponsorsListing) to update.
    # inputs[:short_description] - (optional) The listing's short description.
    # inputs[:full_description] - (optional) The listing's full description.
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(slug:, short_description: nil, full_description: nil, viewer:)
      @sponsors_listing = SponsorsListing.find_by_slug!(slug)
      @short_description = short_description
      @full_description = full_description
      @viewer = viewer
    end

    def call
      update_sponsors_listing
    end

    private

    attr_accessor :sponsors_listing, :short_description, :full_description, :viewer

    def update_sponsors_listing
      unless sponsors_listing.editable_by?(viewer)
        raise ForbiddenError.new("#{viewer} does not have permission to update the sponsors listing.")
      end

      sponsors_listing.short_description = short_description if short_description
      sponsors_listing.full_description = full_description if full_description

      if sponsors_listing.save
        sponsors_listing
      else
        errors = sponsors_listing.errors.full_messages.join(", ")
        raise UnprocessableError.new("Could not update the sponsors listing: #{errors}")
      end
    end
  end
end
