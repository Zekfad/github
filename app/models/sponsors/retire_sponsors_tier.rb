# frozen_string_literal: true

module Sponsors

  # Public: A Plain Old Ruby Object (PORO) used for retiring an existing Sponsors tier.
  class RetireSponsorsTier
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    # inputs - Hash containing attributes to retire a sponsors tier
    # inputs[:tier] - The sponsorship tier to retire.
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(tier:, viewer:)
      @tier = tier
      @viewer = viewer
    end

    def call
      if tier.retired?
        raise UnprocessableError.new("Can't change the state of a retired tier.")
      elsif !tier.editable_by?(viewer)
        raise ForbiddenError.new("#{viewer} does not have permission to change the tier.")
      end

      begin
        tier.retire!
        tier
      rescue SponsorsTier::RetirementNotAllowed => e
        raise UnprocessableError.new(e.message)
      end
    end

    private

    attr_accessor :tier, :viewer
  end
end
