# frozen_string_literal: true

# Public: A Plain Old Ruby Object (PORO) used for updating a sponsorship
module Sponsors
  class UpdateSponsorship
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    # inputs - Hash containing attributes to update a sponsorship
    # inputs[:tier] - The Sponsors tier that was purchased.
    # inputs[:sponsor] - The user who is sponsoring
    # inputs[:sponsorable] - The user being sponsored
    # inputs[:quantity] - How many units of this listing were purchased.
    # inputs[:privacy_level] - public or private
    # inputs[:email_opt_in] - True if the user has opted in to sponsorship updates
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(tier:, sponsor:, sponsorable:, quantity:, viewer:, privacy_level:, email_opt_in:)
      @tier     = tier
      @sponsor  = sponsor
      @sponsorable = sponsorable
      @quantity = quantity.to_i
      @viewer   = viewer
      @privacy_level = privacy_level
      @email_opt_in = email_opt_in
    end

    def call
      ActiveRecord::Base.transaction do
        unless viewer && tier.published? && tier.listing.listable == sponsorable
          raise ForbiddenError.new("Could not update sponsorship")
        end

        previous_sponsorship = sponsor.sponsorship_as_sponsor_for(sponsorable)
        previous_tier = previous_sponsorship&.subscription_item&.subscribable

        result = Billing::UpdateSubscriptionItem.call(
          subscribable: tier,
          quantity: quantity,
          account: sponsor,
          viewer: viewer,
        )
        new_subscription_item = result[:subscription_item]

        sponsorship = Sponsorship.find_by(sponsor: sponsor, sponsorable: sponsorable) ||
          Sponsorship.new(sponsor: sponsor, sponsorable: sponsorable)

        sponsorship.privacy_level = privacy_level
        sponsorship.is_sponsor_opted_in_to_email = email_opt_in
        sponsorship.subscription_item = new_subscription_item

        unless sponsorship.save
          errors = sponsorship.errors.full_messages.join(", ")
          raise UnprocessableError.new("Could not update sponsorship: #{errors}")
        end

        instrument_events(previous_sponsorship: previous_sponsorship,
                          previous_tier: previous_tier,
                          current_sponsorship: sponsorship,
                          current_tier: sponsorship.subscription_item.subscribable)
      end
    end

    private

    def instrument_events(previous_sponsorship:, previous_tier:, current_sponsorship:, current_tier:)
      privacy_level_changed = current_sponsorship.privacy_level != previous_sponsorship.privacy_level
      sponsorship_changed = privacy_level_changed ||
        (current_sponsorship.is_sponsor_opted_in_to_email != previous_sponsorship.is_sponsor_opted_in_to_email)

      if sponsorship_changed
        GitHub.instrument("sponsors.sponsor_sponsorship_preference_change", {
          user: current_sponsorship.sponsorable,
          actor: viewer,
        })
        GlobalInstrumenter.instrument("sponsors.sponsor_sponsorship_preference_change", {
          actor: viewer,
          previous_sponsorship: previous_sponsorship,
          current_sponsorship: current_sponsorship,
          listing: tier.sponsors_listing,
          tier: current_tier,
        })
      end

      if privacy_level_changed
        GitHub.instrument("sponsors.sponsor_sponsorship_edited", {
          actor: viewer,
          sponsorship: current_sponsorship,
          current_tier_id: current_tier.id,
          changes: {
            privacy_level: {
              from: previous_sponsorship.privacy_level,
            },
          },
        })
      end
    end

    attr_reader :tier, :sponsor, :sponsorable, :quantity, :viewer, :privacy_level, :email_opt_in
  end
end
