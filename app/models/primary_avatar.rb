# rubocop:disable Style/FrozenStringLiteralComment

# Joins an owner (User, Org) to the avatar that's actively being shown.
class PrimaryAvatar < ApplicationRecord::Domain::Users
  areas_of_responsibility :avatars
  # storage settings are in Avatar::Shared
  include ::Storage::Uploadable

  include Avatar::Shared
  include Instrumentation::Model

  belongs_to :avatar
  belongs_to :previous_avatar, class_name: "Avatar"
  belongs_to :updater, class_name: "User"
  belongs_to :owner, polymorphic: true
  validate :can_update
  validates_presence_of :avatar_id, :owner_id, :owner_type, :updater_id
  validates_uniqueness_of :owner_id, scope: :owner_type

  after_commit :update_keys_in_cdn
  after_commit :purge_keys_from_cdn, on: :destroy
  after_commit :instrument_update

  # Returns a Hash of URI query arguments for avatars.
  #
  # path - String path for the avatars service. Typically from implementations
  #        of #primary_avatar_path. Example: "/user/123"
  # size - Optional integer specifying a dynamic size for the avatar.
  # uniq - Optional string representing a unique hash for this avatar. This
  #        should change if the content of the avatar changes, not including
  #        dynamic sizing.
  def self.query_for(path, size: nil, uniq: nil)
    q = {}
    if v = GitHub.avatar_version(path)
      q[:v] = v
    end

    if b = GitHub.browser_avatar_version(path)
      q[:b] = b
    end

    q[:s] = size.to_i if size
    q[:u] = uniq.to_s if uniq
    q
  end

  # Returns a full avatar url for the given path and query parameters.
  #
  # path - String path for the avatars service. Typically from implementations
  #        of #primary_avatar_path. Example: "/user/123"
  # size - Optional integer specifying a dynamic size for the avatar.
  # uniq - Optional string representing a unique hash for this avatar. This
  #        should change if the content of the avatar changes, not including
  #        dynamic sizing.
  def self.url_for(path, size: nil, uniq: nil)
    q = query_for(path, size: size, uniq: uniq)
    path += "?#{q.to_query}" unless q.empty?
    "#{GitHub.alambic_avatar_url(path)}#{path}"
  end

  # Public: Build a new PrimaryAvatar from a given Avatar.
  #
  # avatar - Saved Avatar model.
  #
  # Returns a new, unsaved PrimaryAvatar.
  def self.build(avatar)
    new(owner: avatar.owner)
  end

  # Public: Gets the primary avatar for the owner.
  #
  # owner - An ActiveRecord model that owns an avatar.  Likely a User or
  # Organization.
  #
  # Returns a PrimaryAvatar
  def self.get(owner)
    find_by(owner_id: owner.id, owner_type: owner.class.base_class.name)
  end

  # Public: Sets the given avatar as the primary avatar for the avatar's owner.
  # This will retry once from an `owner_id` constraint failure.  Last write wins.
  #
  # avatar - An Avatar.
  # updater - The User that is setting the avatar.
  #
  # Returns a PrimaryAvatar.
  def self.set!(avatar, updater, handle_previous_avatar: true)
    set(avatar, updater, handle_previous_avatar: handle_previous_avatar)
  rescue ActiveRecord::RecordInvalid
    set(avatar, updater, handle_previous_avatar: handle_previous_avatar)
  end

  # Public: Sets the given avatar as the primary avatar for the avatar's owner.
  # Note: This does not rescue from an `owner_id` constraint failure.
  #
  # avatar - An Avatar.
  # updater - The User that is setting the avatar.
  #
  # Returns a PrimaryAvatar.
  def self.set(avatar, updater, handle_previous_avatar: true)
    primary = get(avatar.owner) || build(avatar)
    transaction do
      primary.set(avatar, updater)
      avatar.owner.try(:handle_previous_avatar, primary) if handle_previous_avatar
    end
    primary
  end

  def set(new_avatar, updater)
    update! avatar: new_avatar, updater: updater,
      previous_avatar: self.avatar,
      cropped_x: new_avatar.cropped_x, cropped_y: new_avatar.cropped_y,
      cropped_width: new_avatar.cropped_width, cropped_height: new_avatar.cropped_height
    owner.touch
  end

  def undo(updater)
    if av = previous_avatar
      set(av, updater || av.uploader)
      av
    else
      destroy
      nil
    end
  end

  def same_as_previous?
    previous_avatar_id == avatar_id
  end

  def avatar_oid
    avatar.try(:avatar_oid)
  end

  def content_type
    avatar.try(:content_type)
  end

  def purged_keys
    keys = [surrogate_key]
    keys << avatar.surrogate_key if avatar
    keys.compact!
    keys
  end

  def surrogate_key
    owner && owner.avatar_list.surrogate_key
  end

  def can_update?
    owner && updater && owner.avatar_editable_by?(updater)
  rescue NameError # raised when owner_type is not a class
    false
  end

  def can_update
    return if can_update?
    errors.add(:updater_id, "#{updater.try(:login) || :nil} does not have access to change #{owner.try(:login) || :nil}'s avatar.")
  end

  def avatar_storage_blob
    if !GitHub.storage_cluster_enabled?
      nil
    else
      avatar.storage_blob || raise(GitHub::DataQualityError.new(avatar, :storage_blob))
    end
  end

  private

  def instrument_update
    return if avatar.nil?

    owner = if avatar.owner.is_a?(NonMarketplaceListing)
      "#{avatar.owner.name}-#{avatar.owner.id}"
    else
      avatar.owner
    end

    payload = { actor: updater, owner: owner, type: avatar.owner.class.name }

    key = case avatar.owner
    when Organization
      :org
    when User
      :user
    when OauthApplication
      :oauth_application
    when Integration
      :integration
    when Marketplace::Listing
      :marketplace_listing
    when NonMarketplaceListing
      :non_marketplace_listing
    when Team
      :team
    when Business
      :business
    else
      raise "avatar on #{avatar.owner.class} not supported"
    end

    payload[key] = owner

    instrument :update, payload

    GlobalInstrumenter.instrument("profile_picture.update", {
      actor: updater,
      owner: avatar.owner,
      avatar: avatar,
    })
  end

  def event_prefix
    "profile_picture"
  end

  # Include this on models that act as avatar owners
  module Model
    def self.included(model)
      model.has_many :avatars,
        -> { order("id DESC") },
        as: :owner,
        dependent: :destroy
      model.send :attr_writer, :primary_avatar
    end

    def all_avatars
      avs = avatars.limit(30).to_a

      if primary = primary_avatar
        # find the Avatar matching the PrimaryAvatar record
        if primary_avatar = avs.detect { |a| a.id == primary.avatar_id }
          avs.delete(primary_avatar)
        # if it's not in the batch of avatars returned, select it
        else
          primary_avatar = primary.avatar
        end
        return [primary_avatar, avs]
      end

      return [nil, avs]
    end

    def can_be_avatar_owner?
      true
    end

    def primary_avatar
      @primary_avatar ||= async_primary_avatar.sync
    end

    def async_primary_avatar
      return Promise.resolve(@primary_avatar) if @primary_avatar

      Platform::Loaders::PrimaryAvatar.load(self).then do |primary_avatar|
        @primary_avatar ||= primary_avatar
      end
    end

    def primary_avatar?(avatar)
      avatar && primary_avatar && primary_avatar.avatar_id == avatar.id
    end

    def keep_old_avatar?
      true
    end

    def handle_previous_avatar(primary)
      return if keep_old_avatar?
      return if primary.same_as_previous?
      primary.previous_avatar.try(:destroy)
    end

    def primary_avatar_url(size = nil)
      # only load uniq avatar hash if `@primary_avatar` is loaded, otherwise
      # it's a potential n+1 query issue.
      uniq = @primary_avatar.try(:unique_signature)
      PrimaryAvatar.url_for(primary_avatar_path, size: size, uniq: uniq)
    end

    def static_avatar_url(size = nil)
      q = PrimaryAvatar.query_for(primary_avatar_path, size: size)
      query_string = ""
      query_string += "?#{q.to_query}" unless q.empty?
      "#{GitHub.alambic_avatar_url}#{primary_avatar_path}#{query_string}"
    end

    def unique_avatar_url(size = nil)
      primary_avatar
      primary_avatar_url(size)
    end

    def primary_avatar_path
      raise NotImplementedError, "cannot build an avatar url for a #{self.class} owner"
    end
  end
end
