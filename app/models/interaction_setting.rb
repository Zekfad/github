# frozen_string_literal: true

# Contains user settings related to interactions with other users
class InteractionSetting < ApplicationRecord::Domain::Users

  belongs_to :user

  # Public: Toggle the warning message displayed when viewing a repo if a
  # contributor is blocked by a user.
  #
  # user - The User to toggle the setting for.
  #
  # Returns a Boolean.
  def self.toggle_show_blocked_repo_contributors(user)
    return false if user.organization?

    setting = user.interaction_setting || InteractionSetting.create!(user_id: user.id)
    setting.update_attribute(:show_blocked_contributors_warning, !setting.show_blocked_contributors_warning)
  end
end
