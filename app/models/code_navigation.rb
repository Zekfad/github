# frozen_string_literal: true

class CodeNavigation
  # Known states based on availability of the code nav service, indexing
  # status, and if the blob is in one of the supported code nav langauges.
  STATES = [
    :ok,
    :error_index_not_found,
    :error_index_unreachable,
    :error_internal,
    :index_requested,
  ]

  # Public: Load code navigation symbols for the given repository
  def self.load(current_repository:, current_user:, tree_name:, commit_oid:, path:)
    args = [current_repository, current_user, tree_name]

    response = GitHub::Aleph.find_symbols_for_path(
      repo_id: current_repository.id,
      sha: commit_oid,
      path: path,
      is_public: current_repository.public?,
    )
    return new(:error_index_unreachable, *args) if response.nil?

    state, symbols = if response.error
      if response.error.code == :not_found
        [:error_index_not_found, nil]
      else
        [:error_internal, nil]
      end
    else
      [:ok, response.data.tags]
    end

    new(state, *args, code_symbols: symbols).tap do |nav|
      nav.reindex if state == :error_index_not_found
    end
  end

  def initialize(state, current_repository, current_user, tree_name, code_symbols: nil)
    @state = state
    @current_repository = current_repository
    @current_user = current_user
    @tree_name = tree_name
    @code_symbols = code_symbols || []
  end

  attr_reader :state, :current_repository, :current_user, :tree_name, :commit_oid, :code_symbols

  def has_code_symbols?
    state == :ok
  end

  def reindex
    return unless GitHub.flipper[:aleph_reindex_on_blob_view].enabled?
    return unless ref = current_repository.refs.find(tree_name) || current_repository.tags.find(tree_name)

    GlobalInstrumenter.instrument("repository.aleph_reindex", {
      repository: current_repository,
      owner: current_repository.owner,
      actor: current_user,
      after: ref.target_oid,
      ref: ref.qualified_name,
      feature_flags: current_repository.alephd_push_feature_flags,
    })
    @state = :index_requested
  rescue GitRPC::Error
    # ignore any errors and just skip reindexing
  end
end
