# frozen_string_literal: true

module AdvisoryDB
  class MungerDetection < OpenStruct

    SOURCE_IDENTITY = "munger/vulnerability_detections"

    # bathompso [15:36] 2018-07-23
    # Yeah, you could probably just concat `package_manager`, `package_name`, and `dt` into a primary key
    #
    # this identifier will be used retroactively to
    # help train the detection model, hence the
    # source_identifier attribute on pending_vulns
    def identifier
      ["VD", package_manager, package, detected_at].join("-")
    end

    # we HELLA need to standardize these
    def platform
      case package_manager
      when "NPM"
        "npm"
      when "PyPI"
        "pip"
      else
        package_manager
      end
    end

    def description
      "Potential vulnerability identified by the vulnerability detections api
      Probability: #{probability}
      Detected: \t#{detected_time}
      Processed: \t#{processed_time}"
    end

    def detected_time
      Time.at detected_at
    end

    def processed_time
      Time.at processed_at
    end

    def review_notes
      reference_urls.join("\n")
    end

    def to_pending_vuln
      {
        external_identifier: identifier,
        platform: platform,
        description: description,
        review_notes: review_notes,
        source: SOURCE_IDENTITY,
        source_identifier: identifier,
      }
    end

    def version_ranges
      versions.map do |v|
        {affects: package,
         requirements: v["affected"],
         fixed_in: v["fixed"]}
      end
    end
  end
end
