# frozen_string_literal: true

module AdvisoryDB
  module Ecosystems
    class Ecosystem
      attr_reader :name, :description

      def initialize(name, description, is_public)
        @name, @description, @is_public = name, description, is_public
      end

      def public?
        @is_public
      end
    end
    private_constant :Ecosystem

    # Dictionary of our supported Ecosystems.
    #
    # name        - Ecosystem name as it is persisted in the database
    # description - A user-facing description of the field used in
    #               our API documentation
    # public      - Is this ecosystem available via our API?
    RUBYGEMS = Ecosystem.new("RubyGems", "Ruby gems hosted at RubyGems.org", true)
    NPM = Ecosystem.new("npm", "JavaScript packages hosted at npmjs.com", true)
    PIP = Ecosystem.new("pip", "Python packages hosted at PyPI.org", true)
    MAVEN = Ecosystem.new("maven", "Java artifacts hosted at the Maven central repository", true)
    NUGET =  Ecosystem.new("nuget", ".NET packages hosted at the NuGet Gallery", true)
    COMPOSER = Ecosystem.new("composer", "PHP packages hosted at packagist.org", true)

    # define an ecosystem for testing that we can use to test the non-public ecosystems
    # also, the max length of ecosystem is 20 chars, therefore the word "ecosystem" is chopped to "eco"
    TEST_PREVIEW_ECO = Ecosystem.new("test_preview_eco", "a non-public ecosystem for testing", false)

    SUPPORTED = [RUBYGEMS, NPM, PIP, MAVEN, NUGET, COMPOSER, TEST_PREVIEW_ECO].freeze
    SUPPORTED_NAMES = SUPPORTED.collect(&:name).freeze
    PUBLIC = SUPPORTED.select(&:public?).freeze
    PUBLIC_NAMES = PUBLIC.collect(&:name).freeze

    def self.internal
      SUPPORTED
    end

    def self.public
      PUBLIC
    end

    # A list of ecosystems readable via ::SecurityAdvisory for our integrations
    def self.api_filter
      PUBLIC_NAMES
    end

    # A list of ecosystems permitted in the database by ::Vulnerability
    def self.database_enum
      SUPPORTED_NAMES
    end

    INTERNAL_ADVISORY_DB_SCHEMA_ECOSYSTEM_TYPES = %i(RUBYGEMS NPM PIP MAVEN NUGET COMPOSER).freeze
    def self.from_advisory_vulnerability(advisory_vulnerability)
      platform = advisory_vulnerability[:package_ecosystem]
      return nil unless INTERNAL_ADVISORY_DB_SCHEMA_ECOSYSTEM_TYPES.include? platform
      const_get(platform)
    end
  end
end
