# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module WebSocket

    LUAU_SOCKET_ID_VERSION = "V3".freeze

    # The Default Time to Live for EVERYTHING having to do with
    # these sockets in Redis.  We want to make sure that these are all
    # expiration based values, as a WebSocket is inherently a transient thing
    TTL = 1.hour.to_i

    # Length of random numbers generated for channel ids
    CHANNEL_ID_MAX = 2 ** 32

    # Used to report an unknown channel type to datadog
    UNKNOWN_CHANNEL_TYPE = "unknown".freeze

    class << self
      delegate :logger, to: Rails

      # Is WebSocket event fanout enabled.
      def enabled?
        return @enabled if defined?(@enabled)

        # Check global live updates switch first
        return false unless GitHub.live_updates_enabled?

        true
      end

      # Configure enabled flag in tests.
      def enabled=(val)
        @enabled = val
      end

      # Internal: Get shared verify for signing socket ids & subscription messages.
      #
      # Returns singleton MessageVerifier instance.
      def luau_verifier
        @luau_verifier ||= ActiveSupport::MessageVerifier.new(GitHub.longpoll_socket_id_secret, digest: "SHA256", serializer: JSON)
      end

      def luau_sign_socket_id(session)
        luau_verifier.generate(luau_socket_id(session))
      end

      def luau_url(user, session)
        signed_id = GitHub::WebSocket.luau_sign_socket_id(session)
        "#{GitHub.urls.alive_ws_url}/_sockets/u/#{user.id}/ws?session=#{signed_id}"
      end

      # Public: Generate unique socket id for a given user session.
      # Timestamp is so that we only have to allow recent values and
      # don't accept sockets forever.
      def luau_socket_id(session)
        raise ArgumentError, "session cannot be nil" unless session

        connection_id = SecureRandom.random_number(CHANNEL_ID_MAX)
        timestamp = Time.now.to_i
        { v: LUAU_SOCKET_ID_VERSION, u: session.user_id, s: session.id, c: connection_id, t: timestamp }
      end

      # Public: Generate a signed channel subscription for Luau.
      def signed_channel(channel)
        luau_verifier.generate({c: channel, t: Time.now.to_i})
      end

      def notify_pre_receive_environment_channel(pre_receive_environment, channel_id, data = {})
        attrs = generate_authzd_attributes(pre_receive_environment)
        notify_site_admin_channel(pre_receive_environment, channel_id, data, authzd_attributes: attrs)
      end

      def notify_spam_queue_entry_channel(spam_queue_entry, channel_id, data = {})
        attrs = generate_authzd_attributes(spam_queue_entry)
        notify_site_admin_channel(spam_queue_entry, channel_id, data, authzd_attributes: attrs)
      end

      def notify_bulk_dmca_takedown_channel(bulk_dmca_takedown, channel_id, data = {})
        attrs = generate_authzd_attributes(bulk_dmca_takedown)
        notify_site_admin_channel(bulk_dmca_takedown, channel_id, data, authzd_attributes: attrs)
      end

      def notify_user_channel(user_id, channel_id, data = {})
        notify_channel(channel_id, data, authzd_attributes: [])
      end

      def notify_repository_channel(repository, channel_id, data = {})
        if repository.public?
          attrs = []
        else
          attrs = generate_authzd_attributes(repository)
        end
        notify_entity_channel(repository, channel_id, data, authzd_attributes: attrs)
      end

      def notify_deployment_channel(deployment, channel_id, data = {})
        attrs = generate_authzd_attributes(deployment)
        notify_repository_writable_channel(deployment, channel_id, data, authzd_attributes: attrs)
      end

      def notify_repository_advisory_channel(repository_advisory, channel_id, data = {})
        if repository_advisory.published?
          GitHub::WebSocket.notify_repository_channel(repository_advisory.repository, channel_id, data)
        else
          attrs = generate_authzd_attributes(repository_advisory)
          notify_repository_writable_channel(repository_advisory, channel_id, data, authzd_attributes: attrs)
        end
      end

      def notify_issue_channel(issue, channel_id, data = {})
        return unless issue.repository

        if issue.repository.public?
          attrs = []
        else
          attrs = generate_authzd_attributes(issue)
        end

        notify_entity_channel(issue, channel_id, data,
          authzd_attributes: attrs)
      end

      def notify_pull_request_channel(pull_request, channel_id, data = {})
        return unless pull_request.repository

        if pull_request.repository.public?
          attrs = []
        else
          attrs = generate_authzd_attributes(pull_request)
        end
        notify_entity_channel(pull_request, channel_id, data,
          authzd_attributes: attrs)
      end

      def notify_project_channel(project, channel_id, data = {})
        if project.public?
          attrs = []
        else
          attrs = generate_authzd_attributes(project)
        end
        notify_entity_channel(project, channel_id, data,
          authzd_attributes: attrs)
      end

      def notify_discussion_channel(discussion, channel_id, data = {})
        if discussion.public?
          attrs = []
        else
          attrs = generate_authzd_attributes(discussion)
        end
        notify_entity_channel(discussion, channel_id, data,
          authzd_attributes: attrs)
      end

      def notify_discussion_post_channel(discussion_post, channel_id, data = {})
        attrs = generate_authzd_attributes(discussion_post)
        notify_entity_channel(discussion_post, channel_id, data, authzd_attributes: attrs)
      end

      private

      def generate_authzd_attributes(subject)
        attrs = subject.permissions_wrapper.serialized_subject_attributes
        attrs << Authzd::Proto::Attribute.wrap(:action, :view_live_update)
      end

      # Private: Publish message to Hydro for consumption in Alive.
      def publish_message(channel, data, authzd_attributes)
        payload = {
          channel: channel,
          data: GitHub::JSON.dump(data),
        }
        if authzd_attributes.any?
          pb = Google::Protobuf::Any.new
          pb.pack(Authzd::Proto::Request.new(attributes: authzd_attributes))
          payload[:authzd_attributes] = pb
        end
        GitHub.hydro_publisher.publish(payload, schema: "live_updates.v0.Message")
      end

      # Private: Notify all subscribes for the specific channel about updates
      # regarding 'data'.
      #
      # channel_id  - the String id of the channel to find sockets that are subscribed
      #               example: "issue:5601", "pull_request:1232"
      # data        - Hash to send as JSON to the sockets
      def notify_channel(channel_id, data, authzd_attributes:)
        raise TypeError, "expected data to be a Hash, but was #{data.class}" unless data.is_a?(Hash)
        return unless enabled?
        publish_message(channel_id, data, authzd_attributes)
      end

      # Private: Notify all subscribers for the specific channel about updates
      # regarding 'data', and experiment with getting the authorized channels from
      # authzd.
      #
      # entity      - Entity (e.g a Repository or Team) to scope permissions to
      # channel_id  - the String id of the channel to find sockets that are subscribed
      #               example: "issue:5601", "pull_request:1232"
      # data        - Hash to send as JSON to the sockets
      def notify_entity_channel(entity, channel_id, data, authzd_attributes:)
        notify_channel(channel_id, data, authzd_attributes: authzd_attributes)
      end

      def notify_site_admin_channel(entity, channel_id, data, authzd_attributes:)
        notify_channel(channel_id, data, authzd_attributes: authzd_attributes)
      end

      # Private: Notify all subscribers with write access to the specific
      # repository-related channel about updates regarding 'data', and experiment
      # with getting the authorized channels from authzd.
      #
      # entity      - Entity (e.g a Repository or Team) to scope permissions to
      # channel_id  - the String id of the channel to find sockets that are subscribed
      #               example: "issue:5601", "pull_request:1232"
      # data        - Hash to send as JSON to the sockets
      def notify_repository_writable_channel(entity, channel_id, data, authzd_attributes:)
        notify_channel(channel_id, data, authzd_attributes: authzd_attributes)
      end
    end
  end
end
