# rubocop:disable Style/FrozenStringLiteralComment

# Helper methods to generate WebSocket channel names.
module GitHub::WebSocket::Channels
  # Channel that gets updated whenever a branch is created, deleted, or
  # pushed to.
  #
  # repository  - A Repository.
  # branch_name - A String with a ref name (like "master").
  def self.branch(repository, branch_name)
    join("repo", repository.id, "branch", normalize_branch(branch_name))
  end

  # Channel that gets updated whenever someone pushes code to the repo.
  #
  # repository - A Repository.
  # user       - The User who pushed code into `repository`.
  def self.post_receive(repository, user)
    join("repo", repository.id, "post-receive", user.id)
  end

  def self.milestone_prioritized(milestone)
    join("milestone", milestone.id, "prioritized")
  end

  def self.project(project)
    join("projects", project.id)
  end

  def self.project_metadata(project)
    join("projects", "metadata", project.id)
  end

  def self.project_add_cards_link(project_id)
    join("projects", "add_cards", project_id)
  end

  def self.project_card(card)
    join("projects", "cards", card.id)
  end

  def self.team(team)
    join("teams", team.id)
  end

  def self.discussion_post(post)
    join("discussion_posts", post.id)
  end

  # Channel that gets updated whenever a Discussion is updated,
  # or when reactions or comments are updated for this Discussion.
  #
  # discussion - A Discussion.
  def self.discussion(discussion)
    join("discussion", discussion.id)
  end

  # Channel that gets updates whenever an item on a Discussion timeline is created.
  #
  # discussion - a Discussion
  def self.discussion_timeline(discussion)
    join("discussion", discussion.id, "timeline")
  end

  # Channel that is alerted when a thread is marked as read.
  # Messages here will contain individual items that are being
  # marked as read.
  #
  # user - The user for which to open the mark as read channel.
  #
  # Returns String.
  def self.marked_as_read(user)
    join("marked-as-read", user.id)
  end

  # Channel that gets updated whenever a new Status is created for a
  # given commit.
  #
  # repository - A Repository.
  # oid        - A Commit's OID, as a String.
  def self.commit(repository, oid)
    join("repo", repository.id, "commit", oid)
  end

  # Channel that gets updated whenever an issue gets updated.
  #
  # issue - An issue.
  def self.issue(issue)
    join("issue", issue.id)
  end

  # Channel that gets updated whenever the issue state changes.
  # issue - An issue.
  def self.issue_state(issue)
    join("issue", issue.id, "state")
  end

  # Channel that gets updates whenever an item on an issue timeline changes.
  #
  # issue - An issue
  def self.issue_timeline(issue)
    join("issue", issue.id, "timeline")
  end

  # Channel that gets updates whenever an item is added/removed from close_issue_references.
  #
  # issue - An issue
  def self.close_issue_references(issue)
    join("issue", issue.id, "close_issue_references")
  end

  # Channel that gets updated whenever a Pull Request is updated, or
  # the head_ref gets pushed to.
  #
  # pr - A PullRequest.
  def self.pull_request(pr)
    join("pull_request", pr.id)
  end

  # Channel that gets updated whenever the pull request state changes.
  #
  # pr - A PullRequest.
  def self.pull_request_state(pr)
    issue_state(pr.issue)
  end

  # Channel that gets updates whenever an item on a pull request timeline changes.
  #
  # pr - An PullRequest
  def self.pull_request_timeline(pr)
    join("pull_request", pr.id, "timeline")
  end

  # Channel that gets updated whenever a Pull Request is approved or changes are requested.
  #
  # pr - A PullRequest
  def self.pull_request_review_state(pr)
    join("pull_request", pr.id, "review_state")
  end

  # Channel that gets updated whenever a Pull Request Review is updated
  #
  # review - A PullRequestReview.
  def self.pull_request_review(review)
    join("pull_request_review", review.id)
  end

  # Channel that gets updated whenever the Pre-receive Environment download_state changes.
  #
  # env - A PreReceiveEnvironment.
  def self.pre_receive_environment_state(env)
    join("pre_receive_environment", env.id, "state")
  end

  # Channel list that gets updated whenever either the PR's head or base
  # branches change, or when the head status changes, so we can check
  # for mergeability.
  #
  # pr - A PullRequest.
  def self.pull_request_mergeable(pr)
    [
      # If the head_repository is nil, this is a cross-repo fork with a deleted head.
      (pr.head_repository ? branch(pr.head_repository, pr.display_head_ref_name) : nil),
      branch(pr.base_repository, pr.display_base_ref_name),
      commit(pr.base_repository, pr.head_sha),
    ].compact
  end

  # Channel that gets touched anytime the user's list subscription status
  # changes. For an example, after a user clicks to watch/unwatch a repo.
  #
  # list - A list object (Repository, Team, etc)
  def self.list_subscription(user, list)
    join("list-subscription", subject_id(list), user.id)
  end

  # Channel that gets touched anytime the user's thread subscription status
  # changes. For an example, after a user clicks to watch a thread or
  # mutes the thread.
  #
  # user      - A User
  # list      - A Repository
  # thread_id - Thread Integer id
  def self.thread_subscription(user, list, thread_id)
    join("thread-subscription", thread_id, user.id)
  end

  # Channel that gets updated anytime a user notification list is changed.
  # Includes new notifications as well has user marking existing notifications
  # as read.
  #
  # user - A User
  def self.notifications_changed(user)
    join("notification-changed", user.id)
  end

  # Channel that gets updated anytime a list is changed.
  # Includes new notifications as well has user marking existing notifications
  # as read.
  #
  # queue - A SpamQueue
  def self.spam_queue_changed(queue)
    join("spam-queue", queue.id)
  end

  # Channel that gets updated any time an import has new progress information.
  #
  # repository - A Repository
  def self.source_import(repository)
    join("repository_import", repository.id)
  end

  def self.check_run(check_run)
    join("check_runs", check_run.id)
  end

  def self.check_suite(check_suite)
    join("check_suites", check_suite.id)
  end

  def self.check_step(check_step)
    join("check_steps", check_step.id)
  end

  def self.workflow_runs(repository)
    join("workflow_runs", repository.id)
  end

  def self.deployments_dashboard_environment(repository, environment)
    join("deployments_dashboard", repository.id, environment)
  end

  # Channel that gets updated whenever a RepositoryAdvisory
  # gets updated.
  #
  # advisory - A RepositoryAdvisory.
  def self.repository_advisory(advisory)
    join("repository_advisory", advisory.id)
  end

  # Channel that gets updated whenever an ComposableComment
  # gets updated.
  #
  # composable_comment - An ComposableComment.
  def self.composable_comment(composable_comment)
    join("composable_comment", composable_comment.id)
  end

  # Channel that gets updated whenever a BulkDmcaTakedown status is updated
  #
  #  # takedown  - A Stafftools::BulkDmcaTakedown
  def self.bulk_takedown_status(takedown)
    join("bulk_takedown_status", takedown.id)
  end

  # Internal: Generate a String name out of parts consistently.
  def self.join(*args)
    args.join(":")
  end
  private_class_method :join

  # Internal: Normalize different representations of git branch names
  def self.normalize_branch(branch_name)
    branch_name.sub(%r{^refs/heads/}, "")
  end
  private_class_method :normalize_branch

  def self.subject_id(subject)
    join(subject.class.name.underscore.dasherize, subject.id)
  end
  private_class_method :subject_id
end
