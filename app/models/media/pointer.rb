# rubocop:disable Style/FrozenStringLiteralComment

# A parsed representation of a Git LFS blob pointer.  Comes in two flavors:
#
# Old school:
#
#   # external
#   {oid}
#
# Current:
#
#   version https://git-lfs.github.com/spec/v1
#   oid sha256:{oid}
#   size {size-in-bytes}
#
class Media::Pointer
  attr_accessor :version, :oid, :oid_type, :size

  MIN_SIZE_LIMIT = 77 # This is the size of an unversioned git-media blob, the smallest known
  SIZE_LIMIT = 200 # Max size of a blob pointer
  ALPHA_VERSION = "http://git-media.io/v/1"
  ALPHA_OID_TYPE = "sha256"

  VERSIONS = {
    "https://git-lfs.github.com/spec/v1" => {
      "oid" => lambda { |p, v|
        oid_type, oid = v.split(":", 2)
        p.oid_type = oid_type
        p.oid = oid
      },
      "size" => lambda { |p, v| p.size = v.to_i },
      :keys => %w(oid size),
    },
  }

  # backwards compatible with beta objects
  VERSIONS["https://hawser.github.com/spec/v1"] = VERSIONS["https://git-lfs.github.com/spec/v1"]
  VERSIONS["http://git-media.io/v/2"] = VERSIONS["https://git-lfs.github.com/spec/v1"]

  def initialize(version = nil, oid = nil, oid_type = nil, size = nil)
    @version = version
    @oid = oid
    @oid_type = oid_type
    @size = size
  end

  def self.parse(data)
    return if data.nil? || data.size > SIZE_LIMIT

    parse_lines(clean_encoding(data).split(/\n/u))
  end

  def self.parse_lines(lines)
    key = "lfs.parse_pointer"
    case lines[0]
    when /git-lfs/u
      GitHub.dogstats.increment(key, tags: %w(version:git-lfs))
      parse_versioned(lines)
    when /\A# .*git-media/u
      GitHub.dogstats.increment(key, tags: %w(version:legacy-git-media))
      new(ALPHA_VERSION, lines.last, ALPHA_OID_TYPE, 0)
    when /\A# .*|external/u
      GitHub.dogstats.increment(key, tags: %w(version:legacy-external))
      return nil
    when /hawser/u
      GitHub.dogstats.increment(key, tags: %w(version:hawser))
      parse_versioned(lines)
    when /git-media/u
      GitHub.dogstats.increment(key, tags: %w(version:git-media))
      parse_versioned(lines)
    end
  end

  def self.parse_versioned(lines)
    key, version = lines.shift.split(" ", 2)
    return unless key == "version"
    return unless parser = VERSIONS[version]

    pointer = new(version)

    parser[:keys].each_with_index do |key, idx|
      k, v = lines.shift.split(" ", 2)
      return unless k == key
      parser[k].call(pointer, v)
    end

    pointer
  end

  DIFF_OPERATIONS = {
    " " => lambda { |line, o, n| o << line; n << line },
    "-" => lambda { |line, o, n| o << line },
    "+" => lambda { |line, o, n| n << line },
  }

  # Parses a diff into two Media::Blob pointers.
  #
  #  @@ -1,3 +1,3 @@
  #   version http://git-media.io/v/2
  #  -oid sha256:3e7d379989220ede2ad46243f658f3414043dfa67cacfaedd4b46f1c037401ea
  #  -size 6294357
  #  +oid sha256:01c3450384945520d3bc618f5f88f2ce02ff05d11df8b823cfa935aa0cee94c0
  #  +size 6308114
  #
  # Returns an Array of two GitMedia::Pointer objects, or an empty Array.
  def self.parse_diff(diff)
    text = diff.try(:text) || diff.to_s
    return [] if text.size > (SIZE_LIMIT * 2) || text.blank?
    lines = clean_encoding(text.dup).split("\n")
    lines.shift # first line has the @@ diff meta stuff
    old_data = []
    new_data = []

    lines.each do |line|
      next unless op = DIFF_OPERATIONS[line[0]]
      op.call(line[1..-1], old_data, new_data)
    end

    [parse_lines(old_data), parse_lines(new_data)]
  end

  def valid?
    oid.present?
  end

  def self.clean_encoding(data)
    if data.encoding != Encoding::UTF_8
      data.encode!(
        Encoding::UTF_8,
        invalid: :replace,
        undef: :replace,
        replace: "?",
      )
    end

    data
  end
  private_class_method :clean_encoding
end
