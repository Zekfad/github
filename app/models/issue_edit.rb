# rubocop:disable Style/FrozenStringLiteralComment

class IssueEdit < ApplicationRecord::Domain::Repositories
  include FilterPipelineHelper
  include Instrumentation::Model
  include UserContentEdit::Core

  belongs_to :issue

  alias_attribute :user_content_id, :issue_id
  alias_method :user_content, :issue
  alias_method :async_user_content, :async_issue

  def user_content_type
    "Issue"
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, user_content_edit_id || "IssueEdit:#{id}")
  end
end
