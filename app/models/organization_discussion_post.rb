# frozen_string_literal: true

class OrganizationDiscussionPost < ApplicationRecord::Collab
  belongs_to :user
  belongs_to :pinned_by, class_name: "User", foreign_key: "pinned_by_user_id"
  belongs_to :organization

  include OrganizationDiscussionItem

  has_many :replies, class_name: "OrganizationDiscussionPostReply"
  destroy_dependents_in_background :replies

  attr_readonly :number

  before_create :set_number

  # Use VARBINARY limit from the database
  TITLE_BYTESIZE_LIMIT = 1024

  force_utf8_encoding :title

  validates :title, bytesize: { maximum: TITLE_BYTESIZE_LIMIT }, unicode: true

  scope :public_posts, -> { where(private: false) }

  scope :visible_to, ->(user) do
    if user
      org_ids = User::OrganizationFilter.new(user).unscoped_ids
      public_posts.or(where(organization_id: org_ids))
    else
      public_posts
    end
  end

  scope :most_recent, -> { order(id: :desc) }

  # Implements a required property of the Platform::Interfaces::Comment.
  #
  # You cannot create a discussion by email.
  def created_via_email
    false
  end

  def async_viewer_can_pin?(viewer)
    async_organization.then { |org| org.async_adminable_by?(viewer) }
  end

  def async_readable_by?(viewer)
    if public?
      Promise.resolve(true)
    else
      async_organization.then { |org| org.member_or_can_view_members?(viewer) }
    end
  end

  def title_changed?
    self.title.try(&:b) != self.title_was.try(&:b)
  end

  def pin_by(user_id:)
    self.pinned_at = Time.now
    self.pinned_by_user_id = user_id
  end

  def unpin
    self.pinned_at = nil
    self.pinned_by_user_id = nil
  end

  def pinned?
    !pinned_at.nil?
  end

  def public?
    !private?
  end

  def async_is_integration_with_write_access?(actor)
    async_is_integration_with_access?(actor, :write)
  end

  def platform_type_name
    "OrganizationDiscussion"
  end

  private

  # Does the given actor have access to org discussions?
  #
  # actor - Either an IntegrationInstallation or a Bot. Bots can only write
  #         public discussions, but IntegrationInstallations can write both
  #         public and private discussions. This is safe because an
  #         IntegrationInstallation can never act on its own, so ultimately this
  #         will get called again with the installation's bot user or the human
  #         user on behalf of whom the installation is acting.
  #
  # action - Symbol representing the desired access level (:read or :write).
  #
  # Returns a Promise of a Boolean.
  def async_is_integration_with_access?(actor, action)
    unless actor.can_act_for_integration?
      return Promise.resolve(false)
    end

    async_organization.then do |org|
      if action == :read
        org.resources.team_discussions.async_readable_by?(actor)
      elsif action == :write
        org.resources.team_discussions.async_writable_by?(actor)
      end
    end
  end

  def set_number
    unless Sequence.exists?(organization)
      Sequence.create(organization, organization.discussion_posts.count)
    end
    self.number = Sequence.next(organization)
  end
end
