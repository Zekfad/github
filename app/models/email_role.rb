# rubocop:disable Style/FrozenStringLiteralComment

class EmailRole < ApplicationRecord::Domain::Users
  include Instrumentation::Model
  class CannotDeleteLastPrimaryEmail < RuntimeError; end
  class CannotDeletePrimaryWithoutAlternate < RuntimeError; end
  Roles = %w[primary backup stealth soft_bounce hard_bounce suppressed]
  SOFT_BOUNCE_REENABLE_TIME = 24.hours
  belongs_to :user
  belongs_to :email, class_name: "UserEmail"
  after_create_commit   :instrument_creation
  before_destroy :prevent_last_primary_email_deletion,  if: :primary?
  before_destroy :enqueue_sendgrid_suppression_removal, if: :hard_bounce?

  scope :with_roles, ->(*roles) { where(role: roles) }
  scope :primary, -> { with_roles("primary") }
  scope :backup, -> { with_roles("backup") }
  scope :suppressed, -> { with_roles("suppressed") }
  scope :stealth, -> { with_roles("stealth") }

  delegate :no_existing_email_roles?, to: :user

  validates_presence_of :role
  validates_inclusion_of :role, in: Roles
  validates :role, uniqueness: { scope: :email_id, case_sensitive: true }, unless: :no_existing_email_roles?
  validate :ensure_only_one_primary, if: :primary?
  validate :ensure_only_one_backup, if: :backup?

  def self.public
    where(public: true)
  end

  # Public: define query methods for all the valid roles
  #
  # Return Boolean
  Roles.each do |role_name|
    define_method("#{role_name}?") do
      role == role_name
    end
  end

  # Public: Should the related email be "visible" to the outside world?
  def visibility
    public? ? "public" : "private"
  end

  def private?
    !public?
  end

  # Public: toggle the visibility of this email
  def toggle_visibility
    update!(public: !self.public?)
  end

  # Public: when an email in a soft_bounce state will be re-enabled (approximately)
  def reenable_time
    raise "only valid for soft_bounce roles" unless soft_bounce?
    created_at + SOFT_BOUNCE_REENABLE_TIME
  end

  BATCH_SIZE = 500

  # Internal: clear soft bounces that have been paused long enough
  def self.clear_soft_bounces
    GitHub.dogstats.time "email_role.time", tags: ["action:clear_soft_bounces"] do
      soft_bounces = github_sql.new \
        role: "soft_bounce",
        created_at: (Time.now - SOFT_BOUNCE_REENABLE_TIME)
      soft_bounces.add <<-SQL
        SELECT id FROM email_roles
        WHERE role = :role and created_at <= :created_at
      SQL

      soft_bounces.values.each_slice(BATCH_SIZE) do |slice|
        where(id: slice).delete_all
      end
      GitHub.dogstats.count "email_role.soft_bounces.deleted", soft_bounces.values.size
    end
  end

  private

  # Internal: validate that a User can only have one primary email
  def ensure_only_one_primary
    return true if user.existing_primary_email_role.nil?
    other_primary_roles = user.email_roles.primary - [self]
    if other_primary_roles.any?
      self.errors.add(:base, "Cannot assign multiple primary emails")
    end
  end

  # Internal: validate that a User can only have one backup email
  def ensure_only_one_backup
    return true if user.existing_backup_email_role.nil?
    other_backup_roles = user.email_roles.backup - [self]
    if other_backup_roles.any?
      self.errors.add(:base, "Cannot assign multiple backup emails")
    end
  end

  # Internal: Prevent removal of a primary email
  # if it's the User's only email.
  def prevent_last_primary_email_deletion
    if primary? && email.last_email?
      raise CannotDeleteLastPrimaryEmail
    end
  end

  # Internal: event payload for Instrumentation
  def event_payload
    {
      email_role: role,
      email_role_id: id,
      email: email.email,
      email_id: email.id,
      user: email.user,
    }
  end

  # Internal: instrument creation of an EmailRole
  def instrument_creation
    instrument :create, event_payload
  end

  def enqueue_sendgrid_suppression_removal
    if GitHub.sendgrid_enabled?
      SendgridSuppressionRemovalJob.perform_later(email.id)
    end
  end
end
