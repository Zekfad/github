# rubocop:disable Style/FrozenStringLiteralComment

module User::ConfigurationDependency
  extend ActiveSupport::Concern

  include Configurable

  # Internal: Set the type for associating configuration entries
  # explicitly using 'User' here instead of the class name so behavior
  # correctly gets inherited by Organization
  def configuration_entry_type
    "User"
  end

  # Internal: Set the configuration owner.
  # values for a User can be cascaded from (or overridden by) the global
  # GitHub object, or the global GitHub enterprise account in GHE server
  def configuration_owner
    return GitHub.global_business if GitHub.single_business_environment?
    GitHub
  end

  include Configurable::ForcePushRejection
  include Configurable::GitLfs
  include Configurable::PackageRegistry
  include Configurable::MaxObjectSize
  include Configurable::DiskQuota
  include Configurable::OperatorMode
  include Configurable::Ssh

  def git_lfs_enabled?
    git_lfs_config_enabled?
  end
end
