# frozen_string_literal: true

module User::PasswordDependency
  extend ActiveSupport::Concern

  BcryptSalt = "bcrypt"
  WEAK_PASSWORD_MESSAGE = "is in a list of passwords commonly used on other websites"
  OAUTH_TOKEN_BASIC_PASSWORD = "x-oauth-basic" # TODO: Deprecate constant if not used.
  TIME_ALLOWED_BEFORE_BLOCK = 30.days

  class_methods do
    def encrypt_with_sha1(password, salt)
      Digest::SHA1.hexdigest("--#{salt}--#{password}--")
    end

    def encrypt_with_bcrypt(password)
      GitHub.dogstats.increment "bcrypt", tags: ["action:encrypt"]
      BCrypt::Password.create(password, cost: GitHub.password_cost)
    end
  end

  # Public: Set new password.
  #
  # old_password - String password.
  # password - String password.
  # password_confirmation - String password.
  def change_password(old_password:, password:, password_confirmation:)
    unless authenticated_by_bcrypt?(old_password)
      errors.add(:old_password, "isn't valid")
      return false
    end

    if update(old_password: old_password, password: password, password_confirmation: password_confirmation)
      self.update_weak_password_check_result
      clear_dependencies(:password_changed)

      instrument :change_password
      GlobalInstrumenter.instrument(
        "user.password_update",
        actor: self,
        account: self,
        update_type: :USER_CHANGE,
      )
      AccountMailer.password_changed(self, "changed").deliver_later
    end
  end

  # Public: Set new password.
  #
  # password - String password.
  # password_confirmation - String password.
  #
  # Returns true if password is updated, false otherwise.
  def apply_password_reset(password:, password_confirmation:)
    # because regular validation allows blank password (silently ignores it)
    if password.blank?
      errors.add(:password, "can't be blank")
      return false
    end

    # TODO: remove once password_not_weak doesn't need it
    @password_changed_reason = "reset"
    if update(password: password, password_confirmation: password_confirmation)
      self.update_weak_password_check_result
      clear_auth_limit
      clear_dependencies(:password_reset)

      instrument :reset_password, actor: self
      GlobalInstrumenter.instrument(
        "user.password_update",
        actor: self,
        account: self,
        update_type: :USER_RESET,
      )
      AccountMailer.password_changed(self, "reset").deliver_later
      return true
    end

    false
  end

  # Called after password resets, password changes, and password randomizaitons.
  private def clear_dependencies(reason, revoke_oauth_accesses: false)
    revoke_active_sessions(reason)
    unverify_all_devices(reason)
    revoke_oauth_accesses(reason) if revoke_oauth_accesses
  end

  private def unverify_all_devices(reason)
    count = authenticated_devices.verified.update_all(approved_at: nil)
    GitHub.dogstats.count("authenticated_device", count, tags: ["action:unverify", "reason:#{reason}"])
  end

  private def revoke_oauth_accesses(reason)
    ::RevokeOauthAccessesJob.perform_later(self, explanation: reason, enqueued_at: Time.zone.now)
  end

  # Public: scramble the user's password. Used by support during recovery of a hijacked account.
  def set_random_password(actor:, send_notification: true)
    random_password = SecureRandom.hex(32)

    if update(password: random_password, password_confirmation: random_password)
      self.update_weak_password_check_result
      clear_dependencies(:password_randomized, revoke_oauth_accesses: true)

      instrument :randomize_password, GitHub.guarded_audit_log_staff_actor_entry(actor)
      GlobalInstrumenter.instrument(
        "user.password_update",
        actor: actor,
        account: self,
        update_type: :SET_RANDOM,
      )
      AccountMailer.password_changed(self, "changed").deliver_later if send_notification
      true
    end
  end

  # Public: Given a user and a requested email address, look for a match on the user's stored password reset emails and return it.
  #
  # Returns the stored data instead of using the input data so that case is preserved, since the local part of emails can technically be case-sensitive.
  #
  # Uses `password_reset_emails` to determine which emails are allowed.
  #
  # Returns String or nil
  def lookup_password_reset_email(requested_email_address)
    password_reset_emails.map(&:to_s).find { |email| ascii_upcase(email) == ascii_upcase(requested_email_address) }
  end

  def ascii_upcase(str)
    str.upcase(:ascii)
  end

  # Public: Is the specified email address acceptable for us to use to contact
  # them for password resets?
  #
  # If email verification is disabled (on Enterprise), we can use either
  # unverified or verified emails. Yes, emails shouldn't be verified if email
  # verification is disabled, but there have been bugs in the past that have
  # allowed this to happen.
  #
  # Otherwise, the following are allowed:
  #   * Primary email
  #   * Backup email (if set)
  #   * Any "notifiable" email - This includes all verified emails.
  #     If no emails are verified it includes all user entered emails.
  #
  # Returns Boolean
  def is_password_reset_email?(email_address)
    lookup_password_reset_email(email_address).present?
  end

  # Public: return the list of UserEmail that can be used for password reset.
  # This method doubles as the list of email addresses where important account
  # security emails are to be delivered. All email addresses returned should
  # pass the `is_password_reset_email?` check
  #
  # Returns:
  #   * Primary email if password_reset_with_primary_email_only is set
  #   * Primary email and Backup email (if set)
  #   * Primary email and any "notifiable" email
  #      * This includes all verified emails.
  #      * If no emails are verified it includes all user entered emails.
  def password_reset_emails
    if password_reset_with_primary_email_only?
      [primary_user_email]
    elsif has_backup_email?
      [primary_user_email, backup_user_email]
    else
      # The primary email is always valid for password reset, verified or not
      Set.new(emails.notifiable + [primary_user_email])
    end
  end
  alias account_related_emails password_reset_emails

  def forgot_password(password_reset)
    hours_until_expiry = ((password_reset.expires - Time.now) / 1.hour).round
    AccountMailer.new_password(
      password_reset.user,
      password_reset.email,
      password_reset.link,
      hours_until_expiry,
      password_reset.forced_weak_password_reset?,
    ).deliver_later
    instrument :forgot_password, email: password_reset.email, forced_reset: password_reset.forced_weak_password_reset?
  end

  # Internal: Is bcrypt_auth_token blank or password not nil?
  #
  # Returns true or false.
  def password_required?
    bcrypt_auth_token.blank? || !password.nil?
  end

  # Internal: Encrypt the password and set bcrypt_auth_token.
  def encrypt_password
    if !password.blank? && @password
      self.salt = BcryptSalt

      self.bcrypt_auth_token = self.class.encrypt_with_bcrypt(password)
      GitHub.dogstats.increment "bcrypt", tags: ["action:convert_to_plain"]

      @password = nil
    end
  end

  # Calculates the Shannon entropy value.
  # Source: https://rosettacode.org/wiki/Entropy#Ruby
  def character_variety_heuristic(password)
    counts = Hash.new(0.0)
    password.each_char { |c| counts[c] += 1 }
    leng = password.length

    counts.values.reduce(0) do |entropy, count|
      freq = count / leng
      entropy - freq * Math.log2(freq)
    end
  end

  # Internal: We (loosely) follow some of the password guidelines recommended in
  # the PCI Compliance docs. Namely: at least one downcase letter and
  # one number. If the user is using a passphrase (16+ chars and 2+ spaces),
  # we don't make them conform to those rules.
  #
  # Returns nothing.
  def enforce_stronger_password
    return if password.blank?

    if enhanced_employee_password_requirements?
      if password.length < GitHub.employee_password_minimum_length
        errors.add(:password, "is too short for an employee (must be at least #{GitHub.employee_password_minimum_length} characters)")
      end
      if character_variety_heuristic(password) < GitHub.employee_password_minimum_entropy
        errors.add(:password, "does not have enough unique characters")
      end
    end

    if enhanced_employee_password_requirements? || password.length < User::PASSPHRASE_LENGTH
      if password.length < GitHub.password_minimum_length
        errors.add(:password, "is too short (minimum is #{GitHub.password_minimum_length} #{"character".pluralize(GitHub.password_minimum_length)})")
      end

      if password !~ /[a-z]{#{GitHub.password_lowercase_requirement},}/
        errors.add(:password, "needs at least #{GitHub.password_lowercase_requirement} lowercase #{"letter".pluralize(GitHub.password_lowercase_requirement)}")
      end

      if password !~ /[A-Z]{#{GitHub.password_uppercase_requirement},}/
        errors.add(:password, "needs at least #{GitHub.password_uppercase_requirement} uppercase #{"letter".pluralize(GitHub.password_uppercase_requirement)}")
      end

      if password !~ /[0-9]{#{GitHub.password_digit_requirement},}/
        errors.add(:password, "needs at least #{GitHub.password_digit_requirement} #{"number".pluralize(GitHub.password_digit_requirement)}")
      end

      if password !~ /\W{#{GitHub.password_special_character_requirement},}/
        errors.add(:password, "needs at least #{GitHub.password_special_character_requirement} #{"special character".pluralize(GitHub.password_special_character_requirement)}")
      end
    end

    if login.present? && password.downcase.gsub(login.downcase, "").length <= 3
      errors.add(:password, "cannot include your login")
    end
  end

  # Public: returns true if a new password will be saved
  def will_save_change_to_password?
    will_save_change_to_bcrypt_auth_token?
  end

  # Internal: returns true if a new password has been saved
  def saved_change_to_password?
    saved_change_to_bcrypt_auth_token?
  end

  def provided_weak_password?
    return unless password
    self.validate # if the object has already been validated this won't re-run checks
    errors[:password].include?(WEAK_PASSWORD_MESSAGE)
  end

  def password_not_weak
    return if password.blank?
    tag = if new_record?
      :create_user
    elsif @password_changed_reason == "reset"
      :password_reset
    else
      :password_change
    end
    if CompromisedPassword.find_using_password(password, user: self, action: tag)
      errors.add(:password, WEAK_PASSWORD_MESSAGE)
    end
  end

  def authenticated_by_password?(password)
    unless authenticated_by_bcrypt? password
      return false
    end

    upgrade_bcrypt_password_cost(password)
    write_plain_bcrypt(password) if !plain_bcrypt?
    true
  end

  def authenticated_by_bcrypt?(password)
    return false if bcrypt_auth_token.nil?

    stored_password = BCrypt::Password.new(bcrypt_auth_token)
    crypted_password = self.class.encrypt_with_sha1(password, salt)
    bcrypted_password = plain_bcrypt? ? password : crypted_password

    GitHub.dogstats.time "bcrypt", tags: ["action:authenticated_by_bcrypt", "cost:#{stored_password.cost}"] do
      stored_password == bcrypted_password
    end
  rescue BCrypt::Errors::InvalidHash
    false
  end

  def plain_bcrypt?
    self.salt == BcryptSalt
  end

  def write_plain_bcrypt(password)
    GitHub.dogstats.increment "bcrypt", tags: ["action:convert_to_plain"]

    ActiveRecord::Base.connected_to(role: :writing) do
      update_column(:salt, BcryptSalt)
      update_column(:bcrypt_auth_token, self.class.encrypt_with_bcrypt(password))
    end
  end

  def upgrade_bcrypt_password_cost(password)
    if needs_cost_upgrade?
      GitHub.dogstats.increment "bcrypt", tags: ["action:update_cost"]
      write_plain_bcrypt(password)
    end
  end

  def needs_cost_upgrade?
    BCrypt::Password.new(bcrypt_auth_token).cost < GitHub.password_cost
  end

  def password_check_metadata
    # we handled the case in encrypted_attribute that if read fails, nil will be returned
    # and we fail open
    if self.plaintext_weak_password_check_result.present?
      PasswordCheckMetadata.new.read(self.plaintext_weak_password_check_result)
    else
      PasswordCheckMetadata.new
    end
  end

  def block_deadline
    deadline = if !password_check_metadata.weak?
      TIME_ALLOWED_BEFORE_BLOCK.after(Time.zone.now)
    else
      TIME_ALLOWED_BEFORE_BLOCK.after(Time.zone.at(password_check_metadata.discovery_timestamp))
    end

    deadline.strftime("%B %-d, %Y")
  end

  def update_weak_password_check_result(compromised_password: nil)
    check = PasswordCheckMetadata.new
    if compromised_password
      if !password_check_metadata.weak?
        check.discovery_timestamp = Time.now.to_i
        check.compromised_password_id = compromised_password.id
        # if check result was false before and now true, we store the timestamp
        update_attribute(:plaintext_weak_password_check_result, check.to_binary_s)
      else
        # if check result was true before, and now still true, we reencrypt for
        # security reasons so that it will be indistinguishable from when we update
        # the field when the check returns a none compromised password.
        update_attribute(:plaintext_weak_password_check_result, self.plaintext_weak_password_check_result)
      end
    else
      # if check result is false now, we update it to 0
      check.discovery_timestamp = 0
      check.exact_email_and_password_match = 0
      update_attribute(:plaintext_weak_password_check_result, check.to_binary_s)
    end
  end

  def mark_compromised_via_direct_match(password:, name:, version:)
    compromised_password = nil

    compromised_password = CompromisedPassword.find_using_password(
      password,
      user: self,
      stat: false
    )

    tags = [
      "employee:#{self.employee?}",
      "spammy:#{self.spammy?}",
      "tfa_enabled:#{self.two_factor_authentication_enabled?}",
      "version:#{version}",
      "datasource:#{name}",
    ]

    # Very rarely the password is nil after a lookup due to replication
    # lag, we got the OK to read from master in this case
    if compromised_password.nil?
      CompromisedPassword.find_using_password(
        password,
        user: self,
        role: :writing,
        stat: false
      )
      GitHub.dogstats.increment("auth.compromised_password.read_from_primary", tags: tags)
    end

    id = if compromised_password.nil?
      GitHub.dogstats.increment("auth.compromised_password.nil_compromised_password", tags: tags)
      0
    else
      compromised_password.id
    end

    check = PasswordCheckMetadata.new(
      compromised_password_id: id,
      discovery_timestamp: Time.now.to_i,
      exact_email_and_password_match: 1,
    )
    update_attribute(:plaintext_weak_password_check_result, check.to_binary_s)
    instrument :exact_email_and_password_match, compromised_password: compromised_password
  end
end
