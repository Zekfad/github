# frozen_string_literal: true

module User::DelegatedAccountRecoveryDependency
  extend ActiveSupport::Concern

  included do
    has_many :delegated_recovery_tokens
  end
end
