# rubocop:disable Style/FrozenStringLiteralComment

# Public: A response is created when we attempt to set the user's primary email address.
class User::SetPrimaryEmailStatus

  def initialize(invalid_model: nil)
    @invalid_model = invalid_model
    freeze
  end

  def error
    @invalid_model.errors.full_messages.to_sentence
  end

  def error?
    !success?
  end
  alias_method :failure?, :error?

  def success?
    @invalid_model.nil?
  end

  def raise_on_error!
    if error?
      GitHub::Logger.log_exception(
        {fn: "#{self.class.name}#raise_on_error!",
         error: @invalid_model.errors.full_messages.to_sentence},
        ActiveRecord::RecordInvalid.new(@invalid_model))
      raise ActiveRecord::RecordInvalid.new
    end
  end

  SUCCESS = new
end
