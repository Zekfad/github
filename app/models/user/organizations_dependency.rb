# rubocop:disable Style/FrozenStringLiteralComment

# Adds organizations-specific functionality to users. That is, real world
# users who can log in to the site and do fun things.
#
# It includes the various organizations-related associations, team
# membership discovery, organization discovery, etc.
module User::OrganizationsDependency
  extend ActiveSupport::Concern

  included do
    before_destroy :verify_no_owned_organizations
    before_destroy :remove_from_teams
  end

  # Public: An ActiveRecord scope representing the teams this user belongs to.
  def teams(with_ancestors: false)
    async_teams(with_ancestors: with_ancestors).sync
  end

  def team_ids(with_ancestors: false)
    async_team_ids(with_ancestors: with_ancestors).sync
  end

  # Public: Get this user's teams that are visible to the provided viewer.
  # Doesn't include teams this user is a member of indirectly via child teams.
  # Returns an ActiveRecord relation
  def async_visible_teams_for(viewer)
    async_teams.then do |user_teams|
      next Team.none unless viewer

      if viewer == self
        shared_org_ids = organization_ids
      else
        shared_org_ids = organization_ids & viewer.organization_ids
      end

      visible_team_ids = Organization.with_ids(shared_org_ids).flat_map do |org|
        next if org.deleted?
        org.visible_teams_for(viewer).pluck(:id)
      end

      user_teams.where(id: visible_team_ids)
    end
  end

  # Public: Returns a Promise that resolves as a Team relation for this User's
  # teams.
  #
  # If `with_ancestors:` is `true`, loads Teams and their ancestors
  # (the Teams this user is a member of directly and indirectly).
  # Defaults to `false`.
  #
  # Returns an ActiveRecord::Relation.
  def async_teams(with_ancestors: false)
    async_team_ids(with_ancestors: with_ancestors).then do |team_ids|
      next Team.none unless team_ids.any?
      Team.where(id: team_ids)
    end
  end

  def async_team_ids(with_ancestors: false)
    Platform::Loaders::UserTeams.load(self, with_ancestors: with_ancestors)
  end

  # Public: The organizations this user is associated with.
  def organizations
    User::OrganizationFilter.new(self).scope
  end

  # Public: The organization ids this user is associated with. Avoids an extra
  # look up of organizations as opposed to User#organizations.
  def organization_ids
    User::OrganizationFilter.new(self).scoped_ids
  end

  # The organizations that this user is a member of that are visible to the viewer
  def organizations_visible_to(viewer)
    return organizations if viewer == self

    # Viewers can see public organizations and organizations where they are also a member
    viewer_organization_ids = viewer&.organizations&.pluck(:id) || []
    public_organization_ids = public_organizations.pluck(:id)
    visible_organizations = organizations.where(id: public_organization_ids | viewer_organization_ids)
  end

  def organizations_info
    organizations_hash = Ability.where(
      actor_id: ability_id,
      actor_type: ability_type,
      subject_type: "Organization",
      priority: Ability.priorities[:direct],
    ).index_by(&:subject_id)
    # TODO(ams11): eager load Configurable entries for MembersCanCreateRepositories once
    # serializable field :members_can_create_repositories is removed
    Organization.includes(:business).where(id: organizations_hash.keys).each_with_object({}) do |org, hash|
      access = organizations_hash[org.id].action.to_sym
      if access == :read
        if org.members_can_create_repositories?
          access = :write
        elsif teams.owned_by(org).legacy_admin.any?
          # Keep support for allowing members of legacy admin teams to add repos.
          access = :admin
        end
      end

      org_hash = {access: access}

      case access
      when :write
        org_hash[:allow_public_repos] = org.members_can_create_public_repositories?
        org_hash[:allow_private_repos] = org.members_can_create_private_repositories?
        org_hash[:allow_internal_repos] = org.members_can_create_internal_repositories?
      when :read
        org_hash[:allow_public_repos] = org_hash[:allow_private_repos] = org_hash[:allow_internal_repos] = false
      else
        org_hash[:allow_public_repos] = org_hash[:allow_private_repos] = true
        org_hash[:allow_internal_repos] = org.supports_internal_repositories?
      end

      hash[org] = org_hash
    end
  end

  def organizations_info_sorted_hash
    organizations_info.sort_by { |o| o[0].login.downcase }.to_h
  end

  def installable_apps
    Marketplace::Listing.quick_installable_for(self)
  end

  def installable_marketplace_apps_hash
    installable = { self => installable_apps }

    organizations_info_sorted_hash.each_with_object(installable) do |(org, info), apps|
      apps[org] = Marketplace::Listing.quick_installable_for(org) unless info[:access] == :read
    end
  end

  # Public: The organizations this user is invited to.
  def invited_organizations
    OrganizationInvitation.where(invitee_id: id, accepted_at: nil).includes(:organization).map(&:organization)
  end

  # The ids of all organizations this user is an owner of.
  #
  # Returns an Array of Organization ids
  def owned_organization_ids
    Ability.user_admin_on_organizations(
      actor_id: id,
    ).pluck(:subject_id)
  end

  # The organizations this user is an owner of.
  #
  # Returns an Array of Organization objects (or an empty Array)
  def owned_organizations
    # Returning a scope rather than an array so users of this method that
    # call `count`, etc. don't need to load full objects for every owned org.
    Organization.where(id: owned_organization_ids)
  end

  def solitarily_owned_organizations
    owned_organizations.reject { |org| org.can_leave?(self) }
  end

  # Finds organizations that this user is a publicized member of.
  #
  # Returns an Array of Organization instances.
  def public_organizations
    Organization.where(
      "public_org_members.user_id = ?", id
    ).joins(
      "inner join public_org_members on users.id = public_org_members.organization_id",
    )
  end

  # Public: The organizations that the user is associated with via direct
  # membership or outside collaboratorship.
  #
  # Added for authorizing OAuth apps. Includes organizations that this user is
  # able to request authorization for.
  #
  # Returns an ActiveRecord::Relation of Organizations.
  def authorizable_organizations
    repository_ids = associated_repository_ids(including: [:direct])

    # We can find repositories owned by organiations (and not just forks of
    # private org repos) by seeing both it's organization_id is set, and it
    # matches the owner_id
    organization_ids = Repository.where("organization_id = owner_id AND id IN (?)", repository_ids).distinct.pluck(:organization_id)
    organization_ids |= User::OrganizationFilter.new(self).unscoped_ids

    Organization.where(id: organization_ids).order(:login)
  end

  # Can this user add members for the specified org and teams?
  #
  # org   - The organization the user wants to add members for.
  # teams - (Optional) The teams the user wants to add members for.
  # role  - (Optional) The role the user wants to add member for
  #
  # Returns a boolean.
  def can_add_members_for?(org, teams: [], role: nil)
    can_send_invitations_for?(org, teams: teams, role: role)
  end

  # Can this user send invitations for the specified org and teams?
  #
  # org   - The organization the user wants to send invitations for.
  # teams - (Optional) The teams the user wants to send invitations for.
  # role  - (Optional) The role the user wants to send invitations for
  #
  # Returns a boolean.
  def can_send_invitations_for?(org, teams: [], role: nil)
    return false if teams.any? { |t| t.organization_id != org.id }

    # The inviter can be the organization in certain obscure cases, like for an
    # invitation sent when transforming a user into an org.
    return true if org == self

    # In direct and legacy org membership, org admins/owners can send
    # invitations.
    return true if org.adminable_by?(self)

    # allow enterprise account owners to invite users to organizations.
    # currently only done via SCIM
    return true if org.business && org.business.owner?(self)

    # A GitHub App Bot, whose current installation
    # has write permission on the org's members
    if self.respond_to?(:installation)
      return true if org.resources.members.writable_by?(self.installation)
      return true if org.business && org.business.resources.enterprise_administration.writable_by?(self.installation)
    end

    # Is this user a manager inviting another manager to the organization?
    manager_adding_another?(org, role)
  end

  # Returns true if this user is a manager adding another manager to this org.
  #
  # org  - the organization to which the user wants to add another user
  # role - the role the new user will be given in the organization
  #
  # Only returns true if the manager is adding another manager of the same kind.
  #
  # Returns a boolean.
  def manager_adding_another?(org, role)
    # Billing managers can invite other billing managers
    role == :billing_manager && org.billing_manager?(self)
  end

  # called when destroying the user
  def remove_from_teams
    teams.each { |team| team.remove_member self }
  end

  # Verify that the user doesn't own any organizations before it is deleted.
  #
  # Returns a Boolean
  def verify_no_owned_organizations
    error_message = if !GitHub.enterprise? && owned_organizations.any?
      "You must transfer or delete all owned organizations"
    elsif solitarily_owned_organizations.any?
      "You must transfer or delete all organizations where you're the only owner"
    end

    if error_message.present?
      errors.add(:organizations, error_message)

      throw :abort
    else
      true
    end
  end

  # Internal: Find all Organizations for which this user is a billing manager
  #
  # Returns an ActiveRecord::Relation object
  def billing_manager_organizations
    org_ids = Ability.where(
      actor_id: ability_id,
      actor_type: ability_type,
      subject_type: "Organization::BillingManagement",
    ).pluck(:subject_id)
    Organization.where(id: org_ids)
  end

  # Public: The organizations this user is an owner of or
  # has billing management access to.
  #
  # Be careful with how you use this, as billing managers should
  # not have access to an organization's repositories or
  # member list.
  #
  # Returns an Array of Organization objects (or an empty Array).
  def admin_or_manager_organizations
    owned_organizations | billing_manager_organizations
  end

  # Public: The user's roles on the given Organization ORG
  #
  # Returns an Array of Strings
  def abilities_on_organization(org)
    [].tap do |memo|
      memo << "Owner" if org.adminable_by?(self)
      memo << "Member" if !org.adminable_by?(self) && org.direct_member?(self)
      memo << "Billing manager" if org.billing_manager?(self)

      if org.collaborators.exists?(self)
        count = collaborations_count(org)
        repositories = (count == 1) ? "repository" : "repositories"
        memo << "Collaborator on #{count} #{repositories}"
      end
    end
  end

  # Public: The number of outside collaborations the user has with a given
  #         Organization ORG
  #
  # Returns an Array of Strings
  def collaborations_count(org)
    repos = org.collaborating_repositories_for(id)
    repos.fetch(id).count
  end

  # Returns a Promise that resolves to a relation of organizations
  # that are mentioned in the company profile field
  # and are also verified as organizations for the user
  def async_verified_company_organizations
    async_profile.then do |profile|
      if profile&.company.present?
        mentioned_logins = []
        GitHub::HTML::MentionFilter.mentioned_logins_in(profile.company) do |_, login, _|
          mentioned_logins << login
        end
        public_organizations.where(login: mentioned_logins)
      else
        Organization.none
      end
    end
  end
end
