# rubocop:disable Style/FrozenStringLiteralComment

module User::RateLimitWhitelistingDependency
  extend ActiveSupport::Concern

  included do
    has_one :user_whitelisting, dependent: :destroy
    has_many :user_whitelistings, as: :whitelister
  end

  # Public: Returns whether or not this user can bypass content creation rate limits
  #
  # Returns true or false
  def content_creation_rate_limit_whitelisted?
    !user_whitelisting.nil? || temporarily_content_creation_whitelisted?
  end

  # Public: Toggle this user's rate limit whitelist status.
  #
  # See #content_creation_rate_limit_whitelist for a description of arguments, which will be
  # passed through.
  #
  # Returns nothing.
  def toggle_content_creation_rate_limit_whitelisted(**kwargs)
    if content_creation_rate_limit_whitelisted?
      content_creation_rate_limit_dewhitelist!(**kwargs)
    else
      content_creation_rate_limit_whitelist!(**kwargs)
    end
  end

  # Public: Temporarily allow this user to create content without limits.
  #
  # Returns nothing.
  def temporarily_whitelist_content_creation(whitelister:, duration: 3.days)
    key = content_creation_temporary_rate_limit_key
    content = [whitelister.id, Time.now.to_i].join(":")
    GitHub.kv.set(key, content, expires: duration.from_now)
    instrument :rate_limit_whitelist, \
      prefix: :staff,
      user: self,
      actor: whitelister,
      temporary: true
  end

  # Public. The user that temporarily whitelisted this user.
  #
  # Returns a User
  def temporary_content_creation_whitelisting_user
    User.find(temporary_content_creation_whitelist_info.split(":").first)
  end

  # Public. When this user was temporarily whitelisted.
  #
  # Returns a Time
  def temporary_content_creation_whitelisted_at
    Time.at(temporary_content_creation_whitelist_info.split(":").last.to_i)
  end

  # Public: Determine if this user is temporarily content creation whitelisted.
  #
  # Returns truthy or falsey.
  def temporarily_content_creation_whitelisted?
    temporary_content_creation_whitelist_info
  end

  # Stored information in GitHub::KV about temporary content creation limits
  #
  # Returns a string
  private def temporary_content_creation_whitelist_info
    return @_temporarily_whitelisted_content if defined?(@_temporarily_whitelisted_content)

    # If KV is unavailable, return nil. While KV is unavailable, we will treat
    # all users as if they are NOT temporarily whitelisted.
    @_temporarily_whitelisted_content =
      GitHub.kv.get(content_creation_temporary_rate_limit_key).value { nil }
  end

  # GitHub::KV key for temporary rate limits.
  #
  # Returns a String
  private def content_creation_temporary_rate_limit_key
    ["rate_limited_creation", "temporary_whitelist", self.id].join ":"
  end

  # Public: Remove this user from the whitelist.
  #
  # Returns nothing.
  def content_creation_rate_limit_dewhitelist!(whitelister:)
    user_whitelisting.try(:destroy)
    GitHub.kv.del(content_creation_temporary_rate_limit_key)
    instrument :rate_limit_dewhitelist, \
      prefix: :staff,
      user: self,
      actor: whitelister
  end

  # Public: Add this user to the whitelist.
  #
  #   whitelister: The staff User adding this user to the whitelist
  #
  # Returns nothing.
  def content_creation_rate_limit_whitelist!(whitelister:)
    self.create_user_whitelisting(whitelister: whitelister)
    instrument :rate_limit_whitelist, \
      prefix: :staff,
      user: self,
      actor: whitelister,
      temporary: false
  end

  # Public: Returns when this user was whitelisted
  #
  # Returns a Datetime or nil if the user is not whitelisted
  def content_creation_rate_limit_whitelisted_at
    user_whitelisting.try(:created_at)
  end

  # Public: Returns the user who whitelisted this user
  #
  # Returns a User or nil if the user is not whitelisted
  def content_creation_rate_limit_whitelister
    user_whitelisting.try(:whitelister)
  end

  # Public: Return the audit log event from the last time
  # this user was blocked due to submitting content too quickly.
  #
  # Returns an AuditLogEntry or nil if the user has never violated the whitelist.
  def last_content_creation_rate_limit_violation
    query = {
      allowlist: ["user.creation_rate_limit_exceeded"],
      actor_id: self.id,
      limit: 1,
    }
    res = Audit::Driftwood::Query.new_user_query(query).execute.results.first
    return nil unless res
    AuditLogEntry.new_from_hash(res.to_hash)
  end
end
