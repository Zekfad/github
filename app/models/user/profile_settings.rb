# frozen_string_literal: true

class User
  class ProfileSettings
    PRIVATE_CONTRIBS_SETTING = "show_private_contribution_count"
    ACTIVITY_OVERVIEW_SETTING = "activity_overview"
    DISABLE_PRO_BADGE_SETTING = "disable_pro_badge"
    DISABLE_MOBILE_SITE_SETTING = "disable_mobile_site"
    DISABLE_ACV_BADGE_SETTING = "disable_acv_badge"
    EMOJI_SKIN_TONE_PREFERENCE = :emoji_skin_tone_preference

    # enums for instrumenting beta features enrollment
    ACTIVITY_OVERVIEW_FEATURE = :org_scoped_activity
    PRO_BADGE_FEATURE = :pro_badge
    ACV_BADGE_FEATURE = :acv_badge

    def initialize(user)
      @user = user
    end

    # Public: Does this user want their contributions to private repositories to be shown in the
    # contributions graph and also summarized in the activity list on their profile?
    #
    # Returns a Boolean.
    def show_private_contribution_count?
      user_setting_enabled?(PRIVATE_CONTRIBS_SETTING)
    end
    alias_method :show_private_contribution_count, :show_private_contribution_count?

    # Public: Update the user's preference on whether private contribution counts should be shown
    # in their contributions graph and summarized in their profile activity list.
    #
    # value - Boolean, "1", or "0"
    #
    # Returns nothing.
    def show_private_contribution_count=(value)
      if update_user_setting(PRIVATE_CONTRIBS_SETTING, value)
        Contribution.clear_caches_for_user(@user)
        @user.instrument_private_contributions(enabled: value)
      end
    end

    # Public: Determines whether this user wants to show an 'Activity overview' section on their
    # profile, with links to filter contributions by organization.
    #
    # Returns a Boolean.
    def activity_overview_enabled?
      user_setting_enabled?(ACTIVITY_OVERVIEW_SETTING)
    end

    # Public: Update the user's preference on whether the "Activity overview" section should be
    # shown on their profile.
    #
    # value - Boolean, "1", or "0"
    #
    # Returns nothing.
    def activity_overview_enabled=(value)
      if update_user_setting(ACTIVITY_OVERVIEW_SETTING, value)
        if user_setting_enabled?(ACTIVITY_OVERVIEW_SETTING)
          GlobalInstrumenter.instrument("user.beta_feature.enroll",
            actor: @user,
            action: "enroll",
            feature: ACTIVITY_OVERVIEW_FEATURE,
          )
        else
          GlobalInstrumenter.instrument("user.beta_feature.unenroll",
            actor: @user,
            action: "unenroll",
            feature: ACTIVITY_OVERVIEW_FEATURE,
          )
        end
      end
    end

    # Public: Update the user's preference on whether the PRO badge should be
    # shown on their profile/hovercard (only if they have the Pro plan).
    #
    # value - Boolean, "1", or "0"
    #
    # Returns nothing.
    def pro_badge_enabled=(enable)
      # user setting is actually disabling the pro badge, but, for readability, all other
      # methods are referencing enabling the feature so the value needs to be flipped
      disable = toggle_setting_value(enable)

      if update_user_setting(DISABLE_PRO_BADGE_SETTING, disable)
        if user_setting_enabled?(DISABLE_PRO_BADGE_SETTING)
          GlobalInstrumenter.instrument("user.beta_feature.unenroll",
            actor: @user,
            action: "unenroll",
            feature: PRO_BADGE_FEATURE,
          )
        else
          GlobalInstrumenter.instrument("user.beta_feature.enroll",
            actor: @user,
            action: "enroll",
            feature: PRO_BADGE_FEATURE,
          )
        end
      end
    end

    # Public: Determines whether this user wants to enable the PRO badge on their profile/hovercard
    #
    # Returns a Boolean.
    def pro_badge_enabled?
      !user_setting_enabled?(DISABLE_PRO_BADGE_SETTING)
    end

    # Public: Update the user's preference on displaying the arctic code vault
    # contributor badge.
    #
    # enable - Boolean, "1", or "0"
    #
    # Returns nothing.
    def acv_badge_enabled=(enable)
      # Note: this setting is by default true, and we want to record the
      # opt-out, so we need to flip the value here.
      disable = toggle_setting_value(enable)
      update_user_setting(DISABLE_ACV_BADGE_SETTING, disable)
      if user_setting_enabled?(DISABLE_ACV_BADGE_SETTING)
        GlobalInstrumenter.instrument("user.beta_feature.unenroll",
          actor: @user,
          action: "unenroll",
          feature: ACV_BADGE_FEATURE,
        )
      else
        GlobalInstrumenter.instrument("user.beta_feature.enroll",
          actor: @user,
          action: "enroll",
          feature: ACV_BADGE_FEATURE,
        )
      end
    end

    # Public: Determines whether this user has opted out of displaying the arctic
    # code vault contributor badge.
    #
    # Returns a Boolean.
    def acv_badge_enabled?
      !user_setting_enabled?(DISABLE_ACV_BADGE_SETTING)
    end

    # Public: Update the user's preference on whether the mobile site should
    # not be served to them.
    #
    # opt_out - Boolean, "1", or "0"
    #
    # Returns nothing.
    def mobile_opt_out_enabled=(opt_out)
      update_user_setting(DISABLE_MOBILE_SITE_SETTING, opt_out)
    end

    # Public: Determines whether this user wants to opt out of the mobile
    # site experience.
    #
    # Returns a Boolean.
    def mobile_opt_out_enabled?
      user_setting_enabled?(DISABLE_MOBILE_SITE_SETTING)
    end

    # Public: Has the mobile opt out setting ever been set by the user?
    #
    # Returns a Boolean.
    def mobile_opt_out_setting_explicitly_set?
      result = GitHub.kv.get("user.#{DISABLE_MOBILE_SITE_SETTING}.#{@user.id}")
      value = result.value { nil }
      !value.nil?
    end

    # Public: Returns user's preferred emoji skin tone index.

    # Returns the preferred skin tone for emoji that support it. The
    # default tone is zero.
    #
    # https://en.wikipedia.org/wiki/Fitzpatrick_scale
    #
    # Returns a Integer 0-5.
    def preferred_emoji_skin_tone
      default_value = 0
      return default_value if @user.new_record?


      result = GitHub.kv.get("user.#{EMOJI_SKIN_TONE_PREFERENCE}.#{@user.id}")
      value = result.value { default_value } # On errors, the block value is returned
      value.to_i
    end

    # Public: Update user's emoji skin tone preference.
    #
    # value - Number between 0-5
    #
    # Returns nothing.
    def set_preferred_emoji_skin_tone=(value)
      return unless @user.persisted?
      value = [0, 1, 2, 3, 4, 5].include?(value.to_i) ? value.to_i : 0

      GitHub.kv.set("user.#{EMOJI_SKIN_TONE_PREFERENCE}.#{@user.id}", value.to_s)
    end

    private

    def toggle_setting_value(value)
      case value
      when "0" then "1"
      when "1" then "0"
      when true then false
      when false then true
      end
    end

    def user_setting_enabled?(setting)
      return false if @user.new_record?

      result = GitHub.kv.get("user.#{setting}.#{@user.id}")
      value = result.value { "false" } # On errors, the block value is returned
      value == "true"
    end

    # Private: Enable or disable a user profile setting for this user.
    #
    # setting - String or Symbol; the setting to change
    # value - Boolean; whether or not to enable the setting
    #
    # Returns true if setting was changed, false otherwise.
    def update_user_setting(setting, value)
      return false unless @user.persisted?

      value = false if %w(0 false).include?(value)
      value = !!value
      GitHub.kv.set("user.#{setting}.#{@user.id}", value.to_s)
      true
    end
  end
end
