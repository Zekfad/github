# rubocop:disable Style/FrozenStringLiteralComment

module User::BillingDependency
  extend ActiveSupport::Concern
  include Billing::MeteredBillable

  INVOICE_BILLING_TYPE = "invoice".freeze
  CARD_BILLING_TYPE = "card".freeze
  BILLING_TYPES = %w( card gift teacher invoice )
  ENCODING_BASE = 36
  BILLING_ATTEMPTS_LIMIT = 3

  delegate :zuora_account,
    to: :customer, allow_nil: true
  delegate :zuora_account_id, :bill_cycle_day, to: :customer, prefix: true, allow_nil: true

  # The max amount of days passed over all billing attempts
  #
  # 0th day + 7 days (1st retry) + 7 additional days (2nd retry)
  # = 14 day dunning period.
  #
  # https://www.braintreegateway.com/merchants/yrvvxhf7w35y8v9f/processing
  DUNNING_DAYS = 14.days

  # Plan durations. We offer monthly and yearly plans.
  YEARLY_PLAN = "year"
  MONTHLY_PLAN = "month"
  PLAN_DURATIONS = [YEARLY_PLAN, MONTHLY_PLAN]

  included do
    has_many :braintree_migrations
    has_many :pending_plan_changes, class_name: "Billing::PendingPlanChange",
      dependent: :destroy

    has_many :incomplete_pending_plan_changes, -> { incomplete }, class_name: "Billing::PendingPlanChange"
    has_many :pending_subscription_item_changes,
      class_name: "Billing::PendingSubscriptionItemChange",
      through: :incomplete_pending_plan_changes

    has_one :plan_subscription,
      class_name: "Billing::PlanSubscription",
      dependent: :destroy

    # Some named scopes to make things easier on the nightly billing batch
    # and to provide various user lists under /admin

    # Public: Paying users are trying to give us money
    #
    # Returns iterable User objects
    scope :paying, -> {
      where(
        "plan IS NOT NULL
        AND plan NOT IN (?)
        AND billing_type IN (?)",
        GitHub::Plan.free_names,
        %w[card],
      )
    }

    # Public: Users who need to be billed for their services as of today
    #
    # Returns iterable User objects
    scope :needs_billed, -> {
      select("users.*").
        joins(<<-SQL).
            LEFT OUTER JOIN plan_subscriptions ON users.id = user_id
            LEFT OUTER JOIN payment_methods ON payment_methods.user_id = users.id
        SQL
        where(
          "users.billing_type = 'card'
           AND users.plan IS NOT NULL
           AND users.plan NOT IN (?)
           AND (users.billed_on IS NULL OR users.billed_on <= ?)
           AND (users.disabled IS NULL OR users.disabled = ?)
           AND plan_subscriptions.braintree_id IS NULL
           AND (plan_subscriptions.zuora_subscription_id IS NULL
            OR plan_subscriptions.zuora_subscription_id IS NOT NULL
            AND (payment_methods.payment_token IS NULL
              OR payment_methods.payment_token = 'payment-token-cleared'))",

          GitHub::Plan.free_names,
          GitHub::Billing.today.to_s(:db),
          false,
        )
    }

    # Public: Users who expire in 2 weeks
    #
    # Returns iterable User objects
    scope :expiring_soon, -> {
      where(
        "(billed_on IS NOT NULL AND billed_on = ?)
        AND disabled = ?",
        (GitHub::Billing.today + 2.weeks).to_s(:db),
        false,
      )
    }

    scope :yearly, -> { where(plan_duration: YEARLY_PLAN) }

    # Named scopes for the various billing types
    scope :carded,   -> { where(billing_type: CARD_BILLING_TYPE) }
    scope :gift,     -> { where(billing_type: "gift") }
    scope :teacher,  -> { where(billing_type: "teacher") }
    scope :invoiced, -> { where(billing_type: INVOICE_BILLING_TYPE) }

    after_create   :signup_transaction
    after_update   :update_external_customer, :update_external_subscription
    after_commit   :run_scheduled_external_customer_update, :run_scheduled_subscription_synchronization
    before_destroy :delete_transaction
    has_many       :transactions, after_add: :clear_signup_transaction

    validates_inclusion_of :plan_duration, in: PLAN_DURATIONS
    validates_inclusion_of :billing_type,  in: BILLING_TYPES

    validate :seat_count_greater_than_member_count
    validate :seat_count_greater_than_base_units
    validate :seat_count_no_greater_than_world_population
  end

  class_methods do
    # Public: Provide a nice custom descriptor used for our billing statements
    # that get processed through Braintree. Descriptor are what customers will see
    # on their bank statement when they make a purchase. We use dyamic descriptors
    # to add in an encoded user_id so that support can match up a bank statement
    # to a user account.
    #
    # NB: The name/DBA section must be either 3, 7 or 12 characters and the
    # product descriptor can be up to 18, 14, or 9 characters respectively (with
    # an * in between for a total descriptor name of 22 characters)
    #
    # See Billing::PlanSubscription::Synchronizer::BILLING_DESCRIPTOR_URL as well.
    #
    # References:
    #   https://articles.braintreepayments.com/control-panel/transactions/descriptors
    #   https://halp.githubapp.com/discussions/374feb3ee51f11e49ff1e0b7fe3c8730
    #
    #   user_id: the user id that we convert for the descriptor
    #
    # Returns a String
    def custom_descriptor(user_id)
      "GitHub.com  *%s" % encode_id(user_id)
    end
    #
    # Public: encodes a user id into a short string for our billing statements
    #
    #   id: the user id to encode
    #
    # Returns a String
    def encode_id(id)
      id.to_s(ENCODING_BASE)
    end

    # Public: decodes a user id from a short string into a database id
    #
    #   id: the user id to decode
    #
    # Returns an Integer
    def decode_id(id)
      id.to_i(ENCODING_BASE)
    end

    # Public: Send out credit card expiring reminders to users whose card on
    # file will expire in the next three weeks.
    def send_expiring_credit_card_reminders
      PaymentMethod.expiring_credit_cards.includes(:user).find_each do |credit_card|
        BillingExpiringCardReminderJob.perform_later(credit_card.user) if credit_card.user
      end
    end

    # Sends emails and activates in-site notices for users that have a free trial
    # that is going to end in four days. You should only run this once per
    # day.
    #
    # Returns an Array of usernames that were notified.
    def notify_free_trial_ending
      usernames = []
      Billing::PendingSubscriptionItemChange.trial_ending_in_four_days.find_each do |subscription_item_change|
        user = subscription_item_change.user
        next if user.nil? || !subscription_item_change.free_trial

        BillingNotificationsMailer.free_trial_will_end_soon(user, subscription_item_change).deliver_now
        usernames << user.login
      end
      usernames
    end
  end

  delegate \
    :data_packs,
    :payment_amount,
    :plan,
    :plan_duration,
    :seats,
    :has_plan_changes?,
    :changing_plan?,
    :changing_seats?,
    :changing_duration?,
    to: :pending_cycle, prefix: true

  delegate :external_subscription_type, to: :plan_subscription, allow_nil: true

  delegate :metered_billing_eligible?, to: :plan, prefix: true
  delegate :codespaces_eligible?, to: :plan, prefix: true

  # Public: instance level version of User.custom_descriptor
  def custom_descriptor
    self.class.custom_descriptor(id)
  end

  # Public: Returns whether a user is eligible to use/sign up for a free trial
  # on a specific marketplace listing. A user is eligible for marketplace free
  # trial if:
  #  - They have not paid for a plan on this listing in the past
  #  - They have not used a free trial for the listing
  # An optional subscription item can be provided to exclude the item from the
  # checks. For example, when the subscription item is first created, we don't
  # want to check against it since it may be the first subscription item and
  # in that case the user is eligible for the free trial.
  #
  # Returns a Boolean
  def eligible_for_free_trial_on?(marketplace_listing:, subscription_item: nil)
    subscription_items = async_plan_subscription.sync.try(:async_subscription_items)&.sync || []
    return true if subscription_items.empty?

    items = subscription_items.select do |item|
      item.subscribable.listing == marketplace_listing
    end
    items = items.reject { |item| item == subscription_item } if subscription_item
    items.none?(&:disqualifies_listing_plan_free_trial?)
  end

  def update_external_customer
    if saved_change_to_login? && self.customer
      schedule_external_customer_update
    end
  end

  # Public: Schedule and immediately run a job to synchronize this user's plan
  # information with the third party provider if there are changes that need to be synchronized
  #
  # See User#update_external_subscription for more information
  def update_external_subscription!(**kwargs)
    update_external_subscription(**kwargs)
    run_scheduled_subscription_synchronization
  end

  # Public: Schedule and immediately run a job to synchronize this user's plan
  #         creates an external subscription if not present
  # See User#create_or_update_external_subscription for more information
  def create_or_update_external_subscription!(**kwargs)
    create_or_update_external_subscription(**kwargs)
    run_scheduled_subscription_synchronization
  end

  # Public: Create or update external subscription and add data packs to plan.
  def update_plan_with_data_packs
    # handle adding data packs to free plan
    ensure_addons_billed if data_packs > 0

    downgrade_to_free_if_nothing_billed_on_free_with_addons_plan

    if external_subscription?
      update_external_subscription!(force: true)
    elsif upcoming_charges?
      Billing::PlanSubscription::Transition.activate self
    end
  end

  # Public: Ensures that the plan is not `free?` so that addon charges (like LFS data and marketplace items)
  # are synched and billed correctly
  def ensure_addons_billed
    update(plan: GitHub::Plan::FREE_WITH_ADDONS) if plan.free?
    Billing::EnterpriseCloudTrial.new(self).ensure_addons_remain_billable_at_trial_expiry
  end

  # Public: If there are no remaining billable addons (datapacks or subscription items) and the plan is
  # `free_with_addons` we should change the plan to `free`. We also have to make sure that any enterprise
  # cloud trials are handled correctly.
  def downgrade_to_free_if_nothing_billed_on_free_with_addons_plan
    return if data_packs > 0 || active_subscription_items.any?

    update(plan: GitHub::Plan::FREE) if plan.free_with_addons?
    Billing::EnterpriseCloudTrial.new(self).ensure_free_plan_at_trial_expiry
  end

  # Public: Reset user's data packs to 0. Used for cleaning up after we've
  # downgraded a user's plan and disabled their repos due to failed billing, etc.
  def reset_data_packs(actor: self)
    old_lfs_count = data_packs

    asset_status.update!(data_packs: 0) if asset_status
    asset_status.update!(asset_packs: 0) if asset_status

    GlobalInstrumenter.instrument(
      "billing.lfs_change",
      actor_id: actor.id,
      user_id: id,
      old_lfs_count: old_lfs_count,
      new_lfs_count: 0,
    )
  end

  # Public: Reset user's billing attempts to 0. Used for cleaning up after we've
  # downgraded a user's plan.
  def reset_billing_attempts
    update!(billing_attempts: 0)
  end

  # Public: Reset user's billing date. Used for cleaning up after we've
  # downgraded a user to free.
  def reset_billed_on
    update!(billed_on: nil)
  end

  # Public: Should the user be reminded that their credit card is expiring?
  #
  # Returns truthy if the user should be reminded.
  def should_remind_about_expiring_card?
    has_credit_card? &&
      card_expiring_in_less_than_three_weeks? &&
      payment_method.expiration_reminders == 0
  end

  # Public: Has the credit card on file expired?
  #
  # Returns truthy if the card is expired.
  def card_expired?
    return false unless has_credit_card?

    today = GitHub::Billing.today
    payment_method.expiration_year < today.year ||
      (payment_method.expiration_year == today.year &&
       payment_method.expiration_month < today.month)
  end

  # Public: Is the credit card on file going to expire in the next three
  # weeks?
  #
  # Returns truthy if the card is about to expire.
  def card_expiring_in_less_than_three_weeks?
    has_credit_card? && payment_method.expiring_in_less_than_three_weeks?
  end

  # Public: The billing email address for this user.  If not billing_email address
  # is specified, then return the user's default email address
  #
  # Returns a String
  def billing_email
    organization_billing_email || email
  end

  def billing_email=(s)
    self.organization_billing_email = s
  end

  def billing_email_invalid?
    !billing_email.blank? && billing_email !~ User::EMAIL_REGEX
  end

  # Public: The user and the organizations owned by the user
  def owned_accounts
    owned_organizations + [self]
  end

  # Public: Checks if the given user or any of the user's adminable organizations has an
  # active or canceled subscription item for the given OAuth application or
  # integration, if it is listed in GitHub Marketplace.
  #
  # Returns a Boolean.
  def async_adminable_account_has_purchased_app?(app)
    app.async_marketplace_listing.then do |listing|
      next false if listing.blank?

      adminable_account_has_purchased_listing?(listing)
    end
  end

  # Public: Checks if the given user or any of the user's adminable organizations has an
  # active or canceled subscription item for the given Marketplace listing.
  #
  # Returns a Boolean.
  def adminable_account_has_purchased_listing?(listing)
    adminable_account_ids = adminable_org_ids | [id]
    has_subscription_for?(adminable_account_ids, listing)
  end

  # Public: IDs of accounts for the user and any orgs they belong to as member/admin/billing_manager.
  #
  # See https://github.com/github/github/pull/72915 for context on why we all
  # the ids from the org filter here (historically referred to as oap_unscoped)
  #
  # Returns an Array of account IDs
  def user_or_org_account_ids
    member_and_admin_org_ids = User::OrganizationFilter.new(self).unscoped_ids
    billing_manageable_org_ids = Ability.where(
        subject_type: "Organization::BillingManagement",
        actor_id: self,
        actor_type: self.ability_type,
        priority: Ability.priorities[:direct],
    ).pluck(:subject_id)

    member_and_admin_org_ids | billing_manageable_org_ids | [id]
  end

  # Public: Ids of accounts for the user and any orgs they admin.
  def user_or_adminable_org_ids
    return @user_or_adminable_org_ids if defined?(@user_or_adminable_org_ids)
    @user_or_adminable_org_ids = adminable_org_ids << id
  end

  # Public: Checks if the given user or an org the user is a member/admin/billing manager of has
  # an active or canceled subscription item for the given Marketplace listing.
  def user_or_org_account_has_purchased_listing?(listing)
    has_subscription_for?(user_or_org_account_ids, listing)
  end

  def has_subscription_for?(account_ids, listing)
    subscriptions_for_account = ::Billing::PlanSubscription.where(user_id: account_ids)
    listing_plan_ids = Marketplace::ListingPlan.where(marketplace_listing_id: listing.id).pluck(:id)

    ::Billing::SubscriptionItem
      .with_ids(subscriptions_for_account.pluck(:id), field: "plan_subscription_id")
      .for_marketplace_listing_plans(listing_plan_ids)
      .exists?
  end

  # Public: Checks if the given user has an active subscription item for any of
  # the given Marketplace listing plan IDs
  def has_active_subscription_item_for?(subscribable_ids)
    plan_subscription.present? && active_subscription_items.where(
      subscribable_type: Marketplace::ListingPlan.name,
      subscribable_id: subscribable_ids,
    ).any?
  end

  # Public: Checks if any of the given user's owned accounts has an active subscription
  # item for the given Marketplace listing.
  def any_active_subscription_items_for_owned_accounts?(marketplace_listing)
    marketplace_listing_plan_ids = marketplace_listing.listing_plans.pluck(:id)

    owned_accounts.any? do |account|
      account.has_active_subscription_item_for?(marketplace_listing_plan_ids)
    end
  end

  # Public: Checks if the given user can administer subscription items owned by this account.
  # This includes cancelling and editing subscription items.
  #
  # Returns a Boolean.
  def subscription_items_adminable_by?(user)
    return false unless user
    user == self || self.adminable_by?(user)
  end

  # Public: Schedules a cancellation for all of a user's associated subscription items
  #
  # force - cancel the subscription items immediately
  def cancel_subscription_items!(force: false)
    active_subscription_items.select(&:subscribable_paid?).each { |item| item.cancel!(force: force) }
  end

  # Internal: The Users that should receive billing email
  # for an account.
  #
  # Returns an Array.
  def billing_users
    [self]
  end

  # Internal: The Users that should receive admin email
  # for an account.
  #
  # Returns an Array.
  def admins
    [self]
  end

  # Public: Convenience method to pull out PaymentMethod from this user's Customer
  #
  # Returns a PaymentMethod
  def payment_method
    customer.try(:payment_method)
  end

  # Public: Returns a String of the Braintree customer ID
  def braintree_customer_id
    customer.try :braintree_customer_id
  end

  # Public: Returns the VAT Identification Number as a String
  #         This was historically sotred in billing_extra, but
  #         now should live in customer.vat_code
  def vat_code
    customer.try(:vat_code).presence ||
      GitHub::Billing::VatCodeParser.parse(attributes["billing_extra"])
  end

  # Public: Returns the billing_extra column, unless the column contains
  #         the vat_code
  def billing_extra
    value = attributes["billing_extra"]
    value if value != vat_code
  end

  # Public: convenience method to set:
  #   * braintree_customer_id attribute
  #   * payment_processor_customer_id on the PaymentMethod
  #   * braintree_customer_id on the Customer
  #
  #  NOTE: This is a transitional state while we maintain all three fields in sync.
  #  To be cleaned up later.
  #
  # Returns a String. Raises an ArgumentError if Customer doesn't exist.
  def braintree_customer_id=(val)
    if customer.present?
      customer.update_attribute(:braintree_customer_id, val)
      # Nota Bene - Keep this field in sync while we transition away from keeping customer id on payment
      # methods
      payment_method.update_attribute(:payment_processor_customer_id, val) if payment_method.present?
    else
      raise ArgumentError
    end
  end

  # Public: Boolean if this user has an external subscription linked
  def external_subscription?
    plan_subscription.present? && plan_subscription.has_external_subscription?
  end

  # Public: Boolean if this user's subscription is a Braintree::Subscription
  def braintree_subscription?
    plan_subscription.present? && plan_subscription.braintree_id.present?
  end

  # Public: Boolean if this user's subscription is a Zuora::Subscription
  def zuora_subscription?
    plan_subscription.present? && plan_subscription.zuora_subscription_number?
  end

  # Public: Boolean if this user's subscription is an Apple IAP subscription
  def apple_iap_subscription?
    plan_subscription.present? && plan_subscription.apple_iap_subscription?
  end

  # Public: If this user has a Zuora account
  #
  # Returns Boolean
  def zuora_account?
    customer&.zuora?
  end

  # Public: Returns this customer's payment method token
  #
  # Returns String payment method token
  def payment_method_token
    payment_method.try(:payment_token)
  end

  # Public: Is this User classified as a gift account?
  #
  # Returns a Boolean
  def gift?
    %w(gift teacher).include?(billing_type)
  end

  # Public: Boolean if this user has no upcoming charges to their account
  # because they are either:
  #   - On a gift account type or...
  #   - Have a coupon that covers 100% of their plan
  #
  # Returns truthy if this customer has no upcoming charges.
  def no_upcoming_charges?
    free_trial? || !paying_customer?
  end

  # Public: Boolean inverse of `no_upcoming_charges?`
  #
  # Returns truthy if user has upcoming charges.
  def upcoming_charges?
    !no_upcoming_charges?
  end

  # Public: figures out whether a user needs to input a credit card if they're
  # moving to a paid plan. If you're on a free plan with a $12/mo coupon, you
  # don't need a cc# to upgrade to Micro or Small. But you need one for Medium.
  # If you've already entered your payment information, we don't need it.
  #
  # new_plan - The GitHub::Plan object the user wants to upgrade to.
  #            If passed a String, we'll try to find the GitHub::Plan.
  #
  # Returns a Boolean true if they need to enter in a CC, false otherwise.
  def needs_valid_payment_method_to_switch_to_plan?(new_plan, new_seats = seats)
    return false unless GitHub.billing_enabled?

    new_plan = GitHub::Plan.find(
      new_plan,
      effective_at: plan_effective_at,
    ) if new_plan.is_a?(String)

    new_plan = MunichPlan.wrap(new_plan, target: self)

    if new_plan.free?
      # First one's free.
      false
    elsif invoiced? || gift? || has_valid_payment_method?
      # They already entered their card, or they don't need one ever.
      false
    elsif has_an_active_coupon?
      # They have a coupon - see if the discount covers their plan.
      # If they'll owe us money then ask for a card.
      new_plan_price = Billing::Pricing.new(
        account: self,
        plan: new_plan,
        plan_duration: User::MONTHLY_PLAN,
        seats: new_seats,
        discount: coupon&.discount,
      ).discounted

      new_plan_price.positive?
    else
      true
    end
  end

  # Public: figures out whether a user needs to input a credit card if they're
  # buying more data packs.
  #
  # delta_packs - Integer number of data packs user is attempting to add on
  #
  # Returns a Boolean true if they need to enter in a CC, false otherwise.
  def needs_valid_payment_method_to_buy_data_packs?(total_packs)
    return false unless GitHub.billing_enabled?

    if invoiced? || has_valid_payment_method?
      false
    else
      new_price = Billing::Pricing.new(
        account: self,
        plan_duration: MONTHLY_PLAN,
        data_packs: total_packs,
      ).discounted

      new_price.to_f > 0
    end
  end

  # Public: Charge this user for the current recurring payment amount.
  #
  # Returns GitHub::Billing::Result object indicating the success or
  # failure of the recurring charge
  def recurring_charge
    GitHub::Logger.log \
      at: "billing.user.recurring_charge",
      login: login,
      zuora_subscription_number: plan_subscription&.zuora_subscription_number

    if external_subscription?
      plan_subscription.retry_charge
    elsif should_transition_to_external_subscription?
      result = GitHub::Billing.transition_to_external_subscription(self)
      GitHub::Billing::Result.new(result, nil, nil)
    else
      recurring_charge_with_coupon(Billing::Money.new(payment_amount * 100).cents, recurring_charge_type)
    end
  end

  # Public: Charge type label for a recurring charge
  #
  # Returns a String
  def recurring_charge_type
    first_time_charge? ? "first-time-paid-upgrade" : "recurring-charge"
  end

  # Public: Record a $0 charge and move serivce date appropriately.
  #
  # charge_type - String type of charge being made. Usually 'recurring-charge'
  #               or 'prorate-charge'. Optional, default: recurring-charge.
  #
  # Returns GitHub::Billing::Result.success
  def process_zero_charge_transaction(charge_type = nil)
    billing_transaction = log_zero_charge_transaction(charge_type) do |txn|
      move_billed_on(txn)
      self.billing_attempts = 0
      enable!

      # We have to bypass validation, a user may be invalid (e.g. no email address),
      # but we don't want to re-charge them every day they are!
      save(validate: false)
    end

    GitHub::Billing::Result.success(billing_transaction)
  end

  # Public: Log a $0 billing transaction. If block is provided, call block.
  #         Update end of service metadata (after block finishes if block).
  #
  # charge_type - String type of charge being made. Usually 'recurring-charge'
  #               or 'prorate-charge'. Optional, default: recurring-charge.
  #
  # Returns the new billing transaction.
  def log_zero_charge_transaction(charge_type = nil, **options)
    charge_type ||= recurring_charge_type

    defaults = {
      user: self,
      amount_in_cents: 0,
      transaction_type: charge_type,
      payment_type: :no_charge,
      service_ends_at: billed_on,
    }

    billing_transaction = Billing::BillingTransaction.log defaults.merge(options)

    yield billing_transaction if block_given?

    # Update billing_transaction with actual end-of-service
    service_ends_at = options[:service_ends_at] || billed_on
    billing_transaction.update \
      last_status: :settled,
      transaction_id: billing_transaction.our_transaction_id,
      service_ends_at: service_ends_at

    billing_transaction
  end

  # Public: Remove all credit cards that this account has saved in our payment
  # processor's (Braintree or Zuora) vault.
  #
  # actor - The User taking this action.
  #
  # Returns truthy if all cards are successfully delete.
  def remove_all_payment_methods(actor)
    payment_method && payment_method.clear_payment_details(actor)
  end

  # Public: Should this user be charged?
  #
  # Returns a Boolean indicating wether we should charge the user
  def past_due?
    return false if plan.free?

    billed_on.nil? || billed_on <= GitHub::Billing.today
  end

  # Is this user getting service that they should be paying for, but aren't?
  #
  # Returns a Boolean
  def beneficiary?
    past_due? && !has_valid_payment_method? &&
      payment_amount > 0
  end

  # Public: Getter for this User's seat count. If the seats are explicitly
  # set to nil, 0 will be returned. If current object is an organization and
  # belongs to a business we delegate the seat count to the business.
  #
  # Returns an integer representing the number of seats
  def seats
    if delegate_billing_to_business?
      business.licenses
    else
      super.to_i
    end
  end

  # Public: Setter for this User's plan
  #
  # plan - The plan as either a GitHub::Plan or the plan's name as a String
  #
  # Returns the success or failure of the attribute write as a Boolean
  def plan=(new_plan)
    write_attribute(:plan, new_plan.to_s)
    @_plan_changed_this_save = true
  end

  # Public: Getter for this User's plan.  If the plan attribute is blank
  # the free default plan is used.
  #
  # Returns the Plan that this user is on or the free default plan
  def plan
    name = read_attribute(:plan)
    name = GitHub.default_plan_name if name.blank?

    if delegate_billing_to_business?
      business&.plan
    else
      plan = GitHub::Plan.find(name, effective_at: plan_effective_at)

      MunichPlan.wrap(plan, target: self)
    end
  end

  # Public: Previous value according to ActiveModel::Dirty
  #
  # plan_was *could* be a String in which case we find the plan for that string.
  def plan_was
    plan = super
    plan.is_a?(String) ? GitHub::Plan.find(plan) : plan
  end

  def plan_before_last_save
    plan = super
    plan.is_a?(String) ? GitHub::Plan.find(plan) : plan
  end

  def plan_changed_this_save?
    @_plan_changed_this_save
  end

  def forget_plan_changed_this_save
    unless frozen?
      @_plan_changed_this_save = false
    end
  end

  # Public: The DateTime the user signed up for their current plan
  #
  # Returns DateTime
  def plan_effective_at
    return GitHub::Billing.now unless persisted?
    return @plan_signup_transaction&.timestamp&.in_billing_timezone if @plan_signup_transaction&.timestamp&.in_billing_timezone
    @plan_signup_transaction = ActiveRecord::Base.connected_to(role: :reading) do
      transactions.where(
        action: ["signed-up", "downgraded", "upgraded", "switched-to-yearly", "switched-to-monthly"],
        current_plan: read_attribute(:plan),
      ).last
    end

    @plan_signup_transaction&.timestamp&.in_billing_timezone ||
      GitHub::Billing.now
  end

  # Public: Find the best fitting repository plan for this user based on
  # their number of repos.
  #
  # Returns GitHub::Plan.free because it always includes unlimited repos now.
  def best_plan
    GitHub::Plan.free
  end

  # Public: The next GitHub::Plan this User or Organization can upgrade to.
  #
  # Returns a GitHub::Plan or nil
  def next_plan
    plans =
      if organization?
        GitHub::Plan.all_non_free_org_plans.
          reject { |p| ["engineyard", "unlimited"].include? p.name }
      else
        GitHub::Plan.all_non_free_user_plans
      end

    current_plan = plan
    plans = [] if current_plan.name == "unlimited"
    plans.sort.find do |plan|
      repo_limit = organization? ? plan.org_repos : plan.repos
      plan.cost > current_plan.cost && repo_limit > private_repo_count_for_limit_check
    end
  end

  # Public: The next GitHub::Plan this User or Organization can upgrade to.
  #
  # Returns a GitHub::Plan or raises GitHub::Plan::Error if none are available to this user
  def next_plan!
    next_plan || raise(GitHub::Plan::Error, "no plan after #{self.plan.name}")
  end

  def plan_duration
    if delegate_billing_to_business?
      business.async_customer.sync
      business&.plan_duration
    else
      self[:plan_duration] || MONTHLY_PLAN
    end
  end

  # Public: Tells if this user is being billed on a yearly basis.
  #
  # Returns true if billing on a yearly cycle.
  def yearly_plan?
    plan_duration == YEARLY_PLAN
  end

  # Public: Tells if this user is being billed on a monthly basis.
  #
  # Returns true if billing on a monthly cycle.
  def monthly_plan?
    plan_duration == MONTHLY_PLAN
  end

  # Converts the wordy plan duration into something you can do math with
  #
  # Returns an Integer representing months
  def plan_duration_in_months
    yearly_plan? ? 12 : 1
  end

  # Public: If the users current plan does not support feature, trigger the removal of the feature
  #
  # Returns nil
  def remove_gated_features
    unpublish_private_pages unless plan_supports?(:pages, visibility: :private, fallback_to_free: disabled?)
    reset_private_protected_branches unless plan_supports?(:protected_branches, visibility: :private, fallback_to_free: disabled?)
  end

  # Public: Does this user's/org's plan support a gated feature?
  #
  # feature     - The feature Symbol to check (e.g. :codeowners)
  # visibility  - (Optional) The visibility scope. :public or :private.
  # fallback_to_free - (Optional) Should use the free plan instead if the User is disabled. Boolean
  #
  # Returns a Boolean.
  def plan_supports?(feature, visibility: nil, fallback_to_free: false, org: organization?)
    feature_gated_plan(fallback_to_free: fallback_to_free).
      supports?(feature,
        visibility: visibility,
        org: org,
        feature_flag: plan_override_feature_flag,
      )
  end

  # Public: This org's/user's plan limit for a gated feature.
  #
  # feature     - The feature Symbol to check (e.g. :collaborators)
  # visibility  - (Optional) The visibility scope. :public or :private.
  # fallback_to_free - (Optional) Should use the free plan instead if the User is disabled. Boolean
  #
  # Returns an Integer.
  def plan_limit(feature, visibility: nil, fallback_to_free: false)
    feature_gated_plan(fallback_to_free: fallback_to_free).
      limit(feature,
        visibility: visibility,
        org: organization?,
        feature_flag: plan_override_feature_flag,
      )
  end

  # Public: Whether this user qualifies for HelpHub support
  #
  # eligibiliy_checker - The class used to determine eligibility for HelpHub
  #
  # Returns a ::Billing::HelpHubEligibility
  def help_hub_eligibility
    ::Billing::HelpHubEligibility.new(account: self)
  end

  # Internal: This org's/user's current plan
  #
  # Returns a GitHub::Plan
  private def feature_gated_plan(fallback_to_free: false)
    target_plan = plan
    target_plan = GitHub::Plan.free if fallback_to_free && disabled? && !free_plan?

    target_plan
  end

  # Private: Returns the name of a Flipper feature flag if
  #   it is enabled globally or individually for this user/org.
  #
  #   At the moment, we only expect to use one feature flag override at a time.
  #
  #   Any overrides defined in config/plans.yml under `feature_flag_overrides`
  #   for the user/org's plan under the flag name will be preferred.
  #
  # Returns a Symbol, or nil if the feature is not enabled for the user/org
  private def plan_override_feature_flag
    nil # Set this to `nil` if there are no currently feature-flagged plan overrides.
  end

  # Public: Checks if the user is in a billing grace period on their current
  # plan, e.g. upgraded plans or changed to yearly and has not been billed yet.
  #
  # Returns true if the user is in grace period between billing for a monthly
  # plan and a yearly plan
  def in_yearly_grace_period?
    return false unless yearly_plan?
    return false if GitHub::Plan.find(plan_was).try(:free?)
    return false unless billing_transactions.last.try(:monthly?)

    billing_transactions.current.yearly.none? { |t| t.plan_name == plan.name }
  end

  # Public: How much should they actually be paying? Takes into account any discounts
  # their account currently has and any differing plan durations.
  #
  # options - Hash of options
  #        :plan               - GitHub::Plan object. Defaults to the current plan.
  #        :duration_in_months - Duration of requested plan. Defaults to current
  #                              plan duration in months
  #        :type               - Price type to return as a Symbol. Optional. Defaults to :final
  #
  # Returns a BigDecimal of dollars the person will pay.
  def payment_amount(plan: nil, duration: nil, duration_in_months: nil, type: :final, plan_seats: seats)
    period = \
      if duration
        duration
      elsif duration_in_months
        duration_in_months == 1 ? :month : :year
      end

    Billing::Pricing.new(
      account: self,
      plan: plan,
      plan_duration: period,
      seats: plan_seats,
    ).discounted.dollars
  end

  # Public: How much is this user paying not accounting for discounts
  #
  # Returns BigDecimal of dollars
  def undiscounted_payment_amount(plan: nil, duration: nil, type: :final, plan_seats: seats)
    Billing::Pricing.new(
      account: self,
      plan: plan,
      plan_duration: duration,
      seats: plan_seats,
    ).undiscounted.dollars
  end

  # Public: Returns the difference in payment amount of moving to a new plan.
  #
  # new_plan    - GitHub::Plan this user is moving to
  # use_balance - Boolean factor in current account balance
  # seat_count  - Integer - the number of seats for the new plan
  #
  # Returns a BigDecimal amount in dollars
  def payment_difference(new_plan, use_balance: false, seat_count: seats)
    if external_subscription?
      plan_change = Billing::PlanChange.new \
        subscription,
        Billing::Subscription.for_account(self, plan: new_plan, seats: seat_count)
      plan_change.final_price(use_balance: use_balance).dollars
    else
      payment_amount(plan: new_plan, plan_seats: seat_count) - payment_amount
    end
  end

  def undiscounted_payment_difference(new_plan, seat_count: seats)
    undiscounted_payment_amount(plan: new_plan, plan_seats: seat_count) - undiscounted_payment_amount
  end

  # Public: Return a Billing::Subscription to model this user's plan subscription
  #
  # Returns a Billing::Subscription
  def subscription
    Billing::Subscription.for_account(self)
  end

  # The balance on the customer's subscription. Negative means they have a
  # credit.
  #
  # Returns dollars in BigDecimal or 0
  def balance
    plan_subscription.try(:balance) || 0
  end

  # The credit the customer has on their subscription. Positive means they have
  # a credit
  #
  # Returns dollars in BigDecimal or 0
  def credit
    -1 * balance
  end

  # Whether a user has any credit.
  #
  # Returns true if a user has any credit
  def has_credit?
    credit > 0
  end

  # The next amount the customer will be charged. Includes the balance on the
  # customer's account.
  #
  # Returns non-negative dollars in BigDecimal or 0
  def next_charge_amount
    return [(payment_amount + balance), 0].max if balance < 0

    plan_subscription&.active? ? balance : payment_amount
  end

  def free_plan?
    plan.free? && !plan.coupon?
  end

  # Public: Void or refund last transaction for this user if one exists
  #
  # Returns GitHub::Billing::Result
  def void_or_refund_last_transaction!
    last_transaction = billing_transactions.sales.last
    if last_transaction.try(:refundable?)
      GitHub::Billing.void_or_refund_transaction(last_transaction)
    else
      GitHub::Billing::Result.success
    end
  end

  # Public: Returns a promise that resolves to a Boolean indicating whether this User has an
  # active subscription to the Marketplace listing with the given database ID.
  def async_has_purchased_marketplace_listing?(listing_id)
    listing_plans_ids = Marketplace::ListingPlan.where(marketplace_listing_id: listing_id).pluck(:id)
    async_plan_subscription.then { active_subscription_items.for_marketplace_listing_plans(listing_plans_ids).exists? }
  end

  # Public: Integer number of default seats when starting a per-seat plan
  def default_seats(new_plan: nil)
    check_plan = new_plan ? new_plan : self.plan
    check_seats = self.plan.paid? ? seats : 0
    base = check_plan.per_seat? ? check_plan.base_units : 0
    [check_seats, filled_seats, base].max
  end

  # Public: Integer number of seats that are in use.
  #
  # How filled seats is calculated:
  #
  #     direct organization members and admins
  #   + outside collaborators on private repositories
  #   + outside collaborators on private repositories with pending invites
  #   + any invited direct members
  #   + any invited admins
  # ---------------------------
  #     filled seats
  #
  # Returns an Integer of the number of seats occupied by users.
  def filled_seats
    GitHub.dogstats.time("organization", tags: ["action:filled_seats"]) do
      if delegate_billing_to_business?
        business.consumed_licenses
      elsif organization?
        Organization::LicenseAttributer.new(self).unique_count
      else
        collaborators_on_private_repositories.count + 1
      end
    end
  end

  # Public: Integer number of seats that need to be *added* to support the
  #         number of collaborators to make the passed repository private or to
  #         transfer the private repository to this organization.
  #
  # repository - Repository to check
  # pending_cycle - Boolean to check pending cyle rather than current plan/seats. Default false.
  # Returns an Integer
  def seats_needed_for_collaborators_on(repository, pending_cycle: false)
    return repository.filled_seats if user?
    return 0 if GitHub.enterprise?
    return 0 if !pending_cycle && plan.per_repository?
    return 0 if pending_cycle && pending_cycle_plan.per_repository?

    if delegate_billing_to_business?
      business.additional_licenses_required_for_repository(repository)
    else
      repository_user_ids = repository.member_ids + repository.repository_invitations.pluck(:invitee_id)
      needed_seats = (repository_user_ids - Organization::LicenseAttributer.new(self).user_ids.to_a).size
      [needed_seats - (pending_cycle ? pending_cycle_available_seats : available_seats), 0].max
    end
  end

  # Public: checks if a user is an outside collaborator. This will perform better than
  #         calling `outside_collaborators.include?` as it won't query for all outside
  #         collaborators.
  #         Note: this should only be called on organization objects, not User objects.
  # repo_ids - specific repo ids to check
  #
  # Returns a boolean
  def user_is_outside_collaborator?(user_id, repository_ids = [])
    repo_ids = Array(repository_ids)
    org_membership = Ability.where(
      actor_id: user_id,
      subject_id: id,
      actor_type: "User",
      subject_type: "Organization",
      priority: Ability.priorities[:direct],
    )
    return false if org_membership.exists?

    if repo_ids.empty?
      repo_ids = Repository.active.where(organization_id: id).ids
    end

    return false if repo_ids.empty?

    Ability.with_ids(repo_ids, field: "subject_id").where(
      actor_type: "User",
      actor_id: user_id,
      subject_type: "Repository",
      priority: Ability.priorities[:direct],
    ).exists?
  end

  def outside_collaborators_count
    outside_collaborator_ids.count
  end

  # Public: Get all the other users who have access to this user's
  # repositories (that aren't members of the org).
  #
  # Note: This is a fairly expensive method, and it's not memoized so that it
  # won't act weird after changing organization membership and repo access. If
  # you need to access this multiple times in one request, save it in a variable
  # first.
  #
  # on_repositories_with_visibility - Array of symbols defining visibility of
  #                                   repositories whose collaborators we're
  #                                   looking for. Valid values are :public and
  #                                   :private. Default: [:public, :private]
  #
  # include_forks                   - A Boolean value of whether to include
  #                                   counting forked repositories
  #                                   Default: true
  #
  # Returns a User scope.
  def outside_collaborators(on_repositories_with_visibility: [:public, :private], include_forks: true)
    ids = outside_collaborator_ids(on_repositories_with_visibility: on_repositories_with_visibility, include_forks: include_forks)

    User.with_ids(ids)
  end

  def outside_collaborator_ids(on_repositories_with_visibility: [:public, :private], include_forks: true, actor_ids: nil)
    return [] if new_record?

    visibility = on_repositories_with_visibility.map do |visibility|
      { public: 1, private: 0 }[visibility]
    end.compact

    # if both visibility options are included (public and private), then _all_
    # repos are included and repository_visibility is nil so that it isn't
    # included in the query as a filter
    repository_visibility = visibility.first if visibility.count == 1

    permission_cache_key = ["outside_collaborator_ids_no_join", id, \
                            on_repositories_with_visibility, \
                            (:exclude_forks unless include_forks)].compact

    PermissionCache.fetch permission_cache_key do
      repos = Repository.active
      repos = repos.where(public: repository_visibility) if repository_visibility
      repos = repos.where(parent_id: nil) unless include_forks
      if organization?
        repos = repos.where(organization_id: id)
      else
        repos = repos.where(owner_id: id)
      end

      repo_ids = repos.ids

      return [] if repo_ids.empty?

      repo_ability_ids = Set.new

      abilities_scope = Ability.where(
        actor_type: "User",
        subject_type: "Repository",
        priority: Ability.priorities[:direct],
      )

      if actor_ids
        abilities_scope = abilities_scope.with_ids(actor_ids, field: "actor_id")
      end

      repo_ids.in_groups_of(1000, false) do |rids|
        repo_ability_ids.merge abilities_scope.with_ids(rids, field: "subject_id").distinct.pluck(:actor_id)
      end

      org_abilities_scope = Ability.where(
        subject_id: id,
        actor_type: "User",
        subject_type: "Organization",
        priority: Ability.priorities[:direct],
      )

      if actor_ids
        org_abilities_scope = org_abilities_scope.with_ids(actor_ids, field: "actor_id")
      end

      org_member_ids = org_abilities_scope.distinct.pluck(:actor_id)

      repo_ability_ids - org_member_ids
    end
  end

  # Internal: Is the user already using the zuora service?
  #
  # Returns Boolean
  def existing_zuora_service?
    payment_method&.on_zuora? || zuora_subscription?
  end

  # Internal: IDs of users invited to collaborate
  def pending_non_manager_invited_user_ids
    pending_non_manager_invitations.excluding_expired.pluck(:invitee_id).compact
  end

  # Internal: A list pending email addresses not associated with user invited to join the organization.
  def pending_invited_non_user_emails
    pending_non_manager_invitations.excluding_expired.where("normalized_email IS NOT NULL and invitee_id IS NULL").pluck(:normalized_email)
  end

  # Internal: collaborators on private repositories only
  def collaborators_on_private_repositories
    outside_collaborators(on_repositories_with_visibility: [:private], include_forks: false)
  end

  def user_ids_with_private_repo_access
    return [] if new_record?
    repos = Repository.active.private_scope.where(parent_id: nil)

    if organization?
      repos = repos.where(organization_id: id)
    else
      repos = repos.where(owner_id: id)
    end

    repo_ids = repos.ids

    return [] if repo_ids.empty?

    user_ids = []

    repo_ids.each_slice(5000) do |rids|
      user_ids.concat Ability.with_ids(rids, field: "subject_id").where(
        actor_type: "User",
        subject_type: "Repository",
        priority: Ability.priorities[:direct],
        ).group(:subject_id, :subject_type, :priority, :actor_type, :actor_id).pluck(:actor_id)
    end
    user_ids
  end

  # Internal: returns invited users that are not already collaborating either
  # as contributors or outside collaborators in the organization
  def private_repo_non_collaborator_invitee_ids
    private_repo_invitee_ids -
      people_ids -
      collaborators_on_private_repositories.pluck(:id) -
      pending_non_manager_invited_user_ids
  end

  # Internal: User ids of users who have been invited to collaborate on private repositories.
  # This list may contain ids of users who may already be members of the organization or
  # outside collaborators.
  def private_repo_invitee_ids
    owned_private_repositories
      .joins(:repository_invitations)
      .merge(RepositoryInvitation.excluding_expired)
      .where("email IS NULL and invitee_id IS NOT NULL")
      .pluck(Arel.sql("DISTINCT repository_invitations.invitee_id"))
  end

  # Internal: A list pending email addresses not associated with users invited to collaborate on org private repositories.
  def private_repo_non_user_invited_emails
    owned_private_repositories
      .joins(:repository_invitations)
      .merge(RepositoryInvitation.excluding_expired)
      .where("email IS NOT NULL and invitee_id IS NULL")
      .distinct
      .pluck(:email)
  end

  # Internal: Users that are collaborators on private repositories only and have
  # NOT been invited to be a member/admin of the organization.
  #
  # Returns an Array of Users
  def collaborators_on_private_repositories_without_invitations
    collaborators_on_private_repositories.map(&:id) -
      pending_non_manager_invited_user_ids
  end

  # Public: returns float percentage of seats that are in use
  def filled_seats_percent
    return 0 unless plan.per_seat?
    [(filled_seats / seats.to_f * 100).round(1), 100].min
  end

  # Public: returns integer number of seats that are not in use
  def available_seats
    if delegate_billing_to_business?
      business.available_licenses
    else
      [seats - filled_seats, 0].max
    end
  end

  def pending_cycle_available_seats
    [pending_cycle_seats - filled_seats, 0].max
  end

  # Public: Boolean if on a per seat plan and at the seat limit (no available
  # seats). Always false for organizations on repository plans.
  # Pending Cycle - Boolean if at seat limit for pending cycle. Default false.
  def at_seat_limit?(pending_cycle: false)
    return false if has_unlimited_seats?
    if pending_cycle
      pending_cycle_plan.per_seat? && pending_cycle_available_seats < 1
    else
      plan.per_seat? && available_seats < 1
    end
  end

  # Public: Boolean if on a per seat plan and can give a seat to the user.
  # Always true for organizations on a repository plan.
  # Pending Cycle - Boolean if has seats for pending cycle. Default false.
  def has_seat_for?(user, pending_cycle: false)
    if delegate_billing_to_business?
      business.has_sufficient_licenses_for?(user)
    else
      !at_seat_limit?(pending_cycle: pending_cycle) ||
        Organization::LicenseAttributer.new(self).user_ids.include?(user&.id)
    end
  end

  # Public: Boolean if account is on free trial (has unlimited seats)
  def has_unlimited_seats?
    !!(has_an_active_coupon? && coupon.trial?)
  end

  # Public: Whether their plan has unlimited private repos
  #
  # Returns: Boolean
  def has_unlimited_private_repositories?
    feature_gated_plan.unlimited?(org: organization?, feature_flag: plan_override_feature_flag)
  end

  def has_downgradable_seats?
    seats > plan.base_units && available_seats > 0
  end

  def per_seat_plan_only?
    organization? && (plan.free? || plan.per_seat?)
  end

  # Public: Is this a user on the new personal plan (free tier).
  # TODO: Move this to plan.rb once we no longer need the feature flags.
  def personal_plan?
    user? && (plan.free? || plan.free_with_addons?)
  end

  # Public: Is this an org on the free plan.
  # TODO: Move this to plan.rb once we no longer need the feature flags.
  def org_free_plan?
    organization? && (plan.free? || plan.free_with_addons?)
  end

  def new_or_asset_status
    asset_status || Asset::Status.new(asset_type: :lfs, owner: self)
  end

  # Public: Integer number of data packs this user/org has purchased.
  def data_packs
    new_or_asset_status.asset_packs
  end

  # Public: Money price of all current data packs for current plan duration.
  def data_pack_price
    data_pack_monthly_price * plan_duration_in_months
  end

  # Public: Money price of all current data packs per month.
  def data_pack_monthly_price
    data_packs * Asset::Status.data_pack_unit_price
  end

  # Public: Integer days remaining in the current lfs cycle.
  #
  # Returns integer days.
  def days_remaining_in_lfs_cycle
    if free_plan?
      first_day = first_day_in_lfs_cycle
      return unless first_day

      end_date = first_day + 1.month
      today = GitHub::Billing.today
      (end_date - today).to_i
    else
      subscription.service_days_remaining
    end
  end

  # Public: Integer percent remaining in billing cycle.
  #
  # Returns an integer between 0 and 100.
  def percent_passed_in_billing_cycle
    100 - (subscription.service_percent_remaining * 100).to_i
  end

  def first_day_in_lfs_cycle
    if free_plan?
      # We're a not a paying customer thus have no billing date to go off of, so
      # lets check when the user started using LFS
      status_created_at = self.new_or_asset_status.created_at&.in_billing_timezone
      # If the user has never used LFS, give up
      return if status_created_at.nil?

      today = GitHub::Billing.today
      last_month_day = today.end_of_month.day
      day = [status_created_at.day, last_month_day].min
      first_day = ::GitHub::Billing.today.change(day: day)
      if first_day > today
        [first_day - 1.month, status_created_at.to_date].max
      else
        first_day
      end
    else
      if invoiced? && plan_subscription&.billing_start_date
        start = plan_subscription.billing_start_date
        today = ::GitHub::Billing.today
        year = today.year
        if Date.new(year, start.month, start.day) > today
          year -= 1
        end
        Date.new(year, start.month, start.day)
      else
        next_billing_date - (yearly_plan? ? 1.year : 1.month)
      end
    end
  end

  def paid_plan?
    plan.try(:paid?)
  end

  # Public: Checks to see if the user can change to a particular plan by an actor.
  #         If no actor is defined, it assumes the user itself.
  #
  # Returns a Boolean
  def can_change_plan_to?(target_plan, actor: self)
    Billing::ChangeSubscription.can_perform?(self, plan: target_plan.to_s, actor: actor)
  end

  def paying_customer?
    !gift? && paid_plan?
  end

  # Public: Is this user/org being billed via invoice?
  #
  # Returns truthy if the user is being billed by invoice.
  def invoiced?
    billing_type == INVOICE_BILLING_TYPE || async_business.sync.present?
  end

  # Public: Switch to credit card billing
  #
  # actor - User taking this action.
  #
  # Returns truthy if the switch was successful.
  def switch_billing_type_to_card(actor)
    User::CardConverter.new(self).convert(actor: actor)
  end

  # Public: Switch to invoice billing
  #
  # actor - User taking this action.
  #
  # Returns truthy if the switch was successful.
  def switch_billing_type_to_invoice(actor)
    User::InvoiceConverter.new(self).convert(actor: actor)
  end

  # Check if the user has any billing transactions.
  #
  # Returns true or false
  def has_billing_transactions?
    billing_transactions.present?
  end

  # Public: Boolean if the user has valid billing information, either on
  # Braintree, on Zuora, or as an invoiced customer.
  #
  # Returns truthy if the user can be billed.
  def has_billing_record?
    return false if payment_method.nil?

    customer.zuora_account_id.present?
  end

  # Public: Boolean if a user has a valid credit card on their account
  #
  # Returns truthy if there is a external vault customer with a credit card.
  def has_credit_card?
    payment_method.present? && payment_method.credit_card?
  end

  # Public: Boolean if a user has a valid paypal account registered.
  #
  # Returns truthy if there is a external vault customer with a paypal account.
  def has_paypal_account?
    payment_method.present? && payment_method.paypal?
  end

  # Public: Boolean if user has a valid payment method on file.
  def has_valid_payment_method?
    payment_method.present? && payment_method.valid_payment_token?
  end

  # Public: String friendly name for the payment method on file.
  def friendly_payment_method_name(none_text = nil)
    if has_credit_card?
      "credit card"
    elsif has_paypal_account?
      "PayPal account"
    else
      none_text || "payment information"
    end
  end

  # Public: Set a new date to bill this user on.
  def billed_on=(date)
    unless date.nil? || date.is_a?(Date)
      raise TypeError, "expected a Date, got #{date.inspect} instead. " \
        "Hint: #to_billing_date can be used to convert a Time into the right Date according to the billing timezone."
    end

    self[:billed_on] = date
  end

  def billed_on
    if delegate_billing_to_business?
      business.billed_on
    else
      self[:billed_on]
    end
  end

  # Public: The first date of the next billing cycle.
  #         For accounts in good standing, this is the next day we will
  #         automatically bill on.
  #
  #  with_dunning - Boolean. Whether to consider our dunning process when
  #                          calculating the date for a past due account.
  #         When false, returns today's date for past due accounts.
  #              (this is used in billing calculations when renewing an account
  #               to determine when the next cycle should begin)
  #         When true, returns the next day we will automatically attempt to
  #              charge the account's stored payment method, assuming that the
  #              user takes no action. This can return nil if no further
  #              attempts will be made.
  #         Defaults to false.
  def next_billing_date(with_dunning: false)
    next_billing_date = billed_on || GitHub::Billing.today
    if with_dunning
      next_dunning_day = [0, 7, 15][billing_attempts]
      return unless next_dunning_day
      next_billing_date + next_dunning_day.days
    else
      [next_billing_date, GitHub::Billing.today].max
    end
  end

  # Public: The soonest date a free trial will be ending for the user
  #
  # Returns a Date
  def next_free_trial_end_date
    if free_trials
      free_trials.order(:free_trial_ends_on).first&.free_trial_ends_on
    end
  end

  # Public: The next time the user will need to make a payment
  #
  # Returns a Date
  def next_payment_due_on
    [billed_on, next_free_trial_end_date].compact.sort.first
  end

  # Public: The users subscription items that are on a current free trial
  #
  # Returns an ActiveRecord::Relation of SubscriptionItems
  def free_trials
    active_subscription_items.free_trials if active_subscription_items.present?
  end

  # Public: The previous billed_on based on the cycle.
  #
  # cycles - How many cycles to count the billing date. Defaults to 1
  #
  # Returns a Date
  def previous_billing_date(cycles: 1)
    billing_date = billed_on || GitHub::Billing.today

    if monthly_plan?
      billing_date - cycles.month
    else
      billing_date - cycles.year
    end
  end

  # Public: Calculate billed_on date for an out of date metered cycle
  #
  # N.B. this will only update in memory, use #advance_metered_cycle_reset_date!
  # in the event you want to save the user instance this is called on.
  #
  # Returns Date or nil
  def advanced_metered_cycle_reset_date
    return billed_on unless billed_on.nil? || billed_on < ::GitHub::Billing.today

    active_charged_through_date = plan_subscription&.zuora_subscription&.active_charged_through_date
    external_account            = zuora_account

    # will return nil if no active_charged_through_date, no zuora_account, and also
    # no budget it is the intent to set the billed_on to nil
    # in those cases, as it will cause the metered billing quota reset to happen on
    # the first of every month
    #
    # note the nil check above, that is so that we give a chance to update if
    # something got out of sync, but we are ok with a nil billed_on
    if active_charged_through_date.present?
      active_charged_through_date
    elsif external_account&.body&.dig(:billingAndPayment, :billCycleDay).to_i > 0
      build_date_in_future(external_account[:billingAndPayment][:billCycleDay])
    # This will have a side effect of persisting a previously non-existent
    # budget record if a saving operation is called
    # on the the user record
    else
      budget_enabled_date = budgets.first&.created_at
      if budget_enabled_date
        build_date_in_future(budget_enabled_date.day)
      end
    end
  end

  # Public: Uses the in memory calculation done in #advanced_metered_cycle_reset_date
  # and sets the billed_on date accordingly
  #
  # Returns Boolean
  # Raises ActiveRecord::RecordInvalid
  def advance_metered_cycle_reset_date!
    update!(billed_on: advanced_metered_cycle_reset_date)
  end

  # Internal: Builds a date based on the combination of todays
  # date, and a passed day.
  #
  # base_day - the day to base the change on
  #
  # Examples:
  #
  #   Today is Jan 31st, 2020 and base_day is 30
  #   set_billed_on_to_future(30) => Sat, 29 Feb 2020
  #
  #   Today is Jan 25th, 2020 and base_day is 29
  #   set_billed_on_to_future(29) => Wed, 29 Jan 2020
  #
  # Returns Date
  def build_date_in_future(base_day)
    today = ::GitHub::Billing.today

    if base_day < today.day
      temp_date = (today + 1.month)
      update_date_with_day(temp_date, base_day)
    else
      update_date_with_day(today, base_day)
    end
  end

  # Internal: Handle date manipulations ensure we don't fall outside the end of the month
  #
  # Returns Date
  def update_date_with_day(date, day)
    if day > date.end_of_month.day
      date.end_of_month
    else
      date.change(day: day)
    end
  end

  # Internal: Retrieves first date starting next metered billing cycle
  #
  # Returns date starting next metered billing cycle
  def first_day_in_next_metered_cycle
    if delegate_billing_to_business?
      business.first_day_in_next_metered_cycle
    elsif !billed_on
      # Free plan users do not have a billed_on date so use start of next month
      GitHub::Billing.today.at_beginning_of_month.next_month
    else
      estimated = first_day_in_metered_cycle + 1.month
      year = estimated.year
      month = estimated.month
      day = day_starting_metered_cycle(year, month)
      Date.new(year, month, day)
    end
  end

  # Internal: Retrieves first date starting current metered billing cycle
  #
  # Returns date starting current metered billing cycle
  def first_day_in_metered_cycle
    if delegate_billing_to_business?
      business.first_day_in_metered_cycle
    elsif !billed_on
      # Free plan users do not have a billed_on date so use beginning of current month
      GitHub::Billing.today.beginning_of_month
    elsif monthly_plan?
      previous_billing_date
    else # yearly plan
      year = year_starting_metered_cycle
      month = month_starting_metered_cycle
      day = day_starting_metered_cycle(year, month)
      Date.new(year, month, day)
    end
  end

  # Internal: Retrieves day starting current metered billing cycle
  # accounting for end of month days compared to bill cycle day
  #
  # Returns integer representing day starting current metered billing cycle
  def day_starting_metered_cycle(year, month)
    day = previous_billing_date.day
    end_of_month = Date.new(year, month).end_of_month
    if day > end_of_month.day
      end_of_month.day
    else
      day
    end
  end

  # Internal: Retrieves month starting current metered billing cycle
  #
  # Returns integer representing month starting current metered billing cycle
  def month_starting_metered_cycle
    if previous_billing_date.day > GitHub::Billing.today.day
      (GitHub::Billing.today - 1.month).month
    else
      GitHub::Billing.today.month
    end
  end

  # Internal: Retrieves year starting current metered billing cycle (might be the previous year)
  #
  # Returns integer representing month starting current metered billing cycle
  def year_starting_metered_cycle
    if previous_billing_date.day > GitHub::Billing.today.day
      (GitHub::Billing.today - 1.month).year
    else
      GitHub::Billing.today.year
    end
  end

  # Public: Set disabled flag based on current plan limits, create a
  # Transaction to reflect the update, and update some stats.
  #
  # Returns nothing.
  def enable_or_disable!
    should_disable? ? disable! : enable!
  end

  def enabled?
    !disabled?
  end

  # Public: Enables a User so that they may continue using their private
  # repositories. Creates a new PlanSubscription, if necessary.
  #
  # Returns nothing
  def enable!
    if disabled?
      self.update_attribute(:disabled, false)
      track_unlock_billing
    end
  end

  # Public: Unlocks a User for 2 days by resetting their billing attempts,
  # moving their billing date 2 days into the future, and enabling their
  # account.
  #
  # After 2 days, the account will enter the normal dunning cycle.
  #
  # Returns nothing
  def unlock_billing!
    unless plan_subscription
      self.update \
        billing_attempts: 0,
        billed_on: [next_billing_date, relock_on].max
    end

    enable!
  end

  # Public: Will this user be relocked if unlocked with unlock_billing!
  #
  # Returns Boolean
  def will_relock?
    billed_on && billed_on < relock_on && over_billing_attempts_limit?
  end

  # Public: Date this users will be relocked if unlocked with unlock_billing!
  #
  # Returns Date
  def relock_on
    GitHub::Billing.today + 2.days
  end

  # Public: disables a user so they can no longer access their private
  # repositories. Also cancels their plan subscription, if they wouldn't need it
  # to pay for addons or if they were disabled due to billing problems.
  #
  # Returns nothing
  def disable!
    if enabled?
      self.update_attribute(:disabled, true)
      track_lock_billing
    end

    return unless external_subscription?

    if data_packs == 0 || over_billing_attempts_limit?
      plan_subscription.cancel_external_subscription
      reset_data_packs
    end
  end

  # Public: Unlock billing-locked private repos if the user has started paying
  #
  # Returns true
  def update_locked_repositories(log_as_part_of_job: nil)
    repositories.locked_repos.each do |repo|
      if log_as_part_of_job
        GitHub.dogstats.increment "#{log_as_part_of_job.underscore}.user.update_locked_repositories.repo"
        GitHub::Logger.log \
          job: log_as_part_of_job,
          at: "user.update_locked_repositories.repo",
          user_id: self.id,
          repository_id: repo.id,
          network_id: repo.network_id
      end
      repo.network.billing_unlock(log_as_part_of_job: log_as_part_of_job)
    end
    true
  end

  # Public: Queue a job to unlock billing-locked private repos if the user has
  # started paying for them.
  #
  # Returns nothing.
  def enqueue_update_locked_repositories
    forget_plan_changed_this_save
    UpdateLockedRepositoriesJob.perform_later(self.id)
  end

  def plan_changed_to_paid?
    plan_changed_this_save? && paid_plan?
  end

  # Public: Boolean if we have attempted billing on the account but failed.
  #
  # Returns true if there have been any billing attempts.
  def dunning?
    billing_attempts > 0
  end

  # Public: Boolean if a paid user has 3 or greater billing attempts.
  #
  # Returns truthy if we have been unable to bill this user.
  def unable_to_bill?
    billing_attempts >= 3
  end

  # Public: Boolean if a paid user is in billing trouble:
  #   - 3 or greater billing attempts OR
  #   - Failure to pay invoice
  #
  # Returns a Boolean indicating whether this User is in billing trouble.
  def billing_trouble?
    payment_amount > 0 && unable_to_bill?
  end

  # Public: Boolean if a user has had Sponsorship subscriptions rolled back
  #
  # Returns a Boolean
  def has_sponsorship_rollback?
    notices_for_dashboard.include?("sponsorship_rollback")
  end

  # Public: Set the Sponsorship rollback notification setting for a user.
  #
  # Returns nothing
  def set_sponsorship_rollback_notification
    activate_notice(:sponsorship_rollback) # Makes rollback an active notice for this user
    reset_notice(:sponsorship_rollback)    # Clears any previous dismissal by this user
  end

  # Public: Find Organizations owned by this user that are in billing trouble
  #
  # Returns an array of troubled Organizations
  def billing_troubled_orgs
    @troubled_orgs ||= owned_organizations.paying.select(&:billing_trouble?)
  end

  # Public: Does this User own an Organization that is in billing trouble?
  #
  # Returns a Boolean
  def org_billing_trouble?
    billing_troubled_orgs.size > 0
  end

  # Public: Is this org in billing trouble?
  #
  # Returns a   # Returns a Boolean
  def org_in_billing_trouble?(org:)
    billing_troubled_orgs.include?(org)
  end

  # Public: Returns true when a user has exceeded the billing attempts limit
  #
  # Returns a Boolean
  def over_billing_attempts_limit?
    # NB: This is to prevent first time payers from getting 2 grace period
    if never_successfully_billed?
      billing_attempts > 0
    else
      billing_attempts >= BILLING_ATTEMPTS_LIMIT
    end
  end

  # Returns truthy if this user has never successfully been billed.
  def never_successfully_billed?
    billed_on.nil?
  end

  # Internal: Cancels subscription, setting plan to Free and resetting billing
  #           date.
  #
  # Returns truthy if cancellation succeeds
  def cancel_subscription
    self.plan = GitHub::Plan.free
    plan_subscription.synchronize if external_subscription?
  end

  # Public: Find the next day the user will need to pay after their payment has
  # been processed.
  #
  # service_started_on - Date service starts, from which we calculate the new billed on.
  # duration           - "month" or "year" of service. Set this to determine next billing date
  #                      when plan duration is changing. Defaults to user's current plan_duration.
  #
  # Returns the Date that this User should be billed on next.
  def new_billed_on(service_started_on, duration = nil)
    duration ||= plan_duration
    if duration.to_s == YEARLY_PLAN
      service_started_on + 1.year
    else
      service_started_on + 1.month
    end.to_date
  end

  # Public: Applies the dunning rules to the account and performs
  # the necessary actions, like sending notifications or disabling the account.
  #
  # message - Message to append to the Mailer body.
  #
  # Returns nothing.
  def dun_subscription(message)
    Billing::DunSubscription.perform self, message: message
  end

  # Internal: can this user be billed?
  #
  # Returns a boolean
  def billable?
    true
  end

  # Public: Find Organizations billing manageable by this user that are on per repository plan
  #
  # Returns an array of per repository Organizations
  def billing_manageable_per_repo_orgs
    @billing_manageable_per_repo_orgs ||= admin_or_manager_organizations.select { |o| !o.plan.per_seat? }
  end

  def billing_manageable_non_business_plus_orgs
    @billing_manageable_non_business_plus_orgs ||= admin_or_manager_organizations.reject { |o| o.plan.business_plus? }
  end

  def billing_manageable_business_plus_orgs
    @billing_manageable_business_plus_orgs ||= admin_or_manager_organizations.reject { |o| !o.plan.business_plus? }
  end

  # Public: Find Organizations billing manageable by this user that are gifted (edu/nonprofit/opensource)
  #
  # Returns an array of gifted Organizations
  def billing_manageable_gifted_orgs
    @billing_manageable_gifted_orgs ||= admin_or_manager_organizations.select { |org|
        org.plan.per_seat? &&
        org.coupon &&
        (["notsoprofitable", "opensource"].include?(org.coupon.code) || org.coupon.group == "educaton-org")
    }
  end

  # Public: Downgrade and disable this account if necessary based on a disputed
  # transaction.
  #
  # payment_token - the payment token from the disputed transaction
  #
  # Returns nothing.
  def chargeback_disable(payment_token)
    return unless braintree_subscription? && has_credit_card? && enabled?
    return unless payment_token == payment_method_token

    # Manually bump billing_attempts so that disable! will work its magic
    self.update_attribute(:billing_attempts, BILLING_ATTEMPTS_LIMIT + 1)
    disable!

    GitHub.instrument "staff.chargeback_disable", user: self
  end

  CHARGEBACK_REASONS = %w(fraud cancelled_recurring_transaction)
  def chargeback_disable_reason?(reason)
    CHARGEBACK_REASONS.include? reason
  end

  # Shows whether a given user has a plan or organization on the
  # `business_plus` / Business plan.
  #
  # Returns true for:
  #    - an organization on the Business plan
  #    - an owner, member or billing manager of an organization that is on the business_plus plan
  #    - an outside collaborator on a repo belonging to an organization that is on the business_plus plan
  #    - an owner or a billing manager of a Business
  #
  # Returns a Boolean
  def business_plus?
    plan.business_plus? || business_plus_member? || business_plus_collaborator? || business_admin_or_manager?
  end

  # Is this user a member, admin, or billing manager of a Business org?
  #
  # Returns a Boolean
  def business_plus_member?
    return false unless user?
    !!(organizations.detect(&:business_plus?) || billing_manager_organizations.detect(&:business_plus?))
  end

  # Is this user an outside collaborator in a Business organization?
  #
  # Returns a Boolean
  def business_plus_collaborator?
    return false unless user?
    !!(member_repositories.where(public: false).includes(:owner).map(&:owner)
      .select(&:organization?).detect(&:business_plus?))
  end

  def business_admin_or_manager?
    return false unless user?
    businesses(membership_type: :admin).any? || businesses(membership_type: :billing_manager).any?
  end

  def subscription_items
    plan_subscription.try(:subscription_items) || Billing::SubscriptionItem.none
  end

  def active_subscription_items
    plan_subscription.try(:active_subscription_items) || Billing::SubscriptionItem.none
  end

  # Public: Returns this User's active SubscriptionItem for the Marketplace listing with the given
  # database ID.
  #
  # Returns nil or a SubscriptionItem.
  def subscription_item_for_marketplace_listing(listing_id)
    async_subscription_item_for_marketplace_listing(listing_id).sync
  end

  def async_subscription_item_for_marketplace_listing(listing_id)
    async_plan_subscription.then do
      listing_plans_ids = Marketplace::ListingPlan.where(marketplace_listing_id: listing_id).
          pluck(:id)
      active_subscription_items.for_marketplace_listing_plans(listing_plans_ids).first
    end
  end

  # Public: Returns a list of Organizations that have an active subscription to the Marketplace
  # listing with the given ID, and for which this User can administer those subscriptions.
  def billed_organizations_for_marketplace_listing(listing_id)
    async_billed_organizations_for_marketplace_listing(listing_id).sync
  end

  def async_billed_organizations_for_marketplace_listing(listing_id)
    if user?
      orgs = owned_organizations
      item_promises = orgs.map { |org| org.async_subscription_item_for_marketplace_listing(listing_id) }

      Promise.all(item_promises).then do
        orgs.select do |org|
          item = org.subscription_item_for_marketplace_listing(listing_id)
          item && item.adminable_by?(self)
        end
      end
    else
      Promise.resolve([])
    end
  end

  def pending_cycle
    Billing::PendingCycle.new(self)
  end

  # The changes that will occur on the users next billing date,
  # not including free trial changes
  def pending_cycle_change
    @pending_cycle_change ||= (pending_plan_changes.incomplete.not_past - pending_free_trial_changes).first
  end

  # Public: Returns the given users pending_cycle_change
  def async_pending_cycle_change
    async_pending_plan_changes.then do |pending_changes|
      incomplete_changes = pending_changes.reject(&:is_complete)

      Promise.all(incomplete_changes.map(&:async_pending_subscription_item_changes)).then do |changes_item_changes|
        first_non_free_trial_change = nil

        changes_item_changes.each_with_index do |item_changes, idx|
          if !item_changes.any?(&:free_trial)
            # This is the first incomplete change without any free trial items
            first_non_free_trial_change = incomplete_changes[idx]
            # since we found what we were looking for, get out
            break
          end
        end

        first_non_free_trial_change
      end
    end
  end

  def pending_free_trial_changes
    pending_plan_changes
       .joins(:pending_subscription_item_changes)
       .where(pending_subscription_item_changes: { free_trial: true })
  end

  # Should this user be disabled if it's not already?
  #
  # Returns a Boolean
  def should_disable?
    return false if never_disable?
    over_plan_limit? || (over_billing_attempts_limit? && dunning_period_expired?)
  end

  def never_disable?
    invoiced? || (paying_customer? && payment_amount == 0)
  end

  # Private: Returns if we're upgrading from an enterprise cloud trial or not.
  # If the trial is active we return true. We also return true if the trial is
  # expired and the user is on a free plan.
  #
  # Returns true if upgrading from a trial, otherwise false
  def upgrading_from_trial?
    trial = Billing::PlanTrial.find_by(user: self, plan: GitHub::Plan::BUSINESS_PLUS)

    trial&.active? || (plan.name == GitHub::Plan::FREE && trial.present?)
  end

  # Public: Returns true if we're an organization with an associated business,
  # and not in enterprise mode
  #
  # Returns true if we should delegate seat related things to the business
  def delegate_billing_to_business?
    organization? && !GitHub.single_business_environment? && async_business.sync.present?
  end

  # Public: checks if account is on free plan or undiscounted amount
  # is zero (which is the case on free trials, or free with free addons, etc.
  #
  # Returns Boolean
  def uncharged_account?
    plan&.free? || undiscounted_payment_amount.zero?
  end

  def charged_account?
    !uncharged_account?
  end

  # Public: Checks if the user is entitled to use Advanced Security.
  # On GHES, this can be found by looking at the license file.
  # On GHEC, delegate to the associated business. Only businesses can buy
  # Advanced Security, so if there is no business then the answer is false.
  #
  # Returns Boolean
  def advanced_security_enabled?
    return GitHub::Enterprise.license.advanced_security_enabled if GitHub.enterprise?
    delegate_billing_to_business? && business.advanced_security_enabled?
  end

  private

  # Internal: Schedule a job to synchronize this user's plan information
  # if there are changes that need to be synchronized.
  #
  # The job will be run after all database changes are committed. To run the
  # job immediately, use User#update_external_subscription!
  #
  # options - :force - Force the synchronizer to run if we know that the
  #                    plan subscription is out of date (default: false)
  def update_external_subscription(force: false)
    if external_subscription?
      if force || remote_subscription_needs_update?
        schedule_subscription_synchronization
      end
    end
  end

  # Internal: Schdule a job to create an external subscription if it does not
  #           present, or update an existing one.
  # See #update_external_subscription for update behavior
  def create_or_update_external_subscription(**kwargs)
    return update_external_subscription(**kwargs) if external_subscription?
    schedule_subscription_synchronization
  end

  # Internal: Enqueues an UpdateExternalCustomer job to update the customer
  # record in Braintree or Zuora.
  #
  # This happens after commit to ensure that when the job is picked up by the
  # worker, all of the changes processed in the enqueuing thread are saved.
  #
  # Returns nothing
  def run_scheduled_external_customer_update
    UpdateExternalCustomerJob.perform_later(self.customer) if @needs_external_customer_update
  end

  # Internal: Enqueues a SynchronizePlanSubscription job to synchronize the
  # customer's plan subscription with Braintree or Zuora.
  #
  # This happens after commit to ensure that when the job is picked up by the
  # worker, all of the changes processed in the enqueuing thread are saved.
  #
  # Returns nothing
  def run_scheduled_subscription_synchronization
    return unless @needs_subscription_synchronization
    SynchronizePlanSubscriptionJob.perform_later("user_id" => id)
  end

  # Internal: Mark the record as requiring a Braintree customer update, which
  # is handled in the UpdateExternalCustomer job
  #
  # Returns nothing
  def schedule_external_customer_update
    @needs_external_customer_update = true
  end

  # Internal: Mark the record as requiring a subscription synchronization,
  # which is handled by the SynchronizePlanSubscription job
  #
  # Returns nothing
  def schedule_subscription_synchronization
    @needs_subscription_synchronization = true
  end

  # Internal: Are there plan changes that need to be synchronized
  def remote_subscription_needs_update?
    (saved_change_to_plan? || saved_change_to_seats? || saved_change_to_plan_duration?) &&
      !Organization.transforming?(self)
  end

  # Internal: Should this user be transitioned to an external subscription?
  #           The transition itself has its own guards but this will explicitly
  #           prevent creating Pending Subscriptions if #recurring_charge is called.
  #
  # Returns a Boolean
  def should_transition_to_external_subscription?
    has_valid_payment_method? && past_due?
  end

  # Internal: Uncache user plan signup transaction in case this value has changed
  #
  # _transaction: Because this is called on `after_add` AR passes the added
  # transaction. We, however, don't need it.
  #
  # Returns a Boolean
  def clear_signup_transaction(_transaction)
    @plan_signup_transaction = nil
  end

  # Internal: Has the 2-week dunning period for this user elapsed?
  #
  # Returns a Boolean
  def dunning_period_expired?
    return true if never_successfully_billed?
    GitHub::Billing.today >= billed_on + DUNNING_DAYS
  end

  def first_time_charge?
    billing_transactions.first_time_charge.blank?
  end

  def signup_transaction
    transactions.create(user: self, action: "signed-up")
  end

  def delete_transaction
    transactions.create(action: "deleted")
  end

  def seat_count_no_greater_than_world_population
    if changing_seats? && seats > 10_000_000_000
      errors.add :seats, "must be no more than the total number of people on Planet Earth"
    end
  end

  def seat_count_greater_than_member_count
    if !delegate_billing_to_business? && changing_seats? && seats < filled_seats
      errors.add :seats, "must be at least the number of currently filled seats"
    end
  end

  def seat_count_greater_than_base_units
    if changing_seats? && seats < plan.base_units
      errors.add :seats, "must be at least #{plan.base_units}"
    end
  end

  def changing_seats?
    organization? && will_save_change_to_seats? && plan && plan.per_seat?
  end

  # Internal: Updates user account after a billing attempt succeeds.
  #
  # transaction - Billing::BillingTransaction, but can be another transaction
  #               object (eg, Billing::BraintreeTransaction, etc), which responds to id
  #               with a transaction id.
  # prorated   - Boolean if the transaction charge was prorated (eg, yearly plan
  #              upgrade).
  #
  # Returns nothing.
  def handle_successful_transaction(transaction, prorated)
    unless transaction.is_a?(Billing::BillingTransaction)
      transaction = Billing::BillingTransaction.find_by_transaction_id(transaction.id)
    end
    GitHub.dogstats.count("cream", transaction.amount_in_cents)

    move_billed_on(transaction) unless prorated
    self.billing_attempts = 0
    # We have to bypass validation, a user may be invalid (e.g. no email address),
    # but we don't want to re-charge them every day they are!
    save(validate: false)
    enable_or_disable!

    transaction.update_attribute :service_ends_at, billed_on

    BillingNotificationsMailer.receipt(self, transaction).deliver_later
    BillingNotificationsMailer.receipt_bcc(self, transaction).deliver_later if BillingNotificationsMailer.send_receipt_bcc?
  end

  # Private: Given a transaction, determine the date the user should be billed
  # next. Depending on the state of the user, we might need to start a new
  # billing cycle.
  #
  # transaction - a successful Billing::BillingTransaction whose date
  #               may be used to set the new billed_on
  def move_billed_on(transaction)
    return billed_on if braintree_subscription?

    self.billed_on = if start_new_billing_period?
       new_billed_on(transaction.date)
    else
       new_billed_on(billed_on)
    end
  end

  # Private: Boolean if we should start a brand new billing period for this
  # user. Generally users keep a standard billing cycle (we charge you every
  # month on the 4th, for example). We only start a new cycle if:
  #   - Your card fails for more than 14 days OR
  #   - You don't have a cycle for some reason (billed_on.blank?) OR
  #   - Your account gets disabled (staff locks, outside repo limits) OR
  #   - You've somehow ended up with an extremely old billing date
  #
  # Returns truthy if a new billing period should be started.
  def start_new_billing_period?
    over_billing_attempts_limit? || billed_on.blank? || disabled? ||
      billed_on < (GitHub::Billing.today - DUNNING_DAYS)
  end

  # Private: Handles coupon expiry and fully covered paid plans.
  #
  # Returns GitHub::Billing::Result
  def recurring_charge_with_coupon(amount_in_cents, charge_type)
    if !past_due?
      enable_or_disable!
      GitHub::Billing::Result.success
    elsif amount_in_cents.zero?
      process_zero_charge_transaction(charge_type)
    else
      Failbot.push \
        areas_of_responsibility: [:gitcoin],
        user: self
    end
  end

  def adminable_org_ids
    Ability.where(
      actor_id: id,
      actor_type: "User",
      subject_type: "Organization",
      action: Ability.actions[:admin],
    ).pluck(:subject_id)
  end
end
