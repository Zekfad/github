# frozen_string_literal: true

module User::PolicyDependency
  # Is the given user a GitHub employee that has opted out of the policies feature?
  #
  # user - A User
  #
  # Returns a Boolean.
  def opted_out_of_policy_staff_ship?
    employee? &&
      GitHub.flipper[:policies_feature_preview].enabled?(self) &&
      !self.beta_feature_enabled?(:policies)
  end
end
