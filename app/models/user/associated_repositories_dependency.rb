# frozen_string_literal: true

module User::AssociatedRepositoriesDependency
  # Check whether the given repository is associated with this user.
  def async_associated_repository?(repository, min_action: nil, including: nil, include_oauth_restriction: true, include_indirect_forks: true, include_oopfs: true, resource: nil)
    Platform::Loaders::AssociatedRepositoryCheck.load(self, repository.id,
      min_action: min_action,
      including: including,
      include_oauth_restriction: include_oauth_restriction,
      include_indirect_forks: include_indirect_forks,
      include_oopfs: include_oopfs,
      resource: resource,
    )
  end

  # Public: Return a list of repository IDs associated with this user.
  #
  # Includes repositories that are:
  #
  # :owned    - owned by the user, which includes both root repos and forks
  # :direct   - repositories where the user is a direct collaborator
  # :indirect - repositories where the user is a collaborator either via admin
  #             on an organization, or has access to the repo via a team
  #
  # Arguments:
  #
  # min_action: - An optional Symbol representing the minimum ability action
  #               (:read, :write, or :admin) that the user must have for the
  #               returned repositories. Default is nil, for no min_action
  #               restrictions.
  # including:  - An optional Array of Symbols representing the way that the
  #               user is associated with the returned repositories. Default is
  #               nil, which means "all". Valid values are :owned, :direct, and
  #               :indirect.
  # include_oauth_restriction: - Optional flag to enable or disable oauth
  #               application restrictions when querying for associated
  #               repositories. Defaults to true.
  # include_indirect_forks: - Optional flag to enable or disable the loading of
  #               user-owned forks of organization-owned repositories that the
  #               user has access to through their organization membership (such
  #               as through a user-owned fork being added to a team). Defaults
  #               to true
  # include_oopfs: - Optional flag to enable or disable the loading of
  #               organization-owned forks of private organization-owned
  #               repositories where the user is an owner of the root
  #               organization.
  # resource:     - An optional String to represent a child association.
  #               Accepted by this method for consistent calls with
  #               Bot#associated_repository_ids, but currently not used for Users.
  # repository_ids: An optional list of candidate repository ids. Only
  #                 repositories that are in this list and are associated with
  #                 the user will be returned.
  #
  # Returns an Array of Integer repository IDs.
  def associated_repository_ids(options = {})
    defaults = {
      min_action: nil,
      including: nil,
      include_oauth_restriction: true,
      include_indirect_forks: true,
      include_oopfs: true,
      resource: nil,
      repository_ids: nil,
    }
    options_with_defaults = options.reverse_merge(defaults)

    if options_with_defaults == defaults
      # Memoize the IDs if no options were overridden
      return @default_associated_repository_ids if defined?(@default_associated_repository_ids)
      @default_associated_repository_ids = fetch_associated_repository_ids(options_with_defaults)
    else
      # Don't memo-ize if options were overridden
      fetch_associated_repository_ids(options_with_defaults)
    end
  end

  private def fetch_associated_repository_ids(options)
    filter = AssociatedRepositories.new(self,
      **options.slice(:min_action, :including, :include_indirect_forks, :include_oopfs, :repository_ids),
    )

    if governed_by_oauth_application_policy? && options[:include_oauth_restriction]
      Repository.oauth_app_policy_approved_repository_ids(repository_ids: filter.ids, app: oauth_application)
    else
      filter.ids
    end
  end

  # Public: Return a list of repository IDs this site admin has unlocked.
  #
  # Returns an Array of Integer Repository IDs.
  def unlocked_repository_ids
    return [] unless can_unlock_repos?
    RepositoryUnlock.active_for_user(self).pluck(:repository_id)
  end

  # Public: Returns a symbol representing this User's relationship to the given Repository.
  #
  # repo - a Repository
  #
  # Returns one of :owner, :member, or :none.
  def relationship_to(repo)
    if repo.owner == self
      :owner
    elsif repo.member?(self)
      :member
    else
      :none
    end
  end

  # Internal: Filter and retrieve the set of repositories associated with a
  # user.
  class AssociatedRepositories
    VALID_INCLUDING_VALUES = [:owned, :direct, :indirect].freeze
    DEFAULT_INCLUDING_VALUES = VALID_INCLUDING_VALUES
    RUBY_INTERSECTION_THRESHOLD = 8_000

    attr_reader :user
    attr_reader :min_action
    attr_reader :including

    def initialize(user, repository_ids: nil, min_action: nil, including: nil, include_indirect_forks: true, include_oopfs: true)
      @user = user

      @repository_ids = repository_ids

      @min_action = min_action
      validate_min_action(@min_action)

      @including = including ? Array(including) : DEFAULT_INCLUDING_VALUES
      validate_including(@including)

      @include_indirect_forks = include_indirect_forks
      @include_oopfs = include_oopfs
    end

    def ids
      return [] if user.new_record?

      tags = {
        includes_repository_ids: !!@repository_ids,
        use_db_repository_filter: use_db_repository_filter?,
        many_repos: many_repos?,
      }.map { |k, v| "#{k}:#{v}" }

      GitHub.dogstats.time("associated_repository_ids.calculate_ids", tags: tags) { calculate_ids }
    end

    private

    def many_repos?
      !!(@repository_ids && @repository_ids.count > RUBY_INTERSECTION_THRESHOLD)
    end

    # Private: Returns a boolean of whether or not we should be filtering
    # repositories in the database
    def use_db_repository_filter?
      !!(@repository_ids && @repository_ids.count <= RUBY_INTERSECTION_THRESHOLD)
    end

    def calculate_ids
      return [] if @repository_ids && @repository_ids.empty?

      ids = []

      if include_repositories_with_direct_grants?
        ids.concat(repository_ids_with_direct_grants)
      end

      if include_repositories_with_indirect_grants?
        ids.concat(repository_ids_with_indirect_grants)
      end

      if include_repositories_owned_by_user?
        ids.concat(repository_ids_owned_by_user)
      end

      if include_org_owned_forks_of_private_org_owned_repos?
        ids.concat(repository_ids_for_org_owned_forks_of_private_org_owned_repos)
      end

      # Filter repository_ids in memory if we didn't filter via the database queries
      ids &= @repository_ids if @repository_ids.present? && !use_db_repository_filter?

      ids.sort.uniq
    end

    def repository_ids_with_direct_grants
      ActiveRecord::Base.connected_to(role: :reading) do
        sql = Ability.github_sql.new <<~SQL, actor_id: user.ability_delegate.ability_id, direct: Ability.priorities[:direct]
          SELECT ability.subject_id
          FROM   abilities AS ability
          WHERE ability.actor_id     = :actor_id
          AND   ability.actor_type   = 'User'
          AND   ability.subject_type = 'Repository'
          AND   ability.priority     = :direct
        SQL

        if use_db_repository_filter?
          sql.add "AND ability.subject_id IN :repository_ids", repository_ids: @repository_ids
        end

        if require_a_minimum_ability_action?
          sql.add "AND action >= :min_action", min_action: Ability.actions[min_action]
        end

        sql.values
      end
    end

    def repository_ids_with_indirect_grants
      ids = repository_ids_indirect_via_adminship | repository_ids_indirect_via_membership
      return ids if ids.empty? || include_indirect_forks?

      Repository.github_sql.values <<~SQL, ids: ids
        SELECT id
        FROM repositories
        WHERE id IN :ids
        AND   owner_id = organization_id
      SQL
    end

    def repository_ids_indirect_via_adminship
      if use_db_repository_filter?
        Repository.accessible_via_org_admin(user, @repository_ids)
      else
        # get a list of orgs user admins
        org_ids_user_admins = Ability.user_admin_on_organizations(
          actor_id: user.ability_delegate.ability_id,
        ).pluck(:subject_id)

        # get the repos those orgs own, assuming this includes forks
        Repository.where(active: true, organization_id: org_ids_user_admins).pluck(:id)
      end
    end

    def repository_ids_indirect_via_membership
      ActiveRecord::Base.connected_to(role: :reading) do
        sql = Ability.github_sql.new actor_id: user.ability_delegate.ability_id
        # NOTE: the grandparent subject_type and parent actor_type restrictions
        # being limited to valid connectors is an optimization. If any
        # additional User -> <Connector> -> Repository types are added, they need
        # to be added here.
        sql.add <<~SQL, direct: Ability.priorities[:direct], valid_connectors: %w(Team Organization)
          SELECT parent.subject_id
          FROM   abilities AS parent
          JOIN   abilities AS grandparent
          ON     grandparent.subject_type = parent.actor_type
          AND    grandparent.subject_id   = parent.actor_id
          WHERE  grandparent.actor_id     = :actor_id
          AND    grandparent.actor_type   = 'User'
          AND    grandparent.subject_type IN :valid_connectors
          AND    grandparent.priority     <= :direct
          AND    parent.actor_type        IN :valid_connectors
          AND    parent.subject_type      = 'Repository'
          AND    parent.priority          <= :direct
        SQL

        if use_db_repository_filter?
          sql.add "AND parent.subject_id IN :repository_ids", repository_ids: @repository_ids
        end

        if require_a_minimum_ability_action?
          sql.add "AND parent.action >= :min_action", min_action: Ability.actions[min_action]
        end

        sql.values
      end
    end

    def repository_ids_owned_by_user
      sql = Repository.github_sql.new <<~SQL, user_id: user.id
        SELECT id
        FROM repositories
        WHERE owner_id = :user_id
      SQL

      if use_db_repository_filter?
        sql.add "AND id IN :repository_ids", repository_ids: @repository_ids
      end

      sql.values
    end

    def repository_ids_for_org_owned_forks_of_private_org_owned_repos
      organization_ids =
        ActiveRecord::Base.connected_to(role: :reading) do
          sql = Ability.github_sql.new <<~SQL, user_id: user.id, admin: Ability.actions[:admin], indirect: Ability.priorities[:indirect]
            SELECT ab.subject_id
            FROM abilities ab
            WHERE  ab.actor_type = 'User'
            AND    ab.actor_id = :user_id
            AND    ab.priority <> :indirect
            AND    ab.action = :admin
            AND    ab.subject_type = 'Organization'
            /* abilities-select-no-priority-needed */
          SQL

          sql.values
        end

      return [] if organization_ids.empty?

      sql = Repository.github_sql.new <<~SQL, organization_ids: organization_ids
        SELECT `repositories`.`id`
        FROM `repositories`
        FORCE INDEX(index_repositories_on_source_id_and_organization_id)
        INNER JOIN `repositories` AS `root` ON `root`.source_id = repositories.source_id
        WHERE root.parent_id IS NULL
        AND   root.public = 0
        AND   root.owner_id IN :organization_ids
        AND   repositories.organization_id <> root.owner_id
      SQL

      if use_db_repository_filter?
        sql.add "AND `repositories`.`id` IN :repository_ids", repository_ids: @repository_ids
      end

      sql.values
    end

    def require_a_minimum_ability_action?
      !@min_action.nil?
    end

    def include_repositories_owned_by_user?
      including.include?(:owned)
    end

    def include_repositories_with_direct_grants?
      including.include?(:direct)
    end

    def include_repositories_with_indirect_grants?
      including.include?(:indirect)
    end

    def include_indirect_forks?
      @include_indirect_forks
    end

    # Include org-owned forks of private org repos where the user has admin
    # on the root organization? This matches pullable_by permissions checks.
    def include_org_owned_forks_of_private_org_owned_repos?
      @include_oopfs &&
        include_repositories_with_indirect_grants? &&
        include_indirect_forks? &&
        (!require_a_minimum_ability_action? || min_action == :read)
    end

    # Will the query return no results? This can happen if the includes option
    # is explicitly an empty array.
    def nothing_to_select?
      !(include_repositories_with_direct_grants? || include_repositories_with_indirect_grants?) &&
      !include_repositories_with_indirect_grants? &&
      !include_repositories_owned_by_user? &&
      !include_org_owned_forks_of_private_org_owned_repos?
    end

    def validate_min_action(value)
      return unless value

      unless Ability.actions.include?(value)
        raise ArgumentError, "Invalid min_action: #{min_action}"
      end
    end

    def validate_including(values)
      invalid_values = values - VALID_INCLUDING_VALUES
      if invalid_values.any?
        raise ArgumentError, "Invalid including values: #{invalid_values}"
      end
    end
  end
end
