# frozen_string_literal: true

# Codespace-specific functionality for users.
module User::CodespacesDependency

  # Public: Enable Workspaces for the user.
  def enable_codespaces
    GitHub.flipper[:workspaces].enable(self)
  end

  # Public: Disable Workspaces for the user.
  def disable_codespaces
    GitHub.flipper[:workspaces].disable(self)
  end

  # Public: Onboard the user into the beta.
  #
  # If they have an early access membership we'll send them a nice welcome
  # email and instrument their joining.
  def onboard_into_codespaces_beta!
    enable_codespaces

    if membership = EarlyAccessMembership.workspaces_waitlist.find_by(member: self)
      CodespacesOnboardWaitlistUserJob.set(wait: 5.minutes).perform_later(membership)

      GlobalInstrumenter.instrument("user.beta_feature.enroll",
        actor: self,
        action: "enroll",
        feature: "workspaces",
      )
    end
  end

  # Public: Offboard the user from the beta.
  #
  # If they have an early access membership we'll instrument their offboarding.
  def offboard_from_codespaces_beta!
    disable_codespaces

    if EarlyAccessMembership.workspaces_waitlist.exists?(member: self)
      GlobalInstrumenter.instrument("user.beta_feature.unenroll",
        actor: self,
        action: "unenroll",
        feature: "workspaces",
      )
    end
  end

  def at_codespace_limit?
    accessible_codespaces.count >= GitHub.codespaces_per_user_limit
  end

  def accessible_codespaces
    codespaces.visible_to(self)
  end

  def deiframed_flow_enabled?
    GitHub.flipper[:codespaces_deiframed_flow].enabled?(self)
  end
end
