# rubocop:disable Style/FrozenStringLiteralComment

module User::RolesDependency
  extend ActiveSupport::Concern

  included do
    has_many :user_stafftools_roles, dependent: :destroy
    has_many :stafftools_roles, through: :user_stafftools_roles
  end

  # Can this user make and edit blog posts?  Staff, support and blogger roles
  # have this power.
  def blogger?
    employee?
  end

  # Is this user a participant in the GitHub Security Bug Bounty. These users
  # get a special profile badge and may be given early access to features.
  def bounty_hunter?
    !GitHub.enterprise? && team_access?(:bounty_hunters)
  end

  # Is this user a participant in the GitHub Campus Experts program.
  # These users get a special profile badge.
  def campus_expert?
    !GitHub.enterprise? && team_access?(:campus_experts_badge)
  end

  # NB: Whenever a GitHub Employee is hired, we revoke their OAuth applications
  # and SSH keys so that 3rd parties don't inadvertently get access to GitHub code.
  def revoke_new_hire_accesses
    async_revoke_oauth_tokens(:all)
    public_keys.each { |key| key.unverify(:new_hire) }
  end

  # Is this user on the @github/employees team? This should be used for
  # behaviors that need to be available to all GitHub staff. User#staff?
  # should be used for administrative actions that not all staff need access
  # to.
  def employee?
    return @is_employee if defined?(@is_employee)
    @is_employee = !GitHub.enterprise? && (team_access?(:employees) || team_access?(:interns))
  end

  # Used to clear the memo'ized employee status; useful if you need to add
  # add someone as an employee but have already queried if they are.
  #
  # Note: We really should figure out a better way to change this during
  #       employee membership addition.
  def clear_employee_memo
    remove_instance_variable(:@is_employee) if defined?(@is_employee)
  end

  def biztools_user?
    GitHub.billing_enabled? && gh_role == "biz"
  end

  # Let the user pretend not to be an employee. Will cause #employee? to return
  # false.
  #
  # Returns nothing.
  def disable_employee_mode
    @is_employee = false
    @disabled_employee_mode = true
  end

  # Public: Is the user an employee who's disabled employee mode?
  def disabled_employee_mode?
    !!@disabled_employee_mode
  end

  attr_accessor :site_admin

  # Is this user a member of staff? Can be set to false using the accessor to
  # temporarily lower a user's status. Staff users include both github.com staff
  # members but also admin level users on Enterprise instances.
  def site_admin?
    user? && site_admin_without_two_factor_check? &&
      (!GitHub.require_two_factor_for_site_admin? || two_factor_authentication_enabled?)
  end

  # Working for GitHub isn't the same as getting access to staff features.
  # - For GitHub, this is defined as an employee in the staff role who also
  #   has access to the employees team.
  # - For GHE, this is a user which is explicitely a site_admin
  def site_admin_without_two_factor_check?
    (!GitHub.require_employee_for_site_admin? || employee?) &&
    gh_role == "staff" &&
    (@site_admin.nil? || @site_admin)
  end

  # Does this GitHubber need access to devtools?
  def github_developer?
    GitHub.devtools_enabled? && gh_role == "dev"
  end

  # Catch usage of legacy User#staff? method.
  def staff?
    site_admin?
  end

  # Catch usage of legacy User#staff= method.
  def staff=(value)
    site_admin = value
  end

  # Determines whether the user can unlock private repos.
  def can_unlock_repos?
    return false unless site_admin?
    return true if GitHub.enterprise?

    stafftools_action = { controller: "Stafftools::Repositories::StaffAccessController", action: "unlock" }
    Stafftools::AccessControl.authorized?(self, stafftools_action)
  end

  # Determines whether the user can unlock private repos, without requesting permission from the owner.
  # This privilege exists only for security & legal purposes.
  def can_unlock_repos_without_owners_permission?
    return false unless site_admin?
    return true if GitHub.enterprise?

    stafftools_action = { controller: "Stafftools::Repositories::StaffAccessController", action: "override_unlock" }
    Stafftools::AccessControl.authorized?(self, stafftools_action)
  end

  # Determines whether the user can unlock fake login as a user.
  def can_fake_login?
    return false unless site_admin?
    return true if GitHub.enterprise?

    stafftools_action = { controller: "Stafftools::SessionsController", action: "impersonate" }
    Stafftools::AccessControl.authorized?(self, stafftools_action)
  end

  # Determines whether the user can impersonate users without an active staff access grant.
  def can_impersonate_users_without_permission?
    return false unless site_admin?
    return false unless access_grant_required_for_user_impersonation?

    stafftools_action = { controller: "Stafftools::SessionsController", action: "override_impersonate" }
    Stafftools::AccessControl.authorized?(self, stafftools_action)
  end

  def access_grant_required_for_user_impersonation?
    !GitHub.enterprise?
  end

  # Determines whether the user can manage Marketplace listings.
  # Users with site admin or biztools access are allowed.
  #
  def can_admin_marketplace_listings?
    site_admin? || biztools_user?
  end

  # Determines whether the user can manage Works with GitHub listings.
  # Users with site admin or biztools access are allowed.
  #
  def can_admin_non_marketplace_listings?
    site_admin? || biztools_user?
  end

  # Determines whether the user can manage Sponsors listings
  # Users with site admin or biztools access are allowed.
  #
  def can_admin_sponsors_listings?
    site_admin? || biztools_user?
  end

  # Determines whether the user can manage Sponsors membership
  # Users with site admin or biztools access are allowed.
  #
  def can_admin_sponsors_memberships?
    site_admin? || biztools_user?
  end

  # Determines whether the user can manage Sponsors newsletter
  # Users with site admin or biztools access are allowed.
  #
  def can_admin_sponsors_newsletters?
    site_admin? || biztools_user?
  end

  # Determines whether the user can manage Repository Actions
  # Users with site admin or biztools access are allowed.
  #
  def can_admin_repository_actions?
    site_admin? || biztools_user?
  end

  def can_become_staff?
    !(GitHub.require_employee_for_site_admin? && !employee?)
  end

  # Grant the user site-admin permissions.
  #
  # for .com, this user must be a member of the employees group in order to
  # prevent improper access being granted.
  #
  # reason - a String explanation for why/how the user was given access. Required.
  #
  # Returns false if no reason was provided, true otherwise.
  def grant_site_admin_access(reason)
    return false unless can_become_staff?
    change_role("staff", reason, :promote)
  end

  # Grant the user github-developer permissions.
  #
  # reason - a String explanation for why/how the user was given access. Required.
  #
  # Returns false if no reason was provided, true otherwise.
  def grant_github_developer_access(reason)
    raise "devtools disabled" unless GitHub.devtools_enabled?
    change_role("dev", reason, :grant_github_developer)
  end

  # Grant the user biztools permissions.
  #
  # reason - a String explanation for why/how the user was given access. Required.
  #
  # Returns false if no reason was provided, true otherwise.
  def grant_github_biztools_access(reason)
    raise "billing disabled" unless GitHub.billing_enabled?
    change_role("biz", reason, :grant_biztools_user)
  end

  # Revoke the user's site-admin, biztools, or github-developer permissions.
  #
  # reason - a String explanation for why/how the access was revoked. Required.
  #
  # Returns false if no reason was provided, true otherwise.
  def revoke_privileged_access(reason)
    roles_removed = change_role(nil, reason, :demote)
    # Rmove all the stafftools actions/controller access control rules for this user.
    stafftools_roles.destroy_all if roles_removed
    roles_removed
  end

  # Updates the user's role (site admin, developer).
  #
  # role   - The new role to give the user.
  # reason - A String explanation for why/how the user was updated. Required.
  # action - The action to instrument (grant_site_admin).
  #
  # Returns false if no reason was provided, true otherwise.
  def change_role(role, reason, action)
    return false if reason.blank?

    unless self.nil? || self.profile.nil? || !(action == :demote)
      self.profile.display_staff_badge = false
      self.profile.save!
    end

    update_attribute(:gh_role, role)
    instrument(action, reason: reason)
    true
  end

  # Unlock a repository.
  #
  # repo - the repository to be unlocked
  # reason - internal reason for the unlock -- if none given, the reason will be pulled
  # => from the active staff access grant
  #
  # Returns the RepositoryUnlock object if successful, false if not.
  def unlock_repository(repo, reason = nil)
    if can_unlock_repos?
      if grant = repo.active_staff_access_grant
        clear_cached_unlocked_repository(repo)
        return RepositoryUnlock.create_unlock(self, repo, grant.reason, grant)
      elsif GitHub.enterprise?
        clear_cached_unlocked_repository(repo)
        return RepositoryUnlock.create_unlock(self, repo, reason)
      end
    end

    false
  end

  # Has this user unlocked a given repository?
  #
  # Returns a boolean
  def has_unlocked_repository?(repo)
    async_has_unlocked_repository?(repo).sync
  end

  def async_has_unlocked_repository?(repo)
    unless defined?(@async_has_unlocked_repository)
      @async_has_unlocked_repository = Hash.new do |hash, repo|
        hash[repo] = Platform::Loaders::UnlockedRepositoryCheck.load(self, repo.id)
      end
    end

    @async_has_unlocked_repository[repo]
  end


  # The class name persisted as `target_type` when a UserRole is created
  # with this object as target.
  def user_role_target_type
    "User"
  end

  private

  def clear_cached_unlocked_repository(repo)
    @async_has_unlocked_repository.delete(repo) if defined?(@async_has_unlocked_repository)
  end
end
