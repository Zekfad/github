# rubocop:disable Style/FrozenStringLiteralComment

# See https://githubber.com/article/technology/dotcom/feature-flags
class User
  module FeatureFlagMethods
    # A list of public beta features that a user can opt into and out of.
    ELIGIBLE_BETA_FEATURES = [
      "windrose_disabled",
      "collaborative_editing",
      "gist_playground",
    ].freeze

    # Internal: Get a key to represent the user with the given beta feature.
    #
    # feature - String or Symbol feature flag name
    #
    # Returns a String.
    private def beta_feature_flag_key(feature)
      "feature_opt_in:#{feature}:#{flipper_id}"
    end

    # Private: Determine if the given feature is a beta users can opt into.
    #
    # feature - String or Symbol feature flag name
    #
    # Returns a Boolean.
    private def is_beta_feature?(feature)
      ELIGIBLE_BETA_FEATURES.include?(feature.to_s)
    end

    private def is_v2_beta_feature?(feature)
      feature.is_a?(Feature) || Feature.find_by(slug: feature.to_s)
    end

    def enhanced_employee_password_requirements?
      (employee? || staff?) &&
        GitHub.flipper[:enhanced_employee_password_requirements].enabled?(self) &&
        !GitHub.flipper[:enhanced_employee_password_requirements_opt_out].enabled?(self)
    end

    # Public: Determine if features like device verification challenges and
    # unrecognized sign in location alerts. The goal is to entirely bypass
    # writes while reducing reads of AuthenticationRecords and
    # AuthenticatedDevices.
    def sign_in_analysis_enabled?
      GitHub.sign_in_analysis_enabled? &&
        !GitHub.flipper[:verified_device_enforcement_opt_out].enabled?(self)
    end

    # Public: Enable the given beta feature for this user.
    #
    # feature - String or Symbol feature flag name
    #
    # Returns a Boolean: true if the user was added to the feature, false if the feature
    # is not an eligible beta feature.
    def enable_beta_feature(feature, skip_instrumentation: false)
      result = if is_v2_beta_feature?(feature)
        # Use new Feature and FeatureEnrollment models for all future
        # enrollments of this feature
        enable_edge_feature(feature)
      elsif is_beta_feature?(feature)
        GitHub.kv.set(beta_feature_flag_key(feature), "true")
        GlobalInstrumenter.instrument("user.beta_feature.enroll",
          actor: self,
          action: "enroll",
          feature: feature,
        ) unless skip_instrumentation
        true
      else
        false
      end

      reset_beta_feature_memoization(feature)
      result
    end

    # Public: Disable the given beta feature for this user.
    #
    # feature - String or Symbol feature flag name
    #
    # Returns nothing.
    def disable_beta_feature(feature, skip_instrumentation: false)
      result = if is_v2_beta_feature?(feature)
        disable_edge_feature(feature)
      elsif is_beta_feature?(feature)
        GitHub.kv.del(beta_feature_flag_key(feature))
        GlobalInstrumenter.instrument("user.beta_feature.unenroll",
          actor: self,
          action: "unenroll",
          feature: feature,
        ) unless skip_instrumentation
        true
      else
        false
      end

      reset_beta_feature_memoization(feature)
      result
    end

    # Public: Check if this user has opted into a beta feature.
    #
    # feature - String or Symbol feature flag name
    #
    # Returns a Boolean: true if the feature is a beta feature and the user is in it.
    def beta_feature_enabled?(feature)
      return @enabled_beta_features[feature] if defined?(@enabled_beta_features)

      @enabled_beta_features = Hash.new do |hash, feature|
        value = if v2_feature = is_v2_beta_feature?(feature)
          v2_beta_feature_enabled?(v2_feature)
        elsif is_beta_feature?(feature)
          v1_beta_feature_enabled?(feature)
        else
          false
        end

        hash[feature.to_s] = value
      end

      @enabled_beta_features[feature.to_s]
    end

    private def v1_beta_feature_enabled?(feature)
      !!GitHub.kv.exists(beta_feature_flag_key(feature)).value!
    end

    # TODO: Simplify once no further fallbacks are anticipated
    private def v2_beta_feature_enabled?(feature)
      return false unless feature&.viewer_can_read?(self)

      if enrollment = feature.enrollments.find_by(enrollee: self)
        # User has set an explicit preference
        enrollment.enrolled?
      elsif is_beta_feature?(feature.to_s)
        # Fallback for v1 settings that haven't been migrated
        v1_beta_feature_enabled?(feature.to_s) || feature.enrolled_by_default?
      else
        # We know nothing about this user's preference so we
        # use the default.
        feature.enrolled_by_default?
      end
    end

    private def reset_beta_feature_memoization(key = nil)
      return unless defined?(@enabled_beta_features)

      if key
        @enabled_beta_features.delete(key.to_s)
      else
        remove_instance_variable(:@enabled_beta_features)
      end
    end

    # Does this user have access to staff-only features on github.com? Make sure
    # to use this method instead of `User#site_admin?` for early access github.com
    # features, otherwise these features will be visible to Enterprise admins.
    #
    # Returns a Boolean.
    def preview_features?
      return unless GitHub.preview_features_enabled?
      if GitHub.enterprise?
        enterprise_preview_features?
      else
        employee?
      end
    end

    # Does this user have access to preview features on Enterprise? This method
    # should be used for early access to Enterprise features that should not
    # be visible to Enterprise admins (customers) just yet.
    #
    # Returns a Boolean.
    def enterprise_preview_features?
      GitHub.enterprise? && enterprise_preview_features_team?
    end

    # Does this user have access to some open-source maintainers-relevant
    # early access features on GitHub.com?
    #
    # Returns a Boolean.
    def maintainers_early_access?
      !GitHub.enterprise? && maintainers_early_access_team?
    end

    # Does this user have access to integrators early access features?
    #
    # Returns a Boolean.
    def integrators_early_access?
      !GitHub.enterprise? && integrators_early_access_team?
    end

    # Should this user see prerelease badges for features they have early access to
    # Currently only visible to the open source maintainers EAP and staff
    #
    # Returns boolean
    def prerelease_badges?
      !GitHub.enterprise? && (maintainers_early_access? || preview_features?)
    end

    # Returns whether the new exact match search UI is enabled for this user directly or
    # via an organization. Note this does not check enrollment in the beta feature of the
    # same name. Note also that this does not check whether a repository is indexed by EMS
    # nor whether EMS is offline.
    def exact_match_search_enabled?
      @exact_match_search_enabled ||= begin
        return false if GitHub.enterprise?
        GitHub.flipper[:exact_match_search].enabled?(self) ||
          self.organizations.detect do |org|
            GitHub.flipper[:exact_match_search].enabled?(org)
          end
      end
    end

    def abilities_team_enabled?
      preview_features? && abilities_team?
    end

    # New security feature to require callback url registration and validation.
    def oauth_registered_callback_urls_enabled?
      GitHub.flipper[:oauth_multiple_callback_urls].enabled?(self)
    end

    # Internal: Is this an abilibuddy?
    def abilities_team?
      team_access? :abilities
    end

    # Internal: Is this user a stafftooler
    def stafftools_team?
      team_access? :stafftools
    end

    def user_security_team?
      team_access? :user_security
    end

    # Does this user have "operator mode" as it pertains to git pushes?  When
    # set, additional log info is echoed back to users' terminals including
    # timings of custom pre-receive hook messages.  Enabled only for GHE
    # because this runs for every git operation, and `site_admin?` uses
    # expensive queries.
    def has_operator_mode?(conf)
      GitHub.enterprise? && site_admin? && ![nil, "false"].include?(conf["operator_mode"])
    end

    def enterprise_preview_features_team?
      team_access? :enterprise_preview_features
    end

    def maintainers_early_access_team?
      team_access? :maintainers_early_access
    end

    def integrators_early_access_team?
      team_access? :integrators_early_access
    end

    def security_incident_response_access?
      team_access?(:platform_health) || team_access?(:sirt)
    end

    def integrations_directory_enabled?
      return false if GitHub.enterprise?
      GitHub.flipper[:integrations_directory].enabled?(self)
    end

    # Is codesearch disabled for the current user? This feature flag is used to
    # block users that are negatively impacting the Elasticsearch cluster.
    #
    # see https://github.com/devtools/feature_flags/disable_codesearch
    #
    # Returns a Boolean.
    def codesearch_disabled?
      GitHub.flipper[:disable_codesearch].enabled?(self)
    end

    # Internal: Checks if the user is a member of a team.
    #
    # team - Symbol name for a Team that matches a FeatureFlagMethods class
    #        method that loads a team.  `:early_access` will check against
    #        FeatureFlagMethods.early_access_team.
    #
    # Returns a Boolean.
    def team_access?(team)
      GitHub::FeatureFlag.user_team_access?(team, self)
    end

    # Public: Check whether this user should see billing for codespaces and have their subscription synchronized with the codespaces rate plan
    #
    # Returns Boolean
    def billing_github_codespaces_enabled?
      if organization?
        GitHub.flipper[:billing_github_codespaces_for_orgs].enabled?(self)
      else
        GitHub.flipper[:billing_github_codespaces].enabled?(self)
      end
    end

    # Internal: Does the user have access to a feature based on a pattern contained in their email address?
    # See app/models/user/email_suffix_actor.rb for more details on this method of enabling features.
    #
    # Returns a Boolean.
    def feature_enabled_via_email_suffix?(feature_name)
      GitHub.flipper[feature_name].enabled?(User::EmailSuffixActor.from_user(self))
    end

    # Public: Check if Workspaces is enabled for the user.
    #
    # Returns Boolean
    def workspaces_enabled?
      GitHub.flipper[:workspaces].enabled?(self)
    end
  end
end
