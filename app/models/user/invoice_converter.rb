# frozen_string_literal: true

class User
  class InvoiceConverter
    def initialize(target)
      @target = target
    end

    # Public: Determines if target user/organization can be converted to
    # invoice billing.
    #
    # If an organization is owned by a business its `invoiced?` method will
    # always return true. This means we have to check business owned
    # organization's `billing_type` value directly to determine if it has
    # already been migrated to invoice billing or not.
    #
    # Returns true if target is invoiced, otherwise false
    def convertable?
      !target.invoiced? || needs_business_billing_migration?
    end

    # Public: Switches target user/organization to invoice billing
    #
    # actor - User taking this action
    #
    # Returns truthy if successful
    def convert(actor:)
      return false unless convertable?

      # Cancel the external subscription before updating plan duration to
      # ensure no charges are made on the subscription
      target.plan_subscription&.cancel_external_subscription

      target.remove_all_payment_methods(actor)

      target.incomplete_pending_plan_changes.each(&:cancel)

      target.update!(billing_type: User::INVOICE_BILLING_TYPE, billing_attempts: 0, plan_duration: User::YEARLY_PLAN).tap do
        instrument(actor: actor)
        deactivate_trial
      end
    end

    private

    attr_reader :target

    # Private: This instrument call sends data to Hydro. There is a similar
    # instrumentation in StaffTools::UsersController#pay_by_invoice
    # that logs to the Audit Log, but can't be used with Hydro
    def instrument(actor:)
      GlobalInstrumenter.instrument(
        "billing.change_billing_type",
        old_billing_type: target.attribute_before_last_save(:billing_type),
        billing_type: target.billing_type,
        user: target,
        actor: actor,
      )
    end

    # Private: Returns whether or not the current organization needs to have
    # the switch_billing_type_to_invoice code run when owned by a business.
    #
    # Returns true if the current organization is owned by a business and is
    # not invoiced, otherwise false
    def needs_business_billing_migration?
      return false if !target.organization?
      return false if target.business.nil?

      # invoiced? always returns true when owned by a business so we check the
      # billing type manually to see if a migration is necessary
      target.billing_type != User::INVOICE_BILLING_TYPE
    end

    # Private: Deactivates an Enterprise Cloud trial if one exists and is
    # active. Because the billing is now managed by sales we shouldn't run the
    # pending_plan_change and their terms should go back to CToS
    #
    # Returns nothing
    def deactivate_trial
      trial = Billing::EnterpriseCloudTrial.new(target)

      if trial.active?
        trial.deactivate!
      end
    end
  end
end
