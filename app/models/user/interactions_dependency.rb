# rubocop:disable Style/FrozenStringLiteralComment

module User::InteractionsDependency

  # Generates the set of Issue IDs that the user has interacted with in some
  # fashion. The user is either the author of the issue or they have commented
  # on the issue.
  #
  # Returns a Set of Issue IDs.
  def interacted_issue_ids
    queries = []

    queries <<
       "(    SELECT issues.id FROM issues
              WHERE issues.pull_request_id IS NULL
                AND issues.user_id = #{self.id})"

    queries <<
       "(    SELECT issues.id FROM issues
         INNER JOIN issue_comments ON issue_comments.issue_id = issues.id
              WHERE issues.pull_request_id IS NULL
                AND issue_comments.user_id = #{self.id})"

    ary = Issue.connection.select_values queries.join("\nUNION ALL\n")
    Set.new ary
  end

  # Generates the set of PullRequest IDs that the user has interacted with in
  # some fashion. The user is either the author of the pull request or they
  # have commented on the pull request. The comment can be either a review
  # comment or a normal issue-style comment.
  #
  # Returns a Set of PullRequest IDs.
  def interacted_pull_request_ids
    queries = []

    # the user is the author
    queries <<
       "(    SELECT pull_requests.id FROM pull_requests
              WHERE pull_requests.user_id = #{self.id})"

    # the user has a review comment on the pull request
    queries <<
       "(    SELECT pull_request_review_comments.pull_request_id FROM pull_request_review_comments
              WHERE pull_request_review_comments.user_id = #{self.id})"

    # the user has a normal comment on the pull request
    queries <<
       "(    SELECT issues.pull_request_id FROM issues
         INNER JOIN issue_comments ON issue_comments.issue_id = issues.id
              WHERE issues.pull_request_id IS NOT NULL
                AND issue_comments.user_id = #{self.id})"

    ary = Issue.connection.select_values queries.join("\nUNION ALL\n")
    Set.new ary
  end
end
