# frozen_string_literal: true

module User::OnboardingDependency
  BEGINNER_ANSWERS = [
    "None—I don't program at all",
    "New to programming",
  ]

  INTERMEDIATE_ANSWERS = [
    "Somewhat experienced",
    "Very experienced",
  ]

  # Onboarding milestones for users.

  def beginner_level_experience?
    programming_experience.in?(BEGINNER_ANSWERS)
  end

  def intermediate_level_experience?
    programming_experience.in?(INTERMEDIATE_ANSWERS)
  end

  def programming_experience
    return unless Onboarding.for(self).answered_user_identification_questions?

    SurveyQuestion.find_by(short_text: "level_of_experience")&.tap do |question|
      SurveyAnswer.for(self).find_by(question: question)&.tap do |answer|
        return answer.choice&.text
      end
    end
  end
end
