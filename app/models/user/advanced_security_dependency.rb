# frozen_string_literal: true

module User::AdvancedSecurityDependency
  def advanced_security_public_beta_enabled?
    GitHub.flipper[:advanced_security_public_beta].enabled?(self)
  end

  def enable_advanced_security_public_beta
    if !advanced_security_public_beta_enabled?
      GitHub.flipper[:advanced_security_public_beta].enable(self)
      onboard_advanced_security_beta
    end
  end

  def disable_advanced_security_public_beta
    GitHub.flipper[:advanced_security_public_beta].disable(self)
  end

  def advanced_security_private_beta_enabled?
    GitHub.flipper[:advanced_security_private_beta].enabled?(self)
  end

  def enable_advanced_security_private_beta
    if !advanced_security_private_beta_enabled?
      GitHub.flipper[:advanced_security_private_beta].enable(self)
      onboard_advanced_security_beta
    end
  end

  def disable_advanced_security_private_beta
    GitHub.flipper[:advanced_security_private_beta].disable(self)
  end

  private

  def onboard_advanced_security_beta
    membership = EarlyAccessMembership.code_scanning_waitlist.find_by!(member: self)
    OnboardAdvancedSecurityBetaJob.set(wait: 5.minutes).perform_later(membership_id: membership.id)
  end
end
