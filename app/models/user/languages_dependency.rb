# frozen_string_literal: true

module User::LanguagesDependency

  # Get the user's primary language from database cache,
  # or update it on the fly if not found.
  #
  # Returns primary language name as a String.
  def primary_language
    return @primary_language unless @primary_language.blank?

    language_id = self.primary_language_name_id
    @primary_language = language_id.present? ? LanguageName.find_by_id(language_id) : calculate_primary_language!
  end

  # Public: Returns the most common language from the set of public,
  # active repositories that the user owns.
  #
  # Returns a LanguageName record, nil otherwise.
  def calculate_primary_language!
    return unless id
    primary_lang_name_id = Repository.github_sql.value <<-SQL, user_id: id
      SELECT primary_language_name_id, count(*) as total FROM repositories
      WHERE repositories.owner_id = :user_id
        AND repositories.public = 1
        AND repositories.active = 1
        AND primary_language_name_id IS NOT NULL
      GROUP BY primary_language_name_id
      ORDER BY total DESC
      LIMIT 1
    SQL
    @primary_language = LanguageName.find(primary_lang_name_id) if primary_lang_name_id

    if @primary_language
      ActiveRecord::Base.connected_to(role: :writing) do
        self.update_column(:primary_language_name_id, @primary_language.id)
      end
    end

    @primary_language
  end

  def starred_repositories_by_language(limit = nil)
    repo_language_breakdown(visible_starred_repositories, limit)
  end

  def starred_public_repositories_by_language(limit = nil)
    repo_language_breakdown(starred_repositories.public_scope, limit)
  end

  def repo_language_breakdown(repos, limit)
    ActiveRecord::Base.connected_to(role: :reading) do
      # Returns an Hash of { repo.primary_language_name_id => total, ... }
      repo_primary_language_name_ids_and_totals =
        repos.
        where("primary_language_name_id is NOT NULL").
        group(:primary_language_name_id).
        order(count_all: :desc).
        limit(limit).
        count

      language_name_ids = repo_primary_language_name_ids_and_totals.map(&:first)
      language_names_by_id = LanguageName.ids_and_names(language_name_ids)

      language_names_by_id.map do |id, name|
        total = repo_primary_language_name_ids_and_totals[id]
        [name, total]
      end.to_h
    end
  end

end
