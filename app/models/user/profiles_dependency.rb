# frozen_string_literal: true

module User::ProfilesDependency

  ACV_CONTRIBUTED_REPOSITORIES_LIMIT = 3
  ACV_TOOLTIP = "acv_tooltip"

  # Public: Generates list of users followed by the viewer that also
  # follow the current user
  #
  # viewer - Viewer user object
  # limit - How many followers to return
  #
  # Returns list of followers
  def mutual_followers(viewer:, order_by: nil, limit: 5)
    user_followings = Following.for(self, viewer: viewer, type: :follower)
    viewer_followings = Following.for(viewer, viewer: viewer, type: :followed_user)

    scope = user_followings.where(user_id: viewer_followings.pluck(:following_id)).limit(limit)
    scope = scope.order("followers.created_at #{order_by[:direction]}") if order_by.present? && order_by[:field] == "followed_at"

    return User.none unless scope.present?
    id_sort = Arel.sql("field(id, #{scope.pluck(:user_id).join(',')})") # Maintain order passed in by ids from followings
    User.where(id: scope.pluck(:user_id)).order(id_sort, "id")
  end

  def following_users_count
    return @following_users_count if defined?(@following_users_count)

    @following_users_count = Following.for(
      self,
      order_by: {field: "followed_at", direction: "DESC"},
      type: :followed_user,
    ).size
  end

  def starred_repos_count
    return @starred_repos_count if defined?(@starred_repos_count)

    repository_scope = Repository.filter_spam_and_disabled_for(self).public_or_accessible_by(self)
    starred_repository_ids = stars.repositories.pluck(:starrable_id)

    @starred_repos_count = starred_repository_ids.each_slice(10_000).sum do |repository_ids|
      repository_scope.with_ids(repository_ids).count
    end
  end

  def can_have_acv_badge?
    acv_contribution_count > 0
  end

  # Public: returns the number of repositories archived in
  # the Arctic Code Vault that the user has contributed to, filtered down to
  # those that are still public and have not opted out of the GH Archive
  # Program. Use this to quickly get the count without repo information.
  def acv_contribution_count
    @acv_contribution_count ||= acv_repositories_scope.count
  end

  def all_emails
    @all_emails ||= self.emails.verified.pluck(:email) <<
                    # Include a normalized form of the modern stealth email format to ignore
                    # the user's login, which may or may not have been renamed. See the
                    # repository contribution graph model for prior art.
                    "#{self.id}+username@#{GitHub.stealth_email_host_name}" <<
                    # Include the legacy stealth email format in the hopes that users with
                    # older contributions to the Arctic Code Vault may not have renamed their
                    # user login since then
                    StealthEmail.new(self).legacy_email
  end

  # Private: returns the full unfiltered list of the repository IDs archived in
  # the Arctic Code Vault that the user has contributed to
  def all_acv_repository_ids
    return @all_acv_repository_ids if defined?(@all_acv_repository_ids)

    @all_acv_repository_ids = AcvContributor.group_by_repository(all_emails).pluck(:repository_id)
  end

  # Private: returns an ActiveRecord scope to query for repositories that are
  # archived in the the Arctic Code Vault which the user has contributed to,
  # filtering out the repositories that are any of the following:
  #   - not public
  #   - opted out of GH Archive Program
  #   - owned by users that have blocked current user
  #   - owned by users that have been blocked by current user
  def acv_repositories_scope
    return @acv_repositories_scope if defined?(@acv_repositories_scope)

    # Find the intersection of the user's ACV repository IDs and opted-out
    # repository IDs
    opted_out_repo_ids =
      Configuration::Entry.where(
        target_type: "Repository",
        name: Configurable::ArchiveProgramOptOut::KEY,
        target_id: all_acv_repository_ids
      ).pluck(:target_id)

    # Filter out any repositories that have opted out of the GH Archive Program
    filtered_acv_repo_ids = all_acv_repository_ids - opted_out_repo_ids

    # exclude repositories owned by users that have blocked current user,
    # or have been blocked by current user.
    blockers_and_blockees = ignored_by_users.pluck(:user_id) + ignored_users.pluck(:ignored_id)

    @acv_repositories_scope =
      Repository
        .public_scope
        .with_ids(filtered_acv_repo_ids)

    if blockers_and_blockees.length > 0
      @acv_repositories_scope = @acv_repositories_scope.where("repositories.owner_id NOT IN (?)", blockers_and_blockees)
    end

    @acv_repositories_scope
  end

  # Public: returns a list of most starred repositories archived in the
  # Arctic Code Vault that the user has contributed to that are
  # still publicly available and have not opted out of the GH Archive Program
  def top_acv_repositories(limit: ACV_CONTRIBUTED_REPOSITORIES_LIMIT)
    return @top_acv_repositories if defined?(@top_acv_repositories)
    return [] unless can_have_acv_badge?

    @top_acv_repositories =
      acv_repositories_scope
        .order("#{Repository.stargazer_count_column} DESC")
        .first(limit)
  end
end
