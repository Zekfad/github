# rubocop:disable Style/FrozenStringLiteralComment

module User::CouponsDependency
  extend ActiveSupport::Concern

  included do
    validate     :coupon_required?, :validate_coupon!
    after_save   :redeem_coupon!, :clear_coupon_local_cache
    after_update :end_trial_on_plan_change

    has_many :coupon_redemptions,
      -> { where(expired: false).order("coupon_redemptions.id DESC") },
      dependent: :destroy

    has_many :expired_coupons,
      -> { where(expired: true) },
      class_name: "CouponRedemption"

    has_many :coupons,
      -> { order("coupons.id DESC") },
      through: :coupon_redemptions
  end

  class_methods do
    # Sends emails and activates in-site notices for users that have a coupon
    # that is going to expire in two weeks. You should only run this once per
    # day.
    #
    # Returns an Array of usernames that were notified.
    def notify_coupon_expiring_in_two_weeks
      usernames = []
      CouponRedemption.expiring_in_two_weeks.find_each do |cr|
        user = cr.user
        next if user.nil? || cr.coupon.nil?
        next if !user.paid_plan?

        user.activate_notice!(:coupon_will_expire) unless user.organization?
        BillingNotificationsMailer.coupon_will_expire_soon(user, cr).deliver_now
        usernames << user.login
      end
      usernames
    end
  end

  # A coupon code can be set for easy redemption.
  #
  # @user.coupon = "my-code"
  # @user.save
  def coupon=(code)
    return if code.blank?
    clear_coupon_local_cache
    @new_coupon_code = code
  end

  # The active coupon. Only one coupon can be active at a time.
  #
  # Returns a Coupon if one is active.
  # Returns nil if no coupons are active.
  def coupon
    @coupon ||= Coupon.find_by_code(@new_coupon_code) || coupons.first
  end

  # The active coupon's redemption for this user.  The redemption
  # will tell us how long the coupon is active for, when the user
  # redeemed it, and so on.
  #
  # Returns a CouponRedemption if a coupon is active.
  # Returns nil if no coupon is active.
  def coupon_redemption
    return if new_record?
    coupon_redemptions.find_by_coupon_id(coupon)
  end

  # Was this user using a coupon that expired in a previous billing
  # cycle?
  #
  # Returns a Boolean
  def coupon_expired_since_last_billing?
    if redemption = last_expired_coupon_redemption
      redemption.expired_since_last_billing?
    end
  end

  # Is this user's most recently expired coupon an educational coupon?
  # We expose this because we need to communicate slightly differently to
  # users that were on educational coupons since they may not know that
  # they were actually using a coupon.
  #
  # Returns a Boolean
  def expired_education_coupon?
    if redemption = last_expired_coupon_redemption
      redemption.coupon.education_coupon?
    end
  end

  # Does the user currently have an active coupon?
  # coupon_redemption.expires_at can be in the past while it hasn't
  # been marked as expired through the job.
  #
  # Returns a Boolean
  def has_an_active_coupon?
    coupon_redemption && !coupon_redemption.stale?
  end

  # Does the user currently have an active plan-specific coupon?
  #
  # Returns a Boolean
  def has_a_plan_specific_coupon?
    coupon_redemption && !coupon_redemption.stale? && coupon.plan_specific?
  end

  # Is this user currently enjoying a free trial?
  #
  # Returns a Boolean
  def free_trial?
    if has_an_active_coupon?
      (coupon.trial? && data_packs == 0) ||
      coupon.discount >= plan.cost + data_pack_price.to_f
    else
      false
    end
  end

  # Does this free trial only work for a specific plan?
  #
  # Returns a Boolean
  def free_trial_locked_to_plan?
    free_trial? && coupon.plan_specific?
  end

  # User is on a free trial that hasn't expired yet, but
  # they're changing plans and entering their cc# info.
  #
  # In other words, they're upgrading out of their free trial.
  #
  # Returns a Boolean
  def upgrading_out_of_free_trial?
    return false if leaving_free_plan?
    old_plan = GitHub::Plan.find(plan_before_last_save)
    saved_change_to_plan? && old_plan && free_trial_locked_to_plan? && plan.cost > old_plan.cost
  end

  # User is on a free trial that hasn't expired yet, but
  # they're downgrading plans.
  #
  # In other words, they're dowgrading out of their free trial.
  #
  # Returns a Boolean
  def downgrading_out_of_free_trial?
    return false if leaving_free_plan?
    old_plan = GitHub::Plan.find(plan_before_last_save)
    saved_change_to_plan? && old_plan && free_trial_locked_to_plan? && plan.cost < old_plan.cost
  end

  # Lets you know how many days are remaining in this user's free trial,
  # if it is a trial. See Coupon#trial? for more info on trials.
  #
  # Returns an Integer if this is a trial.
  # Returns nil if this isn't a trial.
  def trial_days_left
    (coupon_redemption.expires_at.to_date - Date.today).to_i if free_trial?
  end

  # The monthly discount in dollars this user should receive due to any active
  # coupons.
  #
  # based_on_plan - optional GitHub::Plan object to calculate discount on. Defaults to the current plan.
  # based_on_seats - optional Integer number of seats being purchased. Defaults to the current number of seats for this account.
  #
  # Returns a Money
  def discount(based_on_plan: plan, based_on_seats: seats, based_on_data_packs: data_packs)
    Billing::Pricing.new(
      account: self,
      plan: based_on_plan,
      seats: based_on_seats,
      data_packs: based_on_data_packs,
      plan_duration: User::MONTHLY_PLAN,
    ).discount
  end

  # Amount of unused discount available to put toward add-ons like data packs,
  # after discount has been applied to plans and already-purchased add-ons.
  #
  # Returns a Money discount amount
  def remaining_discount
    return Billing::Money.new(0) if coupon.blank? || coupon.percentage? || coupon.plan_specific?

    plan_list_price = Billing::Pricing.new(account: self, plan_duration: User::MONTHLY_PLAN).undiscounted

    remaining = Billing::Money.new(coupon.discount * 100) - plan_list_price
    [remaining , Billing::Money.new(0)].max
  end

  # Some plans require a coupon. Maybe they are too awesome for normal
  # use, who knows.
  #
  # If this method returns `true` it will also set
  # `errors[:coupon]` to an error message if no coupon is set.
  #
  # Returns a Boolean indicating whether or not the plan this user has
  #   chosen requires a coupon.
  def coupon_required?
    return if gh_role == "staff"

    if plan && plan.coupon? && coupon.blank?
      errors.add(:coupon, "is required for the #{plan} plan.")
      true
    else
      false
    end
  end

  # Given a coupon code, attempts to validate it. Any errors will be
  # set on this user's `coupon` attribute.
  #
  # code - A String code or Coupon object to validate against this user.
  # options - The Hash options (default: {}):
  #           :actor - User attempting perform the redemption
  #           :allow_reuse - A Boolean that permits a previously-expired coupon
  #                          to be reused on this user (default: false).
  #
  # Returns false if the given code is invalid.
  # Returns the Coupon if it's valid for this user.
  def validate_coupon(code, opts = {})
    if has_any_trade_restrictions?
      errors.add(:coupon, "can't be redeemed - #{TradeControls::Notices::Plaintext.user_account_restricted}")
      return false
    end

    coupon = Coupon.find_by_code(code)

    if coupon.blank?
      errors.add(:coupon, "can't be found.")
      return false
    end

    if has_an_active_coupon?
      errors.add(:coupon, "can't be redeemed - you have an active coupon!")
      return false
    end

    if billing_type != "card"
      errors.add(:coupon,
        "can't be redeemed with your account. Please contact support.")
      return false
    end

    unless coupon.redeemable_by?(opts[:actor])
      errors.add(:coupon,
        "can't be redeemed with your account. Please contact support.")
      return false
    end

    if coupon.expires_at && coupon.expires_at < Time.now
      errors.add(:coupon, "has expired. Sorry!")
      return false
    end

    if coupon.limit == 0
      errors.add(:coupon, "can't be redeemed any more times.")
      return false
    end

    if coupon_plan = coupon.plan
      if !coupon.plan.free? && coupon_plan.orgs? && !organization?
        errors.add(:coupon, "is for organizations only.")
        return false
      end

      if !coupon.plan.free? && !coupon_plan.orgs? && organization?
        errors.add(:coupon, "is for users only.")
        return false
      end

      if plan.coupon? && coupon.plan != plan
        errors.add(:coupon, "can't be used with the #{plan} plan.")
        return false
      end

      if coupon.plan != plan && !coupon.trial?
        errors.add(:coupon, "can't be used with the #{plan} plan.")
        return false
      end

      if private_repo_count_for_limit_check > coupon_plan.repos
        errors.add(:coupon, "can't be used with #{private_repo_count_for_limit_check} private repositories.")
        return false
      end

    end

    if !opts[:allow_reuse] && expired_coupons.map(&:coupon_id).include?(coupon.id)
      errors.add(:coupon, "has already been redeemed.")
      return false
    end

    return coupon
  end

  # Same as `validate_coupon` but uses a coupon set with this user's
  # `coupon=` accessor.
  #
  # Returns nothing.
  def validate_coupon!
    validate_coupon(@new_coupon_code) if @new_coupon_code.present?
  end

  # Redeems a coupon and applies the discount to this user. If the coupon
  # is a "trial" for a paid plan, upgrades the user's plan automatically
  # and sets the billed_on appropriately.
  #
  # So if the user is on Free and there's a trial coupon for Small,
  # we'll switch the user to Small without asking for their cc# info.
  #
  # This will also upgrade the user's plan if their coupon affords them
  # a large plan. Say, a $12 discount applied to a Free plan. Bumps you
  # up to Small.
  #
  # Only one coupon may be redeemed at a time. If an active coupon exists,
  # this method will fail to redeem the new coupon.
  #
  # code  - A coupon code String or the Coupon object to be
  #         redeemed.
  # options - The Hash options (default: {}):
  #           :actor       - The User who is redeeming this coupon (default: self).
  #           :allow_reuse - A Boolean that permits a previously-expired coupon
  #                          to be reused on this user (default: false).
  #           :instrument  - Tracks the change to the user's plan (default: true).
  #
  # Returns false (and sets errors) if the coupon can't be redeemed.
  # Returns the applied CouponRedemption object if the coupon was
  #   successfully applied.
  def redeem_coupon(code, opts = {})
    return false if spammy?

    opts = opts.reverse_merge \
      actor: self,
      allow_reuse: false,
      instrument: true

    return false unless coupon = validate_coupon(code, opts)

    clear_coupon_local_cache

    redeemed = false
    transaction do
      expire_active_coupon if coupon_redemption
      redeemed = coupon_redemptions.create(coupon: coupon)

      old_plan = plan
      plan = best_plan_for_coupon

      # Reset billing_attempts if coupon fully covers the cost
      new_billing_attempts = free_trial? ? 0 : billing_attempts

      # Figure out when to bill them next
      start_date = billed_on || GitHub::Billing.today

      # Bump them if we need a new_plan.
      if old_plan != plan
        update \
          plan: plan,
          billed_on: start_date,
          billing_attempts: new_billing_attempts,
          seats: plan.per_seat? ? default_seats(new_plan: plan) : 0,
          disabled: false

        if opts[:instrument]
          track_plan_change(opts[:actor], old_plan, coupon: coupon)
        end
      else
        update \
          billed_on: start_date,
          billing_attempts: new_billing_attempts,
          seats: plan.per_seat? ? default_seats : 0,
          disabled: false
      end

      if coupon.non_profit?
        result = void_or_refund_last_transaction!

        unless result.success?
          redeemed = false
          errors.add(:coupon, "cannot be redeemed. #{result.message}")
          raise ActiveRecord::Rollback
        end
      end
    end

    if redeemed
      GlobalInstrumenter.instrument(
        "billing.redeem_coupon",
        actor_id: opts[:actor]&.id,
        user_id: id,
        coupon_id: coupon.id,
      )
    end

    redeemed
  end

  # Internal: Determine the best plan for the user's current coupon
  #
  # Returns a GitHub::Plan object
  def best_plan_for_coupon
    return plan if coupon.nil?
    return plan if plan.per_seat? || plan.pro?
    return coupon.plan if coupon.plan_specific?

    new_plan = if user?
      GitHub::Plan.pro
    elsif organization? && plan.free?
      # Test for plan.free? is to prevent grandfathered orgs
      # from getting moved to business
      GitHub::Plan.business
    end

    new_plan ||= if organization?
      GitHub::Plan.org_plan_for_discount(discount_after_data_packs)
    else
      GitHub::Plan.user_plan_for_discount(discount_after_data_packs)
    end

    # Don't upgrade user out of coupon discount if currently not paying
    return plan if (payment_amount == 0 && payment_amount(plan: new_plan) > 0) && !will_be_expired?

    # Don't downgrade organizations (users should never be on bigger plans than pro)
    return plan if payment_amount > payment_amount(plan: new_plan) && organization?
    return plan if new_plan.free? && plan.free_with_addons?

    new_plan
  end

  # Same as `redeem_coupon` but uses a coupon set with this user's
  # `coupon=` accessor.
  #
  # Returns nothing.
  def redeem_coupon!
    redeem_coupon(@new_coupon_code) if @new_coupon_code.present?
  end

  # Did they just upgrade or downgrade out of a free trial? If so that's awesome,
  # but it means we need to expire their coupon.
  #
  # Should be used as an after_save callback.
  #
  # Returns nothing
  def end_trial_on_plan_change
    if upgrading_out_of_free_trial? || downgrading_out_of_free_trial?
      expire_active_coupon
    end
  end

  # Expires the active coupon, if one exists.
  #
  # If the user was on a free trial and has private repos,
  # we'll disable them and send an email. If they're on a free
  # trial but don't have any private repos, we'll downgrade them to
  # the 'free' plan.
  #
  # quiet - Boolean to suppress email notification that the account has been
  #         disabled due to coupon expiration.
  #
  # Returns true if there was an active coupon and it's now expired.
  # Returns false if there's no active coupon.
  def expire_active_coupon(quiet: false)
    return false if coupon.blank?

    # If they're on a free trial, downgrade them to free and check to see if
    # we need to disable their account.
    unless has_valid_payment_method? || invoiced?
      if private_repo_count_for_limit_check == 0
        old_plan = plan
        self.plan = "free"
        track_plan_change(self, old_plan)
      else
        disable!
        BillingNotificationsMailer.coupon_expired_failure(self, education_coupon: coupon.education_coupon?).deliver_later unless quiet
      end
    end

    # Delete any notice about coupon expiration when the coupon goes away.
    delete_notice :coupon_will_expire

    clear_coupon_local_cache
    coupon_redemption.try(&:expire!)

    save
  end

  # Expires the active coupon, if it's stale.
  #
  # Returns true if coupon is stale and gets expired, false otherwise
  def expire_stale_coupon
    has_stale_coupon_redemptions? && expire_active_coupon
  end

  def has_stale_coupon_redemptions?
    coupon_redemptions.any?(&:stale?)
  end

  # Best used in an after_save. Coupons are cached locally using an ivar,
  # but that can cause problems in tests. For example:
  #
  # 1. Create user w/ coupon
  # 2. Upgrade user in separate codepath
  # 3. Call user.reload on user object from #1
  # 4. user.coupon still exists, even though it shouldn't!
  #
  # Clearing the local cache fixes this issue.
  #
  # Returns nothing.
  def clear_coupon_local_cache
    @coupon = @new_coupon_code = nil
  end

  # Check if the active coupon will already be expired on the
  # next_billing_date.
  #
  # Yearly payments do not take coupon expiration into account currently.
  #
  # Returns a Boolean.
  def will_be_expired?(service_started_on = nil)
    coupon_redemption && coupon_redemption.expires_at.to_date < (service_started_on || next_billing_date).to_date && monthly_plan?
  end

  private

  # Internal: Amount of discount remaining after applying discount ONLY to data packs.
  #
  # Returns a Float
  def discount_after_data_packs
    [discount.to_f - data_pack_monthly_price.to_f, 0.0].max
  end

  # Check whether the plan the user was on is the free plan.
  #
  # Returns a Boolean.
  def leaving_free_plan?
    saved_change_to_plan? &&
      (plan_before_last_save == "free" || plan == coupon.try(:plan))
  end

  # Finds most recently expired CouponRedemption
  #
  # Returns CouponRedemption or nil
  def last_expired_coupon_redemption
    expired_coupons.order("expires_at ASC").last
  end

  # Private: Expires the users coupon if they're upgrading to business plus
  #
  # Returns nothing
  def remove_coupon_on_business_plus_upgrade
    return if coupon&.plan&.business_plus?

    if saved_change_to_plan? && has_an_active_coupon? && plan.business_plus?
      coupon_redemption.expire!
    end
  end
end
