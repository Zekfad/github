# rubocop:disable Style/FrozenStringLiteralComment

module User::TwoFactorAuthenticationDependency
  extend ActiveSupport::Concern

  included do
    has_one :two_factor_credential

    scope :two_factor_disabled,
      -> {
        joins("LEFT OUTER JOIN two_factor_credentials " \
              "ON two_factor_credentials.user_id = users.id").
        where("two_factor_credentials.user_id IS NULL")
      }
  end

  class_methods do
    # Authenticates a user using a two-factor one-time-use password.
    # User must have 2fa enabled.
    def two_factor_authenticate(login, otp)
      user = User.find_by_login(login)
      return unless user && user.two_factor_authentication_enabled?

      user.two_factor_credential.verify_totp(otp, allow_reuse: false) ? user : nil
    end

    def two_factor_authenticate_with_recovery(login, recovery_code)
      user = User.find_by_login(login)
      return unless user
      user.two_factor_recover(recovery_code) ? user : nil
    end
  end

  # Public: Has this user configured their 2FA credentials?
  #
  # On Enterprise, if user migrates their account to an external authentication
  # provider that doesn't support 2FA (SAML/CAS), then 2FA is no longer enabled.
  #
  # Returns a Boolean.
  def two_factor_authentication_enabled?
    two_factor_credential.present? && GitHub.auth.two_factor_authentication_allowed?(self)
  end

  def async_two_factor_authentication_enabled?
    self.async_two_factor_credential.then do |two_factor_credential|
      two_factor_credential.present? && GitHub.auth.two_factor_authentication_allowed?(self)
    end
  end

  def sms_enabled?
    GitHub.two_factor_sms_enabled? && two_factor_authentication_enabled? && (two_factor_fallback_enabled? || two_factor_credential.configured_with?(:sms))
  end

  def two_factor_fallback_enabled?
    two_factor_credential && two_factor_credential.fallback_enabled?
  end

  def has_registered_security_key?
    u2f_registrations.any?
  end

  def add_or_replace_two_factor_credential(credential)
    if two_factor_credential
      two_factor_credential.destroy
    else
      GitHub.dogstats.increment "user", tags: ["action:two_factor_enable"]
    end
    self.two_factor_credential = credential.dup
  end

  # Verifies the current recovery code for this account, against the
  # recovery code that the user submitted.
  #
  # If the recovery code matches, **the recovery code will be invalidated
  # and won't be usable again for this account**, and the method will
  # return `true`.
  #
  # If the recovery code doesn't match, `false` is returned.
  #
  # Whitespace in the recovery code is not taken into account
  # when performing the comparison.
  def two_factor_recover(recovery_code)
    return false unless two_factor_authentication_enabled?

    recovery_code = TwoFactorCredential.normalize_otp(recovery_code)
    idx = two_factor_credential.recovery_codes.index(recovery_code)

    if idx.nil? || two_factor_credential.recovery_code_used?(idx)
      GitHub.dogstats.increment("authentication.2fa_recovery", tags: ["result:failure"])
      return false
    end

    GitHub.dogstats.increment("authentication.2fa_recovery", tags: ["result:success"])
    two_factor_credential.recovery_code_mark_used(idx)
    true
  end

  # TODO: Remove this once we've fully migrated to `webauthn`. https://github.com/github/prodsec/issues/26
  def webauthn_or_u2f_authenticated?(origin, challenge, sign_response_json_serialized)
    sign_response_hash = JSON.parse(sign_response_json_serialized)

    if sign_response_hash.has_key?("keyHandle")
      sign_response = U2F::SignResponse.load_from_json(sign_response_json_serialized)
      return u2f_authenticated?(challenge, sign_response)
    else
      sign_response = WebAuthn::AuthenticatorAssertionResponse.new(
        authenticator_data: Base64.urlsafe_decode64(sign_response_hash["response"]["authenticatorData"]),
        signature: Base64.urlsafe_decode64(sign_response_hash["response"]["signature"]),
        client_data_json: Base64.urlsafe_decode64(sign_response_hash["response"]["clientDataJSON"]),
      )
      return webauthn_authenticated?(origin, challenge, sign_response_hash["rawId"], sign_response)
    end
  rescue JSON::ParserError
    return false
  end

  # Checks a sign-response against the challenges and the user's security keys.
  #
  # challenge - An challenge String.
  # response  - A SignResponse.
  #
  # Returns boolean.
  def u2f_authenticated?(challenge, response)
    registration = u2f_registrations.where(key_handle: response.key_handle).first

    # There isn't a registration with the key handle from the response.
    unless registration
      return false
    end

    # We couldn't verify the response.
    unless registration.authenticated?(challenge, response)
      return false
    end

    true
  end

  # Checks a sign-response against the challenges and the user's security keys.
  #
  # origin    - The web origin of the client response.
  # challenge - An challenge String.
  # response  - A WebAuthn::AuthenticatorAssertionResponse.
  #
  # Returns boolean.
  def webauthn_authenticated?(origin, challenge, key_handle, response)
    registration = u2f_registrations.where(key_handle: key_handle).first

    # There isn't a registration with the key handle from the response.
    unless registration
      GitHub.dogstats.increment("authentication.webauthn", tags: [
        "result:failure",
        "reason:no_matching_request_error",
      ])
      return false
    end

    # We couldn't verify the response.
    unless registration.webauthn_authenticated?(origin, challenge, response)
      return false
    end

    true
  end
end
