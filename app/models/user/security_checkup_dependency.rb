# frozen_string_literal: true

module User::SecurityCheckupDependency
  SECURITY_CHECKUP_INTERVAL = 3.months
  SECURITY_CHECKUP_POSTPONED_INTERVAL = 1.week
  SECURITY_CHECKUP_RECENCY_INTERVAL = 1.hour

  # User Security Checkup
  #
  # Every SECURITY_CHECKUP_INTERVAL, we show users the Security Checkup screen.
  #
  # When they complete the screen (security_checkup_completed), we record the time,
  # which we then compare against SECURITY_CHECKUP_INTERVAL to determine whether
  # to show them the screen again.
  #
  # When the user chooses to postpone the Security Checkup (security_checkup_postponed),
  # we update security_checkup_completed_at by SECURITY_CHECKUP_POSTPONED_INTERVAL,
  # moving the reminder forward that amount of time.
  #
  # In designing this implementation, we considered storing the timestamp as something
  # along the lines of `security_checkup_next_due_at`, but decided against doing so
  # as that would limit our ability to change the SECURITY_CHECKUP_INTERVAL, at least
  # without doing a transition of some sort.

  def security_checkup_key
    "user.security_checkup_completed_at.#{self.id}"
  end

  def security_checkup_completed(type = nil)
    GitHub.kv.set(security_checkup_key, Time.now.utc.to_s)
    if type == "updated"
      GitHub.kv.set(security_checkup_action_key, "true", expires: SECURITY_CHECKUP_RECENCY_INTERVAL.from_now)
    end
  end

  def security_checkup_postponed
    GitHub.kv.set(security_checkup_key, (Time.now.utc - SECURITY_CHECKUP_INTERVAL + SECURITY_CHECKUP_POSTPONED_INTERVAL).to_s)
  end

  def security_checkup_due?
    return false unless two_factor_authentication_enabled?

    value = GitHub.kv.get(security_checkup_key).value { nil }

    if value.present?
      Time.parse(value) < Time.now.utc - SECURITY_CHECKUP_INTERVAL
    elsif two_factor_credential&.created_at
      # Fall back to an offset of the two factor credential creation date, to prevent
      # us from nagging users who _just_ enrolled.
      two_factor_credential&.created_at < Time.now.utc - SECURITY_CHECKUP_POSTPONED_INTERVAL
    else
      true
    end
  end

  def security_checkup_action_key
    "security-checkup-updated:#{self.id}"
  end

  def recently_took_action_on_security_checkup?
    !!GitHub.kv.get(security_checkup_action_key).value { nil }
  end

  def instrument_security_checkup(event)
    GitHub.dogstats.increment("user.security_checkup", tags: ["action:#{event}"])
    instrument "security_checkup_#{event}".to_sym, { actor: self }
  end

  def recovery_codes_status
    @recovery_codes_status ||= begin
      query = {
        user_id: self.id,
      }
      recovery_events = Audit::Driftwood::Query.new_2fa_user_query(query).execute.results

      return nil unless recovery_events.any?

      latest_event = recovery_events.first

      {
        action: latest_event[:action].split("_").last.capitalize,
        created_at: Time.at(latest_event[:created_at] / 1000),
      }
    end
  end
end
