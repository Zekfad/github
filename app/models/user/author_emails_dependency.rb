# frozen_string_literal: true

module User::AuthorEmailsDependency

  # Public: Gets a list of emails the user can use to author a commit
  #
  # Return type: [] | [String]
  # Returns [] if the user has email privacy enabled, if the current instance of GitHub
  #   does not have this feature enabled, if the feature flag is not enabled, or if the user
  #   only has one verified and visible email.
  # Else, returns the list of verified, visible emails
  def author_emails
    return [] if use_stealth_email? || !GitHub.choose_commit_email_enabled?
    emails = self.emails.verified.visible
    # We don't want to display the dropdown if the user only has one email to choose since they have no other option
    emails.count > 1 ? emails.map(&:email) : []
  end

  def default_author_email_cache_key(repository)
    "user:default_author_email:#{repository.owner_id}:#{self.id}"
  end

  # Public: Gets the cached default author email for a commit in a repository
  #
  # repository - The Repository in which the user is committing.
  # current_sha - The sha of the current pull request branch, if available
  #
  # Return type: nil | String
  # Returns nil if author_emails returns [], or if repository is nil
  # If the user is a contributor to the repository, returns the last email the user contributed with
  # Else, if the repository is in an org and the user has custom notification routing, returns the notification email
  # Else, if something above fails or if the above conditions aren't true, returns the user's git_author_email
  def default_author_email(repository, current_sha = nil)
    return unless author_emails.any? && repository

    default_email = GitHub.kv.get(default_author_email_cache_key(repository)).value { nil }
    if default_email && author_emails.include?(default_email)
      default_email
    else
      uncached_default_author_email(repository, current_sha)
    end
  end

  def uncached_default_author_email(repository, current_sha)
    if repository.contributor?(self)
      # default to the last email they committed with on this repository or current branch
      branches_to_search = [repository.default_oid, current_sha].compact
      commit_sha = begin
        repository.rpc.list_revision_history_multiple(branches_to_search, authors: author_emails, max: 1, timeout: 1).first
      rescue GitRPC::Timeout
        nil
      end
      if commit_sha
        last_commit = repository.commits.find(commit_sha)
        return last_commit.author_email if last_commit&.author_email
      end
    end

    git_author_email
  end
end
