# frozen_string_literal: true

module User::ProfileReadmeDependency
  # Public: Whether or not the user has a profile readme,
  #         regardless of visiblity
  #
  # Returns a boolean
  def has_profile_readme?
    return false unless has_configuration_repository?
    configuration_repository.has_readme?
  end

  # Public: The user's profile readme if one exists,
  #         regardless of visiblity
  #
  # Returns a TreeEntry or nil
  def profile_readme
    async_profile_readme.sync
  end

  def async_profile_readme
    async_configuration_repository.then do |repo|
      next if repo.nil?
      repo.async_preferred_readme
    end
  end

  # Public: Whether or not the user is in the correct state
  #         to display a profile readme
  #
  # Returns a boolean
  def eligible_to_display_profile_readme?
    async_eligible_to_display_profile_readme?.sync
  end

  def async_eligible_to_display_profile_readme?
    return Promise.resolve(false) if spammy?
    return Promise.resolve(false) if GitHub.flipper[:hide_profile_readme].enabled?(self)

    async_profile.then do |profile|
      next false unless profile
      profile.readme_opt_in?
    end
  end

  # Public: Whether or not the the users's profile readme
  #         is currently visible on their profile
  #
  # Returns a boolean
  def profile_readme_visible?
    async_profile_readme_visible?.sync
  end

  def async_profile_readme_visible?
    Promise.all([
      async_eligible_to_display_profile_readme?,
      async_configuration_repository.then,
      async_profile_readme
    ]).then do |is_eligible, repo, readme|
      next false unless is_eligible
      next false unless repo&.public?

      # As of now, it's necessary to check both disabled? methods.
      # See https://github.com/github/github/pull/148922/files#r452507975
      # for details
      next false if repo.disabled?
      next false if repo.access.disabled?

      next false unless readme
      !readme.data.blank?
    end
  end
end
