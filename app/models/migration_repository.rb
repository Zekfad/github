# frozen_string_literal: true

class MigrationRepository < ApplicationRecord::Domain::Migrations
  areas_of_responsibility :migration
  belongs_to :migration
  belongs_to :repository
end
