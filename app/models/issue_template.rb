# frozen_string_literal: true

class IssueTemplate
  include ActiveModel::Validations

  attr_writer :other_templates

  validates :name, presence: true, length: { maximum: 200, minimum: 3, allow_blank: true }
  validate :name_uniqueness
  validates :about, presence: true, length: { maximum: 200, minimum: 3, allow_blank: true }

  def self.default(name, repository: nil)
    case name
    when :bug
      new(repository: repository, filename: "bug_report.md",
        name: "Bug report",
        about: "Create a report to help us improve",
        title: "",
        labels_string: "",
        assignees_string: "",
        body: <<~MARKDOWN
        **Describe the bug**
        A clear and concise description of what the bug is.

        **To Reproduce**
        Steps to reproduce the behavior:
        1. Go to '...'
        2. Click on '....'
        3. Scroll down to '....'
        4. See error

        **Expected behavior**
        A clear and concise description of what you expected to happen.

        **Screenshots**
        If applicable, add screenshots to help explain your problem.

        **Desktop (please complete the following information):**
         - OS: [e.g. iOS]
         - Browser [e.g. chrome, safari]
         - Version [e.g. 22]

        **Smartphone (please complete the following information):**
         - Device: [e.g. iPhone6]
         - OS: [e.g. iOS8.1]
         - Browser [e.g. stock browser, safari]
         - Version [e.g. 22]

        **Additional context**
        Add any other context about the problem here.
        MARKDOWN
      )
    when :feature
      new(repository: repository, filename: "feature_request.md",
        name: "Feature request",
        about: "Suggest an idea for this project",
        title: "",
        labels_string: "",
        assignees_string: "",
        body: <<~MARKDOWN
        **Is your feature request related to a problem? Please describe.**
        A clear and concise description of what the problem is. Ex. I'm always frustrated when [...]

        **Describe the solution you'd like**
        A clear and concise description of what you want to happen.

        **Describe alternatives you've considered**
        A clear and concise description of any alternative solutions or features you've considered.

        **Additional context**
        Add any other context or screenshots about the feature request here.
        MARKDOWN
      )
    when :blank
      new(repository: repository, filename: "custom.md",
        name: "Custom issue template",
        about: "Describe this issue template's purpose here.",
        title: "",
        labels_string: "",
        assignees_string: ""
      )
    end
  end

  def self.preview(repository, filename, hashes)
    template = from_hash(repository, hashes[filename])
    template.other_templates = hashes.values.map { |v| from_hash(repository, v) }
    template
  end

  def self.from_hash(repository, hash)
    new(
      repository: repository,
      filename: hash["filename"],
      name: hash["name"],
      about: hash["about"],
      title: hash["title"],
      body: hash["body"],
      labels_string: hash["labels"],
      assignees_string: hash["assignees"],
    )
  end

  def self.from_tree_entry(tree_entry)
    from_markdown(tree_entry.repository, tree_entry.name, tree_entry.data)
  end

  def self.from_markdown(repository, filename, markdown)
    front_matter = FrontMatter.new(markdown)
    config = FrontMatter.new(markdown).parsed_yaml

    if config.nil?
      return
    end

    new(
      repository: repository,
      filename: filename,
      name: config["name"],
      about: config["about"],
      title: config["title"],
      body: front_matter.body,
      labels_string: config["labels"],
      assignees_string: config["assignees"],
    )
  end

  attr_reader :repository, :name, :about, :body, :labels_string, :assignees_string, :title

  def initialize(repository:, filename: "", name: "", about: "", body: "", other_templates: nil, labels_string: "", assignees_string: "", title: "")
    @repository = repository
    @filename = filename
    @name = name
    @about = about
    @body = body
    @labels_string = labels_string
    @assignees_string = assignees_string
    @other_templates = other_templates
    @title = title
  end

  def async_repository
    Promise.resolve(repository)
  end

  def filename
    @filename.presence || name.gsub(/[^\w]/, "-") + ".md"
  end

  def issue
    @issue ||= repository.issues.new.tap do |new_issue|
      new_issue.labels = labels
      new_issue.assignees = assignees
    end
  end

  def assignees
    @assignees ||= prefilled_issue_fields.assignees
  end

  def labels
    @labels ||= prefilled_issue_fields.labels
  end

  def to_markdown
    front_matter = YAML.dump(front_matter_hash)
    [front_matter, "---", "", @body.to_s.strip, ""].join("\n")
  end

  def parameterized_name
    @name&.parameterize(separator: "_")
  end

  def uses?(field_name)
    used_front_matter_fields.include?(field_name.to_s)
  end

  private

  def used_front_matter_fields
    front_matter_hash.delete_if { |_, value| value.blank? }.keys
  end

  def front_matter_hash
    hash = {
      "name" => @name,
      "about" => @about,
      "title" => @title,
      "labels" => @labels_string,
      "assignees" => @assignees_string,
    }

    hash
  end


  def prefilled_issue_fields
    @prefilled_issue_fields ||= PrefilledIssueFields.new(
      params: { assignees: assignees_string, labels: labels_string },
      repository: repository,
      user: nil,
    )
  end

  def other_templates
    @other_templates || repository.issue_templates.templates
  end

  def name_uniqueness
    if other_templates.any? { |template| template.name == name && template.filename != filename }
      errors.add(:name, "must be unique")
    end
  end
end
