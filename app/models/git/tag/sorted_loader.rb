# frozen_string_literal: true

class Git::Tag::SortedLoader < Git::Ref::Loader
  def initialize(repository)
    super(repository, "default")
  end

  private

  def load_all_refs
    rpc.sorted_tag_refs.map do |name, target_oid, peeled_oid|
      [name.b, target_oid, peeled_oid]
    end
  rescue GitRPC::InvalidRepository, GitHub::DGit::UnroutedError
    []
  end
end
