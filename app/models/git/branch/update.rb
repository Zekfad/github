# frozen_string_literal: true

module Git
  module Branch
    class Update < Git::Ref::Update
      attr_reader :force

      # Public: Representation of a single branch update
      #
      # policy_commit_oid - String OID of the reference state for policy check
      # force             - Boolean: whether updates that are not
      #                     fast-forwardable should be allowed.
      #
      # See Git::Ref::Update#initialize for the list of options
      def initialize(policy_commit_oid: nil, force: true, **args)
        @force = force
        @policy_commit_oid = policy_commit_oid
        super(**args)
      end

      # When squashing before merging, the policy commit is the previous merge
      # commit SHA, referenced in PullRequest#merge_commit_sha, that is rewritten
      # into a squashed commit.
      #
      # Returns a Commit.
      def policy_commit
        @policy_commit ||= repository.commits.find(policy_commit_oid)
      end

      def policy_commit_oid
        @policy_commit_oid || @after_oid
      end

      protected

      def validate_attributes
        super
        GitRPC::Util.ensure_valid_full_sha1(policy_commit_oid)
        enforce_fast_forward_policy
      end

      private

      def enforce_fast_forward_policy
        return if (creation? || deletion?) # only want to check for "true" updates
        return if force
        if !repository.rpc.descendant_of?(after_oid, before_oid)
          raise ::Git::Ref::NotFastForward
        end
      end

      def state
        super + [policy_commit_oid]
      end
    end
  end
end
