# frozen_string_literal: true

class RepositoryPushJobTrigger

  attr_reader :repository, :author, :ref_updates, :protocol, :pushed_at

  def initialize(repository, author, ref_updates, protocol)
    @repository  = repository
    @author      = author
    @ref_updates = ref_updates
    @protocol    = protocol
    @pushed_at   = Time.zone.now
  end

  def enqueue
    enqueue_repository_push_job
    enqueue_token_scan_job
    instrument_in_hydro
  end

  private

  def enqueue_repository_push_job
    # Ordering of ref_updates is different for this job.
    updates = ref_updates.map do |ref_update|
      [ref_update.refname, ref_update.before_oid, ref_update.after_oid]
    end
    repository_push_class.perform_later(repository.shard_path, author, updates, protocol)
  end

  def enqueue_token_scan_job
    return unless changed_ref_updates.any?

    if repository.scan_for_tokens?
      if gist?
        GistCommitTokenScanningJob.perform_later(repository.id, changed_ref_updates, author)
      else
        RepoCommitTokenScanningJob.perform_later(repository.id, changed_ref_updates, author)
      end
    end

    if !gist? && repository.token_scanning_enabled? && !repository.token_scanning_service_enabled?
      TokenScanningConfigChangeJob.perform_later(repository.id) if config_file_changed?
      CommitPrivateTokenScanningJob.perform_later(repository.id, changed_ref_updates, author)
    end
  end

  def instrument_in_hydro
    return if gist? || wiki?
    return unless changed_ref_updates.any?

    GlobalInstrumenter.instrument("repository.post_receive", {
      repository: repository,
      owner: repository.owner,
      actor: author && User.find_by_login(author),
      ref_updates: changed_ref_updates,
      feature_flags: repository.post_receive_instrumentation_feature_flags,
      pushed_at: pushed_at }
    )
  end

  def repository_push_class
    gist? ? GistPushJob : RepositoryPushJob
  end

  def gist?
    repository.is_a?(::Gist)
  end

  def wiki?
    repository.namespace == "wiki"
  end

  def changed_ref_updates
    @changed_ref_updates ||= begin
      ref_updates.each_with_object([]) do |ref_update, updates|
        if ref_update.changed?
          updates << [ref_update.before_oid, ref_update.after_oid, ref_update.refname]
        end
      end
    end
  end

  def config_file_changed?
    ref_updates.any? do |ref_update|
      # make sure this update is going into master
      # ref_update.refname example: "refs/heads/master". Extracting "master" as the branch being merged into.
      update_branch = ref_update.refname.split("/")[-1]
      return false unless update_branch == repository.default_branch

      # check if a config file was one of the changes
      changed_commit_diff = repository.commits.find(ref_update.after_oid).diff
      return true if changed_commit_diff.map { |file| file.path }.include? (".github/" + TokenScanningConfigurationFile::DEFAULT_NAME)

      # check if config was renamed
      changed_commit_diff.map { |file| file.a_path }.include? (".github/" + TokenScanningConfigurationFile::DEFAULT_NAME)
    end
  end
end
