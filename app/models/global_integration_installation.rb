# frozen_string_literal: true

class GlobalIntegrationInstallation
  include Ability::Actor
  include IntegrationInstallable

  attr_reader :integration, :integration_id

  def initialize(integration)
    return nil unless Apps::Internal.capable?(:installed_globally, app: integration)

    @integration    = integration
    @integration_id = integration.id
  end

  def id
    0
  end

  def installed_on_all_repositories?(min_action: :read, resource: nil)
    raise NotImplementedError
  end

  def repository_ids(min_action: nil, resource: nil, repository_ids: nil)
    raise NotImplementedError
  end

  def repository_selection
    raise NotImplementedError
  end

  def repositories(min_action: :read, resource: nil)
    raise NotImplementedError
  end

  def permissions
    return @permissions if defined?(@permissions)
    @permissions = integration.default_permissions
  end

  def abilities(subject_types: [], subject_ids: [])
    raise NotImplementedError
  end

  def async_can?(collection:, action:)
    ::Permissions::Service.async_can?(self, action, collection).then do |result|
      result || async_has_granular_permission_on_specific_resource?(collection, action)
    end
  end

  private

  def async_has_granular_permission_on_specific_resource?(collection, action)
    return Promise.resolve(false) unless permissions.key?(collection.name)

    installation_action = permissions[collection.name]
    return Promise.resolve(false) unless Ability.actions[installation_action] >= Ability.actions[action]

    Promise.resolve(true)
  end
end
