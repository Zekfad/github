# frozen_string_literal: true

class DiscussionSpotlight < ApplicationRecord::Collab
  include GitHub::Validations

  PRECONFIGURED_COLOR_STOPS = {
    peach_red: ["EE886E", "B31329"],
    orange_magenta: ["EE8540", "8E135C"],
    gold_red: ["D59900", "990000"],
    green: ["01AF70", "005916"],
    teal: ["00B0A7", "025348"],
    cyan_purple: ["00ACC6", "450774"],
    periwinkle_navy: ["6B97FE", "0E2B91"],
    purple_navy: ["C581EF", "002B6C"],
    purple: ["B378FE", "59009F"],
    salmon_purple: ["FF759E", "64006B"],
    fuchsia_purple: ["FD58CE", "7B0076"],
    gray: ["808794", "222B3C"],
  }.freeze

  LIMIT_PER_REPOSITORY = 3
  COLOR_REGEXP = /\A[0-9a-f]{6}\z/iu # e.g., ff00ff
  COLOR_SHORTHAND_REGEXP = /\A[0-9a-f]{3}\z/iu # e.g., fff

  belongs_to :repository, required: true
  belongs_to :spotlighted_by, class_name: "User"
  belongs_to :discussion, required: true

  has_one :category, through: :discussion

  extend GitHub::Encoding
  force_utf8_encoding :emoji

  before_validation :set_repository
  before_validation :expand_custom_color_shorthand
  before_validation :set_position, on: :create

  enum preconfigured_color: {
    peach_red: 0,
    orange_magenta: 1,
    gold_red: 2,
    green: 3,
    teal: 4,
    cyan_purple: 5,
    periwinkle_navy: 6,
    purple_navy: 7,
    purple: 8,
    salmon_purple: 9,
    fuchsia_purple: 10,
    gray: 11,
  }

  enum pattern: {
    "dot-fill" => 0,
    "plus" => 1,
    "octoface" => 2,
    "chevron-up" => 3,
    "dot" => 4,
    "heart-fill" => 5
  }

  validates :spotlighted_by, presence: true, on: :create
  validates :discussion, uniqueness: { scope: :repository_id }
  validates :emoji, allow_blank: true, single_emoji: true, allowed_emoji: true
  validates :position, presence: true,
            numericality: { only_integer: true, greater_than: 0 },
            uniqueness: { scope: :repository_id }
  validates :other_repo_spotlights_count, numericality: { less_than: :repo_spotlight_limit }
  validates_format_of :custom_color, allow_blank: true, with: COLOR_REGEXP
  validate :ensure_at_least_one_color_option_set

  scope :for_discussion, ->(discussion) { where(discussion_id: discussion) }
  scope :for_repository, ->(repo) { where(repository_id: repo) }

  # Public: Returns a list of valid preconfigured color options for a spotlight's background.
  def self.preconfigured_color_names
    preconfigured_colors.keys
  end

  # Public: Returns a list of valid pattern options for a spotlight's background.
  def self.pattern_names
    patterns.keys
  end

  # Public: Returns a list of two HTML color codes representing the gradient color stops for the
  # given preconfigured color option, to style the spotlight's background.
  def self.color_stops_for(preconfigured_color)
    PRECONFIGURED_COLOR_STOPS[preconfigured_color.to_sym]
  end

  # Public: Is the given repository already at the limit for how many discussion spotlights
  # it can have?
  #
  # repo - a Repository
  #
  # Returns a Boolean.
  def self.repository_at_limit?(repo)
    repo.discussion_spotlights.count >= LIMIT_PER_REPOSITORY
  end

  def discussion_author
    discussion.author
  end

  # Public: Returns HTML to render the emoji, or nil.
  def emoji_html
    if emoji.present?
      GitHub::Goomba::ProfileBioPipeline.to_html(emoji, base_url: GitHub.url)
    end
  end

  # Public: Returns the other DiscussionSpotlights for this repository, excluding this one.
  def other_repo_spotlights
    repository.discussion_spotlights.where.not(id: id)
  end

  # Public: Returns the count of discussion spotlights the repository has other than this one.
  def other_repo_spotlights_count
    return 0 unless repository
    other_repo_spotlights.count
  end

  private

  # Private: Expands colors of three character length `"000"`
  # into six character hex code `"000000"`.
  def expand_custom_color_shorthand
    if self.custom_color =~ COLOR_SHORTHAND_REGEXP
      c = self.custom_color
      self.custom_color = [c[0] * 2, c[1] * 2, c[2] * 2].join
    end
  end

  def set_repository
    self.repository = discussion&.repository
  end

  def repo_spotlight_limit
    return 1 unless repository
    LIMIT_PER_REPOSITORY
  end

  def ensure_at_least_one_color_option_set
    if preconfigured_color.blank? && custom_color.blank?
      errors.add(:base, "Either a preconfigured color or a custom color must be set.")
    end
  end

  def set_position
    self.position = other_repo_spotlights_count + 1
  end
end
