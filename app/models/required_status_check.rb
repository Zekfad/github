# frozen_string_literal: true

# Required status checks are in support of Protected Branches.
# See ProtectedBranch class for more # information.
#
# The existence of a protected branch status for a status context indicates
# that a successful context must be associated with the commit to update the
# target branch.
class RequiredStatusCheck < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :protected_branches
  include GitHub::UTF8
  include GitHub::Validations
  include Instrumentation::Model

  # These are for special casing travis contexts.
  #
  # See https://github.com/github/repos/issues/143 for details
  TRAVIS_SPECIAL_CASE_CONTEXT = "continuous-integration/travis-ci"
  TRAVIS_PUSH_CONTEXT = "continuous-integration/travis-ci/push"
  TRAVIS_PR_CONTEXT = "continuous-integration/travis-ci/pr"
  MAXIMUM_CONTEXT_BYTESIZE = 1020

  belongs_to :protected_branch

  validates :protected_branch_id, presence: true
  validates :context, presence: true, bytesize: { maximum: MAXIMUM_CONTEXT_BYTESIZE },
    unicode: true, uniqueness: { scope: :protected_branch_id, case_sensitive: true }
  validate :limit_statuses, on: :create

  after_create_commit :instrument_create
  after_destroy_commit :instrument_destroy

  # The maximum number of RequiredStatusChecks allowed per ProtectedBranch.
  MAX_PER_BRANCH = Status::MAX_PER_SHA_AND_CONTEXT

  # Description of expected status in the merge box.
  DESCRIPTION = "Waiting for status to be reported"

  # Public: Get all RequiredStatusChecks for a set of ProtectedBranches
  #
  # protected_branches - Array of ProtectedBranches
  #
  # Returns an Array of RequiredStatusChecks
  def self.for_branches(protected_branches)
    checks = self.where(protected_branch_id: protected_branches)

    # Fill in RequiredStatusCheck#protected_branch in case it is accessed later.
    branches_by_id = protected_branches.index_by(&:id)
    checks.each do |check|
      branch = branches_by_id[check.protected_branch_id]
      check.association(:protected_branch).target = branch
    end

    # Fill in ProtectedBranch#required_status_checks in case it is accessed later.
    checks_by_branch = checks.group_by(&:protected_branch_id)
    protected_branches.each do |branch|
      branch.association(:required_status_checks).target = checks_by_branch[branch.id] || []
    end

    checks
  end

  # Public: Get repository association.
  #
  # Delegates through protected branch parent.
  #
  # Returns Repository.
  def repository
    @repository ||= protected_branch.repository
  end

  # Override reading the context to guarantee returning valid utf8 encoded data.
  #
  # See also GitHub::UTF8
  def context
    utf8(read_attribute(:context))
  end

  ###
  # The following methods provide a StatusCheck ducktype
  ###

  def duration_in_seconds
    0
  end

  def required_for_pull_request?(pull)
    async_required_for_pull_request?(pull).sync
  end

  def async_required_for_pull_request?(pull)
    Promise.resolve(true)
  end

  def application
    nil
  end

  def creator
    nil
  end

  def target_url
    nil
  end

  def state
    StatusCheckRollup::EXPECTED
  end

  def state_changed_at
    Time.now
  end

  def sort_order
    [CheckRun::MAX_NUMBER_VALUE, StatusCheckRollup::STATE_SORT_ORDER[state], context]
  end

  def description
    DESCRIPTION
  end

  def contextual_name
    context
  end

  private

  # Private: Validation to limit the number of RequiredStatusCheck's per ProtectedBranch
  #
  # Returns nothing.
  def limit_statuses
    if protected_branch_limit_reached?
      errors.add :base, "maximum number of status contexts for this branch reached"
    end
  end

  # Private: Returns Boolean indicating if we have reached the limit of RequiredStatusChecks
  def protected_branch_limit_reached?
    protected_branch_id &&
      self.class.where(protected_branch_id: protected_branch_id).count >= MAX_PER_BRANCH
  end

  # Private: Instrument creation of this ProtectedBranch
  def instrument_create
    instrument :create
  end

  # Private: Instrument deletion of this record with the actor who did it
  def instrument_destroy
    instrument :destroy
  end

  def event_payload
    if protected_branch
      repo = protected_branch.repository
    end

    payload = {
      event_prefix => self,
      :protected_branch_id => protected_branch_id,
      # protected_branch can be nil in the case of cascading destroys
      :protected_branch_name => protected_branch && protected_branch.name,
      :repo => repo,
      :context => context,
    }

    if repo && repo.in_organization?
      payload[:org] = repo.organization
    end

    payload
  end
end
