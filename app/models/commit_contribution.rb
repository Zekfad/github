# rubocop:disable Style/FrozenStringLiteralComment

# Tracks which repositories a user has committed to.
#
# We don't track the individual commits, just the total number.
class CommitContribution < ApplicationRecord::Collab
  areas_of_responsibility :repositories, :user_profile

  DATA_CUTOFF_DATE = Time.utc(2012, 12, 04) # Date we started tracking commit contributions
  DELETE_BATCH_SIZE = 50
  INSERT_BATCH_SIZE = 20

  # See https://github.com/github/github/issues/61784#issuecomment-247716619
  LARGE_SCALE_CONTRIBUTOR_LIMIT = 40_000

  scope :no_ghost_users, -> { where("user_id <> ?", User.ghost.id) }
  scope :for_repository, ->(repo) { where(repository_id: repo) }
  scope :for_user, ->(user) { where(user_id: user) }

  belongs_to :user
  belongs_to :repository

  def contributed_on
    committed_date
  end

  # Count of distinct contributors to a given repository
  #
  # repository – The repository to get the commit contribution count from
  #
  # Returns an Integer count.
  def self.contributors_count_for_repository(repository)
    ActiveRecord::Base.connected_to(role: :reading) do
      self.for_repository(repository).no_ghost_users.distinct.count("user_id")
    end
  end

  # Queue up a ContributionsTrackPush job.
  #
  # push - The Push to track.
  #
  # Returns nothing.
  def self.track_push(push)
    ContributionsTrackPushJob.perform_later(push)
  end

  # Given a Push object, finds the authors of the commits and records
  # their contributions.
  #
  # Only records commits on the default branch and the gh-pages branch.
  #
  # push - A Push object.
  #
  # Returns nothing.
  def self.track_push!(push, user = nil)
    repo = push.repository
    return unless repo

    # Only check master and gh-pages branches
    return unless track_commits_in_branch?(repository: repo, branch: push.branch_name)

    # Ignore forks
    return if repo.fork?

    GitHub.dogstats.time "commit_contributions.track_push" do
      commit_counts_by_author_and_date = {}
      Promise.all(push.commits.map { |commit|
        commit.async_authors.then do |authors|
          # Use the date the commit was authored.
          date = commit.contributed_on

          report_author_count(authors.count) unless user

          authors.each do |author|
            next if user && author != user
            commit_counts_by_author_and_date[author] ||= Hash.new(0)
            commit_counts_by_author_and_date[author][date] += 1
          end
        end
      }).sync

      rows = []

      commit_counts_by_author_and_date.each do |author, dates|
        next unless repo.pullable_by?(author)

        dates.each do |date, count|
          rows << [author.id, repo.id, date, count, GitHub::SQL::NOW, GitHub::SQL::NOW]
        end
      end

      GitHub.dogstats.histogram("commit_contributions.track_push.push_commit_count", push.commits.length)
      GitHub.dogstats.histogram("commit_contributions.track_push.row_count", rows.length)
      GitHub.dogstats.histogram("commit_contributions.track_push.updated_author_count", commit_counts_by_author_and_date.length)

      rows.each_slice(INSERT_BATCH_SIZE) do |chunk|
        throttle do
          github_sql.run(<<-SQL, rows: GitHub::SQL::ROWS(chunk))
            INSERT INTO commit_contributions
              (user_id, repository_id, committed_date, commit_count, created_at, updated_at)
            VALUES :rows
            ON DUPLICATE KEY UPDATE
              commit_count = commit_count + VALUES(commit_count),
              updated_at = VALUES(updated_at)
          SQL
        end
      end

      # Ensure the commits we found count towards the author's contributions.
      commit_counts_by_author_and_date.each_key do |author|
        Contribution.clear_caches_for_user(author)
      end
    end
  end

  # Queue up a BackfillContributions job if the given repo needs to be
  # backfilled. Runs `backfill!`.
  #
  # repo - Repository to backfill.
  #
  # Returns a Boolean indicating whether the job was queued or not.
  def self.backfill(repo, reset = false)
    if reset || backfill?(repo)
      ContributionsBackfillJob.perform_later(repo.id, reset)
      true
    else
      false
    end
  end

  # Should we backfill the repo?
  #
  # repo - Repository in question.
  #
  # Returns a Boolean.
  def self.backfill?(repo)
    !repo.fork? && repo.commit_contributions.count.zero?
  end

  # Given a Repository object, goes through the commit history and creates
  # CommitContribution records for all the previous commits.
  #
  # repo  - Repository to backfill
  # reset - Reset any accumulated data and backfill from scratch.
  #
  # Returns a Boolean indicating whether the repo was backfilled. When reset is
  # true, the backfill always occurs.
  def self.backfill!(repo, reset = false)
    raise ArgumentError, "invalid repo: nil" if repo.nil?
    if reset || backfill?(repo)
      GitHub.dogstats.time "commit_contributions.backfill" do
        clear_contributions(repo)
        branches(repo).each { |branch| backfill_branch(repo, branch) }
        GitHub::RepoGraph.clear_cache(repo, "contributors")

        # Add ghost as a contributor if no contributors were found
        if repo.commit_contributions.count.zero?
          CommitContribution.create(user: User.ghost, repository: repo, committed_date: Date.new(1970, 1, 1), commit_count: 0)
        end
      end

      true
    else
      false
    end
  end

  def self.clear_contributions(repo)
    repo.commit_contribution_ids.each_slice(DELETE_BATCH_SIZE) do |slice|
      throttle do
        github_sql.run <<-SQL, ids: slice
          DELETE FROM commit_contributions WHERE id IN :ids
        SQL
      end
    end
  end

  def self.backfill_user!(repo, user)
    raise ArgumentError, "invalid repo: nil" if repo.nil?
    raise ArgumentError, "invalid user: nil" if user.nil?
    GitHub.dogstats.time "commit_contributions.backfill_user" do

      ids = ActiveRecord::Base.connected_to(role: :reading) do
        # this is only called from a job so its resilient in that way and we
        # don't need to wrap this in a resilient response
        sql = github_sql.new <<-SQL, repo_id: repo.id, user_id: user.id
          SELECT id
          FROM   commit_contributions
          WHERE  repository_id = :repo_id
          AND    user_id = :user_id
        SQL
        sql.values
      end

      ids.each_slice(DELETE_BATCH_SIZE) do |slice|
        throttle do
          github_sql.run <<-SQL, ids: slice
            DELETE FROM commit_contributions WHERE id IN :ids
          SQL
        end
      end

      branches(repo).each { |branch| backfill_branch(repo, branch, user) }
      GitHub::RepoGraph.clear_cache(repo, "contributors")
    end
    true
  end

  CommitContributionJobInfo = Struct.new(:repository, :commits, :branch_name)
  # Given a Repository object and a branch, goes through the commit history
  # and creates CommitContribution records for all the commits.
  #
  #   repo - Repository to track
  # branch - String branch name to track
  #
  # Returns nothing.
  def self.backfill_branch(repo, branch, user = nil)
    page = 1
    if commit_oid = repo.ref_to_sha(branch)
      while (commits = repo.commits.paged_history(commit_oid, page, 500)).any?
        page += 1
        push = CommitContributionJobInfo.new(repo, commits, branch)
        track_push!(push, user)
      end
    end
  end

  # Which branches count towards contributions?
  #
  # repo - Repository to check for.
  #
  # Returns an Array of String branch names.
  def self.branches(repo)
    [repo.default_branch, "gh-pages"].uniq
  end

  # Public: Returns true if commits in the branch should be counted for the given repository.
  #
  # branch - a string branch name
  # repository - a Repository instance
  #
  # Returns a Boolean.
  def self.track_commits_in_branch?(branch:, repository:)
    branches(repository).include?(branch)
  end

  # Which branches count towards contributions?
  #
  # Returns an Array of String branch names.
  def branches
    self.class.branches(repository)
  end

  def self.report_author_count(count)
    range = count.clamp(1, 5)
    range = "5-or-more" if range == 5
    range = "range:#{range}"
    GitHub.dogstats.increment("commit_contributions.author_count", tags: [range])
  end
end
