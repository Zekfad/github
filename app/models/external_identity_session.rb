# rubocop:disable Style/FrozenStringLiteralComment

class ExternalIdentitySession < ApplicationRecord::Domain::Users
  belongs_to :user_session
  belongs_to :external_identity

  delegate :target, to: :external_identity

  validates :user_session, presence: true
  validates :external_identity, presence: true
  validates :expires_at, presence: true

  before_validation :set_default_expiration

  scope :active,  -> { where("external_identity_sessions.expires_at > ?", Time.now) }
  scope :expired, -> { where("external_identity_sessions.expires_at <= ?", Time.now) }

  # Used by `UserSession` to find the active session for a specific identity.
  scope :by_identity, -> (identity) { where(external_identity_id: identity.id) }

  scope :by_saml_provider, -> (provider) { joins(:external_identity).
    where(external_identities: {  provider_id: provider.id, provider_type: provider.class.name  })
  }

  private

  def set_default_expiration
    self.expires_at ||= 1.day.from_now
  end
end
