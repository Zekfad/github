# rubocop:disable Style/FrozenStringLiteralComment

require "posix-spawn"
require "digest/sha1"

class Page < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :pages

  VALID_SOURCES = ["gh-pages", "master", "master /docs"].freeze
  VALID_LEGACY_BRANCHES = ["master", "gh-pages"].freeze
  VALID_SUBDIRS = ["/", "/docs"].freeze

  class PageError < StandardError
    def areas_of_responsibility
      [:pages]
    end
  end

  class PageBuildFailed < PageError; end
  class InvalidCNAME < PageError; end

  include GitHub::CacheLock
  include Instrumentation::Model
  extend GitHub::BackgroundDependentDeletes # used by destroy_dependents_in_background

  belongs_to :repository
  has_many :deployments, -> { order("updated_at desc") }, dependent: :delete_all

  has_many :builds, -> { order("updated_at desc") }
  destroy_dependents_in_background :builds

  before_validation :set_default_source_fields
  before_validation :set_cname
  before_validation :set_404
  before_validation :set_https_redirect
  before_validation :set_hsts_max_age
  before_validation :validate_legacy_source
  before_create :set_visibility
  before_create :legacy_backport_source_fields
  before_update :legacy_backport_source_fields
  after_create :track_page_creation
  after_destroy :destroy_dependent_pages_replicas
  after_destroy :instrument_destroy
  after_update :instrument_https_redirect_toggled, if: :saved_change_to_https_redirect?
  after_update :instrument_cname, if: :saved_change_to_cname?
  after_update :instrument_source, if: :saved_change_to_source?
  after_update :instrument_visibility, if: :saved_change_to_public?

  after_create :create_cname_certificate, if: :cname?
  after_update :create_cname_certificate, if: :saved_change_to_cname?
  after_create :create_subdomain_certificate, if: :subdomain?
  after_update :create_subdomain_certificate, if: :saved_change_to_subdomain?
  after_create :toggle_subdomain
  after_update :toggle_subdomain, if: :saved_change_to_public?


  # We rely on Owner a lot, which goes through repository, so lets delegate
  delegate :owner, to: :repository

  def async_owner
    async_repository.then do |repository|
      next unless repository
      repository.async_owner
    end
  end

  # Reset memoized variables on reload. Defined in ApplicationRecord::Base, called in #reload.
  def reset_memoized_attributes
    [
      :@async_primary,
      :@async_https_available,
      :@async_https_redirect_required,
      :@async_certificate,
      :@certificate,
      :@async_certificate_domain,
      :@certificate_domain,
    ].each do |instance_var|
      remove_instance_variable(instance_var) if instance_variable_defined?(instance_var)
    end
  end

  # Returns true if the page visibility has been set to private
  def private?
    !public?
  end

  # Determine if this is a user pages repository. User pages repositories are
  # named like "<user>.github.com" and are published to the root of the user's
  # namespace.
  #
  # Returns a boolean.
  def primary?
    async_primary?.sync
  end

  def async_primary?
    return @async_primary if defined?(@async_primary)

    @async_primary = async_repository.then do |repository|
      next false unless repository
      repository.async_is_user_pages_repo?
    end
  end

  def async_source_branch
    # Load the owner association explicitly to fix Graphql Platform::Errors::AssociationRefused: Refusing to load the `owner` association on `Repository` while resolving `PageDeployment.url`!
    async_owner.then { |owner|
      # Use new source_ref_name logic if available (and feature flagged)
      if GitHub.flipper[:pages_any_branch].enabled?(owner) && source_ref_name
        source_ref_name

      # Use legacy logic
      elsif source == "master" || source == "master /docs"
        "master"
      else
        async_primary?.then { |is_primary| is_primary ? "master" : "gh-pages" }
      end
    }
  end

  # The branch name for Pages builds.
  def source_branch
    async_source_branch.sync
  end

  # The source directory for Pages builds.
  def source_dir
    # Use new source_subdir logic if available (and feature flagged)
    # Load the owner association explicitly to fix Graphql Platform::Errors::AssociationRefused: Refusing to load the `owner` association on `Repository` while resolving `PageDeployment.sourcePath`!
    owner = async_owner.sync
    return source_subdir if GitHub.flipper[:pages_any_branch].enabled?(owner) && source_subdir

    if source == "master /docs"
      "/docs"
    else
      "/"
    end
  end

  # True if a source with a non-root subdir is configured for Pages builds
  def subdir_source?
    source_dir != "/"
  end

  # Validate the legacy source value before validation
  def validate_legacy_source
    set_source(self.source)
  end

  # Set the source from which to build a page
  #
  # When :pages_any_branch is enabled, source should be set to nil and only ref_name and subdir should be passed
  def set_source(source, ref_name = nil, subdir = nil)
    if GitHub.flipper[:pages_any_branch].enabled?(async_owner.sync) && !ref_name.nil? && !subdir.nil?
      # Set a backward compatible value for source (in case we disable the feature flag)
      if (ref_name == "master" && subdir == "/")
        write_attribute :source, "master"
      elsif (ref_name == "master" && subdir == "/docs")
        write_attribute :source, "master /docs"
      else
        # Note: this value is not necessarily correct if ref_name was a total arbitrary branch
        # that means in theory if we were to use this field again, we would need to rebuild the page
        write_attribute :source, nil
      end
      # Update the source fields
      write_attribute :source_ref_name, ref_name
      write_attribute :source_subdir, subdir

      # Finally save the model once (calling update_attribute for each field would call the callbacks multiple times and meddle with the value)
      save
    # Use legacy logic for setting the source (and source only) - this will call the before create/update callbacks
    elsif (source == "master" || source == "master /docs") && (!primary? || GitHub.flipper[:pages_any_branch].enabled?(async_owner.sync))
        update_attribute :source, source
    else
      update_attribute :source, nil
    end
  end

  # clean page
  def clear_source
    write_attribute :source, nil
    write_attribute :source_ref_name, nil
    write_attribute :source_subdir, nil
    save
  end

  # Prior the pages_any_branch feature flag, initialize the following two columns in the model:
  # - source_ref_name
  # - source_subdir
  #
  # Using values matching the current source field.
  def legacy_backport_source_fields
    # Skip this altogether if the feature flag is set and the two fields were initialized already
    return if GitHub.flipper[:pages_any_branch].enabled?(async_owner.sync) && !self.source_ref_name.nil? && !self.source_subdir.nil?

    # On a project/user page, only master and / are allowed (source is nil)
    if primary?
      self.source_ref_name = "master"
      self.source_subdir = "/"

    # master and /
    elsif self.source == "master"
      self.source_ref_name = "master"
      self.source_subdir = "/"

    # master and /docs
    elsif self.source == "master /docs"
      self.source_ref_name = "master"
      self.source_subdir = "/docs"

    # gh-pages and / (source is nil)
    else
      self.source_ref_name = "gh-pages"
      self.source_subdir = "/"
    end
  end

  def set_default_source_fields
    if GitHub.flipper[:pages_any_branch].enabled?(async_owner.sync) && self.source_ref_name.nil? && self.source_subdir.nil? && self.source.nil?
      if primary?
        self.source_ref_name = repository.default_branch
        self.source_subdir = "/"
      elsif repository.has_gh_pages_branch?
        self.source_ref_name = "gh-pages"
        self.source_subdir = "/"
      end
    end
  end

  # Do we have a certificate for serving HTTPS requests for this page?
  #
  # Returns Promise which resolves to a boolean.
  def async_https_available?
    return @async_https_available if defined?(@async_https_available)

    return @async_https_available = Promise.resolve(false) unless GitHub.pages_https_redirect_enabled?

    @async_https_available = url.async_host.then do |url_host|
      next true if GitHub.pages_https_domains.include?(url_host)

      url.async_pages_host_name.then do |url_pages_host_name|
        next true if url_host.ends_with?(".#{url_pages_host_name}")

        async_certificate.then do |certificate|
          !!certificate&.usable?  # double bang in case this result can make it back
          # to the platform interface which is probably expecting a boolean, not a boolean-ish
        end
      end
    end
  end

  def https_available?
    async_https_available?.sync
  end

  # Is it required that this Page have https_redirect enabled?
  #
  # Returns a Promise which resolves to a boolean.
  def async_https_redirect_required?
    return @async_https_redirect_required if defined?(@async_https_redirect_required)

    return @async_https_redirect_required = Promise.resolve(false) unless GitHub.pages_https_redirect_enabled?

    @async_https_redirect_required = url.async_host.then do |url_host|
      next true if url_host.ends_with? ".#{GitHub.pages_host_name_v1}"
      next false unless url_host.ends_with?(".#{GitHub.pages_host_name_v2}") || GitHub.pages_https_domains.include?(url_host)

      async_https_required_for_repository?.then do |https_required|
        next async_https_available? if https_required

        false
      end
    end
  end

  def https_redirect_required?
    async_https_redirect_required?.sync
  end

  # If HTTPS is required for the repository due to the time it was created.
  #
  # Returns a Promise which resolves to a Boolean value.
  # The feature flag must be enabled, and the repository must have been
  # created after the given date where we began requiring this.
  def async_https_required_for_repository?
    async_repository.then do |repository|
      next false unless repository
      repository.created_at > GitHub.pages_https_required_after
    end
  end

  # Can the owner toggle HTTPS redirects for this repo?
  #
  # Returns boolean.
  def https_redirect_toggleable?
    https_available? && !https_redirect_required?
  end

  # Is HSTS required for this site?
  #
  # Returns boolean.
  def hsts_required?
    https_redirect_required? && owner.created_at > GitHub.pages_https_required_after
  end

  # Force the https_redirect value based on whether it is allowed/required.
  #
  # Returns true.
  def set_https_redirect
    return if repository.nil?

    if https_redirect_required?
      self.https_redirect = true
    elsif !https_available?
      self.https_redirect = false
    end

    true
  end

  # For the hsts_max_age value for new users.
  #
  # Returns true
  def set_hsts_max_age
    self.hsts_max_age = if hsts_required?
       1.year.to_i
    elsif https_available?
       # Do not force HSTS for all Pages sites since we might not have certs for them.
       # This change allows us to enable HSTS on the console for specific sites.
       # If they have are HTTPS-able, then let us set an HSTS value.
       hsts_max_age
    else
      nil
    end

    true
  end

  # The certificate associated with this Page's CNAME.
  #
  # Returns a Promise which resolves to an Page::Certificate instance or nil.
  def async_certificate
    return @async_certificate if defined?(@async_certificate)

    @async_certificate = async_certificate_domain.then do |host|
      next nil unless host
      Platform::Loaders::PageCertificateByDomain.load(host)
    end
  end

  # The certificate associated with this Page's CNAME.
  #
  # Returns a Page::Certificate instance or nil.
  def certificate
    @certificate ||= async_certificate.sync
  end

  # The domain the certificate should be issued for.
  #
  # Returns a Promise which resolves to a string hostname or nil if no custom domain is specified.
  def async_certificate_domain
    return @async_certificate_domain if defined?(@async_certificate_domain)

    @async_certificate_domain = url.async_custom_domain?.then do |has_custom_domain|
      next nil unless has_custom_domain
      url.async_host
    end
  end

  # The domain the certificate should be issued for.
  #
  # Returns a string hostname or nil if no custom domain is specified.
  def certificate_domain
    return @certificate_domain if defined?(@certificate_domain)
    @certificate_domain = async_certificate_domain.sync
  end

  # Writes a new subdomain which only has a normalized repo name
  # that only allows a-z (no uppercase), 0-9, and a hyphen (-)
  # If we detect that an org has two repos that conflict (e.g. they have both repo_a and repo-a),
  # we'll have to store one with a random number at the end.
  # Returns the value written to the database or `nil` if an existing subdomain was removed
  def toggle_subdomain
    return nil if primary?
    return nil unless repository.plan_supports?(:private_pages)
  # If this page is now public and had a subdomain, remove it

    return reset_subdomain if public?

    subdomain_with_org = normalize_subdomain(repository.name)

    begin
    update_attribute :subdomain, subdomain_with_org
    rescue ActiveRecord::RecordNotUnique
      subdomain_with_org = normalize_subdomain(repository.name, true)
      update_attribute :subdomain, subdomain_with_org
    end
    subdomain_with_org
  end

  # Whether unique subdomains are enabled for this page
  def unique_subdomains_enabled?
    # Unique subdomains are not available for <user>.github.io or public pages
    return !repository.page.primary? || repository.page.private?
  end

  # A page URL, represented as a distinct class.
  #
  # Returns a Page::URL instance for the current Page.
  def url
    @url ||= Page::URL.new(self)
  end

  # Enqueue a page-build job for this page.
  #
  # Returns nothing.
  def publish(pusher = nil, git_ref_name: nil)
    return unless GitHub.pages_enabled?
    return unless repository&.plan_supports?(:pages)
    return if owner&.has_any_trade_restrictions? && repository&.private?

    GitHub.dogstats.increment "pages.build_jobs"

    # This build is initiated by a staff user that would otherwise not have push access
    if GitHub.guard_audit_log_staff_actor? && pusher.site_admin? && !repository.pushable_by?(pusher)
      actor_hash = GitHub.guarded_audit_log_staff_actor_entry(pusher)
      pusher = repository.gh_pages_rebuilder(User.staff_user)
      repository.instrument :pages_build, actor_hash
    end

    args = [id, pusher&.id]
    args << {"git_ref_name" => git_ref_name} if git_ref_name.present?
    PageBuildJob.set(queue: page_queue).perform_later(*args)

    GlobalInstrumenter.instrument "pages.build", {
      actor: pusher,
      page: self,
    }

    true
  end

  def page_queue
    self.class.page_queue
  end

  def unpublish
    self.class.transaction do
      raise ActiveRecord::Rollback unless update(built_revision: nil)
      delete_built_site_data
    end
  end

  # The queue to use when building new pages.
  def self.page_queue
    if build_in_docker?
      "pages-docker"
    else
      :page
    end
  end

  def self.build_in_docker?
    Rails.production? && !GitHub.enterprise?
  end

  def lock_build
    cache_lock_obtain("page:#{id}:lock")
  end

  def unlock_build
    cache_lock_release("page:#{id}:lock")
  end

  def lock_build_for_pusher(pusher)
    cache_lock_obtain("page:pusher:#{pusher.id}:lock")
  end

  def unlock_build_for_pusher(pusher)
    cache_lock_release("page:pusher:#{pusher.id}:lock")
  end

  # Public: Get an auth token for the user and session.
  #
  # session - the UserSession instance to which the token should be tied.
  #
  # Returns the String token.
  def auth_token(session:)
    @token ||= GitHub::Authentication::SignedAuthToken.generate(
      session: session,
      scope: auth_token_scope,
      expires: GitHub.user_session_timeout.from_now,
    )
  end

  def auth_token_scope
    "PrivatePages:#{repository.id}"
  end

  # The new shiny delete_pages for the dpages world
  def delete_built_site_data
    self.class.transaction do
      update_attribute :built_revision, nil

      destroy_dependent_pages_replicas
    end
  end

  def subdomain_with_host
    return nil if subdomain.nil? || subdomain.blank?
    # repos in the github org automatically get routed on "github.com"
    return "#{subdomain}.github.com" if owner.login == "github"
    return "#{subdomain}.#{GitHub.pages_host_name_v2}"
  end

  # Clear the cname, regardless of whether there is a valid CNAME blob.
  # Used when a domain has been claimed by a user who does not own it.
  def clear_cname
    update_attribute :cname, nil
  end

  # Commit new CNAME blob to the head of the pages branch and trigger rebuild
  # Removes CNAME file if input is blank.
  #
  # Returns new cname or "" on success, nil if no change
  def write_cname(cname, committer)
    old_cname = cname_from_blob
    new_cname = normalize_cname(cname)
    return nil if repository.nil? || pages_ref.nil? || new_cname == old_cname
    Page::CName.new(self, new_cname).validate if new_cname.present?
    op = if !cname_exists?
      "Create"
    elsif new_cname.present?
      "Update"
    else
      "Delete"
    end
    repository.heads.read(repository.pages_branch).append_commit(
      {message: "#{op} CNAME", committer: committer},
      committer,
    ) do |files|
      if op == "Delete"
        files.remove(cname_path)
      else
        files.add(cname_path, new_cname)
      end
    end

    # rebuild ensures immediate save and enables cascading builds
    repository.rebuild_pages committer

    # Publish cname_change event to Hydro
    GlobalInstrumenter.instrument "pages.cname_change", {
      cname: new_cname,
      old_cname: old_cname,
      actor: committer,
      page: self,
    }

    new_cname || ""
  end

  # When create a new GitHub pages, check repository visibility and enforce to pages.
  def set_visibility
    if repository && GitHub.flipper[:private_pages].enabled?(repository.owner) && repository.plan_supports?(:private_pages)
      self.public = repository.public?
    end
  end

  # Determine if this is a nojekyll page
  #
  # Returns true if the source dir contains a .nojekyll file.
  def nojekyll?
    repository.includes_file?(source_file_path(".nojekyll"), repository.pages_branch)
  end

  # Read theme from config
  def theme
    return nil if config.nil?
    config[:theme]
  end

  # Write theme to config. Commit on save! should trigger a build.
  def write_theme(theme_gem, committer)
    return if config.nil?
    config[:theme] = theme_gem
    config.save!(committer, "Set theme #{theme_gem}")
  end

  # JekyllConfig for this page
  def config
    return @config if defined?(@config)
    return @config = nil if nojekyll?
    @config = GitHub::Pages::JekyllConfig.new self
  end

  # Whether preview deployments are enabled for this page.
  def preview_deployments_enabled?
    repository && repository.owner && GitHub.flipper[:pages_preview_builds].enabled?(repository.owner)
  end

  # If a preview deployment exists for the given ref_name on this repository.
  #
  # git_ref_name - a String ref name to search for, e.g. "branch-build" (omit "refs/heads")
  #
  # Returns true if any Page::Deployment exists for the given git_ref_name
  def deployment_enabled_for?(git_ref_name)
    deployments_for(git_ref_name).exists?
  end

  # The ActiveRecord::Association for the Page::Deployment association with the given git_ref_name
  #
  # git_ref_name - a String ref name to search for, e.g. "branch-build" (omit "refs/heads")
  #
  # Returns the ActiveRecord::Association
  def deployments_for(git_ref_name)
    deployments.where(ref_name: git_ref_name)
  end

  # The Page::Deployment or the given git_ref_name
  #
  # git_ref_name - a String ref name to search for, e.g. "branch-build" (omit "refs/heads")
  #
  # Returns the Page::Deployment instance for the given git_ref_name or nil if none exists
  def deployment_for(git_ref_name)
    deployments_for(git_ref_name).first
  end

  # Create or find the deployment for a specific ref_name
  # The revision will be null for new deployments
  # ONLY lib/github/pages/builder.rb should modify revisions on deployments
  def create_or_find_deployment_for(ref_name)
    preexisting = deployments_for(ref_name).first
    return preexisting if preexisting.present?

    begin
      Page::Deployment.create!(page_id: self.id, ref_name: ref_name)
    rescue ActiveRecord::RecordNotUnique => e
      # This happens when the deployment.token clashes with another deployment token
      # for the same page_id. The token must be unique based on the page_id.
      # In a best-effort, we'll give this another try.
      Page::Deployment.create!(page_id: self.id, ref_name: ref_name)
    end
  end

  # Find the Page's Page::Deployment for its source branch.
  #
  # Returns a Promise which resolves to a Page::Deployment or nil.
  def async_primary_deployment
    Platform::Loaders::PageDeploymentByRefName.load(id, source_branch)
  end

  # Create a Page cname certificate
  #
  # Returns true if the certificate was created, false otherwise.
  def create_cname_certificate
    return create_certificate(cname)
  end

  # Create a Page subdomain certificate
  #
  # Returns true if the certificate was created, false otherwise.
  def create_subdomain_certificate
    return create_certificate(subdomain_with_host)
  end

  # Create a Page::Certificate for this page.
  #
  # Returns true if the certificate was created, false otherwise.
  def create_certificate(domain_name)
    return false unless eligible_for_certificate?

    # Certificate for domain name already exists.
    reload # clear the caching of any previously nil `@certificate` value
    return false if certificate.present?

    cert = begin
             Page::Certificate.create(domain: domain_name)
           rescue ActiveRecord::RecordNotUnique => e
             Failbot.report(e, app: "pages-certificates")
             return
           end

    !cert.new_record?
  end

  def eligible_for_certificate?

    # Our Let's Encrypt integration isn't enabled.
    return false unless GitHub.pages_custom_domain_https_enabled?

    # CNAME is blank and subdomain is blank.
    return false if cname.blank? && subdomain.blank?

    # Ensure the DNS is configured properly.
    return Page::Certificate.eligible?(cname) if cname
    return Page::Certificate.eligible?(subdomain_with_host) if subdomain
  end

  def get_normalized_subdomain(repo_name)
    return nil if repo_name.nil?

    subdomain = normalize_subdomain(repo_name)
    if subdomain_exists?(subdomain)
      subdomain = normalize_subdomain(repo_name, true)
    end
    self.subdomain = subdomain
  end

  def build_hostname(repo_name)
    if primary?
      url.async_host.then do |host|
        return host.to_s
      end
    end
    subdomain = get_normalized_subdomain(repo_name)
    subdomain_with_github = subdomain_with_host
  end

  def subdomain_exists?(subdomain_with_org)
    repo_id = repository.id
    row = ApplicationRecord::Domain::Repositories.github_sql.values <<-SQL, subdomain_with_org: subdomain_with_org, repo_id: repo_id
      SELECT * FROM pages
        WHERE subdomain = :subdomain_with_org
        and repository_id != :repo_id
    SQL

    return false if row.empty?
    true
  end

  private

  def instrument_source
    repository.instrument :pages_source,
      source: GitHub.flipper[:pages_any_branch].enabled?(async_owner.sync) ? "#{source_ref_name} #{source_subdir}" : source,
      old_source: source_before_last_save

      if source_branch == "master" || source_branch == "gh-pages"
        GitHub.dogstats.increment "pages.source", tags: ["branch:#{source_branch}", "dir:#{source_dir}"]
      else
        GitHub.dogstats.increment "pages.source", tags: ["branch:other", "dir:#{source_dir}"]
      end
  end

  def instrument_cname
    repository.instrument :pages_cname,
      cname: cname,
      old_cname: cname_before_last_save

    if cname.blank? && cname_before_last_save.present?
      GitHub.dogstats.increment "pages.cname", tags: ["action:removed"]
    elsif cname_before_last_save.blank? && cname.present?
      GitHub.dogstats.increment "pages.cnames", tags: ["action:added"]
    elsif cname.present? && cname_before_last_save.present?
      GitHub.dogstats.increment "pages.cnames", tags: ["action:changed"]
    end
  end

  def instrument_https_redirect_toggled
    return unless repository

    if https_redirect?
      GitHub.dogstats.increment "pages.https_redirect", tags: ["action:enabled"]
      repository.instrument :pages_https_redirect_enabled
    else
      GitHub.dogstats.increment "pages.https_redirect", tags: ["action:disabled"]
      repository.instrument :pages_https_redirect_disabled
    end
  end

  def instrument_visibility
    if public?
      GitHub.dogstats.increment "pages.visibility", tags: ["action:public"]
      repository.instrument :pages_public
    else
      GitHub.dogstats.increment "pages.visibility", tags: ["action:private"]
      repository.instrument :pages_private
    end
  end

  # Sets the CNAME from a file in the repository.
  #
  # Returns true.
  def set_cname
    cname = if !GitHub.pages_custom_cnames?
      nil
    elsif repository.nil? || pages_ref.nil?
      self.cname
    else
      cname_from_blob
    end

    if cname.nil? || cname_error(cname)
      self.cname = nil
    else
      self.cname = cname
    end

    true
  end

  # Normalize subdomain to remove characters from repo names
  # that are not allowed in hostnames
  def normalize_subdomain(repo_name, randomize = false)
    subdomain = repo_name.split(/([ _-])/).map(&:downcase).join
    normalized_subdomain = subdomain
                            .tr("_", "-")
                            .tr(".", "-")
                            .gsub(/\A\-+|\-+\z/, "")

    #Repo name and owner name can each only have at the max 64 characters. If it is more than that, they need to be shortened
    shortened_repo = normalized_subdomain[0..63]
    shortened_owner = owner.login[0..63]

    if randomize
      shortened_repo = normalized_subdomain[0..56]

      hash = Digest::SHA1.hexdigest repo_name
      shortened_repo = shortened_repo + "-" + hash[0..5]
    end

    subdomain_with_org = "#{shortened_repo}.#{shortened_owner}"
  end

  def reset_subdomain
    return nil if repository.nil?

    update_attribute :subdomain, nil if public?
  end

  # Sets the four_or_four from a file in the repository.
  #
  # Returns true.
  def set_404
    return if repository.nil? || pages_ref.nil?

    begin
      self.four_oh_four = !!(repository.blob(pages_ref.commit.oid, "404.html"))
    rescue GitRPC::InvalidObject
      # 404.html is a Git tree, not a blob. Ignore.
      self.four_oh_four = false
    end

    true
  end

  # Lookup the ref for the master or gh-pages branch depending on repo-name
  # not memoized - rely on repo refs collection being cached
  #
  # Returns Ref or nil
  def pages_ref(ref_name = nil)
    repository.refs.find(ref_name || repository.pages_branch)
  end
  public :pages_ref

  def tree_name
    repository.pages_branch
  end
  public :tree_name

  # Read index.html from the root of the pages branch
  #
  # Returns a String or nil if the files doesn't exist
  def index_html
    if pages_ref && (blob = repository.blob(pages_ref.commit.oid, source_file_path("index.html")))
      blob.data
    end
  rescue GitRPC::InvalidObject
    nil
  end
  public :index_html

  # Does repository have a CNAME blob?
  #
  # Returns boolean.
  def cname_exists?
    return true if pages_ref && repository.blob(pages_ref.commit.oid,  cname_path)
    false
  rescue GitRPC::InvalidObject
    false
  end
  public :cname_exists?

  # CNAME from blob, if one exists.
  #
  # Returns a String or nil.
  def cname_from_blob
    if pages_ref && (blob = repository.blob(pages_ref.commit.oid,  cname_path))
      normalize_cname(blob.data)
    end
  rescue GitRPC::InvalidObject
    nil
  end
  public :cname_from_blob

  # path of the CNAME file without a leading slash
  #
  # Returns a String
  def cname_path
    source_file_path("CNAME")
  end
  public :cname_path

  # Repository path of a file in the source dir without leading or trailing slash
  #
  # file = file path inside source-dir, defaults to blank = root
  #
  # Returns a String
  def source_file_path(file = "")
    File.join(source_dir, file).gsub(%r(\A/|/\Z), "")
  end
  public :source_file_path

  # Read a source file, if one exists.
  #
  # file_path is the path inside the source directory
  #
  # Returns blob.data (usually a String) or nil
  def source_file(file_path)
    return if pages_ref.nil?
    blob = repository.blob(pages_ref.commit.oid, source_file_path(file_path))
    blob.data unless blob.nil?
  rescue GitRPC::InvalidObject
    nil
  end
  public :source_file

  # Fetch the source directory
  #
  # Returns a Directory or nil if no pages-branch or no source dir
  def source_directory
    ref = repository.heads.read(source_branch)
    dir = source_file_path
    return nil if !ref.exist? || !repository.includes_directory?(dir, source_branch)
    repository.directory(ref.target_oid, dir)
  end
  public :source_directory

  # Normalizes a user-supplied cname
  #
  # Ignores everything after the first line, trims whitespace, downcases
  # Abstracted from cname_from_blob to make testing easier
  #
  # Returns the cname string, or nil if blank
  def normalize_cname(cname = "")
    cname = cname.split("\n").first.to_s.downcase.sub(/https?:\/\//, "").strip.sub(/[[:space:]]+/, "")

    cname = GitHub::Encoding.strip_bom(cname)

    cname unless cname.blank?
  end

  # Validate our CNAME from disk during the build process.
  # Can be called when self.cname is nil - always checks against the
  # content of the CNAME blob.
  #
  # Returns the cname error message string or nil if no error
  def cname_error(cname = cname_from_blob)
    nil if Page::CName.new(self, cname).validate
  rescue InvalidCNAME => e
    e.message
  end
  public :cname_error

  # Instrument Page creation.
  def track_page_creation
    repository.instrument :pages_create, cname: cname, source: GitHub.flipper[:pages_any_branch].enabled?(async_owner.sync) ? "#{source_ref_name} #{source_subdir}" : source
    # Publish visibility change event to Hydro after create page.
    GlobalInstrumenter.instrument "pages.visibility_change", {
      actor: repository.owner,
      page: self,
      public: self.public
    }
    GitHub.dogstats.increment "pages.sites"
    instrument_source if source
  end

  def instrument_destroy
    repository.instrument :pages_destroy, cname: cname, source: GitHub.flipper[:pages_any_branch].enabled?(async_owner.sync) ? "#{source_ref_name} #{source_subdir}" : source
    GitHub.dogstats.decrement "pages.sites"
  end

  def destroy_dependent_pages_replicas
    github_sql.run(<<-SQL, page_id: self.id)
      DELETE FROM pages_replicas WHERE page_id = :page_id
    SQL
  end
end
