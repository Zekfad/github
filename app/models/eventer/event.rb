# frozen_string_literal: true

module Eventer
  class Event
    RETRY_ON_PUBLISH_SLEEP = 1
    MAX_RETRIES = 5

    class << self

      # Public: Creates and publishes the described event to hydro.
      #
      # event_name  - The name that's associated with this metric.
      # owner - An object or array representing the owner(s) for this event.
      # timestamp - The timestamp for the metric.
      # bucket_size - The period for the rollup (day or hour). Default is day
      # tag - Optional. A user defined string associated with this metric.
      # async - Boolean indicating whether to log this event asynchronously or not.
      # publish_options - A hash of options that is merged into the options sent to Hydro::Publisher#publish.
      #
      # Returns nothing.
      def track(event_name, owner:, delta: nil, timestamp: Time.now.utc,
                bucket_size: Eventer::TimeBucket::BUCKET_SIZE_DAY, tag: nil, async: true, publish_options: {})
        tries = 0
        track_event_messages = Eventer::Messages::IncrementCounter.build(
          event_name,
          owner: owner,
          delta: delta,
          timestamp: timestamp,
          bucket_size: bucket_size,
          tag: tag,
        )

        publisher = async ? GitHub.hydro_publisher : GitHub.sync_hydro_publisher

        track_event_messages.each do |msg|
          begin
            # msg.first = the eventer message json
            # msg.last = the eventer message hydro options (schema and partition key), overridable by publish_options
            result = publisher.publish(msg.first, **msg.last.merge(publish_options))
            raise(result.error) unless result.success?
          rescue Hydro::Sink::Error, Kafka::DeliveryFailed => e
            if retry_publish_on?(e, tries += 1)
              sleep RETRY_ON_PUBLISH_SLEEP
              retry
            end

            GitHub.dogstats.increment("eventer.publish.track_event_error", tags: ["event:#{event_name}"])
            raise
          end
        end
      end

      # Public: Deletes all counters for the given event/owners.
      #
      # event_name  - The event name to delete all counters for.
      # owner - An object or array representing the owner(s) for this event.
      # async - Boolean indicating whether to log this event asynchronously or not.
      # publish_options - A hash of options that is merged into the options sent to Hydro::Publisher#publish.
      #
      # Returns nothing.
      def delete_counters(event_name:, owner:, async: true, publish_options: {})
        tries = 0
        delete_counters_messages = Eventer::Messages::DeleteCounters.build(
          event_name,
          owner: owner,
        )

        publisher = async ? GitHub.hydro_publisher : GitHub.sync_hydro_publisher

        delete_counters_messages.each do |msg|
          begin
            # msg.first = the eventer message json
            # msg.last = the eventer message hydro options (schema and partition key), overridable by publish_options
            result = publisher.publish(msg.first, **msg.last.merge(publish_options))
            raise(result.error) unless result.success?
          rescue Hydro::Sink::Error, Kafka::DeliveryFailed => e
            if retry_publish_on?(e, tries += 1)
              sleep RETRY_ON_PUBLISH_SLEEP
              retry
            end

            GitHub.dogstats.increment("eventer.publish.delete_counters_error", tags: ["event:#{event_name}"])
            raise
          end
        end
      end

      private

      def retry_publish_on?(error, tries)
        (
          error.kind_of?(Hydro::Sink::BufferOverflow) || error.kind_of?(Kafka::DeliveryFailed)
        ) && tries < MAX_RETRIES
      end
    end
  end
end
