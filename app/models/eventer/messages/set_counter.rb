# frozen_string_literal: true

module Eventer
  module Messages
    class SetCounter
      # SetCounter messages set an absolute value for the event being tracked, overwriting previous value.
      def self.build(event_name, owner:, delta: nil, timestamp: Time.now.utc, bucket_size: Eventer::TimeBucket::BUCKET_SIZE_DAY, tag: nil)
        Eventer::Messages.build_event_owners(owner).map do |o|
          message = {
            event_owner: o,
            event_type: {
              name: event_name,
            },
            time_bucket: Eventer::TimeBucket.build(timestamp: timestamp, size: bucket_size),
          }

          message[:count] = delta if delta
          message[:tag] = { name: tag } if tag

          [
            {set_counter: message},
            Eventer::Messages.hydro_options(o),
          ]
        end
      end
    end
  end
end
