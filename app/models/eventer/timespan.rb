# frozen_string_literal: true

module Eventer
  class Timespan
    include Enumerable

    class_attribute :duration

    def initialize(beginning:)
      @beginning = beginning

      @ending = Time.now
    end

    def to_range
      @beginning..@ending
    end

    class FullPeriod < Eventer::Timespan
      self.duration = 2.years

      def initialize(*)
        super(beginning: duration.before(Date.today.beginning_of_day))
      end

      def group_by(column)
        <<-SQL
      DATE(#{column})
        SQL
      end
    end
  end
end
