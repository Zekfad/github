# frozen_string_literal: true

module Eventer
  class Push
    class << self
      def instrument_push(push)
        ContributionsInstrumentPushJob.perform_later(push)
      end

      # This was largely lifted from CommitContribution.track_push!
      # This will aggregate all of the commit contributed dates and
      # instrument once for each date to eventer via hydro
      def instrument_push!(push)
        repo = push.repository

        return unless repo

        # Only check master and gh-pages branches
        return unless track_commits_in_branch?(repository: repo, branch: push.branch_name)

        # Ignore forks
        return if repo.fork?

        GitHub.dogstats.time "eventer.instrument_push!" do
          commit_counts_by_date = Hash.new(0)
          Promise.all(push.commits.map do |commit|
            commit_counts_by_date[commit.contributed_on] += 1
          end).sync

          commit_counts_by_date.each do |date, count|
            GlobalInstrumenter.instrument("commit.create", {
              repository: repo,
              contributed_on: date,
              count: count,
            })
          end
        end

      rescue GitRPC::ObjectMissing
        # do nothing, there's no commits to count
      end

      private

      def track_commits_in_branch?(branch:, repository:)
        [repository.default_branch, "gh-pages"].uniq.include?(branch)
      end
    end
  end
end
