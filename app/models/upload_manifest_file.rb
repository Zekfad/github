# rubocop:disable Style/FrozenStringLiteralComment

class UploadManifestFile < ApplicationRecord::Domain::Assets
  areas_of_responsibility :lfs
  include ::Storage::Uploadable
  include GitHub::Validations

  MAX_FILES_PER_MANIFEST = 100

  # Allow overriding value during tests.
  attr_writer :max_files_per_manifest

  set_uploadable_policy_path "upload-manifest-files"
  add_uploadable_policy_attributes :repository_id, :upload_manifest_id, :directory

  belongs_to :repository
  belongs_to :uploader, class_name: "User"
  belongs_to :manifest, class_name: "UploadManifest", foreign_key: :upload_manifest_id
  belongs_to :storage_blob, class_name: "Storage::Blob"

  before_validation :default_content_type

  validates :uploader, presence: true
  validates :repository, presence: true
  validates :manifest, presence: true
  validates :name, presence: true
  validates_format_of :name, with: /\A[[[:graph:]] ]+\z/
  validates :directory, length: { maximum: 1024 }
  validates :content_type, presence: true
  validates_inclusion_of :size, in: 1..25.megabytes
  validate  :uploader_access, on: :create
  validate  :file_count_limit, on: :create
  validates_with UploadDirectoryValidator

  validates_presence_of :storage_blob, if: lambda { GitHub.storage_cluster_enabled? }
  validate :storage_ensure_inner_asset

  before_create :choose_storage_provider
  before_destroy :storage_delete_object

  enum state: Storage::Uploadable::STATES

  # BEGIN storage settings

  def storage_policy(actor: nil, repository: nil)
    klass = if GitHub.storage_cluster_enabled?
      ::Storage::ClusterPolicy
    else
      ::Storage::S3Policy
    end
    klass.new(self, actor: actor, repository: self.repository)
  end

  def self.storage_new(uploader, blob, meta)
    new(
      content_type: meta[:content_type],
      directory: meta[:directory],
      name: meta[:name],
      repository_id: meta[:repository_id],
      size: meta[:size],
      storage_blob: blob,
      upload_manifest_id: meta[:upload_manifest_id],
      uploader: uploader,
    )
  end

  def self.storage_create(uploader, blob, meta)
    storage_new(uploader, blob, meta).tap do |file|
      file.update(state: :uploaded)
    end
  end

  def storage_external_url(_ = nil)
    "#{creation_url}/#{id}"
  end

  def upload_access_grant(actor)
    Api::AccessControl.access_grant(
      verb: :push,
      user: actor,
      resource: repository,
    )
  end

  # s3 storage settings

  def self.storage_s3_new_bucket
    GitHub.s3_upload_manifest_file_new_bucket
  end

  def self.storage_s3_new_bucket_host
    GitHub.s3_upload_manifest_file_new_host
  end

  def self.storage_s3_bucket
    GitHub.s3_environment_config[:asset_bucket_name]
  end

  def storage_s3_bucket
    if storage_provider == :s3_production_data
      self.class.storage_s3_new_bucket
    else
      self.class.storage_s3_bucket
    end
  end

  def storage_s3_key(policy)
    if storage_provider == :s3_production_data
      "#{repository_id}/#{id}"
    else
      "upload-manifest-files/#{repository_id}/#{id}"
    end
  end

  def storage_s3_access_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_access_key
    else
      GitHub.s3_environment_config[:access_key_id]
    end
  end

  def storage_s3_secret_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_secret_key
    else
      GitHub.s3_environment_config[:secret_access_key]
    end
  end

  # cluster settings

  def creation_url
    "#{GitHub.storage_cluster_url}/upload-manifest-files/#{upload_manifest_id}/files"
  end

  def storage_upload_path_info(policy)
    "/internal/storage/upload-manifest-files/#{upload_manifest_id}/files"
  end

  def storage_download_path_info(policy)
    "#{storage_upload_path_info(policy)}/#{id}"
  end

  def storage_cluster_url(policy)
    creation_url + "/#{id}"
  end

  def storage_transition_ready?
    blob_oid.to_s.strip.blank? && storage_blob_accessible?
  end

  # END storage settings

  # Download and store the file in git.
  #
  # Returns the blob's git SHA String.
  def to_blob
    git = manifest.repository.rpc
    contents = download

    # Store the file contents as a blob.
    sha = git.write_blob(contents)
    update(blob_oid: sha)
    sha
  end

  extend GitHub::Encoding
  force_utf8_encoding :directory, :name

  def directory=(value)
    validator = UploadDirectoryValidator.new({})
    write_attribute :directory, validator.sanitize_directory_name(value)
  end

  def full_name
    path = []
    path << manifest.directory if manifest.directory.present?
    path << directory if directory.present?
    path << name
    File.join(path)
  end

  def guid
    # We don't have a guid, but this satisfies an alambic requirement.
  end

  def uploadable_surrogate_key
    "upload-manifest-file-#{repository_id} #{super}"
  end

  def cleanup!
    transaction do
      GitHub::Storage::Destroyer.dereference(self)
      update_attribute(:storage_blob_id, nil)
    end
  end

  private

  def download
    uri = URI.parse(storage_policy(actor: uploader).download_url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.open_timeout = 3
    http.read_timeout = 3

    if GitHub.ssl?
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    end

    response = http.get(uri.request_uri)
    if response.code != "200"
      raise RuntimeError, "download error: #{response.code}"
    end

    response.body
  end

  def choose_storage_provider
    return if GitHub.storage_cluster_enabled?
    self.storage_provider = :s3_production_data
  end

  def max_files_per_manifest
    @max_files_per_manifest || MAX_FILES_PER_MANIFEST
  end

  def file_count_limit
    return unless manifest
    if manifest.files.count >= max_files_per_manifest
      errors.add :file_count, "contains too many files"
    end
  end

  def uploader_access
    if !repository
      errors.add :repository, "is required"
    elsif !manifest
      errors.add :manifest, "is required"
    elsif !repository.pushable_by?(uploader)
      errors.add :uploader, "does not have push access to this repository"
    elsif manifest.uploader != uploader
      errors.add :uploader, "does not have access to upload manifest"
    elsif !manifest.state_new?
      errors.add :manifest, "is complete and processing"
    end
  end

  # The browser sends an empty string for unknown file types. Default to a
  # generic byte stream type in that case.
  #
  # Returns nothing.
  def default_content_type
    if content_type.blank?
      self.content_type = "application/octet-stream"
    end
  end

  # Normally we sanitize these names with this method in `app/models/uploadable.rb`.
  # However with this model we reject filenames we can't deal in validation, so
  # we override this method here to do nothing instead. This allows users to keep
  # the username they wanted
  def sanitize_file_name(name)
    name
  end
end
