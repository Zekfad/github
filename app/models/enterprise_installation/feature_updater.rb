# rubocop:disable Style/FrozenStringLiteralComment

class EnterpriseInstallation
  class FeatureUpdater
    # inputs
    attr_reader :enterprise_installation, :actor

    # Result from the EnterpriseInstallation feature update
    class Result
      class Error < StandardError; end

      def self.success(enterprise_installation) new(:success, enterprise_installation: enterprise_installation) end
      def self.failed(error) new(:failed, error: error) end

      attr_reader :error, :enterprise_installation

      def initialize(status, enterprise_installation: nil, error: nil)
        @status       = status
        @enterprise_installation = enterprise_installation
        @error        = error
      end

      def success?
        @status == :success
      end

      def failed?
        @status == :failed
      end
    end

    # Public: Update an EnterpriseInstallation for requested feature updates.
    #
    # enterprise_installation - The EnterpriseInstallation that is being updated.
    # actor:                  - The User performing the action.
    #
    # Returns an EnterpriseInstallation::FeatureUpdater::Result.
    def self.perform(enterprise_installation, actor:)
      new(enterprise_installation, actor: actor).perform
    end

    def initialize(enterprise_installation, actor:)
      @enterprise_installation  = enterprise_installation
      @actor                    = actor
    end

    def perform
      validate_actor

      integration = enterprise_installation.github_app
      if integration.nil?
        raise Result::Error, "Enterprise server installation is not set up for GitHub Connect"
      end

      version = integration.latest_version
      installation_on_owner = enterprise_installation.integration_installation
      previous_version = installation_on_owner.version

      if previous_version.number != version.number
        update_integration_installation_version(installation_on_owner, version)
        edit_integration_installation_repositories(installation_on_owner)
        instrument_features_updated(previous_version, version)
        if enterprise_installation.owner.is_a?(Business)
          UpdatePrivateSearchOnEnterpriseOrgsJob.perform_later(enterprise_installation.owner.id)
        end
      end

      Result.success(enterprise_installation)
    rescue Result::Error => e
      Failbot.report(e)
      Result.failed e.message
    end

    private

    def update_integration_installation_version(installation, version)
      result = installation.auto_update_version(editor: actor, version: version)
      return if result.success?

      raise Result::Error, result.error
    end

    def edit_integration_installation_repositories(installation)
      return unless repository_installation_required?(installation)

      result = installation.edit(editor: actor, repositories: nil)
      return if result.success?

      raise Result::Error, result.error
    end

    def repository_installation_required?(installation)
      installation.integration.repository_installation_required?(installation.target) && installation.repositories.none?
    end

    def instrument_features_updated(previous_version, latest_version)
      diff = latest_version.diff(previous_version)
      permissions = diff.permissions_added
      features = enterprise_installation.features_for_github_app_permissions(permissions)
      enterprise_installation.instrument_features_updated(features, actor)
    end

    def validate_actor
      return if enterprise_installation.owner.adminable_by?(actor)

      raise Result::Error, "Actor does not have permissions to enable or disable connect features for enterprise installation #{enterprise_installation.id}"
    end
  end
end
