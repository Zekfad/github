# frozen_string_literal: true

# Public: Used to convert a single Issue to a Discussion.
class IssueToDiscussionConverter
  attr_reader :issue, :actor, :discussion, :category, :issue_originally_open

  # Public: Create a new converter for the given issue.
  #
  # issue - the Issue to delete and replace with a Discussion
  # actor - the User who initiated the conversion
  # issue_originally_open - an optional Boolean representing if the Issue was originally in 'open'
  #                         state, and thus should be reopened if the conversion fails; omit if
  #                         this is not known
  # category - an optional DiscussionCategory to categorize the Discussion once it's created
  def initialize(issue, actor:, issue_originally_open: nil, category: nil)
    @issue = issue
    @actor = actor
    @category = category
    @discussion = nil
    @issue_originally_open = issue_originally_open
  end

  # Public: Determine if the conversion can continue. Checks to see if the conversion
  # has already started, if the issue can be closed to start the conversion, and
  # creates the Discussion while the Issue still exists.
  #
  # Returns a Boolean indicating if the Discussion was successfully created or already
  # exists for the Issue.
  def prepare_for_conversion
    # See if this issue is already being converted to a discussion.
    if (existing_discussion = issue.discussion)
      @discussion = existing_discussion
      return true
    end

    # Make sure the issue is closed, or we can't start converting it to a discussion.
    if issue.closed?
      @issue_originally_open = false
    else
      @issue_originally_open = true
      return false unless issue.close(actor)
    end

    # Create a discussion record to start the conversion process.
    @discussion = Discussion.from_issue(issue)
    @discussion.category = @category

    # Return whether the discussion was successfully updated:
    @discussion.save
  end

  # Public: Complete the conversion by copying over all the dependent records from the Issue to
  # the Discussion and deleting the original Issue.
  #
  # Returns a Boolean indicating if the conversion was successful.
  def finish_conversion
    @discussion ||= issue.discussion
    return false unless @discussion

    success = true

    Discussion.transaction do
      begin
        copy_reactions_to_discussion!
        copy_comments_to_discussion!
        copy_subscriptions_to_discussion! if GitHub.flipper[:discussion_converter_subscriptions].enabled?

        if issue.locked?
          lock_discussion
        else
          @discussion.state = :open
        end
        @discussion.converted_at = Time.now.utc
        @discussion.save!
      rescue ActiveRecord::RecordInvalid => ex
        success = false
        Failbot.report(ex)
        raise ActiveRecord::Rollback, ex.message
      end
    end

    if success
      # TODO: create audit log entry about discussion conversion
      DeletedIssue.delete_issue(issue, deleter: actor)
    else
      clean_up_after_failed_conversion
      false
    end
  end

  private

  def clean_up_after_failed_conversion
    # Prefer to delete the discussion entirely so the user's issue can continue being used:
    if discussion.destroy
      issue.open(actor) if issue.closed? && issue_originally_open
    else
      # If deleting the discussion fails for whatever reason, try to log that the discussion
      # isn't in a good state, either:
      discussion.update!(error_reason: :reset_conversion_failure, state: :error)
    end
  end

  def copy_reactions_to_discussion!
    issue.reactions.each do |reaction|
      attrs = reaction_attributes_to_copy(reaction)
      discussion.reactions.create!(attrs)
    end
  end

  def copy_comments_to_discussion!
    issue.comments.each do |comment|
      copy_comment_to_discussion!(comment)
    end
  end

  def copy_comment_to_discussion!(issue_comment)
    discussion_comment = discussion.comments.create!(
      user: issue_comment.user,
      body: issue_comment.body,
      repository: issue.repository,
      updated_at: issue_comment.updated_at,
      created_at: issue_comment.created_at,
      formatter: issue_comment.formatter,
      comment_hidden: issue_comment.comment_hidden,
      comment_hidden_reason: issue_comment.comment_hidden_reason,
      comment_hidden_classifier: issue_comment.comment_hidden_classifier,
      comment_hidden_by: issue_comment.comment_hidden_by,
    )

    copy_comment_reactions_to_discussion_comment!(issue_comment, discussion_comment: discussion_comment)
  end

  def copy_comment_reactions_to_discussion_comment!(issue_comment, discussion_comment:)
    issue_comment.reactions.each do |reaction|
      attrs = reaction_attributes_to_copy(reaction)
      discussion_comment.reactions.create!(attrs)
    end
  end

  def reaction_attributes_to_copy(reaction)
    {
      content: reaction.content,
      user_id: reaction.user_id,
      created_at: reaction.created_at,
      updated_at: reaction.updated_at,
      user_hidden: reaction.user_hidden
    }
  end

  def lock_discussion
    lock_event = issue.events.locks.last
    @discussion.lock(lock_event.actor)
  end

  def copy_subscriptions_to_discussion!
    # each_subscriber doesn't include ignored subscribers
    issue.each_subscriber do |subscriber|
      ApplicationRecord::Domain::Notifications.throttle do
        discussion.subscribe(subscriber.user, subscriber.reason)
      end
    end

    users_ignoring_issue.each do |user|
      ApplicationRecord::Domain::Notifications.throttle do
        discussion.ignore(user)
      end
    end
  end

  def users_ignoring_issue
    @users_ignoring_issue ||= User.where(id: issue_subscriber_set.ignored).select(:id)
  end

  def issue_subscriber_set
    @subscriber_set ||= GitHub.newsies.subscriber_set_for(
      list: @issue.repository,
      thread: @issue,
    )
  end
end
