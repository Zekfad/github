# frozen_string_literal: true

# Decorator for a Vulnerability object and associated VulnerableVersionRanges
# which acts a ready-only shim between database models the public API
class SecurityAdvisory < Vulnerability
  include GitHub::Relay::GlobalIdentification
  include HasCveUrl

  areas_of_responsibility :security_advisories

  DEFAULT_COLLECTION_ORDER = { "field" => "id", "direction" => "DESC" }

  has_many :vulnerabilities, class_name: "SecurityVulnerability",
                             foreign_key: :vulnerability_id,
                             inverse_of: :security_advisory

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id("SecurityAdvisory", ghsa_id)
  end

  def summary
    read_attribute(:summary).presence ||
      "#{severity.capitalize} severity vulnerability that affects #{affects.to_sentence}"
  end

  def identifiers
    array = [{ value: ghsa_id, type: "GHSA" }]
    if cve_id
      array << { value: cve_id, type: "CVE" }
    elsif white_source_id
      array << { value: white_source_id, type: "WHITE_SOURCE" }
    end
    array
  end

  def primary_identifier
    return ghsa_id if has_repository_advisory_source?
    self.identifier
  end

  def references
    urls = [external_reference, cve_url, permalink]
    urls.reject!(&:blank?)
    urls.uniq!

    urls.map { |url| { url: url } }
  end

  def primary_reference
    references.first&.fetch(:url)
  end

  def matches_ecosystem?(ecosystem = nil)
    return true if ecosystem.nil?

    platform.downcase == ecosystem
  end

  def matches_severities?(severities = [])
    return true if severities.nil? || severities.empty?

    severities.include?(severity.downcase)
  end

  def hydro_entity_payload(overrides: {})
    {
      ghsa_id: ghsa_id,
      cve_id: cve_id,
      severity: Hydro::EntitySerializer.enum_from_string(severity),
      published_at: published_at,
      updated_at: updated_at,
      withdrawn_at: withdrawn_at,
    }
  end
end
