# frozen_string_literal: true

module UserEntitySerializer
  USER_TYPES = {
    "Bot" => :BOT,
    "Organization" => :ORGANIZATION,
    "User" => :USER,
  }

  def self.serialize(entity)
    if entity.is_a?(GraphQL::Client::Schema::ObjectClass)
      graphql_serialize(entity)
    else
      ar_serialize(entity)
    end
  end

  def self.ar_serialize(entity)
    spamurai_classification = if entity.spammy?
      :SPAMMY
    elsif entity.hammy?
      :HAMMY
    else
      :SPAMURAI_CLASSIFICATION_UNKNOWN
    end

    {
      id: entity.id,
      login: entity.login,
      type: USER_TYPES.fetch(entity.class.name, :UNKNOWN),
      billing_plan: entity.plan.try(:display_name),
      spammy: entity.spammy?,
      suspended: entity.suspended?,
      spamurai_classification: spamurai_classification,
      global_relay_id: entity.global_relay_id,
      created_at: entity.created_at,
      analytics_tracking_id: entity.analytics_tracking_id,
    }
  end

  def self.graphql_serialize(entity)
    entity_is_a_user = entity.is_a?(PlatformTypes::User)
    entity = EntityQuery::AnalyticsUser.new(entity)
    spamurai_classification = if entity.is_spammy
      :SPAMMY
    elsif entity_is_a_user && entity.is_hammy
      :HAMMY
    else
      :SPAMURAI_CLASSIFICATION_UNKNOWN
    end

    {
      id: entity.database_id,
      login: entity.login,
      type: USER_TYPES.fetch(entity.__typename, :UNKNOWN),
      billing_plan: entity.plan&.display_name,
      spammy: entity.is_spammy,
      suspended: (entity_is_a_user && entity.suspended_at.present?),
      spamurai_classification: spamurai_classification,
      global_relay_id: entity.id,
      created_at: entity.created_at,
      analytics_tracking_id: (entity_is_a_user ? entity.analytics_tracking_id : nil),
    }
  end
end
