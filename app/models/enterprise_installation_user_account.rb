# frozen_string_literal: true

# A user account on an Enterprise Server installation.
#
# Belongs to a single Enterprise Server installation.
# Has possibly many associated emails, each represented by
# EnterpriseInstallationUserAccountEmail.
# Belongs to a single BusinessUserAccount.
class EnterpriseInstallationUserAccount < ApplicationRecord::Collab
  include GitHub::Relay::GlobalIdentification

  belongs_to :enterprise_installation
  belongs_to :business_user_account

  has_many :emails, dependent: :delete_all, class_name: "EnterpriseInstallationUserAccountEmail"

  validates :enterprise_installation, presence: true
  validates :remote_user_id,
    presence: true,
    uniqueness: { scope: :enterprise_installation_id, message: "already exists on this installation" }
  validates :remote_created_at, presence: true
  validates :login, presence: true, length: { in: 1..255 }
  validates :profile_name, length: { maximum: 255 }

  # destroy callbacks are not called for records deleted from
  # `EnterpriseInstallationUserAccountsImporter#delete_missing_accounts`
  after_commit :cleanup_business_user_account, on: :destroy

  def platform_type_name
    "EnterpriseServerUserAccount"
  end

  private

  def cleanup_business_user_account
    return unless business_user_account.present?
    return if business_user_account.user_id.present?
    return if business_user_account.enterprise_installation_user_accounts.where.not(id: self.id).exists?
    business_user_account.destroy
  end
end
