# rubocop:disable Style/FrozenStringLiteralComment

module Spam
  module UpdateUserHidden
    BATCH_SIZE = 50

    def self.update_table(table, user_hidden, user_id)
      return unless user_id
      return unless table

      table = table.to_sym
      klass = Spam::Spammable.tables_classes_including[table]
      return unless klass

      ids = if table == :followers
        select_ids(klass, user_id, user_hidden, :following_id) +
          select_ids(klass, user_id, user_hidden, :user_id)
      else
        select_ids(klass, user_id, user_hidden)
      end

      update_table_ids(klass, ids, user_hidden)
    end

    extend Scientist

    # Public: Get ids for the users that have content that does not match their user_hidden state.
    #
    # updated_since - Time that user was updated at to use as lower bound in search.
    #
    # Returns an Array of user ids.
    def self.user_ids_for_mismatches(updated_since:)
      user_ids_with_mismatch = Set.new

      User.transaction do
        ActiveRecord::Base.connected_to(role: :reading) do
          scope = User.where("updated_at >= ?", updated_since)
          spammy_user_ids = scope.spammy.pluck(:id)
          not_spammy_user_ids = scope.not_spammy.pluck(:id)

          Spam::Spammable.tables_classes_including.each do |table, klass|
            [true, false].each do |user_hidden|
              user_ids = user_hidden ? not_spammy_user_ids : spammy_user_ids

              user_ids.each_slice(1_000) do |slice_user_ids|
                table_user_ids = klass.where(user_hidden: user_hidden).
                  where(klass.spammable_user_foreign_key => slice_user_ids).
                  pluck(klass.spammable_user_foreign_key)

                user_ids_with_mismatch.merge(table_user_ids)
              end
            end
          end
        end
      end

      user_ids_with_mismatch.to_a
    end

    def self.select_ids(klass, user_id, user_hidden, foreign_key = nil)
      user_key = foreign_key || klass.spammable_user_foreign_key

      ActiveRecord::Base.connected_to(role: :reading) do
        klass.github_sql.values <<-SQL, user_id: user_id, user_hidden: !user_hidden
          SELECT id FROM #{klass.table_name}
          WHERE #{user_key} = :user_id
          AND user_hidden = :user_hidden
        SQL
      end
    end
    private_class_method :select_ids

    def self.update_table_ids(klass, ids, user_hidden)
      ids.each_slice(BATCH_SIZE) do |slice|
        klass.throttle do
          klass.github_sql.run <<-SQL, ids: slice, user_hidden: user_hidden
            UPDATE #{klass.table_name}
            SET user_hidden = :user_hidden
            WHERE id IN :ids
          SQL
        end
      end
    end
    private_class_method :update_table_ids
  end
end
