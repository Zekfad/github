# frozen_string_literal: true

module UserRanked
  class Cache
    TTL = 1.hour

    def self.fetch_ranked_ids(ranked_class_name, user, &block_to_calculate)
      new(ranked_class_name, user, &block_to_calculate).fetch_ranked_ids
    end

    def initialize(ranked_class_name, user, &block_to_calculate)
      @ranked_class_name = ranked_class_name
      @user = user
      @block_to_calculate = block_to_calculate
    end

    def fetch_ranked_ids
      GitHub.cache.fetch(
        cache_key,
        stats_key: stats_key,
        ttl: TTL,
        &block_to_calculate
      )
    end

    private

    attr_reader :block_to_calculate, :ranked_class_name, :user

    def cache_key
      "user-ranked:#{ranked_class_name}:#{user.id}"
    end

    def stats_key
      "user-ranked:#{ranked_class_name}:cache"
    end
  end
end
