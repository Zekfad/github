# frozen_string_literal: true

module UserRanked
  module Scope
    extend ActiveSupport::Concern

    included do
      # Public: Refine the current scope by adding an ORDER clause based on activity
      # performed by the given user
      #
      # user - a User
      # scope - an ActiveRecord relation
      # direction - optional direction to order, either :asc or :desc
      #
      # Returns an ActiveRecord relation.
      def self.ranked_for(user, scope:, direction: :desc)
        ids = Cache.fetch_ranked_ids(name, user) do
          compute_ranked_ids(user: user)
        end

        return scope if ids.empty?
        direction = :desc unless [:asc, :desc].include?(direction)
        scope.order(Arel.sql("FIELD(#{table_name}.id, #{ids.reverse.join(', ')}) #{direction}")).
              order(:id)
      end
    end
  end
end
