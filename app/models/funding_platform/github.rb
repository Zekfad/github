# frozen_string_literal: true

class FundingPlatform::GitHub
  def self.name
    "GitHub"
  end

  def self.url
    URI("https://github.com/")
  end

  def self.key
    :github
  end

  def self.template_placeholder
    "# Replace with up to 4 GitHub Sponsors-enabled usernames e.g., [user1, user2]"
  end
end
