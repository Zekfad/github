# frozen_string_literal: true

class FundingPlatform::OpenCollective
  def self.name
    "Open Collective"
  end

  def self.url
    URI("https://opencollective.com/")
  end

  def self.key
    :open_collective
  end

  def self.template_placeholder
    "# Replace with a single #{name} username"
  end
end
