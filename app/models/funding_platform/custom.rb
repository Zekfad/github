# frozen_string_literal: true

class FundingPlatform::Custom
  def self.name
    "Custom"
  end

  def self.url
    nil
  end

  def self.key
    :custom
  end

  def self.template_placeholder
    "# Replace with up to 4 custom sponsorship URLs e.g., ['link1', 'link2']"
  end
end
