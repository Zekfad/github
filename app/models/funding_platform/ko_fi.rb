# frozen_string_literal: true

class FundingPlatform::KoFi
  def self.name
    "Ko-fi"
  end

  def self.url
    URI("https://ko-fi.com/")
  end

  def self.key
    :ko_fi
  end

  def self.template_placeholder
    "# Replace with a single #{name} username"
  end
end
