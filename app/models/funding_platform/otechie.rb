# frozen_string_literal: true

class FundingPlatform::Otechie
  def self.name
    "Otechie"
  end

  def self.url
    URI("https://otechie.com/")
  end

  def self.key
    :otechie
  end

  def self.template_placeholder
    "# Replace with a single #{name} username"
  end
end
