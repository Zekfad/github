# frozen_string_literal: true

class FundingPlatform::Tidelift
  def self.name
    "Tidelift"
  end

  def self.url
    URI("https://tidelift.com/funding/github/")
  end

  def self.key
    :tidelift
  end

  def self.template_placeholder
    "# Replace with a single #{name} platform-name/package-name e.g., npm/babel"
  end
end
