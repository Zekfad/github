# frozen_string_literal: true

module DiscussionComment::SearchAdapter
  def synchronize_search_index
    discussion&.synchronize_search_index
  end
end
