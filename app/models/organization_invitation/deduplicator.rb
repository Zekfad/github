# frozen_string_literal: true

class OrganizationInvitation
  # When there are duplicate invitations for a given user/org, transfer all
  # team_invitations to the invitation the user is accepting and cancel all the
  # duplicate invitations.
  #
  # Note: this is intended to be run inside a transaction where the acceptance
  # of the non-duplicate invitation will either succeed or it (and this
  # destructive action) will be rolled back.
  class Deduplicator
    # Public: See #call!
    def self.call!(invitation:, acceptor:)
      new(invitation: invitation, acceptor: acceptor).call!
    end

    # Public: Initialize the Deduplicator
    #
    # invitation - The OrganizationInvitation we are going to accept
    # accpetor - The User accepting the OrganizationInvitation
    def initialize(invitation:, acceptor:)
      @invitation = invitation
      @acceptor = acceptor
    end

    # Public: When there are duplicates invitations:
    # 1. log pertinent info
    # 2. add each team_invitation for each duplicate to the non-duplicate
    # 3. mark all duplicates as cancelled
    #
    # Examples
    #
    #   call!
    #   # => true
    #
    # Returns TrueClass or raises.
    def call!
      return true unless @invitation.email?

      duplicate_invitations.each do |duplicate|
        log(duplicate)
        transfer_team_invitations(duplicate)
        cancel(duplicate)
      end
    end

    private

    def duplicate_invitations
      @invitation
        .organization
        .pending_invitations
        .where.not(id: @invitation.id)
        .with_invitee_or_normalized_email(
          invitee: @acceptor,
          emails: acceptor_emails,
      )
    end

    def acceptor_emails
      @acceptor.emails.pluck(:email)
    end

    def log(duplicate)
      GitHub::Logger.log(
        msg: "duplicate_organization_invitations",
        ns: self.class.to_s,
        fn: "call!",
        acceptor: @acceptor,
        invitee_id: duplicate.invitee_id,
        inviter_id: duplicate.inviter_id,
        email: duplicate.email,
        organization_invitation_id: duplicate.id,
        duplicate_of_id: @invitation.id,
      )
    end

    # Move all team invitations from duplicates to the non-duplicate
    def transfer_team_invitations(duplicate)
      duplicate.team_invitations.each do |team_invitation|
        @invitation.add_team(
          team_invitation.team,
          inviter: team_invitation.inviter,
        )
      end
    end

    # Do _not_ instrument this cancel call since this is internal cleaning up
    # and should not be served to the user as webhook/audit_log entry
    def cancel(duplicate)
      duplicate.cancel(actor: nil, notify: false, instrument: false)
    end
  end
end
