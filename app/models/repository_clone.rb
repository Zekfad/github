# frozen_string_literal: true

class RepositoryClone < ApplicationRecord::Collab
  belongs_to :template_repository, class_name: "Repository"
  belongs_to :clone_repository, class_name: "Repository"
  belongs_to :cloning_user, class_name: "User"

  MAX_TEMPLATE_FILE_SIZE_IN_MEGABYTES = 10

  enum error_reason_code: [
    :missing_template_repo,
    :missing_clone_repo,
    :no_commit_oid,
    :failed_to_update_branch,
    :file_too_large,
    :git_object_missing,
    :git_timeout,
    :cannot_finish,
    :dgit_exception,
    :gitrpc_exception,
    :other_exception,
  ]

  enum state: {
    cloning: 0,
    finished: 1,
    error: 2,
  }

  validates :template_repository, :clone_repository, :cloning_user, :state, presence: true
  validates :clone_repository_id, uniqueness: true
  validate :template_repository_is_template
  validate :template_repository_is_active
  validate :cloning_user_has_access_to_clone_repository

  scope :for_clone_repo, ->(repo) { where(clone_repository_id: repo) }
  scope :cloning, -> { where(state: states[:cloning]) }
  scope :finished, -> { where(state: states[:finished]) }
  scope :for_user, ->(user) { where(cloning_user_id: user) }

  after_create :instrument_create


  def template_owner
    template_repository&.owner
  end

  private

  def instrument_create
    payload = {
      user: cloning_user,
      clone_repository: clone_repository,
      template_repository: template_repository,
    }
    GlobalInstrumenter.instrument("repository_clone.created", payload)
  end

  def template_repository_is_template
    return unless template_repository

    unless template_repository.template?
      errors.add(:template_repository, "must be marked as a template")
    end
  end

  def template_repository_is_active
    return unless template_repository

    unless template_repository.active?
      errors.add(:template_repository, "must be active")
    end
  end

  def cloning_user_has_access_to_clone_repository
    return unless cloning_user && clone_repository

    if cloning_user.bot? && !cloning_user.installation
      cloning_user.installation = cloning_user.integration.installations.with_repository(clone_repository).first
    end

    unless clone_repository.resources.contents.readable_by?(cloning_user)
      errors.add(:cloning_user, "does not have permission to view the clone repository")
    end
  end
end
