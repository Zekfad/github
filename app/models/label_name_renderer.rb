# frozen_string_literal: true

class LabelNameRenderer
  # Regexp for labels which don't need modification: anything matching this
  # MUST be guaranteed HTML_safe and to contain no emoji.
  SAFE_LABEL_REGEXP = /\A[a-z0-9. _-]+\z/i

  def initialize(name, skip_cache: false)
    @name = name
    @skip_cache = skip_cache
  end

  # Public: Returns a Promise that resolves to an HTML string for displaying this label's name.
  # The resulting string may include g-emoji HTML tags.
  def async_to_html
    return Promise.resolve(nil) unless @name # avoid errors when passing nil to #escape_html
    return Promise.resolve(EscapeUtils.escape_html_as_html_safe(@name)) if SAFE_LABEL_REGEXP.match?(@name)
    return Promise.resolve(g_emoji_taggify_name) if @skip_cache

    Platform::Loaders::Cache.fetch(cache_key) { g_emoji_taggify_name }
  end

  # Public: Returns an HTML string for displaying this label's name. May include g-emoji HTML
  # tags.
  def to_html
    async_to_html.sync
  end

  private

  def cache_key
    name_hash = Digest::MD5.hexdigest(@name)
    "label:#{name_hash}"
  end

  def g_emoji_taggify_name
    clean_input = EscapeUtils.escape_html(@name) # escape HTML tags
    html = GitHub::HTML::EmojiFilter.to_html(clean_input) # produce g-emoji tags
    html.html_safe # rubocop:disable Rails/OutputSafety
  end
end
