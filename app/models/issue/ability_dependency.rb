# frozen_string_literal: true

module Issue::AbilityDependency
  extend ActiveSupport::Concern

  class_methods do
    # Public: Filter out issues from the given list that the viewer can't see.
    #
    # issue_ids - A list of issue ids that are being accessed.
    # viewer - The viewer (usually a User) trying to access the given issues (optional).
    #
    # Returns a Array<Integer>
    def visible_ids_for(issue_ids, viewer: nil)
      return visible_ids_for_viewer(issue_ids, viewer) if viewer

      public_scope.where(id: issue_ids).ids
    end

    # Public: Build a scope that filters out issues from the given list that the viewer can't see.
    #
    # issue_ids - A list of issue ids that are being accessed.
    # viewer - The viewer (usually a User) trying to access the given issues (optional).
    #
    # Returns a ActiveRecord::Relation
    def visible_for(issue_ids, viewer: nil)
      return where(id: visible_ids_for_viewer(issue_ids, viewer)) if viewer

      public_scope.where(id: issue_ids)
    end

    private def visible_ids_for_viewer(issue_ids, viewer)
      visible_issue_ids = []
      issue_ids_by_private_repository_id = Hash.new { |hash, key| hash[key] = [] }

      where(id: issue_ids).joins(:repository).pluck(:id, :repository_id, Arel.sql("repositories.public")).each do |id, repository_id, repository_public|
        if GitHub.rails_6_0?
          if repository_public == 1
            visible_issue_ids << id
          else
            issue_ids_by_private_repository_id[repository_id] << id
          end
        else
          if repository_public
            visible_issue_ids << id
          else
            issue_ids_by_private_repository_id[repository_id] << id
          end
        end
      end

      if issue_ids_by_private_repository_id.any?
        accessible_repository_ids_by_viewer = viewer.associated_repository_ids(repository_ids: issue_ids_by_private_repository_id.keys)
        accessible_repository_ids_by_viewer.each do |repository_id|
          visible_issue_ids_for_repository_id = issue_ids_by_private_repository_id.delete(repository_id)
          visible_issue_ids.concat(visible_issue_ids_for_repository_id)
        end
      end

      if issue_ids_by_private_repository_id.any?
        unlocked_repository_ids_by_viewer = viewer.unlocked_repository_ids & issue_ids_by_private_repository_id.keys
        unlocked_repository_ids_by_viewer.each do |repository_id|
          visible_issue_ids.concat(issue_ids_by_private_repository_id[repository_id])
        end
      end

      visible_issue_ids
    end
  end

  # Public: Can this issue be read by the specified actor?
  #
  # actor - The actor (usually a User) trying to read the issue.
  #
  # Returns a boolean.
  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  # Public: Can this issue be read by the specified actor?
  #
  # actor - The actor (usually a User) trying to read the issue.
  #
  # Same as the readable_by? method above, but returns Promise<bool>
  def async_readable_by?(actor)
    # This issue must belong to an existing repo.
    async_repository.then do |repository|
      next false unless repository

      if pull_request_id
        # The actor must be able to read pull requests on this issue's repository.
        repository.resources.pull_requests.async_readable_by?(actor)
      else
        # The actor must be able to read issues on this issue's repository.
        repository.resources.issues.async_readable_by?(actor)
      end
    end
  end

  def subscribable_by?(user)
    super && readable_by?(user)
  end

  def can_modify?(user)
    return false if user.nil?
    return false if repository.archived?
    return true if self.user == user
    repository.pushable_by?(user)
  end

  # Public: Get all the IDs of users with privileged access to this issue.
  #
  # Note: "Privileged access" is an Abilities term that refers to access gained
  # through any method other than the repository being public. A random user
  # who can see a public repository doesn't have privileged access to it, but
  # if they're added to a team that grants access to the repository (even if
  # it's still just read-only access), they now have privileged access to it.
  #
  # Returns an Array of Integers.
  def user_ids_with_privileged_access(actor_ids_filter: nil)
    return [] if repository.nil?

    user_ids = repository.user_ids_with_privileged_access(min_action: :read, actor_ids_filter: actor_ids_filter)

    # The issue's author has privileged access if they're still able to read
    # the issue's repository.
    user_ids |= [user_id] if repository.readable_by?(user)

    user_ids
  end
end
