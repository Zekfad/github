# frozen_string_literal: true

module Issue::DiscussionsDependency
  # Public: Returns the related Discussion if this Issue was converted to a Discussion.
  def discussion
    Discussion.with_number(number).converted_from_issue(id).first if persisted?
  end

  # Public: Determine whether this Issue is eligible to be converted to a Discussion.
  def can_be_converted_to_discussion?
    # Pull requests can't be converted, only issues
    return false if pull_request?

    # Already in the process of converting this Issue to a Discussion
    return false if discussion

    true
  end

  # Public: Determine whether this Issue can be converted to a Discussion by the specified user.
  #
  # actor - the current authenticated User
  #
  # Returns a Boolean.
  def can_be_converted_by?(actor)
    # Ensure this issue is eligible to be converted to a discussion
    return false unless can_be_converted_to_discussion?

    return false unless repository.can_convert_issues_to_discussions?(actor)

    # User must be able to close this issue or the conversion can't begin
    return false unless closeable_by?(actor)

    # If the issue author has blocked this user, don't let them convert it
    return false if user && actor.blocked_by?(user)

    true
  end
end
