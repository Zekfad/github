# rubocop:disable Style/FrozenStringLiteralComment

class Issue
  module EsSearch
    def self.search(query:, repo: nil, current_user: nil, remote_ip: nil, user_session: nil, force_pulls: false, page: 1, per_page: ApplicationController::DEFAULT_PER_PAGE, **kargs)
      # Convert parsed query to String for ES
      # TODO: Search::Queries::IssueQuery should accept a parsed query
      if query.is_a?(Array)
        query = Search::Queries::IssueQuery.stringify(query)
      end

      unless query.is_a?(String)
        raise ArgumentError, "query must be a String: #{query.class}"
      end

      hash = {
        phrase: query,
        repo_id: (repo && repo.id),
        aggregations: :state,
        page: page,
        per_page: per_page,
        current_user: current_user,
        remote_ip: remote_ip,
        user_session: user_session,
        source_fields: false,
      }
      hash[:type] = "pr" if force_pulls
      hash[:sort] = ["created", "desc"] if !query.include?("sort:")

      # Bypass the ES result cap if there's a logged-in user.
      hash[:max_offset] = 10_000 if current_user

      es = ::Search::Queries::IssueQuery.new(hash).execute

      open_count = closed_count = 0

      state_counts = es.state_counts
      if state_counts.present?
        open_count = state_counts["open"] || 0
        closed_count = state_counts["closed"] || 0
      end

      issues = WillPaginate::Collection.create(page, per_page, es.total) do |pager|
        pager.replace es.results.map { |result| result["_model"] }
      end

      {
        open_count: open_count,
        closed_count: closed_count,
        issues: issues,
      }
    rescue Search::Query::MaxOffsetError
      {
        open_count: 0,
        closed_count: 0,
        issues: [],
      }
    end
  end
end
