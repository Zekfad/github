# frozen_string_literal: true

class Issue
  # Primary interface into searching elasticsearch for results. In the event of
  # elasticsearch being down, return a naive result set from MySQL directly.
  class SearchResult
    # Run the search over elasticsearch. If that fails, rescue, and then fall
    # back to MySQL.
    #
    # Returns an Array of Issues.
    def self.search(query:, repo: nil, prefill_associations: true, **kargs)
      GitHub.tracer.with_span("Issue::SearchResult.search") do |span|
        backend = if repo && MysqlSearch.supported?(query)
          MysqlSearch
        else
          EsSearch
        end

        # Make sure the page values look appropriate before passing them on.
        page     = (kargs[:page] || 1).to_i
        per_page = (kargs[:per_page] || 25).to_i
        kargs[:page]     = page > 0 ? page : 1
        kargs[:per_page] = per_page > 0 ? per_page : 25

        results = backend.search(query: query, repo: repo, **kargs)

        if repo && prefill_associations
          GitHub::PrefillAssociations.for_issues(results[:issues], repository: repo, current_user: kargs[:current_user])

          pull_requests = results[:issues].map(&:pull_request).compact
          if pull_requests.any?
            GitHub::PrefillAssociations.for_pulls(pull_requests, issues: results[:issues])
            GitHub::PrefillAssociations.for_pulls_protected_branches(repo, pull_requests)

            PullRequest.attach_statuses(repo, pull_requests)
          end
        end

        if kargs[:current_user]
          read_issues = Conversation.read_for(kargs[:current_user], results[:issues])
          results[:issues].each do |issue|
            issue.read_by_current_user = read_issues.include?(issue.id)
          end
        end

        results
      end
    end
  end
end
