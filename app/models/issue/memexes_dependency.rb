# frozen_string_literal: true

module Issue::MemexesDependency
  extend ActiveSupport::Concern

  def memex_items_enabled_for_viewer?(viewer)
    owner = self.repository.owner

    owner.organization? &&
      (GitHub.flipper[:memex_beta].enabled? || GitHub.flipper[:memex_beta].enabled?(viewer)) &&
      owner.direct_or_team_member?(viewer)
  end

  # Returns memex_project_items for the issue that the viewer has access to see.
  #
  # Note: Currently returns all memex_project_items if the viewer has access to
  # memex items, but this logic may expand to cover additional permissions / gating.
  def visible_memex_items_for(viewer)
    scope = MemexProjectItem

    if memex_items_enabled_for_viewer?(viewer)
      content = self.pull_request || self
      scope.where(content: content).includes(:memex_project)
    else
      scope.none
    end
  end

  def memex_suggestion_hash(last_interaction_at: nil, as_pull: false)
    result = {
      id: id,
      last_interaction_at: last_interaction_at&.utc&.iso8601,
      number: number,
      state: state.to_s,
      title: title,
      type: "Issue",
      updated_at: updated_at.utc.iso8601,
    }

    if as_pull && pull_request_id
      # Here we try to produce PullRequest#memex_suggestion_hash without actually loading a
      # PullRequest object. Tradeoff is that we can't tell draft status, and the updated at
      # timestamp in the result might be off, because it is actually the updated at timestamp
      # of this, the underlying issue, rather than the pull request object.
      result = result.merge({
        id: pull_request_id,
        is_draft: false,
        type: "PullRequest",
      })
    end

    result
  end

  # Implements MemexProjectItem::Content#memex_content_hash.
  def memex_content_hash
    {
      id: id,

      # We include repository because it is used for caching issue suggestions on the client.
      repository: repository.memex_column_hash,

      url: permalink,
    }
  end

  # Implements MemexProjectItem::Content#memex_system_defined_column_value.
  def memex_system_defined_column_value(column, require_prefilled_associations: true)
    ensure_preloaded_association!(column) if require_prefilled_associations

    case column.name
    when MemexProjectColumn::ASSIGNEES_COLUMN_NAME, MemexProjectColumn::LABELS_COLUMN_NAME
      public_send(column.content_association)
        .sort_by { |i| i[column.content_sort_key]&.downcase }
        .map(&:memex_column_hash)
    when MemexProjectColumn::MILESTONE_COLUMN_NAME, MemexProjectColumn::REPOSITORY_COLUMN_NAME
      public_send(column.content_association)&.memex_column_hash
    when MemexProjectColumn::TITLE_COLUMN_NAME
      {
        number: number,
        state: state.to_s,
        title: title,
      }
    end
  end

  private

  def ensure_preloaded_association!(memex_column)
    if (Rails.env.test? &&
        memex_column.content_association &&
        !self.association(memex_column.content_association).loaded?)

      raise AssociationRefused.new(memex_column.content_association)
    end
  end

  # Used only in tests to report that a proposed operation is not efficient.
  class AssociationRefused < StandardError
    def initialize(association)
      super(
        "Refusing to map over unloaded :#{association} association: " +
        "please prefill this association efficiently before calling this method."
      )
    end
  end

end
