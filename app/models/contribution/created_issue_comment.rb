# frozen_string_literal: true

class Contribution::CreatedIssueComment < Contribution

  delegate :repository, :repository_id, to: :comment

  def associated_subject
    comment.repository
  end

  def occurred_at
    comment.created_at
  end

  def comment
    subject
  end

  def self.subjects_for(user, date_range:, organization_id: nil, excluded_organization_ids: [])
    ::Collab::Responses::Array.new do
      ActiveRecord::Base.connected_to(role: :reading) do
        return [] if user.spammy?

        scope = user.issue_comments
        scope = scope.for_organization(organization_id) if organization_id
        scope = scope.excluding_organization_ids(excluded_organization_ids) if excluded_organization_ids.any?

        scope.
          where(created_at: date_range_to_time_range(date_range)).
          includes(:repository).
          limit(Contribution::DEFAULT_COUNT_LIMIT)
      end
    end
  end
end
