# rubocop:disable Style/FrozenStringLiteralComment

class Contribution::JoinedGitHub < Contribution
  areas_of_responsibility :user_profile

  def associated_subject
    user
  end

  def occurred_at
    user.created_at
  end

  # Public: Formats the contribution time like 'March 17, 2009'.
  def pretty_date
    occurred_at.strftime("%B %-d, %Y")
  end

  def self.subjects_for(user, date_range:, organization_id: nil, excluded_organization_ids: [])
    subject = first_subject_for(user)
    date_range.cover?(subject.created_at.to_date) ? [subject] : []
  end

  def self.first_subject_for(user, excluded_organization_ids: [])
    user
  end

  def platform_type_name
    "JoinedGitHubContribution"
  end
end
