# rubocop:disable Style/FrozenStringLiteralComment

class Contribution::CreatedIssue < Contribution
  areas_of_responsibility :user_profile

  def async_repository
    subject.async_repository
  end

  # Public: Returns state of the pull request as a Symbol. (e.g. :merged)
  def state_class
    issue.state
  end

  # Public: The total number of comments on the issue.
  # Returns an Integer.
  def comments_count
    issue.issue_comments_count
  end

  def issue
    subject
  end

  def associated_subject
    issue.repository
  end

  def organization_id
    associated_subject.try(:organization_id)
  end

  # Public: Returns the issue's contribution time as a Time.
  def occurred_at
    issue.contribution_time
  end

  def eql?(other_contribution)
    other_contribution.respond_to?(:issue) && issue == other_contribution.issue
  end

  def hash
    issue.hash
  end

  def repository_id
    issue.repository_id
  end

  def platform_type_name
    "CreatedIssueContribution"
  end

  delegate :state, :repository, :contribution_time, :title, :number,
           to: :issue

  def self.subjects_for(user, date_range:, organization_id: nil, excluded_organization_ids: [])
    scope = user.issues.without_pull_requests.where(created_at: buffered_time_range(date_range))
    scope = scope.for_organization(organization_id) if organization_id
    scope = scope.excluding_organization_ids(excluded_organization_ids) if excluded_organization_ids.any?
    scope = scope.includes(:repository).limit(Contribution::DEFAULT_COUNT_LIMIT)
  end

  def self.first_subject_for(user, excluded_organization_ids: [])
    # Getting the first issue for a non-orphaned repository can be a
    # slow query, so only run it if this faster query indicates there
    # will be data. See https://github.com/github/github/issues/69815.
    return unless user.issues.without_pull_requests.exists?

    scope = user.issues.includes(:repository).joins(:repository).
      merge(Repository.with_owner).without_pull_requests
    scope = scope.excluding_organization_ids(excluded_organization_ids) if excluded_organization_ids.any?
    scope.order(created_at: :asc).first
  end

  def self.needs_filtering_by_occurred_at?
    true
  end
end
