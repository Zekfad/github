# frozen_string_literal: true

class Contribution::CreatedPullRequestReview < Contribution

  areas_of_responsibility :user_profile

  def self.subjects_for(user, date_range:, organization_id: nil, excluded_organization_ids: [])
    time_range = date_range_to_time_range(date_range)
    reviews = user.reviews.visible_in_timeline_for(nil).where(submitted_at: time_range)
    reviews = reviews.for_organization(organization_id) if organization_id
    reviews = reviews.excluding_organization_ids(excluded_organization_ids) if excluded_organization_ids.any?

    # Preload PR & repo
    reviews = reviews.includes(pull_request: :repository).limit(Contribution::DEFAULT_COUNT_LIMIT)

    # We only have to join to the pull_requests table to make sure we're
    # omitting reviews that no longer have a pull request. See
    # https://github.com/github/github/issues/64386#issuecomment-258510294
    reviews = reviews.joins(:pull_request)

    reviews_by_pr_id = reviews.group_by(&:pull_request_id)
    reviews_by_pr_id.map { |pull_request_id, pr_reviews| pr_reviews.max_by(&:submitted_at) }
  end

  def pull_request_review
    subject
  end

  def pull_request
    pull_request_review.pull_request
  end

  def repository
    pull_request.repository
  end

  def organization_id
    repository.try(:organization_id)
  end

  def repository_id
    pull_request.repository_id
  end

  def associated_subject
    repository
  end

  def occurred_at
    pull_request_review.submitted_at
  end

  def name_with_owner
    repository.name_with_owner
  end

  def title
    pull_request.title
  end

  def platform_type_name
    "CreatedPullRequestReviewContribution"
  end
end
