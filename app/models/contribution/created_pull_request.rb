# rubocop:disable Style/FrozenStringLiteralComment

class Contribution::CreatedPullRequest < Contribution

  BATCH_SIZE = 100

  areas_of_responsibility :user_profile

  def async_repository
    subject.async_repository
  end

  def async_issue
    pull_request.async_issue
  end

  def pull_request
    subject
  end

  # Public: Returns the pull request's contribution time as a Time.
  def occurred_at
    pull_request.contribution_time
  end

  def eql?(other_contribution)
    other_contribution.respond_to?(:pull_request) && pull_request == other_contribution.pull_request
  end

  def hash
    pull_request.hash
  end

  def repository
    pull_request.repository
  end

  def organization_id
    repository.try(:organization_id)
  end

  def associated_subject
    repository
  end

  def repository_id
    pull_request.repository_id
  end

  # Public: The total number of comments on the pull request.
  # Returns an Integer.
  def comments_count
    pull_request.issue_comments_count
  end

  # Public: Returns state of the pull request as a Symbol. (e.g. :merged)
  def state_class
    pull_request.state
  end

  delegate :number, to: :pull_request

  def platform_type_name
    "CreatedPullRequestContribution"
  end

  def self.subjects_for(user, date_range:, organization_id: nil, excluded_organization_ids: [])
    scope = user.pull_requests.where(created_at: buffered_time_range(date_range))
    scope = scope.for_organization(organization_id) if organization_id
    scope = scope.excluding_organization_ids(excluded_organization_ids) if excluded_organization_ids.any?
    pull_request_ids = scope.limit(Contribution::DEFAULT_COUNT_LIMIT).pluck(:id)

    pull_request_ids.each_slice(BATCH_SIZE).with_object([]) do |pull_ids, list|
      list.concat PullRequest.where(id: pull_ids).
        includes(:base_user, :head_user, :issue, repository: :owner)
    end
  end

  def self.first_subject_for(user, excluded_organization_ids: [])
    scope = user.pull_requests.includes(:repository, :issue).
      joins(:repository).merge(Repository.with_owner)
    scope = scope.excluding_organization_ids(excluded_organization_ids) if excluded_organization_ids.any?
    scope.order("pull_requests.created_at ASC").first
  end

  def self.needs_filtering_by_occurred_at?
    true
  end
end
