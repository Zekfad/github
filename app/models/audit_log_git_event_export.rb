# rubocop:disable Style/FrozenStringLiteralComment

require "github/redis/mutex_group"

class AuditLogGitEventExport < ApplicationRecord::Collab
  areas_of_responsibility :audit_log

  VALID_SUBJECT_TYPES = %w(User Organization Business)

  include Instrumentation::Model

  belongs_to :actor, class_name: "User"
  belongs_to :subject, polymorphic: true

  validates :subject_type, inclusion: { in: VALID_SUBJECT_TYPES }
  validates :subject_id,   presence: true
  validates :actor_id,     presence: true
  validates :token,        presence: true
  validates :start,        presence: true
  validates :end,          presence: true
  validate  :end_after_start

  after_initialize :default_values

  before_validation :generate_token, on: :create

  after_create :increment_create_count
  after_commit :enqueue_process_export_results, on: :create

  class ExportError < StandardError
  end

  # Class that implements a remote object
  # that can be used by the existing audit_log_helpers.rb
  # to render the results back to the user.
  class RemoteObject
    def initialize(results:)
      @results = results
    end

    def size
      @results.length
    end

    def get
      yield @results
    end
  end

  # Public: this is called from the asynchronous job to
  #         kick off a export job in Driftwood and wait
  #         for it to be done.
  #
  # Parameters:
  #
  #   lock_key - mutex group key to unlock once the process
  #              is done
  def process
    start_export_job

    resp = nil
    GitHub.dogstats.time("audit_log_git_event_export_time", tags: ["action:process"]) do
      start = Time.now
      loop do
        resp = Audit::Driftwood::GitEventExport.new.check_results(id: subject_id, token: token)
        break if resp[:finished]

        if Time.now > start + GitHub.audit_git_export_timeout
          GitHub.dogstats.increment("audit_log_git_event_export_timeout_count")
          raise ExportError, "timeout waiting for job to finish"
        end

        sleep(1)
      end
    end

    if resp.nil? || !resp[:successful]
      GitHub.dogstats.increment("audit_log_git_event_export_timeout_failed")
      raise ExportError, "job didn't complete successfully"
    end

    instrument :audit_log_git_event_export
  end

  def to_param
    token
  end

  def increment_create_count
    GitHub.dogstats.increment("audit_log_git_event_export_count", tags: ["action:create"])
  end

  def event_payload
    payload = {
      actor: actor,
      start: start,
      end: self[:end]
    }

    if subject.respond_to?(:event_prefix)
      payload[subject.event_prefix] = subject
    else
      raise ArgumentError, "#{subject} does not respond to #event_prefix"
    end

    payload
  end

  def content_type
    "application/zip"
  end

  # Public: fetch and catch the results of a previously started export job
  #
  # Returns: a RemoteObject instance that contains the results
  def remote_object
    if @remote.nil?
      resp = Audit::Driftwood::GitEventExport.new.fetch_results(id: subject_id, token: token)
      raise ExportError, "results are not available" unless resp[:successful]
      raise ExportError, "results are empty" unless resp[:json_zip].length > 0
      @remote = RemoteObject.new(results: resp[:json_zip])
    end
    @remote
  end

  # Public: verify if the results of a previously started export job are available
  #
  # Returns: bool
  def remote_object?
    resp = Audit::Driftwood::GitEventExport.new.check_results(id: subject_id, token: token)
    resp[:successful]
  end

  def human_filename
    "export-#{subject_label}-#{created_at.to_i}.zip"
  end

  # Public: The human readable name for the subject.
  #
  # Returns String.
  def subject_label
    case subject
    when User, Organization
      subject.login
    when Business
      subject.slug
    end
  end

  # Public: The event prefix for the audit log export event. It is dependent on
  # the subject the export is performed against.
  #
  # Returns Symbol
  def event_prefix
    subject.event_prefix
  end

  private

  # Private: The unique token for the audit log used to download the export.
  #
  # Returns String.
  def generate_token
    self.token = SecureRandom.uuid
  end

  # Private: Determine if the start date is before the end date
  #
  # Returns nothing.
  def end_after_start
    if self[:end] < start
      errors.add(:end, "must be after the start date")
    end
  end

  # Private: Process the export request in the background and report back to the
  # user when their export is ready for downloading.
  #
  # Returns nothing.
  def enqueue_process_export_results
    JobStatus.create(id: token)
    AuditLogGitEventExportJob.perform_later(export: self)
  end

  # Private: initializes the time range with (-24h, current_time) to use sane
  #          defaults
  def default_values
    self.start ||= Time.now - 1.day
    self.end ||= Time.now
  end

  # Private: kick off export job
  def start_export_job
    git_export = Audit::Driftwood::GitEventExport.new

    if subject_type == "Business"
      git_export.start_business_export(
        business_id: subject_id,
        token: token,
        start_time: start.to_time,
        end_time: self[:end].to_time)
        return
    end

    git_export.start_org_export(
      org_id: subject_id,
      token: token,
      start_time: start.to_time,
      end_time: self[:end].to_time)
  end
end
