# rubocop:disable Style/FrozenStringLiteralComment

require "github/dgit/enterprise.rb"

module Gist::Removal
  extend ActiveSupport::Concern

  included do
    scope :deleted, -> { where(delete_flag: true) }
    scope :alive,   -> { where(delete_flag: false) }
  end

  module ClassMethods
    # Public: Restores a gist and its dependents from the archive table.
    # If the gist has not been archived, just returns that gist.
    #
    # When restoring the gist some cleanup actions are performed:
    # - Unhides the gist so that it may be displayed.
    #
    # id - Integer ID of the gist to restore
    #
    # Returns the restored Gist.
    def restore(id)
      if existing_gist = find_by_id(id)
        return existing_gist
      end

      archived = Archived::Gist.find(id)

      restored_gist = archived.restore
      restored_gist.restore_to_disk

      restored_gist
    end
  end

  class GistRestoreError < StandardError; end
  class RepositoryNotFound < StandardError; end

  def restore_to_disk
    fileservers = fileservers_for_restore
    dest_fs, remaining_fs = fileservers[0], fileservers[1..-1]

    GitHub::Logger.log(at: "restore_to_disk",
                      dest_fs: dest_fs.name,
                      gist: self.id,
                      nwo: nwo,
                      now: Time.now.utc.iso8601)

    # Enterprise expects the data to have been left on disk.
    restore_to_dgit_from_backup(dest_fs) unless GitHub.enterprise?

    # Create new DB records such that this restored replica can be used to
    # populate other new replicas.
    GitHub::DGit::Maintenance.insert_restore_placeholder_gist_replica_and_checksum(self.id, dest_fs)

    GitHub::DGit::Enterprise.delete_archived_gist_replicas_and_checksums(self.id) if GitHub.enterprise?

    GitHub::DGit::Maintenance.recompute_gist_checksums(self, :vote, fileservers: [dest_fs])

    ctx = GitHub::DGit::Maintenance::GistMaintenanceContext.new
    remaining_fs.each do |fs|
      ctx.enqueue_create_replica(self.id, fs.name)
    end

    unhide
  end

  def remove(async: true)
    hide

    if async
      enqueue_archive_job
    else
      archive
    end
  end

  # Public: Archives the gist including its comments.
  #
  # Returns the Archived::Gist.
  def archive
    if GitHub.enterprise?
      grs = GitHub::DGit::Routing.all_gist_replicas(self.id)
      grs.each do |gr|
        sql = GitHub::DGit::DB.for_gist_id(self.id).SQL.new \
          gist_id: self.id,
          host: gr.host,
          checksum: gr.checksum,
          state: gr.state,
          read_weight: gr.read_weight,
          created_at: gr.created_at,
          updated_at: GitHub::SQL::NOW
        sql.run <<-SQL
          INSERT INTO archived_gist_replicas (gist_id, host, checksum, state, read_weight, created_at, updated_at)
          VALUES (:gist_id, :host, :checksum, :state, :read_weight, :created_at, :updated_at)
        SQL
      end
    end

    remove_from_disk(self)
    Archived::Gist.archive(self)
  end

  def remove_from_disk(gist)
    return if GitHub.enterprise?
    return unless gist.exists_on_disk?

    backup_ng! if backups_enabled?
    gist.rpc.fs_delete(".")
  end

  # TODO: Make these private. Public interface should focus on remove/archive/restore.
  # Public: soft-deletes the gist in prep for archiving.
  def hide
    transaction do
      touch_parent
      update!(delete_flag: true)
    end

    instrument :destroy
  end

  def unhide
    transaction do
      touch_parent
      update!(delete_flag: false)
    end
  end

  def active?
    !delete_flag
  end

  def enqueue_archive_job
    GistDeleteJob.perform_later(id, path)
  end

private

  # Picks fileservers to be the target for restoring
  def fileservers_for_restore
    existing_fileserver = nil

    if GitHub.enterprise?
      agrs = GitHub::DGit::Enterprise.archived_gist_replicas(self.id)
      online_replica = agrs.find(&:online?)
      raise Gist::Removal::GistRestoreError, "No available file server found: #{agrs.map(&:host)}" if online_replica.nil?
      existing_fileserver = online_replica.fileserver
    end

    begin
      new_fileservers = GitHub::Spokes.client.pick_fileservers(actor: user)
    rescue GitHub::DGit::HostSelectionError
      # Be satisified as long as we have *a* fileserver to restore to
      # This is really only something that can happen in enterprise.
      raise Gist::Removal::GistRestoreError, "No available file servers found" unless existing_fileserver
      new_fileservers = []
    end

    if existing_fileserver.nil?
      fileservers = new_fileservers
    else
      needed_copies = new_fileservers.size

      # Enterprise is tricky because we're relying on using a previous
      # replica where we assume the gist will still have data. But we also need
      # a full compliment of replicas. What we don't know is whether the
      # previous replica would have been selected for a brand-new placement.
      new_fileservers.select! do |fs|
        fs.name != existing_fileserver.name
      end

      fileservers = [existing_fileserver] + new_fileservers[0...(needed_copies-1)]
    end

    fileservers
  end

  def restore_to_dgit_from_backup(dest_fs)
    Failbot.push gist_repo_name: repo_name,
                 original_shard_path: original_shard_path,
                 dest_host: dest_fs.name

    dest_path = if Rails.development? || Rails.test?
      GitHub::DGit.dev_route(original_shard_path, dest_fs.name)
    else
      original_shard_path
    end

    dest_parent_path = File.dirname(dest_path)
    dest_clone_dir   = File.basename(dest_path)

    Failbot.push dest_parent_path: dest_parent_path,
                 dest_clone_dir: dest_clone_dir

    # RPC handle for cloning from the parent directory.
    parent_rpc = dest_fs.build_maint_rpc(File.dirname(original_shard_path))

    #
    # Sanity check: it is assumed that the last 5 directory components leading
    # up to a gist's location will be the same between the parent RPC handle
    # and the original shard path.
    #
    dest_last_five = dest_parent_path.split("/").last(5)
    parent_last_five = parent_rpc.backend.path.split("/").last(5)
    if dest_last_five != parent_last_five
      raise GistRestoreError.new(original_shard_path),
            "failed path check, aborting " \
            "(#{dest_last_five.inspect} != #{parent_last_five.inspect})"
    end

    # Move anything that might be there to a backup directory.  Any
    # exception here is ignored for the case that there are no such
    # contents already there.
    backup_dir = "gist-repair-backup-#{dest_clone_dir}-#{Time.now.to_i}"
    begin
      parent_rpc.fs_move(dest_clone_dir, backup_dir)
    rescue GitRPC::SystemError
    end

    begin
      parent_rpc.ensure_dir(dest_parent_path)
    rescue GitRPC::CommandFailed => e
      raise GistRestoreError, "failed to create directory: #{e}"
    end

    if Rails.development? || Rails.test?
      development_restore(dest_fs)
    else
      restore_from_gitbackups(parent_rpc, dest_parent_path)
    end

    # Sanity check again.
    gist_rpc = dest_fs.build_maint_rpc(original_shard_path)
    unless gist_rpc.exist?
      raise GistRestoreError.new(gist_rpc), "failed after clone, aborting"
    end

    # fixup hooks symlink
    source_hook_dir = "#{GitHub.gist3_repository_template}/hooks"
    begin
      gist_rpc.symlink_hooks_directory(link_source: source_hook_dir)
    rescue GitRPC::Failure => e
      raise GistRestoreError.new(e), "failed to symlink hooks directory, aborting"
    end
  end

  def restore_from_gitbackups(parent_rpc, dest_clone_dir)
    result = parent_rpc.gitbackups_restore(repository_spec, dest_clone_dir)
    raise RepositoryNotFound if result == :not_found
    raise GistRestoreError.new({out: result[0], err: result[1]}) if result.is_a?(Array)
  rescue GitRPC::CommandFailed => e
    raise GistRestoreError.new(e), "gist restore failed"
  end

  # There is no backup support in development so we just create an empty repository.
  def development_restore(dest_fs)
    rpc = dest_fs.build_maint_rpc(original_shard_path)
    rpc.ensure_initialized(hooks_template: "#{GitHub.gist3_repository_template}/hooks")
  end
end
