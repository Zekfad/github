# rubocop:disable Style/FrozenStringLiteralComment

module Gist::RefsDependency
  # For API compatibility with Repository::RefsDependency
  # Gists only allow "master" as a default branch.
  def default_branch
    "master"
  end

  # Public: This gist's refs.
  def refs
    return @refs if defined?(@refs)
    @refs = Git::Ref::Collection.new(loader: refs_loader)
  end

  # Public: Retrieve a ref collection for all branch refs in the gist.
  #
  # Returns a refs collection object with the prefix set to "refs/heads/".
  def heads
    return @heads if defined?(@heads)
    @heads = Git::Ref::Collection.new(loader: refs_loader, prefix: "refs/heads/")
  end

  private def refs_loader
    return @refs_loader if defined?(@refs_loader)
    @refs_loader = Git::Ref::Loader.new(self, "default")
  end

  # Public: Resolve a name or oid down to a commit oid.
  #
  # Returns a string commit oid when the revision is found, nil otherwise.
  def ref_to_sha(revision)
    if ref = refs.find(revision)
      ref.target_oid
    elsif GitRPC::Util.valid_full_sha1?(revision)
      revision
    end
  end

  # Public: Current HEAD commit sha1.
  #
  # Returns nil if there was an error.
  def sha
    rpc.gist_head_oid
  rescue GitRPC::Error, GitHub::DGit::UnroutedError
    nil
  rescue Errno::EHOSTUNREACH => boom
    Failbot.report boom, failbot_context
    nil
  end
  alias_method :default_oid, :sha

  # The oid of the master head commit.
  def master_oid
    return @master_oid if defined?(@master_oid)

    @master_oid =
      if ref = heads.find("master")
        ref.target_oid
      end
  end

  # Public: Clear the refs caches. This forces a fetch of refs hash data from
  # the repository on disk the next time #refs or #extended_refs is accessed by
  # any process.
  #
  # This should be called any time a ref is modified to make the change visible
  # to other processes. You typically don't need to worry about this if you're
  # using Ref#update since the refs cache is automatically cleared.
  #
  # Note that the refs cache key included the repository's pushed_at timestamp.
  # If the pushed_at timestamp on the receiver is not in sync with the database,
  # the wrong refs cache key will be cleared.
  #
  # Returns nothing.
  def clear_ref_cache
    rpc.clear_repository_reference_key!
    reset_refs
  end

  def reset_refs
    remove_instance_variable(:@refs) if defined?(@refs)
    remove_instance_variable(:@refs_loader) if defined?(@refs_loader)
    remove_instance_variable(:@heads) if defined?(@heads)
  end
end
