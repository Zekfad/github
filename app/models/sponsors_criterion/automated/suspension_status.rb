# frozen_string_literal: true

class SponsorsCriterion::Automated::SuspensionStatus < SponsorsCriterion::Automated

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    "suspension_status"
  end

  # Public: Indicates if the criterion is met. For SuspensionStatus, we return
  #         true if the sponsorable's account is not suspended.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    sponsors_membership.async_sponsorable.then do |sponsorable|
       !sponsorable.suspended?
    end
  end

  # Public: The value to store for this criterion, if applicable.
  #
  # Returns a Promise<nil>.
  def async_value
    Promise.resolve(nil)
  end
end
