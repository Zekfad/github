# frozen_string_literal: true

class SponsorsCriterion::Automated::BulkWriter
  def initialize(bulk_results:, throttle_writes: false)
    @bulk_results = bulk_results
    @throttle_writes = throttle_writes
  end

  def self.call(inputs)
    new(**inputs).call
  end

  # Public: Update the criteria records for the memberships.
  #
  # Returns an Array[SponsorsCriterion::Automated::BulkWriter::Result].
  def call
    @bulk_results.map do |result|
      write_records_for(
        membership: result.sponsors_membership,
        results: result.results,
      )
    end
  end

  class Result
    attr_reader :sponsors_membership, :success, :errors
    alias_method :success?, :success

    # sponsors_membership - The SponsorsMembership that the records are updated for
    # success - A Boolean indicating if updating the records succeeded
    # errors - An Array of any errors that occured when updating the records
    def initialize(sponsors_membership:, success:, errors:)
      @sponsors_membership = sponsors_membership
      @success = success
      @errors = errors
    end

    def self.success(sponsors_membership:)
      new(
        sponsors_membership: sponsors_membership,
        success: true,
        errors: [],
      )
    end

    def self.failure(sponsors_membership:, errors:)
      new(
        sponsors_membership: sponsors_membership,
        success: false,
        errors: errors,
      )
    end
  end

  private

  # Private: Create or update the criteria records for a membership.
  #
  # membership - The SponsorsMembership to update.
  # results – An Array of SponsorsCriterion::Automated::Result objects for the membership.
  #
  # Returns a SponsorsCriterion::Automated::BulkWriter::Result.
  def write_records_for(membership:, results:)
    criteria = membership.sponsors_memberships_criteria.index_by(&:sponsors_criterion_id)

    results.each do |result|
      next unless result.sponsors_criterion

      criterion_id = result.sponsors_criterion.id

      # update existing record if we already have recorded this check
      # for this membership
      if criterion = criteria[criterion_id]
        criterion.met = result.met
        criterion.value = result.value
      else
        membership.sponsors_memberships_criteria.build(
          sponsors_criterion: result.sponsors_criterion,
          met: result.met,
          value: result.value,
        )
      end
    end

    success = if @throttle_writes
      SponsorsCriterion.throttle_writes_with_retry(max_retry_count: 8) do
        membership.save
      end
    else
      membership.save
    end

    if success
      Result.success(sponsors_membership: membership)
    else
      Result.failure(
        sponsors_membership: membership,
        errors: membership.errors.full_messages,
      )
    end
  end
end
