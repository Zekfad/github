# frozen_string_literal: true

class SponsorsCriterion::Automated::OFACCompliance < SponsorsCriterion::Automated

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    "ofac_compliance"
  end

  # Public: Indicates if the criterion is met. For OFACCompliance, we return
  #         true if the sponsorable is not subject to OFAC sanctions.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    sponsors_membership.async_sponsorable.then do |sponsorable|
      !sponsorable.has_any_trade_restrictions?
    end
  end

  # Public: The value to store for this criterion, if applicable.
  #
  # Returns a Promise<nil>.
  def async_value
    Promise.resolve(nil)
  end
end
