# frozen_string_literal: true

class SponsorsCriterion::Automated::SpammyStatus < SponsorsCriterion::Automated

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    "spammy_status"
  end

  # Public: Indicates if the criterion is met. For SpammyStatus, we return
  #         true if the sponsorable's account is not spammy.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    sponsors_membership.async_sponsorable.then do |sponsorable|
       !sponsorable.spammy?
    end
  end

  # Public: The value to store for this criterion, if applicable.
  #
  # Returns a Promise<nil>.
  def async_value
    Promise.resolve(nil)
  end
end
