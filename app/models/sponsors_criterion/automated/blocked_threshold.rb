# frozen_string_literal: true

class SponsorsCriterion::Automated::BlockedThreshold < SponsorsCriterion::Automated

  MAXIMUM_BLOCKED_BY = 15

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    "blocked_threshold"
  end

  # Public: Indicates if the criterion is met. For BlockedThreshold, we return
  #         true if the sponsorable's account is not blocked by more users than
  #         the limit defined by MAXIMUM_BLOCKED_BY.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    async_blocked_count.then do |blocked_count|
      blocked_count <= MAXIMUM_BLOCKED_BY
    end
  end

  # Public: The number of users who have blocked this sponsorable.
  #
  # Returns a Promise<String>.
  def async_value
    async_blocked_count.then { |count| count.to_s }
  end

  private

  # Private: The number of users blocking a sponsorable account.
  #
  # Returns a Promise<Integer>.
  def async_blocked_count
    sponsors_membership.async_sponsorable.then do |sponsorable|
      sponsorable.ignored_by_users.then do |blocked|
        blocked.count
      end
    end
  end
end
