# frozen_string_literal: true

class SponsorsCriterion::Automated::AbuseReports < SponsorsCriterion::Automated

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    "abuse_reports"
  end

  # Public: Indicates if the criterion is met. For AbuseReports, we return
  #         true if the sponsorable's account has not been reported for abuse.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    async_abuse_report_count.then do |report_count|
      report_count == 0
    end
  end

  # Public: The number of abuse reports received for a sponsorable.
  #
  # Returns a Promise<String>.
  def async_value
    async_abuse_report_count.then { |count| count.to_s }
  end

  private

  # Private: The number of abuse reports received for a sponsorable account.
  #
  # Returns a Promise<Integer>.
  def async_abuse_report_count
    sponsors_membership.async_sponsorable.then do |sponsorable|
      sponsorable.async_received_abuse_reports.then do |abuse_reports|
        abuse_reports.count
      end
    end
  end
end
