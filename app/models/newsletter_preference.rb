# rubocop:disable Style/FrozenStringLiteralComment

class NewsletterPreference < ApplicationRecord::Domain::Users
  include Instrumentation::Model

  belongs_to :user

  validates_presence_of :user_id
  validates_uniqueness_of :user_id

  validate :single_preference
  validate :preference_present

  after_commit :instrument_create, on: :create
  after_commit :instrument_update, on: :update

  scope :transactional, -> { where("elected_transactional_at is not null") }

  # Public: Should a user receive marketing email?
  #
  # We assume that users do not want to receive marketing email
  # from GitHub unless they have set their email preference
  # to receive marketing email.
  #
  # user - a User object
  #
  # Returns a Boolean.
  def self.marketing?(user:)
    return false if user.nil? || user.new_record?

    preference = find_by_user_id(user.id)
    return false unless preference

    preference.elected_marketing_at.present?
  end

  # Public: Set a user's newsletter preference to
  # receive marketing/promotional email.
  #
  # user - a User object.
  # signup - Whether or not the setting is being set during initial signup. Defaults to false.
  #
  # Returns a NewsletterPreference object.
  def self.set_to_marketing(user:, signup: false, source: "undefined")
    tags = ["source:#{source}"]

    if preference = find_by_user_id(user.id)
      preference.update(elected_marketing_at: Time.current, elected_transactional_at: nil) if preference.elected_marketing_at.nil?
      tags << "action:updated"
    else
      preference = create(user: user, elected_marketing_at: Time.current, elected_transactional_at: nil)
      tags << "action:created"
    end

    GitHub.dogstats.increment("newsletter_preference.marketing", tags: tags)

    # Users shouldn't be enrolled in MailChimp on signup until they've verified their email address.
    unless signup
      # Remove all of the user's eligible emails from the suppression list
      # and subscribe them to the MailChimp master list.
      RemoveFromSuppressionListJob.perform_later(user.id)
    end

    preference
  end

  # Public: Set a user's newsletter preference to
  # only receive transactional email.
  #
  # user - a User object.
  #
  # Returns a NewsletterPreference object.
  def self.set_to_transactional(user:, source: "undefined")
    tags = ["source:#{source}"]

    if preference = find_by_user_id(user.id)
      preference.update(elected_marketing_at: nil, elected_transactional_at: Time.current) if preference.elected_transactional_at.nil?
      tags << "action:updated"
    else
      preference = create(user: user, elected_marketing_at: nil, elected_transactional_at: Time.current)
      tags << "action:created"
    end

    GitHub.dogstats.increment("newsletter_preference.transactional", tags: tags)

    # Deactivate any existing newsletter subscriptions.
    NewsletterSubscription.unsubscribe_all(user)

    # Add all of the user's emails to the suppression list.
    AddToSuppressionListJob.perform_later(user.id)

    preference
  end

  # Public: Which preference does the user have currently?

  # Returns a String.
  def name
    elected_marketing_at ? "marketing" : "transactional"
  end

  # Public: Returns if the user has marketing email opt in or "blank" if no preference is set.
  #
  # user - a User object
  #
  # Returns a Boolean or String.
  def self.marketing_preference(user:)
    return false if user.nil? || user.new_record?

    preference = find_by_user_id(user.id)
    return "blank" unless preference

    preference.elected_marketing_at.present?
  end

  private

  # Private: Validate that only one elected_at timestamp is set.
  #
  # We use the elected_marketing_at and elected_transactional_at timestamps to
  # set the user's preference to receive or not receive marketing email.
  # Only one can be set.
  #
  # Returns a Boolean.
  def single_preference
    if elected_marketing_at && elected_transactional_at
      errors.add(:base, "can only select one preference")
    end
  end

  def preference_present
    unless elected_marketing_at || elected_transactional_at
      errors.add(:base, "one preference must be set")
    end
  end

  def instrument_create
    instrument :create

    experiment_arm =
      case GitHub.context[:email_opt_in_experiment_arm]
      when "control"
        "OPT_IN_PLACEMENT_CONTROL"
      when "alternative"
        "OPT_IN_PLACEMENT_ALTERNATE"
      else
        GitHub.context[:email_opt_in_experiment_arm]
      end

    data = {
      user: {
        id: user.id,
        login: user.login,
        created_at: user.created_at,
        billing_plan: user.plan.name,
        spammy: user.spammy,
        type: user.class.name.upcase,
      },
      experimental_arm: experiment_arm,
      preference_choice: name,
      visitor_id: GitHub.context[:visitor_id].to_s,
      write_type: "CREATE",
    }

    GlobalInstrumenter.instrument("newsletter.preference.change", data)
  end

  def instrument_update
    instrument :update

    data = {
      user: {
        id: user.id,
        login: user.login,
        created_at: user.created_at,
        billing_plan: user.plan.name,
        spammy: user.spammy,
        type: user.class.name.upcase,
      },
      preference_choice: name,
      write_type: "UPDATE",
    }

    GlobalInstrumenter.instrument("newsletter.preference.change", data)
  end

  def event_payload
    {
      user:                     user,
      elected:                  name,
      elected_marketing_at:     elected_marketing_at,
      elected_transactional_at: elected_transactional_at,
    }
  end

end
