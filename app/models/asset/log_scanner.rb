# rubocop:disable Style/FrozenStringLiteralComment

class Asset::LogScanner
  TIME_OFFSET = 1.hour
  MAX_KEYS = 300

  class XmlBodyError < StandardError
    def initialize(file)
      super("fetching #{file.inspect} returned xml error")
    end
  end

  attr_reader :client
  attr_reader :bucket
  attr_reader :log_key_prefix
  attr_accessor :time_offset
  attr_accessor :max_keys
  attr_accessor :object_key_matcher
  attr_accessor :minimum_log_parts

  def initialize(client, bucket, log_key_prefix)
    @bucket = bucket
    @log_key_prefix = log_key_prefix
    @time_offset = TIME_OFFSET
    @max_keys = 300
    @object_key_matcher = nil
    @minimum_log_parts = 0
    @client = client
  end

  # Finds the next logs to scan based on the marker and this scanner's
  # properties. Does not read the log data until #each is called.
  def find(marker)
    earliest = marker_to_time(marker) - @time_offset

    logs = @client.list_objects(
      bucket: @bucket,
      marker: marker,
      max_keys: @max_keys,
    )

    is_truncated = logs.is_truncated
    log_files = logs.contents.delete_if do |log|
      log.key !~ /\A#{@log_key_prefix}\d{4}-\d{2}-\d{2}-\d{2}-\d{2}-\d{2}-[A-Z0-9]+\z/ ||
        marker_to_time(log.key) < earliest
    end

    log_files.map! do |log_file|
      Aws::S3::Object.new(@bucket, log_file.key, client: @client)
    end

    LogSet.new(self, log_files, is_truncated)
  end

  # log_key_prefix: "s3/"
  # s3/2015-01-27-01-32-29-A0A382B60FE63B4D
  def marker_to_time(key)
    parts = key.to_s[@log_key_prefix.size..-1].split("-")
    Time.utc(*parts[0..5])
  end

  def time_to_marker(time)
    time.strftime("#{@log_key_prefix}%Y-%m-%d-%H-%M")
  end

  def allows_object_key?(uri)
    return true unless @object_key_matcher
    uri =~ @object_key_matcher
  end

  # Represents a group of logs to parse. It is designed for a single parsing
  # attempt. Any errors will be reported to Failbot. Call LogScanner#find to
  # resume parsing logs in case of an error.
  class LogSet
    attr_reader :logs
    attr_reader :keys
    attr_reader :line_count
    attr_reader :log_count
    attr_reader :scan_errors

    def initialize(scanner, logs, is_truncated)
      @scanner = scanner
      @logs = logs
      @is_truncated = is_truncated
      @keys = []
      @line_count = 0
      @log_count = 0
      @scan_errors = []
      @scanned = false
      @client = scanner.client
    end

    def truncated?
      !!@is_truncated
    end

    def scan_error?
      @scan_errors.any?
    end

    def size
      @logs.size
    end

    def last_log_key
      @keys.last
    end

    def each(&block)
      raise "already scanned logs" if @scanned
      @scanned = true

      @logs.each do |log|
        log_date = @scanner.marker_to_time(log.key)
        begin
          each_line(log) do |line|
            next unless item = parse_line(line, log_date)
            block.call(log_date, log, item)
            @line_count += 1
          end
        rescue GitHub::Jobs::SyncAssetUsage::LogLineError => err
          raise_error(err.orig_err, s3_log_file: log.key, s3_log_line: err.line)
          return
        rescue StandardError => err
          raise_error(err, s3_log_file: log.key)
          return
        end
        @keys << log.key
        @log_count += 1
      end
    ensure
      @keys.sort! unless @keys.frozen?
      @keys.freeze
      @scan_errors.freeze
    end

    def raise_error(err, context)
      @scan_errors << err
      Failbot.report(err, context)
    end

    # Stream the object contents from a log file, and turn each line into an
    # s3 line. The S3Object#value interface is described here:
    #
    #   https://github.com/github/aws-s3/blob/7221224c8bcce12279ab8788b6b8f6bd4b838ddb/lib/aws/s3/object.rb#L477-L482
    def each_line(log_object, &block)
      remainder = ""
      line_ending = "\n" # s3 logs use \n, regardless of $/
      line_count = 0
      log_path = "#{log_object.bucket_name}/#{log_object.key}"

      @client.get_object(bucket: log_object.bucket_name, key: log_object.key) do |chunk|
        next if chunk.blank?

        pieces = chunk.split(line_ending)
        next if pieces.size.zero?

        pieces[0] = remainder + pieces[0] if remainder.size > 0
        if chunk.ends_with?(line_ending)
          remainder = ""
        else
          remainder = pieces.pop
        end

        pieces.each do |p|
          build_log_line(block, p, line_count, log_path)
          line_count += 1
        end
      end

      build_log_line(block, remainder, line_count, log_path) if remainder.size > 0
    end

    def build_log_line(block, line, line_count, log_path)
      if line_count == 0 && line =~ /^\s*<\?xml/i
        raise XmlBodyError.new(log_path)
      end

      logline = Asset::LogLine.new(line)

      # ensure the log has enough parts to process the entire log entry
      # https://github.com/github/aws-s3/blob/7221224c8bcce12279ab8788b6b8f6bd4b838ddb/lib/aws/s3/logging.rb#L178-L194
      if @scanner.minimum_log_parts.to_i > 0 && logline.parts.size < @scanner.minimum_log_parts
        return
      end

      block.call(logline)
    end

    def parse_line(line, log_date = nil)
      # Ignore this line if it has no request URI. This happens for S3 copying
      return if line.request_uri.nil?

      # ignore if it's a bad http status
      return if line.http_status.to_i > 299

      # Ignore this line if it happened 4 hours before the log time
      line_time = line.time.utc
      return if log_date && ((line_time - log_date.utc).abs / 60 / 60) > 4

      method, uri = *line.request_uri.split(" ")

      return unless @scanner.allows_object_key?(uri)

      query_string = uri.split("?", 2)[1]
      query_params = query_string.blank? ? {} : Rack::Utils.parse_query(query_string)

      {
        request_id: line.request_id,
        time: line_time,
        hour_aligned_time: line_time.change(min: 0, sec: 0),
        method: method.upcase,
        uri: uri,
        query: query_params,
        raw: line,
      }
    rescue ArgumentError => err
      if err.to_s =~ /no time information/i
        return nil
      end
      raise
    end
  end
end
