# rubocop:disable Style/FrozenStringLiteralComment

class Asset::Status < ApplicationRecord::Domain::Assets
  include Asset::Types

  self.table_name = :asset_statuses

  areas_of_responsibility :lfs

  include Asset::Status::ZuoraDependency

  def self.build_for_owner(asset_type, owner_id)
    ActiveRecord::Base.connected_to(role: :writing) do
      sql = github_sql.new(
        asset_type: self.asset_types[asset_type],
        owner_id: owner_id,
      )

      sql.run %Q(
        INSERT INTO asset_statuses (asset_type, owner_id, created_at, updated_at)
        VALUES (:asset_type, :owner_id, NOW(), NOW())
        ON DUPLICATE KEY UPDATE updated_at = NOW()
      )
    end
  end

  def self.data_pack_unit_price
    # This can result in HTTP calls to update exchange rates,
    # so don't assign it to a constant to avoid HTTP calls when booting the app.
    @_data_pack_unit_price ||= Billing::Money.new(500)
  end

  DATA_PACK_BANDWIDTH  = 50.0 # GB
  DATA_PACK_STORAGE    = 50.0 # GB
  FREE_BANDWIDTH_QUOTA = 1.0  # GB
  FREE_STORAGE_QUOTA   = 1.0  # GB

  # A list of user IDs who are exempt from bandwidth quota checks.
  #
  # This is a temporary list of existing users to allow them to come into
  # compliance.  Normally this list should be empty.
  USER_IDS_EXEMPT_FROM_BANDWIDTH_QUOTA = Set.new([
    6124714, # babylonhealth
    2034458, # algolia
    1342004, # google
    1784819, # eightcard
    9811576, # Verasl
    45615063, # binance-chain
    1194464, # sumup
    16404016, # ImagenTechnologies
    15328982, # miraibox
    8613067, # kolibree-git
    26282870, # carbonated-dev
    33469677, # ninesinc
    43072207, # Faire
    44989994, # ekgamesGit
    41761423, # Lumen5
    49271625, # mutate-ab
    7308261, # wagonhq
    2582296, # westjet
    5193128, # illera88
    22245363, # Nuro-ai
    12452489, # openfmb
    16158762, # MerrillCorporation
    2931911, # quip
    4257267, # senderle
    260667, # amir20
    26666889, # brexhq
    5213110, # ShaneD53
    1218847, # joeblau
    1822661, # odnh
    29658246, # hm-it
    9861553, # leonteq
    19538647, # facebookincubator
    36013696, # eagleinvsys
    10530973, # Smittyvb
    1572679, # kanishkaganguly
    15138188, # Flynn-Apps-Ltd
    15220349, # RespinGames
    12844409, # DSB-IT
    813788, # leonardCarpentier
    16391639, # RadikSalakhov
    8573584, # rpwachowski
    14876095, # FeedJonathanFoundation
    968319, # lionep
    1300129, # zoltaroth
    4125105, # RaimondKempees
    2927513, # iCHEF
    19436907, # UPOD-2016
    3655882, # overhq
    16494024, # maliceio
    3763518, # zaiminc
    1418487, # prichey
    10761077, # Disciple-Media
    10403622, # myPinPad
    1630970, # liftopia
    4674703, # ClassifiedAds
    445750, # carezone
    678853, # coryleeio
    18548691, # WeFarm
    10774714, # o2-priority
    6309667, # gamalon
    19646120, # poppin-games
    24317940, # SCP093-Dev
    24468737, # ReneoIO
    24493957, # snappypark
    547012, # cleanunicorn
    1747533, # VCNC
    5561132, # halilozercan
    25407930, # digitalglue-software
    2392542, # janpawellek
    25486060, # cancer-genomics
    3187922, # depop
    10700525, # JamesAarronCaudill
    27927831, # SystemSpace
    28687811, # PubOrganization
    20791525, # cusanoalessio
    28539879, # HEXproject
    23650344, # bluzelle
    29809362, # airsonic
    17579780, # qoriq-open-source
    26567514, # lsdk
    16629827, # JustinDarlington
    30869212, # crimson-unicorn
    31074122, # EvictionLab
    31020370, # waddletown
    12549192, # USGS-EROS
    7284022, # 0urobor0s
    16281093, # Mrwilson2015
    10659544, # Einstein42
    32538073, # romvo
    20730184, # populin
    32883608, # slicelife
    20883419, # we-inc
    24300208, # globus-labs
    1102621, # devendrachaplot
    32600154, # NervInGame
    2719533, # ryohei-kamiya
    36249656, # appman-agm
    6373759, # Darkmyr
    38028474, # AYMCommerce
    34948587, # datamastery
    1262456, # pnuzhdin
    38526945, # ProjectDrgn
    38981146, # albusapp
    30433438, # escience-academy
    39524425, # stitch3d
    32261543, # tensorlayer
    9928134, # kevlatus
    40441775, # Pay-Baymax
    41692909, # tgbsf-wivan
    32821091, # hse-aml
    37064221, # i-con
    9694108, # plantier-clement
    43095917, # domainadaptation
    19332270, # acouloir
    11455958, # rockspoon
    16025679, # YBIGTA
    6388508, # roscue
    47884629, # discovery-ltd
    47984052, # caurateam
    40217997, # PickFlicks
    17814736, # amethyst
    26596429, # AntSwordProject
    44132006, # mugazzz
    36465262, # RLBot
    7514500, # lexmed
    16110040, # eighties-cities
    47796399, # NuSpaceSim
    27217493, # bst-mug
    42146413, # iplab-naist
    30798381, # KIT-CMS
    20902775, # M2PRO-VR
    26769197, # des-science
    38851171, # HumanCapitalAnalysis
    23032665, # mediathekview
    52448830, # koreauniv12
    53986450, # copperforgellc
  ])

  # Public: User/Org that owns this asset status.
  belongs_to :owner, class_name: "User"
  validates_presence_of :owner_id

  # Public: Float total bandwidth down in GB accrued in the current billing cycle.
  # column :bandwidth_down
  validates_numericality_of :bandwidth_down, greater_than_or_equal_to: 0.0

  # Public: Float total bandwidth up in GB accrued in the current billing cycle.
  # column :bandwidth_up
  validates_numericality_of :bandwidth_up, greater_than_or_equal_to: 0.0

  # Public: Float total storage up in GB accrued in the current billing cycle.
  # column :storage
  validates_numericality_of :storage, greater_than_or_equal_to: 0.0

  # Public: Integer number of data packs purchased by the user/org.
  # column :asset_packs
  validates_numericality_of :asset_packs, greater_than_or_equal_to: 0

  # Public: Integer number of data packs purchased by the user/org.
  # column :data_packs
  validates_numericality_of :data_packs, greater_than_or_equal_to: 0

  # Public: Enumeration of valid notified states.
  enum notified_state: {
    none: 0,
    approaching_quota: 1, # At 80% of quota
    over_quota: 2, # At 100%+ of bandwidth quota
    disabled_over_quota: 3, # At 150% of quota, auto disabled
    disabled_abuse: 4, # Disabled due to abuse
  }, _prefix: true

  validates :notified_state, presence: true

  # Public: Check usage against quotas and notify the owner if necessary. If an
  # email notification is necessary a job is queued to send the email. See
  # Asset::Status#send_quota_notification.
  #
  # Returns nothing.
  def check_quotas_and_notify
    return if skip_quota_check?

    current_state = if approaching_bandwidth_quota? || approaching_storage_quota?
      :approaching_quota
    elsif over_bandwidth_quota? || over_storage_quota?
      :over_quota
    elsif over_bandwidth_quota_needs_disabling?
      if disable_when_over_bandwidth_quota?
        :disabled_over_quota
      else
        :over_quota
      end
    elsif over_storage_quota_needs_disabling?
      :disabled_over_quota
    end

    return unless current_state
    return if notified_at && notified_state == current_state.to_s
    send_quota_notification(state: current_state)
  end

  def disable_when_over_bandwidth_quota?
    !USER_IDS_EXEMPT_FROM_BANDWIDTH_QUOTA.include?(owner_id)
  end

  # Public: Send the owner of this asset status a notification about the state
  # of their storage/bandwidth quota.
  #
  # state - Symbol state to notify about. See notified_states.
  #
  # Returns nothing.
  def send_quota_notification(state:)
    return if skip_quota_check?

    unless self.class.notified_states[state].present?
      raise ArgumentError, "state must be one of #{self.class.notified_states.keys.inspect}"
    end

    return if notified_at && notified_state == state.to_s

    transaction do
      update(notified_at: DateTime.now,
        notified_state: state)

      case state
      when :approaching_quota
        AssetStatusMailer.approaching_quota(self).deliver_later
      when :over_quota
        AssetStatusMailer.over_quota(self).deliver_later
      when :disabled_over_quota
        disable_owner_feature
        AssetStatusMailer.over_quota_disable(self).deliver_later
      else
        return
      end

      GitHub.dogstats.increment("asset_status.notifications", tags: ["state:#{state}"])
    end
  end

  # Public: Float bandwidth quota.
  def bandwidth_quota
    monthly_quota = [FREE_BANDWIDTH_QUOTA, asset_packs * DATA_PACK_BANDWIDTH].max
    duration_in_months = async_owner.then do |owner|
      owner.plan_duration_in_months
    end.sync
    monthly_quota * duration_in_months
  end

  # Public: Float bandwidth usage.
  def bandwidth_usage
    bandwidth_down.round(2)
  end

  # Public: Integer bandwidth usage percentage
  def bandwidth_usage_percentage
    percent = (bandwidth_usage / bandwidth_quota).round(2)
    (percent * 100).to_i
  end

  # Public: Boolean if bandwidth is between 80% - 100% of quota.
  def approaching_bandwidth_quota?
    bandwidth_down >= 0.8 * bandwidth_quota && bandwidth_down <= bandwidth_quota
  end

  # Public: Boolean if bandwidth is between 100% - 150% of quota.
  def over_bandwidth_quota?
    bandwidth_down > bandwidth_quota && bandwidth_down < 1.5 * bandwidth_quota
  end

  # Public: Boolean if bandwidth is between 100% - 150% of quota.
  def over_bandwidth_quota_needs_disabling?
    bandwidth_down >= 1.5 * bandwidth_quota
  end

  # Public: Float storage quota.
  def storage_quota
    [FREE_STORAGE_QUOTA, asset_packs * DATA_PACK_STORAGE].max
  end

  # Public: Float storage usage.
  def storage_usage
    storage.round(2)
  end

  # Public: Integer storage usage percentage
  def storage_usage_percentage
    percent = (storage_usage / storage_quota).round(2)
    (percent * 100).to_i
  end

  # Public: Boolean if storage is between 80% - 100% of quota.
  def approaching_storage_quota?
    storage >= 0.8 * storage_quota && storage <= storage_quota
  end

  # Public: Boolean if storage is between 100% - 150% of quota.
  def over_storage_quota?
    storage > storage_quota && storage < 1.5 * storage_quota
  end

  # Public: Boolean if storage is between 100% - 150% of quota.
  def over_storage_quota_needs_disabling?
    storage >= 1.5 * storage_quota
  end

  # Public: Update the number of purchased data packs.
  #
  # quantity - Integer new quantity of data packs.
  # actor    - User making the change.
  #
  # Returns nothing.
  def update_data_packs(quantity:, actor:, force: false)
    old_data_packs = asset_packs
    return if old_data_packs == quantity

    if !force && old_data_packs > quantity.to_i
      Billing::SchedulePlanChange.run \
        account: owner,
        actor: actor,
        data_packs: quantity
    else
      # Temporarily write to both columns while we rename asset_packs to data_packs
      update!(asset_packs: quantity.to_i)
      update!(data_packs: quantity.to_i)

      # We need to update the pending plan change with the new data count
      if owner.pending_cycle_change
        owner.pending_cycle_change.update(data_packs: quantity.to_i)
      end

      owner.update_plan_with_data_packs
      owner.track_data_pack_change(actor, old_data_packs: old_data_packs)

      GlobalInstrumenter.instrument(
        "billing.lfs_change",
        actor_id: actor.id,
        user_id: owner.id,
        old_lfs_count: old_data_packs,
        new_lfs_count: quantity,
      )

      # Turn on Git LFS if the new data packs fully cover usage.
      if !owner_feature_enabled? &&
         !over_storage_quota? && !over_storage_quota_needs_disabling? &&
         !over_bandwidth_quota? && !over_bandwidth_quota_needs_disabling?
        enable_owner_feature
      end
    end
  end

  # Public: Resets and rebuilds Asset::Status bandwidth and storage meters
  def rebuild(skip_notify: false)
    self.bandwidth_up = 0.0
    self.bandwidth_down = 0.0
    start_of_billing_cycle = owner.first_day_in_lfs_cycle.to_time.utc

    ActiveRecord::Base.connected_to(role: :reading) do
      Asset::Activity.fetch_for_owner(asset_type, owner, start_of_billing_cycle, Time.now.utc).each do |asset|
        self.bandwidth_up += asset.bandwidth_up
        self.bandwidth_down += asset.bandwidth_down
      end
    end

    calculate_storage
    save

    check_quotas_and_notify unless skip_notify
  end

  def disabled_because_over_quota?
    !owner_feature_enabled? &&
      (over_storage_quota? || over_storage_quota_needs_disabling? ||
       over_bandwidth_quota? || over_bandwidth_quota_needs_disabling?)
  end

  # Future placeholder to block Asset access for invalid uses.
  def active?
    skip_quota_check? || !disabled_because_over_quota?
  end

  def used?
    storage_usage.nonzero? || bandwidth_usage.nonzero?
  end

  # Public: Calculates total storage consumed by the owner of this Asset::Status
  # record in GB and assigns the :storage field.
  #
  # Warning: This is a slow method and should only be called by a job.
  #
  # Returns total storage in GB.
  def calculate_storage
    self.storage = if lfs?
      Media::Blob.storage_by_owner(owner) / (1024 ** 3).to_f
    else
      0
    end
  end

  def skip_quota_check?
    GitHub.enterprise? || !lfs?
  end

  private

  def enable_owner_feature
    return unless lfs?
    owner.enable_git_lfs(owner)
  end

  def disable_owner_feature
    return unless lfs?
    owner.disable_git_lfs(owner)
  end

  def owner_feature_enabled?
    return true unless lfs?
    owner.git_lfs_enabled?
  end
end
