# frozen_string_literal: true

module Asset::Status::ZuoraDependency
  extend ActiveSupport::Concern

  class_methods do
    def zuora_id(cycle:)
      product_uuid(cycle).zuora_product_rate_plan_id
    end

    def zuora_charge_ids(cycle:)
      product_uuid(cycle).zuora_product_rate_plan_charge_ids
    end

    def product_uuid(billing_cycle)
      ::Billing::ProductUUID.find_by(product_type: "github.lfs", product_key: "v0", billing_cycle: billing_cycle)
    end

    def sync_to_zuora
      GitHub::Billing::ZuoraProduct.create \
        product_type: "github.lfs",
        product_key: "v0",
        product_name: zuora_product_name,
        charges: zuora_charges
    end

    def zuora_charges
      [{
        type: :unit,
        prices: { year: yearly_cost_in_dollars, month: monthly_cost_in_dollars },
        unit: "Seats",
      }]
    end

    def yearly_cost_in_dollars
      Billing::Money.new(data_pack_unit_price * 12).dollars
    end

    def monthly_cost_in_dollars
      data_pack_unit_price.dollars
    end

    def zuora_product_name
      "Git LFS Data Pack"
    end
  end
end
