# frozen_string_literal: true

module CodeScanning
  class Tool
    def self.known_tools
      return @known_tools if defined? @known_tools
      if GitHub.enterprise?
        @known_tools = [self.new("CodeQL", "CodeQL")]
      else
        @known_tools = [self.new("CodeQL command-line toolchain", "CodeQL")]
      end
      @known_tools +=  [
          self.new("Golang security checks by gosec", "gosec"),
          self.new("Security audit for python by bandit", "bandit"),
      ]
    end

    def self.tools_by_name
      @tools_by_name ||= known_tools.index_by(&:name)
    end

    def self.tools_by_display_name
      @tools_by_display_name ||= known_tools.index_by(&:display_name)
    end

    def self.from_name(name)
      tools_by_name[name] || self.new(name, name)
    end

    def self.from_name_or_display_name(namelike)
      tools_by_name[namelike] || tools_by_display_name[namelike] || self.new(namelike, namelike)
    end

    def self.default_tool_name
      known_tools.first.name
    end

    attr_reader :name, :display_name

    def initialize(name, display_name)
      @name = name
      @display_name = display_name
    end
  end
end
