# frozen_string_literal: true

module SamlProviderAlgorithms

  # The USE_DEFAULT_ options provided here are so that ActiveRecord::Enum knows what
  # to do with the default value of 0. This will get re-written to the proper default key
  # in the before_validation callbacks.

  # https://www.w3.org/TR/2002/REC-xmlenc-core-20021210/Overview.html#sec-Alg-MessageAuthentication
  SIGNATURE_METHOD_MAPPING = {
    "USE_DEFAULT_SIGNATURE_MAPPING"                     => 0,
    "http://www.w3.org/2001/04/xmldsig-more#rsa-sha1"   => 160,
    "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256" => 256,
    "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384" => 384,
    "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512" => 512,
  }
  DEFAULT_SIGNATURE_METHOD = SIGNATURE_METHOD_MAPPING.key(256)

  # https://www.w3.org/TR/2002/REC-xmlenc-core-20021210/Overview.html#sec-Alg-MessageDigest
  DIGEST_METHOD_MAPPING = {
    "USE_DEFAULT_DIGEST_MAPPING"              => 0,
    "http://www.w3.org/2000/09/xmldsig#sha1"  => 160,
    "http://www.w3.org/2001/04/xmlenc#sha256" => 256,
    "http://www.w3.org/2001/04/xmlenc#sha384" => 384,
    "http://www.w3.org/2001/04/xmlenc#sha512" => 512,
  }
  DEFAULT_DIGEST_METHOD = DIGEST_METHOD_MAPPING.key(256)

  def self.included(base)
    base.enum signature_method: SIGNATURE_METHOD_MAPPING
    base.enum digest_method: DIGEST_METHOD_MAPPING
    base.before_validation :set_default_signature_method, :set_default_digest_method
  end

  def set_default_signature_method
    return unless self.class.signature_methods[signature_method].zero?
    self.signature_method = DEFAULT_SIGNATURE_METHOD
  end

  def set_default_digest_method
    return unless self.class.digest_methods[digest_method].zero?
    self.digest_method = DEFAULT_DIGEST_METHOD
  end
end
