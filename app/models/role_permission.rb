# frozen_string_literal: true

class RolePermission < ApplicationRecord::Iam
  validates :fine_grained_permission_id, presence: true
  validates :role_id, presence: true

  belongs_to :role
  belongs_to :fine_grained_permission

  scope :custom_roles_enabled, -> {
    joins(:fine_grained_permission).where(fine_grained_permissions: {custom_roles_enabled: true})
  }
end
