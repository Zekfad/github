# frozen_string_literal: true

module UserAsset::Quarantine
  # Public: Makes the asset not visible to anyone on GitHub, but does not delete it
  # Built for Project Schaefer https://github.com/github/schaefer
  def quarantine(reason:)
    return false if GitHub.storage_cluster_enabled?
    if make_private && clear_from_cdn_cache
      instrument :quarantine, user_asset: self, reason: reason
      true
    else
      false
    end
  end

  def unquarantine(reason:)
    return false if GitHub.storage_cluster_enabled?
    if set_default_acl && clear_from_cdn_cache
      instrument :unquarantine, user_asset: self, reason: reason
      true
    else
      false
    end
  end

  def set_default_acl
    return false if GitHub.storage_cluster_enabled?

    storage_s3_client.put_object_acl(
      bucket: storage_s3_bucket,
      key: storage_s3_key(nil),
      acl: storage_policy.acl,
    )

    true
  end

  def make_private
    return false if GitHub.storage_cluster_enabled?

    storage_s3_client.put_object_acl(
      bucket: storage_s3_bucket,
      key: storage_s3_key(nil),
      acl: "private",
    )

    true
  end

  def clear_from_cdn_cache
    return false if GitHub.storage_cluster_enabled?

    urls = [storage_external_url]
    with_storage_provider(:default) do
      urls << storage_external_url
    end

    urls.compact.each do |url|
      PurgeFastlyUrlJob.perform_later("url" => url)
    end

    true
  end
end
