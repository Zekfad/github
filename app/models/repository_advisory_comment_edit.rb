# rubocop:disable Style/FrozenStringLiteralComment

class RepositoryAdvisoryCommentEdit < ApplicationRecord::Domain::Repositories
  include FilterPipelineHelper
  include Instrumentation::Model
  include UserContentEdit::Core

  belongs_to :repository_advisory_comment

  alias_attribute :user_content_id, :repository_advisory_comment_id
  alias_method :user_content, :repository_advisory_comment
  alias_method :async_user_content, :async_repository_advisory_comment

  def user_content_type
    "RepositoryAdvisoryComment"
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, user_content_edit_id || "RepositoryAdvisoryCommentEdit:#{id}")
  end
end
