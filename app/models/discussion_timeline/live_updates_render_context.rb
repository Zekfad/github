# frozen_string_literal: true

# Public: Represents new comments, events, and event groups that need to be
# rendered on the page via ajax.
class DiscussionTimeline::LiveUpdatesRenderContext
  attr_reader :discussion, :viewer, :timeline_last_rendered

  def initialize(discussion, viewer:, timeline_last_rendered:)
    @discussion = discussion
    @viewer = viewer
    @timeline_last_rendered = timeline_last_rendered
  end

  def renderables
    @renderables ||= [
      :unread_marker,
      new_timeline_items,
    ]
  end

  def timeline_items
    @timeline_items ||= new_timeline_items
  end

  private

  def new_timeline_items
    @new_timeline_items ||= DiscussionTimeline::ItemFinder
      .new(discussion, viewer: viewer)
      .select_top_level_items
      .since(timeline_last_rendered)
      .to_a
  end
end
