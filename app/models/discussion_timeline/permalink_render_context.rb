# frozen_string_literal: true

# Public: Represents a permalinked comment in a single discussions show view
# e.g., `discussioncomment-1` in "http://github.localhost/user/repo/discussions/1#discussioncomment-1"
class DiscussionTimeline::PermalinkRenderContext
  attr_reader :discussion, :viewer

  def initialize(discussion, viewer:, before_cursor:, after_cursor:, permalink_comment_id:)
    @discussion = discussion
    @viewer = viewer
    @before_cursor = before_cursor
    @after_cursor = after_cursor
    @permalink_comment_id = permalink_comment_id
  end

  def renderables
    if permalink_comment.present?
      [
        hidden_items_before_permalink_comment,
        [permalink_comment],
        hidden_items_after_permalink_comment,
      ]
    else
      [
        DiscussionTimelineHiddenItems.new(
          all_timeline_items.count,
          before: before_cursor,
          after: after_cursor,
        )
      ]
    end
  end

  def timeline_items
    if permalink_comment.present?
      [permalink_comment]
    else
      []
    end
  end

  private

  attr_reader :before_cursor, :after_cursor, :permalink_comment_id

  def permalink_comment
    @permalink_comment ||= all_timeline_items.detect do |event|
      nested_comment_matches = if event.respond_to?(:comments)
        event.comments.any? { |e| e&.id == permalink_comment_id }
      end

      nested_comment_matches || event&.id == permalink_comment_id
    end
  end

  def permalink_comment_index
    @permalink_comment_index ||= all_timeline_items.index(permalink_comment)
  end

  def hidden_items_before_permalink_comment
    DiscussionTimelineHiddenItems.new(
      permalink_comment_index,
      before: permalink_comment,
      after: after_cursor,
    )
  end

  def hidden_items_after_permalink_comment
    after_cursor_count = [0, all_timeline_items.size - permalink_comment_index - 1].max

    DiscussionTimelineHiddenItems.new(
      after_cursor_count,
      before: before_cursor,
      after: permalink_comment,
    )
  end

  def all_timeline_items
    @all_timeline_items ||= begin
      timeline = DiscussionTimeline::ItemFinder
        .new(discussion, viewer: viewer)
        .select_top_level_items

      if before_cursor && after_cursor
        timeline.between_cursors(before: before_cursor, after: after_cursor)
      end

      timeline.to_a
    end
  end
end
