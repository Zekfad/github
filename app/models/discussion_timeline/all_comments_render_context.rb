# frozen_string_literal: true

# Public: Represents the state of the timeline items we will render and the
# timeline items themselves. Used by DiscussionTimeline to determine what
# comments to use when calling helper methods. e.g. Permissions, body HTML,
# etc.
class DiscussionTimeline::AllCommentsRenderContext
  attr_reader :discussion, :viewer

  def initialize(discussion, viewer:)
    @discussion = discussion
    @viewer = viewer
  end

  def renderables
    [
      timeline_items,
      DiscussionTimeline::UNREAD_MARKER,
    ]
  end

  def timeline_items
    @timeline_items ||= DiscussionTimeline::ItemFinder
      .new(discussion, viewer: viewer)
      .select_top_level_items
      .to_a
  end
end
