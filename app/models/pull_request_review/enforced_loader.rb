# frozen_string_literal: true

class PullRequestReview
  class EnforcedLoader
    def initialize(repository:, pull_request: nil, pull_request_head_sha: nil,
        base_ref_name: nil, writers_only: false, open_pulls_only: false)
      if [pull_request, pull_request_head_sha && base_ref_name].all?(&:blank?)
        raise ArgumentError, "either pull_request or (pull_request_head_sha && base_ref_name) must be present"
      end

      @repository = repository
      @pull_request = pull_request
      @pull_request_head_sha = pull_request_head_sha
      @writers_only = writers_only
      @open_pulls_only = open_pulls_only
      @base_ref_name = base_ref_name
    end

    # Public: Returns a list of the most recent review that each user has left,
    # as long as the review's status is approved or rejected.
    #
    # Includes dismissed in the query so that we can filter those out after.
    # This is so that we consider a most recent review which is dismissed
    # to be essentially ignored in the merge area.
    #
    # Returns Array of PullRequestReviews
    def execute
      load_reviews.tap do |reviews|
        prefill_pull_request(reviews)
        reviews.reject!(&:dismissed?)
        select_writers_only!(reviews) if writers_only
      end
    end

    private

    attr_reader :repository, :pull_request, :pull_request_head_sha,
                :writers_only, :open_pulls_only, :base_ref_name

    def load_reviews
      PullRequestReview.github_sql.new(sql_query,
        repository_id: repository.id,
        head_sha: pull_request_head_sha,
        pull_request_id: pull_request&.id,
        base_ref_names: base_ref_name ? base_ref_names_for_query : nil,
        states: [
          PullRequestReview.state_value(:approved),
          PullRequestReview.state_value(:changes_requested),
          PullRequestReview.state_value(:dismissed),
        ],
      ).models(PullRequestReview)
    end

    def select_writers_only!(reviews)
      review_by_writer = Promise.all(reviews.map { |review|
        review.async_author_can_push_to_repository?.then do |made_by_writer|
          [review, made_by_writer]
        end
      }).sync.to_h

      reviews.select! { |review| review_by_writer[review] }
    end

    def base_ref_names_for_query
      Git::Ref.permutations(base_ref_name).map { |ref| GitHub::SQL::BINARY(ref) }
    end

    def sql_query
      # Group-wise maximum against id column rather than updated_at because
      # http://bugs.mysql.com/bug.php?id=54784
      <<-SQL
        SELECT r1.*
        FROM pull_request_reviews r1
        INNER JOIN
        (
          SELECT max(r3.id) as id
          FROM pull_request_reviews r3
          #{lookup_query_clause}
          AND   r3.state IN :states
          GROUP BY r3.pull_request_id, r3.user_id
        ) r2
        ON r1.id = r2.id
        ORDER BY r1.updated_at DESC
      SQL
    end

    def lookup_query_clause
      if pull_request_head_sha
        head_sha_query_clause
      else
        <<-SQL
          WHERE pull_request_id = :pull_request_id
        SQL
      end
    end

    def head_sha_query_clause
      if open_pulls_only
        <<-SQL
          INNER JOIN pull_requests ON pull_requests.id = r3.pull_request_id
          INNER JOIN issues ON issues.pull_request_id = r3.pull_request_id
          WHERE pull_requests.repository_id = :repository_id
          AND   pull_requests.head_sha = :head_sha
          AND  	issues.state = 'open'
          AND   pull_requests.base_ref IN :base_ref_names
        SQL
      else
        <<-SQL
          INNER JOIN pull_requests ON pull_requests.id = r3.pull_request_id
          WHERE pull_requests.repository_id = :repository_id
          AND   pull_requests.head_sha = :head_sha
          AND   pull_requests.base_ref IN :base_ref_names
        SQL
      end
    end

    # Prefill pull request on every review so it isn't loaded by each review
    # when select_writers_only! is called
    def prefill_pull_request(reviews)
      return unless pull_request

      reviews.each do |review|
        # Check ID since reviews may be loaded by head SHA instead of pull
        # request ID
        next if review.pull_request_id != pull_request.id
        review.association(:pull_request).target = pull_request
      end
    end
  end
end
