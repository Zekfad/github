# rubocop:disable Style/FrozenStringLiteralComment

# PullRequestReview::Loader handles setting the review_comment association on PullRequestReviews,
# and also loads the reply associations on all the PullRequestReviewComments.
#
# Note that this object makes NO SQL queries!  It expects you, the caller, to
# get the reviews and pull_request_review_thread(s) you care about for a particular
# PullRequest.  This object just handles setting up association(s) to make
# sure ActiveRecord doesn't go and load objects in the view that we've already
# loaded.
class PullRequestReview::Loader
  # Create a Loader with all the PullRequestReviews and DeprecatedPullRequestReviewThread
  # for a particular PullRequest.  We assume you have loaded the `comments`
  # array on the PullRequestReviewThread objects already.
  #
  # reviews - collection of PullRequestReviews
  # threads - collection of DeprecatedPullRequestReviewThread
  def initialize(reviews, threads)
    @reviews = reviews
    @comments = threads.flat_map(&:comments)
  end

  # Do the loading!
  #
  # Many of the things being done below are via reaching deep into the Association objects
  # on the ActiveRecord objects themselves.  Because of this, it is very important
  # that your calling code use the same instance of the objects in memory.
  #
  # Returns the array of PullRequestReviews we have been manipulating.
  def execute
    load_replies

    load_comments_on_reviews

    reviews
  end

  private

  attr_reader :reviews, :comments

  # Load all `replies` associations onto comments, and also load the inverse
  # side of `in_reply_to`
  def load_replies
    replies, parents = comments.partition { |c| c.reply? }
    # we know that a reply cannot have replies itself - so preload with empty array
    replies.each { |reply| load_has_many(reply, association: :replies, records: []) }

    parents_by_id = parents.index_by(&:id)

    replies.group_by(&:reply_to_id).each do |reply_to_id, replies|
      parent = parents_by_id.delete(reply_to_id)
      if parent # guard on the parent, as it may have been deleted
        load_has_many(parent, association: :replies, records: replies)
      end
    end
    parents_by_id.each_value do |comment_without_reply|
      load_has_many(comment_without_reply, association: :replies, records: [])
    end
  end

  # Loads the has_many relationship between PullRequestReview <-> PullRequestReviewComment
  def load_comments_on_reviews
    comments_by_review_id = comments.group_by(&:pull_request_review_id)
    reviews.each do |review|
      load_has_many(review, association: :review_comments, records: comments_by_review_id[review.id])
    end
  end

  # Load a has_many association and tell ActiveRecord its "loaded!" so it doesn't go and try
  # to load it again.  Also attempts to set the inverse side of the relationship,
  # which depends on relationships being declared with `:inverse_of` -- note that
  # many of ours do not!
  def load_has_many(model, association:, records:)
    association = model.association(association)
    association.loaded!
    if records && records.any?
      association.target.concat(records)
      records.each { |r| association.set_inverse_instance(r) }
    end
  end

end
