# rubocop:disable Style/FrozenStringLiteralComment

# A Coupon can be redeemed by a User to get a discount on their
# monthly plan. Once applied, a CouponRedemption record is created to keep
# track of who redeemed what.
class Coupon < ApplicationRecord::Domain::Users
  areas_of_responsibility :gitcoin

  include Coupon::ZuoraDependency

  # If you change this, don't forget to change the regexp in routes.rb
  FORMAT_CHARACTERS = 'a-z0-9\._\-+:*!&<>()^'

  validates_uniqueness_of   :code, case_sensitive: true
  validates_presence_of     :code
  validates_format_of       :code, with: /\A[#{FORMAT_CHARACTERS}]+\z/i
  validates_presence_of     :note
  validates_presence_of     :discount
  validates_numericality_of :discount
  validates_presence_of     :duration
  validates_presence_of     :limit
  validates_presence_of     :expires_at

  STUDENT_DEVELOPER_PACK_NAME = "students-"
  NON_PROFIT_CODE = "notsoprofitable"
  EDUCATION_GROUP_NAMES = %w(education-individual education-org education-promo)
  SALES_SERVE_GROUP_NAME = "sales-serve"
  DREAMFORCE_2017_GROUP_NAME = "dreamforce-2017"
  SELF_SERVE_BUSINESS_PLUS_GROUP_NAMES = ["microsoft", "startup-program"]
  BUSINESS_PLUS_ONLY_GROUP_NAMES = [SALES_SERVE_GROUP_NAME,  DREAMFORCE_2017_GROUP_NAME]
  # The possible values for the group field.
  GROUP_NAMES = (EDUCATION_GROUP_NAMES + SELF_SERVE_BUSINESS_PLUS_GROUP_NAMES + %w(
    boxdev2014
    cisco-devnet
    cloneageddon
    community
    diversity
    enterprise
    expired
    facebook-fbstart
    fbstart2016
    gifts
    golden-ticket-experiment
    hackathons
    ibm-devworks
    incubators
    internal
    prepaid
    prizes
    sales
    security
    si-bulk
    support)).sort
  validates_inclusion_of :group, in: (BUSINESS_PLUS_ONLY_GROUP_NAMES + GROUP_NAMES)

  SALES_SERVE_DURATION = 30
  DREAMFORCE_2017_DURATION = 180

  has_many :coupon_redemptions, dependent: :destroy
  has_many :users, through: :coupon_redemptions

  scope :multi_use,          -> { where("`coupons`.`limit` > 1") }
  scope :active,             -> { where("expires_at IS NULL OR expires_at > ?", GitHub::Billing.now) }
  scope :education,          -> { where(group: EDUCATION_GROUP_NAMES) }
  scope :business_plus_only, -> { where(group: BUSINESS_PLUS_ONLY_GROUP_NAMES) }

  # Finds a coupon from a code. If passed a Coupon, returns it.
  #
  # code - A String code or Coupon object
  #
  # Returns a Coupon if one is found.
  # Returns nil if nothing can be found.
  def self.find_by_code(code)  # rubocop:disable GitHub/FindByDef
    return nil if code.blank?
    code.is_a?(Coupon) ? code : where(code: code).first
  end

  # Cleans the code of an old coupon that may have an invalid code by
  # replacing invalid characters with '-'. See #16950.
  #
  # code - A String code
  #
  # Returns a String code
  def self.clean_old_code(code)
    code.gsub(/[^#{FORMAT_CHARACTERS}]/i, "-") if code
  end

  # Determine that the code is in a valid format.
  #
  # code - a String code
  #
  # Returns truthy if the code is in a valid format
  def self.valid_code?(code)
    code.match(/\A[#{FORMAT_CHARACTERS}]+\z/i) if code
  end

  # Returns the number of redemptions for this coupon.
  # The count will query the database only until the collection is loaded
  #
  # Returns Integer representing the current coupon redemption count
  def redeemed_count
    coupon_redemptions.size
  end

  def clean_code!
    self.update_attribute(:code, Coupon.clean_old_code(code))
  end

  # Every coupon has a code which can be used to redeem it.
  # If no code is provided at the time of creation, one will be provided
  # for you.
  #
  # Returns a String
  def code
    self[:code] ||= SecureRandom.hex(4)[0, 7]
  end
  alias_method :to_s, :code
  alias_method :to_param, :code

  # Is this coupon for a free trial? Free trials allow a user to
  # use a paid plan without entering their CC#.
  #
  # Aliased as one_hundred_percent_discount? for different use cases than
  # just trial? checking.
  #
  # Returns a Boolean
  def trial?
    !!(discount == 1.0)
  end
  alias :one_hundred_percent_discount? :trial?

  def percentage?
    discount <= 1.0
  end

  # Returns true if this coupon is used for non-profit organizations.
  def non_profit?
    code == NON_PROFIT_CODE
  end

  # The coupons's lifespan in days. Defaults to 31
  #
  # Returns an Integer representing the duration in days.
  def duration
    self[:duration] ||= 31
  end

  # How many times this coupon may be redeemed. Defaults to 1
  #
  # Returns an Integer
  def limit
    self[:limit] ||= 1
  end

  # When this coupon expires. Defaults to 1 year from today
  #
  # Returns a TimeWithZone
  def expires_at
    self[:expires_at] ||= GitHub::Billing.now + 1.year
    self[:expires_at].in_billing_timezone
  end

  # Public: Setter for this Coupon's plan
  #
  # plan - The plan as either a GitHub::Plan or the plan's name as a String
  #
  # Returns the success or failure of the attribute write as a Boolean
  def plan=(new_plan)
    write_attribute(:plan, new_plan.to_s)
  end

  # Public: Getter for this Coupon's plan.
  #
  # Returns the Plan that this coupon is on or nil
  def plan
    GitHub::Plan.find(read_attribute(:plan))
  end

  # Allow discounts to be set using strings such as "$12" or "50%"
  # by converting it to number as soon as it's set.
  #
  # discount - A String or Number representing the discount this coupon
  #            grants.
  #
  # Returns nothing.
  def discount=(discount)
    if !discount.is_a? String
      return self[:discount] = discount
    end

    if discount.include? "%"
      super discount.to_f / 100
    elsif discount.include? "$"
      super discount.gsub(/[^\d.]/, "").to_f
    elsif !discount.empty?
      super discount.to_f
    end
  end

  # A pretty string we can use to represent this discount,
  # either as a % value or a set dollar amount.
  #
  # Returns a String
  def human_discount
    return unless discount
    if discount > 1
      "$%.02f" % discount
    else
      "#{discount_in_cents.to_i}%"
    end
  end

  def discount_in_cents
    discount * 100
  end

  # The display name of plan specific coupon.
  #
  # Returns a String or nil
  def plan_display_name
    plan && plan.display_name.humanize
  end

  # Durations can be any number of days, but not all
  # durations are created equal. Here are some suggestions.
  FUN_DURATIONS = {
    30    => "30-day",
    31    => "1 month",
    60    => "2 months",
    90    => "3 months",
    120   => "4 months",
    150   => "5 months",
    180   => "6 months",
    210   => "7 months",
    240   => "8 months",
    270   => "9 months",
    300   => "10 months",
    330   => "11 months",
    365   => "1 year",
    730   => "2 years",
    29970 => "Forever",
  }

  # Need our FUN_DURATIONS nice and neat? This'll give 'em to you in
  # order of least (one day) to more (1 year).
  #
  # Returns an Array of two element Arrays
  def fun_durations
    FUN_DURATIONS.to_a.sort.map { |i| i.reverse }
  end

  # Is the current duration fun? Does it appear in FUN_DURATIONS?
  #
  # Returns a Boolean
  def fun_duration?
    !!FUN_DURATIONS[duration]
  end

  # The duration as something appropriate for human consumption.
  #
  # Returns a String
  def human_duration
    FUN_DURATIONS[duration] ||
      (duration > 30 ? "#{duration / 30} months" : "#{duration} days")
  end

  # Will this coupon ever expire?
  #
  # Returns a Boolean
  def will_expire?
    duration < 29970
  end

  # Can the actor perform the redemption? This doesn't guarantee that the coupon
  # can be applied to a given user, just that the redemption can be perfomed
  # by the actor.
  #
  # actor - User performing redemption
  #
  # Returns a Boolean
  def redeemable_by?(actor)
    if staff_actor_only?
      !!actor.try(:site_admin?) || !!actor.try(:biztools_user?)
    else
      true
    end
  end

  # Has this coupon expired?
  #
  # Returns a Boolean
  def expired?
    !expires_at.nil? && expires_at < GitHub::Billing.now
  end

  # The expiration date as something appropriate for human consumption.
  #
  # Returns a String
  def human_expires_at
    return "never" if expires_at.nil?
    return "expired" if expired?
    expires_at.to_date
  end

  # The expiration date as something appropriate for human editing.
  #
  # Returns a String
  def form_expires_at
    expires_at.to_date.iso8601 unless expires_at.nil?
  end

  # Whether this coupon can be applied to user accounts.
  #
  # Returns a Boolean
  def user_coupon?
    !plan || plan.user_plan?
  end

  # Whether this coupon can be applied to org accounts.
  #
  # Returns a Boolean
  def org_coupon?
    !plan || plan.org_plan?
  end

  # Public: Returns true if this coupon is an education coupon
  #
  # Returns a Boolean
  def education_coupon?
    EDUCATION_GROUP_NAMES.include?(group)
  end

  # Public: Returns true if this coupon is a sales-serve coupon
  #
  # Returns a Boolean
  def business_plus_only_coupon?
    BUSINESS_PLUS_ONLY_GROUP_NAMES.include?(group)
  end

  def dreamforce_2017_coupon?
    DREAMFORCE_2017_GROUP_NAME == group
  end

  def sales_serve_coupon?
    SALES_SERVE_GROUP_NAME == group
  end

  def self_serve_business_plus_coupon?
    SELF_SERVE_BUSINESS_PLUS_GROUP_NAMES.include?(group)
  end

  # Public: Whether this coupon is part of the golden ticket experiment
  #
  # Returns a Boolean
  def golden_ticket?
    group == "golden-ticket-experiment"
  end

  # Whether this coupon can only be applied to user accounts.
  #
  # Returns a Boolean
  def user_only?
    user_coupon? && !org_coupon?
  end

  # Whether this coupon can only be applied to org accounts.
  #
  # Returns a Boolean
  def org_only?
    !user_coupon? && org_coupon?
  end

  # Public: Is this coupon tied to a specific plan?
  #
  # Returns a Boolean
  def plan_specific?
    plan.present?
  end

  # Whether this coupon can be applied to both user and org accounts.
  #
  # Returns a Boolean
  def all_accounts?
    user_coupon? && org_coupon?
  end

  # Can this coupon be used with the target plan
  #
  # Returns a Boolean
  def applicable_to?(target_plan)
    !plan_specific? || (plan && target_plan == plan)
  end

  # User's accounts that are eligible for the sales-serve coupon group
  #
  # user - returns this user and/or their owned orgs
  # group - a coupon group in BUSINESS_PLUS_ONLY_GROUP_NAMES
  #
  # Returns an array of users
  def business_plus_only_eligible_accounts(user)
    user.admin_or_manager_organizations.reject do |org|
      case self.group
      when SALES_SERVE_GROUP_NAME
        org.invoiced? || org.business_plus? || !org.per_seat_plan_only? || org.has_an_active_coupon?
      else
        true
      end
    end
  end

  # User's accounts that are eligible for this coupon.
  #
  # user - returns this user and/or their owned orgs
  #
  # Returns an array of users
  def eligible_accounts(user)
    return business_plus_only_eligible_accounts(user) if business_plus_only_coupon?

    if plan.present?
      if plan.org_plan? || (self_serve_business_plus_coupon? && plan.hidden_org_plan?)
        user.owned_organizations.reject(&:invoiced?)
      else
        [user]
      end
    else
      [user] + user.owned_organizations.reject do |org|
        org.invoiced? || org.business_plus?
      end
    end
  end
end
