# frozen_string_literal: true

class EphemeralNotice < ApplicationRecord::Collab
  areas_of_responsibility :ce_extensibility

  include GitHub::UTF8
  include GitHub::UserContent
  include GitHub::Validations
  include GitHub::RateLimitedCreation
  include GitHub::Relay::GlobalIdentification

  extend GitHub::Encoding
  force_utf8_encoding :link_title, :notice_text

  belongs_to :parent, polymorphic: true
  belongs_to :user

  delegate :notify_socket_subscribers, to: :parent
  after_commit :notify_socket_subscribers, on: [:create, :destroy]

  validates :notice_text, length: { maximum: 140 }, presence: true
  validates :link_title, length: { maximum: 25 }
  validate  :validate_link_presence

  validates_format_of :link_to, with: /\Ahttps:\/\//, message: "must be an HTTPS url", allow_blank: true
  enum notice_type: { default: 0, warning: 1, success: 2, error: 3 }

  def validate_link_presence
    return if link_to.blank? && link_title.blank?
    return if link_title.present? && link_to.present?
    errors.add(:link_title, "must not be blank") if link_title.blank?
    errors.add(:link_to, "must not be blank") if link_to.blank?
  end

  def expired?
    created_at < 15.minutes.ago
  end
end
