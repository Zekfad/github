# frozen_string_literal: true

GitbackupsSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::GitbackupsScheduler)
