# frozen_string_literal: true

require "active_job"
require "active_job/locking_job"
require "active_job/scheduled_job"

require "github/areas_of_responsibility"
require "github/service_mapping"
require "github/throttler"

class ApplicationJob < ActiveJob::Base
  module LegacyToggle
    def perform(*args)
      if legacy_class
        if GitHub.flipper["unwrapped-aj-#{self.class.name}"].enabled?
          super
        else
          legacy_class.new.perform(*args)
        end
      else
        super
      end
    end
  end

  class InvalidClusterError < StandardError; end

  include GitHub::AreasOfResponsibility
  include GitHub::ServiceMapping
  include ActiveJob::LockingJob
  include ActiveJob::ScheduledJob

  DEFAULT_LOCK_KEY = "key"

  # This is copied from lib/github/hash_lock to match our existing locks. The only
  # modification here is the support for ApplicationRecord::Base objects
  DEFAULT_LOCK_PROC = proc do |job|
    if job.arguments.empty?
      DEFAULT_LOCK_KEY
    else
      job.arguments.map do |obj|
        case obj
        when ApplicationRecord::Base
          "#{obj.class.name}#{obj.id}"
        when Hash
          sorted = obj.to_a.sort_by { |x| x.first.to_s }.join(",")
          "{#{sorted}}"
        when Array
          "[#{obj.join(",")}]"
        else
          obj
        end
      end.join(" ")
    end
  end

  # These queue names aren't set in job classes but are referred to explicitly
  # during enqueue.
  ALLOWED_QUEUE_NAMES = %w[
    team_member_added_emails
    signup_emails
    spam
    high
    dgit_repairs
    pages-docker
    test
  ] # WARNING: make sure these queues are added to /config/resqued/*

  before_enqueue do
    klass_queue_name = self.class.queue_name
    klass_queue_name = instance_exec(&klass_queue_name) if klass_queue_name.is_a?(Proc)
    next if queue_name == klass_queue_name
    # indexing jobs switch between different queues:
    next if queue_name.start_with?("index_") && klass_queue_name.start_with?("index_")
    next if queue_name.start_with?("maint") # dgit-related jobs
    next if ALLOWED_QUEUE_NAMES.include?(queue_name)
    err = ArgumentError.new "queue: '#{queue_name}' is not recognized"
    if GitHub.raise_on_unrecognized_queue?
      raise err
    else
      Failbot.report(err, {
        app: "github-legacy-jobs",
        job_class: self.class,
      })
    end
  end

  around_enqueue do |job, block|
    if GitHub.respond_to?(:flipper) &&
      GitHub.flipper[:active_job_skip_enqueue].enabled?(job.job_class_actor)
      GitHub.dogstats.increment("active_job.enqueue_skipped", tags: job.all_stats_tags)
      next
    end

    block.call
  rescue => e
    instrument_error!(e) unless handler_for_rescue(e)
    raise
  end

  around_perform do |job, block|
    # Call this with the connected to wrapper to enable the Rails query cache inside the job.
    ActiveRecord::Base.connected_to(role: :writing) do
      with_logging_contexts(&block)
    end
  rescue => e
    instrument_error!(e) unless handler_for_rescue(e)
    raise
  end

  # Explicitly specify if a job should be enqueued to hydro. Used to bypass
  # Flipper checks in environments that don't load Flipper.
  attr_accessor :enqueue_to_hydro

  # The backend used to enqueue the job. Purely informational and only used in metrics.
  attr_accessor :enqueue_backend

  # Support Job.set(enqueue_to_hydro: true).perform_later
  # :deserialize_arguments can be used to force the job to deserialize
  #     it's arguments before enqueueing.
  def enqueue(options = {})
    if options.key?(:enqueue_to_hydro)
      self.enqueue_to_hydro = options[:enqueue_to_hydro]
    end

    super
  end

  def self.perform_enqueued_jobs?
    # internal way ActiveJob's :test adapter knows to perform_now
    # (rails's RockQueue.inline {})
    queue_adapter.respond_to?(:perform_enqueued_jobs) && queue_adapter.perform_enqueued_jobs
  end

  # Public: Enqueue this job to run only _once_ at the end of the specified
  # interval
  #
  # I.e. if you enqueue 10 jobs with the same `unique_id` at the same
  # time, only _the first_ job will be ran at the end of the interval
  #
  # This is useful to throttle jobs that can be "abused" by user actions;
  # if an user performs 10 pushes in 10 seconds, and there is an expensive
  # operation we want to perform "on push", using this helper method will
  # ensure that the operation only runs once.
  #
  # The interval begins when the first unique job--identified by an optional
  # unique id or its arguments--is queued. Part of a job's identifier is the
  # size of the interval, not a specific rounded deterministic time value.
  # This means if a job is supposed to have been re-queued but hasn't
  # yet, this method will prevent a new copy of the job from being added even
  # though the previous time window has passed.
  #
  # args      - arguments for the job
  # interval  - interval in Seconds during which we'll deduplicate the job;
  #             defaults to the current minute; integer number of seconds
  # unique_id - value that uniquely identifies this job for "deduplication"
  #             i.e. to prevent the same job from running twice in the interval.
  #             if missing, we will deduplicate based on the arguments for the job
  #
  # Returns true if job was enqueued, false if not.
  def self.enqueue_once_per_interval(args, interval: 60, unique_id: nil)
    unique_id ||= args.join(":")
    tags = ["class:#{self.name.underscore}"]
    cache_key = "job:once_per_interval:#{interval}:#{unique_id}"
    if GitHub.kv.exists(cache_key).value { false }
      GitHub.dogstats.increment("job.once_per_interval.duplicate", tags: tags)
      false
    else
      ttl = interval.seconds
      now = Time.now
      GitHub.dogstats.increment("job.once_per_interval.queued", tags: tags)
      GitHub.kv.set(cache_key, now.iso8601, expires: ttl.from_now)
      set(wait_until: now + ttl).perform_later(*args)
      true
    end
  end

  # Catch the exception and reschedule job for re-execution after so many seconds, for a specific number of attempts.
  # If the exception keeps getting raised beyond the specified number of attempts, the exception is allowed to
  # bubble up to the underlying queuing system, which may have its own retry mechanism or place it in a
  # holding queue for inspection.
  #
  # You can also pass a block that'll be invoked if the retry attempts fail for custom logic rather than letting
  # the exception bubble up. This block is yielded with the job instance as the first and the error instance as the second parameter.
  def self.retry_on(*exceptions, wait: 3.seconds, attempts: 5)
    rescue_from *exceptions do |error|
      execution_count = executions_for(exceptions)

      if execution_count < attempts
        logger.error "Retrying #{self.class} in #{wait} seconds, due to a #{error.class}. The original exception was #{error.cause.inspect}."
        retry_job wait: determine_delay(seconds_or_duration_or_algorithm: wait, executions: execution_count), error: error
      else
        if block_given?
          instrument_job(:retry_stopped, error: error) do
            yield self, error
          end
        else
          instrument_job(:retry_stopped, error: error)
          logger.error "Stopped retrying #{self.class} due to a #{error.class}, which reoccurred on #{executions} attempts. The original exception was #{error.cause.inspect}."
          raise error
        end
      end
    end
  end

  # Helper for jobs that are confirmed idempotent and safe to retry on worker shutdown
  def self.retry_on_dirty_exit
    if defined? Aqueduct::Worker::Worker::DirtyExit
      retry_on Resque::DirtyExit, Aqueduct::Worker::Worker::DirtyExit
    else
      retry_on Resque::DirtyExit
    end
  end

  def self.redeliver_after(timeout)
    @redelivery_timeout = timeout
  end

  def self.redelivery_timeout
    @redelivery_timeout
  end

  def redelivery_timeout
    self.class.redelivery_timeout
  end

  # This excludes the original delivery. Setting this to 2 will allow Aqueduct to deliver a job up to 3 times in total.
  # This is because the first delivery doesn't count as a re-delivery.
  #
  # This setting only applies to jobs that are either lost while in transit to the worker or jobs don't finish
  # executing gracefully (Due to a hard shutdown for instance). This timeout does not apply to jobs that throw an
  # uncaught exception.
  #
  # Ordinarily, there should be no need to change this setting.
  def self.set_max_redelivery_attempts(attempts)
    @max_redelivery_attempts = attempts
  end

  def self.max_redelivery_attempts
    @max_redelivery_attempts
  end

  def max_redelivery_attempts
    self.class.max_redelivery_attempts
  end

  # Automatically retry when throttler errors are encountered.
  retry_on Freno::Throttler::Error, wait: :exponentially_longer, attempts: 10

  def all_stats_tags(error: $!)
    [
      "catalog_service:#{logical_service}",
      "class:#{self.class.name.underscore}",
      "queue:#{queue_name}",
      "adapter:#{self.class.queue_adapter_name}",
      error ? "error:#{error.class.to_s.underscore}" : nil, # to_s accounts for anonymous classes
      ("backend:#{enqueue_backend}" if enqueue_backend),
    ].concat(stats_tags).compact
  end

  # For use with feature flags
  def self.job_class_actor
    ActiveJob::JobClassActor.new(self)
  end

  # Provide a way to switch between LegacyApplicationJob wrapped jobs
  # and the updated non-wrapped jobs
  class_attribute :legacy_class

  def job_class_actor
    self.class.job_class_actor
  end

  protected

  # Override this in your job if you'd like to include custom tags
  def stats_tags
    []
  end

  # Override this in your job to add the provided data to the logging context
  def logging_context
    { arguments: arguments.inspect }
  end

  # Override this in your job to add the provided data to the failbot context
  # NOTE: arguments are not allowed keys so their potentially sensitive data
  # should not be at risk on dotcom.
  def failbot_context
    { arguments: arguments.inspect }
  end

  private

  def instrument_job(name, error: nil)
    GitHub.instrument("#{name}.active_job", {job: self, error: error}.compact) do |*args|
      yield(*args) if block_given?
    end
  end

  def instrument_error!(error)
    instrument_job("error", error: error) do |payload|
      raise # allow ActiveSupport::Notifications to add exception to payload
    end
  end

  def record_error(error)
    GitHub.dogstats.increment("active_job.error", tags: all_stats_tags(error: error))
  end

  def with_logging_contexts(&block)
    # The important stuff, this cann't be overridden. The job is set in the worker but there
    # should be no harm setting it here as well.
    base_log_context = { job: self.class.name }

    # Just push our contexts without cleanup. Popping a context could easily result in
    # the wrong data being popped. This way we leak our data like everyone else and
    # rely on the job running to reset things.
    Failbot.push(base_log_context.reverse_merge(failbot_context))

    push_service_mapping_context do
      # Logging contexts are required to be scoped and only apply within that scope.
      GitHub::Logger.log_context(base_log_context.reverse_merge(logging_context), &block)
    end
  end

end
