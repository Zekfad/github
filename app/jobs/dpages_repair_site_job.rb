# frozen_string_literal: true

DpagesRepairSiteJob = LegacyApplicationJob.wrap(GitHub::Jobs::DpagesRepairSite)
