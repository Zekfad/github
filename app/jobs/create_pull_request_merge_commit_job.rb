# frozen_string_literal: true

# This job creates the latest merge commit for the given pull request. As this
# is needed in order to refresh the merge box or get the latest merge related
# attributes for a pull request via the APIs, it can have fairly heavy traffic.
# This is especially true in the case that a pull request is merged to a common
# base branch, causing the merge commits for all other pull requests in the
# repository to become out of date.
#
# ## Job Restrictions

# To manage the stampede of merge commit creations caused by such a merge, we have
# careful and thorough rate limiting in place at multiple levels so that the
# number of simultaneous merge commit updates to a single repository is kept to
# minimum.
#
# ### 1. Job Locking
#
# We lock the job based on a custom key consisting of the pull request id,
# base oid, and head oid at the time of enqueue. This ensures that numerous
# requests to a single pull request don't flood the queue with identical jobs
# all trying to do the same thing.
#
# ### 2. Resource Locking
#
# We utilize GitHub::Restraint to limit the number of concurrent jobs on a single
# repository. This helps busy monorepos with many developers working simultaneously,
# or with bots responding to web hooks. With this limit, some jobs may be delayed
# but we shouldn't have so many that other jobs or user pushes are rejected.
#
# ### 3. Retries
#
# When the resource is locked, or the git repo is just too busy, we retry the job
# a limited number of times with a delay. If all the attempts are spent, the pull
# request will remain in an undefined mergeable state until the user re-initiates
# a merge commit calculation request.

class CreatePullRequestMergeCommitJob < ApplicationJob
  set_max_redelivery_attempts 0

  queue_as :create_pull_request_merge_commit

  RepoRepairingError = Class.new(StandardError)

  # The maximum number of CreatePullRequestMergeCommit that are
  # allowed to run for a single repository at one time.
  MAX_CONCURRENT = 10

  # The lifespan of the rate limit count. If not queried within this span, we restart
  # the rate limit count from 0 concurrent processes.
  # This should be kept at a value greater than the wait for the `UnableToLock` retry
  # plus the max sum of the time for all the git operations inside the
  # PullRequest#create_merge_commit method. As we don't have a known firm value for this,
  # we will go with something we believe should be large enough in most cases.
  RATE_LIMITER_TTL = 30.seconds

  # Max number of attempts and wait time for all retryable failures
  MAX_ATTEMPTS = 20

  # Wait a random number of seconds within the given range
  RETRY_WAIT_RANGE = 6..12
  RETRY_PROC = proc { rand(RETRY_WAIT_RANGE) }

  # Trivially discard any attempts to enqueue a job when the pull request's commits
  # haven't changed since a previously enqueued job.
  locked_by key: -> (job) { job.commit_lock_key }, timeout: 5.minutes

  # the error or group of errors will have a unique retry count
  RETRYABLE_ERRORS = [
    RepoRepairingError,
    GitHub::Restraint::UnableToLock,
    [GitHub::DGit::ThreepcBusyError, GitHub::DGit::ThreepcFailedToLock]
  ].freeze

  # declare a separate `retry_on` for each error so their counts don't stack
  RETRYABLE_ERRORS.each do |error_class|
    retry_on *Array(error_class), wait: RETRY_PROC, attempts: MAX_ATTEMPTS do |job, error|
      # no need for extra details here as they should have been pushed to the log context during `perform`
      GitHub::Logger.log(error_class: error.class.name, error_message: error.message)
      raise error
    end
  end

  # The pull request is gone, we don't care about this merge commit any more.
  discard_on ActiveRecord::RecordNotFound

  def perform(pull_request_id)
    raise RepoRepairingError if pull_request.repository.repairing?

    with_restraint(pull_request.repository) do
      pull_request.create_merge_commit(priority: :low)
    end
  end

  def pull_request
    return @pull_request if defined?(@pull_request)
    pull_id = arguments.first
    @pull_request = PullRequest.find(pull_id)
  end

  # odd name to avoid conflicting with ActiveJob::LockingJob#lock_key
  def commit_lock_key
    # TODO: our association tracker is forcing bad patterns. This is only needed
    # because the job can be enqueued as a side effect of a graphql query which
    # disallows direct association loads, so we do this async dance first to work
    # around it.
    promises = [pull_request.async_repository, pull_request.async_base_user, pull_request.async_head_user]
    key_promise = Promise.all(promises).then do
      base_oid = pull_request.current_base_oid
      head_oid = pull_request.current_head_oid
      "#{pull_request.id}:#{base_oid}:#{head_oid}"
    end
    key_promise.sync
  rescue ActiveRecord::RecordNotFound
    "nil:nil:nil"
  end

  def failbot_context
    {
      repo_id: pull_request.repository_id,
      pull_request_id: pull_request.id,
      base_oid: pull_request.mergeable_base_sha,
      head_oid: pull_request.mergeable_head_sha,
    }
  end

  def logging_context
    failbot_context.merge(spec: pull_request.repository&.dgit_spec)
  end

  def with_restraint(repository)
    lock_key = "CreatePullRequestMergeCommit-#{repository.id}"

    restraint = GitHub::Restraint.new

    max_concurrent = MAX_CONCURRENT

    if GitHub.flipper[:cprmc_lower_concurrency].enabled?(repository)
      max_concurrent = 5
    end

    restraint.lock!(lock_key, max_concurrent, RATE_LIMITER_TTL) do
      yield
    end
  end
end
