# frozen_string_literal: true

SpokesDestroyGistReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitDestroyGistReplica)
