# frozen_string_literal: true

# Updates security and analysis settings for every org/user repo
class SecurityAnalysisSettingsUpdateJob < ApplicationJob

  areas_of_responsibility :token_scanning
  queue_as :security_analysis_settings
  locked_by timeout: 30.seconds, key: DEFAULT_LOCK_PROC
  retry_on_dirty_exit

  # Public: Updates security and analysis settings for every org/user repo
  #
  # owner                   - The organization/user to perform security and analysis settings updates on.
  # update_type             - The type of security and analysis settings being updated.
  # actor_id                - Id of the actor that initiated the job
  # last_processed_repo_id  - The last processed repository in the given job run
  # batch_size              - The number of repositories to read from the database per batch.
  # duration                - The amount of time in seconds that each job instance can run
  #                           for. After this time is elapsed, if there are still more
  #                           batches to process, another job is enqueued to finish the work,
  #                           and the current job terminates.
  def perform(owner, update_type, actor_id, last_processed_repo_id: 0, batch_size: 100, duration: 60)
    end_at = Time.current + duration
    update_private_repos_only = false

    params = {}
    case update_type
    when :dependency_graph_enable_all
      params[:dependency_graph_enabled] = "1"
      update_private_repos_only = true
    when :dependency_graph_disable_all
      params[:dependency_graph_enabled] = "0"
      update_private_repos_only = true
    when :security_alerts_enable_all
      params[:vulnerability_alerts_enabled] = "1"
    when :security_alerts_disable_all
      params[:vulnerability_alerts_enabled] = "0"
    when :vulnerability_updates_enable_all
      params[:vulnerability_updates_enabled] = "1"
    when :vulnerability_updates_disable_all
      params[:vulnerability_updates_enabled] = "0"
    when :secret_scanning_enable_all
      params[:token_scanning_enabled] = "1"
      update_private_repos_only = true
    when :secret_scanning_disable_all
      params[:token_scanning_enabled] = "0"
      update_private_repos_only = true
    end

    actor = User.find(actor_id)
    repos = Repository.where(owner_id: owner.id)
    repos = repos.private_scope if update_private_repos_only

    loop do
      repo_batch = repos.where("id > ?", last_processed_repo_id)
                    .order(:id)
                    .limit(batch_size)

      repo_batch.each do |repo|
        repo.manage_security_settings(params, actor: actor)
      end

      break if repo_batch.empty?

      last_processed_repo_id = repo_batch.last.id

      if Time.current >= end_at
        SecurityAnalysisSettingsUpdateJob.perform_later(
          owner,
          update_type,
          actor_id,
          last_processed_repo_id: last_processed_repo_id,
          batch_size: batch_size,
          duration: duration
        )

        break
      end
    end

    true
  end
end
