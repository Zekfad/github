# frozen_string_literal: true

DpagesEvacuationSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::DpagesEvacuationScheduler)
