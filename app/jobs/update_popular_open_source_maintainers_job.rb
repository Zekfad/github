# frozen_string_literal: true

UpdatePopularOpenSourceMaintainersJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdatePopularOpenSourceMaintainers)
