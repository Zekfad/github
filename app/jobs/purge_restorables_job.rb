# frozen_string_literal: true

PurgeRestorablesJob = LegacyApplicationJob.wrap(GitHub::Jobs::PurgeRestorables)
