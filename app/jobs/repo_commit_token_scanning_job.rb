# frozen_string_literal: true

class RepoCommitTokenScanningJob < TokenScanningJob
  areas_of_responsibility :token_scanning
  queue_as :token_scanning

  def job_scope
    "commit"
  end

  def job_visibility
    "public"
  end

  # Scan the blobs modified by the commit for tokens.
  def scan_for_tokens
    chain = GitHub::TokenScanning::ChainProcessor.new(git_repo, actor)

    # The token scan can take a long time if a large number of objects need
    # to be scanned.
    ::GitRPC.with_timeout(15.minutes) do
      new_tips = refs.map { |_, new_tip, _| new_tip }
      # We don't want to include deleted references since the GitRPC call
      # would fail.
      new_tips.select! { |new_tip| new_tip != GitHub::NULL_OID }
      add_blob_locations(token_infos, new_tips)
      token_infos.each do |token_info|
        chain << token_info
      end
    end

    chain.process
    chain.report_stats(stats_tags)
    "processed"
  end

  # Populate token_info hashes using which ever tokens scanning approach
  # is enabled for this repository.
  #
  # Returns an Array of token_info hashes or Raises GitRPC::CommandFailed.
  def token_infos
    @token_infos ||= find_creds(refs: refs, dedup_results: true)
  end
end
