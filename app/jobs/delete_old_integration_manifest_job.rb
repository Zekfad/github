# frozen_string_literal: true

class DeleteOldIntegrationManifestJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :integration_manifests

  schedule interval: 1.hour

  def perform
    IntegrationManifest.where("created_at < ?", 1.hour.ago).find_in_batches do |group|
      IntegrationManifest.throttle do
        group.each(&:destroy)
      end
    end
  end
end
