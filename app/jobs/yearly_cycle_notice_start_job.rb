# frozen_string_literal: true

class YearlyCycleNoticeStartJob < ApplicationJob
  areas_of_responsibility :gitcoin

  queue_as :billing

  schedule interval: 24.hours, condition: -> { !GitHub.enterprise? }

  def perform
    ::Billing::YearlyCycleNotice.run
  end
end
