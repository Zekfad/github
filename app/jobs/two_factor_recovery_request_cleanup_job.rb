# frozen_string_literal: true

class TwoFactorRecoveryRequestCleanupJob < ApplicationJob
  queue_as :two_factor_recovery

  # Amount of time to wait before sending Zendesk ticket for verification
  # This is a default time decided upon by prodsec to prevent account takeovers
  EXPIRY_TIME = 2.weeks

  before_enqueue do |job|
    # Create a JobStatus for the job being enqueued.
    if job.arguments.first
      request = job.arguments.first[:request]
      JobStatus.create(id: TwoFactorRecoveryRequestCleanupJob.job_id(request.id))
    end
  end

  def self.job_id(request_id)
    "two_factor_recovery_cleanup_#{request_id}"
  end

  def self.status(request_id)
    JobStatus.find(TwoFactorRecoveryRequestCleanupJob.job_id(request_id))
  end

  schedule interval: 1.hour, condition: -> { !GitHub.enterprise? }

  def perform(request: nil)
    return request.destroy! if request.present?

    expired_request_ids = TwoFactorRecoveryRequest.where(["updated_at <= ?", EXPIRY_TIME.ago]).pluck(:id)
    expired_token_request_ids = TwoFactorRecoveryRequest.where(["reviewer_id IS NOT NULL and approved_at <= ?", TwoFactorRecoveryRequest::COMPLETE_TOKEN_EXPIRY.ago]).pluck(:id)
    recovery_request_ids = (expired_request_ids + expired_token_request_ids).uniq
    # Avoid collisions with other requests that are queued for deletion
    recovery_request_ids.delete_if { |id| TwoFactorRecoveryRequestCleanupJob.status(id).present? }
    unless recovery_request_ids.empty?
      TwoFactorRecoveryRequest.destroy(recovery_request_ids)
    end
  end
end
