# frozen_string_literal: true

class ProcessEventJob < ApplicationJob
  extend Forwardable

  areas_of_responsibility :news_feeds

  queue_as :event
  set_max_redelivery_attempts 0

  # This exception means the repo the event was for has since been deleted.
  # There's nothing this job can do when that happens.
  discard_on GitRPC::InvalidRepository

  # override stats_tags to include stratocaster tags
  def_delegator :dogstats_tags, :all, :stats_tags

  before_perform prepend: true do
    dogstats_tags.event_type = arguments.first

    Failbot.push(
      action: "strat",
      event_type: arguments[0],
      event_args: arguments[1],
    )
  end

  # Triggers the actual event.
  #
  # event_type: string (e.g. 'IssueEvent')
  # args: array of args from the event
  #
  # See Stratocaster::Service#queue.
  #
  def perform(event_type, args)
    GitHub::Logger.log(stratocaster_event_type: event_type)

    dispatcher = GitHub.stratocaster.dispatcher(event_type, *args)
    event = Stratocaster::Model.throttle { dispatcher&.perform }

    return if event.blank?

    dogstats_tags.event = event
    GitHub.dogstats.increment("stratocaster.dispatched_event", tags: stats_tags)

    SubmitEventToOctolyticsJob.perform_later(event.id) if GitHub.octolytics_enabled?
    UpdateEventFeedsJob.perform_later(event) if dispatcher.perform_fanout?

    @success = true
  ensure
    record_mysql_metrics(mysql_tags: stats_tags)
    dogstats_tags.success = !!@success
  end

  private

  def record_mysql_metrics(mysql_tags:)
    GitHub::MysqlQueryCounter.counts.each do |db_host, counts|
      counts ||= {}
      host_tag = "rpc_host:#{db_host}"
      tags = mysql_tags + [host_tag]
      GitHub.dogstats.count(
        "job.process_event.rpc.mysql.counts.reads",
        counts[:read].to_i,
        tags: tags,
      )
      GitHub.dogstats.count(
        "job.process_event.rpc.mysql.counts.writes",
        counts[:write].to_i,
        tags: tags,
      )
    end
  end

  def dogstats_tags
    @dogstats_tags ||= Stratocaster::DogstatsTags.new
  end
end
