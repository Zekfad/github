# frozen_string_literal: true

class RemoveOrgMemberIssueAssignmentsJob < RemoveOrgMemberDataJob
  areas_of_responsibility :orgs, :repositories, :issues

  queue_as :remove_org_member_data

  # For a given @user, goes through private org repositories that have
  # issue assignments for the user and are not pullable by the user and
  # removes the issue assignments.
  #
  # Saves a restorable archive of each cleared record before clearing.
  # options is used by the base class it is a hash of { "organization_id" => value, "user_id" => value }
  def perform(options)
    org.org_repositories.private_scope.find_in_batches do |batch|
      org_repos = inaccessible_org_repos(batch)
      repo_to_clear_ids = org_repos.collect(&:id)

      issue_assignments_to_remove = Issue.where(repository_id: repo_to_clear_ids)
                                      .assigned_to(user)
                                      .includes(:assignments)
                                      .all
      restorable.save_issue_assignments(issue_assignments_to_remove)

      issue_assignments_to_remove.each do |issue|
        issue.remove_assignees(@user)

        begin
          GitHub::SchemaDomain.allowing_cross_domain_transactions { issue.save! }
        rescue ActiveRecord::ActiveRecordError => error
          Failbot.report(error)
        end
      end
    end

    restorable.save_issue_assignments_complete
  end
end
