# frozen_string_literal: true

WikiMaintenanceSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::WikiMaintenanceScheduler)
