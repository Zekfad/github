# frozen_string_literal: true

class ArchiveDanglingForksJob < ApplicationJob
  queue_as :archive_dangling_forks
  retry_on_dirty_exit

  BATCH_SIZE = 75_000

  areas_of_responsibility :orgs, :repositories

  # Only one of these jobs should run at any given time.
  locked_by timeout: 1.hour, key: ApplicationJob::DEFAULT_LOCK_PROC

  # Perform a throttled archive of forks in which the fork owner no longer
  # has permission on the source repository.
  # https://github.com/github/security/issues/2826
  #
  def perform(start = 0, _opts = nil) # will remove _opts after merging https://github.com/github/github/pull/150582
    unless GitHub.enterprise?
      finish = start + BATCH_SIZE
      GitHub.context.push(job: "ArchiveDanglingForks") do
        Audit.context.push(job: "ArchiveDanglingForks") do
          # `job` is set in context to assist with structured logging
          query = DataQuality::RepositoryNetwork::DanglingForks.new(clean: true, verbose: false, start: start, finish: finish)
          query.run
          ArchiveDanglingForksJob.perform_later(finish + 1) if finish < query.max_id
        end
      end
    end
  end
end
