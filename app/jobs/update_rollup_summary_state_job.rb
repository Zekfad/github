# frozen_string_literal: true

UpdateRollupSummaryStateJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateRollupSummaryState)
