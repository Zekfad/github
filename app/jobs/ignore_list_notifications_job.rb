# frozen_string_literal: true

IgnoreListNotificationsJob = LegacyApplicationJob.wrap(GitHub::Jobs::IgnoreListNotifications)
