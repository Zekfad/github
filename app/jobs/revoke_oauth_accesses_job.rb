# frozen_string_literal: true

class RevokeOauthAccessesJob < ApplicationJob
  areas_of_responsibility :ecosystem_apps

  BATCH_SIZE = 100

  queue_as :revoke_oauth_accesses

  discard_on ActiveRecord::RecordNotFound

  def perform(user, explanation:, enqueued_at:)
    ActiveRecord::Base.connected_to(role: :reading) do
      user.oauth_accesses.where("created_at < ?", enqueued_at).find_in_batches(batch_size: BATCH_SIZE) do |oauth_accesses|
        OauthAccess.throttle do
          oauth_accesses.each do |oauth_access|
            ActiveRecord::Base.connected_to(role: :writing) do
              # We want to destroy each one individually so that we get
              # the proper audit logging for security.
              oauth_access.destroy_with_explanation(explanation)
            end
          end
        end
      end
    end
  end
end
