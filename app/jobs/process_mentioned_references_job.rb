# frozen_string_literal: true

class ProcessMentionedReferencesJob < ApplicationJob
  queue_as :process_mentioned_references

  discard_on ActiveJob::DeserializationError

  # Create cross references record mentioned in the referrer's body
  # via Referrer#create_mentioned_references.
  #
  # referrer - A model with the Referrer module included.
  # ref_time - Timestamp used to record when the reference was made.
  def perform(referrer, ref_time)
    referrer.create_mentioned_references(ref_time)
  end
end
