# frozen_string_literal: true

# Does three things:
#   1. Pushes stats about manifest file changes.
#   2. Persists manifest files for the repository.
#   3. Updates the dependency graph API with detected manifests.
class RepositoryDependencyManifestInitializationJob < ApplicationJob
  areas_of_responsibility :dependabot
  queue_as :repository_dependencies

  UpdateManifestsMutationError = Class.new(StandardError)

  retry_on UpdateManifestsMutationError,
    wait: :exponentially_longer,
    attempts: 10

  include GitHub::CacheLock

  def perform(repository_id, options = {})
    options = options.with_indifferent_access
    got_lock = cache_lock_obtain(cache_key(repository_id), 5.minutes)
    return unless got_lock

    repository = Repository.find_by(id: repository_id)
    return unless repository&.default_oid
    return unless repository.default_oid != GitHub::NULL_OID
    return unless repository&.dependency_graph_enabled?

    repository.detect_dependency_manifests

    DependencyManifestFile.record_repository_manifest_changed_stats(
      repository: repository,
      paths: repository.dependency_manifest_paths,
      initial_commit: true
    )

    update_dependency_manifests(repository, options: options)
    scan_dependencies_for_vulnerabilities(repository)
  rescue ::GitRPC::InvalidRepository
    # The repository either doesn't exist yet or has been deleted. Either way
    # we should behave as though we hadn't found the repository in the database.
    nil
  ensure
    cache_lock_release(cache_key(repository_id)) if got_lock
  end

  private

  # If a repository has any manifest paths, we should send an event to
  # Dependabot so it can activate dependency updates and scan for
  # vulnerabilities.
  #
  # If the repository no longer has any manifest files,
  # but repository vulnerability alerts still remain,
  # Dependabot should ultimately remove those alerts.
  #
  # It's important that this method is separated and shares its name with the
  # overridden version in the
  # RepositoryDependencyManifestViewerInitializationJob job class.
  def scan_dependencies_for_vulnerabilities(repository)
    if repository.dependency_manifest_paths.any? || repository.repository_vulnerability_alerts.any?
      Dependabot.dependency_graph_initialized(repository: repository)
    end
  end

  def update_dependency_manifests(repository, options: {})
    if GitHub.enterprise? || Rails.env.development?
      update_dependency_manifests_mutation(repository, options: options)
    else
      enqueue_hydro_events(repository)
    end
  end

  def update_dependency_manifests_mutation(repository, options)
    repository.dependency_manifest_blobs.each do |blob|
      mutation = DependencyGraph::UpdateManifestsMutation.new(
        input: {
          repositoryId: repository.id,
          ownerId: repository.owner_id,
          repositoryPrivate: repository.private?,
          repositoryNwo: repository.name_with_owner,
          repositoryStargazerCount: repository.stargazer_count,
          manifestFiles: [
            {
              filename: blob.filename,
              path: blob.path,
              encoded: blob.encoded,
              gitRef: repository.default_oid,
              pushedAt: repository.pushed_at.iso8601
            }
          ]
        }
      )
      result = mutation.execute
      failed = false
      result.value do
        Failbot.report(result.error, app: "github-dependency-graph", repo_id: repository.id)
        failed = true
      end

      raise UpdateManifestsMutationError if failed
    end
  end

  def enqueue_hydro_events(repository)
    repository.dependency_manifest_blobs.each do |blob|
      next if blob.content.empty?
      GlobalInstrumenter.instrument "update_manifest.repository", {
        repository_id: repository.id,
        owner_id: repository.owner_id,
        repository_private: repository.private?,
        repository_fork: repository.fork?,
        repository_nwo: repository.name_with_owner,
        repository_stargazer_count: repository.stargazer_count,
        manifest_file: {
          filename: blob.filename,
          path: blob.path,
          content: blob.content,
          git_ref: repository.default_oid,
          pushed_at: repository.pushed_at.to_i
        }
      }
    end
  end

  def cache_key(repository_id)
    "repository-dependency-manifest-initialization:#{repository_id}"
  end
end
