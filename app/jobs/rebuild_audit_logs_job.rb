# frozen_string_literal: true

RebuildAuditLogsJob = LegacyApplicationJob.wrap(GitHub::Jobs::RebuildAuditLogs)
