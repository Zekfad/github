# frozen_string_literal: true

ChangeNetworkRootJob = LegacyApplicationJob.wrap(GitHub::Jobs::ChangeNetworkRoot)
