# frozen_string_literal: true

AddToSuppressionListJob = LegacyApplicationJob.wrap(GitHub::Jobs::AddToSuppressionList)
