# frozen_string_literal: true

OrganizationApplicationAccessApprovedJob = LegacyApplicationJob.wrap(GitHub::Jobs::OrganizationApplicationAccessApproved)
