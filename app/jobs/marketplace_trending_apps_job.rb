# frozen_string_literal: true

class MarketplaceTrendingAppsJob < ApplicationJob
  queue_as :marketplace
  schedule interval: 24.hours, condition: -> { !GitHub.enterprise? }

  def perform
    listings = []
    ActiveRecord::Base.connected_to(role: :reading) do
      Marketplace::Listing.non_sponsorable.with_state(:verified).find_each do |listing|
        score = listing.installation_count_trending_score
        listings << { id: listing.id, growth: score } unless score.zero?
      end

      listings.sort! { |a, b| b[:growth] <=> a[:growth] }
    end
    GitHub.kv.set("marketplace/trending_apps", listings.first(8).to_json)
  end
end
