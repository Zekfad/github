# frozen_string_literal: true

# Recalculates the cached number of stars when any of the starred users' spammy
# status changes. We hide spammy users, so this number has to match.
#
# The methods that calculate the stars count already know to exclude spammy
# users. We just have to run them.
class CalculateStarsCountJob < ApplicationJob
  queue_as :calculate_stars_count

  areas_of_responsibility :user_profile

  BATCH_SIZE = 100

  def perform(user_id)
    return unless user = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }

    ActiveRecord::Base.connected_to(role: :reading) do
      user.starred_repositories.find_in_batches(batch_size: BATCH_SIZE) do |batch|
        Repository.throttle do
          batch.each do |repo|
            ActiveRecord::Base.connected_to(role: :writing) { repo.update_stargazer_count! }
          end
        end
      end
    end
  end
end
