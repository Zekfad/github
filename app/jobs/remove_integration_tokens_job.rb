# frozen_string_literal: true

class RemoveIntegrationTokensJob < ApplicationJob
  areas_of_responsibility :platform

  queue_as :remove_integration_tokens

  # Discard the job if the integration is deleted before the job runs
  discard_on ActiveRecord::RecordNotFound

  def perform(integration)
    integration.revoke_tokens
  end
end
