# frozen_string_literal: true

RemoveRepoKeysForPolicymakerAndAppJob = LegacyApplicationJob.wrap(GitHub::Jobs::RemoveRepoKeysForPolicymakerAndApp)
