# frozen_string_literal: true

ClearGraphDataOnSpammyJob = LegacyApplicationJob.wrap(GitHub::Jobs::ClearGraphDataOnSpammy)
