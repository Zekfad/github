# frozen_string_literal: true

class MarketplaceListingPlanZuoraSyncJob < ApplicationJob
  areas_of_responsibility :gitcoin, :marketplace
  queue_as :zuora

  RETRYABLE_ERRORS = [
    Errno::ECONNREFUSED,
    Errno::ECONNRESET,
    Errno::ETIMEDOUT,
    Faraday::ConnectionFailed,
    Faraday::TimeoutError,
    Net::HTTPRequestTimeOut,
    Net::ReadTimeout,
    Zuorest::HttpError,
  ]

  RETRYABLE_ERRORS.each do |err|
    retry_on(err) do |_, error|
      Failbot.report(error)
    end
  end

  def perform(listing_plan)
    listing_plan.sync_to_zuora
  end
end
