# frozen_string_literal: true

class BusinessOrganizationBillingJob < ApplicationJob
  areas_of_responsibility :gitcoin

  RETRYABLE_ERRORS = [
    Braintree::UnexpectedError,
    GitHub::Restraint::UnableToLock,
    Faraday::ConnectionFailed,
    Faraday::SSLError,
    Faraday::TimeoutError,
    Net::OpenTimeout,
    ActiveRecord::RecordNotUnique,
  ].freeze

  discard_on(StandardError) do |job, error|
    Failbot.report(error)
  end

  RETRYABLE_ERRORS.each do |error_class|
    retry_on error_class do |_job, error|
      Failbot.report(error)
    end
  end

  queue_as :billing

  discard_on ActiveRecord::RecordNotFound

  def perform(business, org)
    return unless org.business == business

    lock(business.id, org.id) do
      business.migrate_organization_to_business_billing(org)
    end
  end

  private

  # Internal: Use a GitHub::Restraint to prevent simultaneous updates
  #
  # business_id - ID of the business we want to lock
  # block       - The code block to execute with an exclusive lock
  #
  # Returns nothing
  def lock(business_id, organization_id, &block)
    lock_key = "business-#{business_id}-organization-#{organization_id}-billing-job"

    restraint.lock!(lock_key, _max_concurrency = 1, _ttl = 5.minutes, &block)
  end

  # Internal: The restraint for locking and preventing simultaneous updates
  #
  # Returns GitHub::Restraint
  def restraint
    @restraint ||= GitHub::Restraint.new
  end
end
