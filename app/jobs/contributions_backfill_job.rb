# frozen_string_literal: true

ContributionsBackfillJob = LegacyApplicationJob.wrap(GitHub::Jobs::ContributionsBackfill)
