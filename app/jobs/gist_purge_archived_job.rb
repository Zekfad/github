# frozen_string_literal: true

GistPurgeArchivedJob = LegacyApplicationJob.wrap(GitHub::Jobs::GistPurgeArchived)
