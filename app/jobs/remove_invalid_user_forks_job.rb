# frozen_string_literal: true

# Removes forks owned by users who no longer have access to the fork's network root.
class RemoveInvalidUserForksJob < ApplicationJob
  areas_of_responsibility :repositories
  queue_as :remove_invalid_user_forks

  def perform(network_id:)
    start = Time.current
    RepositoryNetwork.find(network_id).remove_invalid_user_forks
  ensure
    GitHub.dogstats.timing_since("job.remove_invalid_user_forks.time", start)
  end
end
