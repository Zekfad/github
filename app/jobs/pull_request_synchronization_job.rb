# frozen_string_literal: true

class PullRequestSynchronizationJob < ApplicationJob
  areas_of_responsibility :code_collab, :pull_requests
  queue_as :pull_request_synchronization

  def perform(repository, ref, pusher, forced: false, before: nil, after: nil)
    PullRequest.synchronize_requests_for_ref(repository, ref, pusher,
      forced: forced,
      before: before,
      after: after
    )
  end
end
