# frozen_string_literal: true

UserSuspendDormantJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserSuspendDormant)
