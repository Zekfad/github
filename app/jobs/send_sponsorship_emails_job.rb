# frozen_string_literal: true

class SendSponsorshipEmailsJob < ApplicationJob
  queue_as :mailers

  def perform(sponsorship:, actor: nil)
    subscription = sponsorship.subscription_item

    # Ensure the subscription is still active, billing failure may have cancelled it
    return unless subscription.active?

    tier = subscription.subscribable
    sponsor = sponsorship.sponsor
    sponsorable = sponsorship.sponsorable
    listing = sponsorable.sponsors_listing
    sponsorship_amount = tier.base_price(duration: sponsor.plan_duration).to_s

    SponsorsMailer.new_sponsor(sponsorship).deliver_later

    SponsorsMailer.now_sponsoring(
      sponsorable_login: sponsorable.login,
      sponsor: sponsor,
      sponsorship_amount: sponsorship_amount,
      sponsor_billing_cycle: sponsor.plan_duration,
      sponsor_next_billing_date: sponsor.next_billing_date,
      actor: actor,
    ).deliver_later

    if !listing.milestone_email_sent? && listing.goals.none?
      return unless milestone = SponsorsMilestone.for_listing(listing)

      SponsorsPrimerMailer.milestone_reached(
        sponsorable: sponsorable,
        milestone_title: milestone.title,
      ).deliver_later

      listing.milestone_email_sent!
    end
  end
end
