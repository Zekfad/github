# frozen_string_literal: true

class RemoveOrgMemberWatchedRepositoriesJob < RemoveOrgMemberDataJob
  areas_of_responsibility :orgs, :repositories

  queue_as :remove_org_member_data

  # For a given @user, unwatches repositories with ids in repos_to_clear.
  # Saves a restorable archive of each cleared record before clearing.
  # options is used by the base class it is a hash of { "organization_id" => value, "user_id" => value }
  def perform(options)
    watched = 0

    org.org_repositories.private_scope.find_in_batches do |batch|
      org_repos = inaccessible_org_repos(batch)
      repo_to_clear_ids = org_repos.collect(&:id)

      response = GitHub.newsies.list_subscriptions(user, repo_to_clear_ids)
      next unless response.success?

      tracked_repo_ids = response.map(&:list_id)
      tracked_repo_to_clear_ids = (tracked_repo_ids & repo_to_clear_ids)

      repos = org_repos.select { |org_repo| tracked_repo_to_clear_ids.include?(org_repo.id) }
      repos = with_ignored_status(repos, response)
      restorable.save_watched_repositories(repos)

      GitHub.newsies.async_delete_all_for_user_and_lists(user, repos)
      watched += repos.length
    end

    restorable.save_watched_repositories_complete
    GitHub.dogstats.histogram "newsies", watched, tags: ["action:clear_memberships", "type:watched"]
  end

  private

  # Private: Update repositories with "ignored status" from
  #          subscriptions.
  #
  # For each Repository in REPOSITORIES, decorates the
  # object with a boolean @ignored attributes, which is then
  # updated with the ignored status for a given repository.
  #
  # The latter is taken from the corresponding subscription entry
  # in SUBSCRIPTIONS.
  #
  # repositories - an Array of Repositories
  # subscriptions - a Newsies::Responses::Array collection
  #
  # returns an Array of Repositories
  def with_ignored_status(repositories, subscriptions)
    WatchedRepositories::SubscriptionDetails
      .decorate_collection(repositories, subscriptions: subscriptions)
  end
end
