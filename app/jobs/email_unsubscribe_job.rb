# frozen_string_literal: true

EmailUnsubscribeJob = LegacyApplicationJob.wrap(GitHub::Jobs::EmailUnsubscribe)
