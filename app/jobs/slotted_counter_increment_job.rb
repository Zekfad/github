# frozen_string_literal: true

SlottedCounterIncrementJob = LegacyApplicationJob.wrap(GitHub::Jobs::SlottedCounterIncrement)
