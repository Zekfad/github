# frozen_string_literal: true

CreateCheckSuitesJob = LegacyApplicationJob.wrap(GitHub::Jobs::CreateCheckSuites)
