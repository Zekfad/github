# frozen_string_literal: true

class DestroyDeletedPackageVersionsJob < ApplicationJob
  queue_as :background_destroy

  schedule interval: 1.day, condition: -> { !GitHub.enterprise? }

  BATCH_SIZE = 100

  # How long deleted package versions are retained for restore. After this
  # period they are destroyed and unrecoverable.
  def expiration_period
    3.months
  end

  def perform
    SlowQueryLogger.disabled do
      expire_time = Time.now - expiration_period

      ActiveRecord::Base.connected_to(role: :writing) do
        Registry::PackageVersion.
          where("package_versions.deleted_at < ?", expire_time).
          in_batches(of: BATCH_SIZE) do |versions|
            versions.destroy_all
          end
      end
    end
  end
end
