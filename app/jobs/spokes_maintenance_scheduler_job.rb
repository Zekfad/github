# rubocop:disable Style/FrozenStringLiteralComment

require "github/hash_lock"

class SpokesMaintenanceSchedulerJob < ApplicationJob
  extend GitHub::HashLock
  areas_of_responsibility :dgit
  queue_as :dgit_schedulers

  schedule(interval: 120.seconds, scope: :global)

  # Run every interval, schedules jobs_per_interval maintenance jobs to run.
  def perform(network_id = nil)
    Failbot.push app: "github-dgit"
    SlowQueryLogger.disabled do
      start = Time.now
      result = GitHub.dogstats.time "dgit.maintenance-queries" do
        ActiveRecord::Base.connected_to(role: :reading) do
          GitHub::DGit::Maintenance.run_stats_and_maintenance(only_network_id: network_id)
        end
      end
      GitHub.stats.timing("dgit.maintenance-queries", Time.now-start) if GitHub.enterprise?
      result
    end
  end
end
