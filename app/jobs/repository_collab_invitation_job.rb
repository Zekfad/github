# frozen_string_literal: true

RepositoryCollabInvitationJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryCollabInvitation)
