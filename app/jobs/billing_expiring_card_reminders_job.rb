# frozen_string_literal: true

class BillingExpiringCardRemindersJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  schedule interval: 1.week, condition: -> { !GitHub.enterprise? }

  def perform
    User.send_expiring_credit_card_reminders
  end
end
