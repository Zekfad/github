# frozen_string_literal: true

class BillingRunStartJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  schedule interval: 1.hour, condition: -> { !GitHub.enterprise? }

  START_TIME = GitHub::Billing.timezone.parse("07:00:00")
  END_TIME = GitHub::Billing.timezone.parse("07:59:59")

  def perform
    if GitHub::Billing.now.between?(START_TIME, END_TIME)
      GitHub.dogstats.gauge("billing_run_start.job.run.time", Time.now.to_f)
      if GitHub.flipper[:k8s_billing_run_start_jobs].enabled?
        GitHub::Billing::Run.run_start
      else
        GitHub::Billing::Legacy::Run.run_start
      end
    end
  end
end
