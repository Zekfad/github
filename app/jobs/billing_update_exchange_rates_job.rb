# frozen_string_literal: true

class BillingUpdateExchangeRatesJob < ApplicationJob
  schedule interval: 6.hours, condition: -> { !GitHub.enterprise? }

  queue_as :billing

  def perform
    GitHub::Billing::Currency.update_rates
  end
end
