# frozen_string_literal: true

# See EmailDomainReputationRecord for more details.
class ProcessEmailDomainForReputationDataJob < ApplicationJob
  queue_as :spam

  # Only run one job per unique domain at a time.
  locked_by timeout: 1.hour, key: -> (job) { "process_email_domain_for_reputation_data::#{job.arguments[0]}" }

  def perform(domain)
    SlowQueryLogger.disabled do
      EmailDomainReputationRecord.process(domain)
    end
  end
end
