# frozen_string_literal: true
#
class UpdateCloseIssueReferencesJob < ApplicationJob

  queue_as :update_close_issue_references

  retry_on_dirty_exit

  def perform(issue_id)
    return unless pull_issue = Issue.find(issue_id)
    return unless pull = pull_issue.pull_request
    return unless pull.close_issues_on_merge_permitted?

    author_close_issues = pull.calculate_close_issues(user: pull.user)

    modifying_user_close_issues = if pull_issue.editor
      pull.calculate_close_issues(user: pull_issue.editor)
    else
      author_close_issues.dup
    end

    # Keep any refs that were previously created by the author or modifying user
    refs_to_keep = pull.close_issue_references.includes(:issue).xref.
      where(issue_id: (author_close_issues + modifying_user_close_issues).map(&:id))
    refs_to_remove = pull.close_issue_references.xref - refs_to_keep

    # remove any pull_request issues before attempting to save reference, as
    # a pull request cannot close another pull request
    # only create new xrefs that the modifying user has permission to create
    issues_to_add = (modifying_user_close_issues - refs_to_keep.map(&:issue)).reject(&:pull_request?)

    refs_to_remove.each do |ref|
      CloseIssueReference.throttle { ref.destroy }
    end

    return if pull.spammy?

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      issues_to_add.each do |issue|
        CloseIssueReference.throttle { pull.close_issue_references.create(issue: issue, actor_id: GitHub.context[:actor_id]) }
      end
    end
  end
end
