# frozen_string_literal: true

RepositorySetCommunityHealthFlagsJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositorySetCommunityHealthFlags)
