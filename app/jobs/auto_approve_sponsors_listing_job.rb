# frozen_string_literal: true

class AutoApproveSponsorsListingJob < ApplicationJob
  queue_as :sponsors_application_processing

  locked_by timeout: 1.hour, key: ->(job) { job.arguments[0].id }

  class ListingAcceptanceError < StandardError; end

  def perform(sponsors_listing)
    return unless sponsors_listing.auto_approvable?
    return unless sponsors_listing.verified_and_pending_approval?

    Failbot.push(app: "github-sponsors")

    result = Sponsors::ApproveSponsorsListing.call(
      sponsors_listing: sponsors_listing,
      actor: nil,
      automated: true,
    )

    unless result.success?
      raise ListingAcceptanceError.new \
        "Failed to auto accept listing: #{result.errors.to_sentence}"
    end
  end
end
