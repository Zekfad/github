# frozen_string_literal: true

class TransferIssueJob < ApplicationJob
  queue_as :transfer_issue

  retry_on_dirty_exit

  # Discard the job if the subject or author are deleted before the job runs
  discard_on ActiveRecord::RecordNotFound

  def perform(issue_transfer)
    issue_transfer.complete_transfer
  end
end
