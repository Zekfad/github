# frozen_string_literal: true

RepairEnterprisesIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairEnterprisesIndex)
