# frozen_string_literal: true

SpokesRepairDoaRepoJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitRepairDoaRepo)
