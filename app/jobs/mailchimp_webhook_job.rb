# frozen_string_literal: true

MailchimpWebhookJob = LegacyApplicationJob.wrap(GitHub::Jobs::MailchimpWebhook)
