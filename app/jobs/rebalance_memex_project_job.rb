# frozen_string_literal: true

class RebalanceMemexProjectJob < ApplicationJob
  areas_of_responsibility :projects
  queue_as :rebalance_memex_project

  retry_on_dirty_exit

  locked_by timeout: 1.hour, key: ->(job) { job.arguments[0] }

  def perform(memex_project_id, _actor_id)
    return unless (memex_project = MemexProject.find_by(id: memex_project_id))
    memex_project.rebalance!
  end
end
