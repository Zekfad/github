# frozen_string_literal: true

RepairCodeSearchIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairCodeSearchIndex)
