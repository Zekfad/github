# frozen_string_literal: true

SpokesGistMaintenanceSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitGistMaintenanceScheduler)
