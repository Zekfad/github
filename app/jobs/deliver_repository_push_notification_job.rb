# frozen_string_literal: true

# This job replaces the legacy `email` service from `github-services` which,
# when configured, sends email notifications about Push events to repositories.
#
# See https://github.com/github/experience-product/issues/114 for more detail
class DeliverRepositoryPushNotificationJob < ApplicationJob
  areas_of_responsibility :notifications
  queue_as :deliver_repository_push_notifications

  def perform(payload)
    @payload = payload
    return unless should_deliver?

    RepositoryPushNotificationMailer.build(
      repository: repository,
      ref: @payload[:ref],
      before: @payload[:before],
      after: @payload[:after],
      pusher: pusher,
      address: hook_configuration["address"],
      secret: hook_configuration["secret"],
    ).deliver_now
  end

  private

  def should_deliver?
    return false if pusher&.spammy?
    return false if repository&.spammy?
    repository && hook_configuration.present?
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def hook_configuration
    @hook_configuration ||= begin
      Hook.active.where(
        installation_target: repository,
        name: "email",
      ).first&.config_attributes
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def repository
    @repository ||= Repository.find_by_id(@payload[:repository_id])
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def pusher
    @pusher ||= begin
      User.find_by_id(@payload[:pusher_id]) if @payload[:pusher_id]
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
end
