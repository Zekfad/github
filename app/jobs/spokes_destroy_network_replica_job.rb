# frozen_string_literal: true

SpokesDestroyNetworkReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitDestroyNetworkReplica)
