# frozen_string_literal: true

DeleteDependentAbilitiesJob = LegacyApplicationJob.wrap(GitHub::Jobs::DeleteDependentAbilities)
