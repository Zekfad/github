# frozen_string_literal: true

class OnboardRemindersJob < ApplicationJob
  class RepositoryInviteError < StandardError; end

  areas_of_responsibility :ce_extensibility

  queue_as :mailers

  locked_by timeout: 1.hour, key: ->(job) { job.arguments[0].id }

  def perform(membership)
    return unless membership.member.is_a?(Organization)
    return unless membership.member.member?(membership.actor)
    return unless membership.member.reminders_enabled?

    RemindersMailer.waitlist_acceptance(membership).deliver_now
  end
end
