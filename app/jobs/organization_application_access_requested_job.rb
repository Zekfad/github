# frozen_string_literal: true

OrganizationApplicationAccessRequestedJob = LegacyApplicationJob.wrap(GitHub::Jobs::OrganizationApplicationAccessRequested)
