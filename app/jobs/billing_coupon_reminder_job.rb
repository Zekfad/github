# frozen_string_literal: true

class BillingCouponReminderJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  schedule interval: 24.hours, condition: -> { !GitHub.enterprise? }

  def perform
    GitHub::Billing.send_coupon_reminders
  end
end
