# frozen_string_literal: true

RepositoryAddTeamsJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryAddTeams)
