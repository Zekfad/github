# frozen_string_literal: true

RepositoryPreReceiveJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryPreReceive)
