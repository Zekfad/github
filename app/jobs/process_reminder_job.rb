# frozen_string_literal: true

class ProcessReminderJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :reminders

  locked_by timeout: 30.minutes, key: ApplicationJob::DEFAULT_LOCK_PROC # Lock on all arguments

  # These two limits may seem arbitrary, but they are to prevent an unbounded Slack message storm (which we encountered).
  # Right now, all scheduled reminders going to Slack end up as separate messages on a per repository basis.
  # That's due to the existing permission scheme for Hook::Event. Right now it's User, Org, or Repo.
  #
  # The REPOSITORY_LIMIT means that no more than 5 Slack messages will be posted at a given time (for a given organization).
  #
  # The NOTIFICATION_LIMIT means that WITHIN one of those 5 chosen Repositories, we won't create a massive message, if there were
  # 100s of pull requests that match.
  REPOSITORY_LIMIT = 5
  NOTIFICATION_LIMIT = 20

  # Process given reminder.
  #
  # @param reminder [Reminder or PersonalReminder] record to process
  # @param delivery_target: [Time] the time the reminder is expected to be delivered
  # @param test: [Boolean] tests the job by sending the reminder, but not updating the next delivery time
  def perform(reminder, delivery_target:, test: false)
    unless test
      if !reminder.valid_delivery_at?(delivery_target)
        GitHub.dogstats.increment("reminders.stale_delivery_target")
        return
      end

      reminder.update_next_delivery_times
    end

    if !GitHub.flipper[:scheduled_reminders_backend].enabled?(reminder.remindable)
      return
    end

    unless reminder.has_valid_associations?
      return
    end

    results = ActiveRecord::Base.connected_to(role: :reading) do
      reminder.filtered_pull_requests
    end

    prs_by_repo_id = results.prs_for_review.group_by(&:repository_id)
    prs_for_author_by_repo_id = results.prs_for_author.group_by(&:repository_id)
    repo_ids = (prs_by_repo_id.keys + prs_for_author_by_repo_id.keys).uniq

    repo_ids.each.with_index do |repo_id, index|
      break if index > REPOSITORY_LIMIT - 1

      pull_request_ids = (prs_by_repo_id[repo_id] || []).map(&:id)[0...NOTIFICATION_LIMIT]
      pull_request_ids_for_author = (prs_for_author_by_repo_id[repo_id] || []).map(&:id)[0...NOTIFICATION_LIMIT]

      Hook::Event::ReminderEvent.queue(
        event_at: delivery_target,
        pull_request_ids: pull_request_ids,
        pull_request_ids_for_author: pull_request_ids_for_author,
        reminder: reminder,
        repository_id: repo_id,
      )
    end
  end
end
