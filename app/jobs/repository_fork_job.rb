# frozen_string_literal: true

RepositoryForkJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryFork)
