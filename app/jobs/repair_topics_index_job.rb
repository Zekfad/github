# frozen_string_literal: true

RepairTopicsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairTopicsIndex)
