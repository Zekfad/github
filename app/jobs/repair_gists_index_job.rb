# frozen_string_literal: true

RepairGistsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairGistsIndex)
