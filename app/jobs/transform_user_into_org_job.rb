# frozen_string_literal: true

TransformUserIntoOrgJob = LegacyApplicationJob.wrap(GitHub::Jobs::TransformUserIntoOrg)
