# frozen_string_literal: true

RestoreOrganizationUserJob = LegacyApplicationJob.wrap(GitHub::Jobs::RestoreOrganizationUser)
