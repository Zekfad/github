# frozen_string_literal: true

CommunityProfileUpdateHelpWantedCountersJob = LegacyApplicationJob.wrap(GitHub::Jobs::CommunityProfileUpdateHelpWantedCounters)
