# frozen_string_literal: true

MigrationDestroyFileJob = LegacyApplicationJob.wrap(GitHub::Jobs::MigrationDestroyFile)
