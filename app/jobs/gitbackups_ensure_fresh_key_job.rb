# frozen_string_literal: true

GitbackupsEnsureFreshKeyJob = LegacyApplicationJob.wrap(GitHub::Jobs::GitbackupsEnsureFreshKey)
