# frozen_string_literal: true

class UpgradeIntegrationInstallationVersionJob < ApplicationJob
  queue_as :upgrade_integration_installation_version

  discard_on ActiveRecord::RecordNotFound

  BATCH_SIZE = 100
  TIME_LIMIT = 4.minutes

  # This value represents the minimum installations count an app should have
  # in order to be considered "popular".
  #
  # This job emits app-specific tags based on apps popularity.
  MINIMIMUM_POPULAR_INSTALLATIONS_COUNT = 100_000

  def perform(integration_id, old_version_number, new_version_number, installation_id: nil)
    integration = Integration.find(integration_id)
    # try read replica
    version = read_latest_version(integration, new_version_number, :reading)
    # try write master when not found
    version = version.nil? ? read_latest_version(integration, new_version_number, :writing) : version
    # if still not found drop it
    if version.nil?
      emit_count("drop_older_version", integration)
      return
    end
    ActiveRecord::Base.connected_to(role: :reading) do
      if installation_id.nil?
        emit_histogram("total_installations", installations_count(integration), integration)
      end

      GitHub::SafeTimer.timeout(TIME_LIMIT) do |timer|
        batch_options = { batch_size: BATCH_SIZE }
        batch_options[:start] = installation_id unless installation_id.nil?

        integration.outdated_installations(version).includes(:target).find_in_batches(**batch_options) do |installations|
          # This job can take a long time for Apps with many thousands of
          # installations. To avoid the job queue system killing this in the
          # middle of upgrading, we want to enqueue the job again after some
          # time.
          unless timer.run?
            self.class.perform_later(integration_id, old_version_number, new_version_number, installation_id: installations.first.id)
            emit_count("timed_out", integration)
            return
          end

          initial_time = Time.now

          if cancel_upgrade?(integration)
            emit_count("cancelled", integration)
            return
          end

          ActiveRecord::Base.connected_to(role: :writing) do
            upgrade_installation_version(installations, version)
          end

          emit_histogram("updated_installations", installations.count, integration)

          elapsed_time_in_seconds  = [1, (Time.now - initial_time).to_i].max
          installations_per_second = (installations.count / elapsed_time_in_seconds).to_i

          emit_histogram("installations_per_second", installations_per_second, integration)
        end

        emit_count("finished", integration)
      end
    end
  end

  private

  def upgrade_installation_version(installations, version)
    IntegrationInstallation.throttle do
      installations.each do |installation|
        editor =
          if installation.target.is_a?(::Business)
            User.find_by(id: GitHub.context[:actor_id])
          else
            installation.target
          end

        unless editor
          boom = RuntimeError.new "No permissions editor found for installation with ID #{installation.id}"
          Failbot.report boom
          next
        end

        result = installation.auto_update_version(editor: editor, version: version)
        next if result.success?

        if result.cannot_auto_upgrade?
          DeliverIntegrationUpdateEmailJob.perform_later(installation.id)
        else
          Failbot.report(result.error)
        end
      end
    end
  end

  def build_tags_for(integration)
    popular = popular_integration?(integration)
    tags = ["popular:#{popular}"]
    return tags unless popular

    tags.append("integration:#{integration.slug}")
  end

  def popular_integration?(integration)
    installations_count(integration) > MINIMIMUM_POPULAR_INSTALLATIONS_COUNT
  end

  def installations_count(integration)
    @total_installations_count ||= integration.installations.count
  end

  def emit_histogram(suffix, value, integration)
    GitHub.dogstats.histogram(
      "job.upgrade_integration_installation_version_job.#{suffix}",
      value,
      tags: build_tags_for(integration),
    )
  end

  def emit_count(suffix, integration)
    GitHub.dogstats.count(
      "job.upgrade_integration_installation_version_job.#{suffix}",
      1,
      tags: build_tags_for(integration),
    )
  end

  def cancel_upgrade?(integration)
    GitHub.flipper[:cancel_upgrade_integration_installation_version_job].enabled?(integration)
    # TODO: should eventually prevent concurrent upgrades for the same integration.
    #
    # If an App is upgraded multiple times within a short period of time,
    # we will have competiting jobs that could override each other's versions.
  end

  def read_latest_version(integration, version_number, role)
    version = ActiveRecord::Base.connected_to(role: role) do
      v = integration.versions.reorder(id: :desc).first
      v.number == version_number ? v : nil
    end
  end

end
