# frozen_string_literal: true

MarketplaceRetargetingJob = LegacyApplicationJob.wrap(GitHub::Jobs::MarketplaceRetargeting)
