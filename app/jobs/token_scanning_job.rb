# frozen_string_literal: true

class TokenScanningJob < ApplicationJob
  areas_of_responsibility :token_scanning
  queue_as :token_scanning

  DEFAULT_MAX_SCANNING_RESULTS = 8000

  retry_on(GitRPC::Timeout, wait: :exponentially_longer, attempts: 2) do |job, error|
    Failbot.report(error, repo_id: job.git_repo&.id)
  end

  # we retry again to work around issues with newer versions in hypercredscan having trouble
  # with cgo pointers being garbage collected and failing the job unexpectedly.
  retry_on(GitRPC::CommandFailed, wait: :exponentially_longer, attempts: 2) do |job, error|
    # avoid logging the actual error since its redacted in sentry and can include sensitive data in splunk
    # since we log scan results directly to console.
    Failbot.report(StandardError.new("find-creds command failed"), repo_id: job.git_repo&.id)
  end

  # We allow 5 repo-scoped retries to allow most short-lived lock backlogs to self stabilize without dropping scans.
  # After 5th retry we log to Sentry and Hydro to collect repo information
  retry_on(GitHub::Restraint::UnableToLock, wait: :exponentially_longer, attempts: 5) do |job, error|
    Failbot.report(StandardError.new("Scan terminated due to stop retry - unable to lock"), repo_id: job.git_repo&.id)
    TokenScanningJob.publish_job_failure_hydro_event(:STOP_RETRY_UNABLE_TO_LOCK, job.git_repo, job.actor)
  end

  discard_on(GitRPC::InvalidRepository)

  def dry_run?
    false
  end

  def max_token_scan_results
    DEFAULT_MAX_SCANNING_RESULTS
  end

  around_perform :with_stats

  def perform(*args)
    return "no_repo" if git_repo.nil?

    # We should never reach this line of code unless we should be scanning.
    # If we see stats for this we should hunt down the cause and add a new
    # granular result reason similar to `no_repo`, `private_repo`, etc.
    return "no_scan_for_tokens" unless token_scanning_enabled?
    repo_lock_key = "token_scanning_lock:" + git_repo.id.to_s
    max_concurrent_jobs = GitHub.configuration_secret_scanning_max_scans_per_repo
    lock_ttl = 15.minutes
    restraint.lock!(repo_lock_key, max_concurrent_jobs, lock_ttl) do
      scan_for_tokens
    end
  end

  # Collect stats about each attempt of perform
  def with_stats
    time_start = Time.now
    result = yield
  rescue GitRPC::Timeout, GitRPC::CommandFailed => error
    result = "error"
    TokenScanningJob.publish_job_failure_hydro_event(:FAILED_EXECUTION, git_repo, actor)
    # raise again to pass error to `retry_on`
    raise
  rescue GitHub::Restraint::UnableToLock => error
    # Another job is holding the lock at this point.
    result = "lock_unavailable"
    raise
  rescue GitRPC::InvalidRepository => error
    # Repository has been deleted so report as no_repo result
    result = "no_repo"
    TokenScanningJob.publish_job_failure_hydro_event(:NO_REPO, git_repo, actor)
    raise
  ensure
    time_delta = Time.now - time_start
    errname = error.nil? ? "nil" : error.class.name.underscore
    repo_public = git_repo.present? && git_repo.public?

    GitHub.dogstats.increment("token_scan.perform.try", tags: stats_tags +
      [
        "error:#{errname}",
        "try:#{executions}",
        "result:#{result}",
        "public:#{repo_public}",
        "dry_run:#{dry_run?}"
      ]
    )

    GitHub.dogstats.timing("token_scan.perform.time", time_delta,
      tags: stats_tags +
        [
          "error:#{errname}",
          "result:#{result}",
          "public:#{repo_public}",
          "dry_run:#{dry_run?}"
        ]
    )

    if is_tracked_by_scheduler?
        #populate a status entry if one was not already present.
        TokenScanStatus.ensure_status_entry_for_repo!(git_repo)

        if git_repo.token_scanning_enabled?
          # Record a token scan status entry. This allows for subsequent rescheduling of failed jobs and closure of completed job cycles.
          if result == "lock_unavailable"
            git_repo.token_scan_status.update_failed_scan_state!(:failed_capacity_unavailable)
          elsif !error.nil?
            git_repo.token_scan_status.update_failed_scan_state!(:failed_execution)
          else
            git_repo.token_scan_status.update_completed_scan_state!
          end
        else
          git_repo.token_scan_status.update_failed_scan_state!(:non_qualifying_repo)
        end
    end
  end

  # Add commit/url information to token_info hashes.
  #
  # Note that for performance reasons, the returned commit
  # is not the commit that introduced the blob, but the first
  # one that we found containing the blob
  #
  # token_infos - An Array of token_info Hashes.
  #
  # Returns nothing.
  def add_blob_locations(token_infos, roots = [])
    token_infos_by_blob = token_infos.group_by { |t| t[:blob] }
    blobs = token_infos_by_blob.keys
    return if blobs.empty?

    begin
      lines = git_repo.rpc.list_revision_history_multiple(roots, findobjs: blobs)
      lines.each do |line|
        blob, loc = line.split(" ", 2)
        commit_sha, path = loc.split("/", 2)
        token_infos_by_blob[blob].each do |token_info|
          token_info[:commit] = commit_sha
          token_info[:path] = path
          token_info[:url] = blob_url(commit_sha, path)
        end
      end
    rescue GitRPC::CommandFailed => e
      Failbot.report(e, repo_id: git_repo&.id, blobs: blobs)
    end

    token_infos.select do |token_info|
      token_info[:url].nil?
    end.each do |token_info|
      token_info[:url] = repo_url
    end
  end

  # Get the GitHub URL for a commit/file.
  #
  # commit_sha - A String commit sha1.
  # path       - The path within a repository to the given file.
  #
  # Returns a String URL.
  def blob_url(commit_sha, path)
    Rails.application.routes.url_helpers.blob_url(
      user_id:    git_repo.owner.login,
      repository: git_repo.name,
      name:       commit_sha,
      protocol:   GitHub.ssl? ? "https:" : "http:",
      host:       GitHub.host_name,
      path:       path,
    )
  end

  # Get the GitHub URL for the repository.
  #
  # Returns a String URL.
  def repo_url
    [GitHub.url, git_repo.nwo].join("/")
  end

  # Extensibility point for jobs to control enablement of scan.
  # This check varies with private repos.
  def token_scanning_enabled?
    git_repo.scan_for_tokens?
  end

  # Perform whatever token scanning is necessary for the job.
  #
  # This *must* be implemented by subclasses.
  #
  # Returns the status of the scan, should be "processed" on success.
  def scan_for_tokens
    raise "implement me"
  end

  # Indicates if this job is tracked by the scheduler.
  # Currently, only private repo scoped jobs are tracked in this fashion
  # to facilitate a retry cycle, after such jobs are starved out.
  def is_tracked_by_scheduler?
    return false
  end

  # Tags to include in any stats.
  #
  # Returns an Array of Strings.
  def stats_tags
    ["job:#{self.class.name.demodulize.underscore}"]
  end

  # Run the Git find creds command with the given arguments
  #
  # Returns an Array of token info hashes.
  def find_creds(**options)
    options[:max_result] = max_token_scan_results
    results = git_repo.rpc.find_creds(**options).map do |line|
      JSON.parse(line, symbolize_names: true)
    end

    publish_find_creds_result_metrics(results, git_repo)
    report_stats_for_token_groups(results)
    results
  end

  ## Used to publish metrics about tokens found in hypercredscan before dotcom post processing
  def publish_find_creds_result_metrics(results, repository)
    if results.count >= max_token_scan_results
      GitHub.dogstats.increment("token_scan_job.max_results", tags: [
        "scan_scope:#{job_scope}",
        "visibility:#{job_visibility}",
        "max_results:#{max_token_scan_results}",
        "dry_run:#{dry_run?}"
        ])
      Failbot.report(StandardError.new("Scan terminated early due to max results reached"), app: "github-user", repo_id: git_repo&.id)

      results.map { |token_info|
        GitHub.dogstats.increment("token_scan_job.max_results.max_type", tags: [
          "scan_scope:#{job_scope}",
          "visibility:#{job_visibility}",
          "max_results:#{max_token_scan_results}",
          "token_type:#{token_info.has_key?(:type)? token_info[:type]: ""}",
          "dry_run:#{dry_run?}"
          ])
      }
      TokenScanningJob.publish_job_failure_hydro_event(:TERMINATED_MAX_RESULTS, repository, actor)
    end
  end

  ## Publish failure information to hydro
  def self.publish_job_failure_hydro_event(error_type, repository, actor = nil)
    if !repository.is_a?(Gist)
      GlobalInstrumenter.instrument("secret_scanning.error", {
        request_context: nil,
        actor: actor,
        error_type: error_type,
        repository: repository,
      })
    end
  end

  ## Used to report token pair stats all scans, as if they are using private scan filtering logic
  ## This code detects pairs similar to PrivateTokenScanningJob.filter_same_file_token_pairs
  ## and MultiTypeTokenProcessor.pre_process_target_by_type_group_all, but only used for dogstats
  def report_stats_for_token_groups(tokens)
    pair_candidates, tokens_to_report = tokens.partition do |token|
      PrivateTokenScanningJob::same_file_tokens.include?(token[:type])
    end

    pair_candidates.group_by { |token| token[:path] }.each_value do |tokens_at_path|
      token_types_at_path = tokens_at_path.map { |token| token[:type] }.to_set
      tokens_at_path.each do |token|
        pair_matches = PrivateTokenScanningJob::same_file_token_pairs.any? do |pair|
          pair.include?(token[:type]) && pair.subset?(token_types_at_path)
        end

        GitHub.dogstats.increment("token_scan_job.pair_matches", tags: [
          "token_type:#{token[:type]}",
          "pair_matches:#{pair_matches}",
          "scan_scope:#{job_scope}",
          "visibility:#{job_visibility}",
          "dry_run:#{dry_run?}"
        ])
      end
    end
  end

  # Lazily instantiate a restraint lock
  def restraint
    @restraint ||= GitHub::Restraint.new
  end

  def job_scope
    raise "implement me"

  end

  def job_visibility
    raise "implement me"
  end

  def git_repo
    return @git_repo if defined? @git_repo

    ActiveRecord::Base.connected_to(role: :reading) do
      @git_repo = Repository.find_by_id(self.arguments.first)
    end
  end

  def refs
    @refs ||= self.arguments.second
  end

  def actor
    return @actor if defined? @actor

    ActiveRecord::Base.connected_to(role: :reading) do
      actor_login = self.arguments.third
      @actor = if actor_login.present?
        User.find_by_login(actor_login)
      else
        nil
      end
    end
  end
end
