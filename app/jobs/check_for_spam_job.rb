# frozen_string_literal: true

CheckForSpamJob = LegacyApplicationJob.wrap(GitHub::Jobs::CheckForSpam)
