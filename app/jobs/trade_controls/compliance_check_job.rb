# frozen_string_literal: true

module TradeControls
  class ComplianceCheckJob < ApplicationJob
    queue_as :trade_controls

    discard_on ActiveRecord::RecordNotFound

    def perform(user, ip: nil, email: nil)
      return if ip.nil? && email.nil?

      compliance = Compliance.for(ip: ip, email: email)

      if compliance.violation?
        user.trade_controls_restriction.enforce!(compliance: compliance)
      end
    end
  end
end
