# frozen_string_literal: true

module TradeControls
  class OrganizationComplianceCheckJob < ApplicationJob
    queue_as :trade_controls

    discard_on(ActiveRecord::RecordNotFound) do |job, error|
      Failbot.report(error)
    end

    before_enqueue do |job|
      throw(:abort) unless GitHub.billing_enabled?
    end

    def perform(organization_id, email: nil, website_url: nil, location: nil, reason: nil)

      organization = Organization.find(organization_id)

      return if organization.trade_controls_restriction.any?

      compliance = Compliance.for(
        email: email,
        website_url: website_url,
        check_outside_collaborators: reason == :organization_outside_collaborator,
        organization: organization,
        check_organization_members: reason == :organization_member,
        check_billing_managers: reason == :organization_billing_manager,
        check_admin: reason == :organization_admin,
        check_profile_email: reason == :organization_profile_email,
        check_billing_email: reason == :organization_billing_email
      )

      if GitHub.flipper[:org_compliance_refactor].enabled?(organization)
        compliance.check_and_enforce!
      else
        if compliance.minor_threshold_violation?
          organization.schedule_pending_enforcement(reason: compliance.reason, threshold: compliance.current_threshold)
        elsif compliance.violation?
          if organization.charged_account?
            if !organization.has_any_pending_trade_restriction?
              organization.trade_controls_restriction.enforce!(compliance: compliance)
            end
          else
            if GitHub.flipper[:automated_org_flagging_1_1].enabled?
              organization.trade_controls_restriction.tier_1_enforce!(compliance: compliance)
            else
              organization.trade_controls_restriction.partially_enforce!(compliance: compliance)
            end
          end
        end
      end
    end
  end
end
