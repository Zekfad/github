# frozen_string_literal: true

module TradeControls
  class PerformOrganizationEnforcementJob < ApplicationJob
    queue_as :trade_controls

    schedule interval: 12.hours, condition: -> { GitHub.billing_enabled? }

    def perform
      TradeControls::PendingRestriction.scheduled_for(GitHub::Billing.today).find_each do |restriction|
        OrganizationEnforcementJob.perform_later(restriction)
      end
    end
  end
end
