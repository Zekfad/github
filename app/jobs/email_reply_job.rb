# frozen_string_literal: true

EmailReplyJob = LegacyApplicationJob.wrap(GitHub::Jobs::EmailReply)
