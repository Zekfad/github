# frozen_string_literal: true

class LegacyStaleCheckJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :stale_check_runs

  # Takes a CheckSuite id and marks the check stale, if the check is older than
  # CheckSuite::DEFAULT_STALE_THRESHOLD (2 weeks).
  def perform(check_suite_id)
    return if GitHub.flipper[:k8s_stale_check_jobs].enabled?
    ActiveRecord::Base.connected_to(role: :reading) do
      check_suite = CheckSuite.find_by!(id: check_suite_id)

      # Verify that the check suite isn't completed yet (as a double precaution).
      return if check_suite.completed?

      # Verify that the check suite is older than 2 weeks (as a double precaution).
      return if check_suite.updated_at > (Time.zone.now - CheckSuite::DEFAULT_STALE_THRESHOLD)

      check_runs = []

      GitHub.dogstats.time("checks.stale.latest_runs_time") do
        check_runs = check_suite.latest_check_runs do |check_runs|
          check_runs.where.not(status: :completed)
        end
      end


      if check_runs.empty?
        stale_check_suite(check_suite)
      else
        # Otherwise, mark each check run as stale, which would update the check suite (indirectly).
        check_runs.each do |check_run|
          GitHub.dogstats.time("checks.stale.update_time") do
            CheckRun.throttle_writes do
              check_run.update!(conclusion: :stale, status: :completed)
            end

            GitHub.dogstats.increment("checks.marked_stale")
          end
        end

        if check_suite.explicit_completion
          stale_check_suite(check_suite)
        end
      end
    end
  end

  def stale_check_suite(check_suite)
    # If there are no incomplete check runs, but the check suite is still incomplete
    # then mark the check suite as stale.
    GitHub.dogstats.time("checks.stale.update_time.check_suite_only") do
      CheckSuite.throttle_writes do
        # Temporarily only use #update_columns to ensure no webhook events are created for this case.
        # See https://github.com/github/github/pull/143314#issuecomment-626029535 for details.
        # TODO: change #update_columns to #update! after the rollout.
        check_suite.update_columns(conclusion: :stale, status: :completed)
      end
      GitHub.dogstats.increment("checks.marked_stale")
    end
  end
end
