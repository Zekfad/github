# frozen_string_literal: true

IgnoreUserJob = LegacyApplicationJob.wrap(GitHub::Jobs::IgnoreUser)
