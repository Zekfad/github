# frozen_string_literal: true

class OnboardSponsorableJob < ApplicationJob
  class RepositoryInviteError < StandardError; end

  queue_as :mailers

  locked_by timeout: 1.hour, key: ->(job) { job.arguments[0].id }

  def perform(sponsors_membership)
    return unless sponsors_membership.accepted?
    sponsorable = sponsors_membership.sponsorable

    SponsorsMailer.waitlist_acceptance(sponsorable: sponsorable).deliver_now
  end
end
