# frozen_string_literal: true

LdapTeamSyncJob = LegacyApplicationJob.wrap(GitHub::Jobs::LdapTeamSync)
