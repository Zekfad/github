# frozen_string_literal: true

RepositoryPurgeJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryPurge)
