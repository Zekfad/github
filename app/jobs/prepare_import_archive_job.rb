# frozen_string_literal: true

class PrepareImportArchiveJob < ApplicationJob
  areas_of_responsibility :migration

  queue_as :prepare_import_archive

  retry_on Freno::Throttler::WaitedTooLong

  discard_on StandardError do |job, error|
    # Set migration state to :failed
    job.fail_migration_and_raise(error)
  end

  before_enqueue do |job|
    job.enqueue_migration
  end

  def perform(migration, actor)
    # Set failbot context
    Failbot.push(migration_id: migration.id)

    # Set state to preparing
    migration.begin_prepare!

    GitHub.migrator.prepare(migration, actor)
  end

  def enqueue_migration
    migration = self.arguments.first
    migration.prepare!
  end

  def fail_migration_and_raise(error)
    migration = self.arguments.first
    migration.failed!

    Failbot.report!(error)
    raise error
  end
end
