# frozen_string_literal: true

LdapNewMemberSyncJob = LegacyApplicationJob.wrap(GitHub::Jobs::LdapNewMemberSync)
