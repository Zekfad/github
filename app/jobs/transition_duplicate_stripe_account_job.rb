# frozen_string_literal: true

require "stripe"

class TransitionDuplicateStripeAccountJob < ApplicationJob
  queue_as :stripe

  ISSUE_URL = "https://github.com/github/sponsors/issues/1431"

  class DuplicateNotFoundError < StandardError; end

  RETRYABLE_ERRORS = [
    Stripe::APIConnectionError,
    Stripe::OAuth::InvalidRequestError,
    Stripe::StripeError,
  ].freeze

  DISCARDABLE_ERRORS = [
    Stripe::PermissionError,
    Stripe::OAuth::InvalidGrantError,
    DuplicateNotFoundError,
  ].freeze

  retry_on(*RETRYABLE_ERRORS) do |job, error|
    Failbot.report(error,
      sponsors_listing_id: job.arguments.first.id,
      app: "github-external-request",
    )
  end

  discard_on(*DISCARDABLE_ERRORS) do |job, error|
    Failbot.report(error,
      sponsors_listing_id: job.arguments.first.id,
      app: "github-external-request",
    )
  end

  def perform(sponsors_listing)
    return unless GitHub.sponsors_enabled?

    @sponsors_listing = sponsors_listing
    @old_stripe_account_id = sponsors_listing.stripe_connect_account.stripe_account_id

    if duplicate_stripe_account.blank?
      raise DuplicateNotFoundError.new("Couldn't find duplicate for #{old_stripe_account_id}")
    end

    authorization_code = sponsors_listing.duplicate_stripe_remediation_auth_code
    access_token_response = Billing::StripeConnect::Account::Oauth.request_access_token(
      authorization_code: authorization_code,
    )
    new_stripe_account_id = access_token_response[:stripe_user_id]
    new_account_details = Stripe::Account.retrieve(new_stripe_account_id).to_hash

    sponsors_listing.stripe_connect_account.update!(
      stripe_account_id: new_stripe_account_id,
      stripe_account_details: new_account_details,
    )

    sponsors_listing.update!(stripe_authorization_code: authorization_code)
    sponsors_listing.clear_duplicate_stripe_remediation_auth_code

    create_staff_notes!

    [sponsors_listing.stripe_connect_account, duplicate_stripe_account].each do |account|
      SetStripeConnectAccountMetadataJob.perform_later(account)
    end
  end

  private

  attr_reader :sponsors_listing, :old_stripe_account_id

  def duplicate_stripe_account
    @duplicate_stripe_account ||= Billing::StripeConnect::Account.where(
      stripe_account_id: old_stripe_account_id,
    ).where.not(payable: sponsors_listing).first
  end

  def create_staff_notes!
    listing_note = "Transitioned away from duplicate Stripe account " \
    "[`#{duplicate_stripe_account.stripe_account_id}`](#{duplicate_stripe_account.stripe_dashboard_url}). " \
    "See #{ISSUE_URL}."

    stafftools_url = Rails.application.routes.url_helpers.stafftools_sponsors_member_path(sponsors_listing.sponsorable.login)
    duplicate_note = "Stripe account contains transactions that were previously associated with" \
      "[`#{sponsors_listing.sponsorable.login}`](#{stafftools_url}). " \
      "See #{ISSUE_URL}."

    sponsors_listing.sponsors_membership.staff_notes.create(
      user: User.staff_user,
      note: listing_note,
    )

    duplicate_stripe_account.payable.sponsors_membership.staff_notes.create(
      user: User.staff_user,
      note: duplicate_note,
    )
  end
end
