# frozen_string_literal: true

require "codespaces"

class CodespacesProvisionJob < ApplicationJob
  class ForkNotReady < StandardError; end

  queue_as :codespaces
  locked_by timeout: 1.minute, key: DEFAULT_LOCK_PROC

  RETRYABLE_ERRORS = [
    ActiveRecord::RecordNotFound,
    Codespaces::Client::BadResponseError,
    Codespaces::Client::TimeoutError
  ]

  ALL_RETRYABLE_ERRORS = [
    *RETRYABLE_ERRORS,
    Codespaces::FindEnvironment::NotFoundError,
    ActiveRecord::RecordNotUnique,
    ForkNotReady
  ]

  # will retry twice over ~30 seconds
  retry_on *RETRYABLE_ERRORS,
    wait: :exponentially_longer, attempts: 3 do |job, error|
    job.provisioning_failed(error)
  end

  # These are raised effectively while we are "polling" for a previously timed
  # out environment creation to eventually finish being created. We want to
  # allow more of these retries over a longer period of time than the normal
  # retries.
  retry_on Codespaces::FindEnvironment::NotFoundError, attempts: 10 do |job, error|
    job.provisioning_failed(error)
  end

  # If we attempt to provision a plan (or resource group) and hit a `RecordNotUnique`
  # error from a race condition this will allow us to retry the job which should
  # subsequently find the conflicting record.
  retry_on ActiveRecord::RecordNotUnique, attempts: 1 do |job, error|
    job.provisioning_failed(error)
  end

  # Raised in perform if the codespace's backing repo was forked but git is not
  # yet ready on the fork. We retry until it becomes ready.
  retry_on ForkNotReady,
    wait: :exponentially_longer, attempts: 10 do |job, error|
    job.provisioning_failed(error)
  end

  def perform(codespace:, vscs_target: nil)
    # If the backing repository is `empty?` then the we cannot really provision
    # the codespace fully. It would work on our side but would break when VSCS
    # attempts to clone the repo. Raising here allows us to retry until it's
    # not `empty?`.
    raise ForkNotReady if codespace.repository.empty?
    Codespaces::ProvisionEnvironment.call(codespace, vscs_target: vscs_target)
  rescue *ALL_RETRYABLE_ERRORS
    raise
  rescue
    codespace.failed!
    raise
  end

  def stats_tags
    return [] unless codespace = arguments.first[:codespace]

    ["location:#{codespace.location}"]
  end

  def provisioning_failed(error)
    codespace = arguments.first[:codespace]
    codespace&.failed!

    raise error
  end
end
