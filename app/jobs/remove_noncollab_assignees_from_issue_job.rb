# frozen_string_literal: true

class RemoveNoncollabAssigneesFromIssueJob < ApplicationJob
  queue_as :remove_noncollab_assignees_from_issue

  retry_on_dirty_exit

  def perform(issue_id)
    if issue = Issue.find_by_id(issue_id)
      issue.remove_noncollab_assignees
    end
  end
end
