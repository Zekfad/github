# frozen_string_literal: true

RepairAuditLogIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairAuditLogIndex)
