# frozen_string_literal: true

OverrideSpammyRenamingDeletingAbilitiesJob = LegacyApplicationJob.wrap(GitHub::Jobs::OverrideSpammyRenamingDeletingAbilities)
