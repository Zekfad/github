# frozen_string_literal: true

# Job to scan all the commits in the changed refs for tokens
class CommitPrivateTokenScanningJob < PrivateTokenScanningJob
  def job_scope
    "commit"
  end

  def job_visibility
    "private"
  end

  def token_infos
    @token_infos ||= find_creds(refs: refs, buffer_results: true, skip_fp: true)
  end

  def is_commit_scoped?
    true
  end
end
