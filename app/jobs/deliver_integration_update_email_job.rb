# frozen_string_literal: true

DeliverIntegrationUpdateEmailJob = LegacyApplicationJob.wrap(GitHub::Jobs::DeliverIntegrationUpdateEmail)
