# frozen_string_literal: true

RepairAbilitiesFromUsersToAncestorTeamsJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairAbilitiesFromUsersToAncestorTeams)
