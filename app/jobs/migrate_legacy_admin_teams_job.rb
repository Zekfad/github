# frozen_string_literal: true

MigrateLegacyAdminTeamsJob = LegacyApplicationJob.wrap(GitHub::Jobs::MigrateLegacyAdminTeams)
