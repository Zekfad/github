# frozen_string_literal: true

class ImportArchiveJob < ApplicationJob
  areas_of_responsibility :migration

  queue_as :dotcom_importer

  retry_on Freno::Throttler::WaitedTooLong

  discard_on StandardError do |job, error|
    # Set migration state to :failed_import
    job.fail_migration_and_raise(error)
  end

  before_enqueue do |job|
    job.enqueue_migration
  end

  def perform(migration, actor)
    # Set failbot context
    Failbot.push(migration_id: migration.id)

    # Set state to importing
    migration.begin_import!

    GitHub.migrator.import(migration, actor)

    migration.complete_import!
  end

  def enqueue_migration
    migration = self.arguments.first
    migration.failed_import? ? migration.retry_import! : migration.import!
  end

  def fail_migration_and_raise(error)
    migration = self.arguments.first
    migration.force_failed_import!

    Failbot.report!(error)
    raise error
  end
end
