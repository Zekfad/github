# frozen_string_literal: true

RepairShowcasesIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairShowcasesIndex)
