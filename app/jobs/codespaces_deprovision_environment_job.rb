# frozen_string_literal: true

require "codespaces"

class CodespacesDeprovisionEnvironmentJob < ApplicationJob
  queue_as :codespaces
  locked_by timeout: 1.minute, key: DEFAULT_LOCK_PROC

  retry_on Codespaces::Client::BadResponseError, wait: :exponentially_longer, attempts: 5 # will retry 4 times over 6 minutes

  rescue_from ActiveRecord::RecordNotFound do
    # Ignore plan not found since it was likely deleted as the result of an account deletion.
  end

  # TODO: Make `owner` required once all existing jobs have run
  def perform(codespace_id:, plan:, environment_id:, owner: nil)
    Codespaces::ErrorReporter.push(
      plan: plan,
      codespace_id: codespace_id, # The codespace has already been deleted so we cannot provide it
      environment_id: environment_id,
      owner: owner
    )

    Codespace.deprovision_environment(codespace_id, plan, environment_id, owner || plan.owner)
  end
end
