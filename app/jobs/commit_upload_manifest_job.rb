# frozen_string_literal: true

class CommitUploadManifestJob < ApplicationJob
  areas_of_responsibility :repos
  queue_as :commit_upload_manifest

  attr_reader :manifest, :status

  MAX_ATTEMPTS = 4

  RETRY_ERRORS = [ActiveRecord::RecordNotFound, JobStatus::RecordNotFound, Git::Ref::ComparisonMismatch,
    Net::ReadTimeout, Net::OpenTimeout, GitHub::DGit::InsufficientQuorumError].freeze

  retry_on(*RETRY_ERRORS, wait: :exponentially_longer, attempts: MAX_ATTEMPTS) do |job, error|
    job.handle_failure(error)
  end

  def perform(manifest_id, status_id, options = {})
    UploadManifest.throttle do
      @manifest = UploadManifest.find(manifest_id)
      JobStatus.throttle do
        @status = JobStatus.find!(status_id)

        # We don't use JobStatus#track here because it will set the status to error prematurely
        # in the case of a retryable failure in UploadManifest#commit. JobStatuses are stored
        # on Mysql5.
        status.started!
        manifest.commit
        status.success!
      end
    end
  rescue Freno::Throttler::Error, *RETRY_ERRORS
    raise
  rescue => error
    handle_failure(error)
  end

  def handle_failure(error)
    if manifest
      UploadManifest.throttle { manifest.state_failed! }
      Failbot.push(user: manifest.uploader.login, repo_id: manifest.repository.id)
    end
    JobStatus.throttle { status&.error! }
    raise error
  rescue Freno::Throttler::Error
    raise StandardError.new("Throttled while reporting failure, cannot retry")
  end
end
