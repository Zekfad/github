# frozen_string_literal: true


# Public: Marks a spammy user as spammy in the notifications database.
#
class ToggleHiddenUserInNotificationsJob < ApplicationJob
  areas_of_responsibility :community_and_safety, :notifications

  queue_as :toggle_hidden_user_in_notifications

  retry_on GitHub::Restraint::UnableToLock

  retry_on_dirty_exit

  def perform(user_id, mode)
    restraint.lock!("toggle_hidden_user_in_notifications_for_user_#{user_id}", 1, 30.minutes) do
      Newsies::HiddenUser.throttle do
        if mode == "hide"
          Newsies::HiddenUser.hide_user(user_id)
        else
          Newsies::HiddenUser.unhide_user(user_id)
        end
      end
    end
  end

  private

  def restraint
    @restraint ||= GitHub::Restraint.new
  end
end
