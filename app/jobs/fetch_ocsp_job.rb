# frozen_string_literal: true

FetchOcspJob = LegacyApplicationJob.wrap(GitHub::Jobs::FetchOcsp)
