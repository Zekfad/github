# frozen_string_literal: true

RepairWorkflowRunsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairWorkflowRunsIndex)
