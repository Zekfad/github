# frozen_string_literal: true

require "terminal-table"

class GeyserBulkIngestJob < ApplicationJob
  areas_of_responsibility :code_search

  queue_as :index_high

  ELIGIBLE_EVENT_TYPES = %i(PUSHED REPAIR ADMIN_PUSHED ADMIN_REPAIR)
  DEFAULT_ROOM_ID = "#dsp-code-search-ops"
  REJECTION_HEADINGS = ["ID", "NWO", "Reason", "Public?", "Empty?", "Fork?"].freeze
  REJECTION_ROW_LIMIT = 20
  REJECTIONS_SPLUNK_PARAM = "search index=* app=github job=GeyserBulkIngestJob env=%{env} job_id=%{job_id} event_type=%{event_type} lab_scoped=%{lab_scoped}"
  REJECTIONS_SPLUNK_URL = "https://splunk.githubapp.com/en-US/app/gh_reference_app/search"

  def self.job_id(ids, collections = [], lab_scoped = false)
    "geyser_bulk_ingest_#{collections.join("-")}_#{lab_scoped ? "lab" : "production"}_#{Digest::SHA256.hexdigest(ids.uniq.sort.join("|"))}"
  end

  def self.status(ids, collections = [], lab_scoped = false)
    JobStatus.throttle do
      JobStatus.find!(GeyserBulkIngestJob.job_id(ids, collections, lab_scoped))
    end
  end

  def perform(ids, collections = [], lab_scoped = false, room_id = nil, event_type = :PUSHED)
    raise ArgumentError, "No repository ids specified" unless ids.present?
    raise ArgumentError, "Invalid event type #{event_type}" unless ELIGIBLE_EVENT_TYPES.include?(event_type)

    # select publisher override here, based on cardinality of repo ID list
    selector = Search::Geyser::Publisher.selector_by_list_size(ids)

    status = GeyserBulkIngestJob.status(ids, collections, lab_scoped)

    sent_count = 0
    rejection_log = []
    rejection_logger_context = {
      event_type: event_type,
      job_id: status.id,
      target_collections: collections.join(","),
      lab_scoped: lab_scoped,
      publisher: selector
    }

    status.track do
      Repository.throttle do
        Repository.where(id: ids).find_in_batches do |batch|
          batch.each do |repo|
            determination, reason = indexable_repository?(repo, lab_scoped)
            rejection_log << [repo, reason] and next unless determination

            payload = {
              change: event_type,
              repository: repo,
              updated_at: Time.now.utc,
              ref: "refs/heads/#{repo.default_branch}",
              lab_scoped: lab_scoped,
              target_collections: collections,
              publisher: selector,
            }

            GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
            sent_count += 1
          end
        end
      end
    end

    GitHub.dogstats.count("geyser.repo_changed_event.published", sent_count, {tags: ["change_type:{event_type.downcase}"]})
    msg = "GeyserBulkIngestJob complete. #{sent_count} RepositoryChange #{event_type} events published, #{ids.length - sent_count} rejected."
    msg += handle_rejections(rejection_log, rejection_logger_context)
    chatterbox_say(room_id, msg)
  end

  private

  # Returns [<boolean> determination, <string> reason]
  def indexable_repository?(repo, lab_scoped)
    indexing_enabled = repo.geyser_indexing_enabled_verbose

    # Make sure they're actually indexable (searchable, not blacklisted, etc.)
    return [indexing_enabled[:determination], indexing_enabled[:reason]] unless indexing_enabled[:determination]

    # As with geyser_indexing_enabled? this is checked again at publish time.
    # Repeat it here for job filtering test cases.
    return [false, "lab scoped and not public"] if lab_scoped && !repo.public?

    [true, nil]
  end

  def handle_rejections(rejection_log, logger_context)
    return "" if rejection_log.empty?
    event_type = logger_context[:event_type]
    log = "\n Rejections for this job can be <a href=\"#{splunk_url(logger_context)}\">found in splunk</a>."
    tablerize_rows = []

    GitHub::Logger.log_context(logger_context) do
      rejection_log.each do |repo, reason|
        # Log all rejections to splunk
        GitHub::Logger.log(logger_rejection_data(repo, reason))

        # Setup rejections data for Chatterbox while we're looping over them
        tablerize_rows << tablerize_rejection_data(repo, reason) if tablerize_rows.size <= rejection_row_limit
      end
    end

    # Include first REJECTION_ROW_LIMIT rejections in Chatterbox output
    if tablerize_rows.any?
      if rejection_log.size > rejection_row_limit
        log += " The first #{rejection_row_limit} rejections are shown below."
      end
      title = "#{event_type} Rejections"
      table = tablerize(title, REJECTION_HEADINGS, tablerize_rows)
      log += %Q{\n```\n#{table}\n```}
    end

    log
  end

  # Ensures subclasses can override this (mainly for testing)
  def rejection_row_limit
    self.class::REJECTION_ROW_LIMIT
  end

  def splunk_url(context)
    query = {
      q: REJECTIONS_SPLUNK_PARAM % {
        env: Rails.env,
        job_id: context[:job_id],
        event_type: context[:event_type],
        lab_scoped: context[:lab_scoped]
      }
    }
    "#{REJECTIONS_SPLUNK_URL}?#{query.to_query}"
  end

  def logger_rejection_data(repo, reason)
    {
      repo_id: repo.id,
      nwo: repo.nwo,
      reason: reason,
      public: repo.public?,
      empty: repo.empty?,
      fork: repo.fork?
    }
  end

  def tablerize_rejection_data(repo, reason)
    [
      repo.id,
      repo.nwo,
      reason,
      repo.public? ? "true" : "false",
      repo.empty? ? "true" : "false",
      repo.fork? ? "true" : "false"
    ]
  end

  def tablerize(title, headings, rows)
    table = Terminal::Table.new(title: title, headings: headings)
    table.rows = rows
    table
  end

  def chatterbox_say(room_id, msg)
    if room_id
      room_id = room_id.start_with?("#") ? room_id : "##{room_id}"
    else
      room_id = DEFAULT_ROOM_ID
    end

    GitHub::Chatterbox.client.say!(room_id, msg)
  end
end
