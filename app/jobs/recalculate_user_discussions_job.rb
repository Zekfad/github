# frozen_string_literal: true

class RecalculateUserDiscussionsJob < ApplicationJob
  queue_as :recalculate_user_discussions

  def perform(user)
    return unless user.spammy? || user.suspended?

    chosen_comments = user.discussion_comments.chosen_answers
    DiscussionComment.unmark_as_answers(chosen_comments) if chosen_comments.present?
  end
end
