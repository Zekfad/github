# frozen_string_literal: true

RepositoryFsckJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryFsck)
