# frozen_string_literal: true

MirrorSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::MirrorScheduler)
