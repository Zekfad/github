# frozen_string_literal: true

class ZuoraUsageUploadStatusJob < ApplicationJob
  areas_of_responsibility :gitcoin

  LOOKBACK_LIMIT = 14.days

  PRODUCTS_LINE_ITEMS_MAP = {
    gh_actions: [-> { Billing::ActionsUsageLineItem }],
    gh_packages: [-> { Billing::PackageRegistry::DataTransferLineItem }],
    gh_storage: [-> { Billing::SharedStorage::ArtifactAggregation }],
    all: [
      -> { Billing::ActionsUsageLineItem },
      -> { Billing::PackageRegistry::DataTransferLineItem },
      -> { Billing::SharedStorage::ArtifactAggregation },
    ],
  }

  schedule interval: 5.minutes, condition: -> { GitHub.actions_enabled? }

  queue_as :zuora

  class UnknownStatus < StandardError; end

  attr_reader :assumed_stuck_threshold

  def perform
    @assumed_stuck_threshold = Hash.new(3.hours)
    @assumed_stuck_threshold[:building] = 7.hours

    check_status_of_submitted_batches
    check_for_failed_batches
    check_for_stuck_batches
    check_time_since_last_completed_batch
  end

  def check_status_of_submitted_batches
    Billing::UsageSynchronizationBatch.submitted.find_each do |batch|
      next unless batch.usage_id
      status = Billing::Zuora::Import.find(batch.usage_id).status.downcase

      GitHub.dogstats.increment("zuora.usage_upload_status", tags: ["status:#{status}"])

      case status
      when "completed"
        batch.completed!
        Billing::ReconcileMeteredLineItemsJob.perform_later(batch)
      when "failed", "canceled"
        batch.failed!
      when "processing", "pending"
        # Do nothing if upload is still pending or processing
      else
        Failbot.report(UnknownStatus.new("Unknown status: #{status} returned from ZuoraUsageUploadStatusJob"))
      end
    end
  end

  def check_for_failed_batches
    min_created_at = Time.now - LOOKBACK_LIMIT
    PRODUCTS_LINE_ITEMS_MAP.keys.each do |product|
      batches = Billing::UsageSynchronizationBatch
        .failed
        .where(product: product)
        .where("created_at > ?", min_created_at)

      GitHub.dogstats.gauge(
        "billing.usage_synchronization_batch.failed",
        batches.count,
        tags: ["product:#{product}"],
      )
    end
  end

  def check_for_stuck_batches
    %i[building uploading uploaded ready_to_submit submitted].each do |status|
      PRODUCTS_LINE_ITEMS_MAP.each do |product, line_item_scope_procs|
        min_updated_at = Time.now - LOOKBACK_LIMIT
        max_updated_at = Time.now - assumed_stuck_threshold[status]

        batches = Billing::UsageSynchronizationBatch.where(
          status: status,
          product: product,
          updated_at: (min_updated_at..max_updated_at),
        )

        stuck_batches = line_item_scope_procs.flat_map do |line_item_scope_proc|
          line_item_scope = line_item_scope_proc.call
          batches.select { |batch| line_item_scope.where(synchronization_batch_id: batch.id).any? }
        end

        GitHub.dogstats.gauge(
          "billing.usage_synchronization_batch.stuck",
          stuck_batches.uniq(&:id).length,
          tags: ["product:#{product}", "status:#{status}"],
        )
      end
    end
  end

  def check_time_since_last_completed_batch
    now = Time.now
    PRODUCTS_LINE_ITEMS_MAP.keys.each do |product|
      last_completed_batch = Billing::UsageSynchronizationBatch
        .completed
        .where(product: product)
        .order(:created_at)
        .last

      next unless last_completed_batch

      time_since_last_completed_batch_in_ms = 1000 * (now.to_i - last_completed_batch.created_at.to_i)

      GitHub.dogstats.gauge(
        "billing.usage_synchronization_batch.time_since_completed_batch",
        time_since_last_completed_batch_in_ms,
        tags: ["product:#{product}"],
      )
    end
  end
end
