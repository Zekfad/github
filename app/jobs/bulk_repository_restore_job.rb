# frozen_string_literal: true

# Serially restores a list of repositories.
class BulkRepositoryRestoreJob < ApplicationJob

  areas_of_responsibility :repository

  queue_as :archive_restore

  class LockError < StandardError; end

  def self.job_id(ids)
    "bulk_repository_restore_#{ids.first}"
  end

  def self.status(ids)
    JobStatus.find(BulkRepositoryRestoreJob.job_id(ids))
  end

  def perform(ids, user_id)
    status = BulkRepositoryRestoreJob.status(ids)
    return unless status
    return unless ids.present?

    status.track do
      repo_id = ids.shift
      raise LockError unless GitHub::Jobs::RepositoryRestore.before_enqueue_lock(repo_id)
      GitHub::Jobs::RepositoryRestore.around_perform_lock(repo_id) do
        GitHub::Jobs::RepositoryRestore.new(repo_id, user_id).perform
        # create next job to continue bulk restore
        if ids.present?
          JobStatus.create(id: BulkRepositoryRestoreJob.job_id(ids))
          BulkRepositoryRestoreJob.perform_later(ids, user_id)
        end
      end
    end
  end

end
