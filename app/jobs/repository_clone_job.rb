# frozen_string_literal: true

class RepositoryCloneJob < ApplicationJob
  queue_as :critical

  locked_by timeout: 1.hour, key: -> (job) { job.arguments[0].id }

  # Discard the job if the user or repository are deleted before the job runs
  discard_on ActiveRecord::RecordNotFound

  def perform(repository, remote_url)
    Repository::Clone.from_url(repository, remote_url)
  end
end
