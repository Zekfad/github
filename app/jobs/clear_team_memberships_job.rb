# frozen_string_literal: true

ClearTeamMembershipsJob = LegacyApplicationJob.wrap(GitHub::Jobs::ClearTeamMemberships)
