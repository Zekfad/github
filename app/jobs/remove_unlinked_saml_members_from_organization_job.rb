# frozen_string_literal: true

RemoveUnlinkedSamlMembersFromOrganizationJob = LegacyApplicationJob.wrap(GitHub::Jobs::RemoveUnlinkedSamlMembersFromOrganization)
