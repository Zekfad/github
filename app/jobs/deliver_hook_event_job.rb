# frozen_string_literal: true

DeliverHookEventJob = LegacyApplicationJob.wrap(GitHub::Jobs::DeliverHookEvent)
