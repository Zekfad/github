# frozen_string_literal: true

class NotifyNotificationRestrictedMembersJob < ApplicationJob
  queue_as :mailers

  class ServiceUnavailable < RuntimeError; end

  retry_on(ServiceUnavailable, wait: 10.seconds, attempts: 5) do |job, error|
    Failbot.report(error)
  end

  # Public: Finds members in an organization that are no longer able to receive
  #         email notifications due to notification restrictions, and sends them
  #         an email informing them of that.
  #
  # organization – the Organization that has enabled notification restrictions.
  #
  # Returns nothing.
  def perform(organization)
    members = organization.async_members_without_verified_domain_email.sync

    # Get the newsies settings for each member, since we only want to send
    # this email to users who receive email notifications.
    newsies_response = GitHub.newsies.load_user_settings(members.pluck(:id))
    raise ServiceUnavailable unless newsies_response.success?

    newsies_settings = newsies_response.value.index_by(&:user)

    members.each do |member|
      if setting = newsies_settings[member]
        next unless receives_email_notifications?(setting)
        OrganizationMailer.notification_restrictions_enabled(member, organization).deliver_later
      end
    end
  end

  private

  # Private: Does this newsies config specify that the user should receive
  #          email notifications?
  #
  # settings - A Newsies::Settings object.
  #
  # Returns a Boolean.
  def receives_email_notifications?(setting)
    setting.participating_email? || setting.subscribed_email?
  end
end
