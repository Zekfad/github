# frozen_string_literal: true

# Job to clean up a repository's scan status and results
class TokenScanningCleanupJob < ApplicationJob
    queue_as :token_scanning

    retry_on_dirty_exit

    discard_on(GitRPC::InvalidRepository, GitRPC::CommandFailed) do |job, error|
      if error.is_a?(GitRPC::CommandFailed)
        Failbot.report(error, repo_id: job.git_repo&.id)
      end
    end

    def perform(*args)
      return "no_repo" if git_repo.nil?

      git_repo.token_scan_status && git_repo.token_scan_status.destroy
      git_repo.token_scan_results && git_repo.token_scan_results.destroy_all
    end

    def git_repo
      ActiveRecord::Base.connected_to(role: :reading) do
        @git_repo ||= Repository.find_by_id(self.arguments.first)
      end
    end
end
