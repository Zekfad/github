# frozen_string_literal: true

ScheduledArchiveDanglingForksJob = LegacyApplicationJob.wrap(GitHub::Jobs::ScheduledArchiveDanglingForks)
