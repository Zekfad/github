# frozen_string_literal: true

FastTimelineExperimentJob = LegacyApplicationJob.wrap(GitHub::Jobs::FastTimelineExperiment)
