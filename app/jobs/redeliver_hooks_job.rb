# frozen_string_literal: true

RedeliverHooksJob = LegacyApplicationJob.wrap(GitHub::Jobs::RedeliverHooks)
