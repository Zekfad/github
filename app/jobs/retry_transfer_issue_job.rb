# frozen_string_literal: true

class RetryTransferIssueJob < ApplicationJob
  queue_as :retry_transfer_issue

  # Discard the job if the subject or author are deleted before the job runs
  discard_on ActiveRecord::RecordNotFound

  def perform(old_repository_id, staff_user)
    issue_transfers = IssueTransfer.where(old_repository_id: old_repository_id, state: "errored")
    issue_transfers.each do |issue_transfer|
      if issue_transfer.old_issue
        issue_transfer.throttle { issue_transfer.transfer!(staff_user) }
      else
        issue_transfer.throttle { issue_transfer.retry_transfer }
      end
    end
  end
end
