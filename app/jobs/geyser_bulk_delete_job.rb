# frozen_string_literal: true
#
class GeyserBulkDeleteJob < ApplicationJob
  areas_of_responsibility :code_search

  queue_as :index_high

  DEFAULT_ROOM_ID = "#dsp-code-search-ops"
  ELIGIBLE_EVENT_TYPES = %i(DELETED ADMIN_DELETED)

  def self.job_id(ids, collections = [], lab_scoped = false)
    "geyser_bulk_delete_#{collections.join("-")}_#{lab_scoped ? "lab" : "production"}_#{Digest::SHA256.hexdigest(ids.uniq.sort.join("|"))}"
  end

  def self.status(ids, collections = [], lab_scoped = false)
    JobStatus.throttle do
      JobStatus.find!(GeyserBulkDeleteJob.job_id(ids, collections, lab_scoped))
    end
  end

  def perform(ids, collections = [], lab_scoped = false, room_id = nil, event_type = :DELETED)
    raise ArgumentError, "No repository ids specified" unless ids.present?
    raise ArgumentError, "Invalid event type #{event_type}" unless ELIGIBLE_EVENT_TYPES.include?(event_type)

    # select publisher override here, based on cardinality of repo ID list
    selector = Search::Geyser::Publisher.selector_by_list_size(ids)

    status = GeyserBulkDeleteJob.status(ids, collections, lab_scoped)

    status.track do
      ids.each do |repo_id|
        payload = {
          change: event_type,
          repository_id: repo_id,
          lab_scoped: lab_scoped,
          target_collections: collections,
          publisher: selector,
        }

        GlobalInstrumenter.instrument("search_indexing.repository_deleted", payload)
      end
    end

    GitHub.dogstats.count("geyser.repo_changed_event.published", ids.length, {tags: ["change_type:{event_type.downcase}"]})
    msg = "GeyserBulkDeleteJob complete. #{ids.length} RepositoryChange #{event_type} events published, 0 rejected."
    chatterbox_say(room_id, msg)
  end

  def chatterbox_say(room_id, msg)
    if room_id
      room_id = room_id.start_with?("#") ? room_id : "##{room_id}"
    else
      room_id = DEFAULT_ROOM_ID
    end

    GitHub::Chatterbox.client.say!(room_id, msg)
  end
end
