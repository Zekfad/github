# frozen_string_literal: true

module Newsies
  class MaintenanceBaseJob < ApplicationJob
    include SloHelper
    include ActiveJob::InitiallyEnqueuedAt

    areas_of_responsibility :notifications
    queue_as :notifications_maintenance

    RESTRAINT_LOCK_KEY = "notifications-maintenance-lock"
    RESTRAINT_LOCK_CONCURRENT_JOBS = 10
    RESTRAINT_LOCK_TTL = 10.minutes
    # the maximum length of list_hashes that _should_ be passed to the job
    MAX_LIST_SIZE = 1000
    # batch size for iterating the users in the job to ensure we throttle queries appropriately
    BATCH_SIZE = 10

    # Map from (inclusive) upper bound of a timing measurement to Datadog tag for SLO bucket.
    SLO_BUCKETS_MS = [
      # upper bound ms, tag name
      [1.second.in_milliseconds,   "bucket:0s-1s"],
      [10.seconds.in_milliseconds, "bucket:1s-10s"],
      [1.minute.in_milliseconds,   "bucket:10s-1m"],
      [5.minutes.in_milliseconds,  "bucket:1m-5m"],
      [20.minutes.in_milliseconds, "bucket:5m-20m"],
      [Float::INFINITY,            "bucket:20m-plus"]
    ]

    # These are "expected" exceptions where mysql2 is down, from lib/resiliency/response.rb
    Resiliency::Response::UnavailableExceptions.each do |error_class|
      retry_on error_class, wait: :exponentially_longer
    end

    # We wait a random amount of time so that we don't force jobs
    # to wait longer than they have to if more than one job is attempting to
    # grab the lock at the same time.
    retry_on GitHub::Restraint::UnableToLock, wait: :exponentially_longer, attempts: 10

    retry_on_dirty_exit

    around_perform do |job, block|
      unless GitHub.flipper[:notification_maintenance_restraint_lock].enabled?
        block.call
        record_time_to_perform
        next
      end

      GitHub::Restraint.new.lock!(RESTRAINT_LOCK_KEY, RESTRAINT_LOCK_CONCURRENT_JOBS, RESTRAINT_LOCK_TTL) do
        block.call
      end

      record_time_to_perform
    end

    def record_time_to_perform
      time_to_perform_ms = (Time.now.utc.to_f - initially_enqueued_at.to_f) * 1000.0
      GitHub.dogstats.timing("newsies.maintenance.time_to_perform", time_to_perform_ms, tags: [
        "class:#{self.class.name.underscore}",
        slo_bucket_tag(SLO_BUCKETS_MS, time_to_perform_ms),
        *@datadog_tags,
      ])
    end

    private

    def delete_thread_subscription_events(subscription_ids)
      SubscriptionEvent.where(
        subscription_type: ThreadSubscription.name,
        subscription_id: subscription_ids,
      ).in_batches(of: BATCH_SIZE) do |scope|
        SubscriptionEvent.throttle { scope.delete_all }
      end
    end
  end
end
