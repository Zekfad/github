# frozen_string_literal: true

# Indexes all notifications into elastic search for a given list of users
#
# An individual run of this job will index `BATCH_SIZE` notifications for a single user (the last one in the `user_ids` list)
# - If that user has more than `BATCH_SIZE` this job will requeue itself for the next batch.
# - Once all notifications for the last user in `user_ids` have been processed, this job will requeue itself for the remaining
#   users in the list.
#
# This repeats until the `user_ids` list is empty
module Newsies
  class IndexNotificationsJob < ApplicationJob
    areas_of_responsibility :notifications
    queue_as :notifications

    RetriesExceededError = Class.new(RuntimeError)
    MAX_ATTEMPTS_PER_BATCH = 5

    retry_on_dirty_exit

    # These are "expected" exceptions where the database is down, from lib/resiliency/response.rb
    Resiliency::Response::UnavailableExceptions.each do |error_class|
      # Add one so that we can catch when we've run out in the last perform call and requeue for remaining users
      retry_on error_class, wait: :exponentially_longer, attempts: MAX_ATTEMPTS_PER_BATCH + 1
    end

    # Arguments
    # user_ids:        required [Integer] - list of user ids to index notifications for
    # kv_enabled_key:  required String    - key to check in GitHub.kv, if this key exists the job will run, if not it will exit
    #                                       this allows the indexing process to be stopped by the RepairNotificationsIndexJob by clearing this key
    # index_name:      optional String    - specific name to index into if not the default
    # cluster_name:    optional String    - specific cluster name to index into if not the default
    #
    # These arguments are optional, if we are currently processing notifications for a given user
    # to allow us to start processing at a specific notification (the last one on the `user_ids` list)
    #
    # last_updated_at: optional String    - initial updated_at to start indexing from for the current user
    # last_id:         optional Integer   - initial last_id to start indexing from for the current user
    def perform(user_ids:, kv_enabled_key:, index_name: nil, cluster_name: nil, last_updated_at: 0, last_id: 0)
      # If the user id list is empty, we're done
      return if user_ids.empty?

      # Stop indexing if the KV flag has been disabled by the repair job
      return unless GitHub.kv.exists(kv_enabled_key).value { false }

      user_id = user_ids.pop

      # If we've failed to process this user more than the max attempts, discard
      # this user and continue for the others
      if executions >= MAX_ATTEMPTS_PER_BATCH
        # Requeue for any remaining users
        Failbot.report(RetriesExceededError.new, {
          user_id: user_id
        })
        self.class.perform_later(
          user_ids: user_ids,
          kv_enabled_key: kv_enabled_key,
          index_name: index_name,
          cluster_name: cluster_name
        )
        return
      end

      iterator_for_user = Iterator.new(user_id, last_updated_at, last_id)

      # Index a batch of notifications for the current user
      adapter = Elastomer::Adapters::BulkNotifications.create(iterator_for_user)
      index = Elastomer::Indexes::Notifications.new(index_name, cluster_name)
      index.store(adapter)

      if iterator_for_user.finished?
        return if user_ids.empty?

        # Requeue for any remaining users
        self.class.perform_later(
          user_ids: user_ids,
          kv_enabled_key: kv_enabled_key,
          index_name: index_name,
          cluster_name: cluster_name
        )
      else
        # Requeue for the current user as they still have notifications
        user_ids << user_id
        self.class.perform_later(
          user_ids: user_ids,
          kv_enabled_key: kv_enabled_key,
          index_name: index_name,
          cluster_name: cluster_name,
          last_updated_at: iterator_for_user.last_updated_at,
          last_id: iterator_for_user.last_id,
        )
      end
    end

    # This iterator is an iterator compatible with the API that Elastomer::Adapters::BulkNotifications.new(iterator) expects
    # Which is that it has a `each_record` method which takes a block and yields for each notification.
    #
    # This specific iterator will yield the next 1000 records for `user_id` starting from `initial_updated_at` and `initial_id`.
    class Iterator
      BATCH_SIZE = 1000

      # `updated_at` and `id` values for the last yielded record in the batch
      # usable by the caller to queue up the next batch with the _next_ `initial_updated_at` and `initial_id`
      attr_reader :last_updated_at, :last_id

      def initialize(user_id, initial_updated_at, initial_id)
        @user_id = user_id
        @initial_updated_at = initial_updated_at
        @initial_id = initial_id
        @finished = false
      end

      # If called after calling `.each_record` this predicate will tell us if we finished
      # iterating all of the user's notifications or not
      def finished?
        @finished
      end

      def each_record(&block)
        # The `(updated_at = :last_updated_at AND id > last_id) OR (updated_at > ?)` condition here
        # handles the case where the user has many notifications with the same updated_at and we want to
        # make sure we index them all but not get stuck in an infinite loop
        notifications = Newsies::NotificationEntry.from("`#{Newsies::NotificationEntry.table_name}` FORCE INDEX (index_notification_entries_on_user_id_and_updated_at)")
                                                  .for_user(@user_id)
                                                  .where("(updated_at = ? AND id > ?) OR (updated_at > ?)", @initial_updated_at, @initial_id, @initial_updated_at)
                                                  .order(updated_at: :asc, id: :asc)
                                                  .limit(BATCH_SIZE)
                                                  .includes(:rollup_summary)
                                                  .to_a

        # If we got no records back, we are done
        if notifications.empty?
          finish!
          return
        end

        # Yield each record we found to the block
        notifications.each(&block)

        # If we didn't get a full batch back, we know we are done for this user
        if notifications.length < BATCH_SIZE
          finish!
          return
        end

        # Set these for inspection by the caller for future batches
        @last_updated_at = notifications.last.updated_at.utc.strftime(ApplicationRecord::UTCTimeType::DATE_FORMAT)
        @last_id = notifications.last.id
      end

      private

      # Call this when we've iterated through all of the given user's notifications
      def finish!
        @finished = true
      end
    end
  end
end
