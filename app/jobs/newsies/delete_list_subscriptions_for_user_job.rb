# frozen_string_literal: true

module Newsies
  class DeleteListSubscriptionsForUserJob < MaintenanceBaseJob
    def perform(user_id, list_type)
      ListSubscription
        .for_user(user_id)
        .where(list_type: list_type)
        .excluding_ignored
        .in_batches(of: BATCH_SIZE) do |scope|

        ListSubscription.throttle { scope.delete_all }
      end
    end
  end
end
