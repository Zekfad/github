# frozen_string_literal: true

module Newsies
  # (Un)marks notifications as sapmmy for a given thread
  class UpdateNotificationsSpamStatusJob < MaintenanceBaseJob
    class UnableToLockThread < GitHub::Restraint::UnableToLock; end

    LOCK_RETRY_TTL = 5.minutes

    retry_on UnableToLockThread, wait: LOCK_RETRY_TTL

    around_perform do |job, block|
      # This job is also lock-restrained by MaintenanceBaseJob.
      # MaintenanceBaseJob lock is applied first and then per thread lock.
      # In order to prevent `retry_on` from MaintenanceBaseJob being overriden
      # in this class we rescue and raise a different exception and be explicit
      # about the behavior.
      list_type, list_id, thread_type, thread_id = job.arguments
      list = List.new(list_type, list_id)
      thread = Thread.new(thread_type, thread_id, list: list)
      begin
        GitHub::Restraint.new.lock!("update-notifications-spam-status-#{list.key}-#{thread.key}", 1, LOCK_RETRY_TTL) do
          block.call
        end
      rescue GitHub::Restraint::UnableToLock
        raise UnableToLockThread
      end
    end

    # list_type   - String newsies list type
    # list_id     - Integer newsies list id
    # thread_type - String newsies thread type
    # thread_id   - Integer newsies thread id
    def perform(list_type, list_id, thread_type, thread_id)
      thread_subject = thread_type.constantize.find_by_id(thread_id)
      is_thread_spammy = thread_subject&.respond_to?(:spammy?) && thread_subject.spammy?

      list = List.new(list_type, list_id)
      thread = Thread.new(thread_type, thread_id, list: list)

      move_notification_entries(
        thread: thread,
        source_class: is_thread_spammy ? NotificationEntry : SpammyNotificationEntry,
        destination_class: is_thread_spammy ? SpammyNotificationEntry : NotificationEntry,
        ignore_duplicates: !is_thread_spammy,
      )
    end

    private

    def move_notification_entries(thread:, source_class:, destination_class:, ignore_duplicates:)
      source_class.for_thread(thread).in_batches(of: BATCH_SIZE) do |batched_scope|
        values = batched_scope.map(&:attributes)

        destination_class.throttle do
          if ignore_duplicates
            destination_class.insert_all(values)
          else
            destination_class.upsert_all(values)
          end
        end

        source_class.throttle { batched_scope.delete_all }
      end
    end
  end
end
