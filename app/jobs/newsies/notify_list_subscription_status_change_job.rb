# frozen_string_literal: true

module Newsies
  class NotifyListSubscriptionStatusChangeJob < ApplicationJob
    areas_of_responsibility :notifications
    queue_as :notify_subscription_status_change
    retry_on_dirty_exit

    def perform(user_id, list_type, list_id)
      user = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }
      return unless user

      list = ActiveRecord::Base.connected_to(role: :reading) { list_type.constantize.find_by_id(list_id) }
      return unless list

      channel = GitHub::WebSocket::Channels.list_subscription(user, list)
      data = {timestamp: Time.now.to_i}

      GitHub::WebSocket.notify_user_channel(user, channel, data)
    end
  end
end
