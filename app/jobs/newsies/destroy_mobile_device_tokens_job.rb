# frozen_string_literal: true

module Newsies
  class DestroyMobileDeviceTokensJob < ApplicationJob
    areas_of_responsibility :notifications

    queue_as :notifications
    retry_on_dirty_exit

    retry_on(
      *Resiliency::Response::UnavailableExceptions,
      wait: :exponentially_longer,
      attempts: 6,
    )

    def perform(user_id)
      GitHub.newsies.destroy_mobile_device_tokens(user_id)
    end
  end
end
