# frozen_string_literal: true

module Newsies
  class AutoSubscribeUsersToRepositoryJob < ApplicationJob
    class AutoSubscribeUsersToRepositoryError < StandardError; end

    areas_of_responsibility :notifications

    queue_as :notifications

    retry_on_dirty_exit

    retry_on AutoSubscribeUsersToRepositoryError, wait: :exponentially_longer

    BATCH_SIZE = 100

    def perform(repo_id, user_ids, repository_creator_id = nil)
      return if user_ids.empty?
      return unless repo = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(repo_id) }

      user_ids.each_slice(BATCH_SIZE) do |slice|
        users = ActiveRecord::Base.connected_to(role: :reading) { User.with_ids(slice.flatten.compact) }

        users.each do |user|
          notify = !repository_creator_id || repository_creator_id != user.id

          auto_subscribe_to_own_repos = repo.owner_id == user.id
          # Throttling on Domain::Notifications because auto_subscribe writes
          # using the ListSubscription and ThreadTypeSubscription models which
          # belong to the Notifications domain.
          response = ApplicationRecord::Domain::Notifications.throttle { GitHub.newsies.auto_subscribe(user, repo, notify, auto_subscribe_to_own_repos) }

          raise AutoSubscribeUsersToRepositoryError unless response.success?
        end
      end
    end
  end
end
