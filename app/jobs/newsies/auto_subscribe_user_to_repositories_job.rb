# frozen_string_literal: true

module Newsies
  class AutoSubscribeUserToRepositoriesJob < ApplicationJob
    areas_of_responsibility :notifications
    queue_as :notifications

    BATCH_SIZE = 100

    class AutoSubscribeError < StandardError; end
    retry_on AutoSubscribeError, wait: :exponentially_longer

    # Subscribes a User to a set of repositories, by calling GitHub.newsies.auto_subscribe.
    #
    #
    # user_id - A User id
    # repository_ids - An array of Repository ids. Could be just one.
    # notify - Boolean that determines if the user is notified.
    #
    def perform(user_id, repository_ids, notify = true)
      return if repository_ids.empty?
      return unless user = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }

      repository_ids.each_slice(BATCH_SIZE) do |slice|
        repos = ActiveRecord::Base.connected_to(role: :reading) { Repository.with_ids(slice.flatten.compact) }

        repos.each do |repo|
          # Throttling on Domain::Notifications because auto_subscribe writes
          # using the ListSubscription and ThreadTypeSubscription models which
          # belong to the Notifications domain.
          response = ApplicationRecord::Domain::Notifications.throttle { GitHub.newsies.auto_subscribe(user, repo, notify) }

          raise AutoSubscribeError unless response.success?
        end
      end
    end
  end
end
