# frozen_string_literal: true

module Newsies
  # Delete all subscriptions and notifications for a user and a given set of lists
  class DeleteAllForUserAndListsJob < MaintenanceBaseJob
    # user_id - Integer user to act on
    # lists   - Array of hashes: [ { type: "Repository", id: 1}, ...]
    def perform(user_id, list_hashes)
      lists = list_hashes.map { |l| List.new(l[:type], l[:id]) }

      lists.each_slice(BATCH_SIZE) do |batched_lists|
        ListSubscription.throttle do
          ListSubscription.for_user(user_id).for_lists(batched_lists).delete_all
        end

        ThreadTypeSubscription.throttle do
          ThreadTypeSubscription.for_user(user_id).for_lists(batched_lists).delete_all
        end

        ThreadSubscription.for_user(user_id)
                          .for_lists(batched_lists)
                          .in_batches(of: BATCH_SIZE) do |batched_scope|

          delete_thread_subscription_events(batched_scope.pluck(:id))
          ThreadSubscription.throttle { batched_scope.delete_all }
        end

        NotificationEntry.for_user(user_id)
                         .for_lists(batched_lists)
                         .in_batches(of: BATCH_SIZE) do |scope|
          NotificationEntry.throttle { scope.delete_all }
        end

        SavedNotificationEntry.for_user(user_id)
                              .for_lists(batched_lists)
                              .in_batches(of: BATCH_SIZE) do |scope|
          SavedNotificationEntry.throttle { scope.delete_all }
        end
      end
    end
  end
end
