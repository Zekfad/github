# frozen_string_literal: true

module Newsies
  # Delete all subscriptions and notifications for a user
  class DeleteAllForUserJob < MaintenanceBaseJob
    # user_id - Integer user to act on
    def perform(user_id)
      scopes_by_klass = {
        ListSubscription => ListSubscription.for_user(user_id),
        ThreadTypeSubscription => ThreadTypeSubscription.for_user(user_id),
        NotificationEntry => NotificationEntry.for_user(user_id),
        SavedNotificationEntry => SavedNotificationEntry.for_user(user_id),
        CustomInbox => CustomInbox.where(user_id: user_id),
        MobileDeviceToken => MobileDeviceToken.where(user_id: user_id),
      }

      scopes_by_klass.each do |klass, scope|
        scope.in_batches(of: BATCH_SIZE) do |batched_scope|
          klass.throttle { batched_scope.delete_all }
        end
      end

      ThreadSubscription.for_user(user_id).in_batches(of: BATCH_SIZE) do |batched_scope|
        delete_thread_subscription_events(batched_scope.pluck(:id))
        ThreadSubscription.throttle { batched_scope.delete_all }
      end
    end
  end
end
