# frozen_string_literal: true

DestroyDependentRecordsJob = LegacyApplicationJob.wrap(GitHub::Jobs::DestroyDependentRecords)
