# frozen_string_literal: true

SpokesRepairRepoReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitRepairRepoReplica)
