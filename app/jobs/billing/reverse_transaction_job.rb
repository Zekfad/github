# frozen_string_literal: true

module Billing
  class ReverseTransactionJob < ApplicationJob
    areas_of_responsibility :gitcoin
    queue_as :billing

    discard_on ActiveRecord::RecordNotFound

    locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 5.minutes

    def perform(transaction)
      GitHub::Billing.void_or_refund_transaction(transaction)
    end
  end
end
