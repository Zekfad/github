# frozen_string_literal: true

module Billing
  module PackageRegistry
    class UsageAggregationJob < ApplicationJob
      queue_as :billing

      discard_on(StandardError) do |_job, error|
        GitHub.dogstats.increment("billing.package_registry.aggregation_error")
        Failbot.report(error)
      end

      discard_on(UsageAggregator::LineItemOutOfBoundsError) do
        GitHub.dogstats.increment("billing.package_registry.aggregation_line_item_out_of_bounds")
      end

      def perform(line_item)
        UsageAggregator.perform(line_item)
      end
    end
  end
end
