# frozen_string_literal: true

module Billing
  class VssSubscriptionEventProcessingJob < ApplicationJob
    InvalidEventError = Class.new(StandardError)

    areas_of_responsibility :gitcoin
    queue_as :billing

    def perform(event)
      parsed_event = ParsedVssSubscriptionEvent.new(event.payload)

      if !parsed_event.valid?
        Failbot.report(InvalidEventError.new("Invalid JSON"), vss_subscription_event_id: event.id)
        event.failed!
        return
      end

      # we only need to process new assignments (at the moment)
      if parsed_event.new_assignment?
        Billing::BundledLicenseAssignment.create!(
          enterprise_agreement_number: parsed_event.enterprise_agreement_number,
          business_id: Billing::EnterpriseAgreement.find_by(agreement_id: parsed_event.enterprise_agreement_number)&.business_id,
          email: parsed_event.email,
          subscription_id: parsed_event.subscription_id,
          revoked: false
        )
      end

      event.processed!
    rescue ActiveRecord::RecordInvalid
      Failbot.report(InvalidEventError.new("Invalid Event"), vss_subscription_event_id: event.id)
      event.failed!
    end
  end
end
