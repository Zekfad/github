# frozen_string_literal: true

module Billing
  module SharedStorage
    class FanoutAggregationJob < ApplicationJob
      queue_as :billing

      discard_on(StandardError) do |job, error|
        GitHub.dogstats.increment(
          "billing.shared_storage.aggregation_error",
          tags: ["job:fanout_aggregation_job"],
        )

        Failbot.report(error)
      end

      retry_on(GitHub::Restraint::UnableToLock, wait: 5.minutes) do |job, error|
        GitHub.dogstats.increment(
          "billing.shared_storage.aggregation_error",
          tags: ["job:aggregation_job"],
        )

        Failbot.report(error)
      end

      # Perform the FanoutAggregationJob
      #
      # cutoff - The cutoff time for shared storage artifact events; only events
      #          with an effective_at time before the cutoff will be aggregated
      def perform(cutoff)
        ArtifactEventAggregationFanout.new(cutoff: cutoff).perform
      end
    end
  end
end
