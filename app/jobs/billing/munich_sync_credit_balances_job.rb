# frozen_string_literal: true

# NOTE: this job is planned to be short living. There were some accounts
# that were given account credit to offset some burden from the changes
# of project Munich. Once all of the accounts have used up their credits,
# we should remove this job, and the associated FF. Alternatively, if
# https://github.com/github/gitcoin/issues/4502 is completed, this should
# be removed, as we will be handling it in a more robust way.
module Billing
  class MunichSyncCreditBalancesJob < ApplicationJob
    require "faraday"
    require "zuorest"

    queue_as :zuora
    schedule interval: 12.hours, condition: -> { GitHub.billing_enabled? }

    class AccountWithSponsorshipsError < StandardError; end
    class MunichCreditGeneralError < StandardError; end

    MAX_THROTTLE_RETRIES = 5

    SKIPPABLE_ERRORS = [
      Errno::ECONNREFUSED,
      Errno::ECONNRESET,
      Errno::ETIMEDOUT,
      Net::HTTPRequestTimeOut,
      Net::ReadTimeout,
      Faraday::ConnectionFailed,
      Faraday::TimeoutError,
      Zuorest::HttpError,
    ]

    # IDs taken from https://github.com/github/github/pull/142986#issuecomment-625473932
    AFFECTED_ACCOUNTS = [
      26082513, 17062516, 60093335, 14199680, 47140425,
      48106925, 1259053, 5452545, 56954704, 32060209,
      33752186, 48765393, 47341816, 7561929, 51993182,
      33520792, 7443987, 61500837, 57373338, 40170941,
      18718289, 52156110, 38600381, 38525432, 34308787,
      13155350, 57230860, 25810539, 1175789, 19580129,
      24717486, 46288141, 57719955, 47581411, 53158175,
      61311422, 27803210, 57350089, 43927314, 58692119,
      12294433, 43270238, 53180852, 57867123, 59600613,
      12717990, 45596870, 59059547, 55397463, 59866032,
      1156073, 61148054, 37988817, 60224631, 56025210,
      14226671, 35266076, 3726938, 8161400, 43186587,
      43924236, 28899405, 61531672, 60611904, 53571787,
      29538499, 17320237, 11225274, 15890507, 55173105,
      6864447, 58740808, 45080473, 60464826, 37185978,
      57744687, 47645307, 8042965, 28543612, 61360785,
      49649630, 46711855, 50154490, 11827412, 15801841,
      61118568, 33410311, 58428604, 44145212, 18675622,
      13652544, 51527264, 53809930, 48210689, 54303659,
      40503742, 34175881, 22908044, 44532517, 1514296,
      42923087, 44402898, 47297392, 1932773, 45976574,
      45567120, 8481890, 52569430, 23473198, 52008574,
      10902327, 29761645, 31145087, 52921647, 60918883,
      32137768, 59289063, 35112493, 12961533, 26648673,
      57532497, 50804819, 48009886, 54186782, 10778642,
      54661828, 16728586, 57081276, 40668583, 46592213,
      45656549, 43501156, 58767178, 6511518, 59422094,
      52319471, 45032549, 33842174, 22898424, 2315268,
      19795376, 19802566, 56863362, 29747853, 58534278,
      42538842, 46656519, 53097487, 13241739, 57229909,
      57235737, 31976851, 55189234, 12233361, 57897853,
      10975527, 32225321, 22058215, 35511208, 35904153,
      53259754, 58183033, 30006837, 1410622, 22116112,
      9438307, 40385244, 35769962, 28626505, 17601259,
      13098793, 25626574, 46364408, 1846869, 56128253,
      49813771, 41735806, 10534019, 60384364, 13145426,
      33264210, 48393671, 4831149, 61533388, 45580459,
      59060542, 55612964, 7593997, 40864551, 23058471,
      52662647, 42726180, 60446691, 31215716, 51254741,
      57735973, 59741323, 465876, 20278509, 36711115,
      28954611, 32846408, 55495957, 46792101, 57792393,
      57779386, 5064416, 22958618, 44354035, 19615367,
      33716935, 56683841, 14262913, 55847494, 16534509,
      59846381, 1699863, 44374088, 38757776, 33830673,
      24672210, 51288319, 17318535, 51727080, 19981816,
      24708025, 55193500, 55958114, 34022344, 56345474,
      31706258, 16645716, 385458, 55112746, 52931478,
      52474114, 59520104, 51245727, 56051992, 41970352,
      59442741, 25459952, 1014733, 61637637, 3639115,
      24528320, 44739107, 61472339, 17412377, 34274232,
      8061830, 34793257, 8087852, 29920179, 7780779,
      35506833, 47571226, 52422323, 29048253, 41866627,
      57098516, 30271045, 51083040, 24437900, 19464204,
      17016431, 6735834, 52991200, 48932923, 29681290,
      49046730, 14234075, 2940053, 59724684, 54567527,
      59570456, 56560892, 16723999, 26238397, 7537829,
      43823738, 55544050, 45152898, 26790003, 25102091,
      4316822, 56027435, 35269857, 7900150, 18581229,
      54961807, 39800481, 58350330, 19283251, 55797246,
      45189342, 52414990, 11424519, 39317002, 46490948,
      55001538, 46028908, 1638248, 12545239, 38894380,
      1459348, 19696916, 53653121, 32790984, 47484356,
      39599027, 48131774, 58861669, 49277759, 54514129,
      57181397, 1449869, 55297796, 2388390, 43474432,
      60659979, 43335248, 7078613, 57290647, 47557864,
      55832077, 1693270, 37997887, 50012635, 18402584,
      58052398, 60978303, 34236648, 58776783, 59019100,
      57188240, 52677404, 46159215, 772478, 7113421,
      57943136, 50667527, 22626154, 59067230, 59219933,
      29869111, 59682776, 11945782, 54513108, 24900157,
      38237082, 45284995, 54614242, 52573954, 46333846,
      42145439, 53607482, 32207093, 32689923, 56394972,
      39499333, 42769001, 53952783, 54073058, 39650460,
      16546907, 15940851, 8418395, 8452494, 40529081,
      42472221, 5150377, 50216170, 35134268, 33655245,
      55236300, 5726494, 42236955, 36609879, 4831711,
      2494547, 21993560, 37149352, 61325681, 44871842,
      61830338, 58355348, 29255527, 58194257, 35351757]

    # Attempts to sync two fields from Zuora for the accounts we credited
    # as part of the Munich project: billed_on, and balance_in_cents. These
    # fields would be updated as part of the payment_processed webhook,
    # but we don't receive that webhook when the entire invoice is covered
    # by a credit balance in Zuora.
    def perform
      return unless GitHub.flipper[:munich_sync_credit_balances].enabled?

      affected_accounts.find_each do |account|
        next if account.invoiced?

        begin
          update_account_to_be_in_sync_with_zuora(account)
        rescue *(SKIPPABLE_ERRORS << AccountWithSponsorshipsError)
          Failbot.report(MunichCreditGeneralError.new(account))
          GitHub.dogstats.increment("billing.munich_credits_error", tags: ["reason:exception"])
        end
      end
    end

    private

    # Internal: Retrieve the affected accounts and their plan_subscription
    # records
    #
    # Returns ActiveRecord::Relation
    def affected_accounts
      User
        .includes(:plan_subscription)
        .where(users: { id: AFFECTED_ACCOUNTS })
        .where("users.billed_on <= ?", GitHub::Billing.today)
    end


    # Internal: Retrieve the relevant fields from Zuora and update
    # our local records
    #
    # account -- An Organization record
    #
    # Returns nothing
    # Raises AccountWithSponsorshipsError if the account has an active
    # sponsorship -- we need to handle these cases manually.
    def update_account_to_be_in_sync_with_zuora(account)
      if account.active_subscription_items.for_sponsors_tiers.present?
        raise AccountWithSponsorshipsError.new(account)
      end

      plan_subscription = account.plan_subscription
      return unless plan_subscription

      update_balance_in_cents(plan_subscription)
      update_billed_on(account)
    end

    # Internal: Update the plan_subscription.balance_in_cents
    # to match what is found on the associated Zuora account.
    #
    # plan_subscription - the PlanSubscription record associated
    # with the affected account
    #
    # Returns nothing
    def update_balance_in_cents(plan_subscription)
      zuora_account = plan_subscription.zuora_account
      zuora_balance = zuora_account && zuora_account["Balance"]

      return unless zuora_balance

      zuora_balance_in_cents = zuora_balance * 100
      return if zuora_balance_in_cents == plan_subscription.balance_in_cents

      ::Billing::PlanSubscription.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        if plan_subscription.update(balance_in_cents: zuora_balance_in_cents)
          GitHub.dogstats.increment("billing.munich_credits_balance_in_cents_updated")
        else
          GitHub.dogstats.increment("billing.munich_credits_error", tags: ["reason:balance_not_updated"])
        end
      end
    end

    # Internal: Update the user.billed_on
    # to match what is found on the associated Zuora subscription
    #
    # account -- an Organization record
    #
    # Returns nothing
    def update_billed_on(account)
      plan_subscription = account.plan_subscription
      zuora_subscription = plan_subscription.zuora_subscription

      return unless zuora_subscription

      # If the charged_through_date is in the past, and the same date as billed_on
      # there is no need to update, as we match up.
      charged_through_date = zuora_subscription.charged_through_date
      return if charged_through_date == plan_subscription.billed_on

      User.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        if account.update(billed_on: charged_through_date)
          GitHub.dogstats.increment("billing.munich_credits_billed_on_advanced")
        else
          GitHub.dogstats.increment("billing.munich_credits_error", tags: ["reason:billed_on_not_updated"])
        end
      end
    end
  end
end
