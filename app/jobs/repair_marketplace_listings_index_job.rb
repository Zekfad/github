# frozen_string_literal: true

RepairMarketplaceListingsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairMarketplaceListingsIndex)
