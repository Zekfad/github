# frozen_string_literal: true

class ProcessUnfurlsJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :process_unfurls

  discard_on ActiveRecord::RecordNotFound

  def perform(user, subject, urls, body_version)
    if subject.body_version != body_version
      GitHub.dogstats.increment("url_unfurls.body_version_mismatch", tags: ["object_type:#{subject.class.name}"])
      return
    end

    GitHub::UnfurlProcessor.process_urls(user, subject, urls, body_version)
  end
end
