# frozen_string_literal: true

class SpokesEvacuateNetworksNonvotingJob < ApplicationJob
  queue_as :dgit_schedulers
  schedule interval: 1.minute
  locked_by timeout: 10.minutes, key: DEFAULT_LOCK_PROC

  areas_of_responsibility :dgit

  def perform
    Failbot.push app: "github-dgit"

    return if GitHub.flipper[:dgit_disable_network_evacuation].enabled?

    SlowQueryLogger.disabled do
      start = Time.now
      GitHub::DGit::Maintenance.run_network_evacuation_nonvoting
      GitHub.stats.timing("dgit.evacuate-networks-nonvoting", Time.now-start) if GitHub.enterprise?
    end
  end
end
