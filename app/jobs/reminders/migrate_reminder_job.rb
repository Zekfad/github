# frozen_string_literal: true

module Reminders
  class MigrateReminderJob < MigrateJob
    def notify_user(reason:, tip:)
      return unless notify_user_of_problem?

      pull_reminders = PullRemindersClient.new(organization)
      pull_reminders.send_team_reminder_failure(data["id"], reason: reason, tip: tip)
    end

    # Unless specified otherwise, we notify the user when there is a problem.
    #
    # To disable user notifications, pass a third argument like this:
    # perform_later(some_org, some_data, notify_user_of_problem: false)
    def notify_user_of_problem?
      return true unless arguments.length == 3
      return true unless arguments.last.is_a?(Hash)

      arguments.last[:notify_user_of_problem]
    end

    def organization
      arguments.first
    end

    def data
      arguments.second
    end

    def options
      arguments[2] || {}
    end

    def get_suggested_teams(repository_ids)
      team_ids_per_repo = Array(repository_ids).map do |repository_id|
        ActiveRecord::Base.connected_to(role: :reading) do
          Ability.teams_direct_on_repos(repo_id: repository_id).pluck(:actor_id)
        end
      end

      team_ids_with_access_to_every_repo = team_ids_per_repo.inject(&:&)
      Team.closed.where(id: team_ids_with_access_to_every_repo).limit(25)
    end

    def send_team_warnings
      return false if options[:ignore_team_warnings]
      return false if data["team_ids"].length == 1

      client = PullRemindersClient.new(organization)
      suggested_teams = get_suggested_teams(data["repository_ids"])

      if suggested_teams.present?
        client.send_team_reminder_failure(
          data["id"],
          reason: "team select",
          tip: suggested_teams.map { |team| [team.id, team.name] }
        )
      else
        team_filter_description = data["team_ids"].length.zero? ? "isn't filtered to a team" : "is filtered to #{data["team_ids"].length} teams"
        client.send_team_reminder_failure(
          data["id"],
          reason: "⚠️  After migrating, this reminder can only be managed by organization owners",
          tip: <<~TIP
            Reminders that are filtered to a single team can be managed by anyone who can manage that team. Otherwise, an organization owner is required to make changes to the reminder.

            Right now, this reminder #{team_filter_description}.
          TIP
        )
      end

      true
    end

    def perform(organization, data, options = {})
      return if send_team_warnings

      validate_data(data)

      slack_workspace = fetch_slack_workspace(organization, data["slack_workspace_id"])
      reminder = Reminder.new(remindable: organization, slack_workspace: slack_workspace, ignore_draft_prs: false)
      reminder.time_zone_name         = derive_time_zone(data["timezone"])
      reminder.slack_channel          = data["slack_channel_name"]
      reminder.slack_channel_id       = data["slack_channel_id"]
      reminder.min_age                = data["min_age"]
      reminder.min_staleness          = data["min_staleness"]
      reminder.required_labels        = data["filter_labels"]
      reminder.ignored_labels         = data["ignore_labels"]
      reminder.ignored_terms          = data["ignore_terms"]
      reminder.require_review_request = data["require_review_request"]
      reminder.include_unassigned_prs = data["include_unassigned_prs"]
      reminder.include_reviewed_prs   = data["include_reviewed_prs"]
      reminder.needed_reviews         = data["needed_reviews"] || 0

      reminder.delivery_times_attributes = derive_delivery_times(data)

      reminder.teams = organization.teams.where(id: data["team_ids"])
      reminder.tracked_repository_ids = data["repository_ids"]

      if reminder.save
        begin
          mark_as_migrated(data, reminder)
          reminder
        rescue
          reminder.destroy!
          raise Error.new("a problem occurred while marking the reminder as migrated in Pull Reminders", code: "unable-to-mark-as-migrated")
        end
      else
        reason = reminder.errors.full_messages.to_sentence
        code = reason.include?("No repositories visible") ? "no-repositories-visible" : "invalid-record"

        raise Error.new(reason, code: code, tip: build_tip(reminder))
      end
    end

    def build_tip(reminder)
      if reminder.errors.full_messages.to_sentence.include?("No repositories visible")
        tips = []

        if reminder.teams.present?
          teams_with_no_repositories = reminder.teams.select do |team|
            team.direct_or_inherited_repo_ids(affiliation: :all)
          end

          if teams_with_no_repositories.present?
            team_links = teams_with_no_repositories.map { |team| team_link(team) }
            tips << "The #{team_links.to_sentence} #{"team".pluralize(team_links.count)} #{"has".pluralize(team_links.count)} doesn't have access to any repositories"
          end

          team_links = reminder.teams.map { |team| team_link(team) }
          tips << "Check to make sure the #{team_links.to_sentence} #{"team".pluralize(team_links.count)} #{"has".pluralize(team_links.count)} permission to your repositories (<#{GitHub.help_url}/github/setting-up-and-managing-organizations-and-teams/managing-team-access-to-an-organization-repository|learn more>)"
        end

        if reminder.tracked_repository_ids.present?
          tips << "Edit the reminder to change the selected repositories: #{reminder.tracked_repository_ids.inspect}"
        end

        unless reminder.integration_installation.installed_on_all_repositories?
          installation_link = "<#{settings_org_installation_url(reminder.remindable, reminder.integration_installation)}|Slack integration>"
          tips << "Check to make sure that #{installation_link} has permission your repositories"
        end

        <<~MESSAGE
          Here are some things to look into:
          - #{tips.join("\n- ")}
        MESSAGE
      end
    end

    def team_link(team)
      "<#{team_repositories_url(team.organization, team)}|#{team.name}>"
    end

    def validate_data(data)
      unless channel_reachable?(data)
        raise Error.new("@github isn't in this channel", code: "channel-unreachable", tip: "You can invite @github to this channel like this: `/invite @github`")
      end

      invalid_times = data["times"].reject { |time| ReminderDeliveryTime.times.keys.include?(time) }
      if invalid_times.present?
        raise Error.new("Unexpected delivery time: #{invalid_times.map(&:inspect).join(", ")}", code: "invalid-delivery-time")
      end

      if data["required_terms"].present?
        raise Error.new("This reminder uses _required terms_, which are no longer supported", code: "unsupported-feature")
      end

      if derive_time_zone(data["timezone"]).nil?
        raise Error.new("Unexpected time zone: #{data["timezone"].inspect}", code: "invalid-timezone")
      end
    end

    def channel_reachable?(data)
      channel_status = SlackApi.channel_status(
        data["slack_channel_id"],
        workspace_id: data["slack_workspace_id"],
      )

      channel_status == "found"
    end

    def derive_event_subscriptions(data)
      attributes = []
      attributes << { event_type: :pull_request_opened } if data["rtm_opened"]
      attributes << { event_type: :pull_request_merged } if data["rtm_merged"]

      if data["rtm_labeled"]
        attributes << {
          event_type: :pull_request_labeled,
          options: data["rtm_labeled_labels"] || "",
        }
      end

      attributes
    end

    def mark_as_migrated(data, reminder)
      url = if reminder.teams.size == 1
        team_reminder_url(reminder.remindable, reminder.teams.first, reminder)
      else
        org_reminder_url(reminder.remindable, reminder)
      end

      pull_reminders = PullRemindersClient.new(reminder.remindable)
      pull_reminders.mark_team_reminder_as_migrated(data["id"], migrated_url: url)
    end
  end
end
