# frozen_string_literal: true

module Reminders
  class MigratePersonalReminderJob < MigrateJob
    def perform(organization, workspace_id:, username:, ignore_missing: false)
      return unless uses_pull_reminders?(organization)

      user = User.find_by!(login: username)
      if PersonalReminder.where(remindable: organization, user: user).exists?
        raise Error.new(
          "You already have a personal reminder for @#{organization}",
          tip: "To stop this receiving messages from Pull Reminders, <https://pullreminders.com/installs/#{organization.id}/me|edit the reminder>.",
          code: "personal-reminder-already-exists"
        )
      end

      pull_reminders_client = PullRemindersClient.new(organization)
      data = begin
        pull_reminders_client.personal_reminder(user.id)
      rescue PullRemindersClient::Error => e
        if e.message.include?("404 Not Found") && ignore_missing
          return
        else
          raise
        end
      end

      validate(data)
      reminder = create_from(data)
      if reminder.persisted?
        begin
          pull_reminders_client.mark_personal_reminder_as_migrated(data["github_user_id"], migrated_url: personal_reminder_url(organization))
          reminder
        rescue PullRemindersClient::Error
          reminder.destroy!
          raise Error.new("a problem occurred while marking the reminder as migrated in Pull Reminders", code: "unable-to-mark-as-migrated")
        end
      else
        reason = reminder.errors.full_messages.to_sentence
        code = reason.include?("Slack workspace can't be blank") ? "need-to-authorize-slack" : "invalid-record"

        raise Error.new(reason, code: code)
      end
    end

    def create_from(data)
      organization = arguments.first
      slack_workspace = fetch_slack_workspace(organization, data["slack_workspace_id"])

      reminder = PersonalReminder.new(remindable: organization, user_id: data["github_user_id"], slack_workspace: slack_workspace)
      reminder.include_review_requests        = data["review_request_reminders"]
      reminder.include_team_review_requests   = data["team_review_request_reminders"]
      reminder.time_zone_name                 = derive_time_zone(data["timezone"])
      reminder.delivery_times_attributes      = derive_delivery_times(data)
      reminder.event_subscriptions_attributes = derive_event_subscriptions(data)
      reminder.save
      reminder
    end

    def validate(data)
      if data["filter_labels"].present?
        raise Error.new(
          "This reminder uses *Required labels*, which are no longer supported",
          tip: "Remove them from the reminder and try migrating again.",
          code: "unsupported-feature"
        )
      end

      if data["ignore_labels"].present?
        raise Error.new(
          "This reminder uses *Ignored labels*, which are no longer supported",
          tip: "Remove them from the reminder and try migrating again.",
          code: "unsupported-feature"
        )
      end

      if data["ignore_terms"].present? && data["ignore_terms"] != "WIP"
        raise Error.new(
          "This reminder uses *Ignored terms*, which are no longer supported",
          tip: "Remove them from the reminder and try migrating again.",
          code: "unsupported-feature"
        )
      end

      if derive_time_zone(data["timezone"]).nil?
        raise Error.new("Unexpected time zone: #{data["timezone"].inspect}", code: "invalid-timezone")
      end
    end

    def username
      arguments.last.fetch(:username)
    end

    def organization
      arguments.first
    end

    def notify_user(reason:, tip:)
      user = User.find_by!(login: username)

      pull_reminders = PullRemindersClient.new(organization)
      pull_reminders.send_personal_reminder_failure(user.id, reason: reason, tip: tip)
    end

    private


    def derive_event_subscriptions(data)
      attributes = []

      attributes << { event_type: :review_request } if data["review_request_alerts"]
      attributes << { event_type: :review_submission } if data["review_alerts"]
      attributes << { event_type: :team_review_request } if data["team_review_request_alerts"]
      attributes << { event_type: :comment } if data["comment_alerts"]
      attributes << { event_type: :mention } if data["mention_alerts"]
      attributes << { event_type: :merge_conflict } if data["merge_conflict_alerts"]
      attributes << { event_type: :comment_reply } if data["reply_alerts"]
      attributes << { event_type: :assignment } if data["assignee_alerts"]
      attributes << { event_type: :pull_request_merged } if data["merge_alerts"]

      if data["build_alerts"]
        attributes << {
          event_type: :check_failure,
          options: data["subscribed_checks"] || ""
        }
      end

      attributes
    end

    def uses_pull_reminders?(organization)
      Apps::Internal.integration(:pull_panda).installations.where(target: organization).exists?
    end
  end
end
