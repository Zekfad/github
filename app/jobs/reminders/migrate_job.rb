# frozen_string_literal: true

module Reminders
  class MigrateJob < ApplicationJob
    areas_of_responsibility :ce_extensibility
    queue_as :reminders

    def log_migration_error(error)
      return unless GitHub.flipper[:scheduled_reminders_verbose_errors].enabled?

      GitHub::Logger.log({
        organization_id: organization&.id,
        migration_error_message: error.message,
        migration_error_code: error.code
      })
    end

    rescue_from StandardError do |exception|
      if exception.is_a?(MigrateJob::Error)
        GitHub.dogstats.increment("reminders.migration-error", tags: ["reason:#{exception.code}"])
        log_migration_error(exception)
        notify_user(reason: exception.message, tip: exception.tip)
      else
        GitHub.dogstats.increment("reminders.migration-error", tags: ["reason:unexpected-error"])
        notify_user(
          reason: "Something went wrong",
          tip: "We've been notified of the error but if the problem persists, reach out to support to let us know."
        )

        raise exception
      end
    end

    class Error < StandardError
      attr_reader :code, :tip
      def initialize(message, code:, tip: nil)
        @tip = tip
        @code = code

        super(message)
      end
    end

    def perform(*)
      raise NotImplementError("Only subclasses of #{self.class} should be performed")
    end

    include Rails.application.routes.url_helpers
    def default_url_options
      uri = URI.parse(GitHub.url)
      {host: uri.host, protocol: uri.scheme}
    end

    def derive_time_zone(pull_reminders_time_zone)
      ReminderScheduling.timezone_for(pull_reminders_time_zone)&.name
    end

    def fetch_slack_workspace(organization, slack_id)
      slack_workspace = ReminderSlackWorkspace.find_by(remindable: organization, slack_id: slack_id)

      if slack_workspace.nil?
        raise Error.new(
          "@#{organization} isn't connected to this Slack workspace",
          tip: "Try <#{authorize_reminder_slack_workspace_url(organization)}|connecting your organization> to Slack and trying the migration again.",
          code: "missing-workspace"
        )
      else
        slack_workspace
      end
    end

    def organization
      arguments.first
    end

    # Extracts delivery times from Pull Reminders data.
    #
    # @param [Hash] Team reminder data from Pull Reminders API
    #
    # @return [Array] array of hashes with "day" and "time" keys.
    def derive_delivery_times(data)
      days = []
      days << "Monday"    if data["mon"]
      days << "Tuesday"   if data["tue"]
      days << "Wednesday" if data["wed"]
      days << "Thursday"  if data["thu"]
      days << "Friday"    if data["fri"]
      days << "Saturday"  if data["sat"]
      days << "Sunday"    if data["sun"]

      days.flat_map do |day|
        data["times"].map do |time|
          { "day" => day, "time" => time }
        end
      end
    end

    def notify_user(reason:, tip:)
      raise NotImplementedError
    end
  end
end
