# frozen_string_literal: true

SyncOrganizationDefaultRepositoryPermissionJob = LegacyApplicationJob.wrap(GitHub::Jobs::SyncOrganizationDefaultRepositoryPermission)
