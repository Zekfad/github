# frozen_string_literal: true

DeleteUnconfirmedAccountRecoveryTokensJob = LegacyApplicationJob.wrap(GitHub::Jobs::DeleteUnconfirmedAccountRecoveryTokens)
