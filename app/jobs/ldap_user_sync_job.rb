# frozen_string_literal: true

LdapUserSyncJob = LegacyApplicationJob.wrap(GitHub::Jobs::LdapUserSync)
