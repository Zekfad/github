# frozen_string_literal: true

GitbackupsSweeperJob = LegacyApplicationJob.wrap(GitHub::Jobs::GitbackupsSweeper)
