# frozen_string_literal: true

NewsletterPreviewJob = LegacyApplicationJob.wrap(GitHub::Jobs::NewsletterPreview)
