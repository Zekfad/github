# frozen_string_literal: true

RepairRepositoryActionsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairRepositoryActionsIndex)
