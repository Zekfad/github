# frozen_string_literal: true
require "grpc"
require "github/launch_client/deployer"

class CancelActiveActionsWorkflowsJob < ApplicationJob
  map_to_service :actions_experience

  queue_as :actions

  def perform(user:)
    return unless GitHub.actions_enabled?

    ActiveRecord::Base.connected_to(role: :reading) do
      user.repositories.find_each do |repo|
        cancel_workflows(user, repo)
      end
    end
  end

  def cancel_workflows(user, repo)
    if repo.actions_app_installed?
      GrpcHelper.rescue_from_grpc_errors("Actions") do
        GitHub::LaunchClient::Deployer.workflow_cancel_all(repo)
      end
    end
  end
end
