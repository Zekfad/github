# frozen_string_literal: true

require "codespaces"

class CodespacesDeprovisionPlanJob < ApplicationJob
  queue_as :codespaces
  locked_by timeout: 1.minute, key: DEFAULT_LOCK_PROC

  retry_on Codespaces::Client::BadResponseError, wait: :exponentially_longer, attempts: 5 # will retry 4 times over 6 minutes

  def perform(vscs_plan_id:, user_id:, vscs_target:, resource_provider:)
    Codespaces::ErrorReporter.push(
      codespace_vscs_plan_id: vscs_plan_id,
      user_id: user_id,
    )

    client = Codespaces::ArmClient.new(resource_provider: resource_provider)
    client.delete_plan(vscs_target: vscs_target, plan_id: vscs_plan_id)
  end
end
