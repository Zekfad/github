# frozen_string_literal: true

require "codespaces"

class CodespacesFetchBillingStorageAccountNamesJob < ApplicationJob
  areas_of_responsibility :codespaces
  queue_as :codespaces
  locked_by timeout: 1.minute, key: DEFAULT_LOCK_PROC
  schedule interval: 5.minutes, condition: -> { !GitHub.enterprise? }

  def perform
    Codespaces::ArmClient.new(resource_provider: nil).list_storage_account_names.each_key do |azure_storage_account_name|
      CodespacesFetchBillingMessagesJob.perform_later(azure_storage_account_name: azure_storage_account_name)
    end
  end
end
