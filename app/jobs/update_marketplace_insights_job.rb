# frozen_string_literal: true

UpdateMarketplaceInsightsJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateMarketplaceInsights)
