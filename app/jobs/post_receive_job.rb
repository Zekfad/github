# frozen_string_literal: true

PostReceiveJob = LegacyApplicationJob.wrap(GitHub::Jobs::PostReceive)
