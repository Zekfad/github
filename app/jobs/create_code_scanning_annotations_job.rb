# frozen_string_literal: true

class CreateCodeScanningAnnotationsJob < ApplicationJob
  class TurboscanError < StandardError; end

  queue_as :code_scanning

  retry_on ActiveRecord::RecordNotFound, wait: :exponentially_longer

  retry_on TurboscanError, wait: :exponentially_longer do |job, error|
    Failbot.report(error, app: "github-turboscan-client", check_run_id: job.check_run_id)
    job.mark_check_run_timed_out
  end

  retry_on GitHub::Restraint::UnableToLock, wait: 1.minute, attempts: 10 do |job, error|
    Failbot.report(error, app: "github-turboscan-client", check_run_id: job.check_run_id)
    job.mark_check_run_timed_out
  end

  BATCH_SIZE = 25
  ALERT_LIMIT = 250

  def perform(*args)
    return unless base_commit_oid.present?

    lock! do
      check_run.update(status: "in_progress")
      new_alerts, fixed_alerts, errors = load_alert_diff
      create_annotations(new_alerts)
      check_run.update_for_code_scanning_diff!(new_alerts, fixed_alerts, base_ref_name, errors)
    end
  end

  def mark_check_run_timed_out
    check_run.update(conclusion: "timed_out")
  end

  def check_run_id
    arguments.first[:check_run_id]
  end

  private

  def lock!
    restraint = GitHub::Restraint.new
    lock_key = "#{self.class.name}:#{check_run.id}"
    max_concurrent_jobs = 1
    lock_ttl = 5.minutes
    restraint.lock!(lock_key, max_concurrent_jobs, lock_ttl) do
      yield
    end
  end

  def base_commit_oid
    if pull_request.present?
      pull_request.base_sha
    else
      check_run.repository.default_branch_ref&.target_oid
    end
  end

  def head_commit_oid
    check_run.head_sha
  end

  def base_ref_name
    return @base_ref_name if defined? @base_ref_name
    @base_ref_name = check_run.repository.refs.find(pull_request&.base_ref)&.qualified_name
  end

  def pull_request
    return @pull_request if defined? @pull_request

    @pull_request = ActiveRecord::Base.connected_to(role: :reading) do
      check_run.check_suite.matching_pull_requests&.first
    end
  end

  def check_run
    @check_run ||= ActiveRecord::Base.connected_to(role: :reading) do
      CheckRun.find(check_run_id)
    end
  end

  def load_alert_diff
    response = GitHub::Turboscan.diff(
      repository_id: check_run.repository.id,
      base_commit_oid: base_commit_oid,
      head_commit_oid: head_commit_oid,
      tool: check_run.code_scanning_tool_name,
      limit: ALERT_LIMIT,
    )

    if response.nil? || response.data.nil? || response.error.present?
      raise TurboscanError, "Turboscan diff endpoint failed for annotations job"
    end

    new_alerts = response.data.new_alerts.select { |alert|
      alert.resolution == :NO_RESOLUTION
    }
    fixed_alerts = response.data.fixed_alerts
    analyses = response.data.analyses
    errors = []

    # TODO: If missingAfter or missingBefore are not empty, we might want to ask Turboscan for details.
    #       It would be best to do it here, so that the code in the checkrun does not depend on Turboscan
    # Note: If an After is missing, we might want to flag it, but it is typically not problematic
    if base_commit_oid == head_commit_oid
        # Ran on push we will not get a valid Diff
        # TODO: Shall we skip the call to turboscan altogether?
    elsif analyses&.missing_before&.present?
      errors << "Missing analysis for base commit #{base_commit_oid}"
      errors << "This usually happens when you enable code scanning for the first time, or if the build failed. " +
                "Since there is no analysis in the base branch, we do not know if there are any new alerts in this commit. " +
                "If you enabled analysis on push, you should check for alerts for this branch in the security tab. "
    elsif analyses&.included&.empty?
      errors << "Missing analyses for commits #{base_commit_oid} and #{head_commit_oid}"
    end

    return new_alerts, fixed_alerts, errors
  end

  def create_annotations(new_alerts)
    return if new_alerts.empty?

    annotations_by_number = ActiveRecord::Base.connected_to(role: :reading) do
      check_run.annotations.joins(:code_scanning_alert).group_by do |annotation|
        annotation.code_scanning_alert.alert_number
      end
    end

    new_alerts.each_slice(BATCH_SIZE) do |alerts_slice|
      CheckAnnotation.throttle_writes_with_retry(max_retry_count: 5) do
        alerts_slice.each do |alert|
          annotations = annotations_by_number[alert.number]
          next if annotations.present? && annotation_exists?(annotations, alert)
          build_annotation(alert)
        end
      end
    end
  end

  # Does an annotation already exist for the given alert?
  #
  # Return true if an annotation already has been created for the alert number
  # and location, false otherwise.
  def annotation_exists?(annotations, alert)
    annotations.any? do |annotation|
      annotation.code_scanning_alert.alert_number == alert.number &&
        annotation.path == alert.location.file_path &&
        annotation.start_line == alert.location.start_line &&
        annotation.end_line == alert.location.end_line &&
        annotation.start_column == alert.location.start_column &&
        annotation.end_column == alert.location.end_column
    end
  end

  def build_annotation(alert)
    location = alert.location
    attributes = {
      path: location.file_path,
      annotation_level: level_for_severity(alert.rule_severity),
      message: alert.message_text,
      start_line: location.start_line,
      end_line: location.end_line,
      title: alert.rule_short_description,
      code_scanning_alert_attributes: {
        alert_number: alert.number,
        repository: check_run.repository,
        check_run: check_run,
      }
    }

    # Check annotations can only have start/end columns when the annotation
    # does not span multiple lines
    if location.start_line == location.end_line && location.start_column > 0
      attributes[:start_column] = location.start_column
      # Use start column when end column is missing
      attributes[:end_column] = location.end_column.zero? ? location.start_column : location.end_column
    end
    check_run.annotations.create!(attributes)
  end

  def level_for_severity(rule_severity)
    case rule_severity
    when :ERROR
      "failure"
    when :WARNING
      "warning"
    else
      "notice"
    end
  end
end
