# frozen_string_literal: true

class AuditLogGitEventExportJob < ApplicationJob
  areas_of_responsibility :audit_log
  queue_as :audit_log_export

  RETRYABLE_ERRORS = [
    Errno::ECONNRESET,
    Faraday::ConnectionFailed,
    Faraday::SSLError,
    Faraday::TimeoutError,
    GitHub::Restraint::UnableToLock,
    Driftwood::ExportTwirpRequest::Error,
  ].freeze

  discard_on(StandardError) do |job, error|
    GitHub.dogstats.increment(
      "audit_log_git_export.job_error",
      tags: ["exception:#{error.class.name.parameterize}"],
    )
    Failbot.report(error)
  end

  RETRYABLE_ERRORS.each do |retryable|
    retry_on(retryable) do |job, error|
      GitHub.dogstats.increment(
        "audit_log_git_export.job_error",
        tags: ["exception:#{error.class.name.parameterize}"],
      )
      Failbot.report(error)
    end
  end

  def perform(export:)
    lock(create_lock_key(export)) do
      exec_job(export: export)
    end
  end

  def stats_tags
    []
  end

  # Internal: Use a GitHub::Restraint to limit the number of concurrent jobs per subject
  def lock(key, &block)
    restraint = GitHub::Restraint.new
    restraint.lock!(
      lock_key,
      _max_concurrency =  GitHub.audit_git_export_max_concurrent_jobs_subject,
      _ttl = GitHub.audit_git_export_timeout,
      &block
    )
  end

  def exec_job(export:)
    status = JobStatus.find(export.token)
    status.track do
      export.process
    end
  end

  def create_lock_key(export)
    type = ""
    case export.subject
    when Organization
      type = "org"
    when Business
      type = "business"
    end
    "audit-log-git-export-#{type}-#{export.subject_id}"
  end
end
