# frozen_string_literal: true

class UpdateInvitationRepoPermissionsJob < ApplicationJob
  queue_as :update_invitation_repo_permissions

  def perform(invitation, action:, setter:)
    invitation.set_permissions(action, setter)
  end
end
