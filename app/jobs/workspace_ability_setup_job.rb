# frozen_string_literal: true

# This job sets up the correct abilities on a Workspace repository by copying
# the admin abilities on its parent repository and injecting them directly into
# the Ability table for performance.
#
# This job is used on creation of a workspace repository and after transfering
# ownership of a repository & workspaces to restore the correct abilities
class WorkspaceAbilitySetupJob < ApplicationJob
  areas_of_responsibility :repositories, :security_advisories

  queue_as :workspace_repository_abilities

  def perform(workspace_repository_id)
    workspace_repository = Repository.find(workspace_repository_id)

    return unless workspace_repository.advisory_workspace?

    Ability.throttle do
      workspace_repository.throttle do
        RepositoryAdvisory::WorkspaceAbilityManager.new(workspace_repository).perform
      end
    end
  end
end
