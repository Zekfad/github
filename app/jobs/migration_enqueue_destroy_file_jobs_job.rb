# frozen_string_literal: true

MigrationEnqueueDestroyFileJobsJob = LegacyApplicationJob.wrap(GitHub::Jobs::MigrationEnqueueDestroyFileJobs)
