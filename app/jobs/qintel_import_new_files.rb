# frozen_string_literal: true

class QintelImportNewFiles < ApplicationJob
  queue_as :qintel_import_new_files

  schedule interval: 1.minute, condition: -> { !GitHub.enterprise? }

  locked_by timeout: 10.minutes, key: ->(job) {
    "qintel_import_file"
  }

  # Occasionally the qintel API will timeout attempting to get the list
  # of files. When this happens we can attempt to retry the job. We'll
  # retry 5 times, waiting 3 seconds between attempts before ultimately
  # logging the error to failbot. Retrying isn't urgent since we schedule
  # the job to run often. However its nice to keep the noise out of
  # haystack unless there is a real issue (which there likely is if the
  # retried jobs continue to timeout)
  retry_on GitHub::Qintel::TimeoutError

  # We've seen a few cases where things like DNS lookups will briefly fail
  # for one run throwing a request failed error. We can simply retry on this
  # error as well
  retry_on GitHub::Qintel::RequestFailed

  MAX_PARALLEL_IMPORTS = 10

  def perform
    report_stats

    return if slots_available_for_importing == 0
    return if unimported_file_uuids.empty?

    unimported_file_uuids.each_with_index do |uuid, idx|
      # No more parallel imports permitted
      return if idx >= slots_available_for_importing

      to_import = available_files.find do |file|
        file.uuid == uuid
      end

      QintelImportFile.perform_later(
        uuid: to_import.uuid,
        num_credential_pairs: to_import.raw[:email_count],
      )
    end
  end

  def report_stats
    GitHub.dogstats.gauge("qintel.files.all", available_file_uuids.count)
    GitHub.dogstats.gauge("qintel.files.imported", imported_file_uuids.count)
    GitHub.dogstats.gauge("qintel.files.semi_imported", semi_imported_files_count)
    GitHub.dogstats.gauge("qintel.files.unimported", unimported_file_uuids.count)
  end

  def unimported_file_uuids
    @unimported ||= available_file_uuids - imported_file_uuids
  end

  def available_file_uuids
    @imported ||= available_files.map(&:uuid)
  end

  def available_files
    @available_files ||= GitHub::Qintel::Catalog.fetch.credential_files.sort_by do |file|
      file.crack_count
    end
  end

  def semi_imported_files_count
    CompromisedPasswordDatasource.where(
      name: "qintel",
      import_finished_at: nil,
    ).count
  end

  def slots_available_for_importing
    @importing ||= MAX_PARALLEL_IMPORTS - CompromisedPasswordDatasource.where(
      import_finished_at: nil
    ).count
  end

  def imported_file_uuids
    @imported_file_uuids ||= CompromisedPasswordDatasource.where(
      name: "qintel",
    ).pluck(:version)
  end
end
