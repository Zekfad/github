# frozen_string_literal: true

SpokesFixZeroReplicaGistJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitFixZeroReplicaGist)
