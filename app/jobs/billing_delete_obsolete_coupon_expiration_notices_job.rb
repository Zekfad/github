# frozen_string_literal: true

class BillingDeleteObsoleteCouponExpirationNoticesJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  schedule interval: 24.hours, condition: -> { !GitHub.enterprise? }

  def perform
    user_ids_with_deleted_notices = []

    GitHub::SQL::Readonly.new(user_id_iterator.batches).each do |rows|
      user_ids = rows.flatten
      users = User.where(id: user_ids).includes(:coupon_redemptions)

      users_with_obsolete_notices =
        users.reject do |user|
          user.has_an_active_coupon? &&
            user.paid_plan? &&
            user.coupon_redemption.expires_this_billing_cycle?
        end

      users_with_obsolete_notices.each do |user|
        user.throttle_writes do
          user.delete_notice(:coupon_will_expire)
        end
        user_ids_with_deleted_notices << user.id
      end
    end

    GitHub.dogstats.gauge(
      "dashboard_notices.obsolete_coupon_expiration_notices_deleted",
      user_ids_with_deleted_notices.size,
    )

    user_ids_with_deleted_notices
  end

  private

  def user_id_iterator
    iterator = ApplicationRecord::Domain::Users.github_sql_batched(start: 0, limit: 1_000)
    iterator.add <<-SQL
          SELECT user_id
          FROM dashboard_notices
          WHERE notice_name = "coupon_will_expire"
          AND user_id > :last
          ORDER BY id
          LIMIT :limit
    SQL
    iterator
  end
end
