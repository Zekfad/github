# frozen_string_literal: true

RepairVulnerabilitiesIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairVulnerabilitiesIndex)
