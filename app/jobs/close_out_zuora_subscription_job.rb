# frozen_string_literal: true

class CloseOutZuoraSubscriptionJob < ApplicationJob
  queue_as :zuora

  RETRYABLE_ERRORS = [
    Faraday::ConnectionFailed,
  ].freeze

  RETRYABLE_ERRORS.each do |error|
    retry_on error
  end

  def perform(zuora_subscription_number:, plan_subscription: nil)
    ::Billing::CloseZuoraSubscription.perform(
      zuora_subscription_number: zuora_subscription_number,
      plan_subscription: plan_subscription,
    )
  end
end
