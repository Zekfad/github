# frozen_string_literal: true

class BraintreeTransactionsCsvJob < ApplicationJob
  areas_of_responsibility :gitcoin

  HEADER = %w(created_at transaction_id transaction_type transaction_amount customer_id is_braintree)

  queue_as :billing

  schedule interval: 24.hours, condition: -> { !GitHub.enterprise? }

  def perform(timestamp = nil)
    return unless GitHub.s3_uploads_enabled?

    s3_bucket = ENV["TRANSACTIONS_CSV_S3_BUCKET"] || GitHub.s3_configs[Rails.env]["transactions_csv_bucket"]

    if s3_bucket.nil?
      Rails.logger.warn("Can't create a CSV dump of transaction data for S3: no bucket name provided.")
      return
    end

    last_day = (timestamp.nil? ? Time.now : Time.at(timestamp)) - 24.hours
    create_csv_for_date(last_day, s3_bucket)
    create_monthly_rollups(s3_bucket)
  end

  def create_csv_for_date(timestamp, s3_bucket)
    day = timestamp.to_billing_date
    starts_at = day.to_datetime # From 00:00:00 on the day you ask
    ends_at = starts_at + 24.hours - 1.second # Through to the second before the next day begins

    # This looks like it abstracts pagination away from you, so it shouldn't matter if there's lots of results.
    search_results = Braintree::Transaction.search do |search|
      search.created_at.between(starts_at, ends_at)
    end

    # Include GitHub's stored transaction stream.
    gh_results = ::Billing::BillingTransaction.where("created_at BETWEEN ? AND ?", starts_at, ends_at)

    daily_key = "transactions-#{day.strftime('%Y%m%d')}.csv"
    GitHub::Billing::Reporting.create_and_upload_csv(daily_key, s3_bucket) do |csv|
      # header
      csv << HEADER
      search_results.each do |transaction|
        csv << braintree_transaction_to_csv(transaction)
      end

      gh_results.each do |transaction|
        csv << billing_transaction_to_csv(transaction)
      end
    end
  end

  # Look in the given S3 bucket under the given directory for daily files; if there are enough to generate a monthly rollup,
  # and the monthly rollup doesn't exist, generate it.
  #
  # subdir - which prefix to look under in the S3 bucket for daily and monthly files
  # s3_bucket - which S3 bucket to look in
  def create_monthly_rollups(s3_bucket)
    # Maintain monthly rollups.
    monthly_files = s3_client.list_objects(bucket: s3_bucket, prefix: "monthly").contents.map(&:key)
    daily_files = s3_client.list_objects(bucket: s3_bucket).contents.map(&:key) - monthly_files

    # Find monthly files that have enough daily data to be generated, but don't yet exist.
    days = daily_files.map { |key| key.gsub(/[^0-9]/, "") }.sort
    extant_months = monthly_files.map { |key| key.gsub(/[^0-9]/, "") }
    potential_months = days.map { |day| day[0..5] }.uniq # just grab the YYYYmm portion of the filenames, then remove dupes

    # Set difference.
    potential_months -= extant_months
    generate_months = []

    # For ungenerated months, check if we have all the data available,
    # and remember that month to generate a file for in the next step.
    potential_months.each do |month|
      generate_months << month if (days_of_month(month) - days == [])
    end

    # Finally, generate each eligible month's CSV file and upload to S3.
    generate_months.each do |month|
      Rails.logger.info("Generate monthly CSV transaction report in S3 for #{month}")
      generate_monthly_csv_file(month, s3_bucket)
    end
  end

  # Iterate through the daily files in the S3 bucket for the given month, and produce a rolled up monthly
  # file.
  #
  # We expect the files to be named like "transactions-YYYYmmdd.csv", and monthly file will be named
  # "monthly/transactions-YYYYmm.csv".
  #
  # prefix - what top-level subdirectory in the bucket to look for daily files under
  def generate_monthly_csv_file(month, s3_bucket)
    # What I'd like to do here is open an upload stream for the monthly file, then iterate
    # through the days and stream them to the new file in S3, thus avoiding building up a
    # potentially-big temp file locally. But I don't see direct support for such a thing in the S3
    # gem. So instead I have to build the monthly file's contents locally, then upload.
    month_key = "monthly/transactions-#{month}.csv"

    temp_monthly = Tempfile.new("transactions-csv-monthly")
    monthly_header = nil
    begin
      wrote_monthly_header = false

      days_of_month(month).each do |k|
        key = "transactions-#{k}.csv"

        # Download the daily file from S3 and append to the monthly one we're building locally.
        skipped_header = false

        # Retain the header from the first file, but only the first file.
        monthly_header = ""
        s3_client.get_object(bucket: s3_bucket, key: key) do |chunk|
          if skipped_header
            temp_monthly.write(chunk)
          else
            nl = chunk.index("\n")
            if nl.nil?
              monthly_header += chunk
              next
            end

            monthly_header += chunk[0..nl]
            unless wrote_monthly_header
              temp_monthly.write(monthly_header)
              wrote_monthly_header = true
            end

            temp_monthly.write(chunk[(nl+1)..-1])
            skipped_header = true
          end
        end
      end

      temp_monthly.close
      object = Aws::S3::Resource.new(client: s3_client).bucket(s3_bucket).object(month_key)
      object.put(
        body: File.read(temp_monthly.path),
        content_type: "text/csv",
      )
    ensure
      temp_monthly.close
      temp_monthly.unlink
    end

    month_key
  end

  # Convert a Braintree::Transaction to an array of strings for CSV output.
  def braintree_transaction_to_csv(transaction)
    [transaction.created_at.strftime("%Y-%m-%d %H:%M:%S"), transaction.id, transaction.type, transaction.amount, (transaction.customer_details.id || "na"), 1].map(&:to_s)
  end

  # Convert a Billing::BillingTransaction to an array of strings for CSV output.
  def billing_transaction_to_csv(transaction)
    [transaction.created_at.strftime("%Y-%m-%d %H:%M:%S"), transaction.transaction_id, (transaction.transaction_type || "na"), (transaction.amount_in_cents/100.00), (transaction.user.nil? ? "na" : transaction.user.braintree_customer_id), 0].map(&:to_s)
  end

  # Given a string representing a month in YYYYMM format, return each day of that month as
  # a YYYYMMDD string.
  def days_of_month(month)
    starts = DateTime.parse(month + "01")
    ends = starts + 1.month - 1.second

    days_of_month = []
    starts.upto(ends) { |dt| days_of_month << dt.strftime("%Y%m%d") }
    days_of_month
  end

  def s3_client
    GitHub.s3_primary_client
  end
end
