# frozen_string_literal: true

class CollectZuoraInvoiceJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :zuora

  # Attempts to collect the invoice for the given ID.
  # Because invoices are collected daily by Zuora, a failure to collect
  # in this job is not critical since it will be done by Zuora for us.
  #
  # zuora_account_id - The String representing the user's Zuora account ID
  # invoice_id - The String long format identifying the invoice in Zuora
  def perform(zuora_account_id, invoice_id)
    GitHub.zuorest_client.create_invoice_collect({
      accountKey: zuora_account_id,
      invoiceId: invoice_id,
    })
  rescue Zuorest::HttpError, Faraday::Error
    # If this API call fails for any reason, exception or error in Zuora/Gateway
    # the invoice will be collected on the next Payment Run and dun if necessary
  end
end
