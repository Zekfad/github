# frozen_string_literal: true

RebuildStorageUsageJob = LegacyApplicationJob.wrap(GitHub::Jobs::RebuildStorageUsage)
