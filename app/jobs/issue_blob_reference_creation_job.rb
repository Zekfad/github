# frozen_string_literal: true

# Handle any tasks that can be done out of the scope of a request
# after an IssueBlobReference is created.
class IssueBlobReferenceCreationJob < ApplicationJob
  queue_as :kubernetes_analytics

  retry_on_dirty_exit

  # Perform tasks that are useful after IssueBlobReference creation.
  #
  # Pushes some data about the blob ref creation to Octolytics, including
  # the creator and some plan info.
  #
  # issue_blob_reference_id - The id of the IssueBlobReference that was created.
  # creation_context        - Data about the Author of the IssueBlobReference (default: {}):
  #                             :user_login - The user's login String.
  #                             :user_id    - The user's id as a String.
  #
  # Returns nothing.
  def perform(issue_blob_reference_id, creation_context = {})
    return unless GitHub.octolytics_enabled?
    return if GitHub.enterprise?

    creation_context = creation_context.with_indifferent_access

    Failbot.push(issue_blob_reference_id: issue_blob_reference_id)

    issue_blob_reference = IssueBlobReference.find_by_id(issue_blob_reference_id)
    return unless issue_blob_reference

    GitHub.analytics.record([{
      event_type: "issue_blob_reference_create",
      timestamp: issue_blob_reference.issue.created_at,
      dimensions: {
        repo_id: issue_blob_reference.issue.repository.id.to_s,
        issue_id: issue_blob_reference.issue.id.to_s,
        blob_oid: issue_blob_reference.blob_oid,
        commit_oid: issue_blob_reference.commit_oid,
        filepath: issue_blob_reference.filepath,
        range_start: issue_blob_reference.range_start,
        range_end: issue_blob_reference.range_end,
      },
      context: {
        user_id: creation_context["user_id"],
        user_login: creation_context["user_login"],
      },
    }])
  end
end
