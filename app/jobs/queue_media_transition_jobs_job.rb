# frozen_string_literal: true

QueueMediaTransitionJobsJob = LegacyApplicationJob.wrap(GitHub::Jobs::QueueMediaTransitionJobs)
