# frozen_string_literal: true

class DeleteRepositoryRetryJob < ApplicationJob
  queue_as :repository_purge
  schedule interval: 1.hour
  retry_on_dirty_exit

  BATCH_SIZE = 50

  def perform
    Repository.schedule_failed_delete_jobs(BATCH_SIZE)
  end
end
