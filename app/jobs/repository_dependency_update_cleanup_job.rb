# frozen_string_literal: true

class RepositoryDependencyUpdateCleanupJob < ApplicationJob
  areas_of_responsibility :dependabot
  queue_as :dependabot

  # TODO: Cleanup updates from any trigger once we tuned our throughput enough
  #       to have an SLA in place for automated PR creation.
  def perform(repository_dependency_update_id)
    dependency_update = RepositoryDependencyUpdate.visible.
                                                   requested.
                                                   find_by(id: repository_dependency_update_id)

    return unless dependency_update && dependency_update.repository

    dependency_update.update!(error_title: RepositoryDependencyUpdate::DEFAULT_ERROR_TITLE,
                              error_body: RepositoryDependencyUpdate::DEFAULT_ERROR_BODY,
                              error_type: RepositoryDependencyUpdate::TIMEOUT_ERROR_TYPE,
                              state: :error)

    GitHub.dogstats.increment(
      "repository_dependency_update.cleaned_up",
      tags: ["trigger:#{dependency_update.trigger_type}",
             "installed:#{dependency_update.repository.dependabot_installed?}"]
    )

    GlobalInstrumenter.instrument("repository_dependency_update.cleaned_up", {
      repository_dependency_update: dependency_update,
      repository: dependency_update.repository,
    })
  end
end
