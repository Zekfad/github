# frozen_string_literal: true

SyncAssetUsageJob = LegacyApplicationJob.wrap(GitHub::Jobs::SyncAssetUsage)
