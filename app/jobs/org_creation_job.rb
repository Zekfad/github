# frozen_string_literal: true

OrgCreationJob = LegacyApplicationJob.wrap(GitHub::Jobs::OrgCreation)
