# frozen_string_literal: true

SyncRegistryUsageJob = LegacyApplicationJob.wrap(GitHub::Jobs::SyncRegistryUsage)
