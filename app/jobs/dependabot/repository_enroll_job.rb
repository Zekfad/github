# frozen_string_literal: true

# Finalizes the enrollment of a Repository in Dependabot by sending any outstanding
# updates to Dependabot for processing.
class Dependabot::RepositoryEnrollJob < ApplicationJob
  areas_of_responsibility :dependabot
  queue_as :dependabot

  def perform(repository_id)
    repository = Repository.find(repository_id)

    # Enrollment will generate a large volume of RepositoryDependencyUpdate
    # rows, so lets throttle for safety.
    RepositoryDependencyUpdate.throttle do
      RepositoryDependencyUpdate.request_for_repository(repository,
                                                        trigger: :install)
    end
  end
end
