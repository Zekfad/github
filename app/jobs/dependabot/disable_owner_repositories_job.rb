# frozen_string_literal: true

# Disables vulnerability updates on all repositories that have Dependabot installeed
class Dependabot::DisableOwnerRepositoriesJob < ApplicationJob

  areas_of_responsibility :dependabot
  queue_as :dependabot

  attr_reader :repository_owner_id, :actor_user_id

  def perform(repository_owner_id, actor_user_id)
    @repository_owner_id = repository_owner_id
    @actor_user_id = actor_user_id

    Repository.where(id: installed_repository_ids).each do |repo|
      repo.disable_vulnerability_updates(actor: actor)
    end
  end

  private

  def installed_repository_ids
    dependabot_installations.flat_map { |install| install.repository_ids }
  end

  def dependabot_installations
    IntegrationInstallation.where(integration_id: GitHub.dependabot_github_app.id,
                                  target_id: repository_owner_id)
  end

  def actor
    @actor ||= User.find(actor_user_id)
  end
end
