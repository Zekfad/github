# frozen_string_literal: true

class DelistSpammyActionFromMarketplaceJob < ApplicationJob
  areas_of_responsibility :marketplace

  queue_as :marketplace

  def perform(user:)
    if user&.spammy?
      RepositoryAction
        .joins(:repository)
        .where(state: :listed, repositories: { owner_id: user.id }).each do |action|
        action.delisted!
      end

      RepositoryAction
        .joins(:releases)
        .where(state: :listed, releases: { author_id: user.id }).each do |action|
        action.delisted!
      end
    end
  end
end
