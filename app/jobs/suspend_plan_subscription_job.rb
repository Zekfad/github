# frozen_string_literal: true

class SuspendPlanSubscriptionJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :zuora

  discard_on ActiveJob::DeserializationError

  RETRYABLE_ERRORS = [
    Errno::ECONNREFUSED,
    Errno::ECONNRESET,
    Errno::ETIMEDOUT,
    Faraday::ConnectionFailed,
    Faraday::TimeoutError,
    GitHub::Restraint::UnableToLock,
    Net::HTTPRequestTimeOut,
    Net::ReadTimeout,
    Zuorest::HttpError,
  ]

  RETRYABLE_ERRORS.each do |error|
    retry_on(error) do
      Failbot.report(error)
    end
  end

  locked_by timeout: 5.minutes, key: ApplicationJob::DEFAULT_LOCK_PROC

  def perform(plan_subscription)
    plan_subscription.suspend
  end
end
