# frozen_string_literal: true

class DisableRepositoryAccessJob < ApplicationJob
  areas_of_responsibility :stafftools

  queue_as :repository_access

  discard_on ActiveJob::DeserializationError do |job, error|
    Failbot.report(error)
  end

  locked_by timeout: 10.minutes, key: DEFAULT_LOCK_PROC

  before_enqueue do |job|
    repo = job.arguments[0]
    status = Stafftools::DisableRepositoryAccessStatus.new(repo)
    status.start_tracking_job
  end

  def self.status(repo)
    Stafftools::DisableRepositoryAccessStatus.new(repo).job_status
  end

  def perform(repo, reason, user, **opts)
    return if GitHub.flipper[:darkship_dmca_takedown_skip_for_perfomance_reason].enabled?
    take_down_status = Stafftools::DisableRepositoryAccessStatus.new(repo)
    take_down_status.track do
      repo.access.disable(reason, user, **opts)
    end
  end
end
