# frozen_string_literal: true

class CompleteSponsorsGoalJob < ApplicationJob
  queue_as :complete_sponsors_goal

  locked_by timeout: 1.hour, key: ->(job) { job.arguments[0].id }

  def perform(goal)
    return unless goal.can_complete?

    goal.complete!

    SponsorsMailer.goal_completed(goal: goal).deliver_later
  end
end
