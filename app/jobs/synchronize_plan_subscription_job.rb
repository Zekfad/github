# frozen_string_literal: true

SynchronizePlanSubscriptionJob = LegacyApplicationJob.wrap(GitHub::Jobs::SynchronizePlanSubscription)
