# frozen_string_literal: true

class OrgInsightsBackfillJob < ApplicationJob
  extend GitHub::HashLock

  queue_as :org_insights_backfill

  RETRYABLE_ERRORS = [
    ActiveRecord::ConnectionTimeoutError,
    ActiveRecord::QueryCanceled,
  ].freeze

  RETRYABLE_ERRORS.each do |error|
    retry_on(error) do |job, error|
      Failbot.report(error)
    end
  end

  def perform(org_id, metric_classname)
    metric_class = GitHub::OrgInsights::Metric[metric_classname]
    GitHub::OrgInsights::Backfill.new(org_id, metric_class).backfill
  rescue NameError => e
    Failbot.report(e)
  end
end
