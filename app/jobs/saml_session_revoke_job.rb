# frozen_string_literal: true

SamlSessionRevokeJob = LegacyApplicationJob.wrap(GitHub::Jobs::SamlSessionRevoke)
