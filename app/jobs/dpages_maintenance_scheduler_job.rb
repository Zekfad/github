# frozen_string_literal: true

DpagesMaintenanceSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::DpagesMaintenanceScheduler)
