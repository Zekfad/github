# frozen_string_literal: true

class ResumePlanSubscriptionJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :zuora

  discard_on ActiveJob::DeserializationError

  RETRYABLE_ERRORS = [
    Errno::ECONNREFUSED,
    Errno::ECONNRESET,
    Errno::ETIMEDOUT,
    Faraday::ConnectionFailed,
    Faraday::TimeoutError,
    GitHub::Restraint::UnableToLock,
    Net::HTTPRequestTimeOut,
    Net::ReadTimeout,
    Zuorest::HttpError,
  ]

  RETRYABLE_ERRORS.each do |error|
    retry_on(error) do
      Failbot.report(error)
    end
  end

  locked_by timeout: 5.minutes, key: -> (job) do
    plan_subscription = job.arguments.first

    "#{plan_subscription.class.name}#{plan_subscription.id}"
  end

  def perform(plan_subscription)
    plan_subscription.resume
  end
end
