# frozen_string_literal: true

MassInstallAutomaticIntegrationsJob = LegacyApplicationJob.wrap(GitHub::Jobs::MassInstallAutomaticIntegrations)
