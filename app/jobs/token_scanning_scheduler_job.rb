# frozen_string_literal: true

class TokenScanningSchedulerJob < ApplicationJob
  areas_of_responsibility :token_scanning
  queue_as :token_scanning_scheduler

  schedule interval: 5.minutes, scope: :global, condition: -> { GitHub.configuration_advanced_security_enabled? }

  # We want the job to be locked globally so we can only ever have one
  # instance of the job running. We don't want other instances of the job to be running on
  # the same set of records
  locked_by key: ->(job) { "token_scanning_scheduler" }, timeout: 5.minutes

  # Grace period for retrying scan of a repo based on peak retries with job timeouts
  RETRY_ELIGIBLE_TIME_THRESHOLD = 30.minutes

  def scan_concurrency_limit
    @scan_concurrency_limit ||= self.arguments.first || GitHub.configuration_secret_scanning_max_backfill_scans
  end

  def perform(custom_concurrency_limit = nil)
    processingCount = 0
    deletingCount = 0
    repository = nil
    repos_to_scan.each do |token_scan_status|
      #Conditionally throttle down or stop job creation in presence of Dark-shipping opt-out feature flag.
      if !GitHub.flipper["disable_token_scanning_scheduler"].enabled?
        update_repo(token_scan_status.repository, token_scan_status)
        processingCount += 1
      end
    end

    repos_to_delete.each do |token_scan_status|
      TokenScanningCleanupJob.perform_later(token_scan_status.repository_id)
      deletingCount += 1
    end
    measure_queue_size

    processingResult = processingCount == 0 ? "no_scheduling_performed" : "scheduling_done"
    deletingResult = deletingCount == 0 ? "no_deletions_performed" : "deleting_done"
    return [processingResult, deletingResult]
  end

  def update_repo(repository, token_scan_status)
    if repository && repository.token_scanning_enabled?
      previous_scan_state = token_scan_status.scan_state
      token_scan_status.update_queued_scan_state!
      record_queued_scan_states(previous_scan_state, token_scan_status.scan_state, token_scan_status.retry_count)
      unless token_scan_status.failed_retry_count_exceeded?
        # Materialize the repo scan job for execution
        RepositoryPrivateTokenScanningJob.perform_later(token_scan_status.repository_id)
      end
    else
      token_scan_status.update_failed_scan_state!(:non_qualifying_repo)
    end
  end

  # Gets a list of repos to queue for scanning
  # Requested or retry eligible repos are ordered by state to prioritise requested
  def repos_to_scan
    ActiveRecord::Base.connected_to(role: :reading) do
      TokenScanStatus.requested
      .or(TokenScanStatus.retry_eligible
      .older_than_threshold(RETRY_ELIGIBLE_TIME_THRESHOLD))
      .ordered_by_scan_state
      .limit(scan_concurrency_limit)
      .preload(:repository)
    end
  end

  # Gets a list of repos for which token scan status and results are to be deleted
  def repos_to_delete
    TokenScanStatus.non_qualifying_repo.limit(GitHub.configuration_secret_scanning_max_backfill_scans)
  end

  def measure_queue_size
    TokenScanStatus.group(:scan_state).count.each do |key, value|
      GitHub.dogstats.gauge("token_scan_scheduler.queue", value, tags:
        [
          "scan_state:#{key}",
        ]
      )
    end
  end

  def record_queued_scan_states(previous_scan_state, current_scan_state, retry_count)
    GitHub.dogstats.increment("token_scan_scheduler.status.change", tags: [
      "previous_scan_state:#{previous_scan_state}",
      "current_scan_state:#{current_scan_state}",
      "retry_count:#{retry_count}",
      ])
  end
end
