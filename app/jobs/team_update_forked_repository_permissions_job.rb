# frozen_string_literal: true

TeamUpdateForkedRepositoryPermissionsJob = LegacyApplicationJob.wrap(GitHub::Jobs::TeamUpdateForkedRepositoryPermissions)
