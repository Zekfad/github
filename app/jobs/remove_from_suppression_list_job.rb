# frozen_string_literal: true

RemoveFromSuppressionListJob = LegacyApplicationJob.wrap(GitHub::Jobs::RemoveFromSuppressionList)
