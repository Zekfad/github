# frozen_string_literal: true

UpdateOrganizationTeamPrivacyJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateOrganizationTeamPrivacy)
