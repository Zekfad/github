# frozen_string_literal: true

DestroyTeamDependantsJob = LegacyApplicationJob.wrap(GitHub::Jobs::DestroyTeamDependants)
