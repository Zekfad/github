# frozen_string_literal: true

class RemoveOrgMemberRepositoryStarsJob < RemoveOrgMemberDataJob
  areas_of_responsibility :orgs, :repositories

  queue_as :remove_org_member_data

  # For a given @user, goes through private org repositories that have stars
  # and are not pullable by the user, then removes those stars
  #
  # Saves a restorable archive of each cleared record before clearing.
  # options is used by the base class it is a hash of { "organization_id" => value, "user_id" => value }
  def perform(options)
    org.org_repositories.private_scope.find_in_batches do |batch|
      org_repos = inaccessible_org_repos(batch)
      repo_to_clear_ids = org_repos.collect(&:id)

      starred_repos_to_remove = user.starred_repositories
                                    .where(id: repo_to_clear_ids)
                                    .all

      restorable.save_repository_stars(starred_repos_to_remove)
      Star.throttle do
        starred_repos_to_remove.each { |repo| @user.unstar(repo) }
      end
    end

    restorable.save_repository_stars_complete
  end
end
