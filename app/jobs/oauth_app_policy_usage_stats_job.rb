# frozen_string_literal: true

OauthAppPolicyUsageStatsJob = LegacyApplicationJob.wrap(GitHub::Jobs::OauthAppPolicyUsageStats)
