# frozen_string_literal: true

NetworkMaintenanceJob = LegacyApplicationJob.wrap(GitHub::Jobs::NetworkMaintenance)
