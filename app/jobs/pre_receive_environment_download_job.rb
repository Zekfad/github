# frozen_string_literal: true

PreReceiveEnvironmentDownloadJob = LegacyApplicationJob.wrap(GitHub::Jobs::PreReceiveEnvironmentDownload)
