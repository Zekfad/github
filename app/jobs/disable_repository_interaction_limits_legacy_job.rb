# frozen_string_literal: true

DisableRepositoryInteractionLimitsLegacyJob = LegacyApplicationJob.wrap(GitHub::Jobs::DisableRepositoryInteractionLimits)
