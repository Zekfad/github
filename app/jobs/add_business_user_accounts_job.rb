# frozen_string_literal: true

class AddBusinessUserAccountsJob < ApplicationJob
  areas_of_responsibility :admin_experience

  queue_as :business_user_accounts

  # Public: Background job wrapper for Business#add_user_accounts
  #
  # business - Business to which members are being added.
  # user_ids - Array of Integer representing the IDs of users being added.
  def perform(business, user_ids = [])
    business.add_user_accounts user_ids unless user_ids.blank?
  end
end
