# frozen_string_literal: true

RemoveFromSearchIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RemoveFromSearchIndex)
