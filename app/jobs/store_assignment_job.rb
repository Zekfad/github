# frozen_string_literal: true

class StoreAssignmentJob < ApplicationJob
  areas_of_responsibility :feature_lifecycle

  locked_by timeout: 5.minutes, key: DEFAULT_LOCK_PROC

  queue_as :analytics

  # Public: Perform the job
  #
  # assignment - A Verdict::Assignment
  #
  # Returns nothing
  def perform(user_experiment_id:, user_id:, subject_id:, subject_type:, subgroup:)
    UserExperimentEnrollment.throttle do
      begin
        UserExperimentEnrollment.create!(user_id: user_id, subject_id: subject_id,
        subject_type: subject_type, subgroup: subgroup, user_experiment_id: user_experiment_id)
      rescue ActiveRecord::RecordNotUnique
        GitHub.dogstats.increment("user_experiment_enrollment.duplicates")
      end
    end
  end
end
