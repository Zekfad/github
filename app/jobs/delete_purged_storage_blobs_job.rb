# frozen_string_literal: true

DeletePurgedStorageBlobsJob = LegacyApplicationJob.wrap(GitHub::Jobs::DeletePurgedStorageBlobs)
