# frozen_string_literal: true

class ApplicationDeliveryJob < ApplicationJob
  queue_as :mailers

  rescue_from ActiveJob::DeserializationError do |error|
    record_error(error)

    mailer, mail_method, delivery_method = @serialized_arguments
    GitHub.dogstats.increment("active_job.application_delivery_job.error", tags: [
      "mailer:#{mailer}",
      "mailer_method:#{mail_method}",
      "delivery_method:#{delivery_method}",
      "error:#{error.class.name.underscore}",
    ])

    context = {
      mailer: mailer,
      mailer_method: mail_method,
      deliver_method: delivery_method,
    }
    Failbot.report(error, context)
  end

  def perform(mailer, mail_method, delivery_method, *args)
    GitHub.dogstats.increment("active_job.application_delivery_job", tags: [
      "mailer:#{mailer}",
      "mailer_method:#{mail_method}",
      "delivery_method:#{delivery_method}",
    ])

    mailer.constantize.public_send(mail_method, *args).send(delivery_method)
  end
end
