# frozen_string_literal: true

NetworkMaintenanceSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::NetworkMaintenanceScheduler)
