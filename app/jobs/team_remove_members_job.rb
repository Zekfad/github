# frozen_string_literal: true

TeamRemoveMembersJob = LegacyApplicationJob.wrap(GitHub::Jobs::TeamRemoveMembers)
