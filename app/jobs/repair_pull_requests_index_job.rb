# frozen_string_literal: true

RepairPullRequestsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairPullRequestsIndex)
