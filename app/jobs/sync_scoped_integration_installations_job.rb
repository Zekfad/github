# frozen_string_literal: true

class SyncScopedIntegrationInstallationsJob < ApplicationJob
  queue_as :sync_scoped_integration_installations

  discard_on ActiveRecord::RecordNotFound

  attr_reader :parent

  def perform(installation, action:, **options)
    @parent = installation

    return true if active_scoped_integration_installation_ids.none?

    case action
    when :permissions_updated
      update_permissions(options[:old_version], options[:new_version])
    when :repositories_removed
      uninstall_repositories(options[:repository_ids])
    end
  end

  private

  def active_scoped_integration_installation_ids
    return @active_scoped_integration_installation_ids if defined?(@active_scoped_integration_installation_ids)

    scoped_integration_installation_ids = parent.children.pluck(:id)

    # We can't JOIN because ScopedIntegrationInstallation
    # records are in the collab cluster.
    #
    # So let's cheat and find all of the active scoped
    # installations by finding all of the active
    # AuthenticationToken records.
    @active_scoped_integration_installation_ids = AuthenticationToken.where(
      authenticatable_id:    scoped_integration_installation_ids,
      authenticatable_type: "ScopedIntegrationInstallation",
    ).active.pluck(:authenticatable_id)
  end

  def downgrade_permissions(permissions)
    args = {
      actor_ids:  active_scoped_integration_installation_ids,
      actor_type: "ScopedIntegrationInstallation",
    }

    # This transforms a set of permissions
    #
    # { "pull_requests" => :read, "issues" => :write, "repository_projects" => :write }
    #
    # and groups the resources by the action
    #
    # { :read => ["pull_requests"], :write => ["issues", "repository_projects"] }
    permissions = permissions.each_with_object(Hash.new { |h, k| h[k] = [] }) do |(resource, access), out|
      out[access] << resource
    end

    permissions.each_pair do |access, resources|
      args[:action]        = Permission.actions[access]
      args[:subject_types] = subject_types_from(resources)

      next if args[:subject_types].empty?

      ApplicationRecord::Iam.github_sql.run(<<-SQL, args)
        UPDATE permissions
        SET action = :action
        WHERE actor_type = :actor_type
          AND actor_id     IN :actor_ids
          AND subject_type IN :subject_types
      SQL
    end
  end

  def subject_types_from(resources)
    if resources.is_a?(Array)
      resources.map do |resource|
        next unless Repository::Resources.subject_types.include?(resource)
        "#{Repository::Resources::INDIVIDUAL_ABILITY_TYPE_PREFIX}/#{resource}"
      end.compact
    elsif resources.is_a?(Hash)
      subject_types_from(resources.keys)
    end
  end

  def remove_permissions(permissions)
    args = {
      actor_ids:     active_scoped_integration_installation_ids,
      actor_type:    "ScopedIntegrationInstallation",
      subject_types: subject_types_from(permissions),
    }

    return if args[:subject_types].empty?

    ApplicationRecord::Iam.github_sql.run(<<-SQL, args)
      DELETE FROM permissions
      WHERE actor_type = :actor_type
        AND actor_id     IN :actor_ids
        AND subject_type IN :subject_types
    SQL
  end

  # Internal: Uninstall a set of repositories from all active ScopedIntegrationInstallation
  # records.
  #
  # Returns nil.
  def uninstall_repositories(repository_ids)
    return if repository_ids.empty?

    # Limit the query to the permissions we know are granted on the installation
    subject_types = subject_types_from(parent.version.permissions_of_type(Repository))
    return if subject_types.empty?

    args = {
      actor_ids:     active_scoped_integration_installation_ids,
      actor_type:    "ScopedIntegrationInstallation",
      subject_ids:   repository_ids,
      subject_types: subject_types,
    }

    ApplicationRecord::Iam.github_sql.run(<<-SQL, args)
      DELETE FROM permissions
      WHERE actor_type = :actor_type
        AND actor_id     IN :actor_ids
        AND subject_id   IN :subject_ids
        AND subject_type IN :subject_types
    SQL
  end

  def update_permissions(old_version, new_version)
    diff = new_version.diff(old_version)

    ApplicationRecord::Iam.transaction do
      downgrade_permissions(diff.permissions_downgraded)
      remove_permissions(diff.permissions_removed)
    end
  end
end
