# frozen_string_literal: true

class MapImportRecordsJob < ApplicationJob
  areas_of_responsibility :migration
  queue_as :map_import_records

  def perform(migration, mappings, actor)
    # Set failbot context
    Failbot.push(migration_id: migration.id)

    # Set state to "mapping" from "pending"
    migration.begin_map!

    GitHub::Timer.timeout(GitHub.max_gh_migrator_export_time, GitHub::Migrator::TimeoutError) do
      GitHub.migrator.map(migration, mappings, actor)
    end
  rescue StandardError => exception
    # Set migration state to :failed
    migration.failed!

    Failbot.report!(exception)

    # Re-raise error
    raise exception
  end
end
