# frozen_string_literal: true

RepairCommitsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairCommitsIndex)
