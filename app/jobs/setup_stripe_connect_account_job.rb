# frozen_string_literal: true

require "stripe"

class SetupStripeConnectAccountJob < ApplicationJob
  queue_as :stripe

  class AccountMismatchError < StandardError
    attr_reader :old_account_id
    attr_reader :new_account_id

    def initialize(old_account_id:, new_account_id:)
      @old_account_id = old_account_id
      @new_account_id = new_account_id
    end
  end;

  RETRYABLE_ERRORS = [
    Stripe::APIConnectionError,
    Stripe::OAuth::InvalidRequestError,
    Stripe::StripeError,
  ].freeze

  DISCARDABLE_ERRORS = [
    Stripe::PermissionError,
    Stripe::OAuth::InvalidGrantError,
    AccountMismatchError,
  ].freeze

  retry_on(*RETRYABLE_ERRORS) do |job, error|
    Failbot.report(error,
      sponsors_listing_id: job.arguments.first.id,
      app: "github-external-request",
    )
  end

  discard_on(*DISCARDABLE_ERRORS) do |job, error|
    Failbot.report(error,
      sponsors_listing_id: job.arguments.first.id,
      app: "github-external-request",
    )
    if error.is_a?(Stripe::PermissionError)
      GitHub.dogstats.increment("stripe.permission_error", tags: ["action:setup_account"])
    end
  end

  def perform(sponsors_listing)
    # Clear any error that may have occurred during a previous setup attempt
    sponsors_listing.clear_stripe_connect_link_error

    authorization_code = sponsors_listing.stripe_authorization_code
    access_token_response = Billing::StripeConnect::Account::Oauth
      .request_access_token(authorization_code: authorization_code)

    stripe_account_id = access_token_response[:stripe_user_id]
    existing_stripe_account = sponsors_listing.stripe_connect_account

    if existing_stripe_account
      # This is to handle a unique case where an account gets disconnected.
      # see: https://github.com/github/sponsors/issues/819
      if existing_stripe_account.stripe_account_id == stripe_account_id
        Sponsors::FetchStripeAccountDetails.call(existing_stripe_account)
        return
      else
        raise AccountMismatchError.new(
          old_account_id: existing_stripe_account.stripe_account_id,
          new_account_id: stripe_account_id,
        )
      end
    end

    # Disallow linking the same Stripe account to multiple listings
    if Billing::StripeConnect::Account.where(stripe_account_id: stripe_account_id).exists?
      sponsors_listing.set_stripe_connect_link_error!(error: :duplicate_account_id)
      return
    end

    sponsors_listing.create_stripe_connect_account! \
      stripe_account_id: stripe_account_id

    if sponsors_listing.exempt_from_payout_probation?
      sponsors_listing.enable_payouts
    else
      sponsors_listing.disable_payouts
    end
  end
end
