# frozen_string_literal: true

class SyncBundledLicenseAssignmentJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  def perform(enterprise_agreement)
    assignments = Billing::BundledLicenseAssignment.for_enterprise_agreement(enterprise_agreement.agreement_id)
    return if assignments.empty?

    # sync bundled license assignements business to the enterprise agreement
    assignments.update_all(business_id: enterprise_agreement.business.id)
  end
end
