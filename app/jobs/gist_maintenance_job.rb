# frozen_string_literal: true

GistMaintenanceJob = LegacyApplicationJob.wrap(GitHub::Jobs::GistMaintenance)
