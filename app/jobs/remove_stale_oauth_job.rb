# rubocop:disable Style/FrozenStringLiteralComment

# This job looks for all unused or stale (not recently used) OAuth related
# records that should be deleted.

# We first remove all stale OauthAccesses upfront via a mass delete. We're
# not worried about logging for accesses since they are meant to be created
# and destroyed as needed. Since these accesses haven't been used in a long
# time there shouldn't be any forensic need for logs.
#
# After deleting all stale accesses we then process any authorizations that
# are eligible to be destroyed. It's important we destroy authorizations so
# that we get the dependent destroys as well as the associated logging.
class RemoveStaleOauthJob < ApplicationJob

  areas_of_responsibility :platform
  queue_as :remove_stale_oauth
  locked_by key: ->(job) { DEFAULT_LOCK_KEY }, timeout: 1.day
  schedule interval: 5.minutes, condition: -> { !GitHub.enterprise? }
  retry_on_dirty_exit

  REMOVE_AFTER_DURATION = 1.year

  REMOVAL_CONDITIONS = <<-SQL
    (accessed_at IS NULL AND created_at < :after) or
    (accessed_at < :after)
  SQL

  # Accesses shouldn't remove PATs
  IS_APPLICATION = <<-SQL
    is_application = 1
  SQL

  # Public: Remove all stale OAuth accesses and authorizations.
  #
  # batch_size - The number of records to remove in each batch.
  # duration   - The amount of time in seconds that each job instance can run.
  #              After this time is elapsed, if there are still more batches to
  #              process, another job is enqueued to finish the work, and the
  #              current job terminates.
  #
  # Returns nothing.
  def perform(batch_size: 100, duration: 60)
    end_at = Time.current + duration

    status = remove_accesses(batch_size: batch_size, end_at: end_at)
    return requeue(batch_size: batch_size, duration: duration) if status == :timeout

    status = remove_authorizations(batch_size: batch_size, end_at: end_at)
    requeue(batch_size: batch_size, duration: duration) if status == :timeout
  end

  private

  # Returns :completed if all stale accesses were removed. Returns :timeout if
  #   the given end_at time was reached before all stale accesses could be
  #   removed.
  def remove_accesses(batch_size:, end_at:)
    loop do
      oauth_accesses = OauthAccess.where(REMOVAL_CONDITIONS, {
        after: REMOVE_AFTER_DURATION.ago,
      }).where(IS_APPLICATION).limit(batch_size)

      deleted_accesses = OauthAccess.throttle_with_retry(max_retry_count: 8) do
        oauth_accesses.delete_all
      end
      GitHub.dogstats.count("account_security.oauth_access", deleted_accesses, tags: ["action:destroy", "explanation:stale"])

      break(:completed) if deleted_accesses < batch_size

      break(:timeout) if Time.current >= end_at
    end
  end

  # Returns :completed if all stale authorizations were removed. Returns :timeout
  #   if the given end_at time was reached before all stale authorizations
  #   could be removed.
  def remove_authorizations(batch_size:, end_at:)
    loop do
      oauth_authorizations = OauthAuthorization.where(REMOVAL_CONDITIONS, {
        after: REMOVE_AFTER_DURATION.ago,
      }).limit(batch_size)

      destroyed_count = OauthAuthorization.throttle_with_retry(max_retry_count: 8) do
        oauth_authorizations.reduce(0) do |destroyed, authorization|
          GitHub.audit.inline do
            authorization.destroy_with_explanation(:stale)
            destroyed + 1
          end
        end
      end

      break(:completed) if destroyed_count < batch_size

      break(:timeout) if Time.current >= end_at
    end
  end

  def requeue(**args)
    clear_lock
    self.class.perform_later(**args)
  end
end
