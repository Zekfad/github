# frozen_string_literal: true

# Performs instrumentation of repositories added or removed from an
# IntegrationInstallation asynchronously because sometimes a large number of
# repos are involved in an installation change, which can cause a timeout in a
# normal web request.
class IntegrationInstallationInstrumentationJob < ApplicationJob
  areas_of_responsibility :ecosystem_apps

  queue_as :integration_installation_instrumentation

  def perform(action, installation_id, actor_id, repository_ids, repository_selection, requester_id: nil)
    installation = IntegrationInstallation.where(id: installation_id).first
    actor = User.where(id: actor_id).select(:id).first

    return if installation.blank? || actor.blank?

    options = {
      actor: actor,
      async: false, # Do it synchronously so we don't recursively enqueue this job
    }

    case action
    when :repositories_added
      options[:repository_selection] = repository_selection
      options[:requester_id]         = requester_id if requester_id

      installation.instrument_repositories_added(repository_ids, **options)
    when :repositories_removed
      installation.instrument_repositories_removed(repository_ids, **options)
    end
  end
end
