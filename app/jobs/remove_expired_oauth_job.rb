# frozen_string_literal: true

RemoveExpiredOauthJob = LegacyApplicationJob.wrap(GitHub::Jobs::RemoveExpiredOauth)
