# frozen_string_literal: true

class RemoveTeamFromRepoJob < ApplicationJob
  queue_as :remove_team_from_repo

  def perform(team:, repo:)
    team.remove_repository(repo)
  end
end
