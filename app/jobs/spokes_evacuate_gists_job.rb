# frozen_string_literal: true

class SpokesEvacuateGistsJob < ApplicationJob
  queue_as :dgit_schedulers
  schedule interval: 1.minute
  locked_by timeout: 10.minutes, key: DEFAULT_LOCK_PROC

  areas_of_responsibility :dgit

  def perform
    Failbot.push app: "github-dgit"

    return if GitHub.flipper[:dgit_disable_gist_evacuation].enabled?

    SlowQueryLogger.disabled do
      start = Time.now
      GitHub::DGit::Maintenance.run_gist_evacuation
      GitHub.stats.timing("dgit.evacuate-gists", Time.now-start) if GitHub.enterprise?
    end
  end
end
