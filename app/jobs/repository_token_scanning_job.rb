# frozen_string_literal: true

class RepositoryTokenScanningJob < TokenScanningJob
  areas_of_responsibility :token_scanning
  queue_as :token_scanning

  def job_scope
    "repo"
  end

  def job_visibility
    "public"
  end

  # Scan the repository for sensitive tokens.
  def scan_for_tokens
    chain = GitHub::TokenScanning::ChainProcessor.new(git_repo, actor)

    # The repack/token-scan can take a long time...
    ::GitRPC.with_timeout(15.minutes) do
      add_blob_locations(token_infos)

      token_infos.each do |token_info|
        chain << token_info
      end
    end

    chain.process
    chain.report_stats(stats_tags)
    "processed"
  end

  # Populate token_info hashes using which ever tokens scanning approach
  # is enabled for this repository.
  #
  # Returns an Array of token_info hashes or Raises GitRPC::CommandFailed.
  def token_infos
    @token_infos ||= find_creds(dedup_results: true)
  end
end
