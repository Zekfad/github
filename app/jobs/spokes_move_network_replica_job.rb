# frozen_string_literal: true

SpokesMoveNetworkReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitMoveNetworkReplica)
