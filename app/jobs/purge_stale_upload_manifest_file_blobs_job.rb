# frozen_string_literal: true

PurgeStaleUploadManifestFileBlobsJob = LegacyApplicationJob.wrap(GitHub::Jobs::PurgeStaleUploadManifestFileBlobs)
