# frozen_string_literal: true

PreReceiveEnvironmentDeleteJob = LegacyApplicationJob.wrap(GitHub::Jobs::PreReceiveEnvironmentDelete)
