# frozen_string_literal: true

class EnableSponsorsPayoutsJob < ApplicationJob
  queue_as :billing

  schedule interval: 1.day, condition: -> { !GitHub.enterprise? }

  def perform
    payouts_enabled = 0

    listings.each do |listing|
      next unless meets_payout_requirements?(listing)

      listing.update!(payout_probation_ended_at: Time.zone.now)
      listing.enable_payouts

      payouts_enabled += 1
    end

    GitHub.dogstats.count("sponsors.payout_probation_ended", payouts_enabled)
  end

  private

  def listings
    @listings ||= SponsorsListing
      .includes(:sponsors_membership, :docusign_envelopes)
      .on_payout_probation
      .with_approved_state
      .where("payout_probation_started_at < ?", SponsorsListing::PAYOUT_PROBATION_DAYS.days.ago)
  end

  def meets_payout_requirements?(listing)
    listing.docusign_completed? ||
      !listing.sponsorable.sponsors_docusign_enabled?
  end
end
