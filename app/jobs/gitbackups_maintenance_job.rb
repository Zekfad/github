# frozen_string_literal: true

GitbackupsMaintenanceJob = LegacyApplicationJob.wrap(GitHub::Jobs::GitbackupsMaintenance)
