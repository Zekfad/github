# frozen_string_literal: true

PostDelayedAuditJob = LegacyApplicationJob.wrap(GitHub::Jobs::PostDelayedAudit)
