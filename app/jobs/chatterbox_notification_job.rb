# frozen_string_literal: true

ChatterboxNotificationJob = LegacyApplicationJob.wrap(GitHub::Jobs::ChatterboxNotification)
