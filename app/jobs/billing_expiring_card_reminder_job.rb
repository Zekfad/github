# frozen_string_literal: true

class BillingExpiringCardReminderJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  discard_on ActiveRecord::RecordNotFound

  def perform(user)
    return unless user&.should_remind_about_expiring_card?

    BillingNotificationsMailer.credit_card_will_expire_soon(user).deliver_later

    user.payment_method.increment!(:expiration_reminders)
    GitHub.dogstats.increment("billing.reminders.card_expiring")
  end
end
