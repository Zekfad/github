# frozen_string_literal: true

SubscribeToListNotificationsJob = LegacyApplicationJob.wrap(GitHub::Jobs::SubscribeToListNotifications)
