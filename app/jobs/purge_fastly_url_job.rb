# frozen_string_literal: true

PurgeFastlyUrlJob = LegacyApplicationJob.wrap(GitHub::Jobs::PurgeFastlyUrl)
