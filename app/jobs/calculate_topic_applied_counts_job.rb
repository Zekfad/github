# frozen_string_literal: true

# Updates `topics.applied_count` for records with new `repository_topics` rows since the last run.
#
# The `applied_count` column is a denormalization of `repository_topics.topic_id` where the
# `repository_topics` row would be considered "applied," and is used in various Topic queries.
class CalculateTopicAppliedCountsJob < ApplicationJob
  queue_as :calculate_topic_applied_counts

  schedule interval: 1.hour

  locked_by timeout: 45.minutes, key: ->(job) { job.class.name }

  def perform
    Topic.ids_in_batches do |ids|
      Topic.update_applied_counts(ids)
    end
  end
end
