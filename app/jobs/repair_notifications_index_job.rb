# frozen_string_literal: true

RepairNotificationsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairNotificationsIndex)
