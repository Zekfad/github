# frozen_string_literal: true

class SpokesColdStorageSweeperJob < ApplicationJob
  # Schedule Jobs can't be toggled with the legacy_class since they have to be scheduled in timers.fe.rb
  # Switching to this worker will require a deploy
  areas_of_responsibility :dgit
  queue_as :dgit_cold_storage_sweeper

  def self.enabled?
    !GitHub.enterprise?
  end

  schedule(interval: 20.seconds, scope: :global, condition: ->() { !GitHub.enterprise? })

  def perform
    return unless GitHub.flipper.enabled?(:dgit_cold_storage_sweeper)
    Failbot.push app: "github-dgit"

    SlowQueryLogger.disabled do
      GitHub::DGit::ColdStorageSweeper.run_sweeper
    end

    approx_count = GitHub::DGit::ColdStorage::approximate_count
    if approx_count != nil
      GitHub.dogstats.count("dgit.cold-storage.networks", approx_count)
    end
  end
end
