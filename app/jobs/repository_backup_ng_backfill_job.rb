# frozen_string_literal: true

RepositoryBackupNgBackfillJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryBackupNgBackfill)
