# frozen_string_literal: true

RepairLabelsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairLabelsIndex)
