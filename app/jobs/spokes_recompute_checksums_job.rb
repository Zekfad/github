# frozen_string_literal: true

SpokesRecomputeChecksumsJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitRecomputeChecksums)
