# frozen_string_literal: true

class BillingChargeJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  discard_on ActiveRecord::RecordNotFound

  def perform(user)
    GitHub::Logger.log \
      at: "billing.jobs.charge",
      login: user.login,
      user_id: user.id

    user.recurring_charge
  end
end
