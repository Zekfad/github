# frozen_string_literal: true

RepositoryDeleteJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryDelete)
