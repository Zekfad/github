# frozen_string_literal: true

StorageReplicateObjectJob = LegacyApplicationJob.wrap(GitHub::Jobs::StorageReplicateObject)
