# frozen_string_literal: true

DelayedJob = LegacyApplicationJob.wrap(GitHub::Jobs::Delayed)
