# frozen_string_literal: true

class CloneTemplateRepositoryFilesJob < ApplicationJob
  DEFAULT_BRANCH_COMMIT_MESSAGE = "Initial commit"

  class CloneError < StandardError
    attr_accessor :error_reason_code

    def initialize(message, error_reason_code = nil)
      super("Could not finish template repository clone job: #{message}")
      @error_reason_code = error_reason_code
    end
  end

  areas_of_responsibility :repositories

  queue_as :clone_template_repo_files

  def perform(repo_clone_id, copy_branches: false)
    Failbot.push(repo_clone_id: repo_clone_id)
    unless repo_clone = RepositoryClone.find_by_id(repo_clone_id)
      raise CloneError.new("Clone with id #{repo_clone_id} was not found.")
    end
    Failbot.push(repo_id: repo_clone.template_repository_id,
      clone_repo_id: repo_clone.clone_repository_id,
      cloning_user: repo_clone.cloning_user.login)

    unless (template_repo = repo_clone.template_repository)&.active?
      raise CloneError.new("missing active template repo", :missing_template_repo)
    end

    unless target_repo = repo_clone.clone_repository
      raise CloneError.new("missing clone repository", :missing_clone_repo)
    end

    branches_to_copy = [template_repo.default_branch.b]
    branches_to_copy.concat(get_other_branch_names_for(template_repo)) if copy_branches
    GitHub.dogstats.time "clone_template_repository.copy_branches.time", tags: ["all:#{copy_branches}"] do
      branches_to_copy.each do |branch_name|
        copy_branch(repo_clone, branch_name, template_repo: template_repo, target_repo: target_repo,
          committer: repo_clone.cloning_user)
      end
    end
    set_default_branch(repo_clone)

    finish_clone_success(repo_clone)
  rescue GitHub::DGit::Error => e
    Failbot.report(e)
    finish_clone_error(repo_clone, :dgit_exception)
    raise
  rescue GitRPC::Error => e
    Failbot.report(e)
    finish_clone_error(repo_clone, :gitrpc_exception)
    raise
  rescue CloneError => e
    Failbot.report(e)
    finish_clone_error(repo_clone, e.error_reason_code)
    raise
  rescue => e
    Failbot.report(e)
    finish_clone_error(repo_clone, :other_exception)
    raise
  end

  private

  def set_default_branch(repo_clone)
    template_repo = repo_clone.template_repository
    target_repo = repo_clone.clone_repository
    new_default_branch = template_repo.default_branch.b
    return if target_repo.default_branch.b == new_default_branch

    unless target_repo.update_default_branch(new_default_branch)
      raise CloneError.new("failed to update default branch", :failed_to_update_branch)
    end
  end

  # Private: Get a list of non-default branches in the given repository.
  #
  # repo - a Repository
  #
  # Returns an Array of Strings.
  def get_other_branch_names_for(repo)
    branch_indicator_prefix = "refs/heads/"
    repo.rpc.raw_branch_names_and_dates.map { |date, ref_name| ref_name }. # all refs
         select { |ref_name| ref_name.start_with?(branch_indicator_prefix) }. # just the branches
         map { |ref_name| ref_name.split(branch_indicator_prefix).last.b }. # drop 'refs/heads/' prefix
         reject { |branch_name| branch_name == repo.default_branch.b } # default branch is already copied
  end

  def copy_branch(repo_clone, branch_name, template_repo:, target_repo:, committer:)
    template_commit_oid = template_repo.ref_to_sha(branch_name)
    if template_commit_oid.nil?
      raise CloneError.new("no commit oid for template repo branch", :no_commit_oid)
    end

    file_list = template_repo.tree_file_list(template_commit_oid, skip_directories: [])
    return true if file_list.empty?

    file_list.each do |path|
      tree_entry = template_repo.tree_entry(template_commit_oid, path)
      if tree_entry.size > RepositoryClone::MAX_TEMPLATE_FILE_SIZE_IN_MEGABYTES.megabytes
        raise CloneError.new("file too large for template repo branch", :file_too_large)
      end
    end

    base_ref = target_repo.heads.find_or_build(branch_name)
    commit_details = {
      message: commit_message_for(branch_name, default_branch: template_repo.default_branch),
      committer: committer,
    }

    base_ref.append_commit(commit_details, committer) do |files|
      file_list.each do |path|
        tree_entry = template_repo.tree_entry(template_commit_oid, path, limit: 10.megabytes)
        data = file_content_for(tree_entry)

        if (mode = tree_entry.mode).present?
          files.add(path, data, mode: mode.to_i(8))
        else
          files.add(path, data)
        end
      end
    end
  end

  def commit_message_for(branch_name, default_branch:)
    if branch_name == default_branch
      DEFAULT_BRANCH_COMMIT_MESSAGE
    else
      "Initialize #{branch_name}"
    end
  end

  def file_content_for(tree_entry)
    if tree_entry.encoding
      tree_entry.data.force_encoding(tree_entry.encoding)
    else
      tree_entry.data
    end
  end

  def finish_clone_error(repo_clone, error_reason_code)
    GitHub.dogstats.increment "clone_template_repository", tags: ["action:error"]
    return unless repo_clone
    repo_clone.state = :error
    repo_clone.error_reason_code = error_reason_code
    repo_clone.save(validate: false)
  end

  def finish_clone_success(repo_clone)
    repo_clone.state = :finished

    if repo_clone.save
      GitHub.dogstats.increment "clone_template_repository", tags: ["action:success"]
    else
      repo_clone.update_attribute(:state, :error)
      repo_clone.update_attribute(:error_reason_code, :cannot_finish)
      raise CloneError.new("validation failure: #{repo_clone.errors.full_messages.to_sentence}")
    end
  end
end
