# frozen_string_literal: true

StorageClusterMoveObjectJob = LegacyApplicationJob.wrap(GitHub::Jobs::StorageClusterMoveObject)
