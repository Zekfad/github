# frozen_string_literal: true

# Moves repositories between networks. This is a disk and network operation
# on github.com since repositories are stored alongside their network.
#
# The job takes an operation to perform, repository to perform it on, and for
# operations that require it, a destination repository. The operation may be
# any of the following:
#
# detach      - Move repo into its own network without forks.
# extract     - Move repo and all forks out of the current network and into a
#               new network.
# new_extract - Move repo and all forks out of the current network and into a
#               new network with updated extract code.
# reattach    - Make this repo's network part of its old parent network if there
#             - was one.
# attach_to   - Make this repo's network part of the specified destination
#             - repo's network if they are related.
class RepositoryNetworkOperationsJob < ApplicationJob
  areas_of_responsibility :repos
  queue_as :repository_network_operations

  # Lock any operation on the source repo. Prevent queueing jobs to e.g. extract
  # and reattach the same repo at the same time.
  locked_by timeout: 1.week, key: -> (job) { job.arguments[1]&.id }

  # Discard the job if a repository is deleted before the job runs
  discard_on ActiveJob::DeserializationError # TODO

  def perform(operation, source_repo, dest_repo = nil)
    raise ArgumentError.new("source_repo is required") unless source_repo
    raise ArgumentError.new("unsupported operation: #{operation}") unless self.respond_to?(operation, true)

    Failbot.push operation: @operation, source_repo_id: source_repo.id, dest_repo_id: dest_repo&.id

    @source_repo = source_repo
    @dest_repo = dest_repo

    send(operation)
  end

  private

  # "Detach" a repository, by pulling it and only it out into a whole new
  # network. The detached repository's child repositories remain in the original
  # network, but reparented under some other repository. The repository is moved
  # to a new location on disk (possibly over the network).
  def detach
    @source_repo.detach!
  end

  # "Extract" a repository and all child forks from the current network
  # into a whole new network.
  def extract
    @source_repo.extract!
  end

  def new_extract
    @source_repo.new_extract!
  end

  # Reattach a repository to its parent network, if it exists
  def reattach
    @source_repo.reattach!
  end

  # Attach a repo to another repo in the same network family
  def attach_to
    raise ArgumentError.new("dest_repo is required for attach_to") unless @dest_repo
    @source_repo.attach_to!(@dest_repo, async: false)
  end
end
