# frozen_string_literal: true

WikiPageSpamCheckJob = LegacyApplicationJob.wrap(GitHub::Jobs::WikiPageSpamCheck)
