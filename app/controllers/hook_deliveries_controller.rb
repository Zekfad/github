# frozen_string_literal: true

class HookDeliveriesController < ApplicationController
  areas_of_responsibility :webhook

  statsd_tag_actions only: :show

  before_action :authorization_required
  before_action :sudo_filter
  before_action :find_delivery, only: [:show, :payload]

  helper_method :delivery_guid, :current_hook

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    max: 30,
    key: :hook_delivery_rate_limit_key,
    at_limit: :hook_delivery_rate_limit_record

  include Hookshot::DeliverJobLogger

  def index
    if params[:updated_after].blank? || deliveries_updated?(delivery_guid, params[:updated_after])

      @query = params[:deliveries_q]
      params_for_search = parse_search_query(@query)
      params_for_search[:limit] = HookHelper::HOOKSHOT_MAX_DELIVERIES
      if params[:next_cursor]
        params_for_search[:cursor] = params[:next_cursor]
      end

      status, data = hookshot.deliveries_for_hook(current_hook.id, params_for_search)
      if status == 200
        @next_cursor = data.dig("page_info", "next_cursor")
        deliveries = Hookshot::Delivery.load(data["deliveries"])
        @deliveries_view = Hooks::DeliveriesView.new(deliveries: deliveries, outages: data["outages"])

        respond_to do |format|
          format.html { render "hook_deliveries/index", layout: false }
        end
      else
        body = data&.fetch("message")
        raise Hookshot::BadResponseError.new(status, body)
      end

    else
      head :accepted
    end
  end

  def parse_search_query(query)
    allowed_delivery_api_queries = [:since, :until, :status, :status_code, :events, :redelivery, :guid, :repo_id, :installation_id]
    parsed_query = Search::ParsedQuery.parse(query, allowed_delivery_api_queries)
    parsed_query.select! { |query_item| query_item.is_a?(Array) } # ignore any parts of the query not in our list of terms
    # TODO: should do some kind of validation/error handling if one item
    # term is provided multiple times, because right now we'll only take the
    # last one
    Hash[parsed_query]
  end

  def show
    gauge_payload_and_response_size
    @delivery_view = Hooks::DeliveryView.new hook: current_hook, delivery: @delivery, current_user: current_user

    respond_to do |format|
      format.html { render "hook_deliveries/show", layout: false }
    end
  end

  def payload
    render json: @delivery.payload
  end

  def redeliver
    if GitHub.hookshot_go_enabled?
      begin
        Hook::DeliverySystem.redeliver(delivery_guid, current_hook)
      rescue Hookshot::PayloadTooLarge => e
        log_params = {
          error: e,
          target: current_hook.installation_target,
          parent: current_hook.hookshot_parent_id,
          guid: delivery_guid,
          hook_ids: current_hook.id,
        }

        log_payload_too_large_error(log_params)
        Failbot.report e
      end
    else
      Hook::DeliverySystem.redeliver_later(delivery_guid, current_hook)
    end

    respond_to do |format|
      format.html { render "hook_deliveries/redeliver", layout: false }
    end
  end

  def target_for_conditional_access
    target = current_hook.installation_target
    return :no_target_for_conditional_access unless target

    return target.owner if target.respond_to?(:owner)
    target
  end

  private

  def current_hook
    @current_hook ||= Hook.find_by_id(params[:hook_id]) || Hook.find_by_id(params[:webhook_id]) || raise(NotFound)
  end

  def delivery_guid
    params[:guid]
  end

  def delivery_id
    params[:id]
  end

  def find_delivery
    status, resp = hookshot.delivery_for_hook(delivery_id, current_hook.id, {"include_payload" => true})
    raise Hookshot::BadResponseError.new(status) unless status == 200
    @delivery = Hookshot::Delivery.load resp
  end

  def authorized?
    logged_in? && can_view_hook_deliveries?
  end

  def can_view_hook_deliveries?
    return false if installation_target_context && installation_target_context != current_hook.installation_target

    owner = case current_hook.installation_target
    when Repository, Marketplace::Listing, Integration, SponsorsListing
      current_hook.installation_target.owner
    when User, Organization, Business
      current_hook.installation_target
    else
      raise "Unknown hook installation target"
    end

    result = if GitHub.flipper[:custom_roles].enabled?(owner) &&
                 GitHub.flipper[:fgp_webhook].enabled?(owner) &&
                 GitHub.flipper[:can_view_hook_deliveries_fgp_friendly].enabled?(owner)
      # users can also be granted manage_webhooks FGP over a Repository, which is a superset of view_hook_deliveries.
      # we do this trickery so we don't have to implement manage_webhooks policies for all other actor/subject combinations
      action = if current_user.user? && current_hook.installation_target.is_a?(Repository)
        [:view_hook_deliveries, :manage_webhooks]
      else
        :view_hook_deliveries
      end
      ::Permissions::Enforcer.authorize(
        action: action,
        actor: current_user,
        subject: current_hook.installation_target,
        context: {
          considers_site_admin: true
        }
      )
    else
      result = science "can_view_hook_deliveries_version_two" do |e|
        e.context user: current_user,
                  hook: current_hook

        e.use {
          ::Permissions::Enforcer.authorize(
            action: :view_hook_deliveries,
            actor: current_user,
            subject: current_hook.installation_target,
            context: {
              subject_owner_id: current_hook.installation_target.try(:owner)&.id,
              site_admin: current_user.site_admin?,
              target_adminable: current_hook.installation_target.adminable_by?(current_user),
              version: 1
            }
          )
        }
        e.try {
          # users can also be granted manage_webhooks FGP over a Repository, which is a superset of view_hook_deliveries.
          # we do this trickery so we don't have to implement manage_webhooks policies for all other actor/subject combinations
          action = if current_user.user? && current_hook.installation_target.is_a?(Repository)
            [:view_hook_deliveries, :manage_webhooks]
          else
            :view_hook_deliveries
          end
          ::Permissions::Enforcer.authorize(
            action: action,
            actor: current_user,
            subject: current_hook.installation_target,
            context: {
              considers_site_admin: true
            }
          )
        }
        e.compare { |control, candidate| control.allow? == candidate.allow? }

        # Reduce noise of this experiment by ignoring network failures
        e.ignore { |control, candidate| Authzd.ignore_error_in_science?(candidate) || Authzd.ignore_error_in_science?(control) }

        # :fgp_webhook is the flag that guards all new webhook functionality
        # it is temporary and meant to be used as emergency switch.
        # To avoid modifying the existing policies to include a check for it, we ignore the mismatch.
        e.ignore { |_control, _candidate| !GitHub.flipper[:fgp_webhook].enabled?(owner) }
      end
    end
    result.allow?
  end

  def hookshot
    @hookshot ||= Hookshot::Client.for_parent current_hook.hookshot_parent_id
  end

  # check whether a redelivery for the given guid after the provided time is
  # included in the /deliveries output
  def deliveries_updated?(guid, updated_after)
    params = { since: updated_after,
               guid: guid,
               limit: 1} # we don't actually care about the data returned, just that there is data
    status, data = hookshot.deliveries_for_hook(current_hook.id, params)
    status == 200 && !data["deliveries"].empty? # TODO should we do something else if there's not a 200?
  end

  def hook_delivery_rate_limit_key
    "hook_deliveries:#{action_name}:#{current_user.id}"
  end

  def hook_delivery_rate_limit_record
    GitHub.dogstats.increment "hook_delivery", tags: ["action:#{action_name}", "error:ratelimited"]
  end

  # Measure payloads and response bodies so we can make a more intelligent
  # decision on where to limit these when displaying them in the UI.
  #
  # https://github.com/github/github/pull/20668
  def gauge_payload_and_response_size
    payload = @delivery.payload
    response_body = @delivery.response_body
    GitHub.dogstats.gauge "hook_delivery.payload_size", payload.to_s.size, tags: ["action:show"]
    GitHub.dogstats.gauge "hook_delivery.response_body_size", response_body.to_s.size, tags: ["action:show"]
  end

  # Allow access to these endpoints from restricted stafftools frontends.
  def require_conditional_access_checks?
    return false if GitHub.admin_host? && logged_in? && current_user.site_admin?
    return true
  end

  def installation_target_context
    case params[:context]
    when "repository"
      Repository.with_name_with_owner(params[:user_id], params[:repository])
    when "organization"
      Organization.find_by(login: params[:organization_id])
    when "enterprise"
      Business.find_by(slug: params[:slug])
    when "user_integration"
      current_user.integrations.not_marked_for_deletion.find_by!(slug: params[:app_id])
    when "org_integration"
      org = Organization.find_by(login: params[:organization_id])
      org && org.integrations.not_marked_for_deletion.find_by!(slug: params[:app_id])
    when "marketplace_listing"
      Marketplace::Listing.find_by(slug: params[:listing_slug])
    when "sponsors_listing"
      User.find_by(login: params[:sponsorable_id])&.sponsors_listing
    end
  end
end
