# frozen_string_literal: true

class TrendingController < ApplicationController
  areas_of_responsibility :explore

  statsd_tag_actions only: [:index, :developers]

  include Trending::TrendingMethods
  include ExploreHelper

  skip_before_action :perform_conditional_access_checks

  layout "site"

  def index
    @user_languages = user_languages

    if GitHub.enterprise?
      if trending_cache_exists("repos", since)
        @popular = popular_repositories
      end

      render_template_view(
        "trending/index",
        Trending::ListView,
        language: language,
        since: since,
        context: IndexContext,
      )
    else
      repositories = ExploreFeed::Trending::Repository.all(
        language: language,
        period: since,
        spoken_language_code: spoken_language_code,
      )

      render(
        "trending/index_redesign",
        locals: {
          language: language,
          repositories: repositories,
          since: since,
          spoken_language_code: spoken_language_code,
          user_languages: @user_languages,
          context: IndexContext,
        },
      )
    end
  end

  def developers
    @user_languages = user_languages

    if GitHub.enterprise?
      if trending_cache_exists("users", since)
        @popular = popular_developers
      end

      render_template_view(
        "trending/developers",
        Trending::ListView,
        context: DevelopersContext,
        language: language,
        since: since,
      )
    else
      developers = ExploreFeed::Trending::Developer.all(
        language: language,
        period: since,
      )

      render(
        "trending/developers_redesign",
        locals: {
          developers: developers,
          language: language,
          since: since,
          user_languages: @user_languages,
          context: DevelopersContext,
        },
      )
    end
  end

  private

  def language
    # Maintain backwards compatibility with params[:l]
    @language ||= (params[:language] || params.delete(:l)).try(:downcase)
  end

  def spoken_language_code
    if trending_repositories_spoken_language_filtering_enabled?
      params.fetch(:spoken_language_code, current_user&.profile_spoken_language_preference_code)
    end
  end

  def since
    s = params[:since].try(:downcase)
    date_options.keys.include?(s) ? s : "daily"
  end

  def popular_repositories
    Trending.repos(viewer: current_user,
                   cache_key: cache_key_for_query("repositories"),
                   ttl: 3.hours,
                   options: trending_query_options)
  end

  def popular_developers
    results = GitHub.cache.fetch(cache_key_for_query("developer_ids"), ttl: 3.hours) do
      Trending.users(trending_query_options).map { |user, data| [user.id, data] }
    end

    users = User.where(id: results.map { |user_id, _| user_id }).index_by(&:id)

    results.map { |user_id, data| [users[user_id], data] }
  end

  def trending_query_options
    {
      language: language,
      period: since,
      skip_min: GitHub.enterprise? || Rails.development?,
    }
  end

  def cache_key_for_query(type)
    if unknown_language?
      default_alias = "unknown"
    else
      default_alias = selected_language.default_alias if known_language?
    end
    "popular#{type}:#{default_alias}#{since}"
  end

  # The cached version of starred languages.
  def user_languages
    user_cache_key = logged_in? ? current_user.cache_key : "anonymous"
    GitHub.cache.fetch("staruserlangcount:#{user_cache_key}", ttl: 5.minutes) do
      user_languages!
    end
  end

  # The counts of the languages in a user's starred repos.
  #
  # Returns an array of Language objects.
  def user_languages!
    if logged_in? && current_user.starred_repositories.count > 0
      keys = current_user.starred_repositories_by_language(7).keys
    else
      keys = LanguageName.sorted_rankings.first(7).map { |lang| lang.first }
    end
    Linguist::Language.all.map { |lang| lang if keys.include? lang.name }.compact
  end
end
