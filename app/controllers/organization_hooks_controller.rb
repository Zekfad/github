# frozen_string_literal: true

class OrganizationHooksController < ApplicationController
  areas_of_responsibility :webhook

  statsd_tag_actions

  before_action :org_admins_only
  before_action :ensure_trade_restrictions_allows_org_settings_access

  javascript_bundle :settings

  include OrganizationsHelper
  include HooksControllerMethods # All Hook related actions

  private

  # Org hooks
  def current_context
    current_organization
  end
end
