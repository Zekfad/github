# frozen_string_literal: true

class RepositoryActionsSettingsController < AbstractRepositoryController
  include Actions::RunnersHelper
  include Actions::ForkPrWorkflowsPolicyHelper

  areas_of_responsibility :repo_settings
  map_to_service :actions_experience

  before_action :login_required
  before_action :ensure_admin_access
  before_action :setup_actions_app, only: [:add_runner]
  before_action :ensure_tenant_exists, only: [:add_runner]
  before_action :skip_unverified_and_spammy, only: [:delete_runner_modal, :delete_runner]
  before_action :sudo_filter, only: [:delete_runner_modal, :delete_runner]
  before_action :set_cache_control_no_store, only: [:delete_runner_modal]

  javascript_bundle :settings

  def index
    session[:initiated_launch_app_setup] = false
    render "edit_repositories/pages/actions", locals: { runners: repo_runners }
  end

  def list_runners
    view = Actions::RepoRunnersView.new(settings_owner: current_repository, current_user: current_user)
    render partial: "actions/settings/runners_list", locals: { view: view, runners: repo_runners }
  end

  def add_runner
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_downloads(current_repository)
    end

    downloads = resp&.value&.downloads

    if downloads.blank?
      # Actions aren't setup yet for this repo.
      return render "edit_repositories/pages/actions/add_runner_error"
    end

    scope = current_repository.runner_creation_token_scope
    expires_at = 1.hour.from_now
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)

    add_runner_data = runner_options(downloads: downloads, token: token)
    render "edit_repositories/pages/actions/add_runner", locals: add_runner_data
  end

  def delete_runner_modal
    scope = current_repository.runner_registration_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: 1.hour.from_now)

    respond_to do |format|
      format.html do
        render partial: "edit_repositories/pages/actions/delete_runner_modal",
               locals: { token: token, runner_id: params[:id], runner_os: params[:os] }
      end
    end
  end

  def delete_runner
    delete_status = delete_runner_for(current_repository, id: params[:id].to_i, actor: current_user)

    if delete_status == "deleted"
      flash[:notice] = "Runner successfully deleted."
    else
      flash[:error] = "Sorry, there was a problem deleting your runner."
    end

    redirect_to repository_actions_settings_path
  end

  def update_execution_capabilities
    unless params[:action_execution_capability].to_s.empty?
      current_repository.update_action_execution_capabilities(params[:action_execution_capability], actor: current_user)
    end

    if request.xhr?
      respond_to do |format|
        format.html do
          render partial: "navigation/repository/main",
                 locals: { repository: current_repository, highlight: :repo_settings }
        end
      end
    elsif current_repository.errors.any?
      flash[:error] = "Error saving your changes: #{current_repository.errors.full_messages.to_sentence}"
      redirect_to :back
    else
      flash[:notice] = "Actions settings saved."
      redirect_to :back
    end
  rescue Configurable::ActionExecutionCapabilities::Error
    flash[:error] = "Error saving your changes."
    redirect_to :back
  end

  def bulk_actions
    if runner_ids = params[:runner_ids]
      render partial: "actions/settings/runner_bulk_action", locals: {
        runner_ids: params[:runner_ids],
        menu_name: "Labels",
        menu_path: repo_runner_bulk_labels_path(runner_ids: params[:runner_ids]),
        update_path: repo_runner_bulk_update_labels_path,
      }
    else
      head :ok
    end
  end

  def update_fork_pr_workflows_policy
    return render_404 unless GitHub.flipper[:fork_pr_workflows_policy].enabled?(current_repository.owner)

    if current_repository.public?
      flash[:error] = "Fork pull request workflow policy can only be changed for private repositories."
      return redirect_to :back
    end

    form_policy = params[:fork_pr_workflows_policy] || {}

    set_fork_pr_workflows_policy(current_repository, form_policy)
  end

  private

  helper_method :current_repository

  def setup_actions_app
    return if current_repository.actions_app_installed?

    # Actions has not been setup yet, install the app.
    GitHub.dogstats.increment("actions.self_hosted_tenant_setup", tags: ["runner_type:repo"])
    ActiveRecord::Base.connected_to(role: :writing) do
      current_repository.enable_actions_app
    end
  end

  def ensure_tenant_exists
    result = GrpcHelper.rescue_from_grpc_errors("RepoTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(current_repository)
    end
    raise Timeout::Error, "Timeout fetching tenant" unless result.call_succeeded?
  end

  def unverified_or_spammy
    current_user.spammy? || current_repository.owner.spammy? || current_user.should_verify_email?
  end

  def skip_unverified_and_spammy
    render_404 if unverified_or_spammy
  end

  def repo_runners
    @_runners ||= Actions::Runner.for_entity(current_repository)
  end
end
