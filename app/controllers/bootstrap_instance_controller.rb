# frozen_string_literal: true

# Controller to handle the GitHub Private Instance bootstrap process.
class BootstrapInstanceController < ApplicationController
  include BusinessesHelper

  skip_before_action :private_instance_bootstrap_check
  before_action :private_instance_required
  before_action :login_required
  before_action :already_bootstrapped_check

  layout "bootstrap_instance"

  def index
    render "bootstrap_instance/index", locals: { business: global_business }
  end

  def update_enterprise_profile
    if GitHub.private_instance_bootstrapper.enterprise_profile_update.perform global_business, current_user, enterprise_profile_params
      GitHub.private_instance_bootstrapper.policies_configuration.set_defaults(global_business, current_user)
      redirect_to bootstrap_instance_configuration_path
    else
      render "bootstrap_instance/index", locals: { business: global_business }
    end
  end

  def configuration
    render "bootstrap_instance/configuration", locals: {
      business: global_business,
      step: params[:step],
      params: params_for_saml_test_result,
    }
  end

  def update_configuration
    step = params[:step]
    case step
    when "policies"
      config = GitHub.private_instance_bootstrapper.policies_configuration
      config.perform(global_business, current_user, params)
      config.complete!
    when GitHub.private_instance_bootstrapper.internal_support_contact.key_suffix
      config = GitHub.private_instance_bootstrapper.internal_support_contact
      errors = validate_support_link(support_link_params[:type], support_link_params[:support_address])
      if errors.any?
        flash[:error] = errors.to_sentence
        return redirect_to bootstrap_instance_configuration_path(config.key_suffix)
      else
        config.perform(**support_link_params)
      end
    end

    redirect_to bootstrap_instance_configuration_path
  end

  def commit
    if GitHub.private_instance_bootstrapper.can_be_committed?
      GitHub.private_instance_bootstrapper.committed_configuration.perform global_business, current_user
      redirect_to enterprise_path(global_business)
    else
      titles = GitHub.private_instance_bootstrapper.pending_required_bootstrap_setting_titles
      flash[:error] = "You need to complete these required configuration steps: #{titles.to_sentence}"
      return redirect_to bootstrap_instance_configuration_path
    end
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  private

  def support_link_params
    if params["internal_support_contact_type"] == "url"
      { type: params["internal_support_contact_type"], support_address: params["internal_support_contact_url"] }
    else
      { type: params["internal_support_contact_type"], support_address: params["internal_support_contact_email"] }
    end
  end

  def already_bootstrapped_check
    if GitHub.private_instance_bootstrapper.bootstrapped?
      redirect_to enterprise_path(global_business)
    end
  end

  def enterprise_profile_params
    params.require(:business).permit(:name)
  end

  def saml_idp_params
    params.require(:saml).permit(*GitHub.private_instance_bootstrapper.saml_idp_configuration.attribute_keys)
  end

  def global_business
    return @global_business if defined?(@global_business)
    @global_business = GitHub.global_business
  end
  alias_method :this_business, :global_business
  helper_method :this_business, :global_business
end
