# frozen_string_literal: true

class Hovercards::CommitsController < ApplicationController
  # opted out SAML & IP Allowlist to be handled manually in show action to render a custom SAML SSO interstitial
  include Hovercards::ConditionalAccessMethods

  areas_of_responsibility :user_profile

  statsd_tag_actions only: :show

  before_action :require_xhr, only: :show

  ShowQuery = parse_query <<-'GRAPHQL'
    query($repo_owner: String!, $repo_name: String!, $commit: GitObjectID) {
      repository(owner: $repo_owner, name: $repo_name) {
        ...Views::Hovercards::Commits::IpWhitelisting::Repository
        ...Views::Hovercards::Commits::SSO::Repository

        isPrivate
        owner {
          databaseId
        }
        object(oid: $commit) {
          ... on Commit {
            ...Views::Hovercards::Commits::Show::Commit
          }
        }
      }
    }
  GRAPHQL

  def show
    return render_404 unless this_commit
    return render_ip_whitelisting unless ip_whitelisting_policy_satisfied?
    return render_sso unless required_external_identity_session_present?

    render "hovercards/commits/show", locals: { commit: this_commit }, layout: false
  end

  private

  def render_ip_whitelisting
    render "hovercards/commits/ip_whitelisting",
      locals: { repository: this_repository },
      layout: false
  end

  # used to render in the conditional access enforcement pop-up
  def target_type
    "commit"
  end

  def render_sso
    render "hovercards/commits/sso", locals: { repository: this_repository, return_to_path: return_to_path }, layout: false
  end

  # only allow relative paths for the return_to option on the SSO link, otherwise link to the
  # user dashboard
  #
  def return_to_path
    return home_path unless params[:current_path]
    parsed = Addressable::URI.parse(params[:current_path])

    if parsed.relative? && parsed.host.nil?
      parsed.userinfo = nil
      parsed.normalize.to_s
    else
      home_path
    end
  end

  def this_commit
    git_object = this_repository&.object
    return nil unless git_object.is_a? PlatformTypes::Commit

    git_object
  end

  def this_repository
    graphql_data.repository
  end

  def graphql_data
    return @graphql_data if defined? @graphql_data

    variables = {
      repo_owner: params[:user_id],
      repo_name: params[:repository],
      commit: full_commit_oid,
    }
    # See https://github.com/github/ecosystem-api/issues/1546
    context = { nullify_private_repos_for_staff: true }
    @graphql_data = platform_execute(ShowQuery, variables: variables, context: context)
  end

  def full_commit_oid
    return @full_commit_oid if defined? @full_commit_oid

    # params[:name] could be a short oid or a ref name - we need the full oid for GraphQL.
    # Unfortunately we have to load the AR repo first to do this conversion.
    repo_owner = User.find_by_login(params[:user_id])
    return @full_commit_oid = nil unless repo_owner

    repo_model = repo_owner.find_repo_by_name(params[:repository])
    return @full_commit_oid = nil unless repo_model && repo_model.pullable_by?(current_user)

    @full_commit_oid = repo_model.ref_to_sha(params[:name])
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_repository&.is_private?

    # security issue: repository.owner does not take into account org-owned private repository forks
    # see https://github.com/github/iam/issues/1623 for more information
    target = User.find(this_repository.owner.database_id)
    return :no_target_for_conditional_access unless target
    target
  end
end
