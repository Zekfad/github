# frozen_string_literal: true

class Hovercards::TeamsController < ApplicationController
  areas_of_responsibility :user_profile

  statsd_tag_actions only: :show

  before_action :require_xhr, only: :show

  MEGA_TEAM_THRESHOLD = 50_000
  MEMBERS_TO_DISPLAY = 3

  def show
    return render_404 unless team.readable_by?(current_user)

    render "hovercards/teams/show",
      locals: {
      team: team,
      members: members,
      other_members_count: other_members_count,
      any_remaining: any_remaining,
    },
    layout: false
  end

  private

  def this_organization
    organization ||= Organization.find_by!(login: params[:org])
  end

  def team
    @team ||= this_organization.teams.find_by!(slug: params[:team_slug])
  end

  def target_for_conditional_access
    this_organization
  end

  def scope_builder
    Team::Membership::ScopeBuilder.new(
      team_id: team.id,
      viewer: current_user,
      max_members_limit: MEGA_TEAM_THRESHOLD,
    )
  end

  def members
    team.ranked_members_for(
      current_user,
      scope: scope_builder.scope,
      direction: "desc",
    ).first(MEMBERS_TO_DISPLAY)
  end

  def members_count
    @members_count ||= scope_builder.count
  end

  def other_members_count
    members_count - MEMBERS_TO_DISPLAY
  end

  def any_remaining
    other_members_count > MEMBERS_TO_DISPLAY
  end
end
