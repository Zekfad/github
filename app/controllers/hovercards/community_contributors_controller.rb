# frozen_string_literal: true

class Hovercards::CommunityContributorsController < ApplicationController
  include RepositoryControllerMethods

  before_action :require_xhr
  before_action :require_community_contributors_enabled
  before_action :require_public_repository

  def show
    render "hovercards/repositories/community_contributors", layout: false
  end

  CommunityContributorsQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $first: Int!) {
      repository: node(id: $id) {
        ...Views::Hovercards::Repositories::CommunityContributorsList::Repository
      }
    }
  GRAPHQL

  DEFAULT_COUNT = 20

  def list
    variables = { id: current_repository.global_relay_id, first: DEFAULT_COUNT }
    data = platform_execute(CommunityContributorsQuery, variables: variables)

    render "hovercards/repositories/community_contributors_list",
      locals: { repository: data.repository, default_count: DEFAULT_COUNT },
      layout: false
  end

  def community_contributors_cache_key
    contributors = current_repository.contributors
    contributors_count = contributors.computed? ? contributors.value.count : 0
    "community:contributors:list:#{current_repository.id}:#{contributors_count}"
  end
  helper_method :community_contributors_cache_key

  private

  def require_community_contributors_enabled
    return render_404 unless community_contributors_enabled?
  end

  def require_public_repository
    return render_404 unless current_repository&.public?
  end
end
