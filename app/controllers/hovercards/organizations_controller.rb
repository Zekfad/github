# frozen_string_literal: true

class Hovercards::OrganizationsController < ApplicationController
  areas_of_responsibility :user_profile

  statsd_tag_actions only: :show

  before_action :require_xhr, only: :show

  def show
    return render_404 if this_organization.spammy?

    # Calculate the members count
    user_ids = this_organization.visible_user_ids_for(current_user)
    members = User.with_ids(user_ids)
    @member_count = members.filter_spam_for(current_user).size

    # Calculate the repositories count
    repositories = this_organization.all_org_repos_for_user(current_user).active
    @repository_count = repositories.filter_spam_and_disabled_for(current_user).size

    render "hovercards/organizations/show", locals: {
      organization: this_organization,
      member_count: @member_count,
      repository_count: @repository_count,
    }, layout: false
  end

  private

  def this_organization
    return @this_organization if defined?(@this_organization)
    @this_organization = Organization.find_by_login!(params[:org])
  end

  def target_for_conditional_access
    this_organization
  end
end
