# frozen_string_literal: true
class AbuseReportsController < AbstractRepositoryController
  areas_of_responsibility :community_and_safety

  before_action :login_required
  before_action :ask_the_gatekeeper

  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: ReportContentInput!) {
      reportContent(input: $input) {
        errors {
          path
          message
        }
      }
    }
  GRAPHQL

  def create
    user_supplied_reason = params[:reason]&.upcase
    possible_reasons = Platform::Enums::AbuseReportReason.values.keys
    sanitized_reason = possible_reasons.include?(user_supplied_reason) ? user_supplied_reason : "UNSPECIFIED"

    data = platform_execute(CreateQuery, variables: { input: { reportedContentId: params[:comment_id], reason: sanitized_reason } })

    if data.errors.any?
      if data.errors.details["reportContent"].first["type"] == "NOT_FOUND"
        flash[:error] = "Content not found"
      else
        flash[:error] = data.errors.messages.values.join(", ")
      end
    elsif data.report_content.errors.any?
      flash[:error] = data.report_content.errors.map(&:message).to_sentence
    else
      flash[:notice] = "Your report has been submitted for review."
    end

    redirect_to :back
  end

  def resolve_abuse_reports
    return render_404 if params[:type] == "GistComment" or params[:type] == "Gist"

    if AbuseReport.mark_resolved_for_content(current_user, params[:type], params[:id])
      flash[:notice] = "Report marked as resolved"
    else
      flash[:error] = "You cannot perform that action at this time."
    end

    if request.xhr?
      respond_to do |format|
        format.html do
          render partial: "navigation/repository/main",
                 locals: { repository: current_repository, highlight: :tiered_reporting }
        end
      end
    else
     redirect_to reported_content_path(resolved_filter: params[:resolved_filter])
    end
  end

  def unresolve_abuse_reports
    return render_404 if params[:type] == "GistComment" or params[:type] == "Gist"

    if AbuseReport.mark_unresolved_for_content(current_user, params[:type], params[:id])
      flash[:notice] = "Report marked as unresolved"
    else
      flash[:error] = "You cannot perform that action at this time."
    end

    if request.xhr?
      respond_to do |format|
        format.html do
          render partial: "navigation/repository/main",
                 locals: { repository: current_repository, highlight: :tiered_reporting }
        end
      end
    else
     redirect_to reported_content_path(resolved_filter: params[:resolved_filter])
    end
  end
end
