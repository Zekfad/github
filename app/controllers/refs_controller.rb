# frozen_string_literal: true

class RefsController < GitContentController
  CONDITIONAL_ACCESS_BYPASS_PUBLIC_REPO_ACTIONS = %w(index tags).freeze
  NAME_PLACEHOLDER = "REPLACE_THIS_TOTALLY_UNIQUE_STRING_WITH_TARGET_NAME"
  TAGS_LIMIT = 100

  layout :repository_layout

  param_encoding :tags, :q, "ASCII-8BIT"

  def index
    url_prefix, url_suffix = ref_url_template_parts

    respond_to do |format|
      format.html do
        render partial: "refs/ref_list_content", layout: false, locals: {
          url_prefix: url_prefix,
          url_suffix: url_suffix,
          source_controller: params[:source_controller].to_s,
          source_action: params[:source_action].to_s,
        }
      end
    end
  rescue ActionController::UrlGenerationError
    return render_404
  end

  def tags
    search_query = params[:q].to_s
    tags = current_repository.tags.substring_filter(substring: search_query, limit: TAGS_LIMIT)
    url_prefix, url_suffix = ref_url_template_parts

    url_portion_callable = -> (tag) {
      escaped_tag = escape_url_branch(tag.name).gsub("%2B", "+")
      "#{url_prefix}#{escaped_tag}#{url_suffix}"
    }

    respond_to do |format|
      format.html do
        render "refs/tags",
          formats: :html,
          layout: false,
          locals: {
            url_portion_callable: url_portion_callable,
            current_tag_name: params[:tag_name],
            tags: tags,
          }
      end
    end
  rescue ActionController::UrlGenerationError
    return render_404
  end

  private

  def ref_url_template_parts
    url_for(
      controller: params[:source_controller].to_s,
      action: params[:source_action].to_s,
      name: NAME_PLACEHOLDER,
      path: params[:path],
    ).split(NAME_PLACEHOLDER, 2)
  end

  def route_supports_advisory_workspaces?
    true
  end

  # Bypass conditional access requirements for the specified actions if the
  # current action is exempt.
  def require_conditional_access_checks?
    return false if conditional_access_exempt?
    super
  end
end
