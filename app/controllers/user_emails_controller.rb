# frozen_string_literal: true

class UserEmailsController < ApplicationController
  skip_before_action :perform_conditional_access_checks

  before_action :login_required, :find_user_or_organization
  before_action :find_email, except: [:create, :confirm_verification, :toggle_visibility, :toggle_email_visibility_warning, :set_backup]
  before_action :ensure_authority
  before_action :sudo_filter, only: [:destroy]
  before_action :sudo_filter, only: [:create]
  before_action :limit_unverified_emails, only: [:create] if GitHub.email_verification_enabled?

  MAXIMUM_UNVERIFIED_EMAILS = 20
  BACKUP_EMAIL_PRIMARY_ONLY = "primary_only"
  BACKUP_EMAIL_ALLOW_ALL = "all"

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: :request_verification,
    key: :send_email_verification_rate_limit_key,
    log_key: :send_email_verification_log_key,
    max: 15,
    ttl: 1.hour,
    at_limit: :send_email_verification_rate_limit_render

  def create
    email = params[:user_email] && params[:user_email][:email]
    handle_dupe_email!(email) and return if UserEmail.duplicate?(email)

    user_email = if @user.organization?
      @user.add_email(email)
    else
      current_user.add_email(email)
    end

    if user_email && user_email.valid?
      redirect = params[:user_email] && params[:user_email][:redirect]
      if GitHub.email_verification_enabled? && user_email.request_verification(requested_by: current_user, redirect: redirect)
        flash[:notice] = "We sent a verification email to #{email}. Please follow the instructions in it."
      end

      # Track how often the claim form is actually used.
      if params[:ref] == "commit-claim-email-form"
        GitHub.dogstats.increment("contributions", tags: ["action:claim-form-used"])
      end
    else
      error =
        if user_email
          user_email.errors.full_messages.to_sentence
        else
          "email is already in use"
        end
      flash[:error] = "Error adding #{email}: #{error}."
    end

    redirect_to settings_user_emails_path
  end

  def destroy
    destroyed = current_user.remove_email @email
    if request.xhr?
      head :ok
    else
      if destroyed
        flash[:notice] = "Removed email #{@email} from your account."
      else
        flash[:error] = current_user.errors.full_messages.to_sentence
      end
      redirect_to settings_user_emails_path
    end
  end

  def toggle_visibility
    if @user.primary_user_email.toggle_visibility
      if @user.primary_user_email.private?
        @user.update!(warn_private_email: true)
      else
        @user.update!(warn_private_email: false)
      end
      flash[:toggle_visibility_notice] = true
      redirect_to settings_user_emails_path(@user)
    else
      flash[:error] = "Error changing visibility on your email"
      redirect_to settings_user_emails_path
    end
  end

  def toggle_email_visibility_warning
    if @user.toggle_warn_private_email
      if @user.warn_private_email?
        flash[:notice] = "Commits pushed with a private email will now be blocked and you will see a warning."
      else
        flash[:notice] = "Commits pushed with a private email will no longer be blocked."
      end
    else
      flash[:error] = "Error changing your private email warning settings."
    end
    redirect_to settings_user_emails_path
  end

  def set_primary
    verified_email_required =
      GitHub.email_verification_enabled? &&
      @user.emails.not_bouncing.user_entered_emails.verified.any? &&
      @email.unverified?
    if verified_email_required
      flash[:error] = "Please use a verified email address."
    elsif (set_primary_email_status = @user.set_primary_email(@email)).success?
      if notification_email_changed?(@email)
        flash[:prompt_notification_email] = true
      else
        flash[:notice] = "Your primary email was changed to #{@email}."
      end
    else
      flash[:error] = \
        "Error setting your primary email address: #{set_primary_email_status.error.downcase}"
    end

    redirect_to settings_user_emails_path
  end

  def set_backup
    if params[:id] == BACKUP_EMAIL_ALLOW_ALL
      @user.allow_password_reset_with_any_email
      email_type = if !GitHub.email_verification_enabled?
        "All"
      elsif @user.verified_emails?
        "All verified"
      else
        "All unverified"
      end
      flash[:notice] = "#{email_type} emails can now be used for password resets."
    elsif params[:id] == BACKUP_EMAIL_PRIMARY_ONLY
      @user.allow_password_reset_with_primary_email_only
      flash[:notice] = "Only your primary email address can now be used for password resets."
    else
      email = @user.emails.find(params[:id])
      if GitHub.email_verification_enabled? && email.unverified?
        flash[:error] = "Please use a verified email address."
      elsif @user.primary_user_email == email
        flash[:error] = "Please select the 'Only allow primary email' option."
      elsif @user.set_backup_email(email)
        flash[:notice] = "Your backup email was changed to #{email}."
      else
        flash[:error] = "Error setting your backup email address"
      end
    end

    redirect_to settings_user_emails_path
  end

  def request_verification
    if @email.request_verification
      flash[:notice] = "We sent a verification email to #{@email}. Please follow the instructions in it."
    else
      flash[:error] = if @email.disposable?
        "Emails from the '#{@email.domain}' domain cannot be verified."
      else
        "Could not send email verification."
      end
    end

    redirect_to_return_to(fallback: settings_user_emails_path)
  end

  def confirm_verification
    @email = UserEmail.find_email_for_verification(params[:id], owned_by: current_user)

    if @email.nil?
      flash[:error] = "Sorry, we couldn’t find an email to verify."
      return redirect_to settings_user_emails_path
    elsif @email.verified?
      flash[:notice] = "#{@email} is already verified."
      return redirect_to "/"
    elsif !@email.verification_token
      flash[:error] = "There was an error verifying your email. Please try re-verifying it."
      return redirect_to settings_user_emails_path
    end

    verification_succeeded = false
    ActiveRecord::Base.connected_to(role: :writing) do
      verification_succeeded = @email.confirm_verification(params[:t])
    end

    if verification_succeeded
      flash[:notice] = "Your email was verified."
      safe_redirect_to params.fetch(:redirect, get_started_path)
    else
      flash[:error] = "There was an error verifying your email."
      redirect_to settings_user_emails_path
    end
  end

  def access_denied
    redirect_to_login(request.url)
  end

  private

  # Set flash error and redirect on a duplicate email error
  def handle_dupe_email!(email)
    flash[:error] = "Error adding #{email}: email is already in use"
    redirect_to settings_user_emails_path
  end

  def find_user_or_organization
    @user = User.find_by_login!(params[:user_id] || params[:organization_id])
  end

  def find_email
    @email = @user.emails.find(params[:id])
  end

  def ensure_authority
    if @user.organization?
      unless (current_user.site_admin? || @user.adminable_by?(current_user))
        redirect_to :back, alert: "You must be an owner to add an email to this organization."
      end
    else
      redirect_to settings_user_emails_path unless (current_user.site_admin? || current_user == @user)
    end
  end

  def send_email_verification_rate_limit_key
    "resend_verification_email:#{current_user.id}"
  end

  def send_email_verification_log_key
    "resend-verification-email-#{current_user.id}"
  end

  def send_email_verification_rate_limit_render
    GitHub.dogstats.increment("rate_limited", tags: ["action:user_email_request_verification"])
    flash[:error] = "You have exceeded our email verification rate limit. Please try again later."
    redirect_to settings_user_emails_path
  end

  def limit_unverified_emails
    if @user.emails.user_entered_emails.unverified.count >= MAXIMUM_UNVERIFIED_EMAILS
      flash[:error] = "Please verify one or more of your unverified emails before adding another."
      redirect_to settings_user_emails_path
    end
  end

  def notification_email_changed?(primary_email)
    settings_response = GitHub.newsies.settings(@user)

    if settings_response.success?
      settings = settings_response.value
      notification_email = settings.email(:global).address

      return notification_email != primary_email.to_s
    end

    false
  end
end
