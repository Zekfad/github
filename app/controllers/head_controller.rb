# frozen_string_literal: true

class HeadController < ApplicationController
  areas_of_responsibility :unassigned
  before_action :login_required, except: [:manifest]

  def manifest
   expires_in 1.week, public: true
   manifest = {
      "name": "GitHub",
      "short_name": "GitHub",
      "icons": [{
        "sizes": "114x114",
        "src": image_path("/apple-touch-icon-114x114.png"),
      }, {
        "sizes": "120x120",
        "src": image_path("/apple-touch-icon-120x120.png"),
      }, {
        "sizes": "144x144",
        "src": image_path("/apple-touch-icon-144x144.png"),
      }, {
        "sizes": "152x152",
        "src": image_path("/apple-touch-icon-152x152.png"),
      }, {
        "sizes": "180x180",
        "src": image_path("/apple-touch-icon-180x180.png"),
      }, {
        "sizes": "57x57",
        "src": image_path("/apple-touch-icon-57x57.png"),
      }, {
        "sizes": "60x60",
        "src": image_path("/apple-touch-icon-60x60.png"),
      }, {
        "sizes": "72x72",
        "src": image_path("/apple-touch-icon-72x72.png"),
      }, {
        "sizes": "76x76",
        "src": image_path("/apple-touch-icon-76x76.png"),
      }, {
        "src": image_path("/app-icon-192.png"),
        "type": "image/png",
        "sizes": "192x192"
      }, {
        "src": image_path("/app-icon-512.png"),
        "type": "image/png",
        "sizes": "512x512"
      }],
      "prefer_related_applications": true,
      "related_applications": [
        {
          "platform": "play",
          "url": "https://play.google.com/store/apps/details?id=com.github.android",
          "id": "com.github.android"
        }
      ],
    }
    render json: manifest, content_type: "application/manifest+json"
  end

  private

  def require_conditional_access_checks?
    false
  end
end
