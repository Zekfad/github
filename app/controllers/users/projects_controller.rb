# frozen_string_literal: true

class Users::ProjectsController < Users::Controller
  include ProjectControllerActions
  include SharedProjectControllerActions

  statsd_tag_actions only: [:clone, :search_results, :index, :show]

  layout "user_projects"

  before_action :current_user_required, except: %w[index show]
  before_action :require_this_user, except: [:new]
  before_action :this_project_required, except: %w[index new create destroy linkable_repositories]
  before_action :project_read_required, except: %w[index new create destroy linkable_repositories]
  before_action :project_write_required, except: %w[index new create show activity target_owner_results destroy linkable_repositories clone]
  before_action :verify_cloning_conditional_access, only: [:clone]
  before_action :set_client_uid
  before_action :set_cache_control_no_store, only: %w[show search_results]

  def index
    if request.xhr? && !pjax?
      render_projects_index(owner: this_user)
    else
      # We need to do this manually because our overridden user_path helper
      # doesn't allow keyword args.
      args = "?tab=projects"
      args += "&q=#{params[:query]}" if params[:query].present?

      redirect_to user_path(this_user) + args
    end
  end

  def new
    @this_project = Project.new
    render_new_project(owner: current_user)
  end

  def edit
    override_analytics_location "/users/<user-login>/projects/<id>/edit"
    render_edit_project(owner: this_user, project: this_project)
  end

  def show
    override_analytics_location "/users/<user-login>/projects/<id>"
    strip_analytics_query_string
    render_show_project(owner: this_user, project: this_project)
  end

  def search_results
    render_project_search_results(owner: this_user, project: this_project)
  end

  def activity
    render_project_activity(owner: this_user, project: this_project)
  end

  def add_cards_link
    render_project_add_cards_link(project: this_project)
  end

  def create
    return render_404 if this_user != current_user

    create_project(owner: this_user)
  end

  def update
    update_project(owner: this_user, project: this_project)
  end

  def update_state
    update_project_state(project: this_project, state: params[:state], sync: params[:sync])
  end

  def clone
    clone_project(project: this_project)
  end

  def destroy
    destroy_project(owner: this_user)
  end

  def repository_results
    if params[:search_in] == "suggestions"
      repos = this_project.repository_suggestions(viewer: current_user, filter: params[:q])
    else
      query = Search::Queries::RepoQuery.new(
        current_user: current_user,
        remote_ip: request.remote_ip,
        phrase: "user:#{this_user.login} #{params[:query]}",
      )

      repos = query.execute.results.map { |result| result["_model"] }.select(&:has_issues?)
    end

    respond_to do |format|
      format.html_fragment do
        render partial: "projects/repository_results", formats: :html, locals: { repositories: repos }
      end
    end
  end

  def target_owner_results
    render_target_owner_results(project: this_project)
  end

  def linkable_repositories
    render_linkable_repositories(owner: this_user, project: this_project)
  end

  protected

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  private

  def this_project
    @this_project ||= this_user.projects.find_by(number: params[:number])
  end

  def this_project_required
    render_404 if this_project.nil?
  end

  def project_read_required
    render_404 unless this_project.readable_by?(current_user)
  end

  def project_write_required
    return true if this_project.writable_by?(current_user)
    return render_404 unless this_project.readable_by?(current_user)

    # If we can read the project but can't write it, show an informative error
    # message or status code.
    respond_to do |format|
      format.html_fragment do
        head 403
      end

      format.json do
        head 403
      end

      format.html do
        flash[:error] = "You don't have permission to perform this action on this project."
        redirect_to project_path(this_project)
      end
    end
  end

  def current_user_required
    render_404 unless logged_in?
  end

  helper_method :parsed_projects_query
  helper_method :project_index_query
end
