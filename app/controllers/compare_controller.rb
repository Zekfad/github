# frozen_string_literal: true

class CompareController < GitContentController
  map_to_service :diff
  map_to_service :ref, only: [:branch_list, :tag_list]

  statsd_tag_actions only: [:show, :branch_list]

  include CompareHelper, ControllerMethods::Diffs

  layout :repository_layout

  skip_before_action :try_to_expand_path
  before_action :check_for_empty_repository
  before_action :clean_params

  TAGS_LIMIT = 100

  param_encoding :tag_list, :q, "ASCII-8BIT"

  # Landing page for "what do I want to compare?" — starting point for branch
  # discussions and pull requests as well.
  def new
    head = current_repository.default_branch
    base = current_repository.base_branch(head, current_user)
    if base != head
      redirect_to compare_path(current_repository, "#{base}...#{head}")
    else
      # Prime up the range editor with an empty Comparison
      @comparison = current_repository.comparison(head, head)
      render "compare/new"
    end
  end

  ShowComparisonQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) { ...Views::Compare::Show }
    }
  GRAPHQL

  param_encoding :show, :range, "ASCII-8BIT"
  # A comparison without a discussion, kind of like a branch discussion in
  # waiting.
  def show
    unsafe_params = params.to_unsafe_h.with_indifferent_access

    if unsafe_params[:range].blank?
      redirect_to range: current_repository.default_branch
      return
    end

    @comparison = GitHub::Comparison.from_range(
      current_repository,
      unsafe_params[:range],
      limit: 250,
    )

    # monitoring feature usage for direct comparison for rollout period.
    GitHub.dogstats.increment("compare.direct") if @comparison.direct_compare?

    # Attempt to distinguish a compare view for an existing ref named
    # something.patch or something.diff from the patch/diff view for a
    # ref named something.
    head_ref_valid = @comparison.head_repo && @comparison.head_repo.refs.exist?(@comparison.head_ref)
    if @comparison.head.end_with?(".patch") && !head_ref_valid
      @comparison = GitHub::Comparison.deprecated_build(
        @comparison.repo, @comparison.base, @comparison.head.chomp(".patch"), limit: 250
      )
      return render_patch
    elsif @comparison.head.end_with?(".diff") && !head_ref_valid
      @comparison = GitHub::Comparison.deprecated_build(
        @comparison.repo, @comparison.base, @comparison.head.chomp(".diff"), limit: 250
      )
      return render_diff
    end

    if ref = unsafe_params[:new_compare_ref]
      if unsafe_params[:new_compare_type] == "base"
        redirect_to base_ref_comparison_path(@comparison, ref)
      else
        redirect_to head_ref_comparison_path(@comparison, ref)
      end
      return
    end

    # Since comparisons can be cross repository, we need to ensure you can't
    # see things you don't have permission to.
    if !@comparison.viewable_by?(current_user)
      # make a new in-repo comparison for the range_editor partial to not act weird
      @comparison = current_repository.comparison(@comparison.base_ref, @comparison.head_ref, limit=250)

      return render "compare/invalid", status: 404
    end

    if @comparison.base_repo && @comparison.base_repo != current_repository && !current_repository.advisory_workspace?
      flash.keep
      range = "#{@comparison.base_ref}...#{@comparison.head_user_login}:#{@comparison.head_ref}"
      return redirect_to(compare_path(@comparison.base_repo, range, unsafe_params[:expand]))
    end

    existing_pull_conditions = {
      base_repository_id: @comparison.base_repo.id,
      head_repository_id: @comparison.head_repo.id,
      base_ref: Git::Ref.permutations(@comparison.base_ref),
      head_ref: Git::Ref.permutations(@comparison.head_ref),
    }

    existing_pull_scope = @comparison.repo.pull_requests.filter_spam_for(current_user, show_spam_to_staff: false)

    # There can only be one open PR for a given comparison.
    existing_open_pull = existing_pull_scope.open_pulls.where(existing_pull_conditions).first

    # Pull requests that have been closed without merging
    # but still match the tip of the head branch.
    existing_closed_pulls = existing_pull_scope \
      .includes(:issue)
      .where(head_sha: @comparison.head_sha, merged_at: nil, issues: { state: "closed" })
      .order("pull_requests.id DESC")
      .limit((existing_open_pull.nil? ? 5 : 4))
      .to_a

    @existing_pulls = [existing_open_pull, *existing_closed_pulls].compact

    @comparison.set_diff_options(
      top_only:          true,
      use_summary:       true,
      ignore_whitespace: ignore_whitespace?,
      timeout:           request_time_left / 2,
    )

    # This will copy over missing commits from the base repo into the head repo
    # to make comparisons work across networks for advisory workspaces.
    @comparison.prepare!(origin_for_stats: "compare_show")

    if @comparison.valid?
      # run possibly-expensive GraphQL query before time-limited diff
      comparison_node = platform_execute(ShowComparisonQuery, variables: { id: @comparison.global_relay_id }).node

      # load diff
      GitHub.dogstats.time("diff.load.initial", tags: dogstats_request_tags) do
        @comparison.init_diffs.apply_auto_load_single_entry_limits!
        @comparison.diffs
      end
    end

    if !@comparison.valid? || @comparison.diffs.missing_commits?
      return render "compare/invalid", status: 404
    end

    # this makes links in the nav go to the right place ...
    unsafe_params[:name] = @comparison.head_ref

    if logged_in?
      @pull = @comparison.build_pull_request(user: current_user)
      @pull.build_issue repository: @pull.repository

      if flash[:pull_request]
        title_from_flash = flash[:pull_request]["title"]
        body_from_flash = flash[:pull_request]["body"]
      end

      if unsafe_params[:pull_request].is_a?(Hash)
        unsafe_params[:title] ||= unsafe_params[:pull_request][:title]
        unsafe_params[:body] ||= unsafe_params[:pull_request][:body]
      end

      fields = PrefilledIssueFields.new(
        params: unsafe_params,
        repository: current_repository,
        user: current_user,
      )

      @pull.body_template_name = fields.template
      @pull.issue.title = title_from_flash || fields.title
      @pull.issue.body = body_from_flash || fields.body
      @pull.issue.labels = fields.labels
      @pull.issue.projects = fields.projects
      @pull.issue.milestone = fields.milestone
      @pull.issue.assignees = fields.assignees

      @pull.issue.title = @pull.default_title if @pull.issue.title.blank?
      @pull.issue.body = @pull.default_body if @pull.issue.body.blank?
    end

    Commit.prefill_verified_signature(@comparison.commits, @comparison.head_repo)

    GitHub.dogstats.increment("compare.show.tabs", tags: ["tabs:#{render_tabs_on_compare?(@comparison)}"])

    GitHub.dogstats.time("view", tags: ["subject:compare", "action:show", "repo:#{repo_stats_key}", "branch:#{branch_stats_key}"]) do
      render "compare/show", locals: { comparison_node: comparison_node }
    end
  end

  rescue_from_timeout only: [:show] do |boom|
    # TODO this isn't quite right with regard to the .patch/.diff branch name
    # differentiation that takes place in #show.
    range = params[:range].sub(/\.(?:diff|patch)\z/, "")
    @comparison = GitHub::Comparison.from_range(current_repository, range, limit: 250)
    GitHub.dogstats.increment("compare.show.rescued_timeout", tags: ["repo:#{repo_stats_key}", "branch:#{branch_stats_key}"])

    render "compare/timeout"
  end

  def branch_list
    return render_404 unless params[:type]

    range = params[:range] || current_repository.default_branch
    comparison = GitHub::Comparison.from_range(current_repository, range, limit: 250)

    respond_to do |format|
      format.html do
        render partial: "compare/commitish_suggester_content", layout: false, locals: {
          comparison: comparison,
          type: params[:type].to_sym,
          selected: params[:selected],
          expand: params[:expand],
        }
      end
    end
  end

  def tag_list
    search_query = params[:q].to_s
    range = params[:range] || current_repository.default_branch
    comparison = GitHub::Comparison.from_range(current_repository, range, limit: 250)
    target_repo = params[:type] == "head" ? comparison.head_repo : comparison.base_repo
    target_repo ||= comparison.repo

    tags = target_repo.tags.substring_filter(substring: search_query, limit: TAGS_LIMIT)

    url_portion_callable = -> (tag) {
      if params[:type] == "base"
        base_ref_comparison_path(comparison, tag.name, expand: params[:expand])
      else
        head_ref_comparison_path(comparison, tag.name, expand: params[:expand])
      end
    }

    respond_to do |format|
      format.html_fragment do
        render "refs/tags",
          formats: :html,
          layout: false,
          locals: {
            url_portion_callable: url_portion_callable,
            current_tag_name: params[:tag_name],
            tags: tags,
          }
      end
    end
  end

  # Show this comparison as a .diff
  def diff
    timeout_client_error do
      set_request_category! "raw"
      show
      render_diff
    end
  end

  # Show this comparison as a .patch
  def patch
    timeout_client_error do
      set_request_category! "raw"
      show
      render_patch
    end
  end

  private
  # If it's empty, you can't compare nothin'
  def check_for_empty_repository
    if current_repository.empty?
      redirect_to current_repository
    end
  end

  # Internal: clean out the params hash
  #
  # sets blank params to nil
  def clean_params
    params[:quick_pull] = nil if params[:quick_pull] && params[:quick_pull].blank?
    true
  end

  # Internal: render a plain text .patch representation
  def render_patch
    return render "compare/invalid", status: 404 if !@comparison.valid?
    render plain: @comparison.to_patch if !performed?
  end

  # Internal: render a plain text .diff representation
  def render_diff
    return render "compare/invalid", status: 404 if !@comparison.valid?
    render plain: @comparison.to_diff if !performed?
  end

  def repo_stats_key
    if @comparison.base_repo == @comparison.head_repo
      "same_repo"
    elsif @comparison.base_repo == @comparison.head_repo.parent
      "from_parent"
    elsif @comparison.base_repo == @comparison.head_repo.network.root
      "from_network_root"
    elsif @comparison.head_repo == @comparison.base_repo.parent
      "to_parent"
    elsif @comparison.head_repo == @comparison.base_repo.network.root
      "to_network_root"
    else
      "other_repo"
    end
  end

  def branch_stats_key
    if @comparison.base_ref == @comparison.base_repo.default_branch
      if @comparison.head_ref == @comparison.head_repo.default_branch
        "both_default_branch"
      else
        "from_default_branch"
      end
    elsif @comparison.head_ref == @comparison.head_repo.default_branch
      "to_default_branch"
    else
      "other_branch"
    end
  end

  def route_supports_advisory_workspaces?
    true
  end
end
