# frozen_string_literal: true

class DiscussionsController < Discussions::BaseController
  statsd_tag_actions only: [:index, :show]

  before_action :login_required, only: [:create, :update, :destroy, :new, :lock, :unlock]
  before_action :handle_deleted_discussion, only: :show
  before_action :handle_transferred_discussion, only: :show
  before_action :handle_issue_redirect, only: :show
  before_action :require_discussion, only: [
    :show, :title, :body, :sidebar, :block_from_comment_modal, :timeline,
    :update, :destroy, :lock, :unlock, :open_new_issue_modal, :comment_header_reaction_button
  ]
  before_action :require_ability_to_open_discussion, only: [:new, :create]
  before_action :require_repository_not_migrating, only: [:lock, :unlock]
  before_action :require_ability_to_lock_discussion, only: [:lock]
  before_action :require_ability_to_unlock_discussion, only: [:unlock]
  before_action :add_spamurai_form_signals, only: [:create, :lock, :unlock, :update]

  helper_method :render_layout?

  def index
    list_flow = Discussion::ListControlFlow.new(
      params: params,
      parsed_discussions_query: parsed_discussions_query,
      repo: current_repository)

    return safe_redirect_to(list_flow.redirect_path) if list_flow.needs_redirection?

    update_discussions_last_viewed_timestamp

    type_filter = params[:type]

    discussions = load_discussions(
      type_filter: type_filter,
      search_query: raw_discussions_search_query.presence && parsed_discussions_query,
    )

    participants_by_discussion_id = Discussion.participants_by_discussion_id(discussions,
      viewer: current_user)

    spotlights = if discussion_spotlights_enabled?
      current_repository.discussion_spotlights.includes(discussion: [:category, :user])
    end

    render "discussions/index",
      layout: default_layout_or_override,
      locals: {
        query: sanitized_query_string,
        discussions: discussions,
        discussion_categories: current_repository.discussion_categories.sort,
        type_filter: type_filter,
        participants_by_discussion_id: participants_by_discussion_id,
        spotlights: spotlights,
      }
  end

  def author_filter_content
    filtered_authors = Set.new(helpers.discussions_search_term_values(:author))

    users = Discussion.authors_in_repo(current_repository).filter_spam_for(current_user)
    sorted_users = users.partition do |user|
      (user.is_a?(Bot) && filtered_authors.include?("app/#{user.display_login}")) ||
        filtered_authors.include?(user.login)
    end

    respond_to do |format|
      format.html do
        render partial: "discussions/author_filter_content", locals: {
          users: sorted_users.flatten,
          query: sanitized_query_string,
        }
      end
    end
  end

  def show
    if discussion.converting? || discussion.error?
      return render("discussions/show_conversion", locals: { discussion: discussion })
    end

    update_discussions_last_viewed_timestamp
    mark_thread_as_read discussion
    set_hovercard_subject(discussion)

    respond_to do |format|
      format.html do
        render "discussions/show",
          layout: default_layout_or_override,
          locals: {
            discussion: discussion,
            timeline: discussion_timeline,
          }
      end

      format.json do
        render json: { title: discussion.title }
      end
    end
  end

  def comment_header_reaction_button
    respond_to do |format|
      format.html do
        render partial: "discussions/header_reaction_button",
               locals: { discussion_or_comment: discussion, timeline: discussion_timeline }
      end
    end
  end

  def title
    respond_to do |format|
      format.html do
        render partial: "discussions/title",
               locals: { discussion: discussion, timeline: discussion_timeline }
      end
    end
  end

  def body
    respond_to do |format|
      format.html do
        render partial: "discussions/body", locals: { timeline: discussion_timeline }
      end
    end
  end

  def sidebar
    respond_to do |format|
      format.html do
        render partial: "discussions/sidebar", locals: {
          discussion: discussion, timeline: discussion_timeline
        }
      end
    end
  end

  def timeline
    render_context = DiscussionTimeline::LiveUpdatesRenderContext.new(
      discussion,
      viewer: current_user,
      timeline_last_rendered: params[:timeline_last_modified],
    )

    discussion_timeline = DiscussionTimeline.new(render_context: render_context)

    respond_to do |format|
      format.html do
        render partial: "discussions/timeline", locals: { timeline: discussion_timeline }
      end
    end
  end

  def block_from_comment_modal
    comment = if params[:comment_id]
      discussion.comments.find_by_id(params[:comment_id])
    else
      discussion
    end
    return render_404 unless comment

    respond_to do |format|
      format.html do
        render partial: "discussions/block_from_comment_modal", locals: { comment: comment }
      end
    end
  end

  def new
    discussion = current_repository.discussions.question.new
    if params[:welcome_text]
      discussion.title = "Welcome to #{current_repository.name} Discussions!"
      discussion.body = Discussion::WELCOME_BODY
    end

    render "discussions/new", locals: { discussion: discussion, category: category, welcome_message: params[:welcome_text] }
  end

  def create
    repo = current_repository
    return render_404 unless repo
    return if reject_user_for_repo?(repo)

    discussion = Discussion.new(discussion_params)
    discussion.repository = repo
    discussion.discussion_category_id = category&.id
    discussion.user = current_user

    if discussion.save
      redirect_to discussion_path(discussion)
    else
      flash[:error] = "Could not start a discussion at this time."
      redirect_to discussions_path(repo.owner, repo)
    end
  end

  def open_new_issue_modal
    respond_to do |format|
      format.html do
        render partial: "discussions/open_new_issue_modal", locals: {
          comment: discussion, timeline: discussion_timeline
        }
      end
    end
  end

  def update
    return render_404 unless discussion.modifiable_by?(current_user)

    if stale_model?(discussion)
      return render_stale_error(model: discussion,
        error: "Could not edit discussion. Please try again.", path: discussion_path(discussion))
    end

    # Set the actor so we can log a Hydro event about this discussion
    # being updated.
    discussion.actor = current_user

    discussion.assign_attributes(discussion_params)
    return render_discussion_update_error unless discussion.valid?

    if update_discussion
      if request.xhr?
        render json: {
          "source" => discussion.body,
          "body" => discussion.body_html,
          "newBodyVersion" => discussion.body_version,
          "editUrl" => edits_menu_discussion_path(current_repository.owner,
            current_repository, discussion),
          "issue_title" => discussion.title,
          "page_title" => helpers.discussion_page_title(discussion),
          "category_id" => discussion.category_id,
        }
      else
        redirect_to discussion_path(discussion)
      end
    else
      render_discussion_update_error
    end
  end

  def destroy
    return render_404 unless discussion.deletable_by?(current_user)

    deleter = DiscussionDeleter.new(discussion)

    if deleter.delete(current_user)
      if request.xhr?
        head :ok
      else
        redirect_to discussions_path(current_repository.owner, current_repository)
      end
    else
      if request.xhr?
        render json: discussion.errors.full_messages, status: :unprocessable_entity
      else
        flash[:error] = "Could not delete the discussion at this time."
        redirect_to discussion_path(discussion)
      end
    end
  end

  def lock
    unless discussion.locked?
      # Set the actor so we can log a Hydro event about this discussion
      # being updated.
      discussion.actor = current_user

      discussion.lock(current_user)
    end
    redirect_to discussion_path(discussion)
  end

  def unlock
    if discussion.locked?
      # Set the actor so we can log a Hydro event about this discussion
      # being updated.
      discussion.actor = current_user

      discussion.unlock(current_user)
    end
    redirect_to discussion_path(discussion)
  end

  private

  def update_discussions_last_viewed_timestamp
    return unless logged_in?
    return unless current_repository.discussions_activity_indicator_enabled?

    ActiveRecord::Base.connected_to(role: :writing) do
      current_repository.update_discussions_last_viewed_for(current_user)
    end
  end

  def default_layout_or_override
    render_layout? ? :default : false
  end

  def render_layout?
    requested_no_layout = params[:no_layout] == "1"
    !requested_no_layout
  end

  def load_discussions(type_filter:, search_query:)
    if search_query.present?
      Discussion::SearchResult.search(
        query: parsed_discussions_query,
        page: current_page,
        per_page: DEFAULT_PER_PAGE,
        repo: current_repository,
        current_user: current_user,
        remote_ip: request.remote_ip,
        user_session: user_session
      )
    else
      current_repository.discussions.
        filter_spam_for(current_user).
        paginate(page: current_page, per_page: DEFAULT_PER_PAGE).
        filter_by_type(type_filter).
        recently_updated_first
    end
  end

  def category
    if params[:category_id].present?
      current_repository.discussion_categories.find(params[:category_id])
    end
  end

  def sanitized_query_string
    @sanitized_query_string ||= Search::Queries::DiscussionQuery.
      stringify(parsed_discussions_query)
  end

  def parsed_discussions_query
    @parsed_discussions_query ||= Search::Queries::DiscussionQuery.normalize(
      Search::Queries::DiscussionQuery.parse(raw_discussions_search_query, current_user),
    )
  end
  helper_method :parsed_discussions_query

  def raw_discussions_search_query
    params[:discussions_q]
  end

  def discussion_timeline
    @discussion_timeline ||= begin
      render_context = if GitHub.flipper[:discussions_show_more].enabled?(current_repository)
        DiscussionTimeline::PaginatedRenderContext.new(
          discussion,
          viewer: current_user,
        )
      else
        DiscussionTimeline::AllCommentsRenderContext.new(
          discussion,
          viewer: current_user,
        )
      end

      DiscussionTimeline.new(render_context: render_context)
    end
  end

  def render_discussion_update_error
    if request.xhr?
      return render json: discussion.errors.full_messages, status: :unprocessable_entity
    end

    flash[:error] = "Could not edit the discussion at this time."
    redirect_to discussion_path(discussion)
  end

  def discussion_params
    permitted_fields = [:title, :body]
    permitted_fields << :category_id if discussion_categories_enabled?
    params.require(:discussion).permit(*permitted_fields)
  end

  def handle_issue_redirect
    if discussion.nil? && params[:number]
      issue = current_repository.issues.find_by_number(params[:number])
      return unless issue

      if params[:converting] == "1"
        flash[:error] = "Unable to convert this issue to a discussion."
      end

      redirect_to issue_path(issue)
    end
  end

  def require_repository_not_migrating
    if current_repository.locked_on_migration?
      flash[:error] = "You can't perform that action at this time, the repository has been " \
        "locked for migration."
      redirect_to discussion_path(discussion)
    end
  end

  def require_ability_to_lock_discussion
    unless discussion.lockable_by?(current_user)
      flash[:error] = "You can't perform that action at this time."
      redirect_to discussion_path(discussion)
    end
  end

  def require_ability_to_unlock_discussion
    unless discussion.unlockable_by?(current_user)
      flash[:error] = "You can't perform that action at this time."
      redirect_to discussion_path(discussion)
    end
  end

  def reject_user_for_repo?(repo, discussion: nil)
    return false if repo.pushable_by?(current_user)

    if blocked_by_owner?(repo.owner_id) || (discussion && blocked_by_author?(discussion.author))
      flash[:error] = "You can't perform that action at this time."
      redirect_to repo.permalink
      true
    end
  end

  def handle_deleted_discussion
    return if discussion

    deleted_discussion = DeletedDiscussion.for_repository(current_repository).
      with_number(params[:number]).first
    if deleted_discussion
      render "discussions/deleted", locals: { deleted_discussion: deleted_discussion }
    end
  end

  def handle_transferred_discussion
    if transfer = transfer_where_new_discussion_still_exists
      new_repo = transfer.new_repository

      if new_repo&.readable_by?(current_user) && !new_repo.hide_from_user?(current_user)
        flash[:notice] = "This discussion was transferred here."
        redirect_to discussion_path(transfer.new_discussion, new_repo)
      else
        render "discussions/transfer_no_access"
      end
    elsif discussion_transferred_then_deleted?
      render "discussions/transfer_no_access", discussion_deleted: true
    end
  end

  def transfer_where_new_discussion_still_exists
    if defined?(@transfer_where_new_discussion_still_exists)
      return @transfer_where_new_discussion_still_exists
    end
    @transfer_where_new_discussion_still_exists = DiscussionTransfer.find_from(
      repository: current_repository,
      number: params[:number],
    )
  end

  def discussion_transferred_then_deleted?
    return false if transfer_where_new_discussion_still_exists

    DiscussionTransfer.find_by(
      old_repository_id: current_repository.id,
      old_discussion_number: params[:number],
    )
  end

  def require_feature
    render_404 unless show_discussions?
  end

  def update_discussion
    if operation = TaskListOperation.from(params[:task_list_operation])
      text = operation.call(discussion.body)
      discussion.update_body(text, current_user)
    elsif discussion_params[:body]
      discussion.update_body(discussion_params[:body], current_user)
    else
      discussion.save
    end
  end
end
