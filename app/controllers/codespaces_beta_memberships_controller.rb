# frozen_string_literal: true

class CodespacesBetaMembershipsController < ApplicationController
  include AccountMembershipHelper

  FEATURE_SLUG = "workspaces"

  before_action :login_required
  before_action :feature_required

  def signup
    render_template_view(
      "codespaces_beta_memberships/signup",
      CodespacesBetaMemberships::View,
      membership: PrereleaseProgramMember.new,
      survey: survey,
    )
  end

  def agree
    if current_user.should_verify_email?
      flash[:error] = "Signing up for the Codespaces beta requires a verified email address."
      render_email_verification_required
      return
    end

    membership = create_early_access_membership
    pre_release_member = create_pre_release_member

    if membership.persisted? && pre_release_member.persisted?
      GitHub.dogstats.increment("codespaces.joined_waitlist")

      if params[:redirect_back]
        safe_redirect_to params[:redirect_back]
      else
        redirect_to codespaces_beta_thanks_path
      end
    else
      field_errors = membership.errors.full_messages + pre_release_member.errors.full_messages
      flash[:error] = "Sorry that didn’t work. #{field_errors.to_sentence}."
      redirect_to codespaces_beta_signup_path
    end
  end

  def thanks
    if EarlyAccessMembership.workspaces_waitlist.exists?(member: current_user)
      render "codespaces_beta_memberships/thanks"
    else
      redirect_to codespaces_beta_signup_path
    end
  end

  private

  def create_early_access_membership
    membership = EarlyAccessMembership.new(
      member_id: current_user.id,
      actor_id: current_user.id,
      feature_slug: FEATURE_SLUG,
      survey: survey,
    )

    if survey_answers.present?
      membership.save_with_survey_answers(survey_answers)
    else
      membership.save
    end

    membership
  end

  def create_pre_release_member
    attrs = { member_id: current_user.id, actor_id: current_user.id }
    PrereleaseProgramMember.find_by(attrs) || PrereleaseProgramMember.create(attrs)
  end

  def survey
    @survey ||= Survey.find_by(slug: FEATURE_SLUG)
  end

  def survey_answers
    @survey_answers ||= Array(params[:answers]).select do |answer|
      next false unless answer[:choice_id].present?

      # Ignore answers of empty text
      answer.key?(:other_text) ? answer[:other_text].present? : true
    end
  end

  def feature_required
    render_404 unless workspaces_sign_up_enabled?
  end
end
