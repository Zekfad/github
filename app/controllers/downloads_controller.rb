# frozen_string_literal: true

class DownloadsController < AbstractRepositoryController

  before_action :pushers_only,  only: [:destroy]
  before_action :find_download, only: [:show]
  layout :repository_layout

  def index
    @uploaded_downloads = current_repository.downloads.sort_by(&:timestamp).reverse
    SlottedCounterService.prefill @uploaded_downloads

    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "downloads/uploaded_downloads", object: @uploaded_downloads
        else
          render "downloads/index"
        end
      end
    end
  end

  def show
    if @download.file_exists?
      @download.hit!
      redirect_to @download.url, status: 302
    else
      GitHub.dogstats.increment("download", tags: ["error:not_found"])
      flash[:error] = "File was not found."
      redirect_to action: :index, id: nil
    end
  rescue Aws::S3::Errors::ServiceError # timeout or something
    flash[:error] = "Error downloading file."
    redirect_to action: :index, id: nil
  end

  def destroy
    if download = current_repository.downloads.find_by_id(params[:id])
      download.destroy
      flash[:notice] = "#{download} was deleted"
      redirect_to gh_downloads_path(current_repository)
    else
      render_404
    end
  end

private
  def find_download
    dl_file   = params[:file] || params[:name]
    @download = current_repository.downloads.find_by_name(dl_file) if dl_file.present?
    render_404 if !@download
  end

  def valid_oauth_scopes
    return %w(invalid) unless current_repository
    scopes = [:repo]
    scopes << :public_repo if current_repository.public?
    scopes
  end

  def redirect_to_new_repository_location
    if match = request.fullpath.scan(%r{\A/downloads/([\w.-]+/[\w.-]+)(.*)\z}).first
      redirect_to_new_repository_location_with match[0], "/downloads", match[1]
    else
      super
    end
  end
end
