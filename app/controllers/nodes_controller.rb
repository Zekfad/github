# frozen_string_literal: true

# NOTE: This controller has been DEPRECATED! Please do not add new partials to
# it. We are moving away from using internal GraphQL schema for views.

# Next Generation "show partial" endpoint.
#
# NodesController#show exposes a single endpoint clients can access to re-render
# parts of the page.
#
#   GET /_render_node/MDU6SXNzdWUzNDEy/comments/show
#
# Prefer using the `show_node_partial_path` URL helper to generate this path.
#
#   show_node_partial_path(path: "comments/show", id: comment.id)
#
# In order to use this endpoint, partials MUST fetch all their data from GraphQL
# and define their dependencies via an ERB GraphQL query.
#
# Obsoletes most of our existing `#show_partial` action usage.
class NodesController < ApplicationController
  # This controller is a general purpose controller, not specific to domain of
  # the application. But most of these partials are used by live updates, so
  # we'll use that for now.
  areas_of_responsibility :live_updates

  statsd_tag_actions only: :show_partial

  # XXX: This controller doesn't require any authorization checks. Data
  # loading is handled by GraphQL Platform. Conditional access is enforced via the
  # `enforce_conditional_access_via_graphql` context option.
  skip_before_action :perform_conditional_access_checks

  # Internal: Registry of partials.
  @@partials = {}
  cattr_reader :partials

  # Internal: Container to register static show action queries.
  module Queries
  end

  module Fragments
  end

  # Public: Expose partial via #show action.
  #
  # partial - String render path of partial.
  #           Just "comments/show", not "app/views/comments/_reactions.html.erb".
  #
  # fragment - GraphQL::Client fragment to fetch data for. Assumes the first
  #            fragment defined by the partial by default.
  #
  # local_assign - Symbol local name the data should be assigned to in the
  #                partial. Assumes a downcased name of the fragment.
  #
  # Returns nothing.
  def self.route_partial(partial, fragment: nil, local_assign: nil, id_method: nil)
    unless fragment
      namespace = "views/#{partial}".camelize.constantize
      fragment = "#{namespace}::#{namespace.constants.first}".constantize
    end

    # comment
    local_assign ||= fragment.name.split("::").last.underscore

    if fragment.definition_node.type.name == "Query"
      # query { ...Views::Dashboard::Index::Data }
      query = PlatformHelper::PlatformClient.create_operation(fragment)
      type = :query
    else
      # query($id: ID!) { node(id: $id) { ...Views::Comments::Show::Comment } }
      node_fragment_const_name = partial.camelize.gsub("::", "") + "Fragment"
      node_fragment = Fragments.const_set(
        node_fragment_const_name,
        parse_query("fragment on Query { node(id: $id) { ...#{fragment.name} } }"),
      )

      query = PlatformHelper::PlatformClient.create_operation(node_fragment)
      type = :node
    end

    # NodesController::Queries::CommentsShow
    query_const_name = partial.camelize.gsub("::", "")
    Queries.const_set(query_const_name, query)

    schema_klass = PlatformTypes.const_get(fragment.definition_node.type.name)

    @@partials[partial] = {
      type: type,
      path: partial,
      local_assign: local_assign.to_sym,
      node_fragment: node_fragment,
      fragment: fragment,
      schema_klass: schema_klass,
      query: query,
      id_method: id_method,
    }
  end

  # Only allow id and path params. Views MUST NOT depend on any additional
  # URL parameters in order to use this actions.
  ALLOWED_PARAM_KEYS = %i(controller action id path variables utf8 _features legacy)

  # GET /_render_node/MDU6SXNzdWUzNDEy/comments/show
  def show

    # Check if partial is registered, otherwise 404
    unless partial = @@partials[node_params[:path]]
      return head :not_found
    end

    variables = node_params[:variables]&.to_h || {}

    if partial[:type] == :node
      if node_params[:legacy].nil? && partial[:id_method]
        # Pass in original params
        variables[:id] = self.send(partial[:id_method])
      else
        variables[:id] = node_params[:id]
      end
    end

    type_cast_variables!(variables, partial[:query].definition_node.variables)
    params[:variables] = variables

    begin
      data = platform_execute(partial[:query], variables: variables, context: { enforce_conditional_access_via_graphql: true })
    rescue PlatformHelper::ConditionalAccessError => e
      # this is an xhr request, so follow the pattern in
      # https://github.com/github/github/blob/4bf41fdfcdc9b7b30615e5dc7df6ae55bf73c583/app/controllers/application_controller/external_sessions_dependency.rb#L186-L187
      return head :unauthorized
    rescue PlatformHelper::ExecutionError => e
      if e.message =~ /\AVariable.*was provided invalid value\Z/
        return head :bad_request
      else
        raise
      end
    end

    locals = { variables: variables }

    if node_params[:id] == "query"
      locals[partial[:local_assign]] = data
    else
      data = partial[:node_fragment].new(data)

      unless data.node
        return head :not_found
      end

      # Detect if node was found but was unexpected type.
      unless data.node.is_a?(partial[:schema_klass])
        return head :not_found
      end

      locals[partial[:local_assign]] = data.node
    end

    respond_to do |format|
      GitHub.dogstats.increment "nodes.show", tags: %W(type:#{partial[:schema_klass].type.graphql_name.underscore} partial:#{partial[:path]})
      format.html_fragment do
        render partial: partial[:path], formats: [:html], locals: locals # rubocop:disable GitHub/RailsControllerRenderLiteral
      end
      format.html do
        render partial: partial[:path], locals: locals # rubocop:disable GitHub/RailsControllerRenderLiteral
      end
    end
  end

  if Rails.env.test?
    route_partial "nodes/test_node"
    route_partial "nodes/test_root"
  end

  # Routed Partials
  #
  # These partials are exposed via the #show action.
  #
  # Keep list sorted alphabetically.
  #
  # DEPRECATED! PLEASE DO NOT ADD TO THIS LIST
  route_partial "comments/block_from_comment_modal"
  route_partial "comments/comment_edit_history_log"
  route_partial "comments/comment_header_reaction_button"
  route_partial "comments/reactions"
  route_partial "comments/review_comment"
  route_partial "commit/pull_condensed", id_method: :pull_request_commit
  route_partial "commit/condensed", id_method: :repository_commit
  route_partial "commits/commits_list_item", id_method: :repository_commit
  route_partial "commits/pull_commits_list_item", id_method: :pull_request_commit
  route_partial "compare/commit", id_method: :repository_commit
  route_partial "issues/body"
  route_partial "issues/unread_timeline"
  route_partial "issues/transfer_form"
  route_partial "issues/transfer_form_possible_repositories"
  route_partial "network/dependencies_list"
  route_partial "notification_subscriptions/repository_filter"
  route_partial "orgs/team_discussion_comments/cursor_input"
  route_partial "orgs/team_discussions/node"
  route_partial "orgs/team_discussions/cursor_input"
  route_partial "projects/fullscreen_header"
  route_partial "projects/header"
  route_partial "projects/panes/metadata"
  route_partial "projects/show_progress"
  route_partial "pull_requests/body"
  route_partial "pull_requests/commits_partial"
  route_partial "pull_requests/events/deployed", local_assign: :event
  route_partial "pull_requests/events/deployment_environment_changed", local_assign: :event
  route_partial "pull_requests/unread_timeline"
  route_partial "pull_request_commit_comment_threads/more_comments"
  route_partial "pull_request_reviews/body"
  route_partial "pull_request_reviews/more_threads"
  route_partial "pull_request_review_threads/discussion"
  route_partial "pull_request_review_threads/more_comments"
  route_partial "pull_request_review_threads/reply"
  route_partial "mobile/discussions/comment_thread/more_comments"
  route_partial "statuses/combined_branch_status"
  route_partial "timeline/issue_comment"
  route_partial "timeline/more_items"
  # DEPRECATED! PLEASE DO NOT ADD TO THIS LIST

  private

  # Internal: Type casts param variables to the specified type of the query variable
  #
  # - params_variables     - Hash of param variables
  # - variable_definitions - Array of `GraphQL::Language::Nodes::VariableDefinition`s
  #
  # Returns nothing
  def type_cast_variables!(params_variables, variable_definitions)
    variable_definitions.each do |variable|
      base_type = variable.type
      if base_type.is_a? GraphQL::Language::Nodes::NonNullType
        base_type = base_type.of_type
      end
      next unless base_type.respond_to?(:name)
      next unless value = params_variables[variable.name]

      casted_value =
        case base_type.name
        when "Boolean"
          ActiveRecord::Type::Boolean.new.deserialize(value)
        when "Int"
          ActiveRecord::Type::Integer.new.deserialize(value)
        end

      params_variables[variable.name] = casted_value unless casted_value.nil?
    end
  end

  def node_params
    @node_params ||= begin
      # Since we raise on unallowed keys, we want to drop them
      # here first and only add allowed ones.
      filtered = params.class.new
      ALLOWED_PARAM_KEYS.each do |key|
        val = params[key]
        val.permit! if val.respond_to?(:permit!)
        filtered[key] = val
      end

      filtered.permit!
      filtered
    end
  end

  def current_repository
    return @current_repository if defined?(@current_repository)
    @current_repository = owner.find_repo_by_name(params[:repository]) if owner && params[:repository]
  end

  def owner
    return @owner if defined?(@owner)
    @owner = User.find_by_login(params[:user_id]) if params[:user_id]
  end

  def pull_request_commit
    return nil unless current_repository
    pull = PullRequest.with_number_and_repo(params.require(:id), current_repository)
    return nil unless pull
    pull.async_load_pull_request_commit(params.require(:sha)).sync&.global_relay_id
  end

  def repository_commit
    return nil unless current_repository
    current_repository.commits.find(params.require(:name))&.global_relay_id
  rescue GitRPC::ObjectMissing, GitRPC::InvalidObject, RepositoryObjectsCollection::InvalidObjectId
    nil
  end

end
