# frozen_string_literal: true

module ScanningControllerMethods
  include PlatformHelper

  CommitsQuery = parse_query <<-'GRAPHQL'
    query($ids: [ID!]!) {
      nodes(ids: $ids) {
        ...PlatformPreloadHelper::BaseFragment
        ...Views::Commit::Condensed::Commit
        ... on Commit {
          oid
        }
      }
    }
  GRAPHQL

  # Load the commits for the given commit oids.
  #
  # Returns a tuple of an array of raw Commit objects from `app/models/commit.rb`
  # and an array of GraphQL commit objects that can be rendered by the shared
  # `app/views/repos/scanning/_commit.html.erb` view.
  def load_commits(commit_oids)
    commits = Platform::Loaders::GitObject.load_all(current_repository, commit_oids.uniq, expected_type: "commit").sync
    graphql_commits = nil

    preload_platform_objects(commits) do |ids|
      graphql_commits = platform_execute(CommitsQuery, variables: { ids: ids }).nodes
    end

    return commits, graphql_commits
  end

  # Load the commits for the given commit ids and return them as hashed indexed
  # by commit oid.
  #
  # Returns a tuple of an hashes of raw Commit objects from `app/models/commit.rb`
  # and an array of GraphQL commit objects that can be rendered by the shared
  # `app/views/repos/scanning/_commit.html.erb` view indexed by oid.
  def commits_by_oid(commit_oids)
    commits, graphql_commits = load_commits(commit_oids)

    commits = commits.index_by(&:oid)
    graphql_commits = graphql_commits.index_by { |commit| commit.oid }

    return commits, graphql_commits
  end
end
