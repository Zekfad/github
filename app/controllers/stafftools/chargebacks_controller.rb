# frozen_string_literal: true

class Stafftools::ChargebacksController < StafftoolsController
  areas_of_responsibility :stafftools

  CHARGEBACKS_PER_PAGE = 75

  before_action :ensure_billing_enabled

  layout "stafftools/base"

  def index
    respond_to do |format|
      format.html do
        if request.xhr? && !pjax?
          render partial: "stafftools/chargebacks/recorded_chargebacks", locals: {
            chargebacks: fetch_chargebacks.paginate(page: params[:page], per_page: CHARGEBACKS_PER_PAGE),
          }
        else
          render "stafftools/chargebacks/index", locals: {
            chargebacks: fetch_chargebacks.paginate(page: params[:page], per_page: CHARGEBACKS_PER_PAGE),
          }
        end
      end
    end
  end

  def unrecorded_disputes
    respond_to do |format|
      format.html do
        render partial: "stafftools/chargebacks/disputes", locals: {
          disputes: Billing::BillingTransaction.unrecorded_disputes,
        }
      end
    end
  end

  def create
    unless transaction_id = params[:transaction_id]
      error = "No transaction ID found"

      if request.xhr?
        render status: 422, json: { error: error }
      else
        flash[:error] = error
        redirect_to :back
      end
      return
    end

    begin
      raw_transaction = GitHub.dogstats.time("braintree.timing.transaction_find") do
        Braintree::Transaction.find(transaction_id)
      end

      unless dispute = raw_transaction.disputes.first
        error = "Transaction #{transaction_id} is not disputed"

        if request.xhr?
          render status: 422, json: { error: error }
        else
          flash[:error] = error
          redirect_to :back
        end
        return
      end

      transaction = Billing::BillingTransaction.
        find_by_transaction_id(transaction_id)

      if transaction
        transaction.chargeback! dispute

        user = transaction.user
        reason = dispute.reason

        if user && user.chargeback_disable_reason?(reason)
          token = raw_transaction.credit_card_details.token
          user.chargeback_disable(token)
        end

        if request.xhr?
          head 200
        else
          redirect_to :back
        end
      else
        error = "Couldn't find a billing transaction for #{transaction_id}"

        if request.xhr?
          render status: 422, json: { error: error }
        else
          flash[:error] = error
          redirect_to :back
        end
      end
    rescue Braintree::NotFoundError
      error = "Transaction #{transaction_id} not found"

      if request.xhr?
        render status: 422, json: { error: error }
      else
        flash[:error] = error
        redirect_to :back
      end
    end
  end

  private

  def fetch_chargebacks
    chargebacks = Billing::BillingTransaction.charged_back.includes(:live_user)
    chargebacks = chargebacks.to_a
    chargebacks.reject! { |txn| txn.live_user.nil? }
    chargebacks.sort_by! { |txn| txn.updated_at }.reverse!
  end
end
