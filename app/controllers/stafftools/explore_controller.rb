# frozen_string_literal: true

# biztools for explore stuff
class Stafftools::ExploreController < StafftoolsController
  areas_of_responsibility :explore, :stafftools

  before_action :find_collection, except: [:index, :queue_trending]

  def index
    featured_collections = ::Showcase::Collection.featured
    collections = ::Showcase::Collection.order("published ASC, name ASC").to_a

    render "stafftools/explore/index", locals: {
      featured_collections: featured_collections,
      collections: collections,
    }
  end

  def queue_trending
    case params[:period]
    when "daily"
      flash[:notice] = "Calculating daily trending repositories and users. This could take a few minutes."
      CalculateTrendingReposJob.perform_later("daily")
      CalculateTrendingUsersJob.perform_later("daily")
    when "weekly"
      flash[:notice] = "Calculating weekly trending repositories and users. This could take a few minutes."
      CalculateTrendingReposJob.perform_later("weekly")
      CalculateTrendingUsersJob.perform_later("weekly")
    when "monthly"
      flash[:notice] = "Calculating monthly trending repositories and users. This could take a few minutes."
      CalculateTrendingReposJob.perform_later("monthly")
      CalculateTrendingUsersJob.perform_later("monthly")
    else
      flash[:notice] = "Calculating all trending repositories and users. This could take a few minutes."
      CalculateTrendingReposJob.perform_later("daily")
      CalculateTrendingUsersJob.perform_later("daily")
      CalculateTrendingReposJob.perform_later("weekly")
      CalculateTrendingUsersJob.perform_later("weekly")
      CalculateTrendingReposJob.perform_later("monthly")
      CalculateTrendingUsersJob.perform_later("monthly")
    end
    redirect_to stafftools_explore_path
  end

  private

  helper_method :trending_cache_set_time

  def trending_cache_set_time(type, period)
    cache_key = "trending:#{type}:query:#{period}:time"
    Time.at(GitHub.kv.get(cache_key).value!.to_i).to_datetime
  end
end
