# frozen_string_literal: true

class Stafftools::FeatureEnrollmentsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_user_exists

  def show
    render_template_view "stafftools/feature_enrollments/show", Stafftools::User::FeatureEnrollmentsView,
      { user: this_user },
      layout: "stafftools/user/overview"
  end

  def toggle
    if should_unenroll = this_user.beta_feature_enabled?(feature)
      this_user.disable_beta_feature(feature)
    else
      this_user.enable_beta_feature(feature)
    end

    flash[:notice] = "#{this_user.login} is now #{should_unenroll ? "unenrolled from" : "enrolled in"} #{feature}."

    redirect_to :back
  end

  private

  def feature
    params[:feature]
  end
end
