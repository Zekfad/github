# frozen_string_literal: true

class Stafftools::SubscriptionItemsController < StafftoolsController
  areas_of_responsibility :platform, :stafftools

  before_action :marketplace_required

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($id: ID!, $immediatelyCancel: Boolean!) {
      cancelSubscriptionItem(input: { id: $id, immediatelyCancel: $immediatelyCancel }) {
        onFreeTrial
        subscription {
          nextBillingDate
        }
        subscriptionItem {
          subscribable {
            ... on MarketplaceListingPlan {
              name
              listing {
                name
              }
            }
            ... on SponsorsTier {
              name
              listing: sponsorsListing {
                name
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DestroyQuery, variables: { id: params[:id], immediatelyCancel: true })

    if data.errors.any?
      error_type = data.errors.details[:cancelSubscriptionItem]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
    else
      tier = data.cancel_subscription_item.subscription_item.subscribable
      tier_name = tier.name
      listing_name = tier.listing.name
      notice = "You've cancelled the subscription to #{tier_name} for #{listing_name}."
      flash[:notice] = notice
    end

    redirect_to params[:redirect_to] || :back
  end
end
