# frozen_string_literal: true

class Stafftools::ActionInvocationController < StafftoolsController
  areas_of_responsibility :stafftools
  map_to_service :actions_experience

  before_action :ensure_user_exists, only: [:block, :unblock]

  def block
    this_user.block_action_invocation(current_user)
    flash[:notice] = "Action invocation blocked."
    redirect_to :back
  end

  def unblock
    this_user.unblock_action_invocation(current_user)
    flash[:notice] = "Action invocation unblocked."
    redirect_to :back
  end
end
