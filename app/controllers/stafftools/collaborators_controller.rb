# frozen_string_literal: true

class Stafftools::CollaboratorsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_repo_exists
  before_action :redirect_if_org_owned

  layout "stafftools/repository/security"

  def index
    query = [
      "repo_id:#{current_repository.id}",
      "action:(repository_invitation.* OR repo.*_member)",
    ]
    locals = fetch_audit_log_teaser query.join(" ")

    locals[:collaborators] = current_repository.members.order("login ASC")
      .paginate(page: current_page)

    locals[:invites] = if current_page == 1
      current_repository.repository_invitations.includes(:invitee)
    else
      []
    end

    render "stafftools/collaborators/index", locals: locals
  end

  private

  def redirect_if_org_owned
    if current_repository.organization
      redirect_to gh_permissions_stafftools_repository_path(current_repository)
    end
  end
end
