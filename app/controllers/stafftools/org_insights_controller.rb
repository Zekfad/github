# frozen_string_literal: true

module Stafftools
  class OrgInsightsController < StafftoolsController

    before_action :eventer_required

    def index
      render "stafftools/org_insights/index", locals: { dropdown_metrics: dropdown_metrics }
    end

    def enqueue_org_insights_backfill
      OrgInsightsBackfillAllOrgsJobs.perform_later(metrics: metric_param)
      flash[:notice] = notice_message
      redirect_to :back
    end

    private

    def notice_message
      if metric_param && metric_param != "all"
        "Org Insights backfill of #{metric_param} started for for all orgs..."
      else
        "Org Insights backfill started for all orgs..."
      end
    end

    def metric_param
      metric = params[:metric_name]
      metric if allowed_metrics.include?(metric)
    end

    def allowed_metrics
      GitHub::OrgInsights::Metric.subclasses
    end

    def dropdown_metrics
      return @dropdown_metrics if @dropdown_metrics

      @dropdown_metrics = [["All Metrics", "all"]]
      allowed_metrics.each do |m|
        @dropdown_metrics << [m.titleize, m]
      end

      @dropdown_metrics
    end
  end
end
