# frozen_string_literal: true

class Stafftools::RedirectsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_repo_exists

  layout "stafftools/repository/overview"

  def index
    @redirects = current_repository.redirects
    render "stafftools/redirects/index"
  end

  def create
    location = params[:repository_redirect][:repository_name]
    redirect = current_repository.redirect_from_previous_location(location)
    if redirect.valid?
      flash[:notice] = "#{h location} now redirects to this repository."
    else
      flash[:error] = "That was an invalid location for the redirect. Try again."
    end
    redirect_to :back
  end

  def destroy
    redirect = current_repository.redirects.find(params[:id])
    redirect.destroy
    flash[:notice] = "#{h redirect.repository_name} no longer redirects to this repository."
    redirect_to :back
  end
end
