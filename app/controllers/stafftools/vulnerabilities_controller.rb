# frozen_string_literal: true

module Stafftools
  class VulnerabilitiesController < StafftoolsController
    areas_of_responsibility :security_advisories

    before_action :dotcom_required, except: [:index, :show, :enqueue_dotcom_sync]
    before_action :ghe_content_analysis_required, only: [:index, :show, :enqueue_dotcom_sync]
    before_action :find_vulnerability, only: [:show, :edit, :update, :publish, :create_range_alerts, :withdraw]

    IndexQuery = parse_query <<-'GRAPHQL'
      query($first: Int, $last: Int, $before: String, $after: String, $ecosystem: VulnerabilityPlatform, $status: VulnerabilityStatus, $identifier_query: String, $affects_query: String, $severities: [VulnerabilitySeverity], $orderBy: VulnerabilityOrder, $simulation: Boolean, $source_query: String) {
        ...Views::Stafftools::Vulnerabilities::Index::Root
      }
    GRAPHQL

    VULNERABILITIES_PAGE_SIZE = 50

    def index
      variables = default_index_params.merge(computed_index_params)
                                      .merge(computed_graphql_pagination_params)

      # handle sorting
      if params[:order_field]
        order_hsh = {}
        order_hsh["field"] = params[:order_field]
        order_hsh["direction"] = params[:order_direction]
        variables["orderBy"] = order_hsh
      end

      variables[:data] = platform_execute IndexQuery, variables: variables
      last_synced_at = VulnerabilitySyncWithDotcom.last_run_at
      variables[:sync_data] = {
        last_synced_at: last_synced_at,
        show: last_synced_at.present?,
      }
      render "stafftools/vulnerabilities/index", locals: variables
    end

    def show
      vulnerability = Vulnerability.find(params[:id])

      render "stafftools/vulnerabilities/show", locals: { vulnerability: vulnerability }
    end

    def edit
      render "stafftools/vulnerabilities/edit", locals: {
        vulnerability: vulnerability,
      }
    end

    def update
      if vulnerability.update(vulnerability_params)
        redirect_to stafftools_vulnerability_path(vulnerability.id)
      else
        render "stafftools/vulnerabilities/edit", locals: {
          vulnerability: vulnerability,
        }
      end
    end

    def publish
      vulnerability.publish(published_by: current_user)

      redirect_to stafftools_vulnerability_path(vulnerability.id), {
        notice: "Vulnerability successfully published",
      }
    end

    def create_range_alerts
      vulnerability.process_alerts(
        actor: current_user,
        skip_email: (params[:skip_email] == "true"),
      )

      redirect_to stafftools_vulnerability_path(vulnerability.id), {
        notice: "Vulnerability range alerts enqueued successfully",
      }
    end

    def withdraw
      vulnerability.withdraw(withdrawn_by: current_user)
      vulnerability.enqueue_deletion_of_withdrawn_alerts

      redirect_to stafftools_vulnerability_path(vulnerability.id), {
        notice: "Vulnerability successfully withdrawn",
      }
    end

    def enqueue_dotcom_sync
      return render_404 unless GitHub.ghe_content_analysis_enabled?
      VulnerabilitySyncWithDotcom.perform_later
      flash[:notice] = "Vulnerability sync job enqueued ..."
      redirect_to :back
    end

    private

    attr_reader :vulnerability

    def find_vulnerability
      @vulnerability = Vulnerability.find(params[:id])
    end

    def vulnerability_params
      params.require(:vulnerability).permit(
        %i[
          classification
          identifier
          cve_id
          white_source_id
          severity
          external_reference
          summary
          description
          simulation
        ],
      ).reject { |p, v| p.nil? }.transform_values { |v| v.blank? ? nil : v }
    end

    def ghe_content_analysis_required
      render_404 and return if GitHub.enterprise? && !GitHub.ghe_content_analysis_enabled?
    end

    def computed_index_params
      computed_params = params.permit!.to_h
                    .with_indifferent_access
                    .slice(*default_index_params.keys)
                    .select { |key, value| value.present? }
      # convert simulation parameter to a boolean
      computed_params[:simulation] = ActiveRecord::Type::Boolean.new.deserialize(computed_params[:simulation])
      computed_params
    end

    def default_index_params
      {
        status: nil,
        ecosystem: nil,
        identifier_query: nil,
        affects_query: nil,
        severities: nil,
        simulation: nil,
        source_query: nil,
      }.with_indifferent_access
    end

    def computed_graphql_pagination_params
      graphql_pagination_params page_size: params.fetch(:page_size, VULNERABILITIES_PAGE_SIZE).to_i
    end
  end
end
