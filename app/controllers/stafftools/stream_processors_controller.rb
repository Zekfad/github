# frozen_string_literal: true

class Stafftools::StreamProcessorsController < StafftoolsController

  before_action :ensure_group_id_is_present, only: [:pause, :resume]

  def index
    processors = ::GitHub::StreamProcessors::BaseProcessor.direct_descendants.map(&:new)
    processors, unknown_group_id_processors = processors.partition { |processor| processor.group_id.present? }
    processors_by_group_id = processors.group_by { |processor| [processor.group_id, processor.paused_at] }
      .sort { |((_, paused_at_a), _), ((_, paused_at_b), _)| compare_paused_at(paused_at_a, paused_at_b) }

    render "stafftools/stream_processors/index", locals: {
      stream_processors_class: ::GitHub::StreamProcessors::BaseProcessor,
      processors_by_group_id: processors_by_group_id,
      unknown_group_id_processors: unknown_group_id_processors
    }
  end

  def pause
    ::GitHub::StreamProcessors::BaseProcessor.pause(params[:stream_processor_group_id])

    redirect_to stafftools_stream_processors_path, notice: "Stream processor #{params[:stream_processor_group_id]} paused!"
  end

  def resume
    ::GitHub::StreamProcessors::BaseProcessor.resume(params[:stream_processor_group_id])

    redirect_to stafftools_stream_processors_path, notice: "Stream processor #{params[:stream_processor_group_id]} resumed!"
  end

  private

  # Compares the paused at times for the stream processors
  # Since resumed processors will have this value set to nil and
  # we can't compare nil to time we set nil values to the bottom
  def compare_paused_at(paused_at_a, paused_at_b)
    if paused_at_a && paused_at_b
      paused_at_a <=> paused_at_b
    elsif paused_at_a
      -1
    else
      1
    end
  end

  def ensure_group_id_is_present
    if params[:stream_processor_group_id].blank?
      redirect_to stafftools_stream_processors_path, alert: "Processor Group ID is required"
    end
  end
end
