# frozen_string_literal: true

class Stafftools::IntegrationInstallationsController < StafftoolsController
  areas_of_responsibility :platform, :stafftools

  before_action :ensure_user_exists

  layout :new_nav_layout

  def index
    installations = @user.integration_installations.includes(:integration).
      order("integrations.name asc").paginate(page: params[:page])
    render_template_view "stafftools/integration_installations/index",
      Stafftools::IntegrationInstallations::IndexView, installations: installations
  end

  def show
    installation = @user.integration_installations.find(params[:id])
    repositories = installation.repositories.includes(:mirror, :owner, :parent).paginate(page: current_page)

    render_template_view "stafftools/integration_installations/show",
      Stafftools::IntegrationInstallations::ShowView,
      installation: installation, repositories: repositories, user: @user
  end

  def update
    installation = @user.integration_installations.find(params[:id])

    if params[:rate_limit].present?
      rate = params[:rate_limit].to_i

      if params[:temporary]
        installation.set_temporary_rate_limit rate
      else
        # Clear the temportary rate limit so the new rate limit
        # is applied immediately
        installation.update!(
          temporary_rate_limit:            nil,
          temporary_rate_limit_expires_at: nil,
          rate_limit:                      rate,
        )
      end

      temporarily = "temporarily " if params[:temporary]
      flash[:notice] = "Installation rate limit #{temporarily}set to #{rate}"
    end

    redirect_to :back
  end

  private

  def new_nav_layout
    case this_user.site_admin_context
    when "organization"
      "stafftools/organization/security"
    when "user"
      "stafftools/user/security"
    end
  end
end
