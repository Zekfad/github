# frozen_string_literal: true

class Stafftools::PasswordsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_user_exists

  # Send the user a password reset email
  #
  # email - Which address to send to.  Optional, if not passed the user's default email is used.
  def send_reset_email
    reset = PasswordReset.create(
      user: this_user,
      email: params[:email],
      force: true,
      expires: 24.hours.from_now,
    )

    if reset.valid?
      flash[:notice] = "Sent #{reset.email} password reset link: #{reset.link}"
    else
      flash[:error] = reset.error_message
    end
    redirect_to :back
  end

  # Scramble the user's password
  def randomize
    this_user.set_random_password(actor: current_user)
    flash[:notice] = "Password randomized"
    redirect_to :back
  end
end
