# frozen_string_literal: true

module Stafftools
  class PendingVulnerabilitiesController < StafftoolsController
    areas_of_responsibility :security_advisories

    before_action :dotcom_required
    before_action :find_pending_vulnerability,
      only: [:show, :edit, :update, :review, :submit, :reject]

    PendingVulnerabilitiesIndexQuery = parse_query <<-'GRAPHQL'
      query($first: Int, $last: Int, $before: String, $after: String, $ecosystem: VulnerabilityPlatform, $status: PendingVulnerabilityStatus, $identifier_query: String, $affects_query: String, $severities: [VulnerabilitySeverity], $orderBy: VulnerabilityOrder, $simulation: Boolean, $source_query: String) {
        ...Views::Stafftools::PendingVulnerabilities::Index::Root
      }
    GRAPHQL

    VULNERABILITIES_PAGE_SIZE = 50

    SOURCE_IDENTITY = "github/stafftools"

    def index
      variables = default_index_params.merge(computed_index_params)
                                      .merge(computed_graphql_pagination_params)

      # handle sorting
      if params[:order_field]
        order_hsh = {}
        order_hsh["field"] = params[:order_field]
        order_hsh["direction"] = params[:order_direction]
        variables["orderBy"] = order_hsh
      end

      variables[:data] = platform_execute PendingVulnerabilitiesIndexQuery, variables: variables

      render "stafftools/pending_vulnerabilities/index", locals: variables
    end

    def new
      render "stafftools/pending_vulnerabilities/new", locals: {
        pending_vulnerability: PendingVulnerability.new,
      }
    end

    ShowQuery = parse_query <<-'GRAPHQL'
      query($databaseId: Int!) {
        stafftoolsInfo {
          pendingVulnerability(databaseId: $databaseId) {
            ...Views::Stafftools::PendingVulnerabilities::Show::PendingVulnerability
          }
        }
      }
    GRAPHQL

    def create
      pending_vulnerability = PendingVulnerability.pending
        .created_by(current_user)
        .from_source(SOURCE_IDENTITY)
        .new(pending_vulnerability_params)

      if pending_vulnerability.save
        redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      else
        render "stafftools/pending_vulnerabilities/new", locals: {
          pending_vulnerability: pending_vulnerability,
        }
      end
    end

    def show
      data = platform_execute(ShowQuery, variables: {
        databaseId: params[:id].to_i,
      })

      render "stafftools/pending_vulnerabilities/show", {
        locals: {
          data: data.stafftools_info.pending_vulnerability,
        },
      }
    end

    def edit
      render "stafftools/pending_vulnerabilities/edit", locals: {
        pending_vulnerability: pending_vulnerability,
      }
    end

    def update
      if pending_vulnerability.update(pending_vulnerability_params)
        redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      else
        render "stafftools/pending_vulnerabilities/edit", locals: {
          pending_vulnerability: pending_vulnerability,
        }
      end
    end

    def review
      reviewer = pending_vulnerability.reviewer_a.nil? ? :a : :b
      review_attrs = if reviewer == :a
        { reviewer_a: current_user, review_a_at: Time.now, status: "reviewed_a" }
      else
        { reviewer_b: current_user, review_b_at: Time.now, status: "reviewed_b" }
      end
      if pending_vulnerability.update(review_attrs)
        flash[:notice] = if reviewer == :a
          "Reviewer A set.  Ready for review by reviewer B"
        else
          "Reviewer B set.  Ready for submission to Vulnerabilities"
        end
        redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      else
        flash[:error] =
          "Unable to mark reviewed: " + pending_vulnerability.errors.full_messages.join(", ")
        redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      end
    end

    def submit
      if !pending_vulnerability.valid?
        flash[:error] = "Submit failed: " + pending_vulnerability.errors.full_messages.join(", ")
        return redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      end

      new_vuln = pending_vulnerability.submit_into_vulnerability(current_user)

      if new_vuln.errors.present?
        flash[:error] = "Submit failed: " + new_vuln.errors.full_messages.join(", ")
        redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      else
        flash[:notice] = "Submitted!"
        redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      end
    end

    def reject
      if !pending_vulnerability.rejectable?
        flash[:error] =
          "Vulnerabilities in status '#{pending_vulnerability.status}' have aleady been submitted.  They can not be rejected."
        return redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      end
      if pending_vulnerability.reject(actor: current_user)
        flash[:notice] = "Status set to #{pending_vulnerability.status.titleize} successfully"
        redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      else
        flash[:error] =
          "Reject request failed: " + pending_vulnerability.errors.full_messages.join(", ")
        redirect_to stafftools_pending_vulnerability_path(pending_vulnerability.id)
      end
    end

    private

    attr_reader :pending_vulnerability

    def find_pending_vulnerability
      @pending_vulnerability = PendingVulnerability.find(params[:id])
    end

    def computed_index_params
      computed_params = params.permit!.to_h
                    .with_indifferent_access
                    .slice(*default_index_params.keys)
                    .select { |key, value| value.present? }
      # convert simulation parameter to a boolean
      computed_params[:simulation] = ActiveRecord::Type::Boolean.new.deserialize(computed_params[:simulation])
      computed_params
    end

    def default_index_params
      {
        status: nil,
        ecosystem: nil,
        identifier_query: nil,
        affects_query: nil,
        severities: nil,
        simulation: nil,
        source_query: nil,
      }.with_indifferent_access
    end

    def computed_graphql_pagination_params
      graphql_pagination_params page_size: params.fetch(:page_size, VULNERABILITIES_PAGE_SIZE).to_i
    end

    def pending_vulnerability_params
      params.require(:pending_vulnerability).permit(
        %i[
          classification
          identifier
          external_identifier
          cve_id
          white_source_id
          severity
          external_reference
          summary
          description
          review_notes
          simulation
        ],
      ).reject { |p, v| p.nil? }.transform_values { |v| v.blank? ? nil : v }
    end
  end
end
