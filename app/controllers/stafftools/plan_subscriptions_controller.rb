# frozen_string_literal: true

class Stafftools::PlanSubscriptionsController < StafftoolsController
  areas_of_responsibility :stafftools, :gitcoin

  include BillingSettingsHelper

  before_action :ensure_user_exists
  before_action :ensure_billing_enabled

  def update_billing_date
    unless billing_date
      flash[:error] = "Billing date #{params[:billed_on]} is not a valid date. Needs to be in the future from today."
      return redirect_to :back
    end

    unless this_user.external_subscription?
      flash[:error] = "#{this_user.login} does not have an external subscription to change the billing date"
      return redirect_to :back
    end

    begin
      Billing::PlanSubscription::UpdateBillingDate.perform this_user, billing_date
      redirect_to billing_stafftools_user_path(this_user)
    rescue => e
      Failbot.report e
      flash[:error] = e.message
      redirect_to :back
    end
  end

  def synchronization
    respond_to do |format|
      format.html do
        if this_user.external_subscription?
          render partial: "stafftools/users/external_subscription_synchronization",
                 locals: { user: this_user }
        else
          head :ok, content_type: "text/html"
        end
      end
    end
  end

  private

  def billing_date
    begin
      if params[:billed_on].present? && (date = Date.strptime(params[:billed_on], "%F"))
        return date if date > GitHub::Billing.today
      end
    rescue
      nil
    end
  end

end
