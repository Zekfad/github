# frozen_string_literal: true

class Stafftools::IntegrationListingsController < StafftoolsController
  areas_of_responsibility :stafftools, :api

  before_action :ensure_user_exists
  before_action :load_integration

  def index
    render "stafftools/integration_installations/index"
  end

  def create
    listing = @integration.build_integration_listing(integration_listing_params)
    if listing.save
      flash[:notice] = "Integration listing has been created."
    else
      flash[:error] = "We couldn't create the integration listing: #{listing.errors.full_messages.to_sentence}"
    end
    redirect_to :back
  end

  def update
    listing = @integration.integration_listing
    if listing.update(integration_listing_params)
      flash[:notice] = "Integration listing has been updated."
    else
      flash[:error] = "We couldn't update the integration listing: #{listing.errors.full_messages.to_sentence}"
    end
    redirect_to :back
  end

  private

  def load_integration
    @integration = if params[:application_id]
      this_user.oauth_applications.find(params[:application_id])
    else
      this_user.integrations.find(params[:app_id])
    end
  end

  def integration_listing_params
    params.require(:integration_listing).permit :name,
                                                :blurb,
                                                :slug,
                                                :body,
                                                :installation_url,
                                                :state,
                                                { feature_ids: [] },
                                                :pricing_url,
                                                :documentation_url,
                                                :tos_url,
                                                :privacy_policy_url,
                                                :support_url,
                                                :status_url,
                                                :company_url,
                                                :language_ids,
                                                :learn_more_url
  end
end
