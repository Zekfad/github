# frozen_string_literal: true

class Stafftools::TopicsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :normalize_topic, only: :show

  ShowQuery = parse_query <<-'GRAPHQL'
    query($name: String!) {
      topic(name: $name) {
        ...Views::Stafftools::Topics::Show::Topic
      }
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: { name: params[:topic] })

    return render_404 unless data.topic

    render "stafftools/topics/show", layout: "application", locals: { topic: data.topic }
  end

  UpdateMutation = parse_query <<-'GRAPHQL'
    mutation($name: String!, $isFlagged: Boolean!) {
      updateTopic(input: {name: $name, isFlagged: $isFlagged}) {
        topic {
          name
          stafftoolsInfo {
            isFlagged
          }
        }
      }
    }
  GRAPHQL

  def update
    data = platform_execute(UpdateMutation, variables: {
      name: params[:topic],
      isFlagged: (params[:flagged].to_i == 1),
    })

    unless data.errors.any?
      topic = data.update_topic.topic
      message = if topic.stafftools_info.is_flagged?
        "Flagged topic '#{topic.name}'."
      else
        "Removed flag from topic '#{topic.name}'."
      end
      redirect_to stafftools_topic_path(topic.name), notice: message
    else
      flash[:error] = data.errors.messages.values.join(", ")
      redirect_to stafftools_topic_path(params[:topic])
    end
  end

  private

  def normalize_topic
    normalized_name = Topic.normalize(params[:topic])
    return if normalized_name == params[:topic]

    redirect_to stafftools_topic_path(normalized_name)
  end
end
