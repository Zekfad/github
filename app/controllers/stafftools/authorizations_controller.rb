# frozen_string_literal: true

class Stafftools::AuthorizationsController < StafftoolsController
  areas_of_responsibility :stafftools, :api

  before_action :ensure_user_exists
  before_action :ensure_authorization_exists

  layout "stafftools/user/security"

  def show
    @authorization_accesses = @authorization.accesses.paginate(page: params[:page])
    render "stafftools/authorizations/show"
  end

  def destroy
    @authorization.destroy_with_explanation(:site_admin)
    flash[:notice] = "OAuth authorization for '#{@authorization.application.name}' revoked"
    redirect_to stafftools_user_applications_path(this_user)
  end

  private

  def ensure_authorization_exists
    @authorization = @user.oauth_authorizations.third_party.find_by_application_id(params[:application_id])
    render_404 unless @authorization
  end

end
