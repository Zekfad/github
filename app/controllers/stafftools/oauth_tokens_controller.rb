# frozen_string_literal: true

class Stafftools::OauthTokensController < StafftoolsController
  areas_of_responsibility :stafftools, :api

  before_action :ensure_user_exists
  before_action :ensure_oauth_access_exists, except: [:index]

  layout "stafftools/user/security"

  def index
    render "stafftools/oauth_tokens/index"
  end

  def show
    render "stafftools/oauth_tokens/show"
  end

  def destroy
    oauth_access.destroy_with_explanation(:site_admin)

    flash[:notice] = "OAuth token '#{oauth_access.token_last_eight}' revoked"
    redirect_to stafftools_user_oauth_tokens_path(this_user)
  end

  def compare
    hashed_input = OauthAccess.hash_token(params[:compromized_token])
    if oauth_access.hashed_token == hashed_input
      flash[:notice] = "Compromized token matched, you should revoke this token."
    else
      flash[:notice] = "Token does not match"
    end
    redirect_to :back
  end

  private

  def oauth_access
    @access ||= this_user.oauth_accesses.find(params[:id])
  end

  def ensure_oauth_access_exists
    render_404 unless oauth_access
  end

end
