# frozen_string_literal: true

module Stafftools
  class BundledLicenseAssignmentsController < Stafftools::Businesses::BusinessBaseController
    include BundledLicenseAssignmentHelper

    PER_PAGE = 20

    skip_before_action :business_required, only: :orphaned

    def enterprise
      return render_404 unless bundled_license_assignments_enabled?(this_business)

      render "stafftools/bundled_license_assignments/enterprise", locals: {
          assignments: this_business.bundled_license_assigments.paginate(page: current_page, per_page: PER_PAGE),
          header: "Bundled license assignments for #{this_business.name}",
          action: "enterprise"
        }
    end

    def orphaned
      return render_404 unless bundled_license_assignments_enabled?(current_user)

      render "stafftools/bundled_license_assignments/orphaned", layout: "stafftools", locals: {
          assignments: Billing::BundledLicenseAssignment.orphaned.paginate(page: current_page, per_page: PER_PAGE),
          header: "Orphaned bundled license assignments",
          action: "orphaned"
        }
    end
  end
end
