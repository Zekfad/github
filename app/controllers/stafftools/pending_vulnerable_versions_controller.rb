# frozen_string_literal: true

module Stafftools
  class PendingVulnerableVersionsController < StafftoolsController
    areas_of_responsibility :security_advisories

    def new
      render "stafftools/pending_vulnerable_versions/new", locals: {
        pending_vulnerability:      pending_vulnerability,
        pending_vulnerable_version: PendingVulnerableVersionRange.new(range_params),
      }
    end

    def edit
      render "stafftools/pending_vulnerable_versions/edit", locals: {
        pending_vulnerability:      pending_vulnerability,
        pending_vulnerable_version: pending_vulnerable_version_range,
      }
    end

    def update
      if pending_vulnerable_version_range.update(range_params)
        redirect_to stafftools_pending_vulnerability_url(pending_vulnerability.id), {
          notice: "Vulnerable version successfully updated",
        }
      else
        render "stafftools/pending_vulnerable_versions/edit", locals: {
          pending_vulnerability:      pending_vulnerability,
          pending_vulnerable_version: pending_vulnerable_version_range,
        }
      end
    end

    def create
      pending_version_range = pending_vulnerability.pending_vulnerable_version_ranges.new(range_params)

      if pending_version_range.save
        redirect_to stafftools_pending_vulnerability_url(pending_vulnerability.id), {
          notice: "Vulnerable version successfully added",
        }
      else
        render "stafftools/pending_vulnerable_versions/new", locals: {
          pending_vulnerability:      pending_vulnerability,
          pending_vulnerable_version: pending_version_range,
        }
      end
    end

    def destroy
      pending_vulnerable_version_range.destroy

      redirect_to stafftools_pending_vulnerability_url(pending_vulnerable_version_range.pending_vulnerability), {
        notice: "Vulnerable version successfully deleted",
      }
    end

    def preview_alert
      view = create_view_model(
        RepositoryAlerts::StafftoolsPreviewView,
        vulnerable_version_range: pending_vulnerable_version_range,
      )
      render "stafftools/vulnerable_versions/preview", locals: { view: view }
    end

    AffectedRepoCountQuery = parse_query <<-'GRAPHQL'
      query($databaseId: Int!) {
        stafftoolsInfo {
          range: pendingVulnerableVersionRange(databaseId: $databaseId) {
            estimatedAffectedRepositoryCount
          }
        }
      }
    GRAPHQL

    def dependent_count
      count = cache_dependent_count do
        data = platform_execute(AffectedRepoCountQuery, variables: {
          databaseId: params[:id].to_i,
        })

        data.stafftools_info.range.estimated_affected_repository_count
      end

      render html: ActiveSupport::NumberHelper.number_to_delimited(count)
    end

    private

    def pending_vulnerability
      @pending_vulnerability ||= PendingVulnerability.find(params[:pending_vulnerability_id])
    end

    def pending_vulnerable_version_range
      @pending_vulnerable_version_range ||= pending_vulnerability
        .pending_vulnerable_version_ranges
        .find(params[:id])
    end

    def cache_dependent_count(&block)
      ttl = 24.hours
      cache_key = [
        PendingVulnerableVersionRange,
        pending_vulnerable_version_range.id,
        pending_vulnerable_version_range.updated_at.to_i,
        "affected_repo_count",
      ].join(":")

      GitHub.cache.fetch(cache_key, ttl: ttl, &block)
    end

    def range_params
      if params[:pending_vulnerable_version_range]
        params[:pending_vulnerable_version_range].permit(%i[affects ecosystem requirements fixed_in])
      else
        {}
      end
    end
  end
end
