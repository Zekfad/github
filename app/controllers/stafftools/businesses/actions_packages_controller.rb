# frozen_string_literal: true

module Stafftools
  module Businesses
    class ActionsPackagesController < Stafftools::Businesses::BusinessBaseController
      areas_of_responsibility :stafftools, :gitcoin

      def show
        render "stafftools/businesses/actions_packages/show"
      end

      private

      def actions_usage
        @_actions_usage ||= Billing::ActionsUsage.new(this_business)
      end
      helper_method :actions_usage

      def package_registry_usage
        @_package_registry_usage ||= Billing::PackageRegistryUsage.new(this_business)
      end
      helper_method :package_registry_usage

      def shared_storage_usage
        @_shared_storage_usage ||= Billing::SharedStorageUsage.new(this_business)
      end
      helper_method :shared_storage_usage

      def metered_billing_permissions
        @_metered_billing_permissions ||= Billing::MeteredBillingPermission.new(this_business)
      end
      helper_method :metered_billing_permissions
    end
  end
end
