# frozen_string_literal: true

module Stafftools
  module Businesses
    class WatermarkReportsController < BusinessBaseController
      areas_of_responsibility :gitcoin

      def index
        render "stafftools/businesses/watermark_reports/index", locals: {
          this_business: this_business,
          watermark_report: watermark_report,
        }
      end

      private

      def watermark_report
        return nil unless params[:range_start] && params[:range_end]

        Business::LicenseWatermarkReport.new(
          this_business,
          range_start: params[:range_start],
          range_end: params[:range_end])
      end

    end
  end
end
