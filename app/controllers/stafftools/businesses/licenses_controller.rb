# frozen_string_literal: true

module Stafftools
  module Businesses
    class LicensesController < BusinessBaseController
      areas_of_responsibility :gitcoin

      def index
        respond_to do |format|
          format.html do
            render "stafftools/businesses/licenses/index", locals: {
              view: Stafftools::Businesses::LicensesView.new(business: this_business),
            }
          end

          format.csv do
            send_data Business::LicenseCsvGenerator.new(this_business).generate,
              filename: "#{this_business.slug}-consumed_licenses.csv"
          end
        end
      end
    end
  end
end
