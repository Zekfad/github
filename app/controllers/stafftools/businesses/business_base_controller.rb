# frozen_string_literal: true

module Stafftools
  module Businesses
    class BusinessBaseController < StafftoolsController
      before_action :dotcom_required
      before_action :business_required

      layout "stafftools/business"

      private

      def business_required
        render_404 unless this_business
      end

      def this_business
        @this_business ||= ::Business.find_by(slug: params[:slug])
      end
      helper_method :this_business
    end
  end
end
