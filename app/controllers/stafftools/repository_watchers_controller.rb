# frozen_string_literal: true

class Stafftools::RepositoryWatchersController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_repo_exists

  layout "stafftools/repository/collaboration"

  def index
    users = Stafftools::Newsies.users_watching_repository(current_repository)
      .order(:login)
      .paginate(page: params[:page] || 1)

    settings = {}
    users.each do |user|
      # I know this is an N+1, but newsies is a mess and fixing this seems like
      # more effort than it is worth since the list is paginated
      settings[user] = Stafftools::NewsiesSettings.new user
    end

    render "stafftools/repository_watchers/index", locals: { users: users, settings: settings }
  end

end
