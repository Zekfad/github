# frozen_string_literal: true
class Stafftools::UsersController < StafftoolsController
  include MarketingMethods

  ERR_COULD_NOT_PROMOTE_GH = "You are not able to promote that user; you can only promote current employees and must specify a reason for the log.".freeze
  ERR_COULD_NOT_PROMOTE_GHE = "You have to specify a reason for the log.".freeze

  areas_of_responsibility :enterprise_only, :stafftools

  before_action :ensure_user_exists,
                except: [:index, :by_ip, :invite, :send_invite,
                            :site_admins, :suspended,
                            :dismiss_license_notice, :dismiss_certificate_notice,
                            :invoiced, :dormant, :suspend_dormant, :restore,
                            :rename_status, :toggle_legal_hold]

  before_action :ensure_user_not_org,
    only: [:collab_repos, :watching, :starred_repos, :contributing_repos, :acv_contributions]

  before_action :ensure_org_not_user, only: [
    :domains, :enterprise_installations, :set_interaction_limit,
    :ssh_certificate_authorities, :ip_whitelisting, :disable_ip_whitelisting,
    :start_org_token_scan_backfill, :clear_org_token_scan_backfill
  ]

  before_action :ensure_billing_enabled,
                only: [:charge, :invoiced, :ofac_sanction_override]

  before_action :dotcom_required, only: [:dmca_repos, :successors, :acv_contributions]

  before_action :enterprise_required,
                only: [:index, :suspended, :dormant,
                          :invite, :send_invite, :dismiss_license_notice,
                          :dismiss_certificate_notice]

  before_action :ensure_user_invites_enabled,
                only: [:invite, :send_invite]

  before_action :ensure_rate_limiting_enabled,
                only: [:rate_limits, :reset_rate_limits]

  before_action :check_anon_access_enabled,
                only: [:anon_access_repos]

  before_action :ensure_domain_verification_enabled, only: [:domains]

  before_action :eventer_required, only: [:backfill_org_insights]

  before_action :ensure_ip_whitelisting_available, only: [
    :ip_whitelisting, :disable_ip_whitelisting
  ]

  HIGH_RISK_SUDO_ACTIONS = [
    :block_user,
    :change_allow_force_push,
    :clear_all_activity,
    :clear_public_activity,
    :destroy,
    :lock_billing,
    :mark_deceased,
    :rename,
    :remove_lockout,
    :revoke_employee_access,
    :set_github_biztools_user,
    :set_github_developer,
    :set_site_admin,
    :site_admins,
    :unblock_user,
    :unlock_billing,
  ].freeze

  skip_before_action :sudo_filter,
    only: [:dismiss_license_notice, :dismiss_certificate_notice]

  skip_before_action :prompt_for_hubber_access, only: [:set_hubber_access_reason] unless GitHub.enterprise?

  layout :new_nav_layout

  private def new_nav_layout
    if this_user
      case action_name
      when "billing"
        case this_user.site_admin_context
        when "organization"
          "stafftools/organization/billing"
        when "user"
          "stafftools/user/billing"
        end
      when "collab_repos", "watching", "starred_repos", "contributing_repos", "acv_contributions"
        # These actions are only valid for users, not orgs.
        # See #ensure_user_not_org used as a before_action filter.
        "stafftools/user/collaboration"
      when "repositories", "public_repos", "internal_repos", "anon_access_repos", "disabled_repos", "dmca_repos", "purgatory"
        case this_user.site_admin_context
        when "organization"
          "stafftools/organization/content"
        when "user"
          "stafftools/user/content"
        end
      when "security", "keys", "org_owners", "enterprise_installations",
        "ssh_certificate_authorities", "ip_whitelisting"
        case this_user.site_admin_context
        when "organization"
          "stafftools/organization/security"
        when "user"
          "stafftools/user/security"
        end
      else
        case this_user.site_admin_context
        when "organization"
          "stafftools/organization/overview"
        when "user"
          "stafftools/user/overview"
        end
      end
    else
      "stafftools"
    end
  end

  def index
    index_view = Stafftools::User::IndexView.new(title: "All users")
    users = paginate(User, user_query)
    render "stafftools/users/index", locals: { view: index_view, users: users }
  end

  def log
    log_view = Stafftools::User::LogView.new(user: @user)
    render "stafftools/users/log", locals: { view: log_view }
  end

  def purgatory
    purgatory_view = Stafftools::User::PurgatoryView.new(user: @user, page: params[:repo_page])
    render "stafftools/users/purgatory", locals: { view: purgatory_view }
  end

  def contributing_repos
    repos_view = Stafftools::User::ReposView.new(user: @user, page: params[:repo_page])
    render "stafftools/users/contributing_repos", locals: { view: repos_view }
  end

  def repositories
    repos_view = Stafftools::User::ReposView.new(user: @user, page: params[:repo_page])
    private_forks_view = Stafftools::User::PrivateForksView.new(user: @user, page: params[:repo_page])
    render "stafftools/users/repositories", locals: { view: repos_view, private_forks_view: private_forks_view }
  end

  def starred_repos
    starred_view = Stafftools::User::StarredReposView.new(user: @user, page: params[:repo_page])
    render "stafftools/users/starred_repos", locals: { view: starred_view }
  end

  def acv_contributions
    acv_contributions_view = Stafftools::User::AcvContributionsView.new(user: @user)
    render "stafftools/users/acv_contributions", locals: { view: acv_contributions_view }
  end

  def disabled_repos
    disabled_repos_view = Stafftools::User::DisabledReposView.new(user: @user, page: params[:repo_page])
    render "stafftools/users/disabled_repos", locals: { view: disabled_repos_view }
  end

  def dmca_repos
    dmca_repos_view = Stafftools::User::DMCAReposView.new(user: @user, page: params[:repo_page])
    render "stafftools/users/dmca_repos", locals: { view: dmca_repos_view }
  end

  def internal_repos
    internal_repos_view = Stafftools::User::InternalReposView.new(user: @user, page: params[:repo_page])
    render "stafftools/users/internal_repos", locals: { view: internal_repos_view }
  end

  def enterprise_installations
    installations = @user.enterprise_installations.order("created_at desc").paginate(page: params[:page])
    render "stafftools/organizations/enterprise_installations/index", locals: { installations: installations }
  end

  def ssh_certificate_authorities
    render_template_view(
      "stafftools/organizations/ssh_certificate_authorities",
      Stafftools::Organization::SshCertificateAuthoritiesView,
      org: @user,
    )
  end

  def ip_whitelisting
    entries = IpWhitelistEntry.usable_for(@user).order(whitelisted_value: :asc).paginate(page: params[:page])
    render "stafftools/organizations/ip_whitelisting",
      locals: { organization: @user, ip_whitelist_entries: entries }
  end

  def disable_ip_whitelisting
    if params[:reason].blank?
      flash[:error] = "You must provide a reason for disabling the IP allow list."
    elsif @user.ip_whitelisting_enabled_policy?
      # Don't let staff disable an IP allow list on an org if the IP allow list
      # is configured on the owning enterprise account (and inherited by the org).
      flash[:error] = "You must disable the IP allow list on the owning enterprise account."
    else
      @user.disable_ip_whitelisting actor: current_user, reason: params[:reason]
      flash[:notice] = "Disabled IP allow list."
    end
    redirect_to ip_whitelisting_stafftools_user_path(@user)
  end

  def show
    fetch_error_states

    @counts = {
      total_repos: this_user.repository_counts.total_repositories,
      private_repos: this_user.repository_counts.private_repositories,
      internal_repos: this_user.repository_counts.internal_repositories,
      public_repos: this_user.repository_counts.public_repositories,
      disabled_repos: this_user.repository_counts.disabled_repositories,
      locked_repos: this_user.repository_counts.locked_repositories,
      total_projects: this_user.projects.size,
      public_projects: this_user.projects.open_projects.where(public: true).size,
      private_projects: this_user.projects.open_projects.where(public: false).size,
      closed_projects: this_user.projects.closed_projects.size,
      blocked_users: this_user.ignored.size,
      owned_apps: this_user.oauth_applications.size,
      owned_integrations: this_user.integrations.size,
      installed_integrations: this_user.integration_installations.size,
      ignored_by: this_user.ignored_by.size,
      pinned_repos: this_user.total_pinned_repositories,
      minimized_comments: Stafftools::RecentComments.minimized_comment_count(this_user),
      packages: this_user.packages.size
    }

    archived = Archived::Repository.where(owner_id: this_user.id).size
    @counts[:archived_repos] = archived
    @counts[:total_repos] += archived

    @last_transaction = this_user.billing_transactions.sales.last

    if GitHub.user_abuse_mitigation_enabled?
      abuse_query = Audit::Driftwood::Query.new_stafftools_query(
        phrase: this_user.recent_abuse_reports_query,
        current_user: current_user,
      )
      @counts[:recent_abuse_reports] = abuse_query.execute.size
    end

    if this_user.user?
      @counts.merge! \
        emails: this_user.emails.user_entered_emails.size,
        gpg_keys: this_user.gpg_keys.primary_keys.size,
        ssh_keys: this_user.public_keys.size,
        security_keys: this_user.u2f_registrations.size,
        delegated_recovery_tokens: this_user.delegated_recovery_tokens.size

      orgs = {
        member_orgs: this_user.organizations.size,
        owned_orgs: this_user.owned_organizations.size,
        billing_orgs: this_user.billing_manager_organizations.size,
      }
      @counts.merge! orgs
      @counts[:orgs] = orgs.values.sum

      @counts[:authed_integrations] = this_user.oauth_authorizations.github_apps.size

      oauths = {
        owned_apps: @counts[:owned_apps],
        authed_apps: this_user.oauth_authorizations.third_party.oauth_apps.size,
        tokens: this_user.oauth_accesses.personal_tokens.size,
      }

      @counts.merge! oauths
      @counts[:oauths] = oauths.values.sum

      archived_gists = Archived::Gist.where \
        user_id: this_user.id
      gists = {
        public_gists: this_user.public_gists.size,
        secret_gists: this_user.private_gists.size,
        archived_gists: archived_gists.size,
      }
      @counts.merge! gists
      @counts[:gists] = gists.values.sum

    else
      @counts[:webhooks] = this_user.hooks.size if GitHub.hookshot_enabled?

      @counts[:enterprise_installations] = this_user.enterprise_installations.size
      @counts[:owned_projects] = this_user.projects.size
      @counts[:teams] = this_user.teams.size
      @counts[:members] = this_user.members.limit(Organization::MEGA_ORG_MEMBER_THRESHOLD).size
      @counts[:admins] = this_user.admins.size

      if GitHub.billing_enabled?
        @counts[:billing_managers] = this_user.billing_managers.size
      end
    end

    case this_user.site_admin_context
    when "organization"
      render "stafftools/organizations/show", layout: "stafftools/organization"
    when "user"
      render "stafftools/users/show", layout: "stafftools/user", locals: {
        view: Stafftools::User::ShowView.new(user: this_user, current_user: current_user),
        layout: "stafftools/user",
        counts: @counts,
        error_states: @error_states,
        obfuscated_dupe_emails: @obfuscated_dupe_emails,
        spam_flag_timestamp: @spam_flag_timestamp,
        last_transaction: @last_transaction,
        user: this_user,
      }
    else
      render_404
    end
  end

  def terms_of_service
    if @user.is_a?(Organization) && !GitHub.enterprise?
      locals = { organization: @user }
      render "stafftools/users/terms_of_service", layout: "stafftools/organization", locals: locals
    else
      render_404
    end
  end

  def set_terms_of_service
    if params[:organization][:terms_of_service_change_note].blank?
      flash[:error] = "Change note is required."
      return redirect_to :back
    end

    if @user.terms_of_service.update(
      type: params[:organization][:terms_of_service_type],
      actor: current_user,
      company_name: params[:organization][:company_name],
      staff_actor: true,
      change_note: params[:organization][:terms_of_service_change_note],
    )
      flash[:notice] = "Updated the terms of service."
    else
      flash[:error] = "Failed to update terms of service."
    end

    redirect_to terms_of_service_stafftools_user_path(@user)
  end

  def overview
    fetch_error_states
    view = Stafftools::User::ShowView.new(user: @user, current_user: current_user)
    render "stafftools/users/overview",
           locals: { view: view, error_states: @error_states, spam_flag_timestamp: @spam_flag_timestamp }
  end

  def public_repos
    render_template_view "stafftools/users/public_repos",
        Stafftools::User::PublicReposView,
        user: @user,
        page: params[:repo_page]
  end

  def anon_access_repos
    render_template_view "stafftools/users/anon_access_repos",
                         Stafftools::User::AnonymousGitAccessReposView,
                         user: @user,
                         page: params[:repo_page]
  end

  private def fetch_error_states
    is_user = this_user.user?
    verified_emails = this_user.emails.user_entered_emails.verified
    has_keys = !this_user.public_keys.count.zero?

    @error_states = {
      spammy: this_user.spammy?,
      suspended: this_user.suspended?,
      no_primary_email: is_user && !this_user.has_primary_email?,
      no_verified_email: GitHub.email_verification_enabled? && is_user && verified_emails.count.zero?,
      dupe_email: is_user && this_user.has_duplicate_email?,
      org_transform: is_user && Organization.transforming?(this_user),
      unverified_keys: has_keys && this_user.verified_keys.count.zero?,
      blacklisted_login: this_user.login_blacklisted?,
    }

    @spam_flag_timestamp = spam_flag_timestamp
    @obfuscated_dupe_emails = if GitHub.enterprise?
      0
    else
      GitHub::SpamChecker.count_obfuscated_duplicate_emails(this_user)
    end
  end

  def site_admins
    index_view = Stafftools::User::IndexView.new(title: "Site admins")
    users = paginate(User, user_query("gh_role = 'staff'"))

    instrument("staff.view_site_admins")
    render "stafftools/users/index", locals: { view: index_view, users: users }
  end

  def suspended
    index_view = Stafftools::User::IndexView.new(title: "Suspended users")
    users = paginate(User, user_query("suspended_at IS NOT NULL"))
    render "stafftools/users/index", locals: { view: index_view, users: users }
  end

  def invoiced
    render "stafftools/users/invoiced", locals: { invoiced_accounts: User.invoiced.order("billed_on asc") }
  end

  def restore
    account = Stafftools::RestoreAccount.perform(current_user, params)
    success = account.save

    msg = success ? "Account restored" : "Unable to restore the account: #{account.errors.full_messages.join(", ")}"

    respond_to do |format|
      format.html do
        if request.xhr?
          render html: msg
        else
          key = success ? :notice : :error
          flash[key] = msg
          redirect_to :back
        end
      end
    end
  end

  def dormant
    index_view = Stafftools::User::IndexView.new(title: "Dormant users")
    users = User.dormant_users.paginate(page: current_page)
    render "stafftools/users/dormant", locals: { view: index_view, users: users }
  end

  def by_ip
    return render_404 unless params[:last_ip]

    conditions = "suspended_at is null"

    @title = "Users at #{params[:last_ip]}"
    @users = User.by_ip(params[:last_ip]).where(conditions).page(current_page)
    index_view = Stafftools::User::IndexView.new(title: @title)
    render "stafftools/users/index", locals: { view: index_view, users: @users }
  end

  def billing_managers
    billing_managers = this_user.billing_managers.paginate(page: current_page)
    render "stafftools/users/billing_managers", locals: { user: this_user, billing_managers: billing_managers }
  end

  UserAdminQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Stafftools::Users::Admin::Account
      }
    }
  GRAPHQL

  def admin
    headers["Cache-Control"] = "no-cache, no-store"

    data = platform_execute(UserAdminQuery, variables: { id: this_user.global_relay_id })
    admin_view = Stafftools::User::AdminView.new(user: @user)
    render "stafftools/users/admin", locals: {
      view: admin_view,
      spam_flag_timestamp: spam_flag_timestamp,
      graphql_account: data.node,
      dropdown_metrics: org_insights_dropdown_metrics,
    }
  end

  UserBillingQuery = parse_query <<-'GRAPHQL'
    query($id: ID!,
      $first: Int,
      $last: Int,
      $after: String,
      $before: String ) {
      ...Views::Stafftools::Users::Billing::Fragment
    }
  GRAPHQL

  def billing
    data = platform_execute UserBillingQuery, variables: {id: this_user.global_relay_id}.merge(graphql_pagination_params(page_size: 30))

    if @user.plan.per_seat? || (@user.plan.free? && @user.organization?)
      @seat_change = Billing::PlanChange::SeatChange.new(@user, seats: @user.seats)
    end
    billing_view = Stafftools::User::BillingView.new(user: @user, current_user: current_user, params: params, seat_change: @seat_change)
    render "stafftools/users/billing", locals: { view: billing_view, data: data }
  end

  def emails
    # ensure email verification actions redirect to staff land
    store_location
  end

  def collab_repos
    locals = {}

    locals[:repos] = this_user.member_repositories.order(:name)
      .paginate(page: current_page)

    locals[:invites] = if current_page == 1
      this_user.received_repository_invitations.includes(:repository)
    else
      []
    end

    render "stafftools/users/collab_repos", locals: locals
  end

  def keys
    if request.post? && params[:token]
      @valid, @message = GitHub::SshVerification.validate(params[:token], this_user)
    end
    keys_view = Stafftools::User::KeysView.new(user: @user)
    render "stafftools/users/keys", locals: {view: keys_view, valid: @valid, message: @message}
  end

  def abilities
    grants = Ability.grants(@user)
    abilities_view = Stafftools::User::AbilitiesView.new(user: @user, grants: grants)
    render "stafftools/users/abilities", locals: { view: abilities_view }
  end

  def search
    search_view = Stafftools::User::SearchView.new(user: @user)
    render "stafftools/users/search", locals: { view: search_view }
  end

  def activity
    activity_view = Stafftools::User::ShowView.new(user: @user, current_user: current_user)
    render "stafftools/users/activity", locals: { view: activity_view }
  end

  def watching
    watching_view = Stafftools::User::WatchingView.new(user: @user)
    render "stafftools/users/watching", locals: { view: watching_view }
  end

  def database
    database_view = Stafftools::User::DatabaseView.new(user: @user)
    render "stafftools/users/database", locals: { view: database_view }
  end

  def rate_limits
    context = Stafftools::RateLimitContext.new(@user)
    rate_limits_view = Stafftools::User::RateLimitsView.new(@user, context)
    render "stafftools/users/rate_limits", locals: { view: rate_limits_view }
  end

  def invite
    render "stafftools/users/invite", locals: { new_user: User.new }
  end

  RETIRED_NAMESPACES_PAGE_SIZE = 25
  RetiredNamespacesQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $first: Int, $last: Int, $before: String, $after: String) {
      ...Views::Stafftools::Users::RetiredNamespaces::RetiredNamespace
    }
  GRAPHQL

  def retired_namespaces
    return render_404 unless GitHub.retired_namespaces_enabled?

    variables = { login: @user.login }
    variables.merge!(graphql_pagination_params(page_size: params.fetch(:page_size, RETIRED_NAMESPACES_PAGE_SIZE).to_i))
    data = platform_execute RetiredNamespacesQuery, variables: variables
    render "stafftools/users/retired_namespaces", locals: { data: data, user: @user }
  end

  def domains
    render "stafftools/users/domains", locals: { organization: this_user }
  end

  AbuseReportsQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $first: Int,
      $after: String,
      $last: Int,
      $before: String
    ) {
      user(login: $login) {
        ...Views::Stafftools::Users::AbuseReports::User
      }
    }
  GRAPHQL

  def abuse_reports
    return render_404 unless GitHub.user_abuse_mitigation_enabled?

    variables = { login: params[:id] }.merge(graphql_pagination_params(page_size: 50))

    data = platform_execute(AbuseReportsQuery, variables: variables)
    return render_404 unless data.user.present?

    render "stafftools/users/abuse_reports", locals: { user: data.user }
  end

  def successors
    latest_invite = SuccessorInvitation.where(inviter: this_user, target: this_user).last
    render "stafftools/users/successors", locals: { user: this_user, latest_invite: latest_invite }
  end

  def send_invite
    user_params[:login].strip!
    user_params[:email].strip!
    new_user = User.new(user_params)
    new_user.password = generated_password
    new_user.password_confirmation = generated_password
    new_user.plan = GitHub::Plan::ENTERPRISE
    if new_user.save
      reset = PasswordReset.new(
        user: new_user,
        email: new_user.email,
        force: true,
        expires: 24.hours.from_now,
      )
      if reset.valid?
        if GitHub.smtp_enabled?
          flash.now[:notice] = "Account created for #{new_user.login}. Email sent to #{new_user.email}"
          EnterpriseMailer.invite_user(new_user, reset.link, reset.expires.to_s).deliver_later
        else
          flash.now[:notice] = "Account created for #{new_user.login}. This password reset link must be manually sent to the new user: #{reset.link}"
        end
      else
        flash.now[:error] = reset.error_message
      end
      new_user = User.new
    else
      flash.now[:error] = "Could not invite user."
    end
    render "stafftools/users/invite", locals: { new_user: new_user }
  end

  def dismiss_license_notice
    expires = params[:ttl] ? params[:ttl].to_i.seconds.from_now : nil

    enterprise_license.hide_warning(current_user, params[:type], expires: expires)

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  def dismiss_certificate_notice
    expires = params[:ttl] ? params[:ttl].to_i.seconds.from_now : nil
    enterprise_certificate.hide_warning(current_user, expires: expires)

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  def rename
    GitHub.context.push({
      hydro: { actor: current_user }, # nested to avoid event_context serialization
    })

    login = params[:login]
    old_name = this_user.login

    if this_user.rename(login, actor: current_user)
      render "stafftools/users/rename", locals: { user: this_user, new_login: login, old_login: old_name }
    else
      flash[:error] = this_user.errors.full_messages.to_sentence
      redirect_to admin_stafftools_user_path(this_user)
    end
  end

  def rename_status
    user = User.find_by_id(params[:user_id])
    return render_404 unless user

    if user.renaming?
      head 202
    else
      respond_to do |format|
        format.html do
          render partial: "stafftools/users/rename_complete", locals: { user: user }
        end
      end
    end
  end

  def toggle_operator_mode
    if this_user.operator_mode_enabled?
      this_user.disable_operator_mode(current_user)
      flash[:notice] = "Operator mode disabled."
    else
      this_user.enable_operator_mode(current_user)
      flash[:notice] = "Operator mode enabled."
    end

    redirect_to :back
  end

  def change_ssh_access
    val = params[:value].presence || false
    policy = params[:ssh_local_policy].present? || false

    if val == "_clear"
      @user.clear_ssh(current_user)
    else
      val == "true" ? @user.enable_ssh(current_user, policy) : @user.disable_ssh(current_user, policy)
      policy_level = policy ? "and enforced on all repositories" : "by default"
    end

    flash[:notice] = case val
    when "true" || true
      "Git SSH access enabled #{policy_level}."
    when "false" || false
      "Git SSH access disabled #{policy_level}."
    when "_clear"
      "Setting cleared. Instance default will be used."
    end

    redirect_to :back
  end

  # Mark a user as suspended. This prevents the user from being able to login,
  # push, pull, etc.
  #
  # If LDAP sync is enabled with Active Directory, notify the admin that account
  # suspension needs to be managed by LDAP.
  #
  # If the user is a site admin, notify the admin that the account needs to be demoted
  # before being suspended
  def toggle_suspension
    if this_user.external_account_suspension?
      flash[:error] = "Account suspension is managed by Active Directory. Disable the user from your directory."
    elsif this_user.suspended?
      if this_user.unsuspend(params[:reason], actor: current_user)
        flash[:notice] = "#{this_user.login} unsuspended"
      else
        flash[:error] = this_user.errors[:base].to_sentence
      end
    else
      if !this_user.suspendable?
        flash[:error] = "Site admins can not be suspended. Remove site admin privileges before suspending."
      elsif this_user.suspend(params[:reason], actor: current_user)
        flash[:notice] = "#{this_user.login} suspended"
      else
        flash[:error] = this_user.errors[:base].to_sentence
      end
    end

    redirect_to :back
  end

  def toggle_rate_limit_whitelisted
    this_user.toggle_content_creation_rate_limit_whitelisted whitelister: current_user
    redirect_to :back
  end

  def temporary_rate_limit_whitelist
    this_user.temporarily_whitelist_content_creation(whitelister: current_user)
    redirect_to :back
  end

  # Mark all dormat users as suspended.
  def suspend_dormant
    User.suspend_dormant_users
    flash[:notice] = "Dormant users queued for suspension."
    redirect_to stafftools_path
  end

  # Resets the rate limits found in RATE_LIMIT_FAMILIES for the specified user
  def reset_rate_limits
    context = Stafftools::RateLimitContext.new(this_user)

    Api::RateLimitConfiguration::ALL_FAMILIES.each do |family|
      config = Api::RateLimitConfiguration.for(family, context)
      Api::ConfigThrottler.new(config).remove!
    end

    flash[:notice] = "Rate limits reset for #{this_user.login}"

    redirect_to :back
  end

  # Set the "staff" flag on this user.  Gives the user superpowers across the
  # whole site.
  def set_site_admin
    if this_user.grant_site_admin_access(params[:reason])
      flash[:notice] = "#{this_user.login} promoted to site admin"
      redirect_to :back
    else
      flash[:error] = if GitHub.enterprise?
        ERR_COULD_NOT_PROMOTE_GHE
      else
        ERR_COULD_NOT_PROMOTE_GH
      end
      redirect_to :back
    end
  end

  def mark_deceased
    if this_user.mark_deceased
      flash[:notice] = "#{this_user.login} marked as deceased"
    else
      flash[:error] = this_user.errors.full_messages.to_sentence
    end
    redirect_to stafftools_user_path(this_user)
  end

  # Set the "dev" flag on this user. Gives them access to devtools.
  def set_github_developer
    return render_404 unless GitHub.devtools_enabled?
    if this_user.grant_github_developer_access(params[:reason])
      flash[:notice] = "#{this_user.login} promoted to GitHub developer"
      redirect_to :back
    else
      flash[:error] = "You have to specify a reason for the log."
      redirect_to :back
    end
  end

  # Set the "biz" flag on this user. Gives them access to biztools.
  def set_github_biztools_user
    return render_404 unless GitHub.billing_enabled?
    if this_user.grant_github_biztools_access(params[:reason])
      flash[:notice] = "#{this_user.login} promoted to GitHub biztools user"
      redirect_to :back
    else
      flash[:error] = "You have to specify a reason for the log."
      redirect_to :back
    end
  end

  # Unset the "staff" , "biz". and "dev" flags on this user.
  def revoke_privileged_access
    if this_user.revoke_privileged_access(params[:reason])
      flash[:notice] = "#{this_user.login} demoted to normal user"
      redirect_to :back
    else
      flash[:error] = "You have to specify a reason for the log."
      redirect_to :back
    end
  end

  # Deletes an account directly, with failsafes to block deleting paying users
  def destroy
    if this_user.system_account?
      flash[:error] = "This account is a system account and can't be deleted."
      return redirect_to :back
    end

    if GitHub.enterprise? && !this_user.organization?
      last_admin_of = this_user.solitarily_owned_organizations

      if last_admin_of.any?
        flash[:error] = "This user is the only admin in the following organizations and can't be deleted: #{last_admin_of.to_sentence}"
        return redirect_to :back
      end
    end

    if GitHub.billing_enabled?
      if this_user.paid_plan?
        flash[:error] = "This is a paying user, what do you think you're doing?"
        return redirect_to :back
      elsif this_user.organizations.any? { |org| org.paid_plan? }
        flash[:error] = "This user is in a paid org, it can't be deleted."
        return redirect_to :back
      end
    end

    # Set the "staff_delete" role to ensure they are NOT sent a
    # "you deleted your account" email
    this_user.update_attribute :gh_role, "staff_delete"
    this_user.async_destroy(current_user)
    flash[:notice] = "User deleted"
    redirect_to stafftools_path
  end

  # Unwatches a repo for a user
  def remove_watch
    repo = Repository.find(params[:repo_id])
    this_user.unwatch_repo(repo)
    respond_to do |format|
      format.html do
        if request.xhr?
          head :ok
        else
          redirect_to :back
        end
      end
    end
  end

  # Removes a lockout due to rate limiting authentication attempts.
  def remove_lockout
    AuthenticationLimit.clear_data(AuthenticationLimit.all_data_for_login(this_user.login))
    flash[:notice] = "#{this_user.login} unlocked"
    redirect_to :back
  end

  # In the event that an employee has been compromised (lost equipment, stolen
  # passwords etc.) this will revoke all tokens, passwords, GitHub org
  # affiliations, etc until that employee is able to sort things out.
  def revoke_employee_access
    unless this_user.user? && this_user.employee? && GitHub.require_employee_for_site_admin?
      flash[:error] = "Access for #{this_user.login} cannot be revoked."
      return redirect_to :back
    end

    # Suspend the user, this also revokes all active sessions.
    this_user.suspend(params[:reason], actor: current_user)

    # Revoke all OAuth authorizations.
    this_user.oauth_authorizations.each do |oauth_authorization|
      oauth_authorization.destroy_with_explanation(:site_admin)
    end

    # Unverify all public keys.
    this_user.public_keys.each do |public_key|
      public_key.unverify(:site_admin)
    end

    # Remove GitHub org affiliations.
    github_org = Organization.find_by_login("github")
    github_org.remove_any_affiliation(this_user, actor: current_user)

    # Remove all special hubber privileged roles.
    this_user.revoke_privileged_access(params[:reason])

    # Clear their password.
    this_user.set_random_password(actor: current_user)

    flash[:notice] = "All access for #{this_user.login} has been revoked."
    redirect_to :back
  end

  # Charges the user's card
  def charge
    if !this_user.has_valid_payment_method?
      flash[:error] = "No valid payment method on file for this user."
    elsif !(this_user.billed_on.nil? || this_user.billed_on <= Date.today || this_user.billing_attempts > 0)
      flash[:error] = "User cannot be charged at this time."
    else
      result = this_user.recurring_charge
      if result.success?
        # Accounts transitioning to external subscriptions are charged asynchronously
        flash[:notice] = "Charge initiated – check Payment History for status."
        return redirect_to stafftools_user_billing_history_url(this_user)
      else
        flash[:error] = result.error_message
      end
    end
    redirect_to :back
  end

  def remove_credit_card
    if this_user.remove_all_payment_methods(current_user)
      flash[:notice] = "Successfully removed all payment methods."
    else
      flash[:error] = "One or more payment methods were not removed."
    end
    redirect_to :back
  end

  def pay_by_invoice
    old_type = this_user.billing_type
    billed_on = Date.parse(params[:new_term_end_date]) + 1.day if params[:new_term_end_date]
    this_user.billed_on = billed_on
    this_user.switch_billing_type_to_invoice(current_user)
    if params[:plan] && params[:plan] != this_user.plan.to_s
        GitHub::Billing.change_subscription(this_user, actor: current_user, plan: params[:plan])
    end
    # This instrument call sends data to the Audit Log.
    # There is a similar instrumentation in User#switch_billing_type_to_invoice
    # that logs to Hydro but it does not have access to the stafftools controller context.
    instrument("billing.change_billing_type",
               old_billing_type: old_type,
               billing_type: this_user.billing_type,
               user: this_user)
    flash[:notice] = "Successfully switched account to invoice billing."
    redirect_to :back
  end

  def pay_by_credit_card
    old_type = this_user.billing_type
    this_user.switch_billing_type_to_card(current_user)
    # This instrument call sends data to the Audit Log.
    # There is a similar instrumentation in User#switch_billing_type_to_card
    # that logs to Hydro but it does not have access to the stafftools controller context.
    instrument("billing.change_billing_type", old_billing_type: old_type,
               billing_type: this_user.billing_type, user: this_user)
    flash[:notice] = "Successfully switched account to self serve billing."

    redirect_to :back
  end

  # Creates a new staff note on an account
  def add_note
    StaffNote.create({user: current_user, notable: this_user, note: params[:staff_note][:note]})
    flash[:notice] = "Note added."
    redirect_to :back
  end

  # Clear a user's public and private activity
  def clear_all_activity
    if this_user.clear_all_timelines
      flash[:notice] = "Cleared public and private activity for #{@user}"
    else
      flash[:error] = "Could not clear activity for #{@user}"
    end

    redirect_to stafftools_user_path(@user)
  end

  # Clear a user's public activity
  def clear_public_activity
    timeline = this_user.events_key(type: :actor_public)
    GitHub.stratocaster.clear_timelines(timeline)

    flash[:notice] = "Cleared public activity for #{this_user.login}"
    redirect_to stafftools_user_path(this_user)
  end

  def change_plan
    old_seat_count = @user.seats
    old_plan = @user.plan
    old_plan_duration = @user.plan_duration
    old_plan = @user.plan
    old_seat_count = @user.seats
    @user.billing_extra = params[:billing_extra] if params[:billing_extra]
    @user.billing_type  = params[:billing_type]  if params[:billing_type]

    result = if params[:seats]
      GitHub::Billing.change_seats \
        @user,
        actor: current_user,
        seats: params[:seats].to_i,
        plan_duration: params[:plan_duration]
    elsif params[:plan] || params[:plan_duration]
      GitHub::Billing.change_subscription \
        @user,
        actor: current_user,
        plan: params[:plan],
        plan_duration: params[:plan_duration]
    else
      GitHub::Billing::Result.success
    end

    if params[:plan]
      analytics_event(
        organization_plan_change_ga_event_attributes(
          result.success?,
          @user,
          current_user,
          old_plan,
          params[:plan] || old_plan,
          old_seat_count,
          params[:seats].present? ? params[:seats].to_i : old_seat_count,
        ),
      )
    end

    if result.success?
      if @user.save
        if delete_custom_roles?(@user, old_plan.name, params[:plan])
          @user.custom_roles.each { |role| Permissions::CustomRoles.destroy!(role) }
        end

        if old_plan_duration != @user.plan_duration
          @user.track_plan_duration_change(current_user, old_plan_duration)
        end

        if params[:seats]
          publish_billing_seat_count_change_for(
            actor: current_user,
            user: @user,
            old_seat_count: old_seat_count,
            new_seat_count: @user.seats,
          )
        else
          publish_billing_plan_changed_for(
            actor: current_user,
            user: @user,
            old_seat_count: old_seat_count,
            new_seat_count: @user.seats,
            old_plan_name: old_plan.name,
            new_plan_name: params[:plan],
          )

          notify_marketing_of_cloud_trial_upgrade(@user, params[:plan], :sales_serve)
        end

        flash[:notice] = "Plan changed"
      else
        flash[:error] = "Error changing plan: #{@user.errors.full_messages.join}"
      end
    else
      flash[:error] = "Error changing plan: #{result.error_message}"
    end

    redirect_to :back
  end

  def lock_billing
    @user.disable!

    flash[:notice] = "User locked"
    redirect_to :back
  end

  def unlock_billing
    @user.unlock_billing!

    flash[:notice] = "User unlocked"
    redirect_to :back
  end

  def sync_external_subscription
    if this_user.invoiced?
      flash[:notice] = "Invoiced subscriptions cannot be synced."
    else
      this_user.create_or_update_external_subscription!(force: true)
      flash[:notice] = "External subscription sychronization enqueued."
    end
    redirect_to :back
  end

  def reset_billing_attempts
    @user.reset_billing_attempts

    Billing::Zuora::ResetPaymentMethodConsecutiveFailures.new(payment_method: @user.payment_method).perform

    flash[:notice] = "Billing attempts reset"
    redirect_back(fallback_location: billing_stafftools_user_path(@user))
  end

  def reindex
    @user.calculate_primary_language!
    Search.add_to_search_index("user", @user.id)
    flash[:notice] = "Reindex job was enqueued."
    redirect_to :back
  end

  # This will reset the org insights dashboard data for the business org
  def backfill_org_insights
    if this_user.organization?
      GitHub.dogstats.increment("business", tags: ["action:backfill_org_insights", "type:user"])
      GitHub::OrgInsights::Backfill.for_org(org_id: this_user.id, metrics: org_insights_metric_param)
      flash[:notice] = org_insights_notice_message
    else
      flash[:notice] = "Cannot backfill for a non-organization user."
    end

    redirect_to :back
  end

  # Rebuild CommitContribution data for this user's repositories. Sometimes
  # needed to reindex commits due to failed job or other indexing issue.
  def rebuild_commit_contributions
    GitHub.dogstats.increment("repository", tags: ["action:rebuild_commit_contributions", "type:user"])

    this_user.rebuild_contributions

    flash[:notice] = "Rebuild commit contributions jobs enqueued…"
    redirect_to :back
  end

  def toggle_large_scale_contributor
    if @user.large_scale_contributor?
      @user.remove_large_scale_contributor_flag!(actor: current_user)
      flash[:notice] = "Successfully removed large-scale contributor flag for #{@user}."
    else
      @user.flag_as_large_scale_contributor!(actor: current_user)
      flash[:notice] = "Successfully flagged #{@user} as a large-scale contributor."
    end

    redirect_to :back
  end

  def flag_contribution_classes
    contribution_classes = params[:contribution_classes]&.map(&:constantize) || []

    @user.flag_contribution_classes!(contribution_classes, actor: current_user)
    if contribution_classes.empty?
      flash[:notice] = "Successfully unflagged all contribution types for #{@user}."
    else
      flagged_contributions = contribution_classes.map do |contribution_class|
        contribution_class.name.demodulize
      end
      flash[:notice] = "Successfully flagged #{flagged_contributions.join(", ")} for #{@user}."
    end

    redirect_to :back
  end

  def change_allow_force_push
    val = params[:value].presence || false
    policy = params[:policy].present? || false

    if val == "_clear"
      @user.clear_force_push_rejection(current_user)
    else
      @user.set_force_push_rejection val, current_user, policy
      policy_level = policy ? "and enforced on all repositories" : "by default"
    end

    flash[:notice] = case val
    when false
      "Force pushing allowed #{policy_level}."
    when "all"
      "Force pushing blocked #{policy_level}."
    when "default"
      "Force pushing blocked on the default branch #{policy_level}."
    when "_clear"
      "Setting cleared. Instance default will be used."
    end

    redirect_to :back
  end

  # Block a user / organization on behalf of this user.
  #
  # login - The username of the account to block.
  def block_user
    if blocked_user = User.find_by_login(params[:login])
      this_user.block blocked_user, actor: current_user
      instrument("staff.block_user", user: this_user,
        blocked_user: blocked_user.login, blocked_user_id: blocked_user.id)
      flash[:notice] = "Blocking #{blocked_user.login} on behalf of #{this_user.login}"
      redirect_to :back
    else
      flash[:error] = "Nice try, there isn't anybody called #{params[:login]}"
      redirect_to :back
    end

  end

  # Unblock a user / organization on behalf of this user.
  #
  # login - The username of the account to unblock.
  def unblock_user
    if other_user = User.find_by_login(params[:login])
      this_user.unblock other_user, actor: current_user
      instrument("staff.unblock_user", user: this_user,
        unblocked_user: other_user.login, unblocked_user_id: other_user.id)
      flash[:notice] = "#{this_user.login} is no longer ignoring #{other_user.login}"
      redirect_to :back
    else
      flash[:error] = "Whoops. There isn't anyone called #{params[:login]}"
      redirect_to :back
    end
  end

  # Unfollow a user / organization on behalf of this user.
  #
  # login - The username of the account to unfollow.
  def unfollow_user
    if other_user = User.find_by_login(params[:login])
      this_user.unfollow other_user
      instrument("staff.unfollow_user", user: this_user,
        unfollowed_user: other_user.login, unfollowed_user_id: other_user.id)
      flash[:notice] = "Unfollwed #{other_user.login} on behalf of #{this_user.login}"
      redirect_to :back
    else
      flash[:error] = "Whoops. There isn't anyone called #{params[:login]}"
      redirect_to :back
    end
  end

  def security
    security_view = Stafftools::User::SecurityView.new(user: @user)
    render "stafftools/users/security", locals: { view: security_view }
  end

  # Redirect the old audit log path to the new audit log query
  def security_history
    keys = %w{actor_id user_id}
    keys << "org_id" if this_user.organization?
    query = keys.reverse.map { |k| "#{k}:#{this_user.id}" }.join " OR "
    redirect_to stafftools_audit_log_path(query: query)
  end

  def remove_transform_lockout
    Organization.end_transform(this_user)
    flash[:notice] = "Org transform lockout cleared for #{this_user}"
    redirect_to :back
  end

  def run_team_sync_configuration
    UpdateTeamSyncForBusinessOrganizationJob.perform_later(org_id: this_user.id)
    flash[:notice] = "Scheduled configuration for #{@user} to match enterprise configuration."
    redirect_to :back
  end

  def ofac_sanction_override
    if @user.trade_controls_restriction.override!(
        compliance: TradeControls::Compliance.for(actor: current_user, reason: params[:reason]))
      flash[:notice] = "Successfully removed Office of Foreign Assets Control sanctions for #{@user}."
    else
      flash[:error] = "Error while overriding Office of Foreign Assets Control flag: #{@user.errors.full_messages.to_sentence}"
    end

    redirect_back(fallback_location: billing_stafftools_user_url(@user))
  end

  # Flag/unflag an organization as subject to EU data transfer SCC.
  # Used by GitHub's legal teams to track the organizations.
  def toggle_standard_contractual_clauses
    if @user.standard_contractual_clauses?
      @user.remove_standard_contractual_clauses_flag!(actor: current_user)
      flash[:notice] = "Succesfully removed SCC flag for #{@user}."
    else
      @user.flag_for_standard_contractual_clauses!(actor: current_user)
      flash[:notice] = "Succesfully flagged #{@user} for Standard Contractual Clauses."
    end
    redirect_to :back
  end

  def user_interactions
    user_interactions_view = Stafftools::User::UserInteractionsView.new(user: @user)
    instrument("staff.view_user_interactions", user: this_user)
    render "stafftools/users/user_interactions", locals: { view: user_interactions_view }
  end

  def toggle_interaction_ban
    User::InteractionAbility.toggle_interaction_ban(@user.id)
    redirect_to :back
  end

  def toggle_legal_hold
    if params[:deleted_user_id]
      user  = User.new(id: params[:deleted_user_id], login: params[:login])
    else
      ensure_user_exists
      user = @user
    end
    if user.legal_hold?
      if user.clear_legal_hold(actor: current_user)
        flash[:notice] = "Successfully cleared a legal hold on #{user.login}."
      else
        flash[:error] = "Couldn't remove a legal hold on #{user.login}."
      end
    else
      if user.place_legal_hold(actor: current_user)
        flash[:notice] = "Successfully placed a legal hold on #{user.login}."
      else
        flash[:error] = "Couldn't place a legal hold on #{user.login}."
      end
    end
    redirect_to :back
  end

  def toggle_acv_contribution_ignore
    contribution = AcvContributor.find(params[:contribution_id])

    if contribution
      contribution.ignore = !contribution.ignore
      contribution.save
    else
      return render_404
    end

    flash[:notice] = "Contribution has been #{contribution.ignore ? "flagged" : "unflagged"} for ignore."
    redirect_to :back
  end

  def run_pending_changes
    plan_change =
      if params[:pending_change_id]
        @user.pending_plan_changes.find_by(id: params[:pending_change_id])
      else
        @user.pending_cycle_change
      end

    unless plan_change
      flash[:error] = "No pending plan change was found for #{@user}"
      return redirect_to :back
    end

    if plan_change.run
      flash[:notice] = "Pending plan changes were successfully applied for #{@user}."
    else
      flash[:error] = "Unable to run pending plan changes: #{plan_change.errors.full_messages.join(", ")}"
    end

    redirect_to :back
  end

  def set_hubber_access_reason
    current_user.set_hubber_access_reason(this_user, params[:reason])
    flash[:notice] = "The staff user #{this_user} has been unlocked for #{current_user}."
    redirect_to :back
  end

  RetireNamespaceMutation = parse_query <<-'GRAPHQL'
    mutation($ownerLogin: String!, $name: String!) {
      retireNamespace(input: { ownerLogin: $ownerLogin, name: $name }) {
        retiredNamespace {
          nameWithOwner
        }
      }
    }
  GRAPHQL

  def retire_namespace
    return render_404 unless GitHub.retired_namespaces_enabled?

    variables = { ownerLogin: @user.login, name: params[:name] }
    mutation = platform_execute RetireNamespaceMutation, variables: variables
    if mutation.errors.any?
      flash[:error] = mutation.errors.messages.values.join(", ")
      redirect_to :back
    else
      flash[:notice] = "Retired #{@user.login}/#{params[:name]}"
      redirect_to :back
    end
  end

  UnretireNamespaceMutation = parse_query <<-'GRAPHQL'
    mutation($ownerLogin: String!, $name: String!) {
      unretireNamespace(input: { ownerLogin: $ownerLogin, name: $name }) {
        clientMutationId
      }
    }
  GRAPHQL

  def unretire_namespace
    return render_404 unless GitHub.retired_namespaces_enabled?

    variables = { ownerLogin: @user.login, name: params[:name] }
    mutation = platform_execute UnretireNamespaceMutation, variables: variables

    if mutation.errors.any?
      flash[:error] = mutation.errors.messages.values.join(", ")
      redirect_to :back
    else
      flash[:notice] = "Unretired #{@user.login}/#{params[:name]}"
      redirect_to :back
    end

  end

  SetInteractionLimitMutation = parse_query <<-'GRAPHQL'
     mutation($input: SetOrganizationInteractionLimitInput!) {
      setOrganizationInteractionLimit(input: $input)
    }
  GRAPHQL

  def set_interaction_limit
    return render_404 unless GitHub.interaction_limits_enabled?

    input_variables = {
      organizationId: this_user.global_relay_id,
      limit: params[:interaction_setting],
      expiry: "ONE_WEEK",
      isStaffActor: true,
    }

    results = platform_execute(SetInteractionLimitMutation, variables: { input: input_variables })

    if results.errors.any?
      flash[:error] = results.errors.all.values.flatten.to_sentence
    else
      flash[:notice] = "Organization interaction limit settings saved."
    end

    redirect_to :back
  end

  def clear_org_token_scan_backfill
    if @user.token_scanning_stafftools_available?
      TokenScanningBackfillOrganizationJob.perform_later(@user.id, "clear")
      flash[:notice] = "Secret scanning organization job is now queued up to clear backfill scan state of this organization's private repositories."
    else
      flash[:error] = "This organization is not eligible for secret scanning."
    end
    redirect_to :back
  end

  def start_org_token_scan_backfill
    if @user.token_scanning_stafftools_available?
      TokenScanningBackfillOrganizationJob.perform_later(@user.id)
      flash[:notice] = "Secret scanning backfill organization job is now queued up to run scans of this organization's private repositories."
    else
      flash[:error] = "This organization is not eligible for secret scanning."
    end
    redirect_to :back
  end

  private

  def generate_password!(len = 20)
    SecureRandom.hex(len)
  end

  def generated_password
    @generated_password ||= generate_password!
  end

  def user_query(extra = nil)
    extra = "AND #{extra}" unless extra.blank?
    order = users_sort_labels[user_sort_order] || "created_at DESC"

    {
      order: order,
      page: current_page,
      conditions: [
        "login != ? AND type = 'User' #{extra}",
        [GitHub.ghost_user_login],
      ],
    }
  end

  def paginate(scope, query)
    scope.order(query[:order]).where(*query[:conditions]).page(query[:page])
  end

  def user_sort_order
    @user_sort_order ||= if users_sort_labels.keys.include?(params[:sort])
      params[:sort]
    else
      users_sort_labels.keys.first
    end
  end
  helper_method :user_sort_order

  def users_sort_labels
    {
      "alphanumerically" => "login ASC",
      "newest"           => "created_at DESC",
      "oldest"           => "created_at ASC",
    }
  end
  helper_method :users_sort_labels

  def spam_flag_timestamp
    if this_user.spammy?
      options = {
        phrase:       "action:staff.mark_as_spammy user_id:#{this_user.id}",
        current_user: current_user,
      }
      query = Audit::Driftwood::Query.new_stafftools_query(options)
      results = AuditLogEntry.new_from_array(query.execute)

      results.first.created_at if results.first
    end
  end

  def ensure_user_invites_enabled
    render_404 unless GitHub.user_invites_enabled?
  end

  def ensure_rate_limiting_enabled
    render_404 unless GitHub.rate_limiting_enabled? && !GitHub.enterprise?
  end

  def check_anon_access_enabled
    render_404 unless GitHub.anonymous_git_access_enabled?
  end

  def ensure_user_not_org
    render_404 unless this_user&.user?
  end

  def user_params
    params.require(:user).permit(:login, :email)
  end

  def publish_billing_plan_changed_for(actor:,
                                       user:,
                                       new_seat_count: 0,
                                       old_seat_count: 0,
                                       old_plan_name: "",
                                       new_plan_name:)
    GlobalInstrumenter.instrument(
      "billing.plan_change",
      actor_id: actor.id,
      user_id: user.id,
      old_plan_name: old_plan_name,
      old_seat_count: old_seat_count,
      new_plan_name: new_plan_name,
      new_seat_count: new_seat_count,
    )
  end

  def publish_billing_seat_count_change_for(actor:,
                                       user:,
                                       new_seat_count: 0,
                                       old_seat_count: 0)
    GlobalInstrumenter.instrument(
      "billing.seat_count_change",
      actor_id: actor.id,
      user_id: user.id,
      old_seat_count: old_seat_count,
      new_seat_count: new_seat_count,
    )
  end

  def org_insights_notice_message
    if org_insights_metric_param && org_insights_metric_param != "all"
      "Org Insights backfill of #{org_insights_metric_param} started for #{this_user}..."
    else
      "Org Insights backfill started for #{this_user}..."
    end
  end

  def org_insights_metric_param
    metric = params[:metric_name]
    metric if org_insights_allowed_metrics.include?(metric)
  end

  def org_insights_allowed_metrics
    GitHub::OrgInsights::Metric.subclasses
  end

  def org_insights_dropdown_metrics
    return @org_insights_dropdown_metrics if @org_insights_dropdown_metrics

    @org_insights_dropdown_metrics = [["All Metrics", "all"]]
    org_insights_allowed_metrics.each do |m|
      @org_insights_dropdown_metrics << [m.titleize, m]
    end

    @org_insights_dropdown_metrics
  end

  # Internal: business_plus supports 3 custom roles. business and free support none.
  # staff forced billing change from business_plus to any other plan results in the deletion of all custom roles.
  # This check only makes sense for Organizations.
  #
  # - actor: the target organization. A User.
  # - old_plan: the old billing plan. A String.
  # - new_plan: the new billing plan. A String.
  #
  # Returns a Boolean
  def delete_custom_roles?(actor, old_plan, new_plan)
    actor.organization? && GitHub.flipper[:custom_roles].enabled?(actor) &&
      old_plan == GitHub::Plan.business_plus.name && new_plan != GitHub::Plan.business_plus.name
  end
end
