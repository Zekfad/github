# frozen_string_literal: true

class Stafftools::RepositoryRecommendationsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :discover_repos_dashboard_required
  layout "stafftools/repository/overview"

  IndexQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository: staffAccessedRepository(owner: $owner, name: $name) {
        ...Views::Stafftools::RepositoryRecommendations::Index::Repository
      }
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: {
      owner: params[:user_id], name: params[:repository_id]
    })
    repository = data.repository
    return render_404 unless repository

    render "stafftools/repository_recommendations/index", locals: { repository: repository }
  end

  OptOutQuery = parse_query <<-'GRAPHQL'
    mutation($input: OptRepositoryOutOfRecommendationsInput!) {
      optRepositoryOutOfRecommendations(input: $input)
    }
  GRAPHQL

  def opt_out
    failure_message = "Could not opt the repository out of recommendations at this time."
    change_repo_recommendation_status(OptOutQuery, :optRepositoryOutOfRecommendations,
                                      failure_message)
  end

  OptInQuery = parse_query <<-'GRAPHQL'
    mutation($input: OptRepositoryIntoRecommendationsInput!) {
      optRepositoryIntoRecommendations(input: $input)
    }
  GRAPHQL

  def opt_in
    failure_message = "Could not opt the repository back into recommendations at this time."
    change_repo_recommendation_status(OptInQuery, :optRepositoryIntoRecommendations,
                                      failure_message)
  end

  private

  def change_repo_recommendation_status(query, key, failure_message)
    data = platform_execute(query, variables: {
      input: { repositoryId: params[:repository_graphql_id] },
    })

    if data.errors.any?
      error_type = data.errors.details[key]&.first["type"]
      case error_type
      when "NOT_FOUND", "FORBIDDEN"
        return render_404
      else
        flash[:error] = failure_message
      end
    end

    redirect_to stafftools_repository_recommendations_path(params[:user_id], params[:repository_id])
  end
end
