# frozen_string_literal: true

class Stafftools::DiscussionCommentsController < StafftoolsController
  areas_of_responsibility :discussions, :stafftools

  before_action :ensure_discussion_exists
  before_action :ensure_comment_exists, only: [:show, :database, :nested_comments]

  layout "stafftools/repository/collaboration"

  def index
    comments = this_discussion.comments.paginate \
      page: params[:page] || 1,
      per_page: 25

    render("stafftools/discussion_comments/index", locals: {
      comments: comments,
    })
  end

  def show
    conditions = [
      "data.discussion_comment_id:#{this_comment.id}",
      "data.user_content_id:#{this_comment.id} AND data.user_content_type:#{this_comment.class} AND action:user_content_edit.*",
      "data.comment_id:#{this_comment.id} AND data.comment_type:#{this_comment.class}",
    ]
    audit_log_query = conditions.map { |cond| "(#{cond})" }.join(" OR ")
    audit_log_data = fetch_audit_log_teaser(audit_log_query)

    deliveries = Stafftools::UserNotificationDeliveries.for_thread(this_comment)

    notifications_view = Stafftools::RepositoryViews::NotificationsView.new(
      repository: current_repository,
      params: {
        thread: Newsies::Thread.new("Discussion", this_comment.discussion_id || this_comment.id).key,
        comment: Newsies::Comment.to_key(this_comment),
      },
    )

    render("stafftools/discussion_comments/show", locals: {
      notification_deliveries: deliveries,
      notifications_view: notifications_view,
      audit_log_data: audit_log_data,
    })
  end

  def database
    render "stafftools/discussion_comments/database"
  end

  def nested_comments
    comments = this_comment.comments.paginate \
      page: params[:page] || 1,
      per_page: 25

    render("stafftools/discussion_comments/nested_comments", locals: {
      comments: comments,
    })
  end

  private

  def this_comment
    @this_comment ||= this_discussion.comments.find_by_id(params[:id])
  end
  helper_method :this_comment

  def ensure_comment_exists
    return render_404 if this_comment.nil?
  end

end
