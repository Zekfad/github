# frozen_string_literal: true

class Stafftools::SearchIndexesController < StafftoolsController
  before_action :disable_for_private_instance
  before_action :find_index, except: [:index, :new, :create, :update,
                                         :toggle_code_search,
                                         :toggle_code_search_indexing]

  def index
    @clusters = ::Search::ClusterStatus.new
    @indices = []

    index_map = ::Elastomer.router.index_map
    index_map.refresh

    index_names.each do |key|
      ary = index_map.fetch(key)
      next if ary.nil?
      ary.each { |settings| @indices << Search::IndexView.new(settings.to_h) }
    end

    @indices = VersionSorter.sort(@indices) { |index| index.name }

    render "stafftools/search_indexes/index"
  end

  def toggle_code_search
    clusters = ::Search::ClusterStatus.new
    if clusters.code_search_enabled?
      instrument("staff.disable_code_search")
      clusters.disable_code_search
    else
      instrument("staff.enable_code_search")
      clusters.enable_code_search
    end

    redirect_to :back
  end

  def toggle_code_search_indexing
    clusters = ::Search::ClusterStatus.new
    if clusters.code_search_indexing_enabled?
      instrument("staff.disable_code_search_indexing")
      clusters.disable_code_search_indexing
    else
      instrument("staff.enable_code_search_indexing")
      clusters.enable_code_search_indexing
    end

    redirect_to :back
  end

  def new
    @indices = index_names
    @clusters = ::Elastomer.router.available_clusters
    @index = Search::IndexView.new
    render "stafftools/search_indexes/new"
  end

  def create
    index = Search::IndexView.new(search_index_view_params)
    index.create
    redirect_to stafftools_search_index_path(index.name)
  end

  def show
    @repair_job = @index.repair_job
    @kind_has_multiple_primaries = kind_has_multiple_primaries(@index.name)
    render "stafftools/search_indexes/show"
  end

  def update
    index = Search::IndexView.new(search_index_view_params)
    index.update
    redirect_to :back
  end

  def destroy
    if @index.primary
      flash[:error] = "The primary index cannot be deleted."
      redirect_to :back
    else
      @index.destroy
      redirect_to stafftools_search_indexes_path
    end
  end

  def make_primary
    @index.make_primary
    redirect_to :back
  end

  def repair
    respond_to do |format|
      format.html do
        if repair_job = @index.repair_job
          render partial: "stafftools/search_indexes/repair", locals: {repair_job: repair_job}
        else
          head 404
        end
      end
    end
  end

  def start_repair
    if repair_job = @index.repair_job
      workers_count = Integer(params[:workers] || 1)
      repair_job.enable
      repair_job.start workers_count
    end

    redirect_to :back
  end

  def pause_repair
    if repair_job = @index.repair_job
      repair_job.pause
    end

    redirect_to :back
  end

  def reset_repair
    if repair_job = @index.repair_job
      repair_job.reset!
    end

    redirect_to :back
  end

  private

  def find_index
    return render_404 if ::Elastomer.router.get_index_config(params[:id]).nil?
    @index = Search::IndexView.new(name: params[:id])
  end

  def kind_has_multiple_primaries(name)
      kind = Elastomer.env.lookup_index(name).logical_index_name
      primaries = Elastomer.router.index_map.primaries(kind)
      primaries.size > 1
  end

  def index_names
    names = ::Elastomer.env.index_names - %w[audit_log]
    if GitHub.enterprise?
      names -= %w[blog marketplace-listings non-marketplace-listings registry-packages repository-actions enterprises]
      names -= %w[showcases] unless GitHub.showcase_enabled?
    end
    names
  end

  def search_index_view_params
    params.require(:search_index_view).permit(
      :kind,
      :name,
      :cluster,
      :searchable,
      :writable,
    )
  end
end
