# frozen_string_literal: true

class Stafftools::ZuoraWebhooksController < StafftoolsController
  before_action :ensure_billing_enabled
  before_action :lookup_webhook, except: :index

  def index
    @webhooks = ::Billing::ZuoraWebhook.where(status: [:pending, :investigating]).ignoring_recent.order(:created_at)
    render "stafftools/zuora_webhooks/index", locals: { webhooks: @webhooks }
  end

  def ignore
    @webhook.ignored!
    flash[:success] = "Webhook id #{@webhook.id} permanently ignored"
    redirect_back(fallback_location: "stafftools/zuora_webhooks")
  end

  def investigate
    @webhook.update(status: :investigating, investigation_notes: params[:investigation_notes])
    flash[:success] = "Webhook id #{@webhook.id} marked as being investigated"
    redirect_back(fallback_location: "stafftools/zuora_webhooks")
  end

  def perform
    begin
      @webhook.perform
      flash[:success] = "Webhook id #{@webhook.id} successfully processed"
    rescue StandardError => e
      Failbot.report(e, webhook_id: @webhook.id)
      flash[:error] = e
    ensure
      redirect_back(fallback_location: "stafftools/zuora_webhooks")
    end
  end

  private

  def lookup_webhook
    @webhook = ::Billing::ZuoraWebhook.find(params[:zuora_webhook_id])
  rescue ActiveRecord::RecordNotFound => e
    Failbot.report(e, zuora_webhook_id: params[:zuora_webhook_id])
    flash[:error] = e
    redirect_back(fallback_location: "stafftools/zuora_webhooks")
  end
end
