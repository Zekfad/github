# frozen_string_literal: true

class Stafftools::Users::PendingTradeControlsRestrictionsController < StafftoolsController
  before_action :ensure_user_exists
  before_action :ensure_billing_enabled

  def extend
    unless this_user.has_any_pending_trade_restriction?
      flash[:error] = "There is no pending trade restriction enforcement for #{this_user}"
      return redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
    end

    if this_user.scheduled_pending_trade_restriction.extend
      flash[:notice] = "Successfully extended scheduled trade restriction enforcement for #{this_user} by 14 days."
    else
      flash[:error] = "Error while extending scheduled trade restriction enforcement: #{this_user.errors.full_messages.to_sentence}"
    end

    redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
  end

  def cancel
    unless this_user.has_any_pending_trade_restriction?
      flash[:error] = "There is no pending trade restriction enforcement for #{this_user}"
      return redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
    end

    if this_user.scheduled_pending_trade_restriction.cancel
      flash[:notice] = "Successfully cancelled scheduled trade restriction enforcement for #{this_user}."
    else
      flash[:error] = "Error while cancelling scheduled trade restriction enforcement: #{this_user.errors.full_messages.to_sentence}"
    end

    redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
  end
end
