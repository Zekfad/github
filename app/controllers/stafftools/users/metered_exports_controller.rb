# frozen_string_literal: true

class Stafftools::Users::MeteredExportsController < StafftoolsController
  areas_of_responsibility :gitcoin

  layout :nav_layout
  before_action :ensure_user_exists

  def index
    render "stafftools/users/metered_exports/index", locals: {
      metered_exports: this_user.metered_usage_exports.preload(:requester).order(created_at: :desc),
    }
  end

  def show
    export = this_user.metered_usage_exports.find(params[:id])

    redirect_to export.generate_expiring_url
  end

  private

  def nav_layout
    case this_user.site_admin_context
    when "organization"
      "stafftools/organization/billing"
    when "user"
      "stafftools/user/billing"
    end
  end
end
