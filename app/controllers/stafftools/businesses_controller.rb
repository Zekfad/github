# frozen_string_literal: true

module Stafftools
  class BusinessesController < Stafftools::Businesses::BusinessBaseController

    skip_before_action :business_required, only: %w(index new create check_slug)
    before_action :saml_provider_required, only: %w(remove_saml_provider)
    before_action :check_for_owners, only: %w(
      show people organizations members pending_members outside_collaborators
      pending_collaborators billing_managers pending_billing_managers admins
      pending_admins
    )
    before_action :hook_view, only: [:hook, :toggle_hook_active_status]

    BUSINESSES_PAGE_SIZE = 50
    ORGANIZATIONS_PAGE_SIZE = 30
    PEOPLE_PAGE_SIZE = 30
    ADD_OWNERS_PAGE_SIZE = 10
    ENTERPRISE_INSTALLATIONS_PAGE_SIZE = 30

    def index
      businesses = ::Business.by_slug
      render "stafftools/businesses/index", layout: "stafftools",
        locals: { businesses: businesses.paginate(page: current_page, per_page: BUSINESSES_PAGE_SIZE) }
    end

    def new
      render "stafftools/businesses/new", layout: "stafftools"
    end

    def check_slug
      slug = params[:value]
      return head 400 if slug.blank?

      if Business.find_by(slug: slug).present?
        # the slug already exists
        exists = true
        respond_to do |format|
          format.html_fragment do
            render partial: "stafftools/businesses/slug_message", formats: :html, status: 422, locals: {
              exists: true,
              slug: slug,
              not_alphanumeric: false,
            }
          end
        end
      elsif !slug.match?(Business::SLUG_REGEX)
        # the slug is invalid
        respond_to do |format|
          format.html_fragment do
            render partial: "stafftools/businesses/slug_message", formats: :html, status: 422, locals: {
              exists: false,
              slug: slug,
              not_alphanumeric: true,
            }
          end
        end
      else
        respond_to do |format|
          format.html_fragment do
            render partial: "stafftools/businesses/slug_message", formats: :html, locals: {
              exists: false,
              slug: slug,
              not_alphanumeric: false,
            }
          end
        end
      end
    end

    def create
      business_creator = Business::Creator.new(
        business_params: business_params.merge(
          owners: owners_from_params,
          customer_attributes: business_params.fetch(:customer_attributes, {}).merge(
            billing_type: "invoice")),
        require_owners: false)

      if business_creator.valid?
        business_creator.save!
        business = business_creator.business
        flash[:notice] = "Created #{business.name}."
        redirect_to complete_stafftools_enterprise_path(business)
      else
        flash[:error] = "Failed to save enterprise account. #{business_creator.error_message}."
        render "stafftools/businesses/new", layout: "stafftools", locals: { business_params: business_params }
      end
    end

    def complete
      render "stafftools/businesses/complete", locals: {
        owners: this_business.owners.paginate(page: current_page, per_page: ADD_OWNERS_PAGE_SIZE),
        pending_owner_invitations:
          this_business.pending_admin_invitations(role: [:owner])
            .paginate(page: current_page, per_page: ADD_OWNERS_PAGE_SIZE),
      }
    end

    def show
      render "stafftools/businesses/show"
    end

    def organizations
      render "stafftools/businesses/organizations", locals: {
        business: this_business,
        organizations: this_business.organizations.paginate(page: current_page, per_page: ORGANIZATIONS_PAGE_SIZE),
      }
    end

    def find_organizations
      organizations = ::Organization.search(params[:q], limit: 10)
      respond_to do |format|
        format.html_fragment do
          render partial: "stafftools/businesses/find_organizations_results", formats: :html, locals: {
            organizations: organizations, business: this_business
          }
        end
      end
    end

    def admin_suggestions
      headers["Cache-Control"] = "no-cache, no-store"

      respond_to do |format|
        format.html_fragment do
          render_partial_view "businesses/admins/suggestions",
                              ::Businesses::Admins::SuggestionsView,
                              business: this_business,
                              stafftools_invite: true,
                              query: params[:q]
        end
        format.html do
          render_partial_view "businesses/admins/suggestions",
                              ::Businesses::Admins::SuggestionsView,
                              business: this_business,
                              stafftools_invite: true,
                              query: params[:q]
        end
      end
    end

    def edit
      render "stafftools/businesses/edit", locals: { business: this_business }
    end

    def update
      if billing_email_valid? && this_business.update(business_params_with_billing_email(include_slug: false))
        flash[:notice] = "Updated #{this_business.name}."
        redirect_to stafftools_enterprise_path(this_business)
      else
        flash[:error] = "Failed to update enterprise account. #{this_business.errors.full_messages.to_sentence}."
        render "stafftools/businesses/edit", locals: {
          business: this_business, business_params: business_params
        }
      end
    end

    def add_organization
      org = organization_from_param
      unless org
        flash[:error] = "Organization #{params[:organization]} does not exist."
        return redirect_to organizations_stafftools_enterprise_path(this_business)
      end

      if this_business.organizations.include? org
        flash[:notice] = "Organization #{org.login} is already associated with #{this_business.name}."
        return redirect_to organizations_stafftools_enterprise_path(this_business)
      end

      if this_business.two_factor_requirement_enabled? && !org.two_factor_requirement_enabled? && org.affiliated_users_with_two_factor_disabled_exist?
        if !params.has_key?(:verify)
          flash[:error] = "Adding #{org.name} to the enterprise account requires confirming the organization's name."
          return redirect_to organizations_stafftools_enterprise_path(this_business)
        elsif org.name.downcase != params[:verify].downcase
          flash[:error] = "You must type the name of the organization to confirm."
          return redirect_to organizations_stafftools_enterprise_path(this_business)
        elsif !org.can_two_factor_requirement_be_enabled?
          flash[:error] = "Adding the organization #{org.name} to #{this_business.name} would remove all admins."
          return redirect_to organizations_stafftools_enterprise_path(this_business)
        end
      end

      membership = this_business.add_organization(org)
      if membership.valid?
        flash[:notice] = "Added organization #{org.login} and queued background job to create enterprise user accounts for organization members."
      else
        flash[:error] = "Failed to add organization #{org.login}. #{membership.errors.full_messages.to_sentence}."
      end

      redirect_to organizations_stafftools_enterprise_path(this_business)
    end

    def remove_organization
      org = organization_from_param
      unless org
        flash[:error] = "Organization #{params[:organization]} does not exist."
        return redirect_to organizations_stafftools_enterprise_path(this_business)
      end

      membership = this_business.organization_memberships.where organization: org
      unless membership.any?
        flash[:error] = "Organization #{params[:organization]} doesn't belong to #{this_business.name}."
        return redirect_to organizations_stafftools_enterprise_path(this_business)
      end

      this_business.remove_organization(org)
      flash[:notice] = "Removed organization #{org.login}."
      redirect_to organizations_stafftools_enterprise_path(this_business)
    end

    def transfer_organization
      org = organization_from_param

      unless org
        flash[:error] = "Organization #{params[:organization]} does not exist."
        return redirect_to organizations_stafftools_enterprise_path(this_business)
      end

      source_business = org.business

      unless source_business
        flash[:error] = "Organization #{params[:organization]} does not belong to an enterprise."
        return redirect_to organizations_stafftools_enterprise_path(this_business)
      end

      new_membership = source_business.transfer_organization(org, this_business)

      if new_membership.valid?
        flash[:notice] = "Transferred organization #{org.login} and queued background job to create enterprise user accounts for organization members."
      else
        flash[:error] = "Failed to transfer organization #{org.login}. #{new_membership.errors.full_messages.to_sentence}."
      end

      redirect_to organizations_stafftools_enterprise_path(this_business)
    end

    def people
      render "stafftools/businesses/people", locals: {
        members_count: this_business.filtered_members(current_user, ignore_org_membership_visibility: true).count,
        pending_members_count: this_business.unique_pending_member_invitation_count,
        outside_collaborators_count: this_business.filtered_outside_collaborators.count,
        pending_outside_collaborators_count: this_business.pending_collaborator_invitations.count,
        support_entitlees_count: this_business.support_entitlees.count,
        owners_count: this_business.owners.count,
        pending_owner_invitations_count: this_business.pending_admin_invitations(role: [:owner]).count,
        billing_managers_count: this_business.billing_managers.count,
        pending_billing_managers_invitations_count: this_business.pending_admin_invitations(role: [:billing_manager]).count,
      }
    end

    def members
      render "stafftools/businesses/members", locals: {
        members: this_business
          .filtered_members(current_user, ignore_org_membership_visibility: true)
          .paginate(page: current_page, per_page: PEOPLE_PAGE_SIZE),
      }
    end

    def pending_members
      render "stafftools/businesses/pending_members", locals: {
        pending_member_invitations: this_business
          .pending_member_invitations(order_by_field: "created_at", order_by_direction: "desc")
          .paginate(page: current_page, per_page: PEOPLE_PAGE_SIZE),
      }
    end

    def outside_collaborators
      render "stafftools/businesses/outside_collaborators", locals: {
        outside_collaborators: this_business
          .filtered_outside_collaborators.paginate(page: current_page, per_page: PEOPLE_PAGE_SIZE),
      }
    end

    def pending_collaborators
      render "stafftools/businesses/pending_collaborators", locals: {
        pending_collaborator_invitations: this_business
          .pending_collaborator_invitations
          .paginate(page: current_page, per_page: PEOPLE_PAGE_SIZE),
      }
    end

    def support_entitlees
      render "stafftools/businesses/support_entitlees", locals: {
        support_entitlees: this_business.support_entitlees
      }
    end

    def billing_managers
      render "stafftools/businesses/billing_managers", locals: {
        billing_managers: this_business.billing_managers.paginate(page: current_page, per_page: PEOPLE_PAGE_SIZE),
      }
    end

    def pending_billing_managers
      render "stafftools/businesses/pending_billing_managers", locals: {
        pending_billing_manager_invitations: this_business.pending_admin_invitations(role: [:billing_manager])
          .paginate(page: current_page, per_page: PEOPLE_PAGE_SIZE),
      }
    end

    def remove_billing_manager
      billing_manager = admin_from_param

      unless billing_manager
        flash[:error] = "User #{params[:admin]} does not exist."
        return redirect_to billing_managers_stafftools_enterprise_path(this_business)
      end

      this_business.billing.remove_manager(billing_manager, actor: current_user)
      flash[:notice] = "Removed billing manager #{billing_manager.login}."
      redirect_to billing_managers_stafftools_enterprise_path(this_business)
    end

    def admins
      render "stafftools/businesses/admins", locals: {
        owners: this_business.owners.paginate(page: current_page, per_page: PEOPLE_PAGE_SIZE),
      }
    end

    def pending_admins
      render "stafftools/businesses/pending_admins", locals: {
        pending_owner_invitations: this_business.pending_admin_invitations(role: [:owner])
          .paginate(page: current_page, per_page: PEOPLE_PAGE_SIZE),
      }
    end

    def add_owner
      if ::User.valid_email?(params[:admin])
        begin
          this_business.invite_admin(email: params[:admin], inviter: current_user,
                                     role: "owner", stafftools_invite: true)

          notice = <<~NOTICE
            You've invited #{params[:admin]} to become an
            owner of #{this_business.name}!
            They'll be receiving an email shortly.
          NOTICE
          redirect_to owners_redirect_path(default_redirect_path: pending_admins_stafftools_enterprise_path(this_business)), notice: notice
        rescue BusinessMemberInvitation::AlreadyAcceptedError, BusinessMemberInvitation::InvalidError, ActiveRecord::RecordInvalid => error
          flash[:error] = error.message
          redirect_to owners_redirect_path(default_redirect_path: pending_admins_stafftools_enterprise_path(this_business))
        end
      else
        owner = admin_from_param
        unless owner
          flash[:error] = "User #{params[:admin]} does not exist."
          return redirect_to owners_redirect_path
        end

        unless owner.try(:user?)
          flash[:error] = "Owner must be a user."
          return redirect_to owners_redirect_path
        end

        if this_business.owners.include? owner
          flash[:notice] = "User #{owner.login} is already an owner of #{this_business.name}."
          return redirect_to owners_redirect_path
        end

        this_business.add_owner(owner, actor: current_user, send_email_notification: true)
        flash[:notice] = "Added owner #{owner.login} and notified them by email."
        redirect_to owners_redirect_path
      end
    end

    def cancel_admin_invitation
      invitation = this_business.invitations.pending.with_business_role(:owner)
        .find_by!(id: params[:invitation_id])
      invitation.cancel actor: current_user

      flash[:notice] = \
        "You've canceled #{invitation.email_or_invitee_name}'s invitation to become #{invitation.role_for_message} of #{invitation.business.name}."
      redirect_to pending_admins_stafftools_enterprise_path(this_business)
    end

    def remove_owner
      owner = admin_from_param

      unless owner
        flash[:error] = "User #{params[:admin]} does not exist."
        return redirect_to owners_redirect_path
      end

      begin
        this_business.remove_owner(owner, actor: current_user)
        flash[:notice] = "Removed owner #{owner.login}."
      rescue ::Business::NoAdminsError
        flash[:error] = "You cannot remove the last owner of this enterprise account."
      end
      redirect_to owners_redirect_path
    end

    def rename_slug
      if this_business.rename_slug params[:new_slug], actor: current_user
        flash[:notice] = "Renamed enterprise account URL slug to '#{params[:new_slug]}'."
      else
        this_business.restore_attributes
        flash[:error] = "Failed to rename URL slug. #{this_business.errors.full_messages.to_sentence}."
      end
      redirect_to stafftools_enterprise_path(this_business)
    end

    def destroy
      if this_business.destroy
        flash[:notice] = "Deleted #{this_business.name}."
        redirect_to stafftools_enterprises_path
      else
        render "stafftools/businesses/edit", locals: { business: this_business }
      end
    end

    def hooks
      # Need to load the last status for hooks to list
      hooks = Hook::StatusLoader.load_statuses \
        hook_records: this_business.hooks.ordered.all, parent: this_business
      render "stafftools/businesses/hooks", locals: { business: this_business, hooks: hooks }
    end

    def hook
      render "stafftools/hooks/show", locals: { hook_deliveries_query: params[:deliveries_q] }
    end

    def toggle_hook_active_status
      this_hook.toggle!(:active)
      flash[:notice] = "Okay, the webhook was successfully #{hook_view.hook_active_status}."
      redirect_to :back
    end

    def security
      render "stafftools/businesses/security"
    end

    def remove_saml_provider
      if params[:reason].blank?
        flash[:error] = "You must provide a reason for the log."
      else
        this_business.saml_provider.destroy
        flash[:notice] = "SAML SSO was disabled."
        instrument "staff.delete_business_saml_provider", \
          business: this_business, note: params[:reason]
      end
      redirect_to security_stafftools_enterprise_path(this_business)
    end

    def disable_ip_whitelisting
      if params[:reason].blank?
        flash[:error] = "You must provide a reason for disabling the IP allow list."
      else
        this_business.disable_ip_whitelisting actor: current_user, reason: params[:reason]
        flash[:notice] = "Disabled IP allow list."
      end
      redirect_to security_stafftools_enterprise_path(this_business)
    end

    def enterprise_installations
      installations = this_business
        .enterprise_installations
        .order("enterprise_installations.host_name asc")
        .paginate(page: current_page, per_page: ENTERPRISE_INSTALLATIONS_PAGE_SIZE)
      render "stafftools/businesses/enterprise_installations", locals: {
        enterprise_installations: installations,
      }
    end

    private

    def saml_provider_required
      render_404 unless this_business.saml_provider.present?
    end

    def hook_view
      @hook_view = Hooks::ShowView.new hook: this_hook
    end

    def this_hook
      @hook = this_business.hooks.find(params[:id])
    end

    def business_params
      params.require(:business).permit(
        :owners,
        :billing_email,
        :name,
        :slug,
        :can_self_serve,
        :staff_owned,
        :terms_of_service_type,
        :terms_of_service_notes,
        :terms_of_service_company_name,
        :seats,
        :enterprise_web_business_id,
        :reseller_customer,
        :advanced_security_enabled,
        customer_attributes: [
          :billing_end_date,
        ],
      )
    end

    def owners_from_params
      return [] if params[:business][:owners].blank?
      ::User.where login: params[:business][:owners].strip.split
    end

    def admin_from_param
      ::User.find_by_login params[:admin]
    end

    def organization_from_param
      ::Organization.find_by_login params[:organization]
    end

    def billing_email_from_params
      @billing_email ||= UserEmail.find_by(email: business_params.dig(:customer_attributes, :billing_email))
    end

    def billing_email_valid?
      billing_email = business_params.dig(:customer_attributes, :billing_email)
      return true unless billing_email.present?
      return true if billing_email_from_params.present?
      this_business.errors.add(:base, :invalid_billing_email, message: "Billing email must be associated with a user")
      false
    end

    def business_params_with_billing_email(include_slug: true)
      params = if business_params.dig(:customer_attributes, :billing_email).present?
        business_params.tap { |params| params[:customer_attributes][:billing_email] = billing_email_from_params }
      elsif business_params.dig(:customer_attributes, :billing_email) == ""
        business_params.tap { |params| params[:customer_attributes][:billing_email] = nil }
      else
        business_params
      end

      params = params.except(:slug) unless include_slug

      params
    end

    def owners_redirect_path(default_redirect_path: admins_stafftools_enterprise_path(this_business))
      return complete_stafftools_enterprise_path(this_business) if params[:complete_page] == "1"
      default_redirect_path
    end

    def check_for_owners
      if this_business.owners.empty? && this_business.invitations.pending.with_business_role(:owner).empty?
        flash[:error] = "This Enterprise account does not have any owners. Please add or invite an owner for this enterprise."
      end
    end

    # Required as a helper method by the shared hooks views.
    # See app/views/stafftools/hooks/show.html.erb.
    def current_context
      this_business
    end
    helper_method :current_context
  end
end
