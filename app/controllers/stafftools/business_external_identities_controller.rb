# frozen_string_literal: true

module Stafftools
  class BusinessExternalIdentitiesController < Stafftools::BusinessesController

    before_action :ensure_saml_enabled

    PAGE_SIZE = 100

    def linked_saml_members
      render "stafftools/business_external_identities/linked_saml_members",
        layout: "stafftools/business",
        locals: { members: this_business.linked_saml_members.paginate(page: current_page, per_page: PAGE_SIZE) }
    end

    def unlinked_saml_members
      render "stafftools/business_external_identities/unlinked_saml_members",
        layout: "stafftools/business",
        locals: { members: this_business.unlinked_saml_members.paginate(page: current_page, per_page: PAGE_SIZE) }
    end

    def linked_saml_orgs
      render "stafftools/business_external_identities/linked_saml_orgs",
        layout: "stafftools/business",
        locals: { orgs: this_business.linked_saml_orgs.paginate(page: current_page, per_page: PAGE_SIZE) }
    end

    def show
      user = ::User.find_by!(login: params[:id])
      external_identity = \
        ExternalIdentity.find_by(user: user, provider: this_business.saml_provider)

      render "stafftools/business_external_identities/show",
        layout: "stafftools/business", locals: {
          user: user,
          business: this_business,
          external_identity: external_identity,
        }
    end

    def destroy
      user = ::User.find_by(login: params[:id])
      return render_404 unless user
      return render_404 unless ExternalIdentity.linked?(provider: this_business.saml_provider,
                                                        user: user)

      payload = {
        user: user,
      }.merge(GitHub.guarded_audit_log_staff_actor_entry(current_user))

      ExternalIdentity.unlink(
        provider: this_business.saml_provider,
        user: user,
        perform_instrumentation: true,
        instrumentation_payload: payload,
      )

      flash[:notice] = "External identity for #{user} successfully unlinked."
      redirect_to :back
    end

    private

    def ensure_saml_enabled
      render_404 unless this_business.saml_sso_enabled?
    end
  end
end
