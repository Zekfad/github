# frozen_string_literal: true

class Stafftools::IssueSubscriptionsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_repo_exists
  before_action :ensure_issue_exists

  layout "stafftools/repository/collaboration"

  def index
    @watchers = Stafftools::Newsies.subscriptions_for_issue(this_issue)
    @ignorers = Stafftools::Newsies.users_ignoring_issue(this_issue)
    @repo_ignorers =
      Stafftools::Newsies.users_ignoring_repository(current_repository)

    render "stafftools/issue_subscriptions/index"
  end

end
