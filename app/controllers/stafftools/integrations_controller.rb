# frozen_string_literal: true

class Stafftools::IntegrationsController < StafftoolsController
  areas_of_responsibility :platform, :stafftools

  before_action :ensure_user_exists

  layout :new_nav_layout

  def index
    @integrations = @user.integrations.
      order("integrations.name asc").paginate(page: params[:page])

    render "stafftools/integrations/index"
  end

  def show
    render_template_view "stafftools/integrations/show", show_view
  end

  def rename
    integration.name = params[:name]
    if integration.save
      flash[:notice] = "Name successfully updated."
    else
      flash[:error] = integration.errors.full_messages.join(". ")
    end
    redirect_to stafftools_user_app_path(user_id: @user.login, id: integration.slug)
  end

  def toggle_hook_active_status
    integration.hook.toggle!(:active)
    flash[:notice] = "Okay, the webhook was successfully #{show_view.hook_active_status}."
    redirect_to :back
  end

  def authorizations
    authorized_apps = @user.oauth_authorizations.joins(:integration).order("integrations.name asc").paginate(page: params[:page])
    render "stafftools/applications/user", locals: { application_type: "integration", authorized_apps: authorized_apps }
  end

  def update_creation_limit
    this_user.set_custom_applications_limit(Integration.new, params[:creation_limit])

    flash[:notice] = "Creation limit updated successfully."
    redirect_to stafftools_user_apps_path(user_id: this_user.login)
  end

  private

  def show_view
    @show_view = Stafftools::Integrations::ShowView.new integration: integration, hook_deliveries_query: params[:deliveries_q]
  end

  def integration
    @integration ||= @user.integrations.find_by_slug!(params[:id])
  end

  def new_nav_layout
    case this_user.site_admin_context
    when "organization"
      "stafftools/organization/security"
    when "user"
      "stafftools/user/security"
    end
  end
end
