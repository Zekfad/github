# frozen_string_literal: true

module Stafftools
  class RemindersBetaSignupController < StafftoolsController
    areas_of_responsibility :ce_extensibility

    def index
      memberships = EarlyAccessMembership.
        preload(:member, :actor).
        order("created_at ASC").
        reminders_waitlist.
        paginate(page: current_page, per_page: 100)

      target_ids = memberships.map(&:member_id)

      render_template_view(
        "stafftools/reminders_beta_signup/index",
        Stafftools::RemindersBetaSignup::IndexView,
        memberships: memberships,
        slack_installations: installations_by_target(Apps::Internal.integration(:slack), target_ids: target_ids),
        pull_panda_installations: installations_by_target(Apps::Internal.integration(:pull_panda), target_ids: target_ids),
      )
    end

    def toggle_access
      organization = ::Organization.find(params[:user_id])

      if organization.reminders_enabled?
        organization.disable_reminders
      else
        organization.enable_reminders
      end

      head :ok
    end

    private

    def installations_by_target(integration, target_ids:)
      installations = IntegrationInstallation.
        preload(integration: :latest_version, target: []).
        where(integration_id: integration.id, target_id: target_ids)

      installations.to_h do |installation|
        [installation.target, installation]
      end
    end
  end
end
