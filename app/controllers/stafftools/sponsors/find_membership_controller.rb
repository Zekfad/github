# frozen_string_literal: true

class Stafftools::Sponsors::FindMembershipController < Stafftools::SponsorsController
  skip_before_action :membership_required
  skip_before_action :listing_required

  def show
    if membership = SponsorsMembership.find_by(id: params[:id])
      redirect_to stafftools_sponsors_member_path(membership.sponsorable)
    else
      flash[:error] = "Couldn't find that membership."
      redirect_to stafftools_sponsors_members_path
    end
  end
end
