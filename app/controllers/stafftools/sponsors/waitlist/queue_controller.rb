# frozen_string_literal: true

class Stafftools::Sponsors::Waitlist::QueueController < StafftoolsController
  IndexQuery = parse_query <<-'GRAPHQL'
    query(
      $filter: SponsorsMembershipFilters,
      $first: Int,
      $after: String,
      $last: Int,
      $before: String,
    ) {
      biztoolsInfo {
        sponsorsMemberships(
          filterBy: $filter,
          first: $first,
          after: $after,
          last: $last,
          before: $before
        ) {
          nodes {
            sponsorable {
              ... on Actor {
                login
              }
            }
          }
          ...Views::Stafftools::Sponsors::Waitlist::Queue::Index::Memberships
        }
      }
    }
  GRAPHQL

  def index
    variables = {
      filter: {
        states: ["SUBMITTED"],
        spammy: false,
        suspended: false,
      },
    }.merge(graphql_pagination_params(page_size: 1))

    data = platform_execute(IndexQuery, variables: variables)

    sponsorable_login = data.biztools_info.sponsors_memberships.nodes.first&.sponsorable&.login
    sponsorable = ::User.find_by(login: sponsorable_login)

    if sponsorable.present?
      membership = sponsorable.sponsors_membership
      criteria = membership.sponsors_memberships_criteria.includes(:sponsors_criterion)
      automated_criteria, manual_criteria = criteria.partition do |criterion|
        criterion.sponsors_criterion.automated?
      end
    end

    render "stafftools/sponsors/waitlist/queue/index", layout: "application", locals: {
      sponsorable: sponsorable,
      graphql_memberships: data.biztools_info.sponsors_memberships,
      membership: membership,
      manual_criteria: manual_criteria,
      automated_criteria: automated_criteria,
    }
  end
end
