# frozen_string_literal: true

class Stafftools::Sponsors::Waitlist::AutoAcceptanceReviewController < StafftoolsController
  before_action :sponsors_required

  PER_PAGE = 100

  def index
    memberships = SponsorsMembership
      .joins(:sponsors_memberships_criteria)
      .where(
        state: 0,
        billing_country: SponsorsMembership::AUTO_ACCEPTABLE_COUNTRIES,
        country_of_residence: SponsorsMembership::AUTO_ACCEPTABLE_COUNTRIES,
        ignored_at: nil,
        sponsors_memberships_criteria: {
          sponsors_criterion_id: automated_criteria_ids,
          met: 0,
        },
      )
      .for_automated_criterion(
        slug: ::SponsorsCriterion::Automated::SpammyStatus.slug,
        met:  true,
      )
      .for_automated_criterion(
        slug: ::SponsorsCriterion::Automated::SuspensionStatus.slug,
        met:  true,
      )
      .for_automated_criterion(
        slug: ::SponsorsCriterion::Automated::OFACCompliance.slug,
        met:  true,
      )
      .distinct
      .paginate(page: current_page, per_page: PER_PAGE)
      .order("sponsors_memberships.created_at ASC")

    automated_criteria = SponsorsMembershipsCriterion.joins(:sponsors_criterion).where(
      sponsors_membership_id: memberships.pluck(:id),
      sponsors_criteria: { automated: true, active: true },
    ).group_by(&:sponsors_membership_id)

    render "stafftools/sponsors/waitlist/auto_acceptance_review/index", layout: "application", locals: {
      memberships: memberships,
      automated_criteria: automated_criteria,
    }
  end

  private

  def automated_criteria_ids
    @automated_criteria_ids ||= SponsorsCriterion.automated.where(active: true).pluck(:id)
  end
end
