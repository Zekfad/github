# frozen_string_literal: true

class Stafftools::Sponsors::Members::ReputableOrgsPartialsController < Stafftools::SponsorsController
  skip_before_action :listing_required

  REPUTABLE_ORGS_PER_PAGE = 10

  def show
    render partial: "stafftools/sponsors/members/criteria/member_reputable_org/organizations",
      locals: { membership: this_membership }
  end
end
