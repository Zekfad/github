# frozen_string_literal: true

class Stafftools::Sponsors::Members::BansController < Stafftools::SponsorsController
  skip_before_action :listing_required

  def create
    this_membership.ban!(banned_by: current_user, banned_reason: params[:banned_reason])

    flash[:notice] = "Successfully banned #{this_sponsorable.login} from the Sponsors membership program"

    redirect_to stafftools_sponsors_member_path(this_membership.sponsorable)
  end

  def destroy
    this_membership.un_ban!(un_banned_by: current_user)

    flash[:notice] = "Successfully un-banned #{this_sponsorable.login} from the Sponsors membership program"

    redirect_to stafftools_sponsors_member_path(this_membership.sponsorable)
  end
end
