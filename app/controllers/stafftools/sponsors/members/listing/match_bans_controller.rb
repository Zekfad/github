# frozen_string_literal: true

class Stafftools::Sponsors::Members::Listing::MatchBansController < Stafftools::SponsorsController
  def create
    if sponsor = User.find_by_login(params[:sponsor_login])
      match_ban = this_sponsorable.sponsorship_match_bans_as_sponsorable.build(sponsor: sponsor)

      if match_ban.save
        flash[:notice] = "Disabled matching sponsorships for #{this_sponsorable} from #{sponsor}."
      else
        flash[:error] = match_ban.errors.full_messages.to_sentence
      end
    else
      flash[:error] = "Could not find account with login #{params[:sponsor_login]}."
    end

    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  def destroy
    match_ban = this_sponsorable.sponsorship_match_bans_as_sponsorable.find(params[:id])
    match_ban.destroy!
    flash[:notice] = "Enabled matching sponsorships for #{this_sponsorable} from #{match_ban.sponsor}."
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end
end
