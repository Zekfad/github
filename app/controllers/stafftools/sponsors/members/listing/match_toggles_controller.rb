# frozen_string_literal: true

class Stafftools::Sponsors::Members::Listing::MatchTogglesController < Stafftools::SponsorsController
  CreateMutation = parse_query <<-'GRAPHQL'
    mutation($input: EnableSponsorsMatchInput!) {
      enableSponsorsMatch(input: $input) {
        sponsorsListing {
          databaseId
          sponsorable {
            ... on Actor {
              login
            }
          }
        }
      }
    }
  GRAPHQL

  def create
    input = { sponsorsListingId: this_listing.global_relay_id }
    data = platform_execute(CreateMutation, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:enableSponsorsMatch]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to stafftools_sponsors_member_path(this_sponsorable)
    end

    listing = data.enable_sponsors_match.sponsors_listing
    sponsorable = listing.sponsorable

    flash[:notice] = "Enabled match for #{sponsorable.login}."
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  DestroyMutation = parse_query <<-'GRAPHQL'
    mutation($input: DisableSponsorsMatchInput!) {
      disableSponsorsMatch(input: $input) {
        sponsorsListing {
          databaseId
          sponsorable {
            ... on Actor {
              login
            }
          }
        }
      }
    }
  GRAPHQL

  def destroy
    input = { sponsorsListingId: this_listing.global_relay_id, reason: params[:disable_reason] }
    data = platform_execute(DestroyMutation, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:disableSponsorsMatch]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to stafftools_sponsors_member_path(this_sponsorable)
    end

    listing = data.disable_sponsors_match.sponsors_listing
    sponsorable = listing.sponsorable

    flash[:notice] = "Disabled match for #{sponsorable.login}."
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end
end
