# frozen_string_literal: true

class Stafftools::Sponsors::Members::Listing::PayoutTogglesController < Stafftools::SponsorsController
  before_action :stripe_connect_account_required
  before_action :staff_note_required, only: [:destroy]

  def create
    this_sponsorable.enable_sponsors_payouts!
    flash[:notice] = "Enabled payouts for #{this_sponsorable.login}."
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  rescue Stripe::APIConnectionError, Stripe::StripeError => error
    flash[:error] = "Failed to enable payouts: #{error.message}"
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  def destroy
    ActiveRecord::Base.transaction do
      this_sponsorable.disable_sponsors_payouts!
      this_membership.staff_notes.create!(
        note: params[:disable_reason],
        user: current_user
      )
    end
    flash[:notice] = "Disabled payouts for #{this_sponsorable.login}."
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  rescue Stripe::APIConnectionError, Stripe::StripeError => error
    flash[:error] = "Failed to disable payouts: #{error.message}"
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  private

  def staff_note_required
    if params[:disable_reason].blank?
      flash[:error] = "Please enter a reason for disabling payouts"
      redirect_to stafftools_sponsors_member_path(this_sponsorable)
    end
  end
end
