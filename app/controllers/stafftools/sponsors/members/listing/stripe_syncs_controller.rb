# frozen_string_literal: true

class Stafftools::Sponsors::Members::Listing::StripeSyncsController < Stafftools::SponsorsController
  def create
    details = Stripe::Account.retrieve(this_listing.stripe_connect_account.stripe_account_id)
    this_listing.stripe_connect_account.update(stripe_account_details: details.as_json)
    flash[:notice] = "Stripe details synced for #{this_sponsorable.login}"
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  rescue Stripe::APIConnectionError, Stripe::StripeError => e
    flash[:error] = "Failed to sync Stripe details: #{e.message}"
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end
end
