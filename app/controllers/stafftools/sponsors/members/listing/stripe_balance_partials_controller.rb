# frozen_string_literal: true

class Stafftools::Sponsors::Members::Listing::StripeBalancePartialsController < Stafftools::SponsorsController
  def show
    stripe_account = this_listing.stripe_connect_account
    return render_404 unless stripe_account.present?

    response = stripe_account.current_balance
    render partial: "stafftools/sponsors/members/stripe_balance", locals: {
      response: response,
    }
  end
end
