# frozen_string_literal: true

class Stafftools::Sponsors::Members::TransactionsExportsController < StafftoolsController
  def create
    report = SponsorsListing::TransactionsExport.new(**export_params.to_h.symbolize_keys)
    send_data(report.as_csv, type: "text/csv", filename: report.filename)
  end

  private

  def export_params
    params.require(:export).permit(:fiscal_host, :timeframe)
  end
end
