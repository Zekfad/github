# frozen_string_literal: true

class Stafftools::Sponsors::Members::ReceiptsController < Stafftools::SponsorsController
  def create
    receipt = SponsorsListing::Receipt.new \
      sponsorable: this_sponsorable,
      **receipt_params.to_h.symbolize_keys

    send_data receipt.as_pdf,
      filename:    receipt.pdf_filename,
      type:        "application/pdf"

  rescue ArgumentError => e
    flash[:error] = e.message
    redirect_back fallback_location: stafftools_sponsors_member_transfers_path(this_sponsorable.login)
  end

  private

  def receipt_params
    params.require(:receipt).permit(:start_date, :end_date)
  end
end
