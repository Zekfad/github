# frozen_string_literal: true

class Stafftools::Sponsors::Members::CriterionController < Stafftools::SponsorsController
  skip_before_action :listing_required
  before_action :criterion_required

  UpdateMutation = parse_query <<-'GRAPHQL'
    mutation($input: UpdateSponsorsMembershipsCriterionInput!) {
      updateSponsorsMembershipsCriterion(input: $input) {
        criterion {
          id
          isMet
        }
      }
    }
  GRAPHQL

  def update
    input = {
      sponsorsMembershipsCriterionId: this_criterion.global_relay_id,
      isMet: params[:is_met] == "1" ? true : false,
    }

    result = platform_execute(UpdateMutation, variables: { input: input })

    if result.errors.any?
      error_type = result.errors.details[:updateSponsorsMembershipsCriterion]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN].include?(error_type)

      error = result.errors.messages.values.join(", ")
      return render json: { error: error }, status: :unprocessable_entity
    end

    record = result.update_sponsors_memberships_criterion.criterion

    render json: {
      sponsorsMembershipsCriterionId: record.id,
      isMet: record.is_met?,
    }, status: :ok
  end

  private

  def this_criterion
    this_membership.sponsors_memberships_criteria.find_by(id: params[:id])
  end

  def criterion_required
    render_404 unless this_criterion.present?
  end
end
