# frozen_string_literal: true

class Stafftools::Sponsors::Members::AcceptancesController < Stafftools::SponsorsController
  skip_before_action :listing_required

  CreateMutation = parse_query <<-'GRAPHQL'
    mutation($input: AcceptSponsorsMembershipInput!) {
      acceptSponsorsMembership(input: $input) {
        sponsorsMembership {
          sponsorable {
            ... on Actor {
              login
            }
          }
        }
      }
    }
  GRAPHQL

  def create
    input = { sponsorsMembershipId: this_membership.global_relay_id }
    result = platform_execute(CreateMutation, variables: { input: input })

    if result.errors.any?
      flash[:error] = result.errors.messages.values.flatten.to_sentence
    else
      user = result.accept_sponsors_membership.sponsors_membership.sponsorable
      email_time_in_minutes = (SponsorsMembership::EMAIL_WAIT_TIME.to_i / 60)
      flash[:notice] = "Successfully accepted Sponsors membership for #{user.login}, scheduling an acceptance " \
                       "email to be sent to the user in #{email_time_in_minutes} minutes. If you undo acceptance " \
                       "before then, it will cancel the scheduled email."
    end

    if params[:queue_cursor].present?
      redirect_to stafftools_sponsors_waitlist_queue_index_path(
        after: params[:queue_cursor],
      )
    else
      redirect_to stafftools_sponsors_member_path(this_sponsorable)
    end
  end

  DestroyMutation = parse_query <<-'GRAPHQL'
    mutation($input: RevertSponsorsMembershipToSubmittedInput!) {
      revertSponsorsMembershipToSubmitted(input: $input) {
        sponsorsMembership {
          sponsorable {
            ... on Actor {
              login
            }
          }
        }
        errors {
          message
        }
      }
    }
  GRAPHQL

  def destroy
    input = { sponsorsMembershipId: this_membership.global_relay_id }
    result = platform_execute(DestroyMutation, variables: { input: input })

    if result.revert_sponsors_membership_to_submitted.errors.any?
      flash[:error] = result.revert_sponsors_membership_to_submitted.errors.map(&:message).to_sentence
    else
      user = result.revert_sponsors_membership_to_submitted.sponsors_membership.sponsorable
      flash[:notice] = "Successfully reverted Sponsors membership for #{user.login} to submitted."
    end

    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end
end
