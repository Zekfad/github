# frozen_string_literal: true

class Stafftools::Sponsors::Members::StaffNotesController < Stafftools::SponsorsController
  skip_before_action :listing_required

  def create
    this_membership.staff_notes.create!(
      staff_note_params.merge({ user: current_user }),
    )

    flash[:notice] = "Okay, added staff note."
    redirect_to :back
  end

  private

  def staff_note_params
    params.require(:staff_note).permit(:note)
  end
end
