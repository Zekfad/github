# frozen_string_literal: true

class Stafftools::Sponsors::Members::FeaturesController < Stafftools::SponsorsController
  def update
    update_featured_attributes

    featured_msg = params[:is_featured] ? "featured" : "unfeatured"
    flash[:notice] = "Successfully #{featured_msg} Sponsors membership for #{this_sponsorable.login}."

    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  rescue ActiveRecord::RecordInvalid => e
    flash[:error] = e.message
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  private

  def update_featured_attributes
    featured_state = params[:is_featured] ? :active : :allowed
    featured_description = params[:featured_description]

    ApplicationRecord::Collab.transaction do
      this_membership.update!(featured_state: featured_state, featured_description: featured_description)
      this_listing.update!(short_description: featured_description)
    end
  end
end
