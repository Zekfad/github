# frozen_string_literal: true

class Stafftools::OrganizationExternalIdentitiesController < StafftoolsController
  before_action :ensure_user_exists
  before_action :ensure_saml_enabled

  layout "stafftools/organization/security"

  def index
    render_template_view "stafftools/organization_external_identities/index", Stafftools::Organization::SamlSettingsView,
      organization: this_organization,
      filter: params[:scope],
      page: params[:page]
  end

  def show
    user = User.find_by_login(params[:id])
    return render_404 unless user

    external_identity = this_organization.saml_provider.external_identities.
      linked_to(user).first

    render_template_view "stafftools/organization_external_identities/show", Stafftools::Organization::ExternalIdentityView,
      organization: this_organization,
      user: user,
      external_identity: external_identity
  end

  def destroy
    user = User.find_by_login(params[:id])
    return render_404 unless user
    return render_404 unless ExternalIdentity.linked?(provider: this_organization.saml_provider,
                                                      user: user)

    payload = {
      user: user,
    }.merge(GitHub.guarded_audit_log_staff_actor_entry(current_user))

    ExternalIdentity.unlink(
      provider: this_organization.saml_provider,
      user: user,
      perform_instrumentation: true,
      instrumentation_payload: payload,
    )

    flash[:notice] = "External identity for #{user} successfully unlinked."
    redirect_to :back
  end

  private

  def this_organization
    this_user
  end

  def ensure_saml_enabled
    render_404 unless this_organization.organization? && this_organization.saml_sso_enabled?
  end
end
