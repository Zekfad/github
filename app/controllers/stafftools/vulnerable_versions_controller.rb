# frozen_string_literal: true

module Stafftools
  class VulnerableVersionsController < StafftoolsController
    areas_of_responsibility :security_advisories

    def new
      render "stafftools/vulnerable_versions/new", locals: {
        vulnerability:      vulnerability,
        vulnerable_version: VulnerableVersionRange.new,
      }
    end

    def create
      version_range = vulnerability.vulnerable_version_ranges.new(range_params)

      if version_range.save
        redirect_to stafftools_vulnerability_url(vulnerability.id), {
          notice: "Vulnerable version successfully added",
        }
      else
        render "stafftools/vulnerable_versions/new", locals: {
          vulnerability:      vulnerability,
          vulnerable_version: version_range,
        }
      end
    end

    def process_alerts
      vulnerable_version_range.process_alerts(
        actor: current_user,
        skip_email: (params[:skip_email] == "true"),
      )

      redirect_to stafftools_vulnerability_url(vulnerable_version_range.vulnerability), {
        notice: "Vulnerable alerts job now processing",
      }
    end

    def destroy
      vulnerable_version_range.destroy

      redirect_to stafftools_vulnerability_url(vulnerable_version_range.vulnerability), {
        notice: "Vulnerable version successfully deleted",
      }
    end

    def preview_alert
      view = create_view_model(
        RepositoryAlerts::StafftoolsPreviewView,
        vulnerable_version_range: vulnerable_version_range,
      )
      render "stafftools/vulnerable_versions/preview", locals: { view: view }
    end

    AffectedRepoCountQuery = parse_query <<-'GRAPHQL'
      query($databaseId: Int!) {
        stafftoolsInfo {
          range: vulnerableVersionRange(databaseId: $databaseId) {
            estimatedAffectedRepositoryCount
          }
        }
      }
    GRAPHQL

    def dependent_count
      render html: ActiveSupport::NumberHelper.number_to_delimited(cached_dependent_count)
    end

    def alert_progress
      render Stafftools::Vulnerabilities::ProgressComponent.new(
        value: vulnerable_version_range.total_alerts_processed,
        max_value: cached_dependent_count,
        aria_labelledby: "vulnerable-version-ranges-#{params[:id]}-alerts",
      )
    end

    private

    def vulnerability
      @vulnerability ||= Vulnerability.find(params[:vulnerability_id])
    end

    def vulnerable_version_range
      @vulnerable_version_range ||= vulnerability
        .vulnerable_version_ranges
        .find(params[:id])
    end

    def cached_dependent_count
      cache_key = [
        VulnerableVersionRange,
        vulnerable_version_range.id,
        vulnerable_version_range.updated_at.to_i,
        "affected_repo_count",
      ].join(":")

      GitHub.cache.fetch(cache_key, ttl: 24.hours) do
        data = platform_execute(AffectedRepoCountQuery, variables: {
          databaseId: params[:id].to_i,
        })

        data.stafftools_info.range.estimated_affected_repository_count
      end
    end

    def range_params
      params.require(:vulnerable_version_range).permit(:affects, :ecosystem, :requirements, :fixed_in)
    end
  end
end
