# frozen_string_literal: true

class Stafftools::AzureMeteredBillingExtractsController < StafftoolsController
  areas_of_responsibility :gitcoin

  def index
    render_template_view "stafftools/azure_metered_billing_extracts/index", Stafftools::AzureMeteredBillingExtractsView
  end
end
