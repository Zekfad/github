# frozen_string_literal: true

class Stafftools::RefundsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_billing_enabled

  def create
    transaction = ::Billing::BillingTransaction.find_by(transaction_id: params[:transaction_id])
    if transaction
      ::Billing::ReverseTransactionJob.perform_later(transaction)
      flash[:notice] = "Transaction reversal has been queued up and will process in the background."
    else
      flash[:error] = "Transaction not found."
    end

    redirect_to return_path
  end

  private
  # Internal
  def return_path
    if this_user.present?
      :back
    else
      stafftools_billing_transaction_path(transaction_id: params[:transaction_id])
    end
  end
end
