# frozen_string_literal: true

class Stafftools::ProtectedBranchesController < StafftoolsController
  areas_of_responsibility :code_collab, :stafftools

  before_action :ensure_repo_exists

  layout "stafftools/repository/security"

  def index
    branches = current_repository.protected_branches
    render "stafftools/protected_branches/index", locals: { branches: branches }
  end
end
