# frozen_string_literal: true

class Stafftools::SpamController < StafftoolsController
  before_action :ensure_spam_enabled
  before_action :ensure_user_exists

  # Sets the spammy flag on a user
  def create
    if params[:reason].blank?
      flash[:error] = "Please specify a reason for flagging this user as spam."
      redirect_to :back
      return
    end

    flag_account(this_user, reason: params[:reason], paid_confirm: params[:paid_confirm])
    redirect_to :back
  end

  # Removes the spammy flag from a user
  def destroy
    this_user.mark_not_spammy(actor: current_user, origin: :stafftools)
    redirect_to :back
  end

  # Flag all other free users on the same IP as spam
  def flag_all
    User.transaction do
      User.by_ip(this_user.last_ip).each { |user| flag_account user }
    end

    redirect_to :back
  end

  # Whitelists the user from spam flagging
  def whitelist
    this_user.whitelist_from_spam_flag(actor: current_user, origin: :stafftools)
    redirect_to :back
  end

  def toggle_renaming_or_deleting
    Stafftools::SpammyRenameDeleteOverride.toggle_override(actor: current_user, user: this_user, toggle_type: params[:toggle_type].to_sym)
    flash[:notice] = "Spammy account #{params[:toggle_type]} abilities changed."
    redirect_to :back
  end

  def toggle_override_for_owned_orgs
    Stafftools::SpammyRenameDeleteOverride.toggle_override_for_owned_orgs(actor: current_user, user: this_user, toggle_type: params[:toggle_type].to_sym)
    flash[:notice] = "Spammy account #{params[:toggle_type]} abilities changed."
    redirect_to :back
  end

  private

  def ensure_spam_enabled
    render_404 unless GitHub.spamminess_check_enabled?
  end

  def flag_account(user, reason: nil, paid_confirm: false)
    user.spammy_reason = nil  # to get around the whitelist
    reason ||= "Flagged by staff"
    reason += " #{GitHub::SpamChecker::HARD_SPAM_FLAG_PHRASE}" if paid_confirm
    user.mark_as_spammy(reason: reason, actor: current_user, origin: :stafftools)
  end

end
