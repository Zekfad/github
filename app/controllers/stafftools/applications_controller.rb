# frozen_string_literal: true

class Stafftools::ApplicationsController < StafftoolsController
  areas_of_responsibility :stafftools, :api

  before_action :ensure_user_exists
  before_action :load_application

  DEFAULT_TEMP_RATE_LIMIT_DURATION = 3

  layout :new_nav_layout
  private def new_nav_layout
    case action_name
    when "show", "user", "developers", "github"
      case this_user.site_admin_context
      when "organization"
        "stafftools/organization/security"
      when "user"
        "stafftools/user/security"
      end
    else
      "stafftools"
    end
  end

  def developers
    @owned_apps = @user.oauth_applications.paginate(page: params[:page])
    render "stafftools/applications/developers"
  end

  def github
    @github_apps = @user.oauth_authorizations.github_owned.paginate(page: params[:page])
    render "stafftools/applications/github"
  end

  def user
    if @user.organization?
      @owned_apps = @user.oauth_applications.paginate(page: params[:page])
      render "stafftools/applications/developers"
    else
      authorized_apps = @user.oauth_authorizations.third_party.order("oauth_applications.name asc").paginate(page: params[:page])
      render "stafftools/applications/user", locals: { application_type: "oauth", authorized_apps: authorized_apps }
    end
  end

  def show
    render "stafftools/applications/show"
  end

  def update
    if params[:rate_limit].present?
      rate = params[:rate_limit].to_i
      if params[:temporary]
        duration = params[:duration]&.match?(/\A\d+\z/) && params[:duration] != "0" ? params[:duration].to_i : DEFAULT_TEMP_RATE_LIMIT_DURATION
        @app.set_temporary_rate_limit rate, duration.days
      else
        @app.rate_limit = rate
      end
      temporarily = " temporarily for #{duration} days" if params[:temporary]
      flash[:notice] = "Application rate limit set to #{rate}#{temporarily}"
    end

    @app.save!
    redirect_to :back
  end

  def enable
    @app.state = :active
    @app.save!
    flash[:notice] = "Application reactivated"
    redirect_to :back
  end

  def suspend
    @app.state = :suspended
    @app.save!
    flash[:notice] = "Application suspended"
    redirect_to :back
  end

  # Public: Disable full trust flag on an oauth application.
  #
  # Instead of blindly allowing an OAuthApplication to be
  # "full trust" we are utilizing Apps::Internal capabilities.
  def disable_full_trust
    @app.update(full_trust: false, scopes: [])
    redirect_to :back
  end

  def update_creation_limit
    this_user.set_custom_applications_limit(OauthApplication.new, params[:creation_limit])

    flash[:notice] = "Creation limit updated successfully."
    redirect_to stafftools_user_applications_path(user_id: this_user.login)
  end

  private

  def load_application
    @app = OauthApplication.find_by_id(params[:id])
  end

end
