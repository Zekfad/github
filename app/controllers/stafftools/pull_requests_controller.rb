# frozen_string_literal: true

class Stafftools::PullRequestsController < StafftoolsController
  areas_of_responsibility :code_collab, :pull_requests, :stafftools

  before_action :ensure_repo_exists
  before_action :ensure_pull_request_exists, except: [:purge, :reindex]

  layout "stafftools/repository/collaboration"

  def show
    fetch_audit_log_teaser "data.issue_id:#{this_pull.issue.id} OR (data.user_content_id:#{this_pull.issue.id} AND data.user_content_type:Issue AND action:user_content_edit.*)"
    render "stafftools/pull_requests/show"
  end

  def database
    render "stafftools/pull_requests/database"
  end

  # Updates the head and base SHA for a pull request to be current with the
  # underlying git repository. Sometimes useful when a pull request fails to
  # update due to post-receive failure.
  def sync
    this_pull.repository.network.increment_cache_version!

    if this_pull.catch_up
      flash[:notice] = "Pull Request synchronized"
    else
      flash[:error] = "No changes detected"
    end
    redirect_to :back
  end

  def destroy
    issue = this_pull.issue
    pull_num = this_pull.number

    this_pull.destroy_tracking_refs
    this_pull.destroy
    issue.reload
    issue.destroy

    instrument \
      "staff.delete_pull_request",
      user: current_repository.owner,
      repo: current_repository,
      note: "Deleted pull request #{current_repository.nwo}##{pull_num}"

    flash[:notice] = "Pull request ##{pull_num} deleted"
    redirect_to gh_stafftools_repository_issues_path(current_repository)
  end

  # Reindex the repo's pull requests for search
  def reindex
    purge = params[:purge] == "true" ? true : false
    current_repository.reindex_pull_requests(purge)
    flash[:notice] = "Reindexing #{current_repository.pull_requests.count} pull requests ..."
    redirect_to :back
  end

  def purge
    current_repository.purge_pull_requests
    flash[:notice] = "Purging pull requests from the search index ..."
    redirect_to :back
  end

  private

  def this_pull
    @this_pull ||= PullRequest.with_number_and_repo \
      params[:id].to_i,
      current_repository,
      include: [{issue: { comments: :user }}]
  end
  helper_method :this_pull

  def pull_request
    this_pull
  end
  helper_method :pull_request

  def ensure_pull_request_exists
    return render_404 if this_pull.nil?
  end

  def request_reflog_data(via)
    {
      real_ip: request.remote_ip,
      repo_name: current_repository.name_with_owner,
      repo_public: current_repository.public?,
      user_login: current_user.login,
      user_agent: request.user_agent,
      from: GitHub.context[:from],
      via: via,
    }
  end

  def counters
    {
      open: current_repository.pull_requests.joins(:issue).where(issues: {state: "open"}).count,
      closed: current_repository.pull_requests.joins(:issue).where(issues: {state: "closed"}).count,
    }
  end
end
