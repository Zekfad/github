# frozen_string_literal: true

class PagesAuthController < ApplicationController
  areas_of_responsibility :pages

  # Permissions
  before_action :access_check

  javascript_bundle :settings
  include PagesHelper

  def authenticate
    return render_404 unless params[:path].present?
    return render_404 unless params[:nonce].present?

    return render_404 unless @page.private?
    return render_404 unless current_repository.plan_supports?(:private_pages)

    uuid_regex = /\A[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\z/
    return render_404 unless uuid_regex.match?(params[:nonce].to_s.downcase)

    token = @page.auth_token(session: user_session)
    query_params = {
      path:     params[:path],
      page_id:  params[:page_id],
      nonce:    params[:nonce],
      token:    token
    }
    redirect_url = "#{GitHub.pages_auth_url}/redirect?#{query_params.to_param}"

    render "pages_auth/meta_refresh_redirect_private_page", locals: { redirect_url: redirect_url }, layout: "redirect"

    rescue Addressable::URI::InvalidURIError
      return render_404
  end

  private

  def current_repository
    return @current_repository if defined?(@current_repository)

    return render_404 unless params[:page_id].present?

    @page = Page.find_by id: params[:page_id]

    return render_404 if @page.nil?

    @current_repository = @page.repository
  end

  def target_for_conditional_access
    current_repository.owner
  end

  def access_check
    return render_404 unless @page.private? && current_repository.pullable_by?(current_user)
  end
end
