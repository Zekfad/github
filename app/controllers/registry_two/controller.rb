# frozen_string_literal: true

class RegistryTwo::Controller < ApplicationController

  LEGACY_ECOSYSTEMS = %w(
    docker
    maven
    npm
    nuget
    rubygems
  )

  SUPPORTED_ECOSYSTEMS = %w(
    container
  )

  protected

  DEFAULT_PACKAGE_VERSIONS_LIMIT = 10

  # For legacy packages, redirect to the repository-scoped package path.
  def redirect_legacy_packages
    GitHub::Logger.log(fn: "registry_two_controller.redirect_legacy_packages", ecosystem: params[:ecosystem])
    if LEGACY_ECOSYSTEMS.include?(params[:ecosystem])
      redirect_to package_path(owner, package.repository, package.id)
    end
  end

  def ensure_supported_ecosystem
    GitHub::Logger.log(fn: "registry_two_controller.ensure_supported_ecosystem", ecosystem: params[:ecosystem])
    render_404 unless SUPPORTED_ECOSYSTEMS.include?(params[:ecosystem])
  end

  def ensure_package
    render_404 unless package
  end

  def ensure_package_admin
    unless logged_in?
      return redirect_to :back
    end

    response = ::Permissions::Enforcer.authorize(
      action: :admin_package,
      actor: current_user,
      subject: package,
      context: {
        "subject.owner.id" => package.owner.id,
        "subject.author.id" => package.author.id,
       },
    )
    unless response.allow?
      flash[:error] = "You do not have permissions to administrate this package."
      return redirect_to :back
    end
  end

  def ensure_can_see_containers_ui
    GitHub::Logger.log(fn: "registry_two_controller.ensure_can_see_containers_ui", can_see: GitHub.flipper[:container_registry_ui].enabled?(owner))
    render_404 unless GitHub.flipper[:container_registry_ui].enabled?(owner)
  end

  def client
    @client ||= PackageRegistry::Twirp.metadata_client
  end

  def package
    @package ||= if params[:ecosystem] == "container"
      begin
        resp = client.get_package_metadata(
          namespace: owner.login,
          name: params[:name],
          ecosystem: params[:ecosystem],
          user_id: current_user&.id,
          version_limit: DEFAULT_PACKAGE_VERSIONS_LIMIT,
        )

        resp.try(:package)
      rescue PackageRegistry::Twirp::BaseError => e
        GitHub::Logger.log(fn: "registry_two_controller.get_metadata", error: e)
      end
    else
      owner.packages.find_by!(package_type: params[:ecosystem], name: params[:name])
    end
  end

  def owner
    @owner ||= User.find_by_login!(params[:owner_id])
  end

  def target_for_conditional_access
    owner = User.find_by_login(params[:owner_id])
    return :no_target_for_conditional_access unless owner
    owner
  end

  def get_metadata
    GitHub::Logger.log(
        fn: "registry_two_controller.get_metadata",
        ecosystem: params[:ecosystem],
        namespace: owner.login,
        name: params[:name],
        user_id: current_user&.id,
    )
    @metadata = client.get_package_metadata(ecosystem: params[:ecosystem], namespace: owner.name, name: params[:name], user_id: current_user&.id, version_limit: DEFAULT_PACKAGE_VERSIONS_LIMIT)
    GitHub::Logger.log(fn: "registry_two_controller.get_metadata", metadata: @metadata)
    render_404 unless @metadata
  rescue PackageRegistry::Twirp::BaseError => e
    GitHub::Logger.log(fn: "registry_two_controller.get_metadata", error: e)
    render_404
  end
end
