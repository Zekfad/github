# frozen_string_literal: true

class RegistryTwo::PackageSettingsController < RegistryTwo::Controller

  before_action :ensure_package
  before_action :ensure_package_admin
  before_action :ensure_container
  before_action :ensure_can_see_containers_ui
  before_action :load_actor_from_params, only: [:add_collaborator, :update_collaborator, :remove_collaborator]

  attr_reader :actor

  SUPPORTED_ECOSYSTEMS = %w(container)

  def show
    render "registry_two/package_settings/show", locals: {
      package: package,
      members_to_roles: members_to_roles,
    }
  end

  def add_collaborator
    # If owner is an organization, verify actor is org member.
    unless actor_can_be_granted_permissions?
      flash[:error] = "Can't grant permissions to users who cannot access this organization."
      return redirect_to :back
    end

    # Give all new collaborators the lowest permission level: read.
    Permissions::Granters::RoleGranter.new(
      actor: actor, target: package, role_name: "package_reader"
    ).grant!

    redirect_to :back
  end

  def update_collaborator
    # If owner is an organization, verify actor is org member.
    unless actor_can_be_granted_permissions?
      flash[:error] = "Can't grant permissions to users who cannot access this organization."
      return redirect_to :back
    end

    role_to_grant = params[:permission]
    unless Role::PACKAGES_SYSTEM_ROLES.include?(role_to_grant)
      flash[:error] = "Can't grant permissions"
      return redirect_to :back
    end

    # Revoke all old roles
    other_package_roles = Role::PACKAGES_SYSTEM_ROLES - [role_to_grant]
    other_package_roles.each do |role_name|
      Permissions::Granters::RoleGranter.new(
        actor: actor, target: package, role_name: role_name
      ).revoke_if_exists!
    end

    # Grant the new role
    result = Permissions::Granters::RoleGranter.new(
      actor: actor, target: package, role_name: role_to_grant
    ).grant!

    if result.success?
      head 200
    else
      head 403
    end
  end

  def bulk_update_collaborators
    role_to_grant = params[:role]
    unless Role::PACKAGES_SYSTEM_ROLES.include?(role_to_grant)
      flash[:error] = "Can't grant permissions"
      return redirect_to :back
    end

    selected_members = fetch_members(params[:selected_ids])
    other_package_roles = Role::PACKAGES_SYSTEM_ROLES - [role_to_grant]

    # TODO(dinahshi): make this more performant by bulk fetching UserRoles and
    # skipping if member already has role_to_grant. OR put into background job.
    results = selected_members.each_with_object([]) do |actor, results|
      other_package_roles.each do |role_name|
        Permissions::Granters::RoleGranter.new(
          actor: actor, target: package, role_name: role_name
        ).revoke_if_exists!
      end

      results << Permissions::Granters::RoleGranter.new(
        actor: actor, target: package, role_name: role_to_grant
      ).grant!.success?
    end

    if results.all?
      flash[:notice] = "Permissions updated for selected members."
    else
      flash[:notice] = "Something went wrong while bulk updating permissions."
    end

    redirect_to :back
  end

  def remove_collaborator
    # Revoke all old roles
    Role::PACKAGES_SYSTEM_ROLES.each do |role_name|
      Permissions::Granters::RoleGranter.new(
        actor: actor, target: package, role_name: role_name
      ).revoke_if_exists!
    end

    respond_to do |wants|
      wants.html do
        if request.xhr?
          head :ok
        else
          username = actor.is_a?(User) ? actor.login : actor.name
          flash_message = "Removed #{username} as a package collaborator."
          redirect_to :back, flash: { notice: flash_message }
        end
      end
    end
  end

  def collaborator_suggestions
    respond_to do |format|
      format.html_fragment do
        render partial: "registry_two/package_settings/collaborator_suggestions", formats: :html, locals: { suggestions: filtered_suggestions }
      end
      format.html do
        render partial: "registry_two/package_settings/collaborator_suggestions", locals: { suggestions: filtered_suggestions }
      end
    end
  end

  def toolbar_actions
    selected_count = params[:selected_ids]&.count || 0

    respond_to do |format|
      format.html do
        render partial: "registry_two/package_settings/access_management/members_toolbar_actions", locals: { selected_count: selected_count, selected_ids: params[:selected_ids] }
      end
    end
  end

  def change_visibility
    unless params[:name].casecmp?(params[:verify])
      flash[:error] = "You must type the name of the package to confirm."
      return redirect_to package_settings_path
    end

    begin
      client.update_package(
        ecosystem: package.package_type,
        namespace: package.namespace,
        name: package.name,
        is_public: true,
        user_id: current_user.id,
      )
    rescue PackageRegistry::Twirp::BaseError
      flash[:error] = "The package could not be updated. Please try again later."
    end

    redirect_to package_settings_path
  end

  def delete_package
    unless params[:name].casecmp?(params[:verify])
      flash[:error] = "You must type the name of the package to confirm."
      return redirect_to package_settings_path
    end

    begin
      client.delete_package(
          ecosystem: package.package_type,
          namespace: package.namespace,
          name: package.name,
          user_id: current_user.id,
          mode: :soft
          )
    rescue PackageRegistry::Twirp::BaseError
      flash[:error] = "The package could not be deleted. Please try again later."
      return redirect_to package_settings_path
    end

    redirect_to packages_two_path
  end

  private

  def ensure_container
    redirect_to package_two_path unless SUPPORTED_ECOSYSTEMS.include?(params[:ecosystem])
  end

  def load_actor_from_params
    @actor_type, @actor_id = params[:collaborator]&.split("/", 2)

    unless @actor_type && @actor_id
      flash[:error] = "Can't grant permissions to unknown collaborator"
      return redirect_to :back
    end

    if UserRole::VALID_ACTOR_TYPES.include?(@actor_type.capitalize)
      @actor = if @actor_type == "team"
        Team.find_by(id: @actor_id)
      else
        User.find_by(id: @actor_id)
      end
    end

    unless @actor
      flash[:error] = "Can't grant permissions"
      return redirect_to :back
    end
  end

  def fetch_members(member_strings)
    user_ids, team_ids = [], []
    member_strings.each do |member_string|
      member_type, member_id = member_string.split("/", 2)
      skip unless UserRole::VALID_ACTOR_TYPES.include?(member_type.capitalize)

      if member_type == "team"
        team_ids << member_id.to_i
      else
        user_ids << member_id.to_i
      end
    end

    # If this is an org owned package, ensure all users and teams are members
    # of the organization.
    if owner.is_a?(Organization)
      user_ids = user_ids & owner.members.pluck(:id)
      team_ids = team_ids & owner.teams.pluck(:id)
    end

    User.with_ids(user_ids) + Team.with_ids(team_ids)
  end

  def actor_can_be_granted_permissions?
    return true unless owner.is_a?(Organization)

    if actor.is_a?(User)
      owner.readable_by?(actor)
    else
      owner.visible_teams_for(current_user).pluck(:id).include?(actor.id)
    end
  end

  # Returns: { User/Team => Role}
  def members_to_roles
    return @members_to_roles if defined?(@members_to_roles)

    package_roles = Role.system_package_roles.to_a
    package_admin_role = package_roles.select { |r| r.name == "package_admin" }

    @members_to_roles = UserRole.where(role_id: package_roles.map(&:id))
      .where(target_type: "Package", target_id: package.id)
      .includes(:actor)
      .map { |ur| [ur.actor, ur.role] }
      .unshift([package.author, package_admin_role])
      .to_h
  end

  def filtered_suggestions
    autocompleteQuery = if owner.is_a?(Organization)
      AutocompleteQuery.new(current_user, params[:q],
        organization: owner,
        include_teams: true,
        org_members_only: true,
      )
    else
      AutocompleteQuery.new(current_user, params[:q])
    end

    autocompleteQuery.suggestions - members_to_roles.keys
  end
end
