# frozen_string_literal: true

class GitContentController < AbstractRepositoryController
  before_action :ask_the_gitkeeper
  before_action :try_to_expand_path,  only: [:show, :raw]

  def expand
    redirect_to "/#{owner}/#{current_repository}/#{params[:controller]}/#{default_tree}", status: 301
  end

  rescue_from GitRPC::InvalidObject, GitRPC::NoSuchPath do |e|
    render_404(e)
  end

  rescue_from GitRPC::ConnectionError do |e|
    render_offline
  end

  def show
    raise NotImplementedError
  end

  def raw
    raise NotImplementedError
  end

  private

  helper_method :tree_commit_key

  def tree_commit_key
    parts = [
      AvatarHelper::CACHE_VERSION,
      "v13",
      "commit",
      current_repository.name_with_owner,
      commit_sha, path_string
    ]
    git_content_cache_key :tree, parts
  end

  # Generates a cache key for Blob and Tree view content.  This lets you break
  # both of them at the same time if desired.
  def git_content_cache_key(prefix, pieces)
    "#{prefix}:v22:#{pieces.join(":").to_md5}"
  end

  # There are many reasons we may not be able to show a repository's code.
  # This filter goes through all possible scenarios and renders the
  # appropriate template if we cannot show the code.
  #
  # nonexist      - The content straight up doesn't exist. Or has been deleted.
  # empty         - The repo is empty and should be. Show the Next Step page.
  # importing     - The repo is empty and an import is in progress and we know
  #                 where Porter is. Redirect to the import progress page.
  # locked        - The repo is locked due to a long-running operation (rename).
  # network_move  - The repo is locked in preparation for extraction into a new
  #                 network.
  # down          - The repo looks like it's been pushed to, but doesn't have git.
  # offline       - The repo is empty, but it shouldn't be. Something is amiss.
  # forking       - The repo is still trying to fork.
  # mirroring     - The repo is still trying to mirror.
  # nothing       - The current_user doesn't exist or can't push to the repo and
  #                 there's nothing to show them (one of the above states).
  # blank         - There's no git data, the user needs to push it up.
  #
  # You can fake each of these states with params[:fakestate] equal to the
  # words listed above.
  #
  # Please see AbstractRepositoryController#ask_the_gatekeeper! It checks for
  # similar ideas, but for the entire Repository (code or not) and runs before
  # this one.
  #
  # Returns nothing.
  def ask_the_gitkeeper
    repo = current_repository
    state = params[:fakestate] if real_user_site_admin?

    repo_move_locked    = repo.moving? || state == "move_locked"
    repo_billing_locked = repo.locked_on_billing? || state == "billing_locked"
    repo_disk_locked    = repo.locked_on_disk? || state == "locked"
    repo_migrating      = repo.locked_on_migration? || state == "migrating"
    repo_empty          = (repository_specified? && repo.empty? && !repo.offline?) || state == "empty"
    repo_cloning        = repository_specified? && !repo.offline? && repo.cloning_from_template?
    repo_importing      = (repo_empty && GitHub.porter_available? && repo.porter_importing?) || state == "importing"
    repo_down           = (repo.empty? && repo.offline? && !repo.pushed_at.blank?) || state == "down"
    repo_offline        = (repo.empty? && repo.offline?) || state == "offline"
    repo_forking        = (repo.creating? && repo.parent_id?) || state == "forking"
    repo_mirroring      = (repo.creating? && repo.mirror) || state == "mirroring"
    repo_nothing        = (repo.empty? && !repo.pushable_by?(current_user)) || state == "nothing"
    repo_nobranch       = (!repo.empty? && repo.heads.empty?) || state == "nobranch"

    content_nonexist    = (!repo.empty? && !current_commit && bad_default_branch_ref? && !repair_bad_default_branch_ref)

    if repo_move_locked
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:move-locked"])
      render "repositories/states/network_move"
    elsif repo_billing_locked
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:billing-locked"])
      render "repositories/states/billing_locked"
    elsif repo_disk_locked
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:locked"])
      render "repositories/states/locked"
    elsif repo_migrating
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:migrating"])
      render "repositories/states/migrating", layout: "repository_disabled", locals: { repo: repo }
    elsif repo_nothing
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:nothing"])
      render "repositories/states/nothing"
    elsif repo_importing && !working_with_new_blob?
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:importing"])
      redirect_to repository_import_path(repository: repo, user_id: repo.owner.login)
    elsif repo_empty && viewing_graphs?
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:empty"])
      render "graphs/blank_slate"
    elsif repo_cloning
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:cloning"])
      render "repositories/states/cloning"
    elsif repo_empty && !working_with_new_blob?
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:empty"])
      render "repositories/states/empty"
    elsif repo_down
      render_offline("down")
    elsif repo_offline
      render_offline
    elsif repo_forking
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:forking"])
      render "repositories/states/forking"
    elsif repo_mirroring
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:mirroring"])
      render "repositories/states/mirroring"
    elsif repo_nobranch
      GitHub.dogstats.increment("repo", tags: ["action:ask_the_gitkeeper", "state:nobranch"])
      render "repositories/states/nobranch"
    elsif content_nonexist
      render_404
    end
  rescue ::GitRPC::InvalidRepository
    render_offline("down")
  rescue ::GitRPC::RepositoryOffline, GitHub::DGit::UnroutedError, GitHub::DGit::InsufficientQuorumError => e
    GitHub::Logger.log_exception({method: "GitContentController.ask_the_gitkeeper", spec: repo.dgit_spec}, e)
    render_offline
  end

  def working_with_new_blob?
    params[:controller] == "blob" && %w(new create preview).include?(params[:action])
  end

  def viewing_graphs?
    params[:controller] == "graphs"
  end

  # Find out if a bad default branch was specified. This will only be the case
  # if these 3 conditions are true:
  #
  # 1. The default branch was specified in the request
  # 2. The repository has at least one branch
  # 3. The repository does not have a branch with the name that was specified
  #
  # Returns true if a bad default branch was specified, false otherwise.
  def bad_default_branch_ref?
    (tree_name == current_repository.default_branch) &&
    !current_repository.heads.empty? &&
    !current_repository.heads.include?(tree_name)
  end

  # If the repository's default branch doesn't exist, elect a new default branch
  # now instead of 404'ing. This is a weird thing to happen, since a new default
  # branch should be chosen when the old one is deleted. It sometimes happens
  # though, likely due to failed jobs or other error conditions.
  #
  # This should live in the model.
  #
  # Returns truthy if a new branch was selected and a commit is available, false
  # otherwise.
  def repair_bad_default_branch_ref
    return unless bad_default_branch_ref?

    default_branch =
      if current_repository.heads.include?("master")
        "master"
      else
        current_repository.heads.first.name
      end
    current_repository.update_default_branch default_branch
    @tree_name = default_branch
    @current_commit = current_repository.commit_for_ref(default_branch)
    @commit_sha = @current_commit.oid if @current_commit
  end

  def try_to_expand_path
    if params[:controller] == "tree" && params[:action] == "show"
      # https://github.com/defunkt/cijoe shows the default tree
      return if params[:name].blank?

      # https://github.com/defunkt/cijoe/tree/master => https://github.com/defunkt/cijoe
      if params[:name] == default_tree && params[:path].blank?
        return redirect_to "/#{owner}/#{current_repository}", status: 301
      end
    end

    return unless params[:name].blank? && params[:raw].blank?
    redirect_to "/#{owner}/#{current_repository}/#{params[:controller]}/#{default_tree}", status: 301
  end

  def default_tree
    current_repository.default_branch rescue "master"
  end
end
