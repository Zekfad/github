# frozen_string_literal: true

class SuggestionsController < ApplicationController
  include OcticonsCachingHelper

  areas_of_responsibility :search

  statsd_tag_actions only: :show

  skip_before_action :perform_conditional_access_checks

  before_action :login_required
  before_action :enforce_required_parameters
  before_action :load_legacy_params
  before_action :repo_access_required
  before_action :require_xhr

  def show
    suggester = suggester_for(suggestion_subject)
    return render_404 unless suggester

    respond_to do |format|
      format.json do
        if params[:issue_suggester].present?
          suggestions = if show_discussions?
            suggester.issues_and_discussions
          else
            suggester.issues
          end

          output_suggestions = suggestions.map do |elem|
            type = if elem.respond_to?(:pull_request_id?)
              elem.pull_request_id? ? "pull_request" : "issue"
            else
              "discussion"
            end

            { id: elem.id, number: elem.number, title: elem.title, type: type }
          end

          icons = {
            pull_request: octicon("git-pull-request"),
            issue: octicon("issue-opened"),
            discussion: octicon("comment-discussion"),
          }

          render json: {
            suggestions: output_suggestions,
            icons: icons,
          }
        elsif params[:mention_suggester].present?
          render json: suggester.mentions
        else
          head 400
        end
      end
    end
  end

  private

  # Bridge new GraphQL Relay ID into old Suggester interface which accepts a
  # subject type and database id.
  def load_legacy_params
    return unless params[:global_id]
    return unless suggestion_subject

    # Ideally Suggester doesn't depend on a repository parameter altogether
    @current_repository = suggestion_subject.try(:repository)
    @owner = current_repository.owner if current_repository
    params[:user_id] ||= owner&.id
  end

  POSSIBLE_SUBJECT_TYPES = [
    Platform::Objects::CommitComment,
    Platform::Objects::Discussion,
    Platform::Objects::Issue,
    Platform::Objects::IssueComment,
    Platform::Objects::Organization,
    Platform::Objects::PullRequest,
    Platform::Objects::PullRequestReview,
    Platform::Objects::PullRequestReviewComment,
    Platform::Objects::Team,
    Platform::Objects::TeamDiscussion,
    Platform::Objects::TeamDiscussionComment,
  ].freeze

  def suggestion_subject
    return @suggestion_subject if defined?(@suggestion_subject)
    @suggestion_subject = params[:global_id] ?
      find_subject_by_global_id :
      find_subject_by_type
  end

  # Creates the suggester instance for the subject.
  #
  # NOTE: When adding to the list below, consider writing a new controller to
  # provide suggestions for your type instead. There isn't much reusable code
  # in this controller. The reuse is in the suggester model classes.
  #
  # Returns nil if no appropriate suggester can be created.
  def suggester_for(subject)
    case subject
    when CommitComment
      Suggester::RepositorySuggester.new(viewer: current_user, subject: subject.commit)
    when Commit, Issue, PullRequest, Repository, Discussion
      Suggester::RepositorySuggester.new(viewer: current_user, subject: subject)
    when PullRequestReview
      Suggester::RepositorySuggester.new(viewer: current_user, subject: subject.pull_request)
    when IssueComment
      if subject.issue.pull_request?
        Suggester::RepositorySuggester.new(viewer: current_user, subject: subject.issue.pull_request)
      else
        Suggester::RepositorySuggester.new(viewer: current_user, subject: subject.issue)
      end
    when DiscussionPost, DiscussionPostReply
      Suggester::TeamSuggester.new(viewer: current_user, team: subject.team)
    when Organization
      Suggester::OrganizationMemberSuggester.new(viewer: current_user, org: subject)
    when Team
      Suggester::TeamSuggester.new(viewer: current_user, team: subject)
    end
  end

  def find_subject_by_global_id
    typed_object_from_id(POSSIBLE_SUBJECT_TYPES, params[:global_id])
  rescue Platform::Errors::NotFound
    nil
  end

  # Finds the suggester's subject for the subject_type + subject_id pair. Both
  # parameters are optional so default to the current repository as a subject
  # if they are missing.
  #
  # Returns an Issue, PullRequest, Commit, Repository, Discussion or nil.
  def find_subject_by_type
    id = params[:subject_id]

    case params[:subject_type]
    when "issue"
      id ? current_repository.issues.find_by_id(id) : current_repository
    when "pull_request"
      id ? current_repository.pull_requests.find_by_id(id) : current_repository
    when "commit"
      begin
        current_repository.commits.find(id)
      rescue RepositoryObjectsCollection::InvalidObjectId
        nil
      end
    when "discussion"
      begin
        current_repository.discussions.find(id)
      rescue RepositoryObjectsCollection::InvalidObjectId
        nil
      end
    else
      current_repository
    end
  end

  def current_repository
    return @current_repository if defined?(@current_repository)
    @current_repository = (owner.find_repo_by_name(params[:repository]) if owner && params[:repository].present?)
  end

  # TODO Remove user_id parameter and owner method. Only a repository_id
  # parameter is needed since owner can be retrieved from it. Another option
  # is to change the route to /:owner/:repo/suggestions to make the data
  # dependency explict.
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def owner
    @owner ||= (User.find_by_login(params[:user_id]) if params[:user_id].present?)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def login_required
    render_404 unless logged_in?
  end

  # Documents the combinations of parameters we need. This can be removed if
  # we move the global_id code path to a separate controller.
  def enforce_required_parameters
    return if params[:global_id].present?
    return if params[:subject_type].present?
    return if params[:subject_type].blank? && params[:subject_id].blank?
    head 400
  end

  def repo_access_required
    return unless params[:user_id].present? # if owner is defined, we need to check for repo access
    return render_404 unless owner
    return render_404 unless current_repository
    return render_404 unless current_repository.public? || current_repository.pullable_by?(current_user)
  end
end
