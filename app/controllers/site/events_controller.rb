# frozen_string_literal: true

class Site::EventsController < ApplicationController
  areas_of_responsibility :events_feed

  statsd_tag_actions only: [:index, :show]

  skip_before_action :perform_conditional_access_checks, only: [:index, :show]
  before_action :add_csp_exceptions, only: [:index, :show]

  layout "site"

  CSP_EXCEPTIONS = { img_src: [ExploreFeed::Event::FEED_URL] }

  def index
    render "site/events/index"
  end

  def show
    if event.present?
      render "site/events/show", locals: { event: event }
    else
      render_404
    end
  end

  private

  def event
    @_event ||= ExploreFeed::Event.fetch_by_parameterized_name(params[:id])
  end
end
