# frozen_string_literal: true

class Site::CustomerStoriesController < ApplicationController
  areas_of_responsibility :events_feed

  skip_before_action :perform_conditional_access_checks, only: [:index, :show]
  before_action :add_csp_exceptions, only: [:index, :show]

  layout "site"

  CSP_EXCEPTIONS = { img_src: [ExploreFeed::CustomerStory::CUSTOMER_STORIES_FEED_URL] }
  VALID_STORY_TYPES = %w(all enterprise developers)

  def index
    if params[:type].present?
      render "site/customer_stories/index", locals: { permitted_params: permitted_params }
    else
      redirect_to customer_stories_path(type: :enterprise)
    end
  end

  def show
    if request_for_story_type?
      redirect_to customer_stories_path(type: params[:id].titleize.downcase)
    else
      if customer_story.present?
        render "site/customer_stories/show", locals: { customer_story: customer_story }
      else
        render_404
      end
    end
  end

  private

  def customer_story
    @_customer_story ||= ExploreFeed::CustomerStory.find_by_parameterized_name(params[:id])
  end

  def request_for_story_type?
    VALID_STORY_TYPES.include?(params[:id])
  end

  def permitted_params
    params
      .slice(:type, :industry, :region)
      .permit(:type, :industry, :region)
  end
end
