# frozen_string_literal: true

class Site::ReadmeController < ApplicationController
  areas_of_responsibility :marketing_site

  before_action :require_feature_flag

  def index
    render "site/readme/index"
  end

  def show
    maintainer_story = Site::MaintainerStory.find_by_parameterized_name(params[:id])

    if maintainer_story
      render "site/readme/show", locals: { maintainer_story: maintainer_story }
    else
      render_404
    end
  end

  private

  def require_feature_flag
    unless maintainer_stories_enabled?
      render_404
    end
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
