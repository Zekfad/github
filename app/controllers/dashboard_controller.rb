# frozen_string_literal: true

class DashboardController < ApplicationController
  areas_of_responsibility :news_feeds, :user_growth, :repository_recommendations

  statsd_tag_actions only: [:index, :recent_activity]

  include DashboardAnalyticsHelper
  include DashboardHelper
  include Site::CustomerStoriesFeedHelper

  before_action :check_email_verification, only: :index, if: :logged_in?
  before_action :clear_weak_password_session_variable, only: :index, unless: :logged_in?
  before_action :login_required, except: %w( index logged_out )
  before_action :add_csp_exceptions, only: [:index]

  skip_before_action :perform_conditional_access_checks

  CSP_EXCEPTIONS = {
    img_src: [
      ExploreFeed::CustomerStory::CUSTOMER_STORIES_FEED_URL,
      ExploreFeed::Spotlight::SPOTLIGHTS_FEED_URL,
    ],
  }
  RECOMMENDATIONS_LIMIT = 20
  REPOS_LIMIT = 7
  YOUR_TEAMS_LIMIT = 7
  SAVED_NOTIFICATIONS_LIMIT = 4

  DesktopIndexQuery = parse_query <<-'GRAPHQL'
    query(
      $reposLimit: Int!,
      $reposCursor: String,
      $yourTeamsLimit: Int!,
      $yourTeamsCursor: String,
      $since: DateTime,
      $discoverDashboardEnabled: Boolean!,
      $dashboardPinsLimit: Int!,
      $dashboardPinsEnabled: Boolean!,
      $syncReposLoading: Boolean!
    ) {
      enabledFeatures
      ...Views::Dashboard::Index::Root
    }
  GRAPHQL

  def index
    variables = {
      yourTeamsLimit: YOUR_TEAMS_LIMIT,
      reposLimit: REPOS_LIMIT,
      yourTeamsCursor: params[:your_teams_cursor],
      reposCursor: params[:your_repos_cursor],
      since: ranked_repositories_since,
      discoverDashboardEnabled: discover_repos_dashboard_enabled?,
      dashboardPinsLimit: UserDashboardPin::LIMIT_PER_USER_DASHBOARD,
      dashboardPinsEnabled: user_dashboard_pins_enabled?,
      syncReposLoading: !async_dashboard_repositories_enabled?,
    }

    locals = { query: params[:q], team_query: params[:team] }

    if logged_in?
      override_analytics_location "/dashboard"

      set_context
      respond_to do |format|
        format.html do
          data = platform_execute(DesktopIndexQuery, variables: variables)
          render "dashboard/index", layout: "dashboard_three_column", locals: locals.merge(
            data: data,
          )
        end
      end
    # SAML: If we redirect to login before there is a repository in
    # #enterprise_index, the idP may immediately authenticate the user again,
    # making it feel like no logout took place.
    elsif GitHub.enterprise?
      enterprise_index
    else
      # Homepage
      render "site/index", layout: "site"
    end
  end

  TopRepositoriesQuery = parse_query <<-'GRAPHQL'
    query($reposLimit: Int!, $reposCursor: String, $since: DateTime) {
      viewer {
        ...Views::Dashboard::RepositoriesBox::Viewer
      }
    }
  GRAPHQL

  def top_repositories
    data = platform_execute(TopRepositoriesQuery, variables: {
      reposCursor: nil,
      reposLimit: REPOS_LIMIT,
      since: ranked_repositories_since
    })

    render partial: "dashboard/top_repositories", locals: {
      query: params[:q],
      location: params[:location],
      viewer: data.viewer,
    }
  end

  RecentActivityQuery = parse_query <<-'GRAPHQL'
    query($recentlyViewedLimit: Int!, $organizationID: ID) {
      enabledFeatures
      viewer {
        ...Views::Dashboard::RecentActivity::Viewer
      }
    }
  GRAPHQL

  def recent_activity
    variables = { recentlyViewedLimit: 12, organizationID: params[:organization_id] }
    data = platform_execute(RecentActivityQuery, variables: variables)

    respond_to do |format|
      format.html do
        render partial: "dashboard/recent_activity", locals: { viewer: data.viewer }
      end
    end
  end

  def ajax_context_list
    if request.xhr?
      context = User.find_by_login(params[:current_context])
      respond_to do |format|
        format.html do
          render partial: "dashboard/context_list", locals: { current_context: context }
        end
      end
    else
      return render_404
    end
  end

  def logged_out
    if GitHub.enterprise?
      render "dashboard/logged_out", layout: authentication_layout
    else
      return render_404
    end
  end

  AjaxRepositoriesQuery = parse_query <<-'GRAPHQL'
    query($reposLimit: Int!, $reposCursor: String, $since: DateTime) {
      viewer {
        ...Views::Dashboard::Repositories::User
      }
    }
  GRAPHQL

  def ajax_repositories
    if request.xhr?
      data = platform_execute(AjaxRepositoriesQuery, variables: {
        reposLimit: 100, reposCursor: params[:repos_cursor], since: ranked_repositories_since
      })
      respond_to do |format|
        format.html do
          render partial: "dashboard/repositories", locals: {
            user: data.viewer,
            query: params[:q],
            location: params[:location],
          }
        end
      end
    else
      redirect_to user_path(current_user, params: {tab: "repositories"})
    end
  end

  AjaxYourTeamsQuery = parse_query <<-'GRAPHQL'
    query($yourTeamsLimit: Int!, $yourTeamsCursor: String) {
      enabledFeatures
      viewer {
        ...Views::Dashboard::Teams::Viewer
      }
    }
  GRAPHQL

  def ajax_your_teams
    if request.xhr?
      data = platform_execute(AjaxYourTeamsQuery, variables: {
        yourTeamsLimit: 100, yourTeamsCursor: params[:your_teams_cursor]
      })

      respond_to do |format|
        format.html do
          render partial: "dashboard/teams", locals: {
            viewer: data.viewer,
            location: params[:location],
          }
        end
      end
    else
      redirect_to dashboard_path
    end
  end

  def ignore_upgrade
    current_user.upgrade_ignore = current_user.plan.name
    current_user.save
    redirect_to action: "index"
  end

  def dismiss_bootcamp
    current_user.dismiss_notice("bootcamp")

    if request.xhr?
      head :ok
    else
      redirect_to home_path
    end
  end

  def current_context
    current_user
  end
  helper_method :current_context

private

  # Non logged in user hitting home page under Enterprise. When the admin user
  # has already been created and at least one public repository exists, redirect
  # to the /repositories page so people can browse around. If no repositories
  # exist yet, redirect to the login page - there's nothing interesting to see
  # in that case. SAML is the exception because the idP may immediately
  # authenticate the user again, making it feel like no logout took place.
  def enterprise_index
    if GitHub.auth.saml? || GitHub.auth.github_oauth? || GitHub.auth.cas?
      render "dashboard/logged_out", layout: "session_authentication", locals: { index_page: true }
    elsif Repository.public_scope.first && (!GitHub.private_mode || logged_in?)
      redirect_to "/repositories"
    else
      redirect_to "/login"
    end
  end

  def all_events
    @all_events ||= events_timeline.events(page: 1, per_page: Stratocaster::DEFAULT_INDEX_SIZE)
  end

  def events_timeline_key
    "user:#{current_user.id}"
  end

  def ranked_repositories_since
    4.months.ago.iso8601
  end

  def check_email_verification
    return unless GitHub.email_verification_enabled?
    return unless current_user.should_verify_email?

    redirect_to unverified_email_path
  end
end
