# frozen_string_literal: true

class MergeQueuesController < AbstractRepositoryController
  include MergeQueues::SharedControllerMethods

  before_action :merge_queue_required

  def show
    render "merge_queue/show", locals: {
      merge_queue: merge_queue,
      up_next_entries_for_display: up_next_entries_for_display,
      up_next_entries_has_next_page: up_next_entries_has_next_page,
      user_entries: current_user_entries,
      state: params[:state],
    }
  end

  private

  def merge_queue_required
    return render_404 unless GitHub.merge_queues_enabled?
    return render_404 unless GitHub.flipper[:merge_queue].enabled?(current_repository)
  end

  def current_user_entries
    MergeQueue.prefill_associations(
      entries: merge_queue.entries.where(enqueuer: current_user),
      repository: current_repository,
    )
  end

  def merge_queue
    current_repository.default_merge_queue
  end
end
