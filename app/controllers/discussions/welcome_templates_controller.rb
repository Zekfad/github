# frozen_string_literal: true

class Discussions::WelcomeTemplatesController < Discussions::BaseController
  before_action :login_required
  before_action :require_ability_to_open_discussion

  def create
    current_repository.has_discussions = true
    if current_repository.save
      redirect_to new_discussion_path(current_repository.owner, current_repository, welcome_text: true)
    else
      flash[:error] = "Could not enable discussions for #{current_repository.name_with_owner} at this time."
      redirect_to repository_path(current_repository)
    end
  end

  private

  def require_feature
    render_404 unless discussions_enabled?
  end
end
