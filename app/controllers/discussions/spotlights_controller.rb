# frozen_string_literal: true

class Discussions::SpotlightsController < Discussions::BaseController
  before_action :login_required
  before_action :require_discussion
  before_action :require_spotlights_feature
  before_action :require_spotlight_management
  before_action :require_spotlight, only: [:update, :destroy, :edit]
  layout false

  def new
    spotlight = DiscussionSpotlight.new(
      preconfigured_color: DiscussionSpotlight.preconfigured_color_names.sample,
      pattern: DiscussionSpotlight.pattern_names.sample,
      emoji: ":smile:",
      discussion: discussion
    )
    render "discussions/spotlights/new", locals: {
      discussion: discussion,
      spotlight: spotlight,
    }
  end

  def create
    spotlight = DiscussionSpotlight.new(spotlight_params)
    spotlight.spotlighted_by = current_user
    spotlight.discussion = discussion

    if spotlight.save
      redirect_to discussions_path
    else
      flash[:error] = "Could not save spotlight at this time: " \
        "#{spotlight.errors.full_messages.to_sentence}"
      redirect_to discussion_path(discussion)
    end
  end

  def edit
    render "discussions/spotlights/edit", locals: {
      discussion: discussion,
      spotlight: spotlight,
    }
  end

  def update
    if spotlight.update(spotlight_params)
      redirect_to discussions_path
    else
      flash[:error] = "Could not save spotlight at this time: " \
        "#{spotlight.errors.full_messages.to_sentence}"
      redirect_to discussion_path(discussion)
    end
  end

  def destroy
    if spotlight.destroy
      flash[:notice] = "Discussion \"#{discussion.title}\" is no longer a spotlight."
      redirect_to discussions_path
    else
      flash[:error] = "Could not remove spotlight at this time: " \
        "#{spotlight.errors.full_messages.to_sentence}"
      redirect_to discussion_path(discussion)
    end
  end

  private

  def spotlight
    return @spotlight if defined?(@spotlight)
    @spotlight = DiscussionSpotlight.for_repository(current_repository).
      for_discussion(discussion).find(params[:id])
  end

  def require_spotlight
    render_404 unless spotlight
  end

  def spotlight_params
    params.require(:discussion_spotlight).
      permit(:emoji, :position, :preconfigured_color, :custom_color, :pattern)
  end

  def require_spotlight_management
    render_404 unless current_user.can_manage_discussion_spotlights?(current_repository)
  end
end
