# frozen_string_literal: true

# Public: Handles rendering a dropdown menu that contains a list of the revisions for
# a Discussion or a DiscussionComment.
class Discussions::EditsController < Discussions::BaseController
  before_action :require_discussion
  before_action :require_discussion_comment_if_necessary
  before_action :require_edit_history_access

  def edit_history_log
    edits = discussion_or_comment.user_content_edits.order(id: :desc)
    includes_created_edit = discussion_or_comment.async_includes_created_edit?.sync
    edit_count = if includes_created_edit
      edits.count - 1
    else
      edits.count
    end

    respond_to do |format|
      format.html do
        render partial: "discussion_edits/edit_history_log", locals: {
          edits: edits,
          includes_created_edit: includes_created_edit,
          edit_count: edit_count,
        }
      end
    end
  end

  def edit_history
    render_context = DiscussionTimeline::SingleCommentRenderContext.new(
      discussion,
      discussion_comment,
      viewer: current_user,
    )

    timeline = DiscussionTimeline.new(render_context: render_context)

    respond_to do |format|
      format.html do
        render partial: "discussion_edits/edit_history", locals: {
          discussion_or_comment: discussion_or_comment, timeline: timeline
        }
      end
    end
  end

  private

  def discussion_comment
    return @discussion_comment if defined?(@discussion_comment)
    @discussion_comment = if params[:id]
      discussion.comments.find_by_id(params[:id].to_i)
    end
  end

  def discussion_or_comment
    discussion_comment || discussion
  end

  def require_discussion_comment_if_necessary
    return unless params[:id] && params[:discussion_number]
    render_404 unless discussion_comment
  end

  def require_edit_history_access
    render_404 unless discussion_or_comment.viewer_can_read_user_content_edits?(current_user)
  end
end
