# frozen_string_literal: true

class Discussions::SpotlightEmojiPickersController < Discussions::BaseController
  before_action :login_required
  before_action :require_discussion
  before_action :require_spotlights_feature
  layout false

  def show
    render "discussions/spotlight_emoji_pickers/show", locals: {
      emoji_name: params[:emoji_name],
      show_picker: params[:show_picker] == "1",
    }
  end
end
