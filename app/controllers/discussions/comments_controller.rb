# frozen_string_literal: true

class Discussions::CommentsController < Discussions::BaseController
  statsd_tag_actions only: [:create, :update, :destroy, :minimize, :unminimize]

  before_action :login_required
  before_action :require_discussion
  before_action :require_discussion_comment, only: [
    :update, :destroy, :minimize, :unminimize, :open_new_issue_modal,
    :minimize_form, :comment_header_reaction_button, :show
  ]
  before_action :require_minimize_state_is_toggleable, only: [:minimize, :unminimize]
  before_action :add_spamurai_form_signals, only: [:create, :update, :minimize, :unminimize]

  def comment_header_reaction_button
    respond_to do |format|
      format.html do
        render partial: "discussions/header_reaction_button",
               locals: { discussion_or_comment: discussion_comment, timeline: single_comment_discussion_timeline }
      end
    end
  end

  def create
    return render_404 unless discussion.can_comment?(current_user)

    timeline_last_modified_at = DiscussionTimeline.last_modified_at_for(discussion: discussion)
    comment = discussion.comments.new(comment_params)
    comment.user = current_user

    if comment.save
      if request.xhr?
        if comment.parent_comment.present?
          render json: {
            updateContent: {
              "#discussioncomment-#{comment.parent_comment_id} [data-child-comments]" =>
                render_to_string(
                  partial: "discussions/child_comments",
                  formats: [:html],
                  locals: {
                    parent_comment: comment.parent_comment,
                    timeline: single_comment_discussion_timeline(comment.parent_comment),
                  },
                ),
            },
          }
        else
          render json: {
            updateContent: {
              "[data-timeline-updated-at='#{timeline_last_modified_at}']" => render_to_string(
                partial: "discussions/timeline",
                formats: [:html],
                locals: {
                  timeline: new_comments_timeline(timeline_last_modified_at),
                },
              ),
            },
          }
        end
      else
        redirect_to discussion_path(discussion)
      end
    else
      if request.xhr?
        render json: comment.errors, status: :unprocessable_entity
      else
        flash[:error] = "Could not comment on the discussion at this time."
        redirect_to discussion_path(discussion)
      end
    end
  end

  def update
    return render_404 unless discussion_comment.modifiable_by?(current_user)

    if stale_model?(discussion_comment)
      return render_stale_error(model: discussion_comment,
        error: "Could not edit comment. Please try again.", path: discussion_path(discussion))
    end

    # Set the actor so we can log a Hydro event about this comment
    # being updated.
    discussion_comment.actor = current_user

    discussion_comment.assign_attributes(comment_params)
    return render_discussion_comment_update_error unless discussion_comment.valid?

    if update_comment
      if request.xhr?
        render json: {
          "source" => discussion_comment.body,
          "body" => discussion_comment.body_html,
          "newBodyVersion" => discussion_comment.body_version,
          "editUrl" => edits_menu_discussion_comment_path(current_repository.owner,
            current_repository, discussion, discussion_comment),
        }
      else
        redirect_to discussion_path(discussion)
      end
    else
      render_discussion_comment_update_error
    end
  end

  def destroy
    return render_404 unless discussion_comment.deletable_by?(current_user)

    if discussion_comment.wipe_or_destroy(current_user)
      if request.xhr?
        head :ok
      else
        redirect_to discussion_path(discussion)
      end
    else
      if request.xhr?
        render json: discussion_comment.errors.full_messages, status: :unprocessable_entity
      else
        flash[:error] = "Could not delete the discussion comment at this time."
        redirect_to discussion_path(discussion)
      end
    end
  end

  def minimize
    reason = ""
    staff = false

    # Set the actor so we can log a Hydro event about this comment
    # being updated.
    discussion_comment.actor = current_user

    if discussion_comment.set_minimized(current_user, reason, minimize_classifier, discussion_comment.author, staff)
      if request.xhr?
        render_single_comment(discussion_comment)
      else
        redirect_to discussion_path(discussion)
      end
    else
      error_message = "Could not hide the comment at this time."
      if request.xhr?
        render_single_comment(
          discussion_comment,
          error_message: error_message,
          status: :unprocessable_entity,
        )
      else
        flash[:error] = error_message
        redirect_to discussion_path(discussion)
      end
    end
  end

  def unminimize
    reason = ""
    staff = false

    # Set the actor so we can log a Hydro event about this comment
    # being updated.
    discussion_comment.actor = current_user

    if discussion_comment.set_unminimized(current_user, reason, discussion_comment.author, staff)
      if request.xhr?
        render_single_comment(discussion_comment)
      else
        redirect_to discussion_path(discussion)
      end
    else
      error_message = "Could not unhide the comment at this time."
      if request.xhr?
        render_single_comment(
          discussion_comment,
          error_message: error_message,
          status: :unprocessable_entity
        )
      else
        flash[:error] = error_message
        redirect_to discussion_path(discussion)
      end
    end
  end

  def open_new_issue_modal
    respond_to do |format|
      format.html do
        render partial: "discussions/open_new_issue_modal", locals: {
          comment: discussion_comment, timeline: single_comment_discussion_timeline
        }
      end
    end
  end

  def minimize_form
    respond_to do |format|
      format.html do
        render partial: "discussion_comments/minimize_form", locals: {
          comment: discussion_comment, timeline: single_comment_discussion_timeline
        }
      end
    end
  end

  def show
    timeline = single_comment_discussion_timeline

    respond_to do |format|
      format.html do
        render partial: "discussions/comment", locals: {
          timeline: timeline,
          comment: discussion_comment,
          form_path: discussion_comment_path(timeline.repo_owner, timeline.repo_name, timeline.discussion, discussion_comment),
          dom_id: helpers.comment_dom_id(discussion_comment),
        }
      end
    end
  end

  private

  def minimize_classifier
    # Convert the classifier reason to the correct format by using the same conversion
    # logic that the GraphQL layer uses.
    classifier = Platform::Enums::ReportedContentClassifiers.values[params[:classifier]].value

    return render_422 unless classifier

    classifier
  end

  def comment_params
    params.require(:comment).permit(:body, :parent_comment_id)
  end

  def discussion_comment
    @discussion_comment ||= discussion.comments.find_by_id(params[:id].to_i)
  end

  def render_discussion_comment_update_error
    if request.xhr?
      return render(json: discussion_comment.errors.full_messages, status: :unprocessable_entity)
    end

    flash[:error] = "Could not edit the discussion comment at this time."
    redirect_to discussion_path(discussion)
  end

  def require_discussion_comment
    render_404 unless discussion_comment
  end

  def single_comment_discussion_timeline(comment = nil)
    render_context = DiscussionTimeline::SingleCommentRenderContext.new(
      discussion,
      comment || discussion_comment,
      viewer: current_user,
    )

    DiscussionTimeline.new(render_context: render_context)
  end

  def new_comments_timeline(last_modified_at)
    render_context = DiscussionTimeline::LiveUpdatesRenderContext.new(
      discussion,
      viewer: current_user,
      timeline_last_rendered: last_modified_at,
    )

    DiscussionTimeline.new(render_context: render_context)
  end

  def require_minimize_state_is_toggleable
    has_permission = DiscussionComment.can_toggle_minimized_discussion_comment?(
      discussion, actor: current_user
    )
    render_404 unless has_permission
  end

  def update_comment
    if operation = TaskListOperation.from(params[:task_list_operation])
      text = operation.call(discussion_comment.body)
      discussion_comment.update_body(text, current_user)
    else
      discussion_comment.update_body(comment_params[:body], current_user)
    end
  end
end
