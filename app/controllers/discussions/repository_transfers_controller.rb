# frozen_string_literal: true

class Discussions::RepositoryTransfersController < Discussions::BaseController
  before_action :login_required
  before_action :require_discussion
  before_action :require_ability_to_transfer_discussion

  layout false

  def repositories
    repositories = discussion.async_possible_transfer_repositories(viewer: current_user,
      query: params[:query]).sync

    respond_to do |format|
      format.html_fragment do
        render "discussions/repository_transfers/repositories", formats: [:html], locals: {
          repositories: repositories
        }
      end
      format.html do
        render "discussions/repository_transfers/repositories", locals: {
          repositories: repositories
        }
      end
    end
  end

  def show
    render "discussions/repository_transfers/show", locals: {
      discussion: discussion
    }
  end

  def update
    new_repository = Repository.filter_spam_and_disabled_for(current_user).
      find(params[:repository_id])
    transferrer = DiscussionRepositoryTransferrer.for_new_transfer(discussion,
      new_repository: new_repository, actor: current_user)

    if transferrer.start_transfer
      TransferDiscussionJob.perform_later(transferrer.discussion_transfer)

      flash[:notice] = "Discussion transfer to #{new_repository.name_with_owner} is in progress."
      redirect_to discussion_path(transferrer.new_discussion, new_repository)
    else
      flash[:error] = "Could not transfer the discussion to " \
        "#{new_repository.name_with_owner} at this time."
      redirect_to discussion_path(discussion, current_repository)
    end
  end

  private

  def require_ability_to_transfer_discussion
    render_404 unless discussion.transferrable_by?(current_user)
  end
end
