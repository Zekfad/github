# frozen_string_literal: true

class Discussions::LeaderboardsController < Discussions::BaseController
  statsd_tag_actions only: [:show]

  before_action :require_leaderboard_feature

  def show
    leaderboard = DiscussionLeaderboard.new(repository: current_repository, viewer: current_user)
    render "discussions/leaderboards/show", layout: false, locals: { leaderboard: leaderboard }
  end

  private

  def require_leaderboard_feature
    render_404 unless discussions_leaderboard_enabled?
  end
end
