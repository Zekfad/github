# frozen_string_literal: true

class Discussions::BaseController < AbstractRepositoryController
  before_action :require_feature
  layout :repository_layout
  javascript_bundle :discussions

  private

  def require_feature
    render_404 unless show_discussions?
  end

  def require_discussion_categories_feature
    render_404 unless discussion_categories_enabled?
  end

  def require_spotlights_feature
    render_404 unless discussion_spotlights_enabled?
  end

  def require_discussion
    render_404 unless discussion && discussion.readable_by?(current_user)
  end

  def require_ability_to_open_discussion
    unless current_user.can_create_discussion?(current_repository)
      flash[:error] = "You can't perform that action at this time."
      redirect_to discussions_path(current_repository.owner, current_repository)
    end
  end

  def discussion
    @discussion ||= begin
      discussion_number = if params[:discussion_number].present?
        params[:discussion_number]
      else
        params[:number]
      end

      @discussion ||= Discussion.filter_spam_for(current_user).
        for_repository(current_repository).
        with_number(discussion_number).first
    end
  end

  def render_single_comment(comment, status: :ok, error_message: nil)
    render_context = DiscussionTimeline::SingleCommentRenderContext.new(
      discussion,
      comment,
      viewer: current_user,
    )
    timeline = DiscussionTimeline.new(render_context: render_context)

    render partial: "discussions/comment", status: status, locals: {
      dom_id: helpers.comment_dom_id(comment),
      timeline: timeline,
      comment: comment,
      form_path: discussion_comment_path(timeline.repo_owner, timeline.repo_name, timeline.discussion, comment),
      action_text: comment.answer? ? "answered" : "replied",
      error_message: error_message,
    }
  end
end
