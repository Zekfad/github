# frozen_string_literal: true

class Discussions::ChosenCommentsController < Discussions::BaseController
  before_action :login_required
  before_action :require_discussion
  before_action :require_discussion_is_question
  before_action :require_discussion_comment
  before_action :add_spamurai_form_signals, only: [:create, :destroy]

  def create
    return render_404 unless discussion_comment.can_mark_as_answer?(current_user)

    # Set the actor so we can create a DiscussionEvent for the answer
    # being marked.
    discussion_comment.actor = current_user

    if discussion_comment.mark_as_answer
      if request.xhr?
        discussion.reload
        render_single_comment(discussion_comment)
      else
        redirect_to discussion_path(discussion)
      end
    else
      if request.xhr?
        render json: discussion_comment.errors.full_messages, status: :unprocessable_entity
      else
        flash[:error] = "Could not mark as answer at this time."
        redirect_to discussion_path(discussion)
      end
    end
  end

  def destroy
    return render_404 unless discussion_comment.can_unmark_as_answer?(current_user)

    # Set the actor so we can create a DiscussionEvent for the answer
    # being unmarked.
    discussion_comment.actor = current_user

    if discussion_comment.unmark_as_answer
      if request.xhr?
        discussion.reload
        render_single_comment(discussion_comment)
      else
        redirect_to discussion_path(discussion)
      end
    else
      if request.xhr?
        render json: discussion_comment.errors.full_messages, status: :unprocessable_entity
      else
        flash[:error] = "Could not remove as answer at this time."
        redirect_to discussion_path(discussion)
      end
    end
  end

  private

  def require_discussion_is_question
    unless discussion.question?
      flash[:error] = "Cannot mark or unmark an answer for a discussion that is not a question."
      redirect_to discussion_path(discussion)
    end
  end

  def discussion_comment
    @discussion_comment ||= discussion.comments.find_by_id(params[:id].to_i)
  end

  def require_discussion_comment
    render_404 unless discussion_comment
  end
end
