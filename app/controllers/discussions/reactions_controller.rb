# frozen_string_literal: true

class Discussions::ReactionsController < Discussions::BaseController
  areas_of_responsibility :discussions

  before_action :require_discussion
  before_action :require_discussion_comment, if: -> { target.is_a?(DiscussionComment) }

  def show
    if request.xhr?
      render partial: "discussions/reactions", locals: {
        discussion_or_comment: target,
        timeline: timeline,
      }
    else
      render_404
    end
  end

  private

  def target
    if params[:target] == "discussion_comment"
      discussion_comment
    else
      discussion
    end
  end

  def timeline
    render_context = DiscussionTimeline::SingleCommentRenderContext.new(
      discussion,
      target,
      viewer: current_user,
    )

    DiscussionTimeline.new(render_context: render_context)
  end

  def discussion_comment
    @discussion_comment ||= discussion.comments.find_by_id(params[:id].to_i)
  end

  def require_discussion_comment
    render_404 unless discussion_comment
  end
end
