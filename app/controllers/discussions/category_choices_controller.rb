# frozen_string_literal: true

class Discussions::CategoryChoicesController < Discussions::BaseController
  before_action :login_required
  before_action :require_discussion_categories_feature
  before_action :require_ability_to_open_discussion

  layout :repository_layout

  def show
    if current_repository.discussion_categories.empty?
      redirect_to new_discussion_path
    else
      render "discussions/category_choices/show",
        locals: { categories: current_repository.discussion_categories.sort }
    end
  end
end
