# frozen_string_literal: true

class Discussions::CategoryMenusController < Discussions::BaseController
  before_action :login_required
  before_action :require_discussion_categories_feature
  before_action :require_discussion

  def show
    categories = current_repository.discussion_categories
    render "discussions/category_menus/show", layout: false, locals: {
      categories: categories,
      discussion: discussion,
    }
  end
end
