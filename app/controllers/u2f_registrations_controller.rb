# frozen_string_literal: true

class U2fRegistrationsController < ApplicationController

  # U2fRegistrationsController does not require conditional access checks
  # because it *doesn't* access protected organization resources.
  skip_before_action :perform_conditional_access_checks

  before_action :ensure_security_key_enabled, except: [:trusted_facets]
  before_action :login_required,              except: [:trusted_facets]
  before_action :sudo_filter,       except: [:trusted_facets]
  skip_after_action :block_non_xhr_json_responses, only: :trusted_facets

  def create
    register_response_hash = JSON.parse(params[:response])

    # Persist the registration
    # We determine whether to treat this as a webauthn-formatted registration
    # based on the presence of a key in the JSON.
    if register_response_hash.has_key?("response")
      attestation_object = Base64.urlsafe_decode64(register_response_hash["response"]["attestationObject"])

      # Create and verify the registration response object.
      registration = WebAuthn::AuthenticatorAttestationResponse.new(
        client_data_json: Base64.urlsafe_decode64(register_response_hash["response"]["clientDataJSON"]),
        attestation_object: attestation_object,
      )

      # Error string are shown to the user, so we use more general language
      # similar to our the client-side strings in `manage_two_factor.html.erb`.
      origin = Addressable::URI.new(scheme: request.scheme, host: request.host).to_s
      unless GitHub.webauthn_allowed_origin?(origin)
        # The server is running on an unexpected host. In this case, there is no point in telling the user to retry.
        render(status: 422, json: new_requests.merge(error: "Something went really wrong."))
        return
      end

      webauthn_register_challenge = session.delete(:webauthn_register_challenge)
      webauthn_user_handle = Base64.strict_decode64(session.delete(:webauthn_user_handle))

      handle_record = WebauthnUserHandle.find_by_user_id(current_user.id)
      if handle_record.nil?
        if !WebauthnUserHandle.new(user_id: current_user.id, webauthn_user_handle: webauthn_user_handle).save
          # Unable to persist the user handle (e.g. transient error or race condition).
          render(status: 422, json: new_requests.merge(error: "Something went wrong. Please try again."))
          return
        end
      elsif handle_record.webauthn_user_handle != webauthn_user_handle
        # We've had a race condition and used the wrong handle for this registration.
        render(status: 422, json: new_requests.merge(error: "Something went wrong. Please try again."))
        return
      end

      origin = Addressable::URI.new(scheme: request.scheme, host: request.host).to_s
      unless registration.valid?(Base64.urlsafe_decode64(webauthn_register_challenge), origin, rp_id: GitHub.webauthn_rp_id)
        # The client replied with an invalid registration attempt. This
        # shouldn't happen in normal browsers.
        render(status: 422, json: new_requests.merge(error: "This device cannot be registered."))
        return
      end

      # Create the registration
      u2f_registration = current_user.u2f_registrations.create(
        key_handle: Base64.urlsafe_encode64(registration.authenticator_data.credential.id, padding: false),
        public_key: Base64.strict_encode64(registration.authenticator_data.credential.public_key.to_str),
        counter: registration.authenticator_data.sign_count,
        nickname: params[:nickname].to_s,
        webauthn_attestation: attestation_object,
        is_webauthn_registration: true,
      )
    else
      # Create and verify the registration response object.
      response = U2F::RegisterResponse.load_from_json(params[:response].to_s)
      registration = GitHub.u2f.register!(u2f_register_challenges, response)

      # Create the registration
      u2f_registration = current_user.u2f_registrations.create(
        certificate: registration.certificate,
        key_handle: registration.key_handle,
        public_key: registration.public_key,
        counter: registration.counter,
        nickname: params[:nickname].to_s,
        is_webauthn_registration: false,
      )
    end

    if u2f_registration.new_record?
      error = u2f_registration.errors.full_messages.to_sentence
      render(status: 422, json: new_requests.merge(error: error))
    else
      AccountMailer.security_key_added(current_user).deliver_later
      render status: 201, json: new_requests.merge(
        registration: render_to_string(
          partial: "settings/user/u2f_registration",
          formats: [:html],
          locals: {registration: u2f_registration},
        ),
      )
    end
  rescue U2F::Error => e
    render(status: 422, json: new_requests.merge(error: e.message))
  end

  def destroy
    registration = current_user.u2f_registrations.find_by_id!(params[:id])
    registration.destroy
    if request.xhr?
      render json: new_requests
    else
      redirect_to :back
    end
  end

  def trusted_facets
    headers["Content-Type"] = "application/fido.trusted-apps+json"
    headers["Access-Control-Allow-Origin"] = "*"

    render(status: 200, json: {
      trustedFacets: [{
        version: {major: 1, minor: 0},
        ids: GitHub.u2f_trusted_facets,
      }],
    })
  end

  private
  # a Hash of new RegisterRequests and SignRequests to be sent back to client.
  #
  # Returns a Hash.
  def new_requests
    {
      register_requests: u2f_register_requests,
      sign_requests: u2f_sign_requests,
      webauthn_register_request: webauthn_register_request,
    }
  end

  # Check that the browser/server support U2F.
  #
  # Returns boolean.
  def ensure_security_key_enabled
    security_key_enabled_for_request?
  end
end
