# frozen_string_literal: true

class Email::OptinsController < ApplicationController
  areas_of_responsibility :email

  skip_before_action :perform_conditional_access_checks, only: [:show, :create]
  before_action :login_required, except: [:show]

  layout "site"

  REQUIRED_SCOPE = "EmailOptin:marketing-emails"

  def show
    if user = user_from_email_token
      ActiveRecord::Base.connected_to(role: :writing) do
        NewsletterPreference.set_to_marketing user: user, signup: false, source: "opt-in"
      end
      render "email/optins/create"
    elsif logged_in?
      render "email/optins/show"
    else
      redirect_to_login email_optin_path
    end
  end

  def create
    NewsletterPreference.set_to_marketing user: current_user, signup: false, source: "opt-in"
    render "email/optins/create"
  end

  private

  def stateless_request?
    user_from_email_token || super
  end

  def user_from_email_token
    return if action_name != "show"
    User.authenticate_with_signed_auth_token token: params[:token],
                                             scope: REQUIRED_SCOPE
  end
end
