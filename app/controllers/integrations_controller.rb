# frozen_string_literal: true

class IntegrationsController < ApplicationController
  areas_of_responsibility :platform

  def show
    if current_integration.readable_by?(current_user)
      render_template_view "integrations/show", Integrations::SelectTargetView,
        integration: current_integration,
        page: params[:page] || 1
    else
      render_template_view "integrations/show_restricted", Integrations::SelectTargetView,
        integration: current_integration
    end
  end

  private

  def current_integration
    @integration ||= Integration.from_owner_and_slug!(
      viewer:        current_user,
      slug:          params[:id],
      user_login:    params[:owner],
      business_slug: params[:slug],
    )
  end

  def target_for_conditional_access
    current_integration.owner
  end
end
