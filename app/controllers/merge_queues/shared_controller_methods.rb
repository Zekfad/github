# frozen_string_literal: true

module MergeQueues::SharedControllerMethods
  FAILING_STATE = "failing"
  PER_PAGE = 25

  extend ActiveSupport::Concern

  included do
    private

    attr_accessor :up_next_entries_has_next_page

    def up_next_entries_for_display
      up_next_entries = if params[:state] == FAILING_STATE
        merge_queue.failing_up_next_entries
      else
        merge_queue.
          up_next_entries.
          paginate(page: current_page, per_page: PER_PAGE)
      end

      self.up_next_entries_has_next_page = up_next_entries.respond_to?(:total_pages) &&
        up_next_entries.total_pages > current_page

      MergeQueue.prefill_associations(
        entries: up_next_entries,
        repository: current_repository,
      )
    end
  end
end
