# frozen_string_literal: true

class MergeQueues::EntriesController < AbstractRepositoryController
  include MergeQueues::SharedControllerMethods

  before_action :merge_queue_required
  before_action :pull_request_required, only: [:create]
  before_action :require_xhr, only: [:create]
  before_action :content_authorization_required, only: [:create]
  before_action :entry_adminable_by_current_user_required, only: [:destroy]

  include TimelineHelper
  include ShowPartial

  statsd_tag_actions

  def index
    render partial: "merge_queue/entries", locals: {
      entries: up_next_entries_for_display,
      has_next_page: up_next_entries_has_next_page,
      page: current_page,
      state: params[:state],
    }
  end

  def destroy
    pull_request = entry.pull_request
    merge_queue.dequeue(pull_request: pull_request, dequeuer: current_user)
    flash[:notice] = "Successfully dequeued #{pull_request.title}"
    redirect_to :back
  end

  ShowPartialTimelineQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $timelineSince: DateTime,
      $timelinePageSize: Int,
      $syntaxHighlightingEnabled: Boolean = true,
      $deferCollapsedThreads: Boolean,
      $focusedReviewThread: ID,
      $focusedReviewComment: ID,
      $hasFocusedReviewThread: Boolean = false
      $hasFocusedReviewComment: Boolean = false
      $scopedItemTypes: [IssueTimelineItemsItemType!]
    ) {
      node(id: $id) {
        ...Views::Issues::FormActions::FormActionFragment,
        ...Views::PullRequests::Timeline::PullRequest
      }
    }
  GRAPHQL

  def create
    entry = current_repository
      .default_merge_queue
      .enqueue(
        pull_request: pull_request,
        enqueuer: current_user
      )

    if entry.valid?
      pull_node = timeline_owner_response_for(
        query: ShowPartialTimelineQuery,
        id: pull_request.global_relay_id,
        defer_collapsed_threads: defer_collapsed_threads?
      )&.node

      # required for the issue timeline partial
      @pull = pull_request

      render_immediate_partials(pull_request,
        partials: {
          timeline: {
            pull_request: pull_node,
          },
          sidebar: {
            pull_node: pull_node,
          },
          merging: {},
          form_actions: {
            pull: pull_request,
            issue_node: pull_node,
          },
        }
      )
    else
      render_immediate_partials(
        pull_request,
        partials: {
          merging: {
            merging_error: {
              title: "Merge attempt failed",
              message: (entry.errors.full_messages.to_sentence || "We couldn’t merge this pull request.")
            },
          },
        },
        status: :unprocessable_entity,
      )
    end
  end

  private

  def merge_queue_required
    return render_404 unless merge_queue_enabled?
    return render_404 unless merge_queue
  end

  def entry_adminable_by_current_user_required
    render_404 unless entry.adminable_by?(current_user)
  end

  def merge_queue
    current_repository.default_merge_queue
  end

  def entry
    merge_queue.entries.includes(pull_request: [:issue]).find(params[:id])
  end

  def pull_request_required
    render_404 unless pull_request.present?
  end

  def content_authorization_required
    authorize_content(:pull_request, repo: current_repository)
  end

  def pull_request
    return @pull_request if defined?(@pull_request)

    @pull_request = current_repository
      .issues
      .find_by_number(params[:pull_number].to_i)
      &.pull_request
  end
end
