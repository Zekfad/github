# frozen_string_literal: true

class GitignoreController < ApplicationController

  # The following actions do not require conditional access checks:
  # - show: serves `/site/gitignore/:template`, a simple endpoint to
  #   look up and render .gitignore templates, with no access to protected
  #   organization resources.
  skip_before_action :perform_conditional_access_checks, only: %w(show)

  def show
    template = params[:template]
    render plain: Gitignore.template(template)
  end
end
