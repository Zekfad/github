# frozen_string_literal: true

class RepositoryHooksController < AbstractRepositoryController
  areas_of_responsibility :webhook

  statsd_tag_actions

  include HooksControllerMethods # All Hook related actions

  before_action :block_new_github_services!, only: [:new, :create]
  before_action :block_updates_to_github_services!, only: [:update]

  layout :repository_layout
  javascript_bundle :settings

  private

  # Repo hooks
  def current_context
    current_repository
  end

  # Set up as a before_action in RepositoryControllerMethods.
  # Overloaded so that hooks aren’t visible to people who can pull public repos.
  def privacy_check
    has_permissions =
      if GitHub.flipper[:custom_roles].enabled?(current_repository.owner) &&
        GitHub.flipper[:fgp_webhook].enabled?(current_repository.owner) &&
        GitHub.flipper[:repository_hooks_controller_fgp_friendly_privacy_check].enabled?(current_repository.owner)

        current_repository.async_can_manage_webhooks?(current_user).sync
      else
        science "repository_hooks_controller_fgp_friendly_privacy_check" do |e|
          e.context user: current_user,
            repo: current_repository

          e.use {
            current_repository.adminable_by? current_user
          }
          e.try {
            current_repository.async_can_manage_webhooks_result(current_user).sync
          }
          e.compare { |control, candidate| control == candidate.allow? }

          # Reduce noise of this experiment by ignoring network failures
          e.ignore { |_control, candidate| Authzd.ignore_error_in_science?(candidate) }

          # :fgp_webhook is the flag that guards all new webhook functionality.
          # It is temporary and meant to be used as emergency switch.
          # To avoid modifying the existing policies to include a check for it, we ignore the mismatch.
          e.ignore { |_control, _candidate| !GitHub.flipper[:fgp_webhook].enabled?(current_repository.owner) }
        end
      end

    return if has_permissions
    return render "admin/locked_repo" if logged_in? && current_user.site_admin?
    render_404
  end

  def block_new_github_services!
    return if hook_type == "web"

    error_str = "GitHub Services hooks are no longer able to be created."
    redirect_to(repository_installations_path, flash: {error: error_str})
  end

  # rubocop:disable Rails/OutputSafety
  def block_updates_to_github_services!
    return if @hook&.webhook?

    flash.now[:error] = "GitHub Services hooks are no longer able to be modified. Please see our <a href='#{GitHub.developer_blog_url}/2018-04-25-github-services-deprecation'>blog post</a> for more info.".html_safe
    render_template_view("hooks/show", Hooks::ShowView, hook: @hook)
  end
end
