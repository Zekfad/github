# frozen_string_literal: true

class Repos::ProjectsController < AbstractRepositoryController
  include ProjectControllerActions
  include SharedProjectControllerActions

  statsd_tag_actions only: [:index, :show]

  before_action :require_push_access, except: [:index, :show, :clone, :destroy, :activity, :target_owner_results]
  before_action :user_with_project_read_required, only: [:clone, :target_owner_results]
  before_action :set_client_uid
  before_action :require_projects_enabled
  before_action :verify_cloning_conditional_access, only: [:clone]
  before_action :non_migrating_repository_required, except: [:index, :show]
  before_action :set_cache_control_no_store, only: [:show, :search_results]

  layout :repository_layout

  def index
    render_projects_index(owner: current_repository)
  end

  def new
    render_new_project(owner: current_repository)
  end

  def edit
    render_edit_project(owner: current_repository, project: this_project)
  end

  def show
    render_show_project(owner: current_repository, project: this_project)
  end

  def create
    create_project(owner: current_repository)
  end

  def update
    update_project(owner: current_repository, project: this_project)
  end

  def update_state
    update_project_state(project: this_project, state: params[:state], sync: params[:sync])
  end

  def clone
    clone_project(project: this_project)
  end

  def search_results
    render_project_search_results(owner: current_repository, project: this_project)
  end

  def activity
    render_project_activity(owner: current_repository, project: this_project)
  end

  def add_cards_link
    render_project_add_cards_link(project: this_project)
  end

  def destroy
    destroy_project(owner: current_repository)
  end

  def target_owner_results
    render_target_owner_results(project: this_project)
  end

  private

  helper_method :parsed_projects_query

  def require_push_access
    return true if current_user_can_push?
    return render_404 unless this_project.readable_by?(current_user)

    # If we can read the project but can't write it, show an informative error
    # message or status code.
    respond_to do |format|
      format.html do
        flash[:error] = "You don't have permission to perform this action on this project."
        redirect_to project_path(this_project)
      end

      format.json do
        head 403
      end
    end
  end

  def user_with_project_read_required
    render_404 unless logged_in? && this_project.readable_by?(current_user)
  end

  def this_project
    @this_project ||= current_repository.projects.find_by_number!(params[:number])
  end
end
