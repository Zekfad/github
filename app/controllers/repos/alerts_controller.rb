# frozen_string_literal: true

class Repos::AlertsController < GitContentController
  areas_of_responsibility :dependabot_alerts, :dependabot

  map_to_service :dependabot, only: [:update_error, :bot_resolve]

  statsd_tag_actions only: [:index, :show]

  skip_before_action :try_to_expand_path
  before_action :authorize_alerts
  before_action :require_dependabot, only: [:bot_resolve, :update_error]
  layout :repository_layout

  def index
    query_string = params[:q].kind_of?(String) ? params[:q] : "is:open"
    query_hash = RepositoryVulnerabilityAlert::QueryParser.parse_and_normalize(query_string)
    query = RepositoryVulnerabilityAlert::AlertQuery.new(current_repository)

    query = query.paginate(params[:page]).
      state_is(query_hash[:is]).
      sort_by(query_hash[:sort])

    alerts = query.resolve

    render_template_view "repos/alerts/index", RepositoryAlerts::IndexView, {
      current_repository: current_repository,
      query_string: query_string,
      query: query,
      alerts: alerts,
    }
  end

  def show
    view = create_view_model(
      RepositoryAlerts::ShowView,
      repository: current_repository,
      manifest_path: params[:manifest_path],
      package_name: params[:package_name],
      state: params[:state],
    )

    if view.missing?
      render "repos/alerts/missing", locals: { view: view }, status: :not_found
    else
      render "repos/alerts/show", locals: { view: view }
    end
  end

  def update_error
    dependency_update = fetch_dependency_update(params.require(:dependency_update_id))

    view = create_view_model(
      RepositoryAlerts::UpdateErrorView,
      repository: current_repository,
      manifest_path: params[:manifest_path],
      package_name: params[:package_name],
      state: params[:state],
      dependency_update: dependency_update,
    )

    if view.missing?
      render "repos/alerts/missing_update_error", locals: { view: view }, status: :not_found
    else
      render "repos/alerts/update_error", locals: { view: view }
    end
  end

  def bot_resolve
    dependency_update = RepositoryDependencyUpdate.request_for_dependency(repository: current_repository,
                                                                          manifest_path: params[:manifest_path],
                                                                          package_name: params[:package_name],
                                                                          trigger: :manual)

    if dependency_update
      flash[:notice] = "Started generating an automated security update for #{alert_params[:package_name]}."
    else
      flash[:error] = "Failed to generate an automated security update for #{alert_params[:package_name]}. Try again later."
    end

    redirect_to network_alert_path(alert_params)
  end

  private

  def alert_params
    {
      manifest_path: params[:manifest_path],
      package_name: params[:package_name],
      state: params[:state],
    }
  end

  def fetch_dependency_update(dependency_update_id)
    if current_repository.dependabot_api_error_pages_enabled?
      dependency_update = current_repository.dependency_updates.find_by(id: dependency_update_id)
      Dependabot::UpdateJob.get_update_job(dependency_update)
    else
      current_repository.dependency_updates.find_by(id: dependency_update_id)
    end
  end

  def authorize_alerts
    render_404 unless current_repository.show_dedicated_alerts_ui?(current_user)
  end

  def require_dependabot
    render_404 unless current_repository.automated_security_updates_visible_to?(current_user)
  end
end
