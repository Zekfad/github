# frozen_string_literal: true

class Repos::ProjectColumnsController < AbstractRepositoryController
  include ProjectColumnControllerActions
  include SharedProjectControllerActions

  statsd_tag_actions only: :show

  before_action :require_push_access, except: [:show]
  before_action :set_client_uid
  before_action :require_projects_enabled

  def show
    render_show_column(project: this_project)
  end

  def create
    create_project_column(project: this_project)
  end

  def update
    update_project_column(project: this_project)
  end

  def reorder
    reorder_project_columns(project: this_project)
  end

  def archive
    archive_project_column(project: this_project)
  end

  def update_workflow
    update_project_column_workflow(project: this_project)
  end

  def automation_options
    render_project_column_automation_options(project: this_project)
  end

  def destroy
    destroy_project_column(project: this_project)
  end

  private

  def this_project
    @this_project ||= current_repository.projects.find_by_number!(params[:project_number])
  end

  def require_push_access
    render_404 unless current_user_can_push?
  end
end
