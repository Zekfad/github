# frozen_string_literal: true

class MarketplaceAgreementSignaturesController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required
  before_action :login_required

  layout "site"

  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: SignMarketplaceAgreementInput!) {
      signMarketplaceAgreement(input: $input) {
        marketplaceListing
      }
    }
  GRAPHQL

  def create
    unless params[:accept].to_s == "1"
      flash[:error] = "You must accept the terms of the agreement."
      return redirect_to :back
    end

    input = { listingID: params[:marketplace_listing_id],
              agreementID: params[:marketplace_agreement_id] }
    data = platform_execute(CreateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:signMarketplaceAgreement]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
    end

    redirect_to :back
  end

  private

  def target_for_conditional_access
    agreement = typed_object_from_id([Platform::Objects::MarketplaceAgreement], params[:marketplace_agreement_id])
    return :no_target_for_conditional_access unless agreement.integrator?

    listing = typed_object_from_id([Platform::Objects::MarketplaceListing], params[:marketplace_listing_id])
    listing.owner
  rescue Platform::Errors::NotFound
    return :no_target_for_conditional_access # Marketplace::Agreement or Marketplace::Listing not found
  end
end
