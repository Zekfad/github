# frozen_string_literal: true

class AutocompleteController < ApplicationController
  areas_of_responsibility :orgs, :repositories

  include OrganizationsHelper

  # Because this controller doesn't deal with protected organization resources,
  # we can safely `skip_before_action` its actions.
  skip_before_action :perform_conditional_access_checks,
    only: [:users, :organizations, :user_suggestions, :organization_suggestions, :emoji_suggestions]

  before_action :login_required
  before_action :require_xhr, only: [:user_suggestions, :organization_suggestions, :emoji_suggestions]

  def users
    respond_to do |format|
      format.html_fragment do
        render_partial_view "users/autocomplete", AutocompleteView, query: params[:q]
      end
      format.html do
        return head :not_acceptable unless request.xhr?
        render_partial_view "users/autocomplete", AutocompleteView, query: params[:q]
      end
    end
  end

  def organizations
    organizations = ::Organization.search(params[:q], limit: 10)
    respond_to do |format|
      format.html_fragment do
        render partial: "organizations/autocomplete", formats: :html, locals: {results: organizations}
      end
      format.html do
        return head :not_acceptable unless request.xhr?
        render partial: "organizations/autocomplete", locals: {results: organizations}
      end
    end
  end

  def org_users
    render_404 and return unless current_organization
    respond_to do |format|
      format.html_fragment do
        render_partial_view "users/autocomplete", AutocompleteView, { query: params[:q], organization: current_organization, org_members_only: true }
      end
      format.html do
        return head :not_acceptable unless request.xhr?
        render_partial_view "users/autocomplete", AutocompleteView, { query: params[:q], organization: current_organization, org_members_only: true }
      end
    end
  end

  def user_suggestions
    suggester = Suggester::UserSuggester.new(viewer: current_user)
    respond_to do |format|
      format.json do
        render json: suggester.mentions
      end
    end
  end

  def organization_suggestions
    suggester = Suggester::OrganizationSuggester.new(viewer: current_user)
    respond_to do |format|
      format.json do
        render json: suggester.mentions
      end
    end
  end

  def emoji_suggestions
    respond_to do |format|
      format.html do
        render partial: "comments/suggesters/emoji_suggester", locals: {
          emojis: GitHub::Emoji,
          use_colon_emoji: params[:use_colon_emoji].present?,
          tone: logged_in? ? current_user.profile_settings.preferred_emoji_skin_tone : 0,
        }
      end
    end
  end

  private

  def current_organization
    super || (
      if id = params[:organization_id] || params[:org] || params[:id]
        if logged_in?
          org = Organization.find_by_login(id)
          return org if org && org.user_is_outside_collaborator?(current_user.id)
        end
      end
    )
  end
end
