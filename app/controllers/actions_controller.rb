# frozen_string_literal: true

class ActionsController < AbstractRepositoryController
  map_to_service :actions_experience

  include ActionsHelper
  include TreeHelper
  include ActionsControllerMethods

  layout :repository_layout

  around_action :track_and_report_render_view_time, only: [:index]
  around_action :track_and_report_graphql_executions, only: [:index]
  around_action :track_and_report_mysql_executions, only: [:index]
  before_action :login_required, except: [:index]
  before_action :actions_enabled_for_repo?
  before_action :installable_by_current_user?, only: [:enable]
  before_action :actions_already_installed?, only: [:enable]

  statsd_tag_actions only: [:index, :new]

  helper_method :new_workflow_path

  def index
    if show_enable_actions_blankslate?
      emit_stat("render_enable_actions", ["fork:#{current_repository.fork?}"])
      render "actions/enable_actions_blankslate"
    elsif has_workflow_runs_or_file?
      show_survey_prompt = Actions::Survey.show_survey_prompt_for_user(current_user)
      Actions::Survey.emit_stat("shown") if show_survey_prompt

      render "actions/index", locals: {
        workflow_run_filters: workflow_run_filters,
        workflow_runs: paged_workflow_runs,
        workflows: workflows,
        selected_workflow_name: selected_workflow_name,
        selected_workflow: selected_workflow,
        show_survey_prompt: show_survey_prompt,
      }
    elsif !logged_in? || !current_user_can_push?
      render "actions/actions_blankslate"
    else
      redirect_to new_workflow_path
    end
  end

  def enable
    result = current_repository.enable_actions_app

    if result.success?
      emit_stat("enable", ["result:success", "fork:#{current_repository.fork?}"])
      redirect_to actions_path, flash: { notice: "Actions Enabled." }
    else
      reason = result.reason || ""
      emit_stat("enable", ["result:failed", "fork:#{current_repository.fork?}", "reason:#{reason}"])
      redirect_to actions_path, flash: { notice: "Unable to enable Actions for this repository." }
    end
  end

  def new
    if current_repository.writable_by?(current_user)
      render_template_view "actions/new",
        Actions::StarterWorkflowsView,
        {
          repository: current_repository,
          category_filter: category_filter,
        },
        locals: { workflows: workflows }
    else
      render "actions/deny_read_only_users"
    end
  end

  def workflow_runs
    return head :not_found unless request.xhr?

    workflow_runs = paged_workflow_runs
    render partial: "actions/workflow_runs", locals: { workflow_runs: workflow_runs }
  end

  def workflow_run_item
    check_suite = CheckSuite.find_by(id: params[:workflow_run_id])
    return head :not_found unless check_suite
    workflow_run = check_suite.workflow_run
    return head :not_found unless workflow_run && request.xhr?

    render_partial_view "actions/workflow_run_item", Actions::WorkflowRunItemView, workflow_run: workflow_run
  end

  private

  def paged_workflow_runs
    if workflow_run_filters.empty?
      current_repository.workflow_runs
        .from("#{Actions::WorkflowRun.table_name} USE INDEX(index_workflow_runs_on_repository_id)")
        .includes(:check_suite, :trigger)
        .most_recent
        .paginate(
          page: current_page,
          per_page: 25,
        )
    else
      filtered_workflow_runs[:workflow_runs]
    end
  end

  def find_workflow_run
    workflow_run = Actions::WorkflowRun.find_by(id: params[:workflow_run_id])
    if !workflow_run || workflow_run.check_suite.repository_id != current_repository.id
      return nil
    end
    workflow_run
  end

  def new_workflow_path
    actions_onboarding_path
  end

  def show_enable_actions_blankslate?
    return false unless current_repository.writable_by?(current_user)
    actions_app_not_installed? && has_workflow_runs_or_file?
  end

  def actions_app_not_installed?
    GitHub.launch_github_app.installations_on(
      current_repository.owner,
    ).with_repository(current_repository).none?
  end

  def filtered_workflow_runs
    return @_filtered_workflow_runs if defined?(@_filtered_workflow_runs)

    @_filtered_workflow_runs = Actions::WorkflowRun.search(
      query: workflow_query,
      workflow: params[:workflow],
      repo: current_repository,
      current_user: current_user,
      remote_ip: request.remote_ip,
      user_session: user_session,
      page: current_page,
    )
  end

  def category_filter
    params[:category]
  end

  def all_workflow_runs
    @_all_workflow_runs ||= current_repository.actions_check_suites
  end

  def has_workflow_runs?
    all_workflow_runs.any?
  end

  def has_workflow_runs_or_file?
    has_workflow_runs? ||
      current_repository.workflow_v2_file_present?(current_branch_or_tag_name)
  end

  def installable_by_current_user?
    render_404 unless current_repository.writable_by?(current_user)
    render_404 unless has_workflow_runs_or_file?
  end

  def actions_already_installed?
    unless actions_app_not_installed?
      redirect_to actions_path, flash: { notice: "Actions Enabled." }
    end
  end

  def emit_stat(suffix, tags = [])
    GitHub.dogstats.increment("actions_controller.#{suffix}", tags: tags)
  end
end
