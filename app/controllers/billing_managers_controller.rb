# frozen_string_literal: true

class BillingManagersController < ApplicationController
  include OrganizationsHelper
  include Orgs::InvitationsControllerMethods

  limit_invitation_roles :billing_manager

  # Always render a 404 when billing is disabled.
  # See Application#ensure_billing_enabled.
  before_action :ensure_billing_enabled

  before_action :login_required, except: [:show_pending, :sign_up]
  before_action :this_organization_required
  before_action :find_pending_invitation, only: [:show_pending, :accept, :sign_up]
  before_action :check_ofac_flagged_user, only: [:show_pending, :accept]
  before_action :org_billing_management_only, except: [:show_pending, :accept, :sign_up]
  before_action :ensure_two_factor_requirement_is_met, only: [:accept]

  include Orgs::Invitations::RateLimiting
  setup_org_invite_rate_limiting only: [:create]

  def new
    respond_to do |format|
      format.html do
        render_template_view "billing_managers/new", BillingManagers::NewView,
          organization: this_organization,
          rate_limited: at_rate_limit?(action: :create, check_without_increment: true)
      end
    end
  end

  def show_pending
    respond_to do |format|
      format.html do
        render_template_view "billing_managers/show_pending", BillingManagers::ShowPendingView,
          invitation: pending_invitation
      end
    end
  end

  def accept
    result = pending_invitation.accept(acceptor: current_user, via_email: params[:via_email].present?)

    if result.success?
      flash[:notice] = "You are now a billing manager of #{this_organization.safe_profile_name}!"
      redirect_to settings_org_billing_path(this_organization)
    else
      flash[:error] = result.error
      redirect_to org_show_pending_billing_manager_invitation_path(this_organization)
    end
  end

  def sign_up
    render_sign_up_via_invitation invitation: pending_invitation
  end

  def cancel
    if User.valid_email?(params[:id])
      email = params[:id]
    else
      user = User.find_by_login!(params[:id])
    end

    invitation = this_organization.pending_invitation_for(user, email: email, role: :billing_manager)
    return render_404 unless invitation

    invitation.cancel(actor: current_user)
    flash[:notice] = "Successfully canceled the invitation."
    redirect_to settings_org_billing_path(this_organization)
  end

  def destroy
    user = User.find_by_login!(params[:id])
    current_organization_for_member_or_billing.billing.remove_manager(user, actor: current_user)
    flash[:notice] = "Successfully removed billing management access for @#{user.login}."
    if current_organization_for_member_or_billing.billing_manager?(current_user) || current_organization_for_member_or_billing.adminable_by?(current_user)
      redirect_to settings_org_billing_path(current_organization_for_member_or_billing)
    else
      redirect_to dashboard_path
    end
  end

  def invitee_suggestions
    headers["Cache-Control"] = "no-cache, no-store"

    respond_to do |format|
      format.html_fragment do
        render_partial_view "billing_managers/invitee_suggestions", BillingManagers::InviteeSuggestionsView,
          organization: this_organization,
          query: params[:q]
      end
    end
  end

  def create
    if User.valid_email?(params[:id])
      email = params[:id]

      if email_verification_required?
        # Require verified email address to send invites to email addresses
        return render_email_verification_required
      end
    else
      user = User.find_by_login(params[:id])
    end

    if user.nil? && email.nil?
      flash[:error] = if params[:id].match(User::LOGIN_REGEX)
        "We couldn’t find a user named @#{params[:id]}. Please enter the GitHub username of the person you want to invite."
      else
        "We couldn’t find a user with the username you provided. Please enter the GitHub username of the person you want to invite."
      end
      redirect_to :back
    else
      if user&.has_any_trade_restrictions? || this_organization.has_any_trade_restrictions?
        flash[:error] = "The user cannot be added as a billing manager of the organization."
        return redirect_back(fallback_location: settings_org_billing_path(current_organization_for_member_or_billing))
      end

      if user == current_user
        # Add yourself directly as a billing manager
        current_organization_for_member_or_billing.billing.add_manager(current_user, actor: current_user)
        invitation = current_organization_for_member_or_billing.pending_invitation_for(current_user, role: :billing_manager)
        invitation.cancel(actor: current_user) if invitation
        flash[:notice] = "You are now a billing manager for #{current_organization_for_member_or_billing.safe_profile_name}."
      else
        # Otherwise create a normal invitation
        begin
          invitation = current_organization_for_member_or_billing.invite(user, email: email, inviter: current_user, role: :billing_manager)
          GitHub.dogstats.increment("billing.managers.invite.count")
        rescue OrganizationInvitation::AlreadyAcceptedError, OrganizationInvitation::InvalidError, ActiveRecord::RecordInvalid
          return render_404
        end

        flash[:notice] = "Successfully invited #{invitation.email_or_invitee_name} to be a billing manager. They’ll be receiving an email shortly."
      end

      redirect_to settings_org_billing_path(current_organization_for_member_or_billing)
    end
  end

  def resend_invitation
    if User.valid_email?(params[:id])
      email = params[:id]
    else
      user = User.find_by_login(params[:id])
    end

    invitation = this_organization.pending_invitation_for(user, email: email, role: :billing_manager)
    return render_404 unless invitation

    unless current_user.spammy? || invitation.opted_out?
      invitation.reset_token
      OrganizationMailer.invited_to_billing_manager_role(invitation, invitation.token).deliver_later
    end

    flash[:notice] = "Successfully sent #{invitation.email_or_invitee_name} another invite."
    redirect_to settings_org_billing_path(current_organization_for_member_or_billing)
  end

  private

  # Internal: Gets the organization we're operating inside based on an
  # `:organization_id`
  #
  # Returns an Organization or nil if the org isn't found.
  def this_organization
    return @this_organization if defined? @this_organization
    @this_organization = Organization.find_by_login(params[:organization_id])
  end

  # Internal: This before_action renders a standard 404 page if
  # `this_organization` is nil.
  #
  # Returns nothing.
  def this_organization_required
    render_404 if this_organization.nil?
  end

  def check_ofac_flagged_user
    if current_user&.has_any_trade_restrictions?
      flash[:trade_controls_user_billing_error] = true
      redirect_to settings_user_billing_url
    end
  end

  def pending_invitation_for_current_user
    @pending_invitation_for_current_user ||= this_organization.pending_invitation_for(current_user, role: :billing_manager)
    render_404 if @pending_invitation_for_current_user.nil?

    @pending_invitation_for_current_user
  end

  def email_verification_required?
    authorization = ContentAuthorizer.authorize(
      current_user, :organization_invitation, :create
    )

    authorization.has_email_verification_error?
  end

  def org_invite_rate_limited
    org_invite_rate_limit_policy.record_rate_limited

    view = create_view_model BillingManagers::NewView,
      organization: this_organization, rate_limited: true
    render "billing_managers/new", status: 429, locals: {view: view}
  end

  def invite_rate_limited_organization
    this_organization
  end

  def ensure_two_factor_requirement_is_met
    unless this_organization.two_factor_requirement_met_by?(current_user)
      redirect_to org_show_pending_billing_manager_invitation_path(this_organization)
    end
  end
end
