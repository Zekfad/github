# frozen_string_literal: true

# Class ExploreController: This is the controller for all things /explore
class ExploreController < ApplicationController
  areas_of_responsibility :explore

  statsd_tag_actions only: :index

  VIMEO_IMAGE_THUMBNAIL_HOST = "i.vimeocdn.com"
  YOUTUBE_IMAGE_THUMBNAIL_HOST = "img.youtube.com"
  USER_SUMMARY_TOPIC_STARS_LIMIT = 5

  before_action :add_csp_exceptions, only: [:index]

  CSP_EXCEPTIONS = {
    img_src: [
      VIMEO_IMAGE_THUMBNAIL_HOST,
      YOUTUBE_IMAGE_THUMBNAIL_HOST,
      ExploreFeed::Event::FEED_URL,
      ExploreFeed::Spotlight::SPOTLIGHTS_FEED_URL,
    ],
    media_src: [SecureHeaders::CSP::SELF, GitHub.asset_host_url],
  }

  skip_before_action :perform_conditional_access_checks

  include ExploreHelper
  include Trending::FeedHelper

  layout "site"
  javascript_bundle :explore

  def index
    if GitHub.enterprise?
      @sections = []
      @trending = get_trending_records

      setup_stars_section
      @subscription = newsletter_subscription if logged_in?

      # sort the sections by the sort object
      @sections = @sections.sort { |a, b| a[:sort] <=> b[:sort] }

      render "explore/index"
    else
      marketplace_listings = Marketplace::Listing.verified_and_shuffled
      featured_collection = ExploreCollection.featured_and_shuffled(limit: 1).first
      spotlights = ExploreFeed::Spotlight.all.current.to_a.shuffle
      featured_event = ExploreFeed::Event.all.upcoming_or_current.sample

      if logged_in?
        non_spotlight_topics = ExploreFeed::Recommendation::Topic
          .all(for_user: current_user)
          .non_spotlight
        featured_topic = ExploreFeed::Recommendation::Topic.all(for_user: current_user).spotlight
        repository_recommendations = RepositoryRecommendation
          .filtered_for(current_user, per_page: 10) || []
        trending_repositories = ExploreFeed::Trending::Repository
          .all(spoken_language_code: current_user&.profile_spoken_language_preference_code)
      else
        featured_topic = Topic.not_flagged.curated.with_logo.featured_and_shuffled.first
        trending_repositories = ExploreFeed::Trending::Repository.all
      end

      render "explore/feed/index", locals: {
        featured_collection: featured_collection,
        featured_event: featured_event,
        featured_topic: featured_topic,
        marketplace_listings: marketplace_listings,
        non_spotlight_topics: non_spotlight_topics,
        recommended_developers: recommended_developers,
        repository_recommendations: repository_recommendations,
        spotlights: spotlights,
        trending_repositories: trending_repositories,
      }
    end
  end

  def email
    if tokened_user = user_from_token
      @subscription      = NewsletterSubscription.subscribe(tokened_user, "explore", explore_period)
      @unsubscribe_token = @subscription.unsubscribe_token
      @visible           = { explore_period.to_sym => "visible" }
    elsif logged_in?
      @subscription      = newsletter_subscription
      @unsubscribe_token = NewsletterSubscription.get_unsubscribe_token(current_user, "explore")
    end
    @visible ||= {}

    render "explore/email"
  end

  private

  # Return the attached user to a valid unsubscribe token
  def user_from_token
    token = NewsletterSubscription.validate_token(params[:token].to_s)
    token.try(:user)
  end

  def newsletter_subscription
    NewsletterSubscription.where("user_id = ? AND name = ?", current_user.id, "explore").first
  end

  def setup_stars_section
    if logged_in?
      @stars = explore_follow_stars_repositories(
        @trending.map { |r| r[0].id },
      )
      if @stars.present?
        @sections.push(
          id: "explore-stars",
          component: Explore::StarsComponent,
          args: {stars: @stars, explore_period: explore_period},
          sort: 1,
        )
      end
    else
      @sections.push(
        id: "explore-stars",
        component: Explore::StarsComponent,
        args: {stars: [], explore_period: explore_period},
      )
    end
  end

  def get_trending_records
    if GitHub.munger_available?
      ExploreFeed::Trending::Repository.all(period: explore_period)
    else
      []
    end
  end


  def recommended_developers
    return [] unless current_user_feature_enabled?(:recommended_developers)
    ExploreFeed::Recommendation::Developer.all(current_user)
  end

  def explore_follow_stars_repositories(exclude_repos)
    GitHub.dogstats.time("explore.stars", tags: ["action:follow", "period:#{explore_period}"]) do
      Star.from_following(current_user, period: explore_period, limit: 6, exclude_repos: exclude_repos)
    end
  end

  # normalize since, in case there's weird things passed in
  def explore_period
    @since ||=
      case params[:since]
      when "daily"
        "daily"
      when "monthly"
        "monthly"
      else
        "weekly"
      end
  end
  helper_method :explore_period

  def parsed_issues_query
    [[:is, "open"], [:is, "issue"]]
  end
  helper_method :parsed_issues_query
end
