# frozen_string_literal: true

class RepositoryOpenGraphImagesController < AbstractRepositoryController
  skip_before_action :perform_conditional_access_checks, only: [:template]
  before_action :login_required
  before_action :set_open_graph_image_permission_required

  def template
    path = Rails.root.join("public", "static", "images", "repository-og-image-template.png")
    send_file path, type: "image/png", filename: "repository-open-graph-template.png"
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteRepositoryImageInput!) {
      deleteRepositoryImage(input: $input) {
        repository
      }
    }
  GRAPHQL

  def destroy
    image = RepositoryImage.find(params[:id])
    data = platform_execute(DestroyQuery, variables: { input: { id: image.global_relay_id } })

    if data.errors.any?
      error_type = data.errors.details[:deleteRepositoryImage]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
    end

    redirect_to edit_repository_path(params[:user_id], params[:repository])
  end

  def set_open_graph_image_permission_required
    render_404 unless current_repository.async_can_set_social_preview?(current_user).sync
  end

end
