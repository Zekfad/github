# frozen_string_literal: true

module Editor
  class ActionsController < ApplicationController
    map_to_service :actions_experience

    layout false

    helper_method :pagination_url, :search_placeholder

    FEATURED_ACTIONS_LIMIT = 5

    SEARCH_RESULTS = 10

    SearchQuery = parse_query <<-'GRAPHQL'
      query($first: Int, $after: String, $last: Int, $before: String, $searchType: MarketplaceSearchType, $categorySlug: String!, $query: String!) {
        marketplaceCategory(slug: $categorySlug) {
          name
          slug
        }
        ...Views::Editor::Actions::Search::Root
      }
    GRAPHQL

    def index
      variables = {
        query: params.fetch(:query, ""),
        categorySlug: params.fetch(:category, ""),
        searchType: "MARKETPLACE_ACTIONS",
      }
      variables.merge!(graphql_pagination_params(page_size: SEARCH_RESULTS))
      data = platform_execute(SearchQuery, variables: variables)
      return head :not_found if params[:category].present? && data.marketplace_category.nil?

      if params[:category].blank? && params[:query].blank?
        render "editor/actions/index", formats: :html, locals: {
          featured_actions: featured_actions,
          seed: seed,
          categories: featured_categories,
        }
      else
        render "editor/actions/search", formats: :html, locals: { data: data, category_name: data.marketplace_category&.name }
      end
    end


    def show
      action = RepositoryAction.find_by(id: params[:action_id])
      render "editor/actions/show", locals: { action: action }
    end

    private
    def target_for_conditional_access
      :no_target_for_conditional_access
    end

    def seed
      @seed ||= params.fetch(:seed, rand(4294967295)).to_i
    end

    def featured_actions
      featured_actions = RepositoryAction.
        featured.
        listed.
        preload(repository: [:owner]).
        order(Arel.sql("RAND(#{seed})")).
        limit(FEATURED_ACTIONS_LIMIT).
        sort_by { |action| action.repository.stargazer_count }.
        reverse
    end

    def pagination_url(before: nil, after: nil)
      pagination_params = {
        category: params[:category],
        before: before,
        after: after,
        seed: params[:seed],
      }.compact
      editor_actions_search_url(pagination_params)
    end

    def search_placeholder(category: nil)
      if category.present?
        "Search Marketplace for #{category.capitalize} Actions"
      else
        "Search Marketplace for Actions"
      end
    end

    def featured_categories
      # Hard-code the list of categories for now, see https://github.com/github/c2c-actions-experience/issues/1702
      [
        {
          name: "Code quality",
          slug: "code-quality",
        },
        {
          name: "Continuous integration",
          slug: "continuous-integration",
        },
        {
          name: "Deployment",
          slug: "deployment",
        },
        {
          name: "Monitoring",
          slug: "monitoring",
        },
        {
          name: "Project management",
          slug: "project-management",
        },
        {
          name: "Testing",
          slug: "testing",
        },
      ]
    end
  end
end
