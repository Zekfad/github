# frozen_string_literal: true

class TreeController < GitContentController
  map_to_service :tree, only: %i(delete destroy dismiss_tree_finder_help find list tree_commit tarball zipball archive)
  map_to_service :blob, only: [:check]
  map_to_service :ref, only: [:show_partial]
  map_to_service :repo_creation, only: [:fork_select, :fork]
  map_to_service :star, only: [:star, :unstar]

  CONDITIONAL_ACCESS_BYPASS_PUBLIC_REPO_ACTIONS = %w(
    archive tarball zipball show_partial find
    list tree_commit star unstar fork_select fork
  ).freeze

  statsd_tag_actions only: [:list, :tree_commit]

  include ActionView::Helpers::NumberHelper
  include ShowPartial
  include GitRPC::Util
  include WebCommitControllerMethods

  before_action :clean_params, only: [:delete, :destroy]
  skip_before_action :ask_the_gitkeeper, only: [:star, :unstar, :check]
  skip_before_action :ask_the_gatekeeper, only: [:unstar]
  skip_before_action :authorization_required, only: [:unstar]
  before_action :login_required, only: [:delete, :destroy, :star, :unstar]
  before_action :content_authorization_required, only: [:delete, :destroy, :fork, :fork_select]
  layout :repository_layout

  def delete
    return render_404 unless GitHub.flipper[:delete_directory_web_ui].enabled?(current_repository)
    return render_404 if params[:path].blank?

    @path = path_string
    @branch = tree_name

    return render_404 unless current_repository.includes_directory?(@path, @branch)
    return render_404 unless establish_target_repository(branch: @branch, create_fork: true)
    return render_404 unless @target_repo.heads.find(@branch)
    return render_404 unless set_form_commit

    render "tree/delete"
  end

  def destroy
    return render_404 unless GitHub.flipper[:delete_directory_web_ui].enabled?(current_repository)
    return render_404 if params[:path].blank?

    path = path_string
    branch = tree_name

    return render_404 unless current_repository.includes_directory?(path, branch)
    return render_404 unless establish_target_repository(branch: branch, create_fork: false)

    ref = @target_repo.heads.find(branch)
    return render_404 unless ref

    increment_quick_pull_start_stats

    if @target_repo != current_repository
      params[:quick_pull] ||= [current_repository.owner.login, branch].join(":")
    end

    # TODO: Refactor after `web_commit.detect_conflict` experiment is graduated.
    #
    # Currently WebCommitControllerMethods#change_conflict? requires a blob path,
    # so we have to iterate through each file in the directory to check for changes.
    # The experiment candidate accepts any path type (tree or blob), so we can
    # remove the enumeration below and simply call:
    #   if change_conflict?(path, params[:commit], ref.target_oid)
    #     increment_quick_pull_error_stats
    #     # etc...
    # See https://github.com/github/github/pull/148539 for more details.
    has_conflicts = current_directory.tree_entries.map(&:path).any? do |tree_entry_path|
      change_conflict?(tree_entry_path, params[:commit], ref.target_oid)
    end

    if has_conflicts
      increment_quick_pull_error_stats
      return delete
    end

    message = commit_message_from_request("delete")
    files = {"#{current_directory.path}" => nil}

    branch = commit_change_to_repo_for_user(@target_repo, current_user, branch, params[:commit], files, message)

    unless branch
      # Don't show the flash error on hook failures, since we show their output separately
      if @hook_out.present?
        @hook_message = "Directory could not be deleted."
      else
        flash[:error] = "Directory could not be deleted."
      end

      increment_quick_pull_error_stats
      return delete
    end

    increment_quick_pull_commit_stats

    if params[:quick_pull]
      # redirect to new pull request page
      base = params[:quick_pull]
      base = nil if base == "1"
      range = [base, branch].compact.join("...")
      redirect_to compare_path(current_repository, range) + "?quick_pull=1"
    elsif redirect_back_to_pr?
      # redirect to existing pull request page
      redirect_to "#{params[:pr]}/files##{diff_path_anchor(path)}"
    else
      # redirect to parent directory
      flash[:notice] = "Directory successfully deleted."
      redirect_path = current_repository.longest_existing_subpath(path, branch)
      redirect_path = nil if redirect_path.empty?
      redirect_to tree_url(name: branch, path: redirect_path)
    end
  end

  def check
    if current_repository.nil? || !current_repository.ready_for_writes?
      head 202
    else
      head :ok
    end
  end

  param_encoding :find, :name, "ASCII-8BIT"

  def find
    return render_404 if tree_sha.nil?

    render "tree/find"
  end

  def list
    expires_in 1.day

    if stale? etag: params[:name]
      if valid_full_sha1?(params[:name])
        paths = current_repository.tree_file_list(params[:name])
        render json: { paths: paths }
      else
        render status: 400, json: { paths: [] }
      end
    end
  end

  def dismiss_tree_finder_help
    current_user.dismiss_notice("tree_finder_help")

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  param_encoding :tree_commit, :name, "ASCII-8BIT"

  TreeCommitQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $refName: String, $path: String) {
      repository: node(id: $id) {
        ... on Repository {
          latestCommit(refName: $refName, path: $path) {
            ...Views::Commit::CommitTease
          }
        }
      }
    }
  GRAPHQL

  def tree_commit
    begin
      data = platform_execute(TreeCommitQuery, variables: {
        id:      current_repository.global_relay_id,
        path:    path_string,
        refName: tree_name,
      })
    rescue GitRPC::Timeout => boom
      Failbot.report(boom)
      return render_optional_error_file(500)
    end

    return render_404 if data.repository.latest_commit.nil?

    respond_to do |format|
      format.html do
        render partial: "commit/commit_tease",
          locals: {
            commit: data.repository.latest_commit,
            show_checks_status: GitHub.actions_enabled? && conditional_access_checks_satisfied?,
          }
      end
    end
  end

  def fork
    if !params[:organization].blank? # forking to an org
      org = Organization.find_by_login(params[:organization])
      unless required_external_identity_session_present?(target: org)
        render_external_identity_session_required(target: org)
        return
      end
    end

    forked, reason, errors = current_repository.fork(org: org, forker: current_user)

    if forked
      GitHub.dogstats.increment("repo", tags: ["action:fork", "valid:true"])
      redirect_to clean_repository_path(forked)
    else
      GitHub.dogstats.increment "repo", tags: ["action:fork", "valid:false", "error:#{reason}"]
      Failbot.report_user_error Repository::ForkFailure.new,
        organization: params[:organization], repo: current_repository,
        reason: reason, errors: errors

      flash[:error] = RepositoryForker.message_from_reason(reason, errors)

      redirect_to current_repository.permalink
    end
  end

  # The "Where do you want to fork this to?" dialog that pops up when you hit
  # fork. This is loaded in via Ajax when the Fork button is clicked because
  # calculating the fork existence and permissions for each organization on each
  # repository page hit is very expensive.
  def fork_select
    if logged_in?
      respond_to do |format|
        format.html do
          if request.xhr? || params[:fragment].to_i == 1
            render partial: "repositories/fork_select_partial", layout: false
          else
            if current_repository.forking_disabled?
              flash[:error] = "Forking is disabled for this repository."
              redirect_to repository_url(current_repository)
            elsif GitHub.flipper[:disallow_nested_private_forks].enabled?(current_user) && current_repository.private_fork?
              flash[:error] = "Cannot fork a private fork"
              redirect_to repository_url(current_repository)
            else
              render "repositories/fork_select"
            end
          end
        end
      end
    else
      redirect_to_login(url_for)
    end
  end

  def tarball
    current_repository ? codeload("legacy.tar.gz".dup) : render_404
  end

  def zipball
    current_repository ? codeload("legacy.zip".dup) : render_404
  end

  def archive
    current_repository ? codeload(params[:_format]) : render_404
  end

  def codeload(type)
    unless meets_oauth_application_policy_for_current_repo?
      render_404
      return
    end

    # Only tree_name is used, params[:path] is ignored.
    # It's useful to control the filename of downloads in clients that ignore the
    # Content-Disposition header.
    #
    #  https://github.com/{OWNER}/{PROJECT}/archive/{COMMIT}/{FILENAME}.tar.gz
    ac = current_repository.archive_command(branch_to_codeload, type)
    url = ac.codeload_url(current_user)
    expires_in 0.seconds # make sure nothing caches the redirect url (e.g. heroku)
    redirect_to url
  end

  def branch_to_codeload
    if GitHub.flipper[:missing_archive_branch_redirect].enabled?(current_repository.owner)
      if tree_name != "master" || current_repository.valid_branch?(tree_name)
        tree_name
      else
        current_repository.default_branch
      end
    else
      tree_name
    end
  end

  def star
    authorization = ContentAuthorizer.authorize(current_user, :star, :create, starrable: current_repository)

    if authorization.failed?
      if request.xhr?
        render status: :unprocessable_entity, json: { error: authorization.error_messages }
      else
        flash[:error] = authorization.error_messages
        redirect_to :back
      end
    else
      current_user.star(current_repository, context: params[:context])
      GitHub.dogstats.increment("stars.star")

      respond_to do |wants|
        wants.json { render json: { count: star_count } }
        wants.html { redirect_to :back }
      end
    end
  end

  def unstar
    # Users should be able to unstar a disabled repo so we skip the gatekeeper
    # before filter and do it ourselves.
    if current_repository.nil? || !current_repository.pullable_by?(current_user)
      return render_404
    end

    current_user.unstar(current_repository, context: params[:context])
    GitHub.dogstats.increment("star", tags: ["action:unstar"])

    respond_to do |wants|
      wants.json { render json: { count: star_count } }
      wants.html { redirect_to :back }
    end
  end

  # TODO: this is only used for _recently_touched_branches_list, it could be renamed appropriately
  def show_partial
    partial = params[:partial]
    return head :not_found unless valid_partial?(partial)

    respond_to do |format|
      format.html do
        render partial: partial, layout: false # rubocop:disable GitHub/RailsControllerRenderLiteral
      end
    end
  end

private

  def archive_action?
    %w{archive tarball zipball}.include?(action_name)
  end

  def authentication_methods
    set_path_and_name
    if user = super
      GitHub.dogstats.increment "auth", tags: ["action:archive", "type:session"]
    elsif archive_action? && user = login_from_authorization_header_auth
      type = if user.using_oauth_application?
        "token"
      elsif user.using_oauth?
        "personal_access_token"
      else
        "password"
      end
      GitHub.dogstats.increment "auth", tags: ["action:archive", "type:#{type}"]
    end
    user
  end

  def meets_oauth_application_policy_for_current_repo?
    OauthApplicationPolicy::HttpRequest.new(
      repository: current_repository,
      user: current_user,
      request: request,
    ).satisfied?
  end

  def content_authorization_required
    if current_repository.public? && !logged_in?
      redirect_to_login(request.url)
    elsif !rendering_modal_fork_selector?
      authorize_content(:repo)
    end
  end

  def rendering_modal_fork_selector?
    # Block the full-page fork_select screen, but allow the modal version.
    action_name == "fork_select" && (request.xhr? || params[:fragment].to_i == 1)
  end

  # Bypassing conditional access for show_partial also requires that
  # the partial is exempted.
  def conditional_access_exempt?
    super && (action_name != "show_partial" || conditional_access_exempt_partial?(params[:partial]))
  end

  # Bypass conditional access requirements for the specified actions if the
  # current action is exempt.
  def require_conditional_access_checks?
    return false if conditional_access_exempt?
    super
  end

  # Overrides before filter `require_active_external_identity_session` from
  # controllers/application_controller/external_sessions_dependency.rb
  #
  # For legacy routes that allow basic authentication we can't support SAML SSO
  # as there is no associated user_session from which to find external
  # identity sessions.
  def require_active_external_identity_session
    return true unless organization_saml_enforced?

    case authorization_header_auth_status
    when :active_credential_authorization; true
    when :no_active_credential_authorization; head :forbidden
    else
      # Fallback to the default implementation when the current user is not
      # using basic auth.
      super
    end
  end

  def organization_saml_enforced?
    target = safe_target_for_conditional_access
    return false if target == :no_target_for_conditional_access

    policy = saml_enforcement_policy_for(target)
    return false if policy.nil?
    policy.enforced?
  end

  def authorization_header_auth_status
    return unless authorization_header_authed?

    if active_credential_authorization?
      :active_credential_authorization
    else
      :no_active_credential_authorization
    end
  end

  def active_credential_authorization?
    if logged_in? && current_user.using_oauth?
      target = safe_target_for_conditional_access
      return false if target == :no_target_for_conditional_access
      # Using personal access token
      credential_authorization = Organization::CredentialAuthorization.by_organization_credential(
        organization: target,
        credential: current_user.oauth_access,
      ).first

      credential_authorization && credential_authorization.active?
    end
  end

  def star_count
    number_with_delimiter(current_repository.stargazer_count)
  end

  def route_supports_advisory_workspaces?
    !["fork", "fork_select"].include?(action_name)
  end

  def employee?
    logged_in? && current_user.employee?
  end
end
