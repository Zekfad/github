# frozen_string_literal: true

class ProjectIssuesController < AbstractRepositoryController

  def new
    issue = current_repository.issues.build

    project_ids = issue_project_ids(params.to_unsafe_h).each_with_object([]) do |(project_id, should_add), result|
      result << project_id if should_add == "on"
    end
    projects = issue.potential_projects_for(current_user, ids: project_ids)
    issue.project_ids = projects.map(&:id)

    respond_to do |format|
      format.html do
        render partial: "issues/sidebar/new/projects",
               locals: { issue: issue, projects: projects, deferred_content: false }
      end
    end
  end

  def update
    issue_params = update_params
    project_ids = issue_project_ids issue_params

    issue = current_repository.issues.find_by_number(issue_params[:issue_number])
    old_issue_project_ids = issue.project_ids
    issue_project_ids_to_remove = []
    projects = issue.potential_projects_for(current_user, ids: project_ids.keys)

    projects.each do |project|
      if project_ids[project.id.to_s] == "on"
        next if old_issue_project_ids.include?(project.id)
        card = project.cards.build(creator: current_user)
        card.set_content_from(actor: current_user, params: { content_type: "Issue", content_id: issue.id })
        card.save!
      else
        issue_project_ids_to_remove << project.id
      end
    end

    if issue_project_ids_to_remove.any?
      ProjectCard.where(project_id: issue_project_ids_to_remove).for_content(issue).destroy_all
    end

    track_issue_edits_from_project_board(edited_fields: ["projects"])

    respond_to do |format|
      format.html do
        render partial: "issues/sidebar/show/projects",
               locals: { issue: issue, projects: projects }
      end
    end
  end

  private

  def update_params
    params.to_unsafe_h.with_indifferent_access.slice :issue_project_ids, :issue_number
  end

  def issue_project_ids(parameters)
    ids = parameters[:issue_project_ids]
    return ids if ids.is_a?(Hash)

    {}
  end
end
