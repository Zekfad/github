# frozen_string_literal: true

class MarketplaceListingPlansController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required
  before_action :render_404, unless: :logged_in?
  before_action :sudo_filter

  layout "marketplace"

  IndexQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!) {
      marketplaceListing(slug: $listingSlug) {
        viewerCanEdit
      }
      ...Views::MarketplaceListingPlans::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: {
      listingSlug: params[:listing_slug],
    })

    return render_404 unless data.marketplace_listing && data.marketplace_listing.viewer_can_edit?

    respond_to do |format|
      format.html do
        render "marketplace_listing_plans/index", locals: { data: data }
      end
    end
  end


  NewQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $id: ID!, $planSelected: Boolean!) {
      marketplaceListing(slug: $listingSlug) {
        viewerCanEdit
      }
      ...Views::MarketplaceListingPlans::New::Root
    }
  GRAPHQL

  def new
    data = platform_execute(NewQuery, variables: {
      listingSlug: params[:listing_slug],
      planSelected: false,
      id: "",
    })

    return render_404 unless data.marketplace_listing && data.marketplace_listing.viewer_can_edit?

    respond_to do |format|
      format.html do
        render "marketplace_listing_plans/new", locals: { data: data }
      end
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $id: ID!, $planSelected: Boolean!) {
      marketplaceListing(slug: $listingSlug) {
        databaseId
        viewerCanEdit
      }
      listingPlan: node(id: $id) {
        ... on MarketplaceListingPlan {
          listing {
            databaseId
          }
        }
      }
      ...Views::MarketplaceListingPlans::Show::Root
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: {
      id: params[:id],
      listingSlug: params[:listing_slug],
      planSelected: true,
    })

    return render_404 unless data.marketplace_listing && data.marketplace_listing.viewer_can_edit?
    return render_404 unless data.listing_plan && data.listing_plan.listing&.database_id == data.marketplace_listing.database_id

    respond_to do |format|
      format.html do
        render "marketplace_listing_plans/show", locals: { data: data }
      end
    end
  end

  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: CreateMarketplaceListingPlanInput!) {
      createMarketplaceListingPlan(input: $input) {
        marketplaceListingPlan {
          id
        }
      }
    }
  GRAPHQL

  CreateBulletQuery = parse_query <<-'GRAPHQL'
    mutation($input: CreateMarketplaceListingPlanBulletInput!) {
      createMarketplaceListingPlanBullet(input: $input) {
        marketplaceListingPlanBullet
      }
    }
  GRAPHQL

  def create
    data = platform_execute(CreateQuery, variables: {
      input: computed_plan_params.merge(
        slug: params[:listing_slug],
        bulletValues: Array(params[:bullet_values]).select(&:present?),
      ),
    })

    if data.errors.any?
      error_type = data.errors.details[:createMarketplaceListingPlan]&.first["type"]
      return head :not_found if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to new_marketplace_listing_plan_path(params[:listing_slug])
    end

    id = data.create_marketplace_listing_plan.marketplace_listing_plan.id

    redirect_to marketplace_listing_plan_path(params[:listing_slug], id)
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateMarketplaceListingPlanInput!) {
      updateMarketplaceListingPlan(input: $input) {
        marketplaceListingPlan {
          id
          bullets(first: 4) {
            edges {
              node {
                id
              }
            }
          }
        }
      }
    }
  GRAPHQL

  UpdateBulletQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateMarketplaceListingPlanBulletInput!) {
      updateMarketplaceListingPlanBullet(input: $input) {
        marketplaceListingPlanBullet
      }
    }
  GRAPHQL

  DeleteBulletQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteMarketplaceListingPlanBulletInput!) {
      deleteMarketplaceListingPlanBullet(input: $input) {
        marketplaceListingPlan
      }
    }
  GRAPHQL

  def update
    data = platform_execute(UpdateQuery, variables: { input: computed_plan_params.merge(id: params[:id]) })

    if data.errors.any?
      error_type = data.errors.details[:updateMarketplaceListingPlan]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to marketplace_listing_plan_path(params[:listing_slug],
                                                       params[:id])
    end

    updated_plan = data.update_marketplace_listing_plan.marketplace_listing_plan
    id = updated_plan.id
    bullet_ids = updated_plan.bullets.edges.map { |edge| edge.node.id }

    unless create_bullets(id) && update_bullets(bullet_ids)
      flash[:error] = "Could not save all bullet points for your new plan."
    end

    redirect_to marketplace_listing_plan_path(params[:listing_slug], id)
  end

  PublishQuery = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      publishMarketplaceListingPlan(input: { id: $id }) {
        marketplaceListingPlan
      }
    }
  GRAPHQL

  def publish
    data = platform_execute(PublishQuery, variables: { id: params[:id] })

    if data.errors.any?
      error_type = data.errors.details[:publishMarketplaceListingPlan]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
      flash[:error] = data.errors.messages.values.join(", ")
    else
      flash[:notice] = "Your plan has been published."
    end

    redirect_to marketplace_listing_plan_path(params[:listing_slug], params[:id])
  end

  RetireQuery = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      retireMarketplaceListingPlan(input: { id: $id }) {
        marketplaceListingPlan {
          id
          name
        }
      }
    }
  GRAPHQL

  def retire
    data = platform_execute(RetireQuery, variables: { id: params[:id] })

    if data.errors.any?
      error_type = data.errors.details[:retireMarketplaceListingPlan]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
    else
      flash[:notice] = "Your plan has been retired."
    end

    redirect_to marketplace_listing_plan_path(params[:listing_slug], params[:id])
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteMarketplaceListingPlanInput!) {
      deleteMarketplaceListingPlan(input: $input) {
        marketplaceListing {
          slug
        }
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DestroyQuery, variables: { input: { id: params[:id] } })

    if data.errors.any?
      error_type = data.errors.details[:deleteMarketplaceListingPlan]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")

      flash[:error] = message
      return redirect_to(marketplace_listing_plan_path(params[:listing_slug]))
    end

    listing = data.delete_marketplace_listing_plan.marketplace_listing
    redirect_to marketplace_listing_plans_path(listing.slug)
  end

  private

  def plan_params
    params.require(:marketplace_listing_plan).permit %i[name description unitName]
  end

  def computed_plan_params
    plan_params.to_h.merge "priceModel" => params[:price_model],
                           "monthlyPriceInCents" => params[:monthly_price_in_dollars].to_i * 100,
                           "yearlyPriceInCents" => params[:yearly_price_in_dollars].to_i * 100,
                           "hasFreeTrial" => params[:has_free_trial].present?,
                           "forAccountType" => params[:for_account_type]
  end

  def create_bullets(plan_id)
    values = Array(params[:bullet_values]).select(&:present?)
    return true if values.empty?

    results = values.map do |value|
      data = platform_execute(CreateBulletQuery, variables: {
        input: { value: value, planId: plan_id },
      })

      data.errors.blank?
    end

    results.all?
  end

  def update_bullets(bullet_ids)
    values_by_id = bullet_ids.map { |id| [id, params["bullet_values_#{id}"]] }.to_h
    return true if values_by_id.empty?

    results = values_by_id.map do |id, value|
      data = if value.present?
        platform_execute(UpdateBulletQuery, variables: { input: { value: value, id: id } })
      else
        platform_execute(DeleteBulletQuery, variables: { input: { id: id } })
      end

      data.errors.blank?
    end

    results.all?
  end

  def target_for_conditional_access
    listing = Marketplace::Listing.find_by_slug(params[:listing_slug])
    return :no_target_for_conditional_access unless listing
    listing.owner
  end
end
