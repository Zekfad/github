# frozen_string_literal: true

class CollectionsController < ApplicationController
  areas_of_responsibility :explore

  statsd_tag_actions only: [:index, :show]

  YOUTUBE_HOST = "www.youtube.com"
  VIMEO_HOST = "player.vimeo.com"

  before_action :dotcom_required
  before_action :add_csp_exceptions, only: [:show]
  CSP_EXCEPTIONS = { frame_src: [YOUTUBE_HOST, VIMEO_HOST] }

  layout "site"

  def index
    locals = {
      featured_collections: ExploreCollection.featured_and_shuffled,
      collections: ExploreCollection.paginate(page: params.fetch(:page, 1)),
    }

    if request.xhr?
      render partial: "collections/featured_collections", locals: locals
    else
      render "collections/index", locals: locals
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query($slug: String!, $perPage: Int!, $after: String) {
      collection(slug: $slug)
      ...Views::Collections::Show::Root
    }
  GRAPHQL

  ShowXhrQuery = parse_query <<-'GRAPHQL'
    query($slug: String!, $perPage: Int!, $after: String) {
      collection(slug: $slug) {
        ...Views::Collections::CollectionItems::ExploreCollection
      }
    }
  GRAPHQL

  def show
    variables = { slug: params[:slug], after: params[:after], perPage: 20 }
    if request.xhr?
      data = platform_execute(ShowXhrQuery, variables: variables)
      return render_404 unless data.collection

      respond_to do |format|
        format.html do
          render partial: "collections/collection_items", locals: { collection: data.collection }
        end
      end
    else
      data = platform_execute(ShowQuery, variables: variables)
      return render_404 unless data.collection
      render "collections/show", locals: { data: data }
    end
  end

  private

  def require_conditional_access_checks?
    false
  end
end
