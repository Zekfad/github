# frozen_string_literal: true

class CommentPreviewController <  ApplicationController
  include OrganizationsHelper

  # Enforcement is required inline because requests are not consistently scoped
  # to an organization.
  skip_before_action :perform_conditional_access_checks, only: %w(show)

  include TextHelper

  # NOTE: <KLAXON>This behavior is shared between dotcom and gist.</KLAXON>
  #       If you change this, or it's route, please review the Gist routes as well
  #       as testing comment previewing in Gist on gist-garage or production.
  def show
    markdown = params[:text]
    context = default_html_filter_context.merge(current_user: current_user)

    html = GitHub.dogstats.time("markdown", tags: ["action:preview"]) do
      context[:entity] = entity_for_context
      context[:organization] = org_for_context
      context[:original_lines] = original_lines
      context[:path] = params[:path] if params[:path]
      context[:line_number] = params[:line_number].to_i if params[:line_number]
      context[:line_number] = params[:lineNumber].to_i if params[:lineNumber]
      context[:start_line_number] = params[:start_line_number].to_i if params[:start_line_number]

      if params[:markdown_unsupported] && params[:markdown_unsupported] == "true"
        GitHub::HTML::EmailPipeline.to_html(markdown, context).gsub("\n", "<br>\n").html_safe # rubocop:disable Rails/OutputSafety
      else
        GitHub::Goomba::MarkdownPipeline.to_html(markdown, context)
      end
    end

    render html: html
  end

  private

  # Returns the requested entity (repository) if the user can access it.
  def entity_for_context
    repository_id = params[:repository].presence
    return unless logged_in? && repository_id

    repo = Repository.find_by_id(repository_id)
    return unless repo && repo.pullable_by?(current_user)

    # Require allowed IP for org-owned repositories with IP allow list enforced
    return if repo.owner.organization? && !ip_whitelisting_policy_satisfied?(owner: repo.owner)

    # Require active SAML session for org-owned repositories with SAML SSO enabled
    return if repo.owner.organization? && !required_external_identity_session_present?(target: repo.owner)

    repo
  end

  # Returns the requested organization if the user can access it.
  def org_for_context
    return unless org = current_organization

    # Require allowed IP for org-owned repositories with IP allow list enforced
    return if !ip_whitelisting_policy_satisfied?(owner: org)

    # Require active SAML session for org-owned repositories with SAML SSO enabled
    return if !required_external_identity_session_present?(target: org)

    org
  end

  def pull_request
    return @pull_request if defined?(@pull_request)

    if entity_for_context && params[:subject_type] == "PullRequest"
      @pull_request = entity_for_context.issues.find_by_number(params[:subject])&.pull_request
    elsif entity_for_context && params[:pull_request].present?
      @pull_request = entity_for_context.pull_requests.find_by(id: params[:pull_request])
    end
  end

  def original_lines
    return params[:original_line] if params[:original_line]
    return params[:originalLine] if params[:originalLine]

    return unless params[:comment_id] || (params[:path] && params[:line_number] && commits_present?)

    if pull_request
      if params[:comment_id].present?
        comment = pull_request.review_comments.where(id: params[:comment_id]).first
        PullRequestReviewComment::SuggestedChangeSelection.from_persisted_comment(comment).lines
      else
        PullRequestReviewComment::SuggestedChangeSelection.new(
          pull: pull_request,
          path: params[:path],
          start_commit_oid: params[:start_commit_oid],
          end_commit_oid: params[:end_commit_oid],
          base_commit_oid: params[:base_commit_oid],
          start_line: params[:start_line_number]&.to_i,
          start_side: params[:start_side],
          line: params[:line_number]&.to_i,
          side: params[:side],
        ).lines
      end
    end
  end

  def commits_present?
    [params[:start_commit_oid], params[:end_commit_oid], params[:base_commit_oid]].all?(&:present?)
  end
end
