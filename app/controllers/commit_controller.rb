# frozen_string_literal: true

class CommitController < GitContentController
  statsd_tag_actions only: :show

  include ShowPartial
  include GitHub::Encoding
  include ViewModelHelper
  include DiscussionPreloadHelper
  include ControllerMethods::Diffs
  include GitHub::RateLimitedRequest

  rate_limit_requests only: [:show], max: 5000

  helper :diff

  layout :repository_layout

  COMMENTS_PER_PAGE = 100

  ShowCommitQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Commit::Show::Commit
      }
    }
  GRAPHQL

  def show
    return render_404 if current_commit.nil? || params[:name].blank?
    mark_thread_as_read current_commit

    # 404 if scoped path isn't in the diff
    return render_404 if path_string.present? && commit_tree_diff_page.nil?

    diff_options = {
      use_summary: true,
      ignore_whitespace: ignore_whitespace?,
      paths: path_string.present? ? [path_string] : [],
    }
    current_commit.set_diff_options(diff_options)

    diff_options[:top_only] = true
    current_commit.set_diff_options(diff_options)

    GitHub.dogstats.time("diff.load.initial", tags: dogstats_request_tags) do
      current_commit.init_diff.apply_auto_load_single_entry_limits!
      current_commit.diff # loads diff
    end

    @diffs = current_commit.diff
    # Indicates that this commit was in the commit cache, but isn't
    # actually present in the repository.
    return render_404 if @diffs.missing_commits?

    respond_to do |wants|
      wants.html do
        comments = CommitComment.for_display(current_user, current_commit, current_repository)
        inline_comments = comments.inline
        last_discussion_comments = fetch_page(comments.discussion)
        file_list_view_comments = file_list_view&.threads&.flat_map(&:comments) || []

        preload_discussion_group_data(
          inline_comments +
          last_discussion_comments +
          file_list_view_comments,
        )

        current_commit.comments = last_discussion_comments
        current_commit.comment_count = comments.discussion.count

        view_params = {
          commit: current_commit,
          file_list_view: file_list_view,
          show_checks_status: GitHub.actions_enabled?,
        }
        commit_node = platform_execute(ShowCommitQuery, variables: { id: current_commit.global_relay_id }).node

        # We can run into race conditions where the commit exists when we load `current_commit` above,
        # but it's been force pushed away by the time we look it up in GraphQL-land.
        # Return 404 in this case.
        return render_404 unless commit_node

        view_params[:commit_node] = commit_node
        render "commit/show", locals: view_params
      end
      wants.diff  { diff }
      wants.patch { diff(as_patch: true) }
      wants.all   { head :not_acceptable }
    end
  end

  ShowCommitTimeoutQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Commit::FullCommitDetail
      }
    }
  GRAPHQL

  rescue_from_timeout only: [:show] do |boom|
    commit_node = platform_execute(ShowCommitTimeoutQuery, variables: {
      id: current_commit.global_relay_id,
    }).node

    render "commit/timeout", locals: { commit: current_commit, commit_node: commit_node }
  end

  def branch_commits
    return head :not_found unless current_commit

    respond_to do |format|
      format.html do
        render_partial_view "commit/branch_commits", Commits::BranchListView,
          commit: current_commit
      end
    end
  end

  def show_partial
    partial = params[:partial]
    return head :not_found unless valid_partial?(partial)
    return head :not_found unless current_commit
    locals = { commit: current_commit }

    Commit.prefill_combined_statuses([current_commit], current_repository)

    if partial == "commit/timeline_marker"
      comments = CommitComment.for_display(current_user, current_commit, current_repository)
      if params[:before_comment_id]
        current_commit.comments = fetch_page(comments.discussion, params[:before_comment_id])
      else
        current_commit.comments = comments
          .discussion
          .where("created_at > ?", discussion_last_modified_at)
      end
      preload_discussion_group_data(current_commit.comments)
    end

    respond_to do |format|
      format.html do
        render partial: partial, object: current_commit, layout: false, locals: locals # rubocop:disable GitHub/RailsControllerRenderLiteral
      end
    end
  end

  def lock
    current_commit.lock(current_user)
    redirect_to :back
  end

  def unlock
    current_commit.unlock(current_user)
    redirect_to :back
  end

  private

  PATCH_TIMEOUT_SECONDS = 3

  def diff(as_patch: false)
    timeout_client_error do
      set_request_category! "raw"

      full_index = !!params[:full_index]
      diff = with_timeout(current_repository.rpc, PATCH_TIMEOUT_SECONDS) do
        if as_patch
          current_repository.rpc.native_patch_text(current_commit.oid, full_index: full_index)
        else
          current_repository.rpc.native_diff_text(current_commit.oid, full_index: full_index)
        end
      end

      # Security fix. See #36923
      render_error_for_pdf_headers(diff)

      content_type = detect_content_type(diff)

      render(content_type: content_type, body: diff) unless performed?
    end
  end

  def with_timeout(rpc, seconds)
    old_timeout = rpc.options[:timeout]
    rpc.options[:timeout] = seconds
    yield
  ensure
    rpc.options[:timeout] = old_timeout
  end

  def detect_content_type(data)
    content_type = "text/plain"
    encoding     = "utf-8"

    if detected = guess_encoding(data) and detected[:type] == :text
      encoding = detected[:encoding].downcase
    end

    "#{content_type}; charset=#{encoding}"
  end

  def fetch_page(comments, before_comment_id = nil)
    if before_comment_id
      comments = comments.where("commit_comments.id < ?", before_comment_id)
    end

    ids = comments.pluck(:id)
    current_commit.can_load_more_comments = ids.count > COMMENTS_PER_PAGE

    comments.where(id: ids.last(COMMENTS_PER_PAGE))
  end

  def file_list_view
    return nil if params[:toc]
    @file_list_view ||= Diff::FileListView.new(
      commit: current_commit,
      diffs: @diffs,
      params: params,
      current_user: current_user,
      commentable: view_context.issue_thread_commentable?,
      progressive: true,
    )
  end

  def commit_tree_diff
    @commit_tree_diff ||= current_repository.rpc.read_tree_diff(current_commit.oid)
  end
  helper_method :commit_tree_diff

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def commit_tree_diff_page
    @commit_tree_diff_page ||= begin
      filenames = commit_tree_diff.map { |c| c["old_file"]["path"] }
      if index = filenames.index(path_string)
        index + 1
      end
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  helper_method :commit_tree_diff_page

  def route_supports_advisory_workspaces?
    true
  end
end
