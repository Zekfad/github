# frozen_string_literal: true

class NoodleController < AbstractRepositoryController
  areas_of_responsibility :code_collab

  before_action :add_csp_exceptions, only: [:workspace]
  before_action :require_active_staff_external_identity_session, only: [:workspace]

  def workspace
    return render_404 unless collaborative_editing_enabled?

    AutomaticAppInstallation.trigger(
       type: :editor_opened,
       originator: current_repository,
       actor: current_user,
     )

    render_template_view  "noodle/workspace",
                          Noodle::WorkspaceView,
                          { owner: params[:user_id],
                            repo: params[:repository],
                            name: params[:name],
                            path: path_string,
                            tree_name: tree_name,
                            current_repository: current_repository,
                          },
                          layout: false
  end

private

  # If rendering Noodle as a staff member on a repo not owned by github, we must
  # require an active external identity session for the github org anyway. This
  # would not usually be necessary, but GitHub staff require an SSO session
  # before completing any OAuth flow. Because Noodle embeds an OAuth flow in an
  # iframe, we cannot reliably redirect to an external identity provider in case
  # they set a frame-ancestors CSP directive (Okta does this).
  # See https://github.com/github/noodle/issues/774 for more details
  def require_active_staff_external_identity_session
    return unless current_user&.employee?
    return unless collaborative_editing_enabled?

    if github_org = Organization.find_by(id: GitHub.trusted_apps_owner_id)
      unless required_external_identity_session_present?(target: github_org)
        render_external_identity_session_required(target: github_org)
        return
      end
    end
  end

  def target_for_conditional_access
    # this will fetch either a user or an organization since Organization inherits from User
    # the safe_target_for_conditional_access method will handle type checking to return the relevant target
    User.find_by_login(params[:user_id] || params[:organization_id])
  end

  def add_csp_exceptions
    return if !collaborative_editing_enabled?

    SecureHeaders.append_content_security_policy_directives(
      request,
      frame_src: [
        GitHub.noodle_editor_host_url,
        SecureHeaders::CSP::SELF,
      ],
    )

  end

end
