# frozen_string_literal: true

class UserContributionsController < ApplicationController
  areas_of_responsibility :user_profile

  statsd_tag_actions only: :show

  include UserContributionsHelper

  skip_before_action :perform_conditional_access_checks

  before_action :ensure_user_exists

  ShowQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $selected_organization_login: String!,
      $organization_selected: Boolean!,
      $selected_organization_id: ID,
      $activity_overview_enabled: Boolean!,
      $logged_in: Boolean!,
      $show_teams: Boolean!,
      $from: DateTime,
      $to: DateTime,
      $is_viewer: Boolean!
    ) {
      ...Views::Users::Tabs::ActivityOverview::RootFragment @include(if: $activity_overview_enabled)

      node(id: $id) {
        ...Views::Users::ContributionGraph::User
      }

      organization(login: $selected_organization_login) @include(if: $organization_selected) {
        databaseId
        ...Views::Users::Tabs::YearlyContributions::Organization
      }
    }
  GRAPHQL

  def show
    variables = activity_overview_variables.merge(
      id: this_user.global_relay_id,
      activity_overview_enabled: activity_overview_enabled?,
      is_viewer: this_user == current_user,
    )
    to_iso8601 = parse_graphql_datetime(params[:to]) if params[:to].present?
    if to_iso8601
      ending_on = Time.zone.parse(to_iso8601)
      time_range = Contribution::Calendar.time_range_ending_on(ending_on)
      variables[:from] = time_range.begin.iso8601
      variables[:to] = time_range.end.iso8601
    end

    data = platform_execute(ShowQuery, variables: variables)

    respond_to do |format|
      format.html do
        options = params.slice(:from, :to).permit(:from, :to).to_h
          .merge(current_user: current_user, organization_id: data.organization&.database_id)

        if data.errors.any?
          render_404
        else
          render partial: "users/tabs/yearly_contributions",
                 locals: { data: data, user: data.node, org: data.organization }
        end
      end
    end
  end

  SampleQuery = parse_query <<-'GRAPHQL'
    query($login: String!) {
      user(login: $login) {
        contributionsCollection {
          contributionCalendar(sample: true) {
            ...Views::Users::Contributions::ContributionCalendar
          }
        }
      }
    }
  GRAPHQL

  def sample
    calendar = Contribution::CalendarSample.new
    data = platform_execute(SampleQuery, variables: { login: params[:user_id] })
    calendar = data.user.contributions_collection.contribution_calendar

    respond_to do |format|
      format.html do
        render partial: "users/contributions",
               locals: { calendar: calendar, activity_overview_enabled: activity_overview_enabled? }
      end
    end
  end

  private

  def ensure_user_exists
    render_404 unless this_user && this_user.user?
  end
end
