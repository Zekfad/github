# frozen_string_literal: true

class Gists::CommentsController < Gists::ApplicationController
  include ShowPartial
  include DiscussionPreloadHelper

  before_action :gist_required

  def create
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)
    comment_body = params[:comment][:body]
    comment = this_gist.comments.build(body: comment_body, user: current_user)

    # Don't allow blocked users to comment on gists
    if comment.author_blocking?(current_user)
      valid = false
      comment.errors.add(:message, "cannot be saved")
    elsif valid = comment.save
        flash[:ga_gist_comment] = true # Track GA event on next page load

        GitHub.dogstats.increment("gist.web.comment", \
                                  tags: ["visibility:#{this_gist.visibility}",
                                         "ownership:#{this_gist.ownership}"])
    end

    preload_discussion_group_data(this_gist.comments)

    respond_to do |format|
      format.html do
        if valid
          redirect_to user_gist_path(this_gist.user_param, this_gist, anchor: comment.anchor)
        else
          flash[:error] = comment.errors.full_messages.to_sentence
          redirect_to :back
        end
      end

      format.json do
        if valid
          render_immediate_partials(this_gist, partials: { timeline_marker: {}, form_actions: {} })
        else
          if GitHub.rails_6_0?
            errors = comment.errors.map { |attr, msg| msg }
          else
            errors = comment.errors.map(&:message)
          end
          render json: { errors: errors }, status: :unprocessable_entity
        end
      end
    end
  end

  def update
    comment = this_comment
    comment_body = params.fetch(:gist_comment, {}).fetch(:body, nil)
    valid = false

    if comment.author_blocking?(current_user)
      valid = false
      comment.errors.add(:message, "cannot be saved")
    elsif this_comment.adminable_by?(current_user) && comment_body.present?
      if operation = TaskListOperation.from(params[:task_list_operation])
        text = operation.call(comment.body)
        comment.update_body(text, current_user) if text
        valid = true
      else
        valid = comment.update_body(comment_body, current_user)
      end
    end

    preload_discussion_group_data(this_gist.comments, mobile: show_mobile_view?)

    respond_to do |wants|
      wants.html do
        redirect_to user_gist_path(this_gist.user_param, this_gist)
      end

      wants.json do
        if valid
          render json: {
            "source" => comment.body,
            "body" => comment.body_html,
            "newBodyVersion" => comment.body_version,
            "editUrl" => show_comment_edit_history_path(comment.global_relay_id),
          }
        else
          render json: { errors: comment.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  end

  def destroy
    if this_comment.adminable_by?(current_user)
      this_comment.destroy
    end

    if request.xhr?
      head :ok
    else
      redirect_to user_gist_path(this_gist.user_param, this_gist)
    end
  end

  private

  # A simple before filter to ensure the gist could be found.
  def gist_required
    this_gist
  end

  def this_comment
    @comment ||= this_gist.comments.find(params[:comment_id])
  end
end
