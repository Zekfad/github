# frozen_string_literal: true

class Settings::SecureDevelopmentController < ApplicationController
  areas_of_responsibility :dependabot

  skip_before_action :perform_conditional_access_checks, only: %w(
    update_automated_security_fixes_opt_out
  )

  before_action :login_required

  def update_automated_security_fixes_opt_out
    if params[:automated_security_fixes_opt_out] == "on"
      current_user.disable_vulnerability_updates(actor: current_user)

      ::Dependabot::DisableOwnerRepositoriesJob.perform_later(current_user.id, current_user.id)

      flash[:notice] = "Opted out of Dependabot security updates."
    else
      current_user.enable_vulnerability_updates(actor: current_user)
      flash[:notice] = "Opted in to Dependabot security updates."
    end
    redirect_to settings_user_security_path
  end
end
