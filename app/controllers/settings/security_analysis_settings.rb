# frozen_string_literal: true

module Settings::SecurityAnalysisSettings
  extend ActiveSupport::Concern

  def update_security_settings(owner, params, current_user = owner)
    # Dependency Graph
    if params[:dependency_graph] == "enable_all"
      settings_update_job_caller(owner, :dependency_graph_enable_all, current_user)
      publish_metrics("dependency_graph.enable", owner, current_user)
    elsif params[:dependency_graph] == "disable_all"
      settings_update_job_caller(owner, :dependency_graph_disable_all, current_user)
      publish_metrics("dependency_graph.disable", owner, current_user)
    end

    if params[:dependency_graph_new_repos] == "enabled"
      owner.enable_dependency_graph_for_new_repos(actor: current_user)
      publish_metrics("dependency_graph_new_repos.enable", owner, current_user)
    elsif params[:dependency_graph_new_repos] == "disabled"
      owner.disable_dependency_graph_for_new_repos(actor: current_user)
      publish_metrics("dependency_graph_new_repos.disable", owner, current_user)
    end

    # Dependabot Alerts
    if params[:security_alerts] == "enable_all"
      settings_update_job_caller(owner, :security_alerts_enable_all, current_user)
      publish_metrics("dependabot_alerts.enable", owner, current_user)
    elsif params[:security_alerts] == "disable_all"
      settings_update_job_caller(owner, :security_alerts_disable_all, current_user)
      publish_metrics("dependabot_alerts.disable", owner, current_user)
    end

    if params[:security_alerts_new_repos] == "enabled"
      owner.enable_security_alerts_for_new_repos(actor: current_user)
      publish_metrics("dependabot_alerts_new_repos.enable", owner, current_user)
    elsif params[:security_alerts_new_repos] == "disabled"
      owner.disable_security_alerts_for_new_repos(actor: current_user)
      publish_metrics("dependabot_alerts_new_repos.disable", owner, current_user)
    end

    # Dependabot security updates
    if params[:vulnerability_updates] == "enable_all"
      settings_update_job_caller(owner, :vulnerability_updates_enable_all, current_user)
      publish_metrics("dependabot_security_updates.enable", owner, current_user)
    elsif params[:vulnerability_updates] == "disable_all"
      settings_update_job_caller(owner, :vulnerability_updates_disable_all, current_user)
      publish_metrics("dependabot_security_updates.disable", owner, current_user)
    end

    if params[:vulnerability_updates_new_repos] == "enabled"
      owner.enable_vulnerability_updates_for_new_repos(actor: current_user)
      publish_metrics("dependabot_security_updates_new_repos.enable", owner, current_user)
    elsif params[:vulnerability_updates_new_repos] == "disabled"
      owner.disable_vulnerability_updates_for_new_repos(actor: current_user)
      publish_metrics("dependabot_security_updates_new_repos.disable", owner, current_user)
    end

    # Secret Scanning
    if params[:secret_scanning] == "enable_all"
      settings_update_job_caller(owner, :secret_scanning_enable_all, current_user)
      publish_metrics("secret_scanning.enable", owner, current_user)
    elsif params[:secret_scanning] == "disable_all"
      settings_update_job_caller(owner, :secret_scanning_disable_all, current_user)
      publish_metrics("secret_scanning.disable", owner, current_user)
    end

    if params[:secret_scanning_new_repos] == "enabled"
      owner.enable_secret_scanning_for_new_repos(actor: current_user)
      publish_metrics("secret_scanning_new_repos.enable", owner, current_user)
    elsif params[:secret_scanning_new_repos] == "disabled"
      owner.disable_secret_scanning_for_new_repos(actor: current_user)
      publish_metrics("secret_scanning_new_repos.disable", owner, current_user)
    end
  end

  def publish_metrics(event_name, owner, actor)
    # Audit Log
    GitHub.instrument("#{event_name}", instrumentation_payload(owner, actor))

    # Hydro - used for recording usage telemetry.
    GlobalInstrumenter.instrument("security_analysis.update", {
      event_name: event_name,
      owner: owner,
      actor: actor,
    })
  end

  def settings_update_job_caller(owner, type, current_user)
    SecurityAnalysisSettingsUpdateJob.perform_later(owner, type, current_user.id)
  end

  def initialize_security_settings(owner, repository, user = current_user)
    options = Hash.new

    if repository.private?
      # Dependency graph
      options[:dependency_graph_enabled] = owner.dependency_graph_enabled_for_new_repos? ? "1" : "0"

      # Secret scanning
      if owner.organization?
        options[:token_scanning_enabled] = owner.secret_scanning_enabled_for_new_repos? ? "1" : "0"
      end
    end

    # Dependabot alerts
    options[:vulnerability_alerts_enabled] = owner.security_alerts_enabled_for_new_repos? ? "1" : "0"

    # Dependabot security updates
    options[:vulnerability_updates_enabled] = owner.vulnerability_updates_enabled_for_new_repos? ? "1" : "0"

    repository.manage_security_settings(options, actor: user)
  end

  private

  def instrumentation_payload(owner, actor)
    payload = {
      user: actor,
      org: owner
    }

    payload
  end

  # Is any IP allow list policy in place satisfied for the request?
  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
