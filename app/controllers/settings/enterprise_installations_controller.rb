# frozen_string_literal: true
module Settings
  class EnterpriseInstallationsController < ApplicationController
    include OrganizationsHelper
    include BusinessesHelper
    include EnterpriseInstallationsHelper
    include EnterpriseAccountStatsHelper

    class UnknownControllerActionExternalIdentyOrganization < StandardError; end

    before_action :login_required
    before_action :ensure_current_organization_admin, except: %w(new create)
    before_action :require_valid_installation_token, only: %w(new create)
    before_action :require_valid_state, only: %w(new create)
    before_action :require_valid_hostname_in_data, only: %w(new create)
    before_action :require_valid_server_id_in_data, only: %w(create)
    before_action :destroy_installations_for_server_uuid, only: %w(create)
    around_action :register_enterprise_account_controller_stats

    javascript_bundle :settings

    # List enterprise installations mapped to this organization
    def index
      render "settings/enterprise_installations/index", locals: { org_enterprise_installations: org_enterprise_installations }
    end

    # this method is used by GHES versions < 2.17. it can be removed after all customers are on GHES >= 2.17
    def new
      redirect_to new_enterprise_installation_url(state: params[:state], token: params[:token])
    end

    def create
      return render_404 if !target_organization
      conflicting_installations(target_organization, installations_data["host_name"]).destroy_all

      result = EnterpriseInstallation::Creator.perform(target_organization,
        actor: current_user,
        server_data: installations_data,
      )

      if result.success?
        set_github_app_icon(result.enterprise_installation, current_user)
        redirect_to complete_enterprise_installation_url(result.enterprise_installation, token: params[:token], state: params[:state])
      else
        flash[:error] = "Failed to connect #{installations_data["host_name"]} to the #{target_organization.name} organization."
        redirect_to new_enterprise_installation_path(token: params[:token], state: params[:state])
      end
    end

    def destroy
      enterprise_installation = org_enterprise_installations.find(params["id"])
      enterprise_installation.destroy

      if org_enterprise_installations.present?
        redirect_to organization_enterprise_installations_list_path(params["organization_id"])
      else
        redirect_to settings_org_profile_path(current_organization)
      end
    end

    private

    def ensure_current_organization_admin
      render_404 if !current_organization.present? || !org_admin?
    end

    def current_installation
      @current_installation ||= begin
        EnterpriseInstallation.find_by(id: GitHub.kv.get("ghe-install-id-#{token_hash}").value { nil })
      end
    end

    def target_for_conditional_access
      case params[:action]
      when "index", "destroy"
        # current_organization can be nil if the current_user doesn't have access to it
        # in this case, we can ignore conditional access policies and subject them to regular authz checks
        return :no_target_for_conditional_access unless current_organization
        current_organization
      when "create"
        # target_organization can be nil if the current_user doesn't have admin access to it
        # in this case, we can ignore conditional access policies and subject them to regular authz checks
        return :no_target_for_conditional_access unless target_organization
        target_organization
      when "new"
        # Lets you see the page listing each org you're an admin on without a specific SAML session yet
        :no_target_for_conditional_access
      else
        # Whenever a new route is added to this controller it needs to be accounted for in this method
        raise UnknownControllerActionExternalIdentyOrganization
      end
    end

    def target_organization
      @organization ||= target_organizations.where(login: params[:login]).first
    end

    def target_organizations
      orgs_with_admin_access
    end

    def orgs_with_admin_access
      current_user.owned_organizations
    end

    def org_enterprise_installations
      @org_enterprise_installations ||= current_organization.enterprise_installations
    end
  end
end
