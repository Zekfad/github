# frozen_string_literal: true

class Settings::MigrationsController < SettingsController
  areas_of_responsibility :community_and_safety

  statsd_tag_actions only: [:download, :start]
  before_action :sudo_filter, only: [:start, :download, :delete]
  before_action :download_everything_button_feature_required

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:email],
    key: :migration_email_rate_limit_key,
    max: 5,
    ttl: 1.hour,
    at_limit: :email_rate_limit_render

  def start
    begin
      warnings = GitHub.migrator.download_everything(current_user)

      if warnings.any?
         flash[:notice] = warnings.join(" ")
      else
         flash[:notice] = "Export started."
      end

    rescue GitHub::MigrationCoordinator::RateLimitExceeded
      flash[:error] = "You've reached the maximum number of exports at this time. Please try again later."
    ensure
      redirect_to settings_user_admin_path
    end
  end

  def email
    migration = Migration.find_by_id(params[:migration_id])
    return render_404 unless migration && migration.owner == current_user

    migration.send_user_migration_email

    flash[:notice] = "An email has been sent to you with a link to your export."
    redirect_to settings_user_admin_path
  end

  def download
    url = Migration.token_to_url(params[:token], current_user)
    return render_404 unless url
    redirect_to url
  end

  def delete
    migration = Migration.find_by_id(params[:migration_id])
    return render_404 unless migration && current_user == migration.owner

    MigrationDestroyFileJob.enqueue(migration)
    GitHub.dogstats.increment("download_everything.delete")
    flash[:notice] = "Job queued to delete file."
    redirect_to settings_user_admin_path
  end

  private

  def migration_email_rate_limit_key
    "migration-email:#{current_user.id}"
  end

  def download_everything_button_feature_required
    return render_404 unless GitHub.download_everything_button_enabled?
  end

  def email_rate_limit_render
    message = "You have exceeded our migration email rate limit. You will not be able to request another migration email for the next hour."
    render status: 422, plain: message
  end
end
