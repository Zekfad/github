# frozen_string_literal: true

module Settings
  class SavedRepliesController < ApplicationController

    # The following actions do not require conditional access checks because
    # they *don't* access any protected organization resources.
    skip_before_action :perform_conditional_access_checks, only: %w(
      index
      create
      update
      edit
      destroy
    )

    before_action :login_required
    before_action :highlight_settings_sidebar, only: [:edit]

    javascript_bundle :settings

    def index
      respond_to do |format|
        format.html do
          if request.xhr? && !pjax?
            render partial: "settings/user/replies/menu", locals: {
              replies: user_replies,
            }
          else
            return_to = request.referrer if params[:return_to]
            render "settings/user/replies/index", locals: {
              replies: user_replies,
              reply: SavedReply.new,
              return_to: return_to,
            }
          end
        end
      end
    end

    def create
      reply = current_user.saved_replies.create(title: params[:title], body: params[:body])

      if reply.save
        GitHub.dogstats.increment("saved_reply", tags: ["action:create"])
        flash[:notice] = "Your saved reply was created successfully."
        if params[:return_to]
          safe_redirect_to params[:return_to]
        else
          redirect_to :back
        end
      else
        if reply.errors.include?(:reply_count)
          flash[:error] = "You’ve reached the maximum number of #{SavedReply::MAX_REPLIES_PER_USER} saved replies."
        else
          flash[:error] = "Error creating your saved reply."
        end
        redirect_to :back
      end
    end

    def edit
      reply = current_user.saved_replies.find(params[:id])
      render "settings/user/replies/edit", locals: { reply: reply }
    end

    def update
      reply = current_user.saved_replies.find(params[:id])
      if reply.update(title: params[:title], body: params[:body])
        GitHub.dogstats.increment("saved_reply", tags: ["action:update"])
        flash[:notice] = "Your saved reply was updated successfully."
        redirect_to saved_replies_path
      else
        flash[:error] = "Error updating your saved reply."
        render "settings/user/replies/edit", locals: { reply: reply }
      end
    end

    def destroy
      reply = current_user.saved_replies.find(params[:id])
      reply.destroy
      GitHub.dogstats.increment("saved_reply", tags: ["action:destroy"])

      if request.xhr?
        head :ok
      else
        flash[:notice] = "Your saved reply was deleted successfully."
        redirect_to saved_replies_path
      end
    end

    private

    def highlight_settings_sidebar
      @selected_link = :edit_saved_reply
    end

    def user_replies
      context = (params[:context].presence || "none").to_sym
      current_user.available_saved_replies(context: context)
    end
  end
end
