# frozen_string_literal: true

module Settings
  class PersonalRemindersController < ApplicationController
    areas_of_responsibility :ce_extensibility

    before_action :login_required
    before_action :check_feature_flag
    before_action { @selected_link = :reminders }
    javascript_bundle :reminders

    def index
      raise "Slack App is not configured" if Apps::Internal.integration(:slack).nil?
      reminders = Reminders::PersonalReminderViewModel.all_for(current_user)

      respond_to do |format|
        format.html do
          render "settings/user/reminders/index", locals: { reminders: reminders }
        end
      end
    end

    def show
      personal_reminder = PersonalReminder.find_by(user: current_user, remindable: this_organization)
      personal_reminder ||= PersonalReminder.new(user: current_user, remindable: this_organization)

      render_edit(personal_reminder)
    end

    def update
      personal_reminder = begin
        retries ||= 0
        PersonalReminder.find_by(user: current_user, remindable: this_organization) || PersonalReminder.create(user: current_user, remindable: this_organization)
      rescue ActiveRecord::RecordNotUnique
        retry if (retries += 1) < 5

        flash[:error] = "There was an internal error trying to create your reminder. Please try again in a few moments."
        render_edit(PersonalReminder.new(user: current_user, remindable: this_organization))
        return
      end

      if personal_reminder.update(personal_reminder_params)
        flash[:notice] = "Your reminder was updated successfully"
        redirect_to personal_reminders_path
      else
        flash[:error] = "There was an error: #{personal_reminder.errors.full_messages.to_sentence}"

        render_edit(personal_reminder)
      end
    end

    def reminder_test
      personal_reminder = PersonalReminder.find_by!(user: current_user, remindable: this_organization)

      pull_requests = ActiveRecord::Base.connected_to(role: :reading) do
        personal_reminder.filtered_pull_requests
      end

      if pull_requests.empty?
        flash[:warn] = "We tried to send a test message, however you won't receive any messages because no matching pull requests were found."
      else
        ProcessReminderJob.perform_later(personal_reminder, delivery_target: Time.zone.now, test: true)
        flash[:notice] = "A test message is being sent."
      end

      redirect_to personal_reminders_path
    end

    def destroy
      personal_reminder = PersonalReminder.find_by!(user: current_user, remindable: this_organization)

      if personal_reminder.destroy
        flash[:notice] = "Your reminder was removed successfully"
      else
        flash[:error] = "An error occurred"
      end

      redirect_to personal_reminders_path
    end

    private

    def target_for_conditional_access
      # bypassing logged_in? is temporary until experiment is removed - see https://github.com/github/github/pull/146998
      return :no_target_for_conditional_access unless logged_in?
      return :no_target_for_conditional_access if action_name == "index"
      this_organization
    end

    def this_organization
      @this_organization ||= current_user.organizations.find_by!(login: params[:organization_id])
    end
    helper_method :this_organization

    def check_feature_flag
      orgs = if action_name != "index"
        [this_organization]
      else
        current_user.organizations
      end

      unless orgs.any? { |org| GitHub.flipper[:scheduled_reminders_frontend].enabled?(org) }
        render_404
      end
    end
    helper_method :check_feature_flag

    def render_edit(personal_reminder)
      respond_to do |format|
        format.html do
          render_template_view(
            "settings/user/reminders/edit",
            Settings::PersonalReminders::EditView,
            personal_reminder: personal_reminder,
          )
        end
      end
    end

    def personal_reminder_params
      personal_reminder_params = params.require(:personal_reminder).permit(
        :reminder_slack_workspace_id,
        :include_review_requests,
        :include_team_review_requests,
        :time_zone_name,
        :subscribed_to_events,
        {
          event_types: [], # for backwards compatibility
        },
        {
          reminder_event_subscriptions: [
            :event_type,
            :options,
          ],
        },
        {
          delivery_time: [
            {days: []},
            {times: []},
          ],
        },
      )

      personal_reminder_params[:reminder_event_subscriptions] = [] unless personal_reminder_params.delete(:subscribed_to_events) == "1"

      if personal_reminder_params.delete(:subscribed_to_events) == "0"
        personal_reminder_params[:event_subscriptions_attributes] = []
      elsif personal_reminder_params[:reminder_event_subscriptions]
        event_subscriptions = personal_reminder_params[:reminder_event_subscriptions]
          .reject { |event| event[:event_type].to_s.blank? }
          .uniq { |event| event[:event_type].to_s }

        personal_reminder_params[:event_subscriptions_attributes] = event_subscriptions.map do |event_subscription|
          { event_type: event_subscription[:event_type], options: event_subscription[:options].to_s }
        end
      elsif personal_reminder_params[:event_types] # nb: temporary backwards compatibility for form
        event_types = personal_reminder_params[:event_types].reject(&:blank?).uniq
        personal_reminder_params[:event_subscriptions_attributes] = event_types.map do |type|
          { event_type: type }
        end
      end

      personal_reminder_params.delete(:event_types)
      personal_reminder_params.delete(:reminder_event_subscriptions)
      personal_reminder_params[:event_subscriptions_attributes].map(&:permit!) if personal_reminder_params[:event_subscriptions_attributes]

      personal_reminder_params[:delivery_times_attributes] = build_delivery_times_attributes(personal_reminder_params.delete(:delivery_time))
      personal_reminder_params[:delivery_times_attributes].map(&:permit!)
      personal_reminder_params
    end

    # Combine user-supplied day, time, and time zone into delivery time attributes
    def build_delivery_times_attributes(delivery_time_params)
      delivery_time_params ||= {}

      times = Array(delivery_time_params[:times]).uniq.select(&:present?)
      days = Array(delivery_time_params[:days]).uniq.select(&:present?)

      days.product(times).map do |(day, time)|
        { day: day, time: time }
      end
    end
  end
end
