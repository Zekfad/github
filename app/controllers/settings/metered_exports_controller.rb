# frozen_string_literal: true

class Settings::MeteredExportsController < ApplicationController
  include BillingSettingsHelper
  include OrganizationsHelper

  areas_of_responsibility :gitcoin

  before_action :ensure_billing_enabled
  before_action :login_required
  before_action :ensure_target_is_billable

  def show
    export = target.metered_usage_exports.find(params[:id])

    redirect_to export.generate_expiring_url
  end

  def create
    if valid_date_range?
      Billing::MeteredReportExportJob.perform_later(current_user, target, params[:days].to_i)
      flash[:notice] = "We're preparing your report! We’ll send an email to #{current_user.email} when it’s ready."
    else
      flash[:error] = "We're having issues preparing your report. Please contact support or try again later."
    end

    redirect_back fallback_location: path_for_billing_settings
  end

  private

  def valid_date_range?
    Billing::MeteredUsageReportGenerator::VALID_DURATIONS.include?(params[:days].to_i)
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless target
    target
  end

  def target
    @target ||= target!
  end

  def target!
    if params[:target] == "organization"
      org = current_organization_for_member_or_billing
      if org && org.billing_manageable_by?(current_user)
        org
      end
    else
      current_user
    end
  end

  def ensure_target_is_billable
    render_404 unless target&.billable?
  end

  def path_for_billing_settings
    if target.organization?
      settings_org_billing_path(target)
    else
      billing_path
    end
  end
end
