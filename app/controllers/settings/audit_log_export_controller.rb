# frozen_string_literal: true

class Settings::AuditLogExportController < ApplicationController
  include AuditLogExportHelper

  areas_of_responsibility :account_settings, :audit_log

  statsd_tag_actions only: :create

  before_action :audit_log_export_required
  skip_before_action :perform_conditional_access_checks

  def show
    export = current_user.audit_log_exports.find_by_token!(params[:token])
    render_audit_log_export(export)
  end

  def create
    options = {
      actor: current_user,
      phrase: params[:q],
      format: params[:export_format],
    }

    export = current_user.audit_log_exports.create(options)
    respond_with_audit_log_export \
      export: export,
      export_url: settings_user_audit_log_export_url(token: export.token, format: export.format)
  end
end
