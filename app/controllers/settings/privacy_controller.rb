# frozen_string_literal: true

class Settings::PrivacyController < ApplicationController
  areas_of_responsibility :privacy_settings

  skip_before_action :perform_conditional_access_checks

  before_action :login_required
  before_action :require_third_party_analytics_setting

  def toggle_third_party_analytics
    current_user.update!(user_params)

    redirect_to settings_user_security_path, flash: { toggle_third_party_analytics_notice: true }
  end

  private

  def user_params
    params.require(:user).permit(:report_third_party_analytics)
  end

  def require_third_party_analytics_setting
    unless third_party_analytics_setting_enabled?
      head :not_found
    end
  end
end
