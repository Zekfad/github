# frozen_string_literal: true

class Settings::InstallationsController < ApplicationController
  areas_of_responsibility :platform

  include IntegrationInstallationsControllerMethods

  # The following actions do not require conditional access checks because
  # they *don't* access any protected organization resources
  skip_before_action :perform_conditional_access_checks, only: %w(
    index
    show
    update
    destroy
    permissions_update_request
    update_permissions
    suspend
    unsuspend
  )

  javascript_bundle :settings

  private

  def current_context
    current_user
  end
end
