# frozen_string_literal: true

class RepositoryInstallationsController < AbstractRepositoryController
  areas_of_responsibility :platform

  include IntegrationInstallationsControllerMethods

  layout :repository_layout
  javascript_bundle :settings

  private

  def current_context
    current_repository
  end

  def target_for_conditional_access
    current_repository.owner
  end

  # Set up as a before_action in RepositoryControllerMethods.
  # Overloaded so installations aren’t visible to people who can pull public repos.
  def privacy_check
    return true if current_repository.adminable_by? current_user

    if logged_in? && current_user.site_admin?
      render "admin/locked_repo"
    else
      render_404
    end
  end
end
