# frozen_string_literal: true

class Biztools::RepositoryActionsController < BiztoolsController
  before_action :actions_required
  before_action :cast_boolean_params, only: :update
  before_action :cast_float_params, only: :update
  before_action :merge_category_params, only: :update

  IndexQuery = parse_query <<-'GRAPHQL'
    query($cursor: String, $filterBy: RepositoryActionFilters, $perPage: Int!) {
      ...Views::Biztools::RepositoryActions::Index::Root
    }
  GRAPHQL

  IndexXhrQuery = parse_query <<-'GRAPHQL'
    query($cursor: String, $filterBy: RepositoryActionFilters, $perPage: Int!) {
      ...Views::Biztools::RepositoryActions::Actions::Root
    }
  GRAPHQL

  def index
    variables = { perPage: 100, filterBy: filter_params, cursor: params[:cursor] }

    if request.xhr?
      data = platform_execute(IndexXhrQuery, variables: variables)
      render partial: "biztools/repository_actions/actions", locals: { data: data, filters: filter_params }
    else
      data = platform_execute(IndexQuery, variables: variables)
      render "biztools/repository_actions/index", locals: { data: data, filters: filter_params }
    end
  end

  ShowQuery = parse_query <<~'GRAPHQL'
    query($id: ID!) {
      repositoryAction(id: $id) {
        id
      }
      ...Biztools::RepositoryActions::ShowView::Query::Root
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: { id: params[:id] })
    return render_404 if data.repository_action.blank?

    render_template_view \
      "biztools/repository_actions/show",
      Biztools::RepositoryActions::ShowView,
      data: data
  end

  EditQuery = parse_query <<~'GRAPHQL'
    query($id: ID!) {
      repositoryAction(id: $id) {
        id
      }
      ...Biztools::RepositoryActions::EditView::Query::Root
    }
  GRAPHQL

  def edit
    data = platform_execute(EditQuery, variables: { id: params[:id] })
    return render_404 if data.repository_action.blank?

    render_template_view \
      "biztools/repository_actions/edit",
      Biztools::RepositoryActions::EditView,
      data: data
  end

  UpdateQuery = parse_query <<~'GRAPHQL'
      mutation($input: UpdateRepositoryActionInput!) {
        updateRepositoryAction(input: $input) {
          repositoryAction {
            id
            name
            featured
          }
        }
      }
  GRAPHQL

  def update
    input = repository_action_params.merge(repositoryActionId: params[:id])
    data = platform_execute(UpdateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateRepositoryAction]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      error = data.errors.messages.values.join(", ")
      flash[:error] = error
      redirect_to biztools_repository_actions_path(params: { filters: filter_params })
    else
      action = data.update_repository_action.repository_action
      flash[:notice] = "#{action.name} was updated."
      redirect_to biztools_repository_actions_path(params: { filters: filter_params })
    end
  end

  def creators
    creators = if params[:query].present?
      Organization.search(params[:query])
    else
      Configurable::RepositoryActionVerifiedOrg.verified_for_repo_actions.paginate(page: current_page)
    end
    render "biztools/repository_actions/creators", locals: { creators: creators }
  end

  def verify_creator
    creator = Organization.find(params[:creator_id])

    if params[:verify] == "true"
      creator.verify_for_repo_actions(current_user)
      flash[:notice] = "Verified #{creator.login}. Search results should be updated within a few moments"
    else
      creator.unverify_for_repo_actions(current_user)
      flash[:notice] = "Removed verification for #{creator.login}. Search results should be updated within a few moments"
    end

    redirect_to creators_biztools_repository_actions_path(query: params[:query])
  end

  def reindex_creator
    creator = Organization.find(params[:creator_id])
    RepositoryAction.owned_by(creator.login).each(&:synchronize_search_index)
    flash[:notice] = "Repository actions for #{creator.login} are queued to be re-index. Search results should be updated within a few moments"
    redirect_to creators_biztools_repository_actions_path(query: params[:query])
  end

  private

  def repository_action_params
    params.require(:repository_action)
      .permit(:featured, :rankMultiplier, categories: [], filterCategories: [])
  end

  def cast_boolean_params
    %i(featured).each do |attr|
      if (bool_param = params.dig(:repository_action, attr)).present?
        params[:repository_action][attr] = ActiveModel::Type::Boolean.new.cast(bool_param)
      end
    end
  end

  def cast_float_params
    %i(rankMultiplier).each do |attr|
      if (float_param = params.dig(:repository_action, attr)).present?
        params[:repository_action][attr] = ActiveModel::Type::Float.new.cast(float_param)
      end
    end
  end

  def merge_category_params
    categories = []

    if (first_category = params[:repository_action].delete(:primaryCategoryName)).present?
      categories << first_category
    end

    if (second_category = params[:repository_action].delete(:secondaryCategoryName)).present?
      categories << second_category
    end

    if categories.present?
      params[:repository_action][:categories] = categories
    end
  end

  def filter_params
    @filter_params ||= [:name, :featured].each_with_object({}) do |key, filters|
      filters[key] = params.dig(:filters, key).presence
    end
  end

  def actions_required
    render_404 unless GitHub.actions_enabled?
  end
end
