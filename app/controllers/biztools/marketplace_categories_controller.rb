# frozen_string_literal: true

class Biztools::MarketplaceCategoriesController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :marketplace_required
  before_action :cast_boolean_params, only: %i(create update)
  before_action :cast_integer_params, only: %i(create update)

  IndexQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::Biztools::MarketplaceCategories::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery)

    respond_to do |format|
      format.html do
        render "biztools/marketplace_categories/index", locals: { data: data }
      end
    end
  end

  NewQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::Biztools::MarketplaceCategories::New::Root
    }
  GRAPHQL

  def new
    data = platform_execute(NewQuery)

    respond_to do |format|
      format.html do
        render "biztools/marketplace_categories/new", locals: { data: data }
      end
    end
  end

  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: CreateMarketplaceCategoryInput!) {
      createMarketplaceCategory(input: $input) {
        marketplaceCategory
      }
    }
  GRAPHQL

  def create
    input = marketplace_category_params
    data = platform_execute(CreateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:createMarketplaceCategory]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")

      return redirect_to biztools_new_marketplace_category_path
    end

    flash[:notice] = "Successfully created the Marketplace category"
    redirect_to biztools_marketplace_categories_path
  end

  EditQuery = parse_query <<-'GRAPHQL'
    query($slug: String!) {
      ...Views::Biztools::MarketplaceCategories::Edit::Root
      marketplaceCategory(slug: $slug)
    }
  GRAPHQL

  def edit
    data = platform_execute(EditQuery, variables: { slug: params[:category_slug] })

    return render_404 unless data.marketplace_category

    respond_to do |format|
      format.html do
        render "biztools/marketplace_categories/edit", locals: { data: data }
      end
    end
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateMarketplaceCategoryInput!) {
      updateMarketplaceCategory(input: $input) {
        marketplaceCategory
      }
    }
  GRAPHQL

  def update
    input = marketplace_category_params.merge(slug: params[:category_slug])
    data = platform_execute(UpdateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateMarketplaceCategory]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")
      flash[:error] = message
      return redirect_to biztools_edit_marketplace_category_path(params[:category_slug])
    end

    flash[:notice] = "Successfully updated the Marketplace category"
    redirect_to biztools_marketplace_categories_path
  end

  private

  def marketplace_category_params
    params
      .require(:marketplace_category)
      .permit(:name, :description, :howItWorks, :isNavigationVisible,
              :isFilter, :isFeatured, :featuredPosition, subCategories: [])
  end

  def cast_boolean_params
    %i(isNavigationVisible isFilter isFeatured).each do |attr|
      if (bool_param = params.dig(:marketplace_category, attr)).present?
        params[:marketplace_category][attr] = ActiveModel::Type::Boolean.new.cast(bool_param)
      end
    end
  end

  def cast_integer_params
    %i(featuredPosition).each do |attr|
      if (int_param = params.dig(:marketplace_category, attr)).present?
        params[:marketplace_category][attr] = ActiveModel::Type::Integer.new.cast(int_param)
      else
        params[:marketplace_category].delete attr
      end
    end
  end
end
