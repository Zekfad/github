# frozen_string_literal: true

class Biztools::AccountingController < BiztoolsController
  before_action :set_content_type, only: [:yearly_transactions, :yearly_refunds]

  def show
    render "biztools/accounting/show"
  end

  def yearly_transactions
    report = Biztools::YearlySalesReport.new \
      params[:year].to_i,
      params[:month].to_i

    send_data \
      report.generate,
      type: "text/csv",
      disposition: "attachment;filename=#{report.filename}"
  end

  def yearly_refunds
    report = Biztools::YearlyRefundsReport.new \
      params[:year].to_i,
      params[:month].to_i

    send_data \
      report.generate,
      type: "text/csv",
      disposition: "attachment;filename=#{report.filename}"
  end

  private

  def set_content_type
    response.headers["Content-Type"] = "text/csv"
  end
end
