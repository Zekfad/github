# frozen_string_literal: true

class Biztools::TopicsController < BiztoolsController
  before_action :dotcom_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query($perPage: Int!, $after: String, $curated: Boolean, $featured: Boolean, $query: String,
          $owner: String!, $name: String!) {
      ...Views::Biztools::Topics::Index::Root
    }
  GRAPHQL

  IndexXHRQuery = parse_query <<-'GRAPHQL'
    query($perPage: Int!, $after: String, $curated: Boolean, $featured: Boolean, $query: String) {
      ...Views::Biztools::Topics::CuratedTopics::Root
    }
  GRAPHQL

  def index
    query = params[:q]
    variables = { perPage: 30, after: params[:after], curated: true, featured: nil,
                  query: query }
    locals = { featured: nil, query: query }

    if params[:featured].present?
      featured = params[:featured].to_i == 1
      variables[:featured] = featured
      locals[:featured] = featured
    end

    respond_to do |format|
      format.html do
        if request.xhr?
          locals[:data] = platform_execute(IndexXHRQuery, variables: variables)
          render partial: "biztools/topics/curated_topics", locals: locals
        else
          variables[:owner] = TopicImporter::REPO_OWNER
          variables[:name] = TopicImporter::REPO_NAME
          locals[:data] = platform_execute(IndexQuery, variables: variables)
          render "biztools/topics/index", locals: locals
        end
      end
    end
  end

  UpdateMutation = parse_query <<-'GRAPHQL'
    mutation($name: String!, $isFeatured: Boolean!) {
      updateTopic(input: { name: $name, isFeatured: $isFeatured }) {
        topic {
          ...Views::Biztools::Topics::CuratedTopic::Topic
        }
      }
    }
  GRAPHQL

  def update
    variables = { name: params[:topic], isFeatured: params[:featured].to_i == 1 }
    data = platform_execute(UpdateMutation, variables: variables)

    if data.errors.any?
      error = data.errors.messages.values.join(", ")
      return render json: { error: error }, status: :unprocessable_entity
    end

    respond_to do |format|
      format.html do
        topic = data.update_topic.topic
        render partial: "biztools/topics/curated_topic", locals: { topic: topic }
      end
    end
  end

  ImportQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        ...Views::Biztools::Topics::Import::Repository
      }
    }
  GRAPHQL

  def import
    write_mode = params[:write_mode] == "1"
    dry_run = !write_mode
    importer = TopicImporter.new
    importer.import(dry_run: dry_run, topic_names: params[:topics])

    unless importer.any_changes?
      flash[:notice] = "Topics in GitHub are up-to-date with those in " +
                       "#{TopicImporter.repository_name_with_owner}!"
      return redirect_to(biztools_topics_path)
    end

    if dry_run || importer.errors.any?
      data = platform_execute(ImportQuery, variables: {
        owner: TopicImporter::REPO_OWNER, name: TopicImporter::REPO_NAME
      })
      render "biztools/topics/import", locals: {
        importer: importer, new_topic_changesets: importer.new_topic_changesets,
        updated_topic_changesets: importer.updated_topic_changesets,
        topics_repository: data.repository
      }
    else
      flash[:notice] = "Imported #{importer.new_count} new topic(s) and updated " +
                       "#{importer.updated_count} topic(s)."
      redirect_to biztools_topics_path
    end
  end
end
