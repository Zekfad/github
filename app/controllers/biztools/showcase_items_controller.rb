# frozen_string_literal: true

class Biztools::ShowcaseItemsController < BiztoolsController
  include ShowcaseItemsControllerMethods

  areas_of_responsibility :explore, :stafftools

  before_action :find_collection, except: [:perform_healthcheck]
  before_action :find_item, except: [:create, :perform_healthcheck]
  skip_before_action :verify_authenticity_token, only: [:perform_healthcheck]

  def create
    create_item(:biztools)
  end

  def update
    update_item(:biztools)
  end

  def edit
    edit_item(:biztools)
  end

  def destroy
    destroy_item(:biztools)
  end
end
