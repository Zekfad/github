# frozen_string_literal: true

class Biztools::CollectionsController < BiztoolsController
  areas_of_responsibility :explore

  before_action :dotcom_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query($perPage: Int!, $after: String, $featured: Boolean, $query: String,
          $owner: String!, $name: String!) {
      ...Views::Biztools::Collections::Index::Root
    }
  GRAPHQL

  IndexXHRQuery = parse_query <<-'GRAPHQL'
    query($perPage: Int!, $after: String, $featured: Boolean, $query: String) {
      ...Views::Biztools::Collections::CuratedCollections::Root
    }
  GRAPHQL

  def index
    query = params[:q]

    # If the featured param is passed in, check for 1 or 0. If not, show all collections.
    if params[:featured].present?
      featured = params[:featured].to_i == 1
    else
      featured = nil
    end

    variables = { perPage: 30, after: params[:after], featured: featured, query: query }
    locals = { featured: featured, query: query }

    respond_to do |format|
      format.html do
        if request.xhr?
          locals[:data] = platform_execute(IndexXHRQuery, variables: variables)
          render partial: "biztools/collections/curated_collections", locals: locals
        else
          variables[:owner] = ExploreRepositoryFileReader::REPO_OWNER
          variables[:name] = ExploreRepositoryFileReader::REPO_NAME
          locals[:data] = platform_execute(IndexQuery, variables: variables)

          render "biztools/collections/index", locals: locals
        end
      end
    end
  end

  ImportQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        ...Views::Biztools::Collections::Import::Repository
      }
    }
  GRAPHQL

  UpdateMutation = parse_query <<-'GRAPHQL'
    mutation($slug: String!, $isFeatured: Boolean!) {
      updateExploreCollection(input: { slug: $slug, isFeatured: $isFeatured }) {
        collection {
          ...Views::Biztools::Collections::CuratedCollection::ExploreCollection
        }
      }
    }
  GRAPHQL

  def update
    variables = { slug: params[:collection], isFeatured: params[:featured].to_i == 1 }
    data = platform_execute(UpdateMutation, variables: variables)

    if data.errors.any?
      error = data.errors.messages.values.join(", ")
      return render json: { error: error }, status: :unprocessable_entity
    end

    respond_to do |format|
      format.html do
        collection = data.update_explore_collection.collection
        render partial: "biztools/collections/curated_collection", locals: { collection: collection }
      end
    end
  end

  def import
    write_mode = params[:write_mode] == "1"
    dry_run = !write_mode

    repository_file_reader = ExploreRepositoryFileReader.new("collections")
    importer = CollectionImporter.new(repository_file_reader)
    import_result = importer.import(dry_run: dry_run, collection_slugs: params[:collections])

    unless import_result.any_changes?
      flash[:notice] = "Collections in GitHub are up-to-date with those in " +
                       "#{ExploreRepositoryFileReader.repository_name_with_owner}!"
      return redirect_to(biztools_collections_path)
    end

    if dry_run || importer.errors.any?
      data = platform_execute(ImportQuery, variables: {
        owner: ExploreRepositoryFileReader::REPO_OWNER,
        name: ExploreRepositoryFileReader::REPO_NAME,
      })

      render "biztools/collections/import", locals: {
        data: data,
        importer: importer,
        import_result: import_result,
        new_changesets: import_result.new_changesets,
        updated_changesets: import_result.updated_changesets,
        explore_repository: data.repository,
      }
    else
      flash[:notice] = "Imported #{import_result.new_count} new collections(s), updated " \
                       "#{import_result.updated_count} collections(s), and deleted " \
                       "#{import_result.deleted_count} collection(s)."

      redirect_to biztools_collections_path
    end
  end
end
