# frozen_string_literal: true

# biztools for showcase crud
class Biztools::ShowcaseCollectionsController < BiztoolsController
  include ShowcaseCollectionsControllerMethods

  areas_of_responsibility :explore, :stafftools

  before_action :find_collection, except: [:index, :new, :create]

  def index
    featured_collections = ::Showcase::Collection.featured
    collections = ::Showcase::Collection.order("published ASC, name ASC").to_a

    render "biztools/showcase_collections/index", locals: {
      featured_collections: featured_collections,
      collections: collections,
    }
  end

  def show
    items = @collection.items

    render "biztools/showcase_collections/show", locals: {
      collection: @collection,
      items: items,
    }
  end

  def new
    new_collection(:biztools)
  end

  def create
    create_collection(:biztools)
  end

  def edit
    edit_collection(:biztools)
  end

  def update
    update_collection(:biztools)
  end

  def destroy
    destroy_collection(:biztools)
  end
end
