# frozen_string_literal: true

class Biztools::MarketplaceAgreementsController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query($cursor: String) {
      ...Views::Biztools::MarketplaceAgreements::Agreements::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: { cursor: params[:cursor] })

    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "biztools/marketplace_agreements/agreements", locals: { data: data }
        else
          render "biztools/marketplace_agreements/index", locals: { data: data }
        end
      end
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on MarketplaceAgreement {
          ...Views::Biztools::MarketplaceAgreements::Show::MarketplaceAgreement
        }
      }
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: { id: params[:id] })

    return render_404 unless data.node

    respond_to do |format|
      format.html do
        render "biztools/marketplace_agreements/show", locals: { agreement: data.node }
      end
    end
  end

  NewQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::Biztools::MarketplaceAgreements::New::Root
    }
  GRAPHQL

  def new
    data = platform_execute(NewQuery)

    render "biztools/marketplace_agreements/new", locals: { data: data }
  end

  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: CreateMarketplaceAgreementInput!) {
      createMarketplaceAgreement(input: $input) {
        marketplaceAgreement {
          id
        }
      }
    }
  GRAPHQL

  def create
    data = platform_execute(CreateQuery, variables: { input: marketplace_agreement_params })

    if data.errors.any?
      error_type = data.errors.details[:createMarketplaceAgreement]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")

      return redirect_to biztools_new_marketplace_agreement_path
    end

    id = data.create_marketplace_agreement.marketplace_agreement.id
    redirect_to biztools_marketplace_agreement_path(id)
  end

  private

  def marketplace_agreement_params
    params.require(:marketplace_agreement).permit(:version, :body, :signatoryType)
  end
end
