# frozen_string_literal: true

class Biztools::Marketplace::StoriesController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::Biztools::Marketplace::Stories::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery)

    render "biztools/marketplace/stories/index", locals: { data: data }
  end
end
