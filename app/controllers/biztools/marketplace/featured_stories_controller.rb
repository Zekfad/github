# frozen_string_literal: true

class Biztools::Marketplace::FeaturedStoriesController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  FeatureQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateMarketplaceStoryInput!) {
      updateMarketplaceStory(input: $input) {
        marketplaceStory
      }
    }
  GRAPHQL

  def create
    input = { id: params[:id], featured: true }
    data = platform_execute(FeatureQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateMarketplaceStory]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
    else
      flash[:notice] = "Successfully marked the story as featured."
    end

    redirect_to biztools_marketplace_stories_path
  end

  def destroy
    input = { id: params[:id], featured: false }
    data = platform_execute(FeatureQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateMarketplaceStory]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
    else
      flash[:notice] = "Successfully unfeatured the story."
    end

    redirect_to biztools_marketplace_stories_path
  end
end
