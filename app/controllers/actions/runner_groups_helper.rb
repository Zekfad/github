# frozen_string_literal: true

require "grpc"
require "github-launch"
require "github/launch_client"

module Actions::RunnerGroupsHelper
  extend ActiveSupport::Concern

  included do
    before_action :ensure_actions_enabled
  end

  private

  def runner_group_for(owner, id:)
    resp = GrpcHelper.rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.get_group(owner: owner, group_id: id)
    end

    resp&.value&.runner_group
  end

  def create_group_for(owner, name:, visibility:, selected_targets: [])
    GrpcHelper.rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.create_group(
        actor: current_user,
        owner: owner,
        name: name,
        visibility: visibility,
        selected_targets: selected_targets
      )
    end
  end

  def update_group_for(owner, id:, name:, visibility:, selected_targets: [])
    GrpcHelper.rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.update_group(
        actor: current_user,
        owner: owner,
        group_id: id,
        name: name,
        visibility: visibility,
        selected_targets: selected_targets
      )
    end
  end

  def delete_group_for(owner, id:)
    GrpcHelper.rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.delete_group(
        actor: current_user,
        owner: owner,
        group_id: id,
      )
    end
  end

  def add_runners_for(owner, id:, runners_ids:)
    resp = GrpcHelper.rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.add_runners(
        actor: current_user,
        owner: owner,
        group_id: id,
        runner_ids: runners_ids,
      )
    end

    resp&.value&.runner_group
  end

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end
end
