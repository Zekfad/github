# frozen_string_literal: true

class Actions::WorkflowRunsController < AbstractRepositoryController
  map_to_service :actions_experience
  statsd_tag_actions

  include ActionsControllerMethods

  layout :repository_layout

  before_action :actions_enabled_for_repo?

  WorkflowRunShowQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $annotationsLimit: Int!, $after: String) {
      node(id: $id) {
        ...Views::Checks::WorkflowRun
      }
    }
  GRAPHQL

  def show
    return render_404 if request.xhr?

    workflow_run = find_workflow_run
    return render_404 unless workflow_run

    check_suite = workflow_run.check_suite

    begin
      commit = current_repository.commits.find(check_suite.head_sha)
    rescue GitRPC::ObjectMissing
      render_404 and return
    end

    pull = current_repository.pull_requests.find_by_id(params[:pull_id].to_i)
    variables = { id: check_suite.global_relay_id, annotationsLimit: CheckAnnotation::MAX_PER_REQUEST }
    graphql_check_suite = platform_execute(WorkflowRunShowQuery, variables: variables).node

    check_run_annotations = CheckAnnotation.joins(:check_run).where("check_runs.check_suite_id = ?", check_suite.id).to_a
    all_annotations = check_run_annotations + check_suite.annotations.to_a

    respond_to do |format|
      format.html do
        render "checks/show", locals: {
          selected_check_suite: check_suite,
          graphql_check_suite: graphql_check_suite,
          selected_check_run: nil,
          check_suites: [check_suite],
          pull: pull,
          commit_check_runs: check_suite.check_runs,
          commit: commit,
          blankslate: false,
          workflows_loading: false,
          all_annotations: all_annotations,
        }, layout: "repository"
      end
    end
  end

  def workflow_run_timing_details_partial
    return render_404 if current_repository.public? # We only bill for private repositories.

    workflow_run = Actions::WorkflowRun.includes(:check_suite).find_by(id: params[:workflow_run_id])
    return head :not_found unless workflow_run && request.xhr?

    render partial: "actions/workflow_runs/timing_details", locals: { workflow_run: workflow_run }
  end

  WorkflowFileTabQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $annotationsLimit: Int!, $after: String) {
      node(id: $id) {
        ...Views::Checks::WorkflowRun
      }
    }
  GRAPHQL

  def workflow_run_partial
    return render_404 unless request.xhr?

    workflow_run = find_workflow_run
    return render_404 unless workflow_run

    check_suite = workflow_run.check_suite
    variables = { id: check_suite.global_relay_id, annotationsLimit: CheckAnnotation::MAX_PER_REQUEST }
    graphql_check_suite = platform_execute(WorkflowFileTabQuery, variables: variables).node

    begin
      commit = current_repository.commits.find(check_suite.head_sha)
    rescue GitRPC::ObjectMissing
      render_404 and return
    end

    pull = current_repository.pull_requests.find_by_id(params[:pull_id])
    check_run_annotations = CheckAnnotation.joins(:check_run).where("check_runs.check_suite_id = ?", check_suite.id).to_a
    all_annotations = check_run_annotations + check_suite.annotations.to_a

    respond_to do |format|
      format.html do
        render partial: "checks/workflow_run", locals: {
          graphql_check_suite: graphql_check_suite,
          pull: pull,
          commit: commit,
          all_annotations: all_annotations,
          workflow_run: workflow_run
        }
      end
    end
  end

  def delete_logs
    return render_404 unless current_user_can_push?

    workflow_run = find_workflow_run
    return render_404 unless workflow_run

    check_suite = workflow_run.check_suite
    begin
      check_suite.delete_logs(actor: current_user)
      flash[:notice] = "Logs deleted successfully."
    rescue RuntimeError
      flash[:error] = "Failed to delete the logs for this workflow run."
    end

    path = workflow_run_path(workflow_run, user_id: current_repository.owner, repository: current_repository)
    redirect_to path
  end

  def delete_workflow_run
    return render_404 unless current_user_can_push?

    workflow_run = find_workflow_run
    return render_404 unless workflow_run

    check_suite = workflow_run.check_suite
    begin
      workflow_run.hard_delete(actor: current_user)
      flash[:notice] = "Workflow run deleted successfully."
    rescue Actions::WorkflowRun::NotDeleteableError
      flash[:error] = "Failed to delete this workflow run."
    end

    redirect_to :back
  end

  def delete_workflow_run_modal
    return render_404 unless request.xhr?

    workflow_run = find_workflow_run
    return render_404 unless workflow_run

    pull_requests = current_repository.pull_requests.where(
      head_sha: workflow_run.head_sha,
      head_repository_id: workflow_run.check_suite.head_repository_id,
    )

    respond_to do |format|
      format.html do
        render partial: "actions/workflow_run_item_delete_modal", locals: {
          workflow_run: workflow_run,
          pull_requests: pull_requests
        }
      end
    end
  end

  def disable_workflow
    return render_404 unless GitHub.flipper[:actions_disabled_workflows].enabled?(current_user)
    return render_404 unless current_user_can_push?

    workflow = find_workflow
    return render_404 unless workflow

    begin
      workflow.disable(current_user)
      flash[:notice] = "Workflow disabled successfully."
    rescue Actions::Workflow::NotActiveError
      flash[:error] = "Unable to disable a workflow that is not active."
    end

    redirect_to :back
  end

  def enable_workflow
    return render_404 unless GitHub.flipper[:actions_disabled_workflows].enabled?(current_user)
    return render_404 unless current_user_can_push?

    workflow = find_workflow
    return render_404 unless workflow

    begin
      workflow.enable(current_user)
      flash[:notice] = "Workflow enabled successfully."
    rescue Actions::Workflow::CannotBeEnabledError
      flash[:error] = "Unable to enable a workflow that is not active."
    end

    redirect_to :back
  end

  private

  def find_workflow_run
    workflow_run = Actions::WorkflowRun.find_by(id: params[:workflow_run_id])
    if !workflow_run || workflow_run.check_suite.repository_id != current_repository.id
      return nil
    end
    workflow_run
  end

  def find_workflow
    workflow = Actions::Workflow.find_by(id: params[:workflow_id])
    if !workflow || workflow.repository_id != current_repository.id
      return nil
    end
    workflow
  end
end
