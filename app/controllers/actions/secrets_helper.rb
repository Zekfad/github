# frozen_string_literal: true

require "grpc"
require "github-launch"
require "github/launch_client/credz"
require "github/launch_client"

module Actions::SecretsHelper
  include Api::App::GrpcHelpers
  include GitHub::LaunchClient
  extend ActiveSupport::Concern

  included do
    before_action :ensure_actions_enabled
  end

  private

  def secrets_for(owner, app:)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::Credz.list_credentials(app: app, owner: owner, actor: current_user)
    end

    unless resp.call_succeeded?
      flash[:error] = "Failed to load secrets. Please refresh and try again."
    end

    resp&.value&.credentials || []
  end

  def secret_last_update(secret)
    return nil unless secret.updated_at || secret.created_at

    return Time.at(secret.updated_at&.seconds || secret.created_at&.seconds).utc.to_datetime
  end

  def github_public_key(owner)
    return [1, GitHub.actions_secrets_public_key] if GitHub.enterprise?

    begin
      key = GitHub.earthsmoke.low_level_key(Platform::EarthsmokeKeys::CUSTOM_TASKS)
      version = key.export(scope: owner.global_relay_id).key_versions.first
      encoded = Base64.strict_encode64(version.parsed_key_pair.public_bytes)
      [version.id, encoded]
    rescue ::Earthsmoke::UnavailableError, ::Earthsmoke::TimeoutError
      ["", ""]
    end
  end

  def decrypt_enterprise_secret(value)
    box = RbNaCl::Boxes::Sealed.from_private_key(Base64.decode64(GitHub.actions_secrets_private_key))
    box.decrypt(Base64.strict_decode64(value))
  end

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end

  def visibility_description_for(secret, can_use_secrets_for_private_repos)
    case secret.visibility
    when GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_ALL_REPOS
      can_use_secrets_for_private_repos && "all repositories" || "public repositories"
    when GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_PRIVATE_REPOS
      "private repositories"
    when GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_SELECTED_REPOS
      "#{secret.selected_repositories_count} #{"repository".pluralize(secret.selected_repositories_count)}"
    else
      ""
    end
  end
end
