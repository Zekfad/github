# frozen_string_literal: true

require "grpc"
require "github-launch"
require "github/launch_client"

module Actions::RunnersHelper
  extend ActiveSupport::Concern

  included do
    before_action :ensure_actions_enabled
  end

  private

  OS_TO_PLATFORM_MAPPING = { "win" => "Windows", "osx" => "macOS", "linux" => "Linux" }

  def access_policy_for(owner)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.get_access_policy(owner)
    end

    GitHub::Launch::Services::Selfhostedrunners::GetAccessPolicyResponse.new(
      permission_type: resp&.value&.permission_type || Organizations::Settings::RepositoryPermissionsComponent::DEFAULT_PERMISSION_TYPE,
      selected_repositories: Array(resp&.value&.selected_repositories),
    )
  end

  def delete_runner_for(owner, id:, actor:)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.delete_runner(owner, id, actor: actor)
    end

    resp&.value&.status
  end


  def downloads_for(owner)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_downloads(owner)
    end

    resp&.value&.downloads || []
  end

  def labels_for(owner)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_labels(owner)
    end

    labels = resp&.value&.labels || []

    # Fake filtering out system variables. This would otherwise be:
    #   labels.select { |label| label.type == "user" }
    system_labels = ["self-hosted", "macos", "windows", "linux", "x64", "x32"]

    labels.reject { |label| system_labels.include?(label.name.downcase) }
  end

  def create_label_for(owner, name:)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.create_label(owner, name)
    end

    resp&.value&.label
  end

  def update_label_for(owner, runner_id:, labels:)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.update_labels(owner, runner_id: runner_id, labels: labels)
    end

    if updated_runner = resp&.value&.runners&.first
      Actions::Runner.from_grpc_object(updated_runner)
    end
  end

  def bulk_update_labels_for(owner, runner_ids:, additions:, removals:)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.bulk_update_labels(owner, runner_ids: runner_ids, additions: additions, removals: removals)
    end

    Actions::Runner.from_grpc_collection(Array(resp&.value&.runners))
  end

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end

  def ensure_can_use_org_runners
    return render_404 unless can_use_org_runners?
  end

  def can_use_org_runners?
    Billing::ActionsPermission.new(current_organization).status[:error][:reason] != "PLAN_INELIGIBLE"
  end

  def runner_options(downloads:, token:)
    os_options = downloads.map { |d| d.os }.uniq

    if params[:os].blank? && params[:arch].blank?
      browser = Browser.new(request.user_agent)
      if browser.known?
        os_param = "osx" if browser.platform.mac?
        os_param = "linux" if browser.platform.linux?
        os_param = "win" if browser.platform.windows?
      end
    end

    os_param = params[:os].to_s.downcase unless os_param
    os = os_options.include?(os_param) ? os_param : "linux"
    platform = OS_TO_PLATFORM_MAPPING[os]

    architecture_options = downloads.find_all { |d| d.os == os }.map { |d| d.architecture }

    arch_param = (params[:arch] || "").downcase
    architecture = architecture_options.include?(arch_param) ? arch_param : "x64"

    selected_download = downloads.find { |d| d.os == os && d.architecture == architecture }
    platform_options = os_options.map { |d| { os: d, platform: OS_TO_PLATFORM_MAPPING[d] } }

    {
      os: os,
      platform: platform,
      architecture: architecture,
      platform_options: platform_options,
      downloads: downloads,
      selected_download: selected_download,
      token: token
    }
  end
end
