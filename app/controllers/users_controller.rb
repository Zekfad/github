# frozen_string_literal: true

class UsersController < ApplicationController
  areas_of_responsibility :avatars, :orgs, :user_profile

  statsd_tag_actions only: [:ignore_user, :show, :unignore_user, :update]

  include ActionView::Helpers::NumberHelper
  include BillingSettingsHelper

  before_action :ensure_billing_enabled, only: %[billing]
  before_action :delete_sponsorship_rollback_notice, only: %[billing]

  # How many actions does this thing have? What about some :except?
  before_action :login_required, only: %w( ignore_user unignore_user edit update change_password
                                           follow unfollow billing destroy rename
                                           organizations dismiss_notice hide_jobs set_protocol
                                           update_checkout_preference
                                           set_private_contributions_preference
                                           suggestions
                                           organizations_info
                                           dismiss_repository_notice )

  before_action only: [:destroy] do
    check_ofac_sanctions(redirect_url: settings_user_admin_path)
  end

  before_action :ensure_valid_email, only: [:update, :change_password]
  before_action :sudo_filter, only: :destroy
  before_action :require_xhr, only: :organizations_info
  before_action :find_new_repository_owner, only: :organizations_info

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:update, :change_password],
    if: :update_rate_limit_filter,
    key: :update_rate_limit_key,
    log_key: :update_rate_limit_log_key,
    max: 60,
    ttl: 1.day,
    at_limit: :update_rate_limit_record

  def index
    redirect_to "/"
  end

  def edit
    url =
      case params["tab"]
      when "profile"
        settings_user_profile_url
      when "admin"
        settings_user_admin_url
      when "email"
        settings_user_emails_url
      when "ssh"
        settings_user_keys_url
      when "job"
        settings_user_profile_url
      when "connections"
        settings_user_applications_url
      else
        settings_user_profile_url
      end

    redirect_to url
  end

  InlineProfileQuery = parse_query <<-'GRAPHQL'
    query($user_id: ID!, $mutualFollowersEnabled: Boolean!) {
      node(id: $user_id) {
        ... on User {
          ...Views::Users::ProfileDetails::User
        }
      }
    }
  GRAPHQL

  def update
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

    @user = current_user
    @selected_tab = params[:tab]
    user_params = params.require(:user)
    private_contribs = user_params.delete(:show_private_contribution_count)
    enable_pro_badge = user_params.delete(:pro_badge_enabled)

    permitted_user_params = %i[
      gift
      profile_name
      profile_email
      profile_blog
      profile_company
      profile_location
      profile_hireable
      profile_bio
      public_keys
      wants_email
      gravatar_email
      profile_display_staff_badge
      profile_spoken_language_preference_code
      profile_twitter_username
    ]

    # Update user/profile params
    update_params = user_params.permit(permitted_user_params)
    @user.update(update_params) if @user.errors.none?

    if private_contribs.present?
      profile_settings = @user.profile_settings
      profile_settings.show_private_contribution_count = private_contribs
    end

    if enable_pro_badge.present? && @user.can_have_pro_badge?
      profile_settings = @user.profile_settings
      profile_settings.pro_badge_enabled = enable_pro_badge
    end

    if @user.errors.any?
      # If this was for an inline edit, render error via JSON
      if request.xhr?
        return render json: {message: @user.errors.full_messages.to_sentence}, status: :unprocessable_entity
      end

      flash[:error] = @user.errors.full_messages.to_sentence

      if params[:user][:profile_bio]
        redirect_to settings_user_profile_path
      else
        redirect_to settings_user_admin_path
      end
    else
      # If this was for an inline edit, render the details partial
      if request.xhr?
        result = platform_execute(InlineProfileQuery, variables: {
          user_id: current_user.global_relay_id,
          mutualFollowersEnabled: GitHub.flipper[:mutual_followers].enabled?(current_user),
        })
        return render partial: "users/profile_details", locals: {user: result.node}
      end

      if params[:user][:profile_bio] ||
          params[:user][:profile_hireable] ||
          params[:user][:profile_display_staff_badge] ||
          params[:user][:profile_spoken_language_preference_code] ||
          private_contribs.present?
        flash[:notice] = "Profile updated successfully"
        if params[:return_to] == "profile"
          redirect_to user_path(@user)
        else
          flash[:profile_updated] = true
          redirect_to settings_user_profile_path
        end
      else
        flash[:notice] = "Account updated successfully"
        if params[:return_to] == "back"
          redirect_to :back
        else
          redirect_to settings_user_admin_path
        end
      end
    end
  end

  # Inline validation check
  def password_check
    respond_to do |format|
      format.html_fragment do
        user = if logged_in?
          User.find(current_user.id)
        else
          User.new
        end

        user.readonly!
        user.password = params[:value]

        # trigger validations, we only care about the password validation and
        # not e.g. login or any others.
        user.valid?

        return head :ok if user.errors[:password].empty?

        error_message = user.errors[:password].to_sentence
        render body: "Password #{error_message}", status: 422, content_type: "text/fragment+html"
      end
    end
  end

  def change_password
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

    @user = current_user
    user_params = params.require(:user)
    password_params = user_params.permit(:old_password, :password, :password_confirmation)

    @user.change_password(
      old_password: password_params[:old_password],
      password: password_params[:password],
      password_confirmation: password_params[:password_confirmation],
    )

    if @user.errors.any?
      if @user.provided_weak_password?
        flash[::CompromisedPassword::WEAK_PASSWORD_KEY] = true
      else
        flash[:error] = @user.errors.full_messages.to_sentence
      end
    else
      # Log user out and back in.
      logout_user :password_changed

      # If the device is verified no updated is needed.
      # If the device is unverified, we shouldn't verify it
      login_user @user, sign_in_verification_method: :nil, client: :password_changed
      clear_weak_password_session_variable

      # Notify the user that their password changed.
      flash[:notice] = "Password changed successfully."
      GitHub.dogstats.increment("user", tags: [
        "action:update",
        "password_changed:true",
        "from_session_revocation:#{!!params[:session_revoked]}",
        "recent_security_checkup:#{@user.recently_took_action_on_security_checkup?}",
      ])
    end

    redirect_to settings_user_security_path
  end

  def follow
    toggle_follow do |target_user|
      current_user.follow(target_user, context: "user_profile")
      GitHub.dogstats.increment("user", tags: ["action:follow"])
    end
  end

  def unfollow
    toggle_follow do |target_user|
      current_user.unfollow(target_user, context: "user_profile")
      GitHub.dogstats.increment("user", tags: ["action:unfollow"])
    end
  end

  def destroy
    unless current_user.permit_deletion?(current_user)
      flash[:error] = "Cannot delete account. Please contact support."
      return redirect_to settings_user_admin_path
    end

    unless account_deletion_phrase_verified? params[:confirmation_phrase]
      flash[:error] = "The confirmation phrase didn’t match. Please enter it again."
      return redirect_to settings_user_admin_path
    end

    current_user.instrument :destroy
    current_user.async_destroy

    logout_user :account_destroy

    if request.xhr?
      head 200
    else
      redirect_to "/", notice: "Account successfully deleted."
    end
  end

  def rename
    GitHub.context.push({
      hydro: { actor: current_user }, # nested to avoid event_context serialization
      spamurai_form_signals: spamurai_form_signals,
    })

    @user = User.find_by_id(current_user.id)

    if @user.rename(params[:login])
      render "account/rename"
    else
      error_message = @user.errors[:login].to_sentence
      flash[:error] = "Username #{error_message}"
      redirect_to settings_user_admin_path
    end
  end

  def spammer
    render "users/spammer"
  end

  def billing
    redirect_to settings_user_billing_url
  end

  def organizations_info
    organizations_with_access = current_user.organizations_info_sorted_hash
    installable_marketplace_apps = current_user.installable_marketplace_apps_hash

    respond_to do |format|
      format.html do
        render partial: "repositories/new/orgs_select_menu", locals: {
            organizations_info: organizations_with_access,
            installable_marketplace_apps: installable_marketplace_apps,
            owner: @new_repository_owner,
        }
      end
    end
  end

  def dismiss_notice
    current_user.deactivate_notice(params[:notice_name])
    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  def dismiss_repository_notice
    current_user.dismiss_repository_notice(params[:notice_name],
                                           repository_id: params[:repository_id])
    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  def hide_jobs
    current_user.hide_jobs_until = Time.now + 6.months
    current_user.save
    redirect_to :back
  end

  def organizations
    redirect_to settings_user_organizations_url
  end

  def ignore_user
    other_user = User.find_by_login(params[:login])

    # Users should not be able to block themselves
    if other_user && other_user != current_user
      current_user.block other_user

      # Remove graph caches for contributions on blocker's repos
      ClearGraphDataOnBlockJob.perform_later(current_user.id, other_user.id)

      # Special handling for user blocking from settings pgae
      if from_settings_page?(settings_user_blocks_path)
        respond_to do |format|
          format.html do
            if request.xhr?
              ignored = current_user.ignored_users.where(ignored_id: other_user.id).first
              render partial: "settings/user_blocks/blocked_users",
                locals: { user_ignore: ignored }
            else
              redirect_to settings_user_blocks_path
            end
          end
        end
      else
        redirect_to user_path(other_user)
      end
    else
      head 404
    end
  end

  def unignore_user
    if other_user = User.find_by_login(params[:login])
      current_user.unblock other_user

      # Remove graph caches for contributions on blocker's repos
      ClearGraphDataOnBlockJob.perform_later(current_user.id, other_user.id)

      if from_settings_page?(settings_user_blocks_path)
        redirect_to settings_user_blocks_path
      else
        redirect_to user_path(other_user)
      end
    else
      head 404
    end
  end

  def suggestions
    search_term = params[:q]

    headers["Cache-Control"] = "no-cache, no-store"

    blockable_users = User.search(search_term, limit: 10).select do |user|
      current_user.can_block(user)
    end

    respond_to do |format|
      format.html_fragment do
        render partial: "settings/user_blocks/suggestions", formats: :html, locals: {
          suggestions: blockable_users,
        }
      end
      format.html do
        render partial: "settings/user_blocks/suggestions",
           locals: { formats: :html, suggestions: blockable_users }
      end
    end
  end

  def set_protocol
    if current_user.set_protocol_preference(
        params[:protocol_type], params[:protocol_selector])
      current_user.save!
    end
    head :ok
  end

  def update_checkout_preference
    pref = PullRequestCheckoutPreference.new(current_user, parsed_useragent.platform)
    success = pref.set(params[:type])

    if request.xhr?
      head success ? :ok : :bad_request
    else
      redirect_to :back
    end
  end

  def record_developer_tools_survey_popup_shown
    current_user.dismiss_notice("desktop_survey_popup")

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  def set_private_contributions_preference
    @user = current_user
    @selected_tab = params[:tab]
    value = params[:user][:show_private_contribution_count]
    profile_settings = @user.profile_settings
    profile_settings.show_private_contribution_count = value
    back_to_profile = params[:return_to] == "profile"
    flash_key = :notice
    flash_key = :contribution_graph_notice if back_to_profile

    if @user.errors.any?
      flash[:error] = @user.errors.full_messages.to_sentence
    else
      if params[:user][:show_private_contribution_count] == "1"
        flash[flash_key] = "Visitors will now see your public and " +
                           "anonymized private contributions."
      else
        flash[flash_key] = "Visitors will now see only your public " +
                           "contributions."
      end
    end

    if back_to_profile
      redirect_to user_path(@user)
    else
      redirect_to settings_user_profile_path
    end
  end

  private

  helper_method :this_user

  # This is the private method for toggling follow for the controllers
  #
  # stats_key - this is what we're using to track the follow interaction
  def toggle_follow
    if target = User.find_by_login(params[:target])
      yield target
    else
      return render_404
    end

    if request.xhr?
      render json: { count: number_with_delimiter(target.followers_count) }
    else
      redirect_to :back
    end
  end

  # Ensure the user specified via the login in the URL is a real User.
  def ensure_user_exists
    unless this_user
      if request.xhr?
        head :not_found
      else
        render_404
      end
    end
  end

  # Private: Ensure `this_user` is not an organization.
  def ensure_not_organization
    return unless this_user

    head :not_acceptable if this_user.organization?
  end

  # Public: Should the 'Activity overview' section be shown on this user's profile?
  def activity_overview_enabled?
    profile_settings = this_user.profile_settings
    profile_settings.activity_overview_enabled?
  end

  def this_user
    return @user if defined?(@user)
    @user = User.find_by_login(params[:user_id]) if (params[:user_id] && GitHub::UTF8.valid_unicode3?(params[:user_id]))
  end

  def account_deletion_phrase_verified?(phrase)
    return false if phrase.blank?
    User.account_deletion_phrase.downcase == phrase.downcase
  end

  def ensure_valid_email
    return unless params[:user] && params[:user][:profile_email].present?
    valid_emails = current_user.possible_profile_emails
    # Ideally we wouldn't allow the existing profile email unless it was
    # verified. But, changing that behavior would probably surprise/confuse
    # existing users. But, this does prevent a bad actor from purposefully
    # adding a new arbitrary profile email address going forward.
    valid_emails << current_user.profile_email
    render_404 unless valid_emails.include?(params[:user][:profile_email])
  end

  def update_rate_limit_filter
    return false unless params[:user]
    params[:user][:old_password].present?
  end

  def update_rate_limit_key
    "users_update_limiter:#{current_user.id}"
  end

  def update_rate_limit_log_key
    "update-user-#{current_user.id}"
  end

  def update_rate_limit_record
    GitHub.dogstats.increment("rate_limited", tags: ["action:user_update"])
  end

  def from_settings_page?(pathname)
    request.referrer.present? && URI(request.referrer).path == pathname
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_user.present?
    this_user
  end

  def find_new_repository_owner
    owner_id = params.fetch(:owner_id, nil).to_s
    @new_repository_owner = if owner_id.blank? || owner_id == current_user.id.to_s
      current_user
    else
      # will raise an exception if the owner_id param doesn't point to
      # an Organization that current_user belongs to
      current_user.organizations.find(owner_id)
    end
  end

  def ensure_user_is_a_user
    render_404 unless this_user.user?
  end

  def ensure_user_not_hidden_from_viewer
    render_404 if this_user.hide_from_user?(current_user)
  end

  def delete_sponsorship_rollback_notice
    current_user&.delete_notice(:sponsorship_rollback)
  end
end
