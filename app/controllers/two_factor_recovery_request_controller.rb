# frozen_string_literal: true

class TwoFactorRecoveryRequestController < ApplicationController
  layout :authentication_layout

  # This controller does not access protected organization resources.
  skip_before_action :perform_conditional_access_checks

  before_action :not_github_enterprise

  before_action :require_valid_user, except: [:abort, :confirm_abort, :continue, :confirm_continue]

  before_action :require_current_request, except: [:start, :abort, :confirm_abort, :continue, :confirm_continue]

  OTP_SEND_LIMIT_OPTIONS = {
    max_tries: 5,
    ttl: 15.minutes,
  }

  # Window of time in which the OTP code is valid.
  ONE_TIME_PASSWORD_EXPIRY = 1.hour

  def get_partially_authenticated_two_factor_session
    session.delete(:two_factor_user)
  end

  def set_partially_authenticated_two_factor_session(user)
    session[:two_factor_user] = user.login
  end

  def not_github_enterprise
    return render_404 if GitHub.enterprise?
  end

  def require_valid_user
    login = get_partially_authenticated_two_factor_session
    return render_404 unless login

    @locked_out_user = User.find_by_login(login)
    return render_404 unless @locked_out_user

    return render_404 unless @locked_out_user.two_factor_credential.present?

    # clearing and recreating like this feels like an anti-pattern, but I've
    # borrowed this from SessionsController as that is representing the user in
    # this similar "partially authenticated" state - open to suggestions about
    # better ways to manage this state
    set_partially_authenticated_two_factor_session(@locked_out_user)
  end

  def require_current_request
    device = @locked_out_user.authenticated_devices.find_by_device_id(current_device_id)
    return render_404 unless device.present?

    @current_request = TwoFactorRecoveryRequest.find_request(@locked_out_user, device)
    return render_404 unless @current_request.present?
  end

  def otp_present?
    session[:one_time_password].present?
  end

  def otp_not_expired?
    session[:one_time_password_expiration].present? && session[:one_time_password_expiration].to_i >= Time.now.to_i
  end

  def otp_found?
    otp_present? && otp_not_expired?
  end

  def otp_matches?(otp)
    return false unless otp_found?

    otp == session[:one_time_password]
  end

  def token_valid_for_recovery?(oauth_access)
    return false unless oauth_access&.personal_access_token?
    return false unless oauth_access.user == @locked_out_user
    return false unless oauth_access.scopes.include? "repo"
    return false unless oauth_access.created_at < @current_request.created_at

    true
  end

  def request_completed
    GlobalInstrumenter.instrument("two_factor_account_recovery.updated",
      action_type: :REQUEST_COMPLETED,
      user: @current_request.user,
      evidence_type: @current_request.hydro_evidence_type,
    )

    @current_request.mark_as_complete!

    send_mailer_to_user
  end

  def start
    current_device = @locked_out_user.authenticated_devices.find_by_device_id(current_device_id)
    request = TwoFactorRecoveryRequest.find_request(@locked_out_user, current_device)

    unless request.present?
      @locked_out_user.two_factor_recovery_requests.create(requesting_device: current_device)
      clear_otp_state
      set_device_hash(current_device_id)
    end

    redirect_to two_factor_recovery_request_path
  end

  def prompt
    if @current_request.review_state == :ready_for_review
      return render "sessions/recovery/pending_review"
    end

    if @current_request.review_state == :evidence_missing || @current_request.review_state == :reviewed_by_staff
      return render_404
    end

    unless @current_request.otp_verified?
      if otp_found?
        return render "sessions/recovery/enter_otp"
      else
        return render "sessions/recovery/send_otp"
      end
    end

    current_device = @locked_out_user.authenticated_devices.find_by_device_id(current_device_id)
    verified_device = GitHub.sign_in_analysis_enabled? && current_device&.verified?

    threshold_for_secondary_evidence = @current_request.created_at

    ssh_keys_available = @locked_out_user.verified_keys.where("verified_at < ?", threshold_for_secondary_evidence).any?

    tokens =  @locked_out_user.oauth_accesses.personal_tokens.where("created_at < ?", threshold_for_secondary_evidence)
    tokens_with_repo_scope = tokens.filter { |token| token.scopes.include? "repo" }
    tokens_available = tokens_with_repo_scope.any?

    has_delegated_recovery_tokens = @locked_out_user.delegated_recovery_tokens.confirmed.exists?
    GitHub.dogstats.increment("two_factor_account_recovery.otp_verified", tags: ["has_delegated_recovery_tokens:#{has_delegated_recovery_tokens}"])

    return render "sessions/recovery/otp_verified", locals: {
      verified_device: verified_device,
      ssh_keys_available: ssh_keys_available,
      tokens_available: tokens_available,
    }
  end

  def send_otp
    rate_limit_key = "two-factor-recovery:#{@locked_out_user.id}"
    if RateLimiter.at_limit?(rate_limit_key, OTP_SEND_LIMIT_OPTIONS)
      flash[:error] = "Too many OTP codes requested. Try again later."
      return redirect_to two_factor_recovery_request_path
    end

    one_time_password = TwoFactorRecoveryRequest.generate_one_time_password
    AccountRecoveryMailer.send_otp(@locked_out_user, one_time_password).deliver_later

    GlobalInstrumenter.instrument("two_factor_account_recovery.updated",
      action_type: :OTP_SENT,
      user: @locked_out_user,
    )

    set_otp_state(one_time_password)

    flash[:notice] = "Recovery email sent"

    redirect_to two_factor_recovery_request_path
  end

  def verify_otp
    if AuthenticationLimit.at_any?(two_factor_login: @locked_out_user.login, increment: true, actor_ip: request.remote_ip)
      flash[:error] = "Too many incorrect OTP attempts. Try again later."
      return redirect_to two_factor_recovery_request_path
    end

    if otp_matches?(params[:otp])
      @current_request.otp_verified = true
      @current_request.save
      GlobalInstrumenter.instrument("two_factor_account_recovery.updated",
        action_type: :OTP_VERIFIED,
        user: @locked_out_user,
      )
    else
      flash[:error] = "Unable to verify one-time password"
    end

    redirect_to two_factor_recovery_request_path
  end

  def verify_device
    hash = get_device_hash
    if hash.present?
      # detect if the user somehow is trying to verify using a different device
      # than what was identified at the start
      unless SecurityUtils.secure_compare(current_device_id, hash)
        GitHub.dogstats.increment("two_factor_account_recovery.verify_device", tags: ["kind:mismatch"])
        return render_404
      end
    else
      # there's a possibility that the user restarted the flow with an existing
      # request on the same device, and the session value is missing. log this
      # rather than throwing an error because we don't have a value to compare.
      GitHub.dogstats.increment("two_factor_account_recovery.verify_device", tags: ["kind:missing"])
    end

    current_device = @locked_out_user.authenticated_devices.find_by_device_id(current_device_id)

    verified = current_device&.verified?

    if verified
      @current_request.authenticated_device = current_device
      request_completed
      flash[:notice] = "Device verified and request submitted!"
    else
      flash[:error] = "Unable to verify current device"
    end

    redirect_to two_factor_recovery_request_path
  end

  def enter_ssh_key
    render "sessions/recovery/enter_ssh_key"
  end

  def verify_ssh_key
    token = GitHub::Authentication::SignedAuthToken.verify(
      token: params[:token].gsub(/[[:space:]]+/, ""),
      scope: GitHub::SshVerification::SSH_VERIFICATION_TOKEN_SCOPE
    )

    public_key = @locked_out_user.public_keys.find_by_id(token.data["public_key"]) if token.valid?

    if public_key_valid_for_account_recovery?(public_key)
      @current_request.public_key = public_key
      request_completed
      flash[:notice] = "SSH key verified and request submitted!"
      redirect_to two_factor_recovery_request_path
    else
      flash[:error] = "Unable to verify SSH key"
      render "sessions/recovery/enter_ssh_key"
    end
  end

  def enter_token
    render "sessions/recovery/enter_token"
  end

  def verify_token
    oauth_access = OauthAccess.find_by_token(params[:token])

    if token_valid_for_recovery?(oauth_access)
      @current_request.oauth_access = oauth_access
      request_completed
      flash[:notice] = "Token verified and request submitted!"
      redirect_to two_factor_recovery_request_path
    else
      flash[:error] = "Unable to verify personal access token"
      render "sessions/recovery/enter_token"
    end
  end

  def abort
    request = TwoFactorRecoveryRequest.find_by_id(params[:id])

    return render_404 unless request.present?

    result = TwoFactorRecoveryRequest.verify_abort_token(params[:token])

    return render_404 unless result.valid? && result.user == request.user

    render "sessions/recovery/confirm_abort"
  end

  def confirm_abort
    request = TwoFactorRecoveryRequest.find_by_id(params[:id])

    return render_404 unless request.present?

    result = TwoFactorRecoveryRequest.verify_abort_token(params[:token])

    return render_404 unless result.valid? && result.user == request.user

    user = request.user
    user.two_factor_recovery_requests.where(reviewer: nil).each do |request|
      request.instrument_abort
      TwoFactorRecoveryRequestCleanupJob.perform_later(request: request)
    end

    flash[:notice] = "Recovery review aborted"
    render "sessions/recovery/aborted"
  end

  def continue
    request = TwoFactorRecoveryRequest.find_by_id(params[:id])

    return render_404 unless request.present?

    result = TwoFactorRecoveryRequest.verify_complete_token(params[:token])

    return render_404 unless result.valid? && result.user == request.user

    return redirect_to two_factor_recovery_request_path unless request.review_state == :reviewed_by_staff

    return render_404 unless request.approved?

    render "sessions/recovery/completed"
  end

  def confirm_continue
    request = TwoFactorRecoveryRequest.find_by_id(params[:id])

    return render_404 unless request.present? && request.approved?

    result = TwoFactorRecoveryRequest.verify_complete_token(params[:token])

    return render_404 unless result.valid? && result.user == request.user

    user = User.find_by_id(result.user.id)

    remove_user_from_2fa_required_orgs(user, request)

    flash[:notice] = "It is recommended to re-enable 2FA on your account and store your recovery codes for future use"

    TwoFactorRecoveryRequestCleanupJob.perform_later(request: request)
    redirect_to settings_user_security_path
  end

  # since we're recovering 2FA we don't need to check for org enforcement
  # and prompt for setup
  def two_factor_enforceable
    :no
  end

  private

  def set_device_hash(device_hash)
    return if device_hash.nil?
    session[:recovery_request_device] = device_hash
  end

  def get_device_hash
    session[:recovery_request_device]
  end

  def clear_otp_state
    session.delete(:one_time_password)
    session.delete(:one_time_password_expiration)
  end

  def set_otp_state(otp)
    session[:one_time_password] = otp
    session[:one_time_password_expiration] = Time.now.to_i + ONE_TIME_PASSWORD_EXPIRY.to_i
  end

  def user_agent
    # The user-agent is already in the user's session, if they have one;
    # use that to avoid a second parse.
    ua = if user_session && user_session.ua
      user_session.ua
    else
      Browser.new(request.env["HTTP_USER_AGENT"])
    end

    {
      browser: "#{ua.name} #{ua.full_version} #{"(mobile)" if ua.device.mobile?}".strip,
    }
  end

  def public_key_valid_for_account_recovery?(public_key)
    return false unless public_key&.can_verify_account_ownership?
    return false unless public_key.verified_at < @current_request.created_at

    true
  end

  def remove_user_from_2fa_required_orgs(user, request)
    request.instrument_two_factor_destroy

    GlobalInstrumenter.instrument("two_factor_account_recovery.updated",
      action_type: :TWO_FACTOR_REMOVED,
      user: user,
      evidence_type: request.hydro_evidence_type,
    )

    DisableUserTwoFactorCredentialsJob.perform_later(user.id)
  end

  def send_mailer_to_user
    abort_token = @current_request.generate_abort_token
    AccountRecoveryMailer.confirm_request_completed(
      @locked_out_user,
      @current_request.id,
      abort_token,
    ).deliver_later
  end
end
