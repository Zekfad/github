# frozen_string_literal: true

class PinnedIssuesController < AbstractRepositoryController
  before_action :login_required
  before_action :write_access_required

  def create
    if issue(params[:id]).pin(actor: current_user)
      flash[:notice] = "The issue has been pinned."
    else
      flash[:error] = "The issue could not be pinned."
    end
    redirect_to :back
  end

  def destroy
    if issue(params[:id]).unpin(actor: current_user)
      flash[:notice] = "The issue has been unpinned."
    else
      flash[:error] = "The issue could not be unpinned."
    end
    redirect_to :back
  end

  def prioritize
    issue_ids = params[:issue_ids] || []
    if issue_ids.present?
      current_repository.reorder_pinned_issues(issue_ids.map(&:to_i))
    end
    head :ok
  end

  private

  def issue(number)
    @issue ||= current_repository.issues.find_by!(number: number)
  end

  def write_access_required
    render_404 unless current_repository.can_pin_issues?(current_user)
  end
end
