# frozen_string_literal: true

class GeneratedPagesController < AbstractRepositoryController
  areas_of_responsibility :pages

  DEFAULT_BODY = File.read(File.expand_path("../../views/generated_pages/default_body.md", __FILE__))

  before_action :repo_admin_only
  before_action :add_csp_exceptions, only: [:themes, :preview]
  CSP_EXCEPTIONS = {
    frame_src: [SecureHeaders::CSP::SELF],
    style_src: [SecureHeaders::CSP::SELF],
  }
  layout :repository_layout

  def create
    current_repository.pages_create current_user, params[:page], true

    page_type = current_repository.is_user_pages_repo? ? "user" : "project"

    if (theme = params[:page][:theme_slug]).present?
      GitHub.dogstats.increment("pages", tags: ["action:create", "theme:#{theme}"])
    end

    flash[:notice] = <<-END
      Your #{page_type} page has been created at #{current_repository.gh_pages_url}.
      Read more at #{GitHub.help_url}/pages.
    END

    redirect_to repository_path(current_repository) unless performed?
  end

  def new
    @page_params = params[:page] || {}
    @readme      = current_repository.preferred_readme

    # Grab the serialized params if they exist
    if current_repository.has_gh_pages?
      revision = current_repository.heads.find(current_repository.pages_branch).target_oid
      file = current_repository.blob(revision, "params.json")

      if file.present?
        decoded_params = Yajl::Parser.parse(file.data, symbolize_keys: true)
        @page_params.reverse_merge!(decoded_params || {})
      end
    end

    populate_with_defaults! @page_params
    @page_params[:tagline] ||= current_repository.description

    @editor = GitHub::Unsullied::Editor.new("page")

    @editor.add_params :body,
      value: @page_params[:body],
      format: "markdown"

    render "generated_pages/new"
  end

  def preview
    unless params[:theme_slug]
      redirect_to generated_pages_new_path(current_repository.owner, current_repository)
      return
    end

    theme = GitHub::Pages::Theme.find(params[:theme_slug])
    template_params = params.merge(
      stylesheet_paths: theme.stylesheet_paths(:web),
      javascript_paths: theme.javascript_paths(:web),
      preview: true)

    SecureHeaders.override_x_frame_options(request, SecureHeaders::XFrameOptions::SAMEORIGIN)
    SecureHeaders.override_content_security_policy_directives(
      request,
      frame_ancestors: [SecureHeaders::CSP::SELF],
    )

    render html: GitHub::Pages.content_for(
      current_repository, current_user, template_params).html_safe # rubocop:disable Rails/OutputSafety
  end

  def themes
    return redirect_to(generated_pages_new_path(current_repository.owner, current_repository)) unless params[:page]

    @page = page_params
    populate_with_defaults! @page

    @themes = GitHub::Pages::Theme.all

    # By default, the repositories layout is rendered, so we need to tell it to
    # use the application template so we don't get the extra repo markup.
    render "generated_pages/themes", layout: "application"
  end

  private

  def page_params
    params.require(:page).permit %i[name tagline body]
  end

  # Populate the provided hash with defaults.
  #
  # params_hash - Hash to populate.
  #
  # Returns the passed Hash.
  def populate_with_defaults!(params_hash)
    params_hash[:name] = current_repository.name.humanize if params_hash[:name].blank?
    params_hash[:body] = DEFAULT_BODY if params_hash[:body].blank?
    params_hash
  end

  def repo_admin_only
    render_404 unless current_repository.adminable_by? current_user
  end
end
