# frozen_string_literal: true

class PullRequestsController < AbstractRepositoryController
  # All other actions will be mapped to the pull_requests service
  map_to_service :merge, only: [:merge, :merge_button]

  statsd_tag_actions only: [:comment, :create, :merge, :merge_button, :revert, :show, :apply_suggestions]

  include ShowPartial, PrefetchHelper, ProgressiveTimeline, DiscussionPreloadHelper,
    ControllerMethods::Diffs, MarketplaceHelper, TimelineHelper, HydroHelper, ActionsContentPolicy

  helper :compare
  before_action :login_required, only: [:create, :comment, :merge_button, :dismiss_protip, :ready_for_review, :convert_to_draft, :change_base, :apply_suggestions]
  before_action :login_required_redirect_for_public_repo, only: [:new]
  before_action :writable_repository_required,
    except: [:new, :show, :show_partial, :merge_button, :diff, :patch, :merge_button_matrix, :cleanup, :undo_cleanup, :ready_for_review, :convert_to_draft, :changes_since_last_review]
  before_action :check_for_empty_repository, only: [:create, :new]
  before_action :ensure_pull_head_pushable, only: [:cleanup, :undo_cleanup]
  before_action :content_authorization_required, only: [:create, :merge, :apply_suggestions]
  skip_before_action :cap_pagination, unless: :robot?
  before_action :add_dreamlifter_csp_exceptions, only: [:show]

  layout :repository_layout

  param_encoding :create, :base, "ASCII-8BIT"
  param_encoding :create, :head, "ASCII-8BIT"
  param_encoding :new, :range, "ASCII-8BIT"

  def new
    redirect_to compare_path(current_repository, params[:range], true)
  end

  def create
    repo = current_repository
    return render_404 unless repo
    return if reject_bully_for?(repo)
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

    unsafe_params = params.permit!.to_h.with_indifferent_access
    unsafe_pull_request_params = unsafe_params[:pull_request]

    issue = build_issue unsafe_params

    if issue.present?
      issue.title = unsafe_pull_request_params[:title] unless issue.title.present?
      issue.body = unsafe_pull_request_params[:body] unless issue.body.present?
    end

    options = unsafe_params.slice :base, :head
    options[:user] = current_user
    options[:issue] = issue
    options[:collab_privs] = !!unsafe_params[:collab_privs]
    options[:reviewer_user_ids] = unsafe_params[:reviewer_user_ids]
    options[:reviewer_team_ids] = unsafe_params[:reviewer_team_ids]

    options[:draft] = unsafe_params[:draft] == "on"

    if GitHub.flipper[:pull_request_revisions].enabled?(current_repository) || GitHub.flipper[:pull_request_revisions].enabled?(current_repository.owner)
      options[:draft] = unsafe_params.key?(:ready_for_review) && unsafe_params[:ready_for_review] != "on"
    end

    begin
      @pull_request = PullRequest.create_for!(repo, options)
      @comparison   = @pull_request.comparison
      @issue        = @pull_request.issue

      GitHub.instrument "pull_request.create", user: current_user
      instrument_issue_creation_via_ui(@pull_request)
      instrument_saved_reply_use(unsafe_params[:saved_reply_id], "pull_request")
      # Keep track of if a pull request targets the repo's
      # default branch, or one in progress.
      if options[:base].strip == "#{repo.owner}:#{repo.default_branch}"
        GitHub.dogstats.increment("pull_request", tags: ["action:create", "target:default"])
      else
        GitHub.dogstats.increment("pull_request", tags: ["action:create", "target:other"])
      end

      if unsafe_params[:quick_pull].present?
        type = @pull_request.cross_repo? ? "cross" : "same"
        GitHub.dogstats.increment("pull_request", tags: ["action:quick_create", "type:#{type}"])
      end

      redirect_to pull_request_path(@pull_request, repo)
    rescue ActiveRecord::RecordInvalid => e
      flash[:error] = "Pull request creation failed. #{human_failure_message(e)}"
      range = [options[:base], options[:head]].compact.join("...")
      redirect_to compare_path(repo, range, true)
    end
  end

  ShowConversationQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $timelineSince: DateTime,
      $timelinePageSize: Int,
      $syntaxHighlightingEnabled: Boolean = true,
      $deferCollapsedThreads: Boolean,
      $focusedReviewThread: ID,
      $focusedReviewComment: ID,
      $hasFocusedReviewThread: Boolean = false
      $hasFocusedReviewComment: Boolean = false
      $scopedItemTypes: [IssueTimelineItemsItemType!]
    ) {
      node(id: $id) {
        ...Views::PullRequests::Conversation::PullRequest
      }
    }
  GRAPHQL

  MobileShowConversationQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on Reactable {
          ...Views::Mobile::Discussions::Reactions::Subject
          ...Views::Mobile::PullRequests::SidebarMetadata::PullRequest
          ...CommentsHelper::CommentFragment
        }
        ...Issues::FormButtonsView::FormButtonClosable
      }
    }
  GRAPHQL

  ShowCommitsQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::PullRequests::Commits::PullRequest
      }
    }
  GRAPHQL

  CommitDetailQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Commit::PullCommitDetail
      }
    }
  GRAPHQL

  ShowChecksQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $annotationsLimit: Int!, $after: String) {
      node(id: $id) {
        ...Views::Checks::Show
      }
    }
  GRAPHQL

  around_action :record_show_metrics, only: [:show]
  private def record_show_metrics
    action_start_time = GitHub::Dogstats.monotonic_time

    yield

    GitHub.dogstats.timing_since("pull_request.show", action_start_time, tags: [
      "mobile:#{!!show_mobile_view?}",
      "logged_in:#{!!logged_in?}",
      "tab:#{specified_tab}",
      "dreamlifter_enabled:#{GitHub.actions_enabled?}",
    ])
  end

  def show
    unsafe_params = params.permit!.to_h.with_indifferent_access

    @mobile_view_available = !responsive_pull_requests_enabled?

    redirect_or_error = ensure_valid_pull_request

    if performed?
      return redirect_or_error
    end

    set_hovercard_subject(@pull)

    if unsafe_params[:range]
      env["pull_request.timeout_reason"] = "compute_diff"

      if oids = parse_show_range_oid_components(@pull, unsafe_params[:range])
        oid1, oid2 = oids
      else
        return render "pull_requests/bad_range", status: :not_found
      end

      allowed_commits = @pull.changed_commit_oids
      if !(oid1.nil? || allowed_commits.include?(oid1)) || !allowed_commits.include?(oid2)
        return render "pull_requests/bad_range", status: :not_found
      end

      if oid1 && (oid1 == oid2 || !@pull.repository.rpc.descendant_of([[oid2, oid1]]).values.first)
        return render "pull_requests/bad_range", status: :not_found
      end

      expected_canonical_range = oid1 ? "#{oid1}..#{oid2}" : "#{oid2}"
      if unsafe_params[:range] != expected_canonical_range
        return redirect_to(range: expected_canonical_range)
      end

      if unsafe_params[:tab] == "commits"
        @specified_tab = "files"
        oid1 = current_repository.commits.find(oid2).parent_oids.first
      end

      @comparison = @pull.historical_comparison
      merge_base_oid = @comparison.compare_repository.best_merge_base(@pull.base_sha, oid2)

      if merge_base_oid.nil?
        return render "pull_requests/orphan_commit", status: :not_found, locals: { oid2: oid2 }
      end

      oid1 ||= @pull.compare_repository.best_merge_base(oid2, merge_base_oid) if merge_base_oid
      unless @pull_comparison = PullRequest::Comparison.find(pull: @pull, start_commit_oid: oid1, end_commit_oid: oid2, base_commit_oid: merge_base_oid)
        return render "pull_requests/bad_range", status: :not_found
      end

      load_diff

      env["pull_request.timeout_reason"] = nil
    else
      @comparison = @pull.comparison

      if specified_tab == "files"
        return if pjax? && GitHub.flipper[:js_http_cache_headers].enabled?(current_user) && !stale?(@pull, template: false)

        env["pull_request.timeout_reason"] = "compute_diff"

        start_oid, end_oid = @pull.merge_base, @pull.head_sha
        if start_oid && end_oid
          begin
            start_commit, end_commit = @pull.compare_repository.commits.find([start_oid, end_oid])
            @pull_comparison = PullRequest::Comparison.new(pull: @pull, start_commit: start_commit, end_commit: end_commit, base_commit: start_commit)

            load_diff
          rescue GitRPC::ObjectMissing
          end
        end

        env["pull_request.timeout_reason"] = nil
      end
    end

    prefetch_deferred do
      if specified_tab == "files" && @pull_comparison && logged_in?
        unless @pull.base_repository.pull_request_revisions_enabled?
          @pull_comparison.mark_as_seen(user: current_user)
        end
        mark_thread_as_read @pull.issue
      elsif specified_tab == "discussion"
        mark_thread_as_read @pull.issue
      end
    end

    if prefetch_viewed?
      return head(:no_content)
    end

    if show_mobile_view?
      prepare_for_rendering(timeline: specified_tab == "discussion", pull_comparison: @pull_comparison)
      prepare_for_rendering_files(pull_comparison: @pull_comparison)
    elsif specified_tab == "files"
      prepare_for_rendering_files(pull_comparison: @pull_comparison)
    end

    unless "discussion" == specified_tab
      override_analytics_location "/<user-name>/<repo-name>/pull_requests/show/#{specified_tab}"
    end

    respond_to do |format|
      format.html do
        if show_mobile_view? && (request_category != "raw") && specified_tab != "checks"
          # explicitly enqueue merge commit creation on read on mobile
          @pull.enqueue_mergeable_update

          # include the graphql node for reactions (and future re-factor)
          pull_node = platform_execute(MobileShowConversationQuery, variables: {
            id: @pull.global_relay_id,
            timelinePageSize: initial_timeline_page_size,
            syntaxHighlightingEnabled: syntax_highlighted_diffs_enabled?,
          }).node

          return render_template_view("mobile/pull_requests/show", Mobile::PullRequests::ShowPageView, {
            pull: @pull,
            diffs: @pull_comparison.try(:diffs),
            tab: specified_tab,
            visible_timeline_items: visible_timeline_items,
            showing_full_timeline: showing_full_timeline?,
            pull_node: pull_node,
          }, layout: "mobile/application")
        end

        if unsafe_params[:shdds]
          @syntax_highlighted_diffs_forced = true
          render_to_string
          head :no_content
        else
          if logged_in?
            @current_review = @pull.latest_pending_review_for(current_user)
          end

          if specified_tab == "files"
            if @pull_comparison.nil?
              render "pull_requests/files_unavailable"
            else
              if @pull_comparison.commits.count == 1 && unsafe_params[:tab] == "commits"
                commit_node = platform_execute(CommitDetailQuery, variables: { id: @pull_comparison.commits.first.global_relay_id }).node
              end
              render "pull_requests/files", locals: {
                commit_node: commit_node,
                show_checks_status: GitHub.actions_enabled?,
              }
            end
          elsif specified_tab == "commits"
            pull_node = platform_execute(ShowCommitsQuery, variables: {
              id: @pull.global_relay_id,
            }).node
            render "pull_requests/commits", locals: { pull_node: pull_node }
          elsif specified_tab == "checks"
            selected_check_run = CheckRun.find_by(id: unsafe_params[:check_run_id]) if unsafe_params[:check_run_id]

            if unsafe_params[:check_run_id] && !@pull.changed_commit_oids.include?(selected_check_run&.head_sha)
              flash[:notice] = "No check run found with ID #{unsafe_params[:check_run_id]} for this pull request."
              redirect_to "#{pull_request_path(@pull)}/checks" and return
            end

            Commit.prefill_combined_statuses(@pull.changed_commits, current_repository)

            if selected_check_run || params[:sha]
              sha = selected_check_run&.head_sha || params[:sha]
              commit = @pull.changed_commits.find { |commit| commit.oid == sha }

              unless commit
                flash[:notice] = "No commit was found with sha #{sha} for this pull request."
                redirect_to "#{pull_request_path(@pull)}/checks" and return
              end
            else
              sha = @pull.head_sha
              commit = @pull.changed_commits.find { |commit| commit.oid == sha }
            end

            check_suites       = @pull.matching_check_suites(head_sha: sha)
            selected_check_run ||= check_suites.first&.latest_check_runs&.first

            if selected_check_run
              variables = { id: selected_check_run.global_relay_id, annotationsLimit: CheckAnnotation::MAX_PER_REQUEST }
              graphql_check_run = platform_execute(ShowChecksQuery, variables: variables).node
            end

            commit_check_runs = commit.nil? ? [] : CheckRun.for_sha_and_repository_id(commit.oid, current_repository.id)
            render "checks/show", locals: {
              commit: commit,
              selected_check_run: selected_check_run,
              check_suites: check_suites,
              blankslate: show_blankslate?(check_suites, selected_check_run, current_repository),
              workflows_loading: workflows_loading?(check_suites, selected_check_run),
              graphql_check_run: graphql_check_run,
              commit_check_runs: commit_check_runs,
              pull: @pull,
            }
          else
            pull_node = pull_node_for(ShowConversationQuery)
            render "pull_requests/conversation", locals: { pull_node: pull_node }
          end
        end
      end
      if unsafe_params[:tab] == "commits"
        format.diff { commit_diff(oid2) }
        format.patch { commit_patch(oid2) }
      end
    end
  end

  def changes_since_last_review
    redirect_or_error = ensure_valid_pull_request
    return redirect_or_error if performed?

    unless logged_in?
      return redirect_to pull_request_diff_range_path(@pull)
    end

    unless most_recent_review = @pull.latest_non_pending_review_for(current_user)
      return redirect_to pull_request_diff_range_path(@pull)
    end

    unless most_recent_review.pull_request_has_changed? && most_recent_review.applies_to_current_diff?
      return redirect_to pull_request_diff_range_path(@pull)
    end

    redirect_to pull_request_diff_range_path(@pull, range: [most_recent_review.head_sha, "HEAD"])
  end

  def commit_diff(oid)
    redirect_to commit_path(oid, current_repository) + ".diff"
  end

  def commit_patch(oid)
    redirect_to commit_path(oid, current_repository) + ".patch"
  end

  VALID_REASONS = %w[
    view-more-button
    expose-fragment
  ].freeze

  FormActionChecksQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Issues::FormActions::FormActionFragment
      }
    }
  GRAPHQL

  StateButtonChecksQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Issues::StateButtonWrapper::StateButtonFragment
      }
    }
  GRAPHQL

  def show_partial
    partial = params[:partial]
    return head :not_found unless valid_partial?(partial)

    GitHub.dogstats.time("view", tags: ["subject:pull_request", "action:show_partial_find"]) do
      @pull = PullRequest.with_number_and_repo(params[:id].to_i, current_repository)
    end

    return head :not_found unless @pull

    if partial == "pull_requests/form_actions"
      issue_node = pull_node_for(FormActionChecksQuery)
    elsif partial == "pull_requests/state_button_wrapper"
      issue_node = pull_node_for(StateButtonChecksQuery)
    end

    GitHub.dogstats.time("view", tags: ["subject:pull_request", "action:show_partial_render"]) do
      respond_to do |format|
        format.html do
          render partial: partial, object: @pull, layout: false, locals: { # rubocop:disable GitHub/RailsControllerRenderLiteral
            pull: @pull,
            issue_node: issue_node,
            merge_type: params[:merge_type],
            sticky: params[:sticky] == "true",
          }
        end
      end
    end
  end

  def show_partial_comparison
    partial = params[:partial]
    return head :not_found unless valid_partial?(partial)

    unless pull = PullRequest.with_number_and_repo(params[:id].to_i, current_repository)
      return head :not_found
    end

    unless pull_comparison = PullRequest::Comparison.find(pull: pull, start_commit_oid: params[:start_commit_oid], end_commit_oid: params[:end_commit_oid], base_commit_oid: params[:base_commit_oid])
      return head :not_found
    end

    GitHub.dogstats.time("view", tags: ["subject:pull_request", "action:show_partial_render"]) do
      respond_to do |format|
        format.html do
          render partial: partial, locals: { pull: pull, pull_comparison: pull_comparison } # rubocop:disable GitHub/RailsControllerRenderLiteral
        end
      end
    end
  end

  def show_toc
    pull = PullRequest.with_number_and_repo(params[:id], current_repository)
    return head :not_found unless pull

    return head :not_found unless valid_sha_param?(:sha1) &&
                                  valid_sha_param?(:sha2) && params[:sha2].present? &&
                                  valid_sha_param?(:base_sha)

    diff_options = { base_sha: params[:base_sha] }
    diff = GitHub::Diff.new(pull.compare_repository, params[:sha1], params[:sha2], diff_options)

    respond_to do |format|
      format.html do
        render partial: "pull_requests/diffbar/toc_menu_items",
          layout: false,
          locals: {
            summary_delta_views: diff.summary.deltas.map { |d| Diff::SummaryDeltaView.new(d) }
          }
      end
    end
  end

  def cleanup
    # The branch was deleted by someone else before this user
    # clicked the button. Send us to the bottom.
    if !@pull.head_ref_exist?
      raise Git::Ref::NotFound
    end

    result = @pull.cleanup_head_ref(current_user,
                              reflog_data: request_reflog_data("pull request branch delete button"),
                              allow_updating_dependents: true)

    GitHub.dogstats.increment("pull_request", tags: ["action:cleanup", "#{result ? "result:success" : "error:invalid"}"])

    if request.xhr?
      status = result ? :ok : :unprocessable_entity
      render_head_ref_update(status: status)
    else
      if result
        flash[:notice] = "Branch deleted successfully."
      else
        flash[:error] = "Oops, something went wrong."
      end

      redirect_to pull_request_path(@pull)
    end

  # We can get here both from above where the branch has already
  # been deleted before the button is clicked, or a race condition
  # where the application code thinks the branch exists but by
  # the time we execute the git command, someone else has already deleted it.
  rescue Git::Ref::NotFound
    GitHub.dogstats.increment("pull_request", tags: ["action:cleanup", "error:already_deleted"])
    render_head_ref_update(status: :unprocessable_entity, unretryable: true)
  end

  def undo_cleanup
    stats_key = @pull.cross_repo? ? "cross_repo" : "same_repo"

    status = :ok
    if @pull.restore_head_ref(current_user,
                              request_reflog_data("pull request branch undo button"))
      GitHub.dogstats.increment("pull_request", tags: ["action:undo_cleanup", "result:success", "repo:#{stats_key}"])
    else
      status = :unprocessable_entity
      GitHub.dogstats.increment("pull_request", tags: ["action:undo_cleanup", "error:invalid", "repo:#{stats_key}"])
    end

    render_head_ref_update(status: status, unretryable: true)
  end

  ShowPartialTimelineQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $timelineSince: DateTime,
      $timelinePageSize: Int,
      $syntaxHighlightingEnabled: Boolean = true,
      $deferCollapsedThreads: Boolean,
      $focusedReviewThread: ID,
      $focusedReviewComment: ID,
      $hasFocusedReviewThread: Boolean = false
      $hasFocusedReviewComment: Boolean = false
      $scopedItemTypes: [IssueTimelineItemsItemType!]
    ) {
      node(id: $id) {
        ...Views::Issues::FormActions::FormActionFragment,
        ...Views::PullRequests::Timeline::PullRequest
      }
    }
  GRAPHQL

  def merge
    @pull = current_repository.issues.find_by_number(params[:id].to_i).try(:pull_request)
    return render_404 unless @pull

    if params[:squash_commits] == "1"
      merge_method = "squash"
    elsif params[:do].present?
      merge_method = params[:do]
    else
      if current_repository.merge_commit_allowed?
        merge_method = "merge"
      else
        merge_method = "squash"
      end
    end

    merge_method_allowed = case merge_method
      when "merge"
        current_repository.merge_commit_allowed?
      when "squash"
        current_repository.squash_merge_allowed?
      when "rebase"
        current_repository.rebase_merge_allowed?
      else
        false
    end

    if !merge_method_allowed
      result, message = nil, "The selected merge method (#{merge_method}) is not allowed."
    elsif invalid_author_email?(params[:author_email])
      result, message = nil, "Invalid email for web commit."
    elsif @pull.git_merges_cleanly? and @pull.base_repository.pushable_by?(current_user)
      GitHub.dogstats.increment("pull_request", tags: ["action:merge"])
      begin
        result, message = @pull.merge(current_user,
                                      message_title: params[:commit_title],
                                      message: params[:commit_message],
                                      author_email: params[:author_email],
                                      reflog_data: request_reflog_data("pull request merge button"),
                                      expected_head: params[:head_sha],
                                      method: merge_method.to_sym)
      rescue Git::Ref::HookFailed => e
        @hook_out = e.message.force_encoding("UTF-8").scrub!
        result, message = nil, "Merging was blocked by pre-receive hooks."
      end

      if merge_method == "rebase"
        @pull.base_repository.set_sticky_merge_method(current_user, "rebase")
      elsif merge_method == "squash"
        @pull.base_repository.set_sticky_merge_method(current_user, "squash")
      elsif merge_method == "merge"
        @pull.base_repository.set_sticky_merge_method(current_user, "merge_commit")
      end
    else
      result, message = nil, "We couldn’t merge this pull request. Reload the page before trying again."
    end

    if request.xhr?
      if result
        GitHub.dogstats.histogram("pull_request.merged.requested_reviewers.count", @pull.review_requests.pending.size)
        respond_to do |format|
          # The `format.json` block must stay at the top here if any format
          # block is added in future, so that when the `Accept` header is `*/*`
          # we will continue to send JSON by default.
          format.json do
            pull_node = pull_node_for(ShowPartialTimelineQuery)
            render_immediate_partials(@pull,
              partials: {
                timeline: {
                  pull_request: pull_node,
                },
                sidebar: {
                  pull_node: pull_node,
                },
                merging: {},
                form_actions: {
                  pull: @pull,
                  issue_node: pull_node,
                },
              },
            )
          end
        end
      else
        GitHub.dogstats.increment("pull_request", tags: ["action:merge", "error:invalid"])
        respond_to do |format|
          format.json do
            render_immediate_partials(
              @pull,
              partials: {
                merging: {
                  merging_error: {
                    title: "Merge attempt failed",
                    message: (message || "We couldn’t merge this pull request."),
                    hook_output: @hook_out,
                  },
                },
              },
              status: :unprocessable_entity,
            )
          end
        end
      end
    else
      if result
        GitHub.dogstats.histogram("pull_request.merged.requested_reviewers.count", @pull.review_requests.pending.size)
        redirect_to pull_request_path(@pull) + "#merged-event"
      else
        flash[:error] = message
        GitHub.dogstats.increment("pull_request", tags: ["action:merge", "error:invalid"])
        redirect_to pull_request_path(@pull)
      end
    end
  end

  def change_base
    return render_404 unless params[:new_base_binary]

    @pull = find_pull_request
    return render_404 unless @pull && @pull.issue.can_modify?(current_user)

    new_base = Base64.decode64(params[:new_base_binary])
    unless new_base.present?
      return redirect_to pull_request_path(@pull), flash: { error: "Please select a base branch." }
    end

    begin
      @pull.change_base_branch(current_user, new_base)
      flash[:notice] = "Updated base branch to #{new_base}."
    rescue PullRequest::BaseNotChangeableError => e
      flash[:error] = e.ui_message
    end
    redirect_to pull_request_path(@pull)
  end

  def update_branch
    @pull = find_pull_request
    return render_404 unless @pull

    begin
      @pull.merge_base_into_head(
        user: current_user,
        author_email: current_user&.default_author_email(@pull.repository, @pull.head_sha),
        expected_head_oid: params[:expected_head_oid],
      )

      if request.xhr?
        respond_to do |format|
          # The `format.json` block must stay at the top here if any format
          # block is added in future, so that when the `Accept` header is `*/*`
          # we will continue to send JSON by default.
          format.json do
            render_immediate_partials(@pull,
              partials: {
                timeline: {
                  pull_request: pull_node_for(ShowPartialTimelineQuery),
                },
                merging: {},
              },
            )
          end
        end
      else
        redirect_to pull_request_path(@pull) + "#partial-pull-merging"
      end
    rescue GitHub::UIError => e
      if request.xhr?
        respond_to do |format|
          format.json do
            render_immediate_partials(
              @pull,
              partials: {
                merging: {
                  merging_error: {
                    title: "Update branch attempt failed",
                    message: e.ui_message,
                  },
                },
              },
              status: :unprocessable_entity,
            )
          end
        end
      else
        flash[:error] = e.ui_message
        redirect_to pull_request_path(@pull) + "#partial-pull-merging"
      end
    end
  end

  def revert
    @pull = find_pull_request
    return render_404 unless @pull && @pull.revertable_by?(current_user)

    stats_key = @pull.cross_repo? ? "cross_repo" : "same_repo"

    begin
      revert_branch, error = @pull.revert(current_user, request_reflog_data("pull request revert button"), timeout: (request_time_left / 3))
      if revert_branch
        GitHub.dogstats.increment("pull_request", tags: ["action:revert", "repo:#{stats_key}"])

        base_label, head_label =
          if revert_branch.repository == @pull.base_repository
            [@pull.base_ref_name, revert_branch.name]
          else
            ["#{@pull.base_label(username_qualified: true)}", "#{revert_branch.repository.owner.login}:#{revert_branch.name}"]
          end

        flash[:pull_request] = {
          title: "Revert \"#{@pull.title}\"",
          body: "Reverts #{@pull.base_repository.name_with_owner}##{@pull.number}",
        }
        redirect_to(compare_path(@pull.base_repository, "#{base_label}...#{head_label}", true))
      else
        if error == :merge_conflict
          GitHub.dogstats.increment("pull_request", tags: ["action:revert", "error:merge_conflict"])
        else
          GitHub.dogstats.increment("pull_request", tags: ["action:revert", "error:invalid"])
        end

        flash[:error] = "Sorry, this pull request couldn’t be reverted automatically. It may have \
                         already been reverted, or the content may have changed since it was merged."
        redirect_to pull_request_path(@pull)
      end
    rescue Git::Ref::HookFailed => e
      flash[:hook_out] = e.message
      flash[:hook_message] = "Pull request could not be reverted."
      redirect_to pull_request_path(@pull)
    end
  end

  def merge_button
    pull = current_repository.issues.find_by_number(params[:id].to_i).try(:pull_request)

    return render_404 if pull.nil?
    merge_state = pull.cached_merge_state(viewer: current_user)

    # explicitly force firing off the merge commit job if needed
    pull.enqueue_mergeable_update

    respond_to do |format|
      format.html do
        if merge_state.unknown?
          head :accepted
        else
          render partial: "pull_requests/merge_button", locals: { pull: pull }
        end
      end

      format.json do
        render json: { mergeable_state: merge_state.status }.to_json
      end
    end
  end

  def diff
    # This might be a request for a redirect to the PR for a branch name ending in .diff,
    # or might be a request for a numbered PR in .diff format.
    diff_ref = "#{params[:id]}.diff"
    if current_repository.heads.include?(diff_ref)
      return redirect_or_404(diff_ref)
    end

    redirect_or_error = ensure_valid_pull_request
    if performed?
      return redirect_or_error
    else
      redirect_to build_pull_request_diff_url
    end
  end

  def patch
    # This might be a request for a redirect to the PR for a branch name ending in .patch,
    # or might be a request for a numbered PR in .patch format.
    patch_ref = "#{params[:id]}.patch"
    if current_repository.heads.include?(patch_ref)
      return redirect_or_404(patch_ref)
    end

    redirect_or_error = ensure_valid_pull_request
    if performed?
      return redirect_or_error
    else
      redirect_to build_pull_request_patch_url
    end
  end

  def comment
    @pull = current_repository.issues.find_by_number(params[:id].to_i).try(:pull_request)
    return if reject_bully?(@pull)
    return render_404 unless @pull
    issue = @pull.issue

    valid = true
    comment_body = params[:comment][:body]

    if !comment_body.nil? && !can_skip_creating_comment?
      comment = issue.create_comment(current_user, comment_body)
      valid &&= comment.persisted?

    elsif params[:comment_and_close] == "1"
      comment = issue.comment_and_close(current_user, comment_body)
      valid &= comment if comment_body.present?

      GitHub.dogstats.increment("pull_request.closed", tags: ["with_comment:#{comment_body.present?}"])
      GitHub.dogstats.histogram("pull_request.closed.requested_reviewers.count", @pull.review_requests.pending.size)

    elsif params[:comment_and_open] == "1"
      comment = issue.comment_and_open(current_user, comment_body)
      valid &= comment if comment_body.present?
    end

    mark_thread_as_read issue
    if valid && comment_body.present?
      GitHub.instrument "comment.create", user: current_user
      instrument_saved_reply_use(params[:saved_reply_id], "pull_request_comment")
    end

    respond_to do |format|
      # The `format.json` block must stay at the top here if any format
      # block is added in future, so that when the `Accept` header is `*/*`
      # we will continue to send JSON by default.
      format.json do
        if valid
          pull_node = if params[:context] == "project_sidebar"
            pull_node_for(StateButtonChecksQuery)
          else
            pull_node_for(ShowPartialTimelineQuery)
          end
          partials = after_create_partials(@pull, pull_node)

          render_immediate_partials(@pull, partials: partials)
        else
          if GitHub.rails_6_0?
            errors = comment.errors.map { |attr, msg| msg }
          else
            errors = comment.errors.map(&:message)
          end
          render json: { errors: errors }, status: :unprocessable_entity
        end
      end
      format.html do
        if valid
          anchor = comment ? "#issuecomment-#{comment.id}" : ""
          redirect_to pull_request_path(@pull) + anchor
        else
          flash[:error] = comment.errors.full_messages.to_sentence
          redirect_to :back
        end
      end
    end
  end

  def dismiss_protip
    current_user.dismiss_notice("continuous_integration_tip")

    head :ok
  end

  def merge_button_matrix
    return render_404 unless Rails.env.development?
    if mobile?
      render "pull_requests/merge_button_matrix_mobile", layout: "mobile/application"
    else
      render "pull_requests/merge_button_matrix"
    end
  end

  def set_collab
    pull = current_repository.issues.find_by_number(params[:id].to_i).try(:pull_request)

    return render_404 if !pull || !pull.head_repository.repository.pushable_by?(current_user)

    if !!params[:collab_privs]
      pull.fork_collab_allowed!
    else
      pull.fork_collab_denied!
    end

    redirect_to pull_request_path(pull)
  end

  def resolve_conflicts
    redirect_or_error = ensure_valid_pull_request
    return redirect_or_error if performed?

    unless logged_in? && @pull.head_repository && @pull.head_repository.pushable_by?(current_user, ref: @pull.head_ref_name)
      return render_404
    end

    can_push_merge_to_head = !@pull.protected_head_branch&.required_linear_history_enabled?

    unless @pull.conflict_resolvable? && conflict_editor_enabled?(@pull) && can_push_merge_to_head
      return redirect_to pull_request_path(@pull)
    end

    render "pull_requests/resolve_conflicts", locals: { pull: @pull }
  end

  ReadyForReviewMutation = parse_query <<-'GRAPHQL'
    mutation($pullRequestId: ID!) {
      markPullRequestReadyForReview(input: { pullRequestId: $pullRequestId })
    }
  GRAPHQL

  def ready_for_review
    results = platform_execute(ReadyForReviewMutation, variables: { pullRequestId: params[:pull_request_id] })

    if results.errors.all.any?
      Failbot.report(PullRequest::Error.new("ReadyForReviewError"))

      flash[:error] = "Something went wrong!"
    else
      flash[:notice] = "Marked pull request as ready for review."
    end

    redirect_to :back
  end

  def convert_to_draft
    ensure_valid_pull_request
    return if performed?

    return render_404 unless @pull.can_convert_to_draft?(current_user)

    @pull.convert_to_draft(user: current_user)

    if request.xhr?
      head :ok
    else
      flash[:notice] = "Pull request review paused."
      redirect_back fallback_location: pull_request_path(@pull), allow_other_host: false
    end
  end

  ApplySuggestedChangesMutation = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: ApplySuggestedChangesInput!) {
      applySuggestedChanges(input: $input)
    }
  GRAPHQL

  class BadSuggestionError < StandardError; end

  def apply_suggestions
    @pull = find_pull_request
    return render_404 unless @pull

    json = GitHub::JSON.parse(params[:changes])
    changes = Array(json).map do |change|
      raise BadSuggestionError unless change.respond_to?(:fetch)
      {
        commentId: change.fetch("commentId", ""),
        path: change.fetch("path", ""),
        suggestion: change.fetch("suggestion", []),
      }
    end

    input = {
      pullRequestId: @pull.global_relay_id,
      changes: changes,
      message: params[:message].to_s,
      currentOID: params[:current_oid],
      sign: true,
    }

    data = platform_execute(ApplySuggestedChangesMutation, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:applySuggestedChanges]&.first["type"]
      status = if %w[FORBIDDEN NOT_FOUND].include?(error_type)
        error_type.downcase.to_sym
      else
        :unprocessable_entity
      end

      message = data.errors.messages[:applySuggestedChanges].first
      if status == :not_found && message.match?("Could not resolve to a node")
        message = "One or more of the suggestion comments has been deleted."
      end

      return render json: { error: message }, status: status
    else
      flash[:notice] = "#{"Suggestion".pluralize(input[:changes].count)} successfully applied."
      head :ok
    end
  rescue GitHub::JSON::ParseError, BadSuggestionError
    return render json: { error: "Sorry, the changes couldn't be applied." }, status: :unprocessable_entity
  end

protected

  helper_method :tab_specified?
  def tab_specified?(tab_name)
    specified_tab.to_s == tab_name.to_s
  end

  helper_method :pull_request_subscribe_enabled?
  def pull_request_subscribe_enabled?
    Rails.development? || Rails.test? || preview_features?
  end

  def specified_tab
    return @specified_tab if defined?(@specified_tab)
    params[:tab].presence || "discussion"
  end
  helper_method :specified_tab

  def valid_tab?
    params[:tab].blank? || %w{discussion commits files tasks checks}.include?(params[:tab])
  end

  def reject_bully_for?(repo, pull = nil)
    return false if current_repository.pushable_by?(current_user)
    if blocked_by_owner?(repo.owner_id) || (pull && blocked_by_author?(pull.user))
      flash[:error] = "You can't perform that action at this time."
      redirect_to repo.permalink
      true
    end
  end

  def reject_bully?(pull = nil)
    reject_bully_for?(current_repository, pull)
  end

  def tree_name
    if @pull && @pull.open?
      @pull.head_ref_name
    elsif @pull
      @pull.head_sha
    else
      super
    end
  end

  private

  def show_blankslate?(check_suites, selected_check_run, current_repository)
    return false if check_suites.any?
    return false if selected_check_run
    return false if current_repository.has_apps_that_write_checks?
    true
  end

  def workflows_loading?(check_suites, selected_check_run)
    # selected_check_run will be nil if no check runs have been created (yet)
    return false if selected_check_run

    check_suites.any? { |check_suite| check_suite.actions_app? && !check_suite.completed? }
  end

  def find_pull_request
    PullRequest.with_number_and_repo(params[:id].to_i, current_repository, include: [{ issue: :comments }])
  end

  # Private: For a given ref name, redirect to the appropriate pull request
  # path if one exists, or 404 otherwise.
  #
  # Returns the redirect or render_404 result.
  def redirect_or_404(ref)
    # redirect to number version if ref is a branch name,
    # redirect to new if ref is a branch with no pull request
    # 404 otherwise
    if pull = current_repository.pull_requests.for_branch(ref).last
      redirect_to pull_request_path(pull)
    elsif ref =~ /[:.]/ || current_repository.heads.include?(ref)
      redirect_to new_pull_request_path(range: ref)
    else
      render_404
    end
  rescue ActionController::UrlGenerationError
    render_404
  end

  # Private: Validate the requested pull request ID. If necessary redirect to
  # a more appropriate URL or return a 404 if the PR isn't/shouldn't be
  # available.
  def ensure_valid_pull_request
    if params[:id] =~ /\D/
      return redirect_or_404(params[:id])
    end

    @pull = find_pull_request

    return redirect_to(issue_path(id: params[:id])) unless @pull
    return redirect_to(pull_request_path @pull) unless valid_tab?

    return render_404 if @pull.hide_from_user?(current_user)

    @prose_url_hints = { tab: "files" }

    @pull.set_diff_options(
      use_summary: true,
      ignore_whitespace: ignore_whitespace?,
    )
  end

  def conflict_editor_enabled?(pull)
    return true unless pull.cross_repo?

    GitHub.cross_repo_conflict_editor_enabled?
  end

  # Validates the provided parameter is a valid sha, or nil
  def valid_sha_param?(param_name)
    param = params[param_name]
    param.nil? || GitRPC::Util.valid_full_sha1?(param)
  end

  def load_diff
    @pull_comparison.ignore_whitespace = ignore_whitespace?
    @pull_comparison.diff_options[:use_summary] = true

    # load diff data
    if !show_mobile_view?
      @pull_comparison.diff_options[:top_only] = true

      GitHub.dogstats.time("diff.load.initial", tags: dogstats_request_tags) do
        @pull_comparison.diffs.apply_auto_load_single_entry_limits!
        @pull_comparison.diffs.load_diff(timeout: request_time_left / 2)
      end
    else
      @pull_comparison.diffs.load_diff(timeout: request_time_left / 2)
    end
  end

  # Preload data needed for rendering the PR.
  #
  # timeline - Boolean specifying whether the PR's timeline will be rendered.
  # pull_comparison - PullRequest::Comparison specifying whether the PR's diff will be rendered.
  #
  # Returns nothing.
  def prepare_for_rendering(timeline:, pull_comparison:)
    prepare_for_rendering_diffs(timeline: timeline, pull_comparison: pull_comparison)
    prepare_for_rendering_timeline_items(timeline: timeline, pull_comparison: pull_comparison)
  end

  # Preload data needed for rendering timeline items.
  #
  # timeline - Boolean specifying whether the PR's timeline will be rendered.
  # pull_comparison - PullRequest::Comparison specifying whether the PR's diff
  #   (which contains timeline items corresponding to live review threads) will be rendered.
  #
  # Returns nothing.
  def prepare_for_rendering_timeline_items(timeline:, pull_comparison:)
    timeline_items = visible_timeline_items

    items_to_prefill = timeline_items.dup
    items_to_preload = timeline_items.dup

    if pull_comparison && !timeline
      threads = pull_comparison.review_threads(viewer: current_user).to_a
      items_to_prefill.concat(threads)
      items_to_preload.concat(threads.flat_map(&:comments))
    end

    @pull.prefill_timeline_associations(items_to_prefill,
      preload_diff_entries: timeline, show_mobile_view: show_mobile_view?)

    preload_discussion_group_data(items_to_preload, mobile: show_mobile_view?)
  end

  # Preload data needed for rendering diffs.
  #
  # timeline - Boolean specifying whether the PR's timeline (which contains
  #            diffs for review threads) will be rendered.
  # pull_comparison - PullRequest::Comparison specifying whether the PR's diff will be rendered.
  #
  # Returns nothing.
  def prepare_for_rendering_diffs(timeline:, pull_comparison:)
    return unless syntax_highlighted_diffs_enabled?

    diffs_to_highlight = []
    if timeline
      visible_timeline_items.each do |item|
        next unless item.is_a?(DeprecatedPullRequestReviewThread)
        next unless item.diff_entry
        diffs_to_highlight << item.diff_entry
      end
    end
    if pull_comparison
      diffs_to_highlight.concat(pull_comparison.diffs.to_a)
    end

    if show_mobile_view?
      preload_discussion_group_data(visible_timeline_items.select { |item| item.is_a?(DeprecatedPullRequestReviewThread) }, mobile: true)
    end

    SyntaxHighlightedDiff.new(@pull.comparison.compare_repository).highlight!(diffs_to_highlight, attributes_commit_oid: @pull.head_sha)
  end

  # Prepare for rendering the files tab
  #
  # pull_comparison - PullRequest::Comparison specifying whether the PR's diff will be rendered.
  #
  # Returns nothing.
  def prepare_for_rendering_files(pull_comparison:)
    return unless pull_comparison

    if syntax_highlighted_diffs_enabled?
      SyntaxHighlightedDiff.new(pull_comparison.repository).highlight!(
        pull_comparison.diffs.to_a,
        attributes_commit_oid: @pull.head_sha,
      )
    end

    review_threads = pull_comparison.review_threads(viewer: current_user)

    pull_comparison.pull.prefill_timeline_associations(
      review_threads,
      preload_diff_entries: false,
      show_mobile_view: show_mobile_view?,
    )
    preload_discussion_group_data(review_threads.threads + review_threads.to_a.flat_map(&:comments), mobile: show_mobile_view?)
  end

  def can_skip_creating_comment?
    params[:comment_and_close].present? ||
      params[:comment_and_open].present?
  end

  # If it's empty, you can't issue a pull request.  There will be no
  # base SHA to merge against.
  def check_for_empty_repository
    if current_repository.empty?
      redirect_to current_repository
    end
  end

  def ensure_pull_head_pushable
    @pull = current_repository.issues.find_by_number(params[:id].to_i).try(:pull_request)
    return render_404 if @pull.nil?
    return writable_repository_required unless @pull.head_repository.writable?
    render_404 unless @pull.head_repository.pushable_by?(current_user)
  end

  # Reload the pull so the deletable/restorable status is current,
  # then render updates for the event list and the merge/delete buttons.
  def render_head_ref_update(status:, unretryable: false)
    @pull.reload
    merging_error = nil
    if status == :unprocessable_entity
      merging_error = { unretryable: unretryable }
    end
    respond_to do |format|
      # The `format.json` block must stay at the top here if any format
      # block is added in future, so that when the `Accept` header is `*/*`
      # we will continue to send JSON by default.
      format.json do
        pull_node = pull_node_for(ShowPartialTimelineQuery)
        render_immediate_partials(@pull,
          partials: {
            timeline: {
              pull_request: pull_node,
            },
            merging: {
              merging_error: merging_error,
            },
            form_actions: {
              pull: @pull,
              issue_node: pull_node,
            },
          },
          status: status,
        )
      end
    end
  end

  # Internal: Provide a better failure message than simply taking the validation errors
  #
  # e - the ActiveRecord::RecordInvalid exception from the creation failure
  #
  # Returns a String
  def human_failure_message(e)
    pr = e.record

    # e.record can be an Issue, raised in PullRequest.create_for.
    return e.message unless pr.is_a?(PullRequest)

    if bad_branches = pr.missing_refs
      if bad_branches.length == 1
        "The #{bad_branches.first} branch doesn’t exist."
      else
        "The #{bad_branches.join(' and ')} branches don’t exist."
      end
    elsif [:base_ref, :head_ref].any? { |attr| pr.errors[attr].include?(GitHub::Validations::Unicode3Validator::ERROR_MESSAGE) }
      "Branch names cannot contain unicode characters above 0xffff."
    else
      e.message
    end
  end

  # If there are this many timeline items or fewer, we'll render the timeline
  # inline when the user is viewing the Files tab. If there are more than this
  # many items, we'll leave the timeline out and load it as needed to try to
  # avoid timing out.
  MAXIMUM_TIMELINE_SIZE_FOR_INLINE_RENDER = 149

  def render_discussion_page?
    return @render_discussion_page if defined?(@render_discussion_page)
    @render_discussion_page =
      tab_specified?("discussion") || (!show_mobile_view? && @pull.timeline_children_for(current_user).count <= MAXIMUM_TIMELINE_SIZE_FOR_INLINE_RENDER)
  end
  helper_method :render_discussion_page?

  def render_files_page?
    return @render_files_page if defined?(@render_files_page)
    @render_files_page = tab_specified?("files") || (!show_mobile_view? && !@pull.corrupt? && !@pull.large_diff?)
  end
  helper_method :render_files_page?

  def pull_request_authorization_token
    current_user.signed_auth_token expires: 60.seconds.from_now,
                                   scope: pull_request_authorization_token_scope_key
  end

  def build_pull_request_diff_url
    route_options ||= {}

    if GitHub.prs_content_domain?
      route_options[:host] = GitHub.prs_content_host_name
    end

    if current_repository.private?
      route_options[:token] = pull_request_authorization_token
    end

    route_options[:full_index] = params[:full_index]

    pull_request_raw_diff_url(route_options)
  end

  def build_pull_request_patch_url
    route_options ||= {}

    if GitHub.prs_content_domain?
      route_options[:host] = GitHub.prs_content_host_name
    end

    if current_repository.private?
      route_options[:token] = pull_request_authorization_token
    end

    route_options[:full_index] = params[:full_index]

    pull_request_raw_patch_url(route_options)
  end

  def request_reflog_data(via)
    super(via).merge({ pr_author_login: @pull.safe_user.login })
  end

  def content_authorization_required
    authorize_content(:pull_request, repo: current_repository)
  end

  def invalid_author_email?(author_email)
    author_email && (!GitHub.choose_commit_email_enabled? || !current_user&.author_emails.include?(author_email))
  end

  # Internal: Extract OID components from PR range.
  #
  #   /github/github/pull/123/files/abc123..def456
  #   /github/github/pull/123/files/def456
  #
  # pull  - Current PullRequest
  # range - String range parameter
  #
  # If a complete range is given, a pair of resolved String OIDs will be
  # returned. If only one end sha is given, nil and a resolved String OID
  # will be returned. Otherwise nil is returned if no range was matched.
  def parse_show_range_oid_components(pull, range)
    if m = range.to_s.match(/\A(?<sha1>[a-fA-F0-9]{7,40})\.\.(?<sha2>[a-fA-F0-9]{7,40}|HEAD)\z/)
      sha2 = m[:sha2] == "HEAD" ? pull.head_sha : m[:sha2]
      result = pull.repository.rpc.expand_shas([m[:sha1], sha2], "commit")
      sha1, sha2 = result[m[:sha1]], result[sha2]
      [sha1, sha2] if sha1 && sha2
    elsif m = range.to_s.match(/^(?<sha2>[a-fA-F0-9]{7,40})$/)
      result = pull.repository.rpc.expand_shas([m[:sha2]], "commit")
      sha2 = result[m[:sha2]]
      return [nil, sha2] if sha2
    end
  end

  def visible_timeline_items
    return [] unless render_discussion_page?
    super
  end

  def showing_full_timeline?
    return false unless render_discussion_page?
    super
  end

  def timeline_owner
    @pull
  end

  def pull_node_for(query)
    timeline_owner_response_for(query: query, id: @pull.global_relay_id, defer_collapsed_threads: defer_collapsed_threads?)&.node
  end

  MobileCommitsQuery = parse_query <<-'GRAPHQL'
    query($ids: [ID!]!) {
      nodes(ids: $ids) {
        ...Views::Mobile::PullRequests::CommitList::Commit
      }
    }
  GRAPHQL

  # XXX: Adhoc GraphQL loader for PullRequest.changedChanges connection.
  #
  # Load required GraphQL data for each Commit in PullRequest#changed_commits and
  # wrap it in a fake connection to be compatible with mobile/commits/list template.
  def load_mobile_pull_request_commits
    commits = @pull.changed_commits

    # we need to "fudge" the repository for these commits as the head repository could disappear
    # but we should always have these commits in the base. This shouldn't be necessary when this
    # query is updated to use proper PullRequestCommit objects instead.
    ids = commits.map do |c|
      c.repository = @pull.base_repository
      c.global_relay_id
    end
    data = platform_execute(MobileCommitsQuery, variables: { "ids" => ids })
    data.nodes
  end
  helper_method :load_mobile_pull_request_commits

  def route_supports_advisory_workspaces?
    return false if action_name == "merge"
    return true unless action_name == "show"

    %w(discussion status commits files).include?(specified_tab)
  end

  def after_create_partials(pull, pull_node)
    case params[:context]
    when "project_sidebar"
      {
        state_button_wrapper: {
          pull: @pull,
          issue_node: pull_node,
          addl_btn_classes: ["width-full mt-2"],
        },
      }
    else
      {
        timeline: {
          pull_request: pull_node,
        },
        sidebar: {
          pull_node: pull_node,
        },
        merging: {},
        form_actions: {
          pull: @pull,
          issue_node: pull_node,
        },
        title: {
          sticky: params[:sticky] == "true",
        },
      }
    end
  end

  def require_merge_queue_enabled
    return render_404 unless merge_queue_enabled?
  end
end
