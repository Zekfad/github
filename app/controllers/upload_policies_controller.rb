# frozen_string_literal: true

# The file upload flow is three steps:
#
#   1. POST to /upload/policies to create the policy document validating the file
#      upload.
#   2. POST the file upload form to the endpoint specified by the policy.
#   3. PUT to /upload/:asset_id to signal that the file upload completed
#      successfully.
#
# All three steps are carried out by the JavaScript uploader code in
# uploads.coffee when triggered by a drag-and-drop of files onto a comment
# form field.
#
# This controller implements step one. See UploadController for steps two and
# three.
class UploadPoliciesController < ApplicationController
  areas_of_responsibility :data_infrastructure

  before_action :login_required

  # We POST to this action over ajax to generate a policy document before POSTing
  # the file upload to S3.
  #
  # Amazon requires a policy document signed with our secret key to be submitted
  # along with the file upload to ensure only users of our app are allowed to
  # store files in our buckets.
  #
  # More info on the form fields required by S3 here:
  #
  #   http://aws.amazon.com/articles/1434
  #
  # Returns nothing.
  def create
    return render_404 unless uploader = ::Storage.policy_creator.for(model_name)
    if GitHub.storage_cluster_enabled?
      create_with_storage_policy(uploader.model)
    else
      deliver_policy uploader.create(current_user, policy_params(params))
    end
  end

  private

  def create_with_storage_policy(uploadable_class)
    meta = {}
    uploadable_class.uploadable_policy_attributes.each do |key|
      meta[key] = params[key]
    end
    if params[:business_id]
      business = Business.where(slug: params[:business_id]).first!
      meta[:business_id] = business.id
    end
    blob = ::Storage::Blob.new(size: meta[:size])
    deliver_policy uploadable_class.storage_new(current_user, blob, meta)
  end

  def policy_params(params)
    if params[:business_id]
      business = Business.where(slug: params[:business_id]).first!
      return params.merge(business_id: business.id)
    end
    params
  end

  def deliver_policy(uploadable)
    if uploadable.valid?
      policy = uploadable.storage_policy(actor: current_user)
      increment_logo_stat(uploadable)
      render status: 201, json: add_authenticity_tokens(policy.policy_hash)
      return
    end

    if !uploadable.upload_access_allowed?(current_user)
      return render_404
    end

    extra = {
      model_name: uploadable.class.name,
    }

    if GitHub.rails_6_0?
      uploadable.errors.each do |key, msg|
        extra["#{key}_error"] = msg
      end
    else
      uploadable.errors.each do |error|
        extra["#{error.attribute}_error"] = error.message
      end
    end

    err = ActiveRecord::ActiveRecordError.new("Failed to create #{uploadable.class.name}")
    GitHub::Logger.log(method: __method__,
                       err: err,
                       extra: extra)
    Failbot.report_user_error(err)

    render status: 422,
      json: {errors: Api::Serializer.validation_errors(uploadable.errors)}
  end

  def add_authenticity_tokens(policy_hash)
    if upload_url = policy_hash[:upload_url]
      token = authenticity_token_for(upload_url)
      policy_hash[:upload_authenticity_token] = token
    end

    if asset_upload_url = policy_hash[:asset_upload_url]
      token = authenticity_token_for(asset_upload_url, method: :put)
      policy_hash[:asset_upload_authenticity_token] = token
    end

    policy_hash
  end

  def image?
    ::Storage::Uploadable::CONTENT_TYPES[:images].include?(params[:content_type])
  end

  def increment_logo_stat(uploadable)
    return unless uploadable.is_a?(OauthApplicationLogo)
    GitHub.dogstats.increment("oauth_application", tags: ["action:logo_created"])
  end

  def model_name
    @model_name ||= begin
      model_name = params[:model]

      if model_name == "assets" && params[:repository_id].present? && !image?
        model_name = "repository-files"
      end

      model_name
    end
  end

  def target_for_conditional_access
    case model_name
    when "assets", "repository-files", "upload-manifest-files", "repository-images"
      repo = Repository.find_by_id(params[:repository_id])
      # Can upload without a repository ID
      return :no_target_for_conditional_access unless repo
      repo.owner
    when "releases"
      release = Release.find_by_id(params[:release_id])
      # Redirects to login page
      return :no_target_for_conditional_access unless release
      release.repository.owner
    when "oauth_applications"
      app = OauthApplication.find_by_id(params[:application_id])
      # Can upload without an application ID
      return :no_target_for_conditional_access unless app
      app.owner
    when "avatars"
      # TEMPORARY SECURITY BYPASS: see https://github.com/github/github/pull/149626#issuecomment-659422129
      return :no_target_for_conditional_access unless params[:owner_type] == "Organization"

      User.find_by_id(params[:owner_id])
    when "marketplace_listing_screenshots"
      listing = Marketplace::Listing.find_by_id(params[:marketplace_listing_id])
      listing.owner
    when "enterprise_installation_user_accounts_uploads"
      Business.find_by slug: params[:business_id]
    end
  end
end
