# frozen_string_literal: true

class InvoicesController < ApplicationController
  include BillingSettingsHelper

  before_action :ensure_billing_enabled
  before_action :login_required
  before_action :ensure_target
  before_action :ensure_target_billing_manageable
  before_action :require_invoice, only: [:show, :pay, :payment_method]

  def show
    instrument_show

    headers["Content-Disposition"] = "inline; filename=\"#{invoice.number&.downcase}.pdf\""
    render body: Base64.decode64(invoice.body), content_type: "application/pdf"
  end

  def pay
    raise GitHub::Zuora::Error unless params[:success] == "true"
    payload = { AccountId: target.customer.zuora_account_id,
                Amount: invoice.balance,
                AppliedCreditBalanceAmount: 0,
                AppliedInvoiceAmount: invoice.balance,
                EffectiveDate: Date.today.to_s,
                InvoiceId: invoice.id,
                PaymentMethodId: params[:ref_id],
                Status: "Processed",
                Type: "Electronic" }
    GitHub.zuorest_client.create_payment(payload)

    instrument_pay(payment_method_id: params[:ref_id])

    redirect_to target_billing_path(target)
  rescue Zuorest::HttpError, GitHub::Zuora::Error => e
    payment_processor_error(error: e)
  ensure
    ZuoraDeleteCardsFromAccountJob.perform_later(target.customer.zuora_account_id)
  end

  def payment_method
    unless invoice.paid?
      SecureHeaders.append_content_security_policy_directives(
        request,
        frame_src: [GitHub.zuora_payment_page_server],
      )
      render "billing_settings/invoice_payment_method", locals: { target: target, invoice: invoice }
    else
      payment_processor_error(message: "This invoice is already fully paid.")
    end
  end

  def payment_page_signature
    page_params = ::Billing::Zuora::HostedPaymentsPage.params(
      page_id:  GitHub.zuora_payment_page_id,
      target: current_user,
      account_id: target.customer.zuora_account_id,
    )

    render json: page_params
  rescue Zuorest::HttpError => e
    payment_processor_error(error: e)
  end

  private

  def target_for_conditional_access
    target
  end

  def ensure_target
    render_404 if target.nil?
  end

  def ensure_target_billing_manageable
    render_404 unless target.adminable_by?(current_user) || target.billing_manager?(current_user)
  end

  def target
    @_target ||= params[:slug].present? ? business : organization
  end

  def business
    @_business ||= Business.find_by(slug: params[:slug])
  end

  def organization
    @_organization ||= Organization.find_by(login: params[:organization_id])
  end

  def invoice
    @_invoice ||= Billing::ZuoraInvoice.invoice_for_number(params[:invoice_number])
  end

  def require_invoice
    render_404 unless invoice&.account_id == target&.customer&.zuora_account_id
  rescue Zuorest::HttpError => e
    Failbot.report(e)
    render_404
  end

  def payment_processor_error(error: nil,
                              message: "There was a problem communicating with our payment provider. Please try again.",
                              redirect_url: target_billing_path(target))
    Failbot.report(error) if error
    flash[:error] = message
    redirect_to redirect_url
  end

  def instrument_show
    GitHub.instrument("invoice.download", invoice_payload)
  end

  def instrument_pay(payment_method_id:)
    payload = invoice_payload.merge(
      amount_applied_to_invoice: Billing::Money.new(invoice.balance * 100).format,
      payment_method_id: payment_method_id,
    )

    GitHub.instrument("invoice.pay", payload)
  end

  def invoice_payload
    payload = { invoice_number: invoice.number.upcase }

    if target.is_a?(Business)
      payload.update(business: business)
    else
      payload.update(org: organization)
    end

    if current_user.staff?
      payload.update(GitHub.guarded_audit_log_staff_actor_entry(current_user))
    else
      payload.update(actor: current_user)
    end

    payload
  end
end
