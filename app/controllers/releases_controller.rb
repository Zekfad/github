# frozen_string_literal: true
class ReleasesController < AbstractRepositoryController
  map_to_service :release
  map_to_service :ref, only: [:create_tag]

  before_action :writable_repository_required,
    except: [:index, :latest, :download_latest, :tag_history, :show, :download, :check_tag, :preview]
  before_action :pushers_only, except: [:index, :tag_history, :show, :download, :latest, :download_latest]
  layout :repository_layout

  before_action :sudo_filter, only: [:create, :update], if: :publishing_to_marketplace?

  before_action :add_csp_exceptions, only: [:index, :new, :edit]
  CSP_EXCEPTIONS = {
    connect_src: [ReleaseAsset.storage_s3_new_bucket_host],
  }

  def index
    @releases = Release.page(current_repository, after: params[:after])

    respond_to do |wants|
      wants.html do
        @drafts = current_repository.releases.drafted.reverse if current_user_can_push?
        if @releases.blank? && @drafts.blank?
          render "releases/empty"
        else
          render "releases/index"
        end
      end
      wants.atom do
        render "releases/index", layout: false
      end
    end
  end

  def latest
    if rel = Release.latest_for(current_repository)
      prefill_releases([rel], rel.uploaded_assets)
      redirect_to action: "show", name: rel.tag_name
    else
      redirect_to action: "index"
    end
  end

  def download_latest
    if rel = Release.latest_for(current_repository)
      prefill_releases([rel], rel.uploaded_assets)
      redirect_to action: "download", name: rel.tag_name, path: params[:path]
    else
      render_404
    end
  end

  param_encoding :tag_history, :q, "ASCII-8BIT"

  def tag_history
    @releases = Release.page(current_repository, after: params[:after])

    respond_to do |wants|
      wants.html do
        if @releases.empty?
          render "releases/empty", locals: { tags: true }
        else
          render "releases/tag_history"
        end
      end
      wants.html_fragment do
        # The <auto-complete> element only shows the first 9 results.
        query = params[:q].to_s
        tags = current_repository.tags.substring_filter(substring: query, limit: 9)
        render "releases/tag_history_autocomplete", formats: :html, layout: false, locals: {tags: tags}
      end
      wants.atom do
        render "releases/tag_history", layout: false
      end
    end
  end

  param_encoding :show, :name, "ASCII-8BIT"

  ReleaseQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Releases::Release::Release
      }
    }
  GRAPHQL

  def show
    include_drafts = current_user_can_push?

    @recent_releases = Release.where(repository_id: current_repository.id).latest.limit(5)
    @release = find_release_by_slug(params[:name], include_drafts)

    return if performed?
    release_data = platform_execute(ReleaseQuery, variables: { id: @release.global_relay_id })
    prefill_releases [@release], @release.uploaded_assets
    mark_thread_as_read @release unless @release.new_record?
    respond_to do |wants|
      wants.html do
        if @release
          render "releases/show", locals: { release_graphql: release_data.node }
        else
          render_404
        end
      end
      wants.json do
        if @release.nil?
          render status: 404, json: {message: "Not found"}
        else
          render status: 200, json: release_json
        end
      end
    end
  end

  def new
    @release = current_repository.releases.build state: :draft
    @release.pending_tag = params[:tag] if params[:tag]
    @release.body = params[:body] if params[:body]
    @release.name = params[:title] if params[:title]
    @release.target_commitish = params[:target] if params[:target]
    @release.prerelease = (params[:prerelease] == "1" || params[:prerelease] == "true")

    if @release.potential_github_action?
      @release.build_repository_action_release
      @release.repository_action = @release.repository.action_at_root
    end

    render "releases/new"
  end

  def create
    @release = current_repository.releases.build author: current_user, reflog_data: request_reflog_data("releases create button")
    saved = save_release_from_params

    respond_to do |wants|
      wants.html do
        if saved
          redirect_to release_path(@release)
        else
          if @release.errors[:pre_receive].any?
            flash.now[:hook_message] = "Tag could not be created."
            flash.now[:hook_out] = @release.errors[:pre_receive].join(", ")
          else
            flash.now[:error] = "We weren’t able to create the release for you. Please correct the errors below."
          end
          render "releases/new"
        end
      end
      wants.json do
        if saved
          render status: 201, json: release_json
        else
          render status: 422, json: {errors: @release.errors.full_messages}
        end
      end
    end
  end

  def download
    include_drafts = current_user_can_push?
    @release = find_release_by_slug(params[:name], include_drafts)
    return if performed?

    @asset = if !@release # nada
    # legacy route with :asset_id
    elsif (asset_id = params[:asset_id].to_i) > 0
      @release.release_assets.find_by_id(params[:asset_id].to_i)
    # current route with :name/*path
    elsif params[:path].present?
      path = params[:path].join("/")
      @release.release_assets.find_by_name(path) if GitHub::UTF8.valid_unicode3?(path)
    end

    if @asset.nil?
      render_404 and return
    end
    @asset.download
    redirect_to @asset.storage_policy(actor: current_user).download_url
  end

  param_encoding :edit, :name, "ASCII-8BIT"

  def edit
    @release = find_release_by_slug
    prefill_releases [@release], @release.uploaded_assets if @release
    @real_release = @release

    if @release && @release.potential_github_action?
      @release.build_repository_action_release if @release.repository_action_release.nil?
    end

    render "releases/edit" unless performed?
  end

  param_encoding :update, :name, "ASCII-8BIT"

  def update
    @release = find_release_by_slug
    return if performed?

    saved = save_release_from_params

    respond_to do |wants|
      wants.html do
        if saved
          redirect_to release_path(@release)
        else
          @real_release = Release.find(@release.id)
          if !@release.tagged?
            flash.now[:error] = "We weren't able to create the release for you. Make sure you have a valid tag."
          end
          render "releases/edit"
        end
      end
      wants.json do
        if saved
          render status: 200, json: release_json
        else
          render status: 422, json: {errors: @release.errors.full_messages}
        end
      end
    end
  end

  param_encoding :destroy, :name, "ASCII-8BIT"

  def destroy
    @release = find_release_by_slug!
    if !@release
      flash[:message] = "No release was found"
    elsif @release.only_tag?
      expected_oid = params[:expected_commit_oid] || @release.tag.sha
      reflog = request_reflog_data("releases delete button")
      begin
        if @release.delete_tag(expected_oid, current_user, reflog)
          flash[:message] = "Your tag was removed"
        else
          actual = current_repository.tags.find(@release.tag_name)
          flash[:error] = "The #{@release.tag_name} tag was not removed because it was not at #{expected_oid}: #{actual.inspect}"
        end
      rescue Git::Ref::HookFailed => e
        flash[:hook_out] = e.message
        flash[:hook_message] = "Tag could not be deleted."
      end
    elsif @release.destroy
      flash[:message] = "Your release was removed"
    end
    redirect_to action: "index"
  end

  def create_tag
    return render_404 if !request.xhr?

    name = params[:tag_name].gsub(/[^0-9A-Za-z_\.]/, "-")
    branch_name = params[:name] || current_repository.default_branch
    ref = current_repository.refs.find(branch_name)
    target = ref ? ref.target : current_repository.ref_to_sha(branch_name)

    current_repository.tags.create(name, target, current_user, reflog_data: request_reflog_data("create tag through releases"))
    GitHub.instrument "tag.create", user: current_user
    head :ok
  rescue Git::Ref::ExistsError
    render status: 403, json: { error: "The tag `#{name}' already exists." }
  end

  param_encoding :check_tag, :tag_name, "ASCII-8BIT"

  # Determines the status of a tag. Possible results:
  #
  # 1. Tag has a persisted Release record (duplicate)
  # 2. Tag exists, without Release record (valid)
  # 3. Tag does not exist, and is well formed (pending)
  # 4. Tag does not exist, and is not well formed (invalid)
  def check_tag
    release = find_release_by_slug!(params[:tag_name])

    # Tag exists
    if release
      if release.new_record?
        json = {status: "valid"}
      else
        json = {
          status: "duplicate",
          release_id: release.id,
          url: edit_release_path(release),
        }
      end

    # Tag does not exist
    else
      tag = current_repository.tags.build(params[:tag_name])
      if tag.well_formed?
        json = {status: "pending"}
      else
        json = {status: "invalid"}
      end
    end

    respond_to do |format|
      format.json { render json: json }
    end
  end

  def preview
    release = current_repository.releases.build(
      author: current_user,
      body: params[:text],
    )
    release.with_params(params[:release]) if params[:release]
    render html: release.body_html.html_safe # rubocop:disable Rails/OutputSafety
  end

  private

  # Handle auth specifics for feed requests.
  include GitHub::Authentication::Feed

  # Private: Actions that can response to atom requests.
  #
  # Returns an Array or Strings.
  def feed_actions
    %w(index tag_history)
  end

  def publishing_to_marketplace?
    return false unless action_params = params.dig(:release, :repository_action_release_attributes)

    ActiveModel::Type::Boolean.new.cast(action_params[:published_on_marketplace]) &&
      !ActiveModel::Type::Boolean.new.cast(release_params[:draft])
  end

  def find_release_by_slug(slug = nil, include_drafts = true)
    if rel = find_release_by_slug!(slug, include_drafts)
      rel
    else
      render_404 && nil
    end
  end

  def find_release_by_slug!(slug = nil, include_drafts = true)
    slug ||= params[:name]
    Release.from_tag_or_slug(current_repository, slug, include_drafts)
  end

  def save_release_from_params
    rel_options = release_params
    rel_options = filter_action_params(rel_options)
    rel_options[:tag_name] ||= params[:tag_name]
    was_draft = @release.draft?
    was_published_on_marketplace = current_repository.listed_action.present?
    @release.with_params(rel_options)
    @release.repository_action_release.actor = current_user if @release.repository_action_release
    is_new_record = @release.new_record?
    saved = nil

    saved = @release.save
    if saved && @release.repository_action_release&.published_on_marketplace?
      save_action_categories
      instrument_action_list_event unless was_published_on_marketplace
    end

    if was_draft && !saved
      @release.draft = true
    end

    @release.actor = current_user

    saved
  end

  def filter_action_params(repo_action_params)
    unless current_repository.listable_action? && current_repository.preferred_readme.present?
      repo_action_params.delete(:repository_action_release_attributes)
    end

    repo_action_params
  end

  def release_params
    return ActionController::Parameters.new unless params.key?(:release)

    params.require(:release).permit :id,
                                    :name,
                                    :body,
                                    :draft,
                                    :tag_name,
                                    :target_commitish,
                                    :prerelease,
                                    {
                                      repository_action_release_attributes: %i[id published_on_marketplace],
                                    },
                                    {
                                      repository_action_attributes: %i[id security_email],
                                    },
                                    {
                                      release_assets_attributes: %i[id name size _destroy],
                                    }
  end

  def ref_sha_path_extractor
    @ref_sha_path_extractor ||= TagExtractor.new(current_repository)
  end

  def prefill_releases(releases, assets)
    GitHub::PrefillAssociations.for_releases(current_repository, releases, assets)
  end

  class TagExtractor < GitHub::RefShaPathExtractor
    def call(path)
      pieces = path.split("/")
      if Release.untagged_name?(pieces[0])
        [pieces[0], pieces[1..-1].join("/")]
      else
        super(path)
      end
    end
  end

  def release_json
    update_url  = release_path(@release)
    update_authenticity_token = authenticity_token_for(update_url, method: :put)

    delete_url = release_path(@release)
    delete_authenticity_token = authenticity_token_for(delete_url, method: :delete)

    {
      id: @release.id,
      tag_name: @release.tag_name,
      update_url: update_url,
      update_authenticity_token: update_authenticity_token,
      delete_url: delete_url,
      delete_authenticity_token: delete_authenticity_token,
      edit_url: edit_release_path(@release),
    }
  end

  # The current release's name/path should not affect what is shown in the
  # sidebar.
  #
  # https://github.com/github/github/issues/25011
  def sidebar_tree_name
    current_repository.default_branch
  end

  def save_action_categories
    categories = []

    if (first_category = params.dig(:repository_action, :primary_category_id)).present?
      categories << first_category
    end

    if (second_category = params.dig(:repository_action, :secondary_category_id)).present?
      categories << second_category if second_category != categories.first
    end

    if categories.present?
      filter_categories = @release.repository_action.filter_category_ids
      @release.repository_action.category_ids = categories + filter_categories
    end
  end

  def instrument_action_list_event
    GlobalInstrumenter.instrument("marketplace.action.list",
      actor: current_user,
      repository_owner: current_repository.owner,
      repository_action_id: @release.repository_action_release&.repository_action&.global_relay_id,
      repository: current_repository,
      release_tag_name: @release.tag_name,
    )
  end

  def route_supports_advisory_workspaces?
    true
  end
end
