# frozen_string_literal: true

class BranchProtectionRulesController < AbstractRepositoryController
  before_action :login_required
  before_action :writable_repository_required

  before_action :sudo_filter, only: %i(
    create
    update
    destroy
  )

  before_action :ensure_admin_access
  before_action :ensure_plan_supports_protected_branches
  before_action :ensure_user_can_update_branch_protection, only: [:new, :create, :update, :destroy]

  layout :repository_layout
  javascript_bundle :settings

  NewQuery = parse_query <<~'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::BranchProtectionRules::New::Repository
      }
    }
  GRAPHQL

  def new
    # TODO: Remove reliance on this temporary branch protection rule.
    #       It's currently needed to fetch some data in the view that has not been
    #       moved to GraphQL yet and requires a `ProtectedBranch` instance.
    protected_branch = current_repository.protected_branches.build(name: "*")
    @branch_protection_rule = Platform::Models::BranchProtectionRule.new(protected_branch)

    render "branch_protection_rules/new", locals: {
      repository: platform_execute(NewQuery, variables: {
        id: current_repository.global_relay_id,
      }).node,
    }
  end

  CreateMutation = parse_query <<~'GRAPHQL'
    mutation($input: CreateBranchProtectionRuleInput!) {
      createBranchProtectionRule(input: $input) {
        branchProtectionRule {
          databaseId
        }
      }
    }
  GRAPHQL

  def create
    result = platform_execute(CreateMutation, variables: {
      input: {
        repositoryId: current_repository.global_relay_id,
        pattern: params[:rule],
        requiresApprovingReviews: params[:has_required_reviews].present?,
        requiredApprovingReviewCount: params[:required_approving_review_count].present? ? params[:required_approving_review_count].to_i : nil,
        restrictsReviewDismissals: params[:required_reviews_enforce_dismissal].present?,
        requiresCommitSignatures: params[:has_signature_requirement].present?,
        requiresLinearHistory: params[:block_merge_commits].present?,
        allowsForcePushes: params[:allows_force_pushes].present?,
        allowsDeletions: params[:allows_deletions].present?,
        isAdminEnforced: params[:enforce_all_for_admins].present?,
        requiresStatusChecks: params[:has_required_statuses].present?,
        requiresStrictStatusChecks: params[:strict_required_status_checks_policy].present?,
        requiresCodeOwnerReviews: params[:require_code_owner_review].present?,
        dismissesStaleReviews: params[:dismiss_stale_reviews_on_push].present?,
        restrictsPushes: params[:authorized_actors].present?,
        reviewDismissalActorIds: review_dismissal_actor_ids,
        pushActorIds: push_actor_ids,
        requiredStatusCheckContexts: params[:contexts] || [],
      },
    })

    if result.errors.any?
      flash[:error] = result.errors.messages.values.flatten.to_sentence
      redirect_to new_branch_protection_rule_path
    else
      flash[:notice] = "Branch protection rule created."
      redirect_to branch_protection_rule_path(id: result.create_branch_protection_rule.branch_protection_rule.database_id)
    end
  end

  ShowQuery = parse_query <<~'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::BranchProtectionRules::Show::BranchProtectionRule
      }
    }
  GRAPHQL

  def show
    # TODO: Remove reliance on this temporary branch protection rule.
    #       It's currently needed to fetch some data in the view that has not been
    #       moved to GraphQL yet and requires a `ProtectedBranch` instance.
    protected_branch = current_repository.protected_branches.find(params[:id])
    @branch_protection_rule = Platform::Models::BranchProtectionRule.new(protected_branch)

    render "branch_protection_rules/show", locals: {
      branch_protection_rule: platform_execute(ShowQuery, variables: {
        id: @branch_protection_rule.global_relay_id,
      }).node,
    }
  end

  UpdateMutation = parse_query <<~'GRAPHQL'
    mutation($input: UpdateBranchProtectionRuleInput!) {
      updateBranchProtectionRule(input: $input)
    }
  GRAPHQL

  def update
    # TODO: Remove reliance on this temporary branch protection rule.
    #       It's currently needed to fetch some data in the view that has not been
    #       moved to GraphQL yet and requires a `ProtectedBranch` instance.
    protected_branch = current_repository.protected_branches.find(params[:id])
    @branch_protection_rule = Platform::Models::BranchProtectionRule.new(protected_branch)

    result = platform_execute(UpdateMutation, variables: {
      input: {
        branchProtectionRuleId: @branch_protection_rule.global_relay_id,
        pattern: params[:rule],
        requiresApprovingReviews: params[:has_required_reviews].present?,
        requiredApprovingReviewCount: params[:required_approving_review_count].present? ? params[:required_approving_review_count].to_i : nil,
        restrictsReviewDismissals: params[:required_reviews_enforce_dismissal].present?,
        requiresCommitSignatures: params[:has_signature_requirement].present?,
        requiresLinearHistory: params[:block_merge_commits].present?,
        allowsForcePushes: params[:allows_force_pushes].present?,
        allowsDeletions: params[:allows_deletions].present?,
        isAdminEnforced: params[:enforce_all_for_admins].present?,
        requiresStatusChecks: params[:has_required_statuses].present?,
        requiresStrictStatusChecks: params[:strict_required_status_checks_policy].present?,
        requiresCodeOwnerReviews: params[:require_code_owner_review].present?,
        dismissesStaleReviews: params[:dismiss_stale_reviews_on_push].present?,
        restrictsPushes: params[:authorized_actors].present?,
        reviewDismissalActorIds: review_dismissal_actor_ids,
        pushActorIds: push_actor_ids,
        requiredStatusCheckContexts: params[:contexts] || [],
      },
    })

    if result.errors.any?
      flash[:error] = result.errors.messages.values.flatten.to_sentence
    else
      flash[:notice] = "Branch protection rule settings saved."
    end

    redirect_to branch_protection_rule_path(id: @branch_protection_rule.id)
  end

  DestroyMutation = parse_query <<~'GRAPHQL'
    mutation($id: ID!) {
      deleteBranchProtectionRule(input: { branchProtectionRuleId: $id }) {
        clientMutationId
      }
    }
  GRAPHQL

  def destroy
    # TODO: Remove reliance on this temporary branch protection rule.
    #       It's currently needed to fetch some data in the view that has not been
    #       moved to GraphQL yet and requires a `ProtectedBranch` instance.
    protected_branch = current_repository.protected_branches.find(params[:id])
    @branch_protection_rule = Platform::Models::BranchProtectionRule.new(protected_branch)

    result = platform_execute(DestroyMutation, variables: {
      id: @branch_protection_rule.global_relay_id,
    })

    if result.errors.any?
      flash[:error] = result.errors.messages.values.flatten.to_sentence
    else
      flash[:notice] = "Branch protection rule was successfully deleted."
    end

    redirect_to edit_repository_branches_path
  end

  private
  def review_dismissal_actor_ids
    (User.find(Array(params[:dismiss_user_ids])) + Team.find(Array(params[:dismiss_team_ids]))).map(&:global_relay_id)
  end

  def push_actor_ids
    actors = User.find(Array(params[:push_user_ids])) +
             Team.find(Array(params[:push_team_ids])) +
             Integration.find(Array(params[:push_integration_ids]))
    actors.map(&:global_relay_id)
  end

  def ensure_plan_supports_protected_branches
    unless current_repository.plan_supports?(:protected_branches)
      render plain: "Upgrade to #{current_repository.next_plan} or make this repository public to enable this feature.", status: 403
    end
  end

  def ensure_user_can_update_branch_protection
    unless current_repository.can_update_protected_branches?(current_user)
      render plain: "Protected branch updating is disabled on this repository.", status: 403
    end
  end
end
