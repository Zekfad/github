# frozen_string_literal: true

# Accepts image file uploads from the issue/pull request comment forms. This
# is typically triggered by dragging and dropping images onto the browser
# window.
class UploadsController < ApplicationController
  areas_of_responsibility :data_infrastructure

  before_action :login_required
  before_action :ensure_asset

  # Signal that the upload completed successfully. The browser performs an
  # ajax PUT request to /upload/:id after the file is successfully uploaded
  # to S3.
  #
  # Returns nothing.
  def update
    if current_asset.track_uploaded
      render status: 200, json: current_asset.storage_policy.asset_hash
    else
      render status: 400, json: {errors: current_asset.errors.full_messages}
    end
  end

  private

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def current_asset
    @current_asset ||=
      case params[:model]
      when "assets"
        find_assets
      when "repository-files"
        find_repository_files
      when "repository-images"
        find_repository_images
      when "upload-manifest-files"
        find_upload_manifest_files
      when "releases"
        find_releases
      when "marketplace_listing_screenshots"
        find_marketplace_screenshots
      when "marketplace_listing_images"
        find_marketplace_listing_images
      when "oauth_applications"
        find_oauth_applications
      end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def ensure_asset
    return if current_asset.present?
    render(status: 404, json: {errors: ["Invalid Asset"]})
  end

  def find_assets
    UserAsset.find_by(id: params[:id], user_id: current_user.id)
  end

  def find_releases
    ReleaseAsset.find_by(id: params[:id], uploader_id: current_user.id)
  end

  def find_oauth_applications
    OauthApplicationLogo.find_by(id: params[:id], uploader_id: current_user.id)
  end

  def find_repository_files
    RepositoryFile.where(id: params[:id], uploader_id: current_user.id).first
  end

  def find_repository_images
    RepositoryImage.where(id: params[:id], uploader_id: current_user.id).first
  end

  def find_upload_manifest_files
    UploadManifestFile.where(id: params[:id], uploader_id: current_user.id).first
  end

  def find_marketplace_screenshots
    Marketplace::ListingScreenshot.where(id: params[:id], uploader_id: current_user.id).first
  end

  def find_marketplace_listing_images
    Marketplace::ListingImage.where(id: params[:id], uploader_id: current_user.id).first
  end

  def target_for_conditional_access
    case current_asset
    when ReleaseAsset, RepositoryFile, UploadManifestFile, RepositoryImage
      current_asset.repository.owner
    when UserAsset
      current_asset.uploader
    when Marketplace::ListingScreenshot, Marketplace::ListingImage
      current_asset.listing.owner
    when OauthApplicationLogo
      # Test performs an upload with no OAuth Application ID
      return :no_target_for_conditional_access unless current_asset.oauth_application
      current_asset.oauth_application.owner
    when nil
      # Will 404 in `ensure_asset`
      :no_target_for_conditional_access
    end
  end
end
