# frozen_string_literal: true

module ShowcaseItemsControllerMethods
  INDICATORS = {
      positive: { octicon: "check", class: "mr-2 text-green" },
      negative: { octicon: "x", class: "mr-2 text-red"},
    }.freeze

  def create_item(item_path)
    @repo = ::Repository.with_name_with_owner(item_hash[:name_with_owner])
    @item = @collection.items.build(item: @repo, body: item_hash[:body])

    unless @repo
      flash[:error] = "Couldn’t find repo '#{item_hash[:name_with_owner]}'"
      case item_path
      when :biztools
        render "biztools/showcase_items/edit", locals: { collection: @collection, item: @item } and return
      when :stafftools
        render "stafftools/showcase_items/edit", locals: { collection: @collection, item: @item } and return
      else
        raise NotImplementedError
      end
    end

    if @item.save
      flash[:notice] = "#{@repo.name_with_owner} added to #{@collection.name}"
      redirect_to [item_path, @collection]
    else
      flash[:notice] = @item.errors.full_messages.to_sentence

      case item_path
      when :biztools
        render "biztools/showcase_items/edit", locals: { collection: @collection, item: @item }
      when :stafftools
        render "stafftools/showcase_items/edit", locals: { collection: @collection, item: @item }
      else
        raise NotImplementedError
      end
    end
  end

  def update_item(item_path)
    if @item.update(item_hash)
      flash[:notice] = "#{@item} successfully updated"
      redirect_to [item_path, @collection]
    else
      flash[:error] = "Something went wrong there"

      case item_path
      when :biztools
        render "biztools/showcase_items/edit", locals: { collection: @collection, item: @item }
      when :stafftools
        render "stafftools/showcase_items/edit", locals: { collection: @collection, item: @item }
      else
        raise NotImplementedError
      end
    end
  end

  def edit_item(item_path)
    case item_path
    when :biztools
      render "biztools/showcase_items/edit", locals: { collection: @collection, item: @item }
    when :stafftools
      render "stafftools/showcase_items/edit", locals: { collection: @collection, item: @item }
    else
      raise NotImplementedError
    end
  end

  def destroy_item(item_path)
    @item.destroy
    flash[:notice] = "#{@item} removed from #{@collection}"
    redirect_to [item_path, @collection]
  end

  def perform_healthcheck
    repo = ::Repository.with_name_with_owner(params[:repo_name])
    community_profile = (repo && repo.community_profile) || CommunityProfile.new
    if community_profile.health_percentage == 100
      health_message = "This project meets minimum standards for a healthy community."
    else
      health_message = "When curating Showcases, we recommend that open source projects include these health files:"
    end

    health_stats = {
      score: community_profile.health_percentage,
      readme: community_profile.readme? ? INDICATORS[:positive] : INDICATORS[:negative],
      code_of_conduct: community_profile.code_of_conduct? ? INDICATORS[:positive] : INDICATORS[:negative],
      license: community_profile.license? ? INDICATORS[:positive] : INDICATORS[:negative],
      contributing: community_profile.contributing? ? INDICATORS[:positive] : INDICATORS[:negative],
      description: community_profile.description? ? INDICATORS[:positive] :INDICATORS[:negative],
      pr_or_issue_template: community_profile.pr_or_issue_template? ? INDICATORS[:positive] :INDICATORS[:negative],
    }

    respond_to do |format|
      format.html { render partial: "biztools/showcase_items/health", locals: {health_message: health_message, health_stats: health_stats} }
    end
  end

  private

  def item_hash
    params.require(:showcase_item).permit(:name_with_owner, :body)
  end

  def find_item
    @item = ::Showcase::Item.find(params[:id])
  end

  def find_collection
    @collection = ::Showcase::Collection.find_by_slug!(params[:showcase_collection_id])
  end
end
