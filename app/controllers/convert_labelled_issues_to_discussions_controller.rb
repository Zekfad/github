# frozen_string_literal: true

class ConvertLabelledIssuesToDiscussionsController < AbstractRepositoryController
  before_action :require_feature
  before_action :login_required
  before_action :require_label
  before_action :require_convertable_label

  layout :repository_layout

  def show
    issue_count = label.convertable_issues(current_user).size
    discussion_categories = if discussion_categories_enabled?
      current_repository.discussion_categories.sort
    else
      []
    end

    respond_to do |format|
      format.html do
        render "convert_labelled_issues_to_discussions/show", layout: false,
          locals: { label: label, issue_count: issue_count, discussion_categories: discussion_categories }
      end
    end
  end

  def create
    category = if params[:category_id]
      current_repository.discussion_categories.find_by(id: params[:category_id])
    end

    ConvertLabelledIssuesToDiscussionsJob.perform_later(current_user, label, category: category)

    flash[:notice] = "Open issues with label '#{label.name}' are being converted to discussions."
    redirect_to gh_labels_path(current_repository)
  end

  private

  def label
    return @label if defined?(@label)
    @label = current_repository.labels.find_by_id(params[:id])
  end

  def require_label
    render_404 unless label
  end

  def require_convertable_label
    render_404 unless current_repository.can_convert_issues_to_discussions?(current_user)
  end

  def require_feature
    render_404 unless show_discussions?
  end
end
