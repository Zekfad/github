# frozen_string_literal: true

class NotificationsController < ApplicationController
  SUBSCRIPTIONS_PER_PAGE = 25

  statsd_tag_actions only: [:index, :mark_as_unread, :subscription]

  # The only events we currently support subscribing to with custom thread notifications
  SUPPORTED_THREAD_SUBSCRIPTION_EVENTS = %w(
    closed
    reopened
    merged
  )

  UNWATCH_LIMIT = 10

  around_action :select_write_database, only: [:beacon, :unsubscribe_via_email, :email_mute_via_list, :email_mute_via_footer, :email_mute_vulnerabilities]

  before_action :start_timer, only: :index
  after_action :send_timer, only: :index

  before_action :login_required, except: [:beacon, :email_mute_via_list]
  before_action :summary_required, only: [:mute, :unmute, :mark_as_unread]
  before_action :validate_thread_subscription_events, only: [:thread_subscribe]

  # This must come *after* login_required, so we don't check the repository's
  # privacy settings before we check if the user is logged in.
  include RepositoryControllerMethods
  include ActionView::Helpers::NumberHelper

  IndexQuery = parse_query <<-'GRAPHQL'
    query($includeSavedThreads: Boolean!, $savedThreadsLimit: Int!, $after: String) {
      ...Views::Notifications::Index::Root
    }
  GRAPHQL

  def index
    headers["Cache-Control"] = "no-cache, no-store"

    return render_404 if (params[:repository] && current_list.nil?)

    @view = Notifications::IndexView.new \
      user: current_user,
      list: current_list,
      controller: self,
      unauthorized_saml_targets: unauthorized_saml_targets,
      unauthorized_ip_whitelisting_targets: unauthorized_ip_whitelisting_targets

    data = platform_execute(IndexQuery, variables: {
      after: params[:after].presence,
      includeSavedThreads: @view.display_saved_threads?,
      savedThreadsLimit: @view.saved_threads_limit })

    respond_to do |format|
      format.html do
        render "notifications/index", locals: { data: data }
      end
      format.rss do
        return render_404
      end
    end
  end

  def mark_as_read
    response = \
      if params[:ids].present?
        GitHub.newsies.web.mark_summaries_as_read(current_user, params[:ids])
      else
        response = GitHub.newsies.web.mark_all_notifications_as_read(notification_mark_time, current_user, current_list)
        if response.success? && !response.value
          flash[:notice] = "We are marking your notifications as read right now. This may take a minute or two to process."
        end

        response
      end

    flash[:error] = "Mark as read is not available at the moment." if response.failed?

    if request.xhr?
      head response.success? ? 200 : 503
    else
      redirect_to notifications_path
    end
  end

  def mark_as_unread
    response = GitHub.newsies.web.mark_summary_unread(current_user, summary)
    head(response.success? ? 200 : 503)
  end

  def mute
    response = GitHub.newsies.web.mute(current_user, summary)
    head(response.success? ? 200 : 503)
  end

  def unmute
    response = GitHub.newsies.web.unmute(current_user, summary)
    head(response.success? ? 200 : 503)
  end

  # A page to manage your subscription status for a specific repository
  def subscription
    return render_404 unless current_list.is_a?(Repository)
    return render_404 if current_list.hide_from_user?(current_user)

    @subscription_response = GitHub.newsies.subscription_status(current_user, current_list)
    @subscription = @subscription_response.value

    render "notifications/subscription"
  end

  # Unsubscribe from the current repo. Intended to be hit via email.
  def unsubscribe_via_email
    return render_404 unless current_list.is_a?(Repository)

    if GitHub.newsies.valid_unsubscribe_token? params[:data], current_user, current_list
      response = GitHub.newsies.unsubscribe current_user, current_list
      if response.success?
        flash[:notice] = "You are no longer watching this repository."
      else
        flash[:error] = "Unsubscribe is not available at the moment."
      end
    else
      flash[:error] = "Sorry, someone sent you an invalid link."
    end

    redirect_to repository_path(current_list)
  end

  GIF = "GIF89a\001\000\001\000\200\377\000\377\377\377\000\000\000,\000\000\000\000\001\000\001\000\000\002\002D\001\000;".b

  def beacon
    begin
      user, id, extra_data = GitHub.newsies.user_and_id_from_token :beacon, params[:data]
      if user && id
        summary_response = GitHub.newsies.web.find_rollup_summary_by_id(id)
        summary = summary_response.value
        if summary_response.success? && summary.present?
          summary_hash_response = GitHub.newsies.web.summary_hash_from_summary_id(user, id)
          summary_hash = summary_hash_response.value
          # Only mark unread items as read
          if summary_hash_response.success? && summary_hash.present? && summary_hash[:unread]
            GitHub.newsies.web.mark_summary_read(user, summary)
          end

          if extra_data.present?
            GlobalInstrumenter.instrument(
              "notifications.read",
              user: user,
              list_type: summary.newsies_list.type,
              list_id: summary.newsies_list.id,
              thread_type: summary.newsies_thread.type,
              thread_id: summary.newsies_thread.id,
              comment_type: extra_data["comment_type"],
              comment_id: extra_data["comment_id"],
              handler: :email,
            )
          end
        end
      end
    rescue Exception => e # rubocop:disable Lint/RescueException
      Failbot.report!(e)
    end
    send_data GIF, type: "image/gif", disposition: "inline"
  end

  def email_mute_via_footer
    user = fetch_user_from_token(:mute_auth, params[:data])

    if @summary_response.success? && user
      Failbot.push(user: user.login)
      summary = @summary_response.value
      if current_user.login != user.login
        flash[:error] = "Whoops! That unsubscribe link isn't valid for your account."
        return redirect_to summary.thread.permalink
      end

      return if mute_thread(user, summary)
    end

    # TODO: old unsubscribe page; consider redesigning or removing
    @view = Notifications::EmailMuteView.new(@summary_response, @mute_response)
    render "notifications/email_mute"
  end

  def email_mute_vulnerabilities
    user = fetch_user_from_token(:mute_vuln, params[:data])

    if user
      Failbot.push(user: user.login)
      if current_user.login != user.login
        flash[:error] = "Whoops! That unsubscribe link isn't valid for your account."
      else
        response = GitHub.newsies.get_and_update_settings user do |settings|
          settings.vulnerability_email = false
        end

        if response.failed?
          flash[:error] = "Updating notification settings is unavailable right now."
        end
      end
    end

    redirect_to settings_user_notifications_path
  end

  def email_mute_via_list
    user = fetch_user_from_token(:mute_list, params[:data])

    if @summary_response.success? && user
      Failbot.push(user: user.login)
      summary = @summary_response.value
      return if mute_thread(user, summary)
    end

    # TODO: old unsubscribe page; consider redesigning or removing
    @view = Notifications::EmailMuteView.new(@summary_response, @mute_response)
    render "notifications/email_mute"
  end

  def fetch_user_from_token(token, data)
    user, id = GitHub.newsies.user_and_id_from_token(token, data)
    @summary_response = GitHub.newsies.web.find_rollup_summary_by_id(id.to_i)
    @mute_response = nil

    user
  end

  def mute_thread(user, summary)
    if summary
      @mute_response = GitHub.newsies.web.mute(user, summary)
      if @mute_response.success? && summary.thread
        if logged_in?
          flash[:notice] = "You’ve been unsubscribed from this thread."
          redirect_to summary.thread.permalink
        else
          # TODO: old unsubscribe page; consider redesigning or removing
          @view = Notifications::EmailMuteView.new(@summary_response, @mute_response)
          render "notifications/email_mute"
        end
        true
      end
    end
  end

  def subscribe
    xhr_error_status = 404
    is_success = false

    if params[:repository_global_id]
      repo = typed_object_from_id([Platform::Objects::Repository], params[:repository_global_id])
    else
      repo = Repository.find_by_id(params[:repository_id].to_i)
    end

    if repo && repo.pullable_by?(current_user)
      xhr_error_status = request.get? ? 405 : 422
      is_success =
        case params[:do]
        when "release_only" then current_user.subscribe_to_releases_only(repo)
        when "subscribed"   then current_user.watch_repo(repo)
        when "ignore"       then current_user.ignore_repo(repo)
        else                     current_user.unwatch_repo(repo)
        end
    end

    if request.xhr?
      if is_success
        render json: { count: number_with_delimiter(repo.watchers_next_count) }
      else
        head(xhr_error_status)
      end
    else
      flash[:notice] = "Subscription status updated." if is_success
      location = request.referrer.present? ? :back : watching_path
      redirect_to location
    end
  end

  def unwatch_all
    GitHub.newsies.async_delete_list_subscriptions_for_user(current_user.id, "Repository")

    flash[:notice] = (
      "We're unwatching your repositories in the background. " +
      "Please wait a few minutes and then refresh the page to see your changes."
    )

    redirect_to watching_path
  end

  class UnexpectedThreadTypeError < StandardError
    def initialize(thread_type, expected)
      @thread_type = thread_type
      @expected = expected
    end

    def message
      "#{@thread_type.inspect} isn’t one of #{@expected.inspect}"
    end
  end

  # Subscribe, unsubscribe, or mute a single thread.
  # This happens on the thread's page, eg issues/show
  #
  # params[:repository_id] - The repo you're acting on.
  #     params[:thread_id] - The thread (issue,commit) you're acting on.
  #  params[:thread_class] - The thread's class - Issue, Commit, Discussion.
  #            params[:id] - The String action to perform.
  #                          One of: subscribe, unsubscribe, mute, subscribe_to_custom_notifications.
  def thread_subscribe
    return render_404 unless current_list.is_a?(Repository)

    thread = GitHub.newsies.thread(
      current_list,
      params[:thread_class],
      params[:thread_id],
      actor: current_user)

    if !thread || (thread.is_a?(Discussion) && !show_discussions?) || !thread.readable_by?(current_user)
      head :not_found
      return
    end

    case thread_subscribe_action
    when "subscribe"
      GitHub.newsies.subscribe_to_thread(current_user, current_list, thread, "manual")
    when "unsubscribe"
      GitHub.newsies.unsubscribe(current_user, current_list, thread)
    when "mute"
      GitHub.newsies.ignore_thread(current_user, current_list, thread)
    when "subscribe_to_custom_notifications"
      GitHub.newsies.subscribe_to_thread(
        current_user,
        current_list,
        thread,
        { reason: "manual", force: true },
        valid_thread_subscription_events)
    end

    respond_to do |format|
      format.html do
        if request.xhr?
          render_partial_view(
            "notifications/thread_subscription",
            Notifications::ThreadSubscriptionView,
            list: current_list,
            thread: thread,
            display_explanation_text: true,
          )
        else
          path = case params[:thread_class]
          when "Issue"
            issue_path(thread)
          when "Commit"
            commit_path(thread)
          when "Discussion"
            discussion_path(thread)
          else
            error = UnexpectedThreadTypeError.new(params[:thread_class], %w[Issue Commit])
            error.set_backtrace(caller)
            Failbot.report(error, thread_class: params[:thread_class])
            nil
          end

          redirect_to(path + "#thread-subscription-status")
        end
      end
    end
  end

  def thread_subscription
    thread = nil

    if repo = Repository.find_by_id(params[:repository_id])
      if repo.pullable_by?(current_user)
        thread = GitHub.newsies.thread(repo, params[:thread_class],
          params[:thread_id], actor: current_user)
      end
    end

    if !thread
      head :not_found
      return
    end

    respond_to do |format|
      format.html do
        render_partial_view(
          "notifications/thread_subscription",
          Notifications::ThreadSubscriptionView,
          list: repo,
          thread: thread,
          display_explanation_text: true,
        )
      end
    end
  end

  UnwatchSuggestionsQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::Notifications::UnwatchSuggestions
    }
  GRAPHQL

  def watching
    visible_repository_ids = current_user.associated_repository_ids(
      include_oauth_restriction: false,
    )

    newsies_responses = {
      watched_repositories: GitHub.newsies.subscribed_repositories(
        current_user,
        set: :subscribed,
        page: subscriptions_page,
        per_page: SUBSCRIPTIONS_PER_PAGE,
        visible_repo_ids: visible_repository_ids,
      ),
      watched_repositories_count: GitHub.newsies.count_subscriptions(
        current_user,
        set: :subscribed,
        list_type: ::Repository.name,
      ),
      releases_only_repositories: GitHub.newsies.subscribed_repositories(
        current_user,
        set: :releases_only,
        page: subscriptions_page,
        per_page: SUBSCRIPTIONS_PER_PAGE,
        visible_repo_ids: visible_repository_ids,
      ),
      releases_only_repositories_count: GitHub.newsies.count_thread_type_subscriptions(
        current_user.id,
      ),
      ignored_repositories: GitHub.newsies.subscribed_repositories(
        current_user,
        set: :ignored,
        page: subscriptions_page,
        per_page: SUBSCRIPTIONS_PER_PAGE,
        visible_repo_ids: visible_repository_ids,
      ),
      ignored_repositories_count: GitHub.newsies.count_subscriptions(
        current_user,
        set: :ignored,
        list_type: ::Repository.name,
      ),
    }

    locals = { newsies_responses: newsies_responses }

    if GitHub.flipper[:unwatch_suggestions].enabled?(current_user) && !current_user.dismissed_unwatch_suggestions?
      begin
        locals[:unwatch_suggestions] = { response: platform_execute(UnwatchSuggestionsQuery) }
      rescue PlatformHelper::ExecutionError
        locals[:unwatch_suggestions] = { error: true }
      end
    end

    render "notifications/watching", locals: locals
  end

  def unwatch_repositories
    unless params[:repository_ids].present? && GitHub.flipper[:unwatch_suggestions].enabled?(current_user)
      return redirect_to watching_path
    end

    lists = params[:repository_ids][0...UNWATCH_LIMIT].map { |id| Newsies::List.new(Newsies::List.type_from_class(Repository), id.to_i) }
    Newsies::ListSubscription.unsubscribe(current_user.id, lists)

    flash[:notice] = "Successfully unwatched suggested repositories."
    redirect_to watching_path
  end

  # Saves the notification handler types (email, web)
  def save_handlers
    handler_keys = GitHub.newsies.handler_keys

    response = GitHub.newsies.get_and_update_settings current_user do |settings|
      %w(participating subscribed).each do |prefix|
        key = "#{prefix}_settings"
        sent = save_handler_params key, handler_keys
        next if sent.blank?

        handlers = settings.send(key)
        sent.each do |handler, checked|
          if checked == "1"
            handlers << handler
          else
            handlers.delete(handler)
          end
        end
        handlers.uniq!
      end
    end

    if request.xhr?
      head response.success? ? 200 : 503
    else
      if response.failed?
        flash[:error] = "Updating notification settings is unavailable right now."
      end
      redirect_to settings_user_notifications_path
    end
  end

  # Saves the preferred email for notifications
  def save_email_settings
    notifiable_emails = current_user.notifiable_emails
    redirect_path = settings_user_notifications_path

    if params[:redirect_path] == "email_settings"
      redirect_path = settings_user_emails_path
    end

    settings_response = GitHub.newsies.get_and_update_settings(current_user) do |settings|
      settings.email :global, params[:email].to_s.strip
      @cleared_emails = settings.clear_unverified_emails(notifiable_emails)
      @settings = settings
    end

    if settings_response.failed?
      flash[:error] = "Updating notification settings is unavailable right now."
      return redirect_to redirect_path
    end

    if @cleared_emails && GitHub.email_verification_enabled?
      flash[:error] = "The following emails are unverified: #{@cleared_emails.to_a.to_sentence}"
      redirect_to redirect_path
    else
      flash[:notice] = "Your default notification email was changed to #{@settings.email(:global).address}."
      redirect_to redirect_path
    end
  end

  UpdateOrganizationRoutingMutation = parse_query <<-'GRAPHQL'
     mutation($input: UpdateEmailRouteInput!) {
      updateEmailRoute(input: $input) {
        emailRoute {
          organization {
            login
          }
        }
      }
    }
  GRAPHQL

  def update_organization_routing
    input_variables = {
      organizationId: params[:organization_id],
      email: params[:email],
    }

    result = platform_execute(UpdateOrganizationRoutingMutation, variables: { input: input_variables })

    if result.errors.any?
      flash[:error] = result.errors.all.values.flatten.to_sentence
    else
      flash[:notice] = "Successfully updated notification routing for #{result.update_email_route.email_route.organization.login}"
    end

    redirect_to settings_user_notifications_path
  end

  # Toggles the auto subscribe repositories setting in Newsies
  # Handling for legacy route for users that haven't reloaded the page
  def toggle_auto
    toggle_flag do |settings|
      settings.auto_subscribe_repositories = params[:auto_subscribe] == "1"
    end
  end

  # Toggles the auto subscribe repositories setting in Newsies
  def toggle_auto_subscribe_repositories
    toggle_flag do |settings|
      settings.auto_subscribe_repositories = params[:auto_subscribe_repositories] == "1"
    end
  end

  # Toggles the auto subscribe teams setting in Newsies
  def toggle_auto_subscribe_teams
    toggle_flag do |settings|
      settings.auto_subscribe_teams = params[:auto_subscribe_teams] == "1"
    end
  end

  # Toggles the setting for receiving own contributions via email
  def toggle_own
    toggle_flag do |settings|
      settings.notify_own_via_email = params[:notify_own_via_email] == "1"
    end
  end

  def toggle_comment
    toggle_flag do |settings|
      settings.notify_comment_email = params[:notify_comment_email] == "1"
    end
  end

  def toggle_pull_request_review
    toggle_flag do |settings|
      settings.notify_pull_request_review_email = params[:notify_pull_request_review_email] == "1"
    end
  end

  def toggle_pull_request_push
    toggle_flag do |settings|
      settings.notify_pull_request_push_email = params[:notify_pull_request_push_email] == "1"
    end
  end

  def toggle_vulnerability
    toggle_flag do |settings|
      settings.vulnerability_ui_alert = params[:vulnerability_ui_alert] == "1"
      settings.vulnerability_cli = params[:vulnerability_cli] == "1"
      settings.vulnerability_web = params[:vulnerability_web] == "1"
      settings.vulnerability_email = params[:vulnerability_email] == "1"
    end
  end

  def toggle_ci
    toggle_flag do |settings|
      settings.continuous_integration_web = params[:continuous_integration_web] == "1"
      settings.continuous_integration_email = params[:continuous_integration_email] == "1"
      settings.continuous_integration_failures_only = params[:continuous_integration_failures_only] == "1"
    end
  end

  def toggle_digest_subscription
    if params[:vulnerability_digest] == "1"
      if params[:subscription_kind] == "daily"
        NewsletterSubscription.subscribe(current_user, "vulnerability", "daily")
      else
        NewsletterSubscription.subscribe(current_user, "vulnerability", "weekly")
      end
    else
      @newsletter ||= NewsletterSubscription.where("user_id = ? AND name = ?", current_user.id, "vulnerability").first
      NewsletterSubscription.unsubscribe(@newsletter.unsubscribe_token) if @newsletter
    end

    redirect_to settings_user_notifications_path
  end

  private

  def save_handler_params(required_key, permitted_keys)
    return ActionController::Parameters.new unless params.key?(required_key)
    params.require(required_key).permit *permitted_keys
  end

  def toggle_flag
    response = GitHub.newsies.get_and_update_settings current_user do |settings|
      yield settings
    end

    if request.xhr?
      head response.success? ? 200 : 503
    else
      if response.failed?
        flash[:error] = "Updating notification settings is unavailable right now."
      end
      redirect_to watching_path
    end
  end

  def start_timer
    @stats_timer = Time.now
  end

  def send_timer
    return unless @view

    ms = ((Time.now - @stats_timer) * 1000).round
    GitHub.dogstats.timing("newsies.view.new_inbox", ms)
    GitHub.dogstats.count("newsies.view.new_inbox.entries", @view.notifications.size)
  end

  # Overrides RepositoryControllerMethods#privacy_check.
  # This method is run as a `before_action` on every route.
  def privacy_check
    # Return early from a variety of cases if the privacy check passes.
    if current_list.is_a?(Team)
      return if current_list.visible_to?(current_user)
    elsif current_list.is_a?(Repository)
      return if current_list.pullable_by?(current_user)
    else
      return unless repository_specified?
    end

    if logged_in? && current_list.is_a?(Repository)
      CleanupListNotificationsJob.perform_later(current_user.id, "Repository", [current_list.id])
      GitHub.dogstats.count("newsies.cleanup.inaccessible_lists", 1, tags: ["method:privacy_check"])
    end

    # Privacy check has failed, so respond with an error.
    request.xhr? ? head(404) : render_404
  end

  def notification_mark_time
    @notification_mark_time ||= Time.at(params[:mark_by].to_i)
  end

  helper_method :subscriptions_page
  def subscriptions_page
    @subscriptions_page ||= [params[:page].to_i, 1].max
  end

  def map_subscriptions_to_repositories(subscriptions, bad_list_set)
    repositories = []
    subscriptions.each do |sub|
      if sub.list
        repositories << sub.list
      else
        bad_list_set << sub.list_id
      end
    end
    repositories
  end

  # Private: Returns the current notification list as an object.
  def current_list
    if params[:list_type]&.downcase == "team"
      current_team
    elsif repository_specified?
      current_repository
    elsif params[:id]
      summary&.list
    end
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def current_team
    @current_team ||= begin
      # HACK: Rather than commit to a large refactor of this controller, just repurpose the
      # repository argument for the team slug.
      team_slug = params[:repository]

      owner.teams.where(slug: team_slug).first if owner.is_a?(Organization) && team_slug.present?
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def summary_required
    if !params[:id] && request.xhr?
      return head(404)
    elsif !params[:id]
      flash[:error] = "Sorry, we couldn't find that notification."
      return redirect_to(notifications_path)
    end

    if summary_response.failed? && request.xhr?
      return head(503)
    elsif summary_response.failed?
      flash[:error] = "#{params[:action].humanize} is not available at the moment."
      return redirect_to(notifications_path)
    end

    if summary.blank? && request.xhr?
      return head(404)
    elsif summary.blank?
      flash[:error] = "Sorry, we couldn't find that notification."
      return redirect_to(notifications_path)
    end

    nil
  end

  def summary_response
    @summary_response ||= GitHub.newsies.web.find_rollup_summary(params[:id]) if params[:id]
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def summary
    @summary ||= summary_response&.value
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # Overrides RepositoryControllerMethods#repository_specified?
  def repository_specified?
    super || params[:repository_id]
  end

  # Overrides RepositoryControllerMethods#current_repository
  def current_repository
    super if !params[:repository_id]
    return @current_repository if defined?(@current_repository)
    @current_repository = Repository.find_by_id(params[:repository_id])
  end

  def validate_thread_subscription_events
    if params[:id] == "subscribe_to_custom_notifications" && invalid_thread_subscription_events.any?
      head(:bad_request)
    end
  end

  def thread_subscribe_action
    action = params[:id]

    if action == "subscribe_to_custom_notifications" && params[:events].blank?
      # The user has deselected the last custom notification event, so unsubscribe
      # them from the thread to reset their settings.
      action = "unsubscribe"
    end

    action
  end

  def invalid_thread_subscription_events
    @invalid_thread_subscription_events ||= (params[:events] || []) - SUPPORTED_THREAD_SUBSCRIPTION_EVENTS
  end

  def valid_thread_subscription_events
    @valid_thread_subscription_events ||= params[:events] & SUPPORTED_THREAD_SUBSCRIPTION_EVENTS
  end
end
