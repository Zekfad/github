# frozen_string_literal: true

class Apps::BetaFeaturesController < ApplicationController
  include OrganizationsHelper
  include OauthApplicationsHelper

  before_action :login_required_or_org_admins_only
  before_action :sudo_filter

  before_action :find_feature_flag
  before_action :find_application

  def enable
    GitHub.flipper[@feature_flag].enable(@application)

    flash[:notice] = "#{feature_name} is being enabled for #{@application.name}."
    redirect_to settings_oauth_application_beta_features_path(@application)
  end

  def disable
    GitHub.flipper[@feature_flag].disable(@application)

    flash[:notice] = "#{feature_name} is being disabled for #{@application.name}."
    redirect_to settings_oauth_application_beta_features_path(@application)
  end

  private

  def feature_name
    return @feature_name if defined?(@feature_name)
    @feature_name = Apps::BetaFeatureComponent::BETA_FEATURES[@feature_flag][:title]
  end

  def find_application
    return @application if defined?(@application)
    @application = current_context.oauth_applications.find(params[:id])
  end

  def find_feature_flag
    potential_flag = params[:feature_flag]&.to_sym
    return render_404 unless Apps::BetaFeatureComponent::BETA_FEATURES.key?(potential_flag)

    @feature_flag = potential_flag
  end
end
