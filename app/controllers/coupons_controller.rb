# frozen_string_literal: true
class CouponsController < ApplicationController
  include BillingSettingsHelper
  include OrganizationsHelper

  before_action :find_coupon
  before_action :login_required, only: [:redeem, :show_golden_ticket,
    :redeem_golden_ticket, :confirm_golden_ticket]
  before_action :check_ofac_sanctions, except: [:redeem]
  before_action :redemption_trade_controls_restrictions, only: [:redeem]

  before_action :golden_ticket_required, only: [:show_golden_ticket,
      :redeem_golden_ticket]

  before_action :business_plus_only_required, only: [:show_business_plus_only]

  # The following actions do not require conditional access checks because
  # they *don't* access protected organization resources.
  skip_before_action :perform_conditional_access_checks, only: %w(
    find
    show
    redirect_to_redeem
    show_golden_ticket
    redeem_golden_ticket
    confirm_golden_ticket
    show_business_plus_only
  )

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:show, :redeem],
    key: :coupon_rate_limit_key,
    max: 1000,
    ttl: 1.hour,
    stealthy: true

  def find
    render "coupons/find"
  end

  def show
    if @coupon
      @plan = @coupon.try(:plan) || GitHub::Plan.default_plan
      override_analytics_location(request.path.sub(@coupon.code, "<coupon-code>"))

      if @coupon.golden_ticket?
        redirect_to show_golden_ticket_coupon_path(code: @coupon.code)
      elsif @coupon.business_plus_only_coupon?
        if logged_in? && (@coupon.dreamforce_2017_coupon? || @coupon.eligible_accounts(current_user).empty?)
            redirect_to new_organization_path(coupon: @coupon.code)
        else
          show_business_plus_only
        end
      elsif @coupon.org_only? && user_has_no_organizations?
        redirect_to new_organization_path(coupon: @coupon.code)
      elsif logged_in? && !@coupon.redeemable_by?(current_user)
        flash[:error] = "This coupon can't be redeemed with your account. Please contact support."
        redirect_to find_coupon_url
      else
        render "coupons/show"
      end
    else
      check_rate_limit(check_without_increment: false)
      return if performed?

      if Coupon.valid_code? params[:code]
        flash[:error] = "Sorry, we couldn’t find a coupon with the code: #{params[:code]}"
      else
        flash[:error] = "Sorry, we couldn’t find a coupon with that code"
      end
      redirect_to find_coupon_url
    end
  end

  def redeem
    @selected_plan = GitHub::Plan.find(params[:plan])

    if selected_account && @coupon
      details = payment_details.merge(actor: current_user, plan: @selected_plan)
      result = GitHub::Billing.redeem_coupon_and_charge(selected_account, @coupon, details)

      if result.success?
        flash[:notice] = "You have successfully redeemed your coupon."

        if selected_account.organization?
          redirect_to org_dashboard_path(selected_account)
        else
          redirect_to "/"
        end
      else
        flash[:error] = result.error_message
        render "coupons/show"
      end
    else
      check_rate_limit(check_without_increment: false)
      render_404 unless performed?
    end
  end

  def redirect_to_redeem
    redirect_to redeem_coupon_url(params[:code])
  end

  def show_golden_ticket
    @plan = @coupon.plan
    @survey = Survey.find_by_slug("free-private-repositories")
    override_analytics_location(request.path.sub(@coupon.code, "<coupon-code>"))
    render "coupons/show_golden_ticket"
  end

  def show_business_plus_only
    @plan = @coupon.plan
    render "coupons/business_plus_only/show"
  end

  def redeem_golden_ticket
    @plan = @coupon.plan
    @survey = Survey.find_by_slug("free-private-repositories")

    User.transaction do
      answers = (params[:answers] || []).map do |answer|
        SurveyAnswer.new \
          user_id: current_user.id,
          survey_id: @survey.id,
          question_id: answer[:question_id],
          choice_id: answer[:choice_id],
          other_text: answer[:other_text]
      end

      unless answers.map(&:save).all?
        flash[:error] = "All questions are required."
        render "coupons/show_golden_ticket"
      else
        details = payment_details.merge(actor: current_user, plan: @plan)
        result = GitHub::Billing.redeem_coupon_and_charge(current_user, @coupon, details)
        if result.success?
          redirect_to confirm_golden_ticket_coupon_path
        else
          flash[:error] = result.error_message
          render "coupons/show_golden_ticket"
          raise ActiveRecord::Rollback
        end
      end
    end
  end

  def confirm_golden_ticket
    if current_user.coupon && current_user.coupon.group == "golden-ticket-experiment"
      GitHub.dogstats.increment("experiments.free_private_repos.redeemed")
      render "coupons/confirm_golden_ticket"
    else
      redirect_to find_coupon_url
    end
  end

  private

  def user_has_no_organizations?
    logged_in? && current_user.owned_organizations.reject(&:invoiced?).empty?
  end

  def selected_account
    @selected_account ||= \
      if params[:id] == current_user.login
        current_user
      elsif current_organization && current_organization.adminable_by?(current_user)
        current_organization
      end
  end

  def coupon_rate_limit_key
    "coupon-redemption:#{request.remote_ip}"
  end

  def find_coupon
    code = Coupon.clean_old_code(params[:code])
    @coupon = Coupon.find_by_code(code)
    @coupon = nil if @coupon && @coupon.expired?
  end

  def golden_ticket_required
    unless @coupon && @coupon.group == "golden-ticket-experiment"
      redirect_to find_coupon_url
      return false
    end

    if current_user.coupon && current_user.coupon == @coupon
      redirect_to confirm_golden_ticket_coupon_path
      return false
    end

    unless golden_ticket_matches_user?
      flash[:error] = "The code #{@coupon.code} isn't valid for the user currently signed in."
      redirect_to find_coupon_url
      return false
    end

    true
  end

  def business_plus_only_required
    unless @coupon && @coupon.business_plus_only_coupon?
      render_404
      return false
    end

    true
  end

  def golden_ticket_matches_user?
    @coupon && @coupon.code.split("-").last == User.encode_id(current_user.id)
  end

  def redemption_trade_controls_restrictions
    if selected_account&.has_any_trade_restrictions?
      if selected_account.organization?
        flash[:trade_controls_organization_billing_error] = true
        return redirect_to(org_root_path(selected_account))
      else
        flash[:trade_controls_user_billing_error] = true
        return redirect_to(billing_url)
      end
    end
    check_ofac_sanctions
  end
end
