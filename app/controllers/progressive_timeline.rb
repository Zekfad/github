# frozen_string_literal: true

# depends on base#current_user and base#timeline_owner being present,
# where base is typically the IssueCommentsController, IssuesController, or PullRequestsController
module ProgressiveTimeline
  extend ActiveSupport::Concern

  UnsupportedTimelineOwnerError = Class.new(StandardError)

  included do
    helper_method :visible_timeline_items
    helper_method :showing_full_timeline?
  end

  DEFAULT_TIMELINE_ITEMS_LIMIT = 120

  # Reasons for showing a partial timeline, as required by the partial endpoint in issues and pull requests controllers
  EXPECTED_REASONS = %w[
    view-more-button
    expose-fragment
  ].freeze

  # Returns an Array of timeline items to be rendered in the current page view.
  # If the timeline is large enough some items may be omitted (and can be
  # loaded later via progressive disclosure).
  def visible_timeline_items
    @visible_timeline_items ||= visible_timeline_items!
  end

  # Are we rendering the full timeline in this view? Will be false if the
  # timeline is not being rendered at all or if some items are omitted.
  def showing_full_timeline?
    return @showing_full_timeline if defined?(@showing_full_timeline)
    @showing_full_timeline = !discussion_last_modified_at && !params[:first] && !focused_timeline? && !timeline_slice.limited?
  end

  # Is the request for a timeline segment focused on a particular timeline item? This generally happens
  # when the url includes an anchor to an item which is concealed by progressive disclosure, resulting
  # in a secondary request for the specific segment.
  #
  # @return [true, false]
  def focused_timeline?
    !!params[:focus_value]
  end

  # Convenience method provided to the partial rendering methods in IssuesController and PullRequestsController.
  # This records the counts for explicit "view more" clicks, and implicit progressive disclosure requests
  # triggered via anchor tags in the show issue url.
  def record_metrics_partial_timeline
    reason = if EXPECTED_REASONS.include?(params[:reason])
      params[:reason]
    else
      "other"
    end
    GitHub.dogstats.increment("view", tags: ["subject:#{owner_class_name}", "action:show_partial_timeline_#{reason}"])
  end

  private

  def visible_timeline_items!
    if since = discussion_last_modified_at
      return timeline_owner.timeline_for(current_user, since: since)
    end

    items = timeline_slice.items

    items = [issue_for_timeline_owner] + items unless params[:first]

    record_metrics_full_timeline unless focused_timeline?

    items
  end

  def issue_for_timeline_owner
    case timeline_owner
    when Issue then timeline_owner
    when PullRequest then timeline_owner.issue
    else
      fail UnsupportedTimelineOwnerError, "Owner class: #{timeline_owner.class.name}"
    end
  end

  def timeline_slice
    @timeline_slice ||= begin
      IssueTimelineSlice.new(
        timeline_owner,
        viewer: current_user,
        first:  params[:first] && params[:first].to_i,
        last:   params[:last] && params[:last].to_i,
        limit:  timeline_items_limit,
        # We only include new items on the first page load (to give some context
        # at the end of the conversation). After that we always load the oldest
        # comments.
        new_items_count: params[:first] ? nil : timeline_new_items_count,
        focus_type: params[:focus_type],
        focus_value: params[:focus_value],
        focus_padding: params[:pr_padding] && params[:pr_padding].to_i,
      )
    end
  end

  # The number of PR timeline items to render in a single request
  def timeline_items_limit
    limit = params[:pr_limit] if preview_features?
    limit ||= DEFAULT_TIMELINE_ITEMS_LIMIT
    limit.to_i
  end

  # The number of newest PR timeline items to display on the intial page load
  def timeline_new_items_count
    count = params[:pr_new_items] if preview_features?
    count ||= (timeline_items_limit / 2.0).round
    count.to_i
  end

  def owner_class_name
    timeline_owner.class.name.downcase
  end

  def record_metrics_full_timeline
    key = showing_full_timeline? ? "true" : "false"
    GitHub.dogstats.increment("view", tags: ["subject:#{owner_class_name}", "action:full_timeline_shown_#{key}"])
  end

end
