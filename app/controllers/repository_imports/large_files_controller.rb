# frozen_string_literal: true

module RepositoryImports
  class LargeFilesController < RepositoryImports::BaseController
    areas_of_responsibility :migration

    # Public: List large files found during import and provide a form for opting
    # in or out of large file storage.
    def index
      unless repository_import.has_large_files?
        return redirect_to repository_import_path(repository_import.url_params)
      end

      page = if params[:page].nil?
        1
      else
        params[:page].to_i
      end

      respond_to do |format|
        format.html do
          if pjax?
            render_partial_view "repository_imports/large_files/lfs_file_list", RepositoryImports::LargeFiles::IndexView,
              repository_import: repository_import, page: page
          else
            render_template_view "repository_imports/large_files/index", RepositoryImports::LargeFiles::IndexView,
              repository_import: repository_import, page: page
          end
        end
      end
    end

    # Public: Opt-in or out of using large file storage.
    def update
      data = {
        "committer" => {
          "name"        => current_user.git_author_name,
          "email"       => current_user.git_author_email,
          "time"        => Time.now.to_i,
          "time_offset" => current_user.time_zone.utc_offset,
        },
      }

      if params[:gitlfs] == "opt-in"
        repository_import.lfs_opt_in(data)
      else
        repository_import.lfs_opt_out(data)
      end

      redirect_to repository_import_path(repository_import.url_params)
    end
  end
end
