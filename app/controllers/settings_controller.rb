# frozen_string_literal: true

class SettingsController < ApplicationController
  areas_of_responsibility :account_settings

  include SettingsHelper

  statsd_tag_actions only: [:user_security]

  before_action :login_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :ensure_billing_enabled, only: [:user_billing]
  before_action :ensure_user_can_connect_to_dotcom, only: [:dotcom_user]
  before_action :check_ofac_sanctions, only: [:spending_limit]
  before_action :ensure_gravatar_enabled, only: [:user_has_gravatar]

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:send_two_factor_sms, :add_two_factor_sms_backup],
    key: :settings_rate_limit_key,
    log_key: "2fa-sms",
    max: 5,
    ttl: 1.hour,
    at_limit: :settings_rate_limit_render

  # Don't cache any page where we show the TOTP secret or recovery code secret.
  before_action :set_cache_control_no_store,
                only: %w(two_factor_authentication_recovery_codes
                         two_factor_authentication_verify
                         recovery_download
                         recovery_print)

  # All actions that are invalid unless a user already has 2FA setup.
  two_factor_enabled_actions = %w(two_factor_authentication_disable
                                  add_two_factor_sms_backup
                                  destroy_two_factor_sms_backup
                                  two_factor_authentication)

  # All actions that are invalid unless a user has initiated a 2FA setup flow.
  two_factor_pending_setup_actions = %w(two_factor_authentication_recovery_codes
                                        two_factor_authentication_verify
                                        send_two_factor_sms
                                        recovery_download
                                        recovery_print
                                        two_factor_authentication_enable)

  two_factor_all_actions = two_factor_enabled_actions +
                           two_factor_pending_setup_actions +
                           %w(two_factor_authentication_intro
                              two_factor_authentication_initiate)

  two_factor_sms_actions = %w(
    add_two_factor_sms_backup
    destroy_two_factor_sms_backup
  )

  map_to_service :account_login, only: two_factor_all_actions

  before_action :ensure_two_factor_enabled, only: two_factor_all_actions
  before_action :ensure_user_has_two_factor_enabled, only: two_factor_enabled_actions
  before_action :ensure_user_has_pending_two_factor_setup, only: two_factor_pending_setup_actions
  before_action :sudo_filter, only: two_factor_all_actions
  before_action :ensure_two_factor_sms_enabled, only: two_factor_sms_actions

  javascript_bundle :settings

  # Bring in current_organization helper method.
  include OrganizationsHelper
  include BusinessesHelper

  ORG_NOTIFICATION_ROUTING_PER_PAGE = 100
  TWO_FACTOR_CAP_OPT_OUT_ACTIONS = %w(
    two_factor_authentication_intro
    two_factor_authentication_initiate
    two_factor_authentication_recovery_codes
    two_factor_authentication_verify
    two_factor_authentication_enable
    send_two_factor_sms
    enable_two_factor
    recovery_print
    recovery_download
    two_factor_authentication_disable
    two_factor_authentication
    add_two_factor_sms_backup
    validate_two_factor_sms_backup_confirmation
    send_two_factor_sms_backup_setup_otp
    two_factor_sms_backup_confirmation_data
    add_two_factor_sms_backup_key
    destroy_two_factor_sms_backup
  )

  def user_profile
    @emails = current_user.possible_profile_emails
    @emails << current_user.profile_email if current_user.profile_email.present? && @emails.exclude?(current_user.profile_email)
    render "settings/user/profile"
  end

  def unset_profile_email
    current_user.update_attribute(:profile_email, nil)
    redirect_to settings_user_profile_path(current_user)
  end

  def reset_avatar
    current_user.primary_avatar&.destroy
    flash[:notice] = "Your profile picture has been reset. It may take a few moments to update across the site."
    redirect_to settings_user_profile_path
  end

  def user_has_gravatar
    gravatar_url = "https://en.gravatar.com/#{current_user.gravatar_id}.json"

    begin
      gravatar_response = Faraday.get(gravatar_url, {"Accept" => "application/json"})
      render json: { has_gravatar: gravatar_response.success? }
    rescue Faraday::ConnectionFailed, Faraday::TimeoutError
      return render json: { has_gravatar: false }
    end
  end

  UserNotificationsQuery = parse_query <<-'GRAPHQL'
    query($orgRoutingLimit: Int!, $orgRoutingCursor: String) {
      viewer {
        notificationSettings {
          ...Views::Settings::User::Notifications::Settings
        }
      }
    }
  GRAPHQL

  def user_notifications
    settings_response = GitHub.newsies.settings(current_user)
    settings = settings_response.value

    variables = { orgRoutingLimit: ORG_NOTIFICATION_ROUTING_PER_PAGE }
    data = platform_execute(UserNotificationsQuery, variables: variables)

    newsies_unavailable = data.viewer.errors.any? &&
      data.viewer.errors.details[:notificationSettings]&.first["type"] == "SERVICE_UNAVAILABLE"

    locals = {
      newsies_unavailable: newsies_unavailable,
      settings_response: settings_response,
      settings: settings,
      data: data.viewer.notification_settings,
    }

    render "settings/user/notifications", locals: locals
  end

  UserNotificationOrgRoutingQuery = parse_query <<-'GRAPHQL'
    query($orgRoutingLimit: Int!, $orgRoutingCursor: String) {
      viewer {
        notificationSettings {
          ...Views::Settings::User::Notifications::OrganizationRoutingList::Settings
        }
      }
    }
  GRAPHQL

  def user_notification_org_routing
    return render_404 unless request.xhr?

    variables = {
       orgRoutingLimit: ORG_NOTIFICATION_ROUTING_PER_PAGE,
       orgRoutingCursor: params[:cursor],
    }

    data = platform_execute(UserNotificationOrgRoutingQuery, variables: variables)

    newsies_unavailable = data.viewer.errors.any? && data.viewer.errors.details[:notificationSettings]&.first["type"] == "SERVICE_UNAVAILABLE"
    locals = {
      newsies_unavailable: newsies_unavailable,
      settings: data.viewer.notification_settings,
    }

    respond_to do |format|
      format.html do
        render partial: "settings/user/notifications/organization_routing_list", locals: locals
      end
    end
  end

  def user_admin
    user_migrations = Migration.for_owner(current_user).newest.where("created_at > ?", 7.days.ago).limit(5).includes(:file)
    if GitHub.flipper[:account_succession].enabled?(current_user) && !GitHub.enterprise?
      successor_invite = current_user.get_successor_invitation
      render "settings/user/admin", locals: { user_migrations: user_migrations, successor_invite: successor_invite }
    else
      render "settings/user/admin", locals: { user_migrations: user_migrations }
    end
  end

  def user_emails
    store_location
    render "settings/user/emails"
  end

  UserBillingQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::Settings::User::Billing::Fragment
    }
  GRAPHQL

  def user_billing
    ActiveRecord::Base.connected_to(role: :writing) { current_user.expire_stale_coupon }

    if params[:coupon]
      redirect_to redeem_coupon_path params[:coupon]
    else
      tab = params[:tab]
      return render_404 if tab == "cost_management" && !Billing::Budget.configurable?(current_user)

      data = platform_execute UserBillingQuery
      render "settings/user/billing", locals: { data: data, selected_tab: tab }
    end
  end

  def spending_limit
    owner = current_user
    return render_404 unless Billing::Budget.configurable?(owner)
    return render_404 unless Billing::Budget.valid_product?(params[:product])

    unless owner.has_valid_payment_method?
      flash[:error] = "You can’t increase the spending limits until you set up a valid payment method"
      redirect_to :back
      return
    end

    budget = owner.budget_for(product: params[:product])
    budget.configure(
      enforce_spending_limit: params[:enforce_spending_limit] == "true",
      limit: params[:spending_limit],
    )

    if budget.errors.any?
      flash[:error] = "Unable to set a spending limit. Please check your payment method and limit"
    else
      flash[:notice] = "Spending limit configuration has been updated"
    end

    redirect_to :back
  end

  def new_user_ssh
    @selected_link = settings_user_keys_path
    render "settings/user/new_user_ssh"
  end

  def new_user_gpg
    @selected_link = settings_user_keys_path
    render "settings/user/new_user_gpg"
  end

  def user_ssh
    redirect_to settings_user_keys_path(params[:id])
  end

  def user_keys
    @selected_link = settings_user_keys_path
    @public_key = current_user.public_keys.find_by_id(params[:id])
    user_ssh_keys = current_user.public_keys.to_a
    render_template_view "settings/user/keys",
      Settings::SshKeysView, ssh_keys: user_ssh_keys
  end

  def user_ssh_unknown_origin_audit
    @public_key = PublicKey.find(params[:key])

    if @public_key && @public_key.adminable_by?(current_user)
      render "settings/user/ssh_unknown_origin_audit"
    else
      render_404
    end
  end

  def user_repositories
    render "settings/user/repositories"
  end


  def user_deleted_repositories
    unless GitHub.enterprise?
      render_template_view "settings/user/deleted_repositories",
      Settings::DeletedRepositoriesView,
        { target: current_user,
          deleted_repos: Archived::Repository.owned_by(current_user).network_safe_restoreable.order(updated_at: :desc).limit(100),
        }
    else
      return render_404
    end
  end

  def user_organizations
    render_template_view "settings/user/orgs",
      Settings::OrgsView
  end

  def user_enterprises
    return render_404 if GitHub.single_business_environment?
    render_template_view "settings/user/enterprises",
      Settings::EnterprisesView
  end

  ConfirmedTokensQuery = parse_query <<-'GRAPHQL'
    query($providers: [String!]!) {
      viewer {
        delegatedRecoveryTokens(first: 100, providers: $providers) {
          ...Views::Settings::User::Security::DelegatedAccountRecoveryRow::Tokens
        }
      }
    }
  GRAPHQL

  def user_security
    recovery_providers = Darrrr.recovery_providers
    data = platform_execute(ConfirmedTokensQuery,  variables: {
      providers: recovery_providers,
    })

    if GitHub.github_as_recovery_provider_enabled?
      # because of PJAX, we need to set this here
      SecureHeaders.append_content_security_policy_directives(request, form_action: recovery_providers)
    end

    render "settings/user/security", locals: { tokens_connection: data.viewer.delegated_recovery_tokens }
  end

  def user_security_analysis
    render "settings/user/security_analysis"
  end

  def user_blocks
    render "settings/user_blocks/index", locals: { login: params[:block_user] }
  end

  # Renaming runs in the background. Once it begins, we display a spinner
  # saying "Your rename is in progress..." then poll this URL to see
  # if the rename has completed or not. Both orgs and users use this route.
  #
  # When a rename has succeeded it will return an HTML partial to replace
  # the poll-include-fragment element that was polling this endpoint.
  def renaming
    if organization_id = params[:org]
      org = Organization.find(organization_id)
      return render_404 unless org.adminable_by?(current_user)
      if org.renaming?
        head 202
      else
        respond_to do |format|
          format.html do
            render partial: "organizations/rename_complete", locals: { organization: org }
          end
        end
      end
    else
      if current_user.renaming?
        head 202
      else
        respond_to do |format|
          format.html do
            render partial: "account/rename_complete"
          end
        end
      end
    end
  end

  # Setup Step 1: Starting the 2FA setup flow.
  def two_factor_authentication_intro
    GitHub.dogstats.increment(
      "two_factor.setup",
      tags: ["action:intro"],
    )
    # Let's remember where the user came from in case the 2FA setup flow was enabled when attempting to accept an org invite
    if Rails.application.routes.recognize_path(request.referrer)[:controller] == "orgs/invitations"
      session[:return_to] = request.referrer
    end
    render "settings/user/two_factor_steps/intro"
  end

  # Setup Step 2: Starting the 2FA setup flow and redirect user to download recovery codes.
  def two_factor_authentication_initiate
    case params[:type]
    when "sms"
      two_factor_type = :sms
      return render_404 unless GitHub.two_factor_sms_enabled?
    when "app"
      two_factor_type = :app
    else
      flash[:error] = "Invalid two-factor type. Please attempt setup again."
      return redirect_to settings_user_two_factor_authentication_intro_path
    end
    GitHub.dogstats.increment(
      "two_factor.setup",
      tags: [
        "action:initiate",
        "two_factor_type:#{two_factor_type}",
      ],
    )
    two_factor_setup_flow.new_setup(two_factor_type)
    redirect_to settings_user_two_factor_authentication_recovery_codes_path
  end

  # Setup Step 3: Get user to download their recovery codes.
  def two_factor_authentication_recovery_codes
    GitHub.dogstats.increment(
      "two_factor.setup",
      tags: ["action:recovery_codes"],
    )
    render "settings/user/two_factor_steps/recovery_codes"
  end

  # Setup Step 4: Setup two-factor with app or sms by confirming TOTP code.
  def two_factor_authentication_verify
    GitHub.dogstats.increment(
      "two_factor.setup",
      tags: ["action:verify"],
    )
    case two_factor_setup_flow.type
    when "sms"
      render "settings/user/two_factor_steps/sms"
    when "app"
      render "settings/user/two_factor_steps/app"
    else
      flash[:error] = "Invalid two-factor type. Please attempt setup again."
      redirect_to settings_user_two_factor_authentication_intro_path
    end
  end

  # Setup Step 4.5: Send the user a confirmation SMS during setup.
  def send_two_factor_sms
    GitHub.dogstats.increment(
      "two_factor.setup",
      tags: ["action:send_sms"],
    )

    message = tmp_two_factor_credential.totp.now +
      " is your #{GitHub.flavor} authentication setup code."

    number  = GitHub::SMS.normalize_number(params[:number])
    # We will save the number provided before they enter the validation code to
    # ensure we save the number to the DB that we actually sent the code to.
    two_factor_setup_flow.save_sms_number(number)
    # If we have already been through this flow once, the provider will be set
    # in two_factor_setup_flow and this is likely a "retry", so we should use
    # the alternate SMS provider. If the provider is `nil`, then this is the
    # first time through this flow and we should use the primary SMS provider.
    provider = if GitHub::SMS.number_from_india?(number)
      GitHub::SMS.provider_for_india
    elsif two_factor_setup_flow.provider
      GitHub::SMS.get_alternate_provider(two_factor_setup_flow.provider)
    else
      GitHub::SMS.get_provider
    end
    two_factor_setup_flow.save_provider(provider)

    GitHub::SMS.send_message(number, message, provider: provider)
    GitHub.dogstats.increment("two_factor.setup.sms.send", tags: ["result:success"])
    head :ok
  rescue GitHub::SMS::Error => e
    GitHub.dogstats.increment("two_factor.setup.sms.send", tags: ["result:failure"])
    render_sms_error 422, e.message
  end

  # Setup Step 5: Verifies code received via sms or app and starts enforcing two-factor authentication.
  def two_factor_authentication_enable
    GitHub.dogstats.increment(
      "two_factor.setup",
      tags: [
        "action:enable",
        "two_factor_type:#{two_factor_setup_flow.type}",
      ],
    )
    if tmp_two_factor_credential.verify_totp(params[:otp], allow_reuse: true)
      if two_factor_setup_flow.enable_two_factor
        flash[:notice] = "Two-factor authentication successfully enabled!"
        return_to = session.delete(:return_to) || settings_user_two_factor_authentication_configuration_path
        safe_redirect_to return_to
      else
        flash[:error] = "Two-factor setup failed. Please attempt setup again."
        redirect_to settings_user_two_factor_authentication_intro_path
      end
    else
      flash[:error] = "Two-factor code verification failed. Please try again."
      redirect_to settings_user_two_factor_authentication_verify_path
    end
  end

  def recovery_print
    codes = tmp_two_factor_credential.formatted_recovery_codes
    GitHub.dogstats.increment("two_factor.recovery_codes", tags: ["access_type:printed"])
    current_user.instrument_two_factor_recovery_codes_printed
    render "settings/auth_recovery_codes/print", layout: "popup", locals: { codes: codes }
  end

  def recovery_download
    # Windows notepad likes extra fancy newlines https://github.com/github/github/issues/29828
    codes = tmp_two_factor_credential.formatted_recovery_codes
    GitHub.dogstats.increment("two_factor.recovery_codes", tags: ["access_type:downloaded"])
    current_user.instrument_two_factor_recovery_codes_downloaded
    send_data(
      codes.join("\r\n"),
      filename: "github-recovery-codes.txt",
    )
  end

  # Disable the user's two_factor_credential
  def two_factor_authentication_disable
    if current_user.two_factor_auth_can_be_disabled?
      current_user.two_factor_credential.destroy
      AccountMailer.two_factor_disable(current_user).deliver_later
      GitHub.dogstats.increment("two_factor.disable")
      flash[:notice] = "Two-factor authentication successfully disabled."
      redirect_to settings_user_security_path
    else
      redirect_to settings_user_two_factor_authentication_configuration_path
    end
  end

  # Main 2FA config page.
  def two_factor_authentication
    @hide_two_factor_recover_code_warning = true
    two_factor_orgs = current_user.affiliated_organizations_with_two_factor_requirement

    render "settings/user/manage_two_factor",
      locals: {
        orgs_with_two_factor_requirement_count: two_factor_orgs.count,
      }
  end

  # Set a fallback sms number for two-factor authentication.
  #
  # Params:
  #  * countrycode - required
  #  * number      - required string fallback phone number
  #  * otp         - optional verification code sent to `number`
  #
  # Returns:
  #  * 201 - verified otp, set number as fallback
  #  * 202 - successfully sent otp to number, number not persisted
  #  * 422 - failed to send otp to number, or failed verification
  #
  def add_two_factor_sms_backup
    number = "#{params[:countrycode]} #{params[:number]}"
    number = GitHub::SMS.normalize_number(number)
    if params[:otp].present?
      valid, err = validate_two_factor_sms_backup_confirmation(
        number, params[:otp]
      )
      if valid
        provider = two_factor_sms_backup_confirmation_data[:provider]
        current_user.two_factor_credential.configure_sms_fallback(number, provider)
        flash[:notice] = "Added SMS fallback number."
        head 201
      else
        render status: 422, plain: err
      end
    else
      send_two_factor_sms_backup_setup_otp(number)
      head 202
    end
  rescue GitHub::SMS::Error => e
    render_sms_error 422, e.message
  end

  def validate_two_factor_sms_backup_confirmation(number, otp)
    data = two_factor_sms_backup_confirmation_data
    valid_number = data[:number].present? && data[:number] == number
    valid_otp = data[:otp].present? &&
      SecurityUtils.secure_compare(data[:otp], otp)

    if valid_number && valid_otp
      [true, nil]
    elsif valid_number
      [false, "Verification failed. Please re-enter the OTP again."]
    else
      [false, "Verification failed. Please add your number again."]
    end
  end

  def send_two_factor_sms_backup_setup_otp(number)
    # Check to see if we have existing data from a recent prior attempt to setup
    # a backup SMS number.
    data = two_factor_sms_backup_confirmation_data

    provider = GitHub::SMS.select_provider(number, primary_provider: current_user.two_factor_credential&.provider, last_provider: data[:provider])

    otp = 6.times.map { SecureRandom.random_number(10) }.join

    GitHub.kv.set(
      add_two_factor_sms_backup_key,
      { number: number, otp: otp, provider: provider }.to_json,
      expires: 10.minutes.from_now,
    )

    message = otp +
        " is your #{GitHub.flavor} SMS fallback setup code."
    GitHub::SMS.send_message(number, message, { provider: provider })
    GitHub.dogstats.increment("two_factor.setup_fallback_sms")
  end

  def two_factor_sms_backup_confirmation_data
    data = GitHub.kv.get(add_two_factor_sms_backup_key).value { "{}" }
    # The KV block value is only returned in case of an error. If the key is
    # simply expired that isn't considered an error and will return `nil`.
    data ||= "{}"
    data = JSON.parse(data).symbolize_keys
  end

  def add_two_factor_sms_backup_key
    "AddTwoFactorSMSBackupOTP:#{current_user.two_factor_credential.id}"
  end

  def destroy_two_factor_sms_backup
    current_user.two_factor_credential.remove_sms_fallback
    GitHub.dogstats.increment("two_factor.backup_number", tags: ["action:destroy"])
    flash[:notice] = "Removed SMS fallback number."
    redirect_to :back
  end

  def security_checkup
    case params[:type]
    when "updated"
      current_user.instrument_security_checkup(params[:type])
      current_user.security_checkup_completed(params[:type])
      safe_redirect_to settings_user_security_path
    when "confirmed"
      current_user.instrument_security_checkup(params[:type])
      current_user.security_checkup_completed(params[:type])
      safe_redirect_to params[:return_to]
    when "postponed"
      current_user.instrument_security_checkup(params[:type])
      current_user.security_checkup_postponed
      safe_redirect_to params[:return_to]
    else
      safe_redirect_to params[:return_to]
    end
  end

  def toggle_show_blocked_repo_contributors
    InteractionSetting.toggle_show_blocked_repo_contributors(current_user)
    redirect_to settings_user_blocks_path(@user)
  end

  def dotcom_user
    SecureHeaders.append_content_security_policy_directives(request, { form_action: ["#{GitHub.dotcom_host_protocol}://#{GitHub.dotcom_host_name}"], preserve_schemes: GitHub.dotcom_host_protocol != "https" })
    render "settings/user/dotcom_user"
  end

  # setting up 2FA does not require us to check for org
  # 2FA enabled
  def two_factor_enforceable
    if TWO_FACTOR_CAP_OPT_OUT_ACTIONS.include?(action_name)
      :no
    else
      :yes
    end
  end

  private

  def two_factor_setup_flow
    @two_factor_setup_flow ||= TwoFactorSetupFlow.new(current_user)
  end

  def tmp_two_factor_credential
    two_factor_setup_flow.tmp_two_factor_credential
  end
  helper_method :tmp_two_factor_credential

  def ensure_two_factor_enabled
    unless GitHub.auth.two_factor_authentication_enabled?
      render_404
    end
  end

  def ensure_user_has_two_factor_enabled
    unless current_user.two_factor_authentication_enabled?
      if request.xhr?
        head 422
      else
        redirect_to settings_user_two_factor_authentication_intro_path
      end
    end
  end

  def ensure_user_has_pending_two_factor_setup
    unless two_factor_setup_flow.pending?
      if request.xhr?
        render status: 422,
          plain: "No two-factor setup found. Please cancel and attempt setup again."
      else
        flash[:error] = "No two-factor setup found. Please attempt setup again."
        redirect_to settings_user_two_factor_authentication_intro_path
      end
    end
  end

  def settings_rate_limit_key
    "2fa-sms:#{current_user.id}"
  end

  def settings_rate_limit_render
    message = "You have exceeded our SMS rate limit. You will not be able to send another SMS for the next hour."
    render status: 422, plain: message
  end

  def render_sms_error(status, text)
    message = "We tried delivering an SMS to that number, but #{text}."
    render status: status, plain: message
  end

  def ensure_user_can_connect_to_dotcom
    render_404 if !GitHub.dotcom_user_connection_enabled? || GitHub.fedramp_private_instance? ||
                  (!GitHub::Connect.unified_contributions_enabled? && !GitHub::Connect.unified_private_search_enabled?)
  end

  def ensure_billing_enabled
    return render_404 unless GitHub.billing_enabled?
  end

  def ensure_gravatar_enabled
    return render_404 unless GitHub.gravatar_enabled?
  end
end
