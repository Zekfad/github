# frozen_string_literal: true

class CodeScanningBetaMembershipsController < ApplicationController
  include AccountMembershipHelper

  before_action :dotcom_required
  before_action :login_required

  FEATURE_SLUG = "code_scanning"

  def signup
    # Scope signup for current user and adminable orgs only
    accounts = adminable_accounts_without_businesses

    render_template_view(
      "code_scanning_beta_memberships/signup",
      CodeScanningBetaMemberships::View,
      membership: PrereleaseProgramMember.new,
      adminable_accounts: accounts,
      selected_account: accounts.include?(account) ? account : accounts.first,
      survey: survey,
    )
  end

  def agree
    if current_user.should_verify_email?
      flash[:error] = "Signing up for the code scanning and secret scanning betas requires a verified email address."
      render_email_verification_required
      return
    end

    valid_account_ids = adminable_accounts_without_businesses.map(&:id).map(&:to_s)
    unless valid_account_ids.include?(params[:user_id])
      flash[:error] = "Invalid account specified."
      redirect_to code_scanning_beta_signup_path(account: account)
      return
    end

    membership = create_membership
    pre_release_member = create_pre_release_member

    if membership.persisted? && pre_release_member.persisted?
      GitHub.dogstats.increment("code_scanning.joined_waitlist")

      GlobalInstrumenter.instrument("user.beta_feature.enroll",
        actor: current_user,
        action: "enroll",
        feature: FEATURE_SLUG,
        organization: membership.member.organization? ? membership.member : nil,
      )

      # Eloqua integration
      private_repositories = false
      languages = []
      static_analysis = ""

      answers = survey.answers_for(membership.member)
      answers.each do |answer|
        if answer.question.short_text == "static_analysis"
          static_analysis = answer.other_text
        elsif answer.question.short_text == "programming_languages"
          languages.push(answer.choice.text)
        elsif answer.question.short_text == "private_repositories"
          if answer.choice.text == "Yes"
            private_repositories = true
          end
        end
      end

      if private_repositories
        org = membership.member.organization? ? membership.member : nil
        eloqua_data = {
          company_name: org&.company_name,
          organization_id: org&.id,
          organization_login: org&.login,
          full_name: current_user.safe_profile_name,
          email: current_user.email,
          user_id: current_user.id,
          user_agent: user_session&.user_agent,
          remote_ip_address: user_session&.ip,
          state: user_session&.location&.dig(:region),
          city: user_session&.location&.dig(:city),
          country: user_session&.location&.dig(:country_code),
          postal_code: user_session&.location&.dig(:postal_code),
          marketing_email_opt_in: NewsletterPreference.marketing_preference(user: current_user),
          agreed_to_terms: true,
          billing_email: org&.billing_email,
          event_type: "advanced_security",
          total_billable_seats: org&.seats,
          advanced_security_private_repos: private_repositories,
          static_tools: static_analysis,
          languages: languages.join(", "),
          github_username: current_user.login,
          plan_type: membership.member.plan.name
        }.merge(
          session.fetch(:utm_memo, Hash.new).slice("utm_medium", "utm_source", "utm_campaign"),
        )

        EnterpriseCloudTrialMarketingNotificationJob.perform_later(eloqua_data)
      end

      if params[:redirect_back]
        safe_redirect_to params[:redirect_back]
      else
        redirect_to code_scanning_beta_thanks_path(account: account)
      end
    else
      field_errors = membership.errors.full_messages + pre_release_member.errors.full_messages
      flash[:error] = "Sorry that didn’t work. #{field_errors.to_sentence}."
      redirect_to code_scanning_beta_signup_path(account: account)
    end
  end

  def thanks
    membership = EarlyAccessMembership.code_scanning_waitlist.find_by(member: account) if account

    if membership.present?
      render_template_view(
        "code_scanning_beta_memberships/thanks",
        CodeScanningBetaMemberships::View,
        membership: membership,
        adminable_accounts: adminable_accounts,
      )
    else
      redirect_to code_scanning_beta_signup_path(account: account)
    end
  end

  private

  def create_membership
    membership = EarlyAccessMembership.new(
      member_id: params[:user_id],
      actor_id: current_user.id,
      feature_slug: FEATURE_SLUG,
      survey: survey,
    )

    membership.transaction do
      survey_answers.each do |answer|
        SurveyAnswer.create!(
          user_id: params[:user_id],
          survey_id: survey.id,
          question_id: answer[:question_id],
          choice_id: answer[:choice_id],
          other_text: answer[:other_text],
        )
      end

      membership.save
    end

    membership
  end

  def create_pre_release_member
    attrs = { member_id:  params[:user_id], actor_id: current_user.id }
    PrereleaseProgramMember.find_by(attrs) || PrereleaseProgramMember.create(attrs)
  end

  def survey
    @survey ||= Survey.preload(questions: :choices).find_by(slug: FEATURE_SLUG)
  end

  def survey_answers
    @survey_answers ||= Array(params[:answers]).select do |answer|
      next false unless answer[:choice_id].present?

      # Ignore answers of empty text
      next false if answer.key?(:other_text) && answer[:other_text].blank?

      # Ignore invalid answers/choices
      survey.questions.any? do |question|
        question.id.to_s == answer[:question_id] &&
          question.choices.any? { |choice| choice.id.to_s == answer[:choice_id] }
      end
    end
  end
end
