# frozen_string_literal: true

class IssueCommentsController < AbstractRepositoryController

  statsd_tag_actions only: :create

  include ShowPartial
  include ProgressiveTimeline
  include PrecacheIssues
  include TimelineHelper
  include CommentsHelper

  before_action :login_required
  before_action :require_issue_comment, only: [:update, :destroy]
  before_action :require_logged_in_and_not_blocked, only: [:create, :update]
  before_action :writable_repository_required, except: [:destroy]
  before_action :non_migrating_repository_required, only: [:destroy]
  before_action :content_authorization_required, only: [:create, :update]

  IssueActivityQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $timelineSince: DateTime,
      $timelinePageSize: Int,
      $syntaxHighlightingEnabled: Boolean = false,
      $deferCollapsedThreads: Boolean = false,
      $focusedReviewThread: ID,
      $focusedReviewComment: ID,
      $hasFocusedReviewThread: Boolean = false
      $hasFocusedReviewComment: Boolean = false
      $scopedItemTypes: [IssueTimelineItemsItemType!]
  ) {
      node(id: $id) {
        ...Views::Issues::Timeline::Issue
        ...Views::Issues::FormActions::FormActionFragment
      }
    }
  GRAPHQL

  StateButtonChecksQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Issues::StateButtonWrapper::StateButtonFragment
      }
    }
  GRAPHQL

  def create
    if issue = current_issue
      GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

      valid = true
      comment_body = create_params_comment_body

      if blocked_from_commenting?(issue)
        if request.xhr?
          head :unprocessable_entity
        else
          redirect_to :back
        end
      end

      if !comment_body.nil? && !can_skip_creating_comment?
        @comment = issue.create_comment(current_user, comment_body)
        valid &&= @comment.persisted?
      elsif params[:comment_and_close] == "1"
        @comment = issue.comment_and_close(current_user, comment_body)
        valid &= @comment if comment_body.present?

        GitHub.dogstats.increment("issue.closed", tags: ["with_comment:#{comment_body.present?}"])
      elsif params[:comment_and_open] == "1"
        @comment = issue.comment_and_open(current_user, comment_body)
        valid &= @comment if comment_body.present?
      end

      mark_thread_as_read issue

      if valid && comment_body.present?
        GitHub.instrument "comment.create", user: current_user
        instrument_saved_reply_use(params[:saved_reply_id], "issue_comment")
      end

      respond_to do |format|
        format.html do
          if valid
            anchor = (@comment && @comment.valid?) ? "#issuecomment-#{@comment.id}" : ""
            redirect_to issue_path(issue.repository.owner, issue.repository, issue) + anchor
          else
            if @comment
              flash[:error] = @comment.errors.full_messages.to_sentence
            end

            redirect_to :back
          end
        end

        format.json do
          if valid

            query = if params[:context] == "project_sidebar"
              StateButtonChecksQuery
            else
              IssueActivityQuery
            end

            scope_timeline = scoped_issue_timeline_enabled? && (params[:scope_timeline] == "1")
            issue_node = timeline_owner_response_for(query: query, id: issue.global_relay_id, scope_timeline: scope_timeline)&.node

            partials = after_create_partials(issue, issue_node)
            render_immediate_partials(issue, partials: partials)
          else
            if @comment
              if GitHub.rails_6_0?
                errors = @comment.errors.map { |attr, msg| msg }
              else
                errors = @comment.errors.map { |error| error.message }
              end
            else
              errors = ["Sorry, but we couldn't do that"]
            end
            render json: { errors: errors }, status: :unprocessable_entity
          end
        end
      end
    else
      return render_404
    end
  end

  def update
    comment = current_comment
    comment_params = issue_comment_params

    if can_modify_issue_comment?(comment) && comment_params.present?
      # prevent updates from stale data
      return render_stale_error(model: comment, error: "Could not edit comment. Please try again", path: issue_path(comment.issue)) if stale_model?(comment)

      mark_thread_as_read comment.issue

      if operation = TaskListOperation.from(params[:task_list_operation])
        text = operation.call(comment.body)
        comment.update_body(text, current_user) if text
      else
        if comment.update_body(comment_params[:body], current_user)
          duplicate_issue_events = comment.issue.
            build_duplicate_issue_events(comment.user, comment.duplicate_issues)
          duplicate_issue_events.each(&:save)
        end
      end
    end

    respond_to do |wants|
      wants.html do
        redirect_to issue_path(comment.issue)
      end

      wants.json do
        if comment.valid?
          render json: {
            "source" => comment.body,
            "body" => comment.body_html,
            "newBodyVersion" => comment.body_version,
            "editUrl" => show_comment_edit_history_path(comment.global_relay_id),
          }
        else
          render json: { errors: comment.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  end

  def destroy
    if site_admin? || can_modify_issue_comment?(current_comment)
      current_comment.destroy
    elsif current_repository.archived?
      return respond_to do |format|
        format.json do
          render json: { error: "repository archived" },
                 status: :forbidden
        end
        format.all do
          render "repositories/states/archived",
                 status: :forbidden
        end
      end
    elsif current_comment&.issue.locked?
      return respond_to do |format|
        # This should be an edge case, likely only happening if the issue is
        # locked and someone without permissions tries to delete a comment
        # without reloading the page
        format.json do
          render json: { error: "issue locked" },
                 status: :forbidden
        end
      end
    end

    if request.xhr?
      head :ok
    else
      redirect_to issue_path(current_comment.issue)
    end
  end

  def template_suggestions
    return render_404 unless GitHub.flipper[:structured_issue_comment_templates].enabled?(current_repository)

    templates = IssueCommentTemplates.new(current_repository)

    respond_to do |format|
      format.html do
        render partial: "comments/template_suggester", formats: :html, locals: { templates: templates }
      end
    end
  end

  private

  def create_params_comment_body
    # TODO: improve validation of form submission - https://github.com/github/project-convito/issues/43
    if current_repository.structured_issue_comment_templates_enabled? && create_comment_body_is_a_hash?
      structured_issue_comment_body
    else
      params[:comment][:body]
    end
  end

  def structured_issue_comment_body
    params[:comment][:body].permit!.to_h.map do |id, response|
      next if id.start_with?("label.")

      label = params[:comment][:body]["label.#{id}"] || id
      "### #{label}\n\n#{response}"
    end.compact.join("\n\n")
  end

  def create_comment_body_is_a_hash?
    params.to_unsafe_h.with_indifferent_access[:comment][:body].is_a?(Hash)
  end

  def issue_comment_params
    params.to_unsafe_h.with_indifferent_access[:issue_comment].slice :body
  end

  def can_skip_creating_comment?
    params[:comment_and_close].present? ||
      params[:comment_and_open].present?
  end

  def can_modify_issue_comment?(comment)
    return false unless logged_in?

    authorization = ContentAuthorizer.authorize(current_user, :issue_comment, :delete, repo: current_repository, issue: comment.issue)
    comment_author = comment.user_id == current_user.id
    can_push = current_repository.owner_id == current_user.id || current_user_can_push?

    # The issue/repo are liberated! Users can delete comments if:
    # - they're the comment author
    # - they have push access to the repo
    return can_push || comment_author if authorization.authorized?

    # Either the issue or repo are in an invalid state for modifying comments
    # We forbid adding modifying comments in all scenarios *except issue lock*
    # Users with push access can modify comments on locked issues
    auth_errors = authorization.errors(fail_fast: true).map(&:symbolic_error_code)
    if auth_errors.include?(:locked) || auth_errors.include?(:archived)
      can_push
    else
      false
    end
  end

  def current_comment
    return @current_comment if defined?(@current_comment)

    comment = current_repository.issue_comments.find(params[:id])
    @current_comment = comment.readable_by?(current_user) ? comment : nil
  end

  def require_issue_comment
    if current_comment.nil?
      render_404
    end
  end

  def require_logged_in_and_not_blocked
    if blocked_from_commenting?(current_issue)
      flash[:error] = "You can't perform that action at this time."
      redirect_to current_repository.permalink
    end
  end

  def current_issue
    @current_issue ||= if action_name == "create"
      current_repository.issues.find_by_number(params[:issue].to_i)
    else
      current_comment.issue
    end
  end
  alias :timeline_owner :current_issue

  def content_authorization_required
    authorize_content(:issue_comment, issue: current_issue)
  end

  def route_supports_advisory_workspaces?
    return true if action_name == "update" # allow updating comment body
    false
  end

  def after_create_partials(issue, issue_node)
    case params[:context]
    when "project_sidebar"
      {
        state_button_wrapper: {
          issue: current_issue,
          issue_node: issue_node,
          addl_btn_classes: ["width-full mt-2"],
        },
      }
    else
      discussion_categories = if discussions_enabled? && discussion_categories_enabled?
        current_repository.discussion_categories.sort
      else
        []
      end

      scope_timeline = scoped_issue_timeline_enabled? && (params[:scope_timeline] == "1")
      {
        timeline: {
          issue: issue_node,
          scope_timeline: scope_timeline,
        },
        sidebar: {
          issue_node: issue_node,
          discussion_categories: discussion_categories,
        },
        form_actions: {
          issue: current_issue,
          issue_node: issue_node,
          scope_timeline: scope_timeline,
        },
      }
    end
  end
end
