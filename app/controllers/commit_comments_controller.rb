# frozen_string_literal: true

class CommitCommentsController < GitContentController
  include ShowPartial
  include CommitCommentsHelper
  include DiscussionPreloadHelper

  def create
    return unless request.post?

    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

    current_commit = current_repository.commits.find(params[:commit_id])
    attributes    = {
      user: current_user,
      repository: current_repository,
      commit_id: params[:commit_id],
      path: params[:path],
      position: params[:position],
      line: params[:line],
      body: params[:comment].nil? ? nil : params[:comment][:body],
    }

    comment = CommitComment.new(attributes)
    if comment.save
      mark_thread_as_read current_commit
      GitHub.instrument "comment.create", user: current_user
      preload_discussion_group_data([comment])

      if request.xhr?
        if comment.inline?
          json = {}
          # If this is a "reply", we can just render a single comment
          if params[:in_reply_to].present?
            json[:inline_comment] = render_to_string(
              partial: "comments/discussion_or_commit_comment",
              formats: [:html],
              locals: {
                dom_id: "commitcomment-#{comment.id}",
                comment: preloaded_platform_object(comment),
                comment_context: params[:comment_context],
              })
          # Otherwise, we need to render the start of the thread
          else
            json[:inline_comment] = render_to_string(
              partial: "diff/commit_thread",
              formats: [:html],
              object: CommitCommentThread.group_comments([comment]).first,
            )
          end

          respond_to do |format|
            format.json do
              render json: json
            end
          end
        else
          respond_to do |format|
            format.json do
              comments = CommitComment.for_display(current_user, current_commit, current_repository)
              current_commit.comments = [comment]
              current_commit.comment_count = comments.discussion.count
              render_immediate_partials(current_commit, partials: { timeline_marker: {}, visible_comments_header: {} })
            end
          end
        end
      else
        hash = "#commitcomment-#{comment.id}"
        redirect_to commit_path(params[:commit_id]) + hash
      end
    else
      if request.xhr?
        head :unprocessable_entity
      else
        flash[:error] = comment.errors.full_messages.to_sentence
        redirect_to commit_path(params[:commit_id])
      end
    end
  end

  def update
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

    comment = current_comment
    if can_modify_commit_comment?(comment)
      # prevent updates from stale data
      if stale_model?(comment)
        return render_stale_error(model: comment, error: "Could not edit comment. Please try again.", path: redirect_path(comment))
      end

      if operation = TaskListOperation.from(params[:task_list_operation])
        text = operation.call(comment.body)
        comment.update_body(text, current_user) if text
      else
        comment.update_body(params[:commit_comment][:body], current_user)
      end

      respond_to do |format|
        format.json do
          if comment.valid?
            render json: {
              "source" => comment.body,
              "body" => comment.body_html,
              "newBodyVersion" => comment.body_version,
              "editUrl" => show_comment_edit_history_path(comment.global_relay_id),
            }
          else
            render json: comment.errors, status: :unprocessable_entity
          end
        end

        format.html do
          unless comment.valid?
            flash[:error] = "Could not edit comment."
          end

          redirect_to redirect_path(comment)
        end
      end
    else
      access_denied
    end
  end

  def destroy
    comment = current_comment

    if site_admin? || (logged_in? && (
        comment.user_id == current_user.id ||
        comment.repository.pushable_by?(current_user)))
      comment.destroy
      respond_to do |format|
        format.html do
          redirect_to redirect_path(comment, false)
        end
        format.json do
          head 200
        end
      end
    else
      access_denied
    end
  end

private

  def authorized?
    params[:action] == "create" ? (logged_in? && super) : super
  end

  def current_comment
    current_repository.commit_comments.find(params[:id])
  end

  def redirect_path(comment, deep_link = true)
      if params[:pull_request_number]
        "#{pull_request_path(params[:pull_request_number].to_i, comment.commit.repository)}#{deep_link ? "#discussion_r#{comment.id}" : ""}"
      else
        commit_path(comment.commit)
      end
  end

  def route_supports_advisory_workspaces?
    true
  end
end
