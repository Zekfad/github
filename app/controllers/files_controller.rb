# frozen_string_literal: true

# FilesController is all about browsing files on GitHub. This controller is
# used for both private and public content, so there is a mixture of
# authenticated requests and non-authenticated requests.
class FilesController < GitContentController
  areas_of_responsibility :repositories, :raw
  map_to_service :tree

  CONDITIONAL_ACCESS_BYPASS_PUBLIC_REPO_ACTIONS = %w(disambiguate file_list).freeze

  # As of 2019-04-16, disambiguate is averaging around 580 rq/sec, or around 3%
  # of total requests
  set_statsd_sample_rate 0.01, only: :disambiguate

  statsd_tag_actions only: [:disambiguate, :file_list, :overview]

  include TreeHelper
  layout :repository_layout

  rescue_from_timeout only: [:file_list] do |boom|
    # Since file_list is alway an xhr request, just send back a head 504 for
    # debugging purposes.
    head :gateway_timeout
  end

  before_action :raw_redirect, only: [:disambiguate]
  before_action :add_spamurai_form_signals, only: [:disambiguate]
  before_action :redirect_for_missing_branch, only: [:disambiguate]
  before_action :ensure_commit_sha, only: [:disambiguate]

  param_encoding :file_list, :name, "ASCII-8BIT"

  # Used for deferred content loading of the file list in a directory.
  def file_list
    return render_404 unless commit_sha

    current_directory.simplify_paths
    current_directory.tree_history.calculate_all_entries

    if current_directory.history_cached?
      commits = current_directory.map { |row| row[:commit] }.reject { |c| c.is_a?(Directory::NilCommit) }
      Commit.prefill_short_messages(commits)
    end

    view = Files::DirectoryPartialView.new(current_user, current_repository, commit_sha, current_branch_or_tag_name, path_string, current_directory)

    respond_to do |format|
      format.html do
        render partial: "files/file_list", locals: { view: view }
      end
    end
  end

  param_encoding :disambiguate, :name, "ASCII-8BIT"

  # This is the catchall for all repository file listing pages
  #
  # The url structure for these pages makes it impossible to check
  # where the ref-name stops and the path starts. We have to look
  # at the refs on the repository. See GitHub::RefShaPathExtractor
  # for how we do this.
  #
  # This is due to the possibility of there being foward-slashes in
  # ref names. Given the url /user/repo/tree/foo/bar/bang
  # That could be:
  #
  # ref: foo, path: bar/bang
  # ref: foo/bar, path: bang
  # ref: foo/bar/bang, path: [root]
  #
  # If the path ends up empty, we are at the root of the repo and
  # should show the overview page. Otherwise we are in a subdirectory.
  #
  # params[:path] and params[:name] are set in:
  # AbstractRepositoryController#set_path_and_name
  def disambiguate
    current_directory.simplify_paths

    if !current_path.blank? || params[:files] == "1"
      subdirectory
    else
      overview
    end
  rescue GitRPC::InvalidObject => boom
    if boom.message =~ /expected tree, got blob/
      blob_redirect boom
    else
      raise
    end
  end

  def subdirectory
    template_directory = current_path.for_display == IssueTemplates.template_directory

    respond_to do |format|
      format.html do
        render "files/subdirectory", locals: { template_directory: template_directory }
      end
    end
  end

  OverviewQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Files::OverviewPageTitle::Repository
        ...Views::Topics::List::Repository
        ...Files::OverviewView::RepositoryFragment
        ...Views::Community::OrgInteractionLimitsBanner::Node
      }
    }
  GRAPHQL

  def overview
    if ref.blank? || ref == current_repository.default_branch
      # Override the default behavior of `analytics_location_meta_tag` where the
      # reported URL would end with "/files/disambiguate" which would make it
      # impossible to distinguish whether someone was visiting the project home
      # page vs. drilling down into subdirectories.
      override_analytics_location "/<user-name>/<repo-name>"
    end

    respond_to do |format|
      format.html do
        repository_data = platform_execute(OverviewQuery, variables: { "id" => current_repository.global_relay_id })

        unless repository_data.node
          return render_404
        end

        page_title = Files::OverviewPageTitle.new(
          ref: ref,
          repository: repository_data.node,
          logged_in: logged_in?,
        )

        render_template_view("files/overview", Files::OverviewView, {
          user: current_user,
          repository: current_repository,
          commit: current_commit,
          community_contributors_enabled: community_contributors_enabled?,
          repository_data: repository_data.node,
          page_title: page_title,
          source_url: request.url,
          location: controller_name + "#" + action_name,
        }, locals: { commit: current_commit, focus_description: params[:focus_description], repository_data: repository_data })
      end
    end
  end

  private

  def redirect_for_missing_branch
    # See `AbstractRepositoryController#set_path_and_name`
    return if params[:branch].present?

    # Feature flag check
    return unless missing_tree_ref_redirect_enabled?

    missing_branch_regex = %r{/tree/(?<branch>[^/]+)/}
    matches = request.path.match(missing_branch_regex)
    return unless matches && matches[:branch] == "master"

    GlobalInstrumenter.instrument "repository.missing_branch_redirect",
      repository: current_repository,
      actor: current_user,
      missing_branch_was_master: true

    redirect_path = request.path.sub(missing_branch_regex, "/tree/#{current_repository.default_branch}/")
    redirect_to redirect_path, notice: "Branch not found, redirected to default branch."
  end

  def blob_redirect(exception = nil)
    s_tree = escape_url_branch(tree_name)
    if exception.is_a?(GitRPC::InvalidObject)
      s_path = escape_url_paths(current_path)
      redirect_to "/#{current_repository.name_with_owner}/blob/#{s_tree}/#{s_path}",
      status: 301
    elsif current_path.size > 1
      s_path = escape_url_paths(current_path.parent)
      redirect_to "/#{current_repository.name_with_owner}/tree/#{s_tree}/#{s_path}",
      status: 301
    else
      render_404
    end
  end

  def raw_redirect
    if params[:raw]
      url = request.url
      if url.include?(".git") && !url.include?(".git/")
        redirect_to url.sub(".git", ".git/")
        true
      end
    end
  end

  def ensure_commit_sha
    render_404 if commit_sha.blank? || current_commit.nil?
  end

  def ref
    params[:ref] || params[:name]
  end

  def route_supports_advisory_workspaces?
    true
  end

  # Bypass conditional access requirements for the specified actions if the
  # current action is exempt.
  def require_conditional_access_checks?
    return false if conditional_access_exempt?
    super
  end
end
