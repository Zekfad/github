# frozen_string_literal: true

class Devtools::ExceptionsController < DevtoolsController
  def index
    load_exceptions
    render "devtools/exceptions/index"
  end

  private

  def load_exceptions
    @exceptions ||=
      begin
        all = File.foreach(GitHub.failbot_log_path).each_with_index.map { |line, i|
          begin
            line = GitHub::Encoding.transcode(line, "UTF-8", "UTF-8")
            Yajl.load(line).tap do |report|
              report["created_at"] = Time.parse(report["created_at"])
            end
          rescue => e
            {
              "created_at" => Time.now,
              "app"        => "github",
              "class"      => e.class.name,
              "message"    => "Unable to parse #{GitHub.failbot_log_path}, line #{i + 1}: #{e.message}",
              "backtrace"  => "",
            }
          end
        }
        filtered = params["app"] ? all.select { |report| report["app"] == params["app"] } : all
        sorted = filtered.sort_by { |report| report["created_at"] }.reverse
      rescue Errno::ENOENT
        []
      end
  end

  # Special snowflake
  def dotcom_required
    return render_404 unless Rails.env.development?
  end
end
