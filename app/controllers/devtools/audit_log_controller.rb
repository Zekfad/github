# frozen_string_literal: true

class Devtools::AuditLogController < DevtoolsController
  areas_of_responsibility :stafftools, :audit_log
end
