# frozen_string_literal: true

class Devtools::SurveysController < DevtoolsController
  areas_of_responsibility :feature_lifecycle

  def index
    render "devtools/surveys/index", locals: {surveys: Survey.first(30)}
  end

  def show
    respond_to do |format|
      format.csv do
        @survey = Survey.find_by_slug(params["slug"])
        filename = "survey_#{@survey.slug}.csv"
        data = @survey.csv_dump

        send_data data, type: "text/csv",
                        disposition: "attachment;filename=#{filename}"
      end

      format.html do
        render "devtools/surveys/show", locals: {survey: Survey.find_by_slug(params["slug"])}
      end
    end
  end
end
