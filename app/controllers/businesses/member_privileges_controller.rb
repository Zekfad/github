# frozen_string_literal: true

class Businesses::MemberPrivilegesController < Businesses::BusinessController

  before_action :business_admin_required
  before_action :update_protected_branches_setting_flag_required, only: [:update_members_can_update_protected_branches]

  def index
    render_template_view "businesses/settings/member_privileges",
      Businesses::Settings::MemberPrivilegesView,
      business: this_business,
      params: params
  end

  def update_members_can_delete_issues
    setting_value = members_can_delete_issues_params[:members_can_delete_issues]
    validate_setting value: setting_value

    message = ""
    case setting_value
    when "enabled"
      this_business.allow_members_can_delete_issues(actor: current_user, force: true)
      message = "Members can now delete issues."
    when "disabled"
      this_business.disallow_members_can_delete_issues(actor: current_user, force: true)
      message = "Members can no longer delete issues."
    when "no_policy"
      this_business.clear_members_can_delete_issues(actor: current_user)
      message = "Organization administrators can now change this setting for individual organizations."
    end

    redirect_to :back, notice: message
  end

  def update_members_can_update_protected_branches
    setting_value = members_can_update_protected_branches_params[:members_can_update_protected_branches]
    validate_setting value: setting_value

    message = ""
    case setting_value
    when "enabled"
      this_business.allow_members_can_update_protected_branches(actor: current_user, force: true)
      message = "Members can now update protected branches."
    when "disabled"
      this_business.disallow_members_can_update_protected_branches(actor: current_user, force: true)
      message = "Members can no longer update protected branches."
    when "no_policy"
      this_business.clear_members_can_update_protected_branches(actor: current_user)
      message = "Organization administrators can now change this setting for individual organizations."
    end

    redirect_to :back, notice: message
  end

  def update_members_can_delete_repositories
    setting_value = members_can_delete_repos_params[:members_can_delete_repositories]
    validate_setting value: setting_value

    message = ""
    case setting_value
    when "enabled"
      this_business.allow_members_can_delete_repositories(actor: current_user, force: true)
      message = "Members can now delete or transfer repositories."
    when "disabled"
      this_business.disallow_members_can_delete_repositories(actor: current_user, force: true)
      message = "Members can no longer delete or transfer repositories."
    when "no_policy"
      this_business.clear_members_can_delete_repositories(actor: current_user)
      message = "Organization administrators can now change this setting for individual organizations."
    end

    redirect_to :back, notice: message
  end

  NO_POLICY_VALUE = "no_policy"
  DISABLED_VALUE = "disabled"
  ALLOWED_VALUE = "allowed"
  ALL_VALUES = [NO_POLICY_VALUE, DISABLED_VALUE, ALLOWED_VALUE].freeze

  def update_members_can_create_repositories
    members_can_create_repositories = members_can_create_repos_params[:members_can_create_repositories]
    validate_setting value: members_can_create_repositories, valid_values: ALL_VALUES

    enabled_visibilities = case members_can_create_repositories
    when NO_POLICY_VALUE
      this_business.clear_members_can_create_repositories(actor: current_user)
    when DISABLED_VALUE
      this_business.allow_members_can_create_repositories_with_visibilities(
        force: true,
        actor: current_user,
        public_visibility: false,
        private_visibility: false,
        internal_visibility: false,
      )
    when ALLOWED_VALUE
      this_business.allow_members_can_create_repositories_with_visibilities(
        force: true,
        actor: current_user,
        public_visibility: members_can_create_repos_params[:public] == "true",
        private_visibility: members_can_create_repos_params[:private] == "true",
        internal_visibility: members_can_create_repos_params[:internal] == "true",
      )
    end

    message = if enabled_visibilities.nil?
      "Organization administrators can now change this setting for individual organizations."
    elsif enabled_visibilities.present?
      "Members can now create #{to_sentence(enabled_visibilities)} repositories."
    else
      "Members can no longer create repositories."
    end

    redirect_to :back, notice: message
  end

  def update_default_branch_setting
    unless GitHub.flipper[:configurable_repo_default_branch].enabled?(this_business)
      return render_404
    end

    enforce = params[:default_branch_enforce] == "1"
    raw_name = params[:default_branch]
    normalized_name = Git::Ref.normalize(raw_name)

    if normalized_name.blank?
      flash[:error] = "Could not update default branch name preference, " \
        "'#{raw_name}' is not a valid branch name."
    elsif this_business.set_default_new_repo_branch(normalized_name, actor: current_user, enforce: enforce)
      flash[:notice] = "New repositories created in #{this_business} will use " \
        "#{normalized_name} as their default branch. Organizations will#{' not' if enforce} " \
        "be able to override this."
    else
      flash[:error] = "Could not set the default branch name preference for " \
        "#{this_business} at this time."
    end

    redirect_to settings_member_privileges_enterprise_path(this_business)
  end

  def update_members_can_invite_outside_collaborators
    setting_value = members_can_invite_outside_collaborators_params[:members_can_invite_outside_collaborators]
    validate_setting value: setting_value

    message = ""
    case setting_value
    when "enabled"
      this_business.allow_members_can_invite_outside_collaborators(actor: current_user, force: true)
      message = "Members can now invite outside collaborators."
    when "disabled"
      this_business.disallow_members_can_invite_outside_collaborators(actor: current_user, force: true)
      message = "Members can no longer invite outside collaborators."
    when "no_policy"
      this_business.clear_members_can_invite_outside_collaborators(actor: current_user)
      message = "Organization administrators can now change this setting for individual organizations."
    end

    redirect_to :back, notice: message
  end

  def update_members_can_change_repo_visibility
    setting_value = params[:members_can_change_repo_visibility]&.to_s
    validate_setting value: setting_value

    message = ""
    case setting_value
    when "enabled"
      this_business.allow_members_to_change_repo_visibility(actor: current_user, force: true)
      message = "Members can change repository visibilities and this is enforced for this enterprise."
    when "disabled"
      this_business.block_members_from_changing_repo_visibility(actor: current_user, force: true)
      message = "Members cannot change repository visibilities and this is enforced for this enterprise."
    when "no_policy"
      this_business.clear_members_can_change_repo_visibility_setting(actor: current_user)
      message = "Policy removed for repository visibility change setting."
    end

    redirect_to settings_member_privileges_enterprise_path(this_business), notice: message
  end

  def update_allow_private_repository_forking
    setting_value = params[:allow_private_repository_forking]&.to_s
    validate_setting value: setting_value

    message = ""
    case setting_value
    when "enabled"
      this_business.allow_private_repository_forking(force: true, actor: current_user)
      message = "Private and internal repository forks are enabled and enforced for this enterprise."
    when "disabled"
      this_business.block_private_repository_forking(actor: current_user)
      message = "Private and internal repository forks are disabled and enforced for this enterprise."
    when "no_policy"
      this_business.clear_private_repository_forking_setting(actor: current_user)
      message = "Private and internal repository forks policy removed."
    end

    redirect_to settings_member_privileges_enterprise_path(this_business), notice: message
  end

  def update_default_repository_permission
    permission = params[:default_repository_permission]&.to_s
    validate_setting value: permission, valid_values: %w(no_policy none read write admin)

    message = ""
    if "no_policy" == permission
      this_business.clear_default_repository_permission(actor: current_user)
      message = "The default repository permission policy is removed."
    else
      begin
        permission = permission.to_sym
        this_business.update_default_repository_permission permission, force: true, actor: current_user
        message = "The default repository permission is set to #{permission} and is enforced for this enterprise."
      rescue Configurable::DefaultRepositoryPermission::AlreadyUpdating
        flash[:error] = "The default repository permission is already being updated."
        return redirect_to settings_member_privileges_enterprise_path(this_business)
      end
    end

    redirect_to settings_member_privileges_enterprise_path(this_business), notice: message
  end

  def update_action_execution_capability
    capability = params[:action_execution_capability]&.to_s
    validate_setting(
      value: capability,
      valid_values: [
        "no_policy",
        Configurable::ActionExecutionCapabilities::DISABLED,
        Configurable::ActionExecutionCapabilities::ALL_ACTIONS,
        Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS,
      ],
    )

    message = ""
    case capability
    when Configurable::ActionExecutionCapabilities::DISABLED
      this_business.disable_all_action_executions(force: true, actor: current_user)
      message = "All Actions are now disabled for all repositories."
    when Configurable::ActionExecutionCapabilities::ALL_ACTIONS
      this_business.enable_all_action_executions(force: true, actor: current_user)
      message = "All Actions are now enabled for all repositories."
    when Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS
      this_business.enable_only_local_action_executions(force: true, actor: current_user)
      message = "Only Local/In-Repo Actions are now enabled for all repositories."
    when Configurable::ActionExecutionCapabilities::NO_POLICY
      this_business.clear_action_execution_capabilities(actor: current_user)
      message = "Organization administrators can now change this setting for individual organizations."
    end

    redirect_to settings_member_privileges_enterprise_path(this_business), notice: message
  end

  private

  def members_can_update_protected_branches_params
    @members_can_update_protected_branches_params ||= params.require(:business).permit(:members_can_update_protected_branches)
  end

  def members_can_delete_repos_params
    @members_can_delete_repos_params ||= params.require(:business).permit(:members_can_delete_repositories)
  end

  def members_can_delete_issues_params
    @members_can_delete_issues_params ||= params.require(:business).permit(:members_can_delete_issues)
  end

  def members_can_create_repos_params
    @members_can_create_repos_params ||= params.require(:business).permit(:members_can_create_repositories, :public, :private, :internal)
  end

  def members_can_invite_outside_collaborators_params
    @members_can_invite_outside_collaborators_params ||= params.require(:business).permit(:members_can_invite_outside_collaborators)
  end

  def update_protected_branches_setting_flag_required
    render_404 unless GitHub.update_protected_branches_setting_enabled? || GitHub.flipper[:update_protected_branches_setting].enabled?(this_business)
  end

  def to_sentence(arr)
    return arr.first if arr.length == 1

    string = arr[0..-2].join(", ")
    string += "," if arr.length > 2 # Oxford commas keep it classy
    string += " and "
    string += arr.last

    string
  end
end
