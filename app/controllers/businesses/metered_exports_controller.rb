# frozen_string_literal: true

class Businesses::MeteredExportsController < Businesses::BusinessController
  include BillingSettingsHelper

  areas_of_responsibility :gitcoin

  before_action :ensure_billing_enabled
  before_action :business_access_required
  before_action :ensure_feature_flag_enabled

  def show
    export = this_business.metered_usage_exports.find(params[:id])
    redirect_to export.generate_expiring_url
  end

  def create
    if valid_date_range?
      Billing::MeteredReportExportJob.perform_later(current_user, this_business, params[:days].to_i)
      flash[:notice] = "We're preparing your report! We’ll send an email to #{current_user.email} when it’s ready."
    else
      flash[:error] = "Selected date range was invalid. Please contact support or try again later."
    end

    redirect_back fallback_location: settings_billing_enterprise_path(this_business)
  end

  private

  def valid_date_range?
    Billing::MeteredUsageReportGenerator::VALID_DURATIONS.include?(params[:days].to_i)
  end

  def ensure_feature_flag_enabled
    if !GitHub.flipper[:metered_usage_export].enabled?(this_business) && !GitHub.flipper[:metered_usage_export].enabled?(current_user)
      render_404
    end
  end
end
