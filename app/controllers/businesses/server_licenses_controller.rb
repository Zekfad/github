# frozen_string_literal: true

class Businesses::ServerLicensesController < Businesses::BusinessController
  areas_of_responsibility :gitcoin

  before_action :business_admin_required

  def show
    license_file_data = GitHub::EnterpriseWeb::License.download(this_business.enterprise_web_business_id, params[:id])
    instrument(params[:id])
    send_data license_file_data, filename: "github-enterprise-#{params[:id]}.ghl"
  rescue Faraday::Error
    head :not_found
  end

  private

  def instrument(license_id)
    GitHub.instrument "business.enterprise_server_license_download",
      user: current_user,
      license_id: license_id,
      business: this_business
  end
end
