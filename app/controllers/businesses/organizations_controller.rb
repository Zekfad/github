# frozen_string_literal: true

class Businesses::OrganizationsController < Businesses::BusinessController

  before_action :dependency_insights_required, only: [:settings, :update_members_can_view_dependency_insights]
  before_action :business_admin_required
  before_action :dotcom_required
  before_action :require_organization_creatable, only: [:new, :create]
  before_action :sudo_filter, only: [:new, :create, :invite, :add_owner, :cancel_invitation]
  before_action :require_current_organization, only: [:invite, :add_owner, :cancel_invitation]
  before_action :require_organization_admin, only: [:invite, :add_owner, :cancel_invitation]

  def new
    render "businesses/organizations/new", locals: {
      current_organization: this_business.organizations.build,
    }
  end

  # Creates a fresh organization that belongs to `this_business`
  def create
    new_org_params = create_params.merge(
      company_name: this_business.name,
      billing_email: current_user.primary_user_email,
      admin_logins: [current_user.login],
    )

    creator_result = Organization::Creator.perform \
      current_user,
      GitHub::Plan.free,
      new_org_params,
      business_owned: true,
      business: this_business

    if creator_result.success?
      redirect_to invite_enterprise_organization_url(this_business, creator_result.organization)
    else
      flash.now[:error] = creator_result.error_message || creator_result.organization.errors.full_messages.join(", ")
      render "businesses/organizations/new", locals: {
        current_organization: creator_result.organization,
      }
    end
  end

  def finish
    # presence of this flash key will trigger the banner, only show for orgs created in the last day
    flash[:show_new_business_org_message] = "true" if current_organization.created_at > 1.day.ago
    redirect_to user_path(current_organization)
  end

  def invite
    render "businesses/organizations/invite", locals: {
      current_organization: current_organization,
      # list all invited owners. `reverse` so we show the most-recently-added owner first
      # which will keep the list order consistent between refreshes.
      owner_invitations: current_organization.pending_invitations.with_business_role(:admin).includes(:invitee),
    }
  end

  def add_owner
    identifier = params[:member].to_s.strip
    return render_404 if identifier.blank?

    begin
      if User.valid_email?(identifier)
        current_organization.invite(email: identifier, inviter: current_user, role: :admin)
      elsif invitee = User.find_by(login: identifier)
        current_organization.invite(invitee, inviter: current_user, role: :admin)
      else
        return render_404
      end
    rescue OrganizationInvitation::NoAvailableSeatsError
      flash[:error] = "No seats available to invite that user."
    rescue OrganizationInvitation::AlreadyAcceptedError, OrganizationInvitation::InvalidError,
      ActiveRecord::RecordInvalid, ::OrganizationInvitation::TradeControlsError
      flash[:error] = "User could not be invited."
    end

    redirect_to invite_enterprise_organization_url(this_business, current_organization)
  end

  def cancel_invitation
    invitation = current_organization.pending_invitations.with_business_role(:admin).find(params[:invitation_id])

    invitation.cancel(actor: current_user)

    redirect_to invite_enterprise_organization_url(this_business, current_organization)
  end

  def settings
    render_template_view "businesses/settings/organizations",
      Businesses::Settings::OrganizationsView,
      business: this_business,
      params: params
  end

  def update_members_can_view_dependency_insights
    enabled = params[:members_can_view_dependency_insights]&.to_s
    validate_setting value: enabled

    message = ""
    case enabled
    when "enabled"
      this_business.allow_members_can_view_dependency_insights(actor: current_user, force: true)
      message = "Members can now view dependency insights."
    when "disabled"
      this_business.disallow_members_can_view_dependency_insights(actor: current_user, force: true)
      message = "Members can no longer view dependency insights."
    when "no_policy"
      this_business.clear_members_can_view_dependency_insights(actor: current_user)
      message = "Organization administrators can now change this setting for individual organizations."
    end

    redirect_to settings_organizations_enterprise_path(this_business), notice: message
  end

  private

  def create_params
    params.require(:organization).permit(:login, :profile_name)
  end

  def current_organization
    @current_organization ||= this_business.organizations.find_by(login: params[:id])
  end
  helper_method :current_organization

  def require_current_organization
    render_404 if current_organization.nil?
  end

  def require_organization_admin
    render_404 unless current_organization.adminable_by?(current_user)
  end

  def require_organization_creatable
    if !Business::OrganizationPermission.new(this_business, current_user).can_create_organization?
      flash[:error] = "Not enough seats to create a new organization"
      return redirect_to enterprise_path(this_business)
    end
  end
end
