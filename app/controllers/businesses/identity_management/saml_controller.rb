# frozen_string_literal: true

class Businesses::IdentityManagement::SamlController < Businesses::BusinessController

  # How long does an admin's recovery session last?
  RECOVERY_SESSION_EXPIRY = 24.hours

  before_action :business_saml_required
  before_action :login_required, except: %w(metadata initiate consume)
  before_action :business_admin_required, only: %w(recover_prompt recover)
  # This should be before any filters that require a user session after consume
  before_action :repost_saml_response, if: :repost_saml_response?, only: %w(consume)
  before_action :resume_consume_on_staff_host, only: %w(consume)
  before_action :saml_config_required, except: %w(metadata)

  skip_before_action :verify_authenticity_token, only: [:consume]
  skip_before_action :private_instance_bootstrap_check, only: [:initiate, :consume]

  # Override that determines whether an individual controller action should have
  # IP allow list enforcement applied.
  #
  # The following actions do not require IP allow list enforcement:
  #
  # - metadata: serves `/enterprises/:slug/saml/metadata`, public metadata about this
  #   business's SAML SSO configuration
  # - consume: serves `/enterprises/:slug/saml/consume`, consumes SAML response,
  #   creating SAML sessions (when successful)
  #
  # Returns Boolean.
  def require_whitelisted_ip?
    !%w(
      metadata
      consume
    ).include?(action_name)
  end

  # Override that determines whether an individual controller action should have
  # external identity session enforcement applied.
  #
  # The following actions do not require external identity session enforcement:
  #
  # - metadata: serves `/enterprises/:slug/saml/metadata`, public metadata about this
  #   business's SAML SSO configuration
  # - initiate: serves `/enterprises/:slug/saml/initiate`, initiates SSO
  # - consume: serves `/enterprises/:slug/saml/consume`, consumes SAML response,
  #   creating SAML sessions (when successful)
  # - recover_prompt: serves `GET /enterprises/:slug/saml/recover`, allows admins to
  #   enter a recovery code to bypass SSO; POSTs to `/enterprises/:slug/saml/recover`
  # - recover: serves `POST /enterprises/:slug/saml/recover`, allows admins to submit
  #   a recovery code to bypass SSO
  #
  # Returns Boolean.
  def require_active_external_identity_session?
    !%w(
      metadata
      initiate
      consume
      recover_prompt
      recover
    ).include?(action_name)
  end

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: :recover,
    if: :saml_recover_rate_limit_filter,
    key: :saml_recover_rate_limit_key,
    log_key: :saml_recover_rate_limit_log_key,
    max: :saml_recover_rate_limit_max,
    at_limit: :saml_recover_rate_limit_record

  delegate :saml_provider, to: :this_business
  delegate :issuer, to: :saml_provider
  delegate :sso_url, to: :saml_provider
  delegate :idp_certificate, to: :saml_provider
  delegate :signature_method, :digest_method, to: :saml_provider

  # SAML metadata specific to the SSO flow for this business.
  # GET /enterprises/:slug/saml/metadata
  def metadata
    saml_metadata = ::SAML::Message::Metadata.new \
      assertion_consumer_service_url: assertion_consumer_service_url,
      sign_assertions: false,
      issuer: enterprise_url(this_business),
      name_identifier_format: "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified"

    render xml: saml_metadata
  end

  # POST /enterprises/:slug/saml/initiate
  def initiate
    options = {
      sp_url: enterprise_url(this_business),
      sso_url: sso_url,
      assertion_consumer_service_url: assertion_consumer_service_url,
      destination: sso_url,
      issuer: enterprise_url(this_business),
      signature_method: signature_method,
      digest_method: digest_method,
    }

    authn_request_url = Platform::Authentication::SamlAuthnRequestUrl.new(options)
    authn_request_url.relay_state = initiate_relay_state(authn_request_url.request)

    record_sso_initiated

    redirect_url = authn_request_url.to_s
    render "businesses/identity_management/sso_meta_redirect", locals: { redirect_url: redirect_url }, layout: "redirect"
  end

  # POST /enterprises/:slug/saml/consume
  def consume
    if validate_provider_settings?
      validate_provider_settings
    else
      consume_saml_response
    end
  end

  # Revokes the currently active external identity session for this business.
  # DELETE /enterprises/:slug/saml/revoke
  def revoke
    if session = current_external_identity_session(target: this_business)
      session.destroy
    end

    if request.xhr?
      head :ok
    else
      redirect_to "#{enterprise_url(this_business)}/sso"
    end
  end

  # Render a form to enter a recovery code.
  # GET /enterprises/:slug/saml/recover
  def recover_prompt
    render_template_view "businesses/identity_management/recover",
      Businesses::IdentityManagement::SingleSignOnView,
      business: this_business,
      layout: "session_authentication"
  end

  # Verify the recovery code provided and skip SAML SSO by creating a
  # temporary ExternalIdentitySession for the user.
  # POST /enterprises/:slug/saml/recover
  def recover
    if params[:recovery_code]
      if this_business.saml_provider.verify_recovery_code!(params[:recovery_code])
        if external_identity = this_business.saml_provider.external_identities.linked_to(current_user).first
          # Set temporary external identity session
          update_or_create_external_identity_session external_identity,
            expires_at: RECOVERY_SESSION_EXPIRY.from_now

          flash[:notice] = "Your recovery code was accepted."
          redirect_to settings_security_enterprise_path(this_business)
          return
        else
          flash[:error] = "You must have authenticated via SAML SSO at least once."
        end
      else
        flash[:error] = "Your recovery code was invalid."
      end
    end
    recover_prompt
  end

  private

  def test_settings
    @test_settings ||= Business::SamlProviderTestSettings.most_recent_for(
      user: current_user,
      business: this_business,
    )
  end

  def validate_provider_settings?
    validate_provider_settings_cookie == "validate"
  end

  def validate_provider_settings_cookie
    saml_return_to = cookies.encrypted[:saml_return_to] || cookies.encrypted[:saml_return_to_legacy]

    cookies.delete :saml_return_to
    cookies.delete :saml_return_to_legacy

    saml_return_to
  end

  def validate_provider_settings
    if logged_in? && this_business.adminable_by?(current_user)
      flash[:saml_test_result] = { status: nil, message: nil }

      saml_consumer = Platform::Authentication::SamlConsumer.new \
        sp_url: service_provider_url,
        idp_certificate: test_settings.idp_certificate,
        issuer: test_settings.issuer,
        signature_method: test_settings.signature_method,
        digest_method: test_settings.digest_method

      auth_result = saml_consumer.perform(params[:SAMLResponse])

      # if SAMLResponse is successful, verify the RelayState, and possibly
      # override with Invalid state if that fails.
      if auth_result.success? && auth_result.assertion.in_response_to.present?
        relay_state = consume_relay_state(auth_result)

        if relay_state.ok? && test_settings.valid?
          flash[:saml_test_result][:status] = Business::SamlProviderTestSettings::SUCCESS
          flash[:saml_test_result][:message] =  "Your SAML provider settings have been validated. Remember to save your changes."
          flash[:notice] = flash[:saml_test_result][:message]
        else
          # override result to be invalid if relay state isn't valid
          auth_result = Platform::Authentication::SamlResult.invalid(auth_result.assertion)
          flash[:saml_test_result][:status] = Business::SamlProviderTestSettings::FAILURE
          flash[:saml_test_result][:message] = saml_validation_error_message(auth_result, test_settings, required_relay_state: relay_state)
          flash[:error] = "Single sign-on via your configured IdP failed."
        end
      else
        flash[:saml_test_result][:status] = Business::SamlProviderTestSettings::FAILURE
        flash[:saml_test_result][:message] = saml_validation_error_message(auth_result, test_settings)
        flash[:error] = "Single sign-on via your configured IdP failed."
      end

      if flash[:saml_test_result][:status] == Business::SamlProviderTestSettings::FAILURE
        error = Business::SamlProviderTestSettings::InvalidSamlResponseError.new(flash[:saml_test_result][:message])
        error.set_backtrace(caller)
        Failbot.report!(error)
      end

      if GitHub.private_instance? && !GitHub.private_instance_bootstrapper.bootstrapped?
        redirect_to bootstrap_instance_configuration_path(step: "saml-idp")
      else
        redirect_to settings_security_enterprise_path(this_business)
      end
    else
      access_denied
    end
  end

  def consume_saml_response
    # if an org has test settings but no saved provider, return a 404
    # safeguards against an exception that exposes HTTP ENV
    render_404 and return if saml_provider.nil?

    saml_consumer = Platform::Authentication::SamlConsumer.new \
      sp_url: enterprise_url(this_business, host: GitHub.host_domain),
      idp_certificate: idp_certificate,
      issuer: issuer,
      signature_method: signature_method,
      digest_method: digest_method

    auth_result = saml_consumer.perform(params[:SAMLResponse])

    # if SAMLResponse is successful, verify the RelayState, and possibly
    # override with Invalid state if that fails.
    if auth_result.success? && auth_result.assertion.in_response_to.present?
      relay_state = consume_relay_state(auth_result)

      if relay_state.ok?
        auth_result.redirect_url = (relay_state.data || Hash.new)["return_to"].presence
      else
        # override result to be invalid if relay state isn't valid
        auth_result = Platform::Authentication::SamlResult.invalid(auth_result.assertion)
      end
    end

    record_sso_completed(auth_result, relay_state)

    if auth_result.success?
      provisioning_result = provision_external_identity(auth_result.user_data)

      if provisioning_result.success?
        if logged_in?
          update_or_create_external_identity_session provisioning_result.external_identity,
            expires_at: auth_result.assertion.session_expires_at

          if credential_authorization = authorize_credential_request(relay_state)
            # SSO flow was used to authorize a credential. Notify the user they're good to go.
            return_to = auth_result.redirect_url || user_path(credential_authorization.organization)

            render_template_view "businesses/identity_management/credential_authorized",
              Businesses::IdentityManagement::CredentialAuthorizedView,
              { business: this_business,
                credential_authorization: credential_authorization,
                return_to: return_to,
              },
              layout: "session_authentication"
            return
          elsif form_data = form_data_for_replay(relay_state)
            # Non-GET request was enforced. Prompt user to replay to request.
            render_template_view "businesses/identity_management/replay_enforced_request",
              Businesses::IdentityManagement::ReplayEnforcedRequestView,
              { business: this_business,
                form_data: form_data,
              },
              layout: "session_authentication"
            return
          end
        else
          prompt_sign_in_or_sign_up provisioning_result.external_identity, auth_result
          return
        end
      else
        flash[:error] = "There was an issue joining the enterprise: #{ provisioning_result.errors.full_messages.to_sentence }"
        flash[:saml_error] = :provisioning
      end
    elsif auth_result.unauthorized?
      log \
        at: "failure - Unauthorized",
        login: auth_result.assertion.name_id || "_unknown",
        errors: auth_result.assertion.errors,
        params: request.params
      flash[:error] = "Unsuccessful SAML authentication."
      flash[:saml_error] = :unauthorized
    elsif auth_result.invalid?
      log \
        at: "failure - Invalid SAML response",
        login: auth_result.assertion.name_id || "_unknown",
        errors: auth_result.assertion.errors,
        params: request.params
      if this_business.adminable_by?(current_user)
        flash[:error] = "Invalid SAML message: #{auth_result.assertion.errors.join(" ")}"
      else
        flash[:error] = "Unable to authenticate your SAML session (invalid SAML message). Please try again or contact an administrator of your enterprise."
      end
      flash[:saml_error] = :invalid
    end

    redirect_url = auth_result.redirect_url || enterprise_saml_redirect_url(auth_result, enterprise_account: this_business)
    safe_redirect_to redirect_url
  end

  def enterprise_saml_redirect_url(auth_result, enterprise_account: nil, organization: nil)
    if auth_result.success?
      if enterprise_account&.member?(current_user)
        enterprise_url(enterprise_account)
      elsif organization
        user_path(organization)
      else
        dashboard_url
      end
    else
      # Take user to their dashboard if SSO had failed - they may not have access
      # to the url they were trying to reach
      dashboard_url
    end
  end

  def saml_validation_error_message(auth_result, test_settings, required_relay_state: :not_required)
    errors = auth_result.errors || []

    if !auth_result.assertion.in_response_to.present?
      errors << "InResponseTo was invalid or missing"
    end

    errors += test_settings.errors.full_messages

    if required_relay_state != :not_required && required_relay_state.invalid?
      errors << "RelayState is invalid"
    end

    errors.join(", ")
  end

  def assertion_consumer_service_url
    idm_saml_consume_enterprise_url(this_business)
  end

  def saml_config_required
    return if this_business&.saml_provider.present? || test_settings.persisted?
    render_404
  end

  # Private: Attempts to provision an external identity for the current_user
  # using the details returned by the identity provider.
  #
  # saml_user_data  - A Platform::Provisioning::UserData instance containing
  #                   user information provided by the IdP.
  #
  # Returns a Platform::Provisioning:Status
  def provision_external_identity(saml_user_data)
    if logged_in? && GitHub.flipper[:enterprise_idp_provisioning].enabled?(this_business)
      Platform::Provisioning::IdentityProvisioner.provision_and_add_member \
        target: this_business,
        user_id: current_user.id,
        user_data: saml_user_data,
        mapper: Platform::Provisioning::SamlMapper
    else
      Platform::Provisioning::IdentityProvisioner.provision \
        target: this_business,
        user_id: current_user&.id,
        user_data: saml_user_data,
        mapper: Platform::Provisioning::SamlMapper
    end
  end

  # Private: Sends the user to a page prompting them to sign in or sign up.
  # The provisioned external identity is stored in the session so that we
  # can link it up after they are signed in.
  # If the external_identity is already linked to a GitHub account, we prompt
  # the user to sign in.
  #
  # external_identity - The ExternalIdentity provisioned during SAML SSO.
  # auth_result       - The consumed SAML::Message::Response.
  #
  # Returns nothing
  def prompt_sign_in_or_sign_up(external_identity, auth_result)
    session[:unlinked_external_identity_id] = external_identity.id
    session[:unlinked_session_expires_at] = auth_result.assertion.session_expires_at
    session[:sso_return_to] = auth_result.redirect_url.presence

    if external_identity.user.present?
      anonymous_flash[:notice] = "Sign in to your personal account to complete SSO for #{this_business.name}"
      redirect_to login_path(
        login: external_identity.user.login,
        return_to: business_idm_sso_sign_up_enterprise_path(this_business),
      )
    else
      redirect_to business_idm_sso_sign_up_enterprise_path(this_business)
    end
  end

  def initiate_relay_state(authn_request, data: {})
    data[:return_to] ||= params[:return_to] if params[:return_to].present?

    # Persist captured form data from enforced request so user
    # can replay it after SSO.
    data[:form_data] = params[:form_data] if params[:form_data]

    if credential_authorization_request.presence
      data.update credential_authorization_request_params_for(credential_authorization_request) || Hash.new
    end

    relay_state = Platform::Authentication::SamlRelayState.initiate(
      request_id: authn_request.id,
      data: data,
    )

    # persist the relay state digest for future validation
    # Cookie attributes are additionally set by by app/controller/application_controller/security_headers_dependency.rb
    # By default the :saml_csrf_token will have the SameSite=none attribute set from the secure_headers gem.
    # The :saml_csrf_legacy cookie does not have the SameSite=none attribute set. This is to accomodate
    # browsers that reject or mistreat cookies with the SameSite=none attribute. In particular, there is
    # a bug that treats SameSite=None and invalid values as Strict in macOS before 10.15 Catalina and in iOS before 13.
    cookies.encrypted[:saml_csrf_token] = saml_csrf_cookie(relay_state.digest)
    cookies.encrypted[:saml_csrf_token_legacy] = saml_csrf_cookie(relay_state.digest)

    [relay_state.nonce, employee_unicorn_relay_host].compact.join(";")
  end

  # Encrypted cookies have some odd behavior when the exact same object is used as a value
  # This reduces repetition while helping create valid cookies.
  def saml_csrf_cookie(digest)
    {
      value: digest,
      expires: ::Platform::Authentication::SamlRelayState::DEFAULT_EXPIRY,
      secure: request && request.ssl?,
      httponly: true,
      domain: cookie_domain,
    }
  end

  def cookie_domain
    ".#{GitHub.host_name}"
  end

  def consume_relay_state(auth_result)
    digest = cookies.encrypted[:saml_csrf_token]
    digest_legacy = cookies.encrypted[:saml_csrf_token_legacy]

    record_saml_cookie_usage(digest, digest_legacy)
    remove_saml_cookies

    digest = digest.presence || digest_legacy.presence

    Platform::Authentication::SamlRelayState.consume(
      nonce:  params[:RelayState],
      request_id: auth_result.success? && auth_result.assertion.in_response_to,
      digest: digest,
    )
  end

  def remove_saml_cookies
    cookies.delete(:saml_csrf_token, domain: cookie_domain)
    cookies.delete(:saml_csrf_token_legacy, domain: cookie_domain)
  end

  # Keep track of which cookies are being presented. This will give
  # insight into browser adoption of the new cookie standards
  #
  # The meaning of the various cookie_presence states are:
  # both:               The browser has not implemented the SameSite=none restrictions
  # only_samesite_none: The browser has implemented the SameSite=none restriction
  # only_legacy:        The browser is one that needs the legacy workaround. This number
  #                     will drive when it's safe to remove the legacy cookies
  # missing:            An IdP-initiated SAML response, so no cookie is expected.
  def record_saml_cookie_usage(digest, digest_legacy)
    if digest.present? && digest_legacy.present?
      cookie_presence = "both"
    elsif digest.present? && digest_legacy.nil?
      cookie_presence = "only_samesite_none"
    elsif digest.nil? && digest_legacy.present?
      cookie_presence = "only_legacy"
      digest = digest_legacy
    elsif params[:RelayState].present?
      cookie_presence = "missing"
    else
      return # nothing to log
    end

    tags = ["cookie_presence:#{cookie_presence}"] + dogstats_request_tags
    GitHub.dogstats.increment "saml.cookie_presence", tags: tags
  end

  def credential_authorization_request_params_for(request)
    return unless request.present?

    organization_id = request.data["organization_id"]
    credential_type = request.data["credential_type"]
    credential_id   = request.data["credential_id"]
    fingerprint     = request.data["fingerprint"]

    return unless organization_id && credential_type && credential_id

    organization = Organization.find_by(id: organization_id)

    return unless organization.present?

    credential = current_user.oauth_accesses.find_by_id(credential_id) if credential_type == "OauthAccess"
    credential =  current_user.public_keys.find_by_id(credential_id) if credential_type == "PublicKey"

    return unless credential.present?
    {
      organization_id: organization.id,
      credential_id: credential.id,
      credential_type: credential.class.name,
      fingerprint: credential.fingerprint,
    }
  end

  # Check if the SAML Response should be redirected via POST to the consume endpoint
  # Only needed if the user_session isn't present, and should only happen once.
  def repost_saml_response?
    # user_session is present so a repost isn't necessary
    return false if logged_in?

    # only repost once
    return false if session.delete(:reposted_saml_response)

    true
  end

  # Redirect incoming SAML responses via POST to consume.
  # This will enable the user_session and other same site cookies to be available
  def repost_saml_response
    # set a session variable so we don't keep looping
    session[:reposted_saml_response] = true

    form_data = {
      "SAMLResponse" => params[:SAMLResponse],
      "RelayState" => params[:RelayState],
      "_target" => idm_saml_consume_enterprise_url(this_business),
    }

    render_template_view "businesses/identity_management/replay_enforced_request",
      Businesses::IdentityManagement::ReplayEnforcedRequestView,
      { business: this_business,
        form_data: form_data,
      },
      layout: "session_authentication"
  end

  # Returns the staff host name to resume SAML SSO on if initiated from a
  # staff host, branch lab, review lab, or stafftools host (admin.github.com).
  #
  # Returns host name String or nil if not initiating SSO from a staff host.
  def employee_unicorn_relay_host
    # short circuit if the request host matches the root GitHub.com domain;
    # this is needed for the "lab" host (github-staff1) since it uses the same
    # hostname as production and doesn't require a relay.
    return if request.host == GitHub.host_domain

    # allow dev env to not require employee unicorn/admin host to test
    return request.host if Rails.env.development?

    # bail out if the current host is not an employee unicorn/admin host
    return unless GitHub.employee_unicorn? || GitHub.admin_host?

    request.host
  end

  SAFE_RELAY_HOSTS = [
    "*.#{GitHub.host_domain}",
  ].freeze

  # Redirect to the initiating staff host to consume the SAML Response.
  def resume_consume_on_staff_host
    return unless logged_in? && current_user.employee?
    return unless params[:RelayState].present?

    nonce, relay_host = params[:RelayState].split(";")
    return unless relay_host.present?
    return unless relay_host.end_with?("." + GitHub.host_domain)

    SecureHeaders.append_content_security_policy_directives(
      request,
      form_action: SAFE_RELAY_HOSTS,
    )

    form_data = {
      "SAMLResponse" => params[:SAMLResponse],
      "RelayState" => nonce,
      "_target" => idm_saml_consume_enterprise_url(this_business, host: relay_host),
    }

    render_template_view "businesses/identity_management/replay_enforced_request",
      Businesses::IdentityManagement::ReplayEnforcedRequestView,
      { business: this_business,
        form_data: form_data,
      },
      layout: "session_authentication"
  end

  def credential_authorization_request
    return unless token = params[:authorization_request].presence

    Organization::CredentialAuthorization.consume_request(
      target: this_business,
      token: token,
      actor: current_user,
    )
  end

  def authorize_credential_request(relay_state)
    return unless data = relay_state.present? && relay_state.data.presence

    # NOTE: fingerprint may be nil
    organization_id = data["organization_id"]
    credential_id   = data["credential_id"]
    credential_type = data["credential_type"]
    fingerprint     = data["fingerprint"]

    organization = Organization.find_by(id: organization_id)
    return unless organization.present? && credential_id.present? && credential_type.present?

    # find personal access token to authorize
    credential = case credential_type
     when "OauthAccess"
       current_user.oauth_accesses.personal_tokens.
         where(fingerprint: fingerprint).
         find_by_id(credential_id)
     when "PublicKey"
       current_user.public_keys.
         where(fingerprint: fingerprint).
         find_by_id(credential_id)
    end
    return unless credential.present?

    # authorize the personal access token
    Organization::CredentialAuthorization.grant \
      organization: organization,
      credential: credential,
      actor: current_user
  end

  def form_data_for_replay(relay_state)
    return unless data = relay_state.present? && relay_state.data.presence

    data["form_data"]
  end

  def saml_recover_rate_limit_key
    "saml_recover_limiter:#{current_user.id}"
  end

  def saml_recover_rate_limit_filter
    params[:recovery_code].present?
  end

  def saml_recover_rate_limit_max
    10
  end

  def saml_recover_rate_limit_log_key
    "saml-recover"
  end

  def saml_recover_rate_limit_record
    key = "saml.recover_rate_limited"
    # TODO - remove once the second dogstats call has populated
    GitHub.dogstats.increment(key)
    GitHub.dogstats.increment("rate_limited", tags: ["subject:saml", "action:recover"])
  end

  # For tagging metrics
  def authed_or_anon
    logged_in? ? "auth" : "anon"
  end

  def record_sso_initiated
    GitHub.dogstats.increment "saml.sso_initiated",
      tags: dogstats_request_tags + [
        "logged_in:#{authed_or_anon}",
        "return_to:#{params[:return_to].present?}",
        "authorization_request:#{params[:authorization_request].present?}",
      ]
  end

  def record_sso_completed(auth_result, relay_state)
    result_status =
      if auth_result.success?
        :success
      else
        auth_result.failure_type
      end

    relay_state_status =
      if relay_state.nil?
        if params[:RelayState].present?
          :ignored
        else
          :missing
        end
      else
        if relay_state.ok?
          :ok
        else
          :invalid
        end
      end

    GitHub.dogstats.increment "saml.sso_completed",
      tags: dogstats_request_tags + [
        "saml_result:#{result_status}",
        "relay_state:#{relay_state_status}",
        "logged_in:#{authed_or_anon}",
        "return_to:#{params[:return_to].present?}",
        "authorization_request:#{params[:authorization_request].present?}",
      ]

    # Instrument and write to the audit log
    this_business.instrument :sso_response,
      name_id: auth_result.assertion.name_id,
      result: result_status,
      relay_state: relay_state_status,
      issuer: issuer,
      errors: !auth_result.success?,
      error_messages: auth_result.errors
  end
end
