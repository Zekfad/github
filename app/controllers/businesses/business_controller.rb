# frozen_string_literal: true

class Businesses::BusinessController < ApplicationController
  include BusinessesHelper
  include EnterpriseAccountStatsHelper

  class InvalidBusinessSettingValueError < StandardError; end

  rescue_from InvalidBusinessSettingValueError, with: :report_invalid_input_error
  around_action :register_enterprise_account_controller_stats

  private

  def business_access_required
    return render_404 unless this_business

    if business_access?
      return
    elsif this_business.pending_admin_invitation_for(current_user, role: :owner)
      redirect_to admin_invitation_enterprise_path(this_business)
    elsif this_business.pending_admin_invitation_for(current_user, role: :billing_manager)
      redirect_to billing_manager_invitation_enterprise_path(this_business)
    else
      render_404
    end
  end

  def dependency_insights_required
    render_404 unless this_business&.dependency_insights_enabled_for?(current_user)
  end

  def business_admin_required
    render_404 unless this_business&.owner?(current_user)
  end

  def single_business_environment_required
    render_404 unless GitHub.single_business_environment? && GitHub.global_business.owner?(current_user)
  end

  def business_member_invitations_required
    render_404 if GitHub.bypass_business_member_invites_enabled?
  end

  def business_saml_required
    return unless GitHub.enterprise?
    render_404 unless GitHub.private_instance_user_provisioning?
  end

  def business_access?
    this_business&.owner?(current_user) ||
      this_business&.billing_manager?(current_user) ||
      has_member_business_access?
  end

  # Does the current user have restricted access to the enterprise account
  # because they are a member of an org within the enterprise?
  #
  # Note: Only returns true if the member is accessing GET /enterprises/:slug.
  #
  # Returns Boolean.
  def has_member_business_access?
    return false if this_business.nil? || !logged_in?

    # Members of member orgs can only GET /enterprises/:slug
    controller_name == "businesses" && action_name == "show" &&
      this_business.user_is_member_of_owned_org?(current_user)
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_business
    this_business
  end

  def this_business
    @this_business ||=
      if GitHub.single_business_environment? && !params[:slug]
        GitHub.global_business
      else
        Business.find_by(slug: slug_param)
      end
  end
  helper_method :this_business

  def slug_param
    params.require(:slug)
  end

  def admin_login_param
    params.require(:admin)
  end

  def validate_setting(value:, valid_values: %w(enabled disabled no_policy))
    raise InvalidBusinessSettingValueError unless valid_values.include?(value)
  end

  def report_invalid_input_error
    flash[:error] = "You provided an invalid input value. Please try again."
    redirect_back fallback_location: settings_profile_enterprise_path(this_business)
  end

  # before_action that sets flash[:error] and redirects back if there is a
  # configuration run in progress.
  def check_for_in_progress_config_run
    if config_run_in_progress?
      flash[:error] = "A configuration run is currently applying settings \
        on your instance. Please wait a few minutes before trying again.".squish
      redirect_to :back
    end
  end
end
