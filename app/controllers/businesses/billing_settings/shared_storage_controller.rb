# frozen_string_literal: true

class Businesses::BillingSettings::SharedStorageController < Businesses::BusinessController
  areas_of_responsibility :gitcoin

  before_action :ensure_billing_enabled
  before_action :business_access_required

  def show
    view = Businesses::BillingSettings::ShowView.new(current_user: current_user, business: this_business)
    render partial: "businesses/billing_settings/shared_storage", locals: { view: view }
  end
end
