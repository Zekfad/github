# frozen_string_literal: true

class Businesses::EnterpriseLicensingController < Businesses::BusinessController
  areas_of_responsibility :gitcoin

  before_action :business_access_required

  def show
    render "businesses/enterprise_licensing/show"
  end

  def download_consumed_licenses
    send_data Business::LicenseCsvGenerator.new(this_business).generate,
      filename: "consumed_licenses.csv"
  end
end
