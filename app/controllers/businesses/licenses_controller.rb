# frozen_string_literal: true

class Businesses::LicensesController < Businesses::BusinessController

  before_action :business_admin_required

  def index
    return render_404 unless GitHub.licensed_mode?

    render "businesses/settings/license", locals: {
      params: params,
    }
  end

  def download
    return render_404 unless GitHub.licensed_mode?
    info = DotcomConnection.new.license_info
    send_data JSON.generate(info), filename: "#{GitHub.host_name}.json"
  end
end
