# frozen_string_literal: true

class Businesses::UserLicensesController < Businesses::BusinessController
  areas_of_responsibility :gitcoin

  before_action :business_admin_required

  def update
    user = User.find_by!(login: params[:login])
    user_license = this_business.user_licenses.ensure_exists(user_id: user.id)

    if user_license.update(user_license_params)
      flash[:notice] = "Updated #{user.login}'s license"
    else
      flash[:error] = "Unable to update #{user.login}'s license"
    end

    redirect_to people_enterprise_path(this_business)
  end

  private

  def user_license_params
    params.require(:user_license).permit(:license_type)
  end
end
