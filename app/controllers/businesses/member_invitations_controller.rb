# frozen_string_literal: true

class Businesses::MemberInvitationsController < Businesses::BusinessController

  before_action :business_member_invitations_required

  before_action :sudo_filter, only: [:invite_admin]

  # When users who are not logged in attempt to view a pending invitation,
  # redirect to login with a return_to param.
  before_action :login_required, only: [
    :show_pending_admin_invitation, :show_pending_billing_manager_invitation
  ]

  before_action :permission_to_invite_required, except: [
    :show_pending_admin_invitation, :accept_pending_admin_invitation,
    :show_pending_billing_manager_invitation, :accept_pending_billing_manager_invitation
  ]
  before_action :find_pending_admin_invitation, only: [
    :show_pending_admin_invitation, :accept_pending_admin_invitation
  ]
  before_action :find_pending_billing_manager_invitation, only: [
    :show_pending_billing_manager_invitation, :accept_pending_billing_manager_invitation
  ]

  before_action :ensure_two_factor_requirement_is_met, only: [
    :accept_pending_admin_invitation, :accept_pending_billing_manager_invitation
  ]

  before_action :ensure_saml_sso_requirement_is_met, only: [
    :accept_pending_admin_invitation, :accept_pending_billing_manager_invitation
  ]

  def invite_admin
    if User.valid_email?(params[:admin])
      invitee = User.find_by_email(params[:admin])
      email = params[:admin]
    else
      invitee = User.find_by(login: params[:admin])
      if invitee.blank?
        flash[:error] = "Could not find user #{params[:admin]} to invite as an administrator"
        return redirect_to admins_enterprise_path(this_business)
      end
    end

    errors = []
    invitation = nil
    begin
      invitation = if invitee.present?
        this_business.invite_admin user: invitee, inviter: current_user, role: params[:role]&.downcase
      else
        this_business.invite_admin email: email, inviter: current_user, role: params[:role]&.downcase
      end
    rescue BusinessMemberInvitation::AlreadyAcceptedError, BusinessMemberInvitation::InvalidError, ActiveRecord::RecordInvalid => error
      errors << error.message
    end

    if errors.any?
      flash[:error] = errors.first
      redirect_to admins_enterprise_path(this_business)
    else
      invitation_url = if invitation.owner?
        admin_invitation_enterprise_url(this_business)
      elsif invitation.billing_manager?
        billing_manager_invitation_enterprise_url(this_business)
      end

      extra = "They can also visit #{invitation_url} to accept the invitation."
      notice = <<~NOTICE
        You've invited #{params[:admin]} to become an
        administrator of #{this_business.name}!
        They'll be receiving an email shortly.
        #{extra unless invitation.email.present?}
      NOTICE
      redirect_to admins_enterprise_path(this_business), notice: notice
    end
  end

  def show_pending_admin_invitation
    render "businesses/admins/pending_invitation", locals: {
      invitation: pending_admin_invitation,
      must_enable_two_factor: must_enable_two_factor?,
    }
  end

  def accept_pending_admin_invitation
    errors = []
    begin
      pending_admin_invitation.accept acceptor: current_user
    rescue BusinessMemberInvitation::ExpiredError
      errors << "This invitation has already expired."
    rescue BusinessMemberInvitation::AlreadyAcceptedError
      errors << "This invitation has already been accepted."
    rescue BusinessMemberInvitation::InvalidAcceptorError
      errors << "Viewer cannot accept an invitation when they are not the invitee."
    end

    if errors.any?
      flash[:error] = errors.first
    else
      flash[:notice] = "You are now #{pending_admin_invitation.role_for_message} of #{this_business.name}."
    end
    redirect_to enterprise_path(this_business)
  end

  def cancel_admin_invitation
    invitation = this_business.invitations.pending.find_by!(id: params[:invitation_id])
    errors = []
    begin
      if invitation.cancelable_by?(current_user)
        invitation.cancel actor: current_user
      else
        errors << "#{current_user} cannot cancel administrator invitations for #{invitation.business.name}."
      end
    rescue BusinessMemberInvitation::AlreadyAcceptedError
      errors << "This invitation has already been accepted."
    end

    if errors.any?
      flash[:error] = errors.first
    else
      flash[:notice] = \
        "You've canceled #{invitation.email_or_invitee_name}'s invitation to become #{invitation.role_for_message} of #{invitation.business.name}."
    end
    redirect_to pending_admins_enterprise_path(this_business)
  end

  def show_pending_billing_manager_invitation
    render "businesses/billing_settings/pending_manager_invitation", locals: {
      invitation: pending_billing_manager_invitation,
      must_enable_two_factor: must_enable_two_factor?,
    }
  end

  def accept_pending_billing_manager_invitation
    errors = []
    begin
      pending_billing_manager_invitation.accept acceptor: current_user
    rescue BusinessMemberInvitation::AlreadyAcceptedError
      errors << "This invitation has already been accepted."
    rescue BusinessMemberInvitation::InvalidAcceptorError
      errors << "Viewer cannot accept an invitation when they are not the invitee."
    end

    if errors.any?
      flash[:error] = errors.first
    else
      flash[:notice] = "You are now #{pending_billing_manager_invitation.role_for_message} of #{this_business.name}."
    end
    redirect_to settings_billing_enterprise_path(this_business)
  end

  private

  def email_invitation?
    params[:invitation_token].present?
  end

  def find_pending_admin_invitation
    return render_404 unless this_business
    @pending_admin_invitation ||= if email_invitation?
      this_business.invitations.pending
        .with_business_role(:owner).with_token(params[:invitation_token])
    else
      this_business.pending_admin_invitation_for current_user, role: :owner
    end
    @pending_admin_invitation || pending_admin_invitation_not_found
  end
  attr_reader :pending_admin_invitation

  def pending_admin_invitation_not_found
    if this_business.owner?(current_user)
      redirect_to enterprise_path(this_business)
    else
      pending_member_invitation_not_found
    end
  end

  def find_pending_billing_manager_invitation
    return render_404 unless this_business
    @pending_billing_manager_invitation ||= if email_invitation?
      this_business.invitations.pending
        .with_business_role(:billing_manager).with_token(params[:invitation_token])
    else
      this_business.pending_admin_invitation_for(current_user, role: :billing_manager)
    end
    @pending_billing_manager_invitation || pending_billing_manager_invitation_not_found
  end
  attr_reader :pending_billing_manager_invitation

  def pending_billing_manager_invitation_not_found
    if this_business.billing_manager?(current_user)
      redirect_to enterprise_path(this_business)
    else
      pending_member_invitation_not_found
    end
  end

  def pending_member_invitation_not_found
    return render_404 unless this_business
    render "businesses/pending_member_invitation_not_found", status: :not_found
  end

  def permission_to_invite_required
    business_admin_required
  end

  def must_enable_two_factor?
    this_business.two_factor_requirement_enabled? &&
      !current_user.two_factor_authentication_enabled?
  end

  def ensure_two_factor_requirement_is_met
    if must_enable_two_factor?
      case action_name
      when "accept_pending_admin_invitation"
        redirect_to admin_invitation_enterprise_path(this_business)
      when "accept_pending_billing_manager_invitation"
        redirect_to billing_manager_invitation_enterprise_path(this_business)
      end
    end
  end

  def ensure_saml_sso_requirement_is_met
    return if this_business.saml_sso_requirement_met_by?(current_user)
    render_external_identity_session_required(target: this_business)
  end
end
