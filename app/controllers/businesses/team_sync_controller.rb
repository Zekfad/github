# frozen_string_literal: true

class Businesses::TeamSyncController < Businesses::BusinessController

  # skip here so that we can check feature flag first
  skip_before_action :perform_conditional_access_checks
  before_action :this_business_required, except: [:azure_callback]

  before_action :business_admin_required, except: [:setup, :initiate, :azure_callback, :review]
  before_action :perform_conditional_access_checks, except: [:azure_callback]
  before_action :require_external_identity, except: [:azure_callback]

  around_action :select_write_database, only: [:azure_callback]

  ERROR_MESSAGES = {
    invalid_state_transition: "Cannot do that right now. Check the current state and try again.",
    invalid_provider_type: "The selected identity provider is not supported. Select a valid identity provider and try again.",
    invalid_provider_id: "The identity provider was not detected or is not supported. Ensure your SAML SSO identity provider is correctly configured.",
    invalid_state: "There was an error reading the response. Try again.",
    registration_error: "There was an internal error. Try again.",
    admin_consent_failed: "Admin approval failed: [%s] %s",
    tenant_update_failed: "There was an error saving the settings. Try again.",
    token_not_found: "There was a problem with your setup token. Please try again.",
  }
  ERROR_MESSAGES.default = "An unknown error occurred. Please try again. (code: %s)"

  # If allowed set the state of the tenant locally and on group syncer to allow setup to occur.
  # This is a negotiation between the local state and the group syncer service to check if we are in a state
  # in which this is allowed and if so, make group syncer aware we are transitioning.
  def install
    provider = ::TeamSync::Provider.detect(issuer: this_business.external_identity_session_owner.saml_provider.issuer)
    if provider.nil? || !provider.supported_for_business?(this_business)
      flash[:error] = ERROR_MESSAGES[:invalid_provider_type]
      redirect_to settings_security_enterprise_url(this_business)
      return
    end

    err, downstream_err = team_sync_setup_flow.initiate_setup(provider_type: provider.type, provider_id: provider.id)
    if err
      flash[:error] = ERROR_MESSAGES[err] % {err: err, provider_type: provider.type}
      redirect_to settings_security_enterprise_url(this_business)
      return
    end

   token, err = team_sync_setup_flow.generate_setup_lease_token
    if err
      flash[:error] = ERROR_MESSAGES[err] % {err: err, provider_type: provider.type}
      redirect_to settings_security_enterprise_url(this_business)
      return
    end

    redirect_to team_sync_setup_enterprise_path(this_business, token: token)
  end

  def setup
    token = setup_params[:token]
    if token.nil? || !team_sync_setup_flow.get_token(token).valid?
      flash[:error] = ERROR_MESSAGES[:token_not_found]
      redirect_to settings_security_enterprise_url(this_business)
      return
    end

    if team_sync_setup_flow.review_required?
      redirect_to team_sync_review_enterprise_path(this_business, token: token)
      return
    end

    unless team_sync_setup_flow.pending?
      flash[:error] = ERROR_MESSAGES[:invalid_state_transition]
      redirect_to settings_security_enterprise_url(this_business)
      return
    end

    render_template_view "businesses/security_settings/team_sync/setup",
      Businesses::SecuritySettings::TeamSync::InitiateSetupView,
      business: this_business,
      tenant: team_sync_setup_flow.tenant,
      layout: "session_authentication",
      token: setup_params[:token]
  end

  def initiate
    token = initiate_params[:token]
    if token.nil? || !team_sync_setup_flow.get_token(token).valid?
      render_404
      return
    end

    unless team_sync_setup_flow.pending?
      flash[:error] = ERROR_MESSAGES[:invalid_state_transition]
      redirect_to settings_security_enterprise_url(this_business)
      return
    end

    setup_url = team_sync_setup_flow.setup_url(redirect_uri: enterprises_team_sync_azure_callback_url, token: token)
    redirect_url = setup_url.to_s
    render "businesses/security_settings/meta_redirect", locals: { redirect_url: redirect_url }, layout: "redirect"
  end

  def azure_callback
    token = azure_callback_params["state"]
    @team_sync_setup_flow, err = ::TeamSync::SetupFlow.callback(
      token: token,
      provider_id: azure_callback_params["tenant"],
      actor: current_user, # may be nil
    )
    if err
      flash[:error] = ERROR_MESSAGES[err] % {err: err}
      if @team_sync_setup_flow.present?
        redirect_to team_sync_setup_enterprise_url(@team_sync_setup_flow.business, token: token)
      else
        render_404
      end
      return
    end

    @this_business = @team_sync_setup_flow.business

    return if performed?

    perform_conditional_access_checks
    return if performed?

    require_external_identity
    return if performed?

    err = team_sync_setup_flow.setup_callback(
      provider_type: "azuread",
      params: azure_callback_params,
    )
    if err
      if err == :admin_consent_failed
        flash[:error] = ERROR_MESSAGES[:admin_consent_failed] % azure_callback_params.values_at("error", "error_description")
      else
        flash[:error] = ERROR_MESSAGES[err] % {err: err}
      end
      redirect_to team_sync_setup_enterprise_url(this_business)
      return
    end

    redirect_to team_sync_review_enterprise_path(this_business, token: token)
  end

  def review
    token = review_params[:token]

    if viewer_is_non_business_owner?
      if token.nil? || !team_sync_setup_flow.get_token(token).valid?
        render_404
        return
      end

      if !team_sync_setup_flow.review_required?
        flash[:error] = ERROR_MESSAGES[:invalid_state_transition]
        redirect_to team_sync_setup_enterprise_url(this_business, token: token)
        return
      end

      render_template_view "businesses/security_settings/team_sync/review_unprivileged",
        Businesses::SecuritySettings::TeamSync::ReviewPendingAssignmentView,
        layout: "session_authentication",
        team_sync_setup_flow: team_sync_setup_flow,
        business: this_business,
        business_name: this_business.name,
        tenant_name: team_sync_setup_flow.provider_id,
        token: token
      return
    end

    if !team_sync_setup_flow.review_required?
      flash[:error] = ERROR_MESSAGES[:invalid_state_transition]
      redirect_to team_sync_setup_enterprise_url(this_business, token: token)
      return
    end

    render_template_view "businesses/security_settings/team_sync/review",
      Businesses::SecuritySettings::TeamSync::ReviewPendingAssignmentView,
      layout: "session_authentication",
      team_sync_setup_flow: team_sync_setup_flow,
      business: this_business,
      business_name: this_business.name,
      tenant_name: team_sync_setup_flow.provider_id
  end

  def approve
    err = team_sync_setup_flow.approve
    if err
      flash[:error] = ERROR_MESSAGES[err] % {err: err}
      redirect_to team_sync_review_enterprise_url(this_business)
      return
    end
    redirect_to settings_security_enterprise_url(this_business)
  end

  def cancel
    err = team_sync_setup_flow.cancel
    if err
      flash[:error] = ERROR_MESSAGES[err] % {err: err}
      redirect_to team_sync_review_enterprise_url(this_business)
      return
    end
    flash[:notice] = "Team synchronization setup has been cancelled"
    redirect_to settings_security_enterprise_url(this_business)
  end

  def disable
    err = team_sync_setup_flow.disable
    if err
      flash[:error] = ERROR_MESSAGES[err] % {err: err}
      redirect_to team_sync_review_enterprise_url(this_business)
      return
    end
    flash[:notice] = "Team synchronization setup has been disabled"
    redirect_to settings_security_enterprise_url(this_business)
  end

  private

  def viewer_is_non_business_owner?
    return true if !logged_in?
    !this_business.owner?(current_user)
  end

  def azure_callback_params
    params.permit("admin_consent", "tenant", "state", "error", "error_description")
  end

  def setup_params
    params.permit("token", "slug")
  end

  def initiate_params
    params.permit("token", "utf8", "authenticity_token", "slug")
  end

  def review_params
    params.permit("token", "slug")
  end

  def team_sync_setup_flow
    @team_sync_setup_flow ||= ::TeamSync::SetupFlow.new(business: this_business, actor: current_user)
  end

  def require_external_identity
    return unless GitHub.external_identity_session_enforcement_enabled?
    return unless logged_in?
    return unless this_business.member?(current_user)
    return if current_external_identity(target: this_business).present?

    if request.xhr?
      head :unauthorized
    else
      render_external_identity_session_required
    end
  end

  # Internal: This before_action renders a standard 404 page if
  # `this_business` is nil.
  #
  # Returns nothing.
  def this_business_required
    render_404 if this_business.nil?
  end
end
