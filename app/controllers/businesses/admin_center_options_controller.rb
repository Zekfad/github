# frozen_string_literal: true

class Businesses::AdminCenterOptionsController < Businesses::BusinessController
  include ActionView::Helpers::TextHelper


  before_action :business_admin_required
  before_action :single_business_environment_required

  def index
    render "businesses/settings/admin_center_options", locals: {
      slug: this_business.slug,
    }
  end

  def change_allow_force_push
    val = params[:value].presence || "false"
    policy = params[:force_push_policy].present? || false

    if val == "_clear"
      GitHub.clear_force_push_rejection(current_user)
    else
      GitHub.set_force_push_rejection val, current_user, policy
      policy_level = policy ? "and enforced on all repositories" : "by default"
    end

    flash[:notice] = case val
    when "false"
      "Force pushing allowed #{policy_level}."
    when "all"
      "Force pushing blocked #{policy_level}."
    when "default"
      "Force pushing blocked on the default branch #{policy_level}."
    when "_clear"
      "Setting cleared. Instance default will be used."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_ssh_access
    val = params[:value].presence || false
    policy = params[:ssh_policy].present? || false

    if val == "_clear"
      GitHub.clear_ssh(current_user)
    else
      val == "true" ? GitHub.enable_ssh(current_user, policy) : GitHub.disable_ssh(current_user, policy)
      policy_level = policy ? "and enforced on all repositories" : "by default"
    end

    flash[:notice] = case val
    when "true" || true
      "Git SSH access enabled #{policy_level}."
    when "false" || false
      "Git SSH access disabled #{policy_level}."
    when "_clear"
      "Setting cleared. Instance default will be used."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_suggested_protocol
    val = params[:value]
    if val.in? %w(sticky http ssh)
      GitHub.set_suggested_protocol(val, current_user)
    else
      flash[:error] = "Invalid request."
    end

    flash[:notice] = case val
    when "sticky"
      "The last protocol selected by the user will be suggested."
    when "http"
      "HTTPS will be suggested."
    when "ssh"
      "SSH will be suggested."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_git_lfs_access
    val = params[:value]

    if val == "true"
      GitHub.enable_git_lfs(current_user)
      flash[:notice] = "Git LFS support enabled."
    elsif val == "false"
      GitHub.disable_git_lfs(current_user)
      flash[:notice] = "Git LFS support disabled."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_showcase_access
    val = params[:value]

    if val == "true"
      GitHub.enable_showcase(current_user)
      flash[:notice] = "Showcases enabled."
    elsif val == "false"
      GitHub.disable_showcase(current_user)
      flash[:notice] = "Showcases disabled."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_max_object_size
    val = params[:value].presence
    policy = params[:max_object_policy].present? || false

    policy_level = policy ? "and enforced on all repositories" : "by default"

    if val
      GitHub.set_max_object_size(val.to_i, @current_user, policy)
      if val.to_i == 0
        flash[:notice] = "Maximum object size set to unlimited #{policy_level}."
      else
        flash[:notice] = "Maximum object size set to #{val}MB #{policy_level}."
      end
    else
      flash[:error] = "Maximum object size value must be a positive integer or zero"
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_ldap_search_depth
    if depth = params[:search_depth].presence
      begin
        depth = Integer(depth)
        GitHub.config.set("ldap.search_strategy_depth", depth, current_user)
        flash[:notice] = "Search depth set to #{depth}."
      rescue ArgumentError
        flash[:error] = "Search depth must be a valid integer."
      end
    else
      GitHub.config.delete("ldap.search_strategy_depth", current_user)
      flash[:notice] = "Search depth cleared."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_org_creation
    val = params[:value]

    if val == "true"
      GitHub.enable_org_creation(current_user)
      flash[:notice] = "User organization creation enabled."
    elsif val == "false"
      GitHub.disable_org_creation(current_user)
      flash[:notice] = "User organization creation disabled."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_org_membership_visibility
    current_visibility = GitHub.default_org_membership_visibility
    currently_enforced = GitHub.default_org_membership_visibility_enforced?

    visibility = params[:value]
    enforce = params[:org_membership_visibility_enforcement].present?
    enforced_message = enforce ? " and enforced on organization members" : ""

    GitHub.set_default_org_membership_visibility(visibility, current_user, enforce)

    flash[:notice] = "Organization membership visibility is now #{visibility} by default#{enforced_message}."

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_reactivate_suspended
    val = params[:value]

    if val == "true"
      GitHub.config.enable("auth.reactivate-suspended", current_user)
      flash[:notice] = "User reactivation enabled."
    else
      GitHub.config.disable("auth.reactivate-suspended", current_user)
      flash[:notice] = "User reactivation disabled."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_reactivate_suspended_on_sync
    val = params[:value]

    if val == "true"
      GitHub.config.enable("auth.reactivate-suspended-on-sync", current_user)
      flash[:notice] = "User reactivation on sync enabled."
    else
      GitHub.config.disable("auth.reactivate-suspended-on-sync", current_user)
      flash[:notice] = "User reactivation on sync disabled."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def toggle_ldap_debugging
    val = params[:value]

    if val == "true"
      GitHub.config.enable("ldap.debug_logging_enabled", current_user)
      flash[:notice] = "LDAP debugging enabled."
    else
      GitHub.config.delete("ldap.debug_logging_enabled", current_user)
      flash[:notice] = "LDAP debugging disabled."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def toggle_saml_debugging
    val = params[:value]

    if val == "true"
      GitHub.config.enable("saml.debug_logging_enabled", current_user)
      flash[:notice] = "SAML debugging enabled."
    else
      GitHub.config.delete("saml.debug_logging_enabled", current_user)
      flash[:notice] = "SAML debugging disabled."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_repo_visibility
    # The true/false cases here just handle requests in-flight when this is
    # deployed and can be removed in the future.
    val = case params[:value]
    when "true" then "private"
    when "false" then "public"
    else params[:value]
    end
    GitHub.set_default_repo_visibility(val, current_user) # value is validated by model
    flash[:notice] = "New repositories are #{val} by default."
    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_cross_repo_conflict_editor
    val = params[:value]

    if val == "true"
      GitHub.enable_cross_repo_conflict_editor(current_user)
      flash[:notice] = "Conflict editor is now enabled for resolving conflicts between repositories."
    elsif val == "false"
      GitHub.disable_cross_repo_conflict_editor(current_user)
      flash[:notice] = "Conflict editor is now disabled for resolving conflicts between repositories."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_dormancy_threshold
    days = params[:dormancy_threshold_days]&.to_i ||
      Configurable::EnterpriseDormancyThreshold::DEFAULT_DORMANCY_THRESHOLD_DAYS

    if GitHub.set_enterprise_dormancy_threshold_days(days, current_user)
      flash[:notice] = "Dormancy threshold set to #{pluralize(days, "days")}."
    else
      flash[:error] = "Invalid dormancy threshold submitted."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_anonymous_git_access
    val = params[:value]

    if val == "true"
      GitHub.enable_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access can be enabled for repositories."
    elsif val == "false"
      GitHub.disable_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access can no longer be enabled for repositories."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end

  def change_anonymous_git_access_locked
    return render_404 unless GitHub.anonymous_git_access_enabled?

    val = params[:lock_setting]&.to_s
    if val == "true"
      GitHub.lock_anonymous_git_access(current_user, force: true)
      flash[:notice] = "Anonymous Git read access is now locked."
    else
      GitHub.unlock_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access is now unlocked."
    end

    redirect_to admin_center_options_enterprise_path(this_business)
  end
end
