# frozen_string_literal: true

class Businesses::EnterpriseInstallationsController < Businesses::BusinessController
  include EnterpriseInstallationsHelper

  before_action :business_admin_required
  before_action :require_valid_installation_token, only: %w(create)
  before_action :require_valid_state, only: %w(create)
  before_action :require_valid_hostname_in_data, only: %w(create)
  before_action :require_valid_server_id_in_data, only: %w(create)
  before_action :destroy_installations_for_server_uuid, only: %w(create)

  def index
    installations = this_business.connected_enterprise_installations.paginate(page: current_page)
    render "businesses/settings/enterprise_installations/index", locals: {
      installations: installations,
    }
  end

  def sync_user_accounts
    if params[:enterprise_installation_user_accounts_upload_id]
      installation = nil
      if params[:enterprise_installation_id]
        installation = this_business.enterprise_installations.find_by \
          id: params[:enterprise_installation_id]
      end

      EnterpriseInstallation.synchronize_user_accounts_data \
        business: this_business,
        installation: installation,
        upload_id: params[:enterprise_installation_user_accounts_upload_id],
        actor: current_user

      flash[:notice] = "Enterprise Server license usage import started."
    else
      flash[:error] = "No license usage file was uploaded."
    end
    redirect_back fallback_location: settings_billing_enterprise_path(this_business)
  end

  def create
    conflicting_installations(this_business, installations_data["host_name"]).destroy_all

    result = EnterpriseInstallation::Creator.perform(this_business,
      actor: current_user,
      server_data: installations_data,
    )

    if result.success?
      # TODO: reenable this once https://github.com/github/github/issues/111185 is fixed
      #set_github_app_icon(result.enterprise_installation, current_user)

      redirect_to complete_enterprise_installation_url(result.enterprise_installation, token: params[:token], state: params[:state])
    else
      flash[:error] = "Failed to connect #{installations_data["host_name"]} to the #{this_business.name} enterprise account."
      redirect_to new_enterprise_installation_url(token: params[:token], state: params[:state])
    end
  end
end
