# frozen_string_literal: true

class Businesses::Actions::PoliciesController < BusinessesController
  include ::Actions::ForkPrWorkflowsPolicyHelper

  map_to_service :actions_experience

  before_action :business_admin_required
  before_action :ensure_actions_enabled

  def update
    begin
      policy = Actions::PolicyResolver.perform(
        entity: this_business,
        policy: params[:policy]
      )

      this_business.update_action_execution_capabilities(policy, actor: current_user)
    rescue Configurable::ActionExecutionCapabilities::Error
      flash[:error] = "Sorry, there was an issue updating your settings."
    ensure
      redirect_to settings_actions_enterprise_path
    end
  end

  def update_fork_pr_workflows_policy
    return render_404 unless GitHub.flipper[:fork_pr_workflows_policy].enabled?(this_business)
    form_policy = params[:fork_pr_workflows_policy] || {}
    set_fork_pr_workflows_policy(this_business, form_policy)
  end

  def bulk
    orgs = this_business.organizations.where(id: params[:organization_ids])

    respond_to do |format|
      format.html do
        render Businesses::Actions::OrgBulkEnableComponent.new(
          business: this_business,
          organizations: orgs
        )
      end
    end
  end

  def bulk_update
    organizations = this_business.organizations.where(id: params[:organization_ids])
    enablement = params[:enablement]

    case enablement
    when "enabled"
      organizations.each { |org| org.allow_actions(actor: current_user) }
    when "disabled"
      organizations.each { |org| org.disallow_actions(actor: current_user) }
    end

    view = Businesses::Settings::ActionsPoliciesView.new(
      business: this_business,
      query: params["q"],
      current_page: current_page
    )

    render partial: "businesses/settings/actions/orgs", locals: { view: view }
  end

  def enable_org
    organization_name = params[:organization]
    enablement = params[:enablement]

    organization = this_business.organizations.find_by(name: organization_name)

    return unless organization.present?

    case enablement
    when "enabled"
      organization.allow_actions(actor: current_user)
    when "disabled"
      organization.disallow_actions(actor: current_user)
    end

    render Businesses::Actions::OrgRowComponent.new(business: this_business, organization: organization, enabled: organization.actions_allowed_by_owner?)
  end

  private

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end
end
