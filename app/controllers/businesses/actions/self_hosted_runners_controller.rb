# frozen_string_literal: true

class Businesses::Actions::SelfHostedRunnersController < Businesses::BusinessController
  include ::Actions::RunnersHelper
  include ::Actions::RunnerGroupsHelper

  map_to_service :actions_runners

  before_action :business_admin_required
  before_action :ensure_actions_enabled
  before_action :ensure_actions_enterprise_runners
  before_action :ensure_tenant_exists

  javascript_bundle :settings

  def index
    render "businesses/settings/actions/self_hosted_runners", locals: {
      runner_groups: Actions::RunnerGroup.for_entity(this_business, include_runners: true)
    }
  end

  def new
    render "businesses/settings/actions/add_runner", locals: {
      platform: add_runner_data[:platform],
      os: add_runner_data[:os],
      architecture: add_runner_data[:architecture],
      downloads: add_runner_data[:downloads],
      platform_options: add_runner_data[:platform_options],
      token: add_runner_data[:token],
      selected_download: add_runner_data[:selected_download]
    }
  end

  def destroy
    delete_status = delete_runner_for(this_business, id: params[:id].to_i, actor: current_user)

    if delete_status == "deleted"
      flash[:notice] = "Runner successfully deleted."
    else
      flash[:error] = "Sorry, there was a problem deleting your runner."
    end

    redirect_to settings_actions_self_hosted_runners_enterprise_path
  end

  def delete_runner_modal
    scope = this_business.runner_deletion_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: 1.hour.from_now)

    respond_to do |format|
      format.html do
        render Actions::Runners::DeleteModalComponent.new(
          owner_settings: Actions::EnterpriseRunnersView.new(settings_owner: this_business, current_user: current_user),
          runner_id: params[:id],
          runner_os: params[:os],
          token: token,
        )
      end
    end
  end

  private

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end

  def ensure_actions_enterprise_runners
    render_404 unless GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) || GitHub.enterprise?
  end

  def ensure_tenant_exists
    result = GrpcHelper.rescue_from_grpc_errors("EnterpriseTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(this_business)
    end
    raise Timeout::Error, "Timeout fetching tenant" unless result.call_succeeded?
  end

  def add_runner_data
    return @add_runner_data if defined?(@add_runner_data)

    downloads = downloads_for(this_business)

    scope = this_business.runner_creation_token_scope
    expires_at = 1.hour.from_now
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)

    @add_runner_data = runner_options(downloads: downloads, token: token)
  end

end
