# frozen_string_literal: true

class Businesses::Actions::RunnerLabelsController < Businesses::BusinessController
  areas_of_responsibility :actions

  include ::Actions::RunnersHelper

  before_action :login_required
  before_action :business_admin_required
  before_action :ensure_actions_enabled
  before_action :ensure_actions_enterprise_runners

  def index
    respond_to do |format|
      format.html do
        render Actions::RunnerLabelsComponent.new(
          owner: this_business,
          runner_ids: [params[:runner_id]],
          labels: labels_for(this_business),
          selected_labels: params[:applied_labels]
        )
      end
    end
  end

  def create
    label = create_label_for(this_business, name: params[:name])

    respond_to do |wants|
      wants.html do
        if label
          render Actions::RunnerLabelComponent.new(label: label, selected: false)
        else
          errorMessage = "Sorry, there was a problem adding your label."
          render json: { message: errorMessage }, status: :unprocessable_entity
        end
      end
    end
  end

  def update
    labels = Array(params[:labels])
    affected_runner = update_label_for(this_business, runner_id: params[:runner_id], labels: labels)

    unless affected_runner
      flash[:error] = "Sorry, there was a problem updating your labels"
    end

    owner_settings = Actions::EnterpriseRunnersView.new(settings_owner: this_business, current_user: current_user)

    render Actions::RunnerComponent.new(
      runner: affected_runner,
      owner_settings: owner_settings,
      hidden: false,
      is_child_row: true
    )
  end

  private

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end

  def ensure_actions_enterprise_runners
    render_404 unless GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) || GitHub.enterprise?
  end
end
