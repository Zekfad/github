# frozen_string_literal: true

class Businesses::PrivateInstanceSupportController < Businesses::BusinessController
  before_action :business_admin_required
  before_action :private_instance_required

  def index
    render "businesses/private_instance_support/index"
  end
end
