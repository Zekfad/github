# frozen_string_literal: true

class Businesses::ProfilesController < Businesses::BusinessController

  before_action :business_admin_required

  def show
    render "businesses/settings/profile"
  end

  def update
    if this_business.update(profile_params)
      redirect_to settings_profile_enterprise_path(this_business),
        notice: "Got it. The profile has been updated."
    else
      flash.now[:error] = this_business.errors.full_messages.join(", ")
      render "businesses/settings/profile"
    end
  end

  private

  def profile_params
    @profile_params ||= params.require(:business).permit(:name, :description, :website_url, :location)
  end
end
