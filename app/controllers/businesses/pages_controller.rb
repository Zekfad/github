# frozen_string_literal: true

class Businesses::PagesController < Businesses::BusinessController

  before_action :business_admin_required
  before_action :private_instance_required
  before_action :check_for_in_progress_config_run, only: %w(update_pages_settings)

  def index
    render "businesses/settings/pages"
  end

  def update_pages_settings
    pages_enabled = params[:pages_enabled] == "1"
    public_pages = params[:public_pages] == "1"
    if save_pages_settings(pages_enabled, public_pages)
      redirect_to settings_pages_enterprise_path(this_business), notice: "GitHub Pages settings saved."
    else
      flash[:error] = "There was an error saving the GitHub Pages settings."
      redirect_to settings_pages_enterprise_path(this_business)
    end
  end

  private

  def save_pages_settings(pages_enabled, public_pages)
    return true unless pages_settings_changed?(pages_enabled, public_pages)

    # Public pages cannot be enabled if pages is disabled
    public_pages = false unless pages_enabled
    settings = {
      kv_hash: {
        GitHub::Config::Pages::PAGES_ENABLED_KEY => pages_enabled ? "true" : "false",
        GitHub::Config::Pages::PUBLIC_PAGES_KEY => public_pages ? "true" : "false",
      },
      enterprise_hash: {
        "enterprise": {
          "public_pages": public_pages,
          "pages": {
            "enabled": pages_enabled
          }
        }
      }
    }
    GitHub.enterprise_configuration_updater.update_configuration!(settings: settings)
  end

  def pages_settings_changed?(pages_enabled, public_pages)
    return true if pages_enabled != GitHub.pages_enabled?
    return true if public_pages != GitHub.public_pages?
    false
  end
end
