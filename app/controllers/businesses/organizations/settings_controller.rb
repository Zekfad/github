# frozen_string_literal: true

class Businesses::Organizations::SettingsController < Businesses::BusinessController

  before_action :business_admin_required
  before_action :only_accept_xhr_requests
  before_action :dependency_insights_required, only: [:members_can_view_dependency_insights]

  def default_repository_permission
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/default_repository_permission",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/default_repository_permission"
      end
    end
  end

  def repository_creation
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/repository_creation",
        formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/repository_creation"
      end
    end
  end

  def allow_private_repository_forking
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/allow_private_repository_forking",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/allow_private_repository_forking"
      end
    end
  end

  def members_can_invite_collaborators
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/members_can_invite_collaborators",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/members_can_invite_collaborators"
      end
    end
  end

  def members_can_change_repository_visibility
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/members_can_change_repository_visibility",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/members_can_change_repository_visibility"
      end
    end
  end

  def members_can_delete_repositories
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/members_can_delete_repositories",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/members_can_delete_repositories"
      end
    end
  end

  def members_can_delete_issues
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/members_can_delete_issues",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/members_can_delete_issues"
      end
    end
  end

  def action_execution_capability
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/action_execution_capability",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/action_execution_capability"
      end
    end
  end

  def members_can_update_protected_branches
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/members_can_update_protected_branches",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/members_can_update_protected_branches"
      end
    end
  end

  def team_discussions
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/team_discussions",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/team_discussions"
      end
    end
  end

  def organization_projects
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/organization_projects",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/organization_projects"
      end
    end
  end

  def repository_projects
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/repository_projects",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/repository_projects"
      end
    end
  end

  def two_factor_required
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/two_factor_required",
               formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/two_factor_required"
      end
    end
  end

  def saml_identity_provider
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/saml_identity_provider",
               formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/saml_identity_provider"
      end
    end
  end

  def members_can_view_dependency_insights
    respond_to do |format|
      format.html_fragment do
        render partial: "businesses/organizations/settings/dependency_insights",
          formats: :html
      end
      format.html do
        render partial: "businesses/organizations/settings/dependency_insights"
      end
    end
  end

  private

  def only_accept_xhr_requests
    return if request.xhr?

    respond_to do |format|
      format.html { return head :not_acceptable }
    end
  end
end
