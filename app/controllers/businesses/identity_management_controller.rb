# frozen_string_literal: true

class Businesses::IdentityManagementController < Businesses::BusinessController
  include BusinessesHelper


  before_action :business_saml_required
  before_action :sso_provider_required

  # The following actions do not require conditional access checks:
  # - sso: serves `/enterprises/:slug/sso`, serves as the prompt to SSO and create the
  #   require external identity session for protected endpoints.
  # - sso_sign_up: serves `/enterprises/:slug/sso/signup`, same as above but for
  #   users that need to sign up first.
  skip_before_action :perform_conditional_access_checks, only: %w(
    sso
    sso_sign_up
    sso_status
    sso_modal
    sso_complete
  )

  skip_before_action :private_instance_bootstrap_check, only: %w(sso)

  def sso
    allow_external_redirect_after_post(provider: this_business.saml_provider)

    url = idm_saml_initiate_enterprise_url(this_business,
      authorization_request: params[:authorization_request],
      return_to: params[:return_to],
    )

    render_template_view "businesses/identity_management/sso",
      Businesses::IdentityManagement::SingleSignOnView,
      {
        business: this_business,
        initiate_sso_url: url,
        credential_authorization_request: credential_authorization_request,
      },
      layout: "session_authentication"
  end

  def sso_sign_up
    unless session[:unlinked_external_identity_id]
      redirect_to business_idm_sso_enterprise_path(this_business)
      return
    end

    if logged_in?
      unlinked_id = session.delete(:unlinked_external_identity_id)
      unlinked = this_business.saml_provider.external_identities.find(unlinked_id)
      return_to = session.delete(:sso_return_to) || enterprise_path(this_business)

      ActiveRecord::Base.connected_to(role: :writing) do
        result = if GitHub.flipper[:enterprise_idp_provisioning].enabled?(this_business)
          Platform::Provisioning::IdentityProvisioner.provision_and_add_member \
            target: this_business,
            user_id: current_user.id,
            user_data: unlinked.saml_user_data,
            mapper: Platform::Provisioning::SamlMapper
        else
          Platform::Provisioning::IdentityProvisioner.provision \
            target: this_business,
            user_id: current_user.id,
            user_data: unlinked.saml_user_data,
            mapper: Platform::Provisioning::SamlMapper
        end

        if result.success?
          update_or_create_external_identity_session result.external_identity,
            expires_at: session[:unlinked_session_expires_at]

          flash[:notice] = "Welcome to the #{ this_business.name } enterprise."
        else
          flash[:error] = "There was an issue joining the enterprise: #{ result.errors.full_messages.to_sentence }"
        end
      end

      redirect_to_return_to(fallback: return_to)
    else
      render_template_view "businesses/identity_management/sign_up_via_sso",
        Businesses::IdentityManagement::SingleSignOnView,
        { business: this_business },
        layout: "session_authentication"
    end
  end

  # Action: Renders a partial prompting the user to renew their single sign-on
  # session. This will be shown to the user via a dialog when attempting an
  # XHR action with an expired SSO session.
  def sso_modal
    url = idm_saml_initiate_enterprise_url(this_business, return_to: business_idm_sso_complete_enterprise_url(this_business))

    render_template_view "businesses/identity_management/sso_modal",
      Businesses::IdentityManagement::SingleSignOnView,
      {
        business: this_business,
        initiate_sso_url: url,
      },
      layout: false
  end

  # Action: The landing page users are redirected to after completing
  # SSO initiated from a client-side modal.
  #
  # Since SSO takes place in a new window, this page is responsible for
  # communicating status with the original window and closing itself.
  #
  # If communication with original window fails, the window will try to
  # automatically redirect to a fallback URL. If that too fails, the user will
  # be asked to click a link directing them to the fallback URL.
  def sso_complete
    render_template_view "businesses/identity_management/sso_complete",
      Businesses::IdentityManagement::SingleSignOnCompleteView,
      view_attrs = {
        business: this_business,
        saml_error: flash[:saml_error],
        fallback_url: enterprise_path(this_business),
      },
      layout: "session_authentication"
  end

  # Action: Returns a JSON response indicating whether or not the current user
  # has a valid single sign on session for this organization.
  def sso_status
    return render_404 unless request.format.json?

    session_present =
      if params[:fakestate] == "promptssomodal"
        false
      else
        required_external_identity_session_present?(target: this_business)
      end

    render json: session_present
  end

  private

  def sso_provider_required
    render_404 unless this_business&.saml_sso_enabled?
  end

  def credential_authorization_request
    return unless token = params[:authorization_request].presence
    return unless logged_in?

    Organization::CredentialAuthorization.consume_request(
      target: this_business,
      token: token,
      actor: current_user,
    )
  end
end
