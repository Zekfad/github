# frozen_string_literal: true

class Businesses::ActionsSettingsController < Businesses::BusinessController
  map_to_service :actions_experience

  before_action :business_admin_required
  before_action :ensure_actions_enabled

  def show
    view = Businesses::Settings::ActionsPoliciesView.new(
      business: this_business,
      query: params["q"],
      current_page: current_page
    )

    if request.xhr?
      headers["Cache-Control"] = "no-cache, no-store"
      render partial: "businesses/settings/actions/orgs", locals: { view: view }
    else
      render "businesses/settings/actions", locals: { view: view }
    end
  end

  private

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end
end
