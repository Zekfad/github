# frozen_string_literal: true

class Businesses::HooksController < Businesses::BusinessController
  areas_of_responsibility :admin_experience, :webhook

  before_action :business_admin_required

  include HooksControllerMethods # All Hook related actions

  # include admin bundles for pre-receive hooks code
  if GitHub.single_business_environment?
    stylesheet_bundle :admin
    javascript_bundle :admin
  end

  private

  # Business hooks
  def current_context
    this_business
  end

  def default_events
    return %w(*) if GitHub.single_business_environment?
    super
  end

  def pre_receive_targets_with_hook
    @pre_receive_targets_with_hook ||= PreReceiveHookTarget.where(hookable: GitHub.global_business)
                                                           .includes(:hook)
                                                           .all
  end
  helper_method :pre_receive_targets_with_hook

end
