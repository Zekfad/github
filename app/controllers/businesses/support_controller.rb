# frozen_string_literal: true

class Businesses::SupportController < Businesses::BusinessController

  before_action :business_admin_required
  before_action :dotcom_required, only: %w(
    entitlee_suggestions create_entitlee delete_entitlee
  )
  before_action :private_instance_required, only: %w(update_support_link)
  before_action :check_for_in_progress_config_run, only: %w(update_support_link)

  def index
    if !GitHub.private_instance? && GitHub.single_business_environment?
      return render_404
    end

    render "businesses/settings/support"
  end

  def update_support_link
    type = params["internal_support_contact_type"]
    link = type == "url" ? params["internal_support_contact_url"] : params["internal_support_contact_email"]
    errors = validate_support_link(type, link)
    if errors.any?
      flash[:error] = errors.to_sentence
      return redirect_to settings_support_enterprise_path(this_business)
    end

    if save_support_link(link, type)
      redirect_to settings_support_enterprise_path(this_business), notice: "Support link set."
    else
      flash[:error] = "There was an error setting the support link."
      redirect_to settings_support_enterprise_path(this_business)
    end
  end

  def entitlee_suggestions
    render_partial_view "businesses/settings/support_suggestions",
      Businesses::Settings::SupportSuggestionsView,
      business: this_business,
      query: params[:q]
  end

  def create_entitlee
    if user = User.find_by(login: params[:user])
      this_business.add_support_entitlee(user, actor: current_user)
      redirect_to settings_support_enterprise_path, notice: "Updated."
    else
      flash.now[:error] = "Something went wrong."
      render "businesses/settings/support", locals: { entitlees: this_business.support_entitlees}
    end
  end

  def delete_entitlee
    if user = User.find_by_id(params[:id].to_i)
      this_business.remove_support_entitlee(user, actor: current_user)
      redirect_to settings_support_enterprise_path, notice: "Updated."
    else
      flash.now[:error] = "Something went wrong."
      render "businesses/settings/support", locals: { entitlees: this_business.support_entitlees}
    end
  end

  private

  def save_support_link(link, type)
    return true unless support_link_changed?(link, type)

    settings = {
      kv_hash: {
        GitHub::Config::SupportLink::SUPPORT_LINK_KEY => link,
        GitHub::Config::SupportLink::SUPPORT_LINK_TYPE_KEY => type
      },
      enterprise_hash: {
        "enterprise": {
          "smtp": {
            "support_address": link,
            "support_address_type": type
          }
        }
      }
    }
    GitHub.enterprise_configuration_updater.update_configuration!(settings: settings)
  end

  def support_link_changed?(link, type)
    return true if type != GitHub.support_link_type
    return true if link != GitHub.support_link
    false
  end
end
