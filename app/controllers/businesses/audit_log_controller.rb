# frozen_string_literal: true

class Businesses::AuditLogController < Businesses::BusinessController
  include AuditLogExportHelper

  before_action :business_admin_required
  before_action :audit_log_export_required, only: [:export, :create_export]
  before_action -> { audit_log_git_event_export_required(this_business) }, only: [:git_export, :create_git_export]

  def index
    render_template_view "businesses/audit_log/index",
      Businesses::AuditLog::IndexPageView,
      business: this_business,
      query: params[:q],
      page: current_page,
      git_export_enabled: audit_log_git_event_export_enabled?(this_business),
      after: params[:after],
      before: params[:before]
  end

  def suggestions
    headers["Cache-Control"] = "no-cache, no-store"

    respond_to do |format|
      format.html do
        render_partial_view "businesses/audit_log/suggestions",
          Businesses::AuditLog::SuggestionsView,
          business: this_business
      end
    end
  end

  def export
    export = this_business.audit_log_exports.find_by_token!(params[:token])
    render_audit_log_export(export)
  end

  def create_export
    options = {
      actor: current_user,
      phrase: params[:q],
      format: params[:export_format],
    }

    export = this_business.audit_log_exports.create(options)
    respond_with_audit_log_export \
      export: export,
      export_url: settings_audit_log_export_enterprise_url(
        token: export.token, format: export.format,
      )
  end

  def git_export
    export = this_business.audit_log_git_event_exports.find_by!(actor_id: current_user.id, token: params[:token])
    render_audit_log_export(export)
  end

  def create_git_export
    options = {
      actor: current_user,
      start: parse_user_time(params[:start]),
      end: parse_user_time(params[:end]),
    }

    export = this_business.audit_log_git_event_exports.create(options)
    respond_with_audit_log_export \
      export: export,
      export_url: settings_audit_log_git_event_export_enterprise_url(
        token: export.token,
      )
  end

  def parse_user_time(param)
     zone = current_user&.time_zone || Time.zone
     zone.parse(param)
  end
end
