# frozen_string_literal: true

class CommitsController < GitContentController
  PAGE_SIZE = 35
  statsd_tag_actions only: :show

  helper :diff
  layout :repository_layout

  skip_before_action :try_to_expand_path
  skip_before_action :cap_pagination

  def show
    respond_to do |format|
      format.html do
        show_html
      end
      format.atom do
        show_feed
      end
    end
  end

  def show_html
    fix_up_pagination_params!

    if commit_oid = current_repository.ref_to_sha(tree_name)
      # this raises ObjectMissing when the commit does not exist
      current_repository.commits.find(commit_oid)
    end

    render_show_graphql_html
  rescue GitRPC::ObjectMissing, Platform::Errors::Cursor
    render_404
  end

  def show_feed
    variables = {
      "id"        => current_repository.global_relay_id,
      "revision"  => params[:name] || current_repository.default_branch,
      "path"      => path_string.presence,
      "author"    => author_input,
    }

    etag_data = platform_execute(Views::Commits::Feed::ETagQuery, variables: variables)

    # TODO: Cleanup this template's GraphQL usage
    GraphQL::Client.allow_noncollocated_callers do
      if etag = etag_data.node.try(:commit).try(:history).try(:nodes).try(:[], 0).try(:oid)
        fresh_when strong_etag: etag, template: false

        return if performed?
      else
        return render_404
      end

      data = platform_execute(Views::Commits::Feed::RootQuery, variables: variables)

      render "commits/feed", layout: false, locals: {
        data: Views::Commits::Feed::RootQuery.new(data),
      }
    end
  end

  # Handle a request timeout in the show action for any of the supported formats.
  rescue_from_timeout only: [:show] do |boom|
    respond_to do |format|
      format.html do
        render "commits/timeout"
      end

      format.atom do
        render status: 503,
          plain: "Sorry, this feed is taking too long to generate.",
          layout: false
      end

      format.all do
        head 503
      end
    end
  end

private
  def fix_up_pagination_params!
    if page = params.delete(:page)
      if page.to_i > 1
        params[:after] = Platform::ConnectionWrappers::CursorGenerator.generate_cursor((((page.to_i - 1) * PAGE_SIZE) - 1).to_s)
      end
    end
  end

  ShowCommitsQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $revision: String!, $first: Int, $after: String, $last: Int, $before: String,
          $path: String, $author: CommitAuthor, $since: GitTimestamp, $until: GitTimestamp) {
      node(id: $id) {
        ...Views::Commits::Show::RepositoryCommits
      }
    }
  GRAPHQL

  def render_show_graphql_html
    path = path_string.presence

    variables = {
      "id"       => current_repository.global_relay_id,
      "revision" => params[:name] || current_repository.default_branch,
      "path"     => path,
      "author"   => author_input,
      "since"    => parsed_since,
      "until"    => parsed_until,
    }

    variables.merge!(graphql_pagination_params(page_size: PAGE_SIZE))

    begin
      data = platform_execute(ShowCommitsQuery, variables: variables)
    rescue PlatformHelper::ExecutionError
      return render_404
    end

    commits_data = Views::Commits::Show::RepositoryCommits.new(data.node)

    # TODO there must be a better way of checking history without repeating the entire field
    #   in the controller and without disabling the noncollocated_callers check 🤔
    GraphQL::Client.allow_noncollocated_callers do
      no_filters_present = [:after, :before, :author, :since, :until].none? { |key| params[key].present? }
      history_present = commits_data.commit&.history&.nodes&.any?
      return render_404 if !history_present && no_filters_present
    end

    render "commits/show", locals: { repository_commits: commits_data, current_blob_path: path, from_profile: coming_from_profile? }
  end

  def author_input
    if params[:author]
      author = user_or_author_to_filter_on(params[:author])

      case author
      when ::User
        { "id" => author.global_relay_id }
      when ::String
        { "emails" => [author] }
      when ::Array
        { "emails" => author.map(&:to_s) }
      end
    end
  end

  def parsed_since
    date = parse_date(params[:since]) or return
    date.at_beginning_of_day.utc.iso8601
  end

  def parsed_until
    date = parse_date(params[:until]) or return
    # the `until` date is inclusive, so we go until the beginning of the next day
    date += 1.day
    date.at_beginning_of_day.utc.iso8601
  end

  # Parses a ISO8601 date from a param string into a TimeWithZone at 00:00:00
  # in the current user's time zone
  def parse_date(str)
    date = begin
      Date.iso8601(str)
    rescue ArgumentError
      return
    end
    date.in_time_zone(current_user&.time_zone || Time.zone)
  end

  # Handle auth specifics for feed requests.
  include GitHub::Authentication::Feed

  # Private: Actions that can response to atom requests.
  #
  # Returns an Array or Strings.
  def feed_actions
    %w(show)
  end

  # Private: The commits for the current URL and query params.
  #
  # Returns an Array.
  def commits_list
    options = {}
    user    = user_or_author_to_filter_on(params[:author])
    options[:author] = user
    options[:path]   = path_string if params[:path]

    if date_str = parsed_since
      options[:since] = date_str
    end
    if date_str = parsed_until
      options[:until] = date_str
    end

    # params[:page] is sometimes a single element array for some reason
    page = Array(params[:page]).first || 1
    commits =
      begin
        current_repository.
          paged_commits(tree_name, page, PAGE_SIZE, options)
      rescue GitRPC::ObjectMissing
        []
      rescue GitRPC::CommandFailed, Repository::CommandFailed => boom
        raise boom if !boom.err.to_s.include?("bad object")
        []
      end

    # replace ?author login with their email so the user sees the actual query
    if commits.blank? && logged_in? && user == current_user
      params[:author] = user.email
    end

    # reduce round trips finding repositories
    commits.each { |commit| commit.repository = current_repository }

    # prefill the local cache wrapper with a multiget
    cache_keys =  commits.map { |c| c.message_cache_key("short_message_html") }
    cache_keys += commits.map { |c| c.message_cache_key("message_body_html")  }

    GitHub.cache.get_multi(cache_keys) if cache_keys.any?

    commits
  end

  # Private: find a User via login, or just return the search
  # string that is passed in (which is the case where the user is
  # searching for an email)
  #
  # Returns a User or a String
  def user_or_author_to_filter_on(author)
    User.find_by_login(author) || author
  end

  # Private: determines if the referral is coming from the
  # user profile page
  #
  # Returns a Boolean
  def coming_from_profile?
    referring_route = github_internal_referrer_route

    return false if referring_route.nil?

    referring_route[:controller] == "profiles" && referring_route[:action] == "show"
  end

  def route_supports_advisory_workspaces?
    true
  end
end
