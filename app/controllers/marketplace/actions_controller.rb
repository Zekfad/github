# frozen_string_literal: true

class Marketplace::ActionsController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  layout "marketplace"

  ShowQuery = parse_query <<-'GRAPHQL'
    query($slug: String!, $selectedVersion: String!, $contributorsCount: Int!) {
      repositoryAction: repositoryActionBySlug(slug: $slug) {
        isListed
        latestRelease(published: true)

        selectedRelease: release(tagName: $selectedVersion) {
          tagName
        }
      }

      ...Views::Marketplace::Actions::Show::Root
      ...Views::Marketplace::Actions::Sidebar::Root
    }
  GRAPHQL

  def show
    selected_version = params[:version] || ""

    data = platform_execute(
      ShowQuery,
      variables: {
        slug: params[:slug],
        selectedVersion: selected_version,
        contributorsCount: 12,
      },
    )

    local_variables = {
      data: data,
      selected_version: selected_version,
    }

    action = data.repository_action
    return render_404 unless (action&.is_listed? && action.latest_release.present?)

    if selected_version.present? && action&.selected_release.nil?
      flash[:error] = "Sorry, we couldn’t find that version of this Action. Here’s the latest version."
      redirect_to marketplace_action_path(params[:slug]) and return
    end

    render "marketplace/actions/show", locals: local_variables
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DelistRepositoryActionInput!) {
      delistRepositoryAction(input: $input) {
        repositoryAction {
          name
        }
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DestroyQuery, variables: { input: { slug: params[:slug] } })

    if data.errors.any?
      error_type = data.errors.details[:delistRepositoryAction]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")

      return render json: { error: message }, status: :unprocessable_entity
    end

    redirect_to marketplace_path, notice: "Okay, #{data.delist_repository_action.repository_action.name} has been delisted."
  end

  private

  def require_conditional_access_checks?
    action_name == "destroy"
  end

  def target_for_conditional_access
    repository_action = RepositoryAction.find_by(slug: params[:slug])
    return :no_target_for_conditional_access unless repository_action
    repository_action.owner
  end
end
