# frozen_string_literal: true

class Marketplace::PendingOrdersController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required
  before_action :login_required

  layout "marketplace"

  IndexQuery = parse_query <<-'GRAPHQL'
    query($loggedIn: Boolean!) {
      ...Views::Marketplace::PendingOrders::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: { loggedIn: logged_in? })

    render "marketplace/pending_orders/index", locals: { data: data }
  end

  private

  def require_conditional_access_checks?
    false
  end
end
