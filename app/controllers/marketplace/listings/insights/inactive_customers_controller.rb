# frozen_string_literal: true

class Marketplace::Listings::Insights::InactiveCustomersController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :login_required
  before_action :marketplace_required
  before_action :marketplace_pending_installations_required
  before_action :permission_to_edit_listing_required

  def index
    respond_to do |format|
      format.csv do
        report = Marketplace::InactiveCustomersReport.new(listing: listing)
        send_data(report.as_csv, type: "text/csv", filename: report.filename)
      end

      format.all { render_404 }
    end
  end

  private

  def permission_to_edit_listing_required
    render_404 unless listing.allowed_to_edit?(current_user)
  end

  def listing
    @listing ||= Marketplace::Listing.find_by_slug(params[:listing_slug])
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless listing
    listing.owner
  end
end
