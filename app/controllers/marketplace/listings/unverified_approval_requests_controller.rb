# frozen_string_literal: true

class Marketplace::Listings::UnverifiedApprovalRequestsController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required
  before_action :login_required

  RequestApprovalQuery = parse_query <<-'GRAPHQL'
    mutation($input: RequestUnverifiedMarketplaceListingApprovalInput!) {
      requestUnverifiedMarketplaceListingApproval(input: $input) {
        marketplaceListing {
          resourcePath
        }
      }
    }
  GRAPHQL

  def create
    listing_slug = params[:listing_slug]
    variables = { input: { slug: listing_slug } }

    data = platform_execute(RequestApprovalQuery, variables: variables)

    if data.errors.any?
      error_type = data.errors.details[:requestUnverifiedMarketplaceListingApproval]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
    else
      flash[:notice] = "Thank you for your submission. We will review your listing and get back to " +
                       "you shortly."
    end

    redirect_to marketplace_listing_path(listing_slug)
  end

  private

  def target_for_conditional_access
    listing = Marketplace::Listing.find_by_slug(params[:listing_slug])
    return :no_target_for_conditional_access unless listing
    listing.owner
  end
end
