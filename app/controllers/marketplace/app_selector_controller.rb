# frozen_string_literal: true

class Marketplace::AppSelectorController < ApplicationController
  include GitHub::RateLimitedRequest

  layout false

  before_action :marketplace_required

  DefaultSuggestionsQuery = parse_query <<-'GRAPHQL'
    query($categorySlug: String!, $hasCategory: Boolean!) {
      ...Views::Marketplace::AppSelector::Index::Root
    }
  GRAPHQL

  SearchSuggestionsQuery = parse_query <<-'GRAPHQL'
    query($query: String!, $categorySlug: String) {
      ...Views::Marketplace::AppSelector::SearchResults::Root
    }
  GRAPHQL

  # Protect this endpoint from spamming since searches hit Elastic Search
  rate_limit_requests(only: :index, max: 25, if: :app_selector_rate_limit_filter,
                      key: :app_selector_rate_limit_key, log_key: :app_selector_rate_limit_log_key)

  def index
    search_phrase = params[:q]
    category_slug = params[:category_slug].to_s

    if search_phrase.present?
      headers["Cache-Control"] = "no-cache, no-store"

      data = platform_execute(SearchSuggestionsQuery, variables: {
        query: search_phrase,
        categorySlug: category_slug,
        hasCategory: category_slug.present?,
      })

      render partial: "marketplace/app_selector/search_results",
        locals: { data: data, search_phrase: search_phrase, category_slug: category_slug }, formats: :html
    else
      data = platform_execute(DefaultSuggestionsQuery, variables: {
        categorySlug: category_slug,
        hasCategory: category_slug.present?,
      })

      render "marketplace/app_selector/index",
        locals: { data: data, category_slug: category_slug }, formats: :html
    end
  end

  private

  def app_selector_rate_limit_filter
    params[:q].present?
  end

  def app_selector_rate_limit_key
    "marketplace_app_suggestions_search_limiter:#{logged_in? ? current_user.id : request.remote_ip}"
  end

  def app_selector_rate_limit_log_key
    logged_in_or_anon = logged_in ? "logged_in" : "anon"
    "marketplace_app_suggestions_search_limiter:#{logged_in_or_anon}"
  end

  # App selector is always public
  def require_conditional_access_checks?
    false
  end
end
