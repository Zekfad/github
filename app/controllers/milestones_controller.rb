# frozen_string_literal: true

class MilestonesController < AbstractRepositoryController
  CLOSED_PER_PAGE = 25
  OPEN_PER_PAGE = 100
  MAX_PER_PAGE = GitHub::Prioritizable::MAXIMUM_PRIORITIZABLE_ITEM_COUNT

  before_action :writable_repository_required, except: [:index, :show, :issues, :paginated_issues]
  before_action :milestone_must_exist, except: %w(create index new)
  before_action :modifiers_only, except: [:index, :show, :issues, :paginated_issues]
  before_action :set_client_uid, only: [:prioritize]
  layout :repository_layout

  def index
    @milestones = current_repository.milestones

    @counts = {
      open: current_repository.milestones.open_milestones.count,
      closed: current_repository.milestones.closed_milestones.count,
    }

    case params[:state]
    when "open"
      @milestones = @milestones.open_milestones
    when "closed"
      @milestones = @milestones.closed_milestones
    else
      @milestones = @milestones.open_milestones
    end

    @counts[:with_issues] = @milestones.with_issues.count
    @counts[:without_issues] = @milestones.without_issues.count

    @milestones = @milestones.sorted_by(params[:sort], params[:direction])

    render "milestones/index"
  end

  def new
    @milestone = current_repository.milestones.new
    render "milestones/new"
  end

  def create
    @milestone = current_repository.milestones.build(milestone_params)
    @milestone.created_by = current_user
    @milestone.repository = current_repository

    if @milestone.save
      redirect_to milestones_path(current_repository.owner, current_repository) + "?with_issues=no"
    else
      flash.now[:error] = @milestone.errors.full_messages.to_sentence
      render "milestones/new"
    end
  end

  def show
    render_template_view "milestones/show", Milestones::ShowView,
      milestone: current_milestone,
      issues: current_milestone_issues,
      showing_closed: params[:closed] == "1"
  end

  def issues
    respond_to do |format|
      format.html do
        render_partial_view "milestones/issue_list", Milestones::ShowView,
          milestone: current_milestone,
          issues: current_milestone_issues,
          showing_closed: params[:closed] == "1"
      end
    end
  end

  def paginated_issues
    respond_to do |format|
      format.html do
        render_partial_view "milestones/paginated_issue_list", Milestones::ShowView,
          milestone: current_milestone,
          issues: current_milestone_issues(per_page: OPEN_PER_PAGE)
      end
    end
  end

  def edit
    render "milestones/edit"
  end

  def toggle
    if current_milestone.toggle_state!
      redirect_to milestones_path(current_repository.owner, current_repository)
    else
      render "milestones/index"
    end
  end

  def update
    return render "milestones/edit" unless current_milestone

    if current_milestone.update(milestone_params)
      redirect_to milestones_path(current_repository.owner, current_repository)
    else
      render "milestones/edit"
    end
  end

  def destroy
    current_milestone.destroy
    flash[:notice] = "Milestone deleted"

    if request.xhr?
      head 200
    else
      redirect_to milestones_path(current_repository.owner, current_repository)
    end
  end

  def prioritize
    if params[:timestamp].to_i != current_milestone.updated_at.to_i
      GitHub.dogstats.increment "issue", tags: ["action:prioritize", "via:web", "error:outdated"]
      respond_to do |format|
        format.json { render json: { error: "outdated" } }
      end
      return
    end

    # This shouldn't happen (the UI is disabled), so at least we won't 500.
    unless current_milestone.prioritizable?
      GitHub.dogstats.increment "issue", tags: ["action:prioritize", "via:web", "error:too_many"]
      respond_to do |format|
        format.json { render json: { error: "too_many" } }
      end
      return
    end

    issue = current_milestone.issues.find(params[:item_id])

    begin
      if params[:prev_id].present?
        after = current_milestone.issues.find(params[:prev_id])
        current_milestone.prioritize_issue!(issue, after: after)
      else
        current_milestone.prioritize_issue!(issue, position: :top)
      end
    rescue GitHub::Prioritizable::Context::LockedForRebalance
      GitHub.dogstats.increment("milestones.exceptions.locked_for_rebalance", { tags: ["context:milestones_controller.prioritize"] })
      return render status: 503, plain: "Sorry! This milestone is temporarily locked for maintenance. Please try again."
    end

    GitHub.dogstats.increment "issue", tags: ["action:prioritize", "via:web", "response:success"]

    respond_to do |format|
      format.json do
        render json: {
          updated_at: current_milestone.reload.updated_at.to_i,
        }
      end
    end
  end

  protected

    def current_milestone
      @current_milestone ||=
        if params[:id]
          current_repository.milestones.find_by_number(params[:id])
        elsif params[:number]
          current_repository.milestones.find_by_number(params[:number])
        elsif params[:slug]
          current_repository.milestones.find_by_slug(params[:slug])
        end
    end
    helper_method :current_milestone

    def milestone_must_exist
      unless current_milestone
        render_404
      end
    end

    def current_milestone_issues(per_page: MAX_PER_PAGE)
      state = params[:closed] == "1" ? :closed : :open

      closed = params[:closed] == "1"
      per_page = closed ? CLOSED_PER_PAGE : per_page

      @current_milestone_issues ||= current_milestone.issues_to_render_for_viewer(
        current_user,
        state: state,
        page: current_page,
        per_page: per_page,
      )
    end

    def modifiers_only
      return redirect_to_login unless logged_in?
      return redirect_to "/" unless current_repository
      redirect_to "/" unless current_user_can_push?
    end

  private

  def milestone_params
    params.require(:milestone).permit %i[title description due_on state body]
  end
end
