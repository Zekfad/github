# frozen_string_literal: true

class ApplicationController < ActionController::Base
  abstract!

  NUMERIC_PAGE_PATTERN = /\A-?\d+/
  DEFAULT_PER_PAGE = 25      # Pagination
  # Maximum length of URL redirects before login flow. Varnish allows
  # headers up to 8k in length, this gives us enough headroom for a
  # safe `Location` header.
  MAX_URL_LOGIN_REDIRECT_LENGTH = 7680

  include PlatformHelper, UrlHelper, NewsiesControllerHelper,
    GitHub::Timer, ActionView::Helpers::AssetTagHelper,
    Mobile::ApplicationHelper

  include ApplicationController::ErrorHandlingDependency
  include ApplicationController::PjaxDependency
  include ApplicationController::RolesDependency
  include ApplicationController::EventsDependency
  include ApplicationController::EnterpriseDependency
  include ApplicationController::ModelSettingsDependency
  include ApplicationController::RequestCategoryDependency
  include ApplicationController::AreasOfResponsibilityDependency
  include ApplicationController::GracefulTimeoutDependency
  include ApplicationController::AuthenticatedSystem
  include ApplicationController::UserSessionDependency
  include ApplicationController::SudoDependency
  include ApplicationController::ConditionalAccessDependency
  include ApplicationController::IpWhitelistingDependency
  include ApplicationController::ExternalSessionsDependency
  include ApplicationController::CookiesDependency
  include ApplicationController::SecurityHeadersDependency
  include ApplicationController::AuthenticityTokenDependency
  include ApplicationController::AnalyticsDependency
  include ApplicationController::EmailTrackingDependency
  include ApplicationController::StatsDependency
  include ApplicationController::DatabaseDependency
  include ApplicationController::JsonXhrRequirementDependency
  include ApplicationController::ControllerAssetsBundlerDependency
  include ApplicationController::PaginationDependency
  include ApplicationController::HovercardDependency
  include ApplicationController::ProjectsDependency
  include ApplicationController::SecurityCheckupDependency
  include ApplicationController::AuditDependency
  include ApplicationController::TradeControlsDependency
  include ApplicationController::NotificationsDependency
  include ApplicationController::DiscussionsDependency
  include ApplicationController::VarnishDependency

  include GitHub::ServiceMapping

  if !Rails.env.production?
    prepend ApplicationController::SafetyCheckForRenderWhenRespondingTo
  end

  helper :all # rails is a wildcard, see #all_application_helpers below
  include GitHub::RouteHelpers
  include ViewModelHelper
  include FailbotHelper
  include GitHub::BrowserStatsHelper
  include Scientist

  helper_method :failbot

  # Application-wide filters
  #
  # Please be cautious when adding new app filters. They affect the behavior
  # and performance of every controller and action.
  #
  # No prepend_ filters allowed.
  #
  # PDI: All these filters could use an audit to see how necessary they are.
  # Or if they could simpily be invoked by manually. We should group and
  # document filters that are expected to throw exceptions and halt the filter
  # chain.

  if Rails.test?
    before_action :raise_on_bad_filter_chain
  end
  around_action :with_user_timezone
  before_action :setup_request_context
  before_action :initialize_log_data
  # This filter must execute before any others that query `preview_features?`
  before_action :set_site_admin_and_employee_status

  # Filters to set global response headers, cookies, and categorize requests.
  # Most other general filters, especially those that may render an HTML
  # template, should not appear before this group.
  before_action :set_security_headers
  before_action :employee_only_unicorn
  before_action :set_employee_cookie, unless: :stateless_request?
  before_action :request_categorization_filter
  before_action :areas_of_responsibility_filter
  before_action :push_service_mapping_context
  before_action :set_user_headers
  before_action :set_rails_version_header
  before_action :set_vary_pjax, :strip_pjax_params, :set_pjax_url

  # After this point, it should be safe to render HTML from a filter.
  before_action :require_security_checkup

  # Simulate an action that calls `current_visitor` for tests
  before_action :current_visitor, if: -> { Rails.env.test? && params[:find_current_visitor] }

  if GitHub.enterprise?
    # See enterprise_dependency.rb
    before_action :enforce_private_mode
    before_action :license_invalid_check
    before_action :license_expiration_check
    before_action :first_run_check
  else
    before_action :store_utm_parameters
    before_action :track_emails
  end

  protect_from_forgery(with: :exception, if: :verify_authenticity_token?)

  before_action :trace_web_request

  before_action :private_instance_bootstrap_check, if: -> { GitHub.private_instance? }

  # AbstractRepositoryController has 2 filters that it insists on running
  # before others. We can't use prepend_before_action anymore, so the order
  # is explicitly stated here but conditioned to only run for the
  # AbstractRepositoryController subclass.
  #
  # TODO: We should try to refactor these filters so they don't need to be ran
  # so early in the chain. Or maybe just removed entirely.
  before_action :ask_the_gatekeeper, :set_path_and_name,
    if: lambda { |c| c.is_a?(AbstractRepositoryController) }

  before_action :disable_session, if: :stateless_request?
  before_action :permission_denied_if_mismatched_login_and_token
  before_action :skip_mc_check
  around_action :staff_rails_instrumentation
  around_action :staff_external_service_profiler
  around_action :staff_mysql_instrumentation, if: lambda {
    params[:mysql_query_trace]
  }
  around_action :record_stats
  before_action :enforce_oauth_scope
  before_action :cap_pagination
  before_action :perform_conditional_access_checks, if: :require_conditional_access_checks?
  before_action :initialize_hydro_context

  after_action :set_pjax_version
  after_action :set_login_cookie, unless: :stateless_request?
  after_action :sanitize_pjax_redirects
  after_action :block_non_xhr_json_responses

  def clear_weak_password_session_variable
    session.delete(::CompromisedPassword::WEAK_PASSWORD_KEY)
  end

  def request_time_left
    start_time = request.env["process.request_start"] || Time.now.to_f
    GitHub.request_timeout(request.env) - (Time.now.to_f - start_time)
  end
  helper_method :request_time_left

  def params
    @_params ||= begin
      ActionController::Parameters.new apply_param_encoding(request.parameters)
    end
  end

  PARAM_ENCODINGS = {}

  def self.param_encoding(action, param_name, encoding)
    PARAM_ENCODINGS[self.controller_path] ||= {}
    PARAM_ENCODINGS[self.controller_path][action.to_s] ||= {}

    PARAM_ENCODINGS[self.controller_path][action.to_s][param_name.to_s] = encoding
  end

  # The Rack environment hash.
  def env
    request.env
  end

  # Public: A Hash of data to be logged that is stored in the request env.
  # This will eventually be logged by Rack::RequestLogger.
  #
  # Returns a Hash.
  def log_data
    request.env[Rack::RequestLogger::APPLICATION_LOG_DATA] ||= GitHub::Logger.empty
  end
  helper_method :log_data

  def read_fragment(key, options = nil)
    fragment = super
    if fragment && fragment.encoding == Encoding::ASCII_8BIT
      GitHub.dogstats.increment("fragment", tags: ["action:view", "type:binary"])

      fragment.force_encoding "UTF-8"
      unless fragment.valid_encoding?
        GitHub.dogstats.increment("fragment", tags: ["action:view", "type:binary", "error:invalid_bytes"])
        fragment.scrub!
      end
    end

    fragment
  end

  ##
  # Feature Toggles

  def repository_mirror_enabled?
    preview_features? || current_user.to_s == "mirrors" && !enterprise?
  end
  helper_method :repository_mirror_enabled?

  # SSH Key auditing enabled?
  #
  # Disable auditing if LDAP Sync is enabled and SSH Keys are managed by LDAP.
  def audit_ssh_enabled?
    return true if !GitHub.enterprise?
    !GitHub.auth.ssh_keys_managed_externally?
  end
  helper_method :audit_ssh_enabled?

  # Password resets can only be done for built-in users
  # (external users should reach their providers for that)
  def password_reset_enabled?
    !GitHub.auth.external_user?(current_user)
  end
  helper_method :password_reset_enabled?

  def octolytics_enabled?
    GitHub.octolytics_enabled?
  end
  helper_method :octolytics_enabled?

  def response_code_for_rescue(exception)
    ActionDispatch::ExceptionWrapper.rescue_responses[exception.class.name]
  end

  def render_optional_error_file(status_code)
    set_static_file_csp
    status = Rack::Utils.status_code(status_code)
    path = "#{Rails.public_path}/#{status.to_s[0, 3]}.html"
    if !performed? && File.exist?(path)
      render file: path, status: status, layout: false, formats: [:html]
    else
      head status
    end
  end

  def blocked_by_owner?(owner_id = current_repository.try(:owner_id))
    return false if !owner_id || !logged_in? || current_user.id == owner_id
    current_user.blocked_by?(owner_id)
  end

  def blocked_by_author?(author)
    logged_in? && current_user.blocked_by?(author)
  end

  # The GitHub::ServiceMapping module expects this method in order to properly initialize the
  # service mapping for the request in push_service_mapping_context.
  alias service_mapping_method action_name

  # Always returns false (even if no repository exists for this request).
  # Overridden in RepositoryControllerMethods for Repository based controllers
  # and views.
  #
  # Returns false.
  def current_repository_locked_for_migration?
    false
  end
  helper_method :current_repository_locked_for_migration?

  # Always returns true (even if no repository exists for this request).
  # Overridden in RepositoryControllerMethods for Repository based controllers
  # and views.
  #
  # Returns true.
  def current_repository_writable?
    true
  end
  helper_method :current_repository_writable?

  # Public: Find or create a site visitor that is trackable by Octolytics. The
  # primary use case for interacting with the `current_visitor` is to perform
  # A/B testing.
  #
  # Visitor ids are persisted in the `_octo` cookie.
  #
  # Examples
  #
  #   GitHub::UserResearch.experiment_variant({
  #     :experiment => :my_experiment,
  #     :subject    => current_visitor
  #   })
  #
  def current_visitor
    @current_visitor ||= find_current_visitor || create_current_visitor
  end
  helper_method :current_visitor

private
  helper_method :current_repository, :issue_path, :current_page

  def helper
    @helper ||= Class.new do
      include ActionView::Helpers, ApplicationHelper
    end.new
  end

  def at_auth_limit?
    return @at_auth_limit if defined? @at_auth_limit

    @at_auth_limit = if GitHub.web_ip_lockouts_enabled?
      AuthenticationLimit.at_any?(web_ip: request.remote_ip)
    end
  end
  helper_method :at_auth_limit?

  def at_auth_limit_login?
    # For dotcom, we cache GET and HEAD responses in varnish, so
    # we shouldn't use remote_ip to decide anything about how to
    # handle the request.
    return false if request.get? || request.head?
    at_auth_limit?
  end
  helper_method :at_auth_limit_login?

  def sanitize_referrer
    @sanitize_referrer = true
  end

  def apply_param_encoding(params_hash)
    params_hash.each_pair do |k, v|
      if encoding_template
        encoding = encoding_template[k] || Encoding::UTF_8
        if v.is_a?(String) && v.encoding != encoding
          params_hash[k] = v.force_encoding(encoding)
        end
      end

      check_param_encoding(k, v)
    end

    params_hash
  end

  class InvalidParameterError < ArgumentError; end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def encoding_template
    @encoding_template ||= begin
      if PARAM_ENCODINGS[self.controller_path] &&
           PARAM_ENCODINGS[self.controller_path][self.action_name]
        PARAM_ENCODINGS[self.controller_path][self.action_name]
      end
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def check_param_encoding(key, param)
    case param
    when Array
      param.each { |element| check_param_encoding(key, element) }
    when Hash
      param.each_value { |value| check_param_encoding(key, value) }
    when String
      unless param.valid_encoding?
        error = InvalidParameterError.new("Non UTF-8 value in param")
        Failbot.report_user_error(error)
        head 400
      end
    end
  end

  def set_return_to
    if params[:return_to] == "gist"
      session[:return_to] = GitHub.gist_url
    elsif params[:return_to].present?
      session[:return_to] = params[:return_to]
    end
  end

  # Redirect to the URI stored by the most recent store_location call or
  # to the passed default.  Also clears the return_to value from the session.
  def redirect_to_return_to(fallback: nil)
    safe_redirect_to(
      return_to,
      fallback: fallback ||= request.env["return_to"],
      allow_query: true,
      allow_fragment: true,
      allow_hosts: safe_redirect_hosts,
    )
  end

  def return_to
    @return_to ||= session.delete(:return_to) || request.env["return_to"] || params[:return_to]
  end
  helper_method :return_to

  def safe_redirect_hosts
    [
      GitHub.urls.host_name,
      GitHub.urls.raw_host_name,
      GitHub.pages_host_name_v2,
      GitHub.pages_auth_host_name,
      GitHub.gist_host_name,
      GitHub.gist3_host_name,
      GitHub.urls.codeload_host_name,
      GitHub.render_host,
      GitHub.classroom_host,
    ].uniq.compact
  end

  def current_page(page_param = :page)
    if params[page_param].blank? || !params[page_param].respond_to?(:to_i)
      1
    else
      params[page_param].to_i.abs
    end
  end

  def current_repository
    nil
  end

  def log_login(sign_in_verification_method, authentication_record)
    payload = {
      actor_ip: request.remote_ip,
      note: "From #{request.host}",
      user_session_id: user_session.id,
    }

    if current_user.sign_in_analysis_enabled?
      payload[:sign_in_verification_method] = sign_in_verification_method
    end

    current_user.instrument_login(payload) if current_user.is_a?(User)

    GlobalInstrumenter.instrument "user.successful_login", {
      actor: current_user,
      primary_email: current_user.primary_user_email,
      elected_to_receive_marketing_email: NewsletterPreference.marketing?(user: current_user),
      return_to: session[:return_to],
      authentication_record: authentication_record,
    }
  end

  def update_login_metadata
    if logged_in? && current_user.is_a?(User)
      current_user.save_login_metadata(ip: request.remote_ip)
    end
  end

  def redirect_to(*args)
    if sudo_redirect_to_back?(args)
      safe_redirect_to(params[:sudo_referrer], allow_hosts: [GitHub.host_name, GitHub.admin_host_name])
    elsif args && args.first == :back
      without_back = args - [:back]
      if without_back.empty?
        redirect_back(fallback_location: "/")
      else
        redirect_back(**without_back.last.merge(fallback_location: "/"))
      end
    else
      super
    end
  end

  # detect postbacks for sudo actions using redirect_to :back (which would break)
  def sudo_redirect_to_back?(args)
    args == [:back] && params[:sudo_referrer].present?
  end

  # Redirects to login and sets the return_to to the argument.
  def redirect_to_login(return_to = nil)
    # Block fuzzing that triggers 500 errors in Varnish due to buffer
    # overflows with huge URLs.
    if return_to && URI.encode_www_form_component(return_to).bytesize > MAX_URL_LOGIN_REDIRECT_LENGTH
      return_to = nil
    end
    url_options = return_to ? { return_to: return_to } : {}
    redirect_to login_url(url_options)
  end

  # Redirects to the dashboard.
  def redirect_home
    redirect_to home_url
  end

  # Set the sticky context.
  def set_context
    return unless logged_in?

    if session[:context] != current_context.to_s
      session[:context] = context_path(current_context)
    end
  end

  # The most recently selected context, aka what dashboard the user
  # will see when going "home" while logged in.
  def saved_context
    session[:context] if session && logged_in?
  end

  # Used to redirect to a URL given in a param if it is considered safe. By
  # default only onsite URL are considered safe. Additional safe hosts can be
  # passed as an option to allow offsite redirects.
  #
  # to      - String URL, usually from params[:to]
  # options - Optional Hash passed to ActionController::Base#redirect_to. The
  #           below options are used by safe_redirect_to itself and will be
  #           deleted before calling ActionController::Base#redirect_to. Please
  #           see ActionController::Base#redirect_to for a full description of
  #           the ActionController::Base#redirect_to specific options.
  #           :fallback        - The redirection URL if `to` points to a
  #                              disallowed off-site URL.
  #           :allow_hosts     - An array of hosts that can be redirected to.
  #                              By default safe_redirect_to only allows
  #                              redirects to onsite hosts (default: []).
  #           :allow_query     - Allow query string for offsite hosts
  #                              (default: false)
  #           :allow_fragment  - Allow fragment for offsite hosts
  #                              (default: false)
  #
  # Returns nothing.
  def safe_redirect_to(to, options = {})
    fallback = options.delete(:fallback) || "#{request.base_url}/"
    to ||= fallback
    allow_hosts = options.delete(:allow_hosts) || []
    allow_query = options.delete(:allow_query)
    allow_fragment = options.delete(:allow_fragment)

    parsed_url = Addressable::URI.parse(to)
    onsite_absolute_url =
      parsed_url.absolute? &&
      ["https", "http"].include?(parsed_url.scheme) &&
      parsed_url.userinfo.nil? &&
      parsed_url.host == request.host &&
      (parsed_url.port.nil? || parsed_url.port == request.port)

    onsite_relative_url =
      parsed_url.relative? &&
      parsed_url.userinfo.nil? &&
      (parsed_url.host.nil? || parsed_url.host == request.host) &&
      (parsed_url.port.nil? || parsed_url.port == request.port)

    allowed_offsite_absolute_url =
      parsed_url.absolute? &&
      ["https", "http"].include?(parsed_url.scheme) &&
      parsed_url.userinfo.nil? &&
      allow_hosts.include?(parsed_url.host) &&
      parsed_url.port.nil? &&
      (parsed_url.fragment.nil? || allow_fragment) &&
      (parsed_url.query.nil? || allow_query)

    if onsite_absolute_url || onsite_relative_url
      parsed_url.host = request.host
      parsed_url.scheme = request.scheme
      redirect_to parsed_url.to_s, options
    elsif allowed_offsite_absolute_url
      redirect_to parsed_url.to_s, options
    else
      err = BadRedirect.new("Sketchy redirect URL")
      Failbot.report_user_error(err)
      GitHub.dogstats.increment("safe_redirect_to", tags: ["action:fallback", "error:sketchy_url"])
      redirect_to fallback, options
    end
  rescue Addressable::URI::InvalidURIError
    err = BadRedirect.new("Invalid redirect URL")
    Failbot.report_user_error(err)
    GitHub.dogstats.increment("safe_redirect_to", tags: ["action:fallback", "error:invalid_url"])
    redirect_to fallback, options
  end

  ##
  # Filters

  def force_email
    if logged_in? && current_user.email.blank?
      flash[:error] = "You’ll need to add an email address to your account."
      redirect_to settings_user_emails_url
    end
  end

  # Test if cache should be skipped for a request.
  # This is enabled by tacking ?skipmc=1 onto any URL.
  def skip_mc_check
    if params[:skipmc] || request.env["HTTP_X_SKIP_MC"]
      skip_mc
    end
  end

  # Special error class to track uses of skipmc. See below.
  class SkipmcCalled < StandardError; end

  # Configures the caches to always miss, causing values to be regenerated and
  # written to cache again.
  # Note: Certain requests (i.e., atom and raw) disable the session, so the
  # staff check fails.
  def skip_mc
    if real_user_site_admin?

      # Report this skipmc invocation to Haystack, don't raise
      # roll up skipmc needles based on controller/action
      action = params.values_at(:controller, :action).join("#")
      e = SkipmcCalled.new(action)
      e.set_backtrace(caller)
      Failbot.report!(e,
        app: "github-skipmc",
        rollup: Digest::MD5.hexdigest([e.class.name, action].join("|")),
      )

      GitHub.cache.skip = true
    else
      params.delete(:skipmc)
    end
  end

  # Version all fragment caches
  def fragment_cache_key(key)
    prefix = GitHub.fragment_cache_version
    ActiveSupport::Cache.expand_cache_key(key.is_a?(Hash) ? url_for(key).split("://").last : key, prefix)
  end

  # Set Time.zone to the clients browser zone we detect via JavaScript.
  # See app/assets/javascripts/github/behaviors/timezone.coffee
  def with_user_timezone
    old_zone = Time.zone

    begin
      tz = cookies[:tz].to_s.dup.force_encoding("utf-8")
      raise ArgumentError.new("bad TZ encoding") if !tz.valid_encoding?

      if zone = ActiveSupport::TimeZone[cookies[:tz].to_s]
        Time.zone = zone
      end
    rescue ArgumentError => err
      Failbot.report(
        err,
        tz_string: cookies[:tz].b,
        tz_encoding: cookies[:tz].encoding,
      )
    end

    yield
  ensure
    # Reset zone back to its original state.
    # Default zone is set to "UTC" in `config/initializers/libraries.rb`.
    Time.zone = old_zone
  end

  # Before filter to setup request related globals. GitHub.context and
  # Failbot.context need to be populated with basic request metadata. This
  # should be registered as early in the filter chain as possible.
  #
  # Returns nothing.
  def setup_request_context
    GitHub.context.push(initial_request_context)
    Failbot.push(initial_failbot_context)
    CoreDumpContext.set(initial_failbot_context)
    SensitiveData.context.push(initial_sensitive_data_context)
  end

  # Special error class to raise when a bad filter chain conditional action is
  # found. See below.
  class BadFilterChainConditionalActionError < StandardError; end

  # This filter runs in test and ensures that no invalid conditional actions
  # (:only, :except) are found in a filter chain entry. When the filter chain
  # is built up, it is valid to add a filter that references an, as of yet,
  # undefined action. As a result, Rails can't prevent someone from adding an
  # action that, even at runtime, will be undefined. Rails' current handling
  # of the situation is to silently ignore invalid actions at runtime. This
  # can be problematic, as you might have made a typo or refactored an action
  # name, causing an intended filter to never get called. For example, you
  # might have a filter like:
  #
  #     before_action :check_authorization, only[:uppdate]
  #
  # The intent was to ensure the user is authorized to make updates. But,
  # because of the typo, no authorization check will be done.
  def raise_on_bad_filter_chain
    # Be doubly paranoid about not running in production.
    return unless Rails.test?
    # This is a gross hack into Rails internals. But, it seems to work fine on
    # Rails 3 and 4 (haven't tested on 5). Given we only call this filter in
    # test environments, the risk of relying on an internal method is
    # relatively low.
    return unless callbacks = self.class._process_action_callbacks
    callbacks.each do |cb|
      filter = cb.filter
      # Some internal/dynamically created callbacks are not symbols.
      next unless filter.is_a?(Symbol)
      if_ = cb.instance_variable_get(:@if)
      unless_ = cb.instance_variable_get(:@unless)
      next unless if_.present? || unless_.present?
      actions = (Array(if_) + Array(unless_)).map { |c| _actions_for_callback_option(c) }.flatten.compact

      bad_conditional_action = actions.find do |action|
        !self.respond_to?(action, true)
      end

      if bad_conditional_action
        raise BadFilterChainConditionalActionError,
          "#{self.class} contains a filter (#{filter}) that is conditionally called on a non-existant action (#{bad_conditional_action}). Please update or remove the action."
      end
    end
  end

  # Helper to extract the actions from an entry in a callback's if / unless
  # The Rails 5 version depends on the monkeypatch in track_callback_params.rb
  def _actions_for_callback_option(cb_option)
    return unless cb_option.is_a?(Proc)
    cb_option.instance_variable_get(:@_from).to_a
  end
  private :_actions_for_callback_option

  # The initial context to populate GitHub.context when the request starts.
  #
  # Maybe overridden in subclasses to add additional debugging metadata. Be
  # sure to call super and merge the hash onto the existing data.
  #
  # Returns a Hash.
  def initial_request_context
    request_url =
      "#{request.protocol}#{request.host_with_port}#{request.fullpath}" rescue nil

    context = {
      actor_ip: request.remote_ip,
      user_agent: request.user_agent.to_s,
      connections: ApplicationRecord.connection_info,
      from: "%s#%s" % [params[:controller], params[:action]],
      method: request.try(:request_method),
      request_id: request_id,
      server_id: Rack::ServerId.get(request.env),
      request_category: request_category,
      controller_action: params[:action],
      url: request_url,
      client_id: persistent_client_id,
      referrer: request.referrer,
      device_cookie: cookies[:_device_id],
    }
    context[:robot] = robot_type if robot?
    context
  end

  # The initial context to populate Failbot.context when the request starts.
  #
  # Maybe overridden in subclasses to add additional debugging metadata. Be
  # sure to call super and merge the hash onto the existing data.
  #
  # Returns a Hash.
  def initial_failbot_context
    env = request.try(:env) || {}
    request_url =
      "#{request.protocol}#{request.host_with_port}#{request.filtered_path}" rescue nil
    context = {
      accept: env["HTTP_ACCEPT"],
      action: params[:action],
      controller: self.class,
      connections: ApplicationRecord.connection_info,
      device_cookie: cookies[:_device_id],
      enabled_features: proc { env[Rack::ProcessUtilization::ENV_KEY].enabled_features },
      language: env["HTTP_ACCEPT_LANGUAGE"],
      master_pid: Process.ppid,
      master_started_at: GitHub.unicorn_master_start_time,
      method: request.try(:request_method),
      oauth_access_id: proc { logged_in? && current_user.oauth_access.try(:id) },
      params: request.filtered_parameters,
      private_repo: proc { current_repository.try(:private?) },
      rails: Rails.version,
      referrer: request.try(:referrer),
      remote_ip: request.try(:remote_ip),
      repo: proc { current_repository.try(:name_with_owner) },
      repo_id: proc { current_repository.try(:id) },
      request_category: request_category,
      request_id: env["HTTP_X_GITHUB_REQUEST_ID"],
      request_wait_time: env[GitHub::TaggingHelper::REQ_WAIT_TIME],
      requested_at: Time.now.utc,
      route: proc { current_repository.try(:safe_route) },
      server_id: Rack::ServerId.get(env),
      session: session.try(:to_hash),
      stateless: stateless_request?,
      tested_features: proc { env[Rack::ProcessUtilization::ENV_KEY].tested_features },
      url: request_url,
      user: proc { current_user.try(:login) },
      user_spammy: proc { current_user.try(:spammy?) },
      user_agent: env["HTTP_USER_AGENT"],
      user_session_id: proc { user_session.try(:id) },
      worker_pid: Process.pid,
      worker_request_count: GitHub.unicorn_worker_request_count,
      worker_started_at: GitHub.unicorn_worker_start_time,
      zone: Time.zone.name,
    }
    if user_session.try(:impersonator).present?
      context[:impersonator] = proc { user_session.impersonator.login }
    end
    context[:robot] = robot_type if robot?
    context
  end

  # The initial context to populate SensitiveData.context when the request starts.
  #
  # May be overridden in subclasses to track additional sensitive data. Be
  # sure to call super and merge the hash onto the existing data.
  #
  # Returns a Hash.
  def initial_sensitive_data_context
    context = {
      # repo name is sensitive when it's private
      repo: proc {
        if current_repository && current_repository.private?
          current_repository.name_with_owner
        end
      },
      remote_ip: request.try(:remote_ip),
    }

    # A _device_id is a randomly generated cookie set during sign in and never expires. If a _device_id were knowingly associated with a user,
    # that information could be used to bypass device verification if the username/password is also known. It would also be able to perform
    # one of the required steps for 2FA recovery.
    if logged_in?
      context[:device_cookie] = cookies[:_device_id]
    end

    context
  end

  # Internal: Log
  #
  # data   - A Hash of key/value pairs to log in addition to the
  #          standard logging data
  # blk    - Execute a block that we wrap with log messages measuring the
  #          block execution.
  def log(data, &blk)
    GitHub::Logger.log(with_standard_logging_data(data), &blk)
  end

  # Internal: Combines the given data with other relevant contextual data, and
  # returns a Hash that is ready to be logged.
  #
  # data - A Hash of data that is intended for logging.
  #
  # Returns a Hash.
  def with_standard_logging_data(data)
    { now: Time.now.iso8601 }.merge(data)
  end

  # Internal: Store application specific data for this request to be logged.
  #
  # Returns nothing.
  def initialize_log_data
    context = initial_failbot_context
    context_log_data = {
      user: context[:user].try(:call),
      user_spammy: context[:user_spammy].try(:call),
      repo: context[:repo].try(:call),
      repo_route: context[:route].try(:call),
      oauth_access_id: context[:oauth_access_id].try(:call),
      user_session_id: context[:user_session_id].try(:call),
      time_zone: context[:zone],
      controller: context[:controller],
      action: context[:action],
      stateless: context[:stateless],
    }
    log_data.merge!(context_log_data)
  end

  # Determine the domain used for setting cookies.
  def cookie_domain
    unless GitHub.enterprise?
      ".#{GitHub.host_name}"
    else
      GitHub.host_name
    end
  end

  # Sets a cookie for the entire .github.com domain if the user is logged in
  # used for oauth apps to know whether to automatically redirect.
  def set_login_cookie
    logged_in_value   = logged_in? ? "yes" : "no"
    dotcom_user_value = logged_in? ? current_user.login : nil

    if cookies[:logged_in] != logged_in_value
      cookies[:logged_in] = {
        value: logged_in_value,
        expires: 1.year.from_now,
        domain: cookie_domain,
      }
    end

    if cookies[:dotcom_user] != dotcom_user_value
      if dotcom_user_value
        cookies[:dotcom_user] = {
          value: dotcom_user_value,
          expires: 1.year.from_now,
          domain: cookie_domain,
        }
      else
        cookies.delete(:dotcom_user, domain: cookie_domain)
      end
    end
  end

  # These are used for nginx logging.
  def set_user_headers
    response.headers["X-GitHub-User"] = current_user.login if logged_in?
    response.headers["X-GitHub-Session-Id"] = user_session.id.to_s if user_session
  end

  def set_rails_version_header
    response.headers["X-Rails-Version"] = Rails.version if site_admin?
  end

  # Caps the current page for paginated requests
  def cap_pagination
    if cap_pagination? && over_pagination_limit?
      GitHub.dogstats.increment("pagination_cap", tags: ["via:web"])
      render_404
    end
  end

  # Only cap pagination for numeric page numbers outside of Enterprise
  def cap_pagination?
    !GitHub.enterprise? && params[:page].to_s =~ NUMERIC_PAGE_PATTERN
  end

  def over_pagination_limit?
    current_page > max_pagination_page
  end

  helper_method :at_pagination_limit?
  def at_pagination_limit?
    current_page == max_pagination_page
  end

  # Forwards `utm_*` parameters to enterprise_web_url links
  def store_utm_parameters
    # Skip if action disables sessions
    return unless request.get? && session

    get_params = request.GET
    utm_keys = get_params.keys.grep(/^utm_/)
    if utm_keys.any?
      session[:utm_memo] = utm_keys.each_with_object({}) do |key, memo|
        memo[key] = get_params[key]
      end
    end
  end

  def enforce_oauth_scope
    return unless logged_in? && current_user.using_oauth?
    render_404 unless adequate_oauth_scope(current_user.scopes)
  end

  def authorize_content(kind, data = {})
    action_to_authorize = data.delete(:action_to_authorize) || action_name
    authorization = ContentAuthorizer.authorize(current_user, kind, action_to_authorize, data)

    if authorization.has_email_verification_error?
      render_email_verification_required
    elsif authorization.failed?
      render_404
    end
  end

  def required_oauth_scopes
    %w(user repo).freeze
  end

  def adequate_oauth_scope(scopes)
    scopes = scopes.map &:to_s
    (required_oauth_scopes - scopes).empty?
  end

  def live_updates_enabled?
    # Check global live updates switch first
    return false unless GitHub.live_updates_enabled?

    # No anonymous live update connections
    return false unless logged_in?

    # Otherwise, its enabled for all users
    true
  end
  helper_method :live_updates_enabled?

  # A lot of pages don't work well when trailing slashes are present. Using this
  # in a before_action causes a redirect with all trailing slashes removed.
  def redirect_away_from_trailing_slashes
    # request.path in Rails 3 automatically chomps away the / so we need to
    # dive into the CGI-ish variables Rack passes us to get the original path.
    path = request.env["REQUEST_PATH"] || request.env["REQUEST_URI"] || request.env["PATH_INFO"]
    if path.end_with?("/")
      redirect_to path.chomp("/")
    end
  end

  # GitHub staff get a special cookie `staffonly` set to `yes`
  # The cookie is also used by nginx to redirect to Lab if the cookie
  # is set to `true` instead of `yes`. Once we have garage.github.com
  # working, hopefully we can phase out nginx redirect and the `true`
  # value.
  def set_employee_cookie
    if cookie = GitHub::StaffOnlyCookie.read(cookies)
      # if the cookie is valid and current, we update it
      set_employee_only_cookie(user: cookie.user, for_lab: cookie.for_lab?)
    elsif logged_in? && GitHub::StaffOnlyCookie.allowed_user?(current_user)
      # Set the "staffonly" cookie on the .github.com domain. This will be sent
      # to *.github.com domains and is used to authenticate requests to lab
      # environments other than "lab" (staff1).
      set_employee_only_cookie(user: current_user, for_lab: false)
    end
    return true
  end

  # Only folks with a valid cookie can use the staff-only fe.
  # Only staff get the cookie set, but it lingers after logout
  # for testing the login page.
  #
  # Returns true to skip the filter, or redirects the request.
  def employee_only_unicorn
    # if this isn't an employee unicorn, return true
    return true unless GitHub.employee_unicorn?
    # if the cookie is valid and current, return true
    return true if GitHub::StaffOnlyCookie.read(cookies)

    # otherwise, kick them back to genpop
    GitHub::StaffOnlyCookie.delete!(cookies)
    # delete the tempoary Kubernetes cookie here too
    cookies.delete(:haproxy_backend)
    redirect_to "https://github.com"
  end

  def set_employee_only_cookie(user:, for_lab:)
    if cookie = GitHub::StaffOnlyCookie.generate(user: user, for_lab: for_lab)
      cookie.save!(cookies)
    end
  end

  # before_action to hide dotcom only features
  # used mostly in DevtoolsController
  def dotcom_required
    return render_404 if GitHub.enterprise?
  end

  # before_action to hide GitHub Enterprise Server only features
  def enterprise_required
    return render_404 unless GitHub.enterprise?
  end

  # before_action for features that are only available on a GitHub Private Instance
  def private_instance_required
    return render_404 unless GitHub.private_instance?
  end

  # before_action for features that should not be available on a GitHub Private Instance
  def disable_for_private_instance
    return render_404 if GitHub.private_instance?
  end

  # before_action for features that should not be available on a FedRAMP GitHub Private Instance
  def disable_for_fedramp_private_instance
    return render_404 if GitHub.fedramp_private_instance?
  end

  # before_action to hide showcase on Enterprise only if not enabled by the admin
  def showcase_disabled
    return render_404 if (GitHub.enterprise? && !GitHub.showcase_enabled?)
  end

  # Is this a request for the gist application?
  def gist_request?
    false
  end
  helper_method :gist_request?

  # Helper methods to determine if the current request is being made by a robot,
  # and if so, which one.
  def robot?
    GitHub.robot?(request.user_agent.to_s)
  end
  helper_method :robot?

  def robot_type
    GitHub.robot_type(request.user_agent.to_s)
  end
  helper_method :robot_type

  # Marks timeout errors as being the client's fault for the duration of the
  # block. All timeout exceptions are rescued and a 403 response is sent back.
  # Timeout exceptions are not reported to haystack.
  def timeout_client_error(message = nil)
    yield
  rescue Object => boom
    raise if !timeout_error?(boom) || performed?
    message ||= "error: too big or took too long to generate"
    render status: 403, plain: message
  end

  # Overridden in AbstractRepositoryController. Many non-repository scoped layout
  # templates need this helper.
  def repository_offline?
    false
  end
  helper_method :repository_offline?

  # Deprecated: Should request be considered stateless? Does it use any cookies?
  #
  # Maybe overridden in subclasses to add whitelist certain actions as
  # stateless.
  #
  # This functionality should be considered deprecated. Stateless endpoints
  # are better off not inheriting from ApplicationController which has many
  # assumptions about browser clients.
  #
  # Current Usage
  # * Atom feeds (consider moving to api.github.com)
  #
  # Security note: stateless requests have no CSRF protection.
  #
  # Returns true or false.
  def stateless_request?
    false
  end
  helper_method :stateless_request?

  def disable_session
    request.session_options[:skip] = true
  end

  ##
  # Hacks

  # Rails is a wildcard.  helper(:all) is really doing
  # helper(all_application_helpers), which just loads all the helpers using
  # Dir[app/helpers/*].  Ideally, the helpers are loaded like this:
  #
  #   application_helper + shared_helpers + controller_helper
  #
  # This way, the controller can always override helpers that are defined
  # in the shared helpers.  Currently, we don't have shared helpers sorted
  # out from controller helpers, so we're just loading everything.
  class << self
  private
    def all_application_helpers
      helpers = super
      helpers.delete("application")
      helpers.delete(controller_name)
      helpers.unshift("application")
      helpers
    end
  end

  # Per-request unique identifier generated by the Rack::RequestId middleware
  def request_id
    request.env[Rack::RequestId::GITHUB_REQUEST_ID]
  end
  helper_method :request_id

  def referrer
    request.headers["Referer"]
  end
  helper_method :referrer

  # Header passed by GLB indicating the region in which the request originally landed
  def glb_edge_region
    request.headers["X-GLB-Edge-Region"]
  end
  helper_method :glb_edge_region

  # make User/Repo feature flag and experiment methods available as controller/helper/filter methods
  #
  # NOTE: Keep this at the bottom, after any other methods are defined,
  # so conflicts can be caught
  include ApplicationController::ExperimentsDependency
  include ApplicationController::FeatureFlagsDependency

protected

  def check_ofac_sanctions(target: current_user, redirect_url: nil)
    if current_user&.has_any_trade_restrictions? || target&.has_any_trade_restrictions?
      is_organization = target&.organization?
      if is_organization
        flash[:trade_controls_organization_billing_error] = true
      else
        flash[:trade_controls_user_billing_error] = true
      end
      yield if block_given?
      if redirect_url
        return redirect_to(redirect_url)
      elsif is_organization
        return redirect_to(org_root_path(target))
      else
        return redirect_to(billing_url)
      end
    end
  end

  def ensure_billing_enabled
    render_404 unless GitHub.billing_enabled?
  end

  def ensure_two_factor_sms_enabled
    render_404 unless GitHub.two_factor_sms_enabled?
  end

  def ensure_domain_verification_enabled
    render_404 unless logged_in? && GitHub.domain_verification_enabled?
  end

  def ensure_ip_whitelisting_available
    render_404 unless GitHub.ip_whitelisting_available?
  end

  def github_internal_referrer_route
    referrer = Addressable::URI.parse(request.referrer)
    if referrer && referrer.domain == request.domain
      GitHub::Application.routes.recognize_path(Addressable::URI.escape(referrer.path))
    end
  rescue ActionController::RoutingError, Addressable::URI::InvalidURIError
    # Referrer is invalid so cannot be internal route
    nil
  end

private
  def max_pagination_page
    GitHub.max_ui_pagination_page
  end

  def default_render(*args)
    raise NotImplementedError,
      "Do not use Rails implicit render. Explicitly call `render` in #{self.class.name}##{action_name}.\n  " +
        "render \"#{controller_path}/#{action_name}\"\n\n"
  end

  # Maintain compatibility with backported behavior
  # The native Rails 5+ version won't set Content-Type to text/plain
  # if it's already been set (by respond_to, for instance)
  # Prevents info leakage; see #66171 for more detail
  def render(*args)
    if args.first.is_a?(Hash)
      options = args.first
      if options[:plain]
        options[:content_type] = Mime[:text]
      elsif options[:html]
        options[:content_type] = Mime[:html]
      end
      if options[:text]
        raise NotImplementedError, "render text: is not supported in Rails 5+"
      end
    elsif args.first.is_a?(String)
      args[0] = args[0].dup
    end

    super
  end

  # Private: Instantiate a site visitor using the `_octo` cookie as a persistent
  # identifer.
  def find_current_visitor
    Analytics::Visitor.with_octolytics_id(cookies[:_octo])
  end

  # Private: Generate a new site visitor and store the visitor's ID in the
  # `_octo` cookie for the entire .github.com domain.
  def create_current_visitor
    visitor = Analytics::Visitor.create

    GitHub.dogstats.increment("cookie", tags: ["action:set", "type:_octo"])

    # This cookie is also used in Javascript client side, so it is explicitly
    # not marked as `httponly`
    cookies[:_octo] = {
      value: visitor.octolytics_id,
      expires: 1.year.from_now,
      domain: cookie_domain,
    }

    visitor
  end

  def repository_layout
    "repository"
  end

  def authentication_layout
    "session_authentication"
  end

  # Private: Returns boolean whether to protect from CSRF. Override in
  # subclasses to skip for specific cases.
  def verify_authenticity_token?
    !stateless_request?
  end

  # Private: checks to see if there are any CSP exceptions that should be added
  # to a given request. This method was extracted purely to prevent duplicate
  # code. It is still the calling controller's responsibility to add a filter
  # that calls this method. This will raise an error if called but CSP_EXCEPTIONS
  # is not defined.
  def add_csp_exceptions
    SecureHeaders.append_content_security_policy_directives(request, self.class::CSP_EXCEPTIONS)
  end

  def set_client_uid
    GitHub.context.push(client_uid: params[:client_uid])
    Audit.context.push(client_uid: params[:client_uid])
  end

  # Requires content to be served on an admin frontend (behind okta network
  # gateway). Redirects admin requests when served on non-admin frontends.
  def require_admin_frontend
    # Restricted front-ends aren't used/available in dev, test, and enterprise.
    return unless GitHub.admin_frontend_enabled?
    # Staging lab is a special snowflake, as it is wired up as a production
    # enviroment, but isn't actually holding production data. There is no
    # staging-lab specific configuration we can edit to set things like
    # `admin_frontend_enabled` to false. So, we special case it here to not
    # require an admin front-end.
    return if GitHub.staging_lab?
    # No need to redirect if the request is to an admin front-end.
    return if GitHub.admin_host?
    # If the current_user has no access to administrative functionality we 404
    # for normal users and 403 for employees.
    return render_403_for_employees unless admin_frontend_accessible?

    redirect_to(params.to_unsafe_hash.merge(host: GitHub.admin_host_name))
  end

  # Public: Captures request params along with current timestamp and provides interface for getting
  # Spamurai signals. See SignupController#create_account and user.create event for example usage.
  #
  # Returns a SpamuraiFormSignals instance.
  def spamurai_form_signals
    cleaned_params = request.params.dup
    blacklist = [:controller, :action, :password, :password_confirmation, :old_password]

    blacklist.each do |key|
      cleaned_params.delete(key)
      cleaned_params[:user].delete(key) if cleaned_params[:user]
    end

    @spamurai_form_signals ||= SpamuraiFormSignals.new(request_params: cleaned_params)
  end

  # Public: Return the persistent client ID from the _octo cookie, if present.
  # This is different than the session ID because it is not reset when the user
  # logs out.
  def persistent_client_id
    Analytics::OctolyticsId.coerce(cookies[:_octo]).unversioned
  rescue Analytics::OctolyticsId::CoercionError
    nil
  end

  def require_xhr
    head :not_acceptable unless request.xhr?
  end

  def initialize_hydro_context
    if hydro_context[:enabled]
      hydro_context.merge!({
        request_category: request_category,
        controller: self.class.name,
        controller_action: action_name,
        client_id: persistent_client_id,
        session_id: user_session&.id,
        current_user: current_user&.login,
        current_user_id: current_user&.id,
        analytics_tracking_id: current_user&.analytics_tracking_id,
        server: Failbot.context&.first&.[]("server"),
      })

      if current_repository
        hydro_context.merge!({
          current_repo: current_repository.nwo,
          current_repo_id: current_repository.id,
          current_repo_visibility: (current_repository.private? ? :PRIVATE : :PUBLIC),
        })
      end
    end
  end

  def disable_hydro_request_logging
    hydro_context[:enabled] = false
  end

  def hydro_context
    request.env[GitHub::HydroMiddleware::CONTEXT] ||= {}
  end

  def referral_params
    {
      ref_page: params[:ref_page],
      ref_cta: params[:ref_cta],
      ref_loc: params[:ref_loc]
    }
  end

  def with_referral_params(**options)
    referral_params.merge(options)
  end

  def private_instance_bootstrap_check
    unless GitHub.private_instance_bootstrapper.bootstrapped?
      if GitHub.private_instance_bootstrapper.enterprise_profile_update.completed?
        redirect_to bootstrap_instance_configuration_path
      else
        redirect_to bootstrap_instance_path
      end
    end
  end
end
