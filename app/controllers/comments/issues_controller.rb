# frozen_string_literal: true

class Comments::IssuesController < ApplicationController
  CreateIssueFromCommentMutation = parse_query <<-'GRAPHQL'
    mutation($repositoryId: ID!, $body: String!, $title: String!) {
      createIssue(input: { repositoryId: $repositoryId, body: $body, title: $title }) {
        issue {
          number
        }
      }
    }
  GRAPHQL

  def create
    results = platform_execute(
      CreateIssueFromCommentMutation,
      variables: {
        repositoryId: repository.global_relay_id,
        title: issue_params[:title],
        body: issue_params[:body],
      },
    )

    return render_404 if results.errors.any?

    respond_to do |format|
      format.html do
        redirect_to issue_path(owner, repository, results.create_issue.issue.number)
      end
    end
  end

  def issue_params
    params.require(:issue).permit(:title, :body, :repository_id)
  end

  def repository
    @repository ||= Repository.find(issue_params[:repository_id])
  end

  def owner
    @owner ||= repository.owner
  end

  def target_for_conditional_access
    owner
  end
end
