# frozen_string_literal: true

class MarketplacesController < ApplicationController
  areas_of_responsibility :marketplace

  statsd_tag_actions only: [:index, :show]

  before_action :marketplace_required

  layout "marketplace"

  NUMBER_OF_FEATURED_ITEMS = 8

  ShowQuery = parse_query <<-'GRAPHQL'
    query($loggedIn: Boolean!, $numberOfFeaturedItems: Int!) {
      ...Views::Marketplaces::Show::Root
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: {
      numberOfFeaturedItems: NUMBER_OF_FEATURED_ITEMS,
      loggedIn: logged_in?,
    })

    render "marketplaces/show", locals: {
      data: data,
      number_of_featured_items: NUMBER_OF_FEATURED_ITEMS,
    }
  end

  private

  def require_conditional_access_checks?
    false
  end
end
