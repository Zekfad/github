# frozen_string_literal: true

class CommunityController < AbstractRepositoryController
  layout :repository_layout

  statsd_tag_actions only: :index

  before_action :login_required
  before_action :ask_the_gatekeeper
  before_action :non_forks_only, except: [:license_tool, :code_of_conduct_tool, :minimize_comment, :unminimize_comment]
  before_action :render_404, unless: :community_profile_enabled?, except: [:minimize_comment, :unminimize_comment]
  before_action :enforce_plan_supports_insights, only: :index

  def index
    return render_404 unless current_repository.public?

    render_template_view(
       "/community/index",
       Community::CommunityIndexView,
       community_profile: community_profile,
       repository: current_repository,
     )
  end

  MinimizeCommentMutation = parse_query <<~'GRAPHQL'
    mutation($input: MinimizeCommentInput!) {
      minimizeComment(input: $input)
    }
  GRAPHQL

  def minimize_comment
    data = platform_execute(MinimizeCommentMutation, variables: {
      input: {
        subjectId: params[:comment_id],
        classifier: params[:classifier],
      },
    })

    if data.errors.any?
      respond_to do |format|
        format.json do
          render json: { error: "could not minimize comment" }, status: :forbidden
        end
        format.all do
          render plain: "could not minimize comment", status: :forbidden
        end
      end
    else
      if request.xhr?
        head :ok
      else
        redirect_to :back
      end
    end
  end

  UnminimizeCommentMutation = parse_query <<~'GRAPHQL'
    mutation($input: UnminimizeCommentInput!) {
      unminimizeComment(input: $input)
    }
  GRAPHQL

  def unminimize_comment
    data = platform_execute(UnminimizeCommentMutation, variables: {
      input: {
        subjectId: params[:comment_id],
      },
    })

    if data.errors.any?
      respond_to do |format|
        format.json do
          render json: { error: "could not unminimize comment" }, status: :forbidden
        end
        format.all do
          render plain: "could not minimize comment", status: :forbidden
        end
      end
    else
      if request.xhr?
        head :ok
      else
        redirect_to :back
      end
    end
  end

  # See Coconductor::Field.all.map(&:key) for a list of valid fields
  def code_of_conduct_tool
   code_of_conduct = params[:template] ? CodeOfConduct.find_by_key(params[:template]) : nil
   owner = current_repository.owner
   render "/community/add_code_of_conduct", locals: {
     code_of_conduct: code_of_conduct,
     community_name: current_repository.name.titleize,
     email_address: owner.publicly_visible_email(logged_in: true),
     contact_info: owner.publicly_visible_email(logged_in: true),
     governing_body: (owner if owner.organization?),
   }
  end

  def license_tool
    if params[:template]
      @license = License.find(params[:template])
    else
      @license = nil
    end

    render "community/add_license", locals: { license: @license }
  end

  private

  def community_profile
    current_repository.community_profile || CommunityProfile.new(repository_id: current_repository.id)
  end

  def non_forks_only
    render_404 if current_repository.fork?
  end

  def community_profile_enabled?
    GitHub.community_profile_enabled?
  end
end
