# frozen_string_literal: true

class RepositoryFilesController < AbstractRepositoryController
  areas_of_responsibility :code_collab

  statsd_tag_actions only: :show

  def show
    file = RepositoryFile.where(
      id: params[:id],
      name: params[:path],
      repository_id: current_repository.id,
      state: 1, # uploaded
    ).first

    return render_404 unless file

    file.download
    redirect_to file.redirect_url(actor: current_user)
  end
end
