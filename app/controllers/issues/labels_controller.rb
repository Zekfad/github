# frozen_string_literal: true

class Issues::LabelsController < IssuesController

  before_action :issue_required

  # These `before_action`s are not needed, since the mutation for `#update`
  # handles if the user is authorized to perform this action.
  skip_before_action :issue_modifiers_only, only: %w(update)
  skip_before_action :writable_repository_required, only: %w(update)
  skip_before_action :content_authorization_required, only: %w(update)

  UpdateLabelsMutation = parse_query <<-'GRAPHQL'
    mutation($input: ReplaceLabelsForLabelableInput!) {
      replaceLabelsForLabelable(input: $input) {
        errors {
          message
        }
      }
    }
  GRAPHQL

  def update
    label_attrs = []
    label_ids = params[:issue][:labels]

    if label_ids.any?
      labels = current_repository.load_labels(label_ids)

      labels.each do |label|
        label_attrs << { name: label.name }
      end
    end

    variables = {
      input: {
        labelableId: current_issue.global_relay_id,
        labels: label_attrs,
      },
    }

    data = platform_execute(UpdateLabelsMutation, variables: variables)
    track_issue_edits_from_project_board(edited_fields: ["labels"]) unless data.errors.any?

    if data.errors.any?
      error_type = data.errors.details[:replaceLabelsForLabelable]&.first["type"]
      return head :not_found if error_type == "NOT_FOUND"
      return head :forbidden if error_type == "FORBIDDEN"
      return head :unprocessable_entity
    end

    if data.replace_labels_for_labelable.errors.any?
      return head :unprocessable_entity
    end

    respond_to do |format|
      format.html do
        render partial: "issues/sidebar/show/labels", locals: { issue: current_issue.reload }
      end
    end
  end
end
