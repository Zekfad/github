# frozen_string_literal: true

class MarketplaceListingInstallationsController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  before_action :login_required

  NewQuery = parse_query <<-'GRAPHQL'
    query($subscriptionItemId: ID!) {
      subscriptionItem: node(id: $subscriptionItemId) {
        ... on SubscriptionItem {
          account {
            ... on User {
              databaseId
            }

            ... on Organization {
              id
              databaseId
            }
          }

          marketplaceListingPlan: subscribable {
            ... on MarketplaceListingPlan {
              databaseId

              listing {
                slug
                oauthApplicationDatabaseId
                app {
                  ownerScopedGitHubAppsEnabled: isFeatureEnabled(name: "owner_scoped_github_apps")
                  slug
                  owner {
                    ... on User {
                      __typename
                      login
                    }
                    ... on Organization {
                      __typename
                      login
                    }
                    ... on Enterprise {
                      __typename
                      slug
                    }
                  }
                }
                installationUrl
              }
            }
          }
        }
      }
    }
  GRAPHQL

  GrantOapForMarketplaceListingQuery = parse_query <<-'GRAPHQL'
    mutation($organizationId: ID!, $listingSlug: String!) {
      grantOapForMarketplaceListing(input: { organizationId: $organizationId, listingSlug: $listingSlug })
    }
  GRAPHQL

  def new
    variables = { subscriptionItemId: params[:subscription_item_id] }
    data = platform_execute(NewQuery, variables: variables)
    subscription_item = data.subscription_item

    return render_404 unless subscription_item && subscription_item.is_a?(PlatformTypes::SubscriptionItem)

    listing_plan = subscription_item.marketplace_listing_plan
    listing = listing_plan.listing
    listing_app = listing.app

    if params[:grant_oap].present? && listing.oauth_application_database_id.present?
      account = subscription_item.account
      platform_execute(GrantOapForMarketplaceListingQuery, variables: { organizationId: account.id, listingSlug: listing.slug })
    end

    if listing_app
      target_account = subscription_item.account

      query_params = { target_id: target_account.database_id }
      read_suggested_target_id_cookie(query_params)
      read_repository_ids_cookie(query_params)

      # Because of https://git.io/v1syX
      # we have to pass in the attributes rather than the object
      # to a special routing method.
      options = {
        app:                              listing_app.slug,
        owner_scoped_github_apps_enabled: listing_app.owner_scoped_github_apps_enabled,
        owner_type:                       listing_app.owner.__typename,
        query_params:                     query_params,
      }

      if listing_app.owner.__typename == "Business"
        options[:owner] = listing_app.owner.slug
      else
        options[:owner] = listing_app.owner.login
      end

      redirect_to gh_graphql_app_installation_permissions_path(**options)
    else
      redirect_to listing_installation_url_with_plan(listing_plan)
    end
  end

  private

  def listing_installation_url_with_plan(listing_plan)
    listing = listing_plan.listing
    uri = URI(listing.installation_url)
    existing_params = URI.decode_www_form(uri.query || "")
    existing_params.push(["marketplace_listing_plan_id", listing_plan.database_id])
    uri.query = URI.encode_www_form(existing_params)
    uri.to_s
  end

  def read_suggested_target_id_cookie(query_params)
    return unless cookies[:marketplace_suggested_target_id].present?

    parsed_val = JSON.parse(cookies.delete :marketplace_suggested_target_id)
    query_params[:suggested_target_id] = parsed_val
  end

  def read_repository_ids_cookie(query_params)
    return unless cookies[:marketplace_repository_ids].present?

    parsed_val = JSON.parse(cookies.delete :marketplace_repository_ids)
    query_params[:repository_ids] = parsed_val
  end

  def target_for_conditional_access
    typed_object_from_id([Platform::Objects::SubscriptionItem], params[:subscription_item_id]).user
  rescue Platform::Errors::NotFound
    return :no_target_for_conditional_access # Billing::SubscriptionItem not found
  end
end
