# frozen_string_literal: true

class Orgs::MemexItemContentsController < Orgs::Controller
  before_action :require_organization_member
  before_action :require_this_content
  before_action :require_this_content_owned_by_org
  before_action :require_this_content_readable
  before_action :require_memex_sidebar_enabled

  def suggested_memexes
    org_memexes = this_organization.memex_projects.order(number: :desc)
    possible_memexes = org_memexes - this_content_memex_projects

    render partial: "issues/sidebar/memexes_menu_content", locals: {
      issue_memex_items: this_content_memex_items,
      possible_memexes: possible_memexes,
    }
  end

  def set_memexes
    memexes_to_add = MemexProject.where(id: memex_ids_to_add)
    memexes_to_add.each do |memex|
      item = memex.build_item(creator: current_user, issue_or_pull: this_content)
      memex.save_with_lowest_priority!(item) if item.valid?
    end

    this_content_memex_items.each do |item|
      item.destroy unless memex_ids_to_keep.include?(item.memex_project.id)
    end

    render partial: "issues/sidebar/show/memexes", locals: { issue: this_content.to_issue }
  end

  private

  def require_organization_member
    render_404 unless this_organization.direct_or_team_member?(current_user)
  end

  def require_this_content
    render_404 unless this_content
  end

  def require_this_content_owned_by_org
    render_404 unless this_content.repository.owner == this_organization
  end

  def require_this_content_readable
    render_404 unless this_content.readable_by?(current_user)
  end

  def require_memex_sidebar_enabled
    render_404 unless memex_beta_enabled? && memex_issue_sidebar_enabled?
  end

  def this_content
    return @this_content if defined?(@this_content)
    return if content_type == MemexProjectItem::DRAFT_ISSUE_TYPE

    @this_content = if content_type == MemexProjectItem::ISSUE_TYPE
      Issue.includes(memex_project_items: :memex_project).find_by(id: content_id)
    elsif content_type == MemexProjectItem::PULL_REQUEST_TYPE
      PullRequest.includes(memex_project_items: :memex_project).find_by(id: content_id)
    end
  end

  def content_type
    @content_type ||= params[:content_type]
  end

  def content_id
    @content_id ||= params[:content_id]
  end

  def this_content_memex_items
    @this_content_memex_items ||= this_content.memex_project_items
  end

  def this_content_memex_projects
    @this_content_memex_projects ||= this_content_memex_items.map(&:memex_project).uniq
  end

  def memex_ids_to_add
    @memex_ids_to_add ||= params[:memex_ids_to_add] || []
  end

  def memex_ids_to_keep
    @memex_ids_to_keep ||= params[:memex_ids_to_keep]&.map(&:to_i) || []
  end
end
