# frozen_string_literal: true

class Orgs::MarketingController < ApplicationController
  areas_of_responsibility :orgs

  before_action :redirect_to_org_new

  def index
    render_404
  end

  def require_conditional_access_checks?
    false
  end

  private

  # Deprecating the Organization migration pages and functionality -
  # now just redirect the user to the new Organization page
  def redirect_to_org_new
    redirect_to new_organization_path
  end
end
