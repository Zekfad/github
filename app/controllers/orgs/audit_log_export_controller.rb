# frozen_string_literal: true

class Orgs::AuditLogExportController < Orgs::Controller
  include AuditLogExportHelper

  areas_of_responsibility :orgs, :audit_log

  statsd_tag_actions only: :create

  before_action :organization_admin_required, :audit_log_export_required

  def show
    export = this_organization.audit_log_exports.find_by_token!(params[:token])
    render_audit_log_export(export)
  end

  def create
    options = {
      actor: current_user,
      phrase: params[:q],
      format: params[:export_format],
    }

    export = this_organization.audit_log_exports.create(options)
    respond_with_audit_log_export \
      export: export,
      export_url: org_audit_log_export_url(token: export.token, format: export.format)
  end
end
