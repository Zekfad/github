# frozen_string_literal: true

class Orgs::AttributionInvitationsController < Orgs::Controller
  before_action :login_required
  before_action :organization_admin_required, except: :index

  def index
    invitations = AttributionInvitation.where(target: current_user, owner: this_organization)

    render "orgs/attribution_invitations/index", locals: { invitations: invitations }
  end

  def create
    source = Mannequin.find_by_login(params[:source_login])
    target = User.find_by_login(params[:target_login])

    attribution_invitation = AttributionInvitation.new(
      source: source,
      target: target,
      owner: this_organization,
      creator: current_user,
    )

    if attribution_invitation.save
      flash[:notice] = "Successfully sent a reattribution invitation to #{target.login}"
    else
      flash[:error] = "Could not invite #{target.login} to claim this data"
    end

    redirect_to settings_org_import_export_path(this_organization)
  end

  def target_suggestions
    headers["Cache-Control"] = "no-cache, no-store"

    respond_to do |format|
      format.html_fragment do
        render_partial_view "orgs/attribution_invitations/target_suggestions",
          AutocompleteView,
          org_members_only: true,
          organization: this_organization,
          query: params[:q]
      end
      format.html do
        render_partial_view "orgs/attribution_invitations/target_suggestions",
          AutocompleteView,
          org_members_only: true,
          organization: this_organization,
          query: params[:q]
      end
    end
  end
end
