# frozen_string_literal: true

module Orgs::InvitationsControllerMethods
  extend ActiveSupport::Concern

  module ClassMethods
    def limit_invitation_roles(*roles)
      @limited_invitation_roles ||= []
      @limited_invitation_roles += Array.wrap(roles.map(&:to_s))
    end

    def limited_invitation_roles
      @limited_invitation_roles || []
    end
  end

  private

  # Internal: Finds a pending invitation for the current user or from the
  # supplied invitation token.
  #
  # Returns an OrganizationInvitation or renders a response.
  def find_pending_invitation
    @pending_invitation ||= if email_invitation?
      find_pending_email_invitation
    elsif logged_in?
      find_pending_user_invitation
    end

    @pending_invitation || pending_invitation_not_found
  end
  attr_reader :pending_invitation

  def pending_invitation_not_found
    if login_required_to_view_invitation?
      login_required
    elsif already_member?
      redirect_to_return_to_or_org_profile
    else
      render "orgs/invitations/not_found", status: :not_found,
        locals: { organization: this_organization }
    end
  end

  def render_sign_up_via_invitation(invitation:, invitation_token: nil)
    render_template_view "orgs/invitations/sign_up_via_invitation", Orgs::Invitations::ShowPendingPageView,
      {invitation: invitation, invitation_token: invitation_token}, layout: "session_authentication"
  end

  def find_pending_email_invitation
    return unless email_invitation?
    this_organization.pending_invitations.
      with_business_role(*self.class.limited_invitation_roles).
      find_by_token(params[:invitation_token])
  end

  def find_pending_user_invitation
    this_organization.pending_invitation_for \
      current_user,
      role: self.class.limited_invitation_roles
  end

  def email_invitation?
    params[:invitation_token].present?
  end

  def login_required_to_view_invitation?
    !logged_in? && !email_invitation?
  end

  def already_member?
    this_organization.direct_or_team_member?(current_user)
  end

  def sso_required_for_joining?
    this_organization.external_identity_session_owner.saml_sso_enforced? || pending_invitation.external_identity.present?
  end

  def redirect_to_return_to_or_org_profile
    if params[:return_to].present?
      safe_redirect_to params[:return_to], fallback: user_path(this_organization)
    else
      redirect_to user_path(this_organization)
    end
  end
end
