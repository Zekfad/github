# frozen_string_literal: true

class Orgs::AnnouncementsController < Orgs::Controller

  before_action :require_organization_discussions_feature

  PAGE_SIZE = 10

  IndexQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $perPage: Int!,
      $loggedIn: Boolean!,
      $before: String,
      $after: String,
      $isPinned: Boolean
    ) {
      viewer @include(if: $loggedIn) {
        ...Views::Orgs::Announcements::Index::Viewer
      }
      organization(login: $login) {
        ...Views::Orgs::Announcements::Index::Organization
      }
    }
  GRAPHQL

  def index
    pinned_only = params[:pinned].present? ? params[:pinned] == "1" : nil
    variables = {
      login: params[:org],
      perPage: PAGE_SIZE,
      loggedIn: logged_in?,
      before: params[:before],
      after: params[:after],
      isPinned: pinned_only,
    }

    data = platform_execute(IndexQuery, variables: variables)
    render "orgs/announcements/index", locals: {
      org: data.organization,
      viewer: data.viewer,
      selected_tab: pinned_only ? :pinned : :recent,
    }
  end

  CreateOrganizationDiscussionMutation = parse_query <<-'GRAPHQL'
    mutation(
      $input: CreateOrganizationDiscussionInput!,
      $perPage: Int!,
      $before: String,
      $after: String,
      $isPinned: Boolean
    ) {
      createOrganizationDiscussion(input: $input)  {
        discussion {
          organization {
            login
            ...Views::Orgs::Announcements::ListWithMarker::Organization
            ...Views::Orgs::Announcements::CursorInput::Organization
            ...Views::Orgs::Announcements::List::Organization
          }
          author {
            ...Views::Orgs::Announcements::List::Viewer
          }
        }
      }
    }
  GRAPHQL

  def create
    return render_404 unless logged_in?

    input = creation_params
    input[:private] = input[:private] == "1"

    mutation = platform_execute(
      CreateOrganizationDiscussionMutation,
      variables: {
        input: input,
        before: params[:before].presence,
        after: params[:after].presence,
        perPage: PAGE_SIZE,
        isPinned: nil,
      })

    return render_graphql_error(mutation) if mutation.errors.any?

    discussion = mutation.create_organization_discussion.discussion

    respond_to do |format|
      format.html do
        redirect_to org_announcements_path(discussion.organization.login)
      end
      format.json do
        template = render_to_string(
          partial: "orgs/announcements/list_with_marker",
          locals: { org: discussion.organization, viewer: discussion.author },
          formats: [:html],
          )
        cursor_template = render_to_string(
          partial: "orgs/announcements/cursor_input",
          locals: { org: discussion.organization },
          formats: [:html],
        )

        partials = {
          "#cursor_input" => cursor_template,
          "#timeline_marker" => template,
        }
        render json: { updateContent: partials }
      end
    end
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateOrganizationDiscussionInput!) {
      updateOrganizationDiscussion(input: $input) {
        discussion {
          title
          body
          bodyHTML
          bodyVersion
        }
      }
    }
  GRAPHQL

  def update
    return render_404 unless logged_in?
    data = platform_execute(UpdateQuery, variables: { input: update_input })

    respond_to do |wants|
      wants.json do
        if data.errors.any?
          stale_model = data.errors.details[:updateOrganizationDiscussion].first["type"] == "STALE_DATA"

          # Our stale model detection JS requires us to return a JSON payload whose errors are
          # not nested under the "errors" key, unlike the error payload returned by default from
          # `render_graphql_error`.
          return render_graphql_error(data, active_model_errors_format: !stale_model)
        end

        updated_discussion = data.update_organization_discussion.discussion

        render json: {
          title: updated_discussion.title,
          source: updated_discussion.body,
          body: updated_discussion.body_html,
          newBodyVersion: updated_discussion.body_version,
        }
      end

      wants.html do
        redirect_to :back
      end
    end
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteOrganizationDiscussionInput!) {
      deleteOrganizationDiscussion(input: $input)
    }
  GRAPHQL

  def destroy
    input = deletion_params
    data = platform_execute(DestroyQuery, variables: { input: input })

    return render_graphql_error(data) if data.errors.any?
    return head(:ok) if request.xhr?

    redirect_to :back
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $loggedIn: Boolean!,
      $number: Int!
    ) {
      ...Views::Orgs::Announcements::Show::Root

      viewer @include(if: $loggedIn) {
        ...Views::Orgs::Announcements::Show::Viewer
      }

      organization(login: $login) {
        discussion(number: $number) {
          isPrivate
          ...Views::Orgs::Announcements::Discussion::OrganizationDiscussion
        }
      }
    }
  GRAPHQL

  def show
    variables = {
      login: params[:org] || params[:organization_id],
      loggedIn: logged_in?,
      number: params[:number].to_i,
    }

    data = platform_execute(ShowQuery, variables: variables)

    return render_404 unless data.organization&.discussion

    respond_to do |wants|
      wants.html do
        return render_graphql_error(data) if data.errors.any?
        render(
          "orgs/announcements/show",
          locals: {
            data: data,
            viewer: data.viewer,
            graphql_org: data.organization,
          })
      end
    end
  end

  private

  def require_organization_discussions_feature
    render_404 unless organization_discussions_enabled?
  end

  def creation_params
    params.require(:input).permit %i[title body private organizationId]
  end

  def update_params
    params.require(:organization_discussion).permit %i[title body bodyVersion id pinned]
  end

  def deletion_params
    params.require(:input).permit :id
  end

  def update_input
    input = update_params

    task_list_update = TaskListOperation.from(params[:task_list_operation]).try(:call, input[:body])
    input[:body] = task_list_update || input[:body]
    input[:pinned] = input[:pinned].nil? ? nil : input[:pinned] == "1"

    input
  end
end
