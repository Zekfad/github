# frozen_string_literal: true

class Orgs::TeamDiscussionsController < Orgs::Controller
  areas_of_responsibility :teams

  statsd_tag_actions only: [:create, :index, :show]

  include Orgs::Invitations::RateLimiting

  layout "team_two_column"

  before_action :login_required
  before_action :require_team_discussions_allowed, except: [:index]
  before_action :mask_analytics_data

  after_action TaskListInstrumentUpdateFilter, only: [:update]

  PAGE_SIZE = 10
  DEFAULT_COMMENT_LIMIT = 3

  IndexQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $slug: String!,
      $isPinned: Boolean,
      $before: String,
      $after: String,
      $limit: Int!,
      $fromComment: Int,
      $firstNComments: Int,
      $lastNComments: Int,
      $afterComment: String,
      $beforeComment: String,
      $showAdminStuff: Boolean!,
      $orgHasLimitedSeats: Boolean!,
      $teamDiscussionsEnabled: Boolean!,
    ) {

      ...Views::Orgs::TeamDiscussions::Index::Root
      organization(login: $login) {
        login
        viewerCanAdminister
        team(slug: $slug) {
          isLegacyOwnersTeam
        }
        ...Views::Layouts::Team::Organization
      }
    }
  GRAPHQL

  ListQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $slug: String!,
      $isPinned: Boolean,
      $before: String,
      $after: String,
      $limit: Int!,
      $fromComment: Int,
      $firstNComments: Int,
      $lastNComments: Int,
      $afterComment: String,
      $beforeComment: String) {

      viewer {
        ...Views::Orgs::TeamDiscussions::TeamDiscussion::Viewer
      }
      organization(login: $login) {
        team(slug: $slug) {
          ...Views::Orgs::TeamDiscussions::List::Team
        }
      }
    }
  GRAPHQL

  def index
    team_discussions_enabled = this_organization.team_discussions_allowed?

    variables = {
      before: params[:before],
      after: params[:after],
      limit: PAGE_SIZE,
      login: params[:org] || params[:organization_id],
      slug: params[:team_slug],
      isPinned: pinned_discussions_tab_selected? ? true : nil,
      lastNComments: DEFAULT_COMMENT_LIMIT,
      showAdminStuff: !!this_team&.adminable_by?(current_user),
      orgHasLimitedSeats: this_organization.plan.per_seat? && !this_organization.has_unlimited_seats?,
      teamDiscussionsEnabled: team_discussions_enabled,
    }

    respond_to do |format|
      format.html do
        if request.xhr?
          data = platform_execute(ListQuery, variables: variables)
          return render_404 unless data.organization&.team
          render(
            partial: "orgs/team_discussions/list",
            locals: {
              selected_tab: selected_tab,
              team: data.organization.team,
              viewer: data.viewer,
            })
        else
          data = platform_execute(IndexQuery, variables: variables)
          return render_404 unless team = data.organization&.team
          if team.is_legacy_owners_team? && data.organization.viewer_can_administer?
            # Redirect to the upgrade view. This is unrelated to discussions, but it makes sense here because
            # this action handles the default team route (orgs/<org>/teams/<team>).
            redirect_to org_owners_team_path(data.organization.login)
          else
            set_hovercard_subject(this_organization)
            render(
              "orgs/team_discussions/index",
              locals: {
                data: data,
                graphql_org: data.organization,
                rate_limited: org_invite_rate_limited?,
                selected_nav_item: :discussions,
                selected_tab: selected_tab,
                team_discussions_enabled: team_discussions_enabled,
              })
          end
        end
      end
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $slug: String!,
      $number: Int!,
      $fromComment: Int,
      $firstNComments: Int,
      $lastNComments: Int,
      $afterComment: String,
      $beforeComment: String
    ) {
      ...Views::Orgs::TeamDiscussions::Show::Root
      organization(login: $login) {
        team(slug: $slug) {
          discussion(number: $number) {
            ...Views::Orgs::TeamDiscussions::TeamDiscussion::TeamDiscussion
          }
        }
        ...Views::Layouts::Team::Organization
      }
    }
  GRAPHQL

  def show
    # Extract the comment index in a backwards-compatible way.
    comment_index_param = params[:from_comment] || params[:from_reply]

    variables = {
      fromComment: comment_index_param&.to_i,
      login: params[:org] || params[:organization_id],
      number: params[:number].to_i,
      slug: params[:team_slug],
    }
    variables[comment_index_param.presence ? :firstNComments : :lastNComments] = 20
    data = platform_execute(ShowQuery, variables: variables)
    return render_404 unless data.organization&.team&.discussion

    set_hovercard_subject(this_organization)

    respond_to do |wants|
      wants.html do
        return render_graphql_error(data) if data.errors.any?
        render(
          "orgs/team_discussions/show",
          locals: {
            data: data,
            from_comment: comment_index_param&.to_i,
            graphql_org: data.organization,
          })
      end
    end
  end

  CreateTeamDiscussionMutation = parse_query <<-'GRAPHQL'
    mutation(
      $input: CreateTeamDiscussionInput!,
      $before: String,
      $after: String,
      $limit: Int!,
      $fromComment: Int,
      $firstNComments: Int,
      $lastNComments: Int,
      $afterComment: String,
      $beforeComment: String,
      $isPinned: Boolean
    ) {
      createTeamDiscussion(input: $input)  {
        teamDiscussion {
          team {
            ...Views::Orgs::TeamDiscussions::ListWithMarker::Team
            ...Views::Orgs::TeamDiscussions::CursorInput::Team
          }
          author {
            ...Views::Orgs::TeamDiscussions::TeamDiscussion::Viewer
          }
        }
      }
    }
  GRAPHQL

  def create
    input = creation_params
    input[:private] = input[:private] == "1"

    mutation = platform_execute(
      CreateTeamDiscussionMutation,
      variables: {
        input: input,
        before: params[:before].presence,
        after: params[:after].presence,
        limit: PAGE_SIZE,
        lastNComments: DEFAULT_COMMENT_LIMIT,
        isPinned: false,
      })

    respond_to do |format|
      format.json do
        return render_graphql_error(mutation) if mutation.errors.any?

        discussion = mutation.create_team_discussion.team_discussion
        template = render_to_string(
          partial: "orgs/team_discussions/list_with_marker",
          locals: { team: discussion.team, viewer: discussion.author },
          formats: [:html],
          )
        cursor_template = render_to_string(
          partial: "orgs/team_discussions/cursor_input",
          locals: { team: discussion.team },
          formats: [:html],
        )
        partials = {
          "#cursor_input" => cursor_template,
          "#timeline_marker" => template,
        }
        render json: {updateContent: partials}
      end
    end
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateTeamDiscussionInput!) {
      updateTeamDiscussion(input: $input) {
        teamDiscussion {
          title
          body
          bodyHTML
          bodyVersion
        }
      }
    }
  GRAPHQL

  def update
    data = platform_execute(UpdateQuery, variables: { input: graphql_input_for_update })

    respond_to do |wants|
      wants.json do
        if data.errors.any?
          stale_model = data.errors.details[:updateTeamDiscussion].first["type"] == "STALE_DATA"

          # Our stale model detection JS requires us to return a JSON payload whose errors are
          # not nested under the "errors" key, unlike the error payload returned by default from
          # `render_graphql_error`.
          return render_graphql_error(data, active_model_errors_format: !stale_model)
        end

        updated_discussion = data.update_team_discussion.team_discussion

        render json: {
          title: updated_discussion.title,
          source: updated_discussion.body,
          body: updated_discussion.body_html,
          newBodyVersion: updated_discussion.body_version,
        }
      end

      wants.html do
        redirect_to :back
      end
    end
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteTeamDiscussionInput!) {
      deleteTeamDiscussion(input: $input)
    }
  GRAPHQL

  def destroy
    input = deletion_params
    data = platform_execute(DestroyQuery, variables: { input: input })

    return render_graphql_error(data) if data.errors.any?

    return head(:ok) if request.xhr?

    redirect_to :back
  end

  private

  def creation_params
    params.require(:input).permit %i[title body private teamId]
  end

  def update_params
    params.require(:team_discussion).permit %i[title body bodyVersion id pinned]
  end

  def deletion_params
    params.require(:input).permit :id
  end

  def selected_tab
    @selected_tab ||= pinned_discussions_tab_selected? ? :pinned : :recent
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def pinned_discussions_tab_selected?
    @pinned_discussions_tab_selected ||= params[:pinned] == "1"
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def require_team_discussions_allowed
    return render_404 unless this_organization.team_discussions_allowed?
  end

  def graphql_input_for_update
    input = update_params

    task_list_update = TaskListOperation.from(params[:task_list_operation]).try(:call, input[:body])
    input[:body] = task_list_update || input[:body]

    if input[:pinned] == "1"
      input[:pinned] = true
    elsif input[:pinned] == "0"
      input[:pinned] = false
    else
      input[:pinned] = nil
    end

    input
  end

  # Prepares client side data to report to google analytics
  def mask_analytics_data
    url = "/orgs/<org-login>/teams/<team-name>/discussions/#{action_name}"
    override_analytics_location url
  end
end
