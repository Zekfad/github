# frozen_string_literal: true

class Orgs::RemindersController < Orgs::Controller
  include RemindersMethods

  areas_of_responsibility :ce_extensibility

  statsd_tag_actions only: [:create, :update, :index]

  before_action :redirect_organization_members
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :set_reminder!, only: [:show, :update, :destroy, :reminder_test]

  before_action { @selected_link = :reminders }

  def index
    respond_to do |format|
      format.html do
        render_template_view(
          "orgs/reminders/index",
          Reminders::IndexView,
          {
            slack_workspaces: slack_workspaces,
            current_user: current_user,
            organization: this_organization,
            slack_installation: this_slack_installation,
          },
        )
      end
    end
  end

  # The rest of the controller methods are handled in `RemindersMethods`

  private

  def render_edit(reminder)
    respond_to do |format|
      format.html do
        render_template_view(
          "orgs/reminders/edit",
          ::Reminders::EditView,
          {
            slack_workspaces: slack_workspaces,
            reminder: reminder,
          },
        )
      end
    end
  end

  def redirect_organization_members
    return unless this_organization

    if this_organization.member?(current_user) && !this_organization.adminable_by?(current_user)
      flash[:error] = "You must be an organization owner to manage organization reminders."
      redirect_back(fallback_location: user_path(this_organization))
    end
  end
end
