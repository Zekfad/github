# frozen_string_literal: true

class Orgs::UserLicensesController < Orgs::Controller
  areas_of_responsibility :gitcoin

  before_action :login_required
  before_action :organization_admin_required
  before_action :sudo_filter
  before_action :verify_volume_licensing

  def update
    user_license = business.user_licenses.ensure_exists(user_id: person.id)

    if user_license.present? && user_license.update(user_license_params)
      redirect_to org_people_path, notice: "Updated #{person.login}'s license."
    else
      redirect_to org_people_path, alert: "Could not update #{person.login}'s license."
    end
  end

  private

  def verify_volume_licensing
    return render_404 unless business.volume_licensing_enabled?
  end

  def person
    @_person ||= User.find_by_login!(params[:person_login])
  end

  def business
    @_business ||= this_organization.business
  end

  def user_license_params
    params.require(:user_license).permit(:license_type)
  end
end
