# frozen_string_literal: true

class Orgs::LabelSettingsController < Orgs::Controller
  before_action :org_admins_only
  before_action :ensure_trade_restrictions_allows_org_settings_access

  include OrganizationsHelper

  def create
    name = label_params[:name]
    color = label_params[:color]&.delete("#") # Strip any CSS-style hex codes
    description = label_params[:description]

    label = current_organization.user_labels.create(name: name,
                                                    color: color,
                                                    description: description)

    if label.persisted?
      label.instrument_creation(context: "organization settings")
    end

    respond_to do |format|
      format.html do
        if request.xhr?
          if label.errors.any?
            json = label.errors.as_json(full_messages: true)
            json[:message] = label.errors.full_messages.join(", ")
            render json: json, status: :unprocessable_entity
          else
            render partial: "orgs/label_settings/label", object: label
          end
        else
          redirect_to :back
        end
      end
    end
  end

  def update
    return head(:not_found) unless label = current_organization.user_labels.find_by_id(params[:id])

    color = label_params[:color]&.delete("#") # Strip any CSS-style hex codes
    label.name = label_params[:name] if label_params[:name]
    label.color = color if color
    label.description = label_params[:description]

    label.save

    respond_to do |format|
      format.html do
        if request.xhr?
          if label.errors.any?
            json = label.errors.as_json(full_messages: true)
            json[:message] = label.errors.full_messages.join(", ")
            render json: json, status: :unprocessable_entity
          else
            render partial: "orgs/label_settings/label", object: label
          end
        else
          redirect_to :back
        end
      end
    end
  end

  def destroy
    label = current_organization.user_labels.find_by_id(params[:id])
    label.destroy if label
    if request.xhr?
      head(label ? 200 : 404)
    else
      redirect_to :back
    end
  end

  def preview
    label = current_organization.user_labels.find_by_id(params[:id]) if params[:id].present?
    label ||= current_organization.user_labels.new

    label.name = params[:name]
    label.description = params[:description]
    label.color = params[:color]

    unless label.valid?
      json = label.errors.as_json(full_messages: true)
      json[:message] = label.errors.full_messages.join(", ")
      return render(json: json, status: :unprocessable_entity)
    end

    respond_to do |format|
      format.html do
        render partial: "orgs/label_settings/preview", locals: { label: label }, layout: false
      end
    end
  end

  private

  def label_params
    params.require(:label).permit(:name, :color, :description)
  end
end
