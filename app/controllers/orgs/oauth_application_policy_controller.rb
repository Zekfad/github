# frozen_string_literal: true

class Orgs::OauthApplicationPolicyController < Orgs::Controller
  areas_of_responsibility :platform

  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :sudo_filter

  javascript_bundle :settings

  def show
    requests =
      this_organization.oauth_application_approvals

    @pending_requests  = requests.pending_approval
    @approved_requests = requests.approved
    @denied_requests   = requests.denied

    render "orgs/oauth_application_policy/show"
  end

  def splash
    render "orgs/oauth_application_policy/splash"
  end
end
