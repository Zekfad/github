# frozen_string_literal: true

class Orgs::SecretsSettingsController < Orgs::Controller
  include OrganizationsHelper
  include Actions::SecretsHelper
  include ActionView::Helpers::NumberHelper

  before_action :login_required
  before_action :ensure_organization_exists
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :ensure_can_use_org_secrets, except: :index

  map_to_service :actions_experience

  javascript_bundle :settings

  VISIBILITIES = {
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_ALL_REPOS => {
      label: "All repositories",
      description: "This secret may be used by any repository in the organization.",
    },
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_PRIVATE_REPOS => {
      label: "Private repositories",
      description: "This secret may be used by any private repository in the organization.",
    },
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_SELECTED_REPOS => {
      label: "Selected repositories",
      description: "This secret may only be used by specifically selected repositories.",
    }
  }.freeze

  ENTERPRISE_VISIBILITIES = {
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_ALL_REPOS => {
      label: "All repositories",
      description: "This secret may be used by any repository in the organization.",
    },
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_PRIVATE_REPOS => {
      label: "Private and internal repositories",
      description: "This secret may be used by any private or internal repository in the organization.",
    },
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_SELECTED_REPOS => {
      label: "Selected repositories",
      description: "This secret may only be used by specifically selected repositories.",
    }
  }.freeze

  FREE_PLAN_VISIBILITIES = {
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_ALL_REPOS => {
      label: "Public repositories",
      description: "This secret may be used by public repositories in the organization.
      Paid GitHub plans include private repositories.",
    },
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_PRIVATE_REPOS => {
      label: "Private repositories",
      description: "Organization secrets cannot be used by private repositories with your plan.",
      disabled: true
    },
    GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_SELECTED_REPOS => {
      label: "Selected repositories",
      description: "This secret may only be used by specifically selected repositories.",
    }
  }.freeze

  def index
    secrets = secrets_for(current_organization, app: actions_integration).map { |secret| {
        name: secret.name,
        visibility_description: visibility_description_for(secret, can_use_secrets_for_private_repos?),
        updated_at: secret_last_update(secret)
      }
    }
    render "settings/organization/secrets/index", locals: {
      can_use_org_secrets: can_use_org_secrets?,
      can_use_secrets_for_private_repos: can_use_secrets_for_private_repos?,
      secrets: secrets,
    }
  end

  def new_secret
    if can_use_secrets_for_private_repos?
      default_visibility = GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_PRIVATE_REPOS
    else
      default_visibility = GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_ALL_REPOS
    end

    render "settings/organization/secrets/new_secret", locals: {
      public_key: github_public_key(current_organization),
      visibilities: visibilities,
      default_visibility: default_visibility,
      can_use_secrets_for_private_repos: can_use_secrets_for_private_repos?,
    }
  end

  def create_secret
    id, encoded = github_public_key(current_organization)

    if params[:encrypted_value].empty? || params[:key_id].to_i != id
      flash[:error] = "Failed to add secret. Please try again."
      redirect_to settings_org_secrets_path
      return
    end

    validation = Credz.validate_secret(params[:name], params[:encrypted_value])
    unless validation.succeeded?
      flash[:error] = validation.error
      redirect_to settings_org_secrets_path
      return
    end

    value = if GitHub.enterprise?
      decrypt_enterprise_secret(params[:encrypted_value])
    else
      Earthsmoke::Embedding.embed(id, Base64.strict_decode64(params[:encrypted_value]))
    end

    encoded_value = Base64.strict_encode64(value)

    visibility = params[:visibility]
    repository_node_ids = visibility == GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_SELECTED_REPOS.to_s ? Array(params[:repository_ids]) : []
    repository_ids = repository_node_ids.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    selected_repositories = current_organization.repositories.where(id: repository_ids).order(:id).map(&:global_relay_id)

    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.store_credential \
        app:   actions_integration,
        owner:  current_organization,
        actor: current_user,
        key:   params[:name],
        value: encoded_value,
        visibility: visibility.to_sym,
        selected_repositories: selected_repositories
    end

    if !result.call_succeeded?
      if result.status == 429
        flash[:error] = "Failed to add secret, you've reached the #{number_with_delimiter(Credz::SECRET_ORG_MAX)} secret limit."
      else
        flash[:error] = "Failed to add secret."
      end
    else
      flash[:notice] = "Secret added."
    end

    redirect_to settings_org_secrets_path
  end

  def remove_secret_partial
    render_404 unless request.xhr?

    # We need to fetch the secret and check it's visibility here.
    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.fetch_credential(
        app: actions_integration,
        owner: current_organization,
        actor: current_user,
        key: params[:name],
      )
    end

    secret = result.value&.credential
    if secret&.visibility == GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_SELECTED_REPOS
      selected_repositories = secret.selected_repositories.map(&:global_id).to_set
      selected_repository_ids = selected_repositories.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
      repositories = current_organization.repositories.where(id: selected_repository_ids)
    end

    render partial: "settings/organization/secrets/remove_secret",
           locals: {
              secret_name: params[:name],
              repositories: repositories,
           }
  end

  def remove_secret
    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.delete_credential \
        app:  actions_integration,
        owner: current_organization,
        actor: current_user,
        key:  params[:name]
    end

    if !result.value&.success
      flash[:error] = "Failed to delete secret."
    else
      flash[:notice] = "Secret deleted."
    end

    redirect_to settings_org_secrets_path
  end

  def update_secret
    id, encoded = github_public_key(current_organization)

    encoded_value = ""
    unless params[:encrypted_value].empty?
      if params[:key_id].to_i != id
        flash[:error] = "Failed to update secret. Please try again."
        redirect_to settings_org_secrets_path
        return
      end

      validation = Credz.validate_secret(params[:name], params[:encrypted_value])
      unless validation.succeeded?
        flash[:error] = validation.error
        redirect_to settings_org_secrets_path
        return
      end

      value = if GitHub.enterprise?
        decrypt_enterprise_secret(params[:encrypted_value])
      else
        Earthsmoke::Embedding.embed(id, Base64.strict_decode64(params[:encrypted_value]))
      end

      encoded_value = Base64.strict_encode64(value)
    end

    visibility = params[:visibility]
    repository_node_ids = visibility == GitHub::LaunchClient::Credz::CREDENTIAL_VISIBILITY_SELECTED_REPOS.to_s ? Array(params[:repository_ids]) : []
    repository_ids = repository_node_ids.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    selected_repositories = current_organization.repositories.where(id: repository_ids).order(:id).map(&:global_relay_id)

    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.update_credential \
        app:   actions_integration,
        owner: current_organization,
        actor: current_user,
        key:   params[:name],
        value: encoded_value,
        visibility: visibility.to_sym,
        selected_repositories: selected_repositories
    end

    if !result.call_succeeded?
      flash[:error] = "Failed to update secret."
    else
      flash[:notice] = "Secret updated."
    end

    redirect_to settings_org_secrets_path
  end

  def update_secret_page
    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.fetch_credential(
        app: actions_integration,
        owner: current_organization,
        actor: current_user,
        key: params[:name],
      )
    end

    secret = result.value&.credential
    return render_404 unless secret

    selected_repository_node_ids = secret.selected_repositories.map(&:global_id).to_set
    repository_ids = selected_repository_node_ids.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    selected_repositories = current_organization.repositories.where(id: repository_ids)

    render "settings/organization/secrets/update_secret", locals: {
      public_key: github_public_key(current_organization),
      secret: secret,
      repositories: selected_repositories,
      selected_repositories: selected_repository_node_ids,
      selected_visibility: secret.visibility,
      visibilities: visibilities,
      can_use_secrets_for_private_repos: can_use_secrets_for_private_repos?,
    }
  end

  private

  def actions_integration
    # Always store secrets with the prod app, even in lab.
    @integration ||= GitHub.launch_github_app
  end

  def ensure_can_use_org_secrets
    return render_404 unless can_use_org_secrets?
  end

  def can_use_org_secrets?
    current_organization.can_use_org_secrets?
  end

  def can_use_secrets_for_private_repos?
    GitHub.enterprise? || (!current_organization.plan.free? && !current_organization.plan.free_with_addons?)
  end

  def visibilities
    if can_use_secrets_for_private_repos?
      current_organization.business.present? ? ENTERPRISE_VISIBILITIES : VISIBILITIES
    else
      FREE_PLAN_VISIBILITIES
    end
  end
end
