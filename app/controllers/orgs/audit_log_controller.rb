# frozen_string_literal: true

module Orgs
  class AuditLogController < Controller
    areas_of_responsibility :orgs, :audit_log

    statsd_tag_actions only: :index

    before_action :organization_admin_required
    before_action :ensure_trade_restrictions_allows_org_settings_access

    javascript_bundle :settings

    def index
      render_elasticsearch_auditlogs
    end

    def render_elasticsearch_auditlogs
      render_template_view "orgs/audit_log/index",
        Orgs::AuditLog::IndexPageView,
        organization: this_organization,
        query: params[:q],
        page: page,
        after: params[:after],
        before: params[:before],
        git_export_enabled: audit_log_git_event_export_enabled?(this_organization)
    end

    def audit_log_summary
      response = this_organization.audit_log.summary

      render json: response.aggregations["histogram"]
    end

    def suggestions
      headers["Cache-Control"] = "no-cache, no-store"

      respond_to do |format|
        format.html do
          render_partial_view "orgs/audit_log/suggestions",
            Orgs::AuditLog::SuggestionsView,
            organization: this_organization
        end
      end
    end

    protected

    def page
      params[:page].present? ? params[:page] : 1
    end
  end
end
