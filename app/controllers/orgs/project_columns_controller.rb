# frozen_string_literal: true

class Orgs::ProjectColumnsController < Orgs::Controller
  include ProjectColumnControllerActions
  include SharedProjectControllerActions

  statsd_tag_actions only: :show

  before_action :project_read_required
  before_action :require_projects_enabled
  before_action :project_write_required, except: %w[show]
  before_action :set_client_uid

  def show
    render_show_column(project: this_project)
  end

  def create
    create_project_column(project: this_project)
  end

  def update
    update_project_column(project: this_project)
  end

  def reorder
    reorder_project_columns(project: this_project)
  end

  def archive
    archive_project_column(project: this_project)
  end

  def update_workflow
    update_project_column_workflow(project: this_project)
  end

  def automation_options
    render_project_column_automation_options(project: this_project)
  end

  def destroy
    destroy_project_column(project: this_project)
  end

  private

  def this_project
    @this_project ||= this_organization.visible_projects_for(current_user).find_by_number!(params[:project_number])
  end

  def project_read_required
    render_404 unless this_project.readable_by?(current_user)
  end

  def project_write_required
    render_404 unless this_project.writable_by?(current_user)
  end
end
