# frozen_string_literal: true

# Settings related to Organizations, as opposed to Users.
#
# Related security issue: https://github.com/github/github/issues/125004
#
# Actions for this controller previously lived in the SettingsController. Given
# the above issue, they have been extracted out to into this controller pretty
# much whole cloth. For a more acturate git history, check the Settings
# Controller.
class Orgs::SettingsController < Orgs::Controller
  areas_of_responsibility :orgs

  include OrganizationsHelper
  include BusinessesHelper

  before_action :login_required
  before_action :ensure_current_organization, except: [:billing, :spending_limit]
  before_action :ensure_current_organization_for_billing, only: [:billing, :spending_limit]
  before_action :ensure_organization_exists
  before_action :organization_admin_required, except: [:billing, :spending_limit]
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :billing_access_required, only: [:billing, :spending_limit]
  before_action :ensure_billing_enabled, only: [:billing, :spending_limit]
  before_action :ensure_domain_verification_enabled, only: [:domains]
  before_action only: [:spending_limit] do
    check_ofac_sanctions(target: current_organization_for_member_or_billing, redirect_url: settings_org_billing_url(current_organization_for_member_or_billing))
  end

  skip_before_action :cap_pagination, only: [:import_export_mannequins, :import_export_attribution_invitations]

  def profile
    render "settings/organization/profile"
  end

  def member_privileges
    render "settings/organization/member_privileges"
  end

  def security_analysis
    render "settings/organization/security_analysis"
  end

  def teams
    @legacy_admin_teams   = current_organization.teams.legacy_admin
    @legacy_admin_members = current_organization.legacy_admin_members

    render "settings/organization/teams"
  end

  def migrate_legacy_admin_teams
    current_organization.migrate_legacy_admin_teams
    flash[:notice] = "Migrating all of your organization's legacy admin teams. This could take a couple minutes."

    redirect_to settings_org_teams_path(current_organization)
  end

  def projects
    render_template_view "settings/organization/projects",
      ::Settings::Organization::ProjectsView,
      organization: current_organization
  end

  def deleted_repositories
    unless GitHub.enterprise?
      render_template_view "settings/organization/deleted_repositories",
      ::Settings::DeletedRepositoriesView,
      { target: current_organization,
        deleted_repos: Archived::Repository.owned_by(current_organization).network_safe_restoreable.order(updated_at: :desc).limit(100),
      }
    else
      return render_404
    end
  end

  def user_blocks
    render "settings/user_blocks/index"
  end

  def owners
    redirect_to org_owners_path(current_organization)
  end

  OrgBillingQuery = parse_query <<-'GRAPHQL'
    query($login: String!) {
      ...Views::Settings::Organization::Billing::Fragment
    }
  GRAPHQL

  def billing
    owner = current_organization_for_member_or_billing
    ActiveRecord::Base.connected_to(role: :writing) { owner.expire_stale_coupon }

    data = platform_execute OrgBillingQuery, variables: {
      login: owner.login,
    }

    tab = params[:tab]
    return render_404 if tab == "cost_management" && !Billing::Budget.configurable?(owner)
    render "settings/organization/billing", locals: { data: data, selected_tab: tab }
  end

  def spending_limit
    owner = current_organization_for_member_or_billing
    return render_404 unless Billing::Budget.configurable?(owner)
    product = params[:product]
    return render_404 unless Billing::Budget.valid_product?(product)

    unless owner.has_valid_payment_method?
      flash[:error] = "You can’t increase the spending limits until you set up a valid payment method"
      redirect_to :back
      return
    end

    budget = owner.budget_for(product: product)
    budget.configure(
      enforce_spending_limit: params[:enforce_spending_limit] == "true",
      limit: params[:spending_limit],
    )

    if budget.errors.any?
      flash[:error] = "Unable to set a spending limit. Please check your payment method and limit"
    else
      flash[:notice] = "Spending limit configuration has been updated"
    end

    redirect_to :back
  end

  OrgDomainsQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on Organization {
          ...Views::Settings::Organization::Domains::Fragment
          ...Settings::Organization::DomainsView::DomainsViewQuery
        }
      }
    }
  GRAPHQL

  def domains
    data = platform_execute(
      OrgDomainsQuery,
      variables: { "id" => current_organization.global_relay_id },
    )

    org = data.node
    view = Settings::Organization::DomainsView.new(org: org)

    render "settings/organization/domains", locals: { data: org, view: view }
  end

  def members_redirect
    redirect_to org_people_path(current_organization)
  end

  InteractionLimitsQuery = parse_query <<-'GRAPHQL'
    query($login: String!) {
      organization(login: $login) {
        ...Views::Settings::Organization::InteractionLimits::Organization
      }
    }
  GRAPHQL

  def interaction_limits
    return render_404 unless GitHub.interaction_limits_enabled?

    data = platform_execute InteractionLimitsQuery, variables: {
      login: current_organization.login,
    }

    render "settings/organization/interaction_limits", locals: { data: data.organization }
  end

  SetInteractionLimitQuery = parse_query <<-'GRAPHQL'
     mutation($input: SetOrganizationInteractionLimitInput!) {
      setOrganizationInteractionLimit(input: $input)
    }
  GRAPHQL

  def update_interaction_limits
    return render_404 unless GitHub.interaction_limits_enabled?

    input_variables = {
      organizationId: current_organization.global_relay_id,
      limit: params[:interaction_setting],
    }

    results = platform_execute(SetInteractionLimitQuery, variables: { input: input_variables })

    if results.errors.any?
      flash[:error] = results.errors.all.values.flatten.to_sentence
    else
      flash[:notice] = "Organization interaction limit settings saved."
    end

    redirect_to :back
  end

  EnableNotificationRestrictionQuery = parse_query <<-'GRAPHQL'
     mutation($input: EnableOrganizationNotificationRestrictionInput!) {
      enableOrganizationNotificationRestriction(input: $input)
    }
  GRAPHQL

  DisableNotificationRestrictionQuery = parse_query <<-'GRAPHQL'
     mutation($input: DisableOrganizationNotificationRestrictionInput!) {
      disableOrganizationNotificationRestriction(input: $input)
    }
  GRAPHQL

  def set_notification_restriction
    return render_404 unless GitHub.domain_verification_enabled?

    input_variables = {
      organizationId: current_organization.global_relay_id,
    }

    query = (params[:restrict_notifications] == "on") ? EnableNotificationRestrictionQuery : DisableNotificationRestrictionQuery

    results = platform_execute(query, variables: { input: input_variables })

    if results.errors.any?
      flash[:error] = results.errors.all.values.flatten.to_sentence
    else
      flash[:notice] = "Verified domain enforcement settings saved."
    end

    redirect_to :back
  end

  def import_export_mannequins
    if GitHub.flipper[:mannequin_claiming].enabled?(current_organization)
      mannequins = current_organization.mannequins
        .paginate(page: current_page, per_page: 15)

      render "settings/organization/import_export/mannequins",
        locals: {
        mannequins: mannequins,
      }
    else
      redirect_to settings_org_profile_path(current_organization)
    end
  end

  def import_export_attribution_invitations
    if GitHub.flipper[:mannequin_claiming].enabled?(current_organization)
      invitations = current_organization.attribution_invitations
        .paginate(page: current_page, per_page: 15)

      render "settings/organization/import_export/attribution_invitations",
        locals: {
        invitations: invitations,
      }
    else
      redirect_to settings_org_profile_path(current_organization)
    end
  end

  private

  def ensure_current_organization
    unless current_organization

      # Special case during rename to redirect to the new name instead of 404ing
      if session[:org_renaming] && params[:organization_id] == session[:org_renaming]["from"]
        params[:organization_id] = session[:org_renaming]["to"]
        redirect_to url_for(params)
        return
      end

      return render_404
    end
  end

  def ensure_current_organization_for_billing
    render_404 unless current_organization_for_member_or_billing
  end

  def ensure_billing_enabled
    return render_404 unless GitHub.billing_enabled?
  end

  def billing_access_required
    return render_404 unless org_billing_manageable?
  end
end
