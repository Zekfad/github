# frozen_string_literal: true

class Orgs::ActionsSettingsController < Orgs::Controller
  include OrganizationsHelper
  include Actions::RunnersHelper
  include Actions::RunnerGroupsHelper

  before_action :login_required
  before_action :ensure_organization_exists
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :ensure_can_use_org_runners, except: :index
  before_action :ensure_tenant_exists, only: :add_runner

  map_to_service :actions_experience

  javascript_bundle :settings

  def index
    return render "settings/organization/actions/index", locals: { can_use_org_runners: false } unless can_use_org_runners?

    runner_groups_enabled = GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) || GitHub.enterprise?
    runners = Actions::Runner.for_entity(current_organization) unless runner_groups_enabled
    runner_groups = Actions::RunnerGroup.for_entity(current_organization, include_runners: true) if runner_groups_enabled

    render "settings/organization/actions/index", locals: {
      access_policy: access_policy_for(current_organization),
      can_use_org_runners: true,
      runner_permissions: Organizations::Settings::RepositoryPermissionsComponent::PERMISSIONS,
      runners: runners,
      runner_groups: runner_groups,
      show_runner_groups: runner_groups_enabled,
      can_create_runner_groups: can_create_runner_groups?,
    }
  end

  def list_runners
    view = Actions::OrgRunnersView.new(settings_owner: current_organization, current_user: current_user)

    render partial: "actions/settings/runners_list", locals: {
      view: view,
      runners: Actions::Runner.for_entity(current_organization),
    }
  end

  def add_runner
    render "settings/organization/actions/add_runner", locals: {
      platform: add_runner_data[:platform],
      os: add_runner_data[:os],
      architecture: add_runner_data[:architecture],
      downloads: add_runner_data[:downloads],
      platform_options: add_runner_data[:platform_options],
      token: add_runner_data[:token],
      selected_download: add_runner_data[:selected_download]
    }
  end

  def add_runner_instructions
    return head 404 unless request.xhr?
    return head 202 if add_runner_data[:downloads].empty?

    render partial: "actions/settings/add_runner_instructions", locals: {
      platform: add_runner_data[:platform],
      os: add_runner_data[:os],
      architecture: add_runner_data[:architecture],
      downloads: add_runner_data[:downloads],
      platform_options: add_runner_data[:platform_options],
      token: add_runner_data[:token],
      selected_download: add_runner_data[:selected_download],
      registration_url: "#{GitHub.url}/#{current_organization}"
    }
  end

  def delete_runner_modal
    scope = current_organization.runner_deletion_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: 1.hour.from_now)

    respond_to do |format|
      format.html do
        render partial: "settings/organization/actions/delete_runner_modal",
               locals: { token: token, runner_id: params[:id], runner_os: params[:os] }
      end
    end
  end

  def delete_runner
    delete_status = delete_runner_for(current_organization, id: params[:id].to_i, actor: current_user)

    if delete_status == "deleted"
      flash[:notice] = "Runner successfully deleted."
    else
      flash[:error] = "Sorry, there was a problem deleting your runner."
    end

    redirect_to settings_org_actions_path
  end

  def bulk_actions
    if runner_ids = params[:runner_ids]
      render partial: "actions/settings/runner_bulk_action", locals: {
        runner_ids: runner_ids,
        menu_name: "Labels",
        menu_path: org_runner_bulk_labels_path(runner_ids: params[:runner_ids]),
        update_path: org_runner_bulk_update_labels_path,
      }
    else
      head :ok
    end
  end

  private

  def add_runner_data
    return @add_runner_data if defined?(@add_runner_data)

    downloads = downloads_for(current_organization)

    scope = current_organization.runner_creation_token_scope
    expires_at = 1.hour.from_now
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)

    @add_runner_data = runner_options(downloads: downloads, token: token)
  end

  def ensure_tenant_exists
    result = GrpcHelper.rescue_from_grpc_errors("OrgTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(current_organization.business) if current_organization.business
      GitHub::LaunchClient::Deployer.setup_tenant(current_organization)
    end
    raise Timeout::Error, "Timeout fetching tenant" unless result.call_succeeded?
  end

  def can_create_runner_groups?
    GitHub.enterprise? || current_organization.plan.business_plus? || current_organization.plan.enterprise?
  end
end
