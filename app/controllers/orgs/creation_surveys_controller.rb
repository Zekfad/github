# frozen_string_literal: true

module Orgs
  class CreationSurveysController < Orgs::Controller
    areas_of_responsibility :insights

    include ControllerMethods::AnswerParams

    before_action :organization_admin_required
    before_action :find_survey
    before_action :ensure_survey_accessible

    def new
      render "organizations/signup/creation_surveys/new", {
        locals: {survey: @survey},
      }
    end

    def create
      if @survey.save_answers(this_organization, answer_params)
        GitHub.dogstats.increment("org_creation_survey", tags: ["action:create", "valid:true"])
      else
        GitHub.dogstats.increment("org_creation_survey", tags: ["action:create", "valid:false"])
      end

      redirect_to user_url(this_organization)
    end

    private

    def ensure_survey_accessible
      redirect_to user_url(this_organization) unless survey_accessible?
    end

    def survey_accessible?
      !@survey.taken_by?(this_organization)
    end

    def find_survey
      @survey = Survey.find_by_slug!("org_creation")
    end
  end
end
