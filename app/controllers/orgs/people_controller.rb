# frozen_string_literal: true

class Orgs::PeopleController < Orgs::Controller
  class InvalidRoleError < StandardError ; end
  include ActionView::Helpers::TextHelper
  include OrganizationsHelper

  statsd_tag_actions only: %i(
  index
  show
  pending_collaborators
  outside_collaborators
  pending_members
  cancel_pending_collaborator_invitations
  destroy_members
  set_role
  convert_to_outside_collaborators
  remove_outside_collaborators
  )

  before_action :login_required, except: :index
  before_action :organization_admin_required, except: [:tab_counts, :index, :set_visibility, :dismiss_org_membership_banner]
  before_action :saml_enabled_required, only: %i(sso unlink_identity revoke_sso_session revoke_sso_token)
  before_action :saml_owner_required, only: %i(unlink_identity revoke_sso_session revoke_sso_token)
  before_action :validate_user, only: %i(unlink_identity revoke_sso_session revoke_sso_token)
  before_action :require_xhr, only: :tab_counts
  before_action :sudo_filter, except: %i(
    add_member_for_new_org
    cancel_pending_collaborator_invitations
    convert_to_outside_collaborators
    destroy_member_for_new_org
    destroy_members
    destroy_members_dialog
    destroy_repository_permissions
    destroy_invitations
    dismiss_make_direct_members_help
    dismiss_org_membership_banner
    index
    invitations_action_dialog
    failed_invitations
    failed_invitation_toolbar_actions
    members_toolbar_actions
    outside_collaborators
    outside_collaborators_toolbar_actions
    pending_collaborators
    pending_collaborator_invitations_toolbar_actions
    pending_invitations
    pending_invitation_toolbar_actions
    remove_outside_collaborators
    repository_permissions
    revoke_sso_session
    revoke_sso_token
    set_visibility
    show
    sso
    tab_counts
  )

  before_action only: %i(
    destroy_repository_permissions
    set_visibility
    set_role
    destroy_invitations
    destroy_members
    remove_outside_collaborators
    convert_to_outside_collaborators
  ) do
    ensure_trade_restrictions_allows_org_member_management(fallback_location: org_people_url(this_organization))
  end

  # Disabling cap_pagination for the index action. Not sure why we are capping
  # pagination on a non-api route, but this action lives in ApplicationRecord
  skip_before_action :cap_pagination, only: %i(index)

  include RepositoryControllerMethods
  include Orgs::Invitations::RateLimiting

  def tab_counts
    results = {}

    results[".unstyled-orgs-member-count"] = this_organization.visible_user_ids_for(current_user, limit: nil).size if params[:member].present?
    results[".unstyled-orgs-outside-collabo-count"] = this_organization.outside_collaborators.size if params[:outside_collabo].present?
    results[".unstyled-orgs-pending-collabo-count"] = pending_collaborators_invitations_scope.size if params[:pending_collabo].present?
    results[".unstyled-orgs-pending-invite-count"] = this_organization.pending_non_manager_invitations.size if params[:pending_invite].present?
    results[".unstyled-orgs-failed-invite-count"] = this_organization.active_failed_invitations.size if params[:failed_invite].present?

    render json: { selectors: results }
  end

  def index
    set_hovercard_subject(this_organization)

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          partial_path = "orgs/people/members_table"
          render_partial_view partial_path, Orgs::People::IndexPageView,
            organization: this_organization, page: current_page, query: params[:query]
        else
          render_template_view "orgs/people/index", Orgs::People::IndexPageView,
            organization: this_organization,
            finished_migration: params[:finished_migration] == "1",
            page: current_page,
            query: params[:query],
            rate_limited: org_invite_rate_limited?
        end
      end
    end
  end

  PeopleSidebarQuery = parse_query <<-'GRAPHQL'
    query($org: String!, $login: String!) {
      user(login: $login) {
        ...Views::Orgs::People::Sidebar::Person
      }

      organization(login: $org) {
        ...Views::Orgs::People::Sidebar::Organization
      }
    }
  GRAPHQL

  def show
    person = User.find_by_login(params[:person_login])
    return render_404 if person.nil?

    repositories = this_organization.repositories_associated_with(person)

    if params[:query].present?
      repositories = ActiveRecord::Base.connected_to(role: :reading) do
        filter = Organization::RepositoryFilter.new(this_organization, viewer: current_user,
                                                    phrase: params[:query], scope: repositories)
        filter.results
      end
    end

    paginated_repositories = repositories.paginate(page: current_page, per_page: 30)
    override_analytics_location "/orgs/<org-login>/people/<user-name>"

    variables = {
      org: params[:org],
      login: params[:person_login],
    }

    result = platform_execute(PeopleSidebarQuery, variables: variables)

    respond_to do |format|
      format.html do
        if request.xhr?
          return render partial: "orgs/people/repository_list", locals: {
            organization: this_organization,
            person: person,
            repositories: paginated_repositories,
            graphql_person: result.user,
            graphql_org: result.organization,
          }
        else
          return render_template_view "orgs/people/show", Orgs::People::ShowView,
            {
              organization: this_organization,
              person: person,
              paginated_repositories: paginated_repositories,
              repositories_count: repositories.size,
            },
            locals: {
              graphql_person: result.user,
              graphql_org: result.organization,
            }
        end
      end
    end
  end

  # Action: Shows any linked ExternalIdentity information from Saml SSO as well
  # as any whitelisted personal access tokens the user has if the organization
  # has Saml SSO enforced across the org.
  def sso
    return render_404 if person.nil?

    variables = {
      org: params[:org],
      login: params[:person_login],
    }

    result = platform_execute(PeopleSidebarQuery, variables: variables)

    override_analytics_location "/orgs/<org-login>/people/<user-name>/sso"
    render_template_view "orgs/people/sso",
      Sso::ShowView,
      {
        target: this_organization,
        member: person,
        business_user_account: nil,
      },
      locals: {
        graphql_person: result.user,
        graphql_org: result.organization,
      }
  end

  def unlink_identity
    ExternalIdentity.unlink(
      provider: this_organization.external_identity_session_owner.saml_provider,
      user: person,
      perform_instrumentation: true,
      instrumentation_payload: {
        actor: current_user,
        user: person,
      },
    )

    flash[:notice] = "External identity for #{person} successfully revoked."
    redirect_to org_person_sso_path(this_organization, person)
  end

  # Action: Revokes an active SAML SSO session for the member.
  def revoke_sso_session
    saml_provider = this_organization.external_identity_session_owner.saml_provider
    external_identity_session = ExternalIdentitySession.by_saml_provider(saml_provider).
                                                        find_by(id: params[:session_id])
    if external_identity_session&.user_session&.user_id == person.id
      external_identity_session.destroy
      this_organization.instrument_sso_session_revoked(actor: current_user, user: person)
    end

    if request.xhr?
      head :ok
    else
      if external_identity_session.nil?
        flash[:error] = "SAML session could not be found."
      else
        flash[:notice] = "SAML session successfully revoked."
      end
      redirect_to org_person_sso_path(this_organization, person)
    end
  end

  # Action: Revokes whitelisted personal access token. Without the whitelist,
  # this token will not be able access private data via the API or Git for this
  # organization.
  def revoke_sso_token
    credential_type = params[:credential_type]

    token = person.oauth_accesses.find_by_id(params[:token_id]) if credential_type == "OauthAccess"
    token = person.public_keys.find_by_id(params[:token_id]) if credential_type == "PublicKey"
    return render_404 if token.nil?

    Organization::CredentialAuthorization.revoke \
      organization: this_organization,
      credential: token,
      actor: current_user

    flash[:notice] = "API and Git access successfully revoked."
    redirect_to org_person_sso_path(this_organization, person)
  end

  def outside_collaborators
    view_model_params = {
      organization: this_organization,
      page: current_page,
      query: params[:query],
      finished_migration: params[:finished_migration] == "1",
      rate_limited: org_invite_rate_limited?,
    }
    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render_partial_view "orgs/people/outside_collaborators_list", Orgs::People::OutsideCollaboratorsView,
            view_model_params
        else
          render_template_view "orgs/people/outside_collaborators", Orgs::People::OutsideCollaboratorsView,
            view_model_params
        end
      end
    end
  end

  def pending_members
    render_template_view "orgs/invitations/pending_members", Orgs::Invitations::PendingMembersView,
      organization: this_organization,
      invitations: this_organization.pending_non_manager_invitations,
      rate_limited: org_invite_rate_limited?,
      page: params[:page] || 1,
      context: :page
  end

  def pending_collaborators_invitations_scope
    invitations_scope = this_organization
      .repository_invitations
      .excluding_expired
      .preload(:repository, :invitee)

    query = ActiveRecord::Base.sanitize_sql_like(params[:query].strip.downcase) if params[:query]

    if query.present?
      like_login_ids = User.where(["login LIKE :query", { query: "%#{query}%" }]).pluck(:id)
      like_email_ids = invitations_scope.where.not(email: nil).where(["email LIKE :query", { query: "%#{query}%" }]).pluck(:id)
      invitations_scope = invitations_scope.where(invitee_id: like_login_ids).or(invitations_scope.where(id: like_email_ids))
    end

    invitations_scope
  end

  def pending_collaborators
    invitations = pending_collaborators_invitations_scope
      .order(email: :desc, created_at: :desc)
      .paginate(page: params[:page], per_page: 30)

    invitees = invitations.map { |invitation| invitation.invitee ? invitation.invitee : invitation.email }.uniq

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render_partial_view "orgs/people/pending_collaborator_invitations_list",
            Orgs::People::PendingCollaboratorInvitationsListView,
            organization: this_organization,
            invitations: invitations,
            invitees: invitees
        else
          render "orgs/people/pending_collaborator_invitations", locals: {
            organization: this_organization,
            invitations: invitations,
            invitees: invitees
          }
        end
      end
    end
  end

  def pending_collaborator_invitations_toolbar_actions
    selected_invitations = this_organization
      .repository_invitations
      .preload(:invitee)
      .where(id: params[:pending_collaborator_invitation_ids])

    invitees = selected_invitations.map do |invitation|
      invitation.invitee ? invitation.invitee : invitation.email
    end.uniq

    respond_to do |format|
      format.html do
        render partial: "orgs/people/pending_collaborator_invitations_toolbar_actions", locals: {
          selected_invitations: selected_invitations,
          invitees: invitees,
          organization: this_organization,
        }
      end
    end
  end

  def cancel_pending_collaborator_invitations
    pending_collaborator_invitation_ids = params[:pending_collaborator_invitation_ids] || []

    if pending_collaborator_invitation_ids.empty?
      flash[:error] = "You must specify at least one pending collaborator."
      return redirect_to :back
    end

    invitations = this_organization
      .repository_invitations
      .where(id: pending_collaborator_invitation_ids)

    invitations.each do |invitation|
      invitation.enqueue_cancel_invitation(actor: current_user)
    end

    if invitations.empty?
      flash[:error] = "Something went wrong when cancelling repository invitations for those collaborators."
    else
      flash[:notice] = "Successfully cancelled #{pluralize(invitations.size, "repository invitation")}. It may take a few minutes for the removal to process."
    end

    redirect_to :back
  end

  def repository_permissions
    person = User.find_by_login(params[:person_login])

    # We check this instead of pullable_by? because there are cases where the
    # person may have access to the repo but the logged-in user shouldn't know
    # about it.
    if this_organization.repositories_associated_with(person).exclude?(current_repository)
      return render_404
    end

    override_analytics_location "/orgs/<org-login>/people/<user-name>/repositories/<user-name>/<repo-name>"
    if affiliated_with_org?(person)
      render_template_view "orgs/people/repository_permissions", Orgs::People::RepositoryPermissionsView,
        organization: this_organization,
        person: person,
        repository: current_repository
    else
      return render_404
    end
  end

  def destroy_repository_permissions
    person = User.find_by_login(params[:person_login])

    if affiliated_with_org?(person)
      permissions = Organization::RepositoryPermissions.new(current_repository, person)

      if params[:active_only] == "1"
        permission_after_revoking = permissions.permission_after_revoking_active
        permissions.revoke_active(actor: current_user)
      else
        permission_after_revoking = nil
        permissions.revoke_all(actor: current_user)
      end

      if permission_after_revoking.present?
        flash[:notice] = "Decreased #{person.login}'s access to #{current_repository.name_with_owner}."
        redirect_to repository_permissions_path(this_organization, person, current_repository.owner, current_repository)
      else
        flash[:notice] = "Removed #{person.login}'s access to #{current_repository.name_with_owner}."

        if affiliated_with_org?(person)
          redirect_to org_person_path(this_organization, person)
        else
          redirect_to org_people_path(this_organization)
        end
      end
    else
      return render_404
    end
  end

  def set_visibility
    members = User.find(params[:member_ids].split(","))
    if can_set_visibility?(members)
      publicize = params[:publicize].present?

      members.each do |member|
        if publicize
          this_organization.publicize_member(member)
        else
          this_organization.conceal_member(member)
        end
      end

      flash[:notice] = "You #{publicize ? 'publicized' : 'concealed'} #{members.size} #{"membership".pluralize(members.size)}."
    else
      flash[:notice] = "You're not allowed to do that."
    end

    redirect_to :back
  end

  # Add a member directly to an org, and any selected teams.
  # For use when invites are disabled.
  def add_member_to_org
    if !GitHub.bypass_org_invites_enabled?
      return render_404
    end

    member = User.find_by_id(params[:invitee_id])

    if member.blank?
      return render_404
    end

    if !this_organization.two_factor_requirement_met_by?(member)
      flash[:error] = <<-FLASH
        #{this_organization} requires its members to have two-factor
        authentication enabled. #{member} must enable two-factor authentication
        in order to be added to the organization.
      FLASH

      return redirect_to user_path(this_organization)
    end

    if params[:role] == "admin"
      this_organization.add_admin(member, adder: current_user)
    elsif params[:role] == "direct_member"
      this_organization.add_member(member, adder: current_user)
    elsif params[:role] == "reinstate"
      restorable = Restorable::OrganizationUser.restorable(this_organization, member)
      this_organization.restore_membership(restorable, actor: current_user)
    else
      return render_404
    end

    teams = this_organization.teams.where(id: params[:team_ids])
    unless current_user.can_add_members_for?(this_organization, teams: teams)
      flash[:error] = "You do not have permission to add members to all of the selected teams."
      redirect_to org_edit_invitation_path(this_organization, member.login)
      return
    else
      teams.each do |team|
        team.add_member(member, adder: current_user)
      end
    end

    if params[:role] == "reinstate"
      flash[:notice] = "Job queued to reinstate #{member.login} to #{this_organization.safe_profile_name}!"
    else
      flash[:notice] = "You've added #{member.login} to #{this_organization.safe_profile_name}!"
    end
    redirect_to org_people_path(this_organization)
  end

  # This action supports adding a member directly to an organization during the
  # Organization creation workflow.
  # For use when invites are disabled.
  def add_member_for_new_org
    if !GitHub.bypass_org_invites_enabled?
      return render_404
    end

    member = User.find_by_login(params[:member])

    if this_organization.at_seat_limit?
      message_html = render_to_string(
        partial: "organizations/signup/seat_limit_error",
        formats: [:html],
        layout: false)
      render json: { message_html: message_html }, status: :not_found
      return
    end

    this_organization.add_member(member, adder: current_user)
    render json: {
             list_item_html: render_to_string(
               partial: "orgs/people/members_for_new_org",
               formats: [:html],
               locals: {
                 user: member,
                 organization: this_organization,
               }
             )
           }
  end

  def destroy_member_for_new_org
    if !GitHub.bypass_org_invites_enabled?
      return render_404
    end

    user = User.find_by_login(params[:member])

    begin
      this_organization.remove_member(user, background_team_remove_member: true)
    rescue Organization::NoAdminsError
      render json: { error: "You can't remove the last owner of this organization." }, status: :not_found
      return
    end

    head :ok
  end

  def destroy_members
    users = User.where(id: params[:member_ids].split(",")).to_a

    # If the logged-in user is one of the members to be removed, put them at the
    # end in case all other owners are being removed.
    move_current_user_to_end(users)

    begin
      users.each do |user|
        if this_organization.direct_or_team_member?(user)
          this_organization.remove_member(user, background_team_remove_member: true)
        else
          this_organization.remove_direct_repo_access(user)
        end
      end
    rescue Organization::NoAdminsError
      flash[:error] = "You can't remove the last owner of this organization."
    end

    if !flash[:error]
      if users.length == 1
        flash[:notice] = "You've removed #{users.first} from the organization. It may take a few minutes for the removal to process."
      else
        flash[:notice] = "You've removed #{users.length} people from the organization. It may take a few minutes for the removal to process."
      end
    end

    if params[:redirect_to_path].present?
      safe_redirect_to params[:redirect_to_path]
    else
      redirect_to :back
    end
  end

  def destroy_members_dialog
    selected_members = this_organization.visible_users_for(current_user, actor_ids: params[:member_ids])

    respond_to do |format|
      format.html do
        render_partial_view "orgs/people/destroy_members_dialog", Orgs::People::DestroyMembersDialogView,
          organization: this_organization,
          selected_members: selected_members,
          redirect_to_path: params[:redirect_to_path]
      end
    end
  end

  def invitations_action_dialog
    selected_invitations = this_organization.invitations_for(current_user, invitation_ids: params[:invitation_ids])
    return render_404 if selected_invitations.blank?

    action_dialog = params[:action_dialog].presence

    respond_to do |format|
      format.html do
        render_partial_view "orgs/people/invitations_dialog", Orgs::People::InvitationsDialogView,
          organization: this_organization,
          selected_invitations: selected_invitations,
          redirect_to_path: params[:redirect_to_path],
          action_dialog: action_dialog
      end
    end
  end

  # Failed org invitations can be retried or deleted. Pending invitations can be cancelled.
  # Failed invitations are are due to trade restrictions in certain countries or if the invite exceeds the plan's maximum seat count
  def destroy_invitations
    invitations = this_organization.invitations_for(current_user, invitation_ids: params[:invitation_ids])
    return render_404 if invitations.blank?

    failed_or_pending_action = params[:failed_or_pending_action].presence

    # Users will be notified by email if their pending invitations are canceled
    invitations.each do |invitation|
      invitation.cancel(actor: current_user, notify: failed_or_pending_action == "cancelled")
    end

    flash[:notice] = "You've #{failed_or_pending_action} #{pluralize(invitations.length, 'invitation')} from the organization. It may take a few minutes to process."

    if params[:redirect_to_path].present?
      safe_redirect_to params[:redirect_to_path]
    else
      redirect_to :back
    end
  end

  def retry_invitations
    invitations = OrganizationInvitation.where(id: params[:invitation_ids].split(",")).to_a
    return render_404 if invitations.blank?

    invitations.each do |invitation|
      invitation.cancel(actor: current_user, notify: false)
      invitation.instrument_retry_invite(actor: current_user)
    end

    OrganizationBulkInviteJob.perform_later(current_user, this_organization, invitations.map { |i| i.email_or_invitee_login })

    flash[:notice] = "You've retried #{pluralize(invitations.length, 'invitation')} from the organization. It may take a few minutes for the retry to process."

    if params[:redirect_to_path].present?
      safe_redirect_to params[:redirect_to_path]
    else
      redirect_to :back
    end
  end

  def failed_invitations
    set_hovercard_subject(this_organization)

    render_template_view "orgs/people/failed_invitations", Orgs::People::FailedInvitationsPageView,
      organization: this_organization,
      invitations: this_organization.active_failed_invitations,
      page: current_page,
      query: params[:query],
      rate_limited: org_invite_rate_limited?
  end

  def failed_invitation_toolbar_actions
    respond_to do |format|
      format.html do
        render_partial_view "orgs/people/failed_invitation_toolbar_actions", Orgs::People::FailedInvitationToolbarActionsView,
          organization: this_organization,
          selected_invitations: this_organization.active_failed_invitations.where(id: (params[:invitation_ids] || []))
      end
    end
  end

  def pending_invitations
    set_hovercard_subject(this_organization)

    render_template_view "orgs/people/pending_invitations", Orgs::People::PendingInvitationsPageView,
      organization: this_organization,
      invitations: this_organization.pending_non_manager_invitations,
      page: current_page,
      query: params[:query],
      rate_limited: org_invite_rate_limited?
  end

  def pending_invitation_toolbar_actions
    respond_to do |format|
      format.html do
        render_partial_view "orgs/people/pending_invitation_toolbar_actions", Orgs::People::PendingInvitationToolbarActionsView,
          organization: this_organization,
          selected_invitations: this_organization.pending_invitations.where(id: (params[:invitation_ids] || []))
      end
    end
  end

  def members_toolbar_actions
    respond_to do |format|
      format.html do
        render_partial_view "orgs/people/members_toolbar_actions", Orgs::People::ToolbarActionsView,
          organization: this_organization,
          selected_members: this_organization.visible_users_for(current_user, actor_ids: params[:member_ids] || [])
      end
    end
  end

  def outside_collaborators_toolbar_actions
    respond_to do |format|
      format.html do
        render partial: "orgs/people/outside_collaborators_toolbar_actions", locals: {
          organization: this_organization,
          selected_outside_collaborators: selected_outside_collaborators,
        }
      end
    end
  end

  private def selected_outside_collaborators
    Scientist.run("break-outside-collaborators-toolbar-actions-join") do |e|
      e.compare_record_sequence
      e.use { this_organization.outside_collaborators.where(id: params[:outside_collaborator_ids] || []).load }
      e.try do
        outside_collaborator_ids = params[:outside_collaborator_ids] || []
        ids = this_organization.outside_collaborator_ids(actor_ids: outside_collaborator_ids)
        User.with_ids(ids).load
      end
    end
  end

  def set_role
    members = User.where(id: params[:member_ids].split(","))

    case params[:role]
    when "admin"
      member_action = :admin

      notice = if members.size == 1
        "Made #{members.first.login} an owner."
      else
        "Made #{members.size} people owners."
      end
    when "direct_member"
      member_action = :read

      notice = if members.size == 1
        "Made #{members.first.login} a member."
      else
        "Made #{members.size} people members."
      end
    else
      raise InvalidRoleError, "invalid organization role: #{params[:role]}"
    end

    begin
      members.each do |member|
        this_organization.update_member(member, action: member_action)
      end
    rescue Organization::NoAdminsError
      flash[:error] = "You can't remove the organization's last admin."
    else
      flash[:notice] = notice
    end

    redirect_to :back
  end

  def dismiss_make_direct_members_help
    current_user.dismiss_notice("make_direct_members")

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  def dismiss_org_membership_banner
    current_user.dismiss_notice("org_membership_banner")

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  def convert_to_outside_collaborators
    member_ids       = params[:member_ids].present? && params[:member_ids].split(",")
    matching_members = this_organization.visible_users_for(current_user, actor_ids: member_ids).to_a

    # Don't allow the current user to remove themself.
    if matching_members.delete(current_user)
      flash[:error] = "You can't remove yourself from the organization. Have another admin do this for you."
      return redirect_to :back
    end

    # Don't allow all admins to be removed from the org
    remaining_admins = this_organization.admins.pluck(:id) - matching_members.pluck(:id)

    if remaining_admins.empty?
      flash[:error] = "You can't remove all owners of this organization."
      return redirect_to :back
    end

    matching_members.each do |member|
      this_organization.convert_to_outside_collaborator(member)
    end

    flash[:notice] =
      case matching_members.size
      when 0
        "Nobody was converted to an outside collaborator."
      when 1
        "You've removed #{matching_members.first.login} from the organization and preserved their repository access. It may take a few minutes to process."
      else
        "You've removed #{matching_members.size} people from the organization and preserved their repository access. It may take a few minutes to process."
      end

    redirect_to :back
  end

  def remove_outside_collaborators
    users = User.with_ids(Array.wrap(params[:outside_collaborator_ids]))

    count = users.size

    flash[:notice] = "You've requested removal of #{count} #{"outside collaborator".pluralize(count)}. It may take a few minutes to process."

    users.find_each do |user|
      this_organization.remove_outside_collaborator(user)
    end

    if params[:redirect_to_path].present?
      safe_redirect_to params[:redirect_to_path]
    else
      redirect_to :back
    end
  end

  private

  def can_set_visibility?(requested_users)
    if params[:publicize].present?
      return true if this_organization.can_publicize_memberships?(current_user, members: requested_users)
    elsif params[:conceal].present?
      return true if this_organization.can_conceal_memberships?(current_user, members: requested_users)
    end

    false
  end

  def affiliated_with_org?(person)
    return false if person.nil?
    return true if this_organization.direct_or_team_member?(person)

    # If the person is not a direct or team member of the org, they're only
    # affiliated with the org if they're a collaborator on at least one of the
    # org's repositories.
    this_organization.repositories_associated_with(person).any?
  end

  def move_current_user_to_end(users)
    if users.include?(current_user)
      users.delete(current_user)
      users << current_user
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_organization
    this_organization
  end

  def organization_trade_controls_restrictions_check
    if this_organization.has_full_trade_restrictions?
      flash[:trade_controls_organization_billing_error] = true
      return redirect_back(fallback_location: :index)
    end
  end

  def saml_enabled_required
    return render_404 unless this_organization.saml_sso_present?
  end

  def saml_owner_required
    if this_organization.business&.saml_sso_enabled?
      return render_404 unless this_organization.business.adminable_by?(current_user)
    end
  end

  def validate_user
    if person.nil?
      flash["error"] = "This action cannot be performed because the user could not be found."
      redirect_to org_people_path(this_organization)
    end
  end

  def person
    @person = User.find_by_login(params[:person_login])
  end
end
