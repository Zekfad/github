# frozen_string_literal: true
class Orgs::TeamsController < Orgs::Controller

  statsd_tag_actions only: [:create, :index, :teams, :update]

  before_action :login_required
  before_action :organization_read_required, except: [:index, :teams, :minisearch, :child_teams]
  before_action :organization_admin_required, except: [:index, :autocomplete, :teams, :child_teams, :new, :create, :minisearch, :check_name, :goto, :leave, :ldap_group_suggestions, :destroy, :destroy_team_teams, :edit, :update, :dismiss_org_teams_banner, :migrate_legacy_admin_team, :members_toolbar_actions, :repositories_toolbar_actions, :team_teams_toolbar_actions, :parent_search, :child_search, :important_changes_summary, :team_breadcrumbs, :move_child_team, :review_assignment, :update_review_assignment]
  before_action :admin_on_team_required, except: [:index, :autocomplete, :teams, :child_teams, :new, :goto, :create, :minisearch, :check_name, :leave, :ldap_group_suggestions, :dismiss_org_teams_banner, :owners_team, :destroy_owners_team, :rename_owners_team, :set_visibility, :teams_toolbar_actions, :destroy_teams, :parent_search, :important_changes_summary, :team_breadcrumbs]
  before_action :this_team_required, except: [:index, :autocomplete, :child_teams, :teams, :new, :goto, :create, :minisearch, :check_name, :ldap_group_suggestions, :dismiss_org_teams_banner, :owners_team, :destroy_owners_team, :rename_owners_team, :set_visibility, :teams_toolbar_actions, :destroy_teams, :parent_search, :team_breadcrumbs, :important_changes_summary]
  before_action :organization_team_creation_required, only: [:new, :create, :check_name]
  before_action :mask_analytics_data
  before_action only: %i(create update destroy destroy_teams new edit set_visibility) do
    ensure_trade_restrictions_allows_org_member_management(fallback_location: teams_url(this_organization))
  end

  javascript_bundle :settings

  # check whether organization invitations have been rate limited; but do not
  # enforce rate limiting in this controller.
  include Orgs::Invitations::RateLimiting

  PRIVACY_MAPPING = {closed: "VISIBLE", secret: "SECRET"}

  IndexQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $first: Int, $last: Int, $before: String, $after: String, $membership: TeamMembershipType, $query: String, $membersFilter: TeamMembersFilter, $userLogins: [String!], $privacy: TeamPrivacy, $nestedList: Boolean!) {
      organization(login: $login) {
        viewerIsAMember
        ...Orgs::Teams::IndexPageView::OrgFragment
      }
    }
  GRAPHQL

  def index
    graphql_data = platform_execute(IndexQuery, variables: teams_query_vars)
    return render_404 unless graphql_org = graphql_data.organization

    unless graphql_org.viewer_is_a_member
      return render_404 if request.xhr?
      flash[:notice] = "You’re not a member of any teams in this organization."
      return redirect_to user_path(this_organization)
    end

    data = {
      organization: this_organization,
      query: params[:query],
      graphql_org: graphql_org,
    }

    # Make sure the browser caches AJAX responses separately from regular HTML responses.
    response.headers["Vary"] = "X-Requested-With"

    if this_organization.team_sync_failed?
      failed_teams = this_organization.team_sync_failed_teams.order(:name).pluck(:name).join(", ")
      flash.now[:error] = <<~MESSAGE
        We are having trouble syncing some of your teams right now.
        Please ensure they are correctly mapped to accessible groups,
        and open a support ticket if the problem persists.
        Affected teams: #{failed_teams}.
      MESSAGE
    end

    if request.xhr?
      respond_to do |format|
        format.html do
          render_partial_view "orgs/teams/list", Orgs::Teams::IndexPageView, data
        end
      end
    else
      render_template_view "orgs/teams/index", Orgs::Teams::IndexPageView, data
    end
  end

  AutocompleteQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $query: String) {
      organization(login: $login) {
        ...Views::Orgs::Teams::Autocomplete::Organization
      }
    }
  GRAPHQL

  def autocomplete
    graphql_data = platform_execute(AutocompleteQuery, variables: {
      "login" => params[:org] || params[:organization_id],
      "query" => params[:q],
    })

    locals = {
      query: params[:q],
      org: graphql_data.organization,
      has_errors: graphql_data.errors.any?,
    }

    respond_to do |format|
      format.html_fragment do
        render partial: "orgs/teams/autocomplete", formats: :html, locals: locals
      end
    end
  end


  TeamsQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!, $first: Int, $firstRequests: Int, $last: Int, $before: String, $after: String, $membership: TeamMembershipType, $immediateOnly: Boolean, $query: String, $membersFilter: TeamMembersFilter, $userLogins: [String!], $isMaintainer: Boolean!) {
      organization(login: $login) {
        viewerIsAMember
        ...Orgs::Teams::TeamsPageView::OrgFragment
        ...Views::Layouts::Team::Organization
        team(slug: $slug) {
          ...Orgs::Teams::TeamsPageView::TeamFragment
          ...Views::Orgs::Teams::AddChildFormModal::TeamFragment
          ...Views::Orgs::Teams::PendingTeamRequestsModal
        }
      }
    }
  GRAPHQL

  def teams
    graphql_data = platform_execute(TeamsQuery, variables: teams_query_vars)
    return render_404 unless graphql_org = graphql_data.organization
    return render_404 unless graphql_org.viewer_is_a_member
    return render_404 unless graphql_team = graphql_org.team

    data = {
      org: graphql_org,
      graphql_team: graphql_team,
      query: params[:query],
    }

    if request.xhr?
      respond_to do |format|
        format.html do
          render_partial_view "orgs/teams/list", Orgs::Teams::TeamsPageView, data
        end
      end
    else
      render_template_view(
        "orgs/teams/teams",
        Orgs::Teams::TeamsPageView,
        data.merge(layout: "team", team: this_team),
        locals: { graphql_org: graphql_org, selected_nav_item: :teams})
    end
  end

  ChildTeamsQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!, $first: Int!, $after: String, $membership: TeamMembershipType) {
      viewer {
        organization(login: $login) {
          viewerIsAMember
          teamsResourcePath
          viewerCanAdminister
          team(slug: $slug) {
            databaseId
            slug
            viewerCanAdminister
            ...Views::Orgs::Teams::ChildTeams::Team
          }
        }
      }
    }
  GRAPHQL

  PAGE_SIZE = 10

  def child_teams
    return head 404 unless params[:parent_team_slug].present?

    variables = {}
    variables["login"] = this_organization.login
    variables["slug"]  = params[:parent_team_slug]
    variables["first"] = PAGE_SIZE
    variables["after"] = params[:after] if params[:after]
    graphql_data = platform_execute(ChildTeamsQuery, variables: variables)

    return head 404 unless org = graphql_data.viewer.organization
    return redirect_to org.teams_resource_path.to_s unless request.xhr?
    return head 404 unless org.viewer_is_a_member
    return head 404 unless team = org.team

    respond_to do |format|
      format.html do
        render partial: "orgs/teams/child_teams", locals: {
          parent_team_slug: team.slug,
          parent_indent: params[:parent_indent].to_i,
          show_bulk_actions: org.viewer_can_administer,
          graphql_team: team,
          child_indent: [params[:parent_indent].to_i + 1, 15].min,
        }
      end
    end
  end

  NewQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $query: String, $slug: String!, $parentSlug: String!, $parentSlugPresent: Boolean!) {
      organization(login: $login) {
        ...Views::Orgs::Teams::ParentSearch::Organization
        ...Orgs::Teams::FormView::OrgFragment
        selectedParentTeam: team(slug: $parentSlug) @include(if: $parentSlugPresent) {
          id
          name
          privacy
          viewerCanAdminister
        }
      }
    }
  GRAPHQL

  def new
    @organization = this_organization

    org_data = platform_execute(NewQuery,
      variables: {
        "login" => params[:org] || params[:organization_id],
        "slug" => (params[:team] && params[:team][:slug]) || "",
        "parentSlug" => params[:parent_team].presence || "",
        "parentSlugPresent" => params[:parent_team].present?,
      },
    )
    parent_team = org_data.organization.selected_parent_team
    parent_team_param =
      if parent_team && parent_team.viewer_can_administer && parent_team.privacy == "VISIBLE"
        { "id" => parent_team.id, "name" => parent_team.name }
      else
        nil
      end

    parameters = {"privacy" => "CLOSED", "parentTeam" => parent_team_param, "name" => params[:name]}
    render "orgs/teams/new", locals: { team_params: parameters, org: org_data.organization }
  end

  MoveChildTeamQuery = parse_query <<-'GRAPHQL'
    query($childTeamId: ID!) {
      node(id: $childTeamId) {
        ... on Team {
          viewerCanAdminister
        }
      }
    }
  GRAPHQL

  CreateTeamChangeParentRequestQuery = parse_query <<-'GRAPHQL'
    mutation($childTeamId: ID!, $parentTeamId: ID!) {
      createParentInitiatedTeamChangeParentRequest(input: {parentTeamId: $parentTeamId, childTeamId: $childTeamId})
    }
  GRAPHQL

  def move_child_team
    attributes = params.require(:child_team).permit(:id)
    child_team = Team.find(attributes[:id])

    if child_team.adminable_by?(current_user)
      results = platform_execute(UpdateTeamMutation, variables: {
        teamId: child_team.global_relay_id,
        parentTeamId: this_team.global_relay_id,
        clientMutationId: request_id,
      })

      if results.errors.all.any?
        flash[:error] = results.errors.all.values.flatten.to_sentence
      else
        flash[:notice] = "You’ve successfully moved the team."
      end
    else
      results = platform_execute(CreateTeamChangeParentRequestQuery, variables: {
        childTeamId: child_team.global_relay_id,
        parentTeamId: this_team.global_relay_id,
        clientMutationId: request_id,
      })

      if results.errors.all.any?
        flash[:error] = results.errors.all.values.flatten.to_sentence
      else
        flash[:notice] = "A request has been made to move this team."
      end
    end

    redirect_to team_teams_path(this_team)
  end

  def child_search
    respond_to do |format|
      format.html_fragment do
        render partial: "orgs/teams/child_team_search", formats: :html, locals: {
          organization: this_organization,
          team: this_team,
          potential_child_teams: this_team.async_potential_child_teams(viewer: current_user, query: params[:q]).sync,
          query: params[:q],
        }
      end
    end
  end

  EditQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $query: String, $slug: String!) {
      organization(login: $login) {
        ...Orgs::Teams::FormView::OrgFragment
        ...Views::Layouts::Team::Organization
        ...Views::Orgs::Teams::ParentSearch::Organization
        team(slug: $slug) {
          outboundParentRequests: pendingTeamChangeParentRequests(first: 1, direction: OUTBOUND_CHILD_INITIATED) {
            edges {
              node {
                requestedTeam {
                  name
                }
              }
            }
          }
          parentTeam
          ...Orgs::Teams::FormView::TeamFragment
          ...Views::Orgs::Teams::Edit::TeamFragment
          ...Views::Orgs::Teams::ReviewAssignment::TeamFragment
        }
      }
    }
  GRAPHQL

  def edit
    org_data = platform_execute(EditQuery,
      variables: {
        "login" => params[:org] || params[:organization_id],
        "slug" => params[:team_slug],
      },
    )

    if @team = this_team
      @organization = this_organization

      render(
        "orgs/teams/edit",
        locals: {
          graphql_org: org_data.organization,
          selected_nav_item: :settings,
          team: org_data.organization.team,
          team_entity: @team,
        },
        layout: "team")
    else
      render_404
    end
  end

  def review_assignment
    org_data = platform_execute(EditQuery,
      variables: {
        "login" => params[:org] || params[:organization_id],
        "slug" => params[:team_slug],
      },
    )

    if @team = this_team
      @organization = this_organization

      render(
        "orgs/teams/review_assignment",
        locals: {
          graphql_org: org_data.organization,
          selected_nav_item: :settings,
          graphql_team: org_data.organization.team,
          team: @team,
        },
        layout: "team")
    else
      render_404
    end
  end

  UpdateReviewAssignment = parse_query <<-'GRAPHQL'
    mutation($teamId: ID!, $enabled: Boolean!, $algorithm: TeamReviewAssignmentAlgorithm, $teamMemberCount: Int, $slug: String!, $excludedTeamMemberIds: [ID!], $notifyTeam: Boolean) {
      updateTeamReviewAssignment(input: { id: $teamId, enabled: $enabled, algorithm: $algorithm, teamMemberCount: $teamMemberCount, excludedTeamMemberIds: $excludedTeamMemberIds, notifyTeam: $notifyTeam} ) {
        team {
          ...Views::Orgs::Teams::Edit::TeamFragment
          ...Views::Orgs::Teams::ReviewAssignment::TeamFragment
          ...Orgs::Teams::FormView::TeamFragment
          organization {
            ...Views::Layouts::Team::Organization
          }
        }
      }
    }
  GRAPHQL

  def update_review_assignment
    enabled = !!params[:review_assignment]
    input = {
        "slug" => params[:team_slug],
        "teamId" => this_team.global_relay_id,
        "enabled" => enabled,
      }

    if enabled
      input["teamMemberCount"] = params[:team_member_count].to_i
      input["algorithm"] = params[:algorithm]
      input["notifyTeam"] = false if params[:disable_notify_team]

      if params[:exclude_team_members]
        input["excludedTeamMemberIds"] = params[:excluded_members]
      end
    end

    results = platform_execute(UpdateReviewAssignment, variables: input)

    if @team = this_team
      if results.errors.all.any?
        flash[:error] = results.errors.all.values.flatten.first.to_s

        redirect_to edit_team_review_assignment_path(this_organization, this_team)
      else
        flash[:notice] = "You've successfully updated the team's code review assignment settings."

        redirect_to edit_team_review_assignment_path(this_organization, this_team)
      end
    else
      render_404
    end
  end

  CreateTeamMutation = parse_query <<-'GRAPHQL'
    mutation($organizationId: ID!, $name: String!, $description: String, $privacy: TeamPrivacy!, $parentTeamId: ID, $ldap_dn: String, $maintainers: [String], $groupMappings: [GroupMapping!]) {
      createTeam(input: { organizationId: $organizationId, name: $name, description: $description, privacy: $privacy, parentTeamId: $parentTeamId, ldap_dn: $ldap_dn, maintainers: $maintainers, groupMappings: $groupMappings})  {
        team {
          id
          pendingTeamChangeParentRequests(first: 1) {
            edges {
              node {
                parentTeam{
                  viewerCanAdminister
                }
              }
            }
          }
          resourcePath
          ...Orgs::Teams::FormView::TeamFragment
          ...Views::Organizations::ImportGroup::Team
        }
      }
    }
  GRAPHQL

  UpdateTeamMutation = parse_query <<-'GRAPHQL'
    mutation($teamId: ID!, $name: String, $description: String, $privacy: TeamPrivacy, $parentTeamId: ID, $ldap_dn: String, $groupMappings: [GroupMapping!]) {
      updateTeam(input: { teamId: $teamId, name: $name, description: $description, privacy: $privacy, parentTeamId: $parentTeamId, ldap_dn: $ldap_dn, groupMappings: $groupMappings })  {
        team {
          id
          pendingTeamChangeParentRequests(first: 1, direction: OUTBOUND_CHILD_INITIATED) {
            edges {
              node {
                parentTeam{
                  viewerCanAdminister
                }
              }
            }
          }
          ...Orgs::Teams::FormView::TeamFragment
          ...Views::Organizations::ImportGroup::Team
        }
      }
    }
  GRAPHQL

  CancelParentRequestMutation = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      cancelPendingTeamChangeParentRequest(input: {requestId: $id}) {
        cancelled
      }
    }
  GRAPHQL

  TeamChangeParentRequestQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!) {
      organization(login: $login) {
        team(slug: $slug) {
          pendingTeamChangeParentRequests(first: 100, direction: OUTBOUND_CHILD_INITIATED) {
            edges {
              node {
                id
                parentTeam {
                  id
                }
              }
            }
          }
        }
      }
    }
  GRAPHQL

  OrgQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $query: String, $slug: String!) {
      organization(login: $login) {
        ...Orgs::Teams::FormView::OrgFragment
        ...Views::Layouts::Team::Organization
        ...Views::Orgs::Teams::ParentSearch::Organization
      }
    }
  GRAPHQL

  def create
    attributes = team_params

    privacy = if attributes[:privacy] == "secret"
      "SECRET"
    else
      "VISIBLE"
    end

    variables = {
      "organizationId"   => this_organization.global_relay_id,
      "name"             => attributes[:name],
      "description"      => attributes[:description],
      "privacy"          => privacy,
      "parentTeamId"     => attributes[:parent_team_id].presence,
      "ldap_dn"          => attributes[:ldap_dn],
      "clientMutationId" => request_id,
      "maintainers"      => [current_user.login],
      "groupMappings"   => transform_team_sync_mapping_inputs(attributes[:group_mappings]),
    }

    results = platform_execute(CreateTeamMutation, variables: variables)

    unless results.errors.all.any?
      team = results.create_team.team
      requested_parent_team = if team.pending_team_change_parent_requests.edges.any?
        team.pending_team_change_parent_requests.edges.first.node.parent_team
      end

      if ldap_group_import?
        respond_to do |f|
          f.html do
            render partial: "organizations/import_group",
              locals: {team: results.create_team.team}
          end
        end
      elsif requested_parent_team && !requested_parent_team.viewer_can_administer?
        flash[:notice] = "Team was successfully created. The requested parent team will be reflected pending approval."

        redirect_to results.create_team.team.resource_path.to_s
      else
        redirect_to results.create_team.team.resource_path.to_s
      end
    else
      if request.xhr? && ldap_group_import?
        render status: 422, plain: results.errors.all.values.flatten.first.to_s
        return
      end

      @organization = this_organization

      flash.now[:error] = results.errors.all.values.flatten.first.to_s

      org_data = platform_execute(OrgQuery,
        variables: { "login" => params[:org] || params[:organization_id], "slug" => "" },
      )

      variables["parentTeam"] = { "id" => attributes[:parent_team_id], "name" => attributes[:parent_team_name] }
      render "orgs/teams/new", locals: { team_params: variables, org: org_data.organization}
    end
  end

  def update
    attributes = team_params

    variables = {
      "teamId"           => this_team.global_relay_id,
      "name"             => attributes[:name],
      "description"      => attributes[:description],
      "privacy"          => PRIVACY_MAPPING[attributes[:privacy]&.to_sym],
      "ldap_dn"          => attributes[:ldap_dn],
      "groupMappings"   => transform_team_sync_mapping_inputs(attributes[:group_mappings]),
      "clientMutationId" => request_id,
    }

    # Pass parentTeamId only if received since a `nil` parentTeamId indicates
    # the parent should be removed
    variables[:parentTeamId] = attributes[:parent_team_id].presence if attributes.key?(:parent_team_id)

    requested_parent_team_id = params.dig("team", "parent_team_id")
    new_parent_requested = requested_parent_team_id &&
                          (this_team.parent_team.nil? ||
                          requested_parent_team_id != this_team.parent_team.global_relay_id)

    if new_parent_requested
      requests_to_cancel = platform_execute(TeamChangeParentRequestQuery,
        variables: { "login" => params[:org] || params[:organization_id],
                     "slug" => params[:team_slug] },
      ).organization.team.pending_team_change_parent_requests.edges

      requests_to_cancel.each do |req|
        unless req.node.parent_team.id == requested_parent_team_id
          platform_execute(CancelParentRequestMutation, variables: { "id" => req.node.id })
        end
      end
    end

    results = platform_execute(UpdateTeamMutation, variables: variables)

    if request.xhr?
      if ldap_group_import?
        respond_to do |f|
          f.html do
            render partial: "organizations/import_group",
              locals: {team: results.update_team.team}
          end
        end
      else
        head :ok
      end
    else
      if results.errors.all.any?
        @organization = this_organization
        @team = this_team

        flash.now[:error] = results.errors.all.values.flatten.first.to_s

        org_data = platform_execute(EditQuery,
          variables: {
            "login" => params[:org] || params[:organization_id],
            "slug" => params[:team_slug],
          },
        )

        variables["parentTeam"] = { "id" => attributes[:parent_team_id], "name" => attributes[:parent_team_name] }
        render(
          "orgs/teams/edit",
          locals: {
            graphql_org: org_data.organization,
            selected_nav_item: :settings,
            team: org_data.organization.team,
            team_params: variables,
            team_entity: @team,
          },
          layout: "team")
      else
        team = results.update_team.team
        requested_parent_team = if team.pending_team_change_parent_requests.edges.any?
          team.pending_team_change_parent_requests.edges.first.node.parent_team
        end

        if requested_parent_team && !requested_parent_team.viewer_can_administer?
          flash[:notice] = "Team was successfully updated. The requested parent team will be reflected pending approval."
          redirect_to results.to_h["updateTeam"]["team"]["resourcePath"]
        else
          flash[:notice] = "You’ve successfully updated the team."
          redirect_to results.to_h["updateTeam"]["team"]["resourcePath"]
        end
      end
    end
  end

  DeleteTeamMutation = parse_query <<-'GRAPHQL'
    mutation($teamId: ID!) {
      deleteTeam(input: { teamId: $teamId})  {
        __typename # A selection is grammatically required here
      }
    }
  GRAPHQL

  def destroy
    variables = {
      "teamId"        => this_team.global_relay_id,
    }

    results = platform_execute(DeleteTeamMutation, variables: variables)

    if request.xhr?
      if ldap_group_import?
        head 204
      else
        head 200
      end
    else
      if results.errors.all.any?
        @organization = this_organization
        @team = this_team

        flash.now[:error] = results.errors.all.values.flatten.first.to_s

        org_data = platform_execute(EditQuery,
          variables: {
            "login" => params[:org] || params[:organization_id],
            "slug" => params[:team_slug],
          },
        )

        render(
          "orgs/teams/edit",
          locals: {
            graphql_org: org_data.organization,
            selected_nav_item: :settings,
            team: org_data.organization.team,
            team_entity: @team,
          },
          layout: "team")
      else
        flash[:notice] = "You’ve successfully deleted the team. Updated permissions may take a few minutes to take effect."
        redirect_to teams_path(this_organization)
      end
    end
  end

  def destroy_teams
    teams_to_remove = this_organization.teams.where(id: params[:team_ids]).limit(20).to_a
    destroyed_teams = destroy_these_teams(teams_to_remove)

    case destroyed_teams.size
    when 1
      flash[:notice] = "You removed the #{destroyed_teams.first.name} team. Updated permissions may take a few minutes to take effect."
    else
      flash[:notice] = "You removed #{destroyed_teams.size} teams. Updated permissions may take a few minutes to take effect."
    end

    redirect_to teams_path(this_organization)
  end

  def destroy_team_teams
    teams_to_remove = this_team.descendants.where(id: params[:team_ids]).limit(20).to_a
    destroyed_teams = destroy_these_teams(teams_to_remove)

    case destroyed_teams.size
    when 1
      flash[:notice] = "You removed the #{destroyed_teams.first.name} team. Updated permissions may take a few minutes to take effect."
    else
      flash[:notice] = "You removed #{destroyed_teams.size} teams. Updated permissions may take a few minutes to take effect."
    end

    redirect_to team_teams_path(this_team)
  end

  private def destroy_these_teams(teams_to_remove)
    destroyed_teams = []

    teams_to_remove.each do |team|
      next if team.has_child_teams? && !this_organization.adminable_by?(current_user)
      if team.legacy_owners?
        DestroyTeamJob.perform_later(team.id)

        this_organization.update_attribute(:destroy_owners_team_attempted_at, Time.now)
      else
        team.destroy
      end
      destroyed_teams << team
    end
    destroyed_teams
  end

  def leave
    this_team.remove_member(current_user, send_notification: false)

    if this_organization.direct_or_team_member?(current_user)
      flash[:message] = "You’ve successfully left the team."

      if params[:return_to].present?
        safe_redirect_to params[:return_to]
      else
        redirect_to :back
      end
    else
      flash[:message] = "You’ve successfully left the organization."
      redirect_to user_path(this_organization)
    end
  end

  MinisearchQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $first: Int!, $after: String, $query: String, $membersFilter: TeamMembersFilter, $userLogins: [String!], $privacy: TeamPrivacy) {
      organization(login: $login) {
        viewerIsAMember
        ...Views::Orgs::Teams::Minisearch::OrgFragment
      }
    }
  GRAPHQL

  def minisearch
    graphql_data = platform_execute(MinisearchQuery, variables: teams_query_vars(query: params[:q]))
    return render_404 unless org = graphql_data.organization
    return render_404 unless org.viewer_is_a_member

    respond_to do |format|
      format.html do
        render partial: "orgs/teams/minisearch", locals: { org: org }
      end
    end
  end

  ParentSearchQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $query: String, $slug: String!) {
      organization(login: $login) {
        ...Views::Orgs::Teams::ParentSearch::Organization
      }
    }
  GRAPHQL

  def parent_search
    search_term = params[:q]

    org_data = platform_execute(ParentSearchQuery, variables: {
      "login" => params[:org] || params[:organization_id],
      "query" => search_term,
      "slug" => params[:team_slug],
    })

    respond_to do |format|
      format.html_fragment do
        render partial: "orgs/teams/parent_search", formats: :html, locals: {
          org: org_data.organization,
        }
      end
    end
  end

  def set_visibility
    team_ids = params[:team_ids]
    privacy = params[:privacy]
    privacy = privacy ? privacy.to_sym : :closed
    updates = this_organization.bulk_update_privacy(team_ids, privacy)

    flash[:notice] = if updates.size == 1
      "You've made #{Team.find(updates[0]).name} #{privacy_text(privacy)}."
    else
      "You've made #{updates.size} teams #{privacy_text(privacy)}."
    end

    redirect_to :back
  end

  def goto
    team = this_organization.teams.find_by_slug(params[:team_name])

    if team
      redirect_to team_path(team)
    else
      flash[:error] = "Team not found."
      redirect_to teams_path(this_organization)
    end
  end

  def check_name
    name = params[:value]
    return head 400 if name.blank?
    slug = name.parameterize

    # Name is unchanged for the team.
    if this_team.present? && this_team.slug == slug
      return head 200
    end

    # Name is already used.
    if this_organization.teams.find_by_name(name) || this_organization.teams.find_by_slug(slug)
      respond_to do |format|
        format.html_fragment do
          return render partial: "orgs/teams/name_message", formats: :html, status: 422, locals: { exists: true }
        end
      end
    end

    respond_to do |format|
      format.html_fragment do
        render partial: "orgs/teams/name_message", formats: :html, locals: {
          exists: false,
          slug: slug,
          org: this_organization,
        }
      end
    end
  end

  def ldap_group_suggestions
    groups = GitHub::LDAP.search.find_groups(params[:q])

    respond_to do |format|
      format.html_fragment do
        render partial: "orgs/teams/ldap_group_suggestions", formats: :html, locals: { groups: groups }
      end
      format.html do
        params[:dn]
        render partial: "orgs/teams/ldap_group_suggestions", locals: { groups: groups }
      end
    end
  end

  def dismiss_org_teams_banner
    current_user.dismiss_notice("org_teams_banner")

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  OwnersTeamQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      team: node(id: $id) {
        ... on Team {
          ...Views::Orgs::Teams::OwnersTeam::Team
        }
      }
    }
  GRAPHQL

  def owners_team
    return render_404 unless legacy_owners_team = this_organization.legacy_owners_team
    global_relay_id = legacy_owners_team.global_relay_id
    data = platform_execute(OwnersTeamQuery, variables: {id: global_relay_id})

    render(
      "orgs/teams/owners_team",
      locals: { organization: this_organization, team: data.team },
      layout: "application")
  end

  def rename_owners_team
    if legacy_owners_team = this_organization.legacy_owners_team
      legacy_owners_team.name = params[:name]

      unless legacy_owners_team.save
        flash.now[:error] = legacy_owners_team.errors.full_messages.to_sentence
        return render "orgs/teams/index"
      end

      redirect_to team_path(legacy_owners_team)
    else
      return render_404
    end
  end

  def destroy_owners_team
    if legacy_owners_team = this_organization.legacy_owners_team
      DestroyTeamJob.perform_later(legacy_owners_team.id)

      this_organization.update_attribute(:destroy_owners_team_attempted_at, Time.now)
      GitHub.dogstats.increment("team", tags: ["action:destroy", "type:organization_legacy_owners"])

      flash[:notice] = "Deleting the Owners team. This may take a few minutes to complete."
    end

    redirect_to teams_path(this_organization)
  end

  def migrate_legacy_admin_team
    this_team.migrate_legacy_admin
    flash[:notice] = "You've successfully migrated the team."

    redirect_to team_path(this_team)
  end

  ImportantChangesSummaryQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!, $first: Int!) {
      organization(login: $login) {
        newParent: team(slug: $slug) {
          ...Views::Orgs::Teams::ImportantChangesSummary
        }
      }
    }
  GRAPHQL

  PARENT_SUMMARY_REPOSITORY_LIMIT = 30

  def important_changes_summary
    return render_404 unless request.xhr?

    graphql_data =
      if params[:parent_team].present?
        platform_execute ImportantChangesSummaryQuery, variables: {
          login: params[:org] || params[:organization_id],
          slug: params[:parent_team],
          first: PARENT_SUMMARY_REPOSITORY_LIMIT,
        }
      end

    visibility_changed = params[:visibility_changed] == "true"
    parent_changed = params[:parent_changed] == "true"

    respond_to do |format|
      format.html do
        render "orgs/teams/important_changes_summary", layout: false, locals: {
          edit_team_form: params[:edit_team].present?,
          new_parent: graphql_data&.organization&.new_parent,
          visibility_changed: visibility_changed,
          parent_changed: parent_changed,
        }
      end
    end
  end

  def teams_toolbar_actions
    respond_to do |format|
      format.html do
        render_partial_view "orgs/teams/teams_toolbar_actions", Orgs::Teams::ToolbarActionsView,
          organization: this_organization,
          destroy_teams_path: org_destroy_teams_path(this_organization),
          adminable_by_user: this_organization.adminable_by?(current_user),
          selected_teams: this_organization.teams.where(slug: params[:team_slugs] || [])
      end
    end
  end

  def team_teams_toolbar_actions
    respond_to do |format|
      format.html do
        render_partial_view "orgs/teams/teams_toolbar_actions", Orgs::Teams::ToolbarActionsView,
          organization: this_organization,
          destroy_teams_path: destroy_team_teams_path(this_team),
          adminable_by_user: this_team.adminable_by?(current_user),
          selected_teams: this_organization.teams.where(slug: params[:team_slugs] || []),
          is_child_team: true
      end
    end
  end

  MembersToolbarActionsQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!) {
      organization(login: $login) {
        team(slug: $slug) {
          ...Views::Orgs::Teams::MembersToolbarActions::Team
        }
      }
    }
  GRAPHQL

  def members_toolbar_actions
    data = platform_execute(MembersToolbarActionsQuery,
      variables: { "login" => params[:org] || params[:organization_id], "slug" => params[:team_slug] },
    )
    respond_to do |format|
      format.html do
        render partial: "orgs/teams/members_toolbar_actions", locals: {
          team: data.organization.team,
          selected_team_members: this_team.members.where(login: params[:member_logins] || []),
        }
      end
    end
  end

  RepositoriesToolbarActionsQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!) {
      organization(login: $login) {
        team(slug: $slug) {
          ...Views::Orgs::Teams::RepositoriesToolbarActions::Team
        }
      }
    }
  GRAPHQL

  def repositories_toolbar_actions
    data = platform_execute(RepositoriesToolbarActionsQuery,
      variables: { "login" => params[:org] || params[:organization_id], "slug" => params[:team_slug] },
    )

    repository_names = Array.wrap(params[:repository_names])
    repositories = this_team.repositories_scope(affiliation: :immediate).where(name: repository_names)

    respond_to do |format|
      format.html do
        render partial: "orgs/teams/repositories_toolbar_actions", locals: {
          team: data.organization.team,
          selected_repository_names: repository_names,
          selected_repositories: repositories,
        }
      end
    end
  end

  TeamBreadcrumbsQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!, $first: Int!) {
      organization(login: $login) {
        viewerIsAMember
        team(slug: $slug)
        ...Views::Orgs::Teams::TeamBreadcrumbs::Organization
      }
    }
  GRAPHQL

  # The maximum number of ancestors to select for a given team when rendering a
  # team's breadcrumb hierarchy. Chosen based [platform data's nested team
  # ADR][1] but should be revised for usability and/or performance reasons
  # based on real-world usage.
  #
  # [1]: https://github.com/github/platform-data/blob/88d91bd670c843e2c3bb213ad7b95918e7fe0fc1/docs/abilities/adr_nested_teams.md#path-encoding
  TEAM_BREADCRUMBS_MAX_DEPTH = 85

  def team_breadcrumbs
    graphql_data = platform_execute(
      TeamBreadcrumbsQuery,
      variables: {login: params[:org], slug: params[:team_slug], first: TEAM_BREADCRUMBS_MAX_DEPTH + 1},
    )

    return render_404 unless graphql_org = graphql_data.organization
    return render_404 unless graphql_org.viewer_is_a_member
    return render_404 unless graphql_org.team

    respond_to do |format|
      format.html do
        render partial: "orgs/teams/team_breadcrumbs", locals: { org: graphql_org}
      end
    end
  end

  private

  TEAM_PAGE_SIZE = 20

  def team_params
    return ActionController::Parameters.new if params[:team].blank?

    params.require(:team).permit %i[
      name
      description
      permission
      ldap_dn
      privacy
      parent_team_id
      parent_team_name
    ].concat [group_mappings: {}]
  end

  def teams_query_vars(query: nil)
    query ||= params[:query]

    variables = graphql_pagination_params(page_size: TEAM_PAGE_SIZE)
    variables["firstRequests"]     = TEAM_PAGE_SIZE
    variables["login"]             = params[:org] || params[:organization_id]
    variables["slug"]              = params[:team_slug] if params[:team_slug]
    variables["nestedList"]        = !query.present?
    variables["immediateOnly"]     = variables["nestedList"] ? true : false
    variables["isMaintainer"]      = !!this_team&.adminable_by?(current_user)

    query = TeamSearchQuery.new(query)
    variables["membersFilter"]     = query.members_filter.upcase if query.members_filter.present?
    variables["userLogins"]        = query.users_filter if query.users_filter.present?
    variables["privacy"]           = query.visibility_filter.upcase if query.visibility_filter.present?
    variables["query"]             = query.cleaned_query if query.cleaned_query.present?
    variables
  end

  # Internal: Differentiate XHR requests for LDAP Group import.
  def ldap_group_import?
    return false unless GitHub.ldap_sync_enabled?
    request.headers["X-Context"] == "import"
  end

  def privacy_text(privacy)
    privacy == :closed ? "visible" : "secret"
  end

  def mask_analytics_data
    url = if this_team
      "/orgs/<org-login>/teams/<team-name>/#{action_name}"
    else
      "/orgs/<org-login>/teams/#{action_name}"
    end
    override_analytics_location url
    strip_analytics_query_string
  end

  def transform_team_sync_mapping_inputs(attrs)
    return if !params.key?(:manage_group_mappings) && attrs.blank?
    return [] if params.key?(:manage_group_mappings) && attrs.blank?

    attrs.to_h.map do |group_id, attrs|
      {
        "groupId" => group_id,
        "groupName" => attrs[:name],
        "groupDescription" => attrs[:description],
      }
    end
  end
end
