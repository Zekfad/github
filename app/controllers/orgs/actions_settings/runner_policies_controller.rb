# frozen_string_literal: true

class Orgs::ActionsSettings::RunnerPoliciesController < Orgs::Controller
  map_to_service :actions_runners

  include OrganizationsHelper
  include Actions::RunnersHelper

  before_action :login_required
  before_action :ensure_organization_exists
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :ensure_can_use_org_runners

  def index
    access_policy = access_policy_for(current_organization)
    selected_repositories = access_policy.selected_repositories.map { |identity| identity.global_id }.to_set

    selected_repository_ids = selected_repositories.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    repositories = current_organization.repositories.where(id: selected_repository_ids)

    render partial: "settings/organization/actions/runner_permissions_modal", locals: {
      access_policy: access_policy,
      organization: current_organization,
      permissions: Organizations::Settings::RepositoryPermissionsComponent::PERMISSIONS,
      repositories: repositories,
      selected_repositories: selected_repositories
    }
  end

  def update
    permission_type = params[:permission_type]
    selected_repositories = permission_type == Organizations::Settings::RepositoryPermissionsComponent::SELECTED_REPOSITORIES_TYPE.to_s ? Array(params[:repository_ids]) : []
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.update_access_policy(current_organization,
                                                                   permission_type: permission_type,
                                                                   selected_repositories: selected_repositories)
    end

    flash[:success] = "Policy updated."
    redirect_to settings_org_actions_path
  end
end
