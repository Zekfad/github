# frozen_string_literal: true

class Orgs::ActionsSettings::PoliciesController < Orgs::Controller
  map_to_service :actions_experience

  include OrganizationsHelper

  before_action :login_required
  before_action :ensure_organization_exists
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access

  def update
    begin
      policy = Actions::PolicyResolver.perform(
        entity: current_organization,
        policy: params[:policy]
      )

      current_organization.update_action_execution_capabilities(policy, actor: current_user)
    rescue Configurable::ActionExecutionCapabilities::Error
      flash[:error] = "Sorry, there was an issue updating your settings."
    ensure
      redirect_to settings_org_actions_path
    end
  end

  def repo_dialog
    render partial: "settings/organization/actions/repo_dialog", locals: {
      organization: current_organization,
      repositories: selected_repos,
      selected_repositories: selected_repos.map(&:global_relay_id)
    }
  end

  def update_repos
    nodes_to_enable = (params[:enable] || []).to_set
    ids_to_enable = nodes_to_enable.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    nodes_to_disable = (params[:disable] || []).to_set
    ids_to_disable = nodes_to_disable.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }

    current_organization.repositories.where(id: ids_to_enable).each do |repo|
      repo.allow_actions(actor: current_user)
    end

    current_organization.repositories.where(id: ids_to_disable).each do |repo|
      repo.disallow_actions(actor: current_user)
    end

    redirect_to settings_org_actions_path
  end

  private

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end

  def selected_repos
    return @_selected_repos if defined? @_selected_repos
    @_selected_repos = current_organization.repositories.with_ids(current_organization.actions_allowed_entities)
  end
end
