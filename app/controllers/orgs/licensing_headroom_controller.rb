# frozen_string_literal: true

class Orgs::LicensingHeadroomController < Orgs::Controller
  areas_of_responsibility :gitcoin

  before_action :login_required
  before_action :organization_admin_required

  def show
    render Organizations::LicensingHeadroomComponent.new(
      remaining_units: this_organization.available_seats,
      unit_of_measure: helpers.seat_or_license(this_organization),
      more_units_link_markup: helpers.more_seats_link_for_organization(this_organization, self_serve_return_to: org_people_path(this_organization)),
      more_information_markup: helpers.render_seats_more_info(this_organization)
    )
  end
end
