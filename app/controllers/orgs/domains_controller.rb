# frozen_string_literal: true

class Orgs::DomainsController < Orgs::Controller
  areas_of_responsibility :orgs

  statsd_tag_actions only: :create

  before_action :login_required
  before_action :organization_admin_required
  before_action :ensure_domain_verification_enabled

  NewQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on Organization {
          ...Views::Orgs::Domains::New::Organization
        }
      }
    }
  GRAPHQL

  def new
    data = platform_execute(NewQuery, variables: { id: this_organization.global_relay_id })
    render "orgs/domains/new", locals: { organization: data.node }
  end

  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: AddOrganizationDomainInput!) {
      addOrganizationDomain(input: $input) {
        organizationDomain {
          id
        }
      }
    }
  GRAPHQL

  def create
    org_domain_params = params.require(:organization_domain).permit(:domain)
    input = org_domain_params.merge(organizationId: this_organization.global_relay_id)
    data = platform_execute(CreateQuery, variables: { input: input })

    if data.errors.any?
      flash[:error] = data.errors.messages.values.join(", ")
      data = platform_execute(NewQuery, variables: { id: this_organization.global_relay_id })
      render "orgs/domains/new", locals: { organization: data.node }
    else
      id = data.add_organization_domain.organization_domain.id
      redirect_to verification_steps_org_domain_path(this_organization, id)
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on OrganizationDomain {
          isVerified
          ...Views::Orgs::Domains::VerificationSteps::OrganizationDomain
        }
      }
    }
  GRAPHQL

  def verification_steps
    data = platform_execute(ShowQuery, variables: { id: params[:id] })
    return render_404 unless data.node

    if data.node.is_verified # can't re-verify a domain
      redirect_to settings_org_domains_path(this_organization)
    else
      render "orgs/domains/verification_steps", locals: { organization_domain: data.node }
    end
  end

  RegenerateQuery = parse_query <<-'GRAPHQL'
    mutation($input: RegenerateOrganizationDomainTokenInput!) {
      regenerateOrganizationDomainToken(input: $input) {
        verificationToken
      }
    }
  GRAPHQL

  def regenerate_token
    data = platform_execute(RegenerateQuery, variables: { input: { id: params[:id] } })
    if data.errors.any?
      flash[:error] = data.errors.messages.values.join(", ")
    end

    redirect_to verification_steps_org_domain_path(this_organization, params[:id])
  end

  VerifyQuery = parse_query <<-'GRAPHQL'
    mutation($input: VerifyOrganizationDomainInput!) {
      verifyOrganizationDomain(input: $input) {
        organizationDomain {
          domain
        }
      }
    }
  GRAPHQL

  def verify
    data = platform_execute(VerifyQuery, variables: { input: { id: params[:id] } })

    if data.errors.any?
      flash[:error] = data.errors.messages.values.join(", ")
      redirect_to verification_steps_org_domain_path(this_organization, params[:id])
    else
      verified_domain = data.verify_organization_domain.organization_domain.domain
      flash[:notice] = "You verified the domain #{verified_domain}."
      redirect_to settings_org_domains_path(this_organization)
    end
  end

  DeleteQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteOrganizationDomainInput!) {
      deleteOrganizationDomain(input: $input) {
        organization
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DeleteQuery, variables: { input: { id: params[:id] } })
    flash[:notice] = "The organization domain was deleted."
    redirect_to settings_org_domains_path(this_organization)
  end
end
