# frozen_string_literal: true

class Orgs::IntegrationsController < Orgs::Controller
  areas_of_responsibility :platform

  statsd_tag_actions only: [:create, :installations]

  include IntegrationsControllerMethods
  include IntegrationManagerHelper

  before_action :require_any_app_manager, only: [:index]
  before_action :require_current_app_manager, only: [:show, :update, :permissions, :installations, :advanced, :update_permissions, :generate_key, :make_public, :make_internal, :destroy, :reset_secret, :keys, :remove_key, :revoke_all_tokens, :transfer, :beta_features, :beta_toggle]
  before_action :require_all_apps_manager, except: [:index, :show, :update, :permissions, :installations, :advanced, :update_permissions, :generate_key, :make_public, :make_internal, :destroy, :reset_secret, :keys, :remove_key, :revoke_all_tokens, :transfer, :beta_features, :beta_toggle, :receive_manifest]

  # Even though the new_from_manifest action is a POST, it is idempotent and merely presents
  # the user with a view without making any changses to the database. Since the client side POST
  # is initiated from a host that is not GitHub, CSRF checks need to be disabled.
  skip_before_action :verify_authenticity_token, only: [:receive_manifest]

  javascript_bundle :settings

  def index
    integrations = current_context.integrations.not_for_github_connect

    if manages_all_integrations?(user: current_user, owner: current_context)
      pending_transfers = current_context.inbound_integration_transfers.includes(:integration, :requester)
    else
      integrations = integrations.where(
        id: Permissions::Enumerator.subject_ids_for_permission(action: :manage_app, actor_id: current_user.id),
      )
    end
    integrations = integrations.order("integrations.name asc").paginate(page: current_page, per_page: 15)

    render_template_view "integrations/settings/index", Integrations::IndexView,
      integrations: integrations,
      pending_transfers: pending_transfers,
      owner: current_context
  end

  private

  def current_context
    this_organization
  end

  def require_all_apps_manager
    render_404 unless manages_all_integrations?(user: current_user, owner: this_organization)
  end

  def require_any_app_manager
    render_404 unless manages_any_integration?(user: current_user, organization: this_organization)
  end

  def require_current_app_manager
    render_404 unless manages_integration?(user: current_user, integration: current_integration)
  end
end
