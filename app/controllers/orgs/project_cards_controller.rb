# frozen_string_literal: true

class Orgs::ProjectCardsController < Orgs::Controller
  include ProjectCardControllerActions
  include SharedProjectControllerActions

  statsd_tag_actions only: [:archive, :archived, :check_archived, :index, :show, :update]

  before_action :project_read_required
  before_action :require_projects_enabled
  before_action :project_write_required, except: [:archived, :check_archived, :index, :pull_request_status, :search_archived, :show, :closing_references]
  before_action :set_client_uid
  before_action :set_cache_control_no_store, only: [:index]

  rescue_from ProjectColumn::PrioritizingArchivedCardError, ProjectColumn::InvalidPrioritizationTargetError do |error|
    render_422(error)
  end

  def index
    render_project_cards_index(project: this_project)
  end

  def archived
    render_archived_project_cards(project: this_project)
  end

  def search_archived
    render_search_archived_project_cards(project: this_project)
  end

  def check_archived
    render_check_archived_project_card(project: this_project)
  end

  def show
    render_show_project_card(project: this_project)
  end

  def update
    update_project_card(project: this_project)
  end

  def update_note
    update_project_card_note(project: this_project)
  end

  def update_note_task_list
    update_project_card_note_task_list(project: this_project)
  end

  def convert_to_issue
    repository_id = params[:repository_id]&.to_i

    associated_repo_ids = current_user.associated_repository_ids(
      including: [:direct, :indirect],
      repository_ids: [repository_id],
    )

    repository = Repository.owned_by(this_organization).where(id: associated_repo_ids).find(repository_id)

    convert_project_card_to_issue(project: this_project, repository: repository)
  end

  def preview_note
    preview_project_note(project: this_project)
  end

  def pull_request_status
    render_pull_request_status(project: this_project)
  end

  def archive
    archive_project_card(project: this_project)
  end

  def unarchive
    unarchive_project_card(project: this_project)
  end

  def closing_references
    render_project_card_closing_references(project: this_project)
  end

  def closing_reference
    render_project_card_closing_reference(project: this_project)
  end

  def destroy
    destroy_project_card(project: this_project)
  end

  private

  def this_project
    @this_project ||= this_organization.visible_projects_for(current_user).find_by_number!(params[:project_number])
  end

  def project_read_required
    render_404 unless this_project.readable_by?(current_user)
  end

  def project_write_required
    render_404 unless this_project.writable_by?(current_user)
  end
end
