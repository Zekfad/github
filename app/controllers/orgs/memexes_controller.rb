# frozen_string_literal: true

class Orgs::MemexesController < Orgs::Controller
  include MemexesHelper

  MEMEX_INDEX_LIMIT = 30

  before_action :login_required
  before_action :require_memex_enabled
  before_action :organization_read_required
  before_action :require_this_memex, only: [:show, :update]
  before_action :require_valid_content_parameters, only: [:create]
  before_action :require_this_content_repository, only: [:create]
  before_action :require_this_content, only: [:create]
  before_action :require_this_repository, only: [:suggested_issues_and_pulls]
  before_action :require_verified_email, except: [:new, :suggested_repositories, :suggested_issues_and_pulls]
  before_action :require_title_or_item_or_column, only: [:create]
  before_action :prefill_memex_item_associations, only: [:show]

  before_action :set_client_uid
  before_action :add_memex_csp_exceptions
  before_action :set_cache_control_no_store

  statsd_tag_actions

  layout "memex"

  def index
    parsed_query = Search::Queries::MemexProjectQuery.new(params[:query], viewer: current_user)

    search_result = this_organization.search_memex_projects(
      query: parsed_query,
      viewer: current_user,
      cursor: params[:cursor],
      limit: MEMEX_INDEX_LIMIT,
    )

    locals = {
      project_owner: this_organization,
      memexes: search_result.memex_projects,
      has_next_page: search_result.has_next_page?,
      cursor: search_result.next_page_cursor,
      parsed_query: parsed_query,
    }

    if request.xhr?
      render(partial: "memexes/list", locals: locals)
    else
      render(
        "memexes/index",
        locals: locals.merge(
          open_memex_count: search_result.total_open_count,
          closed_memex_count: search_result.total_closed_count,
        ),
        layout: "org_projects"
      )
    end
  end

  def new
    render(
      "memexes/new",
      locals: {
        page_title: "New table",
        columns: MemexProjectColumn.default_columns,
        create_endpoint_csrf_data: csrf_data(create_org_memex_path(this_organization.login)),
        suggested_repositories_endpoint_data: { url: org_memex_suggested_repositories_path(this_organization.login) },
        suggested_issues_and_pulls_endpoint_data: { url: org_memex_suggested_issues_and_pulls_path(this_organization.login) },
      },
    )
  end

  def suggested_repositories
    suggester = MemexContentSuggester.new(viewer: current_user,
                                          organization: this_organization)

    render(json: camelize_keys(repositories: suggester.repositories))
  end

  def suggested_issues_and_pulls
    suggester = MemexContentSuggester.new(viewer: current_user,
                                          organization: this_organization)

    suggestions = suggester.issues_and_prs_for_repository(
      this_repository,
      memex: find_readable_memex(underscored_params[:memex_number])
    )

    render(json: camelize_keys(suggestions: suggestions))
  end

  def show
    render(
      "memexes/show",
      locals: {
        page_title: this_memex.display_title,
        memex: this_memex,
        items: serialized_memex_item_result.items,
        columns: this_memex.columns,
        sorted_by: serialized_memex_item_result.active_sort,
        update_endpoint_csrf_data: csrf_data(update_org_memex_path(this_memex.owner.login, this_memex.number), method: :put),
        item_create_endpoint_csrf_data: csrf_data(item_create_endpoint(memex: this_memex)),
        item_update_endpoint_csrf_data: csrf_data(item_update_endpoint(memex: this_memex), method: :put),
        item_delete_endpoint_csrf_data: csrf_data(item_delete_endpoint(memex: this_memex), method: :delete),
        item_sort_endpoint_data: { url: org_memex_items_path(this_memex.owner.login, this_memex.number) },
        item_suggested_assignees_endpoint_data: { url: org_memex_item_suggested_assignees_path(this_organization.login, this_memex.number) },
        item_suggested_labels_endpoint_data: { url: org_memex_item_suggested_labels_path(this_organization.login, this_memex.number) },
        item_suggested_milestones_endpoint_data: { url: org_memex_item_suggested_milestones_path(this_organization.login, this_memex.number) },
        column_create_endpoint_csrf_data: csrf_data(create_org_memex_project_column_path(this_organization.login, this_memex.number), method: :post),
        column_update_endpoint_csrf_data: csrf_data(update_org_memex_project_column_path(this_organization.login, this_memex.number), method: :put),
        suggested_repositories_endpoint_data: { url: org_memex_suggested_repositories_path(this_organization.login) },
        suggested_issues_and_pulls_endpoint_data: { url: org_memex_suggested_issues_and_pulls_path(this_organization.login) },
      },
    )
  end

  def create
    memex = MemexProject.create_with_associations(
      owner: this_organization,
      creator: current_user,
      title: create_memex_params[:title],
      item_options: {
        draft_issue_title: create_memex_first_item_params.dig(:content, :title),
        issue_or_pull: this_content,
      },
      column_options: create_memex_first_column_params,
    )

    if memex.valid? && memex.persisted?
      items = memex.memex_project_items.all.to_a
      GitHub::PrefillAssociations.for_memex_project_items(items, columns: memex.visible_columns)

      newly_modified_column = if (column_data_type = create_memex_first_column_params[:data_type])
        memex.columns.find { |c| c.data_type == column_data_type }
      elsif (column_name = create_memex_first_column_params[:name])
        memex.find_column_by_name_or_id(column_name)
      end

      render(
        json: camelize_keys(
          {
            memex_update_api_data: csrf_data(update_org_memex_path(memex.owner.login, memex.number), method: :put),
            memex_item_create_api_data: csrf_data(item_create_endpoint(memex: memex)),
            memex_item_update_api_data: csrf_data(item_update_endpoint(memex: memex), method: :put),
            memex_item_delete_api_data: csrf_data(item_delete_endpoint(memex: memex), method: :delete),
            memex_item_sort_api_data: { url: org_memex_items_path(memex.owner.login, memex.number) },
            memex_item_suggested_assignees_api_data: { url: org_memex_item_suggested_assignees_path(memex.owner.login, memex.number) },
            memex_item_suggested_labels_api_data: { url: org_memex_item_suggested_labels_path(this_organization.login, memex.number) },
            memex_item_suggested_milestones_api_data: { url: org_memex_item_suggested_milestones_path(this_organization.login, memex.number) },
            memex_column_create_api_data: csrf_data(create_org_memex_project_column_path(memex.owner.login, memex.number), method: :post),
            memex_column_update_api_data: csrf_data(update_org_memex_project_column_path(memex.owner.login, memex.number), method: :put),
            memex_project: memex.to_hash,
            memex_project_items: items.map { |i| i.to_hash(columns: memex.visible_columns) },
            memex_project_columns: [newly_modified_column&.to_hash].compact,
          },
        ),
        status: :created,
      )
    else
      render(json: { errors: memex.errors.full_messages }, status: :unprocessable_entity)
    end
  rescue ActiveRecord::RecordInvalid => e
    render(json: { errors: e.record.errors.full_messages }, status: :unprocessable_entity)
  end

  def update
    final_update_params = update_memex_params.slice(:title, :description)

    closed = update_memex_params[:closed]&.to_s&.downcase
    if ["1", "true"].include?(closed)
      final_update_params[:closed_at] = Time.zone.now
    elsif ["0", "false"].include?(closed)
      final_update_params[:closed_at] = nil
    end

    success = final_update_params.empty? || this_memex.update(final_update_params)

    if request.xhr? && success
      render(json: camelize_keys(memex_project: this_memex.to_hash))
    elsif request.xhr?
      render(json: { errors: memex.errors.full_messages }, status: :unprocessable_entity)
    elsif success
      verb = if final_update_params.has_key?(:closed_at) && final_update_params[:closed_at]
        "closed"
      else
        "reopened"
      end
      flash[:notice] = "Memex #{verb}."
      redirect_to :back
    else
      flash[:error] = "You cannot perform that action at this time."
      redirect_to :back
    end
  end

  private

  def create_memex_params
    underscored_params
      .require(:memex_project)
      .permit(
        :title,
        memex_project_items: [:content_type, content: [:title, :id, :repository_id]],
        memex_project_columns: [:data_type, :name, :position, :visible]
      )
  end

  def create_memex_first_item_params
    create_memex_params[:memex_project_items]&.first || {}
  end

  def create_memex_first_column_params
    create_memex_params[:memex_project_columns]&.first || {}
  end

  def update_memex_params
    underscored_params.permit(
      # These are params from routing.
      :org,
      :memex_number,
      :user_id,

      # These are additional params that Rails includes with an HTML update.
      :_method,
      :authenticity_token,
      :client_uid,

      # These are the params that are actually used for mass assignment.
      :title,
      :description,
      :closed,
    )
  end

  def this_memex
    return @this_memex if defined?(@this_memex)
    @this_memex = find_readable_memex(params[:memex_number])
  end

  def this_memex_items
    return @this_memex_items if defined?(@this_memex_items)
    @this_memex_items = this_memex.prioritized_memex_project_items.limit(MemexProjectItem::PER_PAGE_LIMIT).to_a
  end

  def this_content
    return @this_content if defined?(@this_content)
    @this_content = if creating_first_item?
      find_content(
        repository: this_content_repository,
        content_type: create_memex_first_item_params[:content_type],
        content_id: create_memex_first_item_params[:content][:id],
      )
    else
      nil
    end
  end

  def this_content_repository
    return @this_content_repository if defined?(@this_content_repository)
    repository_id = create_memex_first_item_params[:content][:repository_id]
    @this_content_repository = repository_id && this_organization.repositories.find_by_id(repository_id)
  end

  def this_repository
    return @this_repository if defined?(@this_repository)
    @this_repository = this_organization.repositories.find_by_id(underscored_params[:repository_id])
  end

  def find_readable_memex(memex_number)
    return nil unless memex_number
    this_organization.memex_projects.includes(:owner).find_by(number: memex_number)
  end

  def serialized_memex_item_result
    @serialized_memex_item_result ||= begin
      sort_params = underscored_params.fetch(:sorted_by, {})

      serializer = MemexProjectItemSerializer.new(viewer: current_user,
                                                  memex: this_memex,
                                                  items: this_memex_items,
                                                  sort_column: sort_params[:column_id],
                                                  direction: sort_params[:direction])

      serializer.result
    end
  end

  def require_title_or_item_or_column
    if !(
      create_memex_params[:title] ||
      create_memex_params[:memex_project_items] ||
      create_memex_params[:memex_project_columns]
    )
      return render_json_error(
        error: "You must provide a title, an item or a column",
        status: :unprocessable_entity,
      )
    end

    if create_memex_params.fetch(:memex_project_items, []).length > 1
      return render_json_error(
        error: "You must provide only one item",
        status: :unprocessable_entity,
      )
    end

    if create_memex_params.fetch(:memex_project_columns, []).length > 1
      return render_json_error(
        error: "You must provide only one column",
        status: :unprocessable_entity,
      )
    end

    column_data_type = create_memex_first_column_params&.fetch(:data_type, nil)
    if column_data_type && column_data_type.to_sym != :text
      return render_json_error(error: "Column must have a data type of text", status: :unprocessable_entity)
    end

    column_position = create_memex_first_column_params&.fetch(:position, nil)
    if column_position && column_position.to_i < 1
      return render_json_error(
        error: "Column position must be a positive integer",
        status: :unprocessable_entity
      )
    end
  end

  def require_valid_content_parameters
    return unless creating_first_item?
    verify_content_params(create_memex_first_item_params)
  end

  def require_this_content_repository
    return unless creating_first_item? && create_memex_first_item_params[:content_type] != DraftIssue.name
    render_404 unless this_content_repository&.readable_by?(current_user)
  end

  def require_this_content
    return if !creating_first_item? || create_memex_first_item_params[:content_type] == DraftIssue.name
    render_404 unless this_content&.readable_by?(current_user)
  end

  def require_this_repository
    render_404 unless this_repository&.readable_by?(current_user)
  end

  def creating_first_item?
    !!(params[:action] == "create" && create_memex_first_item_params.present?)
  end

  def prefill_memex_item_associations
    return unless this_memex
    return if this_memex_items.empty?
    GitHub::PrefillAssociations.for_memex_project_items(
      this_memex_items,
      columns: this_memex.visible_columns
    )
  end

  def csrf_data(path, method: :post)
    {
      url: path,
      token: authenticity_token_for(path, method: method),
    }
  end

  def item_create_endpoint(memex:)
    create_org_memex_item_path(memex.owner.login, memex.number)
  end

  # We're using slightly unusual update and delete endpoints so that we don't have to generate CSRF
  # tokens for every item on the page. As a result, those endpoint URLs are the same as the create
  # one (although they are invoked via different HTTP methods).
  alias_method :item_update_endpoint, :item_create_endpoint
  alias_method :item_delete_endpoint, :item_create_endpoint
end
