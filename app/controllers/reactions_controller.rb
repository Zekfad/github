# frozen_string_literal: true

class ReactionsController < ApplicationController
  areas_of_responsibility :orgs

  AddReactionMutation = parse_query <<-'GRAPHQL'
    mutation($input: AddReactionInput!) {
      updateReaction: addReaction(input: $input) {
        subject {
          ... on Discussion {
            databaseId
          }
          ... on DiscussionComment {
            databaseId
          }
          ...Views::Comments::Reactions::Subject
          ...Views::Comments::CommentHeaderReactionButton::Subject
        }
      }
    }
  GRAPHQL

  RemoveReactionMutation = parse_query <<-'GRAPHQL'
    mutation($input: RemoveReactionInput!) {
      updateReaction: removeReaction(input: $input) {
        subject {
          ... on Discussion {
            databaseId
          }
          ... on DiscussionComment {
            databaseId
          }
          ...Views::Comments::Reactions::Subject
          ...Views::Comments::CommentHeaderReactionButton::Subject
        }
      }
    }
  GRAPHQL

  MobileAddReactionMutation = parse_query <<-'GRAPHQL'
    mutation($input: AddReactionInput!) {
      updateReaction: addReaction(input: $input) {
        subject {
          ...Views::Mobile::Discussions::Reactions::Groups::Subject
        }
      }
    }
  GRAPHQL

  MobileRemoveReactionMutation = parse_query <<-'GRAPHQL'
    mutation($input: RemoveReactionInput!) {
      updateReaction: removeReaction(input: $input) {
        subject {
          ...Views::Mobile::Discussions::Reactions::Groups::Subject
        }
      }
    }
  GRAPHQL

  def update
    content, command = params[:input][:content].to_s.split
    is_mobile = params[:mobile].present?

    operation = if command == "unreact"
      is_mobile ? MobileRemoveReactionMutation : RemoveReactionMutation
    else
      is_mobile ? MobileAddReactionMutation : AddReactionMutation
    end

    data = platform_execute(operation, variables: {
      input: {
        subjectId: params[:input][:subjectId],
        content: content,
      },
    })

    track_issue_edits_from_project_board(edited_fields: ["reactions"])

    if request.xhr?
      respond_to do |format|
        format.json do
          if subject = (data.update_reaction && data.update_reaction.subject)
            if is_mobile
              render json: {
                reactions_options: render_to_string(partial: "mobile/discussions/reactions/groups", locals: { subject: subject }, formats: [:html]),
              }
            else
              render json: {
                comment_header_reaction_button: comment_header_reaction_button(subject),
                reactions_container: reactions_container(subject),
              }
            end
          else
            render_graphql_error(data)
          end
        end
      end
    else
      redirect_to :back
    end

  # TODO: Extract rescue_from for malformed variable inputs
  rescue PlatformHelper::MutationInputError
    head :bad_request
  end

  ReactionQuery = parse_query <<-'GRAPHQL'
    query($subjectId: ID!, $afterId: String, $content: ReactionContent) {
      node(id: $subjectId) {
        ... on Reactable {
          reactionPath
          reactionGroups {
            content
            users {
              totalCount
            }
          }
          reactions(first: 10, after: $afterId, content: $content, orderBy: { field: CREATED_AT, direction: DESC }) {
            nodes {
              user {
                avatarUrl
                login
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    }
  GRAPHQL

  def list
    content = params[:content]
    subject_id = params.require(:subject_id)
    data = reactions_for(subject_id, content)
    respond_to do |format|
      format.html do
        if data.errors.any?
          render_graphql_error(data)
        else
          render partial: "reactions/list", locals: {
            reactions: data.node.reactions.to_h,
            groups: Hash[data.node.reaction_groups.select { |g| g.users.total_count > 0 }.map { |g| [g.content, g.users.total_count] }],
            reaction_path: data.node.reaction_path,
            selected_content: content,
            subject_id: subject_id
          }
        end
      end
    end
  end

  def group
    content = params[:content]
    subject_id = params.require(:subject_id)
    data = reactions_for(subject_id, content, params[:after_id])
    respond_to do |format|
      format.html do
        if data.errors.any?
          render_graphql_error(data)
        else
          render partial: "reactions/group", locals: {
            reactions: data.node.reactions.to_h,
            content: content,
            reaction_path: data.node.reaction_path,
            subject_id: subject_id
          }
        end
      end
    end
  end

  private

  def reactions_for(subject_id, content, after_id = nil)
    platform_execute(ReactionQuery, variables: {
      subjectId: subject_id,
      content: content,
      afterId: params[:after_id]
    })
  end

  def reactions_container(subject)
    options = { formats: [:html] }
    if subject.is_a?(PlatformTypes::Discussion) || subject.is_a?(PlatformTypes::DiscussionComment)
      options[:partial] = "discussions/reactions"
      options[:locals] = discussion_view_locals(subject)
    else
      options[:partial] = "comments/reactions"
      options[:locals] = { subject: subject }
    end
    render_to_string(options)
  end

  def comment_header_reaction_button(subject)
    options = { formats: [:html] }
    if subject.is_a?(PlatformTypes::Discussion) || subject.is_a?(PlatformTypes::DiscussionComment)
      options[:partial] = "discussions/header_reaction_button"
      options[:locals] = discussion_view_locals(subject)
    else
      options[:partial] = "comments/comment_header_reaction_button"
      options[:locals] = { subject: subject }
    end
    render_to_string(options)
  end

  def discussion_view_locals(subject)
    discussion_or_comment = if subject.is_a?(PlatformTypes::Discussion)
      Discussion.find(subject.database_id)
    else
      DiscussionComment.find(subject.database_id)
    end

    @current_repository = discussion_or_comment.repository
    discussion = if discussion_or_comment.is_a?(Discussion)
      discussion_or_comment
    else
      discussion_or_comment.discussion
    end
    render_context = DiscussionTimeline::SingleCommentRenderContext.new(
      discussion,
      discussion_or_comment,
      viewer: current_user,
    )
    timeline = DiscussionTimeline.new(render_context: render_context)
    { discussion_or_comment: discussion_or_comment, timeline: timeline }
  end

  def target_for_conditional_access
    org = case params[:context]
    when "team"
      Organization.find_by_login(params[:org])
    else
      User.find_by_login(params[:user_id])
    end
    return :no_target_for_conditional_access unless org
    org
  end

  def current_repository
    @current_repository
  end
end
