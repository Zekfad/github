# frozen_string_literal: true

class SuccessorInvitationsController < ApplicationController
  areas_of_responsibility :community_and_safety

  before_action :login_required
  before_action :set_invitation_as_invitee, only: [:show_pending, :accept, :decline]
  before_action :sudo_filter, only: [:create, :revoke, :cancel]

  def create
    if GitHub.enterprise? || !GitHub.flipper[:account_succession].enabled?(current_user)
      return render_404
    end

    invitee = User.find_by_login(params[:login])
    if invitee.nil?
      flash[:error] = "Sorry, we couldn't send the invitation at this time."
      return redirect_to settings_user_admin_path
    end

    begin
      SuccessorInvitation.create!(inviter: current_user, invitee: invitee, target: current_user)
      flash[:notice] = "You have successfully sent the successor invitation to #{invitee.login}."
    rescue ActiveRecord::RecordInvalid
      flash[:error] = "Sorry, we couldn't send the invitation at this time."
    ensure
      redirect_to settings_user_admin_path
    end
  end

  def cancel
    invitation = current_user.successor_invitations.pending.last

    if invitation.nil?
      flash[:error] = "There was a problem canceling this invitation."
      return redirect_to settings_user_admin_path
    end

    begin
      invitation.cancel(actor: current_user)
      flash[:notice] = "You have canceled the invitation to #{invitation.invitee.login} to be your designated successor."
    rescue SuccessorInvitation::AlreadyCanceledError
      flash[:error] = "There was a problem canceling this invitation."
    ensure
      redirect_to settings_user_admin_path
    end
  end

  def revoke
    invitation = current_user.successor_invitations.accepted.last

    if invitation.nil?
      flash[:error] = "There was a problem revoking this invitation."
      return redirect_to settings_user_admin_path
    end

    begin
      invitation.revoke(actor: current_user)
      flash[:notice] = "You have revoked the successor invitation to #{invitation.invitee.login}."
    rescue SuccessorInvitation::AlreadyCanceledError, SuccessorInvitation::AlreadyAcceptedError
      flash[:error] = "There was a problem revoking this invitation."
    ensure
      redirect_to settings_user_admin_path
    end
  end

  # shown to invitee
  def show_pending
    if GitHub.enterprise?
      return render_404
    end

    unless @invitation&.acceptable_by?(current_user)
      return render "successor_invitations/not_found", status: 404
    end

    if already_actioned?(@invitation)
      return render "successor_invitations/already_actioned", locals: { invitation: @invitation }
    end

    render "successor_invitations/show_pending", locals: { invitation: @invitation }
  end

  # actioned by invitee
  def accept
    begin
      @invitation.accept(actor: current_user)
      flash[:notice] = "You are now the designated successor for #{@inviter.login}'s account."
      redirect_to dashboard_path
    rescue SuccessorInvitation::StateChangeError, SuccessorInvitation::ActorPermissionError
      flash[:error] = "There was a problem accepting this invitation."
      redirect_to pending_successor_invitation_path(user_id: @inviter.login)
    end
  end

  # actioned by invitee
  def decline
    begin
      @invitation.decline(actor: current_user)
      flash[:notice] = "You have declined to become the designated successor for #{@inviter.login}'s account."
      redirect_to dashboard_path
    rescue SuccessorInvitation::StateChangeError, SuccessorInvitation::ActorPermissionError
      flash[:error] = "There was a problem declining this invitation."
      redirect_to pending_successor_invitation_path(user_id: @inviter.login)
    end
  end

  def suggestions
    search_term = params[:q]

    headers["Cache-Control"] = "no-cache, no-store"

    suggested_users = User.search(search_term, limit: 10).select do |user|
      current_user.succeedable_by?(user)
    end

    respond_to do |format|
      format.html_fragment do
        render partial: "settings/successor/suggestions", formats: :html, locals: {
            suggestions: suggested_users,
        }
      end
      format.html do
        render partial: "settings/successor/suggestions",
               locals: { formats: :html, suggestions: suggested_users }
      end
    end
  end

  private

  def set_invitation_as_invitee
    set_inviter
    if current_user == @inviter
      redirect_to settings_user_admin_path
    end

    @invitation = SuccessorInvitation.where(inviter: @inviter, target: @inviter).last
  end

  def set_inviter
    return @inviter if defined?(@inviter)
    @inviter = User.find_by_login(params[:user_id])
  end

  def already_actioned?(invite)
    invite.accepted? || invite.declined?
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
