# frozen_string_literal: true

class PullRequestReviewsController < AbstractRepositoryController

  before_action :login_required
  before_action :load_current_pull_request

  def update
    review = @pull.reviews.find(params[:review_id])

    content =
      if operation = TaskListOperation.from(params[:task_list_operation])
        operation.call(params[:pull_request_review][:body])
      else
        params[:pull_request_review][:body]
      end

    if review && review.editable_by?(current_user)
      # prevent updates from stale data
      if stale_model?(review)
        return render_stale_error(model: review, error: "Could not edit review. Please try again.", path: pull_request_path(@pull, @pull.repository))
      end

      mark_thread_as_read(review)
      review.update_body(content, current_user)
    end

    respond_to do |format|
      format.json do
        if review.valid?
          render json: {
            "source" => review.body,
            "body" => review.body_html,
            "newBodyVersion" => review.body_version,
            "editUrl" => show_comment_edit_history_path(review.global_relay_id),
          }
        else
          render json: { errors: review.errors.full_messages }, status: :unprocessable_entity
        end
      end
      format.html do
        unless review.valid?
          flash[:error] = "Could not edit review."
        end

        redirect_to pull_request_path(@pull, @pull.repository)
      end
    end
  end

  private

  def load_current_pull_request
    @pull = current_repository.issues.find_by_number(params[:pull_id].to_i).try(:pull_request)
    return render_404 unless @pull
  end
end
