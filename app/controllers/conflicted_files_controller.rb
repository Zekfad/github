# frozen_string_literal: true

class ConflictedFilesController < AbstractRepositoryController
  areas_of_responsibility :pull_requests

  before_action :require_push_access

  def show
    respond_to do |format|
      format.json do
        conflicted_file_contents_entry, ancestor_entry, base_entry, head_entry =
          this_pull.conflicted_file_contents(
            CGI.unescape(params[:name]).b,
            params[:ancestor_oid],
            params[:base_oid],
            params[:head_oid],
          )

        lang = head_entry.language || ancestor_entry.language || base_entry.language
        conflicted_file_mime_type = lang ? lang.codemirror_mime_type : nil

        render json: {
          conflicted_file: { data: conflicted_file_contents_entry.data, codemirror_mime_type: conflicted_file_mime_type },
          ancestor: { data: ancestor_entry.data, codemirror_mime_type: ancestor_entry.language ? ancestor_entry.language.codemirror_mime_type : nil },
          base: { data: base_entry.data, codemirror_mime_type: base_entry.language ? base_entry.language.codemirror_mime_type : nil },
          head: { data: head_entry.data, codemirror_mime_type: head_entry.language ? head_entry.language.codemirror_mime_type : nil },
        }
      end
    end
  end

  def resolve
    file_params = params.to_unsafe_h[:files]
    return head(:bad_request) unless file_params.is_a?(Hash)

    cf = this_pull.conflicted_files
    if cf.nil?
      render json: { error: "someone pushed" }
      return
    end

    # we need the unescaped path to preserve line endings, but need to pass
    # the escaped path up through the job to avoid it being mangled by
    # resque/redis
    resolve_conflicts = {}
    cf.each do |file|
      path = CGI.escape(file)
      contents = file_params[path]
      resolve_conflicts[path] = this_pull.repository.preserve_line_endings(params[:pr_head_sha], file, contents) if contents
    end

    new_head_ref = nil
    if params[:commit_choice] == "quick-pull"
      new_head_ref = params[:target_branch]
    end

    status = JobStatus.new
    status.save
    CreatePullRequestResolvedMergeCommitJob.perform_later(status.id, this_pull.id, current_user.id, params[:pr_base_sha], params[:pr_head_sha], resolve_conflicts, new_head_ref)

    flash[:notice] = "Great, the merge conflict was resolved!"

    respond_to do |format|
      format.json do
        render json: { job: { url: job_status_url(status.id) } }
      end
    end
  end

  private

  def this_pull
    @this_pull ||= PullRequest.with_number_and_repo(params[:id].to_i, current_repository)
  end

  def require_push_access
    render_404 unless this_pull && this_pull.head_repository &&
      this_pull.head_repository.pushable_by?(current_user, ref: this_pull.head_ref_name)
  end
end
