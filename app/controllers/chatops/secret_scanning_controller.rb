# frozen_string_literal: true

require "chatops-controller"
require "github_chatops_extensions"
require "github_chatops_extensions/checks"

module Chatops
  class SecretScanningController < ApplicationController
    ALLOWED_ROOMS = ["#dsp-secret-scanning", "#dsp-secret-scanning-shield", "#dsp-secret-scanning-ops", "#incident-command"].freeze
    ALLOWED_TEAMS = ["org_teams/dsp-secret-scanning"].freeze
    ENTITLEMENTS_CONTROLLED_ACTIONS = [:check_enabled, :dry_run].freeze
    DUO_CONTROLLED_ACTIONS = [:rescan].freeze
    DRY_RUN_KEY = "dry_run.active"

    include ::Chatops::Controller
    include ::GitHubChatopsExtensions::Checks::Includable::Duo
    include ::GitHubChatopsExtensions::Checks::Includable::Entitlements
    include ::GitHubChatopsExtensions::Checks::Includable::Room

    before_action -> { require_in_room(ALLOWED_ROOMS) }, only: [ENTITLEMENTS_CONTROLLED_ACTIONS, DUO_CONTROLLED_ACTIONS]
    before_action -> { require_ldap_entitlement(ALLOWED_TEAMS) }, only: ENTITLEMENTS_CONTROLLED_ACTIONS
    before_action :require_duo_2fa, only: DUO_CONTROLLED_ACTIONS

    areas_of_responsibility :token_scanning

    skip_before_action :perform_conditional_access_checks
    skip_before_action :verify_authenticity_token

    chatops_help "Commands for working with Secret Scanning"
    chatops_namespace :secret_scanning
    chatops_error_response "An error occurred. More information is available [in sentry](https://sentry.io/organizations/github/issues/?query=is%3Aunresolved+app%3Ahubot)"

    def validate_repo(nwo_or_id)
      if /\A[0-9]+\Z/.match(nwo_or_id)
        Repository.exists?(nwo_or_id) && nwo_or_id
      else
        repo = Repository.nwo(nwo_or_id)
        repo.present? && repo.id
      end
    end

    chatop :check_enabled,
      /check_enabled (?<repo>.*)?/,
      "check_enabled [repo] - checks if secret scanning is enabled for the repo" do
        rpc_params = jsonrpc_params.permit(:repo)
        repo = rpc_params.fetch(:repo)
        validated_id = validate_repo(repo)
        if validated_id
          git_repo = Repository.find_by_id(validated_id)
          if git_repo && git_repo.token_scanning_enabled?
            chatop_send "Secret Scanning is ENABLED for repo #{repo}"
          else
            chatop_send "Secret Scanning is NOT ENABLED for repo #{repo}"
          end
        else # Repository does not exist
          chatop_send "Repository #{repo} does not exist"
        end
      end

    chatop :rescan,
      /rescan (?<repo>.*)?/,
      "rescan [repo] - rescans a repo, if secret scanning is enabled for it." do
        rpc_params = jsonrpc_params.permit(:repo)
        repo = rpc_params.fetch(:repo)
        validated_id = validate_repo(repo)
        if validated_id
          git_repo = Repository.find_by_id(validated_id)
          if git_repo && git_repo.token_scanning_enabled?
            RepositoryPrivateTokenScanningJob.perform_later(validated_id)
            chatop_send "Scan is QUEUED for repo #{repo}"
          else
            chatop_send "Secret Scanning is NOT ENABLED for repo #{repo}"
          end
        else # Repository does not exist
          chatop_send "Repository #{repo} does not exist"
        end
      end

      chatop :dry_run,
      /dry_run (?<repos>.*[^,]+)?/,
      "dry_run [repos] - performs dry run secret scans on a comma-separated list of repos, limited to a maximum of #{GitHub.configuration_secret_scanning_max_backfill_scans}. Results are not stored, and repo subscribers are not notified" do
        # Prevent queueing of multiple dry runs in quick succession.
        if GitHub.kv.exists(DRY_RUN_KEY).value!
          chatop_send "There is a dry run scan queued already. Please retry again in a few minutes."
          return
        end

        rpc_params = jsonrpc_params.permit(:repos)
        repos = rpc_params.fetch(:repos)
        repo_list = repos.split(/\s*,\s*/)

        # Enforce the limit for max backfill scans, since we're effectively queueing the same here.
        if repo_list.length > GitHub.configuration_secret_scanning_max_backfill_scans
          chatop_send "Maximum limit of #{GitHub.configuration_secret_scanning_max_backfill_scans} repos exceeded."
          return
        end

        dry_run_queued_count = 0
        not_found_repo_count = 0
        secret_scanning_disabled_count = 0

        GitHub.kv.set(DRY_RUN_KEY, "true", expires: 2.minutes.from_now)

        repo_list.each do |repo|
          validated_id = validate_repo(repo)
          if validated_id
            git_repo = Repository.find_by_id(validated_id)
            if git_repo&.token_scanning_enabled?
              RepositoryPrivateTokenScanningDryRunJob.perform_later(validated_id)
              dry_run_queued_count += 1
            else
              secret_scanning_disabled_count += 1
            end
          else # Repo not found
            not_found_repo_count += 1
          end
        end

        chatops_message = "Dry run of secret scan queued on #{dry_run_queued_count} repos."
        chatops_message += "\nSecret scanning is not enabled on #{secret_scanning_disabled_count} repos. " unless secret_scanning_disabled_count.zero?
        chatops_message += "\nRepos not found: #{not_found_repo_count}." unless not_found_repo_count.zero?

        chatop_send chatops_message
      end
  end
end
