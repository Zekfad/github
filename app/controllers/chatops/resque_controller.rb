# frozen_string_literal: true

require "chatops-controller"
require "github_chatops_extensions"
require "github_chatops_extensions/checks"
require "terminal-table"

module Chatops
  class ResqueController < ApplicationController

    ALLOWED_ROOMS = ["#dotcom-ops", "#incident-command"].freeze

    QUEUED_JOBS_LIMIT = 100

    include ::Chatops::Controller
    include ::GitHubChatopsExtensions::Checks::Includable::Duo
    include ::GitHubChatopsExtensions::Checks::Includable::Entitlements
    include ::GitHubChatopsExtensions::Checks::Includable::Room

    CONTROLLED_ACTIONS = [
      :pause,
      :pause_site,
      :resume,
      :resume_site,
    ].freeze

    before_action -> { require_in_room(ALLOWED_ROOMS) }, only: CONTROLLED_ACTIONS
    before_action :require_duo_2fa, only: CONTROLLED_ACTIONS

    skip_before_action :perform_conditional_access_checks
    skip_before_action :verify_authenticity_token

    areas_of_responsibility :background_jobs

    chatops_namespace :resque
    chatops_help "Manage queue processing for Resque."
    chatops_error_response "More information is available in Haystack at https://haystack.githubapp.com/github?q=url%3Ahttps%3A%2F%2Fchatops.github.com%2F_chatops%2Fresque"

    chatop :queues,
           /queues(?:\s+(?<environment>\S+))?/,
           "queues <environment> - Show the number of jobs backlogged in each queue." do
      params = jsonrpc_params.permit(:environment)

      the_queues = case params[:environment]
        when "lab" then lab_queues
        when "staff" then staff_queues
        when "garage" then garage_queues
        when "backup" then backup_queues
        when "fs" then fs_queues
        when "dfs" then dfs_queues
        when "production" then production_queues
        when "all" then resque_queues
        else
          worker_queues
      end

      depths = the_queues.map do |queue|
        [queue, Resque.size(queue), queue_status(queue)]
      end

      table = Terminal::Table.new(headings: %w(Queue Depth Status)) do |t|
        depths.sort_by { |queue, depth, status| [-depth, queue, status] }.each do |queue|
          t.add_row(queue)
        end
      end
      table.align_column 1, :right

      chatop_send "```\n#{table}\n```"
    end

    chatop :roles,
           /roles\s*?/,
           "roles - Show the available roles and their worker summaries." do
      role_workers = Hash.new(0)
      role_working = Hash.new(0)
      resque_workers.each do |worker|
        host = worker.to_s.split(":").first
        role = host.split("/")[1] || "none"
        role_workers[role] += 1
        if resque_working.key? worker.to_s
          role_working[role] += 1
        end
      end

      rows = role_workers.map do |role, count|
        [role, count, role_working[role]]
      end

      headings = ["Role", "Workers", "Working", "Available", "% Free"]
      table = Terminal::Table.new(headings: headings) do |t|
        rows.sort.each do |role, workers, working|
          percent = (workers > 0 ? "%0.2f" % ((workers - working).to_f / working * 100) : 0)
          t.add_row [role, workers, working, workers - working, percent]
        end
      end
      [1, 2, 3, 4].each { |col| table.align_column col, :right }

      chatop_send "```\n#{table}\n```"
    end

    chatop :role,
           /role(?:\s+(?<role>\S+))?/,
           "role <role> - Show queues and worker statistics for a role." do
      param_role = jsonrpc_params.require(:role)

      queue_counts = Hash.new(0)
      queue_working = Hash.new(0)
      queue_available = Hash.new(0)
      queue_depths = {}
      resque_workers.each do |worker|
        host, _ = worker.split(":")
        _, role = host.split("/")
        role ||= "none"
        next unless role == param_role

        if job = resque_working[worker.to_s]
          working = true
          queue_working[job["queue"]] += 1
        else
          resque_working = false
        end

        queues_for_worker(worker).each do |queue|
          queue_counts[queue] += 1
          # if this worker is busy on one queue, the worker is *not*
          # available for any other queue.
          queue_available[queue] += 1 unless working
          queue_depths[queue] ||= Resque.size(queue)
        end
      end

      headings = ["Queue", "Depth", "Workers", "Working", "Available", "% Free"]
      table = Terminal::Table.new(headings: headings) do |t|
        queue_counts.sort_by { |queue, count| [-count, queue] }.each do |queue, count|
          working = queue_working[queue]
          available = queue_available[queue]
          percent = "%0.2f" % (count > 0 ? (available.to_f / count * 100) : 0)
          t.add_row [queue, queue_depths[queue], count, working, available, percent]
        end
      end
      [1, 2, 3, 4, 5].each { |col| table.align_column col, :right }

      chatop_send "```\n#{table}\n```"
    end

    chatop :queued,
           /queued(?:\s+(?<queue>\S+))?/,
           "queued <queue> - Show the #{QUEUED_JOBS_LIMIT} oldest queued jobs in the specified queue." do
      queue_name = jsonrpc_params.require(:queue)

      if resque_queues.detect { |q| q == queue_name }
        table = Terminal::Table.new(headings: ["Job", "Args", "Queued at"]) do |t|
          queued_jobs(queue_name, QUEUED_JOBS_LIMIT).each do |job|
            job_class = job["class"].gsub("GitHub::Jobs::", "")
            if job["class"] =~ /JobWrapper/
              activejob = true
              job_class = (job["args"].first || {}).delete("job_class")
            end
            job_args  = job["args"].inspect
            queued_at = "-"
            if job["meta"] && job["meta"]["queued_at"]
              queued_at = Time.at(job["meta"]["queued_at"])
            end

            t.add_row([job_class, job_args, queued_at])
          end
        end

        chatop_send "100 oldest jobs in the '#{queue_name}' queue:\n```\n#{table}\n```"
      else
        chatop_send "Couldn't find the '#{queue_name}' queue."
      end
    end

    chatop :workers,
           /workers(?:\s+(?<pattern>\S+))?/,
           "workers <pattern> - Show the available workers matching the given pattern(s).
                                Pattern can be hostname, role, or queue name.
                                Pattern can also be key=value for an exact
                                match, where key is \"role\", \"queue\", or \"hostname\"" do
      params = jsonrpc_params.permit(:pattern)
      if params[:pattern].present?
        pattern = params[:pattern].split(/\s+/)
      end

      headings = %W(Host Role PID Busy Queues)
      rows = []

      resque_workers.each do |worker|

        host, pid = worker.to_s.split(":")
        queues = queues_for_worker(worker)
        host, role = host.split("/")
        role ||= "none"

        busy = resque_working.key? worker.to_s

        if pattern && pattern.any?
          matched = pattern.all? do |search|
            if search.include?("=")
              key, value = search.split("=")
              case key
              when "role"
                role == value
              when "host"
                host == value
              when "queue"
                queues.any? { |queue| queue == value }
              else
                abort "unknown pattern key `#{key}` in `#{search}`"
              end
            else
              match_pattern = /\A#{search}/
              queues.any? { |queue| queue =~ match_pattern } ||
              [host, role].any? { |item| item =~ match_pattern }
            end
          end
          next unless matched
        end

        rows << [host, role, pid, busy, queues.join(", ")]
      end

      table = Terminal::Table.new(headings: headings) do |t|
        rows.sort_by { |row| [row[0], row[1.to_i]] }.each do |row|
          t.add_row row
        end
      end

      chatop_send "```\n#{table}\n```"
    end

    chatop :working,
           /working\s*(?<pattern>\S+(?:\s+\S+)*)?\s*?/,
           "working <pattern> - Show all jobs matching the given pattern(s).
                                Pattern can be:
                                  - hostname
                                  - role
                                  - job class
                                  - queue name
                                Pattern can also be key=value for an exact
                                match, where key is \"role\", \"queue\", \"hostname\",
                                or \"job_class\".
                                Specify `--args` to see job arguments." do
      params = jsonrpc_params.permit(:pattern)
      if params[:pattern].present?
        pattern = params[:pattern].split(/\s+/)
        include_args = pattern.reject! { |p| p == "--args" }
      end

      headings = %W(Host Role PID Queue TTime QTime ETime Job)
      if include_args
        headings << "Args"
      end

      rows = []

      resque_working.each do |id, worker|
        json = worker.job
        next if json.empty?
        payload = json["payload"]
        meta = payload["meta"]
        queue = json["queue"]

        host, pid = worker.to_s.split(":")
        host, role = host.split("/")
        role ||= "none"

        job = payload["class"]
        activejob = false
        if job =~ /JobWrapper/
          activejob = true
          job_class = (payload["args"].first || {}).fetch("job_class", job)
          job = job_class # in case the arg parsing didn't find what it expected
        end
        job.gsub!("GitHub::Jobs::", "")

        # if we're looking for a specific queue, job, host, or role, filter out all others
        if pattern && pattern.any?
          matched = pattern.all? do |search|
            if search.include?("=")
              key, value = search.split("=")
              case key
              when "role"
                role == value
              when "job"
                job == value
              when "host"
                host == value
              when "queue"
                queue == value
              else
                abort "unknown pattern key `#{key}` in `#{search}`"
              end
            else
              match_pattern = /\A#{search}/
              [host, job, queue, role].any? { |item| item =~ match_pattern }
            end
          end
          next unless matched
        end

        # Do all this based on whole seconds as `run_at` does not
        # track any finer granularity
        runat = Time.iso8601(json["run_at"]).utc rescue Time.now.utc
        etime = Time.at(Time.now.to_i).utc - runat
        qtime = runat - Time.at(meta["queued_at"].to_i) if meta && meta["queued_at"]
        args = Yajl.dump(payload["args"])

        qtime = qtime.nil? ? 0 : qtime
        etime = etime.nil? ? 0 : etime

        job_with_annotation = activejob ? job + " (ActiveJob)" : job

        row = [
          host,
          role,
          pid,
          queue,
          qtime + etime,
          qtime,
          etime,
          job_with_annotation,
        ]

        row << args if include_args
        rows << row
      end

      table = Terminal::Table.new(headings: headings) do |t|
        rows.sort_by { |r| -r[4] }.each do |row|
          [4, 5, 6].each do |i|
            row[i] = "#{row[i]}s"
          end

          t.add_row row
        end
      end
      chatop_send "```\n#{table}\n```"
    end

    chatop :unworked,
           /unworked\s*?/,
           "unworked - Show queues with no workers listening or processing their jobs." do
      worked = Set.new
      resque_workers.each do |worker|
        queues_for_worker(worker).each { |queue| worked.add queue }
      end
      unworked = Set.new(resque_queues) - worked
      depths = unworked.map { |queue| [queue, Resque.size(queue)] }

      table = Terminal::Table.new(headings: %w(Queue Depth)) do |t|
        depths.sort_by { |queue, depth| [-depth, queue] }.each do |queue|
          t.add_row(queue)
        end
      end
      table.align_column 1, :right
      chatop_send "```\n#{table}\n```"
    end

    chatop :stats,
           /stats\s*?/,
           "stats - Show basic resque stats." do
      headings = %w(Pending Processed Queues Workers Working Failed)

      table = Terminal::Table.new(headings: headings) do |t|
        t.add_row [
          resque_queues.inject(0) { |m, k| m + Resque.size(k) },
          stat("processed"),
          resque_queues.size,
          resque_workers.size,
          resque_working.size,
          stat("failed"),
        ]
      end
      chatop_send "```\n#{table}\n```"
    end

    chatop :pause,
           /pause(?:\s+(?<queue>\S+))?/,
           "pause <queue> - Pause workers from pulling new jobs from the specified queue." do
      queue = jsonrpc_params.require(:queue)

      if validate_queue_name(queue)
        GitHub.resque_pauser.pause_queue(queue)
        chatop_send "Paused work on '#{queue}' queue."
      else
        chatop_send "Couldn't find the '#{queue}' queue."
      end
    end

    chatop :resume,
           /resume(?:\s+(?<queue>\S+))?/,
           "resume <queue> - Allow workers to pull new jobs from the specified queue." do
      queue = jsonrpc_params.require(:queue)

      if validate_queue_name(queue)
        GitHub.resque_pauser.resume_queue(queue)
        chatop_send "Resumed work on '#{queue}' queue."
      else
        chatop_send "Couldn't find the '#{queue}' queue."
      end
    end

    chatop :paused,
           /paused(?:\s+(?<queue>\S+))?/,
           "paused <queue> - Show if the given queue is paused or not." do
      queue = jsonrpc_params.require(:queue)

      if validate_queue_name(queue)
        chatop_send "Queue '#{queue}' state: #{queue_status(queue)}"
      else
        chatop_send "Couldn't find the '#{queue}' queue."
      end
    end

    chatop :pause_site,
           /pause_site(?:\s+(?<site>\S+))?/,
           "pause_site <site> - Pause workers in a specific site from processing new jobs." do
      site = jsonrpc_params.require(:site)

      GitHub.resque_pauser.pause_site(site)
      chatop_send "Paused work in '#{site}' site."
    end

    chatop :resume_site,
           /resume_site(?:\s+(?<site>\S+))?/,
           "resume_site <site> - Allow workers in a specific site to process new jobs." do
      site = jsonrpc_params.require(:site)

      GitHub.resque_pauser.resume_site(site)
      chatop_send "Resumed work in '#{site}' site."
    end

    chatop :paused_site,
           /paused_site(?:\s+(?<site>\S+))?/,
           "paused_site <site> - Show if the given site is paused or not." do
      site = jsonrpc_params.require(:site)
      chatop_send "Site '#{site}' state: #{site_status(site)}"
    end

    private

    def help_args?(args)
      return false if args.nil?
      args.any? { |arg| ["-help", "-h"].include?(arg) } || args.empty?
    end

    def resque_queues
      @resque_queues ||= Resque.queues
    end

    def staff_queues
      @staff_queues ||= resque_queues.select do |queue|
        queue =~ /staff/
      end
    end

    def lab_queues
      @lab_queues ||= resque_queues.select do |queue|
        queue.start_with("lab_")
      end
    end

    def garage_queues
      @garage_room_queues ||= resque_queues.select do |queue|
        queue =~ /garage/
      end
    end

    def production_queues
      @production_queues ||= resque_queues.select do |queue|
        queue !~ /garage/ &&
        queue !~ /staff/ &&
        !queue.start_with?("lab_") &&
        !queue.start_with?("maint_")
      end
    end

    def worker_queues
      @worker_queues ||= production_queues.select do |queue|
        queue !~ /[_-]fs\d+/ && \
          queue !~ /backup/
      end
    end

    def fs_queues
      @fs_queues ||= production_queues.select do |queue|
        queue =~ /[_-]fs\d+/
      end
    end

    def dfs_queues
      @dfs_queues ||= production_queues.select do |queue|
        queue =~ /[_-]dfs-?[0-9a-f]+/
      end
    end

    def backup_queues
      @backup_queues ||= production_queues.select do |queue|
        queue =~ /backup/
      end
    end

    def queue_status(queue)
      GitHub.resque_pauser.paused_queue?(queue) ? "paused" : "running"
    end

    def site_status(site)
      GitHub.resque_pauser.paused_site?(site) ? "paused" : "running"
    end

    def resque_working
      @resque_working ||= begin
        working = {}
        Resque::Worker.working.each do |worker|
          working[worker.to_s] = worker
        end
        working
      end
    end

    def resque_workers
      @resque_workers ||= Resque::Worker.all
    end

    def queues_for_worker(worker)
      queues = Resque.redis.get("worker:#{worker}:queues") || ""
      queues.split(",")
    end

    def queued_jobs(queue, limit)
      Resque.peek(queue, 0, limit)
    end

    def stat(name)
      Resque::Stat.get(name)
    end

    def validate_queue_name(queue)
      resque_queues.include?(queue)
    end
  end
end
