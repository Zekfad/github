# frozen_string_literal: true
class SignupController < ApplicationController
  areas_of_responsibility :enterprise_only, :user_growth

  include AnalyticsHelper
  include BillingSettingsHelper
  include SignupHelper
  include SuggestedUsernamesHelper

  layout "site"

  # This controller does not access protected organization resources.
  skip_before_action :perform_conditional_access_checks
  before_action :set_return_to
  before_action :set_skip_payment, only: [:join]
  before_action :clear_weak_password_session_variable, only: :join
  before_action :login_required, except: [:join, :create_account, :username_check, :email_check, :enterprise_trial_redirect]
  before_action :anon_required,  only: [:join, :create_account]
  before_action :set_plan,  only: [:process_plan_selection, :select_plan, :plan, :billing]
  before_action :add_csp_exceptions, only: [:join, :create_account]

  CSP_EXCEPTIONS = {
    frame_src: [GitHub.urls.octocaptcha_host_name],
  }

  after_action  :update_login_metadata, only: [:create_account]

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:username_check, :email_check],
    key: :signup_check_rate_limit_key,
    max: :signup_check_rate_limit_max,
    ttl: :signup_check_rate_limit_ttl,
    at_limit: :record_username_check_limit

  if enterprise?
    skip_before_action :first_run_check, only: %w( join create_account username_check email_check )
    before_action :signup_enabled?,               only: [:join, :create_account]
  end

  # shows the signup form.
  def join
    @user = User.new

    octocaptcha = Octocaptcha.new(session)
    octocaptcha.set_test_group("join")
    octocaptcha.instrument_event("signup_started")

    repo = Repository.nwo(params[:source_repo]) if params[:source_repo].present?

    if repo.present? && repo.public?
      branch = params[:branch]
      @return_to = session[:return_to] = repository_path(repo, branch)

      if params_from_downloading_repo?
        filename = filename_for_zip_download(repo, branch)
        flash.now[:notice] = "Downloading #{filename}"
      end
    end

    return render_signup_view
  end

  def get_started
    render "signup/get_started"
  end

  def enterprise_trial_redirect
    if logged_in?
      redirect_to new_organization_path(with_referral_params(plan: GitHub::Plan.business_plus))
    else
      redirect_to signup_path(with_referral_params(plan: GitHub::Plan.business_plus, setup_organization: true))
    end
  end

  # creates a new account.
  def create_account
    redirect_to "/login" if GitHub.auth.external?
    octocaptcha = Octocaptcha.new(session, params["octocaptcha-token"])
    octocaptcha.set_test_group(params[:source])
    octocaptcha.instrument_event("signup_started")
    octocaptcha.verify

    analytics_event(category: "Octocaptcha-Signup", action: "Attempt", label: referral_labels("source:#{params[:source]};")) if octocaptcha.show_captcha?

    signup_data = nil
    private_session_id = session.id&.private_id
    signup_data_key = "UserSignupData:#{private_session_id}"

    if params[:user]
      signup_data = computed_signup_params
      @user = User.new(signup_data[:user_hash])
      octocaptcha.instrument_event("signup_attempt",
                                   signup_time: signup_data.dig(:spamurai_signup_signals, :join_to_create_account_distance_in_milliseconds),
                                   email_address: signup_data[:user_hash][:email],
                                  )

      if blacklisted?(@user)
        GitHub.dogstats.increment("user", tags: ["action:create", "error:blacklisted"])
        return render_signup_view
      end

      if password_confirmation_missing?(@user)
        @user.errors.add(:password, "confirmation can't be blank")
        return render_signup_view
      end

      if !@user.valid?
        flash.now[::CompromisedPassword::WEAK_PASSWORD_KEY] = @user.provided_weak_password?

        analytics_event(category: "Octocaptcha-Signup", action: "Failure", label: referral_labels("source:#{params[:source]};")) if octocaptcha.show_captcha?
        return render_failed_user_signup(@user, signup_data.slice(:sso_organization, :invitation_token, :source))
      end

      signup_data[:user_hash].tap do |u|
        u[:bcrypt_auth_token] = User.encrypt_with_bcrypt(u[:password])
        u[:salt] = User::BcryptSalt
        u.delete(:password)
        u.delete(:password_confirmation)
      end

      GitHub.kv.set(signup_data_key, signup_data.to_json, expires: 10.minutes.from_now)
    else
      data = GitHub.kv.get(signup_data_key).value { nil }
      if data
        signup_data = JSON.parse(data).deep_symbolize_keys
        octocaptcha.instrument_event("signup_attempt",
                                     signup_time: signup_data.dig(:spamurai_signup_signals, :join_to_create_account_distance_in_milliseconds),
                                     email_address: signup_data[:user_hash][:email],
                                    )
      end
    end

    if signup_data.blank?
      return render_signup_view
    end

    if !octocaptcha.solved?
      @user = User.new
      @hide_signup_form = true

      if params[:error_loading_captcha]
        Failbot.report(
          Octocaptcha::UnableToLoadCaptcha.new,
          app: "octocaptcha-errors",
          request_id: GitHub.context[:request_id],
          user_agent: request.user_agent.to_s,
        )
        flash.now[:error] = "Unable to verify your captcha response. " \
          "Please visit #{GitHub.help_url}/articles/troubleshooting-connectivity-problems/#troubleshooting-the-captcha for troubleshooting information."
      end

      return render_signup_view
    end

    GitHub.context.push(visitor_id: current_visitor.id)
    GitHub.context.push(ga_id: params[:ga_id])
    GitHub.context.push(spamurai_form_signals: signup_data[:spamurai_form_signals])
    GitHub.context.push(funcaptcha_session_id: octocaptcha.funcaptcha_session_id)
    GitHub.context.push(funcaptcha_solved: octocaptcha.funcaptcha_solved)
    GitHub.context.push(funcaptcha_response: octocaptcha.funcaptcha_response)

    @user = User.new(signup_data[:user_hash])
    @user.solved_interactive_captcha = octocaptcha.solved_interactive_captcha?

    # Allow staff users to create test accounts here without getting marked as spammy
    @user.mark_as_temporarily_exempt_from_spam_checks if user_exempt_from_spam_checks?

    if @user.save
      GitHub.kv.del(signup_data_key)

      octocaptcha.instrument_event("signup_success",
                                   signup_time: signup_data.dig(:spamurai_signup_signals, :join_to_create_account_distance_in_milliseconds),
                                   user: @user.reload,
                                   email_address: signup_data[:user_hash][:email],
                                  )

      @user.queue_signup_tasks

      if suggested_username_used?(username: @user.login, suggested_usernames: params[:suggested_usernames])
        publish_suggested_username_event(
          user_id: @user.id,
          login: @user.login,
          suggestion_count: params[:suggested_usernames].count,
          action: SuggestedUsernamesHelper::EVENT_ACTIONS[:used],
        )
      end

      if !GitHub.enterprise?
        primary_email = @user.existing_primary_email_role.email
        primary_email.toggle_visibility if primary_email.public?
      end

      @user.accept_tos
      @user.reload

      current_device = if @user.sign_in_analysis_enabled?
        # approve devices on new accounts by default
        @user.authenticated_devices.create!(
          accessed_at: Time.now,
          approved_at: Time.now,
          device_id: current_device_id,
          display_name: AuthenticatedDevice.generated_display_name(parsed_useragent),
        )
      end

      login_user @user, authenticated_device: current_device, sign_in_verification_method: :verified_device, client: :sign_up
      analytics_event category: "Sign up", action: "Success", label: referral_labels("source:#{params[:source]};"), redirect: true

      set_email_preference

      if octocaptcha.show_captcha?
        # If we render a page, then the first Attempt event in the beginning of the controller will fire
        # If we request redirect then this event will fire
        analytics_event(category: "Octocaptcha-Signup", action: "Attempt", label: referral_labels("source:#{params[:source]};"), redirect: true)
        analytics_event(category: "Octocaptcha-Signup", action: "Success", label: referral_labels("source:#{params[:source]};"), redirect: true)
      end

      set_analytics_dimension(
        name: GoogleAnalytics::Dimensions::HAS_ACCOUNT,
        value: "Signed Up",
        redirect: true,
      )

      if signup_data[:redeeming_coupon]
        redirect_to_return_to(fallback: signup_plan_path)
        return
      end

      # User signed up after going through the SSO flow for an organization.
      # Bounce the user back back to sign up action to link the identity.
      if signup_data[:sso_organization]
        redirect_to org_idm_sso_sign_up_path(signup_data[:sso_organization])
        return
      end

      # User signed up after going throug the SSO flow for a business.
      # Bounce the user back back to sign up action to link the identity.
      if signup_data[:sso_business]
        redirect_to business_idm_sso_sign_up_enterprise_path(signup_data[:sso_business])
        return
      end

      # If this request originated from an organization invitation via email,
      # accept the invitation and redirect to the organization profile page
      #
      # If organization has enabled the two factor requirement, don't accept
      # the invitation and instead redirect back to the invitation so that
      # the user can enable 2fa and join the organization.
      #
      # Since the user clicked "Join" to get to this point, accepting is
      # continuing the intent of the user.
      if (pending_invitation = get_pending_invitation(signup_data[:invitation_token]))
        if !pending_invitation.organization.two_factor_requirement_met_by?(@user)
          # Add the user to this invite to allow them to accept later in the flow
          pending_invitation.update_attribute(:invitee_id, @user.id)

          redirect_to org_show_invitation_path(pending_invitation.organization,
                                               invitation_token: signup_data[:invitation_token],
                                              )
          return
        end

        pending_invitation.accept(acceptor: @user)
        GitHub.dogstats.increment("organization", tags: ["subject:invitations_by_email", "action:signup"])

        flash[:notice] =
          case pending_invitation.role
          when :direct_member
            "You are now a member of #{pending_invitation.organization.safe_profile_name}!"
          when :admin
            "You are now an owner of #{pending_invitation.organization.safe_profile_name}!"
          end

        redirect_to user_url(pending_invitation.organization)
        return
      end

      # If this request originated from an repository invitation via email,
      # accept the invitation and redirect to the repository invitation page
      if (pending_invitation = get_pending_repo_invitation(signup_data[:repo_invitation_token]))
        # Add the user to this invite to allow them to accept later in the flow
        pending_invitation.update!(invitee_id: @user.id, email: nil, hashed_token: nil)
        redirect_to pending_invitation.permalink
        return
      end

      if GitHub.billing_enabled?
        redirect_after_signup_completed(plan: signup_data[:plan], plan_selected: signup_data[:plan].present?) #skip plan selection
      else
        if signup_data[:setup_organization] && signup_data[:plan]
          redirect_to new_organization_path(plan: signup_data[:plan])
        else
          redirect_to_return_to(fallback: "/")
        end
      end
    else
      analytics_event(category: "Octocaptcha-Signup", action: "Failure", label: referral_labels("source:#{params[:source]};")) if octocaptcha.show_captcha?
      render_failed_user_signup(@user, signup_data.slice(:sso_organization, :invitation_token, :source))
    end
  end

  def render_failed_user_signup(user, signup_data)
    user.password = nil
    user.password_confirmation = nil
    GitHub.dogstats.increment("user", tags: ["action:create", "error:invalid"])
    analytics_event category: "Sign up", action: "Failure", label: referral_labels("source:#{params[:source]};")

    # If this request originated from an organization SSO flow,
    # render the organization SSO signup page.
    if signup_data[:sso_organization]
      render_template_view "orgs/identity_management/sign_up_via_sso",
        Orgs::IdentityManagement::SingleSignOnView,
        view_attrs = { organization: Organization.find_by_login(params[:sso_organization]) },
        layout: "session_authentication"
      return
    end
    # If this request originated from an organization invitation via email,
    # render the organization invitation signup page.
    if (pending_invitation = get_pending_invitation(signup_data[:invitation_token]))
      render_template_view "orgs/invitations/sign_up_via_invitation",
        Orgs::Invitations::ShowPendingPageView,
        { invitation: pending_invitation },
        layout: "session_authentication"
      return
    end

    render_signup_view
  end

  # renders the form.
  def plan
    return render_404 unless GitHub.billing_enabled?

    if params[:setup_organization]
      redirect_to org_plan_path(plan: params[:plan])
    elsif @plan.pro?
      redirect_to signup_billing_path(plan: params[:plan])
    else
      render "signup/plan", locals: { plan: @plan }.merge!(pricing_models)
    end
  end

  # Used by the redesign to redirect users to the billing page if they chose a paid plan.
  # Otherwise we process their selection as usual.
  def select_plan
    if [GitHub::Plan::TEAM_FREE_PLAN_NAME, GitHub::Plan.business.name, GitHub::Plan.business_plus.name].include?(params[:plan])
      redirect_to new_organization_path(with_referral_params(plan: @plan.name))
    elsif @plan.pro?
      redirect_to signup_billing_path(plan: "pro")
    else
      process_plan_selection
    end
  end

  # After you create an account, you choose a plan (or keep it free).
  # POST solidifies your plan decision.
  def process_plan_selection
    GitHub.dogstats.increment("user.plan_selection", tags: ["action:attempt", "plan:#{@plan.name}"])
    if @plan.name == "free"
      current_user.save
      flash[:analytics_location_params] = { target: "User", billing: false, plan: @plan.name }
      GitHub.dogstats.increment("user.plan_selection", tags: ["action:success", "plan:#{@plan.name}"])
      return redirect_after_signup_completed
    end

    if current_user.has_any_trade_restrictions?
      flash[:error] = sanctioned_by_ofac_message
      GitHub.dogstats.increment("user.plan_selection", tags: ["action:rejected", "plan:#{@plan.name}"])
      return redirect_to signup_plan_path
    end

    if no_payment_details?
      flash[:error] = "Missing billing information. " \
        "Make sure you’re using a supported browser.  Please visit #{GitHub.help_url}/articles/supported-browsers"
      GitHub.dogstats.increment("user.plan_selection", tags: ["action:rejected", "plan:#{@plan.name}"])
      return redirect_to signup_billing_path
    end

    # Let's do this

    target = current_user

    context = {
      note: "Added #{target.friendly_payment_method_name} during signup",
      actor: current_user,
      user: current_user,
      elected_to_receive_marketing_email: NewsletterPreference.marketing?(user: current_user),
    }

    GitHub.context.push(context)
    Audit.context.push(context)

    begin
      result = GitHub::Billing.signup(target, @plan.name, actor: current_user, payment_details: payment_details)
      if result.success?
        publish_billing_plan_changed_for(
          actor: current_user,
          user: target,
          new_seat_count: result.record.seats,
          new_plan_name: @plan.name,
          user_first_name: params[:user_first_name],
          user_last_name: params[:user_last_name],
        )

        if target.payment_method
          publish_payment_method_changed_for(
            actor: current_user,
            account: target,
            payment_method: target.payment_method,
          )
        end

      end
    rescue GitHub::Billing::CustomerAlreadyExistsError
      GitHub.dogstats.increment("user.plan_selection", tags: ["action:rejected", "plan:#{@plan.name}"])
      return redirect_after_signup_completed
    end

    if result.success?
      GitHub.dogstats.increment("user.plan_selection", tags: ["action:success", "plan:#{@plan.name}"])
      analytics_ec_purchase(current_user, target.subscription.discounted_price, "Signup")
      flash[:analytics_location_params] = { target: "User", billing: false, plan: @plan.name, action: "upgrade" }
      flash[:notice] = "Thanks so much for deciding to become a paying customer!"
      redirect_after_signup_completed
    else
      GitHub.dogstats.increment("user.plan_selection", tags: ["action:failed", "plan:#{@plan.name}"])
      context = {
        error: result.error_message.to_s,
        metadata: result.error_message.try(:metadata),
      }

      GitHub.context.push(context)
      Audit.context.push(context)

      analytics_event category: "User", action: "plan onboard failure", redirect: false
      flash.now[:error] = result.error_message.to_s

      render "signup/plan", locals: { plan: @plan }.merge!(pricing_models)
    end
  end

  def billing
    if current_user.has_any_trade_restrictions?
      redirect_to(signup_plan_path)
    else
      render "signup/billing", locals: { plan: @plan }
    end
  end

  def zuora_payment_page_signature
    page_id = GitHub.zuora_self_serve_join_payment_page_id

    page_params = ::Billing::Zuora::HostedPaymentsPage.params(
      page_id:  page_id,
      target: current_user,
    )

    render json: page_params
  end

  # Inline validation check
  def username_check
    Failbot.push(user: params[:value])
    user = User.new(login: params[:value])
    user.valid? # call the validations

    actor = params[:value]

    respond_to do |format|
      format.html_fragment do
        if user.errors[:login].empty?
          render html: "#{user.login} is available."
        else
          suggest_names = suggest_usernames?(login_errors: user.errors[:login], suggest_usernames_param: params[:suggest_usernames])

          if suggest_names
            suggested_usernames = get_username_suggestions(base_username: params[:value])
          end

          render partial: "signup/invalid_username", formats: :html, status: 422, locals: {
            error_message:  user.login_error_message,
            suggested_usernames: suggested_usernames,
            submitted_username: params[:value],
            source: params[:source],
          }
        end
      end
    end
  end

  # Inline validation check
  def email_check
    respond_to do |format|
      format.html_fragment do
        user = User.new(email: params[:value])
        return head :ok unless !user.valid? && user.errors[:emails].any?

        render body: "Email is invalid or already taken", status: 422, content_type: "text/fragment+html"
      end
    end
  end

  private

  def computed_signup_params
    root_params.tap do |p|
      p[:user_hash] = user_params
    end
  end

  def user_params
    params[:user].permit(:login,
      :email,
      :password,
      :password_confirmation,
      :public_keys,
      :coupon,
    ).merge(extra_user_params)
  end

  def extra_user_params
    {
      referral_code: (cookies["tracker"] || "direct"),
      last_ip: request.remote_ip,
      gh_role: ("staff" if enterprise? && enterprise_first_run?),
    }
  end

  def root_params
    params.slice(:sso_organization,
      :invitation_token,
      :repo_invitation_token,
      :redeeming_coupon,
      :setup_organization,
      :plan,
      :source,
      :timestamp,
      :timestamp_secret,
      "octocaptcha-token",
      :return_to,
      :suggested_usernames,
      :enterprise_plan,
      "free-trial",
    ).merge(metadata_params)
  end

  def metadata_params
    {
      spamurai_signup_signals: {
        join_to_create_account_distance_in_milliseconds: spamurai_form_signals.load_to_submit_in_milliseconds,
      },
      spamurai_form_signals: spamurai_form_signals,
    }
  end

  def set_plan
    @plan = GitHub::Plan.find(params[:plan]) || GitHub::Plan.default_plan
  end

  def signup_enabled?
    unless GitHub.signup_enabled? || GitHub.enterprise_first_run?
      redirect_to(login_path)
    end
  end

  def render_signup_view
    render "signup/join", locals: { setup_organization: params[:setup_organization], plan: params[:plan] }
  end

  def blacklisted?(user)
    return if !GitHub.spamminess_check_enabled? || Rails.development?
    Spam.ip_is_blacklisted?(user.last_ip)
  end

  def password_confirmation_missing?(user)
    return unless GitHub.password_confirmation_required?
    user.password_confirmation.blank?
  end

  # Where do you go after you are done with sign up?
  def redirect_after_signup_completed(plan: nil, plan_selected: true)
    analytics_event category: "User", action: "plan onboard", label: referral_labels("plan:#{current_user.plan.display_name};"), redirect: true if plan_selected

    if params[:setup_organization] && current_user.spammy?
      flash[:error] = "Something went wrong. Please contact support."
      redirect_to "/support"
    elsif params[:setup_organization]
      redirect_to new_organization_path(with_referral_params(plan: plan))
    elsif session[:return_to]&.include?("oauth")
      redirect_to_return_to(fallback: "/")
    elsif mobile?
      redirect_to "/"
    else
      redirect_to signup_customize_path
    end
  end

  def signup_check_rate_limit_key
    case action_name
    when "username_check"
      "signup_controller.username_check:#{request.remote_ip}"
    when "email_check"
      "email-check:#{request.remote_ip}"
    end
  end

  def signup_check_rate_limit_max
    case action_name
    when "username_check"
      100
    when "email_check"
      GitHub.email_check_rate_limit
    end
  end

  def signup_check_rate_limit_ttl
    case action_name
    when "username_check"
      GitHub::RateLimitedRequest::DEFAULT_RATE_LIMIT_TTL
    when "email_check"
      GitHub.email_check_ttl
    end
  end

  def record_username_check_limit
    GitHub.dogstats.increment("rate_limited", tags: ["action:username_check"]) if action_name == "username_check"
  end

  def get_pending_invitation(invitation_token)
    return unless invitation_token.present?
    OrganizationInvitation.pending.find_by_token(invitation_token)
  end

  def get_pending_repo_invitation(invitation_token)
    return unless invitation_token.present?
    RepositoryInvitation.find_by_token(invitation_token)
  end

  # Private: Enqueues a job to set the user's email marketing preference.
  def set_email_preference
    email_preference = params[:all_emails].present? ? "marketing" : "transactional"

    GitHub.context.push(visitor_id: current_visitor.id)

    UpdateNewsletterPreferenceJob.perform_later(current_user.id, email_preference, true)
  end

  def publish_payment_method_changed_for(actor:, account:, payment_method:)
    GlobalInstrumenter.instrument(
      "billing.payment_method.addition",
      actor_id: actor.id,
      account_id: account.id,
      payment_method_id: payment_method.id,
    )
  end

  def filename_for_zip_download(repo, branch)
    if repo && branch
      repo.archive_command(branch, "zip").filename
    end
  end

  def params_from_downloading_repo?
    params[:source] == "download" && params[:source_repo].present? && params[:branch].present?
  end

  # Staff users can temporarily avoid getting their test accounts flagged as spammy
  # by possessing a valid `staffonly` cookie in combination with a `skip_signup_spam_checks=yes` cookie
  def user_exempt_from_spam_checks?
    GitHub.flipper[:skip_spam_checks_for_staff].enabled? &&
      GitHub.employee_unicorn? &&
      GitHub::StaffOnlyCookie.read(cookies).present? &&
      cookies["skip_signup_spam_checks"] == "yes"
  end

  def pricing_models
    return @pricing_models if defined?(@pricing_models)

    free_org = Organization.new(plan: GitHub::Plan.free)
    free_pricing_model = Billing::PlanChange::PerSeatPricingModel.new(free_org, seats: free_org.seats)

    business_org = Organization.new(plan: GitHub::Plan.business)
    business_pricing_model = Billing::PlanChange::PerSeatPricingModel.new(business_org, seats: business_org.seats)

    default_org = Organization.new(plan: GitHub::Plan.default_plan)
    business_plus_pricing_model = Billing::PlanChange::PerSeatPricingModel.new(default_org, seats: default_org.seats, new_plan: GitHub::Plan.business_plus)

    @pricing_models = {
      free_pricing_model: free_pricing_model,
      business_pricing_model: business_pricing_model,
      business_plus_pricing_model: business_plus_pricing_model,
    }
  end
end
