# frozen_string_literal: true

# Used when rendering a partial for live updates: much of our
# live updates happen via a call to #show_partial in a controller
# that requests a partial specified in the template.  We
# whitelist partials that are okay to render.  Any other requests for
# unknown / invalid partials should get a 404.
module ShowPartial
  include TimelineMarkerHelper

  Whitelist = %w[
    commit/timeline_marker
    commit/visible_comments_header
    composable_comments/show
    issues/checklist
    issues/filters/assigns_content
    issues/filters/authors_content
    issues/filters/labels_content
    issues/filters/milestones_content
    issues/filters/projects_content
    issues/filters/orgs_content
    issues/form_actions
    issues/state_button_wrapper
    issues/sidebar
    issues/sidebar/assignee_menu_content
    issues/sidebar/assignees_menu_content
    issues/sidebar/labels_menu
    issues/sidebar/labels_menu_content
    issues/sidebar/milestone_menu_content
    issues/sidebar/project_card_move
    issues/sidebar/projects_menu_content
    issues/sidebar/new/assignee
    issues/sidebar/new/assignees
    issues/sidebar/new/labels
    issues/sidebar/new/milestone
    issues/sidebar/new/projects
    issues/sidebar/show/assignee
    issues/sidebar/show/assignees
    issues/sidebar/show/labels
    issues/sidebar/show/milestone
    issues/sidebar/show/projects
    issues/sidebar/show/references
    issues/timeline
    issues/title
    issues/triage/actions_content
    issues/triage/assigns_content
    issues/triage/labels_content
    issues/triage/milestones_content
    projects/card_issue_details_title
    projects/card_issue_details_title_icon
    pull_requests/deployments_box
    pull_requests/description_branches_dropdown
    pull_requests/form_actions
    pull_requests/merging
    pull_requests/sidebar
    pull_requests/stale_comparison
    pull_requests/state
    pull_requests/state_button_wrapper
    pull_requests/tabs
    pull_requests/timeline
    pull_requests/title
    pull_requests/in_progress_banner
    tree/recently_touched_branches_list
  ]

  ConditionalAccessExempt = %w[
    tree/recently_touched_branches_list
  ]

  # Maps partial names to dom ids.
  #
  # The key MUST be symbols matching the partial name like
  # "pull_requests/form_actions".
  #
  # The value SHOULD be an ID selector for the element on the page.
  PartialMapping = {
    sidebar: "#partial-discussion-sidebar",
    timeline: "#partial-timeline",
    timeline_marker: "#partial-timeline-marker",
    visible_comments_header: "#partial-visible-comments-header",
    merging: "#partial-pull-merging",
    form_actions: "#partial-new-comment-form-actions",
    new_message: "#partial-new-discussion-message",
    title: "#partial-discussion-header",
    state_button_wrapper: "#issue-state-button-wrapper",
  }

  private

  def valid_partial?(partial)
    return false unless partial
    Whitelist.include?(partial)
  end

  def conditional_access_exempt_partial?(partial)
    return false unless partial
    ConditionalAccessExempt.include?(partial)
  end

  # Public: Render a set of partials back to the browser for immediate
  # updating. You'll want to do this on actions when you don't want the
  # user to wait for a live update to come through to get the content
  # update.
  #
  # object - A "conversable object"
  # partials- Hash where keys are Symbols from PartialMapping.keys and
  #           values are Hashes of local assigns.
  # status - Optional status value to render, default :ok.
  #
  # See cousin actions IssuesController#show_partial and
  # PullRequestsController#show_partial.
  #
  # Returns nothing.
  def render_immediate_partials(object, partials:, status: :ok)
    case object
    when PullRequest
      path = "pull_requests"
    when Issue
      path = "issues"
    when Commit
      path = "commit"
    when Gist
      path = "gists/gists"
    else
      raise TypeError, "Unsupported partial path for #{object.class}"
    end

    partial_content = {}

    begin
      partials.each do |partial, locals|
        options = {
          partial: "#{path}/#{partial}",
          object: object,
          formats: [:html],
        }
        options[:locals] = locals

        partial_content[page_selector_for_partial_name(partial)] = render_to_string(options)
      end
    rescue ActionView::TemplateError => e
      # TemplateErrors can get turned into plain old 404s on production, so
      # it is good to send them off to Failbot.  This can happen if one of the
      # partials invoked by render_to_string can't render another partial,
      # for example.
      Failbot.report(e)
      raise e
    end

    render json: { "updateContent" => partial_content }, status: status
  end

  # Internal: Get document selector for partial name.
  #
  # Also see TimelineMarkerHelper in helpers/timeline_marker_helper.rb.
  #
  # sym - Symbol partial name in PartialMapping whitelist.
  #
  # Returns a String DOM selector.
  def page_selector_for_partial_name(sym)
    # Special case timeline marker to include the timestamp of
    # X-Timeline-Last-Modified header. Ensures we only try replace the
    # originally requested range of comments.
    if sym == :timeline_marker && (last_modified = discussion_last_modified_at)
      "#partial-timeline-marker[data-last-modified='#{last_modified.httpdate}']"
    elsif sym == :timeline && (last_modified = discussion_last_modified_at)
      "#partial-timeline[data-last-modified='#{last_modified.httpdate}']"
    else
      PartialMapping[sym]
    end
  end
end
