# frozen_string_literal: true

# This is an abstract superclass for all Stafftools::* controllers. It will
# never be instantiated directly, and it has no actions.
class StafftoolsController < ApplicationController
  areas_of_responsibility :stafftools

  before_action :push_failbot_metadata
  before_action :require_admin_frontend, except: :modal
  before_action :site_admin_only
  before_action :login_required
  before_action :sudo_filter
  before_action :increment_stats
  skip_before_action :cap_pagination
  skip_before_action :perform_conditional_access_checks
  unless GitHub.enterprise?
    before_action :ensure_stafftools_authorization
    before_action :prompt_for_hubber_access
  end

  layout "stafftools"
  javascript_bundle :stafftools

  helper_method :this_user

  def index
    render "stafftools/index"
  end

  def modal
    respond_to do |format|
      format.html do
        if request.xhr?
          render "stafftools/modal", layout: false
        else
          redirect_to stafftools_url
        end
      end
    end
  end

  # Find the current user from the route
  #
  # user - The login name of the user in question
  def this_user
    return @user if @user

    login = if params[:user_id]
      params[:user_id]
    elsif params[:id]
      params[:id]
    elsif params[:user].is_a?(String)
      # Sometimes (especially on #invite), it's a Hash
      params[:user]
    end

    @user = User.where(type: ["User", "Organization"]).find_by_login(login)
  end

  # Lookup the user, 404 out if we can't find it
  def ensure_user_exists
    render_404 unless this_user
  end

  def ensure_billing_enabled
    render_404 unless GitHub.billing_enabled?
  end

  def prompt_for_hubber_access
    return unless this_user && this_user.employee?
    return if this_user == current_user
    if !current_user.hubber_access_reason_provided?(this_user)
      render "stafftools/users/locked_user"
    end
  end

  # View helper to check if the current user is authorized to access a given
  # controller/action combo in `config/stafftools_permissions.yml`.
  #
  # Accepts two parameters as a hash:
  #   action: (required) the controller action to be authorized;
  #   controller: (optional) the controller the action lives in.
  #               Defaults to current controller.
  #
  def stafftools_action_authorized?(options = {})
    begin
      action = options.fetch(:action).to_s
    rescue KeyError
      raise ArgumentError, ":action missing from #stafftools_action_authorized? check"
    end

    stafftools_action = {
      controller: options.fetch(:controller, self.class.name).to_s,
      action: action,
    }

    Stafftools::AccessControl.authorized?(current_user, stafftools_action)
  end
  helper_method :stafftools_action_authorized?

  # before_action to hide eventer only features
  # used mostly in DevtoolsController
  def eventer_required
    return render_404 unless GitHub.eventer_enabled?
  end

  private

  def push_failbot_metadata
    Failbot.push(app: "github-stafftools", stafftools: true)
  end

  def ensure_account_is_user
    render_404 unless this_user.user?
  end

  def ensure_org_not_user
    render_404 unless @user&.organization?
  end

  helper_method :current_repository

  def current_repository
    return @current_repository if defined?(@current_repository)
    reponame = params[:repository] || params[:repository_id] || params[:id]
    @current_repository = owner.find_repo_by_name(reponame) if owner
  end

  def repository_access
    @repository_access ||= current_repository.access
  end

  def owner
    @owner ||= User.find_by_login(params[:user_id]) if params[:user_id]
  end

  # Lookup the repo, 404 out if we can't find it
  def ensure_repo_exists
    render_404 unless current_repository
  end

  def this_team
    team_id = params[:team_id] || params[:id]
    @this_team ||= this_user.teams.find_by_slug(team_id) if this_user
  end
  helper_method :this_team

  def ensure_team_exists
    render_404 unless this_team
  end

  def this_issue
    @this_issue ||= begin
      issue_num = params[:issue_id] || params[:pull_request_id] || params[:id]
      current_repository.issues.find_by_number(issue_num.to_i)
    end
  end
  helper_method :this_issue

  def ensure_issue_exists
    return render_404 if this_issue.nil?
  end

  def this_discussion
    @this_discussion ||= begin
      discussion_num = params[:discussion_id] || params[:id]
      current_repository.discussions.find_by_number(discussion_num.to_i)
    end
  end
  helper_method :this_discussion

  def ensure_discussion_exists
    return render_404 if this_discussion.nil?
  end

  def fetch_audit_log_teaser(query)
    @query = query
    es_query = Audit::Driftwood::Query.new_stafftools_query(
      phrase: query,
      current_user: current_user,
      per_page: 5,
    )

    results = es_query.execute
    #Driftwood::Results does not have next_page.present?
    @more_results =
      if results.respond_to?(:has_next_page?)
        results.has_next_page?
      else
        results.next_page.present?
      end
    @logs = AuditLogEntry.new_from_array(results)
    {
      query: @query,
      more_results: @more_results,
      logs: @logs,
    }
  end

  # Increment stats so we can see what parts of stafftools are used most
  def increment_stats
    controller = params[:controller].gsub(/^stafftools\//, "").gsub("/", "_")
    method = request.try(:method).downcase
    action = params[:action]
    GitHub.dogstats.increment("request.stafftools", tags: ["controller:#{controller}", "method:#{method}", "action:#{action}"])
  end

  def instrument(key, payload = {})
    auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(current_user)
    GitHub.instrument(key, payload.merge(auditing_actor))
  end

  # Verify Hubber has an appropriate stafftools role for this action.
  def ensure_stafftools_authorization
    stafftools_action = { controller: self.class.name, action: action_name }
    unless logged_in? && Stafftools::AccessControl.authorized?(current_user, stafftools_action)
      render_403_for_employees
    end
  end

  def display_management_console_link?
    GitHub.management_console_enabled? && !GitHub.private_instance?
  end
  helper_method :display_management_console_link?
end
