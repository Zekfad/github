# frozen_string_literal: true

module IntegrationsControllerMethods
  extend ActiveSupport::Concern
  include IntegrationHelper
  include IntegrationManagerHelper
  include GitHub::RateLimitedRequest

  RESOURCE_SUBJECT_TYPES = (Repository::Resources.subject_types + Organization::Resources.subject_types + User::Resources.subject_types)
  MAX_APPS_FLASH_ERROR_MESSAGE = "You can't create any more GitHub Apps. You've reached the limit for the number of applications owned by this account.".freeze

  included do
    rate_limit_requests \
      only: [:receive_manifest],
      if: :anonymous_request?,
      max: 5,
      ttl: 1.minute,
      log_key: "receive-manifest-abuse"

    before_action :login_required, except: [:receive_manifest]
    before_action :sudo_filter, except: [:index, :receive_manifest]
    before_action :check_apps_creation_limit, only: [:new]
    before_action :filter_unused_hook_attributes, only: [:create, :update]

    # While we want to allow full-trust GitHub App owners (site admins) to
    # manage much of their app, we don't want to let them shoot themselves in
    # the foot. So, we 404 the foot-gun actions.
    before_action :prevent_full_trust_apps, only: [
      :installations,
      :make_internal,
      :make_public,
      :destroy,
      :transfer,
    ]

    # To prevent potential abuse, certain actions should only be performed on
    # full trust Apps by GitHub site admins.
    # Bounty: https://github.com/github/github/issues/117891
    before_action :prevent_full_trust_apps_for_non_site_admins, except: [
      :index,
      :show,
      :permissions,
      :installations,
      :advanced,
      :new,
      :receive_manifest,
      :create,
      :preview_note,
      :keys,
      :make_internal,
      :make_public,
      :destroy,
      :transfer,
    ]

    helper_method :current_context
  end

  def index
    @integrations = current_context.integrations.not_marked_for_deletion.not_for_github_connect.
      order("integrations.name asc").paginate(page: current_page, per_page: 15)

    pending_transfers = current_context.inbound_integration_transfers.includes(:integration, :requester)

    render_template_view "integrations/settings/index", Integrations::IndexView,
      integrations: @integrations,
      pending_transfers: pending_transfers,
      owner: current_context
  end

  def show
    render_template_view "integrations/settings/show", Integrations::ShowView,
      integration: current_integration
  end

  def permissions
    render_template_view "integrations/settings/integration_permissions", Integrations::ShowView,
      integration: current_integration
  end

  def installations
    render_template_view "integrations/settings/installations", Integrations::ShowView,
      integration: current_integration,
      page: params[:page] || 1
  end

  def advanced
    render_template_view "integrations/settings/advanced", Integrations::ShowView,
      integration: current_integration
  end

  def beta_features
    render_template_view "integrations/settings/beta_features", Integrations::ShowView,
      integration: current_integration
  end

  def beta_toggle
    flag = params["beta_feature"]
    value = params["beta_feature_toggle"]

    if (attribute = Integrations::ShowView.model_owned_opt_in_attribute(flag))
      value = Integrations::ShowView.enable_or_disable_to_boolean(value)

      begin
        current_integration.update!(attribute => value)
      rescue ActiveRecord::RecordInvalid => e
        Failbot.report(e)
        flash[:error] = "Something went wrong toggling that feature, please try again."
      end
    else
      Integrations::ShowView.toggle_feature(
        flag,
        value,
        integration: current_integration,
      )
      flash_message = Integrations::ShowView.toggle_feature_flash_message(flag, value, integration: current_integration)

      flash[:notice] = flash_message
    end

    redirect_to gh_settings_app_beta_features_path(current_integration)
  end

  def new
    integration = current_context.integrations.build(build_integration_params)

    render_template_view "integrations/settings/new", Integrations::FormView,
      integration: integration
  end

  def receive_manifest
    manifest = IntegrationManifest.new(data: params[:manifest], owner: current_context)
    owner_login = params[:organization_id] || "current_user"

    if manifest.valid?
      kv_data = {
        manifest: manifest[:data],
        owner: owner_login
      }

      manifest_token = manifest.generate_kv_key
      GitHub.kv.set(
        manifest_token,
        kv_data.to_json,
        expires: 5.minutes.from_now
      )

      cookies[:app_manifest_token] = {
        value: manifest_token,
        expires: 5.minutes.from_now,
        secure: request.ssl?,
        httponly: true
      }

      redirect_to new_from_manifest_path
    else
      render_template_view "integrations/settings/invalid_manifest",
        Integrations::NewFromManifestView, manifest: manifest, owner: owner_login
    end
  end

  def create
    return create_manifest if params[:integration_manifest]

    integration = current_context.integrations.build(create_integration_params)

    if integration.save
      flash[:integration_just_created] = true

      GlobalInstrumenter.instrument "integration.create", {
        integration: integration,
        actor: current_user,
        owner: current_context,
        from_manifest: false,
      }

      redirect_to gh_settings_app_path(integration)
    else
      if integration.errors.has_key?(:default_events)
        flash.now[:error] = integration.errors.full_messages_for(:default_events).join("\n")
      end
      render_template_view "integrations/settings/new", Integrations::FormView,
        integration: integration
    end
  end

  def update
    if current_integration.update(update_integration_params)
      if request.xhr?
        head :ok
      else
        redirect_to gh_settings_app_path(current_integration),
          notice: "Got it. Your GitHub App has been updated."
      end
    else
      if request.xhr?
        render json: current_integration.errors, status: :unprocessable_entity
      else
        render_template_view "integrations/settings/show", Integrations::ShowView,
          integration: current_integration
      end
    end
  end

  def update_permissions
    result = Integration::PermissionsEditor.perform(
      integration: current_integration,
      permissions_and_events: update_integration_permissions_params,
    )

    if result.success?
      redirect_to gh_settings_apps_path(current_context),
        notice: "Got it. Your GitHub App has been updated."
    else
      flash.now[:error] = result.error
      render_template_view "integrations/settings/integration_permissions", Integrations::ShowView,
        integration: current_integration
    end
  end

  def preview_note
    return render_404 unless params[:field]

    record = case params[:field]
    when "note"
      IntegrationVersion.new(note: params[:text])
    when "description"
      Integration.new(description: params[:text])
    end

    render html: record.body_html.html_safe # rubocop:disable Rails/OutputSafety
  end

  def generate_key
    key = current_integration.generate_key(creator: current_user)
    filename = [
      current_integration.slug,
      Date.today.iso8601,
      "private-key",
      "pem",
    ].map(&:parameterize).join(".")

    send_data key.private_key.to_pem,
      filename: filename,
      type: "application/x-pem-file",
      disposition: "attachment"
  end

  def remove_key
    public_key = current_integration.public_keys.find_by_id(params[:key_id])
    return render_404 if public_key.nil?

    if public_key.destroy
      if request.xhr?
        head :ok
      else
        redirect_to gh_settings_app_path(current_integration),
          notice: "Master key removed"
      end
    else
      if request.xhr?
        render status: :unprocessable_entity, json: {message: public_key.errors.full_messages.to_sentence}
      else
        redirect_to gh_settings_app_path(current_integration),
          notice: "Master key removed"
      end
    end
  end

  def keys
    return redirect_to gh_settings_app_path(current_integration) unless request.xhr?

    respond_to do |format|
      format.html do
        render_partial_view "integrations/settings/keys", Integrations::ShowView, integration: current_integration
      end
    end
  end

  def make_public
    if current_integration.make_public
      redirect_to gh_settings_app_path(current_integration),
        notice: "The GitHub App is now public. Anyone is free to install it."
    else
      redirect_to gh_settings_app_path(current_integration),
        alert: "There was a problem trying to make this GitHub App public."
    end
  end

  def make_internal
    if current_integration.can_make_internal?

      if current_integration.make_internal
        redirect_to gh_settings_app_path(current_integration),
          notice: "The GitHub App is now internal. It can only be installed on this account."
      else
        redirect_to gh_settings_app_path(current_integration),
          alert: "There was a problem trying to make this GitHub App internal."
      end

    else
      redirect_to gh_settings_app_path(current_integration),
        alert: "This GitHub App cannot be made internal because it is already installed on other accounts."
    end
  end

  def destroy
    if delete_verification_provided?
      if current_integration.update(deleted_at: Time.zone.now)
        DestroyIntegrationJob::Legacy.perform_later(current_integration.id)

        return redirect_to gh_settings_apps_path(current_context),
          notice: "Job queued to delete GitHub App. It may take a few minutes to complete."
      end
    end

    redirect_to gh_settings_app_path(current_integration),
      alert: "The GitHub App was not deleted. Please provide the correct verification phrase."
  end

  # When transferring organization-owned Integrations this action is protected
  # by authz checks in before filters defined in
  # `app/controllers/orgs/integrations_controller.rb`.
  def transfer
    begin
      target = User.find_by_login!(params[:transfer_to])
      xfer = IntegrationTransfer.start(integration: current_integration, target: target, requester: current_user)

      # Redirect the user to the "transfer" page when they have the ability to
      # accept the transfer on behalf of the target (E.g. they *are* the target
      # (User) or they manage Apps for the target (Organization):
      if manages_all_integrations?(user: current_user, owner: target)
        return redirect_to gh_settings_app_transfer_path(target, xfer.id)
      else
        flash[:notice] = "GitHub App transfer request sent to #{target.login}"
      end
    rescue ActiveRecord::RecordInvalid => e
      flash[:error] = e.message
    end

    if manages_integration?(user: current_user, integration: current_integration)
      redirect_to gh_settings_app_path(current_integration)
    else
      redirect_to gh_settings_apps_path(current_context)
    end
  end

  def reset_secret
    current_integration.reset_secret

    if request.xhr?
      head 200
    else
      redirect_to gh_settings_apps_path(current_context),
        notice: "Client secret reset successfully"
    end
  end

  def revoke_all_tokens
    current_integration.async_revoke_tokens

    if request.xhr?
      head 200
    else
      redirect_to gh_settings_apps_path(current_context),
        notice: "Job queued to revoke all user tokens"
    end
  end

  private

  # Private: Returns the correct target depending on if we're working
  # with org or user integrations.
  #
  # Returns an Organization or a User
  def current_context
    raise NotImplementedError
  end

  def current_integration
    return @current_integration if defined?(@current_integration)
    @current_integration = \
      current_context.integrations.not_marked_for_deletion.find_by!(slug: params[:id])
  end

  def permitted_new_integration_attributes
    %i(
      name
      description
      url
      callback_url
      request_oauth_on_install
      setup_url
      setup_on_update
      public
      single_file_name
      user_token_expiration_enabled
    )
  end

  def permitted_integration_hook_attributes
    %i(url secret insecure_ssl active)
  end

  def build_integration_params
    params[:integration] = {}

    params[:integration][:default_permissions] = {}
    RESOURCE_SUBJECT_TYPES.each do |permitted_subject_type|
      action = params.delete(permitted_subject_type)
      next if action.blank?

      params[:integration][:default_permissions][permitted_subject_type] = action.to_sym
    end

    events = Array(params.delete(:events))
    params[:integration][:default_events]    = events.select { |event| Integration::Events::SUPPORTED_EVENTS.include?(event) }
    params[:integration][:integrator_events] = events.select { |event| Integration::Events::INTEGRATOR_EVENTS.include?(event) }

    params[:integration][:hook_attributes]                = {}
    params[:integration][:hook_attributes][:url]          = params.delete(:webhook_url)
    params[:integration][:hook_attributes][:secret]       = params.delete(:webhook_secret)
    params[:integration][:hook_attributes][:insecure_ssl] = params.delete(:webhook_insecure_ssl)
    params[:integration][:hook_attributes][:active]       = params.delete(:webhook_active)

    if params[:content_reference_domains]
      params[:integration][:content_reference] = params.delete(:content_reference_domains)
    end

    if params[:domain]
      params[:integration][:content_reference] ||= []
      params[:integration][:content_reference] << params.delete(:domain)
    end

    permitted_new_integration_attributes.each do |attribute|
      next unless params.key?(attribute)
      params[:integration][attribute] = params.delete(attribute)
    end

    create_integration_params
  end

  def create_integration_params
    params.require(:integration).permit(
      *permitted_new_integration_attributes,
      default_permissions: permitted_default_permissions,
      integrator_events: [],
      default_events: [],
      content_reference: [],
      hook_attributes: permitted_integration_hook_attributes,
    ).with_defaults(
      integrator_events: [],
    )
  end

  def update_integration_params
    params.require(:integration).permit(
      :name,
      :description,
      :url,
      :callback_url,
      :request_oauth_on_install,
      :setup_url,
      :setup_on_update,
      :bgcolor,
      :public,
      hook_attributes: permitted_integration_hook_attributes,
    )
  end

  def update_integration_permissions_params
    params.require(:integration).permit(
      :note,
      :single_file_name,
      content_reference: [],
      integrator_events: [],
      default_events: [],
      default_permissions: permitted_default_permissions,
    ).with_defaults(
      integrator_events: [],
    )
  end

  def permitted_default_permissions
    params[:integration][:default_permissions].reject! do |permission, action|
      RESOURCE_SUBJECT_TYPES.include?(permission) ? action.blank? : false
    end

    default_permissions = params[:integration][:default_permissions]
    default_permissions.empty? ? default_permissions : default_permissions.keys
  end

  def delete_verification_provided?
    verification_pattern = %r{\A#{ Regexp.quote current_integration.name }\z}i

    params[:verify].to_s =~ verification_pattern
  end

  def prevent_full_trust_apps
    render_404 if current_integration.full_trust?
  end

  def prevent_full_trust_apps_for_non_site_admins
    render_404 if current_integration.full_trust? && !current_user.site_admin?
  end

  def check_apps_creation_limit
    if current_context.reached_applications_creation_limit?(application_type: Integration)
      flash[:error] = MAX_APPS_FLASH_ERROR_MESSAGE
      redirect_to gh_settings_apps_path(current_context)
    end
  end

  # In order to show the Hook fields on the page when
  # there isn't a hook we have to build an new hook.
  #
  # This means that hook attributes will always be passed
  # in and therefore even if we don't want to create a hook
  # it tries to.
  #
  # This filters out the hook_attributes from the params
  # if it's just a default set of params.
  def filter_unused_hook_attributes
    default_hook_attributes = {
      "url"          => "",
      "secret"       => "",
      "insecure_ssl" => "0",
      "active"       => "0",
    }

    if params.dig("integration", "hook_attributes") == default_hook_attributes
      params["integration"].delete("hook_attributes")
    end
  end

  def anonymous_request?
    !logged_in?
  end
end
