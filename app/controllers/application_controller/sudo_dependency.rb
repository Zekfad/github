# frozen_string_literal: true

# Sudo filter protects actions by rendering an interstitial when sudo challenge
# has not been authenticated in some period of time.
# * GET requests render the sudo form that POSTs to /sessions/sudo and
#   redirects to params[:sudo_return_to] with the original GET params
# * POST requests render the sudo form but POST to the original URL,
#  preserving the params and adding them to the body.
module ApplicationController::SudoDependency
  extend ActiveSupport::Concern

  included do
    helper_method :valid_sudo_session?
  end

  private
  def sudo_filter
    perform_sudo_filter
  end

  # Request and check sudo access. If the sudo session is valid, it is
  # renewed without a password prompt. If the session is not valid, the sudo
  # prompt will be rendered.
  #
  # store: either flash.now (default, POST requests) or flash (GET requests,
  #   passed in by sudo/sudo action)
  #
  # returns true if the sudo session is valid. Renders a sudo prompt and
  # halts the filter chain if the session is not valid.
  def perform_sudo_filter(store: flash.now)
    return true unless GitHub.auth.sudo_mode_enabled?(current_user)
    return redirect_to_login unless logged_in?

    authenticate_sudo_credentials!(store)

    if valid_sudo_session? || recently_solved_sudo_challenge?
      user_session.enable_sudo
      return true
    else
      if request.xhr?
        if at_auth_limit?
          head :too_many_requests
        else
          head :unauthorized
        end
      else
        render "sudo/sudo", layout: authentication_layout
      end
    end
  end

  # Because GET-based/AJAX sudo filters involve POSTing to /sessions/sudo
  # before redirecting to the desired endpoint, we perform the sudo
  # challenge using flash instead of flash.now.
  def get_based_sudo_filter
    perform_sudo_filter(store: flash)
  end

  # Internal: determine whether or not a sudo session is valid for the
  # current user session.
  def valid_sudo_session?
    return true unless GitHub.auth.sudo_mode_enabled?(current_user)
    user_session&.sudo?
  end

  # Internal: Validate sudo_login/sudo_password params and enable sudo mode.
  #
  # Returns the timestamp of the recent sudo challenge success or nil if
  # invalid (or no) credentials are supplied in this request.
  def authenticate_sudo_credentials!(store)
    if valid_sudo_u2f? || valid_sudo_password?
      set_sudo_authenticated_at(store)
    end
  end

  # Internal: store the last successful sudo challenge timestamp in
  # flash (GETs) or flash.now (POSTs).
  #
  # Returns the current timestamp
  def set_sudo_authenticated_at(store)
    store[:sudo_authenticated_at] = Time.now.to_s
  end

  # Internal: has a user entered their password successfully during a sudo
  # challenge recently?
  #
  # Pull the timestamp of the last successful sudo challenge response from
  # the previous request (by using flash), return true if this value is
  # within the valid window for the access level.
  def recently_solved_sudo_challenge?
    return false unless authed_at = flash[:sudo_authenticated_at] # checks flash and flash.now

    DateTime.parse(authed_at) > UserSession::SUDO_EXPIRY.ago
  end

  # Internal: Did the user successfully authenticate with her password?
  #
  # Returns boolean.
  def valid_sudo_password?
    return false unless params[:sudo_password]

    sudo_login    = params.delete :sudo_login
    sudo_password = params.delete :sudo_password

    # Enterprise + external auth require both the login & password to
    # confirm a user because we may cleanup a login that is valid in LDAP,
    # for example. In that case we use the POSTed login parameter instead
    # of the current user's login.
    if !GitHub.auth.external_user?(current_user)
      sudo_login = current_user.login
    end

    auth_result = GitHub.auth.sudo_password_authenticate(
      sudo_login, sudo_password
    )

    session[::CompromisedPassword::WEAK_PASSWORD_KEY] = auth_result.has_compromised_password

    if auth_result.at_auth_limit_failure?
      flash.now[:error] = "Too many password attempts."
      @at_auth_limit = true
      false
    elsif auth_result.failure?
      flash.now[:error] = "Incorrect password."
      false
    elsif auth_result.user != current_user
      flash.now[:error] = "Incorrect username."
      false
    else
      true
    end
  end

  # Internal: Did the user successfully authenticate with U2F?
  #
  # Returns boolean.
  def valid_sudo_u2f?
    json_sign_response = params[:u2f_response]
    return false if [json_sign_response, webauthn_sign_challenge_from_request].any?(&:blank?)

    origin = Addressable::URI.new(scheme: request.scheme, host: request.host).to_s
    if current_user.webauthn_or_u2f_authenticated?(origin, webauthn_sign_challenge_from_request, json_sign_response)
      true
    else
      false
    end
  end
end
