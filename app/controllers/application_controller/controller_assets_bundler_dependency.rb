# frozen_string_literal: true

# Used to define in the controller what asset tags you want rendered
#
# example:
#### adds admin stylesheet name to the array
# class AdminController < ApplicationController
#   stylesheet_bundle :admin
# end
#
#### creates styletags for all the stylesheets that are in the array
# (In layouts/application.html.erb)
# <%= include_controller_stylesheet_bundles %>
#
module ApplicationController::ControllerAssetsBundlerDependency
  extend ActiveSupport::Concern

  included do
    class_attribute :stylesheet_bundles
    class_attribute :javascript_bundles

    helper_method :stylesheet_bundles
    helper_method :javascript_bundles

    self.stylesheet_bundles = Set.new
    self.javascript_bundles = Set.new
  end

  class_methods do
    def stylesheet_bundle(*sources)
      self.stylesheet_bundles += sources.map(&:to_sym).to_set
    end

    def javascript_bundle(*sources)
      self.javascript_bundles += sources.map(&:to_sym).to_set
    end
  end
end
