# frozen_string_literal: true
module ApplicationController::PaginationDependency
  # Generates parameters to paginate a GraphQL collection using before & after
  #
  # page_size: The page size to use in pagination
  #
  # Returns a hash of parameters for use in execution
  def graphql_pagination_params(page_size:)
    if params[:before]
      {before: params[:before], last: page_size}
    else
      {after: params[:after].presence, first: page_size}
    end
  end
end
