# frozen_string_literal: true

module ApplicationController::NotificationsDependency
  extend ActiveSupport::Concern

  included do
    helper_method :notification_referrer
    helper_method :show_notification_shelf?

    NotificationReferrerQuery = parse_query <<-'GRAPHQL'
    query($notificationThreadId: ID!) {
      node(id: $notificationThreadId) {
        ...Views::Notifications::V2::TopShelf::NotificationThread
      }
    }
    GRAPHQL
  end

  # Returns the notification referrer parameters, and permits them
  # so they can be used in redirects if required
  def notifications_referrer_params
    params.slice(*NotificationsV2Controller::REFERRER_PARAMS).permit!
  end

  def notification_referrer
    return @notification_referrer if defined?(@notification_referrer)

    @notification_referrer = if params[:notification_referrer_id]
      # Don't return a notification if the current controller is going to
      # render the SSO login prompt
      if GitHub.external_identity_session_enforcement_enabled? &&
         require_active_external_identity_session? &&
         !external_identity_session_fresh?

        return :conditional_access_error
      end

      begin
        # Don't return the notification and render the notification shelf if a required external
        # identity is not fresh. This prevents us showing the notification on the SSO login pages
        platform_execute(
          NotificationReferrerQuery,
          variables: { notificationThreadId: params[:notification_referrer_id] },
          context: { enforce_conditional_access_via_graphql: true },
        ).node
      rescue PlatformHelper::ConditionalAccessError
        # This exception means that the user session is currently failing SAML/IP/etc checks
        :conditional_access_error
      rescue StandardError => e
        # Rescuing any StandardError here as we really don't errors in shelf rendering
        # to cause 500s for the page we are trying to navigate.
        if Rails.env.production?
          Failbot.report(e)
        else
          raise e
        end
      end
    end
  end

  def show_notification_shelf?
    notification_referrer.present? && notification_referrer != :conditional_access_error
  end
end
