# frozen_string_literal: true

class ApplicationController
  module AuthenticatedSystem
    protected
      # Returns true or false if the user is logged in.
      # Preloads @current_user with the user model if they're logged in.
      def logged_in?
        !!current_user
      end

      # Allow subclass to add additional authentication methods.
      #
      # Returns User or nil.
      def authentication_methods
        login_from_user_session || nil
      end

      # Accesses the current user from the session.
      def current_user
        return @current_user if defined? @current_user
        self.current_user = authentication_methods
      end

      # Internal: Assign current_user for the duration of the request.
      #
      # Doesn't create any persisent sessions.
      #
      # user - The User
      #
      # Returns user.
      def current_user=(user)
        if user
          actor_context = user.event_context(prefix: :actor)
          GitHub.context.push(actor_context)
          Audit.context.push(actor_context)
        end

        if user_session
          GitHub.context.push(actor_session: user_session.id)
          Audit.context.push(actor_session: user_session.id)
        end

        @current_user = user
      end

      # The user who is either partially or fully authenticated. This is useful
      # for two-factor code.
      #
      # Returns a user or nil.
      def authentication_user
        return @authentication_user if defined? @authentication_user
        @authentication_user = current_user
      end

      # Check if the user is authorized
      #
      # Override this method in your controllers if you want to restrict access
      # to only a few actions or if you want to check if the user
      # has the correct rights.
      #
      # Example:
      #
      #  # only allow nonbobs
      #  def authorized?
      #    current_user.login != "bob"
      #  end
      def authorized?
        logged_in?
      end

      # Filter method to enforce a login requirement.
      #
      # To require logins for all actions, use this in your controllers:
      #
      #   before_action :login_required
      #
      # To require logins for specific actions, use this in your controllers:
      #
      #   before_action :login_required, :only => [ :edit, :update ]
      #
      # To skip this in a subclassed controller:
      #
      #   skip_before_action :login_required
      def login_required
        logged_in? || access_denied
      end

      # Filter method to enforce an anonymous user.
      def anon_required
        redirect_to "/" if logged_in?
      end

      # Filter method to enforce authorization.
      #
      # NOTE: There is a distinct difference between this and enforcing login.
      # The authorized? method is overridden in AbstractRepositoryController to
      # return true when in a public repository.
      def authorization_required
        authorized? || access_denied
      end

      # Redirect as appropriate when an access request fails.
      #
      # The default action is to redirect to the login screen.
      #
      # Override this method in your controllers if you want to have special
      # behavior in case the user is not authorized
      # to access the requested action.  For example, a popup window might
      # simply close itself.
      def access_denied
        if logged_in?
          render_404
        else
          respond_to do |format|
            format.html do
              if request.xhr?
                head :unauthorized
              else
                redirect_to_login(request.url)
              end
            end

            format.html_fragment do
              head :not_found
            end

            format.atom do
              head :unauthorized
            end

            format.json do
              headers["WWW-Authenticate"] = %(Basic realm="GitHub")
              render json: { error: "Couldn’t authenticate you" }, status: :not_found
            end

            format.all do
              headers["WWW-Authenticate"] = %(Basic realm="GitHub")
              render plain: "Couldn’t authenticate you", status: :not_found
            end
          end
        end

        false
      end

      # Override to opt an endpoint out of being able to touch/bump an active
      # session, thus extending its life. We have general restrictions on what
      # can bump a session, but sometimes we need to exclude specific endpoints.
      def allow_session_touching?
        true
      end

      # Store the URI of the current request in the session.
      #
      # We can return to this location by calling #redirect_to_return_to.
      def store_location
        session[:return_to] = request.fullpath
      end

      # Inclusion hook to make #current_user and #logged_in?
      # available as ActionView helper methods.
      def self.included(base) # rubocop:disable Lint/IneffectiveAccessModifier
        base.send :helper_method, :current_user, :logged_in?, :user_session,
          :u2f_register_requests, :webauthn_register_request,
          :webauthn_sign_request, :u2f_sign_requests,
          :security_key_enabled_for_request?,
          :webauthn_sign_challenge_for_response
      end

      def permission_denied_if_mismatched_login_and_token
        access_denied if token_based_request? && !logged_in?
      end

      # login by authenticating from credentials found in the HTTP Authorization
      # header.
      def login_from_authorization_header_auth
        creds = Api::RequestCredentials.from_env(
          request.env,
          headers: true,
          params: false,
        )
        return unless creds.credentials_present?
        attempt = GitHub::Authentication::Attempt.new(
          from:       :web_api,
          login:      creds.login,
          password:   creds.password,
          otp:        creds.otp,
          token:      creds.token,
          ip:         request.remote_ip,
          user_agent: request.user_agent,
          request_id: request.env["HTTP_X_GITHUB_REQUEST_ID"],
          url:        request.url,
        )

        if attempt.result.success?
          @authorization_header_authed = true
          attempt.result.user
        elsif attempt.result.web_api_with_password_failure?
          GitHub.dogstats.increment("api.authentication", tags: ["reason:web-api-password-deprecation"])
          pat_creation_url = settings_user_tokens_url(host: GitHub.host_name)
          respond_to do |format|
            format.all do
              headers["WWW-Authenticate"] = %(Basic realm="GitHub")
              render plain: "Deprecated authentication method. Create a Personal Access Token to access: #{pat_creation_url}", documentation_url: "#{GitHub.help_url}/articles/creating-a-personal-access-token-for-the-command-line", status: :unauthorized
            end
          end
          # we put a nil so that when this method is called in e.g. tree controller, it won't go into the
          # block where user.using_oauth_application? would cause a no method error for unsuccessfuly web_api auth.
          nil
        end
      end

      # Public
      def login_from_signed_auth_token(scope)
        return unless token_based_request? && scope.present?
        user = User.authenticate_with_signed_auth_token(
          token: params[:token],
          scope: scope,
        )
        @signed_token_authed = !user.nil?
        user
      end

      # Does this type of request support token authentication and is the user
      # attempting to authenticate using a token?
      #
      # Returns boolean.
      def token_based_request?
        params[:token] && token_request_format?
      end

      # Does this type of request support token authentication?
      #
      # Returns false unless overridden by the controller.
      def token_request_format?
        false
      end

      def authorization_header_authed?
        !!@authorization_header_authed
      end

      def signed_token_authed?
        !!@signed_token_authed
      end

      def u2f_register_requests(store: true)
        requests = GitHub.u2f.registration_requests
        session[:register_challenges] = requests.map(&:challenge) if store
        requests
      end

      # Returns number of milliseconds
      def webauthn_timeout
        30.seconds.in_milliseconds
      end

      # CredentialCreationOptions for public key
      # TODO: Check the excludeCredentials on the server?
      def webauthn_register_request(store: true, user: current_user)
        # ECDSA_w_SHA256 is required for using FIDO U2F keys (including
        # CTAP2 authenticators):
        #
        # - https://www.w3.org/TR/webauthn/#fido-u2f-attestation
        # - https://www.w3.org/TR/webauthn/#signature-attestation-types
        #
        # RSASSA_PKCS1_v1_5 is required for Windows Hello:
        # https://docs.microsoft.com/en-us/microsoft-edge/dev-guide/windows-integration/web-authentication
        #
        # Note: the order of algs is significant.
        cose_algs = [
          -7, # ECDSA_w_SHA256
          -257, # RSASSA_PKCS1_v1_5
        ]

        # TODO(https://github.com/github/github/issues/113003): Can we use a
        # more specific name than "GitHub Enterprise"?
        rpName = GitHub.enterprise? ? "GitHub Enterprise" : "GitHub"

        challenge = WebAuthn::Credential.options_for_get.challenge
        session[:webauthn_register_challenge] = challenge

        handle_record = WebauthnUserHandle.find_by_user_id(user.id)
        webauthn_user_handle = if handle_record.nil?
          WebauthnUserHandle.generate_value
        else
          handle_record.webauthn_user_handle
        end
        session[:webauthn_user_handle] = Base64.strict_encode64(webauthn_user_handle)

        {
          publicKey: {
            rp: {
              id: GitHub.webauthn_rp_id,
              name: rpName,
            },
            user: {
              id: Base64.strict_encode64(webauthn_user_handle),
              name: user.login,
              displayName: user.safe_profile_name,
            },
            pubKeyCredParams: cose_algs.map { |alg| {type: "public-key", alg: alg} },
            attestation: "none",
            timeout: webauthn_timeout,
            excludeCredentials: user.u2f_registrations.map(&:webauthn_public_key_credential_hash_for_client),
            challenge: challenge,
            authenticatorSelection: {
              userVerification: "discouraged",
            },
            extensions: {
              appidExclude: GitHub.u2f.app_id,
            },
          },
        }
      end

      def u2f_register_challenges
        @u2f_register_challenges ||= session.delete(:register_challenges) || []
      end

      def u2f_sign_requests(user: current_user)
        key_handles = user.u2f_registrations.map(&:key_handle)
        GitHub.u2f.authentication_requests(key_handles)
      end

      # Generate a new signing challenge and store it in the session.
      #
      # Returns a challenge String.
      def webauthn_sign_challenge_for_response
        session[:sign_challenge] ||= WebAuthn::Credential.options_for_get.challenge
      end

      # Retrieve the signing challenge from the session.
      #
      # Returns a challenge String.
      def webauthn_sign_challenge_from_request
        @webauthn_sign_challenge_from_request ||= session.delete(:sign_challenge)
      end

      # Is U2F enabled for this request?
      #
      # Returns boolean.
      def security_key_enabled_for_request?
        # Browsers require HTTPS for U2F to work.
        return false unless request.ssl?

        true
      end

      def current_device_id
        return unless GitHub.sign_in_analysis_enabled?
        return if serving_gist3_standalone?

        return @current_device_id if defined?(@current_device_id)
        @current_device_id = begin
          if cookies[:_device_id] && cookies[:_device_id] !~ AuthenticatedDevice::DEVICE_ID_REGEX
            GitHub.dogstats.increment("authenticated_device", tags: ["action:access", "error:malformed_device_id"])
            cookies.delete(:_device_id)
          end

          cookies[:_device_id] ||= begin
            GitHub.dogstats.increment("authenticated_devices.cookie", tags: [
              "controller:#{params[:controller]}",
              "action:#{params[:action]}",
            ])

            {
              value: AuthenticatedDevice.generate_id,
              expires: 1.year.from_now,
            }
          end

          cookies[:_device_id]
        end
      end

      def webauthn_sign_request(user: current_user)
        {
          publicKey: {
            userVerification: "discouraged",
            timeout: webauthn_timeout,
            challenge: webauthn_sign_challenge_for_response,
            allowCredentials: user.u2f_registrations.map(&:webauthn_public_key_credential_hash_for_client),
            rpId: GitHub.webauthn_rp_id,
            extensions: {
              appid: GitHub.u2f.app_id,
            },
          },
        }
      end

      def authentication_user_has_registered_security_key?
        authentication_user.try(:has_registered_security_key?)
      end

    private
      HTTP_AUTH_HEADERS = %w(X-HTTP_AUTHORIZATION HTTP_AUTHORIZATION Authorization)
      def http_authentication_header
        auth_key = HTTP_AUTH_HEADERS.detect { |h| request.env.has_key?(h) }
        auth_key.present? && request.env[auth_key].to_s[/^Basic\s+(.*)/m, 1]
      end
  end
end
