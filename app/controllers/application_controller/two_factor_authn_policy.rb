# frozen_string_literal: true

# The TwoFactorAuthnPolicy makes sure access to resources are only authorized
# when the user accessing a policy-protected resource has 2FA enabled.
#
# This is the ApplicationController specific implementation, and is meant to be
# used solely in that context.
module ApplicationController::TwoFactorAuthnPolicy

  # The 2FA policy is applicable if an organization or business has the corresponding
  # option enabled
  def two_factor_applicable
    return :no unless logged_in?

    start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    owner = safe_target_for_conditional_access
    end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    GitHub.dogstats.distribution("cap.target_for_conditional_access.dist", (end_time - start_time) * 1_000, tags: ["class:#{self.class.name}"])

    return :no if owner == :no_target_for_conditional_access
    owner_is_org = owner.is_a?(Organization)
    owner_is_business = owner.is_a?(Business)

    return :no unless owner_is_org || owner_is_business

    # enforcement feature flag enabled
    return :no unless owner.two_factor_cap_enforcement_enabled?
    # 2FA globally disabled?
    return :no unless GitHub.auth.two_factor_authentication_allowed?(current_user)
    # 2FA requirement enabled
    return :no unless owner.two_factor_requirement_enabled?

    return :no if owner_is_org && !current_user.affiliated_with_organization?(owner)
    return :no if owner_is_business && !current_user.is_business_member?(owner.id)
    :yes
  end

  # The 2FA policy is satisfied when the user has 2FA enabled
  def two_factor_satisfied
    return :yes if current_user&.two_factor_authentication_enabled?
    :no
  end

  def two_factor_enforce
    render_2fa_interstitial
  end

  private

  ENFORCEMENT_STATUS = :forbidden

  # This is essentially a copy of render_404.
  def render_2fa_interstitial
    target = safe_target_for_conditional_access

    if target == :no_target_for_conditional_access
      raise ArgumentError.new("unable to render 2FA conditional access policy interstitial due to :no_target_for_conditional_access")
    end

    if request.format.try(:html_fragment?)
      head ENFORCEMENT_STATUS
    elsif request.format.try(:html?)
      render "conditional_access_policies/2fa", locals: { target: target }, status: ENFORCEMENT_STATUS, layout: "application"
    elsif request.format.try(:js?) || request.format.try(:json?)
      set_static_file_csp
      render json: { error: "Forbidden - 2FA Required" }, status: ENFORCEMENT_STATUS
    else
      set_static_file_csp
      render plain: "Forbidden - 2FA Required", status: ENFORCEMENT_STATUS
    end
    nil
  end
end
