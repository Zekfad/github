# frozen_string_literal: true
#
# Trade controls restrictions related concern

module ApplicationController::TradeControlsDependency

  protected

  # Internal: trade restriction error for  orgs
  #
  # Will show trade restriction error notice if organization that has trade restrictions and
  # has project Münich enabled, is trying to perform a billing related action.
  def trade_controls_organization_billing_error
    TradeControls::Notices::Plaintext.billing_account_restricted
  end

  # Internal: trade restriction error for users
  #
  # Will show trade restriction error notice if a user that has trade restrictions and
  # has project Münich enabled, is trying to perform a billing related action.
  def trade_controls_user_billing_error
    TradeControls::Notices::Plaintext.billing_account_restricted
  end

  # Internal: used for organization settings checks
  #
  # Will check if the current_organization is trade controls restricted, and default to
  # the org messaging if so. If the org is not restricted, but
  # the current_user is trade restricted, then we use the user account messaging.
  #
  # Returns early if an organization is not in context, as we're only interested in
  # organization settings with this check. Additionally, we allow "partial" trade
  # controls restricted orgs to access their settings page.
  def ensure_trade_restrictions_allows_org_settings_access
    return unless target_org

    restricted = if target_org.has_full_trade_restrictions?
      flash[:trade_controls_organization_billing_error] = true
      true
    elsif current_user&.has_any_trade_restrictions? && target_org.charged_account?
      flash[:trade_controls_user_billing_error] = true
      true
    end

    return redirect_to(org_root_path(target_org)) if restricted
  end

  def ensure_trade_restrictions_allows_org_member_management(fallback_location:)
    return unless target_org

    if target_org.has_full_trade_restrictions?
      flash[:trade_controls_organization_billing_error] = true
      return redirect_back(fallback_location: fallback_location)
    end
  end

  private

  def target_org
    @_target_org ||= if defined?(this_organization)
      this_organization
    else
      current_organization
    end
  end
end
