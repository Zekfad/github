# frozen_string_literal: true

module ApplicationController::FeatureFlagsDependency
  extend ActiveSupport::Concern

  included do
    helper_method :current_user_feature_enabled?
    helper_method :explore_settings_enabled?
    helper_method :discover_repos_dashboard_enabled?
    helper_method :abilities_team_enabled?
    helper_method :file_codeowners_enabled?
    helper_method :mobile_notifications_setting_enabled?
    helper_method :memex_beta_enabled?
    helper_method :user_dashboard_pins_enabled?
    helper_method :gist_playground_enabled?
    helper_method :gist_playground_callout_enabled?
    helper_method :oauth_registered_callback_urls_enabled?
    helper_method :third_party_analytics_setting_enabled?
    helper_method :config_repo_enabled?
    helper_method :sales_chat_enabled?
    helper_method :site_relaunch_enabled?
    helper_method :github_one_launched?
    helper_method :force_render_raw?
    helper_method :syntax_highlighted_diffs_enabled?
    helper_method :instance_audit_log_enabled?
    helper_method :hook_deliveries_filter_enabled?
    helper_method :integrations_directory_enabled?
    helper_method :desktop_survey_hashed_id
    helper_method :aleph_code_navigation_available?
    helper_method :aleph_darkship_language?
    helper_method :marketplace_enabled?
    helper_method :marketplace_pending_installations_notice_enabled?
    helper_method :marketplace_pending_installations_enabled?
    helper_method :github_apps_in_marketplace_search_enabled?
    helper_method :collaborative_editing_enabled?
    helper_method :feature_enabled_globally_or_for_current_user?
    helper_method :responsive_pull_requests_enabled?
    helper_method :responsive_gists_enabled?
    helper_method :responsive_sidenav_enabled?
    helper_method :konami_enabled?
    helper_method :organization_discussions_enabled?
    helper_method :feature_preview_enabled?
    helper_method :community_contributors_enabled?
    helper_method :workspaces_enabled?
    helper_method :discussions_enabled?
    helper_method :merge_queue_enabled?
    helper_method :discussions_leaderboard_enabled?
    helper_method :discussions_zeit_enabled?
    helper_method :discussion_categories_enabled?
    helper_method :discussion_reddit_style_sort_enabled?
    helper_method :discussion_spotlights_enabled?
    helper_method :canary_unicorn_web_enabled?
    helper_method :private_pages_enabled?
    helper_method :trending_repositories_spoken_language_filtering_enabled?
    helper_method :flipper_session
    helper_method :eligible_for_exact_match_search?
    helper_method :enrolled_in_exact_match_search?
    helper_method :wikis_visible_by_default?
    helper_method :accessibility_fixes_enabled?
    helper_method :scoped_issue_timeline_enabled?
    helper_method :memex_issue_sidebar_enabled?
    helper_method :maintainer_stories_enabled?
    helper_method :site_packages_update_enabled?
    helper_method :site_satellite20_codespaces_demo_enabled?
    helper_method :async_dashboard_repositories_enabled?
    helper_method :enrolled_in_exact_match_ux_prototype?
    helper_method :audit_log_git_event_export_enabled?
    helper_method :can_enable_lfs_in_archives?
    helper_method :apple_iap_subscription_enabled?
    helper_method :configurable_repo_default_branch_enabled?
    helper_method :missing_tree_ref_redirect_enabled?
  end

  def current_user_feature_enabled?(sym)
    logged_in? && GitHub.flipper[sym].enabled?(current_user)
  end

  # define before_action restriction method for audit_log_export feature
  def audit_log_export_required
    render_404 unless logged_in? && GitHub.audit_log_export_enabled?
  end
  private :audit_log_export_required

  def audit_log_git_event_export_required(subject)
    render_404 unless audit_log_git_event_export_enabled?(subject)
  end
  private :audit_log_git_event_export_required

  # Public: Is the user able to set their preferred branch name to be used as the default
  # branch in new repositories they create, for themselves or for an organization?
  def configurable_repo_default_branch_enabled?
    if defined?(@configurable_repo_default_branch_enabled)
      return @configurable_repo_default_branch_enabled
    end

    @configurable_repo_default_branch_enabled = if defined?(current_organization) && current_organization
      GitHub.flipper[:configurable_repo_default_branch].enabled?(current_organization)
    else
      feature_enabled_globally_or_for_current_user?(:configurable_repo_default_branch)
    end
  end

  # Public: Should we redirect users to the same file on the default branch of a repo if
  # they try to view a file that doesn't exist on the branch specified in the URL?
  # Used with /tree routes.
  def missing_tree_ref_redirect_enabled?
    return @missing_tree_ref_redirect_enabled if defined?(@missing_tree_ref_redirect_enabled)
    @missing_tree_ref_redirect_enabled = feature_enabled_globally_or_for_current_user?(:missing_tree_ref_redirect)
  end

  # Public: Show the new feed-based Topic page design
  def topic_feed_enabled?
    feature_enabled_globally_or_for_current_user?(:topic_feed)
  end

  def explore_settings_enabled?
    return false if GitHub.enterprise?

    feature_enabled_globally_or_for_current_user?(:explore_settings)
  end

  # Public: Enable new 'Discover repositories' tab on the dashboard
  # See https://github.com/github/github/issues/76414
  def discover_repos_dashboard_enabled?
    !GitHub.enterprise?
  end

  def discover_repos_dashboard_required
    unless discover_repos_dashboard_enabled?
      if request.xhr? || pjax?
        head :not_found
      else
        render_404
      end
    end
  end

  def abilities_team_enabled?
    logged_in? && current_user.abilities_team_enabled?
  end

  # User feature flag-related methods

  def file_codeowners_enabled?
    return @file_codeowners_enabled if defined?(@file_codeowners_enabled)

    @file_codeowners_enabled = feature_enabled_globally_or_for_current_user?(:file_codeowners)
  end

  def mobile_notifications_setting_enabled?
    return @mobile_notifications_setting_enabled if defined?(@mobile_notifications_setting_enabled)

    @mobile_notifications_setting_enabled = \
      feature_enabled_globally_or_for_current_user?(:mobile_notifications_setting)
  end

  def memex_beta_enabled?
    return @memex_beta_enabled if defined?(@memex_beta_enabled)
    @memex_beta_enabled = feature_enabled_globally_or_for_current_user?(:memex_beta)
  end

  def scoped_issue_timeline_enabled?
    return @scoped_issue_timeline_enabled if defined?(@scoped_issue_timeline_enabled)

    @scoped_issue_timeline_enabled = if logged_in?
      current_user.beta_feature_enabled?(:scoped_issue_timeline)
    else
      feature_enabled_globally_or_for_current_user?(:scoped_issue_timeline)
    end
  end

  def memex_issue_sidebar_enabled?
    return @memex_issue_sidebar_enabled if defined?(@memex_issue_sidebar_enabled)

    @memex_issue_sidebar_enabled = feature_enabled_globally_or_for_current_user?(:memex_issue_sidebar)
  end

  def async_dashboard_repositories_enabled?
    return @async_dashboard_repositories_enabled if defined?(@async_dashboard_repositories_enabled)

    @async_dashboard_repositories_enabled = if logged_in?
      feature_enabled_globally_or_for_current_user?(:async_dashboard_repositories)
    else
      false
    end
  end

  # Can user pin items to their dashboard?
  def user_dashboard_pins_enabled?
    return @user_dashboard_pins_enabled if defined?(@user_dashboard_pins_enabled)

    if logged_in?
      @user_dashboard_pins_enabled = current_user.beta_feature_enabled?("user_dashboard_pins")
    else
      @user_dashboard_pins_enabled = feature_enabled_globally_or_for_current_user?(:user_dashboard_pins)
    end
  end

  # Is gist-playground enabled for the user?
  def gist_playground_enabled?
    logged_in? && (
      # We don't currently have a good way of enabling a beta feature for all of
      # staff, so just pretend it's enabled for them.
      current_user.employee? ||
      current_user.beta_feature_enabled?("gist_playground")
    )
  end

  # Is the Gist Playground callout on gist.github.com enabled for the user?
  def gist_playground_callout_enabled?
    current_user_feature_enabled?(:gist_playground_callout) && gist_playground_enabled?
  end

  # define controller method and helper method for `#oauth_registered_callback_urls_enabled?` user feature flag
  def oauth_registered_callback_urls_enabled?
    logged_in? && current_user.oauth_registered_callback_urls_enabled?
  end

  # Public: Check whether privacy settings are enabled
  # See: https://github.com/github/github/issues/95897
  def third_party_analytics_setting_enabled?
    if GitHub.enterprise?
      false
    else
      GitHub.flipper[:third_party_analytics_setting].enabled?(current_user)
    end
  end

  # Public: Check whether we support a special repository per user for configuring things around
  # the site, such as aspects of their profile page for visitors.
  def config_repo_enabled?
    return @config_repo_enabled if defined?(@config_repo_enabled)
    @config_repo_enabled = feature_enabled_globally_or_for_current_user?(:config_repo)
  end

  # Marketing page feature flag-related methods
  def sales_chat_enabled?
    feature_enabled_globally_or_for_current_user?(:sales_chat)
  end

  def site_relaunch_enabled?
    feature_enabled_globally_or_for_current_user?(:site_relaunch)
  end

  def github_one_launched?
    feature_enabled_globally_or_for_current_user?(:site_github_one)
  end

  def maintainer_stories_enabled?
    feature_enabled_globally_or_for_current_user?(:maintainer_stories) || feature_enabled_via_current_visitor?(:maintainer_stories)
  end

  def site_packages_update_enabled?
    feature_enabled_globally_or_for_current_user?(:site_packages_update) || feature_enabled_via_current_visitor?(:site_packages_update)
  end

  def site_satellite20_codespaces_demo_enabled?
    feature_enabled_globally_or_for_current_user?(:site_satellite20_codespaces_demo) || feature_enabled_via_current_visitor?(:site_satellite20_codespaces_demo)
  end

  # Repository feature flag-related methods

  # Force raw/image rendering on a blob.  Needed for now because Git LFS objects
  # look like text files.
  def force_render_raw?
    current_repository ? current_repository.git_lfs_enabled? : false
  end

  def syntax_highlighted_diffs_enabled?
    # Staff can disable syntax-highlighted diffs with ?shd=0.
    disabled = preview_features? && params[:shd] == "0"
    !disabled
  end

  def instance_audit_log_enabled?
    GitHub.instance_audit_log_enabled? && logged_in? && current_user.site_admin?
  end

  def hook_deliveries_filter_enabled?
    logged_in? && GitHub.flipper[:hook_deliveries_filter].enabled?(current_user)
  end

  def integrations_directory_enabled?
    return false if GitHub.enterprise?

    feature_enabled_globally_or_for_current_user?(:integrations_directory)
  end

  def desktop_survey_hashed_id
    @desktop_survey_hashed_id ||= current_user_crc32(unique_string: "In app desktop survey")
  end

  def wikis_visible_by_default?
    if GitHub.flipper[:wikis_visible_by_default].enabled? && (current_repository.owner == current_user)
      true
    end
  end

  # Public: Is code navigation available?
  #
  # Code navigation is enabled for all users
  def aleph_code_navigation_available?
    GitHub.aleph_code_navigation_enabled? &&
      !mobile? &&
      current_repository &&
      current_repository.alephd_indexing_enabled?
  end

  # Public: Should the new LSP proxy for code navigation be used?
  def aleph_use_lsp_proxy?
    GitHub.flipper[:aleph_use_lsp_proxy].enabled? ||
      (current_repository && GitHub.flipper[:aleph_use_lsp_proxy].enabled?(current_repository)) ||
      current_user_feature_enabled?(:aleph_use_lsp_proxy)
  end

  # Public: Is the given language considered an aleph darkship language?
  #
  # Aleph language support is based on the existence of language feature flags.
  # For a language to be considered a darkship language requires that:
  # 1) The language does not have a corresponding aleph language feature flag (non-darkship feature flag).
  # 2) The language is matched against an existing aleph darkship language feature flag.
  #
  # Returns a Boolean.
  def aleph_darkship_language?(lang)
    return false if GitHub.flipper[GitHub::Aleph.convert_language_name(lang)].exist?
    GitHub.flipper[GitHub::Aleph.convert_darkship_language_name(lang)].exist?
  end


  # Check whether the Marketplace is enabled for current request
  #
  # Returns a Boolean value.
  def marketplace_enabled?
    !GitHub.enterprise?
  end

  def marketplace_pending_installations_notice_enabled?
    feature_enabled_globally_or_for_current_user?(:marketplace_pending_installations_notice)
  end

  def marketplace_pending_installations_enabled?
    feature_enabled_globally_or_for_current_user?(:marketplace_pending_installations)
  end

  def marketplace_pending_installations_required
    render_404 unless marketplace_pending_installations_enabled?
  end

  def github_apps_in_marketplace_search_enabled?
    feature_enabled_globally_or_for_current_user?(:github_apps_in_marketplace_search)
  end

  # Halts the request unless the Marketplace feature is enabled for the current request.
  #
  # This is intended for use as a `before` filter for use in conjunction with
  # the #marketplace_enabled? feature flag.
  #
  # Returns nothing.
  def marketplace_required
    render_404 unless marketplace_enabled?
  end

  def sponsors_required
    render_404 unless GitHub.sponsors_enabled?
  end

  def collaborative_editing_available?
    return @collaborative_editing_available if defined?(@collaborative_editing_available)
    @collaborative_editing_available = current_user_feature_enabled?(:collaborative_editing) && !GitHub.enterprise?
  end

  def collaborative_editing_enabled?
    collaborative_editing_available? && logged_in? && current_user.beta_feature_enabled?("collaborative_editing")
  end

  def feature_enabled_globally_or_for_current_user?(feature_name)
    GitHub.flipper[feature_name].enabled? || current_user_feature_enabled?(feature_name)
  end

  def responsive_pull_requests_enabled?
    return @responsive_pull_requests_enabled if instance_variable_defined?(:@responsive_pull_requests_enabled)

    @responsive_pull_requests_enabled = feature_enabled_globally_or_for_current_user?(:responsive_pull_requests)
  end

  def responsive_gists_enabled?
    return @responsive_gists_enabled if instance_variable_defined?(:@responsive_gists_enabled)

    @responsive_gists_enabled = feature_enabled_globally_or_for_current_user?(:responsive_gists)
  end

  def responsive_sidenav_enabled?
    return @responsive_sidenav_enabled if instance_variable_defined?(:@responsive_sidenav_enabled)

    @responsive_sidenav_enabled = feature_enabled_globally_or_for_current_user?(:responsive_sidenav)
  end

  def konami_enabled?
    current_user_feature_enabled?(:konami)
  end

  # Public: Determine if the viewer can see discussion posts that are tied to an organization
  # and not a particular team. github/profiles-and-dashboards
  # https://github.com/github/github/issues/112860
  def organization_discussions_enabled?
    return @organization_discussions_enabled if defined?(@organization_discussions_enabled)
    @organization_discussions_enabled =
      feature_enabled_globally_or_for_current_user?(:organization_discussions)
  end

  def feature_preview_enabled?
    return @feature_preview_enabled if defined?(@feature_preview_enabled)

    @feature_preview_enabled = !GitHub.enterprise?
  end

  def community_contributors_enabled?
    return @community_contributors_enabled if defined?(@community_contributors_enabled)

    @community_contributors_enabled = feature_enabled_globally_or_for_current_user?(:community_contributors)
  end

  # Public: Is Workspaces enabled?
  #
  # Returns a Boolean.
  def workspaces_enabled?
    return @workspaces_enabled if defined?(@workspaces_enabled)
    @workspaces_enabled = feature_enabled_globally_or_for_current_user?(:workspaces)
  end

  # Public: Is Workspaces waitlist sign-up enabled?
  #
  # Returns a Boolean.
  def workspaces_sign_up_enabled?
    return @workspaces_sign_up_enabled if defined?(@workspaces_sign_up_enabled)
    @workspaces_sign_up_enabled = !GitHub.enterprise? && feature_enabled_globally_or_for_current_user?(:workspaces_sign_up)
  end

  # Public: Is the setting for repository discussions enabled.
  # github/discussions
  #
  # Returns a Boolean.
  def discussions_enabled?
    return @discussions_enabled if defined?(@discussions_enabled)
    @discussions_enabled = current_repository&.discussions_enabled?
  end

  def merge_queue_enabled?
    return @merge_queue_enabled if defined?(@merge_queue_enabled)
    @merge_queue_enabled = GitHub.merge_queues_enabled? && current_repository&.merge_queue_enabled?
  end

  # Public: Are users able to see and modify showcased discussions
  # in repositories?
  # github/discussions, https://github.com/github/discussions/issues/611
  #
  # Returns a Boolean.
  def discussion_spotlights_enabled?
    if defined?(@discussion_spotlights_enabled)
      @discussion_spotlights_enabled
    else
      @discussion_spotlights_enabled = current_repository.discussion_spotlights_enabled?
    end
  end

  # Public: Is the setting for the discussions leaderboard enabled?
  # See https://github.com/github/discussions/issues/356
  # github/discussions
  #
  # Returns a Boolean.
  def discussions_leaderboard_enabled?
    if defined?(@discussions_leaderboard_enabled)
      @discussions_leaderboard_enabled
    else
      @discussions_leaderboard_enabled = GitHub.flipper[:discussions_leaderboard].enabled?(current_repository)
    end
  end

  # Public: Is the setting for the zeit specific overview enabled?
  # github/discussions
  #
  # Returns a Boolean.
  def discussions_zeit_enabled?
    if defined?(@discussions_zeit_enabled)
      @discussions_zeit_enabled
    else
      @discussions_zeit_enabled = GitHub.flipper[:discussions_zeit].enabled?(current_repository)
    end
  end

  # Public: Is the setting for the discussion categories enabled?
  # github/discussions
  #
  # Returns a Boolean.
  def discussion_categories_enabled?
    if defined?(@discussion_category_enabled)
      @discussion_category_enabled
    else
      @discussion_category_enabled = GitHub.flipper[:discussion_categories].enabled?(current_repository)
    end
  end

  def discussion_categories_edit_enabled?
    if defined?(@discussion_categories_edit_enabled)
      @discussion_categories_edit_enabled
    else
      @discussion_categories_edit_enabled = GitHub.flipper[:discussion_categories_edit].enabled?(current_repository)
    end
  end

  # Public: Is Reddit-style sorting enabled for repository discussions?
  # github/discussions
  #
  # See: https://github.com/github/discussions/issues/607
  #
  # Returns a Boolean.
  def discussion_reddit_style_sort_enabled?
    if defined?(@discussion_reddit_style_sort_enabled)
      @discussion_reddit_style_sort_enabled
    else
      @discussion_reddit_style_sort_enabled = GitHub.flipper[:discussion_reddit_style_sort].enabled?(current_repository)
    end
  end

  # Public: Can the user enable Git LFS blobs in archives?
  #
  # Returns a Boolean.
  def can_enable_lfs_in_archives?
    return @can_enable_lfs_in_archives if defined?(@can_enable_lfs_in_archives)
    @can_enable_lfs_in_archives = current_repository&.can_enable_lfs_in_archives?
  end

  # Public: Determine if the viewer can see the canary opt-in toggle from the
  # staff bar.
  #
  # https://github.com/github/github/pull/120077
  def canary_unicorn_web_enabled?
    GitHub.kubernetes_backend_available?
  end

  def private_pages_enabled?
    return @private_pages_enabled if defined?(@private_pages_enabled)

    @private_pages_enabled =
      feature_enabled_globally_or_for_current_user?(:private_pages)
  end

  def apple_iap_subscription_enabled?
    return @apple_iap_subscription_enabled if defined?(@apple_iap_subscription_enabled)

    @apple_iap_subscription_enabled =
      feature_enabled_globally_or_for_current_user?(:apple_iap_subscription)
  end

  def trending_repositories_spoken_language_filtering_enabled?
    return @trending_repositories_spoken_language_filtering_enabled if defined? @trending_repositories_spoken_language_filtering_enabled

    @trending_repositories_spoken_language_filtering_enabled =
      feature_enabled_globally_or_for_current_user?(:trending_repositories_spoken_language_filtering_enabled)
  end

  def accessibility_fixes_enabled?
    return @accessibility_fixes_enabled if defined?(@accessibility_fixes_enabled)

    @accessibility_fixes_enabled =
      feature_enabled_globally_or_for_current_user?(:accessibility_fixes)
  end

  private

  def flipper_session_id
    session[FlipperSession.session_key] ||= FlipperSession.generate_id
  end

  def flipper_session
    @flipper_session ||= FlipperSession.new(flipper_session_id)
  end

  # Internal: Calculate crc32 for the current_user or a unique user id from the
  # _octo cookie for logged_out requests.
  #
  # unique_string - String additional random variable so that different popups
  #                 can be run at the same time. See
  #                 http://watchout4snakes.com/wo4snakes/Random/RandomSentence
  #                 for generating random sentences that work well for this
  #                 value.
  #
  # Returns an Integer or nil if user could not be uniquely identified.
  def current_user_crc32(unique_string:)
    id = if logged_in?
      current_user.id.to_s
    elsif cookies[:_octo] =~ /^[^.]+\.[^.]+\.\d+\.\d+$/
      # Turn "GH1.2-3.XYZ.DEF" into "XYZ.DEF"
      cookies[:_octo].split(".")[2..3].join(".")
    end

    return nil unless id

    Zlib.crc32("#{id}#{unique_string}")
  end

  # Internal: Does the user have access to a feature based on the current visitor?
  # See app/models/user/device_id_actor.rb for more details on this method of enabling features.
  #
  # Returns a Boolean.
  def feature_enabled_via_current_visitor?(feature_name)
    GitHub.flipper[feature_name].enabled?(User::CurrentVisitorActor.from_current_visitor(current_visitor.octolytics_id))
  end

  # Returns true if the user is logged in and is included in the `exact_match_search` feature flag
  # and this is not a GHES installation. This does not check whether the user is opted into the
  # preview feature, nor weather the current repository is indexed by EMS nor whether EMS is offline.
  def eligible_for_exact_match_search?
    return @eligible_for_exact_match_search if instance_variable_defined?(:@eligible_for_exact_match_search)

    @eligible_for_exact_match_search = logged_in? &&
      current_user.exact_match_search_enabled?
  end

  # Returns true if the user is eligible for the `exact_match_search` feature and has opted into the
  # preview feature. Note that this does not check whether a repository is indexed by EMS nor whether
  # EMS is offline.
  def enrolled_in_exact_match_search?
    return @enrolled_in_exact_match_search if instance_variable_defined?(:@enrolled_in_exact_match_search)

    @enrolled_in_exact_match_search = eligible_for_exact_match_search? &&
      current_user.beta_feature_enabled?(:exact_match_search)
  end

  # Returns true if user is logged in and has the exact_match_ux_prototype
  # feature flag is enabled and user has opted in
  def enrolled_in_exact_match_ux_prototype?
    return false unless logged_in?
    return @enrolled_in_exact_match_ux_prototype if instance_variable_defined?(:@enrolled_in_exact_match_ux_prototype)
    @enrolled_in_exact_match_ux_prototype = current_user.beta_feature_enabled?(:exact_match_ux_prototype)
  end

  def audit_log_git_event_export_enabled?(subject)
    logged_in? &&
      GitHub.audit_log_export_enabled? &&
      GitHub.flipper[:audit_log_git_event_export].enabled?(subject)
  end
end
