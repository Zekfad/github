# frozen_string_literal: true

module ApplicationController::EmailTrackingDependency
  # This method facilitates email link tracking. A URL param of `email_source` is required in order
  # to track new emails
  def track_emails
    email_source = params["email_source"]

    # Notification emails links don't need to be tracked.
    return redirect_to(email_tracking_redirect_url) if email_source == "notifications"

    if email_source.present?
      GlobalInstrumenter.instrument("user.email_link_click",
        email_source: email_source,
        user: current_user,
        auto_subscribed: params["auto_subscribed"] == "true")

      safe_redirect_to(email_tracking_redirect_url)
    end
  end

  private

  def email_tracking_redirect_url
    query = Addressable::URI.parse(env["REQUEST_URI"]).query_values || {}
    # email_token is a legacy parameter from notifications emails that are no longer used, but need to be
    # stripped from URLs if they are followed from old emails https://github.com/github/github/pull/137773
    filtered_params = query.except("auto_subscribed", "email_source", "email_token")
    safe_url_for(filtered_params)
  end
end
