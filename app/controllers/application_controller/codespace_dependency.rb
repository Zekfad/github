# frozen_string_literal: true

module ApplicationController::CodespaceDependency
  extend ActiveSupport::Concern

  included do
    javascript_bundle :codespaces

    before_action :login_required
    before_action :require_feature
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless current_codespace

    current_codespace.plan&.owner || current_codespace.repository.organization || current_codespace.repository.owner
  end

  def require_feature
    render_404 unless Codespaces::Policy.enabled_for_user?(current_user)
  end

  def require_codespace
    return render_404 unless current_codespace
  end

  def verify_authorization
    return render_404 unless current_codespace.writable_by?(current_user)
  end

  def set_codespace_context
    Codespaces::ErrorReporter.push(codespace: current_codespace)
  end

  def exclude_firefox
    if helpers.should_disable_firefox?
      redirect_to codespaces_path
    end
  end

  def identifier
    raise "must be overridden in including class to use current_codespace"
  end

  def current_codespace
    return unless logged_in?
    return @current_codespace if defined?(@current_codespace)

    # TODO: remove all these other branches once we've verified they're no longer
    # being hit.
    @current_codespace = if codespace = current_user.codespaces.find_by(name: identifier)
      codespace
    elsif codespace = current_user.codespaces.find_by(guid: identifier)
      GitHub.dogstats.increment("codespaces.find_codespace_by_guid")
      codespace
    elsif codespace = current_user.codespaces.find_by(slug: identifier)
      GitHub.dogstats.increment("codespaces.find_codespace_by_slug")
      codespace
    end
  end
end
