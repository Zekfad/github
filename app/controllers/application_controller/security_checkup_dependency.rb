# frozen_string_literal: true

module ApplicationController::SecurityCheckupDependency
  extend ActiveSupport::Concern

  included do
    ConfirmedTokensQuery = parse_query <<-'GRAPHQL'
      query($providers: [String!]!) {
        viewer {
          delegatedRecoveryTokens(first: 100, providers: $providers) {
            ...Views::Settings::User::Security::DelegatedAccountRecoveryRow::Tokens
          }
        }
      }
    GRAPHQL
  end

  # Public: :before_action filter to enforce the display of the security checkup interstitial
  def require_security_checkup
    start = Time.now

    return true unless show_security_checkup?

    current_user.instrument_security_checkup("viewed")

    data = platform_execute(ConfirmedTokensQuery, variables: { providers: Darrrr.recovery_providers })

    render(
      "settings/security_checkup",
      layout: "session_authentication",
      locals: {
        return_to: request.url,
        tokens_connection: data.viewer.delegated_recovery_tokens,
      },
    )
  ensure
    set_has_recent_activity_cookie if logged_in? && !show_security_checkup?
    GitHub.dogstats.timing("security_checkup.overhead", (Time.now - start) * 1000)
  end

  def set_has_recent_activity_cookie
    cookies[:has_recent_activity] = { value: 1, expires: 1.hour.from_now }
  end

  private

  def internal_or_direct_referrer?
    return true unless request.referrer.present?

    Addressable::URI.parse(request.referrer).host == GitHub.host_name
  rescue Addressable::URI::InvalidURIError
    false
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def show_security_checkup?
    @show_security_checkup ||= begin
      internal_or_direct_referrer? &&
      request.format == :html &&
      !GitHub.enterprise? &&
      logged_in? &&
      !current_user.using_personal_access_token? &&
      !request.xhr? &&
      request.get? &&
      !mobile? &&
      !gist_request? &&
      cookies[:has_recent_activity].blank? &&
      current_user.security_checkup_due?
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
end
