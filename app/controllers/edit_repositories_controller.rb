# frozen_string_literal: true

class EditRepositoriesController < AbstractRepositoryController
  map_to_service :branch_protection_rule, only: [:branches, :update_default_branch]
  map_to_service :repo_admin, only: [:transfer, :abort_transfer, :set_visibility]
  map_to_service :repo_state, only: [:archive, :unarchive]
  map_to_service :repo_info, only: [:update_meta, :topics, :update_topics, :rename]
  map_to_service :repo_deletion, only: [:delete]
  map_to_service :security_alerts, only: [:update_alerts, :ensure_can_access_vulnerabilities, :alerts, :enable]
  map_to_service :security_products_experiences, only: [:security_analysis]

  include SecurityAnalysisSettingsHelper

  statsd_tag_actions only: [:update_topics, :update_meta]

  REPORTED_CONTENT_PER_PAGE = 50

  skip_before_action :cap_pagination, unless: :robot?

  skip_before_action :privacy_check, only: [:topics, :update_topics, :update_meta]

  before_action :login_required
  before_action :non_migrating_repository_required,
    only: [:delete, :transfer, :set_visibility, :unarchive]
  before_action :writable_repository_required,
    except: [:access, :delete, :keys, :options, :alerts, :tabs, :transfer, :set_visibility, :unarchive]
  before_action :metadata_permissions_required, only: :update_meta
  before_action :custom_tabs_only, only: [:tabs, :add_tab, :remove_tab]
  before_action :manage_topics_permissions_required, only: [:update_topics]
  before_action :wiki_settings_permissions_required, only: [:update_wiki_settings, :update_wiki_access]
  before_action :toggle_merge_types_permissions_required, only: :update_merge_settings
  before_action :projects_settings_permissions_required, only: :toggle_projects
  before_action :set_interaction_limits_permissions_required, only: [:interaction_limits, :set_interaction_limit]
  before_action :organization_owned_repos_only, only: [:update_wiki_settings, :toggle_projects]
  before_action :repo_access_management_enabled, only: [:member_suggestions]
  before_action do
    render "repositories/states/trade_controls_read_only" if current_repository.trade_controls_read_only?
  end
  before_action :add_spamurai_form_signals, only: [:update, :update_discussions_settings, :rename]

  before_action :add_csp_exceptions, only: [:options]
  CSP_EXCEPTIONS = {
    connect_src: [RepositoryImage.storage_s3_hostname],
  }

  before_action :sudo_filter, only: %i(
    add_team
    access
    delete
    set_visibility
    transfer
    update_member
    update_alerts
    change_anonymous_git_access
  )

  before_action :ensure_admin_access, except: [
    :options,
    :update_meta,
    :topics,
    :update_topics,
    :update_wiki_settings, :update_wiki_access,
    :update_merge_settings,
    :toggle_projects,
    :interaction_limits, :set_interaction_limit,
    :keys, :new_key
  ]
  before_action :ensure_can_access_vulnerabilities, only: [:alerts, :update_alerts]

  layout :repository_layout
  javascript_bundle :settings

  FgpOptionsQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        owner {
          ... on Organization {
            fineGrainedPermissionsSupported: planSupports(feature: FINE_GRAINED_PERMISSIONS)
          }
        }
        ...Views::EditRepositories::Pages::FgpOptions::Repository
        ...Views::Navigation::FgpEditRepository::Repository
      }
      ...Views::EditRepositories::AdminScreen::PagesSourceFgp::Fragment
    }
  GRAPHQL

  def options
    permission = current_repository.async_action_or_role_level_for(current_user).sync

    # Show the classic settings page for users with Admin permission,
    # but show FGP options for those with the Maintain role etc.
    if permission == :admin
      render "edit_repositories/pages/options", locals: {
        delete_confirmation_is_open: params[:confirm_delete].present?,
        repository: options_graphql_data.repository,
      }
    else
      variables = {
        owner: params[:user_id],
        name: params[:repository],
      }

      data = platform_execute(FgpOptionsQuery, variables: variables)
      return render_404 unless data.repository

      owner = data.repository.owner
      return render_404 unless owner.is_a?(PlatformTypes::Organization)
      return render_404 unless owner.fine_grained_permissions_supported

      render "edit_repositories/pages/fgp_options", locals: { repository: data.repository, data: data }
    end
  end

  def access
    if repo_access_management_enabled?
      query = params[:query]
      filter = params.fetch(:filter, :all).to_sym
      before = params[:before]
      after = params[:after]
      limit = params[:limit]

      access_list = ::RepositoryAccessList.new(
        repository: current_repository,
        current_user: current_user,
        query: query,
        filter: filter,
        before: before,
        after: after,
        limit: limit,
      )

      repository_roles = ::RepositoryMemberRoles.fetch(
        repository:   current_repository,
        current_user: current_user,
        members:      access_list.user_results,
        teams:        access_list.repository_teams,
      )

      respond_to do |format|
        format.html do
          if request.xhr? && (!pjax? || request.params["_pjax"] == "#repository-access-table")
            selected_members =
              if current_repository.in_organization?
                current_repository.organization.visible_users_for(current_user, actor_ids: params[:member_ids] || [])
              else
                selected_members_for_user_repo
              end
            return render_partial_view "edit_repositories/admin_screen/access_management/members_table_body", EditRepositories::Pages::ManagedAccessPageView,
              repository: current_repository,
              page: current_page,
              access_list: access_list,
              repository_roles: repository_roles,
              selected_members: selected_members
          else
            return render_template_view "edit_repositories/pages/managed_access", EditRepositories::Pages::ManagedAccessPageView,
              repository: current_repository,
              page: current_page,
              access_list: access_list,
              repository_roles: repository_roles
          end
        end
      end
    else
      return render_template_view "edit_repositories/pages/collaboration", EditRepositories::Pages::CollaborationPageView,
        repository: current_repository
    end
  end

  def member_suggestions
    respond_to do |format|
      format.html_fragment do
        render_partial_view "edit_repositories/admin_screen/access_management/member_suggestions",
          EditRepositories::AdminScreen::MemberSuggestionsView,
          organization: current_repository.organization,
          include_teams: true,
          repository: current_repository,
          query: params[:q]
      end
      format.html do
        render_partial_view "edit_repositories/admin_screen/access_management/member_suggestions",
          EditRepositories::AdminScreen::MemberSuggestionsView,
          organization: current_repository.organization,
          include_teams: true,
          repository: current_repository,
          query: params[:q]
      end
    end
  end

  BRANCHES_PAGE_SIZE = 100

  BranchProtectionRulesQuery = parse_query <<~'GRAPHQL'
    query($repositoryId: ID!, $first: Int, $last: Int, $after: String, $before: String) {
      node(id: $repositoryId) {
        ...Views::BranchProtectionRules::Index::Repository
      }
    }
  GRAPHQL

  def branches
    data = platform_execute(BranchProtectionRulesQuery, variables: {
      repositoryId: current_repository.global_relay_id,
    }.merge(graphql_pagination_params(page_size: BRANCHES_PAGE_SIZE)))

    render "branch_protection_rules/index", locals: {
      repository: data.node,
    }
  end

  def alerts
    redirect_to security_analysis_settings_path(current_repository)
  end

  def enable
    current_repository.transaction do
      current_repository.enable_content_analysis(actor: current_user)
      current_repository.enable_dependency_graph(actor: current_user)
      current_repository.enable_vulnerability_alerts(actor: current_user)
      if current_repository.fork? && current_repository.public?
        current_repository.send_manifests_to_dependency_graph
      end
    end

    redirect_to :back
  end

  def update_alerts
    Repository.transaction do
       current_repository.vulnerability_manager.replace_vulnerability_alert_restricted_users_and_teams(
        user_ids: Array(params[:vulnerability_user_ids]),
        team_ids: Array(params[:vulnerability_team_ids]),
      )
      current_repository.save!
    end

    flash[:notice] = "Alert options saved"
    redirect_to :back
  end

  def security_analysis
    render "edit_repositories/pages/security_analysis"
  end

  def keys
    @public_key = current_repository.public_keys.find_by_id(params[:id])
    render "edit_repositories/pages/keys"
  end

  def new_key
    render "edit_repositories/pages/new_key"
  end

  def update_discussions_settings
    return render_404 unless discussions_enabled?

    success = if params[:has_discussions] == "1"
      current_repository.enable_discussions(current_user)
    else
      current_repository.disable_discussions(current_user)
    end

    unless success
      flash[:error] = "Discussions could not be toggled at this time."
      return redirect_to(edit_repository_path(current_repository))
    end

    if request.xhr?
      # TODO: Should handle with live updates.
      respond_to do |format|
        format.html do
          render partial: "navigation/repository/main",
                 locals: { repository: current_repository, highlight: :repo_settings }
        end
      end
    elsif current_repository.errors.any?
      flash[:error] = "Error saving your changes: #{current_repository.errors.full_messages.to_sentence}"
      redirect_to edit_repository_path(current_repository)
    else
      flash[:notice] = "Repository settings saved."
      redirect_to edit_repository_path(current_repository)
    end
  end

  def update_merge_settings
    merge_types = Array(params[:merge_types])
    squash_selected = merge_types.include?("squash_merge")
    merge_selected = merge_types.include?("merge_commit")
    rebase_selected = merge_types.include?("rebase_merge")
    delete_branch_selected = merge_types.include?("delete_branch")

    begin
      current_repository.set_allowed_merge_types(current_user,
        rebase_allowed: rebase_selected,
        merge_allowed: merge_selected,
        squash_allowed: squash_selected,
        delete_branch_allowed: delete_branch_selected,
      )
    rescue Repository::MergeMethodError => e
      if request.xhr?
        return render status: 422, plain: "#{e.message} (#{e.reason})"
      else
        flash[:error] = e.message
        return redirect_to :back
      end
    end

    if request.xhr?
      head 200
    else
      flash[:notice] = "Repository settings saved."
      redirect_to :back
    end
  end

  def update_archive_settings
    return render_404 unless can_enable_lfs_in_archives?

    success = if params[:include_lfs_objects] == "1"
      current_repository.enable_lfs_in_archives(current_user)
    else
      current_repository.disable_lfs_in_archives(current_user)
    end

    unless success
      flash[:error] = "Archive settings could not be toggled at this time."
      return redirect_to(edit_repository_path(current_repository))
    end

    if request.xhr?
      # TODO: Should handle with live updates.
      respond_to do |format|
        format.html do
          render partial: "navigation/repository/main",
                 locals: { repository: current_repository, highlight: :repo_settings }
        end
      end
    elsif current_repository.errors.any?
      flash[:error] = "Error saving your changes: #{current_repository.errors.full_messages.to_sentence}"
      redirect_to edit_repository_path(current_repository)
    else
      flash[:notice] = "Repository settings saved."
      redirect_to edit_repository_path(current_repository)
    end
  end

  def update_wiki_settings
    current_repository.update(has_wiki: params[:has_wiki])

    if current_repository.errors.any?
      error_msg = "Error saving your changes: #{current_repository.errors.full_messages.to_sentence}"
      if request.xhr?
        return render status: 422, plain: error_msg
      else
        flash[:error] = error_msg
        return redirect_to :back
      end
    end

    if request.xhr?
      head 200
    else
      flash[:notice] = "Repository settings saved."
      redirect_to :back
    end
  end

  def update_wiki_access
    current_repository.update(wiki_access_to_pushers: params[:wiki_access_to_pushers])

    if current_repository.errors.any?
      error_msg = "Error saving your changes: #{current_repository.errors.full_messages.to_sentence}"
      if request.xhr?
        return render status: 422, plain: error_msg
      else
        flash[:error] = error_msg
        return redirect_to :back
      end
    end

    if request.xhr?
      head 200
    else
      flash[:notice] = "Repository settings saved."
      redirect_to :back
    end
  end

  def toggle_projects
    enabling_projects = params[:projects_enabled] == "1"

    if enabling_projects
      current_repository.enable_repository_projects(actor: current_user)
    else
      current_repository.disable_repository_projects(actor: current_user)
    end

    if request.xhr?
      head 200
    else
      flash[:notice] = "Projects #{enabling_projects ? 'enabled' : 'disabled'} for this repository."
      redirect_to :back
    end
  rescue Repository::CannotEnableProjectsError
    # The UI blocks this path from being hit, but just in case someone
    # tries to hack past the UI restriction, this avoids a 500 and gives
    # a helpful error message.
    error_message = "Projects cannot be enabled when the owning organization has projects disabled."

    if request.xhr?
      render status: 422, plain: error_message
    else
      flash[:error] = error_message
      redirect_to :back
    end
  end

  # Are you adding a new checkbox to the repo settings page?
  # Avoid adding it to this method; create a new endpoint, instead.
  # See https://github.com/github/github/pull/144368 for an example.
  def update
    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      Repository.transaction do
        ApplicationRecord::Domain::ConfigurationEntries.transaction do

          # some feature settings are attributes on Repository
          has_params = params.slice(:has_wiki,
                                    :has_issues,
                                    :wiki_access_to_pushers,
                                    :template).permit!

          current_repository.update!(has_params)

          current_repository.manage_security_settings(params, actor: current_user)

          if current_repository.can_participate_in_archive_program?
            if params[:archive_program_opt_out_enabled] == "1"
              current_repository.enable_archive_program_opt_out(actor: current_user)
            elsif params[:archive_program_opt_out_enabled] == "0"
              current_repository.disable_archive_program_opt_out(actor: current_user)
            end
          end

          begin
            if params[:projects_enabled] == "1"
              current_repository.enable_repository_projects(actor: current_user)
            elsif params[:projects_enabled] == "0"
              current_repository.disable_repository_projects(actor: current_user)
            end
          rescue Repository::CannotEnableProjectsError
            # The UI blocks this path from being hit, but just in case someone
            # tries to hack past the UI restriction, this avoids a 500 and gives
            # a helpful error message.
            flash[:error] = "Projects cannot be enabled when the owning organization has projects disabled."
            return redirect_to :back
          end

          if current_repository.private_repository_forking_configurable? && !current_repository.allow_private_repository_forking_disabled_by_inherited_policy?
            if params[:allow_private_repository_forking] == "1"
              current_repository.allow_private_repository_forking(actor: current_user)
            elsif params[:allow_private_repository_forking] == "0"
              current_repository.block_private_repository_forking(actor: current_user)
            end
          end

          if current_repository.can_enable_repository_funding_links?
            if params[:enable_repository_funding_links] == "1"
              current_repository.enable_repository_funding_links(actor: current_user)
            elsif params[:enable_repository_funding_links] == "0"
              current_repository.disable_repository_funding_links(actor: current_user)
            end
          end

          if params[:used_by_package_id]
            if package_id_belongs_to_repo?(params[:used_by_package_id])
              current_repository.set_used_by_package_id(actor: current_user, package_id: params[:used_by_package_id])
            else
              if request.xhr?
                return head :unprocessable_entity
              else
                flash[:error] = "Package selection could not be set."
                return redirect_to :back
              end
            end
          end
        end
      end
    end

    if request.xhr?
      # TODO: Should handle with live updates.
      respond_to do |format|
        format.html do
          render partial: "navigation/repository/main",
                 locals: { repository: current_repository, highlight: :repo_settings }
        end
      end
    elsif current_repository.errors.any?
      flash[:error] = "Error saving your changes: #{current_repository.errors.full_messages.to_sentence}"
      redirect_to :back
    else
      flash[:notice] = "Repository settings saved."
      redirect_to :back
    end
  end

  param_encoding :update_default_branch, :name, "ASCII-8BIT"

  def update_default_branch
    if current_repository.default_branch == params[:name]
      flash[:error] = "Default branch is already #{params[:name]}"
    elsif current_repository.update_default_branch(params[:name])
      flash[:notice] = "Default branch changed to #{params[:name]}"
    else
      flash[:error] = "Could not change default branch"
    end
    redirect_to :back
  end

  # Update the metadata for this repo. Is invoked via the 'Save Changes' button.
  def update_meta
    update_params = {
      description: params[:repo_description],
      homepage: params[:repo_homepage],
    }

    if params[:repo_sections].present?
      current_repository.update_sidebar_section_visibility(params[:repo_sections])
    end

    update_topics_ok = if topics = params[:repo_topics]&.reject(&:empty?)
      current_repository.update_topics(topics, user: current_user)
    else
      true
    end
    if update_topics_ok && current_repository.update(update_params)
      respond_to do |format|
        format.html do
          flash[:notice] = "Your repository details have been saved."
          redirect_to :back
        end
      end
    else
      flash[:error] = "Error saving your changes: #{current_repository.errors.full_messages.to_sentence}"
      redirect_to :back
    end
  end

  RepoTopicsQuery = parse_query <<-'GRAPHQL'
    query($name: String!, $owner: String!) {
      repository(name: $name, owner: $owner) {
        ...Views::Topics::List::Repository
      }
    }
  GRAPHQL

  def topics
    data = platform_execute(RepoTopicsQuery, variables: {
      name: params[:repository],
      owner: params[:user_id],
    })

    respond_to do |format|
      format.html do
        render partial: "topics/list", locals: { repository: data.repository }
      end
    end
  end

  UpdateTopicsQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateTopicsInput!) {
      updateTopics(input: $input) {
        invalidTopicNames
      }
    }
  GRAPHQL

  # Update the topics for this repo. It is invoked via AJAX when the user enters a new
  # topic.
  def update_topics
    unless params[:repo_topics]
      return head :ok if request.xhr?
      return redirect_to :back
    end

    topics = params[:repo_topics].reject { |name| name.blank? }

    data = platform_execute(UpdateTopicsQuery, variables: {
      input: {
        repositoryId: current_repository.global_relay_id,
        topicNames: topics,
      },
    })

    if data.errors.any?
      error_message = data.errors.messages.values.join(", ")

      if request.xhr?
        error = data.errors.details[:updateTopics]&.first
        if error["type"] == "VALIDATION"
          return render json: {
            invalidTopics: data.update_topics.invalid_topic_names,
            message: error_message,
          }, status: :unprocessable_entity
        end

        return head :unprocessable_entity
      end

      flash[:error] = error_message
    end

    return head :ok if request.xhr?
    redirect_to :back
  end

  def delete
    unless repo_name_verification_valid?(params[:verify])
      flash[:error] = "You must type the name of the repository to confirm."
      return redirect_to edit_repository_path(current_repository)
    end

    error = current_repository.cannot_delete_repository_reason(current_user)
    case error
    when :ofac_trade_restricted
      if current_repository.trade_controls_read_only? || current_repository.plan_owner.organization?
        flash[:trade_controls_organization_billing_error] = true
      else
        flash[:trade_controls_user_billing_error] = true
      end
      return redirect_to edit_repository_path(current_repository)
    when :cant_delete_repos_on_this_appliance
      flash[:error] = "Users cannot delete repositories on this appliance."
      return redirect_to edit_repository_path(current_repository)
    when :members_cant_delete_repositories
      flash[:error] = "Organization members cannot delete repositories."
      return redirect_to edit_repository_path(current_repository)
    when :not_ready_for_writes
      flash[:error] = "Repository cannot be deleted until it is done being created on disk."
      return redirect_to edit_repository_path(current_repository)
    end

    current_repository.remove(current_user)
    flash[:notice] = "Your repository \"#{current_repository.name_with_owner}\" was successfully deleted."
    redirect_to home_url
  end

  def archive
    unless repo_name_verification_valid?(params[:verify])
      flash[:error] = "You must type the name of the repository to confirm."
      return redirect_to edit_repository_path(current_repository)
    end

    current_repository.set_archived
    flash[:notice] = "Your repository \"#{current_repository.name_with_owner}\" was successfully archived."
    redirect_to :back
  end

  def unarchive
    unless repo_name_verification_valid?(params[:verify])
      flash[:error] = "You must type the name of the repository to confirm."
      return redirect_to edit_repository_path(current_repository)
    end

    current_repository.unset_archived
    flash[:notice] = "Your repository \"#{current_repository.name_with_owner}\" was successfully unarchived."
    redirect_to :back
  end

  def update_member
    if member = User.find_by_login(params[:member_login])
      begin
        action = find_role_name!(params[:permission])
      rescue ArgumentError, ActiveRecord::RecordNotFound
        flash[:error] = "Invalid permission specified."
        redirect_to :back
      else
        if current_repository.update_member(member, action: action, actor: current_user)
          head 200
        else
          render_404
        end
      end
    else
      render_404
    end
  end

  def remove_member
    @member = User.find_by_login(params[:member])
    current_repository.remove_member(@member, current_user)

    respond_to do |wants|
      wants.html do
        if request.xhr?
          head :ok
        else
          name           = (@member == current_user) ? "yourself" : @member.login
          flash_message = "Removed #{name} as a collaborator of #{current_repository.name_with_owner}"

          if current_repository.adminable_by?(current_user)
            if repo_access_management_enabled?
              redirect_to :back, flash: { notice: flash_message }
            else
              redirect_to edit_repository_path(current_repository), flash: { notice: flash_message }
            end
          else
            redirect_to "/", flash: { notice: flash_message }
          end
        end
      end
    end
  end

  def add_team
    team = if params[:team]
      current_repository.organization.teams.find(params[:team])
    elsif params[:team_slug]
      current_repository.organization.teams.find_by_slug(params[:team_slug])
    end

    if current_repository.teams.include?(team)
      return respond_to do |wants|
        wants.html { redirect_to repository_access_management_path(current_repository.owner, current_repository) }
        wants.json { render json: {error: error_message_for(Team::ModifyRepositoryStatus::DUPE)} }
      end
    end

    if current_repository.can_add_to_team?(team, adder: current_user)
      status = team.add_repository(current_repository, :pull)

      if status == Team::ModifyRepositoryStatus::SUCCESS
        respond_to do |wants|
          wants.html { redirect_to repository_access_management_path(current_repository.owner, current_repository) }
          wants.json do
            render json: {
              name: team.name,
               html: render_to_string(
                partial: "edit_repositories/admin_screen/team", locals: {
                  team: team,
                  organization: team.organization,
                  repository: current_repository,
                  action: team.async_most_capable_action_or_role_for(current_repository).sync,
                },
                formats: [:html]
              ),
            }
          end
        end
      else
        respond_to do |wants|
          wants.html { redirect_to repository_access_management_path(current_repository.owner, current_repository) }
          wants.json { render json: { error: error_message_for(status) } }
        end
      end
    else
      respond_to do |wants|
        wants.html { redirect_to repository_access_management_path(current_repository.owner, current_repository) }
        wants.json { render json: {error: "Team not found"} }
      end
    end
  end

  def remove_team
    # Note: we don't need to do any permissions-checking here. We already know
    # the user has admin on the repo (every action in this controller is
    # protected by an admin check on the repo, see ensure_admin_access in the
    # AbstractRepositoryController controller for more), and a user with admin
    # on a repo should be able to remove any teams from that repo, regadless of
    # the user's permissions with those teams.
    if @team = Team.find(params[:team])
      @team.remove_repository current_repository

      respond_to do |wants|
        wants.html do
          if request.xhr?
            head :ok
          else
            redirect_to repository_access_management_path(current_repository.owner, current_repository)
          end
        end
      end
    else
      respond_to do |wants|
        wants.html { redirect_to repository_access_management_path(current_repository.owner, current_repository) }
        wants.json { render json: { error: "Team not found" } }
      end
    end
  end

  def set_visibility
    unless repo_name_verification_valid?(params[:verify])
      flash[:error] = "You must type the name of the repository to confirm."
      return redirect_to edit_repository_path(current_repository)
    end

    if current_repository.fork? && current_repository.matches_root_visibility?
      return redirect_to edit_repository_path(current_repository)
    end

    # do not allow permission changes when a host is offline since we need to
    # sync the public permission bit.
    if !current_repository.online?
      flash[:error] = "Sorry, repository visibility cannot be changed at this time."
      return redirect_to edit_repository_path(current_repository)
    end

    current_repository.set_visibility(actor: current_user, visibility: params[:visibility])

    if current_repository.errors[:visibility].any?
      flash[:error] = current_repository.errors.full_messages.to_sentence
      return redirect_to edit_repository_path(current_repository)
    end

    respond_to do |wants|
      wants.html do
        if request.xhr?
          head :ok
        elsif params[:return_to].present?
          safe_redirect_to params[:return_to]
        else
          redirect_to edit_repository_path(current_repository)
        end
      end
    end
  end

  def rename
    if params[:new_name].to_s == current_repository.name || params[:new_name].blank?
      flash.now[:error] = "Repository name was not changed"
      return render("edit_repositories/pages/options",
                    locals: { repository: options_graphql_data.repository })
    end

    if current_repository.rename(params[:new_name], actor: current_user)
      redirect_to repository_path(current_repository)
    else
      if current_repository.errors.any?
        flash.now[:error] = current_repository.errors.full_messages.to_sentence
      else
        flash.now[:error] = "Couldn’t rename repository to #{params[:new_name]}"
      end

      return render("edit_repositories/pages/options",
                    locals: { repository: options_graphql_data.repository })
    end
  end

  def transfer_team_suggestions
    org = User.find_by(login: params[:new_owner])
    return render_404 unless org&.organization?

    team_ids = params[:team_ids] || []

    return render_template_view "edit_repositories/pages/team_selection", Orgs::Repositories::TeamSelectionView,
        repo: current_repository,
        organization: org,
        page: params[:page],
        team_ids: team_ids,
        ldap_mapped_teams_selectable: true
  end

  def transfer
    new_owner = User.find_by(login: params[:new_owner])

    transfer_request = RepositoryTransfer.new(
      repository: current_repository,
      requester: current_user,
      target: new_owner,
      requested_target: params[:new_owner],
    )

    if transfer_request.invalid?
      flash.now[:error] = transfer_request.errors.full_messages.to_sentence
      return render("edit_repositories/pages/options",
                    locals: { repository: options_graphql_data.repository })
    end

    if step_to_select_teams_for_transferred_repo?(new_owner)
      return render_template_view "edit_repositories/pages/team_selection", Orgs::Repositories::TeamSelectionView,
          repo: current_repository,
          organization: new_owner,
          team_ids: [],
          ldap_mapped_teams_selectable: true
    end


    if RepositoryTransfer.requires_transfer_request?(target: new_owner, requester: current_user, repository_visibility: current_repository.visibility)
      RepositoryTransfer.start current_repository, new_owner, current_user

      flash[:notice] = "Repository transfer to #{params[:new_owner]} requested"
      redirect_to edit_repository_path(current_repository)
    else

      team_ids = params[:team_ids] || ""
      team_ids = team_ids.split(",") unless team_ids.is_a?(Array)
      team_ids = team_ids.map(&:to_i).uniq
      target_teams = new_owner.teams.where(id: team_ids)
      RepositoryTransfer.transfer_immediately(current_repository, new_owner, current_user, target_teams)

      flash[:notice] = "Moving repository to #{new_owner}/#{current_repository.name}. This may take a few minutes."
      redirect_to dashboard_path
    end
  rescue
    flash.now[:error] = current_repository.errors.full_messages.to_sentence
    return render("edit_repositories/pages/options",
                  locals: { repository: options_graphql_data.repository })
  end

  def abort_transfer
    if current_repository.pending_transfer?
      current_repository.pending_transfer.destroy
    end

    redirect_to edit_repository_path(current_repository)
  end

  def tabs
    return render "edit_repositories/pages/tabs"
  end

  def add_tab
    tab = Tab.new(anchor: params[:anchor], url: params[:url])

    if tab.valid?
      current_repository.tabs << tab
    else
      flash[:error] = if tab.errors.any?
        tab.errors.full_messages.to_sentence
      else
        "Tab is invalid."
      end
    end

    respond_to do |wants|
      wants.html do
        if request.xhr?
          head :ok
        else
          redirect_to edit_repository_path(current_repository) + "/tabs"
        end
      end
    end
  end

  def remove_tab
    tab = current_repository.tabs.find(params[:tab])
    tab.destroy if tab

    respond_to do |wants|
      wants.html do
        if request.xhr?
          head :ok
        else
          redirect_to repository_tabs_path
        end
      end
    end
  end

  def change_anonymous_git_access
    return render_404 unless GitHub.anonymous_git_access_enabled?

    unless repo_name_verification_valid?(params[:verify])
      flash[:error] = "You must type the name of the repository to confirm."
      return redirect_to edit_repository_path(current_repository)
    end

    val = params[:value]&.to_s
    if current_repository.fork?
      flash[:error] = Repository::AnonymousGitAccess::FORK_ERROR
    elsif current_repository.anonymous_git_access_locked?(current_user)
      flash[:error] = "#{Repository::AnonymousGitAccess::LOCKED_ERROR}.  Please contact a site administrator."
    elsif val == "true"
      current_repository.enable_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access is now enabled."
    elsif val == "false"
      current_repository.disable_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access is now disabled."
    else
      flash[:error] = "Failed to change anonymous Git read access."
    end

    redirect_to edit_repository_path(current_repository)
  end

  InteractionLimitsQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        ...Views::EditRepositories::Pages::InteractionLimits::Repository
        ...Views::Navigation::FgpEditRepository::Repository
      }
    }
  GRAPHQL

  def interaction_limits
    return render_404 unless GitHub.interaction_limits_enabled? && !current_repository.private?

    permission = current_repository.async_action_or_role_level_for(current_user).sync

    data = platform_execute InteractionLimitsQuery, variables: {
      owner: params[:user_id],
      name: params[:repository],
    }

    render "edit_repositories/pages/interaction_limits", locals: { data: data.repository, permission: permission }
  end

  SetInteractionLimitQuery = parse_query <<-'GRAPHQL'
     mutation($input: SetRepositoryInteractionLimitInput!) {
      setRepositoryInteractionLimit(input: $input)
    }
  GRAPHQL

  def set_interaction_limit
    return render_404 unless GitHub.interaction_limits_enabled?

    input_variables = {
      repositoryId: current_repository.global_relay_id,
      limit: params[:interaction_setting],
    }

    results = platform_execute(SetInteractionLimitQuery, variables: { input: input_variables })

    if results.errors.any?
      flash[:error] = results.errors.all.values.flatten.to_sentence
    else
      flash[:notice] = "Repository interaction limit settings saved."
    end

    redirect_to :back
  end

  def repository_page
    return render_404 unless current_repository.unsupported_pages?
    render "edit_repositories/admin_screen/unpublish_page"
  end

  def unpublish_page
    current_repository.unpublish_page
    flash[:notice] = "Your repository page has been unpublished."
    redirect_to edit_repository_path(current_repository)
  end

  def reported_content
    return render_404 unless current_repository.can_access_tiered_reporting?(current_user)

    resolved_filter = params[:resolved_filter] == "RESOLVED" ? "RESOLVED" : "UNRESOLVED"

    latest_reports = AbuseReport.
      where(repository: current_repository, resolved: resolved_filter == "RESOLVED", show_to_maintainer: true).
      includes(:reported_content).
      not_spammy.
      most_recent_for_each_reported_content.
      reject(&:content_spammy?).
      paginate(per_page: REPORTED_CONTENT_PER_PAGE, page: current_page)

    reported_contents = latest_reports.map(&:reported_content).compact

    report_counts = AbuseReport.where(repository: current_repository, show_to_maintainer: true, reported_content: reported_contents).group(:reported_content_type, :reported_content_id).count

    comments_with_info = []

    if reported_contents.any?
      promises = reported_contents.each_with_index.map do |content, index|
        comment_with_info(content, report_counts, latest_reports[index])
      end
      comments_with_info = Promise.all(promises).sync
    end

    render "edit_repositories/pages/reported_content",
      locals: { current_repository: current_repository,
                resolved_filter: resolved_filter,
                report_content_enabled: current_repository.tiered_reporting_explicitly_enabled?,
                comments_with_info: comments_with_info,
                paginated_reports: latest_reports,
              }
  end

  def abuse_reporters
    return render_404 unless current_repository.can_access_tiered_reporting?(current_user) && request.xhr?

    reports = AbuseReport.where(reported_content_type: params[:type], reported_content_id: params[:id], show_to_maintainer: true).limit(REPORTED_CONTENT_PER_PAGE)
    return render_404 unless reports

    render partial: "community/repo_abuse_reporters",
      locals: { abuse_reports: reports },
      layout: false
  end

  # Returns a promise that resolves to a fully completed hash
  def comment_with_info(comment, report_counts, latest_report)
    Platform::Loaders::TopReportedAbuseReason.load(comment).then do |top_abuse_reason|
      {
        comment: comment,
        top_reason: top_abuse_reason,
        last_reported: latest_report.created_at,
        report_count: report_counts[[comment.class.to_s, comment.id]],
      }
    end
  end

  def toggle_tiered_reporting
    return render_404 unless current_repository.can_enable_tiered_reporting?(current_user)

    if params[:toggle_tiered_reporting] == "on"
      current_repository.enable_tiered_reporting(actor: current_user)
      flash[:notice] = "Content reporting enabled for this repository"
    else
      current_repository.disable_tiered_reporting(actor: current_user)
      flash[:notice] = "Content reporting disabled for this repository"
    end

    if request.xhr?
      respond_to do |format|
        format.html do
          render partial: "navigation/repository/main",
                 locals: { repository: current_repository, highlight: :tiered_reporting }
        end
      end
    else
     redirect_to reported_content_path
    end
  end

  private

  OptionsQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        ...Views::EditRepositories::Pages::Options::Repository
      }
    }
  GRAPHQL

  def options_graphql_data
    @options_graphql_data ||= platform_execute(OptionsQuery, variables: {
      owner: params[:user_id],
      name: params[:repository],
    })
  end

  helper_method :current_repository

  def branch_name_for_display
    params[:name].dup.force_encoding("UTF-8").scrub!
  end
  helper_method :branch_name_for_display

  # We allow access to the admin page in some cases where we don't allow access
  # to the code. To allow for this, we must override the
  # AbstractRepositoryController#ask_the_gatekeeper method for these cases
  #
  # See AbstractRepositoryController#ask_the_gatekeeper for more details
  def ask_the_gatekeeper
    repo = current_repository
    state = params[:fakestate] if real_user_site_admin?

    super if repo&.private? && (repo&.trade_restricted_by_owner? || current_user&.has_any_trade_restrictions?)

    # Allow disabled accounts to edit their repos
    unless (repository_specified? && repo && repo.disabled?) || state == "disabled"
      super
    end
  end

  # Override RepositoryControllerMethods#privacy_check.
  def privacy_check
    has_permissions = if GitHub.flipper[:custom_roles].enabled?(current_repository.owner) && GitHub.flipper[:edit_repositories_controller_privacy_check_fgp_friendly].enabled?(current_repository.owner)
      current_repository.async_can_view_repository_settings?(current_user).sync.allow?
    else
      science "edit_repositories_fgp_friendly_privacy_check" do |e|
        e.context user: current_user,
                  repo: current_repository

        e.use {
          permission = current_repository.async_action_or_role_level_for(current_user).sync
          [:maintain, :admin].include?(permission)
        }
        e.try {
          current_repository.async_can_view_repository_settings?(current_user).sync
        }
        e.compare { |control, candidate| control == candidate.allow? }

        # Reduce noise of this experiment by ignoring network failures
        e.ignore { |_control, candidate| Authzd.ignore_error_in_science?(candidate) }
      end
    end

    return if has_permissions
    return render "admin/locked_repo" if logged_in? && current_user.site_admin?
    render_404
  end

  # Internal: Get a user-readable error message describing the specified
  # Team::ModifyRepositoryStatus.
  #
  # modify_repository_status - Team::ModifyRepositoryStatus to get a message for.
  #
  # Returns a string or nil.
  def error_message_for(modify_repository_status)
    case modify_repository_status
    when Team::ModifyRepositoryStatus::DUPE
      "This team already has access to this repository."
    when Team::ModifyRepositoryStatus::NOT_OWNED
      "This team belongs to a different organization than this repository."
    when Team::ModifyRepositoryStatus::OWNERS
      "The Owners team already has access to all of this organization’s repositories."
    end
  end

  def step_to_select_teams_for_transferred_repo?(new_owner)
    return false unless new_owner&.organization?
    return false if params[:teams_selection]
    return false if new_owner.visible_teams_for(current_user).empty?

    new_owner.can_create_repository?(current_user, visibility: current_repository.visibility)
  end

  def repo_name_verification_valid?(repo_name)
    current_repository.name_with_owner.casecmp?(repo_name)
  end

  def custom_tabs_only
    render_404 unless GitHub.custom_tabs_enabled?
  end

  def metadata_permissions_required
    render_404 unless current_repository.can_edit_repo_metadata?(current_user)
  end

  def ensure_can_access_vulnerabilities
    if !(current_repository.vulnerability_alerts_enabled? && current_repository.owner.is_a?(Organization))
      render_404
    end
  end

  def manage_topics_permissions_required
    render_404 unless current_repository.can_manage_topics?(current_user)
  end

  def wiki_settings_permissions_required
    render_404 unless current_repository.can_toggle_wiki?(current_user)
  end

  def projects_settings_permissions_required
    render_404 unless current_repository.async_can_toggle_projects?(current_user).sync
  end

  def toggle_merge_types_permissions_required
    render_404 unless current_repository.async_can_toggle_merge_settings?(current_user).sync
  end

  def set_interaction_limits_permissions_required
    render_404 unless current_repository.async_can_set_interaction_limits?(current_user).sync
  end

  def organization_owned_repos_only
    render_404 unless current_repository.owner.organization?
  end

  def repo_access_management_enabled
    render_404 unless repo_access_management_enabled?
  end

  def repo_access_management_enabled?
    GitHub.flipper[:repo_access_management].enabled?(current_repository.owner) || GitHub.flipper[:repo_access_management].enabled?(current_user)
  end

  RepositoryPackagesQuery = parse_query <<-'GRAPHQL'
    query($name: String!, $owner: String!) {
      repository(name: $name, owner: $owner) {
        packages: dependencyGraphPackages {
          nodes {
            packageId
          }
        }
      }
    }
  GRAPHQL

  # Test if a package_id belongs to this repository. This is used to prevent someone from
  # injecting a package ID which doesn't belong to them into the Used By package setting.
  #
  # Returns bool
  def package_id_belongs_to_repo?(package_id)
    data = platform_execute(RepositoryPackagesQuery, variables: { name: current_repository.name, owner: current_repository.owner.login })
    package_ids = data.repository&.packages&.nodes&.collect { |pkg| pkg.package_id }
    package_ids.include?(package_id)
  end

  # Fetch all the members of the current repo.
  # If params[:member_ids] contains any ids, only return the ids which are collaborators of the repo
  #
  # Returns an ActiveRecord::Relation
  def selected_members_for_user_repo
    if params[:member_ids].present?
      current_repository.all_members.select { |member| params[:member_ids].include?(member.id) }
    else
      current_repository.all_members
    end
  end

  # Private: is the member an owner of the organization that owns the current repository?
  #
  # - member: a User
  #
  # Returns a Boolean
  def org_owner?(member)
    return false unless current_repository.in_organization?

    # an org-owner is a member who can admin the organization
    current_repository.organization.adminable_by?(member)
  end

  PLAN_PROTECTED_ROLES = ["triage", "maintain"]
  # returns a the provided role name if it exists as a valid role in the repository
  # it would raise if the role does not exists or is not supported by the current billing plan
  def find_role_name!(role_name)
    if Role.valid_system_role?(role_name)
      if PLAN_PROTECTED_ROLES.include?(role_name)
        raise ArgumentError, "Role: #{role_name}, is not supported for the current billing plan" unless current_repository.fine_grained_permissions_supported?
      end

      Repository.permission_to_action(role_name)
    else
      raise ArgumentError, "Role: #{role_name}, is not supported for the current billing plan" unless current_repository.custom_roles_supported?
      role = Role.find_by!(name: role_name, owner_id: current_repository.organization.id, owner_type: "Organization")
      role.name
    end
  end
end
