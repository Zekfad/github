# frozen_string_literal: true

class DeviceAuthorizationController < ApplicationController
  areas_of_responsibility :ecosystem_apps

  include ControllerMethods::Oauth
  include OauthHelper

  before_action :authorization_required
  before_action :ensure_feature_flag_enabled

  before_action :verify_user_code,                 only: [:request_access]
  before_action :find_device_authorization_grant!, only: [:authorize]
  before_action :set_application,                  only: [:request_access, :authorize]
  before_action :set_scopes,                       only: [:request_access, :authorize]

  before_action :check_eligibility,             only: [:request_access, :authorize]
  before_action :conditional_sudo_filter,       only: [:authorize]
  before_action :reject_dangerous_requests,     only: [:request_access, :authorize]
  before_action :reject_suspended_applications, only: [:request_access, :authorize]

  layout "device_authorization"

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:request_access, :authorize],
    if: :logged_in?,
    max: 50,
    ttl: 1.hour,
    key: :absolute_authorize_rate_limit_key,
    log_key: "device-authorization"

  def user_code_prompt
    render "device_authorization/user_code_prompt"
  end

  def request_access
    if show_sso_selection?(@application)
      more_than_one_target = (unauthorized_saml_organizations.count > 1)
      GitHub.dogstats.increment("device_authorization.sso.required", tags: ["multiple_targets:#{more_than_one_target}"])

      unauthorized_saml_organizations.each do |org|
        allow_external_redirect_after_post(provider: org.saml_provider)
      end

      locals = {
        application:                     @application,
        authorized_saml_organizations:   authorized_saml_organizations,
        form_data:                       captured_form_data_via_enforcement,
        return_to_url:                   request.url,
        unauthorized_saml_organizations: unauthorized_saml_organizations,
        user_code:                       @device_authorization.user_code,
      }

      return render("device_authorization/identity_management/sso", locals: locals, layout: "session_authentication")
    end

    @authorization = current_user.oauth_authorizations.find_by(application: @application)

    options = {
      authorization: @authorization,
      application: @application,
      form_submission_path: device_authorize_path,
      params: { user_code: @device_authorization.user_code },
      scopes: @scopes,
      session: session,
      rate_limited: false,
      include_device_warning: true,
      device_authorization: @device_authorization,
    }

    template, view_model = case @application
    when Integration
      options[:events]      = @application.default_permissions
      options[:permissions] = @application.default_permissions

      ["oauth/authorize_integration", Oauth::AuthorizeIntegrationView]
    else
      options[:unauthorized_saml_organizations] = unauthorized_saml_organizations
      options[:unauthorized_ip_whitelisting_organization_ids] = unauthorized_ip_whitelisting_organization_ids

      ["oauth/authorize", Oauth::AuthorizeView]
    end

    render_template_view template, view_model, options, { layout: "oauth_authorization" }
  end

  def authorize
    if oauth_access_authorized?
      options = {
        scope: @scopes,
        integration_version_number: params[:integration_version_number],
        user_session: user_session,
      }

      @access = @application.grant(current_user, options)
      @device_authorization.update!(oauth_access: @access)

      instrument_integration_listing

      log_data[:verification_code] = secret_last_eight(@access.code)
      redirect_to device_success_path
    else
      GitHub.dogstats.increment("device_authorization.conversion", tags: ["error:access_denied"])
      @device_authorization.update(access_denied: true)

      flash[:notice] = "You have denied #{@application.name} access"
      redirect_to dashboard_path
    end
  end

  def success
    render "device_authorization/success"
  end

  def failure
    render "device_authorization/failure"
  end

  private

  # Regardless of permissiveness, provide an absolute limit to the number of
  # authorize requests within a reasonable timeframe.
  def absolute_authorize_rate_limit_key
    "device-authorization-abs:authorize:#{current_user.id}:#{client_id}"
  end

  def client_id
    return @client_id if defined?(@client_id)
    @client_id = @application.key
  end

  def ensure_feature_flag_enabled
    render_404 unless current_user_feature_enabled?(:device_authorization_flow_ui)
  end

  def find_device_authorization_grant!
    @device_authorization = DeviceAuthorizationGrant.unclaimed.find_by!(user_code: submitted_user_code)
  end

  # We are handling SSO and enforcing external identity
  # sessions elsewhere in the code.
  #
  # This is due to the fact that there isn't a single SSO target.
  #
  # See DeviceAuthorizationController#request_access -> `if show_sso_selection?`
  def require_active_external_identity_session?
    false
  end

  # Deliberately opt out of IP allow list enforcement and do enforcement inline.
  def require_whitelisted_ip?
    false
  end

   # Deliberately opt out of two factor conditional access policy and handle enforcement inline.
  def two_factor_enforceable
    :no
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  def reject_suspended_applications
    return unless @application.suspended?
    redirect_to device_failure_path(reason: :application_suspended)
  end

  def set_application
    return @application if defined?(@application)

    @application = @device_authorization.application

    case @application
    when Integration
      log_data[:integration_id] = @application.id
    when OauthApplication
      log_data[:oauth_application_id] = @application.id
    end

    @application
  end

  def set_scopes
    @scopes = @device_authorization.scopes || []
  end

  def submitted_user_code
    return @submitted_user_code if defined?(@submitted_user_code)
    @submitted_user_code = params[:user_code].to_s.upcase
  end

  def verify_user_code
    device_authorization = DeviceAuthorizationGrant.find_by(user_code: submitted_user_code)

    case
    when device_authorization.nil?
      redirect_to device_failure_path(reason: :not_found)
    when device_authorization.access_denied?
      redirect_to device_failure_path(reason: :not_found)
    when device_authorization.expired?
      redirect_to device_failure_path(reason: :expired)
    when device_authorization.claimed?
      if device_authorization.user == current_user
        redirect_to device_success_path
      else
        redirect_to device_failure_path(reason: :not_found)
      end
    else
      @device_authorization = device_authorization
    end
  end
end
