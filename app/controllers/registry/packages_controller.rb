# frozen_string_literal: true

class Registry::PackagesController < AbstractRepositoryController

  before_action :check_packages_availability
  before_action :set_package, except: [:index, :commit]

  include Registry::QueryHelper

  PAGE_SIZE = 30

  layout :repository_layout

  def index
    if request.xhr? || pjax?
      render partial: "registry/packages/filtered_packages", locals: {
        owner: current_repository.owner,
        packages: packages,
        params: params,
      }
    else
      render "registry/packages/index", locals: {
        packages: packages,
      }
    end
  end

  def show
    return render_404 if params[:version] == "docker-base-layer"

    if params[:version].present?
      latest_version = @package.package_versions.find_by_version(params[:version])
    else
      latest_version = @package.latest_version
    end

    versions = @package.package_versions.order(id: :desc).limit(5)

    render_template_view "registry/packages/show", Registry::Packages::ShowView,
      {
        owner: current_repository.owner,
        repository: current_repository
      },
      locals: { package: @package, versions: versions, latest_version: latest_version }
  end

  def edit
    version = @package.package_versions.find(params[:version_id])

    return render_404 unless @package.repository.writable_by?(current_user)

    render_template_view "registry/packages/edit", Registry::Packages::ShowView,
      {
        owner: current_repository.owner,
        repository: current_repository,
      },
      locals: { package: @package, version: version }
  end

  def commit
    package_version = Registry::PackageVersion.find(params[:version_id])
    return render_404 unless package_version.package.repository.writable_by?(current_user)

    package_version.metadata.set(Registry::Metadatum::KEYS[:README], params[:package_version_body])
    return render_404 unless package_version.save

    flash[:notice] = "Package readme updated successfully."
    redirect_to package_path(current_repository.owner, current_repository.name, params[:id],
      version: package_version.version)
  end

  def versions
    versions = @package.package_versions.includes(:author).order(id: :desc)
    versions = versions.not_deleted unless current_repository.adminable_by?(current_user)

    render_template_view "registry/packages/versions",
      Registry::Packages::ShowView,
      {
        owner: current_repository.owner,
        repository: current_repository,
      },
      locals: {
        package: @package,
        versions: versions.paginate(page: current_page, per_page: PAGE_SIZE)
      }
  end

  private

  def set_package
    @package = Registry::Package.includes(:repository).find(params[:id])
    return render_404 unless @package.repository == current_repository
  end

  def query
    raw_query = params[:q]
    raw_query.strip if raw_query.is_a?(String)
  end

  def packages
    results, packages = packages_for_query(
      current_user: current_user,
      user_session: user_session,
      owner: current_repository.owner,
      repo_id: current_repository.id,
      query: query,
      package_type: ecosystem_param,
      visibility: visibility_param,
      sort: sort_param,
      page: current_page,
      per_page: PAGE_SIZE,
    )

    WillPaginate::Collection.create(current_page, PAGE_SIZE) do |pager|
      pager.replace(packages)
      pager.total_entries ||= results.total_entries
    end
  end

  def check_packages_availability
    render_404 unless PackageRegistryHelper.show_packages?
  end
end
