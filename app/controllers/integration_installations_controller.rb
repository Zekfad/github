# frozen_string_literal: true

class IntegrationInstallationsController < ApplicationController
  areas_of_responsibility :ecosystem_apps

  statsd_tag_actions only: [:create, :update]

  class InstallationError < StandardError; end

  include IntegrationInstallationsControllerStateHelper

  # We can safely skip the `perform_conditional_access_checks` filter
  # for the following actions:
  # - new: this action doesn't directly access an organization; it either
  #        allows the user to select an account to install on, or redirects
  #        to the appropriate installation endpoint where enforcement is
  #        handled.
  skip_before_action :perform_conditional_access_checks, only: %w(new)

  before_action :sanitize_referrer
  before_action :authorization_required, only: [:new, :permissions, :suggestions]
  before_action :sudo_filter, except: [:new, :permissions, :suggestions]
  before_action :find_integration
  before_action :find_target, only: [:permissions, :suggestions, :create]
  before_action :find_installation, only: [:edit, :update, :edit_permissions, :update_permissions]
  before_action :prevent_multiple_requests, only: [:permissions, :create]
  before_action :track_referrer, only: [:permissions]
  before_action :ensure_app_has_not_changed, only: [:create, :update, :update_permissions]

  before_action :set_setup_state_cookie, only: [:new, :permissions]

  MAX_REPOSITORIES_ON_INITIAL_INSTALL = Integration::InstallationService::DEFAULT_MAX_REPOS

  BACKOFF_RETRIES = {
    0 => 0.1,
    1 => 0.250,
    2 => 0.5,
    3 => 1.0,
    4 => 3.0,
  }

  def new
    if select_view.selection_needed?
      render_template_view "integration_installations/select_target", select_view
    elsif select_view.accounts.none?
      redirect_to gh_app_path(current_integration)
    else
      account = select_view.accounts.first

      if @integration.installed_on?(account)
        installation = @integration.installations.with_target(account).first
        redirect_to gh_settings_installation_path(installation)
      else
        redirect_to gh_app_installation_permissions_path(
          @integration,
          target_id: account.id,
        )
      end
    end
  end

  def permissions
    if current_installation.present?
      redirect_params = {}
      redirect_params[:repository_ids] = params[:repository_ids] || "" if params[:suggested_target_id]

      if current_installation.adminable_by?(current_user)
        return redirect_to gh_settings_installation_path(current_installation, redirect_params.merge({request_id: current_integration_installation_request}))
      end

      if current_integration.installable_on_by?(target: current_target, actor: current_user)
        return redirect_to gh_edit_app_installation_path(current_integration, current_installation, **redirect_params)
      end
    end

    render_template_view "integration_installations/permissions", IntegrationInstallations::SuggestionsView,
      target: current_target,
      integration: @integration,
      installation_request: current_integration_installation_request,
      session: session,
      referer: env["HTTP_REFERER"]
  end

  def suggestions
    respond_to do |format|
      format.html_fragment do
        render_partial_view "integration_installations/suggestions", IntegrationInstallations::SuggestionsView,
          target: current_target,
          integration: @integration,
          installation: current_installation,
          query: params[:q],
          skip_installed_on_check: params[:edit].present?,
          session: session
      end
    end
  end

  def create
    with_trilogy_dns_error_retry do
      result = Integration::InstallationService.perform(
        integration: @integration,
        target: current_target,
        actor: current_user,
        pending_request: current_integration_installation_request,
        params: installation_service_params,
        max_installable: MAX_REPOSITORIES_ON_INITIAL_INSTALL,
      )

      if result.failed?
        flash[:error] = result.error.to_s
        render_template_view "integration_installations/permissions",
                             IntegrationInstallations::SuggestionsView,
                             target: current_target,
                             integration: @integration,
                             session: session
        return
      end

      installation_result = result.installation_result
      request_result = result.request_result

      if installation_result&.success?
        version = @integration.versions.find_by_id(params[:version_id]) || @integration.latest_version

        if @integration.can_request_oauth_on_install?
          @access = @integration.grant(
            current_user,
            { integration_version_number: version.number },
          )
        end

        installer_redirect = Integration::InstallerRedirect.new(
          integration: @integration,
          installation: installation_result.installation,
          action: :install,
          return_to_url: session.delete(:return_to_after_installation),
          referring_sha: session.delete(:referring_sha_for_installation),
          oauth_access: @access,
          setup_state_cookie: IntegrationInstallation::SetupStateCookie.new(cookie_jar: cookies),
        )

        if installer_redirect.should_redirect?
          redirect_url = installer_redirect.url
          IntegrationInstallation::SetupStateCookie.delete(cookie_jar: cookies)

          render "integration_installations/setup_redirect",
                 layout: "redirect",
                 locals: { redirect_url: redirect_url, integration: @integration }
        else
          settings_path =
            if installation_result.installation.adminable_by?(current_user)
              gh_settings_installation_path(installation_result.installation)
            else
              gh_edit_app_installation_path(@integration, installation_result.installation)
            end
          redirect_to settings_path,
                      notice: "Okay, #{ @integration.name } was installed on the " \
                              "@#{ current_target } account."
        end
      elsif request_result&.valid?
        installation_request_redirect = Integration::InstallerRedirect.new(
          integration: @integration,
          action: :request,
          return_to_url: session.delete(:return_to_after_installation),
          referring_sha: session.delete(:referring_sha_for_installation),
          oauth_access: @access,
          setup_state_cookie: IntegrationInstallation::SetupStateCookie.new(cookie_jar: cookies),
        )

        if installation_request_redirect.should_redirect?
          redirect_url = installation_request_redirect.url
          IntegrationInstallation::SetupStateCookie.delete(cookie_jar: cookies)
          render "integration_installations/setup_redirect",
                 layout: "redirect",
                 locals: { redirect_url: redirect_url, integration: @integration }
        else
          redirect_to gh_new_app_installation_url(@integration),
                      notice: "A request to install #{ @integration.name } has been submitted on the @#{ current_target } account."
        end
      else
        error = InstallationError.new("Failed to install #{@integration.name} on #{@target}")
        error.set_backtrace(caller)
        Failbot.report!(error, app: "github-apps")

        redirect_to gh_new_app_installation_url(@integration),
                    error: "Something went wrong when trying to install #{ @integration.name } app, please try again"
      end
    end
  end

  # Accessible to non-admins for the installation target account, such as
  # repository admins.
  #
  # Target account admins use installation settings endpoints that are context
  # specific.
  def edit
    render_template_view "integration_installations/edit", IntegrationInstallations::ShowView,
      installation: current_installation,
      installation_request: current_integration_installation_request
  end

  # Accessible to non-admins for the installation target account, such as
  # repository admins.
  #
  # Target account admins use installation settings endpoints that are context
  # specific.
  def update
    result = Integration::InstallationService.perform(
      integration: current_integration,
      installation: current_installation,
      target: current_target,
      actor: current_user,
      params: installation_service_params.merge(install_target: "selected"),
      max_installable: MAX_REPOSITORIES_ON_INITIAL_INSTALL,
      editor: :repository_editor,
    )

    if result.failed?
      flash[:error] = result.error
      return edit
    end

    if current_installation.destroyed?
      redirect_to gh_app_installation_permissions_path(current_integration, target_id: current_target.id),
        notice: "Okay, #{ current_integration.name } was uninstalled from the @#{ current_target } account."
    else
      installer_redirect = Integration::InstallerRedirect.new(
        integration: current_integration,
        installation: current_installation,
        action: :update,
        return_to_url: session.delete(:return_to_after_installation),
        referring_sha: session.delete(:referring_sha_for_installation),
        setup_state_cookie: IntegrationInstallation::SetupStateCookie.new(cookie_jar: cookies),
      )

      if installer_redirect.should_redirect?
        if current_integration.can_request_oauth_on_install?
          @access = current_integration.grant(
            current_user,
            { integration_version_number: current_installation.version.number },
          )

          installer_redirect.oauth_access = @access
        end

        redirect_url = installer_redirect.url
        IntegrationInstallation::SetupStateCookie.delete(cookie_jar: cookies)

        render "integration_installations/setup_redirect",
          layout: "redirect",
          locals: { redirect_url: redirect_url, integration: current_integration }
      else
        redirect_to gh_edit_app_installation_path(current_integration, current_installation),
          notice: "Okay, #{ current_integration.name } was updated for the @#{ current_target } account."
      end
    end
  end

  def edit_permissions
    if current_installation.outdated?
      render_template_view "integration_installations/permissions_update_request",
        IntegrationInstallations::PermissionsUpdateRequestView,
        installation: current_installation
    else
      render_template_view "integration_installations/permissions_already_up_to_date",
        IntegrationInstallations::PermissionsUpdateRequestView,
        installation: current_installation
    end
  end

  def update_permissions
    return render_404 unless current_installation.outdated?

    permissions_result = current_installation.update_version(
      editor: current_user,
      version: current_integration.versions.find_by_id(params[:version_id]) || current_integration.latest_version,
    )

    if permissions_result.success?
      redirect_to gh_edit_app_installation_path(current_integration, current_installation),
        notice: "Okay, #{ current_integration.name } was updated for the @#{ current_target } account."
    else
      flash[:error] = permissions_result.error
      return edit_permissions
    end
  end

  private

  def access_denied
    args = {}.tap do |arg|
      arg[:return_to]   = request.fullpath
      arg[:integration] = current_integration.slug if current_integration
    end

    redirect_to login_path(args)
  end

  def select_view
    @select_view ||= Integrations::SelectTargetView.new(
      current_user: current_user,
      integration: current_integration,
      page: params[:page] || 1
    )
  end

  def track_referrer
    return unless params[:track_referrer] == "true" && request.post?
    session[:return_to_after_installation] = params[:installation_referrer]
    session[:referring_sha_for_installation] = params[:installation_referring_sha]
  end

  def installation_service_params
    params.to_unsafe_h.with_indifferent_access.slice :install_target,
                                                     :integration_installation,
                                                     :repository_ids,
                                                     :version_id
  end

  def current_integration
    find_integration
  end

  def current_installation
    @current_installation ||= current_integration.installations.with_target(current_target).first
  end

  def current_integration_installation_request
    if params[:suggested_target_id] || (current_installation && params[:repository_ids])
      repository_ids = Array(params[:repository_ids]).map { |value| ActiveModel::Type::Integer.new.cast(value) }.compact

      @installation_request ||= IntegrationInstallationRequest.ephemeral(
        integration: current_integration,
        target: current_target,
        repositories: find_requested_repositories(repository_ids),
      )
    end

    return @installation_request unless current_target.adminable_by? current_user

    @installation_request ||= IntegrationInstallationRequest.find_by(
      id:          params[:request_id],
      integration: current_integration,
      target:      current_target,
    )
  end

  def find_requested_repositories(repo_ids)
    return IntegrationInstallationRequest::ALL_REPOS unless repo_ids.present?

    available_repo_ids = repo_ids & (
      current_integration.installable_repository_ids_on_by(target: current_target, actor: current_user) +
      current_integration.requestable_repository_ids_on_by(target: current_target, actor: current_user)
    )

    Repository.find(available_repo_ids)
  end

  def current_target
    return @target if defined?(@target)
    target_id = params[:target_id] || params[:suggested_target_id]

    @target = if params["target_type"] == "Business"
      Business.find_by(id: target_id)
    else
      User.find_by(id: target_id)
    end
  end

  def find_integration
    @integration ||= Integration.from_owner_and_slug!(
      viewer:        current_user,
      slug:          params[:integration_id],
      user_login:    params[:owner],
      business_slug: params[:slug],
    )
  end

  def find_installation
    @current_installation = current_integration.installations.find_by!(id: params[:id])
    @target = current_installation.target # ensure current_target is set

    return redirect_to gh_settings_installation_path(current_installation) if @current_installation.adminable_by?(current_user)

    return render_404 unless current_installation.viewable_by?(current_user)
  end

  def prevent_multiple_requests
    request_to_target_exists = IntegrationInstallationRequest.where(
      requester:   current_user,
      integration: current_integration,
      target:      current_target,
    ).any?
    if request_to_target_exists
      flash[:error] = "A request to #{ current_target.name } already exists, cancel before submitting a new one or wait for an administrator to respond"
      redirect_to gh_new_app_installation_url(@integration)
    end
  end

  def find_target
    return render_404 if current_target.nil?

    installable = current_integration.installable_on_by?(target: current_target, actor: current_user)
    requestable = current_integration.requestable_on_by?(target: current_target, actor: current_user)

    return render_404 unless installable || requestable
  end

  def target_for_conditional_access
    target = current_target
    return target unless target.nil?

    # security violation: current_target does not compute the target when
    # "repository_ids" is passed through params, which leads to nil, and bypasses
    # SAML/Allowlist access controls.
    #
    # We return temporarily this to avoid the science mismatch that complains about
    # unexpected nil value until the owning team addresses the problem
    # see https://github.com/github/iam/issues/1623 for more info
    :no_target_for_conditional_access
  end

  def set_setup_state_cookie
    if params[:state] && current_integration
      IntegrationInstallation::SetupStateCookie.create(
        cookie_jar: cookies,
        data: {state: params[:state]},
        integration_id: current_integration.global_relay_id,
        target_id: params[:target_id] || params[:suggested_target_id],
      )
    end
  end

  # Internal: The path to redirect a user to if the action they are trying to
  # take should not be completed because the GitHub App has been updated since
  # they last loaded the page. This prevents a user from accepting
  # installations/permissions based on out of date information.
  #
  # action  - String. The controller action that was halted due to a changed
  #           App.
  #
  # Returns either a redirect_to or render.
  def app_changed_since_last_viewed(action)
    case action
    when "create"
      redirect_to gh_new_app_installation_url(current_integration)
    when "update"
      redirect_to gh_settings_installation_path(current_installation)
    when "update_permissions"
      edit_permissions
    end
  end

  def with_trilogy_dns_error_retry(&block)
    current_attempt = 0
    retry_metric_name = "integration_installations.create.retry_trilogy_error"
    begin
      block.call
    rescue Trilogy::Error => e
      if /TRILOGY_DNS_ERR/.match(e.message) && current_attempt < BACKOFF_RETRIES.size
        sleep_time = BACKOFF_RETRIES[current_attempt]
        sleep sleep_time
        current_attempt += 1
        GitHub.dogstats.increment(retry_metric_name)
        retry
      end

      raise e
    end
  end
end
