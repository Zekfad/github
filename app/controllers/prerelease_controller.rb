# frozen_string_literal: true

# Any bugs in here can be assigned to @nakajima
class PrereleaseController < ApplicationController
  areas_of_responsibility :user_growth

  include AccountMembershipHelper

  before_action :dotcom_required
  before_action :login_required

  def agreement
    SecureHeaders.append_content_security_policy_directives(request, { form_action: ["'self'"].concat(safe_redirect_hosts) })

    render_template_view "prerelease/agreement", Prerelease::AgreementView,
      membership: PrereleaseProgramMember.new, adminable_accounts: adminable_accounts,
      selected_account: account || adminable_accounts.first
  end

  def agree
    membership = PrereleaseProgramMember.new \
      member: account,
      actor: current_user

    if membership.save
      if params[:redirect_back]
        safe_redirect_to(params[:redirect_back], allow_hosts: safe_redirect_hosts)
      else
        redirect_to prerelease_agreement_thanks_path(redirect_options)
      end
    else
      flash[:error] = "Sorry that didn’t work."
      redirect_to prerelease_agreement_path(redirect_options)
    end
  end

  def thanks
    membership = find_membership
    if membership
      render_template_view "prerelease/thanks", Prerelease::AgreementView,
        membership: membership, adminable_accounts: adminable_accounts
    else
      redirect_to prerelease_agreement_path(redirect_options)
    end
  end

  private

  def find_membership
    return unless account
    PrereleaseProgramMember.find_by(member: account)
  end

  def redirect_options
    if account&.is_a?(Business)
      { business: account }
    elsif account&.is_a?(User)
      { account: account }
    end
  end

  def safe_redirect_hosts
    [
      GitHub.gist_host_name,
      GitHub.gist_playground_host_name,
    ].uniq.compact
  end

end
