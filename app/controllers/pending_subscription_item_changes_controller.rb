# frozen_string_literal: true

class PendingSubscriptionItemChangesController < ApplicationController

  before_action :login_required, :pending_change_required

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: RunPendingMarketplaceChangeInput!) {
      runPendingMarketplaceChange(input: $input) {
        pendingMarketplaceChange {
          __typename # a selection is grammatically required here
        }
      }
    }
  GRAPHQL
  def update
    input = {
      id: params[:id],
    }
    data = platform_execute(UpdateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:runPendingMarketplaceChange]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
      flash[:error] = data.errors.messages.values.join(", ")
    else
      flash[:notice] = "Successfully applied your pending change."
    end

    redirect_to :back
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: CancelPendingMarketplaceChangeInput!) {
      cancelPendingMarketplaceChange(input: $input) {
        pendingMarketplaceChange {
          __typename # a selection is grammatically required here
        }
      }
    }
  GRAPHQL
  def destroy
    input = {
      id: params[:id],
    }
    data = platform_execute(DestroyQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:cancelPendingMarketplaceChange]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
      flash[:error] = data.errors.messages.values.join(", ")
    else
      flash[:notice] = "Successfully cancelled your pending change."
    end

    redirect_to :back
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless pending_subscription_item_change
    user
  end

  private

  def pending_subscription_item_change
    @pending_subscription_item_change ||= Billing::PendingSubscriptionItemChange.find_by_id params[:id]
  end

  def pending_change_required
    head :not_found unless pending_subscription_item_change
  end

  delegate :user, to: :pending_subscription_item_change, allow_nil: true
end
