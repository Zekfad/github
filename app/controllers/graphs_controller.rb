# frozen_string_literal: true

class GraphsController < GitContentController
  areas_of_responsibility :traffic_graphs

  before_action :ensure_repository_not_empty, except: [
    :index,
    :languages,
  ]
  before_action :pushers_only, only: [:traffic, :traffic_data, :clone_activity_data]
  before_action :enforce_plan_supports_insights, only: [:code_frequency, :contributors, :traffic]
  layout :repository_layout

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:contributors_data],
    max: 100,
    ttl: 1.hour,
    key: :contributors_data_rate_limit_key,
    at_limit: :contributors_data_rate_limit_render

  def index
    redirect_to pulse_path
  end

  def languages
    redirect_to repository_path(current_repository), status: 301
  end

  def commit_activity
    handle_skipmc_for "commit-activity"
    render "graphs/commit_activity"
  end

  def commit_activity_data
    if data = GitHub::RepoGraph.commit_activity_data(current_repository, viewer: current_user, force_eventer: params[:force_eventer])
      render json: data
    else
      head 202
    end
  rescue GitHub::RepoGraph::UnusableDataError
    render json: { unusable: true }
  end

  def clone_activity_data
    override = { event_type: "clone" }
    counts = GitHub.dogstats.time "graph", tags: ["action:clones"] do
      GitHub.analytics.counts(options_for_analytics.merge(override)).data.slice("counts", "summary")
    end
    render json: counts
  rescue Octolytics::Error => error
    Failbot.report!(error)
    head 500
  end

  def code_frequency
    handle_skipmc_for "code-frequency"
    render "graphs/code_frequency"
  end

  def code_frequency_data
    if data = GitHub::RepoGraph.code_frequency_data(current_repository, viewer: current_user, force_eventer: params[:force_eventer])
      render json: data
    else
      head 202
    end
  rescue GitHub::RepoGraph::UnusableDataError
    render json: { unusable: true }
  end

  def contributors
    handle_skipmc_for "contributors"

    render "graphs/contributors"
  end

  def contributors_data
    if data = GitHub::RepoGraph.contributors_data(current_repository, viewer: current_user, force_eventer: params[:force_eventer])
      render json: data
    else
      head 202
    end
  rescue GitHub::RepoGraph::UnusableDataError
    render json: { unusable: true }
  end

  def traffic
    strip_analytics_query_string

    if "top_lists" == params[:partial] || "#top-domains" == request.headers["X-PJAX-Container"]
      view = Repositories::Graph::TrafficView.new(
        current_repository,
        current_user,
        params[:referrer],
        options_for_analytics,
      )

      respond_to do |format|
        format.html do
          if "top_lists" == params[:partial]
            render partial: "graphs/top_lists", locals: { view: view }
          elsif !view.show_referrer_paths?
            render partial: "graphs/referring_sites", locals: { view: view }
          else
            render partial: "graphs/referrer_paths", locals: { view: view }
          end
        end
      end
    else
      render "graphs/traffic"
    end
  end

  def traffic_data
    counts = GitHub.dogstats.time "graph", tags: ["action:traffic"] do
      GitHub.analytics.counts(options_for_analytics).data.slice("counts", "summary")
    end
    render json: counts
  rescue Octolytics::Error => error
    Failbot.report!(error)
    head 500
  end

  private

  def handle_skipmc_for(graph_name)
    if GitHub.cache.skip
      GitHub::RepoGraph.clear_cache(current_repository, graph_name)
    end
  end

  def ensure_repository_not_empty
    if current_repository.empty?
      return render_404
    end
  end

  def options_for_analytics
    # Graphs use the UTC+0 timezone to roll up a 'day'.  We want 14 total days
    # worth of data so we ask for the previous 6 days + today.
    from = 13.days.ago.utc.beginning_of_day
    # Round down to last complete hour
    to = Time.now.utc.change(min: 0)

    {
      bucket: "daily",
      event_type: "page_view",
      dimension: "repository_id",
      dimension_value: current_repository.id_for_analytics,
      from: from.iso8601,
      to: to.iso8601,
    }
  end

  def contributors_data_rate_limit_key
    "contributors-data:#{request.remote_ip}"
  end

  def contributors_data_rate_limit_render
    head 429
  end
end
