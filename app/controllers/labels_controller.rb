# frozen_string_literal: true

class LabelsController < AbstractRepositoryController

  include LabelEducationHelper

  before_action :pushers_only, except: [:index, :show]
  before_action :writable_repository_required, except: [:index, :show]
  layout :repository_layout, only: [:index]

  LABELS_PER_PAGE = 30

  def index
    query = params[:q]
    @labels = if query.present?
      search_labels(query)
    else
      list_labels
    end

    GitHub::PrefillAssociations.prefill_issue_counts(@labels)
    @label = current_repository.labels.new(color: Label.defaults.sample.color)
    render "labels/index", locals: { query: query }
  end

  def create
    name = params[:label][:name]
    color = params[:label][:color]
    description = params[:label][:description]
    path  = gh_labels_path(current_repository)

    color.try(:delete!, "#") # Strip any CSS-style hex codes
    label = current_repository.labels.create(name: name, color: color,
                                             description: description)

    if label.persisted?
      issue_id = params[:issue_id] ? params[:issue_id].to_i : nil
      label.instrument_creation(actor: current_user, context: params[:context],
                                issue_id: issue_id)
      track_issue_edits_from_project_board(edited_fields: ["labels"])
    end

    respond_to do |wants|
      wants.html do
        if request.xhr?
          if label.errors.any?
            json = label.errors.as_json(full_messages: true)
            json[:message] = label.errors.full_messages.join(", ")
            render json: json, status: :unprocessable_entity
          elsif params[:return_label_list_item]
            render partial: "issues/sidebar/label", locals: {label: label, selected: true}
          else
            render partial: "labels/label", object: label
          end
        else
          redirect_to(path)
        end
      end
    end
  end

  def update
    return head(:not_found) unless label = current_repository.labels.find_by_id(params[:id])

    color = params[:label][:color].try(:delete, "#") # Strip any CSS-style hex codes
    label.name = params[:label][:name] if params[:label][:name]
    label.color = color if color
    label.description = params[:label][:description]
    changes = label.changes

    label.save

    respond_to do |wants|
      wants.html do
        if request.xhr?
          if label.errors.any?
            json = label.errors.as_json(full_messages: true)
            json[:message] = label.errors.full_messages.join(", ")
            render json: json, status: :unprocessable_entity
          else
            track_issue_edits_from_project_board(edited_fields: ["labels"])
            render partial: "labels/label", object: label
          end
        else
          redirect_to :back
        end
      end
    end
  end

  def destroy
    label = current_repository.labels.find_by_id(params[:id])
    label.destroy if label
    if request.xhr?
      head(label ? 200 : 404)
    else
      redirect_to :back
    end
  end

  def preview
    label = current_repository.labels.find_by_id(params[:id]) if params[:id].present?
    label ||= current_repository.labels.new

    label.name = params[:name]
    label.description = params[:description]
    label.color = params[:color]

    unless label.valid?
      json = label.errors.as_json(full_messages: true)
      json[:message] = label.errors.full_messages.join(", ")
      return render(json: json, status: :unprocessable_entity)
    end

    respond_to do |format|
      format.html do
        render partial: "labels/preview", locals: { label: label }, layout: false
      end
    end
  end

  def show
    if label = current_repository.labels.find_by_name(params[:id])
      redirect_to url_for(controller: :issues, action: :index, labels: label.name)
    else
      render_404
    end
  end

  private

  def list_labels
    labels = current_repository.sorted_labels
    labels = case params[:sort]
    when "count-asc" then labels.sort_by(&:issues_count)
    when "count-desc" then labels.sort_by(&:issues_count).reverse
    when "name-desc" then labels.sort_by(&:name).reverse
    else
      params[:sort] = "name-asc"
      labels
    end
    labels.paginate(per_page: LABELS_PER_PAGE, page: current_page)
  end

  def search_labels(query)
    search_helper = Search::QueryHelper.new(query, "Labels", current_user: current_user,
                                            repo_id: current_repository.id, page: current_page,
                                            per_page: LABELS_PER_PAGE, remote_ip: request.remote_ip)
    search_results = search_helper.current.execute
    WillPaginate::Collection.create(current_page, LABELS_PER_PAGE) do |pager|
      pager.replace(search_results.map(&:label))
      pager.total_entries ||= search_results.total_entries
    end
  end
end
