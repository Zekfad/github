# frozen_string_literal: true

class OktaTeamSyncBetaMembershipsController < ApplicationController

  include AccountMembershipHelper

  SLUG = "okta_team_sync"

  before_action :dotcom_required, :login_required

  def signup
    selected_account = adminable_accounts.include?(account) ? account : adminable_accounts.first
    render_template_view "team_sync/okta_signup", TeamSync::OktaSignupAgreementView,
      membership: find_membership(selected_account) || PrereleaseProgramMember.new,
      adminable_accounts: adminable_accounts,
      selected_account: selected_account,
      redirect_back: params[:redirect_back]
  end

  def agree
    unless account.in?(adminable_accounts)
      flash[:error] = "Please select an organization or enterprise account."
      redirect_to okta_team_sync_beta_signup_path(redirect_options)
      return
    end
    membership = find_membership || EarlyAccessMembership.new(membership_params)
    if membership.save && save_pre_release_agremeent
      if params[:email_optin]
        NewsletterPreference.set_to_marketing(user: current_user, source: "okta-team-sync")
      end
      if params[:redirect_back]
        safe_redirect_to params[:redirect_back]
      else
        redirect_to okta_team_sync_beta_thanks_path(redirect_options)
      end
    else
      flash[:error] = "Sorry that didn’t work."
      redirect_to okta_team_sync_beta_signup_path(redirect_options)
    end
  end

  def thanks
    membership = find_membership
    if membership
      render_template_view "team_sync/okta_signup_thanks", TeamSync::OktaSignupAgreementView,
        membership: membership, adminable_organizations: adminable_organizations
    else
      redirect_to okta_team_sync_beta_signup_path(redirect_options)
    end
  end

  private

  def membership_params
    {
      member: account,
      actor_id: current_user.id,
      feature_slug: SLUG,
      survey: Survey.find_by(slug: SLUG)
    }
  end

  # This feature supports organizations and businesses, but not personal users
  def adminable_accounts
    adminable_businesses + adminable_organizations
  end

  def save_pre_release_agremeent
    # return if the user has already signed the pre-release agreement.
    return true if PrereleaseProgramMember.exists?(member: account, actor_id: current_user.id)
    # welcome new pre-release agreer!
    pre_release_membership = PrereleaseProgramMember.create(member: account, actor_id: current_user.id)
    pre_release_membership.save
  end

  def find_membership(candidate_account = account)
    return unless candidate_account
    EarlyAccessMembership.okta_team_sync_waitlist.find_by(member: candidate_account)
  end

  def redirect_options
    if account.is_a?(Business)
      { business: account.slug }
    else
      { account: account.login}
    end
  end
end
