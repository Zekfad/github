# frozen_string_literal: true

class UserStatusesController < ApplicationController
  areas_of_responsibility :user_profile

  skip_before_action :perform_conditional_access_checks,
    only: [:update, :emoji_picker, :org_picker, :show]

  before_action :login_required

  MemberStatusesQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $statusesPerPage: Int!, $statusesCursor: String) {
      node(id: $id) {
        ... on MemberStatusable {
          ...Views::UserStatuses::MemberStatuses::MemberStatusable
        }
      }
    }
  GRAPHQL

  STATUS_COUNT_FOR_LATER_PAGE = 15
  STATUS_COUNT_FOR_ORG_DASHBOARD = 6
  STATUS_COUNT_FOR_TEAM_PAGE = 3

  def member_statuses
    return render_404 unless params[:id]

    data = platform_execute(MemberStatusesQuery, variables: {
      id: params[:id],
      statusesPerPage: statuses_per_page,
      statusesCursor: params[:after],
    })
    return render_404 unless data.node

    respond_to do |format|
      format.html do
        render partial: "user_statuses/member_statuses",
               locals: { member_statusable: data.node, member_statusable_id: params[:id] }
      end
    end
  end

  def show
    respond_to do |format|
      format.html do
        render partial: "user_statuses/edit", locals: {
          truncate: params[:truncate] == "1",
          link_mentions: params[:link_mentions] == "1",
          compact: params[:compact] == "1",
          circle: params[:circle] == "1"
        }
      end
    end
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: ChangeUserStatusInput!) {
      changeUserStatus(input: $input) {
        __typename # a selection is grammatically required here
      }
    }
  GRAPHQL

  def update
    input = {
      "message" => params[:message],
      "emoji" => params[:emoji],
      "organizationId" => params[:organization_id].presence,
      "expiresAt" => params[:expires_at].presence,
      "limitedAvailability" => params[:limited_availability] == "1",
    }

    data = platform_execute(UpdateQuery, variables: { input: input })
    status = data.errors.any? ? :unprocessable_entity : :ok

    current_user.reload_user_status # Ensure latest status is used

    respond_to do |format|
      format.html do
        render partial: "user_statuses/edit", locals: {
          truncate: params[:truncate] == "1",
          link_mentions: params[:link_mentions] == "1",
          compact: params[:compact] == "1",
          circle: params[:circle] == "1"
        }, status: status
      end
    end
  end

  def emoji_picker
    respond_to do |format|
      format.html do
        render partial: "user_statuses/emoji_picker",
               locals: { status: current_user.user_status_when_not_expired, show_picker: params[:show_picker] == "1" }
      end
    end
  end

  OrgPickerQuery = parse_query <<-'GRAPHQL'
    query {
      viewer {
        ...Views::UserStatuses::OrgPicker::User
      }
    }
  GRAPHQL

  def org_picker
    data = platform_execute(OrgPickerQuery)
    viewer = data.viewer

    respond_to do |format|
      format.html do
        render partial: "user_statuses/org_picker", locals: { viewer: viewer }
      end
    end
  end

  private

  def statuses_per_page
    if params[:after]
      STATUS_COUNT_FOR_LATER_PAGE
    elsif params[:org] == "1"
      STATUS_COUNT_FOR_ORG_DASHBOARD
    else
      STATUS_COUNT_FOR_TEAM_PAGE
    end
  end

  def target_for_conditional_access
    Organization.find_by_login(params[:org]) || :no_target_for_conditional_access
  end
end
