# frozen_string_literal: true

class PackageDependencies::GlobalController < ApplicationController
  include PackageDependencies::SharedActions

  skip_before_action :perform_conditional_access_checks, only: [:index, :show, :security_graph, :licenses_graph, :license_menu_content]
  before_action  :login_required
  before_action :require_enabled_org, only: [:show]

  PACKAGE_PAGE_TABS = [:description, :dependencies, :contributors, :security, :dependents].freeze

  def index
    render_package_dependencies_index(
      owner_ids: selected_org_ids,
      user_organizations: enabled_user_organizations,
      unauthorized_saml_targets: enabled_unauthorized_saml_targets,
      unauthorized_ip_whitelisting_targets: enabled_unauthorized_ip_whitelisting_targets,
    )
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query(
      $packageName: String!,
      $packageManager: String!,
      $packageVersion: String!,
      $requirements: String!,
      $ownerIds: [Int!]!
      $dependentsTab: Boolean!,
      $dependentName: String,
      $dependentsFirst: Int,
      $dependentsLast: Int,
      $dependentsAfter: String,
      $dependentsBefore: String
    ) {
      packageRelease: dependencyGraphPackageReleases(
        packageName: $packageName,
        packageManager: $packageManager,
        requirements: $requirements,
        defaultToLatest: false,
        includeUnpublished: true,
        first: 1
      ) {
        nodes {
          ...Views::PackageDependencies::Show::PackageRelease
          ...Views::PackageDependencies::ShowSidebar::PackageRelease

          repository {
            id
            databaseId
            isPrivate
          }
        }
      }

      requester {
        ... on RequestingUser {
          repositoryPackageRelease: repositoryPackageReleases(
            first: 1,
            packageManager: $packageManager,
            packageName: $packageName,
            packageVersion: $packageVersion,
            ownerIds: $ownerIds,
            exactMatch: true,
            dependentName: $dependentName,
            dependentsFirst: $dependentsFirst,
            dependentsLast: $dependentsLast,
            dependentsBefore: $dependentsBefore,
            dependentsAfter: $dependentsAfter,
            includeDependentVersionCounts: true
          ) {
            nodes {
              ...Views::PackageDependencies::ShowDependents::PackageReleaseDependent @include(if: $dependentsTab)
              ...Views::PackageDependencies::ShowHeaderNav::PackageReleaseDependent
              ...Views::PackageDependencies::ShowSidebar::PackageReleaseDependent
            }
          }
        }
      }

      ...Views::PackageDependencies::ShowSidebar::Query
    }
  GRAPHQL

  def show_data(tab)
    variables = {
      packageManager: package_manager,
      packageName: package_name,
      requirements: "=#{package_version}",
      packageVersion: package_version,
      ownerIds: selected_org_ids,
    }

    variables[:dependentsTab] = tab == :dependents
    if variables[:dependentsTab]
      variables[:dependentName] = dependent_search
      pagination = graphql_pagination_params(page_size: PAGE_SIZE)
      variables[:dependentsFirst] = pagination[:first]
      variables[:dependentsLast] = pagination[:last]
      variables[:dependentsBefore] = pagination[:before]
      variables[:dependentsAfter] = pagination[:after]
    end

    platform_execute(ShowQuery, variables: variables)
  end

  def show
    unless selected_org_ids.empty?
      data = show_data(selected_tab)
      dependency_graph_timed_out = dependency_graph_timed_out?(data)
      dependency_graph_unavailable = dependency_graph_unavailable?(data)
      package_release = data.package_release&.nodes.first
      repository_package_release = data.requester&.repository_package_release&.nodes.first

      unless dependency_graph_timed_out || dependency_graph_unavailable
        return render_404 unless package_release.present? && repository_package_release.present?
      end
    end

    repository = release_repository(package_release)

    locals = {
      data: data,
      package_release: package_release,
      repository_package_release: repository_package_release,
    }

    if package_release && repository_package_release
      tab_data = load_selected_tab_data(package_release, repository)
      if tab_data
        dependency_graph_timed_out = dependency_graph_timed_out?(tab_data)
        dependency_graph_unavailable = dependency_graph_unavailable?(tab_data)
      end
      locals[:selected_tab_data] = tab_data
    end

    respond_to do |format|
      format.html do
        render_template_view "package_dependencies/show", PackageDependencies::ShowView, {
          parsed_query: parsed_query,
          raw_query: raw_query,
          selected_tab: selected_tab,
          enabled_orgs: enabled_user_organizations,
          selected_org_ids: selected_org_ids,
          package_name: package_name,
          package_version: package_version,
          package_manager: package_manager,
          dependent_search: dependent_search,
          repository: repository,
          unauthorized_saml_targets: enabled_unauthorized_saml_targets,
          unauthorized_ip_whitelisting_targets: enabled_unauthorized_ip_whitelisting_targets,
          dependency_graph_timed_out: dependency_graph_timed_out,
          dependency_graph_unavailable: dependency_graph_unavailable,
        }, locals: locals
      end
    end
  end

  def load_selected_tab_data(package_release, repository)
    case selected_tab
    when :description
      description_data(package_release.repository.id) if repository.present?
    when :dependencies
      dependencies_data(package_release.repository.id) if repository.present?
    when :security
      security_vulnerabilities_data
    end
  end

  def security_graph
    render_package_dependencies_security_graph(owner_ids: selected_org_ids)
  end

  def licenses_graph
    render_package_dependencies_licenses_graph(owner_ids: selected_org_ids)
  end

  def license_menu_content
    render_package_dependencies_license_menu_content
  end

  private

  ShowDependenciesQuery = parse_query <<-'GRAPHQL'
    query(
      $first: Int,
      $repositoryId: ID!,
      $dependenciesFirst: Int,
      $dependenciesAfter: String,
      $dependenciesPrefers: [String],
      $packageManager: String!,
      $packageName: String!,
    ) {
      node(id: $repositoryId) {
        ...Views::PackageDependencies::ShowDependencies::Manifests
      }
    }
  GRAPHQL

  def dependencies_data(repositoryId)
    platform_execute(ShowDependenciesQuery, variables: {
      first: 5,
      repositoryId: repositoryId,
      dependenciesFirst: PAGE_SIZE,
      dependenciesAfter: "",
      dependenciesPrefers: "",
      packageManager: package_manager,
      packageName: package_name,
      #TODO: fix these query params
    }).node
  end

  ShowDescriptionQuery = parse_query <<-'GRAPHQL'
    query(
      $repositoryId: ID!,
      $packageManager: String!,
      $packageName: String!,
    ) {
      node(id: $repositoryId) {
        ...Views::PackageDependencies::ShowDescription::Manifests
      }
    }
  GRAPHQL

  def description_data(repositoryId)
    platform_execute(ShowDescriptionQuery, variables: {
      repositoryId: repositoryId,
      packageManager: package_manager,
      packageName: package_name,
    }).node
  end

  SecurityVulnerabilitiesQuery = parse_query <<-'GRAPHQL'
    query(
      $packageManager: String!,
      $packageName: String!,
      $containsVersion: String!,
    ) {
      ...Views::PackageDependencies::ShowSecurity::SecurityVulnerabilities
    }
  GRAPHQL

  def security_vulnerabilities_data
    platform_execute(SecurityVulnerabilitiesQuery, variables: {
      packageManager: package_manager,
      packageName: package_name,
      containsVersion: package_version,
    })
  end

  def release_repository(release)
    repository = release&.repository
    return unless repository.present?

    if repository.is_private?
      Repository.where({
        id: current_user.associated_repository_ids(repository_ids: [repository.database_id]),
      }).find(repository.database_id)
    else
      Repository.find(repository.database_id)
    end
  end

  def require_enabled_org
    render_404 if enabled_user_organizations.empty? &&
      enabled_unauthorized_saml_targets.empty? &&
      enabled_unauthorized_ip_whitelisting_targets.empty?
  end

  def selected_tab
    @selected_tab ||= PACKAGE_PAGE_TABS.include?(params[:tab].to_s.to_sym) ? params[:tab].to_sym : :description
  end

  def selected_org_ids
    @selected_org_ids ||= enabled_user_organizations.select do |org|
      orgs_in_query.empty? || orgs_in_query.any? { |value| org.login.casecmp?(value) }
    end.map(&:id)
  end

  def orgs_in_query
    qualifier_values_in_query(:org)
  end

  def enabled_unauthorized_saml_targets
    @enabled_unauthorized_saml_targets ||= unauthorized_saml_targets.select do |org|
      org.dependency_insights_enabled_for?(current_user)
    end
  end

  def enabled_unauthorized_ip_whitelisting_targets
    @enabled_unauthorized_ip_whitelisting_targets ||= unauthorized_ip_whitelisting_targets.select do |org|
      org.dependency_insights_enabled_for?(current_user)
    end
  end

  def enabled_user_organizations
    @enabled_orgs ||= begin
      current_user.organizations
        .where.not(id: unauthorized_organization_ids)
        .to_a
        .keep_if { |org| org.dependency_insights_enabled_for?(current_user) }
    end
  end

  def graphql_errors(data)
    data.errors.all.messages.values.flatten
  end

  def dependency_graph_timed_out?(data)
    graphql_errors(data).include?("timedout")
  end

  def dependency_graph_unavailable?(data)
    graphql_errors(data).include?("unavailable")
  end
end
