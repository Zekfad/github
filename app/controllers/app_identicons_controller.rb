# frozen_string_literal: true

class AppIdenticonsController < ApplicationController
  areas_of_responsibility :avatars

  def show
    id_or_slug = params[:id]
    return head(:not_found) unless id_or_slug.present?

    app = case params[:type]
    when OauthApplication::IDENTICON_TYPE
      OauthApplication.find_by_id(id_or_slug.to_i)
    when Integration::IDENTICON_TYPE
      Integration.find_by(slug: id_or_slug)
    end
    return head(:not_found) unless app

    headers["Cache-Control"] = "public"
    respond_to do |format|
      format.svg do
        render "avatars/app_identicon", layout: false, locals: { identicon: app.identicon }
      end
    end
  end

  private

  # App identicons are public
  def require_conditional_access_checks?
    false
  end
end
