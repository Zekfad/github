# frozen_string_literal: true

class ContributePagesController < AbstractRepositoryController
  areas_of_responsibility :explore

  before_action :dotcom_required, only: [:show]
  layout :repository_layout

  def show
    if current_repository.public? && current_repository.maintained?
      render(
        "contribute_pages/show",
        locals: {
          repository: current_repository,
          issues: ExploreFeed::RepositoryGoodFirstIssue
            .fetch_by_repo_id(current_repository.id)
            .recommendable,
        },
      )
    else
      render_404
    end
  end
end
