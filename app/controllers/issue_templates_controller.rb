# frozen_string_literal: true

class IssueTemplatesController < AbstractRepositoryController

  before_action :login_required
  before_action :ask_the_gatekeeper
  before_action :pushers_only

  def edit
    branch = params[:branch] || current_repository.default_branch
    issue_templates = IssueTemplates.new(current_repository).templates

    respond_to do |format|
      format.html do
        render "issue_templates/edit", layout: "repository", locals: { branch: branch, issue_templates: issue_templates }
      end
    end
  end

  def preview
    issue_template = IssueTemplate.preview(
      current_repository,
      params[:current],
      GitHub::FailsafeJSON.load(params[:templates]),
    )

    render json: {
      html: render_to_string(partial: "issue_templates/template", formats: :html, locals: { template: issue_template }),
      markdown: issue_template.to_markdown,
      filename: issue_template.filename,
    }
  end

  def create_many
    templates = if params.has_key?(:templates)
      params.require(:templates).map { |t| t.permit!.to_hash }
    else
      []
    end

    issue_templates = IssueTemplates.new(current_repository)

    branch = params["commit-choice"] == "direct" ? current_repository.default_branch : params[:target_branch]

    commit_params = params.require(:commit)

    result = issue_templates.update(templates,
      updater: current_user,
      commit_title: commit_params[:title],
      commit_body: commit_params[:description],
      branch: branch,
      reflog_data: request_reflog_data("issue template builder"),
    )

    if result.ok?
      flash[:notice] = "Updated issue templates for this repository" unless result.pull_request
      redirect_to result.pull_request || current_repository
    else
      # Pre-receive hook notification messages include the output of the pre-receive hook.
      if result.hook_output
        flash[:hook_message] = result.error
        flash[:hook_out] = result.hook_output
      else
        flash[:error] = result.error
      end
      redirect_to edit_issue_templates_path(current_repository.owner, current_repository)
    end
  end

  def markdown_preview
    render html: Issue.new(body: params[:markdown]).body_html
  end
end
