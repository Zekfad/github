# frozen_string_literal: true

class Sponsors::SponsorablesController < ApplicationController
  include Sponsors::SharedControllerMethods

  before_action :non_banned_sponsorable_required
  before_action :sponsors_listing_required

  before_action :verify_visible_to_viewer
  before_action :non_spammy_user_required
  before_action :add_csp_exceptions, only: [:show]
  before_action :require_at_least_one_published_tier

  SPONSORS_PER_PAGE = 54

  # For editing billing information within a modal and sharing sponsorship to Twitter
  CSP_EXCEPTIONS = {
    img_src: [GitHub.paypal_checkout_url].freeze,
    connect_src: [GitHub.braintreegateway_url, GitHub.braintree_analytics_url].freeze,
    frame_src: [GitHub.zuora_payment_page_server].freeze,
    form_action: [Sponsors::TwitterButtonComponent::TWITTER_WEB_INTENT_URL].freeze,
  }.freeze

  ShowQuery = parse_query <<~'GRAPHQL'
    query($thisUserId: ID!) {
      sponsorable: node(id: $thisUserId) {
        ...Views::Sponsors::Sponsorables::Show::Sponsorable
      }
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: {
      thisUserId: sponsorable_id
    })

    instrument_view

    render "sponsors/sponsorables/show", locals: {
      ar_sponsorable: sponsorable,
      sponsorable: data.sponsorable,
      ar_listing: sponsorable_sponsors_listing,
      current_tier: sponsorship&.tier,
      editing: params[:editing] == "true",
      sponsoring: sponsorship.present?,
      sponsorships_as_sponsorable: sponsorships_as_sponsorable
        .paginate(page: current_page, per_page: SPONSORS_PER_PAGE),
      is_new_sponsorship: params[:success] == "true",
      previewing: previewing?,
      featured_users: featured_users,
      sponsorship: sponsorship,
      sponsor: sponsor,
      goal: sponsorable_sponsors_listing.active_goal,
    }
  end

  protected

  def require_at_least_one_published_tier
    unless sponsorable_sponsors_listing.default_tier
      flash[:notice] = "You need at least one published tier to view your GitHub Sponsors profile."
      redirect_to sponsorable_dashboard_tiers_path(sponsorable)
    end
  end

  private

  def sponsor
    return unless logged_in?
    return current_user unless current_user.corporate_sponsors_credit_card_enabled?
    return current_user unless params[:sponsor]
    return current_user unless current_user.potential_sponsor_logins.include?(params[:sponsor])

    return @sponsor if defined? @sponsor
    sponsor_from_params = User.find_by_login(params[:sponsor])
    return current_user if sponsor_from_params.invoiced?
    @sponsor = sponsor_from_params
  end

  def featured_users
    return SponsorsListingFeaturedItem.none unless sponsorable.organization?

    sponsorable.sponsors_listing.featured_users.preload(featureable: :profile)
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  def sponsorship
    return unless logged_in?
    return @sponsorship if defined? @sponsorship
    potential_sponsorship = sponsor.sponsorship_as_sponsor_for(sponsorable)
    @sponsorship = if potential_sponsorship&.active?
      potential_sponsorship
    else
      nil
    end
  end

  def instrument_view
    tracking_params = Sponsors::TrackingParameters.from_params(params)

    GlobalInstrumenter.instrument("sponsors.profile_viewed", {
      actor: current_user,
      sponsorable: sponsorable,
      source: tracking_params.source,
      referring_account: tracking_params.referring_account,
      sponsor: sponsor,
      origin: tracking_params.origin,
    })
  end

  def previewing?
    return true if params[:preview] == "true"
    return true unless sponsorable_sponsors_listing.approved?

    sponsor == sponsorable
  end
end
