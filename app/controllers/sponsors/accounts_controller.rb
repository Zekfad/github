# frozen_string_literal: true

class Sponsors::AccountsController < ApplicationController
  before_action :login_required
  before_action :check_ofac_sanctions

  layout "sponsors"

  def index
    sponsors_accounts = current_user.sponsors_enabled_accounts
    if sponsors_accounts.length == 1
      redirect_to_user sponsors_accounts.first
      return
    end

    render_template_view "sponsors/accounts/index",
      Sponsors::Accounts::IndexView, sponsors_accounts: sponsors_accounts
  end

  private

  def redirect_to_user(user)
    if user.sponsors_listing.present?
      redirect_to sponsorable_dashboard_path(user.login)
    else
      redirect_to sponsorable_waitlist_path(user.login)
    end
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
