# frozen_string_literal: true

class Sponsors::FeaturedController < ApplicationController
  def index
    accounts_max = params[:accounts_max].to_i if params[:accounts_max].present?
    accounts_max ||= SponsorsController::FEATURED_ACCOUNTS_SAMPLE_SIZE
    organizations_max = params[:organizations_max].to_i if params[:organizations_max].present?
    organizations_max ||= SponsorsController::FEATURED_ORGS_SAMPLE_SIZE

    render_template_view "sponsors/_featured_accounts", Sponsors::FeaturedAccountsView, {
      accounts_max: accounts_max.to_i,
      organizations_max: organizations_max.to_i,
    }, layout: false
  end

  private

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
