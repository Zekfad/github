# frozen_string_literal: true

class Sponsors::ButtonsController < ApplicationController
  include Sponsors::Embeddable

  def show
    render "sponsors/button/show", locals: { sponsorable: sponsorable }, layout: "sponsors/embed"
  end
end
