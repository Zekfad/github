# frozen_string_literal: true

class Sponsors::WebhooksController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_membership_required
  before_action :sponsors_listing_required

  def index
    hooks = Hook::StatusLoader.load_statuses(
      hook_records: sponsorable_sponsors_listing.hooks,
      parent: sponsorable_sponsors_listing,
    )

    render "sponsors/webhooks/index", locals: {
      sponsorable: sponsorable,
      hooks: hooks,
    }
  end

  def new
    render "sponsors/webhooks/new", locals: {
      sponsorable: sponsorable,
      hook: sponsorable_sponsors_listing.hooks.build(active: true),
    }
  end

  def create
    hook = sponsorable_sponsors_listing.hooks.build(name: "web")
    hook.track_creator(current_user)

    if hook.update(hook_params)
      flash[:notice] = "Okay, that hook was successfully created. We sent a ping payload to test it out! Read more about it at #{GitHub.developer_help_url}/webhooks/#ping-event."
      redirect_to sponsorable_dashboard_webhooks_path
    else
      flash[:error] = "There was an error setting up your hook: #{hook.errors.full_messages.to_sentence}"
      redirect_to :back
    end
  end

  def edit
    render "sponsors/webhooks/edit", locals: {
      sponsorable: sponsorable,
      hook: sponsorable_sponsors_listing.hooks.find(params[:id]),
    }
  end

  def update
    hook = sponsorable_sponsors_listing.hooks.find(params[:id])

    if hook.update(hook_params)
      flash[:notice] = "Okay, the hook was successfully updated."
      redirect_to sponsorable_dashboard_webhooks_path
    else
      flash[:error] = "There was an error updating your hook: #{hook.errors.full_messages.to_sentence}"
      redirect_to :back
    end
  end

  def destroy
    hook = sponsorable_sponsors_listing.hooks.find(params[:id])

    if hook.destroy
      flash[:notice] = "Okay, the hook was successfully deleted."
      redirect_to sponsorable_dashboard_webhooks_path
    else
      flash[:error] = "There was an error deleting your hook: #{hook.errors.full_messages.to_sentence}"
      redirect_to :back
    end
  end

  private

  def hook_params
    params.require(:hook).permit(
      :active,
      :content_type,
      { events: [] },
      :secret,
      :url,
    )
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
