# frozen_string_literal: true

class Sponsors::YourSponsorsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required

  layout "sponsors"

  def show
    published_tiers, retired_tiers = sponsorable_sponsors_listing
       .sponsors_tiers.where(state: [1, 2])
       .partition { |tier| tier.published? }

    render "sponsors/your_sponsors/show", locals: {
      sponsorable: sponsorable,
      sponsors_listing: sponsorable_sponsors_listing,
      sponsorships: sponsorships.paginate(page: current_page, per_page: 16),
      sponsorships_total_count: sponsorships.size,
      selected_tier: tier_scope,
      published_tiers: published_tiers,
      retired_tiers: retired_tiers,
      tier_subscription_counts: sponsorable.tier_subscription_counts
    }
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end

  def sponsorships
    @sponsorships ||= ::Sponsorship.all_active_as_sponsorable(
      sponsorable: sponsorable,
      tier_scope: tier_scope,
    ).preload(:sponsor, subscription_item: :subscribable)
  end

  def tier_scope
    return @tier_scope if defined?(@tier_scope)
    @tier_scope = sponsorable_sponsors_listing
      .sponsors_tiers
      .where(state: [1, 2])
      .find_by(id: params[:tier_id])
  end
end
