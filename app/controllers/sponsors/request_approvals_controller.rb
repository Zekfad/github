# frozen_string_literal: true

class Sponsors::RequestApprovalsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required

  def create
    if sponsorable_sponsors_listing.can_request_approval?
      sponsorable_sponsors_listing.request_approval!

      flash[:notice] = "Nice! Thanks for submitting your profile. You'll get an email "\
        "from us when your profile has been approved."
    else
      flash[:error] = "Your profile cannot be submitted for approval at this time."
    end

    redirect_to sponsorable_dashboard_path(sponsorable)
  end

  def destroy
    if sponsorable_sponsors_listing.can_cancel_approval_request?
      sponsorable_sponsors_listing.cancel_approval_request!
      flash[:notice] = "Okay, the approval request for this Sponsors profile has been canceled."
    else
      flash[:error] = "An approval request for this Sponsors profile could not be canceled."
    end

    redirect_to sponsorable_dashboard_path(sponsorable)
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
