# frozen_string_literal: true

class Sponsors::CommunityController < ApplicationController
  FEATURED_ACCOUNTS_SAMPLE_SIZE = 25
  FEATURED_ORGS_SAMPLE_SIZE = 10

  layout "site"

  def index
    render_template_view "sponsors/community/index", Sponsors::FeaturedAccountsView, {
      accounts_max: FEATURED_ACCOUNTS_SAMPLE_SIZE,
      organizations_max: FEATURED_ORGS_SAMPLE_SIZE,
    }
  end

  private

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
