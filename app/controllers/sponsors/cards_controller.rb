# frozen_string_literal: true

class Sponsors::CardsController < ApplicationController
  include Sponsors::Embeddable

  def show
    render "sponsors/card/show", locals: { sponsorable: sponsorable }, layout: "sponsors/embed"
  end
end
