# frozen_string_literal: true

class Sponsors::GoalsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :active_goal_required, only: [:edit, :update]

  # paginate completed goals
  COMPLETED_GOALS_PER_PAGE = 50

  def index
    completed_goals = sponsorable_sponsors_listing.goals.completed
      .preload(contributions: :sponsor)
      .paginate(page: current_page, per_page: COMPLETED_GOALS_PER_PAGE)
      .order(completed_at: :desc)

    render "sponsors/dashboard/goals/index", locals: {
      sponsorable: sponsorable,
      active_goal: active_goal,
      completed_goals: completed_goals,
    }
  end

  def new
    render "sponsors/dashboard/goals/new", locals: {
      sponsorable: sponsorable,
      goal: nil,
    }
  end

  def create
    goal = sponsorable.sponsors_listing.goals.new(goal_params)

    if goal.save
      instrument_event(action: :CREATED, goal: goal)

      flash[:notice] = "You successfully created a goal! 🎉"
      redirect_to sponsorable_dashboard_goals_path(sponsorable)
    else
      flash[:error] = goal.errors.full_messages.join(", ")

      render "sponsors/dashboard/goals/new", locals: {
        sponsorable: sponsorable,
        goal: goal,
      }
    end
  end

  def edit
    render "sponsors/dashboard/goals/edit", locals: {
      sponsorable: sponsorable,
      goal: active_goal,
    }
  end

  def update
    if active_goal.update(goal_params)
      instrument_event(action: :UPDATED, goal: active_goal)

      flash[:notice] = "Your active goal has been updated!"
      redirect_to sponsorable_dashboard_goals_path(sponsorable)
    else
      flash[:error] = active_goal.errors.full_messages.join(", ")

      render "sponsors/dashboard/goals/edit", locals: {
        sponsorable: sponsorable,
        goal: active_goal,
      }
    end
  end

  private

  def goal_params
    params.require(:goal).permit(:kind, :target_value, :description)
  end

  def active_goal
    sponsorable_sponsors_listing.active_goal
  end

  def active_goal_required
    return if active_goal.present?
    redirect_to new_sponsorable_dashboard_goal_path
  end

  def instrument_event(action:, goal:)
    GlobalInstrumenter.instrument("sponsors.goal_event", {
      actor: current_user,
      listing: sponsorable_sponsors_listing,
      goal: goal,
      action: action,
    })
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
