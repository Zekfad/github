# frozen_string_literal: true

class Sponsors::PayoutsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_membership_required
  before_action :sponsors_listing_required

  def show
    render "sponsors/payouts/show", locals: {
      sponsorable: sponsorable,
      payout: stripe_account&.latest_payout,
    }
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless sponsorable
    sponsorable
  end
end
