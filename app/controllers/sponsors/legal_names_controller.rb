# frozen_string_literal: true

class Sponsors::LegalNamesController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_membership_required

  def update
    if membership.update(membership_params)
      flash[:notice] = "Updated #{sponsorable.user? ? 'legal' : 'organization'} name to #{membership.legal_name}"
    else
      flash[:error] = membership.errors.full_messages.to_sentence
    end

    redirect_to sponsorable_dashboard_settings_path(sponsorable)
  end

  private

  def membership_params
    params.require(:sponsors_membership).permit(:legal_name)
  end

  def membership
    sponsorable.sponsors_membership
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
