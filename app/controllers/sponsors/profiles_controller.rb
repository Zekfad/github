# frozen_string_literal: true

class Sponsors::ProfilesController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required

  layout "sponsors"

  def show
    render "sponsors/profiles/show", locals: { sponsorable: sponsorable }
  end

  def update
    sponsors_listing = Sponsors::UpdateSponsorsListing.call(
      slug: sponsors_listing_slug,
      short_description: update_params[:short_description],
      full_description: update_params[:full_description],
      viewer: current_user,
    )

    unless sponsors_listing.set_featured_users(featured_users_update_params)
      flash[:error] = sponsors_listing.errors.full_messages.join(", ")
      return show
    end

    unless sponsors_listing.set_featured_repos(featured_repos_update_params)
      flash[:error] = sponsors_listing.errors.full_messages.join(", ")
      return show
    end

    unless update_featured_opt_in_params
      flash[:error] = sponsorable.sponsors_membership.errors.full_messages.join(", ")
      return show
    end

    GitHub.instrument("sponsors.sponsored_developer_profile_update")
    GlobalInstrumenter.instrument("sponsors.sponsored_developer_profile_update", {
      actor: current_user,
      user: sponsorable,
      bio: update_params[:full_description],
    })

    flash[:notice] = "Your profile has been updated"
    redirect_to sponsorable_dashboard_profile_path(sponsorable)
  rescue ActiveRecord::RecordNotFound,
         Sponsors::UpdateSponsorsListing::ForbiddenError
    render_404
  rescue Sponsors::UpdateSponsorsListing::UnprocessableError => e
    flash[:error] = e.message
    return show
  end

  private

  def update_params
    @update_params ||= params
      .require(:sponsors_listing)
      .permit(:full_description, :featured_state, :short_description,
        featured_users: [:id, :featureable_id, :description], featured_users_selection: [],
        featured_repos: [:id, :featureable_id, :description], featured_repos_selection: [])
  end

  def featured_users_update_params
    featured_items_params(
      limit: SponsorsListingFeaturedItem::FEATURED_USERS_LIMIT_PER_LISTING,
      params_key: :featured_users,
      selection_key: :featured_users_selection,
    )
  end

  def featured_repos_update_params
    featured_items_params(
      limit: SponsorsListingFeaturedItem::FEATURED_REPOS_LIMIT_PER_LISTING,
      params_key: :featured_repos,
      selection_key: :featured_repos_selection,
    )
  end

  def featured_items_params(limit: 1, params_key:, selection_key:)
    items_params = Array(update_params[params_key]).first(limit)
    return items_params unless update_params.key?(selection_key)

    selections = Array(update_params[selection_key]).reject(&:blank?).first(limit)

    items_to_keep = items_params.select { |item| selections.include?(item[:featureable_id]) }
    items_to_remove = (items_params - items_to_keep).map { |item| item.merge(_destroy: true) }
    items_to_add = (selections - items_to_keep.map { |item| item[:featureable_id] }).map { |item| { featureable_id: item } }

    items_to_keep + items_to_add + items_to_remove
  end

  def update_featured_opt_in_params
    featured_opt_in_params = { featured_description: update_params[:short_description] }

    if update_params[:featured_state]
      featured_opt_in_params[:featured_state] = find_featured_state
    end

    sponsorable.sponsors_membership.update(featured_opt_in_params)
  end

  def find_featured_state
    if update_params[:featured_state] == "0"
      :disabled
    elsif sponsorable.sponsors_membership.featured_active?
      :active
    else
      :allowed
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
