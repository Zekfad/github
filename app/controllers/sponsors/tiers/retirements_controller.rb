# frozen_string_literal: true

class Sponsors::Tiers::RetirementsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_membership_required
  before_action :sponsors_listing_required

  rescue_from Sponsors::RetireSponsorsTier::ForbiddenError, with: :render_404
  rescue_from Sponsors::RetireSponsorsTier::UnprocessableError do |error|
    flash[:error] = error.message
    redirect_to sponsorable_dashboard_tiers_path
  end

  def create
    Sponsors::RetireSponsorsTier.call(
      tier: tier,
      viewer: current_user,
    )

    flash[:notice] = "You've retired a tier."
    redirect_to sponsorable_dashboard_tiers_path
  end

  protected

  def tier
    @tier ||= sponsorable_sponsors_listing
      .sponsors_tiers
      .find(params[:tier_id])
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless sponsorable
    sponsorable
  end
end
