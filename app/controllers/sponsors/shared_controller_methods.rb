# frozen_string_literal: true

module Sponsors::SharedControllerMethods
  extend ActiveSupport::Concern

  included do
    before_action :sponsorable_required
    before_action :check_ofac_for_current_user
    before_action :check_ofac_for_sponsorable
    before_action :mask_analytics_location

    protected

    def sponsorable_required
      return if sponsorable.present?
      render_404
    end

    def mask_analytics_location
      override_analytics_location(request.path.sub(sponsorable.login, "<sponsorable>"))
      strip_analytics_query_string
      nil
    end

    def sponsorable_adminable_by_current_user_required
      return if sponsorable.adminable_by?(current_user)
      return if logged_in? && request.request_method == "GET" && current_user.can_admin_sponsors_listings?
      render_404
    end

    def sponsorable_sponsored_by_current_user_required
      return if sponsorable.already_sponsored_by?(current_user)
      render_404
    end

    def sponsors_membership_required
      return if sponsorable.sponsors_program_member?
      render_404
    end

    def organization_sponsorable_required
      return if sponsorable&.organization?
      render_404
    end

    def logged_in_user_required
      redirect_to_login(sponsorable_path(sponsorable)) unless logged_in?
    end

    def sponsorable_verified_email_required
      return if sponsorable.organization?
      return if sponsorable.verified_emails?

      flash[:notice] = "You must verify an email address for GitHub Sponsors!"
      redirect_to settings_user_emails_path
    end

    def sponsors_listing_required
      return if sponsorable_sponsors_listing

      if sponsorable.adminable_by?(current_user)
        redirect_to sponsorable_signup_path(sponsorable)
      else
        redirect_to user_path(sponsorable)
      end
    end

    def approved_sponsors_listing_required
      return if sponsorable_sponsors_listing&.approved?
      render_404
    end

    def stripe_account_required
      return if stripe_account.present?

      render_404
    end

    def fiscal_host_sponsorable_required
      return if sponsorable.fiscal_host?

      render_404
    end

    def verify_visible_to_viewer
      return if sponsorable_sponsors_listing.readable_by?(current_user)
      redirect_to user_path(sponsorable)
    end

    def non_banned_sponsorable_required
      render_404 if sponsorable&.sponsors_membership&.banned? && !current_user&.employee?
    end

    def non_spammy_user_required
      return if current_user&.employee?
      return if current_user == sponsorable

      render_404 if sponsorable.spammy?
    end

    def require_sponsorable_is_not_sponsor
      render_404 if sponsorable == sponsor
    end

    def check_ofac_for_current_user
      check_ofac_sanctions(target: current_user)
    end

    def check_ofac_for_sponsorable
      # we check the ofac sanctions for the current user in a separate call
      # no need to check again if the sponsorable is the current user
      return if logged_in? && current_user == sponsorable

      # allow site admins and biztools users to go through
      return if current_user&.can_admin_sponsors_listings?

      check_ofac_sanctions(target: sponsorable)
    end

    def sponsors_listing_slug
      sponsorable_sponsors_listing.slug
    end

    def sponsorable
      return @user if defined?(@user)

      user_login = params[:sponsorable_id] || params[:id]
      return unless user_login
      return unless GitHub::UTF8.valid_unicode3?(user_login)

      @user = User.includes(:sponsors_listing, :sponsors_membership).find_by(login: user_login)
    end

    def sponsorable_sponsors_listing
      @sponsorable_sponsors_listing ||= sponsorable.sponsors_listing
    end

    def sponsorable_id
      sponsorable.global_relay_id
    end

    def current_user_id
      current_user.global_relay_id
    end

    def stripe_account
      sponsorable_sponsors_listing&.stripe_connect_account
    end

    def sponsorable_if_organization
      sponsorable if sponsorable&.organization?
    end

    def sponsor
      return unless logged_in?
      return current_user unless current_user.corporate_sponsors_credit_card_enabled?
      return current_user unless sponsor_login
      return current_user unless current_user.potential_sponsor_logins.include?(sponsor_login)

      return @sponsor if defined? @sponsor
      sponsor_from_params = User.find_by_login(sponsor_login)
      return current_user if sponsor_from_params.invoiced?
      @sponsor = sponsor_from_params
    end

    def sponsorships_as_sponsorable
      sponsorable
        .sponsorships_as_sponsorable
        .preload(:sponsor)
        .active
        .ranked(for_user: current_user)
    end

    def sponsor_login
      params[:sponsor].presence
    end
  end
end
