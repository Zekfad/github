# frozen_string_literal: true

class Sponsors::ContactEmailsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  layout "sponsors"

  def update
    membership = sponsorable.sponsors_membership
    return render_404 unless membership.present?

    membership.contact_email_id = params[:contact_email_id]

    if membership.save
      flash[:notice] = "Updated contact email to #{membership.contact_email.email}"
    else
      flash[:error] = "Failed to set contact email: #{membership.errors.full_messages.to_sentence}"
    end

    if membership.accepted?
      redirect_to sponsorable_dashboard_settings_path(sponsorable)
    else
      redirect_to sponsorable_waitlist_path(sponsorable)
    end
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
