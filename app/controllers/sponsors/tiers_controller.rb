# frozen_string_literal: true

class Sponsors::TiersController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_membership_required
  before_action :sponsors_listing_required
  before_action :validate_amount, only: [:create, :update]
  before_action :validate_tier_count, only: [:create]

  rescue_from Sponsors::CreateSponsorsTier::ForbiddenError,
              Sponsors::UpdateSponsorsTier::ForbiddenError,
              Sponsors::PublishSponsorsTier::ForbiddenError,
              Sponsors::DeleteSponsorsTier::ForbiddenError,
              with: :render_404

  rescue_from Sponsors::CreateSponsorsTier::UnprocessableError,
              Sponsors::UpdateSponsorsTier::UnprocessableError do |error|
    flash[:error] = error.message
    redirect_to :back
  end

  rescue_from Sponsors::PublishSponsorsTier::UnprocessableError,
              Sponsors::DeleteSponsorsTier::UnprocessableError do |error|
    flash[:error] = error.message
    redirect_to sponsorable_dashboard_tiers_path
  end

  def index
    render_template_view "sponsors/tiers/index", Sponsors::Tiers::IndexView,
                         sponsorable: sponsorable,
                         tiers: sponsorable_sponsors_listing.sponsors_tiers.to_a
  end

  def new
    render_template_view "sponsors/tiers/index", Sponsors::Tiers::IndexView,
                         sponsorable: sponsorable,
                         tiers: sponsorable_sponsors_listing.sponsors_tiers.to_a,
                         show_new_form: true
  end

  def edit
    render_template_view "sponsors/tiers/index", Sponsors::Tiers::IndexView,
                         sponsorable: sponsorable,
                         tiers: sponsorable_sponsors_listing.sponsors_tiers.to_a,
                         edit_tier_id: params[:id]
  end

  def create
    tier = Sponsors::CreateSponsorsTier.call(
      listing_slug: params[:listing_slug],
      description: params[:description],
      monthly_price_in_cents: amount * 100,
      yearly_price_in_cents: amount * 12 * 100,
      viewer: current_user,
    )

    if publish_tier?
      params[:id] = tier.id
      return publish
    end

    flash[:notice] = %(You've created a draft tier! When you're ready, you can edit the tier and click the "Publish" button.)
    redirect_to sponsorable_dashboard_tiers_path
  end

  def update
    Sponsors::UpdateSponsorsTier.call(
      tier: tier,
      description: params[:description],
      monthly_price_in_cents: amount * 100,
      yearly_price_in_cents: amount * 12 * 100,
      viewer: current_user,
    )

    return publish if publish_tier?

    flash[:notice] = "You've updated a tier."
    redirect_to sponsorable_dashboard_tiers_path
  end

  def destroy
    Sponsors::DeleteSponsorsTier.call(
      tier: tier,
      viewer: current_user,
    )

    redirect_to sponsorable_dashboard_tiers_path
  end

  protected

  def tier
    @tier ||= sponsorable_sponsors_listing
      .sponsors_tiers
      .find(params[:id])
  end

  def publish_tier?
    params[:submit] == "publish_tier"
  end

  def amount
    @amount ||= params[:amount].to_i.abs
  end

  def validate_amount
    if amount.zero?
      flash[:error] = "Tier amount should be at least $1."
      return redirect_to :back
    end

    if amount > SponsorsTier::MAX_MONTHLY_SPONSORSHIP_AMOUNT
      flash[:error] = "Tier amounts can't be more than $#{SponsorsTier::MAX_MONTHLY_SPONSORSHIP_AMOUNT}."
      return redirect_to :back
    end
  end

  def validate_tier_count
    if sponsorable_sponsors_listing.remaining_tier_count.zero?
      flash[:error] = "Max number of published tiers reached."
      return redirect_to :back
    end
  end

  private

  def publish
    Sponsors::PublishSponsorsTier.call(
      tier: tier,
      viewer: current_user,
    )

    flash[:notice] = "You've published a tier."
    redirect_to sponsorable_dashboard_tiers_path
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
