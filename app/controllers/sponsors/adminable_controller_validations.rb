# frozen_string_literal: true

module Sponsors::AdminableControllerValidations
  extend ActiveSupport::Concern

  included do
    include Sponsors::SharedControllerMethods

    before_action :sponsorable_adminable_by_current_user_required
    before_action :sudo_filter
  end
end
