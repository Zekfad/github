# frozen_string_literal: true

class Sponsors::FiscalHostsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required
  before_action :fiscal_host_sponsorable_required

  layout "sponsors"

  def show
    render "sponsors/fiscal_hosts/show", locals: { sponsorable: sponsorable }
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
