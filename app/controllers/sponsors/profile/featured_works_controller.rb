# frozen_string_literal: true

class Sponsors::Profile::FeaturedWorksController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required

  def edit
    render partial: "sponsors/dashboard/profile/featured_work/edit", locals: {
      sponsorable: sponsorable,
      query: params[:query],
    }
  end

  def show
    render partial: "sponsors/dashboard/profile/featured_work/show", locals: {
      sponsorable: sponsorable,
      query: params[:query],
    }
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless sponsorable
    sponsorable
  end
end
