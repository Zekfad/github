# frozen_string_literal: true

class Sponsors::YourSponsors::ExportsController < ApplicationController
  include Sponsors::AdminableControllerValidations
  include GitHub::RateLimitedRequest

  before_action :sponsors_membership_required
  before_action :sponsors_listing_required

  rate_limit_requests key: :rate_limit_key, max: 10, ttl: 1.hour, at_limit: :rate_limit_render

  def create
    export = SponsorsListing::SponsorshipsExport.new(**export_params)

    if export.valid?
      export.start_export_job

      flash[:notice] = "You've started an export of your sponsors! " \
        "You'll receive an email to #{export.contact_email} shortly with the export attached."
    else
      flash[:error] = "There was an error starting the export: " \
        "#{export.errors.full_messages.to_sentence}"
    end

    redirect_to sponsorable_dashboard_your_sponsors_path(sponsorable)
  end

  private

  def export_params
    params
      .require(:export)
      .permit(:year, :month, :format)
      .merge(sponsors_listing: sponsorable.sponsors_listing)
      .to_h
      .symbolize_keys
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless sponsorable
    sponsorable
  end

  def rate_limit_key
    "#{self.class.to_s.underscore}.#{sponsorable.login}"
  end

  def rate_limit_render
    flash[:error] = "This Sponsors account has exceeded the export rate limit. " \
      "Please wait one hour before requesting another export."

    redirect_to sponsorable_dashboard_your_sponsors_path(sponsorable)
  end
end
