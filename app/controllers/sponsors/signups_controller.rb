# frozen_string_literal: true

class Sponsors::SignupsController < ApplicationController
  include Sponsors::SharedControllerMethods

  before_action :non_banned_sponsorable_required
  before_action :login_required
  before_action :sponsorable_adminable_by_current_user_required
  before_action :sponsorable_verified_email_required
  before_action :non_spammy_user_required
  before_action :sponsors_membership_required, only: [:create]

  ShowQuery = parse_query <<~'GRAPHQL'
    query($sponsorableId: ID!) {
      sponsorable: node(id: $sponsorableId) {
        ... on Actor {
          login
        }
        ... on Sponsorable {
          sponsorsListing
          isSponsorsProgramMember
          ...Views::Sponsors::Signups::Show::Sponsorable
        }
      }
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: { sponsorableId: sponsorable_id })
    if data.sponsorable.sponsors_listing
      redirect_to sponsorable_dashboard_path(data.sponsorable.login)
    elsif !data.sponsorable.is_sponsors_program_member?
      redirect_to sponsorable_waitlist_path(data.sponsorable.login)
    else
      billing_email = sponsorable.billing_email if sponsorable.organization?
      render "sponsors/signups/show", locals: {
        sponsorable: data.sponsorable,
        membership: sponsorable.sponsors_membership,
        possible_emails: sponsorable.emails.user_entered_emails.verified,
        billing_email: billing_email,
      }
    end
  end

  CreateMutation = parse_query <<~'GRAPHQL'
    mutation($input: CreateSponsorsListingInput!) {
      createSponsorsListing(input: $input) {
        listing {
          sponsorable {
            sponsorsMembership {
              isEligibleForStripeConnect
              fullBillingCountry
            }
          }
        }
      }
    }
  GRAPHQL

  def create
    data = platform_execute(CreateMutation, variables: {
      input: {
        name: params[:name],
        sponsorableId: sponsorable.global_relay_id,
        countryCode: params[:country],
        email: params[:email],
      },
    })

    if data.errors.any?
      error = "There was a problem processing your application: #{data.errors.messages.values.join(", ")}"
      return redirect_to sponsorable_signup_path(sponsorable),
             flash: { error: error }
    end

    membership = data.create_sponsors_listing.listing.sponsorable.sponsors_membership
    billing_country = membership.full_billing_country
    sponsorable_email = sponsorable.sponsors_membership.contact_email_address
    sponsorable_two_factor_enabled = if sponsorable.organization?
      sponsorable.two_factor_requirement_enabled?
    else
      sponsorable.two_factor_authentication_enabled?
    end

    eligible_for_stripe_connect = membership.is_eligible_for_stripe_connect
    if !eligible_for_stripe_connect
      SponsorsMailer.access_request(
        sponsorable_login: sponsorable.login,
        sponsorable_name: params[:name],
        sponsorable_email: sponsorable_email,
        sponsorable_country: billing_country,
        sponsorable_two_factor_enabled: sponsorable_two_factor_enabled,
      ).deliver_later
    end

    if sponsorable.organization?
      SponsorsMailer.notify_org_admins(
        org: sponsorable,
        actor: current_user,
      ).deliver_later
    end

    GitHub.instrument("sponsors.sponsored_developer_create", {
      user: sponsorable,
    })
    GlobalInstrumenter.instrument("sponsors.sponsored_developer_create", {
      user: sponsorable,
      action: :CREATED,
    })

    redirect_to sponsorable_dashboard_path(sponsorable)
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
