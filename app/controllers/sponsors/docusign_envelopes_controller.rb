# frozen_string_literal: true

require "docusign_esign"

class Sponsors::DocusignEnvelopesController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :docusign_enabled_required
  before_action :sponsors_membership_required
  before_action :sponsors_listing_required
  before_action :country_of_residence_required

  def create
    redirect_to_embed_view_url_with_retries
  rescue Sponsors::Docusign::CreateEnvelope::InvalidMembershipError => e
    Failbot.report(e, sponsorable_type: sponsorable.type, sponsorable_id: sponsorable.id)
    redirect_to :back, flash: { error: "There was a problem generating your documents." }
  rescue DocuSign_eSign::ApiError => e
    payload = {
      sponsorable_type: sponsorable.type,
      sponsorable_id: sponsorable.id,
      app: "github-external-request",
    }
    if error_body = e.response_body
      payload[:error_code] = JSON.parse(error_body)["message"]
    end
    Failbot.report(e, payload)
    increment_error_stats

    redirect_to :back, flash: { error: e.message }
  rescue ActiveRecord::RecordInvalid => e
    Failbot.report(e,
      sponsorable_type: sponsorable.type,
      sponsorable_id: sponsorable.id,
      app: "github-external-request",
    )
    redirect_to :back, flash: { error: e.message }
  end

  private

  DOCUSIGN_RETRIES = 3

  def redirect_to_embed_view_url_with_retries
    envelope = Sponsors::Docusign::CreateEnvelope.call \
      sponsorable: sponsorable,
      country_code: country_of_residence
    embed_view = Sponsors::Docusign::CreateEmbedView.call \
      sponsorable: sponsorable,
      envelope: envelope,
      return_url: sponsorable_dashboard_url(sponsorable)
    redirect_to embed_view.url
  rescue Sponsors::Docusign::CreateEnvelope::InvalidMembershipError,
          DocuSign_eSign::ApiError,
          ActiveRecord::RecordInvalid => e
    @retries ||= 0
    if @retries < DOCUSIGN_RETRIES
      @retries += 1
      retry
    else
      @retries = 0
      raise
    end
  end

  def docusign_enabled_required
    render_404 unless sponsorable&.sponsors_docusign_enabled?
  end

  def country_of_residence_required
    if country_of_residence.nil?
      redirect_to sponsorable_dashboard_settings_path(sponsorable),
        flash: { error: "Please select a country or region of residence" }
    end
  end

  def country_of_residence
    @country_of_residence ||= sponsorable.sponsors_membership.country_of_residence
  end

  def increment_error_stats
    GitHub.dogstats.increment("docusign",
      tags: [
        "source:sponsors",
        "error:create_envelope"
      ]
    )
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
