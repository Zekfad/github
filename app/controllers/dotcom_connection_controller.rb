# frozen_string_literal: true

class DotcomConnectionController < ApplicationController
  skip_before_action :perform_conditional_access_checks
  before_action :enterprise_required
  before_action :disable_for_fedramp_private_instance

  def webhooks
    render plain: "OK"
  end
end
