# frozen_string_literal: true

class AboutController < ApplicationController
  areas_of_responsibility :user_growth

  statsd_tag_actions only: :careers

  YOUTUBE_HOST = "www.youtube.com"

  skip_before_action :perform_conditional_access_checks
  before_action :add_csp_exceptions, only: [:press_universe, :press_satellite]
  CSP_EXCEPTIONS = { frame_src: [YOUTUBE_HOST] }

  layout "site"

  def index
    render "about/index"
  end

  JobsQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::About::Careers::Jobs
    }
  GRAPHQL

  def careers
    data = platform_execute(JobsQuery)
    render "about/careers", locals: { data: data }
  end

  def remote
    render "about/remote"
  end

  def leadership
    render "about/leadership"
  end

  def diversity_report
    render "about/diversity_report/index"
  end

  def diversity
    render "about/diversity"
  end

  def milestones
    render "about/milestones"
  end

  def press_satellite
    render "about/press_satellite"
  end

  def press_universe
    render "about/press_universe"
  end

end
