# frozen_string_literal: true

class PullRequestReviewThreadResolutionsController < AbstractRepositoryController
  include DiscussionPreloadHelper

  ResolveCommentMutation = parse_query <<-'GRAPHQL'
    mutation($threadId: ID!, $focusedReviewComment: ID, $hasFocusedReviewComment: Boolean!, $syntaxHighlightingEnabled: Boolean!, $deferCollapsedThreads: Boolean = false) {
      resolveReviewThread(input: { threadId: $threadId }) {
        thread {
          databaseId
          ...Views::PullRequestReviewThreads::DiscussionShow::PullRequestReviewThread
        }
      }
    }
  GRAPHQL

  def create
    result = platform_execute(ResolveCommentMutation, variables: {
      threadId: params[:thread_id],
      focusedReviewComment: nil,
      hasFocusedReviewComment: false,
      syntaxHighlightingEnabled: syntax_highlighted_diffs_enabled?,
    })

    if result.errors.any?
      render status: 422, json: { errors: result.errors }
    else
      render_thread_partial(result.resolve_review_thread.thread)
    end
  end

  UnresolveCommentMutation = parse_query <<-'GRAPHQL'
    mutation($threadId: ID!, $focusedReviewComment: ID, $hasFocusedReviewComment: Boolean!, $syntaxHighlightingEnabled: Boolean!, $deferCollapsedThreads: Boolean = false) {
      unresolveReviewThread(input: { threadId: $threadId }) {
        thread {
          databaseId
          ...Views::PullRequestReviewThreads::DiscussionShow::PullRequestReviewThread
        }
      }
    }
  GRAPHQL
  def destroy
    result = platform_execute(UnresolveCommentMutation, variables: {
      threadId: params[:thread_id],
      focusedReviewComment: nil,
      hasFocusedReviewComment: false,
      syntaxHighlightingEnabled: syntax_highlighted_diffs_enabled?,
    })

    if result.errors.any?
      render status: 422, json: { errors: result.errors }
    else
      render_thread_partial(result.unresolve_review_thread.thread)
    end
  end

  private

  def render_thread_partial(thread)
    case params[:resolve_partial]
    when "mobile_timeline"
      thread_id = thread.database_id
      review_thread = PullRequestReviewThread.find(thread_id)
      deprecated_review_thread = review_thread.async_to_deprecated_thread.sync

      comments = deprecated_review_thread.comments_for(current_user)
      preload_discussion_group_data([deprecated_review_thread] + comments, mobile: true)
      render partial: "mobile/discussions/timeline_review_thread", locals: {
        review: review_thread.pull_request_review,
        thread: deprecated_review_thread,
      }
    when "mobile_diff"
      thread_id = thread.database_id
      review_thread = PullRequestReviewThread.find(thread_id).async_to_deprecated_thread.sync

      comments = review_thread.comments_for(current_user)
      preload_discussion_group_data([review_thread] + comments, mobile: true)
      render partial: "mobile/diff/review_thread", locals: {
        thread: review_thread,
        comment_context: :diff,
        timeline_diff_anchor: nil,
      }
    when "diff"
      # Need to get some database IDs so we can look up ActiveRecord objects for the
      # diff partial, since it's not GraphQL-ized... yet.
      thread_id = thread.database_id
      review_thread = PullRequestReviewThread.find(thread_id).async_to_deprecated_thread.sync

      comments = review_thread.comments_for(current_user)
      preload_discussion_group_data([review_thread] + comments)
      render partial: "diff/review_thread", locals: {
        review_thread: review_thread,
      }
    else
      render partial: "pull_request_review_threads/discussion_show", locals: {
        thread: thread,
      }
    end
  end

  def route_supports_advisory_workspaces?
    true
  end
end
