# frozen_string_literal: true

class UserContentEditsController < ApplicationController
  include PlatformHelper

  class UnknownControllerActionExternalIdentityOrganization < StandardError; end

  before_action :login_required, only: [:destroy, :dismiss_edit_history_onboarding]

  helper_method :can_delete_user_content_edit?

  def target_for_conditional_access
    user_content = case params[:action]
    when "show", "destroy"
      user_content_edit&.user_content
    when "show_edit_history"
      nil # handling SAML enforcement in GraphQL
    when "dismiss_edit_history_onboarding"
      content
    else
      # Whenever a new route is added to this controller it needs to be
      # accounted for in this method
      raise UnknownControllerActionExternalIdentityOrganization
    end

    return :no_target_for_conditional_access unless user_content

    owner = case user_content
    when DiscussionItem
      user_content.organization
    when Gist, GistComment
      user_content.user
    when PullRequest, PullRequestReview, PullRequestReviewComment, CommitComment, RepositoryAdvisory, RepositoryAdvisoryComment, DiscussionComment, Discussion
      user_content.repository.owner
    when IssueComment
      user_content.issue.owner
    when Issue
      user_content.owner
    else
      error = TypeError.new("Unexpected user content type '#{user_content.class}'")
      raise error
    end

    owner
  end

  def show
    respond_to do |format|
      format.html do
        if request.xhr? && user_content_edit
          render partial: "comments/comment_edit_history_diff", locals: { user_content_edit: user_content_edit }
        else
          render_404
        end
      end
    end
  end

  def show_edit_history
    respond_to do |format|
      format.html do
        if request.xhr? && comment
          render partial: "comments/comment_edit_history", locals: { comment: comment }
        else
          render_404
        end
      end
    end
  rescue PlatformHelper::ConditionalAccessError
    # this is an xhr request, so follow the pattern in
    # https://github.com/github/github/blob/4bf41fdfcdc9b7b30615e5dc7df6ae55bf73c583/app/controllers/application_controller/external_sessions_dependency.rb#L186-L187
    return head :unauthorized
  end

  def destroy
    return render_404 unless user_content_edit

    if site_admin? || can_delete_user_content_edit?(user_content_edit)
      user_content_edit.soft_delete!(current_user)
    else
      flash[:error] = "Unable to delete edit history"
    end

    redirect_to :back
  end

  def user_content_edit
    return @user_content_edit if defined?(@user_content_edit)

    _, id = Platform::Helpers::NodeIdentification.from_global_id(params[:id])
    user_content_edit = Platform::Objects::UserContentEdit.load_from_global_id(id).sync
    @user_content_edit = user_content_edit.viewer_can_read?(current_user) ? user_content_edit : nil
  end

  CommentQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on Comment { viewerCanReadUserContentEdits }
        ...Views::Comments::CommentEditHistory::Comment
      }
    }
  GRAPHQL

  def comment
    return @comment if defined?(@comment)
    comment = platform_execute(CommentQuery, variables: { "id" => params[:comment_id] },
      context: { enforce_conditional_access_via_graphql: true }).node
    @comment = comment if comment&.viewer_can_read_user_content_edits?
  end

  def content
    return @content if defined?(@content)
    @content = typed_object_from_id([Platform::Interfaces::Comment], params[:content_id])
  # NameError is happenong here as a response to Errors not being defined, but it is the notfound error
  rescue NameError, Platform::Errors::NotFound
    nil
  end

  # Checks if a user can delete the user content edit
  # For gist comment edits we are checking adminable by,
  # but for the rest we check against repo permissions
  def can_delete_user_content_edit?(user_content_edit)
    return false unless logged_in?
    user_content_edit.viewer_can_delete?(current_user)
  end

  def dismiss_edit_history_onboarding
    return head :not_found unless content
    current_user.dismiss_edit_history_onboarding(content)
    head :ok
  end
end
