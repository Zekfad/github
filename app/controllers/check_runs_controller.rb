# frozen_string_literal: true

class CheckRunsController < AbstractRepositoryController
  include ActionsContentPolicy

  areas_of_responsibility :checks
  before_action :require_push_access, except: [:show, :unseen_check_steps_partial, :step_summary_partial, :show_header_partial, :show_toolbar_partial]
  before_action :add_dreamlifter_csp_exceptions, only: [:show]

  CheckRunQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $annotationsLimit: Int!, $after: String) {
      node(id: $id) {
        ...Views::Checks::Show
      }
    }
  GRAPHQL

  around_action :record_show_metrics, only: [:show]
  private def record_show_metrics
    action_start_time = GitHub::Dogstats.monotonic_time

    yield

    GitHub.dogstats.timing_since("check_runs.show", action_start_time, tags: [
      "mobile:#{!!show_mobile_view?}",
      "logged_in:#{!!logged_in?}",
      "dreamlifter_enabled:#{GitHub.actions_enabled?}",
    ])
  end

  def show
    check_run = CheckRun.includes(:check_suite).find_by(id: params[:id])
    if check_run.nil? || check_run.check_suite.repository_id != current_repository.id
      render_404 and return
    end

    head_sha     = check_run.check_suite.head_sha

    unless current_repository.commits.exist?(head_sha)
      render_404 and return
    end

    check_suite_focus = params[:check_suite_focus] == "true"
    check_suites = check_suite_focus ? [check_run.check_suite] : current_repository.check_suites.where(head_sha: head_sha).most_recent
    commit       = current_repository.commits.find(head_sha)

    variables = { id: check_run.global_relay_id, annotationsLimit: CheckAnnotation::MAX_PER_REQUEST }

    graphql_check_run = platform_execute(CheckRunQuery, variables: variables).node
    commit_check_runs = check_suite_focus ? check_run.check_suite.latest_check_runs : CheckRun.for_sha_and_repository_id(commit.oid, current_repository.id)
    render "checks/show", locals: {
      commit: commit,
      selected_check_run: check_run,
      check_suites: check_suites,
      blankslate: false,
      graphql_check_run: graphql_check_run,
      commit_check_runs: commit_check_runs,
    }, layout: "repository"
  end

  def show_header_partial
    check_run = find_check_run
    return head :not_found unless check_run && request.xhr?

    render partial: "checks/checks_header", locals: { check_run: check_run }
  end

  def show_toolbar_partial
    check_run = find_check_run
    return head :not_found unless check_run && request.xhr?

    head_sha = check_run.check_suite.head_sha
    commit = current_repository.commits.find(head_sha)
    commit_check_runs = CheckRun.for_sha_and_repository_id(commit.oid, current_repository.id)

    pull = current_repository.pull_requests.find_by(id: params[:pull_id]) if params[:pull_id]

    if pull # For the commit dropdown when on the PR page
      Commit.prefill_combined_statuses(pull.changed_commits, current_repository)
    end

    check_suites = current_repository.check_suites.where(head_sha: head_sha).most_recent

    render_partial_view "checks/checks_toolbar", Checks::ChecksToolbarView, {
      commit_check_runs: commit_check_runs,
      commit: commit,
      check_suite: check_run.check_suite,
      check_suites: check_suites,
      selected_check_run: check_run,
      pull: pull,
    }, formats: [:html]
  end

  def unseen_check_steps_partial
    check_run = find_check_run
    return head :not_found unless check_run && request.xhr?

    steps = check_run.steps.created_after(DateTime.parse(params[:check_steps_since]))

    if GitHub.flipper[:actions_logs_next].enabled?(current_user)
      render partial: "checks/checks_steps_next", formats: [:html], locals: { check_run: check_run, steps: steps }
    else
      render partial: "checks/checks_steps", formats: [:html], locals: { check_run: check_run, steps: steps }
    end
  end

  def step_summary_partial
    check_run = find_check_run
    return head :not_found unless check_run && request.xhr?

    step = CheckStep.find_by(id: params[:step_id], check_run_id: check_run.id)
    return head :not_found unless step

    if GitHub.flipper[:actions_logs_next].enabled?(current_user)
      render partial: "checks/checks_step_summary_button_next", formats: [:html], locals: { step: step }
    else
      render partial: "checks/checks_step_summary_button", formats: [:html], locals: { step: step }
    end
  end

  def rerequest
    check_run = find_check_run
    return render_404 unless check_run

    if check_run.check_suite.check_runs_rerunnable
      check_run.rerequest(actor: current_user)

      head :ok and return if request.xhr?

      flash[:notice] = "You have successfully requested #{check_run.visible_name} be rerun."
      redirect_to :back
    else
      head :bad_request and return if request.xhr?

      flash[:error] = "Re-runs for individual check runs are disabled for this check suite."
      redirect_to :back and return
    end

  end

  def request_action
    check_run = find_check_run
    return render_404 unless check_run

    requested_action = { identifier: params[:identifier]}

    check_run.request_action(actor: current_user, requested_action: requested_action)
    flash[:notice] = "You have successfully requested '#{params[:label]}' on #{check_run.visible_name}."
    redirect_to :back
  end

  private

  def require_push_access
    render_404 unless current_user_can_push?
  end

  def find_check_run
    check_run = CheckRun.includes(:check_suite).where(id: params[:id]).first
    if !check_run || check_run.check_suite.repository_id != current_repository.id
      return nil
    end
    check_run
  end
end
