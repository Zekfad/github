# frozen_string_literal: true

module Api::App::HttpMethods
  def merge(path, opts = {}, &bk)
    methods :merge, :patch, path, opts, &bk
  end

  def verbs(*methods, &bk)
    opts = methods.last.is_a?(Hash) ? methods.pop : {}
    path = methods.pop
    conditions = @conditions.dup
    methods.each do |method|
      @conditions = conditions
      route(method.to_s.upcase, path, opts, &bk)
    end
  end
end
