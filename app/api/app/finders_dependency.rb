# rubocop:disable Style/FrozenStringLiteralComment

module Api::App::FindersDependency

  # Public: Fetches the current repo of this request.
  #
  # Returns a Repository instance. Halts with a 404 if no Repository is found.
  def current_repo
    @current_repo ||= find_repo!
  end

  def current_repo_loaded?
    !@current_repo.nil?
  end

  # Internal: Set the given repository as *the* repository associated with this
  # request, so that it can be used when determining whether the request has
  # sufficient authorization.
  #
  # repo - A Repository.
  #
  # Returns nothing.
  def set_current_repo_for_access_control(repo)
    @current_repo = repo
  end

  # Public: Selects the Repository defined in the URL of all Repository paths:
  #
  #   /repositories/:repository_id
  #
  # Returns a Repository instance or halts with 404 if not found.
  def find_repo!
    repository_or_404(find_repo)
  end

  # Public: Selects the template Repository defined in the URL:
  #
  #   /repositories/:template_repository_id
  #
  # Returns a Repository instance or halts with 404 if not found.
  def find_template_repo!
    id = int_id_param!(key: :template_repository_id)
    template_repo = Repository.templates.find_by_id(id) if id > 0
    repository_or_404(template_repo)
  end

  # Public: Selects the Gist as defined by params[param_name] in the request URL.
  #
  # param_name  - (optional) The `params` key where the Gist repo name can
  #               be found. Defaults to :id.
  # owner_param - (optional) The `params` key where the owner login can be
  #               found.
  #
  # Returns a Gist or nil if not found.
  def find_gist(param_name: :id, owner_param: nil)
    if owner_param.present?
      owner, repo_name = params.values_at(owner_param, param_name)
      Gist.alive.with_name_with_owner(owner, repo_name)
    elsif param_name == :id
      id = int_id_param!(key: :id)
      Gist.alive.find_by_id(id) if id > 0
    else
      Gist.alive.find_by_repo_name(params[param_name])
    end
  end

  # Public: Selects the Gist as defined by params[param_name] in the request URL.
  #
  # param_name  - (optional) The `params` key where the Gist repo name can
  #               be found. Defaults to :id.
  # owner_param - (optional) The `params` key where the owner login can be
  #               found.
  #
  # Returns a Gist or halts with a 404 if not found.
  def find_gist!(param_name: :id, owner_param: nil)
    find_gist(param_name: param_name, owner_param: owner_param).tap do |gist|
      gist_or_404(gist)

      if gist.access.disabled?
        deliver_disabled_gist_error!(gist)
      end
    end
  end

  # A safer, kinder, gentler way to get a commit. Let's not leak out
  # nasty 500 exceptions to end users if their sha is unknown or malformed
  # This should be handled better at a lower level...some sort of
  # safe way to ask for a commit for a SHA without having to worry about
  # leaking out lower level exceptions.
  def find_commit!(repo, sha, documentation_url = nil)
    if sha && commit_oid = repo.ref_to_sha(sha)
      repo.commits.find(commit_oid)
    else
      deliver_error! 422,
        message: "No commit found for SHA: #{sha}",
        documentation_url: documentation_url
    end
  rescue GitRPC::InvalidObject, GitRPC::ObjectMissing
    deliver_error! 422,
      message: "No commit found for SHA: #{sha}",
      documentation_url: documentation_url
  end

  # Public: Selects the Repository defined in the URL of this request by
  # checking the `:user` and `:repo` parameters.
  #
  # Halts with a redirect if the Repository has relocated (e.g., if it has
  #   been renamed).
  # Halts with a 404 if no Repository is found.
  # Returns a Repository instance.
  def this_repo
    if path_includes_relocated_repo?
      redirect_to_new_repo_location_or_404! &method(:this_repo_redirection)
    end

    repo = env[GitHub::Routers::Api::ThisRepositoryKey] = Repository.nwo(repo_nwo_from_path)

    repository_or_404(repo)
  end

  def repository_or_404(repo)
    repo && repo.network_broken?
    repo = nil if repo && (!repo.active? || repo.disabled? || repo.hide_from_user?(current_user))
    record_or_404 repo
  rescue Repository::NetworkMissingError => e
    Failbot.report e

    deliver_disabled_repo_error!(repo)
  end

  def gist_or_404(gist)
    gist = nil if gist && !gist.active?
    record_or_404 gist
  end

  def record_or_404(record)
    record || deliver_error!(missing_repository_status_code)
  end

  # Public: Selects the PreReceiveEnvironment defined in the URL request.
  #
  # Returns a PreReceiveEnvironment instance if found.
  def find_pre_receive_environment(param_name: :id)
    PreReceiveEnvironment.find_by_id int_id_param!(key: param_name)
  end

  # Public: Selects the PreReceiveEnvironment defined in the URL request.
  #
  # Returns a PreReceiveEnvironment instance or halts with 404 if not found.
  def find_pre_receive_environment!(param_name: :id)
    record_or_404 find_pre_receive_environment(param_name: param_name)
  end

  # Public: Selects the PreReceiveHook defined in the URL request.
  #
  # Returns a PreReceiveHook instance if found.
  def find_pre_receive_hook(param_name: :id)
    PreReceiveHook.find_by_id int_id_param!(key: param_name)
  end

  # Public: Selects the PreReceiveHook defined in the URL request.
  #
  # Returns a PreReceiveHook instance or halts with 404 if not found.
  def find_pre_receive_hook!(param_name: :id)
    record_or_404 find_pre_receive_hook(param_name: param_name)
  end

  # Public: Selects the global PreReceiveHookTarget for the PreReceiveHook defined in the URL request.
  #
  # Returns a PreReceiveHookTarget instance if found.
  def find_global_pre_receive_hook_target(param_name: :id)
    PreReceiveHookTarget.global.for_hook(int_id_param!(key: param_name)).first
  end

  # Public: Selects the global PreReceiveHookTarget for the PreReceiveHook defined in the URL request.
  #
  # Returns a PreReceiveHookTarget instance or halts with 404 if not found.
  def find_global_pre_receive_hook_target!(param_name: :id)
    record_or_404 find_global_pre_receive_hook_target(param_name: param_name)
  end

  # Public: Selects the PreReceiveHookTarget for the PreReceiveHook defined in the URL request.
  #
  # Returns a PreReceiveHookTarget instance if found.
  def find_pre_receive_hook_target(param_name: :id)
    hookable =
      case
        when params[:repository_id]
          find_repo!
        when params[:organization_id]
          find_org!
        else
          GitHub.global_business
      end
    hook_id = int_id_param!(key: param_name)
    PreReceiveHookTarget.enforcement_target(hookable, hook_id)
  end

  # Public: Selects the PreReceiveHookTarget for the PreReceiveHook defined in the URL request.
  #
  # Returns a PreReceiveHookTarget instance or halts with 404 if not found.
  def find_pre_receive_hook_target!(param_name: :id)
    record_or_404 find_pre_receive_hook_target(param_name: param_name)
  end

  # Public: Selects the User defined in the URL request.
  #
  # Returns a User instance if found.
  def find_user
    find_user_or_org_by_id :user_id, User
  end

  # Public: Selects the User defined in the URL request.
  #
  # Returns a User instance or halts with 404 if not found.
  def find_user!
    record_or_404 find_user
  end

  def this_user
    path = (params[:user] || params[:username]).to_s
    user = path.present? && User.find_by_login(path)
    record_or_404 user
  end

  # Deprecated: Use #find_org instead.
  def this_organization
    record_or_404 find_org_by_login
  end

  # Public: Selects the Organization defined in the URL request.
  #
  # Returns an Organization instance, or nil.
  def find_org
    @current_org ||= find_org_by_id || find_org_by_login
  end

  # Public: Selects the Organization defined in the URL request.
  #
  # Halts with 404 if no Organization is found.
  # Returns an Organization instance.
  def find_org!
    record_or_404(find_org)
  end

  def find_enterprise
    @current_enterprise ||= find_enterprise_by_id || find_enterprise_by_login
  end

  # Public: Finds the Enterprise defined in the URL
  #
  # Halts with 404 if no Enterprise is found.
  # Returns an Enterprise instance.
  def find_enterprise!
    record_or_404(find_enterprise)
  end

  # Public: Selects the Team by ID defined in the URL request.
  #
  # Returns a Team instance.
  def find_team(param_name: :team_id, org_param_name: :org_id)
    Team.find_by(id: int_id_param!(key: param_name), organization_id: int_id_param!(key: org_param_name))
  end

  # Public: Selects the Team by ID defined in the URL request.
  #
  # Halts with 404 if no Team is found.
  # Returns a Team instance.
  def find_team!(param_name: :team_id, org_param_name: :org_id)
    record_or_404(find_team(param_name: param_name, org_param_name: org_param_name))
  end

  def find_integration(integration_id)
    Integration.find_by(id: integration_id)
  end

  def find_integration!(integration_id)
    record_or_404(find_integration(integration_id))
  end

  def find_installation(repo, app)
    IntegrationInstallation.with_repository(repo).find_by(integration_id: app.id)
  end

  def find_installation!(repo, app)
    record_or_404(find_installation(repo, app))
  end

  # Private: Selects the User or Organization defined in the URL request.
  def find_user_or_org_by_id(new_param_key, klass)
    if (user = env[GitHub::Routers::Api::ThisUserKey])
      user
    elsif (id = params[new_param_key])
      klass.find_by_id(id.to_i)
    end
  end

  private

  # Private: finds the enterprise by id with `params[:id]`
  #
  # Returns an Enterprise instance or nil
  def find_enterprise_by_id
    Business.find_by(id: params[:enterprise_id])
  end

  # Private: finds the enterprise by login with `params[:id]`
  #
  # Returns an Enterprise instance or nil
  def find_enterprise_by_login
    Business.find_by(slug: params[:enterprise_id])
  end

  # Private: Selects the Organization whose ID is defined in the request's URL.
  #
  # Returns an Organization instance, or nil.
  def find_org_by_id
    user_or_org = find_user_or_org_by_id(:organization_id, Organization)

    if user_or_org && user_or_org.organization?
      user_or_org
    else
      nil
    end
  end

  # Private: Selects the Organization defined in the URL request.
  def find_org_by_login
    login = params[:org].to_s
    login.present? && Organization.find_by_login(login)
  end

  # Internal: Selects the Repository defined in the URL of all Repository paths:
  #
  #   /repositories/:repository_id
  #
  # Returns a Repository instance, or nil.
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def find_repo
    @current_repo ||= begin
                        if repo = env[GitHub::Routers::Api::ThisRepositoryKey]
                          repo
                        else
                          id = int_id_param!(key: :repository_id)

                          Repository.find_by_id(id) if id > 0
                        end
                      end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # Internal: Finds the org owner (if any) for the current resource.
  #
  # Returns an Organization instance, a Business (Enterprise) instance, or nil.
  def current_resource_owner
    return @current_org if @current_org
    return @current_enterprise if @current_enterprise

    if repo = find_repo
      repo.owner if repo.in_organization?
    end
  end

  # Internal: Updates the API url to redirect to the given repository. See
  # #this_repo.
  #
  # path            - String API request path.
  # requested_nwo   - String "user/repo" for the old repository.
  # redirected_repo - The Repository record that is being redirected to.
  #
  # Returns a fixed String API path for the repository.
  def this_repo_redirection(path, requested_nwo, redirected_repo)
    path.sub(requested_nwo, redirected_repo.nwo)
  end
end
