# rubocop:disable Style/FrozenStringLiteralComment

# Functionality related to receiving request input (e.g., unpacking it, parsing
# parsing it, validating that it satisfies a schema).

module Api::App::InputDependency
  # Public: Receive JSON from the request body and parse it.
  #
  # expected_type - An optional Class that is used to check the basic type of
  #                 the parsed JSON object.  Usually `Hash` or `Array`.
  # required      - Optional Boolean that determines if the type or JSON is
  #                 required.  Default: true if the expected type is set.
  #
  # Halts with a 400 if the JSON is invalid.
  # Returns the parsed JSON Object.
  def receive(expected_type = nil, required: true)
    json = request.body.read
    receive_json(json, type: expected_type, required: required)
  end

  # Public: Converts a message from the API that talks about Ruby objects to talk
  # about JavaScript objects.
  #
  # message - A string whose references to Ruby objects should change
  #
  # Returns a string
  def translate_ruby_type_name_to_json_type_name(string)
    string.sub(/Hash/i, "object")
  end

  # Public: Parse the given JSON.
  #
  # type     - An optional Class that is used to check the basic type of the
  #            parsed JSON object.  Usually `Hash` or `Array`.
  # required - Optional Boolean that determines if the type or JSON is required.
  #            Default: true if the expected type is set.
  #
  # Halts with a 400 if the JSON is invalid.
  # Returns the parsed JSON Object.
  def receive_json(json, type: nil, required: true)
    required = false unless type

    res = GitHub::JSON.parse(json) unless json.blank?
    check_type = type && (required || res)

    if check_type && !res.is_a?(type)
      deliver_error! 400, message: "Body should be a JSON #{translate_ruby_type_name_to_json_type_name(type.to_s)}"
    end

    res
  rescue Yajl::ParseError, StandardError
    deliver_error! 400, message: "Problems parsing JSON"
  end

  # Public: Receive JSON from the client, parse it, and validate that it
  # satisfies the schema for requests that perform the specified action on the
  # specified type of resource.
  #
  # resource        - The String name of the type of resource that the request is
  #                   operating on.
  # rel             - The String name of the rel that uniquely identifies the type of
  #                   action that the request is performing on the resource.
  # skip_validation - The Boolean that identifies whether to skip validating
  #                   the JSON schema
  # expected_type   - An optional Class that is used to check the basic type of
  #                   the parsed JSON object.  Usually `Hash` or `Array`.
  #                   Should be nil unless skip_validation is true
  # required        - Optional Boolean that determines if the type or JSON is
  #                   required.  Default: true if the expected type is set.
  #
  # Examples
  #
  #   receive_with_schema("label", "update")
  #
  #   receive_with_schema("label", "update", skip_validation: true)
  #
  # Halts with a 422 if the JSON is invalid and skip_validation is false.
  # Returns a Hash or an Array (depending on the format of the request data).
  def receive_with_schema(resource, rel, skip_validation: false, expected_type: nil, required: true)
    # Be Kind, Please Rewind
    # This ensures that if we have already read the request
    # body we can read it again.
    request.body.rewind

    data = receive(expected_type, required: required)

    unless skip_validation
      result = Api::V3SchemaCollection.validate(data: data, resource: resource, route: request.env["sinatra.route"])
      deliver_schema_validation_error!(result) unless result.valid?
    end

    if data && (data.is_a?(Hash) || data.is_a?(Array))
      data
    else
      {}
    end
  end

  # Public: Verify that the given data satisfies the schema for requests that
  # perform the specified action on the specified type of resource.
  #
  # data     - The Hash or Array representing the request input.
  # resource - The String name of the type of resource that the request is
  #            operating on.
  # rel      - The String name of the rel that uniquely identifies the type of
  #            action that the request is performing on the resource.
  #
  # Halts with a 422 if the data violates the schema.
  # Returns nothing.
  def ensure_data_satisfies_schema!(data, resource, rel)
    result = Api::V3SchemaCollection.validate(data: data, resource: resource, rel: rel)

    return if result.valid?

    deliver_schema_validation_error!(result)
  end

  # Public: Extracts the given keys from the data.  Think of this like
  # `attr_accessible`.  Attributes ending in `_at` or `_on` are parsed into
  # Time objects.
  #
  # data - A Hash of attributes to be set on an object.
  # keys - Array of Symbol keys.
  #
  # Returns a sanitized Hash.
  def attr(data, *keys)
    return nil if data.nil?
    options = keys.extract_options!
    prefix = options[:prefix]
    hash = {}

    return hash unless data.is_a?(Hash)

    keys.each do |k|
      k_s = k.to_s
      if data.has_key?(k_s)
        value = data[k_s]
        if k_s =~ /_(at|on)$/ && value
          value = Time.parse(value) unless value.respond_to?(:utc)
          value = value.localtime if value.utc?
        end
        k_s = "#{prefix}#{k_s}" if prefix
        hash[k_s] = value
      end
    end
    hash.with_indifferent_access
  end

  # Returns the given request parameter as an Integer, either
  # logging or halting if the supplied parameter value is not
  # the exact String representation of the Integer value.
  #
  # key  - Symbol request parameter key containing the id
  #        (default: :id)
  # halt - Boolean for whether to halt with an error or just log
  #        (default: false)
  #
  # returns the Integer value of the parameter, or halts with a 422
  def int_id_param!(key: :id, halt: false)
    id = params[key].try(:b)

    unless id =~ /\A\d+\z/
      instrument_non_integer_id(id)

      if halt
        message = "The #{key} parameter must be an integer."
        deliver_error!(422, message: message)
      end
    end

    id.to_i
  end

  # Returns the request parameter as a Time, halting if
  # the supplied parameter value cannot be parsed as a Time value.
  def time_param!(key)
    return nil if params[key].blank?

    message = "The #{key} parameter needs to be in " \
              "ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ"

    parse_time!(params[key], message: message)
  end

  # These are the minima and maxima allowed by MySQL for the DATETIME type
  # https://dev.mysql.com/doc/refman/5.0/en/datetime.html
  TIME_MIN = Time.parse("1000-01-01T00:00:00Z")
  TIME_MAX = Time.parse("9999-12-31T23:59:59Z")

  def parse_time!(value, options = {})
    begin
      time = Time.parse(value)
      if time.between?(TIME_MIN, TIME_MAX)
        return time
      else
        options[:message] ||= "Time values must be in the range " \
                              "1000-01-01T00:00:00Z to 9999-12-31T23:59:59Z"
      end
    rescue StandardError
      options[:message] ||= "Time values must be in " \
                            "ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ"
    end

    options[:documentation_url] ||= @documentation_url || "/v3/#schema"
    deliver_error! 422, options
  end

  # Checks if a given request parameter contains a properly formatted
  # SHA string.
  #
  # key  - Symbol request parameter key containing the SHA
  # data - Hash of data to check for the key
  #        (optional, defaults to params hash)
  #
  # Returns a String or halts with 422
  def sha_param!(key, data = nil)
    message = "The #{key} parameter must be exactly 40 characters " \
              "and contain only [0-9a-f]."

    sha = (data || params)[key]
    deliver_error!(422, message: message) unless GitRPC::Util.valid_full_sha1?(sha)

    sha
  end

  # Internal: Decodes URL-encoded push user data
  #
  # data   -  Hash with push data that should be URL decoded
  #
  # Modified passed data hash.
  def decode_push_data!(data)
    ["pusher", "hook_warning", "hook_error", "real_api"].each do |name|
      data[name] = CGI.unescape(data[name]) if data[name]
    end

    data["ref_updates"].each do |ref_update|
        ref_update["refname"] = CGI.unescape(ref_update["refname"])
    end
  end
end
