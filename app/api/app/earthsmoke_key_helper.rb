# rubocop:disable Style/FrozenStringLiteralComment

module Api::App::EarthsmokeKeyHelper

  PUBLIC_KEY_COUNT = 10

  def generate_earthsmoke_key_payload(name:, scope: "", count: PUBLIC_KEY_COUNT)
    begin
      key = GitHub.earthsmoke.low_level_key(name)
      key_versions = key.export(scope: scope).key_versions.first(count)
    rescue Earthsmoke::Error => exception
      Failbot.report!(exception, app: "github")
      deliver_error!(503, message: "Public keys unavailable. Please try again later.")
    end

    payload = key_versions.map.with_index do |version, index|
      case version.key_pair_algo
      when Algos::KeyPair::LIBSODIUM_SEALED_BOX_CURVE25519_XSALSA20_POLY1305.algo
        encoded = Base64.strict_encode64(version.parsed_key_pair.public_bytes)
        ident = version.id.to_s
      else
        encoded = version.parsed_key_pair.public_key.to_pem
        ident = Digest::SHA256.hexdigest(encoded)
      end

      out = {
        key_identifier: ident,
        key: encoded,
        is_current: index == 0,
      }
      if !scope.empty?
        out[:scope] = scope
      end
      out
    end

    payload
  end

  def deliver_earthsmoke_key(name:, feature_flag:, scope: "", count: PUBLIC_KEY_COUNT)
    deliver_error!(404) if GitHub.enterprise?

    if feature_flag
      feature_actor = if current_user.respond_to?(:integration)
        current_user.integration
      else
        current_user
      end

      deliver_error!(404) unless feature_actor
      deliver_error!(404) unless GitHub.flipper[feature_flag].enabled?(feature_actor)
    end

    payload = generate_earthsmoke_key_payload(name: name, scope: scope, count: count)

    deliver_raw({ public_keys: payload })
  end
end
