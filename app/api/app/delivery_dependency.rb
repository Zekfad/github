# rubocop:disable Style/FrozenStringLiteralComment

module Api::App::DeliveryDependency
  extend ActiveSupport::Concern

  include Api::App::CachingHelpers
  include Api::App::DeprecationHelpers

  LocationStatusCodes = Set.new([201, 301, 302, 307]).freeze

  class RestGraphQLMismatchError < Scientist::Experiment::MismatchError
    def to_s
      raise result.candidates[0].exception if result.candidates[0].exception
      raise result.control.exception if result.control.exception

      control   = pretty_print_for_diff(result.control.cleaned_value)
      candidate = pretty_print_for_diff(result.candidates[0].cleaned_value)

      diff = GitHub::Diff::Generator.diff("", control, candidate)

      "Mismatch between REST and GraphQL (`a` represents REST, and `b` represents GraphQL):\n\n#{diff}\n\nSee https://githubber.com/article/technology/dotcom/graphql/rest_graphql_mismatch_errors for more information."
    end

    private

    # Borrowed from Devtools::Experiments::MismatchView
    def pretty_print_for_diff(value)
      return value if value.is_a?(String)
      value.pretty_inspect.gsub(/\\n/, "\n").gsub(/:0x[a-fA-F0-9]{4,}/m, ":0xXXXXXX")
    end
  end



  # Public: Deliver a serialized object to the client as JSON.
  #
  # serialize_method - Symbol name of the serializer method to use.
  # obj              - Any record serializable to JSON using the V3 serializer.
  # options          - Optional Hash that is passed to the serializer.
  #                    :status       - Integer of the HTTP Status, if it's
  #                                    different than 200.
  #                    :content_type - String Content-Type header if you want
  #                                    to deliver something other than JSON.
  #                    :compare_payload_to - Symbol or Proc. When a symbol is
  #                                          passed, a method of that name will
  #                                          be invoked and compared against
  #                                          the serialized response using
  #                                          Scientist. When a block is passed,
  #                                          it will be called and its response
  #                                          will be compared against the
  #                                          serialized response.
  #                    :compare_with    - Symbol of a method that will be used to
  #                                       compare the control and candidate.
  #                                       method(compare_with) should resolve
  #                                       to a method with two arguments that
  #                                       returns a boolean. Two :compare_with
  #                                       methods are implemented in this file,
  #                                       shape_comparison and exact_comparison.
  #
  # Returns a String body to be used as the response of this request.
  def deliver(serialize_method, obj = nil, options = nil)
    if !serialize_method.is_a?(Symbol)
      obj = (serialize_method.respond_to?(:map!) ? serialize_method.first : serialize_method)
      raise "Needs serialize_method: #{obj.class.api_serializer_method.to_sym}"
    end

    if [nil, false].include?(obj)
      return deliver_error(404)
    end

    options ||= {}
    options.update(default_options)

    code = options.delete(:status) || 200
    status code

    unless @links.has_pagination?
      # The collection size can be defined in a number of different ways.
      # 1. Explicitly set paginator.collection_size in the API endpoint.
      #    Typically used with REST endpoints backed by GraphQL, as the object passed to the serializer
      #    is not always the one that has a #total_count method.
      #    Additionally, the GraphQL objects get cranky if you call #total_count outside of the API endpoints.
      # 2. Infer from #total_entries on the collection.
      #    This is typically used with paginated ActiveRecord relations and WillPaginate collections,
      #    or collections that have explicitly been made to quack like them.
      # 3. Infer from :total_count passed to the serializer.
      #    This is used when the API payload returns the collection in a Hash rather than a top-level Array,
      #    since in that case the collection to be paginated is passed as part of the hash, and we can't guess
      #    the key name.
      paginator.collection_size ||= obj[:total_count] if obj.is_a?(Hash)
      paginator.collection_size ||= obj.total_entries if obj.respond_to?(:total_entries)
      paginator.collection_size = [paginator.collection_size.to_i, pagination_capped_to_n_entries].min if pagination_capped_to_n_entries
      set_pagination_headers
    end

    if Rails.test?
      env["github.pagination"]                      = @pagination                              if @pagination.present?
      env["github.oauth"]                           = @oauth                                   if @oauth.present?
      env["github.user"]                            = @current_user                            if @current_user.present?
      env["github.oauth_app"]                       = @current_app                             if @current_app.present?
      env["github.integration"]                     = @current_integration                     if @current_integration.present?
      env["github.integration_installation"]        = @current_integration_installation        if @current_integration_installation.present?
      env["github.parent_integration_installation"] = @current_parent_integration_installation if @current_parent_integration_installation.present?
    end

    if @oauth
      @meta["X-OAuth-Scopes"] = @oauth.access_level.dup * ", "
      @meta["X-Accepted-OAuth-Scopes"] = accepted_scopes_string
      if @oauth.application
        @meta["X-OAuth-Client-Id"] = @oauth.application.key
      end
    end

    if GitHub.enterprise?
      @meta["X-GitHub-Enterprise-Version"] = GitHub.version_number
    end

    options[:current_user] ||= @current_user

    skip_serialization = serialize_method == :raw
    custom_content_type = options.delete(:content_type)
    content_type = custom_content_type || default_content_type

    if !(custom_content_type || skip_serialization)
      serializer_options = Api::SerializerOptions.fill(options)
      serializer_options[:route] ||= request.env["sinatra.route"]

      original_obj = obj
      obj = Api::Serializer.serialize(serialize_method, obj, serializer_options)

      if options[:compare_payload_to]
        if Rails.env.production?
          options[:compare_with] = :exact_comparison
        else
          options[:compare_with] ||= :shape_comparison
        end

        science("api.rest_graphql.#{serialize_method}") do |e|
          e.use { obj }
          e.try do
            case options[:compare_payload_to]
            when Symbol
              candidate_method = method(options[:compare_payload_to])
              candidate_method.call(original_obj, serializer_options)
            when Proc
              options[:compare_payload_to].call(original_obj, serializer_options)
            else
              raise ArgumentError.new("Unrecognized `compare_payload_to` type: #{options[:compare_payload_to]}")
            end
          end

          e.raise_with(RestGraphQLMismatchError)

          e.clean do |result|
            case options[:compare_with]
            when :shape_comparison
              api_response_shape(result)
            else
              json_primitives_for(result)
            end
          end

          e.compare do |control, candidate|
            deep_symbolize_payload(control)
            deep_symbolize_payload(candidate)

            method(options[:compare_with]).call(control, candidate)
          end
        end
      end
    end

    if LocationStatusCodes.include?(code)
      url = (obj.is_a?(Hash) && obj[:url]) || options[:url]
      @meta["Location"] = url if url
    end

    caching_options = {
      last_modified: options[:last_modified],
      etag: options[:etag],
      body: encode_json(obj, span_tag: "caching_options"),
    }
    set_caching_headers! caching_options
    geo_block_list = (obj.is_a?(Hash) && obj[:geo_block_list]) || options[:geo_block_list]
    unless medias.empty?
      @meta["X-GitHub-Media-Type"] = medias.to_http_header
    end
    @meta["X-Geo-Block-List"] = geo_block_list if geo_block_list

    pretty = user_agent.cli? || user_agent.browser?
    extra_newline = pretty ? "\n" : ""

    # JSON-P requests get send http 200, with http headers as a json envelope
    # around the json response
    if jsonp?
      status 200
      headers["Content-Type"] = "application/javascript; charset=utf-8"

      @meta["Link"]  = @links.to_meta if @links.present?
      @meta[:status] = code

      # JSONP responses need to decrement the remaining rate limit here
      # because `after` block is too late.
      if increment_rate_limit?(code) && !varnished?
        @meta.update(
          "X-RateLimit-Remaining" => [@meta["X-RateLimit-Remaining"].to_i - 1, 0].max.to_s,
        )
      end

      obj = nil if code == 204
      encode_jsonp(meta: response.headers.merge(@meta), data: obj, pretty: pretty) + extra_newline

    elsif code == 205
      # According to the HTTP spec, a response 'SHOULD' contain a content-type if
      # it has a response body. In other words, content-type is optional, and
      # if there is no response body, there is no reason to include it.
      # However, at the moment it's tricky to get Sinatra to remove the content-type
      # header altogether.
      content_type "text/plain"
      headers @meta if @meta.present?
      ""
    else
      # everyone else gets a sane json response
      headers["Content-Type"] = content_type
      headers @meta if @meta.present?
      set_deprecation_headers!
      headers["Link"] = @links.to_header if @links.present?
      body = custom_content_type ? obj : encode_json(obj, pretty: pretty, span_tag: "json_body") + extra_newline
      code == 204 ? "" : body
    end
  end

  def default_options
    options = {}

    if (mimes = request.accept).present?
      options[:accept_mime_types] = mimes
    end

    media = medias.api
    options[:mime_params] = media&.api_params

    options
  end

  def deep_symbolize_payload(payload)
    if payload.is_a? Array
      payload.each do |element|
        deep_symbolize_payload(element)
      end
    elsif payload.is_a? Hash
      payload.deep_symbolize_keys
    end
  end

  # Compares a control and candidate REST payload exactly. Converts keys and
  # values into JSON primitives before comparsion.
  def exact_comparison(control, candidate)
    json_primitives_for(control) == json_primitives_for(candidate)
  end

  # Compares a control and REST payload by hash shape and value type. Converts
  # hash values into JSON primitive types (String, Integer, Boolean, etc), then
  # compares.
  def shape_comparison(control, candidate)
    api_response_shape(control) == api_response_shape(candidate)
  end

  # Public: Deliver a serialized object to the client as JSON and halt.
  #
  # serialize_method - Symbol name of the serializer method to use.
  # obj              - Any record serializable to JSON using the V3 serializer.
  # options          - Optional Hash that is passed to the serializer.
  #                    :status       - Integer of the HTTP Status, if it's
  #                                    different than 200.
  #                    :content_type - String Content-Type header if you want
  #                                    to deliver something other than JSON.
  #
  # Delivers the response body and halts further processing.
  def deliver!(serialize_method, obj = nil, options = nil)
    halt deliver(serialize_method, obj, options)
  end

  def default_content_type
    "application/json; charset=utf-8"
  end

  # Public: Deliver a serialized object to the client as JSON.
  #
  # Same as:
  #
  #   deliver(:raw, foo, options)
  #
  def deliver_raw(obj, options = nil)
    deliver(:raw, obj, options)
  end

  # Public: Deliver an empty response to the client.
  #
  # Same as:
  #
  #   deliver(:raw, {}, options)
  #
  def deliver_empty(options = nil)
    deliver(:raw, {}, options)
  end

  # Public: Halt the request with a redirect.
  #
  # url               - The String URL to redirect to.
  # status            - The Integer HTTP status code to use for the response.
  # documentation_url - The String URL to include in the response body
  #                     identifying the location of documentation relevant to
  #                     the request/response (optional).
  #
  # Returns nothing.
  def deliver_redirect!(url, status:, documentation_url: "/v3/#http-redirects")
    body = {
      message: "Moved Permanently",
      url: url,
      documentation_url: "#{GitHub.developer_help_url}#{documentation_url}",
    }

    options = {
      url: url,
      status: status,
    }

    halt deliver_raw(body, options)
  end

  def encode_json(object, span_tag: "untagged", **options)
    GitHub.tracer.with_span("encode_json.#{span_tag}") do |span|
      span.set_tag("pretty", options[:pretty].inspect)
      json_str = GitHub::JSON.encode(object, options)
      span.set_tag("json_bytesize", json_str.bytesize)
      json_str
    end
  end

  def encode_jsonp(meta:, data:, pretty:)
    if varnished?
      # Start the response with "<" and set a "X-Iris-JSONP-With-ESI"
      # header so that varnish will process ESI tags. Also, escape any
      # "<" in the data so that we don't process user-supplied data,
      # e.g. a repository description like 'ha ha <esi:include src=bogus> ha ha'.

      headers["X-Iris-JSONP-With-ESI"] = "true"
      enable_esi = "<esi:remove></esi:remove>"

      meta = meta.merge \
        "X-RateLimit-Limit" => "<esi:include src='/_esi/jsonp_rate_limit/limit'/>",
        "X-RateLimit-Remaining" => "<esi:include src='/_esi/jsonp_rate_limit/remaining'/>",
        "X-RateLimit-Reset" => "<esi:include src='/_esi/jsonp_rate_limit/reset'/>"
      meta_json = encode_json(meta, pretty: pretty, span_tag: "jsonp_meta")
      data_json = encode_json(data, pretty: pretty, span_tag: "jsonp").
        gsub("<", "\\u003c").
        gsub(">", "\\u003e")
      payload = '{"meta": %s, "data": %s}' % [meta_json, data_json]
    else
      enable_esi = ""
      payload = encode_json({meta: meta, data: data}, pretty: pretty, span_tag: "jsonp")
    end

    payload = payload.
      gsub("\u2028", '\u2028'). # github/github#6188
      gsub("\u2029", '\u2029')  # http://timelessrepo.com/json-isnt-a-javascript-subset

    "%s/**/%s(%s)" % [enable_esi, jsonp_callback, payload]
  end

  def jsonp_callback
    return @jsonp if defined?(@jsonp)

    return unless @jsonp = params["callback"]
    valid = @jsonp.is_a?(String)
    if valid
      begin
        @jsonp.strip!
        valid = @jsonp.valid_encoding? && !!(@jsonp =~ /\A[\w\.\[\]]+\z/)
      rescue ArgumentError
        valid = false
        @jsonp = nil
      end
    end

    if !valid
      params.delete("callback")
      message = "Invalid callback: #{@jsonp.inspect}"
      @jsonp  = nil
      deliver_error!(400, message: message, documentation_url: "/v3/#json-p-callbacks")
    end

    @jsonp
  end

  def jsonp?
    !!jsonp_callback
  end

  def varnished?
    env["HTTP_X_GITHUB_DYNAMIC_CACHE"] == "api"
  end

  DefaultModifiedTime = Time.utc(1970)

  def calc_last_modified(objects)
    return nil if objects.blank?
    modified =
      Array(objects).map do |o|
        if o && o.respond_to?(:last_modified_at)
          o.last_modified_at || DefaultModifiedTime
        else
          DefaultModifiedTime
        end
      end.max
    modified || raise(ArgumentError, "Invariant: no last-modified calculation found for objects")
  end

  # Builds an API url from the current Rack environment without query
  # parameters.
  #
  # env - The Rack environment Hash.
  #
  # Returns a String URL.
  def url_without_query(env)
    url = request.scheme + "://"
    url << request.host

    if request.scheme == "https" && request.port != 443 ||
        request.scheme == "http" && request.port != 80
      url << ":#{request.port}"
    end

    if prefix = env[GitHub::Routers::Api::API_PATH]
      url << prefix
    end

    url << request.path
  end

  # Build a formatted string of scopes based on the current
  # accepted scopes array.
  #
  # Returns a String
  def accepted_scopes_string
    @accepted_scopes = %w(repo) if @accepted_scopes.nil?
    OauthAccess.filter_public_scopes(@accepted_scopes).sort * ", "
  end

  private

  # Private: JSON serialize, then parse back into a hash. This results in
  #          normalization of keys and values that are identical in JSON.
  #          e.g. symbols become strings, Ruby objects get `to_s`d, etc.
  #
  # hash - The hash to reduce into primitives
  #
  # Returns a Ruby hash
  def json_primitives_for(hash)
    JSON.parse(hash.to_json)
  rescue JSON::ParserError
    # This is unlikely to happen because the incoming `hash` is immediately `.to_json`'ed,
    # but if it does, don't raise this error since it might contain sensitive information.
    raise ArgumentError, "Failed to determine JSON primitives"
  end
end
