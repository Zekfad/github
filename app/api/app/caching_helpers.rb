# rubocop:disable Style/FrozenStringLiteralComment

module Api::App::CachingHelpers

  # Set the appropriate caching headers for the response.
  #
  # options - The Hash of per-action overriddable options.
  #           :last_modified - The Time that the resource represented by the
  #                            response was last updated (optional).
  #           :etag          - The String value to use as the ETag header
  #                            (optional).
  #           :body          - The String response body to use as the basis for
  #                            generating an ETag (optional).
  #           :max-age       - The FixNum Cache-Control max-age in seconds.
  #                            (default: 60).
  #
  # If the response already has an ETag or Last-Modified header set, or if the
  # response has a Cache-Control header set to "no-cache", this method will
  # *not* set any caching headers on the response.
  #
  # If the options Hash includes :last_modified, this method will set the
  # Last-Modified response header using the given value.
  #
  # If the options Hash includes :etag, this method will set the ETag response
  # header to the given value. Otherwise, if the options Hash includes :body,
  # and the response has an appropriate status code for an ETag, then this
  # method will generate an ETag based on the given body.
  #
  # Returns nothing.
  def set_caching_headers!(options = {})
    CacheHeaderSetter.new(self, self, options).apply
  end

  class CacheHeaderSetter

    attr_reader :request_context, :response_context, :options

    DEFAULTS = {
      max_age: 60,
      vary_headers: %w[Accept],
    }

    AUTHENTICATED_VARY_HEADERS = %w[Authorization Cookie X-GitHub-OTP]

    def initialize(request_context, response_context, options = {})
      @request_context = request_context
      @response_context = response_context
      @options = options
    end

    def apply
      return if skip_caching? || noncacheable?

      cache_control_value = response_context.cache_control(*cache_control_headers)
      etag_value = etag

      response_context.headers["Cache-Control"] = cache_control_value if cache_control_value

      # Both `last_modified` and `etag` `halt` with a 304 via Sinatra. According to the spec,
      # 200 and 304 requests must return the same headers. To implement this, we'll set
      # the required headers before relying on Sinatra to handle the rest.
      response_context.headers.merge! \
        "Vary" => vary_headers.join(", "),
        "ETag" => etag_value.inspect

      response_context.headers["Last-Modified"] = last_modified.httpdate if last_modified

      # Convert weak etags to strong etags for less strict matching
      if response_context.env["HTTP_IF_NONE_MATCH"] && response_context.env["HTTP_IF_NONE_MATCH"].start_with?("W/")
        response_context.env["HTTP_IF_NONE_MATCH"] = response_context.env["HTTP_IF_NONE_MATCH"][2..-1]
      end
      response_context.etag(etag_value)

      return unless last_modified

      response_context.last_modified(last_modified)
    end

    # Internal: Determine whether any caching headers should be applied to the
    # response.
    #
    # Returns a Boolean.
    def skip_caching?
      headers = response_context.headers

      (headers["Cache-Control"] && headers["Cache-Control"].include?("no-cache")) ||
        headers.key?("ETag") || headers.key?("Last-Modified")
    end

    def noncacheable?
      !cacheable?
    end

    # Internal: Determines whether we have enough information to apply caching
    # headers to the response.
    #
    # Returns a Boolean.
    def cacheable?
      options[:last_modified] || options[:etag] || etag_derivable_from_body?
    end

    def etag_derivable_from_body?
      etag_status? && etag_body?
    end

    def etag_status?
      response_context.status == 200 || response_context.status == 201
    end

    def etag_body?
      !!body
    end

    def vary_headers
      return @vary_headers if @vary_headers

      @vary_headers = DEFAULTS[:vary_headers]
      @vary_headers += AUTHENTICATED_VARY_HEADERS if authenticated_request?

      @vary_headers
    end

    def last_modified
      options[:last_modified]
    end

    def etag
      options[:etag] || etag_with(body)
    end

    # Internal: Construct an ETag using the Vary headers and the given
    # fingerprint.
    #
    # fingerprint - The String that uniquely identifies the response body.
    #
    # Returns the ETag String, or nil if the given fingerprint is nil.
    def etag_with(fingerprint)
      return if fingerprint.nil?

      etag_string = (variable_request_header_values << fingerprint.b).join(":")
      ::Digest::MD5.hexdigest etag_string
    end

    # Returns a string with binary encoding since users can send garbage bites in header values.
    # We just use these to create a digest and the digest doesn't care if the encoding is encoding is UTF-8.
    def variable_request_header_values
      header_values = []
      vary_headers.sort.each do |key|
        key = "HTTP_#{key.upcase.gsub("-", "_")}"
        # The headers are parsed as ASCII-8BIT, but a user could have sent
        # garbage characters which will :boom: when `.join`ed with a fingerprint
        # _if_ the fingerprint contains UTF-8 characters.
        # See https://github.com/github/github/issues/131858
        header_value = request_context.env[key]
        # If the header isn't present, the value will be `nil`; skip nils.
        if header_value
          header_values << fix_header_value_encoding(header_value)
        end
      end
      header_values
    end

    # `header_value` was pulled out of `request_context.env`, so
    # it might be a string or an array of strings.
    #
    # This method returns the same kind of data, but with strings forced to UTF-8.
    def fix_header_value_encoding(header_value)
      case header_value
      when String
        header_value.b
      when Array
        header_value.map { |v| fix_header_value_encoding(v) }
      else
        # This is some non-string, non-array value in the Rack env.
        # Don't log the value incase it somehow has PII
        raise ArgumentError, "Unexpected header value (#{header_value.class})"
      end
    end

    def cache_control_headers
      max_age = options["max-age"] || DEFAULTS[:max_age]

      [
        authenticated_request? ? "private" : "public",
        {"max-age" => max_age, "s-maxage" => max_age},
      ]
    end

    def authenticated_request?
      request_context.current_user
    end

    def body
      options[:body]
    end
  end
end
