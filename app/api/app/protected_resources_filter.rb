# frozen_string_literal: true

module Api::App::ProtectedResourcesFilter
  def filter_protected_resources(type:, resources:, protected_org_ids:)
    if logged_in?
      case type
        when :starred
          repo_ids = resources.map(&:starrable_id)
          repos = Repository.find(repo_ids)

          resources.to_a.delete_if { |star| protected_org_ids.include? repos.detect { |repo| repo.id == star.starrable_id }.organization_id }
        when :watched
          resources.delete_if { |watched| protected_org_ids.include? watched.organization_id  }
      end
    end
  end
end
