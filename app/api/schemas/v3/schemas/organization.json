{
  "$schema": "http://json-schema.org/draft-04/hyper-schema",
  "id": "https://schema.github.com/v3/organization.json",
  "title": "Organization",
  "description": "GitHub account for managing multiple users, teams, and repositories",
  "type": "object",
  "definitions": {
    "url": {
      "description": "URL for the organization",
      "example": "https://api.github.com/orgs/github",
      "type": "string",
      "format": "uri"
    },
    "login": {
      "description": "Unique login name of the organization",
      "example": "new-org",
      "type": "string"
    },
    "admin": {
      "description": "Login for a user to manage the organization",
      "example": "hubot",
      "type": "string"
    },
    "profile_name": {
      "description": "Display name for the organization",
      "example": "New Org",
      "type": "string"
    },
    "name": {
      "description": "Display name for the organization",
      "example": "New Org",
      "type": "string"
    },
    "email": {
      "description": "Display email for the organization",
      "example": "org@example.com",
      "type": "string",
      "format": "email"
    },
    "blog": {
      "description": "Display blog url for the organization",
      "example": "blog.example-org.com",
      "type": "string",
      "format": "uri"
    },
    "twitter_username": {
      "description": "Twitter username for this organization",
      "example": "github",
      "type": [
        "string",
        "null"
      ]
    },
    "company": {
      "description": "Display company name for the organization",
      "example": "Acme corporation",
      "type": "string"
    },
    "location": {
      "description": "Display location for the organization",
      "example": "Berlin, Germany",
      "type": "string"
    },
    "description": {
      "description": "Description of the organization",
      "example": "The new org is an organization of the people and for the people",
      "type": "string"
    },
    "billing_email": {
      "description": "Billing email address associated with organization",
      "example": "example@abc.com",
      "type": "string"
    },
    "default_repository_permission": {
      "description": "Default repository permission for org members",
      "type": "string",
      "enum": [
        "read",
        "write",
        "admin",
        "none"
      ]
    },
    "members_can_create_repositories": {
      "description": "Specifies if members can create repositories in this org",
      "type": "boolean"
    },
    "members_allowed_repository_creation_type": {
      "description": "Specifies the types of repositories that members can create in this org",
      "type": "string",
      "enum": [
        "all",
        "none",
        "private"
      ]
    },
    "members_can_create_public_repositories": {
      "description": "Specifies if members can create public repositories in this org. Overrides any value provided in members_can_create_repositories or members_allowed_repository_creation_type.",
      "type": [
        "boolean",
        "null"
      ]
    },
    "members_can_create_private_repositories": {
      "description": "Specifies if members can create private repositories in this org. Overrides any value provided in members_can_create_repositories or members_allowed_repository_creation_type.",
      "type": [
        "boolean",
        "null"
      ]
    },
    "members_can_create_internal_repositories": {
      "description": "Specifies if members can create internal repositories in this org. Overrides any value provided in members_can_create_repositories or members_allowed_repository_creation_type.",
      "type": [
        "boolean",
        "null"
      ]
    },
    "has_organization_projects": {
      "description": "Specifies if organization projects are enabled for this org",
      "type": "boolean"
    },
    "has_repository_projects": {
      "description": "Specifies if repository projects are enabled for repositories that belong to this org",
      "type": "boolean"
    },
    "legacy_name": {
      "description": "Display name for the organization",
      "example": "New Org",
      "type": [
        "string",
        "null"
      ]
    },
    "legacy_email": {
      "description": "Display email for the organization",
      "example": "org@example.com",
      "type": [
        "string",
        "null"
      ]
    },
    "legacy_blog": {
      "description": "Display blog url for the organization",
      "example": "blog.example-org.com",
      "type": [
        "string",
        "null"
      ]
    },
    "legacy_company": {
      "description": "Display company name for the organization",
      "example": "Acme corporation",
      "type": [
        "string",
        "null"
      ]
    },
    "legacy_location": {
      "description": "Display location for the organization",
      "example": "Berlin, Germany",
      "type": [
        "string",
        "null"
      ]
    },
    "legacy_description": {
      "description": "Description of the organization",
      "example": "The new org is an organization of the people and for the people",
      "type": [
        "string",
        "null"
      ]
    },
    "organization_id": {
      "description": "The ID of the organization.",
      "type": "integer"
    },
    "id": {
      "type": "integer"
    },
    "node_id": {
      "type": "string"
    },
    "repos_url": {
      "type": "string",
      "format": "uri"
    },
    "events_url": {
      "type": "string",
      "format": "uri"
    },
    "hooks_url": {
      "type": "string"
    },
    "issues_url": {
      "type": "string"
    },
    "members_url": {
      "type": "string"
    },
    "public_members_url": {
      "type": "string"
    },
    "avatar_url": {
      "type": "string"
    },
    "organization_description": {
      "type": [
        "string",
        "null"
      ]
    },
    "html_url": {
      "type": "string",
      "format": "uri"
    },
    "is_verified": {
      "type": "boolean"
    },
    "public_repos": {
      "type": "integer"
    },
    "public_gists": {
      "type": "integer"
    },
    "followers": {
      "type": "integer"
    },
    "following": {
      "type": "integer"
    },
    "type": {
      "type": "string"
    },
    "created_at": {
      "type": "string",
      "format": "date-time"
    },
    "updated_at": {
      "type": "string",
      "format": "date-time"
    },
    "plan": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "space": {
          "type": "number"
        },
        "private_repos": {
          "type": "number"
        },
        "filled_seats": {
          "type": "number"
        },
        "seats": {
          "type": "number"
        }
      }
    }
  },
  "links": [
    {
      "href": "/admin/organizations",
      "method": "POST",
      "rel": "create",
      "schema": {
        "properties": {
          "login": {
            "$ref": "#/definitions/login"
          },
          "admin": {
            "$ref": "#/definitions/admin"
          },
          "profile_name": {
            "$ref": "#/definitions/profile_name"
          }
        },
        "required": [
          "login",
          "admin"
        ],
        "type": "object"
      },
      "title": "Create a new organization"
    },
    {
      "href": "/organizations/{organization_id}",
      "method": "PATCH",
      "rel": "update-legacy",
      "schema": {
        "properties": {
          "name": {
            "$ref": "#/definitions/legacy_name"
          },
          "email": {
            "$ref": "#/definitions/legacy_email"
          },
          "blog": {
            "$ref": "#/definitions/legacy_blog"
          },
          "twitter_username": {
            "$ref": "#/definitions/twitter_username"
          },
          "company": {
            "$ref": "#/definitions/legacy_company"
          },
          "location": {
            "$ref": "#/definitions/legacy_location"
          },
          "description": {
            "$ref": "#/definitions/legacy_description"
          },
          "login": {
            "$ref": "#/definitions/login"
          },
          "billing_email": {
            "$ref": "#/definitions/billing_email"
          },
          "default_repository_permission": {
            "$ref": "#/definitions/default_repository_permission"
          },
          "members_can_create_repositories": {
            "$ref": "#/definitions/members_can_create_repositories"
          },
          "members_allowed_repository_creation_type": {
            "$ref": "#/definitions/members_allowed_repository_creation_type"
          },
          "members_can_create_public_repositories": {
            "$ref": "#/definitions/members_can_create_public_repositories"
          },
          "members_can_create_private_repositories": {
            "$ref": "#/definitions/members_can_create_private_repositories"
          },
          "members_can_create_internal_repositories": {
            "$ref": "#/definitions/members_can_create_internal_repositories"
          },
          "has_organization_projects": {
            "$ref": "#/definitions/has_organization_projects"
          },
          "has_repository_projects": {
            "$ref": "#/definitions/has_repository_projects"
          }
        },
        "type": [
          "object",
          "null"
        ]
      },
      "title": "Update an organization"
    },
    {
      "title": "Update an organization",
      "href": "/organizations/{organization_id}",
      "method": "POST",
      "rel": "update",
      "schema": {
        "properties": {
          "name": {
            "$ref": "#/definitions/name"
          },
          "email": {
            "$ref": "#/definitions/email"
          },
          "blog": {
            "$ref": "#/definitions/blog"
          },
          "company": {
            "$ref": "#/definitions/company"
          },
          "location": {
            "$ref": "#/definitions/location"
          },
          "description": {
            "$ref": "#/definitions/description"
          },
          "login": {
            "$ref": "#/definitions/login"
          },
          "billing_email": {
            "$ref": "#/definitions/billing_email"
          },
          "default_repository_permission": {
            "$ref": "#/definitions/default_repository_permission"
          },
          "members_can_create_repositories": {
            "$ref": "#/definitions/members_can_create_repositories"
          },
          "members_allowed_repository_creation_type": {
            "$ref": "#/definitions/members_allowed_repository_creation_type"
          },
          "members_can_create_public_repositories": {
            "$ref": "#/definitions/members_can_create_public_repositories"
          },
          "members_can_create_private_repositories": {
            "$ref": "#/definitions/members_can_create_private_repositories"
          },
          "members_can_create_internal_repositories": {
            "$ref": "#/definitions/members_can_create_internal_repositories"
          },
          "has_organization_projects": {
            "$ref": "#/definitions/has_organization_projects"
          },
          "has_repository_projects": {
            "$ref": "#/definitions/has_repository_projects"
          }
        },
        "type": [
          "object",
          "null"
        ]
      }
    },
    {
      "title": "Update an organization",
      "method": "PATCH",
      "href": "/organizations/{organization_id}",
      "rel": "update",
      "schema": {
        "properties": {
          "name": {
            "$ref": "#/definitions/name"
          },
          "email": {
            "$ref": "#/definitions/email"
          },
          "blog": {
            "$ref": "#/definitions/blog"
          },
          "company": {
            "$ref": "#/definitions/company"
          },
          "location": {
            "$ref": "#/definitions/location"
          },
          "description": {
            "$ref": "#/definitions/description"
          },
          "login": {
            "$ref": "#/definitions/login"
          },
          "billing_email": {
            "$ref": "#/definitions/billing_email"
          },
          "default_repository_permission": {
            "$ref": "#/definitions/default_repository_permission"
          },
          "members_can_create_repositories": {
            "$ref": "#/definitions/members_can_create_repositories"
          },
          "members_allowed_repository_creation_type": {
            "$ref": "#/definitions/members_allowed_repository_creation_type"
          },
          "members_can_create_public_repositories": {
            "$ref": "#/definitions/members_can_create_public_repositories"
          },
          "members_can_create_private_repositories": {
            "$ref": "#/definitions/members_can_create_private_repositories"
          },
          "members_can_create_internal_repositories": {
            "$ref": "#/definitions/members_can_create_internal_repositories"
          },
          "has_organization_projects": {
            "$ref": "#/definitions/has_organization_projects"
          },
          "has_repository_projects": {
            "$ref": "#/definitions/has_repository_projects"
          }
        },
        "type": [
          "object",
          "null"
        ],
        "additionalProperties": false
      }
    }
  ],
  "properties": {
    "login": {
      "$ref": "#/definitions/login"
    },
    "url": {
      "$ref": "#/definitions/url"
    },
    "id": {
      "$ref": "#/definitions/id"
    },
    "node_id": {
      "$ref": "#/definitions/node_id"
    },
    "repos_url": {
      "$ref": "#/definitions/repos_url"
    },
    "events_url": {
      "$ref": "#/definitions/events_url"
    },
    "hooks_url": {
      "$ref": "#/definitions/hooks_url"
    },
    "issues_url": {
      "$ref": "#/definitions/issues_url"
    },
    "members_url": {
      "$ref": "#/definitions/members_url"
    },
    "public_members_url": {
      "$ref": "#/definitions/public_members_url"
    },
    "avatar_url": {
      "$ref": "#/definitions/avatar_url"
    },
    "description": {
      "$ref": "#/definitions/organization_description"
    },
    "blog": {
      "$ref": "#/definitions/blog"
    },
    "html_url": {
      "$ref": "#/definitions/html_url"
    },
    "name": {
      "$ref": "#/definitions/name"
    },
    "company": {
      "$ref": "#/definitions/company"
    },
    "location": {
      "$ref": "#/definitions/location"
    },
    "email": {
      "$ref": "#/definitions/email"
    },
    "has_organization_projects": {
      "$ref": "#/definitions/has_organization_projects"
    },
    "has_repository_projects": {
      "$ref": "#/definitions/has_repository_projects"
    },
    "is_verified": {
      "$ref": "#/definitions/is_verified"
    },
    "public_repos": {
      "$ref": "#/definitions/public_repos"
    },
    "public_gists": {
      "$ref": "#/definitions/public_gists"
    },
    "followers": {
      "$ref": "#/definitions/followers"
    },
    "following": {
      "$ref": "#/definitions/following"
    },
    "type": {
      "$ref": "#/definitions/type"
    },
    "created_at": {
      "$ref": "#/definitions/created_at"
    },
    "updated_at": {
      "$ref": "#/definitions/updated_at"
    },
    "plan": {
      "$ref": "#/definitions/plan"
    }
  },
  "additionalProperties": false,
  "required": [
    "login",
    "url",
    "id",
    "node_id",
    "repos_url",
    "events_url",
    "hooks_url",
    "issues_url",
    "members_url",
    "public_members_url",
    "avatar_url",
    "description",
    "html_url",
    "has_organization_projects",
    "has_repository_projects",
    "public_repos",
    "public_gists",
    "followers",
    "following",
    "type",
    "created_at",
    "updated_at"
  ]
}
