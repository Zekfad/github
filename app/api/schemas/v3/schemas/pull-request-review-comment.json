{
  "$schema": "http://json-schema.org/draft-04/hyper-schema",
  "id": "https://schema.github.com/v3/pull-request-review-comment.json",
  "title": "Pull Request Review Comment",
  "description": "Pull Request Review Comments are comments on a portion of the Pull Request's diff.",
  "type": "object",
  "definitions": {
    "url": {
      "description": "URL for the pull request review comment",
      "example": "https://api.github.com/repos/octocat/Hello-World/pulls/comments/1",
      "type": "string"
    },
    "id": {
      "description": "The ID of the pull request review comment.",
      "example": 1,
      "type": "integer"
    },
    "node_id": {
      "description": "The node ID of the pull request review comment.",
      "type": "string",
      "example": "MDI0OlB1bGxSZXF1ZXN0UmV2aWV3Q29tbWVudDEw"
    },
    "pull_request_review_id": {
      "description": "The ID of the pull request review to which the comment belongs.",
      "example": 42,
      "type": "integer"
    },
    "diff_hunk": {
      "description": "The diff of the line that the comment refers to.",
      "type": "string",
      "example": "@@ -16,33 +16,40 @@ public class Connection : IConnection..."
    },
    "path": {
      "description": "The relative path of the file to which the comment applies.",
      "example": "config/database.yml",
      "type": "string"
    },
    "position": {
      "description": "The line index in the diff to which the comment applies.",
      "example": 1,
      "type": "integer"
    },
    "original_position": {
      "description": "The index of the original line in the diff to which the comment applies.",
      "example": 4,
      "type": "integer"
    },
    "line": {
      "description": "The line of the blob to which the comment applies. The last line of the range for a multi-line comment",
      "example": 2,
      "type": "integer"
    },
    "original_line": {
      "description": "The original line of the blob to which the comment applies. The last line of the range for a multi-line comment",
      "example": 2,
      "type": "integer"
    },
    "side": {
      "description": "The side of the diff to which the comment applies. The side of the last line of the range for a multi-line comment",
      "enum": [
        "LEFT",
        "RIGHT"
      ],
      "default": "RIGHT",
      "type": "string"
    },
    "start_line": {
      "description": "The first line of the range for a multi-line comment.",
      "example": 2,
      "type": "integer"
    },
    "original_start_line": {
      "description": "The original first line of the range for a multi-line comment.",
      "example": 2,
      "type": "integer"
    },
    "start_side": {
      "description": "The side of the first line of the range for a multi-line comment.",
      "enum": [
        "LEFT",
        "RIGHT"
      ],
      "default": "RIGHT",
      "type": "string"
    },
    "commit_id": {
      "description": "The SHA of the commit to which the comment applies.",
      "example": "6dcb09b5b57875f334f61aebed695e2e4193db5e",
      "type": "string"
    },
    "original_commit_id": {
      "description": "The SHA of the original commit to which the comment applies.",
      "example": "9c48853fa3dc5c1c3d6f1f1cd1f2743e72652840",
      "type": "string"
    },
    "in_reply_to_id": {
      "description": "The comment ID to reply to.",
      "example": 8,
      "type": "number"
    },
    "body": {
      "description": "The text of the comment.",
      "example": "We should probably include a check for null values here.",
      "type": "string"
    },
    "created_at": {
      "type": "string",
      "format": "date-time",
      "example": "2011-04-14T16:00:49Z"
    },
    "updated_at": {
      "type": "string",
      "format": "date-time",
      "example": "2011-04-14T16:00:49Z"
    },
    "html_url": {
      "description": "HTML URL for the pull request review comment.",
      "type": "string",
      "format": "uri",
      "example": "https://github.com/octocat/Hello-World/pull/1#discussion-diff-1"
    },
    "pull_request_url": {
      "description": "URL for the pull request that the review comment belongs to.",
      "type": "string",
      "format": "uri",
      "example": "https://api.github.com/repos/octocat/Hello-World/pulls/1"
    },
    "author_association": {
      "description": "How the author of the comment is associated with the pull request.",
      "type": "string",
      "example": ""
    },
    "in_reply_to": {
      "description": "The comment ID to reply to.",
      "example": 5,
      "type": "number"
    },
    "repository_id": {
      "description": "The ID of the repository.",
      "example": 42,
      "type": "integer"
    },
    "pull_number": {
      "description": "The number of the pull request.",
      "type": "integer"
    }
  },
  "links": [
    {
      "href": "/repositories/{repository_id}/pulls/{pull_number}/comments",
      "method": "POST",
      "rel": "create-legacy",
      "schema": {
        "oneOf": [
          {
            "properties": {
              "body": {
                "$ref": "#/definitions/body"
              },
              "commit_id": {
                "$ref": "#/definitions/commit_id"
              },
              "path": {
                "$ref": "#/definitions/path"
              },
              "position": {
                "$ref": "#/definitions/position"
              }
            },
            "required": [
              "body",
              "commit_id",
              "path",
              "position"
            ],
            "type": "object"
          },
          {
            "properties": {
              "body": {
                "$ref": "#/definitions/body"
              },
              "in_reply_to": {
                "$ref": "#/definitions/in_reply_to"
              }
            },
            "required": [
              "body",
              "in_reply_to"
            ],
            "type": "object"
          },
          {
            "properties": {
              "body": {
                "$ref": "#/definitions/body"
              },
              "commit_id": {
                "$ref": "#/definitions/commit_id"
              },
              "path": {
                "$ref": "#/definitions/path"
              },
              "start_line": {
                "$ref": "#/definitions/start_line"
              },
              "start_side": {
                "$ref": "#/definitions/start_side"
              },
              "line": {
                "$ref": "#/definitions/line"
              },
              "side": {
                "$ref": "#/definitions/side"
              }
            },
            "required": [
              "body",
              "commit_id",
              "path",
              "line"
            ],
            "additionalProperties": false,
            "type": "object"
          }
        ]
      },
      "title": "Create"
    },
    {
      "title": "Create a new pull request review comment",
      "method": "POST",
      "href": "/repositories/{repository_id}/pulls/{pull_number}/comments",
      "rel": "create",
      "schema": {
        "oneOf": [
          {
            "properties": {
              "body": {
                "$ref": "#/definitions/body"
              },
              "commit_id": {
                "$ref": "#/definitions/commit_id"
              },
              "path": {
                "$ref": "#/definitions/path"
              },
              "position": {
                "$ref": "#/definitions/position"
              }
            },
            "required": [
              "body",
              "commit_id",
              "path",
              "position"
            ],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "body": {
                "$ref": "#/definitions/body"
              },
              "in_reply_to": {
                "$ref": "#/definitions/in_reply_to"
              }
            },
            "required": [
              "body",
              "in_reply_to"
            ],
            "additionalProperties": false,
            "type": "object"
          }
        ]
      }
    },
    {
      "title": "Create a new reply pull request review comment",
      "method": "POST",
      "href": "/repositories/{repository_id}/pulls/{pull_number}/comments/{in_reply_to_id}/replies",
      "rel": "create-reply",
      "schema": {
        "properties": {
          "body": {
            "$ref": "#/definitions/body"
          }
        },
        "required": [
          "body"
        ],
        "additionalProperties": false,
        "type": "object"
      }
    }
  ],
  "properties": {
    "url": {
      "$ref": "#/definitions/url"
    },
    "id": {
      "$ref": "#/definitions/id"
    },
    "node_id": {
      "$ref": "#/definitions/node_id"
    },
    "pull_request_review_id": {
      "$ref": "#/definitions/pull_request_review_id"
    },
    "diff_hunk": {
      "$ref": "#/definitions/diff_hunk"
    },
    "path": {
      "$ref": "#/definitions/path"
    },
    "position": {
      "$ref": "#/definitions/position"
    },
    "original_position": {
      "$ref": "#/definitions/original_position"
    },
    "commit_id": {
      "$ref": "#/definitions/commit_id"
    },
    "original_commit_id": {
      "$ref": "#/definitions/original_commit_id"
    },
    "in_reply_to_id": {
      "$ref": "#/definitions/in_reply_to_id"
    },
    "user": {
      "$ref": "https://schema.github.com/v3/simple-user.json#"
    },
    "body": {
      "$ref": "#/definitions/body"
    },
    "created_at": {
      "$ref": "#/definitions/created_at"
    },
    "updated_at": {
      "$ref": "#/definitions/updated_at"
    },
    "html_url": {
      "$ref": "#/definitions/html_url"
    },
    "pull_request_url": {
      "$ref": "#/definitions/pull_request_url"
    },
    "author_association": {
      "$ref": "#/definitions/author_association"
    },
    "reactions": {
      "$ref": "https://schema.github.com/v3/reaction-rollup.json#"
    },
    "line": {
      "$ref": "#/definitions/line"
    },
    "original_line": {
      "$ref": "#/definitions/line"
    },
    "side": {
      "$ref": "#/definitions/side"
    },
    "start_line": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "$ref": "#/definitions/start_line"
        }
      ]
    },
    "original_start_line": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "$ref": "#/definitions/start_line"
        }
      ]
    },
    "start_side": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "$ref": "#/definitions/start_side"
        }
      ]
    },
    "_links": {
      "type": "object",
      "properties": {
        "self": {
          "type": "object",
          "properties": {
            "href": {
              "type": "string",
              "format": "uri",
              "example": "https://api.github.com/repos/octocat/Hello-World/pulls/comments/1"
            }
          },
          "additionalProperties": false,
          "required": [
            "href"
          ]
        },
        "html": {
          "type": "object",
          "properties": {
            "href": {
              "type": "string",
              "format": "uri",
              "example": "https://github.com/octocat/Hello-World/pull/1#discussion-diff-1"
            }
          },
          "additionalProperties": false,
          "required": [
            "href"
          ]
        },
        "pull_request": {
          "type": "object",
          "properties": {
            "href": {
              "type": "string",
              "format": "uri",
              "example": "https://api.github.com/repos/octocat/Hello-World/pulls/1"
            }
          },
          "additionalProperties": false,
          "required": [
            "href"
          ]
        }
      },
      "additionalProperties": false,
      "required": [
        "self",
        "html",
        "pull_request"
      ]
    }
  },
  "additionalProperties": false,
  "required": [
    "url",
    "id",
    "node_id",
    "pull_request_review_id",
    "diff_hunk",
    "path",
    "position",
    "original_position",
    "commit_id",
    "original_commit_id",
    "user",
    "body",
    "created_at",
    "updated_at",
    "html_url",
    "pull_request_url",
    "author_association",
    "_links"
  ]
}
