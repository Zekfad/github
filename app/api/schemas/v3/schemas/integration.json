{
  "$schema": "http://json-schema.org/draft-04/hyper-schema",
  "id": "https://schema.github.com/v3/integration.json",
  "title": "GitHub app",
  "description": "GitHub apps are a new way to extend GitHub. They can be installed directly on organizations and user accounts and granted access to specific repositories. They come with granular permissions and built-in webhooks. GitHub apps are first class actors within GitHub.",
  "type": "object",
  "definitions": {
    "id": {
      "description": "Unique identifier of the GitHub app",
      "example": 37,
      "type": "integer"
    },
    "slug": {
      "description": "The slug name of the GitHub app",
      "example": "probot-owners",
      "type": "string"
    },
    "version": {
      "description": "Version of the manifest used to create the GitHub App",
      "example": "v1",
      "type": "string",
      "enum": [
        "v1"
      ]
    },
    "name": {
      "description": "The name of the GitHub app",
      "example": "Probot Owners",
      "type": "string"
    },
    "description": {
      "description": "The description of the GitHub app",
      "example": "@mention maintainers in Pull Requests based on contents of the OWNERS file",
      "type": "string"
    },
    "url": {
      "description": "The homepage for your GitHub app",
      "example": "https://example.com",
      "type": "string"
    },
    "callback_url": {
      "description": "The full URL to redirect to after a user authorizes an installation",
      "example": "https://example.com/callback",
      "type": "string"
    },
    "setup_url": {
      "description": "A URL to redirect users to after they install your GitHub App if additional setup is required on your end",
      "example": "https://example.com/setup",
      "type": "string"
    },
    "setup_on_update": {
      "description": "Redirect users to the Setup URL after installations are updated (E.g. repositories added/removed)",
      "example": false,
      "type": "boolean"
    },
    "hook_attributes": {
      "description": "The configuration for your GitHub app's webhook",
      "example": {
        "url": "https://example.com",
        "active": true
      },
      "type": "object",
      "required": [
        "url"
      ],
      "properties": {
        "url": {
          "type": "string"
        },
        "active": {
          "example": false,
          "type": "boolean"
        }
      },
      "additionalProperties": false
    },
    "content_references": {
      "description": "The registered content references for which the GitHub App will have permission to create content attachments.",
      "type": "array",
      "minItems": 1,
      "maxItems": 5,
      "items": {
        "type": "object",
        "properties": {
          "type": {
            "type": "string",
            "enum": [
              "domain"
            ]
          },
          "value": {
            "type": "string"
          }
        }
      }
    },
    "default_permissions": {
      "description": "The set of permissions for the GitHub app",
      "example": {
        "issues": "read",
        "deployments": "write"
      },
      "type": "object"
    },
    "default_events": {
      "description": "The list of events for the GitHub app",
      "example": [
        "label",
        "deployment"
      ],
      "type": "array"
    },
    "single_file_name": {
      "description": "The single file name.",
      "example": ".github/issue_TEMPLATE.md",
      "type": "string"
    },
    "public": {
      "description": "Is the GitHub app available to the public?",
      "example": true,
      "type": "boolean"
    },
    "redirect_url": {
      "description": "The full URL to redirect to after a user initiates the creation of a GitHub App from a manifest",
      "example": "https://example.com/complete_creation",
      "type": "string"
    },
    "node_id": {
      "type": "string",
      "example": "MDExOkludGVncmF0aW9uMQ=="
    },
    "integration_description": {
      "type": [
        "string",
        "null"
      ],
      "example": ""
    },
    "external_url": {
      "type": "string",
      "format": "uri",
      "example": "https://example.com"
    },
    "html_url": {
      "type": "string",
      "format": "uri",
      "example": "https://github.com/apps/super-ci"
    },
    "installations_count": {
      "description": "The number of installations associated with the GitHub app",
      "example": 5,
      "type": "integer"
    },
    "created_at": {
      "type": "string",
      "format": "date-time",
      "example": "2017-07-08T16:18:44-04:00"
    },
    "updated_at": {
      "type": "string",
      "format": "date-time",
      "example": "2017-07-08T16:18:44-04:00"
    }
  },
  "links": [
    {
      "title": "Build a new GitHub App from a manifest.",
      "rel": "build",
      "schema": {
        "type": "object",
        "properties": {
          "version": {
            "$ref": "#/definitions/version"
          },
          "name": {
            "$ref": "#/definitions/name"
          },
          "description": {
            "$ref": "#/definitions/description"
          },
          "url": {
            "$ref": "#/definitions/url"
          },
          "callback_url": {
            "$ref": "#/definitions/callback_url"
          },
          "setup_url": {
            "$ref": "#/definitions/setup_url"
          },
          "setup_on_update": {
            "$ref": "#/definitions/setup_on_update"
          },
          "hook_attributes": {
            "$ref": "#/definitions/hook_attributes"
          },
          "content_references": {
            "$ref": "#/definitions/content_references"
          },
          "default_permissions": {
            "$ref": "#/definitions/default_permissions"
          },
          "default_events": {
            "$ref": "#/definitions/default_events"
          },
          "single_file_name": {
            "$ref": "#/definitions/single_file_name"
          },
          "public": {
            "$ref": "#/definitions/public"
          },
          "redirect_url": {
            "$ref": "#/definitions/redirect_url"
          }
        },
        "required": [
          "url",
          "redirect_url"
        ],
        "additionalProperties": false
      }
    }
  ],
  "properties": {
    "id": {
      "$ref": "#/definitions/id"
    },
    "slug": {
      "$ref": "#/definitions/slug"
    },
    "node_id": {
      "$ref": "#/definitions/node_id"
    },
    "owner": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "$ref": "https://schema.github.com/v3/simple-user.json#"
        }
      ]
    },
    "name": {
      "$ref": "#/definitions/name"
    },
    "description": {
      "$ref": "#/definitions/integration_description"
    },
    "external_url": {
      "$ref": "#/definitions/external_url"
    },
    "html_url": {
      "$ref": "#/definitions/html_url"
    },
    "installations_count": {
      "$ref": "#/definitions/installations_count"
    },
    "created_at": {
      "$ref": "#/definitions/created_at"
    },
    "updated_at": {
      "$ref": "#/definitions/updated_at"
    },
    "permissions": {
      "$ref": "#/definitions/default_permissions"
    },
    "events": {
      "$ref": "#/definitions/default_events"
    }
  },
  "additionalProperties": false,
  "required": [
    "id",
    "node_id",
    "owner",
    "name",
    "description",
    "external_url",
    "html_url",
    "created_at",
    "updated_at",
    "permissions",
    "events"
  ]
}
