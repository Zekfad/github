{
  "$schema": "http://json-schema.org/draft-04/hyper-schema",
  "id": "https://schema.github.com/v3/gist.json",
  "title": "Gist",
  "description": "Gists provide a simple way to share snippets and pastes with others. All gists are Git repositories, so they are automatically versioned, forkable and usable from Git.",
  "type": "object",
  "definitions": {
    "gist_id": {
      "description": "The ID of the gist.",
      "type": "integer"
    },
    "description": {
      "description": "Description of the gist",
      "example": "Example Ruby script",
      "type": "string"
    },
    "files": {
      "description": "Names and content for the files that make up the gist",
      "example": {
        "hello.rb": {
          "content": "puts \"Hello, World!\""
        }
      },
      "type": "object",
      "additionalProperties": {
        "type": "object",
        "properties": {
          "content": {
            "description": "Content of the file",
            "readOnly": false,
            "type": "string"
          }
        },
        "required": [
          "content"
        ]
      }
    },
    "updated_files": {
      "description": "Names of files to be updated",
      "example": {
        "hello.rb": {
          "content": "blah",
          "filename": "goodbye.rb"
        }
      },
      "type": "object",
      "additionalProperties": {
        "type": [
          "object",
          "null"
        ],
        "properties": {
          "content": {
            "description": "The new content of the file",
            "readOnly": false,
            "type": "string"
          },
          "filename": {
            "description": "The new filename for the file",
            "readOnly": false,
            "type": "string"
          }
        },
        "anyOf": [
          {
            "required": [
              "content"
            ]
          },
          {
            "required": [
              "filename"
            ]
          },
          {
            "type": "object",
            "maxProperties": 0
          }
        ]
      }
    },
    "public": {
      "description": "Flag indicating whether the gist is public",
      "example": true,
      "type": "boolean"
    },
    "public-input": {
      "anyOf": [
        {
          "$ref": "#/definitions/public"
        },
        {
          "type": "string",
          "enum": [
            "true",
            "false"
          ]
        },
        {
          "type": "null"
        }
      ]
    },
    "url": {
      "description": "URL for the gist",
      "example": "https://api.github.com/gists/aa5a315d61ae9438b18d",
      "type": "string",
      "format": "uri"
    },
    "id": {
      "type": "string",
      "example": "aa5a315d61ae9438b18d"
    },
    "node_id": {
      "type": "string",
      "example": "MDQ6R2lzdGFhNWEzMTVkNjFhZTk0MzhiMThk"
    },
    "forks_url": {
      "type": "string",
      "format": "uri",
      "example": "https://api.github.com/gists/aa5a315d61ae9438b18d/forks"
    },
    "commits_url": {
      "type": "string",
      "format": "uri",
      "example": "https://api.github.com/gists/aa5a315d61ae9438b18d/commits"
    },
    "git_pull_url": {
      "type": "string",
      "format": "uri",
      "example": "https://gist.github.com/aa5a315d61ae9438b18d.git"
    },
    "git_push_url": {
      "type": "string",
      "format": "uri",
      "example": "https://gist.github.com/aa5a315d61ae9438b18d.git"
    },
    "html_url": {
      "type": "string",
      "format": "uri",
      "example": "https://gist.github.com/aa5a315d61ae9438b18d"
    },
    "comments_url": {
      "type": "string",
      "format": "uri",
      "example": "https://api.github.com/gists/aa5a315d61ae9438b18d/comments/"
    },
    "truncated": {
      "type": "boolean"
    },
    "gist_description": {
      "type": [
        "string",
        "null"
      ],
      "example": "Hello World Examples"
    },
    "comments": {
      "type": "integer",
      "example": 0
    },
    "forks": {
      "type": "array"
    },
    "history": {
      "type": "array"
    },
    "created_at": {
      "type": "string",
      "format": "date-time",
      "example": "2010-04-14T02:15:15Z"
    },
    "updated_at": {
      "type": "string",
      "format": "date-time",
      "example": "2011-06-20T11:34:15Z"
    }
  },
  "links": [
    {
      "href": "/gists",
      "method": "POST",
      "rel": "create-legacy",
      "schema": {
        "properties": {
          "description": {
            "$ref": "#/definitions/description"
          },
          "files": {
            "$ref": "#/definitions/files"
          },
          "public": {
            "$ref": "#/definitions/public-input"
          }
        },
        "required": [
          "files"
        ],
        "type": "object"
      },
      "title": "Create a new gist"
    },
    {
      "href": "/gists/{gist_id}",
      "method": "PATCH",
      "rel": "update-legacy",
      "schema": {
        "properties": {
          "description": {
            "$ref": "#/definitions/description"
          },
          "files": {
            "$ref": "#/definitions/updated_files"
          }
        },
        "anyOf": [
          {
            "required": [
              "description"
            ]
          },
          {
            "required": [
              "files"
            ]
          }
        ],
        "type": [
          "object",
          "null"
        ]
      },
      "title": "Update a gist"
    },
    {
      "title": "Fork a gist",
      "method": "POST",
      "rel": "fork",
      "href": "/gists/{gist_id}/forks",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Star a gist",
      "method": "PUT",
      "rel": "star",
      "href": "/gists/{gist_id}/star",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "href": "/gists/{gist_id}",
      "method": "POST",
      "rel": "update",
      "schema": {
        "properties": {
          "description": {
            "$ref": "#/definitions/description"
          },
          "files": {
            "$ref": "#/definitions/updated_files"
          }
        },
        "anyOf": [
          {
            "required": [
              "description"
            ]
          },
          {
            "required": [
              "files"
            ]
          }
        ],
        "type": [
          "object",
          "null"
        ]
      },
      "title": "Update a gist"
    },
    {
      "title": "Delete a gist",
      "method": "DELETE",
      "rel": "delete",
      "href": "/gists/{gist_id}",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Unstar a gist",
      "method": "DELETE",
      "rel": "unstar",
      "href": "/gists/{gist_id}/star",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Create a new gist",
      "method": "POST",
      "href": "/gists",
      "rel": "create",
      "schema": {
        "properties": {
          "description": {
            "$ref": "#/definitions/description"
          },
          "files": {
            "$ref": "#/definitions/files"
          },
          "public": {
            "$ref": "#/definitions/public-input"
          }
        },
        "required": [
          "files"
        ],
        "type": "object",
        "additionalProperties": false
      }
    },
    {
      "title": "Update a gist",
      "method": "PATCH",
      "href": "/gists/{gist_id}",
      "rel": "update",
      "schema": {
        "properties": {
          "description": {
            "$ref": "#/definitions/description"
          },
          "files": {
            "$ref": "#/definitions/updated_files"
          }
        },
        "anyOf": [
          {
            "required": [
              "description"
            ]
          },
          {
            "required": [
              "files"
            ]
          }
        ],
        "type": [
          "object",
          "null"
        ],
        "additionalProperties": false
      }
    }
  ],
  "properties": {
    "id": {
      "$ref": "#/definitions/id"
    },
    "node_id": {
      "$ref": "#/definitions/node_id"
    },
    "url": {
      "$ref": "#/definitions/url"
    },
    "forks_url": {
      "$ref": "#/definitions/forks_url"
    },
    "commits_url": {
      "$ref": "#/definitions/commits_url"
    },
    "git_pull_url": {
      "$ref": "#/definitions/git_pull_url"
    },
    "git_push_url": {
      "$ref": "#/definitions/git_push_url"
    },
    "html_url": {
      "$ref": "#/definitions/html_url"
    },
    "comments_url": {
      "$ref": "#/definitions/comments_url"
    },
    "public": {
      "$ref": "#/definitions/public"
    },
    "truncated": {
      "$ref": "#/definitions/truncated"
    },
    "description": {
      "$ref": "#/definitions/gist_description"
    },
    "comments": {
      "$ref": "#/definitions/comments"
    },
    "owner": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "$ref": "https://schema.github.com/v3/simple-user.json#"
        }
      ]
    },
    "user": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "$ref": "https://schema.github.com/v3/simple-user.json#"
        }
      ]
    },
    "forks": {
      "$ref": "#/definitions/forks"
    },
    "history": {
      "$ref": "#/definitions/history"
    },
    "files": {
      "type": "object",
      "properties": {
      },
      "additionalProperties": true,
      "required": [

      ]
    },
    "created_at": {
      "$ref": "#/definitions/created_at"
    },
    "updated_at": {
      "$ref": "#/definitions/updated_at"
    },
    "fork_of": {
      "$ref": "https://schema.github.com/v3/base-gist.json#"
    }
  },
  "additionalProperties": false,
  "required": [
    "id",
    "node_id",
    "url",
    "forks_url",
    "commits_url",
    "git_pull_url",
    "git_push_url",
    "html_url",
    "comments_url",
    "public",
    "description",
    "comments",
    "user",
    "files",
    "created_at",
    "updated_at"
  ]
}
