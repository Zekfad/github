# rubocop:disable Style/FrozenStringLiteralComment

class Api::OrganizationTeamRepositories < Api::App
  areas_of_responsibility :orgs, :api

  # list repos in a team
  #
  # Note that this logic is essentially duplicated from the Repos Connection on
  # the Team GraphQL object. The reason it's duplicated here is that the
  # previous GraphQL implemenation was found to be significantly slower than
  # this implementation, causing timeouts.
  #
  # Related: https://github.com/github/github/pull/138931
  get "/organizations/:org_id/team/:team_id/repos" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#list-team-repositories"
    team = find_team!
    control_access :list_team_repos,
      resource: team,
      team: team,
      organization: team.try(:organization),
      allow_integrations: true,
      allow_user_via_integration: true

    scope = team.sorted_visible_repositories_for(
      current_user,
      affiliation: :all,
    )
      .where(owner_id: @current_org.id)
      .filter_spam_and_disabled_for(current_user)

    if current_app
      scope = ::Repository.oauth_app_policy_approved_repository_scope(
        repository_scope: scope,
        app: current_app,
      )
    end

    results = scope.
      includes(:network, :owner, :repository_license, :organization)
      .paginate(
        page: pagination[:page],
        per_page: per_page,
    )

    deliver :team_repo_hash, results, team: team
  end

  # get if a repo is in a team
  get "/organizations/:org_id/team/:team_id/repositories/:repository_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#check-team-permissions-for-a-repository"
    team = find_team!
    repo = find_repo

    control_access :get_team_repo,
      team: team,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    if team.direct_or_inherited_repo_ids(affiliation: :all).include?(repo.id)
      if medias.api_param?(:repository)
        GitHub::PrefillAssociations.for_repositories([repo])
        GitHub::PrefillAssociations.fill_licenses([repo])

        deliver :team_repo_hash, repo, team: team, status: 200
      else
        deliver_empty(status: 204)
      end
    else
      deliver_error 404
    end
  end

  UpdateTeamRepositoryQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($teamId: ID!, $repositoryId: ID!, $permission: RepositoryPermission!) {
      updateTeamRepository(input: { teamId: $teamId, repositoryId: $repositoryId, permission: $permission }) {
        __typename # a selection is grammatically required here
      }
    }
  GRAPHQL

  # add a repo to a team
  put "/organizations/:org_id/team/:team_id/repositories/:repository_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/teams#add-or-update-team-repository-permissions"
    team = find_team!
    repo = find_repo!

    has_get_team_repo_access = access_allowed? :get_team_repo,
      team: team,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    if has_get_team_repo_access
      set_forbidden_message "You must have administrative rights on this repository."
    end

    control_access :add_team_repo, team: team, repo: repo, allow_integrations: true, allow_user_via_integration: true

    # Introducing strict validation of the team-repository.replace
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("team-repository", "replace", skip_validation: true)

    if data.has_key?("permission")
      permission = data["permission"]
    else
      # In legacy org membership, teams had a team-wide permission that applied
      # to every repository on the team. In the API, we fall back to that
      # team-wide permission if none is specified, so that old API clients
      # operating on old teams will continue to function normally.
      GitHub.dogstats.increment "team.legacy_team_permission_default"
      permission = team.permission
    end

    permission = Team::PERMISSIONS_TO_ABILITIES.fetch(permission, permission).to_s.upcase

    variables = {
      teamId: team.global_relay_id,
      repositoryId: repo.global_relay_id,
      permission: permission,
    }

    results = platform_execute(UpdateTeamRepositoryQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "TeamRepository",
        documentation_url: @documentation_url,
      })
    else
      deliver_empty(status: 204)
    end
  end

  # remove a repo from a team
  delete "/organizations/:org_id/team/:team_id/repositories/:repository_id" do
    @route_owner = "@github/identity"

    # Introducing strict validation of the team-repository.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("team-repository", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/teams#remove-a-repository-from-a-team"
    @accepted_scopes = %w(admin:org repo)

    team = find_team!
    repo = find_repo!

    has_get_team_repo_access = access_allowed? :get_team_repo,
      team: team,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    if has_get_team_repo_access
      set_forbidden_message "You must have administrative rights on a repository or team in order to remove the repository from that team"
    end

    control_access :remove_team_repo, team: team, repo: repo, allow_integrations: true, allow_user_via_integration: true

    team.remove_repository repo

    deliver_empty(status: 204)
  end

  private

  # Finds the Team defined in the URL request and
  # sets the current_org if found.
  #
  # Returns a Team instance.
  def find_team(param_name: :team_id, org_param_name: :org_id)
    @team ||= Team.find_by(id: int_id_param!(key: param_name), organization_id: int_id_param!(key: org_param_name))
    @current_org ||= @team.try(:organization)
    @team
  end
end
